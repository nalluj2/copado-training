({
	searchForContracts : function(component) {
		
        var inputSearch = component.get("v.contractSearch");
        var inputAccount = component.get("v.selectedAccount");
        var accountId = (inputAccount != null ? inputAccount.Id : null);
        var inputContractType = component.get("v.contractType");
        var inputStartFromDate = component.get("v.startAfterDate");
        var inputStartToDate = component.get("v.startBeforeDate");
        var inputEndFromDate = component.get("v.endAfterDate");
        var inputEndToDate = component.get("v.endBeforeDate");
        var inputMPGCodes = component.get("v.mpgCodes");
                
        if(inputSearch != null && inputSearch != '' && inputSearch.length < 3){
            
            alert("You need to enter at least 3 characters for text search");
            return;
        }   
                
        component.set("v.loading", true);
        component.set("v.contracts", null);             	 
          
        var action = component.get("c.searchContracts");        
        action.setParams({
            searchText : component.get("v.contractSearch"), 
            accountId : accountId, 
            contractType: inputContractType,
            startFromDate: inputStartFromDate,
            startToDate: inputStartToDate,
            endFromDate: inputEndFromDate,
            endToDate: inputEndToDate,
            mpgCodes : inputMPGCodes
        });
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();            
            if(state === "SUCCESS") {
                
                var contracts = response.getReturnValue();
                
                if(contracts.length == 201){
                    
                    contracts.pop();
                    component.set("v.tooManyResults", true);
                    
                }else component.set("v.tooManyResults", false);
                
                if(contracts.length > 0) component.set("v.noContractsResults", false);
                else component.set("v.noContractsResults", true);
                
                component.set("v.contracts", contracts);
                
            } else {
                
                alert('Problem searching Contracts, response state: ' + state);
            }
            
            component.set("v.loading", false);            
        });
        
        $A.enqueueAction(action);
    },
    getPicklists : function(component){
        
    	var action = component.get("c.getPicklistValues");        
               
        action.setCallback(this, function(response) {
            
            var state = response.getState();            
            if(state === "SUCCESS") {
                
                var picklists = response.getReturnValue();
                
                component.set("v.contractTypeList", picklists.Contract_Type);
                component.set("v.mpgCodeList", picklists.MPG_Code);
                
            } else {
                
                alert('Problem searching Contracts, response state: ' + state);
            }
            
            component.set("v.loading", false);            
        });
        
        $A.enqueueAction(action);  
    },
    resize : function(component){
    	
    	var height = document.documentElement.clientHeight - 410;
        component.set("v.tableHeight", height);
    
	}
})