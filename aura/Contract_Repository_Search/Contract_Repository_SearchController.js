({
	init : function(component, event, helper) {
		
        window.addEventListener("resize",function(e){
  			helper.resize(component);
		});
        
        var columns = [               
            {label: 'Contract Id', fieldName: 'id', type: 'url', typeAttributes: {label: { fieldName: 'contractId' }, target: '_blank'}},
            {label: '(Customer) Contract #', fieldName: 'customerContractId', type: 'text'},
            {label: 'Account', fieldName: 'account', type: 'text'},
            {label: 'Contract Type', fieldName: 'contractType', type: 'text', wrapText: true},
            {label: 'MPG Codes', fieldName: 'mpgCodes', type: 'text'},
            {label: 'Valid From', fieldName: 'validFrom', type: 'date', typeAttributes: {day: 'numeric',  month: 'short', year: 'numeric'}},
            {label: 'Valid To', fieldName: 'validTo', type: 'date', typeAttributes: {day: 'numeric',  month: 'short', year: 'numeric'}}                 
        ];
        
        component.set("v.columns", columns);
        
        helper.resize(component);
        
        helper.getPicklists(component);
        
	},doSearch : function(component, event, helper){
        
        helper.searchForContracts(component);            
    }
})