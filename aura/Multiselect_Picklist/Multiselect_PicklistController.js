({
	doInit : function(component, event) {
		
        var selectedValues = component.get("v.selectedValues");
        
        var selectedValuesString = selectedValues != null ? selectedValues.sort().join(';') : '';
        component.set("v.selectedValesString", selectedValuesString);
        
        component.set("v.selectedPickListMulti", selectedValues);        
	},
    openPickListMulti : function(component, event){
		
      	component.set("v.showModelPickListMulti", true);
	},    
    cancel : function(component, event, helper){
        
        var selectedValues = component.get("v.selectedValues");
        
        component.set("v.selectedPickListMulti", selectedValues);
        component.set("v.showModelPickListMulti", false);
    },
    use : function(component, event, helper){
        
        var selectedPickListMulti = component.get("v.selectedPickListMulti");
                
        var selectedValuesString = selectedPickListMulti != null ? selectedPickListMulti.sort().join(';') : '';
        component.set("v.selectedValuesString", selectedValuesString);
        
        component.set("v.selectedValues", selectedPickListMulti);
        component.set("v.showModelPickListMulti", false);
    }
})