({
	applyHighlighting : function(component, event, helper) {
		
        var appEvent = $A.get("e.c:Highlight");        
		appEvent.fire();
	},
    clearHighlighting : function(component, event, helper) {
		
        component.set("v.highLightEventType", []);
        component.set("v.highLightAccount", null);
        component.set("v.highLightProcedure", []);
        component.set("v.highLightActivityType", []);
    }
})