({
	doInit : function(component, event, helper){
    
        helper.getSettings(component);
        helper.getPicklists(component);
    },
    save : function(component, event, helper){
    
        helper.saveSettings(component);
    },
    closePanel : function(component, event, helper) {
		
        var appEvent = $A.get("e.c:Close_Settings");        
		appEvent.fire();
        
	}    
})