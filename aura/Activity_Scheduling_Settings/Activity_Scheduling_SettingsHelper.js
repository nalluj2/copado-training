({
	getSettings : function(component) {
		
        component.set("v.showLoading", true);
        
        var action = component.get("c.getUserSettings");
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var resp = response.getReturnValue();
                
                component.set("v.settings", resp);
                component.set("v.showLoading", false);
                                
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
		$A.enqueueAction(action);
	},
    getPicklists : function(component) {
		
        var action = component.get("c.getCountryPicklistValues");
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var resp = response.getReturnValue();
                
                component.set("v.countryValues", resp);
                           
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
		$A.enqueueAction(action);
	},
    saveSettings : function(component) {
    	
        component.set("v.showLoading", true);
        
        var settings = component.get("v.settings");
        
    	var action = component.get("c.saveUserSettings");        
        action.setParams({ settings : settings});
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                               
                var appEvent = $A.get("e.c:Close_Settings");
				appEvent.fire();
 				                                
            } else if (state === "ERROR") {
                
                var errors = response.getError();
                if (errors) {
                    component.set("v.showError",true);
                    component.set("v.errorMessage", errors[0].message);
                    component.set("v.showLoading", false);
                    setTimeout(function(){
						component.set("v.showError",false);
                    	component.set("v.errorMessage", null);
                    }, 4000);
                } 
            }
        });
        
		$A.enqueueAction(action);
	}
})