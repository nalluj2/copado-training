({
	getCaseInfo : function(component, callback) {
		
        component.set("v.showLoading", true);
        
        var caseId = component.get("v.activityId");
        
        var action = component.get("c.getCase");
        action.setParams({ caseId : caseId });
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var resp = response.getReturnValue();
                
                if(resp.caseRecord.Activity_Scheduling_Attendees__r == null) resp.caseRecord.Activity_Scheduling_Attendees__r = [];
                
                component.set("v.canEdit", resp.canEdit); 
                component.set("v.case", resp.caseRecord); 
                component.set("v.selectedAccount", resp.caseRecord.Account); 
                if(resp.caseRecord.Product_Group__r != null) component.set("v.selectedSBU", resp.caseRecord.Product_Group__r.Therapy_ID__r.Sub_Business_Unit__c); 
                component.set("v.event", null); 
                component.set("v.showLoading", false);  
                
                if(resp.caseRecord.Is_Recurring__c == true){
                    
                	var days = resp.caseRecord.Recurring_Week_Days__c;
                    
                    if(days.includes('1')) component.set("v.recurringMonday" , true);
                    else component.set("v.recurringMonday" , false);
                    
                    if(days.includes('2')) component.set("v.recurringTuesday" , true);
                    else component.set("v.recurringTuesday" , false);
                    
                    if(days.includes('3')) component.set("v.recurringWednesday" , true);
                    else component.set("v.recurringWednesday" , false);
                    
                    if(days.includes('4')) component.set("v.recurringThursday" , true);
                    else component.set("v.recurringThursday" , false);
                    
                    if(days.includes('5')) component.set("v.recurringFriday" , true);
                    else component.set("v.recurringFriday" , false);
                    
                    if(days.includes('6')) component.set("v.recurringSaturday" , true);
                    else component.set("v.recurringSaturday" , false);
                    
                    if(days.includes('7')) component.set("v.recurringSunday" , true);
                    else component.set("v.recurringSunday" , false);
                }
                
                if(callback) callback();
                                
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
		$A.enqueueAction(action);
	},    
    getEventInfo : function(component) {
		
        component.set("v.showLoading", true);
        
        var eventId = component.get("v.activityId");
        
        var action = component.get("c.getEvent");
        action.setParams({ eventId : eventId });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                                
                var resp = response.getReturnValue();
 				
                component.set("v.canEdit", resp.canEdit);
                component.set("v.event", resp.eventRecord);   
                component.set("v.case", null); 
                
                if(resp.eventRecord.IsRecurrence == true){
                                        
                	var dayMask = resp.eventRecord.RecurrenceDayOfWeekMask;
                    
                    if(dayMask >= 64){
                        
                        component.set("v.recurringSaturday" , true);
                        dayMask = dayMask - 64;
                        
                    }else component.set("v.recurringSaturday" , false);
                    
                    if(dayMask >= 32){
                        
                        component.set("v.recurringFriday" , true);
                        dayMask = dayMask - 32;
                        
                    }else component.set("v.recurringFriday" , false);
                    
                    if(dayMask >= 16){
                        
                        component.set("v.recurringThursday" , true);
                        dayMask = dayMask - 16;
                        
                    }else component.set("v.recurringThursday" , false);
                    
                    if(dayMask >= 8){
                        
                        component.set("v.recurringWednesday" , true);
                        dayMask = dayMask - 8;
                        
                    }else component.set("v.recurringWednesday" , false);
                    
                    if(dayMask >= 4){
                        
                        component.set("v.recurringTuesday" , true);
                        dayMask = dayMask - 4;
                        
                    }else component.set("v.recurringTuesday" , false);
                                        
                    if(dayMask >= 2){
                        
                        component.set("v.recurringMonday" , true);
                        dayMask = dayMask - 2;
                        
                    }else component.set("v.recurringMonday" , false);
                    
                    if(dayMask >= 1){
                        
                        component.set("v.recurringSunday" , true);
                        dayMask = dayMask - 1;
                        
                    }else component.set("v.recurringSunday" , false);
                }                
                
                component.set("v.showLoading", false);  
                
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
		$A.enqueueAction(action);
	},
    upsertCase : function(component, assign) {
		
        component.set("v.showLoading", true);  
        
        var caseRecord = component.get("v.case");
        
        var attendees = [];
        
        for(var i = 0; i < caseRecord.Activity_Scheduling_Attendees__r.length; i++){
        	
            if(caseRecord.Activity_Scheduling_Attendees__r[i].Attendee__c != null && caseRecord.Activity_Scheduling_Attendees__r[i].Attendee__c != ''){
                
                if(caseRecord.Assigned_To__c == null || caseRecord.Assigned_To__c == '') caseRecord.Assigned_To__c = caseRecord.Activity_Scheduling_Attendees__r[i].Attendee__c;
                else attendees.push(caseRecord.Activity_Scheduling_Attendees__r[i]);
            }
        }  
        
        if(caseRecord.Is_Recurring__c == true){
            
            var recurringMonday = component.get("v.recurringMonday");
            var recurringTuesday = component.get("v.recurringTuesday");
            var recurringWednesday = component.get("v.recurringWednesday");
            var recurringThursday = component.get("v.recurringThursday");
            var recurringFriday = component.get("v.recurringFriday");
            var recurringSaturday = component.get("v.recurringSaturday");
            var recurringSunday = component.get("v.recurringSunday");
         	
            var days = [];
            
            if(recurringMonday) days.push('1');
            if(recurringTuesday) days.push('2');
            if(recurringWednesday) days.push('3');
            if(recurringThursday) days.push('4');
            if(recurringFriday) days.push('5');
            if(recurringSaturday) days.push('6');
            if(recurringSunday) days.push('7');
            
            caseRecord.Recurring_Week_Days__c = days.join(';');
        }
        
        var action = component.get("c.saveCase");
        
        action.setParams({ caseRecord : caseRecord, attendees : attendees });
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var caseId = response.getReturnValue();
                component.set("v.activityId", caseId);
                
                var appEvent = $A.get("e.c:Refresh_Calendar");
				appEvent.fire();
 				var appEvent = $A.get("e.c:Select_Activity");
        		appEvent.setParams({action : "Close"});
				appEvent.fire();
                
                if(assign){
                    
                    var appEvent = $A.get("e.c:Show_Scheduler");
        			appEvent.setParams({action : "Open"});
					appEvent.fire();
                }
                                
            } else if (state === "ERROR") {
                
                var errors = response.getError();
                if (errors) {
                    component.set("v.showError",true);
                    component.set("v.errorMessage", errors[0].message);
                    component.set("v.showLoading", false);
                    setTimeout(function(){
						component.set("v.showError",false);
                    	component.set("v.errorMessage", null);
                    }, 4000);
                } 
            }
        });
        
		$A.enqueueAction(action);
	},    
    upsertEvent : function(component) {
		
        component.set("v.showLoading", true);  
        
        var eventRecord = component.get("v.event");
        
        if(eventRecord.IsRecurrence == true){
            
            eventRecord.RecurrenceType = 'RecursWeekly';
            eventRecord.RecurrenceStartDateTime = eventRecord.StartDateTime;
            
            var recurringMonday = component.get("v.recurringMonday");
            var recurringTuesday = component.get("v.recurringTuesday");
            var recurringWednesday = component.get("v.recurringWednesday");
            var recurringThursday = component.get("v.recurringThursday");
            var recurringFriday = component.get("v.recurringFriday");
            var recurringSaturday = component.get("v.recurringSaturday");
            var recurringSunday = component.get("v.recurringSunday");
         	
            var dayMask = 0;
            
            if(recurringSunday) dayMask += 1;
            if(recurringMonday) dayMask += 2;
            if(recurringTuesday) dayMask += 4;
            if(recurringWednesday) dayMask += 8;
            if(recurringThursday) dayMask += 16;
            if(recurringFriday) dayMask += 32;
            if(recurringSaturday) dayMask += 64;
            console.log("recurringFriday: " + recurringFriday);
            console.log("dayMask: " + dayMask);
            eventRecord.RecurrenceDayOfWeekMask = parseInt(dayMask, 10);
        }
        
        var action = component.get("c.saveEvent");
        action.setParams({ eventRecord : eventRecord });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('state' + state);
            if (state === "SUCCESS") {
                
                var appEvent = $A.get("e.c:Refresh_Calendar");
				appEvent.fire();
                var appEvent = $A.get("e.c:Select_Activity");
        		appEvent.setParams({action : "Close"});
				appEvent.fire();
                
            } else if (state === "ERROR") {
                
                var errors = response.getError();
                if(errors) {
                    component.set("v.showError",true);
                    component.set("v.errorMessage", errors[0].message);
                    component.set("v.showLoading", false);
                    setTimeout(function(){
						component.set("v.showError",false);
                    	component.set("v.errorMessage", null);
                    }, 4000);
                } 
            }
        });
        
		$A.enqueueAction(action);
        
    },deleteCase : function(component) {
		
        component.set("v.showLoading", true);  
        
        var caseRecord = component.get("v.case");
        
        var action = component.get("c.removeCase");
        action.setParams({ caseRecord : caseRecord });
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var appEvent = $A.get("e.c:Refresh_Calendar");
				appEvent.fire();
 				var appEvent = $A.get("e.c:Select_Activity");
        		appEvent.setParams({action : "Close"});
				appEvent.fire();
                                
            } else if (state === "ERROR") {
                
                var errors = response.getError();
                if(errors) {
                    component.set("v.showError",true);
                    component.set("v.errorMessage", errors[0].message);
                    component.set("v.showLoading", false);
                    setTimeout(function(){
						component.set("v.showError",false);
                    	component.set("v.errorMessage", null);
                    }, 4000);
                } 
            }
        });
        
		$A.enqueueAction(action);
	},    
    deleteEvent : function(component) {
		
        component.set("v.showLoading", true);  
        
        var eventRecord = component.get("v.event");
        
        var action = component.get("c.removeEvent");
        action.setParams({ eventRecord : eventRecord });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var appEvent = $A.get("e.c:Refresh_Calendar");
				appEvent.fire();
                var appEvent = $A.get("e.c:Select_Activity");
        		appEvent.setParams({action : "Close"});
				appEvent.fire();
	                
            } else if (state === "ERROR") {
                
                var errors = response.getError();
                if(errors) {
                    component.set("v.showError",true);
                    component.set("v.errorMessage", errors[0].message);
                    component.set("v.showLoading", false);
                    setTimeout(function(){
						component.set("v.showError",false);
                    	component.set("v.errorMessage", null);
                    }, 4000);
                } 
            }
        });
        
		$A.enqueueAction(action);
    },
    getContacts : function(component, accountId){
        
        component.set("v.showLoadingContact", true);
                
        var action = component.get("c.getContactsForAccount");
        action.setParams({ accountId : accountId });
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var contacts = response.getReturnValue();
                
                var accountContacts = [];
                
                for(var i = 0; i < contacts.length; i++){
                    
                    var contactName = contacts[i].LastName + (contacts[i].FirstName != null ? (', ' + contacts[i].FirstName) : '');
                    accountContacts.push({value : contacts[i].Id, label : contactName});
                }
                
                component.set("v.accountContacts", accountContacts);   
                
                component.set("v.showLoadingContact", false);  
                
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
		$A.enqueueAction(action);
    },
    getTeamMembers : function(component, teamId){
        
        component.set("v.showLoadingMember", true); 
                
        var action = component.get("c.getTeamUsers");
        action.setParams({ teamId : teamId });
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                component.set("v.teamMembers", response.getReturnValue());   
                
                component.set("v.showLoadingMember", false);   
                
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
		$A.enqueueAction(action);
    },
    getProductGroups : function(component, sbuId){
        
        component.set("v.showLoadingProductGroup", true); 
                
        var action = component.get("c.getSBUProductGroups");
        action.setParams({ sbuId : sbuId });
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                component.set("v.productGroups", response.getReturnValue());   
                
                component.set("v.showLoadingProductGroup", false); 
                
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
		$A.enqueueAction(action);
    }
})