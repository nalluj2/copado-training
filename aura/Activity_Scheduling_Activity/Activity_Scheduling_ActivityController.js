({
    doInit : function(component, event, helper){
                
        var userId = $A.get( "$SObjectType.CurrentUser.Id" );
        component.set("v.userId", userId);
        
        var isAgent = component.get("v.isAgent");
        
        if(component.get("v.activityId") != null){
            
            if(component.get("v.activityType") == 'event') helper.getEventInfo(component);
            else helper.getCaseInfo(component);
            
        }else{
            
            var activityStartTime = component.get("v.activityStartTime");
            
            var startDateTime;
            var endDateTime;
            if(activityStartTime != null){
                
                startDateTime = new Date(activityStartTime);           
            	endDateTime = new Date(startDateTime.getTime() + 60*60*1000);
            }
            
            if(component.get("v.activityType") == 'event'){
                
                var newEvent = {'sobjectType':'Event'};
                if(startDateTime != null) newEvent.StartDateTime = startDateTime.toISOString();                
                if(endDateTime != null) newEvent.EndDateTime = endDateTime.toISOString();
                component.set("v.event", newEvent); 
                
            }else{
                
                var picklistValues = component.get("v.picklistValues");
                var selectedMember = component.get("v.memberId");
                var activityType = component.get("v.activityType");
                
                var newCase = {'sobjectType':'Case'};
                newCase.Status = "Open";
                newCase.Activity_Scheduling_Attendees__r = [];
                if(activityType != 'meeting') newCase.Origin = "Customer";
                if(startDateTime != null) newCase.Start_of_Procedure__c = startDateTime.toISOString(); 
                
                if(activityType == 'implant'){
                    
                    newCase.Venue__c = 'Operating Room';
                	newCase.Type = 'Implant Support Request';
                    newCase.Activity_Type_Picklist__c = 'Implant Support';
                    
                }else if(activityType == 'service'){
                    
                    newCase.Type = 'Service Request';                    
                    
                }else if(activityType == 'meeting'){
                    
                    newCase.Type = 'Meeting';                    
                }
                
                if(activityType != 'meeting'){
                	
                    var today = new Date();               
                	newCase.Date_Received_Date__c = today.toISOString().split('T')[0]; 
                }
                
                var teamId = component.get("v.teamId");
                if(teamId != null && teamId != "personal" && teamId != "list"){
                    
                    newCase.Activity_Scheduling_Team__c = teamId;
					
                    if(isAgent == false || selectedMember == userId) newCase.Assigned_To__c = selectedMember;
                    
                }else{
                                                         
                    if(picklistValues.Activity_Scheduling_Team__c.length == 2) newCase.Activity_Scheduling_Team__c = picklistValues.Activity_Scheduling_Team__c[1].value;                    
                }
                
                if(newCase.Activity_Scheduling_Team__c != null) helper.getTeamMembers(component, newCase.Activity_Scheduling_Team__c);
				
                if(activityType != 'meeting'){
                    
                    if(picklistValues.DefaultSBU.length > 0){
                        
                        helper.getProductGroups(component, picklistValues.DefaultSBU[0].value);
                        component.set("v.selectedSBU", picklistValues.DefaultSBU[0].value);
                    }                
                }
                
                component.set("v.case", newCase);  
                component.set("v.selectedAccount", null);  
            }
            
            component.set("v.recurringMonday", false);
            component.set("v.recurringTuesday", false);
            component.set("v.recurringWednesday", false);
            component.set("v.recurringThursday", false);
            component.set("v.recurringFriday", false);
            component.set("v.recurringSaturday", false);
            component.set("v.recurringSunday", false);			
            component.set("v.showLoading", false);
        }   
        
	},
    editRecord : function(component, event, helper) {
		
        component.set("v.mode", 'edit');
        
        if(component.get("v.activityType") != 'event'){
            
            var selectedCase = component.get("v.case");
            helper.getTeamMembers(component, selectedCase.Activity_Scheduling_Team__c);
            
            if(component.get("v.activityType") != 'meeting'){
            	
                var selectedAccount = component.get("v.selectedAccount");
            	helper.getContacts(component, selectedAccount.Id);       
            
                var selectedSBU = component.get("v.selectedSBU");
                if(selectedSBU == null){
                    
                    var picklistValues = component.get("v.picklistValues");
                    if(picklistValues.DefaultSBU.length > 0){
                        
                        selectedSBU = picklistValues.DefaultSBU[0].value;
                        component.set("v.selectedSBU", selectedSBU);
                    } 
                }
                
                if(selectedSBU != null) helper.getProductGroups(component, selectedSBU);
            }
        }
        
	},
    editRecordSeries : function(component, event, helper) {
		
        component.set("v.mode", 'edit');
        
        if(component.get("v.activityType") == 'event'){
        
        	var event = component.get("v.event");
        	component.set("v.activityId", event.RecurrenceActivityId);
            
            helper.getEventInfo(component);     
            
        }else{
            
            var selectedCase = component.get("v.case");
            component.set("v.activityId", selectedCase.ParentId);
            
            helper.getCaseInfo(component,                  
            	function(){
                
                	var selectedCase = component.get("v.case");
                    helper.getTeamMembers(component, selectedCase.Activity_Scheduling_Team__c);
                    
                    if(component.get("v.activityType") != 'meeting'){
                        
                        var selectedAccount = component.get("v.selectedAccount");
                        helper.getContacts(component, selectedAccount.Id);       
                    
                        var selectedSBU = component.get("v.selectedSBU");
                        if(selectedSBU == null){
                            
                            var picklistValues = component.get("v.picklistValues");
                            if(picklistValues.DefaultSBU.length > 0){
                                
                                selectedSBU = picklistValues.DefaultSBU[0].value;
                                component.set("v.selectedSBU", selectedSBU);
                            } 
                        }
                        
                        if(selectedSBU != null) helper.getProductGroups(component, selectedSBU);
                    }
            });            
        }
	},
    cloneRecord : function(component, event, helper) {
		
        component.set("v.mode", 'edit');
        
        var selectedCase = component.get("v.case");
        var activityType = component.get("v.activityType");
        
        var newCase = {'sobjectType':'Case'};       
        newCase.Status = 'Open';
        newCase.Activity_Scheduling_Attendees__r = [];
        
		if(activityType != 'meeting'){        
        	
            newCase.Date_Received_Date__c = (new Date()).toISOString().split('T')[0];
        	newCase.Procedure__c = selectedCase.Procedure__c;        
        	newCase.Activity_Type_Picklist__c = selectedCase.Activity_Type_Picklist__c;                	
        	newCase.Product_Group__c  = selectedCase.Product_Group__c;
        	newCase.Origin = "Customer";
        }
        
        newCase.Is_Recurring__c = selectedCase.Is_Recurring__c;
        newCase.Recurring_N_Weeks__c = selectedCase.Recurring_N_Weeks__c;
        newCase.Recurring_End_Date__c = selectedCase.Recurring_End_Date__c
        
        newCase.Activity_Scheduling_Team__c = selectedCase.Activity_Scheduling_Team__c;
        
        if(activityType == 'implant'){
                    
            newCase.Venue__c = "Operating Room";                    
            newCase.Type = 'Implant Support Request';
            
        }else if(activityType == 'service'){
            
            newCase.Type = 'Service Request';
            
        }else{
            
            newCase.Type = 'Meeting';
            newCase.Subject = selectedCase.Subject;
            newCase.Assigned_To__c = selectedCase.Assigned_To__c;
                        
            for(var i = 0; i < selectedCase.Activity_Scheduling_Attendees__r.length; i++){
                
                var attendee = selectedCase.Activity_Scheduling_Attendees__r[i];
                attendee.Id = null;
                attendee.Case__c = null;
            	newCase.Activity_Scheduling_Attendees__r.push(attendee);    
            }
        }
        
        component.set("v.case", newCase);
        
        helper.getTeamMembers(component, selectedCase.Activity_Scheduling_Team__c);
        
        if(activityType != 'meeting'){ 
            
            var selectedSBU = component.get("v.selectedSBU");
            if(selectedSBU == null){
                
                var picklistValues = component.get("v.picklistValues");
                if(picklistValues.DefaultSBU.length > 0){
                    
                    selectedSBU = picklistValues.DefaultSBU[0].value;
                    component.set("v.selectedSBU", selectedSBU);
                } 
            }
            
            if(selectedSBU != null) helper.getProductGroups(component, selectedSBU);
        }
        
	},
    saveRecord : function(component, event, helper) {
		
        var caseRecord = component.get("v.case");
        var eventRecord = component.get("v.event");
        
        if($A.util.isEmpty(caseRecord) == false){
         	
            if(caseRecord.Id != null && caseRecord.Is_Recurring__c == true && confirm('This action will update all future instances of this series. Do you want to continue?') == false) return;
               
            var selectedAccount = component.get("v.selectedAccount");
            
            if($A.util.isEmpty(selectedAccount)) caseRecord.AccountId = null;
            else caseRecord.AccountId = selectedAccount.Id;
            
            helper.upsertCase(component);
        }
        else{
            
            if(eventRecord.Id != null && eventRecord.IsRecurrence == true && confirm('This action will update all future instances of this series. Do you want to continue?') == false) return;
            
            helper.upsertEvent(component, false);
        }
        
	},
    saveRecordAssign : function(component, event, helper) {
		
        var caseRecord = component.get("v.case");
            
        var selectedAccount = component.get("v.selectedAccount");
            
        if($A.util.isEmpty(selectedAccount)) caseRecord.AccountId = null;
        else caseRecord.AccountId = selectedAccount.Id;
            
        helper.upsertCase(component, true);
	},
    deleteRecord : function(component, event, helper) {
		
        if(confirm("Are you sure you want to delete this activity?")) {
            
            var caseRecord = component.get("v.case");
            var eventRecord = component.get("v.event");
            
            if($A.util.isEmpty(caseRecord) == false) helper.deleteCase(component);
            else helper.deleteEvent(component);
        }
	},
    cancelRejectRequest : function(component, event, helper) {
		
        var caseRecord = component.get("v.case");
        caseRecord.Status = 'Rejected';
        caseRecord.Assigned_To__c = null;
        caseRecord.Reason_for_Rejection_Cancellation__c = null;
        caseRecord.Activity_Scheduling_Attendees__r = [];
        
        component.set("v.mode", 'cancelReject');
        component.set("v.case", caseRecord);
	},    
    rejectAssignment : function(component, event, helper) {
		
        var caseRecord = component.get("v.case");
        caseRecord.Status = 'Open';
        caseRecord.Assigned_To__c = null;
        caseRecord.Reason_for_Reject_Cancel_Assigned_to__c = null;
        caseRecord.Activity_Scheduling_Attendees__r = [];
        
        component.set("v.mode", 'rejectAssignment');
        component.set("v.case", caseRecord);
        
	},
    reopenRecord : function(component, event, helper) {
		
        var caseRecord = component.get("v.case");
        caseRecord.Status = 'Open';
        component.set("v.case", caseRecord);
        
        component.set("v.mode", 'edit');        
	},
    cancelEdit : function(component, event, helper) {
		
        component.set("v.mode", 'view');
        
        if(component.get("v.activityType") == 'event') helper.getEventInfo(component);
        else helper.getCaseInfo(component);
        
	},
    closePanel : function(component, event, helper) {
		
        var appEvent = $A.get("e.c:Select_Activity");
        appEvent.setParams({action : "Close"});
		appEvent.fire();
        
	},
    assignActivity : function(component, event, helper) {
		
        var appEvent = $A.get("e.c:Show_Scheduler");
        appEvent.setParams({action : "Open"});
		appEvent.fire();
        
        var appEvent = $A.get("e.c:Select_Activity");
        appEvent.setParams({action : "Close"});
		appEvent.fire();
	},
    getAccountContacts : function(component, event, helper){
        
        var mode = component.get("v.mode");
        
        if(mode != 'edit') return;
        
        var selectedAccount = component.get("v.selectedAccount");
        
        if(selectedAccount == null || selectedAccount.Id == null){
        
            component.set("v.accountContacts", []);
            
            var selectedCase = component.get("v.case");
            selectedCase.ContactId = null;
            component.set("v.case", selectedCase);
            
        }else{
        
        	helper.getContacts(component, selectedAccount.Id);
        }
    },
    getTeamMembers : function(component, event, helper){
        
        var selectedCase = component.get("v.case");
        
        if(selectedCase.Assigned_To__c != null){            	
            selectedCase.Assigned_To__c = null;
            selectedCase.Activity_Scheduling_Attendees__r = [];
            component.set("v.case", selectedCase);
        }
        
        if(selectedCase.Activity_Scheduling_Team__c == null || selectedCase.Activity_Scheduling_Team__c == ''){
        
            component.set("v.teamMembers", []);
            
        }else{
        
        	helper.getTeamMembers(component, selectedCase.Activity_Scheduling_Team__c);
        }
    },
    getProductGroups : function(component, event, helper){
        
        var selectedSBU = component.get("v.selectedSBU");
        var selectedCase = component.get("v.case");
            
        if(selectedCase.Product_Group__c != null){            
            selectedCase.Product_Group__c = null;
            component.set("v.case", selectedCase);
        }
        
        if(selectedSBU == null || selectedSBU == '' ){
        	
            component.set("v.productGroups", []);
            
        }else{
         
            helper.getProductGroups(component, selectedSBU);
        }
    },
    checkMinutesCaseStart : function(component, event, helper){
     
        var selectedCase = component.get("v.case");
            
        if(selectedCase.Start_of_Procedure__c != null){            
            
            var start = new Date(selectedCase.Start_of_Procedure__c);
            var minutes = start.getMinutes();  
            
            if((minutes%15) != 0){
                
                minutes = minutes - (minutes%15);            
            	start.setMinutes(minutes);
            	
            	selectedCase.Start_of_Procedure__c = start.toISOString();
                
                setInterval(function(){
                	component.set("v.case", selectedCase);                    
                }, 100);                
            }
        }        
    },
    checkMinutesCaseEnd : function(component, event, helper){
     
        var selectedCase = component.get("v.case");
            
        if(selectedCase.End_of_Procedure_DateTime__c != null){            
            
            var start = new Date(selectedCase.End_of_Procedure_DateTime__c);
            var minutes = start.getMinutes();  
            
            if((minutes%15) != 0){
                
                minutes = minutes - (minutes%15);            
            	start.setMinutes(minutes);
            	
            	selectedCase.End_of_Procedure_DateTime__c = start.toISOString();
                
                setInterval(function(){
                	component.set("v.case", selectedCase);                    
                }, 100);                
            }
        }        
    },
    checkMinutesEventStart : function(component, event, helper){
     
        var selectedEvent = component.get("v.event");
            
        if(selectedEvent.StartDateTime != null){            
            
            var start = new Date(selectedEvent.StartDateTime);
            var minutes = start.getMinutes();  
            
            if((minutes%15) != 0){
                
                minutes = minutes - (minutes%15);            
            	start.setMinutes(minutes);
            	
            	selectedEvent.StartDateTime = start.toISOString();
                
                setInterval(function(){
                	component.set("v.event", selectedEvent);                    
                }, 100);                
            }
        }        
    },
    checkMinutesEventEnd : function(component, event, helper){
     
        var selectedEvent = component.get("v.event");
            
        if(selectedEvent.EndDateTime != null){            
            
            var start = new Date(selectedEvent.EndDateTime);
            var minutes = start.getMinutes();  
            
            if((minutes%15) != 0){
                
                minutes = minutes - (minutes%15);            
            	start.setMinutes(minutes);
            	
            	selectedEvent.EndDateTime = start.toISOString();
                
                setInterval(function(){
                	component.set("v.event", selectedEvent);                    
                }, 100);                
            }
        }        
    },
    addNewAttendee : function(component, event, helper) {
    	
        var caseRecord = component.get("v.case");
        
        var newAttendee = {
            sobjectType : 'Activity_Scheduling_Attendee__c',
            Attendee__c : null
        };
        caseRecord.Activity_Scheduling_Attendees__r.push(newAttendee);
        
        component.set("v.case", caseRecord);
    },
    clearAttendees : function(component, event, helper) {
    	
        var caseRecord = component.get("v.case");
        
        caseRecord.Assigned_To__c = null;
        caseRecord.Activity_Scheduling_Attendees__r = [];
        
        component.set("v.case", caseRecord);
    },
    addFullTeam : function(component, event, helper) {
    	console.log('called')
        var caseRecord = component.get("v.case");
        
        var addedAttendees = {};
        var newAttendeeList = [];
        
        if(caseRecord.Assigned_To__c != null) addedAttendees[caseRecord.Assigned_To__c] = true;
        
        for(var i = 0; i < caseRecord.Activity_Scheduling_Attendees__r.length; i++){
            
            if(caseRecord.Activity_Scheduling_Attendees__r[i].Attendee__c != null ){
                
                addedAttendees[caseRecord.Activity_Scheduling_Attendees__r[i].Attendee__c] = true;
                newAttendeeList.push(caseRecord.Activity_Scheduling_Attendees__r[i]);
            }
        }
        
        var teamMembers = component.get("v.teamMembers");
        console.log(caseRecord);
        for(var i = 0; i < teamMembers.length; i++){
            
            if(addedAttendees[teamMembers[i].Id] != true){
				console.log(caseRecord.Assigned_To__c == null);
                console.log(caseRecord.Assigned_To__c == '');
                if(caseRecord.Assigned_To__c == null || caseRecord.Assigned_To__c == '') caseRecord.Assigned_To__c = teamMembers[i].Id;
                else{
                	var newAttendee = {
                    	sobjectType : 'Activity_Scheduling_Attendee__c',
                    	Attendee__c : teamMembers[i].Id
        			};
        			newAttendeeList.push(newAttendee);
                }
            }            
        }
            
        caseRecord.Activity_Scheduling_Attendees__r = newAttendeeList;
        
        component.set("v.case", caseRecord);
    }
})