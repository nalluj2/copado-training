({
	doInit : function(component, event, helper) {
		
        window.addEventListener("resize",function(e){
  			helper.resize(component, event, helper);
		});
        
        var role = component.get("v.role");
        
        var statusOptions = [
            { value: "", label: "All" },
            { value: "Open", label: "Open" },
            { value: "Completed", label: "Completed" }            
        ];
        
        if(role == 'Approval'){
            
            statusOptions.push({ value: "PendingApprover", label: "Pending my approval" });
            statusOptions.push({ value: "ApprovedMe", label: "Approved (by me)" });
            
        }else statusOptions.push({ value: "PendingAssigned", label: "Pending approval" });
        
        statusOptions.push({ value: "Approved", label: "Approved" });
        component.set("v.statusOptions", statusOptions);
        
        var segmentOptions = [
            { value: "", label: "All" },
            { value: "-", label: "Not Segmented" },
            { value: "XS", label: "XS" },
            { value: "S", label: "S" },
            { value: "M", label: "M" },
            { value: "L", label: "L" },
            { value: "XL", label: "XL" }
        ];
        component.set("v.segmentOptions", segmentOptions);
        
        component.set("v.sbuOptions", [{ value: "", label: "All" }]);        
        component.set("v.procedureOptions", [{ value: "", label: "All" }]);
        component.set("v.competitorOptions", [{ value: "Show", label: "Show" }, { value: "Hide", label: "Hide" }]);
        
        helper.resize(component, event, helper);
        
        helper.getPotentialInputs(component, event, helper);         
	},
    selectAll : function(component, event, helper) {
     	
        component.set("v.showLoading", true);
        
        setTimeout($A.getCallback(function(){
            
            var potInputs = component.get("v.potentialInputs");
            var selectedValue = component.get("v.selectedAll");
            
            for (var i = 0; i < potInputs.length; i++) {
                        
                if(potInputs[i].disabled != true) potInputs[i].selected = selectedValue;    		                
            }
            
            helper.selectRow(component);
            
            component.set("v.potentialInputs", potInputs);
            component.set("v.showLoading", false);
        }), 100);
    },
    selectRow : function(component, event, helper) {
     	        
        helper.selectRow(component);
    },
    forceShow : function(component, event, helper) {
        
        component.set("v.showLoading", true);
        
        setTimeout($A.getCallback(function(){
            
            var potInputs = component.get("v.allPotentialInputs");
            
            helper.applyFilters(component, potInputs, true);
                        
            component.set("v.showLoading", false);
            
        }), 100);
    },
    filterSelected : function(component, event, helper) {
        
        component.set("v.showLoading", true);
        
        setTimeout($A.getCallback(function(){
            
            var potInputs = component.get("v.allPotentialInputs");
            
            helper.applyFilters(component, potInputs, false);
                      
            component.set("v.showLoading", false);
            
        }), 100);
    },
    inputFilterChanged : function(component, event, helper) {
        
        var inputFilters = component.get("v.inputFilters");
            
        if(inputFilters != null){
        	
        	component.set("v.showLoading", true);
        	
        	setTimeout($A.getCallback(function(){
                    
                helper.applyInputFilters(component, inputFilters);
                            
                var potInputs = component.get("v.allPotentialInputs");
                
                helper.applyFilters(component, potInputs, false);
                         
                component.set("v.showLoading", false);        
        	}), 100);
        }        
    },
    changeSorting : function(component, event, helper) {
        
        component.set("v.showLoading", true);
        
        setTimeout($A.getCallback(function(){
            
            var sortBy = component.get("v.sortBy");            
            var sortDir = component.get("v.sortDir");
            var newSortBy = event.getSource().get("v.id");
            console.log(sortBy + ':' + sortDir + ':' + newSortBy);
            if(newSortBy == sortBy){
                
                if(sortDir == 'ASC') sortDir = 'DESC';
                else sortDir = 'ASC';
                
            }else{
                
                sortBy = newSortBy;
                sortDir = 'ASC';
            }
            
            component.set("v.sortBy", sortBy); 
            component.set("v.sortDir", sortDir); 
            
            var potInputs = component.get("v.potentialInputs");
            
            helper.applySorting(component, potInputs);
            
            component.set("v.potentialInputs", potInputs);            
            component.set("v.showLoading", false);
            
        }), 100);
    },
    sbuFilterSelected : function(component, event, helper) {
        
        component.set("v.showLoading", true);        
        component.set("v.filterProcedure", "");
        
        helper.applySBUFilter(component);
                
        setTimeout($A.getCallback(function(){
            
            var potInputs = component.get("v.allPotentialInputs");
            
            helper.applyFilters(component, potInputs, false);
            
            component.set("v.showLoading", false);
            
        }), 100);
    },
    markCompleted : function(component, event, helper){
        
        component.set("v.showLoading", true);
        
        setTimeout($A.getCallback(function(){
        
            var potInputs = component.get("v.potentialInputs");
            var hasChange = false;
            var numberEmpty = 0;
            
            for (var i = 0; i < potInputs.length; i++) {
                    
                var procedureInput = potInputs[i];
                
                if(procedureInput.selected == true && procedureInput.completed != true){
                    
                    procedureInput.record.Complete__c = true;
                    procedureInput.completed = true;
                    procedureInput.changed = true;
                    hasChange = true;
                    
                    if(isNaN(parseInt(procedureInput.record.Potential__c)) && procedureInput.record.Last_12_mth_Sales__c != null) numberEmpty++;
                }
                
                procedureInput.selected = false;
            }
            
            if(hasChange) component.set("v.hasChange", true);
            
            helper.selectRow(component);
            component.set("v.selectedAll", false);
            component.set("v.potentialInputs", potInputs);
            component.set("v.showLoading", false);
            
            if(numberEmpty > 0) alert('You have marked as completed ' + numberEmpty + ' Procedure' + (numberEmpty > 1 ? 's' : '') +' with Sales but no Potential provided.');
            
        }), 100);
    },
    reopen : function(component, event, helper){
        
        component.set("v.showLoading", true);
        
        setTimeout($A.getCallback(function(){
            
            var potInputs = component.get("v.potentialInputs");
            var hasChange = false;
            
            for (var i = 0; i < potInputs.length; i++) {
                
                var procedureInput = potInputs[i];
                
                if(procedureInput.selected == true && procedureInput.completed == true){
                    
                    procedureInput.record.Complete__c = false;
                    procedureInput.completed = false;
                    procedureInput.changed = true;
                    hasChange = true;
                }
                
                procedureInput.selected = false;
            }
            
            if(hasChange) component.set("v.hasChange", true);
            
            helper.selectRow(component);
            component.set("v.selectedAll", false);
            component.set("v.potentialInputs", potInputs);
            component.set("v.showLoading", false);
            
        }), 100);
    },
    changeStatus : function(component, event, helper){
        
        component.set("v.showLoading", true);
        
        setTimeout($A.getCallback(function(){
            
            var potInputs = component.get("v.potentialInputs");
            var newStatus = component.get("v.newStatus");
            var userId = $A.get("$SObjectType.CurrentUser.Id");
            var hasChange = false;
            var numberEmpty = 0;
            
            for (var i = 0; i < potInputs.length; i++) {
                
                if(potInputs[i].selected == true){
                    
                    if(newStatus == 'Open'){
                    	
                        if(potInputs[i].record.Complete__c == true){
                    
                            potInputs[i].record.Complete__c = false;
                            potInputs[i].record.Approved__c = false;
                            potInputs[i].record.Approved_2__c = false;                            
                            potInputs[i].changed = true;
                            potInputs[i].completed = false;
                            hasChange = true;
                        }
                    
                    }else if(newStatus == 'Complete'){
                        
                        if(potInputs[i].record.Complete__c != true || potInputs[i].record.Approved__c == true){
                    
                            potInputs[i].record.Complete__c = true;
                            potInputs[i].record.Approved__c = false;
                            potInputs[i].record.Approved_2__c = false;                            
                            potInputs[i].changed = true;
                            potInputs[i].completed = false;
                            hasChange = true;
                            
                            if(isNaN(parseInt(potInputs[i].record.Potential__c)) && potInputs[i].record.Last_12_mth_Sales__c != null) numberEmpty++;
                        }
                        
                    }else if(newStatus == 'Approved1'){
                        
                        if(potInputs[i].record.Approved__c != true || potInputs[i].record.Approved_2__c == true){
                    
                            potInputs[i].record.Complete__c = true;
                            potInputs[i].record.Approved__c = true;
                            potInputs[i].record.Approved_2__c = false;                            
                            potInputs[i].changed = true;
                            if(potInputs[i].record.Approver_2__c != userId) potInputs[i].completed = true;
                            else potInputs[i].completed = false;
                            hasChange = true;
                            
                            if(isNaN(parseInt(potInputs[i].record.Potential__c)) && potInputs[i].record.Last_12_mth_Sales__c != null) numberEmpty++;
                        }
                        
                    }else if(newStatus == 'Approved2'){
                        
                        if(potInputs[i].record.Approved_2__c != true){
                    
                            potInputs[i].record.Complete__c = true;
                            potInputs[i].record.Approved__c = true;
                            if(potInputs[i].record.Approver_2__c == userId) potInputs[i].record.Approved_2__c = true;
                            if(potInputs[i].record.Approver_2__c == userId || potInputs[i].record.Approver_2__c == null) potInputs[i].completed = true;                            
                            potInputs[i].changed = true;                           
                            hasChange = true;
                            
                            if(isNaN(parseInt(potInputs[i].record.Potential__c)) && potInputs[i].record.Last_12_mth_Sales__c != null) numberEmpty++;
                        }
                    }
                }
                
                potInputs[i].selected = false;
            }
            
            component.set("v.newStatus", null);
            if(hasChange) component.set("v.hasChange", true);
            
            component.set("v.selectedAll", false);
            component.set("v.potentialInputs", potInputs);
            component.set("v.showLoading", false);
            helper.selectRow(component);
            
            if(numberEmpty > 0) alert('You have marked as completed or approved' + numberEmpty + ' Procedure' + (numberEmpty > 1 ? 's' : '') +' with Sales but no Potential provided.');
            
        }), 100);
    },          
    copyLY : function(component, event, helper){
		
        component.set("v.showLoading", true);
        
        setTimeout($A.getCallback(function(){
            
            var potInputs = component.get("v.potentialInputs");
            var hasChange = false;
            
            for (var i = 0; i < potInputs.length; i++) {
                  
                var procedureInput = potInputs[i];
                
                if(procedureInput.selected == true && procedureInput.completed != true){
                    
                    if(procedureInput.competitors.length > 0){
                        
                        var procedureTotal = 0;
                        
                        if(procedureInput.prevRecord != null && procedureInput.prevRecord.Potential_Medtronic__c != null && procedureInput.prevRecord.Potential_Medtronic__c != procedureInput.record.Medtronic_Potential__c){
                            
                            procedureInput.record.Potential_Medtronic__c = procedureInput.prevRecord.Potential_Medtronic__c;
                            procedureInput.changed = true;
                            hasChange = true;                            
                        }
                        
                        if(!isNaN(parseInt(procedureInput.record.Potential_Medtronic__c))) procedureTotal += parseInt(procedureInput.record.Potential_Medtronic__c);
                        
                        for(var k = 0; k < procedureInput.competitors.length; k++){
                            
                            var procCompetitor = procedureInput.competitors[k];
                            
                            if(procCompetitor.prevRecord != null && procCompetitor.prevRecord.Potential__c != null && procCompetitor.prevRecord.Potential__c != procCompetitor.record.Potential__c){
                                
                                procCompetitor.record.Potential__c = procCompetitor.prevRecord.Potential__c;
                                procCompetitor.changed = true;
                                hasChange = true;
                            }
                            
                            if(!isNaN(parseInt(procCompetitor.record.Potential__c))) procedureTotal += parseInt(procCompetitor.record.Potential__c);
                        }
                        
                        if(procedureTotal > 0) procedureInput.record.Potential__c = procedureTotal;
                        else procedureInput.record.Potential__c = null;
                        
                    }else{
                        
                        if(procedureInput.prevRecord != null && procedureInput.prevRecord.Potential__c != null && procedureInput.prevRecord.Potential__c != procedureInput.record.Medtronic_Potential__c){
                            
                            procedureInput.record.Potential__c = procedureInput.prevRecord.Potential__c;
                            procedureInput.changed = true;
                            hasChange = true;                            
                        }
                    }
                    
                    if(!isNaN(parseInt(procedureInput.record.Potential__c)) && parseInt(procedureInput.record.Potential__c) > 0 &&  procedureInput.record.Last_12_mth_Sales__c != null){
                        
                        var percentage = (procedureInput.record.Last_12_mth_Sales__c / procedureInput.record.Potential__c) * 100;
                        procedureInput.marketShare = (Math.round(percentage * 100) / 100);
                        
                    }else{
                        
                        procedureInput.marketShare = null;
                    }                    
                }
                
                procedureInput.selected = false;
            }
            
            if(hasChange) component.set("v.hasChange", true);
            
            helper.selectRow(component);
            component.set("v.selectedAll", false);
            component.set("v.potentialInputs", potInputs);
            component.set("v.showLoading", false);
        }), 100);
    },
    cancel : function(component, event, helper){
        
        helper.getPotentialInputs(component, event, helper);        
    },
    saveInputs : function(component, event, helper){
                
        helper.saveInputs(component);
    }
})