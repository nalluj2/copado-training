({
	resize : function (component, event, helper){
  		
        if(component.isValid()){
        
            var timer = component.get('v.timer');
            clearTimeout(timer);
        
            var timer = setTimeout(function(){
                                
                clearTimeout(timer);
                component.set('v.timer', null);
                
                helper.setSizes(component);
                
            }, 500);
    
            component.set('v.timer', timer);      
        }
    },
    setSizes : function(component){
        
        var containerHeight = component.find("mainContainer").getElement().getBoundingClientRect().height;
        
        var tableHeight = containerHeight - 165;
        
        component.set("v.tableMaxHeight", tableHeight);       
    },
    getPotentialInputs : function (component, event, helper){
    	
        var action = component.get("c.getUserPotentialInputs");
        var role = component.get("v.role");
        
        component.set("v.showLoading", true);
        action.setParams({role : role});
        
        action.setCallback(this, function(response) {
    	
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var potInputs = response.getReturnValue();
                var accountOptions = {'list' : []};
                var sbus = {'list' : []};
                var procedures = {'list' : []};
                var assignedTos = {'list' : []};
                
                var userId = $A.get("$SObjectType.CurrentUser.Id");
                
                for (var i = 0; i < potInputs.length; i++) {
    				
                    var procedureInput = potInputs[i];
                    
                    if(! accountOptions.hasOwnProperty(procedureInput.account.accountId)){
                    
                    	var accountOption = {"label" : procedureInput.account.accountName, "value" : procedureInput.account.accountId, "city" : procedureInput.account.accountCity, "SAP_Id" : procedureInput.account.accountSAPId};
                    	accountOptions.list.push(accountOption);
                        accountOptions[procedureInput.account.accountId] = accountOptions;                                            
                    }
                    
                    if(! assignedTos.hasOwnProperty(procedureInput.record.Assigned_To__c)){
                    
                    	var assignedTo = {"label" : procedureInput.record.Assigned_To__r.Name, "value" : procedureInput.record.Assigned_To__c};
                    	assignedTos.list.push(assignedTo);
                        assignedTos[procedureInput.record.Assigned_To__c] = assignedTo;                                            
                    }
                    
                    if(! sbus.hasOwnProperty(procedureInput.record.Segmentation_Potential_Procedure__r.Sub_Business_Unit__c)){
                        
                        var sbuOption = {"label" : procedureInput.record.Segmentation_Potential_Procedure__r.Sub_Business_Unit__c, "value" : procedureInput.record.Segmentation_Potential_Procedure__r.Sub_Business_Unit__c};
                        sbus.list.push(sbuOption);
                        sbus[procedureInput.record.Segmentation_Potential_Procedure__r.Sub_Business_Unit__c] = sbuOption;
                    }
                    
                    if(! procedures.hasOwnProperty(procedureInput.record.Segmentation_Potential_Procedure__c)){
                        
                        var procedureOption = {"label" : procedureInput.record.Segmentation_Potential_Procedure__r.Name, "value" : procedureInput.record.Segmentation_Potential_Procedure__c, "sbu" : procedureInput.record.Segmentation_Potential_Procedure__r.Sub_Business_Unit__c};
                        procedures.list.push(procedureOption);
                        procedures[procedureInput.record.Segmentation_Potential_Procedure__c] = procedureOption;
                    }
                        
                    if(!isNaN(parseInt(procedureInput.record.Potential__c)) && parseInt(procedureInput.record.Potential__c) > 0 &&  procedureInput.record.Last_12_mth_Sales__c != null){
                        
                        var percentage = (procedureInput.record.Last_12_mth_Sales__c / procedureInput.record.Potential__c) * 100;
                        procedureInput.marketShare = (Math.round(percentage * 100) / 100);
                        
                    }else{
                        
                        procedureInput.marketShare = null;
                    }
                    
                    if(role == "Input"){
                        
                        if(procedureInput.record.Approved__c == true) procedureInput.disabled = true;                        
                        if(procedureInput.record.Complete__c == true) procedureInput.completed = true;
                        
                    }else{
                        
                        if(procedureInput.record.Approver_1__c == userId && procedureInput.record.Approved_2__c == true) procedureInput.disabled = true;
                        if((procedureInput.record.Approver_1__c == userId && procedureInput.record.Approved__c == true) || (procedureInput.record.Approver_2__c == userId && procedureInput.record.Approved_2__c == true)) procedureInput.completed = true;
                    }
                }
				
                //Account
				accountOptions.list.sort(function(a, b){return a.label.toLowerCase().localeCompare(b.label.toLowerCase());});                
                component.set("v.accountOptions", accountOptions.list);
                
                //Assigned To
				assignedTos.list.sort(function(a, b){return a.label.toLowerCase().localeCompare(b.label.toLowerCase());});
                assignedTos.list.unshift({"label" : "All", "value" : ""});
                component.set("v.assignedToOptions", assignedTos.list);
                
                //SBU
                sbus.list.sort(function(a, b){return a.label.toLowerCase().localeCompare(b.label.toLowerCase());});
                
                if(sbus.list.length > 1) sbus.list.unshift({"label" : "All", "value" : ""});
                else component.set("v.filterSBU", sbus.list[0].value);
                component.set("v.sbuOptions", sbus.list);
                
                //Procedure
                procedures.list.sort(function(a, b){return a.label.toLowerCase().localeCompare(b.label.toLowerCase());});
                component.set("v.allProcedureOptions", procedures.list); 
                
                if(procedures.list.length > 1) procedures.list.unshift({"label" : "All", "value" : ""});                
                else component.set("v.filterProcedure", procedures.list[0].value);
                
                component.set("v.procedureOptions", procedures.list);                     
                
				//Input Filters                
                var inputFilters = component.get("v.inputFilters");
        		if(inputFilters != null) helper.applyInputFilters(component, inputFilters);                
                
                helper.applyFilters(component, potInputs, false);
                                
                component.set("v.allPotentialInputs", potInputs);                
                component.set("v.hasChange", false);
                
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    
                    var showError = $A.get("e.c:Show_Error_Toast");
                    showError.setParams({ "message" : errors[0].message });
                    showError.fire();                        
                    
                } else {
                    alert("Unknown error");
                }
            }
            
            component.set("v.showLoading", false);            
        });
        
		$A.getCallback(function() {
        	$A.enqueueAction(action);
    	})();    		
    },
    applyFilters : function (component, potInputs, forceDisplay){
        
        var procedureFilter = component.get("v.filterProcedure");
        var sbuFilter = component.get("v.filterSBU");
        var statusFilter = component.get("v.filterStatus");
        var accountFilter = component.get("v.filterAccount");
        var segmentFilter = component.get("v.filterSegment");
        var assignedToFilter = component.get("v.filterAssignedTo");
        
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        
        var hasCompetitors = false;
        var hasSales = false;
        var hasRevenue = false;
        var hasUnits = false;
        var revenueCurrencies = {};
       
        var displayAccInputs = [];
        
        for (var i = 0; i < potInputs.length; i++) {
            
            var showProcedure = true;
                    
            if(sbuFilter != null && sbuFilter.length > 0 && potInputs[i].record.Segmentation_Potential_Procedure__r.Sub_Business_Unit__c != sbuFilter) showProcedure = false;
            if(procedureFilter != null && procedureFilter.length > 0 && potInputs[i].record.Segmentation_Potential_Procedure__c != procedureFilter) showProcedure = false;
            if(accountFilter != null && accountFilter.length > 0 && potInputs[i].account.accountId != accountFilter) showProcedure = false;
            if(assignedToFilter != null && assignedToFilter.length > 0 && potInputs[i].record.Assigned_To__c != assignedToFilter) showProcedure = false;
            if(segmentFilter != null && segmentFilter.length > 0 && potInputs[i].account.accountSegment != segmentFilter) showProcedure = false;
            if(statusFilter != null && statusFilter.length > 0){
                        
                if(statusFilter == 'Open' && potInputs[i].record.Complete__c == true) showProcedure = false;
                else if(statusFilter == 'Completed' && potInputs[i].record.Complete__c != true) showProcedure = false;
                else if(statusFilter == 'Approved' && (potInputs[i].record.Complete__c != true || (potInputs[i].record.Approver_1__c != null && potInputs[i].record.Approved__c != true) || (potInputs[i].record.Approver_2__c != null && potInputs[i].record.Approved_2__c != true)) ) showProcedure = false;
                else if(statusFilter == 'PendingApprover' && (!((potInputs[i].record.Complete__c == true && potInputs[i].record.Approver_1__c == userId && potInputs[i].record.Approved__c != true) || (potInputs[i].record.Approved__c == true && potInputs[i].record.Approver_2__c == userId && potInputs[i].record.Approved_2__c != true)) )) showProcedure = false;
                else if(statusFilter == 'PendingAssigned' && (potInputs[i].record.Complete__c != true || potInputs[i].record.Approver_1__c == null || (potInputs[i].record.Approved__c == true && (potInputs[i].record.Approver_2__c == null || potInputs[i].record.Approved_2__c == true)))) showProcedure = false;
                else if(statusFilter == 'ApprovedMe' && (!((potInputs[i].record.Approver_1__c == userId && potInputs[i].record.Approved__c == true) || (potInputs[i].record.Approver_2__c == userId && potInputs[i].record.Approved_2__c == true)) )) showProcedure = false;
            }
                   
            if(showProcedure){
                
                displayAccInputs.push(potInputs[i]);
                
                if(potInputs[i].competitors.length > 0) hasCompetitors = true;
                if(potInputs[i].record.Last_12_mth_Sales__c != null) hasSales = true;
                if(potInputs[i].record.Segmentation_Potential_Procedure__r.Registration_Unit__c == 'REVENUE'){
                    
                    hasRevenue = true; 
                    revenueCurrencies[potInputs[i].record.CurrencyIsoCode] = potInputs[i].record.CurrencyIsoCode;
                    
                }else if(potInputs[i].record.Segmentation_Potential_Procedure__r.Registration_Unit__c == 'UNITS') hasUnits = true; 
                
            }else{
                        
                potInputs[i].selected = false;
            }
            
        }
        
        var sortOnPotential = false;
        
        if(hasUnits = true && hasRevenue == false) sortOnPotential = true;        
        else if(hasUnits == false && hasRevenue == true && Object.keys(revenueCurrencies).length == 1) sortOnPotential = true;
        
        component.set("v.displayInputsCount", displayAccInputs.length);
        component.set("v.hasCompetitors" , hasCompetitors);
        component.set("v.hasSales" , hasSales);
        component.set("v.sortOnPotential" , sortOnPotential);
        component.set("v.selectedAll", false);
        
        if(displayAccInputs.length < 500 || forceDisplay == true){
        
        	this.applySorting(component, displayAccInputs);            
            component.set("v.potentialInputs", displayAccInputs);
            
        }else{
            
            component.set("v.potentialInputs", null);
        }  
    },
    applySorting : function (component, potInputs){
        
        var sortBy = component.get("v.sortBy");
        var sortDir = component.get("v.sortDir");
        
        var hasSales = component.get("v.hasSales");
        var sortOnPotential = component.get("v.sortOnPotential");
        var filterProcedure = component.get("v.filterProcedure");
        
        if((hasSales == false && sortBy == 'MarketShare') || ((sortOnPotential == false || filterProcedure == null || filterProcedure.length == 0) && (sortBy == 'LastSales' || sortBy == 'LYPotential' || sortBy == 'NYPotential'))){
            
            sortBy = 'AccountName';
            sortDir = 'ASC'
            
            component.set("v.sortBy", sortBy);
            component.set("v.sortDir", sortDir);
        }
        
        var segmentMap = {"-" : 0, "XS" : 1, "S" : 2, "M" : 3, "L" : 4, "XL" : 5};
        
        potInputs.sort(function(a, b){
                       
            var result;
            
            if(sortBy == "AccountName"){
            	
                result = a.account.accountName.toLowerCase().localeCompare(b.account.accountName.toLowerCase());
                
                if(sortDir == 'DESC') result = 0 - result;
            
            }else if(sortBy == "AccountSegment"){
            	
                if(a.account.accountSegment == "-" && b.account.accountSegment != "-") result = 1;
                else if(a.account.accountSegment != "-" && b.account.accountSegment == "-") result = -1;
                else if(a.account.accountSegment == "-" && b.account.accountSegment == "-") result = 0;
                else{
                	result = segmentMap[a.account.accountSegment] - segmentMap[b.account.accountSegment];
                    if(sortDir == 'DESC') result = 0 - result;
                }
                
            }else if(sortBy == "MarketShare"){
            	
                if(a.marketShare == null && b.marketShare != null) result = 1;
                else if(a.marketShare != null && b.marketShare == null) result = -1;
                else if(a.marketShare == null && b.marketShare == null) result = 0;
                else{
                    result = a.marketShare - b.marketShare;                
                	if(sortDir == 'DESC') result = 0 - result;
                }
                
            }else if(sortBy == "LastSales"){
            	
                if(a.record.Last_12_mth_Sales__c == null && b.record.Last_12_mth_Sales__c != null) result = 1;
                else if(a.record.Last_12_mth_Sales__c != null && b.record.Last_12_mth_Sales__c == null) result = -1;
                else if(a.record.Last_12_mth_Sales__c == null && b.record.Last_12_mth_Sales__c == null) result = 0;
                else{
                    result = a.record.Last_12_mth_Sales__c - b.record.Last_12_mth_Sales__c;                
                	if(sortDir == 'DESC') result = 0 - result;
                }
                
            }else if(sortBy == "LYPotential"){
            	
                if((a.prevRecord == null || a.prevRecord.Potential__c == null) && b.prevRecord != null && b.prevRecord.Potential__c != null) result = 1;
                else if(a.prevRecord != null && a.prevRecord.Potential__c != null && (b.prevRecord == null || b.prevRecord.Potential__c == null)) result = -1;
                else if((a.prevRecord == null || a.prevRecord.Potential__c == null) && (b.prevRecord == null || b.prevRecord.Potential__c == null)) result = 0;
                else{
                    result = a.prevRecord.Potential__c - b.prevRecord.Potential__c;                
                	if(sortDir == 'DESC') result = 0 - result;
                }
                                
            }else if(sortBy == "NYPotential"){
            	
                if(a.record.Potential__c == null && b.record.Potential__c != null) result = 1;
                else if(a.record.Potential__c != null && b.record.Potential__c == null) result = -1;
                else if(a.record.Potential__c == null && b.record.Potential__c == null) result = 0;
                else{
                    result = a.record.Potential__c - b.record.Potential__c;                
                	if(sortDir == 'DESC') result = 0 - result;
                }
                                
            }else if(sortBy == "Status"){
            	
                if(a.record.Complete__c == false){
                    
                    if(b.record.Complete__c == true) result = -1;
                    else{
                        
                        if((a.record.Approver_1__c == null && b.record.Approver_1__c != null) || (a.record.Approver_2__c == null && b.record.Approver_2__c != null)) result = -1;
                        else if((a.record.Approver_1__c != null && b.record.Approver_1__c == null) || (a.record.Approver_2__c != null && b.record.Approver_2__c == null)) result = 1;
                        else result = 0;                        
                    }
                    
                }else if(a.record.Approver_1__c == null){
                    
                    if(b.record.Complete__c == false) result = 1;
                    else{
                        
                        if((b.record.Approver_1__c != null && b.record.Approved__c == false) || (b.record.Approver_2__c != null && b.record.Approved_2__c == false)) result = 1;
                        else if(b.record.Approver_1__c != null) result = -1;
                    	else result = 0;
                    }
                    
                }else if(a.record.Approved__c == false){
                    
                    if(b.record.Complete__c == false) result = 1;
                    else{
                        
                        if(b.record.Approver_1__c == null || b.record.Approved__c == true) result = -1;   
                        else if(a.record.Approver_2__c == null){
                            
                            if(b.record.Approver_2__c != null) result = -1;
                            else result = 0;
                            
                        }else{
                        	
                            if(b.record.Approver_2__c == null) result = 1;
                            else result = 0;
                        }                    	
                    }                    
                    
                }else if(a.record.Approver_2__c == null){
                    
                    if(b.record.Complete__c == false) result = 1;
                    else{
                        
                        if(b.record.Approved__c == false || (b.record.Approver_2__c != null && b.record.Approved_2__c == false)) result = 1;
                        else if(b.record.Approver_2__c != null) result = -1;
                    	else result = 0;
                    }
                    
                }else if(a.record.Approved_2__c == false){
                    
                    if(b.record.Complete__c == false || (b.record.Approver_1__c != null && b.record.Approved__c == false)) result = 1;
                    else{
                        
                        if(b.record.Approver_1__c == null || b.record.Approver_2__c == null || a.record.Approved_2__c == true) result = -1                        
                    	else result = 0;
                    }                 
                    
                }else{
                    
                    if(b.record.Complete__c == false || b.record.Approved__c == false || b.record.Approved_2__c == false) result = 1;
                    else result = 0; 
                }
                
                if(sortDir == 'DESC') result = 0 - result;
            }
            
            if(result == 0) result = a.account.accountName.toLowerCase().localeCompare(b.account.accountName.toLowerCase());
            if(result == 0) result = a.account.accountId.localeCompare(b.account.accountId);
            if(result == 0) result = a.record.Segmentation_Potential_Procedure__r.Name.localeCompare(b.record.Segmentation_Potential_Procedure__r.Name);                
            
            return result;        
        });  
                
        var procedures = 0;
        
        for (var i = 0; i < potInputs.length; i++) {
        	
            potInputs[i].oddRow = ((procedures % 2) == 1);
            potInputs[i].index = procedures;
            procedures++;
        }
        
        return potInputs;        
    },
    saveInputs : function(component) {
		
        var allPotInputs = component.get("v.allPotentialInputs");
		
        var changed = [];
        
        for(var i = 0; i < allPotInputs.length; i++){
        	
            if(allPotInputs[i].changed == true) changed.push(allPotInputs[i]);
            else{
                
                var hasChange = false;
                
                for(var j = 0; j < allPotInputs[i].competitors.length; j++){
                    
                    if(allPotInputs[i].competitors[j].changed == true) hasChange = true;
                }
                
                if(hasChange == true) changed.push(allPotInputs[i]);
            }
        }        
        
        var action = component.get("c.savePotentials");
        action.setParams({potentials : changed});
        
        component.set("v.showLoading", true);
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                                
                var newInputs = response.getReturnValue();
                
                for (var i = 0; i < changed.length; i++) {
                        
                    changed[i].record.Id = newInputs[i].record.Id;
                    changed[i].completedTS = newInputs[i].completedTS;
                    changed[i].approved1TS = newInputs[i].approved1TS;
                    changed[i].approved2TS = newInputs[i].approved2TS;                    
                    changed[i].potentialTS = newInputs[i].potentialTS;
                    changed[i].potentialMDTTS = newInputs[i].potentialMDTTS;
                    changed[i].changed = false;
                    
                    for(var j = 0; j < changed[i].competitors.length; j++){
                        
                        changed[i].competitors[j].record.Id = newInputs[i].competitors[j].record.Id;
                        changed[i].competitors[j].potentialTS = newInputs[i].competitors[j].potentialTS;
                        changed[i].competitors[j].changed = false;
                    }
                }
                
                component.set("v.allPotentialInputs", allPotInputs);
                component.set("v.hasChange", false);
				
				this.applyFilters(component, allPotInputs, true);
                
                var refreshEvent = $A.get("e.c:Segmentation_Overview_Refresh");
        		refreshEvent.setParams({ "data_type" : "Potential" });
        		refreshEvent.fire();
                
            }else if (state === "ERROR") {
                
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    
                    var showError = $A.get("e.c:Show_Error_Toast");
                    showError.setParams({ "message" : errors[0].message });
                    showError.fire();                        
                    
                } else {
                    alert("Unknown error");
                }
            }
            
            component.set("v.showLoading", false);
        });
        
		$A.getCallback(function() {
        	$A.enqueueAction(action);
    	})();
	},
    applySBUFilter : function(component){
    	
        var filterSBU = component.get("v.filterSBU");
        var allProcedureOptions = component.get("v.allProcedureOptions");
        
        var procedureOptions = [];
        
        if(filterSBU != null && filterSBU.length > 0){
            
            for(var i = 0; i < allProcedureOptions.length; i++){
                
                if(allProcedureOptions[i].sbu == filterSBU) procedureOptions.push(allProcedureOptions[i]);
            }
            
        }else{
            
            procedureOptions =  allProcedureOptions;
        }
        
        if(procedureOptions.length > 1) procedureOptions.unshift({"label" : "All", "value" : ""});                
        else component.set("v.filterProcedure", procedureOptions[0].value);
        
        component.set("v.procedureOptions", procedureOptions);        
    },
    applyInputFilters : function(component, filters) {
    	
        component.set("v.filterAccount", null);        
    	component.set("v.filterStatus", null);
        component.set("v.filterCompetitor", "Show");
        
        var filterList = filters.split(":");
        
        component.set("v.filterSBU", filterList[0]);
        
    	if(filterList.length > 1) component.set("v.filterProcedure", filterList[1]);
        else component.set("v.filterProcedure", null);
        
        if(filterList.length > 2){
            
            var segmentation = filterList[2];
            if(segmentation == 'Not Segmented') segmentation = '-';
            
            component.set("v.filterSegment", segmentation);
            
        }else component.set("v.filterSegment", null);
        
        if(filterList.length > 3) component.set("v.filterAssignedTo", filterList[3].split(';')[0]);
        else component.set("v.filterAssignedTo", null);
                
        this.applySBUFilter(component);
    },
    selectRow : function(component){
        
        var potInputs = component.get("v.potentialInputs");
        var role = component.get("v.role");
		var hasSelection = false;        
        var hasOpen = false;        
        var hasCompleted = false;        
        var hasLevel1 = false;
        var hasLevel2 = false;
        
        var userId = $A.get( "$SObjectType.CurrentUser.Id" );
        
        for (var i = 0; i < potInputs.length; i++) {
            
            if(potInputs[i].selected == true){
                
                hasSelection = true;
                if(potInputs[i].record.Approver_1__c == userId) hasLevel1 = true;
                if(potInputs[i].record.Approver_2__c == userId){
                    hasLevel1 = true;
                    hasLevel2 = true;
                }
                if(potInputs[i].completed != true) hasOpen = true;
                else hasCompleted = true;
            }    		
        }
		
        if(role == 'Approval'){
        
            var newStatusOptions = [                
                { value: "Open", label: "Open" },
                { value: "Complete", label: "Complete" }            
            ];
            
            if(hasLevel1 == true) newStatusOptions.push({value: "Approved1", label: "Approved level 1"});
            if(hasLevel2 == true) newStatusOptions.push({value: "Approved2", label: "Approved level 2"});
            
            component.set("v.newStatusOptions", newStatusOptions);        
        }
        
		component.set("v.hasSelection", hasSelection);
        component.set("v.hasOpen", hasOpen);
        component.set("v.hasCompleted", hasCompleted);        
    }
})