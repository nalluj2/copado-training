({
	doInit : function(component, event, helper) {
		
        window.addEventListener("resize",function(e){
  			helper.resize(component, event, helper);
		});
        
        helper.getCalendarData(component, event, helper); 
	},    
    nextWeek : function(component, event, helper) {
		
        var pickDay = new Date(component.get("v.pickDay"));        
        pickDay.setDate(pickDay.getDate() + 8);
        component.set("v.pickDay", pickDay.toISOString().split('T')[0]);
        helper.getCalendarData(component, event, helper);
	},
    previousWeek : function(component, event, helper) {
		
        var pickDay = new Date(component.get("v.pickDay"));        
        pickDay.setDate(pickDay.getDate() - 6);        
        component.set("v.pickDay", pickDay.toISOString().split('T')[0]);   
        helper.getCalendarData(component, event, helper);
	},          
    refresh : function(component, event, helper){
        
        if(component.get("v.teamId") != null) helper.getCalendarData(component, event, helper);
    },
    openActivitySelector : function(component, event, helper){  
    	        
        var inputParam = event.currentTarget.id;
        
        var memberId = inputParam.split("/")[0];
        var fullDate = inputParam.split("/")[1];
        var dayOffset = fullDate.split("-")[0];
        var time = fullDate.split("-")[1];        
        var hour = time.split(":")[0];
        var minute = time.split(":")[1];
        
        var pickDay = new Date(component.get("v.pickDay"));   
        pickDay.setDate(pickDay.getDate() + parseInt(dayOffset));
        var selectedDate = new Date(pickDay.getFullYear(), pickDay.getMonth(), pickDay.getDate(), hour, minute, 0, 0);
        
        component.set("v.selectedDate", selectedDate);
        component.set("v.selectedMember", memberId);
        
        component.set("v.posX", event.clientX - 100);
        component.set("v.posY",event.clientY - 60);
        
        var buttonMenu = component.find("activitySelector");                
        buttonMenu.set("v.visible", true);
    },    
    newActivitySelected : function(component, event, helper){  
    
    	var selectedActivity = event.getParam("value");
        var selectedDate = component.get("v.selectedDate");
        var selectedMember = component.get("v.selectedMember");
        if(selectedMember == 'unassigned') selectedMember = null;
        
        var appEvent = $A.get("e.c:New_Activity_Selector");
        appEvent.setParams({selectedDate : selectedDate, selectedMember : selectedMember, activityType : selectedActivity});
		appEvent.fire();
    },
    applyHighlight : function(component, event, helper){
        
        helper.applyHighlight(component);
    },
    openHighlight : function(component, event, helper){
        
        component.set("v.showHighLightPanel", true);
    },
    openDay : function(component, event, helper){
        
        var selectedDay = event.currentTarget.dataset.value.split(":");
        var day = selectedDay[0];
        var month = selectedDay[1];
        var year = selectedDay[2];
        
        var selectedDate = new Date(year, month - 1, day, 12, 0, 0, 0);
        
        var appEvent = $A.get("e.c:OpenDay");
        appEvent.setParams({selectedDate : selectedDate});
		appEvent.fire();
    }    
})