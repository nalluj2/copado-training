({	
    resize : function (component, event, helper){
  		
        if(component.isValid()){
        
            var timer = component.get('v.timer');
            clearTimeout(timer);
        
            var timer = setTimeout(function(){
                            
                helper.getCalendarData (component, event);
                
                clearTimeout(timer);
                component.set('v.timer', null);
                
            }, 500);
    
            component.set('v.timer', timer);      
        }
    }, getCalendarData : function(component, event, helper) {
        
        component.set("v.showLoading", true);
        
        var teamId = component.get("v.teamId");
        if(teamId == 'list' || teamId == 'personal') return;
        var sampleDate = component.get("v.pickDay");
        
        var width = document.documentElement.clientWidth - 220;
        
        var action = component.get("c.getWeek");
        action.setParams({ teamId : teamId, sampleDate : sampleDate, width : width });
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var calendarView = response.getReturnValue();    
                
                var pickDay = new Date(calendarView.firstDay)
                
 				component.set("v.pickDay", pickDay.toISOString().split('T')[0]); 
                component.set("v.hourWidth", calendarView.hourWidth);
                component.set("v.hours", calendarView.hours);
                component.set("v.events", calendarView.events);
                component.set("v.days", calendarView.days);  
                component.set("v.teamMembers", calendarView.members);
                component.set("v.mainLabel", calendarView.headerLabel);                
                                                
                var maxHeight = document.documentElement.clientHeight - 150;
                component.set("v.calendarMaxHeight", maxHeight)    ;
                
                component.set("v.showLoading", false);
                
                var isHighlighting = component.get("v.isHighlighting");
                if(isHighlighting == true) helper.applyHighlight(component);
                
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
		$A.getCallback(function() {            
        	$A.enqueueAction(action);
    	})();
	},
    applyHighlight : function(component){
        
        var isHighlighting = false;
        
        var eventTypes = component.get("v.highLightEventType");
        var account = component.get("v.highLightAccount");
        var procedures = component.get("v.highLightProcedure");
        var activityTypes = component.get("v.highLightActivityType");
        
        if(eventTypes != null && eventTypes.length > 0) isHighlighting = true;
        if(account != null && account.Id != null && account.Id != '') isHighlighting = true;
        if(procedures != null && procedures.length > 0) isHighlighting = true;
        if(activityTypes != null && activityTypes.length > 0) isHighlighting = true;
                
        var events = component.get("v.events");
        for(var key in events){
            
            var eventList = events[key];
            
            for (var i = 0; i < eventList.length; i++){
                
                var event = eventList[i];
                event.hide = false;
                
                if(eventTypes != null && eventTypes.length > 0 && eventTypes.indexOf(event.caseType) < 0) event.hide = true;
                if(account != null && account.Id != null && account.Id != '' && event.caseType != 'event' && event.accountId != account.Id) event.hide = true;
                if(procedures != null && procedures.length > 0 && event.caseType == 'implant' && procedures.indexOf(event.procedure) < 0) event.hide = true;
                if(activityTypes != null && activityTypes.length > 0 && event.caseType == 'service' && activityTypes.indexOf(event.activityType) < 0) event.hide = true;
            }
        }
        
        component.set("v.events", events);        
        component.set("v.showHighLightPanel", false);
        component.set("v.isHighlighting", isHighlighting);
    }
})