({
	searchForProducts : function(component) {
		return new Promise($A.getCallback(function (resolve, reject) {        
            
            var action = component.get("c.searchProducts");        
            action.setParams({searchText : component.get("v.productSearch")});
             
            action.setCallback(this, function(response) {
                               
                var state = response.getState();            
                if(state === "SUCCESS") {
                    
                    var products = response.getReturnValue();
                    
                    if(products.length == 201){
                        
                        products.pop();
                        component.set("v.productSearchError", "Your search returned too many results. Please, be more specific.");
                    }
                    
                    for(var i = 0; i < products.length; i++){
                        
                        var product = products[i];
                        product.Business_Unit__c = product.Business_Unit_ID__r != null ? product.Business_Unit_ID__r.Name : '';
                        product.Business_Unit_Group_Name = product.Business_Unit_Group__r != null ? product.Business_Unit_Group__r.Name : '';
                    }
                    
                    if(products.length > 0) component.set("v.noProductResults", false);
                   	else component.set("v.noProductResults", true);
                    
                    component.set("v.products", products);
                    
                } else {
                    
                    alert('Problem searching Products, response state: ' + state);
                }
                
                component.set("v.loading", false);            
            });
                    
            $A.enqueueAction(action);
        
        }));
    },
    searchForAccount : function(component) {
		
		return new Promise($A.getCallback(function (resolve, reject) {          
            
            var action = component.get("c.searchAccounts");
            action.setParams({searchText: component.get("v.accountSearch")});
             
            action.setCallback(this, function(response) {
                
                var state = response.getState();            
                if(state === "SUCCESS") {
					
                    var accounts = response.getReturnValue();
                    
                    if(accounts.length == 201){
                        
                        accounts.pop();
                        component.set("v.accountSearchError", "Your search returned too many results. Please, be more specific.");
                    }
                    
                    if(accounts.length > 0) component.set("v.noAccountResults", false);
                   	else component.set("v.noAccountResults", true);
                    
                    component.set("v.accounts", accounts);
                    
                } else {
                    
                    alert('Problem searching Accounts, response state: ' + state);                    
                }
                
                component.set("v.loading", false); 
            });
            
            $A.enqueueAction(action);
        }));
    },
    getProductDetails : function(component, helper, productId) {
		return new Promise($A.getCallback(function (resolve, reject) {        
            
            var action = component.get("c.getProduct");        
            action.setParams({productId : productId});
             
            action.setCallback(this, function(response) {
                               
                var state = response.getState();            
                if(state === "SUCCESS") {
                    
                    var selectedProduct = response.getReturnValue();
                    selectedProduct.Business_Unit__c = selectedProduct.Business_Unit_ID__r != null ? selectedProduct.Business_Unit_ID__r.Name : '';
                    selectedProduct.Business_Unit_Group_Name = selectedProduct.Business_Unit_Group__r != null ? selectedProduct.Business_Unit_Group__r.Name : '';
                    component.set("v.selectedProduct", selectedProduct);  
                    
                    var selectedAccount = component.get("v.selectedAccount");
                    
                    if(selectedAccount != null) helper.getSalesReps(component, selectedAccount, selectedProduct);
                    else component.set("v.loading", false);
                    
                } else {
                    
                    alert('Problem getting Product details, response state: ' + state);
                    component.set("v.loading", false);
                }
            });
                    
            $A.enqueueAction(action);
        
        }));
    },
    getAccountDetails : function(component, helper, accountId) {
		return new Promise($A.getCallback(function (resolve, reject) {        
           
            var action = component.get("c.getAccount");        
            action.setParams({accountId : accountId});
             
            action.setCallback(this, function(response) {
                               
                var state = response.getState();            
                if(state === "SUCCESS") {
                                     
                    var selectedAccount = response.getReturnValue();
                    component.set("v.selectedAccount", selectedAccount);
                    
                    var selectedProduct = component.get("v.selectedProduct");
                    
                    if(selectedProduct != null) helper.getSalesReps(component, selectedAccount, selectedProduct);
                    else component.set("v.loading", false);
                    
                } else {
                    
                    alert('Problem getting Account details, response state: ' + state);
                    component.set("v.loading", false);
                }
            });
                    
            $A.enqueueAction(action);
        
        }));
    },
    getSalesReps : function(component, selectedAccount, selectedProduct){
        
        return new Promise($A.getCallback(function (resolve, reject) {          
            
            var action = component.get("c.findSalesRep");
            action.setParams(
                {
                    productGroupId: selectedProduct.Product_Group__c, 
                    businessUnitId: selectedProduct.Business_Unit_ID__c, 
                    businessUnitGroupId: selectedProduct.Business_Unit_Group__c, 
                    accountId : selectedAccount.Id
                }
            );
             
            action.setCallback(this, function(response) {
                
                var state = response.getState();            
                if(state === "SUCCESS") {
                    
                    var subBusinessUnits = response.getReturnValue();
                    
                    if(subBusinessUnits.length > 0) component.set("v.noSubBusinessUnitResults", false);
                   	else component.set("v.noSubBusinessUnitResults", true);
                    
                    component.set("v.subBusinessUnits", subBusinessUnits);   
                    
                } else {
                    
                    alert('Problem searching Sales Reps, response state: ' + state);                    
                }
                
                component.set("v.loading", false); 
            });
            
            $A.enqueueAction(action);
        }));
        
    }
})