({
	productSearchChange : function(component, event, helper){
        
        var timer = component.get('v.timer');
        clearTimeout(timer);

        var timer = setTimeout(function(){
            
            var inputSearch = component.get("v.productSearch");
            
            if(inputSearch == null || inputSearch.length < 3){
                
                component.set("v.productSearchError", "You need to enter at least 3 characters");
                
            }else{
            
            	component.set("v.loading", true);
            	component.set("v.products", null); 
            	component.set("v.selectedProduct", null);  
                component.set("v.subBusinessUnits", null);  
                component.set("v.productSearchError", null);
                
                helper.searchForProducts(component);
            }
            
            clearTimeout(timer);
            component.set('v.timer', null);
        }, 1000);

        component.set('v.timer', timer);
    },
    selectProduct : function(component, event, helper){
        
       	component.set("v.loading", true);
       	component.set("v.products", null); 
        component.set("v.productSearchError", null);
		helper.getProductDetails(component, helper, event.currentTarget.getAttribute("id"));
    },
    accountSearchChange : function(component, event, helper){
        
        var timer = component.get('v.timer');
        clearTimeout(timer);

        var timer = setTimeout(function(){
            
            var inputSearch = component.get("v.accountSearch");
            
            if(inputSearch == null || inputSearch.length < 3){
                
                component.set("v.accountSearchError", "You need to enter at least 3 characters");
                
            }else{
            
                component.set("v.loading", true);
                component.set("v.accounts", null); 
                component.set("v.selectedAccount", null); 
                component.set("v.subBusinessUnits", null);  
                component.set("v.accountSearchError", null);
                
                helper.searchForAccount(component);
            }
            
            clearTimeout(timer);
            component.set('v.timer', null);
        }, 1000);

        component.set('v.timer', timer);
    },
    selectAccount : function(component, event, helper){
        
       	component.set("v.loading", true);
       	component.set("v.accounts", null); 
        component.set("v.accountSearchError", null);
		helper.getAccountDetails(component, helper, event.currentTarget.getAttribute("id"));
    }
})