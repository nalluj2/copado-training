({
    addClass : function (element, className) {
        //Global Aura util method for adding a style class from an aura element
        $A.util.addClass(element,className);
    }, 
    
    removeClass : function (element , className) {
        //Global Aura util method for removing a style class from an aura element
        $A.util.removeClass(element,className);
    },
    
    showElement : function (element) {
        //To show an aura html element
        var self = this;
        self.removeClass(element,'slds-hide');
        self.addClass(element,'slds-show');
    },
    
    hideElement : function (element) {
        //To hide an aura html element
        var self = this;
        self.removeClass(element,'slds-show');
        self.addClass(element,'slds-hide');
    },
    searchContent : function (component,searchContent) {
        
        component.set("v.fetchedRecords", []);
        component.set("v.issearching", true);
        
        var searchContent = component.get("v.searchText");
        var objectType = component.get("v.objectType");
        
        var action = component.get("c.searchAccount");
        action.setParams({ searchText : searchContent, objectType : objectType });
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                component.set("v.fetchedRecords",response.getReturnValue()); 
                var lookupContainerCmp = component.find("lookUpPane");
                this.addClass(lookupContainerCmp,'slds-is-open');
                this.removeClass(lookupContainerCmp,'slds-is-close');                
                component.set("v.issearching", false);
                                
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
		$A.getCallback(function() {
        	$A.enqueueAction(action);
    	})();
    }
})