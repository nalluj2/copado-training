({
    onLeaveLookupPane : function(component, event, helper) {
        //Search - Input control focus removed on mouse leave       
        var inputContainerCmp = component.find('master-container');
        helper.removeClass(inputContainerCmp,'slds-has-input-focus');        
    },
    remove : function (component, event, helper) {
        
        component.set("v.selectedRecord", {});
        
        var onSelectAction = component.get("v.onSelect");            
        if(onSelectAction != null) $A.enqueueAction(onSelectAction);
    },
    onblur : function (component, event, helper){
        
        var timer = setTimeout(function(){
            console.log('onblur');
            //Hide the lookup
            var lookupContainerCmp = component.find("lookUpPane");
            helper.removeClass(lookupContainerCmp,'slds-is-open');
            helper.addClass(lookupContainerCmp,'slds-is-close'); 
            
            component.set("v.searchText", null);
        }, 300);
    },
    onInputChange : function (component, event, helper) {
        
		var timer = component.get('v.timer');
        clearTimeout(timer);
    
        var timer = setTimeout(function(){
						
        	var searchContent = component.get("v.searchText");        
        	            
            if (searchContent && searchContent.trim().length > 0 ) {
            	            
                searchContent = searchContent.trim();

                if(searchContent.length >= 2){
                    
                    component.set("v.inputMessage", null);
                    helper.searchContent(component,searchContent);
                    
                }else{
                    
                    component.set("v.inputMessage", "You need to specify at least 2 characters");
                }
                    
            } else {
            	
                component.set("v.inputMessage", null);
                var lookupContainerCmp = component.find("lookUpPane");
            	helper.removeClass(lookupContainerCmp,'slds-is-open');
            	helper.addClass(lookupContainerCmp,'slds-is-close');
            }
            
        	clearTimeout(timer);
        	component.set('v.timer', null);
            
     	}, 600);

        component.set('v.timer', timer);            
    },
    onSearchInputClick : function (component, event, helper) {
        //input control foucs enabled by adding focus style class
        var inputContainerCmp = component.find('master-container');
        helper.addClass(inputContainerCmp,'slds-has-input-focus');
    },
    selectedRecordRowClick : function (component, event, helper) {
        console.log('onchange');
        var targetSource = event.currentTarget;
        var selectedIndex = targetSource.dataset.index;
        
        var listedRecords = component.get("v.fetchedRecords");
        if (listedRecords && listedRecords.length > 0 && +selectedIndex >=0) {
            
            var selectedRecord = listedRecords[selectedIndex];
            component.set("v.selectedRecord", selectedRecord);
            
            //Search input control value resets
            component.find("searchContent").set("v.value",""); 
            
            //Hide the lookup
            var lookupContainerCmp = component.find("lookUpPane");
            helper.removeClass(lookupContainerCmp,'slds-is-open');
            helper.addClass(lookupContainerCmp,'slds-is-close'); 
            
            var onSelectAction = component.get("v.onSelect");            
            if(onSelectAction != null) $A.enqueueAction(onSelectAction);
        }
    }
})