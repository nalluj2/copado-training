({
	init : function(component, event, helper) {
		
        window.addEventListener("resize",function(e){
  			helper.resize(component);
		});
        
        var columns = [
            {label: '', fixedWidth : 50,  type:'button-icon', typeAttributes: {iconName : 'utility:preview', name : 'view', title : "View Request"}},
            {label: 'Subject', fieldName: 'subject', type: 'text'},
            {label: 'Account', fieldName: 'account', type: 'text'},
            {label: 'Status', fieldName: 'status', type: 'text'},
            {label: 'Request Type', fieldName: 'requestType', type: 'text'},
            {label: 'Procedure / Activity Type', fieldName: 'activity', type: 'text'},
            {label: 'Start of Procedure', fieldName: 'start', type: 'date', typeAttributes: {day: 'numeric',  month: 'short', year: 'numeric', hour: '2-digit', minute: '2-digit', second: '2-digit'  }, sortable: true},
            {label: 'Procedure duration', fieldName: 'duration', type: 'text'},            
            {label: 'Team', fieldName: 'team', type: 'text'},            
            {label: 'Assigned To', fieldName: 'assignedTo', type: 'text', wrapText : true}
        ];
        
        component.set("v.columns", columns);
        
        helper.resize(component)
        
        component.set("v.orderDirection", "asc");
        
        component.set("v.statusFilter", "Open");
                		
        helper.loadData(component, 50, 0, null);
	},
    loadMoreData : function(component, event, helper) {
		
        component.set('v.enableInfiniteLoading', false);
        event.getSource().set("v.isLoading", true);
        var offset = component.get("v.data").length;
        
        helper.loadData(component, 50, offset, event);
	},
    refresh : function(component, event, helper) {
        
        var limit = component.get("v.data").length;        
        component.set("v.data", []);
        
        helper.loadData(component, limit, 0, null);
    },
    sortTable :  function(component, event, helper) {
                
        var direction = event.getParam("sortDirection");
        component.set("v.orderDirection", direction);
        
        component.set("v.data", []);
        
        helper.loadData(component, 50, 0, null);
	},
    viewRequest: function (cmp, event, helper) {
                
        var row = event.getParam('row');

        var selectedType;
        if(row.requestType == 'Implant Support Request') selectedType = 'implant';
        else if(row.requestType == 'Service Request') selectedType = 'service';
        else if(row.requestType == 'Meeting') selectedType = 'meeting';
        var selectedId = row.id; 
        
        var appEvent = $A.get("e.c:Select_Activity");
        appEvent.setParams({action : "Open", selectedType : selectedType, selectedId : selectedId});
		appEvent.fire();
    },
    search  : function(component, event, helper) {
    
    	component.set("v.data", []);        
        
        helper.loadData(component, 50, 0, null);
	}
    
})