({
    resize : function(component){
    	
    	var height = document.documentElement.clientHeight - 230;
        component.set("v.tableHeight", height);
    
	},
 	loadData : function(component, limit, offset, event) {
		
        if(event == null) component.set("v.showLoading", true);
        console.log("called");
        var accountFilter = component.get("v.accountFilter");
        accountFilter = accountFilter.Id;
        
        var statusFilter = component.get("v.statusFilter");
        var startFilter = component.get("v.startFilter");
        var typeFilter = component.get("v.typeFilter");
        var teamFilter = component.get("v.teamFilter");
        var assignedToFilter = component.get("v.assignedToFilter");
        var orderDirection = component.get("v.orderDirection");
         
        var action = component.get("c.getActivityRequests");
        action.setParams({ accountFilter : accountFilter, 
                          statusFilter : statusFilter, 
                          startFilter : startFilter,
                          typeFilter: typeFilter,
                          teamFilter: teamFilter,
                          assignedToFilter: assignedToFilter,
                          size : limit,
                          offset : offset,                          
                          orderDirection : orderDirection});
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var dataLoad = response.getReturnValue();

                var currentData = component.get('v.data');
                var newData = currentData.concat(dataLoad);
                component.set('v.data', newData);

                if(dataLoad.length < limit){
                    
                    component.set('v.loadMoreMessage', null);
                    component.set('v.enableInfiniteLoading', false);
                    
                }else if(newData.length >= 2000){
                    
                    component.set('v.loadMoreMessage', 'Max number of results reached. Refine your search');
                    component.set('v.enableInfiniteLoading', false);
                    
                }else{
                    
                    component.set('v.loadMoreMessage', null);
                    component.set('v.enableInfiniteLoading', true);
                }
                
                if(event == null) component.set("v.showLoading", false);
                else event.getSource().set("v.isLoading", false);
                 
            } else if (state === "ERROR") {
                
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
		$A.getCallback(function() {
        	$A.enqueueAction(action);
    	})();
	}
})