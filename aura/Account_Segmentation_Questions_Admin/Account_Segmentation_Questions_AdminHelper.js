({
	 resize : function (component, event, helper){
  		
        if(component.isValid()){
        
            var timer = component.get('v.timer');
            clearTimeout(timer);
        
            var timer = setTimeout(function(){
                                
                clearTimeout(timer);
                component.set('v.timer', null);
                
                helper.setSizes(component);
                
            }, 500);
    
            component.set('v.timer', timer);      
        }
    },
    setSizes : function(component){
        
		var tableWidth = component.find("mainTable").getElement().getBoundingClientRect().width;
                
        var questions = component.get("v.questions");
        
        if(questions.length > 0 ){
            
            var questionWidth = Math.floor(((tableWidth - 310) / questions.length));
            var widthReminder = (tableWidth - 310) % questions.length;
        
            component.set("v.questionWidth", questionWidth);  
            component.set("v.widthReminder", widthReminder);
            
            setTimeout(function(){
                
                var containerHeight = component.find("mainContainer").getElement().getBoundingClientRect().height;
                var tableHeader = component.find("mainTableHeader").getElement().getBoundingClientRect().height;
                
                var tableHeight = containerHeight - tableHeader - 200;
                
                component.set("v.tableMaxHeight", tableHeight);
            }, 200);
        }
    },
    getMainFilters : function(component, event, helper, withInputFilters) {
		
        var purpose = component.get("v.purpose");        
        var fiscalYear = component.get("v.filterFiscalYear");
        var filterScope = component.get("v.filterScope");
        
        var action = component.get("c.getMainFilterOptions");
        action.setParams({ purpose : purpose, fiscalYear : fiscalYear});
        
        component.set("v.showLoading", true);
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var result = response.getReturnValue();                  
                                
                component.set("v.fiscalYearOptions" , result['fiscalYearOptions']);
                component.set("v.readOnlyFY" , result['readOnlyFY']);
                component.set("v.scopeOptions" , result['scopeOptions']);
                component.set("v.countryOptions" , [{label: 'All', value : ''}]);
                component.set("v.segmentOptions" , [{label: 'All', value : ''}]);
                component.set("v.hasFilterChange" , true); 
                component.set("v.recordCount", null);
                                
                if(withInputFilters == false){
                    
                    if(fiscalYear == null && result['fiscalYear'].length > 0) component.set("v.filterFiscalYear", result['fiscalYear'][0].value);
                    if(filterScope == null){
                        
                        if(result['defaultBUG'] != null && this.filterInOptions(result['defaultBUG'][0].value, result['scopeOptions'])){
                        	
                            filterScope = result['defaultBUG'][0].value;
                        	component.set("v.filterScope", filterScope);                    
                        }
                        
                    }else if(this.filterInOptions(filterScope, result['scopeOptions']) == false){
                        
                        filterScope = null;
                        component.set("v.filterScope", filterScope);                    
                        component.set("v.filterCountry", "");                    
                        component.set("v.filterSegment", "");                                        
                    }
                }    
                
                if(filterScope != null){
                    
                    this.getSecondaryFilters(component, event, helper, withInputFilters);
                    
                }else component.set("v.showLoading", false);
                                
            }else if (state === "ERROR") {
                
                var errors = response.getError();
                
                if (errors && errors[0] && errors[0].message) {
                             
                    var showError = $A.get("e.c:Show_Error_Toast");
                    showError.setParams({ "message" : errors[0].message });
                    showError.fire();                        
                    
                } else {
                    alert("Unknown error");
                }
                
                component.set("v.showLoading", false);
            }            
        });
        
		$A.getCallback(function() {
        	$A.enqueueAction(action);
    	})();
	},
    getSecondaryFilters : function(component, event, helper, inputFilters) {
		
        var purpose = component.get("v.purpose");        
        var fiscalYear = component.get("v.filterFiscalYear");
        var filterScope = component.get("v.filterScope");
        var segment = component.get("v.filterSegment");
        var accountCountry = component.get("v.filterCountry");
                
        var action = component.get("c.getSecondaryFilterOptions");
        action.setParams({ purpose : purpose, fiscalYear : fiscalYear, scope : filterScope, segment : segment, accountCountry : accountCountry});
        
        component.set("v.showLoading", true);
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var result = response.getReturnValue(); 
                
                var reset = false;
                
                var countryOptions = result['countryOptions'];                
                component.set("v.countryOptions", countryOptions);
                if(this.filterInOptions(accountCountry, countryOptions) == false){
                    
                    component.set("v.filterCountry", "");
                    reset = true;
                }

                var segmentOptions = result['segmentOptions'];                
                component.set("v.segmentOptions", segmentOptions);
                if(this.filterInOptions(segment, segmentOptions) == false){
                 
                    component.set("v.filterSegment", "");
                    reset = true;
                }
                
                if(reset == true) this.getSecondaryFilters(component, event, helper, inputFilters);
                else this.getCount(component, event, helper, inputFilters);
                                
            }else if (state === "ERROR") {
                
                var errors = response.getError();
                
                if (errors && errors[0] && errors[0].message) {
                             
                    var showError = $A.get("e.c:Show_Error_Toast");
                    showError.setParams({ "message" : errors[0].message });
                    showError.fire();                        
                    
                } else {
                    alert("Unknown error");
                }
            }            
        });
        
		$A.getCallback(function() {
        	$A.enqueueAction(action);
    	})();
	},
    filterInOptions : function(value, options) {
        
        if(value == null || value == '') return true;
        
        for(var i = 0; i < options.length; i++){
            
            if(value == options[i].value) return true;
        }
        
        return false;
    },
    getCount : function(component, event, helper, inputFilters) {
		
        var purpose = component.get("v.purpose");        
        var fiscalYear = component.get("v.filterFiscalYear");
        var scope = component.get("v.filterScope");
        var account = component.get("v.filterAccount");
        var accoundId = (account != null ? account.Id : null);
        var accountCountry = component.get("v.filterCountry");
        var segmentation = component.get("v.filterSegment");
        var assignedTo = component.get("v.filterAssignedTo");
        var assignedToId = (assignedTo != null ? assignedTo.Id : null);
        var status = component.get("v.filterStatus");
        
        if(fiscalYear == null || fiscalYear == '' || scope == null || scope == ''){
            
            component.set("v.recordCount", null);
            return;
        }
        
        var action = component.get("c.getRecordCount");
        action.setParams({ purpose : purpose, fiscalYear : fiscalYear, scope : scope, accoundId : accoundId, accountCountry : accountCountry, segmentation : segmentation, assignedTo : assignedToId, status : status});
        
        component.set("v.showLoading", true);
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var count = response.getReturnValue();
                component.set("v.recordCount", count);
                
                if(inputFilters && count < 1000){
                    
                    var getFormulaireAction = component.get("c.getFormulaire");
                    $A.enqueueAction(getFormulaireAction);                
                }
               
            }else if (state === "ERROR") {
                
                var errors = response.getError();
                
                if (errors && errors[0] && errors[0].message) {
                             
                    var showError = $A.get("e.c:Show_Error_Toast");
                    showError.setParams({ "message" : errors[0].message });
                    showError.fire();                        
                    
                } else {
                    alert("Unknown error");
                }
            }
            
            component.set("v.showLoading", false);
        });
        
		$A.getCallback(function() {
        	$A.enqueueAction(action);
    	})();
	},
    getFormulaire : function(component, event, helper) {
		
        var purpose = component.get("v.purpose");
        var fiscalYear = component.get("v.filterFiscalYear");
        var scope = component.get("v.filterScope");
        var account = component.get("v.filterAccount");
        var accoundId = (account != null ? account.Id : null);
        var accountCountry = component.get("v.filterCountry");
        var segmentation = component.get("v.filterSegment");
        var assignedTo = component.get("v.filterAssignedTo");
        var assignedToId = (assignedTo != null ? assignedTo.Id : null);
        var status = component.get("v.filterStatus");
        
        var action = component.get("c.getUserFormulaire");
        action.setParams({ purpose : purpose, fiscalYear : fiscalYear, scope : scope, accoundId : accoundId, accountCountry : accountCountry, segmentation : segmentation, assignedTo : assignedToId, status : status});
        
        component.set("v.showLoading", true);
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                
                var formulaire = response.getReturnValue();                  
                var accInputs = formulaire.accountInputs;
                var hasCurrentSegmentation = false;
                
                var readOnlyFY = component.get("v.readOnlyFY");
                var isReadOnly = this.filterInOptions(fiscalYear, readOnlyFY);
                
                for(var i = 0; i < accInputs.length; i++){
                    
                    accInputs[i].disabled = isReadOnly;
                    if(accInputs[i].currentSegmentation != null && (accInputs[i].currentSegmentation.Behavioral_Segment__c != null || accInputs[i].currentSegmentation.Strategical_Segment__c != null)) hasCurrentSegmentation = true;
                }
                                
                component.set("v.questions", formulaire.questions);
                helper.setSizes(component);
                
                helper.applySorting(component, accInputs);
                
                component.set("v.hasCurrentSegmentation", hasCurrentSegmentation);
                component.set("v.accountInputs", accInputs);                                
                component.set("v.hasChange", false);
                component.set("v.hasSelection", false);
                component.set("v.selectedAll", false);
                component.set("v.hasFilterChange" , false);
                
            }else if (state === "ERROR") {
                
                var errors = response.getError();
                
                if (errors && errors[0] && errors[0].message) {
                             
                    var showError = $A.get("e.c:Show_Error_Toast");
                    showError.setParams({ "message" : errors[0].message });
                    showError.fire();                        
                    
                } else alert("Unknown error");                
            }
            
            component.set("v.showLoading", false);
        });
        
		$A.getCallback(function() {
        	$A.enqueueAction(action);
    	})();
	},
    applyInputFilters : function(component, filters) {
    	
        component.set("v.filterAccount", null);        
    	component.set("v.filterStatus", null);
                
		var filterList = filters.split(":");
        
        component.set("v.filterScope", filterList[0]);   
        
        if(filterList.length > 1){
            
            var segmentation = filterList[1];
            if(segmentation == 'Not Segmented') segmentation = '-';
            
            component.set("v.filterSegment", segmentation);
        }
        else component.set("v.filterSegment", null);
                
        if(filterList.length > 2 && filterList[2].split(';')[0] != null && filterList[2].split(';')[0] != ''){
            var assignedTo = {sobjectType : 'User', Id : filterList[2].split(';')[0], Name : filterList[2].split(';')[1]};
			component.set("v.filterAssignedTo", assignedTo);
        }else component.set("v.filterAssignedTo", "{sobjectType:'User'}");
        
        if(filterList.length > 3) component.set("v.filterCountry",  filterList[3]);        
        else component.set("v.filterCountry", null);
        
        if(filterList.length > 4) component.set("v.filterFiscalYear",  filterList[4]);
        else component.set("v.filterFiscalYear", null);
    },
    applySorting : function (component, accInputs){
        
        var sortBy = component.get("v.sortBy");
        var sortDir = component.get("v.sortDir");
        
        accInputs.sort(function(a, b){
            
            if(a.show == false && b.show == true) return 1;
            if(a.show == true && b.show == false) return -1;
            if(a.show == false && b.show == false) return 0;
                        
            var result;
            
            if(sortBy == "AccountName"){
            	
                result = a.record.Account__r.Name.toLowerCase().localeCompare(b.record.Account__r.Name.toLowerCase());
                
                if(sortDir == 'DESC') result = 0 - result;
            
            }else if(sortBy == "Status"){
            	
                if(a.record.Complete__c == false){
                    
                    if(b.record.Complete__c == true) result = -1;
                    else{
                        
                        if((a.record.Approver_1__c == null && b.record.Approver_1__c != null) || (a.record.Approver_2__c == null && b.record.Approver_2__c != null)) result = -1;
                        else if((a.record.Approver_1__c != null && b.record.Approver_1__c == null) || (a.record.Approver_2__c != null && b.record.Approver_2__c == null)) result = 1;
                        else result = 0;                        
                    }
                    
                }else if(a.record.Approver_1__c == null){
                    
                    if(b.record.Complete__c == false) result = 1;
                    else{
                        
                        if((b.record.Approver_1__c != null && b.record.Approved__c == false) || (b.record.Approver_2__c != null && b.record.Approved_2__c == false)) result = 1;
                        else if(b.record.Approver_1__c != null) result = -1;
                    	else result = 0;
                    }
                    
                }else if(a.record.Approved__c == false){
                    
                    if(b.record.Complete__c == false) result = 1;
                    else{
                        
                        if(b.record.Approver_1__c == null || b.record.Approved__c == true) result = -1;   
                        else if(a.record.Approver_2__c == null){
                            
                            if(b.record.Approver_2__c != null) result = -1;
                            else result = 0;
                            
                        }else{
                        	
                            if(b.record.Approver_2__c == null) result = 1;
                            else result = 0;
                        }                    	
                    }                    
                    
                }else if(a.record.Approver_2__c == null){
                    
                    if(b.record.Complete__c == false) result = 1;
                    else{
                        
                        if(b.record.Approved__c == false || (b.record.Approver_2__c != null && b.record.Approved_2__c == false)) result = 1;
                        else if(b.record.Approver_2__c != null) result = -1;
                    	else result = 0;
                    }
                    
                }else if(a.record.Approved_2__c == false){
                    
                    if(b.record.Complete__c == false || (b.record.Approver_1__c != null && b.record.Approved__c == false)) result = 1;
                    else{
                        
                        if(b.record.Approver_1__c == null || b.record.Approver_2__c == null || a.record.Approved_2__c == true) result = -1                        
                    	else result = 0;
                    }                 
                    
                }else{
                    
                    if(b.record.Complete__c == false || b.record.Approved__c == false || b.record.Approved_2__c == false) result = 1;
                    else result = 0; 
                }
                
                if(sortDir == 'DESC') result = 0 - result;
            }
            
            if(result == 0) result = a.record.Account__r.Name.toLowerCase().localeCompare(b.record.Account__r.Name.toLowerCase());
            if(result == 0) result = a.record.Account__c.localeCompare(b.record.Account__c);            
            
            return result;        
        });  
        
        return accInputs;        
    },
    saveForm : function(component, accInputs) {
		
        var action = component.get("c.saveUserFormulaire");
        action.setParams({formulaire : accInputs});
        
        component.set("v.showLoading", true);
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var oldInputs = component.get("v.accountInputs");
                var newInputs = response.getReturnValue();
                
				for (var i = 0; i < oldInputs.length; i++) {
                    
                	oldInputs[i].record.Id = newInputs[i].record.Id;
                    oldInputs[i].completedTS = newInputs[i].completedTS;
                    oldInputs[i].approved1TS = newInputs[i].approved1TS;
                    oldInputs[i].approved2TS = newInputs[i].approved2TS;
                    oldInputs[i].changed = false;
                    
                    for(var j = 0; j < oldInputs[i].accountAnswers.length; j++){
                        
                        oldInputs[i].accountAnswers[j].record.Id = newInputs[i].accountAnswers[j].record.Id;
                        oldInputs[i].accountAnswers[j].answerTS = newInputs[i].accountAnswers[j].answerTS;
                        oldInputs[i].accountAnswers[j].changed = false;
                    }
                }
                
                component.set("v.accountInputs", oldInputs);
                component.set("v.hasChange", false);
                
                var purpose = component.get("v.purpose");
                
                var refreshEvent = $A.get("e.c:Segmentation_Overview_Refresh");
        		refreshEvent.setParams({ "data_type" : purpose });
        		refreshEvent.fire();
                
            }else if (state === "ERROR") {
                
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                        
                    var showError = $A.get("e.c:Show_Error_Toast");
                    showError.setParams({ "message" : errors[0].message });
                    showError.fire();                        
                    
                } else {
                    alert("Unknown error");
                }
            }
            
            component.set("v.showLoading", false);
        });
        
		$A.getCallback(function() {
        	$A.enqueueAction(action);
    	})();
	},
    selectRow : function(component){
        
        var accInputs = component.get("v.accountInputs");
		var hasSelection = false;        
        var hasLevel1 = false;
        var hasLevel2 = false;
        
        for (var i = 0; i < accInputs.length; i++) {
            
            if(accInputs[i].selected == true){
                
                hasSelection = true;
                if(accInputs[i].record.Approver_1__c != null) hasLevel1 = true;
                if(accInputs[i].record.Approver_2__c != null) hasLevel2 = true;
            }    		
        }
		
        var newStatusOptions = [                
            { value: "Open", label: "Open" },
            { value: "Complete", label: "Complete" }            
        ];
        
        if(hasLevel1 == true) newStatusOptions.push({value: "Approved1", label: "Approved level 1"});
        if(hasLevel2 == true) newStatusOptions.push({ value: "Approved2", label: "Approved level 2"});
        
        component.set("v.newStatusOptions", newStatusOptions);        
		component.set("v.hasSelection", hasSelection);
    }
})