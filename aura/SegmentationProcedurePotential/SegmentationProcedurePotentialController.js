({
    init : function(component, event, helper) {
        
        var isChrome = navigator.appVersion.indexOf("Chrome/") != -1;        
        component.set("v.isChrome", isChrome);
        
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        component.set("v.userId", userId);         
	},
    numericOnly : function(component, event) {
        
        var numbers = '-0123456789';
        if (numbers.indexOf(event.key) < 0){
        	event.preventDefault()
    	}
    },
    accountPotentialChanged : function(component, event, helper) {
    
        var procedure = component.get("v.accountProcedure");
        procedure.changed = true; 
		component.set("v.hasChange", true);
        
        if(!isNaN(parseInt(procedure.record.Potential__c)) && parseInt(procedure.record.Potential__c) > 0 &&  procedure.record.Last_12_mth_Sales__c != null){
        	
            var percentage = (procedure.record.Last_12_mth_Sales__c / procedure.record.Potential__c) * 100;
            procedure.marketShare = (Math.round(percentage * 100) / 100);
            
        }else{
            
            procedure.marketShare = null;
        }
        
        component.set("v.accountProcedure", procedure);
    },
    medtronicPotentialChanged : function(component, event, helper) {
    
        var procedure = component.get("v.accountProcedure");
        procedure.changed = true; 
        component.set("v.hasChange", true);

        procedure.record.Potential__c = 0;
        if(!isNaN(parseInt(procedure.record.Potential_Medtronic__c))) procedure.record.Potential__c += parseInt(procedure.record.Potential_Medtronic__c);
            
        for(var i = 0; i < procedure.competitors.length; i++){

            if(!isNaN(parseInt(procedure.competitors[i].record.Potential__c))) procedure.record.Potential__c += parseInt(procedure.competitors[i].record.Potential__c);
        }
        
        if(!isNaN(parseInt(procedure.record.Potential__c)) && parseInt(procedure.record.Potential__c) > 0 &&  procedure.record.Last_12_mth_Sales__c != null){
        	
            var percentage = (procedure.record.Last_12_mth_Sales__c / procedure.record.Potential__c) * 100;
            procedure.marketShare = (Math.round(percentage * 100) / 100);
            
        }else{
            
            procedure.marketShare = null;
        }
        
        component.set("v.accountProcedure", procedure);
    },
    competitorPotentialChanged : function(component, event, helper) {
    
        var ctarget = event.getSource().get("v.id");
        var comp_idx = ctarget.split(":")[2];
        
        var procedure = component.get("v.accountProcedure");     
		procedure.changed = true; 
        procedure.competitors[comp_idx].changed = true;
        component.set("v.hasChange", true);
        
        var totalPotential = 0;
        if(!isNaN(parseInt(procedure.record.Potential_Medtronic__c))) totalPotential += parseInt(procedure.record.Potential_Medtronic__c);
            
        for(var i = 0; i < procedure.competitors.length; i++){
                
            if(!isNaN(parseInt(procedure.competitors[i].record.Potential__c))) totalPotential += parseInt(procedure.competitors[i].record.Potential__c);
        }
        
        if(totalPotential >0) procedure.record.Potential__c = totalPotential;
        else procedure.record.Potential__c = null;
        
        if(!isNaN(parseInt(procedure.record.Potential__c)) && parseInt(procedure.record.Potential__c) > 0 &&  procedure.record.Last_12_mth_Sales__c != null){
        	
            var percentage = (procedure.record.Last_12_mth_Sales__c / procedure.record.Potential__c) * 100;
            procedure.marketShare = (Math.round(percentage * 100) / 100);
            
        }else{
            
            procedure.marketShare = null;
        }
        
        component.set("v.accountProcedure", procedure);
    },
    rowSelect : function(component, event){
        
        var onRowSelect = component.get("v.onRowSelect");            
        if(onRowSelect != null) $A.enqueueAction(onRowSelect);
    }
})