({
	resize : function (component, event){
  		
        if(component.isValid()){
        
            var timer = component.get('v.timer');
            clearTimeout(timer);
        
            var timer = setTimeout(function(){
                                
                clearTimeout(timer);
                component.set('v.timer', null);
                
                var containerHeight = component.find("mainContainer").getElement().getBoundingClientRect().height;        
        		component.set("v.panelHeight", containerHeight - 70);
                
            }, 500);
    
            component.set('v.timer', timer);      
        }
    },
    getOverview : function (component, event, showLoading){
    	
        var action = component.get("c.getUserOverview");
        
        if(showLoading) component.set("v.showLoading", true);
        
        action.setCallback(this, function(response) {
    		
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var selectedOverview = component.get("v.selectedOverview");
                
                var userOverview = response.getReturnValue();
                var overviews = userOverview.completions;
                
                if(overviews.length > 0  && selectedOverview == null){
                    
                    component.set("v.selectedOverview", overviews[0].code);
                    component.set("v.bug" , userOverview.defaultBUG);
                    this.getDetails(component, event, showLoading);
                    
                }else component.set("v.showLoading", false);
                
                if(overviews.length == 0) component.set("v.noOverviews", true);
                else component.set("v.noOverviews", false);
                
                component.set("v.overviews", overviews);
                
            }else if (state === "ERROR") {
                
                component.set("v.showLoading", false);
                
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    
                    var showError = $A.get("e.c:Show_Error_Toast");
                    showError.setParams({ "message" : errors[0].message });
                    showError.fire();                        
                    
                } else {
                    alert("Unknown error");
                }
            }            
        });
        
		$A.getCallback(function() {
        	$A.enqueueAction(action);
    	})();    		
    },
    getDetails : function (component, event, showLoading){
    	
        var overviewCode = component.get("v.selectedOverview");
        
        if(overviewCode == null) return;
        
        var bug = component.get("v.bug");
        var sbu = component.get("v.sbu");
        var country = component.get("v.country");
        var completeStatus = component.get("v.completeStatus");
        
        var action;
        
        if(overviewCode.endsWith("Input")) component.set("v.valueType", "Completed");
        else component.set("v.valueType", "Approved");
        
        if(overviewCode.endsWith("Admin") && country == null){
            
            if(bug == null){
                
                component.set("v.selectedOverviewDetails", null);
                return;
            }
            
			action = component.get("c.getAdminOverviewDetails");
            action.setParams({code : overviewCode, bug : bug, completeStatus : completeStatus});            
                
        }else{
			
            var groupByOptions = [{value: "Segmentation", label: "Segmentation"}];        
            if(overviewCode.endsWith("Approval") || overviewCode.endsWith("Admin")) groupByOptions.push({value: "Assigned", label: "Assigned To"});
            if(overviewCode.endsWith("Admin")) groupByOptions.push({value: "Approver", label: "Approver lvl 1"});
            if(overviewCode.startsWith("Strategic") == false)groupByOptions.push({value: "BU", label: "Business Units"});
            
            component.set("v.groupByOptions", groupByOptions);
            
            var groupBy = component.get("v.groupBy");
            
            if(groupBy == null || (!overviewCode.endsWith("Approval") && !overviewCode.endsWith("Admin") && groupBy == "Assigned") || (!overviewCode.endsWith("Admin") && groupBy == "Approver")){
                
                groupBy = groupByOptions[0].value;
                component.set("v.groupBy", groupBy);
            }
            
            action = component.get("c.getOverviewDetails");
            action.setParams({code : overviewCode, groupBy : groupBy, country : country, bugFilter : bug, sbuFilter : sbu});
        }
        
        if(showLoading) component.set("v.showLoading", true);
        
        action.setCallback(this, function(response) {
    		
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var overviewDetails = response.getReturnValue();                
                component.set("v.selectedOverviewDetails", overviewDetails);
                                
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    
                    var showError = $A.get("e.c:Show_Error_Toast");
                    showError.setParams({ "message" : errors[0].message });
                    showError.fire();                        
                    
                } else {
                    alert("Unknown error");
                }
            }
            
            component.set("v.showLoading", false);
        });
        
		$A.getCallback(function() {
        	$A.enqueueAction(action);
    	})();    		
    }
})