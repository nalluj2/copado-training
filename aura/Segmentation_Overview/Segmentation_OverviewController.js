({
	doInit : function(component, event, helper) {
		
        window.addEventListener("resize",function(e){
  			helper.resize(component, event);
		});
        
        var bugOptions = [{value: "CVG", label: "CVG"}, {value: "MITG", label: "MITG"}, {value: "Restorative", label: "Restorative"}];                                
        component.set("v.bugOptions", bugOptions);
        
        helper.resize(component, event);
        
        helper.getOverview(component, event, true); 
	},
    refreshOverview : function(component, event, helper) {
		
        helper.getOverview(component, event, false); 
        helper.getDetails(component, event, false);
	},
    orderByChanged : function(component, event, helper) {
		
        helper.getDetails(component, event, true);
	},    
    selectOverview : function(component, event, helper) {
        
        var selected = event.target.id;
        
        component.set("v.selectedOverview", selected);
        helper.getDetails(component, event, true);
    },
    selectData : function(component, event, helper) {
    	
        var code = event.getParam("code");        
        var codeKeys = code.split(":");
                        
        var sbu = codeKeys[1];
        if(sbu == '') sbu = null;
        var country;
        if(codeKeys[0].startsWith('Potential')) country = codeKeys[5];
        else country = codeKeys[4];
        
        component.set("v.sbu", sbu);
        component.set("v.country", country);
        
        helper.getDetails(component, event, true);
    },
    clearFilters : function(component, event, helper) {
        
        component.set("v.sbu", null);
        component.set("v.country", null);
        
        helper.getDetails(component, event, true);
    }
})