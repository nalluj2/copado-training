({
	doInit : function(component, event, helper) {
		
        var fullMap = component.get("v.eventMap");
        var key = component.get("v.selectedKey");
        component.set("v.events", fullMap[key]);
	},
    editActivity : function(Component, event){
        
        var selectedEvent = event.currentTarget.dataset.value.split(":");
		   
        var selectedType;
        var selectedId;
        
        if(selectedEvent[1] == ""){
            
            selectedType = 'event';
            selectedId = selectedEvent[0];
            
        }else{
         
            selectedType = selectedEvent[2];
            selectedId = selectedEvent[1];            
        }
        
        var appEvent = $A.get("e.c:Select_Activity");
        appEvent.setParams({action : "Open", selectedType : selectedType, selectedId : selectedId});
		appEvent.fire();
    }
})