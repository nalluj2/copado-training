({
	jumpToData : function(component, event, helper) {
		
        var selectedOverviewDetails = component.get("v.selectedOverviewDetails");
        
        var jumpToEvent = $A.get("e.c:Segmentation_Overview_Jump_To");
       	jumpToEvent.setParams({ "code" : selectedOverviewDetails.code });
        jumpToEvent.fire();
	},
    selectData : function(component, event, helper) {
		
        var selectedOverviewDetails = component.get("v.selectedOverviewDetails");
        
        var selectEvent = $A.get("e.c:Segmentation_Overview_Select");
       	selectEvent.setParams({ "code" : selectedOverviewDetails.code });
        selectEvent.fire();
	},
    highlight : function(component, event, helper){
        
        var label = component.find("label");
        var numbers = component.find("numbers");
        var bar = component.find("progressbar");
        
		$A.util.addClass(label, 'highlight');
        $A.util.addClass(numbers, 'highlight');
        $A.util.addClass(bar, 'highlight');        
    },
    unhighlight : function(component, event, helper){
        
        var label = component.find("label");
        var numbers = component.find("numbers");
        var bar = component.find("progressbar");
        
		$A.util.removeClass(label, 'highlight');
        $A.util.removeClass(numbers, 'highlight');
        $A.util.removeClass(bar, 'highlight');        
    }    
})