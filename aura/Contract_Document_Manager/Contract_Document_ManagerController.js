({
	doInit : function(component, event, helper) {
		
        helper.getContractDocuments(component, helper);
        helper.getDocTypes(component);
	},
    refresh : function(component, event, helper){
        
        helper.getContractDocuments(component, helper);
    },
    versionSelected : function(component, event, helper) {
		
        var documents = component.get("v.documents");
        
        for(var i = 0; i < documents.length; i++){
            
            var document = documents[i];
            
            document.selectedVersion = document.versions[document.selectedVersionId];
        }
        
        component.set("v.documents" , documents);
	},
    createNewDocument : function(component, event, helper) {
        
    	component.set("v.mode", "New");  
        component.set("v.filesToUpload", []);
    },
    cancelNewDocument : function(component, event, helper) {
        
    	component.set("v.mode", "View");
    },
    handleFilesUpload : function(component, event, helper) {
        
    	var newFiles = component.get("v.newFiles");
        var filesToUpload = component.get("v.filesToUpload");
        
        var errors = [];
        
        if(newFiles.length > 0){
        	
            var mode = component.get("v.mode");
            
            for(var i = 0; i < newFiles[0].length; i++){
        		
                var newFile = newFiles[0][i];
                
                //If bigger than 35MB
                if(newFile.size > 36700160){
                 
                    errors.push('File ' + newFile.name + ' is too big. Maximum file size is 35MB.');
                    continue;
                }
                
                var name = newFile.name.substring(0, newFile.name.lastIndexOf('.')).toLowerCase();
                var extension = newFile.name.substring(newFile.name.lastIndexOf('.') + 1, newFile.name.length).toLowerCase();
                
                if(name.length > 80) name = name.substring(0, 80);
                
                newFile.docName = name;
                newFile.fileType = extension;
                newFile.fileSize = Math.ceil(newFile.size / 1024).toLocaleString() + ' KB';
                
                if(mode == 'New') filesToUpload.push(newFile);
                else if(mode == 'Edit') filesToUpload = [newFile];
            }
        }
        
        if(errors.length > 0){
            
            var errorString = '<ul style="list-style : disc">';
            
            for(var i = 0; i < errors.length; i++){
                
                errorString += '<li>' + errors[i] +'</li>';
            }
            
            errorString += '</ul>';
            
            helper.showMessage(component, errorString);
        }
        
        component.set("v.filesToUpload", filesToUpload);
    },
    uploadFiles : function(component, event, helper){
        
        var newFiles = component.get("v.filesToUpload");
        
        if(newFiles.length > 0){
            
            component.set("v.loadingFileName", '');
            component.set("v.progessValue", 0);
            component.set("v.showLoadingDocument", true);            
            helper.createFile(component, helper, 0);        
        }
    },
    updateDocument : function(component, event, helper){
        
        var selectedDoc = component.get("v.selectedDocument");
        var newName = component.get("v.newName");
        //var newDocType = component.get("v.newDocType");
        
        var repoDocument = component.get("v.repoDocumentUpdate");        
        repoDocument.id = selectedDoc.id;
        repoDocument.Name = newName;
        //repoDocument.Document_Type__c = newDocType;
        
        var result = sforce.connection.update([repoDocument]);

        if(result[0].getBoolean("success") == true) {
        	
            var newFiles = component.get("v.filesToUpload");
        
        	if(newFiles.length > 0){
            	
                component.set("v.loadingFileName", '');
                component.set("v.progessValue", 0);
            	component.set("v.showLoadingDocument", true);            
            	helper.createNewVersion(selectedDoc, newFiles, component, helper);     
                
            }else{
          		
                helper.getContractDocuments(component, helper);
            	component.set("v.mode", "View");
            }
        
        } else {
          	helper.showMessage(component, result[0]);
            //alert("Failed to update Document: " + result[0]);
        }    
    },
    deleteNewFile : function(component, event, helper){
    	
        var rowId = event.currentTarget.dataset.value;    	
    	
        var newFiles = component.get("v.filesToUpload");
        
        newFiles.splice(rowId, 1);
        
        component.set("v.filesToUpload", newFiles);
    },
    handleSelectAction : function(component, event, helper){
        var selected = event.detail.menuItem.get("v.value");
        var index = selected.split(':')[0];
        var action = selected.split(':')[1];
        
        console.log(selected);
        console.log(index);
        console.log(action);
		
		var documents = component.get("v.documents");        
        var selectedDoc = documents[index];

        if(action == 'view'){
            
            helper.getURL(component, selectedDoc.selectedVersion.id);
            
        }else if(action =='delete'){
            component.set("v.isDialogVisible", true);
            component.set("v.documentId", selectedDoc.selectedVersion.id);
            component.set("v.action", action);
            //if(confirm('Are you sure you want to delete this version?')) helper.deleteVersion(component, helper, selectedDoc.selectedVersion.id);
            
        }else if(action =='deleteDoc'){
            component.set("v.isDialogVisible", true);
            component.set("v.documentId", selectedDoc.id);
            component.set("v.action", action);
            //if(confirm('Are you sure you want to delete this document?')) helper.deleteDocument(component, helper, selectedDoc.id);
            
        }else if(action == 'version'){
        	
            component.set("v.selectedDocument", selectedDoc);
            component.set("v.newName", selectedDoc.name);
            //component.set("v.newDocType", selectedDoc.documentType);
            
            component.set("v.mode", "Edit");  
        	component.set("v.filesToUpload", []);
        }
    },
    showToast : function(component, event, helper) {
        
        var message = event.getParam('message');          
        
       	helper.showMessage(component, message);
	},    
	closeToast : function(component, event, helper) {
		
        $A.util.addClass( component.find( 'toastModel' ), 'slds-hide' );
        component.set("v.message", "");        
	},
    
    handleClick : function(component, event, helper) {
        var modelDetails = event.getParam('modalDetails');
        component.set("v.isDialogVisible", false);
        if(modelDetails != 'undefined'){
            if(modelDetails.status == 'Delete'){
                component.set("v.showLoading", true);
                if(modelDetails.action =='delete'){
                    helper.deleteVersion(component, helper, modelDetails.recordId);
                }else if(modelDetails.action =='deleteDoc'){
                    helper.deleteDocument(component, helper, modelDetails.recordId);
                } 
            }
        }
	}
})