({
	getContractDocuments : function(component, helper) {
		
        component.set("v.showLoading", true);
        var contractId = component.get("v.contractId");
        
        var action = component.get("c.getContractDocuments");
        action.setParams({ contractId : contractId });
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var repository = response.getReturnValue();
                
                var documents = repository.documents;
                
                for(var i = 0; i < documents.length; i++){
                    
                    if(documents[i].versions.length > 0){
                     
                        documents[i].selectedVersion = documents[i].versions[0];
                        documents[i].lastVersion = documents[i].versions[0];
                        documents[i].selectedVersionId = 0;
                        
                        for(var j = 0; j < documents[i].versions.length; j++){
                        	
                            var documentVersion = documents[i].versions[j];
                            
                            if(documentVersion.size != null) documentVersion.size = Math.ceil(documentVersion.size / 1024).toLocaleString() + ' KB';
                                                        
                            if(documentVersion.documentumId == null || documentVersion.markedDeletion == true || (repository.accessLevel != 'Read' && repository.accessLevel != 'Edit' && repository.accessLevel != 'Only Upload')) documentVersion.canView = false;
                            else documentVersion.canView = true;
                                                        
                            if((documentVersion.documentumId == null && documentVersion.interfaceError == null) || documentVersion.markedDeletion == true || repository.accessLevel != 'Edit' || helper.countActiveVersions(documents[i].versions) <= 1) documentVersion.canDelete = false;
                            else documentVersion.canDelete = true;
                        }
                        
                        if( documents[i].lastVersion.documentumId == null || repository.accessLevel != 'Edit') documents[i].canEdit = false;
                        else documents[i].canEdit = true;
                        
                        if(repository.accessLevel != 'Edit') documents[i].canDelete = false;
                        else documents[i].canDelete = true;
                        
                    }else{
                     
                        documents[i].selectedVersion = null;
                        documents[i].selectedVersionId = null;      
                        documents[i].canEdit = false;
                        documents[i].canDelete = false;
                    }
                    
                    console.log(documents[i].selectedVersion);
                }
                
                component.set("v.documents", documents);
				component.set("v.accessLevel", repository.accessLevel);                
                
            } else if (state === "ERROR") {
                
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    
                    this.showMessage(component, errors[0].message);
                    
                } else alert("Unknown error");                
            }
                            
            component.set("v.showLoading", false);
        });
        
		$A.enqueueAction(action);
	},
    countActiveVersions : function(documentVersions){
    	
    	var counter = 0;
    	
        for(var i = 0; i < documentVersions.length; i++){
            	
            if(documentVersions[i].markedDeletion == false) counter++;
        }
    	
        return counter;
	},    
    getDocTypes : function(component) {
		
        var action = component.get("c.getDocumentTypes");
       
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                component.set("v.documentTypes", response.getReturnValue());
                
            } else if (state === "ERROR") {
                
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    
                    this.showMessage(component, errors[0].message);
                    
                } else alert("Unknown error");           
            }            
        });
        
		$A.enqueueAction(action);
	},
    getURL : function(component, versionId) {
		
        var action = component.get("c.getDocumentVersionURL");
       	action.setParams({ versionId : versionId });
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var versionURL = response.getReturnValue();
                
                window.open(versionURL);
                
            } else if (state === "ERROR") {
                
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    
                    this.showMessage(component, errors[0].message);
                    
                } else alert("Unknown error");           
            }            
        });
        
		$A.enqueueAction(action);
	},
    deleteVersion : function(component, helper, versionId) {
		
        var action = component.get("c.markVersionForDeletion");
       	action.setParams({ versionId : versionId });
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                helper.getContractDocuments(component, helper);
                
            } else if (state === "ERROR") {
                
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    
                    this.showMessage(component, errors[0].message);
                    component.set("v.showLoading", false);
                } else alert("Unknown error");           
            }            
        });
        
		$A.enqueueAction(action);
	},
    deleteDocument : function(component, helper, docId) {
        var action = component.get("c.documentDeletion");
       	action.setParams({ documentId : docId });
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                helper.getContractDocuments(component, helper);
                
            } else if (state === "ERROR") {
                
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    
                    this.showMessage(component, errors[0].message);
                    component.set("v.showLoading", false);
                } else alert("Unknown error");           
            }            
        });
        
		$A.enqueueAction(action);
	},
    createFile : function(component, helper, index){
    	
        var newFiles = component.get("v.filesToUpload");
        
        if(index < newFiles.length){
            
            var newFile = newFiles[index];
            
            var progressValue = Math.ceil((index / newFiles.length) * 100);
            component.set("v.loadingFileName", newFile.name);
            component.set("v.progressValue", progressValue);
            
            var reader = new FileReader();
            reader.onloadend = $A.getCallback(function() {
                                
				var content = reader.result.split('base64,')[1];
                
				var repoDocument = component.get("v.repoDocument");
                repoDocument.Contract_Repository__c = component.get("v.contractId");
	    		repoDocument.Name = newFile.docName;
                repoDocument.Document_Type__c = newFile.documentType;
                var docResult = sforce.connection.create([repoDocument]);
                
                console.log(docResult[0]);
                if(docResult[0].getBoolean("success") == false){
                    helper.showMessage(component, 'Error creating document ' + newFile.docName + ': ' + docResult[0].errors);
                    helper.createFile(component, helper, index + 1);
                    return;
                }
                
                var documentVersion  = component.get("v.documentVersion");
                documentVersion.Document__c = docResult[0].id;
	    		documentVersion.File_Name__c = newFile.name
                documentVersion.Size__c = newFile.size;
	    		documentVersion.Version__c = 1;
	    		documentVersion.File_Type__c = newFile.fileType;
                
                var versionResult = sforce.connection.create([documentVersion]);
                
                console.log(versionResult[0]);
                if(versionResult[0].getBoolean("success") == false){
                    sforce.connection.deleteIds([docResult[0].id]);
                    helper.showMessage(component, 'Error creating document ' + newFile.docName + ': ' + versionResult[0].errors);
                    helper.createFile(component, helper, index + 1);
                    return;
                }
                
                var attachment = component.get("v.attachment");
                
    			attachment.Name = newFile.name;
                attachment.ContentType = newFile.type;
    			attachment.IsPrivate = false;    	                
    			attachment.Body =  content;    			
    			attachment.ParentId = versionResult[0].id;
                
    			var result = sforce.connection.create(
                    [attachment],
                    {
                        onSuccess : function(result) {
                            
                            console.log('callback: ' + index);
                            console.log( result);
                            
                            if(result[0].getBoolean("success") == false){;
                                var deleteResult = sforce.connection.deleteIds([versionResult[0].id]);
                            	console.log( deleteResult);
                                deleteResult = sforce.connection.deleteIds([docResult[0].id]);
                                console.log( deleteResult);
                                helper.showMessage(component, 'Error creating document ' + newFile.docName + ': ' + result[0].errors);                                
                            }
                            
	                        helper.createFile(component, helper, index + 1);                            
                        }                                         
                        ,
                        onFailure : function(error) {
                            console.log( error);
                            var deleteResult = sforce.connection.deleteIds([versionResult[0].id]);
                            console.log( deleteResult);
                            deleteResult = sforce.connection.deleteIds([docResult[0].id]);
                            console.log( deleteResult);
                            
                            helper.showMessage(component, 'Error creating document ' + newFile.docName + ': ' + error);  
                            
                            helper.createFile(component, helper, index + 1);
                        }
                    }
                );
            });            
            
            reader.readAsDataURL(newFile); 
            
        }else{
            
            //End of loading                            
            component.set("v.showLoadingDocument", false);
            
            helper.getContractDocuments(component, helper);
            component.set("v.mode", "View");
        }
    },
    createNewVersion : function(selectedDoc, newFiles, component, helper){
    	        
        var newFile = newFiles[0];
        
        component.set("v.loadingFileName", newFile.name);
        
        var reader = new FileReader();
        reader.onloadend = $A.getCallback(function() {
                                
            var content = reader.result.split('base64,')[1];
            
            var documentVersion  = component.get("v.documentVersion");
            documentVersion.Document__c = selectedDoc.id;
	    	documentVersion.File_Name__c = newFile.name
            documentVersion.Size__c = newFile.size;
            documentVersion.Version__c = (selectedDoc.selectedVersion.version + 1);
            documentVersion.File_Type__c = newFile.fileType;
                
            var versionResult = sforce.connection.create([documentVersion]);
                
            if(versionResult[0].getBoolean("success") == false){
            	
                helper.showMessage(component, versionResult[0].errors); 
                return;            
            }
                
            var attachment = component.get("v.attachment");
            attachment.Name = newFile.name;
            attachment.ContentType = newFile.type;
            attachment.IsPrivate = false;    	                
            attachment.Body =  content;    			
            attachment.ParentId = versionResult[0].id;
                
            var result = sforce.connection.create(
                [attachment],
                    {
                        onSuccess : function(result) {
                            
                            console.log('callback: ' + result);
                            
                            if(result[0].getBoolean("success") == false){
                            	
                                var deleteResult = sforce.connection.deleteIds([versionResult[0].id]);
                            	console.log( deleteResult);
                                
                                helper.showMessage(component, 'Error uploading file ' + newFile.name + ': ' + result[0].errors);                                
                            }
                            
                            component.set("v.showLoadingDocument", false);            
                            helper.getContractDocuments(component, helper);
                            component.set("v.mode", "View");
                        }                                         
                        ,
                        onFailure : function(error) {
                            
                            var deleteResult = sforce.connection.deleteIds([versionResult[0].id]);
                            console.log( deleteResult);
                            
                            helper.showMessage(component, 'Error uploading file ' + newFile.name + ': ' + error);    
                            component.set("v.showLoadingDocument", false);            
                            helper.getContractDocuments(component, helper);
                            component.set("v.mode", "View");
                        }
                    }
            );
        });            
            
        reader.readAsDataURL(newFile);             
    },
    showMessage : function(component, message){
        console.log('displaying error message: ' + message);
        component.set("v.message", message);
		
        $A.util.removeClass( component.find( 'toastModel' ), 'slds-hide' );
        $A.util.addClass( component.find( 'toastModel' ), 'slds-show' );        
    }
})