({
	doInit : function(component, event, helper) {
		
        helper.getUserTeams(component, event);
        
        helper.getOpenRequests(component, helper);
                
        helper.getPicklists(component, event);
        
        component.set("v.pickPersonalWeek", (new Date()).toISOString().split('T')[0]);
        component.set("v.pickTeamDay", (new Date()).toISOString().split('T')[0]);
        component.set("v.pickTeamWeek", (new Date()).toISOString().split('T')[0]);        
	},
    refresh : function(component, event, helper) {
        
        helper.getOpenRequests(component, helper);
    },
    editActivity : function(component, event, helper) {
        
        var action = event.getParam("action");
        
        if(action == "Open"){
        
            var selectedType = event.getParam("selectedType");
            var selectedId = event.getParam("selectedId");
            
            component.set("v.activityType", selectedType);
            
            if(selectedId != null){
                
                component.set("v.activityMode", "view");
                component.set("v.activityId", selectedId);
                
            }else{
                
                component.set("v.activityMode", "edit");
                component.set("v.activityId", null);    
            }
            
            component.set("v.showActivityPopup", true);
            
        }else{
            
            component.set("v.showActivityPopup", false);
        }
    },
    selectTeam : function(component, event, helper){
    	
        var selectedMenuItemValue = event.getParam("value");		
        var teams = component.get("v.teams");
        
        teams.forEach(function (team) {
                        
            if (team.selected) team.selected =  false;
            
            if (team.id === selectedMenuItemValue){
                
                team.selected = true;                
                component.set("v.selectedTeam", selectedMenuItemValue);
                component.set("v.selectedTeamName", team.name);
            }
        });
                
        if(selectedMenuItemValue == 'personal'){
            
            component.set("v.showPersonalCalendar", true);            
            component.set("v.showTeamCalendar", false);
            component.set("v.showRequestList", false);
            
        }else if(selectedMenuItemValue == 'list'){
            
            component.set("v.selectedTime", null);
            component.set("v.showRequestList", true);
            component.set("v.showPersonalCalendar", false);            
            component.set("v.showTeamCalendar", false);
            
        }else{
				
			component.set("v.showPersonalCalendar", false);            
            component.set("v.showRequestList", false);
            component.set("v.showTeamCalendar", true);
        }
        
        component.set("v.teams", teams);
    },
    selectTeamMember : function(component, event, helper){
    	
        var selectedMenuItemValue = event.getParam("value");		
        var picklistValues = component.get("v.picklistValues");
        
        picklistValues.Assigned_To__c.forEach(function (teamMember) {
                        
            if (teamMember.selected) teamMember.selected =  false;
            
            if (teamMember.value === selectedMenuItemValue){
                
                teamMember.selected = true;                
                component.set("v.personalUserId", teamMember.value);
                component.set("v.personalUserName", teamMember.label);
            }
        });         
    },
    openRequestList : function(component){
        
        var now = new Date();
        
        component.set("v.showPersonalCalendar", false);            
        component.set("v.showTeamCalendar", false);
        component.set("v.showRequestList", false);
        component.set("v.selectedTime", now.toISOString().split('T')[0]);
        component.set("v.selectedTeam", "list");
		component.set("v.selectedTeamName", "Request List");                
        component.set("v.showRequestList", true);
        
    },
    newActivitySelected : function(component, event, helper) {
        
        component.set("v.selectedTime", null);
        component.set("v.selectedMember", null);
        
        var selectedMenuItemValue = event.getParam("value");
        
        var appEvent = $A.get("e.c:Select_Activity");
        appEvent.setParams({action : "Open", selectedType : selectedMenuItemValue});
		appEvent.fire();
        
    },
    newActivitySelector : function(component, event, helper) {
        
        var selectedDate = event.getParam("selectedDate");
        var selectedMember = event.getParam("selectedMember");
        var activityType = event.getParam("activityType");
        
        component.set("v.selectedTime", selectedDate);
        component.set("v.selectedMember", selectedMember);
        
        var appEvent = $A.get("e.c:Select_Activity");
        appEvent.setParams({action : "Open", selectedType : activityType});
		appEvent.fire();
    },
    showDayView : function(component, event, helper) {
    	component.set("v.calendarView", "day");
    },
    showWeekView : function(component, event, helper) {
    	component.set("v.calendarView", "week");
    },
    showSettings : function(component, event, helper) {
    	component.set("v.showSettings", true);
    },
    hideSettings : function(component, event, helper) {
    	component.set("v.showSettings", false);
    },
    handleScheduler : function(component, event, helper) {
    	
        var action = event.getParam("action");
        
        if(action == "Open") component.set("v.showScheduler", true);
        else component.set("v.showScheduler", false);        
    },
	reload : function(component, event, helper) {
    	
        var appEvent = $A.get("e.c:Refresh_Calendar");
		appEvent.fire();    
    },
    openDay : function(component, event){
        
        var selectedDate = event.getParam("selectedDate");
            
        component.set("v.pickTeamDay", selectedDate.toISOString().split('T')[0]);        
        component.set("v.calendarView", "day");
    },
    leaveApp : function(component){
        
        if(window.location.href.includes("/partner/")) window.location.href ="/partner/";           
        else window.location.href ="/";
    }
})