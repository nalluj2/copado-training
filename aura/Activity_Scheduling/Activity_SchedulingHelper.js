({
	getUserTeams : function(component, event) {
		
        component.set("v.showLoading", true);
        
        var action = component.get("c.getTeams");
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                component.set("v.teams", response.getReturnValue());
                component.set("v.showRequestList", true);
                
                component.set("v.showLoading", false);
                
            } else if (state === "ERROR") {
                
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
		$A.enqueueAction(action);
	},
    getPicklists : function(component, event) {
		
        var action = component.get("c.getPicklistValues");
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
 				component.set("v.picklistValues", response.getReturnValue()); 
                component.set("v.personalUserName", response.getReturnValue().UserDetails[0].value);
                
                var userType = response.getReturnValue().UserDetails[1].value;
                if(userType == 'PowerPartner') component.set("v.isAgent", true);
                
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
		$A.enqueueAction(action);
	},
    getOpenRequests : function(component, helper) {
		        
        if(component.isValid() != true) return;
        
        var action = component.get("c.getOpenRequestsWeek");
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                component.set("v.openRequests", response.getReturnValue());
                                
            } else if (state === "ERROR") {
                
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
		$A.enqueueAction(action);
	}
})