({
	doInit : function(component, event, helper) {
		
        window.addEventListener("resize",function(e){
  			helper.resize(component, event, helper);
		});
        
        helper.getCalendarData(component, event, false); 
        
	},
    includeTravelTime : function(component, event, helper){
        
        helper.getCalendarData(component, event, true);
    },
    cancelAssignment : function(component, event, helper) {
        
    	var appEvent = $A.get("e.c:Show_Scheduler");
        appEvent.setParams({action : "Close"});
		appEvent.fire();
    }
})