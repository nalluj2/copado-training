({
	resize : function (component, event, helper){
  		
        if(component.isValid()){
        
            var timer = component.get('v.timer');
            clearTimeout(timer);
        
            var timer = setTimeout(function(){
                            
                helper.getCalendarData (component, event);
                
                clearTimeout(timer);
                component.set('v.timer', null);
                
            }, 500);
    
            component.set('v.timer', timer);      
        }
    }, getCalendarData : function(component, event, travelTime) {
		
        component.set("v.showLoading", true);
        component.set("v.isTravelTime", travelTime);
        
        var activityId = component.get("v.activityId");
        
        var width = document.documentElement.clientWidth - 200;
        
        var action = component.get("c.getTeamAvailability");
        action.setParams({ activityId : activityId, width : width, includeTravelTime : travelTime});
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            console.log(state);
            if (state === "SUCCESS") {
                
                var calendarView = response.getReturnValue();    
                
                component.set("v.hourWidth", calendarView.hourWidth);
                component.set("v.hours", calendarView.hours);
                component.set("v.events", calendarView.events);
                component.set("v.days", calendarView.days);  
                component.set("v.teamMembers", calendarView.members);
                component.set("v.activity", calendarView.activity);                
                                                
                var maxHeight = document.documentElement.clientHeight - 220;
                component.set("v.calendarMaxHeight", maxHeight)    ;
                
                component.set("v.showLoading", false);
                
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
		$A.getCallback(function() {
        	$A.enqueueAction(action);
    	})();
	}
})