({
	doInit : function(component, event, helper) {
		
        window.addEventListener("resize",function(e){
  			helper.resize(component, event, helper);
		});
       
        var statusOptions = [
            { value: "", label: "All" },
            { value: "Open", label: "Open" },
            { value: "Completed", label: "Completed" },
            { value: "Approved", label: "Approved" }            
        ];
       
        component.set("v.statusOptions", statusOptions);
        
        component.set("v.competitorOptions", [{ value: "Show", label: "Show" }, { value: "Hide", label: "Hide" }]);
        
        var inputFilters = component.get("v.inputFilters");
        
        if(inputFilters != null){
         
            helper.applyInputFilters(component, inputFilters); 
            component.set("v.hasFilterChange" , true);
            helper.getFilters(component, event, helper, true);
            
        }else{
        
        	helper.getFilters(component, event, helper, false);
        }
        
        helper.resize(component, event, helper);        
	},
    selectAll : function(component, event, helper) {
     	
        component.set("v.showLoading", true);
        
        setTimeout($A.getCallback(function(){
            
            var potInputs = component.get("v.potentialInputs");
            var selectedValue = component.get("v.selectedAll");
            
            for (var i = 0; i < potInputs.length; i++) {
                        
                if(potInputs[i].disabled == false) potInputs[i].selected = selectedValue;    		                
            }
            
            helper.selectRow(component);
            
            component.set("v.potentialInputs", potInputs);
            component.set("v.showLoading", false);
        }), 100);
    },
    selectRow : function(component, event, helper) {
     	        
        helper.selectRow(component);
    }, 
    mainFilterSelected : function(component, event, helper) {
                
        helper.getFilters(component, event, helper);         
        
        component.set("v.hasFilterChange" , true);
    },
    filterSelected : function(component, event, helper) {
        
        helper.getCount(component, event, helper);   
        
        component.set("v.hasFilterChange" , true);
    },
    getRecords : function(component, event, helper) {
        
        var hasChange = component.get("v.hasChange");
        
        if(hasChange && confirm('You have un-saved changes. Do you want to proceed?') == false) return;
        
        helper.getPotentialInputs(component, event, helper);         
    },
    inputFilterChanged : function(component, event, helper) {
        
        var inputFilters = component.get("v.inputFilters");
            
        if(inputFilters != null){
        	            
            helper.applyInputFilters(component, inputFilters);
            component.set("v.hasFilterChange" , true);
            helper.getFilters(component, event, helper, true);           
        }        
    },
    changeSorting : function(component, event, helper) {
        
        component.set("v.showLoading", true);
        
        setTimeout($A.getCallback(function(){
            
            var sortBy = component.get("v.sortBy");            
            var sortDir = component.get("v.sortDir");
            var newSortBy = event.getSource().get("v.id");
            
            if(newSortBy == sortBy){
                
                if(sortDir == 'ASC') sortDir = 'DESC';
                else sortDir = 'ASC';
                
            }else{
                
                sortBy = newSortBy;
                sortDir = 'ASC';
            }
            
            component.set("v.sortBy", sortBy); 
            component.set("v.sortDir", sortDir); 
            
            var potInputs = component.get("v.potentialInputs");
            
            helper.applySorting(component, potInputs);
            
            component.set("v.potentialInputs", potInputs);            
            component.set("v.showLoading", false);
            
        }), 100);
    },
    changeStatus : function(component, event, helper){
        
        component.set("v.showLoading", true);
        
        setTimeout($A.getCallback(function(){
            
            var potInputs = component.get("v.potentialInputs");
            var newStatus = component.get("v.newStatus");
            var hasChange = false;
            var numberEmpty = 0;
            
            for (var i = 0; i < potInputs.length; i++) {
                
                if(potInputs[i].selected == true){
                    
                    if(newStatus == 'Open'){
                    	
                        if(potInputs[i].record.Complete__c == true){
                    
                            potInputs[i].record.Complete__c = false;
                            potInputs[i].record.Approved__c = false;
                            potInputs[i].record.Approved_2__c = false;                            
                            potInputs[i].changed = true;
                            hasChange = true;
                        }
                    
                    }else if(newStatus == 'Complete'){
                        
                        if(potInputs[i].record.Complete__c != true || potInputs[i].record.Approved__c == true){
                    
                            potInputs[i].record.Complete__c = true;
                            potInputs[i].record.Approved__c = false;
                            potInputs[i].record.Approved_2__c = false;                            
                            potInputs[i].changed = true;
                            hasChange = true;
                            
                            if(isNaN(parseInt(potInputs[i].record.Potential__c)) && potInputs[i].record.Last_12_mth_Sales__c != null) numberEmpty++;
                        }
                        
                    }else if(newStatus == 'Approved1'){
                        
                        if(potInputs[i].record.Approved__c != true || potInputs[i].record.Approved_2__c == true){
                    
                            potInputs[i].record.Complete__c = true;
                            if(potInputs[i].record.Approver_1__c != null) potInputs[i].record.Approved__c = true;
                            potInputs[i].record.Approved_2__c = false;                            
                            potInputs[i].changed = true;
                            hasChange = true;
                            
                            if(isNaN(parseInt(potInputs[i].record.Potential__c)) && potInputs[i].record.Last_12_mth_Sales__c != null) numberEmpty++;
                        }
                        
                    }else if(newStatus == 'Approved2'){
                        
                        if(potInputs[i].record.Approved_2__c != true){
                    
                            potInputs[i].record.Complete__c = true;
                            if(potInputs[i].record.Approver_1__c != null) potInputs[i].record.Approved__c = true;
                            if(potInputs[i].record.Approver_2__c != null) potInputs[i].record.Approved_2__c = true;                            
                            potInputs[i].changed = true;
                            hasChange = true;
                            
                            if(isNaN(parseInt(potInputs[i].record.Potential__c)) && potInputs[i].record.Last_12_mth_Sales__c != null) numberEmpty++;
                        }
                    }
                }
                
                potInputs[i].selected = false;
            }
            
            component.set("v.newStatus", null);
            if(hasChange) component.set("v.hasChange", true);
            
            component.set("v.selectedAll", false);
            component.set("v.potentialInputs", potInputs);
            component.set("v.showLoading", false);
            helper.selectRow(component);
            
            if(numberEmpty > 0) alert('You have marked as completed or approved' + numberEmpty + ' Procedure' + (numberEmpty > 1 ? 's' : '') +' with Sales but no Potential provided.');
            
        }), 100);
    },     
    copyLY : function(component, event, helper){
		
        component.set("v.showLoading", true);
        
        setTimeout($A.getCallback(function(){
            
            var potInputs = component.get("v.potentialInputs");
            var hasChange = false;
            
            for (var i = 0; i < potInputs.length; i++) {
                  
                var procedureInput = potInputs[i];
                
                if(procedureInput.selected == true && procedureInput.completed != true){
                    
                    if(procedureInput.competitors.length > 0){
                        
                        var procedureTotal = 0;
                        
                        if(procedureInput.prevRecord != null && procedureInput.prevRecord.Potential_Medtronic__c != null && procedureInput.prevRecord.Potential_Medtronic__c != procedureInput.record.Medtronic_Potential__c){
                            
                            procedureInput.record.Potential_Medtronic__c = procedureInput.prevRecord.Potential_Medtronic__c;
                            procedureInput.changed = true;
                            hasChange = true;                            
                        }
                        
                        if(!isNaN(parseInt(procedureInput.record.Potential_Medtronic__c))) procedureTotal += parseInt(procedureInput.record.Potential_Medtronic__c);
                        
                        for(var k = 0; k < procedureInput.competitors.length; k++){
                            
                            var procCompetitor = procedureInput.competitors[k];
                            
                            if(procCompetitor.prevRecord != null && procCompetitor.prevRecord.Potential__c != null && procCompetitor.prevRecord.Potential__c != procCompetitor.record.Potential__c){
                                
                                procCompetitor.record.Potential__c = procCompetitor.prevRecord.Potential__c;
                                procCompetitor.changed = true;
                                hasChange = true;
                            }
                            
                            if(!isNaN(parseInt(procCompetitor.record.Potential__c))) procedureTotal += parseInt(procCompetitor.record.Potential__c);
                        }
                        
                        if(procedureTotal > 0) procedureInput.record.Potential__c = procedureTotal;
                        else procedureInput.record.Potential__c = null;
                        
                    }else{
                        
                        if(procedureInput.prevRecord != null && procedureInput.prevRecord.Potential__c != null && procedureInput.prevRecord.Potential__c != procedureInput.record.Medtronic_Potential__c){
                            
                            procedureInput.record.Potential__c = procedureInput.prevRecord.Potential__c;
                            procedureInput.changed = true;
                            hasChange = true;                            
                        }
                    }
                    
                    if(!isNaN(parseInt(procedureInput.record.Potential__c)) && parseInt(procedureInput.record.Potential__c) > 0 &&  procedureInput.record.Last_12_mth_Sales__c != null){
                        
                        var percentage = (procedureInput.record.Last_12_mth_Sales__c / procedureInput.record.Potential__c) * 100;
                        procedureInput.marketShare = (Math.round(percentage * 100) / 100);
                        
                    }else{
                        
                        procedureInput.marketShare = null;
                    }                    
                }
                
                procedureInput.selected = false;
            }
            
            if(hasChange) component.set("v.hasChange", true);
            
            component.set("v.selectedAll", false);
            component.set("v.potentialInputs", potInputs);
            component.set("v.showLoading", false);
        }), 100);
    },    
    saveInputs : function(component, event, helper){
        
        var potInputs = component.get("v.potentialInputs");
                   
        helper.saveInputs(component, potInputs);        
    }
})