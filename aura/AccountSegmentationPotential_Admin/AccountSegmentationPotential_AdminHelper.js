({
	resize : function (component, event, helper){
  		
        if(component.isValid()){
        
            var timer = component.get('v.timer');
            clearTimeout(timer);
        
            var timer = setTimeout(function(){
                                
                clearTimeout(timer);
                component.set('v.timer', null);
                
                helper.setSizes(component);
                
            }, 500);
    
            component.set('v.timer', timer);      
        }
    },
    setSizes : function(component){
        
        var containerHeight = component.find("mainContainer").getElement().getBoundingClientRect().height;
        
        var tableHeight = containerHeight - 200;
        
        component.set("v.tableMaxHeight", tableHeight);       
    },
    getFilters : function(component, event, helper, inputFilters) {
		
        var fiscalYear = component.get("v.filterFiscalYear");
        var sbu = component.get("v.filterSBU");
        var procedure = component.get("v.filterProcedure");
        var segment = component.get("v.filterSegment");        
        var country = component.get("v.filterCountry");      
        var account = component.get("v.filterAccount");
        var accountId = (account != null ? account.Id : null);
                
        var action = component.get("c.getFilterOptions");
        action.setParams({fiscalYear : fiscalYear, sbu : sbu, procedure : procedure, segment : segment, country : country, accountId : accountId});
        
        component.set("v.showLoading", true);
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var result = response.getReturnValue();                  
                
                component.set("v.fiscalYearOptions" , result['fiscalYearOptions']);
                component.set("v.readOnlyFY" , result['readOnlyFY']);
                
                var sbuOptions = result['sbuOptions'];                
                component.set("v.sbuOptions", sbuOptions);
                if(this.filterInOptions(sbu, sbuOptions) == false) component.set("v.filterSBU", "");
                
                var procedureOptions = result['procedureOptions'];  
                component.set("v.procedureOptions", procedureOptions);
                if(this.filterInOptions(procedure, procedureOptions) == false) component.set("v.filterProcedure", "");
                
                var segmentOptions = result['segmentOptions'];  
                component.set("v.segmentOptions", segmentOptions);
                if(this.filterInOptions(segment, segmentOptions) == false) component.set("v.filterSegment", "");
                
                var countryOptions = result['countryOptions'];  
                component.set("v.countryOptions", countryOptions);
                if(this.filterInOptions(country, countryOptions) == false) component.set("v.filterCountry", "");
                
                if(fiscalYear == null && result['fiscalYear'].length > 0) component.set("v.filterFiscalYear", result['fiscalYear'][0].value);
                
                this.getCount(component, event, helper, inputFilters);               
                
            }else if (state === "ERROR") {
                
                var errors = response.getError();
                
                if (errors && errors[0] && errors[0].message) {
                             
                    var showError = $A.get("e.c:Show_Error_Toast");
                    showError.setParams({ "message" : errors[0].message });
                    showError.fire();                        
                    
                } else {
                    alert("Unknown error");
                }
            }            
        });
        
		$A.getCallback(function() {
        	$A.enqueueAction(action);
    	})();
	},
    filterInOptions : function(value, options) {
        
        if(value == null || value == '') return true;
        
        for(var i = 0; i < options.length; i++){
            
            if(value == options[i].value) return true;
        }
        
        return false;
    },
    getCount : function(component, event, helper, inputFilters) {
		
        var fiscalYear = component.get("v.filterFiscalYear");
        var sbu = component.get("v.filterSBU");
        var procedure = component.get("v.filterProcedure");
        var country = component.get("v.filterCountry");
        var account = component.get("v.filterAccount");
        var accountId = (account != null ? account.Id : null);        
        var segmentation = component.get("v.filterSegment");
        var assignedTo = component.get("v.filterAssignedTo");
        var assignedToId = (assignedTo != null ? assignedTo.Id : null);
        var status = component.get("v.filterStatus");
        
        if(fiscalYear == null || fiscalYear == ''){
            
            component.set("v.recordCount", null);
            return;
        }
        
        var action = component.get("c.getRecordCount");
        action.setParams({ fiscalYear : fiscalYear, sbu : sbu, procedure : procedure, country : country, accountId : accountId,  segmentation : segmentation, assignedTo : assignedToId, status : status});
        
        component.set("v.showLoading", true);
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var count = response.getReturnValue();
                component.set("v.recordCount", count);
                
                if(inputFilters && count < 1000){
                    
                    var getRecordsAction = component.get("c.getRecords");
                    $A.enqueueAction(getRecordsAction);                
                }
                
            }else if (state === "ERROR") {
                
                var errors = response.getError();
                
                if (errors && errors[0] && errors[0].message) {
                             
                    var showError = $A.get("e.c:Show_Error_Toast");
                    showError.setParams({ "message" : errors[0].message });
                    showError.fire();                        
                    
                }else alert("Unknown error");                
            }
            
            component.set("v.showLoading", false);
        });
        
		$A.getCallback(function() {
        	$A.enqueueAction(action);
    	})();
	},
    getPotentialInputs : function (component, event, helper){
    	
        var fiscalYear = component.get("v.filterFiscalYear");
        var sbu = component.get("v.filterSBU");
        var procedure = component.get("v.filterProcedure");
        var country = component.get("v.filterCountry");
        var account = component.get("v.filterAccount");
        var accountId = (account != null ? account.Id : null);        
        var segmentation = component.get("v.filterSegment");
        var assignedTo = component.get("v.filterAssignedTo");
        var assignedToId = (assignedTo != null ? assignedTo.Id : null);
        var status = component.get("v.filterStatus");
        
        var action = component.get("c.getUserPotentialInputs");
        
        component.set("v.showLoading", true);
        action.setParams({fiscalYear : fiscalYear, sbu : sbu, procedure : procedure, country : country, accountId : accountId,  segmentation : segmentation, assignedTo : assignedToId, status : status});
        
        action.setCallback(this, function(response) {
    	
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var potInputs = response.getReturnValue();
                
				var hasCompetitors = false;
                var hasSales = false;
                var hasRevenue = false;
                var hasUnits = false;
                var hasCurrentSegmentation = false;
                var revenueCurrencies = {};
                
                var readOnlyFY = component.get("v.readOnlyFY");
                var isReadOnly = this.filterInOptions(fiscalYear, readOnlyFY);
                
                for (var i = 0; i < potInputs.length; i++){
                    
                    var procedureInput = potInputs[i];
                    
                    procedureInput.disabled = isReadOnly;
                    if(procedureInput.currentSegmentation != null && procedureInput.currentSegmentation.Size_Segment__c != null) hasCurrentSegmentation = true;
                    
                    if(!isNaN(parseInt(procedureInput.record.Potential__c)) && parseInt(procedureInput.record.Potential__c) > 0 &&  procedureInput.record.Last_12_mth_Sales__c != null){
                        
                        var percentage = (procedureInput.record.Last_12_mth_Sales__c / procedureInput.record.Potential__c) * 100;
                        procedureInput.marketShare = (Math.round(percentage * 100) / 100);
                        
                    }else{
                        
                        procedureInput.marketShare = null;
                    }
                    
                    if(procedureInput.competitors.length > 0) hasCompetitors = true;
                    if(procedureInput.record.Last_12_mth_Sales__c != null) hasSales = true;
                    if(procedureInput.record.Segmentation_Potential_Procedure__r.Registration_Unit__c == 'REVENUE'){
                        
                        hasRevenue = true; 
                        revenueCurrencies[procedureInput.record.CurrencyIsoCode] = procedureInput.record.CurrencyIsoCode;
                        
                    }else if(procedureInput.record.Segmentation_Potential_Procedure__r.Registration_Unit__c == 'UNITS') hasUnits = true;             
                }
                
                var sortOnPotential = false;
                
                if(hasUnits = true && hasRevenue == false) sortOnPotential = true;        
                else if(hasUnits == false && hasRevenue == true && Object.keys(revenueCurrencies).length == 1) sortOnPotential = true;
                
                component.set("v.hasCompetitors" , hasCompetitors);
                component.set("v.hasSales" , hasSales);
                component.set("v.hasCurrentSegmentation" , hasCurrentSegmentation);
                component.set("v.sortOnPotential" , sortOnPotential);
                component.set("v.selectedAll", false);
                
                helper.applySorting(component, potInputs);
                
                component.set("v.potentialInputs", potInputs);                
                component.set("v.hasChange", false);
                component.set("v.hasFilterChange" , false);
                
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    
                    var showError = $A.get("e.c:Show_Error_Toast");
                    showError.setParams({ "message" : errors[0].message });
                    showError.fire();                        
                    
                } else {
                    alert("Unknown error");
                }
            }
            
            component.set("v.showLoading", false);            
        });
        
		$A.getCallback(function() {
        	$A.enqueueAction(action);
    	})();    		
    },
    applySorting : function (component, potInputs){
        
        var sortBy = component.get("v.sortBy");
        var sortDir = component.get("v.sortDir");
        
        var hasSales = component.get("v.hasSales");
        var sortOnPotential = component.get("v.sortOnPotential");
        var filterProcedure = component.get("v.filterProcedure");
        var hasCurrentSegmentation = component.get("v.hasCurrentSegmentation");
        
        if((hasSales == false && sortBy == 'MarketShare') || 
           (hasCurrentSegmentation == false && sortBy == 'AccountNewSegment') ||
           ((sortOnPotential == false || filterProcedure == null || filterProcedure.length == 0) && (sortBy == 'LastSales' || sortBy == 'LYPotential' || sortBy == 'NYPotential'))){
            
            sortBy = 'AccountName';
            sortDir = 'ASC'
            
            component.set("v.sortBy", sortBy);
            component.set("v.sortDir", sortDir);
        }
        
        var segmentMap = {"-" : 0, "XS" : 1, "S" : 2, "M" : 3, "L" : 4, "XL" : 5};
        
        potInputs.sort(function(a, b){
            
            if(a.show == false && b.show == true) return 1;
            if(a.show == true && b.show == false) return -1;
            if(a.show == false && b.show == false) return 0;
                        
            var result;
            
            if(sortBy == "AccountName"){
            	
                result = a.account.accountName.toLowerCase().localeCompare(b.account.accountName.toLowerCase());
                
                if(sortDir == 'DESC') result = 0 - result;
            
            }else if(sortBy == "AccountSegment"){
            	
                if(a.account.accountSegment == "-" && b.account.accountSegment != "-") result = 1;
                else if(a.account.accountSegment != "-" && b.account.accountSegment == "-") result = -1;
                else if(a.account.accountSegment == "-" && b.account.accountSegment == "-") result = 0;
                else{
                	result = segmentMap[a.account.accountSegment] - segmentMap[b.account.accountSegment];
                    if(sortDir == 'DESC') result = 0 - result;
                }
                
            }else if(sortBy == "AccountNewSegment"){
            	
                if(a.currentSegmentation.Size_Segment_Override__c == "-" && b.currentSegmentation.Size_Segment_Override__c != "-") result = 1;
                else if(a.currentSegmentation.Size_Segment_Override__c != "-" && b.currentSegmentation.Size_Segment_Override__c == "-") result = -1;
                else if(a.currentSegmentation.Size_Segment_Override__c == "-" && b.currentSegmentation.Size_Segment_Override__c == "-") result = 0;
                else{
                	result = segmentMap[a.currentSegmentation.Size_Segment_Override__c] - segmentMap[b.currentSegmentation.Size_Segment_Override__c];
                    if(sortDir == 'DESC') result = 0 - result;
                }
                
            }else if(sortBy == "MarketShare"){
            	
                if(a.marketShare == null && b.marketShare != null) result = 1;
                else if(a.marketShare != null && b.marketShare == null) result = -1;
                else if(a.marketShare == null && b.marketShare == null) result = 0;
                else{
                    result = a.marketShare - b.marketShare;                
                	if(sortDir == 'DESC') result = 0 - result;
                }
                
            }else if(sortBy == "LastSales"){
            	
                if(a.record.Last_12_mth_Sales__c == null && b.record.Last_12_mth_Sales__c != null) result = 1;
                else if(a.record.Last_12_mth_Sales__c != null && b.record.Last_12_mth_Sales__c == null) result = -1;
                else if(a.record.Last_12_mth_Sales__c == null && b.record.Last_12_mth_Sales__c == null) result = 0;
                else{
                    result = a.record.Last_12_mth_Sales__c - b.record.Last_12_mth_Sales__c;                
                	if(sortDir == 'DESC') result = 0 - result;
                }
                
            }else if(sortBy == "LYPotential"){
            	
                if((a.prevRecord == null || a.prevRecord.Potential__c == null) && b.prevRecord != null && b.prevRecord.Potential__c != null) result = 1;
                else if(a.prevRecord != null && a.prevRecord.Potential__c != null && (b.prevRecord == null || b.prevRecord.Potential__c == null)) result = -1;
                else if((a.prevRecord == null || a.prevRecord.Potential__c == null) && (b.prevRecord == null || b.prevRecord.Potential__c == null)) result = 0;
                else{
                    result = a.prevRecord.Potential__c - b.prevRecord.Potential__c;                
                	if(sortDir == 'DESC') result = 0 - result;
                }
                                
            }else if(sortBy == "NYPotential"){
            	
                if(a.record.Potential__c == null && b.record.Potential__c != null) result = 1;
                else if(a.record.Potential__c != null && b.record.Potential__c == null) result = -1;
                else if(a.record.Potential__c == null && b.record.Potential__c == null) result = 0;
                else{
                    result = a.record.Potential__c - b.record.Potential__c;                
                	if(sortDir == 'DESC') result = 0 - result;
                }                
                
            }else if(sortBy == "Status"){
            	
                if(a.record.Complete__c == false){
                    
                    if(b.record.Complete__c == true) result = -1;
                    else{
                        
                        if((a.record.Approver_1__c == null && b.record.Approver_1__c != null) || (a.record.Approver_2__c == null && b.record.Approver_2__c != null)) result = -1;
                        else if((a.record.Approver_1__c != null && b.record.Approver_1__c == null) || (a.record.Approver_2__c != null && b.record.Approver_2__c == null)) result = 1;
                        else result = 0;                        
                    }
                    
                }else if(a.record.Approver_1__c == null){
                    
                    if(b.record.Complete__c == false) result = 1;
                    else{
                        
                        if((b.record.Approver_1__c != null && b.record.Approved__c == false) || (b.record.Approver_2__c != null && b.record.Approved_2__c == false)) result = 1;
                        else if(b.record.Approver_1__c != null) result = -1;
                    	else result = 0;
                    }
                    
                }else if(a.record.Approved__c == false){
                    
                    if(b.record.Complete__c == false) result = 1;
                    else{
                        
                        if(b.record.Approver_1__c == null || b.record.Approved__c == true) result = -1;   
                        else if(a.record.Approver_2__c == null){
                            
                            if(b.record.Approver_2__c != null) result = -1;
                            else result = 0;
                            
                        }else{
                        	
                            if(b.record.Approver_2__c == null) result = 1;
                            else result = 0;
                        }                    	
                    }                    
                    
                }else if(a.record.Approver_2__c == null){
                    
                    if(b.record.Complete__c == false) result = 1;
                    else{
                        
                        if(b.record.Approved__c == false || (b.record.Approver_2__c != null && b.record.Approved_2__c == false)) result = 1;
                        else if(b.record.Approver_2__c != null) result = -1;
                    	else result = 0;
                    }
                    
                }else if(a.record.Approved_2__c == false){
                    
                    if(b.record.Complete__c == false || (b.record.Approver_1__c != null && b.record.Approved__c == false)) result = 1;
                    else{
                        
                        if(b.record.Approver_1__c == null || b.record.Approver_2__c == null || a.record.Approved_2__c == true) result = -1                        
                    	else result = 0;
                    }                 
                    
                }else{
                    
                    if(b.record.Complete__c == false || b.record.Approved__c == false || b.record.Approved_2__c == false) result = 1;
                    else result = 0; 
                }
                
                if(sortDir == 'DESC') result = 0 - result;
            }
            
            if(result == 0) result = a.account.accountName.toLowerCase().localeCompare(b.account.accountName.toLowerCase());
            if(result == 0) result = a.account.accountId.localeCompare(b.account.accountId);
            if(result == 0) result = a.record.Segmentation_Potential_Procedure__r.Name.localeCompare(b.record.Segmentation_Potential_Procedure__r.Name);                
            
            return result;        
        });  
                
        var procedures = 0;
        
        for (var i = 0; i < potInputs.length; i++) {
        	
            potInputs[i].oddRow = ((procedures % 2) == 1);
            potInputs[i].index = procedures;
            procedures++;
        }
        
        return potInputs;        
    },
    saveInputs : function(component, potInputs) {
		        
        var action = component.get("c.savePotentials");
        action.setParams({potentials : potInputs});
        
        component.set("v.showLoading", true);
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var oldInputs = potInputs;
                var newInputs = response.getReturnValue();
                
                for (var i = 0; i < oldInputs.length; i++) {
                        
                    oldInputs[i].record.Id = newInputs[i].record.Id;
                    oldInputs[i].completedTS = newInputs[i].completedTS;
                    oldInputs[i].approved1TS = newInputs[i].approved1TS;
                    oldInputs[i].approved2TS = newInputs[i].approved2TS;                    
                    oldInputs[i].potentialTS = newInputs[i].potentialTS;
                    oldInputs[i].potentialMDTTS = newInputs[i].potentialMDTTS;
                    oldInputs[i].changed = false;
                    
                    for(var j = 0; j < oldInputs[i].competitors.length; j++){
                        
                        oldInputs[i].competitors[j].record.Id = newInputs[i].competitors[j].record.Id;
                        oldInputs[i].competitors[j].potentialTS = newInputs[i].competitors[j].potentialTS;
                        oldInputs[i].competitors[j].changed = false;
                    }
                }
                
                component.set("v.potentialInputs", oldInputs);
                component.set("v.hasChange", false);
                
                var refreshEvent = $A.get("e.c:Segmentation_Overview_Refresh");
        		refreshEvent.setParams({ "data_type" : "Potential" });
        		refreshEvent.fire();
                
            }else if (state === "ERROR") {
                
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    
                    var showError = $A.get("e.c:Show_Error_Toast");
                    showError.setParams({ "message" : errors[0].message });
                    showError.fire();                        
                    
                } else {
                    alert("Unknown error");
                }
            }
            
            component.set("v.showLoading", false);
        });
        
		$A.getCallback(function() {
        	$A.enqueueAction(action);
    	})();
	},
    applyInputFilters : function(component, filters) {
    	
        component.set("v.filterAccount", null);        
    	component.set("v.filterStatus", null);        
        component.set("v.filterCompetitor", "Show");
        
        var filterList = filters.split(":");
        
        component.set("v.filterSBU", filterList[0]);
        
    	if(filterList.length > 1) component.set("v.filterProcedure", filterList[1]);
        else component.set("v.filterProcedure", null);
        
        if(filterList.length > 2){
            
            var segmentation = filterList[2];
            if(segmentation == 'Not Segmented') segmentation = '-';
            
            component.set("v.filterSegment", segmentation);
            
        }else component.set("v.filterSegment", null);
                
        if(filterList.length > 3 && filterList[3].split(';')[0] != null && filterList[3].split(';')[0] != ''){
            var assignedTo = {sobjectType : 'User', Id : filterList[3].split(';')[0], Name : filterList[3].split(';')[1]};
			component.set("v.filterAssignedTo", assignedTo);
        }else component.set("v.filterAssignedTo", "{sobjectType:'User'}");
                
        if(filterList.length > 4) component.set("v.filterCountry", filterList[4]);
        else component.set("v.filterCountry", '');
        
        if(filterList.length > 5) component.set("v.filterFiscalYear", filterList[5]);
        else component.set("v.filterFiscalYear", '');        
    },
    selectRow : function(component){
        
        var potInputs = component.get("v.potentialInputs");
		var hasSelection = false;        
        var hasLevel1 = false;
        var hasLevel2 = false;
        
        for (var i = 0; i < potInputs.length; i++) {
            
            if(potInputs[i].selected == true){
                
                hasSelection = true;
                if(potInputs[i].record.Approver_1__c != null) hasLevel1 = true;
                if(potInputs[i].record.Approver_2__c != null) hasLevel2 = true;
            }    		
        }
		
        var newStatusOptions = [                
            { value: "Open", label: "Open" },
            { value: "Complete", label: "Complete" }            
        ];
        
        if(hasLevel1 == true) newStatusOptions.push({value: "Approved1", label: "Approved level 1"});
        if(hasLevel2 == true) newStatusOptions.push({ value: "Approved2", label: "Approved level 2"});
        
        component.set("v.newStatusOptions", newStatusOptions);        
		component.set("v.hasSelection", hasSelection);
    }
})