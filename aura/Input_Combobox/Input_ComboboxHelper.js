({
	addClass : function (element, className) {
        //Global Aura util method for adding a style class from an aura element
        $A.util.addClass(element,className);
    }, 
    
    removeClass : function (element , className) {
        //Global Aura util method for removing a style class from an aura element
        $A.util.removeClass(element,className);
    }
})