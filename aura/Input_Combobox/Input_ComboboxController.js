({
    doInit : function(component, event, helper) {
    	        
        var selectedValue = component.get("v.selectedValue");
        
        if(selectedValue != null){
        	
            var selectedOption;
            
            var allOptions = component.get("v.allOptions");
            for(var i = 0; i < allOptions.length; i++){
                
                if(allOptions[i].value == selectedValue){
                    
                    selectedOption = allOptions[i];
                    break;
                }
            }
        
        	if(selectedOption != null) component.set("v.selectedOption", selectedOption);
            else component.set("v.selectedValue", null);
        }
    },
    onLeaveLookupPane : function(component, event, helper) {
        
        var closeTimer = setTimeout($A.getCallback(function(){
        	
            if(component.isValid()){
            
                component.find("searchContent").set("v.value","");
                
                var lookupContainerCmp = component.find("lookUpPane");
                helper.removeClass(lookupContainerCmp,'slds-is-open');
                helper.addClass(lookupContainerCmp,'slds-is-close');    
            }
        }), 500);
        
        component.set('v.closeTimer', closeTimer);   
    },
    remove : function (component, event, helper) {
        
        component.set("v.selectedOption", {});
        component.set("v.selectedValue", null);
        
        var onSelectAction = component.get("v.onSelect");            
        if(onSelectAction != null) $A.enqueueAction(onSelectAction);
    },
    onInputChange : function (component, event, helper) {
        
		var timer = component.get('v.timer');
        clearTimeout(timer);
    
        var timer = setTimeout($A.getCallback(function(){
			
        	var searchContent = component.get("v.searchText");             	
            var allOptions = component.get("v.allOptions");
			var filteredOptions = [];
            
            if (searchContent && searchContent.trim().length > 0 ) {
                   	
                component.set("v.issearching", true);
                
                searchContent = searchContent.toLowerCase();                
                
                for(var i = 0; i < allOptions.length; i++){
                    
                    if(allOptions[i].label.toLowerCase().includes(searchContent) || 
                       (allOptions[i].city != null && allOptions[i].city.toLowerCase().includes(searchContent)) || 
                       (allOptions[i].SAP_Id != null && allOptions[i].SAP_Id.toLowerCase().includes(searchContent))
                      ) filteredOptions.push(allOptions[i]);
                }
                                
            }else{
                
                filteredOptions = allOptions;               
            } 
            
            component.set("v.filteredOptions", filteredOptions);
                
            var lookupContainerCmp = component.find("lookUpPane");
            
            helper.addClass(lookupContainerCmp,'slds-is-open');
            helper.removeClass(lookupContainerCmp,'slds-is-close');     
            
            component.set("v.issearching", false);
            
            clearTimeout(timer);
            component.set('v.timer', null);
            
     	}), 300);

        component.set('v.timer', timer);            
    },    
    selectedRecordRowClick : function (component, event, helper) {
        
        var closeTimer = component.get('v.closeTimer');
        clearTimeout(closeTimer);
        
        var targetSource = event.currentTarget;
        var selectedIndex = targetSource.dataset.index;
        
        var listedRecords = component.get("v.filteredOptions");
        
        if (listedRecords && listedRecords.length > 0 && +selectedIndex >=0) {
            
            var selectedRecord = listedRecords[selectedIndex];
            component.set("v.selectedOption", selectedRecord);
            component.set("v.selectedValue", selectedRecord.value);
            
            //Search input control value resets
            component.find("searchContent").set("v.value",""); 
            
            //Hide the lookup
            var lookupContainerCmp = component.find("lookUpPane");
            helper.removeClass(lookupContainerCmp,'slds-is-open');
            helper.addClass(lookupContainerCmp,'slds-is-close'); 
            
            var onSelectAction = component.get("v.onSelect");             
            if(onSelectAction != null) $A.enqueueAction(onSelectAction);
        }
    }
})