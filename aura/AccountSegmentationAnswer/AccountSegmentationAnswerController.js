({
	doInit : function(component, event, helper){
		
        var questions = component.get("v.questions");
        var questionIndex = component.get("v.questionIndex");
        
        var question = questions[questionIndex];
        component.set("v.question", question);
        
        var readOnly = component.get("v.readOnly");
        
        if(readOnly == true) return;
        
        var answer = component.get("v.questionAnswer");
        
        if(answer.record.Answer__c != null && answer.record.Answer__c != "") component.set("v.selectedPickListMulti", answer.record.Answer__c.split(";"));
        else component.set("v.selectedPickListMulti", []);
	},
    openPickListMulti : function(component, event, helper){
		
      	component.set("v.showModelPickListMulti", true);
	},
    numericOnly : function(component, event) {
        
        var numbers = '-0123456789';
        if (numbers.indexOf(event.key) < 0){
        	event.preventDefault()
    	}
    },
    cancel : function(component, event, helper){
        
        var answer = component.get("v.questionAnswer");
        
        if(answer.record.Answer__c != null && answer.record.Answer__c != "") component.set("v.selectedPickListMulti", answer.record.Answer__c.split(";"));
        else component.set("v.selectedPickListMulti", []);
        
        component.set("v.showModelPickListMulti", false);
    },
    use : function(component, event, helper){
        
        var values = component.get("v.selectedPickListMulti");
        var answer = component.get("v.questionAnswer");
        
        var newAnswer;
        if(values.length == 0) newAnswer = null;
        else newAnswer = values.join(";");   
        
        if(newAnswer != answer.record.Answer__c){
        	
            answer.record.Answer__c = newAnswer
			answer.changed = true;
        	
            var hasChange = component.get("v.hasChange");
            if(hasChange == false) component.set("v.hasChange", true);
            component.set("v.questionAnswer", answer);    
        }
        
        component.set("v.showModelPickListMulti", false);
    },
    valueChanged : function(component, event, helper){
        
        var answer = component.get("v.questionAnswer");
        var question = component.get("v.question");
        var hasChange = component.get("v.hasChange");
        
        var newValue = answer.record.Answer__c;
        var error;
        
        if(parseInt(question.min_Value) != NaN && newValue < parseInt(question.min_Value)){
                        
            answer.record.Answer__c = null;
            error = "Value must be bigger than " + question.min_Value;
            
        }else if(parseInt(question.max_Value) != NaN && newValue > parseInt(question.max_Value)){
            
            answer.record.Answer__c = null;
            error =  "Value must be smaller than " + question.max_Value;            
        }
        
        if(error != null){
        	
            component.set("v.fieldError" , error);
            $A.util.toggleClass(component.find("number-error"), 'slds-hide');
        	setTimeout($A.getCallback(function(){ $A.util.toggleClass(component.find("number-error"), 'slds-hide'); }), 3000);
        }
        
        answer.changed = true;
        if(hasChange == false) component.set("v.hasChange", true);
        component.set("v.questionAnswer", answer);
    }
})