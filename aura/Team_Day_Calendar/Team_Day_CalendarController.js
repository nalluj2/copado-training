({
	doInit : function(component, event, helper) {
		
        window.addEventListener("resize",function(e){
  			helper.resize(component, event, helper);
		});
        
        helper.getCalendarData(component, event, helper, false); 
	},    
    nextDay : function(component, event, helper) {
		
        var today = new Date(component.get("v.pickDay"));        
        today.setTime(today.getTime() + (25*60*60000));               
        component.set("v.pickDay", today.toISOString().split('T')[0] );       
        helper.getCalendarData(component, event, helper);
	},
    previousDay : function(component, event, helper) {
		
        var today = new Date(component.get("v.pickDay"));        
        today.setTime(today.getTime() - (23*60*60000));      
        component.set("v.pickDay", today.toISOString().split('T')[0] );        
        helper.getCalendarData(component, event, helper);
    },           
    refresh : function(component, event, helper){
        if(component.get("v.teamId") != null) helper.getCalendarData(component, event, helper, false);
    },
    includeTravelTime : function(component, event, helper){
        if(component.get("v.teamId") != null) helper.getCalendarData(component, event, helper, true);
    },
    openActivitySelector : function(component, event, helper){  
    	
        var inputParam = event.currentTarget.id;
        
        var memberId = inputParam.split("-")[0];
        var time = inputParam.split("-")[1];        
        var hour = time.split(":")[0];
        var minute = time.split(":")[1];
        
        var pickDay = new Date(component.get("v.pickDay"));        
        var selectedDate = new Date(pickDay.getFullYear(), pickDay.getMonth(), pickDay.getDate(), hour, minute, 0, 0);
        
        component.set("v.selectedDate", selectedDate);
        component.set("v.selectedMember", memberId);
        
        component.set("v.posX", event.clientX - 100);
        component.set("v.posY",event.clientY - 60);
        
        component.find("activitySelector").set("v.visible", true);
        
    },
    newActivitySelected : function(component, event, helper){  
    
    	var selectedActivity = event.getParam("value");
        var selectedDate = component.get("v.selectedDate");
        var selectedMember = component.get("v.selectedMember");
        if(selectedMember == 'unassigned') selectedMember = null;
        
        var appEvent = $A.get("e.c:New_Activity_Selector");
        appEvent.setParams({selectedDate : selectedDate, selectedMember : selectedMember, activityType : selectedActivity});
		appEvent.fire();
    },
    applyHighlight : function(component, event, helper){
        
        helper.applyHighlight(component);
    },
    openHighlight : function(component, event, helper){
        
        component.set("v.showHighLightPanel", true);
    }
})