({
	handleOnClick : function(component, event, helper) {
        
        var isChrome = navigator.appVersion.indexOf("Chrome/") != -1;
            
        if(!isChrome) $A.util.toggleClass(component.find("divHelp"), 'slds-hide');        
  	},
  	handleOnBlur : function(component, event, helper) {
        
        var isChrome = navigator.appVersion.indexOf("Chrome/") != -1;
        
        if(!isChrome) $A.util.addClass(component.find("divHelp"), 'slds-hide');        
  	},
  	handleMouseEnter : function(component, event, helper) {
        
        var isChrome = navigator.appVersion.indexOf("Chrome/") != -1;
        
        if(isChrome) $A.util.removeClass(component.find("divHelp"), 'slds-hide');        
  	},
  	handleMouseLeave : function(component, event, helper) {
        
        var isChrome = navigator.appVersion.indexOf("Chrome/") != -1;
        
        if(isChrome) $A.util.addClass(component.find("divHelp"), 'slds-hide');        
  	}
})