({    
    doInit : function(component, event, helper) {
		
        window.addEventListener("resize",function(e){
  			helper.resize(component, event, helper);
		});
        
        helper.getCalendarData(component, event, helper);
	},
    nextWeek : function(component, event, helper) {
		
        var pickDay = new Date(component.get("v.pickDay"));        
        pickDay.setDate(pickDay.getDate() + 8);
        component.set("v.pickDay", pickDay.toISOString().split('T')[0]);
        helper.getCalendarData(component, event, helper);
	},
    previousWeek : function(component, event, helper) {
		
        var pickDay = new Date(component.get("v.pickDay"));        
        pickDay.setDate(pickDay.getDate() - 6);        
        component.set("v.pickDay", pickDay.toISOString().split('T')[0]);   
        helper.getCalendarData(component, event, helper);
	},
    openNewActivitySelector: function(component, event, helper) {
        
        var inputParam = event.currentTarget.id;
        
        var dayOffset = inputParam.split("-")[0];
        var time = inputParam.split("-")[1];        
        var hour = time.split(":")[0];
        var minute = time.split(":")[1];
        
        var pickDay = new Date(component.get("v.pickDay"));
        pickDay.setDate(pickDay.getDate() + parseInt(dayOffset));        
       
        var selectedDate = new Date(pickDay.getFullYear(), pickDay.getMonth(), pickDay.getDate(), hour, minute, 0, 0);
        
        component.set("v.selectedTime", selectedDate);
        component.set("v.posX", event.clientX - 100);
        component.set("v.posY",event.clientY - 80);
        
        component.find("activitySelector").set("v.visible", true);
    },
    newActivitySelected : function(component, event, helper){  
    
    	var selectedActivity = event.getParam("value");
        var selectedDate = component.get("v.selectedTime");
        var selectedMember = null;
        
        var appEvent = $A.get("e.c:New_Activity_Selector");
        appEvent.setParams({selectedDate : selectedDate, selectedMember : selectedMember, activityType : selectedActivity});
		appEvent.fire();
    },
    refresh : function(component, event, helper){
        
        helper.getCalendarData(component, event, helper);
    },
    applyHighlight : function(component, event, helper){
        
        helper.applyHighlight(component);
    },
    openHighlight : function(component, event, helper){
        
        component.set("v.showHighLightPanel", true);
    }
    
})