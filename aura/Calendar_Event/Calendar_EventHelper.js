({
	assignMember : function(component, caseId, memberId) {
		
        component.set("v.showLoading", true);
        
        var action = component.get("c.assignCase");
        action.setParams({ caseId : caseId, memberId : memberId });
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                component.set("v.showLoading", false);
                
                var appEvent = $A.get("e.c:Show_Scheduler");
        		appEvent.setParams({action : "Close"});
				appEvent.fire();
                
                var appEvent = $A.get("e.c:Refresh_Calendar");
				appEvent.fire();
                
            } else if (state === "ERROR") {
                
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
		$A.enqueueAction(action);
	}
})