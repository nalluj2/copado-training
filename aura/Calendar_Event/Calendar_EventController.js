({
	doInit : function(component, event, helper) {
		
        var fullMap = component.get("v.eventMap");
        var key = component.get("v.selectedKey");
        
        component.set("v.events", fullMap[key]);
	},
    editActivity : function(component, event){
        
        var viewOnly = component.get("v.viewOnly");
        
        if(viewOnly == true) return;
        
        var selectedEvent = event.currentTarget.dataset.value.split(":");
		   
        var selectedType;
        var selectedId;
        
        if(selectedEvent[1] == ""){
            
            selectedType = 'event';
            selectedId = selectedEvent[0];
            
        }else{
         
            selectedType = selectedEvent[2];
            selectedId = selectedEvent[1];            
        }
        
        var appEvent = $A.get("e.c:Select_Activity");
        appEvent.setParams({action : "Open", selectedType : selectedType, selectedId : selectedId});
		appEvent.fire();
    },
    assignMember : function(component, event, helper){
    	
        var inputData = event.currentTarget.dataset.value.split(":");
        var memberId = inputData[0];
        var eventId = inputData[1];
        
        helper.assignMember(component, eventId, memberId);
        
	},
    showIcon : function(component, event){
    	
        component.set("v.showPin", true);
    },
    hideIcon : function(component, event){
    	
        component.set("v.showPin", false);
    }
})