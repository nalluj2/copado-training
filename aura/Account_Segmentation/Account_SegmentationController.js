({
	doInit : function(component, event, helper) {
		
        helper.getOverview(component, event, helper);    
        
        window.addEventListener('beforeunload', (event) => {
          
            var hasChangePotentialInput = component.get("v.hasChangePotentialInput");
        	var hasChangePotentialApproval = component.get("v.hasChangePotentialApproval");
            var hasChangeBehavioralInput = component.get("v.hasChangeBehavioralInput");
            var hasChangeBehavioralApproval = component.get("v.hasChangeBehavioralApproval");
            var hasChangeStrategicalInput = component.get("v.hasChangeStrategicalInput");
            var hasChangeStrategicalApproval = component.get("v.hasChangeStrategicalApproval");
            var hasChangePotentialAdmin = component.get("v.hasChangePotentialAdmin");
            var hasChangeStrategicAdmin = component.get("v.hasChangeStrategicAdmin");
            var hasChangeBehavioralAdmin = component.get("v.hasChangeBehavioralAdmin");
            
            if(hasChangePotentialInput || hasChangePotentialApproval || hasChangePotentialAdmin ||
            	hasChangeBehavioralInput || hasChangeBehavioralApproval || hasChangeBehavioralAdmin ||
            	hasChangeStrategicalInput || hasChangeStrategicalApproval || hasChangeStrategicAdmin){
          		
            	event.preventDefault();          		
          		event.returnValue = 'You have unsaved changes. Do you want to continue?';
            	return 'You have unsaved changes. Do you want to continue?';
        	}
       });
	},
    handleSelect : function(component, event, helper) {
		
        var selectedValue = event.getParam("value");
        
        if(selectedValue == 'exit'){
        	
            var hasChangePotentialInput = component.get("v.hasChangePotentialInput");
        	var hasChangePotentialApproval = component.get("v.hasChangePotentialApproval");
            var hasChangePotentialAdmin = component.get("v.hasChangePotentialAdmin");
            var hasChangeBehavioralInput = component.get("v.hasChangeBehavioralInput");
            var hasChangeBehavioralApproval = component.get("v.hasChangeBehavioralApproval");
            var hasChangeBehavioralAdmin = component.get("v.hasChangeBehavioralAdmin");
            var hasChangeStrategicalInput = component.get("v.hasChangeStrategicalInput");
            var hasChangeStrategicalApproval = component.get("v.hasChangeStrategicalApproval");
            var hasChangeStrategicAdmin = component.get("v.hasChangeStrategicAdmin");
            
            if( (hasChangePotentialInput || hasChangePotentialApproval || hasChangePotentialAdmin ||
                 hasChangeBehavioralInput || hasChangeBehavioralApproval || hasChangeBehavioralAdmin ||
                 hasChangeStrategicalInput || hasChangeStrategicalApproval || hasChangeStrategicAdmin)
               && !confirm("You have unsaved changes. Do you want to continue?")) return;
            
            component.set("v.hasChangePotentialInput", false);
        	component.set("v.hasChangePotentialApproval", false);
            component.set("v.hasChangePotentialAdmin", false);
            component.set("v.hasChangeBehavioralInput", false);
            component.set("v.hasChangeBehavioralApproval", false);
            component.set("v.hasChangeBehavioralAdmin", false);
            component.set("v.hasChangeStrategicalInput", false);
            component.set("v.hasChangeStrategicalApproval", false);
            component.set("v.hasChangeStrategicAdmin", false);
            
        	if(window.location.href.includes("/partner/")) window.location.href ="/partner/";           
        	else window.location.href ="/";
        }
	},
    jumpToData : function(component, event, helper) {
    	
        var code = event.getParam("code");
        var key = code.split(":")[0];
        
        var inputFilters = {};        
        inputFilters[key] = code.split(key + ":")[1];
        
        component.set("v.inputFilters", inputFilters);
        component.set("v.selectedTab", key);
    },
    showToast : function(component, event, helper) {
        
        var message = event.getParam('message');          
        
       	helper.showMessage(component, message);
	},    
	closeToast : function(component, event, helper) {
		$A.util.addClass( component.find( 'toastModel' ), 'slds-hide' );
        component.set("v.message", "");        
	}
        
})