({
	getOverview : function (component, event, helper){
    	
        var action = component.get("c.getUserOverview");
        
        action.setCallback(this, function(response) {
    		
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var overview = response.getReturnValue();    
                
                component.set("v.hasPotentialInput", overview.hasPotentialInput); 
                component.set("v.hasPotentialApproval", overview.hasPotentialApproval); 
                
 				component.set("v.behavioralOptions", overview.behavioralOptions); 
                component.set("v.behavioralApprovalOptions", overview.behavioralApprovalOptions); 
                                
                component.set("v.strategicOptions", overview.strategicOptions); 
                component.set("v.strategicApprovalOptions", overview.strategicApprovalOptions); 
                
                component.set("v.isAdmin", overview.isAdmin); 
                
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    
                    this.showMessage(component, errors[0].message);
                    
                } else {
                    alert("Unknown error");
                }
            }            
        });
        
		$A.getCallback(function() {
        	$A.enqueueAction(action);
    	})();    		
    },
    showMessage : function(component, message){
        
        component.set("v.message", message);
		
        $A.util.removeClass( component.find( 'toastModel' ), 'slds-hide' );
        $A.util.addClass( component.find( 'toastModel' ), 'slds-show' );
            
        setTimeout(function(){ 
            $A.util.addClass( component.find( 'toastModel' ), 'slds-hide' ); 
            component.set("v.message", "");
        }, 5000);
    }
})