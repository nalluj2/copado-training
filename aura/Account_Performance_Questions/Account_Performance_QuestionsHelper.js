({
	 resize : function (component, event, helper){
  		
        if(component.isValid()){
        
            var timer = component.get('v.timer');
            clearTimeout(timer);
        
            var timer = setTimeout(function(){
                                
                clearTimeout(timer);
                component.set('v.timer', null);
                
                helper.setSizes(component);
                
            }, 500);
    
            component.set('v.timer', timer);      
        }
    },
    setSizes : function(component){
        
		var tableWidth = component.find("mainTable").getElement().getBoundingClientRect().width;
                
        var questions = component.get("v.questions");
        var questionWidth = tableWidth - 310;
        var widthReminder = 0;
        
        if(questions.length > 0 ){
            questionWidth = Math.floor(((tableWidth - 310) / questions.length));
            widthReminder = (tableWidth - 310) % questions.length;
        }
        
        component.set("v.questionWidth", questionWidth);  
        component.set("v.widthReminder", widthReminder);
        
        setTimeout(function(){
            
            var containerHeight = component.find("mainContainer").getElement().getBoundingClientRect().height;
            var tableHeader = component.find("mainTableHeader").getElement().getBoundingClientRect().height;
            
            var tableHeight = containerHeight - tableHeader - 165;
            
            component.set("v.tableMaxHeight", tableHeight);
        }, 200);
    },
    getInputFormulaire : function(component, event, helper) {
		
        var purpose = component.get("v.purpose");
        var role = component.get("v.role");
        var filter = component.get("v.filter");
        
        var action = component.get("c.getUserFormulaire");
        action.setParams({ purpose : purpose, role : role, filter : filter});
        
        component.set("v.showLoading", true);
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var formulaire = response.getReturnValue();                  
                var accInputs = formulaire.accountInputs;
                
                var userId = $A.get("$SObjectType.CurrentUser.Id");
                
                var accountOptions = [];
                var assignedTos = {'list' : []};
                
                for (var i = 0; i < accInputs.length; i++) {
                    
                    var accountOption = {"label" : accInputs[i].record.Account__r.Name, "value" : accInputs[i].record.Account__c, "city" : accInputs[i].record.Account__r.BillingCity, "SAP_Id" : accInputs[i].record.Account__r.SAP_ID__c};
                    accountOptions.push(accountOption);
                    
                    if(! assignedTos.hasOwnProperty(accInputs[i].record.Assigned_To__c)){
                    
                    	var assignedTo = {"label" : accInputs[i].record.Assigned_To__r.Name, "value" : accInputs[i].record.Assigned_To__c};
                    	assignedTos.list.push(assignedTo);
                        assignedTos[accInputs[i].record.Assigned_To__c] = assignedTo;                                            
                    }
                    
                    if(role == "Input"){
                        
                        if(accInputs[i].record.Approved__c == true) accInputs[i].disabled = true;                        
                        if(accInputs[i].record.Complete__c == true) accInputs[i].completed = true;
                        
                    }else{
                        
                        if(accInputs[i].record.Approver_1__c == userId && accInputs[i].record.Approved_2__c == true) accInputs[i].disabled = true;
                        if((accInputs[i].record.Approver_1__c == userId && accInputs[i].record.Approved__c == true) || (accInputs[i].record.Approver_2__c == userId && accInputs[i].record.Approved_2__c == true)) accInputs[i].completed = true;
                    }
                }
                
                //Assigned To
				assignedTos.list.sort(function(a, b){return a.label.toLowerCase().localeCompare(b.label.toLowerCase());});
                assignedTos.list.unshift({"label" : "All", "value" : ""});
                component.set("v.assignedToOptions", assignedTos.list);
                
                component.set("v.questions", formulaire.questions);
                
                helper.applyFilters(component, accInputs, false);                
                helper.setSizes(component);
                
                component.set("v.allAccountInputs", accInputs);                
                component.set("v.accountOptions", accountOptions);
                component.set("v.hasChange", false);
                
            }else if (state === "ERROR") {
                
                var errors = response.getError();
                
                if (errors && errors[0] && errors[0].message) {
                             
                    var showError = $A.get("e.c:Show_Error_Toast");
                    showError.setParams({ "message" : errors[0].message });
                    showError.fire();                        
                    
                } else {
                    alert("Unknown error");
                }
            }
            
            component.set("v.showLoading", false);
        });
        
		$A.getCallback(function() {
        	$A.enqueueAction(action);
    	})();
	},
    applyFilters : function (component, allAccInputs, forceDisplay){
                
        var statusFilter = component.get("v.filterStatus");
        var accountFilter = component.get("v.filterAccount");
        var segmentFilter = component.get("v.filterSegment");
        var assignedToFilter = component.get("v.filterAssignedTo");
        
        var displayAccounts = 0;
		var userId = $A.get("$SObjectType.CurrentUser.Id");
        
        var displayAccInputs = [];
        
        for (var i = 0; i < allAccInputs.length; i++) {
    		
            var showInput = true;
            
            if(accountFilter != null && accountFilter.length > 0 && allAccInputs[i].record.Account__c != accountFilter) showInput = false;
            if(segmentFilter != null && segmentFilter.length > 0 && allAccInputs[i].accountSegment != segmentFilter) showInput = false;
            if(assignedToFilter != null && assignedToFilter.length > 0 && allAccInputs[i].record.Assigned_To__c != assignedToFilter) showInput = false;
            else if(statusFilter != null && statusFilter.length > 0){
                        
                if(statusFilter == 'Open' && allAccInputs[i].record.Complete__c == true) showInput = false;
                else if(statusFilter == 'Completed' && allAccInputs[i].record.Complete__c != true) showInput = false;
                else if(statusFilter == 'Approved' && (allAccInputs[i].record.Complete__c != true || (allAccInputs[i].record.Approver_1__c != null && allAccInputs[i].record.Approved__c != true) || (allAccInputs[i].record.Approver_2__c != null && allAccInputs[i].record.Approved_2__c != true) )) showInput = false;                
                else if(statusFilter == 'PendingApprover' && (!((allAccInputs[i].record.Complete__c == true && allAccInputs[i].record.Approver_1__c == userId && allAccInputs[i].record.Approved__c != true) || (allAccInputs[i].record.Approved__c == true && allAccInputs[i].record.Approver_2__c == userId && allAccInputs[i].record.Approved_2__c != true)) )) showInput = false;
                else if(statusFilter == 'PendingAssigned' && (allAccInputs[i].record.Complete__c != true || allAccInputs[i].record.Approver_1__c == null || (allAccInputs[i].record.Approved__c == true && (allAccInputs[i].record.Approver_2__c == null || allAccInputs[i].record.Approved_2__c == true)))) showInput = false;
                else if(statusFilter == 'ApprovedMe' && (!((allAccInputs[i].record.Approver_1__c == userId && allAccInputs[i].record.Approved__c == true) || (allAccInputs[i].record.Approver_2__c == userId && allAccInputs[i].record.Approved_2__c == true)) )) showInput = false;
           	}
                        
            if(showInput == false) allAccInputs[i].selected = false;
            else displayAccInputs.push(allAccInputs[i]);
        }
        
        component.set("v.selectedAll", false);
        component.set("v.displayInputsCount", displayAccInputs.length);
        
        if(displayAccInputs.length < 500 || forceDisplay == true){
        
        	this.applySorting(component, displayAccInputs);
            component.set("v.accountInputs", displayAccInputs);
            
        }else{
            
            component.set("v.accountInputs", null);
        }              
    },
    applyInputFilters : function(component, filters) {
    	
        component.set("v.filterAccount", null);        
    	component.set("v.filterStatus", '');
        
		var filterList = filters.split(":");
        
        component.set("v.filter", filterList[0]);   
        if(filterList.length > 1){
            
            var segmentation = filterList[1];
            if(segmentation == 'Not Segmented') segmentation = '-';
            
            component.set("v.filterSegment", segmentation);
        }
        else component.set("v.filterSegment", null);
                        
        if(filterList.length > 2) component.set("v.filterAssignedTo", filterList[2].split(';')[0]);
        else component.set("v.filterAssignedTo", null);
    },
    applySorting : function (component, accInputs){
        
        var sortBy = component.get("v.sortBy");
        var sortDir = component.get("v.sortDir");
        
        accInputs.sort(function(a, b){
            
            var result;
            
            if(sortBy == "AccountName"){
            	
                result = a.record.Account__r.Name.toLowerCase().localeCompare(b.record.Account__r.Name.toLowerCase());
                
                if(sortDir == 'DESC') result = 0 - result;
            
            }else if(sortBy == "Status"){
            	
                if(a.record.Complete__c == false){
                    
                    if(b.record.Complete__c == true) result = -1;
                    else{
                        
                        if((a.record.Approver_1__c == null && b.record.Approver_1__c != null) || (a.record.Approver_2__c == null && b.record.Approver_2__c != null)) result = -1;
                        else if((a.record.Approver_1__c != null && b.record.Approver_1__c == null) || (a.record.Approver_2__c != null && b.record.Approver_2__c == null)) result = 1;
                        else result = 0;                        
                    }
                    
                }else if(a.record.Approver_1__c == null){
                    
                    if(b.record.Complete__c == false) result = 1;
                    else{
                        
                        if((b.record.Approver_1__c != null && b.record.Approved__c == false) || (b.record.Approver_2__c != null && b.record.Approved_2__c == false)) result = 1;
                        else if(b.record.Approver_1__c != null) result = -1;
                    	else result = 0;
                    }
                    
                }else if(a.record.Approved__c == false){
                    
                    if(b.record.Complete__c == false) result = 1;
                    else{
                        
                        if(b.record.Approver_1__c == null || b.record.Approved__c == true) result = -1;   
                        else if(a.record.Approver_2__c == null){
                            
                            if(b.record.Approver_2__c != null) result = -1;
                            else result = 0;
                            
                        }else{
                        	
                            if(b.record.Approver_2__c == null) result = 1;
                            else result = 0;
                        }                    	
                    }                    
                    
                }else if(a.record.Approver_2__c == null){
                    
                    if(b.record.Complete__c == false) result = 1;
                    else{
                        
                        if(b.record.Approved__c == false || (b.record.Approver_2__c != null && b.record.Approved_2__c == false)) result = 1;
                        else if(b.record.Approver_2__c != null) result = -1;
                    	else result = 0;
                    }
                    
                }else if(a.record.Approved_2__c == false){
                    
                    if(b.record.Complete__c == false || (b.record.Approver_1__c != null && b.record.Approved__c == false)) result = 1;
                    else{
                        
                        if(b.record.Approver_1__c == null || b.record.Approver_2__c == null || a.record.Approved_2__c == true) result = -1                        
                    	else result = 0;
                    }                 
                    
                }else{
                    
                    if(b.record.Complete__c == false || b.record.Approved__c == false || b.record.Approved_2__c == false) result = 1;
                    else result = 0; 
                }
                
                if(sortDir == 'DESC') result = 0 - result;
            }
            
            if(result == 0) result = a.record.Account__r.Name.toLowerCase().localeCompare(b.record.Account__r.Name.toLowerCase());
            if(result == 0) result = a.record.Account__c.localeCompare(b.record.Account__c);            
            
            return result;        
        });  
        
        return accInputs;        
    },
    saveForm : function(component) {
		
        var allAccInputs = component.get("v.allAccountInputs");
        
        var changed = [];
        
        for(var i = 0; i < allAccInputs.length; i++){
        	
            if(allAccInputs[i].changed == true) changed.push(allAccInputs[i]);
            else{
                
                var hasChange = false;
                
                for(var j = 0; j < allAccInputs[i].accountAnswers.length; j++){
                    
                    if(allAccInputs[i].accountAnswers[j].changed == true) hasChange = true;
                }
                
                if(hasChange == true) changed.push(allAccInputs[i]);
            }
        }
        
        var action = component.get("c.saveUserFormulaire");
        action.setParams({formulaire : changed});
        
        component.set("v.showLoading", true);
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var newInputs = response.getReturnValue();
                
				for (var i = 0; i < changed.length; i++) {
                    
                	changed[i].record.Id = newInputs[i].record.Id;
                    changed[i].completedTS = newInputs[i].completedTS;
                    changed[i].approved1TS = newInputs[i].approved1TS;
                    changed[i].approved2TS = newInputs[i].approved2TS;
                    changed[i].changed = false;
                    
                    for(var j = 0; j < changed[i].accountAnswers.length; j++){
                        
                        changed[i].accountAnswers[j].record.Id = newInputs[i].accountAnswers[j].record.Id;
                        changed[i].accountAnswers[j].answerTS = newInputs[i].accountAnswers[j].answerTS;
                        changed[i].accountAnswers[j].changed = false;
                    }
                }
                
                component.set("v.allAccountInputs", allAccInputs);
                component.set("v.hasChange", false);
                
                this.applyFilters(component, allAccInputs, true);
                
                var purpose = component.get("v.purpose");
                
                var refreshEvent = $A.get("e.c:Segmentation_Overview_Refresh");
        		refreshEvent.setParams({ "data_type" : purpose });
        		refreshEvent.fire();
                
            }else if (state === "ERROR") {
                
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                        
                    var showError = $A.get("e.c:Show_Error_Toast");
                    showError.setParams({ "message" : errors[0].message });
                    showError.fire();                        
                    
                } else {
                    alert("Unknown error");
                }
            }
            
            component.set("v.showLoading", false);
        });
        
		$A.getCallback(function() {
        	$A.enqueueAction(action);
    	})();
	},
    selectRow : function(component){
        
        var accInputs = component.get("v.accountInputs");
        var role = component.get("v.role");
		var hasSelection = false; 
        var hasOpen = false;        
        var hasCompleted = false; 
        var hasLevel1 = false;
        var hasLevel2 = false;
        
        var userId = $A.get( "$SObjectType.CurrentUser.Id" );
        
        for (var i = 0; i < accInputs.length; i++) {
            
            if(accInputs[i].selected == true){
                
                hasSelection = true;
                if(accInputs[i].record.Approver_1__c == userId) hasLevel1 = true;
                if(accInputs[i].record.Approver_2__c == userId){
                    hasLevel1 = true;
                    hasLevel2 = true;
                }
                if(accInputs[i].completed != true) hasOpen = true;
                else hasCompleted = true;
            }    		
        }
		
        if(role == 'Approval'){
        
            var newStatusOptions = [                
                { value: "Open", label: "Open" },
                { value: "Complete", label: "Complete" }            
            ];
            
            if(hasLevel1 == true) newStatusOptions.push({value: "Approved1", label: "Approved level 1"});
            if(hasLevel2 == true) newStatusOptions.push({ value: "Approved2", label: "Approved level 2"});
            
            component.set("v.newStatusOptions", newStatusOptions);        
        }
        
		component.set("v.hasSelection", hasSelection);
        component.set("v.hasOpen", hasOpen);
        component.set("v.hasCompleted", hasCompleted);   
    }
})