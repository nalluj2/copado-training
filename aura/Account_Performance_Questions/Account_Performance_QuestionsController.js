({
	doInit : function(component, event, helper) {
		
        window.addEventListener("resize",function(e){
  			helper.resize(component, event, helper);
		});
        
        var role = component.get("v.role");
        
        var statusOptions = [
            { value: "", label: "All" },
            { value: "Open", label: "Open" },
            { value: "Completed", label: "Completed" }            
        ];
        
        if(role == 'Approval'){
            
            statusOptions.push({ value: "PendingApprover", label: "Pending my approval" });
            statusOptions.push({ value: "ApprovedMe", label: "Approved (by me)" });
            
        }else statusOptions.push({ value: "PendingAssigned", label: "Pending approval" });
        
        statusOptions.push({ value: "Approved", label: "Approved" });
        
        component.set("v.statusOptions", statusOptions);
        
        var purpose = component.get("v.purpose");
                
        var segmentOptions;
        
        if(purpose == 'Behavioral'){
        	segmentOptions = [
                { value: "", label: "All" },
                { value: "-", label: "Not Segmented" },
                { value: "Clinical", label: "Clinical" },
                { value: "Transactional", label: "Transactional" }                
            ];
            
        }else{
            
            segmentOptions = [
                { value: "", label: "All" },
                { value: "-", label: "Not Segmented" },
                { value: "Strategic", label: "Strategic" },
                { value: "Non-Strategic", label: "Non-Strategic" }
            ];
        }
        
        component.set("v.segmentOptions", segmentOptions);
        
        var inputFilters = component.get("v.inputFilters");
        
        if(inputFilters != null){
         
            helper.applyInputFilters(component, inputFilters);
            
        }else{
        
            var filterOptions = component.get("v.filterOptions");
            
            if(filterOptions.length == 1){
                        
                component.set("v.filter", filterOptions[0].value);
                helper.getInputFormulaire(component, event, helper);            
            }
        }
        
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        component.set("v.userId", userId);        
	},
    filterOptionSelected : function(component, event, helper) {
        
        var filterReverted = component.get("v.filterReverted");
        
        if(filterReverted){
            
            component.set("v.filterReverted", false);
            return;
        }
        
        var hasChange = component.get("v.hasChange");
        
        if(hasChange && confirm('You have un-saved changes. Do you want to proceed?') == false){
            
            var prevValue = event.getParam("oldValue");
            
            component.set("v.filterReverted", true);
            
            setTimeout($A.getCallback(function(){
                                
                component.find("filter-combo").set("v.value", prevValue);
                
             }), 1);   
            
            return;
        }
        
        var filter = component.get("v.filter");
        
        if(filter != null && filter != '') helper.getInputFormulaire(component, event, helper);
    },
    selectAll : function(component, event, helper) {
     	
        component.set("v.showLoading", true);
        
        setTimeout($A.getCallback(function(){
        
            var accInputs = component.get("v.accountInputs");
            var selectedValue = component.get("v.selectedAll");
            
            for (var i = 0; i < accInputs.length; i++) {
                
                if(accInputs[i].disabled != true) accInputs[i].selected = selectedValue;    		
            }
                        
            component.set("v.accountInputs", accInputs);
            component.set("v.showLoading", false);
            
            helper.selectRow(component);
            
        }), 100);
    },
    selectRow : function(component, event, helper) {
     	        
        helper.selectRow(component);
    }, 
    forceShow : function(component, event, helper) {
        
        component.set("v.showLoading", true);
        
        setTimeout($A.getCallback(function(){
            
            var accInputs = component.get("v.allAccountInputs");
            
            helper.applyFilters(component, accInputs, true);
            helper.setSizes(component);
            
            component.set("v.showLoading", false);
            
        }), 100);
    },
    filterSelected : function(component, event, helper) {
        
        component.set("v.showLoading", true);
        
        setTimeout($A.getCallback(function(){
            
            var accInputs = component.get("v.allAccountInputs");
            
            helper.applyFilters(component, accInputs, false);
            
            component.set("v.showLoading", false);
            
        }), 100);
    },
    inputFilterChanged : function(component, event, helper) {
        
        var inputFilters = component.get("v.inputFilters");
            
        if(inputFilters != null){
        	
            var filter = component.get("v.filter");            
            helper.applyInputFilters(component, inputFilters);
            
            if(filter == inputFilters.split(":")[0]){
                
                var a = component.get('c.filterSelected');
                a.setParams({ component: component, event : event, helper : helper});
        		$A.enqueueAction(a);
            }            
        }
    },
    changeSorting : function(component, event, helper) {
        
        component.set("v.showLoading", true);
        
        setTimeout($A.getCallback(function(){
            
            var sortBy = component.get("v.sortBy");            
            var sortDir = component.get("v.sortDir");
            var newSortBy = event.getSource().get("v.id");
            
            if(newSortBy == sortBy){
                
                if(sortDir == 'ASC') sortDir = 'DESC';
                else sortDir = 'ASC';
                
            }else{
                
                sortBy = newSortBy;
                sortDir = 'ASC';
            }
            
            component.set("v.sortBy", sortBy); 
            component.set("v.sortDir", sortDir); 
            
            var accInputs = component.get("v.accountInputs");
            
            helper.applySorting(component, accInputs);
            
            component.set("v.accountInputs", accInputs);            
            component.set("v.showLoading", false);
            
        }), 100);
    },
    markCompleted : function(component, event, helper){
        
        component.set("v.showLoading", true);
        
        setTimeout($A.getCallback(function(){
            
            var accInputs = component.get("v.accountInputs");
            var hasChange = false;
            
            for (var i = 0; i < accInputs.length; i++) {
                
                if(accInputs[i].selected == true && accInputs[i].record.Complete__c != true){
                    
                    accInputs[i].record.Complete__c = true;
                    accInputs[i].completed = true;
                    accInputs[i].changed = true;
                    hasChange = true;
                }
                
                accInputs[i].selected = false;
            }
            
            if(hasChange) component.set("v.hasChange", true);
            
            helper.selectRow(component);
            
            component.set("v.selectedAll", false);
            component.set("v.accountInputs", accInputs);
            component.set("v.showLoading", false);
            
        }), 100);
    },
    changeStatus : function(component, event, helper){
        
        component.set("v.showLoading", true);
        
        setTimeout($A.getCallback(function(){
            
            var accInputs = component.get("v.accountInputs");
            var newStatus = component.get("v.newStatus");
            var userId = $A.get("$SObjectType.CurrentUser.Id");
            var hasChange = false;
            
            for (var i = 0; i < accInputs.length; i++) {
                
                if(accInputs[i].selected == true){
                    
                    if(newStatus == 'Open'){
                    	
                        if(accInputs[i].record.Complete__c == true){
                    
                            accInputs[i].record.Complete__c = false;
                            accInputs[i].record.Approved__c = false;
                            accInputs[i].record.Approved_2__c = false;                            
                            accInputs[i].changed = true;
                            hasChange = true;
                        }
                    
                    }else if(newStatus == 'Complete'){
                        
                        if(accInputs[i].record.Complete__c != true || accInputs[i].record.Approved__c == true){
                    
                            accInputs[i].record.Complete__c = true;
                            accInputs[i].record.Approved__c = false;
                            accInputs[i].record.Approved_2__c = false;                            
                            accInputs[i].changed = true;
                            hasChange = true;
                        }
                        
                    }else if(newStatus == 'Approved1'){
                        
                        if(accInputs[i].record.Approved__c != true || accInputs[i].record.Approved_2__c == true){
                    
                            accInputs[i].record.Complete__c = true;
                            if(accInputs[i].record.Approver_1__c != null) accInputs[i].record.Approved__c = true;
                            accInputs[i].record.Approved_2__c = false;                            
                            accInputs[i].changed = true;
                            hasChange = true;
                        }
                        
                    }else if(newStatus == 'Approved2'){
                        
                        if(accInputs[i].record.Approved_2__c != true){
                    
                            accInputs[i].record.Complete__c = true;
                            if(accInputs[i].record.Approver_1__c != null) accInputs[i].record.Approved__c = true;
                            if(accInputs[i].record.Approver_2__c == userId) accInputs[i].record.Approved_2__c = true;                            
                            accInputs[i].changed = true;
                            hasChange = true;
                        }
                    }
                }
                
                accInputs[i].selected = false;
            }
            
            component.set("v.newStatus", null);
            if(hasChange) component.set("v.hasChange", true);
            
            component.set("v.selectedAll", false);
            component.set("v.accountInputs", accInputs);
            component.set("v.showLoading", false);
            helper.selectRow(component);
            
        }), 100);
    }, 
    reopen : function(component, event, helper){
        
        component.set("v.showLoading", true);
        
        setTimeout($A.getCallback(function(){
            
            var accInputs = component.get("v.accountInputs");
            var hasChange = false;
            
            for (var i = 0; i < accInputs.length; i++) {
                
                if(accInputs[i].selected == true && accInputs[i].record.Complete__c == true){
                    
                    accInputs[i].record.Complete__c = false;
                    accInputs[i].completed = false;
                    accInputs[i].changed = true;
                    hasChange = true;
                }
                
                accInputs[i].selected = false;
            }
            
            if(hasChange) component.set("v.hasChange", true);
            
            helper.selectRow(component);
            
            component.set("v.selectedAll", false);
            component.set("v.accountInputs", accInputs);
            component.set("v.showLoading", false);
            
        }), 100);
    },    
    copyLY : function(component, event, helper){
        
        component.set("v.showLoading", true);
        
        setTimeout($A.getCallback(function(){
            
            var accInputs = component.get("v.accountInputs");
            var hasChange = false;
            
            for (var i = 0; i < accInputs.length; i++) {
                
                var accInput = accInputs[i];
                
                if(accInput.selected == true && accInput.completed != true){
                    
                    for(var j = 0; j < accInput.accountAnswers.length; j++){
                        
                        var accountAnswer = accInput.accountAnswers[j];
                        var prevAccountAnswer = accInput.prevAccountAnswers[j];
                        
                        if(prevAccountAnswer.value != null && prevAccountAnswer.value != "" && prevAccountAnswer.value != accountAnswer.record.Answer__c){
                            
                            accountAnswer.record.Answer__c = prevAccountAnswer.value;
                            accountAnswer.changed = true;
                            hasChange = true;
                        }
                    }
                }
                
                accInputs[i].selected = false;
            }
            
            if(hasChange) component.set("v.hasChange", true);
            
            helper.selectRow(component);
            
            component.set("v.selectedAll", false);
            component.set("v.accountInputs", accInputs);
            component.set("v.showLoading", false);
            
        }), 100);
    },
    cancel : function(component, event, helper){
        
        helper.getInputFormulaire(component, event, helper);        
    },
    saveInputs : function(component, event, helper){
                           
        helper.saveForm(component);        
    }
})