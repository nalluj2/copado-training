<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Country_Period_Concatenation</fullName>
        <field>Unique_Check__c</field>
        <formula>DIB_Country__c &amp;  TEXT(Country_Period_Fiscal_Month__c)  &amp;  TEXT(Country_Period_Fiscal_Year__c)</formula>
        <name>Country Period: Fill Concatenation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Country Period%3A Concatenation</fullName>
        <actions>
            <name>Country_Period_Concatenation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>DIB_Country_Period__c.LastModifiedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
