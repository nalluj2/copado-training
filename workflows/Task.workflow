<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Task_Send_Notification_CVG</fullName>
        <description>Task: Send Notification CVG</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Task_Email_Templates/Task_Notification_template_CVG</template>
    </alerts>
    <alerts>
        <fullName>Task_Send_Notification_DIB</fullName>
        <description>Task: Send Notification DIB</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Task_Email_Templates/Task_Notification_template_DIB</template>
    </alerts>
    <alerts>
        <fullName>Task_Send_Notification_RTG</fullName>
        <description>Task: Send Notification RTG</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Task_Email_Templates/Task_Notification_template_RTG</template>
    </alerts>
    <rules>
        <fullName>Task%3A Send Notification - CVG</fullName>
        <actions>
            <name>Task_Send_Notification_CVG</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Sends an email when a Task has been created in Mobile.</description>
        <formula>AND(Created_in_Mobile_CVG__c = True,  OR(  ISNEW(),ischanged( OwnerId )  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Task%3A Send Notification - DIB</fullName>
        <actions>
            <name>Task_Send_Notification_DIB</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends an email when a Task has been created in Mobile.</description>
        <formula>AND(Created_in_Mobile_DIB__c = True, 
NOT(ISPICKVAL(Status, &apos;Completed&apos;)), 
OR( 
ISNEW(),ischanged( OwnerId ) 
) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Task%3A Send Notification - RTG</fullName>
        <actions>
            <name>Task_Send_Notification_RTG</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends an email when a Task has been created in Mobile.</description>
        <formula>AND(Created_in_Mobile_RTG__c = True, 
NOT(ISPICKVAL(Status, &apos;Completed&apos;)), 
OR( 
ISNEW(),ischanged( OwnerId ) 
) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
