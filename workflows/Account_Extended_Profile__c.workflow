<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Account_Extended_Profile_Approval_Notification_for_Section_Facility_Information</fullName>
        <description>Account Extended Profile Approval Notification for Section Facility Information</description>
        <protected>false</protected>
        <recipients>
            <field>Section_Facility_Info_Approver_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>MITG_Email_Templates/Account_Extended_Profile_Section_Approval_Notification</template>
    </alerts>
    <alerts>
        <fullName>Account_Extended_Profile_Approval_Notification_for_Section_SI_AST_Driven</fullName>
        <description>Account Extended Profile Approval Notification for Section SI AST Driven</description>
        <protected>false</protected>
        <recipients>
            <field>Section_SI_AST_Driven_Approver_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>MITG_Email_Templates/Account_Extended_Profile_Section_Approval_Notification</template>
    </alerts>
    <alerts>
        <fullName>Account_Extended_Profile_Approval_Notification_for_Section_SI_GSP_Driven</fullName>
        <description>Account Extended Profile Approval Notification for Section SI GSP Driven</description>
        <protected>false</protected>
        <recipients>
            <field>Section_SI_GSP_Driven_Approver_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>MITG_Email_Templates/Account_Extended_Profile_Section_Approval_Notification</template>
    </alerts>
    <alerts>
        <fullName>Account_Extended_Profile_Approval_Notification_for_Section_SI_Hernia_Driven</fullName>
        <description>Account Extended Profile Approval Notification for Section SI Hernia Driven</description>
        <protected>false</protected>
        <recipients>
            <field>Section_SI_Hernia_Driven_Approver_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>MITG_Email_Templates/Account_Extended_Profile_Section_Approval_Notification</template>
    </alerts>
    <rules>
        <fullName>Account Extended Profile Approval Request Facility info</fullName>
        <actions>
            <name>Account_Extended_Profile_Approval_Notification_for_Section_Facility_Information</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send an emal notification when approval status is set to &quot;Request approval&quot;</description>
        <formula>AND( ISCHANGED(Approval_Status_Facility_Info__c ), CASE(Approval_Status_Facility_Info__c, &quot;Request Approval&quot;, 1, 0) = 1 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account Extended Profile Approval Request SI AST Driven</fullName>
        <actions>
            <name>Account_Extended_Profile_Approval_Notification_for_Section_SI_AST_Driven</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send an emal notification when approval status is set to &quot;Request approval&quot;</description>
        <formula>AND( ISCHANGED(Approval_Status_SI_AST_Driven__c ), CASE(Approval_Status_SI_AST_Driven__c, &quot;Request Approval&quot;, 1, 0) = 1 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account Extended Profile Approval Request SI GSP Driven</fullName>
        <actions>
            <name>Account_Extended_Profile_Approval_Notification_for_Section_SI_GSP_Driven</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send an emal notification when approval status is set to &quot;Request approval&quot;</description>
        <formula>AND( ISCHANGED(Approval_Status_SI_GSP_Driven__c ), CASE(Approval_Status_SI_GSP_Driven__c, &quot;Request Approval&quot;, 1, 0) = 1 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account Extended Profile Approval Request SI Hernia Driven</fullName>
        <actions>
            <name>Account_Extended_Profile_Approval_Notification_for_Section_SI_Hernia_Driven</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send an emal notification when approval status is set to &quot;Request approval&quot;</description>
        <formula>AND( ISCHANGED(Approval_Status_SI_Hernia_Driven__c ), CASE(Approval_Status_SI_Hernia_Driven__c, &quot;Request Approval&quot;, 1, 0) = 1 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
