<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Contract_Anniversary_Reminder_to_Contract_Owner</fullName>
        <description>Send Contract Anniversary Reminder to Contract Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>responsecareservices@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Contract_Email_Templates/Contract_Anniversary_Reminder</template>
    </alerts>
    <fieldUpdates>
        <fullName>Contract_RTG_Approval_Status_Activated</fullName>
        <description>Changes Approval Status on Service Contract to Activated</description>
        <field>Approval_Status__c</field>
        <literalValue>Activated</literalValue>
        <name>Contract RTG: Approval Status Activated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_RTG_Approval_Status_Expired</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Expired</literalValue>
        <name>Contract RTG: Approval Status Expired</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Contract_Inform_Owner_On</fullName>
        <description>Contract EndData &lt; Today() --&gt; set date = null
Contract EndData &gt; Today() --&gt; calc. number of years between StartDate and Today, multiply this by 365 to get number of days we need to add to the Start Date.  Substract the provided notification dates.</description>
        <field>Inform_Owner_On__c</field>
        <formula>IF(
  EndDate &lt; TODAY()
  , null
  , IF(
      StartDate + 365 * CEILING( (TODAY() - StartDate) / 365 ) - VALUE(BLANKVALUE(TEXT(Contract_Anniversary_Notification__c), &quot;0&quot;)) &gt;= TODAY()
      , IF(
        StartDate + 365 * CEILING( (TODAY() - StartDate) / 365 ) &lt;= EndDate
        , StartDate + 365 * CEILING( (TODAY() - StartDate) / 365 ) 
        , EndDate 
      ) 
      , IF(
        StartDate + 365 * ( 1 + CEILING( (TODAY() - StartDate) / 365 ) ) &lt;= EndDate
        , StartDate + 365 * ( 1 + CEILING( (TODAY() - StartDate) / 365 ) ) 
        , EndDate
      )
    )
    - VALUE(BLANKVALUE(TEXT(Contract_Anniversary_Notification__c), &quot;0&quot;))
)</formula>
        <name>Set Contract Inform Owner On</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Contract Anniversary - Inform Contract Owner</fullName>
        <active>true</active>
        <description>Send email to Contract Owner and set the next notification date</description>
        <formula>AND(   NOT(ISBLANK(Inform_Owner_On__c))    , Inform_Owner_On__c &gt;= TODAY() )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Contract_Inform_Owner_On</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Contract.Inform_Owner_On__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Contract_Anniversary_Reminder_to_Contract_Owner</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Contract.Inform_Owner_On__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Contract RTG%3A Auto Activate%2FExpire</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Contract.RecordTypeId</field>
            <operation>equals</operation>
            <value>RTG Service Contract</value>
        </criteriaItems>
        <description>Automatically changes Approval Status on Service Contract to Activated on Start Date and Expired 1 day after End Date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Contract_RTG_Approval_Status_Activated</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Contract.StartDate</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Contract_RTG_Approval_Status_Expired</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Contract.EndDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Set Contract Inform Owner On</fullName>
        <actions>
            <name>Set_Contract_Inform_Owner_On</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflows set the date field Inform Owner On on contract when an email needs to be send to the Owner of the contract</description>
        <formula>IF (   ISNEW()   , NOT(ISBLANK(TEXT(Contract_Anniversary_Notification__c)))   , OR(     ISCHANGED(Contract_Anniversary_Notification__c)     , ISCHANGED(StartDate)     , ISCHANGED(EndDate)   ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
