<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>EMEA_IHS_Fill_Scheduled_Date</fullName>
        <description>Fill the Scheduled Date of the Work Order (IHS) with the</description>
        <field>Scheduled_Date__c</field>
        <formula>DATEVALUE(Scheduled_Time__c)</formula>
        <name>EMEA IHS Fill Scheduled Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>EMEA IHS Scheduled Date</fullName>
        <actions>
            <name>EMEA_IHS_Fill_Scheduled_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Work_Order_IHS__c.Scheduled_Time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Scheduled Date is filled from the Scheduled Time (Scheduled Date is a Date field that is used in a formula to determine the number of days between the WO Scheduled Date and the Next PM Due Date of the Equipment)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
