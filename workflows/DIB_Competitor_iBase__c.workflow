<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CompIB_Copy_Comp_Name</fullName>
        <field>SYSTEM_FIELD_Comp_Name__c</field>
        <formula>Competitor_Account_ID__r.Name</formula>
        <name>CompIB: Copy Comp Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CIB_Unique</fullName>
        <description>CIB_Unique field is updated</description>
        <field>CIB_Unique__c</field>
        <formula>Account_Segmentation__c  &amp; &quot;-&quot; &amp;  Competitor_Account_ID__c &amp; &quot;-&quot; &amp;  
Fiscal_Year_Month__c &amp; &quot;-&quot; &amp;  
TEXT(Type__c)</formula>
        <name>Set CIB_Unique</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Copy Comp to Comp Name</fullName>
        <actions>
            <name>CompIB_Copy_Comp_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>DIB_Competitor_iBase__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Unique Identifier</fullName>
        <actions>
            <name>Set_CIB_Unique</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>DIB_Competitor_iBase__c.Competitor_Name__c</field>
            <operation>notEqual</operation>
            <value>Null</value>
        </criteriaItems>
        <description>Sets the CIB_Unique field to make the record unique by concatenating Department, Competitor, Fiscal Month/Year and Type</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
