<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Call_Topic_Setting_Fill_Unique_Field</fullName>
        <field>Call_Topic_Setting_Unique__c</field>
        <formula>if( ISBLANK(Call_Record_Record_Type__c),Business_Unit__c &amp;  Call_Activity_Type__c &amp;  Call_Category__c &amp; TEXT(Region_vs__c),IF( ISBLANK( Business_Unit__c) ,&apos;&apos;,Business_Unit__c &amp;  Call_Activity_Type__c &amp; TEXT(Region_vs__c) ))</formula>
        <name>Call Topic Setting: Fill Unique Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Call Topic Setting%3A Fill Unique Field</fullName>
        <actions>
            <name>Call_Topic_Setting_Fill_Unique_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Call_Topic_Settings__c.LastModifiedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
