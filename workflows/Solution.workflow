<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_or_updated_solution</fullName>
        <description>New or updated solution</description>
        <protected>false</protected>
        <recipients>
            <recipient>jan.kebeck@medtronic.nl</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>john.kokkelmans@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/New_or_updated_solution</template>
    </alerts>
    <rules>
        <fullName>IT Solution updated</fullName>
        <actions>
            <name>New_or_updated_solution</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Solution.Status</field>
            <operation>equals</operation>
            <value>Reviewed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Solution.RecordTypeId</field>
            <operation>equals</operation>
            <value>Customer Solutions</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
