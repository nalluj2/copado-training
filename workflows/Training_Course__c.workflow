<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Course_no_seats_available_notification</fullName>
        <description>Course no seats available notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>MITG_Email_Templates/Course_no_seats_available</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Status</fullName>
        <field>Status__c</field>
        <literalValue>Nomination Closed</literalValue>
        <name>Update Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>No seats available</fullName>
        <actions>
            <name>Course_no_seats_available_notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Training_Course__c.of_Seats_Open__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <description>Send an email to the course owner when the # of seats for a course is reached (# of Seats Open = 0).</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
