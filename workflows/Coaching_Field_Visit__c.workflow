<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Field_Visit_Has_Been_Updated</fullName>
        <description>Field Visit Has Been Updated</description>
        <protected>false</protected>
        <recipients>
            <field>Manager_Mentor__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>MITG_Email_Templates/Field_Visit_Has_Been_Updated</template>
    </alerts>
    <rules>
        <fullName>Coaching%2FField Visit - Field Visit Has Been Updated</fullName>
        <actions>
            <name>Field_Visit_Has_Been_Updated</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Coaching_Field_Visit__c.Send_Email_Field_Visit_Updated__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Workflow to send e-mail to sales rep and manager listed on a field visit when a field visit has been updated.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
