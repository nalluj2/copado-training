<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Campaign_Account_Start_Date</fullName>
        <description>Update System Start Date Campaign_Account</description>
        <field>SYSTEM_FIELD_Start_Date__c</field>
        <formula>DATEVALUE( Start_Fiscal_Year__c &amp; &apos;-&apos; &amp; Start_Fiscal_Month__c &amp; &apos;-01&apos;)</formula>
        <name>Update Campaign_Account Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Campaign_Account%3A Update Campaign_Account Start Date</fullName>
        <actions>
            <name>Update_Campaign_Account_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>DIB_Campaign_Account__c.Start_Fiscal_Month__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>DIB_Campaign_Account__c.Start_Fiscal_Year__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>DIB_Campaign_Account__c.SYSTEM_FIELD_Start_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Used for DiB Insight project. 
Update the Start Date of a campaign if this is empty</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
