<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Fill_P_Code_MPG_Unique_field</fullName>
        <field>P_Code_MPG_Unique__c</field>
        <formula>P_Code_MPG_Unique_Id__c</formula>
        <name>Fill P-Code/MPG Unique field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>SAP Product Mapping Changed</fullName>
        <actions>
            <name>Fill_P_Code_MPG_Unique_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SAP_Product_Mapping__c.OwnerId</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
