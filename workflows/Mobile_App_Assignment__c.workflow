<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>UserApplicationDup</fullName>
        <field>UserApplication__c</field>
        <formula>Mobile_App__c+User__c</formula>
        <name>UserApplicationDup</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>MobileAppAssignmentDuplicateCheck</fullName>
        <actions>
            <name>UserApplicationDup</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( 
ISBLANK( UserApplication__c ) , 
ISCHANGED( User__c ) , 
ISCHANGED( Mobile_App__c ) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
