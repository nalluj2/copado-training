<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>AC_RTG_Auto_Populate_End_Dates</fullName>
        <field>EndDate__c</field>
        <formula>Contract__r.EndDate</formula>
        <name>AC RTG: Auto-Populate End Dates</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AC_RTG_Auto_Populate_Start_Dates</fullName>
        <field>StartDate__c</field>
        <formula>Contract__r.StartDate</formula>
        <name>AC RTG: Auto-Populate Start Dates</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>AC RTG%3A Auto-Populate Start and End Dates</fullName>
        <actions>
            <name>AC_RTG_Auto_Populate_End_Dates</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>AC_RTG_Auto_Populate_Start_Dates</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>AssetContract__c.StartDate__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>AssetContract__c.EndDate__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
