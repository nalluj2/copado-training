<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>WI_Send_Email_More_Info_needed</fullName>
        <description>WI: Send email when status is set to More Info Needed</description>
        <protected>false</protected>
        <recipients>
            <recipient>ron.van.brandenburg@medtronic.com2</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Application_Management/WI_More_info_Required</template>
    </alerts>
    <alerts>
        <fullName>WI_Send_Email_to_Assignee</fullName>
        <description>WI: Send Email to Assignee</description>
        <protected>false</protected>
        <recipients>
            <field>Assignee_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Application_Management/WI_Assignment</template>
    </alerts>
    <fieldUpdates>
        <fullName>WI_Set_Assignee_Email</fullName>
        <field>Assignee_Email__c</field>
        <formula>Assignee__r.Email</formula>
        <name>WI: Set Assignee Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>WI%3A Send email to assignee</fullName>
        <actions>
            <name>WI_Send_Email_to_Assignee</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>WI_Set_Assignee_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Send email to assignee</description>
        <formula>OR( (AND   (IsChanged (Overall_Status__c),   Change_Request__r.RecordType.DeveloperName = &quot;Mobile&quot; ,      OR   (ISPICKVAL( Overall_Status__c , &quot;Impact Analysis&quot;),     ISPICKVAL( Overall_Status__c , &quot;Planned&quot;)    )       )     ), (AND   (IsChanged (Assignee__c),    Change_Request__r.RecordType.DeveloperName = &quot;Mobile&quot; ,     OR   (ISPICKVAL( Overall_Status__c , &quot;Impact Analysis&quot;),     ISPICKVAL( Overall_Status__c , &quot;Planned&quot;) 		) 	   ) 	 ) , (AND   (ISNEW(),    Change_Request__r.RecordType.DeveloperName = &quot;Mobile&quot; ,     OR   (ISPICKVAL( Overall_Status__c , &quot;Impact Analysis&quot;),     ISPICKVAL( Overall_Status__c , &quot;Planned&quot;) 		) 	   ) 	 )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WI%3A Send email when More Info Needed</fullName>
        <actions>
            <name>WI_Send_Email_More_Info_needed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Work_Item__c.Overall_Status__c</field>
            <operation>equals</operation>
            <value>More Info Needed</value>
        </criteriaItems>
        <description>Send email to Ron van Brandenburg and Roel Bogaarts when status of the WI is &apos;More Info Needed&apos;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
