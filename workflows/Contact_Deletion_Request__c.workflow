<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Contact_Del_Request_ANZ</fullName>
        <description>Contact Del. Request ANZ</description>
        <protected>false</protected>
        <recipients>
            <recipient>ANZ_IT</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_APS</fullName>
        <description>Contact Del. Request APS</description>
        <protected>false</protected>
        <recipients>
            <recipient>cynthia.lavalle@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>faten.nassar@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>larin.hammoud@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sara.rifai@medtronic.com.intl</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_Adriatic_West</fullName>
        <description>Contact Del. Request Adriatic West</description>
        <protected>false</protected>
        <recipients>
            <recipient>darija.radisic@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>data.steward.adriatic@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>maja.sostaric@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tijana.kopilovic@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_Armenia</fullName>
        <description>Contact Del. Request Armenia</description>
        <protected>false</protected>
        <recipients>
            <recipient>cynthia.lavalle@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>egecan.duygulu@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sadik.yilmaz@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sara.rifai@medtronic.com.intl</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_Austria</fullName>
        <description>Contact Del. Request Austria</description>
        <protected>false</protected>
        <recipients>
            <recipient>sibylle.nicoladoni@medtronic.com.emeac</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_Baltics</fullName>
        <description>Contact Del. Request Baltics</description>
        <protected>false</protected>
        <recipients>
            <recipient>cynthia.lavalle@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>data.steward.romania@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sara.rifai@medtronic.com.intl</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_Belarus</fullName>
        <description>Contact Del. Request Belarus</description>
        <protected>false</protected>
        <recipients>
            <recipient>cynthia.lavalle@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>data.steward.romania@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sara.rifai@medtronic.com.intl</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_Belgium</fullName>
        <description>Contact Del. Request Belgium</description>
        <protected>false</protected>
        <recipients>
            <recipient>ann.empsten@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_Bulgaria</fullName>
        <description>Contact Del. Request Bulgaria</description>
        <protected>false</protected>
        <recipients>
            <recipient>elitsa.pavlova@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>iva.petkova@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_CANADA</fullName>
        <description>Contact Del. Request CANADA</description>
        <protected>false</protected>
        <recipients>
            <recipient>anne.proulx@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lucie.genest-ferrie@medtronic.com.ds</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_CEE</fullName>
        <description>Contact Del. Request CEE</description>
        <protected>false</protected>
        <recipients>
            <recipient>cynthia.lavalle@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sara.rifai@medtronic.com.intl</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_Czech_Republic</fullName>
        <description>Contact Del. Request Czech Republic</description>
        <protected>false</protected>
        <recipients>
            <recipient>petra.skardova@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vera.grygerova@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_EA</fullName>
        <description>Contact Del. Request EA</description>
        <protected>false</protected>
        <recipients>
            <recipient>dalal.fawzy@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>emergingafrica@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>hanane.hajaji@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_France</fullName>
        <description>Contact Del. Request France</description>
        <protected>false</protected>
        <recipients>
            <recipient>carol.esseiva@medtronic.com.ds</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>francois.ferraris@medtronic.com.ds</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mounira.delye@medtronic.com.ds</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>olivier.aurin@medtronic.com.ds</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ronan.egron@medtronic.com.ds</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_Georgia</fullName>
        <description>Contact Del. Request Georgia</description>
        <protected>false</protected>
        <recipients>
            <recipient>cynthia.lavalle@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>egecan.duygulu@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sadik.yilmaz@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sara.rifai@medtronic.com.intl</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_Germany</fullName>
        <ccEmails>delphi@medtronic.de</ccEmails>
        <description>Contact Del. Request Germany</description>
        <protected>false</protected>
        <recipients>
            <recipient>andrea.hoeveler@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_Greece</fullName>
        <description>Contact Del. Request Greece</description>
        <protected>false</protected>
        <recipients>
            <recipient>maria.alivizatou@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_Hungary</fullName>
        <description>Contact Del. Request Hungary</description>
        <protected>false</protected>
        <recipients>
            <recipient>agnes.tisza@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>katalin.berkes@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>viktoria.kaso@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_Israel</fullName>
        <description>Contact Del. Request Israel</description>
        <protected>false</protected>
        <recipients>
            <recipient>dikla.lanziano@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mirit.aharon@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_Italy</fullName>
        <description>Contact Del. Request Italy</description>
        <protected>false</protected>
        <recipients>
            <recipient>laura.villa2@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_KOR</fullName>
        <description>Contact Del. Request KOR</description>
        <protected>false</protected>
        <recipients>
            <recipient>vladimir.vujatovic@medtronic.com.int</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>GCN_Email_Templates/Contact_Deletion_Request_KOR</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_KZK</fullName>
        <description>Contact Del. Request KZK</description>
        <protected>false</protected>
        <recipients>
            <recipient>data.steward.kaz@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_Netherlands</fullName>
        <description>Contact Del. Request Netherlands</description>
        <protected>false</protected>
        <recipients>
            <recipient>carine.muijtjens@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>chantal.hanssen@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>monique.hoogeveen@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_Nordics</fullName>
        <description>Contact Del. Request Nordics</description>
        <protected>false</protected>
        <recipients>
            <recipient>johanna.pirtinheimo@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>maria.f.prehn@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_Poland</fullName>
        <description>Contact Del. Request Poland</description>
        <protected>false</protected>
        <recipients>
            <recipient>cventcee@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_Portugal</fullName>
        <description>Contact Del. Request Portugal</description>
        <protected>false</protected>
        <recipients>
            <recipient>francisco.segarra.simon@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_Romania_Moldavia</fullName>
        <description>Contact Del. Request Romania Moldavia</description>
        <protected>false</protected>
        <recipients>
            <recipient>data.steward.romania@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>denisa.cristache@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_Russia</fullName>
        <description>Contact Del. Request Russia</description>
        <protected>false</protected>
        <recipients>
            <recipient>amir.al-falit@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vasyliy.arzhalovskiy@medtronic.com.ds</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_SA</fullName>
        <description>Contact Del. Request SA</description>
        <protected>false</protected>
        <recipients>
            <recipient>cynthia.lavalle@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sara.rifai@medtronic.com.intl</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_Saudi_Arabia</fullName>
        <description>Contact Del. Request Saudi Arabia</description>
        <protected>false</protected>
        <recipients>
            <recipient>faten.nassar@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>larin.hammoud@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_Slovakia</fullName>
        <description>Contact Del. Request Slovakia</description>
        <protected>false</protected>
        <recipients>
            <recipient>dominika.candrakova@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>katarina.durajova@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>magdalena.schavelova@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>martina.ambra@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_South_Africa</fullName>
        <description>Contact Del. Request South Africa</description>
        <protected>false</protected>
        <recipients>
            <recipient>diane.pithey@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_Spain</fullName>
        <description>Contact Del. Request Spain</description>
        <protected>false</protected>
        <recipients>
            <recipient>francisco.segarra.simon@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_Switzerland</fullName>
        <description>Contact Del. Request Switzerland</description>
        <protected>false</protected>
        <recipients>
            <recipient>isabel.hollstein@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>laurence.hurni@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sarah.trena@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_TWAL</fullName>
        <description>Contact Del. Request TWAL</description>
        <protected>false</protected>
        <recipients>
            <recipient>arailym.bailenova@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>cynthia.lavalle@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>egecan.duygulu@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>farrukh.z.kiani@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mona.arab@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sadik.yilmaz@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sara.rifai@medtronic.com.intl</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ulpan.zakirova@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_Turkey</fullName>
        <description>Contact Del. Request Turkey</description>
        <protected>false</protected>
        <recipients>
            <recipient>ayten.dal@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>egecan.duygulu@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>hande.ozen@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sadik.yilmaz@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_UK_Ireland</fullName>
        <description>Contact Del. Request UK/Ireland</description>
        <protected>false</protected>
        <recipients>
            <recipient>annie.fidler@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>catherine.ball@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>fiona.widzowski@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>helen.hutton@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>liz.riley@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rima.shah@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Contact_Del_Request_Ukraine</fullName>
        <description>Contact Del. Request Ukraine</description>
        <protected>false</protected>
        <recipients>
            <recipient>cynthia.lavalle@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>data.steward.romania@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sara.rifai@medtronic.com.intl</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_Deletion_Request_email_template</template>
    </alerts>
    <rules>
        <fullName>Contact%3A Notify Deletion Request ANZ</fullName>
        <actions>
            <name>Contact_Del_Request_ANZ</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>AUSTRALIA,NEW ZEALAND</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the ANZ IT public group.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request APS</fullName>
        <actions>
            <name>Contact_Del_Request_APS</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>,QATAR,UTD.ARAB EMIR.,YEMEN,BAHRAIN,KUWAIT,OMAN</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the local data stewards.(APS Region - SAUDI ARABIA excluded)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request Adriatic</fullName>
        <actions>
            <name>Contact_Del_Request_Adriatic_West</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>ALBANIA,BOSNIA-HERZ.,CROATIA,KOSOVO,SLOVENIA,MONTENEGRO,SERBIA,NORTH MACEDONIA</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the local data stewards.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request Armenia</fullName>
        <actions>
            <name>Contact_Del_Request_Armenia</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>ARMENIA</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the local data stewards.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request Austria</fullName>
        <actions>
            <name>Contact_Del_Request_Austria</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>AUSTRIA</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the local data stewards.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request Baltics</fullName>
        <actions>
            <name>Contact_Del_Request_Baltics</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>ESTONIA,LATVIA,LITHUANIA</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the local data stewards.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request Belarus</fullName>
        <actions>
            <name>Contact_Del_Request_Belarus</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>BELARUS</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the local data stewards.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request Belgium</fullName>
        <actions>
            <name>Contact_Del_Request_Belgium</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>BELGIUM,LUXEMBOURG</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the local data stewards.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request Bulgaria</fullName>
        <actions>
            <name>Contact_Del_Request_Bulgaria</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>BULGARIA</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the local data stewards.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request CANADA</fullName>
        <actions>
            <name>Contact_Del_Request_CANADA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>CANADA</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the local data stewards.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request CEE</fullName>
        <actions>
            <name>Contact_Del_Request_CEE</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>ESTONIA,LATVIA,LITHUANIA</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page.(CEE Region - Armenia, Belarus, Bulgaria, Czechia, Georgia, Hungary, Moldova, Poland, Romania, Slovakia, Ukraine, Adriatic excluded)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request Czech Republic</fullName>
        <actions>
            <name>Contact_Del_Request_Czech_Republic</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>CZECH REPUBLIC</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the local data stewards.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request Emerging Africa</fullName>
        <actions>
            <name>Contact_Del_Request_EA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>ALGERIA,ANGOLA,BENIN,BURKINA FASO,BURUNDI,CAMEROON,CENTRAL AFR.REP,CHAD,CONGO,DJIBOUTI,EGYPT,EQUATORIAL GUIN,ERITREA,ETHIOPIA,GABON,GAMBIA,GHANA,GUINEA,GUINEA-BISSAU,IVORY COAST,KENYA,LIBERIA,LIBYA,MALAWI,MALI,MALTA,MAURITANIA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>MOROCCO,NIGER,NIGERIA,RWANDA,SENEGAL,SIERRA LEONE,SOMALIA,SOUTH SUDAN,SUDAN,TANZANIA,TOGO,TUNISIA,UGANDA,ZAMBIA</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the local data stewards.(For Emerging Africa Region)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request France</fullName>
        <actions>
            <name>Contact_Del_Request_France</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>FRANCE</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the local data stewards.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request Georgia</fullName>
        <actions>
            <name>Contact_Del_Request_Georgia</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>GEORGIA</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the local data stewards.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request Germany</fullName>
        <actions>
            <name>Contact_Del_Request_Germany</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>GERMANY</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the german data stewards.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request Greece</fullName>
        <actions>
            <name>Contact_Del_Request_Greece</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>GREECE</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the local data stewards.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request Hungary</fullName>
        <actions>
            <name>Contact_Del_Request_Hungary</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>HUNGARY</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the local data stewards.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request Israel</fullName>
        <actions>
            <name>Contact_Del_Request_Israel</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>ISRAEL</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the local data stewards.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request Italy</fullName>
        <actions>
            <name>Contact_Del_Request_Italy</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>ITALY</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the italian data stewards.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request KOR</fullName>
        <actions>
            <name>Contact_Del_Request_KOR</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>SOUTH KOREA</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the local data stewards.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request KZK</fullName>
        <actions>
            <name>Contact_Del_Request_KZK</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>AZERBAIJAN,KAZAKHSTAN,TAJIKISTAN,TURKMENISTAN,UZBEKISTAN,KIRGHIZIA</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the local data stewards.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request Netherlands</fullName>
        <actions>
            <name>Contact_Del_Request_Netherlands</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>DUTCH ANTILLES,NETHERLANDS</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the local data stewards.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request Nordics</fullName>
        <actions>
            <name>Contact_Del_Request_Nordics</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>NORWAY,SWEDEN,FINLAND,ICELAND,DENMARK</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the local data stewards.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request Poland</fullName>
        <actions>
            <name>Contact_Del_Request_Poland</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>POLAND</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the local data stewards.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request Portugal</fullName>
        <actions>
            <name>Contact_Del_Request_Portugal</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>PORTUGAL</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the contact data steward.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request Romania Moldavia</fullName>
        <actions>
            <name>Contact_Del_Request_Romania_Moldavia</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>MOLDAVIA,ROMANIA</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the local data stewards.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request Russia</fullName>
        <actions>
            <name>Contact_Del_Request_Russia</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>RUSSIAN FED.</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the local data stewards.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request SA</fullName>
        <actions>
            <name>Contact_Del_Request_SA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>BOTSWANA,COMOROS,LESOTHO,MADAGASCAR,MALDIVES,MAURITIUS,MOZAMBIQUE,NAMIBIA,SEYCHELLES,SWAZILAND,ZIMBABWE</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the local data stewards.(SA Region - 
 South Africa excluded)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request Saudi Arabia</fullName>
        <actions>
            <name>Contact_Del_Request_Saudi_Arabia</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>SAUDI ARABIA</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the local data stewards.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request Slovakia</fullName>
        <actions>
            <name>Contact_Del_Request_Slovakia</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>SLOVAKIA</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the local data stewards.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request South Africa</fullName>
        <actions>
            <name>Contact_Del_Request_South_Africa</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>SOUTH AFRICA</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the contact data steward.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request Spain</fullName>
        <actions>
            <name>Contact_Del_Request_Spain</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>SPAIN,ANDORRA,CANARY ISLANDS</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the local data stewards..</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request Switzerland</fullName>
        <actions>
            <name>Contact_Del_Request_Switzerland</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>SWITZERLAND</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the local data stewards.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request TWAL</fullName>
        <actions>
            <name>Contact_Del_Request_TWAL</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>AFGHANISTAN,AZERBAIJAN,CYPRUS,JORDAN,KAZAKHSTAN,LEBANON,SYRIA,TAJIKISTAN,TURKMENISTAN,UZBEKISTAN</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>KIRGHIZIA,IRAN,IRAQ,PAKISTAN</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the local data stewards.(TWAL Region - Turkey excluded)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request Turkey</fullName>
        <actions>
            <name>Contact_Del_Request_Turkey</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>TURKEY</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the local data stewards.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request UK%2FIreland</fullName>
        <actions>
            <name>Contact_Del_Request_UK_Ireland</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>IRELAND,UNITED KINGDOM</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the local data stewards.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Notify Deletion Request Ukraine</fullName>
        <actions>
            <name>Contact_Del_Request_Ukraine</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>UKRAINE</value>
        </criteriaItems>
        <description>Send email to notify contact deletion request has been filed by user, using the &lt;request deletion&gt; button on contact page. Mail is sent to the local data stewards.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
