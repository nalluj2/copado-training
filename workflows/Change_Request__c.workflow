<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notify_change_manager_on_new_change_request</fullName>
        <description>Notify change manager on new change request</description>
        <protected>false</protected>
        <recipients>
            <recipient>chris.lemmens@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>christian.eulenpesch@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>frank.schiepers@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>gerd.balette@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ron.peters@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rudy.de.coninck@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tom.seyen@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Requestor_Lookup__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Change_Requests/Change_request_logged</template>
    </alerts>
    <fieldUpdates>
        <fullName>CR_Change_Recordtype_to_Mobile</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Mobile</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CR: Change Recordtype to Mobile</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fill_Go_Live_Date</fullName>
        <description>Go Live Date filled with Current Date</description>
        <field>Go_Live_Date__c</field>
        <formula>TODAY()</formula>
        <name>Fill Go Live Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Date</fullName>
        <field>Approval_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_to_Can_be_Planned</fullName>
        <description>When CR is approved, status is set to Can be Planned</description>
        <field>Status__c</field>
        <literalValue>Can be planned</literalValue>
        <name>Status to Can be Planned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_to_Imp_Analysis</fullName>
        <description>Status is set to Impact Analysis when CR is recalled from approval process</description>
        <field>Status__c</field>
        <literalValue>Impact Analysis</literalValue>
        <name>Status to Imp Analysis</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_to_Impact_Analysis</fullName>
        <description>Status is set back to Impact Analysis when CR is rejected</description>
        <field>Status__c</field>
        <literalValue>Impact Analysis</literalValue>
        <name>Status to Impact Analysis</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_to_Pending_Approval</fullName>
        <description>Status is set to Pending Approval when CR is submitted for Approval</description>
        <field>Status__c</field>
        <literalValue>Pending Approval</literalValue>
        <name>Status to Pending Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Total_Cost_Approved</fullName>
        <field>Total_Cost_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Total Cost Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Total_Cost_Rejected</fullName>
        <field>Total_Cost_Approved__c</field>
        <literalValue>0</literalValue>
        <name>Total Cost Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UAT_Status_Failed</fullName>
        <field>UAT_Status__c</field>
        <literalValue>Failed</literalValue>
        <name>UAT Status Failed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UAT_Status_Passed</fullName>
        <field>UAT_Status__c</field>
        <literalValue>Passed</literalValue>
        <name>UAT Status Passed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UAT_Status_Waiting_for_Approval</fullName>
        <description>Update UAT Status to Waiting for Approval</description>
        <field>UAT_Status__c</field>
        <literalValue>Waiting for Approval</literalValue>
        <name>UAT Status Waiting for Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Total_Cost_Approved</fullName>
        <field>Total_Cost_Approved__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Total Cost Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>CR%3A Change Recordtype to Mobile</fullName>
        <actions>
            <name>CR_Change_Recordtype_to_Mobile</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Change_Request__c.Tool__c</field>
            <operation>equals</operation>
            <value>MMX,Crossworld,DIMS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Change_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>General</value>
        </criteriaItems>
        <description>Change RT to Mobile when selected tool is MMX or Crossworld or DIMS</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Fill Go Live Date</fullName>
        <actions>
            <name>Fill_Go_Live_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Change_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>Closed,On Hold,Completed</value>
        </criteriaItems>
        <description>Go Live Date will be filled with current date when status is set to On Hold or Closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New change request</fullName>
        <actions>
            <name>Notify_change_manager_on_new_change_request</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Change_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>General,Mobile</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Uncheck Total Cost Approved</fullName>
        <actions>
            <name>Uncheck_Total_Cost_Approved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Un-check Total Cost Approved when Total Cost, Costcenter or Approver changes</description>
        <formula>AND(  NOT(ISNEW())   , Total_Cost_Approved__c = true  , OR(     ISCHANGED( Total_Cost__c )     , ISCHANGED( Cost_Center__c )     , ISCHANGED( Approver__c )  ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
