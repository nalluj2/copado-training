<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Workorder_7_Day_Reminder</fullName>
        <description>Email is sent to new owner when the ownership of a workorder is changed</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ST_Service_Email_Templates/Work_Order_Alert</template>
    </alerts>
    <fieldUpdates>
        <fullName>Do_not_execute_Internal_Use_Only</fullName>
        <field>Is_Executing_Workflow_Internal_once_only__c</field>
        <literalValue>1</literalValue>
        <name>Do not execute Internal Use Only</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Do_not_execute_workflow</fullName>
        <field>Is_Executing_Workflow_Internal_use_only__c</field>
        <literalValue>1</literalValue>
        <name>Do not execute workflow</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Make_Autoguide_System_Install_Read_Only</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Autoguide_System_Installation_Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Make Autoguide System Install Read Only</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Make_Mazor_System_Checkout_Read_Only</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Mazor_System_Checkout_Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Make Mazor System Checkout Read Only</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Make_Mazor_System_Install_Read_Only</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Mazor_System_Installation_Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Make Mazor System Install Read Only</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Make_Mazor_System_PM_Read_Only</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Mazor_System_PM_Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Make Mazor System PM Read Only</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Make_Visualase_Surgery_Coverag_Read_Only</fullName>
        <description>Change the record type make completed or cancelled Visualage Surgery Coverage work orders read-only.</description>
        <field>RecordTypeId</field>
        <lookupValue>Visualase_Surgery_Coverage_Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Make Visualase Surgery Coverag Read Only</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Make_Visualase_System_Checkout_Read_Only</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Visualase_System_Checkout_Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Make Visualase System Checkout Read Only</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Make_Visualase_System_Install_Read_Only</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Visualase_System_Installation_Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Make Visualase System Install Read Only</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Make_Visualase_System_PM_Read_Only</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Visualase_System_PM_Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Make Visualase System PM Read Only</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_Get_Country</fullName>
        <description>The field Account Country on the Workorder is filled with the country of the account</description>
        <field>Account_Country__c</field>
        <formula>TEXT(Account__r.Account_Country_vs__c)</formula>
        <name>WO Get Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Do_not_exec_Polestar_Syst_Instal</fullName>
        <field>Is_Executing_Workflow_Internal_use_only__c</field>
        <literalValue>1</literalValue>
        <name>WO: ST: Do not exec Polestar Syst Instal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Do_not_execute_Make_Training</fullName>
        <field>Is_Executing_Workflow_Internal_use_only__c</field>
        <literalValue>1</literalValue>
        <name>WO: ST: Do not execute Make Training</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Do_not_execute_Nav_System_Instal</fullName>
        <field>Is_Executing_Workflow_Internal_use_only__c</field>
        <literalValue>1</literalValue>
        <name>WO: ST: Do not execute Nav System Instal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Do_not_execute_Nav_System_PM</fullName>
        <field>Is_Executing_Workflow_Internal_use_only__c</field>
        <literalValue>1</literalValue>
        <name>WO: ST: Do not execute Nav System PM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Do_not_execute_O_Arm_Syst_Instal</fullName>
        <field>Is_Executing_Workflow_Internal_use_only__c</field>
        <literalValue>1</literalValue>
        <name>WO: ST: Do not execute O-Arm Syst Instal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Do_not_execute_O_Arm_System_PM</fullName>
        <field>Is_Executing_Workflow_Internal_use_only__c</field>
        <literalValue>1</literalValue>
        <name>WO: ST: Do not execute O-Arm System PM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Do_not_execute_Other_Site_Visit</fullName>
        <field>Is_Executing_Workflow_Internal_use_only__c</field>
        <literalValue>1</literalValue>
        <name>WO: ST: Do not execute Other Site Visit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Do_not_execute_Polestar_Sys_PM</fullName>
        <field>Is_Executing_Workflow_Internal_use_only__c</field>
        <literalValue>1</literalValue>
        <name>WO: ST: Do not execute Polestar Sys PM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Do_not_execute_workflow</fullName>
        <field>Is_Executing_Workflow_Internal_use_only__c</field>
        <literalValue>1</literalValue>
        <name>WO: ST: Do not execute workflow</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Don_t_exec_Eval_Nav_Sys_Checkout</fullName>
        <field>Is_Executing_Workflow_Internal_use_only__c</field>
        <literalValue>1</literalValue>
        <name>WO: ST: Don&apos;t exec Eval Nav Sys Checkout</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Don_t_exec_Polestar_Sys_Checkout</fullName>
        <field>Is_Executing_Workflow_Internal_use_only__c</field>
        <literalValue>1</literalValue>
        <name>WO: ST: Don&apos;t exec Polestar Sys Checkout</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Don_t_execute_Nav_Sys_Checkout</fullName>
        <field>Is_Executing_Workflow_Internal_use_only__c</field>
        <literalValue>1</literalValue>
        <name>WO: ST: Don&apos;t execute Nav Sys Checkout</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Don_t_execute_O_Arm_Sys_Checkout</fullName>
        <field>Is_Executing_Workflow_Internal_use_only__c</field>
        <literalValue>1</literalValue>
        <name>WO: ST: Don&apos;t execute O-Arm Sys Checkout</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Imaging_Assign_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>ST_Imaging</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>WO: ST: Imaging: Assign Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_In_Process_Status_Update</fullName>
        <description>Changes status to In Process once scheduled end date is today or past</description>
        <field>Status__c</field>
        <literalValue>In Process</literalValue>
        <name>WO: ST: In Process Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Make_Eval_Nav_System_Checkout_RO</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Eval_Nav_Checkout_Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>WO: ST: Make Eval Nav System Checkout RO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Make_Mazor_Surgery_Coverage_RO</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Mazor_Surgery_Coverage_Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>WO: ST: Make Mazor Surgery Coverage RO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Make_Nav_System_Checkout_RO</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Nav_System_Checkout_Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>WO: ST: Make Nav System Checkout RO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Make_Nav_System_Install_RO</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Nav_System_Installation_Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>WO: ST: Make Nav System Install RO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Make_Nav_System_PM_Read_Only</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Nav_PM_Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>WO:ST: Make Nav System PM Read Only</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Make_O2_System_Checkout_RO</fullName>
        <field>RecordTypeId</field>
        <lookupValue>O2_System_Checkout_Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>WO: ST: Make O2 System Checkout RO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Make_O2_System_Install_Read_Only</fullName>
        <field>RecordTypeId</field>
        <lookupValue>O2_System_Installation_Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>WO: ST: Make O2 System Install Read Only</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Make_O2_System_PM_Read_Only</fullName>
        <field>RecordTypeId</field>
        <lookupValue>O2_System_PM_Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>WO: ST: Make O2 System PM Read Only</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Make_O_Arm_System_Install_RO</fullName>
        <field>RecordTypeId</field>
        <lookupValue>O_Arm_System_Install_Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>WO: ST: Make O-Arm System Install RO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Make_O_Arm_System_PM_RO</fullName>
        <field>RecordTypeId</field>
        <lookupValue>O_Arm_System_PM_Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>WO: ST: Make O-Arm System PM RO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Make_O_arm_System_Checkout_RO</fullName>
        <field>RecordTypeId</field>
        <lookupValue>O_Arm_System_Checkout_Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>WO: ST: Make O-arm System Checkout RO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Make_Other_Site_Visit_Read_Only</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Other_Site_Visit_Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>WO: ST: Make Other Site Visit Read Only</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Make_PoleStar_System_Checkout_RO</fullName>
        <field>RecordTypeId</field>
        <lookupValue>PoleStar_System_Checkout_Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>WO: ST: Make PoleStar System Checkout RO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Make_PoleStar_System_Install_RO</fullName>
        <field>RecordTypeId</field>
        <lookupValue>PoleStar_System_Install_Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>WO: ST: Make PoleStar System Install RO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Make_PoleStar_System_PM_RO</fullName>
        <field>RecordTypeId</field>
        <lookupValue>PoleStar_System_PM_Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>WO: ST: Make PoleStar System PM RO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Make_Surgery_Coverage_Read_Only</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Surgery_Coverage_Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>WO: ST: Make Surgery Coverage Read Only</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Make_Training_Read_Only</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Training_Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>WO: ST: Make Training Read Only</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Nav_Assign_Owner_BCEGI</fullName>
        <field>OwnerId</field>
        <lookupValue>ST_Navigation_BCEGI</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>WO: ST: Nav: Assign Owner - BCEGI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Nav_Assign_Owner_France</fullName>
        <field>OwnerId</field>
        <lookupValue>ST_Navigation_France</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>WO: ST: Nav: Assign Owner - France</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Nav_Assign_Owner_Germany</fullName>
        <field>OwnerId</field>
        <lookupValue>ST_Navigation_Germany</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>WO: ST: Nav: Assign Owner - Germany</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Nav_Assign_Owner_Iberia</fullName>
        <field>OwnerId</field>
        <lookupValue>ST_Navigation_Iberia</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>WO: ST: Nav: Assign Owner - Iberia</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Nav_Assign_Owner_Italy</fullName>
        <field>OwnerId</field>
        <lookupValue>ST_Navigation_Italy</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>WO_ST_Nav_Assign_Owner_Italy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Nav_Assign_Owner_NBP</fullName>
        <field>OwnerId</field>
        <lookupValue>ST_Navigation_NBP</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>WO: ST: Nav: Assign Owner - NBP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Nav_Assign_Owner_UK_Ireland</fullName>
        <field>OwnerId</field>
        <lookupValue>ST_Navigation_UK_Ireland</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>WO: ST: Nav: Assign Owner - UK/Ireland</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Scheduled_Status_Update</fullName>
        <description>Changes Status to Scheduled</description>
        <field>Status__c</field>
        <literalValue>Scheduled</literalValue>
        <name>WO: ST: Scheduled Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Set_Reopen_to_TRUE</fullName>
        <description>Used in conjunction with Worflow &apos;WO: ST: Set Reopen&apos;.</description>
        <field>Reopened__c</field>
        <literalValue>1</literalValue>
        <name>WO: ST: Set Reopen to TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Update_Date_Closed_on_Work_Order</fullName>
        <field>Date_Closed__c</field>
        <formula>TODAY()</formula>
        <name>WO: ST: Update Date Closed on Work Order</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Update_Num_SW_Upgrades</fullName>
        <description>ST: Updates # SW Updates field for Nav System Checkouts</description>
        <field>Number_of_Software_Upgraded__c</field>
        <literalValue>1</literalValue>
        <name>WO: ST: Update Num SW Upgrades</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_ST_Update_Num_SW_Upgrades_to_2</fullName>
        <field>Number_of_Software_Upgraded__c</field>
        <literalValue>2</literalValue>
        <name>WO: ST: Update Num SW Upgrades to 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Workorder_Set_Close_Date</fullName>
        <description>The DATE CLOSED field of the Workorder is set to TODAY() when the status is set to CLOSED</description>
        <field>Date_Closed__c</field>
        <formula>TODAY()</formula>
        <name>Workorder Set Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>WO Get Country</fullName>
        <actions>
            <name>WO_Get_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Workorder__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This workflow gets the Country from the related Account</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Imaging%3A Assign Owner</fullName>
        <actions>
            <name>WO_ST_Imaging_Assign_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Assign owner to WO when this is created automatically out of a case (WO is linked to a case)</description>
        <formula>AND ( Case__c &lt;&gt; &quot;&quot;, OR ( $RecordType.DeveloperName = &quot;O_Arm_System_Checkout&quot;, $RecordType.DeveloperName = &quot;O_Arm_System_Checkout_Closed&quot;, $RecordType.DeveloperName = &quot;O_Arm_System_Installation&quot;, $RecordType.DeveloperName = &quot;O_Arm_System_Install_Closed&quot;, $RecordType.DeveloperName = &quot;O_Arm_PM&quot;, $RecordType.DeveloperName = &quot;O_Arm_System_PM_Closed&quot;, $RecordType.DeveloperName = &quot;PoleStar_Checkout&quot;, $RecordType.DeveloperName = &quot;PoleStar_System_Checkout_Closed&quot;, $RecordType.DeveloperName = &quot;PoleStar_System_Install_Closed&quot;, $RecordType.DeveloperName = &quot;PoleStar_System_Installation&quot;, $RecordType.DeveloperName = &quot;PoleStar_PM&quot;, $RecordType.DeveloperName = &quot;PoleStar_System_PM_Closed&quot; ) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Make Autoguide System Install Read Only</fullName>
        <actions>
            <name>Do_not_execute_workflow</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Make_Autoguide_System_Install_Read_Only</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Workorder__c.Status__c</field>
            <operation>equals</operation>
            <value>Cancelled,Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Workorder__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Autoguide System Installation</value>
        </criteriaItems>
        <description>Change record type to read only when statue is complete on Autoguide System Installation</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Make Eval Nav System Checkout RO</fullName>
        <actions>
            <name>WO_ST_Don_t_exec_Eval_Nav_Sys_Checkout</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WO_ST_Make_Eval_Nav_System_Checkout_RO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Workorder__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed,Cancelled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Workorder__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Eval Nav System Checkout</value>
        </criteriaItems>
        <description>Change record type to read only when status is complete on Eval Nav System Checkout</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Make Mazor Surgery Coverage Read Only</fullName>
        <actions>
            <name>WO_ST_Do_not_execute_workflow</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WO_ST_Make_Mazor_Surgery_Coverage_RO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Workorder__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Mazor Surgery Coverage</value>
        </criteriaItems>
        <criteriaItems>
            <field>Workorder__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed,Cancelled</value>
        </criteriaItems>
        <description>Change record type to read only when status is complete on Mazor Surgery Coverage</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Make Mazor System Checkout Read Only</fullName>
        <actions>
            <name>Do_not_execute_Internal_Use_Only</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Make_Mazor_System_Checkout_Read_Only</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Workorder__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed,Cancelled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Workorder__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Mazor System Checkout</value>
        </criteriaItems>
        <description>Change record type to read only when statue is complete on Mazor System Checkout</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Make Mazor System Install Read Only</fullName>
        <actions>
            <name>Do_not_execute_Internal_Use_Only</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Make_Mazor_System_Install_Read_Only</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Workorder__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed,Cancelled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Workorder__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Mazor System Installation</value>
        </criteriaItems>
        <description>Change record type to read only when statue is complete on Mazor System Install</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Make Mazor System PM Read Only</fullName>
        <actions>
            <name>Do_not_execute_Internal_Use_Only</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Make_Mazor_System_PM_Read_Only</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Workorder__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed,Cancelled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Workorder__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Mazor System PM</value>
        </criteriaItems>
        <description>Change record type to read only when statue is complete on Mazor System PM</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Make Nav System Checkout Read Only</fullName>
        <actions>
            <name>WO_ST_Don_t_execute_Nav_Sys_Checkout</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WO_ST_Make_Nav_System_Checkout_RO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Workorder__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed,Cancelled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Workorder__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Nav System Checkout</value>
        </criteriaItems>
        <description>Change record type to read only when status is complete on record type Nav System checkout</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Make Nav System Install Read Only</fullName>
        <actions>
            <name>WO_ST_Do_not_execute_Nav_System_Instal</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WO_ST_Make_Nav_System_Install_RO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Workorder__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed,Cancelled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Workorder__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Nav System Installation</value>
        </criteriaItems>
        <description>Change record type to read only when status is complete on Nav System Installation</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Make Nav System PM Read Only</fullName>
        <actions>
            <name>WO_ST_Do_not_execute_Nav_System_PM</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WO_ST_Make_Nav_System_PM_Read_Only</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Workorder__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed,Cancelled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Workorder__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Nav System PM</value>
        </criteriaItems>
        <description>Change record type to read only when status is complete on Nav System PM</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Make O1 System Checkout Read Only</fullName>
        <actions>
            <name>WO_ST_Don_t_execute_O_Arm_Sys_Checkout</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WO_ST_Make_O_arm_System_Checkout_RO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Workorder__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>O1 System Checkout</value>
        </criteriaItems>
        <criteriaItems>
            <field>Workorder__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed,Cancelled</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Make O1 System Install Read Only</fullName>
        <actions>
            <name>WO_ST_Do_not_execute_O_Arm_Syst_Instal</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WO_ST_Make_O_Arm_System_Install_RO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Workorder__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>O1 System Installation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Workorder__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed,Cancelled</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Make O1 System PM Read Only</fullName>
        <actions>
            <name>WO_ST_Do_not_execute_O_Arm_System_PM</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WO_ST_Make_O_Arm_System_PM_RO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Workorder__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>O1 System PM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Workorder__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed,Cancelled</value>
        </criteriaItems>
        <description>Change record type to read only when status is complete on O1 System PM</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Make O2 System Checkout Read Only</fullName>
        <actions>
            <name>WO_ST_Don_t_execute_O_Arm_Sys_Checkout</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WO_ST_Make_O2_System_Checkout_RO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Workorder__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed,Cancelled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Workorder__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>O2 System Checkout</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Make O2 System Install Read Only</fullName>
        <actions>
            <name>WO_ST_Do_not_execute_O_Arm_Syst_Instal</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WO_ST_Make_O2_System_Install_Read_Only</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Workorder__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed,Cancelled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Workorder__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>O2 System Installation</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Make O2 System PM Read Only</fullName>
        <actions>
            <name>WO_ST_Do_not_execute_O_Arm_System_PM</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WO_ST_Make_O2_System_PM_Read_Only</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Workorder__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed,Cancelled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Workorder__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>O2 System PM</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Make Other Site Visit Read Only</fullName>
        <actions>
            <name>WO_ST_Do_not_execute_Other_Site_Visit</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WO_ST_Make_Other_Site_Visit_Read_Only</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Workorder__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Other Site Visit</value>
        </criteriaItems>
        <criteriaItems>
            <field>Workorder__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed,Cancelled</value>
        </criteriaItems>
        <description>Change record type to read only when status is complete on Other Site Visit</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Make PoleStar System Checkout Read Only</fullName>
        <actions>
            <name>WO_ST_Don_t_exec_Polestar_Sys_Checkout</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WO_ST_Make_PoleStar_System_Checkout_RO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Workorder__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>PoleStar System Checkout</value>
        </criteriaItems>
        <criteriaItems>
            <field>Workorder__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed,Cancelled</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Make PoleStar System Install Read Only</fullName>
        <actions>
            <name>WO_ST_Do_not_exec_Polestar_Syst_Instal</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WO_ST_Make_PoleStar_System_Install_RO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Workorder__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>PoleStar System Installation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Workorder__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed,Cancelled</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Make PoleStar System PM Read Only</fullName>
        <actions>
            <name>WO_ST_Do_not_execute_Polestar_Sys_PM</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WO_ST_Make_PoleStar_System_PM_RO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Workorder__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>PoleStar System PM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Workorder__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed,Cancelled</value>
        </criteriaItems>
        <description>Change record type to read only when status is complete on PoleStar System PM</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Make Surgery Coverage Read Only</fullName>
        <actions>
            <name>WO_ST_Do_not_execute_workflow</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WO_ST_Make_Surgery_Coverage_Read_Only</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Workorder__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Surgery Coverage</value>
        </criteriaItems>
        <criteriaItems>
            <field>Workorder__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed,Cancelled</value>
        </criteriaItems>
        <description>Change record type to read only when status is complete on Surgery Coverage</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Make Training Read Only</fullName>
        <actions>
            <name>WO_ST_Do_not_execute_Make_Training</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WO_ST_Make_Training_Read_Only</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Workorder__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Training</value>
        </criteriaItems>
        <criteriaItems>
            <field>Workorder__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed,Cancelled</value>
        </criteriaItems>
        <description>Change record type to read only when status is complete on Training</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Make Visualase Surgery Coverage Read Only</fullName>
        <actions>
            <name>Do_not_execute_workflow</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Make_Visualase_Surgery_Coverag_Read_Only</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Workorder__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Visualase Surgery Coverage</value>
        </criteriaItems>
        <criteriaItems>
            <field>Workorder__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed,Cancelled</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Make Visualase System Checkout Read Only</fullName>
        <actions>
            <name>Make_Visualase_System_Checkout_Read_Only</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Workorder__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Visualase System Checkout</value>
        </criteriaItems>
        <criteriaItems>
            <field>Workorder__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed,Cancelled</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Make Visualase System Install Read Only</fullName>
        <actions>
            <name>Make_Visualase_System_Install_Read_Only</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Workorder__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Visualase System Installation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Workorder__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed,Cancelled</value>
        </criteriaItems>
        <description>Change record type to read only when statue is complete on Visualase System Installation</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Make Visualase System PM Read Only</fullName>
        <actions>
            <name>Make_Visualase_System_PM_Read_Only</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Workorder__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Visualase System PM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Workorder__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed,Cancelled</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Nav%3A Assign Owner - BCEGI</fullName>
        <actions>
            <name>WO_ST_Nav_Assign_Owner_BCEGI</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Assign owner to WO when this is created automatically out of a case (WO is linked to a case)</description>
        <formula>AND (  Case__c &lt;&gt; &quot;&quot;,  OR(  TEXT(Account__r.Account_Country_vs__c) = &quot;BELGIUM&quot;,  TEXT(Account__r.Account_Country_vs__c) = &quot;LUXEMBOURG&quot;,  TEXT(Account__r.Account_Country_vs__c) = &quot;NETHERLANDS&quot;,  TEXT(Account__r.Account_Country_vs__c) = &quot;SWITZERLAND&quot;,  TEXT(Account__r.Account_Country_vs__c) = &quot;AUSTRIA&quot;,  TEXT(Account__r.Account_Country_vs__c) = &quot;GREECE&quot;,  TEXT(Account__r.Account_Country_vs__c) = &quot;ISRAEL&quot;,  TEXT(Account__r.Account_Country_vs__c) = &quot;HUNGARY&quot;,  TEXT(Account__r.Account_Country_vs__c) = &quot;ROMANIA&quot;,  TEXT(Account__r.Account_Country_vs__c) = &quot;MOLDAVIA&quot;,  TEXT(Account__r.Account_Country_vs__c) = &quot;SLOVAKIA&quot;,  TEXT(Account__r.Account_Country_vs__c) = &quot;CZECH REPUBLIC&quot;,  TEXT(Account__r.Account_Country_vs__c) = &quot;ALBANIA&quot;,  TEXT(Account__r.Account_Country_vs__c) = &quot;BOSNIA-HERZ.&quot;,  TEXT(Account__r.Account_Country_vs__c) = &quot;CROATIA&quot;,  TEXT(Account__r.Account_Country_vs__c) = &quot;SLOVENIA&quot;,  TEXT(Account__r.Account_Country_vs__c) = &quot;KOSOVO&quot;,  TEXT(Account__r.Account_Country_vs__c) = &quot;SERBIA&quot;,  TEXT(Account__r.Account_Country_vs__c) = &quot;BULGARIA&quot;,  TEXT(Account__r.Account_Country_vs__c) = &quot;NORTH MACEDONIA&quot;,  TEXT(Account__r.Account_Country_vs__c) = &quot;MONTENEGRO&quot;,  TEXT(Account__r.Account_Country_vs__c) = &quot;ARMANIA&quot;,  TEXT(Account__r.Account_Country_vs__c) = &quot;BELARUS&quot;,  TEXT(Account__r.Account_Country_vs__c) = &quot;GEORGIA&quot;,  TEXT(Account__r.Account_Country_vs__c) = &quot;UKRAINE&quot;  ),  OR  (  $RecordType.DeveloperName = &quot;Eval_Nav_Checkout&quot;,  $RecordType.DeveloperName = &quot;Eval_Nav_Checkout_Closed&quot;,  $RecordType.DeveloperName = &quot;Nav_Checkout&quot;,  $RecordType.DeveloperName = &quot;Nav_System_Checkout_Closed&quot;,  $RecordType.DeveloperName = &quot;Nav_Installation&quot;,  $RecordType.DeveloperName = &quot;Nav_System_Installation_Closed&quot;,  $RecordType.DeveloperName = &quot;Nav_PM&quot;,  $RecordType.DeveloperName = &quot;Nav_PM_Closed&quot;  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Nav%3A Assign Owner - France</fullName>
        <actions>
            <name>WO_ST_Nav_Assign_Owner_France</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Assign owner to WO when this is created automatically out of a case (WO is linked to a case)</description>
        <formula>AND ( Case__c &lt;&gt; &quot;&quot;, TEXT(Account__r.Account_Country_vs__c) = &quot;FRANCE&quot;, OR ( $RecordType.DeveloperName = &quot;Eval_Nav_Checkout&quot;, $RecordType.DeveloperName = &quot;Eval_Nav_Checkout_Closed&quot;, $RecordType.DeveloperName = &quot;Nav_Checkout&quot;, $RecordType.DeveloperName = &quot;Nav_System_Checkout_Closed&quot;, $RecordType.DeveloperName = &quot;Nav_Installation&quot;, $RecordType.DeveloperName = &quot;Nav_System_Installation_Closed&quot;, $RecordType.DeveloperName = &quot;Nav_PM&quot;, $RecordType.DeveloperName = &quot;Nav_PM_Closed&quot; ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Nav%3A Assign Owner - Germany</fullName>
        <actions>
            <name>WO_ST_Nav_Assign_Owner_Germany</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Assign owner to WO when this is created automatically out of a case (WO is linked to a case)</description>
        <formula>AND ( Case__c &lt;&gt; &quot;&quot;, TEXT(Account__r.Account_Country_vs__c) = &quot;GERMANY&quot;, OR ( $RecordType.DeveloperName = &quot;Eval_Nav_Checkout&quot;, $RecordType.DeveloperName = &quot;Eval_Nav_Checkout_Closed&quot;, $RecordType.DeveloperName = &quot;Nav_Checkout&quot;, $RecordType.DeveloperName = &quot;Nav_System_Checkout_Closed&quot;, $RecordType.DeveloperName = &quot;Nav_Installation&quot;, $RecordType.DeveloperName = &quot;Nav_System_Installation_Closed&quot;, $RecordType.DeveloperName = &quot;Nav_PM&quot;, $RecordType.DeveloperName = &quot;Nav_PM_Closed&quot; ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Nav%3A Assign Owner - Iberia</fullName>
        <actions>
            <name>WO_ST_Nav_Assign_Owner_Iberia</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Assign owner to WO when this is created automatically out of a case (WO is linked to a case)</description>
        <formula>AND (  Case__c &lt;&gt; &quot;&quot;,  OR(  TEXT(Account__r.Account_Country_vs__c) = &quot;SPAIN&quot;,  TEXT(Account__r.Account_Country_vs__c) = &quot;PORTUGAL&quot;  ),  OR  (  $RecordType.DeveloperName = &quot;Eval_Nav_Checkout&quot;,  $RecordType.DeveloperName = &quot;Eval_Nav_Checkout_Closed&quot;,  $RecordType.DeveloperName = &quot;Nav_Checkout&quot;,  $RecordType.DeveloperName = &quot;Nav_System_Checkout_Closed&quot;,  $RecordType.DeveloperName = &quot;Nav_Installation&quot;,  $RecordType.DeveloperName = &quot;Nav_System_Installation_Closed&quot;,  $RecordType.DeveloperName = &quot;Nav_PM&quot;,  $RecordType.DeveloperName = &quot;Nav_PM_Closed&quot;  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Nav%3A Assign Owner - Italy</fullName>
        <actions>
            <name>WO_ST_Nav_Assign_Owner_Italy</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Assign owner to WO when this is created automatically out of a case (WO is linked to a case)</description>
        <formula>AND ( Case__c &lt;&gt; &quot;&quot;, TEXT(Account__r.Account_Country_vs__c) = &quot;ITALY&quot;, OR ( $RecordType.DeveloperName = &quot;Eval_Nav_Checkout&quot;, $RecordType.DeveloperName = &quot;Eval_Nav_Checkout_Closed&quot;, $RecordType.DeveloperName = &quot;Nav_Checkout&quot;, $RecordType.DeveloperName = &quot;Nav_System_Checkout_Closed&quot;, $RecordType.DeveloperName = &quot;Nav_Installation&quot;, $RecordType.DeveloperName = &quot;Nav_System_Installation_Closed&quot;, $RecordType.DeveloperName = &quot;Nav_PM&quot;, $RecordType.DeveloperName = &quot;Nav_PM_Closed&quot; ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Nav%3A Assign Owner - NBP</fullName>
        <actions>
            <name>WO_ST_Nav_Assign_Owner_NBP</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Assign owner to WO when this is created automatically out of a case (WO is linked to a case)</description>
        <formula>AND (  Case__c &lt;&gt; &quot;&quot;,  OR(  TEXT(Account__r.Account_Country_vs__c) = &quot;POLAND&quot;,  TEXT(Account__r.Account_Country_vs__c) = &quot;DENMARK&quot;,  TEXT(Account__r.Account_Country_vs__c) = &quot;FINLAND&quot;,  TEXT(Account__r.Account_Country_vs__c) = &quot;NORWAY&quot;,  TEXT(Account__r.Account_Country_vs__c) = &quot;SWEDEN&quot;,  TEXT(Account__r.Account_Country_vs__c) = &quot;ESTONIA&quot;,  TEXT(Account__r.Account_Country_vs__c) = &quot;LATVIA&quot;,  TEXT(Account__r.Account_Country_vs__c) = &quot;LITHUANIA&quot;  ),  OR  (  $RecordType.DeveloperName = &quot;Eval_Nav_Checkout&quot;,  $RecordType.DeveloperName = &quot;Eval_Nav_Checkout_Closed&quot;,  $RecordType.DeveloperName = &quot;Nav_Checkout&quot;,  $RecordType.DeveloperName = &quot;Nav_System_Checkout_Closed&quot;,  $RecordType.DeveloperName = &quot;Nav_Installation&quot;,  $RecordType.DeveloperName = &quot;Nav_System_Installation_Closed&quot;,  $RecordType.DeveloperName = &quot;Nav_PM&quot;,  $RecordType.DeveloperName = &quot;Nav_PM_Closed&quot;  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Nav%3A Assign Owner - UK%2FIreland</fullName>
        <actions>
            <name>WO_ST_Nav_Assign_Owner_UK_Ireland</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Assign owner to WO when this is created automatically out of a case (WO is linked to a case)</description>
        <formula>AND (  Case__c &lt;&gt; &quot;&quot;,  OR(  TEXT(Account__r.Account_Country_vs__c) = &quot;IRELAND&quot;,  TEXT(Account__r.Account_Country_vs__c) = &quot;UNITED KINGDOM&quot;  ),  OR  (  $RecordType.DeveloperName = &quot;Eval_Nav_Checkout&quot;,  $RecordType.DeveloperName = &quot;Eval_Nav_Checkout_Closed&quot;,  $RecordType.DeveloperName = &quot;Nav_Checkout&quot;,  $RecordType.DeveloperName = &quot;Nav_System_Checkout_Closed&quot;,  $RecordType.DeveloperName = &quot;Nav_Installation&quot;,  $RecordType.DeveloperName = &quot;Nav_System_Installation_Closed&quot;,  $RecordType.DeveloperName = &quot;Nav_PM&quot;,  $RecordType.DeveloperName = &quot;Nav_PM_Closed&quot;  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Owner Change Notification</fullName>
        <actions>
            <name>Workorder_7_Day_Reminder</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(    ISCHANGED(OwnerId),    CONTAINS(RecordType.Name,&quot;Checkout&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Scheduled Status Updates</fullName>
        <actions>
            <name>WO_ST_Scheduled_Status_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Makes status = Scheduled if Scheduled end date is in the future and makes it in process when scheduled end date is today or in the past</description>
        <formula>AND( NOT($RecordType.DeveloperName = &quot;EUR_Technical_Service&quot;), NOT($RecordType.DeveloperName = &quot;EUR_Service_Order&quot;), NOT($RecordType.DeveloperName = &quot;Small_Capital_Field_Service_Order&quot;), OR(    AND(   NOT(ISPICKVAL( Status__c , &quot;Completed&quot;)),   NOT(ISPICKVAL( Status__c , &quot;Cancelled&quot;)),     Scheduled_End_Date_Time__c &gt;  NOW()     ),    AND(   ISPICKVAL( Status__c , &quot;Scheduled&quot;),   Scheduled_End_Date_Time__c &lt;  NOW()  )  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>WO_ST_In_Process_Status_Update</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Workorder__c.Scheduled_End_Date_Time__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Set Reopen</fullName>
        <actions>
            <name>WO_ST_Set_Reopen_to_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets hidden field “Reopened” to True when status changes from “Completed” or &apos;&apos;Cancelled&quot; to &quot;In Process&quot;.</description>
        <formula>AND( ISPICKVAL(Status__c, &quot;In Process&quot;), OR( ISPICKVAL(PRIORVALUE(Status__c) , &quot;Completed&quot;),  ISPICKVAL(PRIORVALUE(Status__c) , &quot;Cancelled&quot;)  ),  NOT(RecordType.DeveloperName = &quot;EUR_Service_Order&quot;),  NOT(RecordType.DeveloperName = &quot;EUR_Technical_Service&quot;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Update Date Closed on Work Order</fullName>
        <actions>
            <name>WO_ST_Update_Date_Closed_on_Work_Order</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Workorder__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Workorder__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>EUR Technical Service,EUR Service Order,Small Capital Service Order</value>
        </criteriaItems>
        <description>Update the date closed on the work order when a user &apos;completes&apos; the work order</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Update Num SW Upgrades</fullName>
        <actions>
            <name>WO_ST_Update_Num_SW_Upgrades</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( OR( RecordType.Name = &quot;Nav System Checkout&quot;, RecordType.Name = &quot;Nav System Checkout (Closed)&quot; ), NOT(ISBLANK(Software_1__c)), ISBLANK(Software_2__c) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO%3A ST%3A Update Num SW Upgrades to 2</fullName>
        <actions>
            <name>WO_ST_Update_Num_SW_Upgrades_to_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used to update num SW Upgrades on the WO for Nav checkout when both SW1 and SW2 are filled out</description>
        <formula>AND( OR( RecordType.Name = &quot;Nav System Checkout&quot;, RecordType.Name = &quot;Nav System Checkout (Closed)&quot; ), NOT(ISBLANK(Software_1__c)), NOT(ISBLANK(Software_2__c)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Workorder Set Close Date</fullName>
        <actions>
            <name>Workorder_Set_Close_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Workorder__c.Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Workorder__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>EUR Technical Service,Small Capital Service Order</value>
        </criteriaItems>
        <description>The DATE CLOSED field of the Workorder is set to TODAY() when the status of the Workorder is set to CLOSED</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
