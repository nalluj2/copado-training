<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Implant_CRM_Info_Needed</fullName>
        <description>Implant: CRM Info Needed</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Implant_CRM_Info_Needed</template>
    </alerts>
    <alerts>
        <fullName>Implant_MMX_As_Owner</fullName>
        <description>Implant: MMX As Owner</description>
        <protected>false</protected>
        <recipients>
            <recipient>jan.kebeck@medtronic.nl</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ron.van.brandenburg@medtronic.com2</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Implant_MMX_Owner</template>
    </alerts>
    <fieldUpdates>
        <fullName>Implant_Country</fullName>
        <description>Country Text field is filled with the value from Account Country</description>
        <field>Account_Country_Text__c</field>
        <formula>TEXT(Implant_Implanting_Account__r.Account_Country_vs__c)</formula>
        <name>Implant Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Implant_Duration_Empty</fullName>
        <field>Duration_Nr__c</field>
        <name>Implant: Duration Empty</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>India_Distributor_CRDM_RT</fullName>
        <description>CRDM Recordtype choice based on choice of BU</description>
        <field>RecordTypeId</field>
        <lookupValue>India_CRDM_Implant</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>India Distributor CRDM RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>India_Distributor_CS_RT</fullName>
        <description>Assignment of CS recordtype to Implant when for Distributor</description>
        <field>RecordTypeId</field>
        <lookupValue>India_CS_Implant</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>India Distributor CS RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>India_Distributor_Diabetes_RT</fullName>
        <description>Assignment of Diabetes recordtype to Implant when for Distributor</description>
        <field>RecordTypeId</field>
        <lookupValue>India_Diabetes_Implant</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>India Distributor Diabetes RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>India_Distributor_SB_RT</fullName>
        <description>Assignment of S&amp;B recordtype to Implant when for Distributor</description>
        <field>RecordTypeId</field>
        <lookupValue>India_Spine_Implant</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>India Distributor SB RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>India_Distributor_ST_RT</fullName>
        <description>Record Type assignment based on BU</description>
        <field>RecordTypeId</field>
        <lookupValue>India_ST_Implant</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>India Distributor ST RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Implant Country</fullName>
        <actions>
            <name>Implant_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Implant__c.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Country Text field is filled with Account Country, field is needed for sharing rules</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Implant%3A CRM Info Checked</fullName>
        <actions>
            <name>Implant_CRM_Info_Needed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( PRIORVALUE(CRM_Info_Bool__c)=FALSE, CRM_Info_Bool__c=TRUE )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Implant%3A MMX as Owner</fullName>
        <actions>
            <name>Implant_MMX_As_Owner</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Implant__c.OwnerId</field>
            <operation>contains</operation>
            <value>MMX</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Implant%3A Unattended Implant No Duration</fullName>
        <actions>
            <name>Implant_Duration_Empty</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Implant__c.Attended_Implant_Text__c</field>
            <operation>equals</operation>
            <value>,Unattended</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>India Distributor CRDM RT</fullName>
        <actions>
            <name>India_Distributor_CRDM_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Implant__c.Business_Unit_Text__c</field>
            <operation>equals</operation>
            <value>India - CRDM,ASEAN - CRDM</value>
        </criteriaItems>
        <description>Assignment of CRDM recordtype to Implant when for Distributor</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>India Distributor CS RT</fullName>
        <actions>
            <name>India_Distributor_CS_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Implant__c.Business_Unit_Text__c</field>
            <operation>equals</operation>
            <value>India - Cardio Vascular</value>
        </criteriaItems>
        <description>Assignment of CS recordtype to Implant when for Distributor</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>India Distributor Diabetes RT</fullName>
        <actions>
            <name>India_Distributor_Diabetes_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Implant__c.Business_Unit_Text__c</field>
            <operation>equals</operation>
            <value>India - Diabetes</value>
        </criteriaItems>
        <description>Assignment of Diabetes recordtype to Implant when for Distributor</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>India Distributor SB RT</fullName>
        <actions>
            <name>India_Distributor_SB_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Implant__c.Business_Unit_Text__c</field>
            <operation>equals</operation>
            <value>India - Spine &amp; Biologics</value>
        </criteriaItems>
        <description>Assignment of S&amp;B recordtype to Implant when for Distributor</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>India Distributor ST RT</fullName>
        <actions>
            <name>India_Distributor_ST_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Implant__c.Business_Unit_Text__c</field>
            <operation>equals</operation>
            <value>India - Surgical Technologies</value>
        </criteriaItems>
        <description>Assignment of Surgical Technologies recordtype to Implant when for Distributor</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
