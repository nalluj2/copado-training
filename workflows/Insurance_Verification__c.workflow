<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>IV_CAN_DIB_Update_HIDDEN_Opp_IT_HI_Prod</fullName>
        <description>Insurance Verfication: Update the HIDDEN_Opp_IT_HI_Prod_Unique with concatenation of the fields Opportunity, Insurer Type, Health Insurer and Product</description>
        <field>HIDDEN_Opp_IT_HI_Prod_Unique__c</field>
        <formula>Opportunity__r.Name &amp; &quot; - &quot; &amp; TEXT(Insurer_Type__c) &amp; &quot; - &quot; &amp;   Health_Insurer__c &amp; &quot; - &quot; &amp;   TEXT(Product__c)</formula>
        <name>IV: CAN DIB Update HIDDEN_Opp_IT_HI_Prod</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IV_CAN_DIB_Update_Health_Insurer</fullName>
        <description>Insurance Verification: Update the Health Insurer field with the value from the field HIDDEN_Health_Insurer on both creation and update</description>
        <field>Health_Insurer__c</field>
        <formula>HIDDEN_Health_Insurer__c</formula>
        <name>IV: CAN DIB Update Health Insurer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Insurance Verfication%3A CAN DIB Update HIDDEN_Opp_IT_HI_Prod_Unique</fullName>
        <actions>
            <name>IV_CAN_DIB_Update_HIDDEN_Opp_IT_HI_Prod</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Insurance_Verification__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Insurance Verfication: Update the HIDDEN_Opp_IT_HI_Prod_Unique with concatenation of the fields Opportunity, Insurer Type, Health Insurer and Product</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Insurance Verfication%3A CAN DIB Update Health Insurer</fullName>
        <actions>
            <name>IV_CAN_DIB_Update_Health_Insurer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Insurance_Verification__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Insurance Verfication: Update the Health Insurer field with the value from the field HIDDEN_Health_Insurer on both creation and update</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
