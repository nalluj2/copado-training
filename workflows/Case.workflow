<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ANZ_CRDM_Clearcase_email_escalation</fullName>
        <description>ANZ CRDM Clearcase email escalation</description>
        <protected>false</protected>
        <recipients>
            <field>Sales_Rep_Lookup__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ANZ_Email_Template/ANZ_Carelink_Escalation</template>
    </alerts>
    <alerts>
        <fullName>Case_ANZ_Carelink_Escalation_Email_Alert</fullName>
        <description>Case: ANZ Carelink Escalation Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Sales_Rep_Lookup__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ANZ_Email_Template/ANZ_Carelink_Escalation</template>
    </alerts>
    <alerts>
        <fullName>Case_Acknowledged_Notification_on_status_change_OMA_Spine</fullName>
        <ccEmails>rs.msdoma@medtronic.com</ccEmails>
        <description>Case: Acknowledged Notification on status change - OMA Spine</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>US_OMA_Automated_Templates/OMA_Spine_Acknowledged_Notification</template>
    </alerts>
    <alerts>
        <fullName>Case_Written_response_request_notification_OMA_Spine</fullName>
        <ccEmails>rs.msdoma@medtronic.com</ccEmails>
        <description>Case: Written response request notification - OMA Spine</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>US_OMA_Automated_Templates/OMA_Spine_Written_Response_Notification</template>
    </alerts>
    <alerts>
        <fullName>Case_assignment_notification_OMA_Neuro</fullName>
        <description>Case assignment notification - OMA Neuro</description>
        <protected>false</protected>
        <recipients>
            <field>OMA_Case_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>US_OMA_Automated_Templates/OMA_Case_Assignment_Owner_Change_Notification_OMA_Neuro</template>
    </alerts>
    <alerts>
        <fullName>Case_assignment_notification_OMA_Spine</fullName>
        <description>Case assignment notification - OMA Spine</description>
        <protected>false</protected>
        <recipients>
            <field>OMA_Case_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>US_OMA_Automated_Templates/OMA_Case_Assignment_Owner_Change_Notification_OMA_Spine</template>
    </alerts>
    <alerts>
        <fullName>EMEA_IHS_Assigned_to_Cathlab_Manager</fullName>
        <description>EMEA IHS Assigned to Cathlab Manager</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>mes.accenture@ngcmedical.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>EMEA_IHS_Equipment_Management_Solution/EMEA_IHS_Case_assigned_to_Cathlab_Manager</template>
    </alerts>
    <alerts>
        <fullName>GCH_Number_Request</fullName>
        <ccEmails>rs.sdmdamagedrarequests@medtronic.com,</ccEmails>
        <ccEmails>john.m.kamer@medtronic.com,</ccEmails>
        <ccEmails>sandra.stokes@medtronic.com,</ccEmails>
        <ccEmails>scott.drapeau@medtronic.com</ccEmails>
        <description>GCH Number Request</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>US_OMA_Automated_Templates/OMA_GCH_Email_Notififcation</template>
    </alerts>
    <alerts>
        <fullName>GCH_Number_Request_Neuro</fullName>
        <ccEmails>rs.neuadverseeventreporting@medtronic.com</ccEmails>
        <description>GCH Number Request(Neuro)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>US_OMA_Automated_Templates/OMA_GCH_Email_Notififcation_Neuro</template>
    </alerts>
    <alerts>
        <fullName>NPS_Followup_Case_assignment</fullName>
        <description>NPS Followup Case assignment</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/NPS_Followup_Case_assignment</template>
    </alerts>
    <alerts>
        <fullName>Send_Escation_Email</fullName>
        <ccEmails>soorya.sharma@hotmail.com</ccEmails>
        <description>Send Escation Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>system@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>US_OMA_Automated_Templates/OMA_Escalation_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>CASE_System_Down_Start</fullName>
        <description>Update the System down start time if it isn&apos;t populated.</description>
        <field>System_Down_Start__c</field>
        <formula>IF(ISPICKVAL(Systems_Down__c,&quot;Yes&quot;),
IF(
ISNULL(System_Down_Start__c), 
NOW(), 
System_Down_Start__c 
),
null /*if system is not down put null value*/
)</formula>
        <name>CASE: System Down Start</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CASE_Update_Complaint_To_Yes</fullName>
        <field>Complaint_Case__c</field>
        <literalValue>Yes</literalValue>
        <name>CASE: Update Complaint To Yes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CASE_Update_Date_Complaint_Became_Known</fullName>
        <field>Date_Complaint_Became_Known__c</field>
        <formula>TODAY()</formula>
        <name>CASE: Update Date Complaint Became Known</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CASE_Update_to_Closed_Record_Type</fullName>
        <description>On Case close change record type to lock case.</description>
        <field>RecordTypeId</field>
        <lookupValue>Closed_Case</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CASE: Update to Closed Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Account_Country</fullName>
        <description>Update Account Country</description>
        <field>Account_Country__c</field>
        <formula>TEXT(Account.Account_Country_vs__c)</formula>
        <name>Case:Update Account Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OMA_Case_Owner_email_update</fullName>
        <description>Copy the email address from the OMA Case Owner (lookup to user) to the OMA Case Owner Email field so it can be used in email alerts.</description>
        <field>OMA_Case_Owner_Email__c</field>
        <formula>OMA_Case_Owner__r.Email</formula>
        <name>OMA Case Owner email update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OMA_Change_Case_Owner_on_case_closure</fullName>
        <field>OwnerId</field>
        <lookupValue>OMA_Closed_Cases</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>OMA_Change Case Owner on case closure</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Date_Closed</fullName>
        <field>Date_Closed_Date__c</field>
        <formula>Today ()</formula>
        <name>Update Date Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Date_Received</fullName>
        <field>Date_Received_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Date Received</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>CASE%3A Close Closed</fullName>
        <actions>
            <name>CASE_Update_to_Closed_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>RTG - Closed Case,RTG - New Case</value>
        </criteriaItems>
        <description>ST Case: Update to Closed Record Type when Status = Closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CASE%3A Update Complaint to Yes</fullName>
        <actions>
            <name>CASE_System_Down_Start</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CASE_Update_Complaint_To_Yes</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CASE_Update_Date_Complaint_Became_Known</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Systems_Down__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>RTG - Closed Case,RTG - New Case</value>
        </criteriaItems>
        <description>ST CASE: If a system is down then mark the case record as complaint.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A ANZ Carelink Escalation</fullName>
        <actions>
            <name>Case_ANZ_Carelink_Escalation_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Escalated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ANZ Carelink</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sales_Rep_Lookup__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Send email notification to Case Team Members when the status is changed to Escalate. Apply for Record Type: ANZ Carelink only.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Acknowledged Notification on status change - OMA Spine</fullName>
        <actions>
            <name>Case_Acknowledged_Notification_on_status_change_OMA_Spine</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Acknowledged</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>OMA Spine - Clinical Support/Case Observation,OMA Spine - Scientific Exchange/Product</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Email notification on ownership change</fullName>
        <actions>
            <name>OMA_Case_Owner_email_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a case is assigned to a new owner, send email notification to the owner to act on the case.</description>
        <formula>OR($RecordType.Name = &apos;OMA Spine - Scientific Exchange/Product&apos;, $RecordType.Name = &apos;OMA Spine - Clinical Support/Case Observation&apos;, $RecordType.Name = &apos;OMA Neuro - MedInfo&apos;, $RecordType.Name = &apos;OMA Neuro - Physician-Scientist Exchange&apos;, $RecordType.Name = &apos;OMA Neuro - Internal Support Request&apos;) &amp;&amp; IsChanged (OwnerId)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A GCH Number Request</fullName>
        <actions>
            <name>GCH_Number_Request</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>GCH_Notification_Sent</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Send an email to the GCH Mailbox when the Adverse Event field is checked. Also create a reminder task for the owner.</description>
        <formula>( ISNEW() || ISCHANGED( Product_Complaint__c ))  &amp;&amp;  (ISPICKVAL(Product_Complaint__c, &quot;Adverse Event&quot;) || ISPICKVAL(Product_Complaint__c, &quot;Product Complaint&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A GCH Number Request%28Neuro%29</fullName>
        <actions>
            <name>GCH_Number_Request_Neuro</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>GCH_Notification_Sent</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>Send an email to the GCH Mailbox when the Adverse Event field is selected. Also create task indicating that the email has been sent.</description>
        <formula>( ISNEW() || ISCHANGED( Product_Complaint__c ))  &amp;&amp;  (ISPICKVAL(Product_Complaint__c, &quot;Yes&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A OMA Case Owner change notification - OMA Neuro</fullName>
        <actions>
            <name>Case_assignment_notification_OMA_Neuro</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>OMA_Case_Owner_email_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Email notify assigned OMA Neuro case owner when a new OMA Neuro case owner is assigned. This will result in an update of the Case Owner and OMA_Case_Owner_Email__c field and this workflow is activated on the change of Case Owner field.</description>
        <formula>OR($RecordType.Name = &apos;OMA Neuro - MedInfo&apos;, $RecordType.Name = &apos;OMA Neuro - Physician-Scientist Exchange&apos;, $RecordType.Name = &apos;OMA Neuro - Internal Support Request&apos;) &amp;&amp; ISCHANGED(OwnerId)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A OMA Case Owner change notification - OMA Spine</fullName>
        <actions>
            <name>Case_assignment_notification_OMA_Spine</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>OMA_Case_Owner_email_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Email notify assigned OMA Spine case owner when a new OMA Spine case owner is assigned. This will result in an update of the Case Owner and OMA_Case_Owner_Email__c field and this workflow is activated on the change of Case Owner field.</description>
        <formula>OR($RecordType.Name = &apos;OMA Spine - Scientific Exchange/Product&apos;, $RecordType.Name = &apos;OMA Spine - Clinical Support/Case Observation&apos;) &amp;&amp; ISCHANGED(OwnerId)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A OMA Set Date Closed</fullName>
        <actions>
            <name>Update_Date_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>OMA Neuro - Internal Support Request,OMA Neuro - MedInfo,OMA Neuro - Physician-Scientist Exchange</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Date_Closed_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A OMA Set Date Received</fullName>
        <actions>
            <name>Update_Date_Received</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>OMA Neuro - Internal Support Request,OMA Neuro - MedInfo,OMA Neuro - Physician-Scientist Exchange</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Date_Received_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Owner change notification during case creation - OMA Neuro</fullName>
        <actions>
            <name>OMA_Case_Owner_email_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Email Notify assigned user of case assignment upon creation (if owner is not the user creating the case)</description>
        <formula>OR($RecordType.Name = &apos;OMA Neuro - MedInfo&apos;, $RecordType.Name = &apos;OMA Neuro - Physician-Scientist Exchange&apos;, $RecordType.Name = &apos;OMA Neuro - Internal Support Request&apos;) &amp;&amp;  CreatedBy.Id  &lt;&gt;  OMA_Case_Owner__r.Id</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Owner change notification during case creation - OMA Spine</fullName>
        <actions>
            <name>OMA_Case_Owner_email_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Email Notify assigned user of case assignment upon creation (if owner is not the user creating the case)</description>
        <formula>OR($RecordType.Name = &apos;OMA Spine - Scientific Exchange/Product&apos;, $RecordType.Name = &apos;OMA Spine - Clinical Support/Case Observation&apos;) &amp;&amp;  CreatedBy.Id  &lt;&gt;  OMA_Case_Owner__r.Id</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Task creation on ownership change</fullName>
        <actions>
            <name>New_Case_Assigned</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>When a case is assigned to a new owner, create a task for the owner to act on the case.</description>
        <formula>OR($RecordType.Name = &apos;OMA Spine - Scientific Exchange/Product&apos;, $RecordType.Name = &apos;OMA Spine - Clinical Support/Case Observation&apos;, $RecordType.Name = &apos;OMA Neuro - MedInfo&apos;, $RecordType.Name = &apos;OMA Neuro - Physician-Scientist Exchange&apos;, $RecordType.Name = &apos;OMA Neuro - Internal Support Request&apos;) &amp;&amp; IsChanged (OwnerId)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Written Response Request Notification on status change - OMA Spine</fullName>
        <actions>
            <name>Case_Written_response_request_notification_OMA_Spine</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Written Response Requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>OMA Spine - Clinical Support/Case Observation,OMA Spine - Scientific Exchange/Product</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3AOwnership Change on Case Closure</fullName>
        <actions>
            <name>OMA_Change_Case_Owner_on_case_closure</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>OMA Spine - Clinical Support/Case Observation,OMA Spine - Scientific Exchange/Product</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>When a OMA Spine case is closed, change the case ownership to &apos;OMA Closed Cases&apos; queue.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3AUpdate Account Country</fullName>
        <actions>
            <name>Case_Update_Account_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Update Account Country with Country from Account</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMEA IHS Assigned to Cathlab Manager</fullName>
        <actions>
            <name>EMEA_IHS_Assigned_to_Cathlab_Manager</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Workflow with Email Alert. When a new IHS Case is assigned to the Cathlab Manager (owner changes), then an email notification shall be sent to the Cathlab Manager and to the Contact in the hospital that is on the case.</description>
        <formula>(ISCHANGED( OwnerId ) ||  (ISNEW()  &amp;&amp;  !ISBLANK(OwnerId )))  &amp;&amp;  RecordType.DeveloperName = &apos;IHS_Temporary_Solution&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>GCH_Notification_Sent</fullName>
        <assignedToType>owner</assignedToType>
        <description>GCH Notification has been sent by the system.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>GCH Notification Sent</subject>
    </tasks>
    <tasks>
        <fullName>New_Case_Assigned</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>New Case Assigned</subject>
    </tasks>
</Workflow>
