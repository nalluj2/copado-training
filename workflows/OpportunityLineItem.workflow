<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Copying_Quantity_to_Demand_Quantity</fullName>
        <description>Field Update (Opportunity Product: Demand QTY)</description>
        <field>Demand__c</field>
        <formula>Quantity</formula>
        <name>Copying Quantity to Demand Quantity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Product_Type_Disposable</fullName>
        <field>Product_Type__c</field>
        <literalValue>Disposable</literalValue>
        <name>Opportunity Product - Type Disposable</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Product_Update_Type_Capital</fullName>
        <field>Product_Type__c</field>
        <literalValue>Capital</literalValue>
        <name>Opportunity Product Update Type–Capital</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_All_Lost_check_box_EMEA_EM</fullName>
        <description>If Non-Compliant check box is checked All Lost check box should be checked as well</description>
        <field>All_Lost__c</field>
        <literalValue>1</literalValue>
        <name>Update All Lost check box EMEA EM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Oppty Prod%3A Copying Quantity to Demand Quantity for EMEA EM</fullName>
        <actions>
            <name>Copying_Quantity_to_Demand_Quantity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow rule (review entry rules)</description>
        <formula>AND(OR ($Profile.Name =&apos;MEA Field Force MITG&apos; , $Profile.Name =&apos;EMEA Field Force MITG&apos;),ISNULL( Demand__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Oppty Prod%3A Update All Lost check box EMEA EM</fullName>
        <actions>
            <name>Update_All_Lost_check_box_EMEA_EM</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>OpportunityLineItem.Non_Compliant__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>MEA Field Force MITG</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>EMEA Field Force MITG</value>
        </criteriaItems>
        <description>If Non-Compliant check box is checked All Lost check box should be checked as well</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Oppty Prod%3A Update Product Types</fullName>
        <actions>
            <name>Opportunity_Product_Update_Type_Capital</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow rule (review entry rules)</description>
        <formula>AND((ISPICKVAL( Product2.Product_Type__c, &quot;Capital&quot;)), (ISPICKVAL( Product_Type__c, &quot;None&quot;)), ( Opportunity.RecordType.DeveloperName = &apos;EUR_MITG_Standard_Opportunity&apos; || Opportunity.RecordType.DeveloperName = &apos;MEA_MITG_Standard_Opportunity&apos;  || Opportunity.RecordType.DeveloperName = &apos;EMEA_MITG_Standard_Opportunity&apos; ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Oppty Prod%3A Update Product Types - Disp</fullName>
        <actions>
            <name>Opportunity_Product_Type_Disposable</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND((ISPICKVAL( Product2.Product_Type__c, &quot;Disposable&quot;)), (ISPICKVAL( Product_Type__c, &quot;None&quot;)), ( Opportunity.RecordType.DeveloperName = &apos;EUR_MITG_Standard_Opportunity&apos; || Opportunity.RecordType.DeveloperName = &apos;MEA_MITG_Standard_Opportunity&apos;  ||Opportunity.RecordType.DeveloperName = &apos;EMEA_MITG_Standard_Opportunity&apos; ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
