<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Copy_Job_Title_to_Role</fullName>
        <description>Copy Job Title to Role when account team member is primary (created by batch apex)</description>
        <field>Role__c</field>
        <formula>TEXT(User__r.Job_Title_vs__c)</formula>
        <name>Copy Job Title to Role</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Primary%3A Copy Job Title to Role</fullName>
        <actions>
            <name>Copy_Job_Title_to_Role</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Team_Member__c.Primary__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
