<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>FU_OpportunityCompetitorUnique</fullName>
        <field>Unique_Opportunity_Competitor__c</field>
        <formula>Opportunity__c + Competitor__c +  Product_MITG__c</formula>
        <name>FU_OpportunityCompetitorUnique</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>WR_Opportunity Competitor Unique</fullName>
        <actions>
            <name>FU_OpportunityCompetitorUnique</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity_Competitor__c.LastModifiedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
