<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ImplProduct_No_Price_For_IND</fullName>
        <ccEmails>ganga.s@medtronic.com.test</ccEmails>
        <description>ImplProduct: No Price For IND</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/ImplProduct_No_Price</template>
    </alerts>
    <rules>
        <fullName>ImplProduct%3A Asset%3A No Price For IND</fullName>
        <actions>
            <name>ImplProduct_No_Price_For_IND</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(  OR(  TEXT(Price__c) = &quot;0&quot;,  TEXT(Price__c) = NULL  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
