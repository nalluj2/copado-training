<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Admin_Request_Status_Changed</fullName>
        <description>Admin Request Status Changed</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>Admin_Request_Approvers</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <field>User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Application_Management/Admin_Request_Status_Changed</template>
    </alerts>
    <alerts>
        <fullName>New_Admin_Request</fullName>
        <description>New Admin Request</description>
        <protected>false</protected>
        <recipients>
            <recipient>Admin_Request_Approvers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Application_Management/New_Admin_Request</template>
    </alerts>
    <rules>
        <fullName>Admin Request Changed</fullName>
        <actions>
            <name>Admin_Request_Status_Changed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Admin_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>Pending,Rejected,Approved</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New Admin Request</fullName>
        <actions>
            <name>New_Admin_Request</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Admin_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
