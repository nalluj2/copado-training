<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>WO_Sparepart_ST_Investigation_Complete_Notification</fullName>
        <description>WO Sparepart: ST: Investigation Complete Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>lorenzo.zanirato@medtronic.com.emeac</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ST_Service_Email_Templates/WE_Case_Part_Investigation_Complete_Notification</template>
    </alerts>
    <alerts>
        <fullName>WO_Sparepart_ST_Investigation_Complete_Notification_BCEGI</fullName>
        <description>WO Sparepart: ST: Investigation Complete Notification - BCEGI</description>
        <protected>false</protected>
        <recipients>
            <recipient>marc.lauvenberg@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ST_Service_Email_Templates/WE_Case_Part_Investigation_Complete_Notification</template>
    </alerts>
    <alerts>
        <fullName>WO_Sparepart_ST_Investigation_Complete_Notification_France</fullName>
        <description>WO Sparepart: ST: Investigation Complete Notification - France</description>
        <protected>false</protected>
        <recipients>
            <recipient>sylvain.villard@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ST_Service_Email_Templates/WE_Case_Part_Investigation_Complete_Notification</template>
    </alerts>
    <alerts>
        <fullName>WO_Sparepart_ST_Investigation_Complete_Notification_Germany</fullName>
        <description>WO Sparepart: ST: Investigation Complete Notification - Germany</description>
        <protected>false</protected>
        <recipients>
            <recipient>christian.schmitt@medtronic.com2</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ST_Service_Email_Templates/WE_Case_Part_Investigation_Complete_Notification</template>
    </alerts>
    <alerts>
        <fullName>WO_Sparepart_ST_Investigation_Complete_Notification_Iberia</fullName>
        <description>WO Sparepart: ST: Investigation Complete Notification - Iberia</description>
        <protected>false</protected>
        <recipients>
            <recipient>carlos.garcia.asensio@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ST_Service_Email_Templates/WE_Case_Part_Investigation_Complete_Notification</template>
    </alerts>
    <alerts>
        <fullName>WO_Sparepart_ST_Investigation_Complete_Notification_Italy</fullName>
        <description>WO Sparepart: ST: Investigation Complete Notification - Italy</description>
        <protected>false</protected>
        <recipients>
            <recipient>daniele.marinucci@medtronic.com2</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ST_Service_Email_Templates/WE_Case_Part_Investigation_Complete_Notification</template>
    </alerts>
    <alerts>
        <fullName>WO_Sparepart_ST_Investigation_Complete_Notification_NBP</fullName>
        <description>WO Sparepart: ST: Investigation Complete Notification - NBP</description>
        <protected>false</protected>
        <recipients>
            <recipient>gerd.balette@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ST_Service_Email_Templates/WE_Case_Part_Investigation_Complete_Notification</template>
    </alerts>
    <alerts>
        <fullName>WO_Sparepart_ST_Investigation_Complete_Notification_O_Arm_Polestar</fullName>
        <description>WO Sparepart ST: Investigation Complete Notification O-Arm Polestar</description>
        <protected>false</protected>
        <recipients>
            <recipient>joao.rodrigues@medtronic.com.emeac</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ST_Service_Email_Templates/WE_Case_Part_Investigation_Complete_Notification</template>
    </alerts>
    <alerts>
        <fullName>WO_Sparepart_ST_Investigation_Complete_Notification_UKIreland</fullName>
        <description>WO Sparepart: ST: Investigation Complete Notification - UK/Ireland</description>
        <protected>false</protected>
        <recipients>
            <recipient>austin.leavy@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ST_Service_Email_Templates/WE_Case_Part_Investigation_Complete_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Get_CFN_Installed</fullName>
        <description>Gets the CFN of the Installed sparepart</description>
        <field>CFN_Installed__c</field>
        <formula>Sparepart_Installed__r.CFN_Code_Text__c</formula>
        <name>Get CFN Installed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Get_CFN_Removed</fullName>
        <description>See the Get CFN Installed Field Update</description>
        <field>CFN_Removed__c</field>
        <formula>Sparepart_Removed__r.CFN_Code_Text__c</formula>
        <name>Get CFN Removed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_Sparepart_Get_Country_General</fullName>
        <description>Account Country is filled with the country from the related account</description>
        <field>Account_Country__c</field>
        <formula>Text(Workorder__r.Account__r.Account_Country_vs__c)</formula>
        <name>WO Sparepart Get Country - General</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WO_Sparepart_Get_Country_ST</fullName>
        <description>Account Country is filled with the country from the related account (stored on WO Sparepart level)</description>
        <field>Account_Country__c</field>
        <formula>TEXT(Account__r.Account_Country_vs__c)</formula>
        <name>WO Sparepart Get Country - ST</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Technical Service Get CFN</fullName>
        <actions>
            <name>Get_CFN_Installed</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Get_CFN_Removed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Workorder_Sparepart__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>EUR Technical Service</value>
        </criteriaItems>
        <description>This workflowrule gets the CFN of the installed and removed sparepart in order to correctly show them on the CFD (Drawloop does not get the right CFNs in each row in a table if retrieved from the Product object)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WO Sparepart Get Country - General</fullName>
        <actions>
            <name>WO_Sparepart_Get_Country_General</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Workorder_Sparepart__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Hardware,Inaccuracy,Service,Software</value>
        </criteriaItems>
        <description>This workflow fills the Account Country on the Workorder Sparepart record with the Account Country</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WO Sparepart Get Country - ST</fullName>
        <actions>
            <name>WO_Sparepart_Get_Country_ST</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Workorder_Sparepart__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Hardware,Inaccuracy,Service,Software</value>
        </criteriaItems>
        <description>This workflow fills the Account Country on the Workorder Sparepart record with the Account Country</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WO Sparepart%3A ST%3A Investigation Complete Notification</fullName>
        <actions>
            <name>WO_Sparepart_ST_Investigation_Complete_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>WO Sparepart: ST: Investigation Complete Notification</description>
        <formula>AND(Asset__r.RecordType.Name = &quot;Navigation&quot;,  OR(  $RecordType.DeveloperName = &quot;Hardware&quot;,  $RecordType.DeveloperName = &quot;Inaccuracy&quot;,  $RecordType.DeveloperName = &quot;Service&quot;,  $RecordType.DeveloperName = &quot;Software&quot;  ),  ISPICKVAL(Return_Item_Status__c , &quot;Analysis Complete&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO Sparepart%3A ST%3A Investigation Complete Notification - BCEGI</fullName>
        <actions>
            <name>WO_Sparepart_ST_Investigation_Complete_Notification_BCEGI</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>WO Sparepart: ST: Investigation Complete Notification</description>
        <formula>AND(OR(Asset__r.RecordType.Name = &quot;Navigation&quot;, Asset__r.RecordType.Name = &quot;Accessories&quot;), OR( $RecordType.DeveloperName = &quot;Hardware&quot;, $RecordType.DeveloperName = &quot;Inaccuracy&quot;, $RecordType.DeveloperName = &quot;Service&quot;, $RecordType.DeveloperName = &quot;Software&quot; ), ISPICKVAL(Return_Item_Status__c , &quot;Analysis Complete&quot;),  ISPICKVAL(Account__r.Account_Country_vs__c, &quot;BELGIUM,LUXEMBOURG,NETHERLANDS,SWITZERLAND,AUSTRIA,GREECE,ISRAEL,HUNGARY,ROMANIA,MOLDAVIA,SLOVAKIA,CZECH REPUBLIC,ALBANIA,BOSNIA-HERZ.,CROATIA,SLOVENIA,KOSOVO,SERBIA,BULGARIA,NORTH MACEDONIA,MONTENEGRO,ARMANIA,BELARUS,GEORGIA,UKRAINE​&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO Sparepart%3A ST%3A Investigation Complete Notification - France</fullName>
        <actions>
            <name>WO_Sparepart_ST_Investigation_Complete_Notification_France</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>WO Sparepart: ST: Investigation Complete Notification</description>
        <formula>AND(OR(Asset__r.RecordType.Name = &quot;Navigation&quot;, Asset__r.RecordType.Name = &quot;Accessories&quot;), OR( $RecordType.DeveloperName = &quot;Hardware&quot;, $RecordType.DeveloperName = &quot;Inaccuracy&quot;, $RecordType.DeveloperName = &quot;Service&quot;, $RecordType.DeveloperName = &quot;Software&quot; ), ISPICKVAL(Return_Item_Status__c , &quot;Analysis Complete&quot;),  ISPICKVAL(Account__r.Account_Country_vs__c, &quot;France​&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO Sparepart%3A ST%3A Investigation Complete Notification - Germany</fullName>
        <actions>
            <name>WO_Sparepart_ST_Investigation_Complete_Notification_Germany</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>WO Sparepart: ST: Investigation Complete Notification</description>
        <formula>AND(OR(Asset__r.RecordType.Name = &quot;Navigation&quot;, Asset__r.RecordType.Name = &quot;Accessories&quot;), OR( $RecordType.DeveloperName = &quot;Hardware&quot;, $RecordType.DeveloperName = &quot;Inaccuracy&quot;, $RecordType.DeveloperName = &quot;Service&quot;, $RecordType.DeveloperName = &quot;Software&quot; ), ISPICKVAL(Return_Item_Status__c , &quot;Analysis Complete&quot;),  ISPICKVAL(Account__r.Account_Country_vs__c, &quot;Germany&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO Sparepart%3A ST%3A Investigation Complete Notification - Iberia</fullName>
        <actions>
            <name>WO_Sparepart_ST_Investigation_Complete_Notification_Iberia</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>WO Sparepart: ST: Investigation Complete Notification</description>
        <formula>AND(OR(Asset__r.RecordType.Name = &quot;Navigation&quot;, Asset__r.RecordType.Name = &quot;Accessories&quot;), OR( $RecordType.DeveloperName = &quot;Hardware&quot;, $RecordType.DeveloperName = &quot;Inaccuracy&quot;, $RecordType.DeveloperName = &quot;Service&quot;, $RecordType.DeveloperName = &quot;Software&quot; ), ISPICKVAL(Return_Item_Status__c , &quot;Analysis Complete&quot;),  ISPICKVAL(Account__r.Account_Country_vs__c, &quot;Spain, Portugal&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO Sparepart%3A ST%3A Investigation Complete Notification - Italy</fullName>
        <actions>
            <name>WO_Sparepart_ST_Investigation_Complete_Notification_Italy</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>WO Sparepart: ST: Investigation Complete Notification</description>
        <formula>AND(OR(Asset__r.RecordType.Name = &quot;Navigation&quot;, Asset__r.RecordType.Name = &quot;Accessories&quot;), OR( $RecordType.DeveloperName = &quot;Hardware&quot;, $RecordType.DeveloperName = &quot;Inaccuracy&quot;, $RecordType.DeveloperName = &quot;Service&quot;, $RecordType.DeveloperName = &quot;Software&quot; ), ISPICKVAL(Return_Item_Status__c , &quot;Analysis Complete&quot;),  ISPICKVAL(Account__r.Account_Country_vs__c, &quot;Italy&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO Sparepart%3A ST%3A Investigation Complete Notification - NBP</fullName>
        <actions>
            <name>WO_Sparepart_ST_Investigation_Complete_Notification_NBP</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>WO Sparepart: ST: Investigation Complete Notification</description>
        <formula>AND(OR(Asset__r.RecordType.Name = &quot;Navigation&quot;, Asset__r.RecordType.Name = &quot;Accessories&quot;), OR( $RecordType.DeveloperName = &quot;Hardware&quot;, $RecordType.DeveloperName = &quot;Inaccuracy&quot;, $RecordType.DeveloperName = &quot;Service&quot;, $RecordType.DeveloperName = &quot;Software&quot; ), ISPICKVAL(Return_Item_Status__c , &quot;Analysis Complete&quot;),  ISPICKVAL(Account__r.Account_Country_vs__c, &quot;POLAND,DENMARK,FINLAND,NORWAY,SWEDEN,ESTONIA,LATVIA,LITHUANIA&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO Sparepart%3A ST%3A Investigation Complete Notification - UK%2FIreland</fullName>
        <actions>
            <name>WO_Sparepart_ST_Investigation_Complete_Notification_UKIreland</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>WO Sparepart: ST: Investigation Complete Notification</description>
        <formula>AND(OR(Asset__r.RecordType.Name = &quot;Navigation&quot;, Asset__r.RecordType.Name = &quot;Accessories&quot;), OR( $RecordType.DeveloperName = &quot;Hardware&quot;, $RecordType.DeveloperName = &quot;Inaccuracy&quot;, $RecordType.DeveloperName = &quot;Service&quot;, $RecordType.DeveloperName = &quot;Software&quot; ), ISPICKVAL(Return_Item_Status__c , &quot;Analysis Complete&quot;),  ISPICKVAL(Account__r.Account_Country_vs__c, &quot;Ireland, United Kingdom&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WO Sparepart%3A ST%3A Investigation Complete Notification O-Arm Polestar</fullName>
        <actions>
            <name>WO_Sparepart_ST_Investigation_Complete_Notification_O_Arm_Polestar</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>WO Sparepart: ST: Investigation Complete Notification O-Arm Polestar</description>
        <formula>AND( OR (Asset__r.RecordType.Name = &quot;O-Arm&quot;, Asset__r.RecordType.Name = &quot;Polestar&quot; ),  OR(  $RecordType.DeveloperName = &quot;Hardware&quot;,  $RecordType.DeveloperName = &quot;Inaccuracy&quot;,  $RecordType.DeveloperName = &quot;Service&quot;,  $RecordType.DeveloperName = &quot;Software&quot;  ),  ISPICKVAL(Return_Item_Status__c , &quot;Analysis Complete&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
