<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Acc_Plan_PESTEL_Last_Modified_Date</fullName>
        <field>PESTEL_Last_Modified_Date__c</field>
        <formula>NOW()</formula>
        <name>Acc Plan: PESTEL Last Modified Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Acc_Plan_SWOT_Last_Modified_Date</fullName>
        <field>SWOT_Last_Modified_Date__c</field>
        <formula>NOW()</formula>
        <name>Acc Plan: SWOT Last Modified Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Plan_Country_update</fullName>
        <description>Update of &quot;Country&quot; field based on value coming from &quot;Country form&quot; formula field. The &quot;Country&quot; field will be used in sharing rules for Account Plans.</description>
        <field>Country__c</field>
        <formula>Country_form__c</formula>
        <name>Account Plan Country update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_SetAccountPlanAccountCountry</fullName>
        <field>Country__c</field>
        <formula>Country_form__c</formula>
        <name>FU_SetAccountPlanAccountCountry</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>iPlan2_Copy_Business_Unit</fullName>
        <description>Copy business unit to a text field so it can be used in Sharing rules</description>
        <field>Business_Unit_Text__c</field>
        <formula>Business_Unit__r.Name</formula>
        <name>iPlan2: Copy Business Unit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Acc Plan%3A PESTEL Last Modified Date</fullName>
        <actions>
            <name>Acc_Plan_PESTEL_Last_Modified_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISCHANGED( Political__c )  ||  ISCHANGED( Economic__c ) ||  ISCHANGED( Legal__c )  ||  ISCHANGED( Environmental__c ) ||   ISCHANGED( Technological__c )   || ISCHANGED( Social__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Acc Plan%3A SWOT Last Modified Date</fullName>
        <actions>
            <name>Acc_Plan_SWOT_Last_Modified_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISCHANGED(Other_Strengths__c)  ||  ISCHANGED(Other_Opportunities__c) ||  ISCHANGED(Other_Weaknesses__c)  ||  ISCHANGED(Other_Threats__c) ||   ISCHANGED(Strengths__c)   || ISCHANGED(Opportunities__c)  || ISCHANGED(Weaknesses__c) || ISCHANGED(Threats__c)|| ISCHANGED(Needs__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account Plan Country update</fullName>
        <actions>
            <name>Account_Plan_Country_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account_Plan_2__c.Country_form__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Update of &quot;Country&quot; field based on value coming from &quot;Country form&quot; formula field. The &quot;Country&quot; field will be used in sharing rules for account plans.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WF_SetAccountCountry</fullName>
        <actions>
            <name>FU_SetAccountPlanAccountCountry</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account_Plan_2__c.Country__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>iPlan2%3A Copy Business Unit</fullName>
        <actions>
            <name>iPlan2_Copy_Business_Unit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account_Plan_2__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>iPlan2: Copy Business Unit
20160428 - CSa - Logic copied to trigger tr_AccountPlan2_BeforeInsertUpdate</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
