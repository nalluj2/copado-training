<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ANZ_DIB_MicroSiteAutoResponseEmail</fullName>
        <description>ANZ DIB Micro-Site Auto-Response Email</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>australia.diabetes@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>ANZ_Email_Template/ANZ_DIB_MicroSiteAutoResponse</template>
    </alerts>
    <alerts>
        <fullName>ANZ_DIB_Web_Lead_Confirmation_Email</fullName>
        <description>ANZ DIB Web Lead Confirmation Email</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ANZ_Email_Template/ANZ_DIB_Lead_Confirmation</template>
    </alerts>
    <alerts>
        <fullName>CAN_DIB_Requested_Phone_No_and_Phone_Opt_In_By_Email_D1_EN</fullName>
        <description>CAN DIB Requested Phone No. and Phone Opt-In By Email D1 (EN)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_D1_EN</template>
    </alerts>
    <alerts>
        <fullName>CAN_DIB_Requested_Phone_No_and_Phone_Opt_In_By_Email_D1_EN_ACTIVITY_HISTORY</fullName>
        <ccEmails>emailtosalesforce@20rrtqrv50l8afdcsqawcqachzy0df86p849c6zrnz6qojb02x.2-iypieag.wl.le.salesforce.com</ccEmails>
        <description>CAN DIB Requested Phone No. and Phone Opt-In By Email D1 (EN) FOR ACTIVITY HISTORY</description>
        <protected>false</protected>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_D1_EN</template>
    </alerts>
    <alerts>
        <fullName>CAN_DIB_Requested_Phone_No_and_Phone_Opt_In_By_Email_D1_FR</fullName>
        <description>CAN DIB Requested Phone No. and Phone Opt-In By Email D1 (FR)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_D1_FR</template>
    </alerts>
    <alerts>
        <fullName>CAN_DIB_Requested_Phone_No_and_Phone_Opt_In_By_Email_D1_FR_ACTIVITY_HISTORY</fullName>
        <ccEmails>emailtosalesforce@20rrtqrv50l8afdcsqawcqachzy0df86p849c6zrnz6qojb02x.2-iypieag.wl.le.salesforce.com</ccEmails>
        <description>CAN DIB Requested Phone No. and Phone Opt-In By Email D1 (FR) FOR ACTIVITY HISTORY</description>
        <protected>false</protected>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_D1_FR</template>
    </alerts>
    <alerts>
        <fullName>CAN_DIB_Requested_Phone_No_and_Phone_Opt_In_By_Email_EN</fullName>
        <description>CAN DIB Requested Phone No. and Phone Opt-In By Email (EN)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_D_EN</template>
    </alerts>
    <alerts>
        <fullName>CAN_DIB_Requested_Phone_No_and_Phone_Opt_In_By_Email_EN_ACTIVITY_HISTORY</fullName>
        <ccEmails>emailtosalesforce@20rrtqrv50l8afdcsqawcqachzy0df86p849c6zrnz6qojb02x.2-iypieag.wl.le.salesforce.com</ccEmails>
        <description>CAN DIB Requested Phone No. and Phone Opt-In By Email (EN) FOR ACTIVITY HISTORY</description>
        <protected>false</protected>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_D_EN</template>
    </alerts>
    <alerts>
        <fullName>CAN_DIB_Requested_Phone_No_and_Phone_Opt_In_By_Email_FR</fullName>
        <description>CAN DIB Requested Phone No. and Phone Opt-In By Email (FR)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_D_FR</template>
    </alerts>
    <alerts>
        <fullName>CAN_DIB_Requested_Phone_No_and_Phone_Opt_In_By_Email_FR_ACTIVITY_HISTORY</fullName>
        <ccEmails>emailtosalesforce@20rrtqrv50l8afdcsqawcqachzy0df86p849c6zrnz6qojb02x.2-iypieag.wl.le.salesforce.com</ccEmails>
        <description>CAN DIB Requested Phone No. and Phone Opt-In By Email (FR) FOR ACTIVITY HISTORY</description>
        <protected>false</protected>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_D_FR</template>
    </alerts>
    <alerts>
        <fullName>CAN_DIB_Update_Insurance_Info_EN</fullName>
        <description>CAN DIB Update Insurance Info (EN)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_B_EN</template>
    </alerts>
    <alerts>
        <fullName>CAN_DIB_Update_Insurance_Info_EN_FOR_ACTIVITY_HISTORY</fullName>
        <ccEmails>emailtosalesforce@20rrtqrv50l8afdcsqawcqachzy0df86p849c6zrnz6qojb02x.2-iypieag.wl.le.salesforce.com</ccEmails>
        <description>CAN DIB Update Insurance Info (EN) FOR ACTIVITY HISTORY</description>
        <protected>false</protected>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_B_EN</template>
    </alerts>
    <alerts>
        <fullName>CAN_DIB_Update_Insurance_Info_FR</fullName>
        <description>CAN DIB Update Insurance Info (FR)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_B_FR</template>
    </alerts>
    <alerts>
        <fullName>CAN_DIB_Update_Insurance_Info_FR_FOR_ACTIVITY_HISTORY</fullName>
        <ccEmails>emailtosalesforce@20rrtqrv50l8afdcsqawcqachzy0df86p849c6zrnz6qojb02x.2-iypieag.wl.le.salesforce.com</ccEmails>
        <description>CAN DIB Update Insurance Info (FR) FOR ACTIVITY HISTORY</description>
        <protected>false</protected>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_B_FR</template>
    </alerts>
    <alerts>
        <fullName>Event_Confirmation_Email_EN</fullName>
        <description>Event - Confirmation Email (EN)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Event_Confirmation_Email_ENGLISH</template>
    </alerts>
    <alerts>
        <fullName>Event_Confirmation_Email_EN_for_activity_history</fullName>
        <ccEmails>emailtosalesforce@20rrtqrv50l8afdcsqawcqachzy0df86p849c6zrnz6qojb02x.2-iypieag.wl.le.salesforce.com</ccEmails>
        <description>Event - Confirmation Email (EN) - for activity history</description>
        <protected>false</protected>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Event_Confirmation_Email_ENGLISH</template>
    </alerts>
    <alerts>
        <fullName>Event_Confirmation_Email_FR</fullName>
        <description>Event - Confirmation Email (FR)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Event_Confirmation_Email_FRENCH</template>
    </alerts>
    <alerts>
        <fullName>Event_Confirmation_Email_FR_for_activity_history</fullName>
        <ccEmails>emailtosalesforce@20rrtqrv50l8afdcsqawcqachzy0df86p849c6zrnz6qojb02x.2-iypieag.wl.le.salesforce.com</ccEmails>
        <description>Event - Confirmation Email (FR) - for activity history</description>
        <protected>false</protected>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Event_Confirmation_Email_FRENCH</template>
    </alerts>
    <alerts>
        <fullName>MITG_New_Lead_Assignment_Email</fullName>
        <description>MITG New Lead Assignment Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>MITG_Email_Templates/MITG_New_Lead_Assignment</template>
    </alerts>
    <alerts>
        <fullName>Partner_Confirmation_Email_EN</fullName>
        <description>Partner - Confirmation Email (EN)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Partner_Confirmation_Email_ENGLISH</template>
    </alerts>
    <alerts>
        <fullName>Partner_Confirmation_Email_EN_for_Activity_History</fullName>
        <ccEmails>emailtosalesforce@20rrtqrv50l8afdcsqawcqachzy0df86p849c6zrnz6qojb02x.2-iypieag.wl.le.salesforce.com</ccEmails>
        <description>Partner - Confirmation Email (EN) - (for Activity History)</description>
        <protected>false</protected>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Partner_Confirmation_Email_ENGLISH</template>
    </alerts>
    <alerts>
        <fullName>Partner_Confirmation_Email_FR</fullName>
        <description>Partner - Confirmation Email (FR)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Partner_Confirmation_Email_FRENCH</template>
    </alerts>
    <alerts>
        <fullName>Partner_Confirmation_Email_FR_For_activity_history</fullName>
        <ccEmails>emailtosalesforce@20rrtqrv50l8afdcsqawcqachzy0df86p849c6zrnz6qojb02x.2-iypieag.wl.le.salesforce.com</ccEmails>
        <description>Partner - Confirmation Email (FR) - For activity history</description>
        <protected>false</protected>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Partner_Confirmation_Email_FRENCH</template>
    </alerts>
    <alerts>
        <fullName>Pharmacy_Confirmation_Email_EN</fullName>
        <description>Pharmacy - Confirmation Email (EN)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Pharmacy_Confirmation_Email_ENGLISH</template>
    </alerts>
    <alerts>
        <fullName>Pharmacy_Confirmation_Email_EN_for_activity_history</fullName>
        <ccEmails>emailtosalesforce@20rrtqrv50l8afdcsqawcqachzy0df86p849c6zrnz6qojb02x.2-iypieag.wl.le.salesforce.com</ccEmails>
        <description>Pharmacy - Confirmation Email (EN) - for activity history</description>
        <protected>false</protected>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Pharmacy_Confirmation_Email_ENGLISH</template>
    </alerts>
    <alerts>
        <fullName>Pharmacy_Confirmation_Email_FR</fullName>
        <description>Pharmacy - Confirmation Email (FR)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Pharmacy_Confirmation_Email_FRENCH</template>
    </alerts>
    <alerts>
        <fullName>Pharmacy_Confirmation_Email_FR_for_activity_history</fullName>
        <ccEmails>emailtosalesforce@20rrtqrv50l8afdcsqawcqachzy0df86p849c6zrnz6qojb02x.2-iypieag.wl.le.salesforce.com</ccEmails>
        <description>Pharmacy - Confirmation Email (FR) - for activity history</description>
        <protected>false</protected>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Pharmacy_Confirmation_Email_FRENCH</template>
    </alerts>
    <alerts>
        <fullName>Rep_Confirmation_Email_EN</fullName>
        <description>Rep - Confirmation Email (EN)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Rep_Confirmation_Email_EN</template>
    </alerts>
    <alerts>
        <fullName>Rep_Confirmation_Email_EN_for_activity_history</fullName>
        <ccEmails>emailtosalesforce@20rrtqrv50l8afdcsqawcqachzy0df86p849c6zrnz6qojb02x.2-iypieag.wl.le.salesforce.com</ccEmails>
        <description>Rep - Confirmation Email (EN) - for activity history</description>
        <protected>false</protected>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Rep_Confirmation_Email_EN</template>
    </alerts>
    <alerts>
        <fullName>Rep_Confirmation_Email_FR</fullName>
        <description>Rep - Confirmation Email (FR)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Rep_Confirmation_Email_FR</template>
    </alerts>
    <alerts>
        <fullName>Rep_Confirmation_Email_FR_for_activity_history</fullName>
        <ccEmails>emailtosalesforce@20rrtqrv50l8afdcsqawcqachzy0df86p849c6zrnz6qojb02x.2-iypieag.wl.le.salesforce.com</ccEmails>
        <description>Rep - Confirmation Email (FR) - for activity history</description>
        <protected>false</protected>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Rep_Confirmation_Email_FR</template>
    </alerts>
    <alerts>
        <fullName>Send_autoresponse</fullName>
        <description>Send autoresponse</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>sfe@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>MITG_Email_Templates/MITG_Connect_me_auto_response</template>
    </alerts>
    <alerts>
        <fullName>Sent_Email_Template_A1_EN</fullName>
        <description>Sent Email Template A1 (EN)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_A1_EN</template>
    </alerts>
    <alerts>
        <fullName>Sent_Email_Template_A1_EN_FOR_ACTIVITY_HISTORY</fullName>
        <ccEmails>emailtosalesforce@20rrtqrv50l8afdcsqawcqachzy0df86p849c6zrnz6qojb02x.2-iypieag.wl.le.salesforce.com</ccEmails>
        <description>Sent Email Template A1 (EN) FOR ACTIVITY HISTORY</description>
        <protected>false</protected>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_A1_EN</template>
    </alerts>
    <alerts>
        <fullName>Sent_Email_Template_A1_FR</fullName>
        <description>Sent Email Template A1 (FR)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_A1_FR</template>
    </alerts>
    <alerts>
        <fullName>Sent_Email_Template_A1_FR_FOR_ACTIVITY_HISTORY</fullName>
        <ccEmails>emailtosalesforce@20rrtqrv50l8afdcsqawcqachzy0df86p849c6zrnz6qojb02x.2-iypieag.wl.le.salesforce.com</ccEmails>
        <description>Sent Email Template A1 (FR) FOR ACTIVITY HISTORY</description>
        <protected>false</protected>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_A1_FR</template>
    </alerts>
    <alerts>
        <fullName>Sent_Email_Template_A_EN</fullName>
        <description>Sent Email Template A (EN)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_A_EN</template>
    </alerts>
    <alerts>
        <fullName>Sent_Email_Template_A_EN_FOR_ACTIVITY_HISTORY</fullName>
        <ccEmails>emailtosalesforce@20rrtqrv50l8afdcsqawcqachzy0df86p849c6zrnz6qojb02x.2-iypieag.wl.le.salesforce.com</ccEmails>
        <description>Sent Email Template A (EN) FOR ACTIVITY HISTORY</description>
        <protected>false</protected>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_A_EN</template>
    </alerts>
    <alerts>
        <fullName>Sent_Email_Template_A_FR</fullName>
        <description>Sent Email Template A (FR)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_A_FR</template>
    </alerts>
    <alerts>
        <fullName>Sent_Email_Template_A_FR_FOR_ACTIVITY_HISTORY</fullName>
        <ccEmails>emailtosalesforce@20rrtqrv50l8afdcsqawcqachzy0df86p849c6zrnz6qojb02x.2-iypieag.wl.le.salesforce.com</ccEmails>
        <description>Sent Email Template A (FR) FOR ACTIVITY HISTORY</description>
        <protected>false</protected>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_A_FR</template>
    </alerts>
    <alerts>
        <fullName>Sent_Email_Template_C1_EN</fullName>
        <description>Sent Email Template C1 (EN)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_Email_Templates/Template_C1_EN</template>
    </alerts>
    <alerts>
        <fullName>Sent_Email_Template_C1_EN_FOR_ACTIVITY_HISTORY</fullName>
        <ccEmails>emailtosalesforce@2ocip1khq3ymyi9s5iasyx5fcwx5y3phknxkl15iwzgiqcyid1.w-73ap5mai.w.le.sandbox.salesforce.com</ccEmails>
        <description>Sent Email Template C1 (EN) FOR ACTIVITY HISTORY</description>
        <protected>false</protected>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_Email_Templates/Template_C1_EN</template>
    </alerts>
    <alerts>
        <fullName>Sent_Email_Template_C1_FR</fullName>
        <description>Sent Email Template C1 (FR)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_Email_Templates/Template_C1_FR</template>
    </alerts>
    <alerts>
        <fullName>Sent_Email_Template_C1_FR_FOR_ACTIVITY_HISTORY</fullName>
        <ccEmails>emailtosalesforce@2ocip1khq3ymyi9s5iasyx5fcwx5y3phknxkl15iwzgiqcyid1.w-73ap5mai.w.le.sandbox.salesforce.com</ccEmails>
        <description>Sent Email Template C1 (FR) FOR ACTIVITY HISTORY</description>
        <protected>false</protected>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_Email_Templates/Template_C1_FR</template>
    </alerts>
    <alerts>
        <fullName>Sent_email_CAN_DIB_not_equal_to_product_interest_Guardian_Sensors</fullName>
        <description>Sent email CAN DIB not equal to product interest Guardian + Sensors</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_To_ID__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAN_DIB_IS_Automated/Guardian_Connect_Lead_Notification</template>
    </alerts>
    <alerts>
        <fullName>Sent_email_CAN_DIB_product_interest_Guardian_Sensors</fullName>
        <description>Sent email CAN DIB product interest Guardian + Sensors</description>
        <protected>false</protected>
        <recipients>
            <recipient>lori.vanderveen@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAN_DIB_IS_Automated/Guardian_Connect_Lead_Notification</template>
    </alerts>
    <alerts>
        <fullName>lead_has_not_been_assigned_to_a_user_for_6_days</fullName>
        <description>lead has not been assigned to a user for 6 days</description>
        <protected>false</protected>
        <recipients>
            <recipient>robin.quaedflieg@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>MITG_Email_Templates/lead_has_not_been_assigned_to_a_user_for_6_days</template>
    </alerts>
    <alerts>
        <fullName>lead_has_not_been_assigned_to_a_user_for_a_few_days</fullName>
        <description>lead has not been assigned to a user for a few days</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>MITG_Email_Templates/lead_has_not_been_assigned_to_a_user_for_a_few_days</template>
    </alerts>
    <fieldUpdates>
        <fullName>ChgLeadStatustoNew</fullName>
        <field>Status</field>
        <literalValue>New</literalValue>
        <name>Chg Lead Status to New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Correct_Lead_Start_Date</fullName>
        <field>Start_Date__c</field>
        <formula>ANZ_DIB_Warranty_Expiration__c - 90</formula>
        <name>Correct Lead Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Existing_Customer_is_True</fullName>
        <field>ANZ_DIB_Existing_DIB_Customer__c</field>
        <literalValue>1</literalValue>
        <name>Existing Customer is True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_LEAD_SetDiabetesFlagToTrue</fullName>
        <description>Flag the Lead for Diabetes. Necesarry for Lead to Account convertion, where the Diabetes flag on the account needs to be set on creation for proper territory assignment.</description>
        <field>Diabetes__c</field>
        <literalValue>1</literalValue>
        <name>FU_LEAD_SetDiabetesFlagToTrue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_LEAD_Set_CompleteBoolFalse</fullName>
        <description>Set reimbursement complete bool fied = false</description>
        <field>Reimbursement_Fields_Complete_Bool__c</field>
        <literalValue>0</literalValue>
        <name>FU_LEAD_Set_CompleteBoolFalse</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_LEAD_copybirthdate</fullName>
        <field>Primary_Plan_Member_Birthdate__c</field>
        <formula>Birthdate__c</formula>
        <name>FU_LEAD_copybirthdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_LEAD_copybirthdate_2</fullName>
        <field>Secondary_Plan_Member_Birthdate__c</field>
        <formula>Birthdate__c</formula>
        <name>FU_LEAD_copybirthdate_2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_LEAD_copyfirstname</fullName>
        <field>Primary_Plan_Member_First_Name_Text__c</field>
        <formula>FirstName</formula>
        <name>FU_LEAD_copyfirstname</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_LEAD_copyfirstname_2</fullName>
        <field>Secondary_Plan_Member_First_Name_Text__c</field>
        <formula>FirstName</formula>
        <name>FU_LEAD_copyfirstname_2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_LEAD_copylastname</fullName>
        <field>Primary_Plan_Member_Last_Name_Text__c</field>
        <formula>LastName</formula>
        <name>FU_LEAD_copylastname</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_LEAD_copylastname_2</fullName>
        <field>Secondary_Plan_Member_Last_Name_Text__c</field>
        <formula>LastName</formula>
        <name>FU_LEAD_copylastname_2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_LEAD_setFollowUpActionToPhone</fullName>
        <field>Follow_up_Actions__c</field>
        <formula>&quot;Contact lead to ask his/her phone number&quot;</formula>
        <name>FU_LEAD_setFollowUpActionToPhone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_SET_RT_To_CAN_DIB_LEAD</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CAN_DiB_Lead</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>FU_SET_RT_To_CAN_DIB_LEAD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_SetStatusToUnqualified</fullName>
        <field>Status</field>
        <literalValue>Unqualified</literalValue>
        <name>FU_SetStatusToUnqualified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_CAN_DIB_ConvertStatusToQualified</fullName>
        <description>update status field to qualified</description>
        <field>Status</field>
        <literalValue>Qualified</literalValue>
        <name>Lead: CAN DIB ConvertStatusToQualified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_CAN_DIB_SET_Follow_upTaskTo_Call_L</fullName>
        <field>Task_Call_Lead__c</field>
        <literalValue>To Do</literalValue>
        <name>Lead: CAN DIB SET Follow-upTaskTo Call L</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_CAN_DIB_SET_Follow_upTaskTo_FUONRL</fullName>
        <description>Set picklist &quot;Task: Phone / Phone Opt-in Follow-up&quot; to &quot;ToDo&quot;</description>
        <field>Task_Phone_Phone_Opt_in_Follow_up__c</field>
        <literalValue>To Do</literalValue>
        <name>Lead: CAN DIB SET Follow-upTaskTo FUONRL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_CAN_DIB_SET_Follow_upTaskTo_RHCI</fullName>
        <description>Set picklist field &quot;Request Hard Copy Info Pack&quot; to &quot;To Do&quot;</description>
        <field>Task_Requested_Hard_Copy_Info_Pack__c</field>
        <literalValue>To Do</literalValue>
        <name>Lead: CAN DIB SET Follow-upTaskTo RHCI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_CAN_DIB_SET_Follow_upTask_To_PHPHO</fullName>
        <description>Phone/ Phone Opt-in Follow up</description>
        <field>Task_Phone_Phone_Opt_in_Follow_up__c</field>
        <literalValue>To Do</literalValue>
        <name>Lead: CAN DIB SET Follow-upTask To PPO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_CAN_DIB_SetHiddenBTRUE</fullName>
        <field>HIDDEN_B_Send__c</field>
        <literalValue>1</literalValue>
        <name>Lead: CAN DIB SetHiddenBTRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_CAN_DIB_Set_Default_Lead_Status</fullName>
        <field>Status</field>
        <literalValue>Unqualified</literalValue>
        <name>Lead: CAN DIB Set Default Lead Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_CAN_DIB_Update_RecordType_To_Quali</fullName>
        <description>Update CAN DIB LEAD Recordtype to Qulified.</description>
        <field>RecordTypeId</field>
        <lookupValue>CAN_DiB_Lead_Conversion</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Lead: CAN DIB Update RecordType To Quali</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_City_copy</fullName>
        <field>Lead_City__c</field>
        <formula>City</formula>
        <name>Lead City copy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Copy_Email_to_Account_Email</fullName>
        <description>Copy Email to Account_Email__c which is mapped to Account.account_Email__c to align with SAP process</description>
        <field>Account_Email__c</field>
        <formula>Email</formula>
        <name>Lead: Copy Email to Account Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_DIB_Set_Email_Opt_in_Date</fullName>
        <field>Email_Opt_In_Date__c</field>
        <formula>TODAY()</formula>
        <name>Lead: DIB Set Email Opt-in Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_DIB_Set_Phone_Opt_in_Date</fullName>
        <field>Phone_Opt_In_Date__c</field>
        <formula>TODAY()</formula>
        <name>Lead: DIB Set Phone Opt-in Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_DIB_Set_Text_Opt_in_Date</fullName>
        <field>Text_Opt_In_Date__c</field>
        <formula>TODAY()</formula>
        <name>Lead: DIB Set Text Opt-in Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_DIB_Set_mail_Opt_in_Date</fullName>
        <field>Mail_Opt_In_Date__c</field>
        <formula>TODAY()</formula>
        <name>Lead: DIB Set mail Opt-in Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Postalcode_copy</fullName>
        <field>Lead_Zip_Postal_Code__c</field>
        <formula>PostalCode</formula>
        <name>Lead Postalcode copy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Street_copy</fullName>
        <field>Lead_Address_Line_1__c</field>
        <formula>Street</formula>
        <name>Lead Street copy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MITG_Time_to_Close_field_update</fullName>
        <field>Time_to_Close__c</field>
        <formula>NOW()-(CreatedDate)</formula>
        <name>MITG Time to Close field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MITG_Time_to_Contact_catch_field_update</fullName>
        <field>Time_to_Contact__c</field>
        <formula>NOW()- (CreatedDate)</formula>
        <name>MITG Time to Contact catch field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MITG_Time_to_Contact_field_update</fullName>
        <field>Time_to_Contact__c</field>
        <formula>NOW()-(CreatedDate)</formula>
        <name>MITG Time to Contact field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Rejection_Date</fullName>
        <field>Rejection_Date__c</field>
        <formula>Today()</formula>
        <name>Set Rejection Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Task_Requested_Hard_Copy_Info_Pack</fullName>
        <description>Updates the &quot;Task: Requested Hard Copy Info Pack&quot; field to &quot;To Do&quot;</description>
        <field>Task_Requested_Hard_Copy_Info_Pack__c</field>
        <literalValue>To Do</literalValue>
        <name>Task: Requested Hard Copy Info Pack</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Existing_Customer</fullName>
        <field>ANZ_DIB_Existing_DIB_Customer__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Existing Customer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Lead%3A ANZ DIB Micro-site Lead Email</fullName>
        <actions>
            <name>ANZ_DIB_MicroSiteAutoResponseEmail</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>ANZ DIB Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Campaign</value>
        </criteriaItems>
        <description>Triggers an auto-response email for new leads created via the ANZ Diabetes micro-site.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A ANZ DIB Web Lead Confirmation Email</fullName>
        <actions>
            <name>ANZ_DIB_Web_Lead_Confirmation_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>ANZ DIB Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <description>Send the confirmation email to the query requester when the query is submitted via online form for ANZ DIB team</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A ANZ DIB Web Lead Record Type Update</fullName>
        <actions>
            <name>ChgLeadStatustoNew</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>By default, any web leads will be created with the Lead Status of &quot;Open&quot;. For ANZ DIB leads, it is necessary that the Lead Status be updated to &quot;New&quot; when new leads are created in the system.</description>
        <formula>AND($RecordType.DeveloperName = &quot;ANZ_DIB_Lead&quot;, ISPICKVAL(Status, &quot;Open&quot;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A CAN DIB Assign info Pack task to PCS agent</fullName>
        <actions>
            <name>Task_Requested_Hard_Copy_Info_Pack</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When &quot;Requested Hard Copy Info Pack&quot; is checked as task has to be created and assigned to the generic PCS agent user.  A task should be created only once.</description>
        <formula>AND(   OR(RecordType.DeveloperName = &quot;CAN_DiB_Lead&quot;,      RecordType.DeveloperName = &quot;CAN_DiB_Lead_Conversion&quot;),  Requested_Hard_Copy_Info_Pack__c = TRUE,   NOT(ISPICKVAL(Task_Requested_Hard_Copy_Info_Pack__c,&quot;Done&quot;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A CAN DIB ConvertToQualified</fullName>
        <actions>
            <name>Lead_CAN_DIB_ConvertStatusToQualified</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lead_CAN_DIB_Update_RecordType_To_Quali</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Change record type to Qualified to show Convert button on page</description>
        <formula>AND( 
RecordType.DeveloperName = &quot;CAN_DiB_Lead&quot;, 
NOT(ISBLANK( FirstName )), 
NOT(ISBLANK( LastName )), 
NOT(ISPICKVAL( Lead_Gender_Text__c,&quot;&quot; )), 
NOT(ISBLANK( Email )), 
NOT(ISBLANK( Lead_Address_Line_1__c )), 
NOT(ISPICKVAL( Lead_State_Province__c,&quot;&quot; )), 
NOT(ISBLANK( Lead_City__c )), 
NOT(ISBLANK( Lead_Zip_Postal_Code__c )), 
ISPICKVAL(Owner:User.Job_Title_vs__c, &quot;Sales Rep&quot;), 
OR( 
NOT(ISBLANK( Phone )), 
NOT(ISBLANK( MobilePhone )), 
NOT(ISBLANK( Work_Phone__c ))), 
NOT(ISPICKVAL( Lead_Country_vs__c ,&quot;&quot; )), 
NOT(ISPICKVAL( Preferred_Language__c,&quot;&quot; )), 
NOT(ISPICKVAL( Status,&quot;&quot; )), 
NOT(ISBLANK ( DIB_Product_Interest__c )), 
OR( 
AND( 
ISPICKVAL(Currently_using_a_pump__c, &quot;Yes&quot;), 
NOT(ISPICKVAL( DIB_Current_Pump_Maker__c ,&quot;&quot; )) ), 
(ISPICKVAL(Currently_using_a_pump__c, &quot;No&quot;)), 
(ISPICKVAL(Currently_using_a_pump__c, &quot;Unknown&quot;)) ), 
OR( 
AND( 
Requested_Reimbursement_Verification_Boo__c == TRUE, 
NOT(ISBLANK( Birthdate__c)), 
NOT(ISBLANK( Primary_Plan_Member_First_Name_Text__c )), 
NOT(ISBLANK( Primary_Plan_Member_Last_Name_Text__c )), 
NOT(ISBLANK( Primary_Plan_Member_Birthdate__c )), 
NOT(ISPICKVAL( Primary_Patient_Relation_to_Plan_Mem_Tex__c,&quot;&quot; )), 
NOT(ISPICKVAL( Primary_Employment_Status_Text__c,&quot;&quot; )), 
NOT(ISBLANK( Primary_Employer_Text__c )), 
NOT(ISBLANK( Primary_Health_Insurer_ID__c )), 
NOT(ISBLANK( Primary_Group_No_Text__c )), 
NOT(ISBLANK( Authorized_By_Initials__c )), 
NOT(ISBLANK( Contact_ID__c )) ), 
Requested_Reimbursement_Verification_Boo__c == FALSE) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A CAN DIB Fill Primary Insurance Member Info</fullName>
        <actions>
            <name>FU_LEAD_copybirthdate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_LEAD_copyfirstname</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_LEAD_copylastname</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  OR(RecordType.DeveloperName = &quot;CAN_DiB_Lead&quot;,     RecordType.DeveloperName = &quot;CAN_DiB_Lead_Conversion&quot;),  Prospect_is_primary_plan_Member_Bool__c =  TRUE,  OR( ISNEW(), ISCHANGED(Prospect_is_primary_plan_Member_Bool__c ), ISCHANGED( FirstName ), ISCHANGED( LastName ), ISCHANGED( Birthdate__c )     ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A CAN DIB Fill Secondary Insurance Member Info</fullName>
        <actions>
            <name>FU_LEAD_copybirthdate_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_LEAD_copyfirstname_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_LEAD_copylastname_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  OR(RecordType.DeveloperName = &quot;CAN_DiB_Lead&quot;,      RecordType.DeveloperName = &quot;CAN_DiB_Lead_Conversion&quot;),    Prospect_is_Secondary_Plan_Member_Bool__c ==  TRUE,  OR( ISNEW(), ISCHANGED(Prospect_is_Secondary_Plan_Member_Bool__c ), ISCHANGED( FirstName ), ISCHANGED( LastName ), ISCHANGED( Birthdate__c )     ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A CAN DIB Phone No and Opt-In Provided via Web</fullName>
        <actions>
            <name>Lead_CAN_DIB_SET_Follow_upTaskTo_Call_L</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Create follow up task for IS to follow up on Lead via phone</description>
        <formula>AND  (  OR(RecordType.DeveloperName = &quot;CAN_DiB_Lead&quot;,  RecordType.DeveloperName = &quot;CAN_DiB_Lead_Conversion&quot;),  NOT(ISBLANK(Phone)),  ISPICKVAL( Phone_Opt_In__c ,&quot;Yes&quot;), NOT(ISPICKVAL(Task_Call_Lead__c,&quot;Done&quot;))  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A CAN DIB Product Interest Guardian %2B Sensors</fullName>
        <actions>
            <name>Sent_email_CAN_DIB_product_interest_Guardian_Sensors</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>CAN DiB Lead,CAN DiB Lead (Conversion)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.DIB_Product_Interest__c</field>
            <operation>equals</operation>
            <value>Guardian + Sensors</value>
        </criteriaItems>
        <description>CAN DIB product interest Guardian + Sensors</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A CAN DIB Requested Phone No%2E and Phone Opt-In By Email %28EN%29</fullName>
        <active>false</active>
        <description>Related to use case: New Lead: phone number not specified or phone opt-in not granted.</description>
        <formula>AND (   OR(RecordType.DeveloperName = &quot;CAN_DiB_Lead&quot;,      RecordType.DeveloperName = &quot;CAN_DiB_Lead_Conversion&quot;),   NOT(ispickval(LeadSource, &quot;Phone&quot;)),   Force_Convert__c == FALSE, OR(  AND(Phone = null, MobilePhone = null,  Work_Phone__c  = null),  NOT(IsPickval(Phone_Opt_In__c,&quot;Yes&quot;)) ),  NOT(ispickval(Preferred_Language__c, &quot;French&quot;)), NOT (ISPICKVAL(Status, &quot;Rejected&quot;)), ISPICKVAL(Email_Opt_In__c, &quot;Yes&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CAN_DIB_Requested_Phone_No_and_Phone_Opt_In_By_Email_EN</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>CAN_DIB_Requested_Phone_No_and_Phone_Opt_In_By_Email_EN_ACTIVITY_HISTORY</name>
                <type>Alert</type>
            </actions>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>CAN_DIB_Requested_Phone_No_and_Phone_Opt_In_By_Email_D1_EN</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>CAN_DIB_Requested_Phone_No_and_Phone_Opt_In_By_Email_D1_EN_ACTIVITY_HISTORY</name>
                <type>Alert</type>
            </actions>
            <timeLength>10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Lead_CAN_DIB_SET_Follow_upTaskTo_FUONRL</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>15</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Lead%3A CAN DIB Requested Phone No%2E and Phone Opt-In By Email %28FR%29</fullName>
        <active>false</active>
        <description>Related to use case: New Lead: phone number not specified or phone opt-in not granted.</description>
        <formula>AND (   OR(RecordType.DeveloperName = &quot;CAN_DiB_Lead&quot;,      RecordType.DeveloperName = &quot;CAN_DiB_Lead_Conversion&quot;),   NOT(ispickval(LeadSource, &quot;Phone&quot;)),   Force_Convert__c == FALSE, OR(  AND(Phone = null, MobilePhone = null, Work_Phone__c = null),   NOT(IsPickval(Phone_Opt_In__c,&quot;Yes&quot;)) ),  ispickval(Preferred_Language__c, &quot;French&quot;),NOT(ISPICKVAL(Status, &quot;Rejected&quot;)), ISPICKVAL(Email_Opt_In__c, &quot;Yes&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Lead_CAN_DIB_SET_Follow_upTaskTo_FUONRL</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>15</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>CAN_DIB_Requested_Phone_No_and_Phone_Opt_In_By_Email_FR</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>CAN_DIB_Requested_Phone_No_and_Phone_Opt_In_By_Email_FR_ACTIVITY_HISTORY</name>
                <type>Alert</type>
            </actions>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>CAN_DIB_Requested_Phone_No_and_Phone_Opt_In_By_Email_D1_FR</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>CAN_DIB_Requested_Phone_No_and_Phone_Opt_In_By_Email_D1_FR_ACTIVITY_HISTORY</name>
                <type>Alert</type>
            </actions>
            <timeLength>10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Lead%3A CAN DIB Send Welcome EmailTemplate A %28EN%29</fullName>
        <actions>
            <name>Sent_Email_Template_A_EN</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sent_Email_Template_A_EN_FOR_ACTIVITY_HISTORY</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Use case: New Lead Via Web Send out template A</description>
        <formula>AND ( RecordType.DeveloperName = &quot;CAN_DiB_Lead&quot;, NOT(ispickval(Preferred_Language__c, &quot;French&quot;)), ISPICKVAL(LeadSource,&quot;Web&quot;), Requested_Digital_Info_Pack_Bool__c == TRUE, NOT (ISPICKVAL(Status, &quot;Rejected&quot;)), NOT(ISPICKVAL(Lead_Referral_Text__c , &quot;Bayer&quot;)), ISPICKVAL(Email_Opt_In__c, &quot;Yes&quot;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A CAN DIB Send Welcome EmailTemplate A %28FR%29</fullName>
        <actions>
            <name>Sent_Email_Template_A_FR</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sent_Email_Template_A_FR_FOR_ACTIVITY_HISTORY</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Use case: New Lead Via Web Send out template A</description>
        <formula>AND ( RecordType.DeveloperName = &quot;CAN_DiB_Lead&quot;, ISPICKVAL(Preferred_Language__c, &quot;French&quot;), ISPICKVAL(LeadSource,&quot;Web&quot;), Requested_Digital_Info_Pack_Bool__c == TRUE, NOT (ISPICKVAL(Status, &quot;Rejected&quot;)),NOT(ISPICKVAL(Lead_Referral_Text__c , &quot;Bayer&quot;)), ISPICKVAL(Email_Opt_In__c, &quot;Yes&quot;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A CAN DIB Send Welcome EmailTemplate A1 %28EN%29</fullName>
        <actions>
            <name>Sent_Email_Template_A1_EN</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sent_Email_Template_A1_EN_FOR_ACTIVITY_HISTORY</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When leadsource = &quot;Phone&quot; send out template A1 instead of template A</description>
        <formula>AND (  RecordType.DeveloperName = &quot;CAN_DiB_Lead&quot;,  ISPICKVAL(LeadSource,&quot;Phone&quot;),  ISPICKVAL( Email_Opt_In__c ,&quot;Yes&quot;),  NOT(ISPICKVAL(Preferred_Language__c, &quot;French&quot;)),NOT(ISPICKVAL(Status, &quot;Rejected&quot;))  )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A CAN DIB Send Welcome EmailTemplate A1 %28FR%29</fullName>
        <actions>
            <name>Sent_Email_Template_A1_FR</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sent_Email_Template_A1_FR_FOR_ACTIVITY_HISTORY</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When leadsource = &quot;Phone&quot; send out template A1 instead of template A</description>
        <formula>AND (  RecordType.DeveloperName = &quot;CAN_DiB_Lead&quot;,  ISPICKVAL(LeadSource,&quot;Phone&quot;),  ISPICKVAL( Email_Opt_In__c ,&quot;Yes&quot;),  ISPICKVAL(Preferred_Language__c, &quot;French&quot;),NOT(ISPICKVAL(Status, &quot;Rejected&quot;)))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A CAN DIB Send Welcome EmailTemplate Bayer %28EN%29</fullName>
        <actions>
            <name>Partner_Confirmation_Email_EN</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Partner_Confirmation_Email_EN_for_Activity_History</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Use case: New Bayer Lead Via Web Send out template: Partner - Confirmation Email (EN)</description>
        <formula>AND ( RecordType.DeveloperName = &quot;CAN_DiB_Lead&quot;, NOT(ispickval(Preferred_Language__c, &quot;French&quot;)), ISPICKVAL(LeadSource,&quot;Web&quot;), ISPICKVAL(Lead_Referral_Text__c , &quot;Bayer&quot;), NOT (ISPICKVAL(Status, &quot;Rejected&quot;)), ISPICKVAL(Email_Opt_In__c, &quot;Yes&quot;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A CAN DIB Send Welcome EmailTemplate Bayer %28FR%29</fullName>
        <actions>
            <name>Partner_Confirmation_Email_FR</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Partner_Confirmation_Email_FR_For_activity_history</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Use case: New Bayer Lead Via Web Send out template: Partner - Confirmation Email (FR)</description>
        <formula>AND ( RecordType.DeveloperName = &quot;CAN_DiB_Lead&quot;, ISPICKVAL(Preferred_Language__c, &quot;French&quot;), ISPICKVAL(LeadSource,&quot;Web&quot;), ISPICKVAL(Lead_Referral_Text__c , &quot;Bayer&quot;), NOT (ISPICKVAL(Status, &quot;Rejected&quot;)), ISPICKVAL(Email_Opt_In__c, &quot;Yes&quot;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A CAN DIB Send Welcome EmailTemplate local event %28EN%29</fullName>
        <actions>
            <name>Event_Confirmation_Email_EN</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Event_Confirmation_Email_EN_for_activity_history</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Use case: New local event Via Web Send out template: Event – Confirmation Email (EN)</description>
        <formula>AND ( RecordType.DeveloperName = &quot;CAN_DiB_Lead&quot;, NOT(ispickval(Preferred_Language__c, &quot;French&quot;)), ISPICKVAL(LeadSource,&quot;Local Event&quot;), ISPICKVAL(Lead_Referral_Text__c , &quot;Information Session&quot;), NOT (ISPICKVAL(Status, &quot;Rejected&quot;)), ISPICKVAL(Email_Opt_In__c, &quot;Yes&quot;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A CAN DIB Send Welcome EmailTemplate local event %28FR%29</fullName>
        <actions>
            <name>Event_Confirmation_Email_FR</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Event_Confirmation_Email_FR_for_activity_history</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Use case: New local event Via Web Send out template: Event – Confirmation Email (FR)</description>
        <formula>AND ( RecordType.DeveloperName = &quot;CAN_DiB_Lead&quot;, ISPICKVAL(Preferred_Language__c, &quot;French&quot;), ISPICKVAL(LeadSource,&quot;Local Event&quot;), ISPICKVAL(Lead_Referral_Text__c , &quot;Information Session&quot;), NOT (ISPICKVAL(Status, &quot;Rejected&quot;)), ISPICKVAL(Email_Opt_In__c, &quot;Yes&quot;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A CAN DIB Send Welcome EmailTemplate mdt%2Erep %28EN%29</fullName>
        <actions>
            <name>Rep_Confirmation_Email_EN</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Rep_Confirmation_Email_EN_for_activity_history</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Use case: New rep Lead Via Web Send out template: Rep – Confirmation Email (EN)</description>
        <formula>AND ( RecordType.DeveloperName = &quot;CAN_DiB_Lead&quot;, NOT(ispickval(Preferred_Language__c, &quot;French&quot;)), ISPICKVAL(LeadSource,&quot;Sales Rep&quot;), ISPICKVAL(Lead_Referral_Text__c , &quot;Medtronic Rep&quot;), NOT (ISPICKVAL(Status, &quot;Rejected&quot;)), ISPICKVAL(Email_Opt_In__c, &quot;Yes&quot;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A CAN DIB Send Welcome EmailTemplate mdt%2Erep %28FR%29</fullName>
        <actions>
            <name>Rep_Confirmation_Email_FR</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Rep_Confirmation_Email_FR_for_activity_history</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Use case: New rep Lead Via Web Send out template: Rep – Confirmation Email (FR)</description>
        <formula>AND ( RecordType.DeveloperName = &quot;CAN_DiB_Lead&quot;, ISPICKVAL(Preferred_Language__c, &quot;French&quot;), ISPICKVAL(LeadSource,&quot;Sales Rep&quot;), ISPICKVAL(Lead_Referral_Text__c , &quot;Medtronic rep&quot;), NOT (ISPICKVAL(Status, &quot;Rejected&quot;)), ISPICKVAL(Email_Opt_In__c, &quot;Yes&quot;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A CAN DIB Send Welcome EmailTemplate pharmacy %28EN%29</fullName>
        <actions>
            <name>Pharmacy_Confirmation_Email_EN</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Pharmacy_Confirmation_Email_EN_for_activity_history</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Use case: New pharmacy Lead Via Web Send out template: Pharmacy – Confirmation Email (EN)</description>
        <formula>AND ( RecordType.DeveloperName = &quot;CAN_DiB_Lead&quot;, NOT(ispickval(Preferred_Language__c, &quot;French&quot;)), ISPICKVAL(LeadSource,&quot;Pharmacy&quot;), ISPICKVAL(Lead_Referral_Text__c , &quot;Pharmacist&quot;), NOT (ISPICKVAL(Status, &quot;Rejected&quot;)), ISPICKVAL(Email_Opt_In__c, &quot;Yes&quot;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A CAN DIB Send Welcome EmailTemplate pharmacy %28FR%29</fullName>
        <actions>
            <name>Pharmacy_Confirmation_Email_FR</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Pharmacy_Confirmation_Email_FR_for_activity_history</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Use case: New pharmacy Lead Via Web Send out template: Pharmacy – Confirmation Email (FR)</description>
        <formula>AND ( RecordType.DeveloperName = &quot;CAN_DiB_Lead&quot;, ISPICKVAL(Preferred_Language__c, &quot;French&quot;), ISPICKVAL(LeadSource,&quot;Pharmacy&quot;), ISPICKVAL(Lead_Referral_Text__c , &quot;Pharmacist&quot;), NOT (ISPICKVAL(Status, &quot;Rejected&quot;)), ISPICKVAL(Email_Opt_In__c, &quot;Yes&quot;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A CAN DIB Set Default Lead Status</fullName>
        <actions>
            <name>Lead_CAN_DIB_Set_Default_Lead_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>CAN DiB Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>equals</operation>
            <value>System</value>
        </criteriaItems>
        <description>Set default lead status is lead is created from the web-2-lead form</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A CAN DIB Set Diabetes flag</fullName>
        <actions>
            <name>FU_LEAD_SetDiabetesFlagToTrue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>CAN DiB Lead,CAN DiB Lead (Conversion)</value>
        </criteriaItems>
        <description>Flag the Lead for Diabetes. Necesarry for Lead to Account convertion, where the Diabetes flag on the account needs to be set on creation for proper territory assignment.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A CAN DIB Set Email Opt-In Date upon Creation%2Fupdate</fullName>
        <actions>
            <name>Lead_DIB_Set_Email_Opt_in_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( OR(  ISNEW(), ISCHANGED(Email_Opt_In__c)), OR(RecordType.DeveloperName = &quot;CAN_DiB_Lead&quot;,  RecordType.DeveloperName = &quot;CAN_DiB_Lead_Conversion&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A CAN DIB Set Phone Opt-In Date upon Creation%2Fupdate</fullName>
        <actions>
            <name>Lead_DIB_Set_Phone_Opt_in_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  OR( ISNEW() , ISCHANGED( Phone_Opt_In__c )),  OR(RecordType.DeveloperName = &quot;CAN_DiB_Lead&quot;,  RecordType.DeveloperName = &quot;CAN_DiB_Lead_Conversion&quot;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A CAN DIB Set Text Opt-In Date upon Creation%2Fupdate</fullName>
        <actions>
            <name>Lead_DIB_Set_Text_Opt_in_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( OR(  ISNEW(), ISCHANGED( Text_Opt_In__c )), OR(RecordType.DeveloperName = &quot;CAN_DiB_Lead&quot;,  RecordType.DeveloperName = &quot;CAN_DiB_Lead_Conversion&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A CAN DIB Set mail Opt-In Date upon Creation%2Fupdate</fullName>
        <actions>
            <name>Lead_DIB_Set_mail_Opt_in_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  OR( ISNEW() , ISCHANGED( Mail_Opt_In__c )),  OR(RecordType.DeveloperName = &quot;CAN_DiB_Lead&quot;,  RecordType.DeveloperName = &quot;CAN_DiB_Lead_Conversion&quot;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A CAN DIB Update Reimbursement Fields Complete Checkbox</fullName>
        <actions>
            <name>CAN_DIB_Update_Insurance_Info_EN</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CAN_DIB_Update_Insurance_Info_EN_FOR_ACTIVITY_HISTORY</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Lead_CAN_DIB_SetHiddenBTRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Related to use case: Existing lead via web - Update Insurance Info (EN)</description>
        <formula>AND  (  RecordType.DeveloperName = &quot;CAN_DiB_Lead&quot;, ISPICKVAL(LeadSource,&quot;Web&quot;),  Requested_Reimbursement_Verification_Boo__c = TRUE,  HIDDEN_B_Send__c = FALSE, NOT(ispickval(Preferred_Language__c, &quot;French&quot;)), OR( ISBLANK(Primary_Plan_Member_First_Name_Text__c),  ISPICKVAL(Primary_Patient_Relation_to_Plan_Mem_Tex__c,&quot;&quot;),  ISBLANK(Primary_Plan_Member_Last_Name_Text__c),  ISBLANK( Primary_Health_Insurer_ID__c),  ISPICKVAL(Primary_Employment_Status_Text__c,&quot;&quot;),  ISBLANK(Primary_Plan_Member_Birthdate__c),  ISBLANK(Primary_Employer_Text__c),  ISBLANK(Primary_Group_No_Text__c),  ISBLANK(Primary_Employer_Phone__c),  ISBLANK( Birthdate__c ), ISBLANK( Patient_Health_Card_Text__c ), ISBLANK( Authorized_By_Initials__c ) ) ,NOT(ISPICKVAL(Status, &quot;Rejected&quot;)), ISPICKVAL(Email_Opt_In__c, &quot;Yes&quot;) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A CAN DIB Update Reimbursement Fields Complete Checkbox %28FR%29</fullName>
        <actions>
            <name>CAN_DIB_Update_Insurance_Info_FR</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CAN_DIB_Update_Insurance_Info_FR_FOR_ACTIVITY_HISTORY</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Lead_CAN_DIB_SetHiddenBTRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Related to use case: Existing lead via web - Update Insurance Info (FR)</description>
        <formula>AND  (  RecordType.DeveloperName = &quot;CAN_DiB_Lead&quot;,  ISPICKVAL(LeadSource,&quot;Web&quot;), Requested_Reimbursement_Verification_Boo__c = TRUE,  ispickval(Preferred_Language__c, &quot;French&quot;), HIDDEN_B_Send__c = FALSE, OR( ISBLANK(Primary_Plan_Member_First_Name_Text__c),  ISPICKVAL(Primary_Patient_Relation_to_Plan_Mem_Tex__c,&quot;&quot;),  ISBLANK(Primary_Plan_Member_Last_Name_Text__c),  ISBLANK( Primary_Health_Insurer_ID__c),  ISPICKVAL(Primary_Employment_Status_Text__c,&quot;&quot;),  ISNULL(Primary_Plan_Member_Birthdate__c),  ISBLANK(Primary_Employer_Text__c),  ISBLANK(Primary_Group_No_Text__c),  ISBLANK(Primary_Employer_Phone__c),  ISBLANK( Authorized_By_Initials__c ), ISNULL( Birthdate__c ), ISBLANK( Patient_Health_Card_Text__c )), NOT(ISPICKVAL(Status, &quot;Rejected&quot;)), ISPICKVAL(Email_Opt_In__c, &quot;Yes&quot;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A CAN DIB not equal to Product Interest Guardian %2B Sensors</fullName>
        <actions>
            <name>Sent_email_CAN_DIB_not_equal_to_product_interest_Guardian_Sensors</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>CAN DiB Lead,CAN DiB Lead (Conversion)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.DIB_Product_Interest__c</field>
            <operation>notEqual</operation>
            <value>Guardian + Sensors</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.DIB_Product_Interest__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>CAN DIB product interest not equal to Guardian + Sensors</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Copy Email to Account Email</fullName>
        <actions>
            <name>Lead_Copy_Email_to_Account_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Copy Email to Account_Email__c which is mapped to Account.account_Email__c to align with SAP process</description>
        <formula>AND (  AND(RecordType.DeveloperName &lt;&gt; &quot;ANZ_DIB_Lead&quot;), (RecordType.DeveloperName &lt;&gt; &quot;LEARNING&quot;),  OR(  ISNEW(),  ISCHANGED(Email))  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Set Existing Customer Checkbox</fullName>
        <actions>
            <name>Existing_Customer_is_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Ticks the Existing Customer checkbox if an EPNP type lead is selected.</description>
        <formula>AND($RecordType.DeveloperName = &quot;ANZ_DIB_Lead&quot;, OR(ISPICKVAL(Lead_Type__c, &quot;EPNP&quot;), ISPICKVAL(Lead_Type__c, &quot;BTG - EPNP&quot;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Set OOW Start Date</fullName>
        <actions>
            <name>Correct_Lead_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Corrects the lead start date where appropriate for OOW leads.</description>
        <formula>AND($RecordType.DeveloperName = &quot;ANZ_DIB_Lead&quot;, ANZ_DIB_OOW_Flag__c &gt; 90, ISPICKVAL(LeadSource, &quot;OOW&quot;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Set Rejection Date</fullName>
        <actions>
            <name>Set_Rejection_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the date value in the &quot;Rejection Date&quot; field once the Lead has been rejected.</description>
        <formula>AND($RecordType.DeveloperName = &quot;ANZ_DIB_Lead&quot;, ISPICKVAL(Status, &quot;Rejected&quot;), ISNULL(Rejection_Date__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Uncheck Existing Customer Checkbox</fullName>
        <actions>
            <name>Uncheck_Existing_Customer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Unticks the Existing Customer checkbox if an NPNP type lead is selected.</description>
        <formula>AND($RecordType.DeveloperName = &quot;ANZ_DIB_Lead&quot;, OR(ISPICKVAL(Lead_Type__c, &quot;NPNP&quot;), ISPICKVAL(Lead_Type__c, &quot;BTG - NPNP&quot;), ISPICKVAL(Lead_Type__c, &quot;NPNP - Competitor&quot;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MITG Connect%2Eme auto response</fullName>
        <actions>
            <name>Send_autoresponse</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Lead_Source_MITG__c</field>
            <operation>equals</operation>
            <value>Connect.me</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Type_MITG__c</field>
            <operation>equals</operation>
            <value>Inbound Email</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>MITG Lead</value>
        </criteriaItems>
        <description>Auto response email on new leads for MITG Connect.me</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>MITG Lead Address change</fullName>
        <actions>
            <name>Lead_City_copy</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lead_Postalcode_copy</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lead_Street_copy</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the standard lead address is changed for an MITG Lead, copy the values over to the custom address fields</description>
        <formula>AND(  RecordType.DeveloperName = &apos;MITG_Lead&apos;,  OR( ISNEW(), ISCHANGED( Street ), ISCHANGED(  City  ), ISCHANGED(  PostalCode  ) ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MITG Lead Assigned to Queue</fullName>
        <active>false</active>
        <formula>AND(NOT(ISBLANK( Owner:Queue.Id )), ISBLANK(Owner:User.Id),NOT (CONTAINS( Owner:Queue.QueueName , &apos;EMEA_MITG_All&apos;)), RecordType.Name = &apos;MITG Lead&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>lead_has_not_been_assigned_to_a_user_for_a_few_days</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>lead_has_not_been_assigned_to_a_user_for_6_days</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>MITG Lead new owner notification</fullName>
        <actions>
            <name>MITG_New_Lead_Assignment_Email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Email the new owner of the lead</description>
        <formula>AND(RecordType.Name =&apos;MITG Lead&apos;, OwnerId &lt;&gt; PRIORVALUE(OwnerId))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MITG Time to Close</fullName>
        <actions>
            <name>MITG_Time_to_Close_field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>MDT EMEA Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Converted</value>
        </criteriaItems>
        <description>Calculates the time it took to close a MITG Lead from the created date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MITG Time to Contact</fullName>
        <actions>
            <name>MITG_Time_to_Contact_field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>MDT EMEA Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>notEqual</operation>
            <value>Open</value>
        </criteriaItems>
        <description>Calculates the time it took to contact a MITG Lead from the created date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MITG Time to Contact catch</fullName>
        <actions>
            <name>MITG_Time_to_Contact_catch_field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND ((2 AND 3) OR (4 AND 5))</booleanFilter>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>MDT EMEA Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Qualified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Time_to_Contact__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Unqualified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Time_to_Contact__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Calculates the time it took to contact a MITG Lead from the created date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WF_ChangeOpenToUnqualified</fullName>
        <actions>
            <name>FU_SetStatusToUnqualified</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>CAN DiB Lead,CAN DiB Lead (Conversion)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>New,Open</value>
        </criteriaItems>
        <description>Workflow is used for DIB CAN.  When status is set to Open by default, this WF will change the status to the default CAN DIB STATUS Unqualified.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
