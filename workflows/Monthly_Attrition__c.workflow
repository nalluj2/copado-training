<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>MonthlyAttritionUnique</fullName>
        <field>MonthlyAttritionUnique__c</field>
        <formula>DIB_Department_ID__r.Name &amp;  Fiscal_Year_Month_Comb__c</formula>
        <name>MonthlyAttritionUnique</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>MonthlyAttritionUnique</fullName>
        <actions>
            <name>MonthlyAttritionUnique</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Monthly_Attrition__c.Department_Text__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
