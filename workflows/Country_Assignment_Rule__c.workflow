<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Assignment_Value_UpperCase</fullName>
        <description>Value in field Assignment Value to Uppercase</description>
        <field>Assignment_Value__c</field>
        <formula>UPPER( Assignment_Value__c )</formula>
        <name>Assignment Value UpperCase</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Assigned Value To Uppercase</fullName>
        <actions>
            <name>Assignment_Value_UpperCase</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Country_Assignment_Rule__c.Assignment_Value__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Update the Assigned Value field to the Uppercase value. This is for assignment of postal codes</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
