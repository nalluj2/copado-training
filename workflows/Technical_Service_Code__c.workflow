<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Fill_TS_Code_Name</fullName>
        <field>Name</field>
        <formula>Code__c &amp; &quot; - &quot; &amp; Description__c</formula>
        <name>Fill TS Code Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Technical Service Fill TS Code Name</fullName>
        <actions>
            <name>Fill_TS_Code_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Technical_Service_Code__c.Code__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Technical_Service_Code__c.Description__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Fills the TS Code Name with a concatenation of Code and Description</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
