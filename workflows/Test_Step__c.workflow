<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Test_Step_Update_Unique_Id</fullName>
        <description>Test Step: Update Unique Id</description>
        <field>Unique__c</field>
        <formula>Test__c &amp;  Step__c</formula>
        <name>Test Step: Update Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Test Step%3A Update Unique Id</fullName>
        <actions>
            <name>Test_Step_Update_Unique_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Test_Step__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Test Step: Update Unique Id</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
