<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_System_End_Fiscal_Month</fullName>
        <description>Fill out the corresponding 2 digit fiscal month code for the local campaign</description>
        <field>End_Fiscal_Month__c</field>
        <formula>CASE(End_Fiscal_Month_FullMonthName__c ,
&quot;May&quot;,&quot;01&quot;,
&quot;June&quot;,&quot;02&quot;,
&quot;July&quot;,&quot;03&quot;,
&quot;August&quot;,&quot;04&quot;,
&quot;September&quot;,&quot;05&quot;,
&quot;October&quot;,&quot;06&quot;,
&quot;November&quot;,&quot;07&quot;,
&quot;December&quot;,&quot;08&quot;,
&quot;January&quot;,&quot;09&quot;,
&quot;February&quot;,&quot;10&quot;,
&quot;March&quot;,&quot;11&quot;,
&quot;April&quot;,&quot;12&quot;,
&quot;&quot;
)</formula>
        <name>Update System End Fiscal Month</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_System_Start_Fiscal_Month</fullName>
        <description>Fill out the corresponding 2 digit fiscal month code for the local campaign</description>
        <field>Start_Fiscal_Month__c</field>
        <formula>CASE(Start_Fiscal_Month_FullMonthName__c ,
&quot;May&quot;,&quot;01&quot;,
&quot;June&quot;,&quot;02&quot;,
&quot;July&quot;,&quot;03&quot;,
&quot;August&quot;,&quot;04&quot;,
&quot;September&quot;,&quot;05&quot;,
&quot;October&quot;,&quot;06&quot;,
&quot;November&quot;,&quot;07&quot;,
&quot;December&quot;,&quot;08&quot;,
&quot;January&quot;,&quot;09&quot;,
&quot;February&quot;,&quot;10&quot;,
&quot;March&quot;,&quot;11&quot;,
&quot;April&quot;,&quot;12&quot;,
&quot;&quot;
)</formula>
        <name>Update System Start Fiscal Month</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Local Campaign Created</fullName>
        <actions>
            <name>Validate_Local_Campaign</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>DIB_Campaign__c.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Local Campaign Edited</fullName>
        <actions>
            <name>Update_System_End_Fiscal_Month</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_System_Start_Fiscal_Month</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>DIB_Campaign__c.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Used for DiB Insight project. 
Make sure selected fiscal start and end month fill out the corresponding fiscal month codes in the system fields.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Validate_Local_Campaign</fullName>
        <assignedTo>alban.de.courville@medtronic.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Validate Local Campaign</subject>
    </tasks>
</Workflow>
