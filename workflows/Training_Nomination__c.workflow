<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Training_Approved_By_PACE</fullName>
        <description>Training Approved By PACE</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Approver_L1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approver_L2__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>MITG_Email_Templates/Training_Approval_PACE</template>
    </alerts>
    <alerts>
        <fullName>Training_Approved_by_ApproverL1</fullName>
        <description>Training Approved by ApproverL1</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>MITG_Email_Templates/Training_Approval_L1</template>
    </alerts>
    <alerts>
        <fullName>Training_Approved_by_ApproverL2</fullName>
        <description>Training Approved by ApproverL2</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Approver_L1__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>MITG_Email_Templates/Training_Approval_L2</template>
    </alerts>
    <alerts>
        <fullName>Training_Cancelled_notification</fullName>
        <description>Training Cancelled notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>MITG_Email_Templates/Training_Cancelled</template>
    </alerts>
    <alerts>
        <fullName>Training_No_Show</fullName>
        <description>Training No-Show</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Approver_L1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approver_L2__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>PACE_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>MITG_Email_Templates/Training_no_show</template>
    </alerts>
    <alerts>
        <fullName>Training_Rejected_by_ApproverL1</fullName>
        <description>Training Rejected by ApproverL1</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>MITG_Email_Templates/Training_Rejected_L1</template>
    </alerts>
    <alerts>
        <fullName>Training_Rejected_by_ApproverL2</fullName>
        <description>Training Rejected by ApproverL2</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Approver_L1__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>MITG_Email_Templates/Training_Rejected_L2</template>
    </alerts>
    <alerts>
        <fullName>Training_Rejected_by_PACE</fullName>
        <description>Training Rejected by PACE</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Approver_L1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Approver_L2__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>MITG_Email_Templates/Training_Rejected_PACE</template>
    </alerts>
    <alerts>
        <fullName>Training_initial_follow_up_notification</fullName>
        <description>Training initial follow up notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>MITG_Email_Templates/Training_initial_follow_up</template>
    </alerts>
    <alerts>
        <fullName>Training_logistics_survey_notification</fullName>
        <description>Training logistics survey notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>MITG_Email_Templates/Training_Logistic_Survey</template>
    </alerts>
    <alerts>
        <fullName>Training_request_not_approved_no_seats_available</fullName>
        <description>Training request not approved - no seats available</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>MITG_Email_Templates/Training_no_seats_available</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approved_by_PACE</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Approved by PACE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ApproverL1_Approved</fullName>
        <field>Status__c</field>
        <literalValue>Evaluation</literalValue>
        <name>ApproverL1 Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Training_Rejected</fullName>
        <field>Status__c</field>
        <literalValue>Not Approved</literalValue>
        <name>Training Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Initial Follow Up Link Notification</fullName>
        <actions>
            <name>Training_initial_follow_up_notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send an email to the training nomination requestor (record creator) when the ‘Follow Up Survey’ link is available.</description>
        <formula>AND(NOT($Profile.Name = &quot;Devart Tool&quot;), ISCHANGED(Initial_Follow_Up_Survey__c),NOT(ISBLANK(  Initial_Follow_Up_Survey__c  )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Training Logistic Survey Notification</fullName>
        <actions>
            <name>Training_logistics_survey_notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send an email to the training nomination requestor (record creator) when the ‘Logistics Survey’ link is available.</description>
        <formula>AND(NOT($Profile.Name = &quot;Devart Tool&quot;), ISCHANGED(Logistics_Survey__c), NOT(ISBLANK( Logistics_Survey__c )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Training Nomination no seats available</fullName>
        <actions>
            <name>Training_request_not_approved_no_seats_available</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send an email to the training nomination requestor (record creator) when the training nomination status has been updated to ‘Not approved – no seats’ notifying them that their request will not be approved.</description>
        <formula>AND(NOT($Profile.Name = &quot;Devart Tool&quot;), ISPICKVAL(Status__c , &quot;Not Approved - No Seats&quot;), ISCHANGED(Status__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Training cancellation notification</fullName>
        <actions>
            <name>Training_Cancelled_notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Training_Nomination__c.Status__c</field>
            <operation>equals</operation>
            <value>Cancelled</value>
        </criteriaItems>
        <description>Send email notification to requester for course cancellation.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Training no show notification</fullName>
        <actions>
            <name>Training_No_Show</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Training_Nomination__c.Status__c</field>
            <operation>equals</operation>
            <value>No Show</value>
        </criteriaItems>
        <description>Send an email notification to the training nomination requestor (record creator) and all approvers notifying them that the HCP was a ‘no show.’</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
