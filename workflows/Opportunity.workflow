<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ANZ_MDTConnectDelayedSaleAlert</fullName>
        <description>ANZ DIB MDT Connect Delayed Sale Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Clinical</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>kaye.morehen@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ANZ_Email_Template/ANZ_MDTConnectSellAlert</template>
    </alerts>
    <alerts>
        <fullName>ANZ_MDTConnectEnrolmentAlert</fullName>
        <description>ANZ DIB MDT Connect Enrolment Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>kaye.morehen@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rupali.surti@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ANZ_Email_Template/ANZ_MDTConnectEnrolmentAlert</template>
    </alerts>
    <alerts>
        <fullName>ANZ_MDTConnectMissingPaperworkAlert</fullName>
        <description>ANZ DIB MDT Connect Missing Paperwork Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Clinical</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>kaye.morehen@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ANZ_Email_Template/ANZ_MDTConnectPaperworkAlert</template>
    </alerts>
    <alerts>
        <fullName>CAN_Customer_Award_Notification</fullName>
        <description>CAN Customer Award Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Admin Support</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Clinical</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Contract &amp; Pricing Mgmt</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Contract Analyst</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Marketing</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Other</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Pricing Operation</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Pricing Strategy</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Project Coordinator</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Reimbursement</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAN_Email_Templates/CAN_Customer_Award_Notification</template>
    </alerts>
    <alerts>
        <fullName>CAN_Customer_RFP_Document</fullName>
        <description>CAN Customer RFP Document</description>
        <protected>false</protected>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Admin Support</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Clinical</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Contract &amp; Pricing Mgmt</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Contract Analyst</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Marketing</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Other</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Pricing Operation</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Pricing Strategy</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Project Coordinator</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Reimbursement</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAN_Email_Templates/CAN_Customer_RFP_Document</template>
    </alerts>
    <alerts>
        <fullName>CAN_DIB_LMN_To_Additional_Insurer_EN</fullName>
        <description>CAN DIB LMN To AdditionalInsurer (EN)</description>
        <protected>false</protected>
        <recipients>
            <field>Person_Account_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_O3_EN</template>
    </alerts>
    <alerts>
        <fullName>CAN_DIB_LMN_To_Additional_Insurer_EN_FOR_ACTIVITY_HISTORY</fullName>
        <ccEmails>emailtosalesforce@20rrtqrv50l8afdcsqawcqachzy0df86p849c6zrnz6qojb02x.2-iypieag.wl.le.salesforce.com</ccEmails>
        <description>CAN DIB LMN To Additional Insurer (EN) FOR ACTIVITY HISTORY</description>
        <protected>false</protected>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_O3_EN</template>
    </alerts>
    <alerts>
        <fullName>CAN_DIB_LMN_To_Additional_Insurer_FR</fullName>
        <description>CAN DIB LMN To AdditionalInsurer (FR)</description>
        <protected>false</protected>
        <recipients>
            <field>Person_Account_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_O3_FR</template>
    </alerts>
    <alerts>
        <fullName>CAN_DIB_LMN_To_Additional_Insurer_FR_FOR_ACTIVITY_HISTORY</fullName>
        <ccEmails>emailtosalesforce@20rrtqrv50l8afdcsqawcqachzy0df86p849c6zrnz6qojb02x.2-iypieag.wl.le.salesforce.com</ccEmails>
        <description>CAN DIB LMN To Additional Insurer (FR) FOR ACTIVITY HISTORY</description>
        <protected>false</protected>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_O3_FR</template>
    </alerts>
    <alerts>
        <fullName>CAN_DIB_LMN_To_Primary_Insurer_EN</fullName>
        <description>CAN DIB LMN To Primary Insurer (EN)</description>
        <protected>false</protected>
        <recipients>
            <field>Person_Account_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_O1_EN</template>
    </alerts>
    <alerts>
        <fullName>CAN_DIB_LMN_To_Primary_Insurer_EN_FOR_ACTIVITY_HISTORY</fullName>
        <ccEmails>emailtosalesforce@20rrtqrv50l8afdcsqawcqachzy0df86p849c6zrnz6qojb02x.2-iypieag.wl.le.salesforce.com</ccEmails>
        <description>CAN DIB LMN To Primary Insurer (EN) FOR ACTIVITY HISTORY</description>
        <protected>false</protected>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_O1_EN</template>
    </alerts>
    <alerts>
        <fullName>CAN_DIB_LMN_To_Primary_Insurer_FR</fullName>
        <description>CAN DIB LMN To Primary Insurer (FR)</description>
        <protected>false</protected>
        <recipients>
            <field>Person_Account_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_O1_FR</template>
    </alerts>
    <alerts>
        <fullName>CAN_DIB_LMN_To_Primary_Insurer_FR_FOR_ACTIVITY_HISTORY</fullName>
        <ccEmails>emailtosalesforce@20rrtqrv50l8afdcsqawcqachzy0df86p849c6zrnz6qojb02x.2-iypieag.wl.le.salesforce.com</ccEmails>
        <description>CAN DIB LMN To Primary Insurer (FR) FOR ACTIVITY HISTORY</description>
        <protected>false</protected>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_O1_FR</template>
    </alerts>
    <alerts>
        <fullName>CAN_DIB_LMN_To_Secondary_Insurer_EN</fullName>
        <description>CAN DIB LMN To Secondary Insurer (EN)</description>
        <protected>false</protected>
        <recipients>
            <field>Person_Account_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_O2_EN</template>
    </alerts>
    <alerts>
        <fullName>CAN_DIB_LMN_To_Secondary_Insurer_EN_FOR_ACTIVITY_HISTORY</fullName>
        <ccEmails>emailtosalesforce@20rrtqrv50l8afdcsqawcqachzy0df86p849c6zrnz6qojb02x.2-iypieag.wl.le.salesforce.com</ccEmails>
        <description>CAN DIB LMN To Secondary Insurer (EN) FOR ACTIVITY HISTORY</description>
        <protected>false</protected>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_O2_EN</template>
    </alerts>
    <alerts>
        <fullName>CAN_DIB_LMN_To_Secondary_Insurer_FR</fullName>
        <description>CAN DIB LMN To Secondary Insurer (FR)</description>
        <protected>false</protected>
        <recipients>
            <field>Person_Account_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_O2_FR</template>
    </alerts>
    <alerts>
        <fullName>CAN_DIB_LMN_To_Secondary_Insurer_FR_FOR_ACTIVITY_HISTORY</fullName>
        <ccEmails>emailtosalesforce@20rrtqrv50l8afdcsqawcqachzy0df86p849c6zrnz6qojb02x.2-iypieag.wl.le.salesforce.com</ccEmails>
        <description>CAN DIB LMN To Secondary Insurer (FR) FOR ACTIVITY HISTORY</description>
        <protected>false</protected>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_O2_FR</template>
    </alerts>
    <alerts>
        <fullName>CAN_Team_Member_Assigned</fullName>
        <description>CAN Team Members Assigned</description>
        <protected>false</protected>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Admin Support</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Clinical</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Contract &amp; Pricing Mgmt</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Contract Analyst</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Marketing</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Other</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Pricing Operation</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Pricing Strategy</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Project Coordinator</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Reimbursement</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAN_Email_Templates/CAN_Team_Members_Assigned</template>
    </alerts>
    <alerts>
        <fullName>EMEA_Contract_Date_Notification</fullName>
        <description>EMEA Contract Date Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunities/Notify_five_days_before_Contract_date</template>
    </alerts>
    <alerts>
        <fullName>EMEA_Tender_Expiration_Date_Notification</fullName>
        <description>EMEA Tender Expiration Date Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunities/Notify_five_days_before_Tender_exp_date</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Close_Date_Approaching</fullName>
        <description>Opportunity Close Date Approaching</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunities/Close_Date_Approaching</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_approval_status_changed</fullName>
        <description>Opportunity approval status changed</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunities/Opportunity_Approval_Status</template>
    </alerts>
    <alerts>
        <fullName>Oppty_CAN_DIB_LMN_Follow_Up_Reminder</fullName>
        <ccEmails>info@racentre.ca</ccEmails>
        <description>Oppty: CAN DIB LMN Follow Up Reminder</description>
        <protected>false</protected>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_M_EN</template>
    </alerts>
    <alerts>
        <fullName>Oppty_CAN_DIB_LMN_Follow_Up_Reminder_FOR_ACTIVITY_HISTORY</fullName>
        <ccEmails>emailtosalesforce@20rrtqrv50l8afdcsqawcqachzy0df86p849c6zrnz6qojb02x.2-iypieag.wl.le.salesforce.com</ccEmails>
        <description>Oppty: CAN DIB LMN Follow Up Reminder FOR ACTIVITY HISTORY</description>
        <protected>false</protected>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_M_EN</template>
    </alerts>
    <alerts>
        <fullName>Oppty_CAN_DIB_LMN_To_Physician_Communication_EN</fullName>
        <description>Oppty: CAN DIB LMN To Physician Communication (EN)</description>
        <protected>false</protected>
        <recipients>
            <field>Opportunity_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_N_EN</template>
    </alerts>
    <alerts>
        <fullName>Oppty_CAN_DIB_LMN_To_Physician_Communication_EN_FOR_ACTIVITY_HISTORY</fullName>
        <ccEmails>emailtosalesforce@20rrtqrv50l8afdcsqawcqachzy0df86p849c6zrnz6qojb02x.2-iypieag.wl.le.salesforce.com</ccEmails>
        <description>Oppty: CAN DIB LMN To Physician Communication (EN) FOR ACTIVITY HISTORY</description>
        <protected>false</protected>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_N_EN</template>
    </alerts>
    <alerts>
        <fullName>Oppty_CAN_DIB_LMN_To_Physician_Communication_FR</fullName>
        <description>Oppty: CAN DIB LMN To Physician Communication  (FR)</description>
        <protected>false</protected>
        <recipients>
            <field>Opportunity_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_N_FR</template>
    </alerts>
    <alerts>
        <fullName>Oppty_CAN_DIB_LMN_To_Physician_Communication_FR_FOR_ACTIVITY_HISTORY</fullName>
        <ccEmails>emailtosalesforce@20rrtqrv50l8afdcsqawcqachzy0df86p849c6zrnz6qojb02x.2-iypieag.wl.le.salesforce.com</ccEmails>
        <description>Oppty: CAN DIB LMN To Physician Communication (FR) FOR ACTIVITY HISTORY</description>
        <protected>false</protected>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_N_FR</template>
    </alerts>
    <alerts>
        <fullName>Oppty_CAN_DIB_Processing_Insurance_Started_Communication_EN</fullName>
        <description>Oppty: CAN DIB Processing Insurance Started Communication (EN)</description>
        <protected>false</protected>
        <recipients>
            <field>Opportunity_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_H_EN</template>
    </alerts>
    <alerts>
        <fullName>Oppty_CAN_DIB_Processing_Insurance_Started_Communication_EN_FOR_ACTIVITY_HISTORY</fullName>
        <ccEmails>emailtosalesforce@20rrtqrv50l8afdcsqawcqachzy0df86p849c6zrnz6qojb02x.2-iypieag.wl.le.salesforce.com</ccEmails>
        <description>Oppty: CAN DIB Processing Insurance Started Communication (EN) FOR ACTIVITY HISTORY</description>
        <protected>false</protected>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_H_EN</template>
    </alerts>
    <alerts>
        <fullName>Oppty_CAN_DIB_Processing_Insurance_Started_Communication_FR</fullName>
        <description>Oppty: CAN DIB Processing Insurance Started Communication (FR)</description>
        <protected>false</protected>
        <recipients>
            <field>Opportunity_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_H_FR</template>
    </alerts>
    <alerts>
        <fullName>Oppty_CAN_DIB_Processing_Insurance_Started_Communication_FR_FOR_ACTIVITY_HISTORY</fullName>
        <ccEmails>emailtosalesforce@20rrtqrv50l8afdcsqawcqachzy0df86p849c6zrnz6qojb02x.2-iypieag.wl.le.salesforce.com</ccEmails>
        <description>Oppty: CAN DIB Processing Insurance Started Communication (FR) FOR ACTIVITY HISTORY</description>
        <protected>false</protected>
        <senderAddress>rs.candiabetesinsidesales@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CAN_DIB_IS_Automated/Template_H_FR</template>
    </alerts>
    <alerts>
        <fullName>Oppty_CVG_Project_Send_Notification</fullName>
        <description>Workflow: &apos;Oppty: CVG Project: Send Notification&apos;</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunities/Opportunity_CVG_Project_Notification_Template</template>
    </alerts>
    <alerts>
        <fullName>Oppty_CurrentExtendedTo30ReminderGermany</fullName>
        <ccEmails>rs.dusausschreibung@medtronic.com</ccEmails>
        <description>Oppty:CurrentExtendedTo30ReminderGermany</description>
        <protected>false</protected>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Clinical</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Contract &amp; Pricing Mgmt</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Management/Admin Support</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Marketing</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Other</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Project Coordinator</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Technical Support</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>gregor.raabe@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Business_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>rs.dusausschreibung@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Opportunities/Oppty_CurrentExtendedTo30ReminderGermany</template>
    </alerts>
    <alerts>
        <fullName>Oppty_CurrentExtendedTo60ReminderGermany</fullName>
        <ccEmails>rs.dusausschreibung@medtronic.com</ccEmails>
        <description>Oppty:CurrentExtendedTo60ReminderGermany</description>
        <protected>false</protected>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Clinical</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Contract &amp; Pricing Mgmt</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Management/Admin Support</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Marketing</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Other</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Project Coordinator</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Technical Support</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>gregor.raabe@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Business_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>rs.dusausschreibung@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Opportunities/Oppty_CurrentExtendedTo60ReminderGermany</template>
    </alerts>
    <alerts>
        <fullName>Oppty_MITG_Send_Notification</fullName>
        <description>Oppty: MITG Send Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunities/Opp_MITG_Notification_Template</template>
    </alerts>
    <alerts>
        <fullName>Oppty_Open_Opportunity_Reminder</fullName>
        <description>Oppty: Open Opportunity Reminder</description>
        <protected>false</protected>
        <recipients>
            <field>Opportunity_Owner_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunities/Open_Opportunity_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Oppty_TenderSubmissionReminderGermany</fullName>
        <ccEmails>rs.dusausschreibung@medtronic.com</ccEmails>
        <description>Oppty:TenderSubmissionReminderGermany</description>
        <protected>false</protected>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Clinical</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Contract &amp; Pricing Mgmt</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Management/Admin Support</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Marketing</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Other</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Project Coordinator</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Technical Support</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>gregor.raabe@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Business_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>rs.dusausschreibung@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Opportunities/Oppty_TenderSubmissionReminderGermany</template>
    </alerts>
    <alerts>
        <fullName>Oppty_Tender_Opportunity_Close_Date</fullName>
        <description>Oppty: Tender Opportunity Close Date</description>
        <protected>false</protected>
        <recipients>
            <field>Opportunity_Owner_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Admin Support</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Clinical</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Contract &amp; Pricing Mgmt</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Contract Analyst</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Marketing</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Other</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Pricing Operation</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Pricing Strategy</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Project Coordinator</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Reimbursement</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAN_Email_Templates/Tender_Opportunity_Close_Date</template>
    </alerts>
    <alerts>
        <fullName>Oppty_Tender_Opportunity_Close_Date_Reminder</fullName>
        <description>Oppty: Tender Opportunity Close Date Reminder</description>
        <protected>false</protected>
        <recipients>
            <field>Opportunity_Owner_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Admin Support</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Clinical</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Contract &amp; Pricing Mgmt</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Contract Analyst</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Marketing</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Other</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Pricing Operation</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Pricing Strategy</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Project Coordinator</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Reimbursement</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CAN_Email_Templates/Tender_Opportunity_Close_Date_Reminder</template>
    </alerts>
    <fieldUpdates>
        <fullName>ANZ_DIB_SetMDTConnectStartFlag</fullName>
        <field>ANZ_DIB_MDTConnectStartDateFlag__c</field>
        <literalValue>1</literalValue>
        <name>Set MDT Connect Start Date Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ANZ_SetMDTConnectPaperworkFlag</fullName>
        <field>ANZ_MDTConnectPaperworkFlag__c</field>
        <literalValue>1</literalValue>
        <name>Set MDT Connect Paperwork Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ANZ_SetMDTConnectSoldFlag</fullName>
        <field>ANZ_MDTConnectSoldFlag__c</field>
        <literalValue>1</literalValue>
        <name>Set MDT Connect Sold Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_Field_Check_True</fullName>
        <field>Approval_Field_Check__c</field>
        <literalValue>1</literalValue>
        <name>Approval Field Check True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approved_by_sales_manager</fullName>
        <field>Approval_Status_Picklist__c</field>
        <literalValue>Approved by sales manager</literalValue>
        <name>Approved by sales manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Default_Admission_Date</fullName>
        <description>Admission Date = Proposed Start Date</description>
        <field>DIB_AdmissionDate__c</field>
        <formula>DIB_ProposedStartDate__c</formula>
        <name>Default Admission Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IHS_FU_Set_Stage_to_3b_Won</fullName>
        <field>StageName</field>
        <literalValue>3b - Won (Pending Contract Signature)</literalValue>
        <name>IHS FU Set Stage to 3b - Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IHS_FU_Set_Stage_to_4a</fullName>
        <field>StageName</field>
        <literalValue>4a - Closed Won (Service Activation)</literalValue>
        <name>IHS FU Set Stage to 4a</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IHS_FU_Set_Stage_to_4b</fullName>
        <field>StageName</field>
        <literalValue>4b - Closed Won (Service Delivery)</literalValue>
        <name>IHS FU Set Stage to 4b</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IHS_FU_Set_Stage_to_4c</fullName>
        <field>StageName</field>
        <literalValue>4c - Closed Won (Contract delivered)</literalValue>
        <name>IHS FU Set Stage to 4c</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IHS_FU_Set_Stage_to_Identifying</fullName>
        <field>StageName</field>
        <literalValue>1 - Identifying</literalValue>
        <name>IHS FU Set Stage to Identifying</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IHS_FU_Set_Stage_to_Pre_contracting</fullName>
        <field>StageName</field>
        <literalValue>3a - Pre-Contracting</literalValue>
        <name>IHS FU Set Stage to Pre-contracting</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IHS_FU_Set_Stage_to_Proposing</fullName>
        <field>StageName</field>
        <literalValue>2 - Proposing</literalValue>
        <name>IHS FU Set Stage to Proposing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IHS_Keep_Probability_If_Above_Stage_1</fullName>
        <description>As the Stage update automatically set the Probability (%) based on IHS Sales Process, this Field update will make sure if the percentage was higher than next stage to be kept when approval is granted</description>
        <field>Probability</field>
        <formula>IF( 
PRIORVALUE( Probability ) &gt; 0.3, 
PRIORVALUE( Probability ), 
0.3 
)</formula>
        <name>IHS Keep Probability If Above Stage 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IHS_Keep_Probability_If_Above_Stage_2</fullName>
        <description>As the Stage update automatically set the Probability (%) based on IHS Sales Process, this Field update will make sure if the percentage was higher than next stage to be kept when approval is granted</description>
        <field>Probability</field>
        <formula>IF( 
PRIORVALUE( Probability ) &gt; 0.5, 
PRIORVALUE( Probability ), 
0.5 
)</formula>
        <name>IHS Keep Probability If Above Stage 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IHS_Keep_Probability_If_Above_Stage_3</fullName>
        <description>As the Stage update automatically set the Probability (%) based on IHS Sales Process, this Field update will make sure if the percentage was higher than next stage to be kept when approval is granted</description>
        <field>Probability</field>
        <formula>IF( 
PRIORVALUE( Probability ) &gt; 0.8, 
PRIORVALUE( Probability ), 
0.8 
)</formula>
        <name>IHS Keep Probability If Above Stage 3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Move_RT_EUR_IHS_Lead_to_IHS_Consulting</fullName>
        <field>RecordTypeId</field>
        <lookupValue>EMEA_IHS_Consulting_Opportunity</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Move RT EUR IHS Lead to IHS Consulting</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Move_RT_EUR_IHS_Lead_to_IHS_Managed_Serv</fullName>
        <field>RecordTypeId</field>
        <lookupValue>EMEA_IHS_Managed_Services</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Move RT EUR IHS Lead to IHS Managed Serv</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OpportunitySource</fullName>
        <field>Opportunity_Source_Text__c</field>
        <literalValue>Software Upgrade</literalValue>
        <name>Opportunity Source</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Oppty_CAN_DIB_Update_Follow_Up</fullName>
        <description>CAN DIB Update Follow Up On Non Responding Physician</description>
        <field>Task_Follow_up_Non_Responding_Physician__c</field>
        <literalValue>To Do</literalValue>
        <name>Oppty: CAN DIB Update Follow Up</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PreviousPumpMakeModel</fullName>
        <field>Previous_pump_make_model__c</field>
        <formula>&quot;770G&quot;</formula>
        <name>Previous Pump Make/Model</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PrimaryReasonLost</fullName>
        <field>Reason_Lost__c</field>
        <literalValue>Unknown reason</literalValue>
        <name>Primary Reason Lost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PumpGroup780G</fullName>
        <field>Pump_Group__c</field>
        <literalValue>780G</literalValue>
        <name>Pump Group = 780G</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejected_by_sales_manger</fullName>
        <field>Approval_Status_Picklist__c</field>
        <literalValue>Rejected by sales manager</literalValue>
        <name>Rejected by sales manger</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Ready_To_Ship_Trigger</fullName>
        <field>ANZ_DIB_Ready_To_Ship_Trigger__c</field>
        <literalValue>0</literalValue>
        <name>Reset Ready To Ship Trigger</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Status_SM</fullName>
        <field>Approval_Status_Picklist__c</field>
        <literalValue>Approved by sales manager</literalValue>
        <name>Set Approval Status to Approved by SM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Status_to_Approved_by_BM</fullName>
        <field>Approval_Status_Picklist__c</field>
        <literalValue>Approved by business manager</literalValue>
        <name>Set Approval Status to Approved by BM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Status_to_Approved_by_HQ</fullName>
        <field>Approval_Status_Picklist__c</field>
        <literalValue>Approved by HQ</literalValue>
        <name>Set Approval Status to Approved by HQ</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Status_to_Rejected_by_BM</fullName>
        <field>Approval_Status_Picklist__c</field>
        <literalValue>Rejected by business manager</literalValue>
        <name>Set Approval Status to Rejected by BM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Status_to_Rejected_by_HQ</fullName>
        <field>Approval_Status_Picklist__c</field>
        <literalValue>Rejected by HQ</literalValue>
        <name>Set Approval Status to Rejected by HQ</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Status_to_Rejected_by_SM</fullName>
        <field>Approval_Status_Picklist__c</field>
        <literalValue>Rejected by sales manager</literalValue>
        <name>Set Approval Status to Rejected by SM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Contract_End_date</fullName>
        <field>Contract_End_Date__c</field>
        <formula>BLANKVALUE( Contract_End_Date__c , ADDMONTHS( CloseDate ,12))</formula>
        <name>Set Contract End date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_DIB_ANZ_Oppty_Name</fullName>
        <field>Name</field>
        <formula>TEXT(LeadSource) &amp; &quot;  - &quot; &amp; Account.FirstName &amp; &quot; &quot; &amp; Account.LastName</formula>
        <name>Set DIB ANZ Oppty Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_MDTConnect_Enrolment_Flag</fullName>
        <field>ANZ_DIB_MDTConnectEnrolmentFlag__c</field>
        <literalValue>1</literalValue>
        <name>Set MDT Connect Enrolment Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_MDTConnect_Status_to_Enrolled</fullName>
        <field>ANZ_DIB_MDTConnectEnrolmentStatus__c</field>
        <literalValue>Enrolled</literalValue>
        <name>Set MDT Connect Status to Enrolled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_Type</fullName>
        <field>Type</field>
        <literalValue>Standard Sale</literalValue>
        <name>Set Oppty Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_PPT_Conversion_Date</fullName>
        <field>ANZ_DIB_Converted_to_PPT__c</field>
        <formula>Today()</formula>
        <name>Set PPT Conversion Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Ready_To_Ship_Trigger</fullName>
        <field>ANZ_DIB_Ready_To_Ship_Trigger__c</field>
        <literalValue>1</literalValue>
        <name>Set Ready To Ship Trigger</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StageClosedLost</fullName>
        <field>StageName</field>
        <literalValue>Closed Lost</literalValue>
        <name>Stage = Closed Lost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateRating</fullName>
        <field>Rating__c</field>
        <literalValue>Waiting for NGP</literalValue>
        <name>Update Rating</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Close_Date</fullName>
        <field>CloseDate</field>
        <formula>TODAY()</formula>
        <name>Update Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_to_Approval_Pending</fullName>
        <field>StageName</field>
        <literalValue>Approval Pending</literalValue>
        <name>Update Stage to Approval Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_to_Approved</fullName>
        <field>StageName</field>
        <literalValue>Approved</literalValue>
        <name>Update Stage to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_to_Closed_Lost</fullName>
        <field>StageName</field>
        <literalValue>Closed Lost</literalValue>
        <name>Update Stage to Closed Lost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_to_Closed_Won</fullName>
        <field>StageName</field>
        <literalValue>Closed Won</literalValue>
        <name>Update Stage to Closed Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_to_New</fullName>
        <field>StageName</field>
        <literalValue>New</literalValue>
        <name>Update Stage to New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_to_Sold</fullName>
        <field>StageName</field>
        <literalValue>Sold</literalValue>
        <name>Update to Sold</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Opportunity not closed yet</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>Stage 5: Implement Contract,Closed Won,Closed Lost,4 - Closed won,4a - Closed Won (Service Activation),4b - Closed Won (Service Delivery),4c - Closed Won (Contract delivered),5a - Closed Lost,5b - Closed Amended</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>CAN DIB Opportunity,EUR MITG Standard Opportunity,MEA MITG Standard Opportunity,EMEA IHS Lead,Small Tender,Business Critical Tender,Deal (VID – Non-tender),EMEA MITG Standard Opportunity,Spinal Business Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Profile_Name__c</field>
            <operation>notEqual</operation>
            <value>Devart Tool</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Managed_Marketing_Team__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Opportunity_Close_Date_Approaching</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>-7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Oppty %3A CAN DIB No response</fullName>
        <actions>
            <name>PrimaryReasonLost</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>StageClosedLost</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Stage_Duration_Days__c</field>
            <operation>equals</operation>
            <value>180</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>CAN DIB Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>FOTA Upgrade</value>
        </criteriaItems>
        <description>Close an opportunity when 180 days in New</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Oppty%3A ANZ DIB Default Admission Date</fullName>
        <actions>
            <name>Default_Admission_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a &quot;Proposed Start Date&quot; is selected, set the &quot;Admission Date&quot; to the same date value (if field is blank)</description>
        <formula>AND($RecordType.DeveloperName = &quot;ANZ_DIB_Opportunity&quot;,  ISBLANK(DIB_AdmissionDate__c),  NOT(ISBLANK(DIB_ProposedStartDate__c)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Oppty%3A CAN Customer Award Notification</fullName>
        <actions>
            <name>CAN_Customer_Award_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Customer_RFP_Award_Notification_Document__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>CAN Surgical Technologies - Tender,CAN CRDM - Tender,CAN Cardio Vasculair - Tender,CAN Diabetes - Tender,CAN Neuromodulation - Tender,CAN Spine &amp; Biologics - Tender</value>
        </criteriaItems>
        <description>Send email to Opportunity team (all roles) when “Customer Award Notification document” is populated to inform them of Customer Award.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Oppty%3A CAN Customer RFP Document</fullName>
        <actions>
            <name>CAN_Customer_RFP_Document</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CAN_Review_RFP_Requirements</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Customer_RFP_Document_Attached__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>CAN Surgical Technologies - Tender,CAN CRDM - Tender,CAN Cardio Vasculair - Tender,CAN Diabetes - Tender,CAN Neuromodulation - Tender,CAN Spine &amp; Biologics - Tender</value>
        </criteriaItems>
        <description>Send email to Opportunity Team (all roles) when field “Customer RFP document” is populated, informing them to read RFP</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Oppty%3A CAN DIB LMN Follow Up Reminder email</fullName>
        <active>true</active>
        <description>Time dependent workflow to send up to maximum 2 reminder emails to RAC agent to follow up with physician to get LMN back.</description>
        <formula>AND(RecordType.DeveloperName = &quot;CAN_DIB_OPPORTUNITY&quot;, NOT(ISBLANK(LMN_To_Physician_Date__c)),ISBLANK( LMN_Received_Date__c),  Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;British Columbia&quot;,  Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Quebec&quot;,  Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;New Brunswick&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Yukon Territory&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Ontario&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Nova Scotia&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Prince Edward Island&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Newfoundland &amp; Labr.&quot; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Oppty_CAN_DIB_Update_Follow_Up</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.LMN_To_Physician_Date__c</offsetFromField>
            <timeLength>17</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Oppty_CAN_DIB_LMN_Follow_Up_Reminder</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Oppty_CAN_DIB_LMN_Follow_Up_Reminder_FOR_ACTIVITY_HISTORY</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.LMN_To_Physician_Date__c</offsetFromField>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Oppty_CAN_DIB_LMN_Follow_Up_Reminder</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Oppty_CAN_DIB_LMN_Follow_Up_Reminder_FOR_ACTIVITY_HISTORY</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.LMN_To_Physician_Date__c</offsetFromField>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Oppty%3A CAN DIB LMN To Additional Insurer %28EN%29</fullName>
        <actions>
            <name>CAN_DIB_LMN_To_Additional_Insurer_EN</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CAN_DIB_LMN_To_Additional_Insurer_EN_FOR_ACTIVITY_HISTORY</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND (RecordType.DeveloperName = &quot;CAN_DIB_OPPORTUNITY&quot;, NOT(ISPICKVAL(Person_Account_Contact__r.Contact_Preferred_Language__c, &quot;French&quot;)), NOT(ISBLANK( LMN_to_Additional_Insurer__c )), ISCHANGED(LMN_to_Additional_Insurer__c), Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;British Columbia&quot; , Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;New Brunswick&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Yukon Territory&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Ontario&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Quebec&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Nova Scotia&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Prince Edward Island&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Newfoundland &amp; Labr.&quot;  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Oppty%3A CAN DIB LMN To Additional Insurer %28FR%29</fullName>
        <actions>
            <name>CAN_DIB_LMN_To_Additional_Insurer_FR</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CAN_DIB_LMN_To_Additional_Insurer_FR_FOR_ACTIVITY_HISTORY</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND (RecordType.DeveloperName = &quot;CAN_DIB_OPPORTUNITY&quot;, ISPICKVAL(Person_Account_Contact__r.Contact_Preferred_Language__c, &quot;French&quot;), NOT(ISBLANK( LMN_to_Additional_Insurer__c )), ISCHANGED(LMN_to_Additional_Insurer__c), Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;British Columbia&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Yukon Territory&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;New Brunswick&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Ontario&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Quebec&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Nova Scotia&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Prince Edward Island&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Newfoundland &amp; Labr.&quot;   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Oppty%3A CAN DIB LMN To Physician Communication %28EN%29</fullName>
        <actions>
            <name>Oppty_CAN_DIB_LMN_To_Physician_Communication_EN</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Oppty_CAN_DIB_LMN_To_Physician_Communication_EN_FOR_ACTIVITY_HISTORY</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>The patient will be informed when the LMN request has been sent to the physician</description>
        <formula>AND (RecordType.DeveloperName = &quot;CAN_DIB_OPPORTUNITY&quot;, NOT(ISPICKVAL(Person_Account_Contact__r.Contact_Preferred_Language__c, &quot;French&quot;)), NOT(ISBLANK(LMN_To_Physician_Date__c)),  Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;British Columbia&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Yukon Territory&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Ontario&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;New Brunswick&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Quebec&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Nova Scotia&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Prince Edward Island&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Newfoundland &amp; Labr.&quot;   )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Oppty%3A CAN DIB LMN To Physician Communication %28FR%29</fullName>
        <actions>
            <name>Oppty_CAN_DIB_LMN_To_Physician_Communication_FR</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Oppty_CAN_DIB_LMN_To_Physician_Communication_FR_FOR_ACTIVITY_HISTORY</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>The patient will be informed when the LMN request has been sent to the physician</description>
        <formula>AND (RecordType.DeveloperName = &quot;CAN_DIB_OPPORTUNITY&quot;, ISPICKVAL(Person_Account_Contact__r.Contact_Preferred_Language__c, &quot;French&quot;), NOT(ISBLANK(LMN_To_Physician_Date__c)), Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;British Columbia&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Yukon Territory&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Ontario&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;New Brunswick&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Quebec&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Nova Scotia&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Prince Edward Island&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Newfoundland &amp; Labr.&quot; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Oppty%3A CAN DIB LMN To Primary Insurer %28EN%29</fullName>
        <actions>
            <name>CAN_DIB_LMN_To_Primary_Insurer_EN</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CAN_DIB_LMN_To_Primary_Insurer_EN_FOR_ACTIVITY_HISTORY</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND (RecordType.DeveloperName = &quot;CAN_DIB_OPPORTUNITY&quot;, NOT(ISPICKVAL(Person_Account_Contact__r.Contact_Preferred_Language__c, &quot;French&quot;)), NOT(ISBLANK( LMN_to_Primary_Insurer__c )), ISCHANGED(LMN_to_Primary_Insurer__c), Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;British Columbia&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Yukon Territory&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Ontario&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;New Brunswick&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Quebec&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Nova Scotia&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Prince Edward Island&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Newfoundland &amp; Labr.&quot; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Oppty%3A CAN DIB LMN To Primary Insurer %28FR%29</fullName>
        <actions>
            <name>CAN_DIB_LMN_To_Primary_Insurer_FR</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CAN_DIB_LMN_To_Primary_Insurer_FR_FOR_ACTIVITY_HISTORY</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND (RecordType.DeveloperName = &quot;CAN_DIB_OPPORTUNITY&quot;, ISPICKVAL(Person_Account_Contact__r.Contact_Preferred_Language__c, &quot;French&quot;), NOT(ISBLANK( LMN_to_Primary_Insurer__c )), ISCHANGED(LMN_to_Primary_Insurer__c), Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;British Columbia&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Yukon Territory&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Ontario&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;New Brunswick&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Quebec&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Nova Scotia&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Prince Edward Island&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Newfoundland &amp; Labr.&quot; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Oppty%3A CAN DIB LMN To Secondary Insurer %28EN%29</fullName>
        <actions>
            <name>CAN_DIB_LMN_To_Secondary_Insurer_EN</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CAN_DIB_LMN_To_Secondary_Insurer_EN_FOR_ACTIVITY_HISTORY</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND (RecordType.DeveloperName = &quot;CAN_DIB_OPPORTUNITY&quot;, NOT(ISPICKVAL(Person_Account_Contact__r.Contact_Preferred_Language__c, &quot;French&quot;)), NOT(ISBLANK( LMN_to_Secondary_Insurer__c )), ISCHANGED(LMN_to_Secondary_Insurer__c ), Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;British Columbia&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Yukon Territory&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Ontario&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;New Brunswick&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Quebec&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Nova Scotia&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Prince Edward Island&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Newfoundland &amp; Labr.&quot;  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Oppty%3A CAN DIB LMN To Secondary Insurer %28FR%29</fullName>
        <actions>
            <name>CAN_DIB_LMN_To_Secondary_Insurer_FR</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CAN_DIB_LMN_To_Secondary_Insurer_FR_FOR_ACTIVITY_HISTORY</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND (RecordType.DeveloperName = &quot;CAN_DIB_OPPORTUNITY&quot;, ISPICKVAL(Person_Account_Contact__r.Contact_Preferred_Language__c, &quot;French&quot;), NOT(ISBLANK( LMN_to_Secondary_Insurer__c )), ISCHANGED(LMN_to_Secondary_Insurer__c ), Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;British Columbia&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Yukon Territory&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Ontario&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;New Brunswick&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Quebec&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Nova Scotia&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Prince Edward Island&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Newfoundland &amp; Labr.&quot;  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Oppty%3A CAN DIB New Fota Opp %26 Pumpgroup</fullName>
        <actions>
            <name>OpportunitySource</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PreviousPumpMakeModel</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PumpGroup780G</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>UpdateRating</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updating the Opportunity Source, Rating &amp; Previous pump make/model when a new CAN DIB Opportunity is created by SAP. Pump Group is set to 780G</description>
        <formula>AND( $Profile.Name = &quot;SAP Interface&quot;, RecordType.DeveloperName = &quot;CAN_DIB_OPPORTUNITY&quot;, ISPICKVAL(Department_Text__c, &quot;Pediatric&quot;), Person_Account_Contact__r.Age__c &gt; 7 )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Oppty%3A CAN DIB New Fota Oppty</fullName>
        <actions>
            <name>OpportunitySource</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PreviousPumpMakeModel</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>UpdateRating</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updating the Opportunity Source, Rating &amp; Previous pump make/model when a new CAN DIB Opportunity is created by SAP. Pump Group is left empty</description>
        <formula>AND( $Profile.Name = &quot;SAP Interface&quot;, RecordType.DeveloperName = &quot;CAN_DIB_OPPORTUNITY&quot;, Person_Account_Contact__r.Age__c &lt; 7 )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Oppty%3A CAN DIB Processing Insurance Started Communication %28EN%29</fullName>
        <actions>
            <name>Oppty_CAN_DIB_Processing_Insurance_Started_Communication_EN</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Oppty_CAN_DIB_Processing_Insurance_Started_Communication_EN_FOR_ACTIVITY_HISTORY</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When reimbursement investigation is requested and all primary insurance fields are filled out, the patient will be informed the insurance verification process will be started.</description>
        <formula>AND (RecordType.DeveloperName = &quot;CAN_DIB_OPPORTUNITY&quot;,  Reimbursement_Verification_Bool__c = TRUE, NOT(ISPICKVAL(Person_Account_Contact__r.Contact_Preferred_Language__c, &quot;French&quot;)), NOT(ISBLANK(Primary_Plan_Member_First_Name_Text__c)),  NOT(ISPICKVAL(Primary_Patient_Rel_to_Plan_Member_Text__c,&quot;&quot;)),  NOT(ISBLANK(Primary_Plan_Member_Last_Name_Text__c)),  NOT(ISBLANK(Primary_Health_Insurer__c)),  NOT(ISPICKVAL(Primary_Employment_Status_Text__c,&quot;&quot;)),  NOT(ISNULL(Primary_Plan_Member_Birthdate__c)),    NOT(ISBLANK(Primary_Employer_Text__c)),  NOT(ISBLANK(Primary_Group_No_Text__c)),    NOT(ISBLANK(Primary_Group_Detail_No_Text__c)), NOT(ISBLANK( Authorized_By_Initials_Text__c )),  Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;British Columbia&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Yukon Territory&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Ontario&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;New Brunswick&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Quebec&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Nova Scotia&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Prince Edward Island&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Newfoundland &amp; Labr.&quot;  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Oppty%3A CAN DIB Processing Insurance Started Communication %28FR%29</fullName>
        <actions>
            <name>Oppty_CAN_DIB_Processing_Insurance_Started_Communication_FR</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Oppty_CAN_DIB_Processing_Insurance_Started_Communication_FR_FOR_ACTIVITY_HISTORY</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When reimbursement investigation is requested and all primary insurance fields are filled out, the patient will be informed the insurance verification process will be started.</description>
        <formula>AND (RecordType.DeveloperName = &quot;CAN_DIB_OPPORTUNITY&quot;,  Reimbursement_Verification_Bool__c = TRUE, ISPICKVAL(Person_Account_Contact__r.Contact_Preferred_Language__c, &quot;French&quot;), NOT(ISBLANK(Primary_Plan_Member_First_Name_Text__c)),  NOT(ISPICKVAL(Primary_Patient_Rel_to_Plan_Member_Text__c,&quot;&quot;)),  NOT(ISBLANK(Primary_Plan_Member_Last_Name_Text__c)),  NOT(ISBLANK(Primary_Health_Insurer__c)),  NOT(ISPICKVAL(Primary_Employment_Status_Text__c,&quot;&quot;)),  NOT(ISNULL(Primary_Plan_Member_Birthdate__c)),    NOT(ISBLANK(Primary_Employer_Text__c)),  NOT(ISBLANK(Primary_Group_No_Text__c)),    NOT(ISBLANK(Primary_Group_Detail_No_Text__c)),  NOT(ISBLANK( Authorized_By_Initials_Text__c )), Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;British Columbia&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Yukon Territory&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Ontario&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;New Brunswick&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Quebec&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Nova Scotia&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Prince Edward Island&quot;, Physician_Account_ID__r.Account_Province__c &lt;&gt; &quot;Newfoundland &amp; Labr.&quot;  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Oppty%3A CAN Team Members Assigned</fullName>
        <actions>
            <name>CAN_Team_Member_Assigned</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Opportunity_team_assigned_incl_CS_PA__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>CAN Surgical Technologies - Tender,CAN CRDM - Tender,CAN Cardio Vasculair - Tender,CAN Diabetes - Tender,CAN Neuromodulation - Tender,CAN Spine &amp; Biologics - Tender</value>
        </criteriaItems>
        <description>Send email to Opportunity team (all roles) when “Opportunity team assigned incl CS &amp; PA” is flagged to inform them of assignment:
Triggered with every check of the check box (can uncheck and recheck).</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Oppty%3A CVG Project%3A Send Notification</fullName>
        <actions>
            <name>Oppty_CVG_Project_Send_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends an email when an Opportunity has been created in Mobile.</description>
        <formula>AND( RecordType.DeveloperName = &quot;CVG_Project&quot;, Created_in_Mobile__c = True, OR( ISNEW(),ischanged( OwnerId ) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Oppty%3A Converted To PPT</fullName>
        <actions>
            <name>Set_PPT_Conversion_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the date value in the &quot;Converted to PPT&quot; field once the Phase enters 
&quot;PPT&quot;.</description>
        <formula>AND($RecordType.DeveloperName = &quot;ANZ_DIB_Opportunity&quot;, ANZ_DIB_Phase__c = &quot;PPT&quot;, ISNULL(ANZ_DIB_Converted_to_PPT__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Oppty%3A MDT Connect CGM Follow-ups</fullName>
        <actions>
            <name>ANZ_DIB_SetMDTConnectStartFlag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ANZ_DIB_MC_CGM_PostCall_1</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>ANZ_DIB_MC_CGM_PostCall_2</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>ANZ_DIB_MC_CGM_PostCall_3</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>ANZ_DIB_MC_CGM_PostCall_4</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>ANZ_DIB_MC_CGM_PostCall_5</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>MC_CGM_Pre_Call_1</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>ANZ DIB Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.ANZ_DIB_MDTConnectEnrolmentStatus__c</field>
            <operation>equals</operation>
            <value>Enrolled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.DIB_StartDateConfirmed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.ANZ_DIB_Lead_Type__c</field>
            <operation>equals</operation>
            <value>CGM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.ANZ_DIB_MDTConnectStartDateFlag__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>When Start Date has been confirmed, all necessary CGM follow-up tasks should be created and assigned. Paperwork and Sale flags are also set via time-delay.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>ANZ_SetMDTConnectPaperworkFlag</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.DIB_ProposedStartDate__c</offsetFromField>
            <timeLength>-14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>ANZ_SetMDTConnectSoldFlag</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.DIB_ProposedStartDate__c</offsetFromField>
            <timeLength>-7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Oppty%3A MDT Connect Delayed Sale Alert</fullName>
        <actions>
            <name>ANZ_MDTConnectDelayedSaleAlert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Alert DTC when an MDT Connect start date is nearing and opportunity is not &quot;Sold&quot;.</description>
        <formula>AND(
	$RecordType.DeveloperName = &quot;ANZ_DIB_Opportunity&quot;, 
	ISPICKVAL(ANZ_DIB_MDTConnectEnrolmentStatus__c, &quot;Enrolled&quot;), 
	DIB_StartDateConfirmed__c = True, 
	ANZ_MDTConnectSoldFlag__c = True, 
	NOT(
		OR(
			ISPICKVAL(StageName, &quot;Sold&quot;), 
			ISPICKVAL(StageName, &quot;Returned&quot;), 
			ISPICKVAL(StageName, &quot;Cancel&quot;)
		)
	)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Oppty%3A MDT Connect EPNP Follow-ups</fullName>
        <actions>
            <name>ANZ_DIB_SetMDTConnectStartFlag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ANZ_DIB_MC_EPNP_PostCall_1</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>ANZ_DIB_MC_EPNP_PostCall_2</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>ANZ_DIB_MC_EPNP_PostCall_3</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>ANZ_DIB_MC_EPNP_PreCall_1</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>ANZ DIB Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.ANZ_DIB_MDTConnectEnrolmentStatus__c</field>
            <operation>equals</operation>
            <value>Enrolled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.DIB_StartDateConfirmed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.ANZ_DIB_Lead_Type__c</field>
            <operation>equals</operation>
            <value>BTG - EPNP,EPNP</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.ANZ_DIB_MDTConnectStartDateFlag__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>When Start Date has been confirmed, all necessary EPNP follow-up tasks should be created and assigned. Paperwork and Sale flags are also set via time-delay.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>ANZ_SetMDTConnectPaperworkFlag</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.DIB_ProposedStartDate__c</offsetFromField>
            <timeLength>-14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>ANZ_SetMDTConnectSoldFlag</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.DIB_ProposedStartDate__c</offsetFromField>
            <timeLength>-7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Oppty%3A MDT Connect Enrolment Confirmation</fullName>
        <actions>
            <name>ANZ_MDTConnectEnrolmentAlert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Set_MDTConnect_Enrolment_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_MDTConnect_Status_to_Enrolled</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>MDTConnect_Welcome_Call</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>When required Patient Consent(s) have been received, the patient should be marked as &quot;Enrolled&quot; in the MDT Connect program and a &quot;Welcome&quot; task should be created.</description>
        <formula>AND( 
$RecordType.DeveloperName = &quot;ANZ_DIB_Opportunity&quot;, 
ANZ_DIB_MDTConnectEnrolmentFlag__c = FALSE, 
NOT(ISBLANK(DIB_ProposedStartDate__c)), 
OR( 
AND( 
NOT(ISPICKVAL(ANZ_DIB_Who_Pays__c, &quot;Self/Charity&quot;)), 
NOT(ISBLANK(ANZ_DIB_PatientConsentReceived__c)), NOT(ISBLANK(ANZ_DIB_PatientPHIConsentReceived__c)) 
), 
AND( 
ISPICKVAL(ANZ_DIB_Who_Pays__c, &quot;Self/Charity&quot;), 
NOT(ISBLANK(ANZ_DIB_PatientConsentReceived__c)) 
) 
) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Oppty%3A MDT Connect NPNP Follow-ups</fullName>
        <actions>
            <name>ANZ_DIB_SetMDTConnectStartFlag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ANZ_DIB_MC_NPNP_PostCall_1</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>ANZ_DIB_MC_NPNP_PostCall_2</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>ANZ_DIB_MC_NPNP_PostCall_3</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>ANZ_DIB_MC_NPNP_PostCall_4</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>ANZ_DIB_MC_NPNP_PostCall_5</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>ANZ_DIB_MC_NPNP_PostCall_6</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>ANZ_DIB_MC_NPNP_PostCall_7</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>ANZ_DIB_MC_NPNP_PreCall_1</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>ANZ_DIB_MC_NPNP_PreCall_2</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>ANZ DIB Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.ANZ_DIB_MDTConnectEnrolmentStatus__c</field>
            <operation>equals</operation>
            <value>Enrolled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.DIB_StartDateConfirmed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.ANZ_DIB_Lead_Type__c</field>
            <operation>equals</operation>
            <value>NPNP,NPNP - Competitor,BTG - NPNP</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.ANZ_DIB_MDTConnectStartDateFlag__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>When Start Date has been confirmed, all necessary NPNP follow-up tasks should be created and assigned. Paperwork and Sale flags are also set via time-delay.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>ANZ_SetMDTConnectPaperworkFlag</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.DIB_ProposedStartDate__c</offsetFromField>
            <timeLength>-14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>ANZ_SetMDTConnectSoldFlag</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.DIB_ProposedStartDate__c</offsetFromField>
            <timeLength>-7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Oppty%3A MDT Connect Paperwork Alert</fullName>
        <actions>
            <name>ANZ_MDTConnectMissingPaperworkAlert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Alert DTC when an MDT Connect start date is nearing and not all required paperwork has been received.</description>
        <formula>AND( 
	$RecordType.DeveloperName = &quot;ANZ_DIB_Opportunity&quot;, 
	DIB_StartDateConfirmed__c = TRUE, 
	ISPICKVAL(ANZ_DIB_MDTConnectEnrolmentStatus__c, &quot;Enrolled&quot;), 
	ANZ_MDTConnectPaperworkFlag__c = True, 
	OR( 
		ISBLANK(ANZ_DIB_MDT_Order_Form_Received__c), 
		AND( 
			ISPICKVAL(ANZ_DIB_Who_Pays__c, &quot;Health Fund&quot;), 
			ISBLANK(ANZ_DIB_HF_Letter_Received__c)
		), 
		AND( 
			ISPICKVAL(ANZ_DIB_Who_Pays__c, &quot;Hospital&quot;), 
			ISBLANK(ANZ_DIB_Hospital_PO_Received__c), 
			ISBLANK(ANZ_DIB_Doctor_s_Letter_Received__c)
		), 
		AND( 
			ISPICKVAL(ANZ_DIB_Who_Pays__c, &quot;Self/Charity&quot;), 
			ISBLANK(ANZ_DIB_Credit_Card_Details_Received__c) 
		), 
		AND( 
			OR( 
				ISPICKVAL(ANZ_DIB_Who_Pays__c, &quot;Health Fund&quot;), 
				ISPICKVAL(ANZ_DIB_Who_Pays__c, &quot;Self/Charity&quot;) 
			), 
			NOT(ISPICKVAL(ANZ_DIB_Lead_Type__c, &quot;CGM&quot;)), 
			NOT(ISPICKVAL(ANZ_DIB_Lead_Type__c, &quot;Other&quot;)), 
			ISBLANK(ANZ_DIB_Doctor_s_Letter_Received__c) 
		) 
	) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Oppty%3A MITG Send Notification</fullName>
        <actions>
            <name>Oppty_MITG_Send_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Sends an email when an opportunity is created or when the owner is changed</description>
        <formula>AND(  (RecordType.DeveloperName = &apos;EUR_MITG_Standard_Opportunity&apos; ||   RecordType.DeveloperName = &apos;MEA_MITG_Standard_Opportunity&apos;), ( ISNEW()  ||  ISCHANGED( OwnerId) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Oppty%3A Move EMEA IHS Lead Oppty to IHS Consulting Oppty</fullName>
        <actions>
            <name>Move_RT_EUR_IHS_Lead_to_IHS_Consulting</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>EMEA IHS Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>0 - Tracking</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IHS_Convert_to__c</field>
            <operation>equals</operation>
            <value>IHS Consulting Opportunity</value>
        </criteriaItems>
        <description>Move EMEA IHS Lead Oppty to EUR IHS Consulting Oppty</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Oppty%3A Move EMEA IHS Lead Oppty to IHS Managed Serv Oppty</fullName>
        <actions>
            <name>Move_RT_EUR_IHS_Lead_to_IHS_Managed_Serv</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>EMEA IHS Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>0 - Tracking</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IHS_Convert_to__c</field>
            <operation>equals</operation>
            <value>IHS Managed Services</value>
        </criteriaItems>
        <description>Move EMEA IHS Lead Oppty to EUR IHS Managed Services Oppty</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Oppty%3A Notify five days before Contract date</fullName>
        <actions>
            <name>EMEA_Contract_Date_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>5 days before Contract Date Opportunity owner will receive notification email</description>
        <formula>AND(NOT($Profile.Name = &quot;Devart Tool&quot;), OR( RecordType.Name = &quot;MEA MITG Standard Opportunity&quot;, RecordType.Name = &quot;EMEA MITG Standard Opportunity&quot;), Contract_Start_Date__c  = TODAY() + 5)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Oppty%3A Notify five days before Tender exp date</fullName>
        <actions>
            <name>EMEA_Tender_Expiration_Date_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>5 days before Tender Expiration Date Opportunity owner will receive notification email</description>
        <formula>AND(NOT($Profile.Name = &quot;Devart Tool&quot;), OR(RecordType.Name = &quot;MEA MITG Standard Opportunity&quot;, RecordType.Name = &quot;EMEA MITG Standard Opportunity&quot;) ,Tender_Expiration_Date__c = TODAY() + 5)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Oppty%3A Open Opportunity Reminder</fullName>
        <active>true</active>
        <description>Send weekly reminder to opp owner and copy manager once the opportunity is expired untill the opportunity is changed</description>
        <formula>AND( 	DATEVALUE(LastModifiedDate) &lt; CloseDate 	,  IsClosed = FALSE 	, OR( 		$RecordType.DeveloperName = &quot;CAN_Surgical_Technologies_Capital_Opportunity&quot; 		, $RecordType.DeveloperName = &quot;CAN_Surgical_Technologies_Standard_Opportunity&quot; 		, $RecordType.DeveloperName = &quot;CAN_Surgical_Technologies_Tender&quot; 		, $RecordType.DeveloperName = &quot;Capital_Opportunity&quot; 		, $RecordType.DeveloperName = &quot;Service_Contract_Opportunity&quot; 		, $RecordType.DeveloperName = &quot;Non_Capital_Opportunity&quot; 		, $RecordType.DeveloperName = &quot;Surgical_Technologies_Tender&quot; 	) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Oppty_Open_Opportunity_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>21</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Oppty_Open_Opportunity_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Oppty_Open_Opportunity_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Oppty_Open_Opportunity_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>28</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Oppty_Open_Opportunity_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Oppty%3A Set ANZ DIB Close Date</fullName>
        <actions>
            <name>Update_Close_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a DIB ANZ opportunity is closed won/lost and the close date is in the past, the close date should be updated to today&apos;s date.</description>
        <formula>AND
(
$RecordType.DeveloperName = &quot;ANZ_DIB_Opportunity&quot;, 
NOT
(
OR
(
ISPICKVAL(PRIORVALUE(StageName), &quot;Sold&quot;), 
ISPICKVAL(PRIORVALUE(StageName), &quot;Cancel&quot;)
)
), 
OR
(
ISPICKVAL(StageName, &quot;Sold&quot;), 
ISPICKVAL(StageName, &quot;Cancel&quot;)
), 
CloseDate &lt;&gt; TODAY()
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Oppty%3A Set ANZ DIB Opportunity Name</fullName>
        <actions>
            <name>Set_DIB_ANZ_Oppty_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Name for ANZ DIB Opportunities</description>
        <formula>$RecordType.DeveloperName = &quot;ANZ_DIB_Opportunity&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Oppty%3A Set ANZ DIB Oppty Type</fullName>
        <actions>
            <name>Set_Oppty_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Automatically determines the ANZ DIB Opportunity &quot;Type&quot; value based on the Standard Sale Flag field.</description>
        <formula>$RecordType.DeveloperName = &quot;ANZ_DIB_Opportunity&quot; &amp;&amp; ANZ_DIB_Standard_Sale_Flag__c = True</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Oppty%3A Set Contract end date</fullName>
        <actions>
            <name>Set_Contract_End_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets contract end date 1 year later then close date (on creation only)</description>
        <formula>RecordType.DeveloperName = &apos;EMEA_MITG_Standard_Opportunity&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Oppty%3A Set Ready To Ship Trigger</fullName>
        <active>true</active>
        <description>Sets a date/time trigger value in the &quot;Ready To Ship Trigger&quot; field.</description>
        <formula>AND($RecordType.DeveloperName = &quot;ANZ_DIB_Opportunity&quot;, ISPICKVAL(StageName, &quot;Ready To Ship&quot;), NOT(ANZ_DIB_Ready_To_Ship_Trigger__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Ready_To_Ship_Trigger</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>12</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Oppty%3A Tender Opportunity Close Date</fullName>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>CAN CRDM - Tender,CAN Cardio Vascular - Tender,CAN Diabetes - Tender,CAN Neuromodulation - Tender,CAN Spine &amp; Biologics - Tender,CAN Surgical Technologies - Tender</value>
        </criteriaItems>
        <description>Oppty: Tender Opportunity Close Date email (1 year before the close date) for CAN Tender Opportunities</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Oppty_Tender_Opportunity_Close_Date</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>-365</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Oppty%3A Tender Opportunity Close Date Reminder</fullName>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>CAN CRDM - Tender,CAN Cardio Vascular - Tender,CAN Diabetes - Tender,CAN Neuromodulation - Tender,CAN Spine &amp; Biologics - Tender,CAN Surgical Technologies - Tender</value>
        </criteriaItems>
        <description>Oppty: Tender Opportunity Close Date reminder email (3 months before the close date) for CAN Tender Opportunities</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Oppty_Tender_Opportunity_Close_Date_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>-90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Oppty%3A Update to Sold</fullName>
        <actions>
            <name>Reset_Ready_To_Ship_Trigger</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_to_Sold</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the Stage from &quot;Ready To Ship&quot; to &quot;Sold&quot; for ANZ Diabetes Opportunities.</description>
        <formula>AND($RecordType.DeveloperName = &quot;ANZ_DIB_Opportunity&quot;, ISPICKVAL(StageName, &quot;Ready To Ship&quot;), ANZ_DIB_Ready_To_Ship_Trigger__c = True)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Oppty%3ACurrentExtendedTo30_60ReminderGermany</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Business Critical Tender</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Current_Extended_To_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Country__c</field>
            <operation>equals</operation>
            <value>GERMANY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Reminderstop__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Oppty_CurrentExtendedTo60ReminderGermany</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.Current_Extended_To_Date__c</offsetFromField>
            <timeLength>-60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Oppty_CurrentExtendedTo30ReminderGermany</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.Current_Extended_To_Date__c</offsetFromField>
            <timeLength>-30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Oppty%3ATenderSubmissionReminderGermany</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Business Critical Tender</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Country__c</field>
            <operation>equals</operation>
            <value>GERMANY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Tender_Submission_Date_Time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Reminder_Stop_Tender_Submission_Date__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Oppty_TenderSubmissionReminderGermany</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.Tender_Submission_Date_Time__c</offsetFromField>
            <timeLength>-240</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <tasks>
        <fullName>ANZ_DIB_MC_CGM_PostCall_1</fullName>
        <assignedTo>Clinical</assignedTo>
        <assignedToType>opportunityTeam</assignedToType>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.DIB_ProposedStartDate__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>MC: CGM Post-Call 1</subject>
    </tasks>
    <tasks>
        <fullName>ANZ_DIB_MC_CGM_PostCall_2</fullName>
        <assignedTo>Clinical</assignedTo>
        <assignedToType>opportunityTeam</assignedToType>
        <dueDateOffset>6</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.DIB_ProposedStartDate__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>MC: CGM Post-Call 2</subject>
    </tasks>
    <tasks>
        <fullName>ANZ_DIB_MC_CGM_PostCall_3</fullName>
        <assignedTo>Clinical</assignedTo>
        <assignedToType>opportunityTeam</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.DIB_ProposedStartDate__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>MC: CGM Post-Call 3</subject>
    </tasks>
    <tasks>
        <fullName>ANZ_DIB_MC_CGM_PostCall_4</fullName>
        <assignedTo>Clinical</assignedTo>
        <assignedToType>opportunityTeam</assignedToType>
        <dueDateOffset>30</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.DIB_ProposedStartDate__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>MC: CGM Post-Call 4</subject>
    </tasks>
    <tasks>
        <fullName>ANZ_DIB_MC_CGM_PostCall_5</fullName>
        <assignedTo>Clinical</assignedTo>
        <assignedToType>opportunityTeam</assignedToType>
        <dueDateOffset>90</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.DIB_ProposedStartDate__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>MC: CGM Post-Call 5</subject>
    </tasks>
    <tasks>
        <fullName>ANZ_DIB_MC_EPNP_PostCall_1</fullName>
        <assignedTo>Clinical</assignedTo>
        <assignedToType>opportunityTeam</assignedToType>
        <dueDateOffset>6</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.DIB_ProposedStartDate__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>MC: EPNP Post-Call 1</subject>
    </tasks>
    <tasks>
        <fullName>ANZ_DIB_MC_EPNP_PostCall_2</fullName>
        <assignedTo>Clinical</assignedTo>
        <assignedToType>opportunityTeam</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.DIB_ProposedStartDate__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>MC: EPNP Post-Call 2</subject>
    </tasks>
    <tasks>
        <fullName>ANZ_DIB_MC_EPNP_PostCall_3</fullName>
        <assignedTo>Clinical</assignedTo>
        <assignedToType>opportunityTeam</assignedToType>
        <dueDateOffset>30</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.DIB_ProposedStartDate__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>MC: EPNP Post-Call 3</subject>
    </tasks>
    <tasks>
        <fullName>ANZ_DIB_MC_EPNP_PreCall_1</fullName>
        <assignedTo>Clinical</assignedTo>
        <assignedToType>opportunityTeam</assignedToType>
        <dueDateOffset>-3</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.DIB_ProposedStartDate__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>MC: EPNP Pre-Call 1</subject>
    </tasks>
    <tasks>
        <fullName>ANZ_DIB_MC_NPNP_PostCall_1</fullName>
        <assignedTo>Clinical</assignedTo>
        <assignedToType>opportunityTeam</assignedToType>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.DIB_ProposedStartDate__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>MC: NPNP Post-Call 1</subject>
    </tasks>
    <tasks>
        <fullName>ANZ_DIB_MC_NPNP_PostCall_2</fullName>
        <assignedTo>Clinical</assignedTo>
        <assignedToType>opportunityTeam</assignedToType>
        <dueDateOffset>6</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.DIB_ProposedStartDate__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>MC: NPNP Post-Call 2</subject>
    </tasks>
    <tasks>
        <fullName>ANZ_DIB_MC_NPNP_PostCall_3</fullName>
        <assignedTo>Clinical</assignedTo>
        <assignedToType>opportunityTeam</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.DIB_ProposedStartDate__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>MC: NPNP Post-Call 3</subject>
    </tasks>
    <tasks>
        <fullName>ANZ_DIB_MC_NPNP_PostCall_4</fullName>
        <assignedTo>Clinical</assignedTo>
        <assignedToType>opportunityTeam</assignedToType>
        <dueDateOffset>30</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.DIB_ProposedStartDate__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>MC: NPNP Post-Call 4</subject>
    </tasks>
    <tasks>
        <fullName>ANZ_DIB_MC_NPNP_PostCall_5</fullName>
        <assignedTo>Clinical</assignedTo>
        <assignedToType>opportunityTeam</assignedToType>
        <dueDateOffset>45</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.DIB_ProposedStartDate__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>MC: NPNP Post-Call 5</subject>
    </tasks>
    <tasks>
        <fullName>ANZ_DIB_MC_NPNP_PostCall_6</fullName>
        <assignedTo>Clinical</assignedTo>
        <assignedToType>opportunityTeam</assignedToType>
        <dueDateOffset>90</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.DIB_ProposedStartDate__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>MC: NPNP Post-Call 6</subject>
    </tasks>
    <tasks>
        <fullName>ANZ_DIB_MC_NPNP_PostCall_7</fullName>
        <assignedTo>Clinical</assignedTo>
        <assignedToType>opportunityTeam</assignedToType>
        <dueDateOffset>180</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.DIB_ProposedStartDate__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>MC: NPNP Post-Call 7</subject>
    </tasks>
    <tasks>
        <fullName>ANZ_DIB_MC_NPNP_PreCall_1</fullName>
        <assignedTo>Clinical</assignedTo>
        <assignedToType>opportunityTeam</assignedToType>
        <dueDateOffset>-14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.DIB_ProposedStartDate__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>MC: NPNP Pre-Call 1</subject>
    </tasks>
    <tasks>
        <fullName>ANZ_DIB_MC_NPNP_PreCall_2</fullName>
        <assignedTo>Clinical</assignedTo>
        <assignedToType>opportunityTeam</assignedToType>
        <dueDateOffset>-3</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.DIB_ProposedStartDate__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>MC: NPNP Pre-Call 2</subject>
    </tasks>
    <tasks>
        <fullName>CAN_Review_RFP_Requirements</fullName>
        <assignedTo>Project Coordinator</assignedTo>
        <assignedToType>opportunityTeam</assignedToType>
        <description>Review RFP Requirements</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Review RFP Requirements</subject>
    </tasks>
    <tasks>
        <fullName>MC_CGM_Pre_Call_1</fullName>
        <assignedTo>Clinical</assignedTo>
        <assignedToType>opportunityTeam</assignedToType>
        <dueDateOffset>-3</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.DIB_ProposedStartDate__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>MC: CGM Pre-Call 1</subject>
    </tasks>
    <tasks>
        <fullName>MDTConnect_Welcome_Call</fullName>
        <assignedTo>cameron.mcgrath@medtronic.com.int</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>-28</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.DIB_ProposedStartDate__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>MDT Connect Welcome Call</subject>
    </tasks>
</Workflow>
