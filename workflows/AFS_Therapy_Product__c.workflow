<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>AFS_Therapy_Product_Unique_key</fullName>
        <field>Unique_key__c</field>
        <formula>Therapy__c + &apos;:&apos; + Product__c</formula>
        <name>AFS Therapy Product Unique key</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>AFS_Therapy_Product_Unique_key</fullName>
        <actions>
            <name>AFS_Therapy_Product_Unique_key</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( NOT(ISBLANK( Product__c )), NOT(ISBLANK( Therapy__c )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
