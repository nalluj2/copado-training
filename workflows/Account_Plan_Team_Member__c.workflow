<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Account_Plan_Team_inform_newly_added_user</fullName>
        <description>Account Plan Team: inform newly added user</description>
        <protected>false</protected>
        <recipients>
            <field>Email_WF__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Account_Plan_Team_new_team_member</template>
    </alerts>
    <fieldUpdates>
        <fullName>Account_Plan_Team_update_email_field</fullName>
        <field>Email_WF__c</field>
        <formula>Email__c</formula>
        <name>Account Plan Team: update email field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Account Plan Team%3A Send email to newly added account plan team member</fullName>
        <actions>
            <name>Account_Plan_Team_inform_newly_added_user</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Account_Plan_Team_update_email_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Plan_Team_Member__c.Email__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account_Plan_Team_Member__c.Account_Plan_Owner__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account_Plan_Team_Member__c.Auto_Created__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Send email to newly added account plan team member</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
