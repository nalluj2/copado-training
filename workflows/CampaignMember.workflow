<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Campaign_Member_enrolled_in_training</fullName>
        <description>Campaign Member enrolled in training</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Training_assignment_successful</template>
    </alerts>
    <alerts>
        <fullName>Customer_Training_Enrollment_Confirmation</fullName>
        <description>Customer Training Enrollment Confirmation</description>
        <protected>false</protected>
        <recipients>
            <field>Key_Customer_Contact__c</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/KCC_Training_assignment_successful</template>
    </alerts>
    <fieldUpdates>
        <fullName>CVent_MDT_Employee_false</fullName>
        <field>MDT_Employee__c</field>
        <literalValue>0</literalValue>
        <name>CVent - MDT Employee (false)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CVent_MDT_Employee_true</fullName>
        <field>MDT_Employee__c</field>
        <literalValue>1</literalValue>
        <name>CVent - MDT Employee (true)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Campaign Update staff member %28false%29</fullName>
        <actions>
            <name>CVent_MDT_Employee_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.MDT_Employee__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Campaign Update staff member %28true%29</fullName>
        <actions>
            <name>CVent_MDT_Employee_true</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.MDT_Employee__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>KCC Training assignment successful</fullName>
        <actions>
            <name>Customer_Training_Enrollment_Confirmation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CampaignMember.Status</field>
            <operation>equals</operation>
            <value>Enrolled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.RecordTypeId</field>
            <operation>equals</operation>
            <value>HCP Portal Onboarding</value>
        </criteriaItems>
        <description>This workflow is triggered when BPMS Integration user updates the campaign member status to Enrolled</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Training assignment successful</fullName>
        <actions>
            <name>Campaign_Member_enrolled_in_training</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CampaignMember.Status</field>
            <operation>equals</operation>
            <value>Enrolled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.RecordTypeId</field>
            <operation>equals</operation>
            <value>HCP Portal Onboarding</value>
        </criteriaItems>
        <description>This workflow is triggered when BPMS Integration user updates the campaign member status to Enrolled</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
