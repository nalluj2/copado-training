<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BI_Support_Request_Assigned</fullName>
        <description>BI Support Request Assigned</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Portal_Templates/BI_Support_Request_Assigned</template>
    </alerts>
    <alerts>
        <fullName>BI_Support_Request_Cancelled</fullName>
        <description>BI Support Request Cancelled</description>
        <protected>false</protected>
        <recipients>
            <recipient>Support_Portal_BI</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Portal_Templates/Support_Request_Cancelled</template>
    </alerts>
    <alerts>
        <fullName>BI_Support_Request_Completed</fullName>
        <description>BI Support Request Completed</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Portal_Templates/BI_Support_Request_Completed</template>
    </alerts>
    <alerts>
        <fullName>BI_Support_Request_Created</fullName>
        <description>BI Support Request Created</description>
        <protected>false</protected>
        <recipients>
            <recipient>Support_Portal_BI</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Portal_Templates/BI_Support_Request_Created</template>
    </alerts>
    <alerts>
        <fullName>BOT_Support_Request_Assigned</fullName>
        <description>BOT Support Request Assigned</description>
        <protected>false</protected>
        <recipients>
            <recipient>Support_Portal_Cvent</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Portal_Templates/BOT_Support_Request_Assigned</template>
    </alerts>
    <alerts>
        <fullName>BOT_Support_Request_Completed</fullName>
        <description>BOT Support Request Completed</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Portal_Templates/BOT_Support_Request_Completed</template>
    </alerts>
    <alerts>
        <fullName>BOT_Support_Request_Created</fullName>
        <description>BOT Support Request Created</description>
        <protected>false</protected>
        <recipients>
            <recipient>Support_Portal_Cvent</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Portal_Templates/BOT_Support_Request_Created</template>
    </alerts>
    <alerts>
        <fullName>Bug_Request_created</fullName>
        <description>Bug Request created</description>
        <protected>false</protected>
        <recipients>
            <recipient>Support_Portal_Admins</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Portal_Templates/Bug_Request_Created</template>
    </alerts>
    <alerts>
        <fullName>CVent_Support_Request_Assigned</fullName>
        <description>CVent Support Request Assigned</description>
        <protected>false</protected>
        <recipients>
            <recipient>Support_Portal_Cvent</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Portal_Templates/CVent_Support_Request_Assigned</template>
    </alerts>
    <alerts>
        <fullName>CVent_Support_Request_Completed</fullName>
        <description>CVent Support Request Completed</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Portal_Templates/CVent_Support_Request_Completed</template>
    </alerts>
    <alerts>
        <fullName>CVent_Support_Request_Created</fullName>
        <description>CVent Support Request Created</description>
        <protected>false</protected>
        <recipients>
            <recipient>Support_Portal_Cvent</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Portal_Templates/CVent_Support_Request_Created</template>
    </alerts>
    <alerts>
        <fullName>Change_Request_created</fullName>
        <description>Change Request created</description>
        <protected>false</protected>
        <recipients>
            <recipient>Support_Portal_Admins</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Portal_Templates/Change_Request_Created</template>
    </alerts>
    <alerts>
        <fullName>Change_User_Request_created</fullName>
        <description>Notify the Admin when a new Change User Request is created</description>
        <protected>false</protected>
        <recipients>
            <recipient>Support_Portal_Admins</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Portal_Templates/Change_User_Support_Request_Created</template>
    </alerts>
    <alerts>
        <fullName>Crossworld_new_ticket</fullName>
        <description>Inform Crossworld Team about new ticket</description>
        <protected>false</protected>
        <recipients>
            <recipient>Support_Portal_Crossworld</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Portal_Templates/XW_Support_Request_Created</template>
    </alerts>
    <alerts>
        <fullName>Crossworld_ticket_assigned</fullName>
        <description>Inform requestor when his ticket has been assigned</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Portal_Templates/XW_Support_Request_Assigned</template>
    </alerts>
    <alerts>
        <fullName>Crossworld_ticket_completed</fullName>
        <description>Inform requestor when his ticket has been completed</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Portal_Templates/XW_Support_Request_Completed</template>
    </alerts>
    <alerts>
        <fullName>Data_Load_Request_created</fullName>
        <description>Data Load Request created</description>
        <protected>false</protected>
        <recipients>
            <recipient>Support_Portal_Admins</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Portal_Templates/Data_Load_Support_Request_Created</template>
    </alerts>
    <alerts>
        <fullName>Deactivate_User_Request_created</fullName>
        <description>Deactivate User Request created</description>
        <protected>false</protected>
        <recipients>
            <recipient>Support_Portal_Admins</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Portal_Templates/Deactivate_User_Support_Request_Created</template>
    </alerts>
    <alerts>
        <fullName>Enable_App_Request_Created</fullName>
        <description>Enable App Request Created</description>
        <protected>false</protected>
        <recipients>
            <recipient>Support_Portal_Admins</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Portal_Templates/Enable_App_Support_Request_Created</template>
    </alerts>
    <alerts>
        <fullName>New_Create_User_Request_information</fullName>
        <description>New Create User Request information</description>
        <protected>false</protected>
        <recipients>
            <recipient>Support_Portal_Admins</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Portal_Templates/Create_User_Support_Request_Created</template>
    </alerts>
    <alerts>
        <fullName>Reactivate_User_Request_created</fullName>
        <description>Reactivate User Request created</description>
        <protected>false</protected>
        <recipients>
            <recipient>Support_Portal_Admins</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Portal_Templates/Reactivate_User_Support_Request_Created</template>
    </alerts>
    <alerts>
        <fullName>Reset_Password_Request_created</fullName>
        <description>Reset Password Request created</description>
        <protected>false</protected>
        <recipients>
            <recipient>Support_Portal_Admins</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Portal_Templates/Reset_Password_Support_Request_Created</template>
    </alerts>
    <alerts>
        <fullName>Service_Request_created</fullName>
        <description>Service Request created</description>
        <protected>false</protected>
        <recipients>
            <recipient>Support_Portal_Admins</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Portal_Templates/Service_Request_Created</template>
    </alerts>
    <alerts>
        <fullName>SupportRequest_Approve_Reject_email</fullName>
        <description>Support Request Approve/Reject email</description>
        <protected>false</protected>
        <recipients>
            <recipient>Support_Portal_Admins</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Portal_Templates/SupportRequest_Approve_Reject</template>
    </alerts>
    <alerts>
        <fullName>Support_Request_Assigned</fullName>
        <description>Notify the Requestor when his ticket is assigned</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Portal_Templates/Support_Request_Assigned</template>
    </alerts>
    <alerts>
        <fullName>Support_Request_Cancelled</fullName>
        <description>Support Request Cancelled</description>
        <protected>false</protected>
        <recipients>
            <recipient>andre.bormans@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>leen.severi@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Portal_Templates/Support_Request_Cancelled</template>
    </alerts>
    <alerts>
        <fullName>Support_Request_Rejected_Completed</fullName>
        <description>Support Request Rejected/Completed</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Portal_Templates/Support_Request_Completed</template>
    </alerts>
    <rules>
        <fullName>BI Support Request Assigned</fullName>
        <actions>
            <name>BI_Support_Request_Assigned</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Inform the requestor when the BI ticket has been assigned to a User responsible for the ticket</description>
        <formula>AND(   ISCHANGED(Assignee__c)   , NOT(ISBLANK(Assignee__c))   ,  ISBLANK(PRIORVALUE( Assignee__c ))   , ISPICKVAL(Application__c,&apos;BI&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI Support Request Cancelled</fullName>
        <actions>
            <name>BI_Support_Request_Cancelled</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(ISPICKVAL(Application__c, &apos;BI&apos;), ISCHANGED(Status__c),  ISPICKVAL(Status__c, &apos;Cancelled&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI Support Request Completed</fullName>
        <actions>
            <name>BI_Support_Request_Completed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Inform Requestor when his BI Requests are Completed, Accepted or Rejected</description>
        <formula>AND(ISPICKVAL( Application__c , &apos;BI&apos;), ISCHANGED( Status__c ), OR( ISPICKVAL( Status__c , &apos;Completed&apos;), ISPICKVAL( Status__c , &apos;Rejected&apos;), ISPICKVAL( Status__c , &apos;Accepted&apos;) ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI Support Request Created</fullName>
        <actions>
            <name>BI_Support_Request_Created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Create_User_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Create_User_Request__c.Application__c</field>
            <operation>equals</operation>
            <value>BI</value>
        </criteriaItems>
        <description>Notify BI Admins about a new BI ticket created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BOT Support Request Assigned</fullName>
        <actions>
            <name>BOT_Support_Request_Assigned</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Inform the requestor when the BOT ticket has been assigned to a User responsible for the ticket</description>
        <formula>AND(   ISCHANGED(Assignee__c)   , NOT(ISBLANK(Assignee__c))   ,  ISBLANK(PRIORVALUE( Assignee__c ))   , ISPICKVAL(Application__c,&apos;BOT&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BOT Support Request Completed</fullName>
        <actions>
            <name>BOT_Support_Request_Completed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Inform Requestor when his BOT Requests are Completed, Accepted or Rejected</description>
        <formula>AND(ISPICKVAL( Application__c , &apos;BOT&apos;), ISCHANGED( Status__c ), OR( ISPICKVAL( Status__c , &apos;Completed&apos;), ISPICKVAL( Status__c , &apos;Rejected&apos;), ISPICKVAL( Status__c , &apos;Accepted&apos;) ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BOT Support Request Created</fullName>
        <actions>
            <name>BOT_Support_Request_Created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Create_User_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Create_User_Request__c.Application__c</field>
            <operation>equals</operation>
            <value>BOT</value>
        </criteriaItems>
        <description>Notify Admins about a new CVent ticket created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Bug_Request_Created</fullName>
        <actions>
            <name>Bug_Request_created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Create_User_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Create_User_Request__c.Request_Type__c</field>
            <operation>equals</operation>
            <value>Bug</value>
        </criteriaItems>
        <criteriaItems>
            <field>Create_User_Request__c.Application__c</field>
            <operation>equals</operation>
            <value>Salesforce.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CVent Support Request Assigned</fullName>
        <actions>
            <name>CVent_Support_Request_Assigned</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Inform the requestor when the Cvent ticket has been assigned to a User responsible for the ticket</description>
        <formula>AND(   ISCHANGED(Assignee__c)   , NOT(ISBLANK(Assignee__c))   ,  ISBLANK(PRIORVALUE( Assignee__c ))   , ISPICKVAL(Application__c,&apos;CVent&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CVent Support Request Completed</fullName>
        <actions>
            <name>CVent_Support_Request_Completed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Inform Requestor when his CVent Requests are Completed, Accepted or Rejected</description>
        <formula>AND(ISPICKVAL( Application__c , &apos;CVent&apos;), ISCHANGED( Status__c ), OR( ISPICKVAL( Status__c , &apos;Completed&apos;), ISPICKVAL( Status__c , &apos;Rejected&apos;), ISPICKVAL( Status__c , &apos;Accepted&apos;) ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CVent Support Request Created</fullName>
        <actions>
            <name>CVent_Support_Request_Created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Create_User_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Create_User_Request__c.Application__c</field>
            <operation>equals</operation>
            <value>CVent</value>
        </criteriaItems>
        <description>Notify Admins about a new CVent ticket created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Change_Request_Created</fullName>
        <actions>
            <name>Change_Request_created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Create_User_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Create_User_Request__c.Request_Type__c</field>
            <operation>equals</operation>
            <value>Change Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Create_User_Request__c.Application__c</field>
            <operation>equals</operation>
            <value>Salesforce.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Change_User_Setup_Request_Created</fullName>
        <actions>
            <name>Change_User_Request_created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Create_User_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Create_User_Request__c.Request_Type__c</field>
            <operation>equals</operation>
            <value>Change User Setup</value>
        </criteriaItems>
        <criteriaItems>
            <field>Create_User_Request__c.Application__c</field>
            <operation>equals</operation>
            <value>Salesforce.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Create_User_Support_Request_Created</fullName>
        <actions>
            <name>New_Create_User_Request_information</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Create_User_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Create_User_Request__c.Request_Type__c</field>
            <operation>equals</operation>
            <value>Create User</value>
        </criteriaItems>
        <criteriaItems>
            <field>Create_User_Request__c.Application__c</field>
            <operation>equals</operation>
            <value>Salesforce.com</value>
        </criteriaItems>
        <description>Inform the Admin that a new Create User Support Request has been created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Crossworld Support Request Assigned</fullName>
        <actions>
            <name>Crossworld_ticket_assigned</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Inform the requestor when the Crossworld ticket has been assigned to a User responsible for the ticket</description>
        <formula>AND(   ISCHANGED(Assignee__c)   , NOT(ISBLANK(Assignee__c))   ,  ISBLANK(PRIORVALUE( Assignee__c ))   , ISPICKVAL(Application__c,&apos;Crossworld&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Crossworld Support Request Completed</fullName>
        <actions>
            <name>Crossworld_ticket_completed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Inform Requestor when his Crossworld Requests are Completed, Accepted or Rejected</description>
        <formula>AND(ISPICKVAL( Application__c , &apos;Crossworld&apos;), ISCHANGED( Status__c ), OR( ISPICKVAL( Status__c , &apos;Completed&apos;), ISPICKVAL( Status__c , &apos;Rejected&apos;), ISPICKVAL( Status__c , &apos;Accepted&apos;) ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Crossworld Support Request Created</fullName>
        <actions>
            <name>Crossworld_new_ticket</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Create_User_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Create_User_Request__c.Application__c</field>
            <operation>equals</operation>
            <value>Crossworld</value>
        </criteriaItems>
        <description>Notify Crossworld Team about a new ticket created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Data_Load_Request_Created</fullName>
        <actions>
            <name>Data_Load_Request_created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Create_User_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Create_User_Request__c.Request_Type__c</field>
            <operation>equals</operation>
            <value>Data Load</value>
        </criteriaItems>
        <criteriaItems>
            <field>Create_User_Request__c.Application__c</field>
            <operation>equals</operation>
            <value>Salesforce.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Deactivate_User_Request_Created</fullName>
        <actions>
            <name>Deactivate_User_Request_created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Create_User_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Create_User_Request__c.Request_Type__c</field>
            <operation>equals</operation>
            <value>De-activate User</value>
        </criteriaItems>
        <criteriaItems>
            <field>Create_User_Request__c.Application__c</field>
            <operation>equals</operation>
            <value>Salesforce.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Enable_App_Request_Created</fullName>
        <actions>
            <name>Enable_App_Request_Created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Create_User_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Create_User_Request__c.Request_Type__c</field>
            <operation>equals</operation>
            <value>Enable Mobile App</value>
        </criteriaItems>
        <criteriaItems>
            <field>Create_User_Request__c.Application__c</field>
            <operation>equals</operation>
            <value>Salesforce.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Reactivate_User_Request_Created</fullName>
        <actions>
            <name>Reactivate_User_Request_created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Create_User_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Create_User_Request__c.Request_Type__c</field>
            <operation>equals</operation>
            <value>Re-activate User</value>
        </criteriaItems>
        <criteriaItems>
            <field>Create_User_Request__c.Application__c</field>
            <operation>equals</operation>
            <value>Salesforce.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Reset_Password_Request_Created</fullName>
        <actions>
            <name>Reset_Password_Request_created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Create_User_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Create_User_Request__c.Request_Type__c</field>
            <operation>equals</operation>
            <value>Reset Password</value>
        </criteriaItems>
        <criteriaItems>
            <field>Create_User_Request__c.Application__c</field>
            <operation>equals</operation>
            <value>Salesforce.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Service_Request_Created</fullName>
        <actions>
            <name>Service_Request_created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Create_User_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Create_User_Request__c.Request_Type__c</field>
            <operation>equals</operation>
            <value>Generic Service</value>
        </criteriaItems>
        <criteriaItems>
            <field>Create_User_Request__c.Application__c</field>
            <operation>equals</operation>
            <value>Salesforce.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Support_Request_Approved_Rejected</fullName>
        <actions>
            <name>SupportRequest_Approve_Reject_email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Inform Admin that a Support Request has been approved/rejected by the approver</description>
        <formula>AND( ISPICKVAL( Application__c , &apos;Salesforce.com&apos;) , OR(ISPICKVAL(Request_Type__c, &apos;Create User&apos;), ISPICKVAL(Request_Type__c, &apos;Data Load&apos;)),ISCHANGED(Status__c),ISPICKVAL(PRIORVALUE(Status__c), &quot;Submitted for Approval&quot;),  OR(ISPICKVAL(Status__c, &apos;Rejected&apos;), ISPICKVAL(Status__c, &apos;Approved&apos;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Support_Request_Cancelled</fullName>
        <actions>
            <name>Support_Request_Cancelled</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Inform Admin when a requestor cancels a Request</description>
        <formula>AND( ISPICKVAL( Application__c , &apos;Salesforce.com&apos;), ISCHANGED( Status__c ) ,  ISPICKVAL(Status__c, &apos;Cancelled&apos;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Support_Request_Rejected_Completed_Accepted</fullName>
        <actions>
            <name>Support_Request_Rejected_Completed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Inform Requestor when his Requests are Completed, Accepted or Rejected</description>
        <formula>AND(ISPICKVAL( Application__c , &apos;Salesforce.com&apos;), ISCHANGED( Status__c ), OR( ISPICKVAL( Status__c , &apos;Completed&apos;), ISPICKVAL( Status__c , &apos;Rejected&apos;), ISPICKVAL( Status__c , &apos;Accepted&apos;) ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Ticket Assigned</fullName>
        <actions>
            <name>Support_Request_Assigned</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Inform the requestor when the ticket has been assigned to a User responsible for the ticket</description>
        <formula>AND(ISCHANGED(Assignee__c) , NOT(ISBLANK(Assignee__c)),  ISBLANK(PRIORVALUE( Assignee__c )), ISPICKVAL(Application__c, &apos;Salesforce.com&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
