<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Unique_Field</fullName>
        <field>UniqueField__c</field>
        <formula>BLANKVALUE(Business_Unit__c, &apos;&apos;) +  sObject_Name__c  +  Field_Name__c</formula>
        <name>Update Unique Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Create Unique Field</fullName>
        <actions>
            <name>Update_Unique_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
