<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Box_Builder_Admins_Product_Country_availability_request</fullName>
        <ccEmails>mercedes.morera.escude@medtronic.com</ccEmails>
        <ccEmails>rs.proceduralsolutionsemea@medtronic.com</ccEmails>
        <description>Alert Box Builder Admins of a new Product Country availability request</description>
        <protected>false</protected>
        <recipients>
            <recipient>Box_Builder_Admins</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Box_Builder/Product_Country_availability_request</template>
    </alerts>
    <alerts>
        <fullName>Product_Country_availability_requestor</fullName>
        <description>Product Country availability request (Requestor)</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Box_Builder/Product_Country_availability_requestor</template>
    </alerts>
    <rules>
        <fullName>Product%3A Country availability request</fullName>
        <actions>
            <name>Box_Builder_Admins_Product_Country_availability_request</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Product_Country_availability_requestor</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Ask to the admins to make a Product available for a country</description>
        <formula>ISPICKVAL(Status__c , &apos;Approval Pending&apos;) &amp;&amp; ( ISNEW() || NOT( ISPICKVAL(PRIORVALUE(Status__c) , &apos;Approval Pending&apos;)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
