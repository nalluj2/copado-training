<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Aggregate_Entry_Unique_Identifier</fullName>
        <description>workflow will try to update this field with the formula &quot;Fiscal_Year__c + Fiscal_Month__c + DIB_Department_ID__c&quot; 
That means that these 3 fields make the record unique</description>
        <field>Unique_Identifier__c</field>
        <formula>Fiscal_Year__c + Fiscal_Month__c + DIB_Department_ID__c</formula>
        <name>Aggregate Entry: Unique Identifier</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Aggregated Entry%3A Unique Identifier Update</fullName>
        <actions>
            <name>Aggregate_Entry_Unique_Identifier</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>workflow will try to update the field with the formula &quot;Fiscal_Year__c + Fiscal_Month__c + DIB_Department_ID__c&quot; 
That means that these 3 fields make the record unique</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
