<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>UpdateUniqueAPDepProd</fullName>
        <field>UniqueAPDepProd__c</field>
        <formula>Account_Plan__r.Name+  Department_Lookup__c  +Product_Text__c</formula>
        <name>UpdateUniqueAPDepProd</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>UpdateUniqueAPDepProd</fullName>
        <actions>
            <name>UpdateUniqueAPDepProd</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Diabetes_Performance__c.Department_Text__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Diabetes_Performance__c.Product_Text__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
