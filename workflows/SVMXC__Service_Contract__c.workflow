<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SVMXC__SVMXC_CalculateContractPrice</fullName>
        <description>DO NOT USE THIS FIELD UPDATE</description>
        <field>SVMXC__Discounted_Price__c</field>
        <formula>SVMXC__Discounted_Price__c * 1</formula>
        <name>SVMXC_CalculateContractPrice</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>SVMXC__SVMXC_ContractPriceOrDiscountChanged</fullName>
        <actions>
            <name>SVMXC__SVMXC_CalculateContractPrice</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>DO NOT ACTIVATE OR USE THIS RULE</description>
        <formula>1 = 2</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
