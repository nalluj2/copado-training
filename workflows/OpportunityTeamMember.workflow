<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_Seller_team_member</fullName>
        <description>New Seller team member</description>
        <protected>false</protected>
        <recipients>
            <field>UserId</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunities/New_Seller_Team_Template</template>
    </alerts>
    <rules>
        <fullName>Notify new team member</fullName>
        <actions>
            <name>New_Seller_team_member</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Notify new team member that they have been added to a seller team.</description>
        <formula>AND( CONTAINS( $User.Profile_Name__c , &quot;MITG&quot;) , CreatedDate =  LastModifiedDate)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
