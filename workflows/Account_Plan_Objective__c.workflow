<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Account_Plan_Objective</fullName>
        <description>Update of &quot;Type_Text&quot; field based on value coming from &quot;Type&quot; formula field. The &quot;Type_Text&quot; field will be used in for roll up summary on Acc Plan / iPlan.</description>
        <field>Type_Text__c</field>
        <formula>Type__c</formula>
        <name>Account Plan Objective Type_Text update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Acc Plan Obj%3A Update field Type_Text</fullName>
        <actions>
            <name>Account_Plan_Objective</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Plan_Objective__c.Type__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Update field Type_Text for roll up summary on Acc Plan / iPlan</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
