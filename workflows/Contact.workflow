<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Germany_New_contact</fullName>
        <ccEmails>delphi@medtronic.de</ccEmails>
        <description>Germany: New Contact</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Contact_New_German_Contact</template>
    </alerts>
    <rules>
        <fullName>Contact%3A Notify New Contact Germany</fullName>
        <actions>
            <name>Germany_New_contact</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>GERMANY</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
