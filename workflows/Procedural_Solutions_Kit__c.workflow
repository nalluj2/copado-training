<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Box_Builder_approved_box</fullName>
        <description>Inform Box owner about approved configuration</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>rs.proceduralsolutionsemea@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Box_Builder/approved_template</template>
    </alerts>
    <alerts>
        <fullName>Box_Builder_available_box</fullName>
        <description>Inform Box owner about available configuration</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>rs.proceduralsolutionsemea@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Box_Builder/available_template</template>
    </alerts>
    <alerts>
        <fullName>Box_Builder_rejected_box</fullName>
        <description>Inform Box owner about rejeced configuration</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>rs.proceduralsolutionsemea@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Box_Builder/rejection_template</template>
    </alerts>
    <rules>
        <fullName>Box Builder%3A Approved Box notification</fullName>
        <actions>
            <name>Box_Builder_approved_box</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Inform the owner of a Box that it has been approved</description>
        <formula>AND( ISPICKVAL(Status__c , &apos;Approved&apos;), NOT(ISPICKVAL(PRIORVALUE(Status__c) , &apos;Approved&apos;)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Box Builder%3A Available Box notification</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Procedural_Solutions_Kit__c.Status__c</field>
            <operation>equals</operation>
            <value>Available</value>
        </criteriaItems>
        <description>Inform the owner of a Box that it is available in SAP</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Box_Builder_available_box</name>
                <type>Alert</type>
            </actions>
            <timeLength>24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Box Builder%3A Rejected Box notification</fullName>
        <actions>
            <name>Box_Builder_rejected_box</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Inform the owner of a Box that has been rejected</description>
        <formula>AND( ISPICKVAL(Status__c , &apos;Open&apos;), NOT(ISNEW()), NOT(ISPICKVAL(PRIORVALUE(Status__c) , &apos;Open&apos;)), NOT(ISBLANK( Rejection_Reason__c )) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
