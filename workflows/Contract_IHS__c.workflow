<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Deactivate_Contract</fullName>
        <description>Deactivate Contract when Enddate has passed</description>
        <field>Active__c</field>
        <literalValue>0</literalValue>
        <name>Deactivate Contract</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Deactivate Contract when enddate passed</fullName>
        <active>true</active>
        <description>Deactivate the Contract when the enddate has passed</description>
        <formula>Active__c = TRUE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Deactivate_Contract</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Contract_IHS__c.End_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
