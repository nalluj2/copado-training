<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SVMX_Set_IP_ID_to_Serial_Number</fullName>
        <field>Name</field>
        <formula>SVMXC__Serial_Lot_Number__c</formula>
        <name>Set IP ID to Serial Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Set Installed Product ID</fullName>
        <actions>
            <name>SVMX_Set_IP_ID_to_Serial_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SVMXC__Installed_Product__c.SVMXC__Serial_Lot_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Workflow rule to set Installed Product ID to serial number</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
