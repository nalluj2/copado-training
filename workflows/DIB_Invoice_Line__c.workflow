<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Invoice_Line_Uncheck_Automated_Allocati</fullName>
        <field>Automated_Allocation__c</field>
        <literalValue>0</literalValue>
        <name>Invoice Line: Uncheck Automated Allocati</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Invoice Line Not Allocated</fullName>
        <actions>
            <name>Invoice_Line_Uncheck_Automated_Allocati</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>DIB_Invoice_Line__c.Is_Allocated__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>DIB_Invoice_Line__c.Automated_Allocation__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>If an automatic allocation has been done, but a rep edits this via the My Sales screen, the &quot;Automated Allocation&quot; checkbox must be unchecked. When the user clicks &quot;edit&quot; the &quot;Is Allocated&quot; will be set to false by code, just as other fields.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
