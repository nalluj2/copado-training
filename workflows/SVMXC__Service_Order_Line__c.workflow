<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>SVMX_Part_Request_Alert_from_the_Work_Order</fullName>
        <description>Part Request Alert from the Work Order</description>
        <protected>false</protected>
        <recipients>
            <recipient>jesus.lozano@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Germany_E_mail_TEST</template>
    </alerts>
    <rules>
        <fullName>SVMX Parts Request Mail Alert</fullName>
        <actions>
            <name>SVMX_Part_Request_Alert_from_the_Work_Order</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SVMXC__Service_Order_Line__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Request/Receipt</value>
        </criteriaItems>
        <criteriaItems>
            <field>SVMXC__Service_Order_Line__c.SVMXC__Line_Type__c</field>
            <operation>equals</operation>
            <value>Parts</value>
        </criteriaItems>
        <description>To send an email alert whenever a Work Detail Parts Request line is created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
