<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Account_Update_Request_ANZ</fullName>
        <description>Account Update Request ANZ</description>
        <protected>false</protected>
        <recipients>
            <recipient>ANZ_IT</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_Update_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Account_Update_Request_ANZ_Person_Account</fullName>
        <description>Account Update Request ANZ Person Account</description>
        <protected>false</protected>
        <recipients>
            <recipient>genericanzdib@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_Update_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Account_Update_Request_Adriatic_West</fullName>
        <description>Account Update Request Adriatic West</description>
        <protected>false</protected>
        <recipients>
            <recipient>data.steward.adriatic@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_Update_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Account_Update_Request_Bulgaria</fullName>
        <description>Account Update Request Bulgaria</description>
        <protected>false</protected>
        <recipients>
            <recipient>gerd.balette@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_Update_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Account_Update_Request_CAN</fullName>
        <description>Account Update Request CAN</description>
        <protected>false</protected>
        <recipients>
            <recipient>anne.proulx@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jeff.mignacco@medtronic.com.emeac</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lucie.genest-ferrie@medtronic.com.ds</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sfe.jeff.mignacco@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_Update_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Account_Update_Request_Emerging_East</fullName>
        <description>Account Update Request Emerging East</description>
        <protected>false</protected>
        <recipients>
            <recipient>gerd.balette@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_Update_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Account_Update_Request_Hungary</fullName>
        <description>Account Update Request Hungary</description>
        <protected>false</protected>
        <recipients>
            <recipient>data.steward.hungary@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_Update_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Account_Update_Request_KOR</fullName>
        <description>Account Update Request KOR</description>
        <protected>false</protected>
        <recipients>
            <recipient>vladimir.vujatovic@medtronic.com.int</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>GCN_Email_Templates/Account_Update_Request_KOR</template>
    </alerts>
    <alerts>
        <fullName>Account_Update_Request_KZK</fullName>
        <description>Account Update Request KZK</description>
        <protected>false</protected>
        <recipients>
            <recipient>gerd.balette@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_Update_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Account_Update_Request_MEA</fullName>
        <description>Account Update Request MEA</description>
        <protected>false</protected>
        <recipients>
            <recipient>diane.pithey@medtronic.com.ds</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_Update_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Account_Update_Request_Romania_Moldavia</fullName>
        <description>Account Update Request Romania Moldavia</description>
        <protected>false</protected>
        <recipients>
            <recipient>data.steward.romania@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>hrvoje.evic@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_Update_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Account_Update_Request_Russian_Fed</fullName>
        <description>Account Update Request Russian Fed.</description>
        <protected>false</protected>
        <recipients>
            <recipient>vasyliy.arzhalovskiy@medtronic.com.ds</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_Update_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Account_Update_Request_Slovakia</fullName>
        <description>Account Update Request Slovakia</description>
        <protected>false</protected>
        <recipients>
            <recipient>gerd.balette@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_Update_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Account_Update_Request_Turkey</fullName>
        <description>Account Update Request Turkey</description>
        <protected>false</protected>
        <recipients>
            <recipient>diane.pithey@medtronic.com.ds</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_Update_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Account_update_requested_Germany</fullName>
        <ccEmails>rs.dussfdc2@medtronic.com</ccEmails>
        <description>Account Update Request Germany</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_Update_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Account_update_requested_in_Salesforce_com</fullName>
        <ccEmails>dl.customermasteremea@medtronic.com</ccEmails>
        <ccEmails>rs.processing-pdscm1@medtronic.com</ccEmails>
        <description>Account Update Request Europe</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_Update_Request_email_template</template>
    </alerts>
    <alerts>
        <fullName>Italy_Account_Update_Request</fullName>
        <ccEmails>dl.customermasteremea@medtronic.com</ccEmails>
        <ccEmails>rs.processing-pdscm1@medtronic.com</ccEmails>
        <description>Account Update Request Italy</description>
        <protected>false</protected>
        <recipients>
            <recipient>micaela.passardi@medtronic.com2</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_Update_Request_email_template</template>
    </alerts>
    <rules>
        <fullName>Account%3A Notify Account Update Request ANZ</fullName>
        <actions>
            <name>Account_Update_Request_ANZ</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>AUSTRALIA,NEW ZEALAND</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>SAP Account</value>
        </criteriaItems>
        <description>Send email to notify account update request when it has been filed by user in ANZ, using the &lt;request update&gt; button on account page. Mail is sent to the data steward</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Account Update Request Adriatic</fullName>
        <actions>
            <name>Account_Update_Request_Adriatic_West</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>ALBANIA,BOSNIA-HERZ.,CROATIA,KOSOVO,SLOVENIA,MONTENEGRO,SERBIA,NORTH MACEDONIA</value>
        </criteriaItems>
        <description>Send email to notify account update request has been filed by user in Adriatic using the &lt;request update&gt; button on account page. Mail is sent to the local data steward.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Account Update Request Adriatic East</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>BULGARIA,MONTENEGRO,SERBIA,NORTH MACEDONIA</value>
        </criteriaItems>
        <description>Send email to notify account update request has been filed by user in Adriatic East, using the &lt;request update&gt; button on account page. Mail is sent to the local data steward.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Account Update Request Bulgaria</fullName>
        <actions>
            <name>Account_Update_Request_Bulgaria</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>BULGARIA</value>
        </criteriaItems>
        <description>Send email to notify account update request has been filed by user in Bulgaria using the &lt;request update&gt; button on account page. Mail is sent to the local data steward.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Account Update Request CAN</fullName>
        <actions>
            <name>Account_Update_Request_CAN</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>CANADA</value>
        </criteriaItems>
        <description>Send email to notify account update request when it has been filed by user in CAN, using the &lt;request update&gt; button on account page. Mail is sent to the data steward</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Account Update Request EMEA</fullName>
        <actions>
            <name>Account_update_requested_in_Salesforce_com</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3 OR 4</booleanFilter>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>ANDORRA,ARUBA,AUSTRIA,BELGIUM,BOSNIA-HERZ.,BULGARIA,CANARY ISLANDS,CROATIA,CURAÇAO,CYPRUS,CZECH REPUBLIC,DENMARK,DUTCH ANTILLES,EGYPT,FAROE ISLANDS,FINLAND,FRENCH GUYANA,FRENCH POLYNES.</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>GREECE,GREENLAND,HUNGARY,ICELAND,IRAQ,IRELAND,ISRAEL,ITALY,LATVIA,LEBANON,LIECHTENSTEIN,LITHUANIA,LUXEMBOURG</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>MALTA,NETHERLANDS,NORTH MACEDONIA,NORWAY,PAKISTAN,POLAND,REUNION,ROMANIA,SAUDI ARABIA,SERBIA,SLOVAKIA,SLOVENIA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>SPAIN,SWEDEN,SWITZERLAND,TUNISIA,TURKEY,UKRAINE,UNITED KINGDOM,UTD.ARAB EMIR.,VATICAN CITY,ESTONIA,SAN MARINO</value>
        </criteriaItems>
        <description>Send email to notify account update request has been filed by user, using the &lt;request update&gt; button on account page. Mail is sent to the EMEA data steward team. (excl. Germany, CEMA (Partly), KAZ, Russia)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Account Update Request Emerging East</fullName>
        <actions>
            <name>Account_Update_Request_Emerging_East</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>ARMENIA,BELARUS,GEORGIA,UKRAINE</value>
        </criteriaItems>
        <description>Send email to notify account update request has been filed by user in Adriatic, using the &lt;request update&gt; button on account page. Mail is sent to the local data steward.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Account Update Request Europe</fullName>
        <actions>
            <name>Account_update_requested_in_Salesforce_com</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 OR 2 OR 3 OR 4</booleanFilter>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>DENMARK,DUTCH ANTILLES,CZECH REPUBLIC,ANDORRA,AUSTRIA,BELGIUM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>FRANCE,NORWAY,ICELAND,NETHERLANDS,GREECE,IRELAND,LUXEMBOURG,FINLAND</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>SPAIN,SWEDEN,SWITZERLAND,PORTUGAL,GERMANY,UNITED KINGDOM,POLAND</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>ESTONIA,GREENLAND,ISRAEL,LATVIA,LITHUANIA,SAN MARINO</value>
        </criteriaItems>
        <description>Send email to notify account update request has been filed by user in Europe, using the &lt;request update&gt; button on account page. Mail is sent to the central data stewards (NOT ITALY)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Account Update Request Germany</fullName>
        <actions>
            <name>Account_update_requested_Germany</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>GERMANY</value>
        </criteriaItems>
        <description>Send email to notify account update request has been filed by user, using the &lt;request update&gt; button on account page. Mail is sent to the local data steward.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Account Update Request Hungary</fullName>
        <actions>
            <name>Account_Update_Request_Hungary</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>HUNGARY</value>
        </criteriaItems>
        <description>Send email to notify account update request has been filed by user in Hungary, using the &lt;request update&gt; button on account page. Mail is sent to the local data steward.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Account Update Request Italy</fullName>
        <actions>
            <name>Italy_Account_Update_Request</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>ITALY</value>
        </criteriaItems>
        <description>Send email to notify account update request has been filed by user, using the &lt;request update&gt; button on account page. Mail is sent to the italian data stewards.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Account Update Request KOR</fullName>
        <actions>
            <name>Account_Update_Request_KOR</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>SOUTH KOREA</value>
        </criteriaItems>
        <description>Send email to notify account update request has been filled by user in KOR, using the &lt;request update&gt; button on account page. Mail is sent to the local data steward.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Account Update Request KZK</fullName>
        <actions>
            <name>Account_Update_Request_KZK</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>AZERBAIJAN,KAZAKHSTAN,TURKMENISTAN,KIRGHIZIA</value>
        </criteriaItems>
        <description>Send email to notify account update request has been filed by user in KZK, using the &lt;request update&gt; button on account page. Mail is sent to the local data steward.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Account Update Request MEA</fullName>
        <actions>
            <name>Account_Update_Request_MEA</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 OR 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>,ALGERIA,BAHRAIN,CAMEROON,EGYPT,JORDAN,KUWAIT,LEBANON,MOROCCO,NIGERIA,PAKISTAN,SAUDI ARABIA,SENEGAL,SOUTH AFRICA,SYRIA,TUNISIA,UTD.ARAB EMIR.</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>ANGOLA,BOTSWANA,CONGO,IVORY COAST,ETHIOPIA,GHANA,KENYA,LESOTHO,MADAGASCAR,MALAWI,MAURITIUS,MOZAMBIQUE,NAMIBIA,NIGERIA,RWANDA,SEYCHELLES,SWAZILAND,TANZANIA,UGANDA,ZAMBIA,ZIMBABWE,DJIBOUTI</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>CYPRUS</value>
        </criteriaItems>
        <description>Send email to notify account update request has been filed by user in MEA, using the &lt;request update&gt; button on account page. Mail is sent to the central MEA data stewards</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Account Update Request Romania Moldavia</fullName>
        <actions>
            <name>Account_Update_Request_Romania_Moldavia</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>MOLDAVIA,ROMANIA</value>
        </criteriaItems>
        <description>Send email to notify account update request has been filed by user in Adriatic, using the &lt;request update&gt; button on account page. Mail is sent to the local data steward.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Account Update Request Russian Fed%2E</fullName>
        <actions>
            <name>Account_Update_Request_Russian_Fed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>RUSSIAN FED.</value>
        </criteriaItems>
        <description>Send email to notify account update request has been filed by user in Russian Fed., using the &lt;request update&gt; button on account page. Mail is sent to the local data steward.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Account Update Request Slovakia</fullName>
        <actions>
            <name>Account_Update_Request_Slovakia</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>SLOVAKIA</value>
        </criteriaItems>
        <description>Send email to notify account update request has been filed by user in Slovakia, using the &lt;request update&gt; button on account page. Mail is sent to the local data steward.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Account Update Request Turkey</fullName>
        <actions>
            <name>Account_Update_Request_Turkey</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>TURKEY</value>
        </criteriaItems>
        <description>Send email to notify account update request has been filed by user in MEA, using the &lt;request update&gt; button on account page. Mail is sent to the central MEA data stewards</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Pending Account MEA non-SAP</fullName>
        <actions>
            <name>Account_Update_Request_MEA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>ALGERIA,BAHRAIN,CAMEROON,JORDAN,KUWAIT,MOROCCO,NIGERIA,SENEGAL,SOUTH AFRICA,SYRIA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>ANGOLA,BOTSWANA,CONGO,DJIBOUTI,ETHIOPIA,GHANA,IVORY COAST,KENYA,LESOTHO,MADAGASCAR,MALAWI,MAURITIUS,MOZAMBIQUE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>NIGERIA,RWANDA,SEYCHELLES,SWAZILAND,TANZANIA,UGANDA,ZAMBIA,ZIMBABWE</value>
        </criteriaItems>
        <description>Send email to notify account update request has been filed by user, using the &lt;request update&gt; button on account page. Mail is sent to the local data steward. (Non-SAP countries in the MEA region + South Africa)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify SAP Person Account Update Request ANZ</fullName>
        <actions>
            <name>Account_Update_Request_ANZ_Person_Account</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>AUSTRALIA,NEW ZEALAND</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>SAP Patient</value>
        </criteriaItems>
        <description>Notify ANZ SAP Person Account Update Request to DSS team</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
