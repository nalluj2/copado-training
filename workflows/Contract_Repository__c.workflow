<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Contr_Rep_upd_Contract_Status_Active</fullName>
        <field>Contract_Status__c</field>
        <literalValue>Active</literalValue>
        <name>Contr Rep: upd Contract Status Active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contr_Rep_upd_Contract_Status_Expired</fullName>
        <field>Contract_Status__c</field>
        <literalValue>Expired</literalValue>
        <name>Contr Rep: upd Contract Status Expired</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Repository_Copy_Contract_Id</fullName>
        <field>Contract_nr__c</field>
        <formula>Name</formula>
        <name>Contract Repository: Copy Contract Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Repository_Update_Tier</fullName>
        <field>TIER__c</field>
        <literalValue>1</literalValue>
        <name>Contract Repository: Update Tier</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Repository_update_Last_Prolong</fullName>
        <description>Update Last Prolongation Date to 31.12.4000</description>
        <field>Last_Possible_Prolongation_Date__c</field>
        <formula>DATE(4000,12,31)</formula>
        <name>Contract Repository: update Last Prolong</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Repository_update_T_C</fullName>
        <description>Update Terms &amp; Conditions to Customer</description>
        <field>Terms_Conditions__c</field>
        <literalValue>Customer</literalValue>
        <name>Contract Repository: update T&amp;C</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Contract Repository%3A Contract Status Active</fullName>
        <actions>
            <name>Contr_Rep_upd_Contract_Status_Active</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Update Status to Active when the executed contract has reached the orginal valid from date</description>
        <formula>OR( AND(Original_Valid_From_Date__c &lt;= TODAY(),(ISBLANK( Last_Prolonged_to__c ) || YEAR(Last_Prolonged_to__c )= 4000), Original_Valid_To_Date__c &gt; TODAY(), ISPICKVAL(PRIORVALUE( Contract_Status__c ),&quot;Executed&quot;)|| ISPICKVAL(PRIORVALUE( Contract_Status__c ),&quot;Expired&quot;)), AND(Original_Valid_From_Date__c &lt;= TODAY(),YEAR(Last_Prolonged_to__c )&lt;&gt; 4000, Last_Prolonged_to__c &gt; TODAY(), (ISPICKVAL(PRIORVALUE( Contract_Status__c ),&quot;Executed&quot;)|| ISPICKVAL(PRIORVALUE( Contract_Status__c ),&quot;Expired&quot;)) ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contract Repository%3A Contract Status Expired</fullName>
        <actions>
            <name>Contr_Rep_upd_Contract_Status_Expired</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Update Status to Expired when the Executed contract reached its end date</description>
        <formula>OR( AND((ISBLANK( Last_Prolonged_to__c ) || YEAR(Last_Prolonged_to__c )= 4000), Original_Valid_To_Date__c &lt; TODAY(), (ISPICKVAL(PRIORVALUE( Contract_Status__c ),&quot;Active&quot;)|| ISPICKVAL(PRIORVALUE( Contract_Status__c ),&quot;Executed&quot;))) , AND(YEAR(Last_Prolonged_to__c )&lt;&gt; 4000, Last_Prolonged_to__c &lt; TODAY(), (ISPICKVAL(PRIORVALUE( Contract_Status__c ),&quot;Active&quot;))|| ISPICKVAL(PRIORVALUE( Contract_Status__c ),&quot;Executed&quot;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contract Repository%3A Copy Contract Id</fullName>
        <actions>
            <name>Contract_Repository_Copy_Contract_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contract_Repository__c.Account_Country__c</field>
            <operation>equals</operation>
            <value>GERMANY</value>
        </criteriaItems>
        <description>Copy Contract Id to (Customer) Contract # for German Users</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contract Repository%3A Populate Customer Contract Nr</fullName>
        <actions>
            <name>Contract_Repository_Copy_Contract_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Contract_Repository__c.Contract_nr__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>populate ‘(Customer) Contract #’ (Contract_nr__c) with ‘Contract Id’ (Name) in case it is not populated by the Vendavo Interface or manual by the user</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contract Repository%3A Set Tier for Framework</fullName>
        <actions>
            <name>Contract_Repository_Update_Tier</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract_Repository__c.Primary_Contract_Type__c</field>
            <operation>equals</operation>
            <value>Framework agreement</value>
        </criteriaItems>
        <description>Set Tier to True when the primary contract type is Framework</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contract Repository%3A update Last Prolongation Date</fullName>
        <actions>
            <name>Contract_Repository_update_Last_Prolong</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract_Repository__c.Evergreen_Clause__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract_Repository__c.Last_Possible_Prolongation_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Update Last Prolongation Date when Evergreen Clause is Yes</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contract Repository%3A update T%26C for Tender</fullName>
        <actions>
            <name>Contract_Repository_update_T_C</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Contract_Repository__c.Primary_Contract_Type__c</field>
            <operation>equals</operation>
            <value>Commercial sales Tender</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract_Repository__c.Additional_Contract_Types__c</field>
            <operation>includes</operation>
            <value>Commercial sales Tender</value>
        </criteriaItems>
        <description>If Primary or additional contract type is Tender then the Terms &amp; Conditions is set to Customer</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
