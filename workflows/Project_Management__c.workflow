<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Change_Request_Email</fullName>
        <description>Send email to Requestor that Project is being converted to Change Request</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>sfe@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Project_Management_Converted_Change_Request_Template</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_To_Project_Requestor_After_Submission</fullName>
        <description>Send Email To Project Requestor After Submission</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>sfe@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Project_Management_Requestor_Approval_Template</template>
    </alerts>
    <alerts>
        <fullName>Send_email_to_Requestor_that_Project_is_being_converted_to_Change_Request</fullName>
        <description>Send email to Requestor that Project is being converted to Change Request</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>sfe@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Project_Management_Converted_Change_Request_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>Stage_Business_Case_Assessment</fullName>
        <field>Stage__c</field>
        <literalValue>2. Business Case Assessment</literalValue>
        <name>Stage=Business Case Assessment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_Closed</fullName>
        <field>Stage__c</field>
        <literalValue>Convert to Change Request</literalValue>
        <name>Stage=Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_Cost_Allocation</fullName>
        <field>Stage__c</field>
        <literalValue>5. Cost Allocation</literalValue>
        <name>Stage=Cost Allocation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_Deployed</fullName>
        <field>Stage__c</field>
        <literalValue>Deployed</literalValue>
        <name>Stage=Deployed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_Deployment</fullName>
        <field>Stage__c</field>
        <literalValue>10. Deployment</literalValue>
        <name>Stage= Deployment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_Development</fullName>
        <field>Stage__c</field>
        <literalValue>7. Development</literalValue>
        <name>Stage = Development</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Change Request</fullName>
        <actions>
            <name>Change_Request_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Send_email_to_Requestor_that_Project_is_being_converted_to_Change_Request</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Stage_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>OR(  	AND(  		Total_Hours__c &lt;35, 		ISPICKVAL(Stage__c,&quot;IT Effort &amp; Cost Assessment&quot;),  		ISCHANGED( Total_Hours__c ) 	),  	AND( 		Total_Hours__c &lt;35, 		ISPICKVAL(Stage__c,&quot;Assessment (Scalability)&quot;) 	) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
