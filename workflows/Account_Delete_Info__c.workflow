<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>AccountDeleteInfo_Notify_User_of_Pending_Account_Deletion_Merge</fullName>
        <description>AccountDeleteInfo: Notify User of Pending Account Deletion/Merge</description>
        <protected>false</protected>
        <recipients>
            <field>Account_Creator__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_Pending_Account_is_deleted_merged</template>
    </alerts>
    <rules>
        <fullName>Notify User of Pending Account Deletion%2FMerge</fullName>
        <actions>
            <name>AccountDeleteInfo_Notify_User_of_Pending_Account_Deletion_Merge</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Delete_Info__c.Deleted_Account_Record_Type__c</field>
            <operation>equals</operation>
            <value>Pending Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account_Delete_Info__c.Deleted_Account_Country__c</field>
            <operation>equals</operation>
            <value>INDIA</value>
        </criteriaItems>
        <description>only enable for India users</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
