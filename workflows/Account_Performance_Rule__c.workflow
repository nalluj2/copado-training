<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>FU_UpdateUniqueCombination</fullName>
        <field>Unique_BU_JobTitle__c</field>
        <formula>Concat_Bu_JobTitle__c</formula>
        <name>FU_UpdateUniqueCombination</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>WF_UpdateBU-JobTitle Field</fullName>
        <actions>
            <name>FU_UpdateUniqueCombination</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Performance_Rule__c.Job_Title_vs__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Update the unique field with the combination of BU and JobTitle</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
