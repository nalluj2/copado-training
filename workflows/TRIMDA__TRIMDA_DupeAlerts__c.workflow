<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>DataTrim_Dupe_Alert_failure</fullName>
        <description>DataTrim Dupe Alert failure</description>
        <protected>false</protected>
        <recipients>
            <recipient>glenn.van.der.kruk@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jan.kebeck@medtronic.nl</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/DataTrim_Dupe_Alert_issue</template>
    </alerts>
    <rules>
        <fullName>DataTrim Dupe Alert issue</fullName>
        <actions>
            <name>DataTrim_Dupe_Alert_failure</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>TRIMDA__TRIMDA_DupeAlerts__c.TRIMDA__Run_Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>TRIMDA__TRIMDA_DupeAlerts__c.TRIMDA__Last_Run_Result__c</field>
            <operation>notEqual</operation>
            <value>OK</value>
        </criteriaItems>
        <description>This workflow rule will sent an email to Jan and Glenn in case a Datatrim Dedupe tool run fails.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
