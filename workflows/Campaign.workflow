<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Send_to_CVent_TRUE</fullName>
        <field>Send_to_CVent__c</field>
        <literalValue>1</literalValue>
        <name>Set Send to CVent (TRUE)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Set_Send_to_CVent_TRUE</fullName>
        <actions>
            <name>Set_Send_to_CVent_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(
	OR(
		RecordType.DeveloperName = &apos;CVent&apos;   
		, RecordType.DeveloperName = &apos;CVent_EMEA&apos;
	)
	, OR(
		ISCHANGED( Total_MDT_Staff__c )
		, ISCHANGED( Total_Delegate__c )
	)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
