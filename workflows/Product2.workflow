<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>MMX_Marketing_product_changed_in_SFDC</fullName>
        <ccEmails>euhelp@mspexcng.medtronic.com</ccEmails>
        <description>MMX: Marketing product changed in SFDC</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Marketing_Product_Changed_in_SFDC</template>
    </alerts>
    <fieldUpdates>
        <fullName>Non_SAP_Product_Unique_Check</fullName>
        <field>Product_Name_Unique__c</field>
        <formula>TEXT(Region_vs__c) &amp; &quot; - &quot; &amp; Product_Group__r.Therapy_ID__r.Name &amp; &quot; - &quot; &amp; Product_Group_Name__c &amp; &quot; - &quot; &amp; Name</formula>
        <name>Non SAP Product Unique Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Product_Copy_RT_to_text_field</fullName>
        <field>Record_Type_Text__c</field>
        <formula>$RecordType.Name</formula>
        <name>Product: Copy RT to text field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Product_Copy_product_code_to_unique_fie</fullName>
        <field>Product_Code_Unique__c</field>
        <formula>RecordType.DeveloperName &amp; ProductCode</formula>
        <name>Product: Copy product code to unique fie</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Product_Mark_SAP_Product_Consumable</fullName>
        <field>Consumable_Bool__c</field>
        <literalValue>1</literalValue>
        <name>Product: Mark SAP Product Consumable</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Product_Unique_No_to_ProductCode</fullName>
        <field>ProductCode</field>
        <formula>Product_Unique_No__c</formula>
        <name>Product: Unique No. to ProductCode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Product_Unique_No_to_Unique</fullName>
        <field>Product_Code_Unique__c</field>
        <formula>RecordType.DeveloperName &amp; ProductCode</formula>
        <name>Product: Unique No. to Unique</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Marketing Product Changed in SFDC</fullName>
        <actions>
            <name>MMX_Marketing_product_changed_in_SFDC</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Send an email to EUHELP when marketing product is changed</description>
        <formula>if(and(  Record_Type_Text__c  = &quot;MDT Marketing Product&quot;,not(Isnew()), OR(ISCHANGED( Name ), ISCHANGED( Product_Group__c ))) ,true,false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Non SAP Product Unique Check</fullName>
        <actions>
            <name>Non_SAP_Product_Unique_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>SAP Product,SAP Bundle Product,SAP CFN Product</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Product%3A Copy RT to text field</fullName>
        <actions>
            <name>Product_Copy_RT_to_text_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.RecordTypeId</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Product%3A Copy product code to unique field</fullName>
        <actions>
            <name>Product_Copy_product_code_to_unique_fie</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.ProductCode</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Product%3A MPG Code not filled</fullName>
        <actions>
            <name>Product_Unique_No_to_ProductCode</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Product_Unique_No_to_Unique</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.MPG_Code_Text__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>SAP Bundle Product,SAP CFN Product</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Product%3A MPG_Code_Text is filled</fullName>
        <actions>
            <name>Product_Mark_SAP_Product_Consumable</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.MPG_Code_Text__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Stocking_Type__c</field>
            <operation>notEqual</operation>
            <value>ROH,HALB</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
