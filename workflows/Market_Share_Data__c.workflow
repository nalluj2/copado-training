<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Market_Share_Data_Update_Country_Name</fullName>
        <description>Market Share Data: Update Country Name based on Country</description>
        <field>Country_Name__c</field>
        <formula>Country__r.Name</formula>
        <name>Market Share Data: Update Country Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Market Share Data%3A Update Country Name</fullName>
        <actions>
            <name>Market_Share_Data_Update_Country_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Market_Share_Data__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Market Share Data: Update Country Name based on Country</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
