<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Last_Num_Update</fullName>
        <description>Last Recorded Value NUM to be updated with the text-value from the new record</description>
        <field>Last_Recorded_Value_Num__c</field>
        <formula>VALUE( Last_Recorded_Value__c )</formula>
        <name>Last Num Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>New_Numeric_Update</fullName>
        <field>New_Recorded_Value_Num__c</field>
        <name>New Numeric Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>New_Text_Update</fullName>
        <description>New Recorded Value TEXT to be updated with the NUM value from the record that was updated in the MFL</description>
        <field>New_Recorded_Value__c</field>
        <formula>TEXT( New_Recorded_Value_Num__c )</formula>
        <name>New Text Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>SVMX Update Num Values</fullName>
        <actions>
            <name>Last_Num_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>New_Numeric_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Installed_Product_Measures__c.New_Recorded_Value__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Rule to update the Numeric values</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SVMX Update Text Values</fullName>
        <actions>
            <name>New_Text_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Installed_Product_Measures__c.New_Recorded_Value_Num__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Rule to update the Text values</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
