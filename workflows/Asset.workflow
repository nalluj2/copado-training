<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Asset_RTG_Imaging_PM_Due</fullName>
        <description>Asset RTG: Imaging PM Due</description>
        <protected>false</protected>
        <recipients>
            <recipient>joao.rodrigues@medtronic.com.emeac</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>ST_Service_Email_Templates/OUS_PM_Due</template>
    </alerts>
    <alerts>
        <fullName>Asset_RTG_Navigation_PM_Due</fullName>
        <description>Asset RTG: Navigation PM Due</description>
        <protected>false</protected>
        <recipients>
            <recipient>lorenzo.zanirato@medtronic.com.emeac</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>ST_Service_Email_Templates/OUS_PM_Due</template>
    </alerts>
    <alerts>
        <fullName>Asset_RTG_Navigation_PM_Due_BCEGI</fullName>
        <description>Asset RTG: Navigation PM Due - BCEGI</description>
        <protected>false</protected>
        <recipients>
            <recipient>marc.lauvenberg@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>responsecareservices@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>ST_Service_Email_Templates/OUS_PM_Due</template>
    </alerts>
    <alerts>
        <fullName>Asset_RTG_Navigation_PM_Due_France</fullName>
        <description>Asset RTG: Navigation PM Due - France</description>
        <protected>false</protected>
        <recipients>
            <recipient>sylvain.villard@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>responsecareservices@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>ST_Service_Email_Templates/OUS_PM_Due</template>
    </alerts>
    <alerts>
        <fullName>Asset_RTG_Navigation_PM_Due_Germany</fullName>
        <description>Asset RTG: Navigation PM Due - Germany</description>
        <protected>false</protected>
        <recipients>
            <recipient>matthias.werner@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>responsecareservices@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>ST_Service_Email_Templates/OUS_PM_Due</template>
    </alerts>
    <alerts>
        <fullName>Asset_RTG_Navigation_PM_Due_Iberia</fullName>
        <description>Asset RTG: Navigation PM Due - Iberia</description>
        <protected>false</protected>
        <recipients>
            <recipient>carlos.garcia.asensio@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>responsecareservices@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>ST_Service_Email_Templates/OUS_PM_Due</template>
    </alerts>
    <alerts>
        <fullName>Asset_RTG_Navigation_PM_Due_Italy</fullName>
        <description>Asset RTG: Navigation PM Due - Italy</description>
        <protected>false</protected>
        <recipients>
            <recipient>daniele.marinucci@medtronic.com2</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>responsecareservices@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>ST_Service_Email_Templates/OUS_PM_Due</template>
    </alerts>
    <alerts>
        <fullName>Asset_RTG_Navigation_PM_Due_NBP</fullName>
        <description>Asset RTG: Navigation PM Due - NBP</description>
        <protected>false</protected>
        <recipients>
            <recipient>gerd.balette@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>responsecareservices@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>ST_Service_Email_Templates/OUS_PM_Due</template>
    </alerts>
    <alerts>
        <fullName>Asset_RTG_Navigation_PM_Due_UKIreland</fullName>
        <description>Asset RTG: Navigation PM Due - UK/Ireland</description>
        <protected>false</protected>
        <recipients>
            <recipient>austin.leavy@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>responsecareservices@medtronic.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>ST_Service_Email_Templates/OUS_PM_Due</template>
    </alerts>
    <fieldUpdates>
        <fullName>Asset_Needs_check_due_to_account_change</fullName>
        <field>Needs_Check_due_to_Account_Change__c</field>
        <literalValue>1</literalValue>
        <name>Asset Needs check due to account change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_RTG_AxiEM_Included_for_Fusion_Sys</fullName>
        <field>AxiEM_Included__c</field>
        <literalValue>1</literalValue>
        <name>Asset RTG: AxiEM Included for Fusion Sys</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_RTG_Camera_Cart_Exp_Date</fullName>
        <field>Camera_Cart_Expiration_Date__c</field>
        <formula>Contract_End_Date__c</formula>
        <name>Asset RTG: Camera Cart Exp Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_RTG_Camera_Cart_Purchase_Date</fullName>
        <field>Camera_Cart_Purchase_Date__c</field>
        <formula>PurchaseDate</formula>
        <name>Asset RTG: Camera Cart Purchase Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_RTG_Camera_Cart_TRUE</fullName>
        <field>Camera_Cart__c</field>
        <literalValue>1</literalValue>
        <name>Asset RTG: Camera Cart TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_RTG_Camera_Cart_Type_N_A</fullName>
        <field>Camera_Type__c</field>
        <literalValue>N/A</literalValue>
        <name>Asset RTG: Camera Cart Type N/A</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_RTG_Camera_Type_Standard</fullName>
        <field>Camera_Type__c</field>
        <literalValue>Standard</literalValue>
        <name>Asset RTG: Camera Type: Standard</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_RTG_Checkpoint_American_Conf</fullName>
        <field>Checkpoint_Firewall_Router__c</field>
        <literalValue>American Config</literalValue>
        <name>Asset RTG: Checkpoint...: American Conf</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_RTG_Checkpoint_N_A</fullName>
        <field>Checkpoint_Firewall_Router__c</field>
        <literalValue>N/A</literalValue>
        <name>Asset RTG: Checkpoint...: N/A</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_RTG_Checkpoint_World_Config</fullName>
        <field>Checkpoint_Firewall_Router__c</field>
        <literalValue>World Config</literalValue>
        <name>Asset RTG: Checkpoint...: World Config</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_RTG_Comp_Type_S8_Comp_w_PCOIP</fullName>
        <field>Computer_Type__c</field>
        <literalValue>S8 Computer w/ PCOIP</literalValue>
        <name>Asset RTG: Comp Type S8 Comp w PCOIP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_RTG_Comp_Type_S8_Comp_w_o_PCOIP</fullName>
        <field>Computer_Type__c</field>
        <literalValue>S8 Computer w/o PCOIP</literalValue>
        <name>Asset RTG: Comp Type S8 Comp w/o PCOIP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_RTG_EM_Included_TRUE</fullName>
        <field>AxiEM_Included__c</field>
        <literalValue>1</literalValue>
        <name>Asset RTG: EM Included TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_RTG_Emitter_Type_Flat_Emitter</fullName>
        <field>AxiEM_Type__c</field>
        <literalValue>Flat Emitter</literalValue>
        <name>Asset RTG: Emitter Type Flat Emitter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_RTG_Emitter_Type_N_A</fullName>
        <field>AxiEM_Type__c</field>
        <literalValue>N/A</literalValue>
        <name>Asset RTG: Emitter Type: N/A</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_RTG_Emitter_Type_Side_Emitter</fullName>
        <field>AxiEM_Type__c</field>
        <literalValue>Side Emitter</literalValue>
        <name>Asset RTG: Emitter Type: Side Emitter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_RTG_MazorXSE_Contract_Expire</fullName>
        <field>Camera_Cart_Expiration_Date__c</field>
        <formula>MazorX_SE_CC_Exp_Date_Ref_only__c</formula>
        <name>Asset RTG: MazorXSE Contract Expire</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_RTG_MazorXSE_Purchase_Date</fullName>
        <field>Camera_Cart_Purchase_Date__c</field>
        <formula>MazorX_SE_CC_Purchase_Date_Ref_only__c</formula>
        <name>Asset RTG: MazorXSE Purchase Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_RTG_MazorX_SE_Camera_Cart_SN_Upda</fullName>
        <field>Camera_Cart_Serial_Number__c</field>
        <formula>Related_MazorX_SE_Camera_Cart__r.Name</formula>
        <name>Asset RTG: MazorX SE Camera Cart SN Upda</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_RTG_SCU_TRUE</fullName>
        <field>SCU__c</field>
        <literalValue>1</literalValue>
        <name>Asset RTG: SCU: TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_RTG_Set_Number_of_PM_s_per_year</fullName>
        <field>Number_of_PM_s_per_year__c</field>
        <literalValue>2</literalValue>
        <name>Asset RTG: Set Number of PM’s per year</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_RTG_Storage_Bin_Flat_Emitter</fullName>
        <field>Storage_Bin__c</field>
        <literalValue>Flat Emitter</literalValue>
        <name>Asset RTG: Storage Bin: Flat Emitter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_RTG_Storage_Bin_N_A</fullName>
        <field>Storage_Bin__c</field>
        <literalValue>N/A</literalValue>
        <name>Asset RTG: Storage Bin: N/A</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_RTG_Storage_Bin_Standard</fullName>
        <field>Storage_Bin__c</field>
        <literalValue>Standard</literalValue>
        <name>Asset RTG: Storage Bin: Standard</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_RTG_Upd_ShipDate_Small_Capital</fullName>
        <field>Customer_Warranty_Start_Date__c</field>
        <formula>CASE( 
MOD(PurchaseDate - DATE(1985, 1, 5), 7), 
0, 10, 
1, 9, 
2, 8, 
3, 8, 
4, 8, 
5, 8, 
6, 10, 
0)+ PurchaseDate</formula>
        <name>Asset RTG: Upd ShipDate Small Capital</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_RTG_Upd_ShipDate_Small_Capital2</fullName>
        <field>Customer_Warranty_End_Date__c</field>
        <formula>CASE( 
MOD( PurchaseDate - DATE(1985, 1, 5), 7), 
0, 10, 
1, 9, 
2, 8, 
3, 8, 
4, 8, 
5, 8, 
6, 10, 
0)+ PurchaseDate +365</formula>
        <name>Asset RTG: Upd ShipDate Small Capital2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_RTG_Update_AxiEM_Type</fullName>
        <field>AxiEM_Type__c</field>
        <literalValue>Portable</literalValue>
        <name>Asset RTG: Update AxiEM Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_RTG_Update_EM_Expiration_Date</fullName>
        <description>EM Expiration Date field needs to be populated with the Contract End Date</description>
        <field>EM_Expiration_Date__c</field>
        <formula>Contract_End_Date__c</formula>
        <name>Asset RTG: Update EM Expiration Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_RTG_Update_EM_Purchase_Date</fullName>
        <description>EM Purchase Date field needs to be populated with the PurchaseDate</description>
        <field>EM_Purchase_Date__c</field>
        <formula>PurchaseDate</formula>
        <name>Asset RTG: Update EM Purchase Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_RTG_Update_Status_Removed_Standard</fullName>
        <field>Status</field>
        <literalValue>Removed</literalValue>
        <name>Asset RTG: Update Status Removed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_RTG_Update_Status_Returned_Std</fullName>
        <field>Status</field>
        <literalValue>Returned</literalValue>
        <name>Asset RTG: Update Status Returned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_RTG_Update_Status_Standard</fullName>
        <field>Status</field>
        <literalValue>Installed</literalValue>
        <name>Asset RTG: Update Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Asset_Agreement_Level_Status</fullName>
        <field>Asset_Agreement_Level_Status__c</field>
        <formula>IF ( AND( Asset_Warranty_Status__c = &quot;Active Warranty&quot;,  Service_Coverage_Status__c = &quot;Active Service Contract&quot;), &quot;Under Warranty with Contract&quot;, 
  IF ( AND( Asset_Warranty_Status__c = &quot;Active Warranty&quot;,  Service_Coverage_Status__c &lt;&gt; &quot;Active Service Contract&quot;), &quot;Under Warranty without Contract&quot;, 
  IF ( AND( Asset_Warranty_Status__c = &quot;Expired Warranty&quot;,  Service_Coverage_Status__c = &quot;Active Service Contract&quot;), &quot;Out of Warranty with Contract&quot;, 
 IF ( AND( Asset_Warranty_Status__c = &quot;Expired Warranty&quot;,  Service_Coverage_Status__c &lt;&gt; &quot;Active Service Contract&quot;), &quot;Out of Warranty without Contract&quot;, &quot; &quot; 

))))</formula>
        <name>Set Asset Agreement Level Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Removal_Date</fullName>
        <field>Removal_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Removal Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Asset Account change</fullName>
        <actions>
            <name>Asset_Needs_check_due_to_account_change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(ISNEW()),ISCHANGED(AccountId))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Asset RTG%3A Auto Update Asset Product Type if Fusion</fullName>
        <actions>
            <name>Asset_RTG_AxiEM_Included_for_Fusion_Sys</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Update_AxiEM_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Asset.RecordTypeId</field>
            <operation>equals</operation>
            <value>ENT,Midas,Navigation,O-Arm,PoleStar,Visualase</value>
        </criteriaItems>
        <criteriaItems>
            <field>Asset.Asset_Product_Type__c</field>
            <operation>equals</operation>
            <value>Fusion,Fusion Compact</value>
        </criteriaItems>
        <description>When a users selects a “Fusion” system type, the AxiEM Included field needs to be checked.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Asset RTG%3A Auto Update Asset Product Type if S7 or i7</fullName>
        <actions>
            <name>Asset_RTG_Update_AxiEM_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Update_EM_Expiration_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Update_EM_Purchase_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a Asset Prod type is “S7” or “i7” and the AxiEM included checkbox is checked, and the Delivery Date field is fillled, then the EM Purchase Date field needs to be populated with the PurchaseDate, and the EM Expiration Date field needs to be filled</description>
        <formula>AND(  OR  (  $RecordType.DeveloperName = &quot;ENT&quot;,   $RecordType.DeveloperName = &quot;Midas&quot;,   $RecordType.DeveloperName = &quot;Navigation&quot;,   $RecordType.DeveloperName = &quot;O_Arm&quot;,   $RecordType.DeveloperName = &quot;PoleStar&quot;, $RecordType.DeveloperName = &quot;Visualase&quot;  ), OR  (   ISPICKVAL( Asset_Product_Type__c , &quot;S7&quot;),    ISPICKVAL( Asset_Product_Type__c , &quot;i7&quot;)   ),  AxiEM_Included__c = True,  NOT(ISBLANK(Delivery_Date__c)),  OR  (  ISCHANGED( Contract_End_Date__c ),   ISBLANK( EM_Expiration_Date__c )  ) ,  NOT( ISCHANGED( EM_Expiration_Date__c ) )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Asset RTG%3A Auto Update Status - Installed</fullName>
        <actions>
            <name>Asset_RTG_Update_Status_Standard</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Asset.RecordTypeId</field>
            <operation>equals</operation>
            <value>ENT,Midas,Navigation,Accessories,O-Arm,PoleStar,Visualase,Mazor,NewTom</value>
        </criteriaItems>
        <criteriaItems>
            <field>Asset.InstallDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Changes Status to Installed once install date is entered</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Asset RTG%3A Auto Update Status - Remove</fullName>
        <actions>
            <name>Asset_RTG_Update_Status_Removed_Standard</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Asset.RecordTypeId</field>
            <operation>equals</operation>
            <value>ENT,Midas,Navigation,Accessories,O-Arm,PoleStar,Visualase,Mazor,NewTom</value>
        </criteriaItems>
        <criteriaItems>
            <field>Asset.Ownership_Status__c</field>
            <operation>equals</operation>
            <value>Removed by Depot,Removed by MENT,Removed by MNAV,Removed by Other</value>
        </criteriaItems>
        <description>Changes Status to Removed if Ownership Status Changes to Removed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Asset RTG%3A Auto Update Status - Return</fullName>
        <actions>
            <name>Asset_RTG_Update_Status_Returned_Std</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Asset.RecordTypeId</field>
            <operation>equals</operation>
            <value>ENT,Midas,Navigation,Accessories,O-Arm,PoleStar,Visualase,Mazor,NewTom</value>
        </criteriaItems>
        <criteriaItems>
            <field>Asset.Ownership_Status__c</field>
            <operation>equals</operation>
            <value>Cancelled/Returned</value>
        </criteriaItems>
        <description>Changes Status to Returned if Ownership Status Changes to Returned</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Asset RTG%3A Default MazorX Stealth Edition Values</fullName>
        <actions>
            <name>Asset_RTG_Camera_Cart_Exp_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Camera_Cart_Purchase_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Camera_Cart_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Camera_Type_Standard</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT ($Profile.Name = &quot;Integration Profile&quot;),  OR(  AND(ISNEW(),  ISPICKVAL(Asset_Product_Type__c,&quot;MazorX Stealth Edition&quot;)),  AND(ISPICKVAL(Asset_Product_Type__c,&quot;MazorX Stealth Edition&quot;),  OR(ISCHANGED(Contract_End_Date__c),  ISCHANGED(PurchaseDate))  )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Asset RTG%3A Default MazorX Stealth values</fullName>
        <actions>
            <name>Asset_RTG_Camera_Cart_Exp_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Camera_Cart_Purchase_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Camera_Type_Standard</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT($Profile.Name = &quot;Integration Profile&quot;),  Camera_Cart__c = TRUE,   OR(  AND(ISNEW(),       ISPICKVAL(Asset_Product_Type__c,&quot;MazorX Stealth&quot;)),    AND(ISPICKVAL(Asset_Product_Type__c,&quot;MazorX Stealth Edition&quot;),       OR(ISCHANGED(Contract_End_Date__c),        ISCHANGED(PurchaseDate))      ) ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Asset RTG%3A Default S8 Basic values</fullName>
        <actions>
            <name>Asset_RTG_Camera_Cart_Exp_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Camera_Cart_Purchase_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Camera_Cart_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Camera_Type_Standard</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Checkpoint_World_Config</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Comp_Type_S8_Comp_w_PCOIP</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Emitter_Type_N_A</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Storage_Bin_Standard</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( OR(  $RecordType.DeveloperName = &quot;Navigation&quot;,  $RecordType.DeveloperName = &quot;O_Arm&quot;,  $RecordType.DeveloperName = &quot;PoleStar&quot;,  $RecordType.DeveloperName = &quot;Visualase&quot;, $RecordType.DeveloperName = &quot;Pending_RTG_Asset&quot;   ), OR(  AND(ISNEW(),  ISPICKVAL(Asset_Product_Type__c,&quot;S8 Basic&quot;)),  AND(ISPICKVAL(Asset_Product_Type__c,&quot;S8 Basic&quot;),  OR(ISCHANGED(Contract_End_Date__c),  ISCHANGED(PurchaseDate))  ) )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Asset RTG%3A Default S8 EM Cran values</fullName>
        <actions>
            <name>Asset_RTG_Camera_Cart_Type_N_A</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Checkpoint_American_Conf</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Comp_Type_S8_Comp_w_PCOIP</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_EM_Included_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Emitter_Type_Side_Emitter</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_SCU_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Storage_Bin_Standard</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Update_EM_Expiration_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Update_EM_Purchase_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( OR(  $RecordType.DeveloperName = &quot;Navigation&quot;,  $RecordType.DeveloperName = &quot;O_Arm&quot;,  $RecordType.DeveloperName = &quot;PoleStar&quot;,  $RecordType.DeveloperName = &quot;Visualase&quot;, $RecordType.DeveloperName = &quot;Pending_RTG_Asset&quot;   ), OR(  AND(ISNEW(),  ISPICKVAL(Asset_Product_Type__c,&quot;S8 EM/Cranial&quot;)),  AND(ISPICKVAL(Asset_Product_Type__c,&quot;S8 EM/Cranial&quot;),  OR(ISCHANGED(Contract_End_Date__c),  ISCHANGED(PurchaseDate))  ) )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Asset RTG%3A Default S8 EM ENT values</fullName>
        <actions>
            <name>Asset_RTG_Camera_Cart_Type_N_A</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Checkpoint_American_Conf</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Comp_Type_S8_Comp_w_o_PCOIP</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_EM_Included_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Emitter_Type_Flat_Emitter</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Storage_Bin_Flat_Emitter</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Update_EM_Expiration_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Update_EM_Purchase_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( OR(  $RecordType.DeveloperName = &quot;Navigation&quot;,  $RecordType.DeveloperName = &quot;O_Arm&quot;,  $RecordType.DeveloperName = &quot;PoleStar&quot;,  $RecordType.DeveloperName = &quot;Visualase&quot;, $RecordType.DeveloperName = &quot;Pending_RTG_Asset&quot;   ), OR(  AND(ISNEW(),  ISPICKVAL(Asset_Product_Type__c,&quot;S8 EM/ENT&quot;)),  AND(ISPICKVAL(Asset_Product_Type__c,&quot;S8 EM/ENT&quot;),  OR(ISCHANGED(Contract_End_Date__c),  ISCHANGED(PurchaseDate))  ) )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Asset RTG%3A Default S8 Planning Station values</fullName>
        <actions>
            <name>Asset_RTG_Camera_Cart_Type_N_A</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Checkpoint_N_A</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Comp_Type_S8_Comp_w_o_PCOIP</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Emitter_Type_N_A</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Storage_Bin_N_A</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( OR(  $RecordType.DeveloperName = &quot;Navigation&quot;,  $RecordType.DeveloperName = &quot;O_Arm&quot;,  $RecordType.DeveloperName = &quot;PoleStar&quot;,  $RecordType.DeveloperName = &quot;Visualase&quot;, $RecordType.DeveloperName = &quot;Pending_RTG_Asset&quot;   ), OR(  AND(ISNEW(),  ISPICKVAL(Asset_Product_Type__c,&quot;S8 Planning Station&quot;)),  AND(ISPICKVAL(Asset_Product_Type__c,&quot;S8 Planning Station&quot;),  OR(ISCHANGED(Contract_End_Date__c),  ISCHANGED(PurchaseDate))  ) )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Asset RTG%3A Default S8 Plus values</fullName>
        <actions>
            <name>Asset_RTG_Camera_Cart_Exp_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Camera_Cart_Purchase_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Camera_Cart_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Camera_Type_Standard</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Checkpoint_World_Config</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Comp_Type_S8_Comp_w_PCOIP</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Emitter_Type_N_A</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_SCU_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Storage_Bin_Standard</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( OR(  $RecordType.DeveloperName = &quot;Navigation&quot;,  $RecordType.DeveloperName = &quot;O_Arm&quot;,  $RecordType.DeveloperName = &quot;PoleStar&quot;,  $RecordType.DeveloperName = &quot;Visualase&quot;, $RecordType.DeveloperName = &quot;Pending_RTG_Asset&quot;   ), OR(  AND(ISNEW(),  ISPICKVAL(Asset_Product_Type__c,&quot;S8 Plus&quot;)),  AND(ISPICKVAL(Asset_Product_Type__c,&quot;S8 Plus&quot;),  OR(ISCHANGED(Contract_End_Date__c),  ISCHANGED(PurchaseDate))  ) )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Asset RTG%3A Default S8 Premium Evaluate EM Included FALSE</fullName>
        <actions>
            <name>Asset_RTG_Emitter_Type_N_A</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( OR(  $RecordType.DeveloperName = &quot;Navigation&quot;,  $RecordType.DeveloperName = &quot;O_Arm&quot;,  $RecordType.DeveloperName = &quot;PoleStar&quot;,  $RecordType.DeveloperName = &quot;Visualase&quot;, $RecordType.DeveloperName = &quot;Pending_RTG_Asset&quot;   ), OR(  AND(ISNEW(),  AxiEM_Included__c = FALSE,  ISPICKVAL(Asset_Product_Type__c,&quot;S8 Premium&quot;)),  AND(AxiEM_Included__c = FALSE,  ISPICKVAL(Asset_Product_Type__c,&quot;S8 Premium&quot;),  OR(ISCHANGED(Contract_End_Date__c),  ISCHANGED(PurchaseDate))  ) )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Asset RTG%3A Default S8 Premium Evaluate EM Included TRUE</fullName>
        <actions>
            <name>Asset_RTG_Emitter_Type_Side_Emitter</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Update_EM_Expiration_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Update_EM_Purchase_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( OR(  $RecordType.DeveloperName = &quot;Navigation&quot;,  $RecordType.DeveloperName = &quot;O_Arm&quot;,  $RecordType.DeveloperName = &quot;PoleStar&quot;,  $RecordType.DeveloperName = &quot;Visualase&quot;, $RecordType.DeveloperName = &quot;Pending_RTG_Asset&quot;   ), OR(  AND(ISNEW(),  AxiEM_Included__c = TRUE,  ISPICKVAL(Asset_Product_Type__c,&quot;S8 Premium&quot;)),  AND(AxiEM_Included__c = TRUE,  ISPICKVAL(Asset_Product_Type__c,&quot;S8 Premium&quot;),  OR(ISCHANGED(Contract_End_Date__c),  ISCHANGED(PurchaseDate))  ) )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Asset RTG%3A Default S8 Premium values</fullName>
        <actions>
            <name>Asset_RTG_Camera_Cart_Exp_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Camera_Cart_Purchase_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Camera_Cart_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Camera_Type_Standard</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Checkpoint_American_Conf</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Comp_Type_S8_Comp_w_PCOIP</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_SCU_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Storage_Bin_Standard</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( OR(  $RecordType.DeveloperName = &quot;Navigation&quot;,  $RecordType.DeveloperName = &quot;O_Arm&quot;,  $RecordType.DeveloperName = &quot;PoleStar&quot;,  $RecordType.DeveloperName = &quot;Visualase&quot;, $RecordType.DeveloperName = &quot;Pending_RTG_Asset&quot;   ), OR(  AND(ISNEW(),  ISPICKVAL(Asset_Product_Type__c,&quot;S8 Premium&quot;)),  AND(ISPICKVAL(Asset_Product_Type__c,&quot;S8 Premium&quot;),  OR(ISCHANGED(Contract_End_Date__c),  ISCHANGED(PurchaseDate))  ) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Asset RTG%3A Imaging PM Due</fullName>
        <active>true</active>
        <description>45 Days before the PM Due Date, an email will go out to Joao Rodrigues saying the PM is coming due with the date due.</description>
        <formula>AND (  Next_PM_Due_Date__c &gt; TODAY() ,  OR   (   RecordType.Name = &quot;PoleStar&quot;,    RecordType.Name = &quot;O-Arm&quot;   )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Asset_RTG_Imaging_PM_Due</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Asset.Next_PM_Due_Date_Calc__c</offsetFromField>
            <timeLength>-45</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Asset RTG%3A Navigation PM Due</fullName>
        <active>false</active>
        <description>45 Days before the PM Due Date, an email will go out  saying the PM is coming due with the date due.</description>
        <formula>AND (  Next_PM_Due_Date_Calc__c &gt; TODAY() ,   RecordType.Name = &quot;Navigation&quot; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Asset_RTG_Navigation_PM_Due</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Asset.Next_PM_Due_Date_Calc__c</offsetFromField>
            <timeLength>-45</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Asset RTG%3A Navigation PM Due - BCEGI</fullName>
        <active>true</active>
        <description>45 Days before the PM Due Date, an email will go out  saying the PM is coming due with the date due.</description>
        <formula>AND ( WF__c = True, Next_PM_Due_Date_Calc__c &gt; TODAY() , RecordType.Name = &quot;Navigation&quot;, ISPICKVAL( Account.Account_Country_vs__c , &apos;BELGIUM,LUXEMBOURG,NETHERLANDS,SWITZERLAND,AUSTRIA,GREECE,ISRAEL,HUNGARY,ROMANIA,MOLDAVIA,SLOVAKIA,CZECH REPUBLIC,ALBANIA,BOSNIA-HERZ.,CROATIA,SLOVENIA,KOSOVO,SERBIA,BULGARIA,MACEDONIA,MONTENEGRO,ARMANIA,BELARUS,GEORGIA,UKRAINE​&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Asset_RTG_Navigation_PM_Due_BCEGI</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Asset.Next_PM_Due_Date_Calc__c</offsetFromField>
            <timeLength>-45</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Asset RTG%3A Navigation PM Due - France</fullName>
        <active>true</active>
        <description>45 Days before the PM Due Date, an email will go out  saying the PM is coming due with the date due.</description>
        <formula>AND (WF__c = True, Next_PM_Due_Date_Calc__c &gt; TODAY() , RecordType.Name = &quot;Navigation&quot;, ISPICKVAL( Account.Account_Country_vs__c, &apos;France&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Asset_RTG_Navigation_PM_Due_France</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Asset.Next_PM_Due_Date_Calc__c</offsetFromField>
            <timeLength>-45</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Asset RTG%3A Navigation PM Due - Germany</fullName>
        <active>true</active>
        <description>45 Days before the PM Due Date, an email will go out  saying the PM is coming due with the date due.</description>
        <formula>AND (WF__c = True, Next_PM_Due_Date_Calc__c &gt; TODAY() , RecordType.Name = &quot;Navigation&quot;, ISPICKVAL( Account.Account_Country_vs__c, &apos;Germany&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Asset_RTG_Navigation_PM_Due_Germany</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Asset.Next_PM_Due_Date_Calc__c</offsetFromField>
            <timeLength>-45</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Asset RTG%3A Navigation PM Due - Iberia</fullName>
        <active>true</active>
        <description>45 Days before the PM Due Date, an email will go out  saying the PM is coming due with the date due.</description>
        <formula>AND (WF__c = True, Next_PM_Due_Date_Calc__c &gt; TODAY() , RecordType.Name = &quot;Navigation&quot;, ISPICKVAL(Account.Account_Country_vs__c, &apos;Spain,Portugal&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Asset_RTG_Navigation_PM_Due_Iberia</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Asset.Next_PM_Due_Date_Calc__c</offsetFromField>
            <timeLength>-45</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Asset RTG%3A Navigation PM Due - Italy</fullName>
        <active>true</active>
        <description>45 Days before the PM Due Date, an email will go out  saying the PM is coming due with the date due.</description>
        <formula>AND (WF__c = True, Next_PM_Due_Date_Calc__c &gt; TODAY() , RecordType.Name = &quot;Navigation&quot;, ISPICKVAL(Account.Account_Country_vs__c, &apos;Italy&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Asset_RTG_Navigation_PM_Due_Italy</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Asset.Next_PM_Due_Date_Calc__c</offsetFromField>
            <timeLength>-45</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Asset RTG%3A Navigation PM Due - NBP</fullName>
        <active>true</active>
        <description>45 Days before the PM Due Date, an email will go out  saying the PM is coming due with the date due.</description>
        <formula>AND (WF__c = True, Next_PM_Due_Date_Calc__c &gt; TODAY() , RecordType.Name = &quot;Navigation&quot;, ISPICKVAL(Account.Account_Country_vs__c, &apos;POLAND,DENMARK,FINLAND,NORWAY,SWEDEN,ESTONIA,LATVIA,LITHUANIA&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Asset_RTG_Navigation_PM_Due_NBP</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Asset.Next_PM_Due_Date_Calc__c</offsetFromField>
            <timeLength>-45</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Asset RTG%3A Navigation PM Due - UK%2FIreland</fullName>
        <active>true</active>
        <description>45 Days before the PM Due Date, an email will go out  saying the PM is coming due with the date due.</description>
        <formula>AND (WF__c = True, Next_PM_Due_Date_Calc__c &gt; TODAY() , RecordType.Name = &quot;Navigation&quot;, ISPICKVAL(Account.Account_Country_vs__c, &apos;Ireland, United Kingdom&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Asset_RTG_Navigation_PM_Due_UKIreland</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Asset.Next_PM_Due_Date_Calc__c</offsetFromField>
            <timeLength>-45</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Asset RTG%3A Set Number of PM%E2%80%99s per year when changing recordtype to Polestar</fullName>
        <actions>
            <name>Asset_RTG_Set_Number_of_PM_s_per_year</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Asset.RecordTypeId</field>
            <operation>equals</operation>
            <value>PoleStar</value>
        </criteriaItems>
        <description>Set Number of PM’s per year to 2 (Default value) when changing recordtype to Polestar</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Asset RTG%3A Update Ship Date Small Capital</fullName>
        <actions>
            <name>Asset_RTG_Upd_ShipDate_Small_Capital</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_Upd_ShipDate_Small_Capital2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Asset.RecordTypeId</field>
            <operation>equals</operation>
            <value>ENT,Midas,Navigation,O-Arm,PoleStar,Visualase,Mazor,NewTom</value>
        </criteriaItems>
        <criteriaItems>
            <field>Asset.PurchaseDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Asset.Customer_Warranty_Start_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Updates Vendor Warranty Start Date = PurchaseDate + 7 business days and Vendor Warranty End Date = Vendor Warranty Start Date + 12 Month</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Asset%3A Set Asset Agreement Level Status</fullName>
        <actions>
            <name>Set_Asset_Agreement_Level_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Asset.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>set Asset Agreement Level Status</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MazorX SE Camera Cart SN Update</fullName>
        <actions>
            <name>Asset_RTG_MazorXSE_Contract_Expire</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_MazorXSE_Purchase_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Asset_RTG_MazorX_SE_Camera_Cart_SN_Upda</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Asset.Upgraded_MazorX__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>used to update Field MazorX SE Camera Cart SN to be equal to Related mazorX SE Camera Cart when populated</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
