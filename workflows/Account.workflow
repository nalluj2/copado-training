<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Account_Notify_Requester_When_Account_is_Approved</fullName>
        <description>Account: Notify Requester When Account is Approved</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_Pending_Account_is_Approved</template>
    </alerts>
    <alerts>
        <fullName>Belux_New_Pending_Account</fullName>
        <ccEmails>dl.customermasteremea@medtronic.com</ccEmails>
        <ccEmails>rs.processing-pdscm1@medtronic.com</ccEmails>
        <description>Belux: New Pending Account</description>
        <protected>false</protected>
        <recipients>
            <recipient>belgiumds@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_New_Pending</template>
    </alerts>
    <alerts>
        <fullName>CAN_New_Pending_Account</fullName>
        <description>CAN: New Pending Account</description>
        <protected>false</protected>
        <recipients>
            <recipient>anne.proulx@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jeff.mignacco@medtronic.com.emeac</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lucie.genest-ferrie@medtronic.com.ds</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sfe.jeff.mignacco@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_New_Pending</template>
    </alerts>
    <alerts>
        <fullName>EUR_DiB_Pending_Account_Alert</fullName>
        <ccEmails>justyna.krawczyk@medtronic.com</ccEmails>
        <description>EUR DiB Pending Account Alert</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/EUR_DiB_Pending_Account_Alert</template>
    </alerts>
    <alerts>
        <fullName>Germany_New_Pending_Account</fullName>
        <ccEmails>rs.dussfdc2@medtronic.com</ccEmails>
        <description>Germany: New Pending Account</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_New_Pending</template>
    </alerts>
    <alerts>
        <fullName>Italy_New_Pending_Account</fullName>
        <description>Italy: New Pending Account</description>
        <protected>false</protected>
        <recipients>
            <recipient>micaela.passardi@medtronic.com2</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_New_Pending</template>
    </alerts>
    <alerts>
        <fullName>New_Pending_Account_ANZ</fullName>
        <description>New Pending Account ANZ</description>
        <protected>false</protected>
        <recipients>
            <recipient>ANZ_IT</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_New_Pending</template>
    </alerts>
    <alerts>
        <fullName>New_Pending_Account_Adriatic_West</fullName>
        <description>New Pending Account Adriatic West</description>
        <protected>false</protected>
        <recipients>
            <recipient>data.steward.adriatic@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_New_Pending</template>
    </alerts>
    <alerts>
        <fullName>New_Pending_Account_Bulgaria</fullName>
        <description>New Pending Account Bulgaria</description>
        <protected>false</protected>
        <recipients>
            <recipient>gerd.balette@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_New_Pending</template>
    </alerts>
    <alerts>
        <fullName>New_Pending_Account_Emerging_East</fullName>
        <ccEmails>rs.mstcrm@medtronic.com</ccEmails>
        <description>New Pending Account Emerging East</description>
        <protected>false</protected>
        <recipients>
            <recipient>gerd.balette@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_New_Pending</template>
    </alerts>
    <alerts>
        <fullName>New_Pending_Account_Hungary</fullName>
        <description>New Pending Account Hungary</description>
        <protected>false</protected>
        <recipients>
            <recipient>data.steward.hungary@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_New_Pending</template>
    </alerts>
    <alerts>
        <fullName>New_Pending_Account_KZK</fullName>
        <description>New Pending Account KZK</description>
        <protected>false</protected>
        <recipients>
            <recipient>data.steward.kaz@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_New_Pending</template>
    </alerts>
    <alerts>
        <fullName>New_Pending_Account_MEA</fullName>
        <description>New Pending Account MEA</description>
        <protected>false</protected>
        <recipients>
            <recipient>diane.pithey@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_New_Pending</template>
    </alerts>
    <alerts>
        <fullName>New_Pending_Account_Romania_Moldavia</fullName>
        <description>New Pending Account Romania Moldavia</description>
        <protected>false</protected>
        <recipients>
            <recipient>data.steward.romania@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>hrvoje.evic@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_New_Pending</template>
    </alerts>
    <alerts>
        <fullName>New_Pending_Account_Russian_Fed</fullName>
        <description>New Pending Account Russian Fed.</description>
        <protected>false</protected>
        <recipients>
            <recipient>vasyliy.arzhalovskiy@medtronic.com.ds</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_New_Pending</template>
    </alerts>
    <alerts>
        <fullName>New_Pending_Account_Turkey</fullName>
        <description>New Pending Account Turkey</description>
        <protected>false</protected>
        <recipients>
            <recipient>diane.pithey@medtronic.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_New_Pending</template>
    </alerts>
    <alerts>
        <fullName>New_Pending_Account_USA</fullName>
        <ccEmails>liz.a.dabruzzi@medtronic.com</ccEmails>
        <description>New Pending Account USA</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_New_Pending_US</template>
    </alerts>
    <alerts>
        <fullName>New_pending_account_created_in_Salesforce_com</fullName>
        <ccEmails>dl.customermasteremea@medtronic.com</ccEmails>
        <ccEmails>rs.processing-pdscm1@medtronic.com</ccEmails>
        <description>New Pending Account Europe</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Data_Steward_Emails/Account_New_Pending</template>
    </alerts>
    <fieldUpdates>
        <fullName>Account_Update_SAP_Sales_Office</fullName>
        <description>Update SAP Sales Office for Buying Groups</description>
        <field>SAP_Sales_Office__c</field>
        <formula>CASE( Account_Country_vs__c , 
&apos;AUSTRIA&apos;,&apos;3200&apos;, 
&apos;BELGIUM&apos;,&apos;4400&apos;, 
&apos;BULGARIA&apos;,&apos;5601&apos;, 
&apos;CROATIA&apos;,&apos;5701&apos;, 
&apos;CZECH REPUBLIC&apos;,&apos;4600&apos;, 
&apos;DENMARK&apos;,&apos;4102&apos;, 
&apos;FINLAND&apos;,&apos;4200&apos;, 
&apos;FRANCE&apos;,&apos;3604&apos;, 
&apos;GERMANY&apos;,&apos;3000&apos;, 
&apos;GREECE&apos;,&apos;4800&apos;, 
&apos;HUNGARY&apos;,&apos;4700&apos;, 
&apos;IRELAND&apos;,&apos;3800&apos;, 
&apos;ISRAEL&apos;,&apos;1031&apos;, 
&apos;ITALY&apos;,&apos;3300&apos;, 
&apos;LUXEMBOURG&apos;,&apos;5900&apos;, 
&apos;NETHERLANDS&apos;,&apos;4308&apos;, 
&apos;NORWAY&apos;,&apos;4000&apos;, 
&apos;POLAND&apos;,&apos;4500&apos;, 
&apos;PORTUGAL&apos;,&apos;3500&apos;, 
&apos;ROMANIA&apos;,&apos;5703&apos;, 
&apos;RUSSIAN FED.&apos;,&apos;5401&apos;, 
&apos;SERBIA&apos;,&apos;5704&apos;, 
&apos;SLOVAKIA&apos;,&apos;5700&apos;, 
&apos;SLOVENIA&apos;,&apos;5705&apos;, 
&apos;SPAIN&apos;,&apos;3400&apos;, 
&apos;SWEDEN&apos;,&apos;3900&apos;, 
&apos;SWITZERLAND&apos;,&apos;3100&apos;, 
&apos;TURKEY&apos;,&apos;5201&apos;, 
&apos;UNITED KINGDOM&apos;,&apos;3700&apos;, 
&apos;UKRAINE&apos;,&apos;5401&apos;, 
&apos;SOUTH AFRICA&apos;,&apos;4900&apos;, 
&apos;MONTENEGRO&apos;,&apos;5704&apos;, 
&apos;LATVIA&apos;,&apos;5600&apos;, 
&apos;KOSOVO&apos;,&apos;5600&apos;, 
&apos;ICELAND&apos;,&apos;3900&apos;, 
&apos;ESTONIA&apos;,&apos;5600&apos;, 
&apos;&apos;)</formula>
        <name>Account: Update SAP Sales Office</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Update_SAP_Sales_Org</fullName>
        <description>Update SAP Sales Org for Buying Groups</description>
        <field>SAP_Sales_Org__c</field>
        <formula>CASE( Account_Country_vs__c , 
&apos;AUSTRIA&apos;,&apos;S059&apos;, 
&apos;BELGIUM&apos;,&apos;S071&apos;, 
&apos;BULGARIA&apos;,&apos;S186&apos;, 
&apos;CROATIA&apos;,&apos;S113&apos;, 
&apos;CZECH REPUBLIC&apos;,&apos;S073&apos;, 
&apos;DENMARK&apos;,&apos;S068&apos;, 
&apos;FINLAND&apos;,&apos;S069&apos;, 
&apos;FRANCE&apos;,&apos;S063&apos;, 
&apos;GERMANY&apos;,&apos;S057&apos;, 
&apos;GREECE&apos;,&apos;S075&apos;, 
&apos;HUNGARY&apos;,&apos;S074&apos;, 
&apos;IRELAND&apos;,&apos;S065&apos;, 
&apos;ISRAEL&apos;,&apos;S039&apos;, 
&apos;ITALY&apos;,&apos;S060&apos;, 
&apos;LUXEMBOURG&apos;,&apos;S071&apos;, 
&apos;NETHERLANDS&apos;,&apos;S070&apos;, 
&apos;NORWAY&apos;,&apos;S067&apos;, 
&apos;POLAND&apos;,&apos;S072&apos;, 
&apos;PORTUGAL&apos;,&apos;S062&apos;, 
&apos;ROMANIA&apos;,&apos;S145&apos;, 
&apos;RUSSIAN FED.&apos;,&apos;S094&apos;, 
&apos;SERBIA&apos;,&apos;S146&apos;, 
&apos;SLOVAKIA&apos;,&apos;S104&apos;, 
&apos;SLOVENIA&apos;,&apos;S147&apos;, 
&apos;SPAIN&apos;,&apos;S061&apos;, 
&apos;SWEDEN&apos;,&apos;S066&apos;, 
&apos;SWITZERLAND&apos;,&apos;S058&apos;, 
&apos;TURKEY&apos;,&apos;S112&apos;, 
&apos;UNITED KINGDOM&apos;,&apos;S064&apos;, 
&apos;UKRAINE&apos;,&apos;S094&apos;, 
&apos;SOUTH AFRICA&apos;,&apos;S076&apos;, 
&apos;MONTENEGRO&apos;,&apos;S146&apos;, 
&apos;LATVIA&apos;,&apos;S070&apos;, 
&apos;KOSOVO&apos;,&apos;S070&apos;, 
&apos;ICELAND&apos;,&apos;S066&apos;, 
&apos;ESTONIA&apos;,&apos;S070&apos;, 
&apos;&apos;)</formula>
        <name>Account: Update SAP Sales Org</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Account_Owner_To_Generic_DIB_ANZ</fullName>
        <description>Set Account Owner To Generic ANZ</description>
        <field>OwnerId</field>
        <lookupValue>genericanzdib@medtronic.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set Account Owner To Generic DIB ANZ</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Account_Owner_To_Generic_DIB_CAN</fullName>
        <description>Set Account Owner To Generic CAN</description>
        <field>OwnerId</field>
        <lookupValue>rs.mstcrm@medtronic.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set Account Owner To Generic DIB CAN</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Account%3A Notify Pending Account ANZ</fullName>
        <actions>
            <name>New_Pending_Account_ANZ</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pending Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>AUSTRALIA,NEW ZEALAND</value>
        </criteriaItems>
        <description>Send email to notify account update request when it has been filed by user in ANZ, using the &lt;request update&gt; button on account page. Mail is sent to the data steward</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Pending Account Adriatic</fullName>
        <actions>
            <name>New_Pending_Account_Adriatic_West</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pending Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>ALBANIA,BOSNIA-HERZ.,CROATIA,KOSOVO,SLOVENIA,MONTENEGRO,SERBIA,NORTH MACEDONIA</value>
        </criteriaItems>
        <description>Send email about pending account creation in Adriatic to local data steward.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Pending Account Belux</fullName>
        <actions>
            <name>Belux_New_Pending_Account</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pending Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>BELGIUM,LUXEMBOURG</value>
        </criteriaItems>
        <description>Send email about pending account creation to BeLux data stewards.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Pending Account Bulgaria</fullName>
        <actions>
            <name>New_Pending_Account_Bulgaria</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pending Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>BULGARIA</value>
        </criteriaItems>
        <description>Send email about pending account creation in Bulgaria to local data steward.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Pending Account CAN</fullName>
        <actions>
            <name>CAN_New_Pending_Account</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pending Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>CANADA</value>
        </criteriaItems>
        <description>Send email about pending account creation in Canada to local data steward.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Pending Account EMEA</fullName>
        <actions>
            <name>New_pending_account_created_in_Salesforce_com</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3 OR 4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pending Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>ANDORRA,ARUBA,AUSTRIA,BELGIUM,BOSNIA-HERZ.,BULGARIA,CANARY ISLANDS,CROATIA,CURAÇAO,CYPRUS,CZECH REPUBLIC,DENMARK,DUTCH ANTILLES,EGYPT,FAROE ISLANDS,FINLAND,FRENCH GUYANA,FRENCH POLYNES.</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>GREECE,GREENLAND,HUNGARY,ICELAND,IRAQ,IRELAND,ISRAEL,ITALY,LATVIA,LEBANON,LIECHTENSTEIN,LITHUANIA,LUXEMBOURG</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>MALTA,NETHERLANDS,NORTH MACEDONIA,NORWAY,PAKISTAN,POLAND,REUNION,ROMANIA,SAUDI ARABIA,SERBIA,SLOVAKIA,SLOVENIA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>SPAIN,SWEDEN,SWITZERLAND,TUNISIA,TURKEY,UKRAINE,UNITED KINGDOM,UTD.ARAB EMIR.,VATICAN CITY,ESTONIA,SAN MARINO</value>
        </criteriaItems>
        <description>Send email about pending account creation to EMEA data steward team. (excl. Germany, CEMA (Partly), KAZ, Russia)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Pending Account Emerging East</fullName>
        <actions>
            <name>New_Pending_Account_Emerging_East</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pending Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>UKRAINE,ARMENIA,BELARUS,GEORGIA</value>
        </criteriaItems>
        <description>Send email about pending account creation in Emerging East to local data steward.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Pending Account Europe</fullName>
        <actions>
            <name>New_pending_account_created_in_Salesforce_com</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3 OR 4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pending Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>DENMARK,DUTCH ANTILLES,CZECH REPUBLIC,ANDORRA,AUSTRIA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>FRANCE,NORWAY,ICELAND,NETHERLANDS,GREECE,IRELAND,FINLAND</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>SPAIN,SWEDEN,SWITZERLAND,PORTUGAL,GERMANY,UNITED KINGDOM,POLAND</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>ESTONIA,GREENLAND,ISRAEL,LATVIA,LITHUANIA,POLAND,SAN MARINO</value>
        </criteriaItems>
        <description>Send email about pending account creation in Europe to central data steward team (NOT ITALY, Belgium and Luxembourg)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Pending Account Germany</fullName>
        <actions>
            <name>Germany_New_Pending_Account</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pending Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>GERMANY</value>
        </criteriaItems>
        <description>Send email about pending account creation to local data steward.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Pending Account Hungary</fullName>
        <actions>
            <name>New_Pending_Account_Hungary</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pending Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>HUNGARY</value>
        </criteriaItems>
        <description>Send email about pending account creation in Hungary to local data steward.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Pending Account Italy</fullName>
        <actions>
            <name>Italy_New_Pending_Account</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pending Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>ITALY</value>
        </criteriaItems>
        <description>Send email about pending account creation to italian data stewards.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Pending Account KZK</fullName>
        <actions>
            <name>New_Pending_Account_KZK</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pending Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>AZERBAIJAN,KAZAKHSTAN,TURKMENISTAN,TAJIKISTAN,UZBEKISTAN,KIRGHIZIA</value>
        </criteriaItems>
        <description>Send email about pending account creation in KZK to local data steward.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Pending Account MEA</fullName>
        <actions>
            <name>New_Pending_Account_MEA</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pending Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>ALGERIA,BAHRAIN,CAMEROON,EGYPT,JORDAN,KUWAIT,LEBANON,MOROCCO,NIGERIA,PAKISTAN,SAUDI ARABIA,SENEGAL,SOUTH AFRICA,SYRIA,TUNISIA,DJIBOUTI,UTD.ARAB EMIR.</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>ANGOLA,BOTSWANA,CONGO,IVORY COAST,ETHIOPIA,GHANA,KENYA,LESOTHO,MADAGASCAR,MALAWI,MAURITIUS,MOZAMBIQUE,NAMIBIA,NIGERIA,RWANDA,SEYCHELLES,SWAZILAND,TANZANIA,UGANDA,ZAMBIA,ZIMBABWE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>CYPRUS</value>
        </criteriaItems>
        <description>Send email about pending account creation in MEA to central data steward team</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Pending Account MEA non-SAP</fullName>
        <actions>
            <name>New_Pending_Account_MEA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pending Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>ALGERIA,BAHRAIN,CAMEROON,JORDAN,KUWAIT,MOROCCO,NIGERIA,SENEGAL,SOUTH AFRICA,SYRIA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>ANGOLA,BOTSWANA,CONGO,DJIBOUTI,ETHIOPIA,GHANA,IVORY COAST,KENYA,LESOTHO,MADAGASCAR,MALAWI,MAURITIUS,MOZAMBIQUE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>NIGERIA,RWANDA,SEYCHELLES,SWAZILAND,TANZANIA,UGANDA,ZAMBIA,ZIMBABWE</value>
        </criteriaItems>
        <description>Send email about pending account creation in Africa to central data steward team (Non-SAP countries in the MEA region + South Africa)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Pending Account Romania Moldavia</fullName>
        <actions>
            <name>New_Pending_Account_Romania_Moldavia</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pending Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>MOLDAVIA,ROMANIA</value>
        </criteriaItems>
        <description>Send email about pending account creation in Romania Moldavia to local data steward.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Pending Account Russian Fed%2E</fullName>
        <actions>
            <name>New_Pending_Account_Russian_Fed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pending Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>RUSSIAN FED.</value>
        </criteriaItems>
        <description>Send email about pending account creation in Russian Fed. to local data steward.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Pending Account Turkey</fullName>
        <actions>
            <name>New_Pending_Account_Turkey</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pending Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>TURKEY</value>
        </criteriaItems>
        <description>Send email about pending account creation in Turkey to the data steward team</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Pending Account USA</fullName>
        <actions>
            <name>New_Pending_Account_USA</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pending Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>USA</value>
        </criteriaItems>
        <description>Send email about pending account creation in USA to local data steward.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Notify Requester When Pending Account is Approved</fullName>
        <actions>
            <name>Account_Notify_Requester_When_Account_is_Approved</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Users should be notified automatically when a pending account is approved [India]</description>
        <formula>AND( ISPICKVAL(Account_Country_vs__c, &apos;INDIA&apos;), RecordTypeId &lt;&gt;&apos;0122000000051mQ&apos;, PRIORVALUE(RecordTypeId ) = &apos;0122000000051mQ&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Update SAP Sales Org and Office</fullName>
        <actions>
            <name>Account_Update_SAP_Sales_Office</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Account_Update_SAP_Sales_Org</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Buying Entity,CAN Buying Group</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Buying_Group_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Update SAP Sales Org and SAP Sales Office for Buying Groups</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EUR DiB Pending Account Alert</fullName>
        <actions>
            <name>EUR_DiB_Pending_Account_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( CreatedBy.Autoflag_DiB_Account_fields__c = true, ISPICKVAL(CreatedBy.Company_Code_Text__c,&quot;EUR&quot;), RecordTypeId = &quot;0122000000051mQ&quot; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Account Owner To Generic DIB ANZ</fullName>
        <active>true</active>
        <booleanFilter>(1 OR 3 OR 4) AND 2</booleanFilter>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>AUSTRALIA,NEW ZEALAND</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Non-SAP Patient</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.PersonMailingCountry</field>
            <operation>equals</operation>
            <value>AUSTRALIA,NEW ZEALAND</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.SAP_Sales_Org__c</field>
            <operation>equals</operation>
            <value>S040,S041</value>
        </criteriaItems>
        <description>Set Account Owner To Generic DIB ANZ</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Account_Owner_To_Generic_DIB_ANZ</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Set Account Owner To Generic DIB CAN</fullName>
        <active>true</active>
        <booleanFilter>2 AND (1 OR 3 OR 7 OR ((4 OR 6) AND 5))</booleanFilter>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>CANADA,USA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Non-SAP Patient</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.PersonMailingCountry</field>
            <operation>equals</operation>
            <value>CANADA,USA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>FRANCE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Postal_Code__c</field>
            <operation>startsWith</operation>
            <value>971,972,973,974,975</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.PersonMailingCountry</field>
            <operation>equals</operation>
            <value>FRANCE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.SAP_Sales_Org__c</field>
            <operation>equals</operation>
            <value>S004</value>
        </criteriaItems>
        <description>Set Account Owner To Generic DIB CAN, for patient accounts in CAN and FRANCE part close to CAN</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Account_Owner_To_Generic_DIB_CAN</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Set Account Owner To Generic DIB EUR</fullName>
        <active>true</active>
        <booleanFilter>(1 OR 3) AND 2</booleanFilter>
        <criteriaItems>
            <field>Account.Account_Country_vs__c</field>
            <operation>equals</operation>
            <value>BELGIUM,IRELAND,NETHERLANDS,UNITED KINGDOM,GERMANY,FRANCE,DUTCH ANTILLES,ITALY,GREECE,NORWAY,SWITZERLAND,SWEDEN,DENMARK,SPAIN,PORTUGAL,AUSTRIA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Non-SAP Patient</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.PersonMailingCountry</field>
            <operation>equals</operation>
            <value>BELGIUM,IRELAND,NETHERLANDS,UNITED KINGDOM,GERMANY,FRANCE,DUTCH ANTILLES,ITALY,GREECE,NORWAY,SWITZERLAND,SWEDEN,DENMARK,SPAIN,PORTUGAL,AUSTRIA</value>
        </criteriaItems>
        <description>Set Account Owner To Generic DIB EUR, for patient accounts in Europe</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
