<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Therapy_Product_Concatenate</fullName>
        <field>Therapy_Product_Unique__c</field>
        <formula>Product__r.Id&amp;Therapy__r.Id</formula>
        <name>Therapy Product: Concatenate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Therapy Product Edited</fullName>
        <actions>
            <name>Therapy_Product_Concatenate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Therapy_Product__c.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
