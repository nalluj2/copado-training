<apex:page standardController="SVMXC__Service_Order__c" extensions="SVMX_SiteWorkOrderCreation">

    <!-- PAGE VARIABLES -->
    <apex:variable var="workOrder" value="{!SVMXC__Service_Order__c}"/>
    <apex:variable value="{!IF(sortType='ASC', 'background-position: 0 -16px;', 'background-position: 0 top;')}" var="sortIcon" id="sortIcon"/>

    <apex:form id="theForm">

        <!-- PAGE SECTION HEADER --> 
        <apex:sectionHeader title="Site Visit {!IF(workOrder.SVMX_PS_Is_Site_Visit__c, 'Modification', 'Creation')}" subtitle="{!workOrder.Name}"/>

        <!-- ERROR MESSAGES SECTION -->
        <apex:pageMessages id="pageMessages"/>

        <!-- STATUS PANEL -->
        <apex:actionStatus id="status">
            <apex:facet name="start">
                <div style="position: fixed; top: 0; left: 0; right: 0; bottom: 0; opacity: 0.25; z-index: 1000; background-color: black;">
                    &nbsp;
                </div>
                <div style="position: fixed; left: 0; top: 0; bottom: 0; right: 0; z-index: 1001; margin: 24% 44%">
                    <div style="display: inline-block; padding: 2px; background-color: #fff; width: 250px;">
                        <img src="/img/loading.gif" style="float: left; margin: 8px;" />
                        <span style="display: inline-block; padding: 10px 0px;">Processing, please wait...</span>
                    </div>
                </div>
            </apex:facet>
        </apex:actionStatus>

        <!-- SOURCE WORK ORDER SECTION -->
        <apex:pageBlock >
            <apex:pageBlockSection >
                <apex:outputField value="{!workOrder.SVMXC__Company__c}"/>
                <apex:outputField value="{!workOrder.SVMXC__Site__c}"/>
                <apex:outputField value="{!workOrder.SVMXC__Group_Member__c}"/>
                <apex:outputField value="{!workOrder.SVMXC__Order_Status__c}"/>
            </apex:pageBlockSection>
        </apex:pageBlock>

        <!-- CHILD WORK ORDERS SECTION -->
        <apex:pageBlock title="Related Work Orders" id="relatedWOs">
            <apex:pageBlockButtons >
                <apex:commandButton value="Apply Changes and Close" action="{!applyChangesAndClose}" status="status" rerender="pageMessages"/>
                <apex:commandButton value="Cancel" action="{!cancel}"/>
            </apex:pageBlockButtons>
            <apex:pageBlockTable value="{!relatedWorkOrders}" var="wo">
                <apex:column >
                    <apex:facet name="header">
                        <apex:inputCheckbox value="{!globalSelected}" onclick="checkUncheckAll(this, '{!relatedWorkOrders.size}');" id="checkAll"/>
                    </apex:facet>
                <apex:inputCheckbox value="{!wo.isSelected}" id="checkItem" onclick="checkUncheckItem(this, '{!relatedWorkOrders.size}')"/> 
                <apex:image value="/img/icon/tag16.gif" rendered="{!wo.isAlreadyLinked}" title="This Work Order is already linked to this Site Visit"/>                       
            </apex:column> 
                <apex:repeat value="{!woFields}" var="field">
                    <apex:column value="{!wo.workOrder[field]}">    
                        <apex:facet name="header">
                            <apex:commandLink action="{!onClickSortBy}" reRender="sortIcon,relatedWOs" status="status">
                                <apex:outputText value="{!$ObjectType.SVMXC__Service_Order__c.fields[field].label}"/>
                                <apex:image value="/img/s.gif" rendered="{!sortBy=field}" style="width: 11px; height: 11px; margin: 0 5px; vertical-align: -2px; background: transparent url(/img/alohaSkin/sortArrows_sprite.png) no-repeat; {!sortIcon}"/>
                                <apex:param name="sortBy" value="{!field}" assignTo="{!sortBy}"/>
                                <apex:param name="sortType" value="{!IF(sortBy=field, IF(sortType='ASC', 'DESC', 'ASC'), 'ASC')}" assignTo="{!sortType}"/>
						</apex:commandLink>
                        </apex:facet>
                    </apex:column> 
                </apex:repeat>
            </apex:pageBlockTable>
        </apex:pageBlock>

    </apex:form>

    <script>
        // checkAll  : thePage:theForm:theBlock:theBlockTable:checkAll
        // checkItem : thePage:theForm:theBlock:theBlockTable:0:checkItem
        function checkUncheckAll(checkboxAll, size) {
            var path = checkboxAll.id.replace(':checkAll', '');
            for(var i=0; i<parseInt(size); i++)
                if(document.getElementById(path + ':' + i + ':checkItem'))
                    document.getElementById(path + ':' + i + ':checkItem').checked = checkboxAll.checked;   
        }
        
        function checkUncheckItem(checkboxItem, size) {
            var path = checkboxItem.id.replace(':checkItem', '');
            path = path.substring(0, path.lastIndexOf(':'))
            document.getElementById(path + ':checkAll').checked = true;
            for(var i=0; i<parseInt(size); i++) {
                if(document.getElementById(path + ':' + i + ':checkItem') && !document.getElementById(path + ':' + i + ':checkItem').checked) {
                    document.getElementById(path + ':checkAll').checked = false;
                    return;
                } 
            }
        } 
        </script>    
</apex:page>