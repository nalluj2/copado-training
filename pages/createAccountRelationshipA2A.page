<apex:page standardController="Affiliation__c" extensions="ControllerCreateAccountRelationshipA2A" id="thePage">
	<apex:sectionHeader title="{!$Label.Relationship_Edit}" />
	<style>
        .textarea {
            width: 500px;
            height: 100px; 
        }
        .
    </style>
	<apex:outputpanel >
		<apex:actionstatus id="actStatusId">
			<apex:facet name="start">
				<div class="waitingSearchDiv" id="el_loading" style="background-color: #DCD6D6; height: 100%;opacity:0.65;width:100%;">
					<div class="waitingHolder" style="top: 74.2px; width: 91px;">
						<img class="waitingImage" src="/img/loading.gif"     title="Please Wait..." />
						<span class="waitingDescription">Updating...</span>
					</div>
				</div>
			</apex:facet>
		</apex:actionstatus>
	</apex:outputpanel>
	<!-- Start of PMT-13254 GPO Management -->
	<apex:form id="theForm">
        
        <apex:actionFunction name="changeRelationshipType" action="{!changeRelationshipType}" rerender="theForm" status="actStatusId"/>
    	<apex:actionFunction name="changeChildAccount" action="{!changeChildAccount}" rerender="theForm" status="actStatusId"/>
        <apex:actionFunction name="changeStartEndDate" action="{!changeStartEndDate}" reRender="theForm" status="actStatusId"/>
        
		<apex:pageBlock title="{!$Label.Relationship_Edit}" mode="edit" id="thePageBlock">
    		<apex:outputPanel id="hiddenpannel">
    			<apex:inputHidden value="{!sBUGSelectedOptions}" />
    			<apex:inputHidden value="{!sBUSelectedOptions}" />
    			<apex:inputHidden value="{!sSBUSelectedOptions}" />
    		</apex:outputPanel>
	
    		<apex:pageBlockButtons >
        		<apex:commandButton action="{!save}" value="{!$Label.Save_R}" disabled="{!inErrorState}" reRender="errors" /> 
	        	<apex:commandButton action="{!saveandnew}" disabled="{!inErrorState}" value="{!$Label.Save_New_R}" /> 
                <apex:commandButton action="{!cancelButton}" value="{!$Label.Cancel_R}" immediate="true" reRender="errors"/>
    		</apex:pageBlockButtons>
   
			<apex:outputPanel id="errors">
				<apex:pageMessages />        
			</apex:outputPanel>
    
			<apex:pageBlockSection title="{!$Label.Relationship_Information}" columns="2" id="thePageBlockSection" rendered="{!!inErrorState}">
		
				<apex:inputField required="true" value="{!Affiliation__c.Affiliation_From_Account__c}" id="fromAccount" onchange="changeChildAccount()"/>
				<apex:inputField required="true" value="{!Affiliation__c.Affiliation_Start_Date__c}" onchange="changeStartEndDate()"/>
        
        		<apex:pageBlockSectionItem id="thePageBlockSectionItem05">
            		<apex:outputLabel value="{!$ObjectType.Affiliation__c.fields.Affiliation_To_Account__c.label}" for="ParentAccount" />
					<apex:outputPanel styleClass="requiredInput" layout="block">
						<apex:outputPanel styleClass="requiredBlock" layout="block"/>                     
						<apex:inputField value="{!Affiliation__c.Affiliation_To_Account__c}" id="ParentAccount"/>		
        	    	</apex:outputPanel>  
	        	</apex:pageBlockSectionItem>
        
    	    	<apex:inputField required="false" value="{!Affiliation__c.Affiliation_End_Date__c}" onchange="changeStartEndDate()"/>
				<apex:inputField required="false" value="{!Affiliation__c.To_Grandparent__c}" id="grandParent" />
		
				<apex:pageBlockSectionItem id="thePageBlockSectionItem06">
					<apex:outputLabel value="{!$ObjectType.Affiliation__c.fields.Affiliation_Type__c.label}" id="relTypeLabel" for="relTypeList" /> 
                    <apex:selectList value="{!Affiliation__c.Affiliation_Type__c}" size="1" id="relTypeList" onchange="changeRelationshipType()">
                        <apex:selectOptions value="{!RelationshipTypes}" />
                    </apex:selectList>
				</apex:pageBlockSectionItem>
		
				<apex:inputField required="false" value="{!Affiliation__c.EUR_Member_ID_Text__c}"/>

				<apex:inputField value="{!Affiliation__c.Primary_Dealer__c}"/>
                
                <apex:pageBlockSection title="{!$Label.Dealer_Information}" columns="1"  Rendered="{!if($ObjectType.Affiliation__c.Fields['Comment__c'].Createable,true,false)}">
        
        		<apex:pageBlockSectionItem >
            		<apex:outputLabel value="{!$ObjectType.Affiliation__c.fields.Comment__c.label}" Rendered="{!$ObjectType.Affiliation__c.Fields['Comment__c'].Createable}" />
            		<apex:inputField required="false" value="{!Affiliation__c.Comment__c}" styleClass="textarea"/>
        		</apex:pageBlockSectionItem>
    
    </apex:pageBlockSection>
        
	    	</apex:pageBlockSection>
    
			<apex:pageBlockSection columns="1">
                
        		<apex:outputPanel id="buOuterInfoPanel" rendered="{!!inErrorState}">
            		<apex:outputPanel layout="block" id="buInfoPanel">
                        <table style="width : 100%; border : none;border: solid; border-width: 1px; border-radius: 7px; border-color: lightgray;" cellspacing="0" id="SBUTable">
							<tr style="height : 30px">
                    			<th style="text-align : center; border-bottom-width: 2px; border-bottom-style: solid; border-bottom-color : rgb(237, 237, 237)">Business Unit Group</th>
                    			<th style="text-align : center; border-bottom-width: 2px; border-bottom-style: solid; border-bottom-color : rgb(237, 237, 237)">Business Unit</th>
                    			<th style="text-align : center; border-bottom-width: 2px; border-bottom-style: solid; border-bottom-color : rgb(237, 237, 237)">Sub-Business Unit</th>
							</tr>
                            <apex:repeat value="{!bugs}" var="BUG" id="bugtable">
								<apex:outputText value="<tr>" escape="false" />
                                <td rowSpan="{!BUG.rowSpan}" style="vertical-align : middle; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color : rgb(237, 237, 237)">
                                <apex:inputCheckBox value="{!BUG.bSelected}" label="{!BUG.sName}" id="bug_selected" disabled="{!!bEnableSBUList || BUG.bDisable}">
                                    <apex:actionSupport event="onchange" action="{!bugSelected}" reRender="hiddenpannel,buOuterInfoPanel" status="actStatusId">
                                        <apex:param name="BUGName" value="{!BUG.sName}" assignTo="{!sChangedBusinessUnitGroup}"/>
                                        <apex:param name="sBUGSelectedOptions" value="{!sBUGSelectedOptions}" assignTo="{!sBUGSelectedOptions}"/>
                                        <apex:param name="sBUSelectedOptions" value="{!sBUSelectedOptions}" assignTo="{!sBUSelectedOptions}"/>
                                        <apex:param name="sSBUSelectedOptions" value="{!sSBUSelectedOptions}" assignTo="{!sSBUSelectedOptions}"/>
                                    </apex:actionSupport>
                                </apex:inputCheckBox>
                                    <apex:outputLabel value="{!BUG.sName}"/>
                        	</td>
                            <apex:repeat value="{!BUG.businessUnits}" var="BU" >
                            	<apex:outputText value="<tr>" escape="false" rendered="{!NOT(BU.firstRow)}"/>
									<td rowSpan="{!BU.rowSpan}" style="vertical-align : middle; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color : rgb(237, 237, 237)">
                                        <apex:inputCheckBox value="{!BU.bSelected}" label="{!BU.sName}" disabled="{!!bEnableSBUList || BU.bDisable}">
                                            <apex:actionSupport event="onchange" action="{!buSelected}" reRender="hiddenpannel,buOuterInfoPanel" status="actStatusId">
                                        		<apex:param name="BUName" value="{!BU.sName}" assignTo="{!sChangedBusinessUnit}"/>
                                        		<apex:param name="sBUGSelectedOptions" value="{!sBUGSelectedOptions}" assignTo="{!sBUGSelectedOptions}"/>
                                        		<apex:param name="sBUSelectedOptions" value="{!sBUSelectedOptions}" assignTo="{!sBUSelectedOptions}"/>
                                        		<apex:param name="sSBUSelectedOptions" value="{!sSBUSelectedOptions}" assignTo="{!sSBUSelectedOptions}"/>
                                    		</apex:actionSupport>
                                        </apex:inputCheckBox>
                                        <apex:outputLabel value="{!BU.sName}"/>
									</td>
									<apex:repeat value="{!BU.subBusinessUnits}" var="SBU">
										<apex:outputText value="<tr>" escape="false" rendered="{!NOT(SBU.firstRow)}"/>
											<td style="vertical-align : middle; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color : rgb(237, 237, 237)">							
                                                <apex:inputCheckBox value="{!SBU.bSelected}" label="{!SBU.sName}"  disabled="{!!bEnableSBUList || SBU.bDisable}">
                                                    <apex:actionSupport event="onchange" action="{!sbuSelected}" reRender="hiddenpannel,buOuterInfoPanel" status="actStatusId">
                                        				<apex:param name="SBUName" value="{!SBU.sName}" assignTo="{!sChangedSubBusinessUnit}"/>
                                        				<apex:param name="sBUGSelectedOptions" value="{!sBUGSelectedOptions}" assignTo="{!sBUGSelectedOptions}"/>
                                        				<apex:param name="sBUSelectedOptions" value="{!sBUSelectedOptions}" assignTo="{!sBUSelectedOptions}"/>
                                        				<apex:param name="sSBUSelectedOptions" value="{!sSBUSelectedOptions}" assignTo="{!sSBUSelectedOptions}"/>
                                    				</apex:actionSupport>
                                                </apex:inputCheckBox>
                                                <apex:outputLabel value="{!SBU.sName}"/>
											</td>
										<apex:outputLabel value="</tr>" escape="false" />
									</apex:repeat>
									<apex:outputLabel value="</tr>" escape="false" />
								</apex:repeat>
                            </apex:repeat>
                        </table>
            		</apex:outputPanel>
        		</apex:outputPanel>
    		</apex:pageBlockSection>
    		<!-- End of PMT-13254 GPO Management -->
    	</apex:pageBlock>
	</apex:form>
</apex:page>