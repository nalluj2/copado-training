<apex:page controller="ctrl_Support_Requests" showHeader="false">
    
    <title>SFDC Support Portal</title>
  
    <style>
        body{
            padding : 15px;
        }
    </style>
        
    <c:Support_Portal_Header application="BI" />
    
    <apex:pageMessage summary="Invalid session. You need to login first" escape="false" severity="error" strength="3" rendered="{!NOT(userLoggedIn)}"/>
    
    <apex:form rendered="{!userLoggedIn}">
        
        <apex:outputPanel layout="block" style="width : 100%; text-align : right">
            <apex:outputLabel value="{!session.uInfo.firstName} {!session.uInfo.lastName} ({!session.username})" style="margin-right : 5px;"/>|
            <apex:outputLink value="/apex/Support_Login" rendered="{!ISPICKVAL($User.UserType,'Guest')}" style="display : inline;margin-left : 5px;color : red; text-decoration : none;">logout</apex:outputLink>
            <apex:outputLink value="/home/home.jsp" rendered="{!NOT(ISPICKVAL($User.UserType,'Guest'))}" style="display : inline;margin-left : 5px;color : red; text-decoration : none;">logout</apex:outputLink>
        </apex:outputPanel>
        <br/>
            
        <apex:outputPanel layout="block" style="text-align:left;margin:10px;">
            <apex:commandLink value="< Back to main screen" action="{!goBack}" style="text-decoration : none;color:blue" />
        </apex:outputPanel>
                
        <apex:pageMessages />
        
        <apex:pageBlock title="Change User Setup" mode="mainDetail" id="mainBlock">
                        
            <apex:outputPanel layout="block" style="text-align:center">
                <apex:commandButton value="Submit" action="{!helper_Request_BI.createChangeUserRequest}" rendered="{!ISBLANK(session.request.Id)}"/>
                <apex:commandButton action="{!updateRequest}" value="Save" rendered="{!NOT(ISBLANK(session.request.Id))}"/>
                <apex:commandButton action="{!cancel}" value="Cancel Request" onclick="if(confirm('Are you sure?')==false) return false;"
                    rendered="{!AND(session.uInfo.email==session.request.Requestor_Email__c,session.request.Status__c=='New')}"/>
            </apex:outputPanel>

            <apex:pageBlockSection columns="1"> 
                                               
                <apex:outputField value="{!session.request.Status__c}" rendered="{!NOT(ISBLANK(session.request.Id))}"/>
                <apex:outputField value="{!session.request.Assignee__c}" rendered="{!NOT(ISBLANK(session.request.Id))}"/>
                <apex:outputField value="{!session.request.Name}" rendered="{!NOT(ISBLANK(session.request.Id))}"/>
                
                <apex:pageBlockSectionItem rendered="{!ISBLANK(session.request.Id)}">
                    <apex:outputLabel value="Medtronic username" />
                    <apex:outputPanel layout="inline">
                    	<apex:outputPanel layout="block" styleclass="requiredInput">                        
                        	<apex:outputPanel layout="block" styleclass="requiredBlock" />
                        	<apex:inputText value="{!helper_Request_BI.searchAlias}"/>
                        	<apex:commandButton value="Find" action="{!helper_Request_BI.searchByAlias}" />
                        </apex:outputPanel>
                    </apex:outputPanel>                 
                </apex:pageBlockSectionItem>
                
                <apex:outputField value="{!session.request.Firstname__c}"/>
                <apex:outputField value="{!session.request.Lastname__c}"/>
                <apex:outputField value="{!session.request.Email__c}"/>
            	
            	<apex:pageBlockSectionItem rendered="{!ISBLANK(session.request.Id)}">
                    <apex:outputLabel value="{!$ObjectType.Create_User_Request__c.Fields.Change_Type__c.Label}" />
                    <apex:outputPanel layout="block" styleclass="requiredInput">                        
                        <apex:outputPanel layout="block" styleclass="requiredBlock" />
                        <apex:inputField value="{!session.request.Change_Type__c}" >
                        	<apex:actionSupport event="onchange" reRender="mainBlock" />
                        </apex:inputField>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                <apex:outputField value="{!session.request.Change_Type__c}" rendered="{!NOT(ISBLANK(session.request.Id))}"/>
            	
                <apex:pageBlockSectionItem rendered="{!AND(ISBLANK(session.request.Id), NOT(ISBLANK(session.request.Change_Type__c)), session.request.Change_Type__c != 'Other')}">
                    <apex:outputLabel value="Application" />
                    <apex:outputpanel layout="block" styleClass="requiredInput">
                    	<apex:outputPanel layout="block" styleclass="requiredBlock" />
                        <apex:selectList value="{!session.request.BI_Content_Group__c}" size="1">
                            <apex:selectOptions value="{!helper_Request_BI.lstSO_ContentGroup}" />
                        </apex:selectList>
                        <i>This value defines which reports will become available for the user (Content Group)</i>
                    </apex:outputpanel>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem rendered="{!AND(NOT(ISBLANK(session.request.Id)), NOT(ISBLANK(session.request.Change_Type__c)), session.request.Change_Type__c != 'Other')}">
                    <apex:outputLabel value="Application" />
                    <apex:outputText value="{!session.request.BI_Content_Group__r.Value__c}" />
                </apex:pageBlockSectionItem>

                <apex:pageBlockSectionItem rendered="{!AND(ISBLANK(session.request.Id), NOT(ISBLANK(session.request.Change_Type__c)), session.request.Change_Type__c == 'Add/modify User Application')}">
                    <apex:outputLabel value="Capability Group" />
                    <apex:outputpanel layout="block" styleClass="requiredInput">
                    	<apex:outputPanel layout="block" styleclass="requiredBlock" />
                        <apex:selectList value="{!session.request.BI_Capability_Group__c}" size="1">
                            <apex:selectOptions value="{!helper_Request_BI.lstSO_CapabilityGroup}" />
                        </apex:selectList>
                        <i>This value defines which functionality will be activated for the user</i>
                    </apex:outputpanel>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem rendered="{!AND(NOT(ISBLANK(session.request.Id)), NOT(ISBLANK(session.request.Change_Type__c)), session.request.Change_Type__c == 'Add/modify User Application')}">
                    <apex:outputLabel value="Capability Group" />
                    <apex:outputText value="{!session.request.BI_Capability_Group__r.Value__c}" />
                </apex:pageBlockSectionItem>
				
				<apex:pageBlockSectionItem rendered="{!ISBLANK(session.request.Id)}" >
                	<apex:outputLabel value="Description" />
                	<apex:outputpanel layout="block" styleClass="requiredInput">
                    	<apex:outputPanel layout="block" styleclass="requiredBlock" rendered="{!AND(NOT(ISBLANK(session.request.Change_Type__c)), session.request.Change_Type__c == 'Other')}"/>
                		<apex:inputField value="{!session.request.Description_Long__c}" style="width:95%;"/>
                	</apex:outputpanel>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem rendered="{!NOT(ISBLANK(session.request.Id))}" >
                    <apex:outputLabel value="Description" />
                    <apex:outputField value="{!session.request.Description_Long__c}" style="width:95%"/>
                </apex:pageBlockSectionItem>

            </apex:pageBlockSection>
                
            <br />

            <apex:pageBlockSection columns="1"> 

                <apex:inputField value="{!session.comment.Comment__c}" rendered="{!AND(NOT(ISBLANK(session.request.Id)), NOT(ISNULL(session.comment)))}" style="width:95%;"/>

            </apex:pageBlockSection>

            <br />

            <apex:pageBlockSection title="Comments" columns="1" rendered="{!AND(NOT(ISNULL(session.comments)), session.comments.size>0)}" collapsible="false">
                                            
                <apex:pageBlockTable value="{!session.comments}" var="comment" >
                
                    <apex:column value="{!comment.CreatedDate}" />
                    <apex:column value="{!comment.From__c}" />
                    <apex:column value="{!comment.Comment__c}" style="width:70%" />                 
                </apex:pageBlockTable>
                
            </apex:pageBlockSection>   
            
        </apex:pageBlock>
    
    </apex:form>
</apex:page>