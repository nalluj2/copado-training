<apex:page standardController="Create_User_Request__c" extensions="ctrlExt_BI_Requests_Admin">

    <chatter:feedWithFollowers entityId="{!Create_User_Request__c.Id}"/>

    <apex:form rendered="{!Create_User_Request__c.Request_Type__c == 'Create User'}">       

        <apex:pageBlock title="Create User Management" mode="mainDetail" >
            <apex:outputPanel layout="block" style="text-align:center">               
                <apex:commandButton action="{!saveRequest}" value="Save"/>
                <apex:commandButton action="{!completeRequest}" value="Complete" rendered="{!AND(isAdminUser, Create_User_Request__c.Status__c=='In Progress', NOT(ISBLANK(Create_User_Request__c.Assignee__c)))}"/>                
                <apex:commandButton action="{!reject}" value="Reject" rendered="{!AND(isAdminUser, Create_User_Request__c.Status__c=='In Progress', NOT(ISBLANK(Create_User_Request__c.Assignee__c)))}"/>
                <apex:commandButton action="{!assignToMe}" value="Assign to me" rendered="{!AND(isAdminUser, Create_User_Request__c.Status__c!='Completed', Create_User_Request__c.Status__c!='Cancelled', Create_User_Request__c.Status__c!='Rejected')}"/>
            </apex:outputPanel>
            
            <apex:pageMessages escape="false"/>

            <apex:pageBlockSection columns="1">
                                                
                <apex:outputField value="{!Create_User_Request__c.Status__c}"/>              
                <apex:inputField value="{!Create_User_Request__c.Sub_Status__c}" rendered="{!AND(bIsUpdatable, bIsAssigned)}" />
                <apex:outputField value="{!Create_User_Request__c.Sub_Status__c}" rendered="{!OR(!bIsUpdatable, !bIsAssigned)}"/>
                <apex:outputField value="{!Create_User_Request__c.Sub_Status_Days__c} " />
                <apex:outputField value="{!Create_User_Request__c.Completed_Date__c}" rendered="{!Create_User_Request__c.Status__c == 'Completed'}"/>               

                <apex:outputField value="{!Create_User_Request__c.Assignee__c}" rendered="{!NOT(bIsUpdatable)}"/>
                <apex:pageBlockSectionItem rendered="{!bIsUpdatable}">
                    <apex:outputLabel value="{!$ObjectType.Create_User_Request__c.Fields.Assignee__c.Label}" />
                    <apex:selectList value="{!tAdminUserId}" size="1" >
                        <apex:selectOptions value="{!lstSO_AdminUser}" />
                    </apex:selectList>
                </apex:pageBlockSectionItem>

                <apex:inputField value="{!Create_User_Request__c.Time_Spent__c}" rendered="{!AND(bIsUpdatable, bIsAssigned)}" />
                <apex:outputField value="{!Create_User_Request__c.Time_Spent__c}" rendered="{!OR(!bIsUpdatable, !bIsAssigned)}" />
                <apex:outputField value="{!Create_User_Request__c.Application__c}"/>             
                <apex:outputField value="{!Create_User_Request__c.Name}"/>               
                <apex:outputField value="{!Create_User_Request__c.CreatedDate}" />
                <apex:outputField value="{!Create_User_Request__c.Requestor_Name__c}" />                            
                <apex:outputField value="{!Create_User_Request__c.Requestor_Email__c}" />
                                
                <br />

                <apex:outputField value="{!Create_User_Request__c.Alias__c}"/>                
                <apex:outputField value="{!Create_User_Request__c.Firstname__c}"/>                
                <apex:outputField value="{!Create_User_Request__c.Lastname__c}"/>               
                <apex:outputField value="{!Create_User_Request__c.Email__c}"/>                             
                <apex:outputField value="{!Create_User_Request__c.Business_Unit__c}"/>                               

                <apex:outputField value="{!Create_User_Request__c.Firstname_same_as_user__c}"/>
                <apex:outputField value="{!Create_User_Request__c.Lastname_same_as_user__c}"/>
                <apex:outputField value="{!Create_User_Request__c.Email_same_as_user__c}"/>


                <apex:outputField value="{!Create_User_Request__c.Company_Code__c}" />

                <apex:outputField value="{!Create_User_Request__c.User_Business_Unit_vs__c}" />
                <apex:outputField value="{!Create_User_Request__c.Primary_sBU__c}" />
                <apex:outputField value="{!Create_User_Request__c.Jobtitle_vs__c}" />
               
                <apex:outputField value="{!Create_User_Request__c.Geographical_Region_vs__c}" />    
                <apex:outputField value="{!Create_User_Request__c.Geographical_Subregion__c}" />    
                <apex:outputField value="{!Create_User_Request__c.Geographical_Country_vs__c}" />    

                <apex:outputField value="{!Create_User_Request__c.Region__c}" />    

                <br />

                <apex:outputField value="{!Create_User_Request__c.BI_Content_Group__r.Value__c}" label="Application (Content Group)"/>
                <apex:outputField value="{!Create_User_Request__c.BI_Capability_Group__r.Value__c}" label="Capability Group"/>

                <br />

                <apex:outputField value="{!Create_User_Request__c.Description_Long__c}"/>

                <br />

                <apex:inputField value="{!Create_User_Request__c.Internal_Notes__c}" rendered="{!bIsUpdatable}" style="width:95%;height:40px"/>
                <apex:outputField value="{!Create_User_Request__c.Internal_Notes__c}" rendered="{!!bIsUpdatable}" style="width:95%;height:40px"/>
                           
            </apex:pageBlockSection>
                        
        </apex:pageBlock>
    </apex:form>
            
    <apex:form rendered="{!Create_User_Request__c.Request_Type__c == 'De-activate User'}">      
        
        <apex:pageBlock title="De-activate User" mode="mainDetail" >
            
            <apex:outputPanel layout="block" style="text-align:center">
                <apex:commandButton action="{!saveRequest}" value="Save" rendered="{!bIsUpdatable}"/>
                <apex:commandButton action="{!completeRequest}" value="Complete" rendered="{!AND(isAdminUser, Create_User_Request__c.Status__c=='In Progress', NOT(ISBLANK(Create_User_Request__c.Assignee__c)))}"/>
                <apex:commandButton action="{!reject}" value="Reject" rendered="{!AND(isAdminUser, Create_User_Request__c.Status__c=='In Progress', NOT(ISBLANK(Create_User_Request__c.Assignee__c)))}"/>
                <apex:commandButton action="{!assignToMe}" value="Assign to me" rendered="{!AND(isAdminUser, Create_User_Request__c.Status__c!='Completed', Create_User_Request__c.Status__c!='Cancelled')}"/>                
            </apex:outputPanel>
            
            <apex:pageMessages escape="false"/>
            
            <apex:pageBlockSection columns="1"> 
                                
                <apex:outputField value="{!Create_User_Request__c.Status__c}"/>  
                <apex:inputField value="{!Create_User_Request__c.Sub_Status__c}" rendered="{!AND(bIsUpdatable, bIsAssigned)}" />
                <apex:outputField value="{!Create_User_Request__c.Sub_Status__c}" rendered="{!OR(!bIsUpdatable, !bIsAssigned)}"/>
                <apex:outputField value="{!Create_User_Request__c.Sub_Status_Days__c} " />

                <apex:outputField value="{!Create_User_Request__c.Assignee__c}" rendered="{!NOT(bIsUpdatable)}"/>
                <apex:pageBlockSectionItem rendered="{!bIsUpdatable}">
                    <apex:outputLabel value="{!$ObjectType.Create_User_Request__c.Fields.Assignee__c.Label}" />
                    <apex:selectList value="{!tAdminUserId}" size="1" >
                        <apex:selectOptions value="{!lstSO_AdminUser}" />
                    </apex:selectList>
                </apex:pageBlockSectionItem>

                <apex:inputField value="{!Create_User_Request__c.Time_Spent__c}" rendered="{!AND(bIsUpdatable, bIsAssigned)}" />
                <apex:outputField value="{!Create_User_Request__c.Time_Spent__c}" rendered="{!OR(!bIsUpdatable, !bIsAssigned)}" />
                <apex:outputField value="{!Create_User_Request__c.Application__c}"/> 
                <apex:outputField value="{!Create_User_Request__c.Completed_Date__c}" rendered="{!Create_User_Request__c.Status__c == 'Completed'}"/>           
                <apex:outputField value="{!Create_User_Request__c.Name}"/>
                <apex:outputField value="{!Create_User_Request__c.CreatedDate}" />
                <apex:outputField value="{!Create_User_Request__c.Requestor_Name__c}" />                
                <apex:outputField value="{!Create_User_Request__c.Requestor_Email__c}" />

                <br />

                <apex:outputField value="{!Create_User_Request__c.Firstname__c}"/>
                <apex:outputField value="{!Create_User_Request__c.Lastname__c}"/>
                <apex:outputField value="{!Create_User_Request__c.Email__c}"/>                       
                <apex:outputField value="{!Create_User_Request__c.Date_of_deactivation__c}" />                                                              
                <apex:outputField value="{!Create_User_Request__c.Deactivation_Reason__c}" />                                                              

                <br />

                <apex:inputField value="{!Create_User_Request__c.Internal_Notes__c}" rendered="{!bIsUpdatable}" style="width:95%;height:40px"/>
                <apex:outputField value="{!Create_User_Request__c.Internal_Notes__c}" rendered="{!!bIsUpdatable}" style="width:95%;height:40px"/>

            </apex:pageBlockSection>
        </apex:pageBlock>
    </apex:form>

    <apex:form rendered="{!Create_User_Request__c.Request_Type__c == 'Change User Setup'}">   
        
        <apex:pageBlock title="Change User Setup" mode="maindetail" >
            
            <apex:outputPanel layout="block" style="text-align:center"> 
                <apex:commandButton action="{!saveRequest}" value="Save" rendered="{!bIsUpdatable}"/>
                <apex:commandButton action="{!completeRequest}" value="Complete" rendered="{!AND(isAdminUser, Create_User_Request__c.Status__c=='In Progress', NOT(ISBLANK(Create_User_Request__c.Assignee__c)))}"/>
                <apex:commandButton action="{!reject}" value="Reject" rendered="{!AND(isAdminUser, Create_User_Request__c.Status__c=='In Progress', NOT(ISBLANK(Create_User_Request__c.Assignee__c)))}"/>
                <apex:commandButton action="{!assignToMe}" value="Assign to me" rendered="{!AND(isAdminUser, Create_User_Request__c.Status__c!='Completed', Create_User_Request__c.Status__c!='Cancelled')}"/>                                
            </apex:outputPanel>
            
            <apex:pageMessages escape="false"/>
            
            <apex:pageBlockSection columns="1" >
                                            
                <apex:outputField value="{!Create_User_Request__c.Status__c}" />
                <apex:inputField value="{!Create_User_Request__c.Sub_Status__c}" rendered="{!AND(bIsUpdatable, bIsAssigned)}" />
                <apex:outputField value="{!Create_User_Request__c.Sub_Status__c}" rendered="{!OR(!bIsUpdatable, !bIsAssigned)}"/>
                <apex:outputField value="{!Create_User_Request__c.Sub_Status_Days__c} " />

                <apex:outputField value="{!Create_User_Request__c.Assignee__c}" rendered="{!NOT(bIsUpdatable)}"/>
                <apex:pageBlockSectionItem rendered="{!bIsUpdatable}">
                    <apex:outputLabel value="{!$ObjectType.Create_User_Request__c.Fields.Assignee__c.Label}" />
                    <apex:selectList value="{!tAdminUserId}" size="1" >
                        <apex:selectOptions value="{!lstSO_AdminUser}" />
                    </apex:selectList>
                </apex:pageBlockSectionItem>

                <apex:inputField value="{!Create_User_Request__c.Time_Spent__c}" rendered="{!AND(bIsUpdatable, bIsAssigned)}" />
                <apex:outputField value="{!Create_User_Request__c.Time_Spent__c}" rendered="{!OR(!bIsUpdatable, !bIsAssigned)}" />
                <apex:outputField value="{!Create_User_Request__c.Application__c}"/>
                <apex:outputField value="{!Create_User_Request__c.Completed_Date__c}" rendered="{!Create_User_Request__c.Status__c == 'Completed'}"/>
                <apex:outputField value="{!Create_User_Request__c.Name}" />
                <apex:outputField value="{!Create_User_Request__c.CreatedDate}" />
                <apex:outputField value="{!Create_User_Request__c.Requestor_Name__c}" />
                <apex:outputField value="{!Create_User_Request__c.Requestor_Email__c}" />                   

                <br />

                <apex:outputField value="{!Create_User_Request__c.Firstname__c}"/>
                <apex:outputField value="{!Create_User_Request__c.Lastname__c}"/>
                <apex:outputField value="{!Create_User_Request__c.Email__c}"/>                     
                
                <apex:outputField value="{!Create_User_Request__c.Change_Type__c}" />
                <apex:outputField value="{!Create_User_Request__c.BI_Content_Group__r.Value__c}" label="Application (Content Group)"/>
                <apex:outputField value="{!Create_User_Request__c.BI_Capability_Group__r.Value__c}" label="Capability Group"/>
                <apex:outputField value="{!Create_User_Request__c.Description_Long__c}" />

                <br />

                <apex:inputField value="{!Create_User_Request__c.Internal_Notes__c}" rendered="{!bIsUpdatable}" style="width:95%;height:40px"/>
                <apex:outputField value="{!Create_User_Request__c.Internal_Notes__c}" rendered="{!!bIsUpdatable}" style="width:95%;height:40px"/>
            
            </apex:pageBlockSection>
        </apex:pageBlock>
    </apex:form>                   

    <apex:form rendered="{!Create_User_Request__c.Request_Type__c == 'Generic Service'}">

        <apex:pageBlock title="Generic Service" mode="maindetail" >
            
            <apex:outputPanel layout="block" style="text-align:center"> 
                <apex:commandButton action="{!saveRequest}" value="Save" rendered="{!bIsUpdatable}"/>
                <apex:commandButton action="{!completeRequest}" value="Complete" rendered="{!AND(isAdminUser, Create_User_Request__c.Status__c=='In Progress', NOT(ISBLANK(Create_User_Request__c.Assignee__c)))}"/>
                <apex:commandButton action="{!reject}" value="Reject" rendered="{!AND(isAdminUser, Create_User_Request__c.Status__c=='In Progress', NOT(ISBLANK(Create_User_Request__c.Assignee__c)))}"/>
                <apex:commandButton action="{!assignToMe}" value="Assign to me" rendered="{!AND(isAdminUser, Create_User_Request__c.Status__c!='Completed', Create_User_Request__c.Status__c!='Cancelled')}"/>                                
            </apex:outputPanel>
            
            <apex:pageMessages escape="false"/>
            
            <apex:pageBlockSection columns="1" >
                                            
                <apex:outputField value="{!Create_User_Request__c.Status__c}" />
                <apex:inputField value="{!Create_User_Request__c.Sub_Status__c}" rendered="{!AND(bIsUpdatable, bIsAssigned)}" />
                <apex:outputField value="{!Create_User_Request__c.Sub_Status__c}" rendered="{!OR(!bIsUpdatable, !bIsAssigned)}"/>
                <apex:outputField value="{!Create_User_Request__c.Sub_Status_Days__c} " />

                <apex:outputField value="{!Create_User_Request__c.Assignee__c}" rendered="{!NOT(bIsUpdatable)}"/>
                <apex:pageBlockSectionItem rendered="{!bIsUpdatable}">
                    <apex:outputLabel value="{!$ObjectType.Create_User_Request__c.Fields.Assignee__c.Label}" />
                    <apex:selectList value="{!tAdminUserId}" size="1" >
                        <apex:selectOptions value="{!lstSO_AdminUser}" />
                    </apex:selectList>
                </apex:pageBlockSectionItem>

                <apex:inputField value="{!Create_User_Request__c.Time_Spent__c}" rendered="{!AND(bIsUpdatable, bIsAssigned)}" />
                <apex:outputField value="{!Create_User_Request__c.Time_Spent__c}" rendered="{!OR(!bIsUpdatable, !bIsAssigned)}" />
                <apex:outputField value="{!Create_User_Request__c.Application__c}"/>
                <apex:outputField value="{!Create_User_Request__c.Completed_Date__c}" rendered="{!Create_User_Request__c.Status__c == 'Completed'}"/>
                <apex:outputField value="{!Create_User_Request__c.Name}" />
                <apex:outputField value="{!Create_User_Request__c.CreatedDate}" />
                <apex:outputField value="{!Create_User_Request__c.Requestor_Name__c}" />
                <apex:outputField value="{!Create_User_Request__c.Requestor_Email__c}" />                   

                <br />

                <apex:outputField value="{!Create_User_Request__c.Area_SR__c}" label="Request type"/>
                <apex:outputField value="{!Create_User_Request__c.Report_Type__c}"/> 

                <apex:outputField value="{!Create_User_Request__c.Region__c}"/>
                <apex:outputField value="{!Create_User_Request__c.Country__c}"/>
                <apex:outputField value="{!Create_User_Request__c.Business_Unit__c}"/>
                <apex:outputField value="{!Create_User_Request__c.BI_Content_Group__r.Value__c}" label="Application (Content Group)"/>
                
                <apex:outputField value="{!Create_User_Request__c.Description_Long__c}"/>

                <br />

                <apex:inputField value="{!Create_User_Request__c.Internal_Notes__c}" rendered="{!bIsUpdatable}" style="width:95%;height:40px"/>
                <apex:outputField value="{!Create_User_Request__c.Internal_Notes__c}" rendered="{!!bIsUpdatable}" style="width:95%;height:40px"/>
            
            </apex:pageBlockSection>
        </apex:pageBlock>

    
    </apex:form>


    <apex:relatedList list="comments__r" subject="{!Create_User_Request__c.Id}" />
    <apex:relatedList list="CombinedAttachments" subject="{!Create_User_Request__c.Id}" />

    <c:FieldHistory objectName="Create_User_Request__History" title="BI Request History" recordId="{!Create_User_Request__c.Id}" numberOfVisibleRecords="5" recordStep="5" />
    
</apex:page>