@isTest
private class Test_Case_UPSERT_BEFORE {
    
    
    private static testmethod void testPopulateInitialReporter(){
    	
		Account acc=new Account();
			acc.AccountNumber='103101';
			acc.Name='MNav House Account';
			acc.Account_Country_vs__c='USA';        
			acc.CurrencyIsoCode='USD';
			acc.Phone='24107954';
			acc.ST_NAV_Non_SAP_Account__c = true; //-BC - 20161101 - Added
		insert acc; 

		Contact objContact=new Contact();
			objContact.LastName='Saha';
			objContact.FirstName = 'test';
			objContact.AccountId = acc.id; 
			objContact.Contact_Department__c = 'Pediatric';
			objContact.Contact_Primary_Specialty__c = 'Neurology';
			objContact.Affiliation_To_Account__c = 'Employee';
			objContact.Primary_Job_Title_vs__c = 'Nurse';
			objContact.Contact_Gender__c = 'Female';
			objContact.Email = 'unit.test.email@example.com';
			objContact.MobilePhone = '+31 666 666 666';		
				
		Contact objContact2=new Contact();
			objContact2.LastName='Saho';
			objContact2.FirstName = 'test';
			objContact2.AccountId = acc.id; 
			objContact2.Contact_Department__c = 'Adult';
			objContact2.Contact_Primary_Specialty__c = 'Cardiology';
			objContact2.Affiliation_To_Account__c = 'Employee';
			objContact2.Primary_Job_Title_vs__c = 'Physician';
			objContact2.Contact_Gender__c = 'Male';
			objContact2.Email = 'unit.test.email2@example.com';
			objContact2.MobilePhone = '+31 666 666 666';
					
		insert new List<Contact>{objContact, objContact2};
   		      
		Asset oAsset_OARM = new Asset();
			oAsset_OARM.Name = 'Asset oarm';
			oAsset_OARM.Serial_Nr__c = 'XXXXX';
			oAsset_OARM.RecordTypeId = clsUtil.getRecordTypeByDevName('Asset', 'O_Arm').Id; 
			oAsset_OARM.Asset_Product_Type__c = 'O-Arm 1000';
			oAsset_OARM.AccountId = acc.Id;
			oAsset_OARM.Ownership_Status__c = 'EOL';		
		insert oAsset_OARM;
		
		//Exlude execution of OMA triggers
		clsUtil.bDoNotExecute = true;
		
		Test.startTest();
		
		Case objcase=new Case();
			objcase.Subject='abc';
			objcase.Description='abc';
			objcase.ContactId=objContact.id;
			objCase.Initial_Reporter__c = objContact.id; 
			objcase.AccountId=acc.Id;
			objcase.Complaint_Case__c = 'Yes';
			objcase.Date_Complaint_Became_Known__c = System.today();
			objcase.Complaint_Handling_Notified_Date__c = System.today();
			objcase.Patient_Date_of_Birth__c = system.today().AddYears(-45);
			objcase.Patient_Age_Units_US__c = 'YR';
			objcase.Patient_Weight_Units_US__c = 'LBS';
			objcase.AssetId=oAsset_OARM.Id;			
			objcase.Status='Open';
			objcase.Was_Navigation_Aborted__c = 'No';
			objcase.Serious_Injury__c = 'No';
			objcase.Case_Aborted__c ='Yes';
			objcase.Was_Medtronic_Imaging_Aborted__c = 'Yes';
			objcase.Patient_Info_Status__c = '3rd Attempt';
			objcase.Patient_Weight_US__c ='12';
			objcase.Patient_Gender_US__c = 'Male';
			objcase.Patient_Weight_US__c = '52';
			objcase.Patient_Age_US__c ='42';
			objcase.Patient_ID_US__c = '43';
			objcase.Event_Happen_During_Surgery__c ='Yes';
			objcase.Type_of_Surgical_Procedure__c ='Tumor resection';
			objcase.When_Issue_Occurred__c ='Post-op';
			objcase.Length_of_Extended_Surgical_Time__c ='Less than 1 hour';
			objcase.What_Software_Task_When_Issue_Occurred__c ='Register';
			objcase.Surgeon__c = objContact.id;
			objcase.System_Brought_Up__c = System.now();
			objcase.Subject = 'abc123';
			objcase.Description = 'abc123';
			objcase.PEI__c = 'Pending Information';
			objcase.Systems_Down__c = 'No';
			objcase.System_Down_Start__c = System.now() ;						      
			objcase.RecordTypeId = clsUtil.getRecordTypeByDevName('Case', 'New_Case').Id;        
		insert objcase; 
		
		objCase = [Select Initial_Reporter_Email__c, Initial_Reporter__c, Initial_Reporter_Phone__c, Initial_Reporter_Occupation__c from Case where Id = :objCase.Id];
		System.assert(objCase.Initial_Reporter_Email__c == 'unit.test.email@example.com');
		System.assert(objCase.Initial_Reporter_Phone__c == '+31 666 666 666');
		System.assert(objCase.Initial_Reporter_Occupation__c == 'Nurse');
		
		Complaint__c caseComplaint = [Select Initial_Reporter__c from Complaint__c where Case__c = :objCase.Id];
		System.assert(caseComplaint.Initial_Reporter__c == objCase.Initial_Reporter__c);
		
		Case_Logic.updateFlag = false;
		
		objCase.Initial_Reporter__c = objContact2.Id;
		update objCase;
		
		objCase = [Select Initial_Reporter_Email__c, Initial_Reporter__c, Initial_Reporter_Phone__c, Initial_Reporter_Occupation__c from Case where Id = :objCase.Id];
		System.assert(objCase.Initial_Reporter_Email__c == 'unit.test.email2@example.com');
		System.assert(objCase.Initial_Reporter_Phone__c == '+31 666 666 666');
		System.assert(objCase.Initial_Reporter_Occupation__c == 'Physician');
		
		caseComplaint = [Select Initial_Reporter__c from Complaint__c where Case__c = :objCase.Id];
		System.assert(caseComplaint.Initial_Reporter__c == objCase.Initial_Reporter__c);
    }
    
    private static testmethod void testComplaintCalculatedCase(){
    	  
		Account acc=new Account();
			acc.AccountNumber='103101';
			acc.Name='MNav House Account';
			acc.Account_Country_vs__c='USA';        
			acc.CurrencyIsoCode='USD';
			acc.Phone='24107954';
			acc.ST_NAV_Non_SAP_Account__c = true; //-BC - 20161101 - Added
		insert acc; 

		Contact objContact=new Contact();
			objContact.LastName='Saha';
			objContact.FirstName = 'test';
			objContact.AccountId = acc.id; 
			objContact.Contact_Department__c = 'Pediatric';
			objContact.Contact_Primary_Specialty__c = 'Neurology';
			objContact.Affiliation_To_Account__c = 'Employee';
			objContact.Primary_Job_Title_vs__c = 'Nurse';
			objContact.Contact_Gender__c = 'Female';
			objContact.Email = 'unit.test.email@example.com';
			objContact.MobilePhone = '+31 666 666 666';									
		insert objContact;
   		      
		Asset oAsset_OARM = new Asset();
			oAsset_OARM.Name = 'Asset oarm';
			oAsset_OARM.Serial_Nr__c = 'XXXXX';
			oAsset_OARM.RecordTypeId = clsUtil.getRecordTypeByDevName('Asset', 'O_Arm').Id; 
			oAsset_OARM.Asset_Product_Type__c = 'O-Arm 1000';
			oAsset_OARM.AccountId = acc.Id;
			oAsset_OARM.Ownership_Status__c = 'EOL';		
		insert oAsset_OARM;
		
		//Exlude execution of OMA triggers
		clsUtil.bDoNotExecute = true;
		
		Test.startTest();
		
		Case objcase=new Case();
			objcase.Subject='abc';
			objcase.Description='abc';
			objcase.ContactId=objContact.id;
			objCase.Initial_Reporter__c = objContact.id; 
			objcase.AccountId=acc.Id;
			objcase.Corrective_Maintenance_Repair__c = 'No';
			objcase.Customer_Communicate_Dissatisfaction__c = 'Yes';//This is enough to mark the Case as Complaint
			objcase.Patient_Impact__c = 'No';			
			objcase.Complaint_Case__c = 'No';			
			objcase.Patient_Date_of_Birth__c = system.today().AddYears(-45);
			objcase.Patient_Age_Units_US__c = 'YR';
			objcase.Patient_Weight_Units_US__c = 'LBS';
			objcase.AssetId=oAsset_OARM.Id;			
			objcase.Status='Open';
			objcase.Was_Navigation_Aborted__c = 'No';
			objcase.Serious_Injury__c = 'No';
			objcase.Case_Aborted__c ='Yes';
			objcase.Was_Medtronic_Imaging_Aborted__c = 'Yes';
			objcase.Patient_Info_Status__c = '3rd Attempt';
			objcase.Patient_Weight_US__c ='12';
			objcase.Patient_Gender_US__c = 'Male';
			objcase.Patient_Weight_US__c = '52';
			objcase.Patient_Age_US__c ='42';
			objcase.Patient_ID_US__c = '43';
			objcase.Event_Happen_During_Surgery__c ='Yes';
			objcase.Type_of_Surgical_Procedure__c ='Tumor resection';
			objcase.When_Issue_Occurred__c ='Post-op';
			objcase.Length_of_Extended_Surgical_Time__c ='Less than 1 hour';
			objcase.What_Software_Task_When_Issue_Occurred__c ='Register';
			objcase.Surgeon__c = objContact.id;
			objcase.System_Brought_Up__c = System.now();
			objcase.Subject = 'abc123';
			objcase.Description = 'abc123';
			objcase.PEI__c = 'Pending Information';
			objcase.Systems_Down__c = 'No';
			objcase.System_Down_Start__c = System.now() ;						      
			objcase.RecordTypeId = clsUtil.getRecordTypeByDevName('Case', 'New_Case').Id;        
		
		Boolean isError = false;
		
		try{
			
			insert objcase;
			
		}catch(Exception e){
			
			System.assert(e.getMessage().contains('Based on entered data this Case will be marked as a Complaint Case.'));			
			isError = true;
		}
    	
    	System.assert(isError == true);
    	
    	objcase.Date_Complaint_Became_Known__c = Date.today();
    	objcase.Complaint_Handling_Notified_Date__c = Date.today();
    	insert objcase;
    	
    	objcase = [Select Id, Complaint_Case__c from Case where Id = :objcase.Id];
    	
    	System.assert(objcase.Complaint_Case__c == 'Yes');
    	
    }
}