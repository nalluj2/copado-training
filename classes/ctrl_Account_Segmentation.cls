public with sharing class ctrl_Account_Segmentation {
    
    @AuraEnabled
    public static UserSegmentationOverview getUserOverview() {
		
		try{
		
			UserSegmentationOverview result = new UserSegmentationOverview();
			
			//Potential
			List<UMD_Purpose__c> potentialPurpose = [Select Id, Fiscal_Year_VS__c from UMD_Purpose__c where Segmentation_Qualification__c = 'Potential' and Status__c = 'Open for input'];
			
			if(potentialPurpose.size() == 1){
				
				//Input	
				List<Segmentation_Potential_Input__c> inputs  = [Select Id from Segmentation_Potential_Input__c where UMD_Purpose__c = :potentialPurpose[0].Id AND Assigned_To__c = :UserInfo.getUserId() LIMIT 1];
							
				if(inputs.size() > 0) result.hasPotentialInput = true;
				else result.hasPotentialInput = false;
				
				//Approval			
				List<Segmentation_Potential_Input__c> approvalInputs  = [Select Id from Segmentation_Potential_Input__c where UMD_Purpose__c = :potentialPurpose[0].Id AND (Approver_1__c = :UserInfo.getUserId() OR Approver_2__c = :UserInfo.getUserId()) LIMIT 1];
							
				if(approvalInputs.size() > 0) result.hasPotentialApproval = true;
				else result.hasPotentialApproval = false;		
			}		
			
			//Behavioral		
			List<UMD_Purpose__c> behavioralPurpose = [Select Id, Fiscal_Year_VS__c from UMD_Purpose__c where Segmentation_Qualification__c = 'Behavioral' and Status__c = 'Open for input'];
			
			if(behavioralPurpose.size() == 1){
				
				//Input			
				List<PicklistValue> behavioralOptions = new List<PicklistValue>();
				
				List<AggregateResult> inputs  = [Select Sub_Business_Unit__c from UMD_Input_Complete__c where UMD_Purpose__c = :behavioralPurpose[0].Id AND Assigned_To__c = :UserInfo.getUserId() GROUP BY Sub_Business_Unit__c ORDER BY Sub_Business_Unit__c];
				
				for(AggregateResult input : inputs){
	    				
					String filterValue = String.valueOf(input.get('Sub_Business_Unit__c'));
					
					behavioralOptions.add(new PicklistValue(filterValue, filterValue));									
				}
				
				if(behavioralOptions.size() > 0) result.behavioralOptions = behavioralOptions;
				
				//Approval
				List<PicklistValue> behavioralApprovalOptions = new List<PicklistValue>();
				
				List<AggregateResult> approveInputs  = [Select Sub_Business_Unit__c from UMD_Input_Complete__c where UMD_Purpose__c = :behavioralPurpose[0].Id AND (Approver_1__c = :UserInfo.getUserId() OR Approver_2__c = :UserInfo.getUserId()) GROUP BY Sub_Business_Unit__c ORDER BY Sub_Business_Unit__c];
				
				for(AggregateResult input : approveInputs){
	    				
					String filterValue = String.valueOf(input.get('Sub_Business_Unit__c'));
					
					behavioralApprovalOptions.add(new PicklistValue(filterValue, filterValue));									
				}
								
				if(behavioralApprovalOptions.size() > 0) result.behavioralApprovalOptions = behavioralApprovalOptions;
			}
			
			//Strategic
			List<UMD_Purpose__c> strategicPurpose = [Select Id, Fiscal_Year_VS__c from UMD_Purpose__c where Segmentation_Qualification__c = 'Strategical' and Status__c = 'Open for input'];
			
			if(strategicPurpose.size() == 1){
				
				//Input						
				List<PicklistValue> strategicOptions = new List<PicklistValue>();
				
				List<AggregateResult> inputs  = [Select Business_Unit_Group__c from UMD_Input_Complete__c where UMD_Purpose__c = :strategicPurpose[0].Id AND Assigned_To__c = :UserInfo.getUserId() GROUP BY Business_Unit_Group__c ORDER BY Business_Unit_Group__c];
				
				for(AggregateResult input : inputs){
	    				
					String filterValue = String.valueOf(input.get('Business_Unit_Group__c'));
					
					strategicOptions.add(new PicklistValue(filterValue, filterValue));									
				}
				
				if(strategicOptions.size() > 0) result.strategicOptions = strategicOptions;
				
				//Approval
				List<PicklistValue> strategicApprovalOptions = new List<PicklistValue>();
				
				List<AggregateResult> approvalInputs  = [Select Business_Unit_Group__c from UMD_Input_Complete__c where UMD_Purpose__c = :strategicPurpose[0].Id AND (Approver_1__c = :UserInfo.getUserId() OR Approver_2__c = :UserInfo.getUserId()) GROUP BY Business_Unit_Group__c ORDER BY Business_Unit_Group__c];
				
				for(AggregateResult input : approvalInputs){
	    				
					String filterValue = String.valueOf(input.get('Business_Unit_Group__c'));
					
					strategicApprovalOptions.add(new PicklistValue(filterValue, filterValue));									
				}
								
				if(strategicApprovalOptions.size() > 0) result.strategicApprovalOptions = strategicApprovalOptions;
			}		
			
			//Admin Permission Set
			
			List<PermissionSetAssignment> assignment = [Select Id from PermissionSetAssignment where AssigneeId = :UserInfo.getUserId() AND PermissionSet.Name = 'Segmentation_GTM_Super_User'];
			if(assignment.size() > 0) result.isAdmin = true;
			else result.isAdmin = false;	
			
			return result;
		
		}catch(Exception e){
    	
    		System.debug(e.getMessage() + ' | ' + e.getStackTraceString());
    		AuraHandledException auraEx = new AuraHandledException(e.getMessage()); 
    		if(test.isRunningTest()) auraEx.setMessage(e.getMessage());
    		throw auraEx;
    	}
    }
    
    public class UserSegmentationOverview{
    	
    	@AuraEnabled public List<PicklistValue> strategicOptions {get; set;}    	
    	@AuraEnabled public List<PicklistValue> strategicApprovalOptions {get; set;}
    	@AuraEnabled public List<PicklistValue> behavioralOptions {get; set;}
    	@AuraEnabled public List<PicklistValue> behavioralApprovalOptions {get; set;}
    	@AuraEnabled public Boolean hasPotentialInput {get; set;}    	
    	@AuraEnabled public Boolean hasPotentialApproval {get; set;}
    	@AuraEnabled public Boolean isAdmin {get; set;}    	
    }
    
    public class PicklistValue{
    	
    	@AuraEnabled public String label {get; set;}
    	@AuraEnabled public String value {get; set;}
    	
    	public PicklistValue(String inputValue, String inputLabel){
    		
    		label = inputLabel;
    		value = inputValue;
    	}
    }
}