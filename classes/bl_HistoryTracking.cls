//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 16-08-2017
//  Description      : APEX Class - Business Logic for History_Tracking__c
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public with sharing class bl_HistoryTracking {

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// Create a History_Tracking__c records based on the input parameters:
	//	- tSObjectName = API name of the Object for which we are creating the History_Tracking__c record
	//	- tParentField = API Name of the field on History_Tracking__c which is a reference (lookup) to the changed record
	//	- oRecordNew = sObject which represents the changed record
	//	- oRecordOld = sObject which represents the old version of the changed record
	//	- setFieldName = contains the fieldnames that we need to validated if there is a change.  For each changed field we create a History_Tracking__c record
	//	- mapRelatedData = Contains the related data which will be used to replace the Id with a String (eg. the name) so that we don't need to log the ID if a lookup (reference) field changes
	//	- bInsert = do we need to insert the records or not?
	//---------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public static List<History_Tracking__c> createHistoryTracking(String tSObjectName, String tParentField, sObject oRecordOld, sObject oRecordNew, Set<String> setFieldName, Map<Id, String> mapRelatedData, Boolean bInsert){

		List<History_Tracking__c> lstHistoryTracking = new List<History_Tracking__c>();

		if (oRecordOld == null){

			// Created Record
			History_Tracking__c oHistoryTracking = new History_Tracking__c();
				oHistoryTracking.put(tParentField, oRecordNew.Id);
				oHistoryTracking.FieldLabel__c = 'Created';
			lstHistoryTracking.add(oHistoryTracking);

		}else{

			Map<String, Schema.DescribeFieldResult> mapFieldDescribe = clsUtil.getFieldDescribeForSobject(tSObjectName);

			// Updated Record
			for (String tFieldName : setFieldName){

				try{

					if (Test.isRunningTest()) clsUtil.bubbleException('bl_HistoryTracking.createHistoryTracking_EXCEPTION');

					if (oRecordNew.get(tFieldName) != oRecordOld.get(tFieldName)){
		
						Schema.DescribeFieldResult oFieldDescribeResult;
						if (mapFieldDescribe.containsKey(tFieldName.toLowerCase())){
							oFieldDescribeResult = mapFieldDescribe.get(tFieldName.toLowerCase());	
						} 

						if (oFieldDescribeResult != null){

							History_Tracking__c oHistoryTracking = new History_Tracking__c();
								oHistoryTracking.put(tParentField, oRecordNew.Id);
								oHistoryTracking.FieldLabel__c = oFieldDescribeResult.getLabel();
								oHistoryTracking.FieldName__c = tFieldName;
								oHistoryTracking.FieldType__c = String.valueOf(oFieldDescribeResult.getType());
								if (oHistoryTracking.FieldType__c == 'Date'){

									if (oRecordOld.get(tFieldName) != null) oHistoryTracking.OldValue__c = Date.valueOf(oRecordOld.get(tFieldName)).format();
									if (oRecordNew.get(tFieldName) != null) oHistoryTracking.NewValue__c = Date.valueOf(oRecordNew.get(tFieldName)).format();

								}else if (oHistoryTracking.FieldType__c == 'DateTime'){

									if (oRecordOld.get(tFieldName) != null)  oHistoryTracking.OldValue__c = Datetime.valueOf(oRecordOld.get(tFieldName)).format();
									if (oRecordNew.get(tFieldName) != null) oHistoryTracking.NewValue__c = Datetime.valueOf(oRecordNew.get(tFieldName)).format();

								}else if (oHistoryTracking.FieldType__c == 'Reference'){

									if (oRecordOld.get(tFieldName) != null)  oHistoryTracking.OldValue__c = String.valueOf(oRecordOld.get(tFieldName));
									if (oRecordNew.get(tFieldName) != null) oHistoryTracking.NewValue__c = String.valueOf(oRecordNew.get(tFieldName));

									if (mapRelatedData != null){
										if (mapRelatedData.containsKey(String.valueOf(oRecordOld.get(tFieldName)))){
											oHistoryTracking.OldValue__c = mapRelatedData.get(String.valueOf(oRecordOld.get(tFieldName)));
										}
										if (mapRelatedData.containsKey(String.valueOf(oRecordNew.get(tFieldName)))){
											oHistoryTracking.NewValue__c = mapRelatedData.get(String.valueOf(oRecordNew.get(tFieldName)));
										}
									}

								}else{

									String tOldValue = String.valueOf(oRecordOld.get(tFieldName));
									String tNewValue = String.valueOf(oRecordNew.get(tFieldName));

									if (!String.isBlank(tOldValue) && tOldValue.length() > 255) tOldValue = tOldValue.left(250) + ' ...';
									if (!String.isBlank(tNewValue) && tNewValue.length() > 255) tNewValue = tNewValue.left(250) + ' ...';

									oHistoryTracking.OldValue__c = tOldValue;
									oHistoryTracking.NewValue__c = tNewValue;

								}

							lstHistoryTracking.add(oHistoryTracking);

						}

					}

				}catch(Exception oEX){
					clsUtil.debug('Error in bl_HistoryTracking.createHistoryTracking on line ' + oEX.getLineNumber() + ' : ' + oEX.getMessage());
				}

			}

		}

		if (bInsert && lstHistoryTracking.size() > 0){
			insert lstHistoryTracking;
		}

		return lstHistoryTracking;

	}
	//---------------------------------------------------------------------------------------------------------------------------------------------------------------------


	//---------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// Load the related data for a History Tracking record so that we can log a (eg.) name instead of an ID if a Lookup (Reference) field changes
	//---------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public static Map<Id, String> loadRelatedData(String tReferenceTo, String tReferenceToField, List<sObject> lstRecordNew, Map<Id, sObject> mapRecordOld, String tFieldName){

		Map<Id, String> mapRelatedData = new Map<Id, String>();

		Set<Id> setID_RelatedRecord = new Set<Id>();
		for (sObject oRecordNew : lstRecordNew){

			if ( (mapRecordOld != null) && (mapRecordOld.containsKey(oRecordNew.Id)) ){
				sObject oRecordOld = mapRecordOld.get(oRecordNew.Id);

				if (oRecordNew.get(tFieldName) != oRecordOld.get(tFieldName)){
					setID_RelatedRecord.add(String.valueOf(oRecordNew.get(tFieldName)));
					setID_RelatedRecord.add(String.valueOf(oRecordOld.get(tFieldName)));
				}
			}
		}

		if (setID_RelatedRecord.size() > 0){

			String tSOQL = 'SELECT Id,' + tReferenceToField + ' FROM ' + tReferenceTo + ' WHERE Id in :setID_RelatedRecord';
			Map<Id, sObject> mapData = new Map<Id, sObject>((List<sObject>)Database.query(tSOQL));

			for (sObject oRecordNew : lstRecordNew){

				try{

					if (Test.isRunningTest()) clsUtil.bubbleException('bl_HistoryTracking.loadRelatedData_EXCEPTION');

					Id id_RecordNew = String.valueOf(oRecordNew.get('Id'));
					Id id_RelatedRecordNew = String.valueOf(oRecordNew.get(tFieldName));

					sObject oRecordOld = mapRecordOld.get(id_RecordNew);
					Id id_RelatedRecordOld = String.valueOf(oRecordOld.get(tFieldName));

					if ( mapData.containsKey(String.valueOf(id_RelatedRecordNew)) ){
						mapRelatedData.put(id_RelatedRecordNew, String.valueOf(mapData.get(id_RelatedRecordNew).get(tReferenceToField)));
					}

					if ( mapData.containsKey(String.valueOf(id_RelatedRecordOld)) ){
						mapRelatedData.put(id_RelatedRecordOld, String.valueOf(mapData.get(id_RelatedRecordOld).get(tReferenceToField)));
					}

				}catch(Exception oEX){
					clsUtil.debug('Error in bl_HistoryTracking.loadRelatedData on line ' + oEX.getLineNumber() + ' : ' + oEX.getMessage());
				}

			}
						
		}


		return mapRelatedData;

	}
	//---------------------------------------------------------------------------------------------------------------------------------------------------------------------

}
//--------------------------------------------------------------------------------------------------------------------