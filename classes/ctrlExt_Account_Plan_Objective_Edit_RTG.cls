public with sharing class ctrlExt_Account_Plan_Objective_Edit_RTG {
	
	private Account_Plan_Objective__c objective;
	private Account_Plan_2__c accPlan;
	
	public Business_Objective__c businessObjective {get; set;}
	public String businessObjectiveFilter {get; set;}
	
	public Boolean isDiabetesDepartment {get; set;}
	
	private Id recordTypeRTG;

	private Set<String> setRecordtypeDevName_AccountPlanRTG = new Set<String>{'EUR_RTG_BUG','EUR_RTG_BU','EUR_RTG_sBU','MEA_RTG_BU','MEA_RTG_sBU'};
		
	public ctrlExt_Account_Plan_Objective_Edit_RTG (ApexPages.StandardController sc){
		
		if( !Test.isRunningTest()){
			sc.addFields(new List<String>{'Business_Objective__c', 'RecordTypeId'});
		}
		
		recordTypeRTG = RecordTypeMedtronic.getRecordTypeByDevName('Account_Plan_Objective__c' , 'RTG').Id;
		
		objective = (Account_Plan_Objective__c) sc.getRecord();
		
		if(objective.Id == null){
									
			objective.RecordTypeId = recordTypeRTG;
		}
		
		if(objective.Account_Plan__c == null){
			
			ApexPages.addMessage(new ApexPages.Message( ApexPages.Severity.Error, 'Account Plan Objective can only be created from an Account Plan page'));
			return;
		}
							
		accPlan = [Select Id, Account_Plan_Level__c, Business_Unit__c, Sub_Business_Unit__c, Business_Unit_Group__c, RecordType.DeveloperName, Account__r.Account_Country_vs__c from Account_Plan_2__c where Id = :objective.Account_Plan__c];
		
		if(accPlan.Account_Plan_Level__c == 'Business Unit Group'){
			
			businessObjectiveFilter = ' WHERE Business_Unit_Group__c = \''+ accPlan.Business_Unit_Group__c + '\' AND Active__c = true ';
			
		}else if(accPlan.Account_Plan_Level__c == 'Business Unit'){
			
			businessObjectiveFilter = ' WHERE Business_Unit__c = \''+ accPlan.Business_Unit__c + '\' AND Active__c = true ';

		}else if(accPlan.Account_Plan_Level__c == 'Sub Business Unit'){
			
			businessObjectiveFilter = ' WHERE Sub_Business_Unit__c = \''+ accPlan.Sub_Business_Unit__c + '\' AND Active__c = true ';
		}
		
		if(objective.Business_Objective__c != null) businessObjectiveSelected();	
		
		if((accPlan.RecordType.DeveloperName == 'EUR_DIB_sBU' || accPlan.RecordType.DeveloperName == 'MEA_DIB_sBU') && accPlan.Account__r.Account_Country_vs__c.toUpperCase() != 'CANADA') isDiabetesDepartment = true;
		else isDiabetesDepartment = false;
	}
	
	public void businessObjectiveSelected(){
		
		businessObjective = null;
				
		if(objective.Business_Objective__c != null){
			
			List<Business_Objective__c> businessObjectives = Database.query('Select Id, Explanation__c, Type__c, Target_Date__c from Business_Objective__c ' + businessObjectiveFilter + ' AND Id = \'' + objective.Business_Objective__c + '\'');
			
			if(businessObjectives.size() > 0) businessObjective = businessObjectives[0];
			else objective.Business_Objective__c = null;
		}	
	}
	
	public void targetDateToCurrentFY(){
		
		DIB_Fiscal_Period__c currentPeriod = [SELECT Fiscal_Year__c FROM DIB_Fiscal_Period__c where Start_Date__c <= TODAY AND End_Date__c >= TODAY];
		
		String currentYear = currentPeriod.Fiscal_Year__c;
		
		DIB_Fiscal_Period__c lastPeriodOfYear = [SELECT End_Date__c FROM DIB_Fiscal_Period__c where Fiscal_Year__c = :currentYear AND Fiscal_Month__c = '12'];
		
		objective.Target_Date__c = lastPeriodOfYear.End_Date__c;
	}
	
	public PageReference save(){
		
		if(objective.Business_Objective__c == null){
			ApexPages.addMessage(new ApexPages.Message( ApexPages.Severity.Error, 'Required field is missing: Objective'));
			return null;
		}
		
		if(isDiabetesDepartment && (objective.Department__c == null || objective.Department__c == '')){
			ApexPages.addMessage(new ApexPages.Message( ApexPages.Severity.Error, 'Required field is missing: Department'));
			return null;
		}
		
		//- Bart Caelen - 20161125 - CR-12913 - START
		Boolean bValidateDuplicate = true;
		if (setRecordtypeDevName_AccountPlanRTG.contains(accPlan.RecordType.DeveloperName)){
			// if Business Objective is "Qualitative" we don't need to check for duplicates
			if (businessObjective.Type__c == 'Qualitative'){
				bValidateDuplicate = false;
			}
		}	
		if (bValidateDuplicate){
			String dupeQuery = 'Select Id from Account_Plan_Objective__c where Account_Plan__c = \'' + objective.Account_Plan__c + '\' AND Business_Objective__c = \'' + objective.Business_Objective__c + '\'  AND Type__c != \'Qualitative\'';
			
			if(isDiabetesDepartment) dupeQuery += ' AND Department__c = \'' + objective.Department__c + '\'';
			if(objective.Id != null) dupeQuery += ' AND Id != \'' + objective.Id + '\'';
			
			List<Account_Plan_Objective__c> dupes = Database.query(dupeQuery);		
			
			if(dupes.size() > 0){
				ApexPages.addMessage(new ApexPages.Message( ApexPages.Severity.Error, 'This Objective is already linked to the selected Account Plan. Please click <Cancel> and update the existing Account Plan Objective for this Objective'));
				return null;
			}
		}
		//- Bart Caelen - 20161125 - CR-12913 - STOP
		
		try{
			
			upsert objective;
			
		}catch(Exception e){
			
			ApexPages.addMessages(e);
			return null;
		}
		
		PageReference pr = new PageReference('/'+accPlan.Id);
		pr.setRedirect(true);
		
		return pr;
	}
	
	public PageReference cancel(){
		
		PageReference pr = new PageReference('/'+accPlan.Id);
		pr.setRedirect(true);
		
		return pr;
	}
}