@isTest
private class Test_Customer_Segmentation {
    
    @TestSetup
    private static void createTestData(){
    	
    	Company__c eur = new Company__c();		
        eur.Name = 'Europe';
        eur.CurrencyIsoCode = 'EUR';
        eur.Current_day_in_Q1__c = 56;
        eur.Current_day_in_Q2__c = 34; 
        eur.Current_day_in_Q3__c = 5; 
        eur.Current_day_in_Q4__c =  0;   
        eur.Current_day_in_year__c = 200;
        eur.Days_in_Q1__c = 56;  
        eur.Days_in_Q2__c = 34;
        eur.Days_in_Q3__c = 13;
        eur.Days_in_Q4__c = 22;
        eur.Days_in_year__c = 250;
        eur.Company_Code_Text__c = 'EUR';
		insert eur;
		
		Business_Unit_Group__c bug =  new Business_Unit_Group__c();
        bug.Master_Data__c = eur.id;
        bug.Abbreviated_Name__c = 'CVG';
        bug.Name = 'CVG';	                
        insert bug;
		
		Business_Unit__c bu =  new Business_Unit__c();
        bu.Company__c = eur.id;
        bu.Name = 'Vascular';
        bu.Business_Unit_Group__c = bug.Id;
        bu.Account_Plan_Activities__c = true;        
        insert bu;
        
        Sub_Business_Units__c sbuPV = new Sub_Business_Units__c();
        sbuPV.Name = 'Coro + PV';
        sbuPV.Business_Unit__c = bu.id;
        
        Sub_Business_Units__c sbuAortic = new Sub_Business_Units__c();
        sbuAortic.Name = 'Aortic';
        sbuAortic.Business_Unit__c = bu.id;
        
		insert new List<Sub_Business_Units__c>{sbuPV, sbuAortic};
    	
    	UMD_Purpose__c currentBehavioral = new UMD_Purpose__c();
    	currentBehavioral.Fiscal_Year_VS__c = 'FY2021';
    	currentBehavioral.Segmentation_Qualification__c = 'Behavioral';
    	currentBehavioral.Status__c = 'Open for input';
    	currentBehavioral.Name = 'Behavioral FY2021';
    	currentBehavioral.UMD_Purpose_Description__c = 'Behavioral FY2021';
    	
    	UMD_Purpose__c currentStrategical = new UMD_Purpose__c();
    	currentStrategical.Fiscal_Year_VS__c = 'FY2021';
    	currentStrategical.Segmentation_Qualification__c = 'Strategical';
    	currentStrategical.Status__c = 'Open for input';
    	currentStrategical.Name = 'Strategical FY2021';
    	currentStrategical.UMD_Purpose_Description__c = 'Strategical FY2021';
    	    	
    	UMD_Purpose__c lastBehavioral = new UMD_Purpose__c();
    	lastBehavioral.Fiscal_Year_VS__c = 'FY2020';
    	lastBehavioral.Segmentation_Qualification__c = 'Behavioral';
    	lastBehavioral.Status__c = 'Completed';
    	lastBehavioral.Name = 'Behavioral FY2020';
    	lastBehavioral.UMD_Purpose_Description__c = 'Behavioral FY2021';
    	
    	UMD_Purpose__c currentPotential = new UMD_Purpose__c();
    	currentPotential.Fiscal_Year_VS__c = 'FY2021';
    	currentPotential.Segmentation_Qualification__c = 'Potential';
    	currentPotential.Status__c = 'Open for input';
    	currentPotential.Name = 'Potential FY2021';
    	currentPotential.UMD_Purpose_Description__c = 'Potential FY2021';
    	    	
    	UMD_Purpose__c lastPotential = new UMD_Purpose__c();
    	lastPotential.Fiscal_Year_VS__c = 'FY2020';
    	lastPotential.Segmentation_Qualification__c = 'Potential';
    	lastPotential.Status__c = 'Completed';
    	lastPotential.Name = 'Potential FY2020';
    	lastPotential.UMD_Purpose_Description__c = 'Potential FY2021';
    	
    	insert new List<UMD_Purpose__c>{currentBehavioral, currentStrategical, lastBehavioral, currentPotential, lastPotential};
    	
    	Segmentation_Potential_Procedure__c proc = new Segmentation_Potential_Procedure__c();
    	proc.Name = 'Procedure 1';
    	proc.Active__c = true;
    	proc.Registration_Unit__c = 'UNITS';
    	proc.Sub_Business_Unit__c = 'Coro + PV';
    	insert proc;
    	
    	Account acc = new Account();
    	acc.Name = 'Test Account';
    	acc.SAP_Id__c = '121231234';
    	acc.Account_City__c = 'Paris';
    	acc.Account_Country_vs__c = 'FRANCE';
    	insert acc;
    	
    	Segmentation_Potential_Input__c lastPotInput = new Segmentation_Potential_Input__c();
    	lastPotInput.Account__c = acc.Id;
    	lastPotInput.UMD_Purpose__c = lastPotential.Id;
    	lastPotInput.Sub_Business_Unit__c = 'Coro + PV';
    	lastPotInput.Last_12_mth_Sales__c = 100;
    	lastPotInput.Segmentation_Potential_Procedure__c = proc.Id;
    	lastPotInput.Potential__c = 230;
    	lastPotInput.Potential_Medtronic__c = 110;
    	lastPotInput.Assigned_To__c = UserInfo.getUserId();
    	lastPotInput.Approver_1__c = UserInfo.getUserId();
    	lastPotInput.Complete__c = true;
    	lastPotInput.Approved__c = true;
    	
    	Segmentation_Potential_Input__c currentPotInput = new Segmentation_Potential_Input__c();
    	currentPotInput.Account__c = acc.Id;
    	currentPotInput.UMD_Purpose__c = currentPotential.Id;
    	currentPotInput.Sub_Business_Unit__c = 'Coro + PV';
    	currentPotInput.Segmentation_Potential_Procedure__c = proc.Id;
    	currentPotInput.Last_12_mth_Sales__c = 120;
    	currentPotInput.Assigned_To__c = UserInfo.getUserId();
    	currentPotInput.Complete__c = true;
    	currentPotInput.Approver_1__c = UserInfo.getUserId();
    	    	    	    	
    	insert new List<Segmentation_Potential_Input__c>{lastPotInput, currentPotInput};
    	
    	UMD_Input_Complete__c lastBehInput = new UMD_Input_Complete__c();
    	lastBehInput.Account__c = acc.Id;
    	lastBehInput.UMD_Purpose__c = lastBehavioral.Id;
    	lastBehInput.Sub_Business_Unit__c = 'Coro + PV';
    	lastBehInput.Assigned_To__c = UserInfo.getUserId();
    	lastBehInput.Approver_1__c = UserInfo.getUserId();
    	lastBehInput.Complete__c = true;
    	lastBehInput.Approved__c = true;
    	
    	UMD_Input_Complete__c currentBehInput = new UMD_Input_Complete__c();
    	currentBehInput.Account__c = acc.Id;
    	currentBehInput.UMD_Purpose__c = currentBehavioral.Id;
    	currentBehInput.Sub_Business_Unit__c = 'Coro + PV';
    	currentBehInput.Assigned_To__c = UserInfo.getUserId();
    	currentBehInput.Approver_1__c = UserInfo.getUserId();
    	currentBehInput.Complete__c = true;
    	currentBehInput.Approved__c = true;
    	
    	UMD_Input_Complete__c currentStratInput = new UMD_Input_Complete__c();
    	currentStratInput.Account__c = acc.Id;
    	currentStratInput.UMD_Purpose__c = currentStrategical.Id;
    	currentStratInput.Business_Unit_Group__c = 'CVG';
    	currentStratInput.Assigned_To__c = UserInfo.getUserId();
    	currentStratInput.Approver_1__c = UserInfo.getUserId();     	    	    	
    	
    	insert new List<UMD_Input_Complete__c>{lastBehInput, currentBehInput, currentStratInput};
    	   	    
    }
    
    private static testmethod void testCtrlAccountSegmentaion(){
    	
    	System.runAs(new User(Id = UserInfo.getUserId())){
    		
    		List<PermissionSetAssignment> assignments = [Select Id from PermissionSetAssignment where AssigneeId = :UserInfo.getUserId() AND PermissionSet.Name = 'Segmentation_GTM_Super_User'];
    		
    		if(assignments.size() == 0){
    			
    			PermissionSet ps = [Select Id from PermissionSet where Name = 'Segmentation_GTM_Super_User'];
    			
    			PermissionSetAssignment assignment = new PermissionSetAssignment();
    			assignment.AssigneeId = UserInfo.getUserId();
    			assignment.PermissionSetId = ps.Id;
    			
    			insert assignment;
    		}	
    	}    	
    	
    	ctrl_Account_Segmentation.UserSegmentationOverview overview = ctrl_Account_Segmentation.getUserOverview();
    	System.assert(overview.strategicOptions.size() == 1);
    	System.assert(overview.strategicApprovalOptions.size() == 1);
    	System.assert(overview.behavioralOptions.size() == 1);
    	System.assert(overview.behavioralApprovalOptions.size() == 1);
    	System.assert(overview.hasPotentialInput == true);
    	System.assert(overview.hasPotentialApproval == true);
    	System.assert(overview.isAdmin == true);
    	
    }
    
    private static testmethod void testCtrlAccountSegmentaionOverview(){
    	
    	System.runAs(new User(Id = UserInfo.getUserId())){
    		
    		List<PermissionSetAssignment> assignments = [Select Id from PermissionSetAssignment where AssigneeId = :UserInfo.getUserId() AND PermissionSet.Name = 'Segmentation_GTM_Super_User'];
    		
    		if(assignments.size() == 0){
    			
    			PermissionSet ps = [Select Id from PermissionSet where Name = 'Segmentation_GTM_Super_User'];
    			
    			PermissionSetAssignment assignment = new PermissionSetAssignment();
    			assignment.AssigneeId = UserInfo.getUserId();
    			assignment.PermissionSetId = ps.Id;
    			
    			insert assignment;
    		}	
    	}   
    	
    	ctrl_Account_Segmentation_Overview.UserOverview result = ctrl_Account_Segmentation_Overview.getUserOverview();
    	    	
    	List<ctrl_Account_Segmentation_Overview.Completion> completions = result.completions;
    	System.assert(completions.size() == 9);
    	System.assert(completions[0].code == 'PotentialInput');
    	System.assert(completions[0].completionPercentageRound == 100);
    	System.assert(completions[1].code == 'PotentialApproval');
    	System.assert(completions[1].completionPercentageRound == 0);
    	System.assert(completions[2].code == 'PotentialAdmin');
    	System.assert(completions[2].completionPercentageRound == 0);
    	System.assert(completions[3].code == 'BehavioralInput');
    	System.assert(completions[3].completionPercentageRound == 100);
    	System.assert(completions[4].code == 'BehavioralApproval');
    	System.assert(completions[4].completionPercentageRound == 100);
    	System.assert(completions[5].code == 'BehavioralAdmin');
    	System.assert(completions[5].completionPercentageRound == 100);
    	System.assert(completions[6].code == 'StrategicInput');
    	System.assert(completions[6].completionPercentageRound == 0);
    	System.assert(completions[7].code == 'StrategicApproval');
    	System.assert(completions[7].completionPercentageRound == 0);
    	System.assert(completions[8].code == 'StrategicAdmin');
    	System.assert(completions[8].completionPercentageRound == 0);
    	
    	ctrl_Account_Segmentation_Overview.Completion resultDetails = ctrl_Account_Segmentation_Overview.getOverviewDetails('PotentialInput', 'Segmentation', null, null, null);
    	resultDetails = ctrl_Account_Segmentation_Overview.getOverviewDetails('PotentialInput', 'BusinessUnit', null, null, null);
    	resultDetails = ctrl_Account_Segmentation_Overview.getOverviewDetails('PotentialApproval', 'Segmentation', null, null, null);
    	resultDetails = ctrl_Account_Segmentation_Overview.getOverviewDetails('PotentialApproval', 'Assigned', null, null, null);
    	resultDetails = ctrl_Account_Segmentation_Overview.getOverviewDetails('PotentialApproval', 'BusinessUnit', null, null, null);
    	resultDetails = ctrl_Account_Segmentation_Overview.getOverviewDetails('PotentialAdmin', 'Segmentation', 'FRANCE', 'CVG', 'Coro + PV');
    	resultDetails = ctrl_Account_Segmentation_Overview.getOverviewDetails('PotentialAdmin', 'Approver', 'FRANCE', 'CVG', 'Coro + PV');
    	resultDetails = ctrl_Account_Segmentation_Overview.getOverviewDetails('PotentialAdmin', 'BusinessUnit', 'FRANCE', 'CVG', 'Coro + PV');
    	    	
    	resultDetails = ctrl_Account_Segmentation_Overview.getOverviewDetails('BehavioralInput', 'Segmentation', null, null, null);
    	resultDetails = ctrl_Account_Segmentation_Overview.getOverviewDetails('BehavioralInput', 'BusinessUnit', null, null, null);
    	resultDetails = ctrl_Account_Segmentation_Overview.getOverviewDetails('BehavioralApproval', 'Segmentation', null, null, null);
    	resultDetails = ctrl_Account_Segmentation_Overview.getOverviewDetails('BehavioralApproval', 'Assigned', null, null, null);
    	resultDetails = ctrl_Account_Segmentation_Overview.getOverviewDetails('BehavioralApproval', 'BusinessUnit', null, null, null);
    	resultDetails = ctrl_Account_Segmentation_Overview.getOverviewDetails('BehavioralAdmin', 'Segmentation', 'FRANCE', 'CVG', 'Coro + PV');
    	resultDetails = ctrl_Account_Segmentation_Overview.getOverviewDetails('BehavioralAdmin', 'Approver', 'FRANCE', 'CVG', 'Coro + PV');
    	resultDetails = ctrl_Account_Segmentation_Overview.getOverviewDetails('BehavioralAdmin', 'BusinessUnit', 'FRANCE', 'CVG', 'Coro + PV');
    	
    	Test.startTest();
    	    	
    	resultDetails = ctrl_Account_Segmentation_Overview.getOverviewDetails('StrategicInput', 'Segmentation', null, null, null);
    	resultDetails = ctrl_Account_Segmentation_Overview.getOverviewDetails('StrategicInput', 'BusinessUnit', null, null, null);
    	resultDetails = ctrl_Account_Segmentation_Overview.getOverviewDetails('StrategicApproval', 'Segmentation', null, null, null);
    	resultDetails = ctrl_Account_Segmentation_Overview.getOverviewDetails('StrategicApproval', 'Assigned', null, null, null);
    	resultDetails = ctrl_Account_Segmentation_Overview.getOverviewDetails('StrategicApproval', 'BusinessUnit', null, null, null);
		resultDetails = ctrl_Account_Segmentation_Overview.getOverviewDetails('StrategicAdmin', 'Segmentation', 'FRANCE', 'CVG', null);		
		    	    	
    	resultDetails = ctrl_Account_Segmentation_Overview.getAdminOverviewDetails('PotentialAdmin', 'CVG');
    	    	
    	resultDetails = ctrl_Account_Segmentation_Overview.getAdminOverviewDetails('BehavioralAdmin', 'CVG');
    	    	
    	resultDetails = ctrl_Account_Segmentation_Overview.getAdminOverviewDetails('StrategicAdmin', 'CVG');    	
    }
    
    private static testmethod void testSecurity(){
    	
    	User testUser = [Select Id from User where Profile.Name = 'EUR Field Force CVG' AND isActive = true LIMIT 1];
    	
    	Segmentation_Potential_Input__c potInput = [Select Id, Assigned_To__c from Segmentation_Potential_Input__c where UMD_Purpose__r.Fiscal_Year_vs__c = 'FY2021' AND Sub_Business_Unit__c = 'Coro + PV'];
    	
    	potInput.Assigned_To__c = testUser.Id;
    	update potInput;
    	
    	PermissionSetMedtronic.userPermissionSets = null;
    	
    	System.runAs(testUser){
    		
    		Boolean hasError = false;
    		
    		try{
    			
    			potInput.Potential__c = 150;
    			update potInput;
    			
    		}catch(Exception e){
    			
    			hasError = true;
    			System.assert(e.getMessage().contains('This record can only be managed through the Customer Segmentation app'));
    		}
    		
    		System.assert(hasError);
    	}
    }
}