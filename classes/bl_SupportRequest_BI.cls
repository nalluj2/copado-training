public without sharing class bl_SupportRequest_BI {

    //------------------------------------------------------------
    // Private variables
    //------------------------------------------------------------
    private ctrl_Support_Requests.SessionData session;
    private Map<String, Company__c> mapCountryCompany = new Map<String, Company__c>();
    //------------------------------------------------------------
    
    //------------------------------------------------------------
    // Constructor
    //------------------------------------------------------------
    public bl_SupportRequest_BI(ctrl_Support_Requests.SessionData sData){
        tApplicationName = 'BI';
        this.session = sData;       
    }
    //------------------------------------------------------------


    //------------------------------------------------------------
    // Getters & Setters
    //------------------------------------------------------------
    public String tApplicationName { get; private set; }
    public String searchAlias { get;set; }
    public String searchAliasSameAsUser { get;set; }
    public List<SelectOption> buOptions {get; set;}
    public Boolean hasSFDCUser {get; set;}

    public List<SelectOption> lstSO_ContentGroup { get;set; }
    public List<SelectOption> lstSO_CapabilityGroup { get;set; }
    private Id defaultCapability;

    public List<SelectOption> lstSO_Region { get;set; } 
    public List<SelectOption> lstSO_Country { get;set; } 
    public List<SelectOption> lstSO_BusinessUnit { get;set; } 
    public List<String> lstSelectedBusinessUnit { get;set; } 
    //------------------------------------------------------------

    
    //------------------------------------------------------------
    // Page-Reference
    //------------------------------------------------------------
    public PageReference requestNewUser(){
    
        session.resetRequest();

        session.request.Status__c = 'New';
        session.request.application__c = tApplicationName;
        session.request.ownerId = session.internalUserId;
        session.request.Request_Type__c = 'Create User';
        //session.request.User_Business_Unit_vs__c = 'All';
        
        session.comment = null;
        session.comments = null;

        initialize();
        
        session.request.BI_Capability_Group__c = defaultCapability;
        hasSFDCUser = false;
        
        loadCountryOptions();
        loadRegionOptions();
        loadBusinessUnitOptions();

        return Page.Support_BI_CreateUser;         
    }

    public PageReference requestChangeUser(){
    
        session.resetRequest();

        session.request.Status__c = 'New';
        session.request.application__c = tApplicationName;
        session.request.ownerId = session.internalUserId;
        session.request.Request_Type__c = 'Change User Setup';

        session.comment = null;
        session.comments = null;

        searchAlias = '';
        searchAliasSameAsUser = '';

        initialize();
        
        session.request.BI_Capability_Group__c = defaultCapability;
                
        return Page.Support_BI_ChangeUserSetup;         
    }
    
    public PageReference requestDeactivateUser(){

        session.resetRequest();

        session.request.Status__c = 'New';
        session.request.application__c = tApplicationName;
        session.request.ownerId = session.internalUserId;
        session.request.Request_Type__c = 'De-activate User';
        session.request.Date_of_deactivation__c = DateTime.now().addhours(1);
        session.request.Alias__c = null;

        session.comment = null;
        session.comments = null;

        return Page.Support_BI_DeactivateUser;         
    }

    public PageReference requestServiceRequest(){

        session.resetRequest();

        session.request.Status__c = 'New';
        session.request.application__c = tApplicationName;
        session.request.ownerId = session.internalUserId;
        session.request.Request_Type__c = 'Generic Service';
        
        session.clearAttachments();        
        session.readOnlyAttachments = new List<Attachment>();

        session.comment = null;
        session.comments = null;
        
        initialize();
        
        hasSFDCUser = false;
        
        checkGeographicBUOptions(session.internalUserId);     
        
        return Page.Support_BI_GenericService;         
    }
    
    private void checkGeographicBUOptions(Id scopeUserId){
        
        List<User> scopeUsers = 
            [
                SELECT 
                    Id, UserType, Job_Title_vs__c, Company_Code_Text__c, CountryOR__c
                    , Region_vs__c, Sub_Region__c, Country_vs__c, User_Business_Unit_vs__c, Primary_sBU__c 
                FROM
                    User
                WHERE 
                    Id = :scopeUserId
            ];

                    
        if(scopeUsers.size() > 0 && scopeUsers[0].UserType != 'Guest'){
            
            User scopeUser = scopeUsers[0];
            
            hasSFDCUser = true;
            
            session.request.Jobtitle_vs__c = scopeUser.Job_Title_vs__c;

            session.request.Company_Code__c = scopeUser.Company_Code_Text__c;

            session.request.User_Business_Unit_vs__c = scopeUser.User_Business_Unit_vs__c;
            session.request.Primary_sBU__c = scopeUser.Primary_sBU__c; 
            session.request.Jobtitle_vs__c = scopeUser.Job_Title_vs__c;
           
            session.request.Geographical_Region_vs__c = scopeUser.Region_vs__c;
            session.request.Geographical_Subregion__c = scopeUser.Sub_Region__c;
            session.request.Geographical_Country_vs__c = scopeUser.Country_vs__c;

            if (session.request.Region__c == null){
                session.request.Region__c = getRequestCompanyId();
            }

            
/*
            List<DiB_Country__c> userCountry = [Select Id, Name, Company__c, Company__r.Name, Company__r.Company_Code_Text__c from DIB_Country__c where Name = :scopeUser.CountryOR__c LIMIT 1];
            
            if(userCountry.size() > 0){

                session.request.Region__c = userCountry[0].Company__c;
                session.request.Country__c = userCountry[0].Id;
                
                session.request.Region__r = userCountry[0].Company__r;
                session.request.Country__r = userCountry[0];
                
            }
*/                
            
            String businessUnit;
            
            for(AggregateResult userBU : [Select Sub_Business_Unit__r.Business_Unit__r.Name BUName from User_Business_Unit__c where User__c = :scopeUser.Id GROUP BY Sub_Business_Unit__r.Business_Unit__r.Name ORDER BY Sub_Business_Unit__r.Business_Unit__r.Name ASC]){
                
                if(businessUnit == null) businessUnit = (String) userBU.get('BUName');
                else businessUnit += ',' + userBU.get('BUName');
            }
            
            if(businessUnit != null){
                
                session.request.Business_Unit__c = '[' + businessUnit + ']';
            }
            
        }else{
            
            hasSFDCUser = false;
            
            session.request.Jobtitle_vs__c = null;

            session.request.Company_Code__c = null;

            session.request.User_Business_Unit_vs__c = null;
            session.request.Primary_sBU__c = null; 
            session.request.Jobtitle_vs__c = null;
           
            session.request.Geographical_Region_vs__c = null;
            session.request.Geographical_Subregion__c = null;
            session.request.Geographical_Country_vs__c = null;

/*            
            session.request.Region__c = null;
            session.request.Country__c = null;
                
            session.request.Region__r = null;
            session.request.Country__r = null;
*/            
            session.request.Business_Unit__c = null;
            
            loadRegionOptions();
            loadCountryOptions();           
            loadBusinessUnitOptions();
        }
        
    }
    
    //------------------------------------------------------------


    //------------------------------------------------------------
    // Private Methods
    //------------------------------------------------------------
    private void initialize(){
        loadSupportRequestParameterData();
        loadBusinessUnitData();
    }

    private void loadSupportRequestParameterData(){
        
        if (lstSO_ContentGroup == null || lstSO_CapabilityGroup == null || lstSO_ContentGroup.size() == 0 || lstSO_CapabilityGroup.size() == 0){
            
            lstSO_ContentGroup = new List<SelectOption>();
            lstSO_CapabilityGroup = new List<SelectOption>();

            lstSO_ContentGroup.add(new SelectOption('', '--None--'));
            lstSO_CapabilityGroup.add(new SelectOption('', '--None--'));
            
            for (Support_Request_Parameter__c oSRP : [SELECT Id, Active__c, Type__c, Value__c FROM Support_Request_Parameter__c WHERE Active__c = true ORDER BY Type__c, Value__c]){
                
                if (oSRP.Type__c == 'BI - Content Group'){
                    lstSO_ContentGroup.add(new SelectOption(oSRP.Id, oSRP.Value__c));
                }else if (oSRP.Type__c == 'BI - Capability Group'){
                    lstSO_CapabilityGroup.add(new SelectOption(oSRP.Id, oSRP.Value__c));
                    if(oSRP.Value__c == 'Information Consumer') defaultCapability = oSRP.Id;
                }
            }       
        }

    }

    private void loadBusinessUnitData(){

        if (buOptions == null || buOptions.size() == 0){
            
            buOptions = new List<SelectOption>();
            buOptions.add(new SelectOption('', '--None--'));

            for(AggregateResult bu : [Select Name from Business_Unit__c GROUP BY Name ORDER BY Name ASC]){
                String buName = String.valueOf(bu.get('Name'));
                buOptions.add(new SelectOption(buName, buName));
            }
        }
    }

    private void fillUserInfo(ws_adUserService.userinfo inputInfo, String tType){
        if (tType == 'ALIAS'){
            session.request.Alias__c = searchAlias.toLowerCase();
            session.request.Firstname__c = inputInfo.firstName;
            session.request.Lastname__c = inputInfo.lastName;
            session.request.Email__c = inputInfo.email;
            session.request.Peoplesoft_ID__c = inputInfo.peopleSoft;
            session.request.Manager__c = inputInfo.managerName;
            session.request.Manager_Email__c = inputInfo.managerEmail;
            session.request.Cost_Center__c = inputInfo.costcenter;
            session.request.Federation_ID__c = session.request.Alias__c;
            
            if(session.request.Request_Type__c == 'Create User'){
                
                List<User> internalUser = [Select Id from User where isActive = true and Alias = :session.request.Alias__c];
                
                if(internalUser.size() == 1){
                    checkGeographicBUOptions(internalUser[0].Id);
                }else{
                    checkGeographicBUOptions(null);
                }
            }
            
        }else if (tType == 'ALIASSAMEASUSER'){
            session.request.Alias_Same_as_user__c = searchAliasSameAsUser;
            session.request.Firstname_same_as_user__c = inputInfo.firstName;
            session.request.Lastname_same_as_user__c = inputInfo.lastName;
            session.request.Email_same_as_user__c = inputInfo.email;
        }
    }  

    private void loadRegionOptions(){
        
        if(lstSO_Region != null) return;
        
        lstSO_Region = new List<SelectOption>();
        
        lstSO_Region.add(new SelectOption('', '--None--'));
        
        for (Company__c oCompany : [SELECT Id, Name FROM Company__c ORDER BY Name]){
            
            lstSO_Region.add(new SelectOption(oCompany.Id, oCompany.Name));            
        }       
    }


    public void geographicalRegionChanged(){
        session.request.Geographical_Subregion__c = null;
        session.request.Geographical_Country_vs__c = null;
        clearCompanyCodeData();
    }

    public void geographicalSubRegionChanged(){
        session.request.Geographical_Country_vs__c = null;
        clearCompanyCodeData();
    }

    public void userBusinessUnitChanged(){

    }

    public void clearCompanyCodeData(){
        session.request.Region__c = null;
        session.request.Company_Code__c = null;
        session.request.User_Business_Unit_vs__c = null;  
        session.request.Jobtitle_vs__c = null;  
    }
    
    public void refreshCompanyCodeData(){

        if (session.request.Geographical_Country_vs__c == null){
            clearCompanyCodeData();
            return;
        }

        Company__c oCompany = getRequestCompany();
        if (oCompany == null){
            clearCompanyCodeData();
        }else{
            session.request.Region__c = oCompany.Id;
            session.request.Company_Code__c = oCompany.Company_Code_Text__c;
        }


    }    

    public void countrySelectionChanged(){

        refreshCompanyCodeData();

//        checkSapIdMandatory();


    }

    private Company__c getRequestCompany(){

        Company__c oCompany = null;

        if (!mapCountryCompany.containsKey(session.request.Geographical_Country_vs__c)){
            List<DIB_Country__c> lstDIBCountry = [SELECT Id, Name, Company__r.Id, Company__r.Company_Code_Text__c FROM DIB_Country__c WHERE Name = :session.request.Geographical_Country_vs__c];
            if (lstDIBCountry.size() > 0){
                mapCountryCompany.put(lstDIBCountry[0].Name, lstDIBCountry[0].Company__r);
            }
        }
        oCompany = mapCountryCompany.get(session.request.Geographical_Country_vs__c);

        return oCompany;

    }

    
    private void loadCountryOptions(){
        
        lstSO_Country = new List<SelectOption>();
        
        lstSO_Country.add(new SelectOption('', '--None--'));
        
        if (session.request.Region__c != null){
            
            for(DIB_Country__c oCountry : [SELECT Id, Name FROM DIB_Country__c WHERE Company__c =: session.request.Region__c ORDER BY Name]){
                lstSO_Country.add(new SelectOption(oCountry.Id, oCountry.Name));
            }
        }
    }

    private void loadBusinessUnitOptions(){

        lstSO_BusinessUnit = new List<SelectOption>();
        lstSO_BusinessUnit.add(new SelectOption('', '--None--'));

        if (session.request.Region__c != null){
            
            for(Business_Unit__c oBU : [SELECT Id, Name FROM Business_Unit__c WHERE Company__c =: session.request.Region__c ORDER BY Name]){
                lstSO_BusinessUnit.add(new SelectOption(oBU.Name, oBU.Name));
            }
        }
    }
    //------------------------------------------------------------

    
    //------------------------------------------------------------
    // Action Methods
    //------------------------------------------------------------

    public void createUserRequest(){

        if (session.request.Firstname__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Firstname is required'));
            return;
        }

        if (session.request.Lastname__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Lastname is required'));
            return;
        }

        if (session.request.Email__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Email is required'));
            return;
        }


        if (hasSFDCUser == false){

            if (session.request.Geographical_Region_vs__c == null || session.request.Geographical_Region_vs__c == '--None--'){          
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Geographical Region is required'));
                return; 
            }
            if (session.request.Geographical_Subregion__c == null || session.request.Geographical_Subregion__c == '--None--'){          
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Geographical Subregion is required'));
                return; 
            }
            if (session.request.Geographical_Country_vs__c == null || session.request.Geographical_Country_vs__c == '--None--'){          
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Geographical Country is required'));
                return; 
            }

            if (session.request.Company_Code__c == null || session.request.Company_Code__c == '--None--'){          
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Company Code is required'));
                return; 
            }

            if (session.request.User_Business_Unit_vs__c == null || session.request.User_Business_Unit_vs__c == '--None--'){          
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'User Business Unit is required'));
                return; 
            }

            if (session.request.Jobtitle_vs__c == null || session.request.Jobtitle_vs__c == '--None--'){            
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Job Title is required'));
                return; 
            }
        
        }

        // CR-9490 - Christophe Saenen - 20160304 - same_as_user criterium added
        if (String.isBlank(session.request.Lastname_same_as_user__c) && session.request.BI_Content_Group__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Application is required'));
            return;
        }
        
        if (String.isBlank(session.request.Lastname_same_as_user__c) && session.request.BI_Capability_Group__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Capability is required'));
            return;
        }

        try{    
        
            session.request.Requestor_Email__c = session.uInfo.email;
            session.request.Requestor_Name__c = session.uInfo.firstName + ' ' + session.uInfo.lastName;
            insert session.request;

            session.comment = new User_Request_Comment__c();        

        }catch(Exception e){

            ApexPages.addMessages(e);
            if(Test.isRunningTest()) throw e;
            return;
        }

        session.request = loadSupportRequest(session.request.Id);

        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request '+session.request.Name+' created successfully.'));    
    }

    public void createChangeUserRequest(){
                        
        if (session.request.Lastname__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Medtronic username is required'));
            return;
        }
        
        if (session.request.Change_Type__c == null || session.request.Change_Type__c == ''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Change Type is required'));
            return;
        }
        
        if (session.request.Change_Type__c != 'Other' && session.request.BI_Content_Group__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Application is required'));
            return;
        }
        
        if (session.request.Change_Type__c == 'Add/modify User Application' && session.request.BI_Capability_Group__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Capability is required'));
            return;
        }
        
        if (session.request.Change_Type__c == 'Other'){ 
            
            session.request.BI_Content_Group__c = null;
            session.request.BI_Capability_Group__c = null;
            
            if(session.request.Description_Long__c == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Description is required'));
                return;
            }           
        }
                
        try{    
            
            session.request.Requestor_Email__c = session.uInfo.email;
            session.request.Requestor_Name__c = session.uInfo.firstName + ' ' + session.uInfo.lastName;
            insert session.request;
            
            session.comment = new User_Request_Comment__c();        
                        
        }catch(Exception e){
            
            ApexPages.addMessages(e);
            if(Test.isRunningTest()) throw e;
            return;
        }
        
        session.request = loadSupportRequest(session.request.Id);
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request '+session.request.Name+' created successfully.'));       
    }
    
    public void createDeactivateRequest(){

        if(session.request.Firstname__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Firstname is required'));
            return;
        }

        if(session.request.Lastname__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Lastname is required'));
            return;
        }

        if(session.request.Email__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Email is required'));
            return;
        }

        if(session.request.Date_of_deactivation__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Date of Deactivation is required'));
            return;
        }
        
        if(session.request.Deactivation_Reason__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Deactivation Reason is required'));
            return;
        }

        try{    

            session.request.Requestor_Email__c = session.uInfo.email;
            session.request.Requestor_Name__c = session.uInfo.firstName + ' ' + session.uInfo.lastName;
            insert session.request;

            session.comment = new User_Request_Comment__c();        

        }catch(Exception e){

            ApexPages.addMessages(e);
            if(Test.isRunningTest()) throw e;
            return;
        }

        session.request = loadSupportRequest(session.request.Id);

        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request '+session.request.Name+' created successfully.'));    
    }

    public void createGenericServiceRequest(){

        if(clsUtil.isNull(session.request.Area_SR__c, '') == ''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Request Type is required'));
            return;
        }
        
        if(clsUtil.isNull(session.request.Report_Type__c, '') == ''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Report Name is required'));
            return;
        }

        if(hasSFDCUser == false && clsUtil.isNull(session.request.Region__c, '') == ''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Region is required'));
            return;
        }

        if(hasSFDCUser == false && clsUtil.isNull(session.request.Country__c, '') == ''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Country is required'));
            return;
        }

        if ( 
            hasSFDCUser == false 
            && 
            (
                clsUtil.isNull(session.request.Business_Unit__c, '') == '' 
                || clsUtil.isNull(session.request.Business_Unit__c, '').contains('--None--')
            )
        ){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Business Unit is required'));
            return;
        }

        
        if (session.request.BI_Content_Group__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Application is required'));
            return;
        }

        if(clsUtil.isNull(session.request.Description_Long__c, '') == ''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Description is required'));
            return;
        }

        Create_User_Request__c toInsertRequest = session.request.clone(false);

        SavePoint sp = Database.setSavepoint();

        try{

            toInsertRequest.Requestor_Name__c = session.uInfo.firstName+' '+session.uInfo.lastName;
            toInsertRequest.Requestor_Email__c = session.uInfo.email;
            insert toInsertRequest;

            session.saveAttachments(toInsertRequest.Id);

            session.request.Id = toInsertRequest.Id;

            session.comment = new User_Request_Comment__c();

        } catch (Exception e) {

            ApexPages.addMessages(e);
            Database.rollback(sp);

            if(Test.isRunningTest()) throw e;

            return ;
        }finally{
            session.clearAttachments(); 
        }

        session.request = loadSupportRequest(session.request.Id);

        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request '+session.request.Name+' created successfully.'));    
    }
    
    public void searchByAlias(){
        ws_adUserService.userinfo searchResult = bl_SupportRequest.searchByAlias(searchAlias);
        if (searchResult != null) fillUserInfo(searchResult, 'ALIAS');     
    }

    public void searchByAliasSameAsUser(){
        
        ws_adUserService.userinfo searchResult = bl_SupportRequest.searchByAlias(searchAliasSameAsUser);
        if (searchResult != null) fillUserInfo(searchResult, 'ALIASSAMEASUSER');     

    }
      
    public Create_User_Request__c loadSupportRequest(Id idSupportRequest){
        return bl_SupportRequest.loadSupportRequestDetailData(idSupportRequest);
    }

    public void regionSelectionChanged(){
        
        loadCountryOptions();
        loadBusinessUnitOptions();
    
    }
    
    public void BUSelectionChanged(){
       
        String tSelectedBusinessUnits = '';

        if (lstSelectedBusinessUnit != null){
           
            if (lstSelectedBusinessUnit.size() > 0){

                for (String tBU : lstSelectedBusinessUnit){
                    if ( (tBU != '') && (tBU != '-None-') ){
                        if (tSelectedBusinessUnits != '') tSelectedBusinessUnits += ',';
                        tSelectedBusinessUnits += tBU;
                    }
                }

                if (lstSelectedBusinessUnit.size() == 1){
                   session.request.User_Business_Unit_vs__c = lstSelectedBusinessUnit[0];
                } else {
                   session.request.User_Business_Unit_vs__c = 'All';
                }

            }

        }

        clsUtil.debug('BUSelectionChanged - tSelectedBusinessUnits : ' + tSelectedBusinessUnits);
        if (tSelectedBusinessUnits != ''){
            tSelectedBusinessUnits = '[' + tSelectedBusinessUnits + ']';
        }
        session.request.Business_Unit__c = tSelectedBusinessUnits;

    } 

    private Id getRequestCompanyId(){

        Id id_Result = null;

        List<DIB_Country__c> lstDIBCountry = [SELECT Id, Name, Company__r.Id FROM DIB_Country__c WHERE Name = :session.request.Geographical_Country_vs__c];
        if (lstDIBCountry.size() > 0){
            id_Result = lstDIBCountry[0].Company__r.Id;
        }

        return id_Result;

    }

    //------------------------------------------------------------
}