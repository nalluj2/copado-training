/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TEST_DIB_controllerAllocation {

    static testMethod void myUnitTest() {
        
        System.debug(' ################## ' + 'BEGIN TEST_DIB_controllerAllocation' + ' ################');     
        final string month = '01' ; 
        final string year = '2000';     
        
        // Sold To Accuonts 
        List<Account> soldToAccounts    = new List<Account>{} ;
        
        // Sales Force Acounnts 
        List<Account> sfaAccounts       = new List<Account>{} ;
        
        List<DIB_Department__c> dimDepartments = new List<DIB_Department__c>{} ;
        
        // Sold to Ids           
        List<Id> soldToIds = new List<Id>{} ;
        
        List<String>shipToIds=new List<String>{};
        
        // Invoice Lines for month & Year  
        List<DIB_Invoice_Line__c> invoiceLines0109 = new List<DIB_Invoice_Line__c>{} ; 
        
        // Invoice lines with length = 1 ; 
        List<DIB_Invoice_Line__c> invoiceLinesSingle0209 = new List<DIB_Invoice_Line__c>{} ;
        
        // List Affiliations 
        List<Affiliation__c> affiliations = new List<Affiliation__c>{} ; 
        
        // Create a few Sold to Accounts ; 
        for (Integer i = 0; i < 30 ; i++){
            Account acc1 = new Account();
            acc1.Name = 'Test Coverage Sold to or ship To Account ' + i ;
            soldToAccounts.add(acc1) ;
        }
        insert soldToAccounts ; 
        
        // create Sales Force Accounts : 
        for (Integer i = 0; i < 20 ; i++){
            Account acc1 = new Account();
            acc1.Name = 'Test Coverage Sales Force Account ' + i ;
            acc1.isSales_Force_Account__c = true ; 
            sfaAccounts.add(acc1) ;
        }
        
        insert sfaAccounts ; 

        // create DIB_Department__c : 
        for (Integer i = 0; i < 20 ; i++){
            DIB_Department__c dep1 = new DIB_Department__c();
            //dep1.Name = 'Test Coverage Department ' + i ;
            dep1.Account__c = sfaAccounts[i].Id;
            dep1.Active__c = true;
            dimDepartments.add(dep1);
        }
        
        insert dimDepartments ;         
         
        Department_Master__c department = new Department_Master__c();
        department.Name = 'Adult';
        insert department;
            
        DIB_Department__c myDepartment = new DIB_Department__c();
        myDepartment.Account__c = sfaAccounts.get(0).Id;
        myDepartment.Department_ID__c = department.Id;
            
        insert myDepartment;
        
        // Create a list of all Ids of Sold To Accounts Ids
        for (Account acc : soldToAccounts){
            soldToIds.add(acc.Id);
            shipToIds.add(acc.name);
        }
        
        // Create a few Affiliations related to the SoldtoAccount & the SalesforceAccount
        for (Integer i = 0 ; i < 18 ; i++){ 
            Affiliation__c aff = new Affiliation__c () ;
            aff.Affiliation_Type__c = FinalConstants.affiliationPrecribingType ; 
            if (i < 3 ||  (i > 3 && i < 6)){
                if (i < 3) {    
                    aff.Affiliation_From_Account__c = null ;
                }else{
                    if(i > 3 && i < 6){
                        aff.Affiliation_From_Account__c = soldToAccounts[i].Id; 
                    }
                }                    
            }else{
                aff.Affiliation_From_Account__c = sfaAccounts[i].Id;    
            }       
            aff.Affiliation_To_Account__c = soldToAccounts[i].Id;
            aff.RecordTypeId = FinalConstants.recordTypeIdA2A;
            affiliations.add(aff); 
        }
        //insert affiliations ; 
        
        
        // create a few Invoice Lines for invoiceLines0109
        for (Integer i = 0; i < 25 ; i++){
            DIB_Invoice_Line__c invl  = new DIB_Invoice_Line__c() ; 
            invl.Distribution_No__c = 'DN' ; 
            invl.name = 'Invoice Name' ;  
            invl.Fiscal_Month_Code__c = month ;  
            invl.Fiscal_Year__c = year ; 
            invl.Allocated_Fiscal_Month_Code__c=month;
            invl.Allocated_Fiscal_Year__c=year; 
            invl.Sales_Force_Account__c=sfaAccounts.get(0).Id;
            invl.Department_ID__c=myDepartment.Id;
            // This field must have a value otherwise can have a problem saying Argument 1 cannot be null Line 101 on Method SharedMethods. 
            invl.Quantity_Invoiced_Number__c = 1; 
            invl.Sold_To__c = soldToAccounts[i].Id ; 
            invl.Ship_To_Name_Text__c = soldToAccounts[i+1].Name ;
            invl.Is_Allocated__c = false ;  
            if (i > 5 && i < 20){
                invl.Is_Allocated__c = true ;
            }
            if (i < 15){
                invl.Product_Type__c = FinalConstants.IL_TYPE_PUMPS ;
                if (invl.Is_Allocated__c == true){
                    invl.Pump_Origin__c = 'NPNP' ;
                }
            }else{
                invl.Product_Type__c = FinalConstants.IL_TYPE_CGMS ;
            }
            invoiceLines0109.add(invl) ; 
        }
        insert invoiceLines0109 ; 
        
        // Create a list with a single Invoice Line 
        DIB_Invoice_Line__c invlSingle  = new DIB_Invoice_Line__c() ; 
        invlSingle.Distribution_No__c = 'DN' ; 
        invlSingle.name = 'Invoice Name' ;  
        invlSingle.Fiscal_Month_Code__c = '02' ;  
        invlSingle.Fiscal_Year__c = '2009';
        invlSingle.Allocated_Fiscal_Month_Code__c=month;
        invlSingle.Allocated_Fiscal_Year__c=year; 
        invlSingle.Sales_Force_Account__c=sfaAccounts.get(0).Id;
        invlSingle.Department_ID__c=myDepartment.Id; 
        invlSingle.Quantity_Invoiced_Number__c = 1 ; 
        invlSingle.Sold_To__c = soldToAccounts[0].Id ; 
        invlSingle.Ship_To_Name_Text__c = soldToAccounts[1].Name ;
        invlSingle.Is_Allocated__c = false ; 
        invlSingle.Product_Type__c = FinalConstants.IL_TYPE_PUMPS ;         
        invoiceLinesSingle0209.add(invlSingle) ; 
        insert invoiceLinesSingle0209 ; 
                
        //ApexPages.Standardcontroller sct = new ApexPages.Standardcontroller(invoiceLines0109[0]);
        DIB_controllerAllocation_V1  controller = new DIB_controllerAllocation_V1();
        
        // News
        controller.setPeriodChosen(month + '-' + year) ;
        controller.setSelectedMonth(month);
        controller.setSelectedYear(year) ; 
    
        
        // SCENARIO 1 : Sold to 
        controller.setAccountAggregationGrouping('Sold-To') ; 
        controller.displayAccounts() ; 
                
        // SCENARIO 2 : Sold to Ship To 
        controller.setAccountAggregationGrouping('Sold-To/Ship-To') ;
        controller.displayAccounts() ;
                
        // Scenario 3 : null 
        controller.setAccountAggregationGrouping(null) ;
        controller.displayAccounts() ;
        
        
        // Scenario 4 : only one Single Invoice line
        controller.setSelectedMonth('02');
        controller.setSelectedYear('2009') ; 
        controller.setAccountAggregationGrouping('Sold-To') ; 
        controller.displayAccounts() ; 
        
        //Added by kaushal
        controller.setIsSelected(true);         
        controller.checkShowHideSubmitButton() ;
        
        // GETS     
        
        controller.getAccountAggregationGrouping() ; 
        controller.getAccountAggregationGroupingOptions(); 
        controller.getAllTAcgms(); 
        controller.getAllTApumps(); 
        controller.getassignedToEditItem(); 
        controller.getIsSelected(); 
        controller.getmassassignedToAllocateItem() ; 
        controller.getMassUpdate(); 
        controller.getperiodAffiliations(); 
        controller.getResultCGMSAccounts(); 
        controller.getResultPUMPAccounts(); 
        controller.getResultToAllocateItems(); 
        controller.getSelectedMonth(); 
        controller.getSelectedYear(); 
        controller.getshowSubmitButton();
        
        
        //Start:Added by kaushal
        controller.getAllocatedPeriod();
        controller.getDepartmentFilterOptions();
    
        //End:Added by kaushal
         
        // Edit  with an Id = null 
        controller.EditAllocation() ; 
        controller.setmassassignedToAllocateItem(invoiceLines0109[0].Id); 
        controller.setassignedToEditItem(invoiceLines0109[0].Id) ;
        // Edit with an existing Id by assigning setassignedToEditItem 
        controller.EditAllocation() ;  
        
        controller.updateAllocationPUMPS() ;
        controller.updateAllocationPUMPS() ; 
            
                
        // Mass Update :
        // Mass Update without anyline selected : 
        controller.massAllocateCGMS(); 
        controller.massAllocatePUMPS() ; 
        //controller.massUpdateDepartmentCGMS() ; 
        //controller.massUpdateDepartmentPUMPS() ; 
        controller.massUpdateOriginCGMS() ;
        controller.massUpdateOriginPUMPS() ; 
        controller.massUpdateSFAccountCGMS() ; 
        controller.massUpdateSFAccountPUMPS() ; 
        controller.massUpdatePrescriptorCGMS(); 
        controller.massUpdatePrescriptorPUMPS(); 

        
        controller.setperiodAffiliations(affiliations);
        // Pumps 
        invoiceLines0109[0].Department_ID__c = dimDepartments[0].Id ;       
       controller.createNewAffiliation(invoiceLines0109[0]) ;
        
        // CGMS :        
        invoiceLines0109[16].Department_ID__c = dimDepartments[0].Id ; 
        controller.createNewAffiliation(invoiceLines0109[16]) ;     
        controller.setMassUpdate(invoiceLines0109[1]) ;

        
        controller.displaySoldToAccounts();
        controller.setperiodAffiliations(affiliations);
        controller.drawResultToAllocateItems(invoiceLines0109,soldToIds,FinalConstants.IL_TYPE_PUMPS);
        controller.drawResultToAllocateItemsSoldToShipTo(invoiceLines0109,soldToIds,shipToIds,FinalConstants.IL_TYPE_PUMPS);
        /**
         INVOICE LINES HAVE BEEN CHANGE FROM THIS POINT, RESTART Some methods all over again. 
         To be able to Mass Update, we need to check lines : 
         **/
         
        // Pumps 
        
        DIB_Class_Allocation[] allTA = controller.getAllTapumps() ;
        controller.updateInvoiceLine(allTA);
        DIB_Class_Allocation[] newAllTa1 = new DIB_Class_Allocation[]{} ;  
        if (allTA != null) {
            for ( DIB_Class_Allocation all : allTA){  
                DIB_Invoice_Line__c mupd = all.getmassUpdate() ;   
                List<DIB_Class_Allocation_Core> taToAlloc = new List<DIB_Class_Allocation_Core>{} ; 
                for (DIB_Class_Allocation_Core ta : all.gettoAllocate()){
                    DIB_Class_Allocation_Core tatoUp = ta ; 
                    tatoUp.setlineSelected(true) ;  
                    DIB_Invoice_Line__c ilToMUP = tatoUp.getinvl() ;
                    ilToMUP.Sales_Force_Account__c = sfaAccounts[0].Id;
                    ilToMUP.Department_ID__c = myDepartment.Id ;
                    iltoMUP.Pump_Origin__c = 'NPNP';
                    iltoMUP.Is_Allocated__c = false ;               
                    tatoUp.setinvl(iltoMUP) ;   
                    taToAlloc.add(tatoUp) ;     
                    controller.setmassassignedToAllocateItem(iltoMUP.Id);   
                    controller.massAllocatePUMPS() ;     
                    controller.massUpdateDepartmentPUMPS() ;    
                    controller.massUpdateOriginPUMPS() ;     
                    controller.massUpdateSFAccountPUMPS() ;                 
                }
                all.settoAllocate(taToAlloc) ; 
                newAllTa1.add(all);             
            }
        }
        controller.setAllTaPumps(newAllTa1);
        controller.updateInvoiceLine(newAllTa1);
        
        
        controller.massAllocatePUMPS() ;     
        //controller.massUpdateDepartmentPUMPS() ;  
        controller.massUpdateOriginPUMPS() ;     
        controller.massUpdateSFAccountPUMPS() ; 
       
        
        
        
        // cGMS
         
        DIB_Class_Allocation[] allTA2 = controller.getAllTacgms() ;
        DIB_Class_Allocation[] newAllTa2 = new DIB_Class_Allocation[]{} ;  
        if (allTA2 != null){
            for ( DIB_Class_Allocation all : allTA2){  
                DIB_Invoice_Line__c mupd = all.getmassUpdate() ;   
                List<DIB_Class_Allocation_Core> taToAlloc = new List<DIB_Class_Allocation_Core>{} ; 
                for (DIB_Class_Allocation_Core ta : all.gettoAllocate()){                   
                    ta.setlineSelected(true) ;   
                    DIB_Invoice_Line__c ilToMUP = ta.getinvl() ;
                    ilToMUP.Sales_Force_Account__c = sfaAccounts[0].Id;
                    ilToMUP.Department_ID__c = myDepartment.Id ;
                    iltoMUP.Pump_Origin__c = 'NPNP';
                    iltoMUP.Is_Allocated__c = false ; 
                    ta.setinvl(iltoMUP) ;
                    taToAlloc.add(ta) ; 
                    controller.setmassassignedToAllocateItem(iltoMUP.Id);   
                    controller.massAllocateCGMS();  
                    controller.massUpdateDepartmentCGMS() ;
                    controller.massUpdateOriginCGMS() ;
                    controller.massUpdateSFAccountCGMS() ;
                    controller.allocateLine(ta);
                }
                all.settoAllocate(taToAlloc) ;
                newAllTa2.add(all);                 
            }
        }
       
        controller.isDepartmentSFAccount('Abc',soldToAccounts[0].id);
        
        controller.setAllTaCGMS(newAllTa2) ; 
        controller.updateInvoiceLine(newAllTa2) ;   
       
        controller.massAllocateCGMS();  
        //controller.massUpdateDepartmentCGMS() ;
        controller.massUpdateOriginCGMS() ;
        controller.massUpdateSFAccountCGMS() ;
        controller.massUpdatePrescriptorCGMS(); 
        controller.massUpdatePrescriptorPUMPS(); 
        
        // When it's null 
        /** Test Coverage for DIB_Class_Allocation & DIB_Class_Allocation_Core 
        **/
        
        DIB_Class_Allocation[] allTA3 = controller.getAllTapumps() ;
        controller.updateInvoiceLine(allTA3);
        DIB_Class_Allocation[] newAllTa3 = new DIB_Class_Allocation[]{} ;  
        if (allTA3 != null){
            for ( DIB_Class_Allocation all : allTA3){  
                DIB_Invoice_Line__c mupd = all.getmassUpdate() ;   
                List<DIB_Class_Allocation_Core> taToAlloc = new List<DIB_Class_Allocation_Core>{} ; 
                for (DIB_Class_Allocation_Core ta : all.gettoAllocate()){
                    DIB_Class_Allocation_Core tatoUp = ta ; 
                    tatoUp.setlineSelected(true) ;  
                    DIB_Invoice_Line__c ilToMUP = tatoUp.getinvl() ;
                    
                    // Test Coverage DIB_Classes 
                    tatoUp.getDepartement(ilToMup.Id) ;
                    tatoUp.getlineSelected() ; 
                    //tatoUp.getselectedSfaId() ; 
                    tatoUp.getsfaPL() ; 
                    //tatoUp.setSelectedSfaId(ilToMup.Id);          
                    ilToMUP.Sales_Force_Account__c = null;
                    ilToMUP.Department_ID__c = null ;
                    iltoMUP.Pump_Origin__c = null;
                    iltoMUP.Is_Allocated__c = false  ;              
                    tatoUp.setinvl(iltoMUP) ;   
                    taToAlloc.add(tatoUp) ;     
                    controller.setmassassignedToAllocateItem(iltoMUP.Id);                   
                }
                all.settoAllocate(taToAlloc) ; 
                // Test Coverage DIB_Classes 
                all.getsectionName(); 
                all.getId() ; 
                all.getDepartement(invoiceLines0109[0].Id) ;
                
                newAllTa3.add(all);             
            }
        }
        
        
        controller.massAllocatePUMPS() ;     
        controller.massUpdateDepartmentPUMPS() ;    
        controller.massUpdateOriginPUMPS() ;     
        controller.massUpdateSFAccountPUMPS() ; 
        controller.massUpdateDepartmentCGMS();
        
        controller.updateAllocationCGMS() ; 
        
        
        
        controller.SubmitcurrentPeriod() ;
        controller.getPeriodChosen2(); 
        controller.getPeriodItems();
        controller.Init();
        controller.getSalesFilterOptions();
        controller.getSegmentFilterOptions();
        controller.getTerritoryFilterOptions();
        controller.Init();
        controller.Init();
        controller.Init();
        System.debug(' ################## ' + 'END TEST_DIB_controllerAllocation' + ' ################');   
        
    }
}