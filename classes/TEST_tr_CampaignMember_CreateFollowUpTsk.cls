//--------------------------------------------------------------------------------------------------------------------
//  Author      : Bart Caelen
//  Version     : 1.0
//  Date        : 20150819
//  Description : This is the APEX Test Class for the APEX Trigger tr_CampaignMember_CreateFollowUpTask
//--------------------------------------------------------------------------------------------------------------------
@isTest
private class TEST_tr_CampaignMember_CreateFollowUpTsk {
    
    @TestSetup static void createTestData() {

        // Create CEE Campaign
        String tRecordTypeID_CEE = clsUtil.getRecordTypeByDevName('Campaign', 'RTG_Campaign').Id;
        List<Campaign> lstCampaign = new List<Campaign>();
        clsTestData.createCampaignData(false, tRecordTypeID_CEE);
            clsTestData.oMain_Campaign.Name = 'TEST Campaign CEE ACTIVE';
            clsTestData.oMain_Campaign.Automatically_create_follow_up_tasks__c = true;
            clsTestData.oMain_Campaign.Type = 'Training & Education';
            clsTestData.oMain_Campaign.EndDate = Date.Today().addDays(30);
            clsTestData.oMain_Campaign.Business_Unit_Picklist__c = 'Cranial Spinal';
        lstCampaign.add(clsTestData.oMain_Campaign);
        clsTestData.oMain_Campaign = null;
        clsTestData.createCampaignData(false, tRecordTypeID_CEE);
            clsTestData.oMain_Campaign.Name = 'TEST Campaign CEE NOT ACTIVE';
            clsTestData.oMain_Campaign.Automatically_create_follow_up_tasks__c = false;
            clsTestData.oMain_Campaign.Type = 'Training & Education';
            clsTestData.oMain_Campaign.EndDate = Date.Today().addDays(30);
            clsTestData.oMain_Campaign.Business_Unit_Picklist__c = 'Cranial Spinal';
        lstCampaign.add(clsTestData.oMain_Campaign);
        clsTestData.oMain_Campaign = null;
        clsTestData.createCampaignData(false, tRecordTypeID_CEE);
            clsTestData.oMain_Campaign.Name = 'TEST Campaign CEE ACTIVE ENDDATE PASSED';
            clsTestData.oMain_Campaign.Automatically_create_follow_up_tasks__c = true;
            clsTestData.oMain_Campaign.Type = 'Training & Education';
            clsTestData.oMain_Campaign.EndDate = Date.Today().addDays(-30);
            clsTestData.oMain_Campaign.Business_Unit_Picklist__c = 'Cranial Spinal';
        lstCampaign.add(clsTestData.oMain_Campaign);

        // Create NON CEE Campaign
        String tRecordTypeID_NON_CEE = clsUtil.getRecordTypeByDevName('Campaign', 'RTG_Campaign').Id;
        clsTestData.oMain_Campaign = null;
        clsTestData.createCampaignData(false, tRecordTypeID_NON_CEE);
            clsTestData.oMain_Campaign.Name = 'TEST Campaign NON CEE';
            clsTestData.oMain_Campaign.Automatically_create_follow_up_tasks__c = true;
            clsTestData.oMain_Campaign.Type = 'Training & Education';
            clsTestData.oMain_Campaign.EndDate = Date.Today().addDays(30);
            clsTestData.oMain_Campaign.Business_Unit_Picklist__c = 'Cranial Spinal';
        lstCampaign.add(clsTestData.oMain_Campaign);
        insert lstCampaign;

        lstCampaign = [SELECT Id, Name FROM Campaign];
        System.assertEquals(lstCampaign.size(), 4);

    }

    @isTest static void test_Campaign_CEE_Active() {

        List<Campaign> lstCampaign = [SELECT Id, Name, (SELECT Id, ContactId, LeadId, CreatedById FROM CampaignMembers) FROM Campaign WHERE Name = 'TEST Campaign CEE ACTIVE'];
        System.assertEquals(lstCampaign.size(), 1);
        System.assertEquals(lstCampaign[0].CampaignMembers.size(), 0);

        List<Task> lstTask = [SELECT Id, Subject, ActivityDate, ReminderDateTime, OwnerId, WhatId, WhoId FROM Task];
        System.assertEquals(lstTask.size(), 0);

        clsTestData.oMain_Campaign = lstCampaign[0];

        Test.startTest();
            clsTestData.createCampaignMemberData(true);
            ba_Campaign_AddCampaignMemberStatus batch = new ba_Campaign_AddCampaignMemberStatus();
            Database.executeBatch(batch, 200);
        Test.stopTest();

        lstCampaign = [SELECT Id, Name, (SELECT Id, ContactId, LeadId, CreatedById FROM CampaignMembers) FROM Campaign WHERE Name = 'TEST Campaign CEE ACTIVE'];
        System.assertEquals(lstCampaign.size(), 1);
        System.assertEquals(lstCampaign[0].CampaignMembers.size(), 1);

        lstTask = [SELECT Id, Subject, ActivityDate, ReminderDateTime, OwnerId, WhatId, WhoId, Campaign__c FROM Task];
        System.assertEquals(lstTask.size(), 1);

        for (Task oTask : lstTask){
            System.assertEquals(oTask.Subject, 'Perform training follow-up action');
            System.assertEquals(oTask.ActivityDate, Date.today().addDays(30).addDays(21));
            System.assertEquals(oTask.ReminderDateTime, null);
            System.assertEquals(oTask.OwnerId, lstCampaign[0].CampaignMembers[0].CreatedById);
            System.assertEquals(oTask.WhatId, lstCampaign[0].Id);
            System.assertEquals(oTask.WhoID, lstCampaign[0].CampaignMembers[0].ContactId);
        }

    }

    @isTest static void test_Campaign_CEE_Active_EndDate_Passed() {

        List<Campaign> lstCampaign = [SELECT Id, Name, (SELECT Id, ContactId, LeadId, CreatedById FROM CampaignMembers) FROM Campaign WHERE Name = 'TEST Campaign CEE ACTIVE ENDDATE PASSED'];
        System.assertEquals(lstCampaign.size(), 1);
        System.assertEquals(lstCampaign[0].CampaignMembers.size(), 0);

        List<Task> lstTask = [SELECT Id, Subject, ActivityDate, ReminderDateTime, OwnerId, WhatId, WhoId, Campaign__c FROM Task];
        System.assertEquals(lstTask.size(), 0);

        clsTestData.oMain_Campaign = lstCampaign[0];

        Test.startTest();
            clsTestData.createCampaignMemberData(true);
            ba_Campaign_AddCampaignMemberStatus batch = new ba_Campaign_AddCampaignMemberStatus();
            Database.executeBatch(batch, 200);
        Test.stopTest();

        lstCampaign = [SELECT Id, Name, (SELECT Id, ContactId, LeadId, CreatedById FROM CampaignMembers) FROM Campaign WHERE Name = 'TEST Campaign CEE ACTIVE ENDDATE PASSED'];
        System.assertEquals(lstCampaign.size(), 1);
        System.assertEquals(lstCampaign[0].CampaignMembers.size(), 1);

        lstTask = [SELECT Id, Subject, ActivityDate, ReminderDateTime, OwnerId, WhatId, WhoId, Campaign__c FROM Task];
        System.assertEquals(lstTask.size(), 1);

        for (Task oTask : lstTask){
            System.assertEquals(oTask.Subject, 'Perform training follow-up action');
            System.assertEquals(oTask.ActivityDate, Date.today());
            System.assertEquals(oTask.ReminderDateTime, null);
            System.assertEquals(oTask.OwnerId, lstCampaign[0].CampaignMembers[0].CreatedById);
            System.assertEquals(oTask.WhatId, lstCampaign[0].Id);
            System.assertEquals(oTask.WhoID, lstCampaign[0].CampaignMembers[0].ContactId);
        }

    }    
    
    @isTest static void test_Campaign_CEE_Not_Active() {

        List<Campaign> lstCampaign = [SELECT Id, Name, (SELECT Id, ContactId, LeadId, CreatedById FROM CampaignMembers) FROM Campaign WHERE Name = 'TEST Campaign CEE NOT ACTIVE'];
        System.assertEquals(lstCampaign.size(), 1);
        System.assertEquals(lstCampaign[0].CampaignMembers.size(), 0);

        List<Task> lstTask = [SELECT Id, Subject, ActivityDate, ReminderDateTime, OwnerId, WhatId, WhoId, Campaign__c FROM Task];
        System.assertEquals(lstTask.size(), 0);

        clsTestData.oMain_Campaign = lstCampaign[0];

        Test.startTest();
            clsTestData.createCampaignMemberData(true);
        Test.stopTest();

        lstCampaign = [SELECT Id, Name, (SELECT Id, ContactId, LeadId, CreatedById FROM CampaignMembers) FROM Campaign WHERE Name = 'TEST Campaign CEE NOT ACTIVE'];
        System.assertEquals(lstCampaign.size(), 1);
        System.assertEquals(lstCampaign[0].CampaignMembers.size(), 1);

        lstTask = [SELECT Id, Subject, ActivityDate, ReminderDateTime, OwnerId, WhatId, WhoId, Campaign__c FROM Task];
        System.assertEquals(lstTask.size(), 0);

    }   

}