@isTest
public class Test_Contract_Repository_Bundle {
    
    private static testmethod void protectInterfacedBundles(){
    	
    	User salesRep;
    	
    	User currentUser = new USer(Id = UserInfo.getUserId());
    	
    	System.runAs(currentUser){
    		
    		salesRep = createTestUser();
    	}
    	
    	User devart = [Select Id from User where Profile.Name IN ('Devart Tool' , 'Devart Tool - Digic') AND IsActive = true LIMIT 1];
    	
    	Contract_Repository_Bundle__c interfaceBundle = new Contract_Repository_Bundle__c();
    	
    	System.runAs(devart){
    	    		
    		interfaceBundle.Name = 'Unit Test bundle interfaced';
    		interfaceBundle.OwnerId = salesRep.Id;
    		insert interfaceBundle;
    	}
    	
    	Contract_Repository_Bundle__c regularBundle = new Contract_Repository_Bundle__c();
    	regularBundle.Name = 'Unit Test bundle';
    	regularBundle.OwnerId = salesRep.Id;
    	insert regularBundle;
    	   
    	System.runAs(salesRep){
    		
    		Boolean failed = false;
    		
    		try{
    			
    			interfaceBundle.Name = 'New name';
    			update interfaceBundle;
    			
    		}catch(Exception e){
    			
    			failed = true;
    			System.assert(e.getMessage().contains('This is an interfaced Bundle'));
    		}
    		
    		System.assert(failed == true);
    		
    		failed = false;
    		
    		regularBundle.Name = 'New name';
			update regularBundle;
			
			try{
    			    			
    			delete interfaceBundle;
    			
    		}catch(Exception e){
    			
    			failed = true;
    			System.assert(e.getMessage().contains('This is an interfaced Bundle'));
    		}
    		
    		System.assert(failed == true);
    		
    		delete regularBundle;
    	}
    	
    }
    
    public static User createTestUser(){
    	    	    	
    	PermissionSet ps = [Select Id from PermissionSet where Name = 'Contract_Repository_Create'];
    	Profile cvgSR = [Select Id from Profile where Name = 'EUR Field Force CVG'];
    	
    	User testUser = new User();    	
    	testUser.FirstName = 'Unit Test';
		testUser.LastName = 'User';
		testUser.ProfileId = cvgSR.Id;
		testUser.CommunityNickname = 'uniTUser';
		testUser.Alias_unique__c = 'uniTUser123456';
		testUser.Username = 'unit.test.user@medtronic.com.unittest'; 
		testUser.Email = 'unit.test.user@medtronic.com';
		testUser.Alias = 'uniTUser';
		testUser.TimeZoneSidKey = 'Europe/Amsterdam'; 
		testUser.LocaleSidKey = 'en_US';
		testUser.EmailEncodingKey =  'ISO-8859-1';
		testUser.LanguageLocaleKey = 'en_US';
		testUser.CostCenter__c = '123456';
    	insert testUser;
    	
    	PermissionSetAssignment psAssig = new PermissionSetAssignment();
    	psAssig.PermissionSetId = ps.Id;
    	psAssig.AssigneeId = testUser.Id;
    	insert psAssig;
    	
    	return testUser;
    }
}