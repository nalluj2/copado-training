//--------------------------------------------------------------------------------------------------------------------
//  Description : This TEST APEX Class is used to test the logic in ws_TenderCalendar2Service
//  Version     : 1.0
//  Author      : Bart Caelen
//  Date        : 16/09/2016
//--------------------------------------------------------------------------------------------------------------------
@isTest private class Test_ws_TenderCalendar2Service {
	
	@isTest static void createTestData() {

		//---------------------------------------------------------------------
		// Create Test Data
		//---------------------------------------------------------------------
		clsTestData_MasterData.tCompanyCode = 'EUR';
		clsTestData_MasterData.tBusinessUnitName = 'CRHF';
		// Create Tender_Calendar_2__c Data
		clsTestData_TenderCalendar.createTenderCalendar2(false);
		clsTestData_TenderCalendar.oMain_TenderCalendar2.Mobile_ID__c = 'MobID001';
		insert clsTestData_TenderCalendar.oMain_TenderCalendar2;
		//---------------------------------------------------------------------

	}

	@isTest static void test_doPost_1(){

		createTestData();

		Test.startTest();

		RestRequest oRestRequest = new RestRequest(); 
		RestResponse oRestResponse = new RestResponse();

		ws_TenderCalendar2Service.IFTenderCalendar2Request oIFTenderCalendar2Request = new ws_TenderCalendar2Service.IFTenderCalendar2Request();
			oIFTenderCalendar2Request.id = clsTestData_TenderCalendar.oMain_TenderCalendar2.Mobile_ID__c;
			oIFTenderCalendar2Request.tenderCalendar2 = clsTestData_TenderCalendar.oMain_TenderCalendar2;
		oRestRequest.requestBody = Blob.valueOf(JSON.serialize(oIFTenderCalendar2Request));

		oRestRequest.requestURI = 'https://cs9.salesforce.com/services/apexrest/TenderCalendar2Service/';  
		oRestRequest.httpMethod = 'POST';
		RestContext.request = oRestRequest;
		RestContext.response = oRestResponse;

		String tResult = ws_TenderCalendar2Service.doPost();

		ws_TenderCalendar2Service.IFTenderCalendar2Response oIFTenderCalendar2Response = (ws_TenderCalendar2Service.IFTenderCalendar2Response)JSON.deserialize(tResult, ws_TenderCalendar2Service.IFTenderCalendar2Response.class);
		Test.stopTest();

		System.assertEquals(true, oIFTenderCalendar2Response.success);

	}

	@isTest static void test_doPost_2(){

		createTestData();

		Test.startTest();

		RestRequest oRestRequest = new RestRequest(); 
		RestResponse oRestResponse = new RestResponse();

		ws_TenderCalendar2Service.IFTenderCalendar2Request oIFTenderCalendar2Request = new ws_TenderCalendar2Service.IFTenderCalendar2Request();
			oIFTenderCalendar2Request.id = 'MobID002';
			oIFTenderCalendar2Request.tenderCalendar2 = new Tender_Calendar_2__c();
		oRestRequest.requestBody = Blob.valueOf(JSON.serialize(oIFTenderCalendar2Request));

		oRestRequest.requestURI = 'https://cs9.salesforce.com/services/apexrest/TenderCalendar2Service/';  
		oRestRequest.httpMethod = 'POST';
		RestContext.request = oRestRequest;
		RestContext.response = oRestResponse;

		String tResult = ws_TenderCalendar2Service.doPost();

		ws_TenderCalendar2Service.IFTenderCalendar2Response oIFTenderCalendar2Response = (ws_TenderCalendar2Service.IFTenderCalendar2Response)JSON.deserialize(tResult, ws_TenderCalendar2Service.IFTenderCalendar2Response.class);
		Test.stopTest();

		System.assertEquals(false, oIFTenderCalendar2Response.success);

	}	

	@isTest static void test_doDelete_1(){

		createTestData();

		Test.startTest();

		RestRequest oRestRequest = new RestRequest(); 
		RestResponse oRestResponse = new RestResponse();

		ws_TenderCalendar2Service.IFTenderCalendar2DeleteRequest oIFTenderCalendar2DeleteRequest = new ws_TenderCalendar2Service.IFTenderCalendar2DeleteRequest();
			oIFTenderCalendar2DeleteRequest.id = clsTestData_TenderCalendar.oMain_TenderCalendar2.Mobile_ID__c;
			oIFTenderCalendar2DeleteRequest.tenderCalendar2 = clsTestData_TenderCalendar.oMain_TenderCalendar2;
		oRestRequest.requestBody = Blob.valueOf(JSON.serialize(oIFTenderCalendar2DeleteRequest));

		oRestRequest.requestURI = 'https://cs9.salesforce.com/services/apexrest/TenderCalendar2Service/';  
		oRestRequest.httpMethod = 'POST';
		RestContext.request = oRestRequest;
		RestContext.response = oRestResponse;

		String tResult = ws_TenderCalendar2Service.doDelete();

		ws_TenderCalendar2Service.IFTenderCalendar2DeleteResponse oIFTenderCalendar2DeleteResponse = new ws_TenderCalendar2Service.IFTenderCalendar2DeleteResponse();
			oIFTenderCalendar2DeleteResponse.tenderCalendar2 = new Tender_Calendar_2__c();
			oIFTenderCalendar2DeleteResponse.id = 'MobID003';
			oIFTenderCalendar2DeleteResponse.success = true;
			oIFTenderCalendar2DeleteResponse.message = 'Message';

		ws_TenderCalendar2Service.IFTenderCalendar2Response oIFTenderCalendar2Response = (ws_TenderCalendar2Service.IFTenderCalendar2Response)JSON.deserialize(tResult, ws_TenderCalendar2Service.IFTenderCalendar2Response.class);
		Test.stopTest();

		System.assertEquals(true, oIFTenderCalendar2Response.success);
	}

	@isTest static void test_doDelete_2(){

		createTestData();

		Test.startTest();

		RestRequest oRestRequest = new RestRequest(); 
		RestResponse oRestResponse = new RestResponse();

		ws_TenderCalendar2Service.IFTenderCalendar2DeleteRequest oIFTenderCalendar2DeleteRequest = new ws_TenderCalendar2Service.IFTenderCalendar2DeleteRequest();
			oIFTenderCalendar2DeleteRequest.id = 'MobID002';
			oIFTenderCalendar2DeleteRequest.tenderCalendar2 = new Tender_Calendar_2__c();
		oRestRequest.requestBody = Blob.valueOf(JSON.serialize(oIFTenderCalendar2DeleteRequest));

		oRestRequest.requestURI = 'https://cs9.salesforce.com/services/apexrest/TenderCalendar2Service/';  
		oRestRequest.httpMethod = 'POST';
		RestContext.request = oRestRequest;
		RestContext.response = oRestResponse;

		String tResult = ws_TenderCalendar2Service.doDelete();

		ws_TenderCalendar2Service.IFTenderCalendar2Response oIFTenderCalendar2Response = (ws_TenderCalendar2Service.IFTenderCalendar2Response)JSON.deserialize(tResult, ws_TenderCalendar2Service.IFTenderCalendar2Response.class);
		Test.stopTest();

		System.assertEquals(true, oIFTenderCalendar2Response.success);
		System.assertEquals('Tender Calendar not found', oIFTenderCalendar2Response.message);
	}	
	
}
//--------------------------------------------------------------------------------------------------------------------