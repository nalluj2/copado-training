global class Contract_Batchupdate implements Database.Batchable<sObject>, Schedulable{
   	global String Query;
  
	global void execute(SchedulableContext sc){
        database.executeBatch(new Contract_Batchupdate(),1); 
    }

   	global Database.QueryLocator start(Database.BatchableContext BC){
   		query='Select id,EndDate,Initial_Surgery_Coverages__c,Initial_FSE_Visits__c, Anniversary__c, FSE_Visits_Expired__c, Total_of_FSE_Visits_Purchased__c, of_FSE_Visits_Used__c, Surgery_Coverages_Expired__c, Total_of_Surgery_Coverages_Purchased__c, of_Surgery_Coverages_Used__c, Additional_FSE_Visits_Purchased__c, Additional_Surgery_Coverages_Purchased__c from Contract where Anniversary__c = Today';
  
   		return Database.getQueryLocator(query);  
   	}

   	global void execute(Database.BatchableContext BC, 
                       List<Contract> scope){
     for(Contract s : scope){
			Date TempAnniversary;
            integer daysbetween;
            /*if(s.Total_of_Hotel_Nights_Purchased__c != null)
            {
                if(s.of_Hotel_Nights_Used__c == null)
                    s.of_Hotel_Nights_Used__c = 0;
                 s.Hotel_Nights_Expired__c = s.Total_of_Hotel_Nights_Purchased__c - s.of_Hotel_Nights_Used__c;
                 
            }*/
            if(s.Total_of_FSE_Visits_Purchased__c != null)  
            { 
                if(s.of_FSE_Visits_Used__c == null)
                    s.of_FSE_Visits_Used__c = 0;
                s.FSE_Visits_Expired__c=s.Total_of_FSE_Visits_Purchased__c- s.of_FSE_Visits_Used__c;
                
            }
            if(s.Total_of_Surgery_Coverages_Purchased__c != null)   
            {
                if(s.of_Surgery_Coverages_Used__c == null)
                    s.of_Surgery_Coverages_Used__c = 0;
                s.Surgery_Coverages_Expired__c = s.Total_of_Surgery_Coverages_Purchased__c - s.of_Surgery_Coverages_Used__c;
                
            }
            If(s.EndDate!= null && s.Anniversary__c != null && s.EndDate < s.Anniversary__c.addmonths(12))
            {
                TempAnniversary = s.EndDate;
                System.debug('If block, line 42: TempAnniversary 1 ');
            }
            else if(s.Anniversary__c != null)
            {
                TempAnniversary = s.Anniversary__c.addmonths(12);
                System.debug('If block, line 50: TempAnniversary 2 ');
            }
            
      
    
                 
            if(tempAnniversary!=s.Anniversary__c)
              
               {
               daysbetween = s.Anniversary__c.daysBetween(TempAnniversary);
               //System.debug('********************* ' +s.id+'***' +TempAnniversary);
               
               
               //System.debug('********************* ' +s.Anniversary__c+'Initial hotel nights' +s.Initial_Hotel_Nights__c);
               if(s.Anniversary__c != null && TempAnniversary != null)
               System.debug('Number of days between anniversary and end date: '+ daysbetween);
               
                  Decimal RefillEntitlements_SurgeryCoverage = 0;
                  Decimal RefillEntitlements_FSEvisits = 0; 
                  //Decimal RefillEntitlements_hotelnights = 0;
                
               /*If(s.Initial_Hotel_Nights__c!=0 && s.Anniversary__c != null && TempAnniversary != null){
                
                System.debug('TempAnniversary for hotel nights: ' + TempAnniversary);
                System.debug('Initial hotel nights: ' + s.Initial_Hotel_Nights__c);  
                System.debug('Days between for hotel nights: '+ daysbetween);
               
                decimal d1= (daysbetween * s.Initial_Hotel_Nights__c) / 365;
                System.debug('Computed hotel nights:' + d1);
               
                RefillEntitlements_hotelnights = d1.round();  
                System.debug('Rounded hotel nights:' + RefillEntitlements_hotelnights);
                }
                */
                
                IF(s.Initial_FSE_Visits__c!=0 && s.Anniversary__c != null && TempAnniversary != null){
                
                System.debug('TempAnniversary for FSE visits: ' + TempAnniversary);
                System.debug('Initial FSE visits: ' + s.Initial_FSE_Visits__c);    
                System.debug('Days between for FSE visits: '+ daysbetween);
                    
                decimal d2= (daysbetween * s.Initial_FSE_Visits__c) / 365;
                System.debug('Computed FSE visits:' + d2);
               
                RefillEntitlements_FSEvisits = d2.round(); 
                System.debug('Rounded FSE visits:' + RefillEntitlements_FSEvisits);
                }
                
                IF(s.Initial_Surgery_Coverages__c!=0 && s.Anniversary__c != null && TempAnniversary != null){
                         
                System.debug('TempAnniversary for surgery coverages: ' + TempAnniversary);
                System.debug('Initial surgery coverages: ' + s.Initial_Surgery_Coverages__c);   
                System.debug('Days between for surgery coverages: '+ daysbetween);
                    
                decimal d3= (daysbetween * s.Initial_Surgery_Coverages__c) / 365;
                System.debug('Computed surgery coverages:' + d3);
                    
                RefillEntitlements_SurgeryCoverage = d3.round(); 
                System.debug('Rounded Surgery coverages:' + RefillEntitlements_SurgeryCoverage);
                }
                //System.debug('s.Additional_Hotel_Nights_Purchased__c : '+s.Additional_Hotel_Nights_Purchased__c);
                System.debug('s.Additional_FSE_Visits_Purchased__c: '+s.Additional_FSE_Visits_Purchased__c);
                System.debug('s.Additional_Surgery_Coverages_Purchased__c : '+s.Additional_Surgery_Coverages_Purchased__c);
                
                /*if(s.Additional_Hotel_Nights_Purchased__c == null) 
                    s.Additional_Hotel_Nights_Purchased__c = 0;
                s.Additional_Hotel_Nights_Purchased__c = s.Additional_Hotel_Nights_Purchased__c + RefillEntitlements_hotelnights ;
                */
                
                if(s.Additional_FSE_Visits_Purchased__c == null)
                    s.Additional_FSE_Visits_Purchased__c = 0;
                s.Additional_FSE_Visits_Purchased__c = s.Additional_FSE_Visits_Purchased__c+ RefillEntitlements_FSEvisits ;
                
                if(s.Additional_Surgery_Coverages_Purchased__c == null)
                    s.Additional_Surgery_Coverages_Purchased__c = 0;
                s.Additional_Surgery_Coverages_Purchased__c = s.Additional_Surgery_Coverages_Purchased__c+ RefillEntitlements_SurgeryCoverage ;

                  //System.debug('s.Additional_Hotel_Nights_Purchased__c : '+s.Additional_Hotel_Nights_Purchased__c);
                  System.debug('s.Additional_FSE_Visits_Purchased__c: '+s.Additional_FSE_Visits_Purchased__c);
                  System.debug('s.Additional_Surgery_Coverages_Purchased__c : '+s.Additional_Surgery_Coverages_Purchased__c);
                  
                  //System.debug('RefillEntitlements_hotelnights : '+RefillEntitlements_hotelnights);
                  System.debug(' RefillEntitlements_FSEvisits : '+ RefillEntitlements_FSEvisits);
                  System.debug('RefillEntitlements_SurgeryCoverage : '+RefillEntitlements_SurgeryCoverage);
                    
            }
                     
                    s.Date_Last_Expired__c = s.Anniversary__c;       
                    s.Anniversary__c = tempAnniversary;   
                    s.entitlement_anniversary_check__c = false;    // added by wipro-- for refilling the service contract after the scheduler has run     

            
            // s.put(Field,Value); 
      }  
        update scope;
   }

   global void finish(Database.BatchableContext BC){

   }

}