@isTest
private class TEST_ctrl_MultiSelectComponent {

	static testMethod void TestUserMultiSelect() {

		//--------------------------------------------------------
		// TEST DATA
		//--------------------------------------------------------
		// set initial user
		List<User> lstUser = [SELECT Id, Name FROM User WHERE IsActive = true and UserType = 'Standard' LIMIT 1];
		//--------------------------------------------------------


		//--------------------------------------------------------
		// PERFORM TEST
		//--------------------------------------------------------
		Test.startTest(); 

		ctrl_MultiSelectComponent oCTRL = new ctrl_MultiSelectComponent();
			oCTRL.tSObject_Name = 'User';
			oCTRL.tFieldName_Id = 'Id';
			oCTRL.tFieldName_Value = 'Name';
			oCTRL.iRecord_Limit = 200;
			oCTRL.tAdditional_Where = 'Id != null';

		// instantiate lists that would be passed in
		oCTRL.lstRight_Initial = new List<String>();
		oCTRL.lstRight_Current = new List<String>();

		// initial list item should show in right list, left should be empty
		oCTRL.lstRight_Initial.add(lstUser[0].Id);
		List<SelectOption> lstSelectOption = oCTRL.lstLeft_Option;
		System.AssertEquals(0, lstSelectOption.size());
		lstSelectOption = oCTRL.lstRight_Option;
		System.AssertEquals(lstUser[0].Id, lstSelectOption[0].getValue());  // returned option list
		System.AssertEquals(lstUser[0].Id, oCTRL.lstRight_Current[0]);  // current right list

		// Execute with Initial Search
			oCTRL.bExecuteInitialSearch = true;
		lstSelectOption = oCTRL.lstLeft_Option;
		System.AssertNotEquals(0, lstSelectOption.size());

		lstSelectOption = new List<SelectOption>();

		// Run Search method, this will add entries into the list lstLeft_Option
		oCTRL.tSearchText = '';  // so search text is not null
		oCTRL.Search(); // assumes there are 2 or more standard users
		lstSelectOption = oCTRL.lstLeft_Option;
		System.AssertNotEquals(0, lstSelectOption.size());  // not 0

		// Select entry to move to right box
		oCTRL.lstLeft_Selected.add(lstSelectOption[0].getValue());
		oCTRL.ClickRight();
		lstSelectOption = oCTRL.lstRight_Option;  // update lstRight_Current with 2 selected IDs
		System.AssertEquals(2, oCTRL.lstRight_Current.size()); 

		// Select entry to move to left box
		oCTRL.lstRight_Selected.add(lstUser[0].Id);
		oCTRL.ClickLeft();
		lstSelectOption = oCTRL.lstRight_Option;  // update lstRight_Current with 1 selected IDs
		System.AssertEquals(1, oCTRL.lstRight_Current.size());
		System.AssertEquals(lstSelectOption[0].getValue(), oCTRL.lstRight_Current[0]);

		Test.stopTest();
		//--------------------------------------------------------

	}

}