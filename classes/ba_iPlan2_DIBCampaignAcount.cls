/*
 * Author 		:	Bart Caelen
 * Date			:	20140225
 * Description	:	CR-1865 - This batch/scheduled Class will collect the Accounts of all Active iPlan 2 records and verifies if the Account is linked to a
 *						DIB_Campaign_Account__c record in order to link the iPlan to this DIB_Campaign_Account__c record.
 */
global class ba_iPlan2_DIBCampaignAcount implements Database.Batchable<sObject>, Schedulable {
	
    public Date dDate = Date.today();

    // SCHEDULE Settings
    global void execute(SchedulableContext sc){
    	
        ba_iPlan2_DIBCampaignAcount oBatch = new ba_iPlan2_DIBCampaignAcount();
        Database.executebatch(oBatch, 200);         
    } 

    // BATCH Settings
    global Database.QueryLocator start(Database.BatchableContext BC) {

        String tToday = string.valueOf(dDate);

        // Select the Active DIB iPlan2 records
        String tQuery = 'SELECT ID, Account__c FROM Account_Plan_2__c ';
        tQuery += ' WHERE RecordTypeID IN (\'' + RecordTypeMedtronic.Iplan2('EUR_DIB_sBU').Id + '\', \'' + RecordTypeMedtronic.Iplan2('MEA_DIB_sBU').Id + '\') AND Sub_Business_Unit__r.Name = \'Diabetes Core\' AND Sub_Business_Unit__r.Business_Unit__r.Company__r.Name IN (\'Europe\', \'MEA\')';
        
    	return Database.getQueryLocator(tQuery);
  	}

    global void execute(Database.BatchableContext BC, List<Account_Plan_2__c> lstAccountPlan2) {

        bl_AccountPlanning.setActiveAccountPlan2OnDIBCampaignAccount(lstAccountPlan2);    	
  	}
	
  	global void finish(Database.BatchableContext BC) {
  	}
	
}