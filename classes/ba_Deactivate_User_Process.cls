//---------------------------------------------------------------------------------------------------------------------------------------------------
//  Author		:    
//  Date 		:    
//  Description	:    This batch is used to schedule when to inactivate a User
//---------------------------------------------------------------------------------------------------------------------------------------------------
//  Author		:    Bart Caelen
//  Date 		:    15/01/2020
//  Description	:    CR-15061 - rewritten the locig to include the following logic:
//						- transfer Opportunities of deactivated user to a new user
//						- create attachments related to the support ticket to store the Opportunity Transfer results
//						- send an email on Success and Failure and include the attachments
//---------------------------------------------------------------------------------------------------------------------------------------------------
global with sharing class ba_Deactivate_User_Process implements Database.Batchable<sObject>, Database.Stateful{
	
	global Id userRequestId { get; set; }

	public Boolean bUserDeactivated = false;
	public Create_User_Request__c oRequest;

	public String tError_Request;
	public String tSuccess_Opportunity;
	public String tError_Opportunity;
	public Integer iSuccess_Opportunity;
	public Integer iError_Opportunity;
	
	public ba_Deactivate_User_Process(){

		tSuccess_Opportunity = '';
		iSuccess_Opportunity = 0;
		tError_Opportunity = '';
		iError_Opportunity = 0;

	}

	global Database.QueryLocator start(Database.BatchableContext BC){
		
		String tSOQL = 'SELECT Id, Name, Created_User__c, Created_User__r.Name, Comment__c, Created_User__r.isActive, Created_User__r.User_Status__c, New_owner_of_opportunities__c';
			tSOQL += ' FROM Create_User_Request__c';
			tSOQL += ' WHERE Id = :userRequestId';	
		oRequest = Database.query(tSOQL);


		tError_Request = '';
		try{
			
			User userToDeactivate = new User(Id = oRequest.Created_User__c);
				userToDeactivate.IsActive = false;
				userToDeactivate.User_Status__c = oRequest.Comment__c;
				userToDeactivate.Deactivation_Date__c = Date.today();
			update userToDeactivate;

			bUserDeactivated = true;

		}catch(Exception oEX){
			tError_Request = 'Error while deactivating user "' + oRequest.Created_User__r.Name + '" : ' + oEX.getMessage();
		}

		return Database.getQueryLocator([SELECT Id, Name, OwnerId FROM Opportunity WHERE OwnerId = :oRequest.Created_User__c]);

	}	
	
	global void execute(Database.BatchableContext BC, List<Opportunity> lstOpportunity){
		
		if (bUserDeactivated && (oRequest.New_owner_of_opportunities__c != null) ){
			
			processOpportunity(lstOpportunity, oRequest.New_owner_of_opportunities__c);

		}
		
	}

	global void finish(Database.BatchableContext BC){
		
		tSuccess_Opportunity = 'Id,Name,OwnerId\n' + tSuccess_Opportunity;
		tError_Opportunity = 'Id,Name,OwnerId,Error\n' + tError_Opportunity;

		// Create Attachments linked to the Support Ticket - not executed in test because of MIXED_DML exception
		List<Attachment> lstAttachment = new List<Attachment>();
		if (!Test.isRunningTest()) lstAttachment = createAttachment();

		// Update Support Ticket Status - not executed in test because of MIXED_DML exception
		if (!Test.isRunningTest()) updateSupportTicket();
	
		// Create Email Subject + Email Body + Email Attachments and send the Email - not executed in test because of MIXED_DML exception in previous steps
		if (!Test.isRunningTest()) sendEmail(lstAttachment);

	}

	// I have created separated methods for each logic part so that we can test each part in the APEX Test class.
	// The reason for this is that we will run into a MIXED_DML exception because we update a User and want to update the Support Ticket and create Attachments.
	// This works when the Batch job is executed by a user but not in an APEX Test Class.
	public void processOpportunity(List<Opportunity> lstOpportunity, Id id_User_NewOwner){

		for (Opportunity oOpportunity : lstOpportunity){
			oOpportunity.OwnerId = id_User_NewOwner;
		}

		Map<Id, Opportunity> mapOpportunity = new Map<Id, Opportunity>();
			mapOpportunity.putAll(lstOpportunity);

		List<Database.SaveResult> lstSaveResult = Database.update(lstOpportunity, false);

		for (Database.SaveResult oSaveResult : lstSaveResult){

			Opportunity oOpportunity = mapOpportunity.get(oSaveResult.getId());

			if (oSaveResult.isSuccess()){

				tSuccess_Opportunity += '"' + oOpportunity.Id + '","' + oOpportunity.Name + '","' + id_User_NewOwner + '"\n';
				iSuccess_Opportunity++;

			}else{

				tError_Opportunity += '"' + oOpportunity.Id + '","' + oOpportunity.Name + '","' + id_User_NewOwner + '"';
				iError_Opportunity++;

				for (Database.Error oError : oSaveResult.getErrors()){

					tError_Opportunity += ',"' + oError.getMessage() + '"';

				}

				tError_Opportunity += '\n';

			}

		}
	}

	public List<Attachment> createAttachment(){

		// Create Attachments linked to the Support Ticket
		List<Attachment> lstAttachment = new List<Attachment>();

		if (iSuccess_Opportunity > 0){
			Attachment oAttachment_Success = new Attachment();
				oAttachment_Success.ParentId = oRequest.Id;
				oAttachment_Success.Name = 'Transfer_Opportunity_Success.csv';
				oAttachment_Success.Body = Blob.valueOf(tSuccess_Opportunity);
				oAttachment_Success.ContentType = 'text/csv';
			lstAttachment.Add(oAttachment_Success);
		}

		if (iError_Opportunity > 0){
			Attachment oAttachment_Error = new Attachment();
				oAttachment_Error.ParentId = oRequest.Id;
				oAttachment_Error.Name = 'Transfer_Opportunity_Error.csv';
				oAttachment_Error.Body = Blob.valueOf(tError_Opportunity);
				oAttachment_Error.ContentType = 'text/csv';
			lstAttachment.Add(oAttachment_Error);
		}

		if (lstAttachment.size() > 0) insert lstAttachment;

		return lstAttachment;

	}

	public void updateSupportTicket(){

		// Update Support Ticket Status
		if (bUserDeactivated && (iError_Opportunity == 0) ){
			oRequest.Status__c = 'Completed';
			oRequest.Opportunities_transferred_in_Production__c = true;
		}else{
			oRequest.Status__c = 'New';
			oRequest.Opportunities_transferred_in_Production__c = false;
		}
		update oRequest;

	}
	
	public void sendEmail(List<Attachment> lstAttachment){

		// Create Email Subject + Email Body + Email Attachments and send the Email
		String tEmailSubject = '';
		String tEmailBody = 'Dear ' + UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
		tEmailBody += '<br /><br />';
		if (oRequest.Status__c == 'Completed'){

			tEmailBody += 'The user ' + oRequest.Created_User__r.Name + ' has been successfully deactivated.';
			tEmailSubject = oRequest.Name + ' : The user ' + oRequest.Created_User__r.Name + ' has been successfully deactivated';

		}else{
			
			tEmailBody += 'The process to inactivate the user ' + oRequest.Created_User__r.Name + ' has failed.';
			tEmailSubject = oRequest.Name + ' : The process to inactivate the user ' + oRequest.Created_User__r.Name + ' has failed';
			
		}

		if (!String.isBlank(tError_Request)) tEmailBody += '<br />Error : ' + tError_Request;
		tEmailBody += '<br /><br />';

		if (iSuccess_Opportunity > 0 || iError_Opportunity > 0){

			tEmailBody += 'Transfered Opportunities to new owner :';
			tEmailBody += '<br /><br />Success : '+  iSuccess_Opportunity;
			tEmailBody += '<br />Error : ' + iError_Opportunity;

		}else{

			tEmailBody += 'No Opportunities found to transfer to the new user.';

		}


		List<Messaging.EmailFileAttachment> lstEmailAttachment = new List<Messaging.EmailFileAttachment>();
		for (Attachment oAttachment : lstAttachment){

			Messaging.EmailFileAttachment oEmailFileAttachment = new Messaging.EmailFileAttachment();
				oEmailFileAttachment.setFileName(oAttachment.Name);
				oEmailFileAttachment.setBody(oAttachment.Body);
				oEmailFileAttachment.setContentType(oAttachment.ContentType);
			lstEmailAttachment.add(oEmailFileAttachment);
		
		}

		Messaging.SingleEmailMessage oEmail = new Messaging.SingleEmailMessage();
			oEmail.setHtmlBody(tEmailBody);
			oEmail.setTargetObjectId(UserInfo.getUserId());
			oEmail.setSubject(tEmailSubject);
			oEmail.setSaveAsActivity(false);
			oEmail.setFileAttachments(lstEmailAttachment);
		Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{ oEmail });
	
	}

}
//---------------------------------------------------------------------------------------------------------------------------------------------------