global without sharing class ba_ContactsPerCountry implements Database.Batchable<SObject>, Database.Stateful{

	global Integer noCountryMatch;
	global Map<String, Integer> contactsPerCountry;

	global Database.QueryLocator start(Database.BatchableContext BC){
      	
      	noCountryMatch = 0;
      	contactsPerCountry = new Map<String, Integer>();
      	
      	for(DIB_Country__c country : [Select Name from DIB_Country__c]){
      		contactsPerCountry.put(country.Name.toUpperCase(), 0);
      	}	      	
      	
      	//We do not count Patients, Patient Advocates, MDT Employees and Agents as Contacts
      	
      	String query = 'SELECT MailingCountry, Account.Account_Country_vs__c FROM Contact ';
        query += 'where RecordTypeId != null AND RecordType.DeveloperName NOT IN (\'MDT_Employee\', \'MDT_Agent_Distributor\')' ;  
    	
    	return Database.getQueryLocator(query);
   	}

	global void execute(Database.BatchableContext BC, List<SObject> scope){
		
		List<Contact> contacts = (List<Contact>) scope;
		
		for(Contact cont : contacts){
		
			String contactCountry;
					
			if(cont.MailingCountry != null && contactsPerCountry.containsKey(cont.MailingCountry.toUpperCase())){
				contactCountry = cont.MailingCountry.toUpperCase();
			}else if(cont.Account != null && cont.Account.Account_Country_vs__c != null && contactsPerCountry.containsKey(cont.Account.Account_Country_vs__c.toUpperCase())){
				contactCountry = cont.Account.Account_Country_vs__c.toUpperCase();
			}
			
			if(contactCountry == null){
				noCountryMatch++;
			}else{
				Integer countryContacts = contactsPerCountry.get(contactCountry);
				countryContacts++;
				contactsPerCountry.put(contactCountry, countryContacts);
			}			
		}				
	}

   	global void finish(Database.BatchableContext BC){
   				
		ba_ContactDuplicatesPerCountry nextBatch = new ba_ContactDuplicatesPerCountry();
		nextBatch.setContactsPerCountry(contactsPerCountry);	
		
		Database.executeBatch(nextBatch, 2000);			
   	}
}