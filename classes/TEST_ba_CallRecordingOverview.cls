@isTest 
private class TEST_ba_CallRecordingOverview {

	@isTest
	private static void test_ba_CallRecordingOverview() {

        //----------------------------------------------------
		// Create Test Data
        //----------------------------------------------------
        // Master Data
		clsTestData_MasterData.createCompany();
		clsTestData_MasterData.createBusinessUnit();
		clsTestData_MasterData.createSubBusinessUnit();
		// Account
		clsTestData_Account.iRecord_Account = 10;
		clsTestData_Account.createAccount();
		// Contact
		clsTestData_Contact.iRecord_Contact = 10;
		clsTestData_Contact.createContact();
		// Contact Visit Report
		clsTestData_CallRecord.createContactVisitReport();
		// Create Call Record
		clsTestData_CallRecord.createCallRecord();

		// Account Call Topic Count
        List<Account_Call_Topic_Count__c> lstAccountCallTopicCount = new List<Account_Call_Topic_Count__c>();
        for (Integer i=0; i<=10; i++){

        	Account_Call_Topic_Count__c oAccountCallTopicCount_BU = new Account_Call_Topic_Count__c();
        		oAccountCallTopicCount_BU.Account__c = clsTestData_Account.oMain_Account.Id;
        		oAccountCallTopicCount_BU.active__c = true;
        		oAccountCallTopicCount_BU.Call_Recording__c = clsTestData_CallRecord.oMain_CallRecord.Id;
				oAccountCallTopicCount_BU.Business_Unit__c = clsTestData_MasterData.oMain_BusinessUnit.Id;
        	lstAccountCallTopicCount.add(oAccountCallTopicCount_BU);

        	Account_Call_Topic_Count__c oAccountCallTopicCount_SBU = new Account_Call_Topic_Count__c();
        		oAccountCallTopicCount_SBU.Account__c = clsTestData_Account.oMain_Account.Id;
        		oAccountCallTopicCount_SBU.active__c = true;
        		oAccountCallTopicCount_SBU.Call_Recording__c = clsTestData_CallRecord.oMain_CallRecord.Id;
				oAccountCallTopicCount_SBU.Sub_Business_Unit__c = clsTestData_MasterData.oMain_SubBusinessUnit.Id;
        	lstAccountCallTopicCount.add(oAccountCallTopicCount_SBU);

        }
        insert lstAccountCallTopicCount;

		// Contact Call Topic Count
        List<Contact_Call_Topic_Count__c> lstContactCallTopicCount = new List<Contact_Call_Topic_Count__c>();
        for (Integer i=0; i<=10; i++){

        	Contact_Call_Topic_Count__c oContactCallTopicCount_BU = new Contact_Call_Topic_Count__c();
        		oContactCallTopicCount_BU.Contact__c = clsTestData_Contact.oMain_Contact.Id;
        		oContactCallTopicCount_BU.active__c = true;
        		oContactCallTopicCount_BU.Call_Recording__c = clsTestData_CallRecord.oMain_CallRecord.Id;
				oContactCallTopicCount_BU.Business_Unit__c = clsTestData_MasterData.oMain_BusinessUnit.Id;
        	lstContactCallTopicCount.add(oContactCallTopicCount_BU);

        	Contact_Call_Topic_Count__c oContactCallTopicCount_SBU = new Contact_Call_Topic_Count__c();
        		oContactCallTopicCount_SBU.Contact__c = clsTestData_Contact.oMain_Contact.Id;
        		oContactCallTopicCount_SBU.active__c = true;
        		oContactCallTopicCount_SBU.Call_Recording__c = clsTestData_CallRecord.oMain_CallRecord.Id;
				oContactCallTopicCount_SBU.Sub_Business_Unit__c = clsTestData_MasterData.oMain_SubBusinessUnit.Id;
        	lstContactCallTopicCount.add(oContactCallTopicCount_SBU);

        }
        insert lstContactCallTopicCount;


		Set<Id> setID_Account_DeleteData = new Set<Id>();
		List<Call_Recording_Overview__c> lstCallRecoringOverview_Account = new List<Call_Recording_Overview__c>();
		for (Account oAccount : clsTestData_Account.lstAccount){
				
			if (oAccount.Id == clsTestData_Account.oMain_Account.Id) continue;

			Call_Recording_Overview__c oCRO = new Call_Recording_Overview__c();
				oCRO.Account__c = oAccount.Id;
			lstCallRecoringOverview_Account.add(oCRO);
			setID_Account_DeleteData.add(oAccount.Id);

		}
		insert lstCallRecoringOverview_Account;

		Set<Id> setID_Contact_DeleteData = new Set<Id>();
		List<Call_Recording_Overview__c> lstCallRecoringOverview_Contact = new List<Call_Recording_Overview__c>();
		for (Contact oContact : clsTestData_Contact.lstContact){
				
			if (oContact.Id == clsTestData_Contact.oMain_Contact.Id) continue;

			Call_Recording_Overview__c oCRO = new Call_Recording_Overview__c();
				oCRO.Contact__c = oContact.Id;
			lstCallRecoringOverview_Contact.add(oCRO);

			setID_Contact_DeleteData.add(oContact.Id);

		}
		insert lstCallRecoringOverview_Contact;
        //----------------------------------------------------


        //----------------------------------------------------
		// Execute Logic
        //----------------------------------------------------
		Test.startTest();
       		
			// Executing ba_CallRecordingOverview - Account
	        ba_CallRecordingOverview oBatch_Account1 = new ba_CallRecordingOverview();   
				oBatch_Account1.tParentSObjectType = 'ACCOUNT';
				oBatch_Account1.tAction = 'COLLECTID';
			Database.executeBatch(oBatch_Account1, 200);

	        ba_CallRecordingOverview oBatch_Account2 = new ba_CallRecordingOverview();   
				oBatch_Account2.tParentSObjectType = 'ACCOUNT';
				oBatch_Account2.tAction = 'DELETEDATA';
				oBatch_Account2.setID_Collected = setID_Account_DeleteData;
			Database.executeBatch(oBatch_Account2, 200);

	        ba_CallRecordingOverview oBatch_Account3 = new ba_CallRecordingOverview();   
				oBatch_Account3.tParentSObjectType = 'ACCOUNT';
				oBatch_Account3.tAction = 'PROCESSDATA';
				oBatch_Account3.setID_Collected = new Set<Id>{clsTestData_Account.oMain_Account.Id};
			Database.executeBatch(oBatch_Account3, 200);

	        
			// Executing ba_CallRecordingOverview - Contact
	        ba_CallRecordingOverview oBatch_Contact1 = new ba_CallRecordingOverview();   
				oBatch_Contact1.tParentSObjectType = 'CONTACT';
				oBatch_Contact1.tAction = 'COLLECTID';
			Database.executeBatch(oBatch_Contact1, 200);

	        ba_CallRecordingOverview oBatch_Contact2 = new ba_CallRecordingOverview();   
				oBatch_Contact2.tParentSObjectType = 'CONTACT';
				oBatch_Contact2.tAction = 'DELETEDATA';
				oBatch_Contact2.setID_Collected = setID_Contact_DeleteData;
			Database.executeBatch(oBatch_Contact2, 200);

			ba_CallRecordingOverview oBatch_Contact3 = new ba_CallRecordingOverview();   
				oBatch_Contact3.tParentSObjectType = 'CONTACT';
				oBatch_Contact3.tAction = 'PROCESSDATA';
				oBatch_Contact3.setID_Collected = new Set<Id>{clsTestData_Contact.oMain_Contact.Id};
			Database.executeBatch(oBatch_Contact3, 200);

        Test.stopTest();  
        //----------------------------------------------------


        //----------------------------------------------------
        //----------------------------------------------------

	}

}