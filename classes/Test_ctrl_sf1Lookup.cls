@isTest
private with sharing class Test_ctrl_sf1Lookup {
	
	private static testmethod void testController(){
		
		Account acc = new Account();
    	acc.RecordTypeId = [select Id from RecordType where DeveloperName = 'CAN_Buying_Group' and sObjectType = 'Account' and isActive = true].Id;
    	acc.Name = 'UnitTest Account Name';
    	acc.Type = 'Buying Group';
    	acc.Account_Country_vs__c = 'CANADA';
    	
    	insert acc;
		
		ctrl_sf1Lookup controller = new ctrl_sf1Lookup();
		controller.objectType = 'Account';
		controller.recordInstance = acc;
		controller.fieldName = 'Id';
		
		System.assert(controller.getInputName() == 'UnitTest Account Name');
		
		controller.getRecentList();
		
		Test.setFixedSearchResults( new List<String>{acc.Id});
		
		List<Sobject> searchResult = ctrl_sf1Lookup.searchObjects('UnitTest', 'Account', '');
		
		System.assert(searchResult.size()>0);
		
		controller.setInputName('New Selected');		
	}	
}