public without sharing class SynchronizationService_Sparepart implements SynchronizationService_Object{
	
	//Set with the external Ids of the records already processed in this transaction. Used to avoid generating multiple notifications for the same change
	private Set<String> processedRecords = new Set<String>();
	private Set<Id> rtInScope {
		
		get{	
			
			if(rtInScope == null){
				
				rtInScope = new Set<Id>();
				
				for(RecordType rt : [Select Id from RecordType where SObjectType = 'Workorder_Sparepart__c' and DeveloperName IN ('Hardware', 'Inaccuracy', 'Service', 'Software')]) rtInScope.add(rt.Id);
			}
			
			return rtInScope;
		}
		
		set;
	}
		
	//This method returns the External Ids of the records that have relevant modifications for the target Org. This includes created records or 
	//modified records where synchronized fields have changed. 
	public Set<String> hasChangeForNotification(List<SObject> records, Map<Id, SObject> oldRecords){
		
		Set<String> result = new Set<String>();
				
		for(SObject rawObject : records){
			
			Workorder_Sparepart__c record = (Workorder_Sparepart__c) rawObject;
			
			if( !rtInScope.contains(record.RecordTypeId) || processedRecords.contains(record.External_Id__c) ) continue;
			
			//On Insert we always send notification
			if(oldRecords == null){
				result.add(record.External_Id__c);
				processedRecords.add(record.External_Id__c);
			}//If Update we only send notification when relevant fields are modified
			else{
				
				if( bl_SynchronizationService_Utils.hasChanged( record, oldRecords.get(record.Id), syncFields ) == true ){
					
					result.add(record.External_Id__c);
					processedRecords.add(record.External_Id__c);
				}
			}
		}
		
		return result;
	}
	
	//This method collects and returns the complete information for a record. This includes the records itself as well as any related record that needs
	//to exist in the target Org too. The returned format is JSON serialized string.
	public String generatePayload(String externalId){
		
		SparepartSyncPayload payload = new SparepartSyncPayload();
		
		String query = 	'SELECT ' + String.join( syncFields, ',') + 
						',Account__r.SAP_Id__c, Account__r.AccountNumber, Account__r.RecordType.DeveloperName, Owner__r.External_Id__c, Asset__r.Serial_Nr__c' +						
						',Order_Part__r.CFN_Code_Text__c, RI_Part__r.CFN_Code_Text__c, Parts_Shipment__r.External_Id__c,Related_Case_Part__r.External_Id__c' +
						',Case__r.External_Id__c, Complaint__r.External_Id__c, Software__r.External_Id__c,Workorder__r.External_Id__c, RecordType.DeveloperName' +
						' FROM Workorder_Sparepart__c WHERE External_Id__c = :externalId LIMIT 2';
						
		List<Workorder_Sparepart__c> spareparts = Database.query(query);
				
		//If no record or more than 1 is found, we return an empty response
		if(spareparts.size() == 1){
			
			Workorder_Sparepart__c sparepart = spareparts[0];	
			bl_SynchronizationService_Utils.prepareForJSON(sparepart, syncFields);		
			sparepart.Id = null;
			
			payload.Sparepart = sparepart;
			
			Set<Id> accountIds = new Set<Id>();
			Set<Id> contactIds = new Set<Id>();
			
			//Account		
			if(sparepart.Account__r != null){
				
				accountIds.add(sparepart.Account__c);
				
				if ( (sparepart.Account__r.RecordType.DeveloperName == 'SAP_Account') || (sparepart.Account__r.RecordType.DeveloperName == 'Distributor') ){
					payload.AccountId = sparepart.Account__r.SAP_Id__c;	
				}else{
					payload.AccountId = sparepart.Account__r.AccountNumber;
				}
				sparepart.Account__c = null;
				sparepart.Account__r = null;							
			}
						
			//Owner		
			if(sparepart.Owner__r != null){
				
				contactIds.add(sparepart.Owner__c);
				
				payload.OwnerId = sparepart.Owner__r.External_Id__c;							
				sparepart.Owner__c = null;
				sparepart.Owner__r = null;				
			}
						
			//Asset
			if(sparepart.Asset__r != null){
				
				payload.assetId = sparepart.Asset__r.Serial_Nr__c;
				sparepart.Asset__r = null;
				sparepart.Asset__c = null;
			}			
			
			//Case
			if(sparepart.Case__r != null){
				
				payload.caseId = sparepart.Case__r.External_Id__c;
				sparepart.Case__r = null;
				sparepart.Case__c = null;
			}
			
			//Complaint
			if(sparepart.Complaint__r != null){
				
				payload.complaintId = sparepart.Complaint__r.External_Id__C;
				sparepart.Complaint__r = null;
				sparepart.Complaint__c = null;
			}
			
			//Parts Shipment
			if(sparepart.Parts_Shipment__r != null){
				
				payload.partShipmentId = sparepart.Parts_Shipment__r.External_Id__c;
				sparepart.Parts_Shipment__c = null;
				sparepart.Parts_Shipment__r = null;
			}
			
			//Software
			if(sparepart.Software__r != null){
				
				payload.softwareId = sparepart.Software__r.External_Id__c;
				sparepart.Software__c = null;
				sparepart.Software__r = null;
			}	
			
			//Related Sparepart
			if(sparepart.Related_Case_Part__r != null){
				
				payload.relSparepartId = sparepart.Related_Case_Part__r.External_Id__c;
				sparepart.Related_Case_Part__c = null;
				sparepart.Related_Case_Part__r = null;
			}			
			
			//Work Order
			if(sparepart.Workorder__r != null){
				
				payload.workorderId = sparepart.Workorder__r.External_Id__c;
				sparepart.Workorder__r = null;
				sparepart.Workorder__c = null;
			}
			
			//Order Part		
			if(sparepart.Order_Part__r != null){
				
				payload.opProductId = sparepart.Order_Part__r.CFN_Code_Text__c;				
				sparepart.Order_Part__r = null;
				sparepart.Order_Part__c = null;	
			}
			
			//RI Part		
			if(sparepart.RI_Part__r != null){
				
				payload.riProductId = sparepart.RI_Part__r.CFN_Code_Text__c;				
				sparepart.RI_Part__r = null;
				sparepart.RI_Part__c = null;	
			}
			
			//Record Type			
			payload.RecordType = sparepart.RecordType.DeveloperName;
			sparepart.RecordTypeId = null;
			sparepart.RecordType = null;
			
			//Done in a separate query because we cannot clear the value from the CreatedDate field before sending it to the target
			Workorder_Sparepart__c sparepartDetails = [Select Name, CreatedDate, CreatedBy.Name, LastModifiedDate, LastModifiedBy.Name from Workorder_Sparepart__c where External_Id__c = :externalId];
						
			payload.PartName = sparepartDetails.Name;						
			payload.CreatedDate = sparepartDetails.CreatedDate;			
			payload.CreatedBy = sparepartDetails.CreatedBy.Name;					
			payload.LastModifiedDate = sparepartDetails.LastModifiedDate;			
			payload.LastModifiedBy = sparepartDetails.LastModifiedBy.Name;							
			
			payload.generateAccountPayload(accountIds, contactIds);
		}
		
		return JSON.serialize(payload);
	}
	
	//Implementation needed only in Target Org
	public String processPayload(String inputPayload){						
		
		String processedPayload = bl_SynchronizationService_Utils.translatePayload(inputPayload, fieldMapping);
		
		SparepartSyncPayload payload = (SparepartSyncPayload) JSON.deserialize(processedPayload, SparepartSyncPayload.class);
		
		//If there was a problem in the source generating the information (no record or multiple records found) we finish here 
		if(payload.Sparepart == null) return null;
							
		Workorder_Sparepart__c sparepart = payload.Sparepart;
		
		//Received by						
		sparepart.Received_By_Text__c = payload.ReceivedBy;					
		//Tested By					
		sparepart.Tested_By_Text__c = payload.TestedBy;			
		
		sparepart.LastModifiedDate__c = payload.LastModifiedDate;
		sparepart.LastModifiedBy__c = payload.LastModifiedBy;
		
		upsert sparepart External_Id__c;
			
		return sparepart.Id;			
	}
	
	//Model classes for the requests and responses. To be used by the JSON serialize/deserialize
	public class SparepartSyncPayload extends SynchronizationService_Payload{
		
		public Workorder_Sparepart__c sparepart {get; set;}
		public String partName {get; set;}	
		public String accountId {get; set;}	
		public String ownerId {get; set;}
		public String receivedBy {get; set;}	
		public String testedBy {get; set;}		
		public String assetId {get; set;}	
		public String caseId {get; set;}	
		public String complaintId {get; set;}
		public String opProductId {get; set;}
		public String riProductId {get; set;}
		public String partShipmentId {get; set;}
		public String relSparepartId {get; set;}
		public String softwareId {get; set;}
		public String workorderId {get; set;}
		public String recordType {get; set;}
		public Datetime createdDate {get; set;}
		public String createdBy {get; set;}
		public Datetime lastModifiedDate {get; set;}
		public String lastModifiedBy {get; set;}	
	}	
	
	private static List<String> syncFields = new List<String>
	{
		
		'Account__c',
		'Amount_of_Inaccuracy__c',
		'Asset__c',		
		'Bad_At_Install__c',
		'BAI_OOBF__c',
		'Case__c',
		'Complaint__c',	
		'Cost_Center__c',
		'Date_Replaced__c',		
		'Direction_of_Inaccuracy_1__c',
		'Direction_of_Inaccuracy_2__c',
		'Discount_Override__c',	
		'Discount_Override_Dollar__c',			
		'External_Id__c',	
		'Findings_and_Conclusions__c',	
		'Lot_Number__c',	
		'MSNT_Accounting__c',
		'Next_Replacement_Due__c',
		'No_Return_Justification__c',
		'Order_Lot_Number__c',
		'Order_Part__c',
		'Order_Quantity__c',
		'Order_Status__c',
		'Owner__c',
		'Part_Shipment__c',
		'Parts_Shipment__c',		
		'PO_Number__c',		
		'Possession__c',
		'RI_Part__c',
		'RI_Part_Quantity__c',		
		'RecordTypeId',
		'Related_Case_Part__c',	
		'Requested_Return__c',	
		'Return_Item_Status__c',
		'Return_Item_Tracking_Number__c',	
		'Return_item_Tracking_info_DC_to_MFR__c',	
		'SAP_Order_Num__c',
		'SAP_Return__c',
		'Ship_Date__c',
		'Software__c',		
		'Special_Notes__c',
		'Time_and_Materials__c',		
		'Workorder__c'

	};
	
	private static Map<String, String> fieldMapping = new Map<String, String>		
	{
		'Order_Return_Item__c' => 'Workorder_Sparepart__c',
		'Received_By__c' => 'Received_By_Text__c',
		'Received_By__r' => 'Received_By_Text__c',
		'Tested_By__c' => 'Tested_By_Text__c',
		'Tested_By__r' => 'Tested_By_Text__c'					
	};
}