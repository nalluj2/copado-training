public with sharing class ctrl_FindResponsibleSR {
    
    @AuraEnabled
    public static List<Product2> searchProducts(String searchText){
    	
    	List<List<Product2>> searchResult = [FIND :searchText IN ALL FIELDS RETURNING Product2(Id, Name, CFN_Code_Text__c, Product_Group__c, Product_Group_Name__c, Business_Unit_ID__c, Business_Unit_ID__r.Name, Business_Unit_Group__c, Business_Unit_Group__r.Name WHERE RecordType.DeveloperName = 'SAP_Product') LIMIT 1000];
    	
    	List<Product2> result = new List<Product2>();
    	
    	Set<String> alreadyAdded = new Set<String>();
    	
    	for(Product2 prod : searchResult[0]){
    		
    		String uniqueKey = prod.CFN_Code_Text__c + ':' + prod.Product_Group__c + ':' + prod.Business_Unit_ID__c + ':' + prod.Business_Unit_Group__c;
    		
    		if(alreadyAdded.add(uniqueKey)){
    			
    			result.add(prod);    			
    			if(result.size() > 200) break;
    		}
    	}
    	
    	return result;    	
    }
    
    @AuraEnabled
    public static Product2 getProduct(String productId){
    	
    	Product2 result = [Select Id, Name, CFN_Code_Text__c, Product_Group__c, Product_Group_Name__c, Business_Unit_ID__c, Business_Unit_ID__r.Name, Business_Unit_Group__c, Business_Unit_Group__r.Name from Product2 where Id = :productId];
    	
    	return result;    	
    }
    
    @AuraEnabled
    public static List<Account> searchAccounts(String searchText){
    	
    	List<Account> result;
    	
    	List<List<Account>> searchResult = [FIND :searchText IN ALL FIELDS RETURNING Account(Id, Name, SAP_Id__c, BillingCity, Account_Country_vs__c WHERE RecordType.DeveloperName IN ('SAP_Account', 'Non_SAP_Account') LIMIT 201)];
    	
    	return searchResult[0];
    }
    
    @AuraEnabled
    public static Account getAccount(String accountId){
    	
    	Account result = [Select Id, Name, SAP_ID__c, BillingCity, Account_Country_vs__c from Account where Id = :accountId];
    	
    	return result;    	
    }
    
    @AuraEnabled
    public static List<SubBusinessUnit> findSalesRep(String productGroupId, String businessUnitId, String businessUnitGroupId, String accountId){
    	
    	Set<Id> therapyGroupIds  = new Set<Id>();
    	Set<Id> subBusinessUnitIds  = new Set<Id>();
    	
    	if(productGroupId != null){
    		
    		for(Product_Group__c prodGroup : [Select Therapy_ID__r.Therapy_Group__c, Therapy_ID__r.Therapy_Group__r.Sub_Business_Unit__c from Product_Group__c where Id = :productGroupId]){
    			
    			therapyGroupIds.add(prodGroup.Therapy_ID__r.Therapy_Group__c);
    			subBusinessUnitIds.add(prodGroup.Therapy_ID__r.Therapy_Group__r.Sub_Business_Unit__c);
    		}
    		
    	}else if(businessUnitId != null){
    		
    		for(Therapy_Group__c therGroup : [Select Id, Sub_Business_Unit__c from Therapy_Group__c where Sub_Business_Unit__r.Business_Unit__c = :BusinessUnitId]){
    			
    			therapyGroupIds.add(therGroup.Id);
    			subBusinessUnitIds.add(therGroup.Sub_Business_Unit__c);
    		}
    		
    	}else if(businessUnitGroupId != null){
    		
    		for(Therapy_Group__c therGroup : [Select Id, Sub_Business_Unit__c from Therapy_Group__c where Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__c = :businessUnitGroupId]){
    			
    			therapyGroupIds.add(therGroup.Id);
    			subBusinessUnitIds.add(therGroup.Sub_Business_Unit__c);
    		}
    		    		
    	}else{
    		
    		for(Therapy_Group__c therGroup : [Select Id, Sub_Business_Unit__c from Therapy_Group__c]){
    			
    			therapyGroupIds.add(therGroup.Id);
    			subBusinessUnitIds.add(therGroup.Sub_Business_Unit__c);
    		}
    	}
    	
    	Map<Id, Account_Plan_2__c> accountPlans = new AccountPlanService().getAccountPlans(accountId, subBusinessUnitIds);
    	Map<Id, List<Account_Team_Member__c>> accTeamMembers = new Map<Id, List<Account_Team_Member__c>>();
    	    	
    	Set<String> alreadyAdded = new Set<String>();
    	
    	for(Account_Team_Member__c teamMember : [Select User__c, User__r.Name, Therapy_Group__r.Sub_Business_Unit__c, Therapy_Group__r.Sub_Business_Unit__r.Name, Job_Title__c, Email__c, Phone__c 
    												from Account_Team_Member__c where Account__c = :accountId AND Active__c = true AND Therapy_Group__c IN :therapyGroupIds
    												ORDER BY Therapy_Group__r.Sub_Business_Unit__r.Name, User__r.Name]){
    		
    		if(alreadyAdded.add(teamMember.User__c + ':' + teamMember.Therapy_Group__r.Sub_Business_Unit__c) == false) continue;
    					
			List<Account_Team_Member__c> sbuAccountTM = accTeamMembers.get(teamMember.Therapy_Group__r.Sub_Business_Unit__c);
			
			if(sbuAccountTM == null){
				
				sbuAccountTM = new List<Account_Team_Member__c>();
				accTeamMembers.put(teamMember.Therapy_Group__r.Sub_Business_Unit__c, sbuAccountTM);
			}
			
			sbuAccountTM.add(teamMember);			
    	}
    	
    	List<SubBusinessUnit> result = new List<SubBusinessUnit>();
    	
    	for(Sub_Business_Units__c sbu : [Select Name from Sub_Business_Units__c where Id IN :subBusinessUnitIds ORDER BY Name]){
    		
    		Account_Plan_2__c accPlan = accountPlans.get(sbu.Id);
    		List<Account_Team_Member__c> teamMembers = accTeamMembers.get(sbu.Id);
    		
    		if(accPlan != null || teamMembers != null){
    			
    			SubBusinessUnit accountSBU = new SubBusinessUnit();
				accountSBU.Name = sbu.Name;
				accountSBU.SalesReps = new List<SalesRep>();
				
				if(accPlan != null){
					
					accountSBU.AccountPlanId = accPlan.Id;
					accountSBU.AccountPlanName = accPlan.Name;
				}	
				
				if(teamMembers != null){
					
					for(Account_Team_Member__c accTeamMember : teamMembers){
					
						SalesRep salesRep = new SalesRep();
						salesRep.Id = accTeamMember.User__c;
						salesRep.Name = accTeamMember.User__r.Name;
						salesRep.JobTitle = accTeamMember.Job_Title__c;
						salesRep.Email = accTeamMember.Email__c;
						salesRep.Phone = accTeamMember.Phone__c;					
												
						accountSBU.SalesReps.add(salesRep);					
					}					
				}
				
				result.add(accountSBU);
    		}
    	}
    	
    	return result;    	
    }
    
    public class SubBusinessUnit{
    	    	
    	@AuraEnabled public String Name {get; set;}
    	@AuraEnabled public String AccountPlanId {get; set;}
    	@AuraEnabled public String AccountPlanName {get; set;}
    	@AuraEnabled public List<SalesRep> SalesReps {get; set;}  
    }
    
    public class SalesRep{
    	
    	@AuraEnabled public String Id {get; set;}
    	@AuraEnabled public String Name {get; set;}
    	@AuraEnabled public String JobTitle {get; set;}
    	@AuraEnabled public String Email {get; set;}
    	@AuraEnabled public String Phone {get; set;}   	  	    	
    }
    
    
    
    public without sharing class AccountPlanService{
    	
    	public Map<Id, Account_Plan_2__c> getAccountPlans(Id accountId, Set<Id> subBusinessUnitIds){
    		
    		Map<Id, Account_Plan_2__c> accountPlans = new Map<Id, Account_Plan_2__c>();
    		
    		for(Account_Plan_2__c accPlan : [Select Id, Name, Sub_Business_Unit__c from Account_Plan_2__c where Account__c = :accountId AND Account_Plan_Level__c = 'Sub Business Unit' AND Sub_Business_Unit__c IN :subBusinessUnitIds]){
    			
    			accountPlans.put(accPlan.Sub_Business_Unit__c, accPlan);
    		}
    		
    		return accountPlans;
    	}
    }
}