@isTest
private class TEST_clsCamMember 
    {
    static testMethod void clsCamMemberMethod() 
        {
                Account a = new Account() ;
        a.Name = 'Test Account Name ' ;  
        a.RecordTypeId = FinalConstants.sapAccountRecordTypeId ;
        a.Account_Country_vs__c='BELGIUM';
        //a.CurrencyIsoCode =DIBcntry.CurrencyIsoCode ;
        insert a;
        
        Contact cont1 = new Contact();
        cont1.LastName = 'Test_ContactActionPlan ' ;  
        cont1.FirstName = 'Test Contact' ;  
        cont1.AccountId = a.Id ;
        cont1.Contact_Active__c = false  ;
        cont1.Contact_Inactive_Reason__c = 'Inactive Reason is required to all inactve Contacts';
        cont1.MailingCountry = 'BELGIUM';
        cont1.Contact_Department__c = 'Diabetes Adult'; 
        cont1.Contact_Primary_Specialty__c = 'ENT';
        cont1.Affiliation_To_Account__c = 'Employee';
        cont1.Primary_Job_Title_vs__c = 'Manager';
        cont1.Contact_Gender__c = 'Male';                 
        insert cont1;    
        
        campaign cmp = new campaign();
        // cmp.Business_Unit__c=bu.id;
        cmp.name='Campaign1';
        cmp.Status='Accounts Approved';
        cmp.isactive=true;
        cmp.Business_Unit_Picklist__c = 'Diabetes';
        cmp.recordtypeid=clsUtil.getRecordTypeByDevName('Campaign', 'Diabetes_Campaign').Id;
        insert cmp;
        
                Target_Account__c tacc=new Target_Account__c();
        tacc.Campaign__c=cmp.id;
        tacc.Account__c=a.id;
        tacc.Status__c='Approved';
        insert tacc;
        
        
           Profile p = [select id from profile where name='System Administrator'];    
           user u = new User(alias = 'standt3', email='standarduser@testorg.com',             
                        emailencodingkey='UTF-8', lastname='TerritoryManagement_Testing',             
                        languagelocalekey='en_US',             
                        localesidkey='en_US', profileid = p.Id,            
                        timezonesidkey='America/Los_Angeles',             
                        CountryOR__c='Netherlands',                        
                        Country_vs__c= 'NETHERLANDS',
                        Region_vs__c = 'Europe',
                        Sub_Region__c = 'NWE',            
                        username='NoBUUser123@testorg.com',
                        Alias_unique__c='standarduser_Alias_unique__c',
                        Company_Code_Text__c='EUR',
                        User_Business_Unit_vs__c = 'Cranial Spinal',
                        Job_Title_vs__c='Sales Rep');
                insert u;        
                            
        Campaignmember cmbr=new Campaignmember();
        cmbr.campaignID=cmp.id;
        cmbr.contactID=cont1.id;
        
        cmbr.Feedback_Caller__c=u.id;
        cmbr.Invitation_Caller__c=u.id;
        cmbr.NPS_Value__c='Detractor';
        cmbr.CC_Status__c='Feedback Call Required';
        cmbr.Feedback_task_generated__c=false;
        insert cmbr;
        
        
        ApexPages.currentPage().getParameters().put('CamMmbrRemid',cmbr.id);
        ApexPages.StandardController ctrl = new ApexPages.StandardController(cmbr);    
        clsCamMember controller = new clsCamMember (ctrl);
        
        controller.EditCamMmbr();
        controller.RemoveCamMmbr();
        
        }
    }