/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 *
 * Creation Date :  20091002
 * Description :    Test coverage for Trigger Subjects Discussed
 * 
 * Author :         Tuan Abdeen / ABSI
 */
 
@isTest
private class TEST_triggerSubjectsDiscussedAfterAll {


    static testMethod void myUnitTest() {
        
        System.debug(' ################## ' + 'BEGIN TEST_triggerSubjectsDiscussedAfterAll' + ' ################');     
//Insert Company
        Company__c cmpny = new Company__c();
        cmpny.name='TestMedtronic';
        cmpny.CurrencyIsoCode = 'EUR';
        cmpny.Current_day_in_Q1__c=56;
        cmpny.Current_day_in_Q2__c=34; 
        cmpny.Current_day_in_Q3__c=5; 
        cmpny.Current_day_in_Q4__c= 0;   
        cmpny.Current_day_in_year__c =200;
        cmpny.Days_in_Q1__c=56;  
        cmpny.Days_in_Q2__c=34;
        cmpny.Days_in_Q3__c=13;
        cmpny.Days_in_Q4__c=22;
        cmpny.Days_in_year__c =250;
        cmpny.Company_Code_Text__c = 'T46';
        insert cmpny;    
        
        Account acc1 = new Account();
        acc1.Name = 'TEST_triggerSubjectsDiscussedAfterAll Test Account 1'; 
        insert acc1;                
        
        Contact cont1 = new Contact();
        cont1.LastName = 'TEST_triggerSubjectsDiscussedAfterAll Test Contact 1';  
		cont1.FirstName = 'test';
        cont1.AccountId = acc1.Id;  
        cont1.Phone = '0714820303'; 
        cont1.Email ='test1@contact.com';
        cont1.Contact_Department__c = 'Diabetes Adult'; 
        cont1.Contact_Primary_Specialty__c = 'ENT';
        cont1.Affiliation_To_Account__c = 'Employee';
        cont1.Primary_Job_Title_vs__c = 'Manager';
        cont1.Contact_Gender__c = 'Male';         
        insert cont1;   
        
        List<Call_Activity_Type__c> subjects = new List<Call_Activity_Type__c>();
        Call_Activity_Type__c subj1 = new Call_Activity_Type__c();
        subj1.name = 'Test Subject 1';
        subj1.Company_ID__c = cmpny.id;
        subjects.add(subj1);
        
        Call_Activity_Type__c subj2 = new Call_Activity_Type__c();
        subj2.name = 'Test Subject 2';
        subj2.Company_ID__c = cmpny.id;
        subjects.add(subj2);
        
        Call_Activity_Type__c subj3 = new Call_Activity_Type__c();
        subj3.name = 'Test Subject 3';
        subj3.Company_ID__c = cmpny.id;
        subjects.add(subj3);
        
        insert subjects;
        
        Call_Records__c vr1 = new Call_Records__c(); 
        vr1.Call_Channel__c = 'Group Visit'; 
        vr1.Call_Date__c = date.newInstance(2009, 10, 20);
        insert vr1;     
        
        Contact_Visit_Report__c cd1 = new Contact_Visit_Report__c (); 
        cd1.Call_Records__c = vr1.Id; 
        cd1.Attending_Contact__c = cont1.Id; 
        cd1.Attending_Affiliated_Account__c = acc1.Id; 
        insert cd1;
        
        List<Call_Topics__c> subjectsDiscussed = new List<Call_Topics__c>();
        Call_Topics__c sb1 = new Call_Topics__c (); 
        sb1.Call_Records__c = vr1.Id; 
        sb1.Call_Activity_Type__c = subj1.Id;
        sb1.Call_Topic_Duration__c=1;
        subjectsDiscussed.add(sb1);
        
        Call_Topics__c sb2 = new Call_Topics__c(); 
        sb2.Call_Records__c = vr1.Id; 
        sb2.Call_Activity_Type__c = subj2.Id;
        sb2.Call_Topic_Duration__c=2;
        subjectsDiscussed.add(sb2);
        
        Call_Topics__c sb3 = new Call_Topics__c(); 
        sb3.Call_Records__c = vr1.Id; 
        sb3.Call_Activity_Type__c = subj3.Id;
        sb3.Call_Topic_Duration__c=3;
        subjectsDiscussed.add(sb3);
        
        insert subjectsDiscussed; 
        
        //Added to cover tr_UpdateSubjectsOnCallRecord trigger.
        Subject__c Sub=new Subject__c();
        Sub.Company_ID__c=cmpny.id;
        Sub.Subject__c='abc';
        insert Sub;
        
        Call_Topic_Subject__c CTS=new Call_Topic_Subject__c();
        CTS.Call_Topic__c=subjectsDiscussed[0].id;
        CTS.Subject__c=Sub.id;
        insert CTS;
        
        Product2 prd1 = new Product2();
        prd1.Name = 'test Product';
        insert prd1;
        
        Call_Topic_Products__c CTP= new Call_Topic_Products__c();
        CTP.Call_Topic__c=subjectsDiscussed[0].id;
        CTP.Product__c=prd1.Id;
        insert CTP;
        try{
            delete Sub;
        }Catch(Exception e){
        }
        System.debug(' ################## ' + 'END TEST_triggerSubjectsDiscussedAfterAll' + ' ################');
    }
 
 
}