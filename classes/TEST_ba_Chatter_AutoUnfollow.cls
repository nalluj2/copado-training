//---------------------------------------------------------------------------------------------------------------------------------------------------
//  Author		:    Bart Caelen
//  Date 		:    20180201
//  Description	:    CR-14975 - APEX Test Class for ba_Chatter_AutoUnfollow
//---------------------------------------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_ba_Chatter_AutoUnfollow {

	
	@TestSetup static void createTestData() {

		List<EntitySubscription> lstFollower = new List<EntitySubscription>();

		// Create Admin User and Custom Setting Data
		User oUser_Admin = clsTestData_User.createUser_SystemAdministratorMDT('tstadm1', true);

		System.runAs(oUser_Admin){

			clsTestData_CustomSetting.createCustomSettingData_SystemAdministratorProfileId();

		}


		//----------------------------------------
		// Create Test Data
		//----------------------------------------
		// Create User
		User oUser1 = clsTestData_User.createUser('tstusr1', true);
		User oUser2 = clsTestData_User.createUser('tstusr2', true);

		System.runAs(oUser_Admin){

			if (Schema.getGlobalDescribe().get('User').getDescribe().isFeedEnabled()){
	            EntitySubscription oFollower = new EntitySubscription(ParentId=oUser1.Id,SubscriberId=oUser2.Id);    
    	        lstFollower.add(oFollower);
    	    }

        }

		// Create Lead
		System.runAs(oUser_Admin){

			clsTestData.createLeadData(false);
	    	Lead oLead = clsTestData.oMain_Lead;
	        	oLead.Email = 'test@test.com';
		        oLead.PostalCode = '1111';
		        oLead.LastName = 'test';
		        oLead.Company = 'testCompany';
		        oLead.Lead_Zip_Postal_Code__c = '1111';
		        oLead.RecordTypeId = clsUtil.getRecordTypeByDevName('Lead', 'MITG_Lead').Id;
	        insert oLead;

			if (Schema.getGlobalDescribe().get('Lead').getDescribe().isFeedEnabled()){
	            EntitySubscription oFollower = new EntitySubscription(ParentId=oLead.Id,SubscriberId=oUser2.Id);    
    	        lstFollower.add(oFollower);
    	    }

        }

        // Create Contact
		System.runAs(oUser_Admin){

			Contact oContact = clsTestData_Contact.createContact()[0];

			if (Schema.getGlobalDescribe().get('Contact').getDescribe().isFeedEnabled()){
	            EntitySubscription oFollower = new EntitySubscription(ParentId=oContact.Id,SubscriberId=oUser2.Id);    
    	        lstFollower.add(oFollower);
    	    }

		}

		// Create Opportunity
		System.runAs(oUser_Admin){

			Opportunity oOpportunity = clsTestData_Opportunity.createOpportunity()[0];

			if (Schema.getGlobalDescribe().get('Opportunity').getDescribe().isFeedEnabled()){
	            EntitySubscription oFollower = new EntitySubscription(ParentId=oOpportunity.Id,SubscriberId=oUser2.Id);    
    	        lstFollower.add(oFollower);
    	    }

		}

		// Create Task
		System.runAs(oUser_Admin){

			Task oTask = new Task();
				oTask.Subject = 'TASK Subject';
				oTask.Description = 'Test';
				oTask.ActivityDate = Date.today();
				oTask.Status = 'Not Started';
				oTask.Priority = 'Normal';
			insert oTask;

			if (Schema.getGlobalDescribe().get('Task').getDescribe().isFeedEnabled()){
	            EntitySubscription oFollower = new EntitySubscription(ParentId=oTask.Id,SubscriberId=oUser2.Id);    
    	        lstFollower.add(oFollower);
    	    }

		}

		// Create Event
		System.runAs(oUser_Admin){

			Event oEvent = new Event();
				oEvent.WhatId = clsTestData_Account.oMain_Account.Id;
				oEvent.Type = 'Other';
				oEvent.DurationInMinutes = 120;
				oEvent.StartDateTime = DateTime.Now();
				oEvent.EndDateTime = DateTime.Now().addHours(2);
			insert oEvent;

			if (Schema.getGlobalDescribe().get('Event').getDescribe().isFeedEnabled()){
	            EntitySubscription oFollower = new EntitySubscription(ParentId=oEvent.Id,SubscriberId=oUser2.Id);    
    	        lstFollower.add(oFollower);
    	    }

		}

		// Create Change_Request__c
		System.runAs(oUser_Admin){

			Project__c oRelease = clsTestData_ProjectManagement.createRelease()[0];
			Change_Request__c oChangeRequest = clsTestData_ProjectManagement.createChangeRequest(false, false)[0];
				oChangeRequest.Project__c = oRelease.Id;
			insert oChangeRequest;

			if (Schema.getGlobalDescribe().get('Change_Request__c').getDescribe().isFeedEnabled()){
	            EntitySubscription oFollower = new EntitySubscription(ParentId=oChangeRequest.Id,SubscriberId=oUser2.Id);    
	            lstFollower.add(oFollower);
	        }

		}


		// Create Account_Extended_Profile__c
		System.runAs(oUser_Admin){

			clsTestData_Account.iRecord_Account_SAPAccount = 1;
			clsTestData_Account.createAccount_SAPAccount();

			Account_Extended_Profile__c oAccountExtendedProfile  = new Account_Extended_Profile__c();
				oAccountExtendedProfile.RecordTypeId = clsUtil.getRecordTypeByDevName('Account_Extended_Profile__c', 'EMEA_Account_Extended_Profile').Id;
				oAccountExtendedProfile.Account__c = clsTestData_Account.oMain_Account_SAPAccount.Id;
				oAccountExtendedProfile.Commencement_Date__c = Date.today();
				oAccountExtendedProfile.General_Surgery_procedures__c = 1;
				oAccountExtendedProfile.Gynecologic_procedures__c = 2;
				oAccountExtendedProfile.Head_Neck_procedures__c = 3;
				oAccountExtendedProfile.Hemorrhoids_procedures__c = 4;
				oAccountExtendedProfile.Section_Owner_Facility_Info__c = oUser_Admin.Id;
				oAccountExtendedProfile.Section_Owner_SI_AST_Driven__c = oUser_Admin.Id;
				oAccountExtendedProfile.Section_Owner_SI_GSP_Driven__c = oUser_Admin.Id;
				oAccountExtendedProfile.Section_Owner_SI_Hernia_Driven__c = oUser_Admin.Id;
			insert oAccountExtendedProfile;

			if (Schema.getGlobalDescribe().get('Account_Extended_Profile__c').getDescribe().isFeedEnabled()){
				EntitySubscription oFollower = new EntitySubscription(ParentId=oAccountExtendedProfile.Id,SubscriberId=oUser2.Id);    
				lstFollower.add(oFollower);
			}

		}

		// Create Followers
		System.runAs(oUser_Admin){

			insert lstFollower;

		}
		//----------------------------------------


	}
	

	@isTest static void test_User() {
		
		Integer iFollower = 0;

		// Load Data
		User oUser_Admin = [SELECT Id FROM User WHERE Alias = 'tstadm1'];
		User oUser1 = [SELECT Id, AutoUnfollow__c FROM User WHERE Alias = 'tstusr1'];
		System.assertEquals(oUser1.AutoUnfollow__c, false);


		List<EntitySubscription> lstFollower = [SELECT Id FROM EntitySubscription WHERE ParentId = :oUser1.Id];
		iFollower = lstFollower.size();
		if (Schema.getGlobalDescribe().get('User').getDescribe().isFeedEnabled()){
			System.assert(lstFollower.size() > 0);
		}else{
			System.assertEquals(lstFollower.size(), 0);
		} 


		Test.startTest();

			// Execute Logic
			ba_Chatter_AutoUnfollow oBatch1 = new ba_Chatter_AutoUnfollow();
				oBatch1.tObjectType = 'User';
				oBatch1.tSOQL = 'SELECT Id FROM User WHERE AutoUnfollow__c = true AND Alias = \'tstusr1\'';
			Database.executebatch(oBatch1, 200);

			lstFollower = [SELECT Id FROM EntitySubscription WHERE ParentId = :oUser1.Id];
			System.assertEquals(lstFollower.size(), iFollower);


			// Update Data
			System.runAs(oUser_Admin){
					oUser1.IsActive = false;
					oUser1.User_Status__c = 'Left Company';
					oUser1.Deactivation_Date__c = Date.today();
				update oUser1;
			}
			oUser1 = [SELECT Id, AutoUnfollow__c FROM User WHERE Alias = 'tstusr1'];
			System.assertEquals(oUser1.AutoUnfollow__c, true);


			// Execute Logic
			ba_Chatter_AutoUnfollow oBatch2 = new ba_Chatter_AutoUnfollow();
				oBatch2.tObjectType = 'User';
				oBatch2.tSOQL = 'SELECT Id FROM User WHERE AutoUnfollow__c = true AND Alias = \'tstusr1\'';
			Database.executebatch(oBatch2, 200);

		Test.stopTest();


		// Test Result
		lstFollower = [SELECT Id FROM EntitySubscription WHERE ParentId = :oUser1.Id];
		System.assertEquals(lstFollower.size(), 0);

	}


	@isTest static void test_Contact() {

		Integer iFollower = 0;

		// Load Data
		User oUser_Admin = [SELECT Id FROM User WHERE Alias = 'tstadm1'];

		Contact oContact = [SELECT Id, AutoUnfollow__c FROM Contact LIMIT 1];
		System.assertEquals(oContact.AutoUnfollow__c, false);

		List<EntitySubscription> lstFollower = [SELECT Id FROM EntitySubscription WHERE ParentId = :oContact.Id];
		iFollower = lstFollower.size();
		if (Schema.getGlobalDescribe().get('Contact').getDescribe().isFeedEnabled()){
			System.assert(lstFollower.size() > 0);
		}else{
			System.assertEquals(lstFollower.size(), 0);
		} 


		Test.startTest();

			// Execute Logic
			ba_Chatter_AutoUnfollow oBatch1 = new ba_Chatter_AutoUnfollow();
				oBatch1.tObjectType = 'Contact';
			Database.executebatch(oBatch1, 200);

			lstFollower = [SELECT Id FROM EntitySubscription WHERE ParentId = :oContact.Id];
			System.assertEquals(lstFollower.size(), iFollower);


			// Update Data
			System.runAs(oUser_Admin){
					oContact.Contact_Active__c = false;
					oContact.Contact_Inactive_Reason__c = 'Left Company';
				update oContact;
			}
			oContact = [SELECT Id, AutoUnfollow__c FROM Contact LIMIT 1];
			System.assertEquals(oContact.AutoUnfollow__c, true);


			// Execute Logic
			ba_Chatter_AutoUnfollow oBatch2 = new ba_Chatter_AutoUnfollow();
				oBatch2.tObjectType = 'Contact';
			Database.executebatch(oBatch2, 200);

		Test.stopTest();


		// Test Result
		lstFollower = [SELECT Id FROM EntitySubscription WHERE ParentId = :oContact.Id];
		System.assertEquals(lstFollower.size(), 0);

	}


	@isTest static void test_ChangeRequest() {

		Integer iFollower = 0;

		// Load Data
		User oUser_Admin = [SELECT Id FROM User WHERE Alias = 'tstadm1'];
		
		Change_Request__c oChangeRequest = [SELECT Id, Project__c, AutoUnfollow__c FROM Change_Request__c LIMIT 1];
		System.assertEquals(oChangeRequest.AutoUnfollow__c, false);

		Project__c oRelease = [SELECT Id, End_Date__c FROM Project__c WHERE Id = :oChangeRequest.Project__c LIMIT 1];
		List<EntitySubscription> lstFollower = [SELECT Id FROM EntitySubscription WHERE ParentId = :oChangeRequest.Id];
		iFollower = lstFollower.size();
		if (Schema.getGlobalDescribe().get('Change_Request__c').getDescribe().isFeedEnabled()){
			System.assert(lstFollower.size() > 0);
		}else{
			System.assertEquals(lstFollower.size(), 0);
		} 


		Test.startTest();

			// Execute Logic
			ba_Chatter_AutoUnfollow oBatch1 = new ba_Chatter_AutoUnfollow();
				oBatch1.tObjectType = 'Change_Request__c';
			Database.executebatch(oBatch1, 200);

			lstFollower = [SELECT Id FROM EntitySubscription WHERE ParentId = :oChangeRequest.Id];
			System.assertEquals(lstFollower.size(), iFollower);


			// Update Data
			System.runAs(oUser_Admin){
					oRelease.End_Date__c = Date.today().addMonths(-8);
				update oRelease;
			}
			oChangeRequest = [SELECT Id, Project__c, AutoUnfollow__c FROM Change_Request__c LIMIT 1];
			System.assertEquals(oChangeRequest.AutoUnfollow__c, true);


			// Execute Logic
			ba_Chatter_AutoUnfollow oBatch2 = new ba_Chatter_AutoUnfollow();
				oBatch2.tObjectType = 'Change_Request__c';
			Database.executebatch(oBatch2, 200);

		Test.stopTest();


		// Test Result
		lstFollower = [SELECT Id FROM EntitySubscription WHERE ParentId = :oChangeRequest.Id];
		System.assertEquals(lstFollower.size(), 0);

	}

	@isTest static void test_Opportunity() {

		Integer iFollower = 0;

		// Load Data
		User oUser_Admin = [SELECT Id FROM User WHERE Alias = 'tstadm1'];
		
		Opportunity oOpportunity = [SELECT Id, AutoUnfollow__c FROM Opportunity LIMIT 1];
		System.assertEquals(oOpportunity.AutoUnfollow__c, false);

		List<EntitySubscription> lstFollower = [SELECT Id FROM EntitySubscription WHERE ParentId = :oOpportunity.Id];
		iFollower = lstFollower.size();
		if (Schema.getGlobalDescribe().get('Opportunity').getDescribe().isFeedEnabled()){
			System.assert(lstFollower.size() > 0);
		}else{
			System.assertEquals(lstFollower.size(), 0);
		} 


		Test.startTest();

			// Execute Logic
			ba_Chatter_AutoUnfollow oBatch1 = new ba_Chatter_AutoUnfollow();
				oBatch1.tObjectType = 'Opportunity';
			Database.executebatch(oBatch1, 200);

			lstFollower = [SELECT Id FROM EntitySubscription WHERE ParentId = :oOpportunity.Id];
			System.assertEquals(lstFollower.size(), iFollower);


			// Update Data
			System.runAs(oUser_Admin){
					oOpportunity.StageName = 'Closed Won';
					oOpportunity.Reason_Won__c = 'Won';
				update oOpportunity;
			}
			oOpportunity = [SELECT Id, AutoUnfollow__c FROM Opportunity LIMIT 1];
			System.assertEquals(oOpportunity.AutoUnfollow__c, true);


			// Execute Logic
			ba_Chatter_AutoUnfollow oBatch2 = new ba_Chatter_AutoUnfollow();
				oBatch2.tObjectType = 'Opportunity';
			Database.executebatch(oBatch2, 200);

		Test.stopTest();


		// Test Result
		lstFollower = [SELECT Id FROM EntitySubscription WHERE ParentId = :oOpportunity.Id];
		System.assertEquals(lstFollower.size(), 0);

	}

	
	@isTest static void test_Lead() {

		Integer iFollower = 0;

		// Load Data
		User oUser_Admin = [SELECT Id FROM User WHERE Alias = 'tstadm1'];
		
		Lead oLead = [SELECT Id, AutoUnfollow__c FROM Lead LIMIT 1];
		System.assertEquals(oLead.AutoUnfollow__c, false);

		Contact oContact = [SELECT Id, AccountId FROM Contact LIMIT 1];

		List<EntitySubscription> lstFollower = [SELECT Id FROM EntitySubscription WHERE ParentId = :oLead.Id];
		iFollower = lstFollower.size();
		if (Schema.getGlobalDescribe().get('Lead').getDescribe().isFeedEnabled()){
			System.assert(lstFollower.size() > 0);
		}else{
			System.assertEquals(lstFollower.size(), 0);
		} 

		Test.startTest();

			// Execute Logic
			ba_Chatter_AutoUnfollow oBatch1 = new ba_Chatter_AutoUnfollow();
				oBatch1.tObjectType = 'Lead';
			Database.executebatch(oBatch1, 200);

			lstFollower = [SELECT Id FROM EntitySubscription WHERE ParentId = :oLead.Id];
			System.assertEquals(lstFollower.size(), iFollower);


			// Update Data
			System.runAs(oUser_Admin){

				Database.LeadConvert oLeadConvert = new database.LeadConvert();
					oLeadConvert.setLeadId(oLead.Id);
					oLeadConvert.setContactId(oContact.Id);
					oLeadConvert.setAccountId(oContact.AccountId);
					oLeadConvert.setDoNotCreateOpportunity(false);
					oLeadConvert.setConvertedStatus('Qualified');

				Database.LeadConvertResult oLeadConvertResult = Database.convertLead(oLeadConvert);
				System.assert(oLeadConvertResult.isSuccess());			

			}
			oLead = [SELECT Id, AutoUnfollow__c FROM Lead LIMIT 1];
			System.assertEquals(oLead.AutoUnfollow__c, true);


			// Execute Logic
			ba_Chatter_AutoUnfollow oBatch2 = new ba_Chatter_AutoUnfollow();
				oBatch2.tObjectType = 'Lead';
			Database.executebatch(oBatch2, 200);

		Test.stopTest();


		// Test Result
		lstFollower = [SELECT Id FROM EntitySubscription WHERE ParentId = :oLead.Id];
		System.assertEquals(lstFollower.size(), 0);

	}


	@isTest static void test_AccountExtendedProfile() {

		Integer iFollower = 0;

		// Load Data
		User oUser_Admin = [SELECT Id FROM User WHERE Alias = 'tstadm1'];
		
		Account_Extended_Profile__c oAccountExtendedProfile = [SELECT Id, AutoUnfollow__c FROM Account_Extended_Profile__c LIMIT 1];
		System.assertEquals(oAccountExtendedProfile.AutoUnfollow__c, false);

		List<EntitySubscription> lstFollower = [SELECT Id FROM EntitySubscription WHERE ParentId = :oAccountExtendedProfile.Id];
		iFollower = lstFollower.size();
		if (Schema.getGlobalDescribe().get('Account_Extended_Profile__c').getDescribe().isFeedEnabled()){
			System.assert(lstFollower.size() > 0);
		}else{
			System.assertEquals(lstFollower.size(), 0);
		} 


		Test.startTest();

			// Execute Logic
			ba_Chatter_AutoUnfollow oBatch1 = new ba_Chatter_AutoUnfollow();
				oBatch1.tObjectType = 'Account_Extended_Profile__c';
			Database.executebatch(oBatch1, 200);

			lstFollower = [SELECT Id FROM EntitySubscription WHERE ParentId = :oAccountExtendedProfile.Id];
			System.assertEquals(lstFollower.size(), iFollower);


			// Update Data
			System.runAs(oUser_Admin){
					oAccountExtendedProfile.Approval_Status_Facility_Info__c = 'Approved';
					oAccountExtendedProfile.Approval_Status_SI_AST_Driven__c = 'Approved';
					oAccountExtendedProfile.Approval_Status_SI_GSP_Driven__c = 'Approved';
					oAccountExtendedProfile.Approval_Status_SI_Hernia_Driven__c = 'Approved';
				update oAccountExtendedProfile;
			}
			oAccountExtendedProfile = [SELECT Id, AutoUnfollow__c FROM Account_Extended_Profile__c LIMIT 1];
			System.assertEquals(oAccountExtendedProfile.AutoUnfollow__c, true);


			// Execute Logic
			ba_Chatter_AutoUnfollow oBatch2 = new ba_Chatter_AutoUnfollow();
				oBatch2.tObjectType = 'Account_Extended_Profile__c';
			Database.executebatch(oBatch2, 200);

		Test.stopTest();


		// Test Result
		lstFollower = [SELECT Id FROM EntitySubscription WHERE ParentId = :oAccountExtendedProfile.Id];
		System.assertEquals(lstFollower.size(), 0);

	}


	@isTest static void test_Task() {

		Integer iFollower = 0;

		// Load Data
		User oUser_Admin = [SELECT Id FROM User WHERE Alias = 'tstadm1'];
		
		Task oTask = [SELECT Id, AutoUnfollow__c FROM Task LIMIT 1];
		System.assertEquals(oTask.AutoUnfollow__c, false);
		List<EntitySubscription> lstFollower = [SELECT Id FROM EntitySubscription WHERE ParentId = :oTask.Id];
		iFollower = lstFollower.size();
		if (Schema.getGlobalDescribe().get('Task').getDescribe().isFeedEnabled()){
			System.assert(lstFollower.size() > 0);
		}else{
			System.assertEquals(lstFollower.size(), 0);
		}


		Test.startTest();

			// Execute Logic
			ba_Chatter_AutoUnfollow oBatch1 = new ba_Chatter_AutoUnfollow();
				oBatch1.tObjectType = 'Task';
			Database.executebatch(oBatch1, 200);

			lstFollower = [SELECT Id FROM EntitySubscription WHERE ParentId = :oTask.Id];
			System.assertEquals(lstFollower.size(), iFollower);


			// Update Data
			System.runAs(oUser_Admin){
					oTask.ActivityDate = Date.today().addMonths(-8);
					oTask.Status = 'Completed';
				update oTask;
			}
			oTask = [SELECT Id, AutoUnfollow__c FROM Task LIMIT 1];
			System.assertEquals(oTask.AutoUnfollow__c, true);


			// Execute Logic
			ba_Chatter_AutoUnfollow oBatch2 = new ba_Chatter_AutoUnfollow();
				oBatch2.tObjectType = 'Task';
			Database.executebatch(oBatch2, 200);

		Test.stopTest();


		// Test Result
		lstFollower = [SELECT Id FROM EntitySubscription WHERE ParentId = :oTask.Id];
		System.assertEquals(lstFollower.size(), 0);

	}


	@isTest static void test_Event() {

		Integer iFollower = 0;

		// Load Data
		User oUser_Admin = [SELECT Id FROM User WHERE Alias = 'tstadm1'];
		
		Event oEvent = [SELECT Id, AutoUnfollow__c FROM Event LIMIT 1];
		System.assertEquals(oEvent.AutoUnfollow__c, false);

		List<EntitySubscription> lstFollower = [SELECT Id FROM EntitySubscription WHERE ParentId = :oEvent.Id];
		iFollower = lstFollower.size();
		if (Schema.getGlobalDescribe().get('Event').getDescribe().isFeedEnabled()){
			System.assert(lstFollower.size() > 0);
		}else{
			System.assertEquals(lstFollower.size(), 0);
		} 


		Test.startTest();

			// Execute Logic
			ba_Chatter_AutoUnfollow oBatch1 = new ba_Chatter_AutoUnfollow();
				oBatch1.tObjectType = 'Event';
			Database.executebatch(oBatch1, 200);

			lstFollower = [SELECT Id FROM EntitySubscription WHERE ParentId = :oEvent.Id];
			System.assertEquals(lstFollower.size(), iFollower);


			// Update Data
			System.runAs(oUser_Admin){
					oEvent.StartDateTime = Date.today().addMonths(-8);
					oEvent.EndDateTime = Date.today().addMonths(-8);
				update oEvent;
			}

			oEvent = [SELECT Id, AutoUnfollow__c FROM Event LIMIT 1];
			System.assertEquals(oEvent.AutoUnfollow__c, true);


			// Execute Logic
			ba_Chatter_AutoUnfollow oBatch2 = new ba_Chatter_AutoUnfollow();
				oBatch2.tObjectType = 'Event';
			Database.executebatch(oBatch2, 200);

		Test.stopTest();


		// Test Result
		lstFollower = [SELECT Id FROM EntitySubscription WHERE ParentId = :oEvent.Id];
		System.assertEquals(lstFollower.size(), 0);

	}


}
//---------------------------------------------------------------------------------------------------------------------------------------------------