public with sharing class ctrlExt_Opportunity_TerritoryCheck {

	//------------------------------------------------------------------------------------
	// PRIVATE Variables
	//------------------------------------------------------------------------------------
	private Opportunity oOpportunity;
    private Id idRecordType_Opportunity;
	private Set<Id> setID_Territory_User_Not_Account = new Set<Id>();
	private String tError_LoadTerritory = '';

	//------------------------------------------------------------------------------------


	//------------------------------------------------------------------------------------
	// GETTERS & SETTERS
	//------------------------------------------------------------------------------------
    public String selectedTerrId {get; set;}
    public Map<Id,Territory2> map_Territory {get; private set;}
    public List<Territory2> lstTerritory { 
		get{
			return map_Territory.values();
		}
		private set;
	}

	public String tLabel_Save { get; private set;}
	//------------------------------------------------------------------------------------
    

	//------------------------------------------------------------------------------------
	// CONSTRUCTOR
	//------------------------------------------------------------------------------------
    public ctrlExt_Opportunity_TerritoryCheck(ApexPages.StandardController stdCtrl) {
        
        if (!Test.isRunningTest()){
            stdCtrl.addFields(new String[]{'CreatedDate','LastModifiedDate','AccountId','OwnerId','Territory2Id','DoNotByPassTerritorySelection__c', 'Owner.Company_Code_Text__c'});     
        }
        oOpportunity = (Opportunity) stdCtrl.getRecord();

        // Get the RecordType of the Opportunity
        idRecordType_Opportunity = [SELECT RecordTypeId FROM Opportunity WHERE Id = :oOpportunity.Id].RecordTypeId;

        map_Territory = new Map<Id,Territory2>();

		tLabel_Save = 'Save';

    }
	//------------------------------------------------------------------------------------


	//------------------------------------------------------------------------------------
	// ACTIONS
	//------------------------------------------------------------------------------------
    public PageReference init(){

        PageReference oPR;

		Set<Id> setID_RecordType = new Set<Id>{clsUtil.getRecordTypeByDevName('Opportunity', 'EUR_MITG_Standard_Opportunity').Id, clsUtil.getRecordTypeByDevName('Opportunity', 'MEA_MITG_Standard_Opportunity').Id, clsUtil.getRecordTypeByDevName('Opportunity', 'EMEA_MITG_Standard_Opportunity').Id};

		// If the Opportunity is not MITG redirect to the Opportunity
		if (!setID_RecordType.contains(idRecordType_Opportunity)){
            return getNoOverrideURL();
        }


        if ( (!oOpportunity.DoNotByPassTerritorySelection__c) || (oOpportunity.OwnerId != UserInfo.getUserId()) ){

            return getNoOverrideURL();
        
        }else{

            Set<String> setRegions_IgnoreAccountTerritory = new Set<String>();

            List<Company__c> lstCompany = [SELECT Id, Company_Code_Text__c, Ignore_Account_Territory__c FROM Company__c WHERE Ignore_Account_Territory__c = true];
            for (Company__c oCompany : lstCompany){
                if (oCompany.Ignore_Account_Territory__c){
                    setRegions_IgnoreAccountTerritory.add(oCompany.Company_Code_Text__c);
                }
            }

			if (setRegions_IgnoreAccountTerritory.contains(oOpportunity.Owner.Company_Code_Text__c)){

				map_Territory = loadTerritory();

				System.debug('**BC** map_Territory (' + map_Territory.size() + ') : ' + map_Territory);
                
                if (map_Territory.size() == 1){

	                    oOpportunity.Territory2Id = map_Territory.values()[0].Id;
                        oOpportunity.DoNotByPassTerritorySelection__c = false;
                    update oOpportunity;
                    return getNoOverrideURL();
                
                }

            }
            
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, tError_LoadTerritory));
            return null;    

		}

		return oPR;
			
	}


    public PageReference getNoOverrideURL(){

        String baseURL = URL.getSalesforceBaseUrl().toExternalForm();
        String VfURL = ApexPages.currentPage().getUrl();
        String pageURL = baseURL+VfURL;
        
        if (!pageURL.contains('displayTrueTeamAccess')){
        
            Pagereference oPageReference = new Pagereference('/' + oOpportunity.id + '?nooverride=1');
            oPageReference.setRedirect(true);
            return oPageReference;
        
        }else{
        
            Pagereference oPageReference = new Pagereference('/' + oOpportunity.id + '?nooverride=1&displayTrueTeamAccess=1#' + oOpportunity.id + '_RelatedOpportunitySalesTeam');
            oPageReference.setRedirect(false);
            return oPageReference;
        
        }              

    }
    

    public PageReference saveTerritory(){

        if (!clsUtil.isBlank(selectedTerrId)){

                oOpportunity.Territory2Id = selectedTerrId;
                oOpportunity.DoNotByPassTerritorySelection__c = false;
            update oOpportunity;

        }
        
        return getNoOverrideURL();

    }


	private Map<Id, Territory2> loadTerritory(){
		
		Map<Id, Territory2> map_Territory = new Map<Id, Territory2>();

		// Load the Territories of the User (Opportunity Owner)
		Set<Id> setID_Territory_User = new Set<Id>();
		for (UserTerritory2Association oUserTerritory : [SELECT Territory2Id FROM UserTerritory2Association WHERE UserId = :oOpportunity.OwnerId AND isActive = True]){
			setID_Territory_User.add(oUserTerritory.Territory2Id);
		}

        if ((idRecordType_Opportunity == clsUtil.getRecordTypeByDevName('Opportunity', 'EUR_MITG_Standard_Opportunity').Id) || (idRecordType_Opportunity == clsUtil.getRecordTypeByDevName('Opportunity', 'EMEA_MITG_Standard_Opportunity').Id)){

			// NEW LOGIC - Display the Territories of the User where the Account is also linked to

			// Load the Territories of the Account of the Opportunity
			List<Territory2> lstTerritory_Account = bl_Territory.getTerritoriesForAccount(oOpportunity.AccountId);
			Map<Id, Territory2> map_Territory_Account = new Map<Id, Territory2>();
			map_Territory_Account.putAll(lstTerritory_Account);

			// Populate map_Territory with the Territories that are shared by the user (Opportunity Owner) and the Account of the Opportunity
			for (Id id_Territory_User : setID_Territory_User){
				
				if (map_Territory_Account.containsKey(id_Territory_User)){
					map_Territory.put(id_Territory_User, map_Territory_Account.get(id_Territory_User));
				}else{
					setID_Territory_User_Not_Account.add(id_Territory_User);
				}
			}

			if (map_Territory.size() == 0){
				tError_LoadTerritory = 'No Territory found for this opportunity. Please validate that the Account on the Opportunity is linked to at least one of your Territories.  Click "OK" to continue to the Opportunity.';
				tLabel_Save = 'OK';
			}else if (map_Territory.size() > 1){
				tError_LoadTerritory = 'You are currently associated to more than one Territories.  Please select the correct Territory to associate with this Opportunity.  Note that this list only contains your Territories that are also linked to the Account of the Opportunity.';
				tLabel_Save = 'Save';
			}

        }else{

			// OLD LOGIC - Display the Territories of the User
			map_Territory = new Map<Id,Territory2>([SELECT Id,Name FROM Territory2 WHERE Id in :setID_Territory_User]);

			if (map_Territory.size() == 0){
				tError_LoadTerritory = 'No Territory found for this Opportunity. Please click "OK" to continue.';
				tLabel_Save = 'OK';
			}else if (map_Territory.size() > 1){
				tError_LoadTerritory = 'You are currently associated to more than one Territories.  Please select the correct Territory to associate with this Opportunity.';
				tLabel_Save = 'Save';
			}
		
		}

		return map_Territory;
	
	}

}