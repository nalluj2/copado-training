@isTest
private class Test_ws_DiabetesPerformanceService {
    
    private static testmethod void testUpdateDiabetesPerformance(){
    	
    	Diabetes_Performance__c dibPerformance = new Diabetes_Performance__c();
    	dibPerformance.Mobile_Id__c = 'UnitTestDiBPerformance';
		dibPerformance.Target_Q1__c = 1;
		dibPerformance.Target_Q2__c = 2;
		dibPerformance.Target_Q3__c = 3;
		dibPerformance.Target_Q4__c = 4;
		
		Test.startTest();
		    	    	
    	ws_DiabetesPerformanceService.IFDiabetesPerformanceRequest request = new ws_DiabetesPerformanceService.IFDiabetesPerformanceRequest();
	 	request.diabetesPerformance = dibPerformance;
	 	request.id = 'testId';
	 	
	 	RestRequest req = new RestRequest();	 	
	 	req.requestbody = Blob.valueOf(JSON.serialize(request));
	 	
	 	RestContext.request = req; 
    	
    	String res = ws_DiabetesPerformanceService.doPost();
    	
    	ws_DiabetesPerformanceService.IFDiabetesPerformanceResponse response = (ws_DiabetesPerformanceService.IFDiabetesPerformanceResponse) JSON.deserialize(res, ws_DiabetesPerformanceService.IFDiabetesPerformanceResponse.class);
    	
    	System.assert(response.success == true);
    	
    	dibPerformance = [Select Id, Target_Q1__c, Target_Q2__c, Target_Q3__c, Target_Q4__c from Diabetes_Performance__c where Mobile_Id__c = 'UnitTestDiBPerformance'];
    	
    	System.assert(dibPerformance.Target_Q1__c == 1);
    	System.assert(dibPerformance.Target_Q2__c == 2);
    	System.assert(dibPerformance.Target_Q3__c == 3);
    	System.assert(dibPerformance.Target_Q4__c == 4);
    }
        
    private static testmethod void testUpdateDIBPerformance_Error(){
    	
    	Diabetes_Performance__c dibPerformance = new Diabetes_Performance__c();
    	dibPerformance.Mobile_Id__c = 'FakeDiBPerformanceId';
		
		Test.startTest();
				    	
    	ws_DiabetesPerformanceService.IFDiabetesPerformanceRequest request = new ws_DiabetesPerformanceService.IFDiabetesPerformanceRequest();
	 	request.DiabetesPerformance = dibPerformance;
	 	request.id = 'testId';
	 	
	 	RestRequest req = new RestRequest();	 	
	 	req.requestbody = Blob.valueOf(JSON.serialize(request));
	 	
	 	RestContext.request = req; 
    	
    	String res = ws_DiabetesPerformanceService.doPost();
    	
    	ws_DiabetesPerformanceService.IFDiabetesPerformanceResponse response = (ws_DiabetesPerformanceService.IFDiabetesPerformanceResponse) JSON.deserialize(res, ws_DiabetesPerformanceService.IFDiabetesPerformanceResponse.class);
    	
    	System.assert(response.success == false);
    	System.assert(response.message.contains('No Diabetes Performance record found for Id'));    	
    }
    
    @TestSetup
    private static void setData() {
	
		Company__c cmpny = new Company__c();		
        cmpny.name='Europe';
        cmpny.CurrencyIsoCode = 'EUR';
        cmpny.Current_day_in_Q1__c=56;
        cmpny.Current_day_in_Q2__c=34; 
        cmpny.Current_day_in_Q3__c=5; 
        cmpny.Current_day_in_Q4__c= 0;   
        cmpny.Current_day_in_year__c =200;
        cmpny.Days_in_Q1__c=56;  
        cmpny.Days_in_Q2__c=34;
        cmpny.Days_in_Q3__c=13;
        cmpny.Days_in_Q4__c=22;
        cmpny.Days_in_year__c =250;
        cmpny.Company_Code_Text__c = 'T35';
		insert cmpny;
		        
        Business_Unit_Group__c bug_DiB =  new Business_Unit_Group__c();
        bug_DiB.Master_Data__c =cmpny.id;
        bug_DiB.name='Diabetes';              
        insert bug_DiB;
		       
        Business_Unit__c bu_DiB =  new Business_Unit__c();
        bu_DiB.Company__c =cmpny.id;
        bu_DiB.name='Diabetes';
        bu_DiB.Business_Unit_Group__c = bug_DiB.Id;             
        insert bu_DiB;
                        
        Sub_Business_Units__c sbu_DiB = new Sub_Business_Units__c();
        sbu_DiB.name='Diabetes Core';
        sbu_DiB.Business_Unit__c=bu_DiB.id;		
		insert sbu_DiB;
		
		DIB_Country__c country = new DIB_Country__c();
		country.Company__c = cmpny.Id;
		country.Name = 'Unit Test Country';
		country.Country_ISO_Code__c = 'ZZ';
		country.CurrencyIsoCode = 'EUR';
		insert country;
								
		Account testAccount = new Account();
		testAccount.Name = 'RTG Test Account';
		testAccount.Account_Country_vs__c = 'BELGIUM';
		insert testAccount;			
		
		Account_Plan_2__c accPlan = new Account_Plan_2__c();
		accPlan.Account__c = testAccount.Id;
		accPlan.Account_Plan_Level__c = 'Sub Business Unit';
		accPlan.Business_Unit_Group__c = bug_DiB.Id;
		accPlan.Business_Unit__c = bu_DiB.Id;
		accPlan.Sub_Business_Unit__c = sbu_DiB.Id;
		accPlan.RecordTypeId = [Select Id from RecordType where SObjectType = 'Account_Plan_2__c' AND DeveloperName = 'EUR_DIB_sBU'].Id;
		insert accPlan;	
		
		Department_Master__c adult1 = new Department_Master__c( Name = 'Adult Type 1');		
		insert adult1;
			
		Diabetes_Performance__c dibPerformance = new Diabetes_Performance__c();
		dibPerformance.Account_Plan__c = accPlan.Id;
		dibPerformance.Department_Lookup__c = adult1.Id;
		dibPerformance.Mobile_Id__c = 'UnitTestDiBPerformance';
		dibPerformance.Product_Type__c = 'Pumps';
		dibPerformance.Pump_Origin__c = 'NPNP';
		dibPerformance.Actual_QTD_Unit__c = 20;
		dibPerformance.Target_Current_FY_Unit__c = 10; 
		dibPerformance.Total_actual_FYTD_Unit__c = 11;
		dibPerformance.Total_last_FY_Unit__c = 5;
		insert dibPerformance;
	}
}