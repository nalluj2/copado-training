@isTest
private class Test_ctrlExt_Load_Opp_Tender_Lots {
    
    private static testmethod void testLoadOppLoats(){
    	
    	RecordType tenderRT = [Select Name, Id from RecordType where sObjectType = 'Opportunity' AND DeveloperName = 'Business_Critical_Tender'];
    	
    	Account acc = new Account();        
        acc.Name = 'Test Germany';        
        acc.Account_Country_vs__c = 'GERMANY';
        acc.RecordTypeId = [Select Id from RecordType where SObjectType = 'Account' AND DeveloperName = 'SAP_Account'].Id;        
        acc.Account_Active__c = true;
        acc.SAP_ID__c = '1111111111';
        //acc.SAP_Channel__c = '30';
        insert acc;
        
        Company__c cmpny = new Company__c();		
        cmpny.name='Europe';
        cmpny.CurrencyIsoCode = 'EUR';
        cmpny.Current_day_in_Q1__c=56;
        cmpny.Current_day_in_Q2__c=34; 
        cmpny.Current_day_in_Q3__c=5; 
        cmpny.Current_day_in_Q4__c= 0;   
        cmpny.Current_day_in_year__c =200;
        cmpny.Days_in_Q1__c=56;  
        cmpny.Days_in_Q2__c=34;
        cmpny.Days_in_Q3__c=13;
        cmpny.Days_in_Q4__c=22;
        cmpny.Days_in_year__c =250;
        cmpny.Company_Code_Text__c = 'T35';
		insert cmpny;
		
		Business_Unit_Group__c bug =  new Business_Unit_Group__c();
        bug.Master_Data__c =cmpny.id;
        bug.name='RTG';      
        insert bug;
		
		Business_Unit__c bu =  new Business_Unit__c();
        bu.Company__c =cmpny.id;
        bu.name='RTG BU';
        bu.Business_Unit_Group__c = bug.Id;
        bu.Account_Plan_Activities__c=true;        
        insert bu;
        
        Sub_Business_Units__c sbu = new Sub_Business_Units__c();
        sbu.name='RTG sBU';
        sbu.Business_Unit__c=bu.id;
		insert sbu;
        
        Opportunity tenderOpp = new Opportunity();
		tenderOpp.RecordTypeId = tenderRT.Id;
		tenderOpp.AccountId = acc.Id;
		tenderOpp.Name = 'Test Tender Opportunity';
        tenderOpp.CloseDate = Date.today().addDays(30);
        tenderOpp.StageName = 'Prospecting/Lead';
		tenderOpp.Contract_Number__c = '123456789';
		tenderOpp.Current_Extended_To_Date__c = Date.today().addYears(1);
		insert tenderOpp;
    	
    	User currentUser = new User(Id = UserInfo.getUserId());
		currentUser.Company_Code_text__c = 'T35';
		update currentUser;
		
		Test.startTest();
		
		Opportunity_Lot__c oppLot = new Opportunity_Lot__c();
		oppLot.Opportunity__c = tenderOpp.Id;
		oppLot.Name = 'Unit Test Lot';
		oppLot.Sub_Business_Unit__c = sbu.Id;
		oppLot.Account__c = acc.Id;
		oppLot.Award_Type__c = 'Multi Award';
		oppLot.Lot_Owner__c = UserInfo.getUserId();
		oppLot.Price_Weight__c = 25;
		oppLot.Quality_Weight__c = 25;
		oppLot.Other_Weight__c = 50;
		insert oppLot;

		List<Opportunity_Lot__c> lstOpportunityLot = [SELECT Id, Name, Opportunity__c FROM Opportunity_Lot__c];
		System.assertEquals(lstOpportunityLot.size(), 1);
		
		ctrlExt_Load_Opportunity_Tender_Lots controller = new ctrlExt_Load_Opportunity_Tender_Lots(new ApexPages.StandardController(tenderOpp));
		
		String fileContent = 'Id,Award_Type__c,Lot_Owner__c,Reason_for_No_Offer__c,Current_Potential__c,Actual_Revenue_Vendavo__c,Status__c,Num_of_Suppliers__c,Reason_for_Losing_Winning__c,Account__c,Product_Offered__c,Vendavo_Lot_Id__c,CurrencyIsoCode,Estimated_Revenue__c,Full_Potential__c,Name,Sub_Business_Unit__c,Winner__c,Other_Weight__c,Quality_Weight__c,Price_Weight__c\n';
		fileContent += oppLot.Id + ',Hunting License,System,,,,,,,,,,EUR,,,Test unit test,RTG sBU,,0,0,100\n';
		fileContent += ',Multi Award,System,,,,,,,,,,EUR,,,Test unit test 2,,,0,0,100\n';		
		
		controller.inputData = Blob.valueOf(fileContent);
		
		controller.loadLots();
		
		lstOpportunityLot = [SELECT Id, Name, Opportunity__c FROM Opportunity_Lot__c];
		System.assertEquals(lstOpportunityLot.size(), 2);

		oppLot = [Select Id, Account__c, Lot_Owner__r.Alias_Unique__c, Sub_Business_Unit__c from Opportunity_Lot__c where Opportunity__c = :tenderOpp.Id AND Name = 'Test unit test'];		
		System.assert(oppLot.Lot_Owner__r.Alias_Unique__c == 'System');		
		
		Opportunity_Lot__c oppLot2 = [Select Id, Account__c, Award_Type__c, Lot_Owner__r.Alias_Unique__c, Sub_Business_Unit__c from Opportunity_Lot__c where Opportunity__c = :tenderOpp.Id AND Name = 'Test unit test 2'];		
		System.assert(oppLot2.Lot_Owner__r.Alias_Unique__c == 'System');
		System.assert(oppLot2.Account__c == null);
		System.assert(oppLot2.Award_Type__c == 'Multi Award');		
    }
    
}