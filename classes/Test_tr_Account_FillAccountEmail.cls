/*
 *      Author 		: Bart Caelen
 *		Date		: 2014-02
 *      Description : 
 *			TEST Class for tr_Account_FillAccountEmail
 *		Version		:  1.0
*/
@isTest private class Test_tr_Account_FillAccountEmail {

	// The original functionality has been changed. Now both Email fields are beeing kept in sync by a workflow
    @isTest static void test_tr_Account_FillAccountEmail() {
				
		//----------------------------------------
		// CREATE TEST DATA
		//----------------------------------------
		clsTestData.idRecordType_PersonAccount 	= RecordTypeMedtronic.Account('SAP_Patient').Id;
		clsTestData.tCountry_PersonAccount		= 'CANADA';
		clsTestData.createPersonAccountData();
		//----------------------------------------

		Account oAccount = [SELECT ID, Account_Email__c, PersonEmail FROM Account WHERE Id = :clsTestData.oMain_PersonAccount.Id];
		system.assertEquals(oAccount.Account_Email__c, null);
		system.assertEquals(oAccount.PersonEmail, null);

		clsTestData.oMain_PersonAccount.Account_Email__c = 'test@medtronic.com';
		update clsTestData.oMain_PersonAccount;

		oAccount = [SELECT ID, Account_Email__c, PersonEmail FROM Account WHERE Id = :clsTestData.oMain_PersonAccount.Id];
		system.assertEquals(oAccount.Account_Email__c, 'test@medtronic.com');
		system.assertEquals(oAccount.PersonEmail, 'test@medtronic.com');	
    }
}