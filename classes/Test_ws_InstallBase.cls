//  Author           : Tom Ansley
//  Created Date     : 02-29-2016
//  Description      : APEX Test Class to test the logic in ws_InstallBaseSAPService
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
@isTest
private class Test_ws_InstallBase {
	
	@testSetup
    static void createTestData() {
        
        // Create Custom Setting - WebService Setting Data
        clsTestData.createCustomSettingData_WebServiceSetting(true);

		clsTestData.createCustomSettingData_WebServiceMappings(true);

        clsTestData.createAccountData(false);
        
        Account acct = clsTestData.oMain_Account;
        acct.SAP_Id__c = '0001928641';
        insert acct;
        
        // Create Installed Product Data
        clsTestData.createSVMXCInstalledProductData(false);
        SVMXC__Installed_Product__c product = clsTestData.oMain_SVMXCInstalledProduct;
        product.SVMX_SAP_Equipment_ID__c = '00000000036078768';
        product.Last_Disinfection_Date__c = Date.today().addDays(-1);
        insert product;
    }
    
    @isTest
    static void testGetInstallBaseWS()
    {
    	ws_InstallBaseSAPService.SAPInstallBaseRequest installBase = new ws_InstallBaseSAPService.SAPInstallBaseRequest();
    	
		installBase.INSTALL_PRODUCT_ID   = '00000000036078768';
    	    	    	
        // Load User Data - SAP Interface
		User oUser = [SELECT Id, Name, isActive FROM User WHERE (Profile.Name = 'SAP Interface') AND IsActive = true LIMIT 1];

        Test.startTest();

        System.runAs(oUser){
        	
        	//get install base
    		ws_InstallBaseSAPService.SAPInstallBaseResponse response = ws_InstallBaseSAPService.getInstallBaseUpdate(installBase);
    		
        }
    	
    	Test.stopTest();
    	
    }
    
    @isTest
    static void testUpdateInstallBaseAckWS()
    {
    	
        // Load User Data - SAP Interface
		User oUser = [SELECT Id, Name, isActive FROM User WHERE (Profile.Name = 'SAP Interface') AND IsActive = true LIMIT 1];

        Test.startTest();

        System.runAs(oUser){
    		    		
    		ws_InstallBaseSAPService.SAPInstallBaseAck ack = new ws_InstallBaseSAPService.SAPInstallBaseAck();
    		ack.INSTALL_PRODUCT_ID = '00000000036078768';
    		ack.DISTRIBUTION_STATUS = 'TRUE';
    		
    		//update valid acknowledgement
    		ack = ws_InstallBaseSAPService.updateInstallBaseAcknowledgement(ack);
 		   	
 		   	ack.INSTALL_PRODUCT_ID = 'Invalid';
    		ack.DISTRIBUTION_STATUS = 'TRUE';
    		
    		//update invalid acknowledgement
    		ack = ws_InstallBaseSAPService.updateInstallBaseAcknowledgement(ack);
        }
    	
    	Test.stopTest();
    	
    }

}