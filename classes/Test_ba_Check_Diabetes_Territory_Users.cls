@isTest
private class Test_ba_Check_Diabetes_Territory_Users{
	
	private static testmethod void testBatchCheckDibUsers(){
		
		UserTerritory2Association dibUserAssig = [Select UserId, Territory2Id from UserTerritory2Association where Territory2.Country_UID__c = 'CA' AND isActive = true LIMIT 1];
		
		UserRole nonDibRole = [Select Id, name from USerRole where PortalType != 'Partner' and Name LIKE '%(RTG)%' LIMIT 1];
		
		User dibUSer = new User(Id = dibUserAssig.UserId);
		dibUser.UserRoleId = nonDibRole.Id;
		
		update dibUser;
		
		Test.startTest();
		
		ba_Check_Diabetes_Territory_Users batch = new ba_Check_Diabetes_Territory_Users();
		batch.testTerritoryId = dibUserAssig.Territory2Id;
		Database.executeBatch(batch);
		
		Test.stopTest();
		
	}
	
}