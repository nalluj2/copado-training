//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 18-04-2016
//  Description      : APEX Class - Business Logic for tr_User
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public class bl_User_Trigger {

    //--------------------------------------------------------------------------------------------------------------------
    // Public variables coming from the trigger
    //--------------------------------------------------------------------------------------------------------------------
    public static List<User> lstTriggerNew = new List<User>();
    public static Map<Id, User> mapTriggerNew = new Map<Id, User>();
    public static Map<Id, User> mapTriggerOld = new Map<Id, User>();
    //--------------------------------------------------------------------------------------------------------------------


    //--------------------------------------------------------------------------------------------------------------------
    // Process Users and add them to or remove them from an existing Public Group
    //--------------------------------------------------------------------------------------------------------------------
    public static void setPublicGroupAssignment_Update(){

		// Load Master Data
		Map<String, Map<String, Map<String, Map<String, String>>>> mapRegion_SBU_Country_JobTitle_GroupName = bl_MasterData.getRegion_SBU_Country_JobTitle_GroupName();

		// Collect all Users that need to be processed based on their Region, Country, Primary SBU and JobTitle
		List<User> lstUser_Processing = new List<User>();
		List<User> lstUser_Processing_Old = new List<User>();

		for (User oUser : lstTriggerNew){
			User oUser_Old = mapTriggerOld.get(oUser.Id);

			if (
				(oUser.Region_vs__c != oUser_Old.Region_vs__c)
				||
				(oUser.Country_vs__c != oUser_Old.Country_vs__c)
				||
				(oUser.Primary_sBU__c != oUser_Old.Primary_sBU__c)
				||
				(oUser.Job_Title_vs__c != oUser_Old.Job_Title_vs__c)
			){
				if (
					(bl_MasterData.setRegion.contains(oUser.Region_vs__c))
					&&
					(bl_MasterData.setCountry.contains(oUser.Country_vs__c))
					&&
					(bl_MasterData.setSBU.contains(oUser.Primary_sBU__c))
					&&
					(bl_MasterData.setJobTitle.contains(oUser.Job_Title_vs__c))
				){
						lstUser_Processing.add(oUser);
				}

				if (
					( (bl_MasterData.setRegion.contains(oUser.Region_vs__c)) || (bl_MasterData.setRegion.contains(oUser_Old.Region_vs__c)) )
					&&
					( (bl_MasterData.setCountry.contains(oUser.Country_vs__c)) || (bl_MasterData.setCountry.contains(oUser_Old.Country_vs__c)) )
					&&
					( (bl_MasterData.setSBU.contains(oUser.Primary_sBU__c)) || (bl_MasterData.setSBU.contains(oUser_Old.Primary_sBU__c)) )
					&&
					( (bl_MasterData.setJobTitle.contains(oUser.Job_Title_vs__c)) || (bl_MasterData.setJobTitle.contains(oUser_Old.Job_Title_vs__c)) )
				){
					lstUser_Processing_Old.add(oUser_Old);
				}

			}
		}

		Map<String, Id> mapGroup;
		List<GroupMember> lstGroupMember_Insert = new List<GroupMember>();
		List<GroupMember> lstGroupMember_Delete = new List<GroupMember>();

		if ( (lstUser_Processing.size() > 0 ) || (lstUser_Processing_Old.size() > 0) ){
			// Get a Map of the Group Name and Group ID
			mapGroup = loadGroupData(bl_MasterData.setGroupName);	
		}

		if (lstUser_Processing.size() > 0){

			// Add each processing user to the correct Public Group if the Public Group exists
			for (User oUser : lstUser_Processing){

				String tGroupName = '';

				if (mapRegion_SBU_Country_JobTitle_GroupName.containsKey(oUser.Region_vs__c)){
					Map<String, Map<String, Map<String, String>>> mapSBU_Country_Jobtitle_GroupName = mapRegion_SBU_Country_JobTitle_GroupName.get(oUser.Region_vs__c);
					if (mapSBU_Country_Jobtitle_GroupName.containsKey(oUser.Primary_sBU__c)){
						Map<String, Map<String, String>> mapCountry_Jobtitle_GroupName = mapSBU_Country_Jobtitle_GroupName.get(oUser.Primary_sBU__c);
						if (mapCountry_Jobtitle_GroupName.containsKey(oUser.Country_vs__c)){
							Map<String, String> mapJobtitle_GroupName = mapCountry_Jobtitle_GroupName.get(oUser.Country_vs__c);
							if (mapJobtitle_GroupName.containsKey(oUser.Job_Title_vs__c)){
								tGroupName = mapJobtitle_GroupName.get(oUser.Job_Title_vs__c);

								if (mapGroup.containsKey(tGroupName)){
									GroupMember oGroupMember = new GroupMember();
										oGroupMember.UserOrGroupId = oUser.Id;
										oGroupMember.GroupId = mapGroup.get(tGroupName);
									lstGroupMember_Insert.add(oGroupMember);
								}
							}
						}
					}
				}

			}

		}

		if (lstUser_Processing_Old.size() > 0){

			Map<Id, Set<Id>> mapGroupId_UserId_Delete = new Map<Id, Set<Id>>();

			// Add each processing user to the correct Public Group if the Public Group exists
			for (User oUser_Old : lstUser_Processing_Old){

				String tGroupName_Old = '';

				if (mapRegion_SBU_Country_JobTitle_GroupName.containsKey(oUser_Old.Region_vs__c)){
					Map<String, Map<String, Map<String, String>>> mapSBU_Country_Jobtitle_GroupName = mapRegion_SBU_Country_JobTitle_GroupName.get(oUser_Old.Region_vs__c);
					if (mapSBU_Country_Jobtitle_GroupName.containsKey(oUser_Old.Primary_sBU__c)){
						Map<String, Map<String, String>> mapCountry_Jobtitle_GroupName = mapSBU_Country_Jobtitle_GroupName.get(oUser_Old.Primary_sBU__c);
						if (mapCountry_Jobtitle_GroupName.containsKey(oUser_Old.Country_vs__c)){
							Map<String, String> mapJobtitle_GroupName = mapCountry_Jobtitle_GroupName.get(oUser_Old.Country_vs__c);
							if (mapJobtitle_GroupName.containsKey(oUser_Old.Job_Title_vs__c)){
								tGroupName_Old = mapJobtitle_GroupName.get(oUser_Old.Job_Title_vs__c);

								if (mapGroup.containsKey(tGroupName_Old)){

									Set<Id> setId_User = new Set<Id>();
									if (mapGroupId_UserId_Delete.containsKey(mapGroup.get(tGroupName_Old))){
										setId_User = mapGroupId_UserId_Delete.get(mapGroup.get(tGroupName_Old));
									}
									setId_User.add(oUser_Old.Id);

									mapGroupId_UserId_Delete.put(mapGroup.get(tGroupName_Old), setId_User);
								}
							}
						}
					}
				}

			}

			// Select the Group Members that need to be deleted
			//	We might need to delete multiple Members from a Group but also multiple Groups
			if (mapGroupId_UserId_Delete.size() > 0){
				String tSOQL = 'SELECT Id';
					tSOQL += ' FROM GroupMember';

				String tWHERE = '';
				for (Id idGroup : mapGroupId_UserId_Delete.keySet()){
					if (tWHERE != ''){
						tWHERE += ' OR ';
					}
					Set<Id> setId_User = mapGroupId_UserId_Delete.get(idGroup);
					tWHERE += ' (GroupId = \'' + String.escapeSingleQuotes(idGroup) + '\' AND (';
					String tSOQL_UserId = '';
					for (Id idUser : setId_User){
						if (tSOQL_UserId != ''){
							tSOQL_UserId += ' OR ';
						}
						tSOQL_UserId += ' (UserOrGroupId = \'' + String.escapeSingleQuotes(idUser) + '\')';
					}
					tWHERE += tSOQL_UserId;
					tWHERE += '))';
				}

				if (tWHERE != ''){
					tSOQL += ' WHERE ' + tWHERE;
					lstGroupMember_Delete = Database.query(tSOQL);
				}
			}					
		}					

		Savepoint oSavePoint = Database.setSavepoint();

		try{
			clsUtil.bubbleException();
			
			// Perform the INSERT
			if (lstGroupMember_Insert.size() > 0){
				setPublicGroupAssignment_Future(JSON.serialize(lstGroupMember_Insert), 'INSERT');
			}

			// Perform the DELETE
			if (lstGroupMember_Delete.size() > 0){
				setPublicGroupAssignment_Future(JSON.serialize(lstGroupMember_Delete), 'DELETE');
			}

        }catch(Exception oEX){
            throw new clsUtil.cException('An error occured in bl_User_Trigger.setPublicGroupAssignment_Update on line ' + oEX.getLineNumber() + ' : ' + oEX.getMessage());
            Database.rollback(oSavePoint);
        }

    }

    @Future
    private static void setPublicGroupAssignment_Future(String lstGroupMember_Serialized, String tAction){

    	List<GroupMember> lstGroupMember = (List<GroupMember>)JSON.deserialize(lstGroupMember_Serialized, List<GroupMember>.class);

    	if (tAction == 'INSERT'){
    		insert lstGroupMember;
		}else if (tAction == 'DELETE'){
			delete lstGroupMember;
		}

    }
    //--------------------------------------------------------------------------------------------------------------------


    //--------------------------------------------------------------------------------------------------------------------
    // Load Group / Queue data (Name - Id)
    //--------------------------------------------------------------------------------------------------------------------
    private static Map<String, Id> loadGroupData(Set<String> aSetGroupName){

    	Map<String, Id> mapGroup = new Map<String, Id>();

		List<Group> lstGroup = [SELECT Id, DeveloperName FROM Group WHERE DeveloperName = :aSetGroupName];
		for (Group oGroup : lstGroup){
			mapGroup.put(oGroup.DeveloperName, oGroup.Id);
		}

    	return mapGroup;

    }
    //--------------------------------------------------------------------------------------------------------------------

}
//--------------------------------------------------------------------------------------------------------------------