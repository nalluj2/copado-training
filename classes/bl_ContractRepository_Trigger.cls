//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 16/07/2020
//  Description      : APEX Class - Business Logic for tr_ContractRepository
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public class bl_ContractRepository_Trigger{

    //------------------------------------------------------------------------------------------------------------------------------------------
    // Create History_Tracking__c records based on the changes on the Contract_Repository__c record
    //------------------------------------------------------------------------------------------------------------------------------------------
    public static void createHistoryTracking(List<Contract_Repository__c> lstTriggerNew, Map<Id, Contract_Repository__c> mapTriggerOld){

        if (bl_Trigger_Deactivation.isTriggerDeactivated('contrRepos_createHistoryTracking')) return;

        List<History_Tracking__c> lstHistoryTracking = new List<History_Tracking__c>();


        // The field name's that should be tracked
        Set<String> setFieldName_HistoryTracking = new Set<String>();

        // Load Related Data
        Map<Id, String> mapRelatedData = new Map<Id, String>();
        Map<String, HistoryTracking_ContractRepository__c> mapHistoryTracking = HistoryTracking_ContractRepository__c.getAll();
        for (String tKey : mapHistoryTracking.keySet()){

            HistoryTracking_ContractRepository__c oHistoryTracking = mapHistoryTracking.get(tKey);
            String tFieldName = oHistoryTracking.Field_Name__c;
            String tReferenceTo = oHistoryTracking.Reference_To__c;
            String tReferenceToField = oHistoryTracking.Reference_To_Field__c;

            if ( (!String.isBlank(tReferenceTo)) && (!String.isBlank(tReferenceToField)) ){

                mapRelatedData.putAll(bl_HistoryTracking.loadRelatedData(tReferenceTo, tReferenceToField, lstTriggerNew, mapTriggerOld, tFieldName));

            }

            setFieldName_HistoryTracking.add(tFieldName);

        }

        if (setFieldName_HistoryTracking.size() == 0) return;

        for (Contract_Repository__c oContractRepository : lstTriggerNew){

            Contract_Repository__c oContractRepository_Old;
            if (mapTriggerOld != null){

                oContractRepository_Old = mapTriggerOld.get(oContractRepository.Id);

            }

            lstHistoryTracking.addAll(bl_HistoryTracking.createHistoryTracking('Contract_Repository__c', 'Contract_Repository__c', oContractRepository_Old, oContractRepository, setFieldName_HistoryTracking, mapRelatedData, false));

        }

        if (lstHistoryTracking.size() > 0){

            insert lstHistoryTracking;
    
        }

    }
    //------------------------------------------------------------------------------------------------------------------------------------------
    

    //------------------------------------------------------------------------------------------------------------------------------------------
    // Populate the Contract Repository record with data from the related Account
    //------------------------------------------------------------------------------------------------------------------------------------------
    public static void populateWithAccountData(List<Contract_Repository__c> lstTriggerNew, Map<Id, Contract_Repository__c> mapTriggerOld){

        if (bl_Trigger_Deactivation.isTriggerDeactivated('contrRepos_populateWithAccountData')) return;

        // Define the fieldmapping between Account and Contract Repository
        Map<String, String> mapFieldMapping_ContractRepository_Account = new Map<String, String>();
            mapFieldMapping_ContractRepository_Account.put('Delivery_Terms__c', 'Delivery_Terms__c');
            mapFieldMapping_ContractRepository_Account.put('Account_Country__c', 'Account_Country_vs__c');
            mapFieldMapping_ContractRepository_Account.put('Payment_Terms__c', 'SAP_Payment_Terms__c');
        

        // Collect the Account ID's of the processing records when the Contract Repository record is created or when the Account is changed
        Set<Id> setID_Account = new Set<Id>();
        for (Contract_Repository__c oContractRepository : lstTriggerNew){

            if ( (mapTriggerOld == null) || (oContractRepository.Account__c != mapTriggerOld.get(oContractRepository.Id).Account__c) 
                    //CR-28627
                    || (String.isBlank(oContractRepository.Delivery_Terms__c) && String.isNotBlank(mapTriggerOld.get(oContractRepository.Id).Delivery_Terms__c))
                        ||(String.isBlank(oContractRepository.Payment_Terms__c) && String.isNotBlank(mapTriggerOld.get(oContractRepository.Id).Payment_Terms__c))){
    
                setID_Account.add(oContractRepository.Account__c);
            
            }

        }


        if (setID_Account.size() == 0) return;

        // Get the Account records with the fields that need to be mapped
        String tSOQL = 'SELECT Id';
        for (String tFieldName : mapFieldMapping_ContractRepository_Account.values()){
            tSOQL += ',' + tFieldName;
        }
        tSOQL += ' FROM Account WHERE Id = :setID_Account';
        Map<Id, Account> mapAccount = new Map<Id, Account>((List<Account>)Database.query(tSOQL));

        // Popualte the Contract Repository fields with the data from the corresponding Account fields
        for (Contract_Repository__c oContractRepository : lstTriggerNew){
            
            if (mapAccount.containsKey(oContractRepository.Account__c)){

                Account oAccount = mapAccount.get(oContractRepository.Account__c);

                for (String tFieldName : mapFieldMapping_ContractRepository_Account.keySet()){
                    //CR-28627
                    if(!((tFieldName.equalsIgnoreCase('Delivery_Terms__c') || tFieldName.equalsIgnoreCase('Payment_Terms__c')) 
                                && String.isNotBlank((String)oContractRepository.get(tFieldName))))
                    oContractRepository.put(tFieldName, oAccount.get(mapFieldMapping_ContractRepository_Account.get(tFieldName)));
                    
                }

            }
        
        }

    }
    //------------------------------------------------------------------------------------------------------------------------------------------


    public static void createOwnerTeamMember(List<Contract_Repository__c> triggerNew, Map<Id, Contract_Repository__c> oldMap){
                
        List<Contract_Repository__c> toProcess = new List<Contract_Repository__c>();
        
        for(Contract_Repository__c contract : triggerNew){
            
            if(oldMap == null || oldMap.get(contract.Id).OwnerId != contract.OwnerId) toProcess.add(contract);          
        }
        
        if(toProcess.size() > 0){
            
            Map<String, Contract_Repository_Team__c> contractTeamMap = new Map<String, Contract_Repository_Team__c>();
            
            if(oldMap != null){
                
                for(Contract_Repository_Team__c contractTeam : [Select Id, Team_Member__c, Contract_Repository__c, Access_Level__c, Auto_Aligned_with_Territory__c, Contract_Repository_Owner__c from Contract_Repository_Team__c where Contract_Repository__c IN :toProcess]){
                
                    contractTeamMap.put(contractTeam.Contract_Repository__c + ':' + contractTeam.Team_Member__c, contractTeam);
                }
            }
            
            List<Contract_Repository_Team__c> toUpsert = new List<Contract_Repository_Team__c>();
            List<Contract_Repository_Team__c> toDelete = new List<Contract_Repository_Team__c>();
            
            for(Contract_Repository__c contract : toProcess){
                
                //Owner             
                Contract_Repository_Team__c owner = contractTeamMap.get(contract.Id + ':' + contract.OwnerId);
                
                if(owner == null){
                    
                    owner = new Contract_Repository_Team__c();
                    
                    owner.Contract_Repository__c = contract.Id;
                    owner.Team_Member__c = contract.OwnerId;    
                }
                
                owner.Auto_Aligned_with_Territory__c = false;
                owner.Contract_Repository_Owner__c = true;
                owner.Access_Level__c = 'Read/Write';
                
                toUpsert.add(owner);
                
                //Prev Owner
                if(oldMap != null){
                    
                    Contract_Repository_Team__c prevOwner = contractTeamMap.get(contract.Id + ':' + oldMap.get(contract.Id).OwnerId);
                    
                    if(prevOwner != null){
                        
                        prevOwner.Contract_Repository_Owner__c = false;
                        
                        toDelete.add(prevOwner);
                    }
                }
            }
            
            bl_Contract_Repository_Team.isAutoProcessRunning = true;
            
            if(toDelete.size() > 0) delete toDelete;
            if(toUpsert.size() > 0) upsert toUpsert;            
            
            bl_Contract_Repository_Team.isAutoProcessRunning = false;
        }
    }
    
    public static void createDefaultTeamMembers(List<Contract_Repository__c> triggerNew, Map<Id, Contract_Repository__c> oldMap){
        
        List<Id> toProcess = new List<Id>();
        
        for(Contract_Repository__c contract : triggerNew){
            
            if( oldMap == null  
                || oldMap.get(contract.Id).Account__c != contract.Account__c 
                || oldMap.get(contract.Id).Primary_Contract_Type__c != contract.Primary_Contract_Type__c 
                || oldMap.get(contract.Id).MPG_Code__c != contract.MPG_Code__c){
                    
                toProcess.add(contract.Id);
            }           
        }
        
        if(toProcess.size() > 0) bl_ContractRepository.createDefaultContractTeam(toProcess);
    }
    
    public static void createPrimaryAccount(List<Contract_Repository__c> triggerNew, Map<Id, Contract_Repository__c> oldMap){
        
        Set<String> toDeleteKeys = new Set<String>();
        List<Contract_Repository_Account__c> toUpsert = new List<Contract_Repository_Account__c>();
        
        for(Contract_Repository__c contract : triggerNew){
            
            if(oldMap == null || contract.Account__c != oldMap.get(contract.Id).Account__c){
                
                Contract_Repository_Account__c contractAcc = new Contract_Repository_Account__c();
                contractAcc.Contract_Repository__c = contract.Id;
                contractAcc.Account__c = contract.Account__c;
                contractAcc.Unique_Key__c = contract.Id + ':' + contract.Account__c;
                contractAcc.Created_By_Auto_Assignment_Job__c = true;
                toUpsert.add(contractAcc);
            }
            
            if(oldMap !=null && contract.Account__c != oldMap.get(contract.Id).Account__c){
                
                toDeleteKeys.add(contract.Id + ':' + oldMap.get(contract.Id).Account__c);
            }
        }
        
        upsert toUpsert Unique_Key__c;
            
        if(toDeleteKeys.size() > 0) delete [Select Id from Contract_Repository_Account__c where Unique_Key__c IN :toDeleteKeys];
    }
    
    private static Id interfacedContractRTId{
        
        get{
            
            if(interfacedContractRTId == null) interfacedContractRTId = [Select Id from RecordType where SObjectType = 'Contract_Repository__c' AND DeveloperName = 'Interfaced_Contracts'].Id;
            
            return interfacedContractRTId;
        }
        
        set;
    }
    
    public static void protectInterfacedContracts(List<Contract_Repository__c> triggerOld){
        
        if (bl_Trigger_Deactivation.isTriggerDeactivated('contrRepos_protectInterfacedContracts')) return;
        
        if(ProfileMedtronic.isSystemAdministratorProfile(UserInfo.getProfileId()) || ProfileMedtronic.isDevartProfile(UserInfo.getProfileId())) return;     
                
        Set<Id> createdByIds = new Set<Id>();
        
        for(Contract_Repository__c contract : triggerOld){
            
            createdByIds.add(contract.CreatedById);
        }
        
        Map<Id, User> createdByMap = new Map<Id, User>([Select Id, ProfileId, Profile.Name from User where Id IN :createdByIds]);
                
        for(Contract_Repository__c contract : triggerOld){
            
            User createdBy = createdByMap.get(contract.createdById);
            
            if(contract.RecordTypeId == interfacedContractRTId && createdBy.Profile.Name.startsWith('Devart')) contract.addError('Interfaced Contracts cannot be deleted');
        }
    }
        //Contract repo backlog #22
    //This method automatically populates the email reminder dates if there is a contract repository setting record with same country and contract type
    //if no mach it assigns a null value to the reminder fields
    Public static void populateEmailReminderDates (List<Contract_Repository__c> newConRepoLst, Map<Id,Contract_Repository__c> oldConRepoMap){
        
        Set<Id> accIdSet = New Set<Id> ();
        Set<String> contractTypeSet= New Set<String> ();
        Map<Id, String> accCountryMap = New Map<Id, String> ();
        Map<String, Contract_Repository_Setting__c> countrySettingsMap = new Map<String, Contract_Repository_Setting__c>();
        
        for(Contract_Repository__c conRepo: newConRepoLst){
            if((oldConRepoMap == null && (conRepo.Email_Reminder_Date_1__c == null || conRepo.Email_Reminder_Date_2__c == null || conRepo.Email_Reminder_Date_3__c == null)) || (oldConRepoMap != null && (oldConRepoMap.get(conRepo.Id).Account__c != conRepo.Account__c
                || oldConRepoMap.get(conRepo.Id).Primary_Contract_Type__c != conRepo.Primary_Contract_Type__c
                ||oldConRepoMap.get(conRepo.Id).Last_Prolonged_to__c != conRepo.Last_Prolonged_to__c
                ||oldConRepoMap.get(conRepo.Id).Original_Valid_To_Date__c != conRepo.Original_Valid_To_Date__c)))
            accIdSet.add(conRepo.Account__c);
            contractTypeSet.add(conRepo.Primary_Contract_Type__c);
        }
        
        if(!accIdSet.isEmpty())
        for(Account acc:[SELECT Id,Account_Country_vs__c FROM Account WHERE Id IN:accIdSet AND Account_Country_vs__c != null]){
            accCountryMap.put(acc.Id,acc.Account_Country_vs__c.toUpperCase());
        }
        
        if(!accCountryMap.isEmpty()) 
        for(Contract_Repository_Setting__c countrySetting : [SELECT Country__r.Name, Contract_Type__c,X1st_Email_Reminder_of_Days__c,
                                                                X2nd_Email_Reminder_of_Days__c,X3rd_Email_Reminder_of_Days__c FROM Contract_Repository_Setting__c 
                                                                    WHERE Country__r.Name IN:accCountryMap.values() AND Contract_Type__c IN:contractTypeSet AND
                                                                    (X1st_Email_Reminder_of_Days__c != null OR X2nd_Email_Reminder_of_Days__c != null OR X3rd_Email_Reminder_of_Days__c != null)]){
            
            countrySettingsMap.put(countrySetting.Country__r.Name.toUpperCase() + ':' + countrySetting.Contract_Type__c, countrySetting);
        }
        
        for(Contract_Repository__c conRepo: newConRepoLst){
            if(accCountryMap.containsKey(conRepo.Account__c) && countrySettingsMap.containsKey(accCountryMap.get(conRepo.Account__c)+':'+conRepo.Primary_Contract_Type__c)){
                Contract_Repository_Setting__c setting = countrySettingsMap.get(accCountryMap.get(conRepo.Account__c)+':'+conRepo.Primary_Contract_Type__c);
                if(oldConRepoMap != null || conRepo.Email_Reminder_Date_1__c ==null)
                conRepo.Email_Reminder_Date_1__c = setting.X1st_Email_Reminder_of_Days__c !=null?conRepo.Last_Prolonged_to__c != null && conRepo.Last_Prolonged_to__c.Year() != 4000?conRepo.Last_Prolonged_to__c.addDays(-Integer.valueOf(setting.X1st_Email_Reminder_of_Days__c)):conRepo.Original_Valid_To_Date__c != null?conRepo.Original_Valid_To_Date__c.addDays(-Integer.valueOf(setting.X1st_Email_Reminder_of_Days__c)):null:null;
                if(oldConRepoMap != null || conRepo.Email_Reminder_Date_2__c ==null)
                conRepo.Email_Reminder_Date_2__c = setting.X2nd_Email_Reminder_of_Days__c !=null?conRepo.Last_Prolonged_to__c != null && conRepo.Last_Prolonged_to__c.Year() != 4000?conRepo.Last_Prolonged_to__c.addDays(-Integer.valueOf(setting.X2nd_Email_Reminder_of_Days__c)):conRepo.Original_Valid_To_Date__c != null?conRepo.Original_Valid_To_Date__c.addDays(-Integer.valueOf(setting.X2nd_Email_Reminder_of_Days__c)):null:null;
                if(oldConRepoMap != null || conRepo.Email_Reminder_Date_3__c ==null)
                conRepo.Email_Reminder_Date_3__c = setting.X3rd_Email_Reminder_of_Days__c !=null?conRepo.Last_Prolonged_to__c != null && conRepo.Last_Prolonged_to__c.Year() != 4000?conRepo.Last_Prolonged_to__c.addDays(-Integer.valueOf(setting.X3rd_Email_Reminder_of_Days__c)):conRepo.Original_Valid_To_Date__c != null?conRepo.Original_Valid_To_Date__c.addDays(-Integer.valueOf(setting.X3rd_Email_Reminder_of_Days__c)):null:null;
            }            
        }
    }
}
//--------------------------------------------------------------------------------------------------------------------