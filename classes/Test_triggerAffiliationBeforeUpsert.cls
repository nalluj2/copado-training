/**
 * Creation Date :  20090305
 * Description :    Test Coverage for trigger AffiliationBeforeUpsert
 * Author :         ABSI - MC
 */
@isTest
private class Test_triggerAffiliationBeforeUpsert {

    //- BC - 20140624 - CR-3713 - START
    @isTest static void test_duplicateC2ARelationship() {
                
        //Insert Account
        List<Account> lstAccount = new List<Account>();
        
        Account oAccount1 = new Account();
            oAccount1.Name = 'Test_triggerAffiliationBeforeUpsert Test Coverage Account 1 ' ; 
            oAccount1.SAP_ID__c = '0004567898';
            oAccount1.Account_Country_vs__c = 'NETHERLANDS';
        lstAccount.Add(oAccount1);
        
        Account oAccount2 = new Account();
            oAccount2.Name = 'Test_triggerAffiliationBeforeUpsert Test Coverage Account 21 ' ; 
            oAccount2.SAP_ID__c = '0003457896';
            oAccount2.Account_Country_vs__c = 'NETHERLANDS';
        lstAccount.Add(oAccount2);
        insert lstAccount ;

        //Insert Contact               
        Contact oContact1 = new Contact();
            oContact1.LastName = 'Test_triggerAffiliationBeforeUpsert cont 1 ';
            oContact1.FirstName = 'TEST';
            oContact1.AccountId = oAccount1.Id ; 
            oContact1.Contact_Department__c = 'Diabetes Adult'; 
            oContact1.Contact_Primary_Specialty__c = 'ENT';
            oContact1.Affiliation_To_Account__c = 'Employee';
            oContact1.Primary_Job_Title_vs__c = 'Manager';
            oContact1.Contact_Gender__c = 'Male';               
        insert oContact1;
                
        Affiliation__c oAffiliation1 = new Affiliation__c();
            oAffiliation1.RecordTypeId = RecordTypeMedtronic.getRecordTypeByDevName('Affiliation__c', 'C2A').Id;             
            oAffiliation1.Affiliation_From_Contact__c = oContact1.id;             
            oAffiliation1.Affiliation_To_Account__c = oAccount2.Id ;
            oAffiliation1.Affiliation_Type__c ='Employee';                                       
        insert oAffiliation1;
        
        Test.startTesT();
        
        // TEST 1 - Insert record with the same Type as an existing record
        Affiliation__c oAffiliation2 = new Affiliation__c();
            oAffiliation2.RecordTypeId = RecordTypeMedtronic.getRecordTypeByDevName('Affiliation__c', 'C2A').Id;          
            oAffiliation2.Affiliation_From_Contact__c=oContact1.id;           
            oAffiliation2.Affiliation_To_Account__c = oAccount2.Id ;
            oAffiliation2.Affiliation_Type__c ='Employee';           
                    
        Boolean bError = false;
            
        try{
            
            insert oAffiliation2;
            
        }catch(Exception oEX){
            
            clsUtil.debug('ERROR TEST1 : ' + oEX.getMessage());
            System.assert(oEX.getMessage().contains(Label.This_Relationship_already_exists));
            bError = true;
        }
        
        system.assertEquals(bError, true);

        // TEST 2 - Insert record with another Type which is not used yet for the Account/Contact relationship
        oAffiliation2.Affiliation_Type__c = 'Referring';
        
        insert oAffiliation2;
        
        List<Affiliation__c> contactRels = [Select Id from Affiliation__c where Affiliation_From_Contact__c = :oContact1.Id AND Affiliation_To_Account__c = :oAccount2.Id];

		System.assert(contactRels.size() == 2);
    }
            
    @isTest static void test_C2AReferringWithDetailsMandatoryFields() {
        
        //Insert Company
        Company__c oCompany = new Company__c();
            oCompany.name='TestMedtronic';
            oCompany.CurrencyIsoCode = 'EUR';
            oCompany.Current_day_in_Q1__c=56;
            oCompany.Current_day_in_Q2__c=34; 
            oCompany.Current_day_in_Q3__c=5; 
            oCompany.Current_day_in_Q4__c= 0;   
            oCompany.Current_day_in_year__c =200;
            oCompany.Days_in_Q1__c=56;  
            oCompany.Days_in_Q2__c=34;
            oCompany.Days_in_Q3__c=13;
            oCompany.Days_in_Q4__c=22;
            oCompany.Days_in_year__c =250;
            oCompany.Company_Code_Text__c = 'T16';
        insert oCompany;        
        
        DIB_Country__c country = new DIB_Country__c();
			country.name= 'NETHERLANDS';
			country.Company__c = oCompany.Id;
			country.BU_Not_Mandatory_on_Relationship__c = false;
			country.Hide_Therapy_on_Relationship__c = false;
			country.Country_ISO_Code__c = 'NL';
		insert country;
              
       	Business_Unit__c oBU = new Business_Unit__c();
       		oBU.Name = 'Vascular';
       		oBU.Company__c = oCompany.Id;
       		oBU.Abbreviated_Name__c = 'Vascular';
       	insert oBU;

        //Insert SBU        
        Sub_Business_Units__c oSBU1 = new Sub_Business_Units__c();
            oSBU1.name='SBUMedtronic1';
            oSBU1.Business_Unit__c = oBU.id;
            oSBU1.Account_Plan_Default__c = 'Revenue';
            oSBU1.Account_Flag__c = 'Coro_PV__c';
            oSBU1.Contact_Flag__c = 'Coro_PV__c';
        insert oSBU1;

        //Insert Therapy Group
        Therapy_Group__c oTherapyGroup = new Therapy_Group__c();
            oTherapyGroup.Name = 'Therapy Group';
            oTherapyGroup.Sub_Business_Unit__c = oSBU1.Id ;
            oTherapyGroup.Company__c = oCompany.id;      
        insert oTherapyGroup;

        //Insert Therapy
        List<Therapy__c> lstTherapy = new List<Therapy__c>();
        Therapy__c oTherapy1 = new Therapy__c();
            oTherapy1.Name='Therapy 1';
            oTherapy1.Therapy_Group__c = oTherapyGroup.id;
            oTherapy1.Business_Unit__c = oBU.id;
            oTherapy1.Sub_Business_Unit__c = oSBU1.id;
            oTherapy1.Therapy_Name_Hidden__c = 'Therapy';
        lstTherapy.add(oTherapy1);
        Therapy__c oTherapy2 = new Therapy__c();
            oTherapy2.Name='Therapy 2';
            oTherapy2.Therapy_Group__c = oTherapyGroup.id;
            oTherapy2.Business_Unit__c = oBU.id;
            oTherapy2.Sub_Business_Unit__c = oSBU1.id;
            oTherapy2.Therapy_Name_Hidden__c = 'Therapy';
        lstTherapy.add(oTherapy2);
        insert lstTherapy;
        
        //Insert Account
        List<Account> lstAccount = new List<Account>();
        
        Account oAccount1 = new Account();
            oAccount1.Name = 'Test_triggerAffiliationBeforeUpsert Test Coverage Account 1 ' ; 
            oAccount1.SAP_ID__c = '0004567898';
            oAccount1.Account_Country_vs__c = 'NETHERLANDS';
        lstAccount.Add(oAccount1);
        
        Account oAccount2 = new Account();
            oAccount2.Name = 'Test_triggerAffiliationBeforeUpsert Test Coverage Account 21 ' ; 
            oAccount2.SAP_ID__c = '0003457896';
            oAccount2.Account_Country_vs__c = 'NETHERLANDS';
        lstAccount.Add(oAccount2);
        insert lstAccount ;

        //Insert Contact               
        Contact oContact1 = new Contact();
            oContact1.LastName = 'Test_triggerAffiliationBeforeUpsert cont 1 ';
            oContact1.FirstName = 'TEST';
            oContact1.AccountId = oAccount1.Id ; 
            oContact1.Contact_Department__c = 'Diabetes Adult'; 
            oContact1.Contact_Primary_Specialty__c = 'ENT';
            oContact1.Affiliation_To_Account__c = 'Employee';
            oContact1.Primary_Job_Title_vs__c = 'Manager';
            oContact1.Contact_Gender__c = 'Male';               
        insert oContact1;
		
		Test.startTest();

        // TEST 1 - Required field missing (INSERT)
        Affiliation__c oAffiliation1 = new Affiliation__c();
            oAffiliation1.RecordTypeId = RecordTypeMedtronic.getRecordTypeByDevName('Affiliation__c', 'C2A').Id;             
            oAffiliation1.Affiliation_From_Contact__c = oContact1.id;             
            oAffiliation1.Affiliation_Type__c ='Referring with details';        
            oAffiliation1.Affiliation_To_Account__c = oAccount2.Id ;        
        
       	Boolean bError = false;
        
        try{            
            
            insert oAffiliation1;
            
        }catch(Exception oEX){
        	
            clsUtil.debug('ERROR TEST1 : ' + oEX.getMessage());
            
            System.assert(oEX.getMessage().contains(Label.C2A_Therapy_Is_Required));
            bError = true;
        }
        
        System.assertEquals(bError, true);


        // TEST 2 - Required field missing (INSERT)
        oAffiliation1 = new Affiliation__c();
            oAffiliation1.RecordTypeId = RecordTypeMedtronic.getRecordTypeByDevName('Affiliation__c', 'C2A').Id;                         
            oAffiliation1.Affiliation_From_Contact__c=oContact1.id;
            oAffiliation1.Affiliation_To_Account__c = oAccount2.Id ; 
            oAffiliation1.Affiliation_Type__c ='Referring with details';           
            oAffiliation1.Business_Unit__c = oBU.Id;         
        
        bError = false;
            
        try{
            
            insert oAffiliation1;
            
        }catch(Exception oEX){
            
            clsUtil.debug('ERROR TEST2 : ' + oEX.getMessage());
            System.assert(oEX.getMessage().contains(Label.C2A_Therapy_Is_Required));
            bError = true;
        }
        
        System.assertEquals(bError, true);

        // TEST 3 - Required fields OK (INSERT)
        oAffiliation1 = new Affiliation__c();
            oAffiliation1.RecordTypeId = RecordTypeMedtronic.getRecordTypeByDevName('Affiliation__c', 'C2A').Id;        
            oAffiliation1.Affiliation_From_Contact__c=oContact1.id;
            oAffiliation1.Affiliation_To_Account__c = oAccount2.Id ;         
            oAffiliation1.Affiliation_Type__c ='Referring with details';           
            oAffiliation1.Business_Unit__c = oBU.Id;
            oAffiliation1.Therapy__c = oTherapy1.Id;
        
        insert oAffiliation1;
                
        // TEST 4 - Required field missing (UPDATE)
            oAffiliation1.RecordTypeId = RecordTypeMedtronic.getRecordTypeByDevName('Affiliation__c', 'C2A').Id;            
            oAffiliation1.Affiliation_From_Contact__c=oContact1.id;             
            oAffiliation1.Affiliation_To_Account__c = oAccount2.Id ;
            oAffiliation1.Affiliation_Type__c ='Referring with details';            
            oAffiliation1.Business_Unit__c = oBU.Id;
            oAffiliation1.Therapy__c = null;
        
        bError = false;
        
        try{
            
            update oAffiliation1;
            
        }catch(Exception oEX){
        
            clsUtil.debug('ERROR TEST3 : ' + oEX.getMessage());
            System.assert(oEX.getMessage().contains(Label.C2A_Therapy_Is_Required));
            bError = true;
        }
        
        system.assertEquals(bError, true);  
        
        // TEST 5 - Duplicate for same Therapy
        Affiliation__c oAffiliation2 = new Affiliation__c();
            oAffiliation2.RecordTypeId = RecordTypeMedtronic.getRecordTypeByDevName('Affiliation__c', 'C2A').Id;        
            oAffiliation2.Affiliation_From_Contact__c=oContact1.id;
            oAffiliation2.Affiliation_To_Account__c = oAccount2.Id ;         
            oAffiliation2.Affiliation_Type__c ='Referring with details';           
            oAffiliation2.Business_Unit__c = oBU.Id;
            oAffiliation2.Therapy__c = oTherapy1.Id;
        
        bError = false;
        
        try{
            
            insert oAffiliation2; 
            
        }catch(Exception oEX){
        
            clsUtil.debug('ERROR TEST5 : ' + oEX.getMessage());
            System.assert(oEX.getMessage().contains(Label.This_Relationship_already_exists));
            bError = true;
        }
        
        system.assertEquals(bError, true);  
        
        // TEST 6 - Different Therapy        
        oAffiliation2.Therapy__c = oTherapy2.Id;
            
        insert oAffiliation2;
    }
    
    //- BC - 20140624 - CR-3713 - STOP
    
    @isTest static void test_C2CSelfRelationship() {
        
        //Insert Company
        Company__c oCompany = new Company__c();
            oCompany.name='TestMedtronic';
            oCompany.CurrencyIsoCode = 'EUR';
            oCompany.Current_day_in_Q1__c=56;
            oCompany.Current_day_in_Q2__c=34; 
            oCompany.Current_day_in_Q3__c=5; 
            oCompany.Current_day_in_Q4__c= 0;   
            oCompany.Current_day_in_year__c =200;
            oCompany.Days_in_Q1__c=56;  
            oCompany.Days_in_Q2__c=34;
            oCompany.Days_in_Q3__c=13;
            oCompany.Days_in_Q4__c=22;
            oCompany.Days_in_year__c =250;
            oCompany.Company_Code_Text__c = 'T16';
        insert oCompany;        
	
		DIB_Country__c country = new DIB_Country__c();
			country.name= 'NETHERLANDS';
			country.Company__c = oCompany.Id;
			country.BU_Not_Mandatory_on_Relationship__c = true;
			country.Hide_Therapy_on_Relationship__c = true;
			country.Country_ISO_Code__c = 'NL';
		insert country;
        
        //Insert Account                
        Account oAccount = new Account();
            oAccount.Name = 'Test_triggerAffiliationBeforeUpsert Test Coverage Account 1 ' ; 
            oAccount.SAP_ID__c = '0004567898';
            oAccount.Account_Country_vs__c = 'NETHERLANDS';             
        insert oAccount;

        //Insert Contact               
        Contact oContact = new Contact();
            oContact.LastName = 'Test_triggerAffiliationBeforeUpsert cont 1 ';
            oContact.FirstName = 'TEST';
            oContact.AccountId = oAccount.Id ; 
            oContact.Contact_Department__c = 'Diabetes Adult'; 
            oContact.Contact_Primary_Specialty__c = 'ENT';
            oContact.Affiliation_To_Account__c = 'Employee';
            oContact.Primary_Job_Title_vs__c = 'Manager';
            oContact.Contact_Gender__c = 'Male';               
        insert oContact;
        
        Test.startTest();
                
        Affiliation__c oAffiliation = new Affiliation__c();
            oAffiliation.RecordTypeId = RecordTypeMedtronic.getRecordTypeByDevName('Affiliation__c', 'C2C').Id;             
            oAffiliation.Affiliation_From_Contact__c = oContact.id;             
            oAffiliation.Affiliation_To_Contact__c = oContact.Id ;
            oAffiliation.Affiliation_Type__c ='Referring';
        
        Boolean bError = false;
            
        try{
            
            insert oAffiliation;
            
        }catch(Exception oEX){
            
            clsUtil.debug('ERROR TEST : ' + oEX.getMessage());
            System.assert(oEX.getMessage().contains(Label.A_contact_cannot_have_an_affiliation_to_itself));
            bError = true;
        }
        
        system.assertEquals(bError, true);        
    }
    
    @isTest static void test_duplicateC2CRelationship() {
        
        //Insert Company
        Company__c oCompany = new Company__c();
            oCompany.name='TestMedtronic';
            oCompany.CurrencyIsoCode = 'EUR';
            oCompany.Current_day_in_Q1__c=56;
            oCompany.Current_day_in_Q2__c=34; 
            oCompany.Current_day_in_Q3__c=5; 
            oCompany.Current_day_in_Q4__c= 0;   
            oCompany.Current_day_in_year__c =200;
            oCompany.Days_in_Q1__c=56;  
            oCompany.Days_in_Q2__c=34;
            oCompany.Days_in_Q3__c=13;
            oCompany.Days_in_Q4__c=22;
            oCompany.Days_in_year__c =250;
            oCompany.Company_Code_Text__c = 'T16';
        insert oCompany;        
	
		DIB_Country__c country = new DIB_Country__c();
			country.name= 'NETHERLANDS';
			country.Company__c = oCompany.Id;
			country.BU_Not_Mandatory_on_Relationship__c = false;
			country.Hide_Therapy_on_Relationship__c = false;
			country.Country_ISO_Code__c = 'NL';
		insert country;
		
		Business_Unit__c oBU = new Business_Unit__c();
       		oBU.Name = 'Vascular';
       		oBU.Company__c = oCompany.Id;
       		oBU.Abbreviated_Name__c = 'Vascular';
       	insert oBU;

        //Insert SBU
        List<Sub_Business_Units__c> lstSBU = new List<Sub_Business_Units__c>();
        Sub_Business_Units__c oSBU1 = new Sub_Business_Units__c();
            oSBU1.name='SBUMedtronic1';
            oSBU1.Business_Unit__c = oBU.id;
            oSBU1.Account_Plan_Default__c = 'Revenue';
            oSBU1.Account_Flag__c = 'Coro_PV__c';
            oSBU1.Contact_Flag__c = 'Coro_PV__c';
        lstSBU.add(oSBU1);

        Sub_Business_Units__c oSBU2 = new Sub_Business_Units__c();
            oSBU2.name='SBUMedtronic2';
            oSBU2.Business_Unit__c = oBU.id;
            oSBU2.Account_Plan_Default__c='Units';
            oSBU2.Account_Flag__c = 'Coro_PV__c';
            oSBU2.Contact_Flag__c = 'Coro_PV__c';
        lstSBU.add(oSBU2);
        insert lstSBU;

        //Insert Therapy Group
        Therapy_Group__c oTherapyGroup = new Therapy_Group__c();
            oTherapyGroup.Name = 'Therapy Group';
            oTherapyGroup.Sub_Business_Unit__c = oSBU2.Id ;
            oTherapyGroup.Company__c = oCompany.id;      
        insert oTherapyGroup;

        //Insert Therapy
        List<Therapy__c> lstTherapy = new List<Therapy__c>();
        Therapy__c oTherapy1 = new Therapy__c();
            oTherapy1.Name='Therapy 1';
            oTherapy1.Therapy_Group__c = oTherapyGroup.id;
            oTherapy1.Business_Unit__c = oBU.id;
            oTherapy1.Sub_Business_Unit__c = oSBU1.id;
            oTherapy1.Therapy_Name_Hidden__c = 'Therapy';
        lstTherapy.add(oTherapy1);
        Therapy__c oTherapy2 = new Therapy__c();
            oTherapy2.Name='Therapy 2';
            oTherapy2.Therapy_Group__c = oTherapyGroup.id;
            oTherapy2.Business_Unit__c = oBU.id;
            oTherapy2.Sub_Business_Unit__c = oSBU1.id;
            oTherapy2.Therapy_Name_Hidden__c = 'Therapy';
        lstTherapy.add(oTherapy2);
        insert lstTherapy;
        
        //Insert Account
        List<Account> lstAccount = new List<Account>();
        Account oAccount1 = new Account();
            oAccount1.Name = 'Test_triggerAffiliationBeforeUpsert Test Coverage Account 1 ' ; 
            oAccount1.SAP_ID__c = '0004567898';
            oAccount1.Account_Country_vs__c = 'NETHERLANDS';
        lstAccount.Add(oAccount1);
        Account oAccount2 = new Account();
            oAccount2.Name = 'Test_triggerAffiliationBeforeUpsert Test Coverage Account 21 ' ; 
            oAccount2.SAP_ID__c = '0003457896';
            oAccount2.Account_Country_vs__c = 'NETHERLANDS';
        lstAccount.Add(oAccount2);
        insert lstAccount;

        //Insert Contact
        List<Contact> lstContact = new List<Contact>();
        Contact oContact1 = new Contact();
            oContact1.LastName = 'Test_triggerAffiliationBeforeUpsert cont 1 ';
            oContact1.FirstName = 'TEST';
            oContact1.AccountId = oAccount1.Id ; 
            oContact1.Contact_Department__c = 'Diabetes Adult'; 
            oContact1.Contact_Primary_Specialty__c = 'ENT';
            oContact1.Affiliation_To_Account__c = 'Employee';
            oContact1.Primary_Job_Title_vs__c = 'Manager';
            oContact1.Contact_Gender__c = 'Male';         
        lstContact.add(oContact1) ;
        Contact oContact2 = new Contact();
            oContact2.LastName = 'Test_triggerAffiliationBeforeUpsert cont 2 ';
            oContact2.FirstName = 'TEST';
            oContact2.AccountId = oAccount2.Id ; 
            oContact2.Contact_Department__c = 'Diabetes Adult'; 
            oContact2.Contact_Primary_Specialty__c = 'ENT';
            oContact2.Affiliation_To_Account__c = 'Employee';
            oContact2.Primary_Job_Title_vs__c = 'Manager';
            oContact2.Contact_Gender__c = 'Male';         
        lstContact.add(oContact2) ;
        insert lstContact;
        
        Test.startTest();
                
        Boolean bError = false;
                        
        Affiliation__c oAffiliation1 = new Affiliation__c();
            oAffiliation1.RecordTypeId = RecordTypeMedtronic.getRecordTypeByDevName('Affiliation__c', 'C2C').Id;             
            oAffiliation1.Affiliation_From_Contact__c = oContact1.id;             
            oAffiliation1.Affiliation_To_Contact__c = oContact2.Id ;
            oAffiliation1.Affiliation_Type__c ='Referring';       
            oAffiliation1.Business_Unit__c = oBU.Id;
            oAffiliation1.Therapy__c = oTherapy1.Id;                                
        insert oAffiliation1;
        
        // TEST 1 - Insert record with the same Type as an existing record
        Affiliation__c oAffiliation2 = new Affiliation__c();
            oAffiliation2.RecordTypeId = RecordTypeMedtronic.getRecordTypeByDevName('Affiliation__c', 'C2C').Id;          
            oAffiliation2.Affiliation_From_Contact__c=oContact1.id;           
            oAffiliation2.Affiliation_To_Contact__c = oContact2.Id;
            oAffiliation2.Affiliation_Type__c ='Referring';    
            oAffiliation2.Business_Unit__c = oBU.Id;
            oAffiliation2.Therapy__c = oTherapy1.Id;       
                    
        bError = false;
            
        try{
            
            insert oAffiliation2;
            
        }catch(Exception oEX){
            
            clsUtil.debug('ERROR TEST1 : ' + oEX.getMessage());
            System.assert(oEX.getMessage().contains(Label.This_Relationship_already_exists));
            bError = true;
        }
        
        system.assertEquals(bError, true);

        // TEST 2 - Insert record with another Type which is not used yet for the Account/Contact relationship
        oAffiliation2.Therapy__c = oTherapy2.Id;        
        insert oAffiliation2;
        
        List<Affiliation__c> contactRels = [Select Id from Affiliation__c where Affiliation_From_Contact__c = :oContact1.Id AND Affiliation_To_Contact__c = :oContact2.Id];

		System.assert(contactRels.size() == 2);
    }

    @isTest static void test_C2C_MandatoryFields() {
                
        //Insert Company
        Company__c oCompany = new Company__c();
            oCompany.name='TestMedtronic';
            oCompany.CurrencyIsoCode = 'EUR';
            oCompany.Current_day_in_Q1__c=56;
            oCompany.Current_day_in_Q2__c=34; 
            oCompany.Current_day_in_Q3__c=5; 
            oCompany.Current_day_in_Q4__c= 0;   
            oCompany.Current_day_in_year__c =200;
            oCompany.Days_in_Q1__c=56;  
            oCompany.Days_in_Q2__c=34;
            oCompany.Days_in_Q3__c=13;
            oCompany.Days_in_Q4__c=22;
            oCompany.Days_in_year__c =250;
            oCompany.Company_Code_Text__c = 'T16';
        insert oCompany;        
	
		DIB_Country__c country = new DIB_Country__c();
			country.name= 'NETHERLANDS';
			country.Company__c = oCompany.Id;
			country.BU_Not_Mandatory_on_Relationship__c = false;
			country.Hide_Therapy_on_Relationship__c = false;
			country.Country_ISO_Code__c = 'NL';
		insert country;
		
        Business_Unit__c oBU = new Business_Unit__c();
       		oBU.Name = 'Vascular';
       		oBU.Company__c = oCompany.Id;
       		oBU.Abbreviated_Name__c = 'Vascular';
       	insert oBU;

        //Insert SBU
        List<Sub_Business_Units__c> lstSBU = new List<Sub_Business_Units__c>();
        Sub_Business_Units__c oSBU1 = new Sub_Business_Units__c();
            oSBU1.name='SBUMedtronic1';
            oSBU1.Business_Unit__c = oBU.id;
            oSBU1.Account_Plan_Default__c = 'Revenue';
            oSBU1.Account_Flag__c = 'Coro_PV__c';
            oSBU1.Contact_Flag__c = 'Coro_PV__c';
        lstSBU.add(oSBU1);

        Sub_Business_Units__c oSBU2 = new Sub_Business_Units__c();
            oSBU2.name='SBUMedtronic2';
            oSBU2.Business_Unit__c = oBU.id;
            oSBU2.Account_Plan_Default__c='Units';
            oSBU2.Account_Flag__c = 'Coro_PV__c';
            oSBU2.Contact_Flag__c = 'Coro_PV__c';
        lstSBU.add(oSBU2);
        insert lstSBU;

        //Insert Therapy Group
        Therapy_Group__c oTherapyGroup = new Therapy_Group__c();
            oTherapyGroup.Name = 'Therapy Group';
            oTherapyGroup.Sub_Business_Unit__c = oSBU2.Id ;
            oTherapyGroup.Company__c = oCompany.id;      
        insert oTherapyGroup;

        //Insert Therapy
        List<Therapy__c> lstTherapy = new List<Therapy__c>();
        Therapy__c oTherapy1 = new Therapy__c();
            oTherapy1.Name='Therapy 1';
            oTherapy1.Therapy_Group__c = oTherapyGroup.id;
            oTherapy1.Business_Unit__c = oBU.id;
            oTherapy1.Sub_Business_Unit__c = oSBU1.id;
            oTherapy1.Therapy_Name_Hidden__c = 'Therapy';
        lstTherapy.add(oTherapy1);
        Therapy__c oTherapy2 = new Therapy__c();
            oTherapy2.Name='Therapy 2';
            oTherapy2.Therapy_Group__c = oTherapyGroup.id;
            oTherapy2.Business_Unit__c = oBU.id;
            oTherapy2.Sub_Business_Unit__c = oSBU1.id;
            oTherapy2.Therapy_Name_Hidden__c = 'Therapy';
        lstTherapy.add(oTherapy2);
        insert lstTherapy;
		
		//Insert Account
        List<Account> lstAccount = new List<Account>();
        Account oAccount1 = new Account();
            oAccount1.Name = 'Test_triggerAffiliationBeforeUpsert Test Coverage Account 1 ' ; 
            oAccount1.SAP_ID__c = '0004567898';
            oAccount1.Account_Country_vs__c = 'NETHERLANDS';
        lstAccount.Add(oAccount1);
        Account oAccount2 = new Account();
            oAccount2.Name = 'Test_triggerAffiliationBeforeUpsert Test Coverage Account 21 ' ; 
            oAccount2.SAP_ID__c = '0003457896';
            oAccount2.Account_Country_vs__c = 'NETHERLANDS';
        lstAccount.Add(oAccount2);
        insert lstAccount;

        //Insert Contact
        List<Contact> lstContact = new List<Contact>();
        Contact oContact1 = new Contact();
            oContact1.LastName = 'Test_triggerAffiliationBeforeUpsert cont 1 ';
            oContact1.FirstName = 'TEST';
            oContact1.AccountId = oAccount1.Id ; 
            oContact1.Contact_Department__c = 'Diabetes Adult'; 
            oContact1.Contact_Primary_Specialty__c = 'ENT';
            oContact1.Affiliation_To_Account__c = 'Employee';
            oContact1.Primary_Job_Title_vs__c = 'Manager';
            oContact1.Contact_Gender__c = 'Male';         
        lstContact.add(oContact1) ;
        Contact oContact2 = new Contact();
            oContact2.LastName = 'Test_triggerAffiliationBeforeUpsert cont 2 ';
            oContact2.FirstName = 'TEST';
            oContact2.AccountId = oAccount2.Id ; 
            oContact2.Contact_Department__c = 'Diabetes Adult'; 
            oContact2.Contact_Primary_Specialty__c = 'ENT';
            oContact2.Affiliation_To_Account__c = 'Employee';
            oContact2.Primary_Job_Title_vs__c = 'Manager';
            oContact2.Contact_Gender__c = 'Male';         
        lstContact.add(oContact2) ;
        insert lstContact;
		
		Test.startTest();
		
        // TEST 1 - Required field missing (INSERT)
        Affiliation__c oAffiliation1 = new Affiliation__c();
	        oAffiliation1.RecordTypeId = RecordTypeMedtronic.getRecordTypeByDevName('Affiliation__c', 'C2C').Id;          
	        oAffiliation1.Affiliation_From_Contact__c = oContact1.id;
	        oAffiliation1.Affiliation_To_Contact__c = oContact2.Id ; 
	        oAffiliation1.Affiliation_Type__c ='Referring'; 
		
		Boolean bError = false;
		                   
        try{
            
            insert oAffiliation1;
            
        }catch(Exception oEX){
            
            clsUtil.debug('ERROR TEST1 : ' + oEX.getMessage());
            
            System.assert(oEX.getMessage().contains('Therapy is mandatory'));
            bError = true;
        }
        
        system.assertEquals(bError, true);

		country.BU_Not_Mandatory_on_Relationship__c = true;
		country.Hide_Therapy_on_Relationship__c = true;
		update country;
		
		//Reset the static variable
		bl_Relationship.countryByNameMap = null;
		
		// TEST 2 - INSERT OK
		
        insert oAffiliation1;        
	}
}