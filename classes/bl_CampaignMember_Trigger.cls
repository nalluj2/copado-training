//---------------------------------------------------------------------------------------------------------------------------------
//	Author 			: Bart Caelen
//  Created Date 	: 22-11-2017
//  Description 	: APEX Class - Business Logic for the APEX Trigger tr_CampaignMember
//  Change Log 		: 
//---------------------------------------------------------------------------------------------------------------------------------
public class bl_CampaignMember_Trigger {

	//-----------------------------------------------------------------------------------------------------------------------------
	// CR-15112 - if the Campaign Member status becomes "Start enrollment" we need to copy some fields to the related Contact.
	//	This logic is only executed for "LEARNING" Campaigns.
	//-----------------------------------------------------------------------------------------------------------------------------
	/**
	* @description 
	* @param lstTriggerNew 
	* @param mapTriggerOld 
	*/ 
	public static void learning_provisioning(List<CampaignMember> lstTriggerNew, Map<Id, CampaignMember> mapTriggerOld){

        if (bl_Trigger_Deactivation.isTriggerDeactivated('cm_updateContactLMS')) return;

		Set<Id> setID_RecordType_Campaign = new Set<Id>{clsUtil.getRecordTypeByDevName('Campaign', 'LEARNING').Id};
		Set<String> setCampaignMemberStatus = new Set<String>{'START ENROLLMENT'};

		// Get the Campaign ID's and Contact ID's of the records that need further processing
		Set<Id> setID_Campaign = new Set<Id>();
		Set<Id> setID_Contact = new Set<Id>();
		
		List<CampaignMember> toProcess = new List<CampaignMember>();
		
		for (CampaignMember oCampaignMember : lstTriggerNew){
			
			if(oCampaignMember.ContactId == null) continue;//A contact is needed 
			
			String tCampaignMemberStatus = clsUtil.isNull(oCampaignMember.Status, '').toUpperCase();
			
			//On insert
			if(mapTriggerOld == null){
				
				if(setCampaignMemberStatus.contains(tCampaignMemberStatus)){
					
					setID_Contact.add(oCampaignMember.ContactId);
					setID_Campaign.add(oCampaignMember.CampaignId);
					
					toProcess.add(oCampaignMember);
				}
			//On update		
			}else{
				
				String tCampaignMemberStatusOld = clsUtil.isNull(mapTriggerOld.get(oCampaignMember.Id).Status, '').toUpperCase();
				
				//If status has changed
				if(tCampaignMemberStatus != tCampaignMemberStatusOld){
				
					if (
						setCampaignMemberStatus.contains(tCampaignMemberStatus)
						|| 
						(
							tCampaignMemberStatusOld == 'ENROLLED'
							&& tCampaignMemberStatus == 'CANCELLED'
						)
					
					){
					
						setID_Contact.add(oCampaignMember.ContactId);
						setID_Campaign.add(oCampaignMember.CampaignId);
						
						toProcess.add(oCampaignMember);
					}					
				}				
			}
		}

		// Validate that logic is not executed when status is updated to Enrolled

		if (toProcess.size() == 0) return; // Nothing to process

		// Get additional data of the Contacts related to the processing Campaign Members
		// We only need to copy data from the Campaign Member to the related Contact if LMS_IsActive__c on Contact is FALSE 
		Map<Id, Contact> mapContact = new Map<Id, Contact>([SELECT Id, LMS_IsActive__c, LMS_Division__c, MailingCountry FROM Contact WHERE Id =: setID_Contact]);

		// Get additional data of the Campaigns related to the processing Campaign Members
		Map<Id, Campaign> mapCampaign = new Map<Id, Campaign>([SELECT Id, Campaign_Country_vs__c, LMS_Language_ISO__c, LMS_Division__c, LMS_ED_Group__c FROM Campaign
				WHERE Id = :setID_Campaign AND RecordTypeId = :setID_RecordType_Campaign AND (LMS_Division__c != null OR LMS_ED_Group__c != null)]);

		List<Contact> lstContact_Update = new List<Contact>();
		Map<Id, Contact_ED_Sync__c> mapContactSync = new Map<Id, Contact_ED_Sync__c>();
		
		System.debug('**BC** bl_CampaignMember_Trigger.learning_provisioning - bl_BMPSIntegration.oAPISettings : ' + bl_BMPSIntegration.oAPISettings);
		if (bl_BMPSIntegration.oAPISettings != null && bl_BMPSIntegration.oAPISettings.Active__c){

			// Use the new Logic and send the request to BMPS

			for (CampaignMember oCampaignMember : toProcess){

				Campaign oCampaign = mapCampaign.get(oCampaignMember.CampaignId);
            
				//If the Campaign is not of Record Type 'LEARNING' or it doesn't have a Division or Group (out of scope)
				if (oCampaign == null) continue;
            
				Contact oContact = mapContact.get(oCampaignMember.ContactId);
            
				//Create Sync record to provision in BPMS
				Contact_ED_Sync__c contactSync = new Contact_ED_Sync__c(Contact__c = oContact.Id, Campaign__c = oCampaign.Id, Campaign_Member_Id__c = oCampaignMember.Id);
				mapContactSync.put(oContact.Id, contactSync);
            
				//If the status has been changed or created to Start enrollment (Activated) and LMS Division is not null (CSOD activation) and the contact is not activate yet
				if (setCampaignMemberStatus.contains(clsUtil.isNull(oCampaignMember.Status, '').toUpperCase()) && oCampaign.LMS_Division__c != null && oContact.LMS_IsActive__c == false){
                                            
					oContact.LMS_Language__c = oCampaign.LMS_Language_ISO__c;
					String tCountryISOCode = '';
					if (!String.isBlank(oCampaign.Campaign_Country_vs__c)){
						tCountryISOCode = clsUtil.getCountryISOCodeByName(oCampaign.Campaign_Country_vs__c);
					}else{
						tCountryISOCode = clsUtil.getCountryISOCodeByName(oContact.MailingCountry);
					}
					oContact.LMS_Country__c = tCountryISOCode;
					oContact.LMS_Division__c = oCampaign.LMS_Division__c;
                
					lstContact_Update.add(oContact);
				}

			}
        

		}else{

			// Use the old Logic and executed the External Directory and CSOD Integrations
 	
			CSOD_API_Settings__c oCSODAPISettings = CSOD_API_Settings__c.getInstance();

			for (CampaignMember oCampaignMember : toProcess){

				Boolean bExecuteCSODIntegration = true;
				if (clsUtil.isNull(oCampaignMember.Status, '').toUpperCase() == 'CANCELLED') bExecuteCSODIntegration = false;
			
				Campaign oCampaign = mapCampaign.get(oCampaignMember.CampaignId);
			
				//If the Campaign is not of Record Type 'LEARNING' or it doesn't have a Division or Group (out of scope)
				if (oCampaign == null) continue;
			
				Contact oContact = mapContact.get(oCampaignMember.ContactId);
			
				//Create Sync record to provision in External Directory or update Groups and start the CSOD Integration if this Integration is active
				if (bExecuteCSODIntegration) bExecuteCSODIntegration = oCSODAPISettings.Active__c;
				Contact_ED_Sync__c contactSync = new Contact_ED_Sync__c(Contact__c = oContact.Id, Campaign__c = oCampaign.Id, CSOD_Integration__c = bExecuteCSODIntegration);
				mapContactSync.put(oContact.Id, contactSync);
			
				//If the status has been changed or created to Start enrollment (Activated) and LMS Division is not null (CSOD activation) and the contact is not activate yet
				if (setCampaignMemberStatus.contains(clsUtil.isNull(oCampaignMember.Status, '').toUpperCase()) && oCampaign.LMS_Division__c != null && oContact.LMS_IsActive__c == false){
											
					oContact.LMS_Language__c = oCampaign.LMS_Language_ISO__c;
					String tCountryISOCode = '';
					if (!String.isBlank(oCampaign.Campaign_Country_vs__c)){
						tCountryISOCode = clsUtil.getCountryISOCodeByName(oCampaign.Campaign_Country_vs__c);
					}else{
						tCountryISOCode = clsUtil.getCountryISOCodeByName(oContact.MailingCountry);
					}
					oContact.LMS_Country__c = tCountryISOCode;
					oContact.LMS_Division__c = oCampaign.LMS_Division__c;
				
					lstContact_Update.add(oContact);
				}
			}

		}
		
		if (lstContact_Update.size() > 0) update lstContact_Update;
				
		if (mapContactSync.size() > 0) insert mapContactSync.values();
	}
	//-----------------------------------------------------------------------------------------------------------------------------

}
//---------------------------------------------------------------------------------------------------------------------------------