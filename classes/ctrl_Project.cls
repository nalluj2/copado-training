/**
 * Creation Date:   20131025
 *
 * Description:     Controller the project console. Contains the logic to get detailled info
 *				    for a selected project (Change Requests, Work Items, ... ). With this info
 *					a visual overview is given about the global status of the project.
 *              
 * Author:  Fabrizio Truzzu
 */

public with sharing class ctrl_Project{
    
    private String projectId;
    private string projectManagerName;
    private Boolean showFilters;
    private Project__c project;
    private List<ChangeRequestWrapper> changeRequestWrapper;
    private List<WorkItemWrapper> workItemWrapper;
    private List<Release_Deployment_Component__c> releaseComponents = new List<Release_Deployment_Component__c>();
    private List<Filter> filters;
    private Integer devNeedle;
    private Integer stagingNeedle;
    private Integer prodNeedle;
    
    private final String id = 'id';
    private final String passed ='Passed';
    
    public ctrl_Project() {
      getSelectedProjectById();
      getChangeRequestsByProjectId();
      getWorkItemsByProjectId();
      getReleaseDeploymentComponents();
      
      filters = new List<Filter>();
    }
    
    private void getSelectedProjectById(){      
       projectId = ApexPages.currentPage().getParameters().get(id);
       
       project =  [Select p.Status__c,p.Start_Date__c,
                          p.Name,
                          p.Project_Manager__c, p.Project_Type__c,
                          p.End_Date__c
                   From Project__c p 
                   Where Id=: projectId
                   Limit 1];  
                   
       User projectManager = [select u.Name
						  	  from User u
						  	  where u.Id =: project.Project_Manager__c 
						  	  limit 1];
						  
	   projectManagerName = projectManager.Name;            
    }
    
    private void getChangeRequestsByProjectId(){
      List<Change_Request__c> changeRequests =[Select cr.Name, cr.Change_Request__c,
                                                      cr.Request_Date__c, cr.Status__c,
                                                      cr.Priority__c
                                               From Change_Request__c cr 
                                               Where cr.Project__r.Id =: projectId] ;
                          
       for(Change_Request__c cr : changerequests){
            if(changerequestwrapper == null){
                changeRequestWrapper = new List<changeRequestWrapper>();
            }
            
            createWapperObjectAndAddToList(cr);
       }
    }
    
    private void createWapperObjectAndAddToList(Change_Request__c cr){
        ChangeRequestWrapper wrapper = new ChangeRequestWrapper(cr,true);
        Wrapper.IsChecked = true;
        changeRequestWrapper.add(wrapper);
    }
    
    private void getWorkItemsByProjectId(){
       List<Work_Item__c> workItems =[Select wi.Name, wi.Change_Request__c,wi.Id,
                                             wi.Description__c,wi.Assignee__r.Name,
                                             wi.Status_DEV__c, wi.Tester__r.Name,
                                             wi.Status_Test_DEV_Icon__c,
                                             wi.Status_Test_PRODUCTION_Icon__c,
                                             wi.Status_Test_STAGING_Icon__c,
                                             wi.Status_Test_DEV__c,wi.Status_Test_PRODUCTION__c,
                                             wi.Status_Test_STAGING__c
                                      From Work_Item__c wi 
                                      Where wi.Change_Request__r.Project__r.Id =: projectId];
		                         
        createWorkItemWrappersAndAddToList(workItems);
    }
    
    private void getReleaseDeploymentComponents(){  
        releaseComponents.clear();   
		
		List<Id> workItemIds=new List<Id>();
		for(WorkItemWrapper wi : workItemWrapper){
			workItemIds.add(wi.workItem.Id);
		}
		           
        releaseComponents.addAll(
                [Select rc.Work_Item__c,rc.Name,rc.Meta_data_type__c,
                        rc.Action_Type__c,rc.Object__c,
                        rc.Item_Name__c,rc.API_Field_name__c
                 From Release_Deployment_Component__c rc
                 Where rc.Work_Item__c in: workItemIds]);
        
        releaseComponents.sort();              
    }
    
    public PageReference onWorkItemCheckChanged(){
        Set<Id> RcdId = getIdsForCheckedWorkItems();
        
        updateReleaseComponentsForNewWorkItems(RcdId);
        return null;
    }
    
    private Set<Id> getIdsForCheckedWorkItems(){
        Set<Id> RcdId = new Set<Id>();           
        for(WorkItemWrapper wir : workItemWrapper){
            if(wir.IsChecked){
                RcdId.add(wir.WorkItem.Id);
            }
        }
        
        return RcdId;
    }
    
    private void updateReleaseComponentsForNewWorkItems(Set<Id> RcdId){
        releaseComponents.clear();   
           
        releaseComponents.addAll(
                [Select rc.Work_Item__c,rc.Name,rc.Meta_data_type__c,
                        rc.Action_Type__c,rc.Object__c,
                        rc.Item_Name__c,rc.API_Field_name__c
                 From Release_Deployment_Component__c rc
                 Where rc.Work_Item__c In: RcdId]);   
        releaseComponents.sort();     
    }
    
    public PageReference onCheckChanged(){
        Set<Id> RcdId = getIdsForCheckedChangeRequests();
            
        updateWorkItemsForNewChangeRequests(RcdId);                      
        getReleaseDeploymentComponents();
        
        return null;
    }
    
    private Set<Id> getIdsForCheckedChangeRequests(){
        Set<Id> RcdId = new Set<Id>();           
        for(ChangeRequestWrapper crw : changeRequestWrapper){
            if(crw.IsChecked){
                RcdId.add(crw.ChangeRequest.Id);
            }
        }
        
        return RcdId;
    }
    
    private void updateWorkItemsForNewChangeRequests(Set<Id> RcdId){
        List<work_Item__c> workItems = [Select wi.Name, wi.Change_Request__c,wi.Id,
                                               wi.Description__c,wi.Assignee__r.Name,
                                               wi.Status_DEV__c, wi.Tester__r.Name,
                                               wi.Status_Test_DEV_Icon__c,
                                               wi.Status_Test_PRODUCTION_Icon__c,
                                               wi.Status_Test_STAGING_Icon__c
                                        From Work_Item__c wi   
                                        Where wi.Change_Request__c In: RcdId];
                                        
        createWorkItemWrappersAndAddToList(workItems);                                       
    }
    
    private void createWorkItemWrappersAndAddToList(List<Work_Item__c> workItems){
        if(workItemWrapper == null){
            workItemWrapper = new List<WorkItemWrapper>();
        }
        else{
            workItemWrapper.clear();
        }
        
        for(Work_Item__c wi : workItems){
            WorkItemWrapper wir = new WorkItemWrapper(wi,true);
            wir.IsChecked = true;
            
            workItemWrapper.add(wir);
        }
    }
    
    public PageReference toggleFilters() {
        if(showFilters == null){
            showFilters = false;
        }
        
        showFilters= !showFilters;
        
        if(showFilters){
            filters = InitializeFilters();
        }
        
        return null;
    }
    
    private List<Filter> InitializeFilters(){
        List<Filter> filtersToReturn = new List<Filter>();
        
        filtersToReturn.add(new Filter(getWorkItemFields(),getOperatorFields(),0,getWorkItemFields()[0].getValue()));
        return filtersToReturn;
    }
    
    private List<SelectOption> getWorkItemFields(){
        List<SelectOption> returnList = new List<SelectOption>();
        
        Map<String, Schema.SObjectField> objectFields = Schema.SObjectType.Work_Item__c.fields.getMap();
        List<string> objectFieldKeySets = new List<string>();
        objectFieldKeySets.addAll(objectFields.keySet());
        objectFieldKeySets.sort();
        
        for(String fieldValue : objectFieldKeySets){
            returnList.add(new SelectOption(fieldValue,fieldValue));
        }
        
        return returnList;
    }
    
    private List<SelectOption> getOperatorFields(){
        List<SelectOption> returnList = new List<SelectOption>();
        
        returnList.add(new SelectOption('Equals','Equals'));
        returnList.add(new SelectOption('Less than','Less than'));
        returnList.add(new SelectOption('Greater than','Greater than'));
        returnList.add(new SelectOption('Contains','Contains'));
                
        return returnList;
    }
    
    public PageReference removeFilter(){
        filters.remove(FilterId);
        
        if(filters.isEmpty()){
            filters = InitializeFilters();
            
            getWorkItemsByProjectId();
            getReleaseDeploymentComponents();
        }
        
        return null;
    }
    
    public PageReference addFilter(){        
        filters.add(new Filter(getWorkItemFields(),getOperatorFields(),filters.size(),getWorkItemFields()[0].getValue()));
 
        return null;
    }
    
    public PageReference applyFilters(){                
       ErrorWrapper errorWrapper =createQueryStringAndExecute();
       if(errorWrapper.hasError){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'An error occured while performing the requested query'));
            return null;
       }
        
        Set<Id> recIds = getIdsFromNewWorkItems(errorWrapper.workItems);   
        
        createWorkItemWrappersAndAddToList(errorWrapper.workItems);
        updateReleaseComponentsForNewWorkItems(recIds);
        
        return null;
    } 
    
    private Set<Id> getIdsFromNewWorkItems(List<Work_Item__c> workItems){
        Set<Id> recIds = new Set<Id>();        
        for(Work_Item__c wi : workItems){
            recIds.add(wi.Id);
        }
        
        return recIds;
    }
    
    private ErrorWrapper createQueryStringAndExecute(){
        string query ='SELECT Name, Change_Request__c,Id, Description__c,' +
                              'Assignee__r.Name,Status_DEV__c, Tester__r.Name,'+
                              'Status_Test_DEV_Icon__c,Status_Test_PRODUCTION_Icon__c,'+
                              'Status_Test_STAGING_Icon__c FROM Work_Item__c';
        
        Boolean isFirstFilter = true;
        for(Filter filter : filters){
            String valueToFilter = filter.Value+'%';
            
            if(isFirstFilter){
                query = query + ' WHERE ' + filter.SelectedObjectField +getOperator(filter.SelectedOperator)+'\'%'+valueToFilter+'%\'';
                isFirstFilter = false;
                continue;
            }           
            query = query + ' AND ' + filter.SelectedObjectField +getOperator(filter.SelectedOperator)+'\'%'+valueToFilter+'%\'';
        }
        
        List<Work_Item__C> results = new List<Work_Item__C>();
        
        try{
            results =  (List<Work_Item__c>) Database.query(query);
        }catch(System.QueryException ex){
            return new ErrorWrapper(true,results);
        }
        
        return new ErrorWrapper(false,results);
    }
    
    private string getOperator(string selectedOperator){
        if(selectedOperator.equals('Equals')){
            return ' = ';
        }
        
        if(selectedOperator.equals('Less than')){
            return ' < ';
        }
        
        if(selectedOperator.equals('Greater than')){
            return ' > ';
        }
        
        if(selectedOperator.equals('Contains')){
            return ' Like ';
        }
        
        return '';
    }
    
    public PageReference getDesiredURL(){
        List<ReleaseDeploymentComponentWrapper> rdcw = new List<ReleaseDeploymentComponentWrapper>();
        for(Release_Deployment_Component__c rdc: releaseComponents){
            rdcw.add(new ReleaseDeploymentComponentWrapper(rdc));
        }
        
        rdcw.sort();
        xmlResult = bl_XmlGenerator.generatePackageXml(rdcw);
        
        pageReference pr =new PageReference('/apex/PackgeDownload'); 
        
        //pr.getParameters().put('xml',result);
        
        return pr;
    }
    
    public string xmlResult{get;set;}
    
    public PageReference save() {
        return null;
    }
    
    public List<ChangeRequestWrapper> getChangeRequestWrapper(){
        return changeRequestWrapper;
    }
    
    public Project__c getProject(){
        return project;
    }  
    
    public string getProjectManagerName(){
    	return projectManagerName;
    }
    
    public List<WorkItemWrapper> getWorkItemWrapper(){
        return workItemWrapper;
    }
    
    public List<Release_Deployment_Component__c> getReleaseComponents(){
        return  releaseComponents;
    } 
    
    public Boolean getShowFilters(){
        return showFilters;
    }
    
    public List<Filter> getFilters(){
        return filters;
    }
    
    public string getDevNeedle(){
        return getDevNeedleValue();
    }
    
    public string getStagingNeedle(){
        return getStagingNeedleValue();
    }
    
    public string getProdNeedle(){
        return getProdNeedleValue();
    }
    
    private string getDevNeedleValue(){
         double itemsCompletedInDev = getNumberOfCompletedDevWorkItems();
         
         if(workItemWrapper.size() == 0){
        	return '0' 	;
         }
         
         return string.valueOf((itemsCompletedInDev/workItemWrapper.size())*100);
    }
    
    private string getStagingNeedleValue(){
         double itemsCompletedInStag = getNumberOfCompletedStagingWorkItems();
         
         if(workItemWrapper.size() == 0){
        	return '0' 	;
         }
         
         return string.valueOf((itemsCompletedInStag/workItemWrapper.size())*100);
    }
    
    private string getProdNeedleValue(){
         double itemsCompletedInProd = getNumberOfCompletedProductionWorkItems();
         
         if(workItemWrapper.size() == 0){
        	return '0' 	;
         }
                 
         return string.valueOf((itemsCompletedInProd/workItemWrapper.size())*100);
    }
    
    private double getNumberOfCompletedProductionWorkItems(){
         double itemsCompletedInProd = 0;         
         for(WorkItemWrapper wi : workItemWrapper){
            if(wi.WorkItem.Status_Test_PRODUCTION__c == passed){
                itemsCompletedInProd += 1;
            }
         }
         
         return itemsCompletedInProd;
    }
    
    private double getNumberOfCompletedStagingWorkItems(){
        double itemsCompletedInStag = 0;         
         for(WorkItemWrapper wi : workItemWrapper){
            if(wi.WorkItem.Status_Test_STAGING__c == passed){
                itemsCompletedInStag += 1;
            }
         }
         
         return itemsCompletedInStag;
    }
    
    private double getNumberOfCompletedDevWorkItems(){
        double itemsCompletedInDev = 0;  System.debug('wi wrapper '+workItemWrapper);       
         for(WorkItemWrapper wi : workItemWrapper){
            if(wi.WorkItem.Status_Test_DEV__c == passed){
                itemsCompletedInDev += 1;
            }
         }
         
         return itemsCompletedInDev;
    }  
    
    public string getPlotBand1EndForDev(){   
    	if(project.End_Date__c == null){
    		return '10';
    	}
    	
        integer daysLeft = Date.today().daysBetween(project.End_Date__c.addDays(-5));
        
        if(daysLeft < 8){
            return '85';
        }
        else{
            return '10';
        }                   
    }
    
    public string getPlotBand1EndForStaging(){  
    	if(project.End_Date__c == null){
    		return '10';
    	}
    	 
        integer daysLeft = Date.today().daysBetween(project.End_Date__c.addDays(-3));
        
        if(daysLeft < 8){
            return '85';
        }
        else{
            return '10';
        }                   
    }
    
    public string getPlotBand1EndForProd(){   
    	if(project.End_Date__c == null){
    		return '10';
    	}
    	
        integer daysLeft = Date.today().daysBetween(project.End_Date__c);
        
        if(daysLeft < 8){
            return '85';
        }
        else{
            return '10';
        }                   
    }
    
    public string getPlotBand2EndForDev(){   
    	if(project.End_Date__c == null){
    		return '85';
    	}
    	
        integer daysLeft = Date.today().daysBetween(project.End_Date__c.addDays(-5));
        
        if(daysLeft < 8){
            return '95';
        }
        else{
            return '85';
        }                   
    }
    
    public string getPlotBand2EndForStaging(){   
    	if(project.End_Date__c == null){
    		return '85';
    	}
    	
        integer daysLeft = Date.today().daysBetween(project.End_Date__c.addDays(-3));
        
        if(daysLeft < 8){
            return '95';
        }
        else{
            return '85';
        }                   
    }
    
    public string getPlotBand2EndForProd(){ 
    	if(project.End_Date__c == null){
    		return '85';
    	}
    	  
        integer daysLeft = Date.today().daysBetween(project.End_Date__c);
        
        if(daysLeft < 8){
            return '95';
        }
        else{
            return '85';
        }                   
    }
    
    public string WorkItemId {get;set;}
    public string ChangeRequestId {get;set;}
    public integer FilterId{get;set;}

    public class ChangeRequestWrapper{
        public Change_Request__c ChangeRequest {get;set;}
        public Boolean IsChecked{get;set;}
        
        public ChangeRequestWrapper(Change_Request__c cr,Boolean boolValue){
            ChangeRequest = cr;
            IsChecked = boolValue;
        }
    }
}