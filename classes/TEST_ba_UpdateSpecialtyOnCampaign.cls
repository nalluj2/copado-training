@isTest private class TEST_ba_UpdateSpecialtyOnCampaign {

	@isTest private static void test_ba_UpdateSpecialtyOnCampaign() {

		//--------------------------------------------------------
		// Create Test Data
		//--------------------------------------------------------
		clsTestData_Campaign.idRecordType_Campaign = clsUtil.getRecordTypeByDevName('Campaign', 'Europe_xBU').Id;
		clsTestData_Campaign.iRecord_Campaign = 1;
		clsTestData_Campaign.createCampaign();
	
		clsTestData_Contact.iRecord_Contact = 5;
		List<Contact> lstContact = clsTestData_Contact.createContact(false);
		Set<String> setSpecialty = new Set<String>{'Administration','Anesthesiology','Audiologist','Bariatrics','Biomedics'};
		List<String> lstSpecialty = new List<String>();
			lstSpecialty.addAll(setSpecialty);
		Integer iCounter = 0;
		for (Contact oContact : lstContact){
			oContact.Contact_Primary_Specialty__c = lstSpecialty[iCounter];
			iCounter++;
		}
		insert clsTestData_Contact.lstContact;

		Map<Id, Set<String>> mapCampaignId_Specialty = new Map<Id, Set<String>>();

		clsTestData_Campaign.iRecord_CampaignMember = 5;
		clsTestData_Campaign.createCampaignMember();
		//--------------------------------------------------------


		//--------------------------------------------------------
		// Execute / Test Logic
		//--------------------------------------------------------
		Test.startTest();

			ba_UpdateSpecialtyOnCampaign oBatch1 = new ba_UpdateSpecialtyOnCampaign();
				oBatch1.tProcessType = 'COLLECTDATA';
			Database.executeBatch(oBatch1, 2000);


			mapCampaignId_Specialty.put(clsTestData_Campaign.oMain_Campaign.Id, new Set<String>());
			ba_UpdateSpecialtyOnCampaign oBatch2 = new ba_UpdateSpecialtyOnCampaign();
				oBatch2.mapCampaignId_Specialty = mapCampaignId_Specialty;
				oBatch2.tProcessType = 'UPDATEDATA';
			Database.executeBatch(oBatch2, 2000);


			mapCampaignId_Specialty.put(clsTestData_Campaign.oMain_Campaign.Id, setSpecialty);
			ba_UpdateSpecialtyOnCampaign oBatch3 = new ba_UpdateSpecialtyOnCampaign();
				oBatch3.mapCampaignId_Specialty = mapCampaignId_Specialty;
				oBatch3.tProcessType = 'UPDATEDATA';
			Database.executeBatch(oBatch3, 2000);

		Test.stopTest();
		//--------------------------------------------------------


		//--------------------------------------------------------
		// Validate Result
		//--------------------------------------------------------
		Campaign oCampaign = [SELECT Id, Recipient_s_Specialties__c FROM Campaign WHERE Id = :clsTestData_Campaign.oMain_Campaign.Id];
		String tSpecialty_All = oCampaign.Recipient_s_Specialties__c;
		List<String> lstSpecialty_All = tSpecialty_All.split(';');
		System.assertEquals(lstSpecialty_All.size(), setSpecialty.size());
		for (String tSpecialty : lstSpecialty_All){
			System.assert(setSpecialty.contains(tSpecialty));
		}
		//--------------------------------------------------------

	}

	@isTest static void test_scheduling() {

        Test.startTest();

        //---------------------------------------
        // TEST SCHEDULING
        //---------------------------------------
        string tCRON_EXP = '0 0 0 3 9 ? 2099';
        
        string tJobId = System.schedule('ba_UpdateSpecialtyOnCampaign_Test', tCRON_EXP, new ba_UpdateSpecialtyOnCampaign());

        // Get the information from the CronTrigger API object
        CronTrigger oCronTrigger = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :tJobId];

        // Verify the expressions are the same
        System.assertEquals(tCRON_EXP, oCronTrigger.CronExpression);

        // Verify the job has not run
        System.assertEquals(0, oCronTrigger.TimesTriggered);

        // Verify the next time the job will run
        System.assertEquals('2099-09-03 00:00:00', String.valueOf(oCronTrigger.NextFireTime));
        //---------------------------------------

        Test.stopTest();

	}

}