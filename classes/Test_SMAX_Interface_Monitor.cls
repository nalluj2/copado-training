@isTest
private class Test_SMAX_Interface_Monitor {
    
    @testSetup
    private static void generateTestData(){
    	
    	SVMXC__Service_Order__c sOrder = new SVMXC__Service_Order__c();
    	sOrder.SVMX_SAP_Service_Order_No__c = '99999999999';
    	insert sOrder;
    	
    	generateNotifications('SVMXC__Service_Order__c', 'ServiceOrder_NotificationSAP', sOrder.Id, '99999999999');
    	
    	Case sNotification = new Case();
    	sNotification.SVMX_SAP_Notification_Number__c = '999999999999';
    	insert sNotification;
    	
    	generateNotifications('Case', 'Case_NotificationSAP', sNotification.Id, '999999999999');
    	
    	Attachment att = new Attachment();
    	att.ParentId = sNotification.Id;
    	att.Name = 'Test Attachment';
    	att.body = Blob.valueOf('test body');
    	insert att;
    	
    	generateNotifications('Attachment', 'Attachment_NotificationSAP', att.Id, '');
    	
    	SVMXC__Installed_Product__c instProd = new SVMXC__Installed_Product__c();
    	instProd.SVMX_SAP_Equipment_ID__c = '999999999999999999';
    	insert instProd;
    	
    	generateNotifications('SVMXC__Installed_Product__c', 'InstalledProduct_NotificationSAP', instProd.Id, '999999999999999999');
    	
    	Installed_Product_Measures__c prodMeasure = new Installed_Product_Measures__c();
    	prodMeasure.Installed_Product__c = instProd.Id;
    	prodMeasure.Active__c = true;
    	prodMeasure.Description__c = 'Unit Test Measurement Value';
    	prodMeasure.Last_Recorded_Timestamp__c = Datetime.now().addDays(-15);
    	prodMeasure.Last_Recorded_Value__c = '20';
    	prodMeasure.New_Recorded_Value__c = '35';
    	prodMeasure.SAP_Measure_Point_ID__c = '11111111';	
		insert prodMeasure;
		
		generateNotifications('Installed_Product_Measures__c', 'InstProductCounter_NotificationSAP', prodMeasure.Id, '11111111');
    }
    	
    private static void generateNotifications(String objectType, String wsName, Id recordId, String recordSAPId){
    	
       	NotificationSAPLog__c notif = new NotificationSAPLog__c();
    	notif.Record_ID__c = recordId;   
    	notif.Record_SAPID__c = recordSAPId;    	
    	notif.Status__c = 'FAILED';
    	notif.WM_Process__c = 'Process';
    	notif.WebServiceName__c = wsName;
    	notif.Retries__c = 0;
    	notif.SFDC_Object_Name__c = objectType;
    	notif.Outbound_Message_Request__c = '{"SFDC_ID" : "500w000001DXKVyAAP","SAP_ID" : "000300559060","MESSAGE" : null,"DISTRIBUTION_STATUS" : "FALSE","DIST_ERROR_DETAILS" : [ {"ERROR_TYPE" : "E","ERROR_NUMBER" : "264","ERROR_MESSAGE" : "New entry not possible, since notification already completed","ERROR_ID" : "IM"}, {"ERROR_TYPE" : "S","ERROR_NUMBER" : "349","ERROR_MESSAGE" : "Notification long text updated successfully!","ERROR_ID" : "ZSM"}, {"ERROR_TYPE" : "S","ERROR_NUMBER" : "351","ERROR_MESSAGE" : "Notification custom field updated successfully!","ERROR_ID" : "ZSM"} ]}';
    	insert notif; 
    	
    	Outbound_Message__c message = new Outbound_Message__c();
    	message.User__c = UserInfo.getUserId();    	   	
    	message.Operation__c = 'getUpdate';
    	message.Request__c = 'Test Request';
    	message.Response__c = 'Test Response';
    	message.Internal_Id__c = recordId;
    	insert message;
    	
    }
    
    private static testMethod void testInterfaceMonitor(){
    	
    	PageReference pageRef = Page.SMAX_Interface_Monitor;    	
		Test.setCurrentPage(pageRef);
    	
    	ctrl_SMAX_Interface_Monitor ctrl = new ctrl_SMAX_Interface_Monitor();
    	
    	System.assert(ctrl.selectedObjectType == 'SVMXC__Service_Order__c');
    	System.assert(ctrl.selectedFilterOption == 'All');
    	
    	pageRef.getParameters().put('objectType', 'SVMXC__Service_Order__c');
    	pageRef.getParameters().put('filterOption', 'Internal_ID__c');
    	pageRef.getParameters().put('filterId', [Select Id from SVMXC__Service_Order__c].Id);
    	
    	ctrl = new ctrl_SMAX_Interface_Monitor();    	
    	
    	System.assert(ctrl.totalItems == 1);
    	
    	String response = ctrl_SMAX_Interface_Monitor.retryItem(ctrl.reportItems[0].notifications[0].Id);    	
    	System.assert(response == 'Success');
    	    	
    	ctrl.selectedObjectType = 'Case';
    	ctrl.selectedFilterOption = 'Internal_ID__c';
    	ctrl.filterId = [Select Id from Case].Id;
    	ctrl.loadReport();
    	
    	response = ctrl_SMAX_Interface_Monitor.retryItem(ctrl.reportItems[0].notifications[0].Id);    	
    	System.assert(response == 'Success');
    	
    	ctrl.selectedFilterOption = 'All';
    	ctrl.filterId = null;
    	ctrl.selectedObjectType = 'Attachment';
    	ctrl.loadReport();
    	
    	response = ctrl_SMAX_Interface_Monitor.retryItem(ctrl.reportItems[0].notifications[0].Id);    	
    	System.assert(response == 'Success');
    	
    	ctrl.selectedObjectType = 'SVMXC__Installed_Product__c';
    	ctrl.loadReport(); 
    	
    	response = ctrl_SMAX_Interface_Monitor.retryItem(ctrl.reportItems[0].notifications[0].Id);    	
    	System.assert(response == 'Success'); 
    	
    	ctrl.selectedObjectType = 'Installed_Product_Measures__c';
    	ctrl.loadReport(); 
    	
    	response = ctrl_SMAX_Interface_Monitor.retryItem(ctrl.reportItems[0].notifications[0].Id);    	
    	System.assert(response == 'Success');   	
    }
    
    private static testMethod void testNotificationMonitor(){
    	
    	PageReference pageRef = Page.SMAX_Notification_Monitor;    	
		Test.setCurrentPage(pageRef);
		
		SVMXC__Service_Order__c sOrder = [Select id from SVMXC__Service_Order__c];
		
		pageRef.getParameters().put('id',sOrder.Id);
    	
    	ctrl_SMAX_Notification_Monitor ctrl = new ctrl_SMAX_Notification_Monitor();
    	
    	System.assert(ctrl.outboundMessages.size() == 1);
    }
    
    private static testMethod void testInterfaceReport(){
    	
    	PageReference pageRef = Page.SMAX_Interface_Report;    	
		Test.setCurrentPage(pageRef);
    	
    	pageRef.getParameters().put('objectType', 'SVMXC__Service_Order__c');
    	
    	ctrl_SMAX_Interface_Report ctrl = new ctrl_SMAX_Interface_Report();
    	
    	System.assert(ctrl.reportItems.size() == 1);
    	
    	pageRef.getParameters().put('objectType', 'Case');
    	
    	ctrl = new ctrl_SMAX_Interface_Report();
    	
    	pageRef.getParameters().put('objectType', 'Attachment');
    	
    	ctrl = new ctrl_SMAX_Interface_Report();
    	
    	pageRef.getParameters().put('objectType', 'SVMXC__Installed_Product__c');
    	
    	ctrl = new ctrl_SMAX_Interface_Report();
    	
    	pageRef.getParameters().put('objectType', 'Installed_Product_Measures__c');
    	
    	ctrl = new ctrl_SMAX_Interface_Report();
    }
    
    private static testMethod void testInterfaceMonitor_Inbound(){
    	
    	Outbound_Message__c messageSO = new Outbound_Message__c();
    	messageSO.User__c = UserInfo.getUserId();
    	messageSO.Operation__c = 'upsertServiceOrder';
    	messageSO.Request__c = '{"MNWRKCNTR_SHORT_TEXT": "MDT NL"}';
    	messageSO.Response__c = '{"DIST_ERROR_DETAILS": "Test error message"}';
    	messageSO.Internal_Id__c = null;
    	messageSO.External_Id__c = '00000';
    	messageSO.Status__c = 'FALSE';
    	
    	Outbound_Message__c messageSN = new Outbound_Message__c();
    	messageSN.User__c = UserInfo.getUserId();
    	messageSN.Operation__c = 'upsertServiceNotification';
    	messageSN.Request__c = '{"MNWRKCNTR_SHORT_TEXT": "MDT NL"}';
    	messageSN.Response__c = '{"DIST_ERROR_DETAILS": "Test error message"}';
    	messageSN.Internal_Id__c = null;
    	messageSN.External_Id__c = '11111';
    	messageSN.Status__c = 'FALSE';
    	    	
    	insert new List<Outbound_Message__c>{messageSO, messageSN};
    	
    	Test.startTest();
    	
    	PageReference pageRef = Page.SMAX_Interface_Monitor_Inbound;    	
		Test.setCurrentPage(pageRef);
    	
    	ctrl_SMAX_Interface_Monitor_Inbound ctrl = new ctrl_SMAX_Interface_Monitor_Inbound();
    	ctrl.loadReport();
    	
    	System.assert(ctrl.totalItems == 1);
    	
    	ctrl.selectedObjectType = 'Case';    	
    	ctrl.filterId = '11111';
    	ctrl.loadReport();    
    	
    	System.assert(ctrl.totalItems == 1);
    }
    
    private static testMethod void testInterfaceReport_Inbound(){
    	
    	Outbound_Message__c messageSO = new Outbound_Message__c();
    	messageSO.User__c = UserInfo.getUserId();
    	messageSO.Operation__c = 'upsertServiceOrder';
    	messageSO.Request__c = '{"MNWRKCNTR_SHORT_TEXT": "MDT NL"}';
    	messageSO.Response__c = '{"DIST_ERROR_DETAILS": "Test error message"}';
    	messageSO.Internal_Id__c = null;
    	messageSO.External_Id__c = '00000';
    	messageSO.Status__c = 'FALSE';
    	    	    	
    	insert messageSO;
    	
    	Test.startTest();
    	
    	PageReference pageRef = Page.SMAX_Interface_Report_Inbound;    	
		Test.setCurrentPage(pageRef);
    	
    	pageRef.getParameters().put('scope', 'SVMXC__Service_Order__c');
    	    	
    	ctrl_SMAX_Interface_Report_Inbound ctrl = new ctrl_SMAX_Interface_Report_Inbound();
    	
    	System.assert(ctrl.reportItems.size() == 1);    	   	
    }
}