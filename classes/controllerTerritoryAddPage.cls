public with sharing class controllerTerritoryAddPage {

    	string terrId='';
    	Territory2 terr;
    	Territory2 newTerritory = new Territory2();
    public controllerTerritoryAddPage(ApexPages.StandardController controller){
        terrId=ApexPages.currentPage().getParameters().get('Id');
        terr = (Territory2)controller.getRecord();
        if(ChangeButtonPressed == true)
        {
            ChangeButtonPressed=false;
        }
    }
   	public Territory2 getNewTerritory(){
   		if (newTerritory == null) newTerritory = new Territory2();
   		return newTerritory;
   		
   	
   	}
     public boolean ChangeButtonPressed {        
         get { return ChangeButtonPressed; }        
         set { ChangeButtonPressed = value; }    
     }
              
                 

    // Get the detail information of the choosen Territory Node
    public Territory2 getTDetail(){
        Territory2 detail;
        if(ChangeButtonPressed != true)
        {    	
			if (terrId != null){
	            detail = [Select Id, Name,Business_Unit__c, Territory2Type.DeveloperName, Country_UID__c,Short_Description__c,Therapy_Groups_Text__c, ParentTerritory2Id  from Territory2 where Id=:terrId];
	            
	        }
	        else{
	            Id userId = UserInfo.getUserId();
	            String userTerrUID =[Select Territory_UID__c from User where id=:userId].Territory_UID__c;
	            detail = [Select Id, Name,Business_Unit__c, Territory2Type.DeveloperName, Country_UID__c,Short_Description__c,Therapy_Groups_Text__c, ParentTerritory2Id  from Territory2 where Territory_UID2__c =:userTerrUID];
	        }    
        }
        return detail;
    }
    public String getCrumbs(){
        String crumbs ;
        if(ChangeButtonPressed != true)
        {    	    	
	        Boolean levelFound = false;
	        crumbs ='';
	        Id userId = UserInfo.getUserId();
	        String userTerrUID =[Select Territory_UID__c from User where id=:userId].Territory_UID__c;
	        Territory2 currTerrId = [Select Id, Territory2Type.DeveloperName from Territory2 where Territory_UID2__c =:userTerrUID];
	        Territory2 allChildren = new Territory2();
			if (terrId != null){
				Id tempId = terrId;
	            do{
	                allChildren = [Select Id, Name,Business_Unit__c, Territory2Type.DeveloperName, Country_UID__c,Short_Description__c,Therapy_Groups_Text__c, ParentTerritory2Id  from Territory2 where Id=:tempId];
	                
	
	                crumbs = allChildren.Name + ' »' + crumbs;
	                tempId = allChildren.ParentTerritory2Id;
	
	                
	                } while (allChildren.Territory2Type.DeveloperName != 'Region');
	        
	            // Remove the last >>
	            integer crmbIndex = crumbs.lastIndexOf('»')  ;
	            if (crmbIndex !=-1){
	                crumbs = crumbs.substring(0,crmbIndex);
	            }          
	        }
        }
    return crumbs;        
    }
    public List<String> getTerrGroups()
    {
    	List<String> groups = new List<String>();
    	if(ChangeButtonPressed != true){    	    	
        String TGroups = [select Therapy_Groups_Text__c from Territory2 where id=:terrId].Therapy_Groups_Text__c;
        System.Debug('TGroups length - ' + TGroups);
        if (TGroups != null ){
            groups = TGroups.split(';');
        }
        else {
        groups.add('Empty list');
        }
        }
        return groups;      

    }
    public pageReference Change()
    {
    	String typeabbr;
    	String sfDescription;
    	ChangeButtonPressed=true;
    	Territory2 currRecord = [select ParentTerritory2Id, Territory2Type.DeveloperName, Country_UID__c, Business_Unit__c, Therapy_Groups_Text__c, Company__c  from Territory2 where id=:terrId];
    	String BUname = currRecord.Business_Unit__c;
    	String CMP = [Select Id from Company__c where id =:currRecord.Company__c].Id;
    	String CountryUID = currRecord.Country_UID__c;
    	String TherapyGRP = currRecord.Therapy_Groups_Text__c;
    	
    	newTerritory.Business_Unit__c = BUname;
    	newTerritory.Country_UID__c = CountryUID;
    	newTerritory.Therapy_Groups_Text__c = TherapyGRP;
    	newTerritory.ParentTerritory2Id = currRecord.Id;
    	newTerritory.Company__c = currRecord.Company__c;   
    	newTerritory.Territory2ModelId = [Select Id from Territory2Model where DeveloperName = 'Medtronic'].Id; 	
    	
    	String terrType = currRecord.Territory2Type.DeveloperName;
    	if (terrType == 'District'){
    		sfDescription = [select Short_Description__c from Territory2 where id=:currRecord.ParentTerritory2Id].Short_Description__c;
    		typeabbr = 'TE';
    		newTerritory.Territory2TypeId = [Select Id from Territory2Type where DeveloperNAme = 'Territory'].Id;
    	}
    	else{
    		sfDescription = [select Short_Description__c from Territory2 where id=:currRecord.Id].Short_Description__c;
    		typeabbr = 'DI';
    		newTerritory.Territory2TypeId = [Select Id from Territory2Type where DeveloperNAme = 'District'].Id;
    	}
        System.Debug('Business Unit Name - ' + BUname);
    	String abbrName =[select abbreviated_name__c from Business_Unit__c where Name=:BUname and Company__c =: CMP].abbreviated_name__c;
    	newTerritory.name = newTerritory.Short_Description__c + ' (' + typeabbr + ' ' + abbrName + ' ' + sfDescription + ' ' + currRecord.Country_UID__c + ')';
    	newTerritory.DeveloperName = newTerritory.Short_Description__c.replace(' ', '_') + '_' + typeabbr + '_' + abbrName + '_' + sfDescription + '_' + currRecord.Country_UID__c;

        Insert newTerritory;     
        System.Debug('Ter - ' + newTerritory);   
        return null;
    }
}