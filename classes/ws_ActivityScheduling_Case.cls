@RestResource(urlMapping='/ActivitySchedulingCaseService/*')
global with sharing class ws_ActivityScheduling_Case {
    
	@HttpPost
    global static void doUpsert() {
 		
		try{
			
			Case activitySchedulingCase = (Case) System.Json.deserialize(RestContext.request.requestBody.toString(), Case.class);
			
			bl_Case_Trigger.runningSchedulingApp = true;
			
			activitySchedulingCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Implant_Scheduling').getRecordTypeId();	
			upsert activitySchedulingCase;
			
			Set<String> attendeeKeys = new Set<String>();
			List<Activity_Scheduling_Attendee__c> attendees = new List<Activity_Scheduling_Attendee__c>();
						
			if(activitySchedulingCase.Activity_Scheduling_Attendees__r != null && activitySchedulingCase.Activity_Scheduling_Attendees__r.size() > 0){
								
				for(Activity_Scheduling_Attendee__c attendee : activitySchedulingCase.Activity_Scheduling_Attendees__r){
	    		
	    			if(attendee.Case__c == null) attendee.Case__c = activitySchedulingCase.Id;	 
	    			
	    			attendee.Unique_Key__c = attendee.Case__c + ':' + attendee.Attendee__c;
		    		attendee.Id = null;	   
		    		
		    		attendeeKeys.add(attendee.Unique_Key__c); 
		    		attendees.add(attendee);   			
	    		}	
			}
			
			delete [Select Id from Activity_Scheduling_Attendee__c where Case__c = :activitySchedulingCase.Id AND Unique_Key__c NOT IN :attendeeKeys];
			
			upsert attendees Unique_Key__c;
		
			bl_Case_Trigger.runningSchedulingApp = false;
			
			RestContext.response.responseBody = Blob.valueOf(JSON.serialize(activitySchedulingCase));
				
		}catch (Exception e){
						
			RestResponse res = RestContext.response;
			res.statusCode = 400;
			
			SalesforceError err = new SalesforceError();
			err.errorCode = 'BAD_REQUEST';
			err.message =  e.getMessage();
			
			RestContext.response.responseBody = Blob.valueOf(JSON.serialize(err));
		}   				
	}

	@HttpDelete
	global static void doDelete(){
		
		String toDeleteId = RestContext.request.params.get('id');
		
		try{
			
			Case activitySchedulingCaseToDelete = [select Id from Case where Id = :toDeleteId];
				
			bl_Case_Trigger.runningSchedulingApp = true;
				
			delete activitySchedulingCaseToDelete;
		
			bl_Case_Trigger.runningSchedulingApp = false;		
			
		}catch (Exception e){
						
			RestResponse res = RestContext.response;
			res.statusCode = 400;
			
			SalesforceError err = new SalesforceError();
			err.errorCode = 'BAD_REQUEST';
			err.message =  e.getMessage();
			
			RestContext.response.responseBody = Blob.valueOf(JSON.serialize(err));
		}			
	}
	
	global class SalesforceError {
    	
    	public String errorCode;
    	public String message;    	
	}
}