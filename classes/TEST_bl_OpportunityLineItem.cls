@isTest private class TEST_bl_OpportunityLineItem {
	
    @isTest static void test_setMobileID(){

        List<Opportunity> lstOpportunity = new List<Opportunity>();

        //------------------------------------------------
        // Create Test Data
        //------------------------------------------------
        // Create CVG Opportunity
        clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'CVG_Project').Id;
        lstOpportunity.addAll(clsTestData_Opportunity.createOpportunity(false));

        // Create RTG Opportunity
        List<String> lstRTG_RecordTypeDevName = new List<String>();
            lstRTG_RecordTypeDevName.add('Capital_Opportunity');
            lstRTG_RecordTypeDevName.add('Service_Contract_Opportunity');
            lstRTG_RecordTypeDevName.add('Business_Critical_Tender');
            lstRTG_RecordTypeDevName.add('Non_Capital_Opportunity');
    
        for (String tRTG_RecordTypeDevName : lstRTG_RecordTypeDevName){
            clsTestData_Opportunity.oMain_Opportunity = null;
            clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', tRTG_RecordTypeDevName).Id;
            lstOpportunity.addAll(clsTestData_Opportunity.createOpportunity(false));
        }
        insert lstOpportunity;

        clsTestData_Opportunity.lstOpportunity = lstOpportunity;
        // Create Opportunity Line Item
        clsTestData_Opportunity.iRecord_OpportunityLineItem = 5;
        clsTestData_Opportunity.createOpportunityLineItem(false);
        //------------------------------------------------

        //------------------------------------------------
        // Perform Tests
        //------------------------------------------------
        Test.startTest();

        insert clsTestData_Opportunity.lstOpportunityLineItem;

        Test.stopTest();
        //------------------------------------------------

        //------------------------------------------------
        // Validation
        //------------------------------------------------
        List<OpportunityLineItem> lstOpportunityLineItem = [SELECT Id, Mobile_ID__c FROM OpportunityLineItem];
        for (OpportunityLineItem oOpportunityLineItem : lstOpportunityLineItem){
            System.assert(!clsUtil.isBlank(oOpportunityLineItem.Mobile_ID__c));
        }
        //------------------------------------------------

    }

}