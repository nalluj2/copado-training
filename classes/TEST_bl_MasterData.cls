@isTest
private class TEST_bl_MasterData {
	
	@isTest static void test_getRegion_SBU_Country_JobTitle_GroupName() {

		// Create Test Dtaa
		clsTestData.createCompanyData();
		clsTestData.createSubBusinessUnitData();
		clsTestData.createCountryData();

		String tCountry1 = 'Belgium';
		String tCountry2 = 'Austria';
		String tJobTitle = 'Field Manager';

		DIB_Country__c oCountry_BE = clsTestData.mapCode_Country.get('BE');
		DIB_Country__c oCountry_AT = clsTestData.mapCode_Country.get('AT');

		List<Sub_Business_Units__c> lstSBU = [SELECT Id, Name FROM Sub_Business_Units__c WHERE Company_Text__c = 'Europe' LIMIT 2];

		String tPrimarySBU1 = lstSBU[0].Name;
		tPrimarySBU1 = tPrimarySBU1.replaceAll(' ', '_');
		String tPrimarySBU2 = lstSBU[1].Name;
		tPrimarySBU2 = tPrimarySBU2.replaceAll(' ', '_');

		String tGroupName1_1 = tCountry1 + '_ALL_' + tPrimarySBU1;
		String tGroupName1_2 = tCountry1 + '_ALL_' + tPrimarySBU2;
		String tGroupName2_1 = tCountry2 + '_ALL_' + tPrimarySBU1;
		String tGroupName2_2 = tCountry2 + '_ALL_' + tPrimarySBU2;

		// Create Queue_Mapping__c
		List<Queue_Mapping__c> lstQM = new List<Queue_Mapping__c>();
		Queue_Mapping__c oQM1_1 = new Queue_Mapping__c();
			oQM1_1.Sub_Business_Unit__c = lstSBU[0].Id;
			oQM1_1.Country__c = oCountry_BE.Id;
			oQM1_1.Job_Title__c = tJobTitle;
			oQM1_1.Queue_Name__c = tGroupName1_1;
		lstQM.add(oQM1_1);
		Queue_Mapping__c oQM1_2 = new Queue_Mapping__c();
			oQM1_2.Sub_Business_Unit__c = lstSBU[1].Id;
			oQM1_2.Country__c = oCountry_BE.Id;
			oQM1_2.Job_Title__c = tJobTitle;
			oQM1_2.Queue_Name__c = tGroupName1_2;
		lstQM.add(oQM1_2);

		Queue_Mapping__c oQM2_1 = new Queue_Mapping__c();
			oQM2_1.Sub_Business_Unit__c = lstSBU[0].Id;
			oQM2_1.Country__c = oCountry_AT.Id;
			oQM2_1.Job_Title__c = tJobTitle;
			oQM2_1.Queue_Name__c = tGroupName2_1;
		lstQM.add(oQM2_1);
		Queue_Mapping__c oQM2_2 = new Queue_Mapping__c();
			oQM2_2.Sub_Business_Unit__c = lstSBU[1].Id;
			oQM2_2.Country__c = oCountry_AT.Id;
			oQM2_2.Job_Title__c = tJobTitle;
			oQM2_2.Queue_Name__c = tGroupName2_2;
		lstQM.add(oQM2_2);
		insert lstQM;

		// TESTING
		Test.startTest();
		
		Map<String, Map<String, Map<String, Map<String, String>>>> mapData = bl_MasterData.getRegion_SBU_Country_JobTitle_GroupName();
		
		Test.stopTest();
	}
	
}