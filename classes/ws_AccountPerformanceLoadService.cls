/*
 *      Description : This class exposes methods to create a Account Performance Load record
 *
 *      Author = Rudy De Coninck
 */
@RestResource(urlMapping='/AccountPerformanceLoadService/*')
global class ws_AccountPerformanceLoadService{
@HttpPost
	global static String doPost() {
 		RestRequest req = RestContext.request;
			
		System.debug('post called '+req);
		System.debug('post requestBody '+req.requestBody);
			
		String body = req.requestBody.toString();
		IFAccountPerformanceLoadRequest accountPerformanceLoadRequest = (IFAccountPerformanceLoadRequest)System.Json.deserialize(body, IFAccountPerformanceLoadRequest.class);
			
		System.debug('post requestBody '+accountPerformanceLoadRequest);
			
		Account_Performance_Load__c accountPerformanceLoad = accountPerformanceLoadRequest.accountPerformanceLoad;

		IFAccountPerformanceLoadResponse resp = new IFAccountPerformanceLoadResponse();

		//We catch all exception to assure that 
		try{
				
			resp.accountPerformanceLoad = bl_AccountPerformanceLoad.saveAccountPerformanceLoad(accountPerformanceLoad);
			resp.id = accountPerformanceLoadRequest.id;
	
			resp.success = true;
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
		}
 
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'AccountPerformanceLoad' ,req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
  			
		return System.JSON.serialize(resp);		
	}

	
	global class IFAccountPerformanceLoadRequest{
		public Account_Performance_Load__c accountPerformanceLoad {get;set;}
		public String id{get;set;}
	}
	
	global class IFAccountPerformanceLoadResponse{
		public Account_Performance_Load__c accountPerformanceLoad {get;set;}
		public String id{get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}
	

}