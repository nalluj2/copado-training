public with sharing class bl_Contract_Repository_Interface {
        
    @Future(callout = true)
    public static void sendDocumentVersionNotification(Id attachmentId){
    	
    	Attachment att = [Select Id, ParentId from Attachment where ID = :attachmentId];
    	
    	sendNotification(att);
    }
    
    public static void sendNotification(Attachment att){
    	    	   	
    	Contract_Repository_Document_Version__c documentVersion = [Select Id, File_Name__c, Document__c from Contract_Repository_Document_Version__c where Id = :att.ParentId];
    	    	
    	Contract_Repository_Document__c document = [Select Id, Name, Document_Type__c, Contract_Repository__c, (Select Id, Version__c from Contract_Repository_Document_Versions__r where Marked_for_Deletion__c = false ORDER BY Version__c DESC LIMIT 2) from Contract_Repository_Document__c where Id = :documentVersion.Document__c];
    	
    	Contract_Repository__c contract = [Select Id, Account__r.Account_Country_vs__c, Contract_nr__c from Contract_Repository__c where Id = :document.Contract_Repository__c];
		
		//If the attachment to be send is not the last version for the document, then error
		if(document.Contract_Repository_Document_Versions__r[0].Id != documentVersion.Id){
			
			documentVersion.Interface_Error__c = 'The attachment file is not the last Version of the document';
			update documentVersion;
			
			return;			
		}
		
		Contract_Repository_Interface_Settings__c settings = Contract_Repository_Interface_Settings__c.getOrgDefaults();
		
		//If the attachment to be send is not the last version for the document, then error
		if(settings == null || Id.valueOf(settings.Org_Id__c) != UserInfo.getOrganizationId()){
			
			documentVersion.Interface_Error__c = 'No valid interface settings found';
			update documentVersion;
			
			return;			
		}
					
		try{
			
			Http service = new Http();
            
            HttpRequest request= new HttpRequest();
            request.setMethod('POST');
            request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
            request.setEndpoint(settings.End_Point_URL_Server__c + '/ws/MdtSalesforce_Contracts.pub.providers.ws:receiveContracts_WSDL/MdtSalesforce_Contracts_pub_providers_ws_receiveContracts_WSDL_Port');
            request.setTimeout(60000);
            request.setHeader('Authorization', 'BASIC ' + EncodingUtil.base64Encode(Blob.valueOf(settings.Username__c + ':' + settings.Password__c)));
            request.setBody(getMessageBody(att, documentVersion, document, contract));

            // Send the request
            HttpResponse resp = service.send(request);

            // Get the response of the Callout and process it
            System.debug(resp.getBody());
        
		}catch(Exception e){
			
			documentVersion.Interface_Error__c = e.getMessage() + ' | ' + e.getStackTraceString();
			update documentVersion;
		}		
    }
    
    private static String getMessageBody(Attachment attachment, Contract_Repository_Document_Version__c version, Contract_Repository_Document__c document, Contract_Repository__c contract){
    	
    	String body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:mdt="http://msplwa115-pr.wby1-dev.medtronic.com/MdtSalesforce_Contracts.pub.providers.ws:receiveContracts_WSDL">';
		body +=	'<soapenv:Header/>';
		body +=	'<soapenv:Body>';
		body +=	'<mdt:receiveContracts>';
		body +=	'<receiveContractsRequest>';
		
		String metadata = '{';
	   	metadata += '"title": "' + document.Name + '",';
	   	metadata += '"m_contract_no": "' + contract.Contract_nr__c + '",';
	   	metadata += '"object_name": "' + version.File_Name__c + '",';
	   	metadata += '"m_country": "' + contract.Account__r.Account_Country_vs__c + '",';
	   	metadata += '"m_document_type": "' + document.Document_Type__c+ '",';
	   	metadata += '"m_source": "SFDC",';
	   	metadata += '"m_source_sys_id": "' + document.Id + '",';
	   	metadata += '"m_source_sys_ver_id":"' + version.Id + '"';	   		   	
		metadata += '}';		
				            
		body +=	'<metadata>' + metadata + '</metadata>';		            
		body +=	'<File_content_type>Payload</File_content_type>';		            
		body +=	'<AttachmentID>' + attachment.Id + '</AttachmentID>';		            
		body +=	'<content_path/>';					            
		body +=	'<config_name>clm_sfdc</config_name>';					            
		body +=	'<type>m_clm_doc</type>';
		
		if(document.Contract_Repository_Document_Versions__r.size() == 2){
			
			body +=	'<id>' + document.Contract_Repository_Document_Versions__r[1].Id + '</id>';							            
			body +=	'<version_type>Major</version_type>';
			
		}else{
			
			body +=	'<id/>';					            
			body +=	'<version_type/>';
		}
							            
		body +=	'<userid/>';
		body +=	'</receiveContractsRequest>';
		body +=	'</mdt:receiveContracts>';
		body +=	'</soapenv:Body>';
		body +=	'</soapenv:Envelope>';
		
		System.debug(body);
		
		return body;    	
    }
}