//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 20-01-2016
//  Description      : APEX Test Class to test the logic in tr_SVMXC_ServiceOrder and bl_SVMXC_ServiceOrder_Trigger
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_tr_SVMXC_ServiceOrder {

    //----------------------------------------------------------------------------------------
    // CREATE TEST DATA
    //----------------------------------------------------------------------------------------
    @testSetup static void createTestData() {

        // Create Custom Setting Data
        clsTestData.createCustomSettingData(true);

        // Create Account Data
        clsTestData.iRecord_Account = 1;
        clsTestData.tCountry_Account = 'United Kingdom';
        clsTestData.createAccountData(false);
        	clsTestData.oMain_Account.SAP_Id__c = 'SAPA001';
        insert clsTestData.oMain_Account;

        // Create SVMXC__Service_Order__c Data
        clsTestData.createSVMXCServiceOrderData(false);
        	clsTestData.oMain_SVMXCServiceOrder.SVMX_SAP_Service_Order_No__c = 'T12345T';
        	clsTestData.oMain_SVMXCServiceOrder.SVMX_Send_Back_to_Service_Center__c = true;
        insert clsTestData.oMain_SVMXCServiceOrder;

        // Create SVMXC__Service_Order_Line__c Data
		clsTestData.createSVMXCServiceOrderLineData(true);

		// Create SVMXC__Site__c Data
		clsTestData.iRecord_SVMXCSite = 2;
		clsTestData.createSVMXCSite(true);

        // Create AdHoc related Data
        // Service Order
        clsTestData.oMain_SVMXCServiceOrder = null;
        clsTestData.createSVMXCServiceOrderData(false);
	        clsTestData.oMain_SVMXCServiceOrder.SVMXC__Purpose_of_Visit__c = 'Placeholder';
	        clsTestData.oMain_SVMXCServiceOrder.SVMX_SAP_Service_Order_No__c = null;
        insert clsTestData.oMain_SVMXCServiceOrder;
    }
    //----------------------------------------------------------------------------------------

	
    //----------------------------------------------------------------------------------------
    // Basic Trigger Testing
    //----------------------------------------------------------------------------------------
	@isTest static void test_tr_SVMXC_ServiceOrder() {
	
		// Select Data
		List<SVMXC__Service_Order__c> lstData = [SELECT Id, SVMXC__Order_Status__c FROM SVMXC__Service_Order__c];
		SVMXC__Service_Order__c oData = lstData[0];
		
		// Update Data
		update oData;

		// Delete Data
		delete oData;

		// Undelete Data
		Undelete oData;

	}
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
    // TEST Update on Service Order
    //----------------------------------------------------------------------------------------
	@isTest static void test_tr_SVMXC_ServiceOrder_Update() {

		// Select Data
		List<SVMXC__Service_Order__c> lstData = [SELECT Id, SVMXC__Order_Status__c FROM SVMXC__Service_Order__c];
		SVMXC__Service_Order__c oData = lstData[0];

		// UPDATE
		Test.startTest();

			oData.SVMX_Contract_Start_Date__c = Date.today();
		update oData;

		Test.stopTest();

	}
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
    // TEST Update Service Order to 'Job Completed'
    //----------------------------------------------------------------------------------------
	@isTest static void test_tr_SVMXC_ServiceOrder_Update_Completed() {
	
		// Select Data
		List<SVMXC__Service_Order__c> lstData = [SELECT Id, SVMXC__Order_Status__c FROM SVMXC__Service_Order__c];
		SVMXC__Service_Order__c oData = lstData[0];

		// UPDATE
		Test.startTest();

			oData.SVMXC__Order_Status__c = bl_SVMXC_ServiceOrder_Trigger.SERVICEORDER_COMPLETED;
		update oData;

		Test.stopTest();
		
		oData = [SELECT Id, Record_Locked__c FROM SVMXC__Service_Order__c where Id = :oData.Id];
		
		System.assert(oData.Record_Locked__c == true);

	}
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
    // TEST Update Completed Service Order AFTER we receive the Acknowledgement
    //----------------------------------------------------------------------------------------
	@isTest static void test_tr_SVMXC_ServiceOrder_Update_Completed_Acknowledgement() {
	
		// Select Data
		List<SVMXC__Service_Order__c> lstData = [SELECT Id, SVMXC__Order_Status__c FROM SVMXC__Service_Order__c];
		SVMXC__Service_Order__c oData = lstData[0];

		// UPDATE
		// UPDATE
		Test.startTest();

			oData.SVMXC__Order_Status__c = bl_SVMXC_ServiceOrder_Trigger.SERVICEORDER_COMPLETED;
			oData.Ready_To_Complete__c = true;
		update oData;


		Test.stopTest();

	} 
	//----------------------------------------------------------------------------------------		
	
	//----------------------------------------------------------------------------------------
    // TEST Update Service Order to 'On Hold' and resume
    //----------------------------------------------------------------------------------------
	@isTest static void test_tr_SVMXC_ServiceOrder_On_Hold_And_Resume() {
	
		// Select Data
		List<SVMXC__Service_Order__c> lstData = [SELECT Id, SVMXC__Order_Status__c FROM SVMXC__Service_Order__c];
		SVMXC__Service_Order__c oData = lstData[0];

		// UPDATE
		Test.startTest();

		oData.SVMXC__Order_Status__c = bl_SVMXC_ServiceOrder_Trigger.SERVICEORDER_ON_HOLD;
		oData.SVMX_On_Hold_Reason__c = 'Health and Safety';
		oData.SVMXC__Actual_Onsite_Response__c = Datetime.now().addMinutes(-30);
		oData.SVMX_Planned_Job_Duration__c = 2;
		oData.SVMX_SRA_Completed_Date__c = Datetime.now().addMinutes(-15);
		oData.SVMXC__Scheduled_Date_Time__c = Datetime.now().addHours(-1);
		update oData;
		
		oData = [SELECT Id, SVMX_On_Hold_Reason__c, SVMXC__Actual_Onsite_Response__c, SVMX_Planned_Job_Duration__c, SVMX_SRA_Completed_Date__c, SVMXC__Scheduled_Date_Time__c, SVMX_Pre_Hold_Order_Status__c FROM SVMXC__Service_Order__c where Id = :oData.Id];
		System.assert(oData.SVMX_On_Hold_Reason__c != null);
		System.assert(oData.SVMXC__Actual_Onsite_Response__c != null);
		System.assert(oData.SVMX_Planned_Job_Duration__c != null);
		System.assert(oData.SVMX_SRA_Completed_Date__c != null);
		System.assert(oData.SVMXC__Scheduled_Date_Time__c != null);
		
		oData.SVMXC__Order_Status__c = 'Ready to Plan';
		oData.SVMX_Pre_Hold_Order_Status__c = bl_SVMXC_ServiceOrder_Trigger.SERVICEORDER_ON_HOLD;
		update oData;
		
		Test.stopTest();
		
		oData = [SELECT Id, SVMX_On_Hold_Reason__c, SVMXC__Actual_Onsite_Response__c, SVMX_Planned_Job_Duration__c, SVMX_SRA_Completed_Date__c, SVMXC__Scheduled_Date_Time__c, SVMX_Pre_Hold_Order_Status__c FROM SVMXC__Service_Order__c where Id = :oData.Id];
		
		System.assert(oData.SVMX_On_Hold_Reason__c == null);
		System.assert(oData.SVMXC__Actual_Onsite_Response__c == null);
		System.assert(oData.SVMX_Planned_Job_Duration__c == null);
		System.assert(oData.SVMX_SRA_Completed_Date__c == null);
		System.assert(oData.SVMXC__Scheduled_Date_Time__c == null);
		System.assert(oData.SVMX_Pre_Hold_Order_Status__c == null);

	}
    //----------------------------------------------------------------------------------------
	

    //----------------------------------------------------------------------------------------
    // TEST Send Emails
    //----------------------------------------------------------------------------------------
	@isTest static void test_tr_SVMXC_ServiceOrder_SendEmail_AdHoc() {

		createInstProductData();

		SVMXC__Service_Order__c oServiceOrder_adHock = new SVMXC__Service_Order__c();

		// Select Data
		oServiceOrder_adHock = [SELECT Id, SVMX_SAP_Service_Order_No__c, SVMXC__Order_Status__c FROM SVMXC__Service_Order__c WHERE SVMX_SAP_Service_Order_No__c = null LIMIT 1];


		// UPDATE
		Test.startTest();

		// Add adHox related data
		// Fault Code
		List<SVMX_Fault_Code__c> lstFaultCode = new List<SVMX_Fault_Code__c>();
		SVMX_Fault_Code__c oFaultCode = new SVMX_Fault_Code__c();
			oFaultCode.SVMX_Work_Order__c = oServiceOrder_adHock.Id;
			oFaultCode.SVMX_Damage_Code_Text__c = 'Damage Text Goes Here';
		lstFaultCode.add(oFaultCode);
		insert lstFaultCode;
        // Create SVMXC__Service_Order_Line__c Data
        clsTestData.oMain_SVMXCServiceOrderLine = null;
        clsTestData.oMain_SVMXCServiceOrder = oServiceOrder_adHock;
		clsTestData.createSVMXCServiceOrderLineData(false);
			clsTestData.oMain_SVMXCServiceOrderLine.SVMXC__Activity_Type__c = 'Activity Type Goes Here';
		insert clsTestData.oMain_SVMXCServiceOrderLine;


			oServiceOrder_adHock.SVMXC__Component__c = [Select Id from SVMXC__Installed_Product__c].Id;
			oServiceOrder_adHock.SVMXC__Purpose_of_Visit__c = 'Not Placeholder';
		update oServiceOrder_adHock;

		Test.stopTest();

	}
/*
	@isTest static void test_tr_SVMXC_ServiceOrder_SendEmail_SparePart() {

		// Craete Product Data
		Product2 oProduct = new Product2();
	    	oProduct.IsActive = true; 
	    	oProduct.Name = 'Test Installed Product';
	    	oProduct.MPG_Code_Text__c = 'AA';
	    	oProduct.RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('SAP Product').getRecordTypeId();
	    	oProduct.ProductCode = '12345';
	    	oProduct.CFN_Code_Text__c = 'CFN Code';
		insert oProduct;

		SVMXC__Service_Order__c oServiceOrder = new SVMXC__Service_Order__c();

		// Select Data
		oServiceOrder = [SELECT Id, SVMX_SAP_Service_Order_No__c, SVMXC__Order_Status__c FROM SVMXC__Service_Order__c WHERE SVMX_SAP_Service_Order_No__c != null];

		List<SVMXC__Site__c> lstSite = [SELECT Id FROM SVMXC__Site__c];

		List<SVMXC__Service_Order_Line__c> lstServiceOrderLine = [SELECT Id, SVMXC__Consumed_From_Location__c FROM SVMXC__Service_Order_Line__c WHERE SVMXC__Service_Order__c = :oServiceOrder.Id];
		System.assert(lstServiceOrderLine.size() > 0);

		// UPDATE
		Test.startTest();

		for (SVMXC__Service_Order_Line__c oServiceOrderLine : lstServiceOrderLine){
			oServiceOrderLine.SVMXC__Consumed_From_Location__c = lstSite[0].Id;
			oServiceOrderLine.SVMXC__From_Location__c = lstSite[1].Id;
			oServiceOrderLine.SVMXC__Product__c = oProduct.Id;
			oServiceOrderLine.SVMXC__Line_Type__c = 'Parts';
			oServiceOrderLine.SVMXC__Expense_Type__c = 'Not Miscellaneous';
			oServiceOrderLine.RecordTypeId = clsUtil.getRecordTypeByDevName('SVMXC__Service_Order_Line__c', 'UsageConsumption').Id;
		}
		update lstServiceOrderLine;

			oServiceOrder.SVMXC__Order_Status__c = bl_SVMXC_ServiceOrder_Trigger.SERVICEORDER_COMPLETED;
			oServiceOrder.SVMX_SAP_Service_Order_No__c = '';
		update oServiceOrder;

		Test.stopTest();

	}
*/
	@isTest static void test_tr_SVMXC_ServiceOrder_SendEmail_ServiceCenter() {

		SVMXC__Service_Order__c oServiceOrder = new SVMXC__Service_Order__c();

		// Select Data
		oServiceOrder = [SELECT Id, SVMX_SAP_Service_Order_No__c, SVMXC__Order_Status__c FROM SVMXC__Service_Order__c WHERE SVMX_SAP_Service_Order_No__c != null];

		List<SVMXC__Site__c> lstSite = [SELECT Id FROM SVMXC__Site__c];

		List<SVMXC__Service_Order_Line__c> lstServiceOrderLine = [SELECT Id, SVMXC__Consumed_From_Location__c FROM SVMXC__Service_Order_Line__c];
		for (SVMXC__Service_Order_Line__c oServiceOrderLine : lstServiceOrderLine){
			oServiceOrderLine.SVMXC__Consumed_From_Location__c = lstSite[0].Id;
		}
		update lstServiceOrderLine;


		// UPDATE
		Test.startTest();

			oServiceOrder.SVMXC__Order_Status__c = bl_SVMXC_ServiceOrder_Trigger.SERVICEORDER_COMPLETED;
			oServiceOrder.SVMX_SAP_Service_Order_No__c = '';
			oServiceOrder.SVMX_Send_Back_to_Service_Center__c = TRUE;
		update oServiceOrder;

		Test.stopTest();

	}
    //----------------------------------------------------------------------------------------		


    //----------------------------------------------------------------------------------------
    // TEST Copy Account Country Logic
    //----------------------------------------------------------------------------------------
	@isTest static void test_tr_SVMXC_ServiceOrder_CopyAccountCountry() {

		// SELECT Data
		Account oAccount = [SELECT Id FROM Account LIMIT 1];

		Test.startTest();

			// INSERT
			clsTestData.createSVMXCServiceOrderData(false);
				clsTestData.oMain_SVMXCServiceOrder.SVMX_SAP_Service_Order_No__c = 'TT54321TT';
				clsTestData.oMain_SVMXCServiceOrder.SVMXC__Company__c = oAccount.Id;
				clsTestData.oMain_SVMXCServiceOrder.SVMXC__Order_Status__c = 'Scheduled';
			insert clsTestData.oMain_SVMXCServiceOrder;

			// UPDATE
			update clsTestData.oMain_SVMXCServiceOrder;

		Test.stopTest();

	}
    //----------------------------------------------------------------------------------------		


    //----------------------------------------------------------------------------------------
    // TEST Copy Account Country Logic
    //----------------------------------------------------------------------------------------
	@isTest static void test_tr_SVMXC_ServiceOrder_ManageWorkOrderEvent() {

		// CREATE Data
//    	clsTestData.createSVMXCServiceOrderData(false);
//    		clsTestData.oMain_SVMXCServiceOrder.SVMXC__Scheduled_Date_Time__c = Datetime.now();

		// SELECT DATA
    	SVMXC__Service_Order__c oServiceOrder = [SELECT Id, SVMXC__Scheduled_Date_Time__c FROM SVMXC__Service_Order__c LIMIT 1];

		Test.startTest();

			// UPDATE
			oServiceOrder.SVMXC__Scheduled_Date_Time__c = Datetime.now().addDays(2);
			update oServiceOrder;

			// INSERT
//			insert clsTestData.oMain_SVMXCServiceOrder;

			// UPDATE
//				clsTestData.oMain_SVMXCServiceOrder.SVMXC__Scheduled_Date_Time__c = Datetime.now().addDays(1);
//			update clsTestData.oMain_SVMXCServiceOrder;

		Test.stopTest();

	}
    //----------------------------------------------------------------------------------------		


    //----------------------------------------------------------------------------------------
    // Test Prevent Update on Restricted Fields
    //----------------------------------------------------------------------------------------
	@isTest static void test_preventUpdateOnRestrictedFields_System() {

		// Select Data
		List<SVMXC__Service_Order__c> lstData = [SELECT Id, SVMX_SAP_Service_Order_No__c FROM SVMXC__Service_Order__c WHERE SVMX_SAP_Service_Order_No__c = 'T12345T'];
		SVMXC__Service_Order__c oData = lstData[0];

		String tSAPServiceOrderNo = oData.SVMX_SAP_Service_Order_No__c;
		System.assertNotEquals(clsUtil.isNull(tSAPServiceOrderNo, ''), 'T1E1S1T1');

		// UPDATE
		Test.startTest();

			oData.SVMX_SAP_Service_Order_No__c = 'T1E1S1T1';
		update oData;

		Test.stopTest();

		// Validate that the update has been performed
		lstData = [SELECT Id, SVMX_SAP_Service_Order_No__c FROM SVMXC__Service_Order__c WHERE Id = :oData.Id];
		oData = lstData[0];
		System.assertEquals(oData.SVMX_SAP_Service_Order_No__c, 'T1E1S1T1');

	}

	@isTest static void test_preventUpdateOnRestrictedFields_NotSystem() {

		// Get the User Profile and Role
		Profile oProfile = [SELECT Id FROM Profile WHERE Name = 'EUR Field Service Engineer' LIMIT 1];
		UserRole oUserRole = [SELECT Id FROM UserRole WHERE DeveloperName = 'EUR_Service_Repair' LIMIT 1];

		// Select an Active User based on the collected Profile and Role
		//	If there is not Active User available, create one
		List<User> lstUser = [SELECT Id FROM User WHERE IsActive = true AND ProfileId = :oProfile.Id AND UserRoleId = :oUserRole.Id LIMIT 1];
		User oUser;
		if (lstUser.size() == 1){
			oUser = lstUser[0];
		}else{
			clsTestData.createUserData(clsUtil.getTimeStamp() + '@test.medtronic.com', 'United Kingdom', oProfile.Id, oUserRole.Id, true);
			oUser = clsTestData.oMain_User;
		}


		List<SVMXC__Service_Order__c> lstData = [SELECT Id, SVMX_SAP_Service_Order_No__c FROM SVMXC__Service_Order__c WHERE SVMX_SAP_Service_Order_No__c = 'T12345T'];
		SVMXC__Service_Order__c oData = lstData[0];
		oData.OwnerId = oUser.Id;
		update oData;
		
		// UPDATE
		Test.startTest();
			System.runAs(oUser){

				System.assertEquals(clsUtil.isNull(oData.SVMX_SAP_Service_Order_No__c, ''), 'T12345T');

					oData.SVMX_SAP_Service_Order_No__c = 'T1E1S1T1';
				update oData;
			}
		Test.stopTest();

		// Validate that the update has NOT been performed
		lstData = [SELECT Id, SVMX_SAP_Service_Order_No__c FROM SVMXC__Service_Order__c WHERE Id = :oData.Id];
		oData = lstData[0];
		System.assertEquals(clsUtil.isNull(oData.SVMX_SAP_Service_Order_No__c, ''), 'T12345T');

	}
    //----------------------------------------------------------------------------------------	
	
	private static testmethod void test_populateTechnicianInfo(){
		
		createTechnicianData();
		
		Test.startTest();
		
		SVMXC__Service_Order__c workorder = new SVMXC__Service_Order__c();
		workorder.SVMX_SAP_Employee_Id__c = '55555';
		insert workorder;
		
		workorder = [Select SVMXC__Service_Group__c, SVMXC__Group_Member__c, SVMX_FSE_Stock_Location__c from SVMXC__Service_Order__c where Id = :workorder.Id];
		
		System.assert(workorder.SVMXC__Service_Group__c != null);	
		System.assert(workorder.SVMXC__Group_Member__c != null);
		System.assert(workorder.SVMX_FSE_Stock_Location__c != null);		
	}
	
	private static testmethod void test_populateTechnicianInfo_NotFound(){
		
		createTechnicianData();
		
		Test.startTest();
		
		SVMXC__Service_Order__c workorder = new SVMXC__Service_Order__c();
		workorder.SVMX_SAP_Employee_Id__c = '44444';
		insert workorder;
		
		workorder = [Select SVMXC__Service_Group__c, SVMXC__Group_Member__c, SVMX_FSE_Stock_Location__c from SVMXC__Service_Order__c where Id = :workorder.Id];
		
		System.assert(workorder.SVMXC__Service_Group__c == null);	
		System.assert(workorder.SVMXC__Group_Member__c == null);
		System.assert(workorder.SVMX_FSE_Stock_Location__c == null);		
	}
	
	private static testmethod void test_updateTechnicianInfo(){
		
		createTechnicianData();
						
		SVMXC__Service_Order__c workorder = new SVMXC__Service_Order__c();
		workorder.SVMX_SAP_Employee_Id__c = '55555';
		insert workorder;
		
		workorder = [Select SVMXC__Service_Group__c, SVMXC__Group_Member__c, SVMX_FSE_Stock_Location__c from SVMXC__Service_Order__c where Id = :workorder.Id];
		
		System.assert(workorder.SVMXC__Service_Group__c != null);	
		System.assert(workorder.SVMXC__Group_Member__c != null);
		System.assert(workorder.SVMX_FSE_Stock_Location__c != null);
		
		Test.startTest();
		
		workorder.SVMXC__Group_Member__c = [Select Id from SVMXC__Service_Group_Members__c where SVMX_SAP_Employee_Id__c = '66666'].Id;
		update workorder;		
		
		workorder = [Select SVMXC__Service_Group__c, SVMX_SAP_Employee_Id__c, SVMX_FSE_Stock_Location__c from SVMXC__Service_Order__c where Id = :workorder.Id];
		
		System.assert(workorder.SVMXC__Service_Group__c != null);	
		System.assert(workorder.SVMX_SAP_Employee_Id__c == '66666');
		System.assert(workorder.SVMX_FSE_Stock_Location__c != null);
	}
	
	private static testmethod void test_clearTechnicianInfo(){
		
		createTechnicianData();
						
		SVMXC__Service_Order__c workorder = new SVMXC__Service_Order__c();
		workorder.SVMX_SAP_Employee_Id__c = '55555';
		insert workorder;
		
		workorder = [Select SVMXC__Service_Group__c, SVMXC__Group_Member__c, SVMX_FSE_Stock_Location__c from SVMXC__Service_Order__c where Id = :workorder.Id];
		
		System.assert(workorder.SVMXC__Service_Group__c != null);	
		System.assert(workorder.SVMXC__Group_Member__c != null);
		System.assert(workorder.SVMX_FSE_Stock_Location__c != null);
		
		Test.startTest();
		
		workorder.SVMXC__Group_Member__c = null;
		update workorder;		
		
		workorder = [Select SVMXC__Service_Group__c, SVMX_SAP_Employee_Id__c, SVMX_FSE_Stock_Location__c from SVMXC__Service_Order__c where Id = :workorder.Id];
		
		System.assert(workorder.SVMXC__Service_Group__c != null);// The Technician group is not cleared as it is the filter criteria to find a colleague of the original Technician	
		System.assert(workorder.SVMX_SAP_Employee_Id__c == null);
		System.assert(workorder.SVMX_FSE_Stock_Location__c == null);
	}
	
	private static testmethod void test_manageWorkorderCalendarEvent(){
		
		createTechnicianData();
		
		Test.startTest();
		
		SVMXC__Service_Order__c workorder = new SVMXC__Service_Order__c();
		workorder.SVMX_SAP_Employee_Id__c = '55555';
		workorder.SVMXC__Order_Status__c = 'Scheduled';
		workorder.SVMXC__Scheduled_Date_Time__c = Datetime.now().addDays(1);
		workorder.SVMX_Planned_Job_Duration__c = 2;
		insert workorder;
		
		Event woEvent = [Select OwnerId, ActivityDateTime, DurationInMinutes from Event where WhatId = :workorder.Id];
		
		System.assert(woEvent.OwnerId == UserInfo.getUserId());
		System.assertEquals(workorder.SVMXC__Scheduled_Date_Time__c, woEvent.ActivityDateTime);
		System.assert(woEvent.DurationInMinutes == 120);
		
		workorder.SVMXC__Scheduled_Date_Time__c = Datetime.now().addDays(2);
		update workorder;
		
		woEvent = [Select OwnerId, ActivityDateTime, DurationInMinutes from Event where WhatId = :workorder.Id];
				
		System.assertEquals(workorder.SVMXC__Scheduled_Date_Time__c, woEvent.ActivityDateTime);		
		
		workorder.SVMXC__Scheduled_Date_Time__c = null;
		update workorder;
		
		List<Event> woEvents = [Select OwnerId, ActivityDateTime, DurationInMinutes from Event where WhatId = :workorder.Id];
		
		System.assert(woEvents.size() == 0);
	}
	
	private static testmethod void test_updateInstProductOnComplete(){
		
		createTechnicianData();
		createInstProductData();
				
		SVMXC__Service_Order__c workorder1 = new SVMXC__Service_Order__c();
		workorder1.SVMXC__Order_Status__c = 'Scheduled';
		workorder1.SVMXC__Component__c = [Select Id from SVMXC__Installed_Product__c].Id;
				
		SVMXC__Service_Order__c workorder2 = new SVMXC__Service_Order__c();
		workorder2.SVMXC__Order_Status__c = 'Scheduled';
		workorder2.SVMXC__Component__c = workorder1.SVMXC__Component__c;
		
		SVMXC__Service_Order__c workorder3 = new SVMXC__Service_Order__c();
		workorder3.SVMXC__Order_Status__c = 'Scheduled';
		workorder3.SVMXC__Component__c = workorder1.SVMXC__Component__c;
		
		insert new List<SVMXC__Service_Order__c>{workorder1, workorder2, workorder3};
		
		Test.startTest();
		
		workorder1.SVMX_Debrief_Complete__c = true;
		workorder1.SVMXC__Order_Status__c = 'Job Completed';		    	
    	workorder1.SVMX_Equipment_Use_Status__c = 'Obsolete';
    	workorder1.SVMXC__Actual_Resolution__c = DateTime.now().addHours(-2);
    	workorder1.SVMX_Equipment_Room__c = '1A302';
    	workorder1.Last_Disinfection_Date__c = Date.today();
    	
    	workorder2.SVMX_Debrief_Complete__c = true;
		workorder2.SVMXC__Order_Status__c = 'Job Completed';		
    	workorder2.SVMX_Software_Version__c = '1.1.2';    	
    	workorder2.SVMX_Equipment_Use_Status__c = 'With Limitations';
    	workorder2.SVMXC__Actual_Resolution__c = DateTime.now().addHours(-1);
    	workorder2.SVMX_Optional_Software_Installed__c = 'Windows 7';
    	    	
		workorder3.SVMXC__Order_Status__c = 'Awaiting Parts';		
    	workorder3.SVMX_Asset_Tag__c = 'Oarm';    	
    	workorder3.SVMX_Equipment_Use_Status__c = 'Obsolete';
    	workorder3.SVMX_Equipment_Room__c = '0B105';
    	    	
    	update new List<SVMXC__Service_Order__c>{workorder1, workorder2, workorder3};
    	
    	Test.stopTest();
    	
    	SVMXC__Installed_Product__c instProduct = [Select Id, SVMX_Software_Version__c, SVMX_Equipment_Use_Status__c, SVMXC__Asset_Tag__c, Equipment_Room__c, SVMX_Optional_Software_Installed__c, Last_Disinfection_Date__c from SVMXC__Installed_Product__c where Id = :workorder1.SVMXC__Component__c];
    	
    	System.assert(instProduct.SVMX_Software_Version__c == '1.1.2');
    	System.assert(instProduct.SVMX_Equipment_Use_Status__c == 'With Limitations');
    	System.assert(instProduct.SVMXC__Asset_Tag__c == 'Oarm');
    	System.assert(instProduct.Equipment_Room__c == '1A302');
    	System.assert(instProduct.SVMX_Optional_Software_Installed__c == 'Windows 7');
    	System.assert(instProduct.Last_Disinfection_Date__c == Date.today());
	}


	@IsTest private static void test_updateInstallProduct_PM(){
		
		createTechnicianData();
		createInstProductData(3);
				
		List<SVMXC__Installed_Product__c> lstInstalledProduct = [SELECT Id FROM SVMXC__Installed_Product__c];
		lstInstalledProduct[0].SAP_InstallBase_ID__c = 'SAP001';
		update lstInstalledProduct[0];

		SVMXC__Service_Order__c workorder1 = new SVMXC__Service_Order__c();
		workorder1.SVMXC__Order_Status__c = 'Scheduled';
		workorder1.SVMXC__Purpose_of_Visit__c = 'Planned Maintenance';
		workorder1.SVMXC__Component__c = lstInstalledProduct[0].Id;
				
		insert new List<SVMXC__Service_Order__c>{workorder1};

		List<Serviced_Equipment__c> lstServicedEquipment = new List<Serviced_Equipment__c>();
		for (SVMXC__Installed_Product__c oInstalledProduct : lstInstalledProduct){

			Serviced_Equipment__c oServicedEquipment1 = new Serviced_Equipment__c();
				oServicedEquipment1.Serviced_Equipment__c = oInstalledProduct.Id;
				oServicedEquipment1.Work_Order__c = workorder1.Id;
			lstServicedEquipment.add(oServicedEquipment1);

		}
		insert lstServicedEquipment;
		
		Test.startTest();
		
		workorder1.SVMX_Debrief_Complete__c = true;
		workorder1.SVMXC__Order_Status__c = 'Job Completed';		    	
    	workorder1.SVMX_Equipment_Use_Status__c = 'Obsolete';
    	workorder1.SVMXC__Actual_Resolution__c = DateTime.now().addHours(-2);
    	workorder1.SVMX_Equipment_Room__c = '1A302';
    	workorder1.Last_Disinfection_Date__c = Date.today();
		workorder1.SVMXC__Actual_Resolution__c = Date.today();
    	    	    	
    	update new List<SVMXC__Service_Order__c>{workorder1};
    	
    	Test.stopTest();
    	
    	List<Serviced_Equipment__c> lstServiceEquipment = 
			[
				SELECT 
					Id
					, Serviced_Equipment__c, Serviced_Equipment__r.Last_PM_Date__c, Serviced_Equipment__r.Last_Service_Date__c, Serviced_Equipment__r.Next_PM_Type__c, Serviced_Equipment__r.Next_PM_Milestone__c
					, Work_Order__c, Work_Order__r.SVMXC__Actual_Resolution__c, Work_Order__r.Next_PM_Type__c, Work_Order__r.Next_PM_Milestone__c
				FROM 
					Serviced_Equipment__c
				WHERE
					Work_Order__c = :workorder1.Id
			];

	
		for (Serviced_Equipment__c oServicedEquipment : lstServiceEquipment){

			System.debug('**BC** oServicedEquipment.Serviced_Equipment__r.Last_PM_Date__c : ' + oServicedEquipment.Serviced_Equipment__r.Last_PM_Date__c);
			System.debug('**BC** oServicedEquipment.Work_Order__r.SVMXC__Actual_Resolution__c : ' + oServicedEquipment.Work_Order__r.SVMXC__Actual_Resolution__c);
			System.debug('**BC** oServicedEquipment.Serviced_Equipment__r.Next_PM_Type__c : ' + oServicedEquipment.Serviced_Equipment__r.Next_PM_Type__c);
			System.debug('**BC** oServicedEquipment.Work_Order__r.Next_PM_Type__c : ' + oServicedEquipment.Work_Order__r.Next_PM_Type__c);
			System.debug('**BC** oServicedEquipment.Serviced_Equipment__r.Next_PM_Type__c : ' + oServicedEquipment.Serviced_Equipment__r.Next_PM_Type__c);
			System.debug('**BC** oServicedEquipment.Work_Order__r.Next_PM_Milestone__c : ' + oServicedEquipment.Work_Order__r.Next_PM_Milestone__c);

			System.assertEquals(oServicedEquipment.Serviced_Equipment__r.Last_PM_Date__c, oServicedEquipment.Work_Order__r.SVMXC__Actual_Resolution__c.date());
			System.assertEquals(oServicedEquipment.Serviced_Equipment__r.Next_PM_Type__c, oServicedEquipment.Work_Order__r.Next_PM_Type__c);
			System.assertEquals(oServicedEquipment.Serviced_Equipment__r.Next_PM_Milestone__c, oServicedEquipment.Work_Order__r.Next_PM_Milestone__c);

		}
    	
	}
	
	private static testmethod void test_addInternalTextComment(){
		
		createTechnicianData();
						
		SVMXC__Service_Order__c workorder1 = new SVMXC__Service_Order__c();
		workorder1.SVMX_SAP_Employee_Id__c = '111111';
		workorder1.SVMXC__Order_Status__c = 'Scheduled';
		workorder1.SVMXC__Scheduled_Date_Time__c = Datetime.now().addDays(1);
		workorder1.SVMX_Planned_Job_Duration__c = 2;
		workorder1.SVMXC__Problem_Description__c = 'Seems like customer misuse';
		
		SVMXC__Service_Order__c workorder2 = new SVMXC__Service_Order__c();
		workorder2.SVMX_SAP_Employee_Id__c = '222222';
		workorder2.SVMXC__Order_Status__c = 'Scheduled';
		workorder2.SVMXC__Scheduled_Date_Time__c = Datetime.now().addDays(5);
		workorder2.SVMX_Planned_Job_Duration__c = 1;
		
		insert new List<SVMXC__Service_Order__c>{workorder1, workorder2};
		
		Test.startTest();
		
		workorder1.SVMX_Problem_Description_Comment__c = 'Customer misuse confirmed';
		workorder2.SVMX_Problem_Description_Comment__c = 'The system is a terrible state, we should replace it';
		
		update new List<SVMXC__Service_Order__c>{workorder1, workorder2};
		
		workorder1 = [Select Id, SVMXC__Problem_Description__c, SVMX_Problem_Description_Comment__c from SVMXC__Service_Order__c where Id = :workorder1.Id];
		
		System.assert(workorder1.SVMXC__Problem_Description__c.startsWith('Seems like customer misuse'));
		System.assert(workorder1.SVMXC__Problem_Description__c.contains('Customer misuse confirmed'));
		System.assert(workorder1.SVMX_Problem_Description_Comment__c == null);
		
		workorder2 = [Select Id, SVMXC__Problem_Description__c, SVMX_Problem_Description_Comment__c from SVMXC__Service_Order__c where Id = :workorder2.Id];
				
		System.assert(workorder2.SVMXC__Problem_Description__c.contains('The system is a terrible state, we should replace it'));
		System.assert(workorder2.SVMX_Problem_Description_Comment__c == null);
	}
	
	private static testmethod void test_alignBillingTypeAndMisuseIndicator(){
		
		createTechnicianData();
		
		Test.startTest();
						
		SVMXC__Service_Order__c workorder1 = new SVMXC__Service_Order__c();
		workorder1.SVMX_SAP_Employee_Id__c = '111111';
		workorder1.SVMXC__Order_Status__c = 'Open';
		workorder1.SVMXC__Scheduled_Date_Time__c = Datetime.now().addDays(1);
		workorder1.SVMXC__Billing_Type__c = 'Customer Misuse';
				
		SVMXC__Service_Order__c workorder2 = new SVMXC__Service_Order__c();
		workorder2.SVMX_SAP_Employee_Id__c = '222222';
		workorder2.SVMXC__Order_Status__c = 'Open';
		workorder2.SVMXC__Scheduled_Date_Time__c = Datetime.now().addDays(5);
		workorder2.SVMXC__Billing_Type__c = 'Service Repair';
				
		insert new List<SVMXC__Service_Order__c>{workorder1, workorder2};
			
		workorder1 = [Select Id, SVMX_Equipment_Misused__c from SVMXC__Service_Order__c where Id = :workorder1.Id];		
		System.assert(workorder1.SVMX_Equipment_Misused__c == true);
		
		workorder2 = [Select Id, SVMX_Equipment_Misused__c from SVMXC__Service_Order__c where Id = :workorder2.Id];		
		System.assert(workorder2.SVMX_Equipment_Misused__c == false);
		
		workorder1.SVMXC__Billing_Type__c = 'Cust Replace/exchange';				
		workorder2.SVMX_Equipment_Misused__c = true;		
		update new List<SVMXC__Service_Order__c>{workorder1, workorder2};
		
		workorder1 = [Select Id, SVMX_Equipment_Misused__c from SVMXC__Service_Order__c where Id = :workorder1.Id];		
		System.assert(workorder1.SVMX_Equipment_Misused__c == false);
		
		workorder2 = [Select Id, SVMX_Equipment_Misused__c from SVMXC__Service_Order__c where Id = :workorder2.Id];		
		System.assert(workorder2.SVMX_Equipment_Misused__c == true);
	}
	
	private static void createInstProductData(){

		createInstProductData(1);

	}
		
	private static void createInstProductData(Integer iRecCount){

		Account sapAccount = new Account();
    		sapAccount.Name = 'Test Account';
    		sapAccount.SAP_Id__c = '11111'; // This will set the record type to SAP Account
    		sapAccount.Account_Country_vs__c = 'NETHERLANDS';
    		sapAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('SAP Account').getRecordTypeId();
    		sapAccount.SAP_Account_Type__c = 'Ship to Party';
    	insert sapAccount;
		
		
		List<SVMXC__Installed_Product__c> lstInstalledProduct = new List<SVMXC__Installed_Product__c>();
		List<Product2> lstProduct = new List<Product2>();
		for (Integer iCounter = 0; iCounter < iRecCount; iCounter++){

			Product2 installed = new Product2();
    			installed.IsActive = true; 
    			installed.Name = 'Test Installed Product';
    			installed.MPG_Code_Text__c = 'AA';
    			installed.RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('SAP Product').getRecordTypeId();
    			installed.ProductCode = '12345' + String.valueOf(iCounter);
			lstProduct.add(installed);

		}
    	insert lstProduct;

		
		for (Integer iCounter = 0; iCounter < iRecCount; iCounter++){

			SVMXC__Installed_Product__c instProduct = new SVMXC__Installed_Product__c();
    			instProduct.SVMXC__Company__c = sapAccount.Id;
    			instProduct.SVMXC__Country__c = 'Netherlands, The';
    			instProduct.SVMXC__Product__c = lstProduct[iCounter].Id;
    			instProduct.SVMX_SAP_Equipment_ID__c = '99999' + String.valueOf(iCounter);
    			instProduct.SVMXC__Serial_Lot_Number__c = '99999' + String.valueOf(iCounter);
			lstInstalledProduct.add(instProduct);

		}
    	insert lstInstalledProduct;
		
	}
	
	private static void createTechnicianData(){
		
		SVMXC__Service_Group__c techGroup = new SVMXC__Service_Group__c();
    	techGroup.SVMXC__Active__c = true; 
    	techGroup.SVMXC__Country__c = 'Netherlands, The';
    	techGroup.SVMXC__Description__c = 'Test Technician Group';    	
    	insert techGroup;
    	
    	SVMXC__Service_Group_Members__c technician = new SVMXC__Service_Group_Members__c();
    	technician.SVMXC__Active__c = true;
    	technician.SVMXC__Country__c = 'Netherlands, The';
    	technician.SVMXC__Salesforce_User__c = UserInfo.getUserId();
    	technician.SVMX_SAP_Employee_Id__c = '55555';
    	technician.SVMXC__Service_Group__c = techGroup.Id; 
    	
    	SVMXC__Service_Group_Members__c techMate = new SVMXC__Service_Group_Members__c();
    	techMate.SVMXC__Active__c = true;
    	techMate.SVMXC__Country__c = 'Netherlands, The';
    	techMate.SVMXC__Salesforce_User__c = UserInfo.getUserId();
    	techMate.SVMX_SAP_Employee_Id__c = '66666';
    	techMate.SVMXC__Service_Group__c = techGroup.Id;    	
    	
    	insert new List<SVMXC__Service_Group_Members__c>{technician, techMate};
    	
    	SVMXC__Site__c groupLocation = new SVMXC__Site__c();
    	groupLocation.SVMXC__Country__c = 'Netherlands, The';
    	groupLocation.SVMX_SAP_Location_ID__c = 'XXXXX';
    	groupLocation.SVMX_Service_Team__c = techGroup.Id;
    	groupLocation.SVMXC__Stocking_Location__c = true;
    	groupLocation.SVMXC__IsStaging_Location__c = true;
    	groupLocation.SVMXC__IsRepair_Location__c = true;
    	groupLocation.SVMXC__IsReceiving_Location__c = true;
    	groupLocation.SVMXC__IsGood_Stock__c = true;
    	    	
    	SVMXC__Site__c techLocation = new SVMXC__Site__c();
    	techLocation.SVMXC__Country__c = 'Netherlands, The';
    	techLocation.SVMX_SAP_Location_ID__c = 'YYYYY';
    	techLocation.SVMXC__Service_Engineer__c = UserInfo.getUserId();
    	techLocation.SVMX_Technician__c = technician.Id;
    	techLocation.SVMXC__Stocking_Location__c = true;
    	techLocation.SVMXC__IsStaging_Location__c = true;
    	techLocation.SVMXC__IsRepair_Location__c = true;
    	techLocation.SVMXC__IsReceiving_Location__c = true;
    	techLocation.SVMXC__IsGood_Stock__c = true;
    	
    	SVMXC__Site__c techMateLocation = new SVMXC__Site__c();
    	techMateLocation.SVMXC__Country__c = 'Netherlands, The';
    	techMateLocation.SVMX_SAP_Location_ID__c = 'ZZZZZ';
    	techMateLocation.SVMXC__Service_Engineer__c = UserInfo.getUserId();
    	techMateLocation.SVMX_Technician__c = techMate.Id;
    	techMateLocation.SVMXC__Stocking_Location__c = true;
    	techMateLocation.SVMXC__IsStaging_Location__c = true;
    	techMateLocation.SVMXC__IsRepair_Location__c = true;
    	techMateLocation.SVMXC__IsReceiving_Location__c = true;
    	techMateLocation.SVMXC__IsGood_Stock__c = true;
    	
    	insert new List<SVMXC__Site__c>{groupLocation, techLocation, techMateLocation};
    	
    	techGroup.SVMX_Service_Team_Location__c = groupLocation.Id;
    	update techGroup;
    	
    	technician.SVMXC__Inventory_Location__c = techLocation.Id;
    	techMate.SVMXC__Inventory_Location__c = techMateLocation.Id;
    	update new List<SVMXC__Service_Group_Members__c>{technician, techMate};
	}	
}
//--------------------------------------------------------------------------------------------------------------------