global with sharing class ctrlExt_Opportunity_Lot_Edit {
    
    public List<OpportunityLotWrapper> oppLots {get; set;}
    public List<SelectOption> oppSBUs {get; set;}
	public List<SelectOption> oppAccounts {get; set;}
	
	public Opportunity opp {get; set;}
	public User currentUser {get; set;}
	public String productFilter {get; set;}
	
	public OpportunityLotWrapper editOppLot {get; set;}

	public ctrlExt_Opportunity_Lot_Edit(ApexPages.StandardController sc){
		
		if(Test.isRunningTest() == false) sc.addFields(new List<String>{'Product_Offered__c'});
						
		Opportunity_Lot__c record = (Opportunity_Lot__c) sc.getRecord();
		
		currentUser = [Select Company_Code_Text__c from User where Id = :UserInfo.getUserId()];
		
		oppSBUs = new List<SelectOption>();
						
		opp = [Select Name, Sub_Business_Unit__c, AccountId, CurrencyISOCode from Opportunity where Id = :record.Opportunity__c];
			
		if(opp.Sub_Business_Unit__c != null){
						
			List<String> oppSBUList = opp.Sub_Business_Unit__c.split(';');
			
			for(Sub_Business_Units__c oppSBU : [Select Id, Name from Sub_Business_Units__c where Business_Unit__r.Company__r.Company_Code_Text__c = :currentUser.Company_Code_Text__c AND Name IN :oppSBUList ORDER BY Name]){
				
				oppSBUs.add(new SelectOption(oppSBU.Id, oppSBU.Name));
			}
		}
		
		oppAccounts = new List<SelectOption>();
						
		for(Opportunity_Account__c oppAccount : [Select Account__c, Account__r.Name from Opportunity_Account__c where Opportunity__c = :opp.Id ORDER BY Account__r.Name]){
			
			oppAccounts.add(new SelectOption(oppAccount.Account__c, oppAccount.Account__r.Name));
		}	
		
		oppLots = new List<OpportunityLotWrapper>();
		
		for(Opportunity_Lot__c lot : [Select Id, Name, Sub_Business_Unit__c, Product_Offered__c, Num_of_Suppliers__c, Award_Type__c, Account__c, Reason_for_No_Offer__c, Actual_Revenue_Vendavo__c,Opportunity__c, Opportunity_Opportunity_Lot_Number__c, Other_Weight__c, Price_Weight__c, Quality_Weight__c,
					Status__c, Reason_for_Losing_Winning__c, Winner__c, Units_in_Tender_QTY__c, Full_Potential__c, CurrencyIsoCode, Current_Potential__c, Estimated_Revenue__c, Lot_Owner__c 
					from Opportunity_Lot__c where Opportunity__c = :opp.Id ORDER BY Name]){
			
			OpportunityLotWrapper lotWrapper = new OpportunityLotWrapper();
			lotWrapper.lot = lot;
			lotWrapper.id = lot.Id;
			
			oppLots.add(lotWrapper);						
		}
		
		if(record.id != null){
			
			if(ApexPages.currentPage().getParameters().get('clone') == '1'){
				
				for(OpportunityLotWrapper lotWrapper : oppLots){
			
					if(lotWrapper.id == record.id){
						
						OpportunityLotWrapper cloneOppLot = new OpportunityLotWrapper();
						cloneOppLot.id = GuidUtil.NewGuid();
						cloneOppLot.lot = lotWrapper.lot.clone(false);
						cloneOppLot.lot.Name += ' (clone)';	
						oppLots.add(cloneOppLot);
						
						selectedLot = cloneOppLot.id;
						editLot();
						
						break;
					}
				}
				
			}else{
			
				selectedLot = record.Id;
				editLot();
			}
		}				
	}
	
	public String selectedLot {get; set;}
	
	public void editLot(){
				
		for(OpportunityLotWrapper lotWrapper : oppLots){
			
			if(lotWrapper.id == selectedLot){
				
				editOppLot = new OpportunityLotWrapper();
				editOppLot.id = lotWrapper.id;
				editOppLot.lot = lotWrapper.lot.clone(true);								
				break;
			}
		}
		
		setProductFilter();
	}
	
	public void cloneLot(){
				
		for(OpportunityLotWrapper lotWrapper : oppLots){
			
			if(lotWrapper.id == selectedLot){
				
				editOppLot = new OpportunityLotWrapper();				
				editOppLot.lot = lotWrapper.lot.clone(false);
				editOppLot.id = GuidUtil.NewGuid();					
				oppLots.add(editOppLot);								
				break;
			}
		}
		
		selectedLot = editOppLot.id;
		oppLots.sort();
		
		setProductFilter();
	}
	
	private void setProductFilter(){
		
		if(editOppLot.lot.Sub_Business_Unit__c != null){
			
			Sub_Business_Units__c sbu = [Select Business_Unit__c, Business_Unit__r.Business_Unit_Group__r.Name from Sub_Business_Units__c where Id = :editOppLot.lot.Sub_Business_Unit__c];
			
			if(sbu.Business_Unit__r.Business_Unit_Group__r.Name == 'Diabetes'){
				
				productFilter = 'WHERE Business_Unit_ID__c = \'' + sbu.Business_Unit__c + '\'';
				
			}else{
				
				productFilter = 'WHERE Product_Group__r.Therapy_ID__r.Sub_Business_Unit__c = \'' + editOppLot.lot.Sub_Business_Unit__c + '\''; 
			}
			
			productFilter += ' AND Business_Unit_ID__r.Company__r.Company_Code_Text__c = \'' + currentUser.Company_Code_Text__c + '\' AND RecordType.DeveloperName = \'MDT_Marketing_Product\' AND IsActive = true';
			
		}else{
			
			productFilter = '';
		}
	}
	
	public void deleteLot(){
		
		for(Integer i = 0; i < oppLots.size(); i++){
			
			if(oppLots[i].id == selectedLot){
				oppLots.remove(i);
				break;
			}
		}
	}
	
	public void addNewLot(){
		
		editOppLot = new OpportunityLotWrapper();		
		editOppLot.lot = new Opportunity_Lot__c();
		editOppLot.lot.Opportunity__c = opp.Id;
		editOppLot.lot.CurrencyISOCode = opp.CurrencyISOCode;
		if(oppAccounts.size() > 0) editOppLot.lot.Account__c = opp.AccountId;
		if(oppSBUs.size() > 0){
			
			editOppLot.lot.Sub_Business_Unit__c = oppSBUs[0].getValue();
			setProductFilter();
		}		
	}
	
	public void applyEdit(){
		
		Boolean saveError = false;
		
		if(editOppLot.lot.Name == null || editOppLot.lot.Name == ''){
			
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Lot Name is a mandatory field'));
			saveError = true;
		}
		
		if(editOppLot.lot.Sub_Business_Unit__c == null){
			
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Sub Business Unit is a mandatory field'));
			saveError = true;
		}
		
		if(editOppLot.lot.Reason_for_no_Offer__c == null && editOppLot.lot.Lot_Owner__c == null){
			
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Lot Owner is a mandatory field'));
			saveError = true;
		}
		
		if(saveError) return;
		
		if(editOppLot.id == null){
		
			editOppLot.id = GuidUtil.NewGuid();					
			oppLots.add(editOppLot);
		
		}else{
			
			for(OpportunityLotWrapper lotWrapper : oppLots){
			
				if(lotWrapper.id == editOppLot.id){
					
					lotWrapper.lot = editOppLot.lot;								
					break;
				}
			}
		}		
		
		selectedLot = null;
		editOppLot = null;
		oppLots.sort();
	}
	
	public void cancelEdit(){
		
		editOppLot = null;
		selectedLot = null;
	}
	
	public void onSBUChange(){
		
		editOppLot.lot.Product_Offered__c = null;
		setProductFilter();
	}
	
	public PageReference save(){
		
		Savepoint sp = Database.setSavepoint();
						
		try{
			
			List<Opportunity_Lot__c> oppLotList = new List<Opportunity_Lot__c>();			
			for(OpportunityLotWrapper lot : oppLots) oppLotList.add(lot.lot);
			
			delete [Select Id from Opportunity_Lot__c where Opportunity__c = :opp.Id AND Id NOT IN :oppLotList];
			
			upsert oppLotList;
			
		}catch(Exception e){
			
			Database.rollback(sp);
			
			ApexPages.addMessages(e);
			return null;
		}
		
		PageReference pr = new PageReference('/' + opp.Id);
		pr.setRedirect(true);
		
		return pr;
	}
	
	public class OpportunityLotWrapper implements Comparable{
		
		public Opportunity_Lot__c lot {get; set;}
		public String id {get; set;}
		
		public Integer compareTo(Object compareTo) {
    		
    		OpportunityLotWrapper other = (OpportunityLotWrapper) compareTo;
    		
    		if(lot.Name > other.lot.Name) return 1;    		
    		if(lot.Name < other.lot.Name) return -1;
    		
    		return 0;
		}				
	}
}