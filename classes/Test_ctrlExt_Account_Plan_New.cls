@isTest
private class Test_ctrlExt_Account_Plan_New {

	private static User oUser_SystemAdministrator;
	private static User oUser_SystemAdministratorMDT;
	private static User oUser;
	@isTest private static void createTestUser(){

		List<User> lstUser = new List<User>();
		oUser = clsTestData_User.createUser_SystemAdministrator('tadm000', false);
		oUser_SystemAdministratorMDT = clsTestData_User.createUser_SystemAdministratorMDT('tadm001', false);
		oUser_SystemAdministrator = clsTestData_User.createUser_SystemAdministrator('tadm002', false);
		lstUser.add(oUser);
		lstUser.add(oUser_SystemAdministratorMDT);
		lstUser.add(oUser_SystemAdministrator);
		insert lstUser;

	}

	static testmethod void testNewAccountPlanNonDiabetesBUG(){
		
		Account acc = new Account();
		acc.Name = 'Unit Test Account';
		acc.SAP_Id__c = '123456789';
		acc.Account_Country_vs__c = 'Belgium';
		insert acc;
		
		Account_Plan_2__c accPlan = new Account_Plan_2__c();
		accPlan.Account__c = acc.Id;
		
		ctrlExt_Account_Plan_New controller = new ctrlExt_Account_Plan_New(new ApexPages.StandardController(accPlan));
		
		System.assert(controller.accountMissing == false);
		System.assert(accPlan.Business_Unit__c != null);
		System.assert(accPlan.Account_Plan_Level__c == 'Sub Business Unit');
				
		accPlan.Business_Unit__c = [Select Id from Business_Unit__c where Name = 'CRHF'].Id;
		controller.BUChanged();
		
		accPlan.Account_Plan_Level__c = 'Business Unit Group';		
		controller.levelChanged();
						
		PageReference pr = controller.createAccountPlan();
		
		System.assert(pr != null);	
	}
	
	static testmethod void testNewAccountPlanNonDiabetesBU(){
		
		Account acc = new Account();
		acc.Name = 'Unit Test Account';
		acc.SAP_Id__c = '123456789';
		acc.Account_Country_vs__c = 'Belgium';
		insert acc;
		
		Account_Plan_2__c accPlan = new Account_Plan_2__c();
		accPlan.Account__c = acc.Id;
		
		ctrlExt_Account_Plan_New controller = new ctrlExt_Account_Plan_New(new ApexPages.StandardController(accPlan));
		
		System.assert(controller.accountMissing == false);
		System.assert(accPlan.Business_Unit__c != null);
		System.assert(accPlan.Account_Plan_Level__c == 'Sub Business Unit');
				
		accPlan.Business_Unit__c = [Select Id from Business_Unit__c where Name = 'CRHF'].Id;
		controller.BUChanged();
		
		accPlan.Account_Plan_Level__c = 'Business Unit';		
		controller.levelChanged();
		
		PageReference pr = controller.createAccountPlan();
		
		System.assert(pr != null);	
	}
	
	static testmethod void testNewAccountPlanNonDiabetesSBU(){
		
		Account acc = new Account();
		acc.Name = 'Unit Test Account';
		acc.SAP_Id__c = '123456789';
		acc.Account_Country_vs__c = 'Belgium';
		insert acc;
		
		Account_Plan_2__c accPlan = new Account_Plan_2__c();
		accPlan.Account__c = acc.Id;
		
		ctrlExt_Account_Plan_New controller = new ctrlExt_Account_Plan_New(new ApexPages.StandardController(accPlan));
		
		System.assert(controller.accountMissing == false);
		System.assert(accPlan.Business_Unit__c != null);
		System.assert(accPlan.Account_Plan_Level__c == 'Sub Business Unit');
				
		accPlan.Business_Unit__c = [Select Id from Business_Unit__c where Name = 'CRHF'].Id;
		controller.BUChanged();
						
		accPlan.Account_Plan_Level__c = 'Sub Business Unit';		
		controller.levelChanged();
		
		accPlan.Sub_Business_Unit__c = [Select Id from Sub_Business_Units__c where Name = 'AF Solutions'].Id;
		
		PageReference pr = controller.createAccountPlan();
		
		System.assert(pr != null);	
	}
	
	static testmethod void testNewAccountPlanDiabetesCAN(){
		
		Account acc = new Account();
		acc.Name = 'Unit Test Account';
		acc.SAP_Id__c = '123456789';
		acc.Account_Country_vs__c = 'Canada';
		insert acc;
				
		Account_Plan_2__c accPlan = new Account_Plan_2__c();
		accPlan.Account__c = acc.Id;		
		
		ctrlExt_Account_Plan_New controller = new ctrlExt_Account_Plan_New(new ApexPages.StandardController(accPlan));
		
		System.assert(controller.accountMissing == false);
		System.assert(accPlan.Business_Unit__c != null);
		System.assert(accPlan.Account_Plan_Level__c == 'Sub Business Unit');
					
		accPlan.Sub_Business_Unit__c = [Select Id from Sub_Business_Units__c where Name = 'Diabetes Core'].Id;
				
		PageReference pr = controller.createAccountPlan();
				
		System.assert(pr != null);	
	}
	
    static testmethod void testNewAccountPlanDiabetesEUR(){
        
        User oUser_SystemAdministrator = clsTestData_User.createUser_SystemAdministrator('tadm001', true);

        User oUser;
        oUser = clsTestData_User.createUser(true);

        List<Business_Unit_Group__c> lstBUG = [SELECT Id, SFE_Manager__c, SFE_Manager_Canada__c FROM Business_Unit_Group__c];
        for (Business_Unit_Group__c oBUG : lstBUG){
            oBUG.SFE_Manager__c = UserInfo.getUserId();
            oBUG.SFE_Manager_Canada__c = UserInfo.getUserId();
        }
        System.runAs(oUser_SystemAdministrator){
        	update lstBUG;
    	}

        Account acc = new Account();
        acc.Name = 'Unit Test Account';
        acc.SAP_Id__c = '123456789';
        acc.Account_Country_vs__c = 'Belgium';
        System.runAs(oUser_SystemAdministrator){
	        insert acc;
	    }
        
        Account_Plan_2__c existingAccPlan = new Account_Plan_2__c();
        existingAccPlan.Account__c = acc.Id;
        existingAccPlan.Business_Unit__c = [Select Id from Business_Unit__c where Name = 'CRHF'].Id;
        existingAccPlan.Account_Plan_Level__c = 'Sub Business Unit';
        existingAccPlan.Sub_Business_Unit__c = [Select Id from Sub_Business_Units__c where Name = 'AF Solutions'].Id;
        existingAccPlan.OwnerId = oUser.Id;
        System.runAs(oUser_SystemAdministrator){
	        insert existingAccPlan;
	    }
        
        Account_Plan_2__c accPlan = new Account_Plan_2__c();
        
        ApexPages.currentPage().getParameters().put('save_new', '1');
        ApexPages.currentPage().getParameters().put('retURL', '/' + String.valueOf(existingAccPlan.Id).subString(0, 15));
        
        ctrlExt_Account_Plan_New controller = new ctrlExt_Account_Plan_New(new ApexPages.StandardController(accPlan));
        
        System.assert(controller.accountMissing == false);
        System.assert(accPlan.Business_Unit__c != null);
        System.assert(accPlan.Account_Plan_Level__c == 'Sub Business Unit');
                
        controller.selectedPlan = existingAccPlan.Id;
        controller.requestAccessToOwner();

        System.runAs(oUser_SystemAdministrator){
                oUser.IsActive = false;
                oUser.User_Status__c = 'Left Company';
            update oUser;
        }

        controller.requestAccessToOwner();

        
        accPlan.Sub_Business_Unit__c = [Select Id from Sub_Business_Units__c where Name = 'Diabetes Core'].Id;
                
        System.runAs(oUser_SystemAdministrator){

	        PageReference pr = controller.createAccountPlan();
	        System.assert(pr != null);  
    
        }
        
    }    

	static testmethod void test_requestAccessToOwner(){
		
		createTestUser();

		System.runAs(oUser_SystemAdministratorMDT){

			List<Business_Unit_Group__c> lstBUG = [SELECT Id, SFE_Manager__c, SFE_Manager_Canada__c FROM Business_Unit_Group__c];
			for (Business_Unit_Group__c oBUG : lstBUG){
				oBUG.SFE_Manager__c = UserInfo.getUserId();
			}
			update lstBUG;

			List<Sub_Business_Units__c> lstSBU = [SELECT Id, Business_Unit__c, Business_Unit__r.Business_Unit_Group__c FROM Sub_Business_Units__c WHERE Business_Unit__r.Business_Unit_Group__r.SFE_Manager__c != null LIMIT 1];



			//--------------------------------------------------------------------------------------------
			// Account Test Data
			//--------------------------------------------------------------------------------------------
			clsTestData_Account.tCountry_Account = 'BELGIUM';
			clsTestData_Account.oMain_Account = null;
			Account oAccount_BE = clsTestData_Account.createAccount()[0];

			clsTestData_Account.tCountry_Account = 'Canada';
			clsTestData_Account.oMain_Account = null;
			Account oAccount_CAN = clsTestData_Account.createAccount()[0];
			//--------------------------------------------------------------------------------------------


			//--------------------------------------------------------------------------------------------
			// Account Plan Test Data
			//--------------------------------------------------------------------------------------------
			List<Account_Plan_2__c> lstAccountPlan = new List<Account_Plan_2__c>();
			
			Account_Plan_2__c oAccountPlant_BE_BUG = new Account_Plan_2__c();
				oAccountPlant_BE_BUG.Account__c = oAccount_BE.Id;
				oAccountPlant_BE_BUG.Account_Plan_Level__c = 'Business Unit Group';
				oAccountPlant_BE_BUG.Business_Unit_Group__c = lstSBU[0].Business_Unit__r.Business_Unit_Group__c;
			lstAccountPlan.add(oAccountPlant_BE_BUG);

			Account_Plan_2__c oAccountPlant_BE_BU = new Account_Plan_2__c();
				oAccountPlant_BE_BU.Account__c = oAccount_BE.Id;
				oAccountPlant_BE_BU.Account_Plan_Level__c = 'Business Unit';
				oAccountPlant_BE_BU.Business_Unit__c = lstSBU[0].Business_Unit__c;
			lstAccountPlan.add(oAccountPlant_BE_BU);

			Account_Plan_2__c oAccountPlant_BE_SBU = new Account_Plan_2__c();
				oAccountPlant_BE_SBU.Account__c = oAccount_BE.Id;
				oAccountPlant_BE_SBU.Account_Plan_Level__c = 'Sub Business Unit';
				oAccountPlant_BE_SBU.Sub_Business_Unit__c = lstSBU[0].Id;
			lstAccountPlan.add(oAccountPlant_BE_SBU);

			insert lstAccountPlan;
			//--------------------------------------------------------------------------------------------
			

			//--------------------------------------------------------------------------------------------
			// TEST
			//--------------------------------------------------------------------------------------------

			Test.startTest();

			ApexPages.currentPage().getParameters().put('save_new', '1');
			ApexPages.currentPage().getParameters().put('retURL', '/' + String.valueOf(oAccountPlant_BE_BUG.Id).subString(0, 15));
			
			ctrlExt_Account_Plan_New controller = new ctrlExt_Account_Plan_New(new ApexPages.StandardController(new Account_Plan_2__c()));
			
			controller.selectedPlan = oAccountPlant_BE_SBU.Id;
			controller.requestAccessToOwner();

			controller.selectedPlan = oAccountPlant_BE_BU.Id;
			controller.requestAccessToOwner();

			controller.selectedPlan = oAccountPlant_BE_BUG.Id;
			controller.requestAccessToOwner();

			System.runAs(oUser_SystemAdministratorMDT){
					oUser.IsActive = false;
					oUser.User_Status__c = 'Left Company';
				update oUser;
			}

			controller.selectedPlan = oAccountPlant_BE_SBU.Id;
			controller.requestAccessToOwner();

			controller.selectedPlan = oAccountPlant_BE_BU.Id;
			controller.requestAccessToOwner();

			controller.selectedPlan = oAccountPlant_BE_BUG.Id;
			controller.requestAccessToOwner();

			Test.stopTest();

			//--------------------------------------------------------------------------------------------

		}

	}	
	
	static testmethod void test_requestAccessToOwner_CAN(){

		createTestUser();		

		System.runAs(oUser_SystemAdministratorMDT){

			List<Business_Unit_Group__c> lstBUG = [SELECT Id, SFE_Manager__c, SFE_Manager_Canada__c FROM Business_Unit_Group__c];
			for (Business_Unit_Group__c oBUG : lstBUG){
				oBUG.SFE_Manager_Canada__c = UserInfo.getUserId();
			}
			update lstBUG;

			List<Sub_Business_Units__c> lstSBU = [SELECT Id, Business_Unit__c, Business_Unit__r.Business_Unit_Group__c FROM Sub_Business_Units__c WHERE Business_Unit__r.Business_Unit_Group__r.SFE_Manager_Canada__c != null LIMIT 1];


			//--------------------------------------------------------------------------------------------
			// Account Test Data
			//--------------------------------------------------------------------------------------------
			clsTestData_Account.tCountry_Account = 'Canada';
			clsTestData_Account.oMain_Account = null;
			Account oAccount_CAN = clsTestData_Account.createAccount()[0];
			//--------------------------------------------------------------------------------------------


			//--------------------------------------------------------------------------------------------
			// Account Plan Test Data
			//--------------------------------------------------------------------------------------------
			List<Account_Plan_2__c> lstAccountPlan = new List<Account_Plan_2__c>();
			
			Account_Plan_2__c oAccountPlant_CAN_BUG = new Account_Plan_2__c();
				oAccountPlant_CAN_BUG.Account__c = oAccount_CAN.Id;
				oAccountPlant_CAN_BUG.Country__c = 'CANADA';
				oAccountPlant_CAN_BUG.Account_Plan_Level__c = 'Business Unit Group';
				oAccountPlant_CAN_BUG.Business_Unit_Group__c = lstSBU[0].Business_Unit__r.Business_Unit_Group__c;
			lstAccountPlan.add(oAccountPlant_CAN_BUG);

			Account_Plan_2__c oAccountPlant_CAN_BU = new Account_Plan_2__c();
				oAccountPlant_CAN_BU.Account__c = oAccount_CAN.Id;
				oAccountPlant_CAN_BU.Country__c = 'CANADA';
				oAccountPlant_CAN_BU.Account_Plan_Level__c = 'Business Unit';
				oAccountPlant_CAN_BU.Business_Unit__c = lstSBU[0].Business_Unit__c;
			lstAccountPlan.add(oAccountPlant_CAN_BU);

			Account_Plan_2__c oAccountPlant_CAN_SBU = new Account_Plan_2__c();
				oAccountPlant_CAN_SBU.Account__c = oAccount_CAN.Id;
				oAccountPlant_CAN_SBU.Country__c = 'CANADA';
				oAccountPlant_CAN_SBU.Account_Plan_Level__c = 'Sub Business Unit';
				oAccountPlant_CAN_SBU.Sub_Business_Unit__c = lstSBU[0].Id;
			lstAccountPlan.add(oAccountPlant_CAN_SBU);

			insert lstAccountPlan;
			//--------------------------------------------------------------------------------------------
			

			//--------------------------------------------------------------------------------------------
			// TEST
			//--------------------------------------------------------------------------------------------

			Test.startTest();

			ApexPages.currentPage().getParameters().put('save_new', '1');
			ApexPages.currentPage().getParameters().put('retURL', '/' + String.valueOf(oAccountPlant_CAN_BUG.Id).subString(0, 15));
			
			ctrlExt_Account_Plan_New controller = new ctrlExt_Account_Plan_New(new ApexPages.StandardController(new Account_Plan_2__c()));
			
			controller.selectedPlan = oAccountPlant_CAN_SBU.Id;
			controller.requestAccessToOwner();

			controller.selectedPlan = oAccountPlant_CAN_BU.Id;
			controller.requestAccessToOwner();

			controller.selectedPlan = oAccountPlant_CAN_BUG.Id;
			controller.requestAccessToOwner();


			System.runAs(oUser_SystemAdministratorMDT){
					oUser.IsActive = false;
					oUser.User_Status__c = 'Left Company';
				update oUser;
			}

			controller.selectedPlan = oAccountPlant_CAN_SBU.Id;
			controller.requestAccessToOwner();

			controller.selectedPlan = oAccountPlant_CAN_BU.Id;
			controller.requestAccessToOwner();

			controller.selectedPlan = oAccountPlant_CAN_BUG.Id;
			controller.requestAccessToOwner();

			Test.stopTest();

			//--------------------------------------------------------------------------------------------

		}

	}	


	static testmethod void testNewAccountPlanDiabetesEUR_Dupe(){
		
		Account acc = new Account();
		acc.Name = 'Unit Test Account';
		acc.SAP_Id__c = '123456789';
		acc.Account_Country_vs__c = 'Belgium';
		insert acc;
		
		Account_Plan_2__c existingAccPlan = new Account_Plan_2__c();
		existingAccPlan.Account__c = acc.Id;
		existingAccPlan.Business_Unit__c = [Select Id from Business_Unit__c where Name = 'Diabetes'].Id;
		existingAccPlan.Account_Plan_Level__c = 'Sub Business Unit';
		existingAccPlan.Sub_Business_Unit__c = [Select Id from Sub_Business_Units__c where Name = 'Diabetes Core'].Id;		
		insert existingAccPlan;
		
		Account_Plan_2__c accPlan = new Account_Plan_2__c();
		accPlan.Account__c = acc.Id;
				
		ctrlExt_Account_Plan_New controller = new ctrlExt_Account_Plan_New(new ApexPages.StandardController(accPlan));
		
		System.assert(controller.accountMissing == false);
		System.assert(accPlan.Business_Unit__c != null);
		System.assert(accPlan.Account_Plan_Level__c == 'Sub Business Unit');
				
		accPlan.Sub_Business_Unit__c = [Select Id from Sub_Business_Units__c where Name = 'Diabetes Core'].Id;
				
		PageReference pr = controller.createAccountPlan();
		
		System.assert(pr == null);
	}
	
	static testmethod void testNewAccountPlanNoAccount(){
		
		Account acc = new Account();
		acc.Name = 'Unit Test Account';
		acc.SAP_Id__c = '123456789';
		acc.Account_Country_vs__c = 'Belgium';
		insert acc;
		
		Account_Plan_2__c accPlan = new Account_Plan_2__c();
						
		ctrlExt_Account_Plan_New controller = new ctrlExt_Account_Plan_New(new ApexPages.StandardController(accPlan));
		
		System.assert(controller.accountMissing == true);
	}
	
	@testSetup
	static void generateMasterData(){
		
		// Master Data
		Company__c europe = new Company__c();
		europe.Name = 'Europe';
		europe.Company_Code_Text__c = 'TST';
		insert europe;
		
		// Business Unit Groups
		Business_Unit_Group__c cvgBUG = new Business_Unit_Group__c();
		cvgBUG.Master_Data__c = europe.id;
        cvgBUG.name='CVG'; 
		
		Business_Unit_Group__c diabetesBUG = new Business_Unit_Group__c();
		diabetesBUG.Master_Data__c = europe.id;
        diabetesBUG.name='Diabetes';    
        
        insert new List<Business_Unit_Group__c>{cvgBUG, diabetesBUG};
        
        // Business Units
        Business_Unit__c crdmBU =  new Business_Unit__c();
        crdmBU.Company__c = Europe.id;
        crdmBU.name = 'CRHF';
        crdmBU.Business_Unit_Group__c = cvgBUG.Id;
        
        Business_Unit__c diabetesBU =  new Business_Unit__c();
        diabetesBU.Company__c = Europe.id;
        diabetesBU.name = 'Diabetes';
        diabetesBU.Business_Unit_Group__c = diabetesBUG.Id;  
        
        insert new List<Business_Unit__c>{crdmBU, diabetesBU};
        
        // Sub-Business Units
        Sub_Business_Units__c AFSolutionsSBU = new Sub_Business_Units__c();
        AFSolutionsSBU.name = 'AF Solutions';
        AFSolutionsSBU.Business_Unit__c = crdmBU.id;
		
		Sub_Business_Units__c diabetesCoreSBU = new Sub_Business_Units__c();
        diabetesCoreSBU.name = 'Diabetes Core';
        diabetesCoreSBU.Business_Unit__c = diabetesBU.id;
		
		insert new List<Sub_Business_Units__c>{AFSolutionsSBU, diabetesCoreSBU};
		
		// Account Plan Levels
		Account_Plan_Level__c cvgAPLBUG = new Account_Plan_Level__c();
		cvgAPLBUG.Business_Unit__c = crdmBU.id;
		cvgAPLBUG.Account_Plan_Level__c = 'Business Unit Group';
		cvgAPLBUG.Company__c = europe.id;
		cvgAPLBUG.Account_Plan_Record_Type__c = [Select Id from RecordType where SObjectType = 'Account_Plan_2__c' AND DeveloperName = 'EUR_CVG_BUG'].Id;
		
		Account_Plan_Level__c crdmAPLBU = new Account_Plan_Level__c();
		crdmAPLBU.Business_Unit__c = crdmBU.id;
		crdmAPLBU.Account_Plan_Level__c = 'Business Unit';
		crdmAPLBU.Company__c = europe.id;
		crdmAPLBU.Account_Plan_Record_Type__c = [Select Id from RecordType where SObjectType = 'Account_Plan_2__c' AND DeveloperName = 'EUR_CVG_sBU'].Id;
		
		Account_Plan_Level__c crdmAPLSBU = new Account_Plan_Level__c();
		crdmAPLSBU.Business_Unit__c = crdmBU.id;
		crdmAPLSBU.Account_Plan_Level__c = 'Sub Business Unit';
		crdmAPLSBU.Company__c = europe.id;
		crdmAPLSBU.Account_Plan_Record_Type__c = [Select Id from RecordType where SObjectType = 'Account_Plan_2__c' AND DeveloperName = 'EUR_CVG_sBU'].Id;
		
		Account_Plan_Level__c diabetesAPLSBU = new Account_Plan_Level__c();
		diabetesAPLSBU.Business_Unit__c = diabetesBU.id;
		diabetesAPLSBU.Account_Plan_Level__c = 'Sub Business Unit';
		diabetesAPLSBU.Company__c = europe.id;
		diabetesAPLSBU.Account_Plan_Record_Type__c = [Select Id from RecordType where SObjectType = 'Account_Plan_2__c' AND DeveloperName = 'EUR_DIB_sBU'].Id;
		
		insert new List<Account_Plan_Level__c>{cvgAPLBUG, crdmAPLBU, crdmAPLSBU, diabetesAPLSBU};
		
		// User Business Units
		
		User_Business_Unit__c userCRDM = new User_Business_Unit__c();
		userCRDM.User__c = UserInfo.getUserId();
		userCRDM.Sub_Business_Unit__c = AFSolutionsSBU.Id;
		
		User_Business_Unit__c userDiabetes = new User_Business_Unit__c();
		userDiabetes.User__c = UserInfo.getUserId();
		userDiabetes.Sub_Business_Unit__c = diabetesCoreSBU.Id;
		userDiabetes.Primary__c = true;
		
		insert new List<User_Business_Unit__c>{userCRDM, userDiabetes};
	}
    
}