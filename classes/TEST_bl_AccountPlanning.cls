@IsTest 
private class TEST_bl_AccountPlanning {

    @isTest static void test_doesIplanExist() {


		//------------------------------------------------------------------------------------
		// Create Test Data
		//------------------------------------------------------------------------------------
		// Create Account Data
		clsTestData_Account.idRecordType_Account = clsUtil.getRecordTypeByDevName('Account', 'SAP_Account').Id;
		clsTestData_Account.tCountry_Account = 'Belgium';
		clsTestData_Account.createAccount();

		// Create Therapy Data
		clsTestData_Therapy.createTherapy();

		// Create Product Group Data
		clsTestData_Product.createProductGroup();

		// Create Account Plan 2
		clsTestData_AccountPlan.createAccountPlan2();

		iPlanResultWrapper oIPlanResultWrapper;
		//------------------------------------------------------------------------------------


		//------------------------------------------------------------------------------------
		// Test Logic
		//------------------------------------------------------------------------------------
		Test.startTest();

			clsTestData_AccountPlan.oMain_AccountPlan2.Business_Unit_Group__c = null;
			clsTestData_AccountPlan.oMain_AccountPlan2.Business_Unit__c = null;
			clsTestData_AccountPlan.oMain_AccountPlan2.Sub_Business_Unit__c = null;
		  oIPlanResultWrapper = bl_AccountPlanning.doesIplanExist(clsTestData_AccountPlan.oMain_AccountPlan2);

			clsTestData_AccountPlan.oMain_AccountPlan2.Business_Unit_Group__c = clsTestData_MasterData.oMain_BusinessUnitGroup.Id;
			clsTestData_AccountPlan.oMain_AccountPlan2.Business_Unit__c = null;
			clsTestData_AccountPlan.oMain_AccountPlan2.Sub_Business_Unit__c = null;
		  oIPlanResultWrapper = bl_AccountPlanning.doesIplanExist(clsTestData_AccountPlan.oMain_AccountPlan2);

			clsTestData_AccountPlan.oMain_AccountPlan2.Business_Unit_Group__c = null;
			clsTestData_AccountPlan.oMain_AccountPlan2.Business_Unit__c = clsTestData_MasterData.oMain_BusinessUnit.Id;
			clsTestData_AccountPlan.oMain_AccountPlan2.Sub_Business_Unit__c = null;
		  oIPlanResultWrapper = bl_AccountPlanning.doesIplanExist(clsTestData_AccountPlan.oMain_AccountPlan2);

			clsTestData_AccountPlan.oMain_AccountPlan2.Business_Unit_Group__c = null;
			clsTestData_AccountPlan.oMain_AccountPlan2.Business_Unit__c = null;
			clsTestData_AccountPlan.oMain_AccountPlan2.Sub_Business_Unit__c = clsTestData_MasterData.oMain_SubBusinessUnit.Id;
		  oIPlanResultWrapper = bl_AccountPlanning.doesIplanExist(clsTestData_AccountPlan.oMain_AccountPlan2);

		Test.stopTest();
		//------------------------------------------------------------------------------------

	}

}