/*
 * Description      : Interface class to send/receive Attachments from SFDC to Attachments in SAP and back 
 * Author           : Jesus Lozano
 * Created Date     : 18-11-2015
 * Change Log       : 
 */
global class  ws_AttachmentSAPService {
    
    public static final Integer TRIM_ERROR_MSG_LENGTH = 132; 
    
    /*
     * Description      : WS Method to get the Work Order SFDC Id out of the Service Notification SAP Id
     * Author           : Jesus Lozano
     * Created Date     : 18-11-2015
     */
    webservice static List<String> getAttachmentDetailsForNotification(String serviceNotificationId){
    	   	    	
    	List<String> parentIds = new List<String>();
    	
    	List<Case> serviceNotification = [Select Id from Case where SVMX_SAP_Notification_Number__c = :serviceNotificationId];
    	
    	if(serviceNotification.size() == 1){
    		    			
    		parentIds.add(serviceNotification[0].Id);    		
    	}
    	
    	String status = parentIds.size() > 0 ? 'TRUE' : 'FALSE';
    	
    	ut_Log.logOutboundMessage(null, serviceNotificationId, serviceNotificationId, JSON.serializePretty(parentIds), status, 'getAttachmentDetailsForNotification', 'Attachment');
    	
    	return parentIds;
    }
    
    webservice static SAPAttachment getAttachmentDetails(String attachmentId){
    	   	
    	SAPAttachment attachmentDetail = new SAPAttachment();
    	
    	List<Attachment> attachmentList = [Select ParentId, Name, Description, ContentType from Attachment where Id = :attachmentId];
    	
    	if(attachmentList.isEmpty()){
    		
    		attachmentDetail.DISTRIBUTION_STATUS = 'FALSE';
    		attachmentDetail.DIST_ERROR_DETAILS = 'No Attachment record was found for Id: ' + attachmentId;
    		
    	}else{
    	
	    	Attachment record = attachmentList[0];
	    	
	    	String parentId;
	    	
	    	if(string.valueof(record.ParentId.getSObjectType()) == 'Case'){
	    		
	    		Case serviceNotification = [Select Id, SVMX_SAP_Notification_Number__c from Case where Id = :record.ParentId];
	    		
	    		parentId = serviceNotification.SVMX_SAP_Notification_Number__c;   		
	    	
	    	}else if(string.valueof(record.ParentId.getSObjectType()) == 'SVMXC__Service_Order__c'){
	    		
	    		SVMXC__Service_Order__c serviceOrder = [Select Id, SVMXC__Case__r.SVMX_SAP_Notification_Number__c from SVMXC__Service_Order__c where Id = :record.ParentId];
	    		
	    		if(serviceOrder.SVMXC__Case__r != null) parentId = serviceOrder.SVMXC__Case__r.SVMX_SAP_Notification_Number__c;   		
	    	}
	    	
	    	if(parentId == null || parentId == ''){
	    		
	    		attachmentDetail.DISTRIBUTION_STATUS = 'FALSE';
	    		attachmentDetail.DIST_ERROR_DETAILS = 'The Attachment is not connected to a valid parent object. Attachment Id: ' + attachmentId;
	    		
	    	}else{
	    	
		    	attachmentDetail.DISTRIBUTION_STATUS = 'TRUE';
		    	attachmentDetail.SFDC_ID = record.Id;
		    	attachmentDetail.NAME = record.Name;
		    	attachmentDetail.DESCRIPTION = record.Description;
		    	attachmentDetail.CONTENT_TYPE = record.ContentType;    	
		    	attachmentDetail.PARENT_ID = parentId;
	    	}
    	}
    	    	
    	ut_Log.logOutboundMessage(attachmentDetail.SFDC_ID, null, attachmentId, JSON.serializePretty(attachmentDetail), attachmentDetail.DISTRIBUTION_STATUS, 'getAttachmentDetails', 'Attachment');
    	    	
    	return attachmentDetail;
    }
    
    webservice static SAPAttachmentAck createSAPAttachmentAcknowledgement(SAPAttachmentAck attachmentAckToUpdate){
    	
    	SAPAttachmentAck response = new SAPAttachmentAck();
    	response.WMTRANSACTION = attachmentAckToUpdate.WMTRANSACTION;
    	response.TIMESTAMP = attachmentAckToUpdate.TIMESTAMP;
    	
    	Savepoint sp = Database.setSavepoint();
    	
    	try{
	    	
            response.SFDC_IDS = attachmentAckToUpdate.SFDC_IDS;
            response.SAP_ID = attachmentAckToUpdate.SAP_ID;

	    	if(attachmentAckToUpdate.DISTRIBUTION_STATUS == 'TRUE'){
	    	
		    	Sync_Record_Id_Mapping__c idMapping = new Sync_Record_Id_Mapping__c();
		    	idMapping.Name = attachmentAckToUpdate.SFDC_IDS;
		    	idMapping.External_Id__c = attachmentAckToUpdate.SAP_ID;
		    	idMapping.Object_Type__c = 'Attachment';
		    	
		    	insert idMapping;
	    	}
	    		    	
	    	response.DISTRIBUTION_STATUS = 'TRUE';
    	
    	}catch(Exception ex){
    		
    		Database.rollback(sp);
    		
    		//TODO: Update notification record with error result
    		
    		// Construct error message
            String errMsg = (ex.getMessage()).left(TRIM_ERROR_MSG_LENGTH);
                                         	        	
        	response.DISTRIBUTION_STATUS = 'FALSE';
        	SAPErrorMessage msg = new SAPErrorMessage();
	        msg.ERROR_MESSAGE = errMsg;
	        response.DIST_ERROR_DETAILS = new List<SAPErrorMessage>{msg};   
	        
	        if(Test.isRunningTest()) throw ex;
    	}
    	    	
    	ut_Log.logOutboundMessage(attachmentAckToUpdate.SFDC_IDS, attachmentAckToUpdate.SAP_ID, JSON.serializePretty(attachmentAckToUpdate), JSON.serializePretty(response), response.DISTRIBUTION_STATUS, 'createSAPAttachmentAcknowledgement', 'Attachment');

        ut_Log.updateNotificationSAPLogStatus(attachmentAckToUpdate.SFDC_IDS, attachmentAckToUpdate.DISTRIBUTION_STATUS, JSON.serializePretty(attachmentAckToUpdate), JSON.serializePretty(response));

    	return response;
    }
    
    webservice static SFDCAttachmentAck createSFDCAttachmentAcknowledgement(SFDCAttachmentAck attachmentAckToUpdate){
    	
    	SFDCAttachmentAck response = new SFDCAttachmentAck();
    	response.WMTRANSACTION = attachmentAckToUpdate.WMTRANSACTION;
    	response.TIMESTAMP = attachmentAckToUpdate.TIMESTAMP;
    	
    	Savepoint sp = Database.setSavepoint();
    	
    	try{
    	   	
	    	if(attachmentAckToUpdate.DISTRIBUTION_STATUS == 'TRUE'){
	    		    
	    		// From SAP we may get a new version of the Attachment. As SFDC does not support the update of the contect, we delete the old and create a new one    		
	    		List<Sync_Record_Id_Mapping__c> previousVersions = [Select Id, Name from Sync_Record_Id_Mapping__c 
	    																where External_Id__c = :attachmentAckToUpdate.SAP_ID AND 
	    																Name NOT IN :attachmentAckToUpdate.SFDC_IDS AND Object_Type__c = 'Attachment'];
	    		
	    		if(previousVersions.size() > 0){
					
					List<Attachment> toDelete = new List<Attachment>();
					
					for(Sync_Record_Id_Mapping__c previousVersion : previousVersions){
						
						toDelete.add( new Attachment(Id = previousVersion.Name));
					}
														    			
					delete toDelete; // TODO: Ignore failure?	
					
					delete previousVersions;
	    		}
	    		
	    		List<Sync_Record_Id_Mapping__c> newVersions = new List<Sync_Record_Id_Mapping__c>();
	    		
	    		for(String SFDC_ID : attachmentAckToUpdate.SFDC_IDS){
		    	
			    	Sync_Record_Id_Mapping__c idMapping = new Sync_Record_Id_Mapping__c();
			    	idMapping.Name = SFDC_ID;
			    	idMapping.External_Id__c = attachmentAckToUpdate.SAP_ID;
			    	idMapping.Object_Type__c = 'Attachment';
			    	
			    	newVersions.add(idMapping);
	    		}
		    	
		    	insert newVersions;
	    	}
    	    		
    		response.DISTRIBUTION_STATUS = 'TRUE';
    		
    	}catch(Exception ex){
    		
    		Database.rollback(sp);
    		
    		// Construct error message
            String errMsg = (ex.getMessage()).left(TRIM_ERROR_MSG_LENGTH);
                                         	        	
        	response.DISTRIBUTION_STATUS = 'FALSE';
        	SAPErrorMessage msg = new SAPErrorMessage();
	        msg.ERROR_MESSAGE = errMsg;
	        response.DIST_ERROR_DETAILS = new List<SAPErrorMessage>{msg};
	        
	        if(Test.isRunningTest()) throw ex;   
    	}
    	    	
    	String internalId;    	
    	if(attachmentAckToUpdate.SFDC_IDS != null && attachmentAckToUpdate.SFDC_IDS.size() > 0) internalId = attachmentAckToUpdate.SFDC_IDS[0];
    	    	
    	ut_Log.logOutboundMessage(internalId, attachmentAckToUpdate.SAP_ID, JSON.serializePretty(attachmentAckToUpdate), JSON.serializePretty(response), response.DISTRIBUTION_STATUS, 'createSFDCAttachmentAcknowledgement', 'Attachment');
    	    	
    	return response;
    }
    
     /*
     * Description      : Structure for Attachment
     */     
    global class SAPAttachment{
        
        webservice String DISTRIBUTION_STATUS;
        webservice String DIST_ERROR_DETAILS;
        
        webservice String SFDC_ID;
        webservice String NAME;
        webservice String DESCRIPTION;
        webservice String CONTENT_TYPE;
        webservice String PARENT_ID;
    }		
            
    /*
    * Description      : Structure for Attachment Acknowledgement to update x-ref table Ids
    */ 
    global class SAPAttachmentAck{
        
        webservice String SAP_ID;
        webservice String SFDC_IDS;
               
        webservice String WMTRANSACTION;                
		webservice String TIMESTAMP;
        webservice String DISTRIBUTION_STATUS;
        webservice List<SAPErrorMessage> DIST_ERROR_DETAILS;  
    }
    
    /*
    * Description      : Structure for Attachment Acknowledgement to update x-ref table Ids
    */ 
    global class SFDCAttachmentAck{

        webservice String SAP_ID;
        webservice List<String> SFDC_IDS;
                
        webservice String WMTRANSACTION;       
		webservice String TIMESTAMP;
        webservice String DISTRIBUTION_STATUS;
        webservice List<SAPErrorMessage> DIST_ERROR_DETAILS;        
    }
    
    /*
     * Description      : Structure for ServiceOrder error message
    */
    global class SAPErrorMessage{
    	
    	webservice String ERROR_TYPE;
        webservice String ERROR_ID;
        webservice String ERROR_NUMBER;
		webservice String ERROR_MESSAGE;
    }
}