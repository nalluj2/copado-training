@isTest
Public Class testCaseLink
{

static testMethod void myTest()
  {
      
		Account acc=new Account();
		acc.AccountNumber='as';
		acc.Name='MNav House Account';
		acc.Account_Country_vs__c='USA';
		acc.CurrencyIsoCode='USD';
		acc.Phone='24107954';
		acc.SAP_ID__c = '01234567';
		insert acc; 
      
	    Contact objContact=new Contact();
	    objContact.LastName='Saha';
	    objContact.FirstName='Saha';
	    objContact.AccountId = acc.id;
	    objContact.Contact_Department__c = 'Pediatric';
	    objContact.Contact_Primary_Specialty__c = 'Neurology';
	    objContact.Affiliation_To_Account__c = 'Employee';
	    objContact.Primary_Job_Title_vs__c = 'Nurse';
	    objContact.Contact_Gender__c = 'Female';
	    insert objContact; 
      
      	Asset sys=new Asset();
        sys.Name='Niha';
        sys.Serial_Nr__c='XXXXX';
        sys.recordtypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('O-Arm').getRecordTypeId(); 
        sys.Asset_Product_Type__c='O-Arm 1000';
        sys.AccountId=acc.Id;
        sys.Ownership_Status__c='EOL';	
		insert sys;
		
	    List<Case> lstOfcase=new List<Case>();
	    Case objcase1=new Case();
	    objcase1.Subject='abc';
	    objcase1.Description='abc';	    
	    objcase1.ContactId=objContact.id;
	    objcase1.AccountId=acc.id;
	    objcase1.ParentId = objcase1.id;
	    objcase1.Date_Complaint_Became_Known__c = System.today();
	    objcase1.Event_Happen_During_Surgery__c= 'No';	    
	    objcase1.AssetId = sys.id;    
	    objcase1.Case_Aborted__c= 'No';
	    objcase1.Was_Medtronic_Imaging_Aborted__c= 'No';
	    objcase1.Was_Navigation_Aborted__c= 'No';
	    objcase1.Serious_Injury__c= 'No';     
	    objcase1.RecordTypeId = clsUtil.getRecordTypeByDevName('Case', 'New_Case').Id;
	      
	    lstOfcase.add(objcase1);
      
	    Case objcase2=new Case();
	    objcase2.Subject='abc';
	    objcase2.Description='Description description description description description description description Description description description description Description description description description Description description description description';
	    objcase2.Date_Complaint_Became_Known__c = system.today();
	    objcase2.ContactId=objContact.id;
	    objcase2.AccountId=acc.id;	    
	    objcase2.Event_Happen_During_Surgery__c= 'No';	    
	    objcase2.AssetId = sys.id;    
	    objcase2.Case_Aborted__c= 'No';
	    objcase2.Was_Medtronic_Imaging_Aborted__c= 'No';
	    objcase2.Was_Navigation_Aborted__c= 'No';
	    objcase2.Serious_Injury__c= 'No';
	    objcase2.RecordTypeId = clsUtil.getRecordTypeByDevName('Case', 'New_Case').Id;
	      
	    lstOfcase.add(objcase2);
	      
	    Case objcase3=new Case();
	    objcase3.Subject='abc';
	    objcase3.Description='Description';
	    objcase3.ContactId=objContact.id;
	    objcase3.AccountId=acc.id;
	    objcase3.AssetId = sys.id;    
	    objcase3.Complaint_Case__c = 'No';
	    objcase3.Status= 'Closed';
	    objcase3.Remote_Presence_or_V_Support_Utilized__c = 'No';
	    objcase3.RecordTypeId = clsUtil.getRecordTypeByDevName('Case', 'New_Case').Id;
	      
	    lstOfcase.add(objcase3);
	    
	    //Exlude execution of OMA triggers
    	clsUtil.bDoNotExecute = true;
	      
	    insert lstOfcase;
      	
      	Test.startTest();
      	
      	objcase1.ParentId = objcase2.id;
		update objcase1;
      
	    Workorder__c WO =new Workorder__c();
	    WO.recordtypeId = Schema.SObjectType.Workorder__c.getRecordTypeInfosByName().get('O1 System Checkout').getRecordTypeId();   
	    WO.Account__c=acc.id;
	    WO.Asset__c=sys.id;
	    WO.CurrencyIsoCode='EUR';
	    WO.Bio_Med_FSE_Visit__c='No';
	    //WO.Case__c = objcase1.id;
	    WO.Date_Completed__c=system.today();
	    WO.Status__c='In Process';
	    insert WO;
    
	    ApexPages.currentPage().getParameters().put('id',objcase1.id);
	    ApexPages.StandardController controller = new ApexPages.StandardController(objcase1);    
	    
	    CaseLink testCaseLink = new CaseLink(controller);
	    
	    for(CaseLink.wrpCaseAndWorkOrder ObjWrap : testCaseLink.getCasesAndWOs()){
	    	        
	    	ObjWrap.selected=true;
	    }
	    	    
	    testCaseLink.AttacheToCase();    
     	
     	System.assert(testCaseLink.CaseList == null);
     	System.assert(testCaseLink.RecordId == null);
     	
      	Test.stopTest();
	}
}