//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 30-03-2017
//  Description      : APEX Test Class for tr_Asset and bl_Asset_Trigger
//  Change Log       : 
//		- 20/03/2019 - Bart Caelen - Replaced MEACAT by CEMA
//--------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_bl_Asset_Trigger {
	
	private static List<Asset> lstAsset;
	private static List<AssetContract__c> lstAssetContract;

	@isTest static void createTestData() {

		// Create Asset
		clsTestData_Asset.iRecord_Asset = 10;
		lstAsset = clsTestData_Asset.createAsset(false);
		for (Asset oAsset : lstAsset){
			oAsset.Service_Level__c = 'Gold';
		}
		insert lstAsset;

		// Create Asset Contract
		clsTestData_Asset.iRecord_AssetContract = 10;
		lstAssetContract = clsTestData_Asset.createAssetContract(false);
		for (AssetContract__c oAssetContract : lstAssetContract){
			oAssetContract.Service_Level__c = 'Gold';
		}
		insert lstAssetContract;

	}
	

	@isTest static void test_processAsset_ClearServiceLevel() {

		//---------------------------------------------
		// Create Test Data
		//---------------------------------------------
		createTestData();
		//---------------------------------------------


		//---------------------------------------------
		// Perform Test
		//---------------------------------------------
		Test.startTest();

		Integer i = 1;
		for (Asset oAsset : lstAsset){

			if (i == 5) break;

			oAsset.Status = 'Removed';

			i++;
		}

		update lstAsset;

		Test.stopTest();
		//---------------------------------------------


		//---------------------------------------------
		// Validate Result
		//---------------------------------------------
		lstAsset = [SELECT Id, Status, Service_Level__c FROM Asset WHERE Id = :lstAsset];
		for (Asset oAsset : lstAsset){

			if (oAsset.Status == 'Removed'){

				System.assertEquals(oAsset.Service_Level__c, null);
			
			}else{

				System.assertNotEquals(oAsset.Service_Level__c, null);

			}

		}
		//---------------------------------------------

	}


	@isTest static void test_processAssetContract_ClearServiceLevel() {

		//---------------------------------------------
		// Create Test Data
		//---------------------------------------------
		createTestData();
		//---------------------------------------------


		//---------------------------------------------
		// Perform Test
		//---------------------------------------------
		Test.startTest();

		Integer i = 1;
		for (Asset oAsset : lstAsset){

			if (i == 5) break;

			oAsset.Status = 'Removed';

			i++;
		}

		update lstAsset;

		Test.stopTest();
		//---------------------------------------------


		//---------------------------------------------
		// Validate Result
		//---------------------------------------------
		lstAssetContract = [SELECT Id, Asset__r.Status, Service_Level__c FROM AssetContract__c WHERE Id = :lstAssetContract];

		for (AssetContract__c oAssetContract : lstAssetContract){

			if (oAssetContract.Asset__r.Status == 'Removed'){

				System.assertEquals(oAssetContract.Service_Level__c, null);
			
			}else{

				System.assertNotEquals(oAssetContract.Service_Level__c, null);

			}

		}
		//---------------------------------------------

	}


	@isTest static void test_preventCreation() {

		//---------------------------------------------
		// Create Test Data
		//---------------------------------------------
        // Create Account
//        Account acc1 = new Account();
//            acc1.Name = 'TEST_triggerImplantBeforeInsert TestAccount1' ;
//            acc1.recordtypeid = clsUtil.getRecordTypeByDevName('Account', 'Competitor').Id; 
//        testAccounts.add(acc1);

		// Create Master Data		
        clsTestData_MasterData.createBusinessUnit();
		
		// Create Product
		clsTestData_Product.createProductGroup(true);
        clsTestData_Product.idRecordType_Product = clsUtil.getRecordTypeByDevName('Product2', 'MDT_Marketing_Product').Id;
        clsTestData_Product.iRecord_Product = 1;
        clsTestData_Product.createProductData_NoBU(false);

        for (Product2 oProduct : clsTestData_Product.lstProduct){
            oProduct.Consumable_Bool__c = true;
            oProduct.IsActive = true;
            oProduct.RecordTypeId = clsTestData_Product.idRecordType_Product;
            oProduct.Business_Unit_ID__c = clsTestData_MasterData.oMain_BusinessUnit.Id;
            oProduct.Product_Group__c = clsTestData_Product.oMain_ProductGroup.Id;
			//oProduct.CFN_Code_Text__c = 'CFN001';
        }
        insert clsTestData_Product.lstProduct;

		// Create CFN Mapping Asset
		List<CFN_Mapping_Asset__c> lstCFNMappingAsset = new List<CFN_Mapping_Asset__c>();
		CFN_Mapping_Asset__c oCFNMappingAsset1 = new CFN_Mapping_Asset__c();
			oCFNMappingAsset1.Name = 'CFN001';
			oCFNMappingAsset1.Product__c = clsTestData_Product.oMain_Product.Id;
			oCFNMappingAsset1.CFN_Description__c = 'CFN001';
			oCFNMappingAsset1.Asset_Name__c = 'CFN001';
		lstCFNMappingAsset.add(oCFNMappingAsset1);
		CFN_Mapping_Asset__c oCFNMappingAsset2 = new CFN_Mapping_Asset__c();
			oCFNMappingAsset2.Name = 'CFN002';
			oCFNMappingAsset2.Product__c = clsTestData_Product.oMain_Product.Id;
			oCFNMappingAsset2.CFN_Description__c = 'CFN002';
			oCFNMappingAsset2.Asset_Name__c = 'CFN002';
		lstCFNMappingAsset.add(oCFNMappingAsset2);
		CFN_Mapping_Asset__c oCFNMappingAsset3 = new CFN_Mapping_Asset__c();
			oCFNMappingAsset3.Name = 'CFN003';
			oCFNMappingAsset3.Product__c = clsTestData_Product.oMain_Product.Id;
			oCFNMappingAsset3.CFN_Description__c = 'CFN003';
			oCFNMappingAsset3.Asset_Name__c = 'CFN003';
		lstCFNMappingAsset.add(oCFNMappingAsset3);
		insert lstCFNMappingAsset;


		// Create Asset
		clsTestData_Asset.iRecord_Asset = 1;
		Asset oAsset1 = clsTestData_Asset.createAsset(false)[0];
			oAsset1.CFN_Text__c = 'CFN002';
		//---------------------------------------------


		//---------------------------------------------
		// Perform Test
		//---------------------------------------------
		Test.startTest();

		String tError = '';
		try{

			insert oAsset1;

		}catch(Exception oEX){
			tError = oEX.getMessage();
		}

		System.assert(tError.contains('You can\'t create an Asset with an CFN Number that is interfaced'));

		delete oCFNMappingAsset1;

		tError = '';
		try{

			insert oAsset1;

		}catch(Exception oEX){
			tError = oEX.getMessage();
		}

		System.assert(tError.contains('You can\'t create an Asset with an CFN Number that is interfaced'));


		delete oCFNMappingAsset2;

		tError = '';
		try{

			insert oAsset1;

		}catch(Exception oEX){
			tError = oEX.getMessage();
		}

		System.assert(String.isBlank(tError));


		Test.stopTest();
		//---------------------------------------------


		//---------------------------------------------
		// Validate Result
		//---------------------------------------------
		//---------------------------------------------		

	}


	@isTest static void test_populateCFNMarketingProduct(){


		//---------------------------------------------
		// Create Test Data
		//---------------------------------------------
		// Create Master Data
		clsTestData_MasterData.tBusinessUnitGroupName = 'MITG';
		clsTestData_MasterData.createBusinessUnitGroup();
		clsTestData_MasterData.tBusinessUnitName = 'Surgical Innovations';
        clsTestData_MasterData.createBusinessUnit();
		
		// Create Product Marketing Product
		clsTestData_Product.createProductGroup(true);
        clsTestData_Product.idRecordType_Product = clsUtil.getRecordTypeByDevName('Product2', 'MDT_Marketing_Product').Id;
        clsTestData_Product.iRecord_Product = 3;
        clsTestData_Product.createProductData_NoBU(false);

        Product2 oProduct_NOCFN = clsTestData_Product.lstProduct[0];
            oProduct_NOCFN.Consumable_Bool__c = true;
            oProduct_NOCFN.IsActive = true;
            oProduct_NOCFN.RecordTypeId = clsTestData_Product.idRecordType_Product;
            oProduct_NOCFN.Business_Unit_ID__c = clsTestData_MasterData.oMain_BusinessUnit.Id;
            oProduct_NOCFN.Product_Group__c = clsTestData_Product.oMain_ProductGroup.Id;
            oProduct_NOCFN.Region_vs__c = 'CEMA';
        Product2 oProduct_CFN1 = clsTestData_Product.lstProduct[1];
            oProduct_CFN1.Consumable_Bool__c = true;
            oProduct_CFN1.IsActive = true;
            oProduct_CFN1.RecordTypeId = clsTestData_Product.idRecordType_Product;
            oProduct_CFN1.Business_Unit_ID__c = clsTestData_MasterData.oMain_BusinessUnit.Id;
            oProduct_CFN1.Product_Group__c = clsTestData_Product.oMain_ProductGroup.Id;
			oProduct_CFN1.CFN_Code_Text__c = 'CFN001';
            oProduct_CFN1.Region_vs__c = 'CEMA';
            oProduct_CFN1.Marketing_Product_Level__c = 'CFN';
        Product2 oProduct_CFN2 = clsTestData_Product.lstProduct[2];
            oProduct_CFN2.Consumable_Bool__c = true;
            oProduct_CFN2.IsActive = true;
            oProduct_CFN2.RecordTypeId = clsTestData_Product.idRecordType_Product;
            oProduct_CFN2.Business_Unit_ID__c = clsTestData_MasterData.oMain_BusinessUnit.Id;
            oProduct_CFN2.Product_Group__c = clsTestData_Product.oMain_ProductGroup.Id;
			oProduct_CFN2.CFN_Code_Text__c = 'CFN002';
            oProduct_CFN2.Region_vs__c = 'CEMA';
            oProduct_CFN2.Marketing_Product_Level__c = 'CFN';
        insert clsTestData_Product.lstProduct;

		// Create Asset
		clsTestData_Asset.iRecord_Asset = 1;
		Asset oAsset1 = clsTestData_Asset.createAsset(false)[0];
			oAsset1.RecordTypeId = clsUtil.getRecordTypeByDevName('Asset', 'General').Id;
		//---------------------------------------------


		//---------------------------------------------
		// Test Logic
		//---------------------------------------------
		Test.startTest();

				oAsset1.CFN_Text__c = 'CFN001';
			insert oAsset1;

			List<Asset> lstAsset = [SELECT Id, CFN_Marketing_Product__c FROM Asset WHERE Id = :oAsset1.Id];
			System.assertEquals(lstAsset[0].CFN_Marketing_Product__c, oProduct_CFN1.Id);


				oAsset1.CFN_Marketing_Product__c = null;
			update oAsset1;

 			lstAsset = [SELECT Id, CFN_Marketing_Product__c FROM Asset WHERE Id = :oAsset1.Id];
			System.assertEquals(lstAsset[0].CFN_Marketing_Product__c, oProduct_CFN1.Id);


				oAsset1.CFN_Text__c = 'CFN002';
			update oAsset1;

 			lstAsset = [SELECT Id, CFN_Marketing_Product__c FROM Asset WHERE Id = :oAsset1.Id];
			System.assertEquals(lstAsset[0].CFN_Marketing_Product__c, oProduct_CFN2.Id);


				oAsset1.CFN_Text__c = 'CFN003';
			update oAsset1;

 			lstAsset = [SELECT Id, CFN_Marketing_Product__c FROM Asset WHERE Id = :oAsset1.Id];
			System.assertEquals(lstAsset[0].CFN_Marketing_Product__c, null);

		Test.stopTest();
		//---------------------------------------------


		//---------------------------------------------
		// Validate Result
		//---------------------------------------------
		//---------------------------------------------

	}

		
}
//--------------------------------------------------------------------------------------------------------------------