/*
 *      Description : This Class is the Controller for the VF Page DIB_Competitor_iBase_V2 and is based on the "old" version "controllerDIB_Competitor_iBase"
 *      Version     : 1.0
 *      Author      : Bart Caelen
 *      Date        : 201401002
 *
 *		CR-10043	christophe saenen		20160112
*/
public with sharing class ctrl_DIB_IB_Adjustment {
            
    //Data to be displayed
    public List<DIB_Class_Competitor_iBase> DIB_Comp_Class_iBases {get;set;}
        
    //List of Competitors
    public List<Account> competitorsPumps {get; set;}
    public List<Account> competitorsCGMS {get; set;}
        
    public String selectedPeriod {get; set;}
    @TestVisible private String selectedMonth;
    @TestVisible private String selectedYear;
    private String currentUserSAPId;
                 
    public Boolean showSubmitButton { get;set; }
    public String newDiabetesSegmentation {get; set;}
    
    public List<SelectOption> pageSizes{get; set;}
     
    //------------------------------------------------------------------------------------------------
    // CONSTRUCTOR 
    //------------------------------------------------------------------------------------------------
    public ctrl_DIB_IB_Adjustment() {
                        
        selectedPeriod = DIB_AllocationSharedMethods.getCurrentPeriod();      
        selectedMonth = selectedPeriod.substring(0, 2);      
        selectedYear = selectedPeriod.substring(3, 7);   
        
        showSubmitButton = DIB_Manage_Task_Status.showHideSubmitButton(selectedMonth, selectedYear); 
        					
		competitorsPumps = new List<Account>();
		competitorsCGMS = new List<Account>();
    	
    	for(Account competitor: [Select Pumps__c, Name, Id, CGMs__c From Account where RecordType.DeveloperName = 'DIB_Competitor' 
        						AND Account_Active__c = true AND Name != 'Other' ORDER BY Name]){
    	
    		if(competitor.Pumps__c == true) competitorsPumps.add(competitor);
    		if(competitor.CGMS__c == true) competitorsCGMS.add(competitor);
    	}        					
                                            
        List<Account> lstCompetitor = [SELECT Pumps__c, Name, Id, CGMs__c FROM Account WHERE RecordType.DeveloperName = 'DIB_Competitor' 
        								AND Account_Active__c = true AND Name = 'Other'];
        								
        if(lstCompetitor.size() > 0){
        
        	competitorsPumps.add(lstCompetitor[0]);
        	competitorsCGMS.add(lstCompetitor[0]);
        }
                        
        deviceFilter = 'Pumps';
        
        currentUserSAPId = DIB_AllocationSharedMethods.getCurrentUserSAPId(System.Userinfo.getUserId());    
        
        PAGE_SIZE = 50;
        
        pageSizes = new List<SelectOption>();
        pageSizes.add(new SelectOption('50', '50'));
        pageSizes.add(new SelectOption('100', '100'));
        pageSizes.add(new SelectOption('200', '200'));
        
        newDiabetesSegmentation = My_IB_Update__c.getValues('My IB Update').New_Segmentation_Link__c;        
    }
    
    
    //------------------------------------------------------------------------------------------------
    // FILTERS
    //------------------------------------------------------------------------------------------------
    
    // Device
    public String deviceFilter {get;set;}
    private List<SelectOption> deviceFilterOptions;
    // Territories
    public String territoryFilter { get;set; }
    private List<SelectOption> territoryFilterOptions;
    // Segments
    public String departmentSegment { get;set; }    
    private List<SelectOption> segmentFilterOptions;
    // Departments
    public String departmentFilter { get;set; }    
    private List<SelectOption> departmentFilterOptions;
    // Account name
    public String searchCriteria { get;set; }
        
    public List<SelectOption> getDeviceFilterOptions() {
        
        if(deviceFilterOptions == null){         
            deviceFilterOptions = new List<SelectOption>();
            deviceFilterOptions.add(new SelectOption('Pumps', 'Pumps'));
            deviceFilterOptions.add(new SelectOption('CGMS', 'CGMS'));
        }
        return deviceFilterOptions;
    }
    
    public List<SelectOption> getTerritoryFilterOptions() {
        
        if(territoryFilterOptions == null){         
            territoryFilterOptions = DIB_AllocationSharedMethods.getTerritoryFilterOptions();
        }
        return territoryFilterOptions;
    }
    
    public List<SelectOption> getSegmentFilterOptions() {
        
        if(segmentFilterOptions == null){
            segmentFilterOptions = DIB_AllocationSharedMethods.getSegmentFilterOptions();            
        }
        
        return segmentFilterOptions;
    } 
    
    public List<SelectOption> getDepartmentFilterOptions() {
        
        if(departmentFilterOptions == null){
            departmentFilterOptions = DIB_AllocationSharedMethods.getDepartmentFilterOptions();
            departmentFilterOptions[0] = new Selectoption('', 'All Departments');
        }
        
        return departmentFilterOptions;
    }
        
    //------------------------------------------------------------------------------------------------
    // PAGINATION
    //------------------------------------------------------------------------------------------------
    
    //List controller for pagination
    public ApexPages.StandardSetController con {get; set;}
    public Integer PAGE_SIZE {get; set;} 
    public Integer pageNr { get;set; }
    public Integer nrOfItems { get;set; }
    public Integer totalPages { get;set; }
    public String selectedPage { get;set; } 
    public String selectedPageSize {get;set;}
            
    public List<SelectOption> pages {
        get{
            List<SelectOption> options = new List<SelectOption>();
            for (Integer i=1 ; i <= this.totalPages ; i++){
                options.add(new SelectOption(''+i,'' + i));
            }
            return options;
        }
        set;
    }
            
    public Boolean hasPrevious {
        get {
            return con.getHasPrevious();
        }
        set;
    }
    
    public Boolean hasNext {
        get {
            return con.getHasNext();
        }
        set;
    }
        
    public void changePage(){
        
        pageNr = Integer.valueOf(selectedPage) ; 
        con.setPageNumber(pageNr);  
        drawComp_iBase(); 
    }
    
    public void first() {
        
        con.first();
        pageNr = 1 ; 
        selectedPage = '1' ;
         
        drawComp_iBase();
    }
 
    public void last() {
        
        con.last();
        pageNr = con.getPageNumber();
        selectedPage = String.valueOf(pageNr) ;
        drawComp_iBase();
    }
 
    public void previous() {
        
        pageNr-- ; 
        con.setPageNumber(pageNr);  
        selectedPage = String.valueOf(pageNr) ; 
        drawComp_iBase(); 
    }
 
    public void next() {
        
        pageNr++;
        con.setPageNumber(pageNr);  
        selectedPage = String.valueOf(pageNr) ; 
        drawComp_iBase();
    }
    
    public void setPageSize(){
    	
    	PAGE_SIZE = Integer.valueOf(selectedPageSize);
    	
    	con.setPageSize(PAGE_SIZE);
                          
        totalPages = Integer.valueOf(((Decimal) nrOfItems).divide(PAGE_SIZE, 0, System.RoundingMode.UP));
        pageNr = 1 ; 
        selectedPage = '1' ;
        
        con.setPageNumber(pageNr);  
        drawComp_iBase(); 
    }
                    
    //------------------------------------------------------------------------------------------------
    // DATA PREPARATION
    //------------------------------------------------------------------------------------------------
    
    public void displayCompetitors()    {
        
        // Called each time The user clicks in the dysplay button : 
        loadSetController();    
        drawComp_iBase();
    }

    // Controller 
    public void loadSetController(){
                
        //Account Name filter       
        String searchCriteriaEntered;       
        if(searchCriteria != null && searchCriteria != ''){
             
            searchCriteriaEntered = searchCriteria;
        
            if(searchCriteriaEntered.startsWith('%') == false ) searchCriteriaEntered = '%' + searchCriteriaEntered;
            if(searchCriteriaEntered.endsWith('%') == false ) searchCriteriaEntered += '%';
        }
        
        //Segment filter
        String departmentSegmentEntered;          
        if(departmentSegment != null && departmentSegment != ''){
            
            departmentSegmentEntered = '%' + departmentSegment + '%';           
        }

        //Get filtered accounts for terrotory. (Return null if the filter is not used)
        Set <Id> filteredAccountIds = DIB_AllocationSharedMethods.getFilteredAccounts(this.territoryFilter);

        String tSOQL = '';
        tSOQL += 'SELECT';
            tSOQL += ' Id';
            tSOQL += ' , Department_ID__r.Name, Department_ID__c, Department_Master_Name__c';
            tSOQL += ' , Account__c, Account__r.Id, Account__r.isSales_Force_Account__c, Account__r.Name, Account__r.isPersonAccount';
            tSOQL += ' , T1_Patients_No__c, MDT_Installed_Base_Pumps__c, MDT_Installed_Base_CGMS__c';                                
        tSOQL += ' FROM';
            tSOQL += ' DIB_Department__c ';
        tSOQL += ' WHERE';
            tSOQL += ' Account__r.isSales_Force_Account__c = true ';            
            tSOQL += ' AND Account__r.Account_Active__c = true';
            tSOQL += ' AND Active__c = true';
            //Account Name filter
            if(searchCriteriaEntered != null) tSOQL += ' AND Account__r.Name like :searchCriteriaEntered';
            if(departmentFilter != null) tSOQL += ' AND Department_ID__c = :departmentFilter' ;
            //Segment filter
            if(departmentSegmentEntered != null){
                tSOQL += ' AND (';
                tSOQL += ' DiB_Segment__c like :departmentSegmentEntered';
                tSOQL += ' OR DiB_Segment__c = \'Unknown\'';
                tSOQL += ' )';
            }
            //Territory filter
            if(filteredAccountIds != null){
                tSOQL += ' AND Account__c in: filteredAccountIds';
            }
        tSOQL += ' ORDER BY Account__r.Name, Account__c, Department_ID__r.Name';

        con = new ApexPages.StandardSetController(Database.getQueryLocator(tSOQL));         
        con.setPageSize(PAGE_SIZE);
                    
        nrOfItems = con.getResultSize() ;           
        totalPages = Integer.valueOf(((Decimal) nrOfItems).divide(PAGE_SIZE, 0, System.RoundingMode.UP));
        pageNr = 1 ; 
        selectedPage = '1' ;
    }
        
    private void drawComp_iBase(){
        
        List<DIB_Department__c> lstDIBDepartment = (List<DIB_Department__c>) con.getRecords();
        
        //List of data to be displayed
        DIB_Comp_Class_iBases = new List<DIB_Class_Competitor_iBase>();
                
        Map<String, DIB_Competitor_iBase__c> mapDIB_Comp_iBase_Stored = new Map<String, DIB_Competitor_iBase__c>();
        
        // get the chosen Department ;      
        for(DIB_Competitor_iBase__c DIB_Comp_iBase_Stored : [SELECT Id, MDT__c, Account_Segmentation__c, Current_IB__c, IB_Change__c, 
                    											Fiscal_year__c, Fiscal_Month__c, Competitor_Account_ID__c, Competitor_Name__c, 
                    											Competitor_Account_ID__r.Id, Competitor_Account_ID__r.Name, Type__c,
                    											Sales_Force_Account__c, Sales_Force_Account__r.Name, SYSTEM_FIELD_Month_IB__c, System_Field_Comp_Name__c
												                FROM DIB_Competitor_iBase__c WHERE Fiscal_Month__c  =: selectedMonth AND Fiscal_Year__c =: selectedYear 
                    											AND Account_Segmentation__c in: lstDIBDepartment 
                												ORDER BY Sales_Force_Account__c, Competitor_Name__c]){
        	
        	if(DIB_Comp_iBase_Stored.MDT__c == true) mapDIB_Comp_iBase_Stored.put(DIB_Comp_iBase_Stored.Type__c + ':' + DIB_Comp_iBase_Stored.Account_Segmentation__c, DIB_Comp_iBase_Stored);        	       	
        	else mapDIB_Comp_iBase_Stored.put(DIB_Comp_iBase_Stored.Type__c + ':' + DIB_Comp_iBase_Stored.Account_Segmentation__c + ':' + DIB_Comp_iBase_Stored.Competitor_Account_ID__c, DIB_Comp_iBase_Stored);
        }                
        
        //Get the stored Competitors iBase data        
        for (DIB_Department__c  oDIBDepartment : lstDIBDepartment) {
            
            Account currentAccount = oDIBDepartment.Account__r;
            
            DIB_Class_Competitor_iBase curDIB_Class_Competitor_iBase = new DIB_Class_Competitor_iBase(currentAccount);
            curDIB_Class_Competitor_iBase.dibDepartment = oDIBDepartment;
            
            DIB_Competitor_iBase__c mdt_iBase_Pumps = mapDIB_Comp_iBase_Stored.get('Pumps:' + oDIBDepartment.Id);
            DIB_Competitor_iBase__c mdt_iBase_CGMS = mapDIB_Comp_iBase_Stored.get('CGMS:' + oDIBDepartment.Id);
            
            // Create a "dummy" MDT Line as a default one
			if(mdt_iBase_Pumps == null){
				
				mdt_iBase_Pumps = new DIB_Competitor_iBase__c();
                mdt_iBase_Pumps.MDT__c = true;
                mdt_iBase_Pumps.Sales_Force_Account__c = currentAccount.Id;
                mdt_iBase_Pumps.Fiscal_Month__c = selectedMonth ; 
                mdt_iBase_Pumps.Fiscal_year__c = selectedYear ;                 
                mdt_iBase_Pumps.IB_Change__c = 0;                
                mdt_iBase_Pumps.Type__c = 'Pumps';
                mdt_iBase_Pumps.Account_Segmentation__c = oDIBDepartment.Id;                
			}
			            
            curDIB_Class_Competitor_iBase.oDIB_MDT_iBase = mdt_iBase_Pumps; 
            
            if(mdt_iBase_CGMS == null){
				
				mdt_iBase_CGMS = new DIB_Competitor_iBase__c();
                mdt_iBase_CGMS.MDT__c = true;
                mdt_iBase_CGMS.Sales_Force_Account__c = currentAccount.Id;
                mdt_iBase_CGMS.Fiscal_Month__c = selectedMonth ; 
                mdt_iBase_CGMS.Fiscal_year__c = selectedYear ;                 
                mdt_iBase_CGMS.IB_Change__c = 0;                
                mdt_iBase_CGMS.Type__c = 'CGMS';
                mdt_iBase_CGMS.Account_Segmentation__c = oDIBDepartment.Id;                
			}
			            
            curDIB_Class_Competitor_iBase.oDIB_MDT_iBase_CGMS = mdt_iBase_CGMS;           
            
            //Pumps
            curDIB_Class_Competitor_iBase.decChange_MDTInstalledBasePumps_NewTotal = clsUtil.isDecimalNull(oDIBDepartment.MDT_Installed_Base_Pumps__c, 0) +  clsUtil.isDecimalNull(mdt_iBase_Pumps.IB_Change__c, 0);
            //CGMS
            curDIB_Class_Competitor_iBase.decChange_MDTInstalledBaseCGMS_NewTotal = clsUtil.isDecimalNull(oDIBDepartment.MDT_Installed_Base_CGMS__c, 0) +  clsUtil.isDecimalNull(mdt_iBase_CGMS.IB_Change__c, 0);

			List<DIB_Competitor_iBase__c> comp_iBase_Pumps = curDIB_Class_Competitor_iBase.comp_iBase;
			                        
            for(Account currentCompetitor : competitorsPumps){
            	
                DIB_Competitor_iBase__c oDIBCompetitoriBase = mapDIB_Comp_iBase_Stored.get('Pumps:' + oDIBDepartment.Id + ':' + currentCompetitor.Id);
                
                // create a "dummy" Competitor Line as a default one for each Competitor                 
                if(oDIBCompetitoriBase == null){
                
                	oDIBCompetitoriBase = new DIB_Competitor_iBase__c();
                    oDIBCompetitoriBase.Competitor_Account_ID__c = currentCompetitor.Id;
                    oDIBCompetitoriBase.Sales_Force_Account__c = currentAccount.Id;
                    oDIBCompetitoriBase.IB_Change__c = 0;
                    oDIBCompetitoriBase.Fiscal_Month__c = selectedMonth ; 
                    oDIBCompetitoriBase.Fiscal_year__c = selectedYear ;                    
                    oDIBCompetitoriBase.Type__c = 'Pumps';                    
                    oDIBCompetitoriBase.System_Field_Comp_Name__c = currentCompetitor.Name ; 
                    oDIBCompetitoriBase.Account_Segmentation__c = oDIBDepartment.Id;
                }
                
                comp_iBase_Pumps.add(oDIBCompetitoriBase);
                curDIB_Class_Competitor_iBase.mapComp_Error.put(oDIBCompetitoriBase.System_Field_Comp_Name__c, false);
            }
            
            List<DIB_Competitor_iBase__c> comp_iBase_CGMS = curDIB_Class_Competitor_iBase.comp_iBase_CGMS;
            
            for(Account currentCompetitor : competitorsCGMS){
            	
                DIB_Competitor_iBase__c oDIBCompetitoriBase = mapDIB_Comp_iBase_Stored.get('CGMS:' + oDIBDepartment.Id + ':' + currentCompetitor.Id);
                
                // create a "dummy" Competitor Line as a default one for each Competitor                 
                if(oDIBCompetitoriBase == null){
                
                	oDIBCompetitoriBase = new DIB_Competitor_iBase__c();
                    oDIBCompetitoriBase.Competitor_Account_ID__c = currentCompetitor.Id;
                    oDIBCompetitoriBase.Sales_Force_Account__c = currentAccount.Id;
                    oDIBCompetitoriBase.IB_Change__c = 0;
                    oDIBCompetitoriBase.Fiscal_Month__c = selectedMonth ; 
                    oDIBCompetitoriBase.Fiscal_year__c = selectedYear ;                    
                    oDIBCompetitoriBase.Type__c = 'CGMS';                    
                    oDIBCompetitoriBase.System_Field_Comp_Name__c = currentCompetitor.Name ; 
                    oDIBCompetitoriBase.Account_Segmentation__c = oDIBDepartment.Id;
                }
                
                comp_iBase_CGMS.add(oDIBCompetitoriBase);
                curDIB_Class_Competitor_iBase.mapComp_Error_CGMS.put(oDIBCompetitoriBase.System_Field_Comp_Name__c, false);
            }
            
            DIB_Comp_Class_iBases.add(curDIB_Class_Competitor_iBase);           
        }
                        
        // Calculate Total
        calculateTotalCompetitors();
    }
    
    
    // Change Request : Calculate Current IB up to the selected month : 
    private void calculateTotalCompetitors(){
        
        List<DIB_Department__c> lstDIBDepartment = (List<DIB_Department__c>) con.getRecords();
        
        Map<Id, DIB_Department__c> deparmentDetails = new Map<Id, DIB_Department__c>([Select Id, Total_Roche_IB_Pumps__c, Total_Roche_IB_CGMS__c, 
        																				Total_Animas_IB_Pumps__c, Total_Others_IB_Pumps__c, Total_Other_IB_CGMS__c, 
        																				Total_Patch_IB_Pumps__c, Total_Abott_IB_CGMS__c, Total_Dexcom_IB_CGMS__c, 
        																				Total_Cellnovo_IB_Pumps__c, Ypsomed_IB_Pumps__c,
        																				Industry_IB_Pumps__c, Industry_IB_CGMS__c 
        																				from DIB_Department__c where ID IN :lstDIBDepartment]);
                                                        
        for (DIB_Class_Competitor_iBase iBaseComps: DIB_Comp_Class_iBases) {
                        
            DIB_Department__c oDIBDepartment = deparmentDetails.get(iBaseComps.dibDepartment.Id);
            
            iBaseComps.industryIB_Pumps = oDIBDepartment.Industry_IB_Pumps__c;
            iBaseComps.industryIB_CGMS = oDIBDepartment.Industry_IB_CGMS__c;
                       
            for (DIB_Competitor_iBase__c currComp : iBaseComps.Comp_iBase ){
                
                Double total = 0;
                                
                if (currComp.SYSTEM_FIELD_Comp_Name__c != null) {
					                    
                    if(currComp.SYSTEM_FIELD_Comp_Name__c.equals('Cellnovo')) total = oDIBDepartment.Total_Cellnovo_IB_Pumps__c;                    
                    else if (currComp.SYSTEM_FIELD_Comp_Name__c.equals('Roche')) total = oDIBDepartment.Total_Roche_IB_Pumps__c; 
                    else if (currComp.SYSTEM_FIELD_Comp_Name__c.equals('Animas')) total = oDIBDepartment.Total_Animas_IB_Pumps__c;
                    else if (currComp.SYSTEM_FIELD_Comp_Name__c.equals('Ypsomed')) total = oDIBDepartment.Ypsomed_IB_Pumps__c;
					else if (currComp.SYSTEM_FIELD_Comp_Name__c.equals('Other')) total = oDIBDepartment.Total_Others_IB_Pumps__c;
                    else if (currComp.SYSTEM_FIELD_Comp_Name__c.equals('Omnipod')) total = oDIBDepartment.Total_Patch_IB_Pumps__c;                                                
                }
                
                currComp.Current_IB__c = total ;
            }
            
            for (DIB_Competitor_iBase__c currComp : iBaseComps.Comp_iBase_CGMS ){
                
                Double total = 0 ; 
                
                if (currComp.SYSTEM_FIELD_Comp_Name__c != null) {
					
                    
                    if (currComp.SYSTEM_FIELD_Comp_Name__c.equals('Roche Eversense')) total = oDIBDepartment.Total_Roche_IB_CGMS__c;
                    if (currComp.SYSTEM_FIELD_Comp_Name__c.equals('Other')) total = oDIBDepartment.Total_Other_IB_CGMS__c;
                    if (currComp.SYSTEM_FIELD_Comp_Name__c.equals('Dexcom')) total = oDIBDepartment.Total_Dexcom_IB_CGMS__c;
                    if (currComp.SYSTEM_FIELD_Comp_Name__c.equals('Abbott Libre')) total = oDIBDepartment.Total_Abott_IB_CGMS__c;                                               
                }
                
                currComp.Current_IB__c = total ;
            }
        }
    }

    //------------------------------------------------------------------------------------------------
    // ACTIONS
    //------------------------------------------------------------------------------------------------

    public void save(){
                
        // Get all the items : 
        List<DIB_Competitor_iBase__c> all_Competitors_To_Upsert = new List<DIB_Competitor_iBase__c>{} ;
        List<DIB_Department__c> lstDIBDepartment_Update = new List<DIB_Department__c>();
        List<DIB_Competitor_iBase__c> lstDIB_MDT_iBase = new List<DIB_Competitor_iBase__c>();       

        Boolean bUpdateDIBDepartment = false;
        Boolean bError = false;
        Boolean bUpsertDIB_MDT_iBase_Pumps = false;
        Boolean bUpsertDIB_MDT_iBase_CGMS = false;

        // Manage the different Lines If they can be updated or not. And if can,
        // - Incremeent or decrement the current Install Base. 
        for (DIB_Class_Competitor_iBase oDIB_Class_Comp_iBase : DIB_Comp_Class_iBases) { 
        	          
            oDIB_Class_Comp_iBase.bError_MDTInstalledBasePumps = false;
            oDIB_Class_Comp_iBase.bError_MDTInstalledBaseCGMS = false;
			
			System.debug('Nb Patients ' + oDIB_Class_Comp_iBase.SFAccount.System_Account__c + ' - ' + oDIB_Class_Comp_iBase.dibDepartment.Department_Master_Name__c + ' : ' + oDIB_Class_Comp_iBase.decChange_T1PatientsNo);
			
            if (oDIB_Class_Comp_iBase.decChange_T1PatientsNo != null){
                if ((clsUtil.isDecimalNull(oDIB_Class_Comp_iBase.dibDepartment.T1_Patients_No__c, 0) + oDIB_Class_Comp_iBase.decChange_T1PatientsNo) < 0){
                    oDIB_Class_Comp_iBase.dibDepartment.T1_Patients_No__c.addError('CSII Potential cannot be negative.');
                    oDIB_Class_Comp_iBase.bError_T1PatientsNo = true;
                    bError = true;
                }
            }
            if (oDIB_Class_Comp_iBase.decChange_MDTInstalledBasePumps != null){
                if ( (clsUtil.isDecimalNull(oDIB_Class_Comp_iBase.dibDepartment.MDT_Installed_Base_Pumps__c, 0) + oDIB_Class_Comp_iBase.decChange_MDTInstalledBasePumps + clsUtil.isDecimalNull(oDIB_Class_Comp_iBase.oDIB_MDT_iBase.IB_Change__c, 0) )  < 0){
                    oDIB_Class_Comp_iBase.oDIB_MDT_iBase.IB_Change__c.addError('MDT IB cannot be negative.');
                    oDIB_Class_Comp_iBase.bError_MDTInstalledBasePumps = true;
                    bError = true;
                }
            }

            if (oDIB_Class_Comp_iBase.decChange_MDTInstalledBaseCGMS != null){
                if ( (clsUtil.isDecimalNull(oDIB_Class_Comp_iBase.dibDepartment.MDT_Installed_Base_CGMS__c, 0) + oDIB_Class_Comp_iBase.decChange_MDTInstalledBaseCGMS + clsUtil.isDecimalNull(oDIB_Class_Comp_iBase.oDIB_MDT_iBase_CGMS.IB_Change__c, 0) ) < 0){
                    oDIB_Class_Comp_iBase.oDIB_MDT_iBase.IB_Change__c.addError('MDT IB cannot be negative.');
                    oDIB_Class_Comp_iBase.bError_MDTInstalledBaseCGMS = true;
                    bError = true;
                }
            }           

            List<DIB_Competitor_iBase__c> lineComps = new List<DIB_Competitor_iBase__c>();
            lineComps.addAll(oDIB_Class_Comp_iBase.Comp_iBase);
            lineComps.addAll(oDIB_Class_Comp_iBase.Comp_iBase_CGMS);
            
            for (DIB_Competitor_iBase__c comp : lineComps ){
                // only upsert when the value is not empty 
                if (comp.SYSTEM_FIELD_Month_IB__c != null){
                    
                    if ( clsUtil.isDecimalNull(comp.SYSTEM_FIELD_Month_IB__c, 0) + clsUtil.isDecimalNull(comp.Current_IB__c, 0) < 0){
                        comp.SYSTEM_FIELD_Month_IB__c.addError('Current IB cannot be negative.');
                        bError = true;
                        
                        if(comp.Type__c == 'Pumps') oDIB_Class_Comp_iBase.mapComp_Error.put(comp.System_Field_Comp_Name__c, true);
                        else if(comp.Type__c == 'CGMS') oDIB_Class_Comp_iBase.mapComp_Error_CGMS.put(comp.System_Field_Comp_Name__c, true);
                    }
                }           
            }
        }

        if (!bError){

            // There is no Error - so clear the needed values before saving
            for (DIB_Class_Competitor_iBase oDIB_Class_Comp_iBase : DIB_Comp_Class_iBases) {
                
                bUpdateDIBDepartment = false;
                bUpsertDIB_MDT_iBase_Pumps = false;
                bUpsertDIB_MDT_iBase_CGMS = false;
                
                if(oDIB_Class_Comp_iBase.decChange_T1PatientsNo != null){
                    oDIB_Class_Comp_iBase.dibDepartment.T1_Patients_No__c = clsUtil.isDecimalNull(oDIB_Class_Comp_iBase.dibDepartment.T1_Patients_No__c, 0) + oDIB_Class_Comp_iBase.decChange_T1PatientsNo;
                    oDIB_Class_Comp_iBase.decChange_T1PatientsNo = null;
                    bUpdateDIBDepartment = true;
                    oDIB_Class_Comp_iBase.bError_T1PatientsNo = false;
                }
                
                if(oDIB_Class_Comp_iBase.decChange_MDTInstalledBasePumps != null){
                    
                    if(oDIB_Class_Comp_iBase.decChange_MDTInstalledBasePumps != 0){
                    	
                    	bUpsertDIB_MDT_iBase_Pumps = true;
                    	oDIB_Class_Comp_iBase.oDIB_MDT_iBase.IB_Change__c = clsUtil.isDecimalNull(oDIB_Class_Comp_iBase.oDIB_MDT_iBase.IB_Change__c, 0) + oDIB_Class_Comp_iBase.decChange_MDTInstalledBasePumps;
                    	//oDIB_Class_Comp_iBase.oDIB_MDT_iBase.SYSTEM_FIELD_Month_IB__c = clsUtil.isDecimalNull(oDIB_Class_Comp_iBase.oDIB_MDT_iBase.SYSTEM_FIELD_Month_IB__c, 0) + oDIB_Class_Comp_iBase.decChange_MDTInstalledBasePumps;
                    	oDIB_Class_Comp_iBase.decChange_MDTInstalledBasePumps_NewTotal = clsUtil.isDecimalNull(oDIB_Class_Comp_iBase.dibDepartment.MDT_Installed_Base_Pumps__c, 0) + clsUtil.isDecimalNull(oDIB_Class_Comp_iBase.oDIB_MDT_iBase.IB_Change__c, 0);       //-BC - 20150609 - CR-8901
                    }
                    oDIB_Class_Comp_iBase.decChange_MDTInstalledBasePumps = null;
                }

                if (oDIB_Class_Comp_iBase.decChange_MDTInstalledBaseCGMS != null){
                
                	if(oDIB_Class_Comp_iBase.decChange_MDTInstalledBaseCGMS != 0){
            	
            	        bUpsertDIB_MDT_iBase_CGMS = true;
        	            oDIB_Class_Comp_iBase.oDIB_MDT_iBase_CGMS.IB_Change__c = clsUtil.isDecimalNull(oDIB_Class_Comp_iBase.oDIB_MDT_iBase_CGMS.IB_Change__c, 0) + oDIB_Class_Comp_iBase.decChange_MDTInstalledBaseCGMS;
    	                //oDIB_Class_Comp_iBase.oDIB_MDT_iBase.SYSTEM_FIELD_Month_IB__c = clsUtil.isDecimalNull(oDIB_Class_Comp_iBase.oDIB_MDT_iBase.SYSTEM_FIELD_Month_IB__c, 0) + oDIB_Class_Comp_iBase.decChange_MDTInstalledBaseCGMS;
	                    oDIB_Class_Comp_iBase.decChange_MDTInstalledBaseCGMS_NewTotal = clsUtil.isDecimalNull(oDIB_Class_Comp_iBase.dibDepartment.MDT_Installed_Base_CGMS__c, 0) + clsUtil.isDecimalNull(oDIB_Class_Comp_iBase.oDIB_MDT_iBase_CGMS.IB_Change__c, 0);         //-BC - 20150609 - CR-8901
                	}
                    oDIB_Class_Comp_iBase.decChange_MDTInstalledBaseCGMS = null;
                }
                
                //if ( (oDIB_Class_Comp_iBase.dibDepartment.Active__c != oDIB_Class_Comp_iBase.bDIBDepartment_Active_OldValue) || (oDIB_Class_Comp_iBase.dibDepartment.NPNP__c != oDIB_Class_Comp_iBase.tDIBDepartment_CompetitiveVSMDTAccount_OldValue) ){
                //    bUpdateDIBDepartment = true;
                //}
                if (bUpdateDIBDepartment){
                    lstDIBDepartment_Update.add(oDIB_Class_Comp_iBase.dibDepartment);
                }
        
                if (bUpsertDIB_MDT_iBase_Pumps){
                    lstDIB_MDT_iBase.add(oDIB_Class_Comp_iBase.oDIB_MDT_iBase);
                }
                
                if (bUpsertDIB_MDT_iBase_CGMS){
                    lstDIB_MDT_iBase.add(oDIB_Class_Comp_iBase.oDIB_MDT_iBase_CGMS);
                }

                List<DIB_Competitor_iBase__c> lineComps = new List<DIB_Competitor_iBase__c>();
                lineComps.addAll(oDIB_Class_Comp_iBase.Comp_iBase);
                lineComps.addAll(oDIB_Class_Comp_iBase.Comp_iBase_CGMS);
                
                for (DIB_Competitor_iBase__c comp : lineComps ){
                    
                    // only upsert when the value is not empty 
                    if (comp.SYSTEM_FIELD_Month_IB__c != null){
                        comp.IB_Change__c += comp.SYSTEM_FIELD_Month_IB__c ;                    
                        //comp.Current_IB__c += comp.SYSTEM_FIELD_Month_IB__c ;
                        comp.Current_IB__c = null;
                        comp.SYSTEM_FIELD_Month_IB__c = null ;                        
                        all_Competitors_To_Upsert.add(comp);
                        
                        if(comp.Type__c == 'Pumps') oDIB_Class_Comp_iBase.mapComp_Error.put(comp.System_Field_Comp_Name__c, false);
                        else if(comp.Type__c == 'CGMS') oDIB_Class_Comp_iBase.mapComp_Error_CGMS.put(comp.System_Field_Comp_Name__c, false);
                    }           
                }
            }

            try{
                update lstDIBDepartment_Update;
            }catch(DmlException oEX){
                clsUtil.debug('Error in ctrl_DIB_ID_Adjustment.save on line ' + oEX.getLineNumber() + ': ' + oEX.getMessage());
                ApexPages.addMessages(oEX); 
            } 
            try{
                upsert lstDIB_MDT_iBase;
            }catch(DmlException oEX){
                clsUtil.debug('Error in ctrl_DIB_ID_Adjustment.save on line ' + oEX.getLineNumber() + ': ' + oEX.getMessage());
                ApexPages.addMessages(oEX); 
            } 
            try{
                upsert all_Competitors_To_Upsert ; 
                // Calcualte total : 
                calculateTotalCompetitors();
            }catch(DmlException oEX){
                clsUtil.debug('Error in ctrl_DIB_ID_Adjustment.save on line ' + oEX.getLineNumber() + ': ' + oEX.getMessage());
                ApexPages.addMessages(oEX); 
            } 
            //-BC - 20150609 - CR-8901 - START
            // Bugfix - if you don't save the Standard Set Controller and you update a value + press save + goto next/previous page = error
            try{
                con.save();
            }catch(DmlException oEX){
                clsUtil.debug('Error in ctrl_DIB_ID_Adjustment.save on line ' + oEX.getLineNumber() + ': ' + oEX.getMessage());
                ApexPages.addMessages(oEX); 
            } 
            //-BC - 20150609 - CR-8901 - STOP
        }
    }
    
     // Submit button : Submit current Month & Year and all accounts for the current user : 
    public void SubmitcurrentPeriod(){
        if (DIB_Manage_Task_Status.submitCompetitor(selectedMonth, selectedYear)) {         
            ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.Confirm, FinalConstants.DiB_submitResultSuccess));
        }
        else {
            ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.Error, FinalConstants.DiB_submitResultFailure));            
        }
    }        
}