public class bl_Contact_Trigger {
	
	// When a Contact is created under a Master ShipTo we change it to the related Sold To and create a secondary relationship to the Master ShiTo (in after trigger logic)
    public static void handleMasterShipToAccount(List<Contact> contacts){
    	
    	Set<Id> accIds = new Set<Id>();    	
    	for(Contact cnt : contacts) if(cnt.AccountId != null) accIds.add(cnt.AccountId);
    	
    	Map<Id, Account> accountMap = new Map<Id, Account>(
    		[Select Id, SAP_Account_Type__c, SAP_Search_Term1__c, SAP_Search_Term2__c,
    		 (Select Affiliation_To_Account__c from Affiliations__r where RecordType.DeveloperName = 'A2A' AND Affiliation_Type__c = 'Master Ship To/Sold To' AND Affiliation_Start_Date__c <= TODAY AND (Affiliation_End_Date__c = null OR Affiliation_End_Date__c >= TODAY ))
    		 from Account where Id IN :accIds]
    	);
    	
    	for(Contact cnt : contacts){
    		
    		if(cnt.AccountId == null) continue; // Null check needed for Contacts like Portal Users where there is no Account required.
    		
    		Account relAcc = accountMap.get(cnt.AccountId);
    		
    		if(relAcc.SAP_Account_Type__c == 'Ship to Party' && relAcc.SAP_Search_Term1__c == 'MASTER' && relAcc.SAP_Search_Term2__c == 'SHIP TO'){
    			
    			//By design, there will be only ONE Sold To for each Master Ship To 
    			if(relAcc.Affiliations__r.size() > 0){
    				
    				Id soldToId = relAcc.Affiliations__r[0].Affiliation_To_Account__c;
    				
    				// Switch the Master ShipTo for the Sold To and Store the original Master ShipTo for reference in the after trigger    				
    				cnt.AccountId = soldToId;
    				cnt.Master_ShipTo__c = relAcc.Id;
    			}
    		}
    	}
    }
    
    // Create a relationship (not primary) between the Contact and the Master Ship To originally selected
    public static void createMasterShipToRelationship(List<Contact> contacts){
    	
    	List<Affiliation__c> relToCreate = new List<Affiliation__c>();
    	
    	for(Contact cnt : contacts){
    		
    		if(cnt.Master_ShipTo__c != null){
    			
    			Affiliation__c rel = new Affiliation__c();
    			rel.Affiliation_From_Contact__c = cnt.Id ; 
	            rel.Affiliation_To_Account__c = cnt.Master_ShipTo__c ; 
	            rel.Affiliation_Primary__c = false ; 
	            rel.Affiliation_Start_Date__c = System.today();
	            rel.Affiliation_Type__c = cnt.Affiliation_To_Account__c ;
	            rel.Affiliation_Position_In_Account__c = cnt.Primary_Job_Title_vs__c ;
				rel.Job_Title_Description__c = cnt.Job_Title_Description__c;
	            rel.Department_Description__c = cnt.Department_Description__c;
	            rel.Buying_influence_role__c = cnt.Buying_influence_role__c;
	            rel.Degree_of_Influence__c = cnt.Degree_of_Influence__c;
	            rel.Office_Phone__c = cnt.Phone ;
	            rel.Department__c = cnt.Contact_Department__c;	                        
	            rel.RecordTypeId = FinalConstants.recordTypeIdC2A ;
	            
	            relToCreate.add(rel); 	
    		}    		
    	}
    	
    	if(relToCreate.size() > 0) insert relToCreate;
    }
    
    private static User currentUser {
    	
    	get{
    		
    		if(currentUser == null){
    			
    			currentUser = [Select Id, User_Business_Unit_vs__c, Company_Code_Text__c, Primary_sBU__c from User where Id = :UserInfo.getUserId()];
    		}
    		
    		return currentUser;
    	}
    	
    	set;
    }
    
    public static void setBUContactFlags(List<Contact> triggerNew){
    	
    	if (bl_Trigger_Deactivation.isTriggerDeactivated('cnt_setBUContactFlags')) return;
    	
    	for(Contact cnt : triggerNew){
    		
            if (currentUser.User_Business_Unit_vs__c == 'Vascular') cnt.Vascular__c = true;   
            else if (currentUser.User_Business_Unit_vs__c == 'CS & SH') cnt.CS_SH__c = true;   
            else if (currentUser.User_Business_Unit_vs__c == 'Cranial Spinal') cnt.Cranial_and_Spinal__c = true;
    		else if (currentUser.User_Business_Unit_vs__c == 'CRHF') cnt.CRHF__c = true;
    		else if (currentUser.User_Business_Unit_vs__c == 'Diabetes') cnt.Diabetes__c = true;
    		else if (currentUser.User_Business_Unit_vs__c == 'Gastrointestinal & Hepatology') cnt.Early_Technologies__c = true;
            else if (currentUser.User_Business_Unit_vs__c == 'ENT') cnt.ENT__c = true;
            else if (currentUser.User_Business_Unit_vs__c == 'IHS') cnt.IHS__c = true;
            else if (currentUser.User_Business_Unit_vs__c == 'Neurovascular') cnt.Neurovascular__c = true;
            else if (currentUser.User_Business_Unit_vs__c == 'Pain') cnt.Pain__c = true;
    		else if (currentUser.User_Business_Unit_vs__c == 'Renal Care Solutions') cnt.Renal_Care_Solutions__c = true;
    		else if (currentUser.User_Business_Unit_vs__c == 'Respiratory & Monitoring Solutions') cnt.Patient_Monitoring_Recovery__c = true;
            else if (currentUser.User_Business_Unit_vs__c == 'Pelvic Health') cnt.Pelvic_Health__c = true;
            else if (currentUser.User_Business_Unit_vs__c == 'RTG IHS') cnt.RTG_IHS__c = true;
    		else if (currentUser.User_Business_Unit_vs__c == 'Surgical Innovations') cnt.Surgical_Innovations__c = true;
    		else if (currentUser.User_Business_Unit_vs__c == 'Robotics') cnt.Robotics__c = true;

    	}    	

    }   
    
    public static void checkBUContactFlags(List<Contact> triggerNew){
    	
    	if (bl_Trigger_Deactivation.isTriggerDeactivated('cnt_checkBUContactFlags')) return;
    	
    	for (Contact cnt : triggerNew){

            // BU : Vascular
            if (cnt.Aortic__c == true || cnt.Coro_PV__c == true || cnt.Deep__c == true || cnt.Embolization__c == true || cnt.Superficial__c == true) cnt.Vascular__c = true;

            // BU : CS & SH
            if (cnt.ECT__c == true  || cnt.Structural_Heart__c == true || cnt.HST__c == true) cnt.CS_SH__c = true;

            // BU : Cranial Spinal
            if (cnt.Spine_Biologics_SBU__c == true || cnt.Transformative_Solutions__c == true || cnt.Brain_Modulation__c == true || cnt.Neuro_Surgery_Bool__c == true || cnt.Interventional__c == true) cnt.Cranial_and_Spinal__c = true;

            // BU : CRHF
            if (cnt.MCS__c == true || cnt.Implantables_Diagnostic__c == true || cnt.AF_Solutions__c == true) cnt.CRHF__c = true;

            // BU : Diabetes
            if (cnt.Diabetes_Core__c == true) cnt.Diabetes__c = true;   // NEEDED?

            // BU : Early Technology
            if (cnt.GIH__c == true) cnt.Early_Technologies__c = true; 

            // BU : ENT
            if (cnt.ENT_NT__c == true) cnt.ENT__c = true; 

            // BU : IHS

            // BU : Neuromodulation

            // BU : Neurovascular
            if (cnt.Neurovascular_SBU__c == true) cnt.Neurovascular__c = true;

            // BU : Pain

            // BU : Renal Care Solutions
            if (cnt.RCS__c == true) cnt.Renal_Care_Solutions__c = true;

            // BU : Respiratory & Monitoring Solutions
            if (cnt.ACC__c == true || cnt.Essentials__c == true || cnt.GCC__c == true) cnt.Patient_Monitoring_Recovery__c = true;

            // BU : Pelvic Health
            if (cnt.Pelvic_Health_SBU__c == true) cnt.Pelvic_Health__c = true;

            // BU : RTG IHS
            if (cnt.RTG_IHS_SBU__c == true) cnt.RTG_IHS__c = true;

            // BU : Spine & Biologics

            // BU : Surgical Inovations
            if (cnt.AST__c == true || cnt.Hernia__c == true || cnt.GSP__c == true || cnt.ILS__c == true) cnt.Surgical_Innovations__c = true;

            // BU : Surgical Technologies

            // BU : Robotics
            if (cnt.Robotics_SBU__c == true) cnt.Robotics__c = true;

    	}    	
    } 
    
    public static void setKORsBUContactFlags(List<Contact> triggerNew){
    	
    	if (bl_Trigger_Deactivation.isTriggerDeactivated('cnt_setKORsBUContactFlags')) return;
    	
    	for(Contact cnt : triggerNew){
    		
    		if(currentUser.Company_Code_Text__c == 'KOR'){
    			
    			if(currentUser.Primary_sBU__c == 'ENT') cnt.ENT_NT__c = true;
    			if(currentUser.Primary_sBU__c == 'Navigation') cnt.Navigation__c = true;
    			if(currentUser.Primary_sBU__c == 'NT') cnt.ANZ_NT__c = true;    			
    		}    		
    	}    	
    }  
    
    private static Map<String, Id> contactRecordTypes {
		
		get{
			
			if(contactRecordTypes == null){
				
				contactRecordTypes = new Map<String, Id>();
				
				for(RecordType rt : [Select Id, DeveloperName from RecordType where SObjectType = 'Contact' AND isActive = true]) contactRecordTypes.put(rt.DeveloperName, rt.Id);
			}
			
			return contactRecordTypes;
		}
		
		set;		
	}	
    
    public static void copySalutationToTitleANZ(List<Contact> triggerNew, Map<Id, Contact> oldMap){
    	
    	if(bl_Trigger_Deactivation.isTriggerDeactivated('cnt_copySalutationToTitleANZ')) return;
		
		Id anz_Contact_RTid = contactRecordTypes.get('ANZ_Contact');
    	
    	for(Contact cnt : triggerNew){
    		
    		if(cnt.RecordTypeId == anz_Contact_RTid){
    			
    			// Create
    			if(oldMap == null){
    				
    				if(cnt.Salutation != null) cnt.Title = cnt.Salutation;
    				
    			// Update	
    			}else{
    				
    				if(cnt.Salutation != oldMap.get(cnt.Id).Salutation) cnt.Title = cnt.Salutation;
    			}    			
    		}    		
    	}    	
    }
    
    public static void MDTEmployeeCopyFields(List<Contact> triggerNew){
    	
    	if(bl_Trigger_Deactivation.isTriggerDeactivated('cnt_MDTEmployeeCopyFields')) return;
    	
    	Set<Id> userIds = new Set<Id>();
    	
    	for(Contact cnt : triggerNew){
    		
    		if(cnt.SFDC_User__c != null) userIds.add(cnt.SFDC_User__c);    			
    	}
    	
    	if(userIds.size() > 0){
    		
    		Map<Id, User> userMap = new Map<Id, User>([Select Id, Email, Job_Title_vs__c, Manager.FirstName, Manager.LastName, MobilePhone, Phone, Alias_unique__c from User where Id In :userIds]);
    		
    		for(Contact cnt : triggerNew){
    		
    			if(cnt.SFDC_User__c != null){
    				
    				User usr = userMap.get(cnt.SFDC_User__c);
    				
    				cnt.Email = usr.Email;
    				cnt.Job_Title_Description__c = usr.Job_Title_vs__c;
    				if(usr.Manager != null) cnt.Manager__c = usr.Manager.FirstName + ' ' + usr.Manager.LastName;
    				else cnt.Manager__c = null;
    				cnt.MobilePhone = usr.MobilePhone;
    				cnt.Phone = usr.Phone;
					cnt.Alias_unique__c = usr.Alias_unique__c;
    			}    			
    		}
    	}
    }
    
    public static void setMDTAgentFlag(List<Contact> triggerNew){
    	
    	if(bl_Trigger_Deactivation.isTriggerDeactivated('cnt_setMDTAgentFlag')) return;
		
		Id MDT_Agent_RTid = contactRecordTypes.get('MDT_Agent_Distributor');
		
		for(Contact cnt : triggerNew){
			
			if(cnt.RecordTypeId == MDT_Agent_RTid && cnt.Affiliation_To_Account__c == 'Agent'){
				
				cnt.MDT_Agent__c = true;
			}
		}
    }
    
    public static void populateUniqueHCPId_Create(List<Contact> triggerNew){
    	
    	if(bl_Trigger_Deactivation.isTriggerDeactivated('cnt_populateUniqueHCPId_Create')) return;
		
		Id MDT_Employee_RTid = contactRecordTypes.get('MDT_Employee');
		Id MDT_Agent_RTid = contactRecordTypes.get('MDT_Agent_Distributor');
		
		List<Contact> toUpdate = new List<Contact>();
		
		for(Contact cnt : triggerNew){
			
			if(cnt.RecordTypeId != null && cnt.RecordTypeId != MDT_Employee_RTid && cnt.RecordTypeId != MDT_Agent_RTid && cnt.HCP_Unique_ID__c == null) toUpdate.add(new Contact(Id = cnt.Id));			
		}
		
		if(toUpdate.size() > 0) update toUpdate;
    }
    
    public static void populateUniqueHCPId(List<Contact> triggerNew, Map<Id, Contact> oldMap){
    	
    	if(bl_Trigger_Deactivation.isTriggerDeactivated('cnt_populateUniqueHCPId')) return;
		
		Id MDT_Employee_RTid = contactRecordTypes.get('MDT_Employee');
		Id MDT_Agent_RTid = contactRecordTypes.get('MDT_Agent_Distributor');
		
		for(Contact cnt : triggerNew){
			
			if(cnt.RecordTypeId != null && cnt.RecordTypeId != MDT_Employee_RTid && cnt.RecordTypeId != MDT_Agent_RTid){
				
				if(cnt.HCP_Unique_ID__c == null || cnt.HCP_Unique_ID__c != cnt.Id) cnt.HCP_Unique_ID__c = cnt.Id;
				
			}else{
				
				cnt.HCP_Unique_ID__c = null;
			}	
		}
    }
    
    public static void deleteRelatedData(List<Contact> triggerOld){
    	
    	// Delete Affiliations         
        List<Affiliation__c> affToDelete = [Select Id from Affiliation__c where Affiliation_To_Contact__c IN :triggerOld OR Affiliation_From_Contact__c IN :triggerOld];       	
       	if(affToDelete.size() > 0) delete affToDelete ;
    }
                
    public static void generateContactDeleteInfo(List<Contact> triggerOld){
    	
    	if(bl_Trigger_Deactivation.isTriggerDeactivated('cnt_generateContactDeleteInfo')) return;
    	
		List<Contact_Delete_Info__c> toInsert = new List<Contact_Delete_Info__c>();
	    list<Contact_Delete_Info__c> toUpdate=new list<Contact_Delete_Info__c>();
	
		Set<Id> conIds = new Set<Id>();
	    Set<Id> accIds = new Set<Id>();
	    
	    for(Contact cnt : triggerOld) {
	        
	        conIds.add(cnt.Id);
	        if(cnt.AccountId != null) accIds.add(cnt.AccountId);	                          
	    }
	       
	    Map<Id, Contact_Delete_Info__c> mapConDeleteInfo = new Map<Id, Contact_Delete_Info__c>();
	       
	    for(Contact_Delete_Info__c deleteInfo : [Select id, Deleted_Contact_SFDC_ID__c	from Contact_Delete_Info__c  where Deleted_Contact_SFDC_ID__c in :conIds]){
	    	
	    	mapConDeleteInfo.put(deleteInfo.Deleted_Contact_SFDC_ID__c, deleteInfo);
	    }
	
		Map<Id, Account> accMap = new Map<Id, Account>([Select Id, Account_Country_vs__c From Account where Id IN :accIds]);
			
	    for(Contact cnt : triggerOld) {
	    	
	    	Map<String, String> mapGetContactCountry = new Map<String, String>();
	    	 
	        if(cnt.MailingCountry!=null){
	        	
	        	mapGetContactCountry = SharedMethods.retIntegrateWithMMXCountries(cnt.MailingCountry.toUpperCase());
	        	
	        } else if(accMap.containsKey(cnt.AccountId) && accMap.get(cnt.AccountId).Account_Country_vs__c != null) {
	           	
	           	mapGetContactCountry = SharedMethods.retIntegrateWithMMXCountries(accMap.get(cnt.AccountId).Account_Country_vs__c.toUpperCase());	           	
	        }
	                 
	        if(mapGetContactCountry.size() > 0 || cnt.Integrate_with_MMX__c == true) {
	        	
	        	Contact_Delete_Info__c deleteInfo = mapConDeleteInfo.get(cnt.Id);
	        	          
	            if(deleteInfo != null){
	                
	                // DO not create another Record 
	                if(cnt.MasterrecordId != null) {
	                	// Merged case...	                                   
	                   	deleteInfo.Type__c = 'Merge';              
	                   	deleteInfo.Surviving_Contact_SFDC_ID__c = cnt.MasterrecordId;                   	
	                   	
	                } else {
	                	// Delete case...	                                 
	                    deleteInfo.Type__c = 'Delete';              
	                    deleteInfo.Surviving_Contact_SFDC_ID__c = null;	                
	                }                                           
	                
	                deleteInfo.Undeleted_Bool__c = false;	                
	                toUpdate.add(deleteInfo);
	                
	            }else{
	                
	                deleteInfo = new Contact_Delete_Info__c();
	                deleteInfo.Deleted_Contact_SFDC_ID__c = cnt.Id;
	                
	                if(cnt.MasterrecordId != null) {
	                    // Merged case...	                                  
	                    deleteInfo.Type__c = 'Merge';              
	                    deleteInfo.Surviving_Contact_SFDC_ID__c = cnt.MasterrecordId; 
	                    
	                } else {
	                    // Delete case...	                                    
	                    deleteInfo.Type__c = 'Delete';              
	                    deleteInfo.Surviving_Contact_SFDC_ID__c = null;
	                            
	                } 
	                
	                deleteInfo.Undeleted_Bool__c = false;	                                            
	                toInsert.add(deleteInfo);                  
	            }   
	        }         
	    }
	    
	    if(toInsert.size()>0) insert toInsert;	        
	    if(toUpdate.size()>0) update toUpdate;
    }
    
   	public static void updateContactDeleteInfo(List<Contact> triggerNew){
   		
    	if(bl_Trigger_Deactivation.isTriggerDeactivated('cnt_updateContactDeleteInfo')) return;
    	
    	Set<String> contactIds = new Set<String>();    	
    	for(Contact cnt : triggerNew) contactIds.add(cnt.Id);
    	
	    List<Contact_Delete_Info__c> conDeleteInfos = [Select Id from Contact_Delete_Info__c where Deleted_Contact_SFDC_ID__c IN :contactIds] ;
	
	    for(Contact_Delete_Info__c deleteInfo : conDeleteInfos){
	    	
			deleteInfo.Undeleted_Bool__c = true;	        
	    }
	    
	    if(conDeleteInfos.size() > 0) update conDeleteInfos;
   	}   
        
    public static void undeleteAffiliations(List<Contact> triggerNew){
    	
    	if(bl_Trigger_Deactivation.isTriggerDeactivated('cnt_undeleteAffiliations')) return;
    	    
    	List<Affiliation__c> contactDeletedAffiliations = [Select Id from Affiliation__c where isDeleted = true and (Affiliation_From_Contact__c in :triggerNew OR Affiliation_To_Contact__c in: triggerNew) ALL ROWS] ;
    	
    	if(contactDeletedAffiliations.size() > 0) undelete contactDeletedAffiliations ;  
    }  
    
    public static void keepIntegrateMMXFlag(List<Contact> triggerNew, Map<Id, Contact> mapOld){
    	
    	if(bl_Trigger_Deactivation.isTriggerDeactivated('cnt_keepIntegrateMMXFlag')) return;
    	
    	for(Contact cnt : triggerNew){
    		
    		Contact cntOld = mapOld.get(cnt.Id);			
			if(cntOld.Integrate_with_MMX__c == true && cnt.Integrate_with_MMX__c == false) cnt.Integrate_with_MMX__c = true;
		}     
    }   
    
    public static void validateOwner(List<Contact> triggerNew, Map<Id, Contact> oldMap){
    	
    	if(bl_Trigger_Deactivation.isTriggerDeactivated('cnt_validateOwner')) return;
    	    	
    	List<Contact> cnts = new List<Contact>();    	
        Set<Id> setID_NewAccount = new Set<ID>();
	
        for(Contact oContact : triggerNew){
        	
            Contact oContact_Old = oldMap.get(oContact.Id);
	
            if(oContact.IsPersonAccount == false && oContact.AccountId != oContact_Old.AccountId){
                
                cnts.add(oContact);
                setID_NewAccount.add(oContact.AccountId);
            }
        }
	
        if(setID_NewAccount.size() > 0){
        	
        	Map<Id, Account> mapAccount = new Map<Id, Account>([SELECT Id, OwnerId FROM Account WHERE Id = :setID_NewAccount]);
        	
            for(Contact oContact : cnts){
                
                if(oContact.OwnerId != mapAccount.get(oContact.AccountId).OwnerId) oContact.OwnerId = mapAccount.get(oContact.AccountId).OwnerId;                
            }
        }
    }
    
    public static void createPrimaryAffiliation(List<Contact> triggerNew){
    	
    	if(bl_Trigger_Deactivation.isTriggerDeactivated('cnt_createPrimaryAffiliation')) return;
    	
    	List<Affiliation__c> affsToInsert = SharedMethods.createAffiliation(triggerNew);    
        insert affsToInsert;      	
    }
    
    public static void updateRelationshipOnAccountChange(List<Contact> triggerNew, Map<Id, Contact> oldMap){
    	
    	if(bl_Trigger_Deactivation.isTriggerDeactivated('cnt_updateRelationshipOnAccountChange')) return;
    	
    	List<Contact> contacts = new List<Contact>();
	        
	    for(Contact cnt : triggerNew){ 
	    	
	    	Contact oldCnt = oldMap.get(cnt.Id);
	    	          	    
	        if(cnt.AccountId != oldCnt.AccountId) contacts.add(cnt);	        
	    }
	    
	    //Only Contacts that have changed the main Account
	    if(contacts.size() == 0) return;
	         
        List<Contact> cntAffiliations = [Select Id, AccountId, Contact_Department__c, Phone, Primary_Job_Title_vs__c, Job_Title_Description__c, Buying_influence_role__c, Degree_of_Influence__c, Department_Description__c, Affiliation_To_Account__c,
        										(Select Id, Affiliation_From_Contact__c, RecordType.DeveloperName, Affiliation_Type__c, Affiliation_To_Account__c, Affiliation_Primary__c, Affiliation_End_Date__c from Affiliations__r ) from Contact where Id IN: contacts];       
    	    	
    	List<Affiliation__c> toUpdate  = new List<Affiliation__c>();    	
    	List<Contact> contactsWithoutPrimary = new List<Contact>();
    	    	    
    	for(Contact cnt : cntAffiliations){
    		
    		Boolean hasPrimary = false;
    		
    		for(Affiliation__c aff : cnt.Affiliations__r){
    			
    			//If existing affiliation already connected to the new Account, update it instead of creating new
    			if(aff.Affiliation_To_Account__c == cnt.AccountId){
            	
            		if(aff.Affiliation_Type__c !='Referring' && aff.RecordType.DeveloperName != 'C2C'){
            			
	 	                aff.Affiliation_Primary__c = true; 
		                aff.Affiliation_End_Date__c = null;
	            		aff.Affiliation_Start_Date__c = system.today();
	            		aff.Affiliation_Active__c = true;
	            		aff.Department__c = cnt.Contact_Department__c;
		            	aff.Office_Phone__c = cnt.Phone;                            
		            	aff.Affiliation_Position_In_Account__c = cnt.Primary_Job_Title_vs__c  ;
						aff.Job_Title_Description__c = cnt.Job_Title_Description__c;
	                    aff.Buying_influence_role__c = cnt.Buying_influence_role__c;
	                    aff.Degree_of_Influence__c = cnt.Degree_of_Influence__c;
		                aff.Affiliation_Type__c = cnt.Affiliation_To_Account__c ;
		                
						toUpdate.add(aff);	
						
						hasPrimary = true;            	  
    	        	}
    	        	
    			}else{
    			    		
        			//If there is already a primary affiliation linked to the contact, set it to false !! 
            		if(aff.Affiliation_Primary__c == true){ 
                	
                		aff.Affiliation_Primary__c = false ;	                
                		aff.Affiliation_End_Date__c = system.today();
                	
                		toUpdate.add(aff);            	
            		}                 
            	}
        	}
        	
        	if(hasPrimary == false) contactsWithoutPrimary.add(cnt);
    	}
    		
    	if(toUpdate.size()>0) update toUpdate;    
	    
	    //Create the primary Affiliation for those Contacts who didn't have one already connected to their new Account
	    if(contactsWithoutPrimary.size() > 0){
	    		    	 
	    	List<Affiliation__c> affsToInsert = SharedMethods.createAffiliation(contactsWithoutPrimary);
	    	insert affsToInsert;
	    }    
    }
    
    public static void updatePrimaryRelationshipOnChange(List<Contact> triggerNew, Map<Id, Contact> oldMap){
    	
    	if(bl_Trigger_Deactivation.isTriggerDeactivated('cnt_updatePrimaryRelationshipOnChange')) return;
    	    	
        List<Contact> updatedContacts = new List<Contact>();
         
	    for(Contact cnt : triggerNew){
	    	
	    	Contact cntOld = oldMap.get(cnt.Id);
	        if(cnt.Primary_Job_Title_vs__c != cntOld.Primary_Job_Title_vs__c || 
	            cnt.Contact_Department__c != cntOld.Contact_Department__c || 
	            cnt.Affiliation_To_Account__c != cntOld.Affiliation_To_Account__c || 
				cnt.Job_Title_Description__c != cntOld.Job_Title_Description__c ||
	            cnt.Phone != cntOld.Phone || 
	            cnt.Degree_of_Influence__c != cntOld.Degree_of_Influence__c || 
	            cnt.Buying_influence_role__c != cntOld.Buying_influence_role__c){
	            	
	        	updatedContacts.add(cnt);
	       }
	    }    	
	       
	    if(updatedContacts.size() == 0) return;
	    
	    List<Affiliation__c> toUpdate = new List<Affiliation__c>();
             	   
	    for(Contact cnt : [Select Id, Primary_Job_Title_vs__c, Job_Title_Description__c, Contact_Department__c, Affiliation_To_Account__c, Phone, Degree_of_Influence__c, Buying_influence_role__c, (Select Id from Affiliations__r where Affiliation_Primary__c = true) from Contact where Id IN :updatedContacts]){
	            	
	    	for(Affiliation__c aff : cnt.Affiliations__r){
	    		
	    		aff.Department__c = cnt.Contact_Department__c;	         
	            aff.Affiliation_Position_In_Account__c = cnt.Primary_Job_Title_vs__c;
				aff.Job_Title_Description__c = cnt.Job_Title_Description__c;
	            aff.Affiliation_Type__c = cnt.Affiliation_To_Account__c;
	            aff.Office_Phone__c = cnt.Phone;
                aff.Degree_of_Influence__c = cnt.Degree_of_Influence__c;
                aff.Buying_influence_role__c = cnt.Buying_influence_role__c;

            	toUpdate.add(aff);
	        }                  
	    }
	    
	    if(toUpdate.size() > 0) update toUpdate;  
    }
    	
	public static void updateAddresOnAccountChange(List<Contact> triggerNew, Map<Id, Contact> oldMap){
		
		if(bl_Trigger_Deactivation.isTriggerDeactivated('cnt_updateAddresOnAccountChange')) return;
		
		List<Contact> contacts = new List<Contact>();
	    Set<Id> accountIds = new Set<Id>();  
	    
	    for(Contact con : triggerNew){
	    	     
	        if(con.AccountId != oldmap.get(con.Id).AccountId){
	        	        
	            contacts.add(con);
	            accountIds.add(con.AccountId);
	            accountIds.add(oldmap.get(con.Id).AccountId);
	        }  
	    } 
		
		if(contacts.size() == 0) return;
			    
	    Map<Id, Account> accMap = new Map<id, Account>([SELECT Id, Account_Address_Line_1__c, Account_Address_Line_2__c, Account_Address_Line_3__c, Account_City__c,Account_Country_vs__c, Account_Postal_Code__c,Account_Province__c FROM Account WHERE Id IN :accountIds]);    
		
	    for(Contact con : contacts){
	    	
	    	Contact oContact_Old = oldMap.get(con.id);
	    	
            String oldAccStreet = '';            
            Id idAccount_Old = oContact_Old.AccountId;
            Id idAccount_New = con.AccountId;
            Account oAccount_Old;
            Account oAccount_New = accMap.get(idAccount_New);

            if(idAccount_Old != null && accMap.containsKey(idAccount_Old)){
            		
                oAccount_Old = accMap.get(idAccount_Old);
                oldAccStreet = oAccount_Old.Account_Address_Line_1__c + oAccount_Old.Account_Address_Line_2__c + oAccount_Old.Account_Address_Line_3__c;              	              
            }

            String newAccStreet=(oAccount_New.Account_Address_Line_1__c +
                                 oAccount_New.Account_Address_Line_2__c +
                                 oAccount_New.Account_Address_Line_3__c).remove('null');
          
            String conStreet=''; 
            if(con.MailingStreet != null){
            	
                List<String> str=con.MailingStreet.split('\n');                
                
                for(integer i=0;i<str.size();i++){
                    conStreet=conStreet.trim()+str[i].trim();
                }
            } 

            if (idAccount_Old == null){
                 
                 con.Mailingstreet = newAccStreet.remove('null');
                 con.MailingCity = oAccount_New.Account_City__c;
                 con.MailingCountry = oAccount_New.Account_Country_vs__c;                                     
                 con.MailingPostalCode = oAccount_New.Account_Postal_Code__c;
                 con.MailingState = oAccount_New.Account_Province__c;
                 
            }else{
            	
            	//If the address is still the same as the one for the previous Account, we update it to the one of the new Account.
            	if(conStreet == oldAccStreet.remove('null') && 
                 	con.MailingState == oAccount_Old.Account_Province__c && 
                 	con.MailingPostalCode == oAccount_Old.Account_Postal_Code__c && 
                 	con.MailingCountry == oAccount_Old.Account_Country_vs__c && 
                 	con.MailingCity == oAccount_Old.Account_City__c){
                 		
                	con.Mailingstreet = newAccStreet.remove('null');
                    con.MailingCity = oAccount_New.Account_City__c;
                    con.MailingCountry = oAccount_New.Account_Country_vs__c;                                     
                    con.MailingPostalCode = oAccount_New.Account_Postal_Code__c;
                    con.MailingState = oAccount_New.Account_Province__c;
              	}
            }
	    }     
	}
	
	public static void populateExternalId(List<Contact> triggerNew){
		
		//Populate the External_Id__c field with a new Id if no value was set before.
		GuidUtil.populateMobileId(triggerNew, 'External_Id__c');
	}
	
	// Get the Country related settings for the sBU Validation
	private static Map<String, Set<String>> mapCountryName_ExcludedJobTitles{
		
		get{
			 
			if(mapCountryName_ExcludedJobTitles == null){
				
				mapCountryName_ExcludedJobTitles = new Map<String, Set<String>>();			
				
				for (DIB_Country__c oCountry : [SELECT Id, Name, Contact_sBU_Validation__c, Contact_sBU_Validation_Excluded_JobTitle__c FROM DIB_Country__c WHERE Contact_sBU_Validation__c = true]){
					
					Set<String> setExcludedJobTitles = new Set<String>();
					
					if(clsUtil.isNull(oCountry.Contact_sBU_Validation_Excluded_JobTitle__c, '') != '') setExcludedJobTitles.addAll(oCountry.Contact_sBU_Validation_Excluded_JobTitle__c.split(';', -2));
					
					mapCountryName_ExcludedJobTitles.put(oCountry.Name.toLowerCase(), setExcludedJobTitles);
				}
			}
			
			return mapCountryName_ExcludedJobTitles;
		}
		
		set;
	}
	
	private static List<Sub_Business_Units__c> sBUList{
		
		get{
			
			if(sBUList == null) sBUList = [SELECT Id, Contact_Flag__c, Business_Unit__r.Contact_Flag__c FROM Sub_Business_Units__c WHERE Contact_Flag__c != null];
			
			return sBUList;
		}
		
		set;		
	}	
	
	public static void sBUValidation(List<Contact> triggerNew){
		
		if(bl_Trigger_Deactivation.isTriggerDeactivated('cnt_sBUValidation')) return;
		
		//exclude sytem administrator profile to avoid that batch processes fail
		if(ProfileMedtronic.isSystemAdministratorProfile(userinfo.GetProfileId())) return;
		
		List<Contact> contacts = new List<Contact>();
		Set<Id> accountIds = new Set<Id>();
		
		for(Contact oContact : triggerNew){
				
			//Only applicable for Generic Contact recordType. Also added static variable to avoid running the same validation multiple times
			if(oContact.RecordTypeId != null && oContact.RecordTypeId.equals(RecordTypeMedtronic.Contact('Generic_Contact').Id) &&				
				(oContact.Id == null || bl_Contact.sbuValidatedContacts.add(oContact.Id) == true)){
				
				contacts.add(oContact);
				accountIds.add(oContact.AccountId);
			}
		}
		
		//Nothing to validate
		if(contacts.size() == 0) return;
		
		Map<Id, Account> mapAccount = new Map<Id, Account>([SELECT Id, Account_Country_vs__c FROM Account WHERE Id = :accountIds]);
			
		// Loop through the processing Contacts
		for(Contact oContact : contacts){
			
			// Validate if there is at least 1 Sub Business Unit selected or not
			// Collect the Business Unit field on Contact that needs to be selected for the selected Sub Business Unit 
			Boolean bIsSBUSelected = false;
			
			Set<String> setContactBUField_Selected = new Set<String>();
			
			for(Sub_Business_Units__c oSBU : sBUList){
				 
				if(Boolean.valueOf(oContact.get(oSBU.Contact_Flag__c)) == true){
					
					bIsSBUSelected = true;
					
					//Set the BU flag also to true to keep the consistency
					if(clsUtil.isNull(oSBU.Business_Unit__r.Contact_Flag__c, '') != '')	oContact.put(oSBU.Business_Unit__r.Contact_Flag__c, true);					
				}
			}

			// There is at least 1 Sub Business Unit selected so not further processing is required for this record
			//	goto the next record
			if(bIsSBUSelected)	continue;
							
			Boolean bShowError = false;
				
			// Get the Account of the processing Contact and make sure the Country of the Account is not emtpy
			Account oAccount = mapAccount.get(oContact.AccountId);
			
			if(clsUtil.isNull(oAccount.Account_Country_vs__c, '') != ''){

				if(mapCountryName_ExcludedJobTitles.containsKey(oAccount.Account_Country_vs__c.toLowerCase())){
					
					// This Contact needs further processing because the Country of the related Account has been set-up for the sBU Validation logic
					// There is no Sub Business Unit selected - validate if the JobTitle is Excluded or not

					// Get the Excluded JobTitles for the Country of the Account
					Set<String> setExcludedJobTitles = mapCountryName_ExcludedJobTitles.get(oAccount.Account_Country_vs__c.toLowerCase());

					// There is no Business Unit selected on the contact so we need to validated if this is allowed.
					// If the JobTitle is NOT in the list of Excluded JobTitles, we need to throw an Errror
					if(clsUtil.isNull(oContact.Primary_Job_Title_vs__c, '') == '') bShowError = true;
					else if(!setExcludedJobTitles.contains(oContact.Primary_Job_Title_vs__c)) bShowError = true;					
				}
			}
			
			if (bShowError)	oContact.addError(Label.Sub_Business_Unit_is_required);					
		}		
	}

    private static Map<String, Contact_Owner__c> contactOwnerMap {
		
		get{
			
			if(contactOwnerMap == null){
				
				contactOwnerMap = Contact_Owner__c.getAll();				
			}
			
			return contactOwnerMap;
		}
		
		set;
	}
    
    public static void setContactOwner(List<Contact> triggerNew){
    	
    	Id BRMContactRTid = contactRecordTypes.get('IT_BRM_Stakeholder');
    	
    	for(Contact cnt : triggerNew){
    		
    		if(cnt.RecordTypeId == BRMContactRTid || cnt.MailingCountry == null) continue;
    		
    		String country = cnt.MailingCountry.toUpperCase();
    		
    		Contact_Owner__c countryOwner = contactOwnerMap.get(country);
    		
    		if(countryOwner != null) cnt.OwnerId = countryOwner.Owner_Id__c;
    	}
    }


	//--------------------------------------------------------------------------------------------------------------------
	// Contact_Job_Title__c will be replaced with Primary_Job_Title_vs__c but is still used in Interfaces.
	//	All APEX Logic in Salesforce.com is using Primary_Job_Title_vs__c so we need to keep these fields in sync until
	//	the Interface logic is updated and is also using Primary_Job_Title_vs__c.
	//--------------------------------------------------------------------------------------------------------------------
	public static void syncJobTitle_JobTitlevs(List<Contact> lstTriggerNew){

		if (bl_Trigger_Deactivation.isTriggerDeactivated('cnt_syncJobTitle_vs')) return;

		for (Contact oContact : lstTriggerNew){
		
			if (oContact.Contact_Job_Title__c != oContact.Primary_Job_Title_vs__c){
				
				oContact.Contact_Job_Title__c = oContact.Primary_Job_Title_vs__c;
				
			}
		}
	}
	
	public static void updateAccPlanStakeholderComments(List<Contact> triggerNew, Map<Id, Contact> oldMap){
		
		Set<Id> updatedComments = new Set<Id>();
		
		for(Contact cnt : triggerNew){
			
			Contact oldCnt = oldMap.get(cnt.Id);
			
			if(cnt.Contact_Comments__c != oldCnt.Contact_Comments__c) updatedComments.add(cnt.Id);
		}
		
		if(updatedComments.size() > 0){
			
			List<Account_Plan_Stakeholder__c> toUpdate = new List<Account_Plan_Stakeholder__c>();
			
			for(Contact cnt : [Select Id, Contact_Comments__c, (Select Id, Comments__c from Account_Plan_Stakeholders__r) from Contact where Id IN :updatedComments]){
				
				for(Account_Plan_Stakeholder__c apStakeholder : cnt.Account_Plan_Stakeholders__r){
					
					apStakeholder.Comments__c = cnt.Contact_Comments__c;
					toUpdate.add(apStakeholder);
				}
			}
			
			if(toUpdate.size() > 0) update toUpdate;
		}
	}
}