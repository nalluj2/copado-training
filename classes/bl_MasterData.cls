public with sharing class bl_MasterData {


	//------------------------------------------------------------------------------------------
	// SBU - QUEUE MAPPING
	//------------------------------------------------------------------------------------------
	public static Set<String> setRegion = new Set<String>();
	public static Set<String> setSBU = new Set<String>();
	public static Set<String> setCountry = new Set<String>();
	public static Set<String> setJobTitle = new Set<String>();
	public static Set<String> setGroupName = new Set<String>();

	public static Map<String, Map<String, Map<String, Map<String, String>>>> getRegion_SBU_Country_JobTitle_GroupName(){

		Map<String, Map<String, Map<String, Map<String, String>>>> mapRegion_SBU_Country_JobTitle_GroupName = new Map<String, Map<String, Map<String, Map<String, String>>>>();

		List<Queue_Mapping__c> lstQueueMapping = 
			[
				SELECT 
					Id, Queue_Name__c, Job_Title__c
					, Sub_Business_Unit__c, Sub_Business_Unit__r.Name, Sub_Business_Unit__r.Company_Text__c
					, Country__c, Country__r.Name
				FROM
					Queue_Mapping__c
				ORDER BY
					Sub_Business_Unit__r.Name
			];

		for (Queue_Mapping__c oQueueMapping : lstQueueMapping){

			// Populate Data Sets
			setRegion.add(oQueueMapping.Sub_Business_Unit__r.Company_Text__c);
			setSBU.add(oQueueMapping.Sub_Business_Unit__r.Name);
			setCountry.add(oQueueMapping.Country__r.Name.toUpperCase());
			setJobTitle.add(oQueueMapping.Job_Title__c);
			setGroupName.add(oQueueMapping.Queue_Name__c);


			Map<String, Map<String, Map<String, String>>> mapSBU_Country_JobTitle_GroupName = new Map<String, Map<String, Map<String, String>>>();
			if (mapRegion_SBU_Country_JobTitle_GroupName.containsKey(oQueueMapping.Sub_Business_Unit__r.Company_Text__c)){
				mapSBU_Country_JobTitle_GroupName = mapRegion_SBU_Country_JobTitle_GroupName.get(oQueueMapping.Sub_Business_Unit__r.Company_Text__c);
			} 

			Map<String, Map<String, String>> mapCountry_JobTitle_GroupName = new Map<String, Map<String, String>>();
			if (mapSBU_Country_JobTitle_GroupName.containsKey(oQueueMapping.Sub_Business_Unit__r.Name)){
				mapCountry_JobTitle_GroupName = mapSBU_Country_JobTitle_GroupName.get(oQueueMapping.Sub_Business_Unit__r.Name);
			}

			Map<String, String> mapJobTitle_GroupName = new Map<String, String>();
			if (mapCountry_JobTitle_GroupName.containsKey(oQueueMapping.Country__r.Name)){
				mapJobTitle_GroupName = mapCountry_JobTitle_GroupName.get(oQueueMapping.Country__r.Name);
			}

			mapJobTitle_GroupName.put(oQueueMapping.Job_Title__c, oQueueMapping.Queue_Name__c);
			mapCountry_JobTitle_GroupName.put(oQueueMapping.Country__r.Name.toUpperCase(), mapJobTitle_GroupName);
			mapSBU_Country_JobTitle_GroupName.put(oQueueMapping.Sub_Business_Unit__r.Name, mapCountry_JobTitle_GroupName);
			mapRegion_SBU_Country_JobTitle_GroupName.put(oQueueMapping.Sub_Business_Unit__r.Company_Text__c, mapSBU_Country_JobTitle_GroupName);

		}					

		return mapRegion_SBU_Country_JobTitle_GroupName;
	}
	//------------------------------------------------------------------------------------------

}