//--------------------------------------------------------------------------------------------------------------------------------
//  Author      :   Bart Caelen
//  Date        :   26/09/2017
//  Description :   APEX Test Class for the APEX Trigger tr_ContractIHS and APEX Class bl_ContractIHS_Trigger
//  Change Log  :    
//--------------------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_bl_ContractIHS_Trigger {
	
	@testSetup static void createTestData() {

        // Account - SAP Account
        List<Account> lstAccount_SAP = clsTestData_Account.createAccount_SAPAccount();

        // Account - Supplier
        List<Account> lstSupplier = clsTestData_Account.createAccount(false);
	        lstSupplier[0].RecordTypeId = clsUtil.getRecordTypeByDevName('Account', 'Supplier').Id;
    	    lstSupplier[0].Type = 'Service Supplier';
        insert lstSupplier;

        // Contract IHS
	    List<Contract_IHS__c> lstContractIHS = new List<Contract_IHS__c>(); 
		Contract_IHS__c oContract_IHS = new Contract_IHS__c();
            oContract_IHS.RecordTypeId = clsUtil.getRecordTypeByDevName('Contract_IHS__c', 'Supplier_Contract').Id;
            oContract_IHS.Start_Date__c = System.today();
            oContract_IHS.End_Date__c = System.today();
            oContract_IHS.Contract_Type__c = 'Maintenance';
            oContract_IHS.Active__c = true;
		lstContractIHS.add(oContract_IHS);
		insert lstContractIHS;

        // Equipment IHS
		List<Equipment_IHS__c> lstEquipmentIHS = new List<Equipment_IHS__c>();
		for (Integer iCounter = 0; iCounter < 5; iCounter++){
	        Equipment_IHS__c oEquipmentIHS = new Equipment_IHS__c();
	            oEquipmentIHS.Account__c = lstAccount_SAP[0].Id;
	            oEquipmentIHS.Supplier__c = lstSupplier[0].Id;
	            oEquipmentIHS.Serial_Number__c = 'SER_' + iCounter;
	            oEquipmentIHS.Next_PM_Date__c = System.today() + (iCounter * 7);
	            oEquipmentIHS.Next_EST_Date__c = System.today() + (iCounter * 7);
	            oEquipmentIHS.Active__c = true;
			lstEquipmentIHS.add(oEquipmentIHS);
		}
		insert lstEquipmentIHS;

		// Contract Equipment Detail IHS
		List<Contract_Equipment_Detail_IHS__c> lstContractEquipmentDetailIHS = new List<Contract_Equipment_Detail_IHS__c>();
		for (Equipment_IHS__c oEquipmentIHS : lstEquipmentIHS){
			Contract_Equipment_Detail_IHS__c oContractEquipment = new Contract_Equipment_Detail_IHS__c();
				oContractEquipment.Contract_IHS__c = oContract_IHS.Id;
				oContractEquipment.Equipment_IHS__c = oEquipmentIHS.Id;
				oContractEquipment.Equipment_Value__c = 100;
			lstContractEquipmentDetailIHS.add(oContractEquipment);
		}
		insert lstContractEquipmentDetailIHS;

	}
	
	@isTest static void test_cloneRelatedData() {

		//----------------------------------------------------------------------
		// Load Test Data
		//----------------------------------------------------------------------
		List<Contract_IHS__c> lstContractIHS = 
			[
				SELECT 
					Id, RecordTypeId, Start_Date__c, End_Date__c, Contract_Type__c, Active__c
					, (
						SELECT Contract_Recordtype__c, Equipment_IHS__c, Equipment_Model__c, Equipment_SerialNumber__c, Equipment_Value__c
						FROM Contract_Equipment_Details_IHS__r
					) 
 
				FROM Contract_IHS__c
			];

		System.assertEquals(lstContractIHS.size(), 1);
		System.assertEquals(lstContractIHS[0].Contract_Equipment_Details_IHS__r.size(), 5);
		//----------------------------------------------------------------------

		//----------------------------------------------------------------------
		// Perform Logic
		//----------------------------------------------------------------------
		Contract_IHS__c oContract_IHS_New = new Contract_IHS__c();
            oContract_IHS_New.RecordTypeId = lstContractIHS[0].RecordTypeId;
            oContract_IHS_New.Start_Date__c = lstContractIHS[0].Start_Date__c;
            oContract_IHS_New.End_Date__c = lstContractIHS[0].End_Date__c;
            oContract_IHS_New.Contract_Type__c = lstContractIHS[0].Contract_Type__c;
            oContract_IHS_New.Active__c = lstContractIHS[0].Active__c;
            oContract_IHS_New.Cloned_From__c = lstContractIHS[0].Id;
	
		Test.startTest();

			insert oContract_IHS_New;

		Test.stopTest();
		//----------------------------------------------------------------------


		//----------------------------------------------------------------------
		// Test Result
		//----------------------------------------------------------------------
		lstContractIHS = 
			[
				SELECT 
					Id
					, (
						SELECT Contract_Recordtype__c, Equipment_IHS__c, Equipment_Model__c, Equipment_SerialNumber__c, Equipment_Value__c
						FROM Contract_Equipment_Details_IHS__r
					) 
 
				FROM Contract_IHS__c
			];

		System.assertEquals(lstContractIHS.size(), 2);
		System.assertEquals(lstContractIHS[0].Contract_Equipment_Details_IHS__r.size(), 5);
		System.assertEquals(lstContractIHS[1].Contract_Equipment_Details_IHS__r.size(), 5);
		//----------------------------------------------------------------------

	}
	
}
//--------------------------------------------------------------------------------------------------------------------------------