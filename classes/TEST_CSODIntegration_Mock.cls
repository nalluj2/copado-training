global class TEST_CSODIntegration_Mock implements HttpCalloutMock{
    
    public Boolean bError { get; set; }
    public Boolean bGetEmployee { get; set; }
    public Boolean bCreateEmployee { get; set; }
    public Boolean bUpdateEmployee { get; set; }    
    public Boolean bCreateEmployeeManager { get; set; }    
    public Boolean bCreateTrainingAssignment { get; set; }    
	public Boolean bEmployeeInCSOD { get; set; }
    
    public TEST_CSODIntegration_Mock(){
    	
    	bError = false;
    	bGetEmployee = false;
    	bCreateEmployee = false;
    	bUpdateEmployee = false;    	
		bCreateEmployeeManager = false;
		bCreateTrainingAssignment = false;

		bEmployeeInCSOD = true;

    }

	private void setProperties(String tWSType){
	
		bGetEmployee = false;
		bCreateEmployee = false;
		bUpdateEmployee = false;
		bCreateEmployeeManager = false;
		bCreateTrainingAssignment = false;
		
		if (tWSType == 'GET_EMPLOYEE'){
			bGetEmployee = true;
		}else if (tWSType == 'CREATE_EMPLOYEE'){
			bCreateEmployee = true;
		}else if (tWSType == 'UPDATE_EMPLOYEE'){
			bUpdateEmployee = true;
		}else if (tWSType == 'CREATE_EMPLOYEE_MANAGER'){
			bCreateEmployeeManager = true;
		}else if (tWSType == 'CREATE_TRAINING_ASSIGNMENT'){
			bCreateTrainingAssignment = true;
		}
		
	}
    	
    global HTTPResponse respond(HTTPRequest oHTTPRequest){

		String tWSType = oHTTPRequest.getHeader('WS_TYPE');
		if (!String.isBlank(tWSType)) setProperties(tWSType);

		String tEnpoint = oHTTPRequest.getEndpoint();
		
		String tBody = '';
		Integer iStatusCode = 200;
		
		if (bGetEmployee){
	
			// Execute Search Employee
			bl_ContactCSODIntegration.ResponseHeader oResponseHeader = new bl_ContactCSODIntegration.ResponseHeader();
			bl_ContactCSODIntegration.employeeResults oEmployeeResult = new bl_ContactCSODIntegration.employeeResults();
			bl_ContactCSODIntegration.ErrorResponse oResponse_Error = new bl_ContactCSODIntegration.ErrorResponse();

			if (bError == false){
				
				if (bEmployeeInCSOD){

					oEmployeeResult.externalUserId = '136686';
					oEmployeeResult.userIdentifier = 'caeleb1';
					oEmployeeResult.userName = 'caeleb1';
					oEmployeeResult.firstName = 'Bart';
					oEmployeeResult.lastName = 'Caelen';
					oEmployeeResult.prefix = 'null';
					oEmployeeResult.primaryEmail = 'bart.caelen@gmail.com';
					oEmployeeResult.countryName = 'null';
					oEmployeeResult.isActive = 'true';
					oEmployeeResult.displayLanguage = 'English (UK)';
					oEmployeeResult.divisionId = 'CAD';
					oEmployeeResult.hcpNumber = '123456';
					oEmployeeResult.learningManagerUserId = 'null';
					oEmployeeResult.learningManagerFirstName = 'null';
					oEmployeeResult.learningManagerLastName = 'null';
					oEmployeeResult.learningManagerEmail = 'robin.quaedflieg@medtronic.com';
					oEmployeeResult.learningManagerPhone = '+31612345678';

					oResponseHeader.employeeResults = oEmployeeResult;
					tBody = JSON.serializePretty(oResponseHeader);

				}else{

					oResponse_Error.errorCode = '400';
					oResponse_Error.errorType = 'Invalid Data';
					oResponse_Error.errorDescription = 'No records found for the requested Id';
					oResponse_Error.supportContact = 'test@medtronic.com';
					iStatusCode = 400;

					tBody = JSON.serializePretty(oResponse_Error);
				}
					
			}else{
				
				oResponse_Error.errorCode = 'ERR';
				oResponse_Error.errorType = 'ERROR';
				oResponse_Error.errorDescription = 'Unit Test Error';
				oResponse_Error.supportContact = 'test@medtronic.com';
				iStatusCode = 400;

				tBody = JSON.serializePretty(oResponse_Error);
			}
			
		
		}else if (bCreateEmployee){
	
			// Execute Create Employee
			bl_ContactCSODIntegration.ResponseHeader oResponseHeader = new bl_ContactCSODIntegration.ResponseHeader();
			bl_ContactCSODIntegration.createEmployeeResult oCreateEmployeeResult = new bl_ContactCSODIntegration.createEmployeeResult();
			bl_ContactCSODIntegration.ErrorResponse oResponse_Error = new bl_ContactCSODIntegration.ErrorResponse();

			if (bError == false){
				
				oCreateEmployeeResult.externalUserId = '136686';
				oCreateEmployeeResult.timestamp = clsUtil.getTimeStamp();
				
				oResponseHeader.createEmployeeResult = oCreateEmployeeResult;
				tBody = JSON.serializePretty(oResponseHeader);

			}else{
				
				oResponse_Error.errorCode = 'ERR';
				oResponse_Error.errorType = 'ERROR';
				oResponse_Error.errorDescription = 'Unit Test Error';
				oResponse_Error.supportContact = 'test@medtronic.com';
				iStatusCode = 400;

				tBody = JSON.serializePretty(oResponse_Error);

			}
			
		}else if (bUpdateEmployee){

			// Execute Update Employee
			bl_ContactCSODIntegration.updateEmployeeResult oResponse = new bl_ContactCSODIntegration.updateEmployeeResult();
			bl_ContactCSODIntegration.ErrorResponse oResponse_Error = new bl_ContactCSODIntegration.ErrorResponse();

			if (bError == false){
				
				oResponse.externalUserId = '136686';
				oResponse.timestamp = '';
					
				tBody = JSON.serializePretty(oResponse);
	
			}else{
				
				oResponse_Error.errorCode = 'ERR';
				oResponse_Error.errorType = 'ERROR';
				oResponse_Error.errorDescription = 'Unit Test Error';
				oResponse_Error.supportContact = 'test@medtronic.com';
				iStatusCode = 400;

				tBody = JSON.serializePretty(oResponse_Error);

			}
			

		}else if (bCreateEmployeeManager){

			// Execute Create Employee Manager
			bl_ContactCSODIntegration.ResponseHeader oResponseHeader = new bl_ContactCSODIntegration.ResponseHeader();
			bl_ContactCSODIntegration.createEmployeeResult oCreateEmployeeResult = new bl_ContactCSODIntegration.createEmployeeResult();
			bl_ContactCSODIntegration.ErrorResponse oResponse_Error = new bl_ContactCSODIntegration.ErrorResponse();

			if (bError == false){
				
				oCreateEmployeeResult.externalUserId = 'caeleb2';
				oCreateEmployeeResult.timestamp = clsUtil.getTimeStamp();

				oResponseHeader.createEmployeeResult = oCreateEmployeeResult;
				tBody = JSON.serializePretty(oResponseHeader);
					
			}else{
				
				oResponse_Error.errorCode = 'ERR';
				oResponse_Error.errorType = 'ERROR';
				oResponse_Error.errorDescription = 'Unit Test Error';
				oResponse_Error.supportContact = 'test@medtronic.com';
				iStatusCode = 400;

				tBody = JSON.serializePretty(oResponse_Error);

			}
			

		}else if (bCreateTrainingAssignment){

			// Execute Training Assignment
			bl_ContactCSODIntegration.ResponseHeader oResponseHeader = new bl_ContactCSODIntegration.ResponseHeader();
			bl_ContactCSODIntegration.trainingAssignmentResult oTrainingAssignmentResult = new bl_ContactCSODIntegration.trainingAssignmentResult();
			bl_ContactCSODIntegration.ErrorResponse oResponse_Error = new bl_ContactCSODIntegration.ErrorResponse();

			if (bError == false){
				
				oTrainingAssignmentResult.status = '201';
				oTrainingAssignmentResult.Reason = 'Training assignment is success';

				oResponseHeader.trainingAssignmentResult = oTrainingAssignmentResult;
				tBody = JSON.serializePretty(oResponseHeader);

			}else{
				
				oResponse_Error.errorCode = 'ERR';
				oResponse_Error.errorType = 'ERROR';
				oResponse_Error.errorDescription = 'Unit Test Error';
				oResponse_Error.supportContact = 'test@medtronic.com';
				iStatusCode = 400;

				tBody = JSON.serializePretty(oResponse_Error);

			}
			
		}
						
        HttpResponse oHTTPResponse = new HttpResponse();
			oHTTPResponse.setHeader('Content-Type', 'application/json');       
			oHTTPResponse.setBody(tBody);
			oHTTPResponse.setStatusCode(iStatusCode);
        return oHTTPResponse;

    }


}