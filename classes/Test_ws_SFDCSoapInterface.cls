//--------------------------------------------------------------------------------------------------------------------
//  Description : This TEST APEX Class is used to test the logic in ws_SFDCSoapInterface
//  Version     : 1.0
//  Author      : Bart Caelen
//  Date        : 14/09/2016
//--------------------------------------------------------------------------------------------------------------------
@isTest private class Test_ws_SFDCSoapInterface {
	
	@isTest static void test_ws_SFDCSoapInterface() {

		Test.startTest();

		CalloutMock oCalloutMock = new CalloutMock();
		Test.setMock(HttpCalloutMock.class, oCalloutMock);
		ws_SFDCSoapInterface.doSoapCall('body', UserInfo.getSessionId());

		Test.stopTest();

	}

	private class CalloutMock implements HttpCalloutMock {

        public HTTPResponse respond(HTTPRequest req) {
            
            HttpResponse oHTTPResponse = new HttpResponse();
	            oHTTPResponse.setStatusCode(200);
    	        oHTTPResponse.setStatus('Complete');
                       
        	String tResponseBody = '{"response" : "goes here"}';
			            
			oHTTPResponse.setBody(tResponseBody);
            
            return oHTTPResponse;
        }
	}		
}
//--------------------------------------------------------------------------------------------------------------------