@RestResource(urlMapping='/AdminRequestService/*')
global with sharing class ws_Admin_Request_Service {
	
	@HttpPost
	global static Response doPost() {
		
		Response resp = new Response();
		resp.isSuccess = true;
		
		try{		
			
			if(UserInfo.getUserName().toLowerCase().startsWith('system@medtronic.com') == false && Test.isRunningTest() == false) throw new ws_Exception('This service can only be executed by the System user');
				
			Request req = (Request) System.Json.deserialize(RestContext.Request.RequestBody.toString(), Request.class);
			
			List<User> user = [Select Id from User where Id = :req.UserId];
			
			if(user.size() != 1) throw new ws_Exception('Context User (' + req.UserId + ') not found in Org');
				
			List<PermissionSetAssignment> userAssignments = [Select Id from PermissionSetAssignment where AssigneeId = :req.UserId AND PermissionSet.Name IN :req.PermissionSets];		
			delete userAssignments;
			
		}catch(Exception e){
			
			resp.isSuccess = false;
			resp.errorMessage = e.getMessage();	
		}
		
		return resp;
	}   
		
	global class Request{
		
		global Id userId {get; set;}
		global List<String> permissionSets {get; set;}
	}
	
	global class Response{
		
		global Boolean isSuccess {get; set;}
		global String errorMessage {get; set;}
	} 
}