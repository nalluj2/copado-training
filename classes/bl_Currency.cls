/*
 *      Created Date : 20121004
 *      Description : This is the class that centralizes logic around currency Types
 *
 *      Author = Rudy De Coninck
 */
public with sharing class bl_Currency {

	static Map<String,CurrencyType> mapCurrencyTypes;
	static List<CurrencyType> currencyTypes;

	static {
		mapCurrencyTypes = new Map<String,CurrencyType>{};
		init();		
	}

	public static void init(){
        currencyTypes = [
        	select Id, IsoCode, ConversionRate, DecimalPlaces, IsActive, IsCorporate 
        	from CurrencyType
        	order by IsoCode
        ];
        for(currencyType typeData : currencyTypes)
         {   
               mapCurrencyTypes.put(typeData.IsoCode,typeData);
         }
	}
	
	public static CurrencyType getCurrencyType(String isoCode){
		CurrencyType result = mapCurrencyTypes.get(isoCode);
		return result;
	}
	
	public static List<CurrencyType> getAllAvailableCurrencies(){
		return currencyTypes;
	}
	
	private static testmethod void testEur(){
		CurrencyType t = bl_Currency.getCurrencyType('EUR');
		System.AssertEquals('EUR',t.IsoCode);
	}

	private static testmethod void testAllAvailable(){
		List<CurrencyType> allCurrencies = bl_Currency.getAllAvailableCurrencies();
		System.Assert(allCurrencies.size()>0);
	}
}