@RestResource(urlMapping='/DeleteEntity/*')
global class TwoRing_DeleteEntityController {
	@HttpDelete
	global static void deleteEntity() {
		Pattern listTeamMemberPattern = Pattern.compile('/DeleteEntity/(.*)');
		Matcher match = listTeamMemberPattern.matcher(RestContext.request.requestURI);
		if(match.matches()) {
			TwoRing_Repository.deleteEntity(match.group(1));
		}
		else {
			throw new TwoRing_ApplicationException('Invalid url');
		}
	}
}