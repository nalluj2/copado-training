public with sharing class ctrlExt_AccountDetailsMobile {
	
	public String selectedView {get; set;}
	public List<SelectOption> availableBUs {get; set;}
		
	public ctrlExt_AccountDetailsMobile(ApexPages.StandardController sc){
		
		String userCompany=[select id,Company_Code_Text__c from User where id = :UserInfo.getUserId()].Company_Code_Text__c;
						
		availableBUs = new List<SelectOption>();
		availableBUs.add(new SelectOption( 'All', 'All'));
		        
        for(Business_Unit__c bu : [select id, Name from Business_Unit__c where Company__r.Company_Code_Text__c =:userCompany order by Name]){
        	
        	availableBUs.add(new SelectOption( bu.Name, bu.Name));        	
        }
                                  
        List<User_Business_Unit__c> primaryBU = [Select Sub_Business_Unit__r.Business_Unit__r.Name From User_Business_Unit__c 
        									where User__c=:UserInfo.getUserId() AND Primary__c = true LIMIT 1];
        if(primaryBU.size()>0){                
            selectedView = primaryBU[0].Sub_Business_Unit__r.Business_Unit__r.Name;             
        }
        
        if(selectedView == null){
        	selectedView = 'All';
        }		
	}
}