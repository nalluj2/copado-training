@isTest
private class TEST_ImplantProAfterInsert 
{

    static testMethod void myUnitTest()
    {
        
        Account a = new Account();
        a.name='Medtronic';
        a.SAP_ID__c='Standard P';
        a.recordtypeId='01220000000Pz6x';
        insert a;
         //Insert Company
       Company__c cmpny = new Company__c();
       cmpny.name='TestMedtronic';
       cmpny.CurrencyIsoCode = 'EUR';
       cmpny.Current_day_in_Q1__c=56;
       cmpny.Current_day_in_Q2__c=34; 
       cmpny.Current_day_in_Q3__c=5; 
       cmpny.Current_day_in_Q4__c= 0;   
       cmpny.Current_day_in_year__c =200;
       cmpny.Days_in_Q1__c=56;  
       cmpny.Days_in_Q2__c=34;
       cmpny.Days_in_Q3__c=13;
       cmpny.Days_in_Q4__c=22;
       cmpny.Days_in_year__c =250;
       cmpny.Company_Code_Text__c = 'T41';
       insert cmpny;     


         Business_Unit__c bu = new Business_Unit__c();
        bu.name = 'Testing BU';
        bu.abbreviated_name__c = 'Abb Bu';
        bu.Company__c = cmpny.Id;
        insert bu;

         //Insert SBU
        Sub_Business_Units__c sbu1 = new Sub_Business_Units__c();
        sbu1.name='SBUMedtronic1';
        sbu1.Business_Unit__c=bu.id;
        insert sbu1;

        //Insert Therapy Group
        Therapy_Group__c tg = new Therapy_Group__c();
        tg.Name='Therapy Group';
        tg.Company__c = cmpny.Id;
        tg.Sub_Business_Unit__c = sbu1.Id;
        insert tg;

        map<String, String> RecrodTypeMapping = new map<String, String>();
        list<Recordtype> rt = [Select Id,Name From RecordType where SobjectType='product2' and IsActive=true and name='MDT Marketing Product'];
        for(RecordType r:rt)
        {
            RecrodTypeMapping.put(string.ValueOf(r.Name),string.ValueOf(r.Id));
        }

        String RecType=RecrodTypeMapping.get('MDT Marketing Product');
        Product2 pro1 = new Product2();
        pro1.Name = 'Test Product';
        pro1.Business_Unit_ID__c = bu.id;
        pro1.Consumable_Bool__c = true;
        pro1.IsActive = true;
        //pro1.DefaultPrice=12.0;
        pro1.recordtypeid = RecType;
      //  pro1.Manufacturer_Account_ID__c = '001P000000R0kKd'; 
        insert pro1;

        Therapy__c th = new Therapy__c();
        th.name='test ## th';
        th.Business_Unit__c = bu.id;
        th.Sub_Business_Unit__c = sbu1.Id;
        th.Therapy_Group__c = tg.Id;
        th.Therapy_Name_Hidden__c = 'test ## th';
        insert th;

        
         Call_Records__c clrcd=new Call_Records__c();
       // clrcd.name='abe';
        insert clrcd;

		//-BC - 20140826 - Added to be sure that all required data is provided - START
        // Call_Activity_Type__c
        Call_Activity_Type__c oCallActivityType1 = new Call_Activity_Type__c();
	        oCallActivityType1.name = 'test C-A-T 1';
	        oCallActivityType1.Company_ID__c = cmpny.id;
        insert oCallActivityType1;

        // Call_Category__c
		Call_Category__c oCallCategory1 = new Call_Category__c();
			oCallCategory1.Company_ID__c = cmpny.Id;
			oCallCategory1.Name = 'test C-C 1';		
		insert oCallCategory1;
				
        
        Call_Topics__c cltpc=new Call_Topics__c();
	        cltpc.Call_Records__c=clrcd.id;
	        cltpc.Call_Activity_Type__c = oCallActivityType1.Id;
	        cltpc.Call_Category__c = oCallCategory1.Id;
        insert cltpc; 
/*
        Call_Topics__c cltpc=new Call_Topics__c();
       // cltpc.name='abc';
        cltpc.Call_Records__c=clrcd.id;
        cltpc.Call_Activity_Type__c = oCallActivityType1.Id;	//'a0W200000038H4HEAU'; //in staging:'a0W200000038H4HEAU'
        cltpc.Call_Category__c = oCallCategory1.Id;				//'a0i2000000178awAAA';//in staging:'a0i2000000178awAAA'
        insert cltpc; 
*/
		//-BC - 20140826 - Added to be sure that all required data is provided - STOP


       /// implant.Implant_Implanting_Contact__c = testContacts.get(1).Id;
       // implant.Implant_Implanting_Account__c = testAccounts.get(0).Id;
        implant__c implant = new implant__c();
        implant.Implant_Date_Of_Surgery__c = System.today() - 1;
        implant.Therapy__c = th.Id;
        implant.Attended_Implant_Text__c = 'UnAttended';
        implant.Duration_Nr__c = 10;
      //  implant.distributor__c=a.id;
        implant.Procedure_Text__c='Replacement';
        implant.Call_Record_ID__c=clrcd.id;
        //implant.Manufacturer_ID__c = '0012000000kvvDU';  // 001P000000R0kKd // Staging Medtronic Competitor Account Id
        insert implant ;            
        
        //Pricebook2 pb = new Pricebook2();
        //pb.name = a.SAP_ID__c;
        //pb.isStandard=true;
        //insert pb;
        list<Pricebook2> pb = [select id from Pricebook2 where name =:'Standard Price Book' limit 1];
        PricebookEntry pbe = new PricebookEntry();
        pbe.Product2Id = pro1.id;
        pbe.UseStandardPrice=false;
        pbe.UnitPrice=20.0;
//        pbe.Pricebook2Id ='01s200000001NG6AAM';
        pbe.Pricebook2Id =pb[0].id;
        pbe.IsActive = true;  
        insert pbe;

       

        //PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = standardPB.Id, Product2Id = prod.Id, UnitPrice = 10000, IsActive = true, UseStandardPrice = false);
        // insert standardPrice;
        Implanted_Product__c ast = new Implanted_Product__c();
        ast.Product2__c = pro1.id;
        //ast.name='assset1';
        ast.Account__c=a.id;
        ast.Implant_ID__c=implant.id;
        insert ast;
        
        
        ast.Price__c=pbe.UnitPrice;
        ast.CurrencyIsoCode = pbe.CurrencyIsoCode;
        update ast;
        }

}