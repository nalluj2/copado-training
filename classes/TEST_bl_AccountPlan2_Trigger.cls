@isTest private class TEST_bl_AccountPlan2_Trigger {

	@isTest private static void test_updateLastModifiedDateFields() {
	
		//------------------------------------------------------
		// Create Tests Data
		//------------------------------------------------------
		clsTestData_AccountPlan.createAccountPlan2();

		Account_Plan_2__c oAccountPlan2 = 
			[
				SELECT 
					Id
					, Political__c, Economic__c, Legal__c, Environmental__c, Technological__c, Social__c, PESTEL_Last_Modified_Date__c
					, Other_Strengths__c, Other_Opportunities__c, Other_Weaknesses__c, Other_Threats__c, Strengths__c, Opportunities__c, Weaknesses__c, Threats__c, Needs__c, SWOT_Last_Modified_Date__c
					, Medtronic_Strengths__c, Medtronic_Opportunities__c, Medtronic_Weaknesses__c, Medtronic_Threats__c, Medtronic_Potential_Synergies__c, Medtronic_Last_Modified_Date__c
					, Them__c, Us__c, Fit__c, Proof__c, Care_Abouts_Last_Modified_Date__c
				FROM Account_Plan_2__c
				LIMIT 1
			];
		
		System.assert(oAccountPlan2.PESTEL_Last_Modified_Date__c == null);
		System.assert(oAccountPlan2.SWOT_Last_Modified_Date__c == null);
		System.assert(oAccountPlan2.Medtronic_Last_Modified_Date__c == null);
		System.assert(oAccountPlan2.Care_Abouts_Last_Modified_Date__c == null);
		//------------------------------------------------------


		//------------------------------------------------------
		// Execute Logic
		//------------------------------------------------------
		Test.startTest();

			// PESTEL LOGIC
				oAccountPlan2.Political__c = 'TEST';
			update oAccountPlan2;

			oAccountPlan2 = 
				[
					SELECT 
						Id
						, Political__c, Economic__c, Legal__c, Environmental__c, Technological__c, Social__c, PESTEL_Last_Modified_Date__c
						, Other_Strengths__c, Other_Opportunities__c, Other_Weaknesses__c, Other_Threats__c, Strengths__c, Opportunities__c, Weaknesses__c, Threats__c, Needs__c, SWOT_Last_Modified_Date__c
						, Medtronic_Strengths__c, Medtronic_Opportunities__c, Medtronic_Weaknesses__c, Medtronic_Threats__c, Medtronic_Potential_Synergies__c, Medtronic_Last_Modified_Date__c
						, Them__c, Us__c, Fit__c, Proof__c, Care_Abouts_Last_Modified_Date__c
					FROM Account_Plan_2__c
					WHERE Id = :oAccountPlan2.Id
				];
			System.assert(oAccountPlan2.PESTEL_Last_Modified_Date__c != null);
			System.assert(oAccountPlan2.SWOT_Last_Modified_Date__c == null);
			System.assert(oAccountPlan2.Medtronic_Last_Modified_Date__c == null);
			System.assert(oAccountPlan2.Care_Abouts_Last_Modified_Date__c == null);


			// SWOT LOGIC
				oAccountPlan2.Other_Strengths__c = 'TEST';
			update oAccountPlan2;

			oAccountPlan2 = 
				[
					SELECT 
						Id
						, Political__c, Economic__c, Legal__c, Environmental__c, Technological__c, Social__c, PESTEL_Last_Modified_Date__c
						, Other_Strengths__c, Other_Opportunities__c, Other_Weaknesses__c, Other_Threats__c, Strengths__c, Opportunities__c, Weaknesses__c, Threats__c, Needs__c, SWOT_Last_Modified_Date__c
						, Medtronic_Strengths__c, Medtronic_Opportunities__c, Medtronic_Weaknesses__c, Medtronic_Threats__c, Medtronic_Potential_Synergies__c, Medtronic_Last_Modified_Date__c
						, Them__c, Us__c, Fit__c, Proof__c, Care_Abouts_Last_Modified_Date__c
					FROM Account_Plan_2__c
					WHERE Id = :oAccountPlan2.Id
				];
			System.assert(oAccountPlan2.PESTEL_Last_Modified_Date__c != null);
			System.assert(oAccountPlan2.SWOT_Last_Modified_Date__c != null);
			System.assert(oAccountPlan2.Medtronic_Last_Modified_Date__c == null);
			System.assert(oAccountPlan2.Care_Abouts_Last_Modified_Date__c == null);


			// MEDTRONIC LOGIC
				oAccountPlan2.Medtronic_Strengths__c = 'TEST';
			update oAccountPlan2;

			oAccountPlan2 = 
				[
					SELECT 
						Id
						, Political__c, Economic__c, Legal__c, Environmental__c, Technological__c, Social__c, PESTEL_Last_Modified_Date__c
						, Other_Strengths__c, Other_Opportunities__c, Other_Weaknesses__c, Other_Threats__c, Strengths__c, Opportunities__c, Weaknesses__c, Threats__c, Needs__c, SWOT_Last_Modified_Date__c
						, Medtronic_Strengths__c, Medtronic_Opportunities__c, Medtronic_Weaknesses__c, Medtronic_Threats__c, Medtronic_Potential_Synergies__c, Medtronic_Last_Modified_Date__c
						, Them__c, Us__c, Fit__c, Proof__c, Care_Abouts_Last_Modified_Date__c
					FROM Account_Plan_2__c
					WHERE Id = :oAccountPlan2.Id
				];
			System.assert(oAccountPlan2.PESTEL_Last_Modified_Date__c != null);
			System.assert(oAccountPlan2.SWOT_Last_Modified_Date__c != null);
			System.assert(oAccountPlan2.Medtronic_Last_Modified_Date__c != null);
			System.assert(oAccountPlan2.Care_Abouts_Last_Modified_Date__c == null);


			// CARE ABOUTS Logic
				oAccountPlan2.Them__c = 'TEST';
			update oAccountPlan2;

			oAccountPlan2 = 
				[
					SELECT 
						Id
						, Political__c, Economic__c, Legal__c, Environmental__c, Technological__c, Social__c, PESTEL_Last_Modified_Date__c
						, Other_Strengths__c, Other_Opportunities__c, Other_Weaknesses__c, Other_Threats__c, Strengths__c, Opportunities__c, Weaknesses__c, Threats__c, Needs__c, SWOT_Last_Modified_Date__c
						, Medtronic_Strengths__c, Medtronic_Opportunities__c, Medtronic_Weaknesses__c, Medtronic_Threats__c, Medtronic_Potential_Synergies__c, Medtronic_Last_Modified_Date__c
						, Them__c, Us__c, Fit__c, Proof__c, Care_Abouts_Last_Modified_Date__c
					FROM Account_Plan_2__c
					WHERE Id = :oAccountPlan2.Id
				];
			System.assert(oAccountPlan2.PESTEL_Last_Modified_Date__c != null);
			System.assert(oAccountPlan2.SWOT_Last_Modified_Date__c != null);
			System.assert(oAccountPlan2.Medtronic_Last_Modified_Date__c != null);
			System.assert(oAccountPlan2.Care_Abouts_Last_Modified_Date__c != null);

		Test.stopTest();
		//------------------------------------------------------


		//------------------------------------------------------
		// Validate Result

		//------------------------------------------------------
		//------------------------------------------------------

	}

}