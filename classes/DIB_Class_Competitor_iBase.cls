public with sharing class DIB_Class_Competitor_iBase {

	public DIB_Competitor_iBase__c oDIB_MDT_iBase { get;set; }
	public List<DIB_Competitor_iBase__c> comp_iBase {get; set;}
	
	public DIB_Competitor_iBase__c oDIB_MDT_iBase_CGMS { get;set; }
	public List<DIB_Competitor_iBase__c> comp_iBase_CGMS {get; set;}
	
	public Account SFAccount {get; set;}
	public DIB_Department__c dibDepartment {set;get;}	
	
	public Boolean bDIBDepartment_Active_OldValue { get;set; }
	public String tDIBDepartment_CompetitiveVSMDTAccount_OldValue { get;set; }
	
	public Decimal decChange_T1PatientsNo {get; set;}
	
	public Decimal decChange_MDTInstalledBasePumps{get; set;}
	public Decimal decChange_MDTInstalledBasePumps_NewTotal { get;set; }	//-BC - 20150609 - CR-8901
	public Decimal decChange_MDTInstalledBaseCGMS {get; set;}
	public Decimal decChange_MDTInstalledBaseCGMS_NewTotal { get;set; }		//-BC - 20150609 - CR-8901
	
	public Decimal industryIB_Pumps {get; set;}
	public Decimal industryIB_CGMS {get; set;}
			
	public Boolean bError_T1PatientsNo { get;set; }
	public Boolean bError_MDTInstalledBasePumps { get;set; }
	public Boolean bError_MDTInstalledBaseCGMS { get;set; }

	public Map<String, Boolean> mapComp_Error { get;set; }
	public Map<String, Boolean> mapComp_Error_CGMS { get;set; }

	public DIB_Class_Competitor_iBase(Account acc){
		
		SFAccount = acc;
		SFAccount.System_Account__c = acc.Id;
				
		comp_iBase = new List<DIB_Competitor_iBase__c>();
		comp_iBase_CGMS = new List<DIB_Competitor_iBase__c>();
		mapComp_Error = new Map<String, Boolean>();
		mapComp_Error_CGMS = new Map<String, Boolean>();
	}	
}