//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 05-01-2016
//  Description      : APEX Class - Trigger Handler for Attachment	
//  Change Log       : PMT-22787: MITG EMEA User Setup - Changed Profile check to EMEA.
//--------------------------------------------------------------------------------------------------------------------
public class bl_Attachment_Trigger {


	//------------------------------------------------------------------------------------------------
	// MITG Related logic
	//	When an Attachment is created by an User with an MITG User we need to set the field Linked_Attachments__c on Account or Opportunity to true.
	//	When an Attachment is deleted we need to verify if there are still attachments related to the Account / Opportunity which have been created by an MITG User and set the field Linked_Attachments__c on Account or Opportunity to true or false.
	//------------------------------------------------------------------------------------------------
	public static void createAttachment_MITG(List<Attachment> lstTriggerNew){

		if (bl_Trigger_Deactivation.isTriggerDeactivated('Att_createAttachment_MITG')) return;

		Set<Id> setID_UserProfile_MITG = new Set<Id>();
        	//PMT-22787
        	//New profile EMEA Field Force MITG will replace profiles: EUR Field Force MITG and MEA Field Force MITG
        	setID_UserProfile_MITG.add(clsUtil.getUserProfileId('EMEA Field Force MITG'));
        
        	//PMT-22787
        	//Delete old profile after migration (Later Release)
			setID_UserProfile_MITG.add(clsUtil.getUserProfileId('EUR Field Force MITG'));
			setID_UserProfile_MITG.add(clsUtil.getUserProfileId('MEA Field Force MITG'));
        	
		if (!setID_UserProfile_MITG.contains(UserInfo.getProfileId())) return;

		Set<Id> setID_Account = new Set<Id>();
		Set<Id> setID_Opportunity = new Set<Id>();
		for (Attachment oAttachment : lstTriggerNew){
			
			String tParentId = oAttachment.ParentId;
			if (tParentId.startsWith('001')){
				setID_Account.add(tParentId);
			}else if (tParentId.startsWith('006')){
				setID_Opportunity.add(tParentId);
			}

		}

		if (setID_Account.size() > 0){

			List<Account> lstAccount = [SELECT Id, Linked_Attachments__c FROM Account WHERE Id = :setID_Account AND Linked_Attachments__c = false];
			
			if (lstAccount.size() > 0){
			
				for (Account oAccount : lstAccount) oAccount.Linked_Attachments__c = true;
				update lstAccount;

			}

		}

		if (setID_Opportunity.size() > 0){

			List<Opportunity> lstOpportunity = [SELECT Id, Linked_Attachments__c FROM Opportunity WHERE Id = :setID_Opportunity AND Linked_Attachments__c = false];
			
			if (lstOpportunity.size() > 0){
			
				for (Opportunity oOpportunity : lstOpportunity) oOpportunity.Linked_Attachments__c = true;
				update lstOpportunity;

			}

		}

	}

	public static void deleteAttachment_MITG(List<Attachment> lstTriggerOld){

		if (bl_Trigger_Deactivation.isTriggerDeactivated('Att_deleteAttachment_MITG')) return;

		Set<Id> setID_UserProfile_MITG = new Set<Id>();
			setID_UserProfile_MITG.add(clsUtil.getUserProfileId('EUR Field Force MITG'));
			setID_UserProfile_MITG.add(clsUtil.getUserProfileId('MEA Field Force MITG'));
        
        	//PMT-22787
        	//New profile EMEA Field Force MITG will replace profiles: EUR Field Force MITG and MEA Field Force MITG
        	setID_UserProfile_MITG.add(clsUtil.getUserProfileId('EMEA Field Force MITG'));

		Set<Id> setID_Account = new Set<Id>();
		Set<Id> setID_Opportunity = new Set<Id>();
		for (Attachment oAttachment : lstTriggerOld){

			String tParentId = oAttachment.ParentId;
			if (tParentId.startsWith('001')){
				setID_Account.add(tParentId);
			}else if (tParentId.startsWith('006')){
				setID_Opportunity.add(tParentId);
			}

		}
		
		if (setID_Account.size() > 0){

			List<Account> lstAccount_Update = new List<Account>();
			List<Account> lstAccount = [SELECT Id, Linked_Attachments__c, (SELECT Id FROM Attachments WHERE CreatedBy.ProfileId = :setID_UserProfile_MITG) FROM Account WHERE Id = :setID_Account];

			for (Account oAccount : lstAccount){

				if ( (oAccount.Attachments.size() == 0) && (oAccount.Linked_Attachments__c == true) ){

					oAccount.Linked_Attachments__c = false;
					lstAccount_Update.add(oAccount);
				
				}
			}

			if (lstAccount_Update.size() > 0) update lstAccount_Update;
		}


		if (setID_Opportunity.size() > 0){

			List<Opportunity> lstOpportunity_Update = new List<Opportunity>();
			List<Opportunity> lstOpportunity = [SELECT Id, Linked_Attachments__c, (SELECT Id FROM Attachments WHERE CreatedBy.ProfileId = :setID_UserProfile_MITG) FROM Opportunity WHERE Id = :setID_Opportunity];
			for (Opportunity oOpportunity : lstOpportunity){
	
				if ( (oOpportunity.Attachments.size() == 0) && (oOpportunity.Linked_Attachments__c == true) ){

					oOpportunity.Linked_Attachments__c = false;
					lstOpportunity_Update.add(oOpportunity);
				
				}
			}

			if (lstOpportunity_Update.size() > 0) update lstOpportunity_Update;
		}

	}
	//------------------------------------------------------------------------------------------------



	//------------------------------------------------------------------------------------------------
	// SAP doesn't process files that have multiple dot like a.b.c.txt This method replaces dot with _
	// before senidng attachment to SAP. Its done in before insert as the file is processed after insert
	//------------------------------------------------------------------------------------------------
	public static void renameAttachment(List<Attachment> lstTriggerNew){
		
		if (bl_Trigger_Deactivation.isTriggerDeactivated('Att_renameAttachment')) return;

		if(UserInfo.getProfileId() != clsUtil.getUserProfileId('SAP Interface') && UserInfo.getProfileId() != clsUtil.getUserProfileId('SMAX Interface')){
			
			for (Attachment att : lstTriggerNew){
				
				if (String.valueof(att.ParentId.getSObjectType()) == 'SVMXC__Service_Order__c'){
				
					String fName = att.Name;
					
					// Replace all but the last dot with underscore if there is more than one.
					if(fName != null && fName.countMatches('.') > 1){
												
						Integer dotlocation = fname.lastIndexOf('.');
						
						String filename = fname.substring(0,dotlocation);
						String fileext = '.' + fname.substring(dotlocation+1,fname.length());
												
						att.Name = filename.replaceAll('\\.','_') + fileext;						
					}
				}
			}
		}
	}
	
	@TestVisible
	private static Boolean unitTestRunInterface = false;
	
	public static void sendAttachmentDocumentum(List<Attachment> triggerNew){
		
		if(Test.isRunningTest() == true && unitTestRunInterface == false) return;
		
		List<Id> toSend = new List<Id>();
		
		for(Attachment att : triggerNew){
			
			 if(String.valueof(att.ParentId.getSObjectType()) == 'Contract_Repository_Document_Version__c') toSend.add(att.Id);
		}
		
		if(toSend.size() == 1){
			
			bl_Contract_Repository_Interface.sendDocumentVersionNotification(toSend[0]);
			
		}else if(toSend.size() > 1){
			
			ba_Contract_Repository_Inteface batch = new ba_Contract_Repository_Inteface(toSend); 
        	Database.executeBatch(batch, 1);
		}
	}
	
	//------------------------------------------------------------------------------------------------
	// Public Methods that will process the records
	//------------------------------------------------------------------------------------------------
	// CGEN2 - Send Notification to SAP / WebMethods
	//------------------------------------------------------------------------------------------------
	public static void notificationSAP(List<Attachment> lstTriggerNew, String tAction){

		if (bl_Trigger_Deactivation.isTriggerDeactivated('Att_notificationSAP')) return;

		if (UserInfo.getProfileId() != clsUtil.getUserProfileId('SAP Interface') && UserInfo.getProfileId() != clsUtil.getUserProfileId('SMAX Interface')){

	        Set<Id> sOrderIds = new Set<Id>();
	        List<Attachment> correctAttachments = new List<Attachment>();
	        List<Attachment> failedAttachments = new List<Attachment>();
	        Map<Id, String> attachmentErrors = new Map<Id, String>();

			for (Attachment oData : lstTriggerNew){

				if (tAction == 'INSERT'){

	                // Only process attachments that are related to :
	                //  - SVMXC__Service_Order__c
	                String tId = clsUtil.isNull(oData.ParentId, '');

			        if (String.valueof(oData.ParentId.getSObjectType()) == 'SVMXC__Service_Order__c'){
	                	
	                	sOrderIds.add(tId);
	                	
	                	String validationError = checkAttachmentFileName(oData);
	                	
	                	//We only send it if the attachment fullfils the restrictions on extension type
	                	if(validationError == null) correctAttachments.add(oData);	                		                		
						else{
							
							failedAttachments.add(oData);
							attachmentErrors.put(oData.Id, validationError);
						}
	                }
	            }
			}
			
			if(sOrderIds.size() > 0){
				
				// Select the Service Orders to validate if they have a SAP ID
				//	Only the attachments that are linked to a Service Order which has an SAP ID need to be processed
				Map<Id, SVMXC__Service_Order__c> sOrders = new Map<Id, SVMXC__Service_Order__c>([SELECT Id, Name, SVMX_SAP_Service_Order_No__c, SVMXC__Country__c,
																									SVMXC__Service_Group__r.SVMX_Service_Team_emails__c FROM SVMXC__Service_Order__c WHERE Id = :sOrderIds]);
			
				List<Attachment> lstData_Processing = new List<Attachment>();
				
				for(Attachment att : correctAttachments){
					
					if(sOrders.get(att.ParentId).SVMX_SAP_Service_Order_No__c != null) lstData_Processing.add(att);
				}
	
				// Process the collected Attachments
		        if (lstData_Processing.size() > 0 && System.isBatch() == false) bl_NotificationSAP.sendNotificationToSAP_Attachment(lstData_Processing, tAction);
		        		        
		        if(failedAttachments.size() > 0) notifyAttachmentFailures(failedAttachments, attachmentErrors, sOrders);
			}
	    }
	}
	
	private static Attachment_Settings__c setting{
		
		get{
			
			if(setting == null)	setting = Attachment_Settings__c.getValues('Service Order Attachment');
			
			return setting;
		}
		
		set;
	}
	
	private static Integer maxLength{
		
		get{
			
			if(maxLength == null){
				
				if(setting != null && setting.Filename_Max_Length__c != null) maxLength = Integer.valueOf(setting.Filename_Max_Length__c);
				else maxLength = 0;		
			}
			
			return maxLength;
		}
		
		set;
	}
	
	private static List<String> extensions{
		
		get{
			
			if(extensions == null){
				
				if(setting != null && setting.File_Extensions_Allowed__c != null) extensions = setting.File_Extensions_Allowed__c.split(',');
				else extensions = new List<String>();				
			}
			
			return extensions;
		}
		
		set;
	}
	
	//------------------------------------------------------------------------------------------------
	// SAP doesn't process filenames that are too long, or, are not certain type (extentions like dos, xlsx, csv etc.)
	// Call this method to verify any attachment before sending to SAP.
	// Method returns true if valid filename else false if not a valid filename.	
	private static String checkAttachmentFileName(Attachment att){
			
		String fName = att.Name;
		
		Integer dotlocation = fname.lastIndexOf('.');
		String filename = '';
		String fileext = '';
		
		if(dotlocation != -1){
			
			filename = fname.substring(0,dotlocation);
			fileext = fname.substring(dotlocation+1,fname.length());
			
		}else filename = fname;                	
				
		if(filename.length() > maxLength) return 'Filename (' + filename +') too long:' + filename.length() + ', Allowed: ' + maxLength;
						
		Boolean extensionFound = false;
		
		for(String extension : extensions){
						  
			if(fileext.equalsIgnoreCase(extension.trim())){
				extensionFound = true;
				break;
			}
		}   
		
		if(extensionFound == false) return 'File extension (' + fileext + ') not valid. Allowed: ' + setting.File_Extensions_Allowed__c;
		
		return null;
	}
	
	private static void notifyAttachmentFailures(List<Attachment> attList, Map<Id, String> attErrors, Map<Id, SVMXC__Service_Order__c> sOrders){
		
		// Send email if the file doesnt follow naming convention
		try{
			
			List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
			
			for(Attachment att : attList){
		
				// Get the order details
				SVMXC__Service_Order__c serviceOrder = sOrders.get(att.ParentId);
	
				List<String> lstTO = new List<String>();
						
				//get the email address that the email needs to be sent to. First try sending to
				//the WOs service teams email. If there is not one then use the email settings in
				//the custom settings.
				if(serviceOrder != null	&& serviceOrder.SVMXC__Service_Group__r != null 
					&& serviceOrder.SVMXC__Service_Group__r.SVMX_Service_Team_emails__c != null){
										
					String toaddr = serviceOrder.SVMXC__Service_Group__r.SVMX_Service_Team_emails__c;
					toaddr = toaddr.replaceAll(' ','');
					lstTO = toaddr.split(',');
					
				} else {
					
					SMAX_Email_Settings__c oEmailSettings = SMAX_Email_Settings__c.getInstance('ATTACHMENTS');
					if (oEmailSettings != null){
										
						String toaddr = oEmailSettings.TO__c;
						toaddr = toaddr.replaceAll(' ','');
						lstTO = toaddr.split(',');	
								
					} 				
				}
	
				//create the single email message
				Messaging.SingleEmailMessage oEmail = new Messaging.SingleEmailMessage();
				oEmail.setToAddresses(lstTo);				
				oEmail.setUseSignature(false);
				oEmail.setSaveAsActivity(false);
				
				String errMessage = attErrors.get(att.Id);
				
				if (serviceOrder != null){
					
					if(serviceOrder.SVMXC__Country__c != null)	oEmail.setSubject(serviceOrder.SVMXC__Country__c + ': Attachment Not Valid for SAP');
					else oEmail.setSubject('Attachment Not Valid for SAP');
						
					oEmail.setPlainTextBody(errMessage + '\n. Work order id: ' + att.parentId + ', Work order number ' + serviceOrder.Name + ', SAP service order number ' + 
							serviceOrder.SVMX_SAP_Service_Order_No__c + ' . Attachment (' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + att.id + ') did not meet the specified criteria to automatically attach and must be manually looked at. ');
		
				} else {
					
					oEmail.setSubject('Attachment Not Valid for SAP');
					oEmail.setPlainTextBody(errMessage + '\n. Attachment (' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + att.id + ') did not meet the specified criteria to automatically attach and must be manually looked at.');
				}
		
				// Create the email attachment
				Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
				efa.setFileName(att.Name);
				efa.setBody(att.Body);
				oEmail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
		
				emails.add(oEmail);
			}
			
			// Sends the email
			List<Messaging.SendEmailResult> res = Messaging.sendEmail(emails);   
			System.debug('Email Result ' + res);
	
		} catch(Exception ex) {
						
			WebServiceSetting__c oWebServiceSetting = bl_NotificationSAP.loadWebServiceSetting('Attachment_NotificationSAP');
			
			if (clsUtil.isNull(oWebServiceSetting.Callout_Failure_Email_Address__c, '') != ''){
			
				List<String> lstTOAddress = oWebServiceSetting.Callout_Failure_Email_Address__c.split(',');
				
				List<Id> attachmentIds = new List<Id>();
				for(Id attId : attErrors.keySet()) attachmentIds.add(attId);
						
				// Send exception to admin
				Messaging.SingleEmailMessage aEmail = new Messaging.SingleEmailMessage();
				aEmail.setToAddresses(lstTOAddress);
				aEmail.setSubject('Email for Attachment Ids  ' + String.join(attachmentIds, ','));
				aEmail.setUseSignature(false);
				aEmail.setSaveAsActivity(true);
				aEmail.setPlainTextBody(ex.getMessage());
				Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {aEmail});   
				System.debug('Admin Email result: ' + r);
			}
		}
	}
}
//--------------------------------------------------------------------------------------------------------------------