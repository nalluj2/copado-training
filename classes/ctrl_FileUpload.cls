global class ctrl_FileUpload {

	//----------------------------------------------------------------------------------------------------------------
	//	Description: Method to delete un attachment.
	//	Parameters:
	//    	tAttachmentId: The record of the current Attachment file to delete
	//----------------------------------------------------------------------------------------------------------------
	@RemoteAction
	global static void deleteAttachment(String tAttachmentId){

		//Delete the attachment
		delete new Attachment(Id = tAttachmentId);

	}
	//----------------------------------------------------------------------------------------------------------------

}