/*
 *      Description	: This Class is the Test Class for the APEX Class ctrl_DIB_IB_Adjustment
 *		Version		: 1.0
 *      Author 		: Bart Caelen
 *		Date		: 201401002
*/
@isTest private class TEST_ctrl_DIB_IB_Adjustment {
	
	@isTest static void test_ctrl_DIB_IB_Adjustment() {

    	Boolean bResult = false;
    	String tResult = '';

    	String tMonth = String.valueOf(Date.Today().month()); 
    	String tYear = String.valueOf(Date.Today().year());
    	if (tMonth.length() == 1) tMonth = '0' + tMonth;
    	if (tYear.length() == 2) tYear = '20' + tYear;
    	
		//------------------------------------------------------------------
		// Create Test Data
		//------------------------------------------------------------------

		// Create Master Data
		clsTestData.createBusinessUnitData();
		clsTestData.createSubBusinessUnitData();

		// Create Account Data
		clsTestData.idRecordType_Account = RecordTypeMedtronic.Account('SAP_Account').Id;
		clsTestData.createAccountData();

		// Create DIB_Department__c Data
		clsTestData.createDepartmentMasterData();
		clsTestData.createDIBDepartmentData();

    	// Create Competitor Data
    	Set<String> setCompetitorName = new Set<String>();
    		setCompetitorName.add('Animas');
    		setCompetitorName.add('Cellnovo');
    		setCompetitorName.add('Roche');
    		setCompetitorName.add('Other');
    		setCompetitorName.add('Patch');
    		setCompetitorName.add('Dexcom');
    		setCompetitorName.add('Abott');
    	List<Account> lstAccount_Competitor = new List<Account>();
    	for (String tCompetitorName : setCompetitorName){
	    	Account oAccount_Competitor = new Account() ; 
	    		oAccount_Competitor.name = tCompetitorName;
	    		oAccount_Competitor.Diabetes__c = true;
	    		oAccount_Competitor.CGMs__c = true;
	    		oAccount_Competitor.Pumps__c = true;
	    		oAccount_Competitor.Account_Active__c = true;
	    		oAccount_Competitor.RecordTypeId = RecordTypeMedtronic.getRecordTypeByDevName('Account', 'DIB_Competitor').Id;
    		lstAccount_Competitor.add(oAccount_Competitor);
    	}
    	insert lstAccount_Competitor;


		// Create DIB Competitor Install Base Data	    	   	    
		Set<String> setType = new Set<String>();
			setType.add('Pumps');
			setType.add('CGMS');
    	List<DIB_Competitor_iBase__c> lstDIBCompetitorIBase = new List<DIB_Competitor_iBase__c>(); 
    	for (Account oAccount : clsTestData.lstAccount){
    		for (Account oAccount_Competitor : lstAccount_Competitor){
    			for (DIB_Department__c oDIBDepartment : clsTestData.lstDIBDepartment){
	    			for (String tType : setType){
			    		DIB_Competitor_iBase__c oDIBCompetitorIBase = new DIB_Competitor_iBase__c(); 
			    			oDIBCompetitorIBase.Current_IB__c = 10;   	    		
			    			oDIBCompetitorIBase.Competitor_Account_ID__c = oAccount_Competitor.Id; 
				    		oDIBCompetitorIBase.Sales_Force_Account__c = oAccount.Id;
				    		oDIBCompetitorIBase.Fiscal_year__c = tYear; 
				    		oDIBCompetitorIBase.Fiscal_Month__c = tMonth;
							oDIBCompetitorIBase.IB_Change__c = 23;
							oDIBCompetitorIBase.Account_Segmentation__c = oDIBDepartment.Id;
							oDIBCompetitorIBase.Type__c = tType;
						lstDIBCompetitorIBase.add(oDIBCompetitorIBase);
	    			}
	    		}
    		}
    	}
    	insert lstDIBCompetitorIBase; 
		//------------------------------------------------------------------

		
		//------------------------------------------------------------------
		// Perform Tests
		//------------------------------------------------------------------

    	Test.startTest();
    	
    	PageReference pPageReference = Page.DIB_IB_Adjustment;
    	Test.setCurrentPage(pPageReference);

		ctrl_DIB_IB_Adjustment oCtrl = new ctrl_DIB_IB_Adjustment();

		oCtrl.searchCriteria = 'TEST Account';
		oCtrl.selectedPeriod = tMonth + '-' + tYear;  
		oCtrl.displayCompetitors() ;

		oCtrl.searchCriteria = '';
		oCtrl.departmentSegment = 'top';
		oCtrl.selectedPeriod = tMonth + '-' + tYear;  
		oCtrl.displayCompetitors() ;
		
		oCtrl.searchCriteria = '';
		oCtrl.departmentSegment = '';
		oCtrl.selectedPeriod = tMonth + '-' + tYear;  
		oCtrl.displayCompetitors() ;
		
		bResult = oCtrl.showSubmitButton; 
		tResult = oCtrl.selectedPeriod; 
		bResult = oCtrl.showSubmitButton; 
		oCtrl.showSubmitButton = true;
		oCtrl.save() ;			
			
		oCtrl.searchCriteria = '';
		oCtrl.departmentSegment = '';
		oCtrl.selectedPeriod = tMonth + '-' + tYear;  
		oCtrl.displayCompetitors() ;


		for (DIB_Class_Competitor_iBase oDIB_Class_Comp_iBase : oCtrl.DIB_Comp_Class_iBases) {
			oDIB_Class_Comp_iBase.decChange_MDTInstalledBaseCGMS = 10;
			oDIB_Class_Comp_iBase.decChange_MDTInstalledBasePumps = 10;
			oDIB_Class_Comp_iBase.decChange_T1PatientsNo = 10;
			oDIB_Class_Comp_iBase.dibDepartment.Active__c = true;

			oDIB_Class_Comp_iBase.oDIB_MDT_iBase.SYSTEM_FIELD_Month_IB__c = 10;

			List<DIB_Competitor_iBase__c> lstComp = oDIB_Class_Comp_iBase.comp_iBase;
			for (DIB_Competitor_iBase__c oComp : lstComp){
				oComp.SYSTEM_FIELD_Month_IB__c = 10;
			}
		}

		oCtrl.save() ;			
			
		oCtrl.searchCriteria = '';
		oCtrl.departmentSegment = '';
		oCtrl.selectedPeriod = tMonth + '-' + tYear;  
		oCtrl.displayCompetitors() ;

			
		oCtrl.selectedPeriod = tMonth + '-' + tYear;
		Boolean hasNext = oCtrl.hasNext ; 
		Boolean hasPrevious = oCtrl.hasPrevious;
		List<SelectOption> pages = oCtrl.pages; 
		oCtrl.SelectedPage = String.valueOf(1) ; 
		oCtrl.changePage();
		oCtrl.first();			
		oCtrl.next();
		oCtrl.previous();
		oCtrl.last();
		oCtrl.SubmitcurrentPeriod();			 

		oCtrl.getTerritoryFilterOptions();
		oCtrl.getSegmentFilterOptions();
		
		Test.stopTest();

		//------------------------------------------------------------------

    }
    
    
	// CR-10043		christophe saenen	20160112
    public static testMethod void testRunAsUser() {
	    // Setup test data
	    // This code runs as the system user
	    Profile p = [SELECT Id FROM Profile WHERE Name='EUR DiB Sales Entry'];
						
        User testUser = new User();
        testUser.alias = 'stadib1';
        testUser.email='dib@medtronic.test.com';
        testUser.emailencodingkey='UTF-8';
        testUser.lastname='TestCAP1';
        testUser.languagelocalekey='en_US';
        testUser.localesidkey='en_US';
        testUser.profileid = p.Id;
        testUser.timezonesidkey='America/Los_Angeles';
        testUser.Country_vs__c = 'NETHERLANDS';
        testUser.Region_vs__c = 'Europe';
        testUser.Sub_Region__c = 'NWE';     
        testUser.username='dib@medtronic.test.com';
        testUser.Alias_unique__c='standarduser_Alias_unique__c';
        testUser.Company_Code_Text__c='T45';
        						

		Account oAccount_or = new Account();
        oAccount_or.Name = 'TEST Account OR'; 
        oAccount_or.Account_Country_vs__c = 'BELGIUM';
        oAccount_or.RecordTypeId = clsUtil.getRecordTypeByDevName('Account', 'NON_SAP_Account').Id;
        oAccount_or.Account_Active__c = true;
        oAccount_or.isSales_Force_Account__c = true;
         
        insert oAccount_or;		
        
        
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       
    	Boolean bResult = false;
    	String tResult = '';

    	String tMonth = String.valueOf(Date.Today().month()); 
    	String tYear = String.valueOf(Date.Today().year());
    	if (tMonth.length() == 1) tMonth = '0' + tMonth;
    	if (tYear.length() == 2) tYear = '20' + tYear;
    	
		//------------------------------------------------------------------
		// Create Test Data
		//------------------------------------------------------------------

		// Create Master Data
		clsTestData.createBusinessUnitData();
		clsTestData.createSubBusinessUnitData();

		// Create Account Data
		clsTestData.idRecordType_Account = RecordTypeMedtronic.Account('SAP_Account').Id;
		clsTestData.createAccountData();

		// Create DIB_Department__c Data
		clsTestData.createDepartmentMasterData();
		clsTestData.createDIBDepartmentData();

    	// Create Competitor Data
    	Set<String> setCompetitorName = new Set<String>();
    		setCompetitorName.add('Animas');
    		setCompetitorName.add('Cellnovo');
    		setCompetitorName.add('Roche');
    		setCompetitorName.add('Other');
    		setCompetitorName.add('Patch');
    		setCompetitorName.add('Dexcom');
    		setCompetitorName.add('Abott');
    	List<Account> lstAccount_Competitor = new List<Account>();
    	for (String tCompetitorName : setCompetitorName){
	    	Account oAccount_Competitor = new Account() ; 
	    		oAccount_Competitor.name = tCompetitorName;
	    		oAccount_Competitor.Diabetes__c = true;
	    		oAccount_Competitor.CGMs__c = true;
	    		oAccount_Competitor.Pumps__c = true;
	    		oAccount_Competitor.Account_Active__c = true;
	    		oAccount_Competitor.RecordTypeId = RecordTypeMedtronic.getRecordTypeByDevName('Account', 'DIB_Competitor').Id;
    		lstAccount_Competitor.add(oAccount_Competitor);
    	}
    	insert lstAccount_Competitor;


		// Create DIB Competitor Install Base Data	    	   	    
		Set<String> setType = new Set<String>();
			setType.add('Pumps');
			setType.add('CGMS');
    	List<DIB_Competitor_iBase__c> lstDIBCompetitorIBase = new List<DIB_Competitor_iBase__c>(); 
		list<Account> lstDIB = new list<Account>();
    	for (Account oAccount : clsTestData.lstAccount){
    		for (Account oAccount_Competitor : lstAccount_Competitor){
    			for (DIB_Department__c oDIBDepartment : clsTestData.lstDIBDepartment){
	    			for (String tType : setType){
			    		DIB_Competitor_iBase__c oDIBCompetitorIBase = new DIB_Competitor_iBase__c(); 
			    			oDIBCompetitorIBase.Current_IB__c = 10;   	    		
			    			oDIBCompetitorIBase.Competitor_Account_ID__c = oAccount_Competitor.Id; 
				    		oDIBCompetitorIBase.Sales_Force_Account__c = oAccount_or.Id; //oAccount.Id;
				    		oDIBCompetitorIBase.Fiscal_year__c = tYear; 
				    		oDIBCompetitorIBase.Fiscal_Month__c = tMonth;
							oDIBCompetitorIBase.IB_Change__c = 23;
							oDIBCompetitorIBase.Account_Segmentation__c = oDIBDepartment.Id;
							oDIBCompetitorIBase.Type__c = tType;
						lstDIBCompetitorIBase.add(oDIBCompetitorIBase);
	    			}
	    		}
    		}
    	}
    	insert lstDIBCompetitorIBase; 
		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

		system.debug('### clsTestData.lstDIBDepartment.size() =' +  clsTestData.lstDIBDepartment.size() + ' ' + clsTestData.lstDIBDepartment[0].Active__c);
		system.debug('### lstDIBCompetitorIBase.size() =' +  lstDIBCompetitorIBase.size());


	    System.runAs(testUser) {
	       	// The following code runs as user 'u'
	       	System.debug('Current User: ' + UserInfo.getUserName());
	       	System.debug('Current Profile: ' + UserInfo.getProfileId());
			
			ctrl_DIB_IB_Adjustment oCtrl = new ctrl_DIB_IB_Adjustment();
			oCtrl.selectedMonth = tMonth; 
			oCtrl.selectedYear = tYear;

			oCtrl.displayCompetitors();  
			
			system.debug('### oCtrl.DIB_Comp_Class_iBases.size() =' +  oCtrl.DIB_Comp_Class_iBases.size());
			
			system.debug('### totaaldepartment =' + [select id, Account__r.name, Account__r.isSales_Force_Account__c, Account__r.Account_Active__c, Active__c from DIB_Department__c].size());
			for(DIB_Department__c d : [select id, Account__r.isSales_Force_Account__c, Account__r.Account_Active__c, Active__c from DIB_Department__c]) {
				system.debug('### result:'+ d.Account__r.name + ' ' + d.Account__r.isSales_Force_Account__c + ' ' + d.Account__r.Account_Active__c +' '+ d.Active__c );
			}
			system.debug('### oCtrl.DIB_Comp_Class_iBases.size() =' + oCtrl.DIB_Comp_Class_iBases.size());
			system.debug('### totaaldepartmentibase =' + [select id from DIB_Competitor_iBase__c ].size());
			
			try {oCtrl.DIB_Comp_Class_iBases[0].dibDepartment.Active__c = false;}
			catch(exception c){}
			
			oCtrl.save();
			
			oCtrl.displayCompetitors();  

			system.debug('### oCtrl.DIB_Comp_Class_iBases.size() =' + oCtrl.DIB_Comp_Class_iBases.size());
			
	    }
	    
   	}
    
    @TestSetup
    private static void createCustomSettings(){
    	
    	My_IB_Update__c settings = My_IB_Update__c.getValues('My IB Update');
    	
    	if(settings == null){
    		
    		settings = new My_IB_Update__c();
    		settings.name = 'My IB Update';
    		settings.New_Segmentation_Link__c = 'test';
    		insert settings;
    	}
    	
    }
}
//------------------------------------------------------------------------------------------------------