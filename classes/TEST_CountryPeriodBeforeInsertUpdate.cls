/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TEST_CountryPeriodBeforeInsertUpdate {

    static testMethod void myUnitTest() {
        DIB_Country__c country = new DIB_Country__c();
        country.Name = 'Test Country';
        country.Country_ISO_Code__c = 'TS';
        insert country;
        
        Test.startTest();
        
        DIB_Country_Period__c countryPeriod1 = new DIB_Country_Period__c();
        countryPeriod1.DIB_Country__c = country.Id;
        countryPeriod1.Country_Period_Fiscal_Month__c = '01';
        countryPeriod1.Country_Period_Fiscal_Year__c = '2010';
        countryPeriod1.Attrition_CGMS__c=0.5;
        countryPeriod1.Attrition_Pumps__c=0.5;
        insert countryPeriod1;
        
        DIB_Country_Period__c countryPeriod2 = new DIB_Country_Period__c();
        countryPeriod2.DIB_Country__c = country.Id;
        countryPeriod2.Country_Period_Fiscal_Month__c = '01';
        countryPeriod2.Country_Period_Fiscal_Year__c = '2010';
        countryPeriod2.Attrition_CGMS__c=0.5;
        countryPeriod2.Attrition_Pumps__c=0.5;
        try{
        	insert countryPeriod2;        
        }catch(Exception e){
        	System.assert(true);
        }
        /*System.assertEquals(countryPeriod2.Id, null);
        
        countryPeriod2.Country_Period_Fiscal_Month__c = '02';
        insert countryPeriod2; 
        
        System.assertNotEquals(countryPeriod2.Id, null);*/
        
        Test.stopTest();
    }
}