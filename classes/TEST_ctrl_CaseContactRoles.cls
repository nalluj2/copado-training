@isTest
private class TEST_ctrl_CaseContactRoles {

	private static Case oCase;

	@isTest static void createTestData() {

        Account oAccount = new Account();
	        oAccount.Name = 'TEST Account 001';
	        oAccount.Account_Country_vs__c = 'FRANCE';
        insert oAccount;
        
        Contact oContact = new Contact();
	        oContact.AccountId = oAccount.Id;
	        oContact.LastName = 'TEST Contact 001';
	        oContact.FirstName = 'TEST Contact 001';
	        oContact.Contact_Gender__c = 'Male';
	        oContact.Contact_Primary_Specialty__c = 'Abdominal Surgery';
	        oContact.Affiliation_To_Account__c = 'Associate';
	        oContact.Contact_Department__c = 'Administration';
	        oContact.Primary_Job_Title_vs__c = 'Director';
	        oContact.MailingCountry = 'FRANCE';
        insert oContact;

        oCase = new Case();        
	        oCase.Status = 'New';
    	    oCase.Origin = 'Patient';
        insert oCase;
        
        Case_Contact_Roles__c oCaseContactRoles = new Case_Contact_Roles__c();
	        oCaseContactRoles.Role__c = 'Other';
	        oCaseContactRoles.Contact__c = oContact.Id;
	        oCaseContactRoles.Case__c = oCase.Id;
        insert oCaseContactRoles;
        
	}
	
	@isTest static void test_method_two() {

		//-----------------------------------------------
		// Create Test Data
		//-----------------------------------------------
		createTestData();
		//-----------------------------------------------


		//-----------------------------------------------
		// Execute Test Logic
		//-----------------------------------------------
        ctrl_CaseContactRoles oCTRL = new ctrl_CaseContactRoles();
    	oCTRL.tCaseId = oCase.Id;
    	Map<Integer, Case_Contact_Roles__c> mapCaseContactRoles = oCTRL.getMapCaseContactRoles();
		//-----------------------------------------------


		//-----------------------------------------------
		// Validate Test Result
		//-----------------------------------------------
		System.assertEquals(mapCaseContactRoles.size(), 1);
		System.assertEquals(mapCaseContactRoles.get(1).Contact__r.Name, 'TEST Contact 001 TEST Contact 001');
		System.assertEquals(mapCaseContactRoles.get(1).Role__c, 'Other');
		//-----------------------------------------------

	}
	
}