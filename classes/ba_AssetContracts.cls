/*
 * Author 		:	Christophe Saenen
 * Date			:	20160426
 * Description	:	CR-11038 
 */
 
global class ba_AssetContracts implements Schedulable,Database.Batchable<sObject>, Database.Stateful {

	global String tProcessType;
	global Set<Id> setID_Asset_Processed;

	private Set<String> setContractStatus_NotAcive = new Set<String>{'Expired', 'Canceled'};

	global void execute(SchedulableContext sc){

    	ba_AssetContracts oBatch = new ba_AssetContracts();
			oBatch.tProcessType = 'ACTIVE_OR_INACTIVE';
        Database.executebatch(oBatch, 200);        	

    } 

    global database.querylocator start(Database.BatchableContext bc){

        String tQuery = '';

		if (tProcessType == 'ACTIVE_OR_INACTIVE'){

			if (setID_Asset_Processed == null) setID_Asset_Processed = new Set<Id>();
			tQuery = 'SELECT Asset__c, Asset__r.Status, Contract__c, Contract__r.Status FROM AssetContract__c WHERE ( Asset__c != null AND Asset__r.Status not in (\'Obsolete\', \'Removed\', \'Returned to US\') )';

		}else if (tProcessType == 'NOT_ACTIVE'){

			if (setID_Asset_Processed == null) setID_Asset_Processed  = new Set<Id>();
			tQuery = 'SELECT Asset__c, Asset__r.Status, Contract__c, Contract__r.Status FROM AssetContract__c WHERE ( Asset__c != null AND Contract__c != null AND Contract__r.Status in (\'Expired\', \'Canceled\') )';

		}

    	return Database.getQueryLocator(tQuery);

    }

    global void execute(Database.BatchableContext bc, List<AssetContract__c> lstAssetContract){
    	
		Set<Id> setID_Asset = new Set<Id>();		

		if (tProcessType == 'ACTIVE_OR_INACTIVE'){

			for (AssetContract__c oAssetContract : lstAssetContract){
		
				if (!setContractStatus_NotAcive.contains(oAssetContract.Contract__r.Status)){

					if (!setID_Asset_Processed.contains(oAssetContract.Asset__c)){

						setID_Asset.add(oAssetContract.Asset__c);
						setID_Asset_Processed.add(oAssetContract.Asset__c);
					
					}

				}

			}

			if (setID_Asset.size() > 0) bl_Contract.calculateCurrentContract(setID_Asset);

		
		}else if (tProcessType == 'NOT_ACTIVE'){

			for (AssetContract__c oAssetContract : lstAssetContract){
		
				if (setContractStatus_NotAcive.contains(oAssetContract.Contract__r.Status)){

					if (!setID_Asset_Processed.contains(oAssetContract.Asset__c)){
	
						setID_Asset.add(oAssetContract.Asset__c);
						setID_Asset_Processed.add(oAssetContract.Asset__c);
					
					}

				}

			}

			if (setID_Asset.size() > 0) bl_Contract.calculateContractNotActive(setID_Asset);

		}

    }
    
    global void finish(Database.BatchableContext bc){

		if (tProcessType == 'ACTIVE_OR_INACTIVE'){

    		ba_AssetContracts oBatch = new ba_AssetContracts();
				oBatch.tProcessType = 'NOT_ACTIVE';
				oBatch.setID_Asset_Processed = setID_Asset_Processed;
			if (!Test.isRunningTest()) Database.executebatch(oBatch, 200);

		}else if (tProcessType == 'NOT_ACTIVE'){

			// THE END

		}

    }

}