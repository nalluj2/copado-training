@isTest
private class Test_ba_IHS_Opportunity_TARP_Reminder {
        
    private static testmethod void testOpportunityTARPReminder(){

    	User currentUser = [Select Alias_Unique__c from User where Id = :UserInfo.getUserId()];
    	
		List<Trigger_Deactivation__c> lstTriggerDeactivation = new List<Trigger_Deactivation__c>();
		Trigger_Deactivation__c oTriggerDeactivation1 = new Trigger_Deactivation__c();
			oTriggerDeactivation1.Name = 'Opp_populateOwnerManagerEmail';
			oTriggerDeactivation1.Deactivate__c = true;
			oTriggerDeactivation1.User_Ids__c = null;
		lstTriggerDeactivation.add(oTriggerDeactivation1);
		Trigger_Deactivation__c oTriggerDeactivation2 = new Trigger_Deactivation__c();
			oTriggerDeactivation2.Name = 'Opp_setBUG_BU_SBU';
			oTriggerDeactivation2.Deactivate__c = true;
			oTriggerDeactivation2.User_Ids__c = null;
		lstTriggerDeactivation.add(oTriggerDeactivation2);
		insert lstTriggerDeactivation;
		bl_Trigger_Deactivation.bIgnoreWhenRunningTest = false;
    	

    	clsTestData_Opportunity.tOpportunityCurrencyIsoCode = 'EUR';
		Opportunity oOpportunity = clsTestData_Opportunity.createOpportunity_IHS_Stage('MANAGEDSERVICE', '4a', true);	// This Opportunity is ready to go to Stage 4a


    	Test.startTest();

		List<Id> lstID_ApprovalWorkitem = new List<Id>();
		Approval.ProcessWorkitemRequest oApprovalWorkitemRequest;
		Approval.ProcessResult oApprovalResult;
		Approval.ProcessSubmitRequest oApprovalSubmitRequest = new Approval.ProcessSubmitRequest();

    			
		oApprovalSubmitRequest = new Approval.ProcessSubmitRequest();
			oApprovalSubmitRequest.setComments('Submitting request for approval.');
			oApprovalSubmitRequest.setObjectId(oOpportunity.id);
    
			// Submit on behalf of a specific submitter
			oApprovalSubmitRequest.setSubmitterId(UserInfo.getUserId()); 
    
    		// Submit the record to specific process and skip the criteria evaluation
			oApprovalSubmitRequest.setProcessDefinitionNameOrId('IHS_Stage_4a_to_4b_approval');
			oApprovalSubmitRequest.setSkipEntryCriteria(true);
    
		// Submit the approval request for the account
		oApprovalResult = Approval.process(oApprovalSubmitRequest);
    
		// Verify the result
		System.assert(oApprovalResult.isSuccess());
		System.assertEquals('Pending', oApprovalResult.getInstanceStatus(), 'Instance Status' + oApprovalResult.getInstanceStatus());
    
		// Approve the submitted request
		// First, get the ID of the newly created item
		lstID_ApprovalWorkitem = oApprovalResult.getNewWorkitemIds();
    
		// Instantiate the new ProcessWorkitemRequest object and populate it
		oApprovalWorkitemRequest = new Approval.ProcessWorkitemRequest();
		oApprovalWorkitemRequest.setComments('Approving request.');
		oApprovalWorkitemRequest.setAction('Approve');
		oApprovalWorkitemRequest.setNextApproverIds(new Id[] {UserInfo.getUserId()});
    
		// Use the ID from the newly created item to specify the item to be worked
		oApprovalWorkitemRequest.setWorkitemId(lstID_ApprovalWorkitem.get(0));
    
		// Submit the request for approval
		oApprovalResult =  Approval.process(oApprovalWorkitemRequest);		
		// Verify the results
		System.assert(oApprovalResult.isSuccess(), 'Result Status:' + oApprovalResult.isSuccess());
		System.assertEquals('Approved', oApprovalResult.getInstanceStatus(),'Instance Status' + oApprovalResult.getInstanceStatus());
	
		oOpportunity = [SELECT Id, StageName, Stage_Completion__c FROM Opportunity WHERE Id = :oOpportunity.Id];
		System.assertEquals(oOpportunity.StageName, '4b - Closed Won (Service Delivery)');
		System.assertEquals(oOpportunity.Stage_Completion__c, 'Ready for Final Stage');
		
		oOpportunity.Contract_Owner__c = UserInfo.getUserId();
		update oOpportunity;
    	
    	Database.executeBatch(new ba_IHS_Opportunity_TARP_Reminder(), 200);   
    	
    	Test.stopTest();
    	
    	List<Task> reminder = [Select Id, Subject, OwnerId from Task where WhatId = :oOpportunity.Id];
    	
    	System.assert(reminder.size() == 1);
    	System.assert(reminder[0].Subject == 'Arrange a TARP with the Account Project Board');
    	System.assert(reminder[0].OwnerId == UserInfo.getUserId());    	
   	
    }
}