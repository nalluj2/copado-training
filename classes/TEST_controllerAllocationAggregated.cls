/**
 * Creation Date :  20091020
 * Description :    Test Coverage of the controller 'controllerAllocationAggregated'.
 *
 *
 * Author :         ABSI - Bob Ceuppen
 */

@isTest
private class TEST_controllerAllocationAggregated {
    
    private datetime currentDateTime;
    private Date currentDate;
    private Date startDate;
    private Date endDate;
    private User currentUser;
    private controllerAllocationAggregated mycontrollerAllocationAggregated;
    private String months;
    private String years;
    private String accountType = 'Adult';
    private Account myTestAccount;
    private DIB_Invoice_Line__c myTestInvoiceLine;
    private String departmentSegment = 'Adult';
 
    static testMethod void myUnitTest() {
        System.debug(' ################## ' + 'BEGIN TEST_controllerAllocationAggregated' + ' ################');
			Id myProfile = clsUtil.getUserProfileId('EUR DiB Sales Entry');
            //User myUser = [Select Id,Country from User where profileId =:myProfile and IsActive=true limit 1];
            User myUser = new User(alias = 'repuser', email='usertest@absi.be', emailencodingkey='UTF-8', lastname='Testing', 
            languagelocalekey='en_US',localesidkey='en_US', profileid = myProfile, timezonesidkey='America/Los_Angeles', 
            username='usertest@absi.be', Alias_unique__c='usertest1234',Company_Code_Text__c='EUR');   

			// Create DIB Fiscal Period Test Data - if needed
	        List<SelectOption> periods = DIB_AllocationSharedMethods.getPeriodLines();
			if (periods.size() == 0) clsTestData_System.createDIBFiscalPeriod();
                
            System.runAs(myUser) {
                System.debug('Current User: ' + UserInfo.getUserName());
                System.debug('Current Profile: ' + UserInfo.getProfileId());
                TEST_controllerAllocationAggregated myTestController = new TEST_controllerAllocationAggregated();
                myTestController.init(myUser);
                myTestController.createRecords();
                myTestController.doTests();
            }
        System.debug(' ################## ' + 'END TEST_controllerAllocationAggregated' + ' ################'); 
    }
    public void init(User myUser) {
        System.debug(' ################## ' + 'TEST_controllerAllocationAggregated_init' + ' ################');
        datetime currentDateTime = datetime.now();
        currentDate = currentDateTime.date();
        startDate = currentDateTime.date().addYears(-2);
        endDate = currentDateTime.date().addYears(+2);
        
        // Get the current fiscal months and current fiscal years. 
        List<SelectOption> periods = DIB_AllocationSharedMethods.getPeriodLines()   ;       
        system.debug('##########################################Period ' + periods ) ; 
        String[] fiscalPeriod = new String[]{} ;        
        for (SelectOption so : periods){
            fiscalPeriod.add(so.getValue());                    
        }
        months = fiscalPeriod.get(0).substring(0,2);
        years = fiscalPeriod.get(0).substring(3,7); 
        
        years = String.valueOf(Integer.valueOf(years) + 10);
        
        
        currentUser = myUser;   
    }
    
    public void doTests() {
        
        System.debug(' ################## ' + 'TEST_controllerAllocationAggregated_doTests' + ' ################');
        Test.startTest();
        
        String periodChosen = months + '-' + years;
        
        DIB_Aggregated_Allocation__c myDIB_Aggregated_Allocation =  new DIB_Aggregated_Allocation__c();
        List<DIB_Class_AccountAggregation> myDIB_Class_AccountAggregations = new List<DIB_Class_AccountAggregation>();
        List<Account> myAccounts = new List<Account>();
        
        this.testDIB_Class_AccountAggregation();
        
        //ApexPages.Standardcontroller sct = new ApexPages.Standardcontroller(myDIB_Aggregated_Allocation);
        mycontrollerAllocationAggregated =  new controllerAllocationAggregated();
        
        mycontrollerAllocationAggregated.getSelectedAccountType();
        mycontrollerAllocationAggregated.setSelectedAccountType(accountType);
        mycontrollerAllocationAggregated.getDIB_Aggregated_Allocation();
        mycontrollerAllocationAggregated.setDIB_Aggregated_Allocation(myDIB_Aggregated_Allocation);
        mycontrollerAllocationAggregated.getMyDIB_Class_AccountAggregations();
        mycontrollerAllocationAggregated.setMyDIB_Class_AccountAggregations(myDIB_Class_AccountAggregations);
        mycontrollerAllocationAggregated.getAccountTypes();
        mycontrollerAllocationAggregated.Months = months;
        mycontrollerAllocationAggregated.Years = years;
        mycontrollerAllocationAggregated.pumpcgmsChoosen = '';
        mycontrollerAllocationAggregated.getCountry();
        mycontrollerAllocationAggregated.getshowSubmitButton();
        mycontrollerAllocationAggregated.getTerritoryFilterOptions();
        mycontrollerAllocationAggregated.setCountry(currentUser.Country);
        
        mycontrollerAllocationAggregated.initiateAggregatedAllocation();
        
        mycontrollerAllocationAggregated.save();
        
        mycontrollerAllocationAggregated.getPeriodItems();
        mycontrollerAllocationAggregated.getPeriodChosen2();
        
        Test.stopTest();
    }
    
    public void testDIB_Class_AccountAggregation(){
        
        DIB_Class_AccountAggregation myDIB_Class_AccountAggregation;
        myDIB_Class_AccountAggregation = new DIB_Class_AccountAggregation();
        myDIB_Class_AccountAggregation = new DIB_Class_AccountAggregation(myTestAccount.Id,1,1,1,true,'myCity');
        myDIB_Class_AccountAggregation = new DIB_Class_AccountAggregation(myTestAccount.Id,myTestAccount.Id,1,1,1,true,'myCity');
        myDIB_Class_AccountAggregation.setCity('myCity');
        myDIB_Class_AccountAggregation.getCity();
        myDIB_Class_AccountAggregation.getAgg_Account();
        myDIB_Class_AccountAggregation.setAgg_Account(myTestInvoiceLine);
        myDIB_Class_AccountAggregation.getDone_nr();
        myDIB_Class_AccountAggregation.setDone_nr(1);
        myDIB_Class_AccountAggregation.getTodo_nr();
        myDIB_Class_AccountAggregation.setTodo_nr(1);       
        myDIB_Class_AccountAggregation.getTotal_nr();
        myDIB_Class_AccountAggregation.setTotal_nr(1);
        myDIB_Class_AccountAggregation.getCompleted();
        myDIB_Class_AccountAggregation.setCompleted(myTestInvoiceLine);
        myDIB_Class_AccountAggregation.getSales_Force_Account();
        myDIB_Class_AccountAggregation.setSales_Force_Account(myTestAccount);
        
    }
      
    public void createRecords() {
        
        String accountType = 'Adult';
            
        // Sold To Accounts 
        List<Account> soldToAccounts    = new List<Account>{} ;
            
        // Sales Force Accounts 
        List<Account> sfaAccounts       = new List<Account>{} ;
    
        //DIB_Departments
        List<DIB_Department__c> dibDepartments      = new List<DIB_Department__c>{} ;
            
        // Create a few Sold to Accounts ; 
        for (Integer i = 0; i < 10 ; i++){
            Account acc1 = new Account();
            acc1.Name = 'Aatest Coverage Sold to or ship To Account ' + i ;
            acc1.Account_Active__c = true;
            soldToAccounts.add(acc1) ;
        }
        insert soldToAccounts ;
            
            
        // create Sales Force Accounts : 
        for (Integer i = 0; i < 10 ; i++){
            Account acc1 = new Account();
            acc1.Name = 'Aatest Coverage Sales Force Account ' + i ;
            acc1.isSales_Force_Account__c = true ; 
            acc1.Account_Active__c = true;
            sfaAccounts.add(acc1) ;
        }
        insert sfaAccounts ;
            
        myTestAccount = sfaAccounts.get(0);
            
        System.debug(' ################## ' + 'controllerDIB_Campaign_createRecords_sfaAccounts: ' + sfaAccounts + ' ################');
            
        // create Departments : 
        for (Integer i = 0; i < 10 ; i++){
            DIB_Department__c myDepartment = new DIB_Department__c();
            myDepartment.Account__c = sfaAccounts.get(i).Id;
            //myDepartment.Department__c = accountType;
            myDepartment.DiB_Segment__c = departmentSegment;
            dibDepartments.add(myDepartment) ;
        }
        insert dibDepartments ;
        
        List<DIB_Invoice_Line__c> invoiceLines0109 = new List<DIB_Invoice_Line__c>{} ;      
        for (Integer i = 0; i < 10 ; i++){
            DIB_Invoice_Line__c invl  = new DIB_Invoice_Line__c() ; 
            invl.Distribution_No__c = 'DN' ; 
            invl.Name = 'Invoice Name' ;  
            invl.Fiscal_Month_Code__c = months;
            invl.Fiscal_Year__c = String.valueOf(Integer.valueOf(years) + 10);
            invl.Allocated_Fiscal_Month_Code__c=months;
            invl.Allocated_Fiscal_Year__c=String.valueOf(Integer.valueOf(years) + 10); 
            invl.Sales_Force_Account__c=sfaAccounts.get(0).Id;
            invl.Department_ID__c=dibDepartments[0].Id; 
            // This field must have a value otherwise can have a problem saying Argument 1 cannot be null Line 101 on Method SharedMethods. 
            invl.Quantity_Invoiced_Number__c = 1 ; 
            invl.Sold_To__c = sfaAccounts.get(i).Id;
            invl.Ship_To_Name_Text__c = sfaAccounts.get(i).Name;
            invl.Is_Allocated__c = false ;  
            if (i > 5 && i < 20){
                invl.Is_Allocated__c = true ; 
            }
            invoiceLines0109.add(invl) ; 
        }
        insert invoiceLines0109 ;
            
        myTestInvoiceLine = invoiceLines0109.get(0);
            
        List<DIB_Aggregated_Allocation__c> myAggregated_Allocations = new List<DIB_Aggregated_Allocation__c>();
        for (Integer i = 0; i < 10 ; i++){
            DIB_Aggregated_Allocation__c myAggregated_Allocation = new DIB_Aggregated_Allocation__c();
            myAggregated_Allocation.DIB_Department_ID__c = dibDepartments.get(i).Id;
            myAggregated_Allocation.Fiscal_Month__c = months;
            myAggregated_Allocation.Fiscal_Year__c = years;
            myAggregated_Allocations.add(myAggregated_Allocation);
        }
        //insert myAggregated_Allocations;

    }
    
}