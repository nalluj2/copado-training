/*	Integration Service related class
	
	This interface defines the methods that each implementation for a concrete SObject needs to be implemented. We have a single interface
	for methods used in the Source and Target Orgs, so depending on where they are located some methods my have a dummy implementation. 
*/
public interface SynchronizationService_Object {
	
	Set<String> hasChangeForNotification(List<SObject> records, Map<Id, SObject> oldRecords);
	
	String generatePayload(String externalId);
	
	String processPayload(String payload);
}