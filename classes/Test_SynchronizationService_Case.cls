@isTest
private class Test_SynchronizationService_Case {
	
	private static testMethod void syncCase(){
		
		Synchronization_Service_Setting__c syncSettings = new Synchronization_Service_Setting__c();
		syncSettings.username__c = 'test@medtronic.com';
		syncSettings.password__c = 'password';
		syncSettings.client_id__c = '123456789';
		syncSettings.client_secret__c = 'XXXXX';
		syncSettings.is_sandbox__c = true;
		syncSettings.sync_Enabled__c = true;
		insert syncSettings;
		
		//Products
		Product2 testProduct = new Product2();
		testProduct.Name = 'PoleStar';
		testProduct.ProductCode = '000999';
		testProduct.isActive = true;
					
		insert testProduct;
		
		//Account
		Account acc = new Account();	
		acc.AccountNumber = '0000000';
		acc.Name = 'Syn Test Account';
		acc.Phone = '+123456789';
		acc.BillingCity = 'Minneapolis';
		acc.BillingCountry = 'UNITED STATES';
		acc.BillingState = 'Minnesota';
		acc.BillingStreet = 'street';
		acc.BillingPostalCode = '123456';
		acc.ST_NAV_Non_SAP_Account__c = true;
		acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non-SAP Account').getRecordTypeId();	
		insert acc;
		
		//Contact
		Contact cnt = new Contact();
		cnt.FirstName = 'Test';
		cnt.LastName = 'Contact';
		cnt.Phone = '+123456789';		
		cnt.Email = 'test.contact@gmail.com';
		cnt.MobilePhone = '+123456789';		
		cnt.MailingCity = 'Minneapolis';
		cnt.MailingCountry = 'UNITED STATES';
		cnt.MailingState = 'Minnesota';
		cnt.MailingStreet = 'street';
		cnt.MailingPostalCode = '123456';
		cnt.External_Id__c = 'Test_Contact_Id';
		cnt.Contact_Department__c = 'Cardiology';
		cnt.Contact_Primary_Specialty__c = 'Cardiovascular'; 
		cnt.Affiliation_To_Account__c = 'Employee'; 
		cnt.Primary_Job_Title_vs__c = 'Surgeon'; 
		cnt.Contact_Gender__c = 'Male'; 
		cnt.AccountId = acc.Id;
		cnt.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Generic Contact').getRecordTypeId();		
		
		//Contact
		Contact surgeon = new Contact();
		surgeon.FirstName = 'Test';
		surgeon.LastName = 'Surgeon';
		surgeon.Phone = '+987654321';		
		surgeon.Email = 'test.surgeon@gmail.com';
		surgeon.MobilePhone = '+987654321';		
		surgeon.MailingCity = 'Minneapolis';
		surgeon.MailingCountry = 'UNITED STATES';
		surgeon.MailingState = 'Minnesota';
		surgeon.MailingStreet = 'street';
		surgeon.MailingPostalCode = '123456';
		surgeon.External_Id__c = 'Test_Surgeon_Id';
		surgeon.Contact_Department__c = 'Cardiology';
		surgeon.Contact_Primary_Specialty__c = 'Cardiovascular'; 
		surgeon.Affiliation_To_Account__c = 'Employee'; 
		surgeon.Primary_Job_Title_vs__c = 'Surgeon'; 
		surgeon.Contact_Gender__c = 'Male'; 
		surgeon.AccountId = acc.Id;
		surgeon.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Generic Contact').getRecordTypeId();		
				
		insert new List<Contact>{cnt, surgeon};
		
		//Asset
		Asset asset = new Asset();
		asset.AccountId = acc.Id;
		asset.Product2Id = testProduct.Id;
		asset.Asset_Product_Type__c = 'PoleStar N-10';
		asset.Ownership_Status__c = 'Purchased';
		asset.Name = '123456789';
		asset.Serial_Nr__c = '123456789';
		asset.Status = 'Installed';
		asset.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('PoleStar').getRecordTypeId();
		insert asset;
				
		//Complaint
		Complaint__c comp = new Complaint__c();
		comp.Account_Name__c = acc.Id;
		comp.Asset__c = asset.Id;
		comp.Status__c = 'Open';
		comp.External_Id__c = 'Test_Complaint_Id';
		insert comp;
		
		//Case
		Case newCase = new Case();
		newCase.AccountId = acc.Id;
		newCase.ContactId = cnt.Id;
		newCase.Initial_Reporter__c = cnt.Id;
		newCase.AssetId = asset.Id;
		newCase.Surgeon__c = surgeon.Id;
		newCase.Escalation_Owner__c = UserInfo.getUserId();
		newCase.Complaint__c = comp.Id;
		newCase.Complaint_Case__c = 'Yes';
		newCase.Date_Complaint_Became_Known__c = Date.today();	
		newCase.RecordTypeId = clsUtil.getRecordTypeByDevName('Case', 'New_Case').Id;
		newCase.External_Id__c = 'Test_Case_Id';					
		insert newCase;
				
		Test.startTest();
		
		Test_bl_SynchronizationService_Utils.CalloutMock mockImpl = new Test_bl_SynchronizationService_Utils.CalloutMock();
		
		Test.setMock(HttpCalloutMock.class, mockImpl);
		bl_SynchronizationService_Utils.calloutMock = mockImpl;

		List<Sync_Notification__c> notifications = [Select Id from Sync_Notification__c where Status__c = 'New' AND Type__c = 'Outbound' AND Record_Object_Type__c = 'Case'];
		
		System.assert(notifications.size() == 1);
		
		SynchronizationService_Case caseService = new SynchronizationService_Case();
		
		String payload = caseService.generatePayload('Test_Case_Id');
		
		System.assert(payload != null);
	}
		
	private static testMethod void syncCaseInbound(){
		
		Synchronization_Service_Setting__c syncSettings = new Synchronization_Service_Setting__c();
		syncSettings.username__c = 'test@medtronic.com';
		syncSettings.password__c = 'password';
		syncSettings.client_id__c = '123456789';
		syncSettings.client_secret__c = 'XXXXX';
		syncSettings.is_sandbox__c = true;
		syncSettings.sync_Enabled__c = true;
		insert syncSettings;
		
		//Products
		Product2 testProduct = new Product2();
		testProduct.Name = 'PoleStar';
		testProduct.ProductCode = '000999';
		testProduct.isActive = true;
					
		insert testProduct;
		
		//Account
		Account acc = new Account();	
		acc.AccountNumber = '0000000';
		acc.Name = 'Syn Test Account';
		acc.Phone = '+123456789';
		acc.BillingCity = 'Minneapolis';
		acc.BillingCountry = 'UNITED STATES';
		acc.BillingState = 'Minnesota';
		acc.BillingStreet = 'street';
		acc.BillingPostalCode = '123456';
		acc.ST_NAV_Non_SAP_Account__c = true;
		acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non-SAP Account').getRecordTypeId();	
		insert acc;
		
		//Contact
		Contact cnt = new Contact();
		cnt.FirstName = 'Test';
		cnt.LastName = 'Contact';
		cnt.Phone = '+123456789';		
		cnt.Email = 'test.contact@gmail.com';
		cnt.MobilePhone = '+123456789';		
		cnt.MailingCity = 'Minneapolis';
		cnt.MailingCountry = 'UNITED STATES';
		cnt.MailingState = 'Minnesota';
		cnt.MailingStreet = 'street';
		cnt.MailingPostalCode = '123456';
		cnt.External_Id__c = 'Test_Contact_Id';
		cnt.Contact_Department__c = 'Cardiology';
		cnt.Contact_Primary_Specialty__c = 'Cardiovascular'; 
		cnt.Affiliation_To_Account__c = 'Employee'; 
		cnt.Primary_Job_Title_vs__c = 'Surgeon'; 
		cnt.Contact_Gender__c = 'Male'; 
		cnt.AccountId = acc.Id;
		cnt.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Generic Contact').getRecordTypeId();		
		
		//Contact
		Contact surgeon = new Contact();
		surgeon.FirstName = 'Test';
		surgeon.LastName = 'Surgeon';
		surgeon.Phone = '+987654321';		
		surgeon.Email = 'test.surgeon@gmail.com';
		surgeon.MobilePhone = '+987654321';		
		surgeon.MailingCity = 'Minneapolis';
		surgeon.MailingCountry = 'UNITED STATES';
		surgeon.MailingState = 'Minnesota';
		surgeon.MailingStreet = 'street';
		surgeon.MailingPostalCode = '123456';
		surgeon.External_Id__c = 'Test_Surgeon_Id';
		surgeon.Contact_Department__c = 'Cardiology';
		surgeon.Contact_Primary_Specialty__c = 'Cardiovascular'; 
		surgeon.Affiliation_To_Account__c = 'Employee'; 
		surgeon.Primary_Job_Title_vs__c = 'Surgeon'; 
		surgeon.Contact_Gender__c = 'Male'; 
		surgeon.AccountId = acc.Id;
		surgeon.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Generic Contact').getRecordTypeId();		
				
		insert new List<Contact>{cnt, surgeon};
		
		//Asset
		Asset asset = new Asset();
		asset.AccountId = acc.Id;
		asset.Product2Id = testProduct.Id;
		asset.Asset_Product_Type__c = 'PoleStar N-10';
		asset.Ownership_Status__c = 'Purchased';
		asset.Name = '123456789';
		asset.Serial_Nr__c = '123456789';
		asset.Status = 'Installed';
		asset.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('PoleStar').getRecordTypeId();
		insert asset;
				
		//Complaint
		Complaint__c comp = new Complaint__c();
		comp.Account_Name__c = acc.Id;
		comp.Asset__c = asset.Id;
		comp.Status__c = 'Open';
		comp.External_Id__c = 'Test_Complaint_Id';
		insert comp;
		
		//Case
		Case newCase = new Case();
		newCase.AccountId = acc.Id;
		newCase.ContactId = cnt.Id;
		newCase.Initial_Reporter__c = cnt.Id;
		newCase.AssetId = asset.Id;
		newCase.Surgeon__c = surgeon.Id;
		newCase.Escalation_Owner__c = UserInfo.getUserId();	
		newCase.RecordTypeId = clsUtil.getRecordTypeByDevName('Case', 'New_Case').Id;
		newCase.External_Id__c = 'Test_Case_Id';					
		insert newCase;
		
		Test.startTest();
		
		Test_bl_SynchronizationService_Utils.CalloutMock mockImpl = new Test_bl_SynchronizationService_Utils.CalloutMock();
		
		Test.setMock(HttpCalloutMock.class, mockImpl);
		bl_SynchronizationService_Utils.calloutMock = mockImpl;
		
		SynchronizationService_Case.CaseSyncPayload request = new SynchronizationService_Case.CaseSyncPayload();
				
		//Case
		Case caseRecord = new Case();
		caseRecord.External_Id__c = 'Test_Case_Id';
		caseRecord.Complaint_Case__c = 'Yes';
		caseRecord.Date_Complaint_Became_Known__c = Date.today();
		request.caseRecord = caseRecord;
				
		//Other		
		request.complaintId = 'Test_Complaint_Id';		
		request.lastModifiedDate = DateTime.now();
		request.lastModifiedBy = 'Test User';
		
		String payload = JSON.serialize(request);
		
		bl_SynchronizationService_Target.processRecordPayload(payload, 'Case');
		
		List<Case> cases = [Select Id, Complaint__c from Case where External_Id__c = 'Test_Case_Id'];
						
		System.assert(cases.size() == 1);	
		System.assert(cases[0].Complaint__c == comp.Id);					
	}
	
	private static testMethod void syncCaseInbound_err(){
		
		Synchronization_Service_Setting__c syncSettings = new Synchronization_Service_Setting__c();
		syncSettings.username__c = 'test@medtronic.com';
		syncSettings.password__c = 'password';
		syncSettings.client_id__c = '123456789';
		syncSettings.client_secret__c = 'XXXXX';
		syncSettings.is_sandbox__c = true;
		syncSettings.sync_Enabled__c = false;
		insert syncSettings;
		
		//Products
		Product2 testProduct = new Product2();
		testProduct.Name = 'PoleStar';
		testProduct.ProductCode = '000999';
		testProduct.isActive = true;
					
		insert testProduct;
		
		//Account
		Account acc = new Account();	
		acc.AccountNumber = '0000000';
		acc.Name = 'Syn Test Account';
		acc.Phone = '+123456789';
		acc.BillingCity = 'Minneapolis';
		acc.BillingCountry = 'UNITED STATES';
		acc.BillingState = 'Minnesota';
		acc.BillingStreet = 'street';
		acc.BillingPostalCode = '123456';
		acc.ST_NAV_Non_SAP_Account__c = true;
		acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non-SAP Account').getRecordTypeId();	
		insert acc;
		
		//Contact
		Contact cnt = new Contact();
		cnt.FirstName = 'Test';
		cnt.LastName = 'Contact';
		cnt.Phone = '+123456789';		
		cnt.Email = 'test.contact@gmail.com';
		cnt.MobilePhone = '+123456789';		
		cnt.MailingCity = 'Minneapolis';
		cnt.MailingCountry = 'UNITED STATES';
		cnt.MailingState = 'Minnesota';
		cnt.MailingStreet = 'street';
		cnt.MailingPostalCode = '123456';
		cnt.External_Id__c = 'Test_Contact_Id';
		cnt.Contact_Department__c = 'Cardiology';
		cnt.Contact_Primary_Specialty__c = 'Cardiovascular'; 
		cnt.Affiliation_To_Account__c = 'Employee'; 
		cnt.Primary_Job_Title_vs__c = 'Surgeon'; 
		cnt.Contact_Gender__c = 'Male'; 
		cnt.AccountId = acc.Id;
		cnt.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Generic Contact').getRecordTypeId();		
		
		//Contact
		Contact surgeon = new Contact();
		surgeon.FirstName = 'Test';
		surgeon.LastName = 'Surgeon';
		surgeon.Phone = '+987654321';		
		surgeon.Email = 'test.surgeon@gmail.com';
		surgeon.MobilePhone = '+987654321';		
		surgeon.MailingCity = 'Minneapolis';
		surgeon.MailingCountry = 'UNITED STATES';
		surgeon.MailingState = 'Minnesota';
		surgeon.MailingStreet = 'street';
		surgeon.MailingPostalCode = '123456';
		surgeon.External_Id__c = 'Test_Surgeon_Id';
		surgeon.Contact_Department__c = 'Cardiology';
		surgeon.Contact_Primary_Specialty__c = 'Cardiovascular'; 
		surgeon.Affiliation_To_Account__c = 'Employee'; 
		surgeon.Primary_Job_Title_vs__c = 'Surgeon'; 
		surgeon.Contact_Gender__c = 'Male'; 
		surgeon.AccountId = acc.Id;
		surgeon.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Generic Contact').getRecordTypeId();		
				
		insert new List<Contact>{cnt, surgeon};
		
		//Asset
		Asset asset = new Asset();
		asset.AccountId = acc.Id;
		asset.Product2Id = testProduct.Id;
		asset.Asset_Product_Type__c = 'PoleStar N-10';
		asset.Ownership_Status__c = 'Purchased';
		asset.Name = '123456789';
		asset.Serial_Nr__c = '123456789';
		asset.Status = 'Installed';
		asset.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('PoleStar').getRecordTypeId();
		insert asset;
					
		//Case
		Case newCase = new Case();
		newCase.AccountId = acc.Id;
		newCase.ContactId = cnt.Id;
		newCase.AssetId = asset.Id;
		newCase.Surgeon__c = surgeon.Id;
		newCase.Escalation_Owner__c = UserInfo.getUserId();	
		newCase.RecordTypeId = clsUtil.getRecordTypeByDevName('Case', 'New_Case').Id;
		newCase.External_Id__c = 'Test_Case_Id';					
		insert newCase;
		
		Test.startTest();
		
		Test_bl_SynchronizationService_Utils.CalloutMock mockImpl = new Test_bl_SynchronizationService_Utils.CalloutMock();
		
		Test.setMock(HttpCalloutMock.class, mockImpl);
		bl_SynchronizationService_Utils.calloutMock = mockImpl;
		
		SynchronizationService_Case.CaseSyncPayload request = new SynchronizationService_Case.CaseSyncPayload();
				
		//Case
		Case caseRecord = new Case();
		caseRecord.External_Id__c = 'Test_Case_Id';
		caseRecord.Complaint_Case__c = 'Yes';
		caseRecord.Date_Complaint_Became_Known__c = Date.today();
		request.caseRecord = caseRecord;
				
		//Other		
		request.complaintId = 'Test_Complaint_Id';		
		request.lastModifiedDate = DateTime.now();
		request.lastModifiedBy = 'Test User';
		
		String payload = JSON.serialize(request);
		
		Boolean error = false;
		
		try{
		
			bl_SynchronizationService_Target.processRecordPayload(payload, 'Case');
		
		}catch(Exception e){
			
			error = true;
		}
						
		System.assert(error == true);							
	} 	
}