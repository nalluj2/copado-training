public with sharing class TwoRing_Repository {

	public static String createEntity(TwoRing_CreateOrUpdateEntityObject entityObject) {
		try {
			SObjectType objectDef = Schema.getGlobalDescribe().get(entityObject.name);
			if(objectDef != null) {
				DescribeSObjectResult objectDescribe = objectDef.getDescribe();
				if(objectDescribe.isCreateable()) {
					Map<String, Schema.SobjectField> objectFieldsMap = objectDescribe.fields.getMap();
					SObject entity = setCreateableProperties(objectDef.newSObject(), objectFieldsMap, entityObject.data);
					Database.SaveResult result = Database.insert(entity, true);
					if (result.isSuccess()) {
						return result.getId();
					}			
					else {
						throw new TwoRing_ApplicationException('Unable to create entity ' + entityObject.name + '. Error:' + result.getErrors()[0].getMessage());
					}
				}
				else {
					throw new TwoRing_ApplicationException('Unable to create entity ' + entityObject.name + '. Error: permissions denied.');
				}
			}
			else {
				throw new TwoRing_ApplicationException('Unable to create entity ' + entityObject.name + '. Error: entity type does not exist.');
			}
		}
		catch(TwoRing_ApplicationException ex) {
			throw ex;
		}
		catch(Exception ex) {
			throw new TwoRing_ApplicationException('Unable to create entity '+ entityObject.name + '. Error:' + ex.getMessage());
		}
	}

	public static void deleteEntity(String entityId) {
		try {
			Id idObject = Id.valueOf(entityId);
			SObjectType objectDef = idObject.getSObjectType();
			if(objectDef != null) {
				DescribeSObjectResult objectDescribe = objectDef.getDescribe();
				if(objectDescribe.isDeletable()) {
					SObject entity = objectDef.newSObject(idObject);
					Database.DeleteResult result = Database.delete(entity, true);
					if (!result.isSuccess()) {
						throw new TwoRing_ApplicationException('Unable to delete entity with Id: '+ entityId + '. Error:' + result.getErrors()[0].getMessage());
					}
				}
				else {
					throw new TwoRing_ApplicationException('Unable to delete entity with Id: ' + entityId + '. Error: permissions denied.');
				}				
			}
			else {
				throw new TwoRing_ApplicationException('Unable to delete entity with Id: ' + entityId + '. Error: entity with specified Id does not exist.');
			}			
		}
		catch(TwoRing_ApplicationException ex) {
			throw ex;
		}
		catch(Exception ex) {
			throw new TwoRing_ApplicationException('Unable to delete entity '+ entityId + '. Error:' + ex.getMessage());
		}
	}
	
	public static SObject readEntity(String entityId) {
		try {
			Id idObject = Id.valueOf(entityId);
			SObjectType objectDef = idObject.getSObjectType();
			DescribeSObjectResult objectResult = objectDef.getDescribe();
			if (objectResult.isAccessible()) {
				List<String> fieldList = new List<String>(objectResult.fields.getMap().keySet());
				List<String> accessibleFields = new List<string>();
				for (Integer i = 0; i < fieldList.size(); i++) {
					if(isFieldAccessible(objectResult, fieldList.get(i))) {
						accessibleFields.add(fieldList[i]);
					}
				}
				String fields = String.join(accessibleFields,',');

				return Database.query('SELECT '+ fields +' FROM '+ objectResult.getName() + ' WHERE Id= \''+ entityId + '\'').get(0);
				
			} else {
				throw new TwoRing_ApplicationException('Unable to read entity '+ entityId + '.');
			}
		}
		catch(TwoRing_ApplicationException ex) {
			throw ex;
		}
		catch(Exception ex) {
			throw new TwoRing_ApplicationException('Unable to read entity '+ entityId + '. Error:' + ex.getMessage());
		}
	}

	public static String updateEntity(TwoRing_CreateOrUpdateEntityObject entityObject) {
		try {
			SObjectType objectDef = Schema.getGlobalDescribe().get(entityObject.name);
			if(objectDef != null) {
				DescribeSObjectResult objectDescribe = objectDef.getDescribe();
				if(objectDescribe.isUpdateable()) {
					Map<String, Schema.SobjectField> objectFieldsMap = objectDef.getDescribe().fields.getMap();
					SObject entity = setUpdateableProperties(objectDef.newSObject(entityObject.id), objectFieldsMap, entityObject.data);
					Database.SaveResult result = Database.update(entity, true);
					if (result.isSuccess()) {
						return result.getId();
					}
					else {
						throw new TwoRing_ApplicationException('Unable to update entity '+ entityObject.name + '. Error:' + result.getErrors()[0].getMessage());
					}
				}
				else {
					throw new TwoRing_ApplicationException('Unable to update entity ' + entityObject.name + '. Error: permissions denied.');
				}				
			}
			else {
				throw new TwoRing_ApplicationException('Unable to update entity ' + entityObject.name + '. Error: entity type does not exist.');
			}			
		}
		catch(TwoRing_ApplicationException ex) {
			throw ex;
		}
		catch(Exception ex) {
			throw new TwoRing_ApplicationException('Unable to update entity '+ entityObject.name + '. Error:' + ex.getMessage());
		}
	}

	public static List<SObject> performLookup(TwoRing_QueryObject query) {
		String searchTmpl = 'FIND {0} IN {1} FIELDS RETURNING {2}';
		Set<String> entities = new Set<String>(query.entityTypes);
		Set<String> deniedEntities = new Set<string>();

		// Get entity types describers
		Map<String, DescribeSObjectResult> entityTypesDescribers = new Map<String, DescribeSObjectResult>();
		for(String entityType:entities) {
			String searchForEntity = '';
			if (entityType == TwoRing_EntityType.PERSONACCOUNT) {
				if (!Schema.SObjectType.Account.fields.getMap().containsKey('isPersonAccount')) {
					throw new TwoRing_ApplicationException('Unable to perform lookup. Person accounts are not enabled.');
				}
				searchForEntity = TwoRing_EntityType.ACCOUNT;
			} else {
				searchForEntity = entityType;
			}
			SObjectType objectDef = Schema.getGlobalDescribe().get(searchForEntity);
			if(objectDef != null) {
				DescribeSObjectResult objectDescribe = objectDef.getDescribe();
				if(objectDescribe.isAccessible()) {
					entityTypesDescribers.put(entityType, objectDescribe);
				} else {
					deniedEntities.add(entityType);
				}
			} else {
				throw new TwoRing_ApplicationException('Unable to perform lookup. Error: entity type ' + searchForEntity + ' does not exist.');
			}
		}

		if(deniedEntities.size() > 0) {
			entities.removeAll(deniedEntities);
		}


		// Get accessible fields
		Map<String, List<String>> accessibleEntityTypeFields = new Map<string, List<string>>();
		if (query.fields != null) {
			for(String entityType:query.fields.keySet()) {
				String[] fields = query.fields.get(entityType);
				List<String> accessibleFields = new List<string>();
				DescribeSObjectResult describer = entityTypesDescribers.get(entityType);
				if (describer != null) {
					if (fields != null) {
						for (Integer i = 0; i < fields.size(); i++) {
							if(isFieldAccessible(describer, fields.get(i))) {
								accessibleFields.add(fields[i]);
							}
						}
					}
					accessibleEntityTypeFields.put(entityType, accessibleFields);
				}
			}

			//check if person account Is enable And if so add isPersonAcocunt field to account
			if (Schema.SObjectType.Account.fields.getMap().containsKey('isPersonAccount')
				&& isFieldAccessible(Schema.SObjectType.Account, 'isPersonAccount')) {

				if(accessibleEntityTypeFields.containsKey(TwoRing_EntityType.PERSONACCOUNT)) {
					accessibleEntityTypeFields.get(TwoRing_EntityType.PERSONACCOUNT).add('isPersonAccount');
				}
				if(accessibleEntityTypeFields.containsKey(TwoRing_EntityType.ACCOUNT)) {
					accessibleEntityTypeFields.get(TwoRing_EntityType.ACCOUNT).add('isPersonAccount');
				}
				if(accessibleEntityTypeFields.containsKey(TwoRing_EntityType.CONTACT)) {
					accessibleEntityTypeFields.get(TwoRing_EntityType.CONTACT).add('Account.isPersonAccount');
				}
			}
		}
		
		String searchQuery, searchCategory, searchString;
		String searchEntityType = getReturningExpression(entities, accessibleEntityTypeFields, query.ids);
		List<String> ids = query.ids;
		List<List<List<sObject>>> queryResults = new List<List<List<sObject>>>();
			
		//Phone Criteria
		if(query.phones != null) {
			searchQuery = getSoslSearchQuery(query.phones);
			searchCategory = 'PHONE';
			searchString = String.format(searchTmpl, new String[] { searchQuery, searchCategory, searchEntityType });
			queryResults.add(Search.query(searchString));
		}
		//Name Criteria
		if(query.names != null) {
			searchQuery = getSoslSearchQuery(query.names);
			searchCategory = 'NAME';
			searchString = String.format(searchTmpl, new String[] { searchQuery, searchCategory, searchEntityType });
			queryResults.add(Search.query(searchString));
		}
		//Email Criteria
		if(query.emails != null) {
			searchQuery = getSoslSearchQuery(query.emails);
			searchCategory = 'EMAIL';
			searchString = String.format(searchTmpl, new String[] { searchQuery, searchCategory, searchEntityType });
			queryResults.add(Search.query(searchString));
		}

		Boolean hasCustomCriteria = false;
		if (query.customCriterias != null) {
			for(String entityType:query.customCriterias.keySet()) {
				if (!query.customCriterias.get(entityType).isEmpty()) {
					hasCustomCriteria = true;
					break;
				}
			}
		}

		if(hasCustomCriteria) {
			List<List<SObject>> customCriteriaResults = new List<List<SObject>>();
			for(String entityType:entities) {
				if(query.customCriterias.containsKey(entityType) && !query.customCriterias.get(entityType).isEmpty()) {
					List<TwoRing_CustomCriteria> entityTypeCustomCriterias = query.customCriterias.get(entityType);
					searchQuery = getSoqlSearchQuery(entityTypeCustomCriterias);
					List<String> fields = new List<string> {'Id'};
					if(query.fields != null && query.fields.containsKey(entityType) && query.fields.get(entityType) != null) {
						fields = accessibleEntityTypeFields.get(entityType);
					}
					String searchForEntity = '';
					if (entityType == TwoRing_EntityType.PERSONACCOUNT) {
						searchForEntity = TwoRing_EntityType.ACCOUNT;
					} else {
						searchForEntity = entityType;
					}
					searchString = String.format('SELECT {0} FROM {1} WHERE {2}', new String[] { String.join(fields, ', '), searchForEntity, searchQuery });
					if(ids != null) {
						searchString += 'AND Id in :ids';
					}
					if(entityType == TwoRing_EntityType.CONTACT && Schema.SObjectType.Contact.fields.getMap().containsKey('isPersonAccount')) {
						searchString += ' AND IsPersonAccount = false';
					}
					if(entityType == TwoRing_EntityType.ACCOUNT && Schema.SObjectType.Account.fields.getMap().containsKey('isPersonAccount')) {
						searchString += ' AND IsPersonAccount = false';
					}
					if(entityType == TwoRing_EntityType.PERSONACCOUNT && Schema.SObjectType.Account.fields.getMap().containsKey('isPersonAccount')) {
						searchString += ' AND IsPersonAccount = true';
					}
					customCriteriaResults.add(Database.query(searchString));
				}
				else if (entities.contains(entityType)) {
					customCriteriaResults.add(new List<SObject>());
				}
			}
			queryResults.add(customCriteriaResults);
		}

		List<SObject> results = new List<SObject>();

		//Id Criteria
		if(queryResults.size() == 0) {
			for(String entityType:entities) {
				DescribeSObjectResult describer = entityTypesDescribers.get(entityType);
				if (describer != null && describer.isAccessible()) {
					String fields = 'Id';
					if(query.fields != null && query.fields.containsKey(entityType) && query.fields.get(entityType) != null) {
						fields = String.join(accessibleEntityTypeFields.get(entityType), ', ');
					}
					String searchForEntity;
					if (entityType == TwoRing_EntityType.PERSONACCOUNT) {
						searchForEntity = TwoRing_EntityType.ACCOUNT;
					} else {
						searchForEntity = entityType;
					}
					String soqlQuery = String.format('SELECT {0} FROM {1} WHERE Id IN :ids', new String[] { fields, searchForEntity });
					if(entityType == TwoRing_EntityType.CONTACT && Schema.SObjectType.Contact.fields.getMap().containsKey('isPersonAccount')) {
						soqlQuery += ' AND IsPersonAccount = false';
					}
					if(entityType == TwoRing_EntityType.ACCOUNT && Schema.SObjectType.Contact.fields.getMap().containsKey('isPersonAccount')) {
						soqlQuery += ' AND IsPersonAccount = false';
					}
					if(entityType == TwoRing_EntityType.PERSONACCOUNT && Schema.SObjectType.Contact.fields.getMap().containsKey('isPersonAccount')) {
						soqlQuery += ' AND IsPersonAccount = true';
					}
					results.addall(Database.query(soqlQuery));
				}
			}
		}
		else {
			results.addall(getEntitiesFromResults(entities, queryResults));
		}
		
		return results;
	}

	private static SObject setCreateableProperties(SObject entity, Map<String, Schema.SobjectField> objectFieldsMap, Map<String,String> properties) {
		for (String fieldName : properties.keySet()) {
			if (objectFieldsMap.containsKey(fieldName)) {
				DescribeFieldResult fieldDescribe = objectFieldsMap.get(fieldName).getDescribe();
				if(fieldDescribe.isCreateable()) {
					setSObjectProperty(entity, fieldName, fieldDescribe, properties);
				}
				else {
					throw new TwoRing_ApplicationException('Entity ' + entity.getSObjectType().getDescribe().getName() + ' - create permission denied for field ' + fieldName + '.' );
				}
			}
			else {
				throw new TwoRing_ApplicationException('Entity ' + entity.getSObjectType().getDescribe().getName() + ' - does not have field ' + fieldName + '.' );
			}
		}
		return entity;
	}

	private static SObject setUpdateableProperties(SObject entity, Map<String, Schema.SobjectField> objectFieldsMap, Map<String,String> properties) {
		for (String fieldName : properties.keySet()) {
			if (objectFieldsMap.containsKey(fieldName)) {
				DescribeFieldResult fieldDescribe = objectFieldsMap.get(fieldName).getDescribe();
				if(fieldDescribe.isUpdateable()) {
					setSObjectProperty(entity, fieldName, fieldDescribe, properties);
				}
				else {
					throw new TwoRing_ApplicationException('Entity ' + entity.getSObjectType().getDescribe().getName() + ' - update permission denied for field ' + fieldName + '.' );
				}
			}
			else {
				throw new TwoRing_ApplicationException('Entity ' + entity.getSObjectType().getDescribe().getName() + ' - does not have field ' + fieldName + '.' );
			}
		}
		return entity;
	}

	private static void setSObjectProperty(SObject entity, String fieldName, DescribeFieldResult fieldDescribe, Map<String,String> properties) {
		String fieldType = fieldDescribe.getSOAPType().name().ToLowerCase();
		String fieldValue = properties.get(fieldName);

		if (fieldType == 'integer') {
			entity.put(fieldName, Integer.valueOf(fieldValue.trim())); 
		}
		else if (fieldType == 'double') {
			entity.put(fieldName, Double.valueOf(fieldValue.trim())); 
		}
		else if(fieldType == 'boolean')
		{
			entity.put(fieldName, Boolean.valueOf(fieldValue)); 
		}
		else if(fieldType == 'date')
		{
			entity.put(fieldName, Date.valueOf(fieldValue)); 
		}
		else if(fieldType == 'datetime')
		{
			entity.put(fieldName, DateTime.valueOf(fieldValue)); 
		}
		else
		{
			entity.put(fieldName, fieldValue);
		}	
	}
	
	private static String getSoslSearchQuery(List<List<String>> criteria) {
		String[] specialCharacters = new String[] { '\\', '?', '&', '|', '!', '{', '}', '[', ']', '(', ')', '^', '~', ':', '"', '\'', '+', '-' };
		String searchQuery = '';
		
		for (Integer i = 0; i < criteria.size(); i++) {
			String innerSearchQuery = '';
			for (Integer j = 0; j < criteria.get(i).size(); j++) { 
				String escapedCriteria = criteria.get(i).get(j);
				for(Integer p = 0; p < specialCharacters.size(); p++) {
					escapedCriteria = escapedCriteria.replace(specialCharacters.get(p), '\\' + specialCharacters.get(p));
				}
				innerSearchQuery = innerSearchQuery + escapedCriteria;
				if(j < criteria.get(i).size() - 1 && criteria.get(i).size() > 1) {
					innerSearchQuery = innerSearchQuery + ' AND ';
				}
			}
			
			if(innerSearchQuery != '') {
				searchQuery = searchQuery + '(' + innerSearchQuery + ')';
			}
			
			if(i < criteria.size() - 1 && criteria.size() > 1) {
				searchQuery = searchQuery + ' OR ';
			}
		}
		
		return '{' + searchQuery + '}';
	}

	private static String getSoqlSearchQuery(List<TwoRing_CustomCriteria> criterias) {
		String criteriaFilterOperator = criterias.size() > 1 ? 'AND' : 'OR';
		TwoRing_Filter criteriaFilter = new TwoRing_Filter(criteriaFilterOperator);
		for(Integer i=0; i< criterias.size(); i++) {
			List<List<String>> conditions = criterias[i].values;
			TwoRing_Filter conditionFilter = new TwoRing_Filter('OR');
			for(Integer j=0; j < conditions.size(); j++) {
				TwoRing_Filter subConditionFilter = new TwoRing_Filter('OR');
				for(Integer k=0; k < criterias[i].fields.size(); k++) {
					List<String> subConditions = conditions[j];
					String subConditionFilterOperator = subConditions.size() > 1 ? 'AND' : 'OR';
					TwoRing_Filter conditionFieldsFilter = new TwoRing_Filter(subConditionFilterOperator);
					for(Integer l=0; l < subConditions.size(); l++) {
						String conditionOperator = null;
						if(criterias[i].isTextField) {
							conditionOperator = 'LIKE';
						}
						else {
							conditionOperator = '=';
						}
						TwoRing_Condition conditionExpression = new TwoRing_Condition(criterias[i].fields[k].Trim(), conditionOperator, subConditions[l]);
						conditionFieldsFilter.AddCondition(conditionExpression);
					}
					subConditionFilter.AddFilter(conditionFieldsFilter);
				}
				conditionFilter.AddFilter(subConditionFilter);
			}
			criteriaFilter.AddFilter(conditionFilter);
		}
		return toSoqlWhereClause(criteriaFilter);
	}

	private static String toSoqlWhereClause (TwoRing_Filter filter) {
		string query = '';
		for(Integer i=0; i < filter.conditions.size(); i++) {
			TwoRing_Condition condition = filter.conditions[i];
			String value = '';
			if(condition.operator == 'LIKE') {
				value = '\'' + condition.value + '\'';
			}
			else {
				value = condition.value;
			}
			if (query == '') {
				
				query += ' ' + condition.columnName + ' ' + condition.operator + ' ' + value;
			}
			else {
				query += ' ' + filter.filterOperator + ' ' + condition.columnName + ' ' + condition.operator + ' ' + value;
			}
		}
		if (filter.filters.size() == 0)
		{
			return query;
		}
		else {
			String subQuery = '';
			for(Integer j=0; j < filter.filters.size(); j++) {
				if (subQuery == '')
				{
					subQuery += '( ' + toSoqlWhereClause(filter.filters[j]) + ' )';
				}
				else
				{
					subQuery += ' ' + filter.filterOperator + ' ( ' + toSoqlWhereClause(filter.filters[j]) + ' ) ';
				}
			}
			if (query == '')
			{
				return subQuery;
			}
			else
			{
				return ' ( ' + query + ' ) ' + filter.filterOperator + ' ( ' + subQuery + ' ) ';
			}
		}
	}
	
	private static String getReturningExpression(Set<String> entities, Map<String, List<String>> fields, List<string> ids) {
		List<String> results = new List<string>();
		Boolean findOnlyPersonAccount = false;
		Boolean findOnlyAccount = false;
		Set<String> tempEntities = entities;
		Map<String, List<String>> tempFields = fields;

		if(tempEntities.contains(TwoRing_EntityType.PERSONACCOUNT)) {
			tempEntities.remove(TwoRing_EntityType.PERSONACCOUNT);
			List<String> personAccountFields = tempFields.get(TwoRing_EntityType.PERSONACCOUNT);
			if(personAccountFields != null) {
				if(tempEntities.contains(TwoRing_EntityType.ACCOUNT)) {
					List<String> accountFields = tempFields.get(TwoRing_EntityType.ACCOUNT);
					if(accountFields != null) {
						Set<String> setMergedFields = new Set<String>();
						List<String> listMergedFields = new List<String>();
						if(!accountFields.equals(personAccountFields)) {
							accountFields.addAll(personAccountFields);
							setMergedFields.addAll(accountFields);
							listMergedFields.addAll(setMergedFields);
							tempFields.put(TwoRing_EntityType.ACCOUNT, listMergedFields);
						}
					} else {
						tempFields.put(TwoRing_EntityType.ACCOUNT, personAccountFields);
					}
				} else {
					findOnlyPersonAccount = true;
					tempEntities.add(TwoRing_EntityType.ACCOUNT);
					tempFields.put(TwoRing_EntityType.ACCOUNT, personAccountFields);
				}
			}
		} else if(tempEntities.contains(TwoRing_EntityType.ACCOUNT)) {
			findOnlyAccount = true;
		}

		if (tempFields != null) {
			for(String entityType:tempEntities) {
					List<String> entityTypeFields = tempFields.get(entityType);
					String result = '';
					if(entityTypeFields != null && entityTypeFields.size() > 0) {
						result = String.join(entityTypeFields, ', ');
						//add filter for IDs
						if(ids != null) {
							result += ' WHERE Id IN :ids';
						}
						//remove contacts that are part of Person Account
						if(entityType == TwoRing_EntityType.CONTACT && Schema.SObjectType.Contact.fields.getMap().containsKey('isPersonAccount')) {
								if(ids != null) {
									result += ' AND IsPersonAccount = false';
								}
								else {
									result += ' WHERE IsPersonAccount = false';
								}
						}
						if(entityType == TwoRing_EntityType.ACCOUNT && Schema.SObjectType.Account.fields.getMap().containsKey('isPersonAccount')) {
							if(findOnlyPersonAccount) {
								if(ids != null) {
									result += ' AND IsPersonAccount = true';
								}
								else {
									result += ' WHERE IsPersonAccount = true';
								}
							} else if (findOnlyAccount) {
								if(ids != null) {
									result += ' AND IsPersonAccount = false';
								}
								else {
									result += ' WHERE IsPersonAccount = false';
								}
							}
						}
					}
					if(result != '') {
						result = '(' + result + ')';
					}
					results.add(String.format('{0}{1}', new String[] { entityType, result }));
			}
		} 

		return (results.size() > 0) ? String.join(results, ', ') : '';
	}

	private static List<SObject> getEntitiesFromResults(Set<String> entityTypes, List<List<List<sObject>>> results) {
		List<SObject> entities = new List<SObject>();
		List<List<SObject>> listResults = new List<List<SObject>>();
		Set<SObject> setResults = new Set<SObject>();
		for(List<List<SObject>> criteria:results) {
			List<SObject> criteriaEntities = new List<SObject>();
			for(List<SObject> listEntity:criteria) {
				criteriaEntities.addAll(listEntity);
			}
			listResults.add(criteriaEntities);
		}
		setResults.addAll(listResults.get(0));
		for(Integer index = 1; index < listResults.size(); index++) {
			setResults.retainAll(listResults.get(index));
		}
		entities.addAll(setResults);
		return entities;
	}
	
	private static boolean isFieldAccessible(Schema.DescribeSObjectResult describeObjectResult, String fieldName) {
		if(fieldName.contains('.')) {
			String apiFieldName = fieldName.substringBefore('.');
			Schema.SObjectField relationshipField = null;
			
			relationshipField = describeObjectResult.fields.getMap().get(apiFieldName);
			if(relationshipField == null) {
				relationshipField = describeObjectResult.fields.getMap().get(apiFieldName + 'Id');
			}

			if(relationshipField == null) {
				return false;
			}
			else if(relationshipField.getDescribe().getRelationshipName().toLowerCase() == apiFieldName.toLowerCase()) {
				List<Schema.SObjectType> relationshipObjectTypes = relationshipField.getDescribe().getReferenceTo();
				for(integer i=0; i < relationshipObjectTypes.size(); i++) {
					if(isFieldAccessible(relationshipObjectTypes.get(i).getDescribe(), fieldName.substringAfter('.')) == true) {
						return true;
					}
				}
				return false;
			}
			else {
				return false;
			}
		}
		else {
			Schema.SObjectField objectField = describeObjectResult.fields.getMap().get(fieldName);
			return objectField != null && objectField.getDescribe().isAccessible();
		}
	}
}