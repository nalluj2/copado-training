@isTest
public class SVMX_ServiceReportPDFGenerateBatch_TEST {
//----------------------------------------------------------------------------------------
    // CREATE TEST DATA
    //----------------------------------------------------------------------------------------
    @testSetup static void createTestData() { 

        // Create Custom Setting Data
        clsTestData.createCustomSettingData(true);

        // Create Account Data
        clsTestData.iRecord_Account = 1;
        clsTestData.tCountry_Account = 'United Kingdom';
        clsTestData.createAccountData(false);
        clsTestData.oMain_Account.SAP_Id__c = 'SAPA001';
        insert clsTestData.oMain_Account;

        // Create parent SVMXC__Service_Order__c Data
        clsTestData.createSVMXCServiceOrderData(false);
        clsTestData.oMain_SVMXCServiceOrder.SVMX_SAP_Service_Order_No__c = 'T12345T';
        clsTestData.oMain_SVMXCServiceOrder.SVMX_Send_Back_to_Service_Center__c = true;
        clsTestData.oMain_SVMXCServiceOrder.SVMX_PS_Service_Report_To_Share__c = true;
        clsTestData.oMain_SVMXCServiceOrder.SVMX_PS_Is_Site_Visit__c = true;
        clsTestData.oMain_SVMXCServiceOrder.SVMX_Debrief_Complete__c = true;
        clsTestData.oMain_SVMXCServiceOrder.SVMX_PS_Service_Report_To_Share__c = false;
        clsTestData.oMain_SVMXCServiceOrder.SVMXC__Completed_Date_Time__c = Date.today().addDays(10);
        insert clsTestData.oMain_SVMXCServiceOrder;
        Id parentWOId = clsTestData.oMain_SVMXCServiceOrder.Id;


        // Create SVMXC__Site__c Data
        clsTestData.iRecord_SVMXCSite = 2;
        clsTestData.createSVMXCSite(true);

        // Create AdHoc related Data
        // child Service Order
        clsTestData.oMain_SVMXCServiceOrder = null;
        clsTestData.createSVMXCServiceOrderData(false);
        clsTestData.oMain_SVMXCServiceOrder.SVMXC__Purpose_of_Visit__c = 'Placeholder';
        clsTestData.oMain_SVMXCServiceOrder.SVMX_SAP_Service_Order_No__c = null;
        clsTestData.oMain_SVMXCServiceOrder.SVMX_Debrief_Complete__c = true;
        clsTestData.oMain_SVMXCServiceOrder.SVMX_Parent_Work_Order__c = parentWOId;
        clsTestData.oMain_SVMXCServiceOrder.SVMXC__Completed_Date_Time__c = Date.today().addDays(10);
        insert clsTestData.oMain_SVMXCServiceOrder;
    }
    //----------------------------------------------------------------------------------------
    @isTest static void runTest() {
        
        Test.startTest();
        System.schedule('SVMX_ServiceReportPDFGenerateBatch', '0 0 11 * * ?', new SVMX_ServiceReportPDFGenerateBatch());
Database.executeBatch(new SVMX_ServiceReportPDFGenerateBatch());
        
        Test.stopTest();
        
        List<SVMXC__Service_Order__c> childOrders = [Select Id,SVMX_PS_Service_Report_To_Share__c, SVMX_Parent_Work_Order__r.SVMX_PS_Service_Report_To_Share__c from SVMXC__Service_Order__c where SVMX_Parent_Work_Order__r.SVMX_SAP_Service_Order_No__c = 'T12345T'];
        List<Attachment> childAttachments = [Select Id from Attachment where ParentId IN : childOrders];
        
        System.assert(childAttachments.size() == 1);
        
        SVMXC__Service_Order__c parentOrder = [Select Id, SVMX_PS_Service_Report_To_Share__c from SVMXC__Service_Order__c where SVMX_SAP_Service_Order_No__c = 'T12345T'];
        
        System.assert(childOrders.get(0).SVMX_PS_Service_Report_To_Share__c == true);
      }
}