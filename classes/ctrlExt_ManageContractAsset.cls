//------------------------------------------------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 18-09-2019
//  Description      : APEX Controller Extension for VF Page manageContractAsset
//  Change Log       : 
//------------------------------------------------------------------------------------------------------------------------------------------------------------
public with sharing class ctrlExt_ManageContractAsset{

	//--------------------------------------------------------------------------------------------------------------------------------------------------------
	// Private variables
	//--------------------------------------------------------------------------------------------------------------------------------------------------------
	private Map<Id, Asset> mapAsset;
	private List<AssetContract__c> lstAssetContract_Insert = new List<AssetContract__c>();
	private List<AssetContract__c> lstAssetContract_Delete = new List<AssetContract__c>();

    private Contract oContract { get; set; }
	//--------------------------------------------------------------------------------------------------------------------------------------------------------


	//--------------------------------------------------------------------------------------------------------------------------------------------------------
	// Constructor
	//--------------------------------------------------------------------------------------------------------------------------------------------------------
    public ctrlExt_ManageContractAsset(ApexPages.StandardController stdCtrl){
    	
		bRenderPage = true;

    	Id id_Contract = stdCtrl.getId();

    	if (id_Contract == null){
    	
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'No context Contract found'));
			bRenderPage = false;
    		return;

    	}

    	oContract = [SELECT Id, AccountId, Account.Name, Account.SAP_ID__c FROM Contract WHERE Id = :id_Contract];

		initialize();

	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------------


	//--------------------------------------------------------------------------------------------------------------------------------------------------------
	// Private Methods
	//--------------------------------------------------------------------------------------------------------------------------------------------------------
	private void initialize(){

		lstAssetContract_Insert = new List<AssetContract__c>();
		lstAssetContract_Delete = new List<AssetContract__c>();

		bShowConfirm = false;

    	lstSO_BU = new List<SelectOption>();
    	lstSO_BU.add(new SelectOption('', '-- All --'));
    	for (AggregateResult oAR : [SELECT Product2.Business_Unit_ID__r.Name BusinessUnit FROM Asset WHERE AccountId = :oContract.AccountId AND Product2.Business_Unit_ID__r.Business_Unit_Group__r.Name = 'Restorative' AND Product2.Business_Unit_ID__r.Name != null GROUP BY Product2.Business_Unit_ID__r.Name]){
    		
    		String tBU = (String)oAR.get('BusinessUnit');
    		lstSO_BU.add(new SelectOption(tBU, tBU));

    	}
    	lstSO_BU.sort();
    	
    	lstSO_ProductGroup = new List<SelectOption>();
    	lstSO_ProductGroup.add(new SelectOption('', '-- All --'));
    	for (AggregateResult oAR : [SELECT Product2.Product_Group__r.Name ProductGroup FROM Asset WHERE AccountId = :oContract.AccountId AND Product2.Business_Unit_ID__r.Business_Unit_Group__r.Name = 'Restorative' AND Product2.Product_Group__r.Name != null GROUP BY Product2.Product_Group__r.Name]){
    		
    		String tProductGroup = (String)oAR.get('ProductGroup');
    		lstSO_ProductGroup.add(new SelectOption(tProductGroup, tProductGroup));

    	}
    	lstSO_ProductGroup.sort();

    	lstSO_Status = new List<SelectOption>();
    	for (AggregateResult oAR : [SELECT Status FROM Asset WHERE AccountId = :oContract.AccountId AND Product2.Business_Unit_ID__r.Business_Unit_Group__r.Name = 'Restorative' AND Status != null GROUP BY Status]){
    		
    		String tStatus = (String)oAR.get('Status');
    		lstSO_Status.add(new SelectOption(tStatus, tStatus));

    	}
    	lstSO_Status.sort();
    	
    	lstFilter_Status = new List<String>();


	}


	private Map<String, String> getDIENCodeMapping(){
	
		Map<String, String> mapDIENCodeMapping = new Map<String, String>();

		for (Mapping_Asset_ServiceLevel_DIENCode__c oMapping : [SELECT CFN_Code__c, Service_Level__c, DIEN_Code__c FROM Mapping_Asset_ServiceLevel_DIENCode__c WHERE CFN_Code__c != null]){

			String tKey = oMapping.CFN_Code__c + '$$' + oMapping.Service_Level__c;
			mapDIENCodeMapping.put(tKey, oMapping.DIEN_Code__c);

		}

		return mapDIENCodeMapping;

	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------------


	//--------------------------------------------------------------------------------------------------------------------------------------------------------
	// Getters & Setters
	//--------------------------------------------------------------------------------------------------------------------------------------------------------
	public Boolean bRenderPage { get; private set; }

    public List<wrData> lstWRData { get; set;}
	public Boolean bEnableButton_ADD_DEL { 
		get{
			return (lstAssetContract_Insert.size() > 0 || lstAssetContract_Delete.size() > 0);
		}
	}
	public Boolean bShowConfirm { get; private set; }
    
    public Boolean bBulkSelected_ADD { get; set; }
    public Boolean bBulkSelected_DEL { get; set; }
    public AssetContract__c oBulkServiceLevel { get; set; }
    
    public String tFilter_BU { get; set; }
    public List<SelectOption> lstSO_BU { get; set; }
    
    public String tFilter_ProductGroup { get; set; }
    public List<SelectOption> lstSO_ProductGroup { get; set; }
    
    public List<String> lstFilter_Status { get; set; }
    public List<SelectOption> lstSO_Status { get; set; }

	public Integer iNum_ADD { 

		get{
			return lstAssetContract_Insert.size();
		}

	}

	public Integer iNum_DEL { 

		get{
			return lstAssetContract_Delete.size();
		}

	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------------
    

	//--------------------------------------------------------------------------------------------------------------------------------------------------------
	// Actions
	//--------------------------------------------------------------------------------------------------------------------------------------------------------
	public void loadAssetData(){
    	
    	Map<Id, AssetContract__c> mapAssetAddedToContract = new Map<Id, AssetContract__c>();
    	
    	for (AssetContract__c oAssetContract : [SELECT Id, Asset__c, Service_Level__c FROM AssetContract__c WHERE Contract__c = :oContract.Id]){
    		
    		mapAssetAddedToContract.put(oAssetContract.Asset__c, oAssetContract);

    	}
    	
    	lstWRData = new List<wrData>();
		lstAssetContract_Insert = new List<AssetContract__c>();
		lstAssetContract_Delete = new List<AssetContract__c>();

    	
    	String tSOQL = 'SELECT Id, Name, Serial_Nr__c, CFN_Text__c, Status, Product2Id, Product2.Business_Unit_ID__c, Product2.Product_Group__c';
		tSOQL += ' FROM Asset';
		tSOQL += ' WHERE AccountId = \'' + oContract.AccountId + '\' AND Product2.Business_Unit_ID__r.Business_Unit_Group__r.Name = \'Restorative\'';
    	
    	if (String.isNotBlank(tFilter_BU)) tSOQL += ' AND Product2.Business_Unit_ID__r.Name = \'' + tFilter_BU + '\'';
    	if (String.isNotBlank(tFilter_ProductGroup)) tSOQL += ' AND Product2.Product_Group__r.Name = \'' + tFilter_ProductGroup + '\'';
    	if (lstFilter_Status.size() > 0) tSOQL += ' AND Status IN (\'' + String.join(lstFilter_Status, '\',\'') + '\')';
    	    	
    	tSOQL += ' ORDER BY Serial_Nr__c, CFN_Text__c, Name';
    	
		mapAsset = new Map<Id, Asset>();

    	for (Asset oAsset : Database.query(tSOQL)){
    		
			mapAsset.put(oAsset.Id, oAsset);

    		wrData oWRData= new wrData();
    		oWRData.oAsset = oAsset;
    		
    		if (mapAssetAddedToContract.containsKey(oAsset.Id)){
    			
    			oWRData.bSelected_ADD = true;
    			oWRData.oAssetContract = mapAssetAddedToContract.get(oAsset.Id);
    			
    		}else{
    			
    			AssetContract__c oAssetContract = new AssetContract__c();
    				oAssetContract.Contract__c = oContract.Id;
    				oAssetContract.Asset__c = oAsset.Id;
    			oWRData.oAssetContract = oAssetContract;

    		}
    		
    		lstWRData.add(oWRData);
    	}
    	
    	bBulkSelected_ADD = false;
		bBulkSelected_DEL = false;
    	oBulkServiceLevel = new AssetContract__c();

    }


    public void selectionAddChanged(){

    	lstAssetContract_Insert = new List<AssetContract__c>();
    	for (wrData oWRData : lstWRData){
    			
    		if (oWRData.bSelected_ADD == true && oWRData.oAssetContract.Id == null){
				lstAssetContract_Insert.add(oWRData.oAssetContract);
			}

    	}
		
	}    

    public void bulkSelectedAddChange(){
    	
    	lstAssetContract_Insert = new List<AssetContract__c>();

    	for (wrData oWRData : lstWRData){
    		
    		if (oWRData.oAssetContract.Id == null){
				oWRData.bSelected_ADD = bBulkSelected_ADD;
				if (bBulkSelected_ADD){
					lstAssetContract_Insert.add(oWRData.oAssetContract);
				}
			}

    	}    	

    }


    public void selectionDelChanged(){

    	lstAssetContract_Delete = new List<AssetContract__c>();
    	for (wrData oWRData : lstWRData){
    			
			if (oWRData.oAssetContract.Id != null && oWRData.bSelected_DEL){
				lstAssetContract_Delete.add(oWRData.oAssetContract);
			}

    	}
		
	}

    public void bulkSelectedDelChange(){
    	
    	lstAssetContract_Delete = new List<AssetContract__c>();

    	for (wrData oWRData : lstWRData){
    		
    		if (oWRData.oAssetContract.Id != null){
				oWRData.bSelected_DEL = bBulkSelected_DEL;	
				if (bBulkSelected_DEL){
					lstAssetContract_Delete.add(oWRData.oAssetContract);
				}
			}

    	}    	

    }


    public void bulkServiceLevelChange(){
    	
    	for (wrData oWRData : lstWRData){
    		
    		if (oWRData.oAssetContract.Id == null) oWRData.oAssetContract.Service_Level__c = oBulkServiceLevel.Service_Level__c;	

    	}    	

    }


    public void filterBUChange(){
    	
    	lstSO_ProductGroup = new List<SelectOption>();
    	lstSO_ProductGroup.add(new SelectOption('', '-- All --'));
    	
    	String tSOQL = 'SELECT Product2.Product_Group__r.Name ProductGroup FROM Asset WHERE AccountId = \'' + oContract.AccountId + '\' AND Product2.Business_Unit_ID__r.Business_Unit_Group__r.Name = \'Restorative\' AND Product2.Product_Group__r.Name != null';
    	if (String.isNotBlank(tFilter_BU)) tSOQL += ' AND Product2.Business_Unit_ID__r.Name = \'' + tFilter_BU + '\' ';
    	tSOQL += ' GROUP BY Product2.Product_Group__r.Name';
    	
    	for (AggregateResult oAR : Database.query(tSOQL)){
    		
    		String tProductGroup = (String)oAR.get('ProductGroup');
    		lstSO_ProductGroup.add(new SelectOption(tProductGroup, tProductGroup));

    	}
    	
    	lstSO_ProductGroup.sort();
    	
    	tFilter_ProductGroup = null;
    	
    	filterProductGroupChange();

    }

    
    public void filterProductGroupChange(){
    	
    	lstSO_Status = new List<SelectOption>();
    	
    	String tSOQL = 'SELECT Status FROM Asset WHERE AccountId = \'' + oContract.AccountId + '\' AND Product2.Business_Unit_ID__r.Business_Unit_Group__r.Name = \'Restorative\' AND Status != null';
    	
    	if (String.isNotBlank(tFilter_BU)) tSOQL += ' AND Product2.Business_Unit_ID__r.Name = \'' + tFilter_BU + '\'';
    	if (String.isNotBlank(tFilter_ProductGroup)) tSOQL += ' AND Product2.Product_Group__r.Name = \'' + tFilter_ProductGroup + '\'';
    	
    	tSOQL += ' GROUP BY Status';

		for (AggregateResult oAR : Database.query(tSOQL)){
    		
    		String tStatus = (String)oAR.get('Status');
    		lstSO_Status.add(new SelectOption(tStatus, tStatus));

    	}
    	    	
    	lstSO_Status.sort();
    	
    	lstFilter_Status.clear();

    }


    public PageReference addDelAsset(){
    	
    	try{
    		
			if (Test.isRunningTest()) clsUtil.bubbleException('ctrlExt_ManageContractAsset.addDelAsset');
			Boolean bLoadData = false;

    		if (lstAssetContract_Insert.size() > 0){

				Map<String, String> mapDIENCode = getDIENCodeMapping();
				for (AssetContract__c oAssetContract : lstAssetContract_Insert){

					if (mapAsset.containsKey(oAssetContract.Asset__c)){

						String tKey = mapAsset.get(oAssetContract.Asset__c).CFN_Text__c + '$$' + oAssetContract.Service_Level__c;
						if (mapDIENCode.containsKey(tKey)) oAssetContract.DIEN_Code__c = mapDIENCode.get(tKey);

					}

				}

				insert lstAssetContract_Insert;
				bLoadData = true;

			}

			if (lstAssetContract_Delete.size() > 0){

				delete lstAssetContract_Delete;
				bLoadData = true;

			}

			if (bLoadData) loadAssetData();

    		
    	}catch(Exception e){
    		
    		ApexPages.addMessages(e);
    		return null;	
    	}
    	    	
    	return null;

    }
	//--------------------------------------------------------------------------------------------------------------------------------------------------------


	//--------------------------------------------------------------------------------------------------------------------------------------------------------
	// Wrapper Class
	//--------------------------------------------------------------------------------------------------------------------------------------------------------
    public class wrData{
    	
    	public Boolean bSelected_ADD { get; set; }
    	public Boolean bSelected_DEL { get; set; }
    	public Asset oAsset { get; set; }
    	public AssetContract__c oAssetContract { get; set; }

    }
	//--------------------------------------------------------------------------------------------------------------------------------------------------------

}
//------------------------------------------------------------------------------------------------------------------------------------------------------------