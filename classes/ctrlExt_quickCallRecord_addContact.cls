public with sharing class ctrlExt_quickCallRecord_addContact {
	
	public Contact_Visit_Report__c callContact {get; set;}
	
	public Boolean isSaveError {get; set;}
	
	public Boolean contactControlled {get; set;}
	public Boolean accountControlled {get; set;}
	
	public List<SelectOption> possibleAccounts {get; set;}
	public List<SelectOption> possibleContacts {get; set;}
		
	public ctrlExt_quickCallRecord_addContact ( ApexPages.StandardController sc){
		
		Call_Records__c callRecord = (Call_Records__c) sc.getRecord();
		
		callContact = new Contact_Visit_Report__c();
		callContact.Call_Records__c = callRecord.Id;
		
		contactControlled = false;
		accountControlled = false;
	}
	
	public void selectedContact(){
		
		callContact.Attending_Affiliated_Account__c = null;
		
		if(callContact.Attending_Contact__c!=null && String.valueOf(callContact.Attending_Contact__c)!= ''){
			
			contactControlled = true;
			
			possibleAccounts = new List<SelectOption>();
            
            possibleAccounts.add(new SelectOption('','-- None --'));
                
	        for(Affiliation__c aff : [SELECT Affiliation_From_Contact__r.AccountId, Affiliation_To_Account__c, Affiliation_To_Account__r.Name FROM Affiliation__c
	                                        where RecordType.DeveloperName = 'C2A'
	                                        and Affiliation_Active__c = true
	                                        and (Affiliation_Type__c <> 'Referring')
	                                        and Affiliation_To_Account__r.Id != null
	                                        and Affiliation_From_Contact__c=:callContact.Attending_Contact__c
	                                        ORDER BY Affiliation_To_Account__r.Name ASC]){
	                                        	
	            String id = aff.Affiliation_To_Account__c;
	            String name = aff.Affiliation_To_Account__r.Name;
	                
	        	possibleAccounts.add(new SelectOption(id,name));
	        	
	        	if(aff.Affiliation_From_Contact__r.AccountId == aff.Affiliation_To_Account__c){
	        		callContact.Attending_Affiliated_Account__c = aff.Affiliation_To_Account__c;
	        	}
	        }						
			
		}else{
			
			contactControlled = false;
			possibleAccounts = null;
		}				
	}
	
	public void selectedAccount(){
		
		callContact.Attending_Contact__c = null;
		
		if(callContact.Attending_Affiliated_Account__c!=null && String.valueOf(callContact.Attending_Affiliated_Account__c)!= ''){
			
			accountControlled = true;
			
			possibleContacts = new List<SelectOption>();
            
            possibleContacts.add(new SelectOption('','-- None --'));
                
	        for(Affiliation__c aff : [SELECT Affiliation_From_Contact__c, Affiliation_From_Contact__r.Name FROM Affiliation__c
	                                        where RecordType.DeveloperName = 'C2A'
	                                        and Affiliation_Active__c = true
	                                        and (Affiliation_Type__c <> 'Referring')
	                                        and Affiliation_From_Contact__r.Id != null
	                                        and Affiliation_To_Account__c=:callContact.Attending_Affiliated_Account__c
	                                        ORDER BY Affiliation_From_Contact__r.Name ASC]){
	                                        	
	            String id = aff.Affiliation_From_Contact__c;
	            String name = aff.Affiliation_From_Contact__r.Name;
	                
	        	possibleContacts.add(new SelectOption(id,name));
	        }       
						
			
		}else{
			
			accountControlled = false;
			possibleContacts = null;
		}				
	}
	
	public PageReference save(){
    	
    	isSaveError = false;
    	    	
    	if (callContact.Attending_Affiliated_Account__c == null || String.valueOf(callContact.Attending_Affiliated_Account__c)== '') {    		
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Related Account is required'));            
            isSaveError=true;
    	}
    	
    	if (callContact.Attending_Contact__c == null || String.valueOf(callContact.Attending_Contact__c)== '') {    		
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Attended Contact is required'));            
            isSaveError=true;
    	}
    	    	
    	if(isSaveError == true) return null;
    	    	    	
    	try{
    		   		
    		insert callContact;
    		    		    		
    	}catch(Exception e){
    		
    		ApexPages.addMessages(e);
    		   		   		
    		isSaveError=true;
    		
    		if(Test.isRunningTest() == true) throw e;
    		
    		return null;
    	}
    	    	    	
    	return null;
    }      
}