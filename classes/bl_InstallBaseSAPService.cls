public with sharing class bl_InstallBaseSAPService {
    
    public static Map<String, SVMXC__Installed_Product__c> getInstallProductInfo(Set<Id> Ids)
    {
    	List<SVMXC__Installed_Product__c> installedProducts = [SELECT Id, 
    																  SVMXC__Warranty__c, 
    																  SVMXC__Warranty_End_Date__c,
    																  SVMXC__Warranty_Exchange_Type__c, 
    																  SVMXC__Warranty_Start_Date__c, 
    																  SVMXC__Service_Contract_Start_Date__c,
    																  SVMXC__Service_Contract_End_Date__c,
    																  SVMX_Contract_Number__c,
    																  SVMX_Contract_Type__c
    														   FROM SVMXC__Installed_Product__c
    														   WHERE Id IN :Ids];
    	
    	Map<String, SVMXC__Installed_Product__c> installedProductsByEquipId = new Map<String, SVMXC__Installed_Product__c>();
    	for (SVMXC__Installed_Product__c installedProduct: installedProducts)
    		installedProductsByEquipId.put(installedProduct.Id, installedProduct);
    	
    	return installedProductsByEquipId;
    }
    
    public static SVMXC__Installed_Product__c getInstallProductInfo(String installedProductId){
    	
    	List<SVMXC__Installed_Product__c> prod = [SELECT Id,   			
    						SVMX_SAP_Equipment_ID__c,			
							SVMX_Software_Version__c,
							SVMXC__Asset_Tag__c,
							SVMX_Equipment_Use_Status__c,
							Equipment_Room__c,
							SVMX_Optional_Software_Installed__c,
							Last_Disinfection_Date__c,
							Last_PM_Date__c
						FROM SVMXC__Installed_Product__c
						WHERE SVMX_SAP_Equipment_ID__c = :installedProductId];
                
    	
    	if(prod.isEmpty()) throw new ws_Exception('No Installed Product was found for Id: ' +installedProductId);  	
    	if(prod.size() > 1) throw new ws_Exception('Multiple Installed Products were found for Id: ' +installedProductId);
    	
    	return prod[0];
    }
    
    public static SVMXC__Installed_Product__c mapRequestToInstalledProduct(ws_InstallBaseSAPService.SAPInstallBaseRequest request){
    	
    	SVMXC__Installed_Product__c prod = getInstallProductInfo(request.INSTALL_PRODUCT_ID);
    	
    	return prod;
    }
    
    public static ws_InstallBaseSAPService.SAPInstallBaseResponse mapInstalledProductToResponse(SVMXC__Installed_Product__c prod){
    	
    	ws_InstallBaseSAPService.SAPInstallBaseResponse response = new ws_InstallBaseSAPService.SAPInstallBaseResponse(); 
		response.EQUIPMENT_NUMBER = ut_String.getStringValue(prod.SVMX_SAP_Equipment_ID__c);
		response.INVENTORY = ut_String.getStringValue(prod.SVMXC__Asset_Tag__c);
		response.CHARACTERISTIC = ut_String.getStringValue(prod.SVMX_Software_Version__c);    
		response.INSTALL_CLASS = '';    
		response.EQUIPMENT_ROOM = prod.Equipment_Room__c;
		response.OPTIONAL_SOFTWARE = prod.SVMX_Optional_Software_Installed__c;
		response.USER_STATUS = ws_SharedMethods.getSAPMapping('SVMXC__Installed_Product__c', 'SVMX_Equipment_Use_Status__c', ut_String.getStringValue(prod.SVMX_Equipment_Use_Status__c));
		response.LAST_DISINFECTION_DATE = dateToString(prod.Last_Disinfection_Date__c);
		response.LAST_PM_DATE = dateToString(prod.Last_PM_Date__c);
		    	
    	return response;	 
    }
    
    // Convert Date into String with UTC format and timezone
    private static String dateToString(Date inputDate){
    	
    	if(inputDate == null) return null;
    	
    	String year = String.valueOf(inputDate.year());
    	String month = addPaddingCeros(String.valueOf(inputDate.month()), 2);
    	String day = addPaddingCeros(String.valueOf(inputDate.day()), 2);
    	
    	return year + '-' + month +'-' + day;
    }
    
    private static String addPaddingCeros(String input, Integer length){
    	
    	while(input.length() < length){
    		input = '0' + input;
    	}
    	
    	return input;
    }
}