@isTest
private class Test_bl_Product_Country_Availability {
        
    static testMethod void test_approvedRejectedEmail(){
		
		Product2 prod = new Product2();
		prod.Name = 'Test Product';
		prod.RecordTypeId = [Select Id from RecordType where SObjectType = 'Product2' AND DeveloperName = 'SAP_Product'].Id;
		prod.KitableIndex__c = 'Not Approved';
		insert prod;
				
		DIB_Country__c countryNL = new DIB_Country__c();
		countryNL.Name = 'Netherlands';
		countryNL.Region_vs__c = 'Europe';
		countryNL.Box_Builder_enabled__c = true;
		countryNL.Country_ISO_Code__c = 'NL';
		
		DIB_Country__c countryBE = new DIB_Country__c();
		countryBE.Name = 'Belgium';
		countryBE.Region_vs__c = 'Europe';
		countryBE.Box_Builder_enabled__c = true;
		countryBE.Country_ISO_Code__c = 'BE';
									
		insert new List<DiB_Country__c>{countryNL, countryBE};
		
		Product_Country_Availability__c pcaNL = new Product_Country_Availability__c();
		pcaNL.Product__c = prod.Id;
		pcaNL.Country__c = countryNL.Id;
		pcaNL.Status__c = 'Not Approved';
		
		Product_Country_Availability__c pcaBE = new Product_Country_Availability__c();
		pcaBE.Product__c = prod.Id;
		pcaBE.Country__c = countryBE.Id;
		pcaBE.Status__c = 'Not Approved';
		
		insert new List<Product_Country_Availability__c>{pcaNL, pcaBE};
		
		Procedural_Solutions_Kit__c boxNL = new Procedural_Solutions_Kit__c();
		boxNL.Country__c = 'Netherlands';
		boxNL.Status__c = 'Open';
		
		Procedural_Solutions_Kit__c boxBE = new Procedural_Solutions_Kit__c();
		boxBE.Country__c = 'Belgium';
		boxBE.Status__c = 'Open'; 
		
		insert new List<Procedural_Solutions_Kit__c>{boxNL, boxBE};
		
		Procedural_Solutions_Kit_Product__c boxNLProd = new Procedural_Solutions_Kit_Product__c();
		boxNLProd.Procedural_Solutions_Kit__c = boxNL.Id;
		boxNLProd.Product__c = prod.Id;
		boxNLProd.Quantity__c = 1;
		
		Procedural_Solutions_Kit_Product__c boxBEProd = new Procedural_Solutions_Kit_Product__c();
		boxBEProd.Procedural_Solutions_Kit__c = boxBE.Id;
		boxBEProd.Product__c = prod.Id;
		boxBEProd.Quantity__c = 1;
		
		insert new List<Procedural_Solutions_Kit_Product__c>{boxNLProd, boxBEProd};
		
		Test.startTest();
		
		pcaNL.Status__c = 'Available';
		pcaBE.Status__c = 'Request Rejected';
		pcaBE.Rejected_Reason__c = 'Unit test reason';
		
		update new List<Product_Country_Availability__c>{pcaNL, pcaBE};
		
		System.assertEquals(2, Limits.getEmailInvocations());
		
		prod.KitableIndex__c = 'Request Rejected';
		prod.Rejected_Reason__c = 'Unit test reason';
		update prod;
		
		System.assertEquals(3, Limits.getEmailInvocations());
    }
    
    private static testmethod void testSyncSpainCanaryIslands(){
    	
    	Product2 prod = new Product2();
		prod.Name = 'Test Product';
		prod.RecordTypeId = [Select Id from RecordType where SObjectType = 'Product2' AND DeveloperName = 'SAP_Product'].Id;
		prod.KitableIndex__c = 'Not Approved';
		insert prod;
				
		DIB_Country__c countryES = new DIB_Country__c();
		countryES.Name = 'Spain';
		countryES.Region_vs__c = 'Europe';
		countryES.Box_Builder_enabled__c = true;
		countryES.Country_ISO_Code__c = 'ES';
		
		DIB_Country__c countryIC = new DIB_Country__c();
		countryIC.Name = 'Canary Islands';
		countryIC.Region_vs__c = 'Europe';
		countryIC.Box_Builder_enabled__c = true;
		countryIC.Country_ISO_Code__c = 'IC';
									
		insert new List<DiB_Country__c>{countryES, countryIC};
		
		Product_Country_Availability__c pcaES = new Product_Country_Availability__c();
		pcaES.Product__c = prod.Id;
		pcaES.Country__c = countryES.Id;
		pcaES.Status__c = 'Available';
		pcaES.Unique_Key__c = prod.Id + ':' + countryES.Id;
		insert pcaES;
		
		Product_Country_Availability__c pcaIC = [Select Id, Product__c, Country__c, Status__c, Unique_key__c from Product_Country_Availability__c where Product__c = :prod.Id AND Country__c = :countryIC.ID];
		
		System.assert(pcaIC.Status__c == 'Available');
		System.assert(pcaIC.Unique_key__c == prod.Id + ':' + countryIC.Id);
		
		pcaES.Status__c = 'Request Rejected';
		pcaES.Rejected_Reason__c = 'Test Rejection';
		
		update pcaES;
		
		pcaIC = [Select Id, Product__c, Country__c, Status__c, Unique_key__c, Rejected_Reason__c from Product_Country_Availability__c where Product__c = :prod.Id AND Country__c = :countryIC.ID];
		
		System.assert(pcaIC.Status__c == 'Request Rejected');
		System.assert(pcaIC.Rejected_Reason__c == 'Test Rejection');    	
    }
}