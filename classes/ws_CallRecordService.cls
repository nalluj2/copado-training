/*
 *      Description : This class exposes methods related to Call Record creation
 *
 *      Author = Rudy De Coninck
 */

@RestResource(urlMapping='/CallRecordService/*')
global with sharing class ws_CallRecordService {

		@HttpDelete
		global static String doDelete(){
			RestRequest req = RestContext.request;
			
			System.debug('delete called '+req);
			System.debug('delete requestBody '+req.requestBody);
			
			String body = req.requestBody.toString();
    		IFCallRecordDeleteRequest callRecordDeleteRequest = (IFCallRecordDeleteRequest)System.Json.deserialize(body, IFCallRecordDeleteRequest.class);
			
			System.debug('post requestBody '+callRecordDeleteRequest);
			
			Call_Records__c inputCallRecord = callRecordDeleteRequest.callRecord;
			IFCallRecordDeleteResponse resp = new IFCallRecordDeleteResponse();
			
			List<Call_Records__c> callRecordsToDelete = [select id, mobile_id__c from Call_Records__c where Mobile_ID__c = :inputCallRecord.Mobile_ID__c];
			
			try{
				
				if (callRecordsToDelete !=null && callRecordsToDelete.size()>0 ){	
					
					Call_Records__c callRecordToDelete = callRecordsToDelete.get(0);
					delete callRecordToDelete;
					resp.callRecord = callRecordToDelete;
					resp.id = callRecordDeleteRequest.id;
					resp.success = true;
				}else{
					resp.callRecord = inputCallRecord;
					resp.id = callRecordDeleteRequest.id;
					resp.message='CallRecord not found';
					resp.success = true;
				}
				
			}catch (Exception e){
				
				resp.message=e.getMessage();
				resp.success=false;
			}
  			
			
			bl_Mobilesync.storeRequest(UserInfo.getUserId(),'CallRecordDelete',req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
			
			return System.JSON.serialize(resp);
		}
		
		
		@HttpPost
  		global static String doPost() {
			
			RestRequest req = RestContext.request;
			
			System.debug('post called '+req);
			System.debug('post requestBody '+req.requestBody);
			
			String body = req.requestBody.toString();
    		IFCallRecordRequest callRecordRequest = (IFCallRecordRequest)System.Json.deserialize(body, IFCallRecordRequest.class);
			
			System.debug('post requestBody '+callRecordRequest);
			
			Call_Records__c inputCallRecord = callRecordRequest.callRecord;
			System.debug('before upsert '+inputCallRecord);
			Call_Records__c processCallRecord = callRecordRequest.callRecord.clone();
			
  			IFCallRecordResponse resp = new IFCallRecordResponse();

			//We catch all exception to assure that 
			try{
				//Set source mobilecallrecording
				processCallRecord.Mobile_Call_Recording__c = true;
				processCallRecord.RecordTypeId = RecordTypeMedtronic.CallRecord('Business_Unit').Id;
				
				upsert processCallRecord Mobile_Id__c; 
				resp.callRecord = inputCallRecord;
				resp.id = callRecordRequest.id;
	
				System.debug('created '+inputCallRecord);
				
				
	  			List<Contact_Visit_Report__c> attendedContacts= bl_CallManagement.saveCallContacts( inputCallRecord, callRecordRequest.attendedContacts);
				resp.attendedContacts = attendedContacts;
				List<Call_Topics__c> callTopics = bl_CallManagement.saveCallTopics(inputCallRecord, callrecordRequest.callTopics);
				resp.callTopics = callTopics;
				
				
				List<Call_Topic_Subject__c> callTopicSubjects = bl_CallManagement.saveCallTopicSubjects(inputCallRecord, callrecordRequest.callTopicSubjects);
				resp.callTopicSubjects = callTopicSubjects;
				
				List<Call_Topic_Products__c> callTopicProducts = bl_CallManagement.saveCallTopicProducts(inputCallRecord, callrecordRequest.callTopicProducts);
				resp.callTopicProducts = callTopicProducts;
								
				List<Call_Topic_Business_Unit__c> callTopicBusinessUnits = bl_CallManagement.saveCallTopicBusinessUnits(inputCallRecord, callrecordRequest.callTopicBusinessUnit);
	  			resp.callTopicBusinessUnit = callTopicBusinessUnits;
				
				resp.success = true;
			}catch (Exception e){
				
				resp.message=e.getMessage();
				resp.success=false;
			}
 
 			bl_Mobilesync.storeRequest(UserInfo.getUserId(),'CallRecord' ,req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
  			
			return System.JSON.serialize(resp);
  		}
	
		global class IFCallRecordRequest{

			public Call_Records__c callRecord {get;set;}
			public List<Contact_Visit_Report__c> attendedContacts {get;set;}
						
			public String id{get;set;}
			public List<Call_Topics__c> callTopics {get;set;}
			public List<Call_Topic_Subject__c> callTopicSubjects {get;set;}
			public List<Call_Topic_Products__c> callTopicProducts {get;set;}
			public List<Call_Topic_Business_Unit__c> callTopicBusinessUnit {get;set;}
		
		}
		
		global class IFCallRecordDeleteRequest{

			public Call_Records__c callRecord {get;set;}
			public String id{get;set;}
		
		}
		
		global class IFCallRecordDeleteResponse{

			public Call_Records__c callRecord {get;set;}
			public String id{get;set;}
			public boolean success {get;set;}
			public String message {get;set;} 
		
		}

		global class IFCallRecordResponse{

			public Call_Records__c callRecord {get;set;}
			public List<Contact_Visit_Report__c> attendedContacts {get;set;}			
			public String id{get;set;}
			public List<Call_Topics__c> callTopics {get;set;}
			public List<Call_Topic_Subject__c> callTopicSubjects {get;set;}
			public List<Call_Topic_Products__c> callTopicProducts {get;set;}
			public List<Call_Topic_Business_Unit__c> callTopicBusinessUnit {get;set;}
			public boolean success {get;set;}
			public String message {get;set;} 
		}
		
		
		
		
}