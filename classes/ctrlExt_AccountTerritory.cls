public with sharing class ctrlExt_AccountTerritory {

	private final Account acct;
	public boolean manageTerrRights{get;set;}
    
    public ctrlExt_AccountTerritory(ApexPages.StandardController stdController) {
        this.acct = (Account)stdController.getRecord();
        manageTerrRights = false;
		if (ProfileMedtronic.isSystemAdministratorProfile(UserInfo.getProfileId())){
			manageTerrRights= true;
		}
		List<PermissionSetAssignment> perm = [
			SELECT AssigneeId,Id,PermissionSetId 
			FROM PermissionSetAssignment 
			where PermissionSet.name='TMA_Access' and AssigneeId = :UserInfo.getUserId()];
		if (perm.size()==1){
			manageTerrRights= true;
		}
    }


	public List<Territory2> getTerritories(){
		return bl_Territory.getTerritoriesForAccount(acct.id); 
	}
	
}