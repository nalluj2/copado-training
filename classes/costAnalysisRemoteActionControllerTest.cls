@isTest
private class costAnalysisRemoteActionControllerTest{

    static testmethod void testCovProductsEmpty(){
        Test.startTest();
        String region = 'EUR';        
        String searchText = '';
        List<SObject> products = costAnalysisRemoteActionController.covProducts(region,searchText);
        Test.stopTest();
        System.assertEquals(0, products.size());
    }    
    
    static testmethod void testCovProductsFilled(){
        
        Company__c cmpny = new Company__c();		
        cmpny.name='Europe';
        cmpny.CurrencyIsoCode = 'EUR';       
        cmpny.Company_Code_Text__c = 'EUR';
        cmpny.Current_day_in_Q1__c=56;
        cmpny.Current_day_in_Q2__c=34; 
        cmpny.Current_day_in_Q3__c=5; 
        cmpny.Current_day_in_Q4__c= 0;   
        cmpny.Current_day_in_year__c =200;
        cmpny.Days_in_Q1__c=57;  
        cmpny.Days_in_Q2__c=35;
        cmpny.Days_in_Q3__c=13;
        cmpny.Days_in_Q4__c=22;
        cmpny.Days_in_year__c =250;
		insert cmpny;
		
		Business_Unit__c bu =  new Business_Unit__c();
        bu.Company__c =cmpny.id;
        bu.name='MITG Test BU';                
        insert bu;
        
        Product2 product = new Product2() ;
        product.Name = 'Test MITG Product';
        product.Business_Unit_ID__c = bu.Id;
        product.Region_vs__c = 'Europe';
        insert product;
        
        Test.startTest();
        
        String region = 'Europe';        
        String searchText = '%MITG%';
        List<SObject> products = costAnalysisRemoteActionController.covProducts(region, searchText);
        Test.stopTest();
        System.assertEquals(1, products.size());
    }    
    
    static testmethod void gettersTests(){
        Test.startTest();
        costAnalysisRemoteActionController instance = new costAnalysisRemoteActionController(new ngForceController()); 
        System.assertEquals(instance.userLang, UserInfo.getLanguage());
        System.assertEquals(instance.userLocale , UserInfo.getLocale());
        String enDict = instance.enDict;
        String frDict = instance.frDict;
        String deDict = instance.deDict;
        String itDict = instance.itDict;
        String esDict = instance.esDict;

    }  
}