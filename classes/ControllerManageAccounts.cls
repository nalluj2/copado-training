public class ControllerManageAccounts {

    List<wrapClass>lstwrap;
    List<wrapperClass>lstSelectedAccount;
    public Id campaignId{get;set;}
    public List<SelectOption> options {get;set;}
    public String selected{get;set;}
    public Target_Account__c tac{get;set;}
    public boolean checkSelected{get;set;}//////
    public boolean boolLstView{get;set;}
    public string selectedTab{get;set;}
    public string strSelFilter{get;set;}
       
    List<Target_Account__c>lsttargetAccount=new List<Target_Account__c>();
    List<Target_Account__c>lstApprovedtargetAccount=new List<Target_Account__c>();
    List<Target_Account__c> lstRemovetargetAccount=new List<Target_Account__c>();
        ////
    Public Integer noOfRecords{get; set;}
    Public Integer noOfRecordsTarget{get; set;}
    Public Integer size{get;set;}
        ////
    public boolean errortrgt{get;set;}
        //////
    String qryString;
    public String FieldName1{ get; set; }
    public String FieldName2{ get; set; }
    public String FieldName3{ get; set; } 
    public String FieldName4{ get; set; }
    public String FieldName5{ get; set; }
    public String OutName1{ get; set; }
    public String OutName2{ get; set; }
    public String OutName3{ get; set; }
    public String OutName4{ get; set; }
    public String OutName5{ get; set; }
    public String Value1{ get; set; }
    public String Value2{ get; set; } 
    public String Value3{ get; set; }
    public String Value4{ get; set; }
    public String Value5{ get; set; }
    public boolean check{get;set;}
        //////
  
    public ApexPages.StandardSetController setCon{get{
        if(setCon == null){
            size=100;
            setCon=new ApexPages.StandardSetController(Database.getQueryLocator([select id,type,name,phone,Industry,Account_Address_Line_1__c,Account_City__c,Strategic_Account__c  from account limit 9000]));
            setCon.setPageSize(size);
            noOfRecords = setCon.getResultSize();
        }
            return setCon;
        }set;}
   ////
        
    public ApexPages.StandardSetController setConAccount{get{
        if(setConAccount== null) {  
            size=100;
            setConAccount=new ApexPages.StandardSetController(Database.getQueryLocator([select id,name,Status__c,type__c,City__c,Address_Line_1__c,lastmodifiedDate from Target_Account__c where Campaign__c=:campaignId]));
            setConAccount.setPageSize(size);
            noOfRecordsTarget= setConAccount.getResultSize();
        }
            return setConAccount;
        }set;}
   ////
    public ControllerManageAccounts(ApexPages.StandardSetController controller) {
            
        check=true;
        Schema.DescribeSObjectResult r = Target_Account__c.SObjectType.getDescribe();
        check=r.isCreateable();
        checkSelected=true;
        selectedTab='name1';
        campaignId=ApexPages.currentPage().getParameters().get('cid'); 
    }
    
    public List<wrapClass>getlstwrap(){
        List<Account> Accounts= (List<Account>)setCon.getRecords();
        noOfRecords = setCon.getResultSize();
        lstwrap=new List<wrapClass>();
        for(Account acc:Accounts){
            wrapClass obj=new wrapClass(false,acc);
            lstwrap.add(obj);
        } 
            return lstwrap;
    }
    public void setlstwrap(List<wrapClass>lst){
        this.lstwrap=lst;
    }
    public List<wrapperClass>getlstSelectedAccount(){
        List<Target_Account__c> Accounts= (List<Target_Account__c>)setConAccount.getRecords();
        lstSelectedAccount=new List<wrapperClass>();
        for(Target_Account__c tacc:Accounts){
            wrapperClass obj=new wrapperClass(false,tacc);
            lstSelectedAccount.add(obj);
        } 
            return lstSelectedAccount;
    }
    public void setlstSelectedAccount(List<wrapperClass>lst){
        this.lstSelectedAccount=lst;
    }
     
    public class wrapClass{
        public boolean bln{get;set;}
        public Account acc{get;set;}
        //public Target_Account__c trgtacc{get;set;}  
        public wrapClass(boolean bln, Account acc){
            this.bln=bln;
            this.acc=acc;           
        }
    }
    public class wrapperClass{
        public boolean bn{get;set;}
        public Target_Account__c trgtacc{get;set;}
        public wrapperClass(boolean bn, Target_Account__c trgtacc){
            this.bn=bn;
            this.trgtacc=trgtacc;           
        }
    }
    public pageReference FilterAccount(){
        setCon.setFilterId(strSelFilter);
        boolLstView=true;
        return null;
    }
    
    public pageReference refresh() {
        setConAccount = null;
        getlstSelectedAccount();
        setConAccount.setPageNumber(1);
        return null;
    }
  
    public List<SelectOption> getcolumnNamesNew() {
        List<SelectOption> optionsFieldName = new List<SelectOption>();   
             
        optionsFieldName.add(new SelectOption('','--None--'));
        optionsFieldName.Add(new SelectOption('Name','Name'));
        optionsFieldName.Add(new SelectOption('City__c','City'));
        optionsFieldName.Add(new SelectOption('Type__c','Type'));
        optionsFieldName.Add(new SelectOption('Status__c','Status'));
        optionsFieldName.Add(new SelectOption('Address_Line_1__c','Address Line 1')); 
          
        return optionsFieldName ; 
    }

    public List<SelectOption> getOperatorNames() {
        List<SelectOption> optionsFieldName = new List<SelectOption>();       
        optionsFieldName .add(new SelectOption('','--None--'));
        optionsFieldName .add(new SelectOption('e','equals'));
        optionsFieldName .add(new SelectOption('n','not equal to'));
        optionsFieldName .add(new SelectOption('s','starts with'));
        optionsFieldName .add(new SelectOption('c','contains'));   
        //optionsFieldName .add(new SelectOption('k','does not contain'));      
        optionsFieldName .add(new SelectOption('l','less than'));
        optionsFieldName .add(new SelectOption('g','greater than'));
        optionsFieldName .add(new SelectOption('m','less or equal'));
        optionsFieldName .add(new SelectOption('h','greater or equal'));              
        return optionsFieldName ;        
    }  
        
    public pageReference filterAccountExisting(){
        String strOpr;
        if (FieldName1!= Null && OutName1!=Null ){
            strOpr=getOprater(OutName1,value1,FieldName1);   
            if(strOpr=='Error'){
                                                   
                return null;
             }
            qryString = 'Select ID,Name,Type__c,city__c,Address_Line_1__c,Status__c,LastModifiedDate from Target_Account__c WHERE campaign__c=:campaignId and ' + strOpr ;
            
                if (FieldName2!= Null && OutName2!=Null ){   
                    strOpr=getOprater(OutName2,value2,FieldName2);
                    if(strOpr=='Error'){
                        //String strTemp= ErrorMsg(fieldName2);                               
                        return null;
                    }
                    qryString = qryString + ' and ' + strOpr;
                    
                if (FieldName3!= Null && OutName3!=Null ){   
                    strOpr=getOprater(OutName3,value3,FieldName3);
                    if(strOpr=='Error'){
                        //String strTemp= ErrorMsg(fieldName3);                               
                        return null;
                    }
                    qryString = qryString + ' and ' + strOpr;
                    if (FieldName4!= Null && OutName4!=Null ){   
                        strOpr=getOprater(OutName4,value4,FieldName4);
                        if(strOpr=='Error'){
                            //String strTemp= ErrorMsg(fieldName4);                               
                            return null;
                        }
                        qryString = qryString + ' and ' + strOpr;
                        if (FieldName5!= Null && OutName5!=Null){   
                            strOpr=getOprater(OutName5,value5,FieldName5);
                            if(strOpr=='Error'){
                                //tring strTemp= ErrorMsg(fieldName5);                               
                                return null;
                            }
                            qryString = qryString + ' and ' + strOpr;
                        }
                    }
                 }       
                }
                
            }else{
            refresh();
            return null;
            }
     
                     setConAccount=new ApexPages.StandardSetController(Database.getQueryLocator(qryString)); 
                     
                     setConAccount.setPageSize(size);
                     noOfRecordsTarget= setConAccount.getResultSize();
  
          return null;
    }
 /////
    private string getOprater(String OutName,String Value,String FieldName){
           
        String fieldtype;//Account.PS_Company_Id__c Owner__r.Name
        if (FieldName!='Name'){
            Map<String, Schema.sObjectField> M = Schema.SObjectType.Target_Account__c.fields.getMap();
            schema.DescribeFieldResult F = m.get(FieldName).getDescribe();          
            fieldtype = F.getType().name();
        }
        else
        fieldtype='STRING';
        System.debug('Atulaa------------>'+fieldtype);
        string OptrName;
        //NUMBER Percent
        /*commented on 26 July by kaushal singh
        if ( fieldtype=='CURRENCY' || fieldtype=='NUMBER' || fieldtype=='PERCENT' || fieldtype=='INTEGER' ){
            if (OutName=='e'){
                OptrName=FieldName + ' = ' + Value  + '';       
            }       
            else if (OutName=='n'){
                OptrName=FieldName + ' != ' + Value  + '';   
            }
            else if (OutName=='s'){
                OptrName='Error';    
            }
            else if (OutName=='c'){
                OptrName='Error';   
            }
            else if (OutName=='k'){
                OptrName='Error';   
            }
            else if (OutName=='l'){
                OptrName=FieldName + ' < ' + Value; 
            }
            else if (OutName=='g'){
                OptrName=FieldName + ' > ' + Value; 
            }
            else if (OutName=='m'){
                OptrName=FieldName + ' <= ' + Value;    
            }
            else if (OutName=='h'){
                OptrName=FieldName + ' >= ' + Value;    
            }
        }
        else if (fieldtype=='BOOLEAN'){
            if (OutName=='e'){
                OptrName=FieldName + ' = ' + Value;         
            }       
            else if (OutName=='n'){
                OptrName=FieldName + ' != ' + Value;     
            }
            else if (OutName=='s'){
                OptrName='Error';    
            }
            else if (OutName=='c'){
                OptrName='Error';   
            }
            else if (OutName=='k'){
                OptrName='Error';   
            }
            else if (OutName=='l'){
                OptrName='Error';
            }
            else if (OutName=='g'){
                OptrName='Error';
            }
            else if (OutName=='m'){
                OptrName='Error';
            }
            else if (OutName=='h'){
                OptrName='Error';
            }
        }
        else if ( fieldtype=='DATE' || fieldtype=='DATETIME'){          
            List<String> parts = Value.split('/');          
            if (parts.size()==3){
                if (fieldtype=='Date') //T00:00:00-00:00
                    Value= parts[2] + '-' + parts[0] + '-' + parts[1];
                else{                   
                    //Value= parts[2] + '-' + parts[0] + '-' + parts[1] + 'T00:00:00-00:00'; //T00:00:00.000Z
                    if (parts[1].contains(':'))
                        Value= parts[2] + '-' + parts[0] + '-' + parts[1];                  
                    else            
                        Value= parts[2] + '-' + parts[0] + '-' + parts[1] + 'T00:00:00.000Z';
                }
                    
                
                
                if (OutName=='e'){
                    OptrName=FieldName + ' = ' + Value;         
                }       
                else if (OutName=='n'){
                    OptrName=FieldName + ' != ' + Value;     
                }
                else if (OutName=='s'){
                    OptrName='Error';    
                }
                else if (OutName=='c'){
                    OptrName='Error';   
                }           
                else if (OutName=='l'){
                    OptrName=FieldName + ' < ' + Value;
                }
                else if (OutName=='g'){
                    OptrName=FieldName + ' > ' + Value; 
                }
                else if (OutName=='m'){
                    OptrName=FieldName + ' <= ' + Value; 
                }
                else if (OutName=='h'){
                    OptrName=FieldName + ' >= ' + Value; 
                }  
            }
            else
                OptrName='Error';   
        }*/
       // else{
            if (OutName=='e'){
                OptrName=FieldName + ' = \'' + Value  + '\'';       
            }       
            else if (OutName=='n'){
                OptrName=FieldName + ' != \'' + Value + '\'';    
            }
            else if (OutName=='s'){
                OptrName=FieldName + ' like \'' + Value + '%\'';     
            }
            else if (OutName=='c'){
                OptrName=FieldName + ' like \'%' + Value+ '%\'';    
            }           
            else if (OutName=='l'){
                OptrName='Error';   
            }
            else if (OutName=='g'){
                OptrName='Error';   
            }
            else if (OutName=='m'){
                OptrName='Error';   
            }
            else if (OutName=='h'){
                OptrName='Error';   
            }
       // }
        return  OptrName;
       }
 /////
    public pageReference AddingTargetAccount(){
       //checkSelected
        lsttargetAccount.clear();
        List<Target_Account__c> lstTemTargetAccount=new List<Target_Account__c>();
        Map<Id,Id>mapOfAccIdandAccId=new Map<id,id>(); 
        lstTemTargetAccount=[Select Account__c ,id from Target_Account__c where campaign__c=:campaignId ];//Need to add filter of campaign
        for(Target_Account__c obj:lstTemTargetAccount){
            mapOfAccIdandAccId.put(obj.Account__c,obj.Account__c);
        }
        for(wrapClass obj:lstwrap){
            if(obj.bln==true && mapOfAccIdandAccId.get(obj.acc.id)==null ){
                //Target_Account__c taobj=new Target_Account__c(Name=obj.acc.name,status__c='Pending Approval',Account__c=obj.acc.id,Campaign__c=campaignId);
                //CR-12999 Name is a auto numer
                Target_Account__c taobj=new Target_Account__c(status__c='Pending Approval',Account__c=obj.acc.id,Campaign__c=campaignId);
                lsttargetAccount.add(taobj);
                checkSelected=false;
            }
        }
        if(lsttargetAccount.size()>0){
            insert lsttargetAccount;
            selectedTab='name2';
        }
        Refresh();
        return null;
    }
 /////
    public pageReference AddingApprovedTargetAccount(){
        lstApprovedtargetAccount.clear();
        for(wrapperClass obj:lstSelectedAccount){
            if(obj.bn==true){
                obj.trgtacc.status__c='Approved';
                lstApprovedtargetAccount.add(obj.trgtacc);
            }
        }
        if(lstApprovedtargetAccount.size()>0){
            update lstApprovedtargetAccount;
       }
       return null;
       }
       public pageReference RemoveTargetAccount(){
          lstRemovetargetAccount.clear();
          Set<id> Trgtactid=new Set<id>();
          for(wrapperClass obj:lstSelectedAccount){
            if(obj.bn==true){
                Trgtactid.add(obj.trgtacc.id);
            //obj.trgtacc.status__c='Approved';
            lstRemovetargetAccount.add(obj.trgtacc);
       }
       
       }
       List<CampaignMember> lstcmp=[Select id,Target_account__c from CampaignMember where Target_account__c in:Trgtactid];
       if(lstRemovetargetAccount.size()>0 && lstcmp.size()==0){
           errortrgt=false;
           delete lstRemovetargetAccount;
       }
       if(lstcmp.size()>0){
       errortrgt=true;  
       //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please delete related campaign members first');
       //ApexPages.addMessage(myMsg);
       return null;
       }
       Refresh();
       return null;
       }
       public pagereference clearfilters(){
           fieldName1='';
           fieldName2='';
           fieldName3='';
           fieldName4='';
           fieldName5='';
           OutName1='';
           OutName2='';
           OutName3='';
           OutName4='';
           OutName5='';
           value1=null;
           value2=null;
           value3=null;
           value4=null;
           value5=null;
           return null;
           }
       }