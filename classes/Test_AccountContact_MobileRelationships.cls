@isTest
private class Test_AccountContact_MobileRelationships {
	
	private static testmethod void dipslayContactC2A(){
		
		Map<String, SObject> contactData = createTestAccountContact();
		Map<String, SObject> masterData = createTestMasterData();
                
        Account acc2 = (Account) contactData.get('Account2');
        
        Contact con1 = (Contact) contactData.get('Contact1');
        
        ApexPages.StandardController sc = new ApexPages.StandardController(con1);
        
        ctrlExt_quickC2ARelationship createController = new ctrlExt_quickC2ARelationship(sc);
        createController.relationship.Affiliation_To_Account__c = acc2.Id;
        
        Business_Unit__c bu = (Business_Unit__c) masterData.get('BusinessUnit1');
        Therapy__c therapy = (Therapy__c) masterData.get('Therapy1');
        
        createController.relationship.Affiliation_Type__c = 'Referring with details';
        createController.onRelTypeChange();
        
        createController.relationship.Business_Unit__c = bu.Id;
        createController.relationship.Therapy__c = therapy.Id;
                
        createController.save();  
        
        System.assert(createController.isSaveError == false);   
                
        ctrlExt_AccountC2ARelationshipsMobile viewController = new ctrlExt_AccountC2ARelationshipsMobile(sc);      
	}
	
	private static testmethod void dipslayContactC2A_Error(){
		
		Map<String, SObject> contactData = createTestAccountContact();
		Map<String, SObject> masterData = createTestMasterData();
                
        Account acc2 = (Account) contactData.get('Account2');        
        Contact con1 = (Contact) contactData.get('Contact1');
        
        ApexPages.StandardController sc = new ApexPages.StandardController(con1);
        
        ctrlExt_quickC2ARelationship createController = new ctrlExt_quickC2ARelationship(sc);
        createController.relationship.Affiliation_From_Contact__c = null;
        createController.relationship.Affiliation_Start_Date__c = null;
        
        createController.save();
        System.assert(createController.isSaveError == true);
                        
        createController.relationship.Affiliation_Type__c = 'Referring with details';
                
        createController.save();       
        System.assert(createController.isSaveError == true);             
	}
	
	private static testmethod void dipslayAccountA2C(){
		
		Map<String, SObject> contactData = createTestAccountContact();
		Map<String, SObject> masterData = createTestMasterData();
                
        Account acc2 = (Account) contactData.get('Account2');        
        Contact con1 = (Contact) contactData.get('Contact1');
        
        ApexPages.StandardController sc = new ApexPages.StandardController(acc2);
        
        ctrlExt_quickC2ARelationship createController = new ctrlExt_quickC2ARelationship(sc);
        createController.relationship.Affiliation_From_Contact__c = con1.Id;
                       
        createController.relationship.Affiliation_Type__c = 'Referring';
        createController.onRelTypeChange();
                                
        createController.save();  
        
        System.assert(createController.isSaveError == false);   
                
        ctrlExt_AccountC2ARelationshipsMobile viewController = new ctrlExt_AccountC2ARelationshipsMobile(sc);      
	}
			
	private static testmethod void dipslayContactC2C(){
		
		Map<String, SObject> contactData = createTestAccountContact();
		Map<String, SObject> masterData = createTestMasterData();
                
        Account acc1 = (Account) contactData.get('Account1');
        Account acc2 = (Account) contactData.get('Account2');
          
        Contact con1 = (Contact) contactData.get('Contact1');
        Contact con2 = (Contact) contactData.get('Contact2');
        
        ApexPages.StandardController sc = new ApexPages.StandardController(con1);
        
        ctrlExt_quickC2CRelationship createController = new ctrlExt_quickC2CRelationship(sc);
        createController.relationship.Affiliation_From_Account__c = acc1.Id;
        
        createController.relationship.Affiliation_To_Contact__c = con2.Id;
        createController.loadToAccountNames();
        
        createController.relationship.Affiliation_To_Account__c = acc2.Id;
                       
        createController.relationship.Affiliation_Type__c = 'Referring';
        
        Business_Unit__c bu = (Business_Unit__c) masterData.get('BusinessUnit1');
        Therapy__c therapy = (Therapy__c) masterData.get('Therapy1');
        
        createController.relationship.Business_Unit__c = bu.Id;
        createController.relationship.Therapy__c = therapy.Id;
                                        
        createController.save();  
        
        System.assert(createController.isSaveError == false);   
        
        sc = new ApexPages.StandardController(con2);        
        ctrlExt_ContactC2CRelationshipsMobile viewController = new ctrlExt_ContactC2CRelationshipsMobile(sc);      
	}
	
	private static testmethod void dipslayContactC2C_Error(){
		
		Map<String, SObject> contactData = createTestAccountContact();
		Map<String, SObject> masterData = createTestMasterData();
                
        Account acc1 = (Account) contactData.get('Account1');
        Account acc2 = (Account) contactData.get('Account2');
          
        Contact con1 = (Contact) contactData.get('Contact1');
        Contact con2 = (Contact) contactData.get('Contact2');
        
        ApexPages.StandardController sc = new ApexPages.StandardController(con1);
        
        ctrlExt_quickC2CRelationship createController = new ctrlExt_quickC2CRelationship(sc);
        createController.relationship.Affiliation_From_Contact__c = null;
        createController.relationship.Affiliation_From_Account__c = null;
        createController.relationship.Affiliation_Start_Date__c = null;
        createController.relationship.Business_Unit__c = null;
                                                
        createController.save();          
        System.assert(createController.isSaveError == true);
        
        createController.relationship.Affiliation_From_Contact__c = con1.Id;
        createController.relationship.Affiliation_To_Contact__c = con1.Id;
        
        createController.save();          
        System.assert(createController.isSaveError == true);      
	}
	
	private static Map<String, SObject> createTestAccountContact(){
		
		Map<String, SObject> result = new Map<String, SObject>();
		
		//Insert Account
        List<Account> lstAccount = new List<Account>();
        Account oAccount1 = new Account();
            oAccount1.Name = 'Test_triggerAffiliationBeforeUpsert Test Coverage Account 1 ' ; 
            oAccount1.SAP_ID__c = '0004567898';
            oAccount1.Type = 'Test Type';
            oAccount1.Account_Country_vs__c = 'BELGIUM';
        lstAccount.Add(oAccount1);
        result.put('Account1', oAccount1);
        Account oAccount2 = new Account();
            oAccount2.Name = 'Test_triggerAffiliationBeforeUpsert Test Coverage Account 21 ' ; 
            oAccount2.SAP_ID__c = '0003457896';
            oAccount2.Type = 'Test Type';
            oAccount2.Account_Country_vs__c = 'BELGIUM';
        lstAccount.Add(oAccount2);
        result.put('Account2', oAccount2);
        insert lstAccount ;

        //Insert Contact
        List<Contact> lstContact = new List<Contact>();
        Contact oContact1 = new Contact();
            oContact1.LastName = 'Test_triggerAffiliationBeforeUpsert cont 1 ';
			oContact1.FirstName = 'test';
            oContact1.AccountId = oAccount1.Id ; 
            oContact1.Contact_Department__c = 'Diabetes Adult'; 
            oContact1.Contact_Primary_Specialty__c = 'ENT';
            oContact1.Affiliation_To_Account__c = 'Employee';
            oContact1.Primary_Job_Title_vs__c = 'Manager';
            oContact1.Contact_Gender__c = 'Male';         
        lstContact.add(oContact1) ;
        result.put('Contact1', oContact1);
        Contact oContact2 = new Contact();
            oContact2.LastName = 'Test_triggerAffiliationBeforeUpsert cont 2 ';
			oContact2.FirstName = 'test';
            oContact2.AccountId = oAccount2.Id ; 
            oContact2.Contact_Department__c = 'Diabetes Adult'; 
            oContact2.Contact_Primary_Specialty__c = 'ENT';
            oContact2.Affiliation_To_Account__c = 'Employee';
            oContact2.Primary_Job_Title_vs__c = 'Manager';
            oContact2.Contact_Gender__c = 'Male';         
        lstContact.add(oContact2) ;
        result.put('Contact2', oContact2);
        insert lstContact;
		
		return result;
	}
	
	private static Map<String, SObject> createTestMasterData(){
		
		Map<String, SObject> result = new Map<String, SObject>();
				
		//Insert Company
        Company__c oCompany = new Company__c();
            oCompany.name='TestMedtronic';
            oCompany.CurrencyIsoCode = 'EUR';
            oCompany.Current_day_in_Q1__c=56;
            oCompany.Current_day_in_Q2__c=34; 
            oCompany.Current_day_in_Q3__c=5; 
            oCompany.Current_day_in_Q4__c= 0;   
            oCompany.Current_day_in_year__c =200;
            oCompany.Days_in_Q1__c=56;  
            oCompany.Days_in_Q2__c=34;
            oCompany.Days_in_Q3__c=13;
            oCompany.Days_in_Q4__c=22;
            oCompany.Days_in_year__c =250;
            oCompany.Company_Code_Text__c = 'T16';
        insert oCompany;
		result.put('Company', oCompany);
		
		Business_Unit_Group__c bug = new Business_Unit_Group__c();
			bug.Name = 'BUGMedtronic1';
		insert bug;
		
       	Business_Unit__c bu =  new Business_Unit__c();
        	bu.Company__c =oCompany.id;
        	bu.name='BUMedtronic1';
        	bu.Business_Unit_Group__c = bug.Id;        	      
        insert bu;
		result.put('BusinessUnit1', bu);
		
        //Insert SBU
        List<Sub_Business_Units__c> lstSBU = new List<Sub_Business_Units__c>();
        Sub_Business_Units__c oSBU1 = new Sub_Business_Units__c();
            oSBU1.name='SBUMedtronic1';
            oSBU1.Business_Unit__c = bu.id;
            oSBU1.Account_Plan_Default__c = 'Revenue';
            oSBU1.Account_Flag__c = 'AF_Solutions__c';
            oSBU1.Contact_Flag__c = 'Coro_PV__c';
        lstSBU.add(oSBU1);
		result.put('SubBusinessUnit1', oSBU1);
		
        Sub_Business_Units__c oSBU2 = new Sub_Business_Units__c();
            oSBU2.name='SBUMedtronic2';
            oSBU2.Business_Unit__c = bu.id;
            oSBU2.Account_Plan_Default__c='Units';
            oSBU2.Account_Flag__c = 'AF_Solutions__c';
            oSBU2.Contact_Flag__c = 'Coro_PV__c';
        lstSBU.add(oSBU2);
        insert lstSBU;
        result.put('SubBusinessUnit2', oSBU2);

        //Insert Therapy Group
        Therapy_Group__c oTherapyGroup = new Therapy_Group__c();
            oTherapyGroup.Name = 'Therapy Group';
            oTherapyGroup.Sub_Business_Unit__c = oSBU2.Id ;
            oTherapyGroup.Company__c = oCompany.id;      
        insert oTherapyGroup;

        //Insert Therapy        
        Therapy__c oTherapy1 = new Therapy__c();
            oTherapy1.Name='Therapy 1';
            oTherapy1.Therapy_Group__c = oTherapyGroup.id;
            oTherapy1.Business_Unit__c = bu.id;
            oTherapy1.Sub_Business_Unit__c = oSBU1.id;
            oTherapy1.Therapy_Name_Hidden__c = 'Therapy';        
        result.put('Therapy1', oTherapy1);                 
        insert oTherapy1;
        
        //User SBUs
        List<User_Business_Unit__c> lstUserSBUs = new List<User_Business_Unit__c>();
        User_Business_Unit__c uBU1 = new User_Business_Unit__c();
	        uBU1.User__c = UserInfo.getUserId();
	        uBU1.Sub_Business_Unit__c = oSBU1.Id;
	        uBU1.Primary__c = true;        
        lstUserSBUs.add(uBU1);
        
        User_Business_Unit__c uBU2 = new User_Business_Unit__c();
	        uBU2.User__c = UserInfo.getUserId();
	        uBU2.Sub_Business_Unit__c = oSBU2.Id;
	        uBU2.Primary__c = false;        
        lstUserSBUs.add(uBU2);
        
        insert lstUserSBUs;
        
        //Country
        DIB_Country__c country = new DIB_Country__c();
		country.Company__c = oCompany.Id;
		country.Name = 'BELGIUM';
		country.Country_ISO_Code__c = 'BE';
		country.CurrencyIsoCode = 'EUR';
		insert country;
		result.put('Country', country);
		
        Account_Type__c acctType = new Account_Type__c();
        acctType.Account_Type__c = 'Unit Test Type';
        acctType.Account_Type_Level__c = '1';
        acctType.Hierarchy_Type__c = 'Standard';
        acctType.Identify_Field__c = 'Type';
        acctType.Identify_Operator__c = 'Equals';
        acctType.Identify_Value__c = 'Test Type';
        insert acctType;
        
        Account_Type_Country__c acctTypeCountry = new Account_Type_Country__c();
        acctTypeCountry.Account_Type__c = acctType.Id;
        acctTypeCountry.Country__c = country.Id;
        insert acctTypeCountry;
        
        List<Relationship_Type__c> lstRelTypes = new List<Relationship_Type__c>();
        
        Relationship_Type__c relType = new Relationship_Type__c();
        relType.Child_Account_Type__c = acctType.Id;
        relType.Parent_Account_Type__c = acctType.Id;
        relType.Relationship_Type__c = 'Test Relationship';
        lstRelTypes.add(relType);
        
        Relationship_Type__c relType2 = new Relationship_Type__c();
        relType2.Child_Account_Type__c = acctType.Id;
        relType2.Parent_Account_Type__c = acctType.Id;
        relType2.Relationship_Type__c = 'Test Relationship 2';
        lstRelTypes.add(relType2);
        
        Relationship_Type__c relType3 = new Relationship_Type__c();
        relType3.Child_Account_Type__c = acctType.Id;
        relType3.Parent_Account_Type__c = acctType.Id;
        relType3.Relationship_Type__c = 'Test Relationship 3';
        lstRelTypes.add(relType3);
        
        Relationship_Type__c relType4 = new Relationship_Type__c();
        relType4.Child_Account_Type__c = acctType.Id;
        relType4.Parent_Account_Type__c = acctType.Id;
        relType4.Relationship_Type__c = 'Test Relationship 4';
        lstRelTypes.add(relType4);
        
        insert lstRelTypes;
        
        List<Relationship_Type_Country__c> lstRelTypeCountries = new List<Relationship_Type_Country__c>();
        
        Relationship_Type_Country__c relTypeCountry = new Relationship_Type_Country__c();
        relTypeCountry.Country__c = country.Id;
        relTypeCountry.Master_Data_Level__c = 'Business Unit';
        relTypeCountry.Relationship_Type__c = relType.Id;
        lstRelTypeCountries.add(relTypeCountry);
        
        Relationship_Type_Country__c relTypeCountry2 = new Relationship_Type_Country__c();
        relTypeCountry2.Country__c = country.Id;
        relTypeCountry2.Master_Data_Level__c = 'Business Unit Group';
        relTypeCountry2.Relationship_Type__c = relType2.Id;
        lstRelTypeCountries.add(relTypeCountry2);
        
        Relationship_Type_Country__c relTypeCountry3 = new Relationship_Type_Country__c();
        relTypeCountry3.Country__c = country.Id;
        relTypeCountry3.Master_Data_Level__c = 'Sub Business Unit';
        relTypeCountry3.Relationship_Type__c = relType3.Id;
        lstRelTypeCountries.add(relTypeCountry3);
        
        insert lstRelTypeCountries;
                        
        return result;
	}
}