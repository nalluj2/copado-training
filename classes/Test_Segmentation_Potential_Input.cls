@isTest
private class Test_Segmentation_Potential_Input {
	
	@TestSetup
    private static void createMasterData(){
    	
    	Company__c eur = new Company__c();		
        eur.Name = 'Europe';
        eur.CurrencyIsoCode = 'EUR';
        eur.Current_day_in_Q1__c = 56;
        eur.Current_day_in_Q2__c = 34; 
        eur.Current_day_in_Q3__c = 5; 
        eur.Current_day_in_Q4__c =  0;   
        eur.Current_day_in_year__c = 200;
        eur.Days_in_Q1__c = 56;  
        eur.Days_in_Q2__c = 34;
        eur.Days_in_Q3__c = 13;
        eur.Days_in_Q4__c = 22;
        eur.Days_in_year__c = 250;
        eur.Company_Code_Text__c = 'EUR';
		insert eur;
		
		Business_Unit_Group__c bug =  new Business_Unit_Group__c();
        bug.Master_Data__c = eur.id;
        bug.Abbreviated_Name__c = 'CVG';
        bug.Name = 'CVG';	                
        insert bug;
		
		Business_Unit__c bu =  new Business_Unit__c();
        bu.Company__c = eur.id;
        bu.Name = 'Vascular';
        bu.Business_Unit_Group__c = bug.Id;
        bu.Account_Plan_Activities__c = true;        
        insert bu;
        
        Sub_Business_Units__c sbuPV = new Sub_Business_Units__c();
        sbuPV.Name = 'Coro + PV';
        sbuPV.Business_Unit__c = bu.id;
        
        Sub_Business_Units__c sbuAortic = new Sub_Business_Units__c();
        sbuAortic.Name = 'Aortic';
        sbuAortic.Business_Unit__c = bu.id;
        
		insert new List<Sub_Business_Units__c>{sbuPV, sbuAortic};
    	
    	UMD_Purpose__c currentPotential = new UMD_Purpose__c();
    	currentPotential.Fiscal_Year_VS__c = 'FY2021';
    	currentPotential.Segmentation_Qualification__c = 'Potential';
    	currentPotential.Status__c = 'Open for input';
    	currentPotential.Name = 'Potential FY2021';
    	currentPotential.UMD_Purpose_Description__c = 'Potential FY2021';
    	    	
    	UMD_Purpose__c lastPotential = new UMD_Purpose__c();
    	lastPotential.Fiscal_Year_VS__c = 'FY2020';
    	lastPotential.Segmentation_Qualification__c = 'Potential';
    	lastPotential.Status__c = 'Completed';
    	lastPotential.Name = 'Potential FY2020';
    	lastPotential.UMD_Purpose_Description__c = 'Potential FY2021';
    	
    	insert new List<UMD_Purpose__c>{currentPotential, lastPotential};
    	
    	Segmentation_Potential_Procedure__c proc1 = new Segmentation_Potential_Procedure__c();
    	proc1.Name = 'Procedure 1';
    	proc1.Active__c = true;
    	proc1.Registration_Unit__c = 'UNITS';
    	proc1.Sub_Business_Unit__c = 'Coro + PV';
    	
    	Segmentation_Potential_Procedure__c proc2 = new Segmentation_Potential_Procedure__c();
    	proc2.Name = 'Procedure 2';
    	proc2.Active__c = true;
    	proc2.Registration_Unit__c = 'REVENUE';
    	proc2.Sub_Business_Unit__c = 'Aortic';
    	
    	insert new List<Segmentation_Potential_Procedure__c>{proc1, proc2};
    	
    	List<Schema.PicklistEntry> competitorPicklistValues = Segmentation_Potential_Proc_Competitor__c.Competitor_picklist__c.getDescribe().getPicklistValues();
    	
    	Segmentation_Potential_Proc_Competitor__c proc1Comp1 = new Segmentation_Potential_Proc_Competitor__c();    	
    	proc1Comp1.Segmentation_Potential_Procedure__c = proc1.Id;
    	proc1Comp1.Competitor_picklist__c = competitorPicklistValues[0].getValue();
    	    	 	
    	Segmentation_Potential_Proc_Competitor__c proc1Comp2 = new Segmentation_Potential_Proc_Competitor__c();    	
    	proc1Comp2.Segmentation_Potential_Procedure__c = proc1.Id;
    	proc1Comp2.Competitor_picklist__c = competitorPicklistValues[1].getValue();
    	
    	insert new List<Segmentation_Potential_Proc_Competitor__c>{proc1Comp1, proc1Comp2};   
    }    
    
    private static testMethod void testTriggers(){
    	
    	UMD_Purpose__c currentPurpose = [Select Id from UMD_Purpose__c where Name = 'Potential FY2021'];
    	Segmentation_Potential_Procedure__c proc1 = [Select Id from Segmentation_Potential_Procedure__c where Name = 'Procedure 1'];
    	
    	Account acc = new Account();
    	acc.Name = 'Test Account';
    	acc.SAP_Id__c = '121231234';
    	acc.BillingCity = 'Paris';   
    	insert acc;
    	
    	Test.startTest();
    	
    	Customer_Segmentation__c accSegmentSBU = new Customer_Segmentation__c();
    	accSegmentSBU.Account__c = acc.Id;
    	accSegmentSBU.Sub_Business_Unit__c = 'Coro + PV';
    	accSegmentSBU.Size_Segment__c = 'XL';
    	accSegmentSBU.Fiscal_Year__c = 'FY2020';
    	insert accSegmentSBU;
    	    	    	
    	Segmentation_Potential_Input__c potInput = new Segmentation_Potential_Input__c();
    	potInput.Account__c = acc.Id;
    	potInput.Assigned_To__c = UserInfo.getUserId();
    	potInput.Approver_1__c = [Select Id from User where isActive = true and Profile.Name = 'System Administrator' AND Id != :UserInfo.getUserId() LIMIT 1 ].Id;
    	potInput.Last_12_mth_Sales__c = 100000;
    	potInput.Segmentation_Potential_Procedure__c = proc1.Id;
    	potInput.UMD_Purpose__c = currentPurpose.Id;    	
    	insert potInput;
    	
    	potInput = [Select Id, Current_Segmentation__c, Segmentation_Key__c from Segmentation_Potential_Input__c where Id = :potInput.Id];    	
    	System.assert(potInput.Current_Segmentation__c == accSegmentSBU.Id);
    	
    	List<Segmentation_Potential_Input__Share> shares = [Select Id from Segmentation_Potential_Input__Share where ParentId = :potInput.Id AND RowCause = 'Assigned_Approver__c'];
    	System.assert(shares.size() == 2);
    	
    	List<Segmentation_Potential_Proc_Competitor__c> competitors = [Select Id from Segmentation_Potential_Proc_Competitor__c where Segmentation_Potential_Procedure__c = :proc1.Id]; 
    	    	
    	Segmentation_Potential_Input_Competitor__c potInputComp1 = new Segmentation_Potential_Input_Competitor__c();
    	potInputComp1.Potential__c = 10000;
    	potInputComp1.Segmentation_Potential_Input__c = potInput.Id;
    	potInputComp1.Segmentation_Potential_Proc_Competitor__c = competitors[0].Id;
    	
    	Segmentation_Potential_Input_Competitor__c potInputComp2 = new Segmentation_Potential_Input_Competitor__c();
    	potInputComp2.Potential__c = 5000;
    	potInputComp2.Segmentation_Potential_Input__c = potInput.Id;
    	potInputComp2.Segmentation_Potential_Proc_Competitor__c = competitors[1].Id;
    	
    	insert new List<Segmentation_Potential_Input_Competitor__c>{potInputComp1, potInputComp2};
    	
    	Segmentation_Potential_Procedure__c proc2 = [Select Id from Segmentation_Potential_Procedure__c where Name = 'Procedure 2'];
    	
    	potInput.Approver_1__c = null;
    	potInput.Segmentation_Potential_Procedure__c = proc2.Id;
    	potInput.Sub_Business_Unit__c = 'Aortic';
    	update potInput;
    	
    	potInput = [Select Id, Current_Segmentation__c from Segmentation_Potential_Input__c where Id = :potInput.Id];
    	System.assert(potInput.Current_Segmentation__c == null);
    	
    	shares = [Select Id from Segmentation_Potential_Input__Share where ParentId = :potInput.Id AND RowCause = 'Assigned_Approver__c'];
    	System.assert(shares.size() == 1);
    	
    	accSegmentSBU.Sub_Business_Unit__c = 'Aortic';
    	update accSegmentSBU;
    	
    	potInput = [Select Id, Current_Segmentation__c, Segmentation_Key__c from Segmentation_Potential_Input__c where Id = :potInput.Id];    	
    	System.assert(potInput.Current_Segmentation__c == accSegmentSBU.Id);
    }
    
    private static testMethod void testScreenController(){
    	
    	createTestData();
    	
    	List<ctrl_Account_Segmentation_Potential.PotentialProcedureInput> result = ctrl_Account_Segmentation_Potential.getUserPotentialInputs('Input');
    	System.assert(result.size() == 1);    	
    	System.assert(result[0].competitors.size() == 2);
    	System.assert(result[0].prevRecord != null);
    	System.assert(result[0].account.accountSegment == 'L');
    	
    	Segmentation_Potential_Input__c record = result[0].record;
    	record.Potential_Medtronic__c = 115;
    	record.Potential__c = 230;    	
    	result[0].changed = true;
    	
    	result[0].competitors[0].record.Potential__c = 60;
    	result[0].competitors[0].changed = true;
    	result[0].competitors[1].record.Potential__c = 55;
    	result[0].competitors[1].changed = true;
    	
    	ctrl_Account_Segmentation_Potential.savePotentials(result);
    	
    	List<Segmentation_Potential_Input_Competitor__c> inputCompetitors = [Select Id from Segmentation_Potential_Input_Competitor__c where Segmentation_Potential_Input__c = :result[0].record.Id];
    	System.assert(inputCompetitors.size() == 2);
    	
    	result = ctrl_Account_Segmentation_Potential.getUserPotentialInputs('Input');
    	
    	record = result[0].record;    	
    	record.Potential__c = 170;
    	record.Complete__c = true;    	
    	result[0].changed = true;
    	
    	result[0].competitors[0].record.Potential__c = null;
    	result[0].competitors[0].changed = true;
    	
    	ctrl_Account_Segmentation_Potential.savePotentials(result);
    	
    	inputCompetitors = [Select Id from Segmentation_Potential_Input_Competitor__c where Segmentation_Potential_Input__c = :result[0].record.Id];
    	System.assert(inputCompetitors.size() == 1);
    	
    	result = ctrl_Account_Segmentation_Potential.getUserPotentialInputs('Approval');
    	System.assert(result.size() == 0);    	
    	
    }
    
    private static testMethod void testScreenControllerAdmin(){
    	
    	createTestData();
    	
    	Segmentation_Potential_Procedure__c proc1 = [Select Id from Segmentation_Potential_Procedure__c where Name = 'Procedure 1'];
    	Account acc = [Select Id, Name, BillingCountry from Account];
    	
    	Account_Segmentation_Validation__c accSegmentValSBU = new Account_Segmentation_Validation__c();
    	accSegmentValSBU.Account__c = acc.Id;
    	accSegmentValSBU.Sub_Business_Unit__c = 'Coro + PV';
    	accSegmentValSBU.Size_Segment__c = 'XL';
    	accSegmentValSBU.Fiscal_Year__c = 'FY2021';
    	accSegmentValSBU.Segmentation_Level__c = 'Sub Business Unit';
    	insert accSegmentValSBU;
    	
    	Test.startTest();
    	
    	Map<String, List<ctrl_Account_Performance_Questions.PicklistValue>> filters = ctrl_AccountSegmentationPotential_Admin.getFilterOptions(null, null, null, null, null, null);
    	System.assert(filters.get('fiscalYearOptions').size() == 2);
    	System.assert(filters.get('fiscalYear')[0].value == 'FY2021');
    	System.assert(filters.get('readOnlyFY')[0].value == 'FY2020');
    	System.assert(filters.get('sbuOptions').size() == 2);
    	System.assert(filters.get('procedureOptions').size() == 2);
    	System.assert(filters.get('segmentOptions').size() == 2);
    	System.assert(filters.get('countryOptions').size() == 2);
    	    	
    	filters = ctrl_AccountSegmentationPotential_Admin.getFilterOptions('FY2021', 'Coro + PV', proc1.Id, 'L', 'FRANCE', acc.Id);
    	System.assert(filters.get('fiscalYearOptions').size() == 2);
    	System.assert(filters.get('fiscalYear')[0].value == 'FY2021');
    	System.assert(filters.get('readOnlyFY')[0].value == 'FY2020');
    	System.assert(filters.get('sbuOptions').size() == 2);
    	System.assert(filters.get('procedureOptions').size() == 2);
    	System.assert(filters.get('segmentOptions').size() == 2);
    	System.assert(filters.get('countryOptions').size() == 2);
    	
    	Integer recordCount = ctrl_AccountSegmentationPotential_Admin.getRecordCount('FY2021', 'Coro + PV', proc1.Id, 'FRANCE', acc.Id, 'L', UserInfo.getUserId(), 'Open');
    	System.assert(recordCount == 1);
    	
    	List<ctrl_Account_Segmentation_Potential.PotentialProcedureInput> result = ctrl_AccountSegmentationPotential_Admin.getUserPotentialInputs('FY2021', 'Coro + PV', proc1.Id, 'FRANCE', acc.Id, 'L', UserInfo.getUserId(), 'Open');
    	System.assert(result.size() == 1);    	
    	System.assert(result[0].competitors.size() == 2);
    	System.assert(result[0].prevRecord != null);
    	System.assert(result[0].account.accountSegment == 'L');
    	
    	Segmentation_Potential_Input__c record = result[0].record;
    	record.Potential_Medtronic__c = 115;
    	record.Potential__c = 230;    	
    	result[0].changed = true;
    	
    	result[0].competitors[0].record.Potential__c = 60;
    	result[0].competitors[0].changed = true;
    	result[0].competitors[1].record.Potential__c = 55;
    	result[0].competitors[1].changed = true;
    	
    	ctrl_AccountSegmentationPotential_Admin.savePotentials(result);
    	
    	List<Segmentation_Potential_Input_Competitor__c> inputCompetitors = [Select Id from Segmentation_Potential_Input_Competitor__c where Segmentation_Potential_Input__c = :result[0].record.Id];
    	System.assert(inputCompetitors.size() == 2);
    	
    	result = ctrl_AccountSegmentationPotential_Admin.getUserPotentialInputs('FY2021', 'Coro + PV', proc1.Id, 'FRANCE', acc.Id, 'L', UserInfo.getUserId(), 'Open');
    	
    	record = result[0].record;    	
    	record.Potential__c = 170;
    	record.Complete__c = true;    	
    	result[0].changed = true;
    	
    	result[0].competitors[0].record.Potential__c = null;
    	result[0].competitors[0].changed = true;
    	
    	ctrl_AccountSegmentationPotential_Admin.savePotentials(result);
    	
    	inputCompetitors = [Select Id from Segmentation_Potential_Input_Competitor__c where Segmentation_Potential_Input__c = :result[0].record.Id];
    	System.assert(inputCompetitors.size() == 1);    	
    }
    
    private static void createTestData(){
    	
    	UMD_Purpose__c currentPurpose = [Select Id from UMD_Purpose__c where Name = 'Potential FY2021'];
    	UMD_Purpose__c lastPurpose = [Select Id from UMD_Purpose__c where Name = 'Potential FY2020'];
    	Segmentation_Potential_Procedure__c proc1 = [Select Id from Segmentation_Potential_Procedure__c where Name = 'Procedure 1'];
    	
    	Account acc = new Account();
    	acc.Name = 'Test Account';
    	acc.SAP_Id__c = '121231234';
    	acc.Account_City__c = 'Paris';
    	acc.Account_Country_vs__c = 'FRANCE';
    	insert acc;
    	
    	Customer_Segmentation__c accSegmentPV = new Customer_Segmentation__c();
    	accSegmentPV.Account__c = acc.Id;
    	accSegmentPV.Sub_Business_Unit__c = 'Coro + PV';
    	accSegmentPV.Size_Segment__c = 'L';
    	accSegmentPV.Fiscal_Year__c = 'FY2020';
    	    	
    	Customer_Segmentation__c accSegmentAortic = new Customer_Segmentation__c();
    	accSegmentAortic.Account__c = acc.Id;
    	accSegmentAortic.Sub_Business_Unit__c = 'Aortic';
    	accSegmentAortic.Size_Segment__c = 'M';
    	accSegmentAortic.Fiscal_Year__c = 'FY2020';
    	
    	insert new List<Customer_Segmentation__c>{accSegmentPV, accSegmentAortic};
    	
    	Segmentation_Potential_Input__c lastInput = new Segmentation_Potential_Input__c();
    	lastInput.Account__c = acc.Id;
    	lastInput.UMD_Purpose__c = lastPurpose.Id;
    	lastInput.Sub_Business_Unit__c = 'Coro + PV';
    	lastInput.Last_12_mth_Sales__c = 100;
    	lastInput.Segmentation_Potential_Procedure__c = proc1.Id;
    	lastInput.Potential__c = 230;
    	lastInput.Potential_Medtronic__c = 110;
    	lastInput.Assigned_To__c = UserInfo.getUserId();
    	lastInput.Approver_1__c = [Select Id from User where isActive = true and Profile.Name = 'System Administrator' AND Id != :UserInfo.getUserId() LIMIT 1].Id;
    	lastInput.Complete__c = true;
    	lastInput.Approved__c = true;
    	
    	Segmentation_Potential_Input__c currentInput = new Segmentation_Potential_Input__c();
    	currentInput.Account__c = acc.Id;
    	currentInput.UMD_Purpose__c = currentPurpose.Id;
    	currentInput.Sub_Business_Unit__c = 'Coro + PV';
    	currentInput.Segmentation_Potential_Procedure__c = proc1.Id;
    	currentInput.Last_12_mth_Sales__c = 120;
    	currentInput.Assigned_To__c = UserInfo.getUserId();
    	currentInput.Approver_1__c = lastInput.Approver_1__c;
    	    	    	    	
    	insert new List<Segmentation_Potential_Input__c>{lastInput, currentInput};
    	
    	currentInput = [Select Id, Current_Segmentation__c from Segmentation_Potential_Input__c where Id = :currentInput.Id];
    	System.assert(currentInput.Current_Segmentation__c == accSegmentPV.Id);
    	
    	List<Segmentation_Potential_Proc_Competitor__c> competitors = [Select Id from Segmentation_Potential_Proc_Competitor__c where Segmentation_Potential_Procedure__c = :proc1.Id]; 
    	
    	Segmentation_Potential_Input_Competitor__c lastInputComp1 = new Segmentation_Potential_Input_Competitor__c();
    	lastInputComp1.Potential__c = 70;
    	lastInputComp1.Segmentation_Potential_Input__c = lastInput.Id;
    	lastInputComp1.Segmentation_Potential_Proc_Competitor__c = competitors[0].Id;
    	
    	Segmentation_Potential_Input_Competitor__c lastInputComp2 = new Segmentation_Potential_Input_Competitor__c();
    	lastInputComp2.Potential__c = 50;
    	lastInputComp2.Segmentation_Potential_Input__c = lastInput.Id;
    	lastInputComp2.Segmentation_Potential_Proc_Competitor__c = competitors[1].Id;
    	
    	insert new List<Segmentation_Potential_Input_Competitor__c>{lastInputComp1, lastInputComp2};
    	
    }
}