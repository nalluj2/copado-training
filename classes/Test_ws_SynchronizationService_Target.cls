@isTest
private class Test_ws_SynchronizationService_Target {
	
	private static testMethod void testDoPost(){
		
		Synchronization_Service_Setting__c syncSettings = new Synchronization_Service_Setting__c();
		syncSettings.username__c = 'test@medtronic.com';
		syncSettings.password__c = 'password';
		syncSettings.client_id__c = '123456789';
		syncSettings.client_secret__c = 'XXXXX';
		syncSettings.is_sandbox__c = true;
		syncSettings.sync_Enabled__c = true;
		insert syncSettings;
		
		Test.startTest();
		
		Test_bl_SynchronizationService_Utils.CalloutMock mockImpl = new Test_bl_SynchronizationService_Utils.CalloutMock();
		
		Test.setMock(HttpCalloutMock.class, mockImpl);
		bl_SynchronizationService_Utils.calloutMock = mockImpl;
						
		Sync_Notification__c notification = new Sync_Notification__c();
		notification.Notification_Id__c = 'Test_Notification_Id';
		notification.Status__c = 'New';	
		notification.Type__c = 'Inbound';		
		notification.Record_Id__c = 'Test_Record_Id';
		notification.Record_Object_Type__c = 'Case';
						
		List<Sync_Notification__c> notifications = new List<Sync_Notification__c>{notification};
		
		ws_SynchronizationService_Target.SynchronizationServiceResponse response = ws_SynchronizationService_Target.doPost(notifications);
		
		System.assert(response.notifications.size() == notifications.size());
		
		notifications = [Select Id from Sync_Notification__c where Notification_Id__c = 'Test_Notification_Id'];
		
		System.assert(notifications.size() == 1);
		
		Test.stopTest();
	}
}