public with sharing class ctrlExt_Load_Opportunity_Tender_Lots {
	
	private Opportunity opp;
	public transient Blob inputData {get; set;}	
	private Map<String, Schema.DisplayType> oppLotFields;
		
	public ctrlExt_Load_Opportunity_Tender_Lots(ApexPages.StandardController sc){
		
		opp = (Opportunity) sc.getRecord();
		
		Map<String, Schema.SObjectField> describeMap = Schema.SObjectType.Opportunity_Lot__c.fields.getMap();
        
        oppLotFields = new Map<String, Schema.DisplayType>();
                        
        for(String oppLotField : describeMap.keySet()){
        	
        	Schema.DescribeFieldResult fDescribe = describeMap.get(oppLotfield).getDescribe();
        	
        	oppLotFields.put(oppLotField.toUpperCase(), fDescribe.getType());        	      	
        }
                           
        User currentUser = [Select Company_Code_Text__c from User where Id = :UserInfo.getUserId()];        
	}   
	
	public void loadLots(){ 
		
		if (inputData == null){
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'No File found'));
            return;
        }

        String inputDataString;

        try{
            
            inputDataString = inputData.toString();

        }catch (Exception e){

            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'The selected file do not have the expected format. Please make sure you are using a valid plain text csv file.'));
            return;
        }

        // Split the data by lines
        List<String> inputDataLines = inputDataString.split('\n');
        
        if(inputDataLines.size() == 0){
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'The file provided is empty'));
            return;
        }

        // Extract mapped fields from header line
        List<String> inputFields = new List<String>();
                
        for(String inputField : inputDataLines[0].trim().split(',', -1)){
        	
        	String iField = inputField.trim();
        	
        	if(oppLotFields.containsKey(iField.toUpperCase()) == false){
        		
        		ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Invalid field \'' + iField + '\' for object \'Opportunity Lot\''));
            	return;
        	}
        	
        	inputFields.add(iField);	
        }

        // Create records to upsert
        List<Opportunity_Lot__c> oppLots = new List<Opportunity_Lot__c>();
        
        for(Integer i = 1; i < inputDataLines.size(); i++){
        	
        	List<String> inputFieldData = inputDataLines[i].trim().split(',', -1);
        	
        	if(inputFieldData.size() != inputFields.size()){
        		
        		ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Line ' + i + ': Invalid file format found. Number of columns does not match the header : ' + inputDataLines[i].trim()));
            	return;
        	}
        	
        	Opportunity_Lot__c oppLot = new Opportunity_Lot__c();
        	oppLot.Opportunity__c = opp.Id;
        	
        	for(Integer j = 0; j < inputFields.size(); j ++){
        		
        		String fieldName = inputFields[j];

        		if(	fieldName.toUpperCase() != 'SUB_BUSINESS_UNIT__C' && 
        			fieldName.toUpperCase() != 'ACCOUNT__C' && 
        			fieldName.toUpperCase() != 'PRODUCT_OFFERED__C'&& 
        			fieldName.toUpperCase() != 'OPPORTUNITY_LOT_NUMBER__C'&& 
        			fieldName.toUpperCase() != 'LOT_OWNER__C'){
        			
        			Object fieldValue = convertToFieldFormat(inputFieldData[j], oppLotFields.get(fieldName.toUpperCase()));

        			oppLot.put(fieldName, fieldValue);
        			
        		}else if(fieldName.toUpperCase() == 'LOT_OWNER__C'){
        			
        			User lotOwner = new User();
        			lotOwner.Alias_Unique__c = inputFieldData[j];
        			
        			oppLot.putSObject('LOT_OWNER__R', lotOwner);
        		}
        	}
        	
        	oppLots.add(oppLot);
        }

        try{
        
        	upsert oppLots;
        
        }catch(Exception e){
        	
        	ApexPages.addMessages(e);
			System.debug('**BC** Error : ' + e.getMessage());
        	return;

        }
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Confirm, 'Opportunity Lots have been uploaded / updated successfully'));

	}
	
	private Object convertToFieldFormat(String inputFieldDataString, Schema.DisplayType fieldType){
		
		if(inputFieldDataString == null || inputFieldDataString == '') return null;
		
		inputFieldDataString = inputFieldDataString.unescapeCsv();
		
		if(fieldType == Schema.DisplayType.Double || fieldType == Schema.DisplayType.Currency || fieldType == Schema.DisplayType.DOUBLE || fieldType == Schema.DisplayType.PERCENT) return Decimal.valueOf(inputFieldDataString);
		if(fieldType == Schema.DisplayType.Boolean) return Boolean.valueOf(inputFieldDataString);
		if(fieldType == Schema.DisplayType.Id || fieldType == Schema.DisplayType.Reference) return Id.valueOf(inputFieldDataString);
		
		// If not a type different than String
		return inputFieldDataString;
	}
}