@isTest
private class Test_Support_Requests_BOT {
	
	private static testmethod void test_searchByAlias(){

		ws_adUserServiceMockImpl mockup = new ws_adUserServiceMockImpl();		
		Test.setMock(WebServiceMock.class, mockup);
		
		Test.startTest();
		
		ctrl_Support_Requests controller = new ctrl_Support_Requests();			
		System.assert(controller.userLoggedIn==true);
		
		controller.helper_Request_BOT.requestNewUser();
		
		controller.helper_Request_BOT.searchAlias = 'testAlias';
		controller.helper_Request_BOT.searchByAlias();

		Test.stopTest();

	}

	private static testmethod void test_createUser(){
		
		Test.startTest();
		
		ctrl_Support_Requests controller1 = new ctrl_Support_Requests();			
		System.assert(controller1.userLoggedIn==true);
									
		controller1.helper_Request_BOT.requestNewUser();
		
		controller1.session.request.Firstname__c = 'Test';
		controller1.session.request.Lastname__c = 'User';
		controller1.session.request.Email__c = 'test.user@medtronic.com';
		controller1.session.request.BOT_User_Role__c = 'Business Manager';
		controller1.session.request.BOT_Countries__c = 'TEST';
		controller1.session.request.BOT_MPG_Codes__c = 'TEST';
		
		Boolean bShowBOTCountries = controller1.helper_Request_BOT.bShowBOTCountries;
		Boolean bShowBOTMajorClasses = controller1.helper_Request_BOT.bShowBOTMajorClasses;
		Boolean bShowBOTMPGCodes = controller1.helper_Request_BOT.bShowBOTMPGCodes;

		controller1.helper_Request_BOT.createUserRequest();
		
		System.assert(controller1.session.request.Name != null);
		System.assert(bShowBOTCountries);
		System.assert(!bShowBOTMajorClasses);
		System.assert(bShowBOTMPGCodes);
		
		ctrl_Support_Requests controller2 = new ctrl_Support_Requests();			
		System.assert(controller2.userLoggedIn==true);

		controller2.helper_Request_BOT.requestNewUser();
		
		controller2.session.request.Firstname__c = 'Test';
		controller2.session.request.Lastname__c = 'User';
		controller2.session.request.Email__c = 'test.user@medtronic.com';
		controller2.session.request.BOT_User_Role__c = 'Product Manager';
		controller2.session.request.BOT_Countries__c = 'TEST';
		controller2.session.request.BOT_Major_Classes__c = 'TEST';

		bShowBOTCountries = controller2.helper_Request_BOT.bShowBOTCountries;
		bShowBOTMajorClasses = controller2.helper_Request_BOT.bShowBOTMajorClasses;
		bShowBOTMPGCodes = controller2.helper_Request_BOT.bShowBOTMPGCodes;
		
		controller2.helper_Request_BOT.createUserRequest();
		
		System.assert(controller2.session.request.Name != null);
		System.assert(bShowBOTCountries);
		System.assert(bShowBOTMajorClasses);
		System.assert(!bShowBOTMPGCodes);


		ctrl_Support_Requests controller3 = new ctrl_Support_Requests();			
		System.assert(controller3.userLoggedIn==true);

		controller3.helper_Request_BOT.requestNewUser();
		
		controller3.session.request.Firstname__c = 'Test';
		controller3.session.request.Lastname__c = 'User';
		controller3.session.request.Email__c = 'test.user@medtronic.com';
		controller3.session.request.BOT_User_Role__c = 'Business Ops';

		bShowBOTCountries = controller3.helper_Request_BOT.bShowBOTCountries;
		bShowBOTMajorClasses = controller3.helper_Request_BOT.bShowBOTMajorClasses;
		bShowBOTMPGCodes = controller3.helper_Request_BOT.bShowBOTMPGCodes;
		
		
		controller3.helper_Request_BOT.createUserRequest();
		
		System.assert(controller3.session.request.Name != null);
		System.assert(!bShowBOTCountries);
		System.assert(!bShowBOTMajorClasses);
		System.assert(!bShowBOTMPGCodes);

		Test.stopTest();
		
	}

	private static testmethod void test_createUser_Error(){
		
		Test.startTest();
		
		ctrl_Support_Requests controller1 = new ctrl_Support_Requests();			
		System.assert(controller1.userLoggedIn==true);
									
		controller1.helper_Request_BOT.requestNewUser();
	
		controller1.helper_Request_BOT.createUserRequest();
		System.assert(controller1.session.request.Name == null);

		controller1.session.request.Firstname__c = 'Test';
		controller1.helper_Request_BOT.createUserRequest();
		System.assert(controller1.session.request.Name == null);

		controller1.session.request.Lastname__c = 'User';
		controller1.helper_Request_BOT.createUserRequest();
		System.assert(controller1.session.request.Name == null);

		controller1.session.request.Email__c = 'test.user@medtronic.com';
		controller1.helper_Request_BOT.createUserRequest();
		System.assert(controller1.session.request.Name == null);

		controller1.session.request.BOT_User_Role__c = 'Business Manager';
		controller1.helper_Request_BOT.createUserRequest();
		System.assert(controller1.session.request.Name == null);

		controller1.session.request.BOT_Countries__c = 'TEST';
		controller1.helper_Request_BOT.createUserRequest();
		System.assert(controller1.session.request.Name == null);

		controller1.session.request.BOT_User_Role__c = 'Product Manager';
		controller1.helper_Request_BOT.createUserRequest();
		System.assert(controller1.session.request.Name == null);

		controller1.session.request.BOT_Major_Classes__c = 'TEST';
		controller1.session.request.Application__c = null;

		Boolean isError = false;
		
		try{
			controller1.helper_Request_BOT.createUserRequest();
		}catch(Exception e){
			isError = true;
		}
		
		System.assert(isError == true);	

		Test.stopTest();
		
	}	

	private static testmethod void dataExtract(){
						
		Test.startTest();
		
		ctrl_Support_Requests controller = new ctrl_Support_Requests();			
		System.assert(controller.userLoggedIn==true);
									
		controller.helper_Request_BOT.requestDataExtract();
		
		controller.session.request.Firstname__c = 'Test';
		controller.session.request.Lastname__c = 'User';
		controller.session.request.Email__c = 'test.user@medtronic.com';
		controller.session.request.BOT_File_Type__c = 'XLS';
		controller.session.request.Description_Long__c = 'TEST';

		controller.helper_Request_BOT.createDataExtractRequest();
		
		Test.stopTest();
		
		System.assert(controller.session.request.Name != null);
	
	}

	private static testmethod void dataExtractError(){
				
		Test.startTest();
		
		ctrl_Support_Requests controller = new ctrl_Support_Requests();			
								
		controller.helper_Request_BOT.requestDataExtract();
		
		// TEST 1		
		controller.helper_Request_BOT.createDataExtractRequest();
		System.assert(controller.session.request.Name == null);

		// TEST 2
		controller.session.request.BOT_File_Type__c = 'XLS';
		controller.helper_Request_BOT.createDataExtractRequest();
		System.assert(controller.session.request.Name == null);

		// TEST 3
		controller.session.request.Description_Long__c = 'TEST';
		controller.session.request.Application__c = null;

		Boolean isError = false;
		
		try{
			controller.helper_Request_BOT.createDataExtractRequest();
		}catch(Exception e){
			isError = true;
		}
		
		System.assert(isError == true);	

	}
		
	private static testmethod void requestGeneral(){
						
		Test.startTest();
		
		ctrl_Support_Requests controller = new ctrl_Support_Requests();			
		System.assert(controller.userLoggedIn==true);
									
		controller.helper_Request_BOT.requestGeneral();
		
		controller.session.request.Firstname__c = 'Test';
		controller.session.request.Lastname__c = 'User';
		controller.session.request.Email__c = 'test.user@medtronic.com';
		controller.session.request.Description_Long__c = 'TEST';

		controller.helper_Request_BOT.createGeneralRequest();
		
		Test.stopTest();
		
		System.assert(controller.session.request.Name != null);

	}

	private static testmethod void requestGeneralError(){
						
		Test.startTest();
		
		ctrl_Support_Requests controller = new ctrl_Support_Requests();			
								
		controller.helper_Request_BOT.requestGeneral();
		
		// TEST 1		
		controller.helper_Request_BOT.createGeneralRequest();
		System.assert(controller.session.request.Name == null);

		// TEST 2
		controller.session.request.Description_Long__c = 'TEST';
		controller.session.request.Application__c = null;

		Boolean isError = false;
		
		try{

			controller.helper_Request_BOT.createGeneralRequest();
		}catch(Exception e){
			isError = true;
		}
		
		System.assert(isError == true);	

	}

}