public class ctrlExt_ManageStakeholderEmployee{

	//----------------------------------------------------------------------
	// Private variables
	//----------------------------------------------------------------------
	private Opportunity oOpportunity;
	@TestVisible private Map<Integer, EmployeeRelationship> mapEmployeeRelationship;
	@TestVisible private List<Stakeholder_Employee_Relationship__c> lstEmployeeRelationship_Delete;
	private Map<Id, Stakeholder_Employee_Relationship__c> mapEmployeeRelationship_Org;
	private Stakeholder_Mapping__c oStakeholderMapping_Org;
	private Id id_Return;
	private String tStakeholderMappingId;
	//----------------------------------------------------------------------


	//----------------------------------------------------------------------
	// Constructor
	//----------------------------------------------------------------------
	public ctrlExt_ManageStakeholderEmployee(ApexPages.StandardController oStdController){	
	
		
		if (!Test.isRunningTest()){
			List<String> lstAdditionalField = new List<String>{'AccountId', 'Account.Name'};
			oStdController.addFields(lstAdditionalField);
		}
		oOpportunity = (Opportunity)oStdController.getRecord();

		tStakeholderMappingId = clsUtil.isNull(ApexPages.currentPage().getParameters().get('smid'), '');
		String tSaved = clsUtil.isNull(ApexPages.currentPage().getParameters().get('saved'), '0');

		if (tSaved == '1') ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.info, 'Records saved.'));

		tRoleHelp = '';
		tRoleHelp += 'Initiators';
		tRoleHelp += '<br />';
		tRoleHelp += 'The person who mobilizes the organization to look for a new or stay with the current solution to a problem. If this person is also the decider, choose decider as the role.';
		tRoleHelp += '<br /><br />';
		tRoleHelp += 'Gatekeepers';
		tRoleHelp += '<br />';
		tRoleHelp += 'Gatekeepers are responsible for the information provision within the decision-making unit and can limit your access or filter information to others in the decision-making unit.';
		tRoleHelp += '<br /><br />';
		tRoleHelp += 'Buyers';
		tRoleHelp += '<br />';
		tRoleHelp += 'The Buyer is the person responsible for the formal supplier relationship. The buyer negotiates about contract terms and eventually places the order. The degree of influence of this role varies per organization.';
		tRoleHelp += '<br /><br />';
		tRoleHelp += 'Influencers';
		tRoleHelp += '<br />';
		tRoleHelp += 'Influencers are others than users who may have a persuasive role in relation to the deciders. They may be specialists or peers who can exert influence on the purchasing process by making recommendations based upon experience, their knowledge or personal relationship.';
		tRoleHelp += '<br /><br />';
		tRoleHelp += 'Users';
		tRoleHelp += '<br />';
		tRoleHelp += 'These are the people who are actually going to work with the purchased goods or services and they exert influence on the specifications.';
		tRoleHelp += '<br /><br />';
		tRoleHelp += 'Deciders';
		tRoleHelp += '<br />';
		tRoleHelp += 'The decider is the person who is ultimately responsible for making the final deal or decision. This person is authorized to approve the final order.';

		if (!clsUtil.isBlank(ApexPages.currentPage().getParameters().get('returnid'))){
			id_Return = ApexPages.currentPage().getParameters().get('returnid');
		}else{
			id_Return = oOpportunity.Id;
		}

		tLabel_Cancel = 'Cancel';
		if (String.valueOf(id_Return).startsWith('006')){
			tLabel_Cancel = 'Back to Opportunity';
		}else if (String.valueOf(id_Return).startsWith('a3U')){
			tLabel_Cancel = 'Back to Stakeholder Mapping';
		}

		lstEmployeeRelationship_Delete = new List<Stakeholder_Employee_Relationship__c>();
		loadStakeholderMappingData(tStakeholderMappingId);

	}
	//----------------------------------------------------------------------


	//----------------------------------------------------------------------
	// Getters & Setters
	//----------------------------------------------------------------------
	public Stakeholder_Mapping__c oStakeholderMapping { get; set; }
	public List<EmployeeRelationship> lstEmployeeRelationship { 
		get{
			return mapEmployeeRelationship.values();
		} 
		set;
	}
	public Integer iIndex_EmployeeRelationship { get; set; }
	public String tLabel_Cancel { get; private set; }
	public String tRoleHelp { get; private set; }
	//----------------------------------------------------------------------


	//----------------------------------------------------------------------
	// Actions
	//----------------------------------------------------------------------
	public PageReference save(){

		PageReference oPageReference;

		if (bSaveData()){

			if (String.isBlank(tStakeholderMappingId)){

				oPageReference = new PageReference('/apex/ManageStakeholderEmployee?id=' + oOpportunity.Id + '&smid=' + clsUtil.isNull(oStakeholderMapping.Id, ''));
				oPageReference.setRedirect(true);
				return oPageReference;

			}else{
		
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.info, 'Records saved.'));
				return oPageReference;

			}

		}

		return oPageReference;

	}

	public PageReference saveAndNew(){

		PageReference oPageReference;

		if (bSaveData()){

			oPageReference = new PageReference('/apex/ManageStakeholderEmployee?id=' + oOpportunity.Id + '&saved=1');
			oPageReference.setRedirect(true);
			return oPageReference;

		}

		return oPageReference;

	}

	private Boolean bSaveData(){

		Boolean bSaved = true;

		List<String> lstField_StakeholderMapping = new List<String>{'Contact__c', 'Role__c', 'Influence_in_purchasing_decision__c', 'Relationship_with_Medtronic_IHS__c', 'Stakeholder_Type__c', 'Unqualified_Referral__c', 'Transition_Lite_Meeting__c'};
		List<String> lstField_EmployeeRelationship = new List<String>{'Deadline__c', 'Development_Actions__c', 'Employee__c', 'Relationship_Quality__c', 'Title__c'};

		Savepoint oSavePoint = Database.setSavepoint();

		try{

			// Validate Data
			if (oStakeholderMapping.Contact__c != oStakeholderMapping_Org.Contact__c){
			
				Contact oContact = [SELECT Id, AccountId FROM Contact WHERE Id = :oStakeholderMapping.Contact__c];

				if (oContact.AccountId != oOpportunity.AccountId){
				
					oStakeholderMapping_Org.Contact__c.addError('This Contact should be related to the Account of the Opportunity : "' + oOpportunity.Account.Name + '"');
					return false;

				}
			}


			Boolean bChanged = false;
			for (String tField : lstField_StakeholderMapping){

				bChanged = false;

				if (oStakeholderMapping.get(tField) != oStakeholderMapping_Org.get(tField)){

					if (tField == 'Role__c'){

						// Multiselect Picklist in Cloned record - sequence of values can be different
						List<String> lstValue = oStakeholderMapping.Role__c != null ? oStakeholderMapping.Role__c.split(';') : new List<String>();
						List<String> lstValue_Org = oStakeholderMapping_Org.Role__c != null ? oStakeholderMapping_Org.Role__c.split(';') : new List<String>();
						lstValue.sort();
						lstValue_Org.sort();
						if (lstValue != lstValue_Org) bChanged = true;

					}else{

						bChanged = true;

					}

					if (bChanged) break;

				}

			}

			if (bChanged){

				List<Database.UpsertResult> lstUpsertResult = Database.upsert(new List<Stakeholder_Mapping__c>{oStakeholderMapping}, false);						
				if (!lstUpsertResult[0].isSuccess()){
					oStakeholderMapping_Org.addError(lstUpsertResult[0].getErrors()[0].getMessage());
					return false;
				}

			}
			
			List<Stakeholder_Employee_Relationship__c> lstEmployeeRelationship_Upsert = new List<Stakeholder_Employee_Relationship__c>();
			List<Stakeholder_Employee_Relationship__c> lstEmployeeRelationship_Update = new List<Stakeholder_Employee_Relationship__c>();
			for (EmployeeRelationship oEmployeeRelationship : mapEmployeeRelationship.values()){
			
				if (oEmployeeRelationship.oEmployeeRelationship.Id == null){

					oEmployeeRelationship.oEmployeeRelationship.MDT_Contact__c = oStakeholderMapping.Id;
					lstEmployeeRelationship_Upsert.add(oEmployeeRelationship.oEmployeeRelationship);

				}else{

					Stakeholder_Employee_Relationship__c oEmployeeRelationship_Org = mapEmployeeRelationship_Org.get(oEmployeeRelationship.oEmployeeRelationship.Id);
					for (String tField : lstField_EmployeeRelationship){

						if (oEmployeeRelationship.oEmployeeRelationship.get(tField) != oEmployeeRelationship_Org.get(tField)){

							lstEmployeeRelationship_Upsert.add(oEmployeeRelationship.oEmployeeRelationship);
							break;

						}

					}

				}

			}

			if (lstEmployeeRelationship_Delete.size() > 0) delete lstEmployeeRelationship_Delete;
			if (lstEmployeeRelationship_Upsert.size() > 0) upsert lstEmployeeRelationship_Upsert;

		}catch(Exception oEX){

			bSaved = false;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, oEX.getMessage()));
			Database.rollback(oSavePoint);

		}

		return bSaved;

	}

	public PageReference cancel(){
		
		PageReference oPageReference = new PageReference('/' + id_Return);
			oPageReference.setRedirect(true);
		return oPageReference;

	}

	public void deleteSoft(){
	
		if (mapEmployeeRelationship.containsKey(iIndex_EmployeeRelationship)){
			
			EmployeeRelationship oEmployeeRelationship = mapEmployeeRelationship.get(iIndex_EmployeeRelationship);
			if (oEmployeeRelationship.oEmployeeRelationship.Id != null){
				lstEmployeeRelationship_Delete.add(oEmployeeRelationship.oEmployeeRelationship);
			}
			mapEmployeeRelationship.remove(iIndex_EmployeeRelationship);

		}

	}

	public void addEmployeeRelationship(){
	
		Integer iIndex_New = mapEmployeeRelationship.size() + 1;
		EmployeeRelationship oEmployeeRelationship_TMP = new EmployeeRelationship(new Stakeholder_Employee_Relationship__c(), iIndex_New);
		mapEmployeeRelationship.put(iIndex_New, oEmployeeRelationship_TMP);

	}
	//----------------------------------------------------------------------


	//----------------------------------------------------------------------
	// Private Methods
	//----------------------------------------------------------------------
	private void loadStakeholderMappingData(String tStakeholderMappingId){
		
		if (String.isBlank(tStakeholderMappingId)){
		
			oStakeholderMapping = new Stakeholder_Mapping__c();
			oStakeholderMapping.Opportunity__c = oOpportunity.Id;
			oStakeholderMapping.RecordTypeId = clsUtil.getRecordTypeByDevName('Stakeholder_Mapping__c', 'EMEA_IHS_Stakeholder_Mapping').Id;
			oStakeholderMapping_Org = new Stakeholder_Mapping__c();

			mapEmployeeRelationship = new Map<Integer, EmployeeRelationship>();
			mapEmployeeRelationship_Org = new Map<Id, Stakeholder_Employee_Relationship__c>();

		}else{

			// We use 2 seperated SOQL to minimize the Viewstate because we need to store the original values of the Stakeholder_Mapping__c and the values of all related Stakeholder_Employee_Relationship__c records using a CLONE
			oStakeholderMapping = 
				[
					SELECT 
						Id, Opportunity__c, Opportunity__r.Name, Contact__c, Role__c, Influence_in_purchasing_decision__c, Relationship_with_Medtronic_IHS__c, Stakeholder_Type__c, Unqualified_Referral__c, Transition_Lite_Meeting__c
					FROM 
						Stakeholder_Mapping__c
					WHERE 
						Id = :tStakeholderMappingId
				];
		
			// Store the original Stakeholder_Mapping__c so that we can validate if the record is changed
			oStakeholderMapping_Org = oStakeholderMapping.clone(false, false, true, true);


			List<Stakeholder_Employee_Relationship__c> lstEmployeeRelationship_TMP = 
				[
					SELECT 
						Id, Deadline__c, Development_Actions__c, Employee__c, Relationship_Quality__c, Title__c 
					FROM 
						Stakeholder_Employee_Relationship__c
					WHERE
						MDT_Contact__c = :tStakeholderMappingId
				];

			mapEmployeeRelationship = new Map<Integer, EmployeeRelationship>();
			mapEmployeeRelationship_Org = new Map<Id, Stakeholder_Employee_Relationship__c>();
			for (Stakeholder_Employee_Relationship__c oEmployeeRelationship : lstEmployeeRelationship_TMP){
				Integer iIndex = mapEmployeeRelationship.size();
				EmployeeRelationship oEmployeeRelationship_TMP = new EmployeeRelationship(oEmployeeRelationship, iIndex);
				mapEmployeeRelationship.put(iIndex, oEmployeeRelationship_TMP);

				// Store the original Stakeholder_Employee_Relationship__c so that we can validate if the record is changed
				mapEmployeeRelationship_Org.put(oEmployeeRelationship.Id, oEmployeeRelationship.clone(false, false, true, true));
			}

		}

	}
	//----------------------------------------------------------------------


	//----------------------------------------------------------------------
	// Helper Class
	//----------------------------------------------------------------------
	public class EmployeeRelationship{
	
		public EmployeeRelationship(Stakeholder_Employee_Relationship__c oData, Integer aiIndex){
		
			iIndex = aiIndex;
			idEmployeeRelationship = null;
			bIsChanged = false;
			bEdit = false;
			bIsDeleted = false;
			tLabel_EditSave = 'Edit';
			oEmployeeRelationship = oData;


		}

		public Integer iIndex { get; set; }
		public Id idEmployeeRelationship { get; set; }
		public Boolean bIsChanged { get; set; }
		public Boolean bIsDeleted { get; set; }
		public Boolean bEdit { get; set; }
		public String tLabel_EditSave { get; set; }
		public Stakeholder_Employee_Relationship__c oEmployeeRelationship { get; set; }

	}
	//----------------------------------------------------------------------

}