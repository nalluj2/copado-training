@isTest
private class Test_ba_TestEquipment_CalibartionStatus {
    
    private static testmethod void updateCalibrationStatus(){
    	
    	SVMX_Test_Equipment__c currentTestEq = new SVMX_Test_Equipment__c();
    	currentTestEq.SVMX_Calibration_Due_Date__c = Date.Today().addDays(10);
    	currentTestEq.SVMX_Calibration_Status__c = 'In Calibration';
    	
    	SVMX_Test_Equipment__c lastDayTestEq = new SVMX_Test_Equipment__c();
    	lastDayTestEq.SVMX_Calibration_Due_Date__c = Date.Today();
    	lastDayTestEq.SVMX_Calibration_Status__c = 'In Calibration';
    	
    	SVMX_Test_Equipment__c expiredTestEq = new SVMX_Test_Equipment__c();
    	expiredTestEq.SVMX_Calibration_Due_Date__c = Date.Today().addDays(-1);
    	expiredTestEq.SVMX_Calibration_Status__c = 'In Calibration';
    	
    	insert new List<SVMX_Test_Equipment__c>{currentTestEq, lastDayTestEq, expiredTestEq};
    	
    	Test.startTest();
    	
    	Database.executeBatch( new ba_TestEquipment_CalibrationStatus());

    	Test.stopTest();
    	
    	currentTestEq = [Select SVMX_Calibration_Status__c from SVMX_Test_Equipment__c where Id = :currentTestEq.Id];
    	System.assert (currentTestEq.SVMX_Calibration_Status__c == 'In Calibration');
    	
    	lastDayTestEq = [Select SVMX_Calibration_Status__c from SVMX_Test_Equipment__c where Id = :lastDayTestEq.Id];
    	System.assert (lastDayTestEq.SVMX_Calibration_Status__c == 'In Calibration');
    	
    	expiredTestEq = [Select SVMX_Calibration_Status__c from SVMX_Test_Equipment__c where Id = :expiredTestEq.Id];
    	System.assert (expiredTestEq.SVMX_Calibration_Status__c == 'Out of Calibration');    	
    }
}