/*  
    Test Class Name - Test_controllerImplantCloneWithDetails
    Description  –  The written class will cover controllerImplantCloneWithDetails 
    Author - JaKe MDT
    Created Date  - 21/07/2011 
*/

@isTest
Private class Test_controllerImplantCloneWithDetails 
{    
static testMethod void myUnitTest() 
{
    Account acc1 = new Account();
    acc1.Name = 'A1';
    insert acc1;
  
    Contact cnt1 = new Contact();
    cnt1.LastName = 'tester1';
    cnt1.FirstName = 'TEST';
    cnt1.AccountId = acc1.Id;
    cnt1.Contact_Department__c = 'Diabetes Adult'; 
    cnt1.Contact_Primary_Specialty__c = 'ENT';
    cnt1.Affiliation_To_Account__c = 'Employee';
    cnt1.Primary_Job_Title_vs__c = 'Manager';
    cnt1.Contact_Gender__c = 'Male';    
    insert cnt1;

    //Insert Company
     Company__c cmpny = new Company__c();
     cmpny.name='TestMedtronic';
     cmpny.CurrencyIsoCode = 'EUR';
     cmpny.Current_day_in_Q1__c=56;
     cmpny.Current_day_in_Q2__c=34; 
     cmpny.Current_day_in_Q3__c=5; 
     cmpny.Current_day_in_Q4__c= 0;   
     cmpny.Current_day_in_year__c =200;
     cmpny.Days_in_Q1__c=56;  
     cmpny.Days_in_Q2__c=34;
     cmpny.Days_in_Q3__c=13;
     cmpny.Days_in_Q4__c=22;
     cmpny.Days_in_year__c =250;
     cmpny.Company_Code_Text__c = 'T32';
     insert cmpny;
    
    Business_Unit__c bus1 = new Business_Unit__c();
    bus1.Name = 'business1';
    bus1.Company__c = cmpny.Id;
    insert bus1;

      //Insert SBU
      Sub_Business_Units__c sbu1 = new Sub_Business_Units__c();
      sbu1.name='SBUMedtronic1';
      sbu1.Business_Unit__c=bus1.id;
      insert sbu1;

    Therapy_Group__c TG=new Therapy_Group__c();
    TG.Name='Test Therapy Group';
    TG.Sub_Business_Unit__c =sbu1.Id;
    TG.Company__c = cmpny.Id;
    insert TG;    
  
    Therapy__c ter1 = new Therapy__c();
    ter1.Name = 'TestTherapy1';
    ter1.Business_Unit__c = bus1.Id;
    ter1.Sub_Business_Unit__c = sbu1.Id;
    ter1.Therapy_Group__c = TG.Id;
    ter1.Therapy_Name_Hidden__c = 'TestTherapy1';
    insert ter1;
    
    
  
  Implant__c imp1 = new Implant__c();
  imp1.Implant_Implanting_Account__c = acc1.Id;
  imp1.Implant_Implanting_Contact__c = cnt1.Id;
  imp1.Therapy__c = ter1.Id;
  imp1.Business_Unit__c = bus1.Id;
  insert imp1;


  Implant__c imp2 = new Implant__c();
  imp2.Implant_Implanting_Account__c = acc1.Id;
  imp2.Implant_Implanting_Contact__c = cnt1.Id;
  imp2.Therapy__c = ter1.Id;
  imp2.Business_Unit__c = bus1.Id;
  imp2.MMX_Implant_ID_Text__c = 'testId';
  insert imp2;  
    

  /*Asset ass1 = new Asset();
  ass1.Name = 'asset1';
  ass1.AccountId = acc1.Id;
  ass1.Implant_ID__c = imp1.Id;
  insert ass1;
*/
Implanted_Product__c impProduct=new Implanted_Product__c(implant_Id__c=imp1.id);
insert impProduct;
  
  Implant_Option__c io1= new Implant_Option__c();
  io1.Name = 'TestOption1';
  insert io1;
  
  
  Implant_Detail_Junction__c idj1 = new Implant_Detail_Junction__c();
  idj1.Implant_ID__c = imp1.Id;
  idj1.Implant_Option_ID__c = io1.Id;
  insert idj1;

    // Unit Test for ContractCloneWithAssets class 
    ApexPages.currentPage().getParameters().put('Id',imp1.Id);
    ApexPages.StandardController ctrl = new ApexPages.StandardController(imp1);          
//    clsAddContactActionPlan controller = new clsAddContactActionPlan(ctrl);  
    controllerImplantCloneWithDetails controller = new controllerImplantCloneWithDetails(ctrl);
    controller.cloneWithDetails();
    controller.CheckImplant();
    controller.Back();
    controller.getWarntext();
    controller.getBacktext();

    ApexPages.currentPage().getParameters().put('Id',imp2.Id);
    ApexPages.StandardController ctrl1 = new ApexPages.StandardController(imp2);          
    controllerImplantCloneWithDetails controller1 = new controllerImplantCloneWithDetails(ctrl1);
    controller1.cloneWithDetails();
    controller1.CheckImplant();
    
//    ApexPages.currentPage().getParameters().put('tcid',tc.id);
//    ApexPages.currentPage().getParameters().put('id',ap.id);
//    ApexPages.currentPage().getParameters().put('showData',sbu2.Account_Plan_Default__c);
    
}
}