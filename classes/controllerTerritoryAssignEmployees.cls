public class controllerTerritoryAssignEmployees {

    string terrId='';
    Boolean ActiveEmployees = false;
    public controllerTerritoryAssignEmployees(ApexPages.StandardController controller) {
        if(terrId==''){
            terrId=ApexPages.currentPage().getParameters().get('TerId');
        }
        // Total number of records to be shown on page
        this.rowsToShow='5';
        
        AvailableEmployees1();
        if(AssignedButtonPressed!=true)
        {
            AssignedButtonPressed=false;
        }

        if(RemoveFilterPressed == true)
        {
            RemoveFilterPressed = false;
        }
        if(ApplyFilter!=true)
        {
            ApplyFilter=false;
        }
        propTerId=terrId;

    }
    public boolean ApplyFilter {        
    get { return ApplyFilter; }        
    set { ApplyFilter = value; }    
    }   
    public string propTerId {
    get { return propTerId; }
    set { propTerId = value; }
    }

 
    public boolean AssignedButtonPressed {        
        get { return AssignedButtonPressed; }        
        set { AssignedButtonPressed = value; }    
    }

    public Boolean getActiveEmployees(){return ActiveEmployees;}
    public void setActiveEmployees(Boolean ActiveEmployees){this.ActiveEmployees = ActiveEmployees;}
       
    public boolean RemoveFilterPressed {        
        get { return RemoveFilterPressed; }        
        set { RemoveFilterPressed = value; }    
    } 

    public string EmplRecords {        
        get { return EmplRecords; }        
        set { EmplRecords = value; }    
    }
    public List<SelectOption> GetEmplRecSize() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('5','5'));
        options.add(new SelectOption('10','10'));
        options.add(new SelectOption('25','25'));
        options.add(new SelectOption('50','50'));
        options.add(new SelectOption('100','100')); 
        if(EmplRecords==null){
            EmplRecords='10';
        }   
        return options;
    }   



    // Country
    String Country;
    public String getCountry() { 
        System.debug('Ret Call - ' + Country);      
        return this.Country; }
    public void setCountry(String s) { 
        System.Debug('Set Call - ' + s);
        this.Country = s; }

    public List<SelectOption> getCountries() {
        System.debug('Dheeraj####' +Country);
        List<SelectOption> optionList = new List<SelectOption>();
        Territory2 currTerr = [Select Id, Company__c from Territory2 where Id =: terrId];
        String CMP = [Select Id from Company__c where Id =: currTerr.Company__c].Id;
        DIB_Country__c[]  Country1 = [Select Country_ISO_Code__c,Name from DIB_Country__c where Company__c =: CMP order by Name];             
        if(Country1.size()>0){    
          for (DIB_Country__c C : Country1){              
              optionList.add(new SelectOption(String.valueOf(C.get('Country_ISO_Code__c')),String.valueOf(C.get('Name'))));        
        }  
        System.debug('Dheeraj1####' +Country);
        System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> terrId is ' + terrId);
        if(Country==null){          
            String userTerrCountry =[Select Country_UID__c from Territory2 where id=:terrId].Country_UID__c;
            Country=userTerrCountry;  
             System.debug('Dheeraj2####111111' +Country);       
        }     
      }      
      return optionList;     
    } 


    public string EmplName{get;set;}
    public string EmplJobTitle{get;set;}
    public string EmplCountry{get;set;}
    public string EmplBusUnit{get;set;}
     
    // Get the detail information of the choosen Territory Node
    public Territory2 getTDetail(){
        Territory2 detail;
        detail = [Select Id, Name,Business_Unit__c, Territory2Type.DeveloperName, Country_UID__c,Short_Description__c,Therapy_Groups_Text__c, ParentTerritory2Id  from Territory2 where Id=:terrId];
        return detail;
    }
    public String getCrumbs(){
        String crumbs ;
        Boolean levelFound = false;
        crumbs ='';
        Territory2 currTerrId = [Select Id, Territory2Type.DeveloperName from Territory2 where Id =:terrId];
        System.debug('currTerrId##'+currTerrId);
        
        Territory2 allChildren = new Territory2();
        Id tempId = terrId;
            do{
                allChildren = [Select Id, Name,Business_Unit__c, Territory2Type.DeveloperName, Country_UID__c,Short_Description__c,Therapy_Groups_Text__c, ParentTerritory2Id  from Territory2 where Id=:tempId];
                 System.debug('allChildren##'+allChildren);
                If (levelFound){
                    crumbs = allChildren.Name + '  » ' + crumbs;
                }
                else {
                    crumbs = allChildren.Name + '  » ' + crumbs;
                
                }
                tempId = allChildren.ParentTerritory2Id;
                System.debug('tempId##'+tempId);
                If (allChildren.Territory2Type.DeveloperName == currTerrId.Territory2Type.DeveloperName){
                    levelFound = true;
                }
                
                } 
                while (allChildren.Territory2Type.DeveloperName != 'Region');
        
            integer crmbIndex = crumbs.lastIndexOf(' »')  ;
            if (crmbIndex !=-1){
                crumbs = crumbs.substring(0,crmbIndex);
            }       
        return crumbs;       
    } 
    public ApexPages.StandardSetController ConEmp;
    public ApexPages.StandardSetController ConSel;
    public String rowsToShow;      
    public list<User> AvailableEmployees = new  list<User>(); 
    public List<User> getAvailableEmployees() {return AvailableEmployees;}     

    public void AvailableEmployees1() 
    {
        System.Debug('AvailableEmployees1 Function passed');

        string Child = apexpages.currentpage().getUrl();
        integer inx  = Child.indexOf('TerId=');

        String terrId;
        Territory2 detail;
        if (inx != -1){
            terrId = Child.substring(inx + 6  );
        }
        else{
            Id userId = UserInfo.getUserId();
            String userTerrUID =[Select Territory_UID__c from User where id=:userId].Territory_UID__c;
                    System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> userTerrUID is ' + userTerrUID);
            Territory2 currTerrId = [Select Id, Territory2Type.DeveloperName from Territory2 where Territory_UID2__c =:userTerrUID];
            System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> current Id is ' + currTerrId.Id);
            terrId = currTerrId.Id;
        }
        
        System.Debug('AssignedEmployee - ' + terrId);
       Territory2 userTerr =[Select Country_UID__c, Name, Business_Unit__c, Territory2Type.DeveloperName,Company__c from Territory2 where id=:terrId];
        System.Debug('>>>>>>>>>>userTerr Territory2Type - ' + userTerr.Territory2Type.DeveloperName );
        System.Debug('>>>>>>>>>>userTerr Country_UID__c - ' + userTerr.Country_UID__c);
        String userTerrCountry;
        if(Country==null){
            userTerrCountry = userTerr.Country_UID__c;            
        }
        else {
            userTerrCountry = Country;  
        }        
        string strTerCountry='';
        if(userTerrCountry!='' && userTerrCountry!=null)
        {
            strTerCountry=[Select Name from DIB_Country__c where Country_ISO_Code__c=:userTerrCountry].Name;            
        }
        System.Debug('>>>>>>>>>>strTerCountry - ' + strTerCountry);
        Territory_Level_Configuration__c TLC = [select Job_Title_vs__c, Territory_Level__c from Territory_Level_Configuration__c where name=:userTerr.Territory2Type.DeveloperName and Company__c=:userTerr.Company__c limit 1];
        List<String> JobTitles;
        String levelJobTitles = TLC.Job_Title_vs__c;
        JobTitles = levelJobTitles.split(';');
        System.Debug('>>>>>>>>>>levelJobTitles - ' + levelJobTitles);
        list<UserTerritory2Association> userInTerr = [select UserId from UserTerritory2Association where Territory2Id=:terrId AND isActive = true];
            System.Debug('users in UserTerritory - ' + userInTerr.size());        
            System.Debug('Business in Territory - ' + userTerr.Business_Unit__c);
            list<Id> userIds=new list<Id>();
            for(integer i=0;i<userInTerr.size();i++)
            {
                userIds.add(string.valueOf(userInTerr[i].get('UserId')));    
            }         
            System.Debug('userIds - ' + userIds.size());   
            string strQuery='';  
            String userTerrBU =  userTerr.Business_Unit__c;  
            System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Active flag employees ' + ActiveEmployees);  
            System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Territory level =' + TLC.Territory_Level__c);
            set<id> UserSbuIds = new set<id>();
            string strUserProfile = 'Chatter Only';
            string strUserProfile1 = 'Chatter Free';            
            if (TLC.Territory_Level__c <= 3  ){
                if(userIds.size()>0)
                {
                    strQuery='select FirstName, LastName, name,Job_Title_vs__c,User_Business_Unit_vs__c, CountryOR__c, ReceivesInfoEmails, IsActive from User where ProfileID not IN (select ID from Profile where userlicense.name =: strUserProfile or userlicense.name =: strUserProfile1) and id not in : userIds and Job_Title_vs__c in : JobTitles and CountryOR__c=: strTerCountry  ';           }
                else
                {
                    strQuery='select FirstName, LastName, name,Job_Title_vs__c,User_Business_Unit_vs__c,CountryOR__c, ReceivesInfoEmails, IsActive from User where ProfileID not IN (select ID from Profile where userlicense.name =: strUserProfile or userlicense.name =: strUserProfile1) and Job_Title_vs__c in: JobTitles  and CountryOR__c=: strTerCountry ';                
                }         
            }
            else{    
                User_Business_Unit__c[] userSBU = [select user__c from User_Business_Unit__c where Business_Unit_text__c=:userTerrBU and Company__c=:userTerr.Company__c];
                System.Debug('>>>>> userSBU ' + userSBU + ' User Territory Company ' + userTerr.Company__c + ' userTerrBU - ' + userTerrBU);
                for(User_Business_Unit__c uSBU:userSBU){
                    UserSbuIds.add(uSBU.user__c);   
                }
                if(userIds.size()>0)
                {
                    strQuery='select FirstName, LastName, name,Job_Title_vs__c,User_Business_Unit_vs__c, CountryOR__c, ReceivesInfoEmails, IsActive from User where ProfileID not IN (select ID from Profile where userlicense.name =: strUserProfile or userlicense.name =: strUserProfile1) and id in : UserSbuIds and id not in : userIds and Job_Title_vs__c in : JobTitles and CountryOR__c=: strTerCountry  ';           }
                else
                {
                    strQuery='select FirstName, LastName, name,Job_Title_vs__c,User_Business_Unit_vs__c,CountryOR__c, ReceivesInfoEmails, IsActive from User where ProfileID not IN (select ID from Profile where userlicense.name =: strUserProfile or userlicense.name =: strUserProfile1) and id in : UserSbuIds and Job_Title_vs__c in: JobTitles  and CountryOR__c=: strTerCountry ';                
                } 
            }        
            System.Debug('>>>Testing EmplName - ' + EmplName);           
            if(EmplName!=null && EmplName!='')
            {
                 strQuery=strQuery + ' and Name like \''+EmplName+'%\'' ;          
            }
            if(EmplJobTitle!=null && EmplJobTitle!='')
            {
                 strQuery=strQuery + ' and Job_Title_vs__c like \''+EmplJobTitle+'%\'' ;          
            }
            if(EmplBusUnit!=null && EmplBusUnit!='')
            {
                 strQuery=strQuery + ' and User_Business_Unit_vs__c like \''+EmplBusUnit+'%\'' ;          
            }
            if(ActiveEmployees!=false)
            {
                 strQuery=strQuery + '  and IsActive = true' ;          
            }

            strQuery=strQuery + ' order by LastName, FirstName';

            System.Debug('>>>>>>>>>>>strQuery - ' + strQuery);

            ConEmp = new ApexPages.StandardSetController(Database.getQueryLocator(strQuery));
            if(EmplRecords==null){
                EmplRecords='10';
            }   
            ConEmp.setPageSize(integer.valueof(EmplRecords));        
            AvailableEmployees=(List<User>)ConEmp.getRecords();                                                                      
            
            if(EmplName!=null || EmplJobTitle!=null || EmplBusUnit!=null  || ActiveEmployees!=false)
            {
                if(EmplName!='' || EmplJobTitle!='' || EmplBusUnit!='' || ActiveEmployees!=false)
                {
                    ApplyFilter=true;                   
                }
                else
                {
                    ApplyFilter=false;          
                }                   
            }      
            else
            {
                ApplyFilter=false;          
            }    
          //  return null;        
     }



    public String getTotalPagesEmp() {
       if(ConEmp!=null){                
            String s=null;
            double resSize=ConEmp.getResultSize();
            Integer pageNumber;
            if(ConEmp.getResultSize()>0){   
                pageNumber=ConEmp.getPageNumber();
            }
            else{
                pageNumber=0;
            }
            Integer pages=(Math.round(Math.ceil(resSize/ConEmp.getPageSize())));
            s=  'Page ' +pageNumber+ ' of ' + pages+'';
            return s; 
       }
       else{
        return null;
       }
            
    }
  
    public void previousEmp() {
       if(ConEmp!=null){
        ConEmp.save();
        ConEmp.previous();
        AvailableEmployees = ConEmp.getRecords();
     }
    }

    public void nextEmp() {
      if(ConEmp!=null){
        ConEmp.save();
        ConEmp.next();
        AvailableEmployees = ConEmp.getRecords();
       }
    }


   public void firstEmp() {
    if(ConEmp!=null){
        ConEmp.save();
        ConEmp.first();
         AvailableEmployees = ConEmp.getRecords();
        }
    }

    public void lastEmp() {
     if(ConEmp!=null){
        ConEmp.save(); 
        ConEmp.last();
         AvailableEmployees = ConEmp.getRecords();
        }
    }

    public Boolean getHasNextEmp(){    
        if(ConEmp==null) return false;
        return ConEmp.getHasNext();
    }
    
    public Boolean getHasPreviousEmp(){    
        if(ConEmp==null)return false;
        return ConEmp.getHasPrevious();
    }  
    // End  Pagination   
              
    public String SelEmplId {get;set;} 
    public String RemoveSelEmplId {get;set;}   
    
    public list<User> SelectEmployee = new  list<User>(); 
    public List<User> getSelectEmployee() {return SelectEmployee;}         
    public PageReference SelectedEmployee(){
        System.Debug('>>>>>>>>>>>>Selected Employee Method ' + SelEmplId); 
        Boolean Emplfoundflag=false;
        if(SelectEmployee.size()>0)
        {
            System.Debug('>>>>SelectEmployee - ' + SelectEmployee.size());
            for(Integer i=0;i<SelectEmployee.size();i++)
            {
                if(SelectEmployee[i].Division==SelEmplId)
                {
                    Emplfoundflag=true;
                }                  
            }
        }
        if(Emplfoundflag==false)
        {        
            list<User> Usr=[select id, FirstName, LastName, name,Job_Title_vs__c, User_Business_Unit_vs__c, CountryOR__c, IsActive from User where id =: SelEmplId];               
            if(Usr.size()>0)
            {    
                System.Debug('>>>>>>>>>>>>InSide Selected ' + SelEmplId); 
                for(User U:Usr){  
                    User Usr1=new User();
                    Usr1.FirstName=U.FirstName; 
                    Usr1.LastName=U.LastName;                                         
                    Usr1.Division=U.Id; // Need to write the Id to check the duplicate & later insert it into the Database
                    Usr1.EmployeeNumber=U.Name;  // Need to write the Id to check the duplicate & later insert it into the Database
                    Usr1.Job_Title_vs__c=U.Job_Title_vs__c;  
                    Usr1.User_Business_Unit_vs__c=U.User_Business_Unit_vs__c;  
                    Usr1.CountryOR__c=U.CountryOR__c;
                    Usr1.IsActive = U.IsActive;
                    SelectEmployee.add(Usr1);
                }                
            }
        }
        return null;
        
    }
    
    public void RemoveSelectedEmployee(){
      if(SelectEmployee.size()>0){
            for(Integer i=0;i<SelectEmployee.size();i++)
            {
                if(SelectEmployee[i].Division==RemoveSelEmplId)
                {
                    SelectEmployee.remove(i);                     
                }                  
            }
          if(AvailableEmployees.size()>0){
            for(Integer j=0;j<AvailableEmployees.size();j++){          
            if(AvailableEmployees[j].Id == RemoveSelEmplId){
              AvailableEmployees[j].ReceivesInfoEmails = false; /*this is used to set the checkbox */  
                break; 
              }        
            }
          }
      }
    } 
    
 /*       public String getTotalPagesSel() {
       if(ConSel!=null){                
            String s=null;
            double resSize=ConSel.getResultSize();
            Integer pageNumber;
            if(ConSel.getResultSize()>0){   
                pageNumber=ConSel.getPageNumber();
            }
            else{
                pageNumber=0;
            }
            Integer pages=(Math.round(Math.ceil(resSize/ConSel.getPageSize())));
            s=  'Page ' +pageNumber+ ' of ' + pages+'';
            return s; 
       }
       else{
        return null;
       }
            
    }
  
    public void previousSel() {
       if(ConSel!=null){
        ConSel.previous();
        SelectEmployee = ConSel.getRecords();
     }
    }

    public void nextSel() {
      if(ConSel!=null){
        ConSel.next();
        SelectEmployee = ConSel.getRecords();
       }
    }


   public void firstSel() {
    if(ConSel!=null){
        ConSel.first();
         SelectEmployee = ConSel.getRecords();
        }
    }

    public void lastSel() {
     if(ConSel!=null){
        ConSel.last();
         SelectEmployee = ConSel.getRecords();
        }
    }

    public Boolean getHasNextSel(){    
        if(ConSel==null) return false;
        return ConSel.getHasNext();
    }
    
    public Boolean getHasPreviousSel(){    
        if(ConSel==null)return false;
        return ConSel.getHasPrevious();
    }  
    
   */     
    public PageReference CancelEmplAssign(){
         PageReference p = new PageReference('/apex/TerritoryManagement?id='+ terrId);    
        System.debug('>>>>>>>>>>>>>>>>>>>>>Pageref is ' + p);
        p.setRedirect(true);
    return p;
    }
    public static PageReference CancelEmplAssign2(string territoryId){
         PageReference p = new PageReference('/apex/TerritoryManagement?id='+ territoryId);    
        System.debug('>>>>>>>>>>>>>>>>>>>>>Pageref is ' + p);
        p.setRedirect(true);
    return p;
    }

    
    
 // Select all
     public PageReference SelectedEmplAll(){
        System.Debug('>>>>>>>>>>>>Selected Employee Method ' + SelEmplId); 
/*        Boolean Emplfoundflag=false;
        for (user u:AvailableEmployees){
        if(SelectEmployee.size()>0)
        {
            System.Debug('>>>>SelectEmployee - ' + SelectEmployee.size());
             for(Integer i=0;i<SelectEmployee.size();i++)
            {
                //System.Debug('>>>>inside SelectEmployee - ' + A.Id + ' '  + SelAccId);
                if(SelectEmployee[i].Division== u.Id )
                {
                    Emplfoundflag=true;
                    SelectEmployee.remove(i);                     
                }                  
            }
        }
        if(Emplfoundflag==false)
        {        
                User Usr1=new User();
                    Usr1=new User();
                    Usr1.FirstName=U.FirstName; 
                    Usr1.LastName=U.LastName;                                         
                    Usr1.Division=U.Id; // Need to write the Id to check the duplicate & later insert it into the Database
                    Usr1.EmployeeNumber=U.Name;  // Need to write the Id to check the duplicate & later insert it into the Database
                    Usr1.Job_Title_vs__c=U.Job_Title_vs__c;  
                    Usr1.User_Business_Unit_vs__c=U.User_Business_Unit_vs__c;  
                    Usr1.CountryOR__c=U.CountryOR__c; 
                    Usr1.ReceivesInfoEmails= true;                  
                    SelectEmployee.add(Usr1);
                }                
        }*/
        
        if(AvailableEmployees.size()>0){
            //SelectAccount.clear();
                User Usr1=new User();                     
            for(User U:AvailableEmployees){  
                    Usr1=new User();
                    Usr1.FirstName=U.FirstName; 
                    Usr1.LastName=U.LastName;                                         
                    Usr1.Division=U.Id; // Need to write the Id to check the duplicate & later insert it into the Database
                    Usr1.EmployeeNumber=U.Name;  // Need to write the Id to check the duplicate & later insert it into the Database
                    Usr1.Job_Title_vs__c=U.Job_Title_vs__c;  
                    Usr1.User_Business_Unit_vs__c=U.User_Business_Unit_vs__c;  
                    Usr1.CountryOR__c=U.CountryOR__c; 
                    Usr1.IsActive = U.IsActive;
                integer intAlreadyExist=0;
                for(Integer i=0;i<SelectEmployee.size();i++){
                    if( SelectEmployee[i].Division == U.Id){
                        intAlreadyExist = 1;
                        break;
                    }
                }                           
                if(intAlreadyExist==0){
                    SelectEmployee.add(Usr1);                                       
                }                   
            }              
        }        
        return null;
        
    }
 
 
    
    /*public list<String> Test1 {        
        get { return AssignEmployees; }        
        set { Test1 = value; }    
    }*/

    String Test1;
    public String getTest1() { return Test1; } 
    public void setTest1(string value) { Test1 = value; }
    
    private list<String> AssignEmployees = new  list<String>(); 
    public list<String> getAssignEmployees() {return AssignEmployees;}            
    
    public void AssignEmployees(){

        /*if(SelectEmployee.size()>0)        
        {
           System.Debug('Inside iF'); 
            String Uterr ;      
            for(User Usr:SelectEmployee){          

                Uterr =Usr.Division;        
                System.Debug('User Inserted in UserTerritory Table');  
                AssignEmployees.add(Uterr);      
            }                            
        }*/

        if(SelectEmployee.size()>0){
            for(integer i=0;i<SelectEmployee.size();i++)
            {
                Test1 = Test1 + SelectEmployee[i].Division + ',';
            }
            Test1 = Test1.substring(0,(Test1.length()-1));
        }
        else
        {
            Test1 = ''; 
        }
        
        AssignedButtonPressed = true;
        System.Debug('AssignedButtonPressed = ' + AssignedButtonPressed);

    }    
    public PageReference RemoveEmployeeFilter(){
        EmplName=null;
        EmplJobTitle=null;
        EmplCountry=null;
        EmplBusUnit=null; 
        ActiveEmployees = false; 
        AvailableEmployees1();  
        RemoveFilterPressed = true;     
        return null;       
    }    
}