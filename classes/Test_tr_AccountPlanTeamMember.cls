@isTest
private class Test_tr_AccountPlanTeamMember {
    
    @testSetup
    private static void createTestData(){
    
    	Company__c cmpny = new Company__c();		
        cmpny.name='Europe';
        cmpny.CurrencyIsoCode = 'EUR';
        cmpny.Current_day_in_Q1__c=56;
        cmpny.Current_day_in_Q2__c=34; 
        cmpny.Current_day_in_Q3__c=5; 
        cmpny.Current_day_in_Q4__c= 0;   
        cmpny.Current_day_in_year__c =200;
        cmpny.Days_in_Q1__c=56;  
        cmpny.Days_in_Q2__c=34;
        cmpny.Days_in_Q3__c=13;
        cmpny.Days_in_Q4__c=22;
        cmpny.Days_in_year__c =250;
        cmpny.Company_Code_Text__c = 'T35';
		insert cmpny;
		
		Business_Unit_Group__c bug =  new Business_Unit_Group__c();
        bug.Master_Data__c =cmpny.id;
        bug.name='CVG';      
        insert bug;
						
		Account testAccount = new Account();
		testAccount.Name = 'CVG Test Account';
		testAccount.Account_Country_vs__c = 'BELGIUM';
		testAccount.SAP_Id__c = '0123456789';
		insert testAccount;		
		
		Account_Plan_2__c accPlan = new Account_Plan_2__c();
		accPlan.Account__c = testAccount.Id;
		accPlan.Account_Plan_Level__c = 'Business Unit Group';
		accPlan.Business_Unit_Group__c = bug.Id;		
		accPlan.RecordTypeId = RecordTypeMedtronic.getRecordTypeByDevName('Account_Plan_2__c', 'EUR_CVG_BUG').Id;
		insert accPlan;    	
    }    
    
    private static testmethod void testCheckIsOwnerFlag(){
    	
    	Account_Plan_2__c accPlan = [Select Id from Account_Plan_2__c];
    	
    	Account_Plan_Team_Member__c ownerTM = [Select Id, Account_Plan_Owner__c from Account_Plan_Team_Member__c where Account_Plan__c = :accPlan.Id];
    	
    	System.assert(ownerTM.Account_Plan_Owner__c == true);
    }
    
    private static testmethod void testCheckSharing(){
    	
    	Account_Plan_2__c accPlan = [Select Id from Account_Plan_2__c];
    	
        User oUser_System = clsTestData_User.createUser_SystemAdministratorMDT('tadm01', true);
        User rtgUser = clsTestData_User.createUser('test.user.001@medtronic.com.test', 'tu001', 'BELGIUM', clsUtil.getUserProfileId('EUR Field Force RTG'), clsUtil.getUserRoleId('EUR xBU'), true);
        User rtgUser2 = clsTestData_User.createUser('test.user.002@medtronic.com.test', 'tu002', 'BELGIUM', clsUtil.getUserProfileId('EUR Field Force RTG'), clsUtil.getUserRoleId('EUR xBU'), true);
    	
        System.runAs(oUser_System){

        	List<Account_Plan_2__Share> accPlanShare = [Select Id from Account_Plan_2__Share where ParentId = :accPlan.Id AND UserOrGroupId = :rtgUser.Id];
        	
        	System.assert(accPlanShare.size() == 0);
        	
        	Test.startTest();
        	
        	Account_Plan_Team_Member__c tm = new Account_Plan_Team_Member__c();
        	tm.Account_Plan__c = accPlan.Id;
        	tm.Team_Member__c = rtgUser.Id;
        	tm.Role__c = 'Core Acc Team';
        	insert tm;
        	
        	accPlanShare = [Select Id from Account_Plan_2__Share where ParentId = :accPlan.Id AND UserOrGroupId = :rtgUser.Id];
        	
        	System.assert(accPlanShare.size() == 1);
        	
        	tm.Team_Member__c = rtgUser2.Id;
        	update tm;
        	
        	accPlanShare = [Select Id from Account_Plan_2__Share where ParentId = :accPlan.Id AND UserOrGroupId = :rtgUser.Id];    	
        	System.assert(accPlanShare.size() == 0);
        	
        	accPlanShare = [Select Id from Account_Plan_2__Share where ParentId = :accPlan.Id AND UserOrGroupId = :rtgUser2.Id];    	
        	System.assert(accPlanShare.size() == 1);
        	
        	delete tm;
        	
        	accPlanShare = [Select Id from Account_Plan_2__Share where ParentId = :accPlan.Id AND UserOrGroupId = :rtgUser2.Id];    	
        	System.assert(accPlanShare.size() == 0);

        }

    }
    
    private static testmethod void checkValidateDuplicateSAM(){
    	
    	Account_Plan_2__c accPlan = [Select Id, Account__c, Business_Unit_Group__c from Account_Plan_2__c];
    	
        User oUser_System = clsTestData_User.createUser_SystemAdministratorMDT('tadm01', true);
        User rtgUser = clsTestData_User.createUser('test.user.001@medtronic.com.test', 'tu001', 'BELGIUM', clsUtil.getUserProfileId('EUR Field Force RTG'), clsUtil.getUserRoleId('EUR xBU'), true);
        User rtgUser2 = clsTestData_User.createUser('test.user.002@medtronic.com.test', 'tu002', 'BELGIUM', clsUtil.getUserProfileId('EUR Field Force RTG'), clsUtil.getUserRoleId('EUR xBU'), true);

        System.runAs(oUser_System){    	

        	Test.startTest();
        	
        	Account_Plan_Team_Member__c tm = new Account_Plan_Team_Member__c();
        	tm.Account_Plan__c = accPlan.Id;
        	tm.Team_Member__c = rtgUser.Id;
        	tm.Role__c = 'Strategic Account Manager';
        	insert tm;
        	
        	Account_Plan_Team_Member__c tm2 = new Account_Plan_Team_Member__c();
    	    tm2.Account_Plan__c = accPlan.Id;
    	    tm2.Team_Member__c = rtgUser2.Id;
    	    tm2.Role__c = 'Strategic Account Manager';
        	
        	Boolean isError = false;
        	
        	try{
        		    	
    	    	insert tm2;
    	    	
        	}catch(Exception e){
        		
        		System.assert(e.getMessage().contains('Only one Team Member with role \'Strategic Account Manager\' can be created per Account Plan'));
        		isError = true;
        	}
        	
        	System.assert(isError == true);
        	
        	delete tm;
    				
    		insert tm2;
    		
    		isError = false;
        	
        	try{
        		    	
    	    	undelete tm;
    	    	
        	}catch(Exception e){
        		
        		System.assert(e.getMessage().contains('Only one Team Member with role \'Strategic Account Manager\' can be created per Account Plan'));
        		isError = true;
        	}
        	
        	System.assert(isError == true);

        }

    }

}