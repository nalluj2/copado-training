@isTest
private class Test_bl_SynchronizationService_Target {
	
	private static testMethod void syncTarget(){
		
		Synchronization_Service_Setting__c syncSettings = new Synchronization_Service_Setting__c();
		syncSettings.username__c = 'test@medtronic.com';
		syncSettings.password__c = 'password';
		syncSettings.client_id__c = '123456789';
		syncSettings.client_secret__c = 'XXXXX';
		syncSettings.is_sandbox__c = true;
		syncSettings.sync_Enabled__c = true;
		insert syncSettings;
		
		//Account
		Account acc = new Account();	
		acc.AccountNumber = '0000000';
		acc.SAP_Id__c = '0000000';
		acc.Name = 'Syn Test Account';
		acc.Phone = '+123456789';
		acc.BillingCity = 'Minneapolis';
		acc.BillingCountry = 'UNITED STATES';
		acc.BillingState = 'Minnesota';
		acc.BillingStreet = 'street';
		acc.BillingPostalCode = '123456';
		acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non-SAP Account').getRecordTypeId();	
		insert acc;
		
		//Products
		Product2 testProduct = new Product2();
		testProduct.Name = 'PoleStar';
		testProduct.ProductCode = '000999';
		testProduct.isActive = true;					
		insert testProduct;
		
		//Asset
		Asset asset = new Asset();
		asset.AccountId = acc.Id;
		asset.Product2Id = testProduct.Id;
		asset.Asset_Product_Type__c = 'PoleStar N-10';
		asset.Ownership_Status__c = 'Purchased';
		asset.Name = '123456789';
		asset.Serial_Nr__c = '123456789';
		asset.Status = 'Installed';
		asset.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('PoleStar').getRecordTypeId();
		insert asset;
		
		//Software
		Software__c soft = new Software__c();
		soft.Name = 'Test Software';
		soft.Active__c = true;
		soft.External_Id__c = 'Test_Software_Id';
		soft.Software_Name__c = 'Test Software';
		soft.Version__c = '1.0';
		soft.Asset_Product_Type__c = 'PoleStar N-10';
		insert soft;
		
		Test.startTest();
		
		Test_bl_SynchronizationService_Utils.CalloutMock mockImpl = new Test_bl_SynchronizationService_Utils.CalloutMock();
		
		Test.setMock(HttpCalloutMock.class, mockImpl);
		bl_SynchronizationService_Utils.calloutMock = mockImpl;
		
		SynchronizationService_InstSoftware.InstalledSoftwareSyncPayload request = new SynchronizationService_InstSoftware.InstalledSoftwareSyncPayload();
				
		//Installed Software
		Installed_Software__c iSoftware = new Installed_Software__c();
		iSoftware.External_Id__c = 'Test_Installed_Software_Id';
		iSoftware.Purchased__c = true;
		iSoftware.Purchase_Date__c = Date.Today();
		iSoftware.Install_Date__c = Date.Today();
		
		request.instSoftware = iSoftware;
		
		request.assetId = '123456789';
		request.softwareId = 'Test_Software_Id';
		request.createdDate = Datetime.now();
		request.createdBy = 'Unit Test User';
		request.lastModifiedDate = Datetime.now();
		request.lastModifiedBy = 'Unit Test User';
		
		bl_SynchronizationService_Target.processRecordPayload(JSON.serialize(request), 'Installed_Software__c');		
	}
}