@isTest
private class Test_Contract_Account {
	
	
	private static testMethod void testRollupAccountSAPIds(){
		
		Account acc1 = new Account();
		acc1.Name = 'Test Account 1';
		acc1.SAP_Id__c = '1111111111';
		
		Account acc2 = new Account();
		acc2.Name = 'Test Account 2';
		acc2.SAP_Id__c = '2222222222';
		
		Account acc3 = new Account();
		acc3.Name = 'Test Account 3';
		acc3.SAP_Id__c = '3333333333';
		
		Account acc4 = new Account();
		acc4.Name = 'Test Account 4';
		
		insert new List<Account>{acc1, acc2, acc3, acc4};
		
		Contract_xBU__c contract = new Contract_xBU__c();
		contract.Account__c = acc1.Id;
		contract.Status__c = 'In Process C&P';
		insert contract;
				
		Test.startTest();
		
		Contract_Account__c conAcc2 = new Contract_Account__c();
		conAcc2.Contract_xBU__c = contract.Id;
		conAcc2.Account__c = acc2.Id;
		
		Contract_Account__c conAcc3 = new Contract_Account__c();
		conAcc3.Contract_xBU__c = contract.Id;
		conAcc3.Account__c = acc3.Id;
		
		Contract_Account__c conAcc4 = new Contract_Account__c();
		conAcc4.Contract_xBU__c = contract.Id;
		conAcc4.Account__c = acc4.Id;
		
		insert new List<Contract_Account__c>{conAcc2, conAcc3, conAcc4};
		
		contract = [Select Additional_Account_SAP_Id_s_lt__c from Contract_xBU__c where Id = :contract.Id];
		System.assert(contract.Additional_Account_SAP_Id_s_lt__c.contains('1111111111') == false);
		System.assert(contract.Additional_Account_SAP_Id_s_lt__c.contains('2222222222'));
		System.assert(contract.Additional_Account_SAP_Id_s_lt__c.contains('3333333333'));	
		
		delete conAcc3;	
		
		contract = [Select Additional_Account_SAP_Id_s_lt__c from Contract_xBU__c where Id = :contract.Id];
		System.assert(contract.Additional_Account_SAP_Id_s_lt__c.contains('1111111111') == false);
		System.assert(contract.Additional_Account_SAP_Id_s_lt__c.contains('2222222222'));
		System.assert(contract.Additional_Account_SAP_Id_s_lt__c.contains('3333333333') == false);
		
		undelete conAcc3;	
		
		contract = [Select Additional_Account_SAP_Id_s_lt__c from Contract_xBU__c where Id = :contract.Id];
		System.assert(contract.Additional_Account_SAP_Id_s_lt__c.contains('1111111111') == false);
		System.assert(contract.Additional_Account_SAP_Id_s_lt__c.contains('2222222222'));
		System.assert(contract.Additional_Account_SAP_Id_s_lt__c.contains('3333333333'));
		
		update conAcc2;
		
		contract = [Select Additional_Account_SAP_Id_s_lt__c from Contract_xBU__c where Id = :contract.Id];
		System.assert(contract.Additional_Account_SAP_Id_s_lt__c.contains('1111111111') == false);
		System.assert(contract.Additional_Account_SAP_Id_s_lt__c.contains('2222222222'));
		System.assert(contract.Additional_Account_SAP_Id_s_lt__c.contains('3333333333'));
	}    
}