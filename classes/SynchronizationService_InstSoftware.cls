public without sharing class SynchronizationService_InstSoftware implements SynchronizationService_Object{
	
	//Set with the external Ids of the records already processed in this transaction. Used to avoid generating multiple notifications for the same change
	private Set<String> processedRecords = new Set<String>();
		
	//This method returns the External Ids of the records that have relevant modifications for the target Org. This includes created records or 
	//modified records where synchronized fields have changed. 
	public Set<String> hasChangeForNotification(List<SObject> records, Map<Id, SObject> oldRecords){
		
		Set<String> result = new Set<String>();
				
		for(SObject rawObject : records){
			
			Installed_Software__c record = (Installed_Software__c) rawObject;
			
			if(processedRecords.contains(record.External_Id__c)) continue;
			
			//On Insert we always send notification
			if(oldRecords == null){
				result.add(record.External_Id__c);
				processedRecords.add(record.External_Id__c);
			}//If Update we only send notification when relevant fields are modified
			else{
				
				if( bl_SynchronizationService_Utils.hasChanged( record, oldRecords.get(record.Id), syncFields ) == true ){
					
					result.add(record.External_Id__c);
					processedRecords.add(record.External_Id__c);
				}
			}
		}
		
		return result;
	}
	
	//This method collects and returns the complete information for a record. This includes the records itself as well as any related record that needs
	//to exist in the target Org too. The returned format is JSON serialized string.
	public String generatePayload(String externalId){
		
		InstalledSoftwareSyncPayload payload = new InstalledSoftwareSyncPayload();
		
		String query = 	'SELECT ' + String.join( syncFields, ',') + 
						',Asset__r.Serial_Nr__c, Software__r.External_Id__c ' +
						'FROM Installed_Software__c WHERE External_Id__c = :externalId LIMIT 2';
						
		List<Installed_Software__c> instSoftwares = Database.query(query);
			
		//If no record or more than 1 is found, we return an empty response
		if(instSoftwares.size() == 1){
			
			Installed_Software__c instSoftware = instSoftwares[0];	
			bl_SynchronizationService_Utils.prepareForJSON(instSoftware, syncFields);		
			instSoftware.Id = null;//The local SFDC Id is not relevant in the target Org
			
			payload.InstSoftware = instSoftware;	
			
			if(instSoftware.Asset__r != null){
				
				payload.AssetId = instSoftware.Asset__r.Serial_Nr__c;
				instSoftware.Asset__r = null;
				instSoftware.Asset__c = null;
			}
			
			if(instSoftware.Software__r != null){
								
				payload.SoftwareId = instSoftware.Software__r.External_Id__c;
				instSoftware.Software__r = null;
				instSoftware.Software__c = null;
			}
			
			//Done in a separate query because we cannot clear the value from the CreatedDate field before sending it to the target
			Installed_Software__c instSoftwareDetails = [Select CreatedDate, CreatedBy.Name, LastModifiedDate, LastModifiedBy.Name from Installed_Software__c where External_Id__c = :externalId];
						
			payload.CreatedDate = instSoftwareDetails.CreatedDate;			
			payload.CreatedBy = instSoftwareDetails.CreatedBy.Name;					
			payload.LastModifiedDate = instSoftwareDetails.LastModifiedDate;			
			payload.LastModifiedBy = instSoftwareDetails.LastModifiedBy.Name;			
		}
		
		return JSON.serialize(payload);
	}
	
	//Implementation needed only in Target Org
	public String processPayload(String inputPayload){
		
		String processedPayload = bl_SynchronizationService_Utils.translatePayload(inputPayload, fieldMapping);
		
		InstalledSoftwareSyncPayload payload = (InstalledSoftwareSyncPayload) JSON.deserialize(processedPayload, InstalledSoftwareSyncPayload.class);
		
		//If there was a problem in the source generating the information (no record or multiple records found) we finish here 
		if(payload.InstSoftware == null) return null;
		
		Installed_Software__c instSoftware = payload.InstSoftware;
		
		//Asset		
		//Manual External Id mapping
		List<Asset> instSoftAsset = [Select Id from Asset where Serial_Nr__c = :payload.AssetId 
										AND RecordType.DeveloperName IN ('ENT', 'Midas', 'Navigation', 'Accessories', 'PoleStar', 'Mazor', 'Visualase', 'Pain') LIMIT 2];
		
		//Because US side sends all the installed software independently of their region, we filter out the ones that are connected to Assets not found in INTL side.
		if(payload.AssetId == null || instSoftAsset.size() == 0) return 'Asset not in INTL';
					
		if(instSoftAsset.size() == 1) instSoftware.Asset__c = instSoftAsset[0].Id;
		else throw new bl_SynchronizationService_Utils.SyncService_Exception('Multiple Assets found for Serial Number : ' + payload.AssetId);
			
		//Software				
		if(payload.SoftwareId != null){
				
			//Manual External Id mapping
			List<Software__c> software = [Select Id from Software__c where External_Id__c = :payload.SoftwareId LIMIT 2];
				
			if(software.size() == 1) instSoftware.Software__c = software[0].Id;
			else{
					
				if(software.size() == 0) throw new bl_SynchronizationService_Utils.SyncService_Exception('No Software found for Id : ' + payload.SoftwareId);
				else throw new bl_SynchronizationService_Utils.SyncService_Exception('Multiple Softwares found for Id : ' + payload.SoftwareId);					
			}				
		}
			
		instSoftware.CreatedDate__c = payload.CreatedDate;
		instSoftware.CreatedBy__c = payload.CreatedBy;
		instSoftware.LastModifiedDate__c = payload.LastModifiedDate;
		instSoftware.LastModifiedBy__c = payload.LastModifiedBy;
			
		//Manual Upsert operation				
		upsert instSoftware External_Id__c;		
					
		return instSoftware.Id;	
	}
	
	//Model classes for the requests and responses. To be used by the JSON serialize/deserialize
	public class InstalledSoftwareSyncPayload{
		
		public Installed_Software__c instSoftware {get; set;}
		public String softwareId {get; set;}
		public String assetId {get; set;}
		public Datetime createdDate {get; set;}
		public String createdBy {get; set;}
		public Datetime lastModifiedDate {get; set;}
		public String lastModifiedBy {get; set;}				
	}
	
	private static List<String> syncFields = new List<String>
	{
		'External_Id__c',
		'Install_Date__c',
		'Purchased__c',
		'Purchase_Date__c',				
		'Software__c',
		'Asset__c'						
	};	
	
	private static Map<String, String> fieldMapping = new Map<String, String>		
	{
		'System__c' => 'Asset__c',
		'System__r' => 'Asset__r'				
	};
}