@isTest
private class Test_Implant_Scheduling {
	
	private static testmethod void configureTeams(){
		
		Implant_Scheduling_Team__c team = new Implant_Scheduling_Team__c();			
		team.Name = 'Another Test Team';
		team.Team_Color__c = 'rgb(99, 99, 99)';
		team.Work_Day_End__c = '18';
		team.Work_Day_Start__c = '8';
		team.Working_Days__c = '1:5';			
		team.RecordTypeId = [Select Id from RecordType where SObjectType = 'Implant_Scheduling_Team__c' AND DeveloperName = 'Activity_Scheduling_Team'].Id;			
		insert team;
		
		team = [Select Name, Team_Group__c, Team_Group_Externals__c, (Select UserOrGroupId from Shares where RowCause = :Schema.Implant_Scheduling_Team__Share.RowCause.Team_Member__c) from Implant_Scheduling_Team__c where Id = :team.Id];
		
		System.assert(team.Team_Group__c != null);
		System.assert(team.Team_Group_Externals__c != null);
		System.assert(team.Shares.size() == 2);
		System.assert(team.Shares[0].UserOrGroupId == team.Team_Group__c);
		
		Group teamGroup = [Select Id, Name, DoesIncludeBosses from Group where Id = :team.Team_Group__c];
		System.assert(teamGroup.Name == team.Name);
		System.assert(teamGroup.DoesIncludeBosses == false);
		
		Group teamGroupExternals = [Select Id, Name, DoesIncludeBosses from Group where Id = :team.Team_Group_Externals__c];
		System.assert(teamGroupExternals.Name == team.Name + ' - Agents');
		System.assert(teamGroupExternals.DoesIncludeBosses == false);
		
		User otherUser = [Select Id from User where Profile.Name LIKE 'System Administrator%' AND Id != :UserInfo.getUserId() AND isActive = true LIMIT 1];
		
		Test.startTest();
										
		team.OwnerId = otherUser.Id;
		team.Name = 'Another Test Team updated';
		update team;
		
		Test.stopTest();
				
		Implant_Scheduling_Team_Admin__c admin = [Select Id, Admin__c, Owner__c from Implant_Scheduling_Team_Admin__c where Team__c = :team.Id];			
		System.assert(admin.Admin__c == otherUser.Id);
		System.assert(admin.Owner__c == true);
		
		teamGroup = [Select Id, Name, DoesIncludeBosses from Group where Id = :team.Team_Group__c];
		System.assert(teamGroup.Name == team.Name);
				
	}
	
	private static testmethod void configureTeamAdmins(){
						
		Implant_Scheduling_Team__c team = [Select Id, Team_Group__c from Implant_Scheduling_Team__c];
		
		Implant_Scheduling_Team_Admin__c admin = [Select Id, Admin__c, Owner__c from Implant_Scheduling_Team_Admin__c where Team__c = :team.Id];			
		System.assert(admin.Admin__c == UserInfo.getUserId());
		System.assert(admin.Owner__c == true);
		
		List<PermissionSetAssignment> userAssignment = [Select AssigneeId from PermissionSetAssignment where PermissionSet.Name = 'Implant_Scheduling_with_Teams' AND AssigneeId = :UserInfo.getUserId()];        
        System.assert(userAssignment.size() == 1);
        
        List<GroupMember> adminGroupMember = [Select GroupId, UserOrGroupId from GroupMember where UserOrGroupId = :UserInfo.getUserId() AND GroupId = :team.Team_Group__c];
       	System.assert(adminGroupMember.size() == 1);
						
		Test.startTest();
		
		User otherUser = [Select Id from User where Profile.Name LIKE 'System Administrator%' AND Id != :UserInfo.getUserId() AND isActive = true LIMIT 1];
		
		Implant_Scheduling_Team_Admin__c otherAdmin = new Implant_Scheduling_Team_Admin__c();
		otherAdmin.Team__c = team.Id;
		otherAdmin.Admin__c = otherUser.Id;
		
		insert otherAdmin;
		
		Test.stopTest();
		
		List<PermissionSetAssignment> otherUserAssignment = [Select AssigneeId from PermissionSetAssignment where PermissionSet.Name = 'Implant_Scheduling_with_Teams' AND AssigneeId = :otherUser.Id];        
        System.assert(otherUserAssignment.size() == 1);
				
		List<Implant_Scheduling_Team__share> otherAdminShare = [Select Id from Implant_Scheduling_Team__share where ParentId = :team.Id AND UserOrGroupId = :otherUser.Id AND AccessLevel = 'Edit' AND RowCause = : Schema.Implant_Scheduling_Team__Share.RowCause.Team_Admin__c];
        System.assert(otherAdminShare.size() == 1);
        
        List<GroupMember> otherAdminGroupMember = [Select GroupId, UserOrGroupId from GroupMember where UserOrGroupId = :otherUser.Id AND GroupId = :team.Team_Group__c];
       	System.assert(otherAdminGroupMember.size() == 1);
       	
       	List<Mobile_App_Assignment__c> otherUserApp = [Select Id, Active__c from Mobile_App_Assignment__c where User__c = :otherUser.Id AND Mobile_App__r.Unique_Key__c = 'ACT_SCHDL' ];
       	System.assert(otherUserApp.size() == 1);
       	System.assert(otherUserApp[0].Active__c);
	}
	
	private static testmethod void configureTeamMembers(){
				
		Implant_Scheduling_Team__c team = [Select Id, Team_Group__c from Implant_Scheduling_Team__c];
		Implant_Scheduling_Team_Member__c member = [Select Member__c from Implant_Scheduling_Team_Member__c where Team__c = :team.Id];
						
		List<PermissionSetAssignment> otherUserAssignment = [Select AssigneeId from PermissionSetAssignment where PermissionSet.Name = 'Implant_Scheduling_with_Teams' AND AssigneeId = :member.Member__c];        
        System.assert(otherUserAssignment.size() == 1);
		
        List<GroupMember> otherMemberGroupMember = [Select GroupId, UserOrGroupId from GroupMember where UserOrGroupId = :member.Member__c AND GroupId = :team.Team_Group__c];
       	System.assert(otherMemberGroupMember.size() == 1);
       	
       	List<Mobile_App_Assignment__c> otherUserApp = [Select Id, Active__c from Mobile_App_Assignment__c where User__c = :member.Member__c AND Mobile_App__r.Unique_Key__c = 'ACT_SCHDL' ];
       	System.assert(otherUserApp.size() == 1);
       	System.assert(otherUserApp[0].Active__c);
		
		Test.startTest();
		
		User currentUser = new User(Id = UserInfo.getUserId());
		
		System.runAs(currentUser){
	   	
		   	Account acc = new Account();
			acc.Name = 'Test Account';
			acc.Account_Country_vs__c = 'NETHERLANDS';
			insert acc;
				
			Case teamCase = new Case();
			teamCase.AccountId = acc.Id;			
			teamCase.Start_of_Procedure__c = DateTime.now().addHours(2); 
			teamCase.Procedure_Duration_Implants__c = '03:00';
			teamCase.RecordTypeId = [Select Id from RecordType where SObjectType = 'Case' AND DeveloperName = 'Implant_Scheduling'].Id;
			teamCase.Activity_Scheduling_Team__c = team.Id;
			teamCase.Assigned_To__c = member.Member__c;
			teamCase.Status = 'Assigned';			
			
			bl_Case_Trigger.runningSchedulingApp = true;
			bl_Activity.sendNotificationTOMMX = true;
			
			insert teamCase;
			
			Boolean hasError = false;
			try{
				
				delete member;
				
			}catch(Exception e){
				
				hasError = true;
				System.assert(e.getMessage().contains('There are future Requests assigned to this Member'));
			}
	       	
	       	System.assert(hasError);
	       	
	       	teamCase.Status = 'Open';
	       	teamCase.Assigned_To__c = null;
	       	update teamCase;
	       	
	       	delete member;
		}
       	
       	Test.stopTest();
       	
       	otherUserAssignment = [Select AssigneeId from PermissionSetAssignment where PermissionSet.Name = 'Implant_Scheduling_with_Teams' AND AssigneeId = :member.Member__c];        
        System.assert(otherUserAssignment.size() == 0);
		
        otherMemberGroupMember = [Select GroupId, UserOrGroupId from GroupMember where UserOrGroupId = :member.Member__c AND GroupId = :team.Team_Group__c];
       	System.assert(otherMemberGroupMember.size() == 0);
       	
       	otherUserApp = [Select Id, Active__c from Mobile_App_Assignment__c where User__c = :member.Member__c AND Mobile_App__r.Unique_Key__c = 'ACT_SCHDL' ];
       	System.assert(otherUserApp.size() == 1);
       	System.assert(otherUserApp[0].Active__c == false);
	}
	
	private static testmethod void assignCaseTeam(){
		
		Implant_Scheduling_Team__c team = [Select Id, Team_Group__c from Implant_Scheduling_Team__c];
		Implant_Scheduling_Team_Member__c member = [Select Member__c from Implant_Scheduling_Team_Member__c where Team__c = :team.Id];
			
		Account acc = new Account();
		acc.Name = 'Test Account';
		acc.Account_Country_vs__c = 'NETHERLANDS';
		insert acc;
			
		List<Case> cases = new List<Case>();
		
		Case teamCase = new Case();
		teamCase.AccountId = acc.Id;			
		teamCase.Start_of_Procedure__c = DateTime.now(); 
		teamCase.Procedure_Duration_Implants__c = '03:00';
		teamCase.RecordTypeId = [Select Id from RecordType where SObjectType = 'Case' AND DeveloperName = 'Implant_Scheduling'].Id;
		teamCase.Activity_Scheduling_Team__c = team.Id;
		teamCase.Status = 'Open';			
		cases.add(teamCase);
		
		Case memberCase = new Case();
		memberCase.AccountId = acc.Id;		
		memberCase.Start_of_Procedure__c = DateTime.now().addHours(-2); 
		memberCase.Procedure_Duration_Implants__c = '03:00';
		memberCase.RecordTypeId = teamCase.RecordTypeId;
		memberCase.Assigned_To__c = member.Member__c;
		memberCase.Activity_Scheduling_Team__c = team.Id;
		memberCase.Status = 'Assigned';		
		cases.add(memberCase);
			
		bl_Case_Trigger.runningSchedulingApp = true;
		//bl_Activity.sendNotificationTOMMX = true;
			
		insert cases;
			
		List<Event> caseAssignments = [Select Id, WhatId, OwnerId from Event where RecordType.DeveloperName = 'Activity_Scheduling'];
		System.assert(caseAssignments.size() == 1);
		System.assert(caseAssignments[0].WhatId == memberCase.Id);
		System.assert(caseAssignments[0].OwnerId == member.Member__c);		
	}
	
	private static testmethod void recurringCase(){
		
		Implant_Scheduling_Team__c team = [Select Id, Team_Group__c from Implant_Scheduling_Team__c];
		Implant_Scheduling_Team_Member__c member = [Select Member__c from Implant_Scheduling_Team_Member__c where Team__c = :team.Id];
			
		Account acc = new Account();
		acc.Name = 'Test Account';
		acc.Account_Country_vs__c = 'NETHERLANDS';
		insert acc;
		
		Case memberCase = new Case();
		memberCase.AccountId = acc.Id;			
		memberCase.Start_of_Procedure__c = DateTime.now().addHours(1); 
		memberCase.Procedure_Duration_Implants__c = '03:00';
		memberCase.RecordTypeId = [Select Id from RecordType where SObjectType = 'Case' AND DeveloperName = 'Implant_Scheduling'].Id;
		memberCase.Activity_Scheduling_Team__c = team.Id;
		memberCase.Assigned_To__c = member.Member__c;
		memberCase.Is_Recurring__c = true;			
		memberCase.Recurring_End_Date__c = memberCase.Start_of_Procedure__c.Date().addDays(28);
		memberCase.Recurring_N_Weeks__c = '1';
		memberCase.Recurring_Week_Days__c = memberCase.Start_of_Procedure__c.format('u');		
		
		bl_Case_Trigger.runningSchedulingApp = true;
		//bl_Activity.sendNotificationTOMMX = true;
			
		insert memberCase;
					
		List<Event> caseAssignments = [Select Id, WhatId, OwnerId from Event where RecordType.DeveloperName = 'Activity_Scheduling' and WhatId = :memberCase.Id];
		System.assert(caseAssignments.size() == 0);
		
		List<Case> instances = [Select Id from Case where ParentId = :memberCase.Id];
		System.assertEquals(instances.size(), 5);
		
		bl_Case_Trigger.alreadyProcessed = new Set<Id>();
		
		memberCase.Recurring_N_Weeks__c = '2';
		update memberCase;
				
		instances = [Select Id from Case where ParentId = :memberCase.Id];
		System.assert(instances.size() == 3);
	}
	
	@TestSetup
	private static void setData() {
		
		Mobile_App__c app = new Mobile_App__c();
		app.Name = 'Activity Scheduling Tool';
		app.Active__c = true;
		app.Unique_Key__c = 'ACT_SCHDL';
		insert app;
		
		Implant_Scheduling_Team__c team = new Implant_Scheduling_Team__c();			
		team.Name = 'Test Team';
		team.Team_Color__c = 'rgb(99, 99, 99)';
		team.Work_Day_End__c = '18';
		team.Work_Day_Start__c = '8';
		team.Working_Days__c = '1:5';			
		team.RecordTypeId = [Select Id from RecordType where SObjectType = 'Implant_Scheduling_Team__c' AND DeveloperName = 'Activity_Scheduling_Team'].Id;			
		insert team;
		
		User otherUser = [Select Id from User where Profile.Name LIKE 'System Administrator%' AND Id != :UserInfo.getUserId() AND isActive = true LIMIT 1];
		
		Implant_Scheduling_Team_Member__c member = new Implant_Scheduling_Team_Member__c();
		member.Team__c = team.Id;
		member.Member__c = otherUser.Id;				
		insert member;
	}
}