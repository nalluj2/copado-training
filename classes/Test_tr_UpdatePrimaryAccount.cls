/** 
  Description - This test class will cover the tr_CampaignMember_UpdatePrimaryAccount
                trigger.
  Author - Priti Jaiswal
  Created Date - 18-01-2013
  
*/
@isTest
public class Test_tr_UpdatePrimaryAccount{
    private static testMethod void test_CampaignMember_UpdatePrimaryAccount(){
        
        Account acc=new Account();
        acc.name='testAccount';
        insert acc;
        
        
        Contact con=new Contact();
        con.Accountid=acc.id;
        con.LastName='testContact';
        con.FirstName = 'TEST';
        con.Contact_Department__c = 'Diabetes Adult';
        con.Contact_Primary_Specialty__c = 'ENT'; 
        con.Affiliation_To_Account__c = 'Employee';
        con.Primary_Job_Title_vs__c = 'Manager';
        con.Contact_Gender__c = 'Male';      
        insert con; 
        
        
        Campaign camp=new Campaign();
        camp.name='testcampaign';
        camp.isActive=true;
        camp.Business_Unit_Picklist__c = 'Diabetes';
        camp.recordtypeid=clsUtil.getRecordTypeByDevName('Campaign', 'Diabetes_Campaign').Id;
        insert camp;
        
        Campaignmember campMember=new Campaignmember();
        campMember.campaignID=camp.id;
        campMember.contactID=con.id;
        insert campMember;
    }
}