//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 11-04-2019
//  Description      : APEX Class - Trigger Handler for Note
//  Change Log       : PMT-22787: New profile EMEA Field Force MITG will replace profiles: EUR Field Force MITG and MEA Field Force MITG
//--------------------------------------------------------------------------------------------------------------------
public class bl_Note_Trigger  {

	//------------------------------------------------------------------------------------------------
	// MITG Related logic
	//	When an Note is created by an User with an MITG User we need to set the field Linked_Notes__c on Account or Opportunity to true.
	//	When an Note is deleted we need to verify if there are still notes related to the Account / Opportunity which have been created by an MITG User and set the field Linked_Notes__c on Account or Opportunity to true or false.
	//------------------------------------------------------------------------------------------------
	public static void createNote_MITG(List<Note> lstTriggerNew){

		if (bl_Trigger_Deactivation.isTriggerDeactivated('Note_createNote_MITG')) return;

		Set<Id> setID_UserProfile_MITG = new Set<Id>();
        	//PMT-22787
        	//New profile EMEA Field Force MITG will replace profiles: EUR Field Force MITG and MEA Field Force MITG
        	setID_UserProfile_MITG.add(clsUtil.getUserProfileId('EMEA Field Force MITG'));
        
        	//PMT-22787
        	//Delete old profile after migration (Later Release)
			setID_UserProfile_MITG.add(clsUtil.getUserProfileId('EUR Field Force MITG'));
			setID_UserProfile_MITG.add(clsUtil.getUserProfileId('MEA Field Force MITG'));

		if (!setID_UserProfile_MITG.contains(UserInfo.getProfileId())) return;

		Set<Id> setID_Account = new Set<Id>();
		Set<Id> setID_Opportunity = new Set<Id>();
		for (Note oNote : lstTriggerNew){
			
			String tParentId = oNote.ParentId;
			if (tParentId.startsWith('001')){
				setID_Account.add(tParentId);
			}else if (tParentId.startsWith('006')){
				setID_Opportunity.add(tParentId);
			}

		}

		if (setID_Account.size() > 0){

			List<Account> lstAccount = [SELECT Id, Linked_Notes__c FROM Account WHERE Id = :setID_Account AND Linked_Notes__c = false];
			
			if (lstAccount.size() > 0){
			
				for (Account oAccount : lstAccount) oAccount.Linked_Notes__c = true;
				update lstAccount;

			}

		}

		if (setID_Opportunity.size() > 0){

			List<Opportunity> lstOpportunity = [SELECT Id, Linked_Notes__c FROM Opportunity WHERE Id = :setID_Opportunity AND Linked_Notes__c = false];
			
			if (lstOpportunity.size() > 0){
			
				for (Opportunity oOpportunity : lstOpportunity) oOpportunity.Linked_Notes__c = true;
				update lstOpportunity;

			}

		}

	}

	public static void deleteNote_MITG(List<Note> lstTriggerOld){

		if (bl_Trigger_Deactivation.isTriggerDeactivated('Note_deleteNote_MITG')) return;

		Set<Id> setID_UserProfile_MITG = new Set<Id>();
        
        	//PMT-22787
        	//New profile EMEA Field Force MITG will replace profiles: EUR Field Force MITG and MEA Field Force MITG
        	setID_UserProfile_MITG.add(clsUtil.getUserProfileId('EMEA Field Force MITG'));
        
        	//PMT-22787
        	//Delete old profile after migration (Later Release)
			setID_UserProfile_MITG.add(clsUtil.getUserProfileId('EUR Field Force MITG'));
			setID_UserProfile_MITG.add(clsUtil.getUserProfileId('MEA Field Force MITG'));

		Set<Id> setID_Account = new Set<Id>();
		Set<Id> setID_Opportunity = new Set<Id>();
		for (Note oNote : lstTriggerOld){

			String tParentId = oNote.ParentId;
			if (tParentId.startsWith('001')){
				setID_Account.add(tParentId);
			}else if (tParentId.startsWith('006')){
				setID_Opportunity.add(tParentId);
			}

		}
		
		if (setID_Account.size() > 0){

			List<Account> lstAccount_Update = new List<Account>();
			List<Account> lstAccount = [SELECT Id, Linked_Notes__c, (SELECT Id FROM Notes WHERE CreatedBy.ProfileId = :setID_UserProfile_MITG) FROM Account WHERE Id = :setID_Account];

			for (Account oAccount : lstAccount){

				if ( (oAccount.Notes.size() == 0) && (oAccount.Linked_Notes__c = true) ){

					oAccount.Linked_Notes__c = false;
					lstAccount_Update.add(oAccount);
				
				}
			}

			if (lstAccount_Update.size() > 0) update lstAccount_Update;
		}


		if (setID_Opportunity.size() > 0){

			List<Opportunity> lstOpportunity_Update = new List<Opportunity>();
			List<Opportunity> lstOpportunity = [SELECT Id, Linked_Notes__c, (SELECT Id FROM Notes WHERE CreatedBy.ProfileId = :setID_UserProfile_MITG) FROM Opportunity WHERE Id = :setID_Opportunity];

			for (Opportunity oOpportunity : lstOpportunity){

				if ( (oOpportunity.Notes.size() == 0) && (oOpportunity.Linked_Notes__c = true) ){

					oOpportunity.Linked_Notes__c = false;
					lstOpportunity_Update.add(oOpportunity);
				
				}
			}

			if (lstOpportunity_Update.size() > 0) update lstOpportunity_Update;
		}

	}
	//------------------------------------------------------------------------------------------------	

}
//--------------------------------------------------------------------------------------------------------------------