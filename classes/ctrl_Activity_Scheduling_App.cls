public with sharing class ctrl_Activity_Scheduling_App {
    
    public static String timeOffColor = '255, 143, 143';
    public static String implantColor = '159, 186, 252';
    public static String serviceColor = '165, 252, 159';
    public static String meetingColor = '255, 255, 240';
    public static String cancelRejectedColor = '199, 199, 199';
    
    @AuraEnabled
    public static List<Team> getTeams() {
    	
    	List<Team> teams = new List<Team>();
    	teams.add(new Team('list', 'Request List', true, 'utility:list'));
    	teams.add(new Team('personal', 'Personal Calendar', false, 'utility:user'));
    	    	
    	for(Implant_Scheduling_Team__c team : [Select Id, Name from Implant_Scheduling_Team__c ORDER BY Name]){
    		
    		Team userTeam = new Team(team.Id, team.Name, false, 'utility:people');    		    		
    		teams.add(userTeam);
    	}
    	
    	return teams;
    }
    
    @AuraEnabled
    public static Integer getOpenRequestsWeek() {
    	
    	DateTime startFilter =  DateTime.newInstance(DateTime.now().date(), Time.newInstance(0, 0, 0, 0));
    	DateTime endFilter = startFilter.addDays(4);
    	
    	List<Case> openRequests = [Select Id FROM Case where RecordType.DeveloperName = 'Implant_Scheduling' AND Status = 'Open' AND Is_Recurring__c = false AND Type != 'Meeting' AND Activity_Scheduling_Team__c IN (Select Id from Implant_Scheduling_Team__c) AND Start_of_Procedure__c >= :startFilter AND Start_of_Procedure__c <= :endFilter];
    	
    	return openRequests.size();
    }
    
    private static List<String> fieldNames = new List<String>{'Procedure_Duration_Implants__c', 'Activity_Type_Picklist__c', 'Procedure__c', 'Origin', 'Priority', 'Venue__c', 'Experience_of_the_Implanter__c'};
    
    @AuraEnabled
    public static Map<String, List<PicklistValue>> getPicklistValues() {
    	
    	String sessionId = UserInfo.getSessionId();
        Id activitySchedulingRT = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Implant_Scheduling').getRecordTypeId();
        
        Http http = new Http();
        Map<String, List<PicklistValue>> result = new Map<String, List<PicklistValue>>();
        
        String instanceURL = Support_Portal_URL__c.getInstance().Instance_URL__c;
        
        HttpRequest req = new HttpRequest();    	    	
		req.setEndpoint(instanceURL + '/services/data/v46.0/ui-api/object-info/Case/picklist-values/' + activitySchedulingRT);
		req.setMethod('GET');
		req.setHeader('Authorization', 'Bearer ' + sessionId);
		req.setTimeout(120000);
					
	    HttpResponse res = http.send(req);
	    
	    PicklistValuesResponse response = (PicklistValuesResponse) JSON.deserialize(res.getBody(), PicklistValuesResponse.class);
        
        for(String fieldName : fieldNames){
        	
		    List<PicklistValue> pickValues = new List<PicklistValue>();        	
		    
		    for(PicklistValue pickValue : response.picklistFieldValues.get(fieldName).values){
		    	
		    	if(fieldName == 'Activity_Type_Picklist__c' && pickValue.value == 'Implant Support') continue;
		    	
		    	pickValues.add(pickValue); 
		    }
		    
		    result.put(fieldName, pickValues);
        }
        
        List<PicklistValue> teams = new List<PicklistValue>();
        teams.add(new PicklistValue(null, ''));
        for(Implant_Scheduling_Team__c team : [Select Id, Name from Implant_Scheduling_Team__c order by Name]){
        	
        	teams.add(new PicklistValue(team.Id, team.Name));	
        }
        
        result.put('Activity_Scheduling_Team__c', teams);
        
        List<PicklistValue> sbus = new List<PicklistValue>();
        sbus.add(new PicklistValue(null, ''));
        
        List<PicklistValue> defaultSBU = new List<PicklistValue>();
        
        for(User_Business_Unit__c sbu : [Select Sub_Business_Unit__c, Sub_Business_Unit__r.Name, Primary__c from User_Business_Unit__c where User__c = :UserInfo.getUserId()]){
        	
        	sbus.add(new PicklistValue(sbu.Sub_Business_Unit__c, sbu.Sub_Business_Unit__r.Name));
        	
        	if(sbu.Primary__c == true) defaultSBU.add(new PicklistValue(sbu.Sub_Business_Unit__c, sbu.Sub_Business_Unit__r.Name));	
        }
        
        result.put('SubBusinessUnits', sbus);        
        result.put('DefaultSBU', defaultSBU);
        
        List<PicklistValue> members = new List<PicklistValue>();
        Set<Id> teamMembers = new Set<Id>();
        for(Implant_Scheduling_Team_Member__c teamMember : [Select Member__c from Implant_Scheduling_Team_Member__c]) teamMembers.add(teamMember.Member__c);
        
        for(User member : [Select Id, Name FROM User where Id IN :teamMembers OR Id = :UserInfo.getUserId() order by Name]){
        	
        	members.add(new PicklistValue(member.Id, member.Name));	
        }
        
        result.put('Assigned_To__c', members);
        
        List<PicklistValue> userDetails = new List<PicklistValue>();
        userDetails.add(new PicklistValue(UserInfo.getName(), UserInfo.getName()));
        userDetails.add(new PicklistValue(UserInfo.getUserType(), UserInfo.getUserType()));
        result.put('UserDetails', userDetails);
        
        return result;
    }
    
    public class PicklistValuesResponse{
    	
    	public Map<String, FieldPicklistValues> picklistFieldValues {get; set;}
    }
    
    public class FieldPicklistValues{
    	
    	public List<PicklistValue> values {get; set;}
    }
    
    public class PicklistValue{
    	
    	@AuraEnabled public String label {get; set;}
    	@AuraEnabled public String value {get; set;}
    	
    	public PicklistValue(String inputValue, String inputLabel){
    		
    		label = inputLabel;
    		value = inputValue;
    	}
    }
    
    public class Team {
    	
    	@AuraEnabled public String id {get; set;}
    	@AuraEnabled public String name {get; set;}
    	@AuraEnabled public Boolean selected {get; set;}
    	@AuraEnabled public String icon {get; set;}
    	
    	public Team(String inputId, String inputName, Boolean inputSelected, String inputIcon){
    		
    		id = inputId;
    		name = inputName;
    		selected = inputSelected;
    		icon = inputIcon;
    	} 	
    }
}