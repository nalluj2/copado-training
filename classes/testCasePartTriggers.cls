@isTest
private Class testCasePartTriggers
{    

	public static testmethod void ValidateSW()
    {    

        clsTestData_MasterData.tCompanyCode = 'EUR';
        clsTestData_MasterData.tBusinessUnitName = 'Cranial Spinal';
        clsTestData_MasterData.createBusinessUnit(true);

		Account acc=new Account();
        acc.SAP_ID__c = '00000000';
		acc.AccountNumber='as';
		acc.Name='abc';
		acc.Account_Country_vs__c='USA';
		acc.CurrencyIsoCode='USD';
		acc.Phone='24107954';
		insert acc;
          
    Contact objContact=new Contact();
      objContact.LastName='Saha';
	  objContact.FirstName = 'TEST';
      objContact.AccountId = acc.id;
      objContact.Contact_Department__c = 'Pediatric';
      objContact.Contact_Primary_Specialty__c = 'Neurology';
      objContact.Affiliation_To_Account__c = 'Employee';
      objContact.Primary_Job_Title_vs__c = 'Nurse';
      objContact.Contact_Gender__c = 'Female';
  	insert objContact;
          
    Product2 objPro=new Product2();
      objPro.Name='abc';
      objPro.Tracking__c = TRUE ;
      objPro.Maintenance_Frequency__c= 12;
      objPro.RecordTypeId = clsUtil.getRecordTypeByDevName('Product2', 'SAP_Product').Id;
      objPro.Business_Unit_ID__c = clsTestData_MasterData.oMain_BusinessUnit.Id;
    insert objPro;
          
		Software__c objSoft=new Software__c();
		objSoft.Asset_Product_Type__c = 'Planning Station';
		objSoft.Name='Planning Station';
		objSoft.Version__c='12';
		objSoft.Software_Product__c=objPro.id;
		objSoft.Available_for_Cases_Part__c = true;
		insert objSoft;
          
        Asset sys=new Asset();
        sys.Name='Niha';
        sys.Serial_Nr__c = 'XXXXX';
        sys.Asset_Product_Type__c='Planning Station';
        sys.AccountId=acc.id;
        //sys.Service_Contract__c=servicecontract.id;
        sys.Ownership_Status__c='EOL';
        sys.recordtypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('O-Arm').getRecordTypeId(); 
        insert sys;
          
          
        Workorder__c WO =new Workorder__c();        
        WO.Account__c=acc.id;
        WO.Asset__c=sys.id;
        WO.CurrencyIsoCode='EUR';
        WO.Bio_Med_FSE_Visit__c='No';
        WO.Date_Completed__c=system.today();
        WO.Status__c='In Process';
        WO.recordtypeId = Schema.SObjectType.Workorder__c.getRecordTypeInfosByName().get('O1 System PM').getRecordTypeId(); 
        insert WO;
        
        //Exlude execution of OMA triggers
    	clsUtil.bDoNotExecute = true;
            
        Case objcase=new Case();
        objcase.Subject='abc';
        objcase.Description='abc';
        objcase.ContactId=objContact.id;
        objcase.AccountId=acc.id;
        objcase.AssetId=sys.id;
        objcase.Status='Open';
        objcase.RecordTypeId = clsUtil.getRecordTypeByDevName('Case', 'New_Case').Id;
        insert objcase;
        
        Test.startTest();   
            
        Complaint__c objcomplaint=new Complaint__c();
        objcomplaint.Status__c='Open';
        objcomplaint.Account_Name__c=acc.id;
        objcomplaint.Asset__c=sys.id;
        objcomplaint.Medtronic_Aware_Date__c = system.today();
        objcomplaint.Formal_Investigation_Required__c='No';
        objcomplaint.Formal_Investigation_Justification__c='abc';
        objcomplaint.Methods__c='abc';
        objcomplaint.Results__c='abcc';
        objcomplaint.Case__c=objcase.id;
        insert objcomplaint;
        
        List<Workorder_Sparepart__c> objCPartList=new List<Workorder_Sparepart__c>();   
                    
        Workorder_Sparepart__c objCPart1= new Workorder_Sparepart__c();
        objCPart1.recordtypeId = Schema.SObjectType.Workorder_Sparepart__c.getRecordTypeInfosByName().get('Hardware').getRecordTypeId();
        objCPart1.Owner__c = objContact.id;
        objCPart1.Order_Part__c = objPro.id;
        objCPart1.Order_Status__c = 'Shipped';
        objCPart1.Software__c=objSoft.id;
        objCPart1.Complaint__c=objcomplaint.id;
        objCPart1.Date_Replaced__c=system.today();
        objCPartList.add(objCPart1);  
          
        Workorder_Sparepart__c objCPart= new Workorder_Sparepart__c();
        objCPart.recordtypeId = Schema.SObjectType.Workorder_Sparepart__c.getRecordTypeInfosByName().get('Software').getRecordTypeId(); 
        objCPart.Owner__c = objContact.id;
        objCPart.Order_Part__c = objPro.id;
        objCPart.Order_Status__c = 'Shipped';
        objCPart.case__c = objcase.id; 
        objCPart.Software__c=objSoft.id;
          
        objCPartList.add(objCPart);
         
        insert objCPartList;
        
        System.debug('objCPartList >>>>>>>>>>>>>>' + objCPartList[0].CreatedDate); 
               
        CasePartTriggers.Update_Tracked_Parts(objCPartList);
            
        CasePartTriggers.Associate_Case_Parts(objCPartList);
        CasePartTriggers.ValidateSW(objCPartList);
        
        Test.stopTest();    
	}    
	
	private static testmethod void testSetPrice(){
		

        clsTestData_MasterData.tCompanyCode = 'EUR';
        clsTestData_MasterData.tBusinessUnitName = 'Cranial Spinal';
        clsTestData_MasterData.createBusinessUnit(true);

		ST_RecordType_ProductCode__c rtSTSettings = new ST_RecordType_ProductCode__c();
		rtSTSettings.Name = 'PoleStar System PM';
		rtSTSettings.Product_Code__c = 'SERVICEC1-PM-PS';
		insert rtSTSettings;
		
		Test.startTest();
						
		Product2 testProduct=new Product2();
        testProduct.Name = 'PoleStar';
		testProduct.ProductCode = '000999';
		testProduct.isActive = true;
        testProduct.RecordTypeId = clsUtil.getRecordTypeByDevName('Product2', 'SAP_Product').Id;
        testProduct.Business_Unit_ID__c = clsTestData_MasterData.oMain_BusinessUnit.Id;
				
		Product2 polestarInstProduct=new Product2();
        polestarInstProduct.Name = 'PoleStar System PM';
		polestarInstProduct.ProductCode = 'SERVICEC1-PM-PS';
		polestarInstProduct.MPG_Code_Text__c = 'C1';
		polestarInstProduct.isActive = true;
        polestarInstProduct.RecordTypeId = clsUtil.getRecordTypeByDevName('Product2', 'SAP_Product').Id;
        polestarInstProduct.Business_Unit_ID__c = clsTestData_MasterData.oMain_BusinessUnit.Id;
		
		insert new List<Product2>{testProduct, polestarInstProduct};
     	
  		PricebookEntry standardPrice = new PricebookEntry();
   		standardPrice.Pricebook2Id = Test.getStandardPricebookId();
   		standardPrice.Product2Id = testProduct.Id;
  		standardPrice.UnitPrice = 1000;
  		standardPrice.IsActive = true;
  		standardPrice.CurrencyIsoCode = 'EUR';
  		
  		PricebookEntry instStdPrice = new PricebookEntry();
   		instStdPrice.Pricebook2Id = Test.getStandardPricebookId();
   		instStdPrice.Product2Id = polestarInstProduct.Id;
  		instStdPrice.UnitPrice = 2000;
  		instStdPrice.IsActive = true;
  		instStdPrice.CurrencyIsoCode = 'EUR';
  		
 		insert new List<PricebookEntry>{standardPrice, instStdPrice};
     	
     	Pricebook2 stServicePB = new Pricebook2();
		stServicePB.Name = 'EMEA Service Pricebook';
		stServicePB.isActive = true;		
		insert stServicePB;
     	
     	PricebookEntry productEntry = new PricebookEntry();
     	productEntry.Pricebook2Id = stServicePB.Id;
     	productEntry.Product2Id = testProduct.Id;
     	productEntry.isActive = true;
     	productEntry.UnitPrice = 1000;
     	productEntry.CurrencyIsoCode = 'EUR';
     	     	
     	PricebookEntry instEntry = new PricebookEntry();
     	instEntry.Pricebook2Id = stServicePB.Id;
     	instEntry.Product2Id = polestarInstProduct.Id;
     	instEntry.isActive = true;
     	instEntry.UnitPrice = 2000;
     	instEntry.CurrencyIsoCode = 'EUR';
     	
     	insert new List<PricebookEntry>{productEntry, instEntry};
     	
     	//Account
		Account acc = new Account();	
		acc.SAP_ID__c = '00000000';
        acc.AccountNumber = '0000000';
		acc.Name = 'Syn Test Account';
		acc.Phone = '+123456789';
		acc.BillingCity = 'Minneapolis';
		acc.BillingCountry = 'UNITED STATES';
		acc.BillingState = 'Minnesota';
		acc.BillingStreet = 'street';
		acc.BillingPostalCode = '123456';
		acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non-SAP Account').getRecordTypeId();	
		insert acc;
		
		//Contacts
		Contact cnt = new Contact();
		cnt.AccountId = acc.Id;
		cnt.FirstName = 'Test';
		cnt.LastName = 'Contact';
		cnt.FirstName = 'TEST';
		cnt.Phone = '+123456789';		
		cnt.Email = 'test.contact@gmail.com';
		cnt.MobilePhone = '+123456789';		
		cnt.MailingCity = 'Minneapolis';
		cnt.MailingCountry = 'UNITED STATES';
		cnt.MailingState = 'Minnesota';
		cnt.MailingStreet = 'street';
		cnt.MailingPostalCode = '123456';
		cnt.External_Id__c = 'Test_Contact_Id';	 
		cnt.Contact_Department__c = 'Cardiology';
		cnt.Contact_Primary_Specialty__c = 'Cardiovascular'; 
		cnt.Affiliation_To_Account__c = 'Employee'; 
		cnt.Primary_Job_Title_vs__c = 'Surgeon'; 
		cnt.Contact_Gender__c = 'Male'; 
		cnt.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Generic Contact').getRecordTypeId();	
		insert cnt;
				
		//Asset
		Asset asset = new Asset();
		asset.AccountId = acc.Id;
		asset.Product2Id = testProduct.Id;
		asset.Asset_Product_Type__c = 'PoleStar N-10';
		asset.Ownership_Status__c = 'Purchased';
		asset.Name = '123456789';
		asset.Serial_Nr__c = '123456789';
		asset.Status = 'Installed';
		asset.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('PoleStar').getRecordTypeId();
		insert asset;
		
		//Case
		Workorder__c wo = new Workorder__c();
        wo.Account__c=acc.id;
        wo.Asset__c=asset.id;
        wo.Status__c='In Process';      
        wo.Number_of_Software_Upgraded__c = '2';  
        wo.CurrencyIsoCode = 'EUR';
        wo.RecordTypeId = Schema.SObjectType.Workorder__c.getRecordTypeInfosByName().get('PoleStar System PM').getRecordTypeId();
        insert wo; 
		
		//Case Part
		Workorder_Sparepart__c sparePart = new Workorder_Sparepart__c();
  		sparePart.Workorder__c = wo.Id;
  		sparePart.Account__c = acc.Id;
  		sparePart.Asset__c = asset.Id;
  		sparePart.External_Id__c = 'Test_Case_Part_Id';
  		sparePart.Order_Part__c = testProduct.Id;
  		sparePart.Order_Quantity__c = 5;
  		sparePart.Order_Status__c = 'Pending';
  		sparePart.CurrencyIsoCode = 'EUR';		
  		sparePart.RecordTypeId = Schema.SObjectType.Workorder_Sparepart__c.getRecordTypeInfosByName().get('Hardware').getRecordTypeId();
		insert sparePart; 
		
		sparePart = [Select Order_Part_List_Price__c, Order_Part_Total_Price__c from Workorder_Sparepart__c where Id = :sparepart.Id];
		
		System.assert(sparePart.Order_Part_List_Price__c == 1000);
		System.assert(sparePart.Order_Part_Total_Price__c == 5000);
		
		wo = [Select Order_Parts_Total__c, Work_Order_Value_INTL__c from Workorder__c where Id = :wo.Id];
		
		System.assert(wo.Order_Parts_Total__c == 5000);
		System.assert(wo.Work_Order_Value_INTL__c == 2000);
		
		sparePart.Order_Quantity__c = 3;
		update sparePart; 
		
		wo = [Select Order_Parts_Total__c from Workorder__c where Id = :wo.Id];
		
		System.assert(wo.Order_Parts_Total__c == 3000);
		
		delete sparePart;
		
		wo = [Select Order_Parts_Total__c from Workorder__c where Id = :wo.Id];
		
		System.assert(wo.Order_Parts_Total__c == 0.00);
	}
}