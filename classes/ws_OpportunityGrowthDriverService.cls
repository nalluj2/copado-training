/*
 *      Description : This class exposes methods to create / update / delete an Opportunity Growth Driver 
 *
 *      Author = Jesus Lozano
 */
@RestResource(urlMapping='/OpportunityGrowthDriverService/*')
global class ws_OpportunityGrowthDriverService {
    
    @HttpPost
	global static IFOppGrowthDriverResponse doPost() {
 		
 		RestRequest req = RestContext.request;
			
		System.debug('post called ' + req);
		System.debug('post requestBody ' + req.requestBody);
			
		String body = req.requestBody.toString();
		IFOppGrowthDriverRequest oppGrowthDriverRequest = (IFOppGrowthDriverRequest)System.Json.deserialize(body, IFOppGrowthDriverRequest.class);
			
		System.debug('post requestBody ' + oppGrowthDriverRequest);
		
		IFOppGrowthDriverResponse resp = new IFOppGrowthDriverResponse();	
		
		try{
				
			Opportunity_Growth_Driver__c oppGrowthDriver = oppGrowthDriverRequest.oppGrowthDriver;
			
			resp.oppGrowthDriver = bl_AccountPlanning.saveOpportunityGrowthDriver(oppGrowthDriver);
			resp.id = oppGrowthDriverRequest.id;
	
			resp.success = true;
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
		}
 
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'OpportunityGrowthDriver' ,req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
  			
		return resp;		
	}

	@HttpDelete
	global static IFOppGrowthDriverDeleteResponse doDelete(){
		RestRequest req = RestContext.request;
			
		System.debug('post called ' + req);
		System.debug('post requestBody ' + req.requestBody);
			
		String body = req.requestBody.toString();
		IFOppGrowthDriverDeleteResponse resp = new IFOppGrowthDriverDeleteResponse();
		
		IFOppGrowthDriverDeleteResponse oppGrowthDriverDeleteRequest = (IFOppGrowthDriverDeleteResponse)System.Json.deserialize(body, IFOppGrowthDriverDeleteResponse.class);
			
		System.debug('post requestBody ' + oppGrowthDriverDeleteRequest);
			
		Opportunity_Growth_Driver__c oppGrowthDriver = oppGrowthDriverDeleteRequest.oppGrowthDriver;
		
		List<Opportunity_Growth_Driver__c> oppGrowthDriverToDelete;
		
		if(oppGrowthDriver == null || oppGrowthDriver.mobile_Id__c == null){
			oppGrowthDriverToDelete = new List<Opportunity_Growth_Driver__c>();
		}else{
			oppGrowthDriverToDelete = [select id, mobile_id__c from Opportunity_Growth_Driver__c where Mobile_ID__c = :oppGrowthDriver.Mobile_ID__c];
		}
		
		try{
			
			if (oppGrowthDriverToDelete !=null && oppGrowthDriverToDelete.size()>0 ){	
				
				delete oppGrowthDriverToDelete;
				resp.oppGrowthDriver = oppGrowthDriverToDelete.get(0);
				resp.id = oppGrowthDriverDeleteRequest.id;
				resp.success = true;
			}else{
				resp.oppGrowthDriver = oppGrowthDriver;
				resp.id = oppGrowthDriverDeleteRequest.id;
				resp.message='Opportunity not found';
				resp.success = true;
			}
			
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
		}
  			
			
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'OpportunityGrowthDriverDelete',req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
		
		return resp;
	}
	
	global class IFOppGrowthDriverRequest{
		public String id {get;set;}
		public Opportunity_Growth_Driver__c oppGrowthDriver {get;set;}
	}
	
	global class IFOppGrowthDriverResponse{
		public String id {get;set;}
		public Opportunity_Growth_Driver__c oppGrowthDriver {get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}
	
	global class IFOppGrowthDriverDeleteRequest{
		public String id{get;set;}
		public Opportunity_Growth_Driver__c oppGrowthDriver {get;set;}
	}
		
	global class IFOppGrowthDriverDeleteResponse{
		public String id{get;set;}
		public Opportunity_Growth_Driver__c oppGrowthDriver {get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}	
}