global class ba_Contract_Repository_Inteface implements Schedulable, Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{
	
	private List<Id> attachmentIds;
	
	// Schedulable
    global void execute(SchedulableContext ctx){
        
        List<Contract_Repository_Document_Version__c> pendingDocumentVersions = [Select Id, (select Id from Attachments) from Contract_Repository_Document_Version__c where Marked_for_Deletion__c = false AND Documentum_Id__c = null];
        
        List<Id> attIds = new List<Id>();
        
        for(Contract_Repository_Document_Version__c version : pendingDocumentVersions){
        	
        	if(version.attachments.size() == 1) attIds.add(version.attachments[0].Id);        	
        }
        
        if(attIds.size() > 0){
        	
        	ba_Contract_Repository_Inteface batch = new ba_Contract_Repository_Inteface(attIds); 
        	Database.executeBatch(batch, 1);
        }
    }
	
	public ba_Contract_Repository_Inteface() {}
	
    public ba_Contract_Repository_Inteface(List<Id> inputList) {
        
        this.attachmentIds = inputList;
    }
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		
		return Database.getQueryLocator('Select Id, ParentId from Attachment where Id IN :attachmentIds');
	}
	
	global void execute(Database.BatchableContext BC, List<Attachment> attachments) {
		
		bl_Contract_Repository_Interface.sendNotification(attachments[0]);
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}    
}