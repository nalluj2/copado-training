/**
 * Thisclass contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 *
 * Creation Date :  20090928
 * Description :    Test coverage for Trigger Contact Visit Report
 * 
 * Author :         Tuan Abdeen / ABSI
 */
 
@isTest
private class TEST_triggerVisitReport {

    static testMethod void myUnitTest() {
        System.debug(' ################## ' + 'BEGIN TEST_triggerVisitReport' + ' ################');       
        
        Account acc1 = new Account();
        acc1.Name = 'TEST_triggerVisitReport Test Account 1';
        acc1.Account_Country_vs__c='Austria';      
        insert acc1;                
        List<contact> lstCon=new List<Contact>();
        Contact cont1 = new Contact();
        cont1.LastName = 'TEST_triggerVisitReport Test Contact 1';  
		cont1.FirstName = 'test';
        cont1.AccountId = acc1.Id;  
        cont1.Phone = '0714820303'; 
        cont1.Email ='test1@contact.com';
        cont1.Contact_Department__c = 'Diabetes Adult';
        cont1.Contact_Primary_Specialty__c = 'ENT';
        cont1.Affiliation_To_Account__c = 'Employee';
        cont1.Primary_Job_Title_vs__c = 'Manager';
        cont1.Contact_Gender__c ='Male'; 
        cont1.MailingCountry='Austria';
        //insert cont1;
        lstCon.add(cont1);   
        
        Contact cont2 = new Contact();
        cont2.LastName = 'TEST_triggerVisitReport Test Contact 2';  
		cont2.FirstName = 'test';
        cont2.AccountId = acc1.Id;  
        cont2.Phone = '0482222266'; 
        cont2.Email ='test2@contact.com';
        cont2.Contact_Department__c = 'Diabetes Adult';
        cont2.Contact_Primary_Specialty__c = 'ENT';
        cont2.Affiliation_To_Account__c = 'Employee';
        cont2.Primary_Job_Title_vs__c = 'Manager';
        cont2.Contact_Gender__c ='Male';  
        cont2.MailingCountry='Austria';     
       // insert cont2;   
        lstCon.add(cont2);
        insert lstCon;
         //Insert Company
         Company__c cmpny = new Company__c();
         cmpny.name='TestMedtronic';
         cmpny.CurrencyIsoCode = 'EUR';
         cmpny.Current_day_in_Q1__c=56;
         cmpny.Current_day_in_Q2__c=34; 
         cmpny.Current_day_in_Q3__c=5; 
         cmpny.Current_day_in_Q4__c= 0;   
         cmpny.Current_day_in_year__c =200;
         cmpny.Days_in_Q1__c=56;  
         cmpny.Days_in_Q2__c=34;
         cmpny.Days_in_Q3__c=13;
         cmpny.Days_in_Q4__c=22;
         cmpny.Days_in_year__c =250;
         cmpny.Company_Code_Text__c = 'T31';
         insert cmpny;
         
         DIB_Country__c country=new DIB_Country__c();
         country.name='Austria';
         country.Company__c=cmpny.id;
         country.Country_ISO_Code__c='AG';
         country.Call_Record_Chatter_Post_on_Contact__c =true;
         insert country;
        
        Subject__c subj1 = new Subject__c();
        subj1.name = 'Test Subject 1';
        subj1.Company_ID__c = cmpny.Id;
        insert subj1; 
        
            
        Call_Activity_Type__c cat1 = new Call_Activity_Type__c();
        cat1.name = 'Test CAT1';
        cat1.Company_ID__c = cmpny.id;
        insert cat1; 
                        
        Contact_Visit_Report__c cvr1 = new Contact_Visit_Report__c ();
        
        Call_Records__c vr1 = new Call_Records__c(); 
        //vr1.Type__c = 'Group Visit'; 
        vr1.Call_Date__c = date.newInstance(2011, 10, 20);
        insert vr1;     
        list<Contact_Visit_Report__c > lstCVR=new list<Contact_Visit_Report__c >();
        Contact_Visit_Report__c cd1 = new Contact_Visit_Report__c(); 
        cd1.Call_Records__c = vr1.Id; 
        cd1.Attending_Contact__c = cont1.Id; 
        cd1.Attending_Affiliated_Account__c = acc1.Id; 
       // insert cd1;
       lstCVR.add(cd1);
        
        Contact_Visit_Report__c cd2 = new Contact_Visit_Report__c (); 
        cd2.Call_Records__c = vr1.Id; 
        cd2.Attending_Contact__c = cont2.Id; 
        cd2.Attending_Affiliated_Account__c = acc1.Id; 
        //insert cd2;
        lstCVR.add(cd2);
        insert lstCVR;
        
        Call_Topics__c  sb1 = new Call_Topics__c(); 
        sb1.Call_Records__c = vr1.Id;
        sb1.Call_Activity_Type__c = cat1.Id;
        sb1.Call_Topic_Duration__c = 1.0;
        sb1.Call_Topic_Subjects_Concatendated__c = subj1.Id;
        insert sb1; 
        
        // testing the delete visit report
        delete vr1;

        undelete vr1;
        
        /*
        Implant__c imp = [select MMX_Implant_ID_Text__c from Implant__c where MMX_Implant_ID_Text__c<>null limit 1];
        
        
        vr1.Implant__c = imp.id;
        update vr1;
        
        imp.Call_Record_ID__c = vr1.id;
        update imp;

        try {
            delete vr1;
            //undelete vr1;
        } catch (exception e) { }           
        */
        ApexPages.currentPage().getParameters().put('id',vr1.id);
        ApexPages.StandardController newSct = new ApexPages.Standardcontroller(vr1);
        ControllerCallRecordCustomDelete callController = new ControllerCallRecordCustomDelete(newSct);

        try{
            callController.setCanCloseWindow(true);
            boolean canclose=callController.getCanCloseWindow();
            callController.setShowOkButton(true);
            boolean showButton = callController.getShowOkButton();
            string getwartest = callController.getWarntext();
            callController.DelImplant();
        } catch (exception e) { }
        
        
        Business_Unit__c b1 = new Business_Unit__c();
        b1.Name='CRDM1';
        b1.Business_Unit__c='CRDM1';
        b1.abbreviated_name__c='test_ab2';
        b1.Company__c = cmpny.Id; 
        insert b1;  
        Sub_Business_Units__c sb11 = new Sub_Business_Units__c();
        sb11.Name='Kyphon1';
        sb11.Business_Unit__c=b1.id;        
        insert sb11;
        
        Product2 prd2  = new Product2();
        prd2.Name='testprd222';
        insert prd2;
        
        Therapy_Group__c TG=new Therapy_Group__c();
        TG.Name='Test Therapy Group';
        TG.Sub_Business_Unit__c =sb11.id;
        TG.Company__c = cmpny.Id;
        insert TG;    
        
        Therapy__c th  = new Therapy__c();
        th.name = 'Test CAT11';
        th.Business_Unit__c = b1.Id;
        th.Sub_Business_Unit__c = sb11.Id;
        th.Therapy_Group__c = TG.Id;   
        th.Therapy_Name_Hidden__c = 'Test CAT11';     
        insert th; 

        //delete sb11; 
        //delete b1;
        
        Account acc2 = new Account();
        acc2.Name = 'TEST_triggerVisitReport Test Account 2222'; 
        insert acc2;                

        Segmentation__c sc11 = new Segmentation__c();
        sc11.Account__c = acc1.Id;
        sc11.Therapy__c = th.Id;
        sc11.End__c = System.Today();
        sc11.Start__c = System.Today();
        sc11.Segment__c = 'Maintain';        
        insert sc11;
        
        //sc11.End__c = System.Today()+5;
        //update sc11;
        
        Therapy_Product__c tp = new Therapy_Product__c();
        tp.Therapy__c = th.Id;
        tp.Product__c = prd2.Id;
        
        Call_Topic_Products__c ctp = new Call_Topic_Products__c();
        ctp.Product__c = prd2.Id;
        //insert ctp;
        
        delete prd2;       
        
        /*
        Product2 prd3  = new Product2();
        prd3.Name='testprd222';
        insert prd3;
        
        tp.Product__c = prd3.Id;
        insert tp;
*/
         
        System.debug(' ################## ' + 'END TEST_triggerVisitReport' + ' ################');
    }
}