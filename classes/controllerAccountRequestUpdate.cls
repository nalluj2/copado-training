/**
 * Creation Date :  20090326
 * Description : 	Apex Controller called by the page 'accountRequestUpdate.page' as an extension of the standard controller 'Account'!
 * Author : 		ABSI - MC
 */

public without sharing class controllerAccountRequestUpdate {
		
	private Account acc;	
	public Boolean canCloseWindow {get; set;}
	public Account_Update_Request__c accountRequest {get; set;} 
	
	public controllerAccountRequestUpdate(ApexPages.StandardController sc) {
       
       acc = (Account) sc.getRecord();   
       
       accountRequest = new Account_Update_Request__c();
       accountRequest.Account__c = acc.Id;
       
       canCloseWindow = false;
	}	
	
    public void send(){    	
    	
        if (accountRequest.Account_Update_Reason__c == null){
        	        	
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'A Reason is required'));
                        
        }else{
	        
	        try{
	        	      	
	            insert accountRequest;
	            
	            canCloseWindow = true;	
	            
	        }catch(Exception e){
	        	
	          	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Could not send request. Error: ' + e.getMessage()));	          		          
	        } 	
   		}   		   
    }
}