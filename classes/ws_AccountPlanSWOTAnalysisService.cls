@RestResource(urlMapping='/AccountPlanSWOTService/*')
global with sharing class ws_AccountPlanSWOTAnalysisService {
    
    @HttpPost
	global static String doPost() {
		
 		RestRequest req = RestContext.request;
			
		System.debug('post called '+req);
		System.debug('post requestBody '+req.requestBody);
			
		String body = req.requestBody.toString();
		IFAccountPlanSWOTRequest accountPlanSWOTRequest = (IFAccountPlanSWOTRequest)System.Json.deserialize(body, IFAccountPlanSWOTRequest.class);
			
		System.debug('post requestBody '+accountPlanSWOTRequest);
			
		Account_Plan_SWOT_Analysis__c accountPlanSWOT = accountPlanSWOTRequest.accountPlanSWOT;

		IFAccountPlanSWOTResponse resp = new IFAccountPlanSWOTResponse();

		//We catch all exception to assure that 
		try{
				
			resp.accountPlanSWOT = bl_AccountPlanning.saveAccountPlanSWOT(accountPlanSWOT);
			resp.id = accountPlanSWOTRequest.id;
	
			resp.success = true;
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
		}
 
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'AccountPlanSWOTAnalysis' ,req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
  			
		return System.JSON.serialize(resp);		 			
	}

	@HttpDelete
	global static String doDelete(){
		RestRequest req = RestContext.request;
			
		System.debug('post called '+req);
		System.debug('post requestBody '+req.requestBody);
			
		String body = req.requestBody.toString();
		IFAccountPlanSWOTDeleteRequest accountPlanSWOTDeleteRequest = (IFAccountPlanSWOTDeleteRequest)System.Json.deserialize(body, IFAccountPlanSWOTDeleteRequest.class);
			
		System.debug('post requestBody '+accountPlanSWOTDeleteRequest);
			
		Account_Plan_SWOT_Analysis__c accountPlanSWOT = accountPlanSWOTDeleteRequest.accountPlanSWOT;
		IFAccountPlanSWOTDeleteResponse resp = new IFAccountPlanSWOTDeleteResponse();
		
		List<Account_Plan_SWOT_Analysis__c> accountPlanSWOTsToDelete = [select id, mobile_id__c from Account_Plan_SWOT_Analysis__c where Mobile_ID__c = :accountPlanSWOT.Mobile_ID__c];
		
		try{
			
			if (accountPlanSWOTsToDelete !=null && accountPlanSWOTsToDelete.size()>0 ){	
				
				Account_Plan_SWOT_Analysis__c accountPlanSWOTToDelete = accountPlanSWOTsToDelete.get(0);
				delete accountPlanSWOTToDelete;
				resp.accountPlanSWOT = accountPlanSWOTToDelete;
				resp.id = accountPlanSWOTDeleteRequest.id;
				resp.success = true;
			}else{
				resp.accountPlanSWOT = accountPlanSWOT;
				resp.id = accountPlanSWOTDeleteRequest.id;
				resp.message='Account Plan SWOT Analysis not found';
				resp.success = true;
			}
			
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
		}
		
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'AccountPlanSWOTAnalysisDelete',req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
			
		return System.JSON.serialize(resp);		
	}
	
	
	global class IFAccountPlanSWOTRequest{
		public Account_Plan_SWOT_Analysis__c accountPlanSWOT {get;set;}
		public String id{get;set;}
	}
	
	global class IFAccountPlanSWOTDeleteRequest{
		public Account_Plan_SWOT_Analysis__c accountPlanSWOT {get;set;}
		public String id{get;set;}
	}
		
	global class IFAccountPlanSWOTDeleteResponse{
		public Account_Plan_SWOT_Analysis__c accountPlanSWOT {get;set;}
		public String id{get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}

	global class IFAccountPlanSWOTResponse{
		public Account_Plan_SWOT_Analysis__c accountPlanSWOT {get;set;}
		public String id{get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}
}