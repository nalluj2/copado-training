@isTest
private class Test_Opportunity_BU_Involved {
    
    @testSetup
	private static void setData(){
		
		clsTestData_MasterData.createSubBusinessUnit();		
	}
    
    private static testmethod void testEMEAIHS(){
    	
    	Account acc = new Account();
    	acc.Name = 'IHS Account';
    	acc.Account_Country_vs__c = 'Netherlands';
    	insert acc;
    	
    	Id EUR_IHS_rt = clsUtil.getRecordTypeByDevName('Opportunity', 'EMEA_IHS_Managed_Services').Id;
    	
    	Opportunity opp_EMEA = new Opportunity();
        opp_EMEA.RecordTypeId = EUR_IHS_rt;
        opp_EMEA.Name = 'TEST Opportunity';
        opp_EMEA.AccountId = acc.Id;
        opp_EMEA.CloseDate = Date.today().addMonths(6);
        opp_EMEA.StageName = '0 - Tracking';
        opp_EMEA.Deal_Category__c = 'New Customer';
        opp_EMEA.Origin_Text__c = 'NTG Program';
        opp_EMEA.Type = 'Standard Sale';
        opp_EMEA.Amount = 100000;
                	
    	insert opp_EMEA;
    	
    	Test.startTest();	
    	
    	ctrlExt_Manage_Opportunity_BU_Involved controller = new ctrlExt_Manage_Opportunity_BU_Involved(new ApexPages.StandardController(opp_EMEA));
    	
    	System.assert(controller.oppBUs.size() == 4);
    	System.assert(controller.isBUDetailsMandatory == true);    	
    	System.assert(controller.oppBUs[3].record.BU_Involved__c == 'RTG');
    	
    	controller.oppBUs[3].sBUInvolved[0].selected = true;
    	controller.selectedBU = 'RTG';
    	controller.selectBU();
    	
    	System.assert(controller.oppBUs[3].selected == true);    	    	
    	controller.oppBUs[3].record.BU_Percentage__c = 50;
    	    	    	    	
    	controller.oppBUs[3].sBUInvolved[0].record.Percentage__c = 25;
    	
    	controller.oppBUs[3].sBUInvolved[1].selected = true;
    	controller.oppBUs[3].sBUInvolved[1].record.Percentage__c = 25;
    	
    	    	
    	controller.oppBUs[2].selected = true;
    	controller.oppBUs[2].record.BU_Percentage__c = 50;
    	
    	controller.oppBUs[2].sBUInvolved[0].selected = true;
    	   	
    	PageReference pr = controller.save();
    	
    	System.assert(pr != null);
    	
    	opp_EMEA = [Select BU_Involved__c from Opportunity where Id = :opp_EMEA.Id];
    	
    	System.assert(opp_EMEA.BU_Involved__c.contains('RTG'));
    	System.assert(opp_EMEA.BU_Involved__c.contains('MITG'));
	
		Opportunity_BU_Involved__c rtg = [Select SBUs_Involved__c from Opportunity_BU_Involved__c where Opportunity__c = :opp_EMEA.Id AND BU_Involved__c = 'RTG'];
		System.assert(rtg.SBUs_Involved__c.contains(' - 25%'));
		
		Opportunity_BU_Involved__c mitg = [Select SBUs_Involved__c from Opportunity_BU_Involved__c where Opportunity__c = :opp_EMEA.Id AND BU_Involved__c = 'MITG'];
		System.assert(mitg.SBUs_Involved__c != null);
	
		ctrlExt_Manage_Opportunity_BU_Involved controllerEdit = new ctrlExt_Manage_Opportunity_BU_Involved(new ApexPages.StandardController(opp_EMEA));
		System.assert(controllerEdit.oppBUs.size() == 4);
    	System.assert(controller.oppBUs[3].selected == true); 
    	System.assert(controller.oppBUs[2].selected == true); 
    	
    	controllerEdit.oppBUs[2].selected = false;
    	controllerEdit.selectedBU = 'MITG';
    	controllerEdit.unselectBU();
    	
    	System.assert(controllerEdit.oppBUs[2].sbuInvolved[0].selected == false);
    	
    	pr = controllerEdit.save();    	
    	System.assert(pr == null);
    	    	    	
    	controllerEdit.oppBUs[3].record.BU_Percentage__c = 100;
    	controllerEdit.oppBUs[3].sBUInvolved[0].record.Percentage__c = 100;
    	controllerEdit.oppBUs[3].sBUInvolved[1].selected = false;
    	
    	pr = controllerEdit.save();    	
    	
    	System.assert(pr != null);
    }
    
    private static testmethod void testEMEAIHSLead(){
    	
    	Account acc = new Account();
    	acc.Name = 'IHS Account';
    	acc.Account_Country_vs__c = 'Netherlands';
    	insert acc;
    	
    	Id EUR_IHS_rt = clsUtil.getRecordTypeByDevName('Opportunity', 'EMEA_IHS_Lead').Id;
    	
    	Opportunity opp_EMEA = new Opportunity();
        opp_EMEA.RecordTypeId = EUR_IHS_rt;
        opp_EMEA.Name = 'TEST Opportunity';
        opp_EMEA.AccountId = acc.Id;
        opp_EMEA.CloseDate = Date.today().addMonths(6);
        opp_EMEA.StageName = '0 - Lead';
        opp_EMEA.Deal_Category__c = 'New Customer';
        opp_EMEA.Origin_Text__c = 'NTG Program';
        opp_EMEA.Type = 'Standard Sale';
        opp_EMEA.Amount = 100000;
                	
    	insert opp_EMEA;
    	
    	Test.startTest();	
    	
    	ctrlExt_Manage_Opportunity_BU_Involved controller = new ctrlExt_Manage_Opportunity_BU_Involved(new ApexPages.StandardController(opp_EMEA));
    	
    	System.assert(controller.oppBUs.size() == 4);
    	System.assert(controller.isBUDetailsMandatory == false);    	
    	System.assert(controller.oppBUs[2].record.BU_Involved__c == 'MITG');
    	
    	controller.oppBUs[2].selected = true;
    	controller.oppBUs[2].sBUInvolved[0].selected = true;
    	    	
    	PageReference pr = controller.save();
    	
    	System.assert(pr != null);   
    	
    	opp_EMEA = [Select BU_Involved__c from Opportunity where Id = :opp_EMEA.Id];
    	System.assert(opp_EMEA.BU_Involved__c == null); 	
    }
    
    private static testmethod void testEMEAIHS_Error(){
    	
    	Account acc = new Account();
    	acc.Name = 'IHS Account';
    	acc.Account_Country_vs__c = 'Netherlands';
    	insert acc;
    	
    	Id EUR_IHS_rt = clsUtil.getRecordTypeByDevName('Opportunity', 'EMEA_IHS_Managed_Services').Id;
    	
    	Opportunity opp_EMEA = new Opportunity();
        opp_EMEA.RecordTypeId = EUR_IHS_rt;
        opp_EMEA.Name = 'TEST Opportunity';
        opp_EMEA.AccountId = acc.Id;
        opp_EMEA.CloseDate = Date.today().addMonths(6);
        opp_EMEA.StageName = '0 - Tracking';
        opp_EMEA.Deal_Category__c = 'New Customer';
        opp_EMEA.Origin_Text__c = 'NTG Program';
        opp_EMEA.Type = 'Standard Sale';
        opp_EMEA.Amount = 100000;
        
        Test.startTest();	        	
    	
    	ctrlExt_Manage_Opportunity_BU_Involved controller = new ctrlExt_Manage_Opportunity_BU_Involved(new ApexPages.StandardController(opp_EMEA));
    	
    	System.assert(controller.oppBUs == null);
    	
    	insert opp_EMEA;  	
    	
    	controller = new ctrlExt_Manage_Opportunity_BU_Involved(new ApexPages.StandardController(opp_EMEA));
    	
    	System.assert(controller.oppBUs.size() == 4);
    	System.assert(controller.isBUDetailsMandatory == true);
    	
    	controller.oppBUs[2].selected = true;
    	   	    	
    	PageReference pr = controller.save();    	
    	System.assert(pr == null);
    	
    	controller.oppBUs[0].sBUInvolved[0].selected = true;
    	controller.oppBUs[0].sBUInvolved[1].selected = true;
    	
    	pr = controller.save();    	
    	System.assert(pr == null);
    	
    	controller.oppBUs[0].record.BU_Percentage__c = 50;
    	
    	pr = controller.save();    	
    	System.assert(pr == null);
    	
    	controller.oppBUs[2].sBUInvolved[0].record.Percentage__c = 30;
    	pr = controller.save();    	
    	System.assert(pr == null);
    	
    	opp_EMEA = [Select BU_Involved__c from Opportunity where Id = :opp_EMEA.Id];
    	
    	System.assert(opp_EMEA.BU_Involved__c == null); 
    	
    	controller.oppBUs[2].selected = true;
    	controller.oppBUs[2].SBUInvolved[0].selected = true;
    	
    	controller.oppBUs[3].record.BU_Percentage__c = 50;   	
    	
    	pr = controller.save();   
    	System.assert(pr == null);
    }
    
    private static testmethod void testRedirect(){
    	
    	Account acc = new Account();
    	acc.Name = 'IHS Account';
    	acc.Account_Country_vs__c = 'Netherlands';
    	insert acc;
    	
    	Id EUR_IHS_rt = clsUtil.getRecordTypeByDevName('Opportunity', 'EMEA_IHS_Managed_Services').Id;
    	
    	Opportunity opp_EMEA = new Opportunity();
        opp_EMEA.RecordTypeId = EUR_IHS_rt;
        opp_EMEA.Name = 'TEST Opportunity';
        opp_EMEA.AccountId = acc.Id;
        opp_EMEA.CloseDate = Date.today().addMonths(6);
        opp_EMEA.StageName = '0 - Tracking';
        opp_EMEA.Deal_Category__c = 'New Customer';
        opp_EMEA.Origin_Text__c = 'NTG Program';
        opp_EMEA.Type = 'Standard Sale';
        opp_EMEA.Amount = 100000;
        insert opp_EMEA;
        
        Opportunity_BU_Involved__c buInvolved = new Opportunity_BU_Involved__c();
        
        ctrlExt_Opportunity_BU_Involved controller = new ctrlExt_Opportunity_BU_Involved(new ApexPages.standardController(buInvolved));
        
        PageReference pr = controller.redirectToManageOppBU();
        System.assert(pr == null);
        
        buInvolved.Opportunity__c = opp_EMEA.Id;
        
        controller = new ctrlExt_Opportunity_BU_Involved(new ApexPages.standardController(buInvolved));
        
        pr = controller.redirectToManageOppBU();
        System.assert(pr != null);
    }
}