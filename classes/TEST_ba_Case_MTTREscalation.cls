@isTest
private class TEST_ba_Case_MTTREscalation {
	
	@TestSetup static void createTestData() {

		// Custom Setting Data
		MTTR_Case_Escalation__c oMTTRCaseEscalation = new MTTR_Case_Escalation__c();
			oMTTRCaseEscalation.Name = 'MTTR_5_DAYS';
			oMTTRCaseEscalation.Days__c = 5;
			oMTTRCaseEscalation.Include_Case_Owner_in_TO__c = true;
			oMTTRCaseEscalation.Additional_CC__c = 'testCC@medtronic.com.test';
			oMTTRCaseEscalation.Additional_BCC__c = 'testBCC@medtronic.com.test';
		insert oMTTRCaseEscalation;

		// Products
		Product2 oProduct = new Product2();
			oProduct.Name = 'PoleStar';
			oProduct.ProductCode = '000999';
			oProduct.isActive = true;
		insert oProduct;

		// oAccountount
		Account oAccount = new Account();  
			oAccount.AccountNumber = '0000000';
			oAccount.Name = 'Syn Test oAccountount';
			oAccount.Phone = '+123456789';
			oAccount.BillingCity = 'Minneapolis';
			oAccount.BillingCountry = 'UNITED STATES';
			oAccount.BillingState = 'Minnesota';
			oAccount.BillingStreet = 'street';
			oAccount.BillingPostalCode = '123456';
			oAccount.RecordTypeId = clsUtil.getRecordTypeByDevName('Account', 'NON_SAP_Account').Id;  
			oAccount.SAP_Id__c = 'SAP001';
		insert oAccount;

		// Contact
		Contact oContact = new Contact();
			oContact.FirstName = 'Test';
			oContact.LastName = 'Contact';
			oContact.Phone = '+123456789';   
			oContact.Email = 'test.contact@gmail.com';
			oContact.MobilePhone = '+123456789';   
			oContact.MailingCity = 'Minneapolis';
			oContact.MailingCountry = 'UNITED STATES';
			oContact.MailingState = 'Minnesota';
			oContact.MailingStreet = 'street';
			oContact.MailingPostalCode = '123456';
			oContact.External_Id__c = 'Test_Contact_Id';
			oContact.Contact_Department__c = 'Cardiology';
			oContact.Contact_Primary_Specialty__c = 'Cardiovascular'; 
			oContact.Affiliation_To_Account__c = 'Employee'; 
			oContact.Primary_Job_Title_vs__c = 'Surgeon'; 
			oContact.Contact_Gender__c = 'Male'; 
			oContact.AccountId = oAccount.Id;
			oContact.RecordTypeId = clsUtil.getRecordTypeByDevName('Contact', 'Generic_Contact').Id;
		insert oContact;

		// oAsset
		Asset oAsset = new Asset();
			oAsset.AccountId = oAccount.Id;
			oAsset.Product2Id = oProduct.Id;
			oAsset.Asset_Product_Type__c = 'PoleStar N-10';
			oAsset.Ownership_Status__c = 'Purchased';
			oAsset.Name = '123456789';
			oAsset.Serial_Nr__c = '123456789';
			oAsset.Status = 'Installed';
			oAsset.RecordTypeId = clsUtil.getRecordTypeByDevName('Asset', 'PoleStar').Id;
		insert oAsset;

		// oComplaintlaint
		Complaint__c oComplaint = new Complaint__c();
			oComplaint.Account_Name__c = oAccount.Id;
			oComplaint.Asset__c = oAsset.Id;
			oComplaint.Status__c = 'Open';
			oComplaint.External_Id__c = 'Test_oComplaintlaint_Id';
		insert oComplaint;

		// Case
		Case oCase = new Case();
			oCase.AccountId = oAccount.Id;
			oCase.ContactId = oContact.Id;
			oCase.AssetId = oAsset.Id;
			oCase.RecordTypeId = clsUtil.getRecordTypeByDevName('Case', 'New_Case').Id;
			oCase.External_Id__c = 'Test_Case_Id';          
			oCase.Date_Complaint_Became_Known__c = Date.today().addDays(-15);
			oCase.Complaint_Case__c = 'Yes';
			oCase.Complaint__c = oComplaint.Id;
		insert oCase;

		// Related Workorder
		Workorder__c oWorkorder = new Workorder__c();
			oWorkorder.Account__c =oAccount.Id;
			oWorkorder.Case__c = oCase.Id;
			oWorkorder.Complaint__c = oComplaint.Id;
			oWorkorder.Asset__c = oAsset.Id;
			oWorkorder.Status__c = 'In Process';
			oWorkorder.External_Id__c = 'Test_Rel_Work_Order_Id';
			oWorkorder.RecordTypeId = clsUtil.getRecordTypeByDevName('Workorder__c', 'PoleStar_Checkout').Id;
		insert oWorkorder;

	}

    @isTest static void test_ba_Case_MTTREscalation_Scheduling() {

        Test.startTest();

        //---------------------------------------
        // TEST SCHEDULING
        //---------------------------------------
        string tCRON_EXP = '0 0 0 3 9 ? 2099';
        
        string tJobId = System.schedule('ba_Case_MTTREscalation_Test', tCRON_EXP, new ba_Case_MTTREscalation());

        // Get the information from the CronTrigger API object
        CronTrigger oCronTrigger = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :tJobId];

        // Verify the expressions are the same
        System.assertEquals(tCRON_EXP, oCronTrigger.CronExpression);

        // Verify the job has not run
        System.assertEquals(0, oCronTrigger.TimesTriggered);

        // Verify the next time the job will run
        System.assertEquals('2099-09-03 00:00:00', String.valueOf(oCronTrigger.NextFireTime));
        //---------------------------------------

        Test.stopTest();

    }

	
	@isTest static void test_ba_Case_MTTREscalation() {

		// Create Test Data

		List<Case> lstCase = [SELECT Id, MTTR_Escalated__c FROM Case];
		System.assertEquals(lstCase.size(), 1);
		System.assert(lstCase[0].MTTR_Escalated__c == null);

		// Execute Logic
		Test.startTest();

		ba_Case_MTTREscalation oBatch = new ba_Case_MTTREscalation();
		Database.executebatch(oBatch, 200);

		Test.stopTest();

		// Validate Result
		lstCase = [SELECT Id, MTTR_Escalated__c FROM Case];
		System.assertEquals(lstCase.size(), 1);
		System.assert(lstCase[0].MTTR_Escalated__c == 'MTTR_5_DAYS');

	}
	
}