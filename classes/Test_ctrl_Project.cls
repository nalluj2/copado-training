@isTest
private class Test_ctrl_Project {

    static testMethod void toogleFiltersShouldInitializeFilters() {
        Project__c project = new Project__c();
        project.Project_Manager__c = userinfo.getUserId();
        insert(project);
        
        ApexPages.currentPage().getParameters().put('id', project.Id);      
        
        ctrl_Project controllerToTest = new ctrl_Project();
        
        controllerToTest.toggleFilters();
        
        System.assert(true, controllerToTest.getShowFilters());
        System.assertEquals(1, controllerToTest.getFilters().size());
    }
    
    static testMethod void addFilterShouldAdd1MoreFilter(){
        Project__c project = new Project__c();
        project.Project_Manager__c = userinfo.getUserId();
        insert(project);
        
        ApexPages.currentPage().getParameters().put('id', project.Id);      
        
        ctrl_Project controllerToTest = new ctrl_Project();
        
        controllerToTest.toggleFilters();
        controllerToTest.addFilter();
        
        System.assertEquals(2, controllerToTest.getFilters().size());
    }
    
    static testMethod void getPlotBand2EndForProdShouldReturn85(){
    	Project__c project = insertProject(null,'','','');
        
        ApexPages.currentPage().getParameters().put('id', project.Id);      
        
        ctrl_Project controllerToTest = new ctrl_Project();   
        string result = controllerToTest.getPlotBand2EndForProd();
        
        System.assertEquals('85', result);    
    }
    
    static testMethod void getPlotBand2EndForProdShouldReturn95(){
    	date myDate = date.Today();
		date newDate = mydate.addDays(2);
    	Project__c project = insertProject(newDate,'','',''); 
        
        ApexPages.currentPage().getParameters().put('id', project.Id);      
        
        ctrl_Project controllerToTest = new ctrl_Project();   
        string result = controllerToTest.getPlotBand2EndForProd();
        
        System.assertEquals('95', result);    
    }
    
    static testMethod void getPlotBand2EndForStagingShouldReturn85(){
    	Project__c project = insertProject(null,'','','');
        
        ApexPages.currentPage().getParameters().put('id', project.Id);      
        
        ctrl_Project controllerToTest = new ctrl_Project();   
        string result = controllerToTest.getPlotBand2EndForStaging();
        
        System.assertEquals('85', result);    
    }
    
    static testMethod void getPlotBand2EndForStagingShouldReturn95(){
    	date myDate = date.Today();
		date newDate = mydate.addDays(2);
    	Project__c project = insertProject(newDate,'','',''); 
        
        ApexPages.currentPage().getParameters().put('id', project.Id);      
        
        ctrl_Project controllerToTest = new ctrl_Project();   
        string result = controllerToTest.getPlotBand2EndForStaging();
        
        System.assertEquals('95', result);    
    }
    
    static testMethod void getPlotBand2EndForDevShouldReturn85(){
    	Project__c project = insertProject(null,'','','');
        
        ApexPages.currentPage().getParameters().put('id', project.Id);      
        
        ctrl_Project controllerToTest = new ctrl_Project();   
        string result = controllerToTest.getPlotBand2EndForDev();
        
        System.assertEquals('85', result);    
    }
    
    static testMethod void getPlotBand2EndForDevShouldReturn95(){
    	date myDate = date.Today();
		date newDate = mydate.addDays(2);
    	Project__c project = insertProject(newDate,'','',''); 
        
        ApexPages.currentPage().getParameters().put('id', project.Id);      
        
        ctrl_Project controllerToTest = new ctrl_Project();   
        string result = controllerToTest.getPlotBand2EndForDev();
        
        System.assertEquals('95', result);    
    }
    
    static testMethod void getPlotBand1EndForProdShouldReturn1(){
    	Project__c project = insertProject(null,'','','');
        
        ApexPages.currentPage().getParameters().put('id', project.Id);      
        
        ctrl_Project controllerToTest = new ctrl_Project();   
        string result = controllerToTest.getPlotBand1EndForProd();
        
        System.assertEquals('10', result);    
    }
    
    static testMethod void getPlotBand1EndForProdShouldReturn85(){
    	date myDate = date.Today();
		date newDate = mydate.addDays(2);
    	Project__c project = insertProject(newDate,'','',''); 
        
        ApexPages.currentPage().getParameters().put('id', project.Id);      
        
        ctrl_Project controllerToTest = new ctrl_Project();   
        string result = controllerToTest.getPlotBand1EndForProd();
        
        System.assertEquals('85', result);    
    }
    
    static testMethod void getPlotBand1EndForStagingShouldReturn1(){
    	Project__c project = insertProject(null,'','','');
        
        ApexPages.currentPage().getParameters().put('id', project.Id);      
        
        ctrl_Project controllerToTest = new ctrl_Project();   
        string result = controllerToTest.getPlotBand1EndForStaging();
        
        System.assertEquals('10', result);    
    }
    
    static testMethod void getPlotBand1EndForStagingShouldReturn85(){
    	date myDate = date.Today();
		date newDate = mydate.addDays(2);
    	Project__c project = insertProject(newDate,'','',''); 
        
        ApexPages.currentPage().getParameters().put('id', project.Id);      
        
        ctrl_Project controllerToTest = new ctrl_Project();   
        string result = controllerToTest.getPlotBand1EndForStaging();
        
        System.assertEquals('85', result);    
    }
    
    static testMethod void getPlotBand1EndForDevShouldReturn1(){
    	Project__c project = insertProject(null,'','','');
        
        ApexPages.currentPage().getParameters().put('id', project.Id);      
        
        ctrl_Project controllerToTest = new ctrl_Project();   
        string result = controllerToTest.getPlotBand1EndForDev();
        
        System.assertEquals('10', result);    
    }
    
    static testMethod void getPlotBand1EndForDevShouldReturn85(){
    	date myDate = date.Today();
		date newDate = mydate.addDays(2);
    	Project__c project = insertProject(newDate,'','',''); 
        
        ApexPages.currentPage().getParameters().put('id', project.Id);      
        
        ctrl_Project controllerToTest = new ctrl_Project();   
        string result = controllerToTest.getPlotBand1EndForDev();
        
        System.assertEquals('85', result);    
    }
    
    static testMethod void getDevNeedleShouldReturn0(){
    	Project__c project = insertProject(null,'','',''); 
        
        ApexPages.currentPage().getParameters().put('id', project.Id);      
        
        ctrl_Project controllerToTest = new ctrl_Project();   
        string result = controllerToTest.getDevNeedle();
        
        System.assertEquals('0.0', result);
    }
    
    static testMethod void getDevNeedleShouldReturn100(){
    	Project__c project = insertProject(null,'passed','',''); 
        
        ApexPages.currentPage().getParameters().put('id', project.Id);      
        
        ctrl_Project controllerToTest = new ctrl_Project();   
        string result = controllerToTest.getDevNeedle();
        
        System.assertEquals('100.0', result);
    }
    
    static testMethod void getProdNeedleShouldReturn0(){
    	Project__c project = insertProject(null,'','',''); 
        
        ApexPages.currentPage().getParameters().put('id', project.Id);      
        
        ctrl_Project controllerToTest = new ctrl_Project();   
        string result = controllerToTest.getProdNeedle();
        
        System.assertEquals('0.0', result);
    }
    
    static testMethod void getProdNeedleShouldReturn100(){
    	Project__c project = insertProject(null,'passed','passed',''); 
        
        ApexPages.currentPage().getParameters().put('id', project.Id);      
        
        ctrl_Project controllerToTest = new ctrl_Project();   
        string result = controllerToTest.getProdNeedle();
        
        System.assertEquals('100.0', result);
    }
    
    static testMethod void getStagingNeedleShouldReturn0(){
    	Project__c project = insertProject(null,'','',''); 
        
        ApexPages.currentPage().getParameters().put('id', project.Id);      
        
        ctrl_Project controllerToTest = new ctrl_Project();   
        string result = controllerToTest.getStagingNeedle();
        
        System.assertEquals('0.0', result);
    }
    
    static testMethod void getStagingNeedleShouldReturn100(){
    	Project__c project = insertProject(null,'passed','passed','passed'); 
        
        ApexPages.currentPage().getParameters().put('id', project.Id);      
        
        ctrl_Project controllerToTest = new ctrl_Project();   
        string result = controllerToTest.getStagingNeedle();
        
        System.assertEquals('100.0', result);
    }
    
    static testMethod void createPackageFileShouldRedirectToTheCorrectPage(){
    	Project__c project = insertProject(null,'passed','passed','passed'); 
        
        ApexPages.currentPage().getParameters().put('id', project.Id);      
        
        ctrl_Project controllerToTest = new ctrl_Project();   
        
        //PageReference result = controllerToTest.createPackageFile();
        
        //System.assertEquals(result, result);
    }
    
    private static Project__c insertProject(Date endDate,string devPassed,string prodPassed,string stagingPassed){
    	Project__c project = new Project__c();
    	project.Project_Manager__c = userinfo.getUserId();
    	project.End_Date__c = endDate;
    	insert(project);
        
        Change_Request__c cr = new Change_Request__c();
        cr.Project__r = project;
        cr.Project__c = project.Id;
        cr.Request_Date__c = Date.today();
        cr.Change_Request__c = 'test';
        cr.Requestor_Lookup__c = Userinfo.getUserId();
        insert(cr);
                
        Work_Item__c wi = new Work_Item__c();
        wi.Change_Request__r = cr;
        wi.Status_Test_DEV__c = devPassed;
        wi.Status_Test_PRODUCTION__c = prodPassed;
        wi.Status_Test_STAGING__c = stagingPassed;
        wi.Due_Date__c = Date.today();
        wi.Change_Request__c = cr.Id;
        insert(wi);
        
        Release_Deployment_Component__c rdc= new Release_Deployment_Component__c();
        rdc.Work_Item__r = wi;
        rdc.Work_Item__c = wi.Id;
        rdc.Type__c = 'Data Manipulation'; 
        rdc.Deployment_Type_to_Staging__c = 'Manual';
        rdc.Deletion_Order__c = null;
        rdc.Action_type__c = 'Updated';
        rdc.Object__c = 'Account';
        rdc.Responsible_for_Deployment__c = UserInfo.getUserId();
        insert(rdc);
        
        return project;
    }
}