@isTest public class clsTestData_System {
	
	public static Integer iDIBFiscalPeriod_Year_Start = Date.today().year();
	public static Integer iDIBFiscalPeriod_Year_End = Date.today().year() + 3;
	public static DIB_Fiscal_Period__c oMain_DIBFiscalPeriod;
	public static List<DIB_Fiscal_Period__c> lstDIBFiscalPeriod = new List<DIB_Fiscal_Period__c>();

	public static Integer iRecord_ApplicationUsageDetail = 1;
	public static Application_Usage_Detail__c oMain_ApplicationUsageDetail;
	public static List<Application_Usage_Detail__c> lstApplicationUsageDetail = new List<Application_Usage_Detail__c>();

    public static Integer iRecord_CountryAssignmentRule = 1;
    public static Country_Assignment_Rule__c oMain_CountryAssignmentRule;
    public static List<Country_Assignment_Rule__c> lstCountryAssignmentRule = new List<Country_Assignment_Rule__c>();

    //---------------------------------------------------------------------------------------------------------
    // Create DIB Fiscal Period Data 
    //---------------------------------------------------------------------------------------------------------
    public static List<DIB_Fiscal_Period__c> createDIBFiscalPeriod(){
    	return createDIBFiscalPeriod(true);
    }
    public static List<DIB_Fiscal_Period__c> createDIBFiscalPeriod(Boolean bInsert){

        if (oMain_DIBFiscalPeriod == null){

            lstDIBFiscalPeriod = new List<DIB_Fiscal_Period__c>();

            for (Integer iYear = iDIBFiscalPeriod_Year_Start; iYear <= iDIBFiscalPeriod_Year_End; iYear++){
                String tYear = String.valueOf(iYear);
                String tYear_1 = String.valueOf(iYear - 1);
                DIB_Fiscal_Period__c oDIBFP01 = new DIB_Fiscal_Period__c(Fiscal_Month__c='01', Fiscal_Year__c=tYear, Start_Date__c = Date.valueOf(tYear_1 + '-04-26'), End_Date__c = Date.valueOf(tYear_1 + '-05-23'));
                lstDIBFiscalPeriod.add(oDIBFP01);
                DIB_Fiscal_Period__c oDIBFP02 = new DIB_Fiscal_Period__c(Fiscal_Month__c='02', Fiscal_Year__c=tYear, Start_Date__c = Date.valueOf(tYear_1 + '-05-24'), End_Date__c = Date.valueOf(tYear_1 + '-06-24'));
                lstDIBFiscalPeriod.add(oDIBFP02);
                DIB_Fiscal_Period__c oDIBFP03 = new DIB_Fiscal_Period__c(Fiscal_Month__c='03', Fiscal_Year__c=tYear, Start_Date__c = Date.valueOf(tYear_1 + '-06-28'), End_Date__c = Date.valueOf(tYear_1 + '-07-25'));
                lstDIBFiscalPeriod.add(oDIBFP03);
                DIB_Fiscal_Period__c oDIBFP04 = new DIB_Fiscal_Period__c(Fiscal_Month__c='04', Fiscal_Year__c=tYear, Start_Date__c = Date.valueOf(tYear_1 + '-07-26'), End_Date__c = Date.valueOf(tYear_1 + '-08-22'));
                lstDIBFiscalPeriod.add(oDIBFP04);
                DIB_Fiscal_Period__c oDIBFP05 = new DIB_Fiscal_Period__c(Fiscal_Month__c='05', Fiscal_Year__c=tYear, Start_Date__c = Date.valueOf(tYear_1 + '-08-23'), End_Date__c = Date.valueOf(tYear_1 + '-09-26'));
                lstDIBFiscalPeriod.add(oDIBFP05);
                DIB_Fiscal_Period__c oDIBFP06 = new DIB_Fiscal_Period__c(Fiscal_Month__c='06', Fiscal_Year__c=tYear, Start_Date__c = Date.valueOf(tYear_1 + '-09-27'), End_Date__c = Date.valueOf(tYear_1 + '-10-24'));
                lstDIBFiscalPeriod.add(oDIBFP06);
                DIB_Fiscal_Period__c oDIBFP07 = new DIB_Fiscal_Period__c(Fiscal_Month__c='07', Fiscal_Year__c=tYear, Start_Date__c = Date.valueOf(tYear_1 + '-10-25'), End_Date__c = Date.valueOf(tYear_1 + '-11-21'));
                lstDIBFiscalPeriod.add(oDIBFP07);
                DIB_Fiscal_Period__c oDIBFP08 = new DIB_Fiscal_Period__c(Fiscal_Month__c='08', Fiscal_Year__c=tYear, Start_Date__c = Date.valueOf(tYear_1 + '-11-22'), End_Date__c = Date.valueOf(tYear_1 + '-12-26'));
                lstDIBFiscalPeriod.add(oDIBFP08);
                DIB_Fiscal_Period__c oDIBFP09 = new DIB_Fiscal_Period__c(Fiscal_Month__c='09', Fiscal_Year__c=tYear, Start_Date__c = Date.valueOf(tYear_1 + '-12-27'), End_Date__c = Date.valueOf(tYear + '-01-23'));
                lstDIBFiscalPeriod.add(oDIBFP09);
                DIB_Fiscal_Period__c oDIBFP10 = new DIB_Fiscal_Period__c(Fiscal_Month__c='10', Fiscal_Year__c=tYear, Start_Date__c = Date.valueOf(tYear + '-01-24'), End_Date__c = Date.valueOf(tYear + '-02-20'));
                lstDIBFiscalPeriod.add(oDIBFP10);
                DIB_Fiscal_Period__c oDIBFP11 = new DIB_Fiscal_Period__c(Fiscal_Month__c='11', Fiscal_Year__c=tYear, Start_Date__c = Date.valueOf(tYear + '-02-21'), End_Date__c = Date.valueOf(tYear + '-03-27'));
                lstDIBFiscalPeriod.add(oDIBFP11);
                DIB_Fiscal_Period__c oDIBFP12 = new DIB_Fiscal_Period__c(Fiscal_Month__c='12', Fiscal_Year__c=tYear, Start_Date__c = Date.valueOf(tYear + '-03-28'), End_Date__c = Date.valueOf(tYear + '-04-24'));
                lstDIBFiscalPeriod.add(oDIBFP12);
            }

            insert lstDIBFiscalPeriod;

            oMain_DIBFiscalPeriod = lstDIBFiscalPeriod[0];

        }

        return lstDIBFiscalPeriod;
    }	
    //---------------------------------------------------------------------------------------------------------


    //---------------------------------------------------------------------------------------------------------
    // Create Application Usage Detail Data
    //---------------------------------------------------------------------------------------------------------
    public static List<Application_Usage_Detail__c> createApplicationUsageDetail(){
    	return createApplicationUsageDetail(true);
    }
    public static List<Application_Usage_Detail__c> createApplicationUsageDetail(Boolean bInsert){
    	
    	if (oMain_ApplicationUsageDetail == null){

    		lstApplicationUsageDetail = new List<Application_Usage_Detail__c>();

    		for (Integer i=0; i<iRecord_ApplicationUsageDetail; i++){

				Application_Usage_Detail__c oApplicationUsageDetail = new Application_Usage_Detail__c();				
					oApplicationUsageDetail.Action_Date__c = Date.today();
					oApplicationUsageDetail.Action_Type__c = 'Read';
					oApplicationUsageDetail.Capability__c = 'Login';
					oApplicationUsageDetail.Object__c = 'Login';
					oApplicationUsageDetail.Process_Area__c='SFDC Foundation';
					oApplicationUsageDetail.Source__c ='SFDC';
					oApplicationUsageDetail.User_Id__c = UserInfo.getUserId();
					oApplicationUsageDetail.External_Record_Id__c = '0Yaw000006DmAkVCAV';
					oApplicationUsageDetail.Internal_Record_Id__c = '0Yaw000006DmAkVCAV';
				lstApplicationUsageDetail.add(oApplicationUsageDetail);

    		}

    		if (bInsert) insert lstApplicationUsageDetail;

    		oMain_ApplicationUsageDetail = lstApplicationUsageDetail[0];
    	}

    	return lstApplicationUsageDetail;
    }
    //---------------------------------------------------------------------------------------------------------

}