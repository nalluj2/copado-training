@isTest
private class Test_ws_ProcedureService {
    
    @TestSetup
    private static void generateTestData(){
    	
    	Account acc = new Account();
    	acc.Name = 'UnitTest Account';
    	acc.SAP_Id__c = '0123456789';
    	insert acc;
    	
    	//Insert Company
        Company__c oCompany = new Company__c();
            oCompany.name='TestMedtronic';
            oCompany.CurrencyIsoCode = 'EUR';
            oCompany.Current_day_in_Q1__c=56;
            oCompany.Current_day_in_Q2__c=34; 
            oCompany.Current_day_in_Q3__c=5; 
            oCompany.Current_day_in_Q4__c= 0;   
            oCompany.Current_day_in_year__c =200;
            oCompany.Days_in_Q1__c=56;  
            oCompany.Days_in_Q2__c=34;
            oCompany.Days_in_Q3__c=13;
            oCompany.Days_in_Q4__c=22;
            oCompany.Days_in_year__c =250;
            oCompany.Company_Code_Text__c = 'T16';
        insert oCompany;        
              
       	Business_Unit__c oBU = new Business_Unit__c();
       		oBU.Name = 'Vascular';
       		oBU.Company__c = oCompany.Id;
       		oBU.Abbreviated_Name__c = 'Vascular';
       	insert oBU;

        //Insert SBU        
        Sub_Business_Units__c oSBU1 = new Sub_Business_Units__c();
            oSBU1.name='SBUMedtronic1';
            oSBU1.Business_Unit__c = oBU.id;
            oSBU1.Account_Plan_Default__c = 'Revenue';
            oSBU1.Account_Flag__c = 'Coro_PV__c';
            oSBU1.Contact_Flag__c = 'Coro_PV__c';
        insert oSBU1;

        //Insert Therapy Group
        Therapy_Group__c oTherapyGroup = new Therapy_Group__c();
            oTherapyGroup.Name = 'Therapy Group';
            oTherapyGroup.Sub_Business_Unit__c = oSBU1.Id ;
            oTherapyGroup.Company__c = oCompany.id;      
        insert oTherapyGroup;

        //Insert Therapy
        List<Therapy__c> lstTherapy = new List<Therapy__c>();
        Therapy__c oTherapy1 = new Therapy__c();
            oTherapy1.Name='Therapy 1';
            oTherapy1.Therapy_Group__c = oTherapyGroup.id;
            oTherapy1.Business_Unit__c = oBU.id;
            oTherapy1.Sub_Business_Unit__c = oSBU1.id;
            oTherapy1.Therapy_Name_Hidden__c = 'Therapy';
        lstTherapy.add(oTherapy1);
        
        Therapy__c oTherapy2 = new Therapy__c();
            oTherapy2.Name='Therapy 2';
            oTherapy2.Therapy_Group__c = oTherapyGroup.id;
            oTherapy2.Business_Unit__c = oBU.id;
            oTherapy2.Sub_Business_Unit__c = oSBU1.id;
            oTherapy2.Therapy_Name_Hidden__c = 'Therapy';
        lstTherapy.add(oTherapy2);
        
        insert lstTherapy;
        
        //Insert Product Group
        List<Product_Group__c> lstProductGroup = new List<Product_Group__c>();
        Product_Group__c pg = new Product_Group__c();
	        pg.Name = 'Product Group 1';
	        pg.Therapy_ID__c = oTherapy1.Id;
	        pg.Editable_in__c='Revenue';
        lstProductGroup.add(pg);
                
        Product_Group__c pg2 = new Product_Group__c();
	        pg2.Name = 'Product Group 2';
	        pg2.Therapy_ID__c = oTherapy2.id;
	        pg2.Editable_in__c='Revenue';
        lstProductGroup.add(pg2);
        
        insert lstProductGroup;
    }
    
    private static testmethod void testCreateProcedure(){
    	
    	Account acc = [Select Id from Account];
    	
    	Product_Group__c pg = [Select Id from Product_Group__c LIMIT 1];

        Therapy_Group__c oTherapyGroup = [SELECT Id FROM Therapy_Group__c LIMIT 1];
        
        Test.startTest();
        
        String mobileID = GuidUtil.NewGuid();
        
        Account_Procedure__c procedure = new Account_Procedure__c();
        procedure.Account__c = acc.Id;
        procedure.Product_Group__c = pg.Id;
        procedure.Number_of_Procedures__c = 10;
        procedure.Mobile_ID__c = mobileID;
        procedure.Procedure_Name__c = 'Test Procedure Name';
        procedure.Therapy_Group__c = oTherapyGroup.Id;
        
        JSONGenerator gen = JSON.createGenerator(true);
    	
		gen.writeStartObject();
        gen.writeStringField('id', '12346');
	    gen.writeObjectField('procedure', procedure);
	   
        // Get the JSON string.
        String pretty = gen.getAsString();	    	
		System.debug('JSON ' + pretty);
		
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		    			
        req.requestbody = Blob.valueOf(pretty);
    	
     	req.requestURI = '/services/apexrest/ProcedureService';  
	    req.httpMethod = 'POST';
    	
    	RestContext.request = req;
    	RestContext.response = res;
		
		String resp = ws_ProcedureService.doPost();
		
		System.debug('Response' + resp);
		
		ws_ProcedureService.IFProcedureResponse responseObject = (ws_ProcedureService.IFProcedureResponse) JSON.deserialize(resp, ws_ProcedureService.IFProcedureResponse.class);
		Account_Procedure__c proc = responseObject.procedure;
		
		System.debug('JSON Response'+JSON.serializePretty(responseObject));
		
		System.assert(responseObject.success == true);
		
		List<Account_Procedure__c> procedures = [Select Id from Account_Procedure__c where Mobile_ID__c = :mobileID];
		
		System.assert(procedures.size() == 1);
    }
    
    private static testmethod void testUpdateProcedure(){
    	
    	Account acc = [Select Id from Account];
    	
    	List<Product_Group__c> pgs = [Select Id from Product_Group__c];

        Therapy_Group__c oTherapyGroup = [SELECT Id FROM Therapy_Group__c LIMIT 1];
    	
    	Product_Group__c pg1 = pgs[0];
    	Product_Group__c pg2 = pgs[1];
                        
        String mobileID = GuidUtil.NewGuid();
        
        Account_Procedure__c procedure = new Account_Procedure__c();
        procedure.Account__c = acc.Id;
        procedure.Product_Group__c = pg1.Id;
        procedure.Number_of_Procedures__c = 10;
        procedure.Mobile_ID__c = mobileID; 
        procedure.Procedure_Name__c = 'Test Procedure Name';
        procedure.Therapy_Group__c = oTherapyGroup.Id;
        insert procedure;
        
        Test.startTest();
        
        Account_Procedure__c procedureUpdt = new Account_Procedure__c();
        procedureUpdt.Account__c = acc.Id;
        procedureUpdt.Product_Group__c = pg2.Id;
        procedureUpdt.Number_of_Procedures__c = 15;
        procedureUpdt.Mobile_ID__c = mobileID;
                
        JSONGenerator gen = JSON.createGenerator(true);
    	
		gen.writeStartObject();
        gen.writeStringField('id', '12346');
	    gen.writeObjectField('procedure', procedureUpdt);
	   
        // Get the JSON string.
        String pretty = gen.getAsString();	    	
		System.debug('JSON ' + pretty);
		
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		    			
        req.requestbody = Blob.valueOf(pretty);
    	
     	req.requestURI = '/services/apexrest/ProcedureService';  
	    req.httpMethod = 'POST';
    	
    	RestContext.request = req;
    	RestContext.response = res;
		
		String resp = ws_ProcedureService.doPost();
		
		System.debug('Response' + resp);
		
		ws_ProcedureService.IFProcedureResponse responseObject = (ws_ProcedureService.IFProcedureResponse) JSON.deserialize(resp, ws_ProcedureService.IFProcedureResponse.class);
		Account_Procedure__c proc = responseObject.procedure;
		
		System.debug('JSON Response'+JSON.serializePretty(responseObject));
		
		System.assert(responseObject.success == true);
		
		List<Account_Procedure__c> procedures = [Select Id, Product_Group__c, Number_of_Procedures__c from Account_Procedure__c where Mobile_ID__c = :mobileID];
		
		System.assert(procedures.size() == 1);
		
		System.assert(procedures[0].Product_Group__c == pg2.Id);
		System.assert(procedures[0].Number_of_Procedures__c == 15);
    }
    
    private static testmethod void testCreateProcedure_Error(){
    	    	    	
    	Product_Group__c pg = [Select Id from Product_Group__c LIMIT 1];
        
        Test.startTest();
        
        String mobileID = GuidUtil.NewGuid();
        
        Account_Procedure__c procedure = new Account_Procedure__c();        
        procedure.Product_Group__c = pg.Id;
        procedure.Number_of_Procedures__c = 10;
        procedure.Mobile_ID__c = mobileID;
        
        JSONGenerator gen = JSON.createGenerator(true);
    	
		gen.writeStartObject();
        gen.writeStringField('id', '12346');
	    gen.writeObjectField('procedure', procedure);
	   
        // Get the JSON string.
        String pretty = gen.getAsString();	    	
		System.debug('JSON ' + pretty);
		
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		    			
        req.requestbody = Blob.valueOf(pretty);
    	
     	req.requestURI = '/services/apexrest/ProcedureService';  
	    req.httpMethod = 'POST';
    	
    	RestContext.request = req;
    	RestContext.response = res;
		
		String resp = ws_ProcedureService.doPost();
		
		System.debug('Response' + resp);
		
		ws_ProcedureService.IFProcedureResponse responseObject = (ws_ProcedureService.IFProcedureResponse) JSON.deserialize(resp, ws_ProcedureService.IFProcedureResponse.class);
		Account_Procedure__c proc = responseObject.procedure;
		
		System.debug('JSON Response'+JSON.serializePretty(responseObject));
		
		System.assert(responseObject.success == false);
		
		List<Account_Procedure__c> procedures = [Select Id from Account_Procedure__c where Mobile_ID__c = :mobileID];
		
		System.assert(procedures.size() == 0);
    }
}