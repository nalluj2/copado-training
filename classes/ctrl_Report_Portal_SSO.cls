public with sharing class ctrl_Report_Portal_SSO {
    
    private String portalURL;
    
    public ctrl_Report_Portal_SSO(){
    	
    	Map<String, String> inputParams = ApexPages.currentPage().getParameters();    	
    	String bug = inputParams.get('BUG');
    	
    	portalURL = 'apex/report_portal';
    	
    	if(bug != null && bug != ''){
    		
    		if(bug == 'RTG') portalURL += '_RTG';
    		else if(bug == 'MITG') portalURL += '_MITG';    		
    	}
    }
    
    public PageReference redirectToPortal(){
    	
    	Support_Portal_URL__c baseURL = Support_Portal_URL__c.getInstance();
    	
    	PageReference pr = new PageReference(baseURL.Base_URL__c + portalURL);
    	pr.setRedirect(true);
    	
    	User currentUser = [Select Id, FederationIdentifier from User where Id = :UserInfo.getUserId()];
    	
    	pr.getParameters().put('user', CryptoUtils.encryptText(currentUser.FederationIdentifier));
    	pr.getParameters().put('token', CryptoUtils.encryptText(String.valueOf(DateTime.now().getTime())));
    	
    	return pr;
    }
}