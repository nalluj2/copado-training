public with sharing class ut_String {
    
    public static Boolean getBooleanValue(String value)
    {
    	Boolean returnedValue = null;
    	if (value == 'TRUE') returnedValue = true;
    	else if (value == 'FALSE') returnedValue = false;
    	
    	return returnedValue;    	
    }
    
    public static String getStringValue(Boolean value)
    {
    	String returnedValue = '';
    	if (value)
    		returnedValue = 'TRUE';
    	else if (!value)
    		returnedValue = 'FALSE';
    		
    	return returnedValue;
    	
    }
    
    public static String getStringValue(String value)
    {
    	String returnedValue = '';
    	if (value != null)
    		returnedValue = value;
    		
    	return returnedValue;
    }
    
//    public static String getStringValue(Decimal value)
//    {
//    	String returnedValue = '';
//    	if (value != null)
//    		returnedValue = String.valueOf(value);
    	
//    	return returnedValue;
//    }
    
//    public static String getStringValue(DateTime value)
//    {
//    	String returnedValue = '';
//    	if (value != null)
//    		returnedValue = String.valueOf(value);
    	
//    	return returnedValue;
//    }
    
    // Convert Date into String with UTC format and timezone
    public static String dateToString(Date inputDate)
    {
    	
    	if(inputDate == null) return null;
    	
    	String year = String.valueOf(inputDate.year());
    	String month = addPaddingZeros(String.valueOf(inputDate.month()), 2);
    	String day = addPaddingZeros(String.valueOf(inputDate.day()), 2);
    	
    	return year + month + day;
    }
    
    // Convert String with UTC format and timezone into Date instance
    public static Date stringToDate(String inputString)
    {
    	
    	if(inputString == null || inputString == '' || inputString == '00000000') return null;
    	if(inputString.length() != 8) throw new ws_Exception('Invalid date format: ' + inputString);
    	    	    	
    	Integer year = Integer.valueOf(inputString.substring(0, 4));
    	Integer month = Integer.valueOf(inputString.substring(4, 6));
    	Integer day = Integer.valueOf(inputString.substring(6, 8));
    	    	
    	return Date.newInstance(year, month, day);
    }
    
    // Convert DateTime into String in UTC format and timezone
    public static String datetimeToString(DateTime inputDatetime)
    {
    	
    	if(inputDatetime == null) return null;
    	
    	return inputDatetime.formatGMT('yyyyMMddHHmmss');
    }
    
    public static String datetimeToDateString(DateTime inputDatetime)
    {
    	String returnedValue = datetimeToString(inputDatetime);
    	if (returnedValue != null)
    		returnedValue = returnedValue.substring(0, 8);
    		
    	return returnedValue;
    }
    
    public static String datetimeToTimeString(DateTime inputDatetime)
    {
    	String returnedValue = datetimeToString(inputDatetime);
    	if (returnedValue != null)
    		returnedValue = returnedValue.substring(8);
    		
    	return returnedValue;
    }
    
    public static Date datetimeToDate(DateTime inputDatetime)
    {
    	Date returnedDate = null;
    	if (inputDatetime != null)
    		returnedDate = date.newinstance(inputDatetime.year(), inputDatetime.month(), inputDatetime.day());
    	return returnedDate;
    	
    }
    
    // Convert String with UTC format and timezone into DateTime instance
    public static Datetime stringToDatetime(String inputString)
    {
    	
    	if(inputString == null || inputString == '' || inputString == '00000000000000' || inputString == 'nullnull') return null;
    	if(inputString.length() != 14) throw new ws_Exception('Invalid datetime format: ' + inputString);
    	
    	Integer year = Integer.valueOf(inputString.substring(0, 4));
    	Integer month = Integer.valueOf(inputString.substring(4, 6));
    	Integer day = Integer.valueOf(inputString.substring(6, 8));    	 	
    	Integer hour = Integer.valueOf(inputString.substring(8, 10));
    	Integer minute = Integer.valueOf(inputString.substring(10, 12));
    	Integer second = Integer.valueOf(inputString.substring(12, 14));
    	
    	return Datetime.newInstanceGmt(year, month, day, hour, minute, second);
    }    

    public static String addPaddingZeros(String input, Integer length)
    {
    	
    	while(input.length() < length)
    	{
    		input = '0' + input;
    	}
    	
    	return input;
    }

}