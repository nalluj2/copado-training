@isTest
private class Test_ctrl_SMAX_Interface_Retry {
    
    @testSetup
    private static void generateTestData(){
    	
    	SVMXC__Service_Order__c sOrder = new SVMXC__Service_Order__c();
    	sOrder.SVMX_SAP_Service_Order_No__c = '00000401235';
    	insert sOrder;
    	
    	generateNotifications('SVMXC__Service_Order__c', 'ServiceOrder_NotificationSAP', sOrder.Id, 'FAILED', '00000401235');
    	
    	Case sNotification = new Case();
    	sNotification.SVMX_SAP_Notification_Number__c = '000301256446';
    	insert sNotification;
    	
    	generateNotifications('Case', 'Case_NotificationSAP', sNotification.Id, 'SKIPPED', '000301256446');
    	
    	Attachment att = new Attachment();
    	att.ParentId = sNotification.Id;
    	att.Name = 'Test Attachment';
    	att.body = Blob.valueOf('test body');
    	insert att;
    	
    	generateNotifications('Attachment', 'Attachment_NotificationSAP', att.Id, 'NEW', '');
    	
    	SVMXC__Installed_Product__c instProd = new SVMXC__Installed_Product__c();
    	instProd.SVMX_SAP_Equipment_ID__c = '000001223565487965';
    	insert instProd;
    	
    	generateNotifications('SVMXC__Installed_Product__c', 'InstalledProduct_NotificationSAP', instProd.Id, 'RETRY', '000001223565487965');
    	
    	Installed_Product_Measures__c prodMeasure = new Installed_Product_Measures__c();
    	prodMeasure.SAP_Measure_Point_ID__c = '00544687';
    	prodMeasure.Installed_Product__c = instProd.Id;
    	prodMeasure.Active__c = true;
    	prodMeasure.Description__c = 'Unit Test Measurement Value';
    	prodMeasure.Last_Recorded_Timestamp__c = Datetime.now().addDays(-15);
    	prodMeasure.Last_Recorded_Value__c = '20';
    	prodMeasure.New_Recorded_Value__c = '35';    		
		insert prodMeasure;
		
		generateNotifications('Installed_Product_Measures__c', 'InstProductCounter_NotificationSAP', prodMeasure.Id, 'FAILED', '00544687');
    }
    	
    private static void generateNotifications(String objectType, String wsName, Id recordId, String status, String recordSAPId){
    	
       	NotificationSAPLog__c notif = new NotificationSAPLog__c();
    	notif.Record_ID__c = recordId;  
    	notif.Record_SAPID__c = recordSAPId;     	
    	notif.Status__c = status;
    	notif.WM_Process__c = 'Process';
    	notif.WebServiceName__c = wsName;
    	notif.Retries__c = 0;
    	notif.SFDC_Object_Name__c = objectType;    	
    	insert notif;     	
    }
    
    private static testmethod void testRetryAttachment(){
    	
    	Attachment att = [Select Id from Attachment];
    	
    	ctrl_SMAX_Interface_Retry con = new ctrl_SMAX_Interface_Retry();
    	con.recordIds = att.Id;
    	con.objectType = 'Attachment';
    	con.retryRecords();
    	
    	System.assert(con.retryInProcess == true);
    	System.assert(con.retryStatusList.size() == 1);
    	
    	NotificationSAPLog__c notif = [Select Id, Status__c from NotificationSAPLog__c where Record_ID__c = :att.Id Order by CreatedDate DESC, Id DESC LIMIT 1];
    	notif.Status__c = 'FAILED';
    	notif.Outbound_Message_Request__c = '{"SFDC_ID" : "500w000001DXKVyAAP","SAP_ID" : "000300559060","MESSAGE" : null,"DISTRIBUTION_STATUS" : "FALSE","DIST_ERROR_DETAILS" : [ {"ERROR_TYPE" : "E","ERROR_NUMBER" : "264","ERROR_MESSAGE" : "New entry not possible, since notification already completed","ERROR_ID" : "IM"}, {"ERROR_TYPE" : "S","ERROR_NUMBER" : "349","ERROR_MESSAGE" : "Notification long text updated successfully!","ERROR_ID" : "ZSM"}, {"ERROR_TYPE" : "S","ERROR_NUMBER" : "351","ERROR_MESSAGE" : "Notification custom field updated successfully!","ERROR_ID" : "ZSM"} ]}';
    	update notif;
    	
    	con.refreshStatus();
    	
    	System.assert(con.retryInProcess == false);
    	System.assert(con.retryStatusList.size() == 1);
    }
    
    private static testmethod void testRetryInstalledProduct(){
    	
    	SVMXC__Installed_Product__c instProd = [Select Id from SVMXC__Installed_Product__c];
    	
    	ctrl_SMAX_Interface_Retry con = new ctrl_SMAX_Interface_Retry();
    	con.recordIds = instProd.Id;
    	con.objectType = 'SVMXC__Installed_Product__c';
    	con.retryRecords();
    	
    	System.assert(con.retryInProcess == true);
    	System.assert(con.retryStatusList.size() == 1);
    	
    	NotificationSAPLog__c notif = [Select Id, Status__c from NotificationSAPLog__c where Record_ID__c = :instProd.Id Order by CreatedDate DESC, Id DESC LIMIT 1];
    	notif.Status__c = 'COMPLETED';    	
    	update notif;
    	
    	con.refreshStatus();
    	
    	System.assert(con.retryInProcess == false);
    	System.assert(con.retryStatusList.size() == 1);
    }
    
    private static testmethod void testRetryInstalledProductCounter(){
    	
    	Installed_Product_Measures__c prodMeasure = [Select Id from Installed_Product_Measures__c];
    	
    	ctrl_SMAX_Interface_Retry con = new ctrl_SMAX_Interface_Retry();
    	con.recordIds = prodMeasure.Id;
    	con.objectType = 'Installed_Product_Measures__c';
    	con.retryRecords();
    	
    	System.assert(con.retryInProcess == true);
    	System.assert(con.retryStatusList.size() == 1);
    	
    	NotificationSAPLog__c notif = [Select Id, Status__c from NotificationSAPLog__c where Record_ID__c = :prodMeasure.Id Order by CreatedDate DESC, Id DESC LIMIT 1];
    	notif.Status__c = 'COMPLETED';    	
    	update notif;
    	
    	con.refreshStatus();
    	
    	System.assert(con.retryInProcess == false);
    	System.assert(con.retryStatusList.size() == 1);
    }
    
    private static testmethod void testRetryServiceNotification(){
    	
    	Case sNotification = [Select Id from Case];
    	
    	ctrl_SMAX_Interface_Retry con = new ctrl_SMAX_Interface_Retry();
    	con.recordIds = sNotification.Id;
    	con.objectType = 'Case';
    	con.retryRecords();
    	
    	System.assert(con.retryInProcess == true);
    	System.assert(con.retryStatusList.size() == 1);
    	
    	NotificationSAPLog__c notif = [Select Id, Status__c from NotificationSAPLog__c where Record_ID__c = :sNotification.Id Order by CreatedDate DESC, Id DESC LIMIT 1];
    	notif.Status__c = 'RETRY';
    	notif.Error_Description__c = 'Connection Time-out';
    	update notif;
    	
    	con.refreshStatus();
    	
    	System.assert(con.retryInProcess == false);
    	System.assert(con.retryStatusList.size() == 1);
    }
    
    private static testmethod void testRetryServiceOrder(){
    	
    	SVMXC__Service_Order__c sOrder = [Select Id from SVMXC__Service_Order__c];
    	
    	ctrl_SMAX_Interface_Retry con = new ctrl_SMAX_Interface_Retry();
    	con.recordIds = sOrder.Id;
    	con.objectType = 'SVMXC__Service_Order__c';
    	con.retryRecords();
    	
    	System.assert(con.retryInProcess == true);
    	System.assert(con.retryStatusList.size() == 1);
    	
    	NotificationSAPLog__c notif = [Select Id, Status__c from NotificationSAPLog__c where Record_ID__c = :sOrder.Id Order by CreatedDate DESC, Id DESC LIMIT 1];
    	notif.Status__c = 'FAILED';
    	notif.Outbound_Message_Request__c = '{"SFDC_ID" : "500w000001DXKVyAAP","SAP_ID" : "000300559060","MESSAGE" : null,"DISTRIBUTION_STATUS" : "FALSE","DIST_ERROR_DETAILS" : [ {"ERROR_TYPE" : "E","ERROR_NUMBER" : "264","ERROR_MESSAGE" : "New entry not possible, since notification already completed","ERROR_ID" : "IM"}, {"ERROR_TYPE" : "S","ERROR_NUMBER" : "349","ERROR_MESSAGE" : "Notification long text updated successfully!","ERROR_ID" : "ZSM"}, {"ERROR_TYPE" : "S","ERROR_NUMBER" : "351","ERROR_MESSAGE" : "Notification custom field updated successfully!","ERROR_ID" : "ZSM"} ]}';
    	notif.WebServiceName__c = 'ServiceOrderComplete_NotificationSAP';
    	update notif;
    	
    	con.refreshStatus();
    	
    	System.assert(con.retryInProcess == false);
    	System.assert(con.retryStatusList.size() == 1);
    	
    	// Retry for Complete
    	con.retryRecords();
    	System.assert(con.retryInProcess == true);
    	System.assert(con.retryStatusList.size() == 1);
    }
}