@isTest
public with sharing class Test_ctrl_User_Lookup {
	
	private static testmethod void unitTest(){
				
		ctrl_User_Lookup controller = new ctrl_User_Lookup();		
		controller.searchString =  'test';
		
		//Test sosl results
        Test.setFixedSearchResults(new List<Id>{UserInfo.getUserId()});
		
		controller.search();
		
		System.assert(controller.hasResults == true);
		
		controller.clearResults();
		
		System.assert(controller.hasResults == false);
	}
	
}