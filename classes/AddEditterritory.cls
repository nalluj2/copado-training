public class AddEditterritory{
    public string terrId='';
    Territory2 Terr1;
    Territory2 Terr2;
    public Territory2 getTerr1 ()
    { 
        return this.Terr1 ; 
    } 
      
    public Void setTerr1(Territory2 T){ this.Terr1 = T ; }
        
    string Editmode='';
    public string PropEditmode {                
        get { return PropEditmode; 
        }                
        set { 
            PropEditmode = value; 
        }        
    }   
    boolean SelectedChanged;
    public boolean PropSelectedChanged {                
        get { return PropSelectedChanged; 
        }                
        set { 
            PropSelectedChanged = value; 
        }        
    }   

    public list<id> RvAvailableTherapy = new list<id>();
    public list<id> RvSelectedTherapy = new list<id>();
    
    public List<Selectoption> CurrentSelected = new List<Selectoption>();
   	public List<Selectoption> AvailableRecords = new List<Selectoption>();
    public List<Selectoption> UnAvailableRecords = new List<Selectoption>();
    
    public string AddEditMode{get;set;}
    public AddEditterritory(ApexPages.StandardController controller) {
        Terr2 = (Territory2)controller.getRecord();
        terrId=ApexPages.currentPage().getParameters().get('TerId');
        Terr1 = new Territory2(id= terrId);
        if(ApexPages.currentPage().getParameters().get('Edit')!=null)
        {
            Editmode='1';
            PropEditmode='1';
            AddEditMode='Edit Salesforce';
            Territory2 T= [Select Id, Name,Business_Unit__c,CurrencyIsoCode,External_Key__c, Territory2Type.DeveloperName, Country_UID__c,Short_Description__c,Therapy_Groups_Text__c, ParentTerritory2Id,Company__c from Territory2 where Id=:terrId];
            Terr1.Short_Description__c = T.Short_Description__c; 
            Terr2.Short_Description__c = T.Short_Description__c;   
            Terr2.External_Key__c = T.External_Key__c;
            Terr1.Therapy_Groups_Text__c = T.Therapy_Groups_Text__c; 
            Terr2.Business_Unit__c = T.Business_Unit__c;
            Terr2.Country_UID__c = T.Country_UID__c;
            Terr2.Company__c = T.Company__c;                                                                     
        }
        else
        {
            Territory2 T= [Select Id, Name,Business_Unit__c,CurrencyIsoCode, Territory2Type.DeveloperName, Country_UID__c,Short_Description__c,Therapy_Groups_Text__c, ParentTerritory2Id,Company__c from Territory2 where Id=:terrId];
            Terr2.Business_Unit__c = T.Business_Unit__c;
            Terr2.Country_UID__c = T.Country_UID__c;
            Terr2.Company__c = T.Company__c;
            AddEditMode='Add Salesforce';               
        }
        if(AssignedButtonPressed!=true){            
            AssignedButtonPressed=false;        
        } 
//  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  		

        if(Terr1.Therapy_Groups_Text__c!=null){
            List<String> AvlTherapy=new List<String>();
            AvlTherapy = Terr1.Therapy_Groups_Text__c.split(';');           
            list<string> ThgText = new list<string>();
            for(Integer i=0;i<AvlTherapy.size();i++)
            {
                string AvlTherapy1=AvlTherapy.get(i);           
                if(AvlTherapy1!=null && AvlTherapy1.trim()!='')
                {
                    ThgText.add(AvlTherapy1.trim());
                }                           
             }
            Territory2[] currTer=[Select Therapy_Groups_Text__c from Territory2 where Id=:terrId];
            for(Territory2 t1 : currTer)
            {
                AvlTherapy.clear();
                AvlTherapy = string.ValueOf(t1.get('Therapy_Groups_Text__c')).split(';');
                for(Integer j=0;j<AvlTherapy.size();j++)
                {                   
                    string AvlTherapy1=AvlTherapy.get(j);           
                    if(AvlTherapy1!=null && AvlTherapy1.trim()!='')
                    {
                        ThgText.add(AvlTherapy1.trim());
                    }
                }                           
                                
            }                   
            if(ThgText.size()>0){               
                Therapy_Group__c[]  THG = [Select Id,name From Therapy_Group__c where Name in : ThgText and Company__c=:Terr2.Company__c];             
                if(THG.size()>0){    
                    for (Therapy_Group__c T : THG){              
                     CurrentSelected.add(new SelectOption(String.valueOf(T.get('Id')),String.valueOf(T.get('Name'))));
                    }                
                  }     
             }
        }

		fillUnavailable();
		    
    }
    public boolean AssignedButtonPressed {                
        get { return AssignedButtonPressed; 
        }                
        set { 
            AssignedButtonPressed = value; 
        }        
    }   
    
    public String getCrumbs(){
        String crumbs ;
        Boolean levelFound = false;
        crumbs ='';
        Territory2 currTerrId = [Select Id, Territory2Type.DeveloperName from Territory2 where Id =:terrId];
        
        Territory2 allChildren = new Territory2();
        Id tempId = terrId;
            do{
                allChildren = [Select Id, Name,Business_Unit__c, Territory2Type.DeveloperName, Country_UID__c,Short_Description__c,Therapy_Groups_Text__c, ParentTerritory2Id  from Territory2 where Id=:tempId];
                
                If (levelFound){
                    crumbs = allChildren.Name + '  » ' + crumbs;
                }
                else {
                    crumbs = allChildren.Name + '  » ' + crumbs;
                
                }
                tempId = allChildren.ParentTerritory2Id;
                If (allChildren.Territory2Type.DeveloperName == currTerrId.Territory2Type.DeveloperName){
                    levelFound = true;
                }
                
                } while (allChildren.Territory2Type.DeveloperName != 'Region');
        
            // Remove the last >>
            integer crmbIndex = crumbs.lastIndexOf(' »')  ;
            if (crmbIndex !=-1){
                crumbs = crumbs.substring(0,crmbIndex);
            }       
    return crumbs;       
    }   
    String AvailableTherapies;
    public String getAvailableTherapies() { return this.AvailableTherapies; }
    public void setAvailableTherapies(String s) { 
        this.AvailableTherapies = s; 
        }
        
    boolean RemovePressed;      
    public boolean PropRemovePressed {                
        get { return PropRemovePressed; 
        }                
        set { 
            PropRemovePressed = value; 
        }        
    } 
 
     boolean AddPressed;      
    public boolean PropAddPressed{                
        get { return PropAddPressed; 
        }                
        set { 
            PropAddPressed = value; 
        }        
    }    
    
    public PageReference RemoveTherapy(){
    	PropRemovePressed=true; 
    	PropAddPressed=false;   	
    	return null;
    }
    
    public PageReference AddTherapy(){
    	PropRemovePressed=false; 
    	PropAddPressed=true;   	    	
    	return null;
    }    

	public void fillUnavailable(){
      Territory2 currTerr=[Select Business_Unit__c, Country_UID__c, Company__c from Territory2 where Id =:terrId limit 1];
      string BU = currTerr.Business_Unit__c;
      string CU = currTerr.Country_UID__c;
      string CMP = currTerr.Company__c;
      Therapy__c[] TH = [Select Therapy_Group__c From Therapy__c where Business_Unit__r.Name=:BU and Company_Id__c =:CMP and Therapy_Group__c!=null];                           
      if(TH.size()>0){
          list<id> ThgIds = new list<Id>();
          for(Therapy__c thgrp:TH)
          {
            ThgIds.add(thgrp.Therapy_Group__c);                 
          } 
          List<string> unTer=new List<string>();    
          Territory2[] Ter = [select Therapy_Groups_Text__c from Territory2 where Territory2Type.DeveloperName='SalesForce' and Business_Unit__c=:BU and Country_UID__c=:CU and Company__c =: CMP];
          if(Ter.size()>0)
          {
                for(Territory2 t1 : Ter)
                {
                    string t=string.valueOf(t1.get('Therapy_Groups_Text__c'));
                    if(t!=null && t!='')
                    {                                           
                        if(t.contains(';'))
                        {
                            List<String> TerData = t.split(';');
                            if(TerData.size()>0){
                                for(Integer i=0;i<TerData.size();i++)
                                {
                                    string TerData1=TerData.get(i);
                                    unTer.add(TerData1.trim()); 
                                }
                            }
                                                        
                        }
                        else
                        {
                            unTer.add(t.trim());                            
                        }
                    }                       
                }                       
          }
          Therapy_Group__c[]  THG = [Select Id,name From Therapy_Group__c where id in : ThgIds and name in : unTer and Company__c=:Terr2.Company__c];             
          if(THG.size()>0){    
              for (Therapy_Group__c T : THG){              
                  AvailableRecords.add(new SelectOption(String.valueOf(T.get('Id')),String.valueOf(T.get('Name'))));        
            }                
          }
      }
	}
    public List<SelectOption> getUnAvailableTherapy() {  
          List<SelectOption> optionList = new List<SelectOption>();
          if (AvailableRecords.size()>0){
          for (integer i=0; i<AvailableRecords.size(); i++){
          		optionList.add(AvailableRecords[i]);
          }
          }	
    	  
      return optionList;            
    }
      
    public List<SelectOption> getAvailableTherapy() {
      Territory2 currTerr=[Select Business_Unit__c, Country_UID__c, Company__c from Territory2 where Id =:terrId limit 1];
      string BU = currTerr.Business_Unit__c;
      string CU = currTerr.Country_UID__c;
      string CMP = currTerr.Company__c;      
      List<SelectOption> optionList = new List<SelectOption>();      
	  System.Debug('PropAddPressed - ' + PropAddPressed + ' PropRemovePressed - ' + PropRemovePressed);
      if(PropAddPressed==null && PropRemovePressed==null){
	      Therapy__c[] TH = [Select Therapy_Group__c From Therapy__c where Business_Unit__r.Name=:BU and Company_Id__c =:CMP and Therapy_Group__c!=null];
	      System.Debug('Inside If - ' + TH);                           
	      if(TH.size()>0){
	          list<id> ThgIds = new list<Id>();
	          for(Therapy__c thgrp:TH)
	          {
	            ThgIds.add(thgrp.Therapy_Group__c);                 
	          }
	          List<string> unTer=new List<string>();
	          Territory2[] Ter; 
	          Ter = [select Therapy_Groups_Text__c from Territory2 where Territory2Type.DeveloperName='SalesForce' and Business_Unit__c=:BU and Company__c =:CMP and Country_UID__c=:CU];
	          if(Ter.size()>0)
	          {
	                for(Territory2 t1 : Ter)
	                {
	                    string t=string.valueOf(t1.get('Therapy_Groups_Text__c'));
	                    if(t!=null && t!='')
	                    {                       
	                        if(t.contains(';'))
	                        {
	                            List<String> TerData = t.split(';');
	                            if(TerData.size()>0){
	                                for(Integer i=0;i<TerData.size();i++)
	                                {
	                                    string TerData1=TerData.get(i);
	                                    unTer.add(TerData1.trim()); 
	                                }
	                            }
	                                                        
	                        }
	                        else
	                        {
	                            unTer.add(t.trim());                            
	                        }
	                    }                    
	                }                       
	          }
	          
	          System.Debug('>>>>> ThgIds - ' + ThgIds + ' unTer - ' + unTer + ' Company__c - '+ Terr2.Company__c);
	          Therapy_Group__c[]  THG = [Select Id,name From Therapy_Group__c where id in : ThgIds and name not in : unTer and Company__c=:Terr2.Company__c];             
	            
	          if(THG.size()>0){    
	              for (Therapy_Group__c T : THG){              
	                  optionList.add(new SelectOption(String.valueOf(T.get('Id')),String.valueOf(T.get('Name'))));
	                  RvAvailableTherapy.add(String.valueOf(T.get('Id')));        
	            }                
	          }
	      }
      }	
	 
	 	if(PropAddPressed == true){
	 		System.debug('>>>>>>>>>>>>>>Avaialble Therapies RvAvailableTherapy - ' + RvAvailableTherapy);
	 		optionList.clear();	
            List<String> AvlTherapy = AvailableTherapies.split(',');
            list<id> ThgIds = new list<Id>();
            for(Integer i=0;i<AvlTherapy.size();i++)
            {
                string AvlTherapy1=AvlTherapy.get(i);           
                if(AvlTherapy1.contains('[')){
                    AvlTherapy1=AvlTherapy1.replace('[','');
                }
                if(AvlTherapy1.contains(']')){
                    AvlTherapy1=AvlTherapy1.replace(']','');
                }
                if(AvlTherapy1!=null && AvlTherapy1.trim()!='')
                {
                    ThgIds.add(AvlTherapy1.trim());
                }               
            }                   

          Therapy_Group__c[]  THG = [Select Id,name From Therapy_Group__c where id in : RvAvailableTherapy and id not in : ThgIds and Company__c=:Terr2.Company__c];             
  		  RvAvailableTherapy.clear();

	            
          if(THG.size()>0){    
              for (Therapy_Group__c T : THG){              	              
                  optionList.add(new SelectOption(String.valueOf(T.get('Id')),String.valueOf(T.get('Name'))));
                  RvAvailableTherapy.add(String.valueOf(T.get('Id')));        
            }                
          }
	 			
	 	}	 	
	  // If Remove Button is pressed
	  if(PropRemovePressed == true){
		  optionList.clear();	
          Therapy_Group__c[]  THG = [Select Id,name From Therapy_Group__c where id in : RvAvailableTherapy and Company__c=:Terr2.Company__c];             
  		  RvAvailableTherapy.clear();
            
          if(THG.size()>0){    
              for (Therapy_Group__c T : THG){              	              
                  optionList.add(new SelectOption(String.valueOf(T.get('Id')),String.valueOf(T.get('Name'))));
                  RvAvailableTherapy.add(String.valueOf(T.get('Id')));        
            }                
          }
          string AllSelTherapies='';
          if(SeletedTherapies!=null){
            List<String> AvlTherapy = SeletedTherapies.split(',');
            list<id> ThgIds = new list<Id>();
            for(Integer i=0;i<AvlTherapy.size();i++)
            {
                string AvlTherapy1=AvlTherapy.get(i);           
                if(AvlTherapy1.contains('[')){
                    AvlTherapy1=AvlTherapy1.replace('[','');
                }
                if(AvlTherapy1.contains(']')){
                    AvlTherapy1=AvlTherapy1.replace(']','');
                }
                if(AvlTherapy1!=null && AvlTherapy1.trim()!='')
                {
                    ThgIds.add(AvlTherapy1.trim());
                }               
            }                   
            Therapy_Group__c[]  THG1 = [Select Id,name From Therapy_Group__c where id in : ThgIds and Company__c=:Terr2.Company__c];             
            if(THG1.size()>0){    
                for (Therapy_Group__c T : THG1){
                		boolean TheFoundFlag=false;
                		for(integer i=0;i<optionList.size();i++){
                			if(optionList[i].getValue()==String.valueOf(T.get('Id')))
                			{
                				TheFoundFlag=true;	
                				break;
                			}
                		}
                  if(TheFoundFlag==false){
	                  optionList.add(new SelectOption(String.valueOf(T.get('Id')),String.valueOf(T.get('Name'))));
	                  RvAvailableTherapy.add(String.valueOf(T.get('Id')));                                                            	
                  }
                }                
              }     
          }                       
	  }
      return optionList;     
    }
        
    String SeletedTherapies;
    public String getSeletedTherapies() { return this.SeletedTherapies; }
    public void setSeletedTherapies(String s) {
        this.SeletedTherapies = s; 
    }
    public List<SelectOption> getSeletedTherapy() {
      List<SelectOption> optionList = new List<SelectOption>();
      System.Debug('PropAddPressed - ' + PropAddPressed + ' PropRemovePressed- ' + PropRemovePressed + ' AvailableTherapies - ' + AvailableTherapies + ' SeletedTherapies- ' + SeletedTherapies );

		if(PropAddPressed==null && PropRemovePressed==null){
			boolean inlist = false ;
			if (CurrentSelected.size()>0){
				if (optionList.size() > 0){
				for (integer i=0; i<CurrentSelected.size(); i++){
					inlist = false;
					for (integer j=0; j<optionList.size(); j++){
	
						if (CurrentSelected[i].getValue() == optionList[j].getValue()){
							inlist = true;
							break;
						}
					}
					if(inlist == false){	
						optionList.add(CurrentSelected[i]);
	             		if(PropRemovePressed != true){					
							RvSelectedTherapy.add(CurrentSelected[i].getValue());
	             		}
					}
				
				}
				
				}
				else
				{
					for (integer i=0; i<CurrentSelected.size(); i++){
						optionList.add(CurrentSelected[i]);
	             		if(PropRemovePressed != true){					
							RvSelectedTherapy.add(CurrentSelected[i].getValue());
	             		}
					}
				}
			}	
		}
		// When Add Buton is pressed
		if(PropAddPressed == true){
		  optionList.clear();	
          Therapy_Group__c[]  THG = [Select Id,name From Therapy_Group__c where id in : RvSelectedTherapy and Company__c=:Terr2.Company__c]; 
          
	      if(AvailableTherapies!=null){
	        List<String> AvlTherapy=new List<String>();
	        AvlTherapy = AvailableTherapies.split(',');             
	        list<id> ThgIds = new list<Id>();
	        for(Integer i=0;i<AvlTherapy.size();i++)
	        {	        	
	            string AvlTherapy1=AvlTherapy.get(i);           
	            if(AvlTherapy1.contains('[')){
	                AvlTherapy1=AvlTherapy1.replace('[','');
	            }
	            if(AvlTherapy1.contains(']')){
	                AvlTherapy1=AvlTherapy1.replace(']','');
	            }
	            if(AvlTherapy1!=null && AvlTherapy1.trim()!='')
	            {
	                ThgIds.add(AvlTherapy1.trim());
	            }               
	        }                   
	        Therapy_Group__c[]  THG1 = [Select Id,name From Therapy_Group__c where id in : ThgIds and Id Not in : RvSelectedTherapy and Company__c=:Terr2.Company__c];             
                      
  		  RvSelectedTherapy.clear();
            
          if(THG.size()>0){    
              for (Therapy_Group__c T : THG){              	              
                  optionList.add(new SelectOption(String.valueOf(T.get('Id')),String.valueOf(T.get('Name'))));
                  RvSelectedTherapy.add(String.valueOf(T.get('Id')));        
            }                
          }

	        if(THG1.size()>0){    
	            for (Therapy_Group__c T : THG1){                          	
	            	optionList.add(new SelectOption(String.valueOf(T.get('Id')),String.valueOf(T.get('Name'))));             
	             	RvSelectedTherapy.add(String.valueOf(T.get('Id')));
	            }                
	          }     
	      }         
			
		}

	  // If Remove Button is pressed
	  if(PropRemovePressed == true){
		  optionList.clear();	
          Therapy_Group__c[]  THG = [Select Id,name From Therapy_Group__c where id in : RvSelectedTherapy and Company__c=:Terr2.Company__c];             
  		  RvSelectedTherapy.clear();
            
          if(THG.size()>0){    
              for (Therapy_Group__c T : THG){              	              
                  optionList.add(new SelectOption(String.valueOf(T.get('Id')),String.valueOf(T.get('Name'))));
                  RvSelectedTherapy.add(String.valueOf(T.get('Id')));        
            }                
          }
          string AllSelTherapies='';
          if(SeletedTherapies!=null){
            List<String> AvlTherapy = SeletedTherapies.split(',');
            for(Integer i=0;i<AvlTherapy.size();i++)
            {
                string AvlTherapy1=AvlTherapy.get(i);           
                if(AvlTherapy1.contains('[')){
                    AvlTherapy1=AvlTherapy1.replace('[','');
                }
                if(AvlTherapy1.contains(']')){
                    AvlTherapy1=AvlTherapy1.replace(']','');
                }
                if(AvlTherapy1!=null && AvlTherapy1.trim()!='')
                {
            		for(integer j=0;j<optionList.size();j++){
            			if(optionList[j].getValue()==AvlTherapy1.trim())
            			{
            				integer RemoveIndex=-1;
            				System.Debug('>>>>>>>>>>>>>>Size - ' + RvSelectedTherapy.size());	
	                		for(integer k=0;k<RvSelectedTherapy.size();k++){	     
	                			System.Debug('>>>>>>>>>>>>>>K - ' + k);           			
	                			if(optionList[j].getValue()==RvSelectedTherapy.get(k))
	                			{
	                				RemoveIndex=k;	 
            						System.Debug('>>>>>>>>>>>>>>Inside - ' + RemoveIndex);		                				               				
	                				break;	
	                			}
	                		}
	                		if(RemoveIndex>=0){
        						System.Debug('>>>>>>>>>>>>>>Remove - ' + RemoveIndex);		                				               					                			
	                			RvSelectedTherapy.remove(RemoveIndex);	
	                		}
            				optionList.remove(j);	                			                		
	                		break;                					
            			}
            		}	                    
                }               
            }                        
          }                       
	  }
      return optionList;    
    }
        
	public string UserId {        
         get { return UserId; }        
         set { UserId = value; }    
     }

     public string TerId {        
         get { return TerId; }        
         set { TerId = value; }    
     }        
    public PageReference CreateTerritory(){
        if(SeletedTherapies.contains('[]'))
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Selected Therapy can not be empty.'));             
        }
        else
        {
          string AllSelTherapies='';
          if(SeletedTherapies!=null){
            List<String> AvlTherapy = SeletedTherapies.split(',');
            list<id> ThgIds = new list<Id>();
            for(Integer i=0;i<AvlTherapy.size();i++)
            {
                string AvlTherapy1=AvlTherapy.get(i);           
                if(AvlTherapy1.contains('[')){
                    AvlTherapy1=AvlTherapy1.replace('[','');
                }
                if(AvlTherapy1.contains(']')){
                    AvlTherapy1=AvlTherapy1.replace(']','');
                }
                if(AvlTherapy1!=null && AvlTherapy1.trim()!='')
                {
                    ThgIds.add(AvlTherapy1.trim());
                }               
            }                   
            Therapy_Group__c[]  THG = [Select Id,name From Therapy_Group__c where id in : ThgIds and Company__c=:Terr2.Company__c];             
            if(THG.size()>0){    
                for (Therapy_Group__c T : THG){
                    AllSelTherapies=AllSelTherapies+';'+String.valueOf(T.get('Name'));              
                }                
              }     
          }             
            Territory2 Ter=new Territory2(); 
            Territory2 ParTer = [Select Id, Name,Business_Unit__c,CurrencyIsoCode, Territory2Type.DeveloperName, Country_UID__c,Short_Description__c,Therapy_Groups_Text__c, ForecastUserId, ParentTerritory2Id, Company__c  from Territory2 where Id=:terrId];
            if(ParTer!=null && Editmode!='1')
            {               
                string terrId=Terr1.id;
                Ter.Territory2TypeId= [Select Id from Territory2Type where DeveloperName = 'SalesForce'].Id;        
                Ter.ParentTerritory2Id=ParTer.Id;
                Ter.Territory2ModelId = [Select Id from Territory2Model where DeveloperName = 'Medtronic'].Id;
                Ter.Business_Unit__c=ParTer.Business_Unit__c;
                Ter.Country_UID__c=ParTer.Country_UID__c;        
                Ter.Company__c=ParTer.Company__c;  
                Ter.CurrencyIsoCode=ParTer.CurrencyIsoCode;        
                Ter.Therapy_Groups_Text__c = AllSelTherapies;
                Ter.Short_Description__c=Terr2.Short_Description__c;  
                Ter.External_key__c = Terr2.External_key__c;                   
                String abbrName =[select abbreviated_name__c from Business_Unit__c where Name=:ParTer.Business_Unit__c and Company__c =:ParTer.Company__c].abbreviated_name__c;
                Ter.name = Ter.Short_Description__c + ' (SF ' + abbrName + ' ' + Ter.Country_UID__c + ')';
                Ter.DeveloperName = Ter.Short_Description__c.replace(' ', '_') + '_SF_' + abbrName + '_' + Ter.Country_UID__c;
				if(ParTer.ForecastUserId!=null){
		    		Ter.ForecastUserId = ParTer.ForecastUserId;
		    		UserId = Ter.ForecastUserId;
		    	}

                Insert Ter;
				TerId = Ter.Id;                
            }                              
            if(Editmode=='1'){
                string terrId=Terr1.id;
                Terr1.Therapy_Groups_Text__c = AllSelTherapies;
                String abbrName =[select abbreviated_name__c from Business_Unit__c where Name=:Terr2.Business_Unit__c and Company__c =:Terr2.Company__c].abbreviated_name__c;
                Terr1.name = Terr2.Short_Description__c + ' (SF ' + abbrName + ' ' + Terr2.Country_UID__c + ')';
                Terr1.Short_Description__c = Terr2.Short_Description__c;
                Terr1.External_key__c = Terr2.External_key__c;
                
                update Terr1; 
                // Update Child Territories
                Territory2[] allChildren;
                list<Id> tempId=new list<Id>();
                tempId.add(Terr1.id);
                do{
                    allChildren = [Select Id, name, Therapy_Groups_Text__c, ParentTerritory2Id,Territory2Type.DeveloperName, Country_UID__c  from Territory2 where ParentTerritory2Id in :tempId];
                    tempId.clear();
                    for(Territory2 t:allChildren)
                    {
                        tempId.add(t.Id);
                        Territory2 Territory1;
                        Territory1 = new Territory2(id= t.Id);
                        Territory1.Therapy_Groups_Text__c=Terr1.Therapy_Groups_Text__c;
                        integer pos = t.name.lastIndexOf('(');
                        String firstPart = t.Name.substring(0, pos + 3);
                        Territory1.name = firstPart + ' ' + abbrName + ' ' + Terr2.Short_Description__c + ' ' +  t.Country_UID__c + ')';
                        update Territory1;                                  
                    }
                } while (allChildren.size() > 0);
                                
            }
            AssignedButtonPressed=true;                         
                        
        }
        return null;    
    }
}