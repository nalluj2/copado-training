/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 *
 *
 *  Created : 20091208
 *  Description : Test Coverage for accountMGT class.
 *  Author : ABSI / MC
 */
@isTest
private class Test_accountMGT {

    static testMethod void myUnitTest() {
       
       System.debug('####################### BEGIN Test_accountMGT ######################### ') ;
       
       // creating SAP Accounts that will be deleted : 
       
       List<Account> accounts = new List<Account>{} ; 
       for (Integer i = 0 ; i < 10 ; i++){
            Account a = new Account() ; 
            a.Name = 'Test accountMGT Test Coverage Account Name ' + i  ; 
            a.SAP_ID__c = '04556665'+i ; 
            a.RecordTypeId = FinalConstants.sapAccountRecordTypeId ;
            accounts.add(a) ;  
       }
       insert accounts ;
       accountMGT.cannnotDeleteSAPAccount(accounts) ; 
       
       System.debug('####################### END Test_accountMGT ######################### ') ; 
    }
}