@isTest
private class TEST_ctrl_Support_Portal_Header {
	
	@isTest static void test_ctrl_Support_Portal_Header() {

        List<Support_Request_Parameter__c> lstSRP = new List<Support_Request_Parameter__c>();
        Support_Request_Parameter__c oSRP1 = new Support_Request_Parameter__c();
            oSRP1.Active__c = true;
            oSRP1.Key__c = '01511000000Amg8AAC';
            oSRP1.Sequence__c = 1;
            oSRP1.Type__c = 'Cvent - Document';
            oSRP1.Value__c = 'Cvent Test Document';
        lstSRP.add(oSRP1);
        Support_Request_Parameter__c oSRP2 = new Support_Request_Parameter__c();
            oSRP2.Active__c = true;
            oSRP2.Key__c = '01511000000Amg8AAC';
            oSRP2.Sequence__c = 1;
            oSRP2.Type__c = 'BI - Document';
            oSRP2.Value__c = 'BI Test Document';
        lstSRP.add(oSRP2);
        Support_Request_Parameter__c oSRP3 = new Support_Request_Parameter__c();
            oSRP3.Active__c = true;
            oSRP3.Key__c = 'http://www.google.be';
            oSRP3.Sequence__c = 1;
            oSRP3.Type__c = 'Cvent - Link';
            oSRP3.Value__c = 'Google Link';
        lstSRP.add(oSRP3);
        Support_Request_Parameter__c oSRP4 = new Support_Request_Parameter__c();
            oSRP4.Active__c = true;
            oSRP4.Key__c = 'http://www.medtronic.com';
            oSRP4.Sequence__c = 1;
            oSRP4.Type__c = 'BI - Link';
            oSRP4.Value__c = 'Medtronic Link';
        lstSRP.add(oSRP4);
        Support_Request_Parameter__c oSRP5 = new Support_Request_Parameter__c();
            oSRP5.Active__c = true;
            oSRP5.Key__c = 'http://www.google.be';
            oSRP5.Sequence__c = 1;
            oSRP5.Type__c = 'BOT - Link';
            oSRP5.Value__c = 'Google Link';
        lstSRP.add(oSRP5);
        Support_Request_Parameter__c oSRP6 = new Support_Request_Parameter__c();
            oSRP6.Active__c = true;
            oSRP6.Key__c = '01511000000Amg8AAC';
            oSRP6.Sequence__c = 1;
            oSRP6.Type__c = 'BOT - Document';
            oSRP6.Value__c = 'BOT Test Document';
        lstSRP.add(oSRP6);
        insert lstSRP;

        Test.startTest();

		ctrl_Support_Portal_Header oCtrl = new ctrl_Support_Portal_Header();
        List<Support_Request_Parameter__c> lstSRP1 = oCtrl.lstSupportRequestParameter_CventDoc;
        List<Support_Request_Parameter__c> lstSRP2 = oCtrl.lstSupportRequestParameter_BIDoc;
        List<Support_Request_Parameter__c> lstSRP3 = oCtrl.lstSupportRequestParameter_CventLink;
        List<Support_Request_Parameter__c> lstSRP4 = oCtrl.lstSupportRequestParameter_BILink;
        List<Support_Request_Parameter__c> lstSRP5 = oCtrl.lstSupportRequestParameter_BOTLink;
        List<Support_Request_Parameter__c> lstSRP6 = oCtrl.lstSupportRequestParameter_BOTDoc;

        Boolean bHasCventLink = oCtrl.bHasCventLink;
        Boolean bHasBILink = oCtrl.bHasBILink;
        Boolean bHasBOTLink = oCtrl.bHasBOTLink;

        Test.stopTest();

        System.assertEquals(lstSRP1.size(), 1);
        System.assertEquals(lstSRP2.size(), 1);
        System.assertEquals(lstSRP3.size(), 1);
        System.assertEquals(lstSRP4.size(), 1);
        System.assertEquals(lstSRP5.size(), 1);
        System.assertEquals(lstSRP6.size(), 1);
        System.assertEquals(bHasCventLink, true);
        System.assertEquals(bHasBILink, true);
        System.assertEquals(bHasBOTLink, true);

	}
	
}