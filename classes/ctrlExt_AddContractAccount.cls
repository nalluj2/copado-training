//------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 26-03-2019
//  Description      : APEX Controller for VF Page addContractAccount
//  Change Log       : 
//------------------------------------------------------------------------------------------------------------
public class ctrlExt_AddContractAccount  {

    //----------------------------------------------------------------
    // PRIVATE VARIABLES
    //----------------------------------------------------------------
	private List<String> lstSeparator = new List<String>{';', ',', '_'};
	private Set<Id> setID_Account_AlreadyLinkedToContract = new Set<Id>();
    //----------------------------------------------------------------



    //----------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------
    public ctrlExt_AddContractAccount(ApexPages.StandardController oStandardController) {

        oContract = (Contract_xBU__c)oStandardController.getRecord();

        initialize();

    }
    //----------------------------------------------------------------



    //----------------------------------------------------------------
    // PRIVATE METHODS
    //----------------------------------------------------------------
    private void initialize(){

        // Load Data
        oContract =
            [
                SELECT 
                    Id, Name, Account__c, Account__r.SAP_ID__c
					, Contract_Type__c, External_Contract__c, Contract_Number__c, Contract_Version__c, Status__c, Ship_Date__c
					, Account_SAP_Id__c, Additional_Account_SAP_Id_s_lt__c
                FROM 
                    Contract_xBU__c
                WHERE 
                    Id = :oContract.Id
            ];


		setID_Account_AlreadyLinkedToContract = getAccountIdLinkedToContract(oContract.Id);

		lstWRAccount = new List<wrAccount>();

    }


	private Set<Id> getAccountIdLinkedToContract(Id idContract){
		
		Set<Id> setID = new Set<Id>();

		List<Contract_Account__c> lstContractAccount = [SELECT Account__c FROM Contract_Account__c WHERE Contract_xBU__c = :oContract.Id];
		for (Contract_Account__c oContractAccount : lstContractAccount){

			setID.add(oContractAccount.Account__c);
			
		}
	
		return setID;

	}

	private List<wrAccount> loadBuyingGroupAccount(Set<Id> setID_Account_BuyingGroup){

		List<wrAccount> lstWRAccount_BuyingGroup = new List<wrAccount>();
		
		List<Account> lstBuyingGroup = 
			[
				SELECT 
					Id, RecordType.DeveloperName, ParentId, Parent.Name, SAP_ID__c, EUR_Buying_Group_ID_Text__c, Name, Account_City__c
					, (
						SELECT 
							Id, Affiliation_Type__c
							, Affiliation_From_Account__c, Affiliation_From_Account__r.Name, Affiliation_From_Account__r.Account_City__c
							, Affiliation_From_Account__r.SAP_ID__c, Affiliation_From_Account__r.EUR_Buying_Group_ID_Text__c
							, Affiliation_From_Account__r.ParentId, Affiliation_From_Account__r.Parent.Name
						FROM 
							Affiliations1__r
						WHERE 
							RecordType.DeveloperName = 'A2A'
							AND Affiliation_From_Account__c != null
						ORDER BY 
							Affiliation_From_Account__r.Name
					)
				FROM 
					Account
				WHERE 
					Id = :setID_Account_BuyingGroup
			];

		System.debug('**BC** setID_Account_BuyingGroup : ' + setID_Account_BuyingGroup);
		System.debug('**BC** lstBuyingGroup (' + lstBuyingGroup.size() + ' : ' + lstBuyingGroup);

		for (Account oBuyingGroup : lstBuyingGroup){

			// Add Buying Group
			wrAccount oWRAccount_BuyingGroup = new wrAccount(oBuyingGroup, true, false);
				oWRAccount_BuyingGroup.tBuyingGroupId = oBuyingGroup.EUR_Buying_Group_ID_Text__c;
			lstWRAccount_BuyingGroup.add(oWRAccount_BuyingGroup);

			// Add Child Accounts of Buying Group					
			for (Affiliation__c oAffiliation : oBuyingGroup.Affiliations1__r){

				System.debug('**BC** oAffiliation.Affiliation_From_Account__r : ' + oAffiliation.Affiliation_From_Account__r);
				wrAccount oWRAccount = new wrAccount(oAffiliation.Affiliation_From_Account__r, false, true);
					oWRAccount.tBuyingGroupId = oBuyingGroup.EUR_Buying_Group_ID_Text__c;
					oWRAccount.tRelationshipType = oAffiliation.Affiliation_Type__c;
					if (setID_Account_AlreadyLinkedToContract.contains(oAffiliation.Affiliation_From_Account__c)){
						oWRAccount.bSelected = false;
						oWRAccount.bReadOnly = true;
						oWRAccount.tStatus = 'Already linked to the Contract';
					}
				lstWRAccount_BuyingGroup.add(oWRAccount);

			}

		}				

		return lstWRAccount_BuyingGroup;
	}
    //----------------------------------------------------------------



    //----------------------------------------------------------------
    // GETTER & SETTERS
    //----------------------------------------------------------------
    public Contract_xBU__c oContract { get; private set; }
    public List<wrAccount> lstWRAccount { get; private set; }
    public String tProcessingId { get; set; }
    //----------------------------------------------------------------



	//----------------------------------------------------------------
	// ACTIONS
	//----------------------------------------------------------------
	public void searchAccount(){

		List<String> lstID = new List<String>();

		if (String.isBlank(tProcessingId)) return;
		for (String tSeparator : lstSeparator){
			if (tProcessingId.contains(tSeparator)){
				for (String tID : clsUtil.isNull(tProcessingId, '').trim().split(tSeparator)){
					
					tID = tID.trim();
					if (tID.isNumeric()){
						// Make sure that the search ID's are 10 characters by adding 0's at the beginning
						while (tID.length() < 10) tID = '0' + tID;
					}
					lstID.add(tID);

				}
				
			}
		}
		if (lstID.size() == 0){
			tProcessingId = tProcessingId.trim();
			if (tProcessingId.isNumeric()){
				// Make sure that the search ID's are 10 characters by adding 0's at the beginning
				while (tProcessingId.length() < 10) tProcessingId = '0' + tProcessingId;
			}
			lstID.add(tProcessingId);
		}
		System.debug('**BC** lstID : ' + lstID);

		if (lstID.size() > 0){

			Set<String> setID = new Set<String>();
			for (String tID : lstID){
				if (!String.isBlank(tID)) setID.add(tID);
			}

			List<Account> lstAccount_ALL = 
				[
					SELECT 
						Id, RecordType.DeveloperName, ParentId, Parent.Name, SAP_ID__c, EUR_Buying_Group_ID_Text__c, Name, Account_City__c
					FROM 
						Account 
					WHERE 
						SAP_ID__c = :setID 
						OR EUR_Buying_Group_ID_Text__c = :setID
					ORDER BY 
						RecordType.DeveloperName, Name
				];


			
			Set<Id> setID_Account_BuyingGroup = new Set<Id>();
			List<wrAccount> lstWRAccount_Found = new List<wrAccount>();
			List<wrAccount> lstWRAccount_NotFound = new List<wrAccount>();
			List<wrAccount> lstWRAccount_BuyingGroup = new List<wrAccount>();
			Set<String> setID_NotFound = new Set<String>();
			setID_NotFound.addAll(setID);

			for (Account oAccount : lstAccount_ALL){

				if (setID_NotFound.contains(oAccount.SAP_ID__c)) setID_NotFound.remove(oAccount.SAP_ID__c);
				if (setID_NotFound.contains(oAccount.EUR_Buying_Group_ID_Text__c)) setID_NotFound.remove(oAccount.EUR_Buying_Group_ID_Text__c);

				if ( (oAccount.RecordType.DeveloperName == 'CAN_Buying_Group') || (oAccount.RecordType.DeveloperName == 'Buying_Entity') ){
					
					// Collect the Buying Group Id's - they will be added later
					setID_Account_BuyingGroup.add(oAccount.Id);

				}else{
					
					wrAccount oWRAccount_New = new wrAccount(oAccount, false, false);
					if (setID_Account_AlreadyLinkedToContract.contains(oAccount.Id)){
						oWRAccount_New.bSelected = false;
						oWRAccount_New.bReadOnly = true;
						oWRAccount_New.tStatus = 'Already linked to the Contract';
					}
					lstWRAccount_Found.add(oWRAccount_New);

				}

			}

			// Load the Accounts Related to the Buying Groups
			if (setID_Account_BuyingGroup.size() > 0) lstWRAccount_BuyingGroup = loadBuyingGroupAccount(setID_Account_BuyingGroup);

			if (setID_NotFound.size() > 0){
				
				for (String tID : setID_NotFound){

					wrAccount oWRAccount = new wrAccount();
						oWRAccount.tSAPID = tID;
						oWRAccount.tBuyingGroupId = tID;
						oWRAccount.tStatus = 'Not found';
						oWRAccount.bSelected = false;
						oWRAccount.bReadOnly = true;
						oWRAccount.tBGColor = '#FE9E9E';
						oWRAccount.tColor = '#000000';
					lstWRAccount_NotFound.add(oWRAccount);

				}
				
			}

			lstWRAccount = new List<wrAccount>();

			lstWRAccount.addAll(lstWRAccount_NotFound);
			lstWRAccount.addAll(lstWRAccount_Found);
			lstWRAccount.addAll(lstWRAccount_BuyingGroup);

		}

	}


	public PageReference backToContract(){

        PageReference oPageRef = new PageReference('/'+ oContract.Id);
            oPageRef.setRedirect(true);
        return oPageRef;

	}


	public PageReference  addAccountToContract(){

		List<Contract_Account__c> lstContractAccount_Insert = new List<Contract_Account__c>();

		Integer iCounter = 0;
		Map<Integer, wrAccount> mapSelectedWRAccount = new Map<Integer, wrAccount>();
		for (wrAccount oWRAccount : lstWRAccount){

			if (oWRAccount.bSelected && oWRAccount.bBuyingGroup == false){

				mapSelectedWRAccount.put(iCounter, oWRAccount);
				iCounter++;

			}

		}

		for (wrAccount oWRAccount : mapSelectedWRAccount.values()){
			
			Contract_Account__c oContractAccount = new Contract_Account__c();
				oContractAccount.Contract_xBU__c = oContract.Id;
				oContractAccount.Account__c = oWRAccount.idAccount;
			lstContractAccount_Insert.add(oContractAccount);
			
		}

		Boolean bHasError = false;
		if (lstContractAccount_Insert.size() > 0){

			List<Database.SaveResult> lstDatabaseSaveResult = Database.insert(lstContractAccount_Insert, false);

			iCounter = 0;
			for (Database.SaveResult oSaveResult : lstDatabaseSaveResult){

				String tStatus = '';
				wrAccount oWRAccount = mapSelectedWRAccount.get(iCounter);

				if (oSaveResult.isSuccess()){

					tStatus = 'Success';
					oWRAccount.bSelected = false;
					oWRAccount.bReadOnly = true;					
					oWRAccount.tColor = '#2A862A';

				}else{
					bHasError = true;

					tStatus = 'Error : ';
					for (Database.Error oError : oSaveResult.getErrors()){

						tStatus += oError.getMessage();

					}
					oWRAccount.tColor = '#FF0000';

				}

				oWRAccount.tStatus = tStatus;

				iCounter++;

			}

			setID_Account_AlreadyLinkedToContract = getAccountIdLinkedToContract(oContract.Id);
	
		}

		if (bHasError){

			return null;

		}else{

			PageReference oPageRef = new PageReference('/'+ oContract.Id);
				oPageRef.setRedirect(true);
			return oPageRef;

		}
			
	}
	//----------------------------------------------------------------



    //----------------------------------------------------------------
	// WRAPPER / HELPER CLASS
    //----------------------------------------------------------------
    public class wrAccount{

        public Boolean bSelected { get; set; }
		public Boolean bReadOnly { get; set; }
		public Boolean bBuyingGroup { get; set;}

        public Id idAccount { get; set; }
		public String tName { get; set;}
		public String tBuyingGroupId { get; set;}
		public String tSAPID { get; set;}
		public String tCity { get; set;}
		public String tRelationshipType { get; set;}
		public String tParentAccount { get; set;}

        public String tBGColor { get; set; }
        public String tColor { get; set; }
		public String tStatus { get; set; }

        public wrAccount(){

            bSelected = true;
			bReadOnly = false; 
			bBuyingGroup = false;

            idAccount = null;
			tName = '';
			tSAPID = '';
			tBuyingGroupId = '';
			tCity = '';
			tRelationshipType = '';
			tParentAccount = '';

			tBGColor = '';
			tColor = '';
            tStatus = '';

        }

        public wrAccount(Account aoAccount, Boolean abBuyingGroup, Boolean abLinkedToBuyingGroup){

            bSelected = true; 
			bReadOnly = false; 
			bBuyingGroup = abBuyingGroup;

            idAccount = aoAccount.Id;
			tName = aoAccount.Name;
			tSAPID = aoAccount.SAP_ID__c;
			tBuyingGroupId = '';
			tCity = aoAccount.Account_City__c;
			tRelationshipType = '';
			tParentAccount = '';

			if (abLinkedToBuyingGroup){
				tBGColor = '#E3F3FF';
				tColor = '#000000';
			}else{
				if (abBuyingGroup){
					tBuyingGroupId = aoAccount.EUR_Buying_Group_ID_Text__c;
					tBGColor = '#6BD4FF';
					tColor = '#000000';
				}else{
					tBGColor = '';
					tColor = '';
				}
			}
            tStatus = '';

        }

    }
    //----------------------------------------------------------------

}
//------------------------------------------------------------------------------------------------------------