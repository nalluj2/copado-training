/*
 *      Created Date : 23-1-2014
 *      Description : This class centralizes all the logic for the User object
 *
 *      Author = Rudy De Coninck
 */
//------------------------------------------------------------------------------------------------------------------------------
//  Author      : Bart Caelen
//  Date        : 20150326
//  Description : CR-7212
//                  Added method processUserLastLoginReminder
//------------------------------------------------------------------------------------------------------------------------------
//  Author      : Bart Caelen
//  Date        : 20150408
//  Description : CR-8360
//                  Updated method processUserLastLoginReminder
//------------------------------------------------------------------------------------------------------------------------------
//  Author      : Bart Caelen
//  Date        : 20151021
//  Description : CR-8006
//                  Added method updateUserFields
//------------------------------------------------------------------------------------------------------------------------------
public with sharing class bl_User {

	public static List<User>updateUserWithLicense(List<User> usersToProcess){
		
		
		List<Id> userIds = new List<Id>();
		for (User u : usersToProcess){
			userIds.add(u.id);
		}
		
		List<User> users = [SELECT Id, Profile.UserLicense.name FROM User where id in :userIds];
		
		Map<Id,User> enrichedUserInfo = new Map<Id,User>();
		if(users.size()>0){
			for(User u : users){
				enrichedUserInfo.put(u.id,u);
			}
			
	    	for(User userRecord : usersToProcess){
	    		User enrichedUser = enrichedUserInfo.get(userRecord.id); 
	    		String licenseNameString = enrichedUser.Profile.UserLicense.name;
	    		userRecord.License_name__c = licenseNameString;
	    	}
    	}
    	return users;
	}
	
	public static List<User> updateUserWithLastApplicationLoginStatus(List<User> usersToProcess){
		
		List<User> usersToUpdate = new List<User>();
		
		List<Id> userIds = new List<Id>();
		for (User u : usersToProcess){
			DateTime lastAppLogin = u.last_application_login__c;
			String loginStatus='None';
			
			if(lastAppLogin==null){
				if (u.createddate > Date.today()-90){
					loginStatus='Created Within Last 90 Days & Never Logged In';
				}else{
					loginStatus='Never Logged In & Created >90 Days Ago';
				}
			}else{
				if (lastAppLogin <= Date.today()-90){
					loginStatus='Not Logged In Last 90 Days';
				}else{
					if (lastAppLogin <= Date.today()-60){
						loginStatus='Not Logged In Last 60 Days';
					}else{
						if (lastAppLogin <= Date.today()-30){
							loginStatus='Not Logged In Last 30 Days';
						}else{
							if (lastAppLogin <= Date.today()-7){
								loginStatus='Have Logged In Last 7-30 Days';
							}else{
								loginStatus='Have Logged In Last 7 Days';
							}
						}
												
					}
				}
			}
			
			//only change/save when different from existing value
			if(u.last_application_login_status__c==null || loginStatus.compareTo(u.last_application_login_status__c)!=0){
				u.last_application_login_status__c = loginStatus;
				usersToUpdate.add(u);
			}
			
		}
		
    	return usersToUpdate;
	}


	// This method will inform the provided users about their last login in Salesforce.com if the match the criteria's that are defined in the Custom Setting (User_Last_Login_Reminder__c) 
	public static void processUserLastLoginReminder(List<User> lstUser, User_Last_Login_Reminder__c oUserLastLoginReminder){

		// Load / Set additional data based on the Custom Setting Data
		Id idEmailTemplate = [SELECT Id FROM EmailTemplate WHERE DeveloperName = :oUserLastLoginReminder.Email_Template_Developer_Name__c].Id;
		Id idOrgWideEmailAddress;
		try{
			idOrgWideEmailAddress = [SELECT Id, Address, DisplayName FROM OrgWideEmailAddress WHERE Address = :oUserLastLoginReminder.Organization_Wide_Email_Address__c].Id;
		}catch(Exception oEX){}
		List<String> lstCC_FromSetting = new List<String>();
		if (clsUtil.isNull(oUserLastLoginReminder.Additional_CC__c, '') != ''){
			lstCC_FromSetting.addAll(oUserLastLoginReminder.Additional_CC__c.split(';'));	
		}
		List<String> lstBCC = new List<String>();
		if (clsUtil.isNull(oUserLastLoginReminder.Additional_BCC__c, '') != ''){
			lstBCC.addAll(oUserLastLoginReminder.Additional_BCC__c.split(';'));	
		}
		Integer iDays = oUserLastLoginReminder.Inactive_Days__c.intValue();

		List<Messaging.SingleEmailMessage> lstEmailMessage = new List<Messaging.SingleEmailMessage>();
   		List<User> lstUser_Processing = new List<User>();
   		for (User oUser : lstUser){
   			Date dLastApplicationLogin = oUser.Last_Application_Login__c.Date();
   			// Validate if the user didn't logged in for iDays
   			if (dLastApplicationLogin.daysBetween(Date.Today()) == iDays){
				List<String> lstCC = new List<String>();
   				try{
   					if (oUserLastLoginReminder.Notify_Manager__c){
	   					if (oUser.Manager != null && oUser.Manager.Email != ''){
		   					lstCC.add(oUser.Manager.Email);
		   				}
		   			}
	   			}catch(Exception oEX){}
	   			if (lstCC_FromSetting.size() > 0){
	   				lstCC.addAll(lstCC_FromSetting);
	   			}

				Messaging.SingleEmailMessage oEmailMessage = new Messaging.SingleEmailMessage();
					oEmailMessage.setTargetObjectId(oUser.Id);
					oEmailMessage.setCCAddresses(lstCC);
					oEmailMessage.setBCCAddresses(lstBCC);
					oEmailMessage.setTemplateID(idEmailTemplate); 
					oEmailMessage.setSaveAsActivity(false);
					if (idOrgWideEmailAddress!=null){
						oEmailMessage.setOrgWideEmailAddressId(idOrgWideEmailAddress);
					}
				lstEmailMessage.add(oEmailMessage);
	   		}
   		}

   		if (lstEmailMessage.size() > 0 ){
   			// Send the Emails to the users
			Messaging.sendEmail(lstEmailMessage, false); 
		}	
	}


	// This method will update specific fields on user
	// - Company_Code_Text__c
	public static List<User> updateUserFields(List<User> lstUser, boolean bDoUpdate){

		// Collect the Country_vs__c values of the processing users
		Map<String, List<User>> mapCountry_Users = new Map<String, List<User>>();
		for (User oUser : lstUser){
			if (clsUtil.isNull(oUser.Country_vs__c, '') != ''){
				String tUserCountry = oUser.Country_vs__c.toUpperCase();

				List<User> lstUser_Tmp = new List<User>();
				if (mapCountry_Users.containsKey(tUserCountry)){
					lstUser_Tmp = mapCountry_Users.get(tUserCountry);
				}
				lstUser_Tmp.add(oUser);
				mapCountry_Users.put(tUserCountry, lstUser_Tmp);
			}
		}

		// Get the Company Codes for the collected Countries and update the related Users
		List<DIB_Country__c> lstDIBCountry = 
			[
				SELECT 
					Id, Name, Company__c, Company__r.Company_Code_Text__c, Country_ISO_Code__c
				FROM 
					DIB_Country__c
				WHERE
					Name = :mapCountry_Users.keySet()
			];

		List<User> lstUser_Update = new List<User>();
		for (DIB_Country__c oDIBCountry : lstDIBCountry){
			String tDIBCountry = oDIBCountry.Name.toUpperCase();
			if (mapCountry_Users.containsKey(tDIBCountry)){
				for (User oUser : mapCountry_Users.get(tDIBCountry)){
					oUser.Company_Code_Text__c = oDIBCountry.Company__r.Company_Code_Text__c;
					lstUser_Update.add(oUser);
				}
			}
		}

		if (bDoUpdate && lstUser_Update.size() > 0){
			update lstUser_Update;
		}

		return lstUser_Update;

	}
	
	public static void countryFieldsSync(List<User> users, Map<Id, User> usersOld){
		
		for(User userRecord : users){
			
			if(usersOld == null || userRecord.Country_vs__c != usersOld.get(userRecord.Id).Country_vs__c || userRecord.CountryOR__c != usersOld.get(userRecord.Id).CountryOR__c 
				|| userRecord.Country != usersOld.get(userRecord.Id).Country){
				
				userRecord.Country = userRecord.Country_vs__c; 		
				if(userRecord.Country_vs__c != null) userRecord.CountryOR__c = userRecord.Country_vs__c.toUpperCase();
				else userRecord.CountryOR__c = null;					
			}		
		}		
	}
}