global class sc_ContactsPerCountry implements Schedulable{
	
	global void execute(SchedulableContext SC) {
      
		ba_ContactsPerCountry batch = new ba_ContactsPerCountry();
		Database.executeBatch(batch, 2000);	 
   	}
}