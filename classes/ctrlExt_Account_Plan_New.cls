public without sharing class ctrlExt_Account_Plan_New {
    
    private Account_Plan_2__c accPlan;
    public Account acc {get; set;}
    
    private Map<Id, Business_Unit__c> buMap;
    private Map<Id, List<Account_Plan_Level__c>> accPlanLevels;
    public Map<Id, Boolean> hasEditAccess {get; set;}
    
    public List<SelectOption> buOptions {get; set;}
    public List<SelectOption> sbuOptions {get; set;}
    public List<SelectOption> levelOptions {get; set;}
    
    public Boolean accountMissing {get; set;}
        
    public List<Account_Plan_2__c> existingAccPlans {get; private set;}
    
    private Set<Id> userSbuIds;
    
    public ctrlExt_Account_Plan_New(ApexPages.StandardController sc){
    	
    	accPlan = (Account_Plan_2__c) sc.getRecord();
    	    	    	
    	if(ApexPages.currentPage().getParameters().get('save_new') != null){
    		
            String retURL = ApexPages.currentPage().getParameters().get('retURL');                   
            
            Id prevAP;
            
            if(retURL.length() >= 18){
            	
                prevAP = retURL.substring(retURL.length()-18);//id results into 18 character when we save the record & click on edit then on Save & New button
                
            }else if(retURL.length() >= 15){
            	
                prevAP = retURL.substring(retURL.length()-15);//15 character id when we go separately on the record and edit
            }
            
            if(prevAP != null){
            
            	List<Account_Plan_2__c> lstAP = [Select Account__c from Account_Plan_2__c where Id = :prevAP];
            	
            	if(lstAP != null && lstAP.size() > 0) accPlan.Account__c = lstAP[0].Account__c;                 
            }            
        }
    	    	
    	if(accPlan.Account__c == null){
    		
    		accountMissing = true;
    		return;    		
    		
    	}else{
    		
    		accountMissing = false;
    		acc = [Select Id, Name, Account_Country_vs__c from Account where Id = :accPlan.Account__c];
    	}
    	  	
    	// User BUs available    	    	
    	Set<Id> userBuIds = new Set<Id>();
    	Set<Id> userBugIds = new Set<Id>();
    	userSbuIds = new Set<Id>();
    	Id primaryBuId;
                
        for(User_Business_Unit__c userSBU : [Select Sub_Business_Unit__c, Sub_Business_Unit__r.Business_Unit__c, Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__c, Primary__c From User_Business_Unit__c where User__c = :UserInfo.getUserId() order by Business_Unit_text__c]){
        
        	userSbuIds.add(userSBU.Sub_Business_Unit__c);
        	userBuIds.add(userSBU.Sub_Business_Unit__r.Business_Unit__c);
        	userBugIds.add(userSBU.Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__c);
        	
	        if(userSBU.Primary__c == true) primaryBuId = userSBU.Sub_Business_Unit__r.Business_Unit__c;                        
        }        
        
        // Account Plan Levels for User Business Units
        accPlanLevels = new Map<Id, List<Account_Plan_Level__c>>();
        
        for(Account_Plan_Level__c apl : [Select Id, Account_Plan_Level__c, Business_Unit__c from Account_Plan_Level__c where Business_Unit__c IN :userBuIds ORDER BY Account_Plan_Level__c]){
        	
        	if(UserInfo.getUserType().startsWith('Power') && apl.Account_Plan_Level__c != 'Sub Business Unit') continue;
        	
        	List<Account_Plan_Level__c> buLevels = accPlanLevels.get(apl.Business_Unit__c);
        	
        	if(buLevels == null){
        		
        		buLevels = new List<Account_Plan_Level__c>();
        		accPlanLevels.put(apl.Business_Unit__c, buLevels);
        	}
        	
        	buLevels.add(apl);
        }
        
        // Business Unit information for BUs available to the user and with Account Plan Level
        buMap = new Map<Id, Business_Unit__c>();
        buOptions = new List<SelectOption>();
        buOptions.add(new SelectOption('', '--None--'));
               
        for(Business_Unit__c bu : [Select Id, Name, Business_Unit_Group__c, Company_Text__c, 
           							 (Select Id, Name from Sub_Business_Units__r ORDER BY Name)
        							 from Business_Unit__c where Id IN :accPlanLevels.keySet() ORDER BY Name]){
        	
        	buOptions.add(new SelectOption(bu.Id, bu.Name));
        	buMap.put(bu.Id, bu);	
        }
                        
        // Load existing Account Plans for context Account and BU / BUG available to the User        
        existingAccPlans = new List<Account_Plan_2__c>();
        						
        for(Account_Plan_2__c ap: [Select Id, Business_Unit_Group__r.Name, Business_Unit__r.Name, Sub_Business_Unit__c, Sub_Business_Unit__r.Name, Account_Plan_Level__c, Owner.Name
        						from Account_Plan_2__c where Account__c = :accplan.Account__c
        						ORDER BY Business_Unit_Group__r.Name, Business_Unit__r.Name, Sub_Business_Unit__r.Name, Account_Plan_Level__c]){
        	
        	if(UserInfo.getUserType().startsWith('Power')){
        		
        		if(ap.Account_Plan_Level__c == 'Sub Business Unit' && userSbuIds.contains(ap.Sub_Business_Unit__c)){
	                   
	            	existingAccPlans.add(AP);                
	            }
        		
        	}else{	
        						            
	            if( (ap.Account_Plan_Level__c == 'Sub Business Unit' || ap.Account_Plan_Level__c=='Business Unit') && userBuIds.contains(ap.Business_Unit__c)){
	                   
	            	existingAccPlans.add(AP);                
	            }
	                                     
	            if(ap.Account_Plan_Level__c == 'Business Unit Group' && userBugIds.contains(ap.Business_Unit_Group__c)){
	                 
	                existingAccPlans.add(AP);               
	            }
        	}
        }
        
        // Check which existing Account Plans are editable to current user
        hasEditAccess = bl_UserAccess.hasEditAccess(existingAccPlans);
        
        // Pre-select the primary User BU if available
        if(buMap.containsKey(primaryBUId)) accPlan.Business_Unit__c = primaryBUId;
        else if(buOptions.size() == 2) accPlan.Business_Unit__c = buOptions[1].getValue();	
        
        BUChanged(); 	
    }
    
    public void BUChanged(){
    	
    	accPlan.Account_Plan_Level__c = null;
    	accPlan.Sub_Business_Unit__c = null;
    	    	
    	levelOptions = new List<SelectOption>();
    	levelOptions.add(new SelectOption('', '--None--'));
    	
    	if(accPlan.Business_Unit__c != null){
    		    		
    		// Get Account Plan Levels
    		for(Account_Plan_Level__c apl : accPlanLevels.get(accPlan.Business_Unit__c)){
    			
    			levelOptions.add(new SelectOption(apl.Account_Plan_Level__c, apl.Account_Plan_Level__c));    			
    		}    		
    	}
    	
    	if(levelOptions.size() == 2) accPlan.Account_Plan_Level__c = levelOptions[1].getValue();
    	
    	levelChanged();
    }
    
    public void levelChanged(){
    	
    	accPlan.Sub_Business_Unit__c = null;
    	
    	sbuOptions = new List<SelectOption>();
    	sbuOptions.add(new SelectOption('', '--None--'));
    	
    	if(accPlan.Account_Plan_Level__c == 'Sub Business Unit'){
    		
    		if(UserInfo.getUserType().startsWith('Power')){
    			
    			for(Sub_Business_Units__c sbu : [Select Id, Name from Sub_Business_Units__c where Business_Unit__c = :accPlan.Business_Unit__c AND Id IN :userSbuIds AND Name != 'HCC' ORDER BY Name]){
	    			
	    			sbuOptions.add(new SelectOption(sbu.Id, sbu.Name));
	    		}
    			
    		}else{
    			    		    		
	    		//Hardcoded exclusion of HCC as possible sBU for an Account Plan
	    		for(Sub_Business_Units__c sbu : [Select Id, Name from Sub_Business_Units__c where Business_Unit__c = :accPlan.Business_Unit__c AND Name != 'HCC' ORDER BY Name]){
	    			
	    			sbuOptions.add(new SelectOption(sbu.Id, sbu.Name));
	    		}
    		}
    	}
    	
    	if(sbuOptions.size() == 2) accPlan.Sub_Business_Unit__c = sbuOptions[1].getValue();
    }
    
    public Id selectedPlan {get; set;}
    
    public void requestAccessToOwner(){
        
        Account_Plan_2__c selectedPlan = 
            [
                SELECT 
                    Id, Name, OwnerId, Owner.IsActive, Owner.Name
                    , Account__r.Account_Country_vs__c
                    , Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__r.SFE_Manager__c, Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__r.SFE_Manager__r.Name
                    , Business_Unit__r.Business_Unit_Group__r.SFE_Manager__c, Business_Unit__r.Business_Unit_Group__r.SFE_Manager__r.Name
                    , Business_Unit_Group__r.SFE_Manager__c, Business_Unit_Group__r.SFE_Manager__r.Name

                    , Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__r.SFE_Manager_Canada__c, Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__r.SFE_Manager_Canada__r.Name
                    , Business_Unit__r.Business_Unit_Group__r.SFE_Manager_Canada__c, Business_Unit__r.Business_Unit_Group__r.SFE_Manager_Canada__r.Name
                    , Business_Unit_Group__r.SFE_Manager_Canada__c, Business_Unit_Group__r.SFE_Manager_Canada__r.Name
                FROM 
                    Account_Plan_2__c 
                WHERE 
                    Id = :selectedPlan
            ];      


        // Verify if the Account Plan Owner is Active or not and use the SFE_Manager__c in the Master Data if the owner is Inactive
        // For Canada we have a separated field in the masterdata to store the SFE Manager.
        Id idUser = selectedPlan.OwnerId;
        String tUserName = selectedPlan.Owner.Name;
        if (selectedPlan.Owner.IsActive == false){

            if (selectedPlan.Account__r.Account_Country_vs__c == 'CANADA'){

                if (selectedPlan.Business_Unit_Group__r.SFE_Manager_Canada__c != null){
                    idUser = selectedPlan.Business_Unit_Group__r.SFE_Manager_Canada__c;
                    tUserName = selectedPlan.Business_Unit_Group__r.SFE_Manager_Canada__r.Name;
                }else if (selectedPlan.Business_Unit__r.Business_Unit_Group__r.SFE_Manager_Canada__c != null){
                    idUser = selectedPlan.Business_Unit__r.Business_Unit_Group__r.SFE_Manager_Canada__c;
                    tUserName = selectedPlan.Business_Unit__r.Business_Unit_Group__r.SFE_Manager_Canada__r.Name;
                }else if (selectedPlan.Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__r.SFE_Manager_Canada__c != null){
                    idUser = selectedPlan.Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__r.SFE_Manager_Canada__c;
                    tUserName = selectedPlan.Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__r.SFE_Manager_Canada__r.Name;
                }

            }else{

                if (selectedPlan.Business_Unit_Group__r.SFE_Manager__c != null){
                    idUser = selectedPlan.Business_Unit_Group__r.SFE_Manager__c;
                    tUserName = selectedPlan.Business_Unit_Group__r.SFE_Manager__r.Name;
                }else if (selectedPlan.Business_Unit__r.Business_Unit_Group__r.SFE_Manager__c != null){
                    idUser = selectedPlan.Business_Unit__r.Business_Unit_Group__r.SFE_Manager__c;
                    tUserName = selectedPlan.Business_Unit__r.Business_Unit_Group__r.SFE_Manager__r.Name;
                }else if (selectedPlan.Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__r.SFE_Manager__c != null){
                    idUser = selectedPlan.Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__r.SFE_Manager__c;
                    tUserName = selectedPlan.Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__r.SFE_Manager__r.Name;
                }

            }

        }
                        
        String emailBody ='Dear '+ tUserName + ',<br/><br/>'; 
        emailBody += 'Please add me to the Team of your Account Plan (<a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + selectedPlan.Id + '">' + selectedPlan.Name + ' </a>).<br/><br/>'; 
        emailBody += 'Best regards,<br/><br/>'; 
        emailBody += UserInfo.getName();
        
        Messaging.Singleemailmessage mail = new Messaging.Singleemailmessage();              
            mail.setUseSignature(false);
            mail.setSaveAsActivity(false);
            mail.setTargetObjectId(idUser);
            mail.setHtmlBody(emailBody);
            mail.setSubject('Please add me to your Account Plan Team');
            mail.setBccSender(true);
        Messaging.sendEmail( new List<Messaging.Singleemailmessage>{mail} );    
    
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm, 'The Account Plan Owner has been notified'));
    }
    
    public PageReference createAccountPlan(){
    	
    	if(accPlan.Business_Unit__c == null){
    		  		
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a Business Unit'));
            return null;
    	}
    	
    	if(accPlan.Account_Plan_Level__c == null || accPlan.Account_Plan_Level__c == ''){
    		  		
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select an Account Plan Level'));
            return null;
    	}
    	
    	if(accPlan.Account_Plan_Level__c == 'Sub Business Unit' && accPlan.Sub_Business_Unit__c == null){
    		  		
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a Sub-Business Unit'));
            return null;
    	}
    	    	
    	accPlan.Business_Unit_Group__c = buMap.get(accPlan.Business_Unit__c).Business_Unit_Group__c;
    	
    	Account_Plan_Level__c apl = [Select Account_Plan_Record_Type__c from Account_Plan_Level__c where Business_Unit__c = :accPlan.Business_Unit__c AND Account_Plan_Level__c = :accPlan.Account_Plan_Level__c LIMIT 1];
    	accPlan.RecordTypeId = apl.Account_Plan_Record_Type__c;
    	
    	Id selectedBU = accPlan.Business_Unit__c;
    	
    	if(accPlan.Account_Plan_Level__c == 'Business Unit Group'){
    		
    		accPlan.Business_Unit__c = null;
    		accPlan.Sub_Business_Unit__c = null;
    		
    	}else if(accPlan.Account_Plan_Level__c == 'Business Unit'){
    		
    		accPlan.Sub_Business_Unit__c = null;
    		
    	}
    	
    	try{
    		
    		accPlan.Start_Date__c = Date.today();
    		
    		insert accPlan;
    		
    	}catch(Exception e){
    		
    		accPlan.Business_Unit__c = selectedBU;
    		
    		ApexPages.addMessages(e);
            return null;
    	}
    	
    	PageReference pr = new ApexPages.StandardController(accPlan).edit();
    	pr.getParameters().put('retURL', '/' + accPlan.Id);
        pr.setRedirect(true);
        return pr;
    }
}