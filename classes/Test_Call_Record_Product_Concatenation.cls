@isTest
private class Test_Call_Record_Product_Concatenation {
    
    private static testmethod void testProductConcatenation(){
    	    	
    	Account acc = new Account();
        acc.Name = 'Test Account';
        acc.Account_Country_vs__c='NETHERLANDS';      
        insert acc;
                        
        List<Contact> lstCon = new List<Contact>();
        
        Contact cont1 = new Contact();
        cont1.LastName = 'Contact 1';
        cont1.FirstName = 'Test';  
        cont1.AccountId = acc.Id;  
        cont1.Phone = '0714820303'; 
        cont1.Email ='test1@contact.com';
        cont1.Contact_Department__c = 'Diabetes Adult';
        cont1.Contact_Primary_Specialty__c = 'ENT';
        cont1.Affiliation_To_Account__c = 'Employee';
        cont1.Primary_Job_Title_vs__c = 'Manager';
        cont1.Contact_Gender__c ='Male'; 
        cont1.MailingCountry='NETHERLANDS';        
        lstCon.add(cont1);   
        
        Contact cont2 = new Contact();
        cont2.LastName = 'Contact 2';
        cont2.FirstName = 'Test';  
        cont2.AccountId = acc.Id;  
        cont2.Phone = '0482222266'; 
        cont2.Email ='test2@contact.com';
        cont2.Contact_Department__c = 'Diabetes Adult';
        cont2.Contact_Primary_Specialty__c = 'ENT';
        cont2.Affiliation_To_Account__c = 'Employee';
        cont2.Primary_Job_Title_vs__c = 'Manager';
        cont2.Contact_Gender__c ='Male';  
        cont2.MailingCountry='NETHERLANDS';         
        lstCon.add(cont2);
        
        insert lstCon;
        
        //Insert Company
        Company__c cmpny = new Company__c();
        cmpny.name='TestMedtronic';
        cmpny.CurrencyIsoCode = 'EUR';
        cmpny.Current_day_in_Q1__c=56;
        cmpny.Current_day_in_Q2__c=34; 
        cmpny.Current_day_in_Q3__c=5; 
        cmpny.Current_day_in_Q4__c= 0;   
        cmpny.Current_day_in_year__c =200;
        cmpny.Days_in_Q1__c=56;  
        cmpny.Days_in_Q2__c=34;
        cmpny.Days_in_Q3__c=13;
        cmpny.Days_in_Q4__c=22;
        cmpny.Days_in_year__c =250;
        cmpny.Company_Code_Text__c = 'T31';
        insert cmpny;
         
        DIB_Country__c country=new DIB_Country__c();
        country.name='Netherlands';
        country.Company__c = cmpny.id;
        country.Country_ISO_Code__c='NL';
        country.Call_Record_Chatter_Post_on_Contact__c =true;
        insert country;
        
		Subject__c subj = new Subject__c();
		subj.name = 'Test Subject';
		subj.Company_ID__c = cmpny.Id;
		insert subj; 
                    
        Call_Activity_Type__c cat = new Call_Activity_Type__c();
        cat.name = 'Test CAT';
        cat.Company_ID__c = cmpny.id;
        insert cat; 
        
        RecordType mrktProduct = [Select Id from RecordType where SObjectType = 'Product2' AND DeveloperName = 'MDT_Marketing_Product'];
        
        Product2 prd1  = new Product2();
        prd1.Name = 'Product 1';
        prd1.RecordTypeId = mrktProduct.Id;
        
        Product2 prd2  = new Product2();
        prd2.Name = 'Test Product 2';
        prd2.RecordTypeId = mrktProduct.Id;
        
        insert new List<Product2>{prd1, prd2};
        
        RecordType BUcallrecord = [Select Id from RecordType where SObjectType = 'Call_Records__c' AND DeveloperName = 'Business_Unit'];
        
        Call_Records__c vr = new Call_Records__c();
        vr.Call_Date__c = Date.today();
        vr.RecordTypeId = BUcallrecord.Id;
        insert vr;
        
        Call_Topics__c  ct = new Call_Topics__c(); 
        ct.Call_Records__c = vr.Id;
        ct.Call_Activity_Type__c = cat.Id;
        ct.Call_Topic_Duration__c = 10;
        ct.Call_Topic_Subjects_Concatendated__c = subj.Id;
        insert ct;
        
        Test.startTest();
        
        Call_Topic_Products__c ctp1 = new Call_Topic_Products__c();
        ctp1.Product__c = prd1.Id;
        ctp1.Call_Topic__c = ct.Id;
        insert ctp1;
                        
        vr = [Select Id, Products_Concatenated__c from Call_Records__c where Id = :vr.Id];
        System.assert(vr.Products_Concatenated__c == 'Product 1');
                       
        Contact_Visit_Report__c cvr1 = new Contact_Visit_Report__c(); 
        cvr1.Call_Records__c = vr.Id; 
        cvr1.Attending_Contact__c = cont1.Id; 
        cvr1.Attending_Affiliated_Account__c = acc.Id;       
       	insert cvr1;
       	
       	cvr1 = [Select Id, Products_Concatenated__c from Contact_Visit_Report__c where Id = :cvr1.Id];
       	system.assert(cvr1.Products_Concatenated__c == 'Product 1');
                
        Call_Topic_Products__c ctp2 = new Call_Topic_Products__c();
        ctp2.Product__c = prd2.Id;
        ctp2.Call_Topic__c = ct.Id;
        insert ctp2;
        
        vr = [Select Id, Products_Concatenated__c from Call_Records__c where Id = :vr.Id];
        System.assert(vr.Products_Concatenated__c.contains('Product 1'));
        System.assert(vr.Products_Concatenated__c.contains('Product 2'));
        
        cvr1 = [Select Id, Products_Concatenated__c from Contact_Visit_Report__c where Id = :cvr1.Id];
       	System.assert(cvr1.Products_Concatenated__c.contains('Product 1'));
        System.assert(cvr1.Products_Concatenated__c.contains('Product 2'));
        
        Contact_Visit_Report__c cvr2 = new Contact_Visit_Report__c (); 
        cvr2.Call_Records__c = vr.Id; 
        cvr2.Attending_Contact__c = cont2.Id; 
        cvr2.Attending_Affiliated_Account__c = acc.Id;        
        insert cvr2;
        
        cvr2 = [Select Id, Products_Concatenated__c from Contact_Visit_Report__c where Id = :cvr2.Id];
       	System.assert(cvr2.Products_Concatenated__c.contains('Product 1'));
        System.assert(cvr2.Products_Concatenated__c.contains('Product 2'));                  
    }
}