@isTest public class clsTestData_Contact {

    public static Integer iRecord_Contact = 1;
    public static Id idRecordType_Contact = clsUtil.getRecordTypeByDevName('Contact', 'Generic_Contact').Id;
    public static Contact oMain_Contact;
    public static List<Contact> lstContact = new List<Contact>();

    public static Integer iRecord_MDTEmployee = 1;
    public static Id idRecordType_MDTEmployee = clsUtil.getRecordTypeByDevName('Contact', 'MDT_Employee').Id;
    public static Contact oMain_MDTEmployee;
    public static List<Contact> lstMDTEmployee = new List<Contact>();
    

    //---------------------------------------------------------------------------------------------------
    // Create Contact Data
    //---------------------------------------------------------------------------------------------------
    public static List<Contact> createContact() {

        return createContact(true);

    }

    public static List<Contact> createContact(Boolean bInsert) {

        if (oMain_Contact == null){

            if (clsTestData_Account.oMain_Account == null) clsTestData_Account.createAccount(true);

            lstContact = new List<Contact>();

            for (Account oAccount : clsTestData_Account.lstAccount){
                for (Integer i=0; i<iRecord_Contact; i++){
                    Contact oContact = new Contact();
                        oContact.RecordTypeId = idRecordType_Contact;
						oContact.FirstName = 'TEST Contact ' + i;
                        oContact.LastName = 'TEST Contact ' + i;  
                        oContact.AccountId = oAccount.Id;  
                        oContact.Phone = '001222333'; 
                        oContact.Email =' test' + i + '@medtronic.com';
                        oContact.Contact_Department__c = 'Diabetes Adult'; 
                        oContact.Contact_Primary_Specialty__c = 'ENT';
                        oContact.Affiliation_To_Account__c = 'Employee';
                        oContact.Primary_Job_Title_vs__c = 'Manager';
                        oContact.Contact_Gender__c = 'Male';
                    lstContact.add(oContact);
                }
            }
            if (bInsert){
                insert lstContact;
            }
            oMain_Contact = lstContact[0];               

        }

        return lstContact;
        
    }
    //---------------------------------------------------------------------------------------------------


    //---------------------------------------------------------------------------------------------------
    // Create MDT Employee Data
    //---------------------------------------------------------------------------------------------------
    public static List<Contact> createMDTEmployee() {

        return createMDTEmployee(true);

    }

    public static List<Contact> createMDTEmployee(Boolean bInsert) {

        if (oMain_MDTEmployee == null){

            if (clsTestData_Account.oMain_MDTAccount == null) clsTestData_Account.createMDTAccount(true);

            lstMDTEmployee = new List<Contact>();

			String tTimeStamp = clsUtil.getTimeStamp();
            for (Account oAccount : clsTestData_Account.lstMDTAccount){
                for (Integer i=0; i<iRecord_MDTEmployee; i++){
                    Contact oContact = new Contact();
                        oContact.RecordTypeId = idRecordType_MDTEmployee;
                        oContact.LastName = 'TEST MDT Employee ' + i;  
                        oContact.AccountId = oAccount.Id;  
                        oContact.Phone = '001222333'; 
                        oContact.Email =' test' + i + '@medtronic.com';
                        oContact.Contact_Department__c = 'Diabetes Adult'; 
                        oContact.Contact_Primary_Specialty__c = 'ENT';
                        oContact.Affiliation_To_Account__c = 'Employee';
                        oContact.Primary_Job_Title_vs__c = 'Marketing';
                        oContact.Contact_Gender__c = 'Male';
						oContact.Contact_Active__c = true;
						oContact.Alias_unique__c = String.valueOf(i) + tTimeStamp;
                    lstMDTEmployee.add(oContact);
                }
            }
            if (bInsert){
                insert lstMDTEmployee;
            }
            oMain_MDTEmployee = lstMDTEmployee[0];               

        }

        return lstMDTEmployee;
        
    }    
    //---------------------------------------------------------------------------------------------------

}