@isTest
private class Test_ba_AccountBusUnitUpdateFlags {
    
    @testSetup
    static void createValidTestData(){
        List<Account> listAccount = new List<Account>();
        Id devRecordTypeId = clsUtil.GetRecordTypeByDevName('Account','Buying_Entity').Id;
        
        //Create new Account with type Buying Group
		listAccount.add(new Account(name='Test Buying Group Belgium DS', RecordTypeId=devRecordTypeId, Type='Buying Group', Account_Country_vs__c='BELGIUM'));
		//Create new Account with type Group Purchasing Organization
		listAccount.add(new Account(name='Test GPO Belgium DS', RecordTypeId=devRecordTypeId, Type='Group Purchasing Organization', Account_Country_vs__c='BELGIUM'));
		//Create new Account with type Group Purchasing Organization
		listAccount.add(new Account(name='Test Sub GPO Belgium DS', RecordTypeId=devRecordTypeId, Type='Group Purchasing Organization', Account_Country_vs__c='BELGIUM'));
        
        Id RecordTypeIdSAPAccount = clsUtil.GetRecordTypeByDevName('Account','SAP_Account').Id;
       
		//Create new Account with type Group Purchasing Organization
		listAccount.add(new Account(name='UZ Leuven', RecordTypeId=RecordTypeIdSAPAccount, Type='Public Hospital', Account_Country_vs__c='BELGIUM',SAP_Account_Type__c='Sold-to party'));
            
		insert listAccount;
        
        List<Affiliation__c> listAffiliation = new List<Affiliation__c>();
        
        //Create Relationship with start date in the past and no end date
        //Account BU flags Vascular and Diabetes will be set by batch
        Affiliation__c curAff = new Affiliation__c(Affiliation_From_Account__c = [select Id, Name from Account where Name = 'Test Sub GPO Belgium DS'].Id);
        curAff.Affiliation_Type__c = 'SubGPO/GPO';
		curAff.Affiliation_To_Account__c = [select Id from Account where Name = 'Test GPO Belgium DS'].Id;
        curAff.Business_Unit_Group_vs__c = 'CVG; Diabetes';
        curAff.Business_Unit_vs__c = 'Diabetes; Vascular';
        curAff.Sub_Business_Unit_vs__c = 'Coro + PV; Diabetes Core; HCC';
        curAff.Affiliation_Start_Date__c = Date.newInstance(2019, 1, 1);
        
		listAffiliation.add(curAff);
        
        //Create Relationship with start date and end date in the past
        //No Account BU flags will be set by batch
        curAff = new Affiliation__c(Affiliation_From_Account__c = [select Id, Name from Account where Name = 'Test Sub GPO Belgium DS'].Id);
        curAff.Affiliation_Type__c = 'SubGPO/GPO';
		curAff.Affiliation_To_Account__c = [select Id from Account where Name = 'Test GPO Belgium DS'].Id;
        curAff.Business_Unit_Group_vs__c = 'Restorative';
        curAff.Business_Unit_vs__c = 'Pain';
        curAff.Sub_Business_Unit_vs__c = 'Pain Stim; TDD';
        curAff.Affiliation_Start_Date__c = Date.newInstance(2019, 1, 1);
        curAff.Affiliation_End_Date__c = Date.newInstance(2019, 2, 1);
        
		listAffiliation.add(curAff);
        
        //Create Relationship with start date in the future and no end date
        //No Account BU flags will be set by batch
        curAff = new Affiliation__c(Affiliation_From_Account__c = [select Id, Name from Account where Name = 'Test Sub GPO Belgium DS'].Id);
        curAff.Affiliation_Type__c = 'SubGPO/GPO';
		curAff.Affiliation_To_Account__c = [select Id from Account where Name = 'Test GPO Belgium DS'].Id;
        curAff.Business_Unit_Group_vs__c = 'MITG';
        curAff.Business_Unit_vs__c = 'Robotics';
        curAff.Sub_Business_Unit_vs__c = 'Robotics';
        curAff.Affiliation_Start_Date__c = Date.today() + 100;
        
		listAffiliation.add(curAff);
        
        insert listAffiliation;
    }
    
    
    public static testMethod void testBatch(){
        List<Account> listAccountUpdate = new List<Account>();
        
        //Retrieve From Account with BU flags set by trigger tr_Relationship
        Account recFromAccount = [select Id, Name, RecordType.DeveloperName, Vascular__c, CRHF__c, CS_SH__c, Diabetes__c, IHS__c, Early_Technologies__c, Renal_Care_Solutions__c, 
                                                    Patient_Monitoring_Recovery__c, Robotics__c, Surgical_Innovations__c, Cranial_and_spinal__c,ENT__c, Neurovascular__c,
                                                    Pain__c, Pelvic_Health__c, RTG_IHS__c from Account where name = 'Test Sub GPO Belgium DS'];
        
        //Retrieve To Account with BU flags set by trigger tr_Relationship
        Account recToAccount = [select Id, Name, RecordType.DeveloperName, Vascular__c, CRHF__c, CS_SH__c, Diabetes__c, IHS__c, Early_Technologies__c, Renal_Care_Solutions__c, 
                                                    Patient_Monitoring_Recovery__c, Robotics__c, Surgical_Innovations__c, Cranial_and_spinal__c,ENT__c, Neurovascular__c,
                                                    Pain__c, Pelvic_Health__c, RTG_IHS__c from Account where name = 'Test GPO Belgium DS'];
        
        //Check From Account BU flags, set by trigger tr_Relationship
        system.assertEquals(true, recFromAccount.Vascular__c);
        system.assertEquals(false, recFromAccount.CRHF__c);
        system.assertEquals(false, recFromAccount.CS_SH__c);
        system.assertEquals(true, recFromAccount.Diabetes__c);
        system.assertEquals(false, recFromAccount.Early_Technologies__c);
        system.assertEquals(false, recFromAccount.Renal_Care_Solutions__c);
        system.assertEquals(false, recFromAccount.Patient_Monitoring_Recovery__c);
        system.assertEquals(false, recFromAccount.Surgical_Innovations__c);
        system.assertEquals(false, recFromAccount.Cranial_and_spinal__c);
        system.assertEquals(false, recFromAccount.ENT__c);
        system.assertEquals(false, recFromAccount.Neurovascular__c);
        system.assertEquals(false, recFromAccount.Pain__c);
        system.assertEquals(false, recFromAccount.Pelvic_Health__c);
        system.assertEquals(false, recFromAccount.RTG_IHS__c);
        
        //Check To Account BU flags, set by trigger tr_Relationship
        system.assertEquals(true, recToAccount.Vascular__c);
        system.assertEquals(false, recToAccount.CRHF__c);
        system.assertEquals(false, recToAccount.CS_SH__c);
        system.assertEquals(true, recToAccount.Diabetes__c);
        system.assertEquals(false, recToAccount.Early_Technologies__c);
        system.assertEquals(false, recToAccount.Renal_Care_Solutions__c);
        system.assertEquals(false, recToAccount.Patient_Monitoring_Recovery__c);
        system.assertEquals(false, recToAccount.Surgical_Innovations__c);
        system.assertEquals(false, recToAccount.Cranial_and_spinal__c);
        system.assertEquals(false, recToAccount.ENT__c);
        system.assertEquals(false, recToAccount.Neurovascular__c);
        system.assertEquals(false, recToAccount.Pain__c);
        system.assertEquals(false, recToAccount.Pelvic_Health__c);
        system.assertEquals(false, recToAccount.RTG_IHS__c);
        
        
        //Reset From Account BU flags to false, set by trigger tr_Relationship, to test batch
        recFromAccount.Vascular__c =  false;
        recFromAccount.CRHF__c =  false;
		recFromAccount.CS_SH__c =  false;
		recFromAccount.Diabetes__c =  false;
		recFromAccount.Early_Technologies__c =  false;
		recFromAccount.Renal_Care_Solutions__c =  false;
		recFromAccount.Patient_Monitoring_Recovery__c =  false;
		recFromAccount.Surgical_Innovations__c =  false;
		recFromAccount.Cranial_and_spinal__c =  false;
		recFromAccount.ENT__c =  false;
		recFromAccount.Neurovascular__c =  false;
		recFromAccount.Pain__c =  false;
		recFromAccount.Pelvic_Health__c =  false;
		recFromAccount.RTG_IHS__c =  false;
        
        listAccountUpdate.add(recFromAccount);
        
        //Reset To Account BU flags to false, set by trigger tr_Relationship, to test batch
        recToAccount.Vascular__c =  false;
        recToAccount.CRHF__c =  false;
		recToAccount.CS_SH__c =  false;
		recToAccount.Diabetes__c =  false;
		recToAccount.Early_Technologies__c =  false;
		recToAccount.Renal_Care_Solutions__c =  false;
		recToAccount.Patient_Monitoring_Recovery__c =  false;
		recToAccount.Surgical_Innovations__c =  false;
		recToAccount.Cranial_and_spinal__c =  false;
		recToAccount.ENT__c =  false;
		recToAccount.Neurovascular__c =  false;
		recToAccount.Pain__c =  false;
		recToAccount.Pelvic_Health__c =  false;
		recToAccount.RTG_IHS__c =  false;
        
        listAccountUpdate.add(recToAccount);
        
        update listAccountUpdate;
        
        //Retrieve From Account with BU flags reset after trigger
        recFromAccount = [select Id, Name, RecordType.DeveloperName, Vascular__c, CRHF__c, CS_SH__c, Diabetes__c, IHS__c, Early_Technologies__c, Renal_Care_Solutions__c, 
                                                    Patient_Monitoring_Recovery__c, Robotics__c, Surgical_Innovations__c, Cranial_and_spinal__c,ENT__c, Neurovascular__c,
                                                    Pain__c, Pelvic_Health__c, RTG_IHS__c from Account where name = 'Test Sub GPO Belgium DS'];
        
        //Retrieve To Account with BU flags reset after trigger
        recToAccount = [select Id, Name, RecordType.DeveloperName, Vascular__c, CRHF__c, CS_SH__c, Diabetes__c, IHS__c, Early_Technologies__c, Renal_Care_Solutions__c, 
                                                    Patient_Monitoring_Recovery__c, Robotics__c, Surgical_Innovations__c, Cranial_and_spinal__c,ENT__c, Neurovascular__c,
                                                    Pain__c, Pelvic_Health__c, RTG_IHS__c from Account where name = 'Test GPO Belgium DS'];
        
 		//Check From Account BU flags if set to false correctly
        system.assertEquals(false, recFromAccount.Vascular__c);
        system.assertEquals(false, recFromAccount.CRHF__c);
        system.assertEquals(false, recFromAccount.CS_SH__c);
        system.assertEquals(false, recFromAccount.Diabetes__c);
        system.assertEquals(false, recFromAccount.Early_Technologies__c);
        system.assertEquals(false, recFromAccount.Renal_Care_Solutions__c);
        system.assertEquals(false, recFromAccount.Patient_Monitoring_Recovery__c);
        system.assertEquals(false, recFromAccount.Surgical_Innovations__c);
        system.assertEquals(false, recFromAccount.Cranial_and_spinal__c);
        system.assertEquals(false, recFromAccount.ENT__c);
        system.assertEquals(false, recFromAccount.Neurovascular__c);
        system.assertEquals(false, recFromAccount.Pain__c);
        system.assertEquals(false, recFromAccount.Pelvic_Health__c);
        system.assertEquals(false, recFromAccount.RTG_IHS__c);
        
        //Check To Account BU flags if set to false correctly
        system.assertEquals(false, recToAccount.Vascular__c);
        system.assertEquals(false, recToAccount.CRHF__c);
        system.assertEquals(false, recToAccount.CS_SH__c);
        system.assertEquals(false, recToAccount.Diabetes__c);
        system.assertEquals(false, recToAccount.Early_Technologies__c);
        system.assertEquals(false, recToAccount.Renal_Care_Solutions__c);
        system.assertEquals(false, recToAccount.Patient_Monitoring_Recovery__c);
        system.assertEquals(false, recToAccount.Surgical_Innovations__c);
        system.assertEquals(false, recToAccount.Cranial_and_spinal__c);
        system.assertEquals(false, recToAccount.ENT__c);
        system.assertEquals(false, recToAccount.Neurovascular__c);
        system.assertEquals(false, recToAccount.Pain__c);
        system.assertEquals(false, recToAccount.Pelvic_Health__c);
        system.assertEquals(false, recToAccount.RTG_IHS__c);
        
        //Execute Batch
        Test.StartTest();
        ba_AccountBusUnitUpdateFlags accountUpdateBUFlage = new ba_AccountBusUnitUpdateFlags();
        ID batchprocessid = Database.executeBatch(accountUpdateBUFlage);
        Test.StopTest();
        
        //Retrieve From Account with BU flags set by batch
        recFromAccount = [select Id, Name, RecordType.DeveloperName, Vascular__c, CRHF__c, CS_SH__c, Diabetes__c, IHS__c, Early_Technologies__c, Renal_Care_Solutions__c, 
                                                    Patient_Monitoring_Recovery__c, Robotics__c, Surgical_Innovations__c, Cranial_and_spinal__c,ENT__c, Neurovascular__c,
                                                    Pain__c, Pelvic_Health__c, RTG_IHS__c from Account where name = 'Test Sub GPO Belgium DS'];
        
        //Retrieve To Account with BU flags set by batch
        recToAccount = [select Id, Name, RecordType.DeveloperName, Vascular__c, CRHF__c, CS_SH__c, Diabetes__c, IHS__c, Early_Technologies__c, Renal_Care_Solutions__c, 
                                                    Patient_Monitoring_Recovery__c, Robotics__c, Surgical_Innovations__c, Cranial_and_spinal__c,ENT__c, Neurovascular__c,
                                                    Pain__c, Pelvic_Health__c, RTG_IHS__c from Account where name = 'Test GPO Belgium DS'];
        
        //Check From Account BU flags, set by batch
        system.assertEquals(true, recFromAccount.Vascular__c);
        system.assertEquals(false, recFromAccount.CRHF__c);
        system.assertEquals(false, recFromAccount.CS_SH__c);
        system.assertEquals(true, recFromAccount.Diabetes__c);
        system.assertEquals(false, recFromAccount.Early_Technologies__c);
        system.assertEquals(false, recFromAccount.Renal_Care_Solutions__c);
        system.assertEquals(false, recFromAccount.Patient_Monitoring_Recovery__c);
        system.assertEquals(false, recFromAccount.Surgical_Innovations__c);
        system.assertEquals(false, recFromAccount.Cranial_and_spinal__c);
        system.assertEquals(false, recFromAccount.ENT__c);
        system.assertEquals(false, recFromAccount.Neurovascular__c);
        system.assertEquals(false, recFromAccount.Pain__c);
        system.assertEquals(false, recFromAccount.Pelvic_Health__c);
        system.assertEquals(false, recFromAccount.RTG_IHS__c);

		//Check To Account BU flags, set by batch        
        system.assertEquals(true, recToAccount.Vascular__c);
        system.assertEquals(false, recToAccount.CRHF__c);
        system.assertEquals(false, recToAccount.CS_SH__c);
        system.assertEquals(true, recToAccount.Diabetes__c);
        system.assertEquals(false, recToAccount.Early_Technologies__c);
        system.assertEquals(false, recToAccount.Renal_Care_Solutions__c);
        system.assertEquals(false, recToAccount.Patient_Monitoring_Recovery__c);
        system.assertEquals(false, recToAccount.Surgical_Innovations__c);
        system.assertEquals(false, recToAccount.Cranial_and_spinal__c);
        system.assertEquals(false, recToAccount.ENT__c);
        system.assertEquals(false, recToAccount.Neurovascular__c);
        system.assertEquals(false, recToAccount.Pain__c);
        system.assertEquals(false, recToAccount.Pelvic_Health__c);
        system.assertEquals(false, recToAccount.RTG_IHS__c);
    }
    
    static testmethod void testSchedulable(){
        
        Test.startTest();
        
        // Schedule the test job
        String jobId = System.schedule('testBasicScheduledApex','0 0 0 3 9 ? 2022', new ba_AccountBusUnitUpdateFlags());
        
        // Get the information from the CronTrigger API object
		CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        
        // Verify the expressions are the same
        System.assertEquals('0 0 0 3 9 ? 2022', ct.CronExpression);
        
        // Verify the job has not run
        System.assertEquals(0, ct.TimesTriggered);
        
        // Verify the next time the job will run
		System.assertEquals('2022-09-03 00:00:00', String.valueOf(ct.NextFireTime));
        
        Test.stopTest();
    }

}