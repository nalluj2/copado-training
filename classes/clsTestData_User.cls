@isTest public class clsTestData_User {
    
    //---------------------------------------------------------------------------------------------------
    // Create Salesforce.com User
    //---------------------------------------------------------------------------------------------------
    public static Map<String, List<String>> mapBU_JobTitle;
    public static String tUserBusinessUnit = 'CRHF';

    public static User createUser(){
        return createUser(true);
    }
    public static User createUser(Boolean bInsert){

        String tUsername = clsUtil.getTimeStamp() + '@test.medtronic.com';
        User oUser = createUser(tUsername, 'TestUser', 'BELGIUM', UserInfo.getProfileId(), UserInfo.getUserRoleId(), bInsert);

        return oUser;

    }
    public static User createUser(String tAlias, Boolean bInsert){

        return createUser(tAlias, UserInfo.getProfileId(), UserInfo.getUserRoleId(), bInsert);

    }
    public static User createUser(String tAlias, Id idProfile, Id idRole, Boolean bInsert){

        String tUsername = tAlias + '@medtronic.com.test';
        return createUser(tUsername, tAlias, 'Europe', 'NWE', 'BELGIUM', idProfile, idRole, bInsert);

    }
    public static User createUser(String tUsername, String tCountry, Id idProfile, Id idRole){

        return createUser(tUsername, 'TestUser', tCountry, idProfile, idRole);

    }
    public static User createUser(String tUsername, String tAlias, String tCountry, Id idProfile, Id idRole){

        return createUser(tUsername, tAlias, tCountry, idProfile, idRole, true);

    }
    public static User createUser(String tUsername, String tAlias, String tCountry, Id idProfile, Id idRole, Boolean bInsert){

        return createUser(tUsername, tAlias, 'Europe', 'NWE', tCountry, idProfile, idRole, bInsert);

    }
    public static User createUser(String tUsername, String tAlias, String tRegion, String tSubRegion, String tCountry, Id idProfile, Id idRole, Boolean bInsert){

        if (mapBU_JobTitle == null) mapBU_JobTitle = clsUtil.getFieldDependencies('User', 'User_Business_Unit_vs__c', 'Job_Title_vs__c');

        User oUser = new User();
            oUser.Username = tUsername;
            oUser.profileid = idProfile;
            if (idRole != null){
                oUser.UserRoleId = idRole;
            }
            oUser.FirstName= 'Test User';
            oUser.Lastname = 'Test User';
            oUser.Alias = tAlias;
            oUser.Alias_unique__c = tAlias;
            oUser.Email ='testuser@testorg.medtronic.com';
            oUser.Region_vs__c = tRegion;
            oUser.Sub_Region__c = tSubRegion;
            oUser.Country_vs__c = tCountry.toUpperCase();

            oUser.Emailencodingkey = 'UTF-8';
            oUser.Languagelocalekey = 'en_US';
            oUser.Localesidkey = 'en_US';
            oUser.Timezonesidkey = 'America/Los_Angeles';
            oUser.Company_Code_Text__c = 'Eur';
            oUser.User_Business_Unit_vs__c = tUserBusinessUnit;
            if (mapBU_JobTitle.containsKey(tUserBusinessUnit)){
                if (mapBU_JobTitle.get(tUserBusinessUnit).size() > 0){
                    oUser.Job_Title_vs__c = mapBU_JobTitle.get(tUserBusinessUnit)[0];
                }
            }
            oUser.CostCenter__c = '123456';
        if (bInsert) insert oUser;

        return oUser;     

    }

    public static User createUser_Guest(String tAlias, Boolean bInsert){

        if (mapBU_JobTitle == null) mapBU_JobTitle = clsUtil.getFieldDependencies('User', 'User_Business_Unit_vs__c', 'Job_Title_vs__c');

        User oUser = new User();
            oUser.Username = tAlias + '@medtronic.com.test';
            oUser.ProfileId = clsUtil.getUserProfileId('Support Profile');
            oUser.FirstName= 'Test Guest User';
            oUser.Lastname = 'Test Guest User';
            oUser.Alias = tAlias;
            oUser.Alias_unique__c = tAlias;
            oUser.Email ='testguestuser@testorg.medtronic.com';
            oUser.Region_vs__c = 'Europe';
            oUser.Sub_Region__c = 'NWE';
            oUser.Country_vs__c = 'BELGIUM';

            oUser.Emailencodingkey = 'UTF-8';
            oUser.Languagelocalekey = 'en_US';
            oUser.Localesidkey = 'en_US';
            oUser.Timezonesidkey = 'America/Los_Angeles';
            oUser.Company_Code_Text__c = 'Eur';
            oUser.User_Business_Unit_vs__c = tUserBusinessUnit;
            if (mapBU_JobTitle.containsKey(tUserBusinessUnit)){
                if (mapBU_JobTitle.get(tUserBusinessUnit).size() > 0){
                    oUser.Job_Title_vs__c = mapBU_JobTitle.get(tUserBusinessUnit)[0];
                }
            }
            oUser.CostCenter__c = '123456';
        if (bInsert) insert oUser;

        return oUser;     

    }

    public static User createUser_SystemAdministrator(String tAlias, Boolean bInsert){

        if (mapBU_JobTitle == null) mapBU_JobTitle = clsUtil.getFieldDependencies('User', 'User_Business_Unit_vs__c', 'Job_Title_vs__c');

        User oUser = new User();
            oUser.Username = tAlias + '@medtronic.com.test';
            oUser.ProfileId = clsUtil.getUserProfileId('System Administrator');
            oUser.UserRoleId = clsUtil.getUserRoleId('System Administrator');
            oUser.FirstName = 'Test System Administrator';
            oUser.Lastname = 'Test System Administrator';
            oUser.Alias = tAlias;
            oUser.Alias_unique__c = tAlias;
            oUser.Email = tAlias + '@medtronic.com.test';
            oUser.Region_vs__c = 'Europe';
            oUser.Sub_Region__c = 'NWE';
            oUser.Country_vs__c = 'BELGIUM';

            oUser.Emailencodingkey = 'UTF-8';
            oUser.Languagelocalekey = 'en_US';
            oUser.Localesidkey = 'en_US';
            oUser.Timezonesidkey = 'America/Los_Angeles';
            oUser.Company_Code_Text__c = 'Eur';
            oUser.User_Business_Unit_vs__c = tUserBusinessUnit;
            if (mapBU_JobTitle.containsKey(tUserBusinessUnit)){
                if (mapBU_JobTitle.get(tUserBusinessUnit).size() > 0){
                    oUser.Job_Title_vs__c = mapBU_JobTitle.get(tUserBusinessUnit)[0];
                }
            }
            oUser.CostCenter__c = '123456';
        if (bInsert) insert oUser;

        return oUser;

    }   


    public static User createUser_SystemAdministratorMDT(String tAlias, Boolean bInsert){

        if (mapBU_JobTitle == null) mapBU_JobTitle = clsUtil.getFieldDependencies('User', 'User_Business_Unit_vs__c', 'Job_Title_vs__c');

        User oUser = new User();
            oUser.Username = tAlias + '@medtronic.com.test';
            oUser.ProfileId = clsUtil.getUserProfileId('System Administrator MDT');
            oUser.UserRoleId = clsUtil.getUserRoleId('System Administrator');
            oUser.FirstName = 'Test System Administrator';
            oUser.Lastname = 'Test System Administrator';
            oUser.Alias = tAlias;
            oUser.Alias_unique__c = tAlias;
            oUser.Email = tAlias + '@medtronic.com.test';
            oUser.Region_vs__c = 'Europe';
            oUser.Sub_Region__c = 'NWE';
            oUser.Country_vs__c = 'BELGIUM';

            oUser.Emailencodingkey = 'UTF-8';
            oUser.Languagelocalekey = 'en_US';
            oUser.Localesidkey = 'en_US';
            oUser.Timezonesidkey = 'America/Los_Angeles';
            oUser.Company_Code_Text__c = 'Eur';
            oUser.User_Business_Unit_vs__c = tUserBusinessUnit;
            if (mapBU_JobTitle.containsKey(tUserBusinessUnit)){
                if (mapBU_JobTitle.get(tUserBusinessUnit).size() > 0){
                    oUser.Job_Title_vs__c = mapBU_JobTitle.get(tUserBusinessUnit)[0];
                }
            }
            oUser.CostCenter__c = '123456';
            oUser.FederationIdentifier = tAlias;
        if (bInsert) insert oUser;

        return oUser;

    }    
    //---------------------------------------------------------------------------------------------------


    //---------------------------------------------------------------------------------------------------
    // Load User Data
    //---------------------------------------------------------------------------------------------------
    public static List<User> loadUser(String tProfileName){
    
        Set<String> setProfileName = new Set<String>();
        if (!clsUtil.isBlank(tProfileName)){
            setProfileName.add(tProfileName);
        }

        return loadUser(1, setProfileName, new Set<String>());

    }
    public static List<User> loadUser(Integer iRecNum, String tProfileName, String tUserRoleName){

        Set<String> setProfileName = new Set<String>();
        if (!clsUtil.isBlank(tProfileName)){
            setProfileName.add(tProfileName);
        }
        Set<String> setUserRoleName = new Set<String>();
        if (!clsUtil.isBlank(tUserRoleName)){
            setUserRoleName.add(tUserRoleName);
        }

        return loadUser(iRecNum, setProfileName, setUserRoleName);

    }
    public static List<User> loadUser(Integer iRecNum, Set<String> setProfileName, Set<String> setUserRoleName){

        String tSOQL = 'SELECT Id, Name, ProfileId, Profile.Name, UserRoleId, UserRole.Name FROM User';
        tSOQL += ' WHERE (IsActive = true)';
        if (setProfileName.size() > 0){
            tSOQL += ' AND (Profile.Name IN :setProfileName)';
        }
        if (setUserRoleName.size() > 0){
            tSOQL += ' AND (UserRole.Name IN :setUserRoleName)';
        }
        tSOQL += ' LIMIT ' + iRecNum;

        return Database.query(tSOQL);

    }
    //---------------------------------------------------------------------------------------------------


}