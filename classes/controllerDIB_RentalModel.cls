/*public class controllerDIB_RentalModel {
/**
 * Creation Date: 	20091125
 * Description: 	controllerDIB_RentalModel  					
 * Author: 	ABSI - Miguel Coimbra & Bob Ceuppens
 */

/*	// Period 
	public String periodChosen ; 
	
	public String getPeriodChosen()	{
			return this.periodChosen ;
	}
	
	public void setPeriodChosen(String s )	{
		this.periodChosen = s;
	}
	
	private DIB_Aggregated_Allocation__c curDIB_Aggregated_Allocation;
	
	public DIB_Aggregated_Allocation__c getDIB_Aggregated_Allocation() {
		return curDIB_Aggregated_Allocation;
	}

	public controllerDIB_RentalModel(String periodChosenIn){
		setPeriodChosen(periodChosenIn);
	}

	public void setDIB_Aggregated_Allocation(DIB_Aggregated_Allocation__c curDIB_Aggregated_AllocationIn) {
		curDIB_Aggregated_Allocation = curDIB_Aggregated_AllocationIn;
	}
	
	private Account[] SFAccounts = new Account[]{};
	
	// Constants 
	public final String ERRORMSG = 'You must enter a value'; 
	
	private List<DIB_Class_AccountAggregation> myDIB_Class_AccountAggregations;
	
	public List<DIB_Class_AccountAggregation> getMyDIB_Class_AccountAggregations() {
		return myDIB_Class_AccountAggregations;
	}

	public void setMyDIB_Class_AccountAggregations(List<DIB_Class_AccountAggregation> myDIB_Class_AccountAggregationsIn) {
		myDIB_Class_AccountAggregations = myDIB_Class_AccountAggregationsIn;
	}
	
	
	/**
	 *	Section : Period : 
	 *//*
	 public List<SelectOption> getPeriodItems() {	 	
		this.periodChosen = DIB_AllocationSharedMethods.getCurrentPeriod();
		return DIB_AllocationSharedMethods.getPeriodLines() ;  
	}
	
	public Id userId ;
	 
	public DIB_Invoice_Line__c invLine = new DIB_Invoice_Line__c() ;
	
	// Fiscal Months PickList values 
	private String months ;
	private String years ;
	private String pumpcgmsChoosen ; 
 	
 	private String country;

	private DIB_Class_AccountAggregation[] ResultPATIENTS;	
	public DIB_Class_AccountAggregation[] getResultPATIENTS(){				return this.ResultPATIENTS ;		}		
	public void setResultPATIENTS(DIB_Class_AccountAggregation[] accsAggr){	this.ResultPATIENTS = accsAggr ; 	}
	
	private DIB_Class_AccountAggregation[] AggregatedDepartmentLines ;
		
	// get the substring of the period wich will be the first 2 characters 
	public String getSelectedMonth(){
		String pickedPeriod = getPeriodChosen();    	
		return pickedPeriod.substring(0, 2);	
	}
		
	// get the substring of year which will be the last 4 characters. 
	public String getSelectedYear(){
		String pickedPeriod = getPeriodChosen();    	
		return pickedPeriod.substring(3, 7); 	
	}	
	 
	public DIB_Invoice_Line__c[] dibILsALL = new DIB_Invoice_Line__c[]{} ;
	
  
	 public void showPATIENTS(){     	
     	// *** PUMPS 
     	DIB_Class_AccountAggregation[]tempAg = getResultPATIENTS();
     	Id [] checkedIdsPUMP = new Id[]{} ; 
     	for (integer i = 0 ; i < tempAg.size() ; i ++){
     		if (tempAg[i].completed.Is_Allocated__c == true){ 
     			checkedIdsPUMP.add(tempAg[i].agg_account.Sold_To__c) ; 
     		} 
     	}
		//setAllTapumps(drawResultToAllocateItems(getResultToAllocateItems(), checkedIdsPUMP, FinalConstants.IL_TYPE_PUMPS));
     }
     

    
	/**
	 * Constructor & get/set Methods 
	 *
	 *//*
	 
	public controllerDIB_RentalModel(ApexPages.Standardcontroller stdController){
		getPeriodItems();
		initiateAggregatedAllocation();
	}
	
	public controllerDIB_RentalModel(){
		getPeriodItems();
		initiateAggregatedAllocation();
	}	
		
	public String getMonths() {							            return months;					    	}
    public void setMonths(String ms) {			            		this.months = ms;					    }
        
    public String getYears() {							            return years;					    	}
    public void setYears(String ms) {			            			this.years = ms;					    }
    
    public String getpumpcgmsChoosen() {							            return pumpcgmsChoosen;					    	}
    public void setpumpcgmsChoosen(String ms) {			            			this.pumpcgmsChoosen = ms;					    }
    
  	public String getCountry() {       			return country;    		}                    
    public void setCountry(String country) { 	this.country = country; }
  
     /**
     * initiateAggregatedAllocation
     *
     * This method does the initialization of the aggregated allocation
     *
    *//*
 	
	  public PageReference initiateAggregatedAllocation() {
	
		// Calculate the aggragated totals for this account
			
		myDIB_Class_AccountAggregations = new List<DIB_Class_AccountAggregation>();
		
		String SelectedMonth = getSelectedMonth();
		String SelectedYear = getSelectedYear();
		
		System.debug(' ################## ' + 'initiateAggregatedAllocation Variables: getSelectedMonth: ' + getSelectedMonth() + ' ################');
		System.debug(' ################## ' + 'initiateAggregatedAllocation Variables: getMonths: ' + getSelectedYear() + ' ################');
		
	  	Map<String,Account> myAccountsDepartments = new Map<String,Account>();
	  	
	  	List<DIB_Department__c> mySegmentations = [SELECT d.Department__c, d.Account__r.Id,d.Account__r.Name 
	  												   FROM DIB_Department__c d
	  												   WHERE d.Account__r.isSales_Force_Account__c = true
	  												   Order BY d.Account__r.Name, d.Department__c limit 400];
	  	
	  	for (DIB_Department__c mySegmentation:mySegmentations) {
	  		myAccountsDepartments.put(mySegmentation.Account__r.Id + mySegmentation.Department__c, mySegmentation.Account__r);
			//System.debug(' ################## ' + 'DIB_Department__c records: Key: ' + mySegmentation.Account__r.Id + mySegmentation.Department__c + ' Account_Name: '  + mySegmentation.Account__r.Name + ' ################');
	  		
	  	}
	  	
	  	// Get a list of DepartmentNames from the Department picklist on DIB_Department__c
	  	// This list will be used afterwards to automatically generate aggregated lines
	  	Schema.DescribeFieldResult fieldResult = DIB_Department__c.Department__c.getDescribe();
		List<Schema.PicklistEntry> departmentsTemp = fieldResult.getPicklistValues();
		List<String> departmentNames = new List<String>();
		for (Schema.PicklistEntry departmentTemp:departmentsTemp) {
			departmentNames.add(departmentTemp.getValue());
		}
		
	  	
	  	
	  	// Select all existing DIB_Aggregated_Allocation__c
	  	DIB_Aggregated_Allocation__c[] myExisting_DIB_Aggregated_Allocations = [Select d.SystemModstamp, d.Sales_Force_Account__c, d.Pumps_No__c, 
	  		d.OwnerId, d.Non_Identified_Placement_No__c, d.Name, d.NPNP_No__c, d.LastModifiedDate, d.LastModifiedById, d.IsDeleted, d.Id, d.Fiscal_Year__c, 
	  		d.Fiscal_Month__c, d.EPNP_No__c, d.Department__c, d.CurrencyIsoCode, d.CreatedDate, d.CreatedById, d.Competitor_Upgrade_No__c, d.CGMS_No__c, 
	  		d.Sales_Force_Account__r.Id From DIB_Aggregated_Allocation__c d
	  		Where d.Fiscal_Month__c =: SelectedMonth and d.Fiscal_Year__c =: SelectedYear
	  		order by d.Sales_Force_Account__r.Name,d.Department__c];
	  	
	  	// The difficulty is that we need to display lines on the screen that may not be in the database
	   	// The idea is to create a map of DIB_Aggregated_Allocation__c
	  	// Key for the map is a concatenation of the Sales_Force_Account__r and Department
	  	// Afterwards we will very easily be able to check if there is a value for a combination of Sales_Force_Account__r / Department
	  	String uniqueAggregationKey;
	  	Map<String,DIB_Aggregated_Allocation__c> myExisting_DIB_Aggregated_AllocationsMap = new Map<String,DIB_Aggregated_Allocation__c>();
	  	for (DIB_Aggregated_Allocation__c myExisting_DIB_Aggregated_Allocation : myExisting_DIB_Aggregated_Allocations) {
	  		uniqueAggregationKey = myExisting_DIB_Aggregated_Allocation.Sales_Force_Account__r.Id + myExisting_DIB_Aggregated_Allocation.Department__c;
	  		myExisting_DIB_Aggregated_AllocationsMap.put(uniqueAggregationKey, myExisting_DIB_Aggregated_Allocation);
	  	}
	  	
	  	System.debug(' ################## ' + 'initiateAggregatedAllocation myExisting_DIB_Aggregated_Allocations.size(): ' + myExisting_DIB_Aggregated_Allocations.size() + ' ################');
	  	
	  	
	  	// Loop over all Accounts and create lines for all Departments
		// Check in the myExisting_DIB_Aggregated_AllocationsMap if there is already a value for this Sales_Force_Account__r / Department combination
		// If so, use that DIB_Aggregated_Allocation__c, if not, create an empty one
		Integer i = 0;
	  	Map<String,String> uniqueAccountIds = new Map<String,String>();
		for (DIB_Department__c mySegmentation:mySegmentations) {
			// Check to see if the account was already processed.
			// If so, jump out, otherwise we will have duplicates
			Account currentAccount = mySegmentation.Account__r;
			if (uniqueAccountIds.containsKey(currentAccount.Id)) {
				System.debug(' ################## ' + ' current Account: ' + currentAccount.Name + ' already processed' + ' ################');
				continue;
			}
			System.debug(' ################## ' + 'Setting up departments for current Account: ' + currentAccount.Name + ' ################');
			List<DIB_Aggregated_Allocation__c> myDIB_Aggregated_Allocations = new List<DIB_Aggregated_Allocation__c>();
			DIB_Class_AccountAggregation myDIB_Class_AccountAggregationTemp = new DIB_Class_AccountAggregation();
			for (String currentDepartment:departmentNames) {
				String accountDepartmentsKey = currentAccount.Id + currentDepartment;
				if (myAccountsDepartments.containsKey(accountDepartmentsKey) ) {
						//System.debug(' ################## ' + 'Existing Department for current Account: ' + currentAccount.Name + ' currentDepartment: ' + currentDepartment + ' accountDepartmentsKey: ' + accountDepartmentsKey + ' ################');
						String uniqueAggregationKeyToCheck = currentAccount.Id + currentDepartment;
						DIB_Aggregated_Allocation__c myDIB_Aggregated_Allocation;
			  			myDIB_Aggregated_Allocation = myExisting_DIB_Aggregated_AllocationsMap.get(uniqueAggregationKeyToCheck);
			  			if (myDIB_Aggregated_Allocation == null) {
			  				myDIB_Aggregated_Allocation  = new DIB_Aggregated_Allocation__c();
			  				myDIB_Aggregated_Allocation.Sales_Force_Account__c = currentAccount.Id;
			  				myDIB_Aggregated_Allocation.Department__c = currentDepartment;
			  				myDIB_Aggregated_Allocation.Fiscal_Month__c = getSelectedMonth();
			  				myDIB_Aggregated_Allocation.Fiscal_Year__c = getSelectedYear();
			  			}
			  			myDIB_Aggregated_Allocations.add(myDIB_Aggregated_Allocation);
				}
			}
	 		myDIB_Class_AccountAggregationTemp.setSales_Force_Account(currentAccount);
	  		myDIB_Class_AccountAggregationTemp.setAggregated_Allocation_Per_Department(myDIB_Aggregated_Allocations);
	  		myDIB_Class_AccountAggregations.add(myDIB_Class_AccountAggregationTemp);
	  		uniqueAccountIds.put(currentAccount.Id,null);
	  	}
  		return null;
  	
  }

	public pageReference save()	{
		// Loop over the DIB_Class_AccountAggregation object and put all the DIB_Aggregated_Allocation__c in a List
		List<DIB_Aggregated_Allocation__c> allDIB_Aggregated_Allocations = new List<DIB_Aggregated_Allocation__c>(); 
		for (DIB_Class_AccountAggregation myDIB_Class_AccountAggregationTemp:myDIB_Class_AccountAggregations) {
			List<DIB_Aggregated_Allocation__c> myDIB_Aggregated_Allocations = myDIB_Class_AccountAggregationTemp.getAggregated_Allocation_Per_Department();
			for (DIB_Aggregated_Allocation__c myDIB_Aggregated_Allocation:myDIB_Aggregated_Allocations) {
				allDIB_Aggregated_Allocations.add(myDIB_Aggregated_Allocation);
			}
		}
		upsert allDIB_Aggregated_Allocations;
		
		/*	
	   		String gotoURL ; 
	   		gotoURL = '/apex/AllocationAggregatedScreen';
	   		PageReference pageRef = new PageReference(gotoURL);
	   		pageRef.setRedirect(true);
	   	*//*
	   	return null;
	}
}*/

public with sharing class controllerDIB_RentalModel {
/**
 * Creation Date: 	20091001
 * Description: 	controllerDIB_RentalModel controls the Visualforce Page AllocationScreen. 
 					This screen will let the users manage the allocation ( edit & view)  of the different Invoice Lines dispatched trough the 
 					different sales rep ( Per Territory, per account, ... )
 					(depending on the Fiscal Year, Fiscal Month, and the account choosed) 
 					As a first step, the user will be able to allocate per order line. A second one will be to build 
 					a solution with the possibility to allocate per aggregation. 
 					
 * Author: 	ABSI - Miguel Coimbra & Bob Ceuppens
 */

	// Period 
	public String periodChosen {set;get;} 
	//Territories
	public String territoryFilter{set; get;}
	//Segments
	public String departmentSegment {set; get;}
	//Account Name Filter	
	public String accountName {set; get;}
	

	// Period 2
	public String periodChosen2 ; 	
	public String getPeriodChosen2()	{							
		String s = this.periodChosen ;
		String result = DIB_AllocationSharedMethods.getFiscalMonthName(s.substring(0,2)) + ' / ' +  s.substring(3,7) ;  	
		return result ; 			
	}
	
	public String selectedAccountType; 
	
	private Set <Id> filteredAccountIds = null;
	
	public String getSelectedAccountType(){
		return selectedAccountType;
	}
	
	public void setSelectedAccountType(String selectedAccountTypeIn)	{
		this.selectedAccountType = selectedAccountTypeIn;
	}
	
	private DIB_Aggregated_Allocation__c curDIB_Aggregated_Allocation;
	
	public DIB_Aggregated_Allocation__c getDIB_Aggregated_Allocation() {
		return curDIB_Aggregated_Allocation;
	}

	public void setDIB_Aggregated_Allocation(DIB_Aggregated_Allocation__c curDIB_Aggregated_AllocationIn) {
		curDIB_Aggregated_Allocation = curDIB_Aggregated_AllocationIn;
	}
	
	private Account[] SFAccounts = new Account[]{};
	
	// Constants 
	public final String ERRORMSG = 'You must enter a value'; 
	
	private List<DIB_Class_AccountAggregation> myDIB_Class_AccountAggregations;
	
	private List<DIB_Class_AccountAggregation> allDIB_Class_AccountAggregations;
	
	public List<DIB_Class_AccountAggregation> getMyDIB_Class_AccountAggregations() {
		return myDIB_Class_AccountAggregations;
	}

	public void setMyDIB_Class_AccountAggregations(List<DIB_Class_AccountAggregation> myDIB_Class_AccountAggregationsIn) {
		myDIB_Class_AccountAggregations = myDIB_Class_AccountAggregationsIn;
	}
		
	/**
	 *	Section : Period : 
	 */
	 public List<SelectOption> getPeriodItems() {
	 	String paramChosenValue = Apexpages.currentPage().getParameters().get(FinalConstants.PARAM_DEFAULT_CHOSEN_PERIOD);
	 	if(paramChosenValue != null && paramChosenValue != ''){
	 		this.periodChosen = paramChosenValue;
	 	}else{	 	
			this.periodChosen = DIB_AllocationSharedMethods.getCurrentPeriod();
	 	}
		return DIB_AllocationSharedMethods.getPeriodLines() ;  
	}
	
	public List<SelectOption> getAccountTypes() {
		return SharedMethods.getAccountTypes();
	}

	// 1. All Invoice lines that still need to be allocated. (for the FY & FM & Account chosen)
	//private DIB_Invoice_Line__c[] ResultToAllocateItems {set; get;}
	
	// 2. All Invoice lines that are already allocated (for the FY & FM & Account chosen)
	//public DIB_Invoice_Line__c[] ResultAllocatedItems {get;set;} 
	
	//assignedAccountItem is a global var assigned by the VF page depending on which link the user clicks ! 
	private Id assignedAccountItem ; 	
	private Id assignedToAllocateItem ; 
	private Id assignedToEditItem ; 
	
	public Id userId ;
	 
	public DIB_Invoice_Line__c invLine = new DIB_Invoice_Line__c() ;
	
	// Fiscal Months PickList values 
	public String months {set;get;}
	public String years {set;get;}
	public String pumpcgmsChoosen {set;get;}
 	
 	private String country;

	// 0. left side accounts available for that fiscal Month and year 
	public DIB_Class_AccountAggregation[] ResultPUMPAccounts {set; get;}
	public DIB_Class_AccountAggregation[] ResultCGMSAccounts {set; get;}
	public DIB_Class_AccountAggregation[] ResultAllAccounts {set; get;}
	
	private DIB_Class_AccountAggregation[] AggregatedDepartmentLines ;
		
	// get the substring of the period wich will be the first 2 characters 
	public String getSelectedMonth(){
		String pickedPeriod = this.PeriodChosen;    	
		return pickedPeriod.substring(0, 2);	
	}
		
	// get the substring of year which will be the last 4 characters. 
	public String getSelectedYear(){
		String pickedPeriod = this.PeriodChosen;   
		return pickedPeriod.substring(3, 7); 	
	}

	// Accounts 
	public String accountFilter {get; set;} 


	//SubmitResult
	public String submitResult{get;set;}	
	
	public DIB_Invoice_Line__c[] dibILsALL = new DIB_Invoice_Line__c[]{} ;
	
    /*public PageReference displaySoldToAccounts(){

		// Get all the invoice Lines for the chosen month & year : 
		dibILsALL = DIB_AllocationSharedMethods.getInvoiceLinesPerPeriode(getSelectedMonth(), getSelectedYear(), true, false);
		  
		// PUMPS :
		DIB_Class_AccountAggregation[] pumps = new DIB_Class_AccountAggregation[]{};
	 	System.debug('################# ' + 'displaySoldToAccounts dibILsALL: ' +  dibILsALL + ' ################') ;
	
		pumps = DIB_AllocationSharedMethods.getSoldToAccountsList(dibILsALL, FinalConstants.IL_TYPE_PUMPS,accountFilter);
		setResultPUMPAccounts(pumps);
				
		// CGMS 
		DIB_Class_AccountAggregation[] cgms = new DIB_Class_AccountAggregation[]{};					
		cgms = DIB_AllocationSharedMethods.getSoldToAccountsList(dibILsALL, FinalConstants.IL_TYPE_CGMS,accountFilter);					
		setResultCGMSAccounts(cgms) ;				
		
		
    	return null ;     	
    }*/
    
    public void displaySoldToShipToAccounts(){

		// Get all the invoice Lines for the chosen month & year :		
		//dibILsALL = DIB_AllocationSharedMethods.getInvoiceLinesPerPeriode(getSelectedMonth(), getSelectedYear(), true, false);
		dibILsALL = DIB_AllocationSharedMethods.getAllInvoiceLinesPerPeriode(getSelectedMonth(), getSelectedYear(), true, false, this.accountName, this.filteredAccountIds);
		
		// PUMPS : 
		DIB_Class_AccountAggregation [] pumps = new DIB_Class_AccountAggregation[]{};
	 	System.debug('################# ' + 'displaySoldToAccounts dibILsALL: ' +  dibILsALL + ' ################') ;
	
		pumps = DIB_AllocationSharedMethods.getSoldToShipToAccountsList(dibILsALL, FinalConstants.IL_TYPE_PUMPS, accountFilter);
		ResultPUMPAccounts = pumps;
				
		// CGMS 
		DIB_Class_AccountAggregation[] cgms = new DIB_Class_AccountAggregation[]{};					
		cgms = DIB_AllocationSharedMethods.getSoldToShipToAccountsList(dibILsALL, FinalConstants.IL_TYPE_CGMS, accountFilter);		
		ResultCGMSAccounts = cgms;				
		
    }    


    
	/**
	 * Constructor & get/set Methods 
	 *
	 */
	/*public controllerDIB_RentalModel(ApexPages.Standardcontroller stdController){
		ResultPUMPAccounts = new List<DIB_Class_AccountAggregation>();
	    ResultCGMSAccounts = new List<DIB_Class_AccountAggregation>();
		ResultAllAccounts = new List<DIB_Class_AccountAggregation>();
		getPeriodItems();
	}*/
		 
	/*public controllerDIB_RentalModel(ApexPages.Standardcontroller stdController, String periodChosenIn){
		ResultPUMPAccounts = new List<DIB_Class_AccountAggregation>();
	    ResultCGMSAccounts = new List<DIB_Class_AccountAggregation>();
		ResultAllAccounts = new List<DIB_Class_AccountAggregation>();		
		setPeriodChosen(periodChosenIn);
	}*/
	
	public controllerDIB_RentalModel(){
		ResultPUMPAccounts = new List<DIB_Class_AccountAggregation>();
	    ResultCGMSAccounts = new List<DIB_Class_AccountAggregation>();
		ResultAllAccounts = new List<DIB_Class_AccountAggregation>();		
		getPeriodItems();
	}	

	public controllerDIB_RentalModel(String periodChosenIn){
		this.periodChosen = periodChosenIn;
	}
	   
  	public String getCountry() {       			return country;    		}                    
    public void setCountry(String country) { 	this.country = country; }
  
     /**
     * initiateAggregatedAllocation
     *
     * This method does the initialization of the aggregated allocation
     *
    */
 	
	public PageReference initiateAggregatedAllocation() {

		// Clear submitResult
		submitResult = '';		
	
		//Get filtered accounts for segment and terrotory
		this.filteredAccountIds = DIB_AllocationSharedMethods.getFilteredAccounts(this.departmentSegment, this.territoryFilter);
	
		// Calculate the aggragated totals for this account
		displaySoldToShipToAccounts();
	
		String currentUserSAPId = DIB_AllocationSharedMethods.getCurrentUserSAPId(System.Userinfo.getUserId());	
	
		allDIB_Class_AccountAggregations = new List<DIB_Class_AccountAggregation>();
		
		String SelectedMonth = getSelectedMonth();
		String SelectedYear = getSelectedYear();
		
		System.debug(' ################## ' + 'initiateAggregatedAllocation Variables: getSelectedMonth: ' + getSelectedMonth() + ' ################');
		System.debug(' ################## ' + 'initiateAggregatedAllocation Variables: getSelectedYear: ' + getSelectedYear() + ' ################');
		
	  	Map<Id,List<DIB_Department__c>> myAccountsDepartments = new Map<Id,List<DIB_Department__c>>();
	  	
	  	String strQuery = 'SELECT d.Department_ID__c,d.Department_ID__r.Name, d.Account__r.Id,d.Account__r.Name,d.Account__r.System_Account__c';
	  	strQuery += ' FROM DIB_Department__c d';
	  	strQuery += ' WHERE d.Account__r.isSales_Force_Account__c = true';
	  	//Segments Filter
	  	if(this.departmentSegment != null && this.departmentSegment != ''){
	  		strQuery += ' AND d.DiB_Segment__c = \'' + this.departmentSegment + '\'';
	  	}
	  	//Account Name Filter
	  	if(this.accountName != null && this.accountName.trim() != ''){
	  		strQuery += ' AND d.Account__r.Name like \'%' + this.accountName.trim() + '%\'';
	  	}
	  	
	  	strQuery += ' Order BY d.Account__r.Name, d.Account__c, d.Department_ID__c limit 200';
	  	List<DIB_Department__c> mySegmentations = Database.query(strQuery);
	  	
	  	System.debug(' ################## ' + 'initiateAggregatedAllocation_mySegmentations.size: ' + mySegmentations.size() + ' ################');
	  	//To maintain the order in the result list
	  	List <Id> orderedAccounts = new List<Id>();
	  	
	  	//Group the same account record together
	  	//Reason - Accounts have different departments
	  	for (DIB_Department__c mySegmentation:mySegmentations) {
	  		List <DIB_Department__c> lDIBDepartment = myAccountsDepartments.get(mySegmentation.Account__r.Id);
	  		if(lDIBDepartment == null){
	  			orderedAccounts.add(mySegmentation.Account__r.Id);
	  			lDIBDepartment = new List<DIB_Department__c>();
	  		}
	  		lDIBDepartment.add(mySegmentation);
	  		myAccountsDepartments.put(mySegmentation.Account__r.Id, lDIBDepartment);
			//System.debug(' ################## ' + 'DIB_Department__c records: Key: ' + mySegmentation.Account__r.Id + mySegmentation.Department__c + ' Account_Name: '  + mySegmentation.Account__r.Name + ' ################');
	  	}
	  	
	  	// Get a list of DepartmentNames from the Department picklist on DIB_Department__c
	  	// This list will be used afterwards to automatically generate aggregated lines
	  	/* Existing logic commented
	  	Schema.DescribeFieldResult fieldResult = DIB_Department__c.Department__c.getDescribe();
		List<Schema.PicklistEntry> departmentsTemp = fieldResult.getPicklistValues();
		List<String> departmentNames = new List<String>();
		for (Schema.PicklistEntry departmentTemp:departmentsTemp) {
			departmentNames.add(departmentTemp.getValue());
		}*/
		
	  	
	  	System.debug(' ################## ' + 'initiateAggregatedAllocation_SelectedMonth: ' + SelectedMonth + ' SelectedYear: ' + SelectedYear + ' ################');
	  	
	  	// Select all existing DIB_Aggregated_Allocation__c
	  	DIB_Aggregated_Allocation__c[] myExisting_DIB_Aggregated_Allocations = [Select d.SystemModstamp, d.Sales_Force_Account__c, /*d.Pumps_No__c,*/ 
	  		d.OwnerId, d.Non_Identified_Placement_No__c, d.Name, d.NPNP_No__c, d.LastModifiedDate, d.LastModifiedById, d.IsDeleted, d.Id, d.Fiscal_Year__c, 
	  		d.Fiscal_Month__c, d.EPNP_No__c, d.CurrencyIsoCode, d.CreatedDate, d.CreatedById, d.Competitor_Upgrade_No__c, d.CGMS_No__c, 
	  		d.Sales_Force_Account__r.Id, d.DIB_Department_ID__c, d.DIB_Department_ID__r.Department_Master_Name__c  From DIB_Aggregated_Allocation__c d
	  		Where d.Fiscal_Month__c =: SelectedMonth and d.Fiscal_Year__c =: SelectedYear
	  		order by d.Sales_Force_Account__r.Name,d.DIB_Department_ID__c];
	  	
	  	System.debug(' ################## ' + 'initiateAggregatedAllocation_myExisting_DIB_Aggregated_Allocations.size: ' + myExisting_DIB_Aggregated_Allocations.size() + ' ################');
	  	
	  	// The difficulty is that we need to display lines on the screen that may not be in the database
	   	// The idea is to create a map of DIB_Aggregated_Allocation__c
	  	// Key for the map is a concatenation of the Sales_Force_Account__r and Department
	  	// Afterwards we will very easily be able to check if there is a value for a combination of Sales_Force_Account__r / Department
	  	/* New Comment - Group allocations for the department (All above is exising logic)*/
	  	Map<Id,DIB_Aggregated_Allocation__c> myExisting_DIB_Aggregated_AllocationsMap = new Map<Id,DIB_Aggregated_Allocation__c>();
	  	for (DIB_Aggregated_Allocation__c myExisting_DIB_Aggregated_Allocation : myExisting_DIB_Aggregated_Allocations) {
	  		//uniqueAggregationKey = myExisting_DIB_Aggregated_Allocation.Sales_Force_Account__r.Id + myExisting_DIB_Aggregated_Allocation.Department__c;
	  		myExisting_DIB_Aggregated_AllocationsMap.put(myExisting_DIB_Aggregated_Allocation.DIB_Department_ID__c, myExisting_DIB_Aggregated_Allocation);
	  	}
	  	
	  	System.debug(' ################## ' + 'initiateAggregatedAllocation myExisting_DIB_Aggregated_Allocations.size(): ' + myExisting_DIB_Aggregated_Allocations.size() + ' ################');
	  	
	  	
	  	// Loop over all Accounts and create lines for all Departments
		// Check in the myExisting_DIB_Aggregated_AllocationsMap if there is already a value for this Sales_Force_Account__r / Department combination
		// If so, use that DIB_Aggregated_Allocation__c, if not, create an empty one
		Integer i = 0;
	  	myDIB_Class_AccountAggregations = new List<DIB_Class_AccountAggregation>();
	  	ResultAllAccounts = new List<DIB_Class_AccountAggregation>();
	  	Boolean accountSelected = true;
	  	//Generate the result list
		for (Id currentAccountId:orderedAccounts) {
			// Check to see if the account was already processed.
			// If so, jump out, otherwise we will have duplicates
			//Check if the account is available in the filter
			if(filteredAccountIds != null && !filteredAccountIds.contains(currentAccountId)){
				continue;
			}
			//System.debug(' ################## ' + 'Setting up departments for current Account: ' + currentAccount.Name + ' ################');
			List<DIB_Aggregated_Allocation__c> myDIB_Aggregated_Allocations = new List<DIB_Aggregated_Allocation__c>();
			DIB_Class_AccountAggregation myDIB_Class_AccountAggregationTemp = new DIB_Class_AccountAggregation();
			Double cgmsAllocationCount = 0;
			Double otherAllocationCount = 0;
			List <DIB_Department__c> lDIBDepartment = myAccountsDepartments.get(currentAccountId);
			Account currentAccount = null;
			for (DIB_Department__c mySegmentation:lDIBDepartment) {
				/*String accountDepartmentsKey = currentAccount.Id + currentDepartment;
				if (myAccountsDepartments.containsKey(accountDepartmentsKey) ) {*/
				//System.debug(' ################## ' + 'Existing Department for current Account: ' + currentAccount.Name + ' currentDepartment: ' + currentDepartment + ' accountDepartmentsKey: ' + accountDepartmentsKey + ' ################');
				currentAccount = mySegmentation.Account__r;
				DIB_Aggregated_Allocation__c myDIB_Aggregated_Allocation;
		  		myDIB_Aggregated_Allocation = myExisting_DIB_Aggregated_AllocationsMap.get(mySegmentation.Id);
		  		if (myDIB_Aggregated_Allocation == null) {
		  			myDIB_Aggregated_Allocation  = new DIB_Aggregated_Allocation__c();
		  			myDIB_Aggregated_Allocation.Sales_Force_Account__c = currentAccount.Id;
		  			//myDIB_Aggregated_Allocation.Department__c = mySegmentation.Department_ID__r.Name;
		  			myDIB_Aggregated_Allocation.Fiscal_Month__c = getSelectedMonth();
		  			myDIB_Aggregated_Allocation.Fiscal_Year__c = getSelectedYear();
		  			myDIB_Aggregated_Allocation.DIB_Department_ID__c = mySegmentation.Id;
		  		}else{
		  			cgmsAllocationCount += myDIB_Aggregated_Allocation.CGMS_No__c;
		  			otherAllocationCount += myDIB_Aggregated_Allocation.NPNP_No__c;
		  			otherAllocationCount += myDIB_Aggregated_Allocation.EPNP_No__c;
		  			otherAllocationCount += myDIB_Aggregated_Allocation.Competitor_Upgrade_No__c;
		  			otherAllocationCount += myDIB_Aggregated_Allocation.Non_Identified_Placement_No__c;
		  		}
		  		myDIB_Aggregated_Allocations.add(myDIB_Aggregated_Allocation);
			}
	 		myDIB_Class_AccountAggregationTemp.setSales_Force_Account(currentAccount);
	  		myDIB_Class_AccountAggregationTemp.setAggregated_Allocation_Per_Department(myDIB_Aggregated_Allocations);
	  		allDIB_Class_AccountAggregations.add(myDIB_Class_AccountAggregationTemp);
	  		System.debug(' ################## ' + 'myDIB_Class_AccountAggregations.size() ' + allDIB_Class_AccountAggregations.size() + ' ################');
	  		
	  		DIB_Class_AccountAggregation accountSelectList = new DIB_Class_AccountAggregation(currentAccount.Id, cgmsAllocationCount, otherAllocationCount, accountSelected);
	  		accountSelected = false;
	  		ResultAllAccounts.add(accountSelectList);
	  	}
	  	
	  	//Filter the allocation records by accounts that are selected
	  	filterAllocations();
	  	
	  	// Submit button display or not 
		checkShowHideSubmitButton(); 
		
  		return null;
	}
	/**
	* Show allocations based on account filter
	*/
	public void filterAllocations(){
		Set <Id> sAccountIds = new Set<Id>();
		myDIB_Class_AccountAggregations = new List<DIB_Class_AccountAggregation>();
		for(DIB_Class_AccountAggregation dibClass_AccountAggregation:ResultAllAccounts){
			if(dibClass_AccountAggregation.agg_account.Is_Allocated__c){
				sAccountIds.add(dibClass_AccountAggregation.agg_account.Sold_To__c);
			}
		}	
		for(DIB_Class_AccountAggregation myDIB_Class_AccountAggregationTemp:allDIB_Class_AccountAggregations){
			if(sAccountIds.contains(myDIB_Class_AccountAggregationTemp.getSales_Force_Account().Id)){
				myDIB_Class_AccountAggregations.add(myDIB_Class_AccountAggregationTemp);
			}
		}
	}
	
	 // Submit button : Submit current Month & Year and all accounts for the current user : 
    public void SubmitcurrentPeriod(){
    	if (DIB_Manage_Task_Status.submitSalesEntry(this.getSelectedMonth(), this.getSelectedYear())) {
    		submitResult = FinalConstants.DiB_submitResultSuccess;
    	}
    	else {
    		submitResult = FinalConstants.DiB_submitResultFailure;
    	}
    }
  	
  	// Show or Hide the button Submit Depeding on the selected Fiscal Period 
	private Boolean showSubmitButton; 
	public Boolean getshowSubmitButton(){					return this.showSubmitButton ; 	}
	public void setshowSubmitButton(Boolean b){				this.showSubmitButton = b;	 	}	
	
  	public void checkShowHideSubmitButton(){
		System.debug('######################## showHideSubmitButton call method : ' ) ; 
		setshowSubmitButton(DIB_Manage_Task_Status.showHideSubmitButton(getSelectedMonth(), getSelectedYear())) ; 
	}
  
  
	// Save all current page : 
	public pageReference save()	{
		// Clear submitResult
		submitResult = '';
		// Loop over the DIB_Class_AccountAggregation object and put all the DIB_Aggregated_Allocation__c in a List
		List<DIB_Aggregated_Allocation__c> allDIB_Aggregated_Allocations = new List<DIB_Aggregated_Allocation__c>(); 
		for (DIB_Class_AccountAggregation myDIB_Class_AccountAggregationTemp:myDIB_Class_AccountAggregations) {
			List<DIB_Aggregated_Allocation__c> myDIB_Aggregated_Allocations = myDIB_Class_AccountAggregationTemp.getAggregated_Allocation_Per_Department();
			for (DIB_Aggregated_Allocation__c myDIB_Aggregated_Allocation:myDIB_Aggregated_Allocations) {
				if (myDIB_Aggregated_Allocation.NPNP_No__c <> null || myDIB_Aggregated_Allocation.EPNP_No__c <> null || 
				myDIB_Aggregated_Allocation.Competitor_Upgrade_No__c <> null || myDIB_Aggregated_Allocation.Non_Identified_Placement_No__c <> null ||
				myDIB_Aggregated_Allocation.CGMS_No__c <> null) {
					if (myDIB_Aggregated_Allocation.NPNP_No__c == null) {
						myDIB_Aggregated_Allocation.NPNP_No__c = 0;
					}
					if (myDIB_Aggregated_Allocation.EPNP_No__c == null) {
						myDIB_Aggregated_Allocation.EPNP_No__c = 0;
					}
					if (myDIB_Aggregated_Allocation.Competitor_Upgrade_No__c == null) {
						myDIB_Aggregated_Allocation.Competitor_Upgrade_No__c = 0;
					}
					if (myDIB_Aggregated_Allocation.Non_Identified_Placement_No__c == null) {
						myDIB_Aggregated_Allocation.Non_Identified_Placement_No__c = 0;
					}
					if (myDIB_Aggregated_Allocation.CGMS_No__c == null) {
						myDIB_Aggregated_Allocation.CGMS_No__c = 0;
					}					
					allDIB_Aggregated_Allocations.add(myDIB_Aggregated_Allocation);
				}
			}
		}
		upsert allDIB_Aggregated_Allocations;
		return null;
	} 
     
   	/**
 	*	############################# Section : Territory Filter : ##################################################### 
 	*/
	public List<SelectOption> getTerritoryFilterOptions() {
		return DIB_AllocationSharedMethods.getTerritoryFilterOptions();
	}

   	/**
 	*	############################# Section : Segment Filter : ##################################################### 
 	*/
	public List<SelectOption> getSegmentFilterOptions() {
		return DIB_AllocationSharedMethods.getSegmentFilterOptions();
	}
      
}