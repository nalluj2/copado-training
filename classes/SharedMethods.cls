/**
 * Creation Date:   20090219
 * Description:     Some Methods are used in different Triggers, This class will unify it ! 
 *              
 * Author:          ABSI - MC
 */
 
public class SharedMethods {    

    //-BC - 20140428 - CR-27777 - Added a map to store the selected Call_Topic_Settings__c and the needed fields of these records (like Opportunity_Required__c) - START
    public static Map<Id, Call_Topic_Settings__c> mapCallTopicSetting = new Map<Id, Call_Topic_Settings__c>();  
    //-BC - 20140428 - CR-27777 - Added a map to store the selected Call_Topic_Settings__c and the needed fields of these records (like Opportunity_Required__c) - STOP

 /** 
   * Description : Used in ContactAfterInsert & ContactAfterUpdate 
   *
   */
   
    public static List<Affiliation__c> createAffiliation(List<Contact> contacts){
        
        List<Affiliation__c> affsToInsert = new List<Affiliation__c>();  
         
        for(Contact cnt : contacts){      
            
            Affiliation__c aff  = new Affiliation__c();             
            aff.Affiliation_From_Contact__c = cnt.Id ; 
            aff.Affiliation_To_Account__c = cnt.AccountId ; 
            aff.Affiliation_Primary__c = true ; 
            aff.Affiliation_Start_Date__c = System.today();
            aff.Affiliation_Type__c = cnt.Affiliation_To_Account__c ;
            aff.Affiliation_Position_In_Account__c = cnt.Primary_Job_Title_vs__c ;
            aff.Department_Description__c = cnt.Department_Description__c;
            aff.Buying_influence_role__c = cnt.Buying_influence_role__c;
            aff.Degree_of_Influence__c = cnt.Degree_of_Influence__c;
            aff.Office_Phone__c = cnt.Phone ;            
            aff.RecordTypeId = FinalConstants.recordTypeIdC2A ;
            aff.Department__c = cnt.Contact_Department__c; 
                    
            affsToInsert.add(aff) ;            
        }
           
        return affsToInsert ;   
    }
 
    public static RecordType getRecordType(Id recordTypeId,String sObjectTypeName) {
            RecordType recordType = [select Id,Name from RecordType where sObjectType =: sObjectTypeName and Id =: recordTypeId];
            return recordType;
    }
    
 /**
 *  Description: getContactAccountAffiliations gets a List of SelectOptions based on a contactId
 *  Used for various autofill picklist fields
 */
        
    public static List<SelectOption> getContactAccountAffiliations(String contactId) {
        System.debug(' ################## setAccountNamesFromAffiliations ################');
        List<SelectOption> AffAccountNames = new List<SelectOption>();
        AffAccountNames.add(new SelectOption('','--None--'));
        if (contactId==null) {
            return AffAccountNames;
        }
        List<Affiliation__c> affs = [SELECT Affiliation_To_Account__r.Name FROM Affiliation__c
                                        where RecordTypeId =: FinalConstants.recordTypeIdC2A
                                        and Affiliation_Active__c = true
                                        and (Affiliation_Type__c <> 'Referring')
                                        and Affiliation_From_Contact__r.Id=:contactID];
        for(Affiliation__c aff : affs){
                String id = aff.Affiliation_To_Account__r.Id;
                String name = aff.Affiliation_To_Account__r.Name;
                if (id == null) {
                    continue;
                }
                System.debug(' ################## ' + 'Account Name= ' + name + ' ################');
                AffAccountNames.add(new SelectOption(id,name));
        }
        System.debug(' ################## ' + 'AffAccountNames.size =' + AffAccountNames.size() + ' ################');
        return AffAccountNames;
    }
    
    public static Affiliation__c getContactAccAffiObjs(String contactId, String AccId) {
        Affiliation__c affs = new Affiliation__c();
        system.debug('contactId is-----> '+contactId);
        system.debug('Acc is-----> '+AccId);
        try {
                if (contactId==null) {
                    return affs;
                }
                affs = [SELECT Id,Affiliation_To_Account__r.Name,Affiliation_From_Contact__r.Id,Affiliation_Primary__c,Department__c,Affiliation_Position_In_Account__c,Affiliation_From_Contact__r.Contact_Primary_Specialty__c FROM Affiliation__c
                                                where RecordTypeId =: FinalConstants.recordTypeIdC2A
                                                and Affiliation_Active__c = true
                                                and (Affiliation_Type__c <> 'Referring')
                                                and Affiliation_From_Contact__r.Id=:contactID
                                                and Affiliation_To_Account__r.Id=:AccId limit 1];
               // system.debug('Affilation is ------------------------------->'+affs);                                
                return affs;
                
        } catch(Exception ex){
                ApexPages.addMessages(ex);
                system.debug('Error msg is ------------->'+ApexPages.addMessages(ex));
                return null;
          }      
    }
        
    // Get the affiliated primary account to a related Contact Id : 
    public static String getPrimaryAffiliatedAccount(Id contactId){
       
       try  {
                Affiliation__c[] aff =  [Select Affiliation_To_Account__r.Id, affiliation_To_Account__r.Name from Affiliation__c 
                                where RecordTypeId =: FinalConstants.recordTypeIdC2A
                                and Affiliation_primary__c = true
                                and (Affiliation_Type__c <> 'Referring')
                                and Affiliation_From_Contact__r.Id =:contactId limit 1] ; 
                if (aff.size() > 0 ){
                    return aff[0].Affiliation_To_Account__r.Id ; 
                }else{
                    return null ; 
                }
       }  catch(Exception ex){
                ApexPages.addMessages(ex);
                system.debug('Error msg is ------------->'+ApexPages.addMessages(ex));
                return null;
          }       
    }
    
    public static Contact getContactFromContactId(Id contactId) {
        System.debug(' ################## getContactFromContactId ################');
        if (contactId == null) {
            return new Contact();
        }
        return [SELECT Id,Name FROM Contact where Id =: contactId];
    }

    public static Account getAccountFromAccountId(Id accountId) {
        System.debug(' ################## getAccountFromAccountId ################');
        if (accountId == null) {
            return new Account();
        }
        return [SELECT Id,Name FROM Account a where Id =: accountId AND a.Account_Active__c = true];
    }
    
 /**
 *  Description: getTherapies gets a list of therapies based on the user profile
 */
        
    public static List<SelectOption> getTherapies(String BU) {
        System.debug(' ################## getTherapies ################');
        try {
            List<SelectOption> therapiesOptions = new List<SelectOption>();
            therapiesOptions.add(new SelectOption('','--None--'));
            if (BU != null){
                List<Therapy__c> therapies = [Select t.Id, t.Name From Therapy__c t where Business_Unit__c =: BU and Active__c = true order by name];
                for(Therapy__c therapy : therapies){
                        String id = therapy.Id;
                        String name = therapy.Name;
                        if (id == null) {
                            continue;
                        }
                        System.debug(' ################## ' + 'Account Name= ' + name + ' ################');
                        therapiesOptions.add(new SelectOption(id,name));
                }
            }
            return therapiesOptions;
            
       }  catch (Exception ex){
                ApexPages.addMessages(ex);
                return null;
          }      
    }

 /**
 *  Description: getProductGroups gets a list of therapies based on a therapy
 */
        
    public static List<SelectOption> getProductGroups(String therapy) {
        System.debug(' ################## getProductGroups ################');
        try {
            List<SelectOption> pGroupOptions = new List<SelectOption>();
            pGroupOptions.add(new SelectOption('','--None--'));
            if (therapy != null){
                List<Product_Group__c> pGroups = [Select Id, Name From Product_Group__c where Therapy_ID__c =: therapy and Active__c = true order by name];
                for(Product_Group__c pGroup : pGroups){
                        String id = pGroup.Id;
                        String name = pGroup.Name;
                        if (id == null) {
                            continue;
                        }
                        System.debug(' ################## ' + 'Account Name= ' + name + ' ################');
                        pGroupOptions.add(new SelectOption(id,name));
                }
            }
            return pGroupOptions;
            
       }  catch (Exception ex){
                ApexPages.addMessages(ex);
                return null;
          }      
    }  
       
  /*Manish:start commented in order to delete 'Relation Type' object 
  
   public static List<SelectOption> getAffiliationTypes(String rtName){
        List<SelectOption> typesOptions = new List<SelectOption>();
        typesOptions.add(new SelectOption('','--None--')); 
        List<Affiliation_Type__c> afftypes ; 
        if (rtName == 'A2A'){
            afftypes = [Select a.Name, a.C2C__c, a.C2A__c, a.A2A__c From Affiliation_Type__c a where  a.A2A__c = true order by name];
        }else if (rtName == 'Reffering'){
            afftypes = [Select a.Name, a.C2C__c, a.C2A__c, a.A2A__c From Affiliation_Type__c a where  a.C2C__c = true order by name];
        }else{ // C2A
            afftypes = [Select a.Name, a.C2C__c, a.C2A__c, a.A2A__c From Affiliation_Type__c a where  a.C2A__c = true order by name];
        }
        for(Affiliation_Type__c at: afftypes){
                String id = at.Name;
                String name = at.Name;
                if (id == null) {
                    continue;
                }
                typesOptions.add(new SelectOption(id,name));
        }
        return typesOptions; 
    }   
  Manish:End comment in order to delete 'Relation Type' object.
  */
   
    /**
     *      Business Unit 
     */
     
    
    public static Id getBUlookup(){
        
        try { 
        
            Id userId = UserInfo.getUserId();
            String userBuName = [Select User_Business_Unit_vs__c from User where id=:userId].User_Business_Unit_vs__c;
            System.debug('#####################'+ userBuName + '##################');  
            if (userBuName != null){
                Business_Unit__c butoset = [Select id from Business_Unit__c where name=:userBuName limit 1] ; 
                if (butoset != null ){
                    return butoset.id ;
                }else{
                    return null;
                }
            }else{
                return null; 
            }
        
        } catch (DmlException ex){
              ApexPages.addMessages(ex);
              return null;
          }      
                               
    }

    public static map<id,string> getUserBusinessUnits(){
        try {       
            Id userId = UserInfo.getUserId();
            
            map<id,string> mapUserBUs = new map<id,string>();
            List<User_Business_Unit__c> lstUserBUs = [Select Business_Unit_Id__c, Business_Unit_text__c From User_Business_Unit__c where User__c=:userId order by Business_Unit_text__c];
            for (User_Business_Unit__c UserBU:lstUserBUs) {
                mapUserBUs.put(UserBU.Business_Unit_Id__c,UserBU.Business_Unit_text__c); 
            }
            return mapUserBUs;    
        } catch (DmlException ex){
              ApexPages.addMessages(ex);
              return null;
        }               
    }
    
    public static map<id,string> getUserBusinessUnitGroups(){
        try {       
            Id userId = UserInfo.getUserId();
            
            map<id,string> mapUserBUGs = new map<id,string>();
            List<User_Business_Unit__c> lstUserBUs = [Select Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__c, Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__r.name From User_Business_Unit__c where User__c=:userId 
            order by Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__r.name];
            for (User_Business_Unit__c UserBU:lstUserBUs) {
                if(!mapUserBUGs.containsKey(UserBU.Sub_Business_Unit__r.Business_Unit__c)){
                	 mapUserBUGs.put(UserBU.Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__c,UserBU.Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__r.name); 
                }
            }
            return mapUserBUGs;    
        } catch (DmlException ex){
              ApexPages.addMessages(ex);
              return null;
        }               
    }
    
    
    //Returns a map with key = value and value = translation
    public static Map<String, String> getPickValues(Sobject object_name, String field_name) {
      Map<String,String> mapTranslationValue = new Map<String,String>();
      Schema.sObjectType sobject_type = object_name.getSObjectType(); //grab the sobject that was passed
      Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
      Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
      List<Schema.PicklistEntry> pick_list_values = field_map.get(field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
      for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
         mapTranslationValue.put(a.getValue(),a.getLabel()); //add the value and label to our final list
      }
      return mapTranslationValue; //return the List
    }

    public static id getUserPrimaryBUG(){
        try {       
            Id userId = UserInfo.getUserId();
            Id IdUserPrimaryBUG=null; 
            
            List<User_Business_Unit__c> lstUserBUs = [Select Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__c From User_Business_Unit__c where User__c=:userId and Primary__c=:true limit 1];
            for (User_Business_Unit__c UserBU:lstUserBUs) {
                IdUserPrimaryBUG = UserBU.Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__c;
            }
            return IdUserPrimaryBUG;    
        } catch (DmlException ex){
              ApexPages.addMessages(ex);
              return null;
        }               
    }
    
    public static id getUserPrimaryBU(){
        try {       
            Id userId = UserInfo.getUserId();
            Id IdUserPrimaryBU=null; 
            
            List<User_Business_Unit__c> lstUserBUs = [Select Business_Unit_Id__c From User_Business_Unit__c where User__c=:userId and Primary__c=:true limit 1];
            for (User_Business_Unit__c UserBU:lstUserBUs) {
                IdUserPrimaryBU = UserBU.Business_Unit_Id__c;
            }
            return IdUserPrimaryBU;    
        } catch (DmlException ex){
              ApexPages.addMessages(ex);
              return null;
        }               
    }

     public static id getUserPrimarySBU(){
        try {       
            Id userId = UserInfo.getUserId();
            Id IdUserPrimarySBU=null; 
            
            List<User_Business_Unit__c> lstUserSBUs = [Select Sub_Business_Unit__c From User_Business_Unit__c where User__c=:userId and Primary__c=:true limit 1];
            for (User_Business_Unit__c UserSBU:lstUserSBUs) {
                IdUserPrimarySBU = UserSBU.Sub_Business_Unit__c;
            }
            return IdUserPrimarySBU;    
        } catch (DmlException ex){
              ApexPages.addMessages(ex);
              return null;
        }               
    }
    
    public static map<id,string> getCompanies(){
        try {       
            map<id,string> mapCompanies = new map<id,string>();
            List<Company__c> lstCompanies = [Select id, name From Company__c order by name];
            for (Company__c Company:lstCompanies) {
                mapCompanies.put(Company.id,Company.name); 
            }
            return mapCompanies;    
        } catch (DmlException ex){
              ApexPages.addMessages(ex);
              return null;
        }                       
    }

    public static map<id,string> getUserCompany(){
        try {
            Id userId = UserInfo.getUserId();
                    
            map<id,string> mapUserCompany = new map<id,string>();
            List<User_Business_Unit__c> lstUserBUs = [Select Sub_Business_Unit__r.Business_Unit__r.Company__r.Id,Sub_Business_Unit__r.Business_Unit__r.Company__r.Name From User_Business_Unit__c where User__c=:userId limit 1];
            for (User_Business_Unit__c UserBU:lstUserBUs) {
                mapUserCompany.put(UserBU.Sub_Business_Unit__r.Business_Unit__r.Company__r.Id,UserBU.Sub_Business_Unit__r.Business_Unit__r.Company__r.Name); 
            }
            return mapUserCompany;    
        } catch (DmlException ex){
              ApexPages.addMessages(ex);
              return null;
        }                       
    }
 
     /**
     *  Section : Type
     */
     
     public static List<SelectOption> getAccountTypes() {
        List<DIB_System_Department__c> deps = [Select d.Name__c From DIB_System_Department__c d order by Picklist_Order__c];
        List<SelectOption> options = new List<SelectOption>();
        for (DIB_System_Department__c dep:deps) {
            options.add(new SelectOption(dep.Name__c,dep.Name__c)); 
        }
        return options;
     }
     
     /**
      *  Description: getActivityTypes gets a list of Activities based on the user profile
      */
      public static List<SelectOption> getActivities(String BU, String tUser_Region) {
       try {  
            List<SelectOption> activitiesOptions = new List<SelectOption>();
            activitiesOptions.add(new SelectOption('','--None--'));
            if (BU != null && BU !=''){
                 // Should be able to get the Unique records for Call Activity Type based on BU
                List<Call_Topic_Settings__c> activities = 
                    [
                        SELECT
                            Call_activity_type__c
                            , Call_activity_type__r.name
                            , Business_Unit__c
                            , Opportunity_Required__c
                        FROM 
                            Call_Topic_Settings__c
                        WHERE 
                            Region_vs__c = :tUser_Region
                            AND Business_Unit__c = :BU 
                            AND Call_activity_type__c != null
                            AND Call_activity_type__c != ''
                        ORDER BY 
                            Call_activity_type__r.name 
                    ];
                                                 
                if (activities.size()>0) {                                 
                    for (Call_Topic_Settings__c cs : activities){
                        String Id = cs.Call_activity_type__c;
                        String name=cs.Call_activity_type__r.name;
                        activitiesOptions.add(new SelectOption(Id,Name));
                        mapCallTopicSetting.put(cs.Id, cs);
                    }
                }
           }     
            
            return activitiesOptions;
            
       } catch(DmlException ex) {
            ApexPages.addMessages(ex);
            return null;
       }     

    }
    
    
    /**
      *  Description: getCategory function gets a Unique Category based on the selected Activity
      */
    public static String getCategory(String BU,String activity) {
        String categoryStr=''; 
        try { 
                if ((BU != null && BU !='') && (activity!=null && activity!='')){
                   
                   system.debug('BU and Activity both are not null-------------->'); 
                   Call_Topic_Settings__c activities = [Select Call_Category__c,Call_Category__r.name, Business_Unit__c,Call_activity_type__c from Call_Topic_Settings__c where Business_Unit__c =: BU and Call_activity_type__c =:activity limit 1];
                   categoryStr=activities.Call_Category__c;
                }    
                
                return categoryStr;
                
         } catch (DmlException ex){
                ApexPages.addMessages(ex);
                return null;
          }         
    } 
    
    /**
      *  Description: getSubject function gets a list of Subjects based on the selected Activity
      */
    public static List<SelectOption> getSubjects(String activity, String BU, String tRegion) {
        
        try {
             List<SelectOption> subjectOptions = new List<SelectOption>();
              subjectOptions.add(new SelectOption('','--None--'));
                
               if ((BU != null && BU !='')&&(activity!=null && activity!='')){
                
					system.debug('Inside if---Activity is not null--------->'+activity); 
					//List<Call_topic_Setting_Details__c> sObj= [Select Subject_Id__c,Subject_Id__r.Name From Call_topic_Setting_Details__c where Call_Topic_Setting_Id__r.Call_activity_type__c =:activity];
	                //List<Call_topic_Setting_Details__c> sObj= [Select Subject__c,Subject__r.Name From Call_topic_Setting_Details__c where Call_Topic_Setting__r.Business_Unit__c=:BU and Call_Topic_Setting__r.Call_activity_type__c =:activity order by Subject__r.Name];
					List<Call_topic_Setting_Details__c> sObj= 
						[
							SELECT 
								Subject__c,Subject__r.Name
							FROM
								Call_topic_Setting_Details__c 
							WHERE
								Call_Topic_Setting__r.Business_Unit__c = :BU 
								AND Call_Topic_Setting__r.Call_activity_type__c = :activity 
	                            AND Call_Topic_Setting__r.Region_vs__c = :tRegion
							ORDER BY 
								Subject__r.Name
						];

                system.debug('sObj list size is ------------->'+sObj.size());
                system.debug('sObj is---->'+sObj);
              
                for (Call_topic_Setting_Details__c ctd:sObj) {
                
                system.debug('inside for sObjElements are ------------->'+sObj);      
                     String Id = ctd.Subject__c;
                     String name=ctd.Subject__r.Name;
                     
                     system.debug('Id is--------->'+Id);
                     //system.debug('ctd.Subject_Id__c---------->'+ctd.Subject_Id__c);
                     system.debug('name is--------->'+name);
                     
                     if (Id==null){
                         continue;
                     }
                     subjectOptions.add(new SelectOption(Id,name));     
                }
                system.debug('subjectOptions list is------> '+subjectOptions);
                
             }
             return subjectOptions; 
             
        } catch (DmlException ex){
              ApexPages.addMessages(ex);
              return null;
          }        
   
    }
    
    /**
      *  Description: getProduct function gets a list of Products based on the selected Theraphy
      */
        
     public static List<SelectOption> getProducts(String theraphy) {
        System.debug(' ################## getTherapies ################');
        try {
        
                List<SelectOption> productOptions = new List<SelectOption>();
                productOptions.add(new SelectOption('','--None--'));
                if(theraphy!=null && theraphy!=''){
                    List<Therapy_Product__c> therapiesProd = [Select Product__c, Product__r.Name From Therapy_Product__c where Therapy__c =: theraphy order by Product__r.Name];
                    system.debug('therapiesProd are---------------->'+therapiesProd.size());
                    for(Therapy_Product__c tp: therapiesProd ){
                            String id = tp.Product__c;
                            String name = tp.Product__r.Name;
                            if (id == null) {
                                continue;
                            }
                            System.debug(' ################## ' + 'Account Name= ' + name + ' ################');
                            productOptions.add(new SelectOption(id,name));
                    }
                }
                
                return productOptions;
                
        } catch (DmlException ex){
              ApexPages.addMessages(ex);
              return null;
          }        
    } 

/** *  Description: getDepartment gets a list of Departments from Affiliation */            
    public static List<SelectOption> getDepartment() {        
       
       try { 
            List<SelectOption> depOptions = new List<SelectOption>();        
            depOptions.add(new SelectOption('','--None--'));        
            Schema.DescribeFieldResult fieldResult =
                     Affiliation__c.Department__c.getDescribe();
            List<Schema.PicklistEntry> branch = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry f : branch)
            {
              depOptions.add(new SelectOption(f.getValue(), f.getValue()));
            }
                   
            return depOptions;
                
       } catch (DmlException ex) {
              ApexPages.addMessages(ex);
              return null;
       }     
    }
    
    
    public List<Profile> getAdministratorProfiles(){
        return [select Id, Name from Profile where Name in ('System Administrator','System Administrator MDT',
        'EUR DiB Super User Unit/Entry','EUR Account Data Steward','Data Loader DiB')]; 
    } 
    
    /**
    *
    * Check if the user has the edit and delete previlege
    */
    public Boolean checkEditDeletePermission(Id useProfileId){
        
        List<Profile> profiles = getAdministratorProfiles();
        
        for (Profile profile : profiles){
            if (profile.id == useProfileId){
                return false;
                break;
            }
        }
        
        return true;
    }        
    
    private static Set<String> mmxCountries {get; set;}
    
    public static Map<String,String> retIntegrateWithMMXCountries(String countryName){
        
        if(mmxCountries == null){
        	
        	mmxCountries = new Set<String>();
        	
        	for(String country : Integrate_With_MMX_Countries__c.getAll().keySet()){
        		mmxCountries.add(country.toUpperCase());
        	}
        }
        
        Map<String,String> result = new Map<String,String>();
                  
        if(countryName != null && mmxCountries.contains(countryName.toUpperCase())) result.put(countryName.toUpperCase(), countryName.toUpperCase());          
                       
        return result;
    }
    public static set<string> retExcludefromAccountFlagging(){
        set<string> setExcludeCountries = new set<string>();
        list<DIB_Country__c> listExcludeCountries = [Select Name from DIB_Country__c where Exclude_From_Batch_Code_Checkbox__c=:true];
        if(listExcludeCountries.size()>0){
            for(DIB_Country__c exc:listExcludeCountries){
                setExcludeCountries.add(exc.Name);
            }
        }
        System.Debug('>>>>>setExcludeCountries - ' + setExcludeCountries);
        return setExcludeCountries;
    }
    
    public static String CurrencyFormatEurope(String strData){
        return CurrencyFormat(strData, '.', ',');
    }
    
    public static String CurrencyFormatUS(String strData){
        return CurrencyFormat(strData, ',', '.');
    }

    public static String CurrencyFormatByLocale(String strData, String locale){
        //Add locales to list if needed
        if (locale!=null && locale.compareTo('en_US')==0){
            return CurrencyFormatUS(strData);
        }else{
            return CurrencyFormatEurope(strData);
        }
        
    }
    
    public static string CurrencyFormat(string strData, string strThousandSeparator, string strDecimalSeparator){
        string strSubStr1; 
        string strSubStr2; 
        integer lio;
        integer lioNeg;
        boolean bl=false;
        integer count=0;
        string strFormat = strData;
        string reverseFinal='';
        string finalresult='';

        if(strFormat.lastIndexOf('-')>-1){
            bl=true;
            strFormat = strFormat.subString(1,strFormat.length());
        }
    
        lio = strFormat.lastIndexOf('.');

        if(lio >-1){ 
            strSubStr1 = strFormat.subString(lio,strFormat.length());
            strSubStr2 = strFormat.subString(0,lio);
        }else{
            strSubStr1 = '';
            strSubStr2 = strFormat;
        }

        integer alen = strSubStr2.length();

        if(alen>=4){
            for (integer i = alen-1; i >= 0 ; i--){
                if(count>0 && math.mod(count,3)==0){
                    if(strSubStr2.substring(i,i+1)!=null){
                        reverseFinal =  reverseFinal + strThousandSeparator + strSubStr2.substring(i,i+1);
                    }
                }else{
                    if(strSubStr2.substring(i,i+1)!=null){
                        reverseFinal =  reverseFinal + strSubStr2.substring(i,i+1);
                    }
                }
            count = count+1;
            }
            for (integer i = reverseFinal.length()-1 ; i >=0 ; i--){
                if(reverseFinal.substring(i,i+1)!=null){
                    finalresult =  finalresult + reverseFinal.substring(i,i+1);
                }
            }
        }else{
            finalresult=strSubStr2;
        }
    
        strSubStr1=strSubStr1.replace('.',strDecimalSeparator);
        
        strSubStr1 = finalresult+strSubStr1;
        
        if(bl==true){
            strSubStr1='-'+strSubStr1;
        }
        return strSubStr1;
    }
    
    public static Decimal roundNumberToNearby(Decimal inputNumber, Integer position){
        String newString ='';
        String sInput = String.valueOf(inputNumber);
        Integer adaptedPart = Integer.valueOf(sInput.substring(position-1,position));
        newString = sInput.substring(0,position);
        for (Integer g=0; g<sInput.length()-position ; g++){
            newString+='0';
        }
        return Decimal.valueOf(newString);
    }
    
}