//--------------------------------------------------------------------------------------------------------------------
//  Description : This TEST APEX Class is used to test the logic in ws_ChangeNotificationToMMX using Test Class Test_ws_ChangeNotificationToMMX
//  Version     : 1.0
//  Author      : Bart Caelen
//  Date        : 14/09/2016
//--------------------------------------------------------------------------------------------------------------------
@isTest public class Test_ws_ChangeNotificationToMMX_WSMock implements WebServiceMock {

   public void doInvoke(
		Object stub,
		Object request,
		Map<String, Object> response,
		String endpoint,
		String soapAction,
		String requestName,
		String responseNS,
		String responseName,
		String responseType
	){

		if (requestName == 'ChangePassword'){

			ws_ChangeNotificationToMMX.ChangePasswordResponse_element response_x = new ws_ChangeNotificationToMMX.ChangePasswordResponse_element();
			response.put('response_x', response_x);

		}else if (requestName == 'GetProfile'){

			ws_ChangeNotificationToMMX.GetProfileResponse_element response_x = new ws_ChangeNotificationToMMX.GetProfileResponse_element();
			response.put('response_x', response_x);

		}else if (requestName == 'SyncRequest'){

			ws_ChangeNotificationToMMX.SyncRequestResponse_element response_x = new ws_ChangeNotificationToMMX.SyncRequestResponse_element();
			response.put('response_x', response_x);

		}else if (requestName == 'SyncRequestMMX'){

			ws_ChangeNotificationToMMX.SyncRequestMMXResponse_element response_x = new ws_ChangeNotificationToMMX.SyncRequestMMXResponse_element();
			response.put('response_x', response_x);

		}else if (requestName == 'deldata'){

			ws_ChangeNotificationToMMX.deldataResponse_element response_x = new ws_ChangeNotificationToMMX.deldataResponse_element();
			response.put('response_x', response_x);

		}else if (requestName == 'getMobileApps'){

			ws_ChangeNotificationToMMX.getMobileAppsResponse_element response_x = new ws_ChangeNotificationToMMX.getMobileAppsResponse_element();
			response.put('response_x', response_x);

		}else if (requestName == 'getRequestProgress'){

			ws_ChangeNotificationToMMX.getRequestProgressResponse_element response_x = new ws_ChangeNotificationToMMX.getRequestProgressResponse_element();
			response.put('response_x', response_x);

		}else if (requestName == 'getRequestProgressMMX'){

			ws_ChangeNotificationToMMX.getRequestProgressMMXResponse_element response_x = new ws_ChangeNotificationToMMX.getRequestProgressMMXResponse_element();
			response.put('response_x', response_x);

		}else if (requestName == 'getUserInfo'){

			ws_ChangeNotificationToMMX.getUserInfoResponse_element response_x = new ws_ChangeNotificationToMMX.getUserInfoResponse_element();
			response.put('response_x', response_x);

		}else if (requestName == 'setAccountPerformance'){

			ws_ChangeNotificationToMMX.setAccountPerformanceResponse_element response_x = new ws_ChangeNotificationToMMX.setAccountPerformanceResponse_element();
			response.put('response_x', response_x);

		}else if (requestName == 'setImplantScheduling'){

    		ws_ChangeNotificationToMMX.setImplantSchedulingResponse_element response_x = new ws_ChangeNotificationToMMX.setImplantSchedulingResponse_element();
    	    response.put('response_x', response_x);

		}else if (requestName == 'valUser'){

			ws_ChangeNotificationToMMX.valUserResponse_element response_x = new ws_ChangeNotificationToMMX.valUserResponse_element();
    	    response.put('response_x', response_x);

		}else if (requestName == 'valUserinfo'){

	        ws_ChangeNotificationToMMX.valUserinfoResponse_element response_x = new ws_ChangeNotificationToMMX.valUserinfoResponse_element();
    	    response.put('response_x', response_x);

		}

   }
}
//--------------------------------------------------------------------------------------------------------------------