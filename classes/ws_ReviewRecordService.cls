/*
 *      Description : This class exposes methods to create / update / delete an Review Record
 *
 *      Author = Jesus Lozano
 */

@RestResource(urlMapping='/ReviewRecordService/*')
global with sharing class ws_ReviewRecordService {
	
	@HttpPost
	global static IFReviewRecordResponse doPost() {
 		RestRequest req = RestContext.request;
						
		String body = req.requestBody.toString();
		IFReviewRecordRequest reviewRecordRequest = (IFReviewRecordRequest) System.Json.deserialize(body, IFReviewRecordRequest.class);
			
		System.debug('post requestBody '+reviewRecordRequest);
			
		Review_Record__c reviewRecord = reviewRecordRequest.ReviewRecord;

		IFReviewRecordResponse resp = new IFReviewRecordResponse();

		//We catch all exception and return it to the sender
		try{
				
			resp.reviewRecord = bl_ReviewRecord.saveReviewRecord(reviewRecord); 
			resp.id = reviewRecordRequest.id;
	
			resp.success = true;
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
		}
 
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'ReviewRecord' ,req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
  			
		return resp;		
	}
	
	
	@HttpDelete
	global static IFReviewRecordDeleteResponse doDelete(){
		RestRequest req = RestContext.request;
			
		System.debug('post called '+req);
		System.debug('post requestBody '+req.requestBody);
			
		String body = req.requestBody.toString();
		IFReviewRecordDeleteRequest reviewRecordDeleteRequest = (IFReviewRecordDeleteRequest)System.Json.deserialize(body, IFReviewRecordDeleteRequest.class);
			
		System.debug('post requestBody '+ReviewRecordDeleteRequest);
			
		Review_Record__c reviewRecord = reviewRecordDeleteRequest.ReviewRecord;
		IFReviewRecordDeleteResponse resp = new IFReviewRecordDeleteResponse();
		
		List<Review_Record__c> reviewRecordsToDelete = [select id, mobile_id__c from Review_Record__c where Mobile_ID__c = :reviewRecord.Mobile_ID__c];
		
		try{
			
			if (reviewRecordsToDelete !=null && reviewRecordsToDelete.size()>0 ){	
				
				Review_Record__c reviewRecordToDelete = reviewRecordsToDelete.get(0);
				delete reviewRecordToDelete;
				resp.reviewRecord = reviewRecordToDelete;
				resp.id = reviewRecordDeleteRequest.id;
				resp.success = true;
			}else{
				resp.reviewRecord = reviewRecord;
				resp.id = ReviewRecordDeleteRequest.id;
				resp.message='Review Record not found';
				resp.success = true;
			}
			
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
		}
		
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'ReviewRecordDelete',req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
			
		return resp;
	}
	
	global class IFReviewRecordRequest{
		public Review_Record__c reviewRecord {get;set;}
		public String id{get;set;}
	}
	
	global class IFReviewRecordDeleteRequest{
		public Review_Record__c reviewRecord {get;set;}
		public String id{get;set;}
	}
		
	global class IFReviewRecordDeleteResponse{
		public Review_Record__c reviewRecord {get;set;}
		public String id{get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}

	global class IFReviewRecordResponse{
		public Review_Record__c reviewRecord {get;set;}
		public String id{get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}
	
}