//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 01-03-2016
//  Description      : APEX Test Class to test the logic in bl_SVMXC_InstalledProduct_Trigger
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_bl_SVMXC_InstalledProduct_Trigger {

    //----------------------------------------------------------------------------------------
    // CREATE TEST DATA
    //----------------------------------------------------------------------------------------
    @testSetup static void createTestData() {

        // Create Custom Setting Data
        clsTestData.createCustomSettingData(true);

        // Create Account Data
        clsTestData.iRecord_Account = 2;
        clsTestData.createAccountData();

        // Create SVMXC__Installed_Product__c Data
        clsTestData.createSVMXCInstalledProductData(true);

    }
    //----------------------------------------------------------------------------------------

	
    //----------------------------------------------------------------------------------------
    // Basic Testing
    //----------------------------------------------------------------------------------------
	@isTest static void test_bl_SVMXC_InstalledProduct_Trigger() {

		// Select Data
		List<SVMXC__Installed_Product__c> lstData = [SELECT Id, SVMXC__Status__c, SVMXC__Company__c FROM SVMXC__Installed_Product__c];
		SVMXC__Installed_Product__c oData = lstData[0];

		List<Account> lstAccount = [SELECT Id FROM Account LIMIT 2];

		Test.startTest(); 
		
		// Update Data
			oData.SVMX_SAP_Equipment_ID__c = '00112233';
			for (Account oAccount : lstAccount){
				if (oData.SVMXC__Company__c != oAccount.Id){
					oData.SVMXC__Company__c = oAccount.Id;
					break;
				}
			}
		update oData;

		// Delete Data
		delete oData;

		// Undelete Data
		Undelete oData;

		Test.stopTest(); 

	}
    //----------------------------------------------------------------------------------------
	
}