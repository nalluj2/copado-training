/*
 *      Created Date : 05 -Apr-2013
 *      Description :  This class is used to change the status of case in case of status 
                       skip for OMA_Spine_Scientific_Exchange_Product record type.
 *      Author = Kaushal Singh
 *      
 */
public class bl_CaseStatusLogic{
   
   	private static Id RT_Id {
   		
   		get{
   			if(RT_Id == null){
   				
   				RT_Id = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('OMA_Spine_Scientific_Exchange_Product').getRecordTypeId();   				
   			}
   			
   			return RT_Id;
   		}
   		
   		set;
   	}
   
    public static void UpdateCaseStatus(map<id,Case> OldMap,list<Case> lstofCase){
        
        case c1,c2,c3,c4;
        list<Case> lstCase=new list<Case>();
        list<Case> lstCase1=new list<Case>();
        list<Case> lstCase2=new list<Case>();
        list<Case> lstCase3=new list<Case>();
        list<Case> lstCase4=new list<Case>();
        list<Case> lstCase5=new list<Case>();
        list<Case> lstCase6=new list<Case>();
        list<Case> lstCase7=new list<Case>();
        list<Case> lstCase8=new list<Case>();
        list<Case> lstCase9=new list<Case>();
        list<Case> lstCase10=new list<Case>();
        list<Case> lstCase11=new list<Case>();
        list<Case> lstCase12=new list<Case>();
        list<Case> lstCase13=new list<Case>();
        list<Case> lstCase14=new list<Case>();
        list<Case> lstCase15=new list<Case>();
        Boolean UpdateCheck=false;
           
         for(Case c:lstofCase){
            if(c.RecordTypeId==RT_Id){
                
                if((OldMap.get(c.Id).status)=='New' && c.status=='Closed'){
                    
                    bl_tr_CaseTriggerActions.checkAfterUpdate=true;
                    c1=new case(id=c.id,status='Acknowledged');
                    lstCase.add(c1);
                    
                    c2=new case(id=c.id,status='First Response');
                    lstCase1.add(c2);
                    
                    c3=new case(id=c.id,status='Question Confirmed');
                    lstCase2.add(c3);
                    
                    c4=new case(id=c.id,status='Closed');
                    lstCase3.add(c4);
                    
                }else if((oldMap.get(c.Id).status)=='New' && c.status=='Question Confirmed'){
                    
                    bl_tr_CaseTriggerActions.checkAfterUpdate=true;
                    c1=new case(id=c.id,status='Acknowledged');
                    lstCase4.add(c1);
                    
                    c2=new case(id=c.id,status='First Response');
                    lstCase5.add(c2); 
                    
                    c3=new case(id=c.id,status='Question Confirmed');
                    lstCase6.add(c3);           
                
                }else if((oldMap.get(c.Id).status)=='New' && c.status=='First Response'){
                    
                    bl_tr_CaseTriggerActions.checkAfterUpdate=true;
                    c1=new case(id=c.id,status='Acknowledged');
                    lstCase7.add(c1);
                    
                    c2=new case(id=c.id,status='First Response');
                    lstCase8.add(c2);
                
                }else if((oldMap.get(c.Id).status)=='Acknowledged' && c.status=='Closed'){
                    
                    bl_tr_CaseTriggerActions.checkAfterUpdate=true;
                    c1=new case(id=c.id,status='First Response');
                    lstCase9.add(c1);
                    
                    c2=new case(id=c.id,status='Question Confirmed');
                    lstCase10.add(c2);
                    
                    c3=new case(id=c.id,status='Closed');
                    lstCase11.add(c3);
                
                }else if((oldMap.get(c.Id).status)=='Acknowledged' && c.status=='Question Confirmed'){
                    
                    bl_tr_CaseTriggerActions.checkAfterUpdate=true;
                    c1=new case(id=c.id,status='First Response');
                    lstCase12.add(c1);
                    
                    c2=new case(id=c.id,status='Question Confirmed');
                    lstCase13.add(c2);
                
                }else if((oldMap.get(c.Id).status)=='First Response' && c.status=='Closed'){
                    
                    bl_tr_CaseTriggerActions.checkAfterUpdate=true;
                    c1=new case(id=c.id,status='Question Confirmed');
                    lstCase14.add(c1);
                    
                    c2=new case(id=c.id,status='Closed');
                    lstCase15.add(c2);
                
                }
            }            
        }
        
        if(lstCase.size()>0 && lstCase1.size()>0 && lstCase2.size()>0 && lstCase3.size()>0){
            update lstCase;
            update lstCase1;
            update lstCase2;
            update lstCase3;
        }else if(lstCase4.size()>0 && lstCase5.size()>0 && lstCase6.size()>0){
            update lstCase4;
            update lstCase5;
            update lstCase6;    
        }else if(lstCase7.size()>0 && lstCase8.size()>0){
            update lstCase7;
            update lstCase8;    
        }else if(lstCase9.size()>0 && lstCase10.size()>0 && lstCase11.size()>0){
            update lstCase9;
            update lstCase10;
            update lstCase11;        
        }else if(lstCase12.size()>0 && lstCase13.size()>0){
            update lstCase12;
            update lstCase13;
        }else if(lstCase14.size()>0 && lstCase15.size()>0){
            update lstCase14;
            update lstCase15;
        }         
    }
}