/*
 * Description	 	: This is the class is used to centralize logic around Contact
 * Author        	: Rudy De Coninck
 * Created Date		: 04-07-2014
 *  -----------------------------------------------------------------------------------------------
 * Description	 	: Added the method processContactWithInactiveOwner
 * Author        	: Bart Caelen
 * Created Date		: 2014-07-28
 */
public class bl_Contact {

	//To avoid loops of this logic
	public static Set<Id> sbuValidatedContacts = new Set<Id>();

	
	// This method will select all Contacts which has an inactive owner based on the passed Contacts.
	// The Contact Owner will be updated by the owner of the related Account if this Account Owner is Active
	//	otherwise it will be updated by a Generic Data Owner that is defined on Company__c (Master Data) which will be selected based on DIB_Country__c (Region)
	//	and if there is no General Data Owner specified, we don't update the Contact Owner at all.
	// The Parameter is a List of Contacts but in this method a new SOQL is performed to be sure that only Inactive Contacts will be processed 
	//	and to make sure that all necessary fields are selected.  By using this approach we can re-use this logic also in e.g. a trigger (if needed) 
	public static void processContactWithInactiveOwner(List<Contact> lstContact){
    	
		List<Contact> lstContact_Inactive = 
			[
				SELECT 
					Id, OwnerId, Owner.IsActive, Account.OwnerID, Account.Owner.isActive, Account.Account_Country_vs__c 
				FROM 
					Contact 
				WHERE 
					ID = :lstContact 
					AND Owner.IsActive = FALSE
					AND IsPersonAccount = FALSE
			];

		List<Contact> lstContact_Update = new List<Contact>();

		// Get the DIB Countries in a Map to be able to retrieve the Generic Data Owner on Company by the Country Name
		List<DIB_Country__c> lstDIBCountry = [SELECT Id, Name, Company__c, Company__r.Generic_Data_Owner_ID__c FROM DIB_Country__c];
		Map<String, DIB_Country__c> mapDIBCountry = new Map<String, DIB_Country__c>();
		for (DIB_Country__c oDIBCountry : lstDIBCountry){
			mapDIBCountry.put(oDIBCountry.Name.toUpperCase(), oDIBCountry);
		}

		// Update the Inactive Contact Owner with the Active Account Owner 
		//	or the Generic Data Owner for the region (Country) of the Account in case the Account Owner is also Inactive 
		for (Contact oContact : lstContact_Inactive){
			Boolean bDoUpdate = false;
			if (oContact.Account.Owner.isActive){
				oContact.OwnerId = oContact.Account.OwnerId;
				bDoUpdate = true;
			}else{
				if (mapDIBCountry.containsKey(clsUtil.isNull(oContact.Account.Account_Country_vs__c, '').toUpperCase()) ){
					String tGenericDataOwnerID = clsUtil.isNull(mapDIBCountry.get(clsUtil.isNull(oContact.Account.Account_Country_vs__c, '').toUpperCase()).Company__r.Generic_Data_Owner_ID__c, '');
					if (tGenericDataOwnerID != ''){
						oContact.OwnerId = tGenericDataOwnerID;
						bDoUpdate = true;		
					}
				}
			}
			if (bDoUpdate){
				lstContact_Update.add(oContact);
			}
		}

		if (lstContact_Update.size() > 0){
			update lstContact_Update;
		}
    }
}