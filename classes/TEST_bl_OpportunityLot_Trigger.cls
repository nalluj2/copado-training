//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 13-08-2018
//  Description      : APEX Test Class to test the logic in tr_OpportunityLot and bl_OpportunityLot_Trigger
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_bl_OpportunityLot_Trigger {

	@testSetup private static void createTestData(){

		//-------------------------------------------------------------
		// Create Test Data
		//-------------------------------------------------------------
		clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'Business_Critical_Tender').Id;
		clsTestData_Opportunity.createOpportunity();

		List<Opportunity_Lot__c> lstOpportunityLot = new List<Opportunity_Lot__c>();
		Opportunity_Lot__c oOpportunityLot1 = new Opportunity_Lot__c();
			oOpportunityLot1.Opportunity__c = clsTestData_Opportunity.oMain_Opportunity.Id;
			oOpportunityLot1.Account__c = clsTestData_Opportunity.oMain_Opportunity.AccountId;
			oOpportunityLot1.CurrencyISOCode = 'EUR';
			oOpportunityLot1.Name = 'Unit test opp lot 1';
			oOpportunityLot1.Status__c = 'Won';
			oOpportunityLot1.Award_Type__c = 'Multi Award';
			oOpportunityLot1.Lot_Owner__c = UserInfo.getUserId();
			oOpportunityLot1.Other_Weight__c = 25;
			oOpportunityLot1.Price_Weight__c = 25;
			oOpportunityLot1.Quality_Weight__c = 50;
			oOpportunityLot1.Estimated_Revenue__c = 1000;
			oOpportunityLot1.Current_Potential__c = 2000;
			oOpportunityLot1.Full_Potential__c = 3000;
		lstOpportunityLot.add(oOpportunityLot1);
		Opportunity_Lot__c oOpportunityLot2 = new Opportunity_Lot__c();
			oOpportunityLot2.Opportunity__c = clsTestData_Opportunity.oMain_Opportunity.Id;
			oOpportunityLot2.Account__c = clsTestData_Opportunity.oMain_Opportunity.AccountId;
			oOpportunityLot2.CurrencyISOCode = 'EUR';
			oOpportunityLot2.Name = 'Unit test opp lot 1';
			oOpportunityLot2.Status__c = 'Won';
			oOpportunityLot2.Award_Type__c = 'Multi Award';
			oOpportunityLot2.Lot_Owner__c = UserInfo.getUserId();
			oOpportunityLot2.Other_Weight__c = 25;
			oOpportunityLot2.Price_Weight__c = 25;
			oOpportunityLot2.Quality_Weight__c = 50;
			oOpportunityLot2.Estimated_Revenue__c = 100;
			oOpportunityLot2.Current_Potential__c = 200;
			oOpportunityLot2.Full_Potential__c = 300;
		lstOpportunityLot.add(oOpportunityLot2);
		insert lstOpportunityLot;
	
	
	}

	@isTest private static void test_calculateRLSFieldOnOpportunity_INSERT() {

		//-------------------------------------------------------------
		// Create / Load Test Data
		//-------------------------------------------------------------
		Opportunity oOpportunity = [SELECT Id, Estimated_Revenue_Won_Text__c, Current_Potential_Text__c, Full_Potential_Text__c FROM Opportunity LIMIT 1];
		Account oAccount = [SELECT Id FROM Account LIMIT 1];

		System.assertEquals(oOpportunity.Estimated_Revenue_Won_Text__c.round(System.RoundingMode.HALF_EVEN), 1100);
		System.assertEquals(oOpportunity.Current_Potential_Text__c.round(System.RoundingMode.HALF_EVEN), 2200);
		System.assertEquals(oOpportunity.Full_Potential_Text__c.round(System.RoundingMode.HALF_EVEN), 3300);
		//-------------------------------------------------------------


		//-------------------------------------------------------------
		// Test Logic
		//-------------------------------------------------------------
		Test.startTest();

			Opportunity_Lot__c oOpportunityLot = new Opportunity_Lot__c();
				oOpportunityLot.Opportunity__c = oOpportunity.Id;
				oOpportunityLot.Account__c = oAccount.Id;
				oOpportunityLot.CurrencyISOCode = 'EUR';
				oOpportunityLot.Name = 'Unit test opp lot 1';
				oOpportunityLot.Status__c = 'Won';
				oOpportunityLot.Award_Type__c = 'Multi Award';
				oOpportunityLot.Lot_Owner__c = UserInfo.getUserId();
				oOpportunityLot.Other_Weight__c = 25;
				oOpportunityLot.Price_Weight__c = 25;
				oOpportunityLot.Quality_Weight__c = 50;

				oOpportunityLot.Estimated_Revenue__c = 100;
				oOpportunityLot.Current_Potential__c = 200;
				oOpportunityLot.Full_Potential__c = 300;
			insert oOpportunityLot;

		Test.stopTest();
		//-------------------------------------------------------------


		//-------------------------------------------------------------
		// Validate Result
		//-------------------------------------------------------------
		oOpportunity = [SELECT Id, Estimated_Revenue_Won_Text__c, Current_Potential_Text__c, Full_Potential_Text__c FROM Opportunity LIMIT 1];

		System.assertEquals(oOpportunity.Estimated_Revenue_Won_Text__c.round(System.RoundingMode.HALF_EVEN), 1200);
		System.assertEquals(oOpportunity.Current_Potential_Text__c.round(System.RoundingMode.HALF_EVEN), 2400);
		System.assertEquals(oOpportunity.Full_Potential_Text__c.round(System.RoundingMode.HALF_EVEN), 3600);
		//-------------------------------------------------------------

	}

	@isTest private static void test_calculateRLSFieldOnOpportunity_UPDATE() {

		//-------------------------------------------------------------
		// Create / Load Test Data
		//-------------------------------------------------------------
		Opportunity oOpportunity = 
			[
				SELECT 
					Id, Estimated_Revenue_Won_Text__c, Current_Potential_Text__c, Full_Potential_Text__c
					, (SELECT Id, Estimated_Revenue__c, Current_Potential__c, Full_Potential__c FROM Opportunity_Lots__r)
				FROM Opportunity
				LIMIT 1
		];
		Account oAccount = [SELECT Id FROM Account LIMIT 1];

		System.assertEquals(oOpportunity.Estimated_Revenue_Won_Text__c.round(System.RoundingMode.HALF_EVEN), 1100);
		System.assertEquals(oOpportunity.Current_Potential_Text__c.round(System.RoundingMode.HALF_EVEN), 2200);
		System.assertEquals(oOpportunity.Full_Potential_Text__c.round(System.RoundingMode.HALF_EVEN), 3300);
		//-------------------------------------------------------------


		//-------------------------------------------------------------
		// Test Logic
		//-------------------------------------------------------------
		Test.startTest();

			List<Opportunity_Lot__c> lstOpportunityLot_Update = new List<Opportunity_Lot__c>();

			oOpportunity.Opportunity_Lots__r[0].Estimated_Revenue__c += 100;
			oOpportunity.Opportunity_Lots__r[0].Current_Potential__c += 200;
			oOpportunity.Opportunity_Lots__r[0].Full_Potential__c += 300;

			lstOpportunityLot_Update.add(oOpportunity.Opportunity_Lots__r[0]);

			update lstOpportunityLot_Update;

		Test.stopTest();
		//-------------------------------------------------------------


		//-------------------------------------------------------------
		// Validate Result
		//-------------------------------------------------------------
		oOpportunity = [SELECT Id, Estimated_Revenue_Won_Text__c, Current_Potential_Text__c, Full_Potential_Text__c FROM Opportunity LIMIT 1];

		System.assertEquals(oOpportunity.Estimated_Revenue_Won_Text__c.round(System.RoundingMode.HALF_EVEN), 1100 + 100);
		System.assertEquals(oOpportunity.Current_Potential_Text__c.round(System.RoundingMode.HALF_EVEN), 2200 + 200);
		System.assertEquals(oOpportunity.Full_Potential_Text__c.round(System.RoundingMode.HALF_EVEN), 3300 + 300);
		//-------------------------------------------------------------

	}


	@isTest private static void test_calculateRLSFieldOnOpportunity_DELETE_UNDELETE() {

		//-------------------------------------------------------------
		// Create / Load Test Data
		//-------------------------------------------------------------
		Opportunity oOpportunity = 
			[
				SELECT 
					Id, Estimated_Revenue_Won_Text__c, Current_Potential_Text__c, Full_Potential_Text__c
					, (SELECT Id, Estimated_Revenue__c, Current_Potential__c, Full_Potential__c FROM Opportunity_Lots__r)
				FROM Opportunity
				LIMIT 1
		];
		Account oAccount = [SELECT Id FROM Account LIMIT 1];

		System.assertEquals(oOpportunity.Estimated_Revenue_Won_Text__c.round(System.RoundingMode.HALF_EVEN), 1100);
		System.assertEquals(oOpportunity.Current_Potential_Text__c.round(System.RoundingMode.HALF_EVEN), 2200);
		System.assertEquals(oOpportunity.Full_Potential_Text__c.round(System.RoundingMode.HALF_EVEN), 3300);
		//-------------------------------------------------------------


		//-------------------------------------------------------------
		// Test Logic
		//-------------------------------------------------------------
		Test.startTest();

			List<Opportunity_Lot__c> lstOpportunityLot_Delete = new List<Opportunity_Lot__c>();

			// TEST DELETE 1 RECORD
			Decimal decEstimatedRevenue = oOpportunity.Opportunity_Lots__r[0].Estimated_Revenue__c;
			Decimal decCurrentPotential = oOpportunity.Opportunity_Lots__r[0].Current_Potential__c;
			Decimal decFullPotential = oOpportunity.Opportunity_Lots__r[0].Full_Potential__c;

			lstOpportunityLot_Delete.add(oOpportunity.Opportunity_Lots__r[0]);

			delete lstOpportunityLot_Delete;

			oOpportunity = [SELECT Id, Estimated_Revenue_Won_Text__c, Current_Potential_Text__c, Full_Potential_Text__c FROM Opportunity LIMIT 1];

			System.assertEquals(oOpportunity.Estimated_Revenue_Won_Text__c.round(System.RoundingMode.HALF_EVEN), 1100 - decEstimatedRevenue);
			System.assertEquals(oOpportunity.Current_Potential_Text__c.round(System.RoundingMode.HALF_EVEN), 2200 - decCurrentPotential);
			System.assertEquals(oOpportunity.Full_Potential_Text__c.round(System.RoundingMode.HALF_EVEN), 3300 - decFullPotential);


			// TEST UN-DELETE 1 RECORD
			undelete lstOpportunityLot_Delete[0];

			oOpportunity = [SELECT Id, Estimated_Revenue_Won_Text__c, Current_Potential_Text__c, Full_Potential_Text__c FROM Opportunity LIMIT 1];

			System.assertEquals(oOpportunity.Estimated_Revenue_Won_Text__c.round(System.RoundingMode.HALF_EVEN), 1100);
			System.assertEquals(oOpportunity.Current_Potential_Text__c.round(System.RoundingMode.HALF_EVEN), 2200);
			System.assertEquals(oOpportunity.Full_Potential_Text__c.round(System.RoundingMode.HALF_EVEN), 3300);


			// TEST DELETE ALL RECORDS
			oOpportunity = 
				[
					SELECT 
						Id, Estimated_Revenue_Won_Text__c, Current_Potential_Text__c, Full_Potential_Text__c
						, (SELECT Id, Estimated_Revenue__c, Current_Potential__c, Full_Potential__c FROM Opportunity_Lots__r)
					FROM Opportunity
					LIMIT 1
				];

			delete oOpportunity.Opportunity_Lots__r;

		Test.stopTest();
		//-------------------------------------------------------------


		//-------------------------------------------------------------
		// Validate Result
		//-------------------------------------------------------------
		oOpportunity = [SELECT Id, Estimated_Revenue_Won_Text__c, Current_Potential_Text__c, Full_Potential_Text__c FROM Opportunity LIMIT 1];

		System.assertEquals(oOpportunity.Estimated_Revenue_Won_Text__c.round(System.RoundingMode.HALF_EVEN), 0);
		System.assertEquals(oOpportunity.Current_Potential_Text__c.round(System.RoundingMode.HALF_EVEN), 0);
		System.assertEquals(oOpportunity.Full_Potential_Text__c.round(System.RoundingMode.HALF_EVEN), 0);
		//-------------------------------------------------------------

	}
}
//--------------------------------------------------------------------------------------------------------------------