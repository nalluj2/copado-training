@isTest
private class Test_bl_Opportunity_BU_Involved {
    
    private static testmethod void testEMEAIHS(){
    	
    	Account acc = new Account();
    	acc.Name = 'IHS Account';
    	acc.Account_Country_vs__c = 'Netherlands';
    	insert acc;
    	
    	Id EMEA_IHS_rt = clsUtil.getRecordTypeByDevName('Opportunity', 'EMEA_IHS_Managed_Services').Id;
    	
    	Opportunity opp_EMEA = new Opportunity();
        opp_EMEA.RecordTypeId = EMEA_IHS_rt;
        opp_EMEA.Name = 'TEST Opportunity';
        opp_EMEA.AccountId = acc.Id;
        opp_EMEA.CloseDate = Date.today().addMonths(6);
        opp_EMEA.StageName = '0 - Tracking';
        opp_EMEA.Deal_Category__c = 'New Customer';
        opp_EMEA.Origin_Text__c = 'NTG Program';
        opp_EMEA.Type = 'Standard Sale';
        opp_EMEA.Amount = 100000;
        
    	insert new List<Opportunity>{opp_EMEA};
    	
    	Test.startTest();	
    	
    	Opportunity_BU_Involved__c EMEA_RTG = new Opportunity_BU_Involved__c();
    	EMEA_RTG.Opportunity__c = opp_EMEA.Id;
    	EMEA_RTG.BU_Involved__c = 'RTG';    	
    	EMEA_RTG.BU_Percentage__c = 20;
    	
    	Opportunity_BU_Involved__c EMEA_CVG = new Opportunity_BU_Involved__c();
    	EMEA_CVG.Opportunity__c = opp_EMEA.Id;
    	EMEA_CVG.BU_Involved__c = 'CVG';    	
    	EMEA_CVG.BU_Percentage__c = 40;
    	
    	Opportunity_BU_Involved__c EMEA_MITG = new Opportunity_BU_Involved__c();
    	EMEA_MITG.Opportunity__c = opp_EMEA.Id;
    	EMEA_MITG.BU_Involved__c = 'MITG';    	
    	EMEA_MITG.BU_Percentage__c = 40;
    	
    	
    	insert new List<Opportunity_BU_Involved__c>{EMEA_RTG, EMEA_CVG, EMEA_MITG};
    	    	    	
    	opp_EMEA = [Select BU_Involved__c from Opportunity where Id = :opp_EMEA.Id];
    	System.debug('opp_EMEA: ' + opp_EMEA.BU_Involved__c);
    	System.assert(opp_EMEA.BU_Involved__c.contains('MITG'));
    	System.assert(opp_EMEA.BU_Involved__c.contains('RTG'));
    	System.assert(opp_EMEA.BU_Involved__c.contains('CVG'));
    	
    	EMEA_MITG.BU_Percentage__c = 60;
    	
    	Boolean isError = false;
    	
    	try{
    		
    		update EMEA_MITG;
    		
    	}catch(Exception e){
    		
    		isError = true;
    		System.assert(e.getMessage().contains('The total Opportunity percentage of BU involved must be 100%'));
    	}
    	
    	System.assert(isError == true);
    	
    	bl_Opportunity_BU_Involved.setValidateTotal(false);
    	
    	delete EMEA_MITG;
    	
    	bl_Opportunity_BU_Involved.setValidateTotal(true);
    	
    	undelete EMEA_MITG;
    	 
    }
    
    private static testmethod void testEMEALeadIHS(){
    	
    	Account acc = new Account();
    	acc.Name = 'IHS Account';
    	acc.Account_Country_vs__c = 'Netherlands';
    	insert acc;
    	
    	Id EMEA_IHS_rt = clsUtil.getRecordTypeByDevName('Opportunity', 'EMEA_IHS_Lead').Id;
    	
    	Opportunity opp_EMEA= new Opportunity();
        opp_EMEA.RecordTypeId = EMEA_IHS_rt;
        opp_EMEA.Name = 'TEST Opportunity Lead';
        opp_EMEA.AccountId = acc.Id;
        opp_EMEA.CloseDate = Date.today().addMonths(6);
        opp_EMEA.StageName = '0 - Lead';        
        opp_EMEA.Type = 'Standard Sale';
        opp_EMEA.Amount = 100000;
        
        insert new List<Opportunity>{opp_EMEA};
        
        Test.startTest();
                
    	Opportunity_BU_Involved__c EMEA_CVG = new Opportunity_BU_Involved__c();
    	EMEA_CVG.Opportunity__c = opp_EMEA.Id;
    	EMEA_CVG.BU_Involved__c = 'CVG';
    	    	
    	Opportunity_BU_Involved__c EMEA_MITG = new Opportunity_BU_Involved__c();
    	EMEA_MITG.Opportunity__c = opp_EMEA.Id;
    	EMEA_MITG.BU_Involved__c = 'MITG';
    	   	
    	Opportunity_BU_Involved__c EMEA_RTG = new Opportunity_BU_Involved__c();
    	EMEA_RTG.Opportunity__c = opp_EMEA.Id;
    	EMEA_RTG.BU_Involved__c = 'RTG';
    	   	
    	insert new List<Opportunity_BU_Involved__c>{EMEA_CVG, EMEA_MITG, EMEA_RTG};
    	
    	opp_EMEA = [Select BU_Involved__c from Opportunity where Id = :opp_EMEA.Id];
    	
    	System.assert(opp_EMEA.BU_Involved__c == null);
    	    	    	    	
    	EMEA_MITG.BU_Involved__c = 'RTG';
    	
    	Boolean isError = false;
    	
    	try{
    		
    		update EMEA_MITG;
    		
    	}catch(Exception e){
    		
    		isError = true;
    		System.assert(e.getMessage().contains('duplicate value'));
    	}
    	
    	System.assert(isError == true);
    }    
}