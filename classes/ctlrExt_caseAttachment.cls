/*
 *   Controller name: ctlrExt_caseAttachment
 *   Vf Page Name : CaseAttachmentPage
 *   Author : Priti Jaiswal
 *   Description: This class include the logic to add the multiple attachment on Case Object               
*/

public class ctlrExt_caseAttachment { 
    public List<SelectOption> filesCountList {get; set;}  
    public string fileCount{get;set;}   
    public List<Attachment> selFilesList{get;set;} 
    public boolean showerrormessage{get;set;}
    public id caseId{get;set;} 

    public ctlrExt_caseAttachment(ApexPages.StandardController controller) {        
        filesCountList=new List<SelectOption>();
        fileCount='5';//to set the default picklist value to 5. 
        selFilesList=new List<Attachment>();        
        for(integer i=1;i<=10;i++){
            filesCountList.add(new selectOption(''+i,''+i));
        }   
        //Set initial browsing option to 5
        for(integer i=1;i<=integer.valueof(fileCount);i++){ 
            selFilesList.add(new Attachment());
        }       
        caseId=ApexPages.currentPage().getParameters().get('Id');
        Id currentUserId=UserInfo.getUserId();
        //show error message when user dont have write premission on record
        UserRecordAccess useraccess=[SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE  RecordId =:caseId and UserId=:currentUserId];
        if(useraccess.HasEditAccess==false){
            showerrormessage=true;
        }
    }   
    
    public pageReference attchFiles(){
        List<Attachment> insertFileList = new List<Attachment>() ; 
        integer noOfSelectedFiles=0; 
        //logic to insert the slected files in the attachment related list
        for(Attachment a:selFilesList){
            if(a.name != '' && a.name!=null){
                noOfSelectedFiles++;
                insertFileList.add(new Attachment(parentid=caseId,name=a.name,body=a.body));
            }           
        }           
        if(insertFileList.size()>0){
            insert insertFileList;
        }
        //redirect to record detail page if at least one file is selected else show error message
        if(noOfSelectedFiles!=0){
            PageReference pageref=new PageReference('/'+caseId);
            pageref.setredirect(true);
            return pageref;
        }       
        else{
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select a file to upload.');
            ApexPages.addMessage(myMsg);
            return null;
        }
    }
    public pagereference changeCount(){
        selFilesList.clear();   
        //logic to display the browsing option based on the number selected
        for(integer i=1;i<=integer.valueof(fileCount);i++){ 
            selFilesList.add(new Attachment());
        }
        return null;              
    }
}