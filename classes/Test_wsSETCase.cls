//  Change Log       : PMT-22787: MITG EMEA User Setup - Changed profile from EUR Field Force MITG to EMEA Field Force MITG
@isTest
private class Test_wsSETCase {
    
    private static testmethod void testSETCase(){
    	//PMT-22787: MITG EMEA User Setup - Changed profile from EUR Field Force MITG to EMEA Field Force MITG
    	User mitgUser = [Select Id from User where Profile.Name in ('EUR Field Force MITG', 'EMEA Field Force MITG') AND IsActive = true AND Id NOT IN (Select AssigneeId from PermissionSetASsignment where PermissionSet.Name = 'SET_Admin') Limit 1];
        //User mitgUser = [Select Id from User where Profile.Name = 'EMEA Field Force MITG' AND IsActive = true AND Id NOT IN (Select AssigneeId from PermissionSetASsignment where PermissionSet.Name = 'SET_Admin') Limit 1];
    	
    	System.runAs(mitgUser){
	    	
	    	//Create
	    	Cost_Analysis_Case__c setCase = new Cost_Analysis_Case__c();
	    	setCase.Mobile_ID__c = 'mobileId';
	    	
	    	ws_SETCaseService.SETCaseRequest request = new ws_SETCaseService.SETCaseRequest();
		 	request.SETCase = setCase;
		 	request.id = 'testId';
		 		 	
	    	RestRequest req = new RestRequest();	 	
		 	req.requestbody = Blob.valueOf(JSON.serialize(request));
		 	
		 	RestContext.request = req;
		 	
		 	String resp = ws_SETCaseService.doPost();
		 	
		 	ws_SETCaseService.SETCaseResponse response = (ws_SETCaseService.SETCaseResponse) JSON.deserialize(resp, ws_SETCaseService.SETCaseResponse.class);
		 	
		 	System.assert(response.success == true);
		 	
		 	setCase = [Select Id, Mobile_Id__c from Cost_Analysis_Case__c where Mobile_Id__c = 'mobileId'];
			
			//Delete		 	
		 	ws_SETCaseService.SETCaseDeleteRequest requestDel = new ws_SETCaseService.SETCaseDeleteRequest();
	 		requestDel.SETCase = setCase;
	 		requestDel.id = 'testId';
		 	
		 	RestRequest reqDel = new RestRequest();	 	
		 	reqDel.requestbody = Blob.valueOf(JSON.serialize(requestDel));
		 	
		 	RestContext.request = reqDel;
		 	
		 	resp = ws_SETCaseService.doDelete();
		 	
		 	ws_SETCaseService.SETCaseDeleteResponse responseDel = (ws_SETCaseService.SETCaseDeleteResponse) JSON.deserialize(resp, ws_SETCaseService.SETCaseDeleteResponse.class);
		 	
		 	System.assert(responseDel.success == true);
		 	
		 	List<Cost_analysis_case__c> cases = [Select Id from Cost_Analysis_Case__c where Mobile_Id__c = 'mobileId'];
		 	
		 	System.assert(cases.size() == 0);
		 	
		 	//Delete (record not found)
		 	resp = ws_SETCaseService.doDelete();
		 	
		 	responseDel = (ws_SETCaseService.SETCaseDeleteResponse) JSON.deserialize(resp, ws_SETCaseService.SETCaseDeleteResponse.class);
		 	
		 	System.assert(responseDel.success == true);
		 	System.assert(responseDel.message == 'SETCase not found');
    	}
    }
    
    private static testmethod void testSETCaseAnswer(){
    	//PMT-22787: MITG EMEA User Setup - Changed profile from EUR Field Force MITG to EMEA Field Force MITG
    	User mitgUser = [Select Id from User where Profile.Name in ('EUR Field Force MITG', 'EMEA Field Force MITG') AND IsActive = true AND Id NOT IN (Select AssigneeId from PermissionSetASsignment where PermissionSet.Name = 'SET_Admin') Limit 1];
       	//User mitgUser = [Select Id from User where Profile.Name = 'EMEA Field Force MITG' AND IsActive = true AND Id NOT IN (Select AssigneeId from PermissionSetASsignment where PermissionSet.Name = 'SET_Admin') Limit 1];
    	
    	//Create
	    Cost_Analysis_Case__c setCase = new Cost_Analysis_Case__c();
	    setCase.Mobile_ID__c = 'mobileId';
	    setCase.OwnerId = mitgUser.Id;
	    insert setCase;
	    
	    Cost_Analysis_Question__c setCaseQuestion = new Cost_Analysis_Question__c();
		insert setCaseQuestion;
    	
    	System.runAs(mitgUser){
	    	
	    	Cost_Analysis_Case_Answer__c SETCaseAnswer = new Cost_Analysis_Case_Answer__c();
	    	SETCaseAnswer.Cost_Analysis_Case__c = setCase.Id;
    		SETCaseAnswer.Cost_Analysis_Question__c = setCaseQuestion.Id;
    		SETCaseAnswer.Mobile_Id__c = 'mobileId';
	    		    		    	
	    	ws_SETCaseAnswerService.SETCaseAnswerRequest request = new ws_SETCaseAnswerService.SETCaseAnswerRequest();
		 	request.SETCaseAnswer = SETCaseAnswer;
		 	request.id = 'testId';
		 		 	
	    	RestRequest req = new RestRequest();	 	
		 	req.requestbody = Blob.valueOf(JSON.serialize(request));
		 	
		 	RestContext.request = req;
		 	
		 	String resp = ws_SETCaseAnswerService.doPost();
		 	
		 	ws_SETCaseAnswerService.SETCaseAnswerResponse response = (ws_SETCaseAnswerService.SETCaseAnswerResponse) JSON.deserialize(resp, ws_SETCaseAnswerService.SETCaseAnswerResponse.class);
		 	
		 	System.assert(response.success == true);
		 	
		 	SETCaseAnswer = [Select Id, Mobile_Id__c from Cost_Analysis_Case_Answer__c where Mobile_Id__c = 'mobileId'];
			
			//Delete		 	
		 	ws_SETCaseAnswerService.SETCaseAnswerDeleteRequest requestDel = new ws_SETCaseAnswerService.SETCaseAnswerDeleteRequest();
	 		requestDel.SETCaseAnswer = SETCaseAnswer;
	 		requestDel.id = 'testId';
		 	
		 	RestRequest reqDel = new RestRequest();	 	
		 	reqDel.requestbody = Blob.valueOf(JSON.serialize(requestDel));
		 	
		 	RestContext.request = reqDel;
		 	
		 	resp = ws_SETCaseAnswerService.doDelete();
		 	
		 	ws_SETCaseAnswerService.SETCaseAnswerDeleteResponse responseDel = (ws_SETCaseAnswerService.SETCaseAnswerDeleteResponse) JSON.deserialize(resp, ws_SETCaseAnswerService.SETCaseAnswerDeleteResponse.class);
		 	
		 	System.assert(responseDel.success == true);
		 	
		 	List<Cost_Analysis_Case_Answer__c> answers = [Select Id from Cost_Analysis_Case_Answer__c where Mobile_Id__c = 'mobileId'];
		 	
		 	System.assert(answers.size() == 0);
		 	
		 	//Delete (record not found)
		 	resp = ws_SETCaseAnswerService.doDelete();
		 	
		 	responseDel = (ws_SETCaseAnswerService.SETCaseAnswerDeleteResponse) JSON.deserialize(resp, ws_SETCaseAnswerService.SETCaseAnswerDeleteResponse.class);
		 	
		 	System.assert(responseDel.success == true);
		 	System.assert(responseDel.message == 'SETCaseAnswer not found');
    	}
    }
    
    @TestSetup
    private static void setupSettings(){
    	
    	SystemAdministratorProfileId__c ids = SystemAdministratorProfileId__c.getInstance('Default');
    	
    	if(ids == null){
    		ids = new SystemAdministratorProfileId__c();
    		ids.Name = 'Default';
    	}
    	
    	if(ids.SystemAdministrator__c == null){
    		
    		ids.SystemAdministrator__c = String.valueOf([Select Id from Profile where Name = 'System Administrator'].Id).substring(0,15);
    		upsert ids;
    	}
    }
}