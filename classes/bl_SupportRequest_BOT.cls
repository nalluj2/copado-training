public with sharing class bl_SupportRequest_BOT {


    //------------------------------------------------------------
    // Private variables
    //------------------------------------------------------------
    private Set<String> setUserRole_BOTCountries = new Set<String>{'Business Manager', 'Customer Service', 'Other User', 'Product Manager'};
    private Set<String> setUserRole_BOTMajorClasses = new Set<String>{'Product Manager'};
    private Set<String> setUserRole_BOTMPGCodes = new Set<String>{'Business Manager'};
    private ctrl_Support_Requests.SessionData session;
    //------------------------------------------------------------
    
    //------------------------------------------------------------
    // Constructor
    //------------------------------------------------------------
    public bl_SupportRequest_BOT(ctrl_Support_Requests.SessionData sData){
        tApplicationName = 'BOT';
        this.session = sData;       
    }
    //------------------------------------------------------------


    //------------------------------------------------------------
    // Getters & Setters
    //------------------------------------------------------------
    public String tApplicationName { get; private set; }
    public String searchAlias { get;set; }

    public Boolean bShowBOTCountries { 
    	get{

    		Boolean bResult = false;
    		if (
    			(session.request.BOT_User_Role__c != null)
				&& (setUserRole_BOTCountries.contains(session.request.BOT_User_Role__c))
    		){
    			bResult = true;
    		}
    		return bResult;
    	} 
    	private set;
   	}

    public Boolean bShowBOTMajorClasses { 
    	get{
    		Boolean bResult = false;
    		if (
    			(session.request.BOT_User_Role__c != null)
				&& (setUserRole_BOTMajorClasses.contains(session.request.BOT_User_Role__c))
    		){
    			bResult = true;
    		}
    		return bResult;
    	} 
    	private set;
   	}

    public Boolean bShowBOTMPGCodes { 
    	get{
    		Boolean bResult = false;
    		if (
    			(session.request.BOT_User_Role__c != null)
				&& (setUserRole_BOTMPGCodes.contains(session.request.BOT_User_Role__c))
    		){
    			bResult = true;
    		}
    		return bResult;
    	} 
    	private set;
   	}
    //------------------------------------------------------------

    
    //------------------------------------------------------------
    // Page-Reference
    //------------------------------------------------------------
    public PageReference requestNewUser(){
    
        session.resetRequest();

        session.request.Status__c = 'New';
        session.request.application__c = tApplicationName;
        session.request.ownerId = session.internalUserId;
        session.request.Request_Type__c = 'Create User';
        
        session.comment = null;
        session.comments = null;

        return Page.Support_BOT_CreateUser;         
    }

    public PageReference requestDataExtract(){
    
        session.resetRequest();

        session.request.Status__c = 'New';
        session.request.application__c = tApplicationName;
        session.request.ownerId = session.internalUserId;
        session.request.Request_Type__c = 'Data Extract';

        session.comment = null;
        session.comments = null;

        searchAlias = '';

        return Page.Support_BOT_DataExtract;         
    }

    public PageReference requestGeneral(){

        session.resetRequest();

        session.request.Status__c = 'New';
        session.request.application__c = tApplicationName;
        session.request.ownerId = session.internalUserId;
        session.request.Request_Type__c = 'Generic Service';
        
        session.comment = null;
        session.comments = null;
        
        return Page.Support_BOT_GeneralRequest;         
    }

    public void BOTUserRoleChanged(){

    }
    //------------------------------------------------------------


    //------------------------------------------------------------
    // Private Methods
    //------------------------------------------------------------
    private void fillUserInfo(ws_adUserService.userinfo inputInfo){

        session.request.Firstname__c = inputInfo.firstName;
        session.request.Lastname__c = inputInfo.lastName;
        session.request.Email__c = inputInfo.email;
            
    }  
    //------------------------------------------------------------

    
    //------------------------------------------------------------
    // Action Methods
    //------------------------------------------------------------

    public void createUserRequest(){

        if (session.request.Firstname__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Firstname is required - please provide a Medtronic username'));
            return;
        }

        if (session.request.Lastname__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Lastname is required - please provide a Medtronic username'));
            return;
        }

        if (session.request.Email__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Email is required - please provide a Medtronic username'));
            return;
        }

        if (session.request.BOT_User_Role__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'BOT User Role is required'));
            return;
        }

        if (setUserRole_BOTCountries.contains(session.request.BOT_User_Role__c)){

	        if (session.request.BOT_Countries__c == null){
	            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'BOT Countries is required'));
	            return;
	        }

        }

        if (setUserRole_BOTMajorClasses.contains(session.request.BOT_User_Role__c)){

	        if (session.request.BOT_Major_Classes__c == null){
	            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'BOT Major Classes is required'));
	            return;
	        }
        	
        }

        if (setUserRole_BOTMPGCodes.contains(session.request.BOT_User_Role__c)){
        	
	        if (session.request.BOT_MPG_Codes__c == null){
	            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'BOT MPG Codes is required'));
	            return;
	        }

        }

        try{    
        
            session.request.Requestor_Email__c = session.uInfo.email;
            session.request.Requestor_Name__c = session.uInfo.firstName + ' ' + session.uInfo.lastName;
            insert session.request;

            session.comment = new User_Request_Comment__c();        

        }catch(Exception e){

            ApexPages.addMessages(e);
            if(Test.isRunningTest()) throw e;
            return;
        }

        session.request = loadSupportRequest(session.request.Id);

        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request '+session.request.Name+' created successfully.'));    
    }

    public void createDataExtractRequest(){
                        
        if (String.isBlank(session.request.BOT_File_Type__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'BOT File Type is required'));
            return;
        }
        
        if (String.isBlank(session.request.Description_Long__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Description is required'));
            return;
        }
        
        try{    
            
            session.request.Requestor_Email__c = session.uInfo.email;
            session.request.Requestor_Name__c = session.uInfo.firstName + ' ' + session.uInfo.lastName;
            insert session.request;
            
            session.comment = new User_Request_Comment__c();        
                        
        }catch(Exception e){
            
            ApexPages.addMessages(e);
            if(Test.isRunningTest()) throw e;
            return;
        }
        
        session.request = loadSupportRequest(session.request.Id);
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request '+session.request.Name+' created successfully.'));       
    }
    
    public void createGeneralRequest(){

        if (String.isBlank(session.request.Description_Long__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Description is required'));
            return;
        }

        try{    

            session.request.Requestor_Email__c = session.uInfo.email;
            session.request.Requestor_Name__c = session.uInfo.firstName + ' ' + session.uInfo.lastName;
            insert session.request;

            session.comment = new User_Request_Comment__c();        

        }catch(Exception e){

            ApexPages.addMessages(e);
            if(Test.isRunningTest()) throw e;
            return;
        }

        session.request = loadSupportRequest(session.request.Id);

        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request '+session.request.Name+' created successfully.'));    
    }

    public void searchByAlias(){
        ws_adUserService.userinfo searchResult = bl_SupportRequest.searchByAlias(searchAlias);
        if (searchResult != null) fillUserInfo(searchResult);     
    }
      
    public Create_User_Request__c loadSupportRequest(Id idSupportRequest){
        return bl_SupportRequest.loadSupportRequestDetailData(idSupportRequest);
    }
    //------------------------------------------------------------
}