//--------------------------------------------------------------------------------------------------------------------
//  Description : This TEST APEX Class is used to test the logic in ctrlExt_AccountTerritory
//  Version     : 1.0
//  Author      : Bart Caelen
//  Date        : 14/09/2016
//--------------------------------------------------------------------------------------------------------------------
@isTest private class Test_ctrlExt_AccountTerritory {

	public static User oUser_Admin1;
	public static User oUser_Admin2;

	@isTest static void createTestData() {
		
		//------------------------------------------------------------
		// Create Test Data
		//------------------------------------------------------------
		// Create System Admin Users
		oUser_Admin1 = clsTestData_User.createUser_SystemAdministratorMDT('tstadm1', false);
		oUser_Admin2 = clsTestData_User.createUser_SystemAdministratorMDT('tstadm2', false);
		List<User> lstUser = new List<User>{oUser_Admin1, oUser_Admin2};
		insert lstUser;

		System.runAs(oUser_Admin1){

			// Create Custom Setting Data
			clsTestData_CustomSetting.createCustomSettingData_SystemAdministratorProfileId();

			// Permission Set Data
			PermissionSet oPermissionSet = [SELECT ID From PermissionSet WHERE Name = 'TMA_Access'];
			insert new PermissionSetAssignment(AssigneeId = oUser_Admin2.Id, PermissionSetId = oPermissionSet.Id);

			// Create Account Data
			clsTestData_Account.iRecord_Account = 1;
			clsTestData_Account.createAccount();

		}
		//------------------------------------------------------------

	}
	
	@isTest static void test_ctrlExt_AccountTerritory() {

		// Create Test Data
		createTestData();

		Test.startTest();

		System.runAs(oUser_Admin2){
		    PageReference oPage = new PageReference('/Territories?Id=' + clsTestData_Account.oMain_Account.Id);
		    ApexPages.StandardController oCtrlStd = new ApexPages.StandardController(clsTestData_Account.oMain_Account);
		    ctrlExt_AccountTerritory oCtrl = new ctrlExt_AccountTerritory(oCtrlStd);

		    List<Territory2> lstTerritory = oCtrl.getTerritories();
		}

	    Test.stopTest();
	}
	
}
//--------------------------------------------------------------------------------------------------------------------