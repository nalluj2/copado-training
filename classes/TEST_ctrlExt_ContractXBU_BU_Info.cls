@isTest
private class TEST_ctrlExt_ContractXBU_BU_Info {


    private static testmethod void testUpdateOpportunityBUInfo(){

		//--------------------------------------------------------
		// Create Test Data
		//--------------------------------------------------------
		clsTestData_MasterData.createSubBusinessUnit();

    	Account oAccount = new Account();
    		oAccount.Name = 'Unit Test Account 1';
    		oAccount.SAP_Id__c = '0123456789';    	    	
    	insert oAccount;
    	
    	Contract_xBU__c oContractXBU = new Contract_xBU__c();
    		oContractXBU.Account__c = oAccount.Id;
    		oContractXBU.Status__c = 'In Process C&P';
    		oContractXBU.Internal_Contract_Number__c = 101;
    		oContractXBU.Contract_Number__c = '101-' + String.valueOf(Date.today().addYears(-1)).substring(2,4);
		insert oContractXBU;
		//--------------------------------------------------------


		//--------------------------------------------------------
		// Execute Logic
		//--------------------------------------------------------
    	Test.startTest();

			oContractXBU = [SELECT Business_Unit_Group__c, Business_Unit__c, Sub_Business_Unit__c FROM Contract_XBU__c WHERE Id = :oContractXBU.Id];
	
			System.assert(oContractXBU.Business_Unit_Group__c == null);
			System.assert(oContractXBU.Business_Unit__c == null);
			System.assert(oContractXBU.Sub_Business_Unit__c == null);


			ctrlExt_ContractXBU_BU_Info oController = new ctrlExt_ContractXBU_BU_Info(new ApexPages.StandardController(oContractXBU));
		
			System.assert(oController.lstBusinessUnitGroup.size() == clsTestData_MasterData.lstBusinessUnitGroup.size());

			// Select 1 SBU				
			oController.lstBusinessUnitGroup[0].businessUnits[0].subBusinessUnits[0].selected = true;
			oController.sbuSelected();

			// Verify that the related BU and BUG are also selected		
			System.assert(oController.lstBusinessUnitGroup[0].selected == true);
			System.assert(oController.lstBusinessUnitGroup[0].businessUnits[0].selected == true);
		
			// Save and verify that the BUG, BU and SBU are populated
			oController.save();
		
			oContractXBU = [SELECT Business_Unit_Group__c, Business_Unit__c, Sub_Business_Unit__c FROM Contract_XBU__c WHERE Id = :oContractXBU.Id];
	
			System.assert(oContractXBU.Business_Unit_Group__c != null);
			System.assert(oContractXBU.Business_Unit__c != null);
			System.assert(oContractXBU.Sub_Business_Unit__c != null);
				

			// Open the VF Page using the Contract XBU record which has 1 selected BUG, BU and SBU
			oController = new ctrlExt_ContractXBU_BU_Info(new ApexPages.StandardController(oContractXBU));
		
			System.assert(oController.lstBusinessUnitGroup[0].selected == true);
			System.assert(oController.lstBusinessUnitGroup[0].businessUnits[0].selected == true);
			System.assert(oController.lstBusinessUnitGroup[0].businessUnits[0].subBusinessUnits[0].selected == true);

			// Deselect the SBU		
			oController.lstBusinessUnitGroup[0].businessUnits[0].subBusinessUnits[0].selected = false;
			oController.sbuSelected();
		
			// Verify that the related BU and BUG are also deselected
			System.assert(oController.lstBusinessUnitGroup[0].selected == false);
			System.assert(oController.lstBusinessUnitGroup[0].businessUnits[0].selected == false);
		
			// Save and verify that the BUG, BU and SBU are empty
			oController.save();
		
			oContractXBU = [SELECT Business_Unit_Group__c, Business_Unit__c, Sub_Business_Unit__c FROM Contract_XBU__c WHERE Id = :oContractXBU.Id];
	
			System.assert(oContractXBU.Business_Unit_Group__c == null);
			System.assert(oContractXBU.Business_Unit__c == null);
			System.assert(oContractXBU.Sub_Business_Unit__c == null);


		Test.stopTest();
		//--------------------------------------------------------


		//--------------------------------------------------------
		// Validate Result
		//--------------------------------------------------------

		//--------------------------------------------------------

    }
    
    
}