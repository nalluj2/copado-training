public class ctrl_User_Selector {
	public OSS_User_Override__c cur {get;set;}

	public ctrl_User_Selector(ApexPages.StandardController stdController) {
		if(!string.isblank(ApexPages.currentPage().getParameters().get('id'))) {
			system.debug('### ID= ' +  string.isblank(ApexPages.currentPage().getParameters().get('id')));

			this.cur =  [select id,
								name,
								User_Operator__c,
								OSS_Content__c,
								User__c
						   from	OSS_User_Override__c
						  where id = :ApexPages.currentPage().getParameters().get('id')
						  limit 1 
			]; 
			system.debug('### this.cur= ' +  this.cur.id);
			
		} else {
			this.cur = (OSS_User_Override__c)stdController.getRecord();
			if(!string.isblank(ApexPages.currentPage().getParameters().get('00N11000001ay4J'))) this.cur.OSS_Content__c = ApexPages.currentPage().getParameters().get('00N11000001ay4J'); 
			system.debug('### sectionid= ' +  this.cur.OSS_Content__c);
		}
	}
	
	public pagereference cancel() {
		return new pagereference('/' + this.cur.OSS_Content__c);
	}
	
	public pagereference save() {
		upsert this.cur;
		return new pagereference('/' + this.cur.OSS_Content__c);
	}  
	
}