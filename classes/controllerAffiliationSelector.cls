/**
 * Creation Date: 	20090218
 * Description: 	controllerAffiliationSelector class that handles the redirect to the proper Affiliation page
 *				
 * Author: 	ABSI - BC
 */
 
public with sharing class controllerAffiliationSelector {
	
	private Map<String, String> dataMap = new Map<String, String>();
	
   	public void setDataMap(Map<String, String> dataMapIn) {
   		dataMap = dataMapIn;
   	}
   		
	public controllerAffiliationSelector(ApexPages.StandardController stdController) {
		this.parseWeb();
	}
	
	private void parseWeb() {
		dataMap.put('URL',ApexPages.currentPage().getUrl());
		String recordTypeId = ApexPages.currentPage().getParameters().get('RecordType');
		String affId = ApexPages.currentPage().getParameters().get('Id');
		if (affId <> null) {
			Affiliation__c curAff = [select RecordTypeId From Affiliation__c where Id =: affId];
			recordTypeId = curAff.RecordTypeId;
		}
		if (recordTypeId <> null)
			dataMap.put('recordTypeId',recordTypeId.substring(0, 15));
		
	}
	
	// Method that handles the redirects to the appropriate Affiliation Page depending on the RecordType
	
	public PageReference redirectToAffiliationCreatePages(){
		String URL = dataMap.get('URL'); 
		String recordTypeId = dataMap.get('recordTypeId');
		System.debug('################## ' + 'URL=' + URL + '################');
		System.debug('################## ' + 'recordTypeId= ' + recordTypeId + '################');
		PageReference pageRef = new PageReference('/apex/NoRecordTypeId');
		if (recordTypeId == FinalConstants.recordTypeIdC2C) {
			pageRef = new PageReference(URL.replace('affiliationSelectorPage',FinalConstants.C2CAffiliationPage));
		}
		if (recordTypeId == FinalConstants.recordTypeIdC2A) {
			pageRef = new PageReference(URL.replace('affiliationSelectorPage',FinalConstants.C2AAffiliationPage));
		}
		if (recordTypeId == FinalConstants.recordTypeIdA2A) {
			pageRef = new PageReference(URL.replace('affiliationSelectorPage',FinalConstants.A2AAffiliationPage));
		}		
		pageRef.setRedirect(true);
		System.debug('################## ' + 'PageReference= ' + pageRef + '################');
		return pageRef;
	}

}