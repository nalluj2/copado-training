@isTest 
private class TEST_ba_ObjectUsageInspection {


	@testSetup private static void createTestData(){

		// Master Data
		clsTestData_MasterData.createSubBusinessUnit();

		// Account Data
		clsTestData_Account.createAccount();

		// Contact Data
		clsTestData_Contact.createContact();

	}
	

	@isTest private static void test_General(){

		ba_ObjectUsageInspection oBatch = new ba_ObjectUsageInspection();
			oBatch.loadSObjectNames();
			oBatch.loadCompany_BU_SBU_Mapping();
		
	}


	@isTest private static void test_DeleteAll(){

		ba_ObjectUsageInspection oBatch = new ba_ObjectUsageInspection();
			oBatch.lstSObjectName = new List<String>{'Account','Contact'};
			oBatch.tAction = 'Delete_All';
			oBatch.tProcessingObjectType = 'ALL';
		Database.executebatch(oBatch, 2000);

	}

	@isTest private static void test_Created(){

		ba_ObjectUsageInspection oBatch = new ba_ObjectUsageInspection();
			oBatch.lstSObjectName = new List<String>{'Account','Contact'};
			oBatch.tAction = 'Created';
			oBatch.tProcessingObjectType = 'ALL';
		Database.executebatch(oBatch, 2000);

	}


	@isTest private static void test_Created_Error(){

		clsUtil.tExceptionName = 'EX_QueryLocator';

		ba_ObjectUsageInspection oBatch = new ba_ObjectUsageInspection();
			oBatch.lstSObjectName = new List<String>{'Account','Contact'};
			oBatch.tAction = 'Created';
			oBatch.tProcessingObjectType = 'ALL';
			oBatch.bLogProcessingObject = true;
		Database.executebatch(oBatch, 2000);

		List<Object_Usage_Inspection__c> lstObjectUsageInspection = [SELECT Id, Object_Name__c, Action__c, NumRecords__c FROM Object_Usage_Inspection__c];
		for (Object_Usage_Inspection__c oObjectUsageInspection : lstObjectUsageInspection) System.debug('**BC** test_Created_Error - oObjectUsageInspection : ' + oObjectUsageInspection);

	}


	@isTest private static void test_Updated(){

		ba_ObjectUsageInspection oBatch = new ba_ObjectUsageInspection();
			oBatch.lstSObjectName = new List<String>{'Account','Contact'};
			oBatch.tAction = 'Updated';
			oBatch.tProcessingObjectType = 'ALL';
		Database.executebatch(oBatch, 2000);

	}


}