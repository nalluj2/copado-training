public with sharing class ctrl_FieldHistory {

	private List<sObject> lstFieldHistory;
	private Integer iRecordLimit;
	private Integer iRecordMax = 999;


    //--------------------------------------------------------------------------------------------------------------
    // Constructor
    //--------------------------------------------------------------------------------------------------------------
	public ctrl_FieldHistory() {

		tTitle_PageBlock = 'History';
		iNumberOfVisibleRecords = 1;
		iRecordStep = 1;

		bShowMoreRecords = true;
		bShowLessRecords = true;
		bShowAllRecords = true;

		bDisableMoreRecords = false;
		bDisableMoreRecords = false;
		bDisableMoreRecords = false;

	}
    //--------------------------------------------------------------------------------------------------------------


    //--------------------------------------------------------------------------------------------------------------
    // Getters & Setters
    //--------------------------------------------------------------------------------------------------------------
	public Id id_Record { get; set; }
	public String tTitle_PageBlock { get; set; }
	public String tSObjectName { get; set; }
	public Integer iNumberOfVisibleRecords { get; set; }
	public Integer iRecordStep { 
		get; 
		set{
			iRecordStep = value;
			tLabel_ShowMore = '<b>Show ' + iRecordStep + ' more</b>';
			tLabel_ShowLess = '<b>Show ' + iRecordStep + ' less</b>';
			tLabel_ShowAll = '<b>Show all</b>';
		} 
	}
	public Integer iRecordSelected { get; private set; }

	public Boolean bShowMoreRecords { get; set; }
	public Boolean bShowLessRecords { get; set; }
	public Boolean bShowAllRecords { get; set; }

	public Boolean bDisableMoreRecords { get; set; }
	public Boolean bDisableLessRecords { get; set; }
	public Boolean bDisableAllRecords { get; set; }

	public String tLabel_ShowMore { get; private set; }
	public String tLabel_ShowLess { get; private set; }
	public String tLabel_ShowAll { get; private set; }
    //--------------------------------------------------------------------------------------------------------------


    //--------------------------------------------------------------------------------------------------------------
    // Actions
    //--------------------------------------------------------------------------------------------------------------
	public List<cFieldHistory> getFieldHistory(){

		if (id_Record == null) id_Record = ApexPages.currentPage().getParameters().get('Id');
		if (iNumberOfVisibleRecords == null) iNumberOfVisibleRecords = 1;	
		if (iRecordLimit == null) iRecordLimit = iNumberOfVisibleRecords;

		if (lstFieldHistory == null){
			iRecordLimit = iNumberOfVisibleRecords;
		}
		loadData();

		List<cFieldHistory> lstcFieldHistory = new List<cFieldHistory>();
		for (sObject oFieldHistory : lstFieldHistory){

			cFieldHistory ocFieldHistory = new cFieldHistory(oFieldHistory);
			lstcFieldHistory.add(ocFieldHistory);

		}

		return lstcFieldHistory;

	}

	public void showMoreRecords(){

		if (iRecordLimit < iRecordMax) iRecordLimit += iRecordStep;
		loadData();

	}

	public void showLessRecords(){

		if (iRecordLimit == iRecordMax){
			iRecordLimit = iRecordSelected - iRecordStep;
		}else{
			if (iRecordLimit - iRecordStep < iNumberOfVisibleRecords){
				iRecordLimit = iNumberOfVisibleRecords;
			}else{
				iRecordLimit -= iRecordStep;
			}
		}
		if (iRecordLimit < iNumberOfVisibleRecords) iRecordLimit = iNumberOfVisibleRecords;
		loadData();

	}

	public void showAllRecords(){

		iRecordLimit = iRecordMax;
		loadData();

	}
    //--------------------------------------------------------------------------------------------------------------



    //--------------------------------------------------------------------------------------------------------------
    // Private functions
    //--------------------------------------------------------------------------------------------------------------
	private void loadData(){

		// We select x records more then needed because we need to filter out some records AFTER we selected them because this filter can't be included in the SOQL.
		//	When a User is assigned 2 records are logged in the History table - 1 with the SFDC ID's and 1 with the names - we only need the one with the names.
		Integer iRecordLimitPlus10 = iRecordLimit + 10;
		String tSOQL = 'SELECT ParentId, CreatedById, CreatedBy.Name, Field, OldValue, NewValue, CreatedDate FROM ' + tSObjectName + ' WHERE ParentId =:id_Record ORDER BY CreatedDate DESC LIMIT ' + iRecordLimitPlus10;
		List<sObject> lstFieldHistory_tmp = Database.query(String.escapeSingleQuotes(tSOQL));

		lstFieldHistory = new List<sObject>();

		for (sObject oFieldHistory : lstFieldHistory_tmp){

			if ( (oFieldHistory.get('NewValue') != null) && (String.valueOf(oFieldHistory.get('NewValue')).startsWith('005')) ) continue;

			lstFieldHistory.add(oFieldHistory);

			if (lstFieldHistory.size() > iRecordLimit) break;	// This will result in 1 record more in the list than we actually need

		}

		if (lstFieldHistory.size() <= iRecordLimit){

			// There are not more records then record limit so all records are visible on the screen so we don't need the Show More / Show Less / Show All buttons anymore
			bDisableMoreRecords = true;
			bDisableLessRecords = true;
			bDisableAllRecords = true;

			bShowMoreRecords = false;
			bShowLessRecords = false;
			bShowAllRecords = false;

		}else{

			bDisableMoreRecords = false;
			bDisableLessRecords = false;
			bDisableAllRecords = false;

			bShowMoreRecords = true;
			bShowLessRecords = true;
			bShowAllRecords = true;

			lstFieldHistory.remove(lstFieldHistory.size()-1);	// remove the additional record from the list which we don't need to display

		}
		iRecordSelected = lstFieldHistory.size();

		if (lstFieldHistory.size() <= iNumberOfVisibleRecords){
			bDisableLessRecords = true;
		}else{
			bDisableLessRecords = false;
		}


	}
    //--------------------------------------------------------------------------------------------------------------



    //--------------------------------------------------------------------------------------------------------------
    // Helper Class
    //--------------------------------------------------------------------------------------------------------------
	public class cFieldHistory{

		public cFieldHistory(sObject oData){

			tUserName = '';
			tCreatedDateTime = '';
			tAction = '';

			sObject oUser = oData.getSObject('CreatedBy');
			if (oUser != null){
				tUserName = String.valueOf(oUser.get('Name'));
			}

			tCreatedDateTime = DateTime.valueOf(oData.get('CreatedDate')).format();

			String tSObjectPrefix_Parent = String.valueOf(oData.get('ParentId')).left(3);
			String tSObjectName_Parent = clsUtil.getSObjectNameFromIDPrefix(tSObjectPrefix_Parent);

			if (String.valueOf(oData.get('Field')) == 'Created'){

				tAction = 'Created';

			}else{

				String tFieldName = String.valueOf(oData.get('Field'));
				String tFieldLabel = clsUtil.tGetFieldLabel(tSObjectName_Parent, tFieldName);
				tAction = 'Changed <b><i>' + tFieldLabel + '</i></b>';
				if (oData.get('OldValue') != null){

					tAction += ' from <b><i>' + String.valueOf(oData.get('OldValue')) + '</i></b>';

				}
				if (oData.get('NewValue') != null){

					tAction += ' to <b><i>' + String.valueOf(oData.get('NewValue')) + '</i></b>';

				}
			}

		}

		public String tUserName { get; set; }
		public String tCreatedDateTime { get; set; }
		public String tAction { get; set; }
		
	}
    //--------------------------------------------------------------------------------------------------------------

}