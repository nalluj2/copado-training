//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 15-12-2015
//  Description      : Scheduled APEX to reprocess failed callouts to WebMethods record.
//  Change Log       : 
//      25-07-2006 - Bart Caelen
//          Added usage of field Retry_Cron_Job_Id__c instead of the Retry_Cron_Job_Name__c to make sure that the 
//              correct Notification to SAP is Retried / Aborted
//--------------------------------------------------------------------------------------------------------------------
global class sc_NotificationSAP_Retry implements Schedulable {

    global void execute(SchedulableContext sc) {

        // Get the CronTrigger Id
        Id id_CronTrigger = sc.getTriggerId();

        // Select the data that needs reprocessing - this should return 1 record
        String tSOQL_Where = 'Status__c = \'RETRY\' AND Retry_Cron_Job_Id__c = \'' + id_CronTrigger + '\'';
        List<NotificationSAPLog__c> lstNotificationSAPLog = ut_Log.loadNotificationSAPLog(tSOQL_Where, '');

        clsUtil.debug('sc_NotificationSAP_Retry.execute - lstNotificationSAPLog (' + lstNotificationSAPLog.size() + ') : ' + lstNotificationSAPLog);

        if (lstNotificationSAPLog.size() > 0){

            Set<Id> setNotificationSAPLog_Id_Processing= new Set<Id>();
            for (NotificationSAPLog__c oNotificationSAPLog : lstNotificationSAPLog){
                setNotificationSAPLog_Id_Processing.add(oNotificationSAPLog.Id);
            }

            clsUtil.debug('sc_NotificationSAP_Retry - setNotificationSAPLog_Id_Processing (' + setNotificationSAPLog_Id_Processing.size() + ') : ' + setNotificationSAPLog_Id_Processing);
            try{
            	
            	ba_NotificationSAP_Retry oBatch = new ba_NotificationSAP_Retry();
                	oBatch.setId_Record = setNotificationSAPLog_Id_Processing;
            	Database.executebatch(oBatch, 1);   // Batch Size needs to be 1 because we need to call the WebMethods Web Service record by record
            	
            }catch(Exception e){
            	
            	//If for whatever reason the batch cannot be executed we update the notification to reflect the fact that it will not be retried
            	for (NotificationSAPLog__c oNotificationSAPLog : lstNotificationSAPLog){
                	
                	oNotificationSAPLog.Status__c = 'FAILED';
                	oNotificationSAPLog.Error_Description__c = e.getMessage();
            	}	
            	
            	update lstNotificationSAPLog;
            }

        }else{
            clsUtil.debug('sc_NotificationSAP_Retry - no records to process');
        }

        System.abortJob(id_CronTrigger); // Abort / Delete the processed Scheduled Job        

    }

    public static void scheduleAPEX(NotificationSAPLog__c oNotificationSAPLog, Integer iAdditionalDays, Integer iAdditionalHours, Integer iAdditionalMinutes, Integer iAdditionalSeconds){

        String tCronExpression = '';

        // Previous Scheduled Job Id
        Id id_CronTrigger = oNotificationSAPLog.Retry_Cron_Job_Id__c;

        clsUtil.debug('sc_NotificationSAP_Retry - scheduleAPEX - id_CronTrigger: ' + id_CronTrigger);

        // Scheduled Job Name
        String tCronJobName = bl_NotificationSAP.SCHEDULED_NOTIFICATION_NAME + oNotificationSAPLog.WebServiceName__c + ' : ' + oNotificationSAPLog.Record_ID__c;

        // Validate if there is another scheduled apex for the same record
        // NO --> abort previous jobs and schedule the apex
        // YES --> don't schedule a new job
        Boolean bScheduleRetry = true;

        if (!clsUtil.isBlank(id_CronTrigger)){
            // Abort previous scheduled jobs - if any
            List<CronTrigger> lstCronTrigger = [SELECT Id FROM CronTrigger WHERE Id = :id_CronTrigger];
            clsUtil.debug('sc_NotificationSAP_Retry - scheduleAPEX - lstCronTrigger (' + lstCronTrigger.size() + ') : ' + lstCronTrigger);
            for (CronTrigger oCronTrigger : lstCronTrigger){
                System.abortJob(oCronTrigger.Id);
            }
        }

        // Create a new scheduled job
        if (bScheduleRetry){

            // Get the current Datetime
            Datetime dtNow = Datetime.now();

            // Define the next execute time by adding the minutes
            DateTime dtNextSchedule = dtNow.addDays(iAdditionalDays).addHours(iAdditionalHours).addMinutes(iAdditionalMinutes).addSeconds(iAdditionalSeconds);

            // Create the Cron Expression to schedule the class
            tCronExpression = 
                String.valueOf(dtNextSchedule.second()) 
                + ' ' + String.valueOf(dtNextSchedule.minute()) 
                + ' ' + String.valueOf(dtNextSchedule.hour()) 
                + ' ' + String.valueOf(dtNextSchedule.day()) 
                + ' ' + String.valueOf(dtNextSchedule.month()) 
                + ' ?'
                + ' ' + String.valueOf(dtNextSchedule.year());

			try{

	            // Schedule the retry and retrieve the new Job Id
	            sc_NotificationSAP_Retry scNotificationSAPRetry = new sc_NotificationSAP_Retry();
	            id_CronTrigger = System.schedule(tCronJobName, tCronExpression, scNotificationSAPRetry);
	
	            clsUtil.debug('sc_NotificationSAP_Retry - scheduleAPEX - oNotificationSAPLog.Retry_Cron_Job_Id__c : ' + id_CronTrigger);
	
	            oNotificationSAPLog.Retry_Cron_Job_Id__c = id_CronTrigger;
	            oNotificationSAPLog.Retry_Cron_Job_Name__c = tCronJobName;
	            oNotificationSAPLog.Retry_Cron_Expression__c = tCronExpression;
	            oNotificationSAPLog.Retry_DateTime__c = dtNextSchedule;
	            
			}catch(Exception e){
				
				oNotificationSAPLog.Status__c = 'FAILED';	            
			}
        }

        // Update the oNotificationSAPLog
        upsert oNotificationSAPLog;

    }

}
//--------------------------------------------------------------------------------------------------------------------