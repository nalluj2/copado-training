//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 13-02-2019
//  Description      : APEX Test Class for tr_ClientExpectation and bl_ClientExpectation_Trigger
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_bl_ClientExpectation_Trigger{

	//---------------------------------------------------------------------------------------------------------------
	// TEST IHS_SetExpectationReadyDate
	//---------------------------------------------------------------------------------------------------------------
	@isTest private static void test_IHS_SetExpectationReadyDate(){

		// Create Test Data
		User oUser_Admin1 = clsTestData_User.createUser_SystemAdministratorMDT('tstadm1', true);
		Opportunity oOpportunity;
		
		System.runAs(oUser_Admin1){
			oOpportunity = clsTestData_Opportunity.createOpportunity_IHS_Stage('MANAGEDSERVICE', '0', true);
		}

		List<Client_Expectation__c> lstClientExpectation = new List<Client_Expectation__c>();
		Client_Expectation__c oClientExpectation1 = new Client_Expectation__c();
			oClientExpectation1.Opportunity__c = oOpportunity.Id;
			oClientExpectation1.Priority__c = 'Yes';
			oClientExpectation1.Progress__c = 'On track';
			oClientExpectation1.Expectation__c = 'TEST Expectation';
			oClientExpectation1.Expectation_Owner__c = UserInfo.getUserName();
			oClientExpectation1.Expectation_Source__c = 'Other';
			oClientExpectation1.Expectation_Source_Date__c = Date.today();
			oClientExpectation1.Expectation_Realization_days__c = '30';
			oClientExpectation1.Expectation_Ready_Date__c = Date.today();
		lstClientExpectation.add(oClientExpectation1);

		Client_Expectation__c oClientExpectation2 = new Client_Expectation__c();
			oClientExpectation2.Opportunity__c = oOpportunity.Id;
			oClientExpectation2.Priority__c = 'Yes';
			oClientExpectation2.Progress__c = 'On track';
			oClientExpectation2.Expectation__c = 'TEST Expectation';
			oClientExpectation2.Expectation_Owner__c = UserInfo.getUserName();
			oClientExpectation2.Expectation_Source__c = 'Other';
			oClientExpectation2.Expectation_Source_Date__c = Date.today();
			oClientExpectation2.Expectation_Realization_days__c = '30';
		lstClientExpectation.add(oClientExpectation2);


		// Execute Logic
		Test.startTest();
		
			System.runAs(oUser_Admin1){
				insert lstClientExpectation;
			}

			lstClientExpectation = [SELECT Id, Expectation_Source_Date__c, Expectation_Realization_days__c, Expectation_Ready_Date__c FROM Client_Expectation__c];
			System.assertEquals(lstClientExpectation.size(), 2);

			Integer iCounter = 0;
			for (Client_Expectation__c oClientExpectation : lstClientExpectation){
				if (oClientExpectation.Id == oClientExpectation1.Id){
					System.assertEquals(oClientExpectation.Expectation_Ready_Date__c, oClientExpectation.Expectation_Source_Date__c);
					System.assertEquals(oClientExpectation.Expectation_Realization_days__c, '30');
					iCounter++;
				}else if (oClientExpectation.Id == oClientExpectation2.Id){
					System.assertEquals(oClientExpectation.Expectation_Ready_Date__c, oClientExpectation.Expectation_Source_Date__c.addDays(30));
					System.assertEquals(oClientExpectation.Expectation_Realization_days__c, '30');
					iCounter++;
				}
			}
			System.assertEquals(iCounter, 2);


			oClientExpectation1.Expectation_Realization_days__c = '90';
			oClientExpectation2.Expectation_Ready_Date__c = Date.today();
			System.runAs(oUser_Admin1){
				update new List<Client_Expectation__c>{oClientExpectation1, oClientExpectation2};
			}

			lstClientExpectation = [SELECT Id, Expectation_Source_Date__c, Expectation_Realization_days__c, Expectation_Ready_Date__c FROM Client_Expectation__c];
			System.assertEquals(lstClientExpectation.size(), 2);

			iCounter = 0;
			for (Client_Expectation__c oClientExpectation : lstClientExpectation){
				if (oClientExpectation.Id == oClientExpectation1.Id){
					System.assertEquals(oClientExpectation.Expectation_Realization_days__c, '90');
					System.assertEquals(oClientExpectation.Expectation_Ready_Date__c, oClientExpectation.Expectation_Source_Date__c.addDays(90));
					iCounter++;
				}else if (oClientExpectation.Id == oClientExpectation2.Id){
					System.assertEquals(oClientExpectation.Expectation_Realization_days__c, '30');
					System.assertEquals(oClientExpectation.Expectation_Ready_Date__c, Date.today());
					iCounter++;
				}
			}
			System.assertEquals(iCounter, 2);


		Test.stopTest();


		// Validate Result

		
	}
	//---------------------------------------------------------------------------------------------------------------

}
//--------------------------------------------------------------------------------------------------------------------