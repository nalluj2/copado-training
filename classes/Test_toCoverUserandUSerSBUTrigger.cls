/*
 *   Class Name  : Test_toCoverUserandUSerSBUTrigger
 *   Author      : Manish
 *   Modified by : Priti Jaiswal
 *   Description : This class also used to cover the ctrlExt_ManageUserSBUs controller since the trigger is
 *                 deactivated to remove the automatic creation of User SBU record.   
*/

@isTest(seealldata=true)
    Private class Test_toCoverUserandUSerSBUTrigger 
    {    
        static testMethod void myUnitTest() 
        {
            
            user u=new user(FirstName='testUser11',communityNickname='test',LanguageLocaleKey='en_US',
                        TimeZoneSidKey='America/Los_Angeles',
                        LocaleSidKey='en_US',
                        EmailEncodingKey='ISO-8859-1',lastName='test1',
                        email='test1234@wipro.com',username='test123456@wipro.com',alias='hi34',
                        Alias_unique__c='sd34',Company_Code_Text__c='EUR',CostCenter__c='123498',
                        profileid=userinfo.getprofileid(),Job_Title_vs__c='Other',
                        Region_vs__c = 'Europe', Sub_Region__c = 'NWE', Country_vs__c='NETHERLANDS',User_Business_Unit_vs__c='CRHF'); 
            
            
            insert u;
            
            u=new user(id=userinfo.getuserid(),User_Business_Unit_vs__c='All');
            update u;
            u=new user(id=userinfo.getuserid(),User_Business_Unit_vs__c='Neurovascular');
            update u;
            
            ApexPages.currentPage().getParameters().put('id',u.id);
            ApexPages.StandardController IController = new ApexPages.StandardController(u);
            ctrlExt_ManageUserSBUs ctlr=new ctrlExt_ManageUserSBUs(IController);  
            ctlr.save();
            ctlr.selectSBU();
            ctlr.Cancel();
            
            
            }
            }