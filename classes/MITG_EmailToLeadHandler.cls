//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Name    : MITG_EmailToLeadHandler 
// Author  : Bart Caelen
// Date    : 20/01/2017
// Purpose : Handle inbound emails to create MITG Leads
// Dependencies: 
//
// ========================
// = MODIFICATION HISTORY =
// ========================
// DATE        	AUTHOR           	CHANGE
// 06/04/2017	Bart Caelen 		CR-13982 - Store the email Subject in Product_Solution_of_Interest__c on Lead
// 06/04/2017	Bart Caelen 		CR-13997 - Changed the Lead Source from "Connect.me" to "Connect.me email"
// 29/06/2017	Bart Caelen 		CR-14226 - Merged the logic in MITG_EmailToLeadHandler into bl_InboundEmailHandler_MITG.handleInboundEmail_ConnectMe
//										and added additional logic to bl_InboundEmailHandler_MITG to process MITG Emails AdobeConnect and ManualEmail
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
global class MITG_EmailToLeadHandler implements Messaging.InboundEmailHandler {

	global Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail oInboundEmail, Messaging.InboundEnvelope oInboundEnvelope){

		Messaging.InboundEmailResult oInboundEmailResult;		

		if (oInboundEnvelope.toAddress.startsWith('mitg_email_to_lead@')){

			oInboundEmailResult = bl_InboundEmailHandler_MITG.handleInboundEmail_ConnectMe(oInboundEmail, oInboundEnvelope);

		}else if (oInboundEnvelope.toAddress.startsWith('mitg_email_to_lead_adobe_connect@')){

			oInboundEmailResult = bl_InboundEmailHandler_MITG.handleInboundEmail_AdobeConnect(oInboundEmail, oInboundEnvelope);

		}else if (oInboundEnvelope.toAddress.startsWith('mitg_email_to_lead_manual_email@')){

			oInboundEmailResult = bl_InboundEmailHandler_MITG.handleInboundEmail_ManualEmail(oInboundEmail, oInboundEnvelope);

		}

		return oInboundEmailResult;

	}

}
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------