@RestResource(urlMapping='/DiabetesPerformanceService/*')
global with sharing class ws_DiabetesPerformanceService {
    
    @HttpPost
	global static String doPost() {
		
 		RestRequest req = RestContext.request;
			
		System.debug('post called '+req);
		System.debug('post requestBody '+req.requestBody);
			
		String body = req.requestBody.toString();
		IFDiabetesPerformanceRequest diabetesPerformanceRequest = (IFDiabetesPerformanceRequest) System.Json.deserialize(body, IFDiabetesPerformanceRequest.class);
			
		System.debug('post requestBody '+ diabetesPerformanceRequest);
			
		Diabetes_Performance__c DiabetesPerformance = DiabetesPerformanceRequest.DiabetesPerformance;

		IFDiabetesPerformanceResponse resp = new IFDiabetesPerformanceResponse();

		//We catch all exception to assure that 
		try{
				
			resp.DiabetesPerformance = bl_AccountPlanning.saveDiabetesPerformance(diabetesPerformance);
			resp.id = diabetesPerformanceRequest.id;
	
			resp.success = true;
			
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
		}
 
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'DiabetesPerformance' ,req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
  			
		return System.JSON.serialize(resp);		 			
	}
	
	global class IFDiabetesPerformanceRequest{
		public Diabetes_Performance__c diabetesPerformance {get;set;}
		public String id{get;set;}
	}

	global class IFDiabetesPerformanceResponse{
		public Diabetes_Performance__c diabetesPerformance {get;set;}
		public String id{get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}
}