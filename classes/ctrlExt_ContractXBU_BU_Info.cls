public with sharing class ctrlExt_ContractXBU_BU_Info {
    
    private Contract_xBU__c oContractXBU;
    
    public List<BusinessUnitGroup> lstBusinessUnitGroup { get; set; }
    public Map<String, SubBusinessUnit> mapSubBusinessUnit { get; set; }
        
    public ctrlExt_ContractXBU_BU_Info(ApexPages.StandardController oStdCtrl){
    	
    	if(Test.isRunningTest() == false){
    		oStdCtrl.addFields(new List<String>{'Business_Unit_Group__c', 'Business_Unit__c', 'Sub_Business_Unit__c'});
    	}
    	
    	oContractXBU = (Contract_xBU__c)oStdCtrl.getRecord();
    	
    	User oUser = [SELECT Company_Code_Text__c FROM User WHERE Id = :UserInfo.getUserId()];
    	
    	Set<String> set_BUG_Selected = new Set<String>();
    	Set<String> set_BU_Selected = new Set<String>();
    	Set<String> set_SBU_Selected = new Set<String>();
    	
    	if(oContractXBU.Business_Unit_Group__c != null) set_BUG_Selected = new Set<String>(oContractXBU.Business_Unit_Group__c.split(';'));
    	if(oContractXBU.Business_Unit__c != null) set_BU_Selected = new Set<String>(oContractXBU.Business_Unit__c.split(';'));
    	if(oContractXBU.Sub_Business_Unit__c != null) set_SBU_Selected = new Set<String>(oContractXBU.Sub_Business_Unit__c.split(';'));
    	
    	lstBusinessUnitGroup = new List<BusinessUnitGroup>();
    	mapSubBusinessUnit = new Map<String, SubBusinessUnit>();
        	
        Map<Id, Business_Unit__c> mapBusinessUnit = new Map<Id, Business_Unit__c>([SELECT Id, (Select Name FROM Sub_Business_Units__r ORDER BY Name) FROM Business_Unit__c WHERE Company__r.Company_Code_Text__c = :oUser.Company_Code_Text__c]);
        	
    	for(Business_Unit_Group__c oBUG : [SELECT Name, (SELECT Id, Name FROM Business_Units__r ORDER BY Name) FROM Business_Unit_Group__c WHERE Master_Data__r.Company_Code_Text__c = :oUser.Company_Code_Text__c ORDER BY Name]){
    		    		
    		BusinessUnitGroup oBusinessUnitGroup = new BusinessUnitGroup();
    		oBusinessUnitGroup.Name = oBUG.Name;
    		
    		if(set_BUG_Selected.contains(oBusinessUnitGroup.Name)) oBusinessUnitGroup.selected = true;
    		
    		oBusinessUnitGroup.BusinessUnits = new List<BusinessUnit>();
    		
    		Integer bugRowSpan = 0;
    		
    		for(Business_Unit__c bUnit : oBUG.Business_Units__r){
    			
    			BusinessUnit bu = new BusinessUnit();
    			bu.Name = bUnit.Name;
    			    			    			
    			if(set_BU_Selected.contains(bu.Name)) bu.selected = true;
    			
    			bu.SubBusinessUnits = new List<SubBusinessUnit>();
    			
    			for(Sub_Business_Units__c sbUnit : mapBusinessUnit.get(bUnit.Id).Sub_Business_Units__r){
    				
    				SubBusinessUnit sbu = new SubBusinessUnit();
    				sbu.Name = sbUnit.Name;
					    				    				
    				if(set_SBU_Selected.contains(sbu.Name)) sbu.selected = true;
    				
    				bu.SubBusinessUnits.add(sbu);	
    				mapSubBusinessUnit.put(sbu.Name, sbu);
    			}  
    			
    			if(bu.SubBusinessUnits.size() > 0) bu.SubBusinessUnits[0].firstRow = true; 
    			bu.rowSpan = bu.SubBusinessUnits.size();
    			bugRowSpan += bu.SubBusinessUnits.size();
    			
    			oBusinessUnitGroup.BusinessUnits.add(bu);
    		}
    		
    		if(oBusinessUnitGroup.BusinessUnits.size() > 0) oBusinessUnitGroup.BusinessUnits[0].firstRow = true;
    		oBusinessUnitGroup.rowSpan = bugRowSpan;
    		
    		lstBusinessUnitGroup.add(oBusinessUnitGroup);    		

    	}

    }

            
    public void sbuSelected(){
    	
    	for(BusinessUnitGroup oBusinessUnitGroup : lstBusinessUnitGroup){
    		
    		Boolean buSelected = false;
    		
    		for(BusinessUnit bu : oBusinessUnitGroup.BusinessUnits){
    			
    			Boolean sbuSelected = false;
    			
    			for(SubBusinessUnit sbu : bu.SubBusinessUnits){
    				
    				if(sbu.selected == true) sbuSelected = true;
    			}
    			
    			bu.selected = sbuSelected;
    			
    			if(bu.Selected == true) buSelected = true;
    		}	
    		
    		oBusinessUnitGroup.selected = buSelected;
    	}

    }

    
    public PageReference save(){
    	
    	Set<String> set_BUG_Selected = new Set<String>();
    	Set<String> set_BU_Selected = new Set<String>();
    	Set<String> set_SBU_Selected = new Set<String>();
    	
    	for(BusinessUnitGroup oBusinessUnitGroup : lstBusinessUnitGroup){
    		    		    		
    		for(BusinessUnit oBusinessUnit : oBusinessUnitGroup.BusinessUnits){
    		    			
    			for(SubBusinessUnit oSubBusinessUnit : oBusinessUnit.SubBusinessUnits){
    				
    				if(oSubBusinessUnit.selected == true){
    					
    					set_SBU_Selected.add(oSubBusinessUnit.Name);
    					set_BU_Selected.add(oBusinessUnit.Name);
    					set_BUG_Selected.add(oBusinessUnitGroup.Name);
    				}
    			}
    		}    		
    	}
    	
    	oContractXBU.Business_Unit_Group__c = String.join(new List<String>(set_BUG_Selected), ';');
    	oContractXBU.Business_Unit__c = String.join(new List<String>(set_BU_Selected), ';');
    	oContractXBU.Sub_Business_Unit__c = String.join(new List<String>(set_SBU_Selected), ';');
    	
    	try{
    		
    		update oContractXBU;
    		
    	}catch(Exception e){
    		
    		ApexPages.addMessages(e);
    		return null;

    	}
    	
    	PageReference oPageReference = new PageReference('/' + oContractXBU.Id);
	    	oPageReference.setRedirect(true);
    	return oPageReference;

    }
    

    public class BusinessUnitGroup {
    	
    	public String name { get; set; }
    	public Boolean selected { get; set; }
    	public Integer rowSpan { get; set; }
    	
    	public List<BusinessUnit> businessUnits { get; set; }

    }
    
    public class BusinessUnit {
    	
    	public String name { get; set; }
    	public Boolean selected { get; set; }
    	public Boolean firstRow { get; set; }
    	public Integer rowSpan { get; set; }
    	
    	public List<SubBusinessUnit> subBusinessUnits { get; set; }

    }    

    public class SubBusinessUnit {
    	
    	public String name { get; set; }
    	public Boolean selected { get; set; }
    	public Boolean firstRow { get; set; }   	  	    
    }    
}