//------------------------------------------------------------------------------------------------------------------------------------------------------------
//  Author           :	Bart van Steenbergen
//  Created Date     :	10-12-2019
//  Description      :	APEX Batch for retrieving all accounts which are in scope for deletion.
//  					Per Account record all possible relations with the Account Object will be queried
//  					The results will be stored in a csv file and send to the user who initiated the batch execution.
//  					Depending on the amount of Accounts identified for deletion, multiple batches of 2000 records will be started, with corresponding csv.
//  					The CSV files size should be below 6Mb.
//  					
//  					The batch can be started via the Anonymous window and ba_AccountsInScopeForDeletion.startBatch();
//  Change Log       : 
//------------------------------------------------------------------------------------------------------------------------------------------------------------
global class ba_AccountsInScopeForDeletion implements Database.Batchable<SObject>, Database.Stateful {
    
    global String sCSV = '';
    global List<Id> listContactId;
    global String sErrors = 'The following errors occurred: \n';
    global Integer iRecCounter = 0;
    global Integer iRecordBatchStartReport = 0;
    global Integer iRecordBatchStart = 0;
    global Integer iRecordBatchEnd = 0;
    global Integer iBatch = 0;
    
    global static void startBatch(){
        Integer iRecordsPerBatch = 2000;
		Integer iTotalRecordsForDeletion = database.countQuery('select count() from account where In_Scope_for_Deletion__c = \'Yes\'');
		Integer iTotalNumberBatches = iTotalRecordsForDeletion/iRecordsPerBatch;
		Integer iTotalRemainingRecords = iTotalRecordsForDeletion - (iTotalNumberBatches*iRecordsPerBatch);
		Integer iRecordBatchStart = 0;
		Integer iRecordBatchEnd = 0;
		Integer iBatch = 0;
        
 		for (iBatch = 0; iBatch<iTotalNumberBatches; iBatch++){

			iRecordBatchEnd = iRecordBatchStart + iRecordsPerBatch - 1;
		    ba_AccountsInScopeForDeletion a = new ba_AccountsInScopeForDeletion();
    		a.iRecordBatchStartReport = iRecordBatchStart;
			a.iRecordBatchStart = iRecordBatchStart;
			a.iRecordBatchEnd = iRecordBatchEnd;
			a.iBatch = iBatch;
            
            if(Test.isRunningTest() == false){
				Id batchId = Database.executeBatch(a, 1);
            }
            else{
                Id batchId = Database.executeBatch(a);
            }
    
    		iRecordBatchStart = iRecordBatchStart + iRecordsPerBatch;
		}

		if (iTotalRemainingRecords > 0){
			iRecordBatchEnd = iRecordBatchStart + iTotalRemainingRecords - 1;
    
			ba_AccountsInScopeForDeletion a = new ba_AccountsInScopeForDeletion();
    		a.iRecordBatchStart = iRecordBatchStart;
			a.iRecordBatchEnd = iRecordBatchEnd;
    		a.iBatch = iBatch;
            if(Test.isRunningTest() == false){
				Id batchId = Database.executeBatch(a, 1);
            }
            else{
                Id batchId = Database.executeBatch(a);
            }
		}
    }
    
    private void RetrieveAccountRelationships(){
        
        Schema.DescribeSObjectResult R = Account.SObjectType.getDescribe();
		List<Schema.ChildRelationship> C = R.getChildRelationships();
		List<String> listChildRelationship = new List<String>();
        
        sCSV = sCSV + '\'#\',';
        sCSV = sCSV + '\'Id\',';
		sCSV = sCSV + '\'Name\',';
        sCSV = sCSV + '\'I4CAST_In_scope_automatic_interface__c\',';
		sCSV = sCSV + '\'RecordTypeName\',';
        sCSV = sCSV + '\'CreatedDate\',';
        sCSV = sCSV + '\'CreatedByName\',';
        sCSV = sCSV + '\'Account_Country_vs__c\',';
        
        for (Schema.ChildRelationship rec : C){
    		if (rec.getRelationshipName() != null){
				if (rec.getRelationshipName() == 'RecordActionHistories') continue;
				if (rec.getRelationshipName() == 'PersonOutgoingEmailRelations') continue;
                
        		listChildRelationship.add(rec.getRelationshipName());
                sCSV = sCSV + '\'' + rec.getRelationshipName() + '\',';
    		}
		}
        
        sCSV = sCSV.removeEnd(',');
        sCSV = sCSV + '\n';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        RetrieveAccountRelationships();
        String query = 'Select Id, Name, I4CAST_In_scope_automatic_interface__c, RecordType.Name, CreatedDate, CreatedBy.Name, Account_Country_vs__c from account where In_Scope_for_Deletion__c = \'Yes\' order by CreatedDate limit ' +(iRecordBatchEnd+1);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Account> listAccountBatch) {
        Schema.DescribeSObjectResult R = Account.SObjectType.getDescribe();
		List<Schema.ChildRelationship> C = R.getChildRelationships();
		List<String> listChildRelationship = new List<String>();
        
        for (Schema.ChildRelationship rec : C){
    		if (rec.getRelationshipName() != null){
                if (rec.getRelationshipName() == 'RecordActionHistories') continue;
				if (rec.getRelationshipName() == 'PersonOutgoingEmailRelations') continue;
        		listChildRelationship.add(rec.getRelationshipName());       
    		}
		}
        
        Integer iCounter = 0;
        
        for (Account recAccountBatch : listAccountBatch){
            
            if (!((iRecCounter >= iRecordBatchStart) && (iRecCounter < iRecordBatchEnd+1))){
                iRecCounter++;
                continue;
            }
            
            iRecCounter++;

            Boolean bError = false;
            String sQuery = 'select Id ';
            List<String> listRelationshipName = new List<String>();
            List<String> listRelationshipNameTotal = new List<String>();
            
            sCSV = sCSV + '\'' + iRecCounter + '\',';
            sCSV = sCSV + '\'' + recAccountBatch.Id + '\',';
            sCSV = sCSV + '\'' + recAccountBatch.Name + '\',';
            sCSV = sCSV + '\'' + recAccountBatch.I4CAST_In_scope_automatic_interface__c + '\',';
            sCSV = sCSV + '\'' + recAccountBatch.RecordType.Name + '\',';
            sCSV = sCSV + '\'' + recAccountBatch.CreatedDate + '\',';
            sCSV = sCSV + '\'' + recAccountBatch.CreatedBy.Name + '\',';
            sCSV = sCSV + '\'' + recAccountBatch.Account_Country_vs__c  + '\',';
            
            for (String sChildRelationship : listChildRelationship){
                
                listRelationshipNameTotal.add(sChildRelationship);
                
                if (sChildRelationship == 'RecordActionHistories') continue;
                if (sChildRelationship == 'PersonOutgoingEmailRelations') continue;
                
                if ((iCounter < 19) && (listChildRelationship != listRelationshipNameTotal)){
                    listRelationshipName.add(sChildRelationship);
                    
                    sQuery = sQuery + ', (select Id from ' + sChildRelationship + ') ';
                    iCounter++;
                }
                else{
                    listRelationshipName.add(sChildRelationship);
                    sQuery = sQuery + ', (select Id from ' + sChildRelationship + ') ' + 'from Account where Id = \'' + recAccountBatch.Id + '\'';
                    iCounter = 0;
                    
                    List<Account> listAccount = new List<Account>();
                    
                    try{
                        listAccount = Database.query(sQuery);
                    }
                    catch (DMLException  e){
                        bError = true;
                        sErrors = sErrors + recAccountBatch.Id + '\',' + recAccountBatch.Name + '\': ' + e.getMessage() + '\n';
                        continue;
                    }
                    
                    for (Account recAccount : listAccount){

                        String sAccountJSON = JSON.serialize(recAccount);
                        
                        if (sAccountJSON.contains('Contacts')){
                            ListContactId = new List<Id>();
                            
                            for (Contact recContact : recAccount.Contacts){
                                ListContactId.add(recContact.Id);
                            }
                        }

                        for (String sRelationshipName : listRelationshipName){
                            if (sAccountJSON.contains(sRelationshipName)){
                                String sSubStringAccountJSON = sAccountJSON.substringAfter(sRelationshipName + '":{"totalSize":');
                                sSubStringAccountJSON = sSubStringAccountJSON.substringBefore(',');
                                sCSV = sCSV + '\'' +sSubStringAccountJSON + '\',';
                            }
                            else{
                                sCSV = sCSV + '\'0\',';
                            }
                    	}
                    }
                    
                    sQuery = 'select Id ';
                    listRelationshipName = new List<String>();
                }
        	}
            
            sCSV = sCSV.removeEnd(',');
			sCSV = sCSV + '\n';
        }
    }
    
    global void finish(Database.BatchableContext BC) {
       
        Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
		csvAttc.setFileName('Account_Batch_' + iBatch + '.csv');
		csvAttc.setBody(Blob.valueOf(sCSV));
        
        User recUser = [select Id, email from user where Id = : UserInfo.getUserId()];
        
        String sMessageBody = '';
        sMessageBody = sMessageBody + 'Record Batch Start: ' + iRecordBatchStartReport + '\n';
        sMessageBody = sMessageBody + 'Record Batch End: ' + iRecordBatchEnd + '\n';
        sMessageBody = sMessageBody + '\n';
        sMessageBody = sMessageBody + sErrors + '\n';
        
		Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
		email.setSubject('Batch: ' + iBatch + ' CSV Accounts and Relationships for deletion');
		email.setToAddresses( new list<string> {recUser.email} );
		email.setPlainTextBody(sMessageBody);
		email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
		Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
    }
}