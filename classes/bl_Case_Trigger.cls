//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 05-01-2016
//  Description      : APEX Class - Trigger Handler for Case	
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public class bl_Case_Trigger {

	//------------------------------------------------------------------------------------------------
	// Public method to update the notification description by adding a timestamp and username to the
	// description. This is a requirement from SAP.
	//------------------------------------------------------------------------------------------------
	public static void updateNotificationDescription(List<Case> lstTriggerNew, Map<Id, Case> mapTriggerOld)
	{
		for (Case cse: lstTriggerNew)
		{
			//check if the description has been changed.
			if (cse.Description != mapTriggerOld.get(cse.Id).Description)
			{
				//add the timestamp and comment - TODO - waiting on remarks about how to do this.
			}
		}
	}


	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// Prevent updates on restricted fields - when a restricted field which is not empty, is update it will be reverted to the old value
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public static void preventUpdateOnRestrictedFields(List<Case> lstTriggerNew, Map<Id, Case> mapTriggerOld){

		// Do not execute for System Administrator - they are allowed to update Restricted Fields
		if (!ProfileMedtronic.isSystemAdministratorProfile(UserInfo.getProfileId())){

			Set<String> setRestrictedFieldName = new Set<String>();
				setRestrictedFieldName.add('SVMX_SAP_Notification_Number__c');

			for (Case oData : lstTriggerNew){
				Case oData_Old = mapTriggerOld.get(oData.Id);

				// If a restricted field is changed while the field was not empty, revert back to the old value
				for (String tFieldName : setRestrictedFieldName){
					if ( 
						(clsUtil.isNull(oData_Old.get(tFieldName), '') != '') 
						&& (oData.get(tFieldName) != oData_Old.get(tFieldName))
					){
						oData.put(tFieldName, oData_Old.get(tFieldName));

					}
				}
			}

		}

	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// Update the field Case_Complexity__c on Case with this logic:
	//	- If Repeat Case or Standard Response are checked, then Case Complexity = 1
	//	- If Internal Data is checked, then Case Complexity = 3
	//	- If none of these are checked then the default value = 2 and the case owner can change it to a 1 or 3
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public static void setCaseComplexity(List<Case> lstTriggerNew, Map<Id, Case> mapTriggerOld){

		if (bl_Trigger_Deactivation.isTriggerDeactivated('case_setCaseComplexity')) return;

		Set<Id> setID_RecordType = new Set<Id>{clsUtil.getRecordTypeByDevName('Case', 'OMA_Neoro_MedInfo').Id, clsUtil.getRecordTypeByDevName('Case', 'OMA').Id};

		List<Case> lstCase = new List<Case>();

		for (Case oCase : lstTriggerNew){

			if (!setID_RecordType.contains(oCase.RecordTypeId)) continue;
		
			if (mapTriggerOld == null){

				// INSERT
				if (oCase.Case_Complexity__c == '2'){
					// Case Complexity is not changed by the user so we need to calculate it
					lstCase.add(oCase);
				}

			}else{

				// UPDATE
				Case oCase_Old = mapTriggerOld.get(oCase.Id);
				if (
					(oCase.Case_Complexity__c == oCase_Old.Case_Complexity__c)
					&& 
					(
						oCase.Repeat_Case__c != oCase_Old.Repeat_Case__c
						|| oCase.Standard_Response__c != oCase_Old.Standard_Response__c
						|| oCase.Internal_Data__c != oCase_Old.Internal_Data__c
					)
				){
					lstCase.add(oCase);
				}

			}
		
		}

		if (lstCase.size() == 0) return;

		for (Case oCase : lstCase){
		
			if (oCase.Repeat_Case__c || oCase.Standard_Response__c){
			
				oCase.Case_Complexity__c = '1';
			
			}else if (oCase.Internal_Data__c){

				oCase.Case_Complexity__c = '3';

			}else{

				oCase.Case_Complexity__c = '2';

			}

		}
	
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	//------------------------------------------------------------------------------------------------
	// Public Methods that will process the records
	//------------------------------------------------------------------------------------------------
	// CGEN2 - Send Notification to SAP / WebMethods
	public static void notificationSAP(List<Case> lstTriggerNew, Map<Id, Case> mapTriggerOld, String tAction){

		if (UserInfo.getProfileId() != clsUtil.getUserProfileId('SAP Interface') && UserInfo.getProfileId() != clsUtil.getUserProfileId('SMAX Interface')){

	        List<Case> lstData_Processing = new List<Case>();

			if (tAction == 'UPDATE'){
		
				for (Case oData : lstTriggerNew){

				    // Define the fields that we need to validate to indicate if the record needs further processing
	                sObject oData_Old = mapTriggerOld.get(oData.Id);

	                // Only send UPDATE Notificatio to WebMethods if the SVMX_SAP_Notification_Number__c is populated
	                if (clsUtil.isNull(oData.SVMX_SAP_Notification_Number__c, '') != ''){
						lstData_Processing.add(oData);
	                }

	            }

			}

	        if (lstData_Processing.size() > 0){
	        	bl_NotificationSAP.sendNotificationToSAP_Case(lstData_Processing, tAction);
	        }

	    }

	}
	
	public static void updateSOGCHNumber(List<Case> newRecords, Map<Id, Case> oldRecords){
		
		Map<Id, Case> updatedCases = new Map<Id, Case>();
		
		for(Case cse : newRecords){
			
			if(cse.GCH_Number__c != oldRecords.get(cse.Id).GCH_Number__c) updatedCases.put(cse.Id, cse);			
		}
		
		if(updatedCases.isEmpty() == false){
			
			List<SVMXC__Service_Order__c> sOrders = [Select Id, SVMXC__Case__c, GCH_Number__c from SVMXC__Service_Order__c where SVMXC__Case__c IN :updatedCases.keySet()];
			
			for(SVMXC__Service_Order__c sOrder : sOrders){
				
				Case sOrderCase = updatedCases.get(sOrder.SVMXC__Case__c);
				
				sOrder.GCH_Number__c = sOrderCase.GCH_Number__c;
			}
			
			// Inform the trigger logic that the Interface is running
	        bl_SVMXC_ServiceOrder_Trigger.isInterfaceRunning = true;
			
			update sOrders;
		}
	}
	
	//------------------------------------------------------------------------------------------------
	
	private static List<String> caseContactRoleFields {
    	
    	get{
    		
    		if(caseContactRoleFields == null){
	    	
	    		caseContactRoleFields = new List<String>();		        
		        caseContactRoleFields.add('RecordTypeId');
		        		      		        
	    		Map<String, Schema.SObjectField> describeMap = Schema.SObjectType.Case_Contact_Roles__c.fields.getMap();
	        	
		        for(String caseContactRoleField : describeMap.keySet()){
		        	
		        	Schema.DescribeFieldResult fDescribe = describeMap.get(caseContactRoleField).getDescribe();
		        	
		        	if(fDescribe.isCustom() && fDescribe.isCalculated() == false) caseContactRoleFields.add(caseContactRoleField);      	
		        }        	
    		}
    		
    		return caseContactRoleFields;
    	}
    	
    	set;
    }
	
	public static void cloneOMACase(List<Case> triggerNew){
		
		Set<Id> omaRTIds = new Set<Id>();
		
		for(RecordType rt : [Select Id from RecordType where SObjectType = 'Case' AND DeveloperName IN ('OMA', 'OMA_Neoro_MedInfo', 'OMA_Neuro_Physician_Scientist_Exchange')]) omaRTIds.add(rt.Id);
		
		Set<Id> clonedFromIds = new Set<Id>();
    	
    	for(Case omaCase : triggerNew){
    		    		 
    		if(omaRTIds.contains(omaCase.RecordTypeId) == true && omaCase.Cloned_From__c != null){
    		
    			clonedFromIds.add(omaCase.Cloned_From__c);
    		}
    	}
    	
    	if(clonedFromIds.size() > 0){
			
			String caseQuery = 'Select Id, ';
    		caseQuery += '(Select ' + String.join(caseContactRoleFields, ',') + ' from Case_Contact_Roles__r), ';    		
    		caseQuery += '(Select Id from Attachments where BodyLength <= 5242880 AND IsPrivate = false) ';
    		caseQuery += 'from Case where Id IN :clonedFromIds';
			
			Map<Id, Case> clonedFromMap = new Map<Id, Case>((List<Case>) Database.query(caseQuery));
			Map<Id, Attachment> cloneFromAttachmentsMap = new Map<Id, Attachment>([Select Body, ContentType, Description, IsPrivate, Name from Attachment where ParentId IN :clonedFromIds AND BodyLength <= 5242880 AND IsPrivate = false]);
			
			List<Case_Contact_Roles__c> clonedContactRoles = new List<Case_Contact_Roles__c>();
			List<Attachment> clonedAttachments = new List<Attachment>();
			
			for(Case omaCase : triggerNew){
    		    		 
    			if(omaRTIds.contains(omaCase.RecordTypeId) == true && omaCase.Cloned_From__c != null){	
					
					Case clonedFrom = clonedFromMap.get(omaCase.Cloned_From__c);
									
					//Case Contact Roles
					for(Case_Contact_Roles__c caseContactRole : clonedFrom.Case_Contact_Roles__r){
    					
    					Case_Contact_Roles__c clonedContactRole = caseContactRole.clone();
    					clonedContactRole.Case__c = omaCase.Id;
    					    					
    					clonedContactRoles.add(clonedContactRole);
    				}
					
					//Attachments
					for(Attachment caseAttachment : clonedFrom.Attachments){
	    					
	    				Attachment fullAttachment = cloneFromAttachmentsMap.get(caseAttachment.Id);
	    				Attachment clonedAttachment = fullAttachment.clone();
	    				clonedAttachment.ParentId = omaCase.Id;
	    					
	    				clonedAttachments.add(clonedAttachment);
	    			}
    			}
			}
			
			if(clonedContactRoles.size() > 0) insert clonedContactRoles;
			if(clonedAttachments.size() > 0) insert clonedAttachments;
    	}
	}
	
	public static Boolean runningSchedulingApp = false;
	
	private static Id implantScheduling_RT = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Implant_Scheduling').getRecordTypeId();
	
	public static void activitySchedulingBlockOutsideApps(List<Case> triggerNew){
					
    	for(Case record : triggerNew){
    			
    		if(record.RecordTypeId == implantScheduling_RT && runningSchedulingApp == false && ProfileMedtronic.isSystemAdministratorProfile(UserInfo.getProfileId()) == false){
    			
    			record.addError('This type of Cases can only be managed in the Activity Scheduling apps');
    		}	
    	}    	   
	}
	
	public static void activitySchedulingSetInternalOwner(List<Case> triggerNew){
		
		Set<Id> teamIds = new Set<Id>();
		
		for(Case record : triggerNew){
    			
    		if(record.RecordTypeId == implantScheduling_RT){
    			
    			if(UserInfo.getUserType() == 'PowerPartner' && record.OwnerId == UserInfo.getUserId()) teamIds.add(record.Activity_Scheduling_Team__c);    				
    		}
		}		
		
		
		if(teamIds.size() > 0){
			
			Map<Id, Implant_Scheduling_Team__c> teamMap = new Map<Id, Implant_Scheduling_Team__c>([Select Id, OwnerId from Implant_Scheduling_Team__c where Id IN :teamIds]);
			
			for(Case record : triggerNew){
    			
	    		if(record.RecordTypeId == implantScheduling_RT){
	    		
	    			if(UserInfo.getUserType() == 'PowerPartner' && record.OwnerId == UserInfo.getUserId()){
	    				
	    				Implant_Scheduling_Team__c team = teamMap.get(record.Activity_Scheduling_Team__c);
	    				record.OwnerId = team.OwnerId;
	    			}    				
	    		}
			}	
		}
	}
	
	public static void activitySchedulingSetStatus(List<Case> triggerNew, Map<Id, Case> mapOld){
		
		for(Case record : triggerNew){
    			
    		if(record.RecordTypeId == implantScheduling_RT){
    			
    			if(record.Type == 'Meeting') record.Status = 'Open';
    			else{
	    			
	    			if(record.assigned_To__c == null){
	    				
	    				//Re-opened and forgot to update status (not Rejected or Cancelled)
	    				if(record.Status == 'Assigned' || record.Status == 'Executed') record.Status = 'Open';
	    				
	    				//Re-opened
	    				if(mapOld != null && mapOld.get(record.Id).Assigned_To__c == UserInfo.getUserId() && record.Status != 'Cancelled' && record.Status != 'Rejected' ){
	    					
	    					if(record.Assignee__c != null && record.Assignee__c != UserInfo.getUserId() && String.isEmpty(record.Reason_for_Reject_Cancel_Assigned_to__c)){
	    						
	    						if(UserInfo.getUserType() == 'PowerPartner') throw new IllegalArgumentException('As an Agent you cannot reject an assignment made by another user.');
	    						else throw new IllegalArgumentException('You need to provide a reason for the rejection of a Request assigned to you.');	
	    					}    					
	    				}    				
	    				
	    			}else{
	    				
	    				//Cancelled or Rejected but forgot to remove the assignment
	    				if(record.Status == 'Cancelled' || record.Status == 'Rejected'){
	    					
	    					record.assigned_To__c = null;
	    					record.assignee__c = null;
	    				
	    				//Just Assigned or Re-assigned
	    				}else if(mapOld == null || mapOld.get(record.Id).Assigned_To__c != record.assigned_To__c){
	    					    					
	    					record.assignee__c = UserInfo.getUserId();
	    					record.Status = 'Assigned';
	    				}
	    			}    
    			}			
    		}	
    	}
	}
	
	private static User currentUser{
		
		get{
			
			if(currentUser == null || currentUser.Id != UserInfo.getUserId()) currentUser = [Select Id, ContactId from User where Id = :UserInfo.getUserId()];
			
			return currentUser;
		}
		
		set;
	}
	
	public static void activitySchedulingRemoveAgentContact(List<Case> triggerNew){
		
		List<Case> toUpdate = new List<Case>();
		
		for(Case record : triggerNew){
    			
    		if(record.RecordTypeId == implantScheduling_RT && UserInfo.getUserType() == 'PowerPartner' && record.ContactId == currentUser.ContactId){
    			
    			Case caseUpdate = new Case();
    			caseUpdate.Id = record.Id;
    			caseUpdate.ContactId = null;
    			toUpdate.add(caseUpdate);	
    		}    		
		}
		
		if(toUpdate.size() > 0) update toUpdate;
	}
	
	public static void activitySchedulingShareWithTeam(List<Case> triggerNew, Map<Id, Case> mapOld){
		
		Set<Id> teamIds = new Set<Id>();
		Set<Id> groupIds = new Set<Id>();
					
    	for(Case record : triggerNew){
    			
    		if(record.RecordTypeId == implantScheduling_RT){
    			
    			teamIds.add(record.Activity_Scheduling_Team__c);
    			if(record.Assigned_To__c != null) groupIds.add(record.Assigned_To__c);
    			
    			if(mapOld != null && mapOld.get(record.Id).Activity_Scheduling_Team__c != null) teamIds.add(mapOld.get(record.Id).Activity_Scheduling_Team__c);
    			if(mapOld != null && mapOld.get(record.Id).Assigned_To__c != null) groupIds.add(mapOld.get(record.Id).Assigned_To__c);
    		}	
    	}
    	
    	if(teamIds.size() == 0) return;//If there are not teams, then there are no Activity Scheduling Cases in scope
    	
    	Map<Id, Implant_Scheduling_Team__c> teams = new Map<Id, Implant_Scheduling_Team__c>();    	
    	
    	for(Implant_Scheduling_Team__c team : [Select Id, Team_Group__c, Team_Group_Externals__c from Implant_Scheduling_Team__c where Id IN :teamIds]){
    		
    		teams.put(team.Id, team);
    		groupIds.add(team.Team_Group__c);
    		groupIds.add(team.Team_Group_Externals__c);
    	}
    	
    	Map<String, CaseShare> existingShares = new Map<String, CaseShare>();
    	
    	for(CaseShare share : [Select CaseId, UserOrGroupId, RowCause from CaseShare where CaseId IN :triggerNew AND UserOrGroupId IN :groupIds AND RowCause IN ('Manual', 'Owner')]){
    		
    		existingShares.put(share.CaseId + ':' + share.UserOrGroupId, share);    		
    	}
    	    	
    	List<CaseShare> toInsert = new List<CaseShare>();
    	List<CaseShare> toDelete = new List<CaseShare>();
    	    	
    	for(Case record : triggerNew){
    			
    		if(record.RecordTypeId == implantScheduling_RT){ 
    			
    			//Internals
    			String key = record.Id + ':' + teams.get(record.Activity_Scheduling_Team__c).Team_Group__c;
    	  
    			if(existingShares.containsKey(key) == false){
    				
    				CaseShare share = new CaseShare();
    				share.CaseId = record.Id;
    				share.UserOrGroupId = teams.get(record.Activity_Scheduling_Team__c).Team_Group__c;
    				share.RowCause = 'Manual';
    				share.CaseAccessLevel = 'Edit';
    				
    				toInsert.add(share);
    			}
    			
    			//Assigned
    			if(record.Assigned_To__c != null  && record.Type != 'Meeting' && record.Is_Recurring__c == false){
	    		
	    			String assignedKey = record.Id + ':' + record.Assigned_To__c;
	    	  
	    			if(existingShares.containsKey(assignedKey) == false){
	    				
	    				CaseShare share = new CaseShare();
	    				share.CaseId = record.Id;
	    				share.UserOrGroupId = record.Assigned_To__c;
	    				share.RowCause = 'Manual';
	    				share.CaseAccessLevel = 'Edit';
	    				
	    				toInsert.add(share);
	    			}
    			}
    			
    			//Externals    			
    			String externalsKey = record.Id + ':' + teams.get(record.Activity_Scheduling_Team__c).Team_Group_Externals__c;
    			    			
    			if(record.Status == 'Open' && record.Type != 'Meeting' && record.Is_Recurring__c == false){    			
	    			
	    			if(existingShares.containsKey(externalsKey) == false){
	    				
	    				CaseShare share = new CaseShare();
	    				share.CaseId = record.Id;
	    				share.UserOrGroupId = teams.get(record.Activity_Scheduling_Team__c).Team_Group_Externals__c;
	    				share.RowCause = 'Manual';
	    				share.CaseAccessLevel = 'Edit';
	    				
	    				toInsert.add(share);
	    			}
	    			
    			}else{
    			    				
    			    if(existingShares.containsKey(externalsKey)) toDelete.add(existingShares.get(externalsKey));
    			}				
    			
    			//Change Team       			
    			if(mapOld !=null && mapOld.get(record.Id).Activity_Scheduling_Team__c != null && record.Activity_Scheduling_Team__c != mapOld.get(record.Id).Activity_Scheduling_Team__c){
    				
    				String oldKey = record.Id + ':' + teams.get(mapOld.get(record.Id).Activity_Scheduling_Team__c).Team_Group__c;    	  
    				if(existingShares.containsKey(oldKey)) toDelete.add(existingShares.get(oldKey));
    				
    				String externalsOldKey = record.Id + ':' + teams.get(mapOld.get(record.Id).Activity_Scheduling_Team__c).Team_Group_Externals__c;    	  
    				if(existingShares.containsKey(externalsOldKey)) toDelete.add(existingShares.get(externalsOldKey));    				
    			}
    			
    			//Change Assigned       			
    			if(mapOld !=null && mapOld.get(record.Id).Assigned_To__c != null && record.Assigned_To__c != mapOld.get(record.Id).Assigned_To__c){
    				
    				String oldAssignedKey = record.Id + ':' + mapOld.get(record.Id).Assigned_To__c;    	  
    				if(existingShares.containsKey(oldAssignedKey) && existingShares.get(oldAssignedKey).RowCause != 'Owner') toDelete.add(existingShares.get(oldAssignedKey));    				    				
    			}
    		}
    	}  	
    	
    	if(toInsert.size() > 0) insert toInsert;
    	if(toDelete.size() > 0) delete toDelete;   
	}
	
	public static void activitySchedulingManageEvent(List<Case> triggerNew, Map<Id, Case> mapOld){
		
		List<Case> implantSupportCases = new List<Case>();
		Set<Id> accountIds = new Set<Id>();
				
		for(Case record : triggerNew){
    			
    		if(record.RecordTypeId == implantScheduling_RT && record.Is_Recurring__c == false){
    			
    			if(record.Assigned_To__c != null){
    				
    				Case oldRecord = mapOld != null ? mapOld.get(record.Id) : null;
    				
    				if(	oldRecord == null ||
    					record.Assigned_To__c != oldRecord.Assigned_To__c ||
    					record.ContactId != oldRecord.ContactId ||
    					record.Start_of_Procedure__c != oldRecord.Start_of_Procedure__c ||
    					record.Procedure_Duration_Implants__c != oldRecord.Procedure_Duration_Implants__c ||
    					record.End_of_Procedure_DateTime__c != oldRecord.End_of_Procedure_DateTime__c ||
    					record.AccountId != oldRecord.AccountId ||
    					record.Subject != oldRecord.Subject ||
    					record.Description != oldRecord.Description ||
    					record.Status != oldRecord.Status || 
    					record.Procedure__c != oldRecord.Procedure__c){
    						
    						implantSupportCases.add(record);
    						if(record.AccountId != null) accountIds.add(record.AccountId);
    				}
    				
    				
    			}else if(mapOld != null && mapOld.get(record.Id).Assigned_To__c != null) implantSupportCases.add(record);
    		}	
    	} 
    	
    	if(implantSupportCases.size() > 0){
	    	
	    	Map<Id, Account> accounts = new Map<Id, Account>();
	    	if(accountIds.size() > 0) accounts = new Map<Id, Account>([Select Id, Name, BillingCity from Account where Id IN: accountIds]);
	    	
	    	Map<Id, Event> caseEvents = new Map<Id, Event>();
	    	
	    	for(Event event : [select Id, WhatId, Type from Event where WhatId in :implantSupportCases AND isChild = false AND RecordTypeId = :bl_Event_Trigger.activityScheduling_RT ALL ROWS]){
	    		
	    		caseEvents.put(event.WhatId, event);
	    	}
	    	
	    	List<Event> toUpsert = new List<Event>();
	    	List<Event> toDelete = new List<Event>();
	    	
	    	for(Case record : implantSupportCases){
	    		
	    		Event event = caseEvents.get(record.Id);
	    		
	    		if(record.Assigned_To__c != null){
	    			
	    			if(event == null) event = new Event(WhatId = record.Id);
	    			
	    			Account account = accounts.get(record.AccountId);
	    			
	    			event.OwnerId = record.Assigned_To__c;		            
		            event.WhoId = record.ContactId;
		            event.Type = (record.Type == 'Implant Support Request' ? 'Implant Support' : (record.Type == 'Service Request' ? 'Service call' : 'Meeting'));
		            event.StartDateTime = record.Start_of_Procedure__c;
		            event.EndDateTime = record.End_of_Procedure__c;
		            if(account != null){
		            	event.Location = account.Name;
		            	if(account.BillingCity != null)  event.Location += ', ' + account.BillingCity;
		            }
		            event.Subject = record.Subject;
		            event.Description = record.Description;
		            event.ShowAs = 'Busy';
		            event.RecordTypeId = bl_Event_Trigger.activityScheduling_RT;
		            
		            toUpsert.add(event);
	    			
	    		}else{
	    			
	    			if(event != null) toDelete.add(event);
	    		}	    		
	    	}
	    	
	    	bl_Event_Trigger.runningActivitySchedulingEventSync = true;
	    	
		    if(toUpsert.size() > 0) upsert toUpsert;		    
		    if(toDelete.size() > 0) delete toDelete;
		    
		    bl_Event_Trigger.runningActivitySchedulingEventSync = false;
		    
		    //See if already an event created for it, if so send update notification
		    //TODO check with John Update event if linked or send notification of change
		    //bl_Activity.changeNotification(eventsLinkedToCase, 'Update');
	    }    	
	}
	
	private static Id rejectionTemplateId{
		
		get{
			
			if(rejectionTemplateId == null) rejectionTemplateId = [select Id from EmailTemplate where DeveloperName='Activity_Scheduling_Notify_Assignment_Rejection'].Id;
			
			return rejectionTemplateId;
		}
		
		set;
	}
	
	public static void activitySchedulingNotifyOnRejection(List<Case> triggerNew, Map<Id, Case> oldMap){
		
		List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
		
		for(Case record : triggerNew){
			
			if(record.RecordTypeId == implantScheduling_RT && record.Is_Recurring__c == false){
				
				Case oldRecord = oldMap.get(record.Id);
			
				if(record.Assigned_To__c == null && oldRecord.Assigned_To__c != null && record.Status == 'Open' && record.Assignee__c != UserInfo.getUserId() && record.Type != 'Meeting'){
					
					
					Messaging.SingleEmailMessage rejectionEmail = Messaging.renderStoredEmailTemplate(rejectionTemplateId, record.Assignee__c, record.Id); 					          
		            rejectionEmail.setUseSignature(false);
		            rejectionEmail.setSaveAsActivity(false);		            			
					emails.add(rejectionEmail);					
				}
			}			
		}
		
		if(emails.size() > 0) Messaging.sendEmail(emails);
	}
	
	@TestVisible
	private static Set<Id> alreadyProcessed = new Set<Id>();
	
	public static void activityManageRecurrency(List<Case> triggerNew, Map<Id, Case> oldMap){
		
		List<Case> recurringCases = new List<Case>();
		
		for(Case record : triggerNew){
			
			if(alreadyProcessed.add(record.Id) == true && record.RecordTypeId == implantScheduling_RT && record.Is_Recurring__c == true && (oldMap == null || runningSchedulingApp)){
				
				recurringCases.add(record);
			}						
		}
		
		if(recurringCases.isEmpty()) return;
		
		DateTime now = DateTime.now();
						
		if(oldMap != null) delete [Select Id from Case where ParentId IN :recurringCases AND Start_of_Procedure__c > :now];		
		
		List<Case> recurrenceCases = new List<Case>();
		
		for(Case record : recurringCases){
		
			Integer recurringNDays = 7 * Integer.valueOf(record.Recurring_N_Weeks__c);
			Set<String> recurringDays = new Set<String>(record.Recurring_Week_Days__c.split(';'));
			
			Integer durationInSeconds;			
			if(record.End_of_Procedure_DateTime__c != null) durationInSeconds = Integer.valueOf((record.End_of_Procedure_DateTime__c.getTime() - record.Start_of_Procedure__c.getTime()) / 1000);
			
			Integer instancesNumber = 0;
			
			for( Date recDate = record.Start_of_Procedure__c.date().toStartOfWeek(); recDate <= record.Recurring_End_Date__c; recDate = recDate.addDays(recurringNDays)){
				
				DateTime recDateTime = DateTime.newInstance(recDate, record.Start_of_Procedure__c.time());
				
				for(Integer i = 1; i <= 7; i++){
					
					if(recDateTime.date() >= record.Start_of_Procedure__c.date() && recDateTime.date() <= record.Recurring_End_Date__c && (oldMap == null || recDateTime > now)){
						
						if(recurringDays.contains(recDateTime.format('u'))){
														
							Case recurrenceCase = new Case();
							recurrenceCase.RecordTypeId = implantScheduling_RT;
							recurrenceCase.ParentId = record.Id;
							recurrenceCase.Start_of_Procedure__c = recDateTime;
							recurrenceCase.Procedure_Duration_Implants__c = record.Procedure_Duration_Implants__c;
							if(record.End_of_Procedure_DateTime__c != null) recurrenceCase.End_of_Procedure_DateTime__c = recDateTime.addSeconds(durationInSeconds); 
			
							recurrenceCase.Subject = record.Subject;
							recurrenceCase.Type = record.Type;
							recurrenceCase.AccountId = record.AccountId;
							recurrenceCase.ContactId = record.ContactId;
							recurrenceCase.Date_Received_Date__c = record.Date_Received_Date__c;
							recurrenceCase.Status = record.Status;
							recurrenceCase.Origin = record.Origin;
							recurrenceCase.Procedure__c = record.Procedure__c;
							recurrenceCase.Activity_Type_Picklist__c = record.Activity_Type_Picklist__c;
							recurrenceCase.Activity_Scheduling_Team__c = record.Activity_Scheduling_Team__c;
							recurrenceCase.Assigned_To__c = record.Assigned_To__c;
							recurrenceCase.Product_Group__c = record.Product_Group__c;
							recurrenceCase.Venue__c = record.Venue__c;
							recurrenceCase.Experience_of_the_Implanter__c = record.Experience_of_the_Implanter__c;
							recurrenceCase.Duration_of_the_Past_Procedure__c = record.Duration_of_the_Past_Procedure__c;
							recurrenceCase.Duration_of_Waiting_Time__c = record.Duration_of_Waiting_Time__c;
							recurrenceCase.Description = record.Description;
							recurrenceCase.Reason_for_Rejection_Cancellation__c = record.Reason_for_Rejection_Cancellation__c;
							recurrenceCase.Reason_for_Reject_Cancel_Assigned_to__c = record.Reason_for_Reject_Cancel_Assigned_to__c;
							
							//Create Case instance
							recurrenceCases.add(recurrenceCase);
							
							instancesNumber++;
						}
					}
					
					recDateTime = recDateTime.addDays(1);	
				}				
			}
			
			if(instancesNumber > 150) throw new IllegalArgumentException('The selected End Date is too far in the future. Please select a closer date.');
		}
						
		insert recurrenceCases;		
		
		if(oldMap != null){
						
			Map<Id, Case> recurringCasesAttendees = new Map<Id, Case>([Select Id, (Select Attendee__c from Activity_Scheduling_Attendees__r) from Case where Id IN: recurringCases]);
			
			List<Activity_Scheduling_Attendee__c> recurrenceAttendees = new List<Activity_Scheduling_Attendee__c>();
		
			for(Case record : recurrenceCases){
				
				Case parentCase = recurringCasesAttendees.get(record.ParentId);
				
				for(Activity_Scheduling_Attendee__c attendee : parentCase.Activity_Scheduling_Attendees__r){
					
					if(attendee.Attendee__c != record.Assigned_To__c){
						
						Activity_Scheduling_Attendee__c recurrenceAttendee = new Activity_Scheduling_Attendee__c();
						recurrenceAttendee.Case__c = record.Id;
						recurrenceAttendee.Attendee__c = attendee.Attendee__c;
						
						recurrenceAttendees.add(recurrenceAttendee);
					}
				}
			}
			
			if(recurrenceAttendees.isEmpty() == false) insert recurrenceAttendees;
		}
	}
	
	public static void activityDeleteRecurrency(List<Case> triggerOld){
		
		List<Case> recurringCases = new List<Case>();
		
		for(Case record : triggerOld){
			
			if(record.RecordTypeId == implantScheduling_RT && record.Is_Recurring__c == true) recurringCases.add(record);
		}
		
		if(recurringCases.isEmpty()) return;
		
		DateTime now = DateTime.now();
						
		delete [Select Id from Case where ParentId IN :recurringCases AND Start_of_Procedure__c > :now];	
	}
}
//--------------------------------------------------------------------------------------------------------------------