@isTest
private class TEST_triggerMasterDetetionPrevention {

    static TestMethod void triggerMasterDetetionPrevention(){  
    
		User oUser_Admin = clsTestData_User.createUser_SystemAdministratorMDT('tstadm1', true);
		
		System.runAs(oUser_Admin){

			//Insert Company
			Company__c cmpny = new Company__c();
				cmpny.name='TestMedtronic';
				cmpny.CurrencyIsoCode = 'EUR';
				cmpny.Current_day_in_Q1__c=56;
				cmpny.Current_day_in_Q2__c=34; 
				cmpny.Current_day_in_Q3__c=5; 
				cmpny.Current_day_in_Q4__c= 0;   
				cmpny.Current_day_in_year__c =200;
				cmpny.Days_in_Q1__c=56;  
				cmpny.Days_in_Q2__c=34;
				cmpny.Days_in_Q3__c=13;
				cmpny.Days_in_Q4__c=22;
				cmpny.Days_in_year__c =250;
				cmpny.Company_Code_Text__c = 'T26';
			insert cmpny;
    
			Call_Activity_Type__c CAT=new Call_Activity_Type__c();
				CAT.Name='Test Activity';
				CAT.Active__c=true;
				CAT.Company_ID__c = cmpny.Id;
			insert CAT;   

			Call_Category__c catt= new  Call_Category__c();
				catt.Name = 'cat111';
				catt.Company_ID__c = cmpny.id;
			insert catt;
    
			Call_Topic_Settings__c CTS=new  Call_Topic_Settings__c();
				CTS.Business_Unit__c=SharedMethods.getBULookup();
				CTS.Call_Activity_Type__c=CAT.Id;
				CTS.Call_Category__c = catt.id;
				CTS.Region_vs__c = 'Europe';
			insert CTS;
     
			Subject__c s=new    Subject__c();
				s.Subject__c='Test Subject';
				s.Company_ID__c = cmpny.id; 
			Insert s;
     
			Call_topic_Setting_Details__c CTSD = new Call_topic_Setting_Details__c();
				CTSD.Call_Topic_Setting__c=CTS.ID;
				CTSD.subject__C=s.Id;
			insert CTSD;
            
			Update CTSD;
		
		}

	}
    
	
	static TestMethod void triggerBeforeDelete(){  

		User oUser_Admin = clsTestData_User.createUser_SystemAdministratorMDT('tstadm1', true);
		
		System.runAs(oUser_Admin){

			Account acc1 = new Account();
				acc1.Name = 'TEST_controllerDIB_Status Test Coverage Account 1456 ' ; 
			insert acc1 ;       
        
			Contact cont1 = new Contact();
				cont1.LastName = 'TEST_Contact' ;
				cont1.FirstName = 'test';
				cont1.AccountId = acc1.id; 
				cont1.Contact_Department__c = 'Diabetes Adult'; 
				cont1.Contact_Primary_Specialty__c = 'ENT';
				cont1.Affiliation_To_Account__c = 'Employee';
				cont1.Primary_Job_Title_vs__c = 'Manager';
				cont1.Contact_Gender__c = 'Male'; 
			insert cont1 ;      

			Test.startTest();        

				List<Affiliation__c> lstAff = [Select i.id from Affiliation__c i where i.Affiliation_From_Contact__c =:cont1.Id or i.Affiliation_To_Contact__c =: cont1.Id];       
				Delete lstAff;
                
				delete cont1;
				UnDelete cont1;    

			Test.stopTest();        

		}

    }     

}