public class bl_Contract_Account {
	
	public static void rollupAccountSAPIds(List<Contract_Account__c> triggerNew){
		 
		 Set<Id> contractIds = new Set<Id>();
		 
		 for(Contract_Account__c conAcc : triggerNew){
		 	
		 	contractIds.add(conAcc.Contract_xBU__c);
		 }
		 
		 List<Contract_xBU__c> contracts = [Select Id, (Select Account__r.SAP_Id__c from Contract_Accounts__r where Account__r.SAP_Id__c != null) from Contract_xBU__c where Id IN :contractIds];
		 
		 for(Contract_xBU__c contract : contracts){
		 	
		 	Set<String> accSAPIds = new Set<String>();
		 	
		 	for(Contract_Account__c conAcc : contract.Contract_Accounts__r){
		 		
		 		accSAPIds.add(conAcc.Account__r.SAP_Id__c);
		 	}
		 	
		 	contract.Additional_Account_SAP_Id_s_lt__c = String.join(new List<String>(accSAPIds), ', ');
		 }
		 
		 update contracts;
	}    
}