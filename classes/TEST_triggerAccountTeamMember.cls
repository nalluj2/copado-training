/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TEST_triggerAccountTeamMember {

   List<Account> sfaAccounts;
   List<Account_Team_Member__c> myAccountTeamMembers;
   //List<Employee__c> myEmployees;
   
    public void init() {
        System.debug(' ################## ' + 'TEST_triggerAccountTeamMember_Init' + ' ################');
    }
    
       public void createRecords() {
        
            Business_Unit__c myBU = new Business_Unit__c(Name = 'myBU');
            Therapy__c myTherapy = new Therapy__c(Active__c=true, Business_Unit__c=myBU.Id );
            // Sales Force Accounts 
            
            /*myEmployees = new List<Employee__c>();
            
            Employee__c myEmployee;
            
            myEmployee = new Employee__c();
            myEmployee.SAP_Employee_ID__c = String.valueOf(0);
            myEmployee.Name = 'Me';
            myEmployee.First_Name__c = 'Me';
            myEmployees.add(myEmployee);

            myEmployee = new Employee__c();
            myEmployee.SAP_Employee_ID__c = String.valueOf(1);
            myEmployee.Name = 'Me';
            myEmployee.First_Name__c = 'Me';
            myEmployees.add(myEmployee);

            myEmployee = new Employee__c();
            myEmployee.SAP_Employee_ID__c = String.valueOf(2);
            myEmployee.Name = 'Me';
            myEmployee.First_Name__c = 'Me';
            myEmployees.add(myEmployee);

            myEmployee = new Employee__c();
            myEmployee.SAP_Employee_ID__c = String.valueOf(3);
            myEmployee.Name = 'Me';
            myEmployee.First_Name__c = 'Me';
            myEmployees.add(myEmployee);

            myEmployee = new Employee__c();
            myEmployee.SAP_Employee_ID__c = String.valueOf(4);
            myEmployee.Name = 'Me';
            myEmployee.First_Name__c = 'Me';
            myEmployees.add(myEmployee);

            myEmployee = new Employee__c();
            //myEmployee.SAP_Employee_ID__c = String.valueOf(6);
            myEmployee.Name = 'Me';
            myEmployee.First_Name__c = 'Me';
            myEmployees.add(myEmployee);

            insert myEmployees;*/
            
            
            sfaAccounts = new List<Account>{} ;
        
            // create Sales Force Accounts : 
            for (Integer i = 0; i < 10 ; i++){
                Account acc1 = new Account();
                acc1.Name = 'Aatest Coverage Sales Force Account ' + i ;
                acc1.isSales_Force_Account__c = true ; 
                acc1.Account_Active__c = true;
                sfaAccounts.add(acc1) ;
            }
            insert sfaAccounts ;
            
            Integer i=1;            
            
            List<User> users = [Select Id from User where isActive = true LIMIT 2];
            
            myAccountTeamMembers = new List<Account_Team_Member__c>();
            Account_Team_Member__c myAccountTeamMember;
            
            myAccountTeamMember = new Account_Team_Member__c();
            myAccountTeamMember.Account__c = sfaAccounts.get(1).Id;            
            myAccountTeamMember.User__c = users[0].Id;
            //myAccountTeamMember.Employee__c = myEmployees.get(0).Id;
            //myAccountTeamMember.Business_Unit__c = myBU.Id;
            //myAccountTeamMember.Contact__c
            //myAccountTeamMember.Primary__c
            //myAccountTeamMember.Role__c
            //myAccountTeamMember.SAP_Employee_ID__c = String.valueOf(1);
            //myAccountTeamMember.Therapy__c
            myAccountTeamMembers.add(myAccountTeamMember);
			
			myAccountTeamMember = new Account_Team_Member__c();
            myAccountTeamMember.Account__c = sfaAccounts.get(1).Id;
            myAccountTeamMember.User__c = users[1].Id;
            //myAccountTeamMember.Employee__c = myEmployees.get(5).Id;            
            //myAccountTeamMember.Business_Unit__c = myBU.Id;
            //myAccountTeamMember.Contact__c
            //myAccountTeamMember.Primary__c
            //myAccountTeamMember.Role__c
            //myAccountTeamMember.SAP_Employee_ID__c = String.valueOf(6);
            //myAccountTeamMember.Therapy__c
            myAccountTeamMembers.add(myAccountTeamMember);
			
            myAccountTeamMember = new Account_Team_Member__c();
            myAccountTeamMember.Account__c = sfaAccounts.get(2).Id;
            myAccountTeamMember.User__c = users[0].Id;
            //myAccountTeamMember.Employee__c = myEmployees.get(1).Id;
            //myAccountTeamMember.Business_Unit__c = myBU.Id;
            //myAccountTeamMember.Contact__c
            //myAccountTeamMember.Primary__c
            //myAccountTeamMember.Role__c
            //myAccountTeamMember.SAP_Employee_ID__c = String.valueOf(2);
            //myAccountTeamMember.Therapy__c
            myAccountTeamMembers.add(myAccountTeamMember);

            myAccountTeamMember = new Account_Team_Member__c();
            myAccountTeamMember.Account__c = sfaAccounts.get(2).Id;
            myAccountTeamMember.User__c = users[1].Id;
            //myAccountTeamMember.Employee__c = myEmployees.get(2).Id;
            //myAccountTeamMember.Business_Unit__c = myBU.Id;
            //myAccountTeamMember.Contact__c
            //myAccountTeamMember.Primary__c
            //myAccountTeamMember.Role__c
            //myAccountTeamMember.SAP_Employee_ID__c = String.valueOf(3);
            //myAccountTeamMember.Therapy__c
            myAccountTeamMembers.add(myAccountTeamMember);

            myAccountTeamMember = new Account_Team_Member__c();
            myAccountTeamMember.Account__c = sfaAccounts.get(3).Id;
            myAccountTeamMember.User__c = users[0].Id;
            //myAccountTeamMember.Employee__c = myEmployees.get(3).Id;
            //myAccountTeamMember.Business_Unit__c = myBU.Id;
            //myAccountTeamMember.Contact__c
            //myAccountTeamMember.Primary__c
            //myAccountTeamMember.Role__c
            //myAccountTeamMember.SAP_Employee_ID__c = String.valueOf(4);
            //myAccountTeamMember.Therapy__c
            myAccountTeamMembers.add(myAccountTeamMember);

            myAccountTeamMember = new Account_Team_Member__c();
            myAccountTeamMember.Account__c = sfaAccounts.get(3).Id;
            myAccountTeamMember.User__c = users[1].Id;
           // myAccountTeamMember.Employee__c = myEmployees.get(4).Id;
            //myAccountTeamMember.Business_Unit__c = myBU.Id;
            //myAccountTeamMember.Contact__c
            //myAccountTeamMember.Primary__c
            //myAccountTeamMember.Role__c
            //myAccountTeamMember.SAP_Employee_ID__c = String.valueOf(5);
            //myAccountTeamMember.Therapy__c
            myAccountTeamMembers.add(myAccountTeamMember);
            
            
                
            System.debug(' ################## ' + 'END TEST_triggerAccountTeamMember_createRecords' + ' ################');
       }    
    
    public void doTests() {

        insert myAccountTeamMembers;
        
        // Check if the newly created Account have the correct value of Account_Team_Members_SAPId__c           
        List<Account> sfaAccountsTempList= new List<Account>{} ;
        sfaAccountsTempList = [SELECT Id from Account where Id IN :sfaAccounts];
        
        System.debug(' ################## ' + 'TEST_triggerAccountTeamMember_doTests sfaAccountsTempList.size' + sfaAccountsTempList.size() + ' ################');
        
        for (Account sfaAccountTemp: sfaAccountsTempList) {
            if (sfaAccountTemp.Id == sfaAccounts.get(1).Id) {
                //System.debug(' ################## ' + 'TEST_triggerAccountTeamMember_doTests sfaAccountTemp.Id: ' + sfaAccountTemp.Id + ' ################');
                //String concatenatedAccountTeamMembers_EmpSAPId = sfaAccountTemp.Account_Team_Members_SAPId__c;
                //String concatenatedAccountTeamMembers_EmpId = sfaAccountTemp.Account_Team_Members_EmpId__c;
                //System.debug(' ################## ' + 'TEST_triggerAccountTeamMember_doTests concatenatedAccountTeamMembers: ' + concatenatedAccountTeamMembers_EmpSAPId + ' ################');
                //String value2Check_EmpSAPId = myEmployees.get(0).SAP_Employee_ID__c + ';';
                //String value2Check_EmpId =  String.valueOf(myEmployees.get(0).Id).substring(0,15) + ';' + String.valueOf(myEmployees.get(5).Id).substring(0,15) + ';';
                //System.assertEquals(concatenatedAccountTeamMembers_EmpSAPId,value2Check_EmpSAPId);
                //System.assertEquals(concatenatedAccountTeamMembers_EmpId,value2Check_EmpId);
            }
            if (sfaAccountTemp.Id == sfaAccounts.get(2).Id) {
               // System.debug(' ################## ' + 'TEST_triggerAccountTeamMember_doTests sfaAccountTemp.Id: ' + sfaAccountTemp.Id + ' ################');
                //String concatenatedAccountTeamMembers_EmpSAPId = sfaAccountTemp.Account_Team_Members_SAPId__c;
                //String concatenatedAccountTeamMembers_EmpId = sfaAccountTemp.Account_Team_Members_EmpId__c;
               // System.debug(' ################## ' + 'TEST_triggerAccountTeamMember_doTests concatenatedAccountTeamMembers: ' + concatenatedAccountTeamMembers_EmpSAPId + ' ################');
                //String value2Check_EmpSAPId = myEmployees.get(1).SAP_Employee_ID__c + ';' + myEmployees.get(2).SAP_Employee_ID__c + ';';
               // String value2Check_EmpId =  String.valueOf(myEmployees.get(1).Id).substring(0,15) + ';' + String.valueOf(myEmployees.get(2).Id).substring(0,15) + ';';
               // System.assertEquals(concatenatedAccountTeamMembers_EmpSAPId,value2Check_EmpSAPId);
               // System.assertEquals(concatenatedAccountTeamMembers_EmpId,value2Check_EmpId);
            }
        }
        
        delete myAccountTeamMembers;
        
        sfaAccountsTempList = [SELECT ID from Account where Id IN :sfaAccounts];
        
        for (Account sfaAccountTemp: sfaAccountsTempList) {
            if (sfaAccountTemp.Id == sfaAccounts.get(1).Id) {
                //String concatenatedAccountTeamMembers = sfaAccountTemp.Account_Team_Members_SAPId__c;
                //System.debug(' ################## ' + 'TEST_triggerAccountTeamMember_doTests concatenatedAccountTeamMembers: ' + concatenatedAccountTeamMembers + ' ################');
                //if (concatenatedAccountTeamMembers == null || concatenatedAccountTeamMembers == '')
                //    System.assertEquals(1,1);
                //else
                //    System.assertEquals(1,0);
            }
            if (sfaAccountTemp.Id == sfaAccounts.get(2).Id) {
                //String concatenatedAccountTeamMembers = sfaAccountTemp.Account_Team_Members_SAPId__c;
                //System.debug(' ################## ' + 'TEST_triggerAccountTeamMember_doTests concatenatedAccountTeamMembers: ' + concatenatedAccountTeamMembers + ' ################');
                //if (concatenatedAccountTeamMembers == null || concatenatedAccountTeamMembers == '')
                //    System.assertEquals(1,1);
                //else
                //    System.assertEquals(1,0);
            }               
        }
        
        //delete myEmployees;

    }
   
   static testMethod void myUnitTest() {
        System.debug(' ################## ' + 'BEGIN TEST_triggerAccountTeamMember' + ' ################');
                /*
                User myUser = new User(alias = 'DsUser', email='dsusertest@absi.be', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                localesidkey='en_US', timezonesidkey='America/Los_Angeles', username='dsusertest@absi.be');
                */
            String myUserId = System.Userinfo.getUserId();
            User myUser = [select Id from User where Id =: myUserId];
            System.runAs(myUser) {
            TEST_triggerAccountTeamMember myTestAccountTeamMember = new TEST_triggerAccountTeamMember();
            myTestAccountTeamMember.init();
            myTestAccountTeamMember.createRecords();
            myTestAccountTeamMember.doTests();
        }
        
        System.debug(' ################## ' + 'END TEST_triggerAccountTeamMember' + ' ################');
    }       
    
}