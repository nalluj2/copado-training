@isTest 
private class TEST_bl_OpportunityAsset_Trigger {

	@isTest static void test_updateDIENCode() {

		//------------------------------------------------------
        // Create Test Data
		//------------------------------------------------------
		// Create Test Data - Mapping_Asset_ServiceLevel_DIENCode__c
		Mapping_Asset_ServiceLevel_DIENCode__c oMappingDIENCode1 = new Mapping_Asset_ServiceLevel_DIENCode__c();
			oMappingDIENCode1.CFN_Code__c = 'AZERTY';
			oMappingDIENCode1.Service_Level__c = 'GOLD';
			oMappingDIENCode1.DIEN_Code__c = 'AZERTYGOLD';
		Mapping_Asset_ServiceLevel_DIENCode__c oMappingDIENCode2 = new Mapping_Asset_ServiceLevel_DIENCode__c();
			oMappingDIENCode2.CFN_Code__c = 'AZERTY';
			oMappingDIENCode2.Service_Level__c = 'SILVER';
			oMappingDIENCode2.DIEN_Code__c = 'AZERTYSILVER';
		insert new List<Mapping_Asset_ServiceLevel_DIENCode__c>{oMappingDIENCode1, oMappingDIENCode2};

		// Custom Setting
		clsTestData_CustomSetting.createCustomSettingData();

		// Product
		List<Product2> lstProduct = new List<Product2>();
		Product2 oProduct1 = new Product2();
			oProduct1.Name = 'PoleStar1';
			oProduct1.ProductCode = '000999';
			oProduct1.isActive = true;					
		lstProduct.add(oProduct1);
		Product2 oProduct2 = new Product2();
			oProduct2.Name = 'PoleStar';
			oProduct2.ProductCode = '009999';
			oProduct2.isActive = true;					
		lstProduct.add(oProduct2);
		insert lstProduct;

		// Account
		clsTestData_Account.iRecord_Account = 1;
		clsTestData_Account.idRecordType_Account = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non-SAP Account').getRecordTypeId();
		List<Account> lstAccount = clsTestData_Account.createAccount(false);
		for (Account oAccount : lstAccount){
			oAccount.AccountNumber = '0000000';
			oAccount.Name = 'Test Account';
			oAccount.Phone = '+123456789';
			oAccount.BillingCity = 'TEST CITY';
			oAccount.BillingCountry = 'BELGIUM';
			oAccount.BillingState = 'TEST STATE';
			oAccount.BillingStreet = 'TEST STREET';
			oAccount.BillingPostalCode = '123456';
			oAccount.ST_NAV_Non_SAP_Account__c = true;
		}
		insert lstAccount;
		Account oAccount = lstAccount[0];

		// Asset
		List<Asset> lstAsset = new List<Asset>();
		Asset oAsset1 = new Asset();
			oAsset1.AccountId = oAccount.Id;
			oAsset1.Product2Id = oProduct1.Id;
			oAsset1.Asset_Product_Type__c = 'S8 Planning Station';
			oAsset1.Ownership_Status__c = 'Purchased';
			oAsset1.Name = '123456789';
			oAsset1.Serial_Nr__c = '123456789';
			oAsset1.Status = 'Installed';
			oAsset1.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('PoleStar').getRecordTypeId();
			oAsset1.CFN_Text__c = 'AZERTY';
		lstAsset.add(oAsset1);
		insert lstAsset;

		// Create Opportunity Data
		clsTestData_Opportunity.iRecord_Opportunity = 1;
		List<Opportunity> lstOpportunity = clsTestData_Opportunity.createOpportunity();
		
		// Create Opport Data
		Opportunity_Asset__c oOpportunityAsset1 = new Opportunity_Asset__c();
			oOpportunityAsset1.Asset__c = lstAsset[0].Id;
			oOpportunityAsset1.Opportunity__c = lstOpportunity[0].Id;
			oOpportunityAsset1.Service_Level__c = 'Gold';
		Opportunity_Asset__c oOpportunityAsset2 = new Opportunity_Asset__c();
			oOpportunityAsset2.Asset__c = lstAsset[0].Id;
			oOpportunityAsset2.Opportunity__c = lstOpportunity[0].Id;
			oOpportunityAsset2.Service_Level__c = '';
		insert new List<Opportunity_Asset__c>{oOpportunityAsset1, oOpportunityAsset2};
		//------------------------------------------------------


		//------------------------------------------------------
		// Test Logic
		//------------------------------------------------------
		Test.startTest();

			List<Opportunity_Asset__c> lstOpportunityAsset = [SELECT Asset__r.CFN_Text__c, Service_Level__c, DIEN_Code__c FROM Opportunity_Asset__c];
			System.assertEquals(lstOpportunityAsset.size(), 2);

			Integer iDIENCode_Populated = 0;
			Integer iDIENCode_Empty = 0;
			for (Opportunity_Asset__c oOpportunityAsset : lstOpportunityAsset){
			
				if (oOpportunityAsset.Service_Level__c == 'GOLD'){
					System.assertEquals(oOpportunityAsset.DIEN_Code__c, 'AZERTYGOLD');
					iDIENCode_Populated++;
					oOpportunityAsset.Service_Level__c = 'GOLD+';
				}else{
					System.assert(String.isBlank(oOpportunityAsset.DIEN_Code__c));
					iDIENCode_Empty++;
					oOpportunityAsset.Service_Level__c = 'SILVER';
				}
			
			}

			System.assertEquals(iDIENCode_Populated, 1);
			System.assertEquals(iDIENCode_Empty, 1);

			update lstOpportunityAsset;

		Test.stopTest();
		//------------------------------------------------------


		//------------------------------------------------------
		// Validate Result
		//------------------------------------------------------
		lstOpportunityAsset = [SELECT Asset__r.CFN_Text__c, Service_Level__c, DIEN_Code__c FROM Opportunity_Asset__c];
		System.assertEquals(lstOpportunityAsset.size(), 2);

		iDIENCode_Populated = 0;
		iDIENCode_Empty = 0;
		for (Opportunity_Asset__c oOpportunityAsset : lstOpportunityAsset){
			
			if (oOpportunityAsset.Service_Level__c == 'GOLD+'){
				System.assert(String.isBlank(oOpportunityAsset.DIEN_Code__c));
				iDIENCode_Empty++;
			}else if (oOpportunityAsset.Service_Level__c == 'SILVER'){
				System.assertEquals(oOpportunityAsset.DIEN_Code__c, 'AZERTYSILVER');
				iDIENCode_Populated++;
			}else{
				System.assert(String.isBlank(oOpportunityAsset.DIEN_Code__c));
				iDIENCode_Empty++;
			}
			
		}

		System.assertEquals(iDIENCode_Populated, 1);
		System.assertEquals(iDIENCode_Empty, 1);
		//------------------------------------------------------
	}


}