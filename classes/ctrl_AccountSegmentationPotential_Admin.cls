public with sharing class ctrl_AccountSegmentationPotential_Admin {
	
	@AuraEnabled
    public static Map<String, List<ctrl_Account_Performance_Questions.PicklistValue>> getFilterOptions(String fiscalYear, String sbu, String procedure, String segment, String country, String accountId) {
    	
    	Map<String, List<ctrl_Account_Performance_Questions.PicklistValue>> result = new Map<String, List<ctrl_Account_Performance_Questions.PicklistValue>>();
    	
    	UMD_Purpose__c selectedPurpose;
    	
    	//Fiscal Year Options
    	List<ctrl_Account_Performance_Questions.PicklistValue> fiscalYearOptions = new List<ctrl_Account_Performance_Questions.PicklistValue>();
    	
    	for(UMD_Purpose__c UMDPurpose : [Select Id, Fiscal_Year_VS__c, Status__c from UMD_Purpose__c where Segmentation_Qualification__c = 'Potential' ORDER BY Fiscal_Year_VS__c DESC]){
    		
    		fiscalYearOptions.add(new ctrl_Account_Performance_Questions.PicklistValue(UMDPurpose.Fiscal_Year_VS__c, UMDPurpose.Fiscal_Year_VS__c));
    		
    		if(fiscalYear == null && (selectedPurpose == null || UMDPurpose.Status__c == 'Open for input')) selectedPurpose = UMDPurpose;    		
    		else if(fiscalYear != null && UMDPurpose.Fiscal_Year_VS__c == fiscalYear) selectedPurpose = UMDPurpose;
    	}
    	
    	result.put('fiscalYearOptions', fiscalYearOptions);
    	
    	result.put('fiscalYear', new List<ctrl_Account_Performance_Questions.PicklistValue>{new ctrl_Account_Performance_Questions.PicklistValue(selectedPurpose.Fiscal_Year_VS__c, selectedPurpose.Fiscal_Year_VS__c)});
    	
    	List<ctrl_Account_Performance_Questions.PicklistValue> readOnlyFY = new List<ctrl_Account_Performance_Questions.PicklistValue>();
    	    	
    	for(AggregateResult record : [Select Fiscal_Year__c fy from Customer_Segmentation__c GROUP BY Fiscal_Year__c]){
    		
    		String fy = String.valueOf(record.get('fy'));    		
    		readOnlyFY.add(new ctrl_Account_Performance_Questions.PicklistValue(fy, fy));
    	}
    	
    	result.put('readOnlyFY', readOnlyFY);
    	    	
    	String purposeClause =  ' where UMD_Purpose__c = \'' + selectedPurpose.Id + '\' ';
    	
    	String sbuClause = '';
    	if(sbu != null && sbu != '') sbuClause += ' AND Sub_Business_Unit__c = \'' + sbu + '\' ';
    	
    	String procedureClause = '';
    	if(procedure != null && procedure != '') procedureClause += ' AND Segmentation_Potential_Procedure__c = \'' + procedure + '\' ';    
    	
    	String segmentClause='';
    	if(segment != null && segment != ''){
    		
    		if(segment == '-') segmentClause += ' AND (Current_Segmentation__r.Size_Segment__c = null OR Current_Segmentation__r.Size_Segment__c = \'Not Segmented\')';
    		else segmentClause += ' AND Current_Segmentation__r.Size_Segment__c = \'' + segment + '\'';
    	} 
    	
    	String accountClause = '';
    	if(accountId != null && accountId != '') accountClause += ' AND Account__c = \'' + accountId + '\' ';
    	
    	String countryClause = '';
    	if(country != null && country != '') countryClause += ' AND Account__r.BillingCountry = \'' + country + '\' ';
    	
    	//SBU options
    	List<ctrl_Account_Performance_Questions.PicklistValue> sbuOptions = new List<ctrl_Account_Performance_Questions.PicklistValue>();
    	sbuOptions.add(new ctrl_Account_Performance_Questions.PicklistValue('', 'All'));
    	
    	String sbuQuery = 'SELECT Sub_Business_Unit__c from Segmentation_Potential_Input__c ' + purposeClause + ' GROUP BY Sub_Business_Unit__c ORDER BY Sub_Business_Unit__c';
    	    	
    	for(AggregateResult record : Database.query(sbuQuery)){
    		
    		String sbuOption = String.valueOf(record.get('Sub_Business_Unit__c'));    		
    		sbuOptions.add(new ctrl_Account_Performance_Questions.PicklistValue(sbuOption, sbuOption));
    	}
    	
    	result.put('sbuOptions', sbuOptions);
    	
    	//Procedure options
    	List<ctrl_Account_Performance_Questions.PicklistValue> procedureOptions = new List<ctrl_Account_Performance_Questions.PicklistValue>();
    	procedureOptions.add(new ctrl_Account_Performance_Questions.PicklistValue('', 'All'));
    	
    	String procedureQuery = 'SELECT Segmentation_Potential_Procedure__c, Segmentation_Potential_Procedure__r.Name procedure from Segmentation_Potential_Input__c';    	
    	procedureQuery += purposeClause + sbuClause;	
    	procedureQuery += ' GROUP BY Segmentation_Potential_Procedure__c, Segmentation_Potential_Procedure__r.Name ORDER BY Segmentation_Potential_Procedure__r.Name';
    	    	
    	for(AggregateResult record : Database.query(procedureQuery)){
    		
    		String procedureId = String.valueOf(record.get('Segmentation_Potential_Procedure__c'));
    		String procedureName = String.valueOf(record.get('procedure'));    		
    		procedureOptions.add(new ctrl_Account_Performance_Questions.PicklistValue(procedureId, procedureName));
    	}
    	
    	result.put('procedureOptions', procedureOptions);
    	
    	//Segment options
    	List<ctrl_Account_Performance_Questions.PicklistValue> segmentOptions = new List<ctrl_Account_Performance_Questions.PicklistValue>();
    	segmentOptions.add(new ctrl_Account_Performance_Questions.PicklistValue('', 'All'));
    	
    	String segmentQuery = 'SELECT Current_Segmentation__r.Size_Segment__c segment from Segmentation_Potential_Input__c';
    	segmentQuery += purposeClause + sbuClause + procedureClause;   	    	
    	segmentQuery += ' GROUP BY Current_Segmentation__r.Size_Segment__c ORDER BY Current_Segmentation__r.Size_Segment__c NULLS FIRST';    	
    	
    	for(AggregateResult record : Database.query(segmentQuery)){
    		
    		String segmentOption = String.valueOf(record.get('segment'));    		
    		if(segmentOption == null) segmentOption = 'Not Segmented';
    		
    		if(segmentOption == 'Not Segmented'){
    			 
    			 if(segmentOptions.size() == 1) segmentOptions.add(new ctrl_Account_Performance_Questions.PicklistValue('-', 'Not Segmented'));
    			 
    		}else segmentOptions.add(new ctrl_Account_Performance_Questions.PicklistValue(segmentOption, segmentOption));
    	}
    	
    	result.put('segmentOptions', segmentOptions);
    	
    	//Country options
    	List<ctrl_Account_Performance_Questions.PicklistValue> countryOptions = new List<ctrl_Account_Performance_Questions.PicklistValue>();
    	countryOptions.add(new ctrl_Account_Performance_Questions.PicklistValue('', 'All'));
    	
    	String countryQuery = 'SELECT Account__r.BillingCountry country from Segmentation_Potential_Input__c';
    	countryQuery += purposeClause + sbuClause + procedureClause + segmentClause;    	   	
    	countryQuery += ' GROUP BY Account__r.BillingCountry ORDER BY Account__r.BillingCountry';    	
    	
    	for(AggregateResult record : Database.query(countryQuery)){
    		
    		String countryOption = String.valueOf(record.get('country'));    		
    		countryOptions.add(new ctrl_Account_Performance_Questions.PicklistValue(countryOption, countryOption));
    	}
    	
    	result.put('countryOptions', countryOptions);
    	    	
    	return result;
    }
    
    @AuraEnabled
    public static Integer getRecordCount(String fiscalYear, String sbu, String procedure, String country, String accountId, String segmentation, String assignedTo, String status) {
    	
    	List<UMD_Purpose__c> potentialPurpose = [Select Id, Fiscal_Year_VS__c from UMD_Purpose__c where Segmentation_Qualification__c = 'Potential' AND Fiscal_Year_VS__c = :fiscalYear];
    	
    	String query = 'SELECT COUNT() from Segmentation_Potential_Input__c ';				
		query += getQueryClause(true, potentialPurpose[0].Id, sbu, procedure, country, accountId, segmentation, assignedTo, status);		
    	System.debug(query);
    	return Integer.valueOf(Database.countQuery(query));
    }
	
	
	@AuraEnabled
    public static List<ctrl_Account_Segmentation_Potential.PotentialProcedureInput> getUserPotentialInputs(String fiscalYear, String sbu, String procedure, String country, String accountId, String segmentation, String assignedTo, String status) {
    	
    	try{
    	
	    	List<ctrl_Account_Segmentation_Potential.PotentialProcedureInput> result = new List<ctrl_Account_Segmentation_Potential.PotentialProcedureInput>();
	    	
	    	List<UMD_Purpose__c> potentialPurpose = [Select Id, Fiscal_Year_VS__c from UMD_Purpose__c where Segmentation_Qualification__c = 'Potential' AND Fiscal_Year_VS__c = :fiscalYear];
			
			if(potentialPurpose.size() == 1){
	    		    		    		    		
	    		Map<Id, Segmentation_Potential_Procedure__c> procedureMap = new Map<Id, Segmentation_Potential_Procedure__c>(
	    		[Select Id, (Select Id, Competitor_picklist__c, Competitor_Product_picklist__c from Segmentation_Potential_Proc_Competitors__r where Active__c = true ORDER BY Competitor_picklist__c) from Segmentation_Potential_Procedure__c]
	    		);
	    		
		    	Set<String> prevKeys = new Set<String>();
		    	Set<String> segmentationKeys = new Set<String>();	    	
		    	
		    	String query = 'Select Id, Account__c, Account__r.Name, Account__r.BillingCity , Account__r.SAP_Id__c, Last_12_mth_Sales__c, Complete__c, Approved__c, Approved_2__c, Assigned_To__c, Assigned_To__r.Name, Approver_1__c, Approver_1__r.Name, Approver_2__c, Approver_2__r.Name';
		    	query += ', CurrencyIsoCode, Potential__c, Potential_Medtronic__c, Segmentation_Key__c, Current_Segmentation__r.Size_Segment__c, CreatedBy.Name, CreatedDate';	    	
		    	query += ', Segmentation_Potential_Procedure__c, Segmentation_Potential_Procedure__r.Name, Segmentation_Potential_Procedure__r.Registration_Unit__c, Segmentation_Potential_Procedure__r.Sub_Business_Unit__c';    	
		    	query += ', (SELECT Field, CreatedBy.Name, CreatedDate FROM Histories ORDER BY CreatedDate DESC)';
				query += ' from Segmentation_Potential_Input__c ';				
				query += getQueryClause(true, potentialPurpose[0].Id, sbu, procedure, country, accountId, segmentation, assignedTo, status);		
				
				String compQuery = 'Select Id, Potential__c, CurrencyIsoCode, Segmentation_Potential_Input__c, Segmentation_Potential_Proc_Competitor__c,  CreatedBy.Name, CreatedDate, (SELECT Field, CreatedBy.Name, CreatedDate FROM Histories ORDER BY CreatedDate DESC) from Segmentation_Potential_Input_Competitor__c ';				
				compQuery += getQueryClause(false, potentialPurpose[0].Id, sbu, procedure, country, accountId, segmentation, assignedTo, status);							
				compQuery += ' ORDER BY Segmentation_Potential_Proc_Competitor__r.Competitor_picklist__c';
				
				Map<Id, List<Segmentation_Potential_Input_Competitor__c>> inputCompetitorMap = new Map<Id, List<Segmentation_Potential_Input_Competitor__c>>();
				
				for(Segmentation_Potential_Input_Competitor__c inputCompetitor : Database.query(compQuery)){
					
					List<Segmentation_Potential_Input_Competitor__c> inputCompetitors = inputCompetitorMap.get(inputCompetitor.Segmentation_Potential_Input__c);
					
					if(inputCompetitors == null){
						
						inputCompetitors = new List<Segmentation_Potential_Input_Competitor__c>();
						inputCompetitorMap.put(inputCompetitor.Segmentation_Potential_Input__c, inputCompetitors);
					}
					
					inputCompetitors.add(inputCompetitor);
				}
		    	
		    	String prevFiscalYear = 'FY' + (Integer.valueOf(potentialPurpose[0].Fiscal_Year_VS__c.split('FY')[1]) - 1);
		    	
		    	for(Segmentation_Potential_Input__c potentialInput : Database.query(query)){
		    		   			
	    			prevKeys.add(potentialInput.Account__c + ':' + potentialInput.Segmentation_Potential_Procedure__c + ':' + prevFiscalYear);
		    		segmentationKeys.add(potentialInput.Segmentation_Key__c);
		    		
		    		ctrl_Account_Segmentation_Potential.PotentialProcedureInput procedureInput = new ctrl_Account_Segmentation_Potential.PotentialProcedureInput();
		    		procedureInput.record = potentialInput;	    		
		    		procedureInput.account = new ctrl_Account_Segmentation_Potential.PotentialAccount(potentialInput.Account__r);
		    		if(potentialInput.Current_Segmentation__r != null) procedureInput.account.accountSegment = potentialInput.Current_Segmentation__r.Size_Segment__c;
	    			if(procedureInput.account.accountSegment == null || procedureInput.account.accountSegment == 'Not Segmented') procedureInput.account.accountSegment = '-';
		    		
		    		if(potentialInput.Potential__c != null) procedureInput.potentialTS = ctrl_Account_Segmentation_Potential.getTimeStamp(potentialInput, 'Potential__c');
		    		if(potentialInput.Potential_Medtronic__c != null) procedureInput.potentialMDTTS = ctrl_Account_Segmentation_Potential.getTimeStamp(potentialInput, 'Potential_Medtronic__c');
		    		
		    		if(potentialInput.Complete__c == true) procedureInput.completedTS = ctrl_Account_Segmentation_Potential.getTimeStamp(potentialInput, 'Complete__c');
		    		else procedureInput.completedTS = potentialInput.Assigned_To__r.Name;
		    		if(potentialInput.Approved__c == true) procedureInput.approved1TS = ctrl_Account_Segmentation_Potential.getTimeStamp(potentialInput, 'Approved__c');
		    		else if(potentialInput.Approver_1__c != null) procedureInput.approved1TS = potentialInput.Approver_1__r.Name;
		    		if(potentialInput.Approved_2__c == true) procedureInput.approved2TS = ctrl_Account_Segmentation_Potential.getTimeStamp(potentialInput, 'Approved_2__c');
		    		else if(potentialInput.Approver_2__c != null)procedureInput.approved2TS = potentialInput.Approver_2__r.Name;
		    			    		
		    		Map<Id, Segmentation_Potential_Input_Competitor__c> existingCompetitors = new Map<Id, Segmentation_Potential_Input_Competitor__c>();
		    		
		    		List<Segmentation_Potential_Input_Competitor__c> inputCompetitors = inputCompetitorMap.get(potentialInput.Id);
		    		if(inputCompetitors != null){
			    		
			    		for(Segmentation_Potential_Input_Competitor__c	inputCompetitor : inputCompetitors){
			    			
			    			existingCompetitors.put(inputCompetitor.Segmentation_Potential_Proc_Competitor__c, inputCompetitor);
			    		}
		    		}
		    		
		    		procedureInput.competitors = new List<ctrl_Account_Segmentation_Potential.PotentialProcedureCompetitorInput>();
		    		
		    		for(Segmentation_Potential_Proc_Competitor__c procedureCompetitor : procedureMap.get(potentialInput.Segmentation_Potential_Procedure__c).Segmentation_Potential_Proc_Competitors__r){
		    			
		    			ctrl_Account_Segmentation_Potential.PotentialProcedureCompetitorInput competitorInput = new ctrl_Account_Segmentation_Potential.PotentialProcedureCompetitorInput();
		    			
		    			Segmentation_Potential_Input_Competitor__c existingInput = existingCompetitors.get(procedureCompetitor.Id);
		    			
		    			if(existingInput == null){
		    				
		    				existingInput = new Segmentation_Potential_Input_Competitor__c();
		    				existingInput.Segmentation_Potential_Proc_Competitor__c = procedureCompetitor.Id;
		    				existingInput.Segmentation_Potential_Input__c = potentialInput.Id;
		    				existingInput.CurrencyIsoCode = potentialInput.CurrencyIsoCode;
		    			
		    			}else{
		    				
		    				competitorInput.potentialTS = ctrl_Account_Segmentation_Potential.getTimeStamp(existingInput, 'Potential__c');
		    			}
		    			
		    			existingInput.Segmentation_Potential_Proc_Competitor__r = procedureCompetitor;
		    			
		    			competitorInput.record = existingInput;	    			
		    			procedureInput.competitors.add(competitorInput);
		    		}	    		
		    		
		    		result.add(procedureInput);	    		
		    	}
		    		    		    		    	
		    	//Previous FY Data    	
		    	Map<String, Segmentation_Potential_Input__c> prevProcedureInputs = new Map<String, Segmentation_Potential_Input__c>();
			   	Map<String, Segmentation_Potential_Input_Competitor__c> prevProcedureCompInputs = new Map<String, Segmentation_Potential_Input_Competitor__c>();
		    		
		    	String prevQuery = 'Select Id, Account__c, Segmentation_Potential_Procedure__c, CurrencyIsoCode, Potential__c, Potential_Medtronic__c'; 
				prevQuery += ', (Select Id, Potential__c, CurrencyIsoCode, Segmentation_Potential_Proc_Competitor__c from Segmentation_Potential_Input_Competitors__r)';
				prevQuery += ' from Segmentation_Potential_Input__c where Unique_Key__c IN :keys';
		    		    	
		    	for(SObject record : bl_Without_Sharing_Service.queryByKeys(prevQuery, prevKeys)){
		    		
		    		Segmentation_Potential_Input__c potentialInput = (Segmentation_Potential_Input__c) record;
		    		
		    		prevProcedureInputs.put(potentialInput.Account__c + ':' + potentialInput.Segmentation_Potential_Procedure__c, potentialInput);
		    		
		    		for(Segmentation_Potential_Input_Competitor__c potentialCompInput : potentialInput.Segmentation_Potential_Input_Competitors__r){
		    			
		    			prevProcedureCompInputs.put(potentialInput.Account__c + ':' + potentialInput.Segmentation_Potential_Procedure__c + ':' + potentialCompInput.Segmentation_Potential_Proc_Competitor__c, potentialCompInput);	
		    		}
				}
				
				Map<String, Account_Segmentation_Validation__c> segmentationMap = new Map<String, Account_Segmentation_Validation__c>();
				
				String segmentationQuery = 'Select Id, Unique_Key__c, Size_Segment__c, Size_Segment_Override__c from Account_Segmentation_Validation__c where Unique_Key__c IN :segmentationKeys';
				
				for(SObject record : Database.query(segmentationQuery)){
					
		    		Account_Segmentation_Validation__c currentSegmentation = (Account_Segmentation_Validation__c) record;
		    		if(currentSegmentation.Size_Segment__c == 'Not Segmented') currentSegmentation.Size_Segment__c = '-';
		    		if(currentSegmentation.Size_Segment_Override__c == null) currentSegmentation.Size_Segment_Override__c = currentSegmentation.Size_Segment__c;
		    		
		    		segmentationMap.put(currentSegmentation.Unique_Key__c, currentSegmentation);
				}				
		    	
		    	for(ctrl_Account_Segmentation_Potential.PotentialProcedureInput procedurePotential : result){
	    			
	    			//Prev values
	    			String key = procedurePotential.account.accountId + ':' + procedurePotential.record.Segmentation_Potential_Procedure__c;		    			
	    			procedurePotential.prevRecord = prevProcedureInputs.get(key);
	    			
	    			for(ctrl_Account_Segmentation_Potential.PotentialProcedureCompetitorInput compInput : procedurePotential.competitors){
	    				
	    				String compKey = procedurePotential.account.accountId + ':' + procedurePotential.record.Segmentation_Potential_Procedure__c + ':' + compInput.record.Segmentation_Potential_Proc_Competitor__c;		    			
	    				compInput.prevRecord = prevProcedureCompInputs.get(compKey);
	    			}
	    			
	    			//Current Segmentation
	    			Account_Segmentation_Validation__c currentSegment = segmentationMap.get(procedurePotential.record.Segmentation_Key__c);
	    			if(currentSegment == null) currentSegment = new Account_Segmentation_Validation__c(Size_Segment_Override__c = '-'); 
	    			procedurePotential.currentSegmentation = currentSegment;
				}
			}
			
			return result;
		
		}catch(Exception e){
    		
    		System.debug(e.getMessage() + ' | ' + e.getStackTraceString());
    		AuraHandledException auraEx = new AuraHandledException(e.getMessage()); 
    		if(test.isRunningTest()) auraEx.setMessage(e.getMessage());
    		throw auraEx;
    	}
    }
    
    private static String getQueryClause(Boolean isInput, Id questionPurposeId, String sbu, String procedure, String country, String accountId, String segmentation, String assignedTo, String status){
    	
    	String query = '';
    	
    	if(isInput == true) query += ' where UMD_Purpose__c = \'' + questionPurposeId + '\' ';
		else query += ' where Segmentation_Potential_Input__r.UMD_Purpose__c = \'' + questionPurposeId + '\' ';
		
		if(sbu != null && sbu != ''){
			
			if(isInput == true) query += ' AND Sub_Business_Unit__c = \'' + sbu + '\' ';
			else query += ' AND Segmentation_Potential_Input__r.Sub_Business_Unit__c = \'' + sbu + '\' ';			
		}
		
		if(procedure != null && procedure != ''){
			
			if(isInput == true) query += ' AND Segmentation_Potential_Procedure__c = \'' + procedure + '\' ';
			else query += ' AND Segmentation_Potential_Input__r.Segmentation_Potential_Procedure__c = \'' + procedure + '\' ';			
		}
		
		if(country != null && country != ''){
			
			if(isInput == true) query += ' AND Account__r.BillingCountry = \'' + country + '\' ';
			else query += ' AND Segmentation_Potential_Input__r.Account__r.BillingCountry = \'' + country + '\' ';			
		}
		
		if(accountId != null && accountId != ''){
			
			if(isInput == true) query += ' AND Account__c = \'' + accountId + '\' ';
			else query += ' AND Segmentation_Potential_Input__r.Account__c = \'' + accountId + '\' ';			
		}
				
		if(segmentation != null && segmentation != ''){
			
			if(segmentation == '-') segmentation = 'Not Segmented';
			
			if(isInput == true) query += ' AND ';
			else query += ' AND ';
									
			if(segmentation == 'Not Segmented'){					
				if(isInput == true) query += '(Current_Segmentation__r.Size_Segment__c = null OR ';
				else query += '(Segmentation_Potential_Input__r.Current_Segmentation__r.Size_Segment__c = null OR ';
				
			}
				
			if(isInput == true) query += 'Current_Segmentation__r.Size_Segment__c = \'' + segmentation + '\' ';
			else query += 'Segmentation_Potential_Input__r.Current_Segmentation__r.Size_Segment__c = \'' + segmentation + '\' ';
						
			if(segmentation == 'Not Segmented'){
				
				query += ')';
			}		
		}
		
		if(assignedTo != null && assignedTo != ''){
			
			if(isInput == true) query += ' AND (Assigned_To__c = \'' + assignedTo + '\' OR Approver_1__c = \'' + assignedTo + '\' OR Approver_2__c = \'' + assignedTo + '\')';
			else query += ' AND (Segmentation_Potential_Input__r.Assigned_To__c = \'' + assignedTo + '\' OR Segmentation_Potential_Input__r.Approver_1__c = \'' + assignedTo + '\' OR Segmentation_Potential_Input__r.Approver_2__c = \'' + assignedTo + '\')';			
		}
		
		if(status != null && status != ''){
		
			if(status == 'Open'){
				    			
				if(isInput == true) query += ' AND Complete__c = false';
				else query += ' AND Segmentation_Potential_Input__r.Complete__c = false';
				
			}else if(status == 'Completed'){
				
				if(isInput == true) query += ' AND Complete__c = true ';
				else query += ' AND Segmentation_Potential_Input__r.Complete__c = true';
				
			}else if(status == 'Approved'){
				
				if(isInput == true) query += ' AND Complete__c = true AND (Approved__c = true OR Approver_1__c = null) AND (Approved_2__c = true OR Approver_2__c = null)';
				else query += ' AND Segmentation_Potential_Input__r.Complete__c = true AND (Segmentation_Potential_Input__r.Approved__c = true OR Segmentation_Potential_Input__r.Approver_1__c = null) AND (Segmentation_Potential_Input__r.Approved_2__c = true OR Segmentation_Potential_Input__r.Approver_2__c = null)';				
			}		
		}
		
		return query;
    }
    
    
    @AuraEnabled
    public static List<ctrl_Account_Segmentation_Potential.PotentialProcedureInput> savePotentials(List<ctrl_Account_Segmentation_Potential.PotentialProcedureInput> potentials) {
    	
    	return ctrl_Account_Segmentation_Potential.savePotentials(potentials);
    }
}