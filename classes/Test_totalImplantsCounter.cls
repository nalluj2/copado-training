/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_totalImplantsCounter {
    static testMethod void myUnitTest() {
    	System.debug('###################### BEGIN Test_totalImplantsCounter #################');
    	// Create test sObjects	
		Account acc1 = new Account();
		acc1.Name = 'TestAccount1' ;
		Test.startTest();
		insert acc1;

		Account acc2 = new Account();
		acc2.Name = 'TestAccount2' ;
		insert acc2;
		
		Contact cont1 = new Contact();
		cont1.LastName = 'TestCont1' ;  
		cont1.FirstName = 'TEST';  
		cont1.AccountId = acc1.Id ;
		cont1.Contact_Active__c = false  ;
		cont1.Contact_Inactive_Reason__c = 'Inactive Reason is required to all inactve Contacts'; 
	    cont1.Contact_Department__c = 'Diabetes Adult'; 
	    cont1.Contact_Primary_Specialty__c = 'ENT';
	    cont1.Affiliation_To_Account__c = 'Employee';
	    cont1.Primary_Job_Title_vs__c = 'Manager';
	    cont1.Contact_Gender__c = 'Male'; 		
		insert cont1;
		
		Contact cont2 = new Contact();
		cont2.LastName = 'TestCont2';  
		cont2.FirstName = 'TEST';  
		cont2.AccountId = acc1.Id ;
		cont2.Contact_Active__c = false;
		cont2.Contact_Inactive_Reason__c = 'Inactive Reason is required to all inactve Contacts';
	    cont2.Contact_Department__c = 'Diabetes Adult'; 
	    cont2.Contact_Primary_Specialty__c = 'ENT';
	    cont2.Affiliation_To_Account__c = 'Employee';
	    cont2.Primary_Job_Title_vs__c = 'Manager';
	    cont2.Contact_Gender__c = 'Male'; 		 
		insert cont2;

        //Insert Company
        Company__c cmpny = new Company__c();
        cmpny.name='TestMedtronic';
        cmpny.CurrencyIsoCode = 'EUR';
        cmpny.Current_day_in_Q1__c=56;
        cmpny.Current_day_in_Q2__c=34; 
        cmpny.Current_day_in_Q3__c=5; 
        cmpny.Current_day_in_Q4__c= 0;   
        cmpny.Current_day_in_year__c =200;
        cmpny.Days_in_Q1__c=56;  
        cmpny.Days_in_Q2__c=34;
        cmpny.Days_in_Q3__c=13;
        cmpny.Days_in_Q4__c=22;
        cmpny.Days_in_year__c =250;
        cmpny.Company_Code_Text__c = 'T20';
        insert cmpny; 

	     Business_Unit__c bu = new Business_Unit__c();
	     bu.name = 'Testing BU';
	     bu.abbreviated_name__c = 'Abb Bu';
	     bu.Company__c = cmpny.id;
	     insert bu;

        //Insert SBU
        Sub_Business_Units__c sbu1 = new Sub_Business_Units__c();
        sbu1.name='SBUMedtronic1';
        sbu1.Business_Unit__c=bu.id;        
        insert sbu1;

        //Insert Therapy Group
        Therapy_Group__c tg = new Therapy_Group__c();
        tg.Company__c=cmpny.id;
        tg.Name='Therapy Group';
        tg.Sub_Business_Unit__c=sbu1.id;
        insert tg;
	
	 	 Therapy__c th = new Therapy__c();
	     th.name='test ## th';
	     th.Business_Unit__c = bu.id;
	     th.Therapy_Group__c = tg.id;
	     th.Sub_Business_Unit__c=sbu1.id;
	     th.Therapy_Name_Hidden__c='test ## th';
	     insert th;
		
		Implant__c imp1 = new Implant__c();
		imp1.Implant_Implanting_Account__c = acc1.Id ;
		imp1.Implant_Implanting_Contact__c = cont1.Id ;
		imp1.Implant_Referring_Account__c = acc2.Id ;
		imp1.Implant_Referring_Contact__c = cont2.Id ; 
		imp1.Implant_Date_Of_Surgery__c = System.today();
	    imp1.Therapy__c = th.Id;
    	imp1.Attended_Implant_Text__c = 'UnAttended';
        imp1.Duration_Nr__c = 10;             		
		insert imp1 ; 
		     	     
		Implant__c[] imps1 = new Implant__c[]{}; 
		imps1.add(imp1); 
		
        totalImplantsCounter.countImplants(imps1);
        
        
        Implant__c imp2 = new Implant__c();
		imp2.Implant_Implanting_Account__c = acc1.Id ;
		imp2.Implant_Implanting_Contact__c = cont1.Id ;
		imp1.Implant_Date_Of_Surgery__c = System.today();
	    imp2.Therapy__c = th.Id;
    	imp2.Attended_Implant_Text__c = 'UnAttended';
        imp2.Duration_Nr__c = 10;             				
		insert imp2 ; 
		     	     
		Implant__c[] imps2 = new Implant__c[]{}; 
		imps2.add(imp2); 
		
        totalImplantsCounter.countImplants(imps2);
        
        
        Test.stopTest();
        System.debug('###################### END Test_totalImplantsCounter #################');  
        
        
    }
}