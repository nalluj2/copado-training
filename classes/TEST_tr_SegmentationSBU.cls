/*
 *      Created Date 	: 20140226
 *      Author 			: Bart Caelen
 *      Description 	: TEST Class for the APEX TRIGGER tr_SegmentationSBU
 */
@isTest private class TEST_tr_SegmentationSBU {

    /**
    * @description 
    */ 
    @isTest static void test_tr_SegmentationSBU() {

		//------------------------------------------------
		// CREATE TEST DATA
		//------------------------------------------------
		clsTestData.iRecord_Account = 10;
		clsTestData.createAccountData();
		clsTestData.createSubBusinessUnitData();
		//------------------------------------------------

		// Verify that we have the needed Sub Business Unit		
		Set<string> setSBUName = new Set<string>();
			setSBUName.add('Brain Modulation');
			setSBUName.add('Neurosurgery');
		List<Sub_Business_Units__c> lstSBU = [SELECT Id, Name FROM Sub_Business_Units__c WHERE Company_Text__c = 'Europe' AND Business_Unit__r.Name = 'Cranial Spinal' AND Name in :setSBUName];
		System.assertEquals(lstSBU.size(), 2);

		Test.startTest();
		
		// CREATE Segmentation SBU Data - PART 1
		clsTestData.idSegmentationSBU_SubBusinessUnit = lstSBU[0].Id;
		clsTestData.dSegmentationSBU_StartDate = date.today().addDays(-30);
		clsTestData.oMain_SegmentationSBU = null;
		clsTestData.createSegmentationSBUData(false);
		insert clsTestData.lstSegmentationSBU;

		clsTestData.idSegmentationSBU_SubBusinessUnit = lstSBU[1].Id;
		clsTestData.dSegmentationSBU_StartDate = date.today().addDays(-20);
		clsTestData.oMain_SegmentationSBU = null;
		clsTestData.createSegmentationSBUData(true);

		Segmentation_SBU__c oSSBU = clsTestData.oMain_SegmentationSBU.clone(false, true);
			oSSBU.Start__c	= date.today().addDays(-5);
		insert oSSBU;


		clsTestData.idSegmentationSBU_SubBusinessUnit = lstSBU[1].Id;
		clsTestData.dSegmentationSBU_StartDate = date.today().addDays(-10);
		clsTestData.oMain_SegmentationSBU = null;
		try{ 
			clsTestData.createSegmentationSBUData(true);
		}catch(Exception oEX){
			System.assert(oEX.getMessage().contains('Segmentation record exists'));
		}
		
		// CREATE Segmentation SBU Data - PART 2 (same data but the Start Date is different)
		clsTestData.idSegmentationSBU_SubBusinessUnit = lstSBU[0].Id;
		clsTestData.dSegmentationSBU_StartDate = date.today();
		clsTestData.oMain_SegmentationSBU = null;
		clsTestData.createSegmentationSBUData();

		clsTestData.idSegmentationSBU_SubBusinessUnit = lstSBU[1].Id;
		clsTestData.dSegmentationSBU_StartDate = date.today();
		clsTestData.oMain_SegmentationSBU = null;
		clsTestData.createSegmentationSBUData();

    		
		// TEST Delete
		List<Segmentation_SBU__c> lstSSBU = [SELECT Id, Account__c, Sub_Business_Unit__c FROM Segmentation_SBU__c];
		delete lstSSBU;

		test.stopTest();
    }

}