@isTest 
private class TEST_ctrlExt_ManageContractAsset {

    private static testmethod void testManageContractAsset(){
    	
		User oUser_Admin = clsTestData_User.createUser_SystemAdministratorMDT('tstadm1', false);
			oUser_Admin.Primary_sBU__c = 'AF Solutions';
		insert oUser_Admin;

		System.runAs(oUser_Admin){

    		User oUser = [SELECT Id, Company_Code_Text__c FROM User WHERE Id = :UserInfo.getUserId()];
    	
    		Company__c oCompany = new Company__c();
       			oCompany.name='Test Company';
       			oCompany.Company_Code_Text__c = oUser.Company_Code_Text__c;
       			oCompany.CurrencyIsoCode = 'EUR';
       			oCompany.Current_day_in_Q1__c = 56;
       			oCompany.Current_day_in_Q2__c = 34; 
       			oCompany.Current_day_in_Q3__c = 5; 
       			oCompany.Current_day_in_Q4__c = 0;   
       			oCompany.Current_day_in_year__c = 200;
       			oCompany.Days_in_Q1__c = 56;  
       			oCompany.Days_in_Q2__c = 34;
       			oCompany.Days_in_Q3__c = 13;
       			oCompany.Days_in_Q4__c = 22;
       			oCompany.Days_in_year__c = 250;       	
       		insert oCompany;     

			Business_Unit_Group__c oBUG = new Business_Unit_Group__c();
				oBUG.Name = 'Restorative';
				oBUG.Abbreviated_name__c = 'RTG';
				oBUG.Master_Data__c = oCompany.Id;
			insert oBUG;
                
			Business_Unit__c oBU = new Business_Unit__c();
				oBU.name = 'Cranial Spinal';
				oBU.abbreviated_name__c = 'CS';
				oBU.Business_Unit_Group__c = oBUG.Id;
				oBU.Company__c = oCompany.Id;
			insert oBU;
        
			Sub_Business_Units__c oSBU = new Sub_Business_Units__c();
				oSBU.Name = 'Neurosurgery';
				oSBU.Business_Unit__c = oBU.id;
			insert oSBU;

			Therapy_Group__c oTherapyGroup = new Therapy_Group__c();
				oTherapyGroup.Name = 'Midas Rex';
				oTherapyGroup.Company__c = oCompany.Id;
				oTherapyGroup.Sub_Business_Unit__c = oSBU.Id;
			insert oTherapyGroup;
        
			Therapy__c oTherapy = new Therapy__c();
				oTherapy.Name = 'Midas Rex';
				oTherapy.Business_Unit__c = oBU.id;
				oTherapy.Sub_Business_Unit__c = oSBU.Id;
				oTherapy.Therapy_Group__c = oTherapyGroup.Id;
				oTherapy.Therapy_Name_Hidden__c = 'Midas Rex';
			insert oTherapy;
        
			Product_Group__c oProductGroup = new Product_Group__c();
				oProductGroup.Name = 'Midas Rex Capital';
				oProductGroup.Therapy_ID__c = oTherapy.id;
			insert oProductGroup;
                
			Product2 oProduct = new Product2();
				oProduct.Name = 'EHS Motor Capital';
				oProduct.Business_Unit_ID__c = oBU.id;
				oProduct.Product_Group__c = oProductGroup.id;
				oProduct.Consumable_Bool__c = true;
				oProduct.IsActive = true;        
				oProduct.RecordtypeId = clsUtil.getRecordTypeByDevName('Product2', 'MDT_Marketing_Product').Id;       
			insert oProduct;

			Mapping_Asset_ServiceLevel_DIENCode__c oMappingDIENCode = new Mapping_Asset_ServiceLevel_DIENCode__c();
				oMappingDIENCode.CFN_Code__c = 'AZERTY';
				oMappingDIENCode.Service_Level__c = 'GOLD';
				oMappingDIENCode.DIEN_Code__c = 'AZERTYGOLD';
			insert oMappingDIENCode;
		        
			Account oAccount = new Account();
				oAccount.Name = 'Test Account';
				oAccount.SAP_ID__c = '123456789';        
			insert oAccount;
        
			Asset oAsset = new Asset();
				oAsset.Name = 'Test Asset';
				oAsset.AccountId = oAccount.Id;
				oAsset.Product2Id = oProduct.Id;
				oAsset.Serial_Nr__c = 'A123';
				oAsset.Status = 'Purchased';
				oAsset.RecordTypeId = clsUtil.getRecordTypeByDevName('Asset', 'General').Id;
			insert oAsset;
        
            Contract oContract = new Contract();
		        oContract.AccountId = oAccount.Id;
		        oContract.Agreement_level_Picklist__c = 'Test';
		        oContract.StartDate = Date.Today() - 30;
		        oContract.ContractTerm = 12;
			insert oContract;

		    Test.startTest();
	        
				List<AssetContract__c> lstAssetContract = [SELECT Id FROM AssetContract__c WHERE Contract__c = :oContract.Id];
				System.assertEquals(lstAssetContract.size(), 0);

				ctrlExt_ManageContractAsset oCTRL = new ctrlExt_ManageContractAsset(new ApexPages.StandardController(oContract));
				oCTRL.tFilter_BU = oBU.Name;
				oCTRL.lstFilter_Status.add('Purchased');
				oCTRL.filterBUChange();

				oCTRL.loadAssetData();
				System.assertEquals(oCTRL.lstWRData.size(), 1);
				System.assertEquals(oCTRL.bEnableButton_ADD_DEL, false);

				oCTRL.lstWRData[0].bSelected_ADD = true;
				oCTRL.selectionAddChanged();
				oCTRL.bBulkSelected_ADD = true;
				oCTRL.bulkSelectedAddChange();
				System.assertEquals(oCTRL.iNum_ADD, 1);
				System.assertEquals(oCTRL.iNum_DEL, 0);
				System.assertEquals(oCTRL.bEnableButton_ADD_DEL, true);
			
				oCTRL.oBulkServiceLevel.Service_Level__c = 'GOLD';
				oCTRL.bulkServiceLevelChange();

				oCTRL.addDelAsset();
				System.assertEquals(oCTRL.bEnableButton_ADD_DEL, false);
				
				lstAssetContract = [SELECT Id FROM AssetContract__c WHERE Contract__c = :oContract.Id];
				System.assertEquals(lstAssetContract.size(), 1);
				
				oCTRL.loadAssetData();
				System.assertEquals(oCTRL.lstWRData.size(), 1);

				oCTRL.lstWRData[0].bSelected_DEL = true;
				oCTRL.selectionDelChanged();
				oCTRL.bBulkSelected_DEL = true;
				oCTRL.bulkSelectedDelChange();
				System.assertEquals(oCTRL.iNum_ADD, 0);
				System.assertEquals(oCTRL.iNum_DEL, 1);
				System.assertEquals(oCTRL.bEnableButton_ADD_DEL, true);

				oCTRL.addDelAsset();
				System.assertEquals(oCTRL.bEnableButton_ADD_DEL, false);

				lstAssetContract = [SELECT Id FROM AssetContract__c WHERE Contract__c = :oContract.Id];
				System.assertEquals(lstAssetContract.size(), 0);


				// EXCEPTIONS
				clsUtil.tExceptionName = 'ctrlExt_ManageContractAsset.addDelAsset';
				oCTRL.addDelAsset();

				Contract oContract_New = new Contract();
				oCTRL = new ctrlExt_ManageContractAsset(new ApexPages.StandardController(oContract_New));


			Test.stopTest();
		}

	}

}