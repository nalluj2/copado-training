//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 25-03-2020
//  Description      : APEX Test Class for bl_AccountPlan_TeamMember + logic in bl_Opportunity_Trigger.syncAPTM_OTM
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_bl_AccountPlan_TeamMember{

	@isTest
	private static void test_syncAPTM_OTM(){

		// CREATE TEST DATA ----------------------------------------------------------
		List<User> lstUser = new List<User>{clsTestData_User.createUser_SystemAdministratorMDT('tstAdm1', false), clsTestData_User.createUser_SystemAdministratorMDT('tstAdm2', false), clsTestData_User.createUser_SystemAdministratorMDT('tstAdm3', false), clsTestData_User.createUser_SystemAdministratorMDT('tstAdm4', false), clsTestData_User.createUser_SystemAdministratorMDT('tstAdm5', false)};
		insert lstUser;

		List<Account_Plan_2__c> lstAccountPlan = new List<Account_Plan_2__c>();
		List<Account_Plan_Team_Member__c> lstAccountPlanTeamMember = new List<Account_Plan_Team_Member__c>();
		List<Opportunity> lstOpportunity = new List<Opportunity>();
		List<OpportunityTeamMember> lstOpportunityTeamMember = new List<OpportunityTeamMember>();

		System.runAs(lstUser[0]){

			clsTestData_Account.iRecord_Account_SAPAccount = 1;
			List<Account> lstAccount = clsTestData_Account.createAccount_SAPAccount();


			Account_Plan_2__c oAccountPlan = new Account_Plan_2__c();
				oAccountPlan.Account_Plan_Level__c = 'xBUG';
				oAccountPlan.Account__c = lstAccount[0].Id;
				oAccountPlan.Start_Date__c = Date.today();
				oAccountPlan.RecordTypeId = clsUtil.getRecordTypeByDevName('Account_Plan_2__c', 'SAM_Account_Plan').Id; 
			lstAccountPlan.add(oAccountPlan);
			insert lstAccountPlan;

			
			Opportunity oOpportunity = new Opportunity();
				oOpportunity.RecordTypeId = clsUtil.getRecordTypeByDevName('Opportunity', 'Strategic_Account_Management').Id;
				oOpportunity.Name = 'TEST SAM OPP';
				oOpportunity.AccountId = lstAccount[0].Id;
				oOpportunity.Account_Plan__c = oAccountPlan.Id;
				oOpportunity.StageName = 'Prospecting/Lead';
				oOpportunity.CloseDate = Date.today().addYears(1);
			lstOpportunity.add(oOpportunity);
			insert lstOpportunity;

			System.assertEquals(lstOpportunity.size(), 1);
			System.assertEquals(lstOpportunity[0].Account_Plan__c, oAccountPlan.Id);

			lstAccountPlan = [SELECT Id, (SELECT Id FROM Opportunities__r) FROM Account_Plan_2__c WHERE Id = :oAccountPlan.Id AND RecordTypeId = :clsUtil.getRecordTypeByDevName('Account_Plan_2__c', 'SAM_Account_Plan').Id];
			System.assertEquals(lstAccountPlan.size(), 1);
			System.assertEquals(lstAccountPlan[0].Opportunities__r.size(), 1);

			lstOpportunity = [SELECT Id, Account_Plan__c FROM Opportunity];
			System.assertEquals(lstOpportunity[0].Account_Plan__c, oAccountPlan.Id);
			
			lstAccountPlanTeamMember = [SELECT Id, Team_Member__c, Account_Plan__r.RecordTypeId FROM Account_Plan_Team_Member__c WHERE Account_Plan__c = :oAccountPlan.Id];

			System.assertEquals(lstAccountPlanTeamMember.size(), 1);

		}

		// EXECUTE LOGIC -------------------------------------------------------------
		Test.startTest();

			System.runAs(lstUser[0]){

				lstAccountPlanTeamMember = new List<Account_Plan_Team_Member__c>();

				Account_Plan_Team_Member__c oAccountPlanTeamMember1 = new Account_Plan_Team_Member__c();
					oAccountPlanTeamMember1.Account_Plan__c = lstAccountPlan[0].Id;
					oAccountPlanTeamMember1.Team_Member__c = lstUser[1].Id;
				lstAccountPlanTeamMember.add(oAccountPlanTeamMember1);

				Account_Plan_Team_Member__c oAccountPlanTeamMember2 = new Account_Plan_Team_Member__c();
					oAccountPlanTeamMember2.Account_Plan__c = lstAccountPlan[0].Id;
					oAccountPlanTeamMember2.Team_Member__c = lstUser[2].Id;
				lstAccountPlanTeamMember.add(oAccountPlanTeamMember2);

				Account_Plan_Team_Member__c oAccountPlanTeamMember3 = new Account_Plan_Team_Member__c();
					oAccountPlanTeamMember3.Account_Plan__c = lstAccountPlan[0].Id;
					oAccountPlanTeamMember3.Team_Member__c = lstUser[3].Id;
				lstAccountPlanTeamMember.add(oAccountPlanTeamMember3);

				insert lstAccountPlanTeamMember;


					oAccountPlanTeamMember3.Team_Member__c = lstUser[4].Id;
				update oAccountPlanTeamMember3;


				delete oAccountPlanTeamMember3;

			}				

		Test.stopTest();


		// TEST RESULT ---------------------------------------------------------------
		System.runAs(lstUser[0]){

			lstAccountPlanTeamMember = [SELECT Id, Account_Plan__c, Team_Member__c FROM Account_Plan_Team_Member__c WHERE Account_Plan__c = :lstAccountPlan[0].Id];
			lstOpportunityTeamMember = [SELECT Id, OpportunityId, UserId FROM OpportunityTeamMember WHERE OpportunityId = :lstOpportunity[0].Id];

			System.assertEquals(lstAccountPlanTeamMember.size(), 3);
			System.assertEquals(lstOpportunityTeamMember.size(), 3);

			for (Account_Plan_Team_Member__c oAccountPlanTeamMember : lstAccountPlanTeamMember) System.assertEquals(oAccountPlanTeamMember.Account_Plan__c, lstAccountPlan[0].Id);
			for (OpportunityTeamMember oOpportunityTeamMember : lstOpportunityTeamMember) System.assertEquals(oOpportunityTeamMember.OpportunityId, lstOpportunity[0].Id);

		}

	}

}
//--------------------------------------------------------------------------------------------------------------------