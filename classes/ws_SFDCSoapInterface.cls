public class ws_SFDCSoapInterface {


	@future(callout=true)
	public static void doSoapCall(String body, String sessionId){
		HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();

        //req.setHeader('Authorization', 'Bearer '+ sessionId);
        req.setHeader('Content-Type','text/xml;charset=UTF-8');
        
        req.setHeader('Accept-Encoding','gzip,deflate');
		req.setHeader('Content-Type','text/xml;charset=UTF-8');
		req.setHeader('SOAPAction','""');
		req.setHeader('User-Agent','Jakarta Commons-HttpClient/3.1');
        
        req.setEndpoint(URL.getSalesforceBaseUrl().toExternalForm()+'/services/Soap/c/27.0');
        req.setMethod('POST');
        
        String prebody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"\n'
  +' xmlns:urn="urn:enterprise.soap.sforce.com"\n'
  +' xmlns:urn1="urn:sobject.enterprise.soap.sforce.com"\n'
  +' xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">\n'+
  
  						 ' <soapenv:Header>\n'+
     					 ' <urn:SessionHeader>\n'+
        				 ' <urn:sessionId>'+sessionId+'</urn:sessionId>\n'+
     					 ' </urn:SessionHeader>\n'+
  						 ' </soapenv:Header>\n'+
  						 ' <soapenv:Body>\n'+
        				 '';
       
       String postBody = ' '+
  						 '</soapenv:Body>\n'+
						 '</soapenv:Envelope>\n';
        String fullBody = prebody+body+postbody;
        req.setBody(fullBody);
        
        System.debug('req '+req);
        System.debug('body '+fullBody);
		
		req.setHeader('Content-Length',String.valueOf(fullBody.length()));
		

        try {
            res = http.send(req);
            System.debug('Response '+res);
             System.debug('Response '+res.getBody());
              System.debug('Response '+res.getStatus());
        } catch(System.CalloutException e) {
            System.debug('Callout error: '+ e);
            System.debug(res.toString());
        }
	}

}