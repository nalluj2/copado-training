public class bl_Activity_Scheduling_Attendee {
    
    public static void setExternalKey(List<Activity_Scheduling_Attendee__c> triggerNew){
    	
    	for(Activity_Scheduling_Attendee__c attendee : triggerNew){
    		
    		attendee.Unique_Key__c = attendee.Case__c + ':' + attendee.Attendee__c;
    	}
    }
    
    public static void checkAttendeeNotAssignedTo(List<Activity_Scheduling_Attendee__c> triggerNew){
    	
    	Set<Id> caseIds = new Set<Id>();
    	
    	for(Activity_Scheduling_Attendee__c attendee : triggerNew){
    	
    		caseIds.add(attendee.Case__c);
    	}
    	
    	Map<Id, Case> caseMap = new Map<Id, Case>([Select Id, Assigned_To__c from Case where Id IN :caseIds]);
    	
    	for(Activity_Scheduling_Attendee__c attendee : triggerNew){
    		
    		Case attendeeCase = caseMap.get(attendee.Case__c);
    		
    		if(attendee.Attendee__c == attendeeCase.Assigned_To__c) attendee.addError('The user assigned to the activity cannot be added as attendee.');
    	}
    }
    
    public static void manageEventAttendee(List<Activity_Scheduling_Attendee__c> triggerNew, Map<Id, Activity_Scheduling_Attendee__c> oldMap){
    	
    	Set<Id> caseIds = new Set<Id>();
    	
    	for(Activity_Scheduling_Attendee__c attendee : triggerNew){
    		
    		if(oldMap == null || attendee.Attendee__c != oldMap.get(attendee.Id).Attendee__c) caseIds.add(attendee.Case__c);	
    	}    	
    	
    	if(caseIds.size() > 0){
    		    		
    		Map<Id, Event> caseEvents = new Map<Id, Event>();
    		
    		for(Event event : [select Id, WhatId, (Select RelationId from EventRelations where IsInvitee = true) from Event where WhatId in :caseIds AND IsChild = false AND RecordTypeId = :bl_Event_Trigger.activityScheduling_RT ALL ROWS]){
	    		
	    		caseEvents.put(event.WhatId, event);
	    	}
    		
    		List<EventRelation> newRelations = new List<EventRelation>();
    		List<EventRelation> toDelete = new List<EventRelation>();
    		
    		for(Case activityCase : [Select Id, Is_Recurring__c, Assigned_To__c, (Select Attendee__c from Activity_Scheduling_Attendees__r) from Case where Id IN :caseIds]){
    			
    			if(activityCase.Is_Recurring__c) continue;//Skip Recurring Cases
    			
    			Event caseEvent = caseEvents.get(activityCase.Id);
    			System.debug(caseEvent);
    			if(caseEvent != null){
    				    				
    				Map<Id, EventRelation> existingRelations = new Map<Id, EventRelation>();
    				
    				for(EventRelation eventRel : caseEvent.EventRelations){
    					
    					existingRelations.put(eventRel.RelationId, eventRel);
    					System.debug(eventRel.RelationId);
    				}
    				
    				for(Activity_Scheduling_Attendee__c attendee : activityCase.Activity_Scheduling_Attendees__r){
    					System.debug(attendee.Attendee__c);
    					if(existingRelations.remove(attendee.Attendee__c) == null){
    						
    						EventRelation eventRel = new EventRelation();
    						eventRel.EventId = caseEvent.Id;
    						eventRel.RelationId = attendee.Attendee__c;
    						eventRel.Status = 'Accepted';
    						
    						newRelations.add(eventRel);
    					}    					
    				}
    				
    				existingRelations.remove(activityCase.Assigned_To__c);
    				
    				if(existingRelations.size() > 0) toDelete.addAll(existingRelations.values());
    					
    			}   			
    		}	
    		
    		if(newRelations.size() > 0) insert newRelations;
    		if(toDelete.size() > 0) delete toDelete;
    	}
    }
    
    public static void manageRecurringAttendee(List<Activity_Scheduling_Attendee__c> triggerNew, Map<Id, Activity_Scheduling_Attendee__c> oldMap, String action){
    	    	
    	Set<Id> caseIds = new Set<Id>();
    	
    	for(Activity_Scheduling_Attendee__c attendee : triggerNew){
    	
    		if(oldMap == null || attendee.Attendee__c != oldMap.get(attendee.Id).Attendee__c) caseIds.add(attendee.Case__c);
    	}
    	
    	Map<Id, List<Case>> recurrenceMap = new Map<Id, List<Case>>();
    	
    	DateTime now = DateTime.now();
    	
    	for(Case recurrenceCase : [Select Id, ParentId, (Select Attendee__c from Activity_Scheduling_Attendees__r) from Case where ParentId IN :caseIds AND Parent.Is_Recurring__c = true AND Start_of_Procedure__c > :now]){
    		
    		List<Case> parentCases = recurrenceMap.get(recurrenceCase.ParentId);
    		
    		if(parentCases == null){
    			
    			parentCases = new List<Case>();
    			recurrenceMap.put(recurrenceCase.ParentId, parentCases);
    		}	
    		
    		parentCases.add(recurrenceCase);
    	}
    	
    	if(recurrenceMap.isEmpty()) return;
    	
    	List<Activity_Scheduling_Attendee__c> toUpsert = new List<Activity_Scheduling_Attendee__c>();
    	List<Activity_Scheduling_Attendee__c> toDelete = new List<Activity_Scheduling_Attendee__c>();
    	
    	for(Activity_Scheduling_Attendee__c attendee : triggerNew){
    	
    		List<Case> recurrenceCases = recurrenceMap.get(attendee.Case__c);
    		
    		if(recurrenceCases == null) continue;
    		
    		for(Case recurrenceCase : recurrenceCases){
    			
    			if(action == 'INSERT' || action == 'UNDELETE'){
    				
    				Activity_Scheduling_Attendee__c recurrenceAttendee = new Activity_Scheduling_Attendee__c();
    				recurrenceAttendee.Case__c = recurrenceCase.Id;
    				recurrenceAttendee.Attendee__c = attendee.Attendee__c;
    				
    				toUpsert.add(recurrenceAttendee);
    				
    			}else{
    				
    				Map<Id, Activity_Scheduling_Attendee__c> existingAttendees = new Map<Id, Activity_Scheduling_Attendee__c>();
    				
    				for(Activity_Scheduling_Attendee__c existingAttendee : recurrenceCase.Activity_Scheduling_Attendees__r){
    					
    					existingAttendees.put(existingAttendee.Attendee__c, existingAttendee);
    				}
    				
    				if(action == 'UPDATE'){
    					
    					Activity_Scheduling_Attendee__c existingAttendee = existingAttendees.get(oldMap.get(attendee.Id).Attendee__c);
    					existingAttendee.Attendee__c = attendee.Attendee__c;
    					
    					toUpsert.add(existingAttendee);
    					
    				}else if(action =='DELETE'){
    					
    					Activity_Scheduling_Attendee__c existingAttendee = existingAttendees.get(attendee.Attendee__c);    					    					
    					if(existingAttendee !=null) toDelete.add(existingAttendee);
    				}
    			}    			
    		}
    	}
    	
    	if(toDelete.size() > 0) delete toDelete;
    	if(toUpsert.size() > 0) upsert toUpsert;
    }
}