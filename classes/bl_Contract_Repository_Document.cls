public class bl_Contract_Repository_Document {
    
    private static List<Contract_Repository_Document_Version__c> documentVersionsToDelete = new List<Contract_Repository_Document_Version__c>();
    
    public static void collectDocumentVersionsToDelete(List<Contract_Repository_Document__c> triggerOld){
        
        for(Contract_Repository_Document_Version__c docVersion : [Select Id, Marked_for_Deletion__c, Is_Deleted__c, Documentum_Id__c, Interface_Error__c from Contract_Repository_Document_Version__c where Document__c IN :triggerOld]){
            
            documentVersionsToDelete.add(docVersion);
        }
    }   
    
    public static void collectDocumentVersionsToDelete(List<Contract_Repository__c> triggerOld){
        
        for(Contract_Repository_Document_Version__c docVersion : [Select Id, Marked_for_Deletion__c, Is_Deleted__c, Documentum_Id__c, Interface_Error__c from Contract_Repository_Document_Version__c where Document__r.Contract_Repository__c IN :triggerOld]){
            
            documentVersionsToDelete.add(docVersion);
        }
    }    
    
    public static void setDocumentVersionsToDelete(){
        
        List<Contract_Repository_Document_Version__c> toUpdate = new List<Contract_Repository_Document_Version__c>();
        List<Contract_Repository_Document_Version__c> toDelete = new List<Contract_Repository_Document_Version__c>();
                
        for(Contract_Repository_Document_Version__c docVersion : documentVersionsToDelete){
            
            docVersion.Document__c = null;
            
            if(docVersion.Marked_for_Deletion__c == false){
                
                if(docVersion.Documentum_Id__c != null){    
                    
                    docVersion.Marked_for_Deletion__c = true;
                    toUpdate.add(docVersion);
                                    
                }else{
                    
                    if(docVersion.Interface_Error__c != null) toDelete.add(docVersion);
                    else{
                        
                        docVersion.Marked_for_Deletion__c = true;
                        toUpdate.add(docVersion);
                    }
                }
                
            }else{
                
                if(docVersion.Is_Deleted__c == true) toDelete.add(docVersion);
            }
        }
        
        if(toUpdate.size() > 0) update toUpdate;
        if(toDelete.size() > 0) delete toDelete;
    }
    //Method to stop the users from inserting or updating or deletion the documents if they don't have permission to update the Contract repostiry records
    Public static void manageDocumentPermissions (List<Contract_Repository_Document__c> documentLst, Boolean isInsert){
        List<Id> conRepoIds = new List<Id> ();
        Set<String> userWithEditAccess = new Set<String> ();
        Set<String> userWithUploadAccess = new Set<String> ();
               
        for(Contract_Repository_Document__c document: documentLst){
           conRepoIds.add(document.Contract_Repository__c); 
        }     
        
        for(UserRecordAccess access: [SELECT RecordId, HasReadAccess,HasEditAccess FROM UserRecordAccess WHERE UserId =:UserInfo.getUserId() AND RecordId IN :conRepoIds]){
            if(access.HasEditAccess)
            userWithEditAccess.add(access.RecordId+':'+UserInfo.getUserId());
        }
        for(Contract_Repository_Team__c team : [SELECT Id,Contract_Repository__c FROM Contract_Repository_Team__c WHERE Contract_Repository__c IN:conRepoIds 
                        AND Team_Member__c =: UserInfo.getUserId() AND Access_Level__c = 'Read Only - Upload Access to Documents']){
            userWithUploadAccess.add(team.Contract_Repository__c+':'+UserInfo.getUserId());
        }
        
        for(Contract_Repository_Document__c document: documentLst){            
            if(!userWithEditAccess.contains(document.Contract_Repository__c+':'+UserInfo.getUserId()) &&
                    (!isInsert || (!userWithUploadAccess.contains(document.Contract_Repository__c+':'+UserInfo.getUserId())))){
                    document.addError('You do not have the necessary permissions to perform this operation. Please check with your System Administrator.');
            }
        }
        
    }
}