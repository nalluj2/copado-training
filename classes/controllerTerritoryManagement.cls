public class controllerTerritoryManagement {
    Territory2[] TChildren = new Territory2[]{};
    public Territory_Level_Configuration__c tmaConfig;
    public List<Opportunity> oppListEmployee{get;set;}
    public List<Opportunity> oppListTerritory{get;set;}
    public boolean removeEmployee{get;set;}
    public boolean removeTerritory{get;set;} 
    public String terrId;
    public string propTerId {
        get { return propTerId; }
        set { propTerId = value; }
    }
     public boolean RemovePressed {        
         get { return RemovePressed; }        
         set { RemovePressed = value; }    
     }
     public boolean ValidId {        
         get { return ValidId; }        
         set { ValidId = value; }    
     }
    public boolean ApplyFilter {        
        get { return ApplyFilter; }        
        set { ApplyFilter = value; }    
    }     
    
    public boolean ApplyFilterAcc {        
        get { return ApplyFilterAcc; }        
        set { ApplyFilterAcc = value; }    
    } 

    public string EmpRecords {        
        get { return EmpRecords; }        
        set { EmpRecords = value; }    
    }
    public List<SelectOption> GetEmpRecSize() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('5','5'));
        options.add(new SelectOption('10','10'));
        options.add(new SelectOption('25','25'));
        options.add(new SelectOption('50','50'));
        options.add(new SelectOption('100','100')); 
        if(EmpRecords==null){
            EmpRecords='10';
        }   
        return options;
    }   

    public string AccRecords {        
        get { return AccRecords; }        
        set { AccRecords = value; }    
    }
    public List<SelectOption> GetAccRecSize() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('5','5'));
        options.add(new SelectOption('10','10'));
        options.add(new SelectOption('25','25'));
        options.add(new SelectOption('50','50'));
        options.add(new SelectOption('100','100')); 
        if(AccRecords==null){
            AccRecords='10';
        }   
        return options;
    }          
    public id FMUserId { get; set;}           
    public controllerTerritoryManagement(ApexPages.StandardController controller) {
    
        // Total number of records to be shown on page
        oppListEmployee=new List<Opportunity>();
        oppListTerritory=new List<Opportunity>();        
        removeTerritory=false;
        if(removeEmployee==true) {
            removeEmployee=false;
        }
           
        this.rowsToShow='5';       
        if(ApplyFilter!=true)
        {
            ApplyFilter=false;
        }                              
        if(ApplyFilterAcc!=true)
        {
            ApplyFilterAcc=false;
        }                              
        
        // Call Assigned Employeed & Accounts from constructor.
        terrId=ApexPages.currentPage().getParameters().get('Id');
        If (RemovePressed == true){
            RemovePressed = false;
        }        
        Territory2 detail;
        system.debug('Id on controllerTerritoryManagement '  + apexpages.currentpage().getUrl());
        if (terrId == null){

            Id userId = UserInfo.getUserId();
            String userTerrUID =[Select Territory_UID__c from User where id=:userId].Territory_UID__c;            
            Territory2 currTerrId = [Select Id, Territory2Type.DeveloperName from Territory2 where Territory_UID2__c =:userTerrUID];
            System.Debug('userTerrUID - ' + userTerrUID + ' currTerrId- ' + currTerrId);
            terrId = currTerrId.Id;
            System.Debug('Else Index');            
        }  
        // Check if user is allowed to enter this level of the territory
            Id userId = UserInfo.getUserId();
            String userTerrUID =[Select Territory_UID__c from User where id=:userId].Territory_UID__c;
            Territory2 UserTerr = [Select Id, Territory2Type.DeveloperName, Company__c from Territory2 where Territory_UID2__c =:userTerrUID];
      
            Territory2 selTerr =  [Select Id, Territory2Type.DeveloperName,ForecastUserId from Territory2 where Id =:terrId];
            if(selTerr.ForecastUserId!=null){
                FMUserId = selTerr.ForecastUserId;
            } 
            System.Debug('>>>>>FMUserId - ' + FMUserId);
            Territory_Level_Configuration__c[] TLGall = [select Name, Territory_Level__c from Territory_Level_Configuration__c];
            Map<String, Decimal> TLG = new Map<String, Decimal>();
            for(Territory_Level_Configuration__c T:TLGall){
                TLG.put(t.name,t.Territory_Level__c);
            }
            
            Decimal UsrLevel = TLG.get(UserTerr.Territory2Type.DeveloperName);
            Decimal selLevel = TLG.get(selTerr.Territory2Type.DeveloperName);
            String UsrId = UserTerr.Id;
            String parentId;
            String selId = selTerr.Id;
            ValidId = false;
            Do{
                Territory2 searchRec = [select ParentTerritory2Id, Territory2Type.DeveloperName  from Territory2 where id =:selId];
                System.debug('>>>>>>>>>>>>>>> UserId and Parent ' + UsrId + ' ' + searchRec.ParentTerritory2Id);

                if(UsrId == searchRec.ParentTerritory2Id || selId == UsrId){
                    ValidId = true;
                    break;
                }
                selLevel = TLG.get(searchRec.Territory2Type.DeveloperName);
                System.debug('>>>>>>>>>>>>>>> selLevel ' + selLevel);
                System.debug('>>>>>>>>>>>>>>> UsrLevel ' + UsrLevel);
                System.debug('>>>>>>>>>>>>>>> searchRec.Territory2Type ' + searchRec.Territory2Type.DeveloperName);
                if( selLevel > UsrLevel ){
                selId = searchRec.ParentTerritory2Id;
                }
            }
            While (selLevel > UsrLevel);
        
        
        propTerId=terrId;              
        AssignedEmployees();
        AssignedAccount();
    }
    public boolean propNoRec {
        get { return propNoRec; }
        set { propNoRec = value; }
    }

// Retrieve the configuration parameters
    public Territory_Level_Configuration__c gettmaConfig(){
        Territory2 terrName = [Select Territory2Type.DeveloperName,Company__c from Territory2 where id=:terrId];
        System.Debug('>>>>>terrId - ' + terrId + ' terrName - ' + terrName);
        tmaConfig = [Select Territory_Level__c, Territory_Edit_Button__c, Territory_Delete_Button__c, Territory_Abbreviation__c, Name, Message_No_Employees__c, Message_No_Child_Territories__c, Message_No_Accounts__c, Job_Title_vs__c, Add_Territory_Button__c, Add_Employees_Button__c, Add_Accounts_Button__c, Remove_Employees_chk__c, Change_Forecast_Manager_chk__c From Territory_Level_Configuration__c where name=:terrName.Territory2Type.DeveloperName and Company__c=:terrName.Company__c limit 1];     
        return tmaConfig;
    }


    // Get the detail information of the choosen Territory Node
    public Territory2 getTDetail(){



        Territory2 detail;
          if (terrId != null){

            detail = [Select Id, Name,Business_Unit__c, External_Key__c, Territory2Type.DeveloperName, Country_UID__c,Short_Description__c,Therapy_Groups_Text__c, ParentTerritory2Id  from Territory2 where Id=:terrId];
            }


        else{
            Id userId = UserInfo.getUserId();
            String userTerrUID =[Select Territory_UID__c from User where id=:userId].Territory_UID__c;
            detail = [Select Id, Name,Business_Unit__c, Territory2Type.DeveloperName, Country_UID__c,Short_Description__c,Therapy_Groups_Text__c, ParentTerritory2Id  from Territory2 where Territory_UID2__c =:userTerrUID];
        }    
        return detail;
    }
    public Territory2[] getTChildren(){


        Territory2 oneLevel;
        if (terrId != null){    



            system.debug('terrid - ' + terrId);



            oneLevel = [Select Id, Name, ParentTerritory2Id  from Territory2 where Id=:terrId];


        }
        else
        {
            Id userId = UserInfo.getUserId();
            String userTerrUID =[Select Territory_UID__c from User where id=:userId].Territory_UID__c;
            oneLevel = [Select Id, Name, ParentTerritory2Id  from Territory2 where Territory_UID2__c = :userTerrUID];
        }
        Territory2[] allChildren = [Select Id, Name,Business_Unit__c, Territory2Type.DeveloperName, Country_UID__c,Short_Description__c,Therapy_Groups_Text__c  from Territory2 where ParentTerritory2Id=:oneLevel.Id];
        System.debug('propAssignUserTable in Children - ' + propAssignUserTable);
        System.debug('propAssignUserMes in Children - '  + propAssignUserMes);
        if(allChildren.size()>0)
        {
            propNoRec=false;
        }        
        else
        {
            propNoRec=true;
        }
        return allChildren;
    }

    public String getCrumbs(){
        String crumbs ;


        Boolean levelFound = false;
        crumbs ='';
        Id userId = UserInfo.getUserId();
        String userTerrUID =[Select Territory_UID__c from User where id=:userId].Territory_UID__c;
        Territory2 currTerrId = [Select Id, Territory2Type.DeveloperName from Territory2 where Territory_UID2__c =:userTerrUID];
        
        Territory2 allChildren = new Territory2();
        Id tempId;
        if (terrId != null){
            tempId = terrId;



        }
        else{
            tempId = currTerrId.Id;
        }

            do{
                allChildren = [Select Id, Name,Business_Unit__c, Territory2Type.DeveloperName, Country_UID__c,Short_Description__c,Therapy_Groups_Text__c, ParentTerritory2Id  from Territory2 where Id=:tempId];
                
                If (levelFound){
                    crumbs = allChildren.Name + ' »' + crumbs;
                }
                else {
                crumbs = '<a href=../apex/TerritoryManagement?id=' + allChildren.Id + '> ' + allChildren.Name + '</a> »' + crumbs;
                }
                tempId = allChildren.ParentTerritory2Id;
                If (allChildren.Territory2Type.DeveloperName == currTerrId.Territory2Type.DeveloperName){
                    levelFound = true;
                }
                
                } while (allChildren.Territory2Type.DeveloperName != 'Region');
        
            // Remove the last >>
            integer crmbIndex = crumbs.lastIndexOf('</a>')  ;
            
            if (crmbIndex !=-1){
                crumbs = crumbs.substring(0,crmbIndex);
            }   
            integer firstHref = crumbs.lastIndexOf('<');
            integer lastHref = crumbs.lastIndexOf('>');
            String tempcrumb = crumbs.substring(firstHref, lastHref + 1);
            crumbs = crumbs.replace(tempcrumb, '');

        return crumbs;


    }
    public String getSiblings(){
        String siblings = '';

        Id userId = UserInfo.getUserId();
        String userTerrUID =[Select Territory_UID__c from User where id=:userId].Territory_UID__c;
        Territory2 userTerrRecord = [Select Id, Name,Business_Unit__c, Territory2Type.DeveloperName, Country_UID__c,Short_Description__c,Therapy_Groups_Text__c, ParentTerritory2Id  from Territory2 where Territory_UID2__c=:userTerrUID];
        
        Territory2 currRecord = new Territory2();
        if (terrId != null){

            Id tempId = terrId;

            currRecord = [Select Id, Name,Business_Unit__c, Territory2Type.DeveloperName, Country_UID__c,Short_Description__c,Therapy_Groups_Text__c, ParentTerritory2Id  from Territory2 where Id=:tempId];
        }
        else{ 
            currRecord = userTerrRecord;
               
        }    
            Territory2[] allSiblings = [Select Id, Name,Business_Unit__c, Territory2Type.DeveloperName, Country_UID__c,Short_Description__c,Therapy_Groups_Text__c, ParentTerritory2Id  from Territory2 where ParentTerritory2Id=:currRecord.ParentTerritory2Id];
            for (Territory2 s : allSiblings){
                if (s.Id != currRecord.id){ 
                    if(s.Territory2Type.DeveloperName != userTerrRecord.Territory2Type.DeveloperName){        
                         siblings = '<a href=../apex/TerritoryManagement?id=' + s.Id + '> ' + s.Name + '</a> »' + siblings;
                    }
                    else{
                        siblings = s.Name + ' » ' + siblings;
                }
                }
            }
            // Remove the last >>
            integer sibIndex = siblings.lastIndexOf('»')  ;
            if (sibIndex != -1){
                siblings = siblings.substring(0,sibIndex);
            }
        return siblings;      
    }
    public void doSearch(){}
    
    // ---------------------------------------------------------------------------
        public pageReference DelTerritory()
    {
        oppListTerritory=[select id,name,StageName,OwnerId,Territory2Id from 
                                    Opportunity where StageName not in ('Closed Won','Closed Lost') and Territory2Id=:terrId order by CreatedDate];
        System.debug('****oppListTerritory'+oppListTerritory);
        if(oppListTerritory.size()>0){
            removeTerritory=true;
        }
        PageReference newPage;
        if(removeTerritory==false){
            String newPageUrl = '/apex/confirmpage?id=' + terrId;        
            newPage = new PageReference(newPageUrl);
            newPage.setRedirect(true);
        }
        return newPage;  

//        PageReference newPage;
                     
    }    
    private String searchText;
    public String getSearchText() 
    {             
        return SearchText;               
    }    

    public void setSearchText(String searchText)
    { 
        this.searchText = searchText; 
    }
    
    
    // Assign User Functionality
    public boolean propAssignUserTable {
        get { return propAssignUserTable; }
        set { propAssignUserTable = value; }
    }    
    public boolean propAssignUserMes {
        get { return propAssignUserMes; }
        set { propAssignUserMes = value; }
    }    
    public ApexPages.StandardSetController ConEmp;        
    private list<user> AssignedEmployee = new  list<user>();
    public List<user> getAssignedEmployee() {
    
    
    return AssignedEmployee;
    
    }     
    public void AssignedEmployees() 
    {

        Territory2 detail;
        if(terrId == null){
            Id userId = UserInfo.getUserId();
            String userTerrUID =[Select Territory_UID__c from User where id=:userId].Territory_UID__c;
            Territory2 currTerrId = [Select Id, Territory2Type.DeveloperName from Territory2 where Territory_UID2__c =:userTerrUID];
            terrId = currTerrId.Id;
            System.Debug('Else Index');            
        }
        list<UserTerritory2Association> UT = [select userid from UserTerritory2Association where Territory2Id=:terrId and isActive=:True];
        System.Debug('UT - 0 + ' + UT.size() + ' terrId - ' + terrId );
        list<Id> terUserIds=new list<Id>();
        for(integer i=0;i<UT.size();i++)
        {
            System.Debug('userid - ' + UT[i].get('userid'));
            terUserIds.add(string.valueOf(UT[i].get('userid')));    
        }         
        
        System.Debug('searchText - ' + searchText); 
        if(terUserIds!=null)
        {
            string strQueryEmp = '';           
            if(searchText!=null)
            { 
                strQueryEmp = 'select id,Name,Job_Title_vs__c ,CountryOR__c,User_Business_Unit_vs__c from User where Name like \''+ string.escapeSingleQuotes(searchText.replace('*','%').trim())+'%\' and id in : terUserIds order by LastName limit 500';                
            }
            else
            {
                strQueryEmp = 'select id,Name,Job_Title_vs__c ,CountryOR__c,User_Business_Unit_vs__c from User where id in : terUserIds order by LastName limit 500';                            
            }
            ConEmp = new ApexPages.StandardSetController(Database.getQueryLocator(strQueryEmp));
            if(EmpRecords==null){
                EmpRecords='10';
            }               
            ConEmp.setPageSize(integer.valueof(EmpRecords));        
            AssignedEmployee=(List<User>)ConEmp.getRecords();                                                                      
        }            
        if(AssignedEmployee.size()>0)
        {
            propAssignUserTable=true;
            propAssignUserMes=false;
        }    
        else
        {
            propAssignUserTable=false;   
            propAssignUserMes=true;     
        }
        if(SearchText!='' && SearchText!=null)
        {
            ApplyFilter=true;
        }
        else
        {
            ApplyFilter=false;          
        }                    
        System.debug('propAssignUserTable - ' + propAssignUserTable);
        System.debug('propAssignUserMes - '  + propAssignUserMes);
    }    

    public String getTotalPagesEmp() {
       if(ConEmp!=null){                
            String s=null;
            double resSize=ConEmp.getResultSize();
            Integer pageNumber;
            if(ConEmp.getResultSize()>0){   
                pageNumber=ConEmp.getPageNumber();
            }
            else{
                pageNumber=0;
            }
            Integer pages=(Math.round(Math.ceil(resSize/ConEmp.getPageSize())));
            s=  'Page ' +pageNumber+ ' of ' + pages+'';
            return s; 
       }
       else{
        return null;
       }
            
    }
  
    public void previousEmp() {
       System.debug('prev');
       if(ConEmp!=null){
        //ConEmp.save();
        ConEmp.previous();
        AssignedEmployee = ConEmp.getRecords();
     }
    }

    public void nextEmp() {
   System.debug('next');
      if(ConEmp!=null){
        //ConEmp.save();
        ConEmp.next();
       AssignedEmployee = ConEmp.getRecords();
       }
    }


   public void firstEmp() {
   	System.debug('first');
    if(ConEmp!=null){
        //ConEmp.save();
        ConEmp.first();
         AssignedEmployee = ConEmp.getRecords();
        }
    }

    public void lastEmp() {
    	System.debug('last');
     if(ConEmp!=null){
        //ConEmp.save();
        ConEmp.last();
         AssignedEmployee = ConEmp.getRecords();
        }
    }

    public Boolean getHasNextEmp(){    
        if(ConEmp==null) return false;
        return ConEmp.getHasNext();
    }
    
    public Boolean getHasPreviousEmp(){    
        if(ConEmp==null)return false;
        return ConEmp.getHasPrevious();
    }  
    // End Account Pagination   
    
    //Delete Employee Functionality
    public String DelEmpId {get;set;}
    public void  DeleteEmployee(){
        UserTerritory2Association usrTerr = [select id from UserTerritory2Association where userid=:DelEmpId and territory2Id=:terrId] ;
        oppListEmployee=[select id,name,StageName,OwnerId,Territory2Id from 
                        Opportunity where StageName not in ('Closed Won','Closed Lost')  
                        and OwnerId=:DelEmpId and Territory2Id=:terrId order by CreatedDate];       
        if(oppListEmployee.size()>0){
           removeEmployee = true;        
        }             
        else{ 
            RemovePressed = true;
        }     
        DelEmpId = usrTerr.Id;        
        //RemovePressed = true;
    }  
        
    // Forcast Manager Functionaility
    String AllTerritoriesIds;
    public String getAllTerritoriesIds() { return AllTerritoriesIds; } 
    public void setAllTerritoriesIds(string value) { AllTerritoriesIds = value; }

    public boolean AssignedButtonPressed {        
        get { return AssignedButtonPressed; }        
        set { AssignedButtonPressed = value; }    
    }
        
    public String FMEmpId {get;set;}
    public PageReference  AssignFM(){
        set<id> currTer=new set<id>();
        currTer.add(terrId);
        set<id> AllTerrids = new set<id>();
        AllTerrids.add(terrId);
        Territory2 selTerr =  [Select Id, Territory2Type.DeveloperName,ForecastUserId from Territory2 where Id =:terrId];
        if(FMEmpId!=null && terrId!=null){
            System.Debug('FMEmpId - ' + FMEmpId);
            if(selTerr.Territory2Type.DeveloperName == 'Business_Unit'){
                list<Territory2> AllTers = new list<Territory2>();
                list<Territory2> allChildren = new list<Territory2>();
                    do{
                        allChildren = [Select Id, ParentTerritory2Id  from Territory2 where ParentTerritory2Id=:currTer];
                        currTer = new set<id>();
                        if(allChildren!=null && allChildren.size()>0){
                            for(Territory2 t:allChildren){
                                currTer.add(t.id);
                                AllTerrids.add(t.id);
                            }
                        }
                } while(allChildren!=null && allChildren.size()>0);
                AllTerritoriesIds = '';
                for(id tId:AllTerrids){
                    Territory2 TerCurrentTerrtory = new Territory2(id=tId, ForecastUserId=FMEmpId);
                    AllTers.add(TerCurrentTerrtory);
                    if(tID!=null){  
                        AllTerritoriesIds = AllTerritoriesIds + tId + ',';
                    }
                }
                AllTerritoriesIds = AllTerritoriesIds.substring(0,(AllTerritoriesIds.length()-1));

                if(AllTers.size()>0){
                    update AllTers;
                    AssignedButtonPressed = true;                   
                }                               
            } else {
                Territory2 TerCurrentTerrtory = new Territory2(id=terrId, ForecastUserId=FMEmpId);
                update TerCurrentTerrtory;              
            }
        }
        
        /*if(selTerr.ForecastUserId!=null){
            FMUserId = selTerr.ForecastUserId;
        }*/
        FMUserId = FMEmpId; 
        AssignedEmployees();         
        return null;
    }    
    

    
    // Remove Forcast Manager Functionaility    
    public String RMEmpId {get;set;}
    public PageReference  RemoveFM(){
        set<id> currTer=new set<id>();
        currTer.add(terrId);
        set<id> AllTerrids = new set<id>();
        AllTerrids.add(terrId);
        
        if(RMEmpId!=null && terrId!=null){
            Territory2 selTerr =  [Select Id, Territory2Type.DeveloperName,ForecastUserId from Territory2 where Id =:terrId];
            if(selTerr.Territory2Type.DeveloperName == 'Business_Unit'){
                list<Territory2> AllTers = new list<Territory2>();
                list<Territory2> allChildren = new list<Territory2>();
                    do{
                        allChildren = [Select Id, ParentTerritory2Id  from Territory2 where ParentTerritory2Id=:currTer];
                        currTer = new set<id>();
                        if(allChildren!=null && allChildren.size()>0){
                            for(Territory2 t:allChildren){
                                currTer.add(t.id);
                                AllTerrids.add(t.id);
                            }
                        }
                } while(allChildren!=null && allChildren.size()>0);
                for(id tId:AllTerrids){
                    Territory2 TerCurrentTerrtory = new Territory2(id=tId, ForecastUserId=null);
                    AllTers.add(TerCurrentTerrtory);
                }

                if(AllTers.size()>0){
                    update AllTers;
                }                                               
            } else {            
                System.Debug('RMEmpId - ' + RMEmpId);
                Territory2 TerCurrentTerrtory = new Territory2(id=terrId, ForecastUserId=null);
                update TerCurrentTerrtory;
            }
        }
        FMUserId = null;
        AssignedButtonPressed = false;
        AssignedEmployees();        
        return null;
    }    



//  Refresh the page
    public PageReference RemovedEmployee(){
        RemovePressed = false;
       /* String newPageUrl = '/apex/TerritoryManagement';
        System.debug('newPageUrl = ' + newPageUrl);        
        PageReference newPage = ApexPages.currentPage();
        newPage.setRedirect(true); */
        return null;               

    }

    // End Assign User Functionality
    // Assign Account Functionality
    public String DelAccId {get;set;}
    public PageReference DeleteAccount(){
        string Child = apexpages.currentpage().getUrl();
        integer inx  = Child.indexOf('id=');
        String terrId;
        Territory2 detail;
        if (inx != -1){
            //terrId = Child.substring(inx + 3  );
            terrId=ApexPages.currentPage().getParameters().get('id');            
            System.Debug('In Index');            
        }
        else{
            Id userId = UserInfo.getUserId();
            String userTerrUID =[Select Territory_UID__c from User where id=:userId].Territory_UID__c;
            Territory2 currTerrId = [Select Id, Territory2Type.DeveloperName from Territory2 where Territory_UID2__c =:userTerrUID];
            terrId = currTerrId.Id;
            System.Debug('Else Index');            
        }
        
        
        
        ObjectTerritory2Association[] DelAcc = [select id from ObjectTerritory2Association where ObjectID=:DelAccId and Territory2Id=:terrId];
        if(DelAcc.size()>0)
        {
          delete DelAcc;
        }
        
        //return null;
        String newPageUrl = '/apex/TerritoryManagement?id=' + terrId;        
        PageReference newPage = new PageReference(newPageUrl);
        newPage.setRedirect(true);
        return newPage;               
    }    
    private String searchText1;
    public String getSearchText1() 
    {             
        return searchText1;               
    }    

    public void setSearchText1(String searchText1)
    { 
        this.searchText1 = searchText1; 
    }  
    public boolean propAssignAccTable {
        get { return propAssignAccTable; }
        set { propAssignAccTable = value; }
    }    
    public boolean propAssignAccMes {
        get { return propAssignAccMes; }
        set { propAssignAccMes = value; }
    }          

    public ApexPages.StandardSetController Con;    
    public String rowsToShow;
    
    private list<Account> AssignAccount = new  list<Account>(); 
    public List<Account> getAssignAccount() {return AssignAccount;}     
    public void AssignedAccount() 
    {
        string Child = apexpages.currentpage().getUrl();
        integer inx  = Child.indexOf('id=');
        String terrId;
        Territory2 detail;
        if (inx != -1){
             //terrId = Child.substring(inx + 3  );
            terrId=ApexPages.currentPage().getParameters().get('id');            

        }
        else{
            Id userId = UserInfo.getUserId();
            String userTerrUID =[Select Territory_UID__c from User where id=:userId].Territory_UID__c;
            Territory2 currTerrId = [Select Id, Territory2Type.DeveloperName from Territory2 where Territory_UID2__c =:userTerrUID];
            terrId = currTerrId.Id;
        }
        System.Debug('AssignedAccount - ' + terrId);        
       
        if(terrId != null)
        {
                 
            list<ObjectTerritory2Association> AccShare = [select ObjectId from ObjectTerritory2Association where Territory2Id  = :terrId];            
            if(AccShare.size()>0)
            {
                System.Debug('AccShare - ' + AccShare.size());        
                list<Id> accIds=new list<Id>();
                for(integer i=0;i<AccShare.size();i++)
                {
                    accIds.add(string.valueOf(AccShare[i].ObjectId));    
                }         
                System.Debug('accIds - ' + accIds.size()); 
                string strQuery = '';   
                if(searchText1!=null)
                {                              
                    //AssignAccount = [select name,Account_Postal_Code__c,Account_City__c,SAP_ID__c from Account where Name like :searchText1 +'%' and id in : accIds limit 500];
                    strQuery = 'select name,Account_Postal_Code__c,Account_City__c,SAP_ID__c,Account_Country_vs__c from Account where Name like \''+string.escapeSingleQuotes(searchText1.replace('*','%').trim()) +'%\' and id in : accIds order by name limit 500';                     
                }
                else
                {
                    //AssignAccount = [select name,Account_Postal_Code__c,Account_City__c,SAP_ID__c from Account where id in : accIds limit 500];
                    strQuery = 'select name,Account_Postal_Code__c,Account_City__c,SAP_ID__c, Account_Country_vs__c from Account where id in : accIds order by name limit 500';                                     
                }  
                Con = new ApexPages.StandardSetController(Database.getQueryLocator(strQuery));
                if(AccRecords==null){
                    AccRecords='10';
                }                               
                Con.setPageSize(integer.valueof(AccRecords));        
                AssignAccount=(List<Account>)Con.getRecords();                                          
            }                
        }
        if(AssignAccount.size()>0)
        {
            propAssignAccTable=true;
            propAssignAccMes=false;
        }    
        else
        {
            propAssignAccTable=false;   
            propAssignAccMes=true;     
        } 
        if(SearchText1!='' && SearchText1!=null)
        {
            ApplyFilterAcc=true;
        }
        else
        {
         ApplyFilterAcc=false;          
        }                                           
    }    
    
    // Start Account Pagination
    public String getTotalPages() {
       if(con!=null){       
            String s=null;
            double resSize=con.getResultSize();
            Integer pageNumber;
            if(con.getResultSize()>0){   
                pageNumber=con.getPageNumber();
            }
            else{
                pageNumber=0;
            }
            Integer pages=(Math.round(Math.ceil(resSize/con.getPageSize())));
            s=  'Page ' +pageNumber+ ' of ' + pages+'';
            return s;
       }
       else{
        return null;
       }
                
    }
  
    public void previous() {
       System.debug('prev');
       if(con!=null){
        //con.save();
        con.previous();
        AssignAccount = con.getRecords();
     }
    }

    public void next() {
    	System.debug('next');
      if(con!=null){
        //con.save();
        con.next();
        AssignAccount = con.getRecords();
       }
    }


   public void first() {
   	System.debug('first');
    if(con!=null){
        //con.save();
        con.first();
         AssignAccount = con.getRecords();
        }
    }

    public void last() {
    	System.debug('last');
     if(con!=null){
        //con.save();
        con.last();
         AssignAccount = con.getRecords();
        }
    }

    public Boolean getHasNext(){    
        if(con==null) return false;
        return con.getHasNext();
    }
    
    public Boolean getHasPrevious(){    
        if(con==null)return false;
        return con.getHasPrevious();
    }  
    // End Account Pagination    
    
}