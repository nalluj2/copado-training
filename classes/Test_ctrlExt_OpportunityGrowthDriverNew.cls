@isTest
private class Test_ctrlExt_OpportunityGrowthDriverNew {
	
	@testSetup
	private static void setData() {
	
		Company__c cmpny = new Company__c();		
        cmpny.name='Europe';
        cmpny.CurrencyIsoCode = 'EUR';
        cmpny.Current_day_in_Q1__c=56;
        cmpny.Current_day_in_Q2__c=34; 
        cmpny.Current_day_in_Q3__c=5; 
        cmpny.Current_day_in_Q4__c= 0;   
        cmpny.Current_day_in_year__c =200;
        cmpny.Days_in_Q1__c=56;  
        cmpny.Days_in_Q2__c=34;
        cmpny.Days_in_Q3__c=13;
        cmpny.Days_in_Q4__c=22;
        cmpny.Days_in_year__c =250;
        cmpny.Company_Code_Text__c = 'T35';
		insert cmpny;
		
		Business_Unit_Group__c bug =  new Business_Unit_Group__c();
        bug.Master_Data__c =cmpny.id;
        bug.name='CVG';      
        insert bug;
		
		Business_Unit__c bu =  new Business_Unit__c();
        bu.Company__c =cmpny.id;
        bu.name='CVG BU';
        bu.Business_Unit_Group__c = bug.Id;
        bu.Account_Plan_Activities__c=true;        
        insert bu;
        
        Sub_Business_Units__c sbu = new Sub_Business_Units__c();
        sbu.name='CVG sBU';
        sbu.Business_Unit__c=bu.id;
		insert sbu;
						
		Growth_Driver__c sbuDriver = new Growth_Driver__c();		
		sbuDriver.Growth_Driver__c = 'test SBU';
		sbuDriver.Description__c = 'test explanation';		
		sbuDriver.Active__c = true;
		sbuDriver.Sub_Business_Unit__c = sbu.Id;
		insert sbuDriver;
		
		Growth_Driver__c bugDriver = new Growth_Driver__c();	
		bugDriver.Growth_Driver__c = 'test BUG';	
		bugDriver.Description__c = 'test explanation';		
		bugDriver.Active__c = true;
		bugDriver.Business_Unit_Group__c = bug.Id;
		insert bugDriver;		
		
		User currentUser = new User(Id = UserInfo.getUserId());
		currentUSer.Company_Code_Text__c = 'T35';
		update currentUser;
	}
	
	private static testmethod void testCreateOpportunitySBUGrowthDriver(){
		
		Account testAccount = new Account();
		testAccount.Name = 'CVG Test Account';
		insert testAccount;
		
		Opportunity sbuOpp = new Opportunity();
		sbuOpp.AccountId = testAccount.Id;
		sbuOpp.Name = 'Test SBU Opportunity';
		sbuOpp.StageName = 'Open';
		sbuOpp.CloseDate = Date.today().addDays(15);
		sbuOpp.Sub_Business_Unit_Id__c = [Select Id from Sub_Business_Units__c].Id;				
		insert sbuOpp;
		
		Test.startTest();
		
		Opportunity_Growth_Driver__c oppGrowthDriver = new Opportunity_Growth_Driver__c();
		oppGrowthDriver.Opportunity__c = sbuOpp.Id;
		
		ApexPages.StandardController sc = new ApexPages.StandardController(oppGrowthDriver);
		ctrlExt_OpportunityGrowthDriverNew con = new  ctrlExt_OpportunityGrowthDriverNew(sc);
		
		System.assertEquals(' WHERE Active__c = true AND Sub_Business_Unit__c = \'' + sbuOpp.Sub_Business_Unit_Id__c + '\'', con.growthDriverFilter);
	} 
	
	private static testmethod void testCreateOpportunityBUGGrowthDriver(){
		
		Account testAccount = new Account();
		testAccount.Name = 'CVG Test Account';
		insert testAccount;
		
		Opportunity bugOpp = new Opportunity();
		bugOpp.AccountId = testAccount.Id;
		bugOpp.Name = 'Test BUG Opportunity';
		bugOpp.StageName = 'Open';
		bugOpp.CloseDate = Date.today().addDays(15);
		bugOpp.Business_Unit_Group_Id__c = [Select Id from Business_Unit_Group__c].Id;				
		insert bugOpp;
		
		Test.startTest();
		
		Opportunity_Growth_Driver__c oppGrowthDriver = new Opportunity_Growth_Driver__c();
		oppGrowthDriver.Opportunity__c = bugOpp.Id;
		
		ApexPages.StandardController sc = new ApexPages.StandardController(oppGrowthDriver);
		ctrlExt_OpportunityGrowthDriverNew con = new  ctrlExt_OpportunityGrowthDriverNew(sc);
		
		System.assertEquals(' WHERE Active__c = true AND Business_Unit_Group__c = \'' + bugOpp.Business_Unit_Group_Id__c + '\'', con.growthDriverFilter);
	} 
	
	    
}