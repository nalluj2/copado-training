/**
    Test_Utils CLass
    This class contains methods used over all unit tests as local testing record creations
*/
@isTest
public class Test_Utils {    
    /**
     * Method createOpp(Id rtId, String name, String stName, Date clsDate, Id pbId)
     * @param rtId: record type id of the opportunity
     * @param name: name of the opportunity
     * @param stName: stage name of the opportunity
     * @param clsDate: close date of the opportunity
     * @param pbId: pricebook id of the opportunity
     * @return opp: returns the created opportunity
     */
    public static Opportunity createOpp(Id rtId, String name, String stName, Date clsDate, Id pbId) {
        // Create the opportunity and populate its fields
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = rtId;
        opp.Name = name;
        opp.StageName = stName;
        opp.CloseDate = clsDate;
        opp.Pricebook2Id = pbId;
        opp.CurrencyIsoCode = 'USD';

        // Insert the opportunity in Salesforce and save the result as a variable
        //Database.saveResult sr = Database.insert(opp, false);
        insert opp;

        // Return the created opportunity
        return opp;
     }
            
    /**
     * Method createProduct2(String name, Boolean activity)
     * @param name: name of the product2
     * @param activity: sets if the product2 is active
     * @return prod: returns the created product2
     */    
    public static Product2 createProduct2(String name, Boolean activity) {
        // Create the product and populate its fields
        Product2 prod = new Product2();
        prod.Name = name;
        prod.IsActive = true;
        prod.CurrencyIsoCode = 'USD';

        // Insert the product2 in Salesforce and save the result as a variable
        insert prod;

        // Return the created product2
        return prod;
     }

    /**
     * Method createOppLineItem(Id oliId, id oppId, Integer quantity, Integer unitPrice)
     * @param oliId: id of the OpportunityLineItem
     * @param oppId: id of the Opportunity related to the OpportunityLineItem
     * @param quantity: quantity of the OpportunityLineItem
     * @param unitPrice: unit price of the OpportunityLineItem
     * @return oli: returns the created OpportunityLineItem
     */
    public static OpportunityLineItem createOppLineItem(Id opportunityId,Id pbEntryId, Integer quantity, Integer unitPrice, Integer IHSCost) {
        // Create the OpportunityLineItem and populate its fields
        OpportunityLineItem oli = new OpportunityLineItem();
        //oli.Product2Id = prodId;
        oli.PricebookEntryId = pbEntryId;
        oli.Opportunityid = opportunityId;
        oli.Quantity = quantity;
        oli.UnitPrice = unitPrice;

        // Insert the OpportunityLineItem in Salesforce and save the result as a variable
        insert oli;

        // Return the created OpportunityLineItem
        return oli;
     }
}