global class ba_SynchronizationService_Knowledge implements Database.Batchable<Sync_Record_Change__c>, Database.Stateful, Database.AllowsCallouts {
	
	final static public Integer BATCH_SIZE = 25;

	//Mechanism to detect limit errors and notify admins
	private Integer totalBatches;
	private Integer processedBatches;
	
	private bl_SynchronizationService_Utils.SessionInfo sessionInfo;
	
	//List of successfully processed records
	private List<Sync_Record_Change__c>	success = new List<Sync_Record_Change__c>();
	
	//List of errors occured
	private List<String> errors = new List<String>();
				
	global List<Sync_Record_Change__c> start(Database.BatchableContext BC){
		
		sessionInfo = bl_SynchronizationService_Utils.getTargetOrgSession();
      	
      	totalBatches = 0;
      	processedBatches = 0;
     	
     	List<SObject> records;
     	     	    
    	try {
    		
    		String query = 'SELECT Id, Record_Id__c, Secondary_Record_Id__c, Action__c, Status__c, Record_Object_Type__c from Sync_Record_Change__c where Status__c = \'New\' AND Record_Object_Type__c IN (\'KnowledgeArticle\',\'Index_Page__kav\',\'Long_Text__kav\',\'Procedure__kav\',\'Table_of_Contents__kav\',\'Text_Article__kav\') ORDER BY CreatedDate ASC ';		
    			
    		records = bl_SynchronizationService_Utils.executeQuery(query, null, sessionInfo);
    			
    	}catch(Exception e) {
        	
        	errors.add(e.getMessage() + ' - ' + e.getStacktraceString());
        	
        	//If we get an error during the call, we stop here. The last updated datetime will not be updated, so the next time this batch triggers it will retry from the same point
        	return new List<Sync_Record_Change__c>();
    	}
     	
      	if(records.size() > 0){
      		
      		Decimal div = ((Decimal) records.size()) / BATCH_SIZE;
			totalBatches = (Integer)div.round(System.RoundingMode.UP);
      	}
      	      	
      	return (List<Sync_Record_Change__c>) records;
   	}
   	
   	global void execute(Database.BatchableContext BC, List<Sync_Record_Change__c> recordChanges){
        
        Map<String, String> idMapping = new Map<String, String>();
                
        List<String> indexPageArticleIds = new List<String>();
        List<String> longTextArticleIds = new List<String>();
        List<String> procedureArticleIds = new List<String>();
        List<String> textArticleIds = new List<String>();
        List<String> tocArticleIds = new List<String>();
        
        for(Sync_Record_Change__c recordChange : recordChanges){
        	        	
        	idMapping.put(recordChange.Record_Id__c, null);
        	        	
        	if(recordChange.Action__c == 'Update' ){
        		        		
        		if(recordChange.Record_Object_Type__c == 'Index_Page__kav' ) indexPageArticleIds.add(recordChange.Secondary_Record_Id__c);
        		else if(recordChange.Record_Object_Type__c == 'Long_Text__kav' ) longTextArticleIds.add(recordChange.Secondary_Record_Id__c);
        		else if(recordChange.Record_Object_Type__c == 'Procedure__kav' ) procedureArticleIds.add(recordChange.Secondary_Record_Id__c);
        		else if(recordChange.Record_Object_Type__c == 'Text_Article__kav' ) textArticleIds.add(recordChange.Secondary_Record_Id__c);     
        		else if(recordChange.Record_Object_Type__c == 'Table_of_Contents__kav' ) tocArticleIds.add(recordChange.Secondary_Record_Id__c);        		  		
        	}	
        }      
        
        //Build the Map with the Id translation from US to INTL. The query will only return values for Articles and Versions that were sync before.        
        for(Sync_Record_Id_Mapping__c idMap : [Select External_Id__c, Name from Sync_Record_Id_Mapping__c where External_Id__c IN :idMapping.keySet() AND Object_Type__c = 'KnowledgeArticle']){
        	
        	idMapping.put(idMap.External_Id__c, idMap.Name);        	
        }
        
        Map<String, SObject> articleVersions = new Map<String, SObject>();
        
        try{
        	//Text Article
	        if(textArticleIds.size() > 0){
	                
		        //Get Text Article Version records
		        String query = 	'Select Article_Text__c, Id, Intranet_URL__c, Language, Summary, Title, UrlName, ValidationStatus,' + 
		        				'(Select DataCategoryGroupName,DataCategoryName FROM DataCategorySelections ) ' +
		        				'FROM Text_Article__kav where Language = \'en_US\' AND PublishStatus = \'Online\' AND Id IN (\'' + String.join(textArticleIds, '\',\'') + '\')';        
		        
		        List<SObject> records = executeQuery(query);
		        		        
		     	for(SObject record : records){
		     		articleVersions.put(record.Id, record);
		     	}     	  
	        }
	        
	        //Long Text
	        if(longTextArticleIds.size() > 0){
	                
		        //Get Text Article Version records
		        String query = 	'Select Article_Text__c, Article_Text_Continued__c, Id, Intranet_URL__c, Language, Summary, Title, UrlName, ValidationStatus,' +
		        				'(Select DataCategoryGroupName,DataCategoryName FROM DataCategorySelections ) ' +
		        				' from Long_Text__kav where Language = \'en_US\' AND PublishStatus = \'Online\' AND Id IN (\'' + String.join(longTextArticleIds, '\',\'') + '\')';        
		        
		        List<SObject> records = executeQuery(query);
		        		        
		     	for(SObject record : records){
		     		articleVersions.put(record.Id, record);
		     	}     	  
	        }
	        
	        //Procedure
	        if(procedureArticleIds.size() > 0){
	                
		        //Get Text Article Version records
		        String query = 	'Select Id, Instructions__c, Option_1_Description__c, Option_1_Title__c, Option_2_Description__c, Option_2_Title__c,' + 
		        				'Option_3_Description__c, Option_3_Title__c, Option_4_Description__c, Option_4_Title__c, Option_5_Description__c, Option_5_Title__c,' + 
		        				'Language, Summary, Title, UrlName, ValidationStatus,' +
		        				'(Select DataCategoryGroupName,DataCategoryName FROM DataCategorySelections ) ' +
		        				' from Procedure__kav where Language = \'en_US\' AND PublishStatus = \'Online\' AND Id IN (\'' + String.join(procedureArticleIds, '\',\'') + '\')';        
		        
		        List<SObject> records = executeQuery(query);
		        		        
		     	for(SObject record : records){
		     		articleVersions.put(record.Id, record);
		     	}     	  
	        }
	        
	        //Index Page
	        if(indexPageArticleIds.size() > 0){
	                
		        //Get Text Article Version records
		        String query = 	'Select Category__c, Id, Language, Summary, Title, UrlName, ValidationStatus,' +
		        				'(Select DataCategoryGroupName,DataCategoryName FROM DataCategorySelections ) ' +
		        				' from Index_Page__kav where Language = \'en_US\' AND PublishStatus = \'Online\' AND Id IN (\'' + String.join(indexPageArticleIds, '\',\'') + '\')';        
		        
		        List<SObject> records = executeQuery(query);
		        		        
		     	for(SObject record : records){
		     		articleVersions.put(record.Id, record);
		     	}     	  
	        }
	        
	        //Table of Content
	        if(tocArticleIds.size() > 0){
	                
		        //Get Text Article Version records
		        String query = 	'Select Id, Language, Summary, Title, UrlName, ValidationStatus,' +
		        				'(Select DataCategoryGroupName,DataCategoryName FROM DataCategorySelections ) ' +
		        				' from Table_of_Contents__kav where Language = \'en_US\' AND PublishStatus = \'Online\' AND Id IN (\'' + String.join(tocArticleIds, '\',\'') + '\')';        
		        
		        List<SObject> records = executeQuery(query);
		        		        
		     	for(SObject record : records){
		     		articleVersions.put(record.Id, record);
		     	}     	  
	        }
	           
	    }catch(Exception e){
		        	
        	errors.add(e.getMessage() + ' - ' + e.getStacktraceString());
    	
    		//If we get an error during the call, we stop here. The last updated datetime will not be updated, so the next time this batch triggers it will retry from the same point
    		return;
        }	
     	
		//We have all the info we need to process the notifications
		for(Sync_Record_Change__c recordChange : recordChanges){
			
			Id articleId = idMapping.get(recordChange.Record_Id__c);//Translate Ids
									
			if(recordChange.Action__c == 'Delete' ){
												
				try{
					//If we receive an Article which was not sync before there is no action to perform
					if(articleId != null && isOnline(articleId)) KbManagement.PublishingService.archiveOnlineArticle( articleId, null);//Archive Article
					
					success.add(recordChange);
					
				}catch(Exception e){
					errors.add(e.getMessage() + ' - ' + e.getStacktraceString());
				}
								
			}else{
				
				SObject articleVersion = articleVersions.get(recordChange.Secondary_Record_Id__c);
				
				//If the query did not find the specified Version is because it is not Published anymore. In such case no action is required
				if(articleVersion == null) success.add(recordChange);
				else{
					
					SavePoint sp = Database.setSavePoint();
					
					try{										
						
						//If the Article already exists we need to create a new Draft, otherwise we create a new one
						if(articleId != null){
							
							String draftId;
							
							if(isOnline(articleId)) draftId = KbManagement.PublishingService.editOnlineArticle(articleId, false);
							else draftId = KbManagement.PublishingService.editArchivedArticle(articleId);
							
							articleVersion.Id = draftId;
							
							update articleVersion;
							
							//Delete the Categories from the previous version. Later on we will insert the Categories for the new version
							String categoryName = recordChange.Record_Object_Type__c.replace('__kav', '__DataCategorySelection');
							
							delete Database.query('SELECT Id from ' + categoryName + ' WHERE ParentId = \'' + articleVersion.Id + '\'');
							
						}else{
							
							articleVersion.Id = null;							
							insert articleVersion;	
							
							//Update the mapping table for the new Article created
							KnowledgeArticleVersion version = [Select Id, KnowledgeArticleId from KnowledgeArticleVersion where Language = 'en_US' AND PublishStatus = 'Draft' AND Id = :articleVersion.Id];
							
							Sync_Record_Id_Mapping__c articleMapping = new Sync_Record_Id_Mapping__c();
							articleMapping.Name = version.KnowledgeArticleId;
							articleMapping.External_Id__c = recordChange.Record_Id__c;
							articleMapping.Object_Type__c = 'KnowledgeArticle';
							
							articleId = version.KnowledgeArticleId;
							
							idMapping.put(recordChange.Record_Id__c, articleId);						
							upsert articleMapping External_Id__c;						
						}
																	
						System.debug('Article Version Id => ' + articleVersion.Id);
						
						List<SObject> articleVersionCategories = articleVersion.getSObjects('DataCategorySelections');
						
						for(SObject articleVersionCategory : articleVersionCategories){
							
							articleVersionCategory.Id = null;
							articleVersionCategory.put('ParentId', articleVersion.Id);
						}
						
						if(articleVersionCategories.size() > 0){
							
							insert articleVersionCategories;
						}
						
						//Publish the new version (even if in the source Org it was only an update of the online version)
						KbManagement.PublishingService.publishArticle( articleId, true);
																		
						success.add(recordChange);
						
					}catch(Exception e){
						
						Database.rollback(sp);
						
						errors.add(e.getMessage() + ' - ' + e.getStacktraceString());
					}
				}					
			}			
		}		
		
		processedBatches++ ;//Batch processed successfully without any limit error
   	}
   	
   	private Boolean isOnline(String articleId){
   		
		KnowledgeArticle article = [SELECT ArchivedDate FROM KnowledgeArticle where Id = :articleId];
		
		return article.ArchivedDate == null;	
   	} 
   	
   	private List<SObject> executeQuery(String query){
   		
   		List<SObject> records;
   		
   		try{
   			
   			records = bl_SynchronizationService_Utils.executeQuery(query, null, sessionInfo);
   		
   		}catch(Exception ex) {
	    			
			if(ex.getMessage().contains('INVALID_SESSION_ID')){
				
				sessionInfo = bl_SynchronizationService_Utils.getTargetOrgSession();
				
				records = bl_SynchronizationService_Utils.executeQuery(query, null, sessionInfo);
				
			}else throw ex;
		}
   		
   		return records;
   	}	
   	
   	//If we go any error during the execution, we inform the 'watchers' about it
   	global void finish(Database.BatchableContext BC){
   		
   		if(processedBatches != totalBatches && errors.size() == 0){
   			
   			errors.add('An unhandle exception has occurred in at least one of the batches. Check the batch execution for more information. Batch Id = '+ bc.getJobId());
   		}
   		
   		if(success.size() > 0){
   			
   			try {
	    		
	    		RecordChangeNotificationRequest syncRequest = new RecordChangeNotificationRequest();
	    		syncRequest.notifications = success;
	    		
	    		try{
	    			
	    			bl_SynchronizationService_Utils.executeService('/services/apexrest/RecordChangeNotificationProcessed', 'POST', JSON.serialize(syncRequest), null, sessionInfo);
	    		
	    		}catch(Exception ex) {
	    			
	    			if(ex.getMessage().contains('INVALID_SESSION_ID')){
	    				
	    				sessionInfo = bl_SynchronizationService_Utils.getTargetOrgSession();
	    				
	    				bl_SynchronizationService_Utils.executeService('/services/apexrest/RecordChangeNotificationProcessed', 'POST', JSON.serialize(syncRequest), null, sessionInfo);
	    				
	    			}else throw ex;
	    		}	
	    			
	    	} catch(Exception e) {
	        	
				errors.add('Error during Rest call: ' + e.getMessage() + ' - ' + e.getStacktraceString());
	    	}  			
   		}
   		
   		if(errors.size() > 0){
   			   			
   			bl_SynchronizationService_Utils.notifySyncErrors(errors);   			
   		}
   	}  
   	
   	//Model classes for the requests and responses. To be used by the JSON serialize/deserialize
   	global class RecordChangeNotificationRequest{
		global List<Sync_Record_Change__c> notifications {get;set;}
	}
}