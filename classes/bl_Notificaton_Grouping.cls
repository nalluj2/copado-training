public class bl_Notificaton_Grouping {
    
    public static void calculateInboundStatus(List<Notification_Grouping__c> triggerNew){
    	
    	Map<Id, Notification_Grouping__c> groupingDetails = new Map<Id, Notification_Grouping__c>([Select Id, (Select Id, Status__c, Internal_ID__c, Request__c from Outbound_Messages__r ORDER BY CreatedDate DESC, Id DESC LIMIT 1) from Notification_Grouping__c where Id IN :triggerNew]);
    	
    	for(Notification_Grouping__c grouping : triggerNew){
    		
    		Notification_Grouping__c details = groupingDetails.get(grouping.Id);
    		
    		if(details.Outbound_Messages__r.size() > 0){
    			
    			Outbound_Message__c om = details.Outbound_Messages__r[0];
    			
    			grouping.Last_Inbound_Message__c = om.Id;
    			
    			if(om.Status__c == 'TRUE') grouping.Inbound_Status__c = 'Success';
    			else grouping.Inbound_Status__c = 'Failure';
    			
    			if(grouping.Internal_ID__c == null && om.Internal_ID__c != null) grouping.Internal_ID__c = om.Internal_ID__c;
    			
    			RequestMessage req = (RequestMessage) JSON.deserialize(om.Request__c, RequestMessage.class);			
				
				if(req.SALES_ORG_TEXT != null) grouping.Country__c = req.SALES_ORG_TEXT;
				else grouping.Country__c = req.MNWRKCNTR_SHORT_TEXT;
				
				if(req.SAP_NOTIFICATION_TYPE != null) grouping.Related_Record_Type__c = req.SAP_NOTIFICATION_TYPE;
				else grouping.Related_Record_Type__c = req.SAP_ORDER_TYPE;
				
				if(req.ORDER_SUBSTATUS != null) grouping.Related_Record_Status__c = req.ORDER_SUBSTATUS;
				else grouping.Related_Record_Status__c = req.STATUS;
				
				if(req.MAIN_WORK_CENTER != null) grouping.Related_Record_Work_Center__c = req.MAIN_WORK_CENTER;
				
				if(req.TECHNICIAN != null) grouping.Related_Record_Technician__c = req.TECHNICIAN + ' / ' + req.TECHNICIAN_TEXT;

				if(req.MILESTONE_STATUS != null) grouping.Related_System_Status__c = req.MILESTONE_STATUS;

			}else{
    			
    			grouping.Inbound_Status__c = 'Success';
    			grouping.Last_Inbound_Message__c = null;
    		}
    	}
    }
    
    public static void calculateOutboundStatus(List<Notification_Grouping__c> triggerNew){
    	
    	Map<Id, Notification_Grouping__c> groupingDetails = new Map<Id, Notification_Grouping__c>([Select Id, (Select Id, Record_ID__c, SFDC_Object_Name__c, WebServiceName__c, Status__c from NotificationSAPLogs__r ORDER BY CreatedDate DESC, Id DESC) from Notification_Grouping__c where Id IN :triggerNew]);
    	
    	Set<String> sOrderIds = new Set<String>();
    	    	    	
    	for(Notification_Grouping__c grouping : triggerNew){
    		
    		if(grouping.Scope__c == 'SVMXC__Service_Order__c') sOrderIds.add(grouping.External_ID__c);	
    	}
    	
    	Map<String, SVMXC__Service_Order__c> sOrderMap = new Map<String, SVMXC__Service_Order__c>();
    	
    	if(sOrderIds.size() > 0){
    		
    		for(SVMXC__Service_Order__c sOrder : [Select Id, SVMXC__Order_Status__c, SVMX_SAP_Service_Order_No__c from SVMXC__Service_Order__c where SVMX_SAP_Service_Order_No__c IN :sOrderIds]){
    			
    			sOrderMap.put(sOrder.SVMX_SAP_Service_Order_No__c, sOrder);
    		}    		
    	}
    	
    	for(Notification_Grouping__c grouping : triggerNew){
    		
    		Notification_Grouping__c details = groupingDetails.get(grouping.Id);
    		
    		if(details.NotificationSAPLogs__r.size() > 0){
    			
    			NotificationSAPLog__c notif = getLastNotification(details.NotificationSAPLogs__r);
    			
    			grouping.Last_Outbound_Notification__c = notif.Id;
    			grouping.Internal_ID__c = notif.Record_ID__c;
    			grouping.Outbound_Status__c = calculateStatus(notif, sOrderMap.get(grouping.External_ID__c));
    			   			
    		}else{
    			
    			grouping.Outbound_Status__c = 'Success';
    			grouping.Last_Outbound_Notification__c = null;
    		}
    	}	
    }
    
    private static String calculateStatus(NotificationSAPLog__c lastNotif, SVMXC__Service_Order__c sOrder){
    	
    	if(lastNotif.Status__c == 'COMPLETED'){
    		
    		if(lastNotif.SFDC_Object_Name__c == 'SVMXC__Service_Order__c' && sOrder != null && sOrder.SVMXC__Order_Status__c == 'Job Completed' && lastNotif.WebServiceName__c != 'ServiceOrderComplete_NotificationSAP') return 'Failure';
    		
    		return 'Success';
    		
    	}
    	
    	return 'Failure';
    }
    
    private static NotificationSAPLog__c getLastNotification(List<NotificationSAPLog__c> notifications){
    	
    	for(NotificationSAPLog__c notif : notifications){
    		
			if(notif.Status__c != 'SKIPPED') return notif;
    	}
    	
    	return notifications[0];
    }
    
    private class RequestMessage{
    	    	
    	public String MNWRKCNTR_SHORT_TEXT {get; set;}
    	public String SALES_ORG_TEXT {get; set;}
    	public String SAP_NOTIFICATION_TYPE {get; set;}
    	public String SAP_ORDER_TYPE {get; set;}
    	public String STATUS {get; set;}
    	public String ORDER_SUBSTATUS {get; set;}
    	public String MAIN_WORK_CENTER {get; set;}
    	public String TECHNICIAN {get; set;}
    	public String TECHNICIAN_TEXT {get; set;}
		public String MILESTONE_STATUS {get; set;}
    }
}