@isTest public class clsTestData_AccountPerformance {

	public static Integer iRecord_AccountPerformance = 1;
	public static Account_Performance__c oMain_AccountPerformance;
	public static List<Account_Performance__c> lstAccountPerformance = new List<Account_Performance__c>();

	public static Integer iRecord_AccountPerformanceRule = 1;
	public static Account_Performance_Rule__c oMain_AccountPerformanceRule;
	public static List<Account_Performance_Rule__c> lstAccountPerformanceRule = new List<Account_Performance_Rule__c>();
	public static List<String> lstAccountPerformanceRuleValues = new List<String>();

	public static CS_ACC_PER_xBU_Job_Title__c oMain_CS_ACC_PER_xBU_Job_Title;
	public static List<CS_ACC_PER_xBU_Job_Title__c> lstCS_ACC_PER_xBU_Job_Title = new List<CS_ACC_PER_xBU_Job_Title__c>();
	public static List<String> lstCS_ACC_PER_xBU_Job_TitleValues = new List<String>();


    public static List<Account_Performance_Rule__c> createAccountPerformanceRule() {
        return createAccountPerformanceRule(true);
    }
    public static List<Account_Performance_Rule__c> createAccountPerformanceRule(Boolean bInsert) {

    	if (oMain_AccountPerformanceRule == null){

    		if (clsTestData_MasterData.oMain_SubBusinessUnit == null) clsTestData_MasterData.createSubBusinessUnit();
    		if (lstAccountPerformanceRuleValues.size() == 0) initializeAccountPerformanceRuleValues();

    		Integer iCounter_BU = 0;
    		for (String tAccountPerformanceRuleValue : lstAccountPerformanceRuleValues){

    			if (clsTestData_MasterData.lstBusinessUnit.size() <= iCounter_BU) iCounter_BU = 0;

    			List<String> lstValue = tAccountPerformanceRuleValue.split(';');

		        Account_Performance_Rule__c oAccountPerformanceRule = new Account_Performance_Rule__c ();
			        oAccountPerformanceRule.Company__c = clsTestData_MasterData.oMain_Company.Id;
			        oAccountPerformanceRule.Business_Unit__c = clsTestData_MasterData.lstBusinessUnit[iCounter_BU].Id;
			        oAccountPerformanceRule.Highest_level__c = lstValue[0]; 
			        oAccountPerformanceRule.Job_Title_vs__c = lstValue[1];
			        oAccountPerformanceRule.Security_Restriction_Message__c = lstValue[2]; 
			        oAccountPerformanceRule.Show_Account_for__c = lstValue[3];
			        oAccountPerformanceRule.View_Restriction__c = lstValue[4];
		        lstAccountPerformanceRule.add(oAccountPerformanceRule);
		        iCounter_BU++;
    		}

    		if (bInsert) insert lstAccountPerformanceRule;
    		oMain_AccountPerformanceRule = lstAccountPerformanceRule[0];
    	}
    	return lstAccountPerformanceRule;

    }
	

	public static List<CS_ACC_PER_xBU_Job_Title__c> createCS_ACC_PER_xBU_Job_Title(){
		return createCS_ACC_PER_xBU_Job_Title(true);
	}
	public static List<CS_ACC_PER_xBU_Job_Title__c> createCS_ACC_PER_xBU_Job_Title(Boolean bInsert){

		if (oMain_CS_ACC_PER_xBU_Job_Title == null){

    		if (lstCS_ACC_PER_xBU_Job_TitleValues.size() == 0) initializeCS_ACC_PER_xBU_Job_TitleValues();

			lstCS_ACC_PER_xBU_Job_Title = new List<CS_ACC_PER_xBU_Job_Title__c>();

    		for (String tValue : lstCS_ACC_PER_xBU_Job_TitleValues){

    			List<String> lstValue = tValue.split(';');

		        CS_ACC_PER_xBU_Job_Title__c oCS_ACC_PER_xBU_Job_Title = new CS_ACC_PER_xBU_Job_Title__c();
			        oCS_ACC_PER_xBU_Job_Title.CurrencyISOCOde = lstValue[0];
			        oCS_ACC_PER_xBU_Job_Title.Name = lstValue[1];
			        oCS_ACC_PER_xBU_Job_Title.Job_Title__c = lstValue[2];
		        lstCS_ACC_PER_xBU_Job_Title.add(oCS_ACC_PER_xBU_Job_Title);
    		}


			if (bInsert) insert lstCS_ACC_PER_xBU_Job_Title;
			oMain_CS_ACC_PER_xBU_Job_Title = lstCS_ACC_PER_xBU_Job_Title[0];
		}

		return lstCS_ACC_PER_xBU_Job_Title;
	}



	private static List<String> initializeCS_ACC_PER_xBU_Job_TitleValues(){

		lstCS_ACC_PER_xBU_Job_TitleValues = new List<String>();

		lstCS_ACC_PER_xBU_Job_TitleValues.add('USD;Strategic Account Manager;Strategic Account Manager');
		lstCS_ACC_PER_xBU_Job_TitleValues.add('USD;Country Group Manager;Country Group Manager');
		lstCS_ACC_PER_xBU_Job_TitleValues.add('USD;Region Manager;Region Manager');
		lstCS_ACC_PER_xBU_Job_TitleValues.add('USD;Country Manager;Country Manager');
		lstCS_ACC_PER_xBU_Job_TitleValues.add('USD;Subregion Manager;Subregion Manager');
		lstCS_ACC_PER_xBU_Job_TitleValues.add('EUR;MDT Hospital Solutions;MDT Hospital Solutions');
		lstCS_ACC_PER_xBU_Job_TitleValues.add('EUR;Finance;Finance');

		return lstCS_ACC_PER_xBU_Job_TitleValues;

	}



	private static List<String> initializeAccountPerformanceRuleValues(){

		lstAccountPerformanceRuleValues = new List<String>();

		// Highest_level__c;Job_Title__c;Security_Restriction_Message__c;Show_Account_for__c;View_Restriction__c
		lstAccountPerformanceRuleValues.add('Business Unit;Technical Consultant;Test message;User\'s Country;Business Unit Group');
		lstAccountPerformanceRuleValues.add('Business Unit;Technical Consultant;Test message;User\'s Country;XBU');
		lstAccountPerformanceRuleValues.add('Sub Business Unit;Technical Consultant;Test message;User\'s Country;Business Unit');
		lstAccountPerformanceRuleValues.add('Sub Business Unit;Technical Consultant;Test message;User\'s Country;XBU');
		lstAccountPerformanceRuleValues.add('Sub Business Unit;Technical Consultant;Test message;User\'s Country;Business Unit Group');
		lstAccountPerformanceRuleValues.add('Sub Business Unit;Technical Consultant;Test message;User\'s Country;Therapy');
		lstAccountPerformanceRuleValues.add('Therapy;Technical Consultant;Test message;User\'s Country;Therapy');
		lstAccountPerformanceRuleValues.add('Product Group;Technical Consultant;Test message;User\'s Country;Therapy');

		return lstAccountPerformanceRuleValues;
	}
}