public class DIB_Class_Allocation_Core {

    private List<SelectOption> sfaPL ;  
    private DIB_Invoice_Line__c invl ; 
    private Boolean lineSelected ; 
    public String selectedDepartment {set;get;}
    public String selectedAllocation{get;set;}
    public String selectedDepartmentError {set;get;}
    public String departmentName {set;get;}
    public String allocMonthYear {set;get;}
 
    public String selectedSfaId {set;get;}
    List <DIB_Department__c>  dibDepartments;
    
    public List<SelectOption> getsfaPL(){
        return this.sfaPL ; 
    }
    
    public DIB_Invoice_Line__c getinvl (){
        return this.invl ;
    }
    
    public void setinvl(DIB_Invoice_Line__c iltoup){
        this.invl = iltoup ; 
        this.departmentName = iltoup.Department_ID__r.Department_Master_Name__c;
         if(iltoup.Allocated_Fiscal_Month_Code__c != null && iltoup.Allocated_Fiscal_Year__c != null)
        this.allocMonthYear=DIB_AllocationSharedMethods.Format_MonthAnd_Year(iltoup.Allocated_Fiscal_Month_Code__c, iltoup.Allocated_Fiscal_Year__c);
    }
    
    
    public String getDepartement(Id ilid){
        return  invl.Department_ID__c ; 
    }
    
    public DIB_Class_Allocation_Core(DIB_Invoice_Line__c il,  List<Affiliation__c>  affs, List <DIB_Department__c>  dibDepartments){
        this.dibDepartments = dibDepartments;
        this.invl =  il; 
        this.departmentName = il.Department_ID__r.Department_Master_Name__c;
        if(il.Allocated_Fiscal_Month_Code__c != null && il.Allocated_Fiscal_Year__c != null)
        this.allocMonthYear=DIB_AllocationSharedMethods.Format_MonthAnd_Year(il.Allocated_Fiscal_Month_Code__c, il.Allocated_Fiscal_Year__c);        
        this.sfaPL = drawPicklListValues(il.Sold_To__c, il.sold_to__r, affs, il.Sold_To__r.isSales_Force_Account__c);
        this.lineSelected = false ; 
        this.selectedDepartmentError = null;
    }
        
    public Boolean getlineSelected(){
        return this.lineSelected ; 
    }
    
    public void setlineSelected(Boolean is){
        this.lineSelected = is; 
    }
    /**
     *  Draw the DIB_Department picklist 
     *  Parameters : 
     *      @ accountId : Sold To Account Id
     *      @ accountName : Sold To Account Name
     *      @ affs : List of all affiliations 
     *      @ isSFA : Boolean, true if the Sold To Account is a Sales Force account, false if it isn't. 
     */
    public List<SelectOption> drawPicklListValuesDIBDepartment(List <DIB_Department__c>  dibDepartments){
        List<SelectOption> sfaLines = new List<SelectOption>();
        for(DIB_Department__c dibDepartment:dibDepartments){
            if(this.selectedSfaId != null 
                && this.selectedSfaId != DIB_controllerAllocation_V1.SFA_PICKLIST_OTHER 
                &&  this.selectedSfaId == dibDepartment.Account__c){
                    
                String strName = dibDepartment.Account__r.Name + ' (' + dibDepartment.Department_ID__r.Name + ')';
                sfaLines.add(new SelectOption(dibDepartment.Id, strName));
            }
        }       
        
        sfaLines.add(new SelectOption('other','--Other--'));
        
        // if only one value (Other) --set the selectedSfaId to Other
        if (sfaLines.size() == 1){
            this.selectedSfaId = 'other';
        }
        return sfaLines ; 
    }

    /**
     *  Draw the Sales Force Account picklist 
     *  Parameters : 
     *      @ accountId : Sold To Account Id
     *      @ accountName : Sold To Account Name
     *      @ affs : List of all affiliations 
     *      @ isSFA : Boolean, true if the Sold To Account is a Sales Force account, false if it isn't. 
     */
    public List<SelectOption> drawPicklListValues(Id accountId, Account ilAccount, List<Affiliation__c>  affs, Boolean isSFA ){
        List<SelectOption> sfaLines = new List<SelectOption>();
        //sfaLines.add(new SelectOption('','--None--'));
        system.debug('$$$$$$$$$$$$$$$$$$$$$$$$$ AFFILIATION ARE'+affs);
        Boolean accountIdAlreadyAnAffiliation = false ; 
        if (affs.size() > 0 ){
            for(Affiliation__c aff : affs){
                if (aff.Affiliation_To_Account__c != accountId){
                    continue ; 
                }
                String id = aff.Affiliation_From_Account__r.Id;
                String name = aff.Affiliation_From_Account__r.Name + ' - ' + aff.Affiliation_From_Account__r.Account_Address_Line_1__c + ' - ' +  aff.Affiliation_From_Account__r.Account_City__c + ' - ' + aff.Affiliation_From_Account__r.Account_Postal_Code__c; 
                if (id == null) {
                    continue;
                }
                if (id == accountId){
                    accountIdAlreadyAnAffiliation = true ; 
                }
                sfaLines.add(new SelectOption(id,name));
            }
        }
        // Sold To Account as an available value for the SFA picklists . Only if not already in the list. 
        if (isSFA== true  && accountIdAlreadyAnAffiliation == false){
            sfaLines.add(new SelectOption(accountId,ilAccount.Name + ' - ' + ilAccount.Account_Address_Line_1__c + ' - ' +  ilAccount.Account_City__c + ' - ' + ilAccount.Account_Postal_Code__c));
            this.selectedSfaId = accountId ; 
        }
        // Already an affiliation ; 
        if (accountIdAlreadyAnAffiliation == true){
            this.selectedSfaId = accountId;
        }
        sfaLines.add(new SelectOption('other','--Other--'));
        
        // if only one value (Other) --set the selectedSfaId to Other
        if (sfaLines.size() == 1){
            this.selectedSfaId = 'other';
        }
        return sfaLines ; 
    }
    
}