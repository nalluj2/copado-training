/*
 *      Created Date : 20121120
 *      Description : This class tests the logic of ba_AccountCallTopicArchive & ba_ContactCallTopicArchive
 *
 *      Author = Dheeraj
 */
@isTest
private class Test_CallTopicArchiveBatchClasses {

    static testMethod void testCallTopicArchiveBatchClasses() {
        // Preparing Test Data
        // Account Creation
        Account acc1 = new Account();
        acc1.Name = 'TEST_controllerVisitReport Test Coverage Account 1 ' ; 
        insert acc1 ;               
        
        // Contact Creation
        Contact cont1 = new Contact();
        cont1.LastName = 'TEST_controllerVisitReport Test Coverage' ;  
		cont1.FirstName = 'test';
        cont1.AccountId = acc1.Id ;  
        cont1.Phone = '009569699'; 
        cont1.Email ='test@coverage.com';
        cont1.Contact_Department__c = 'Diabetes Adult'; 
        cont1.Contact_Primary_Specialty__c = 'ENT';
        cont1.Affiliation_To_Account__c = 'Employee';
        cont1.Primary_Job_Title_vs__c = 'Manager';
        cont1.Contact_Gender__c = 'Male';
        insert cont1 ;  
        list<id> lstid=new list<id>();
        lstid.add(cont1.id);

         //Company Cration
         Company__c cmpny = new Company__c();
         cmpny.name='TestMedtronic';
         cmpny.CurrencyIsoCode = 'EUR';
         cmpny.Current_day_in_Q1__c=56;
         cmpny.Current_day_in_Q2__c=34; 
         cmpny.Current_day_in_Q3__c=5; 
         cmpny.Current_day_in_Q4__c= 0;   
         cmpny.Current_day_in_year__c =200;
         cmpny.Days_in_Q1__c=56;  
         cmpny.Days_in_Q2__c=34;
         cmpny.Days_in_Q3__c=13;
         cmpny.Days_in_Q4__c=22;
         cmpny.Days_in_year__c =250;
         cmpny.Company_Code_Text__c = 'T27';
         insert cmpny;
        
		// Creating Call Record
        Contact_Visit_Report__c cvr1 = new Contact_Visit_Report__c ();
        
        Call_Records__c vr1 = new Call_Records__c(); 
        vr1.Call_Channel__c = '1:1 visit' ; 
        vr1.Call_Date__c = system.today()-400; 
        insert vr1 ;        
        
		// Create Account Call Topic Counts
        list<Account_Call_Topic_Count__c> lstAccCallTopicCount = new list<Account_Call_Topic_Count__c>();
        for(integer i=0;i<=20;i++){
        	Account_Call_Topic_Count__c AccCallTopicCount = new Account_Call_Topic_Count__c();
        	AccCallTopicCount.Account__c = acc1.Id;
        	AccCallTopicCount.active__c = true;
        	AccCallTopicCount.Call_Recording__c = vr1.id;
        	lstAccCallTopicCount.add(AccCallTopicCount);
        }
        insert lstAccCallTopicCount;

		// Create Contact Call Topic Counts
        list<Contact_Call_Topic_Count__c> lstConCallTopicCount = new list<Contact_Call_Topic_Count__c>();
        for(integer i=0;i<=20;i++){
        	Contact_Call_Topic_Count__c conCallTopicCount = new Contact_Call_Topic_Count__c();
        	conCallTopicCount.Contact__c = cont1.Id;
        	conCallTopicCount.active__c = true;
        	conCallTopicCount.Call_Recording__c = vr1.id;
        	lstConCallTopicCount.add(conCallTopicCount);
        }
        insert lstConCallTopicCount;

       Test.startTest();
       		// Executing ba_AccountCallTopicArchive batch class
	        ba_AccountCallTopicArchive ba_AccCallTopic = new ba_AccountCallTopicArchive();   
	        ID batchprocessid = Database.executeBatch(ba_AccCallTopic,200);
	        
	        // Executing ba_ContactCallTopicArchive batch class
	        ba_ContactCallTopicArchive ba_ConCallTopic = new ba_ContactCallTopicArchive();   
	        ID batchprocessid1 = Database.executeBatch(ba_ConCallTopic,200);	        
        Test.stopTest();  
        
        
        
    }
}