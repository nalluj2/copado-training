public class bl_SETCase {
    
    private static Boolean fromCrossworld = false;
        
    public static Boolean isDMLAllowed(List<SObject> triggerNew){
    	
    	if(fromCrossworld == false && ProfileMedtronic.isSystemAdministratorProfile(UserInfo.getProfileId()) == false && PermissionSetMedtronic.hasPermissionSet('SET_Admin') == false){
    		
    		for(SObject record : triggerNew){
    			
    			record.addError('This record can only be managed in Crossworld or by designated administrators');	
    		}    	
    		
    		return false;		
    	}    	
    	
    	return true;
    }
    
    public static Cost_Analysis_Case__c saveSETCase(Cost_Analysis_Case__c SETCase){
		
		fromCrossworld = true;
				
		upsert SETCase Mobile_ID__c;
		
		fromCrossworld = false;
		
		return SETCase;		
	} 
	
	public static void deleteSETCase(Cost_Analysis_Case__c SETCase){
		
		fromCrossworld = true;
				
		delete SETCase;
		
		fromCrossworld = false;		
	}
	
	public static Cost_Analysis_Case_Answer__c saveSETCaseAnswer(Cost_Analysis_Case_Answer__c SETCaseAnswer){
		
		fromCrossworld = true;
				
		upsert SETCaseAnswer Mobile_ID__c;
		
		fromCrossworld = false;
		
		return SETCaseAnswer;		
	}
	
	public static void deleteSETCaseAnswer(Cost_Analysis_Case_Answer__c SETCaseAnswer){
		
		fromCrossworld = true;
				
		delete SETCaseAnswer;
		
		fromCrossworld = false;		
	}
	
	public static Cost_Analysis_Related_Product__c saveSETCaseProduct(Cost_Analysis_Related_Product__c SETCaseProduct){
		
		fromCrossworld = true;
				
		upsert SETCaseProduct Mobile_ID__c;
		
		fromCrossworld = false;
		
		return SETCaseProduct;		
	} 
	
	public static void deleteSETCaseProduct(Cost_Analysis_Related_Product__c SETCaseProduct){
		
		fromCrossworld = true;
				
		delete SETCaseProduct;
		
		fromCrossworld = false;		
	}
}