public with sharing class bl_Opportunity_BU_Involved {
    
    private static Boolean validateTotal = !bl_Trigger_Deactivation.isTriggerDeactivated('opp_ValidateOppInvolvement');
    public static void setValidateTotal(Boolean value){ validateTotal = value; }    
    
    public static void setUniqueKey(List<Opportunity_BU_Involved__c> triggerNew){
    	
    	for(Opportunity_BU_Involved__c oppBU : triggerNew){
    		
    		oppBU.Unique_Key__c = oppBU.Opportunity__c + ':' + oppBU.BU_Involved__c;
    	}
    }
    
    public static void calculateOpportunityBUInvolvement(List<Opportunity_BU_Involved__c> triggerNew){
    	
    	Set<Id> oppIds = new Set<Id>();
    	
    	for(Opportunity_BU_Involved__c oppBU : triggerNew){
    		
    		oppIds.add(oppBU.Opportunity__c);
    	}
    	    	
    	Set<String> IHS_RT = new Set<String>{'EMEA_IHS_Managed_Services', 'EMEA_IHS_Consulting_Opportunity'};
    	
    	Set<Id> oppTotalError = new Set<Id>();
    	//Set<Id> oppBUError = new Set<Id>();
    	List<Opportunity> opps = new List<Opportunity>();
    	
    	for(Opportunity opp : [Select Id, BU_Involved__c, Annual_Pull_Through_Revenue__c, RecordType.DeveloperName, (Select BU_Involved__c, BU_Percentage__c, Annual_Pull_Through_Revenue__c from Opportunity_BUs_Involved__r) from Opportunity where Id IN :oppIds]){
    		
    		Decimal totalBUs = 0;
    		Set<String> BUsInvolved = new Set<String>();
			Decimal decAnnualPullThroughRevenue = 0;
    		
    		Boolean isIHS = IHS_RT.contains(opp.RecordType.DeveloperName);
    		
    		//Boolean includesIHS = false;
    		
    		for(Opportunity_BU_Involved__c oppBU : opp.Opportunity_BUs_Involved__r){
    			
    			//if(oppBU.BU_Involved__c == 'IHS') includesIHS = true;
    			
    			if(oppBU.BU_Percentage__c != null){
    				
    				totalBUs += oppBU.BU_Percentage__c;
    				BUsInvolved.add(oppBU.BU_Involved__c);
					decAnnualPullThroughRevenue += clsUtil.isDecimalNull(oppBU.Annual_Pull_Through_Revenue__c, -1);

    			}

    		}
			
			//if(includesIHS == true && opp.Opportunity_BUs_Involved__r.size() > 1) oppBUError.add(opp.Id);
			//else{
	    		
	    		if(validateTotal == true && isIHS && totalBUs != 100) oppTotalError.add(opp.Id);
	    		else{
	    			
	    			if(totalBUs == 100){
	    				
	    				opp.BU_Involved__c = String.join(new List<String>(BUsInvolved), ';');
						if (decAnnualPullThroughRevenue >= 0){
							opp.BU_Pull_Through_Added__c = true;
							opp.Annual_Pull_Through_Revenue__c = decAnnualPullThroughRevenue;
						}else{
							opp.BU_Pull_Through_Added__c = false;
							opp.Annual_Pull_Through_Revenue__c = null;
						}
	    				opps.add(opp);

	    			}

	    		}

			//}
    	}
    	
    	if(opps.size() > 0) update opps;
    	
    	//if(oppTotalError.size() > 0 || oppBUError.size() > 0){
    	if(oppTotalError.size() > 0){
    		
    		for(Opportunity_BU_Involved__c oppBU : triggerNew){
    		
    			if(oppTotalError.contains(oppBU.Opportunity__c)) oppBU.addError('The total Opportunity percentage of BU involved must be 100%');    			
    			//if(oppBUError.contains(oppBU.Opportunity__c)) oppBU.addError('IHS cannot be added in combination with other Business Units');
    		}	
    	}
    }
}