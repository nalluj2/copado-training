public with sharing class ctrlExt_quickCallRecord_addAction {
	
	public Call_Topics__c callTopic {get; set;}
	private Call_Records__c callRecord;
	
	public Boolean isXBU {get; set;}
	public String buName {get; set;}
	private String xBURecordTypeId {get; set;}
		
	public Boolean isSaveError {get; set;}
    
    public String[] selectedProducts {get; set;}
    public String[] selectedSubjects {get; set;}
    public String[] selectedBUs {get; set;}
	
	public List<SelectOption> activityTypes {get; set;}
    public List<SelectOption> therapies {get; set;}
    public List<SelectOption> subjects {get; set;}
    public List<SelectOption> products {get; set;}
    public List<SelectOption> bus {get; set;}
    
    public String activityName {get; set;}
        
    public Boolean productMandatory {get; set;}
    public Boolean subjectMandatory {get; set;}
    public Boolean durationMandatory {get; set;}
    // Render the field lobbying when Country is Cananda, then also make it required.
    public Boolean lobbyingMandatory {get; set;}
	public Boolean lobbyingRender {get; set;}
	
	private String userRegion {get; set;}

	public ctrlExt_quickCallRecord_addAction ( ApexPages.StandardController sc){
		
		if(Test.isRunningTest() == false){
			sc.addFields(new List<String>{'Business_Unit__c', 'RecordTypeId'});
		}
		
		User currentUser = [SELECT Region_vs__c FROM User WHERE Id = :UserInfo.getUserId()];
		userRegion = currentUser.Region_vs__c;
		
		callRecord = (Call_Records__c) sc.getRecord();
		
		callTopic = new Call_Topics__c();
		callTopic.Call_Records__c = callRecord.Id;
		
		RecordType rt = [Select DeveloperName from RecordType where Id =: callRecord.RecordTypeId];
		
		isXBU = rt.DeveloperName == 'xBU';

        // The lobbying is for both: BU & xBU
        if(Schema.SObjectType.Call_Topics__c.fields.Call_Topic_Lobbying__c.Accessible){
            lobbyingMandatory = true;
            lobbyingRender = true;
        } else {
            lobbyingMandatory = false;
            lobbyingRender = false;
        }
		
		if(isXBU == false){
			
			Business_Unit__c bu = [Select Name, Product_Mandatory_Check__c, Mandatory_Call_Topic_Duration__c, Mandatory_Subject__c From Business_Unit__c where Id=:callRecord.Business_Unit__c];
        
        	buName = bu.Name;
        
        	durationMandatory = bu.Mandatory_Call_Topic_Duration__c;
        	productMandatory = bu.Product_Mandatory_Check__c;
        	subjectMandatory = bu.Mandatory_Subject__c;

			loadActivities();  
        	loadTherapies();
		}else{
			
			xBURecordTypeId = String.valueOf(callRecord.RecordTypeId).substring(0, 15);
			
			loadActivities();
		}
	}
	
	private void loadActivities(){
    	
    	activityTypes = new List<SelectOption>();
    	activityTypes.add(new SelectOption('','--None--'));
    	
    	List<Call_Topic_Settings__c> activities;
    	
    	if(isXBU){
    		
    		activities = [ SELECT Call_activity_type__c, Call_activity_type__r.name                                       
	    					  FROM Call_Topic_Settings__c WHERE Call_activity_type__c<>null
	        				  AND Call_activity_type__c<>'' AND Region_vs__c = :userRegion
	        				  AND Call_Record_Record_Type__c=:xBURecordTypeId ORDER BY Call_activity_type__r.name ASC ];
    	}else{
    		    			        				  
    		activities = [ Select Call_activity_type__c,Call_activity_type__r.name,
                              Business_Unit__c from Call_Topic_Settings__c where 
                              Call_activity_type__c<>null AND Call_activity_type__c<>''  AND Region_vs__c = :userRegion
                              AND Business_Unit__c =:callRecord.Business_Unit__c  order by Call_activity_type__r.name ];    		
    	}
    	
    	for(Call_Topic_Settings__c activity : activities){
                                                 
			String Id = activity.Call_activity_type__c;
	        String name = activity.Call_activity_type__r.name;
	        activityTypes.add(new SelectOption(Id,Name));	        
	    }	    	    
    }    
            
    private void loadTherapies() {
        
        therapies = new List<SelectOption>();
		therapies.add(new SelectOption('','--None--'));
		
        for(Therapy__c therapy : [Select Id, Name From Therapy__c where Business_Unit__c =:callRecord.Business_Unit__c and Active__c = true order by name]){
	        
	        String id = therapy.Id;
	        String name = therapy.Name;
	                    
            therapies.add(new SelectOption(id,name));
        }              
    }
    
    private void loadSubjects(){
    	
    	subjects = new List<SelectOption>();
        
        List<Call_topic_Setting_Details__c> ctSettingDetails;
        
        if(isXBU){
        	
			ctSettingDetails = [Select Subject__c,Subject__r.Name From Call_topic_Setting_Details__c 
									where Call_Topic_Setting__r.Business_Unit__c = NULL AND Call_Topic_Setting__r.Call_activity_type__c =:callTopic.Call_Activity_Type__c AND Call_Topic_Setting__r.Region_vs__c = :userRegion ORDER BY Subject__r.Name];
        					
        }else{
			
        	ctSettingDetails = [Select Subject__c,Subject__r.Name From Call_topic_Setting_Details__c 
									where Call_Topic_Setting__r.Business_Unit__c=:callRecord.Business_Unit__c and Call_Topic_Setting__r.Call_activity_type__c =:callTopic.Call_Activity_Type__c AND Call_Topic_Setting__r.Region_vs__c = :userRegion order by Subject__r.Name];        	
        }
              
        for (Call_topic_Setting_Details__c subject: ctSettingDetails) {
        
             String Id = subject.Subject__c;
             String name=subject.Subject__r.Name;
                                 
             subjects.add(new SelectOption(Id,name));     
        }    	
    }
    
    private void loadBUs(){
    	
    	Set<id> companyIds = new Set<id>();
    	
    	bus = new List<SelectOption>();
                           
		for(Call_Topic_Settings__c cs : [Select Company_ID__c from Call_Topic_Settings__c where 
	                                         Company_ID__c<>null AND Company_ID__c<>'' AND Region_vs__c = :userRegion
	                                         AND Call_Record_Record_Type__c=:xBURecordTypeId and Call_activity_type__c=:callTopic.Call_Activity_Type__c ]){

	        companyIds.add(cs.Company_ID__c);                                
        }
        
        if(companyIds.size()>0){ 
			for(Business_Unit__c bu:[select Id, Name from Business_Unit__c where Company__c IN:companyIds ORDER BY Name ASC]){
				bus.add(new SelectOption(bu.Id,bu.Name));
          	}
		}            	
    }
    
    private void loadProducts(){
    	
    	products = new List<SelectOption>();
                        
        for(Therapy_Product__c product: [Select Product__c, Product__r.Name From Therapy_Product__c where Therapy__c =:callTopic.Call_Topic_Therapy__c order by Product__r.Name] ){
                
        	String id = product.Product__c;
            String name = product.Product__r.Name;
                
            products.add(new SelectOption(id,name));
        }
    }
       
    public void activityTypeChanged(){
    	
    	if(callTopic.Call_Activity_Type__c != null) activityName = [Select Name from Call_Activity_Type__c where Id = :callTopic.Call_Activity_Type__c].Name;						
		else activityName = null;
    	
    	if(isXBU){
    		
    		selectedSubjects = null;
    		selectedBUs = null;    	    	   	
        	loadSubjects(); 
			loadBUs();
						  
    	}else{
    	    	
    		selectedSubjects = null;    	    	   	
        	loadSubjects();
    	}           	
    }
    
    public void therapyChanged(){
    	    	    	
    	selectedProducts = null;    	    	   	
        loadProducts();           	
    }
	
	public PageReference save(){
    	
    	isSaveError = false;
    	    	
    	if (callTopic.Call_Activity_Type__c == null) {    		
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Activity Type is required'));            
            isSaveError=true;
    	}
    	
    	if (isXBU == false && callTopic.Call_Topic_Therapy__c == null) {    		
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Therapy is required'));            
            isSaveError=true;
    	}    	
    	
    	if(isXBU == false && durationMandatory == true && (callTopic.Call_Topic_Duration__c == null || callTopic.Call_Topic_Duration__c == 0)){
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Duration is required for selected Business Unit'));
        	isSaveError = true ; 
        }
        
        if(isXBU == false && productMandatory == true && selectedProducts.size()==0){
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'At least one Product is required for selected Business Unit'));
        	isSaveError = true ; 
        }
        
        if(isXBU == false && subjectMandatory == true && selectedSubjects.size()==0){
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'At least one Subject is required for selected Business Unit'));
        	isSaveError = true ; 
        }
        
        if (callTopic.Call_Topic_Duration__c != null && callTopic.Call_Topic_Duration__c < 0) {           
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Duration cannot be a negative value'));
        	isSaveError = true ;           
        }

		if(lobbyingMandatory == true && (callTopic.Call_Topic_Lobbying__c == '' || callTopic.Call_Topic_Lobbying__c == null)){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Lobbying is required'));
			isSaveError = true ;
		}
		
		if(isXBU == false && activityName == 'Patient Training' && buName == 'Diabetes' && callTopic.Number_of_Patient_Trained__c == null) {           
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Number of trained patient is required for selected Business Unit and Activity'));
        	isSaveError = true ;           
        }
		
    	if(isSaveError == true) return null;
    	
    	SavePoint sp = Database.setSavepoint();
    	    	    	
    	try{
    		
    		Call_Topic_Settings__c activity = [Select Call_Category__c from Call_Topic_Settings__c where Business_Unit__c =:callRecord.Business_Unit__c and Call_activity_type__c =:callTopic.Call_Activity_Type__c AND Region_vs__c = :userRegion limit 1];
            callTopic.Call_Category__c=activity.Call_Category__c;   		
    		insert callTopic;
    		
    		//Topic Subjects
    		List<Call_Topic_Subject__c> topicSubjects = new List<Call_Topic_Subject__c>();
    		
    		for(String subjectId : selectedSubjects){
    			
    			Call_Topic_Subject__c topicSubject = new Call_Topic_Subject__c();
    			topicSubject.Call_Topic__c = callTopic.Id;
    			topicSubject.Subject__c = subjectId;
    			
    			topicSubjects.add(topicSubject);
    		}
    		
    		if(topicSubjects.size()>0) insert topicSubjects;
    		
    		if(isXBU){
    			
    			//Topic Business Units
	    		List<Call_Topic_Business_Unit__c> topicBUs = new List<Call_Topic_Business_Unit__c>();
	    		
	    		for(String buId : selectedBUs){
	    			
	    			Call_Topic_Business_Unit__c topicBU = new Call_Topic_Business_Unit__c();
	    			topicBU.Call_Topic__c = callTopic.Id;
	    			topicBU.Business_Unit__c = buId;
	    			
	    			topicBUs.add(topicBU);
	    		}
	    		
	    		if(topicBUs.size()>0) insert topicBUs;
    			
    		}else{
    			
    			//Topic Products
	    		List<Call_Topic_Products__c> topicProducts = new List<Call_Topic_Products__c>();
	    		
	    		for(String productId : selectedProducts){
	    			
	    			Call_Topic_Products__c topicProduct = new Call_Topic_Products__c();
	    			topicProduct.Call_Topic__c = callTopic.Id;
	    			topicProduct.Product__c = productId;
	    			
	    			topicProducts.add(topicProduct);
	    		}
	    		
	    		if(topicProducts.size()>0) insert topicProducts;   		
    		}
    		    		    		
    	}catch(Exception e){
    		
    		ApexPages.addMessages(e);
    		
    		callTopic.Id = null;
    		   		   		
    		isSaveError=true;
    		
    		Database.rollback(sp);
    		
    		if(Test.isRunningTest() == true) throw e;
    		
    		return null;
    	}
    	    	    	
    	return null;
    }      
}