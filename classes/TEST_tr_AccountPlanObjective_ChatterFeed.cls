/*
 *      Created Date 	: 17-02-2014
 *      Author 			: Bart Caelen
 *      Description 	: TEST Class for the APEX TRIGGER tr_AccountPlanObjective_ChatterFeed
 */
@isTest private class TEST_tr_AccountPlanObjective_ChatterFeed {

    @isTest static void test_tr_AccountPlanObjective_ChatterFeed() {

		//------------------------------------------------
		// CREATE TEST DATA
		//------------------------------------------------
		clsTestData.createAccountPlanObjectiveData();		// TEST Insert
		//------------------------------------------------

		// TEST Update
		update clsTestData.oMain_AccountPlanObjective;

		// TEST Delete
		delete clsTestData.oMain_AccountPlanObjective;
		
		// TEST Undelete
		undelete clsTestData.oMain_AccountPlanObjective;
		
    }
}