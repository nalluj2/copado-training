/*
 *      Created Date : 08 -Mar-2013
 *      Description : 1. This class is used to validate Account and Contact according C2AReleationShip 
 *                    2. In case Status is not 'closed' and 'Case OMA owner' field is blank, it will be set as standard owner.
 *                    3. If case status is not 'closed'  and 'Case OMA owner' is not blank, Standard owner will be set as 'Case OMA Owner'
 *                    4. 'Call taken by' is always in sync with standard owner if case status is not closed.
 *      Author = Manish Kumar Srivastava
 *      
 */
public class  bl_tr_CaseTriggerActions{
	public static boolean checkBeforeUpdate=false;
	public static boolean checkAfterUpdate=false;
		
	public static map<id,string> OMARecordTypes{
		
		get{
		
			if(OMARecordTypes == null){
				
				OMARecordTypes = new map<id,string>();
					
				Map<String, Schema.RecordTypeInfo> rtByDevName = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName();
			
				for(OMA_Record_Types__c omaRT : OMA_Record_Types__c.getAll().values()){
					
					if(rtByDevNAme.containsKey(omaRT.Developer_Name__c)){
												
						OMARecordTypes.put(rtByDevName.get(omaRT.Developer_Name__c).getRecordTypeId(),omaRT.Developer_Name__c);
					}					
				}			
			}
			
			return OMARecordTypes;
		}
		
		set;
	}
	
	public static void validateAccountContactAreC2ARelationship(List<Case> lstCase){
		
		set<string> setCaseAccContact=new set<string>();
		map<string,string> mapAffidWithAccContact=new map<string,string>();
		
		set<id> setAccid=new set<id>();
		set<id> setConid=new set<id>();
		for(case c:lstCase){
			if(OMARecordTypes.get(c.RecordTypeID) != null && c.Contactid!=null && c.accountid!=null){
				setCaseAccContact.add(c.CaseAccountContact__c);
				setAccid.add(c.accountid);
				setConid.add(c.contactid);
			}
		}
		if(setCaseAccContact.size()>0){
			List<Affiliation__c> lstAff=[SELECT id,ConcatAccountContact__c
						FROM Affiliation__c
						where RecordTypeId =: FinalConstants.recordTypeIdC2A
						and Affiliation_To_Account__c in: setAccid
						and Affiliation_From_Contact__c in: setConid
						and Affiliation_Active__c = true
						and ConcatAccountContact__c in : setCaseAccContact];
			for(Affiliation__c aff:lstAff){
				mapAffidWithAccContact.put(aff.ConcatAccountContact__c,aff.ConcatAccountContact__c);  
			}
			for(case c:lstCase){
				if(OMARecordTypes.get(c.RecordTypeID) != null && c.contactid!=null && c.accountid!=null && mapAffidWithAccContact.get(c.CaseAccountContact__c)==null ){
					c.addError('Invalid Account and Contact Combination');
				}
			}
		}          
	}
	
	//-BC - 20150415 - CR-8394 - START
	// mapCase_Old will be null in case of an insert
	public static void updateOMAFields(List<Case> lstCase, Map<Id, Case> mapCase_Old){
				
		Map<Id, Id> mapCaseID_OwnerID = new Map<Id, Id>();
		for (Case oCase : lstCase){
			if (OMARecordTypes.get(oCase.recordtypeid)!=null){ 
				if ( (mapCase_Old != null) && (oCase.Call_Taken_By__c == null) ){
					oCase.Call_Taken_By__c = oCase.ownerid;
				}
				if (oCase.OMA_Case_Owner__c == null){
					if (mapCase_Old == null){
						// INSERT - we can't set the OMA_Case_Owner__c immediatly because this change is coming from a After Insert trigger (needed because of an issue when the internal comment was entered upon creation of the case (insufficient privileges))
                    	mapCaseID_OwnerID.put(oCase.Id, oCase.OwnerId);
					}else if (oCase.OwnerId != mapCase_Old.get(oCase.Id).OwnerId){
                    	mapCaseID_OwnerID.put(oCase.Id, oCase.OwnerId);
                    }
				}else if (oCase.OMA_Case_Owner__c != null){
					if (mapCase_Old == null){
						// INSERT - we can't set the OMA_Case_Owner__c immediatly because this change is coming from a After Insert trigger (needed because of an issue when the internal comment was entered upon creation of the case (insufficient privileges))
                    	mapCaseID_OwnerID.put(oCase.Id, oCase.OMA_Case_Owner__c);
					}else if ( (oCase.OMA_Case_Owner__c != mapCase_Old.get(oCase.Id).OMA_Case_Owner__c) || (oCase.OwnerId != mapCase_Old.get(oCase.Id).OwnerId) ){
                    	mapCaseID_OwnerID.put(oCase.Id, oCase.OMA_Case_Owner__c);
                    }
				}
			}   
		}
		if (mapCaseID_OwnerID.size()>0){
			if (bl_Case.bCaseDoNotUpdateOwner == false){
				bl_Case.updateOwnership(mapCaseID_OwnerID);
			}
		}
	}
/*
	//-BC - 20150309 - CR-6846 - START
//	public static void updateOMAFields(List<Case> lstCase){
	public static void updateOMAFields(List<Case> lstCase, Map<Id, Case> mapCase_Old){
	//-BC - 20150309 - CR-6846 - STOP
		clsUtil.debug('updateOMAFields is fired');
		map<id,string> mapOMARecTypeidWithDevName=new map<id,string>();
		mapOMARecTypeidWithDevName=OMARecordType();
		Map<Id, Id> mapCaseID_OwnerID = new Map<Id, Id>();  //-BC - 20150309 - CR-6846 - Added
		for(case c:lstCase){
			if(mapOMARecTypeidWithDevName.get(c.recordtypeid)!=null){ 
				if(c.Call_Taken_By__c==null){
					c.Call_Taken_By__c=c.ownerid;
				}
				if(c.OMA_Case_Owner__c==null && c.status!='Closed'){
					//-BC - 20150309 - CR-6846 - START
					if ( (mapCase_Old == null) || (c.OwnerId != mapCase_Old.get(c.Id).OwnerId) ){
                    	mapCaseID_OwnerID.put(c.Id, c.OwnerId);
                    }
//					c.OMA_Case_Owner__c=c.ownerid;
					//-BC - 20150309 - CR-6846 - STOP
				}
				else if(c.OMA_Case_Owner__c!=null && c.status!='Closed'){
					//-BC - 20150309 - CR-6846 - START
					if ( (mapCase_Old == null) || (c.OMA_Case_Owner__c != mapCase_Old.get(c.Id).OMA_Case_Owner__c) ){
                    	mapCaseID_OwnerID.put(c.Id, c.OMA_Case_Owner__c);
                    }
//					c.ownerid=c.OMA_Case_Owner__c;
					//-BC - 20150309 - CR-6846 - STOP
				}
			}   
		}

		//-BC - 20150309 - CR-6846 - START
		if (mapCaseID_OwnerID.size()>0){
			if (bl_Case.bCaseDoNotUpdateOwner == false){
				bl_Case.updateOwnership(mapCaseID_OwnerID);
			}
		}
		//-BC - 20150309 - CR-6846 - STOP

	}
*/	
	//-BC - 20150415 - CR-8394 - STOP

}