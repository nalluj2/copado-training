@isTest
private class TEST_CasePart_UPSERT_AFTER {
	
	private static Contact oContact;
	private static Workorder__c oWorkorder1;
	private static Workorder__c oWorkorder2;
	private static Workorder_Sparepart__c oWorkorderSparepart;

	@isTest static void createTestData(){

		clsTestData_MasterData.tCompanyCode = 'EUR';
		clsTestData_MasterData.tBusinessUnitName = 'Cranial Spinal';
		clsTestData_MasterData.createBusinessUnit(true);
			  
		Account oAccount = new Account();
			oAccount.AccountNumber = '103101';
			oAccount.Name = 'MNav House Account';
			oAccount.Account_Country_vs__c = 'USA';        
			oAccount.CurrencyIsoCode = 'USD';
			oAccount.Phone = '24107954';
			oAccount.ST_NAV_Non_SAP_Account__c = true;
		insert oAccount; 

		oContact = new Contact();
			oContact.LastName = 'Test Contact';
			oContact.FirstName = 'Test Contact';
			oContact.AccountId = oAccount.id; 
			oContact.Contact_Department__c = 'Pediatric';
			oContact.Contact_Primary_Specialty__c = 'Neurology';
			oContact.Affiliation_To_Account__c = 'Employee';
			oContact.Primary_Job_Title_vs__c = 'Nurse';
			oContact.Contact_Gender__c = 'Female';
		insert oContact;
   
		Asset oAsset = new Asset();
			oAsset.Name = 'Test Asset';
			oAsset.AccountId = oAccount.Id;
			oAsset.RecordTypeId = clsUtil.getRecordTypeByDevName('Asset', 'O_Arm').Id;
			oAsset.Serial_Nr__c = 'XXXXX';
			oAsset.Asset_Product_Type__c = 'O-Arm 1000';
			oAsset.Ownership_Status__c = 'EOL';
		insert oAsset;
		
		Product2 oProduct = new Product2();
			oProduct.Name = 'Product ABC';
			oProduct.RecordTypeId = clsUtil.getRecordTypeByDevName('Product2', 'SAP_Product').Id;
			oProduct.Business_Unit_ID__c = clsTestData_MasterData.oMain_BusinessUnit.Id;
			oProduct.Tracking__c = TRUE ;
			oProduct.Maintenance_Frequency__c = 12;
		insert oProduct;

		List<Workorder__c> lstWorkorder = new List<Workorder__c>();
		oWorkorder1 =new Workorder__c();
			oWorkorder1.RecordTypeId = clsUtil.getRecordTypeByDevName('Workorder__c', 'O_Arm_PM').Id;    
			oWorkorder1.Account__c = oAccount.Id;
			oWorkorder1.Asset__c = oAsset.Id;
			oWorkorder1.CurrencyIsoCode = 'EUR';
			oWorkorder1.Bio_Med_FSE_Visit__c = 'No';
			oWorkorder1.Date_Completed__c = System.today();
			oWorkorder1.Status__c = 'In Process';
		lstWorkorder.add(oWorkorder1);
		oWorkorder2 =new Workorder__c();
			oWorkorder2.RecordTypeId = clsUtil.getRecordTypeByDevName('Workorder__c', 'O_Arm_PM').Id;    
			oWorkorder2.Account__c = oAccount.Id;
			oWorkorder2.Asset__c = oAsset.Id;
			oWorkorder2.CurrencyIsoCode = 'EUR';
			oWorkorder2.Bio_Med_FSE_Visit__c = 'No';
			oWorkorder2.Date_Completed__c = System.today();
			oWorkorder2.Status__c = 'In Process';
		lstWorkorder.add(oWorkorder2);
		insert lstWorkorder; 

		Case oCase = new Case();
		insert oCase;

		Complaint__c oComplaint = new Complaint__c();
			oComplaint.Status__c = 'Open';
			oComplaint.Account_Name__c = oAccount.Id;
			oComplaint.Contact_Name__c = oContact.id;
			oComplaint.Asset__c = oAsset.id;
			oComplaint.Medtronic_Aware_Date__c = System.today();
			oComplaint.Formal_Investigation_Required__c = 'No';
			oComplaint.Formal_Investigation_Justification__c = 'abc';
			oComplaint.Formal_Investigation_Reference__c = 'abc';
			oComplaint.Methods__c = 'abc';
			oComplaint.Results__c = 'abcc';
		insert oComplaint;

		oWorkorderSparepart = new Workorder_Sparepart__c();
			oWorkorderSparepart.Workorder__c = oWorkorder1.Id;
			oWorkorderSparepart.Account__c = oAccount.Id;
			oWorkorderSparepart.Owner__c = oContact.id;
			oWorkorderSparepart.Order_Part__c = oProduct.id;
			oWorkorderSparepart.Order_Status__c = 'Pending';
			oWorkorderSparepart.RI_Part__c = oProduct.id;
			oWorkorderSparepart.Return_Item_Status__c = 'Pending Return';
			oWorkorderSparepart.Case__c = oCase.id; 
			oWorkorderSparepart.Able_to_Duplicate_the_Issue__c = 'Yes';
			oWorkorderSparepart.Failure_Mode__c = 'Accuracy';
			oWorkorderSparepart.Findings_and_Conclusions__c = 'TEST Findings And Conclusions';
			oWorkorderSparepart.Root_Cause__c = '  3D Model';
			oWorkorderSparepart.Sub_Root_Cause__c = '3D Model'; 
			oWorkorderSparepart.Root_Cause_Other__c = 'abc'; 
			oWorkorderSparepart.Evaluation_Notes__c = 'TEST Evaluation Notes';
			oWorkorderSparepart.Complaint__c = oComplaint.id;
			oWorkorderSparepart.Current_Location__c = 'testLocation';
		insert oWorkorderSparepart;		

	}

	@isTest static void test_CasePart_UPSERT_AFTER_ChangeWorkorder() {

		// Create Test Data
		createTestData();
		List<Case_Comment__c> lstCaseComment = [SELECT ID, Comment__c FROM Case_Comment__c WHERE Case_Part__c = :oWorkorderSparepart.Id];
		Integer iNumberOfCaseComments = lstCaseComment.size();


		// Execute logic
		Test.startTest();

			oWorkorderSparepart.Workorder__c = oWorkorder2.Id;
		update oWorkorderSparepart;

		Test.stopTest();


		// Validate Result

	}


	@isTest static void test_CasePart_UPSERT_AFTER_AnalysisComplete_1() {

		// Create Test Data
		createTestData();
		List<Case_Comment__c> lstCaseComment = [SELECT ID, Comment__c FROM Case_Comment__c WHERE Case_Part__c = :oWorkorderSparepart.Id];
		Integer iNumberOfCaseComments = lstCaseComment.size();


		// Execute logic
		Test.startTest();

			oWorkorderSparepart.Return_Item_Status__c = 'Analysis Complete';
		update oWorkorderSparepart;

		Test.stopTest();


		// Validate Result
		lstCaseComment = [SELECT ID, Comment__c FROM Case_Comment__c WHERE Case_Part__c = :oWorkorderSparepart.Id];
		System.assert(lstCaseComment.size() > iNumberOfCaseComments);
		Boolean bCaseCommentFound_ReturnItemStatus = false;
		for (Case_Comment__c oCaseComment : lstCaseComment){

			String tComment = oCaseComment.Comment__c;

			if (!String.isBlank(tComment)){

				if (tComment.contains('Return Item Status'))
					bCaseCommentFound_ReturnItemStatus = true;

			}

		}

		System.assert(bCaseCommentFound_ReturnItemStatus);

	}

/*
	@isTest static void test_CasePart_UPSERT_AFTER_UnderAnalysis_1() {

		// Create Test Data
		createTestData();
		List<Case_Comment__c> lstCaseComment = [SELECT ID, Comment__c FROM Case_Comment__c WHERE Case_Part__c = :oWorkorderSparepart.Id];
		Integer iNumberOfCaseComments = lstCaseComment.size();


		// Execute logic
		Test.startTest();

			oWorkorderSparepart.Return_Item_Status__c = 'Under Analysis';
			oWorkorderSparepart.Findings_and_Conclusions__c = 'My findings and conclusions';
		update oWorkorderSparepart;

		Test.stopTest();


		// Validate Result
		lstCaseComment = [SELECT ID, Comment__c FROM Case_Comment__c WHERE Case_Part__c = :oWorkorderSparepart.Id];
		System.assert(lstCaseComment.size() > iNumberOfCaseComments);
		Boolean bCaseCommentFound_FindingAndConclusions = false;
		for (Case_Comment__c oCaseComment : lstCaseComment){

			String tComment = oCaseComment.Comment__c;

			if (!String.isBlank(tComment)){

				if (tComment.contains('Finding & Conclusions'))
					bCaseCommentFound_FindingAndConclusions = true;

			}

		}

		System.assert(bCaseCommentFound_FindingAndConclusions);

	}

	@isTest static void test_CasePart_UPSERT_AFTER_UnderAnalysis_2() {

		// Create Test Data
		createTestData();
		List<Case_Comment__c> lstCaseComment = [SELECT ID, Comment__c FROM Case_Comment__c WHERE Case_Part__c = :oWorkorderSparepart.Id];
		Integer iNumberOfCaseComments = lstCaseComment.size();


		// Execute logic
		Test.startTest();

			oWorkorderSparepart.Return_Item_Status__c = 'Under Analysis';
			oWorkorderSparepart.Evaluation_Notes__c = 'My evaluation notes';
		update oWorkorderSparepart;

		Test.stopTest();


		// Validate Result
		lstCaseComment = [SELECT ID, Comment__c FROM Case_Comment__c WHERE Case_Part__c = :oWorkorderSparepart.Id];
		System.assert(lstCaseComment.size() > iNumberOfCaseComments);

		Boolean bCaseCommentFound_EvaluationNotes = false;
		for (Case_Comment__c oCaseComment : lstCaseComment){

			String tComment = oCaseComment.Comment__c;

			if (!String.isBlank(tComment)){

				if (tComment.contains('Engineering Evaluation Notes'))
					bCaseCommentFound_EvaluationNotes = true;

			}

		}

		System.assert(bCaseCommentFound_EvaluationNotes);

	}

	@isTest static void test_CasePart_UPSERT_AFTER_UnderAnalysis_3() {

		// Create Test Data
		createTestData();
		List<Case_Comment__c> lstCaseComment = [SELECT ID, Comment__c FROM Case_Comment__c WHERE Case_Part__c = :oWorkorderSparepart.Id];
		Integer iNumberOfCaseComments = lstCaseComment.size();


		// Execute logic
		Test.startTest();

			oWorkorderSparepart.Return_Item_Status__c = 'Under Analysis';
			oWorkorderSparepart.Vendor_Root_Cause_Results__c = 'TEST Vendor Investigation Results';
		update oWorkorderSparepart;

		Test.stopTest();


		// Validate Result
		lstCaseComment = [SELECT ID, Comment__c FROM Case_Comment__c WHERE Case_Part__c = :oWorkorderSparepart.Id];
		System.assert(lstCaseComment.size() > iNumberOfCaseComments);

		Boolean bCaseCommentFound_VendorRootCause = false;
		for (Case_Comment__c oCaseComment : lstCaseComment){

			String tComment = oCaseComment.Comment__c;

			if (!String.isBlank(tComment)){

				if (tComment.contains('Vendor Investigation Results'))
					bCaseCommentFound_VendorRootCause = true;

			}

		}

		System.assert(bCaseCommentFound_VendorRootCause);

	}		
*/

	@isTest static void test_CasePart_UPSERT_AFTER_AnalysisComplete() {

		// Create Test Data
		createTestData();
		List<Case_Comment__c> lstCaseComment = [SELECT ID FROM Case_Comment__c WHERE Case_Part__c = :oWorkorderSparepart.Id];
		Integer iNumberOfCaseComments = lstCaseComment.size();


		// Execute logic
		Test.startTest();

			oWorkorderSparepart.Return_Item_Status__c = 'Analysis Complete';
			oWorkorderSparepart.Failure_Mode__c = 'Computer';
			oWorkorderSparepart.Sub_Root_Cause__c = 'BIOS settings changed';
			oWorkorderSparepart.Methodology__c = 'Performance Testing';
			oWorkorderSparepart.Lot_Number__c = 'Lot 100';
			oWorkorderSparepart.Date_Received_Back__c = Date.today();
			oWorkorderSparepart.Received_By_Text__c = oContact.Id;
			oWorkorderSparepart.Tested_By_Text__c = oContact.Id;
			oWorkorderSparepart.Tested_By_Date__c = Date.today();

			oWorkorderSparepart.Findings_and_Conclusions__c = 'My findings and conclusions';
		update oWorkorderSparepart;

			oWorkorderSparepart.Evaluation_Notes__c = 'My evaluation notes';
		update oWorkorderSparepart;

		Test.stopTest();


		// Validate Result
		lstCaseComment = [SELECT ID FROM Case_Comment__c WHERE Case_Part__c = :oWorkorderSparepart.Id];
		System.assert(lstCaseComment.size() > iNumberOfCaseComments);


	}


	@isTest static void test_CasePart_UPSERT_AFTER_ClearComplaint() {

		// Create Test Data
		createTestData();


		// Execute logic
		Test.startTest();

			oWorkorderSparepart.Return_Item_Status__c = 'Under Analysis';
			oWorkorderSparepart.Complaint__c = null;
		update oWorkorderSparepart;

		Test.stopTest();


		// Validate Result
		// We can't clear the Complaint (?) - why is this logic implemented while if will never be fired?


	}

}