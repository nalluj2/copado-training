public with sharing class ctrl_Lookup {
	
	public String compId {get; set;}
	public String objectName {get; set;}
	public String fieldNames {get; set;}
	public String filter{get; set;}

	public Boolean bOverruleTargetFilter { get;set; }	//-BC - 20150616 - CR-5767
			
	public String getEncodedParams(){
		
		String params='sobject='+objectName;
		params+='&fields='+fieldNames;
		params+='&filter='+EncodingUtil.urlEncode(filter, 'UTF-8');		
		params+='&compId='+compId;
		params+='&overruleTargetFilter=' + bOverruleTargetFilter;	//-BC - 20150616 - CR-5767 - START
		
		return EncodingUtil.urlEncode(CryptoUtils.encryptText(params), 'UTF-8');
	}
}