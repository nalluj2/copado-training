@isTest
private class Test_ctrl_Activity_Scheduling_Req_List {
	
	private static testmethod void testGetRequestLists(){
		
		createTestCases();
		
		Test.startTest();
		
		String accountFilter = null;
		Date startFilter = null;
		String statusFilter = 'Open';
		String typeFilter = null;
		String teamFilter = null;
		String assignedToFilter = null;
		Integer size = 50;
		Integer offset = 0;
		String orderDirection = 'ASC';
		
		List<ctrl_Activity_Scheduling_Request_List.ActivityRequest> results = ctrl_Activity_Scheduling_Request_List.getActivityRequests(accountFilter, startFilter, statusFilter, typeFilter, teamFilter, assignedToFilter, size, offset, orderDirection);
		System.assert(results.size() == 6);
		
		Implant_Scheduling_Team_Member__c member = [Select Member__c, Team__c from Implant_Scheduling_Team_Member__c LIMIT 1];
		assignedToFilter = member.Member__c;
		statusFilter = null;
		
		results = ctrl_Activity_Scheduling_Request_List.getActivityRequests(accountFilter, startFilter, statusFilter, typeFilter, teamFilter, assignedToFilter, size, offset, orderDirection);
		System.assert(results.size() == 1);
			
		assignedToFilter = null ;
		teamFilter = member.Team__c ;
		
		results = ctrl_Activity_Scheduling_Request_List.getActivityRequests(accountFilter, startFilter, statusFilter, typeFilter, teamFilter, assignedToFilter, size, offset, orderDirection);
		System.assert(results.size() > 0);
		
		teamFilter = null;
		startFilter = Date.today().addDays(2);
		
		results = ctrl_Activity_Scheduling_Request_List.getActivityRequests(accountFilter, startFilter, statusFilter, typeFilter, teamFilter, assignedToFilter, size, offset, orderDirection);
		System.assert(results.size() == 0);
	}
	
	private static void createTestCases(){
		
		Account acc = new Account();
		acc.Name = 'Test Account';
		acc.Account_Country_vs__c = 'NETHERLANDS';		
		insert acc;
		
		List<Implant_Scheduling_Team__c> teams = [Select Id from Implant_Scheduling_Team__c];
		
		Id caseActivitySchedulingRT = [Select Id from RecordType where SObjectType = 'Case' AND DeveloperName = 'Implant_Scheduling'].Id;
		
		List<Case> caseList = new List<Case>();
		
		for(Integer i = 0; i < 6; i++){
		
			Case openCase = new Case();		
			openCase.AccountId = acc.Id;			
			openCase.Start_of_Procedure__c = DateTime.newInstance(Date.today().addDays(1), Time.newInstance(10 + i, 0, 0, 0)); 
			openCase.Procedure_Duration_Implants__c = '0' + i + ':00';
			openCase.RecordTypeId = caseActivitySchedulingRT;
			openCase.Activity_Scheduling_Team__c = teams.get(Math.mod(i, 2)).Id;
			openCase.Status = 'Open';	
			
			caseList.add(openCase);
		}
		
		List<Implant_Scheduling_Team_Member__c> members = [Select Member__c, Team__c from Implant_Scheduling_Team_Member__c];
		
		for(Integer i = 0; i < members.size(); i++){
			
			Implant_Scheduling_Team_Member__c member = members.get(i);
			
			Case assignedCase = new Case();		
			assignedCase.AccountId = acc.Id;			
			assignedCase.Start_of_Procedure__c = DateTime.newInstance(Date.today().addDays(-i), Time.newInstance(10, 0, 0, 0)); 
			assignedCase.Procedure_Duration_Implants__c = '0' + i + ':00';
			assignedCase.RecordTypeId = caseActivitySchedulingRT;
			assignedCase.Activity_Scheduling_Team__c = member.Team__c;
			assignedCase.Assigned_To__c = member.Member__c;
			assignedCase.Status = 'Assigned';	
			
			caseList.add(assignedCase);
		}
		
		
		bl_Case_Trigger.runningSchedulingApp = true;
		
		insert caseList;
	}
	
	@TestSetup
	private static void setData() {
		
		Mobile_App__c app = new Mobile_App__c();
		app.Name = 'Activity Scheduling Tool';
		app.Active__c = true;
		app.Unique_Key__c = 'ACT_SCHDL';
		insert app;
		
		List<Implant_Scheduling_Team__c> teams = new List<Implant_Scheduling_Team__c>();
		
		Id activitySchedulingTeamRT = [Select Id from RecordType where SObjectType = 'Implant_Scheduling_Team__c' AND DeveloperName = 'Activity_Scheduling_Team'].Id;
		
		Implant_Scheduling_Team__c team1 = new Implant_Scheduling_Team__c();			
		team1.Name = 'Test Team 1';
		team1.Team_Color__c = 'rgb(99, 99, 99)';
		team1.Work_Day_End__c = '18';
		team1.Work_Day_Start__c = '8';
		team1.Working_Days__c = '1:5';			
		team1.RecordTypeId = activitySchedulingTeamRT;			
				
		Implant_Scheduling_Team__c team2 = new Implant_Scheduling_Team__c();			
		team2.Name = 'Test Team 2';
		team2.Team_Color__c = 'rgb(99, 99, 99)';
		team2.Work_Day_End__c = '18';
		team2.Work_Day_Start__c = '8';
		team2.Working_Days__c = '1:5';			
		team2.RecordTypeId = activitySchedulingTeamRT;			
		
		insert new List<Implant_Scheduling_Team__c>{team1, team2};
				
		List<User> otherUsers = [Select Id from User where Profile.Name LIKE 'EUR Field Force CVG%' AND isActive = true LIMIT 10];
		
		List<Implant_Scheduling_Team_Member__c> members = new List<Implant_Scheduling_Team_Member__c>();
		
		Integer i = 0;
		
		for(User otherUser : otherUsers){
			
			Implant_Scheduling_Team_Member__c member = new Implant_Scheduling_Team_Member__c();
			member.Team__c = Math.mod(i, 2) == 0 ? team1.Id : team2.Id;
			member.Member__c = otherUser.Id;				
			
			members.add(member);
			i++;
		}
		
		insert members;		
	}      
}