//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 29-03-2017
//  Description      : APEX Test Class for tr_CreateUserRequest and bl_CreateUserRequest_Trigger
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_bl_CreateUserRequest_Trigger {
	
	private static Create_User_Request__c oCreateUserRequest;

	@isTest static void createTestData() {

		// Create Create_User_Request__c (Support Request)
		oCreateUserRequest = new Create_User_Request__c();          
			oCreateUserRequest.Status__c = 'New';
			oCreateUserRequest.Application__c = 'Salesforce.com';
			oCreateUserRequest.Request_Type__c = 'Reset Password';          
			oCreateUserRequest.Requestor_Email__c = UserInfo.getUserEmail();     
		insert oCreateUserRequest;

	}
	
	@isTest static void test_processStatusChange1() {
		
		//---------------------------------------------
		// Create Test Data
		//---------------------------------------------
		createTestData();
        Map<Id, User> mapUser = bl_CreateUserRequest.loadAdminUsers('Support_Portal_Admins');
        User oUser_Admin = mapUser.values()[0];

			oCreateUserRequest.Status__c = 'In Progress';
			oCreateUserRequest.Sub_Status__c = 'Assigned';
			oCreateUserRequest.Assignee__c = oUser_Admin.Id;
		//---------------------------------------------


		//---------------------------------------------
		// Perform Test
		//---------------------------------------------
		Test.startTest();

		update oCreateUserRequest;

		Test.stopTest();
		//---------------------------------------------


		//---------------------------------------------
		// Validate Result
		//---------------------------------------------
		oCreateUserRequest = [SELECT Id, Last_Status_Change__c, Status_Days__c, Last_Sub_Status_Change__c, Sub_Status_Days__c FROM Create_User_Request__c WHERE Id = :oCreateUserRequest.Id];

		Datetime dtLastStatusChange1 = oCreateUserRequest.Last_Status_Change__c;
		Datetime dtLastSubStatusChange1 = oCreateUserRequest.Last_Sub_Status_Change__c;

		System.assertNotEquals(oCreateUserRequest.Last_Status_Change__c, null);
		System.assertNotEquals(oCreateUserRequest.Last_Sub_Status_Change__c, null);
		//---------------------------------------------

	}


	@isTest static void test_processStatusChange2() {
		
		//---------------------------------------------
		// Create Test Data
		//---------------------------------------------
		createTestData();
        Map<Id, User> mapUser = bl_CreateUserRequest.loadAdminUsers('Support_Portal_Admins');
        User oUser_Admin = mapUser.values()[0];

			oCreateUserRequest.Status__c = 'In Progress';
			oCreateUserRequest.Sub_Status__c = 'Assigned';
			oCreateUserRequest.Assignee__c = oUser_Admin.Id;
		update oCreateUserRequest;

		oCreateUserRequest = [SELECT Id, Last_Status_Change__c, Status_Days__c, Last_Sub_Status_Change__c, Sub_Status_Days__c FROM Create_User_Request__c WHERE Id = :oCreateUserRequest.Id];

		Datetime dtLastStatusChange1 = oCreateUserRequest.Last_Status_Change__c;
		Datetime dtLastSubStatusChange1 = oCreateUserRequest.Last_Sub_Status_Change__c;

		System.assertNotEquals(oCreateUserRequest.Last_Status_Change__c, null);
		System.assertNotEquals(oCreateUserRequest.Last_Sub_Status_Change__c, null);
		//---------------------------------------------


		//---------------------------------------------
		// Perform Test
		//---------------------------------------------
		Test.startTest();


			oCreateUserRequest.Sub_Status__c = 'Awaiting user info';
		update oCreateUserRequest;
				
		Test.stopTest();
		//---------------------------------------------

		// It cannot be validated as the DateTime object only records up to seconds while the code for update happens in miliseconds so both values will be the same.
		//---------------------------------------------
		// Validate Result
		//---------------------------------------------
		/*oCreateUserRequest = [SELECT Id, Last_Status_Change__c, Status_Days__c, Last_Sub_Status_Change__c, Sub_Status_Days__c FROM Create_User_Request__c WHERE Id = :oCreateUserRequest.Id];

		Datetime dtLastStatusChange2 = oCreateUserRequest.Last_Status_Change__c;
		Datetime dtLastSubStatusChange2 = oCreateUserRequest.Last_Sub_Status_Change__c;

		System.assertNotEquals(oCreateUserRequest.Last_Status_Change__c, null);
		System.assertNotEquals(oCreateUserRequest.Last_Sub_Status_Change__c, null);

		System.assertEquals(dtLastStatusChange1, dtLastStatusChange2);
		System.assertNotEquals(dtLastSubStatusChange1.getTime(), dtLastSubStatusChange2.getTime());
		*/
		//---------------------------------------------

	}	
		
}
//--------------------------------------------------------------------------------------------------------------------