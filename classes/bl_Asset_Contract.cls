public class bl_Asset_Contract {
    
    @TestVisible
    private static Set<Id> alreadyProcessedPOAmount = new Set<Id>();
    @TestVisible
    private static Set<Id> alreadyProcessedServiceLevel = new Set<Id>();
    
    public static void calculatePOAmountAndServiceLevel(List<AssetContract__c> triggerNew, Map<Id, AssetContract__c> oldMap){
    	
    	Set<Id> setID_Contract_POAmount = new Set<Id>();
        Set<Id> setID_Contract_ServiceLevel = new Set<Id>();
	
        for (AssetContract__c oAssetContract : triggerNew){
            
            //Insert, Delete, Undelete
            if(oldMap == null){
            	
            	if(clsUtil.isDecimalNull(oAssetContract.PO_Amount2__c, 0) != 0){
                    if(alreadyProcessedPOAmount.add(oAssetContract.Id) == true) setID_Contract_POAmount.add(oAssetContract.Contract__c);
                }

                if(clsUtil.isNull(oAssetContract.Service_Level__c, '') != ''){
                    if(alreadyProcessedServiceLevel.add(oAssetContract.Id) == true) setID_Contract_ServiceLevel.add(oAssetContract.Contract__c);
                }
            
            //Update    
            }else{
            	
            	AssetContract__c oAssetContract_Old = oldMap.get(oAssetContract.Id);

                if (oAssetContract.Contract__c != oAssetContract_Old.Contract__c){
                    // At the moment, the field Contract__c on AssetContract__c is a Master Detail and you can't change this when updating the record --> this code will not be executed/used at the moment
                    if(alreadyProcessedPOAmount.add(oAssetContract.Id) == true){
                    	
                    	setID_Contract_POAmount.add(oAssetContract.Contract__c);
                    	setID_Contract_POAmount.add(oAssetContract_Old.Contract__c);
                    }
					
					if(alreadyProcessedServiceLevel.add(oAssetContract.Id) == true){
                    	
                    	setID_Contract_ServiceLevel.add(oAssetContract.Contract__c);
                    	setID_Contract_ServiceLevel.add(oAssetContract_Old.Contract__c);
					}
                    
                }else{
                    if (clsUtil.isDecimalNull(oAssetContract.PO_Amount2__c, 0) != clsUtil.isDecimalNull(oAssetContract_Old.PO_Amount2__c, 0)){
                        if(alreadyProcessedPOAmount.add(oAssetContract.Id) == true) setID_Contract_POAmount.add(oAssetContract.Contract__c);
                    }

                    if (clsUtil.isNull(oAssetContract.Service_Level__c, '') != clsUtil.isNull(oAssetContract_Old.Service_Level__c, '')){
                        if(alreadyProcessedServiceLevel.add(oAssetContract.Id) == true) setID_Contract_ServiceLevel.add(oAssetContract.Contract__c);
                    }
                }
            }
        }

        Map<Id, Contract> lstContract_Update = new Map<Id, Contract>();

        if (setID_Contract_POAmount.size() > 0){
            // For the collected Contracts, we only need to process the Contracts with PO_Amount_derived_from_linked_assets__c = true
            List<Contract> lstContract = 
                [
                    SELECT 
                        Id, PO_Amount__c, PO_Amount_derived_from_linked_assets__c, Contract_Level__c, CurrencyIsoCode
                    FROM 
                        Contract 
                    WHERE 
                        Id = :setID_Contract_POAmount 
                        AND PO_Amount_derived_from_linked_assets__c = true
                ];
        
            if (lstContract.size() > 0){
                // Perform calculation
                lstContract = bl_Contract.getPOAmountSumFromRelatedAssetContract(lstContract);
                for(Contract contract : lstContract){
                    lstContract_Update.put( contract.Id, contract);
                }
            }
        }

        if (setID_Contract_ServiceLevel.size() > 0){
            List<Contract> lstContract = 
                [
                    SELECT 
                        Id, Contract_Level__c
                    FROM 
                        Contract 
                    WHERE 
                        Id = :setID_Contract_ServiceLevel 
                ];

            lstContract = bl_Contract.getServiceLevelFromRelatedAssetContract(lstContract);
            //Code to avoid adding the same Contract twice in the list to Update. If the contract has been added already for the PO Amount functionality
            //we just copy the value for the Contract Level to the existing copy.
            for(Contract contract : lstContract){
                
                if(lstContract_Update.containsKey(contract.Id)){
                    
                    Contract poAmountContract = lstContract_Update.get(contract.Id);
                    poAmountContract.Contract_Level__c = contract.Contract_Level__c;
                    
                }else{
                    lstContract_Update.put( contract.Id, contract); 
                }                
            }            
        }

        if (lstContract_Update.size() > 0){
            // Update the changed records
            bl_Contract.assetContractCalculationInProcess = true;
            update lstContract_Update.values();
            bl_Contract.assetContractCalculationInProcess = false;
        }
    }
    
    public static void logUserAction(List<AssetContract__c> triggerNew, String operation){
    	
    	//We do not track bulk operations
		if(triggerNew.size() == 1){
				
			for(AssetContract__c a : triggerNew){
				//Only log initial action
				if (!bl_UserAction.userActionInProgress){
					bl_UserAction.logObjectWithParent(UserInfo.getUserId(), operation,'AssetContract__c',a.id,'',a.Asset__c,'Asset');
					bl_UserAction.userActionInProgress = true;  
				} 
			}			
		}
    }
    
    @TestVisible
    private static Set<Id> alreadyCalculated = new Set<Id>();
    
    public static void calculateAssetContractInfo(List<AssetContract__c> triggerNew, Boolean isDelete){
    	    			
		//get all asset Ids related to the triggered assetcontract junction object
		Set<Id> assetIds = new Set<Id>();
		for(AssetContract__c acs : triggerNew){
			if(alreadyCalculated.add(acs.Id) == true && acs.Asset__c != null) assetIds.add(acs.Asset__c);
		}

		
		Set<Id> setID_Asset_ContractNotActive = new Set<Id>();		
		Set<Id> setID_Contract = new Set<Id>();
		for (AssetContract__c oAssetContract : triggerNew){
			if (oAssetContract.Contract__c != null) setID_Contract.add(oAssetContract.Contract__c);
		}
		Map<Id, Contract> mapContract = new Map<Id, Contract>([SELECT Id, Status FROM Contract WHERE Id = :setID_Contract AND Status in ('Expired', 'Canceled')]);

		for (AssetContract__c oAssetContract : triggerNew){
			
			if (oAssetContract.Contract__c != null){

				if (mapContract.containsKey(oAssetContract.Contract__c)) setID_Asset_ContractNotActive.add(oAssetContract.Asset__c);

			}
		}
	    
		 
		if (assetIds.isEmpty() && setID_Asset_ContractNotActive.isEmpty()) return;

		// We can only process the AssetContract__c records for which the Asset Status is not "Obsolete", "Removed" or "Returned to US" 
		if (isDelete == false){ 
			Map<Id, Asset> mapAsset = new Map<Id, Asset>([SELECT Id FROM Asset WHERE Id = :assetIds AND Status not in ('Obsolete', 'Removed', 'Returned to US')]); 
			assetIds = mapAsset.keySet(); 
		} 

		// Remove the Asset that are linked to and Expired / Canceled Contract from the processing Asset List;
		assetIds.removeAll(setID_Asset_ContractNotActive);			
	                        

		if (assetIds.size() > 0) bl_Contract.calculateCurrentContract(assetIds);
		if (setID_Asset_ContractNotActive.size() > 0) bl_Contract.calculateContractNotActive(setID_Asset_ContractNotActive);
    
	}
    
    public static void setContractAniversaryFlag(List<AssetContract__c> triggerNew){
    	    	
    	Set<Id> contractIds = new Set<Id>();
    	    	
    	for(AssetContract__c assetContract : triggerNew){
    		
          contractIds.add(assetContract.Contract__c);          
       	}       
       
    	List<Contract> contracts = [select Id, Entitlement_Anniversary_Check__c from Contract where Id IN :contractIds AND Entitlement_Anniversary_Check__c = false];

    	for(Contract contract : contracts){
    		
        	contract.Entitlement_Anniversary_Check__c= true;        	
        }
        
        bl_Contract.assetContractCalculationInProcess = true;
        if(contracts.size() > 0) update contracts;
        bl_Contract.assetContractCalculationInProcess = false;
    }


	//-----------------------------------------------------------------------------------------------------------
	// Update the DIEN_Code__c on AssetContract__c based on the Asset CFN Code and AssetContract Service Level
	//-----------------------------------------------------------------------------------------------------------
	public static void updateDIENCode(List<AssetContract__c> lstTriggerNew, Map<Id, AssetContract__c> mapTriggerOld){
	
        if (bl_Trigger_Deactivation.isTriggerDeactivated('ac_updateDIENCode')) return;

		Set<Id> setID_Asset = new Set<Id>();
		List<AssetContract__c> lstAssetContract_Processing = new List<AssetContract__c>();
		for (AssetContract__c oAssetContract : lstTriggerNew){
			
			if (String.isBlank(oAssetContract.DIEN_Code__c)){
			
				setID_Asset.add(oAssetContract.Asset__c);
				lstAssetContract_Processing.add(oAssetContract);
			
			}else{
			
				if (mapTriggerOld != null){
					
					AssetContract__c oAssetContract_OLD = mapTriggerOld.get(oAssetContract.Id);

					if (
						(String.isBlank(oAssetContract.DIEN_Code__c))
						|| (oAssetContract.Service_Level__c != oAssetContract_OLD.Service_Level__c)
					){
						
						setID_Asset.add(oAssetContract.Asset__c);
						lstAssetContract_Processing.add(oAssetContract);
		
					}
				
				}

			}

		}

		if (lstAssetContract_Processing.size() == 0) return;

		Map<Id, Asset> mapAsset = new Map<Id, Asset>([SELECT Id, CFN_Text__c FROM Asset WHERE Id = :setID_Asset]);

		for (AssetContract__c oAssetContract : lstAssetContract_Processing){
			
			String tCFNCode = mapAsset.get(oAssetContract.Asset__c).CFN_Text__c;

			String tDIENCode = clsUtil.getDIENCode(tCFNCode, oAssetContract.Service_Level__c);
			
			oAssetContract.DIEN_Code__c = tDIENCode;

		}

	}

}