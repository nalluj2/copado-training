public with sharing class ctrlExt_Account_TeamMember_New {
	
	private Map<String, String> inputParams;
	private Account_Team_Member__c teamMember;
	
	public ctrlExt_Account_TeamMember_New(ApexPages.StandardController sc){		
		
		teamMember = (Account_Team_Member__c) sc.getRecord();	
		inputParams = ApexPages.currentPage().getParameters();
		inputParams.remove('sfdc.override');
	}
	
	public PageReference redirectToStandardPage(){
		
		Map<String, DIB_Country__c> countries = new Map<String, DIB_Country__c>();
		
		for(DIB_Country__c country : [Select Name, Allow_Inactive_Users_as_Members__c from DIB_Country__c]){
			countries.put(country.Name.toUpperCase(), country);
		}
		
		User currentUser = [Select CountryOR__c from User where Id=:UserInfo.getUserId()];
		
		DIB_Country__c userCountry = countries.get(currentUser.CountryOR__c.toUpperCase());
		
		//If we find the country configuration and it allows inactive Users, then we stay in the page
		if(userCountry != null && userCountry.Allow_Inactive_Users_as_Members__c == true) return null;
		
		//Else we redirect to the standard New page
		String targetURL;
		
		if(teamMember.Id == null){		
			targetURL = Account_Team_Member__c.sObjectType.getDescribe().getKeyPrefix();		
		}else{
			targetURL = teamMember.Id;
		}
		
		PageReference pr = new PageReference('/'+targetURL+'/e');
		pr.setRedirect(true);		
		pr.getParameters().putAll(inputParams);	
		pr.getParameters().put( 'nooverride' , '1');
		
		return pr;
	}
}