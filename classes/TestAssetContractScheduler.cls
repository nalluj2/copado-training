@isTest
private class TestAssetContractScheduler {

    static testMethod void TestAssetContractScheduler () {

		//--------------------------------------------------------------------
		// Create Test Data
		//--------------------------------------------------------------------
        Account oAccount = new Account() ; 
            oAccount.Name = 'Test account'; 
            oAccount.SAP_ID__c = '045566651' ; 
            oAccount.Account_Country_vs__c = 'ITALY';
            oAccount.RecordTypeId = clsUtil.getRecordTypeByDevName('Account', 'SAP_Account').Id;
        insert oAccount; 

        clsTestData_Account.oMain_Account = oAccount;
        Asset oAsset = clsTestData_Asset.createAsset(false)[0];
            oAsset.Name = 'test';
            oAsset.AccountId = oAccount.Id;
            oAsset.Asset_Type_Picklist__c = 'Programmer';   
            oAsset.Price = 1;
        insert oAsset;
    	    	
    	Contract oContract = new Contract();
			oContract.RecordTypeId = clsUtil.getRecordTypeByDevName('Contract', 'RTG_Service_Contract').Id;
        	oContract.AccountId = oAccount.Id;
        	oContract.Agreement_level_Picklist__c = 'Service';
        	oContract.StartDate = Date.Today().addMonths(-7);
        	oContract.ContractTerm = 6;
    	insert oContract;

        AssetContract__c oAssetContract = new AssetContract__c();
            oAssetContract.Asset__c = oAsset.Id;
            oAssetContract.Contract__c = oContract.Id;
        insert oAssetContract;
                        
	        oContract.Status = 'Activated';
		update oContract;

		List<Contract> lstContract = [SELECT Id, Status FROM Contract WHERE Id = :oContract.Id];
		System.assertEquals(lstContract.size(), 1);
		System.assertEquals(lstContract[0].Status, 'Activated');
		//--------------------------------------------------------------------


		//--------------------------------------------------------------------
		// Execute Logic
		//--------------------------------------------------------------------
		Test.startTest();
		        
			AssetContractScheduler oBatch = new AssetContractScheduler();
			Database.executebatch(oBatch, 1);        	

		Test.stopTest();
		//--------------------------------------------------------------------


		//--------------------------------------------------------------------
		// Validate Result
		//--------------------------------------------------------------------
		lstContract = [SELECT Id, Status FROM Contract WHERE Id = :oContract.Id];
		System.assertEquals(lstContract.size(), 1);
		System.assertEquals(lstContract[0].Status, 'Expired');
		//--------------------------------------------------------------------

    }

}