public with sharing class bl_TerritoryChange {
	
	@future
	public static void changeTerritory(String operation, String name, String developerName, String description, String parentId, String businessUnitName,
		String companyId, String countryUid, String shortDescription, String terrType, String therapyGroups){
		
		Territory2 t = new Territory2();
		t.name = name;
		t.DeveloperName =developerName;
		t.Description = description;
		t.ParentTerritory2Id = parentId; 
		t.Business_Unit__c = businessUnitName;
		t.Company__c = companyId;
		t.Country_UID__c = countryUid;
		t.Short_Description__c= shortDescription;
		t.Territory2TypeId = [Select Id from Territory2Type where DeveloperName = :terrType].Id;
		t.Therapy_Groups_Text__c= therapyGroups;
		
		if (operation.equals('CREATE')){
			insert t;	
		}
		
		
	}
	
	@future
	public static void changeTerritories(List<Id> territoryChanges){
		
		List<Territory_Change__c> tcList = [SELECT External_Key__c, Business_Unit__c,Company__c,Country_UID__c,Id,Name__c, company__r.id,Territory_Id__c,
			Operation__c,Parent_Id__c,Short_Description__c,Therapy_Groups_Text__c,Type__c, Label__c, Business_Unit__r.id, Business_Unit__r.name 
			FROM Territory_Change__c WHERE Id in :territoryChanges];
		
		System.debug('territory changes to process '+tcList);
		
		List<Territory2> toUpdate = new List<Territory2>();
		List<Territory2> toCreate = new List<Territory2>();
		Map<String,Territory_Change__c> territoryNameChangeMap = new Map<String, Territory_Change__c>();
		for (Territory_Change__c tc : tcList){

			territoryNameChangeMap.put(tc.Name__c,tc);
			Territory2 t = new Territory2();
			if (tc.Operation__c.equals('CREATE')){
				toCreate.add(mapChangeToTerritory(tc,t));
			}
			if (tc.Operation__c.equals('UPDATE')){
				t.id= tc.Territory_Id__c;
				toUpdate.add(mapChangeToTerritory(tc,t));
			}
		}	

		System.debug('territory creates to process '+toCreate);
		System.debug('territory updates to process '+toUpdate);			
		System.debug('territory name Change map '+territoryNameChangeMap);
		
		Savepoint sp = Database.setSavepoint();
		
		try{
			if (toCreate.size()>0){
				insert toCreate;
			}
			if (toUpdate.size()>0){
				update toUpdate;
			}
		}catch (Exception e){
			
			Database.rollback(sp);
			
			List<String> sendTo = new List<String>();
									
			for(User notifyUser : [Select Email from User where Id IN (Select UserOrGroupId from GroupMember where Group.Name = 'Territory Structure Admins')]){
						
      			sendTo.add(notifyUser.Email);
			}
      		
			sendEmail(sendTo,'noreply@medtronic.com','System','Territory change error','Error in TerritoryChange: \n'+e.getMessage());      		
		}		
	}
	
	public static void sendEmail(List<String> to, String replyTo, String senderDisplayName, String subject, String body){
		List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
  		mail.setToAddresses(to);
  		mail.setReplyTo(replyTo);
  		mail.setSenderDisplayName(senderDisplayName);
  		mail.setSubject(subject);
  		mail.setHtmlBody(body);
  		mails.add(mail);
  		Messaging.sendEmail(mails);
      		
	}
	
	public static void changeTerritoriesDelete(List<Id> territoryChanges){
		
		List<Territory_Change__c> tcList = [SELECT External_Key__c, Business_Unit__c,Company__c,Country_UID__c,Id,Name__c, company__r.id,Territory_Id__c,
			Operation__c,Parent_Id__c,Short_Description__c,Therapy_Groups_Text__c,Type__c, Label__c, Business_Unit__r.id, Business_Unit__r.name 
			FROM Territory_Change__c WHERE Id in :territoryChanges];
		
		System.debug('territory changes to process '+tcList);
		
		List<Territory2> toDelete = new List<Territory2>();
		Map<String,Territory_Change__c> territoryNameChangeMap = new Map<String, Territory_Change__c>();
		for (Territory_Change__c tc : tcList){

			territoryNameChangeMap.put(tc.Name__c,tc);
			Territory2 t = new Territory2();
			if (tc.Operation__c.equals('DELETE')){
				t.id= tc.Territory_Id__c;
				toDelete.add(mapChangeToTerritory(tc,t));
			}
		}	

		System.debug('territory deletes to process '+toDelete);			
		System.debug('territory name Change map '+territoryNameChangeMap);
		if (toDelete.size()>0){
			doSoapCallRemove(toDelete, UserInfo.getSessionId());
		}
		
		
	}
	

	public static void doSoapCallRemove(List<Territory2> territoriesToRemove, String sessionId){

        String body = '<urn:delete>\n';
       
        for (Territory2 tc : territoriesToRemove){
            body+='<ids>'+tc.id+'</ids>\n';
        }
        
        body += '</urn:delete>\n';
            
        ws_SFDCSoapInterface.doSoapCall(body, sessionId);       
    }
	
	public static Territory2 mapChangeToTerritory(Territory_Change__c tc, Territory2 t){
		t.name = tc.Label__c;
		t.DeveloperName = tc.Name__c;
		t.Description = '';
		t.ParentTerritory2Id = tc.Parent_Id__c; 
		t.Business_Unit__c = tc.Business_Unit__r.name;
		t.Company__c = tc.company__r.id;
		t.Country_UID__c = tc.Country_UID__c;
		t.Short_Description__c= tc.Short_Description__c;
		t.Territory2TypeId = [Select Id from Territory2Type where developerName = :tc.Type__c].Id;
		t.Therapy_Groups_Text__c=tc.Therapy_Groups_Text__c;
		t.External_Key__c = tc.External_Key__c;
		t.AccountAccessLevel = 'Edit';
		return t;
	}
	
	
	public static List<Territory_Change__c> linkRelationshipDataToTerritoryChange(List<Territory_Change__c> territoryChanges){
		
		Set<String> buNames=new Set<String>();
		Set<String> companyNames = new Set<String>(); 
		Set<String> territoryNames = new Set<String>();
		Set<String> terrExternalKeys = new Set<String>();
		Set<String> parentTerritoryNames = new Set<String>();
		Set<String> terrParentExternalKeys = new Set<String>();
		
		for (Territory_Change__c tc :territoryChanges){
			buNames.add(tc.Business_Unit_Name__c);
			companyNames.add(tc.Company_Name__c);
			territoryNames.add(tc.label__c);
			parentTerritoryNames.add(tc.Parent_Name__c);
			if (tc.External_Key__c!=null && tc.External_Key__c.length()>0){
				terrExternalKeys.add(tc.External_Key__c);
			}
			if (tc.Parent_External_Key__c!=null && tc.Parent_External_Key__c.length()>0){
				terrParentExternalKeys.add(tc.Parent_External_Key__c);
			}
		}
		
		System.debug('buNames : '+buNames);
		System.debug('companyNames : '+companyNames);
		System.debug('territoryNames : '+territoryNames);
		System.debug('parentTerritoryNames : '+parentTerritoryNames);
		System.debug('terrExternalKeys : '+terrExternalKeys);
		System.debug('terrParentExternalKeys : '+terrParentExternalKeys);
		
		List<Business_Unit__c> businessUnits = [select Id, name, Company__c from Business_Unit__c where name in :buNames];
		List<Company__c> companies = [select Id, name from Company__c where name in :companyNames];
		List<Territory2> territories = [select Id, name from Territory2 where name in :territoryNames];
		List<Territory2> parentTerritories = [select Id, name from Territory2 where name in :parentTerritoryNames];
		List<Territory2> territoriesExtKey = [select Id, name, external_key__c from Territory2 where external_key__c in :terrExternalKeys];
		List<Territory2> parentTerritoriesExtKey = new List<Territory2>();
		if (terrParentExternalKeys.size()>0){
			parentTerritoriesExtKey= [select Id, name, external_key__c from Territory2 where external_key__c in :terrParentExternalKeys];
		}
		Map<String,Business_Unit__c> buMap = new Map<String,Business_Unit__c>();
		Map<String,Company__c> companyMap = new Map<String,Company__c>();
		Map<String,Territory2> territoryMap = new Map<String,Territory2>();
		Map<String,Territory2> parentTerritoryMap = new Map<String,Territory2>();
		Map<String,Territory2> territoryExtKeyMap = new Map<String,Territory2>();
		Map<String,Territory2> parentTerritoryExtKeyMap = new Map<String,Territory2>();
		
		for (Business_Unit__c bu : businessUnits){
			buMap.put(bu.name+bu.Company__c,bu);
		}

		for (Company__c c : companies){
			companyMap.put(c.name,c);
		}		
		
		for (Territory2 t : territories){
			territoryMap.put(t.name, t);
		}		

		
		for (Territory2 t : parentTerritories){
			parentTerritoryMap.put(t.name, t);
		}		

		for (Territory2 t : territoriesExtKey){
			territoryExtKeyMap.put(t.external_key__c, t);
		}		

		for (Territory2 t : parentTerritoriesExtKey){
			parentTerritoryExtKeyMap.put(t.external_key__c, t);
		}		

		for (Territory_Change__c tc : territoryChanges){
			Company__c c = companyMap.get(tc.Company_Name__c);
			if (c ==null){
				tc.addError('Company not found');
			}else{ 
				tc.company__c = c.id;
			}
			
			if (tc.Business_Unit_Name__c!=null && tc.Business_Unit_Name__c.compareTo('All')!=null){
				Business_Unit__c bu = buMap.get(tc.Business_Unit_Name__c+tc.Company__c);
				if (bu == null ){
					tc.addError('BU not found');
				}else{
					tc.Business_Unit__c = bu.id;
				}
			}
			
			
			if (tc.Operation__c!=null && !tc.Operation__c.equals('CREATE')){
				System.debug('territoryExtKeyMap '+territoryExtKeyMap);
				Territory2 t = null;
				t = territoryExtKeyMap.get(tc.external_key__c);
				if (t == null){
					t=territoryMap.get(tc.label__c);
					if (t == null){
						tc.addError('Existing territory not found');
					}
				}
				 
				if (t != null){
					tc.Territory_Id__c = t.id;
				}
			}
			
			if (tc.Operation__c!=null && !tc.Operation__c.equals('DELETE')){
				System.debug('parentTerritoryExtKeyMap '+parentTerritoryExtKeyMap+' tc '+tc);
				Territory2 parentT = null;
				
				parentT = parentTerritoryExtKeyMap.get(tc.Parent_External_Key__c);
				if (parentT == null){
					parentT = parentTerritoryMap.get(tc.Parent_Name__c);
					if (parentT == null){
						tc.addError('Parent Territory not found');
					}
				}
				
				if (parentT != null){
					tc.Parent_Id__c = parentT.id;
				}
			}
			
			
		}
		return territoryChanges;
	}
	
	

}