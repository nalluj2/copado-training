/*
 *      Created Date    : 20140811
 *      Author          : Bart Caelen
 *      Description     : TEST Class for the APEX Trigger tr_CallRecord_UpdateParent(
 */
@isTest private class Test_tr_CallRecord_UpdateParent {
	
	@isTest static void test_tr_CallRecord_UpdateParent() {


		//---------------------------------------------------
		// CREATE TEST DATA
		//---------------------------------------------------
		List<Call_Records__c> lstCallRecord_Delete 	= new List<Call_Records__c>();
		clsTestData.createAccountPlan2Data();

        clsTestData.iRecord_CallRecord 	= 1;

        clsTestData.createCallRecordData(false);
        clsTestData.oMain_CallRecord.Account_Plan__c = clsTestData.oMain_AccountPlan2.Id;
		//---------------------------------------------------

		Test.startTest();

        insert clsTestData.oMain_CallRecord;
		delete clsTestData.oMain_CallRecord;

		Test.stopTest();
	}
	
	
}