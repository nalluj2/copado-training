/*
  *  Class Name    : Case_Logic
  *  Description   : This class include logic to create comment for Case Part
  *  Created Date  : 8/14/2013
  *  Author        : Wipro Tech. 
*/
public with sharing class Comment_Logic {
	
	public static boolean firstRun = true;
	public static Set<Id> commentToNewComplaint  = new Set<Id>();
    
    public static void CreateCommentFromCP(List<Workorder_Sparepart__c> newCaseParts, Map<Id, Workorder_Sparepart__c> oldCaseParts) {
        
        // If we receive an update from US to EMEA we don't create comments because they are already created in US
    	if(bl_SynchronizationService_Utils.isSyncProcess == true) return;
        
        List<Case_Comment__c> comments = new List<Case_Comment__c>();
        
        //get all associated contacts for case parts so that we can get their information when creating comments.        
        Set<Id> productIds = new Set<Id>();
        Set<Id> softwareIds = new Set<Id>();
        
        for(Workorder_Sparepart__c newCasePart : newCaseParts)
        {
        	//get new part Ids			
			if (newCasePart.Order_Part__c != null) productIds.add(newCasePart.Order_Part__c);
			if (newCasePart.RI_Part__c != null) productIds.add(newCasePart.RI_Part__c);
			if (newCasePart.Software__c != null) softwareIds.add(newCasePart.Software__c);
			
			//get old part Ids
			if (oldCaseParts != null)
			{				
				if (oldCaseParts.get(newCasePart.Id).Order_Part__c != null) productIds.add(oldCaseParts.get(newCasePart.Id).Order_Part__c);
				if (oldCaseParts.get(newCasePart.Id).RI_Part__c != null) productIds.add(oldCaseParts.get(newCasePart.Id).RI_Part__c);
				if (oldCaseParts.get(newCasePart.Id).Software__c != null) softwareIds.add(oldCaseParts.get(newCasePart.Id).Software__c);
			}
        }
        
        //get the associated contacts from the Ids        
        Map<Id, Product2> associatedProducts = new Map<Id, Product2>();
        if(productIds.size() > 0) associatedProducts = new Map<Id, Product2>([SELECT Id, Name FROM Product2 WHERE Id IN :productIds]);
        
        Map<Id, Software__c> associatedSoftware = new Map<Id, Software__c>();
        if(softwareIds.size() > 0) associatedSoftware = new Map<Id, Software__c>([SELECT Id, Name FROM Software__c WHERE Id IN :softwareIds]);
                
        for(Workorder_Sparepart__c newCasePart : newCaseParts){
        	
        	Workorder_Sparepart__c oldCasePart;            	
            if (oldCaseParts != null) oldCasePart = oldCaseParts.get(newCasePart.Id);
        	
            if(newCasePart.Complaint__c != null) {
                comments.addall(concatenateComment(newCasePart, oldCasePart, associatedProducts, associatedSoftware));                
            }
        }
        
        if(comments.size() > 0) insert comments;
    }
    
    //Method to concatenate commment from Case Part and create comment record
    public static List<Case_Comment__c> concatenateComment(Workorder_Sparepart__c newCasePart, Workorder_Sparepart__c oldCasePart, Map<Id, Product2> associatedProducts, Map<Id, Software__c> associatedSoftware){
        
        Map<ID,Schema.RecordTypeInfo> rtMap = Workorder_Sparepart__c.sObjectType.getDescribe().getRecordTypeInfosById();
        
        String strComment = '';        
        
        if(oldCasePart == null || (newCasePart.RecordTypeId != oldCasePart.RecordTypeId)){
	
			if(oldCasePart == null  && newCasePart.RecordTypeId != null) strComment += '\nRecord type: ' + rtMap.get(newCasePart.RecordTypeId).getName();
			else if (oldCasePart != null) strComment += '\nRecord type updated from ' + rtMap.get(oldCasePart.RecordTypeId).getName() + ' to ' + rtMap.get(newCasePart.RecordTypeId).getName() ; 
		}   
        if(oldCasePart == null || (newCasePart.BAI_OOBF__c != oldCasePart.BAI_OOBF__c)){
           	if(oldCasePart == null  && newCasePart.BAI_OOBF__c == true)
           		strComment += '\nBAI OOBF: ' + newCasePart.BAI_OOBF__c;
           	else if (oldCasePart != null)
				strComment += '\nBAI OOBF updated from ' + oldCasePart.BAI_OOBF__c + ' to ' + newCasePart.BAI_OOBF__c; 
        }    		          
        if(oldCasePart == null || (newCasePart.Order_Part__c != oldCasePart.Order_Part__c)){
           
           	String orderPart = newCasePart.Order_Part__c != null ? associatedProducts.get(newCasePart.Order_Part__c).Name : null;
           	String oldOrderPart = oldCasePart!= null && oldCasePart.Order_Part__c != null ? associatedProducts.get(oldCasePart.Order_Part__c).Name : null;
           
           	if(oldCasePart == null  && newCasePart.Order_Part__c != null)
           		strComment += '\nOrder Part: ' + orderPart;
           	else if (oldCasePart != null)
				strComment += '\nOrder Part updated from ' + oldOrderPart + ' to ' + orderPart; 
        }
        if(oldCasePart == null || (newCasePart.Software__c != oldCasePart.Software__c)){  
        	        	
           	String software = newCasePart.Software__c != null ? associatedSoftware.get(newCasePart.Software__c).Name : null;
           	String oldSoftware = oldCasePart!= null && oldCasePart.Software__c != null ? associatedSoftware.get(oldCasePart.Software__c).Name : null;
                   	
        	if(oldCasePart == null  && newCasePart.Software__c != null)
           		strComment += '\nSoftware: ' + software;
           	else if (oldCasePart != null && (oldCasePart.Software__c != null || newCasePart.Software__c != null))
				strComment += '\nSoftware updated from ' + oldSoftware + ' to ' + software; 
        }       
        if(oldCasePart == null || (newCasePart.RI_Part__c != oldCasePart.RI_Part__c)){
           
           	String returnPart = newCasePart.RI_Part__c != null ? associatedProducts.get(newCasePart.RI_Part__c).Name : null;
           	String oldReturnPart = oldCasePart!= null && oldCasePart.RI_Part__c != null ? associatedProducts.get(oldCasePart.RI_Part__c).Name : null;
           
           	if(oldCasePart == null  && newCasePart.RI_Part__c != null)
           		strComment += '\nReturn Part: ' + returnPart;
           	else if (oldCasePart != null && (oldCasePart.RI_Part__c != null || newCasePart.RI_Part__c != null))
				strComment += '\nReturn Part updated from ' + oldReturnPart + ' to ' + returnPart; 
        }
        if(oldCasePart == null || (newCasePart.Lot_Number__c != oldCasePart.Lot_Number__c)){
           	if(oldCasePart == null  && newCasePart.Lot_Number__c != null)
           		strComment += '\nLot #: ' + newCasePart.Lot_Number__c;
           	else if (oldCasePart != null)
				strComment += '\nLot # updated from ' + oldCasePart.Lot_Number__c + ' to ' + newCasePart.Lot_Number__c; 
        }
        if(oldCasePart == null || (newCasePart.Return_Item_Status__c != oldCasePart.Return_Item_Status__c)){
            if(oldCasePart == null  && newCasePart.Return_Item_Status__c != null)
           		strComment += '\nReturn Item Status: ' + newCasePart.Return_Item_Status__c;
           	else if (oldCasePart != null)
				strComment += '\nReturn Item Status updated from ' + oldCasePart.Return_Item_Status__c + ' to ' + newCasePart.Return_Item_Status__c; 
        }
        if(oldCasePart == null || (newCasePart.Return_item_Tracking_info_DC_to_MFR__c != oldCasePart.Return_item_Tracking_info_DC_to_MFR__c)){
            
            if(oldCasePart == null  && newCasePart.Return_item_Tracking_info_DC_to_MFR__c != null) strComment += '\nReturn item Tracking info (DC to MFR): ' + newCasePart.Return_item_Tracking_info_DC_to_MFR__c;
           	else if (oldCasePart != null) strComment += '\nReturn item Tracking info (DC to MFR) updated from ' + oldCasePart.Return_item_Tracking_info_DC_to_MFR__c + ' to ' + newCasePart.Return_item_Tracking_info_DC_to_MFR__c; 
        }
        if(oldCasePart == null || (newCasePart.Requested_Return__c != oldCasePart.Requested_Return__c)){
            
            if(oldCasePart == null  && newCasePart.Requested_Return__c != null) strComment += '\nRequested Return: ' + newCasePart.Requested_Return__c;
            else if (oldCasePart != null) strComment += '\nRequested Return updated from ' + oldCasePart.Requested_Return__c  + ' to ' + newCasePart.Requested_Return__c; 
        }
        if(oldCasePart == null || (newCasePart.Possession__c != oldCasePart.Possession__c)){
            
            if(oldCasePart == null  && newCasePart.Possession__c != null) strComment += '\nPossession: ' + newCasePart.Possession__c;
            else if (oldCasePart != null) strComment += '\nPossession updated from ' + oldCasePart.Possession__c  + ' to ' + newCasePart.Possession__c; 
        }
        if(oldCasePart == null || (newCasePart.No_Return_Justification__c != oldCasePart.No_Return_Justification__c)){
            
            if(oldCasePart == null  && newCasePart.No_Return_Justification__c != null) strComment += '\nNo Return Justification: ' + newCasePart.No_Return_Justification__c;
            else if (oldCasePart != null) strComment += '\nNo Return Justification updated from ' + oldCasePart.No_Return_Justification__c  + ' to ' + newCasePart.No_Return_Justification__c; 
        }
        if(oldCasePart == null || (newCasePart.SAP_Return__c != oldCasePart.SAP_Return__c)){
            
            if(oldCasePart == null  && newCasePart.SAP_Return__c != null) strComment += '\nSAP Return #: ' + newCasePart.SAP_Return__c;
            else if (oldCasePart != null) strComment += '\nSAP Return # updated from ' + oldCasePart.SAP_Return__c  + ' to ' + newCasePart.SAP_Return__c; 
        } 
        if(oldCasePart == null || (newCasePart.Return_Item_Tracking_Number__c != oldCasePart.Return_Item_Tracking_Number__c)){
            
            if(oldCasePart == null  && newCasePart.Return_Item_Tracking_Number__c != null) strComment += '\nReturn Item Tracking Number: ' + newCasePart.Return_Item_Tracking_Number__c;
            else if (oldCasePart != null) strComment += '\nReturn Item Tracking Number updated from ' + oldCasePart.Return_Item_Tracking_Number__c  + ' to ' + newCasePart.Return_Item_Tracking_Number__c; 
        } 
        if(oldCasePart == null || (newCasePart.ZRIC__c != oldCasePart.ZRIC__c)){
            
            if(oldCasePart == null  && newCasePart.ZRIC__c != null) strComment += '\nZRIC #: ' + newCasePart.ZRIC__c;
            else if (oldCasePart != null) strComment += '\nZRIC # updated from ' + oldCasePart.ZRIC__c  + ' to ' + newCasePart.ZRIC__c; 
        }
       
        //if comment size is more than 32768 creating multiple case comment records 
        List<Case_Comment__c> newCaseComments = new List<Case_Comment__c>();          
        if(strComment != null && strComment != '' && strComment.length() > 32768) 
        {
            for(Integer i = 0; i < strComment.length(); i++)
            {
                String commentSubstring = null;
                if(strComment.length() > 32767)
                    commentSubstring = strComment.substring(0,32767); 
                else
                    commentSubstring = strComment;
                    
                Case_Comment__c objCom    = new Case_Comment__c();
                objCom.Complaint__c       = newCasePart.Complaint__c;
                if(oldCasePart == null) objCom.Comment_Subject__c = 'Case Part '+ newCasePart.Name + ' added to Complaint';
                else objCom.Comment_Subject__c = 'Auto created from '+newCasePart.Name;
                objCom.Comment__c         = commentSubstring; 
                objCom.Case_Part__c       = newCasePart.id;
                                
                newCaseComments.add(objCom);                     
                
                strComment = strComment.remove(commentSubstring);
            }           
        }
        
        //if comment size is less than 32768 creating single case comment records
        else if(strComment != null && strComment != '' && strComment.length() < 32768)
        {
            
            Case_Comment__c objCom    = new Case_Comment__c();
            objCom.Complaint__c       = newCasePart.Complaint__c;
            if(oldCasePart == null) objCom.Comment_Subject__c = 'Case Part '+ newCasePart.Name + ' added to Complaint';
            else objCom.Comment_Subject__c = 'Auto created from '+newCasePart.Name;
            objCom.Comment__c         = strComment;            
            objCom.Case_Part__c       = newCasePart.id;
            
            newCaseComments.add(objCom);
        }
        
        system.debug('Comment_Logic lstCaseComment: ' + newCaseComments);
        
        return newCaseComments;
        //concatenating failures into a string
    }
    
    //Class to manage the logic around Comments
    public static Boolean updateComplaintFlag = False;
    public static void UpdateComplaintNumber(List<Case_Comment__c> objCommentList) {
        
        Set<Id> objCaseSet = new Set<Id>();
        Map<id,Case> mapOfCaseForEachId=new Map<id,Case>();
        
        List<Case_Comment__c> commentList=new List<Case_Comment__c>();
        
        // When a new Comment gets created, check the related Case for Complaint Number. 
        for(Case_Comment__c objComment:objCommentList)
        {
            if(objComment.Case__c != null)
            {
                objCaseSet.add(objComment.Case__c);
            }
        } 
        //If the Complaint Number is available in case  
        if(objCaseSet.size() > 0){
            for(Case objCase :[Select id,Complaint__c FROM Case WHERE id IN : objCaseSet]) {
                if(objCase.Complaint__c != null)
                  mapOfCaseForEachId.put(objCase.id,objCase);
             }
       } 
       // Then update Comment’s Complaint Number field with the Complaint Number captured from the Case.
        if(mapOfCaseForEachId.size() > 0){            
            for(Case_Comment__c objComment1:objCommentList) {
                if(mapOfCaseForEachId.get(objComment1.Case__c) != null)
                    objComment1.Complaint__c = mapOfCaseForEachId.get(objComment1.Case__c).Complaint__c;
                // Updating Comment
                commentList.add(objComment1);
             }
        } 
         system.debug('commentList@@@@@@@' +commentList);
        updateComplaintFlag=true;
    }
    
}