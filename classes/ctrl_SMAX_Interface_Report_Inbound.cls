public without sharing class ctrl_SMAX_Interface_Report_Inbound {
    
    transient public List<FailedRecord> reportItems {get; private set;}
    public String headers {get; set;}
    public String scope {get; set;}
    
    public ctrl_SMAX_Interface_Report_Inbound(){
    	
    	reportItems = new List<FailedRecord>();
    	
    	Map<String, String> inputParams = ApexPages.currentPage().getParameters();
    	
    	scope = inputParams.get('scope');
    	    	    	    	
    	if(scope != null){
    	        	 			
			reportItems = new List<FailedRecord>();
			
			if(scope == 'Case') headers = '"Id","SAP Id","SFDC Id","Status","Notification Status","SAP Type","Country","Error Message","Created Date"';
			else headers = '"Id","SAP Id","SFDC Id","Status","Notification Status","Work Center","Technician","SAP Type","Country","Error Message","Created Date"';
			
			for(Notification_Grouping__c nGrouping : [Select Id, Internal_ID__c, External_ID__c, Related_Record_Status__c, Related_System_Status__c, Country__c, Related_Record_Type__c, Related_Record_Technician__c, Related_Record_Work_Center__c, Last_Inbound_Message__c, Last_Inbound_Message__r.Response__c, Last_Inbound_Message__r.CreatedDate FROM Notification_Grouping__c where Scope__c = :scope AND Inbound_Status__c = 'Failure' AND Archived__c = false ORDER BY LastModifiedDate ASC LIMIT 10000]){
				
				FailedRecord oMessage = new FailedRecord();
				oMessage.Id = nGrouping.Last_Inbound_Message__c;				
				oMessage.SFDC_Id = nGrouping.Internal_ID__c;
				oMessage.SAP_Id = nGrouping.External_ID__c;
				oMessage.Status = nGrouping.Related_Record_Status__c;
				oMessage.Notification_Status = nGrouping.Related_System_Status__c;
				oMessage.CreatedDate = nGrouping.Last_Inbound_Message__r.CreatedDate.format();
				oMessage.Country = nGrouping.Country__c;								
				oMessage.SAP_type = nGrouping.Related_Record_Type__c;
				oMessage.Work_Center = nGrouping.Related_Record_Work_Center__c;
				oMessage.Technician = nGrouping.Related_Record_Technician__c;
								
				ResponseMessage resp = (ResponseMessage) JSON.deserialize(nGrouping.Last_Inbound_Message__r.Response__c, ResponseMessage.class);			
				oMessage.Error = resp.DIST_ERROR_DETAILS;			
								
				reportItems.add(oMessage);	
			}			
    	}
    }
        
    public class FailedRecord{
    
    	public String Id {get; set;}
    	public String SFDC_Id {get; set;}
    	public String SAP_Id {get; set;}
    	public String Status {get; set;}
		public String Notification_Status {get; set;}
    	public String Country {get; set;}
    	public String CreatedDate  {get; set;}
    	public String error {get; set;}	  
    	public String SAP_type {get; set;}
    	public String Work_Center {get; set;}
    	public String Technician {get; set;}
    }
    
    private class ResponseMessage{
    	
    	public String DIST_ERROR_DETAILS {get; set;}    
    }        
}