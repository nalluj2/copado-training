/*
 *      Description : This class exposes methods to create / update / delete a CVG Project Opportunity Line Item
 *
 *      Author = Rudy De Coninck
 */
@RestResource(urlMapping='/CVGProjectOpportunityLineItemService/*')
global class ws_CVGProjectOpportunityLineItemService{
	
	@HttpPost
	global static String doPost() {
 		RestRequest req = RestContext.request;
			
		System.debug('post called '+req);
		System.debug('post requestBody '+req.requestBody);
			
		String body = req.requestBody.toString();
		IFCVGProjectOpportunityLineItemRequest CVGProjectOpportunityLineItemRequest = (IFCVGProjectOpportunityLineItemRequest)System.Json.deserialize(body, IFCVGProjectOpportunityLineItemRequest.class);
			
		System.debug('post requestBody '+CVGProjectOpportunityLineItemRequest);
		ICVGProjectOpportunityLineItemResponse resp = new ICVGProjectOpportunityLineItemResponse();	
		try{
				
			List<OpportunityLineItem> oppLineItems = CVGProjectOpportunityLineItemRequest.opportunityLineItems;
			
			resp.opportunityLineItems = bl_AccountPlanning.saveCVGProjectOpportunity(oppLineItems);
			resp.id = CVGProjectOpportunityLineItemRequest.id;
	
			resp.success = true;
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
		}
 
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'CVGProjectOpportunityLineItem' ,req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
  			
		return System.JSON.serialize(resp);		
	}

	@HttpDelete
	global static String doDelete(){
		RestRequest req = RestContext.request;
			
		System.debug('post called '+req);
		System.debug('post requestBody '+req.requestBody);
			
		String body = req.requestBody.toString();
		IFCVGProjectOpportunityLineItemDeleteResponse resp = new IFCVGProjectOpportunityLineItemDeleteResponse();
		
		IFCVGProjectOpportunityLineItemDeleteRequest CVGProjectOpportunityLineItemDeleteRequest = (IFCVGProjectOpportunityLineItemDeleteRequest)System.Json.deserialize(body, IFCVGProjectOpportunityLineItemDeleteRequest.class);
			
		System.debug('post requestBody '+CVGProjectOpportunityLineItemDeleteRequest);
			
		List<OpportunityLineItem> oppLineItems =CVGProjectOpportunityLineItemDeleteRequest.opportunityLineItems;
		
		List<String> mobileIdsToRemove = new List<String>();
		for (OpportunityLineItem ol : oppLineItems){
			mobileIdsToRemove.add(ol.mobile_id__c);
		}
		
		List<OpportunityLineItem>oppLineItemsToDelete = [select id, mobile_id__c from OpportunityLineItem where Mobile_ID__c in :mobileIdsToRemove];
		
		try{
			
			if (oppLineItemsToDelete !=null && oppLineItemsToDelete.size()>0 ){	
				
				delete oppLineItemsToDelete;
				resp.opportunityLineItems = oppLineItemsToDelete;
				resp.id = CVGProjectOpportunityLineItemDeleteRequest.id;
				resp.success = true;
			}else{
				resp.opportunityLineItems = oppLineItems;
				resp.id = CVGProjectOpportunityLineItemDeleteRequest.id;
				resp.message='Opportunity Line Items not found';
				resp.success = true;
			}
			
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
		}
  			
			
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'CVGProjectOpportunityLineItemDelete',req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
		
		return System.JSON.serialize(resp);
	}
	
	global class IFCVGProjectOpportunityLineItemRequest{
		public String id{get;set;}		
		public List<OpportunityLineItem> opportunityLineItems {get;set;}
	}
	
	global class IFCVGProjectOpportunityLineItemDeleteRequest{
		public String id{get;set;}
		public List<OpportunityLineItem> opportunityLineItems {get;set;}
	}
		
	global class IFCVGProjectOpportunityLineItemDeleteResponse{
		public String id{get;set;}
		public List<OpportunityLineItem> opportunityLineItems {get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}

	global class ICVGProjectOpportunityLineItemResponse{
		public String id {get;set;}
		public List<OpportunityLineItem> opportunityLineItems {get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}
	

}