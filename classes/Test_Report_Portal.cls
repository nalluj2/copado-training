@IsTest 
public with sharing class Test_Report_Portal {

    private static User guestUser {get;set;}
    private static ctrl_Report_Portal controller {get;set;}
    
    private static testmethod void loginSuccess(){
                
        guestUser = Test_Report_Portal.generateUser('unittest', false, false); // no admin user 
        
        System.runAs([Select Id from User where Alias = 'guest' AND IsActive = true][0]){
            
            Test.startTest();
            
            ws_adUserServiceMockImpl mockup = new ws_adUserServiceMockImpl();
            Test.setMock(WebServiceMock.class, mockup);
            
            Test.setCurrentPage(Page.Report_Portal);
            Test_Report_Portal.controller = new ctrl_Report_Portal(); 

            System.assert(controller.userLoggedIn==false);          
            
            controller.session.username = 'unittest';
            controller.session.password = 'testpassword';
            controller.doLogin();
            
            System.assert(controller.userLoggedIn==true);           
        }
        
        List<Application_Usage_Detail__c> usageDetails = [Select Id from Application_Usage_Detail__c where Process_Area__c = 'OSS' AND Capability__c = 'Login'];
        System.assert(usageDetails.size() == 1);
    }   

    private static testmethod void loginFailed(){
                
        guestUser = Test_Report_Portal.generateUser('unittest', false, false);  // no admin user
        
        System.runAs([Select Id from User where Alias = 'guest' AND IsActive = true][0]){
            
            Test.startTest();
            ws_adUserServiceMockImpl mockup = new ws_adUserServiceMockImpl();
            mockup.forceLoginFail = true;       
            Test.setMock(WebServiceMock.class, mockup);
            
            Test.setCurrentPage(Page.Report_Portal);
            Test_Report_Portal.controller = new ctrl_Report_Portal();
            
            System.assert(controller.userLoggedIn==false);          

            controller.session.username = 'unittest';
            controller.session.password = 'testpassword';
            controller.doLogin();
            
            System.assert(controller.userLoggedIn==false);          
        }     
        
        List<Application_Usage_Detail__c> usageDetails = [Select Id from Application_Usage_Detail__c where Process_Area__c = 'OSS' AND Capability__c = 'Login'];
        System.assert(usageDetails.size() == 0);  
    }   
	
	private static testmethod void loginSSOSuccess(){
                
        guestUser = Test_Report_Portal.generateUser('unittest', false, false); // no admin user 
        
        System.runAs([Select Id from User where Alias = 'guest' AND IsActive = true][0]){
            
            Test.startTest();
            
            PageReference pr = Page.Report_Portal;
            pr.getParameters().put('user', CryptoUtils.encryptText('unittest'));
            pr.getParameters().put('token', CryptoUtils.encryptText(String.valueOf(DateTime.now().getTime())));
            
            Test.setCurrentPage(pr);
            
            Test_Report_Portal.controller = new ctrl_Report_Portal(); 

            System.assert(controller.userLoggedIn == true);           
            System.assert(controller.session.internalUserId == guestUser.Id);
			System.assert(controller.session.username == 'unittest');
			
			Test_Report_Portal.controller.onLoadAction();
        }
        
        List<Application_Usage_Detail__c> usageDetails = [Select Id from Application_Usage_Detail__c where Process_Area__c = 'OSS' AND Capability__c = 'Login'];
        System.assert(usageDetails.size() == 1);       
    }   

    private static testmethod void loginSSOFailed(){
                
        guestUser = Test_Report_Portal.generateUser('unittest', false, false);  // no admin user
        
        System.runAs([Select Id from User where Alias = 'guest' AND IsActive = true][0]){
            
            Test.startTest();
            
            PageReference pr = Page.Report_Portal;
            pr.getParameters().put('user', CryptoUtils.encryptText('unittest'));
            pr.getParameters().put('token', CryptoUtils.encryptText(String.valueOf(DateTime.now().addMinutes(-1).getTime())));
            
            Test.setCurrentPage(pr);
            
            Test_Report_Portal.controller = new ctrl_Report_Portal(); 

            System.assert(controller.userLoggedIn == false);           
            System.assert(controller.errorMessage == 'Access denied. Access token has expired');		
            
            Test_Report_Portal.controller.onLoadAction();	         
        }      
        
        List<Application_Usage_Detail__c> usageDetails = [Select Id from Application_Usage_Detail__c where Process_Area__c = 'OSS' AND Capability__c = 'Login'];
        System.assert(usageDetails.size() == 0); 
    }   
	
    private static testmethod void loginAdminSuccess(){
                
        guestUser = Test_Report_Portal.generateUser('unittest', true, false);   // admin user
        
        System.runAs(guestUser){
            
            Test.startTest();
            ws_adUserServiceMockImpl mockup = new ws_adUserServiceMockImpl();
            Test.setMock(WebServiceMock.class, mockup);
            
            Test.setCurrentPage(Page.Report_Portal);
            Test_Report_Portal.controller = new ctrl_Report_Portal();
            
            System.assert(controller.userLoggedIn==true);
        	
        	Test_Report_Portal.controller.onLoadAction();    
        }
        
        List<Application_Usage_Detail__c> usageDetails = [Select Id from Application_Usage_Detail__c where Process_Area__c = 'OSS' AND Capability__c = 'Login'];
        System.assert(usageDetails.size() == 1);        
    }  

    private static testmethod void loginSFDCAdminSuccess(){
                
        guestUser = Test_Report_Portal.generateUser('unittest', true, true);   // admin user
        
        System.runAs(guestUser){
            
            Test.startTest();
            ws_adUserServiceMockImpl mockup = new ws_adUserServiceMockImpl();
            Test.setMock(WebServiceMock.class, mockup);
            
            Test.setCurrentPage(Page.Report_Portal);
            Test_Report_Portal.controller = new ctrl_Report_Portal();
            
            System.assert(controller.userLoggedIn==true);
            
            Test_Report_Portal.controller.onLoadAction();
        }    
        
        List<Application_Usage_Detail__c> usageDetails = [Select Id from Application_Usage_Detail__c where Process_Area__c = 'OSS' AND Capability__c = 'Login'];
        System.assert(usageDetails.size() == 1);    
    }        

    private static testmethod void loginAdminFailed(){
                
        guestUser = Test_Report_Portal.generateUser('unittest', false, false);  // admin user
        
        System.runAs(guestUser){
            
            Test.startTest();
            ws_adUserServiceMockImpl mockup = new ws_adUserServiceMockImpl();
            Test.setMock(WebServiceMock.class, mockup);
            
            Test.setCurrentPage(Page.Report_Portal);
            Test_Report_Portal.controller = new ctrl_Report_Portal();
            
            System.assert(controller.userLoggedIn==false);
            
        }       
    }
    
    private static testmethod void showSections() {
        
        Test_Report_Portal.loginSuccess();
        
        System.runAs(guestUser){       
	        
	        Test_Report_Portal.generateSectionData(4, 6);
	        
	        System.assert(Test_Report_Portal.controller.Sections.size() == 4);
	        System.assert(Test_Report_Portal.controller.Sections[0].linkLst.size() == 6);
            System.assert(Test_Report_Portal.controller.Sections[1].linkLst.size() == 6);
            System.assert(Test_Report_Portal.controller.Sections[2].linkLst.size() == 6);
            System.assert(Test_Report_Portal.controller.Sections[3].linkLst.size() == 6);
	        Test_Report_Portal.addPermissionRestriction(Test_Report_Portal.controller.Sections[0]);
	        Test_Report_Portal.addOverrideRestriction(Test_Report_Portal.controller.Sections[1]);
	        System.assertEquals(6, Test_Report_Portal.controller.Sections[0].linkLst.size());
	        System.assertEquals(5, Test_Report_Portal.controller.Sections[1].linkLst.size());
            System.assertEquals(6, Test_Report_Portal.controller.Sections[2].linkLst.size());
            System.assertEquals(6, Test_Report_Portal.controller.Sections[3].linkLst.size());
	        
	        System.assert(Test_Report_Portal.controller.CVGConfig == null);
	        
	        Test_Report_Portal.simulateButtonClicks();
        }
    } 

    private static testmethod void showSections_Admin() {
        
        Test_Report_Portal.loginSFDCAdminSuccess();
        
        System.runAs(guestUser){       
            
            Test_Report_Portal.generateSectionData(4, 6);
            System.assert(Test_Report_Portal.controller.Sections.size() == 4);
            System.assert(Test_Report_Portal.controller.Sections[0].linkLst.size() == 6);
            System.assert(Test_Report_Portal.controller.Sections[1].linkLst.size() == 6);
            System.assert(Test_Report_Portal.controller.Sections[2].linkLst.size() == 6);
            System.assert(Test_Report_Portal.controller.Sections[3].linkLst.size() == 6);
            Test_Report_Portal.addPermissionRestriction(Test_Report_Portal.controller.Sections[0]);
            Test_Report_Portal.addOverrideRestriction(Test_Report_Portal.controller.Sections[1]);
            System.assertEquals(6, Test_Report_Portal.controller.Sections[0].linkLst.size());
            System.assertEquals(6, Test_Report_Portal.controller.Sections[1].linkLst.size());
            System.assertEquals(6, Test_Report_Portal.controller.Sections[2].linkLst.size());
            System.assertEquals(6, Test_Report_Portal.controller.Sections[3].linkLst.size());
            
            System.assert(Test_Report_Portal.controller.CVGConfig == null);
            
            Test_Report_Portal.simulateButtonClicks();
        }
    }         
    
    private static void simulateButtonClicks() {
               
        Test_Report_Portal.controller.launchLinkId = Test_Report_Portal.controller.Sections[0].linkLst[0].content.Id;
        
        Test_Report_Portal.controller.processLinkButton();
        
        list<Application_Usage_Detail__c> audLst = [select id from Application_Usage_Detail__c where Internal_Record_Id__c = :Test_Report_Portal.controller.Sections[0].linkLst[0].content.Id];
        
        System.assert(audLst.size() == 1);     
    }
    
    public static void addOverrideRestriction(ctrl_Report_Portal.SectionWrapper s) {            
        // create include current guestuser override
        OSS_User_Override__c o = new OSS_User_Override__c(
                    OSS_Content__c = s.linkLst[0].content.Id,
                    User_Operator__c = 'Exclude',
                    User__c = Test_Report_Portal.guestuser.Id        
        );  
        
        insert o;           
    }   

    public static void addPermissionRestriction(ctrl_Report_Portal.SectionWrapper s) {          
        // create include current guestuser override
        OSS_Permission__c p = new OSS_Permission__c (
                    Active__c = true
                    , Region__c = 'Europe'       // only Europe for now, change when more options are added
                    , Company_Code__c = 'EUR'    // only Europe for now, change when more options are added
                    , OSS_Content__c = s.linkLst[0].Content.Id
                    , Operator__c = 'Exclude'
                    , Country_vs__c = Test_Report_Portal.guestuser.Country_vs__c
        );      
        insert p;           
    }
    
    @TestSetup
    private static void generateTestMasterData(){
    	
    	Company__c cmpny = new Company__c();		
        cmpny.name='Europe';
        cmpny.CurrencyIsoCode = 'EUR';        
        cmpny.Company_Code_Text__c = 'TST';
		insert cmpny;
		
		Business_Unit_Group__c bug =  new Business_Unit_Group__c();
		bug.Name = 'CVG';
		bug.Abbreviated_Name__c = 'CVG';
		bug.Master_Data__c = cmpny.Id;
		insert bug;
		
		Business_Unit__c bu =  new Business_Unit__c();
        bu.Company__c =cmpny.id;
        bu.Business_Unit_Group__c = bug.Id;
        bu.name='Test Business Unit';                
        insert bu;
        
        Sub_Business_Units__c sbu = new Sub_Business_Units__c();
        sbu.name='Test Sub-Business Unit';
        sbu.Business_Unit__c=bu.id;        
		insert sbu;    	
    }
    
    
    public static User generateUser(string loginName, boolean isOSSAdmin, boolean bIsSFDCAdmin) {
                
        Profile p;
        
        if (bIsSFDCAdmin){
            p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        }else{
            p = [SELECT Id FROM Profile WHERE Name='EUR Account Data Steward'];
        } 
        
        User usr = new User (
            Lastname = 'lastname',
            Alias = loginName,
            LanguageLocaleKey = 'en_US',
            Email = loginName + '@medtronic.com',
            Username = loginName + '@medtronic.com',
            CommunityNickname = 'Nickame',
            ProfileId = p.Id,
            TimeZoneSidKey = 'Europe/Brussels',
            LocaleSidKey = 'en_US',
            EmailEncodingKey =  'UTF-8',            
            Company_Code_Text__c = 'EUR',
            User_Business_Unit_vs__c = 'All',
            User_Sub_Bus__c = 'Aortic,Structural Hearth,EST,HST,Coro + PV,Renal Denervation',
            Job_Title_vs__c = 'Region Manager',
            CostCenter__c = '00000',
            Alias_unique__c = loginName,
            FederationIdentifier = loginName,
            User_Status__c = 'Current',
            Region_vs__c = 'Europe',
            Sub_Region__c = 'NWE',
            Country_vs__c = 'NETHERLANDS',
            OneStopShop_Admin__c = isOSSAdmin
        );            
                
        insert usr;

        User_Business_Unit__c userBU = new User_Business_Unit__c();
		userBU.User__c = usr.Id;
        userBU.Sub_Business_Unit__c = [Select Id from Sub_Business_Units__c].Id;                
        insert userBU;
        
        return usr;
    }
    
    public static void generateSectionData(integer nbrOfSections, integer nbrOfLinks) { 
        
        One_Stop_Shop__c oss = new One_Stop_Shop__c();
        oss.Name = 'CVG';
        oss.OwnerId = guestUser.Id;
        insert oss;
        
        List<OSS_Section__c> sectLst = new List<OSS_Section__c>();
        List<OSS_Content__c> contLst = new List<OSS_Content__c>();
        List<OSS_Permission__c> permLst = new List<OSS_Permission__c>();
        List<OSS_User_Override__c> ovrrLst = new List<OSS_User_Override__c>();
		
        // create sections
        for(integer i = 0; i < nbrOfSections; i++) {
            OSS_Section__c s = new OSS_Section__c (
                Active__c = true,
                Description__c = 'description' + i,
                Sequence__c = i,
                One_Stop_Shop__c = oss.Id,
                Section_Label__c = 'label' + i
            );
            sectLst.add(s);
        }
        
        insert sectLst;
        
        for(OSS_Section__c s : sectLst) {
            // create section content
            for(integer i = 0; i < nbrOfLinks; i++) {
                OSS_Content__c c = new OSS_Content__c (
                    Active__c = true,
                    Description__c = 'description' + i,
                    Link__c = i + 'www.medtronic.com',
                    Section_Name__c = s.Id,
                    Sequence__c = i,
                    Target__c = '_NEW',
                    Text__c = 'TEXT' + i,
                    Title__c = 'TITLE' + i
                );
                
                contLst.add(c);               
            }           
        }
        
        insert contLst;
        
        for(OSS_Content__c s : contLst) {

            // create general include all permission
            OSS_Permission__c p = new OSS_Permission__c (
                        Active__c = true,
                        Region__c = 'Europe',       // only Europe for now, change when more options are added
                        Company_Code__c = 'EUR',    // only Europe for now, change when more options are added
                        OSS_Content__c = s.Id,
                        Operator__c = 'Include'
            );
            
            permLst.add(p);         
            
            // create include current guestuser override
            OSS_User_Override__c o = new OSS_User_Override__c(
                        OSS_Content__c = s.Id,
                        User_Operator__c = 'Include',
                        User__c = Test_Report_Portal.guestuser.Id        
            );  
            
            ovrrLst.add(o); 

        }
        
        insert permLst;     
        insert ovrrLst;
    }   
}