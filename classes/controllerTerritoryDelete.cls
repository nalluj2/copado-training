public with sharing class controllerTerritoryDelete {
    private final Territory2 delTerr;
    public static string warnText; 
    public ID returnTerr;
    public Id parentRecord;
    public string propTerId {
    get { return propTerId; }
    set { propTerId = value; }
    }
    
    public controllerTerritoryDelete(ApexPages.StandardController controller ){
        this.delTerr =(Territory2)controller.getRecord();
        returnTerr = delTerr.id;
        
    
    }
    
    public PageReference TerritoryDelete(){
        Territory2 detail;
//        Id parentRecord;
        PageReference newPage;
        try{
        Territory2 currRecord = [select Id, Name, ParentTerritory2Id, Territory2Type.DeveloperName, Country_UID__c, Business_Unit__c, Therapy_Groups_Text__c  from Territory2 where id=:returnTerr];
        parentRecord = currRecord.ParentTerritory2Id; 
        List <Territory2> childRecords = [select Id, Name from Territory2 where ParentTerritory2Id=:currRecord.Id];
        Integer size = childRecords.size();
        if (size > 0){
            warntext = 'This Territory can\'t be deleted';
        }
        else{
            propTerId=currRecord.Id;
            warntext = '<br />You are about to delete the territory : ' + currRecord.Name + '. <br /> All links to employees and accounts will also be deleted. <br /> The employee and accounts will not be deleted.'  ;
        String newPageUrl = '/apex/confirmPage?id='+ parentRecord;        
        newPage = new PageReference(newPageUrl);
        newPage.setRedirect(true);      
        }
        } catch (Exception e) {
                ApexPages.addMessages(e);
        }
    return null;
    }
// Show the message on the VF page why the Territory can't be deleted.
    public string getWarntext(){
    return warntext;
  } 
  // Back method will go back to the to deleted Territory, because it is not possible to delete
    public PageReference Cancel(){
    system.debug('return id = ' + returnTerr);
    PageReference p = new PageReference('/apex/TerritoryManagement?id='+ returnTerr);    
    p.setRedirect(true);
    return p;
  
  }
  
    public PageReference DeleteTerritory(){
    system.debug('return id = ' + parentRecord);
    PageReference p = new PageReference('/apex/TerritoryManagement?id='+ parentRecord);    
    p.setRedirect(true);
    return p;
  
  }
  
}