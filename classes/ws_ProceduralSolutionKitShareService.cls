@RestResource(urlMapping='/ProceduralSolutionKitShareService/*')
global with sharing class ws_ProceduralSolutionKitShareService {
    
    @HttpPost
	global static String doPost() {
 		
 		RestRequest req = RestContext.request;
			
		System.debug('post called '+ req);
		System.debug('post requestBody '+ req.requestBody.toString());
			
		String body = req.requestBody.toString();
		ProceduralSolutionKitShareRequest ProceduralSolutionKitShareRequest = (ProceduralSolutionKitShareRequest) System.Json.deserialize(body, ProceduralSolutionKitShareRequest.class);
			
		System.debug('post requestBody ' + ProceduralSolutionKitShareRequest);
			
		Procedural_Solutions_Kit_Share__c ProceduralSolutionKitShare = ProceduralSolutionKitShareRequest.ProceduralSolutionKitShare;

		ProceduralSolutionKitShareResponse resp = new ProceduralSolutionKitShareResponse();

		//We catch all exception to assure that 
		try{
			
			List<Procedural_Solutions_Kit_Share__c> shares = [Select Id from Procedural_Solutions_Kit_Share__c where Procedural_Solutions_Kit__c = :ProceduralSolutionKitShare.Procedural_Solutions_Kit__c AND User__c = :ProceduralSolutionKitShare.User__c];
				
			if(shares.size() == 0) resp.ProceduralSolutionKitShare = bl_ProceduralSolutionKit.saveProceduralSolutionKitShare(ProceduralSolutionKitShare);
						
			resp.id = ProceduralSolutionKitShareRequest.id;
	
			resp.success = true;
			
		}catch (Exception e){
			
			resp.message = e.getMessage();
			resp.success = false;
			RestResponse res = RestContext.response;
			res.statusCode = 400;
		}
 
		bl_Mobilesync.storeRequest(UserInfo.getUserId(), 'ProceduralSolutionKitShare', req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
  			
		return System.JSON.serialize(resp);		
	}

	@HttpDelete
	global static String doDelete(){
		RestRequest req = RestContext.request;
			
		System.debug('post called ' + req);
		System.debug('post requestBody ' + req.requestBody.toString());
			
		String body = req.requestBody.toString();
		ProceduralSolutionKitShareDeleteRequest ProceduralSolutionKitShareDeleteRequest = (ProceduralSolutionKitShareDeleteRequest) System.Json.deserialize(body, ProceduralSolutionKitShareDeleteRequest.class);
			
		System.debug('post requestBody ' + ProceduralSolutionKitShareDeleteRequest);
			
		Procedural_Solutions_Kit_Share__c ProceduralSolutionKitShare = ProceduralSolutionKitShareDeleteRequest.ProceduralSolutionKitShare;
		
		ProceduralSolutionKitShareDeleteResponse resp = new ProceduralSolutionKitShareDeleteResponse();
		
		List<Procedural_Solutions_Kit_Share__c> ProceduralSolutionKitSharesToDelete = [select id, mobile_id__c from Procedural_Solutions_Kit_Share__c where Mobile_ID__c = :ProceduralSolutionKitShare.Mobile_ID__c];
		
		try{
			
			if (ProceduralSolutionKitSharesToDelete != null && ProceduralSolutionKitSharesToDelete.size() > 0){	
				
				Procedural_Solutions_Kit_Share__c ProceduralSolutionKitShareToDelete = ProceduralSolutionKitSharesToDelete.get(0);
				bl_ProceduralSolutionKit.deleteProceduralSolutionKitShare(ProceduralSolutionKitShareToDelete);
				resp.ProceduralSolutionKitShare = ProceduralSolutionKitShareToDelete;
				resp.id = ProceduralSolutionKitShareDeleteRequest.id;
				resp.success = true;
				
			}else{
				
				resp.ProceduralSolutionKitShare = ProceduralSolutionKitShare;
				resp.id = ProceduralSolutionKitShareDeleteRequest.id;
				resp.message='ProceduralSolutionKitShare not found';
				resp.success = true;
			}
			
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
			RestResponse res = RestContext.response;
			res.statusCode = 400;
		}
			
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'ProceduralSolutionKitShareDelete', req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
			
		return System.JSON.serialize(resp);
	}

	global class ProceduralSolutionKitShareRequest{
		public Procedural_Solutions_Kit_Share__c ProceduralSolutionKitShare {get;set;}
		public String id{get;set;}
	}
	
	global class ProceduralSolutionKitShareDeleteRequest{
		public Procedural_Solutions_Kit_Share__c ProceduralSolutionKitShare {get;set;}
		public String id{get;set;}
	}
		
	global class ProceduralSolutionKitShareDeleteResponse{
		public Procedural_Solutions_Kit_Share__c ProceduralSolutionKitShare {get;set;}
		public String id{get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}

	global class ProceduralSolutionKitShareResponse{
		public Procedural_Solutions_Kit_Share__c ProceduralSolutionKitShare {get;set;}
		public String id{get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}
}