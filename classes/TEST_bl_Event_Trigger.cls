//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 04-05-2016
//  Description      : APEX TEST Class for tr_Event and bl_Event_Trigger
//  Change Log       : PMT-22787: MITG EMEA User Setup - Changed profile EUR Field Force MITG to EMEA Field Force MITG
//--------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_bl_Event_Trigger {
	
	private static List<User> lstUser_SystemAdmin;
	private static User oUser;
	private static Id id_RecordType_Event_1;
	private static Id id_RecordType_Event_2;
	private static User oUser_SystemAdmin;

	@isTest static void createTestData() {

		String tRegion = '';
		String tPrimaryBU = '';
		String tPrimarySBU = '';

        oUser_SystemAdmin = clsTestData_User.createUser_SystemAdministrator('tadm000', true);

		System.runAs(oUser_SystemAdmin){

			// Create Test Data
			clsTestData.createCompanyData();
			clsTestData.createSubBusinessUnitData();
			clsTestData.createAccountData();
			clsTestData.createContactData();

			List<Sub_Business_Units__c> lstSBU = [SELECT Id, Name, Company_Text__c, Business_Unit__r.Name FROM Sub_Business_Units__c];
			List<RecordType> lstRecordType = 
				[
					SELECT 
						Id, Name, DeveloperName, IsActive, SobjectType 
					FROM 
						RecordType 
					WHERE 
						SobjectType = 'Event'
						AND IsActive = true
						AND Name like '%MITG%'
					LIMIT 2
				];
			id_RecordType_Event_1 = lstRecordType[0].Id;
			id_RecordType_Event_2 = lstRecordType[1].Id;

			List<Sub_Business_Units__c> lstSBU_Update = new List<Sub_Business_Units__c>();
			for (Sub_Business_Units__c oSBU : lstSBU){
				if (oSBU.Company_Text__c == 'Europe'){
					tRegion = oSBU.Company_Text__c;
					tPrimaryBU = oSBU.Business_Unit__r.Name;
					tPrimarySBU = oSBU.Name;
					lstSBU_Update.add(oSBU);
					break;
				}
			}
			update lstSBU_Update;

		}

		// Create Test User
		// Profile oProfile = [SELECT ID FROM Profile WHERE Name = 'EUR Field Force MITG' LIMIT 1];
        Profile oProfile = [SELECT ID FROM Profile WHERE Name = 'EMEA Field Force MITG' LIMIT 1];
		UserRole oUserRole = [SELECT Id FROM UserRole WHERE Name = 'Netherlands (MITG)' LIMIT 1];

		clsTestData.createUserData('MITG000G_TEST@Medtronic.com', 'MITG000', 'Belgium', oProfile.Id, oUserRole.Id, false);
		oUser = clsTestData.oMain_User;
			oUser.Primary_sBU__c = tPrimarySBU;
			oUser.Job_Title_vs__c = clsUtil.getUserJobTitle(tPrimaryBU)[0];
			oUser.Region_vs__c = tRegion;
		insert oUser;

	}

	@isTest static void test_populateMobileId(){
		
		List<Event> events;

		createTestData();

		Test.startTest();

		System.runAs(oUser_SystemAdmin){

			Event evnt1 = new Event();
				evnt1.Subject = 'Test Event 1';		
				evnt1.IsRecurrence = true;
				evnt1.RecurrenceStartDateTime = DateTime.now();
				evnt1.RecurrenceEndDateOnly = Date.today().addDays(30);
				evnt1.RecurrenceType = 'RecursDaily';
				evnt1.RecurrenceInterval = 1;
				evnt1.DurationInMinutes = 120;

			Event evnt2 = new Event();
				evnt2.Subject = 'Test Event 2';
				evnt2.Mobile_Id__c = 'UT Mobile ID';
				evnt2.StartDateTime = DateTime.now().addHours(1);
		        evnt2.EndDateTime = DateTime.now().addHours(2);
			insert new List<Event>{evnt1, evnt2};
			
			evnt2 = [Select Id, Mobile_Id__c from Event where Id = :evnt2.Id];
			
			System.assert(evnt2.Mobile_Id__c == 'UT Mobile ID');
			
			events = [Select Id, Mobile_Id__c from Event where RecurrenceActivityId = :evnt1.Id AND isRecurrence = false];
			
			Set<String> mobileIds = new Set<String>();
			
			for(Event evnt : events){
				
				mobileIds.add(evnt.Mobile_Id__c);				
			}
			
			System.assertEquals(31, mobileIds.size());
			
			bl_Event_Trigger.processedMobileIds = new Set<String>();
			
				events[1].Subject = 'Updated Subject of series instance';		
			update events[1];
			
		}

		Test.stopTest();

		Event seriesInstance = [Select Id, Mobile_Id__c, Subject from Event where Id = :events[1].Id];
				
		System.assert(SeriesInstance.Mobile_Id__c == events[1].Mobile_Id__c);
		System.assert(SeriesInstance.Subject == 'Updated Subject of series instance');


	}
	
}
//--------------------------------------------------------------------------------------------------------------------