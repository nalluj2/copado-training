public with sharing class ctrlExt_Account_Plan_Stakeholder_Mapping {
	

	private Account_Plan_Stakeholder__c oAccountPlanStakeholder;
	

	public ctrlExt_Account_Plan_Stakeholder_Mapping(ApexPages.StandardController oStdCtrl){
		
		oAccountPlanStakeholder = (Account_Plan_Stakeholder__c)oStdCtrl.getRecord();					
	
	}
	

	public PageReference redirectWithRecordType(){
				
		if (oAccountPlanStakeholder.Account_Plan__c == null) return null;
				
		Account_Plan_2__c oAccountPlan = [SELECT RecordTypeId, RecordType.DeveloperName, Business_Unit__c, Sub_Business_Unit__c, Business_Unit_Group__c, Account_Plan_Level__c FROM Account_Plan_2__c WHERE Id = :oAccountPlanStakeholder.Account_Plan__c];		
		
		Id id_RecordType;

		if (oAccountPlan.RecordType.DeveloperName == 'SAM_Account_Plan'){

			id_RecordType = clsUtil.getRecordTypeByDevName('Account_Plan_Stakeholder__c', 'General').Id;

		}else{

			String tSOQL = 'SELECT Account_Plan_Stakeholder_Record_Type__c FROM Account_Plan_Level__c WHERE Account_Plan_Record_Type__c = \''+ String.valueOf(oAccountPlan.RecordTypeId).left(15) + '\' AND Account_Plan_Level__c = \'' + oAccountPlan.Account_Plan_Level__c + '\' ';
		
			if (oAccountPlan.Account_Plan_Level__c == 'Business Unit Group'){
			
				tSOQL += ' AND Business_Unit__r.Business_Unit_Group__c = \'' + oAccountPlan.Business_Unit_Group__c + '\'';
			
			}else if (oAccountPlan.Account_Plan_Level__c == 'Business Unit'){
			
				tSOQL += ' AND Business_Unit__c = \'' + oAccountPlan.Business_Unit__c + '\'';			

			}
		
			tSOQL += ' LIMIT 1';
		
			List<Account_Plan_Level__c> lstAccountPlanLevel = Database.query(tSOQL);
				
			if (lstAccountPlanLevel.size() == 1){
			
				id_RecordType = lstAccountPlanLevel[0].Account_Plan_Stakeholder_Record_Type__c;

			}

			if (id_RecordType == null){
		
				id_RecordType = clsUtil.getRecordTypeByDevName('Account_Plan_Stakeholder__c', 'General').Id;
			
			}

		}
								
		String tKeyPrefix = Account_Plan_Stakeholder__c.sObjectType.getDescribe().getKeyPrefix();
						
		PageReference oPageReference = new PageReference('/' + tKeyPrefix + '/e');

			oPageReference.getParameters().putAll(ApexPages.currentpage().getparameters());
			oPageReference.getParameters().remove('sfdc.override');
			oPageReference.getParameters().remove('save_new');
			oPageReference.getParameters().put('RecordType', id_RecordType);
			oPageReference.getParameters().put('saveURL', '/' + oAccountPlan.Id );
			oPageReference.getParameters().put('nooverride', '1');
			oPageReference.setRedirect(true);
		
		return oPageReference;

	}


}