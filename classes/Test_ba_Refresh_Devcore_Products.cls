@isTest
private class Test_ba_Refresh_Devcore_Products {
    
    static testmethod void testBatch(){
    	
    	//Insert Company
		Company__c cmpny = new Company__c();
       	cmpny.name='TestMedtronic';
		cmpny.CurrencyIsoCode = 'EUR';
		cmpny.Current_day_in_Q1__c=56;
   		cmpny.Current_day_in_Q2__c=34; 
   		cmpny.Current_day_in_Q3__c=5; 
   		cmpny.Current_day_in_Q4__c= 0;   
   		cmpny.Current_day_in_year__c =200;
       	cmpny.Days_in_Q1__c=56;  
       	cmpny.Days_in_Q2__c=34;
       	cmpny.Days_in_Q3__c=13;
    	cmpny.Days_in_Q4__c=22;
       	cmpny.Days_in_year__c =250;
	    cmpny.Company_Code_Text__c ='TST';
       	insert cmpny;        

       	//Insert Business Unit
       	Business_Unit__c bu =  new Business_Unit__c();
       	bu.Company__c =cmpny.id;
       	bu.name='BUMedtronic';
       	insert bu;
        
	   	//Insert SBU
      	Sub_Business_Units__c sbu = new Sub_Business_Units__c();
      	sbu.name='SBUMedtronic1';
      	sbu.Business_Unit__c = bu.id;
      	sbu.Account_Plan_Default__c='Revenue';
      	sbu.Account_Flag__c = 'AF_Solutions__c';
      	sbu.Contact_Flag__c = 'Coro_PV__c';
      	insert sbu;
       
      	//Insert Therapy Group
      	Therapy_Group__c tg = new Therapy_Group__c();
      	tg.Name='Therapy Group';
      	tg.Company__c = cmpny.Id;
      	tg.Sub_Business_Unit__c = sbu.Id;
      	insert tg;
  
      	//Insert Therapy
      	Therapy__c tp = new Therapy__c();
      	tp.Name='Therapy';
      	tp.Therapy_Group__c = tg.id;
      	tp.Business_Unit__c=bu.id;
      	tp.Sub_Business_Unit__c=sbu.id;
      	tp.Therapy_Name_Hidden__c = 'test';      
      	insert tp;
      	
      	//Insert Product Group
        Product_Group__c pg = new Product_Group__c();
        pg.Name = 'Product Group';
        pg.Therapy_ID__c = tp.id;
        insert pg;
        
        Account acc = new Account() ;
        acc.Name = 'Test Account';  
        acc.RecordTypeId = [Select Id from RecordType where SObjectType = 'Account' AND DeveloperName = 'Competitor'].Id;
	    acc.Account_Country_vs__c='BELGIUM';
    	acc.CurrencyIsoCode = 'EUR';
    	insert acc;
    	
    	Product2 prod = new Product2();
    	prod.Name = 'Test Product';
    	prod.Business_Unit_ID__c = bu.Id;
    	prod.Product_Group__c = pg.Id;
    	prod.Manufacturer_Account_ID__c = acc.Id;
    	prod.RecordTypeId = [Select Id from RecordType where SObjectType = 'Product2' AND DeveloperName = 'SAP_Product'].Id;
    	insert prod;
    	
    	Map<Id, Id> dataMap = new Map<Id, Id>();   
    	dataMap.put(bu.Id, bu.Id);
		dataMap.put(pg.Id, pg.Id);
    	dataMap.put(acc.Id, acc.Id);
    	
    	Test.startTest();
    	
    	ba_Refresh_Devcore_Products batch = new ba_Refresh_Devcore_Products();
    	batch.dataMap = dataMap;
    	
    	Database.executeBatch(batch);
    	
    	Test.stopTest();
    }
}