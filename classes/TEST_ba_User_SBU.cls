@isTest
private class TEST_ba_User_SBU {
	
	@isTest static void test_Logic() {

		List<User> lstUser = [SELECT ID, Name, Company_Code_Text__c, User_Business_Unit_vs__c, User_Sub_Bus__c, Primary_sBU__c, ProfileId, License_Name__c FROM User WHERE License_Name__c = 'Salesforce' limit 10];

		clsTestData_MasterData.createSubBusinessUnit();

		List<User_Business_Unit__c> lstUserBusinessUnit = new List<User_Business_Unit__c>();

		for (User oUser : lstUser){
	
			clsTestData_MasterData.id_User = oUser.Id;
			clsTestData_MasterData.createUserBusinessUnit();
	
		}

		Test.startTest();

			String tIds = '';
			for (User oUser : lstUser){
				if (!String.isBlank(tIds)) tIds += ',';
				tIds += '\'' + oUser.Id + '\'';
			}
			String tSOQL_EXTRA = ' AND Id in (' + tIDs + ') LIMIT 10';

			ba_User_SBU oBatch = new ba_User_SBU();
				oBatch.tSOQL += tSOQL_EXTRA;
			Database.executebatch(oBatch, 200);

		Test.stopTest();	

	}
	
}