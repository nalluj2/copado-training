public with sharing class ctrl_Activity_Scheduling_Activity {
	
	@AuraEnabled
    public static CaseActivity getCase(Id caseId) {
    	
    	Case caseRecord = (new QueryService()).getCase(caseId);
		
		CaseActivity act = new CaseActivity();
		act.caseRecord = caseRecord;
		
		UserRecordAccess caseAccessCheck = [SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId = :UserInfo.getUserId() AND RecordId = :caseId];			
		act.canEdit = caseAccessCheck.HasEditAccess;
		
		return act;
    }    
    
    @AuraEnabled
    public static EventActivity getEvent(Id eventId) {
    	
    	Event eventRecord = (new QueryService()).getEvent(eventId);
    	
    	EventActivity act = new EventActivity();
    	act.eventRecord = eventRecord;
    	act.canEdit =  eventRecord.OwnerId == UserInfo.getUserId() ? true : false;
    	
    	if(eventRecord.IsAllDayEvent){
    		
    		act.canEdit = false;
    		eventRecord.StartDateTime = DateTime.newInstance(eventRecord.StartDateTime.date(), Time.newInstance(0, 0, 0, 0));
	     	eventRecord.EndDateTime = DateTime.newInstance(eventRecord.EndDateTime.date().addDays(1), Time.newInstance(0, 0, 0, 0));
    	}
    	
    	return act;
    }  
    
    @AuraEnabled
    public static String saveCase(Case caseRecord, List<Activity_Scheduling_Attendee__c> attendees){
    	
    	System.debug('Case Record: ' + caseRecord);
    	
    	System.SavePoint sp = Database.setSavepoint();
    	
	    try{
	    	
	    	if(caseRecord.AccountId == null && caseRecord.Type != 'Meeting') throw new IllegalArgumentException('Field Account is mandatory');
	    	if(caseRecord.Date_Received_Date__c == null && caseRecord.Type != 'Meeting') throw new IllegalArgumentException('Field Request Date is mandatory');	    	
	    	if(caseRecord.Type == 'Implant Support Request' && (caseRecord.Procedure__c == null || String.isEmpty(caseRecord.Procedure__c))) throw new IllegalArgumentException('Field Procedure is mandatory');
	    	if(caseRecord.Type == 'Service Request' && (caseRecord.Activity_Type_Picklist__c == null || String.isEmpty(caseRecord.Activity_Type_Picklist__c))) throw new IllegalArgumentException('Field Activity Type is mandatory');	    	
	    	if(caseRecord.Start_of_Procedure__c == null) throw new IllegalArgumentException('Field Start Date/Time is mandatory');
	    	if(caseRecord.End_of_Procedure_DateTime__c == null && caseRecord.Type == 'Meeting') throw new IllegalArgumentException('Field End Date/Time is mandatory');	    	
	    	if((caseRecord.Procedure_Duration_Implants__c == null || String.isEmpty(caseRecord.Procedure_Duration_Implants__c)) && caseRecord.Type != 'Meeting') throw new IllegalArgumentException('Field Procedure Duration is mandatory');
	    	if(caseRecord.Activity_Scheduling_Team__c == null || String.valueOf(caseRecord.Activity_Scheduling_Team__c) == '') throw new IllegalArgumentException('Field Team is mandatory');
	    	if(caseRecord.Type == 'Implant Support Request' && (caseRecord.Product_Group__c == null || String.valueOf(caseRecord.Product_Group__c) == '')) throw new IllegalArgumentException('Field Product Group is mandatory');
	    	
	    	if(caseRecord.Status == 'Rejected' && String.isEmpty(caseRecord.Reason_for_Rejection_Cancellation__c)) throw new IllegalArgumentException('Reason for Rejection is mandatory');
	    	if(caseRecord.Status == 'Cancelled' && String.isEmpty(caseRecord.Reason_for_Rejection_Cancellation__c)) throw new IllegalArgumentException('Reason for Cancellation is mandatory');
	    	
	    	if(caseRecord.Is_Recurring__c == true){
	    		
	    		if(caseRecord.Recurring_Week_Days__c == null || caseRecord.Recurring_Week_Days__c == '')  throw new IllegalArgumentException('At least one day must be selected');
    			if(caseRecord.Recurring_End_Date__c == null)  throw new IllegalArgumentException('Recurrence End Date is mandatory');
    			if(caseRecord.Recurring_End_Date__c <= caseRecord.Start_of_Procedure__c.date()) throw new IllegalArgumentException('Recurrence End Date must be after the Start Date');
    			
	    	}else{
	    		
	    		caseRecord.Recurring_Week_Days__c = null;
	    		caseRecord.Recurring_End_Date__c = null;
	    		caseRecord.Recurring_N_Weeks__c = null;	    		
	    	}
	    	
	    	bl_Case_Trigger.runningSchedulingApp = true;
	    	
	    	caseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Implant_Scheduling').getRecordTypeId();
	    	
	    	Database.UpsertResult  res = Database.upsert(caseRecord, false);
	    	
	    	if(res.isSuccess() == false){
	    		
	    		if(res.getErrors()[0].getFields() != null) System.debug(String.join(res.getErrors()[0].getFields(), ','));
	    		throw new IllegalArgumentException((res.getErrors()[0]).getMessage());
	    	}
	    		    	
	    	Set<String> attendeeKeys = new Set<String>();	    	
	    		    	
	    	for(Activity_Scheduling_Attendee__c attendee : attendees){
	    		
	    		if(attendee.Case__c == null) attendee.Case__c = caseRecord.Id;
	    		
	    		attendee.Unique_Key__c = attendee.Case__c + ':' + attendee.Attendee__c;
	    		attendee.Id = null;	   
	    		
	    		attendeeKeys.add(attendee.Unique_Key__c); 			
	    	}
	    	
	    	if(res.isCreated() == false) delete [Select Id from Activity_Scheduling_Attendee__c where Case__c = :caseRecord.Id AND Unique_Key__c NOT IN :attendeeKeys];
	    	
	    	upsert attendees Unique_Key__c;
	    		    		    	
	    	bl_Case_Trigger.runningSchedulingApp = false;
	    	
	    	return caseRecord.Id;
	    	
    	}catch(Exception e){
    		
    		Database.rollback(sp);
    		String errorMessage;
    		if(e.getMessage().contains('Exception:')) errorMessage = e.getMessage().split('Exception:')[1].split('Class.')[0];
    		else errorMessage = e.getMessage();
    		AuraHandledException auraEx = new AuraHandledException(errorMessage); 
    		if(test.isRunningTest()) auraEx.setMessage(e.getMessage());
    		throw auraEx;
    	}    	
    }    
    
    @AuraEnabled
    public static void saveEvent(Event eventRecord) {
    	
    	System.debug('Event Record: ' + eventRecord);
    	
    	try{
    		
    		if(eventRecord.Subject == null || String.isEmpty(eventRecord.Subject)) throw new IllegalArgumentException('Field Subject is mandatory');
    		if(eventRecord.StartDateTime == null) throw new IllegalArgumentException('Field Start is mandatory');
    		if(eventRecord.EndDateTime == null) throw new IllegalArgumentException('Field End is mandatory');
    		if(eventRecord.EndDateTime <= eventRecord.StartDateTime) throw new IllegalArgumentException('End cannot be equal of before Start');
    		
    		if(eventRecord.isRecurrence){
    			
    			if(eventRecord.RecurrenceDayOfWeekMask == null || eventRecord.RecurrenceDayOfWeekMask == 0)  throw new IllegalArgumentException('At least one day must be selected');
    			if(eventRecord.RecurrenceEndDateOnly == null)  throw new IllegalArgumentException('Recurrence End Date is mandatory');
    			if(eventRecord.RecurrenceStartDateTime == null)  throw new IllegalArgumentException('Recurrence Start Date is mandatory');
    			if(eventRecord.RecurrenceEndDateOnly <= eventRecord.RecurrenceStartDateTime.date()) throw new IllegalArgumentException('Recurrence End Date must be after the Start Date');
    			
    			eventRecord.RecurrenceInterval = Integer.valueOf(eventRecord.RecurrenceInterval);
    			eventRecord.RecurrenceDayOfWeekMask = Integer.valueOf(eventRecord.RecurrenceDayOfWeekMask);    			
    		}
    		
    		bl_Case_Trigger.runningSchedulingApp = true;
    	
    		if(eventRecord.Id == null) eventRecord.RecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get('Activity_Scheduling').getRecordTypeId();
    		Database.UpsertResult  res = Database.upsert(eventRecord, false);
    		
    		if(res.isSuccess() == false) throw new IllegalArgumentException((res.getErrors()[0]).getMessage());
    	
    		bl_Case_Trigger.runningSchedulingApp = false;
    		
    	}catch(Exception e){
    		
    		AuraHandledException auraEx = new AuraHandledException(e.getMessage()); 
    		auraEx.setMessage(e.getMessage());
    		throw auraEx;
    	}
    } 
    
    @AuraEnabled
    public static void removeCase(Case caseRecord) {
    	
    	try{
    		
    		bl_Case_Trigger.runningSchedulingApp = true;
    		
    		UserRecordAccess caseAccessCheck = [SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId = :UserInfo.getUserId() AND RecordId = :caseRecord.Id];			
			
			if(caseAccessCheck.HasEditAccess && UserInfo.getUserType() != 'PowerPartner'){
    		
    			Database.DeleteResult res = (new QueryService()).deleteCase(caseRecord);
    			if(res.isSuccess() == false) throw new IllegalArgumentException((res.getErrors()[0]).getMessage());
    			    		
			}else{
				
				throw new IllegalArgumentException('Insufficient access. You cannot delete this record.');
			}
    		
    		bl_Case_Trigger.runningSchedulingApp = false;
    	
    	}catch(Exception e){
    		
    		AuraHandledException auraEx = new AuraHandledException(e.getMessage()); 
    		auraEx.setMessage(e.getMessage());
    		throw auraEx;
    	}
    }    
    
    @AuraEnabled
    public static void removeEvent(Event eventRecord) {
    	
    	try{
    		
    		bl_Case_Trigger.runningSchedulingApp = true;
    		
    		Database.DeleteResult res = Database.delete(eventRecord, false);
    		if(res.isSuccess() == false) throw new IllegalArgumentException((res.getErrors()[0]).getMessage());
    		
    		bl_Case_Trigger.runningSchedulingApp = false;
    	
    	}catch(Exception e){
    		
    		AuraHandledException auraEx = new AuraHandledException(e.getMessage()); 
    		auraEx.setMessage(e.getMessage());
    		throw auraEx;
    	}
    }
            
    @AuraEnabled    
    public static List<Contact> getContactsForAccount(String accountId){
    	
    	return [Select Id, FirstName, LastName from Contact where Id IN (SELECT Affiliation_From_Contact__c FROM Affiliation__c where RecordTypeId =: FinalConstants.recordTypeIdC2A
                                        and Affiliation_Active__c = true and (Affiliation_Type__c <> 'Referring') and Affiliation_To_Account__r.Id=:accountId) ORDER BY LastName, FirstName];
    }
    
    @AuraEnabled    
    public static List<User> getTeamUsers(String teamId){
    	
    	List<User> result;
    	
    	if(UserInfo.getUserType() == 'PowerPartner') result = [Select Id, Name from User where Id = :UserInfo.getUserId()];
    	else result =  [Select Id, Name from User where Id IN (SELECT Member__c FROM Implant_Scheduling_Team_Member__c where Team__c = :teamId) ORDER BY Name];
    	
    	return result;
    }
    
    @AuraEnabled    
    public static List<Product_Group__c> getSBUProductGroups(String sbuId){
    	
    	return [Select Id, Name from Product_Group__c where Therapy_ID__r.Sub_Business_Unit__c = :sbuId ORDER BY Name];
    }
    
    public class CaseActivity{
    	
    	@AuraEnabled public Boolean canEdit { get; set; }
    	@AuraEnabled public Case caseRecord { get; set; }
    }
    
    public class EventActivity{
    	
    	@AuraEnabled public Boolean canEdit { get; set; }
    	@AuraEnabled public Event eventRecord { get; set; }
    }
    
    public without sharing class QueryService{
    	
    	public Case getCase(Id caseId){
    		
    		return [Select 	Id, 
	    					AccountId, 
	    					Account.Id,
	    					Account.Name,
	    					Account.BillingCity,
	    					Account.SAP_Id__c, 
	    					ContactId,
	    					Contact.Id,
	    					Contact.Name,
	    					Procedure__c,    					 
	    					Date_Received_Date__c, 
	    					Start_of_Procedure__c,
	    					End_of_Procedure_DateTime__c, 
	    					Subject, 
	    					Description, 
	    					Procedure_Duration_Implants__c, 
	    					Origin, 
	    					Priority,
	    					Activity_Scheduling_Team__c,
	    					Activity_Scheduling_Team__r.Name,
	    					Assigned_To__c,
	    					Assigned_To__r.Name,
	    					Assignee__c,
	    					Venue__c,
	    					Experience_of_the_Implanter__c,
	    					Type,
	    					Activity_Type_Picklist__c,
	    					Product_Group__c,
	    					Product_Group__r.Id,
	    					Product_Group__r.Name,
	    					Product_Group__r.Therapy_ID__r.Sub_Business_Unit__c,
	    					Product_Group__r.sBU__c,
	    					Status,
	    					Duration_of_the_Past_Procedure__c,
	    					Duration_of_Waiting_Time__c,
	    					Reason_for_Rejection_Cancellation__c,
	    					CreatedBy.Name,
	    					CreatedDate,
	    					LastModifiedBy.Name,
	    					LastModifiedDate,
	    					ParentId,
	    					Is_Recurring__c,
	    					Recurring_N_Weeks__c,
	    					Recurring_Week_Days__c,
	    					Recurring_End_Date__c,
	    					(Select Attendee__c, Case__c, Attendee__r.Name from Activity_Scheduling_Attendees__r)			    					
	    					from Case where Id = :caseId];
    		
    	}
    	
    	public Event getEvent(Id eventId){
    		
    		return [Select Id, OwnerId, Subject, IsRecurrence, RecurrenceInterval, RecurrenceEndDateOnly, RecurrenceActivityId, RecurrenceType, RecurrenceDayOfWeekMask, RecurrenceStartDateTime, StartDateTime, EndDateTime, IsAllDayEvent, (Select RelationId from EventRelations where IsInvitee = true) from Event where Id = :eventId];
    	}
    	
    	public Database.DeleteResult deleteCase(Case record){
    		
    		return Database.delete(record, false);
    	}
    	
    }
}