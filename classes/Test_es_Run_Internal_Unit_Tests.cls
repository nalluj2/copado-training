@isTest
private class Test_es_Run_Internal_Unit_Tests {
    
    private static testmethod void testRunAllTests(){
    	
    	Test.startTest();
    	
		Messaging.InboundEmail email = new Messaging.InboundEmail() ;
       	Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
       	
       	email.subject = 'Run All Internal Unit Tests';
       	
       	es_Run_Internal_Unit_Tests emailhandler = new es_Run_Internal_Unit_Tests();
       	Messaging.InboundEmailResult result = emailhandler.handleInboundEmail(email, env);    	
       	
       	System.assert(result.success == true);
       	
       	Test.setMock(HttpCalloutMock.class, new Test_DailyUnitTestRun_Mock());
		
		Test.stopTest();		
    }
    
    private static testmethod void testCheckTestResults(){
    	
    	Test.startTest();
    	
    	ApexTestRunResult lastRun = new ApexTestRunResult();				
		bl_Check_UnitTest_Run_Results.lastRun = new List<ApexTestRunResult>{lastRun};		
		
		ApexTestResult testResultPassed = new ApexTestResult();		
		testResultPassed.MethodName = 'TestMethod';
		testResultPassed.ApexClass = new ApexClass(Name = 'Test_UnitTest');		
		testResultPassed.Outcome = 'Pass';	
		
		ApexTestResult testResultFailed = new ApexTestResult();		
		testResultFailed.MethodName = 'TestMethod';
		testResultFailed.ApexClass = new ApexClass(Name = 'Test_UnitTest');	
		testResultFailed.Message = 'Method failed.';
		testResultFailed.StackTrace = 'In first line.';
		testResultFailed.Outcome = 'Fail';	
				
		bl_Check_UnitTest_Run_Results.testResults = new List<ApexTestResult>{testResultPassed, testResultFailed};
    	
		Messaging.InboundEmail email = new Messaging.InboundEmail() ;
       	Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
       	
       	email.subject = 'Check Unit Test Results';
       	
       	es_Run_Internal_Unit_Tests emailhandler = new es_Run_Internal_Unit_Tests();
       	Messaging.InboundEmailResult result = emailhandler.handleInboundEmail(email, env);
       	
       	System.assert(result.success == true);    
       	
       	Test.setMock(HttpCalloutMock.class, new Test_DailyUnitTestRun_Mock());
		
		Test.stopTest();    	
    }
    
    private static testmethod void testError(){
    	
		Messaging.InboundEmail email = new Messaging.InboundEmail() ;
       	Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
       	
       	email.subject = 'Unknown Action';
       	
       	es_Run_Internal_Unit_Tests emailhandler = new es_Run_Internal_Unit_Tests();
       	Messaging.InboundEmailResult result = emailhandler.handleInboundEmail(email, env);
       	
       	System.assert(result.success == false);      			
    }
}