/*
 *      Created Date    : 20140811
 *      Author          : Bart Caelen
 *      Description     : TEST Class for the APEX Trigger tr_Event_UpdateParent
 */
@isTest private class Test_tr_Event_UpdateParent {

    private static List<Account_Plan_2__c> lstIPlan2 = new List<Account_Plan_2__c>();
    private static List<Event> lstEvent_Insert   = new List<Event>();

    private static void createTestData(){

        //---------------------------------------------------
        // CREATE TEST DATA
        //---------------------------------------------------
        clsTestData.createSubBusinessUnitData();
        clsTestData.createAccountData();
        
        RecordType eurDibSbu = [Select Id from RecordType where sObjectType = 'Account_Plan_2__c' AND DeveloperName = 'EUR_DIB_SBU'];
        
        List<Account_Plan_2__c> lstAccountPlan = new List<Account_Plan_2__c>();
        Account_Plan_2__c oAccountPlan_1 = new Account_Plan_2__c();
            oAccountPlan_1.Account__c = clsTestData.oMain_Account.Id;
            oAccountPlan_1.Account_Plan_Level__c = 'Sub Business Unit';
            oAccountPlan_1.Sub_Business_Unit__c = [Select Id from Sub_Business_Units__c where Name = 'Brain Modulation' AND Business_Unit__r.Company__r.Name = 'Europe'].Id;
            oAccountPlan_1.Start_Date__c = Date.today();            
            oAccountPlan_1.RecordTypeId = eurDibSbu.Id;
        lstAccountPlan.add(oAccountPlan_1);

        Account_Plan_2__c oAccountPlan_2 = new Account_Plan_2__c();
            oAccountPlan_2.Account__c = clsTestData.oMain_Account.Id;
            oAccountPlan_2.Account_Plan_Level__c = 'Sub Business Unit';
            oAccountPlan_2.Sub_Business_Unit__c = [Select Id from Sub_Business_Units__c where Name = 'Coro + PV' AND Business_Unit__r.Company__r.Name = 'Europe'].Id;
            oAccountPlan_2.Start_Date__c = Date.today();            
            oAccountPlan_2.RecordTypeId = eurDibSbu.Id;
        lstAccountPlan.add(oAccountPlan_2);
        insert lstAccountPlan;


        lstIPlan2 = [SELECT Id FROM Account_Plan_2__c];
        
        clsTestData.iRecord_Event = 5;

        clsTestData.oMain_Event = null;
        clsTestData.idEvent_What = lstIPlan2[0].Id;
        clsTestData.dtEventStartDateTime = DateTime.now().addDays(-2);
        clsTestData.dtEventEndDateTime = DateTime.now().addDays(-1);
        clsTestData.createEventData(false);
        lstEvent_Insert.addAll(clsTestData.lstEvent);
        //---------------------------------------------------
    }

    @isTest static void test_tr_Event_UpdateParent_Insert() {


        //---------------------------------------------------
        // CREATE TEST DATA
        //---------------------------------------------------
        createTestData();
        //---------------------------------------------------


        //---------------------------------------------------
        // TEST 1 
        //---------------------------------------------------
        List<Account_Plan_2__c> lstAccountPlan = 
            [
                SELECT 
                    Id, Number_of_Activities__c
                FROM
                    Account_Plan_2__c
                WHERE
                    Id = :lstIPlan2
            ];

        for (Account_Plan_2__c oAccountPlan : lstAccountPlan){
            System.assertEquals(oAccountPlan.Number_of_Activities__c, 0);
        }
        //---------------------------------------------------


        Test.startTest();

        insert lstEvent_Insert;

        Test.stopTest();


        //---------------------------------------------------
        // TEST 2
        //---------------------------------------------------
        List<Event> lstEvent0 = [SELECT Id, WhatId FROM Event WHERE WhatId = :lstIPlan2[0].Id];
        List<Event> lstEvent1 = [SELECT Id, WhatId FROM Event WHERE WhatId = :lstIPlan2[1].Id];
        System.assertEquals(lstEvent0.size(), 5);
        System.assertEquals(lstEvent1.size(), 0);

        lstAccountPlan = 
            [
                SELECT 
                    Id, Number_of_Activities__c
                FROM
                    Account_Plan_2__c
                WHERE
                    Id = :lstIPlan2
            ];

        for (Account_Plan_2__c oAccountPlan : lstAccountPlan){
            if (oAccountPlan.Id == lstIPlan2[0].Id){
                System.assertEquals(oAccountPlan.Number_of_Activities__c, 5);
            }else if (oAccountPlan.Id == lstIPlan2[1].Id){
                System.assertEquals(oAccountPlan.Number_of_Activities__c, 0);
            }
        }
        //---------------------------------------------------
    }


    @isTest static void test_tr_Event_UpdateParent_Update() {

        List<Event> lstEvent_Update   = new List<Event>();

        //---------------------------------------------------
        // CREATE TEST DATA
        //---------------------------------------------------
        createTestData();
        insert lstEvent_Insert;
        //---------------------------------------------------

        List<Event> lstEvent = [SELECT Id, WhatId FROM Event WHERE WhatId = :lstIPlan2];
        for (Event oEvent : lstEvent){
            if (oEvent.WhatId == lstIPlan2[0].Id){
                oEvent.WhatId = lstIPlan2[1].Id;
                lstEvent_Update.add(oEvent);
            }
        }

        Test.startTest();

        update lstEvent_Update;

        Test.stopTest();

        //---------------------------------------------------
        // TEST
        //---------------------------------------------------
        List<Account_Plan_2__c> lstAccountPlan = 
            [
                SELECT 
                    Id, Number_of_Activities__c
                FROM
                    Account_Plan_2__c
                WHERE
                    Id = :lstIPlan2
            ];

        for (Account_Plan_2__c oAccountPlan : lstAccountPlan){
            if (oAccountPlan.Id == lstIPlan2[0].Id){
                System.assertEquals(oAccountPlan.Number_of_Activities__c, 0);
            }else if (oAccountPlan.Id == lstIPlan2[1].Id){
                System.assertEquals(oAccountPlan.Number_of_Activities__c, 5);
            }
        }
        //---------------------------------------------------
    }    

    @isTest static void test_tr_Event_UpdateParent_Delete() {

        List<Event> lstEvent_Delete = new List<Event>();

        //---------------------------------------------------
        // CREATE TEST DATA
        //---------------------------------------------------
        createTestData();
        insert lstEvent_Insert;
        //---------------------------------------------------

        List<Event> lstEvent = [SELECT Id, WhatId FROM Event WHERE WhatId = :lstIPlan2];
        for (Event oEvent : lstEvent){
            if (oEvent.WhatId == lstIPlan2[0].Id){
                lstEvent_Delete.add(oEvent);
            }
        }

        Test.startTest();

        delete lstEvent_Delete;

        Test.stopTest();

        //---------------------------------------------------
        // TEST
        //---------------------------------------------------
        List<Account_Plan_2__c> lstAccountPlan = 
            [
                SELECT 
                    Id, Number_of_Activities__c
                FROM
                    Account_Plan_2__c
                WHERE
                    Id = :lstIPlan2
            ];

        for (Account_Plan_2__c oAccountPlan : lstAccountPlan){
            if (oAccountPlan.Id == lstIPlan2[0].Id){
                System.assertEquals(oAccountPlan.Number_of_Activities__c, 0);
            }else if (oAccountPlan.Id == lstIPlan2[1].Id){
                System.assertEquals(oAccountPlan.Number_of_Activities__c, 0);
            }
        }
        //---------------------------------------------------
    }       

}