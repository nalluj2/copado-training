public with sharing class bl_Trigger_Deactivation {
	
	public static Boolean bIgnoreWhenRunningTest = true;

	public static Boolean isTriggerDeactivated(String triggerName){
		
		if(Test.isRunningTest() && bIgnoreWhenRunningTest) return false;
		
		Trigger_Deactivation__c settings = Trigger_Deactivation__c.getValues(triggerName);

		if(settings != null && settings.Deactivate__c == true){
			
			if(settings.User_Ids__c != null && settings.User_Ids__c != ''){
				
				for(Id userId : settings.User_Ids__c.split(',')){
															
					if(userId == UserInfo.getUserId()) return true;
				}
				
			}else return true;		
		}
		
		return false;
	}
}