@isTest
private class Test_SynchronizationService_Sparepart {
	
	private static testMethod void syncSparepart(){
		
		clsTestData_MasterData.tCompanyCode = 'EUR';
		clsTestData_MasterData.tBusinessUnitName = 'Cranial Spinal';
		clsTestData_MasterData.createBusinessUnit(true);

		Synchronization_Service_Setting__c syncSettings = new Synchronization_Service_Setting__c();
			syncSettings.username__c = 'test@medtronic.com';
			syncSettings.password__c = 'password';
			syncSettings.client_id__c = '123456789';
			syncSettings.client_secret__c = 'XXXXX';
			syncSettings.is_sandbox__c = true;
			syncSettings.sync_Enabled__c = true;
		insert syncSettings;
		
		//Products
		Product2 testProduct = new Product2();
			testProduct.RecordTypeId = clsUtil.getRecordTypeByDevName('Product2', 'SAP_Product').Id;
			testProduct.Name = 'PoleStar';
			testProduct.ProductCode = '000999';
			testProduct.isActive = true;
			testProduct.Business_Unit_ID__c = clsTestData_MasterData.oMain_BusinessUnit.Id;
		insert testProduct;
		
		//Software
		Software__c soft = new Software__c();
			soft.Name = 'Test Software';
			soft.Active__c = true;
			soft.External_Id__c = 'Test_Software_Id';
			soft.Software_Name__c = 'Test Software';
			soft.Version__c = '1.0';
			soft.Asset_Product_Type__c = 'PoleStar N-10';
			soft.Available_for_Cases_Part__c = true;
		insert soft;
		
		//Account
		Account acc = new Account();	
			acc.AccountNumber = '0000000';
			acc.Name = 'Syn Test Account';
			acc.Phone = '+123456789';
			acc.BillingCity = 'Minneapolis';
			acc.BillingCountry = 'UNITED STATES';
			acc.BillingState = 'Minnesota';
			acc.BillingStreet = 'street';
			acc.BillingPostalCode = '123456';
			acc.ST_NAV_Non_SAP_Account__c = true;
			acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non-SAP Account').getRecordTypeId();	
		insert acc;
		
		//Contact
		Contact cnt = new Contact();
			cnt.FirstName = 'Test';
			cnt.LastName = 'Contact';
			cnt.Phone = '+123456789';		
			cnt.Email = 'test.contact@gmail.com';
			cnt.MobilePhone = '+123456789';		
			cnt.MailingCity = 'Minneapolis';
			cnt.MailingCountry = 'UNITED STATES';
			cnt.MailingState = 'Minnesota';
			cnt.MailingStreet = 'street';
			cnt.MailingPostalCode = '123456';
			cnt.External_Id__c = 'Test_Contact_Id';
			cnt.Contact_Department__c = 'Cardiology';
			cnt.Contact_Primary_Specialty__c = 'Cardiovascular'; 
			cnt.Affiliation_To_Account__c = 'Employee'; 
			cnt.Primary_Job_Title_vs__c = 'Surgeon'; 
			cnt.Contact_Gender__c = 'Male'; 
			cnt.AccountId = acc.Id;
			cnt.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Generic Contact').getRecordTypeId();				
		insert cnt;
		
		//Asset
		Asset asset = new Asset();
			asset.AccountId = acc.Id;
			asset.Product2Id = testProduct.Id;
			asset.Asset_Product_Type__c = 'PoleStar N-10';
			asset.Ownership_Status__c = 'Purchased';
			asset.Name = '123456789';
			asset.Serial_Nr__c = '123456789';
			asset.Status = 'Installed';
			asset.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('PoleStar').getRecordTypeId();
		insert asset;
				
		//Complaint
		Complaint__c comp = new Complaint__c();
			comp.Account_Name__c = acc.Id;
			comp.Asset__c = asset.Id;
			comp.Status__c = 'Open';
			comp.Contact_Name__c = cnt.Id;
			comp.Surgeon1__c = cnt.Id;
			comp.External_Id__c = 'Test_Complaint_Id';
		insert comp;
		
		//Case
		Case newCase = new Case();
			newCase.AccountId = acc.Id;
			newCase.AssetId = asset.Id;
			newCase.RecordTypeId = clsUtil.getRecordTypeByDevName('Case', 'New_Case').Id;
			newCase.External_Id__c = 'Test_Case_Id';				
		insert newCase;
	
		//Workorder
		Workorder__c workOrder = new Workorder__c();
			workOrder.Account__c = acc.Id;
			workOrder.Asset__c = asset.Id;
			workOrder.Status__c = 'In Process';
			workOrder.External_Id__c = 'Test_Work_Order_Id';
			workOrder.RecordTypeId = Schema.SObjectType.Workorder__c.getRecordTypeInfosByName().get('PoleStar System Checkout').getRecordTypeId();
		insert workOrder;
		
		//Related Case Part
		Workorder_Sparepart__c relSparePart = new Workorder_Sparepart__c();
			relSparePart.Case__c = newCase.Id;
			relSparePart.Account__c = acc.Id;
			relSparePart.Asset__c = asset.Id;
			relSparePart.External_Id__c = 'Test_Rel_Case_Part_Id';		
			relSparePart.Amount_of_Inaccuracy__c = 1000;
			relSparePart.RecordTypeId = Schema.SObjectType.Workorder_Sparepart__c.getRecordTypeInfosByName().get('Inaccuracy').getRecordTypeId();
		insert relSparePart;
		
		Parts_Shipment__c partShipment = new Parts_Shipment__c();
			partShipment.External_Id__c = 'Test_Parts_Shipment_Id';
			partShipment.Ship_Via__c = 'Ground';		
			partShipment.Account__c = acc.Id;
			partShipment.Asset__c = asset.Id;
			partShipment.Case__c = newCase.Id;			
		insert partShipment;
						
		Test.startTest();
		
		Test_bl_SynchronizationService_Utils.CalloutMock mockImpl = new Test_bl_SynchronizationService_Utils.CalloutMock();
		
		Test.setMock(HttpCalloutMock.class, mockImpl);
		bl_SynchronizationService_Utils.calloutMock = mockImpl;
		
		//Case Part
		Workorder_Sparepart__c sparePart = new Workorder_Sparepart__c();
		sparePart.Case__c = newCase.Id;
			sparePart.Asset__c = asset.Id;
			sparePart.Account__c = acc.Id;
			sparePart.Owner__c = cnt.Id;
			sparePart.Complaint__c = comp.Id;
			sparePart.Parts_Shipment__c = partShipment.Id;
			sparePart.Software__c = soft.Id;
			sparePart.Related_Case_Part__c = relSparePart.Id;
			sparePart.Workorder__c = workOrder.Id;
			sparePart.Order_Part__c = testProduct.Id;
			sparePart.RI_Part__c = testProduct.Id;
			sparePart.Order_Status__c = 'Pending';		
			sparePart.External_Id__c = 'Test_Case_Part_Id';		
			sparePart.RecordTypeId = Schema.SObjectType.Workorder_Sparepart__c.getRecordTypeInfosByName().get('Hardware').getRecordTypeId();
		insert sparePart;
		
		List<Sync_Notification__c> notifications = [Select Id from Sync_Notification__c where Status__c = 'New' AND Type__c = 'Outbound' AND Record_Object_Type__c = 'Workorder_Sparepart__c'];
		
		System.assert(notifications.size() == 2);
		
		SynchronizationService_Sparepart sparepartService = new SynchronizationService_Sparepart();
		
		String payload = sparepartService.generatePayload( 'Test_Case_Part_Id');
		
		System.assert(payload != null);
	}
		
	private static testMethod void syncSparepartInbound(){

		clsTestData_MasterData.tCompanyCode = 'EUR';
		clsTestData_MasterData.tBusinessUnitName = 'Cranial Spinal';
		clsTestData_MasterData.createBusinessUnit(true);
		
		Synchronization_Service_Setting__c syncSettings = new Synchronization_Service_Setting__c();
			syncSettings.username__c = 'test@medtronic.com';
			syncSettings.password__c = 'password';
			syncSettings.client_id__c = '123456789';
			syncSettings.client_secret__c = 'XXXXX';
			syncSettings.is_sandbox__c = true;
			syncSettings.sync_Enabled__c = true;
		insert syncSettings;
		
		//Products
		Product2 testProduct = new Product2();
			testProduct.RecordTypeId = clsUtil.getRecordTypeByDevName('Product2', 'SAP_Product').Id;
			testProduct.Name = 'PoleStar';
			testProduct.ProductCode = '000999';
			testProduct.isActive = true;
			testProduct.Business_Unit_ID__c = clsTestData_MasterData.oMain_BusinessUnit.Id;
		insert testProduct;
		
		//Account
		Account acc = new Account();	
			acc.AccountNumber = '0000000';
			acc.Name = 'Syn Test Account';
			acc.Phone = '+123456789';
			acc.BillingCity = 'Minneapolis';
			acc.BillingCountry = 'UNITED STATES';
			acc.BillingState = 'Minnesota';
			acc.BillingStreet = 'street';
			acc.BillingPostalCode = '123456';
			acc.ST_NAV_Non_SAP_Account__c = true;
			acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non-SAP Account').getRecordTypeId();	
		insert acc;
		
		//Contact
		Contact cnt = new Contact();
			cnt.FirstName = 'Test';
			cnt.LastName = 'Contact';
			cnt.Phone = '+123456789';		
			cnt.Email = 'test.contact@gmail.com';
			cnt.MobilePhone = '+123456789';		
			cnt.MailingCity = 'Minneapolis';
			cnt.MailingCountry = 'UNITED STATES';
			cnt.MailingState = 'Minnesota';
			cnt.MailingStreet = 'street';
			cnt.MailingPostalCode = '123456';
			cnt.External_Id__c = 'Test_Contact_Id';
			cnt.Contact_Department__c = 'Cardiology';
			cnt.Contact_Primary_Specialty__c = 'Cardiovascular'; 
			cnt.Affiliation_To_Account__c = 'Employee'; 
			cnt.Primary_Job_Title_vs__c = 'Surgeon'; 
			cnt.Contact_Gender__c = 'Male'; 
			cnt.AccountId = acc.Id;
			cnt.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Generic Contact').getRecordTypeId();				
		insert cnt;
		
		//Asset
		Asset asset = new Asset();
			asset.AccountId = acc.Id;
			asset.Product2Id = testProduct.Id;
			asset.Asset_Product_Type__c = 'PoleStar N-10';
			asset.Ownership_Status__c = 'Purchased';
			asset.Name = '123456789';
			asset.Serial_Nr__c = '123456789';
			asset.Status = 'Installed';
			asset.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('PoleStar').getRecordTypeId();
		insert asset;
		
		//Case
		Case newCase = new Case();
			newCase.AccountId = acc.Id;
			newCase.AssetId = asset.Id;
			newCase.RecordTypeId = clsUtil.getRecordTypeByDevName('Case', 'New_Case').Id;
			newCase.External_Id__c = 'Test_Case_Id';				
		insert newCase;
		
		//Related Case Part
		Workorder_Sparepart__c relSparePart = new Workorder_Sparepart__c();
			relSparePart.Case__c = newCase.Id;
			relSparePart.Account__c = acc.Id;
			relSparePart.Asset__c = asset.Id;
			relSparePart.External_Id__c = 'Test_Case_Part_Id';	
			relSparePart.RecordTypeId = Schema.SObjectType.Workorder_Sparepart__c.getRecordTypeInfosByName().get('Hardware').getRecordTypeId();
		insert relSparePart;
				
		Test.startTest();
		
		Test_bl_SynchronizationService_Utils.CalloutMock mockImpl = new Test_bl_SynchronizationService_Utils.CalloutMock();
		
		Test.setMock(HttpCalloutMock.class, mockImpl);
		bl_SynchronizationService_Utils.calloutMock = mockImpl;
		
		SynchronizationService_Sparepart.SparepartSyncPayload request = new SynchronizationService_Sparepart.SparepartSyncPayload();
			
		//Sparepart
		Workorder_Sparepart__c sparepart = new Workorder_Sparepart__c();
			sparepart.External_Id__c = 'Test_Case_Part_Id';
			sparepart.Order_Status__c = 'Pending';
		request.sparepart = sparepart;
						
		//Other		
		request.ReceivedBy = 'Test_Contact_Id';
		request.TestedBy = 'Test_Contact_Id';
		request.lastModifiedDate = DateTime.now();
		request.lastModifiedBy = 'Test User';
		
		String payload = JSON.serialize(request);
		
		bl_SynchronizationService_Target.processRecordPayload(payload, 'Order_Return_Item__c');
		
		List<Workorder_Sparepart__c> spareparts = [Select Id, Tested_By_Text__c from Workorder_Sparepart__c where External_Id__c = 'Test_Case_Part_Id'];
						
		System.assert(spareparts.size() == 1);
		System.assert(spareparts[0].Tested_By_Text__c == 'Test_Contact_Id');					
	} 	
}