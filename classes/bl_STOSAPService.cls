// Adhoc sync calls to SAP
public with sharing class bl_STOSAPService 
{
    // Query
    public static SVMXC__Parts_Request__c getPartsRequest(String requestId)
    {
        return [SELECT Id,
                        CreatedById,
                        CreatedBy.Name,
                        Delivery_Type__c,
                        Name,                        
                        Part_Request_Comments__c,
                        Delivery_Instructions__c,
                        SM_Catalyst_Work_Order__c,
                        Ship_To_Customer__c,
                        Ship_To_Customer__r.SVMX_SAP_Location_Id__c, // Was SAP Ship To Id
                        Name1__c,
                        Name2__c,
                        Street__c,
                        City__c,
                        Postal_Code__c,
                        StateRegion__c,
                        Country__c,
                        SAP_STO_Number__c,
                        Receiving_Address_Type__c,
                        (SELECT Id,                                
                                SVMXC__Product__r.CFN_Code_Text__c,
                                Requested_Qty__c,
                                Unit_of_Measure__c,
                                Supplying_Storage_Location__c,
                                Message_to_Receiver__c,
                                Name
                        FROM SVMXC__Parts_Request_Line__r)
                FROM SVMXC__Parts_Request__c
                WHERE Id = :requestId];
    }

    // Callout & processing response STOCreate
    public static SVMXC__Parts_Request__c createCalloutSTOCreate(SVMXC__Parts_Request__c data)
    {       
        WebServiceSetting__c oWebServiceSetting = bl_NotificationSAP.loadWebServiceSetting('PartsRequestSTOCreate_NotificationSAP');  
        
        String tRequestBody = getCreateSTORequestBody(data, oWebServiceSetting);
        
        String response = callWebmethods(tRequestBody, oWebServiceSetting);
        
        System.debug('/** RESPONSE BODY : ' + response);
                                
        SVMXC__Parts_Request__c request = handleCreateSTOResponse(response, data);
        update request;
        update request.SVMXC__Parts_Request_Line__r;

        return request;
    }
    
    // Status
    public static SVMXC__Parts_Request__c createCalloutSTOStatus(SVMXC__Parts_Request__c data)
    {
        WebServiceSetting__c oWebServiceSetting = bl_NotificationSAP.loadWebServiceSetting('PartsRequestSTOStatus_NotificationSAP');  
        
        String tRequestBody = getStatusSTORequestBody(data, oWebServiceSetting);
        
        String response = callWebmethods(tRequestBody, oWebServiceSetting);
        
        System.debug('/** RESPONSE BODY : ' + response);
                
        SVMXC__Parts_Request__c request = handleStatusSTOResponse(response,data);
        update request;
        update request.SVMXC__Parts_Request_Line__r;

        return request;
    }
    
    //Perform callout
    private static String callWebmethods(String tRequestBody, WebServiceSetting__c oWebServiceSetting){

        String tAuthorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(Blob.valueOf(oWebServiceSetting.Username__c + ':' + oWebServiceSetting.Password__c));
            
        // Authentication
        Http oHTTP = new Http();

        // Prepare the request
        HttpRequest oHTTPRequest= new HttpRequest();
        oHTTPRequest.setMethod('POST');
        oHTTPRequest.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        oHTTPRequest.setEndpoint(oWebServiceSetting.EndPointURL__c);
        oHTTPRequest.setTimeout(oWebServiceSetting.TimeOut__c.intValue());
        oHTTPRequest.setHeader('Authorization', tAuthorizationHeader);
        oHTTPRequest.setBody(tRequestBody);
        
        System.debug('/** REQUEST BODY : ' + tRequestBody);
        // Send the request
        HttpResponse oHTTPResponse = oHTTP.send(oHTTPRequest);

        // Get the response of the Callout and process it
        return oHTTPResponse.getBody();        
    }
        
    
    // Generate STOCreateRequest
    public static String getCreateSTORequestBody(SVMXC__Parts_Request__c request, WebServiceSetting__c oWebServiceSetting) 
    {
        List<SVMXC__Service_Group_Members__c> technician = [SELECT SVMXC__Inventory_Location__r.SVMX_SAP_Location_ID__c, SVMXC__Inventory_Location__r.SAP_Plant__c FROM SVMXC__Service_Group_Members__c WHERE SVMXC__Salesforce_User__c = :request.CreatedById];

        if (technician.isEmpty()) throw new bl_STOSAPServiceException('No technician found for parts order createdby user.');
        if (technician.size() > 1) throw new bl_STOSAPServiceException('Multiple technicians found for parts order createdby user.');

        String requestBody = '';

        requestBody += '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"  xmlns:' + oWebServiceSetting.XMLNS__c + '>';
            requestBody += '<soapenv:Header/>';
            requestBody += '<soapenv:Body>';
                requestBody += '<' + oWebServiceSetting.WebServiceName__c + '>';
                    requestBody += '<' + oWebServiceSetting.DataName__c + '>';

						String receivingAddressType = ws_SharedMethods.getSAPMapping('SVMXC__Parts_Request__c', 'Receiving_Address_Type__c', request.Receiving_Address_Type__c);

                        requestBody += '<addressOptionCode>' + (receivingAddressType == 'T' ? 'S' : receivingAddressType) + '</addressOptionCode>';
                        requestBody += '<supplyingPlantCode></supplyingPlantCode>';

                        requestBody += '<purchaseOrderType>' + '</purchaseOrderType>';
                        requestBody += '<purchaseOrgCode>' + '</purchaseOrgCode>';
                        requestBody += '<purchaseGroupCode>' + '</purchaseGroupCode>';
                        requestBody += '<purchaseCompanyCode>' + '</purchaseCompanyCode>';
                        requestBody += '<sourceSystemPurchasingRef>' + clsUtil.isNull(request.Name, '') + '</sourceSystemPurchasingRef>';
                        requestBody += '<sourceSystemRef>SMAX_INT_W</sourceSystemRef>';
                        requestBody += '<purchasingPersonReference>' + clsUtil.isNull(request.CreatedBy.Name, '') + '</purchasingPersonReference>';
                        requestBody += '<purchasingRequestComments>' + clsUtil.isNull(request.Part_Request_Comments__c, '').escapeXml() + '</purchasingRequestComments>';
                        requestBody += '<technicalSourceSystemPurchasingRef>' + clsUtil.isNull(request.Id, '') + '</technicalSourceSystemPurchasingRef>';
                        requestBody += '<technicalSourceSystemRef>SMAX_INT_W</technicalSourceSystemRef>';
                        requestBody += '<deliveryPriorityCode>' + '</deliveryPriorityCode>';
                        requestBody += '<deliveryInstructions>' + clsUtil.isNull(request.Delivery_Instructions__c, '').escapeXml() + '</deliveryInstructions>';
                        requestBody += '<shippingConditionCode>' + '</shippingConditionCode>';
                        requestBody += '<shippingRouteCode>' + '</shippingRouteCode>';
                        
                        if(receivingAddressType == 'T'){
                        	requestBody += '<customerNumber>S' + technician[0].SVMXC__Inventory_Location__r.SAP_Plant__c + technician[0].SVMXC__Inventory_Location__r.SVMX_SAP_Location_ID__c + '</customerNumber>';
                        }
                        else{
                        	requestBody += '<customerNumber>' + request.Ship_To_Customer__r.SVMX_SAP_Location_Id__c + '</customerNumber>';
                        }

                        requestBody += '<customDeliveryAddress>';
                            requestBody += '<addressName1>' + clsUtil.isNull(request.Name1__c, '').escapeXml() + '</addressName1>';
                            requestBody += '<addressName2>' + clsUtil.isNull(request.Name2__c, '').escapeXml() + '</addressName2>';
                            requestBody += '<addressStreet>' + clsUtil.isNull(request.Street__c, '') + '</addressStreet>';
                            requestBody += '<addressCity>' + clsUtil.isNull(request.City__c, '').escapeXml() + '</addressCity>';
                            requestBody += '<addressPostalCode>' + clsUtil.isNull(request.Postal_Code__c, '').escapeXml() + '</addressPostalCode>';
                            
                            if (request.Receiving_Address_Type__c == 'I will provide Address')
                            {
                                String countryCode = clsUtil.getCountryISOCodeByName(request.Country__c);
                                String regionCode = [Select Region_Code__c from SAP_Region_Code__c where Name = :request.StateRegion__c].Region_Code__c;
                                
                                requestBody += '<addressRegionCode>' + regionCode + '</addressRegionCode>';
                                requestBody += '<addressCountryCode>' + countryCode + '</addressCountryCode>';
                            } else {
                                requestBody += '<addressRegionCode></addressRegionCode>';
                                requestBody += '<addressCountryCode></addressCountryCode>';
                            }
                        requestBody += '</customDeliveryAddress>';       

                        requestBody += '<deliveryType>' + clsUtil.isNull(request.Delivery_Type__c, '') + '</deliveryType>';
            
                        for (SVMXC__Parts_Request_Line__c lineItem: request.SVMXC__Parts_Request_Line__r)
                        {
                            requestBody += '<purchaseDocItem>';
                                requestBody += '<materialNumber>' + clsUtil.isNull(lineItem.SVMXC__Product__r.CFN_Code_Text__c, '') + '</materialNumber>';
                                requestBody += '<materialQuantity>' + clsUtil.isNull(lineItem.Requested_Qty__c, '') + '</materialQuantity>';
                                requestBody += '<materialUOM>' + clsUtil.isNull(lineItem.Unit_of_Measure__c, '') + '</materialUOM>';
                                requestBody += '<targetPlantCode>' + technician[0].SVMXC__Inventory_Location__r.SAP_Plant__c + '</targetPlantCode>';
                                requestBody += '<targetPlantStorageLocationCode>' + technician[0].SVMXC__Inventory_Location__r.SVMX_SAP_Location_ID__c + '</targetPlantStorageLocationCode>';
                                requestBody += '<supplyingPlantStorageLocationCode>' + clsUtil.isNull(lineItem.Supplying_Storage_Location__c, '') + '</supplyingPlantStorageLocationCode>';
                                requestBody += '<deliveryItemInstructions>' + clsUtil.isNull(lineItem.Message_to_Receiver__c, '').escapeXml() + '</deliveryItemInstructions>';
                                requestBody += '<sourceSystemPurchasingItemRef>' + lineItem.Id + '</sourceSystemPurchasingItemRef>';
                                requestBody += '<technicalSourceSystemPurchasingItemRef>' + clsUtil.isNull(lineItem.Name.removeStart('PRQ'), '') + '</technicalSourceSystemPurchasingItemRef>';
                            requestBody += '</purchaseDocItem>';
                        }
                    requestBody += '</' + oWebServiceSetting.DataName__c + '>';
                requestBody += '</' + oWebServiceSetting.WebServiceName__c + '>';
            requestBody += '</soapenv:Body>';
        requestBody += '</soapenv:Envelope>';

        return requestBody;
    }

    // Parse XML response
    public static SVMXC__Parts_Request__c handleCreateSTOResponse(String responseBody, SVMXC__Parts_Request__c request)
    {
        Dom.Document oDomDoc = new Dom.Document();
        oDomDoc.load(responseBody);
        Dom.XMLNode oDomXMLRoot = oDomDoc.getRootElement();

        for (Dom.XMLNode oDomXMLnode_Body : oDomXMLRoot.getChildren()) 
        {
            for (Dom.XMLNode oDomXMLnode_Response : oDomXMLnode_Body.getChildren()) 
            {

                Dom.XMLNode stoXML = oDomXMLnode_Response.getChildElement('createSTOResponse', null);

                Dom.XMLNode responseMessage = stoXML.getChildElement('responseMessage', null);

                String messageTypeCode = responseMessage.getChildElement('messageTypeCode', null).getText();

                //STO PROCESSING ERRORf
                if (messageTypeCode == 'E')
                {
                    //go through and get all messages
                    String messageText       = '';
                    for (Dom.XMLNode field : stoXML.getChildren()) 
                    {
                        if (field.getName() == 'responseMessage') 
                        {
                            messageText += field.getChildElement('messageText', null).getText() + ' | ';
                        }
                    }
    
                    //set the error messages into the parts request and update status
                    request.SVMXC__Additional_Information__c = messageText;
                    request.SVMXC__Status__c = 'Error in Processing';

                    request.SAP_Last_Sync_Date_Time__c     = System.now();
                    request.SAP_Last_Sync_Error_Message__c = messageText;
                    request.SAP_Last_Sync_Message_Type__c  = 'Create';
                    request.SAP_Last_Sync_Status__c        = 'Failure';
    
                    for (SVMXC__Parts_Request_Line__c lineItem: request.SVMXC__Parts_Request_Line__r)
                        lineItem.SVMXC__Line_Status__c = 'Error in Processing';

                //STO PROCESSED SUCCESSFULLY
                } else if (messageTypeCode == 'S')
                {
                    request.SAP_STO_Number__c = stoXML.getChildElement('purchasingDocNumber', null).getText();
                    request.SVMXC__Status__c  = 'Open';
                    request.SVMXC__Additional_Information__c = null;
                    
                    request.SAP_Last_Sync_Date_Time__c     = System.now();
                    request.SAP_Last_Sync_Error_Message__c = '';
                    request.SAP_Last_Sync_Message_Type__c  = 'Create';
                    request.SAP_Last_Sync_Status__c        = 'Success';
                    
                    for (Dom.XMLNode field : stoXML.getChildren()) 
                    {
                        if (field.getName() == 'purchaseDocItem') 
                        {
                            String itemNumber = field.getChildElement('purchasingItemNumber', null).getText();
                            String lineItemId = field.getChildElement('technicalSourceSystemPurchasingItemRef', null).getText();

                            for (SVMXC__Parts_Request_Line__c lineItem: request.SVMXC__Parts_Request_Line__r)
                            {
                                if (lineItem.Id == lineItemId) {
                                    lineItem.STO_Item_Number__c    = itemNumber;
                                    lineItem.SVMXC__Line_Status__c = 'Open';
                                }
                            }
                        }

                    }
        
                }

            }
        }
        return request;
    }
    
    public static String getStatusSTORequestBody(SVMXC__Parts_Request__c request, WebServiceSetting__c oWebServiceSetting) 
    {
        String requestBody = '';

        requestBody += '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"  xmlns:' + oWebServiceSetting.XMLNS__c + '>';
            requestBody += '<soapenv:Header/>';
            requestBody += '<soapenv:Body>';
                requestBody += '<' + oWebServiceSetting.WebServiceName__c + '>';
                    requestBody += '<' + oWebServiceSetting.DataName__c + '>';
                        requestBody += '<purchasingDocNumber>' + request.SAP_STO_Number__c + '</purchasingDocNumber>';
                        requestBody += '<technicalSourceSystemPurchasingRef>' + request.Id + '</technicalSourceSystemPurchasingRef>';
                        requestBody += '<sourceSystemPurchasingRef>' + request.Name + '</sourceSystemPurchasingRef>';
                        requestBody += '<sourceSystemRef>SMAX_INT_W</sourceSystemRef>';
                    requestBody += '</' + oWebServiceSetting.DataName__c + '>';
                requestBody += '</' + oWebServiceSetting.WebServiceName__c + '>';
            requestBody += '</soapenv:Body>';
        requestBody += '</soapenv:Envelope>';

        return requestBody;
    }

    public static SVMXC__Parts_Request__c handleStatusSTOResponse(String responseBody, SVMXC__Parts_Request__c request)
    {
        List<Parts_Request_Delivery_Schedule__c> deliverySchedules = new List<Parts_Request_Delivery_Schedule__c>();
        List<Parts_Request_Item_Delivery__c> itemDeliveries = new List<Parts_Request_Item_Delivery__c>();
        List<Parts_Request_Goods_Issue__c> goodsIssues = new List<Parts_Request_Goods_Issue__c>();

        Dom.Document oDomDoc = new Dom.Document();
        oDomDoc.load(responseBody);
        Dom.XMLNode oDomXMLRoot = oDomDoc.getRootElement();

        for (Dom.XMLNode oDomXMLnode_Body : oDomXMLRoot.getChildren()) 
        {
            for (Dom.XMLNode oDomXMLnode_Response : oDomXMLnode_Body.getChildren()) 
            {

                Dom.XMLNode stoXML = oDomXMLnode_Response.getChildElement('STODeliveryStatusResponse', null);

                Dom.XMLNode responseMessage = stoXML.getChildElement('responseMessage', null);

                String messageTypeCode = responseMessage.getChildElement('messageTypeCode', null).getText();

                //STO STATUS PROCESSING ERROR
                if (messageTypeCode == 'E')
                {
                    //go through and get all messages
                    String messageText       = '';
                    for (Dom.XMLNode field : stoXML.getChildren()) 
                    {
                        if (field.getName() == 'responseMessage') 
                        {
                            messageText += field.getChildElement('messageText', null).getText() + ' | ';
                        }
                    }
    
                    request.SAP_Last_Sync_Date_Time__c     = System.now();
                    request.SAP_Last_Sync_Error_Message__c = messageText;
                    request.SAP_Last_Sync_Message_Type__c  = 'Status';
                    request.SAP_Last_Sync_Status__c        = 'Failure';
    
                //STO STATUS PROCESSED SUCCESSFULLY
                } else if (messageTypeCode == 'S')
                {

                    //get all the header details
                    request.SAP_STO_Type__c     = stoXML.getChildElement('purchaseOrderType', null).getText();
                    request.Supplying_Plant__c  = ws_SharedMethods.getSAPMapping('SVMXC__Parts_Request__c', 'Supplying_Plant__c', stoXML.getChildElement('supplyingPlantCode', null).getText());
                    request.STO_Created_Date__c = ut_String.stringToDate(stoXML.getChildElement('purchasingDocDate', null).getText());
                    
                    request.SAP_Last_Sync_Date_Time__c     = System.now();
                    request.SAP_Last_Sync_Error_Message__c = '';
                    request.SAP_Last_Sync_Message_Type__c  = 'Status';
                    request.SAP_Last_Sync_Status__c        = 'Success';

                    //get all the line items
                    Dom.XMLNode itemDetails = stoXML.getChildElement('itemDetails', null);

                    for (Dom.XMLNode item : itemDetails.getChildren()) 
                    {

                        //get the line items internal Id
                        String lineItemId = item.getChildElement('technicalSourceSystemPurchasingItemRef', null).getText();

                        //get the line item
                        SVMXC__Parts_Request_Line__c lineItem = null;
                        for (SVMXC__Parts_Request_Line__c line: request.SVMXC__Parts_Request_Line__r)
                        {
                            if (lineItemId != '' && line.Id == lineItemId)
                                lineItem = line;
                        }

                        if (lineItem == null) 
                            throw new bl_STOSAPServiceException('Line Item with id "' + lineItemId + '" cannot be found in request with Id - "' + request.Id + '"');
                        
                        for (Dom.XMLNode itemField : item.getChildren()) 
                        {
                            if (itemField.getName() == 'respMaterialQuantity') lineItem.Requested_Qty__c = Decimal.valueOf(itemField.getText());
                            else if (itemField.getName() == 'respMaterialUOM') lineItem.Unit_of_Measure__c = itemField.getText();
                            else if (itemField.getName() == 'respTargetPlantCode') lineItem.Receiving_Plant__c = itemField.getText();
                            else if (itemField.getName() == 'respTargetPlantLocationCode') lineItem.Receiving_Storage_Location__c = itemField.getText();
                            else if (itemField.getName() == 'respSupplyingPlantStorageLocationCode') lineItem.Supplying_Storage_Location__c = itemField.getText();
                            else if (itemField.getName() == 'deletionIndicator') lineItem.Deletion_Indicator__c = itemField.getText();
                            else if (itemField.getName() == 'respShippingRouteCode') lineItem.Actual_Route__c = itemField.getText();
                            else if (itemField.getName() == 'purchasingDocItemNumber') lineItem.STO_Item_Number__c = itemField.getText();
                            else if (itemField.getName() == 'deliveredQuantity') lineItem.Delivered_Qty__c = Decimal.valueOf(itemField.getText());
                            else if (itemField.getName() == 'remainingQuantity') lineItem.Remaining_Qty__c = Decimal.valueOf(itemField.getText());
                            else if (itemField.getName() == 'respDeliveryPriorityCode') lineItem.Actual_Delivery_Priority__c = itemField.getText();
                            else if (itemField.getName() == 'respShippingConditionCode') lineItem.Actual_Shipping_Condition__c = itemField.getText();
                            else if (itemField.getName() == 'expectedDeliveryDate') lineItem.Expected_Delivery_Date__c = ut_String.stringToDate(itemField.getText());
                            
                                       
                            else if (itemField.getName() == 'deliveryAddress')
                            {
                                for (Dom.XMLNode addressField : itemField.getChildren()) 
                                {
                                    if (addressField.getName() == 'respAddressName1') lineItem.Actual_Delivery_To__c = addressField.getText();
                                    else if (addressField.getName() == 'respAddressName2') lineItem.Actual_Deliver_to_Attn__c = addressField.getText();
                                    else if (addressField.getName() == 'respAddressStreet') lineItem.Actual_Delivery_Street__c = addressField.getText();
                                    else if (addressField.getName() == 'respAddressCity') lineItem.Actual_Delivery_City__c = addressField.getText();
                                    else if (addressField.getName() == 'respAddressPostalCode') lineItem.Actual_Delivery_Postal_Code__c = addressField.getText();
                                    else if (addressField.getName() == 'respAddressRegionCode') lineItem.Actual_Delivery_Region__c = addressField.getText();
                                    else if (addressField.getName() == 'respAddressCountryCode') lineItem.Actual_Delivery_Country__c = addressField.getText();
                                }
                            }

                            else if (itemField.getName() == 'scheduleItem')
                            {
                                for (Dom.XMLNode scheduleItem : itemField.getChildren()) 
                                {

                                    //get the scheduled items line number
                                    String scheduleItemNumber = scheduleItem.getChildElement('scheduleLineNumber', null).getText();

                                    Parts_Request_Delivery_Schedule__c itemSchedule = new Parts_Request_Delivery_Schedule__c(Primary_Key__c = scheduleItemNumber + ' - ' + lineItem.Id);
                                    itemSchedule.Parts_Request_Line__c = lineItem.Id;
                                    itemSchedule.Line_Number__c = scheduleItemNumber;

                                    for (Dom.XMLNode scheduleItemField : scheduleItem.getChildren()) 
                                    {
                                        if (scheduleItemField.getName() == 'scheduleDeliveryDate') itemSchedule.Delivery_Date__c = ut_String.stringToDate(scheduleItemField.getText());
                                        else if (scheduleItemField.getName() == 'scheduleDeliveryTime') itemSchedule.Delivery_Time__c = scheduleItemField.getText();
                                        else if (scheduleItemField.getName() == 'scheduledDeliveryQuantity') itemSchedule.Scheduled_Quantity__c = Decimal.valueOf(scheduleItemField.getText());
                                        else if (scheduleItemField.getName() == 'scheduledDeliveryCommitedDate') itemSchedule.Committed_Date__c = ut_String.stringToDate(scheduleItemField.getText());
                                        else if (scheduleItemField.getName() == 'scheduledDeliveryCommitedQuantity') itemSchedule.Committed_Quantity__c = Decimal.valueOf(scheduleItemField.getText());
                                        else if (scheduleItemField.getName() == 'quantityDelivered') itemSchedule.Delivered_Qty__c = Decimal.valueOf(scheduleItemField.getText());
                                        else if (scheduleItemField.getName() == 'quantityIssued') itemSchedule.Issued_Qty__c = Decimal.valueOf(scheduleItemField.getText());
                                        else if (scheduleItemField.getName() == 'goodsIssueDate') itemSchedule.GI_Date__c = ut_String.stringToDate(scheduleItemField.getText());
                                        else if (scheduleItemField.getName() == 'goodsIssueTime') itemSchedule.GI_Time__c = scheduleItemField.getText();
                                        else if (scheduleItemField.getName() == 'goodsReceiptDate') itemSchedule.GR_Date__c = ut_String.stringToDate(scheduleItemField.getText());
                                        else if (scheduleItemField.getName() == 'goodsReceiptTime') itemSchedule.GR_Time__c = scheduleItemField.getText();
                                        else if (scheduleItemField.getName() == 'openQuantity') itemSchedule.Open_Qty__c = Decimal.valueOf(scheduleItemField.getText());
                                    }

                                    deliverySchedules.add(itemSchedule);

                                }
            
                            }
                            else if (itemField.getName() == 'deliveryItem')
                            {
                                for (Dom.XMLNode deliveryItem : itemField.getChildren()) 
                                {

                                    //get the delivery items line number
                                    String deliveryNumber = deliveryItem.getChildElement('deliveryNumber', null).getText();
                                    String deliveryItemNumber = deliveryItem.getChildElement('deliveryItemNumber', null).getText();

                                    Parts_Request_Item_Delivery__c itemDelivery = new Parts_Request_Item_Delivery__c(Primary_Key__c = deliveryNumber + ':' + deliveryItemNumber);
                                    itemDelivery.Parts_Request_Line__c = lineItem.Id;

                                    for (Dom.XMLNode deliveryItemField : deliveryItem.getChildren()) 
                                    {
                                        if (deliveryItemField.getName() == 'deliveryNumber') itemDelivery.Delivery_Number__c = deliveryItemField.getText();
                                        else if (deliveryItemField.getName() == 'deliveryItemNumber') itemDelivery.Delivery_Item_Number__c = deliveryItemField.getText();
                                        else if (deliveryItemField.getName() == 'deliveryStatus') itemDelivery.Delivery_Status__c = ws_SharedMethods.getSAPMapping('Parts_Request_Item_Delivery__c', 'Delivery_Status__c', deliveryItemField.getText());
                                        else if (deliveryItemField.getName() == 'deliveryQuantity') itemDelivery.Delivery_Quantity__c = Decimal.valueOf(deliveryItemField.getText());
                                        else if (deliveryItemField.getName() == 'deliveryTrackingNumber') itemDelivery.Tracking_Number__c = deliveryItemField.getText();
                                        else if (deliveryItemField.getName() == 'deliveryCarrier') itemDelivery.Carrier__c = deliveryItemField.getText();
                                        else if (deliveryItemField.getName() == 'deliveryETA') itemDelivery.Estimated_Delivery__c = deliveryItemField.getText();
                                        else if (deliveryItemField.getName() == 'deliveryTrackingURL') itemDelivery.Tracking_URL__c = deliveryItemField.getText();
                                    }

                                    if (itemDelivery.Delivery_Quantity__c != 0)
                                        itemDeliveries.add(itemDelivery);

                                }
            
                            }
                            else if (itemField.getName() == 'goodsIssueItem')
                            {
                                for (Dom.XMLNode goodsItem : itemField.getChildren()) 
                                {

                                    //get the delivery items line number
                                    String deliveryNumber = goodsItem.getChildElement('deliveryNumber', null).getText();
                                    String goodsIssueItemNumber = goodsItem.getChildElement('goodsIssueItemNumber', null).getText();
                                    String goodsIssueMaterial = goodsItem.getChildElement('goodsIssueMaterial', null).getText();

                                    Parts_Request_Goods_Issue__c goodsIssue = new Parts_Request_Goods_Issue__c(Primary_Key__c = deliveryNumber + ':' + goodsIssueItemNumber + ':' + goodsIssueMaterial);
                                    goodsIssue.Parts_Request_Line__c = lineItem.Id;

                                    for (Dom.XMLNode goodsItemField : goodsItem.getChildren()) 
                                    {
                                        if (goodsItemField.getName() == 'deliveryNumber') goodsIssue.Delivery_Number__c = goodsItemField.getText();
                                        else if (goodsItemField.getName() == 'goodsIssueItemNumber') goodsIssue.Accounting_Doc_Line__c = goodsItemField.getText();
                                        else if (goodsItemField.getName() == 'goodsIssueMaterial') goodsIssue.Material_Doc_Number__c =goodsItemField.getText();
                                        else if (goodsItemField.getName() == 'goodsIssueUoM') goodsIssue.GI_UoM__c = goodsItemField.getText();
                                        else if (goodsItemField.getName() == 'goodsIssueBatch') goodsIssue.Batch_Serial__c = goodsItemField.getText();
                                        else if (goodsItemField.getName() == 'goodsIssueQuantity') goodsIssue.Batch_Serial_Qty__c = Decimal.valueOf(goodsItemField.getText());
                                        else if (goodsItemField.getName() == 'goodsIssueDate') goodsIssue.Posting_Date__c = ut_String.stringToDate(goodsItemField.getText());
                                    }

                                    goodsIssues.add(goodsIssue);

                                }
            
                            }

                        }

                    }
        
                }

            }
        }

        upsert deliverySchedules Primary_Key__c;
        upsert itemDeliveries Primary_Key__c;
        upsert goodsIssues Primary_Key__c;
        update request;
        update request.SVMXC__Parts_Request_Line__r;
        return request;
    }
    
    public class bl_STOSAPServiceException extends Exception { }
}