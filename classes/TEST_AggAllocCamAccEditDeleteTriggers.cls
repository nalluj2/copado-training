/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TEST_AggAllocCamAccEditDeleteTriggers {

    static testMethod void myUnitTest() {
    	Account account = new Account();
    	account.Name = 'Test Account Name';
    	insert account;
    	
    	Department_Master__c department = new Department_Master__c();
    	department.Name = 'Adult'; 
    	insert department;
    	
    	DIB_Department__c dibDepartment = new DIB_Department__c();
    	dibDepartment.account__c = account.Id;
    	dibDepartment.Department_ID__c = department.Id;
    	dibDepartment.Active__c = true;
    	insert dibDepartment; 
    	
		DIB_Campaign_Program__c campaignProgram = new DIB_Campaign_Program__c();
		campaignProgram.name = 'Campain Pro';
		insert campaignProgram;
			
		DIB_Global_Campaign__c globalCampaign = new DIB_Global_Campaign__c();
		globalCampaign.Name = 'Global Campain';
		globalCampaign.DIB_Campaign_Program__c = campaignProgram.id;
		insert globalCampaign;
			
		DIB_Campaign__c	myCampaign = new DIB_Campaign__c();
		myCampaign.Name = 'Aatest_Campaign1';
		myCampaign.Start_Fiscal_Month__c = '02';
		myCampaign.Start_Fiscal_Year__c = '2009';
		myCampaign.End_Fiscal_Month__c = '03';
		myCampaign.End_Fiscal_Year__c = '2050';
		myCampaign.Global_Campaign__c = globalCampaign.Id;
		myCampaign.isGlobal__c = true;
		insert myCampaign;
    	
    	//User [] user = [Select u.Id From User u where u.ProfileId != :FinalConstants.SystemAdminProfileId and u.IsActive = true limit 10];
    	
    	Id p2 = [select id from profile where name like '%DiB Sales Entry%' limit 1].Id; 
        User u2 = new User(alias = 'DsUser', email='dsusertest@absi.be', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
           		localesidkey='en_US', profileid = p2, timezonesidkey='America/Los_Angeles', username='dsusertest@absi.be', Alias_unique__c='usertest1234',Company_Code_Text__c='EUR');
    	
    	
	    DIB_Aggregated_Allocation__c aggregatedAllocation = new DIB_Aggregated_Allocation__c();
	    aggregatedAllocation.DIB_Department_ID__c = dibDepartment.Id;
	    aggregatedAllocation.Fiscal_Year__c = '2010';
	    aggregatedAllocation.Fiscal_Month__c = '11';
	    insert aggregatedAllocation;
	    
	    update aggregatedAllocation;
	    
    	DIB_Campaign_Account__c campaignAccount = new DIB_Campaign_Account__c();
    	campaignAccount.Account__c = account.Id;
    	campaignAccount.Campaign__c = myCampaign.Id;
    	campaignAccount.Department_ID__c = dibDepartment.Id;
    	insert campaignAccount;	
	    
    	update campaignAccount;
	    
    	Boolean error = false;
 	    System.runAs(u2) {
 	    	try{
    			update aggregatedAllocation;
 	    	}catch(Exception e){
 	    		error = true;
 	    	}
 	    	System.assert(error);
 	    	error = false;
 	    	try{
    			delete aggregatedAllocation;
 	    	}catch(Exception e){
 	    		error = true;
 	    	}
 	    	System.assert(error); 	    	

    		error = false;
    		 	    
 	    	try{
    			update campaignAccount;
 	    	}catch(Exception e){
 	    		error = true;
 	    	}
 	    	System.assert(error);
 	    	error = false;
 	    	try{
    			delete campaignAccount;
 	    	}catch(Exception e){
 	    		error = true;
 	    	}
 	    	System.assert(error); 
 	    		    	
 	    }
        
    }
}