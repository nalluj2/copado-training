/*
  *  Class Name    : CasePartTriggers
  *  Description   : This class include all the logic for Case Part. The class
                     methods are being called by Case Part trigger.
  *  Created Date  : 30/7/2013
  *  Author        : Niha Saha  
*/

public class CasePartTriggers{
       
    //Method for Update_Tracked_Parts(CP) on Case Part object 
    public static Boolean updateFlag = false; 
  
    public static void Update_Tracked_Parts(List<Workorder_SparePart__c> objCPList){
        String sdate;
        Date mydate;
        String sdate1;
        Date mydate1;
        
        Set<Id> RecTypeset = new Set<Id>();
        Set<Id> caseIdSet = new Set<Id>();
        Set<Id> ComplaintIdSet = new Set<Id>();
        Set<Id> SystemId=new Set<Id>();
        Set<Id> ProductIdSet=new Set<Id>();
        //Map<id,Workorder_SparePart__c> maofCPforEachId=new Map<id,Workorder_SparePart__c>();
        //Map<id,id> MapofsystemofCase = new Map<id,id>();
        Map<id,id> MapofsystemofComplaint = new Map<id,id>();
        Map<id,Case> mapSystemCase = new Map<id,Case>();
        Map<id,complaint__c> Mapsystemcomplaint=new Map<id,complaint__c>();
        Map<id,Product2> MapoftrackingofProduct=new Map<id,Product2>();
        
        List<Workorder_SparePart__c> casePartList = new List<Workorder_SparePart__c>();
        
        
        for(Workorder_SparePart__c ObjCP:objCPList)
        {
              //maofCPforEachId.put(ObjCP.id,ObjCP);
             //In CasePart if OrderPart is not Null
             if(objCP.Order_Part__c!=Null) {
                ProductIdSet.add(ObjCP.Order_Part__c);
             } 
             //In CasePart if ReturnPart is not Null         
             if(objCP.RI_Part__c!=Null)  {              
                  ProductIdSet.add(ObjCP.RI_Part__c);
             } 
             //In CasePart if Case is not Null
             if(objCP.case__c != null)
                caseIdSet.add(objCP.case__c);
             //In CasePart if Case is Null but complaint is not Null
             if(objCP.case__c == Null && objCP.complaint__c != null)
                ComplaintIdSet.add(objCP.complaint__c);
         }
        //get system from Complaint
        List<Complaint__c> ComplaintList=[Select id, Asset__c FROM Complaint__c Where id IN:ComplaintIdSet];
        //get system from Case
        List<Case> CaseList=[Select id, assetId FROM Case Where id IN:caseIdSet];
        //Get Maintenance_Frequency__c AnD Tracking__c FROM Product
        List<Product2> proList=[Select id,Maintenance_Frequency__c,Tracking__c from Product2 Where id IN:ProductIdSet];
        // Check CasePartis case is equal to case No.  
        for(Workorder_SparePart__c ObjCP1:objCPList){
            for(Case objCase:CaseList){
                if(ObjCP1.case__c == objCase.id)
                    mapSystemCase.put(ObjCP1.id,objCase);
            }
            //Check CasePart's Case is Null But Complaint is not Null
            for(Complaint__c objcom:ComplaintList){
                if(ObjCP1.case__c == Null && ObjCP1.Complaint__c == objcom.id)
                    Mapsystemcomplaint.put(ObjCP1.id,objcom);  
            }
            //Product Tracking Is TRUE ANd Case Part's OrderPart And returnPart related to Product 
            for(product2 objproduct:proList){
                 if(objproduct.Tracking__c == TRUE && ( ObjCP1.Order_Part__c == objproduct.id || ObjCP1.RI_Part__c == objproduct.id) )
                    MapoftrackingofProduct.put(ObjCP1.id,objproduct);
            }
        }
        //Check recordtype of CAse Part
        List<RecordType> RecordTypeList = [Select id,DeveloperName from Recordtype where (DeveloperName='PartOnly' OR DeveloperName='Software') AND sobjectType='Workorder_Sparepart__c']; 
        for(recordtype r:RecordTypeList)
        {
                RecTypeset.add(r.id);
        }
        
        Workorder_SparePart__c tempCP; 
        Boolean flag = false;
        for(Workorder_SparePart__c CP:objCPList)
        {   
           // Recordtype Nit equal to above Condition ANd Work_Order is equa to Null
           if(!RecTypeset.Contains(CP.recordtypeid) && CP.Workorder__c == Null)
           {  
                tempCP = new Workorder_SparePart__c(Id=CP.id);
                if(MapoftrackingofProduct.ContainsKey(CP.id))
                {   
                    // date Replaced equal to Created date               
                    DateTime dT1 = CP.CreatedDate;
                    System.debug('dT1>>>>>>>' + dT1);
                    if(null != dT1) {
                        myDate = date.newinstance(dT1.year(), dT1.month(), dT1.day());
                        System.debug('myDate>>>>>>>>>>>' + myDate);
                        sdate = String.valueOf(myDate);
                       //CP.Date_Replaced__c=Date.valueof(sdate);
                        tempCP.Date_Replaced__c=Date.valueof(sdate);
                        flag = true;
                    }
                    //Syatem From casePart to System FROm case
                  if(mapSystemCase.size()> 0 && mapSystemCase.get(CP.id) != null) {
                           
                     //CP.System__c = mapSystemCase.get(CP.id).System__c;
                    tempCP.asset__c = mapSystemCase.get(CP.id).assetId;
                  } else {
                  //Syatem From casePart to System FROm Complaint
                  if(Mapsystemcomplaint.size()> 0 && mapSystemCase.get(CP.id) != null ){                      
                      //CP.System__c = Mapsystemcomplaint.get(CP.id).System__c;
                      tempCP.asset__c = mapSystemCase.get(CP.id).assetId;
                      }
                  }
                      
                   System.debug(' CP.RI_Part__r.Maintenance_Frequency__c >>>>>>>>>>' + CP.Order_Part__r.Maintenance_Frequency__c);
                      
                      
                        // CP.Next_Replacement_Due__c=Date.valueof(sdate1)+ Integer.ValueOf(MapoftrackingofProduct.get(CP.id).Maintenance_Frequency__c);x`
                }
                        System.debug('CP.Order_Part__r.Maintenance_Frequency__c >>>>>>>>>>' + CP.Order_Part__r.Maintenance_Frequency__c);
               //If Maintenance Frequency Of Product is not equal to Null
               if(MapoftrackingofProduct.size() > 0 && MapoftrackingofProduct.get(CP.id) != null 
                    &&  MapoftrackingofProduct.get(CP.id).Maintenance_Frequency__c != null ) 
                {
                   DateTime dT = CP.CreatedDate;
                   //Set NeXT Replacement Date equal to Created date with Maintenance Frequency Date
                   if(null != dT) {
                       myDate1 = date.newinstance(dT.year(), dT.month(), dT.day());
                       sdate1 = String.valueOf(myDate);
                       //CP.Next_Replacement_Due__c=Date.valueof(sdate1).addMonths(Integer.ValueOf(MapoftrackingofProduct.get(CP.id).Maintenance_Frequency__c));
                       tempCP.Next_Replacement_Due__c=Date.valueof(sdate1).addMonths(Integer.ValueOf(MapoftrackingofProduct.get(CP.id).Maintenance_Frequency__c));
                       flag = true;
                   }
                }
                if(flag == true) {
                    casePartList.add(tempCP);
                }                    
        
            } 
            //Update CasePartList  
            if(casePartList.size() > 0) {
                updateFlag = true;
               update casePartList;
            }
        }

    }
 

   
    
   //Method for Associate_Case_Parts 
   public static void Associate_Case_Parts(List<Workorder_SparePart__c> objCPList){
 
       //Map<id,Workorder_SparePart__c> MapofEachreordsofCP=new Map<id,Workorder_SparePart__c>();
       Map<id,id> MapWoofCasePart=new Map<id,id>();
       Map<id,Workorder__c> mapofWorkOrder = new Map<id,Workorder__c>();
       Map<id,Case> mapOfCaseForEachId = new Map<id,Case>();
       Set<Id> workOrderSet = new Set<id>();
       Set<Id> caseSet = new Set<Id>();
       for(Workorder_SparePart__c CP:objCPList)
       { 
           //MapofEachreordsofCP.put(CP.id,CP);
           //CasePart's Complaint,Case are Null,But Work_Order is not Null
           if(CP.Complaint__c == Null && CP.Case__c == Null && CP.Workorder__c != Null)
               workOrderSet.add(CP.Workorder__c);
           //CasePart's Complaint is  Null,But Case is not Null   
           else if(CP.Complaint__c == Null && CP.Case__c != Null)
               caseSet.add(CP.Case__c );
           //CasePart's Complaint,Case,Work_Oder are  Null,then show error
           else if(CP.Complaint__c == Null && CP.Case__c == Null && CP.Workorder__c == Null)
               CP.adderror('A Workorder Sparepart requires either a Complaint,Case,Workorder in order to save the record');    
       }
       //Get case,Complaint From WorkOrder Record
       if(workOrderSet.size()>0)
       {
           for(Workorder__c objWO : [Select id,Case__c,Complaint__c FROM Workorder__c where id IN:workOrderSet]) {
           
               mapofWorkOrder.put(objWO.Id, objWO);
           }
       }
       //Get Complaint From Case Record
       if(caseSet.size()>0)
       {
           for(Case objCase :[Select id,Complaint__c FROM Case WHERE id IN : caseSet]) {
           
               mapOfCaseForEachId.put(objCase.Id, objCase);
           }
       }
       
       for(Workorder_Sparepart__c objCP : objCPList) {
          //Set casePart Case And complaint With Work_Oder's case And complaint
          if(mapofWorkOrder.get(objCP.Workorder__c) != null) {
              objCP.Case__c = mapofWorkOrder.get(objCP.Workorder__c).Case__c;
              objCP.Complaint__c = mapofWorkOrder.get(objCP.Workorder__c).Complaint__c;                 
          } 
          //Set casePart Case And complaint With Case's case And complaint
          else if(mapOfCaseForEachId.get(objCP.Case__c) != null) {              
              objCP.Case__c = mapOfCaseForEachId.get(objCP.Case__c).Id;
              objCP.Complaint__c = mapOfCaseForEachId.get(objCP.Case__c).Complaint__c;
                      
          }
                  
        }       
    }
    
    //Method for ValidateSW
    public static void ValidateSW(List<Workorder_SparePart__c> objCPList) {

       String sysType='';
       String Softsys=''; 
       Integer flag=0;
       Map<id,Workorder_SparePart__c> MapofCPRecord=new  Map<id,Workorder_SparePart__c>();
       //Check Recorstype is Software fRom SobjectType=CasePart
       Id  rectypeId=[Select id,DeveloperName from RecordType where DeveloperName='Software' AND sobjectType='Workorder_Sparepart__c'].id;
        Set<ID> CaseSet=new Set<ID>();
        Set<Id> WorkOrderSet=new Set<ID>();
        Set<Id> ComplaintSet=new Set<ID>();
        Set<Id> Softwareset=new Set<Id>();
        Map<id,Case> mapOfCaseForEachId=new Map<id,Case>();
        Map<id,Workorder__c> mapOfWorkOrderForEachId=new Map<id,Workorder__c>();
        Map<id,Complaint__c>  mapOfComplaintForEachId=new Map<id,Complaint__c>();
        Map<id,Software__c>   mapOfSoftwareForEachId=new Map<id,Software__c>();
        //Adding all picklist values oF System type in CompValuesSet
        Set<String> CompValuesSet=new Set<String>();
        CompValuesSet.add('100 C "Pregnant" Blue Cart');
        CompValuesSet.add('iON Cart');
        CompValuesSet.add('Laptop');
        CompValuesSet.add('O2');
        CompValuesSet.add('O-Arm 1000');
        CompValuesSet.add('Instrument Kit');
        CompValuesSet.add('O-Arm 1000 3rd Edition');
        CompValuesSet.add('3rd Edition');
        CompValuesSet.add('Polestar N-10');
        CompValuesSet.add('Polestar N-20');
        CompValuesSet.add('Polestar N-30');
        CompValuesSet.add('Siemens Iso-C C-Arm');
        CompValuesSet.add('Gen II');
        CompValuesSet.add('Server');  

        //-BC - 20161110 - placed SOQL outside loop and don't SOQL Product2 object because this is not needed - START
        Set<Id> setID_Software = new Set<Id>();
        for (Workorder_SparePart__c oWorkorderSparepart : objCPList){
          if (oWorkorderSparepart.recordtypeid == rectypeId){
            if (oWorkorderSparepart.Software__c != null){
              setID_Software.add(oWorkorderSparepart.Software__c);
            }
          }
        }
        Map<Id, Software__c> mapSoftware = new Map<Id, Software__c>();
        if (setID_Software.size() > 0){
          mapSoftware = new Map<Id, Software__c>([SELECT Software_Product__c FROM Software__c WHERE Id = :setID_Software AND Software_Product__c != null]);
        }
        //-BC - 20161110 - placed SOQL outside loop and don't SOQL Product2 object because this is not needed - STOP

        
        for(Workorder_SparePart__c CP:objCPList)
        {
            //check recordType is Software and taking Software Product from software which is equal to Software of CasePart
            if(CP.recordtypeid==rectypeId){
            //sets the order product to the product from software object
                //-BC - 20161110 - Code optimization - START
//                List<Product2> productId = [select Id from Product2 where Id in (select software_product__c from software__c where Id = :CP.Software__c)];
//                for(Product2 myproductId: productId)
//                {
//                    CP.Order_Part__c = myproductId.Id;
//                }
                if (mapSoftware.containsKey(CP.Software__c)){
                  CP.Order_Part__c = mapSoftware.get(CP.Software__c).Software_Product__c;
                }
                //-BC - 20161110 - Code optimization - STOP
            }
            //MapofCPRecord.put(CP.id,CP);
            //Checking recordType is equal to Software and Case of casePart not equal to Null
            if(CP.recordtypeid==rectypeId && CP.Case__c!=Null)
            {   
                CaseSet.add(CP.Case__c);
            }
            //Checking recordType is equal to Software and WorkOrder of casePart not equal to Null
            else if(CP.recordtypeid==rectypeId && CP.Workorder__c!=Null)
            {
               WorkOrderSet.add(CP.Workorder__c);
            }
            //Checking recordType is equal to Software and Complaint of casePart not equal to Null
            else if(CP.recordtypeid==rectypeId && CP.Complaint__c!=Null)
            {
               ComplaintSet.add(CP.Complaint__c);
            }
            //Checking recordType is equal to Software and Software of casePart not equal to Null
            if(CP.recordtypeid==rectypeId  && CP.Software__c != Null)  
            {
                Softwareset.add(CP.Software__c);
            }
        }
        //Take systemType From Case,then add caseID in Map
        if(CaseSet.size()>0)
        {
            for(Case objCase :[Select id,Asset_Product_Type__c,Asset.Asset_Product_Type__c FROM Case WHERE id IN : caseSet]) {   
                mapOfCaseForEachId.put(objCase.id,objCase);
            }
        }
        //Take systemType From Work_Order,then add WorkOrderID in Map
        if(WorkOrderSet.size()>0)
        {
            for(Workorder__c objWorkOrder :[Select id,Asset_Product_Type__c  FROM Workorder__c WHERE ID IN:WorkOrderSet]) {
                mapOfWorkOrderForEachId.put(objWorkOrder.id,objWorkOrder );
            }
        }
        //Take systemType From Complaint,then add Complaintid in Map
        if(ComplaintSet.size()>0)
        {
            for(Complaint__c objComplaint :[Select id,Asset__r.Asset_Product_Type__c from Complaint__c WHERE Id IN:ComplaintSet]) {
       
             mapOfComplaintForEachId.put(objComplaint.id,objComplaint );
            }
        } 
        //Take systemType From Software,then add Softwareid in Map 
        if(Softwareset.size()> 0)
        {
            for(Software__c objsoftware :[Select id,Asset_Product_Type__c from Software__c WHERE Id IN:Softwareset]){
                mapOfSoftwareForEachId.put(objsoftware.id,objsoftware ); 
           }
        } 
        
        for(Workorder_SparePart__c objCP : objCPList) {            

            if (objCP.recordtypeid != rectypeId) continue; // The logic / validation below should only be execute for Software CaseParts

            if(mapOfCaseForEachId.size() > 0) {
                //If casePart Case not equal to null,aDd System type oF case     
                if(mapOfCaseForEachId.get(objCP.Case__c)!= null)
                    sysType = mapOfCaseForEachId.get(objCP.Case__c).Asset_Product_Type__c;                 
            }
            else if(mapOfWorkOrderForEachId.size() > 0){
                //If casePart WorkOrder not equal to null,add System type oF WorkOrder     
                if(mapOfWorkOrderForEachId.get(objCP.Workorder__c)!= null)
                    sysType = mapOfWorkOrderForEachId.get(objCP.Workorder__c).Asset_Product_Type__c;                       
            }
            else if(mapOfComplaintForEachId.size() > 0){
                //If casePart Complaint not equal to null,add System type oF Complaint     
                if(mapOfComplaintForEachId.get(objCP.Complaint__c)!= null)
                    sysType = mapOfComplaintForEachId.get(objCP.Complaint__c).Asset__r.Asset_Product_Type__c;                 
            }
            //Systemtype matches with previous mentioned pickList valus , then do nothing.
            if(CompValuesSet.Contains(sysType)) {
                 //set the flag here...
                flag=1;
            }
            //Systemtype doea not match with previous mentioned pickList valus                 
            if(flag == 0){
                if(mapOfSoftwareForEachId.size() > 0) {
                    if(mapOfSoftwareForEachId.get(objCP.Software__c)!=null){
                        Softsys = mapOfSoftwareForEachId.get(objCP.Software__c).Asset_Product_Type__c;
                    } 
                    //Does not match System type Field of Software  with Systype,then show error                   
                    if(Softsys!= null){
                        if(!Softsys.contains(sysType)){
                            objCP.addError('You must choose the software that is valid for the Asset');   
                        }              
                    }
                    else
                        // Systemtype of Software record is Null
                        objCP.adderror('There is no Asset Product Type Setup on this Software Record');
                } 
            }
        }
    } 
    
    public static void calculateWorkOrderTotal(Set<Id> woToCalculate){
    	
    	List<Workorder__c> workorders = [Select Id, Order_Parts_Total__c, (Select Order_Part_Total_Price__c from Workorder_Spareparts__r) from Workorder__c where Id IN :woToCalculate];
    	
    	for(Workorder__c wo : workorders){
    		
    		wo.Order_Parts_Total__c = 0;
    		
    		for(Workorder_Sparepart__c sPart : wo.Workorder_Spareparts__r){
    			
    			if(sPart.Order_Part_Total_Price__c != null) wo.Order_Parts_Total__c += sPart.Order_Part_Total_Price__c;
    		}
    	}
    	
    	update workorders;
    }
}