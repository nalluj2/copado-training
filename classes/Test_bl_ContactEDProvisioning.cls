@isTest
private class Test_bl_ContactEDProvisioning {
	
	private static testMethod void testProvisionContact_NoProvisioning(){
		
		Contact cnt = [Select Id from Contact WHERE RecordType.DeveloperName != 'MDT_Employee'];
		
		Test.startTest();
		
			Contact_ED_Sync__c sync = new Contact_ED_Sync__c();
				sync.Contact__c = cnt.Id;
			insert sync;
		
			Test_ED_Provisioning_Mock mock = new Test_ED_Provisioning_Mock();
				mock.findUser = false;
			Test.setMock(HttpCalloutMock.class, mock);
		
		Test.stopTest();
		
		sync = [Select Outcome__c, Outcome_Details__c from Contact_ED_Sync__c where Id = :sync.Id];		
	
		System.assert(sync.Outcome__c == 'Success');
		System.assert(sync.Outcome_Details__c.contains('User does not require provisioning.'));
		
		cnt = [Select LMS_IsActive__c, LMS_UserId__c from Contact WHERE Id = :cnt.Id];
		
		System.assert(cnt.LMS_IsActive__c == false);
		System.assert(cnt.LMS_UserId__c == null);		
	}
	
	private static testMethod void testProvisionContact_EDProvisioning_Create(){
		
		Campaign cmp = [Select Id from Campaign];
			cmp.LMS_ED_Group__c = 'TestGroup';
		update cmp;
		
		Contact cnt = [Select Id from Contact WHERE RecordType.DeveloperName != 'MDT_Employee'];
		List<CampaignMember> cmpMember = [Select Id, Status, Campaign.LMS_ED_Group__c from CampaignMember where Campaign.RecordType.DeveloperName = 'LEARNING'  AND ContactID = :cnt.Id];

		Test.startTest();
		
			Contact_ED_Sync__c sync = new Contact_ED_Sync__c();
				sync.Contact__c = cnt.Id;
			insert sync;
		
			Test_ED_Provisioning_Mock mock = new Test_ED_Provisioning_Mock();
				mock.findUser = false;
		
			Test.setMock(HttpCalloutMock.class, mock);
		
		Test.stopTest();
		
		sync = [Select Outcome__c, Outcome_Details__c from Contact_ED_Sync__c where Id = :sync.Id];
	
		System.assert(sync.Outcome__c == 'Success');
		System.assert(sync.Outcome_Details__c.contains('User provisioned.'));
		
		cnt = [Select LMS_IsActive__c, LMS_UserId__c from Contact WHERE Id = :cnt.Id];
		
		System.assert(cnt.LMS_IsActive__c == false);
		System.assert(cnt.LMS_UserId__c != null);		
	}
	
	private static testMethod void testProvisionContact_EDProvisioning_Update(){
		
		Campaign cmp = [Select Id from Campaign];
			cmp.LMS_ED_Group__c = 'TestGroup';
		update cmp;
		
		Contact cnt = [Select Id from Contact WHERE RecordType.DeveloperName != 'MDT_Employee'];
		
		Test.startTest();
		
			Contact_ED_Sync__c sync = new Contact_ED_Sync__c();
				sync.Contact__c = cnt.Id;
			insert sync;
		
			Test_ED_Provisioning_Mock mock = new Test_ED_Provisioning_Mock();
				mock.findUser = true;
				mock.findWithGroups = true;
		
			Test.setMock(HttpCalloutMock.class, mock);
		
		Test.stopTest();
		
		sync = [Select Outcome__c, Outcome_Details__c from Contact_ED_Sync__c where Id = :sync.Id];		
		System.assert(sync.Outcome__c == 'Success');
		System.assert(sync.Outcome_Details__c.contains('User modified'));
		
		cnt = [Select LMS_IsActive__c, LMS_UserId__c from Contact WHERE Id = :cnt.Id];
		
		System.assert(cnt.LMS_IsActive__c == false);
		System.assert(cnt.LMS_UserId__c != null);		
	}
	
	private static testMethod void testProvisionContact_LMSProvisioning(){
		
		Contact cnt = [Select Id from Contact WHERE RecordType.DeveloperName != 'MDT_Employee'];
			cnt.LMS_Division__c = 'Test Division';
		update cnt;
		
		Test.startTest();
		
			Contact_ED_Sync__c sync = new Contact_ED_Sync__c();
				sync.Contact__c = cnt.Id;
			insert sync;
		
			Test_ED_Provisioning_Mock mock = new Test_ED_Provisioning_Mock();
				mock.findUser = false;
	
			Test.setMock(HttpCalloutMock.class, mock);
		
		Test.stopTest();
		
		sync = [Select Outcome__c from Contact_ED_Sync__c where Id = :sync.Id];		
		System.debug(sync);
		System.assert(sync.Outcome__c == 'Success');
		
		cnt = [Select LMS_IsActive__c, LMS_UserId__c from Contact WHERE Id = :cnt.Id];
		
		System.assert(cnt.LMS_IsActive__c == true);
		System.assert(cnt.LMS_UserId__c != null);		
	}
	
	private static testMethod void testProvisionContact_ErrorLogin(){
		
		Contact cnt = [Select Id from Contact WHERE RecordType.DeveloperName != 'MDT_Employee'];
		
		Test.startTest();
		
			Contact_ED_Sync__c sync = new Contact_ED_Sync__c();
				sync.Contact__c = cnt.Id;
			insert sync;
		
			Test_ED_Provisioning_Mock mock = new Test_ED_Provisioning_Mock();
				mock.loginError = true;
		
			Test.setMock(HttpCalloutMock.class, mock);
		
		Test.stopTest();
		
		sync = [Select Outcome__c, Outcome_Details__c from Contact_ED_Sync__c where Id = :sync.Id];		
	
		System.assert(sync.Outcome__c == 'Failure');
		System.assert(sync.Outcome_Details__c.contains('Error acquiring session token:'));		
	}
	
	private static testMethod void testProvisionContact_EDProvisioning_CreateError(){
		
		Campaign cmp = [Select Id from Campaign];
			cmp.LMS_ED_Group__c = 'TestGroup';
		update cmp;
		
		Contact cnt = [Select Id from Contact WHERE RecordType.DeveloperName != 'MDT_Employee'];
		List<CampaignMember> cmpMember = [Select Id, Status, Campaign.LMS_ED_Group__c from CampaignMember where Campaign.RecordType.DeveloperName = 'LEARNING'  AND ContactID = :cnt.Id];

		Test.startTest();
		
			Contact_ED_Sync__c sync = new Contact_ED_Sync__c();
				sync.Contact__c = cnt.Id;
			insert sync;
		
			clsUtil.tExceptionName = 'bl_ContactEDProvisioning.doUserProvision_EXCEPTION';
			Test_ED_Provisioning_Mock mock = new Test_ED_Provisioning_Mock();
				mock.findUser = false;
				mock.createUserError = true;
		
			Test.setMock(HttpCalloutMock.class, mock);
		
		Test.stopTest();
		
		sync = [Select Outcome__c, Outcome_Details__c from Contact_ED_Sync__c where Id = :sync.Id];
	
		System.assertEquals(sync.Outcome__c, 'Failure');
		System.assert(sync.Outcome__c == 'Failure');
//		System.assert(sync.Outcome_Details__c.startsWith('Error creating user:'));			
		System.assert(sync.Outcome_Details__c.contains('bl_ContactEDProvisioning.doUserProvision_EXCEPTION'));
	
	}
	
	@TestSetup
	private static void generateData(){
						
		clsTestData_MasterData.createCountry();
		
		Id id_RecordType_Campaign = clsUtil.getRecordTypeByDevName('Campaign', 'LEARNING').Id;

		clsTestData_CustomSetting.mapCampaignMemberStatus_Responded = new Map<String, Boolean>{'Start enrollment'=>true};
		clsTestData_CustomSetting.createCampaignMemberStatus(true, 'RecordTypeID', id_RecordType_Campaign);
		
		Account acc = new Account();
			acc.Name = 'Test Acc';
			acc.Account_Country_vs__c = 'NETHERLANDS';
		insert acc;
		
		clsTestData_Contact.createMDTEmployee();
		System.assertEquals(clsTestData_Contact.oMain_MDTEmployee.Contact_Active__c, true);
		System.assertEquals(clsTestData_Contact.oMain_MDTEmployee.RecordTypeId, clsUtil.getRecordTypeByDevName('Contact', 'MDT_Employee').Id);
		Contact cnt = new Contact();
			cnt.AccountId = acc.Id;
			cnt.FirstName = 'Unit';
			cnt.LastName = 'Test';
			cnt.Email = 'unit.test@medtronic.com';
			cnt.Contact_Department__c = 'Diabetes Adult'; 
			cnt.Contact_Primary_Specialty__c = 'ENT';
			cnt.Affiliation_To_Account__c = 'Employee';
			cnt.Primary_Job_Title_vs__c = 'Manager';
			cnt.Contact_Gender__c = 'Male';
			cnt.LMS_Country__c = 'NL';
			cnt.LMS_Language__c = 'nl-NL';
			cnt.LMS_Key_Customer_Contact__c = clsTestData_Contact.oMain_MDTEmployee.Id;
			cnt.RecordTypeId = clsUtil.getRecordTypeByDevName('Contact', 'Generic_Contact').Id;
		insert cnt;
		
		Campaign cmp = new Campaign();		
			cmp.Name = 'TEST CAMPAIGN LMS';
			cmp.Type = 'Training & Education';
			cmp.RecordTypeID = clsUtil.getRecordTypeByDevName('Campaign', 'LEARNING').Id;
 			cmp.IsActive  = true;
       insert cmp;
                    
		CampaignMember cmpMember = new CampaignMember();
			cmpMember.ContactId = cnt.Id;
			cmpMember.Status = 'Start enrollment';
			cmpMember.CampaignId = cmp.Id;        
        insert cmpMember;
        
        ED_API_Settings__c apiSettings = new ED_API_Settings__c();
			apiSettings.API_Key__c = '987654321';
			apiSettings.Token_Authentication__c = '123546789';
			apiSettings.Org_Id__c = UserInfo.getOrganizationId();
			apiSettings.Default_Password__c = 'Medtronic123';
			apiSettings.Target_Server_URL__c  = 'https://fakeURL.com';		
		insert apiSettings; 
	}    
}