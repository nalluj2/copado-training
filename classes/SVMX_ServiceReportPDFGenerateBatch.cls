/**
 *      @author -       Servicemax, Inc
 *      @date   -       07/09/2020 (dd/mm/yyyy)
 *      @description    Apex class clone service report reference from parent to child work orders
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------            
        Adrian Modolea                 04/09/2020         Created the class
        Test coverage                  dd/mm/2020         TODO class is covering test coverage for this class.

*/
global class SVMX_ServiceReportPDFGenerateBatch implements Database.Batchable<sObject>, Schedulable, Database.Stateful
{
    public List<Attachment> allNewAttachments = new List<Attachment>();

    /****************************************************************************
     * Batch constructor method
     ****************************************************************************/ 
    global SVMX_ServiceReportPDFGenerateBatch() {}

    /****************************************************************************
     * Method called at the beginning of a batch Apex job
     * @param bc context contains the job ID
     * @return Database.QueryLocator object that contains the records being passed into the job
     ****************************************************************************/     
    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator([SELECT Id, SVMX_PS_Language__c, SVMXC__Completed_Date_Time__c, SVMX_Parent_Work_Order__r.SVMX_PS_Service_Report_Date_Time__c 
                                            FROM SVMXC__Service_Order__c
                                            WHERE SVMX_Parent_Work_Order__c!=null 
                                                AND SVMX_Parent_Work_Order__r.SVMXC__Completed_Date_Time__c != null
                                         		AND SVMX_Parent_Work_Order__r.SVMX_PS_Is_Site_Visit__c  = true
                                                AND SVMX_Debrief_Complete__c=true
                                                AND SVMX_PS_Service_Report_To_Share__c=false]);
    }

    /****************************************************************************
     * Method to process each chunk of records in the batch job
     * @param   bc context contains the job ID
     *          scope list of sObjects to process for a specific chunk
     ****************************************************************************/  
    global void execute(Database.BatchableContext bc, List<SVMXC__Service_Order__c> scope)
    {
        List<Attachment> attachmentsToCreate = new List<Attachment>();
        List<SVMXC__Service_Order__c> workOrdersToUpdate = new List<SVMXC__Service_Order__c>();

        for(SVMXC__Service_Order__c workOrder : scope) {
            String language = workOrder.SVMX_PS_Language__c;
            Blob contentPDF;

            // Generate the PDF content
            PageReference vfPage = Page.SVMX_PS_Service_Report_Standalone;
            if(language == 'iw')
                vfPage = Page.SVMX_PS_Service_Report_Standalone_Hebrew;
            vfPage.getParameters().put('id', workOrder.Id);
            if(Test.isRunningTest()) { 
  				contentPDF = blob.valueOf('Test Class');
            }
            else{
            	contentPDF = vfPage.getContentAsPDF();
            }
            
            // Create a new attachment
            Attachment attach = new Attachment();
            attach.Body = contentPDF;
            attach.ContentType = 'application/pdf';
            attach.ParentId = workOrder.Id;
            DateTime completedDate = workOrder.SVMX_Parent_Work_Order__r.SVMX_PS_Service_Report_Date_Time__c;
            if(completedDate == null)
                completedDate = workOrder.SVMXC__Completed_Date_Time__c;
            String formattedDate = completedDate.formatGMT('yyyyMMddHHmmss');
            attach.Name = 'SVMX_Service_Report_'+language+'_'+workOrder.Id+'_'+formattedDate+'.pdf';
            attachmentsToCreate.add(attach);

            // Update the work order
            workOrdersToUpdate.add(new SVMXC__Service_Order__c(Id=workOrder.Id, SVMX_PS_Service_Report_To_Share__c=true));
        }

        if(attachmentsToCreate.size() > 0) {
            insert attachmentsToCreate;
            for(Attachment att : attachmentsToCreate)
                allNewAttachments.add(new Attachment(Id=att.Id, ParentId=att.ParentId));
        }
        if(workOrdersToUpdate.size() > 0)
            update workOrdersToUpdate;
    }

    /****************************************************************************
     * Method called after all batches are processed to execute post-processing operations
     * @param bc context contains the job ID
     ****************************************************************************/ 
    global void finish(Database.BatchableContext bc) {
        if(allNewAttachments.size() > 0) {
    	    bl_NotificationSAP.sendNotificationToSAP_Attachment(allNewAttachments, 'INSERT');
    	}
    }

    /****************************************************************************
     * Method to instantiate and schedule an apex batch class 
     * @param sc schedulable context contains the job ID
     ****************************************************************************/
    global void execute(SchedulableContext sc)
    {
        SVMX_ServiceReportPDFGenerateBatch batch = new SVMX_ServiceReportPDFGenerateBatch();
        Database.executeBatch(batch, 25);
    }    
}