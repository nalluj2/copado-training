public with sharing class ctrlExt_CreateUserRequestAdmin {

    private Create_User_Request__c userCreateRequest;
    private ApexPages.StandardController controller;
    private User currentUser ;
    private Map<String, String> mapBUName_BUGName;


    public String tAdminUserId { 
        get; 
        set{
            tAdminUserId = value;
            if ( (value != null) && (value != bl_CreateUserRequest.tNONE) ){
                userCreateRequest.Assignee__c = tAdminUserId;
                if ( (lstSO_AdminUser[0].getLabel() == bl_CreateUserRequest.tNONE) ){
                    lstSO_AdminUser.remove(0);
                }
            }
        }
    }
    public List<SelectOption> lstSO_AdminUser { get; set; }
    
    public List<Attachment> attachments {get; private set;}
    
    public List<UserBU> userBusinessUnits {get; set;}
    public Map<Id, UserBU> userBUById {get; set;}
    public Map<Id, UserSBU> userSBUById {get; set;}
    
    public Boolean selectAll {get; set;}
    public String selectedItem {get; set;}
    
    public Boolean isAdminUser {get;set;}    
    
    public List<String> dataLoadApprovers {get; set;}
    public List<SelectOption> approverOptions {get; set;}

	// Mobile App        
	public List<String> lstSelectedMobileApp { get; set; }
	public List<SelectOption> lstSO_MobileApp { 
		get{
			return loadMobileApp();
		} 
		set; 
	}
    
    public CronTrigger deactivationProcess {get; set;}

    public Boolean bIsUpdatable {
        get{
            Boolean bResult = false;
            if (
                (isAdminUser) && (userCreateRequest.Status__c != 'Cancelled') && (userCreateRequest.Status__c != 'Completed') && (userCreateRequest.Status__c != 'Accepted')
            ){
                bResult = true;
            }
            return bResult;
        }
        private set;
    }

    public Boolean bIsAssigned {
        get{
            Boolean bResult = false;
            if (userCreateRequest.Assignee__c != null){
                bResult = true;
            }
            return bResult;
        }
        private set;
    }

    public ctrlExt_CreateUserRequestAdmin(ApexPages.StandardController sc){

		controller = sc;

        if (!Test.isRunningTest()){
            sc.addFields(new List<String>{'Assignee__c', 'Manager_Email__c', 'Profile_of_same_as_user__c', 'Role_of_same_as_user__c', 'OwnerId', 'CreatedDate', 'Data_Load_Approvers__c', 'Role__c', 'Company_Code__c', 'Primary_sBU__c', 'Mobile_Apps__c'});
        }
        userCreateRequest = (Create_User_Request__c) sc.getRecord();  
        
        currentUser = [Select name, Profile.Name, Email from User where Id =:UserInfo.getUserId()];
        

        lstSO_AdminUser = new List<SelectOption>();
        lstSO_AdminUser.add(new SelectOption(bl_CreateUserRequest.tNONE, bl_CreateUserRequest.tNONE));
        Map<Id, User> mapUser_Admin = bl_CreateUserRequest.loadAdminUsers('Support_Portal_Admins');
        for (User oUser : mapUser_Admin.values()){
            lstSO_AdminUser.add(new SelectOption(oUser.Id, oUser.Name));
        }
        tAdminUserId = userCreateRequest.Assignee__c;
        isAdminUser = false;
        if (mapUser_Admin.containsKey(currentUser.Id)){
            isAdminUser = true;
        }
        
        if(userCreateRequest.Request_Type__c == 'Create User' && userCreateRequest.Application__c == 'Salesforce.com'){

            if(isAdminUser){
                initializeUserConfiguration();              
            }
            
            initializeBUInformation();   
			
            
        }else if(userCreateRequest.Request_Type__c == 'De-activate User' 
                    && (userCreateRequest.Status__c == 'Scheduled')){
            
            if(userCreateRequest.Deactivation_Process_Id__c !=null){
                List<CronTrigger> processes = [SELECT NextFireTime, State FROM CronTrigger WHERE Id = :userCreateRequest.Deactivation_Process_Id__c];
                if(processes.isEmpty()== false) deactivationProcess = processes[0];
            }
        }else if(userCreateRequest.Request_Type__c == 'Data Load' 
                    && (userCreateRequest.Status__c == 'New' || userCreateRequest.Status__c == 'Submitted for Approval')){
            
            if(userCreateRequest.Data_Load_Approvers__c == null) dataLoadApprovers = new List<String>();
            else dataLoadApprovers = userCreateRequest.Data_Load_Approvers__c.split(';');
            
            approverOptions = new List<SelectOption>();
            
            Support_application__c config = Support_application__c.getValues('Data Load Request');    

            for(String approverEmail : config.Emails_of_approvers__c.split(';')){
                approverOptions.add(new SelectOption(approverEmail, approverEmail));    
            }           
        }
        
        attachments = [Select Id, Name, BodyLength, CreatedDate, CreatedById 
                            from Attachment where ParentId=:userCreateRequest.Id ORDER BY CreatedDate ASC]; 
    }
        
    private void initializeUserConfiguration(){
        
        if(userCreateRequest.Username__c == null){
            userCreateRequest.Username__c = userCreateRequest.Email__c;
        }
        
        if(userCreateRequest.Nickname__c == null){
            userCreateRequest.Nickname__c = userCreateRequest.Alias__c;
        }
        
        if(userCreateRequest.Profile__c==null && userCreateRequest.Profile_of_same_as_user__c!=null){
            userCreateRequest.Profile__c=userCreateRequest.Profile_of_same_as_user__c;
        }
        
        if(userCreateRequest.Role__c==null && userCreateRequest.Role_of_same_as_user__c!=null){
            userCreateRequest.Role__c=userCreateRequest.Role_of_same_as_user__c;
        }
    }
    
    private void initializeBUInformation(){
        
        userBusinessUnits = new List<UserBU>();
        userBUById = new Map<Id, UserBU>();
        userSBUById = new Map<Id, UserSBU>();
                
        for(Sub_Business_Units__c sbu:[Select Id, Name, Business_Unit__c, Business_Unit__r.Name from Sub_Business_Units__c where Business_Unit__r.Company__c =: userCreateRequest.Region__c ORDER BY Business_Unit__r.Name,Name ASC]){
            
            UserBU userBU = userBUById.get(sbu.Business_Unit__c);
            
            if(userBU==null){
                userBU = new UserBU();
                userBU.id = sbu.Business_Unit__c;
                userBU.name = sbu.Business_Unit__r.Name;
                userBU.selected = false;
                userBU.sbus = new List<UserSBU>();              
                userBUById.put(sbu.Business_Unit__c, userBU);
                userBusinessUnits.add(userBU);
            }
            
            UserSBU userSBU = new UserSBU();
            userSBU.id = sbu.Id;
            userSBU.name = sbu.Name;
            userSBU.parentBUId = sbu.Business_Unit__c;
            userSBU.selected = false;
            userSBU.primary = false;
            
            userBU.sbus.add(userSBU);
            userSBUById.put(sbu.Id, userSBU);
        }
        
        for(User_Request_BU__c requestBU : [Select Sub_Business_Unit__c, Sub_Business_Unit__r.Business_Unit__c, Primary__c 
                                                from User_Request_BU__c where User_Request__c=:userCreateRequest.Id]){
            
            UserSBU sbu = userSBUById.get(requestBU.Sub_Business_Unit__c);
            sbu.selected=true;
            sbu.primary = requestBU.Primary__c;
            
            UserBU bu = userBUBYId.get(requestBU.Sub_Business_Unit__r.Business_Unit__c);
            bu.selected=true;
        }       
    }
    
    public void selectAllBU(){
        
        for(UserBU bu: userBUById.values()){
            bu.selected = selectAll;
        }
        
        for(UserSBU sbu: userSBUById.values()){
            sbu.selected = selectAll;
            if(selectAll==false) sbu.primary = false;
        }
    }
    
    public void selectBU(){
        
        UserBU bu = userBUById.get(selectedItem);
        
        for(UserSBU sbu : bu.sbus){
            
            sbu.selected = bu.selected;
            if(bu.selected==false) sbu.primary = false;         
        }
        
    }
    
    public void selectSBU(){
        
        UserSBU sbu = userSBUById.get(selectedItem);
        UserBU bu = userBUById.get(sbu.parentBUId);
        
        if(sbu.selected==true){         
            bu.selected=true;
        }else{
            Boolean selected=false;
            for(UserSBU childSBU:bu.sbus){
                if(childSBU.selected==true){
                    selected = true;
                    break;
                }
            }
            bu.selected=selected;
        }
    }
    
    public void makePrimary(){
        
        UserSBU sbu = userSBUById.get(selectedItem);
        
        if(sbu.primary==true){
            
            if(sbu.selected==false){
                sbu.selected=true;
                selectSBU();
            }
            
            for(UserSBU otherSbu : userSBUById.values()){
                if(otherSbu.id!=selectedItem){
                    otherSbu.primary = false;                   
                }   
            }
        }       
    }
        
    public void submit(){
        
        String prevStatus = userCreateRequest.status__c;        
                
        if(userCreateRequest.Request_Type__c == 'Data Load'){
            
            if(dataLoadApprovers.size() == 0){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'At least one Approver is required'));
                return;
            }
            
            userCreateRequest.Data_Load_Approvers__c = '';
            
            for(String approver : dataLoadApprovers){
                userCreateRequest.Data_Load_Approvers__c += approver + ';';
            }
        }
        
        userCreateRequest.status__c='Submitted for Approval';
            
        if(save()){
            
            if(userCreateRequest.Request_Type__c == 'Create User'){
                bl_CreateUserRequest.notifyUserRequestApprovers(new List<Create_User_Request__c>{userCreateRequest});
            }else{
                bl_CreateUserRequest.notifyDataLoadApprovers(userCreateRequest);
            }
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request has been Submitted'));          
            return;
        }
        
        userCreateRequest.Status__c = prevStatus;   
    }
    
    public void resubmit(){
        
        Create_User_Request__c request = [Select Id, Name, Manager_Email__c, Application__c from Create_User_Request__c where Id =:userCreateRequest.Id];
        if(Test.isRunningTest()==false) bl_CreateUserRequest.notifyUserRequestApprovers(new List<Create_User_Request__c>{request});     
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request has been re-Submitted'));
    }
    
    public void completeRequest(){
        
        if (userCreateRequest.Application__c == 'Salesforce.com'){

            if ( (userCreateRequest.Request_Type__c == 'Data Load') && (userCreateRequest.Data_Load_Result__c == null) ){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Please, provide the Data Load results to complete the Request'));         
                return;
            }


            if (tAdminUserId != UserInfo.getUserId()){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Only the Assignee can complete this Request.'));         
                return;
            }


            if (clsUtil.isDecimalNull(userCreateRequest.Time_Spent__c, 0) == 0){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Please, provide the time spent to complete the Request'));         
                return;
            }


            if (!clsUtil.isNull(userCreateRequest.Sub_Status__c, '').contains('Solved')){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Please, provide a "Solved" Sub Status to complete the Request'));         
                return;
            } 

        }

        String prevStatus = userCreateRequest.status__c;        
        userCreateRequest.Status__c = 'Completed';
        userCreateRequest.Completed_By__c = currentUser.Name;
            
        if(save()){
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request Completed successfully'));          
            return;
        }
        
        userCreateRequest.Status__c = prevStatus;
    }
    
    public void reject(){
        
        String prevStatus = userCreateRequest.status__c;        
        userCreateRequest.status__c='Rejected';
        userCreateRequest.Approved_Rejected_By__c = currentUser.Name;
            
        if(save()){
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request has been Rejected'));           
            return;
        }
        
        userCreateRequest.Status__c = prevStatus;                           
    }
    
    public void checkUsernameAvailability(){
        
        List<User> otherUser = [Select Id from User where Username =: userCreateRequest.Username__c];
        
        if(!otherUser.isEmpty()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.warning, 'A User already exists in this Organization and will be updated'));
            return;
        }
        
        Savepoint sp = Database.setSavepoint();
        
        try{
            clsUtil.bubbleException();

            String tAlias = clsUtil.isNull(userCreateRequest.alias__c, '').left(8);

            User testUser = new User();
            testUser.Username = userCreateRequest.Username__c;
            testUser.Firstname = userCreateRequest.Firstname__c;
            testUser.Lastname = userCreateRequest.Lastname__c;
            testUser.ProfileId = [Select Id from Profile where UserType = 'Standard' LIMIT 1].Id; 
            testUser.UserRoleId = [Select Id from UserRole where PortalType = 'None' LIMIT 1].Id;
            testUser.Alias = tAlias;
            testUser.Alias_unique__c = userCreateRequest.alias__c;
            testUser.CommunityNickname = userCreateRequest.Nickname__c;
            testUser.Email = userCreateRequest.Email__c;
            testUser.TimeZoneSidKey = 'Europe/Amsterdam'; 
            testUser.LocaleSidKey = 'en_US';
            testUser.EmailEncodingKey =  'ISO-8859-1';
            testUser.LanguageLocaleKey = 'en_US';
            testUser.IsActive = false;
            testUser.User_Status__c = 'Other';
            testUser.CostCenter__c = userCreateRequest.Cost_Center__c;
            
            insert testUser;
            
        }catch(DMLException e){
                        
            ApexPages.addMessages(e);
            if(Test.isRunningTest()) throw e;
            return;             
        }
        
        Database.rollback(sp);
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Username is available'));
    }
    
    public void assignToMe(){
        
        userCreateRequest.Assignee__c = UserInfo.getUserId();
        tAdminUserId = UserInfo.getUserId();
        if ( (userCreateRequest.Status__c != 'Approved') && (userCreateRequest.Status__c != 'Submitted for approval') ){
            userCreateRequest.Status__c = 'In Progress';
            userCreateRequest.Sub_Status__c = 'Assigned';
        }
        if(save()) ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request assigned successfully'));

    }
    
    public void saveConfiguration(){
        
        if(save()) ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request updated successfully')); 
    }
    
    public void saveAndApply(){
        
        if (userCreateRequest.Application__c == 'Salesforce.com' && clsUtil.isDecimalNull(userCreateRequest.Time_Spent__c, 0) == 0){

            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Please, provide the time spent to complete the Request'));         

        }else{

            String prevStatus = userCreateRequest.Status__c;
            userCreateRequest.Status__c = 'In Execution';

            if(save()){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request was sent for execution'));
                return;
            }
            
            userCreateRequest.Status__c = prevStatus;

        }

    }
            
    private Boolean save(){
        
        SavePoint sp = Database.setSavepoint(); 
        try{
            
            clsUtil.bubbleException();

            if (userCreateRequest.Assignee__c != tAdminUserId){

                if (tAdminUserId != bl_CreateUserRequest.tNONE){
                    userCreateRequest.Assignee__c = tAdminUserId;
                    if (userCreateRequest.Status__c == 'New'){
                        userCreateRequest.Status__c = 'In Progress';
                    }
                    if (userCreateRequest.Sub_Status__c == null){
                        userCreateRequest.Sub_Status__c = 'Assigned';
                    }
                }else{
                    userCreateRequest.Assignee__c = null;
                }

            }

            if(userCreateRequest.Request_Type__c == 'Create User' && userCreateRequest.Application__c == 'Salesforce.com'){

				if (lstSelectedMobileApp != null && lstSelectedMobileApp.size() > 0){
					List<String> lstSelectedMobileApp_Tmp = new List<String>();
					for (String tSelectedMobileApp : lstSelectedMobileApp){
						if (tSelectedMobileApp != '--None--') lstSelectedMobileApp_Tmp.add(tSelectedMobileApp);
					}
					if (lstSelectedMobileApp_Tmp.size() > 0){
						userCreateRequest.Mobile_Apps__c = String.join(lstSelectedMobileApp_Tmp, ';');
					}
				}

                List<User_Request_BU__c> requestBUs = new List<User_Request_BU__c>();
                Boolean primaryExists = false;
                
                for(UserSBU sbu: userSBUById.values()){
                                
                    if(sbu.selected == true){
                        
                        User_Request_BU__c requestBU = new User_Request_BU__c();
                        requestBU.User_Request__c = userCreateRequest.Id;
                        requestBU.Sub_Business_Unit__c = sbu.Id;
                        requestBU.Primary__c = sbu.Primary;
                        
                        if(sbu.Primary==true) primaryExists = true;
                        
                        requestBUs.add(requestBU);
                    }
                }
                
                if(primaryExists == false && userCreateRequest.License_Type__c == 'Full Salesforce') throw new ValidationException('You need to select a Sub Business Unit as primary');
                
                List<User_Request_BU__c> existingRequestBUs = [Select Id from User_Request_BU__c where User_Request__c=:userCreateRequest.Id];
                
                if(!existingRequestBUs.isEmpty()) delete existingRequestBUs;
                if(!requestBUs.isEmpty()) insert requestBUs;
            }
            //Made at the end because it will trigger the execution

            if ( !String.isBlank(userCreateRequest.New_Alias__c) ){
                if (userCreateRequest.New_Alias__c != userCreateRequest.Alias__c){
                    userCreateRequest.Alias__c = userCreateRequest.New_Alias__c;
                    userCreateRequest.New_Alias__c = null;
                }
            }
            update userCreateRequest;
            
        }catch(Exception e){
            
            Database.rollback(sp);          
            ApexPages.addMessages(e);
            if(Test.isRunningTest()) throw e;
            return false;

        }
                
        if(Test.isRunningTest() == false){
        
        	controller.reset();        
        	userCreateRequest = (Create_User_Request__c) controller.getRecord();
        }  
        
        return true;
    }
        
    public void scheduleDeactivation(){
        
        DateTime deactDT = userCreateRequest.Date_of_deactivation__c;       
        DateTime nowDT = DateTime.now();
        
        Long diff = deactDT.getTime() - nowDT.getTime();
        
        if(diff<0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Deactivation Date is in the past and cannot be scheduled'));
            return;
        }       
                
        Decimal minsToDeadline = diff/60000+1;//We round up to the next minute
                            
        ba_Deactivate_User_Process deactBatch = new ba_Deactivate_User_Process();
        deactBatch.userRequestId = userCreateRequest.Id;
        
        User toDeactivate = [Select Alias from User where Id =: userCreateRequest.Created_User__c];
        
        String processId = System.scheduleBatch(deactBatch, 'Deactivate '+toDeactivate.Alias, minsToDeadline.intValue());
        
        userCreateRequest.Deactivation_Process_Id__c = processId;
        userCreateRequest.Status__c = 'Scheduled';
        userCreateRequest.Completed_By__c = currentUser.Name;
        
        update userCreateRequest;
        
        deactivationProcess = [SELECT NextFireTime, State FROM CronTrigger WHERE Id = :processId];
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request Scheduled successfully'));
    }
    
    public void deactivateUser(){
        
        if (userCreateRequest.De_activated_in_Staging__c == True){

			ba_Deactivate_User_Process deactBatch = new ba_Deactivate_User_Process();
			deactBatch.userRequestId = userCreateRequest.Id;
        
			Database.executeBatch(deactBatch);
        
				userCreateRequest.Status__c = 'In Execution';   
				userCreateRequest.Completed_By__c = currentUser.Name;   
			update userCreateRequest;
                
	        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request was sent for execution'));

        }else{
	
	        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.warning, 'Please check if you deactivated the user on Staging first.'));
    
	    }
    
	}
    
    public void resetUserPassword(){

        if (userCreateRequest.Application__c == 'Salesforce.com' && clsUtil.isDecimalNull(userCreateRequest.Time_Spent__c, 0) == 0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Please, provide the time spent to complete the Request'));         
        }else{

            if(Test.isRunningTest() == false) System.resetPassword(userCreateRequest.Created_User__c, true);
            
            userCreateRequest.Status__c= 'Completed';
            userCreateRequest.Sub_Status__c = 'Solved (Permanently)';
            userCreateRequest.Completed_By__c = currentUser.Name;
            update userCreateRequest;
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Confirm, 'Password reset completed successfully'));
        }
    }
    
    public void reactivateUser(){
        
        User user = userCreateRequest.Created_User__r;
                
        try{                        
            
            clsUtil.bubbleException();

            user.isActive = true;   
            user.User_Status__c = 'Current';
            user.Reactivation_Date__c = Date.today();               
            update user;
            
        }catch(Exception e){
            ApexPages.addMessages(e);
            if(Test.isRunningTest()) throw e;
            return;
        }       
        
        user.isActive = false;  
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'User re-activated successfully. You can close the request'));       
    }
    
    public PageReference createCR(){
        
        if (userCreateRequest.Application__c == 'Salesforce.com' && clsUtil.isDecimalNull(userCreateRequest.Time_Spent__c, 0) == 0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Please, provide the time spent to complete the Request'));         
            return null;
        }

        Savepoint sp = Database.setSavepoint();
        
        try{

            clsUtil.bubbleException();
                    
            Change_Request__c cr = new Change_Request__c();
            
            if(userCreateRequest.Request_Type__c == 'Bug') cr.Bug_Fix__c = true;
            else cr.Bug_Fix__c = false;
            
            if(userCreateRequest.Description_Long__c.length() > 255){
                cr.Change_Request__c = userCreateRequest.Description_Long__c.substring(0, 252)+'...';
            }else{
                cr.Change_Request__c = userCreateRequest.Description_Long__c;
            }
            cr.Details__c = userCreateRequest.Description_Long__c;
            
            if(userCreateRequest.Business_Rationale__c!=null) cr.Business_Rationale__c = userCreateRequest.Business_Rationale__c;
            
            cr.Status__c = 'New';
            cr.Origin__c='Support Portal';          
            cr.Request_Date__c = userCreateRequest.CreatedDate.date();

            cr.Related_Ticket__c = userCreateRequest.Name;
            
            if(userCreateRequest.Priority__c!=null) cr.Priority__c = userCreateRequest.Priority__c;
            else cr.Priority__c = 'Medium';         
            
            User requestor = [Select Id, UserType from User where Id=:userCreateRequest.OwnerId];
            if(requestor.UserType != 'Guest'){
                cr.Requestor_Lookup__c = userCreateRequest.OwnerId;
                
            }else{
                cr.Requestor_Lookup__c = UserInfo.getUserId();
            }
            
            insert cr;
                                    
            userCreateRequest.Status__c = 'Completed';
            userCreateRequest.Sub_Status__c = 'Solved (CR)';
            userCreateRequest.Completed_By__c = currentUser.Name;
            userCreateRequest.Created_Change_Request__c = cr.Id;
            
            update userCreateRequest;
            
            AggregateResult[] attachmentSize = [Select SUM(BodyLength) Total from Attachment where ParentId = :userCreateRequest.Id];
            
            if( attachmentSize.size()>0){
                
                List<Attachment> crAttach = new List<Attachment>();
                
                Integer totalSize = Integer.valueOf(attachmentSize[0].get('Total'));
                //We only clone if the attachments are smaller than 5MB in total, to avoid Heap Size limits
                if(totalSize<5242880){
                    
                    for(Attachment attach : [Select Name, Body from Attachment where ParentId = :userCreateRequest.Id]){
                    
                        Attachment cloneAttach = new Attachment();
                        cloneAttach.name = attach.name;
                        cloneAttach.body = attach.body;
                        cloneAttach.ParentId = cr.Id;
                        
                        crAttach.add(cloneAttach);
                    }
                    
                    insert crAttach;
                }
            } 
            
            PageReference pr = new ApexPages.StandardController(cr).edit();
            pr.getParameters().put('retURL', '/'+cr.Id);
            pr.setRedirect(true);           
            
            return pr; 
            
        }catch(Exception e){
            ApexPages.addMessages(e);           
            Database.rollback(sp);
            if(Test.isRunningTest() == true) throw e;
            return null;
        }   
        
        return null;    
    }
    
    public void convertToSR(){
        
        try{
            clsUtil.bubbleException();
            
            if(userCreateRequest.Request_Type__c == 'Bug') userCreateRequest.Service_Request_Type__c = 'Problem';
            else userCreateRequest.Service_Request_Type__c = 'Other';
                        
            userCreateRequest.Request_Type__c = 'Generic Service';      
        
            update userCreateRequest;
        
        }catch(Exception e){
            ApexPages.addMessages(e);
            if(Test.isRunningTest() == true) throw e;           
        }
    }

/*
	@TestVisible private void loadMobileApp(){

		List<String> lstMobileAppName = new List<String>();

		List<Mobile_App__c> lstMobileApp = [SELECT Id, Name FROM Mobile_App__c WHERE Active__c = true ORDER BY Name];

		lstSelectedMobileApp = new List<String>{'--None--'};
		lstSO_MobileApp = new List<SelectOption>();
		lstSO_MobileApp.add(new SelectOption('--None--', '--None--'));
		for (Mobile_App__c oMobileApp : lstMobileApp){
			lstSO_MobileApp.add(new SelectOption(oMobileApp.Name, oMobileApp.Name));
		}

		lstSelectedMobileApp = new List<String>();
		if (userCreateRequest.Mobile_Apps__c != null){
			lstSelectedMobileApp.addAll(userCreateRequest.Mobile_Apps__c.split(';'));
		}

	}
*/

	@TestVisible private List<SelectOption> loadMobileApp(){

		String tBusinessUnitGroup = '';

		if (mapBUName_BUGName == null) loadUserBUName_BUGName();

		if (mapBUName_BUGName != null && mapBUName_BUGName.containsKey(userCreateRequest.User_Business_Unit_vs__c)){
			tBusinessUnitGroup = mapBUName_BUGName.get(userCreateRequest.User_Business_Unit_vs__c);
		}

		String tSOQL = 'SELECT Id, Name FROM Mobile_App__c WHERE Active__c = true';
		if (!String.isBlank(tBusinessUnitGroup)) tSOQL += ' AND (Business_Unit_Group__c = null OR Business_Unit_Group__c = :tBusinessUnitGroup)';

		List<Mobile_App__c> lstMobileApp = Database.query(tSOQL);

		List<SelectOption> lstSO_MobileApp = new List<SelectOption>();
		lstSO_MobileApp.add(new SelectOption('--None--', '--None--'));
		for (Mobile_App__c oMobileApp : lstMobileApp){
			lstSO_MobileApp.add(new SelectOption(oMobileApp.Name, oMobileApp.Name));
		}

		lstSelectedMobileApp = new List<String>();
		if (userCreateRequest.Mobile_Apps__c != null){
			lstSelectedMobileApp.addAll(userCreateRequest.Mobile_Apps__c.split(';'));
		}

		return lstSO_MobileApp;

	}


    private void loadUserBUName_BUGName(){

		mapBUName_BUGName = new Map<String, String>();
		if (userCreateRequest.Region__c != null){
            
			List<Sub_Business_Units__c> lstSBU = 
				[
					SELECT
						Id, Name, Business_Unit__r.Business_Unit_Group__r.Name, Business_Unit__c, Business_Unit__r.Name 
					FROM
						Sub_Business_Units__c
					WHERE
						Business_Unit__r.Company__c = :userCreateRequest.Region__c
					ORDER BY
						Business_Unit__r.Business_Unit_Group__r.Name, Business_Unit__r.Name,Name ASC
				];

			for (Sub_Business_Units__c oSBU : lstSBU){

				mapBUName_BUGName.put(oSBU.Business_Unit__r.Name, oSBU.Business_Unit__r.Business_Unit_Group__r.Name);
				
			}
			
		}

    }

    
    private class UserBU{
        
        public Id id {get; set;}
        public String name {get; set;}
        public Boolean selected {get; set;}
        
        public List<UserSBU> sbus {get; set;}
    } 
    
    private class UserSBU{
        
        public Id id {get; set;}
        public String name {get; set;}
        public Id parentBUId {get; set;}
        public Boolean selected {get; set;}
        public Boolean primary {get; set;}
    }
}