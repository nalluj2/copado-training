public class bl_Procedural_Solutions_Kit_Share {
    
    public static void populateUniqueKey(List<Procedural_Solutions_Kit_Share__c> triggerNew){
    	
    	for(Procedural_Solutions_Kit_Share__c kitShare : triggerNew){
    		
    		kitShare.Unique_Key__c = kitShare.Procedural_Solutions_Kit__c + ':' + kitShare.User__c;
    	}
    }
    
    public static void manageSharing(List<Procedural_Solutions_Kit_Share__c> triggerNew, String action){
    	
    	List<Procedural_Solutions_Kit__Share> shares = new List<Procedural_Solutions_Kit__Share>();
    	
    	Set<Id> kitIds = new Set<Id>();
    	for(Procedural_Solutions_Kit_Share__c kitShare : triggerNew) kitIds.add(kitShare.Procedural_Solutions_Kit__c);
    	
    	if(action == 'INSERT'){
    	   	
    	   	Map<Id, Procedural_Solutions_Kit__c> kitMap = new Map<Id, Procedural_Solutions_Kit__c>([Select Id, OwnerId from Procedural_Solutions_Kit__c where Id = :kitIds]);
    	   	
	    	for(Procedural_Solutions_Kit_Share__c kitShare : triggerNew){
	    		
	    		Procedural_Solutions_Kit__c kit = kitMap.get(kitShare.Procedural_Solutions_Kit__c);
				
				if(kitShare.User__c == kit.OwnerId) continue;//We cannot create a manual share with the Owner of Kit
					    			    		
	    		Procedural_Solutions_Kit__share share = new Procedural_Solutions_Kit__share();
			    share.ParentId = kitShare.Procedural_Solutions_Kit__c;
			    share.UserOrGroupId = kitShare.User__c;
			    share.AccessLevel = 'read';
			    share.RowCause = Schema.Procedural_Solutions_Kit__Share.RowCause.manual;    
			        
			    shares.add(share);
	    	}
	    	
    	}else if(action == 'DELETE'){
    		
    		Map<String, Procedural_Solutions_Kit__Share> existingShares = new Map<String, Procedural_Solutions_Kit__Share>(); 
    			
    		for(Procedural_Solutions_Kit__Share share : [Select ParentId, UserOrGroupId from Procedural_Solutions_Kit__Share where ParentId IN :kitIds]){
    			
    			existingShares.put(share.ParentId + ':' + share.UserOrGroupId, share);
    		}
    		
    		for(Procedural_Solutions_Kit_Share__c kitShare : triggerNew){
	    			
	    		Procedural_Solutions_Kit__Share existingShare = existingShares.get(kitShare.Procedural_Solutions_Kit__c + ':' + kitShare.User__c);	
	    		
	    		if(existingShare != null) shares.add(existingShare);
	    	}		
    	}	
    	
    	if(action == 'INSERT') insert shares;
    	else if(action == 'DELETE') delete shares;
    }
}