@isTest(SeeAllData=true)
private class Test_tr_CampaignMemberBefore {

    static testMethod void campaignMemberShouldUpdate() {
    	
    	Profile prof = [select p.Id
    				    from Profile p
    				    where p.Name =: 'System Administrator'];
    	
    	User userToAdd = new User();
    	userToAdd.ProfileId = prof.id;
    	userToAdd.Alias_unique__c = 'alias1';
    	userTOAdd.Alias ='test1';
    	userToAdd.FirstName ='test1';
    	userToAdd.LastName ='test1';
    	userToAdd.Username ='test2@medtronic.test.com';
    	userToAdd.CommunityNickname ='test1';
    	userToAdd.Email ='tes2t@test.com';
 		userToAdd.EmailEncodingKey = 'ISO-8859-1';
 		userToAdd.LanguageLocaleKey = 'en_US';
 		userToAdd.LocaleSidKey = 'en_US';
 		userToAdd.TimeZoneSidKey = 'Europe/Amsterdam';
    	insert(userToAdd);  
    	
    	
    	
    	System.runAs(userToAdd){
    		
    		Account accountToAdd = new Account();
	    	accountToAdd.Name = 'testAccount';
	    	accountToAdd.RecordTypeId = RecordTypeMedtronic.Account('SAP_Account').id;
	    	insert(accountToAdd); 
	    	
	    	Contact ct=new Contact();
	        ct.AccountID=accountToAdd.Id;
	        ct.LastName='Test';
	        ct.FirstName = 'TEST';
	        ct.Contact_Department__c='Angiology';
	        ct.Contact_Primary_Specialty__c='Administration';
	        ct.Affiliation_To_Account__c='Employee';
	        ct.Primary_Job_Title_vs__c='Administrator';
	        ct.Contact_Gender__c='Male';
	    	insert(ct);
	    	
	        Campaign campaign = new Campaign();
	        campaign.Name = 'testCampaign';
	        campaign.RecordTypeId = RecordTypeMedtronic.Campaign('CVent').Id;
	        insert(campaign);
	        
	        CampaignMember campaignMember = new  CampaignMember();
	        campaignMember.CampaignId = campaign.Id;
	        campaignMember.ContactId = ct.Id;
	        insert(campaignMember);
	        
	        campaignMember.Status = 'Responded';
	        update(campaignMember);
	        
	        System.assertEquals('Responded', campaignMember.Status);
    	}
    }
    
    static testMethod void campaignMemberShouldNotUpdate() {
    	
    	Profile prof = [select p.Id
    				    from Profile p
    				    where p.Name =: 'System Administrator'];
    	
    	User userToAdd = new User();
    	userToAdd.ProfileId = prof.id;
    	userToAdd.Alias_unique__c = 'alias1';
    	userTOAdd.Alias ='test1';
    	userToAdd.FirstName ='test1';
    	userToAdd.LastName ='test1';
    	userToAdd.Username ='test2@medtronic.test.com';
    	userToAdd.CommunityNickname ='test1';
    	userToAdd.Email ='tes2t@test.com';
 		userToAdd.EmailEncodingKey = 'ISO-8859-1';
 		userToAdd.LanguageLocaleKey = 'en_US';
 		userToAdd.LocaleSidKey = 'en_US';
 		userToAdd.TimeZoneSidKey = 'Europe/Amsterdam';
    	insert(userToAdd);  
    	    	
        CampaignMember campaignMember = new  CampaignMember();
        Campaign campaign = new Campaign();
        
    	System.runAs(userToAdd){
    		
    		Account accountToAdd = new Account();
	    	accountToAdd.Name = 'testAccount';
	    	accountToAdd.RecordTypeId = RecordTypeMedtronic.Account('SAP_Account').id;
	    	insert(accountToAdd); 
	    	
	    	Contact ct=new Contact();
	        ct.AccountID=accountToAdd.Id;
	        ct.LastName='Test';
	        ct.FirstName = 'TEST';
	        ct.Contact_Department__c='Angiology';
	        ct.Contact_Primary_Specialty__c='Administration';
	        ct.Affiliation_To_Account__c='Employee';
	        ct.Primary_Job_Title_vs__c='Administrator';
	        ct.Contact_Gender__c='Male';
	    	insert(ct);
	    	
	        campaign.Name = 'testCampaign';
	        campaign.RecordTypeId = RecordTypeMedtronic.Campaign('CVent').Id;
	        insert(campaign);
	        
	        campaignMember.CampaignId = campaign.Id;
	        campaignMember.ContactId = ct.Id;
	        insert(campaignMember);
	        	
    	}    
    	
    	prof = [select p.Id
				    from Profile p
				    where p.Name =: 'ANZ DIB DTC'];
	
    	userToAdd.ProfileId = prof.id;
    	update(userToAdd);
    	    
    	System.runAs(userToAdd){	
	        try{        	
			        campaignMember.Status = 'Responded';
			        update(campaignMember);  
	        }
	        catch(Exception e)
			{
				Boolean expectedExceptionThrown =  e.getMessage().contains('CVent campaign members can only be managed from CVent.') ? true : false;
				System.assertEquals(true, expectedExceptionThrown);
			}  
		}  
    }
    
    static testMethod void campaignMemberShouldNotInsert() {
    	
    	Profile prof = [select p.Id
    				    from Profile p
    				    where p.Name =: 'ANZ DIB DTC'];
    	
    	User userToAdd = new User();
    	userToAdd.ProfileId = prof.id;
    	userToAdd.Alias_unique__c = 'alias1';
    	userTOAdd.Alias ='test1';
    	userToAdd.FirstName ='test1';
    	userToAdd.LastName ='test1';
    	userToAdd.Username ='test2@medtronic.test.com';
    	userToAdd.CommunityNickname ='test1';
    	userToAdd.Email ='tes2t@test.com';
 		userToAdd.EmailEncodingKey = 'ISO-8859-1';
 		userToAdd.LanguageLocaleKey = 'en_US';
 		userToAdd.LocaleSidKey = 'en_US';
 		userToAdd.TimeZoneSidKey = 'Europe/Amsterdam';
    	insert(userToAdd);  
    	
    	
    	
    	System.runAs(userToAdd){
    		
    		Account accountToAdd = new Account();
	    	accountToAdd.Name = 'testAccount';
	    	accountToAdd.RecordTypeId = RecordTypeMedtronic.Account('SAP_Account').id;
	    	insert(accountToAdd); 
	    	
	    	Contact ct=new Contact();
	        ct.AccountID=accountToAdd.Id;
	        ct.LastName='Test';
	        ct.FirstName = 'TEST';
	        ct.Contact_Department__c='Angiology';
	        ct.Contact_Primary_Specialty__c='Administration';
	        ct.Affiliation_To_Account__c='Employee';
	        ct.Primary_Job_Title_vs__c='Administrator';
	        ct.Contact_Gender__c='Male';
	    	insert(ct);
	    	
	        Campaign campaign = new Campaign();
	        campaign.Name = 'testCampaign';
	        campaign.RecordTypeId = RecordTypeMedtronic.Campaign('CVent').Id;
	        campaign.Status ='test';
	        insert(campaign);
	             
	        
	        try{
			   	CampaignMember campaignMember = new  CampaignMember();
		        campaignMember.CampaignId = campaign.Id;
		        campaignMember.ContactId = ct.Id;
		        insert(campaignMember);        	
	        }
	        catch(Exception e)
			{
				Boolean expectedExceptionThrown =  e.getMessage().contains('CVent campaign members can only be managed from CVent.') ? true : false;
				System.assertEquals(true, expectedExceptionThrown);
			}       
    	}
    }
    	
    static testMethod void campaignMemberShouldInsert() {
    	
    	Profile prof = [select p.Id
    				    from Profile p
    				    where p.Name =: 'System Administrator'];
    	
    	User userToAdd = new User();
    	userToAdd.ProfileId = prof.id;
    	userToAdd.Alias_unique__c = 'alias1';
    	userTOAdd.Alias ='test1';
    	userToAdd.FirstName ='test1';
    	userToAdd.LastName ='test1';
    	userToAdd.Username ='test2@medtronic.test.com';
    	userToAdd.CommunityNickname ='test1';
    	userToAdd.Email ='tes2t@test.com';
 		userToAdd.EmailEncodingKey = 'ISO-8859-1';
 		userToAdd.LanguageLocaleKey = 'en_US';
 		userToAdd.LocaleSidKey = 'en_US';
 		userToAdd.TimeZoneSidKey = 'Europe/Amsterdam';
    	insert(userToAdd);  
    	
    	
    	
    	System.runAs(userToAdd){
    		
    		Account accountToAdd = new Account();
	    	accountToAdd.Name = 'testAccount';
	    	accountToAdd.RecordTypeId = RecordTypeMedtronic.Account('SAP_Account').id;
	    	insert(accountToAdd); 
	    	
	    	Contact ct=new Contact();
	        ct.AccountID=accountToAdd.Id;
	        ct.LastName='Test';
	        ct.FirstName = 'TEST';
	        ct.Contact_Department__c='Angiology';
	        ct.Contact_Primary_Specialty__c='Administration';
	        ct.Affiliation_To_Account__c='Employee';
	        ct.Primary_Job_Title_vs__c='Administrator';
	        ct.Contact_Gender__c='Male';
	    	insert(ct);
	    	
	        Campaign campaign = new Campaign();
	        campaign.Name = 'testCampaign';
	        campaign.RecordTypeId = RecordTypeMedtronic.Campaign('CVent').Id;
	        campaign.Status ='Responded';
	        insert(campaign);
	             
	        
		   	CampaignMember campaignMember = new  CampaignMember();
	        campaignMember.CampaignId = campaign.Id;
	        campaignMember.ContactId = ct.Id;
	        insert(campaignMember);  
	        
	        System.assertEquals('Responded', campaign.Status);      
    	}
    }

    //- BC - 20150731 - CR-9562 - START
    static testMethod void campaignMemberShouldNotInsertShouldUpdate_PermissionSet() {
        
        List<Profile> lstProfile = [SELECT Id, Name FROM Profile WHERE Name = 'ANZ DIB DTC' OR Name = 'System Administrator'];

        List<User> lstUser = new List<User>();
        User oUser_SA = new User();
        User oUser = new User();
        for (Profile oProfile : lstProfile){
            if (oProfile.Name == 'System Administrator'){

                oUser_SA.ProfileId = oProfile.id;
                oUser_SA.Alias_unique__c = 'aliasSA';
                oUser_SA.Alias ='aliasSA';
                oUser_SA.FirstName ='TEST SA 1';
                oUser_SA.LastName ='TEST SA 1';
                oUser_SA.Username ='testsa1@test2.com3';
                oUser_SA.CommunityNickname ='testsa1';
                oUser_SA.Email ='testsa1@test2.com';
                oUser_SA.EmailEncodingKey = 'ISO-8859-1';
                oUser_SA.LanguageLocaleKey = 'en_US';
                oUser_SA.LocaleSidKey = 'en_US';
                oUser_SA.TimeZoneSidKey = 'Europe/Amsterdam';
                lstUser.add(oUser_SA);

            }else{

                oUser.ProfileId = oProfile.id;
                oUser.Alias_unique__c = 'alias1';
                oUser.Alias ='test1';
                oUser.FirstName ='test1';
                oUser.LastName ='test1';
                oUser.Username ='test2@medtronic.test.com';
                oUser.CommunityNickname ='test1';
                oUser.Email ='tes2t@test.com';
                oUser.EmailEncodingKey = 'ISO-8859-1';
                oUser.LanguageLocaleKey = 'en_US';
                oUser.LocaleSidKey = 'en_US';
                oUser.TimeZoneSidKey = 'Europe/Amsterdam';
                lstUser.add(oUser);

            }
        }
        insert lstUser;  

        List<PermissionSet> lstPermissionSet = [SELECT Id FROM PermissionSet WHERE name = 'Campaign_Cvent_Edit'];
        PermissionSetAssignment oPSA = new PermissionSetAssignment();
            oPSA.AssigneeId = oUser.Id;
            oPSA.PermissionSetId = lstPermissionSet[0].Id;
        insert oPSA;
        
        
        Account oAccount;
        Contact oContact;
        Campaign oCampaign;
        CampaignMember oCampaignMember;

        System.runAs(oUser){
            
            oAccount = new Account();
                oAccount.Name = 'testAccount';
            insert oAccount; 
            
            oContact = new Contact();
                oContact.AccountID = oAccount.Id;
                oContact.LastName = 'Test';
		        oContact.FirstName = 'TEST';
                oContact.Contact_Department__c = 'Angiology';
                oContact.Contact_Primary_Specialty__c = 'Administration';
                oContact.Affiliation_To_Account__c = 'Employee';
                oContact.Primary_Job_Title_vs__c = 'Administrator';
                oContact.Contact_Gender__c = 'Male';
            insert oContact;
            
            oCampaign = new Campaign();
                oCampaign.Name = 'testCampaign';
                oCampaign.RecordTypeId = RecordTypeMedtronic.Campaign('CVent').Id;
                oCampaign.Status ='Responded';
            insert oCampaign;

            Boolean expectedExceptionThrown = false;
            
            try{
                oCampaignMember = new CampaignMember();
                    oCampaignMember.CampaignId = oCampaign.Id;
                    oCampaignMember.ContactId = oContact.Id;
                insert oCampaignMember;         
            }
            catch(Exception e)
            {
                expectedExceptionThrown =  e.getMessage().contains('CVent campaign members can only be managed from CVent.') ? true : false;
            }       
            System.assertEquals(true, expectedExceptionThrown);
            
            System.assertEquals('Responded', oCampaign.Status);      
        }

        System.runAs(oUser_SA){

            Boolean expectedExceptionThrown = false;

            try{
                oCampaignMember = new CampaignMember();
                    oCampaignMember.CampaignId = oCampaign.Id;
                    oCampaignMember.ContactId = oContact.Id;
                insert oCampaignMember;         
            }
            catch(Exception e)
            {
                expectedExceptionThrown = e.getMessage().contains('CVent campaign members can only be managed from CVent.') ? true : false;
            }       
            System.assertEquals(false, expectedExceptionThrown);

        }

        System.runAs(oUser){

            Boolean expectedExceptionThrown = false;
            
            try{
                oCampaignMember.CampaignId = oCampaign.Id;
                oCampaignMember.ContactId = oContact.Id;
                oCampaignMember.Status = 'TEST';
                update oCampaignMember;         
            }
            catch(Exception e)
            {
                expectedExceptionThrown =  e.getMessage().contains('CVent campaign members can only be managed from CVent.') ? true : false;
            }       
            System.assertEquals(false, expectedExceptionThrown);

        }
    }
    //- BC - 20150731 - CR-9562 - STOP
}