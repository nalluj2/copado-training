@isTest
public class Test_STO_Service_Mock  implements HttpCalloutMock{

    protected Boolean isSuccess;
    protected String errorMessage;
    protected String action;
    protected String productId;
    
    public Test_STO_Service_Mock() {
        
        this.isSuccess = true;
        this.errorMessage = null;
        this.action = 'create';
    }
    
    public Test_STO_Service_Mock(Boolean isSuccess, String errorMessage, String action, String productId) {
       
        this.isSuccess = isSuccess;
        this.errorMessage = errorMessage;
        this.action = action;
        this.productId = productId;
    }
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
    // GET A HTTPRESPONSE FOR USE IN TEST APEX
    //----------------------------------------------------------------------------------------
    public HTTPResponse respond(HTTPRequest oHTTPRequest) {

        String tBody = '';

        // Create the Response Body
        if (isSuccess){
        	
        	if(action == 'create'){
	        	
	        	tBody = '<?xml version=\'1.0\' encoding=\'UTF-8\'?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body><ser-root:processCreateSTOResponse xmlns:ser-root="http://msplwa115-pr.wby1-dev.medtronic.com/MdtSapEcc_SMAX_CreateSTO.webservice:processCreateSTO" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
	        	tBody += '<createSTOResponse><responseMessage><messageTypeCode>S</messageTypeCode><messageText>Stock transport ord. created under the number 9999999999</messageText></responseMessage><purchasingDocNumber>9999999999</purchasingDocNumber><sourceSystemPurchasingRef>PRQ-XXXXXXXX</sourceSystemPurchasingRef><technicalSourceSystemPurchasingRef>SMAX_INT_W</technicalSourceSystemPurchasingRef><sourceSystemRef>SMAX_INT_W</sourceSystemRef>';
	        	
	        	
	        	tBody += '<purchaseDocItem><sourceSystemPurchasingItemRef>L-XXXXXXXX</sourceSystemPurchasingItemRef><technicalSourceSystemPurchasingItemRef>a7j1j000000D5jyAAC</technicalSourceSystemPurchasingItemRef><purchasingItemNumber>00010</purchasingItemNumber></purchaseDocItem>';
	        	
	        	tBody += '</createSTOResponse></ser-root:processCreateSTOResponse></soapenv:Body></soapenv:Envelope>';
        	
        	}else{
        		
        		tBody = '<?xml version=\'1.0\' encoding=\'UTF-8\'?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body><ser-root:sendSTODeliveryStatusResponse xmlns:ser-root="http://msplwa115-pr.wby1-dev.medtronic.com/MdtSapEcc_SMAX_STODeliveryStatus.webservice.provider:sendSTODeliveryStatus" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
        		tBody += '<STODeliveryStatusResponse><purchasingDocNumber>99999999</purchasingDocNumber><sourceSystemPurchasingRef>PRQ-XXXXXXXX</sourceSystemPurchasingRef><technicalSourceSystemPurchasingRef>a7k1j000000CcYXAA0</technicalSourceSystemPurchasingRef><purchaseOrderType>UB</purchaseOrderType><supplyingPlantCode>1680</supplyingPlantCode><purchasingDocStatus>9</purchasingDocStatus><purchasingDocDate>20210903</purchasingDocDate><sourceSystemRef>SMAX_INT_W</sourceSystemRef>';
        		
        		tBody +='<itemDetails><item><respMaterialNumber>10097559</respMaterialNumber><respMaterialQuantity>2.0</respMaterialQuantity><respMaterialUOM>EA</respMaterialUOM><respTargetPlantCode>1346</respTargetPlantCode><respTargetPlantLocationCode>T107</respTargetPlantLocationCode><respSupplyingPlantStorageLocationCode>4000</respSupplyingPlantStorageLocationCode><respDeliveryPriorityCode>30</respDeliveryPriorityCode><respShippingConditionCode>ZD</respShippingConditionCode><respShippingRouteCode>AGST2D</respShippingRouteCode><sourceSystemPurchasingItemRef>L-00000061</sourceSystemPurchasingItemRef><technicalSourceSystemPurchasingItemRef>'+ productId +'</technicalSourceSystemPurchasingItemRef><purchasingDocItemNumber>00010</purchasingDocItemNumber><deliveredQuantity>0.0</deliveredQuantity><remainingQuantity>0.0</remainingQuantity><expectedDeliveryDate>20210906</expectedDeliveryDate><deletionIndicator></deletionIndicator><deliveryAddress><respAddressName1>Medtronic UK</respAddressName1><respAddressName2></respAddressName2><respAddressStreet>London Road</respAddressStreet><respAddressCity>Bicester</respAddressCity><respAddressPostalCode>OX26 6HR</respAddressPostalCode><respAddressRegionCode></respAddressRegionCode><respAddressCountryCode>GB</respAddressCountryCode></deliveryAddress>';
        		
        		tBody += '<scheduleItem><item><scheduleLineNumber>0001</scheduleLineNumber><scheduleDeliveryDate>20210906</scheduleDeliveryDate><scheduleDeliveryTime>00:00:00</scheduleDeliveryTime><scheduledDeliveryQuantity>2.0</scheduledDeliveryQuantity><scheduledDeliveryCommitedDate></scheduledDeliveryCommitedDate><scheduledDeliveryCommitedQuantity>0.0</scheduledDeliveryCommitedQuantity><quantityDelivered>0.0</quantityDelivered><quantityIssued>0.0</quantityIssued><goodsIssueDate></goodsIssueDate><goodsIssueTime>00:00:00</goodsIssueTime><goodsReceiptDate></goodsReceiptDate><goodsReceiptTime>00:00:00</goodsReceiptTime><openQuantity>2.0</openQuantity></item></scheduleItem>';
        		
        		tBody += '<deliveryItem><item><deliveryNumber>0001</deliveryNumber><deliveryItemNumber>20210906</deliveryItemNumber><deliveryStatus>STATUS</deliveryStatus><deliveryQuantity>2.0</deliveryQuantity><deliveryTrackingNumber></deliveryTrackingNumber><deliveryCarrier>UPS</deliveryCarrier><deliveryETA>TOMORROW</deliveryETA><deliveryTrackingURL>https://www.medtronic.com</deliveryTrackingURL></item></deliveryItem>';
        		
        		tBody += '<goodsIssueItem><item><deliveryNumber>0001</deliveryNumber><goodsIssueItemNumber>20210906</goodsIssueItemNumber><goodsIssueMaterial>00:00:00</goodsIssueMaterial><goodsIssueUoM>2.0</goodsIssueUoM><goodsIssueBatch></goodsIssueBatch><goodsIssueQuantity>1.0</goodsIssueQuantity><goodsIssueDate>20210906</goodsIssueDate></item></goodsIssueItem>';
        		
        		tBody += '</item></itemDetails><responseMessage><messageTypeCode>S</messageTypeCode><messageText>Document details successfully retrieved</messageText></responseMessage></STODeliveryStatusResponse></ser-root:sendSTODeliveryStatusResponse></soapenv:Body></soapenv:Envelope>';	
        	}
        	
        }else{

			if(action == 'create'){
            	
            	tBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body></soapenv:Body></soapenv:Envelope>';
			}	
        }

        
        // Create a fake HTTP Response
        HttpResponse oHTTPResponse = new HttpResponse();
            oHTTPResponse.setHeader('Content-Type', 'text/xml; charset=utf-8');
            oHTTPResponse.setBody(tBody);
            oHTTPResponse.setStatusCode(200);
            oHTTPResponse.setStatus('OK');
        return oHTTPResponse;
    }
    //----------------------------------------------------------------------------------------   
}