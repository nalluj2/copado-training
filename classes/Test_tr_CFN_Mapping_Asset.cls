@isTest
private class Test_tr_CFN_Mapping_Asset {
    
    /*
    private static testmethod void test_assetInterfacedFlagCalculation(){
    	
    	Id mktProduct_RTId = [Select Id from RecordType where SObjectType = 'Product2' AND DeveloperName = 'MDT_Marketing_Product'].Id;
    	
    	List<Product2> prods = new List<Product2>();
    	
    	for(Integer i = 0; i < 4; i++){
    		
    		Product2 mktProduct = new Product2();
    		mktProduct.name = 'Marketing Product ' + i;
    		mktProduct.RecordTypeId = mktProduct_RTId;
    		prods.add(mktProduct); 
    	}
    	
    	insert prods;
    	
    	Test.startTest();
    	
    	List<CFN_Mapping_Asset__c> mappings = new List<CFN_Mapping_Asset__c>();
    	
    	for(Integer i = 0; i < 3; i++){
    		
    		Product2 prod = prods[i];
    		
    		CFN_Mapping_Asset__c mapping = new CFN_Mapping_Asset__c();
    		mapping.Name = 'UnitTestCFN' + i;
    		mapping.Product__c = prod.Id;
    		
    		mappings.add(mapping);
    	}
    	
    	insert mappings;
    	
    	Product2 prod0 = [Select Id, Linked_Assets_Interfaced__c from Product2 where Id = :prods[0].Id];
    	Product2 prod1 = [Select Id, Linked_Assets_Interfaced__c from Product2 where Id = :prods[1].Id];
    	Product2 prod2 = [Select Id, Linked_Assets_Interfaced__c from Product2 where Id = :prods[2].Id];
    	Product2 prod3 = [Select Id, Linked_Assets_Interfaced__c from Product2 where Id = :prods[3].Id];
    	 
    	System.assert(prod0.Linked_Assets_Interfaced__c == true);
    	System.assert(prod1.Linked_Assets_Interfaced__c == true);
    	System.assert(prod2.Linked_Assets_Interfaced__c == true);
    	System.assert(prod3.Linked_Assets_Interfaced__c == false);
    	    	    	
    	delete mappings[0];
    	
    	prod0 = [Select Id, Linked_Assets_Interfaced__c from Product2 where Id = :prods[0].Id];    	
    	System.assert(prod0.Linked_Assets_Interfaced__c == false);
    	
    	mappings[1].Product__c = prods[3].Id;
    	update mappings[1];
    	
    	prod1 = [Select Id, Linked_Assets_Interfaced__c from Product2 where Id = :prods[1].Id];
    	System.assert(prod1.Linked_Assets_Interfaced__c == false);
    	
    	prod3 = [Select Id, Linked_Assets_Interfaced__c from Product2 where Id = :prods[3].Id];
    	System.assert(prod3.Linked_Assets_Interfaced__c == true);
    	
    	undelete mappings[0];
    	prod0 = [Select Id, Linked_Assets_Interfaced__c from Product2 where Id = :prods[0].Id];    	
    	System.assert(prod0.Linked_Assets_Interfaced__c == true);
    	
    	// Check the trigger doesn't fail if no product is added
    	CFN_Mapping_Asset__c mapping = new CFN_Mapping_Asset__c();
    	insert mapping;
    	
    	delete mapping;
    	
    	undelete mapping;   	
    }
    */
    
    private static testmethod void testDuplicateValidation(){
    	
    	// Regions
    	Company__c eur = new Company__c();    		
        eur.name='Europe';
        eur.CurrencyIsoCode = 'EUR';
        eur.Current_day_in_Q1__c=56;
        eur.Current_day_in_Q2__c=34; 
        eur.Current_day_in_Q3__c=5; 
        eur.Current_day_in_Q4__c= 0;   
        eur.Current_day_in_year__c =200;
        eur.Days_in_Q1__c=56;  
        eur.Days_in_Q2__c=34;
        eur.Days_in_Q3__c=13;
        eur.Days_in_Q4__c=22;
        eur.Days_in_year__c =250;
        eur.Company_Code_Text__c = 'T35';
		insert new List<Company__c>{eur};
		
		// BUGs		
		Business_Unit_Group__c rtgEur = new Business_Unit_Group__c();
		rtgEur.Name = 'Restorative';
		rtgEur.Master_Data__c = eur.Id;
				
		insert new List<Business_Unit_Group__c>{rtgEur};
		
		// BUs
		Business_Unit__c stEur =  new Business_Unit__c();
        stEur.name = 'Cranial Spinal';
        stEur.Company__c = eur.id;
        stEur.Business_Unit_Group__c = rtgEur.Id;
                
        insert new List<Business_Unit__c>{stEur};
    	
    	Id mktProduct_RTId = [Select Id from RecordType where SObjectType = 'Product2' AND DeveloperName = 'MDT_Marketing_Product'].Id;
    	
    	Product2 mktProductEur1 = new Product2();
    	mktProductEur1.name = 'Marketing Product 1';
    	mktProductEur1.RecordTypeId = mktProduct_RTId;
    	mktProductEur1.Business_Unit_ID__c = stEur.Id;
    	
    	Product2 mktProductEur2 = new Product2();
    	mktProductEur2.name = 'Marketing Product 2';
    	mktProductEur2.RecordTypeId = mktProduct_RTId;
    	mktProductEur2.Business_Unit_ID__c = stEur.Id;
    	    	    	
    	insert new List<Product2>{mktProductEur1, mktProductEur2};
    	
    	Test.startTesT();	
    	
    	CFN_Mapping_Asset__c mappingEUR = new CFN_Mapping_Asset__c();
    	mappingEUR.Name = 'UnitTestCFN';
    	mappingEUR.Product__c = mktProductEur1.Id;
    	
    	insert mappingEUR;
    	
    	Boolean isError = false;
    	
    	try{
    		
    		CFN_Mapping_Asset__c mappingEURDupe = new CFN_Mapping_Asset__c();
    		mappingEURDupe.Name = 'UnitTestCFN';
    		mappingEURDupe.Product__c = mktProductEur2.Id;
    	
    		insert mappingEURDupe;
    		
    	}catch(Exception e){
    		
    		System.assert(e.getMessage().contains('duplicate value'));
    		isError = true;
    	}
    	
    	System.assert(isError == true);
    	
    	isError = false;
    	
    }

}