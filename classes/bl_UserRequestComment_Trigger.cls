//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 28-03-2017
//  Description      : APEX Class - Business Logic for tr_UserRequestComment
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public class bl_UserRequestComment_Trigger {

	//----------------------------------------------------------------------------------------------------------------
	// Update the Sub_Status__c of the related Support Request to "Work in progress" a comment is created by 
	//	someone else than the assignee.
	// Also populate the field User_Comment_Received__c with the current date if a comment is created by someone else than
	//	the assignee.  If the assignee creates the comment, we clear the field User_Comment_Received__c.
	//----------------------------------------------------------------------------------------------------------------
	public static void updateCreateUserRequest(List<User_Request_Comment__c> lstUserRequestComment){

		Set<Id> setID_CreateUserRequest = new Set<Id>();
		for (User_Request_Comment__c oUserRequestComment : lstUserRequestComment){

			setID_CreateUserRequest.add(oUserRequestComment.User_Request__c);

		}

		Map<Id, Create_User_Request__c> mapUserRequest = new Map<Id, Create_User_Request__c>(
			[
				SELECT Assignee__c, Assignee__r.Email, Status__c, Sub_Status__c FROM Create_User_Request__c WHERE Id = :setID_CreateUserRequest
			]
		);

		Map<Id, Create_User_Request__c> mapCreateUserRequest_Update = new Map<Id, Create_User_Request__c>();
		for (User_Request_Comment__c oUserRequestComment : lstUserRequestComment){

			Create_User_Request__c oCreateUserRequest = mapUserRequest.get(oUserRequestComment.User_Request__c);

			Create_User_Request__c oCreateUserRequest_Update = oCreateUserRequest;
			if (mapCreateUserRequest_Update.containsKey(oCreateUserRequest.Id)){
				oCreateUserRequest_Update = mapCreateUserRequest_Update.get(oCreateUserRequest.Id);
			}

			Boolean bUpdated = false;

			if (oCreateUserRequest.Assignee__c != null){

				if (UserInfo.getUserEmail() != oCreateUserRequest.Assignee__r.Email){

					// Comment added by someone else as the Assignee of the Support Ticket
					if (oCreateUserRequest.Status__c == 'In Progress'){

						if (oCreateUserRequest_Update.Sub_Status__c != 'Work in progress'){
							oCreateUserRequest_Update.Sub_Status__c = 'Work in progress';
	                        oCreateUserRequest_Update.User_Comment_Received__c = Date.today();
							bUpdated = true;
						}

					}

				}else{

					// Comment added by the Assignee of the Support Ticket
					oCreateUserRequest_Update.User_Comment_Received__c = null;
					bUpdated = true;

				}

			}

			if (bUpdated){
				mapCreateUserRequest_Update.put(oCreateUserRequest_Update.Id, oCreateUserRequest_Update);
			}

		}

		if (mapCreateUserRequest_Update.size() > 0){

			update mapCreateUserRequest_Update.values();

		}

	}
	//----------------------------------------------------------------------------------------------------------------


}
//--------------------------------------------------------------------------------------------------------------------