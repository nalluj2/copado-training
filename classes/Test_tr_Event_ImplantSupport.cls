/*
 *      Created Date    : 20150215
 *      Author          : Rudy De Coninck
 *      Description     : TEST Class for the APEX Trigger test_tr_Event_ImplantSupport
 */
@isTest private class Test_tr_Event_ImplantSupport {


	@isTest 
	static void test_tr_Event_ImplantSupport() {
		
		Account acc = new Account();
		acc.Name = 'RTG Test Account';
		acc.Account_Country_vs__c = 'NETHERLANDS';
		insert acc;
		
		Mobile_App__c app = new Mobile_App__c();
		app.Name = 'Activity Scheduling Tool';
		app.Active__c = true;
		app.Unique_Key__c = 'ACT_SCHDL';
		insert app;
		
		bl_Implant_Scheduling_Team.skipDuringTest = true;
		
		Implant_Scheduling_Team__c team = new Implant_Scheduling_Team__c();			
		team.Name = 'Test Team';
		team.Team_Color__c = 'rgb(99, 99, 99)';
		team.Work_Day_End__c = '18';
		team.Work_Day_Start__c = '8';
		team.Working_Days__c = '1:5';			
		team.RecordTypeId = [Select Id from RecordType where SObjectType = 'Implant_Scheduling_Team__c' AND DeveloperName = 'Activity_Scheduling_Team'].Id;			
		insert team;
		
		Implant_Scheduling_Team_Member__c teamMember = new Implant_Scheduling_Team_Member__c(Team__c = team.Id, Member__c = UserInfo.getUserId());
		insert teamMember;
		
		Test.startTest();
		
		//mock
		Test.setMock(HttpCalloutMock.class, new Test_tr_Event_ImplantSupport.mock_InterfaceMMX());
		bl_Case_Trigger.runningSchedulingApp = true;
				
		Case teamCase = new Case();
		teamCase.AccountId = acc.Id;			
		teamCase.Start_of_Procedure__c = DateTime.now(); 
		teamCase.Procedure_Duration_Implants__c = '03:00';
		teamCase.RecordTypeId = [Select Id from RecordType where SObjectType = 'Case' AND DeveloperName = 'Implant_Scheduling'].Id;
		teamCase.Activity_Scheduling_Team__c = team.Id;
		teamCase.Status = 'Open';			
		insert teamCase;
				
		teamCase.Assigned_To__c = UserInfo.getUserId();
		teamCase.Status = 'Assigned';
		update teamCase;
		
		Test.stopTest();
		
		Event evnt = [Select Id from Event where WhatId = :teamCase.Id];
		
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();
	
		req.requestURI = '/services/apexrest/EventService/' + evnt.Id;  
		req.httpMethod = 'GET';
		
		RestContext.request = req;
		RestContext.response = res;	
		
		ws_Event.IFScheduledImplantEventResponse result =  ws_Event.GET();
				
		ws_Event.IFScheduledImplantEventStatusRequest request = new ws_Event.IFScheduledImplantEventStatusRequest();
	    String JsonMsg=JSON.serialize(request);
	    request.Id = evnt.id;
	    
		req.requestURI = '/services/apexrest/EventService';  
		req.httpMethod = 'POST';
		req.requestBody = Blob.valueOf(JsonMsg);
		RestContext.request = req;
		RestContext.response = res;		
		
		result =  ws_Event.updateEvent();		
	}
	
	private class mock_InterfaceMMX implements HttpCalloutMock {

        public HTTPResponse respond(HTTPRequest req) {
            
            HttpResponse oHTTPResponse = new HttpResponse();
		        oHTTPResponse.setHeader('Content-Type', 'application/json');
		        oHTTPResponse.setBody('{"foo":"bar"}');
		        oHTTPResponse.setStatusCode(200);
            return oHTTPResponse;
        }
	}			
				
}