public without sharing class bl_User_Request_Comment {
	
	public static Map<String,Set<String>> supportAdmins = new Map<String,Set<String>>();
	private static Map<String, Set<String>> appApprovers = new Map<String, Set<String>>();
	private static Set<String> approverStatus = new Set<String>{'Submitted for Approval', 'Approved', 'Rejected'};
	
	static{
		
		Set<String> sfdcAdmins = new Set<String>();
			
		for(User adminUsers : [Select Id, email from User where Id IN 
									(Select UserOrGroupId from GroupMember where Group.DeveloperName = 'Support_Portal_Admins') ]){
			
			sfdcAdmins.add(adminUsers.email.toLowerCase());
		}
		
		supportAdmins.put('Salesforce.com', sfdcAdmins);
		

		Set<String> cventAdmins = new Set<String>();
			
		for(User adminUsers : [Select Id, email from User where Id IN 
									(Select UserOrGroupId from GroupMember where Group.DeveloperName = 'Support_Portal_Cvent') ]){
			
			cventAdmins.add(adminUsers.email.toLowerCase());
		}
		
		supportAdmins.put('CVent', cventAdmins);


		Set<String> botAdmins = new Set<String>();
			
		for(User adminUsers : [Select Id, email from User where Id IN 
									(Select UserOrGroupId from GroupMember where Group.DeveloperName = 'Support_Portal_BOT') ]){
			
			botAdmins.add(adminUsers.email.toLowerCase());
		}
		
		supportAdmins.put('BOT', botAdmins);

		
		Set<String> biAdmins = new Set<String>();
			
		for(User adminUsers : [Select Id, email from User where Id IN 
									(Select UserOrGroupId from GroupMember where Group.DeveloperName = 'Support_Portal_BI') ]){
			
			biAdmins.add(adminUsers.email.toLowerCase());
		}
		
		supportAdmins.put('BI', biAdmins);
		
		
		Set<String> xwAdmins = new Set<String>();
			
		for(User adminUsers : [Select Id, email from User where Id IN 
									(Select UserOrGroupId from GroupMember where Group.DeveloperName = 'Support_Portal_Crossworld') ]){
			
			xwAdmins.add(adminUsers.email.toLowerCase());
		}
		
		supportAdmins.put('Crossworld', xwAdmins);		
		
		Map<String, Support_application__c> config = Support_application__c.getAll();
		
		for(String app : config.keySet()){
			
			Support_application__c appConfig = config.get(app);
			
			appApprovers.put(app, new Set<String>(appConfig.Emails_of_approvers__c.split(';')));						
		}
	}
	
	//Returns the emails of the Support Portal Admins if the comment doesn't come from an Admin User
	public static Set<String> isAdminUser(User_Request_Comment__c comment, Create_User_Request__c request){
		
		Set<String> appAdmins = supportAdmins.get(request.Application__c);

		if(comment.From_Email__c != null && !appAdmins.contains(comment.From_Email__c.toLowerCase())) return appAdmins;
		
		return new Set<String>();	
	}
	
	//Returns the emails of the Support ticket requestor if the comment doesn't come from the requestor
	public static Set<String> isRequestor(User_Request_Comment__c comment, Create_User_Request__c request){
				
		if(comment.From_Email__c != request.Requestor_Email__c) return new Set<String>{request.Requestor_Email__c};
		
		return new Set<String>();
	}
	
	//Returns the emails of the Support ticket approver/s if the comment doesn't come from an approver and the ticket is in an Approve related status
	public static Set<String> isApprover(User_Request_Comment__c comment, Create_User_Request__c request){
				
		if(comment.From_Email__c !=null && request.Request_Type__c == 'Data Load' 
				&& approverStatus.contains(request.Status__c) && request.Data_Load_Approvers__c !=null){
			
			Set<String> approvers = new Set<String>(request.Data_Load_Approvers__c.split(';'));
			
			if(!approvers.contains(comment.From_email__c)) return approvers; 
			
		}else if(comment.From_Email__c !=null && request.Request_Type__c == 'Create User' && approverStatus.contains(request.Status__c)){
			
			if(request.Application__c == 'Salesforce.com'){
				
				if(comment.From_Email__c != request.Manager_Email__c) return new Set<String>{request.Manager_Email__c};
				
			}else{
				
				Set<String> approvers = appApprovers.get(request.Application__c);
				
				if(approvers != null && !approvers.contains(comment.From_email__c)) return approvers;
			}			
		}
				
		return new Set<String>();
	}
	
	public static Map<String, Contact> createDummyContacts(Set<String> emails){
	    
		RecordType mdtRT = [Select Id from RecordType where SobjectType = 'Contact' and DeveloperName = 'MDT_Employee'];
	       	    
	    Map<String, Contact> dummyContacts = new Map<String, Contact>();
	    
		String tTimeStamp = clsUtil.getTimeStamp();
		Integer iCounter = 0;
	    for(String emailAddress : emails){
	      
	    	Contact dummyContact = new Contact();
			dummyContact.Email = emailAddress;
			dummyContact.FirstName = 'dummy';
			dummyContact.LastName = 'contact';
			dummyContact.RecordTypeId = mdtRT.Id;
			dummyContact.Alias_unique__c = tTimeStamp + String.valueOf(iCounter);
			dummyContacts.put(emailAddress, dummyContact);

			iCounter++;
	    }
	    
	    insert dummyContacts.values();
	        
	    return dummyContacts;
	}
}