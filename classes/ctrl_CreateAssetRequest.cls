public with sharing class ctrl_CreateAssetRequest {

    //------------------------------------------------------------------------------------------------------------------------------------
    // PRIVATE VARIABLES
    //------------------------------------------------------------------------------------------------------------------------------------
    @TestVisible private String tID;
    private String tID_Account;
    private String tRecordType;

    private Map<String, Business_Unit__c> mapBusinessUnit = new Map<String, Business_Unit__c>();

    private Account oAccount;
    private Business_Unit__c oBusinessUnit;
    private Asset_Request_Email__c oAssetRequestEmail;

    private Set<Id> setBUID_TypeVisible = new Set<Id>();
    //------------------------------------------------------------------------------------------------------------------------------------
    

    //------------------------------------------------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //------------------------------------------------------------------------------------------------------------------------------------
    public ctrl_CreateAssetRequest() {

        tID = ApexPages.currentPage().getParameters().get('id');

        initialize();

    }
    //------------------------------------------------------------------------------------------------------------------------------------


    //------------------------------------------------------------------------------------------------------------------------------------
    // Initialize Page Variables
    //------------------------------------------------------------------------------------------------------------------------------------
    @TestVisible private void initialize(){

        if (tID.startsWith('001')){
            // Account
            tRecordType = 'ACCOUNT';
            tID_Account = tID;

        }else if (tID.startsWith('a1n')){
            // Account Plan / iPlan (2)
            tRecordType = 'IPLAN';
            Account_Plan_2__c oAccountPlan2 = [SELECT Account__c FROM Account_Plan_2__c WHERE Id = :tID];
            tID_Account = oAccountPlan2.Account__c;
        }
        oAccount = [SELECT Id, Name, Account_Country_vs__c FROM Account WHERE Id = :tID_Account];

        
        // Initialize new Asset Request
        oAssetRequest = new Asset_Request__c();

        bl_AssetRequest.loadBusinessUnitData(oAccount.Account_Country_vs__c);
        setBUID_TypeVisible = bl_AssetRequest.setBUID_TypeVisible;
        mapBusinessUnit = bl_AssetRequest.mapBusinessUnit;
        lstSO_BusinessUnit = bl_AssetRequest.lstSO_BusinessUnit;

        bShowType = false;
        bShowAttachment = false;

    }
    //------------------------------------------------------------------------------------------------------------------------------------


    //------------------------------------------------------------------------------------------------------------------------------------
    // GETTERS & SETTERS
    //------------------------------------------------------------------------------------------------------------------------------------
    public Asset_Request__c oAssetRequest { get; set; }

    public List<SelectOption> lstSO_BusinessUnit { get; private set; }
    public String tSelectedBusinessUnit { get; set; }

    public String tEmailMessage {
        get {
            if ( (oAssetRequestEmail == null) || (String.isBlank(oAssetRequestEmail.Email_Address__c)) ){
                tEmailMessage = 'There is no email address specified for the current selection';
            }else{
                tEmailMessage = 'This request will be sent to "' + oAssetRequestEmail.Email_Address__c + '"';
            }
            return tEmailMessage;
        }
        private set;
    }

    public Boolean bShowType { get; private set;}
    public Boolean bShowAssetChangeType { get; private set; }
    public Boolean bIsSmallCapital { get; private set; }
    public Boolean bShowAttachment { get; private set; }

    // Attachment
    public transient Blob attachment_Body { get; set; }     // must be transient as view state can't handle > 128KB              
    public String attachment_Name { get; set; }     
    //------------------------------------------------------------------------------------------------------------------------------------


    //------------------------------------------------------------------------------------------------------------------------------------
    // ACTIONS
    //------------------------------------------------------------------------------------------------------------------------------------
    public void changedBusinessUnit(){

        oBusinessUnit = mapBusinessUnit.get(tSelectedBusinessUnit);
        if (oBusinessUnit != null){
            oAssetRequest.Business_Unit__c = oBusinessUnit.Id;
            oAssetRequest.Business_Unit__r = oBusinessUnit;
            bShowType = setBUID_TypeVisible.contains(oBusinessUnit.Id);
            changedType();
        }else{
            oAssetRequest.Business_Unit__c = null;
            oAssetRequest.Business_Unit__r = null;
            bShowType = false;
            bShowAssetChangeType = false;
        }
        oAssetRequest.Type__c = null;
        oAssetRequest.Asset_Change_Type__c = null;
        changedType();

        oAssetRequestEmail = getAssetRequestEmail();

    }

    public void changedAssetChangeType(){

        oAssetRequestEmail = getAssetRequestEmail();

    }

    public void changedType(){

        if (oAssetRequest.Type__c == 'Small Capital'){
            bShowAttachment = true;
            bIsSmallCapital = true;
        }else{
            bShowAttachment = false;
            bIsSmallCapital = false;
            oAssetRequest.Asset_Change_Type__c = null;
        }
        
        oAssetRequestEmail = getAssetRequestEmail();

    }
    
    public PageReference cancel(){

        return redirect();
    }
    
    public PageReference save(){

        Boolean bDoSave = true;

        oAssetRequest.Business_Unit__c = tSelectedBusinessUnit;
        oAssetRequestEmail = getAssetRequestEmail();

        // Validate that all required fields are provided
        if (clsUtil.isNull(tSelectedBusinessUnit, 'NONE') == 'NONE'){
            clsUtil.createVFError('Please select a Business Unit from the list');  
            bDoSave = false;
        }else if ( (oAssetRequestEmail == null) || (String.isBlank(oAssetRequestEmail.Email_Address__c)) ){
            clsUtil.createVFError('There is no email address specified for the current selection.  Please contact your System Administrator.');  
            bDoSave = false;
        }

        if (bDoSave){

            List<Attachment> lstAttachment = new List<Attachment>();
            if ( !String.isBlank(attachment_Name) && (attachment_Body != null) ){
                Attachment oAttachment = new Attachment();
                    oAttachment.Name = attachment_Name;
                    oAttachment.Body = attachment_Body;
                lstAttachment.add(oAttachment);
            }

            oAssetRequest.Account__c = tID_Account;
            Boolean bResult = bl_AssetRequest.createAssetRequest(oAssetRequest, oAssetRequestEmail, lstAttachment, true);

            if (bResult){
                return redirect();

            }
        }

        return null;
       
    }
    //------------------------------------------------------------------------------------------------------------------------------------


    //------------------------------------------------------------------------------------------------------------------------------------
    // PRIVATE Methods
    //------------------------------------------------------------------------------------------------------------------------------------
    @TestVisible private Asset_Request_Email__c getAssetRequestEmail(){

        return bl_AssetRequest.getAssetRequestEmail(oAssetRequest, oAccount.Account_Country_vs__c);

    }

    @TestVisible private PageReference redirect(){

        PageReference oPageReference;
        if (tRecordType == 'ACCOUNT'){
            oPageReference = new PageReference('/apex/AccountAssetList?id=' + tID);
        }else if (tRecordType == 'IPLAN'){
            oPageReference = new PageReference('/apex/iPlan2AssetList?id=' + tID);
        }
        oPageReference.setRedirect(true);
        return oPageReference;

    }
    //------------------------------------------------------------------------------------------------------------------------------------


}