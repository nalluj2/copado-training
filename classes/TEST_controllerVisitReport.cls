/**
 * Creation Date :  20090325
 * Description :    Test coverage for controller controllerVisitReport
 * Author :         ABSI - MC
 */
@isTest
private class TEST_controllerVisitReport {
   
    private static testMethod void createxBUCallRecord(){
        
        Map<String, Id> testData = createTestData();
        
        Test.startTest();   
              
        User u = new User(alias = 'standt1' 
                        , email='standarduser@testorg.com'
                        , emailencodingkey='UTF-8'
                        , lastname='TerritoryManagement_Testing'
                        , languagelocalekey='en_US'
                        , localesidkey='en_US'
                        , profileid = UserInfo.getProfileId()
                        , timezonesidkey='America/Los_Angeles'
                        , CountryOR__c='Netherlands '
                        , Country= 'Netherlands '
                        , username='controllerVisitReport@testorg.com'
                        , Alias_unique__c='standarduser_Alias_unique__c'
                        , Company_Code_Text__c='T27'
                        , Region_vs__c = 'Europe'
                      ); 
        insert u;
               
        User_Business_Unit__c USBU = new User_Business_Unit__c();
        USBU.Sub_Business_Unit__c = testData.get('subBusinessUnit');
        USBU.User__c = u.Id;
        USBU.Primary__c = true;
        insert USBU;  
         
        System.runAs(u) {
            
            PageReference pageRef = new PageReference('/VisitReportPage');            
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('BU', testData.get('businessUnit'));
            pageRef.getParameters().put('implAcc', testData.get('account'));
            pageRef.getParameters().put('type', '1:1 visit');
            pageRef.getParameters().put('rt', testData.get('xbuRecordType'));
            
            Call_Records__c vr = new Call_Records__c();
            
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(vr);
            controllerVisitReport controller = new controllerVisitReport(sc);
            
            controller.getCvr1().Attending_contact__c = testData.get('contact');
            
            controller.addAttendingContact();
            
            controller.getActivities();
            controller.activity = testData.get('activityType');
            
            controller.getBUs();            
            controller.setselectedBUs(new List<String>{testData.get('businessUnit')});
            controller.getSelectedBUs();
                        
            controller.getSubjects();            
            controller.setSubject(new List<String>{testData.get('subject1')});
            
            controller.lobbying = 'Not Applicable';
                        
            controller.addSAMTopic();
            
            controller.getTask().Subject = 'test';
            controller.sendNotificationEmailCheckBox = 'True';
            
            PageReference returnRef = controller.saveAndNew();
            
            System.assert(returnRef != null, ApexPages.getMessages().size()>0 ? ApexPages.getMessages()[ApexPages.getMessages().size() - 1].getSummary() : 'Assertion failed. '+returnRef);
        }
    }
    
    private static testMethod void createBUCallRecordFromEvent(){
        
        Map<String, Id> testData = createTestData();
        
        Call_Records__c vr = new Call_Records__c(); 
        vr.Call_Channel__c = '1:1 visit' ; 
        vr.Call_Date__c = system.today()-1;
        vr.Business_Unit__c = testData.get('businessUnit'); 
        insert vr ;            
        
        Event evt = new Event();
        evt.ActivityDate = system.today(); 
        evt.WhatId = vr.Id ; 
        evt.StartDateTime = System.now() ; 
        evt.EndDateTime = System.now() + 3 ;
        evt.whoId = testData.get('contact') ;
        evt.Subject = 'Not specified';
        evt.Description = 'TEST';
        evt.Type = 'Phone call';
        insert evt ;
        
        Test.startTest();   
              
        User u = new User(alias = 'standt1'
                        , email='standarduser@testorg.com'
                        , emailencodingkey='UTF-8'
                        , lastname='TerritoryManagement_Testing'
                        , languagelocalekey='en_US'
                        , localesidkey='en_US'
                        , profileid = UserInfo.getProfileId()
                        , timezonesidkey='America/Los_Angeles'
                        , CountryOR__c='Netherlands'
                        , Country= 'Netherlands'
                        , username='controllerVisitReport@testorg.com'
                        , Alias_unique__c='standarduser_Alias_unique__c'
                        , Company_Code_Text__c='T27'
                        , Region_vs__c = 'Europe'
                      ); 
        insert u;
               
        User_Business_Unit__c USBU = new User_Business_Unit__c();
        USBU.Sub_Business_Unit__c = testData.get('subBusinessUnit');
        USBU.User__c = u.Id;
        USBU.Primary__c = true;
        insert USBU;  
         
        System.runAs(u) {
            
            PageReference pageRef = new PageReference('/VisitReportPage');            
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('id', vr.Id);
            pageRef.getParameters().put('BU', testData.get('businessUnit'));
            pageRef.getParameters().put('conId', testData.get('contact'));
            pageRef.getParameters().put('type', '1:1 visit');
            pageRef.getParameters().put('eventId', evt.Id);
            pageRef.getParameters().put('RecordType', testData.get('buRecordType'));
                        
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(vr);
            controllerVisitReport controller = new controllerVisitReport(sc);
            
            System.assert(controller.getIsNewMode() == false);
            System.assert(controller.getisUpdateMode() == true);
            System.assert(controller.getCItems().size()>0);


            Task oTask = new Task();
                oTask.subject = 'Test Task';
            controller.task = oTask;
            controller.InitTask();
            Affiliation__c oAffiliation = controller.getCItems()[0];
            controller.sendNotificationEmailCheckBox = 'True';
            controller.saveTask(vr, oAffiliation);

            controller.getLobbyings();
            controller.activitySelected();

        }
    }
   
    private static testMethod void testRedirectBU(){
                
        Map<String, Id> testData = createTestData();
        
        Test.startTest();   
              
        User u = new User(alias = 'standt1'
                        , email='standarduser@testorg.com'
                        , emailencodingkey='UTF-8'
                        , lastname='TerritoryManagement_Testing'
                        , languagelocalekey='en_US'
                        , localesidkey='en_US'
                        , profileid = UserInfo.getProfileId()
                        , timezonesidkey='America/Los_Angeles'
                        , CountryOR__c='Netherlands'
                        , Country= 'Netherlands'
                        , username='controllerVisitReport@testorg.com'
                        , Alias_unique__c='standarduser_Alias_unique__c'
                        , Company_Code_Text__c='T27'
                        , Region_vs__c = 'Europe'
                      ); 
        insert u;
                
        Business_Unit__c BU2 = new Business_Unit__c();
        BU2.Name='Test Bu Name';
        BU2.Company__c = testData.get('company');
        insert BU2;
        
        Sub_Business_Units__c SBU2 = new Sub_Business_Units__c();
        SBU2.Name='Test 2 Bu Name';
        SBU2.Business_Unit__c = BU2.Id;
        insert SBU2; 
        
        List<User_Business_Unit__c> userBUs = new List<User_Business_Unit__c>();
               
        User_Business_Unit__c USBU = new User_Business_Unit__c();
        USBU.Sub_Business_Unit__c = testData.get('subBusinessUnit');
        USBU.User__c = u.Id;
        USBU.Primary__c = true;
        userBUs.add(USBU);
        
        User_Business_Unit__c USBU2 = new User_Business_Unit__c();
        USBU2.Sub_Business_Unit__c = SBU2.Id;
        USBU2.User__c = u.Id;
        USBU2.Primary__c = false;
        userBUs.add(USBU2);
        
        insert userBUs;    
        
        CallRecordPageSetting__c settings = new CallRecordPageSetting__c();
        settings.name = 'Business Unit';
        settings.RecordType_Name__c = 'Business Unit';
        settings.Page_Name__c = 'VisitReportPage';
        insert settings;
                        
        System.runAs(u) {
            
            PageReference pageRef = new PageReference('/VisitReportPage');            
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('conId', testData.get('contact'));
            pageRef.getParameters().put('type', '1:1 visit');
            pageRef.getParameters().put('implContact', testData.get('contact'));
            pageRef.getParameters().put('implAcc', testData.get('account'));            
            
            Call_Records__c vr = new Call_Records__c();
            
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(vr);
            controllerVisitReport controller = new controllerVisitReport(sc);
            
            controller.RedirectToDifferntPage();
        }
    }
    
    private static testMethod void testRedirectxBU(){
        
        Map<String, Id> testData = createTestData();
        
        Test.startTest();   
              
        User u = new User(alias = 'standt1'
                        , email='standarduser@testorg.com'
                        , emailencodingkey='UTF-8'
                        , lastname='TerritoryManagement_Testing'
                        , languagelocalekey='en_US'
                        , localesidkey='en_US'
                        , profileid = UserInfo.getProfileId()
                        , timezonesidkey='America/Los_Angeles'
                        , CountryOR__c='Netherlands'
                        , Country= 'Netherlands'
                        , username='controllerVisitReport@testorg.com'
                        , Alias_unique__c='standarduser_Alias_unique__c'
                        , Company_Code_Text__c='T27'
                        , Region_vs__c = 'Europe'
                      ); 
        insert u;
                
        
        User_Business_Unit__c USBU = new User_Business_Unit__c();
        USBU.Sub_Business_Unit__c = testData.get('subBusinessUnit');
        USBU.User__c = u.Id;
        USBU.Primary__c = true;
        insert USBU;    
        
        CallRecordPageSetting__c settings = new CallRecordPageSetting__c();
        settings.name = 'xBU';
        settings.RecordType_Name__c = 'xBU';
        settings.Page_Name__c = 'SAMVisitReportPage';
        insert settings;
        
        RecordType xbuRT = [Select Id from RecordType where SObjectType = 'Call_Records__c' AND Name = 'xBU'];
                        
        System.runAs(u) {
            
            PageReference pageRef = new PageReference('/VisitReportPage');            
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('conId', testData.get('contact'));
            pageRef.getParameters().put('type', '1:1 visit');
            pageRef.getParameters().put('implContact', testData.get('contact'));
            pageRef.getParameters().put('implAcc', testData.get('account')); 
            pageRef.getParameters().put('RecordType', xbuRT.Id);
                       
            Call_Records__c vr = new Call_Records__c();
            
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(vr);
            controllerVisitReport controller = new controllerVisitReport(sc);
            
            controller.RedirectToDifferntPage();
        }
        
    }
    
    
    private static testMethod void updateBUCallRecord(){
        
        Map<String, Id> testData = createTestData();
        
        Test.startTest();   
               
        Call_Records__c vr = new Call_Records__c(); 
        vr.Call_Channel__c = '1:1 visit' ; 
        vr.Call_Date__c = system.today()-1; 
        vr.Business_Unit__c = testData.get('businessUnit');
        vr.RecordTypeId = testData.get('buRecordType');
        insert vr ;    
            
        Contact_Visit_Report__c cd = new Contact_Visit_Report__c (); 
        cd.Call_Records__c = vr.Id ; 
        cd.Attending_Contact__c = testData.get('contact') ; 
        cd.Attending_Affiliated_Account__c = testData.get('account'); 
        insert cd ;
                        
        Call_Topics__c sb = new Call_Topics__c () ; 
        sb.Call_Records__c = vr.Id ; 
        sb.Call_Activity_Type__c = testData.get('activityType');
        sb.Call_Topic_Duration__c=1.0; 
        insert sb ; 
               
        Call_Topic_Products__c ctps= new Call_Topic_Products__c();
        ctps.Call_Topic__c = sb.Id;
        ctps.Product__c = testData.get('product1');
        insert ctps;
               
        Call_Topic_Subject__c CTS = new Call_Topic_Subject__c();
        CTS.Call_Topic__c = sb.Id;
        CTS.Subject__c = testData.get('subject1');
        insert CTS;
        
        User u = new User(alias = 'standt1'
                        , email='standarduser@testorg.com'
                        , emailencodingkey='UTF-8'
                        , lastname='TerritoryManagement_Testing'
                        , languagelocalekey='en_US'
                        , localesidkey='en_US'
                        , profileid = UserInfo.getProfileId()
                        , timezonesidkey='America/Los_Angeles'
                        , CountryOR__c='Netherlands'
                        , Country= 'Netherlands'
                        , username='controllerVisitReport@testorg.com'
                        , Alias_unique__c='standarduser_Alias_unique__c'
                        , Company_Code_Text__c='T27'
                        , Region_vs__c = 'Europe'
                      ); 
        insert u;
               
        User_Business_Unit__c USBU = new User_Business_Unit__c();
        USBU.Sub_Business_Unit__c = testData.get('subBusinessUnit');
        USBU.User__c = u.Id;
        USBU.Primary__c = true;
        insert USBU;  
         
        System.runAs(u) {
                        
            String currPage='/VisitReportPage?';
            currPage += '&id='+  vr.Id ;
            currPage += '&RecordType='+  testData.get('buRecordType') ;
                               
            PageReference pageRef = new PageReference(currPage);            
            Test.setCurrentPage(pageRef);
            
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(vr);
            controllerVisitReport controller = new controllerVisitReport(sc);
            
            System.assert(controller.getCallTopicItems().size()>0);
                                             
            controller.getActivities();
            controller.getTherapies();
            
            controller.activity = testData.get('activityType');
            controller.theraphy = testData.get('therapy');
            controller.category = testData.get('category');
            
            controller.getSubjects();
            controller.getProducts();
            
            controller.setProduct(new List<String>{testData.get('product1')});
            controller.setSubject(new List<String>{testData.get('subject1')});
                        
            controller.addTopic();
            
            PageReference returnRef = controller.save();
            
            System.assert(returnRef != null, ApexPages.getMessages().size()>0 ? ApexPages.getMessages()[0].getSummary() : 'Assertion failed. '+returnRef);
            
            controller.removeSubjectDiscussed();
        }
    }
    
    private static testMethod void updateXBUCallRecord(){
        
        Map<String, Id> testData = createTestData();
        
        Test.startTest();   
               
        Call_Records__c vr = new Call_Records__c(); 
        vr.Call_Channel__c = '1:1 visit' ; 
        vr.Call_Date__c = system.today()-1; 
        vr.Business_Unit__c = testData.get('businessUnit');
        vr.RecordTypeId = testData.get('xbuRecordType');
        insert vr ;    
            
        Contact_Visit_Report__c cd = new Contact_Visit_Report__c (); 
        cd.Call_Records__c = vr.Id ; 
        cd.Attending_Contact__c = testData.get('contact') ; 
        cd.Attending_Affiliated_Account__c = testData.get('account'); 
        insert cd ;
                        
        Call_Topics__c sb = new Call_Topics__c () ; 
        sb.Call_Records__c = vr.Id ; 
        sb.Call_Activity_Type__c = testData.get('activityType');
        sb.Call_Topic_Duration__c=1.0; 
        insert sb ; 
               
        Call_Topic_Business_Unit__c ctbs=new Call_Topic_Business_Unit__c();
        ctbs.Business_Unit__c=testData.get('businessUnit');
        ctbs.Call_Topic__c=sb.id;
        insert ctbs;
               
        Call_Topic_Subject__c CTS = new Call_Topic_Subject__c();
        CTS.Call_Topic__c = sb.Id;
        CTS.Subject__c = testData.get('subject1');
        insert CTS;
        
        User u = new User(alias = 'standt1'
                        , email='standarduser@testorg.com'
                        , emailencodingkey='UTF-8'
                        , lastname='TerritoryManagement_Testing'
                        , languagelocalekey='en_US'
                        , localesidkey='en_US'
                        , profileid = UserInfo.getProfileId()
                        , timezonesidkey='America/Los_Angeles'
                        , CountryOR__c='Netherlands'
                        , Country= 'Netherlands'
                        , username='controllerVisitReport@testorg.com'
                        , Alias_unique__c='standarduser_Alias_unique__c'
                        , Company_Code_Text__c='T27'
                        , Region_vs__c = 'Europe'
                      ); 
        insert u;
               
        User_Business_Unit__c USBU = new User_Business_Unit__c();
        USBU.Sub_Business_Unit__c = testData.get('subBusinessUnit');
        USBU.User__c = u.Id;
        USBU.Primary__c = true;
        insert USBU; 
		 
         
        System.runAs(u) {
                        
            String currPage='/VisitReportPage?';
            currPage += '&id='+  vr.Id ;
            currPage += '&rt='+  testData.get('xbuRecordType') ;
                               
            PageReference pageRef = new PageReference(currPage);            
            Test.setCurrentPage(pageRef);
            
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(vr);
            controllerVisitReport controller = new controllerVisitReport(sc);
            
            System.assert(controller.getCallTopicItems().size()>0);
                                             
            controller.getActivities();
            controller.getBUs();
            
            controller.setselectedBUs(new List<String>{testData.get('businessUnit')});                        
            controller.setSubject(new List<String>{testData.get('subject1')});
                        
            controller.addSAMTopic();
                        
            PageReference returnRef = controller.saveAndNewImplant();
            
            System.assert(returnRef != null, ApexPages.getMessages().size()>0 ? ApexPages.getMessages()[0].getSummary() : 'Assertion failed. '+returnRef);
            
            pageRef.getParameters().put('selectedItem' , cd.Id);
            controller.removeAttendingContact();
                                   
            controller.removeSubjectDiscussed();
        }
    }
    
    private static testmethod void coverlines(){
        
        Map<String, Id> testData = createTestData();
        
        Call_Records__c vr = new Call_Records__c(); 
        vr.Call_Channel__c = '1:1 visit' ; 
        vr.Call_Date__c = system.today()-1;
        vr.Business_Unit__c = testData.get('businessUnit'); 
        insert vr ;            
                
        Test.startTest();   
              
        User u = new User(alias = 'standt1'
                        , email='standarduser@testorg.com'
                        , emailencodingkey='UTF-8'
                        , lastname='TerritoryManagement_Testing'
                        , languagelocalekey='en_US'
                        , localesidkey='en_US'
                        , profileid = UserInfo.getProfileId()
                        , timezonesidkey='America/Los_Angeles'
                        , CountryOR__c='Netherlands'
                        , Country= 'Netherlands'
                        , username='controllerVisitReport@testorg.com'
                        , Alias_unique__c='standarduser_Alias_unique__c'
                        , Company_Code_Text__c='T27'
                        , Region_vs__c = 'Europe'
                      ); 
        insert u;
               
        User_Business_Unit__c USBU = new User_Business_Unit__c();
        USBU.Sub_Business_Unit__c = testData.get('subBusinessUnit');
        USBU.User__c = u.Id;
        USBU.Primary__c = true;
        insert USBU;  
         
        System.runAs(u) {
            
            PageReference pageRef = new PageReference('/VisitReportPage');            
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('id', vr.Id);
            pageRef.getParameters().put('BU', testData.get('businessUnit'));
            pageRef.getParameters().put('conId', testData.get('contact'));
            pageRef.getParameters().put('type', '1:1 visit');
            pageRef.getParameters().put('RecordType', testData.get('buRecordType'));
            
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(vr);
            controllerVisitReport controller = new controllerVisitReport(sc);
            
            controller.resetCallAccCon();
            controller.getcontactToList();
            controller.setaccountsToList('s');
            controller.setcontactToList('x');
            controller.listaccounts();
            controller.getselFromAccountNames();
            controller.getAffFromContactNames();
            controller.getvr();
            controller.setvr(vr);
            controller.setisNewMode(true);
            controller.changeisNewMode();
            controller.getcanSendTask();
            controller.getIsNotCRDM();
            controller.getAccountContactAffiliations(testData.get('account'));
            controller.getProduct();
            controller.getSubject();
            controller.dummyAction();
            controller.cancel();
			controller.vr.Opportunity_ID__c = testData.get('opportunity');
			controller.loadCampaignId();

        }
    }
   
    private static Map<String, Id> createTestData(){ 
    
        Map<String, Id> testData = new Map<String, Id>();
    
        Account acc1 = new Account();
        acc1.Name = 'TEST_controllerVisitReport Test Coverage Account 1 ' ; 
        insert acc1 ;    
        testData.put('account', acc1.Id);           
        
        Contact cont1 = new Contact();
        cont1.LastName = 'TEST_controllerVisitReport Test Coverage' ;  
        cont1.FirstName = 'Test Contact';
        cont1.AccountId = acc1.Id ;  
        cont1.Phone = '009569699'; 
        cont1.Email ='test@coverage.com';
        cont1.Contact_Department__c = 'Diabetes Adult'; 
        cont1.Contact_Primary_Specialty__c = 'ENT';
        cont1.Affiliation_To_Account__c = 'Employee';
        cont1.Primary_Job_Title_vs__c = 'Manager';
        cont1.Contact_Gender__c = 'Male';
        cont1.Primary_Job_Title_vs__c = 'Administrator';
        insert cont1 ;  
        testData.put('contact', cont1.Id);
        

		// Insert Campaign
		clsTestData_Campaign.idRecordType_Campaign = clsUtil.getRecordTypeByDevName('Campaign', 'Europe_xBU').Id;
		Campaign oCampaign = clsTestData_Campaign.createCampaign()[0];
        testData.put('campaign', oCampaign.Id);

		// Insert Opportunity
		Opportunity oOpportunity = new Opportunity();
			oOpportunity.RecordTypeId = clsUtil.getRecordTypeByDevName('Opportunity', 'Service_Contract_Opportunity').Id;
			oOpportunity.AccountId = acc1.Id;
			oOpportunity.Name = 'Test Opp 1';
			oOpportunity.Type = 'Service Agreement';
			oOpportunity.CloseDate = Date.today().addDays(30);
			oOpportunity.StageName = 'Prospecting/Lead';
			oOpportunity.Deal_Category__c = 'New Customer';
			oOpportunity.CampaignId = oCampaign.Id;
		insert oOpportunity;
        testData.put('opportunity', oOpportunity.Id);
		

        //Insert Company
        Company__c cmpny = new Company__c();
        cmpny.name='TestMedtronic';
        cmpny.CurrencyIsoCode = 'EUR';
        cmpny.Current_day_in_Q1__c=56;
        cmpny.Current_day_in_Q2__c=34; 
        cmpny.Current_day_in_Q3__c=5; 
        cmpny.Current_day_in_Q4__c= 0;   
        cmpny.Current_day_in_year__c =200;
        cmpny.Days_in_Q1__c=56;  
        cmpny.Days_in_Q2__c=34;
        cmpny.Days_in_Q3__c=13;
        cmpny.Days_in_Q4__c=22;
        cmpny.Days_in_year__c =250;
        cmpny.Company_Code_Text__c = 'T27';
        insert cmpny;
        testData.put('company',  cmpny.Id);
        
        Call_Activity_Type__c subj1 = new Call_Activity_Type__c();
        subj1.name = 'test subject';
        subj1.Company_ID__c = cmpny.id;
        insert subj1 ; 
        testData.put('activityType', subj1.Id);
        
        List<Product2> prds = new List<Product2>();
        
        Product2 prd1 = new Product2();
        prd1.Name = 'test Product';
        prds.add(prd1);
        testData.put('product1', prd1.Id);
        
        Product2 prd2 = new Product2();
        prd2.Name = 'test Product222';
        prds.add(prd2);
        testData.put('product2', prd2.Id);
        
        insert prds;
        
        List<Subject__c> subs= new List<Subject__c>();
        
        Subject__c sub1 = new Subject__c();
        sub1.Name = 'test subbb';
        sub1.Company_ID__c = cmpny.id;
        subs.add(sub1);
        testData.put('subject1', sub1.Id);
        
        Subject__c sub2 = new Subject__c ();
        sub2.Name = 'test subbb2';
        sub2.Company_ID__c = cmpny.id;
        subs.add(sub2);
        testData.put('subject2', sub2.Id);
        
        insert subs;
        
        Business_Unit__c BU1 = new Business_Unit__c();
        BU1.Name='Test2 Bu Name';
        BU1.Company__c = cmpny.Id;
        insert BU1;
        testData.put('businessUnit', BU1.Id);
        
        Sub_Business_Units__c SBU = new Sub_Business_Units__c();
        SBU.Name='Test Bu Name';
        SBU.Business_Unit__c = BU1.Id;
        insert SBU;
        testData.put('subBusinessUnit', SBU.Id);         
                
        Therapy_Group__c TG=new Therapy_Group__c();
        TG.Name='Test Therapy Group';
        TG.Sub_Business_Unit__c = SBU.id;
        TG.Company__c = cmpny.Id;
        insert TG;    
        testData.put('therapyGroup', TG.Id);
         
        Therapy__c th = new Therapy__c();
        th.Name = 'th11';
        th.Therapy_Group__c = TG.Id;
        th.Sub_Business_Unit__c = SBU.id;
        th.Business_Unit__c = BU1.id;
        th.Therapy_Name_Hidden__c = 'th11'; 
        insert th;
        testData.put('therapy', th.Id);
         
        Call_Category__c catt= new  Call_Category__c();
        catt.Name = 'cat111';
        catt.Company_ID__c = cmpny.id;
        insert catt;
        testData.put('category', catt.Id); 
        
        Schema.DescribeSObjectResult d = Schema.SObjectType.Call_Records__c;
        Map<string,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
        Schema.RecordTypeInfo recType=rtMapByName.get('xBU');
        Schema.RecordTypeInfo recType1=rtMapByName.get('Business Unit');
        testData.put('buRecordType', recType1.getrecordtypeid() );
        testData.put('xbuRecordType', recType.getrecordtypeid() );
        
        Id recTypeid=recType.getrecordtypeid();
        
        List<Call_Topic_Settings__c> lstcats = new List<Call_Topic_Settings__c>();
        
        Call_Topic_Settings__c cats=new Call_Topic_Settings__c();
        cats.Call_activity_type__c = subj1.Id;
        cats.Business_Unit__c = BU1.id;
        cats.Call_Category__c = catt.Id;
        cats.Call_Record_Record_Type__c=rectype1.getrecordtypeid();
        cats.Region_vs__c = 'Europe';
        lstcats.add(cats);
        
        Call_Topic_Settings__c cats1=new Call_Topic_Settings__c();
        cats1.Call_activity_type__c = subj1.Id;
        //cats1.Business_Unit__c = BU1.id;
        cats1.Call_Category__c = catt.Id;
        cats1.Call_Record_Record_Type__c=rectype.getrecordtypeid();
        cats1.Region_vs__c = 'Europe';
        lstcats.add(cats1);
        
        insert lstcats;
        
        Call_topic_Setting_Details__c ctsd1=new Call_topic_Setting_Details__c(Call_Topic_Setting__c=cats.id,Subject__c=subs[0].id);
        insert ctsd1;
        
        return testData;
    }
 /*     
   static void unitTest(){
        
        
           controller.RedirectToDifferntPage();
            controller.getBUs();
           
                                    
            String currPage = '/visitReportPage?';
            currPage += '&id='+  vr1.Id ;
            currPage += '&conId'+ cont1.Id ;
            currPage +='&rt='+recTypeid;
            currPage +='&RecordType='+recTypeid;
            //currPage += '&eventId'+ evt1.Id ; 
            
            PageReference pageRef = new PageReference(currPage);            
            Test.setCurrentPage(pageRef); 
                       
            ApexPages.Standardcontroller sct = new ApexPages.Standardcontroller(vr1);
            controllerVisitReport controller = new controllerVisitReport(sct);
            controller.RedirectToDifferntPage();
              
            controller.getAffiliatedAccountName('abc');
            // changes 
            controller.RedirectToDifferntPage();
                        
            

            
            
            controller.dataMap.put('eventId', evt1.Id);
            
            controller.getTask().Subject = 'Test task';
            controller.saveTask(vr1,af);
            
            controller.checkPresentSubjectSAM('abc');
            controller.saveSubjectsVisitReports(vr1);
            
            
            controller.checkPresentSubject('abc');
            
            
            
            
            controller.saveAndNew();
            controller.saveAndNewImplant();
                                    
            controller.setaccountsToList('mno');
            //controller.addTopic();
            
           // controller.drawRelatedItems(vr1.Id);
            // add a Contact without value :    
            controller.cvr1.Attending_contact__c = null ;
            controller.vr.Call_Date__c = System.Today()-1;
            controller.addAttendingContact();
            
            //Ended by navneet
            
            // add a Contact with a preselected Contact :   
            controller.cvr1.Attending_contact__c = cont1.Id ;
            controller.addAttendingContact();        

            controller.vr.Call_Date__c =System.Today()-1;
            
            controller.getaccountsToList() ; 
            //controller.getSubjectItems();
            //controller.getsubjectsDicussedToDeleteIds();
            controller.getcontactsAttendedToDeleteIds();
            if (sb1 != null){
                Id[] sids = new Id[]{};
                sids.add(sb1.Id);
                //controller.setsubjectsDicussedToDeleteIds(sids);
                //controller.setselectedSubjectitem(sb1.Id);
            }
            if (cd1 != null){
                Id[] sids = new Id[]{};
                sids.add(cd1.Id);
                controller.setcontactsAttendedToDeleteIds(sids);
            }
            
            controller.getTherapies();
            //  controller.getSubjects();
            // controller.getActivities();
            controller.getProducts();
           
            //controller.getpickedSubject();
            //controller.setpickedSubject(subj1.Id);
            //controller.getSubjects();
            //controller.addSubjectDiscussed();
            // controller.removeSubjectDiscussed();
    
            //controller.getAffiliatedAccountName(acc1.Id);
            //controller.getAffiliatedAccountName('test');
            //controller.getitems();
            controller.getselFromAccountNames();
            controller.checkPresent(acc1.Id) ;
            //controller.checkPresent(cont1.Id) ;
            controller.getcvr1();
            controller.getvr();
            //controller.refreshAffiliatedAccounts();
            controller.cvr1.Attending_Affiliated_Account__c = acc1.id;
            controller.refreshFromAccountNames();
            controller.removeAttendingContact();
            controller.getProduct();
            controller.getSubject();
            controller.resetCallAccCon();
            controller.isNewMode=true;
            controller.changeisNewMode();
            controller.getisNewMode();
            controller.getisUpdatemode();
            //controller.isUpdatemode=true;
            controller.gettask();
            
            controller.getcansendtask();
            controller.getCitems();
            controller.getCallTopicItems();  
            controller.setcontactToList('hi');
            controller.getAffFromContactNames();  
            controller.setisNewMode(true);
            //Account selected :
            //controller.setaccountsToList(acc1.Id);
           
            controller.cvr1.Attending_contact__c = cont1.Id ; 
            controller.addAttendingContact();
            
            controller.setvr(vr1) ;         
            controller.listaccounts();
            
            
            //controller.deleteSubjectsDiscussed(sbs);
            //controller.deleteContactsAttended(conts);
            //Save  
            controller.save();
            controller.saveContactVisitReports(vr1);
            
            //controller.items.clear();
            //controller.save();
            controller.saveAndNew();
            controller.saveAndNewImplant();
            //controller.deleteSubjectsDiscussed();
            controller.doaddreminder();
            controller.sendEmail();
          
            controller.getIsNotCRDM();
          
            controller.cancel();
            controller.dummyaction();
            
            controller.isUpdateMode = true;
            ApexPages.currentPage().getParameters().put('id', vr1.Id);
            //controller.InitVR();          
            System.debug(' ################## ' + 'END TEST_controllerVisitReport' + ' ################');
        }        
    } 
    */   
}