public with sharing class bl_Run_All_Internal_Tests {
			
	@future(callout=true)
    public static void runAllInternalTests(){
    	
    	deleteCoverageData();
    	
        String sessionId = UserInfo.getSessionId();
        
        HttpRequest req = new HttpRequest();    	    	
		req.setEndpoint(URL.getSalesforceBaseUrl().toExternalForm()+'/services/data/v44.0/tooling/runTestsAsynchronous');
		req.setMethod('POST');
		req.setHeader('Authorization', 'Bearer ' + sessionId);
		req.setHeader('Content-Type', 'application/json');
		req.setTimeout(120000);
				
		req.setBody('{"classNames": null,"classids": null,"suiteNames": null,"suiteids":null,"maxFailedTests": -1,"testLevel": "RunLocalTests","skipCodeCoverage": false}');
		 		    
	    Http http = new Http();
	    HttpResponse res = http.send(req);
	    	     	
	    if(res.getStatusCode() < 200 || res.getStatusCode() >= 300) throw new ws_Exception('Error during Rest call: ' + res.getStatus() + ' ('+ res.getStatusCode() + '). Response received : ' + res.getBody());	             	
	    	     	     	     	
	    //If we receive a response successully, we collect the Records returned and we create/update them locally
	    String asyncJobId = res.getBody();   
	    
	    System.debug(asyncJobId);    	
    }
    
    public static void deleteCoverageData(){
    	  	
    	String query = 'SELECT Id FROM ApexCodeCoverageAggregate';
        
        HttpRequest req = new HttpRequest();    	    	
		req.setEndpoint(URL.getSalesforceBaseUrl().toExternalForm()+'/services/data/v45.0/tooling/query/?q=' + EncodingUtil.URLEncode(query, 'UTF-8'));
		req.setMethod('GET');
		req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionId());
		req.setHeader('Content-Type', 'application/json');
		req.setTimeout(120000);
				    
	    Http http = new Http();
	    HttpResponse res = http.send(req);
	    	     	
	    if(res.getStatusCode() < 200 || res.getStatusCode() >= 300) throw new ws_Exception('Error during Rest call: ' + res.getStatus() + ' ('+ res.getStatusCode() + '). Response received : ' + res.getBody());	             	
	    	     	     	     	
	    QueryResponse response = (QueryResponse) JSON.deserialize(res.getBody(), QueryResponse.class);
	   	
	   	if(response.records.size() == 0) return;
	   	
	   	List<List<String>> allCoverageIds = new List<List<String>>();
	   	
	   	List<String> batchCoverageIds = new List<String>();
	   	 
	   	for(ApexCodeCoverageAggregate coverage : response.records){
	   		
	   		batchCoverageIds.add(coverage.Id);
	   		
	   		if(batchCoverageIds.size() == 200){
	   			
	   			allCoverageIds.add(batchCoverageIds);
	   			batchCoverageIds = new List<String>();
	   		}
	   	}      
	   	
	   	if(batchCoverageIds.size() > 0){
	   			
	   		allCoverageIds.add(batchCoverageIds);
	   	}
	   	
	   	for(List<String> coverageIds : allCoverageIds){
	   		
	   		deleteBatch(coverageIds);
	   	}	    
    }
    
    private static void deleteBatch(List<String> coverageIds){
    	
    	HttpRequest req = new HttpRequest();
                
        req.setHeader('Content-Type','text/xml;charset=UTF-8');        
        req.setHeader('Accept-Encoding','gzip,deflate');
		req.setHeader('Content-Type','text/xml;charset=UTF-8');
		req.setHeader('SOAPAction','""');
		req.setHeader('User-Agent','Jakarta Commons-HttpClient/3.1');
		        
        req.setEndpoint(URL.getSalesforceBaseUrl().toExternalForm()+'/services/Soap/T/45.0');
        req.setMethod('POST');
        req.setTimeout(120000);
        
        String envOpn =	'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:tooling.soap.sforce.com">'; 
  		
  		String header =	'<soapenv:Header><urn:SessionHeader><urn:sessionId>' + UserInfo.getSessionId() + '</urn:sessionId></urn:SessionHeader></soapenv:Header>\n';
	  						 
		String body = 	'<soapenv:Body>\n' +
							'<urn:delete>\n';
       	
       	for(String coverageId : coverageIds){ 	
       	
       		body +=	'<urn:ids>'+ coverageId +'</urn:ids>\n';
       	}
       		
       	body += '</urn:delete>\n' + 
       			'</soapenv:Body>\n';
       						
       	String envCls = '</soapenv:Envelope>\n';
        
        String fullMessage = envOpn + header + body + envCls;
        
        req.setBody(fullMessage);
        
        System.debug('req '+ fullMessage);
        		
		req.setHeader('Content-Length',String.valueOf(fullMessage.length()));
		
		Http http = new Http();        
        HttpResponse res = http.send(req);
        
        if(res.getStatusCode() < 200 || res.getStatusCode() >= 300) throw new ws_Exception('Error during SOAP call: ' + res.getStatusCode() + '. ' +res.getBody());	    
        
        System.debug(res.getBody());
    }
    
    public class QueryResponse{
    	
		public List<ApexCodeCoverageAggregate> records {get;set;}		
	}
	
	public class ApexCodeCoverageAggregate{
		
		public String id {get; set;}		
	}       
}