@isTest
private class Test_ctrl_RelatedListExport {

	private static User oUser_System;
	private static User oUser;

	@isTest static void createTestData(){

		// Create Users
        oUser_System = clsTestData_User.createUser_SystemAdministratorMDT('tadm01', true);
        oUser = clsTestData_User.createUser('test.user.001@medtronic.com.test', 'tu001', 'NETHERLANDS', clsUtil.getUserProfileId('EUR Field Force RTG'), clsUtil.getUserRoleId('EUR xBU'), false);
        	oUser.User_Business_Unit_vs__c = 'Neurovascular';
        insert oUser;

        //---------------------------------------
        // Create Test Data
        //---------------------------------------
  		System.runAs(oUser_System){

	        clsTestData_MasterData.tCompanyCode = 'EUR';
	        clsTestData_MasterData.createCompany();
	        clsTestData_MasterData.tBusinessUnitGroupName = 'Restorative';
	        clsTestData_MasterData.createBusinessUnitGroup();
	        clsTestData_MasterData.tBusinessUnitName = 'Neurovascular';
	        clsTestData_MasterData.createBusinessUnit();
	        clsTestData_MasterData.tSubBusinessUnitName = 'Neurovascular';
	        clsTestData_MasterData.createSubBusinessUnit();
	        clsTestData_MasterData.id_User = oUser.ID;
	        clsTestData_MasterData.createUserBusinessUnit();
			clsTestData_Asset.iRecord_Asset = 10;
			clsTestData_Asset.createAsset();
	        clsTestData_MasterData.createBURelatedListSetupData_Asset();

	    }
        //---------------------------------------

	}
	
	
	@isTest static void test_ctrl_RelatedListExport_Error() {

		//------------------------------------------
		// Create/Load Test Data
		//------------------------------------------
		// Create Test Data
		createTestData();

		// Create Encrypted URL Parameter
		String tSOQL = 'SELECT Id, Name, AccountId, Contact.Name, Asset_Type_Picklist__c, Quantity, Serial_Nr__c, IsCompetitorProduct, Status, InstallDate, Product_Group__c, Product2.Business_Unit_ID__r.Name, Product2.Product_Group__r.Therapy_ID__r.Sub_Business_Unit__r.Name FROM Asset WHERE Id != null ORDER BY Name';
        String tParameter = '';
	        tParameter += 'buid=' + EncodingUtil.base64Encode(Blob.valueOf(clsTestData_MasterData.oMain_BusinessUnit.Id));
	        tParameter += '&oname=' + EncodingUtil.base64Encode(Blob.valueOf('Asset'));
	        tParameter += '&exporttype=' + EncodingUtil.base64Encode(Blob.valueOf('XLS'));
        tParameter = EncodingUtil.base64Encode(Blob.valueOf(tParameter));
		//------------------------------------------

		//------------------------------------------
		// Test
		//------------------------------------------
		Test.startTest();

    	System.runAs(oUser){
	
			PageReference oPR_RelatedListExport = Page.RelatedListExport;
	        Test.setCurrentPage(oPR_RelatedListExport);
	        ctrl_RelatedListExport oCTRL_RelatedListExport = new ctrl_RelatedListExport();

	        // Add parameters to page URL - Missing SOQL parameter
	        ApexPages.currentPage().getParameters().put('p', tParameter);
	        oCTRL_RelatedListExport = new ctrl_RelatedListExport();

	        // Cover Unexpected Exception Handling
	        clsUtil.hasException = true;
	        oCTRL_RelatedListExport.loadData();
	        oCTRL_RelatedListExport.bInitializePage('');

	    }

		Test.stopTest();
		//------------------------------------------

		//------------------------------------------
		// Validation
		//------------------------------------------
		//------------------------------------------

	}

	@isTest static void test_ctrl_RelatedListExport() {

		//------------------------------------------
		// Create/Load Test Data
		//------------------------------------------
		// Create Test Data
		createTestData();

		List<String> lstContentType = new List<String>();
			lstContentType.add('XLS');
			lstContentType.add('XLSX');
			lstContentType.add('CSV');
			lstContentType.add('PDF');
			lstContentType.add('IDONTEXIST');
		//------------------------------------------

		//------------------------------------------
		// Test
		//------------------------------------------
		Test.startTest();

    	System.runAs(oUser){

			for (String tContentType : lstContentType){
				Boolean bResult = bRelatedListExport(tContentType);
				System.assert(bResult); // Should be True
			}

		}

		Test.stopTest();
		//------------------------------------------

		//------------------------------------------
		// Validation
		//------------------------------------------
		//------------------------------------------

	}	

	private static Boolean bRelatedListExport(String tContentType) {

		Boolean bResult = true;

		try{
			//------------------------------------------
			// Create Encrypted URL Parameter
			//------------------------------------------
			String tSOQL = 'SELECT Id, Name, AccountId, Contact.Name, Asset_Type_Picklist__c, Quantity, Serial_Nr__c, IsCompetitorProduct, Status, InstallDate, Product_Group__c, Product2.Business_Unit_ID__r.Name, Product2.Product_Group__r.Therapy_ID__r.Sub_Business_Unit__r.Name FROM Asset WHERE Id != null ORDER BY Name';
	        String tParameter = '';
		        tParameter += 'soql=' + EncodingUtil.base64Encode(Blob.valueOf(tSOQL));
		        tParameter += '&buid=' + EncodingUtil.base64Encode(Blob.valueOf(clsTestData_MasterData.oMain_BusinessUnit.Id));
		        tParameter += '&oname=' + EncodingUtil.base64Encode(Blob.valueOf('Asset'));
		        tParameter += '&exporttype=' + EncodingUtil.base64Encode(Blob.valueOf(tContentType));
	        tParameter = EncodingUtil.base64Encode(Blob.valueOf(tParameter));
			//------------------------------------------

			//------------------------------------------
			// Test
			//------------------------------------------
	    	System.runAs(oUser){
	
				PageReference oPR_RelatedListExport = Page.RelatedListExport;
		        Test.setCurrentPage(oPR_RelatedListExport);
		        // Add parameters to page URL
		        ApexPages.currentPage().getParameters().put('p', tParameter);
		        // Initiate a new controller with all parameters needed in the page
		        ctrl_RelatedListExport oCTRL_RelatedListExport = new ctrl_RelatedListExport();

		        List<SObject> lstData = oCTRL_RelatedListExport.getRecords();
		        String tContentType_Tmp = oCTRL_RelatedListExport.tContentType;
		        List<String> lstFieldName = oCTRL_RelatedListExport.lstFieldName;
		        Map<String, String> mapFieldLabel = oCTRL_RelatedListExport.mapFieldLabel;
		        String tRenderAs = oCTRL_RelatedListExport.tRenderAs;

		    }
			//------------------------------------------

		}catch(Exception oEX){
			bResult = false;
		}

		return bResult;

	}		
	
}