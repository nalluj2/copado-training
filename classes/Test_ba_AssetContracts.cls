//--------------------------------------------------------------------------------------------------------------------
//  Description : This TEST APEX Class is used to test the logic in ba_AssetContracts
//  Version     : 1.0
//  Author      : Bart Caelen
//  Date        : 07/09/2016
//--------------------------------------------------------------------------------------------------------------------
@isTest private class Test_ba_AssetContracts {
	
    static Integer iBatchSize = 10;

    @testSetup static void createTestData() {

    	//---------------------------------------------------------------------------
    	// Create Test Data
    	//---------------------------------------------------------------------------
    	// Create Master Data
		clsTestData_MasterData.tBusinessUnitName = 'Cranial Spinal';
		clsTestData_MasterData.createBusinessUnit();
    	// Create Account Data
    	clsTestData_Account.idRecordType_Account = clsUtil.getRecordTypeByDevName('Account', 'SAP_Account').Id;
    	clsTestData_Account.createAccount();

    	// Create Asset Data
		clsTestData_Asset.iRecord_Asset = iBatchSize;
		List<Asset> lstAsset = clsTestData_Asset.createAsset(true);

		Integer iCounter = 0;
		for (Asset oAsset : lstAsset){

			if (iCounter < 3){
				oAsset.Status = 'Purchased';
				oAsset.hasActiveContract__c = true;
			}else if (iCounter < 5){
				oAsset.Status = 'Removed';
				oAsset.hasActiveContract__c = true;
			}else if (iCounter < 8){
				oAsset.Status = 'Purchased';
				oAsset.hasActiveContract__c = true;
			}else{
				oAsset.Status = 'Removed';
				oAsset.hasActiveContract__c = true;
			}

		}
		update lstAsset;

    	// Create Contract Data
		clsTestData_Contract.iRecord_Contract = 2;
		clsTestData_Contract.idRecordType_Contract = clsUtil.getRecordTypeByDevName('Contract', 'RTG_Service_Contract').Id;
		List<Contract> lstContract = clsTestData_Contract.createContract(true);

			lstContract[0].Status = 'Activated';
			lstContract[1].Status = 'Activated';
		update lstContract;


    	// Create Asset_Contract Data
		List<AssetContract__c> lstAssetContract = new List<AssetContract__c>();
		iCounter = 0;
		for (Asset oAsset : lstAsset){

            AssetContract__c oAssetContract = new AssetContract__c();
                oAssetContract.Asset__c = oAsset.Id;
				if (iCounter < 5){
					oAssetContract.Contract__c = lstContract[0].Id;
				}else{
					oAssetContract.Contract__c = lstContract[1].Id;
				}
            lstAssetContract.add(oAssetContract);

			iCounter++;

		}
		insert lstAssetContract;
    	//---------------------------------------------------------------------------

    }

    @isTest static void test_ba_AssetContracts_Scheduling() {

        Test.startTest();

        //---------------------------------------
        // TEST SCHEDULING
        //---------------------------------------
        String tCRON_EXP = '0 0 0 3 9 ? 2099';
        
        String tJobId = System.schedule('ba_AssetContracts_TEST', tCRON_EXP, new ba_AssetContracts());

        // Get the information from the CronTrigger API object
        CronTrigger oCronTrigger = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :tJobId];

        // Verify the expressions are the same
        System.assertEquals(tCRON_EXP, oCronTrigger.CronExpression);

        // Verify the job has not run
        System.assertEquals(0, oCronTrigger.TimesTriggered);

        // Verify the next time the job will run
        System.assertEquals('2099-09-03 00:00:00', String.valueOf(oCronTrigger.NextFireTime));
        //---------------------------------------

        Test.stopTest();
    
    }
    

    @isTest static void test_ba_AssetContracts() {

		List<AssetContract__c> lstAssetContract = 
			[
				SELECT 
					Id, Contract_Start_Date__c, Contract_End_Date__c, Agreement_level__c, ContractTerm__c, Service_Level__c
					, Contract__c, Contract__r.Status, Contract__r.StartDate, Contract__r.EndDate
					, Asset__c, Asset__r.Contract_Start_Date__c, Asset__r.Contract_End_Date__c, Asset__r.Agreement_level_Picklist__c, Asset__r.Contract_duration__c, Asset__r.Service_Level__c, Asset__r.hasActiveContract__c
				FROM AssetContract__c
				ORDER BY Contract__c
			];


		for (AssetContract__c oAssetContract : lstAssetContract){

			System.assert(oAssetContract.Contract__r.Status == 'Activated');
			System.assert(oAssetContract.Contract__r.StartDate != null);
			System.assert(oAssetContract.Contract__r.EndDate != null);
			System.assert(oAssetContract.Asset__r.Contract_Start_Date__c != null);
			System.assert(oAssetContract.Asset__r.Contract_End_Date__c != null);
			System.assert(oAssetContract.Asset__r.Agreement_level_Picklist__c != null);
			System.assert(oAssetContract.Asset__r.Contract_duration__c != null);
			System.assert(oAssetContract.Asset__r.hasActiveContract__c == true);

		}

		List<Contract> lstContract = [SELECT Id, Status FROM Contract LIMIT 1];
		System.assertEquals(lstContract.size(), 1);
			
			lstContract[0].StartDate = Date.today().addDays(-60);
			lstContract[0].ContractTerm = 1;
			lstContract[0].Status = 'Canceled';
		update lstContract;						 


        Test.startTest();

        //---------------------------------------
        // TEST BATCH
        //---------------------------------------
    	ba_AssetContracts oBatch1 = new ba_AssetContracts();
			oBatch1.tProcessType = 'ACTIVE_OR_INACTIVE'; 
        Database.executebatch(oBatch1, iBatchSize);        	

    	ba_AssetContracts oBatch2 = new ba_AssetContracts();
			oBatch2.tProcessType = 'NOT_ACTIVE'; 
        Database.executebatch(oBatch2, iBatchSize);        	
        //---------------------------------------

        Test.stopTest();


		lstAssetContract = 
			[
				SELECT 
					Id, Contract_Start_Date__c, Contract_End_Date__c, Agreement_level__c, ContractTerm__c, Service_Level__c
					, Contract__c, Contract__r.Status, Contract__r.StartDate, Contract__r.EndDate
					, Asset__c, Asset__r.Contract_Start_Date__c, Asset__r.Contract_End_Date__c, Asset__r.Agreement_level_Picklist__c, Asset__r.Contract_duration__c, Asset__r.Service_Level__c, Asset__r.hasActiveContract__c
				FROM AssetContract__c
				ORDER BY Contract__c
			];


		for (AssetContract__c oAssetContract : lstAssetContract){

			if (oAssetContract.Contract__r.Status == 'Activated'){

				System.assert(oAssetContract.Contract__r.StartDate != null);
				System.assert(oAssetContract.Contract__r.EndDate != null);
				System.assert(oAssetContract.Asset__r.Contract_Start_Date__c != null);
				System.assert(oAssetContract.Asset__r.Contract_End_Date__c != null);
				System.assert(oAssetContract.Asset__r.Agreement_level_Picklist__c != null);
				System.assert(oAssetContract.Asset__r.Contract_duration__c != null);
				System.assert(oAssetContract.Asset__r.hasActiveContract__c == true);

			}else{
	
				System.assert(oAssetContract.Contract__r.Status == 'Canceled');
				System.assert(oAssetContract.Contract__r.StartDate != null);
				System.assert(oAssetContract.Contract__r.EndDate != null);
				System.assert(oAssetContract.Asset__r.Contract_Start_Date__c == null);
				System.assert(oAssetContract.Asset__r.Contract_End_Date__c == null);
				System.assert(oAssetContract.Asset__r.Agreement_level_Picklist__c == null);
				System.assert(oAssetContract.Asset__r.Contract_duration__c == null);
				System.assert(oAssetContract.Asset__r.Service_Level__c == null);
				System.assert(oAssetContract.Asset__r.hasActiveContract__c == false);
	
			}

		}


		delete lstContract;

    }   


}
//--------------------------------------------------------------------------------------------------------------------