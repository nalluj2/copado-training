@isTest
private class Test_ws_ReviewRecordService {
		
	private static testmethod void createDistrictReview(){
		
		Account a = new Account();
		a.Name = 'test account';
		insert a;
		
		RestRequest req = new RestRequest();  
    	RestResponse res = new RestResponse();
    	
    	String id=GuidUtil.NewGuid();

		Review_Record__c reviewRecord = new Review_Record__c();

		reviewRecord.Review_Type__c = 'District'; 
		reviewRecord.Review_Date__c = Date.today();
		reviewRecord.Comment__c = 'Test comment';
		reviewRecord.Front_Line_Manager__c = UserInfo.getUserId();
		reviewRecord.mobile_id__c = id;
		
    	JSONGenerator gen = JSON.createGenerator(true);
    	
		gen.writeStartObject();
        gen.writeStringField('id', '12346');
	    gen.writeObjectField('reviewRecord', reviewRecord);
	   
        // Get the JSON string.
        String pretty = gen.getAsString();	    	
		System.debug('JSON '+pretty);

        req.requestbody = Blob.valueOf(pretty);
    	
     	req.requestURI = '/services/apexrest/ReviewRecordService';  
	    req.httpMethod = 'POST';
    	RestContext.request = req;
    	RestContext.response = res;
		
		ws_ReviewRecordService.IFReviewRecordResponse resp = ws_ReviewRecordService.doPost();
		System.debug('Response'+resp);
		
		List<Review_Record__c> dbReviewRecord = [Select Id, Name, Status__c, Mobile_Id__c from Review_Record__c where Mobile_Id__c = :id];
		
		System.assert(dbReviewRecord.size() == 1);		
	}
	
	private static testmethod void createDistrictReviewError(){
		
		RestRequest req = new RestRequest();  
    	RestResponse res = new RestResponse();
    	
    	String id=GuidUtil.NewGuid();

		Review_Record__c reviewRecord = new Review_Record__c();

		reviewRecord.Review_Type__c = 'District'; 
		reviewRecord.Review_Date__c = Date.today();
		reviewRecord.Comment__c = 'Test comment';		
		reviewRecord.mobile_id__c = id;
		
    	JSONGenerator gen = JSON.createGenerator(true);
    	
		gen.writeStartObject();
        gen.writeStringField('id', '12346');
	    gen.writeObjectField('reviewRecord', reviewRecord);
	   
        // Get the JSON string.
        String pretty = gen.getAsString();	    	
		System.debug('JSON '+pretty);

        req.requestbody = Blob.valueOf(pretty);
    	
     	req.requestURI = '/services/apexrest/ReviewRecordService';  
	    req.httpMethod = 'POST';
    	RestContext.request = req;
    	RestContext.response = res;
		
		ws_ReviewRecordService.IFReviewRecordResponse resp = ws_ReviewRecordService.doPost();
		System.debug('Response'+resp);
						
		System.assert(resp.success == false);		
	}
	
	private static testmethod void deleteDistrictReview(){

    	String id=GuidUtil.NewGuid();

		Review_Record__c reviewRecord = new Review_Record__c();

		reviewRecord.Review_Type__c = 'District'; 
		reviewRecord.Review_Date__c = Date.today();
		reviewRecord.Comment__c = 'Test comment';
		reviewRecord.Front_Line_Manager__c = UserInfo.getUserId();
		reviewRecord.mobile_id__c = id;
		
		insert reviewRecord;
		
		RestRequest req = new RestRequest();  
    	RestResponse res = new RestResponse();		
		
    	JSONGenerator gen = JSON.createGenerator(true);
    	
		gen.writeStartObject();
        gen.writeStringField('id', '12346');
	    gen.writeObjectField('reviewRecord', reviewRecord);
	   
        // Get the JSON string.
        String pretty = gen.getAsString();	    	
		System.debug('JSON '+pretty);

        req.requestbody = Blob.valueOf(pretty);
    	
     	req.requestURI = '/services/apexrest/ReviewRecordService';  
	    req.httpMethod = 'DELETE';
    	RestContext.request = req;
    	RestContext.response = res;
		
		ws_ReviewRecordService.IFReviewRecordDeleteResponse resp = ws_ReviewRecordService.doDelete();
		System.debug('Response'+resp);
		
		List<Review_Record__c> dbReviewRecord = [Select Id, Name, Status__c, Mobile_Id__c from Review_Record__c where Mobile_Id__c = :id];
		
		System.assert(dbReviewRecord.size() == 0);		
	}
	
	private static testmethod void deleteDistrictReviewError(){

    	String id=GuidUtil.NewGuid();

		Review_Record__c reviewRecord = new Review_Record__c();

		reviewRecord.Review_Type__c = 'District'; 
		reviewRecord.Review_Date__c = Date.today();
		reviewRecord.Comment__c = 'Test comment';
		reviewRecord.Front_Line_Manager__c = UserInfo.getUserId();
		reviewRecord.mobile_id__c = id;
		
		insert reviewRecord;
		
		RestRequest req = new RestRequest();  
    	RestResponse res = new RestResponse();		
		
		//Put invalid mobile Id
		reviewRecord.mobile_id__c = '123456';
		
    	JSONGenerator gen = JSON.createGenerator(true);
    	
		gen.writeStartObject();
        gen.writeStringField('id', '12346');
	    gen.writeObjectField('reviewRecord', reviewRecord);
	   
        // Get the JSON string.
        String pretty = gen.getAsString();	    	
		System.debug('JSON '+pretty);

        req.requestbody = Blob.valueOf(pretty);
    	
     	req.requestURI = '/services/apexrest/ReviewRecordService';  
	    req.httpMethod = 'DELETE';
    	RestContext.request = req;
    	RestContext.response = res;
		
		ws_ReviewRecordService.IFReviewRecordDeleteResponse resp = ws_ReviewRecordService.doDelete();
		System.debug('Response'+resp);
		
		System.assert(resp.message == 'Review Record not found');		
	}
}