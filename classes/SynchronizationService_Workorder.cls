public without sharing class SynchronizationService_Workorder implements SynchronizationService_Object{
	
	//Set with the external Ids of the records already processed in this transaction. Used to avoid generating multiple notifications for the same change
	private Set<String> processedRecords = new Set<String>();
	private Set<Id> rtInScope {
		
		get{	
			
			if(rtInScope == null){
				
				rtInScope = new Set<Id>();
				
				for(RecordType rt : [Select Id from RecordType where SObjectType = 'Workorder__c' and DeveloperName IN :recordTypeNames]) rtInScope.add(rt.Id);
			}
			
			return rtInScope;
		}
		
		set;
	}
	
	//This method returns the External Ids of the records that have relevant modifications for the target Org. This includes created records or 
	//modified records where synchronized fields have changed. 
	public Set<String> hasChangeForNotification(List<SObject> records, Map<Id, SObject> oldRecords){
		
		Set<String> result = new Set<String>();
				
		for(SObject rawObject : records){
			
			Workorder__c record = (Workorder__c) rawObject;
			
			if( !rtInScope.contains(record.RecordTypeId) || processedRecords.contains(record.External_Id__c) ) continue;
			
			//On Insert we always send notification
			if(oldRecords == null){
				result.add(record.External_Id__c);
				processedRecords.add(record.External_Id__c);
			}//If Update we only send notification when relevant fields are modified
			else{
				
				if( bl_SynchronizationService_Utils.hasChanged( record, oldRecords.get(record.Id), syncFields ) == true ){
					
					result.add(record.External_Id__c);
					processedRecords.add(record.External_Id__c);
				}
			}
		}
		
		return result;
	}
	
	//This method collects and returns the complete information for a record. This includes the records itself as well as any related record that needs
	//to exist in the target Org too. The returned format is JSON serialized string.
	public String generatePayload(String externalId){
		
		WorkorderSyncPayload payload = new WorkorderSyncPayload();
		
		String query = 	'SELECT ' + String.join( syncFields, ',') + 
						',Account__r.SAP_Id__c, Account__r.AccountNumber, Account__r.RecordType.DeveloperName, Asset__r.Serial_Nr__c, Related_System__r.Serial_Nr__c, Contact__r.External_Id__c, Case__r.External_Id__c, Complaint__r.External_Id__c' +
						',Destination_Account__r.SAP_Id__c, Destination_Account__r.AccountNumber, Destination_Account__r.RecordType.DeveloperName, Disposable_PN_1__r.CFN_Code_Text__c, Disposable_PN_2__r.CFN_Code_Text__c' +
						',Related_Workorder__r.External_Id__c, Software_1__r.External_Id__c, Software_2__r.External_Id__c, Software_3__r.External_Id__c' +
						',Software_4__r.External_Id__c, Software_5__r.External_Id__c, Software_6__r.External_Id__c, Software_7__r.External_Id__c, Software_8__r.External_Id__c' +
						',Surgeon_Trainee__r.External_Id__c, Biomed_Contact_Name__r.External_Id__c, T_M_Product_Part__r.CFN_Code_Text__c, RecordType.DeveloperName ' +
						',MRI_Scanner__r.External_Id__c ' +
						' FROM Workorder__c WHERE External_Id__c = :externalId LIMIT 2';
						
		List<Workorder__c> workorders = Database.query(query);
				
		//If no record or more than 1 is found, we return an empty response
		if(workorders.size() == 1){
			
			Workorder__c workorder = workorders[0];	
			bl_SynchronizationService_Utils.prepareForJSON(workorder, syncFields);		
			workorder.Id = null;
			
			payload.Workorder = workorder;
			
			Set<Id> accountIds = new Set<Id>();
			Set<Id> contactIds = new Set<Id>();
			
			//Account		
			if(workorder.Account__r != null){
				
				accountIds.add(workorder.Account__c);
				
				if ( (workorder.Account__r.RecordType.DeveloperName == 'SAP_Account') || (workorder.Account__r.RecordType.DeveloperName == 'Distributor') ){
					payload.AccId = workorder.Account__r.SAP_Id__c;
				}else{
					payload.AccId = workorder.Account__r.AccountNumber;							
				}
				workorder.Account__c = null;
				workorder.Account__r = null;				
			}
			
			//Destination Account		
			if(workorder.Destination_Account__r != null){
				
				accountIds.add(workorder.Destination_Account__c);
				
				if ( (workorder.Destination_Account__r.RecordType.DeveloperName == 'SAP_Account') || (workorder.Destination_Account__r.RecordType.DeveloperName == 'Distributor') ){
					payload.DestAccId = workorder.Destination_Account__r.SAP_Id__c;	
				}else{
					payload.DestAccId = workorder.Destination_Account__r.AccountNumber;
				}
				workorder.Destination_Account__r = null;
				workorder.Destination_Account__c = null;				
			}			
			
			//Contact		
			if(workorder.Contact__r != null){
				
				contactIds.add(workorder.Contact__c);
				
				payload.ContId = workorder.Contact__r.External_Id__c;							
				workorder.Contact__c = null;
				workorder.Contact__r = null;				
			}
			
			//Surgeon/Trainee		
			if(workorder.Surgeon_Trainee__r != null){
				
				contactIds.add(workorder.Surgeon_Trainee__c);
				
				payload.SurgeonId = workorder.Surgeon_Trainee__r.External_Id__c;						
				workorder.Surgeon_Trainee__r = null;
				workorder.Surgeon_Trainee__c = null;				
			}
			
			//Biomed Contact		
			if(workorder.Biomed_Contact_Name__r != null){
				
				contactIds.add(workorder.Biomed_Contact_Name__c);
				
				payload.BiomedContactId = workorder.Biomed_Contact_Name__r.External_Id__c;						
				workorder.Biomed_Contact_Name__r = null;
				workorder.Biomed_Contact_Name__c = null;				
			}
			
			//Asset
			if(workorder.Asset__r != null){
				
				payload.assetId = workorder.Asset__r.Serial_Nr__c;
				workorder.Asset__r = null;
				workorder.Asset__c = null;
			}	
			
			//Related Asset
			if(workorder.Related_System__r != null){
				
				payload.RelatedSysId = workorder.Related_System__r.Serial_Nr__c;
				workorder.Related_System__r = null;
				workorder.Related_System__c = null;
			}			
			
			//Case
			if(workorder.Case__r != null){
				
				payload.caseId = workorder.Case__r.External_Id__c;
				workorder.Case__r = null;
				workorder.Case__c = null;
			}
			
			//Complaint
			if(workorder.Complaint__r != null){
				
				payload.complaintId = workorder.Complaint__r.External_Id__C;
				workorder.Complaint__r = null;
				workorder.Complaint__c = null;
			}			
			
			//Disposable PN 1			
			if(workorder.Disposable_PN_1__r != null){
				
				payload.disp1Id = workorder.Disposable_PN_1__r.CFN_Code_Text__c;				
				workorder.Disposable_PN_1__r = null;
				workorder.Disposable_PN_1__c = null;	
			}
			
			//Disposable PN 2			
			if(workorder.Disposable_PN_2__r != null){
				
				payload.disp2Id = workorder.Disposable_PN_2__r.CFN_Code_Text__c;				
				workorder.Disposable_PN_2__r = null;
				workorder.Disposable_PN_2__c = null;	
			}
			
			//TM Product Part	
			if(workorder.T_M_Product_Part__r != null){
				
				payload.tmProdId = workorder.T_M_Product_Part__r.CFN_Code_Text__c;			
				workorder.T_M_Product_Part__r = null;
				workorder.T_M_Product_Part__c = null;	
			}
			
			//Related Work Order
			if(workorder.Related_Workorder__r != null){
				
				payload.relWorkorderId = workorder.Related_Workorder__r.External_Id__c;
				workorder.Related_Workorder__r = null;
				workorder.Related_Workorder__c = null;
			}
			
			//Software #1
			if(workorder.Software_1__r != null){
				
				payload.software1Id = workorder.Software_1__r.External_Id__c;
				workorder.Software_1__r = null;
				workorder.Software_1__c = null;
			}
			
			//Software #2
			if(workorder.Software_2__r != null){
				
				payload.software2Id = workorder.Software_2__r.External_Id__c;
				workorder.Software_2__r = null;
				workorder.Software_2__c = null;
			}
			
			//Software #3
			if(workorder.Software_3__r != null){
				
				payload.software3Id = workorder.Software_3__r.External_Id__c;
				workorder.Software_3__r = null;
				workorder.Software_3__c = null;
			}
			
			//Software #4
			if(workorder.Software_4__r != null){
				
				payload.software4Id = workorder.Software_4__r.External_Id__c;
				workorder.Software_4__r = null;
				workorder.Software_4__c = null;
			}
			
			//Software #5
			if(workorder.Software_5__r != null){
				
				payload.software5Id = workorder.Software_5__r.External_Id__c;
				workorder.Software_5__r = null;
				workorder.Software_5__c = null;
			}
			
			//Software #6
			if(workorder.Software_6__r != null){
				
				payload.software6Id = workorder.Software_6__r.External_Id__c;
				workorder.Software_6__r = null;
				workorder.Software_6__c = null;
			}
			
			//Software #7
			if(workorder.Software_7__r != null){
				
				payload.software7Id = workorder.Software_7__r.External_Id__c;
				workorder.Software_7__r = null;
				workorder.Software_7__c = null;
			}
			
			//Software #8
			if(workorder.Software_8__r != null){
				
				payload.software8Id = workorder.Software_8__r.External_Id__c;
				workorder.Software_8__r = null;
				workorder.Software_8__c = null;
			}	

			//MRI Scanner
			if (workorder.MRI_Scanner__r != null){
				payload.mriScannerId = workorder.MRI_Scanner__r.External_Id__c;
				workorder.MRI_Scanner__r = null;
				workorder.MRI_Scanner__c = null;
			}
			
			//Record Type
			payload.RecordType = workorder.RecordType.DeveloperName;
			workorder.RecordTypeId = null;
			workorder.RecordType = null;	
			
			//Done in a separate query because we cannot clear the value from the CreatedDate field before sending it to the target
			Workorder__c workorderDetails = [Select Name, CreatedDate, CreatedBy.Name, LastModifiedDate, LastModifiedBy.Name from Workorder__c where External_Id__c = :externalId];
			
			payload.WoName = workorderDetails.Name;			
			payload.CreatedDate = workorderDetails.CreatedDate;			
			payload.CreatedBy = workorderDetails.CreatedBy.Name;					
			payload.LastModifiedDate = workorderDetails.LastModifiedDate;			
			payload.LastModifiedBy = workorderDetails.LastModifiedBy.Name;
			
			payload.generateAccountPayload(accountIds, contactIds);		
		}
		
		return JSON.serialize(payload);
	}
	
	//Implementation needed only in Target Org
	public String processPayload(String inputPayload){						
		return null;
	}
	
	//Model classes for the requests and responses. To be used by the JSON serialize/deserialize
	private class WorkorderSyncPayload extends SynchronizationService_Payload{
		
		public Workorder__c workorder {get; set;}
		public String woName {get; set;}
		public String accId {get; set;}
		public String destAccId {get; set;}
		public String contId {get; set;}
		public String surgeonId {get; set;}
		public String biomedContactId {get; set;}
		public String disp1Id {get; set;}
		public String disp2Id {get; set;}
		public String tmProdId {get; set;}
		public String assetId {get; set;}	
		public String relatedSysId {get; set;}
		public String caseId {get; set;}	
		public String complaintId {get; set;}
		public String relWorkorderId {get; set;}
		public String software1Id {get; set;}
		public String software2Id {get; set;}
		public String software3Id {get; set;}
		public String software4Id {get; set;}
		public String software5Id {get; set;}
		public String software6Id {get; set;}
		public String software7Id {get; set;}
		public String software8Id {get; set;}
		public String mriScannerId {get; set;}
		public String recordType {get; set;}
		public Datetime createdDate {get; set;}
		public String createdBy {get; set;}
		public Datetime lastModifiedDate {get; set;}
		public String lastModifiedBy {get; set;}	
	}	
	
	private static List<String> syncFields = new List<String>
	{
		'Accurate_Navigation_on_O_Arm_Image_P_F__c',
		'Activation_of_Additional_Features__c',
		'Advanced_Viewing__c',
		'Asset__c',
		'Asset_Checkout_Failure__c',
		'Asset_Checkout_P_F__c',		
		'Asset_Failure__c',
		'Asset_Inspection_Failure__c',
		'Asset_Inspection_P_F__c',
		'Asset_Inspection_Summary_of_Failure__c',
		'Asset_Install_Complete__c',
		'Asset_P_F__c',
		'Resolution_Notes__c',
		'Actual_Age__c',
		'Additional_SCs__c',
		'Additional_Service_Charges__c',
		'App_testing_Failure__c',
		'Application_Testing_P_F__c',
		'App_Summary_of_Testing_Failure__c',
		'Test_Reports_Attached__c',
		'Attachment_Last_Uploaded__c',
		'Backup_Data_Failure__c',
		'Backup_Data_P_F__c',
		'Backup_Performed__c',
		'Bio_Med_FSE_Visit__c',
		'Biomed_Contact_Name__c',
		'Biomed_Repaired__c',
		'Boost_CF1mm_Holes__c',
		'Boost_CF1mm_kV__c',
		'Boost_CF1mm_mA__c',
		'Boost_CF2mm_Holes__c',
		'Boost_CF2mm_kV__c',
		'Boost_CF2mm_mA__c',
		'Boost_CF3mm_Holes__c',
		'Boost_CF3mm_kV__c',
		'Boost_CF3mm_mA__c',
		'Boost_kV__c',
		'Boost_Lines_mm__c',
		'Boost_mA__c',
		'Cable_Grade__c',
		'Cable_Grade_Summary_of_Failure__c',
		'Case__c',
		'Checks_After_Shutdown_Failure__c',
		'Checks_After_Shutdown_P_F__c',
		'Chill_Water_Level_Failure__c',
		'Chill_Water_Level_P_F__c',
		'Collimated_3D__c',
		'Competitive_Implant_Used__c',
		'Competitive_Spine_Vendor__c',
		'Complaint__c',
		'Compliance_Testing_Failure__c',
		'Compliance_Testing_P_F__c',
		'Compliance_Testing_Summary_of_Failure__c',
		'Configure_Media_I_O_Settings__c',
		'Contact__c',
		'Covered_Under__c',
		'Data_Files_Cleanup_Failure__c',
		'Data_Files_Cleanup_P_F__c',
		'Date_Closed__c',
		'Date_Completed__c',
		'Date_Invoiced__c',
		'Decontamination_of_Product__c',
		'Default_Gateway__c',		
		'Demo_Eval_Failure__c',
		'Demo_Eval_General_System_Check__c',
		'Demo_Eval_Unit__c',
		'Destination_Account__c',
		'Disposable_PN_1__c',
		'Disposable_PN_2__c',
		'DNS_Server_1__c',
		'DNS_Server_2__c',
		'DNS_Server_3__c',
		'External_Id__c',
		'System_Perform_As_Intended__c',
		'Does_this_Result_in_a_Complaint__c',
		'Dosimeter_Calibration_Due_Date__c',
		'Dosimeter_Serial_Number__c',
		'Electrical_Inspection_Failure__c',
		'Electrical_Inspection_P_F__c',
		'Electrical_Insp_Summary_of_Failure__c',
		'EM_Checkout_Summary_of_Failure_s__c',
		'Enhanced_Cranial_3D__c',
		'Final_Dose_Value_for_Boost_Fluoro__c',
		'Final_Dose_Value_for_Standard_Fluoro__c',
		'Final_kV_Value_for_Boost_Fluoro__c',
		'Final_kV_Value_for_Standard_Fluoro__c',
		'Final_mA_Value_for_Boost_Fluoro__c',
		'Final_mA_Value_for_Standard_Fluoro__c',
		'Flow_Meter__c',
		'FPU_Selection__c',
		'FPU_Selection_Sub_Category__c',
		'General_Summary_of_Failure__c',
		'Hardware_Failure__c',
		'Hardware__c',
		'Hardware_Checkout_Failure__c',
		'Has_Attachment__c',
		'HD3D__c',
		'Hospital_Department__c',
		'IAS_System_Image_Version__c',
		'Image_Quality_and_Navigation_Failure__c',
		'Image_Quality_and_Navigation_P_F__c',
		'Image_Quality_Failure__c',
		'Image_Quality_P_F__c',
		'Image_Quality_Ck_Summary_of_Failure__c',
		'Imaging_Modalities_Failure__c',
		'Imaging_Modalities_P_F__c',
		'Imaging_Summary_of_Failure__c',
		'Initial_Dose_Value_for_Boost_Fluoro__c',
		'Initial_Dose_Value_for_Standard_Fluoro__c',
		'Initial_kV_Value_for_Boost_Fluoro__c',
		'Initial_kV_Value_for_Standard_Fluoro__c',
		'Initial_mA_Value_for_Boost_Fluoro__c',
		'Initial_mA_Value_for_Standard_Fluoro__c',
		'Instrument_Checkout__c',
		'Instrument_Checkout_Failure__c',
		'Instruments_Failure__c',
		'Instruments__c',
		'Integrated_System_Failure__c',
		'Integrated_System_P_F__c',
		'Invoice_Amount__c',
		'Invoiced__c',
		'IP_Address__c',		
		'Is_Cable_Grade_Failure_Resolved__c',
		'Is_Compliance_Testing_Failure_Resolved__c',
		'Is_Scanner_Config_Failure_Resolved__c',		
		'Is_Electrical_Insp_Failure_Resolved__c',
		'Is_EM_Checkout_Failure_Resolved__c',
		'Is_Executing_Reopen_only_once__c',
		'Is_Executing_Reopen_only_onceFollow__c',
		'Is_Executing_Reopen_only_onceFollow2__c',
		'Is_Executing_Reopen_use_only__c',
		'Is_Executing_Workflow_Internal_once_only__c',
		'Is_Executing_Workflow_Internal_use_only__c',
		'Is_Failure_Resolved_App_Testing__c',
		'Is_Failure_Resolved_on_Demo_Equip__c',
		'Is_General_Failure_Resolved__c',
		'Is_hardware_Checkout_Failure_Resolved__c',		
		'Is_Image_Quality_Check_Failure_Resolved__c',
		'Is_Imaging_Failure_Resolved__c',
		'Is_Mechanical_Insp_Failure_Resolved__c',
		'Is_Mechanical_Tests_Failure_Resolved__c',
		'Is_Nav_Checkout_Failure_Resolved__c',
		'Is_Nav_Interface_Failure_Resolved__c',
		'Is_Planned_Maintenance_Failure_Resolved__c',
		'Is_Software_Checkout_Failure_Resolved__c',
		'Is_System_Inspection_Failure_Resolved__c',
		'Is_visual_inspection_failure_resolved__c',
		'ISO_Wag__c',
		'Justification_Summary_Report__c',
		'Justification_Test_Report__c',
		'Low_Level_CF1mm_kV__c',
		'Low_Level_CF1mm_mA__c',
		'Low_Level_CF1mm_Holes__c',
		'Low_Level_CF2mm_kV__c',
		'Low_Level_CF2mm_mA__c',
		'Low_Level_CF2mm_Holes__c',
		'Low_Level_CF3mm_kV__c',
		'Low_Level_CF3mm_mA__c',
		'Low_Level_CF3mm_Holes__c',
		'Low_Level_kV__c',
		'Low_Level_Lines_mm__c',
		'Low_Level_mA__c',
		//'Maintenance_Failure__c',
		//'Maintenance_P_F__c',
		'MAC_Address__c',
		'Mechanical_Subsystem_Failure__c',
		'Mechanical_Subsystem_P_F__c',
		'Mechanical_Tests_Failure__c',
		'Mechanical_Tests_P_F__c',
		'Mechanical_Tests_Summary_of_Failure__c',
		'Motion_Failure__c',
		'Motion_P_F__c',
		'Multiple_Field_of_View__c',
		'MVS_Firmware_Actual__c',
		'Nav_Checkout_Summary_of_Failure_s__c',
		'Nav_Interface_Summary_of_Failure__c',
		'Navigation_Checkout__c',
		'Navigation_Checkout_Failure__c',
		'Navigation_Failure__c',
		'Navigation_P_F__c',		
		'Need_Rep__c',
		'Number_of_Software_Upgraded__c',
		'O_arm_Application_Software_Version__c',
		'O_Arm_Generator_Firmware_Actual__c',
		'O_Arm_image_transfers_to_Stealth__c',
		'O_Arm_Interface_with_Navigation__c',
		'Old_SW_Upgrade__c',
		'OR_Table_Comp_Install_Only_Failure__c',
		'OR_Table_Comp_Install_Only_P_F__c',
		'PCAnywhere_Configuration_Failure__c',
		'PCAnywhere_Configuration_P_F__c',
		'Person_Who_Completed_the_Field_Activity__c',
		'Planned_Maintenance_Calibrations_Failure__c',
		'Planned_Maintenance_Calibrations_P_F__c',
		'Planned_Maintenance_Summary_of_Failure__c',
		'Please_Contact_Tech_Services__c',
		'PO_Number__c',
		'Power_Control_Firmware_Actual__c',
		'Pressure_Meter__c',
		'Printer__c',
		'Procedure_Type__c',
		'Reason_for_Checkout__c',
		'Record_Form_Number_from_Form_2579__c',
		'RecordTypeId',
		'Related_Workorder__c',
		'Remote_Access_Failure__c',
		'Remote_Access_P_F__c',
		'Remote_Presence_Enabled__c',
		'Remote_Presence_Notes__c',
		'Replace_S7_Network_Bulkhead_Connector__c',
		'Replace_S7_Power_Cable__c',
		'Replace_S8_Ethernet_Bulkhead_Connector__c',
		'Resolution_of_Visual_Inspection_Failure__c',
		'Revisit__c',
		'RTI_Dosimeter_Ocean_SW_Version__c',
		'Scanner_Configuration_Failure__c',
		'Scanner_Configuration_P_F__c',
		'Scanner_Configuration_S__c',
		'Scans_Requested__c',
		'Scheduled_End_Date_Time__c',
		'Scheduled_Start_Date_Time__c',
		'Setup_DICOM_Network_Transfer__c',
		'SNAP__c',
		'Software__c',
		'Software_1__c',
		'Software_1_P_F__c',
		'Software_2__c',
		'Software_2_P_F__c',
		'Software_3__c',
		'Software_3_P_F__c',
		'Software_4__c',
		'Software_4_P_F__c',
		'Software_5__c',
		'Software_5_P_F__c',
		'Software_6__c',
		'Software_6_P_F__c',
		'Software_7__c',
		'Software_7_P_F__c',
		'Software_8__c',
		'Software_8_P_F__c',
		'Software_Checkout_Failure__c',
		'Software_Checkout_P_F__c',
		'Software_Checkout_Summary_Failure_s__c',
		'Software_Configuration__c',
		'Software_Failure__c',		
		'Software_Upgrade_Value__c',
		'Software_Ver__c',
		'Standard_CF1mm_Holes__c',
		'Standard_CF1mm_kV__c',
		'Standard_CF1mm_mA__c',
		'Standard_CF2mm_Holes__c',
		'Standard_CF2mm_kV__c',
		'Standard_CF2mm_mA__c',
		'Standard_CF3mm_Holes__c',
		'Standard_CF3mm_kV__c',
		'Standard_CF3mm_mA__c',
		'Standard_kV__c',
		'Standard_Lines_mm__c',
		'Standard_mA__c',
		'StarShield__c',
		'StarShield_V2_V3_Failure__c',
		'StarShield_V2_V3_P_F__c',
		'Status__c',
		'StealthLink__c',
		'Stereotaxy__c',		
		'Submit_Form_to_Hospital_and_RSO__c',
		'Subnet_Mask__c',
		'Summary_Report_Cust_and_Attached__c',
		'Surgeon_Trainee__c',
		'Surgery_Start_Time__c',
		'SW_5_FCA_Value__c',
		'SW_6_FCA_Value__c',
		'SW_7_FCA_Value__c',
		'SW_8_FCA_Value__c',
		'T_M_Labor__c',
		'T_M_Parts__c',
		'T_M_Product_Part__c',
		'Tag_the_System_Out_of_Service__c',
		'TA_Calibration_Date__c',
		'Temperature_Control_Subsystem_Failure__c',
		'Temperature_Control_Subsystem_P_F__c',
		'Temperature_Meter__c',
		'Test_Finalization_Failure__c',
		'Test_Finalization_P_F__c',
		'TM_Calibration_Date__c',
		'Torque_Analyzer__c',
		'Training_Subject__c',
		'Travel_Time__c',
		'Troubleshooting_Visit__c',
		'Type_of_Visit__c',
		'Visually_inspected_parts_prior_to_instal__c',
		'Was_Competitive_Implant_Used__c',
		'Was_install_form_sent_to_BioTex__c',
		'Was_MSB_Product_Used__c',
		'Wave_Generator__c',
		'Were_hardware_parts_replaced__c',
		'WG_Calibration_Date__c',
		'Working_Hours__c',
		'Work_Order_Summary_Delivered__c',
		'Work_Order_Summary_with_Value_Delivered__c',
		'Xray_Control_Firmware_Actual__c',

		'of_Ablations_Pullbacks__c',
		'of_Catheters_Placed__c',
		'Ablation_End__c',
		'Ablation_Start__c',
		'Anatomic_Region__c',
		'Biomed_clinical_engineering_check__c',
		'Case_Study_Requested__c',
		'Catheter_Placement__c',
		'Computer_Serial_Number__c',
		'Diagnosis__c',
		'Diagnosis_Detail__c',
		'Gel_Phantom_testing__c',
		'IP_communication__c',
		'Laser_Serial_Number__c',
		'Last_VCLAS_placed__c',
		'LDF_Tip_Length__c',
		'Leave_OR__c',
		'Max_Ablation_Power__c',
		'Max_Laser_Power__c',
		'MRI_Scanner__c',
		'MRI_Scans_End__c',
		'MRI_Scans_Start__c',
		'Pathology__c',
		'Patient_in_Room__c',
		'Planning_Complete__c',
		'Planning_Software__c',
		'Potential_MFG_warranty_claim__c',
		'Pre_op_Scan__c',
		'Pump_Serial_Number__c',
		'Reason_for_Cancellation__c',
		'Recurrent_Lesion_Radiation_Necrosis__c',
		'Registration_Imaging__c',
		'Skin_Incision__c',
		'Stereotactic_Device_Setup__c',
		'Target_Location__c',
		'Test_Dose_Power__c',
		'Visualase_IP_Address_Used__c',
		'VL_CS_en_route_or_onsite__c',

		'Camera_Cart_Serial_Number__c',
		'Self_Test__c',
		'Self_Test_Failure__c',
		'SIU__c',

		'Remote_Presence_Utilized__c',
		'Was_case_resolved_over_the_phone__c',
		'Was_root_cause_or_req_part_identified__c',

		'Laser_Test_Result_P_F__c',
		'Laser_Test_Summary_of_Failure_s__c',
		'Is_Laser_Tests_Failure_Resolved__c',
		
		'Clean_Inspect_MVS_IAS_Failure_Summary__c',
		'Cleaning_Inspecting_MVS_IAS_Failure__c',
		'Cleaning_Inspecting_MVS_IAS_P_F__c',
		'Is_Clean_Inspect_MVS_IAS_Fail_resolved__c',
		
		'IAS_Batteries_Failure__c',
		'IAS_Batteries_P_F__c',
		'IAS_Batteries_Summary_of_Failures__c',
		'Is_IAS_Batteries_Failure_resolved__c',
				
		'Mechanical_Test_Failure_s__c',
		'Mechanical_Test_P_F__c',
		'Mechanical_Test_Summary_of_Failure__c',
		'Is_Mechanical_Test_Failure_Resolved__c',
		
		'Replace_all_IAS_Batteries__c',
		'Replace_IAS_CMOS_battery__c',
		'Replace_MVS_Computer_CMOS_battery__c',
		'Replace_MVS_UPS_battery_pack__c',
			
		'Autoguide_Checkout_Failure__c',
		'Autoguide_Checkout_P_F__c',
		'Autoguide_Checkout_Summary_of_Failure_s__c',
		'Is_Autoguide_Checkout_Failure_Resolved__c',
		
		'Was_Stealth_Autoguide_involved__c',
		'Related_System__c',
		'AG_Firmware_Version__c',

		'Laser_Meter_Calibration_due_date__c',
		'Laser_Meter_Serial_Number__c',				
		'Galil_Positioner__c',
		'Is_MVS_IAS_Batteries_Failure_Resolved__c',
		'MVS_IAS_Batteries_Failure__c',
		'MVS_IAS_Batteries_P_F__c',
		'MVS_IAS_Batteries_Summary_of_Failure__c',
		'MVS_Board_Firmware__c',
		'Replace_IAS_Motion_Batteries__c',
		'Replace_IAS_Xray_Batteries__c',
		'Sedecal_Xray_Generator_Controller_firmw__c',
		'System_Board_Power_Controller_firmware__c',
		'System_Board_Xray_controller_firmware__c',

		'DVM_Calibration_Due_Date__c',
		'DVM_Model__c',
		'DVM_Serial_Number__c',
		'Force_Gauge_Calibration_Due_Date__c',
		'Force_Gauge_Model__c',
		'Force_Gauge_Serial_Number__c',

		'Mechanical_Fit_of_part_upon_install__c',

		'Date_Submitted__c',
		'Follow_Up_Required__c',
		'SAP_Order__c', 
		'T_M_Status__c',
		'Travel_Lodging_Car_Rental_Other_Expenses__c',
		
		'X2D_Long_Film__c',
		'Competitive_Balloon__c',
		'Was_Competitive_Balloon_Used__c',
		
		'Competitor__c',
		'Interbody_Competitor__c',
		'Mazor_Kit_Code__c',
		'Screw_Competitor__c',
		'Single_Position__c',
		'Whose_Implant__c',
		'Whose_Interbody_Implant_was_used__c',
		'Whose_Screw_Implant_was_used__c',
		'Workflow_Type__c'
	};
	
	private static List<String> recordTypeNames = new List<String>
	{
		'Eval_Nav_Checkout',
		'Eval_Nav_Checkout_Closed',
		'Nav_Checkout',
		'Nav_System_Checkout_Closed',
		'Nav_Installation',
		'Nav_System_Installation_Closed',
		'Nav_PM',
		'Nav_PM_Closed',
		'O_Arm_System_Checkout',
		'O_Arm_System_Checkout_Closed',
		'O_Arm_System_Installation',
		'O_Arm_System_Install_Closed',
		'O_Arm_PM',
		'O_Arm_System_PM_Closed',
		'O2_System_Checkout',
		'O2_System_Checkout_Closed',
		'O2_System_Installation',
		'O2_System_Installation_Closed',
		'O2_System_PM',
		'O2_System_PM_Closed',		
		'Other_Site_Visit',
		'Other_Site_Visit_Closed',
		'PoleStar_Checkout',
		'PoleStar_System_Checkout_Closed',
		'PoleStar_System_Install_Closed',
		'PoleStar_System_Installation',
		'PoleStar_PM',
		'PoleStar_System_PM_Closed',
		'Surgery_Coverage',
		'Surgery_Coverage_Closed',
		'Training',
		'Training_Closed',

		'Mazor_System_Checkout',
		'Mazor_System_Checkout_Closed',
		'Mazor_System_Installation',
		'Mazor_System_Installation_Closed',
		'Mazor_System_PM',
		'Mazor_System_PM_Closed',

		'Visualase_Surgery_Coverage',
		'Visualase_Surgery_Coverage_Closed',
		'Visualase_System_Checkout',
		'Visualase_System_Checkout_Closed',
		'Visualase_System_Installation',
		'Visualase_System_Installation_Closed',
		'Visualase_System_PM',
		'Visualase_System_PM_Closed',

		'Autoguide_System_Installation',
		'Autoguide_System_Installation_Closed',
		
		'Mazor_Surgery_Coverage',
		'Mazor_Surgery_Coverage_Closed'
	};
}