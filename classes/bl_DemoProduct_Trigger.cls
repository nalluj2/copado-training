//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 26-04-2016
//  Description      : APEX Class - Business Logic for tr_DemoProduct
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public with sharing class bl_DemoProduct_Trigger {

    //--------------------------------------------------------------------------------------------------------------------
    // Public variables coming from the trigger
    //--------------------------------------------------------------------------------------------------------------------
    public static List<Demo_Product__c> lstTriggerNew = new List<Demo_Product__c>();
    public static Map<Id, Demo_Product__c> mapTriggerNew = new Map<Id, Demo_Product__c>();
    public static Map<Id, Demo_Product__c> mapTriggerOld = new Map<Id, Demo_Product__c>();
    //--------------------------------------------------------------------------------------------------------------------


    //--------------------------------------------------------------------------------------------------------------------
    // Set the Current and Exact Location of a Demo Product when the Current Location is 'Hospital'
    //--------------------------------------------------------------------------------------------------------------------
    public static void setLocationOfDemoProduct(){

        Set<String> setID_DemoProduct = new Set<String>();
        Map<String, String> mapProductName_HospitalName = new Map<String, String>();
        Map<String, String> mapProductName_DeliveryAddress = new Map<String, String>();

        for (Demo_Product__c oDemoProduct : lstTriggerNew){
            if (oDemoProduct.Current_Location__c == 'Hospital'){
                setID_DemoProduct.add(oDemoProduct.Id);
            }
        }
        
        if (setID_DemoProduct.size() > 0){
        
            List<Movement_Request__c> lstMovementRequest = 
            	[
            		SELECT 
            			Id, Delivery_Address__c, Hospital_Name__c, Demo_Product_Name__c
        			FROM 
        				Movement_Request__c
	                WHERE 
	                	Demo_Product_Name__c in :setID_DemoProduct
	                	AND 
	                	(
	                		(
	                			Start_Date__c <= :System.today()
	                			AND End_Date__c >= :System.today()
	                			AND Approval_Status__c = 'Approved'
	                		) 
	                		OR
	                		(
	                			Start_Date_and_Time__c <= :System.now()
	                			AND End_Date_and_Time__c >= :System.now()
	                		)
	                	)
	                ORDER BY 
	                	Start_Date__c DESC
	              ];
            
            for (Movement_Request__c oMovementRequest : lstMovementRequest){
                mapProductName_HospitalName.put(oMovementRequest.Demo_Product_Name__c, oMovementRequest.Hospital_Name__c);
                mapProductName_DeliveryAddress.put(oMovementRequest.Demo_Product_Name__c, oMovementRequest.Delivery_Address__c);
            }
            
            for (Demo_Product__c oDemoProduct : lstTriggerNew){
                if (oDemoProduct.Current_Location__c == 'Hospital'){
                    oDemoProduct.Current_Location__c = mapProductName_HospitalName.get(oDemoProduct.Id);
                    oDemoProduct.Exact_Location__c = mapProductName_DeliveryAddress.get(oDemoProduct.Id);
                    System.debug('oDemoProduct.Exact_Location__c' + mapProductName_DeliveryAddress.get(oDemoProduct.Id));
                }
            }
        }

    }
    //--------------------------------------------------------------------------------------------------------------------

}