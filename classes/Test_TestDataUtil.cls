/*
 * Description		: Centralized util class to create Test Data
 * Author        	: Patrick Brinksma
 * Created Date    	: 15-07-2013
 */
@isTest
public class Test_TestDataUtil {
	
	// Account Type field value for Health Insurance
	public static final String HEALTH_INSURANCE_TYPE = 'Health Insurance';
	// Account Record Type Developer Name for Health Insurance Account
	public static final String HEALTH_INSURANCE_RECORD_TYPE = 'NON_SAP_Account';
	// Account Record Type Developer Name for Business Group Account
	public static final String BUYING_GROUP_RECORD_TYPE = 'CAN_Buying_Group';
	// Non SAP Record Type
	public static final String NON_SAP_RECORD_TYPE = 'NON_SAP_Account';

	
	/*
	 * Description		: Value based on unix time for naming data for testing
	 * Author			: Patrick Brinksma
	 * Created Date		: 15-07-2013
	 */	
	public static String createRandVal(){
		return String.valueof(System.now().getTime());
	}

	/*
	 * Description		: Return recordtype id of record type by DeveloperName and sObject Type
	 * Author			: Patrick Brinksma
	 * Created Date		: 15-07-2013
	 */		
	private static Id getRecordTypeId(String devName, String objType){
		return [select Id from RecordType where DeveloperName = :devName and sObjectType = :objType and isActive = true].Id;
	}

	/*
	 * Description		: Return profile Name to Id Map based on List of Profile Names
	 * Author			: Patrick Brinksma
	 * Created Date		: 16-07-2013
	 */		
	private static Map<String, Id> getProfileIds(List<String> listOfProfileName){
		Map<String, Id> mapOfProfileNameToId = new Map<String, Id>();
		List<Profile> listOfProfile = [select Id, Name from Profile where Name in :listOfProfileName];
		for (Profile thisProfile : listOfProfile){
			mapOfProfileNameToId.put(thisProfile.Name, thisProfile.Id);
		}
		return mapOfProfileNameToId;
	}
	
	/*
	 * Description		: Return UserRole Name to Id Map based on List of UserRole Names
	 * Author			: Patrick Brinksma
	 * Created Date		: 16-07-2013
	 */		
	private static Map<String, Id> getUserRoleIds(List<String> listOfRoleName){
		Map<String, Id> mapOfUserRoleNameToId = new Map<String, Id>();
		List<UserRole> listOfUserRole = [select Id, Name from UserRole where Name in :listOfRoleName];
		for (UserRole thisUserRole : listOfUserRole){
			mapOfUserRoleNameToId.put(thisUserRole.Name, thisUserRole.Id);
		}
		return mapOfUserRoleNameToId;
	}	

	/*
	 * Description		: Create a number of health insurance test accounts
	 * Author			: Patrick Brinksma
	 * Created Date		: 15-07-2013
	 */	
    public static List<Account> createHealthInsurers(String randVal, String sCountry, Integer iNum) {
        List<Account> listOfAccount = new List<Account>();
        for (Integer i=0; i<iNum; i++){
        	Account newAccount = new Account();
        	newAccount.RecordTypeId = getRecordTypeId(HEALTH_INSURANCE_RECORD_TYPE, 'Account');
        	newAccount.Name = RandVal + 'i';
        	newAccount.SAP_ID__c = 'SAPHI' + i;
        	//newAccount.SAP_Channel__c = '30';
        	newAccount.Type = HEALTH_INSURANCE_TYPE;
        	newAccount.Account_Country_vs__c = sCountry;
        	listOfAccount.add(newAccount);
        }
        return listOfAccount;
    }
    
	/*
	 * Description		: Create a number of Business Group test accounts
	 * Author			: Patrick Brinksma
	 * Created Date		: 18-12-2013
	 */	
    public static List<Account> createBGAccounts(String randVal, String sCountry, Integer iNum) {
        List<Account> listOfAccount = new List<Account>();
        for (Integer i=0; i<iNum; i++){
        	Account newAccount = new Account();
        	newAccount.RecordTypeId = getRecordTypeId(BUYING_GROUP_RECORD_TYPE, 'Account');
        	newAccount.Name = RandVal + 'bg' + 'i';
        	newAccount.Type = 'Buying Group';
        	newAccount.Account_Country_vs__c = sCountry;
        	listOfAccount.add(newAccount);
        }
        return listOfAccount;
    }    

	/*
	 * Description		: Create a number of Sub Business Group test accounts
	 * Author			: Patrick Brinksma
	 * Created Date		: 18-12-2013
	 */	
    public static List<Account> createSBGAccounts(String randVal, String sCountry, Integer iNum) {
        List<Account> listOfAccount = new List<Account>();
        for (Integer i=0; i<iNum; i++){
        	Account newAccount = new Account();
        	newAccount.RecordTypeId = getRecordTypeId(NON_SAP_RECORD_TYPE, 'Account');
        	newAccount.Name = RandVal + 'sbg' + 'i';
        	newAccount.Type = 'Sub Buying Group';
        	newAccount.Account_Country_vs__c = sCountry;
        	listOfAccount.add(newAccount);
        }
        return listOfAccount;
    }
    
	/*
	 * Description		: Create a number of non SAP test accounts
	 * Author			: Patrick Brinksma
	 * Created Date		: 18-12-2013
	 */	
    public static List<Account> createNONSAPAccounts(String randVal, String sCountry, Integer iNum) {
        List<Account> listOfAccount = new List<Account>();
        for (Integer i=0; i<iNum; i++){
        	Account newAccount = new Account();
        	newAccount.RecordTypeId = getRecordTypeId(NON_SAP_RECORD_TYPE, 'Account');
        	newAccount.Name = RandVal + 'i';
        	newAccount.Account_Country_vs__c = sCountry;
        	listOfAccount.add(newAccount);
        }
        return listOfAccount;
    }    
    
	
    public static List<Account> createPersonAccounts(String randVal, Integer iNum){
    	return createPersonAccounts(randVal, iNum, 'CANADA');
    }

    public static List<Account> createSAPPersonAccounts(String randVal, Integer iNum){
    	return createSAPPersonAccounts(randVal, iNum,'CANADA');
    }

	/*
	 * Description		: Create a number of person accounts
	 * Author			: Patrick Brinksma
	 * Created Date		: 15-07-2013
	 */	    
    public static List<Account> createPersonAccounts(String randVal, Integer iNum, String country){
    	Id paRTypeId = getRecordTypeId('Non_SAP_Patient', 'Account');
    	List<Account> listOfAccount = new List<Account>();
    	for (Integer i=0; i<iNum; i++){
    		Account thisAccount = new Account();
    		thisAccount.RecordTypeId = paRTypeId;
    		thisAccount.Contact_Gender__pc = 'Male';
    		thisAccount.LastName = randVal + i;
    		thisAccount.FirstName = 'TEST';
    		thisAccount.PersonBirthdate = System.today() - (20 * 365);
    		thisAccount.Account_Country_vs__c = country;
    		listOfAccount.add(thisAccount);
    	} 
    	return listOfAccount;
    }

	/*
	 * Description		: Create a number of SAP person accounts
	 * Author			: Patrick Brinksma
	 * Created Date		: 02-08-2013
	 */	    
    public static List<Account> createSAPPersonAccounts(String randVal, Integer iNum, String country){
    	Id paRTypeId = getRecordTypeId('SAP_Patient', 'Account');
    	List<Account> listOfAccount = new List<Account>();
    	for (Integer i=0; i<iNum; i++){
    		Account thisAccount = new Account();
    		thisAccount.RecordTypeId = paRTypeId;
    		thisAccount.Contact_Gender__pc = 'Male';
    		thisAccount.LastName = randVal + i;
    		thisAccount.FirstName = 'TEST';
    		thisAccount.SAP_Account_Name__c = randVal + i;
    		thisAccount.SAP_ID__c = 'SAPPA' + i;
    		thisAccount.SAP_Channel__c = '30';
    		thisAccount.PersonBirthdate = System.today() - (20 * 365);
    		thisAccount.Account_Country_vs__c = country;
    		thisAccount.DIB_Diabetes_Type__pc = 'Type 1';
			thisAccount.DIB_SAP_SR_Name__c = UserInfo.getLastName() + ', ' + clsUtil.isNull(UserInfo.getFirstName(), '');
     		listOfAccount.add(thisAccount);
    	} 
    	return listOfAccount;
    }

	/*
	 * Description		: Create a number of contacts for list of accounts
	 * Author			: Patrick Brinksma
	 * Created Date		: 15-07-2013
	 */	    
    public static List<Contact> createAccountContacts(String randVal, Integer numOfContacts, List<Account> listOfAccount){
    	List<Contact> listOfContact = new List<Contact>();
    	Id recordTypeId = getRecordTypeId('Generic_Contact', 'Contact');
    	for(Account thisAccount : listOfAccount){
    		for (Integer i=0; i<numOfContacts; i++){
    			Contact thisContact = new Contact();
    			thisContact.RecordTypeId = recordTypeId;
    			thisContact.LastName = thisAccount.Id + '_' + i;
    			thisContact.FirstName = 'TEST';
    			thisContact.Contact_Gender__c = 'Male';
    			thisContact.Contact_Primary_Specialty__c = 'Physiology';
    			thisContact.Contact_Department__c = 'Physiotherapy';
    			thisContact.AccountId = thisAccount.Id;
    			thisContact.Affiliation_To_Account__c = 'Employee';
    			thisContact.Primary_Job_Title_vs__c = 'Physician';
    			thisContact.MailingCountry = thisAccount.Account_Country_vs__c;
				listOfContact.add(thisContact);
    		}
    	}
    	return listOfContact;
    }
    
	/*
	 * Description		: Create a number of users with given profiles and roles, return Map Username to User
	 * Author			: Patrick Brinksma
	 * Created Date		: 16-07-2013
	 */
	 public static Map<String, User> createUsers(String randVal, String atSuffix, List<String> listOfProfileName, List<String> listOfRoleName){
	 	Map<String, User> mapOfUserNameToUser = new Map<String, User>();
	 	// Get profile Ids
	 	Map<String, Id> mapOfProfileNameToId = getProfileIds(listOfProfileName);
	 	// Get UserRole Ids
	 	Map<String, Id> mapOfUserRoleNameToId = getUserRoleIds(listOfRoleName);
	 	for (Integer i=0; i<listOfProfileName.size(); i++){
	 		User u = new User();
	 		u.LastName = randVal + i;
	 		u.UserName = randVal + i + atSuffix;
	 		u.Alias = 'alias' + i;
	 		u.Alias_unique__c = 'alias' + i;
	 		u.Email = randVal + i + atSuffix;
	 		u.EmailEncodingKey = 'ISO-8859-1';
	 		u.LanguageLocaleKey = 'en_US';
	 		u.LocaleSidKey = 'en_US';
	 		u.TimeZoneSidKey = 'Europe/Amsterdam';
	 		u.SAP_ID__c = 'SAPID' + i;
	 		u.ProfileId = mapOfProfileNameToId.get(listOfProfileName[i]);
	 		u.UserRoleId = mapOfUserRoleNameToId.get(listOfRoleName[i]);
	 		mapOfUserNameToUser.put(u.UserName, u);
	 	}
	 	return mapOfUserNameToUser;
	 }

	 /*
	 * Description		: Create a number of EntitySubscription and return list
	 * Author			: Paul Rice
	 * Created Date		: 20-01-2014
	 */
	public static List <EntitySubscription> createEntitySubscription(Integer numOfFollowers,ID subscriberId,List<ID> parentIds, Boolean doInsert) {
        List <EntitySubscription> entitySubscriptions = new List <EntitySubscription> ();

        for (Integer i = 0; i <numOfFollowers; i++) {
        	EntitySubscription sub = new EntitySubscription();			
			sub.parentId = parentIds[i];
			sub.subscriberId = subscriberId;
            entitySubscriptions.add(sub);
        }
        
		if (doInsert)
			insert entitySubscriptions;
			
        return entitySubscriptions;
    }
    	 
	 public static List<Opportunity> createOpportunities(String randVal, List<Account> listOfAccount, List<String> listOfRecordTypeDevName, Integer iNumPerAccount){
	 	List<Opportunity> listOfOppty = new List<Opportunity>();
	 	// Create physician account
	 	Account physAccount = createHealthInsurers('Physician' + randVal, 'CANADA', 1)[0];
	 	insert physAccount;
	 	Contact physContact = createAccountContacts('Physician' + randVal, 1, new List<Account>{physAccount})[0];
	 	insert physContact;
	 	
	 	for (Integer i=0; i<listOfAccount.size(); i++){
			Id opptyRTypeId = getRecordTypeId(listOfRecordTypeDevName[0], 'Opportunity');
			for (Integer j=0; j<iNumPerAccount; j++){
				Opportunity thisOppty = new Opportunity();
				thisOppty.RecordTypeId = opptyRTypeId;
				thisOppty.Name = listOfAccount[i].Id + '' + j;
				thisOppty.AccountId = listOfAccount[i].Id;
				thisOppty.StageName = 'New';
				thisOppty.CloseDate = System.today() + 14;
				thisOppty.Type = 'Other';
				thisOppty.Deal_Category__c = 'New Customer';
				thisOppty.CAN_DIB_Promotion_code__c='50OFF';
                thisOppty.Primary_Health_Insurer__c = physAccount.Id;
                thisOppty.Secondary_Health_Insurer_ID__c = physAccount.Id;	//- BC - 20140422 - CR-2698 - ADDED
                thisOppty.Additional_Health_Insurer_ID__c = physAccount.Id;	//- BC - 20140422 - CR-2698 - ADDED
				thisOppty.Physician_Account_ID__c = physAccount.Id;
				thisOppty.Contact_ID__c = physContact.Id;
				thisOppty.Department_Text__c = 'Adult';
				thisOppty.Opportunity_Source_Text__c = 'Hospital';
				thisOppty.Origin_Text__c='MDT Initiated';
				listOfOppty.add(thisOppty);
			}
	 	}	
	 	return listOfOppty;
	 }
	 
	 public static List<PricebookEntry> createPricebookEntries(Id priceBookId, Integer maxNum){
		// Create Pricebook and entry for product 
		Pricebook2 pbStandard = [select id from Pricebook2 where isStandard=true];
		List<Product2> listOfProd = [select Id from Product2 where ProductCode != null and id not in (SELECT Product2Id FROM PricebookEntry) 
			order by CreatedDate desc limit :maxNum];
		// First in standard pricebook
		List<PricebookEntry> listOfPBEntry = new List<PricebookEntry>();
		List<PricebookEntry> listOfSTPBEntry = new List<PricebookEntry>();
		for (Product2 thisProd : listOfProd){
			PricebookEntry pbSEntry = new PricebookEntry(Pricebook2Id = pbStandard.Id,
														Product2Id = thisProd.Id,
														UnitPrice = 2500.0,
														isActive = true,
														UseStandardPrice = false);
														
			listOfSTPBEntry.add(pbSEntry);	
			// Second in test pricebook
			PricebookEntry pbEntry = new PricebookEntry(Pricebook2Id = priceBookId,
														Product2Id = thisProd.Id,
														UnitPrice = 2500.0,
														isActive = true,
														UseStandardPrice = false);
			
			listOfPBEntry.add(pbEntry);
		}	
		insert listOfSTPBEntry;
		return listOfPBEntry; 	
	 }
	 
    public static List < Project_Management__c > createProjects(Integer numOfProjects, Boolean doInsert) {

        List < Project_Management__c > projects = new List < Project_Management__c > ();
        for (Integer i = 0; i < numOfProjects; i++) {
            Project_Management__c project = new Project_Management__c();
            projects.add(project);
        }
        if (doInsert)
            insert projects;
        return projects;
    }
    /*
	 * Description		: Create a number of custom project management and return list
	 * Author			: Paul Rice
	 * Created Date		: 20-01-2014
	 */
	public static List <Project_Final_Stage__c> createProjectFinalStage(List<String> values, Boolean doInsert) {
        List <Project_Final_Stage__c> stages = new List <Project_Final_Stage__c> ();

        for (Integer i = 0; i <values.size(); i++) {
        	Project_Final_Stage__c stage = new Project_Final_Stage__c();
        	stage.name = values[i];		
			stage.stage__c = values[i];
            stages.add(stage);
        }
        
		if (doInsert)
			insert stages;
			
        return stages;
    }
}