public with sharing class bl_ReviewRecord {
	
	public static Review_Record__c saveReviewRecord(Review_Record__c reviewRecord){
				
		if (null!=reviewRecord){
			upsert reviewRecord Mobile_ID__c; 
		}
		return reviewRecord;		
	} 
}