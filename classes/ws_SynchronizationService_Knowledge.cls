/*	Integration Service related class
	
	This REST service is used by the source Org to inform the target Org that there are changes in Knowledge records to be synchronized
*/
@RestResource(urlMapping='/SynchronizationServiceKnowledge/*')
global with sharing class ws_SynchronizationService_Knowledge {
	
	//This method schedules the execution of the batch that processes the Knowledge related record changes
	@HttpGet
	global static void doGet() {
						
		ba_SynchronizationService_Knowledge batch = new ba_SynchronizationService_Knowledge();
		Database.executeBatch(batch, ba_SynchronizationService_Knowledge.BATCH_SIZE);			
	}
}