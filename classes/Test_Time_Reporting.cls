@isTest
private with sharing class Test_Time_Reporting {
	
	private static testmethod void createSimpleReport(){
				
		Reporting_Activity__c activity = createSampleActivity(true);
		
		Test.startTest();
				
		ctrl_TimeReporting controller = new ctrl_TimeReporting();
		
		Time_Reporting__c inputRecord = controller.inputReport;
		inputRecord.Reporting_Activity__c = activity.Id;		
		
		controller.onActivityChange();		
		System.assert(controller.kronosInputRequired == true);
		
		controller.addNewReport();
		
		System.assert(controller.activityKeys.size()==1);
		
		List<String> allKeys = new List<String>(controller.timeReportMap.keySet());
		System.assert(allKeys.size()==7);
		
		Time_Reporting__c weekReport = controller.timeReportMap.get(allKeys[0]);		
		weekReport.hours__c = 8;
		
		controller.updateReports();
		
		controller.revertChanges();
		
		Test.stopTest();
		
		List<Time_Reporting__c> reports = [Select Id, Hours__c from Time_Reporting__c];
		
		System.assert(reports.size()>0);
		System.assert(reports[0].hours__c == 8);
	}
	
	private static testmethod void createSimpleReport_Failure(){
				
		Reporting_Activity__c activity = createSampleActivity(false);
		
		Test.startTest();
				
		ctrl_TimeReporting controller = new ctrl_TimeReporting();
		
		Time_Reporting__c inputRecord = controller.inputReport;
		inputRecord.Reporting_Activity__c = activity.Id;		
				
		controller.onActivityChange();		
		System.assert(controller.kronosInputRequired == false);
		
		controller.addNewReport();
		
		Test.stopTest();
		
		List<ApexPages.Message> msgList = ApexPages.getMessages();
		
		System.assert(controller.activityKeys.size()==0);
		System.assert(msgList.size()==1);
		
		System.assert(msgList[0].getSummary().contains('You have to choose a Project, Release or Change Request'));		
	}
	
	private static testmethod void deleteReport(){
		
		Project__c release = createSampleRelease();
		Reporting_Activity__c activity = createSampleActivity(false);		
		Time_Reporting__c report = createSampleReport(release, activity);
			
		Test.startTest();
						
		ctrl_TimeReporting controller = new ctrl_TimeReporting();
		
		System.assert(controller.activityKeys.size()== 1);
		
		controller.selectedReportId = controller.activityKeys[0];
		controller.deleteReport();
		
		Test.stopTest();
		
		System.assert(controller.activityKeys.size()== 0);				
	}
	
	private static testmethod void cloneWeekReport(){
		
		Project__c release = createSampleRelease();
		Reporting_Activity__c activity = createSampleActivity(false);		
		Time_Reporting__c report = createSampleReport(release, activity);
			
		Test.startTest();
						
		ctrl_TimeReporting controller = new ctrl_TimeReporting();
		
		System.assert(controller.activityKeys.size()== 1);
				
		controller.cloneWeek();
		
		Test.stopTest();
		
		System.assert(controller.activityKeys.size()== 1);								
	}
	
	
	private static testmethod void changeContextUser(){
		
		Project__c release = createSampleRelease();				
		Reporting_Activity__c activity = createSampleActivity(false);
		Time_Reporting__c report = createSampleReport(release, activity);
		
		Test.startTest();
				
		ctrl_TimeReporting controller = new ctrl_TimeReporting();
		
		System.assert(controller.activityKeys.size()== 1);
		
		User otherUser = [Select Id from User where isActive=true AND Id!=:UserInfo.getUserId() LIMIT 1];
		
		controller.contextUserHelper.ownerId=otherUser.Id;
		controller.changeContextUser();
		
		Test.stopTest();
		
		System.assert(controller.activityKeys.size()== 0);				
	}
	
	private static testmethod void calendarNavigation(){
		
		Project__c release = createSampleRelease();
		Reporting_Activity__c activity = createSampleActivity(false);						
		Time_Reporting__c report = createSampleReport(release, activity);
		
		Test.startTest();
				
		ctrl_TimeReporting controller = new ctrl_TimeReporting();
		
		System.assert(controller.activityKeys.size()== 1);
		
		controller.nextWeek();		
		System.assert(controller.activityKeys.size()== 0);
				
		Date inTwoMonth = Date.today().addMonths(2);
		
		controller.selectedMonth = String.valueOf(inTwoMonth.month());
		controller.selectedYear = String.valueOf(inTwoMonth.year());
		
		controller.changeMonth();
		System.assert(controller.activityKeys.size()== 0);
		
		controller.selectedDay = inTwoMonth.toStartOfMonth().format();
		controller.goToWeek();
		System.assert(controller.activityKeys.size()== 0);
		
		controller.prevWeek();
		controller.selectedMonth = String.valueOf(Date.today().month());
		
		controller.calWeeks[0].days[0].getStyle();
		controller.calWeeks[0].days[0].getDayStyle();
		
		Test.stopTest();						
	}
	
	
	
	/*
		HELPER METHODS
	*/
	private static Time_Registration_Code__c createRegistrationCode(){
		
		Time_Registration_Code__c regCode = new Time_Registration_Code__c();
		regCode.Name = 'Test Project';					
		regCode.Time_Registration_Category__c = 'Test Category';
		regCode.Active__c = true;
		regCode.Business_Unit__c = 'Test BU';
		regCode.Time_Registration_ID__c = 'TRC';
		
		insert regCode;
		
		return regCode;				
	}
	
	private static Project__c createSampleRelease(){
		
		Project__c release = new Project__c();
		release.Name='Test Release';
		release.Start_Date__c=Date.today();
		release.Project_Type__c='Minor Release';
		release.Status__c='Scheduled';
		release.Project_Manager__c=UserInfo.getUserId();
		release.End_Date__c=Date.today().addDays(10);
		//release.Kronos_Category__c='test category Id';
		//release.Kronos_Project__c='test project Id';
		release.Time_Registration_Project__c = createRegistrationCode().Id;
		release.Total_Budget__c=1000;
		
		insert release;
		
		return release;
	}
	
	private static Reporting_Activity__c createSampleActivity(Boolean manualInput){
		
		Reporting_Activity__c activity = new Reporting_Activity__c();
		activity.Name = 'Test Activity';
		activity.Requires_Manual_Input__c = manualInput;
		activity.chargeable__c = true;
		
		insert activity;
		
		return activity;
	}
	
	private static Time_Reporting__c createSampleReport(Project__c release, Reporting_Activity__c activity){
		
		Time_Reporting__c report = new Time_Reporting__c();
		report.Reporting_Activity__c = activity.Id;
		report.Release__c = release.Id;
		report.Hours__c = 8;
		report.OwnerId = UserInfo.getUserId();
		report.Date__c = Date.today();
		
		insert report;
		
		return report;
	}
}