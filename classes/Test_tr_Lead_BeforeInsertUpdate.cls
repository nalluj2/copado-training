/*
 * Work Item Nr		: WI-156 / WI-150 / WI-130
 * Description		: Test Class for Trigger tr_Lead_BeforeInsertUpdate
 * Author        	: Patrick Brinksma
 * Created Date    	: 15-07-2013
 */
@isTest
private class Test_tr_Lead_BeforeInsertUpdate {

    static testMethod void testHILeads() {
    	String randVal = Test_TestDataUtil.createRandVal();
        // Create test accounts
        List<Account> listOfAccount = Test_TestDataUtil.createHealthInsurers(randVal, 'CANADA', 10);
        insert listOfAccount;
        Map<String, Id> mapOfAccountNameToId = new Map<String, Id>();
        for (Account a : listOfAccount){
        	mapOfAccountNameToId.put(a.Name, a.Id);
        }
        
        // Prepare test leads
        List<Lead> listOfLead = new List<Lead>();
        for (Integer i=0; i<10; i++){
        	Lead tstLead = createLead(randVal, i);
        	listOfLead.add(tstLead);
        }
        for (Integer i=10; i<20; i++){
        	Lead tstLead = createLead(randVal, i);
        	listOfLead.add(tstLead);
        }
                
        Test.startTest();
        insert listOfLead;
        Test.stopTest();
        
        // Assert results
        String searchLastName = randVal + '%';
        List<Lead> listOfResult = [select Primary_Health_Insurer_Web__c, Primary_Health_Insurer_ID__c, Secondary_Health_Insurer_Web__c, Secondary_Health_Insurer_ID__c from Lead where LastName like :searchLastName];
        for (Lead thisLead : listOfResult){
        	if (thisLead.Primary_Health_Insurer_Web__c != null){
        		System.assertEquals(thisLead.Primary_Health_Insurer_ID__c, mapOfAccountNameToId.get(thisLead.Primary_Health_Insurer_Web__c));
        	}
        	if (thisLead.Secondary_Health_Insurer_Web__c != null){
        		System.assertEquals(thisLead.Secondary_Health_Insurer_ID__c, mapOfAccountNameToId.get(thisLead.Secondary_Health_Insurer_Web__c));
        	}
        }
        
    }
    
    static testMethod void testAssignLeads(){
    	String randVal = Test_TestDataUtil.createRandVal();
    	String atSuffix = '@salesforcetest.com';
    	// Create 3 users (2 for assignment, 1 admin for runas)
        List<String> lstUsername1 = new List<String>();
            lstUsername1.add('System Administrator');
            lstUsername1.add('CAN DiB IS');
            lstUsername1.add('CAN DiB IS');
        List<String> lstUsername2 = new List<String>();
            lstUsername2.add('System Administrator');
            lstUsername2.add('CAN (DIB)');
            lstUsername2.add('CAN (DIB)');
        Map<String, User> mapOfUserNameToUser = Test_TestDataUtil.createUsers(randVal, atSuffix, lstUsername1, lstUsername2);
//    	Map<String, User> mapOfUserNameToUser = Test_TestDataUtil.createUsers(randVal, atSuffix, new List<String>{'System Administrator', 'CAN DiB IS', 'CAN DiB IS'}, new List<String>{'System Administrator', 'CAN (DIB)', 'CAN (DIB)'});
    	insert mapOfUserNameToUser.values();
    	// Run followin as new sys admin user to prevent mixed DML
    	System.runAs(mapOfUserNameToUser.get(randVal + 0 + atSuffix)){
			// Create Assignment rules    		
			List<Country_Assignment_Rule__c> listOfCAssRule = new List<Country_Assignment_Rule__c>();
			Country_Assignment_Rule__c tstCAssRule1 = new Country_Assignment_Rule__c(Assignment_Value__c = 'TST', 
																					 Assigned_To_1__c = mapOfUserNameToUser.get(randVal + 1 + atSuffix).Id, 
																					 Assigned_To_2__c = mapOfUserNameToUser.get(randVal + 2 + atSuffix).Id);
			listOfCAssRule.add(tstCAssRule1);																		 
			Country_Assignment_Rule__c tstCAssRule2 = new Country_Assignment_Rule__c(Assignment_Value__c = ut_StaticValues.COUNTRY_ASSIGNMENT_BUCKET, 
																					 Assigned_To_1__c = mapOfUserNameToUser.get(randVal + 2 + atSuffix).Id, 
																					 Assigned_To_2__c = mapOfUserNameToUser.get(randVal + 1 + atSuffix).Id);
			listOfCAssRule.add(tstCAssRule2);
			insert listOfCAssRule;
			// Create Leads to test with	
			List<Lead> listOfLead = new List<Lead>();																	 
	        for (Integer i=0; i<10; i++){
	        	Lead tstLead = createLead(randVal, i);
	        	if (i < 5){
	        		tstLead.Lead_Zip_Postal_Code__c = 'TST';
	        	} else {
	        		tstLead.Lead_Zip_Postal_Code__c = 'XXX';
	        	}
	        	listOfLead.add(tstLead);
	        }	    		
	        Test.startTest();
	        insert listOfLead;
	        Test.stopTest();
	        
	        // Assert results
        	String searchLastName = randVal + '%';
        	List<Lead> listOfResult = [select Lead_Zip_Postal_Code__c, OwnerId, Assigned_To_ID__c, DiB_Sales_Rep_Selector__c from Lead where LastName like :searchLastName];
        	for (Lead thisLead : listOfResult){
        		if (thisLead.Lead_Zip_Postal_Code__c == 'TST'){
        			System.assertEquals(thisLead.OwnerId, mapOfUserNameToUser.get(randVal + 1 + atSuffix).Id);
        			System.assertEquals(thisLead.DiB_Sales_Rep_Selector__c, mapOfUserNameToUser.get(randVal + 1 + atSuffix).Id);
        			System.assertEquals(thisLead.Assigned_To_ID__c, mapOfUserNameToUser.get(randVal + 2 + atSuffix).Id);
        		}
        		if (thisLead.Lead_Zip_Postal_Code__c == 'XXX'){
        			System.assertEquals(thisLead.OwnerId, mapOfUserNameToUser.get(randVal + 2 + atSuffix).Id);
        			System.assertEquals(thisLead.DiB_Sales_Rep_Selector__c, mapOfUserNameToUser.get(randVal + 2 + atSuffix).Id);
        			System.assertEquals(thisLead.Assigned_To_ID__c, mapOfUserNameToUser.get(randVal + 1 + atSuffix).Id);
        		}
        	}	        
    	}
	    	
    }
   
    static testMethod void testPhysicianLead(){
    	String randVal = Test_TestDataUtil.createRandVal();
        // Create test accounts
        List<Account> listOfAccount = Test_TestDataUtil.createHealthInsurers(randVal, 'CANADA', 10);
        insert listOfAccount;

        // Create test Contacts for these accounts
        List<Contact> listOfContact = Test_TestDataUtil.createAccountContacts(randVal, 1, listOfAccount);
        insert listOfContact;
        Map<Id, Id> mapOfContactIdToAccountId = new Map<Id, Id>();
        for (Contact thisContact : listOfContact){
        	mapOfContactIdToAccountId.put(thisContact.Id, thisContact.AccountId);
        }
        
        // Create test leads   	
        List<Lead> listOfLead = new List<Lead>();
        for (Integer i=0; i<10; i++){
        	Lead tstLead = createLead(randVal, i);
        	tstLead.Contact_ID__c = listOfContact[i].Id;
            if (i<5){
                tstLead.Account_ID__c = listOfAccount[i].Id;
            }
        	listOfLead.add(tstLead);
        }

        List<Account> lstAccount = Test_TestDataUtil.createHealthInsurers(randVal, 'CANADA', 1);
        lstAccount[0].SAP_ID__c = 'SAPHI' + 50;
        insert lstAccount;
        Lead oLead = createLead(randVal, 20);
        oLead.Contact_ID__c = listOfContact[0].Id;
        oLead.Account_ID__c = lstAccount[0].Id;
        oLead.RecordTypeId = clsUtil.getRecordTypeByDevName('Lead', ut_StaticValues.RTDEVNAME_CAN_DIB_LEAD).Id;
        try{
            upsert oLead;
        }catch(DmlException oEX){
            system.assert(oEX.getMessage().contains('Physician is not related to the chosen account'));
        }

        Test.startTest();
        insert listOfLead;
        Test.stopTest();
        
        // Assert results
    	String searchLastName = randVal + '%';
    	List<Lead> listOfResult = [select Contact_ID__c, Account_ID__c from Lead where LastName like :searchLastName];
    	for (Lead thisLead : listOfResult){
    		System.assertEquals(thisLead.Account_ID__c, mapOfContactIdToAccountId.get(thisLead.Contact_ID__c));
    	}
        
    }
    
    private static Lead createLead(String randVal, Integer i){
    	Lead tstLead = new Lead();
    	tstLead.LastName = randVal + i;
    	tstLead.FirstName = 'TEST';
    	tstLead.Lead_Address_Line_1__c = randVal + i;
    	tstLead.Lead_Gender_Text__c = 'Male';
    	tstLead.Lead_City__c = 'Test City';
    	tstLead.Email = randVal + i + '@salesforcetesting.com';
    	tstLead.Lead_State_Province__c = 'Alberta';
    	tstLead.Lead_Zip_Postal_Code__c = '12345';
    	tstLead.Lead_Country_vs__c = 'CANADA';
    	tstLead.Primary_Health_Insurer_Web__c = randVal + i;
    	tstLead.Secondary_Health_Insurer_Web__c = randVal + i;    	
    	return tstLead;
    }
}