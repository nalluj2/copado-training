@isTest
private class Test_tr_AccountPlanStakeholder {
    
    private static testmethod void testStakeholderContactComments(){
    	
    	clsTestData_AccountPlan.createAccountPlan2();
    	
    	clsTestData_Contact.iRecord_Contact = 2;
    	clsTestData_Contact.createContact(false);
    	
    	Contact cnt1 = clsTestData_Contact.lstContact[0];
    	Contact cnt2 = clsTestData_Contact.lstContact[1];
    	
    	cnt1.Contact_Comments__c = 'Test Comments Contact 1';
    	
    	insert clsTestData_Contact.lstContact;
    	
    	Account_Plan_2__c accPlan = [Select Id from Account_Plan_2__c where Account__c = :clsTestData_Account.oMain_Account.Id AND Sub_Business_Unit__r.Name = 'Aortic'];
    	
    	Account_Plan_Stakeholder__c apStakeholder = new Account_Plan_Stakeholder__c();
    	apStakeholder.Account_Plan__c = accPlan.Id;
    	apStakeholder.Stakeholder_Name__c = cnt1.Id;
    	apStakeholder.CVG_Role__c = 'Influencer';
    	apStakeholder.CVG_Influence__c = '3';
    	apStakeholder.CVG_Relationship__c = '4';    	
    	insert apStakeholder;
    	
    	apStakeholder = [Select Id, Comments__c from Account_Plan_Stakeholder__c where Id = :apStakeholder.Id];
    	System.assert(apStakeholder.Comments__c == 'Test Comments Contact 1');
    	    	
    	apStakeholder.Stakeholder_Name__c = cnt2.Id;
    	update apStakeholder;
    	
    	apStakeholder = [Select Id, Comments__c from Account_Plan_Stakeholder__c where Id = :apStakeholder.Id];
    	System.assert(apStakeholder.Comments__c == null);
    	
    	cnt2.Contact_Comments__c = 'Test Comments Contact 2';
    	update cnt2;
    	
    	apStakeholder = [Select Id, Comments__c from Account_Plan_Stakeholder__c where Id = :apStakeholder.Id];
    	System.assert(apStakeholder.Comments__c == 'Test Comments Contact 2');
    }
}