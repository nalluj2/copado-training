public class controllerTerritoryAssignAccounts{
    string terrId='';
    public controllerTerritoryAssignAccounts(ApexPages.StandardController controller) {
        this.rowsToShow='5';
        
        if(terrId==''){
            terrId=ApexPages.currentPage().getParameters().get('TerId');
        }
        AvailableAccount1();
        if(AssignedButtonPressed!=true)
        {
            AssignedButtonPressed=false;
        }
        if(ApplyFilter!=true)
        {
            ApplyFilter=false;
        }
        if(AccSizeCheck!=true)
        {
            AccSizeCheck=false;
        }        
    }
    public boolean AccSizeCheck {        
        get { return AccSizeCheck; }        
        set { AccSizeCheck = value; }    
    }   
    
    public boolean ApplyFilter {        
        get { return ApplyFilter; }        
        set { ApplyFilter = value; }    
    }   
    public boolean AssignedButtonPressed {        
        get { return AssignedButtonPressed; }        
        set { AssignedButtonPressed = value; }    
    }  
    public string AccRecords {        
        get { return AccRecords; }        
        set { AccRecords = value; }    
    }
    public List<SelectOption> GetAccRecSize() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('5','5'));
        options.add(new SelectOption('10','10'));
        options.add(new SelectOption('25','25'));
        options.add(new SelectOption('50','50'));
        options.add(new SelectOption('100','100')); 
        if(AccRecords==null){
            AccRecords='10';
        }   
        return options;
    }       

     
    public string AccName{get;set;}
    public string ShipCity{get;set;}
    public string ShipPost{get;set;}
    public string SapId{get;set;}

    // Country
    String Country;
    public String getCountry() { return this.Country; }
    public void setCountry(String s) { this.Country = s; }

    public List<SelectOption> getCountries() {
        List<SelectOption> optionList = new List<SelectOption>();
        Territory2 currTerr = [Select Id, Company__c from Territory2 where Id =: terrId];
        String CMP = [Select Id from Company__c where Id =: currTerr.Company__c].Id;
        DIB_Country__c[]  Country1 = [Select Country_ISO_Code__c,Name from DIB_Country__c where Company__c =: CMP order by Name];             
        if(Country1.size()>0){    
          for (DIB_Country__c C : Country1){              
              optionList.add(new SelectOption(String.valueOf(C.get('Country_ISO_Code__c')),String.valueOf(C.get('Name'))));        
        }  
        if(Country==null){          
            String userTerrCountry =[Select Country_UID__c from Territory2 where id=:terrId].Country_UID__c;
            Country=userTerrCountry;         
        }     
      }      
      return optionList;     
    }

     
    // Get the detail information of the choosen Territory Node
    public Territory2 getTDetail(){
        string Child = apexpages.currentpage().getUrl();
        integer inx  = Child.indexOf('TerId=');
        String terrId;
        Territory2 detail;
        if (inx != -1){
            terrId = Child.substring(inx + 6  );
            detail = [Select Id, Name,Business_Unit__c, Territory2Type.DeveloperName, Country_UID__c,Short_Description__c,Therapy_Groups_Text__c, ParentTerritory2Id  from Territory2 where Id=:terrId];
            
        }
        else{
            Id userId = UserInfo.getUserId();
            String userTerrUID =[Select Territory_UID__c from User where id=:userId].Territory_UID__c;
            detail = [Select Id, Name,Business_Unit__c, Territory2Type.DeveloperName, Country_UID__c,Short_Description__c,Therapy_Groups_Text__c, ParentTerritory2Id  from Territory2 where Territory_UID2__c =:userTerrUID];
        }    
        return detail;
    }
    public String getCrumbs(){
        String crumbs ;
        Boolean levelFound = false;
        crumbs ='';
        Territory2 currTerrId = [Select Id, Territory2Type.DeveloperName from Territory2 where Id =:terrId];
        
        Territory2 allChildren = new Territory2();
        Id tempId = terrId;
            do{
                allChildren = [Select Id, Name,Business_Unit__c, Territory2Type.DeveloperName, Country_UID__c,Short_Description__c,Therapy_Groups_Text__c, ParentTerritory2Id  from Territory2 where Id=:tempId];
                
                If (levelFound){
                    crumbs = allChildren.Name + '  » ' + crumbs;
                }
                else {
                    //crumbs = '<a href=../apex/TerritoryManagement?id=' + allChildren.Id + '> ' + allChildren.Name + '</a> &raquo' + crumbs;
                    crumbs = allChildren.Name + '  » ' + crumbs;
                
                }
                tempId = allChildren.ParentTerritory2Id;
                If (allChildren.Territory2Type.DeveloperName == currTerrId.Territory2Type.DeveloperName){
                    levelFound = true;
                }
                
                } while (allChildren.Territory2Type.DeveloperName != 'Region');
        
            // Remove the last >>
            integer crmbIndex = crumbs.lastIndexOf(' »')  ;
            if (crmbIndex !=-1){
                crumbs = crumbs.substring(0,crmbIndex);
            }       
    return crumbs;       
    }

    public ApexPages.StandardSetController Con;    
    public String rowsToShow;

    private list<Account> AvailableAccount = new  list<Account>(); 
    public List<Account> getAvailableAccount() {return AvailableAccount;}     
    public void AvailableAccount1() 
    {
        Territory2 currTerr = [Select Country_UID__c, Territory2Type.DeveloperName  from Territory2 where id=:terrId];
        String userTerrCountry ='';
        if(Country==null){
            userTerrCountry = currTerr.Country_UID__c;            
        }
        else {
            userTerrCountry = Country;  
        }
        string strTerCountry='';
        if(userTerrCountry!='' && userTerrCountry!=null)
        {
            strTerCountry=[Select Name from DIB_Country__c where Country_ISO_Code__c=:userTerrCountry].Name;            
        }        
        // Get existing Accounts
        list<Id> existAccIds=new list<Id>();
        list<Id> CountTerIds=new list<Id>();
        list<Id> DistrIds=new list<Id>();
        list<Id> TerrIds=new list<Id>();
        
        System.debug('userTerrCountry: '+userTerrCountry);
        System.debug('currTerr.Territory2Type: '+currTerr.Territory2Type.DeveloperName);
        
        list<Territory2> CountTers = [Select Id from Territory2 where Territory2Type.DeveloperName=:currTerr.Territory2Type.DeveloperName and Country_UID__c =:userTerrCountry];

        if(CountTers.size()>0)
        {
            for(integer i=0;i<CountTers.size();i++)
            {
                CountTerIds.add(string.valueOf(CountTers[i].get('Id')));    
            }         
        } 
        String TherapyGrps = [select Therapy_Groups_Text__c from Territory2 where Id=:terrId].Therapy_Groups_Text__c;
        List<String> TherapyGroups;
        TherapyGroups = TherapyGrps.split(';');
        String smalltest = '(\'ENT/NT Other\')';
        String TherGrps = '(';
        for (integer j=0;j<TherapyGroups.size();j++)
        {
        TherGrps = TherGrps + '\'' + TherapyGroups[j] + '\',';
        
        }
        integer lngth = TherGrps.lastIndexOf(',');
        TherGrps = TherGrps.substring(0,lngth);
        TherGrps = TherGrps + ')';
        System.debug('>>>>>>>>>>>>>>>>>>>>>>>> Therapy group string  ' + TherGrps);
        
        // Get Territory Id's of the all territories below this salesforce.
        string sqlstring ='select Id from Territory2 where Country_UID__c =\''+userTerrCountry+'\' and Therapy_Groups_Text__c includes ' + TherGrps;
        System.debug('Query: '+sqlstring);
        list<Territory2> lsdistrId = Database.query(sqlstring); 
        System.debug('>>>>>>>>>>>>>>>>>>>>>>>> records with same Therapy group ' + lsdistrId.size());
        for (integer i=0;i<lsdistrId.size();i++)
        {
            DistrIds.add(string.valueOf(lsdistrId[i].get('Id')));
        }
        
        if(DistrIds.size()>0)
        {
            list<ObjectTerritory2Association> AccShare;
            //[RUDY DE CONINCK ] Temporary fix to unblock CANADA (Long term fix should define which countries are candidate for overlap, other countries query with country filter) 
            if (strTerCountry!=null && strTerCountry.equalsIgnoreCase('CANADA')){
            	AccShare = [select ObjectID from ObjectTerritory2Association where ObjectId IN (Select Id from Account where Account_Country_vs__c=: strTerCountry) and Territory2Id  in :DistrIds];
            }else{
             	AccShare = [select ObjectID from ObjectTerritory2Association where Territory2Id  in :DistrIds];            
            }
            if(AccShare.size()>0)
            {
                for(integer i=0;i<AccShare.size();i++)
                {
                    existAccIds.add(string.valueOf(AccShare[i].ObjectId));    
                }         
            }
        }                    
            string strQuery='';
            string strAccType='Trunk';      
            string strSAPAccType='Ship to Party';          
            if(existAccIds.size()>0)
            {
                strQuery='select id,Account_Active__c,SAP_Account_Type__c,name,Account_Postal_Code__c,Account_City__c,SAP_ID__c from Account where Type <> \''+strAccType+'\' and (SAP_Account_Type__c !=: strSAPAccType OR (SAP_Search_Term1__c = \'MASTER\' AND SAP_Search_Term2__c = \'SHIP TO\')) and Account_Country_vs__c=: strTerCountry and id not in : existAccIds';

            }
            else 
            {
                strQuery='select id,Account_Active__c,SAP_Account_Type__c,name,Account_Postal_Code__c,Account_City__c,SAP_ID__c from Account where Type<> \''+strAccType+'\' and (SAP_Account_Type__c !=: strSAPAccType OR (SAP_Search_Term1__c = \'MASTER\' AND SAP_Search_Term2__c = \'SHIP TO\')) and Account_Country_vs__c=: strTerCountry  ';                
            }         
            if(AccName!=null && AccName!='')
            {
                 strQuery=strQuery + ' and Name like \''+ string.escapeSingleQuotes(AccName.replace('*','%').trim())+'%\'' ;          
            }
            if(ShipCity!=null && ShipCity!='')
            {
                 strQuery=strQuery + ' and Account_City__c like \''+ string.escapeSingleQuotes(ShipCity.replace('*','%').trim())+'%\'' ;          
            }
            if(ShipPost!=null && ShipPost!='')
            {
                 strQuery=strQuery + ' and Account_Postal_Code__c like \''+ string.escapeSingleQuotes(ShipPost.replace('*','%').trim())+'%\'' ;          
            }
            if(SapId!=null && SapId!='')
            {
                 strQuery=strQuery + ' and SAP_ID__c like \''+ string.escapeSingleQuotes(SapId.replace('*','%').trim())+'%\'' ;          
            }
            string strQuery1=strQuery + ' limit 505 ';
            
            list<Account> accSize = Database.query(strQuery1);  
            if(accSize.size()>500){
                AccSizeCheck = true;    
            }
            else
            {
                AccSizeCheck = false;                   
            }
            
            
            strQuery=strQuery + ' order by name limit 500 ';            
            //AvailableAccount  = Database.query(strQuery);
            Con = new ApexPages.StandardSetController(Database.getQueryLocator(strQuery));
            if(AccRecords==null){
                AccRecords='10';
            }                                           
            Con.setPageSize(integer.valueof(AccRecords));        
            AvailableAccount=(List<Account>)Con.getRecords();                                                                       
            if(AccName!=null || ShipCity!=null || ShipPost!=null || SapId!=null)
            {
                if(AccName!='' || ShipCity!='' || ShipPost!='' || SapId!='')
                {
                    ApplyFilter=true;
                }
                else
                {
                    ApplyFilter=false;          
                }    
            }      
            else
            {
                ApplyFilter=false;          
            }
            
    }

    // Start Account Pagination
    public String getTotalPages() {
       if(con!=null){       
            String s=null;
            double resSize=con.getResultSize();
            Integer pageNumber;
            if(con.getResultSize()>0){   
                pageNumber=con.getPageNumber();
            }
            else{
                pageNumber=0;
            }
            Integer pages=(Math.round(Math.ceil(resSize/con.getPageSize())));
            s=  'Page ' +pageNumber+ ' of ' + pages+'';
            return s;
       }
       else{
        return null;
       }
                
    }
  
    public void previous() {
       if(con!=null){
        con.save();
        con.previous();
        AvailableAccount = con.getRecords();
     }
    }

    public void next() {
      if(con!=null){
        con.save();
        con.next();
        AvailableAccount = con.getRecords();
       }
    }


   public void first() {
    if(con!=null){
        con.save();
        con.first();
         AvailableAccount = con.getRecords();
        }
    }

    public void last() {
     if(con!=null){
        con.save();
        con.last();
         AvailableAccount = con.getRecords();
        }
    }

    public Boolean getHasNext(){    
        if(con==null) return false;
        return con.getHasNext();
    }
    
    public Boolean getHasPrevious(){    
        if(con==null)return false;
        return con.getHasPrevious();
    }  
    // End Account Pagination    
    
    public String SelAccId {get;set;}
    public String RemoveSelAccId {get;set;}
        
    private list<Account> SelectAccount = new  list<Account>(); 
    public List<Account> getSelectAccount() {return SelectAccount;}         
    public PageReference SelectedAccount(){
        System.Debug('>>>>>>>>>>>>Selected Account Method ' + SelAccId); 
        Boolean Accfoundflag=false;
        if(SelectAccount.size()>0)
        {
            System.Debug('>>>>SelectAccount - ' + SelectAccount.size());
            //for(Account A:SelectAccount){
            for(Integer i=0;i<SelectAccount.size();i++)
            {
                //System.Debug('>>>>inside SelectAccount - ' + A.Id + ' '  + SelAccId);
                if(SelectAccount[i].Description==SelAccId)
                {
                    Accfoundflag=true;
                    //SelectAccount.remove(i);                     
                }                  
            }
        }
        if(Accfoundflag==false)
        {        
            list<Account> Acc=[select id,Account_Active__c,name,Account_Postal_Code__c,Account_City__c,SAP_ID__c,Account_Country_vs__c from Account where id =: SelAccId];               
            if(Acc.size()>0)
            {    
                System.Debug('>>>>>>>>>>>>InSide Selected ' + SelAccId); 
                Account Acc1=new Account();
                for(Account A:Acc){  
                    Acc1=new Account();
                    Acc1.Description=A.Id; // Need to write the Id to check the duplicate & later insert it into the Database
                    Acc1.Account_Active__c=true;
                    Acc1.Name=A.Name;  
                    Acc1.Account_Postal_Code__c=A.Account_Postal_Code__c;  
                    Acc1.Account_City__c=A.Account_City__c;  
                    Acc1.Account_Country_vs__c = A.Account_Country_vs__c;
                    Acc1.SAP_ID__c=A.SAP_ID__c;                  
                    SelectAccount.add(Acc1);
                }                
            }
        }
        return null;
        
    }    
    
    public void RemoveSelectedAccount(){
        if(SelectAccount.size()>0){
            // Remove the account from selected list
            for(Integer i=0;i<SelectAccount.size();i++)
            {
                if(SelectAccount[i].Description==RemoveSelAccId)
                {
                    //Accfoundflag=true;
                    SelectAccount.remove(i);                     
                }                  
            }
        }
    }            
    public PageReference SelectAllAccounts(){
        list<Account> SelectAccounts = new  list<Account>(); 
        if(AvailableAccount.size()>0){
            //SelectAccount.clear();
            Account Acc1=new Account();                      
            for(Account A:AvailableAccount){  
                Acc1=new Account();
                Acc1.Description=A.Id; // Need to write the Id to check the duplicate & later insert it into the Database
                Acc1.Account_Active__c=true;
                Acc1.Name=A.Name;  
                Acc1.Account_Postal_Code__c=A.Account_Postal_Code__c;  
                Acc1.Account_City__c=A.Account_City__c;  
                Acc1.SAP_ID__c=A.SAP_ID__c;  
                integer intAlreadyExist=0;
                for(Integer i=0;i<SelectAccount.size();i++){
                    if( SelectAccount[i].Description == A.Id){
                        intAlreadyExist = 1;
                        break;
                    }
                }                           
                if(intAlreadyExist==0){
                    SelectAccount.add(Acc1);                                        
                }                   
            }              
        }
        return null;            
    }
    public PageReference AssignAccounts(){
                       
        if(SelectAccount.size()>0)
        {
        integer nrOfRecords = SelectAccount.size();
        List <ObjectTerritory2Association> ToAssign = new List <ObjectTerritory2Association>();
            for(Account Acc:SelectAccount){          
                ObjectTerritory2Association Accshare=new ObjectTerritory2Association();
                Accshare.ObjectId=Acc.Description;        
                Accshare.Territory2ID=terrId;
                Accshare.AssociationCause = 'Territory2Manual';
                ToAssign.add(Accshare);
            }  
            try{
                insert ToAssign;
            }catch(System.DMLException e){
                ApexPages.addMessages(e);   
            }
           
        }
        AssignedButtonPressed=true;
        return null;
    }    
}