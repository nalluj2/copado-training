/*
 * Author 		:	Bart Caelen
 * Date			:	20140129
 * Description	:	CR-3006 - Added logic to count the distinct related Accounts & Contacts for a Call Record
 */
public class bl_CallRecord {
	
	@future
	public static void updateAttendedAccountContact_FUTURE(set<id> setCallRecordID){
		updateAttendedAccountContact(setCallRecordID);
	}
	public static void updateAttendedAccountContact(set<id> setCallRecordID){
		list<Contact_Visit_Report__c> lstContactVisitReport = 
			[
				select 
					id
					, Attending_Affiliated_Account__c, Attending_Affiliated_Account__r.Id, Attending_Affiliated_Account__r.name, Attending_Affiliated_Account__r.Account_Country_vs__c
					, Attending_Contact__c, Attending_Contact__r.Id, Attending_Contact__r.Name
					, Call_Records__c, Call_Records__r.Id
				from 
					Contact_Visit_Report__c 
				where 
					Call_Records__c in :setCallRecordID
				order by 
					CreatedDate
			];
			updateAttendedAccountContact(lstContactVisitReport);
	}
	public static void updateAttendedAccountContact(list<Contact_Visit_Report__c> lstContactVisitReport){
		
		map<id, Call_Records__c> mapCallRecord = new map<id, Call_Records__c>();
		map<id, string> mapCallRecID_AccountCountry = new map<id, string>();
		map<id, map<id, string>> mapCallRecID_AccountID_AccountName = new map<id, map<id, string>>();
		map<id, map<id, string>> mapCallRecID_ContactID_ContactName = new map<id, map<id, string>>();

		for (Contact_Visit_Report__c oContactVisitReport : lstContactVisitReport){
			
			// Account Data			
			map<id, string> mapAccountID_Name = new map<id, string>();
			if (mapCallRecID_AccountID_AccountName.containsKey(oContactVisitReport.Call_Records__r.id)){
				mapAccountID_Name = mapCallRecID_AccountID_AccountName.get(oContactVisitReport.Call_Records__r.Id);
			}
			mapAccountID_Name.put(oContactVisitReport.Attending_Affiliated_Account__r.Id, oContactVisitReport.Attending_Affiliated_Account__r.name);
			mapCallRecID_AccountID_AccountName.put(oContactVisitReport.Call_Records__r.Id, mapAccountID_Name);				
			if (!mapCallRecID_AccountCountry.containsKey(oContactVisitReport.Call_Records__r.Id)){
				mapCallRecID_AccountCountry.put(oContactVisitReport.Call_Records__r.Id, oContactVisitReport.Attending_Affiliated_Account__r.Account_Country_vs__c);
			}
			
			// Contact Data			
			map<id, string> mapContactID_Name = new map<id, string>();
			if (mapCallRecID_ContactID_ContactName.containsKey(oContactVisitReport.Call_Records__r.id)){
				mapContactID_Name = mapCallRecID_ContactID_ContactName.get(oContactVisitReport.Call_Records__r.Id);
			}
			mapContactID_Name.put(oContactVisitReport.Attending_Contact__r.Id, oContactVisitReport.Attending_Contact__r.name);
			mapCallRecID_ContactID_ContactName.put(oContactVisitReport.Call_Records__r.Id, mapContactID_Name);				

			// Contact Visit Report Data			
			mapCallRecord.put(oContactVisitReport.Call_Records__r.id, oContactVisitReport.Call_Records__r);

		}
			
		list<Call_Records__c> lstCallRecord_Update = new list<Call_Records__c>();
		
		for (id idCallRecord : mapCallRecord.keySet()){

			Call_Records__c oCallRecord = mapCallRecord.get(idCallRecord);
			
			if (oCallRecord !=  null){
				integer iAccount_Counter = 0;
				string tLink_Account = '';
				if (mapCallRecID_AccountID_AccountName.containsKey(idCallRecord)){
					map<id, string> mapAccountID_Name = mapCallRecID_AccountID_AccountName.get(idCallRecord); 			
					for (ID idAccount : mapAccountID_Name.keySet()){
						tLink_Account += '<a target="_self" href="/' + idAccount + '">' + mapAccountID_Name.get(idAccount) + '</a>;';
						iAccount_Counter++;
					}
				}
				oCallRecord.Related_Accounts__c = tLink_Account;
				oCallRecord.Number_of_Related_Accounts__c = iAccount_Counter;
	
				integer iContact_Counter = 0;
				string tLink_Contact = '';
				if (mapCallRecID_ContactID_ContactName.containsKey(idCallRecord)){
					map<id, string> mapContactID_Name = mapCallRecID_ContactID_ContactName.get(idCallRecord); 			
					for (ID idContact : mapContactID_Name.keySet()){
						tLink_Contact += mapContactID_Name.get(idContact) + ';';
	//					tLink_Contact += '<a target="_self" href="/' + idContact + '">' + mapContactID_Name.get(idContact) + '</a>;';
						iContact_Counter++;
					}
				}
				oCallRecord.Call_Contact_Concatenated__c = tLink_Contact;
	
				if (mapCallRecID_AccountCountry.containsKey(idCallRecord)){
		        	oCallRecord.Countries__c = mapCallRecID_AccountCountry.get(idCallRecord);
				}
				
				oCallRecord.Process_Batch__c = false;
	
				lstCallRecord_Update.add(oCallRecord);
			}
		}
				
		update lstCallRecord_Update;

	}
}