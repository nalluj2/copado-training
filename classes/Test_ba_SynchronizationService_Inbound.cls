@isTest
private class Test_ba_SynchronizationService_Inbound {

	private static testmethod void inboundNotification(){
		
		Synchronization_Service_Setting__c syncSettings = new Synchronization_Service_Setting__c();
		syncSettings.username__c = 'test@medtronic.com';
		syncSettings.password__c = 'password';
		syncSettings.client_id__c = '123456789';
		syncSettings.client_secret__c = 'XXXXX';
		syncSettings.is_sandbox__c = true;
		syncSettings.Sync_Enabled__c = true;
		insert syncSettings;	
		
		Sync_Notification__c inboundNotification = new Sync_Notification__c();
		inboundNotification.Record_Object_Type__c = 'Case';
		inboundNotification.Record_Id__c = 'Record_Id';
		inboundNotification.Status__c = 'New';
		inboundNotification.Type__c = 'Inbound';
		inboundNotification.Notification_Id__c = 'Inbound_Notification_Id';	
		
		Sync_Notification__c outboundNotification = new Sync_Notification__c();
		outboundNotification.Record_Object_Type__c = 'Case';
		outboundNotification.Record_Id__c = 'Record_Id';
		outboundNotification.Status__c = 'New';
		outboundNotification.Type__c = 'Outbound';
		outboundNotification.Notification_Id__c = 'Outbound_Notification_Id';
				
		insert new List<Sync_Notification__c>{inboundNotification, outboundNotification};
		
		Test.startTest();
					
		ba_SynchronizationService_Inbound batch = new ba_SynchronizationService_Inbound();
		Database.executeBatch(batch, 1);
		
		CalloutMock mockImpl = new CalloutMock();
		
		Test.setMock(HttpCalloutMock.class, mockImpl);
		bl_SynchronizationService_Utils.calloutMock = mockImpl;	
		
		Test.stopTest();
	}
	
	private static testmethod void inboundNotification_err(){
		
		Synchronization_Service_Setting__c syncSettings = new Synchronization_Service_Setting__c();
		syncSettings.username__c = 'test@medtronic.com';
		syncSettings.password__c = 'password';
		syncSettings.client_id__c = '123456789';
		syncSettings.client_secret__c = 'XXXXX';
		syncSettings.is_sandbox__c = true;
		syncSettings.Sync_Enabled__c = true;
		insert syncSettings;	
		
		Sync_Notification__c inboundNotification = new Sync_Notification__c();
		inboundNotification.Record_Object_Type__c = 'Case';
		inboundNotification.Record_Id__c = 'Record_Id';
		inboundNotification.Status__c = 'New';
		inboundNotification.Type__c = 'Inbound';
		inboundNotification.Notification_Id__c = 'Inbound_Notification_Id';	
		insert inboundNotification;
		
		Test.startTest();
					
		ba_SynchronizationService_Inbound batch = new ba_SynchronizationService_Inbound();
		Database.executeBatch(batch, 1);
		
		CalloutMock_err mockImpl = new CalloutMock_err();
		
		Test.setMock(HttpCalloutMock.class, mockImpl);
		bl_SynchronizationService_Utils.calloutMock = mockImpl;	
		
		Test.stopTest();
	}
	
	public class CalloutMock implements HttpCalloutMock {

        public HTTPResponse respond(HTTPRequest req) {
            
            HttpResponse resp = new HttpResponse();
            resp.setStatusCode(200);
            resp.setStatus('Complete');
            
            String respBody; 
            
            if(req.getEndpoint().endsWith('services/oauth2/token')){
            	respBody =  '{' +
					            '"id" : "1",' +
					            '"issued_at" : "1",' +
					            '"instance_url" : "1",' +
					            '"signature" : "1",' +
					            '"access_token" : "1",' +
					            '"token_type" : "1"' +
				            '}';
				            
            }else if(req.getEndpoint().endsWith('services/apexrest/SynchronizationServiceSource') || req.getEndpoint().endsWith('services/apexrest/SynchronizationServiceTarget')){
            	
            	respBody =  req.getBody();
            	          	
            }else{
            	
            	SynchronizationService_Case.CaseSyncPayload request = new SynchronizationService_Case.CaseSyncPayload();
		
				//Account
				SynchronizationService_Payload.SyncAccount account = new SynchronizationService_Payload.SyncAccount();		
				account.SAPId = '0000000';
				account.name = 'Syn Test Account';
				account.phone = '+123456789';
				account.city = 'Minneapolis';
				account.country = 'UNITED STATES';
				account.state = 'Minnesota';
				account.street = 'street';
				account.postalCode = '123456';			
				request.accounts = new List<SynchronizationService_Payload.SyncAccount>{account};
				
				//Contact
				SynchronizationService_Payload.SyncContact contact = new SynchronizationService_Payload.SyncContact();		
				contact.FirstName = 'Test';
				contact.LastName = 'Contact';
				contact.Phone = '+1234526789';
				contact.Email = 'test.contact@gmail.com';
				contact.MobilePhone = '+987654321';		
				contact.City = 'Minneapolis';
				contact.Country = 'UNITED STATES';
				contact.State = 'Minnesota';
				contact.Street = 'street';
				contact.PostalCode = '123456';
				contact.ExternalId = 'Test_Contact_Id';
				contact.AccountSAPId = '0000000';				
				request.contacts = new List<SynchronizationService_Payload.SyncContact>{contact};
		
				//Case
				Case caseRecord = new Case();
				caseRecord.External_Id__c = 'Test_Case_Id';
				caseRecord.Status = 'In Process';
				request.caseRecord = caseRecord;
				request.accountId = '0000000';		
				request.contactId = 'Test_Contact_Id';
				request.recordType = 'New_Case';
				
				ba_SynchronizationService_Inbound.SynchronizationServiceResponse inboundResp = new ba_SynchronizationService_Inbound.SynchronizationServiceResponse();
				inboundResp.recordInformation = JSON.serialize(request);
				
				respBody = JSON.serialize(inboundResp);			
            }
            
            resp.setBody(respBody);
            
            return resp;
        }
	}
	
	public class CalloutMock_err implements HttpCalloutMock {

        public HTTPResponse respond(HTTPRequest req) {
            
            HttpResponse resp = new HttpResponse();
                       
            if(req.getEndpoint().endsWith('services/oauth2/token')){
            	
            	resp.setStatusCode(200);
            	resp.setStatus('Complete');
            	
            	String respBody =  '{' +
					            '"id" : "1",' +
					            '"issued_at" : "1",' +
					            '"instance_url" : "1",' +
					            '"signature" : "1",' +
					            '"access_token" : "1",' +
					            '"token_type" : "1"' +
				            '}';
				resp.setBody(respBody);
				            
            }else{
            	
            	resp.setStatusCode(400);
            	resp.setStatus('Complete');		
            }
                        
            return resp;
        }
	}
}