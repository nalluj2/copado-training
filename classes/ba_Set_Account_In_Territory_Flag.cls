global class ba_Set_Account_In_Territory_Flag implements Schedulable, Database.Batchable<sObject> {
	
	@TestVisible
  	private List<Account> accounts;
	
	global void execute(SchedulableContext ctx){
        
		Database.executeBatch(new ba_Set_Account_In_Territory_Flag(), 2000);
  	}

  	global Database.QueryLocator start(Database.BatchableContext ctx){
    
	    String query = 'SELECT Id ';        
	    query += 'FROM Account';
	   	
	    if(accounts != null) query += ' WHERE Id IN :accounts ';
	        
	    return Database.getQueryLocator(query);
  	}

  	global void execute(Database.BatchableContext ctx, List<Account> records){
  		
  		List<Account> toUpdate = new List<Account>();
  		
  		for(Account account : [Select Id, Territory_Assigned__c, (Select Id from ObjectTerritory2Associations where Territory2.Territory2Type.DeveloperName IN ('Territory', 'SalesForce', 'District', 'Country', 'Business_Unit') LIMIT 1) from Account where Id IN :records]){
  			
  			Boolean hasTerritory = false;  			
  			if(account.ObjectTerritory2Associations.size() > 0) hasTerritory = true;
  			
  			if(account.Territory_Assigned__c != hasTerritory){
  				
  				account.Territory_Assigned__c = hasTerritory;
  				toUpdate.add(account);  				
  			}
  		}
  		
  		if(toUpdate.size() > 0) update toUpdate;
  	}
  	
  	global void finish(Database.BatchableContext BC){
  		
  	}    
}