@isTest
private class Test_tr_Contract_Repository_Team {
    
    private static testmethod void testContractTeamSharing(){
        
        Account testAcc = new Account();
        testAcc.Name = 'Test Account';
        testAcc.Account_Country_vs__c = 'BELGIUM';
        insert testAcc;
        
        Contract_Repository__c contract = new Contract_Repository__c();
        contract.Account__c = testAcc.Id;
        contract.Primary_Contract_Type__c = 'Rebate agreement';
        contract.Rebate_Type__c = 'Straight Unit';
        //contract.Payment_Options__c = 'Credit Note';
        contract.put('Payment_Options__c','Credit Note');
        contract.Pre_Payment_Terms__c = 'Final Payment';
        contract.Payment_Period__c = 'Monthly'; 
        contract.Original_Valid_From_Date__c = Date.today();
        contract.Original_Valid_To_Date__c = Date.today().addYears(1);
        insert contract;
        
        Test.startTest();
        
        List<User> otherUsers = [Select Id from User where Profile.Name IN ('System Administrator', 'System Administrator MDT') AND isActive = true and Id != :UserInfo.getUserId() LIMIT 4];
        
        Contract_Repository_Team__c contractTeam1 = new Contract_Repository_Team__c();
        contractTeam1.Contract_Repository__c = contract.Id;
        contractTeam1.Team_Member__c = otherUsers[0].Id;
        contractTeam1.Access_Level__c = 'None';
        
        Contract_Repository_Team__c contractTeam2 = new Contract_Repository_Team__c();
        contractTeam2.Contract_Repository__c = contract.Id;
        contractTeam2.Team_Member__c = otherUsers[1].Id;
        contractTeam2.Access_Level__c = 'Read Only - No Access to Documents';
        
        Contract_Repository_Team__c contractTeam3 = new Contract_Repository_Team__c();
        contractTeam3.Contract_Repository__c = contract.Id;
        contractTeam3.Team_Member__c = otherUsers[2].Id;
        contractTeam3.Access_Level__c = 'Read Only - Access to Documents';
        
        Contract_Repository_Team__c contractTeam4 = new Contract_Repository_Team__c();
        contractTeam4.Contract_Repository__c = contract.Id;
        contractTeam4.Team_Member__c = otherUsers[3].Id;
        contractTeam4.Access_Level__c = 'Read/Write';
               
        insert new List<Contract_Repository_Team__c>{contractTeam1, contractTeam2, contractTeam3, contractTeam4};
        
        List<Contract_Repository__Share> share1 = [Select AccessLevel from Contract_Repository__Share where ParentId = :contract.Id AND UserOrGroupId = :otherUsers[0].Id and RowCause = 'Contract_Team_Member__c'];
        System.assert(share1.size() == 0);
        
        List<Contract_Repository__Share> share2 = [Select AccessLevel from Contract_Repository__Share where ParentId = :contract.Id AND UserOrGroupId = :otherUsers[1].Id and RowCause = 'Contract_Team_Member__c'];
        System.assert(share2.size() == 1);
        System.assert(share2[0].AccessLevel == 'Read');
        
        List<Contract_Repository__Share> share3 = [Select AccessLevel from Contract_Repository__Share where ParentId = :contract.Id AND UserOrGroupId = :otherUsers[2].Id and RowCause = 'Contract_Team_Member__c'];
        System.assert(share3.size() == 1);
        System.assert(share3[0].AccessLevel == 'Read');
        
        List<Contract_Repository__Share> share4 = [Select AccessLevel from Contract_Repository__Share where ParentId = :contract.Id AND UserOrGroupId = :otherUsers[3].Id and RowCause = 'Contract_Team_Member__c'];
        System.assert(share4.size() == 1);
        System.assert(share4[0].AccessLevel == 'Edit');
               
        contractTeam1.Access_Level__c = 'Read/Write';
        update contractTeam1;
        
        share1 = [Select AccessLevel from Contract_Repository__Share where ParentId = :contract.Id AND UserOrGroupId = :otherUsers[0].Id and RowCause = 'Contract_Team_Member__c'];
        System.assert(share1.size() == 1);
        System.assert(share1[0].AccessLevel == 'Edit');
        
        contractTeam4.Access_Level__c = 'Read Only - Access to Documents';
        update contractTeam4;
        
        share4 = [Select AccessLevel from Contract_Repository__Share where ParentId = :contract.Id AND UserOrGroupId = :otherUsers[3].Id and RowCause = 'Contract_Team_Member__c'];
        System.assert(share4.size() == 1);
        System.assert(share4[0].AccessLevel == 'Read');
        
        contractTeam4.Access_Level__c = 'Read Only - Upload Access to Documents';
        update contractTeam4;
        share4 = [Select AccessLevel from Contract_Repository__Share where ParentId = :contract.Id AND UserOrGroupId = :otherUsers[3].Id and RowCause = 'Contract_Team_Member__c'];
        System.assert(share4.size() == 1);
        System.assert(share4[0].AccessLevel == 'Read');
        
        delete contractTeam3;
        
        share3 = [Select AccessLevel from Contract_Repository__Share where ParentId = :contract.Id AND UserOrGroupId = :otherUsers[2].Id and RowCause = 'Contract_Team_Member__c'];
        System.assert(share3.size() == 0);
        
        undelete contractTeam3;
        
        share3 = [Select AccessLevel from Contract_Repository__Share where ParentId = :contract.Id AND UserOrGroupId = :otherUsers[2].Id and RowCause = 'Contract_Team_Member__c'];
        System.assert(share3.size() == 1);
        System.assert(share3[0].AccessLevel == 'Read');
        
    }    
}