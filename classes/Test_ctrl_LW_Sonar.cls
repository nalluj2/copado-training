@isTest (seeAllData = true)
private class Test_ctrl_LW_Sonar {
	
	private static testmethod void testGeographyLevel(){
		
		Territory2 EUR = [Select Id from Territory2 where name='EUR (R)'];
		
		User testUser = [Select Id from User where isActive = true AND Id IN (Select UserId from UserTerritory2Association where Territory2Id =: EUR.Id) LIMIT 1];
		
		Company__c europe = [Select Id from Company__c where Name = 'Europe'];
		
		Map<String, Id> testData = generateTestData(europe.Id, testUser.Id);
		
		System.runAs(testUser){
			
			ctrl_LW_Sonar controller = new ctrl_LW_Sonar();
			
			controller.level = 'geography';
			controller.levelSelected();
			
			System.assert(controller.countryOptions.size()>0);
			System.assert(controller.BUOptions.size()>0);
			System.assert(controller.sBUOptions.size()>0);
			System.assert(controller.TherapyOptions.size()>0);
			
			controller.generateLwURL();
			
			controller.countryFilter.add(controller.countryOptions[0].getValue());
			controller.countrySelected();
			
			controller.buFilter.add(testData.get('BU'));
			controller.businessUnitSelected();
			
			System.assert(controller.sBUOptions.size()==1);
			
			controller.sbuFilter.add(testData.get('SBU'));
			controller.subbusinessUnitSelected();
			
			System.assert(controller.TherapyOptions.size()==1);
			
			controller.therapyFilter.add(controller.therapyOptions[0].getValue());
			
			controller.generateLwURL();
			
			//Test save and load configuration
			controller.saveConfig();
			controller = new ctrl_LW_Sonar();
			
			System.assert(controller.level == 'geography');
		}
	}
	
	private static testmethod void testProductLevel(){
		
		Territory2 EUR = [Select Id from Territory2 where name='EUR (R)'];
		
		User testUser = [SELECT Id FROM User WHERE Id = '005w00000056mdu'];
		
		Company__c europe = [Select Id from Company__c where Name = 'Europe'];
		
		Map<String, Id> testData = generateTestData(europe.Id, testUser.Id);
		
		System.runAs(testUser){
			
			ctrl_LW_Sonar controller = new ctrl_LW_Sonar();
			
			controller.level = 'therapy';
			controller.levelSelected();
			
			System.assert(controller.countryOptions.size()>0);
			System.assert(controller.districtOptions.size()>0);
			System.assert(controller.territoryOptions.size()>0);
			System.assert(controller.therapyOptions.size()>0);
			
			controller.generateLwURL();
			
			controller.countryFilter.add(controller.countryOptions[0].getValue());
			System.debug('**BC** controller.countryOptions[0].getValue() : ' + controller.countryOptions[0].getValue());
			controller.countrySelected();
			
			System.assert(controller.districtOptions.size()>0);
			
			controller.districtFilter.add(controller.districtOptions[0].getValue());
			controller.districtSelected();
						
			if(controller.territoryOptions.size()>0){
				
				controller.territoryFilter.add(controller.territoryOptions[0].getValue());
				controller.territorySelected();								
			}else{	
				controller.territoryFilter.add('territoryId1');
				controller.territoryFilter.add('territoryId2');
			}
			
			controller.therapyFilter.add('therapyId1');
			controller.therapyFilter.add('therapyId2');	
					
			controller.generateLwURL();
			
			controller.clearConfig();
		}
	}
	
	private static testmethod void testSegmentLevel(){
		
		Territory2 EUR = [Select Id from Territory2 where name='EUR (R)'];
	
		User testUser = [SELECT Id FROM User WHERE Id = '005w00000056mdu'];
		
		Company__c europe = [Select Id from Company__c where Name = 'Europe'];
		
		Map<String, Id> testData = generateTestData(europe.Id, testUser.Id);
		
		System.runAs(testUser){
			
			ctrl_LW_Sonar controller = new ctrl_LW_Sonar();
			
			controller.level = 'segment';
			controller.levelSelected();
			
			System.assert(controller.countryOptions.size()>0);			
			System.assert(controller.districtOptions.size()>0);
			System.assert(controller.territoryOptions.size()>0);
			System.assert(controller.therapyOptions.size()>0);
			System.assert(controller.segmentOptions.size()>0);
						
			controller.generateLwURL();
			
			controller.countryFilter.add(controller.countryOptions[0].getValue());
			controller.countrySelected();
								
			System.assert(controller.districtOptions.size()>0);
			
			controller.districtFilter.add(controller.districtOptions[0].getValue());
			controller.districtSelected();
			
			if(controller.territoryOptions.size()>0){
				
				controller.territoryFilter.add(controller.territoryOptions[0].getValue());
				controller.territorySelected();								
			}else{	
				controller.territoryFilter.add('territoryId1');
				controller.territoryFilter.add('territoryId2');
			}
			
			controller.therapyFilter.add('therapyId1');
			controller.therapyFilter.add('therapyId2');		
						
			controller.segmentFilter.add('Develop');
			
			controller.generateLwURL();
		}
	}
		
	public static Map<String, Id> generateTestData(Id europeId, Id userId){
				
		Map<String, Id> testData = new Map<String, Id>();
				
		//Business Unit		
		Business_Unit__c bu =  new Business_Unit__c();
        bu.Company__c =europeId;
        bu.name='Unit test business unit';                
        insert bu;
        
        testData.put('BU', bu.Id);
        
        //Sub-Business Unit
        Sub_Business_Units__c sbu = new Sub_Business_Units__c();
        sbu.name='Unit test sub business unit';
        sbu.Business_Unit__c=bu.id;        
		insert sbu;
		
		testData.put('SBU', sbu.Id);
		
		//User Sub-Business Unit		
		User_Business_Unit__c uSBU = new User_Business_Unit__c();
		uSBU.User__c = userId;
		uSBU.Primary__c = false;
		uSBU.Sub_Business_Unit__c = sbu.Id;
		insert uSBU;
		
		//Therapy Group
		Therapy_Group__c therGroup = new Therapy_Group__c();
		therGroup.Name = 'Test Therapy Group';
		therGroup.Company__c = europeId;
		therGroup.Sub_Business_Unit__c = sbu.Id;
		insert therGroup;
		
		//Therapy
		Therapy__c therapy = new Therapy__c();
		therapy.Active__c = true;
		therapy.Business_Unit__c = bu.Id;
		therapy.Name = 'Test Therapy';
		therapy.Sub_Business_Unit__c = sbu.Id;
		therapy.Therapy_Group__c = therGroup.Id;
		therapy.Therapy_Name_Hidden__c = 'whatever';
		insert therapy;
		
		return testData;	
	}
}