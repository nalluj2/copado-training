//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 11-04-2019
//  Description      : APEX Test Class to test the logic in bl_Note_Trigger
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_bl_Note_Trigger {

	//----------------------------------------------------------------------------------------
    // Test related to MITG Attachments
    //----------------------------------------------------------------------------------------
	@isTest static void test_Note_MITG(){

		// CREATE TEST DATA
		User oUser_Admin = clsTestData_User.createUser_SystemAdministratorMDT('tstadm01', true);
		User oUser_MITG = clsTestData_User.createUser('tstmitg1', false);
        
        	//PMT-22787
        	//New profile EMEA Field Force MITG will replace profiles: EUR Field Force MITG and MEA Field Force MITG
        	//oUser_MITG.ProfileId = clsUtil.getUserProfileId('EUR Field Force MITG');
			oUser_MITG.ProfileId = clsUtil.getUserProfileId('EMEA Field Force MITG');
        
		insert oUser_MITG;

		System.runAs(oUser_MITG){
		
			clsTestData_Account.createAccount(false);
				clsTestData_Account.oMain_Account.Name = 'TEST Account MITG';
			insert clsTestData_Account.oMain_Account;

            //PMT-22787
            //New record type for opportunity EMEA MITG Standard Opportunity will replace record types: EUR MITG Standard Opportunity and MEA MITG Standard Opportunity
			//clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'EUR_MITG_Standard_Opportunity').Id;
            clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'EMEA_MITG_Standard_Opportunity').Id;
			clsTestData_Opportunity.createOpportunity();
		
		}

		System.runAs(oUser_Admin){

			List<Note> lstNote = new List<Note>();
				Note oNote_Account = new Note();
					oNote_Account.ParentId = clsTestData_Account.oMain_Account.Id;
					oNote_Account.Title = 'TEST Note';
					oNote_Account.Body = 'TEST BODY';
				lstNote.add(oNote_Account);
				Note oNote_Opportunity = new Note();
					oNote_Opportunity.ParentId = clsTestData_Opportunity.oMain_Opportunity.Id;
					oNote_Opportunity.Title = 'TEST Attachment';
					oNote_Opportunity.Body = 'TEST BODY';
				lstNote.add(oNote_Opportunity);
			insert lstNote;

		}

		System.runAs(oUser_Admin){

			List<Account> lstAccount = [SELECT Id, Linked_Notes__c FROM Account WHERE Name = 'TEST Account MITG'];
			System.assertEquals(lstAccount.size(), 1);
			System.assertEquals(lstAccount[0].Linked_Notes__c, false);

			List<Opportunity> lstOpportunity = [SELECT Id, Linked_Notes__c FROM Opportunity];
			System.assertEquals(lstOpportunity.size(), 1);
			System.assertEquals(lstOpportunity[0].Linked_Notes__c, false);

		}

		// TEST LOGIC
		Test.startTest();

		System.runAs(oUser_MITG){

			List<Note> lstNote = new List<Note>();
				Note oNote_Account = new Note();
					oNote_Account.ParentId = clsTestData_Account.oMain_Account.Id;
					oNote_Account.Title = 'TEST Note';
					oNote_Account.Body = 'TEST BODY';
				lstNote.add(oNote_Account);
				Note oNote_Opportunity = new Note();
					oNote_Opportunity.ParentId = clsTestData_Opportunity.oMain_Opportunity.Id;
					oNote_Opportunity.Title = 'TEST Attachment';
					oNote_Opportunity.Body = 'TEST BODY';
				lstNote.add(oNote_Opportunity);
			insert lstNote;

			List<Account> lstAccount = [SELECT Id, Linked_Notes__c FROM Account WHERE Name = 'TEST Account MITG'];
			System.assertEquals(lstAccount[0].Linked_Notes__c, true);

			List<Opportunity> lstOpportunity = [SELECT Id, Linked_Notes__c FROM Opportunity];
			System.assertEquals(lstOpportunity[0].Linked_Notes__c, true);


			delete lstNote;

			lstAccount = [SELECT Id, Linked_Notes__c FROM Account WHERE Name = 'TEST Account MITG'];
			System.assertEquals(lstAccount[0].Linked_Notes__c, false);

			lstOpportunity = [SELECT Id, Linked_Notes__c FROM Opportunity];
			System.assertEquals(lstOpportunity[0].Linked_Notes__c, false);


			undelete lstNote;

			lstAccount = [SELECT Id, Linked_Notes__c FROM Account WHERE Name = 'TEST Account MITG'];
			System.assertEquals(lstAccount[0].Linked_Notes__c, true);

			lstOpportunity = [SELECT Id, Linked_Notes__c FROM Opportunity];
			System.assertEquals(lstOpportunity[0].Linked_Notes__c, true);
		}

		Test.stopTest();

		// VALIDATE RESULT

	}
    //----------------------------------------------------------------------------------------

}
//--------------------------------------------------------------------------------------------------------------------