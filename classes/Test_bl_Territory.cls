/*
 *      Created Date : 20121001
 *      Description : This class tests the logic in the bl_Territory class
 *
 *      Author = Rudy De Coninck
 */
@IsTest(seeAllData=true)
public class Test_bl_Territory { 
	
	public static testmethod void testGetTerritoryForAccount(){
				
		Set<Id> accountIds = new Set<Id>();		
		for(ObjectTerritory2Association ashare : [select ObjectId, Territory2ID from ObjectTerritory2Association where ObjectId IN (Select Id from Account where Account_Country_vs__c = 'BELGIUM' and SAP_Customer_Group__c = 'HOSPITAL/ MEDICAL/CL' and Vascular__c=true and SAP_Active__c = true) limit 30]){
			accountIds.add(ashare.ObjectId);	
		}			
		
		bl_Territory.getTerritoryForAccounts(accountIds);	
	}
}