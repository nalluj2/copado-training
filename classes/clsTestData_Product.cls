@isTest public class clsTestData_Product {
	
	public static Integer iRecord_Product = 1;
	public static Id idRecordType_Product;
	public static Product2 oMain_Product;
	public static List<Product2> lstProduct;

	public static List<Product_Group__c> lstProductGroup = new List<Product_Group__c>();
	public static Map<Id, Product_Group__c> mapProductGroup_Main = new Map<Id, Product_Group__c>();
	public static Product_Group__c oMain_ProductGroup;
    public static Boolean bCanUseRevenueSchedule = true;

    public static Map<String, String> mapProductMPGCode = new Map<String, String>();

    //---------------------------------------------------------------------------------------------------
    // Create Product Data
    //---------------------------------------------------------------------------------------------------
    public static List<Product2> createProduct(){
        return createProduct(true);
    }
    public static List<Product2> createProduct(Boolean bInsert){

        if (oMain_Product == null){

            if (clsTestData_MasterData.oMain_BusinessUnit == null) clsTestData_MasterData.createBusinessUnit(true);

            lstProduct = new List<Product2>();

            for (Integer i=0; i<iRecord_Product; i++){
                for (Business_Unit__c oBU : clsTestData_MasterData.lstBusinessUnit){
                    Product2 oProduct = new Product2();
                        oProduct.Name = 'TEST Product ' + oBU.Name + ' ' + i;
                        oProduct.ProductCode = 'TEST_Product_' + oBU.Name + '_' + i;
                        oProduct.Business_Unit_ID__c = oBU.Id;
                        if (idRecordType_Product != null){
//                            oProduct.Record_Type_Text__c = idRecordType_Product;
                            oProduct.RecordTypeId = idRecordType_Product;
                        }
                        oProduct.canUseRevenueSchedule = bCanUseRevenueSchedule;
                    lstProduct.add(oProduct);
                }
            }
            if (bInsert){
                insert lstProduct; 
            }
            oMain_Product = lstProduct[0];           
        }

        return lstProduct;
    }

    public static List<Product2> createProductData_NoBU(){
        return createProductData_NoBU(true);
    }
    public static List<Product2> createProductData_NoBU(Boolean bInsert){

        if (oMain_Product == null){

        	if (oMain_ProductGroup == null) createProductGroup();
        	
            lstProduct = new List<Product2>();

            for (Integer i=0; i<iRecord_Product; i++){
                Product2 oProduct = new Product2();
                    oProduct.Name = 'TEST Product ' + i;
                    oProduct.ProductCode = 'TEST_Product_' + i;
                    if (idRecordType_Product != null){
                        oProduct.RecordTypeId = idRecordType_Product;
                    }
                    oProduct.canUseRevenueSchedule = bCanUseRevenueSchedule;
                lstProduct.add(oProduct);
            }
            if (bInsert){
                insert lstProduct; 
            }
            oMain_Product = lstProduct[0];           
        }

        return lstProduct;
    }

    public static List<Product2> createProduct_MITG(){
        return createProduct_MITG(true);
    }
    public static List<Product2> createProduct_MITG(Boolean bInsert){

        if (oMain_Product == null){

            if (mapProductMPGCode.size() == 0){
                String tData = ' ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                List<String> lstData = tData.split('');
                lstData.remove(0);
                for (Integer i=0; i < lstData.size() - 1; i++){
                    mapProductMPGCode.put(lstData[i], lstData[i+1]);
                }
            }

            lstProduct = new List<Product2>();

            String tMPGCode = (new List<String>(mapProductMPGCode.keySet()))[0];
            for (Integer i=0; i<iRecord_Product; i++){
                Product2 oProduct = new Product2();
                    oProduct.Name = 'TEST Product ' + i;
                    oProduct.ProductCode = 'TEST_Product_' + i;
                    if (mapProductMPGCode.containsKey(tMPGCode)){
                        tMPGCode = mapProductMPGCode.get(tMPGCode);        
                        oProduct.MPG_Code_Text__c = tMPGCode;
                    }
                    oProduct.canUseRevenueSchedule = bCanUseRevenueSchedule;
                lstProduct.add(oProduct);
            }
            if (bInsert){
                insert lstProduct; 
            }
            oMain_Product = lstProduct[0];           
        }

        return lstProduct;
    }    	
    //---------------------------------------------------------------------------------------------------


    //---------------------------------------------------------------------------------------------------
    // Create Product Group Data
    //---------------------------------------------------------------------------------------------------
    public static List<Product_Group__c> createProductGroup(){
        return createProductGroup(true);
    }
    public static List<Product_Group__c> createProductGroup(Boolean bInsert){
    
    	if (oMain_ProductGroup == null){

    		if (clsTestData_Therapy.oMain_Therapy == null) clsTestData_Therapy.createTherapy();

    		lstProductGroup = new List<Product_Group__c>();

    		Product_Group__c oProductGroup = new Product_Group__c();
		        oProductGroup.Name = 'TEST Product Group';
		        oProductGroup.Therapy_ID__c = clsTestData_Therapy.oMain_Therapy.Id;
	        lstProductGroup.add(oProductGroup);

	    	if (bInsert){
	    		insert lstProductGroup;
	    		mapProductGroup_Main.putAll(lstProductGroup);
	    	}

            oMain_ProductGroup = lstProductGroup[0];

    	}

    	return lstProductGroup;

    }
    //---------------------------------------------------------------------------------------------------

}