//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 25-02-2016
//  Description      : APEX Class - Business Logic for tr_SVMXC_RMAShipmentOrder
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public class bl_SVMXC_RMAShipmentOrder_Trigger {

	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// Public variables coming from the trigger
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public static List<SVMXC__RMA_Shipment_Order__c> lstTriggerNew = new List<SVMXC__RMA_Shipment_Order__c>();
	public static Map<Id, SVMXC__RMA_Shipment_Order__c> mapTriggerNew = new Map<Id, SVMXC__RMA_Shipment_Order__c>();
	public static Map<Id, SVMXC__RMA_Shipment_Order__c> mapTriggerOld = new Map<Id, SVMXC__RMA_Shipment_Order__c>();
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	//------------------------------------------------------------------------------------------------
	// Update Account_Country__c on RMA Shipment Order with the Country of the Account
	//------------------------------------------------------------------------------------------------
	public static void copyAccountCountry(String tAction){

		List<SVMXC__RMA_Shipment_Order__c> lstRMAShipmentOrder = new List<SVMXC__RMA_Shipment_Order__c>();
		Set<Id> setID_Account = new Set<Id>();

		for (SVMXC__RMA_Shipment_Order__c oData : lstTriggerNew){

    		if (tAction == 'INSERT'){

        		lstRMAShipmentOrder.add(oData);
        		if (oData.SVMXC__Company__c != null){
    	    		setID_Account.add(oData.SVMXC__Company__c);
    	    	}

    		}else if (tAction == 'UPDATE'){

    			SVMXC__RMA_Shipment_Order__c oData_Old = mapTriggerOld.get(oData.Id);

    			if (oData.SVMXC__Company__c != oData_Old.SVMXC__Company__c){
	        		lstRMAShipmentOrder.add(oData);
	        		if (oData.SVMXC__Company__c != null){
    	    			setID_Account.add(oData.SVMXC__Company__c);
    	    		}
    			}

    		}

        }

        // Get the Account_Country_vs__c of the collected Account ID's
        Map<Id, Account> mapAccount = new Map<Id, Account>([SELECT Id, Account_Country_vs__c FROM Account WHERE Id = :setID_Account]);

        for (SVMXC__RMA_Shipment_Order__c oRMAShipmentOrder : lstRMAShipmentOrder){

        	if (oRMAShipmentOrder.SVMXC__Company__c == null){

        		// If the processing record doesn't have a related Account, clear the Account_Country__c on the processing record
        		oRMAShipmentOrder.Account_Country__c = null;

        	}else if (mapAccount.containsKey(oRMAShipmentOrder.SVMXC__Company__c)){

        		oRMAShipmentOrder.Account_Country__c = mapAccount.get(oRMAShipmentOrder.SVMXC__Company__c).Account_Country_vs__c;

        	}
        
        }

	}
	//------------------------------------------------------------------------------------------------



}