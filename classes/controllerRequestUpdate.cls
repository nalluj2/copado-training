/**
 * Creation Date :  20090319
 * Description : 	Apex Controller called by the page 'requestUpdate.page' as an extension of the standard controller 'Contact'!
 * Author : 		ABSI - MC
 */
public class controllerRequestUpdate {
	
	public string contactId = ApexPages.currentPage().getParameters().get('id');
	public Contact contToUpdate ;
	public Boolean CanCloseWindow = false ; 
	public controllerRequestUpdate(ApexPages.StandardController stdController) {
        	setContact((Contact)stdController.getRecord());  
        	contToUpdate.Contact_Update_Reason__c = null ;           	
	}	
	public Contact getContact(){ 				return this.contToUpdate	; 	}
	public String getContactId(){				return this.contactId       ;   }
	public void setContact(Contact con){ 		this.contToUpdate = con 	;	}
    public void setCanCloseWindow(Boolean st){	this.CanCloseWindow = st 	;	}
    public Boolean getCanCloseWindow(){			return this.CanCloseWindow  ;	}
    
    /**
 	 *	Description: Method called when the user press 'Send' in the VisualForce Page ! 
     * 
     */
     
    public void Send(){
    	Contact co = getContact() ;
    	Boolean isError = false ; 
        if (co.Contact_Update_Reason__c == null){
        	ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'The Reason is required ');
            ApexPages.addmessage(myMsg);
        }else{
	        try{
	        	co.Contact_HIDDEN_isUpdateRequested__c = true ;     	
	            update co ;
	        }
	        catch (DmlException e){
	          ApexPages.addMessages(e);
	          isError  = true ;
	        } 
	    	if (isError){
	    	 	ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Could not send. Please contact your administrator');
	            ApexPages.addmessage(myMsg);
	    	}else{
	    	 	ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info, 'Update Request Sent !');
	            ApexPages.addmessage(myMsg);
	            CanCloseWindow = true ; 
	    	}
   		}  
    }
}