/**
 *      Created Date : 20130222
 *      Description : This class is used as a business logic class for tr_UpdateProductsOnCallRecord trigger on call topic subjects object.
 * 
 *      Author = Kaushal
 */
public class bl_tr_UpdateProductsOnCallRecord{

    public static void UpdateProductsConcatenated(set<id> CTPIds){
    
        map<id,string> mapProductidWithName=new map<id,string>();
        map<id,set<id>> mapCRidWithProductid=new map<id,set<id>>();
        
        Set<Id> callRecordIds = new Set<Id>();
        
        for(Call_Topic_Products__c ctp : [Select Call_Topic__r.Call_Records__c from Call_Topic_Products__c where Id IN :CTPIds]){
        	
        	callRecordIds.add(ctp.Call_Topic__r.Call_Records__c);
        }
                                                   
        for(Call_Topic_Products__c ctp : [Select Id, Call_Topic__r.Call_Records__c, Product__c, Product__r.Name from Call_Topic_Products__c where Call_Topic__r.Call_Records__c IN :callRecordIds ORDER BY Product__r.Name]){
        	
            mapProductidWithName.put(ctp.Product__c,ctp.Product__r.Name);
            
            Set<Id> setTempid = mapCRidWithProductid.get(ctp.Call_Topic__r.Call_Records__c);
            if(setTempid==null){
                setTempid=new set<id>();
                setTempid.add(ctp.Product__c);
                mapCRidWithProductid.put(ctp.Call_Topic__r.Call_Records__c,setTempid);
            }else{
                setTempid.add(ctp.Product__c);
                mapCRidWithProductid.put(ctp.Call_Topic__r.Call_Records__c,setTempid);
            }
        }
        
        List<Call_Records__c> lstCR=new List<Call_Records__c>();
        
        for(id crid:mapCRidWithProductid.keyset()){
            Call_Records__c CR = new Call_Records__c(id=crid);
            
            String strProdName='';
            
            for(id  prodid : mapCRidWithProductid.get(crid)){
                if(strProdName==null || strProdName==''){
                    strProdName=mapProductidWithName.get(prodid);
                }else{
                    strProdName=strProdName+';'+mapProductidWithName.get(prodid);
                }
            }
            
            CR.Products_Concatenated__c=strProdName;
            lstCR.add(CR);
        }
        if(lstCR.size()>0){
            update lstCR;  
        }
    
    }

}