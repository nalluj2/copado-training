/**
 * Email services are automated processes that use Apex classes
 * to process the contents, headers, and attachments of inbound
 * email.
 */
global class es_Procedural_Solutions_Kits implements Messaging.InboundEmailHandler {

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
                
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
    	result.success = true;
    		
		//Aumomatic email from SAP
		if(email.fromAddress.equalsIgnoreCase('Covidien.Intranet@Covidien.com') || (email.subject != null && email.subject.containsIgnoreCase('A New BOX Deliverable has been Released'))){
    		
    		try{	
    			
    			String boxId;    			
    			if(email.plainTextBody != null){
    				
    				boxId = email.plainTextBody.substringBetween('New Deliverable Item Code :  ', '\n');    				
					if(boxId == null || boxId.isWhitespace()) boxId = email.plainTextBody.substringBetween('New Deliverable Item Code :  ', ' ');  
					if(boxId == null || boxId.isWhitespace()) boxId = email.plainTextBody.substringAfter('New Deliverable Item Code :  ');
    			}
				   			    			    			
    			if(boxId == null || boxId.isWhitespace()) throw new IllegalArgumentException('Could not find a Box identifier.');
    			
    			//Support to old Box Id format
    			String boxNameId = boxId;
    			if(boxNameId.startsWith('PST')) boxNameId = 'PST-' + boxNameId.subStringAfter('PST');
    			
    			List<Procedural_Solutions_Kit__c> boxSearch = [Select Id, Status__c from Procedural_Solutions_Kit__c where Name = :boxNameId.trim() OR Customer_Reference__c = :boxId.trim()];
		    	if(boxSearch.size() == 0) throw new IllegalArgumentException('No Box found for identifier \'' + boxId + '\'');
		    	else if(boxSearch.size() > 1) throw new IllegalArgumentException('Multipe Boxes found for identifier \'' + boxId + '\'. Found ' + boxSearch.size());
		    			    		    		
		    	Procedural_Solutions_Kit__c box = boxSearch[0];
		    			
		    	box.Status__c = 'Available';
		    	update box;
		    	    			
    		}catch(Exception e){
    			
    			List<String> PS_EMEA_Emails = new List<String>();
				for(Procedural_Solutions_Kit_Email__c PS_Email_setting : Procedural_Solutions_Kit_Email__c.getall().values()) PS_EMEA_Emails.add(PS_Email_setting.email__c);
				
				Messaging.SingleEmailMessage PSEMEA_email = new Messaging.SingleEmailMessage();    			    			
				PSEMEA_email.setSubject('Error processing automated email for Box availability');
	    		PSEMEA_email.setHtmlBody('There was an error processing an automated email from downstream applications. The error message is:<br/><br/>' + e.getMessage() + '<br/><br/>The message recieved was:<br/><br/>' + email.plainTextBody);
	    		PSEMEA_email.setToAddresses(PS_EMEA_Emails); 
	    		PSEMEA_email.setSaveAsActivity(false);	
				Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{PSEMEA_email});    
    		} 
    			
		//Approval / Rejection email	
		}else{
    		
			try{
    		
	    		if(email.subject == null || email.subject.contains('/') == false ) throw new IllegalArgumentException('Missing or invalid Subject content. Expected values are \'Approve\' or \'Reject\' followed by \'/\' and the Box identifier.');
	    		
	    		List<String> parts = email.subject.split('/');
	    		
	    		String action = parts[0].trim();
	    		String boxId = parts[1].trim();
	    		
	    		List<Procedural_Solutions_Kit__c> boxSearch = [Select Id, Status__c from Procedural_Solutions_Kit__c where Name = :boxId];
	    		if(boxSearch.size() == 0) throw new IllegalArgumentException('No Box found for identifier \'' + boxId + '\'');
	    		else if(boxSearch.size() > 1) throw new IllegalArgumentException('Multipe Boxes found for identifier \'' + boxId + '\'. Found ' + boxSearch.size());
	    		    		
	    		Procedural_Solutions_Kit__c box = boxSearch[0];
	    		
	    		if(action == 'Approve'){
	    			
	    			if(box.Status__c != 'Approval Pending')  throw new IllegalArgumentException('Current Box status does not allow this operation. Current status is \'' + box.Status__c + '\'');
	    			
	    			box.Status__c = 'Approved';
	    			update box;
	    			
	    		}else if(action =='Reject'){
	    			
	    			if(box.Status__c != 'Approval Pending' && box.Status__c != 'Approved') throw new IllegalArgumentException('Current Box status does not allow this operation. Current status is \'' + box.Status__c + '\'');
	    			
	    			String rejectionReason;
	    			
	    			if(email.plainTextBody != null) {
	    				
	    				rejectionReason = email.plainTextBody.substringBetween('<==========' , '==========>');
	    			}
	    			
	    			if(rejectionReason == null || rejectionReason.isWhitespace()) throw new IllegalArgumentException('Could not find a Rejection Reason. Please provide a reason in the email body in the indicated section between the special markers.');
	    			
	    			box.Status__c = 'Open';
	    			box.Rejection_Reason__c = rejectionReason.trim();
	    			update box;
	    			
	    		}else throw new IllegalArgumentException('Unknown action \'' + action + '\'');
	    			    		
    		}catch(Exception e){
    		
    			result.success = false;
    			result.message = e.getMessage();
    		}
    	}	
    		        
        return result;
    }    
}