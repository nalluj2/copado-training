/*
*	Trigger to log user actions on Account (create / update/ delete)
* 	Author: Rudy De Coninck
*	Creation date: 17-12-2014
*/


public class bl_UserAction {

	//static variables to log only initial actions
	public static boolean userActionInProgress=false; 
	
	public static boolean contactActionPlan2Operation = false;
	
	public static boolean childAccountPlanOperation = false;

	public static void logObject(ID userId, String operation, String objectName, String internalId, String externalId){
		
		User_Action__c ua = new User_Action__c();
		ua.Datetime__c =  Datetime.now();
		ua.Date__c = Date.today();
		ua.External_ID__c = externalId;
		ua.Internal_ID__c = internalId;
		ua.object__c = objectName;
		ua.Type__c = operation;
		ua.User__c = userId;
		insert ua;
		
	}
	
	public static void logObjectWithParent(ID userId, String operation, String objectName, String internalId, String externalId, 
		String parentId, String parentObjectName){
		
		User_Action__c ua = new User_Action__c();
		ua.Datetime__c =  Datetime.now();
		ua.Date__c = Date.today();
		ua.External_ID__c = externalId;
		ua.Internal_ID__c = internalId;
		ua.object__c = objectName;
		ua.Type__c = operation;
		ua.User__c = userId;
		ua.Parent_Object_Id__c = parentId;
		ua.Parent_Object_Name__c = parentObjectName;
		insert ua;
		
	}

}