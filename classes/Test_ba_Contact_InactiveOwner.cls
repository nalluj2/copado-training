/*
 * Author 		:	Bart Caelen
 * Date			:	20140724
 * Description	:	CR-2603 - Test Class for the APEX Class ba_Contact_InactiveOwner
 */
@isTest private class Test_ba_Contact_InactiveOwner {

	private static List<User> lstUser_SystemAdministrator = new List<User>();
	private static User oUser_SystemAdministrator;
	private static User oUser_System;
	private static User oUser_GenericDataOwner;
	private static User oUser1;
	private static User oUser2;
	
	@isTest static void test_Scheduling() {

		Test.startTest();

		//---------------------------------------
		// TEST 1
		// SCHEDULING
		//---------------------------------------
		String tCRON_EXP = '0 0 0 3 9 ? 2099';
		
		String tJobId = System.schedule('ba_Contact_InactiveOwner_TEST', tCRON_EXP, new ba_Contact_InactiveOwner());

		// Get the information from the CronTrigger API object
		CronTrigger oCronTrigger = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :tJobId];

		// Verify the expressions are the same
		System.assertEquals(tCRON_EXP, oCronTrigger.CronExpression);

		// Verify the job has not run
		System.assertEquals(0, oCronTrigger.TimesTriggered);

		// Verify the next time the job will run
		System.assertEquals('2099-09-03 00:00:00', String.valueOf(oCronTrigger.NextFireTime));
		//---------------------------------------

		Test.stopTest();

	}

	@isTest static void test_InactiveContact_ActiveAccount() {

		// Create Test System Administratot Users
		createSystemAdministratorUser();

		//---------------------------------------------------
		// CREATE TEST DATA
		//---------------------------------------------------
		System.runAs(oUser_SystemAdministrator){
			createTestUserData();
		}		

		System.runAs(oUser1){
			createTestDataByUser1();
		}

		System.runAs(oUser2){
			createTestDataByUser2();
		}			

		System.runAs(oUser_SystemAdministrator){
				oUser2.IsActive = false;
				oUser2.User_Status__c = 'Other';
			update oUser2;
		}		
		//---------------------------------------------------

		System.runAs(oUser_System){

			Test.startTest();
	
			createTestData();

			// Validate that the Test Data is correct to Test this logic
			Contact oContact = [SELECT Id, OwnerID, Owner.IsActive, Account.OwnerId, Account.Owner.IsActive FROM Contact WHERE Id = :clsTestData.oMain_Contact.Id];
			System.assertEquals(oContact.Account.OwnerId, oUser1.Id);
			System.assertEquals(oContact.OwnerId, oUser2.Id);
			System.assertEquals(oContact.Owner.IsActive, false);
			System.assertEquals(oContact.Account.Owner.IsActive, true);
			System.assertNotEquals(oContact.OwnerId, oContact.Account.OwnerId);
	
			//---------------------------------------------------
			// EXECUTE THE BATCH
			//---------------------------------------------------
	        ba_Contact_InactiveOwner oBatch = new ba_Contact_InactiveOwner();
	        Database.executeBatch(oBatch, 200);    
			//---------------------------------------------------
	
			Test.stopTest();
	
			oContact = [SELECT Id, OwnerID, Owner.IsActive, Account.OwnerId, Account.Owner.IsActive FROM Contact WHERE Id = :clsTestData.oMain_Contact.Id];
			System.assertEquals(oContact.Owner.IsActive, true);
			System.assertEquals(oContact.Account.Owner.IsActive, true);
			System.assertEquals(oContact.OwnerId, oContact.Account.OwnerId);
		}

	}	

	@isTest static void test_InactiveContact_InActiveAccount() {

		// Create Test System Administratot Users
		createSystemAdministratorUser();

		//---------------------------------------------------
		// CREATE TEST DATA
		//---------------------------------------------------
		System.runAs(oUser_SystemAdministrator){
			createTestUserData();
		}		

		System.runAs(oUser1){
			createTestDataByUser1();
		}

		System.runAs(oUser2){
			createTestDataByUser2();
		}

		System.runAs(oUser_SystemAdministrator){
			List<User> lstUser = new List<User>();
				oUser1.IsActive = false;
				oUser1.User_Status__c = 'Other';
			lstUser.add(oUser1);
				oUser2.IsActive = false;
				oUser2.User_Status__c = 'Other';
			lstUser.add(oUser2);
			update lstUser;
		}		
		//---------------------------------------------------

		System.runAs(oUser_System){

			Test.startTest();

			createTestData();

			// Validate that the Test Data is correct to Test this logic
			Contact oContact = [SELECT Id, OwnerID, Owner.IsActive, Account.OwnerId, Account.Owner.IsActive FROM Contact WHERE Id = :clsTestData.oMain_Contact.Id];
			System.assertEquals(oContact.Account.OwnerId, oUser1.Id);
			System.assertEquals(oContact.OwnerId, oUser2.Id);
			System.assertEquals(oContact.Account.Owner.IsActive, false);
			System.assertEquals(oContact.Owner.IsActive, false);
			System.assertNotEquals(oContact.OwnerId, oContact.Account.OwnerId);
	
			//---------------------------------------------------
			// EXECUTE THE BATCH
			//---------------------------------------------------
	        ba_Contact_InactiveOwner oBatch = new ba_Contact_InactiveOwner();
	        Database.executeBatch(oBatch, 200);    
			//---------------------------------------------------
	
			Test.stopTest();

			oContact = [SELECT Id, OwnerID, Owner.IsActive, Owner.Name, Account.OwnerId, Account.Owner.IsActive FROM Contact WHERE Id = :clsTestData.oMain_Contact.Id];
	
			System.assertEquals(oContact.Account.Owner.IsActive, false);
			System.assertEquals(oContact.Owner.IsActive, true);
			System.assertNotEquals(oContact.OwnerId, oContact.Account.OwnerId);
			System.assertEquals(oContact.OwnerId, oUser_GenericDataOwner.Id);
		}
	}

	private static void createSystemAdministratorUser(){

		// Create Test Users : Active System Administrator and the Active System User
		// - the System Administrator User will be used to create/update User data
		// - the System (System Administrator MDT) User will be used to create/update additional data
		// --> this will prevent the MIXED_DML_OPERATION error
		oUser_SystemAdministrator = clsTestData_User.createUser_SystemAdministrator('tadm000', false);
		oUser_System = clsTestData_User.createUser_SystemAdministratorMDT('tadm001', false);

		List<User> lstUser = new List<User>();
		lstUser.add(oUser_SystemAdministrator);
		lstUser.add(oUser_System);
		insert lstUser;

        System.assert(oUser_System != null);
        System.assert(oUser_SystemAdministrator != null);

	}

	private static void createTestData(){

		clsTestData.ptCompanyCode = 'EUR';
		clsTestData.createCountryData();

		clsTestData.oMain_Company.Generic_Data_Owner_Id__c = oUser_GenericDataOwner.Id;
		update clsTestData.oMain_Company;
	
	}

	private static void createTestDataByUser1(){

		clsTestData.iRecord_Account = 1;
		clsTestData.idRecordType_Account = RecordTypeMedtronic.Account('Pending_Account').Id;
		clsTestData.tCountry_Account = 'BELGIUM';
		clsTestData.createAccountData();

	}

	private static void createTestDataByUser2(){

		clsTestData.createContactData();

	}
	
	private static void createTestUserData(){

		Profile oProfile_GenericDataOwner = [SELECT Id FROM Profile WHERE Name = 'EUR Account Data Steward' LIMIT 1];
		UserRole oUserRole_GenericDataOwner = [SELECT Id FROM UserRole WHERE DeveloperName = 'EUR_Data_Steward' LIMIT 1];
		clsTestData.createUserData('GENERICDATAOWNER@Medtronic-TEST.TEST', 'TST00', 'BELGIUM', oProfile_GenericDataOwner.Id, oUserRole_GenericDataOwner.Id);
		oUser_GenericDataOwner = clsTestData.oMain_User;

		clsTestData.oMain_User = null;
		clsTestData.createUserData('TESTUSER1@Medtronic-TEST.TEST', 'TST01', 'BELGIUM', UserInfo.getProfileId(), UserInfo.getUserRoleId());
		oUser1 = clsTestData.oMain_User;

		clsTestData.oMain_User = null;
		clsTestData.createUserData('TESTUSER2@Medtronic-TEST.TEST', 'TST02', 'BELGIUM', UserInfo.getProfileId(), UserInfo.getUserRoleId());
		oUser2 = clsTestData.oMain_User;

		clsTestData.oMain_User = null;

	}
}