public with sharing class ctrlExt_Opportunity_Stakeholder_Mapping {
	
	private Stakeholder_Mapping__c stakeholder;
	
	public ctrlExt_Opportunity_Stakeholder_Mapping(ApexPages.StandardController sc){	
		stakeholder = (Stakeholder_Mapping__c) sc.getRecord();		
	}
	
	public PageReference redirectWithRecordType(){
		if(stakeholder.Opportunity__c == null) return null;
				
		Opportunity opp = [Select Id, 
								  RecordTypeId 
							 From Opportunity 
							Where Id = :stakeholder.Opportunity__c
							limit 1];		
		
		string q1 = 'Select Stakeholder_Mapping_Id__c '; 
		q1 +=       '  From Price_Book_Management__c '; 
		q1 +=       ' Where Opportunity_Record_Type_ID__c = \'' + String.valueOf(opp.RecordTypeId).left(15) + '\' or Opportunity_Record_Type_ID__c = \''+ opp.RecordTypeId +'\''; 
		q1 +=  		' limit 1';

		List<Price_Book_Management__c> pbm = Database.query(q1);
		
		Id id_RecordType; // Make sure we use the 18 char version of the ID
		
		try {		
			if(id_RecordType == null) {	
				if (pbm.size() > 0){
					Id id_StakeholderMapping = pbm[0].Stakeholder_Mapping_Id__c; // Make sure we use the 18 char version of the ID
					id_RecordType = clsUtil.getRecordTypeById(id_StakeholderMapping).Id;
				}else{
					id_RecordType = clsUtil.getRecordTypeByDevName('Stakeholder_Mapping__c', 'General_Stakeholder_Mapping').Id;
				}
			}
		} catch (nullpointerexception e) {
			id_RecordType = clsUtil.getRecordTypeByDevName('Stakeholder_Mapping__c', 'General_Stakeholder_Mapping').Id;
		}
		
											
		String keyPrefix = Stakeholder_Mapping__c.sObjectType.getDescribe().getKeyPrefix();
						
		PageReference pr = new PageReference('/' + keyPrefix + '/e');
		pr.getParameters().putAll(ApexPages.currentpage().getparameters());
		pr.getParameters().remove('sfdc.override');
		pr.getParameters().remove('save_new');
		pr.getParameters().put('RecordType', id_RecordType);
		pr.getParameters().put('saveURL', '/' + opp.Id );
		pr.getParameters().put('nooverride', '1');
		pr.setRedirect(true);
		
		return pr;
	}
}