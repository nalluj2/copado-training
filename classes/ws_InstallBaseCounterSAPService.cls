global class ws_InstallBaseCounterSAPService {
	
	public static final Integer TRIM_ERROR_MSG_LENGTH = 132; 
	
	webservice static SAPInstallBaseCounter getInstallBaseCounterUpdate(String counterId){
		
		SAPInstallBaseCounter response;
		Installed_Product_Measures__c counter;
		
		try{
			
			//map the request to an installed product SFDC custom object
			counter = bl_InstallBaseCounterSAPService.getProductMeasuresDetails(counterId);

			//map the installed product SFDC custom object to a response.
			response = bl_InstallBaseCounterSAPService.mapMeasurementToCounter(counter);
			response.DISTRIBUTION_STATUS = 'TRUE';
			
		} catch (Exception ex){
            
            // Construct error message
            String errMsg = (ex.getMessage()).left(TRIM_ERROR_MSG_LENGTH);
            
            // As the error is thrown, need to construct a new Service Order to populate the response message
            response = new SAPInstallBaseCounter();        	        	
        	response.DISTRIBUTION_STATUS = 'FALSE';
        	response.DIST_ERROR_DETAILS = errMsg;        	 	  
        }
        
        String sapID;
        if(counter != null) sapID = counter.SAP_Measure_Point_ID__c;
         
		ut_Log.logOutboundMessage(counterId, sapID, counterId, JSON.serializePretty(response), response.DISTRIBUTION_STATUS, 'getInstallBaseCounterUpdate', 'Installed_Product_Measures__c');
		
		return response;
	}

	webservice static SAPInstallBaseCounterAck updateInstallBaseCounterAcknowledgement(SAPInstallBaseCounterAck sapInstallBaseCounterAck){
		
    	Installed_Product_Measures__c counter; 
    	   	
    	SAPInstallBaseCounterAck response = new SAPInstallBaseCounterAck();

    	Savepoint sp = Database.setSavepoint();
    	
    	String tNotificationSAPLogStatus = 'FALSE';
    	
	    try{
    	
            clsUtil.debug('updateInstallBaseCounterAcknowledgement - sapInstallBaseCounterAck : ' + sapInstallBaseCounterAck);        

	    	if (sapInstallBaseCounterAck.DISTRIBUTION_STATUS == 'TRUE'){
	    		
                // SUCCESS
				counter = [SELECT Id, SAP_Measure_Point_ID__c FROM Installed_Product_Measures__c WHERE Id = :sapInstallBaseCounterAck.SFDC_ID];
				tNotificationSAPLogStatus = 'TRUE';

	    	}else{

                // ERROR OCCURED
				tNotificationSAPLogStatus = 'FALSE';
            }
	    	
	    	response.DISTRIBUTION_STATUS = 'TRUE';

    	}catch (Exception ex){

			tNotificationSAPLogStatus = 'FALSE';

            Database.rollback(sp);
            
            // Construct error message
            String errMsg = (ex.getMessage()).left(TRIM_ERROR_MSG_LENGTH);
            
            //TODO: Update the Sync Notification record with the error occurred. Special case to take into account for the lock mechanism
                 	        	
        	response.DISTRIBUTION_STATUS = 'FALSE';
        	SAPErrorMessage msg = new SAPErrorMessage();
        	msg.ERROR_MESSAGE = errMsg;
        	response.DIST_ERROR_DETAILS = new List<SAPErrorMessage>{msg};     	  
        }

    	response.SAP_ID = sapInstallBaseCounterAck.SAP_ID;
    	response.SFDC_ID = sapInstallBaseCounterAck.SFDC_ID;
    	    	
		ut_Log.logOutboundMessage(response.SFDC_ID, response.SAP_ID, JSON.serializePretty(sapInstallBaseCounterAck), JSON.serializePretty(response), '', 'updateInstallBaseCounterAcknowledgement', 'Installed_Product_Measures__c');

        // Update the Notification SAP Log record with the Acknowledgement response
        ut_Log.updateNotificationSAPLogStatus(response.SFDC_ID, tNotificationSAPLogStatus, JSON.serializePretty(sapInstallBaseCounterAck), JSON.serializePretty(response));
            	
    	return response;
	}	
    		
	global class SAPInstallBaseCounter {
		
		webservice String DISTRIBUTION_STATUS;
        webservice String DIST_ERROR_DETAILS;
        
        webservice String SAP_IDOC_NUMBER;
        webservice String WM_TRANSACTION_NUMBER;    
                
		webservice String SFDC_ID;
				
		webservice String NEW_RECORDED_DATE;
		webservice String NEW_RECORDED_TIME;
		webservice String NEW_RECORDED_VALUE;
		webservice String COUNTER_ID;
		webservice String DESCRIPTION;		
		Webservice String READ_BY;
	}
     
	global class SAPInstallBaseCounterAck {
		
		webservice String SAP_ID;
        webservice String SFDC_ID;
        
        webservice String WMTRANSACTION;
		webservice String TIMESTAMP;        
        webservice String DISTRIBUTION_STATUS;        
        webservice List<SAPErrorMessage> DIST_ERROR_DETAILS;
	}    
	
	global class SAPErrorMessage{
    	
    	webservice String ERROR_TYPE;
        webservice String ERROR_ID;
        webservice String ERROR_NUMBER;
		webservice String ERROR_MESSAGE;
    }
}