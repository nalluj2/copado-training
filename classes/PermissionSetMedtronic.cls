public without sharing class PermissionSetMedtronic {
    
    @TestVisible private static Set<String> userPermissionSets{
    	
    	get{
    		if(userPermissionSets == null){
    			
    			userPermissionSets = new Set<String>();
    			
    			for(PermissionSetAssignment userPermission : [Select PermissionSet.Name from PermissionSetAssignment where AssigneeId = :UserInfo.getUserId()]){
    				
    				userPermissionSets.add(userPermission.PermissionSet.Name.toUpperCase());
    			}
    		}	
    		
    		return userPermissionSets;
    	}
    	
    	set;
    }
    
    public static Boolean hasPermissionSet(String apiName){
    	
    	return userPermissionSets.contains(apiName.toUpperCase());
    }
}