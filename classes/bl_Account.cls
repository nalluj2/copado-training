/*
 * Description	 	: This is the class is used to centralize logic around Account
 * Author        	: Patrick Brinksma
 * Created Date		: 17-07-2013
 */
public class bl_Account {

	public static boolean accountMerge = false;

	/*
	 * Description	 	: populate DIB Sales Rep fields on Account, values copied from User when sales is manually selected
	 * Author        	: Patrick Brinksma
	 * Created Date		: 17-07-2013
	 * Updated date		: 7-7-2014 (Rudy De Coninck)
	 */
	public static void populateDIBSalesRepDetailsManual(List<Account> listOfAccount, Set<Id> setOfSalesRepId){
		
		if(setOfSalesRepId.size()>0){
		
			Map<Id, User> mapOfIdToUser = new Map<Id, User>([select SAP_ID__c, FirstName, LastName from User where Id in :setOfSalesRepId]);
		
			for (Account thisAccount : listOfAccount){
				
				thisAccount.DIB_SAP_Sales_Rep_Id__c = mapOfIdToUser.get(thisAccount.DIB_Sales_Rep__c).SAP_ID__c;
				thisAccount.DIB_SAP_SR_Name__c = mapOfIdToUser.get(thisAccount.DIB_Sales_Rep__c).LastName + ', ' + mapOfIdToUser.get(thisAccount.DIB_Sales_Rep__c).FirstName;
				thisAccount.DIB_Sales_Rep__c = null; 
			}
		}
	}

	/*
	 * Description	 	: populate DIB Sales Rep fields on Account, values copied from User when not manually selected
	 * Author        	: Rudy De Coninck
	 * Created date		: 7-7-2014 (Rudy De Coninck)
	 */
	public static void populateDIBSalesRepDetailsAuto(List<Account> listOfAccount, Set<String> setOfSalesRepId){
		
		if(setOfSalesRepId != null && setOfSalesRepId.size()>0){
						
			List<User> users = [select SAP_ID__c, FirstName, LastName from User where SAP_ID__c in :setOfSalesRepId];
			
			Map<String, User> mapOfIdToUser = new Map<String, User>();			
			for (User u : users){
				mapOfIdToUser.put(u.SAP_ID__c,u);
			}
			
			for (Account thisAccount : listOfAccount){
				
				User selectedUser = mapOfIdToUser.get(thisAccount.DIB_SAP_Sales_Rep_Id__c);
				
				if(selectedUser != null){
					
					thisAccount.DIB_SAP_SR_Name__c = selectedUser.LastName + ', ' + selectedUser.FirstName;
				}
			}
		}
	}

	/*
	 * Description	 	: For merged accounts, validate for Master Account (survivor) if any CAN DIB Opportunity has to be send out (just by updating the Opportunity) 
	 * Author        	: Patrick Brinksma
	 * Created Date		: 12-08-2013
	 */
	public static void sendMergedAccountOpportunitiesToSAP(Set<Id> setOfMasterAccountId){
		
		accountMerge = true;
		
		// Get RecordType for Account. Only execute if SAP_Patient
		List<Account> sapPatients = [select Id, (Select Id from Opportunities where RecordType.DeveloperName = :ut_StaticValues.RTDEVNAME_CAN_DIB_OPPORTUNITY and SAP_ID_Text__c = null)
										 from Account where Id in :setOfMasterAccountId and RecordType.DeveloperName = :ut_StaticValues.RTDEVNAME_SAP_PATIENT];
		if (sapPatients.size() > 0){
			
			// query for Opportunities for these Accounts of the correct recordtype and where SAP ID is null (not sent to SAP yet)
			List<Opportunity> patientOpportunities = new List<Opportunity>();
			
			for(Account sapPatient : sapPatients){
				
				if(sapPatient.Opportunities.size() > 0) patientOpportunities.addAll(sapPatient.Opportunities);
			}
			
			if (patientOpportunities.size() > 0) update patientOpportunities;			
		}
		
		accountMerge = false;
	}
	
	public static Account findMatchingNonSAPPatient(Account sapPatient){
		
		List<Account> matchingAccounts = [
			select Id, DIB_SAP_SR_Name__c, DIB_SAP_Sales_Rep_Id__c
								    from Account 
									where Firstname = : sapPatient.FirstName 
									and Lastname =: sapPatient.LastName
									and RecordTypeId =: RecordTypeMedtronic.Account('Non_SAP_Patient').id 
									and Account_Country_vs__c =: sapPatient.Account_Country_vs__c
									and Account_City__c =: sapPatient.Account_City__c
									and Account_Address_Line_1__c =: sapPatient.Account_Address_Line_1__c
		];
		
		Account matchingAccount = null;
		
		if (matchingAccounts.size()>0){
			//Always take first
			matchingAccount = matchingAccounts.get(0);
		}
				
		return matchingAccount;
	}
	
	public static Account findMatchingNonSAPPatientEUR(Account sapPatient){
		
		List<Account> matchingAccounts;
		
		if(sapPatient.Account_Email__c != null){
			
			System.debug('Finding matching account by email for sap account '+sapPatient);
			
			matchingAccounts = [select Id, DIB_SAP_SR_Name__c, DIB_SAP_Sales_Rep_Id__c from Account 
									where Account_Email__c = : sapPatient.Account_Email__c									
									and RecordTypeId =: RecordTypeMedtronic.Account('Non_SAP_Patient').id 
									and Account_Country_vs__c =: sapPatient.Account_Country_vs__c LIMIT 2];	
		}else{
			
			System.debug('Finding matching account by firstname, lastname for sap account '+sapPatient);
			
			matchingAccounts = [select Id, DIB_SAP_SR_Name__c, DIB_SAP_Sales_Rep_Id__c from Account 
									where Firstname = : sapPatient.FirstName 
									and Lastname =: sapPatient.LastName
									and RecordTypeId =: RecordTypeMedtronic.Account('Non_SAP_Patient').id 
									and Account_Country_vs__c =: sapPatient.Account_Country_vs__c LIMIT 2];	
		}
				
		if (matchingAccounts.size() != 1){
			return null;			
		}
		
		return matchingAccounts[0];
	}
	
	@future
  	public static void autoMergeSAPPatient(Id masterAccountId, Id matchingAccountId){
    	      
	    Account masterAccount = new Account(Id = masterAccountId);
	    Account matchingAccount = new Account(Id = matchingAccountId);
	    
	    try{
	    	
	    	merge masterAccount matchingAccount;
	    	
	    }catch(Exception e){
	    	
	    	Account notMerged = [Select Id, firstname, lastname, Account_Country_vs__c from Account where Id = :masterAccountId];
	    	    	
	    	if (notMerged.Account_Country_vs__c!=null) {    	
	    		DIB_Country__c country = [select Id, Patient_auto_merge_mailing_userIds__c from DIB_Country__c where name = :notMerged.Account_Country_vs__c];
	    		if (country!=null){
	    			if (country.Patient_auto_merge_mailing_userIds__c!=null){
	    				List<User> usersToEmail = [select id, email from user where id in (:country.Patient_auto_merge_mailing_userIds__c)];
	    				if (usersToEmail!=null && usersToEmail.size()>0){
	    					List<String> emails = new List<String>();
	    					for (User u : usersToEmail){
	    						emails.add(u.email);
	    					}
	    					
	    					String subject='New sap patient but could not be matched: name = '+notMerged.firstname+' '+notMerged.lastName;
	    					String message='A new SAP Patient Account '+notMerged.firstname+' '+notMerged.lastName+' has been received but it could not be merged automatically.<br/><br/>'+e.getMessage();
	    					emailMergeResult(emails,'noreply@medtronic.com','sap_interface@medtronic.com',subject,message);
	    					
	    				}
	    			}
	    		}
	    	
	    	}    	
	    }
  	}
  	
  	
  	public static void emailMergeResult(List<String> toNames, String replyTo, String senderName, String subject, String message){
		List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
		
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			
		String[] toAddressArray = new String[toNames.size()];
		Integer i = 0;
		for (String toName : toNames){
			toAddressArray[i++] = toName;
		}
					
		mail.setToAddresses(toAddressArray);		 
		mail.setReplyTo(replyTo);		
		mail.setSenderDisplayName(senderName);			
		mail.setSubject(subject);				
		mail.setBccSender(false);				
		mail.setUseSignature(false);
		mail.setHtmlBody(message);
						
   		mails.add(mail);     
				
		Messaging.sendEmail(mails);
  		
  	}
  	
  	public static void createSMAXLocations(List<Account> accounts)
    {
  		List<Account> accs = new List<Account>();  		
  		for(Account acc : accounts)
        {  			
  			if ((acc.SAP_Account_Type__c == 'Ship to Party' ||
                 acc.SAP_Account_Type__c == 'Sold-to party' ||
                 acc.SAP_Account_Type__c == 'ZK-Party') &&
                acc.SAP_Id__c != null &&
                acc.SAP_Customer_Group__c != 'PATIENT') 
            {
                accs.add(acc);
            }
  		}
  		
  		if (accs.isEmpty()) 
        {
            return;
        }
  		 		
  		List<SVMXC__Site__c> locations = new List<SVMXC__Site__c>();
  		for(Account acc : accs)
        {  			 				
  			SVMXC__Site__c location = new SVMXC__Site__c();
            
  			location.SVMXC__Account__c = acc.Id;
			location.SVMXC_SAP_Id__c = acc.SAP_Id__c;
  			location.SVMX_SAP_Location_ID__c = acc.SAP_Id__c;

  			String name = acc.Name;
  			if(name == null)
            {
  				if (acc.SAP_Account_Name__c != null &&
                    acc.SAP_Account_Name__c.length() > 80) 
                {
                    name = acc.SAP_Account_Name__c.left(80);
                }
  				else
                {
                    name = acc.SAP_Account_Name__c;
                }
  			}
  			
  			location.Name = name;
  			location.SVMXC__Location_Type__c = 'Customer';  
  			location.SVMXC__Country__c = acc.Account_Country_vs__c;
  			location.SVMXC__City__c = acc.Account_City__c;				
  			location.SVMXC__State__c = acc.Account_Province__c;
  			location.SVMXC__Street__c = acc.Account_Address_Line_1__c;
  			location.SVMXC__Zip__c = acc.Account_Postal_Code__c;
			location.SVMXC_SAP_Department__c = acc.Account_Address_Line_2__c;

  			locations.add(location);  			
  		}
  		
  		upsert locations SVMX_SAP_Location_ID__c;
  	}  		
}