//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 11-09-2017
//  Description      : APEX Test Class for tr_AccountProcedure and bl_AccountProcedure_Trigger
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_bl_AccountProcedure_Trigger {
	
	@isTest static void test_PreventDuplicate() {

		// Create Test Data
		Account oAccount = clsTestData_Account.createAccount()[0];
		Id id_RecordType_AccountProcedure = clsUtil.getRecordTypeByDevName('Account_Procedure__c', 'EMEA_MITG_Procedure').Id;


		// Perform Test
		Test.startTest();

			Account_Procedure__c oAccountProcedure1 = new Account_Procedure__c(Account__c = oAccount.Id, RecordTypeId = id_RecordType_AccountProcedure, Procedure__c = 'Anesthesia');
			insert oAccountProcedure1;

			Account_Procedure__c oAccountProcedure2 = new Account_Procedure__c(Account__c = oAccount.Id, RecordTypeId = id_RecordType_AccountProcedure, Procedure__c = 'Intubation');
			insert oAccountProcedure2;

			Integer iError = 0;
			try{
				Account_Procedure__c oAccountProcedure3 = new Account_Procedure__c(Account__c = oAccount.Id, RecordTypeId = id_RecordType_AccountProcedure, Procedure__c = 'Anesthesia');
				insert oAccountProcedure3;
			}catch(Exception oEX){
				iError++;
				System.debug('**BC** oEX.getMessage() : ' + oEX.getMessage());
				System.assert(oEX.getMessage().contains('The selected Procedure already exists for this Account'));
			}

			try{
				oAccountProcedure2.Procedure__c = 'Anesthesia';
				update oAccountProcedure2;
			}catch(Exception oEX){
				iError++;
				System.debug('**BC** oEX.getMessage() 2 : ' + oEX.getMessage());
				System.assert(oEX.getMessage().contains('The selected Procedure already exists for this Account'));
			}


		Test.stopTest();


		// Validate Test
		System.assertEquals(iError, 2);

		List<Account_Procedure__c> lstAccountProcedure = [SELECT Id FROM Account_Procedure__c WHERE Account__c = :oAccount.Id];
		System.assertEquals(lstAccountProcedure.size(), 2);

	}
	
}
//--------------------------------------------------------------------------------------------------------------------