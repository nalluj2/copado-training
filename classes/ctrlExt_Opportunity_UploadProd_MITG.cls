public with sharing class ctrlExt_Opportunity_UploadProd_MITG {

    //------------------------------------------------------------------------
    // PRIVATE VARIABLES
    //------------------------------------------------------------------------
    private final Opportunity oOpportunity;
    private String[] lstFileLine = new String[]{};

    private String tDelimiter = '\t'; // TAB Delimter
    //------------------------------------------------------------------------


    //------------------------------------------------------------------------
    // CONSTRUCTOR
    //------------------------------------------------------------------------
    public ctrlExt_Opportunity_UploadProd_MITG(ApexPages.StandardController stdController) {

        bRenderPage = false;
        Id id_Opportunity = ApexPages.currentPage().getParameters().get('OppId');

        if (id_Opportunity != null){

            oOpportunity = [SELECT Id, Pricebook2Id, CurrencyIsoCode, Type FROM Opportunity WHERE Id = :id_Opportunity];
            bRenderPage = true; 

        }

        if (oOpportunity == null){

            ApexPages.Message oErrorMsg = new ApexPages.Message(ApexPages.severity.ERROR, label.No_Opportunity_Provided);
            ApexPages.addMessage(oErrorMsg);

        }

        lstIssueRecord = new List<IssueRecord>();

    }
    //------------------------------------------------------------------------


    //------------------------------------------------------------------------
    // GETTER & SETTER
    //------------------------------------------------------------------------
    public Boolean bRenderPage { get;set; }
    public String tFileName { get;set; }
    public Blob oFile { get;set; }
    public List<IssueRecord> lstIssueRecord { get; set; }

    public Boolean hasInvalidRecords {
        get{
            if (lstIssueRecord.size() > 0) return true; 
            return false;
        }
    }
    //------------------------------------------------------------------------


    //------------------------------------------------------------------------
    // ACTIONS    
    //------------------------------------------------------------------------
    public Pagereference ReadFile(){
        //declare a array of Upload records
            
        if (oFile == null){

            ApexPages.Message oErrorMsg = new ApexPages.Message(ApexPages.severity.ERROR, label.Please_choose_a_valid_file);
            ApexPages.addMessage(oErrorMsg);

        }else{
            String tFileData;

            try{
                clsUtil.bubbleException();

                tFileData = oFile.toString();
            }catch (Exception oEX){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, label.An_error_has_occured_Please_check_the_file_template_or_try_again_later));
                return null;
            }
            // Break the content by CRLF
            lstFileLine = tFileData.split('\n');
            
            if (lstFileLine.size() < 2 || (lstFileLine[0].split(tDelimiter).size()<2) ){

                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, label.Please_check_data_in_file_for_correct_format));

            }else{

                List<OpportunityLineItem> lstOpportunityLineItem = new List<OpportunityLineItem>();
                Map<String, PricebookEntry> sku_pbeId = new Map<String, PricebookEntry>(); 
                
                for(PricebookEntry pe : 
                    [
                        SELECT Id, Product2.CFN_Code_Text__c, UnitPrice 
                        FROM PricebookEntry 
                        WHERE IsActive = true AND CurrencyIsoCode = :oOpportunity.CurrencyIsoCode AND Pricebook2Id = :oOpportunity.Pricebook2Id
                    ]
                ){
                    if (!sku_pbeId.containskey(pe.Product2.CFN_Code_Text__c)){
                        sku_pbeId.put(pe.Product2.CFN_Code_Text__c, pe);
                    }
                }

                for (Integer i = 0;i < lstFileLine.size();i++){

                    if (i == 0) continue;
                    
                    String[] inputvalues = new String[]{};
           
                    //split the Record by fields based on the delimiter
                    inputvalues = lstFileLine[i].split(tDelimiter);
                    system.debug('Number of Columns *'+inputvalues.size());
                    if (inputvalues.size() < 2){

                        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, label.File_format_Error_Number_of_columns_are_less_than_expected));
                        return null;

                    }else{

                        string strError = '';
                        Decimal decQuantity = 0;
                        Decimal decUnitPrice = 0;
                        Decimal decBiddingNumber = 0;
                        
                        if (!sku_pbeId.containskey(inputvalues[0].trim())){
                            
                            strError = Label.No_Product_found_with_CFN_Code;

                        }

                        if (inputvalues[1].trim() != null && inputvalues[1].trim() != ''){

                            try{

                                clsUtil.bubbleException1();
                                decQuantity = decimal.valueOf(inputvalues[1].trim());
                                if (oOpportunity.Type == 'At Risk' && decQuantity > 0){
                                    strError += Label.Quantity_should_be_negative_for_at_risk_opportunity;
                                }

                            }catch(Exception oEX){
                                strError += Label.Quantity_is_invalid;  
                            }
                        }

                        if (inputvalues.size() > 2){

                            if (inputvalues[2].trim() != null && inputvalues[2].trim() != ''){ 

                                try{

                                    clsUtil.bubbleException2();
                                    decUnitPrice = decimal.valueOf(inputvalues[2].trim());
                                    if (decUnitPrice < 0){
                                        strError += Label.Sales_Price_cannot_be_negative_for_opportunity_product;
                                    }

                                }catch(Exception oEX){
                                    strError += Label.Sales_Price_is_invalid;  
                                }
                            }
                        }

                        if (inputvalues.size() > 3){

                            if (inputvalues[3].trim() != null && inputvalues[3].trim() != ''){ 

                                try{

                                    clsUtil.bubbleException3();
                                    decBiddingNumber = decimal.valueOf(inputvalues[3].trim());

                                    if (decBiddingNumber < 0){

                                        strError += Label.Bidding_Number_cannot_be_negative_for_opportunity_product;

                                    }

                                }catch(Exception oEX){
                                    strError += Label.Bidding_Number_is_invalid;  
                                }
                            }
                        }
                        
                        
                        if (strError == ''){

                            OpportunityLineItem oli = new OpportunityLineItem();
                                oli.OpportunityId = oOpportunity.Id;
                                oli.PricebookEntryId = sku_pbeId.get(inputvalues[0].trim()).Id;
                                oli.Quantity =  decQuantity;

                            if (decUnitPrice > 0){

                                oli.UnitPrice = decUnitPrice;

                            }else{

                                oli.UnitPrice = sku_pbeId.get(inputvalues[0].trim()).UnitPrice;
                            }

                            if (decBiddingNumber > 0){

                                oli.Bidding_Number__c = decBiddingNumber;

                            }

                            lstOpportunityLineItem.add(oli);

                        }else{  

                            if (inputvalues.size() == 2){

                               lstIssueRecord.add(new IssueRecord(inputvalues[0].trim(),inputvalues[1].trim(),strError));

                            }

                            if (inputvalues.size() == 4){

                                lstIssueRecord.add(new IssueRecord(inputvalues[0].trim(),inputvalues[1].trim(),strError,inputvalues[2].trim(),inputvalues[3].trim()));

                            }

                        }

                    }

                }
                try {

                    clsUtil.bubbleException4();

                    if (lstOpportunityLineItem.size()>0){
                        insert lstOpportunityLineItem;
                    }

                }catch (Exception e){

                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,e.getMessage()));
                    return null;
                    
                }
                    
                if (lstIssueRecord.size() == 0){

                    return new PageReference('/' + oOpportunity.Id);

                }else{

                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Label.Below_data_is_not_valid));
                    return null;

                }

            }

        }

        return null;

    }

    public PageReference goBacktoOpp(){
        return new PageReference('/' + oOpportunity.Id);
    }    
    //------------------------------------------------------------------------


    //------------------------------------------------------------------------
    // HELPER CLASS
    //------------------------------------------------------------------------
    public class IssueRecord{
        public String SKU { get;set; }
        public String Quantity { get;set; }
        public String SalesPrice { get;set; }
        public String BiddingNumber { get;set; }
        public String Error { get;set; }
        public IssueRecord(string tParam1, String tParam2, String tParam3){
            SKU = tParam1;
            Quantity = tParam2;
            Error = tParam3;
        }
        public IssueRecord(String tParam1, String tParam2, String tParam3, String tParam4, String tParam5){
            SKU = tParam1;
            Quantity = tParam2;
            Error = tParam3;
            SalesPrice = tParam4;
            BiddingNumber = tParam5;
        }
    }
    //------------------------------------------------------------------------
   
    

}