@isTest
private class Test_ws_InstallBaseCounter {
    
    @testSetup
    static void createTestData(){
    	
    	WebServiceSetting__c counterSettings = new WebServiceSetting__c();
        counterSettings.Name = 'InstProductCounter_NotificationSAP';
        counterSettings.EndpointUrl__c = 'http://144.15.228.14:33500/ws/MdtAny_SfdcInt.SRServiceOrder.webservice.providers.ws:processServiceOrder_WSDL';
        counterSettings.XMLNS__c = 'mdt="http://msplwa107-pr.wby1-dev.medtronic.com/MdtAny_SfdcInt.SRServiceOrder.webservice.providers.ws:processServiceOrder_WSDL"';
        counterSettings.WebServiceName__c = 'mdt:webservicename';
        counterSettings.DataName__c = 'WebServiceName';
        counterSettings.Timeout__c = 60000;
        counterSettings.Username__c = 'username';
        counterSettings.Password__c = 'password';
        counterSettings.Callout_Failure_Email_Address__c = 'callout.failure@medtronic.com';
        counterSettings.Callout_Max_Retries__c = 5;
        counterSettings.Callout_Retry_Minutes__c = '60';
        counterSettings.Org_Id__c = Userinfo.getOrganizationId().left(15);
        counterSettings.Instance_Id__c = 'XXX';	
        insert counterSettings;	
    	
    	SVMXC__Installed_Product__c instProd = new SVMXC__Installed_Product__c();
    	insert instProd;
    	
    	Installed_Product_Measures__c prodMeasure = new Installed_Product_Measures__c();
    	prodMeasure.Installed_Product__c = instProd.Id;
    	prodMeasure.Active__c = true;
    	prodMeasure.Description__c = 'Unit Test Measurement Value';
    	prodMeasure.Last_Recorded_Timestamp__c = Datetime.now().addDays(-15);
    	prodMeasure.Last_Recorded_Value__c = '20';
    	prodMeasure.New_Recorded_Value__c = '35';    	    	
    	prodMeasure.SAP_Measure_Point_ID__c = '11111111';
    	prodMeasure.Type__c = 'Gauge Counter'; 
		prodMeasure.Unit_of_Measurement__c = 'EA';
		insert prodMeasure;
		
		Test.startTest();

        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new TEST_bl_NotificationSAP_Mock()); 
		
		//Make update that triggers Notification generation
		prodMeasure.New_Recorded_Value__c = '40';
		update prodMeasure;
		
		Test.stopTest();
		
		List<NotificationSAPLog__c> notifications = [Select Id, Status__c, Retries__c from NotificationSAPLog__c where Record_ID__c = :prodMeasure.Id];
        System.assert(notifications.size() == 1);
        System.assert(notifications[0].Status__c == 'NEW');
        System.assert(notifications[0].Retries__c == 0);
    }
    
    @isTest
    private static void testGetInstallBaseCounter()
    {
    	Installed_Product_Measures__c prodMeasure = [Select Id from Installed_Product_Measures__c];
    	    	    	
        // Load User Data - SAP Interface
		User oUser = [SELECT Id, Name, isActive FROM User WHERE (Profile.Name = 'SAP Interface') AND IsActive = true LIMIT 1];

        Test.startTest();

        System.runAs(oUser){
        	
        	//get install base
    		ws_InstallBaseCounterSAPService.SAPInstallBaseCounter response = ws_InstallBaseCounterSAPService.getInstallBaseCounterUpdate(prodMeasure.Id);
    		
            System.assert(response.DISTRIBUTION_STATUS == 'TRUE');
            System.assert(response.NEW_RECORDED_VALUE == '40');
            System.assert(response.COUNTER_ID == '11111111');
            System.assert(response.DESCRIPTION == 'Unit Test Measurement Value');
        }
    	
    	Test.stopTest();    	
    }
    
    @isTest
    private static void testGetInstallBaseCounter_Exception()
    {    	    	    	    	
        // Load User Data - SAP Interface
		User oUser = [SELECT Id, Name, isActive FROM User WHERE (Profile.Name = 'SAP Interface') AND IsActive = true LIMIT 1];

        Test.startTest();

        System.runAs(oUser){
        	
        	//get install base
    		ws_InstallBaseCounterSAPService.SAPInstallBaseCounter response = ws_InstallBaseCounterSAPService.getInstallBaseCounterUpdate(null);
    		
            System.assert(response.DISTRIBUTION_STATUS == 'FALSE');    		
        }
    	
    	Test.stopTest();    	
    }
    
    @isTest
    private static void testAknowledgement_Success(){
    	
    	Installed_Product_Measures__c prodMeasure = [Select Id from Installed_Product_Measures__c];
    	
    	// Load User Data - SAP Interface
		User oUser = [SELECT Id, Name, isActive FROM User WHERE (Profile.Name = 'SAP Interface') AND IsActive = true LIMIT 1];

        Test.startTest();

        System.runAs(oUser){
        	        	
    		//send acknowledgement
    		ws_InstallBaseCounterSAPService.SAPInstallBaseCounterAck ack = new ws_InstallBaseCounterSAPService.SAPInstallBaseCounterAck();
    		ack.SAP_ID = '11111111';
    		ack.SFDC_ID = prodMeasure.Id;
    		ack.DISTRIBUTION_STATUS = 'TRUE';
    		
    		ws_InstallBaseCounterSAPService.SAPInstallBaseCounterAck ackResponse = ws_InstallBaseCounterSAPService.updateInstallBaseCounterAcknowledgement(ack);
            System.assert(ackResponse.DISTRIBUTION_STATUS == 'TRUE');	
    		
    		List<NotificationSAPLog__c> notifications = [Select Id, Status__c from NotificationSAPLog__c where Record_ID__c = :prodMeasure.Id];
            System.assert(notifications.size() == 1);
            System.assert(notifications[0].Status__c == 'COMPLETED');
        }    		
    }
    
    @isTest
    private static void testAknowledgement_Error(){
    	
    	Installed_Product_Measures__c prodMeasure = [Select Id from Installed_Product_Measures__c];
    	
    	// Load User Data - SAP Interface
		User oUser = [SELECT Id, Name, isActive FROM User WHERE (Profile.Name = 'SAP Interface') AND IsActive = true LIMIT 1];

        Test.startTest();

        System.runAs(oUser){
        	        	
    		//send acknowledgement
    		ws_InstallBaseCounterSAPService.SAPInstallBaseCounterAck ack = new ws_InstallBaseCounterSAPService.SAPInstallBaseCounterAck();
    		ack.SAP_ID = '11111111';
    		ack.SFDC_ID = prodMeasure.Id;
    		ack.DISTRIBUTION_STATUS = 'FALSE';
    		
    		ws_InstallBaseCounterSAPService.SAPInstallBaseCounterAck ackResponse = ws_InstallBaseCounterSAPService.updateInstallBaseCounterAcknowledgement(ack);
            System.assert(ackResponse.DISTRIBUTION_STATUS == 'TRUE');
    		
    		List<NotificationSAPLog__c> notifications = [Select Id, Status__c from NotificationSAPLog__c where Record_ID__c = :prodMeasure.Id];
            System.assert(notifications.size() == 1);
            System.assertEquals('RETRY', notifications[0].Status__c);
        }    		
    }
    
    @isTest
    private static void testAknowledgement_Exception(){
    	
    	Installed_Product_Measures__c prodMeasure = [Select Id from Installed_Product_Measures__c];
    	delete prodMeasure;
    	
    	// Load User Data - SAP Interface
		User oUser = [SELECT Id, Name, isActive FROM User WHERE (Profile.Name = 'SAP Interface') AND IsActive = true LIMIT 1];

        Test.startTest();

        System.runAs(oUser){
        	        	
    		//send acknowledgement
    		ws_InstallBaseCounterSAPService.SAPInstallBaseCounterAck ack = new ws_InstallBaseCounterSAPService.SAPInstallBaseCounterAck();
    		ack.SAP_ID = '00000000';
    		ack.SFDC_ID = prodMeasure.Id;// Record doesn't exist anymnore, so exception will happen
    		ack.DISTRIBUTION_STATUS = 'TRUE';
    		
    		ws_InstallBaseCounterSAPService.SAPInstallBaseCounterAck ackResponse = ws_InstallBaseCounterSAPService.updateInstallBaseCounterAcknowledgement(ack);
    		System.assert(ackResponse.DISTRIBUTION_STATUS == 'FALSE');
    		System.assert(ackResponse.DIST_ERROR_DETAILS.size() == 1);
    		
    		List<NotificationSAPLog__c> notifications = [Select Id, Status__c from NotificationSAPLog__c where Record_ID__c = :prodMeasure.Id];
    		System.assert(notifications.size() == 1);
    		System.assertEquals('RETRY', notifications[0].Status__c);    			
        }    		
    }   
}