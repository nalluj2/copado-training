@isTest
private class Test_ContactDuplicates {

	private static Id genericContactTypeId = [Select Id from RecordType where SObjectType = 'Contact' AND DeveloperName = 'Generic_Contact'].Id;
	private static Id pendingAccountTypeId = [Select Id from RecordType where SObjectType = 'Account' AND DeveloperName = 'Pending_Account'].Id;
	private static Id mdtEmployeeTypeId = [Select Id from RecordType where SobjectType = 'Contact' and DeveloperName = 'MDT_Employee'].Id;
	
	private static testmethod void countDuplicatesPerCountry(){
		
		Country_Data_Stewards__c testDataSteward = new Country_Data_Stewards__c();
		testDataSteward.Name = 'Netherlands';
		testDataSteward.Data_Steward_Ids__c = UserInfo.getUserId();
		insert testDataSteward;
		
		User currentUser = [Select Id from User where Id = :UserInfo.getUserId()];
		
		System.runas(currentUser){
			
			Contact mdtEmployee = new Contact();
			mdtEmployee.Email = 'test@medtronic.com.sfdc';
			mdtEmployee.FirstName = 'Test';
			mdtEmployee.LastName = 'MDT Employee';
			mdtEmployee.RecordTypeId =mdtEmployeeTypeId;			
			mdtEmployee.SFDC_User__c = UserInfo.getUserId();
			
			createTestMasterData();
					
			Account acc1 = createDummyAccount(null);		
			Account acc2 = createDummyAccount('NETHERLANDS');
			
			List<Account> testAccounts = new List<Account>{acc1, acc2};		
			insert testAccounts;
			
			Contact cnt1 = createDummyContact(acc1.Id, 'NETHERLANDS');
			Contact cnt2 = createDummyContact(acc1.Id, 'NETHERLANDS');
			Contact cnt3 = createDummyContact(acc1.Id, null);
			Contact cnt4 = createDummyContact(acc2.Id, null);
			Contact cnt5 = createDummyContact(acc2.Id, null);
			Contact cnt6 = createDummyContact(acc2.Id, null);
			Contact cnt7 = createDummyContact(acc2.Id, 'NETHERLANDS');
			
			List<Contact> testContacts = new List<Contact>{mdtEmployee, cnt1, cnt2, cnt3, cnt4, cnt5, cnt6, cnt7};
			insert testContacts;
					
			TRIMDA__TRIMDA_MatchingResult__c dupe1 = new TRIMDA__TRIMDA_MatchingResult__c();
			dupe1.TRIMDA__ObjectName__c = 'Contact-Contact';
			dupe1.TRIMDA__Contact1__c = cnt1.Id;
			dupe1.TRIMDA__Contact2__c = cnt2.Id;
			dupe1.TRIMDA__Country__c = 'NETHERLANDS';
							
			TRIMDA__TRIMDA_MatchingResult__c dupe2 = new TRIMDA__TRIMDA_MatchingResult__c();
			dupe2.TRIMDA__ObjectName__c = 'Contact-Contact';
			dupe2.TRIMDA__Contact1__c = cnt5.Id;
			dupe2.TRIMDA__Contact2__c = cnt6.Id;
			dupe2.TRIMDA__Country_1__c = 'NETHERLANDS';
			
			TRIMDA__TRIMDA_MatchingResult__c dupe3 = new TRIMDA__TRIMDA_MatchingResult__c();
			dupe3.TRIMDA__ObjectName__c = 'Contact-Contact';
			dupe3.TRIMDA__Contact1__c = cnt6.Id;
			dupe3.TRIMDA__Contact2__c = cnt5.Id;
			dupe3.TRIMDA__Country_2__c = 'NETHERLANDS';
			
			TRIMDA__TRIMDA_MatchingResult__c dupe4 = new TRIMDA__TRIMDA_MatchingResult__c();
			dupe4.TRIMDA__ObjectName__c = 'Contact-Contact';
			dupe4.TRIMDA__Contact1__c = cnt6.Id;
			dupe4.TRIMDA__Contact2__c = cnt2.Id;
			
			TRIMDA__TRIMDA_MatchingResult__c dupe5 = new TRIMDA__TRIMDA_MatchingResult__c();
			dupe5.TRIMDA__ObjectName__c = 'Contact-Contact';
			dupe5.TRIMDA__Contact1__c = cnt1.Id;
			dupe5.TRIMDA__Contact2__c = cnt4.Id;
			
			TRIMDA__TRIMDA_MatchingResult__c dupe6 = new TRIMDA__TRIMDA_MatchingResult__c();
			dupe6.TRIMDA__ObjectName__c = 'Contact-Contact';
			dupe6.TRIMDA__Contact1__c = cnt7.Id;
			dupe6.TRIMDA__Contact2__c = cnt3.Id;
			
			TRIMDA__TRIMDA_MatchingResult__c dupe7 = new TRIMDA__TRIMDA_MatchingResult__c();
			dupe7.TRIMDA__ObjectName__c = 'Contact-Contact';
			dupe7.TRIMDA__Contact1__c = cnt4.Id;
			dupe7.TRIMDA__Contact2__c = cnt3.Id;		
			
			List<TRIMDA__TRIMDA_MatchingResult__c> dupes = new List<TRIMDA__TRIMDA_MatchingResult__c>{dupe1, dupe2, dupe3, dupe4, dupe5, dupe6, dupe7};
			insert dupes;
			
			Trimda_Config__c setting = new Trimda_Config__c();
			setting.Name = 'TRIMDA Config';
			setting.Included_Ids__c = '';
			insert setting;			 
								
			Test.startTest();
			
			ba_ContactsPerCountry batch = new ba_ContactsPerCountry(); 
			Database.executeBatch(batch);
			
			Test.stopTest();
			
			DIB_Country__c testCountry = [Select Id, Number_of_Contacts__c, Duplicate_Contacts__c, Percent_of_duplicates__c from DIB_Country__c LIMIT 1];
			
			system.debug('### ' + testCountry.Number_of_Contacts__c + ' '+ testCountry.Duplicate_Contacts__c + ' ' + testCountry.Percent_of_duplicates__c);
			System.assert(testCountry.Number_of_Contacts__c == 6);
			System.assert(testCountry.Duplicate_Contacts__c == 7);
			System.assert(testCountry.Percent_of_duplicates__c > 100);
		}
	}
	
	private static void createTestMasterData(){
		
		Company__c md = new Company__c();
		md.Name = 'Test';
		md.Company_Code_Text__c = 'TST';
		insert md;
		
		DIB_Country__c testCountry = new DIB_Country__c();
		testCountry.Company__c = md.Id;
		testCountry.Name = 'Netherlands';
		testCountry.Country_ISO_Code__c = 'TC';
		testCountry.CurrencyIsoCode = 'EUR';
		insert testCountry;		
	}
	
	private static Account createDummyAccount(String country){
		
		Account acc = new Account();
		acc.Account_Country_vs__c = country;
		acc.Name = 'Test Dummy '+country;
		acc.RecordTypeId = pendingAccountTypeId;
		
		return acc;
	}
	
	private static Contact createDummyContact(Id accountId, String country){
		
		Contact cnt = new Contact();
		cnt.FirstName = 'Test';
		cnt.LastName = 'Dummy Contact';
		cnt.AccountId = accountId;
		cnt.MailingCountry = country;
		cnt.RecordTypeId = genericContactTypeId;
		cnt.Contact_Department__c = 'Diabetes';
		cnt.Contact_Primary_Specialty__c = 'Diabetes';
		cnt.Affiliation_To_Account__c = 'Employee';
		cnt.Primary_Job_Title_vs__c = 'Surgeon';
		cnt.Contact_Gender__c = 'Male';
		
		return cnt;
	}
}