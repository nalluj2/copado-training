/*
 *      Created Date : 20160519
 *      Description : Batch to process Account DiB Sales Rep changes. Due to limitations on the Devart tool, we need to manually trigger the Territory Assignment Rules
 *		with this code on Account that have recently changed their designated Sales Rep.  
 * 
 *      Author = Jesus Lozano
 */
global class ba_Account_DiB_Territory_Rule_Trigger implements Schedulable,Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts {
    
    @TestVisible private List<String> errors = new List<String>();	

    private Datetime twoDaysAgo = Datetime.now().addDays(-2);
	public String query = 'Select AccountId from AccountHistory where Field IN (\'DIB_SAP_Sales_Rep_Id__c\', \'Diabetes__c\', \'SAP_Ord_Block_Reason__c\') AND CreatedBy.Profile.Name IN (\'Devart Tool\', \'SAP Interface\') AND CreatedDate >= :twoDaysAgo';

    private Set<Id> accountIdsProcessed = new Set<Id>();
           
    global void execute(SchedulableContext ctx){        
                               
        Database.executeBatch(new ba_Account_DiB_Territory_Rule_Trigger(), 200);    
    } 
    
    global Database.QueryLocator start(Database.BatchableContext ctx){    
                        
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext ctx, List<AccountHistory> records){
        
        List<Id> accIds = new List<Id>();
                
	    for(AccountHistory accHistory : records){
	        	
	       	if(accountIdsProcessed.add(accHistory.AccountId) == true){
	       		accIds.add(accHistory.AccountId);
	       	}
	    }
	        
	    if(accIds.size() > 0){
	        	
	      	try{      	
        	        	
	        	HttpResponse res;
	        	
	        	try{
	        		
	        		res = runAssignmentRules(accIds);
	        		
	        	}catch(CallOutException e){
	        		
	        		res = runAssignmentRules(accIds);
	        	}
	        	
	        	//Handle partial success
	        	List<Dom.XmlNode> resultElmts = res.getBodyDocument().getRootElement()
				  .getChildElement('Body','http://schemas.xmlsoap.org/soap/envelope/')
				  .getChildElement('updateResponse','urn:enterprise.soap.sforce.com')
				  .getChildElements();
				 
				Integer counter = 0;
				  
				for(Dom.XmlNode updtResult : resultElmts){
					
					String success = updtResult.getChildElement('success','urn:enterprise.soap.sforce.com').getText();
										
					if(success != 'true'){
						
						String recordId = accIds[counter];
						
						accountIdsProcessed.remove(recordId);        	
	        			errors.add('Error updating the Account with Id: ' + recordId + '. Error: ' + updtResult.getChildElement('errors','urn:enterprise.soap.sforce.com'));
					}	
					
					counter ++;
				}
	        	
	        }catch(Exception e){
        	
        		accountIdsProcessed.removeAll(accIds);        	
	        	errors.add(e.getMessage() + ' - ' + e.getStacktraceString());
    	    }	
	    }        	
    }
    
    global void finish(Database.BatchableContext ctx){
    	
    	if(errors.size() > 0){
	    	
	    	//The list of users to be notified is stored in a Public Group
	   		List<GroupMember> notifyUsers = [Select UserOrGroupId from GroupMember where Group.Name = 'Synchronization Service Admins'];
	   			
	   		if(notifyUsers.size()>0){
	   				
	   			List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();
	   				
	   			String emailBody = '';
	   				
	   			for(String errorMessage : errors){
	   				emailBody += errorMessage + ' <br/><br/> ';
	   			}
	   				
	   			for(GroupMember userMember : notifyUsers){
	   				
		   			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
					mail.setTargetObjectId(userMember.UserOrGroupId);
					mail.setSaveAsActivity(false);
					mail.setSubject('There have been errors in the DiB Account Territory Alignment process');
					mail.setHTMLBody('Error details : <br/><br/> '+ emailBody);
						
					messages.add(mail);
	   			}
					
				Messaging.sendEmail(messages);   				
	   		}   	
    	}
    }
    
    private HttpResponse runAssignmentRules(List<Id> accountIds){
    	
    	HttpRequest req = new HttpRequest();
        Http http = new Http();
        
        req.setHeader('Content-Type','text/xml;charset=UTF-8');        
        req.setHeader('Accept-Encoding','gzip,deflate');
		req.setHeader('Content-Type','text/xml;charset=UTF-8');
		req.setHeader('SOAPAction','""');
		req.setHeader('User-Agent','Jakarta Commons-HttpClient/3.1');
		        
        req.setEndpoint(Url.getOrgDomainUrl().toExternalForm() + '/services/Soap/c/27.0'); 
        req.setMethod('POST');
        req.setTimeout(120000);
        
        String envOpn =	'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"\n' +
  							' xmlns:urn="urn:enterprise.soap.sforce.com"\n' +
  							' xmlns:urn1="urn:sobject.enterprise.soap.sforce.com"\n' +
  							' xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">\n'; 
  		
  		String header =	'<soapenv:Header>\n' +
	     					' <urn:SessionHeader>\n' +
	        				' <urn:sessionId>' + UserInfo.getSessionId() + '</urn:sessionId>\n' +
	     					' </urn:SessionHeader>\n' +
	     					' <urn:AssignmentRuleHeader>\n' +         					 
         					' <urn:useDefaultRule>true</urn:useDefaultRule>\n' +
      						' </urn:AssignmentRuleHeader>\n' +
      						' <urn:AllOrNoneHeader>\n' +
         					' <urn:allOrNone>false</urn:allOrNone>\n' +
      						' </urn:AllOrNoneHeader>\n' +      							     					 
	  						' </soapenv:Header>\n';
	  						 
		String body = 	' <soapenv:Body>\n' +
							'<urn:update>\n';
       
       	for (Id accId : accountIds){
       		 	
       		body +=			'<urn:sObjects xsi:type="urn1:Account">\n'+
	           			 	'<Id>' + accId + '</Id>\n'+	           				 	
	        				'</urn:sObjects>\n';
       	}
       		
       	body += 			'</urn:update>\n' + 
       						' </soapenv:Body>\n';
       						
       	String envCls = '</soapenv:Envelope>\n';
        
        String fullMessage = envOpn + header + body + envCls;
        
        req.setBody(fullMessage);
        
        System.debug('req '+req);
        		
		req.setHeader('Content-Length',String.valueOf(fullMessage.length()));
		        
        HttpResponse response = http.send(req);
        
        if(response.getStatusCode() < 200 || response.getStatusCode() >= 300) throw new ws_Exception('Error during SOAP call: ' + response.getStatusCode() + '. ' +response.getBody());
        
        System.debug('Response ' + response);
        System.debug('Response ' + response.getBody());
        
        return response;           	    	
    }
}