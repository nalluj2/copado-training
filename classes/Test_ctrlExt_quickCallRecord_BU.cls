@isTest
private with sharing class Test_ctrlExt_quickCallRecord_BU {


	private static testmethod void createCallRecord(){
				
		Account acc = new Account();
    	acc.RecordTypeId = [select Id from RecordType where DeveloperName = 'CAN_Buying_Group' and sObjectType = 'Account' and isActive = true].Id;
    	acc.Name = 'UnitTest Account Name';
    	acc.Type = 'Buying Group';
    	acc.Account_Country_vs__c = 'CANADA';
    	
    	insert acc;
		
		Contact cnt = new Contact();
		cnt.AccountId = acc.Id;
		cnt.FirstName = 'Unit Test';
		cnt.LastName = 'Contact';		 
		cnt.Contact_Department__c = 'Cardiology';
		cnt.Contact_Primary_Specialty__c = 'Cardiology'; 
		cnt.Affiliation_To_Account__c = 'Employee';
		cnt.Primary_Job_Title_vs__c = 'Surgeon';
		cnt.Contact_Gender__c = 'Male';
		cnt.RecordTypeId = [select Id from RecordType where DeveloperName = 'Generic_Contact' and sObjectType = 'Contact' and isActive = true].Id;				
		insert cnt;
		
		initializeData();
		
		cnt = [Select Id, AccountId, Name from Contact where Id=:cnt.Id];
		
		ApexPages.standardController sc = new ApexPages.StandardController(cnt);
		
		ctrlExt_quickCallRecord_BU controller = new ctrlExt_quickCallRecord_BU(sc);
		
		System.assert(controller.activityTypes.size()==2);		
		controller.callTopic.Call_Activity_Type__c = controller.activityTypes[1].getValue();				
		controller.activityTypeChanged();
		
		System.assert(controller.subjects.size()==1);
		controller.selectedSubjects = new List<String>{controller.subjects[0].getValue()};
		
		System.assert(controller.therapies.size()==2);		
		controller.calltopic.Call_Topic_Therapy__c = controller.therapies[1].getValue();
		controller.therapyChanged();
		
		System.assert(controller.products.size()==1);
		controller.selectedProducts = new List<String>{controller.products[0].getValue()};

		controller.callTopic.Call_Topic_Duration__c = 50;
		controller.callTopic.Call_Topic_Lobbying__c  = 'Selling';
		
		controller.save();

		System.assert(controller.isSaveError == false);
	} 
	
	private static testmethod void testValidation(){
		
		Account acc = new Account();
    	acc.RecordTypeId = [select Id from RecordType where DeveloperName = 'CAN_Buying_Group' and sObjectType = 'Account' and isActive = true].Id;
    	acc.Name = 'UnitTest Account Name';
    	acc.Type = 'Buying Group';
    	acc.Account_Country_vs__c = 'CANADA';
    	
    	insert acc;
		
		Contact cnt = new Contact();
		cnt.AccountId = acc.Id;
		cnt.FirstName = 'Unit Test';
		cnt.LastName = 'Contact';		 
		cnt.Contact_Department__c = 'Cardiology';
		cnt.Contact_Primary_Specialty__c = 'Cardiology'; 
		cnt.Affiliation_To_Account__c = 'Employee';
		cnt.Primary_Job_Title_vs__c = 'Surgeon';
		cnt.Contact_Gender__c = 'Male';
		cnt.RecordTypeId = [select Id from RecordType where DeveloperName = 'Generic_Contact' and sObjectType = 'Contact' and isActive = true].Id;				
		insert cnt;
		
		cnt = [Select Id, AccountId, Name from Contact where Id=:cnt.Id];
		
		ApexPages.standardController sc = new ApexPages.StandardController(cnt);
		
		ctrlExt_quickCallRecord_BU controller = new ctrlExt_quickCallRecord_BU(sc);
		controller.productMandatory = true;
		controller.subjectMandatory = true;
		controller.durationMandatory = true;
		controller.lobbyingMandatory = true;
		
		controller.callRecord.Call_Date__c = null;
		controller.callTopic.Call_Topic_Duration__c = null;
		controller.selectedProducts = new List<String>();
		controller.selectedSubjects = new List<String>();
		
		controller.save();
		
		System.assert(ApexPages.getMessages().size()>0);
	}
	
	private static void initializeData(){
		
		//Master Data
		Company__c cmpny = new Company__c();		
        cmpny.name='Europe';
        cmpny.CurrencyIsoCode = 'EUR';
        cmpny.Current_day_in_Q1__c=56;
        cmpny.Current_day_in_Q2__c=34; 
        cmpny.Current_day_in_Q3__c=5; 
        cmpny.Current_day_in_Q4__c= 0;   
        cmpny.Current_day_in_year__c =200;
        cmpny.Days_in_Q1__c=56;  
        cmpny.Days_in_Q2__c=34;
        cmpny.Days_in_Q3__c=13;
        cmpny.Days_in_Q4__c=22;
        cmpny.Days_in_year__c =250;
        cmpny.Company_Code_Text__c = 'EUR';
		insert cmpny;
		
		//Business Unit
		Business_Unit__c bu =  new Business_Unit__c();
        bu.Company__c =cmpny.id;
        bu.name='BUMedtronic Account Performance';
        bu.Account_Plan_Activities__c=true;        
        insert bu;
        
        //Sub-Business Unit
        Sub_Business_Units__c sbu = new Sub_Business_Units__c();
        sbu.name='SBUMedtronic1 Account Performance1236';
        sbu.Business_Unit__c=bu.id;
        sbu.Account_Plan_Default__c='Revenue';
        sbu.Account_Flag__c = 'AF_Solutions__c';
		insert sbu;	
		
		//User business Units
		User_Business_Unit__c uSBU = new User_Business_Unit__c();
		uSBU.User__c = UserInfo.getUserId();
		uSBU.Primary__c = true;
		uSBU.Sub_Business_Unit__c = sbu.Id;
		insert uSBU;
		
		//Activity Type
		Call_Activity_Type__c actType = new Call_Activity_Type__c();
		actType.Name = 'Test Activity';
		actType.Active__c = true;
		actType.Company_ID__c = cmpny.Id;		
		insert actType;
		
		//Category
		Call_Category__c cat = new Call_Category__c();
		cat.Company_ID__c = cmpny.Id;
		cat.Name = 'Test Category';		
		insert cat;
		
		//Activity Type - Business Unit
		Call_Topic_Settings__c setting = new Call_Topic_Settings__c();
		setting.Business_Unit__c = bu.Id;
		setting.Call_Activity_Type__c = actType.Id;
		setting.Call_Category__c = cat.Id;
		setting.Region_vs__c = 'Europe';
		insert setting;
		
		//Therapy Group
		Therapy_Group__c therGroup = new Therapy_Group__c();
		therGroup.Name = 'Test Therapy Group';
		therGroup.Company__c = cmpny.Id;
		therGroup.Sub_Business_Unit__c = sbu.Id;
		insert therGroup;
		
		//Therapy
		Therapy__c therapy = new Therapy__c();
		therapy.Active__c = true;
		therapy.Business_Unit__c = bu.Id;
		therapy.Name = 'Test Therapy';
		therapy.Sub_Business_Unit__c = sbu.Id;
		therapy.Therapy_Group__c = therGroup.Id;
		therapy.Therapy_Name_Hidden__c = 'whatever';
		insert therapy;
		
		//Subject
		Subject__c subject = new Subject__c();
		subject.Company_ID__c = cmpny.Id;
		subject.Name = 'Test subject';
		insert subject;
				
		//Activity Type Subject
		Call_topic_Setting_Details__c actTypeSubject = new Call_topic_Setting_Details__c();
		actTypeSubject.Call_Topic_Setting__c = setting.Id;
		actTypeSubject.Subject__c = subject.Id;
		insert actTypeSubject;
		
		//Product
		Product2 product = new Product2();
		product.Name = 'UnitTest Product';
		product.RecordTypeId = [Select Id from RecordType where sObjectType='Product2' AND developerName = 'MDT_Marketing_Product' AND isActive=true].Id;
		insert product;
		
		//Therapy Product
		Therapy_Product__c therProduct = new Therapy_Product__c();
		therProduct.Product__c = product.Id;
		therProduct.Therapy__c = therapy.Id;
		insert therProduct;		
	}
}