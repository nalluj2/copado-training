public class bl_Contract_Repository_Account {
	
	public static void setUniqueKey(List<Contract_Repository_Account__c> triggerNew){
		
		for(Contract_Repository_Account__c contractAcc : triggerNew){
			
			contractAcc.Unique_Key__c = contractAcc.Contract_Repository__c + ':' + contractAcc.Account__c;
		}
	}
	
	public static void blockMainAccount(List<Contract_Repository_Account__c> triggerNew, Map<Id, Contract_Repository_Account__c> oldMap){
		
		Set<Id> contractIds = new Set<Id>();
		
		for(Contract_Repository_Account__c contractAcc : triggerNew){
			
			contractIds.add(contractAcc.Contract_Repository__c);
		}
		
		Map<Id, Contract_Repository__c> contracts = new Map<Id, Contract_Repository__c>([Select Id, Account__c from Contract_Repository__c where Id IN :contractIds]);
		
		for(Contract_Repository_Account__c contractAcc : triggerNew){
			
			Contract_Repository__c contract = contracts.get(contractAcc.Contract_Repository__c);
			
			if((oldMap == null && contractAcc.Account__c == contract.Account__c) || (oldMap != null && oldMap.get(contractAcc.Id).Account__c == contract.Account__c && contractAcc.Account__c != contract.Account__c)){
				
				contractAcc.addError('You cannot delete or modify the record for the Contract\'s primary account');
			}
		}
	}    
}