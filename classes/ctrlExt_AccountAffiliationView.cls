public with sharing class ctrlExt_AccountAffiliationView {

    //------------------------------------------------------------------------------
    // Private variables
    //------------------------------------------------------------------------------
    private final Account oAccount;
    private final Id id_Account;
    private final String tNone = '----None----';

    private User oUser;
    private Map<Id, Id> mapAccountRecordType_ContactRecordType;
    private Map<Id, Business_Unit__c> mapBusinessUnit_All;
    private Map<Id, Sub_Business_Units__c> mapSubBusinessUnit_All;
    private List<Sub_Business_Units__c> lstSubBusinessUnit_All;
    private Id id_RecordType_C2A;
    private Id id_RecordType_A2A;
    private String tFilter_C2A = '';
    private Integer iPageSize_C2A = 5;
    private Integer iPageSize_A2A = 5;
    //------------------------------------------------------------------------------



    //------------------------------------------------------------------------------
    // Constructor
    //------------------------------------------------------------------------------
    public ctrlExt_AccountAffiliationView(ApexPages.StandardController oStdController) {
        
        if (!Test.isRunningTest()){
            List<String> lstField = new List<String>{'Account_Country_vs__c', 'RecordTypeId'};
            oStdController.addFields(lstField);
        }

        oAccount = (Account)oStdController.getRecord();
        id_Account = oAccount.Id;

        initialize();

    }
    //------------------------------------------------------------------------------



    //------------------------------------------------------------------------------
    // Getters & Setters - General
    //------------------------------------------------------------------------------
    public Id id_AssignedAffiliation { get; set; }

    public Boolean bShowFilter { get; set; }
    public Boolean bSaveFilter { get; set; }

    public String tShowHideFilterLabel { 
        get{
            if (bShowFilter){
                return Label.Hide_Filters;
            }else{
                return Label.Show_Filters;
            }
        }
        set; 
    }

    public List<SelectOption> lstPageSize { get; set; }
    //------------------------------------------------------------------------------



    //------------------------------------------------------------------------------
    // Getters & Setters - Contact Affiliations (C2A)
    //------------------------------------------------------------------------------
    public String tSortField_C2A { get; set; }
    public String tPrevSortField_C2A { get; set; }
    public String tSortOrder_C2A { get; set; }
    public String tWarning_C2A { get; private set; }

    public List<SObject> lstRecord_C2A {
        get {
            return oSetCtrl_C2A.getRecords();
        }
    }   

    public Boolean hasNext_C2A {
        get {
            return oSetCtrl_C2A.getHasNext();
        }
        set;
    }

    public Boolean hasPrevious_C2A {
        get {
            return oSetCtrl_C2A.getHasPrevious();
        }
        set;
    }

    public Integer pageNumber_C2A {
        get {
            return oSetCtrl_C2A.getPageNumber();
        }
        set{
            oSetCtrl_C2A.setPageNumber(value);
        }
    }

    public Integer totalPages_C2A {
        get {
            if (oSetCtrl_C2A.getResultSize() == 0) return 1;
            Decimal decRecNum =  oSetCtrl_C2A.getResultSize();
            Decimal decRecPage = oSetCtrl_C2A.getPageSize();
            Decimal decTotal = decRecNum.divide(decRecPage, 2);
            return Integer.valueOf(decTotal.round(System.RoundingMode.UP));
        }
        set;
    }

    public Integer pageSize_C2A {
        get {
            return oSetCtrl_C2A.getPageSize();
        }
        set{
            iPageSize_C2A = value;
            oSetCtrl_C2A.setPageSize(value);
        }
    }

    public Integer resultSize_C2A {
        get {
            return oSetCtrl_C2A.getResultSize();
        }
        set;
    }

    public void first_C2A() {
        oSetCtrl_C2A.first();
    }

    public void last_C2A() {
        oSetCtrl_C2A.last();
    }

    public void previous_C2A() {
        oSetCtrl_C2A.previous();
    }

    public void next_C2A() {
        oSetCtrl_C2A.next();
    }  
    //------------------------------------------------------------------------------



    //------------------------------------------------------------------------------
    // Getters & Setters - Account Affiliations (A2A)
    //------------------------------------------------------------------------------
    public String tSortField_A2A { get; set; }
    public String tPrevSortField_A2A { get; set; }
    public String tSortOrder_A2A { get; set; }
    public String tWarning_A2A { get; private set; }

    public List<SObject> lstRecord_A2A {
        get {
            return oSetCtrl_A2A.getRecords();
        }
    }   

    public Boolean hasNext_A2A {
        get {
            return oSetCtrl_A2A.getHasNext();
        }
        set;
    }

    public Boolean hasPrevious_A2A {
        get {
            return oSetCtrl_A2A.getHasPrevious();
        }
        set;
    }

    public Integer pageNumber_A2A {
        get {
            return oSetCtrl_A2A.getPageNumber();
        }
        set{
            oSetCtrl_A2A.setPageNumber(value);
        }
    }

    public Integer totalPages_A2A {
        get {
            if (oSetCtrl_A2A.getResultSize() == 0) return 1;
            Decimal decRecNum =  oSetCtrl_A2A.getResultSize();
            Decimal decRecPage = oSetCtrl_A2A.getPageSize();
            Decimal decTotal = decRecNum.divide(decRecPage, 2);
            return Integer.valueOf(decTotal.round(System.RoundingMode.UP));
        }
        set;
    }

    public Integer pageSize_A2A {
        get {
            return oSetCtrl_A2A.getPageSize();
        }
        set{
            iPageSize_A2A = value;
            oSetCtrl_A2A.setPageSize(value);
        }
    }

    public Integer resultSize_A2A {
        get {
            return oSetCtrl_A2A.getResultSize();
        }
        set;
    }

    public void first_A2A() {
        oSetCtrl_A2A.first();
    }

    public void last_A2A() {
        oSetCtrl_A2A.last();
    }

    public void previous_A2A() {
        oSetCtrl_A2A.previous();
    }

    public void next_A2A() {
        oSetCtrl_A2A.next();
    }  
    //------------------------------------------------------------------------------


    //------------------------------------------------------------------------------
    // Getters & Setters - Filter Related
    //------------------------------------------------------------------------------
    public Affiliation__c oAffiliation_Filter { get; set; }

    public List<String> lstPrimarySpeciality_Selected { get; set; }
    public List<SelectOption> lstSO_PrimarySpeciality { 
        get{
            return loadFilterData_PrimarySpeciality();
        } 
        private set; 
    }

    public String tSelected_BusinessUnit { get; set; }
    public List<SelectOption> lstSO_BusinessUnit { 
        get{
            return loadFilterData_BusinessUnit();
        } 
        private set;
    }

    public String tSelected_SubBusinessUnit { get; set; }
    public List<SelectOption> lstSO_SubBusinessUnit { 
        get{
            return loadFilterData_SubBusinessUnit();
        }
        private set; 
    }

    public String tSelected_Department { get; set; }
    public List<SelectOption> lstSO_Department { 
        get{
            return loadFilterData_Department();
        }
        private set; }

    public String tSelected_ActiveInactive { get; set; }
    public List<SelectOption> lstSO_ActiveInactive { 
        get{
            return loadFilterData_ActiveInactive();
        }
        private set; 
    }

    public boolean bRenderAccountHierarchyButton { get; private set; }
    //------------------------------------------------------------------------------



    //------------------------------------------------------------------------------
    // Actions - C2A
    //------------------------------------------------------------------------------
    public void sortData_C2A() {

        if (tSortField_C2A == tPrevSortField_C2A){
            if (tSortOrder_C2A == 'ASC'){
                tSortOrder_C2A = 'DESC';
            }else{
                tSortOrder_C2A = 'ASC';
            }
        }else{
            tSortOrder_C2A = 'ASC';
            tPrevSortField_C2A = tSortField_C2A;
        }

        oSetCtrl_C2A = new ApexPages.StandardSetController(Database.getQueryLocator(tBuildQuery('C2A', true)));
        oSetCtrl_C2A.setPageSize(iPageSize_C2A);

    }

    public PageReference viewAffiliation_C2A(){

        PageReference oPageRef;

        if ( !clsUtil.isBlank(id_AssignedAffiliation) ){

            oPageRef = new PageReference('/' + id_AssignedAffiliation);
            oPageRef.setRedirect(true);

        }

        return oPageRef;

    }   

    public PageReference createAffiliation_C2A(){

        PageReference oPageRef;

        oPageRef = new PageReference('/apex/createC2AAffiliationPage');
            oPageRef.getParameters().put('RecordType', id_RecordType_C2A);
            oPageRef.getParameters().put(FinalConstants.accountToFieldId, id_Account);
            oPageRef.getParameters().put('retURL', '/apex/AccountAffiliationView');
            oPageRef.getParameters().put('retID', id_Account);
            oPageRef.getParameters().put('sfdc.override', '1');
        oPageRef.setRedirect(true);
        
        return oPageRef;

    }

    public PageReference editAffiliation_C2A(){

        PageReference oPageRef;

        if ( !clsUtil.isBlank(id_AssignedAffiliation) ){
            
            List<Affiliation__c> lstAffiliation_Tmp = [SELECT Id FROM Affiliation__c WHERE Affiliation_Primary__c = true AND Id = :id_AssignedAffiliation];
            
            if (lstAffiliation_Tmp.size() > 0){
            
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.You_can_only_change_primary_relationships_via_the_contact_detail_page));                
            
            }else{  
    
                oPageRef = new PageReference('/' + id_AssignedAffiliation +'/e');
                    oPageRef.getParameters().put('retURL', '/apex/AccountAffiliationView?Id=' + id_Account);
                oPageRef.setRedirect(true);
    
            }                   
    
        }

        return oPageRef;
    }

    public void deleteAffiliation_C2A(){

        if ( !clsUtil.isBlank(id_AssignedAffiliation) ){

            try{

                clsUtil.bubbleException();
    
                List<Affiliation__c> lstAffiliation_Tmp = [SELECT Id, Affiliation_Primary__c FROM Affiliation__c WHERE  Id = :id_AssignedAffiliation];

                if (lstAffiliation_Tmp.size() == 1){

                    if (lstAffiliation_Tmp[0].Affiliation_Primary__c == true){

                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, Label.You_can_only_change_primary_relationships_via_the_contact_detail_page));              
        
                    }else{          

                        delete lstAffiliation_Tmp;
                        oSetCtrl_C2A = null;

                    }

                }    

            }catch(Exception oEX){

                    if (oEX.getMessage().contains('INSUFFICIENT_ACCESS_OR_READONLY')){ 
                
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.Insufficient_Privileges));
                
                }else{            
                
                    ApexPages.addMessages(oEX); 
                
                } 
         
            }

        }

    }

    public PageReference createContact(){

        PageReference oPageRef = new PageReference('/003/e');

        oPageRef.getParameters().put('con4_lkid', id_Account);
            if (mapAccountRecordType_ContactRecordType.containsKey(oAccount.RecordTypeId)){
                oPageRef.getParameters().put('RecordType', mapAccountRecordType_ContactRecordType.get(oAccount.RecordTypeId));
            }
            oPageRef.getParameters().put('retURL', '/apex/AccountAffiliationView?Id=' + id_Account);
        oPageRef.setRedirect(true);

        return oPageRef;

    }

    // Apply Filter Values
    public void applyFilter(){

        tFilter_C2A = '';

        // Department
        if ( clsUtil.isNull(tSelected_Department, tNone) != tNone ){
            if (tFilter_C2A != '') tFilter_C2A += ' AND ';
            tFilter_C2A += ' Department__c = :tSelected_Department';
        }

        // Primary Speciality
        for (Integer i = 0; i < lstPrimarySpeciality_Selected.size(); i++){
            if ( clsUtil.isNull(lstPrimarySpeciality_Selected[i], tNone) == tNone ){
                lstPrimarySpeciality_Selected.remove(i);
                break;
            }
        }
        if (lstPrimarySpeciality_Selected.size() > 0){
            if (tFilter_C2A != '') tFilter_C2A += ' AND ';
            tFilter_C2A += ' Contact_Primary_Specialty__c in :lstPrimarySpeciality_Selected';
        }

        // Active / Inactive
        if ( clsUtil.isNull(tSelected_ActiveInactive, tNone) != tNone ){
            if (tFilter_C2A != '') tFilter_C2A += ' AND ';
            Boolean bIsActive = false;
            if (tSelected_ActiveInactive.toUpperCase() == 'ACTIVE'){
                bIsActive = true;
            }
            tFilter_C2A += ' Affiliation_Active__c = ' + bIsActive;
        }

        // Relationship End Date After
        if (oAffiliation_Filter.Affiliation_End_Date__c != null){
            String tContactDateFilter = clsUtil.tFormatDate_YYYYMMDD(oAffiliation_Filter.Affiliation_End_Date__c, '-');
            if (tFilter_C2A != '') tFilter_C2A += ' AND ';
            tFilter_C2A += ' (Affiliation_End_Date__c = null OR Affiliation_End_Date__c > ' + tContactDateFilter + ')';
        }        


        // Business Unit / Sub Business Unit
        // Business Unit
        if ( clsUtil.isNull(tSelected_BusinessUnit, tNone) != tNone ){

            String tBU_ContactFlagField = '';
        
            if ( mapBusinessUnit_All.containsKey(tSelected_BusinessUnit) ){
                if ( !clsUtil.isBlank(mapBusinessUnit_All.get(tSelected_BusinessUnit).Contact_Flag__c) ){
                    tBU_ContactFlagField = mapBusinessUnit_All.get(tSelected_BusinessUnit).Contact_Flag__c;
                }
            }

            if ( !String.isBlank(tBU_ContactFlagField) ){
                if (tFilter_C2A != '') tFilter_C2A += ' AND ';
                tFilter_C2A += ' Affiliation_From_Contact__r.' + tBU_ContactFlagField + ' = true';
            }

        }

        // Sub Business Unit
        if ( clsUtil.isNull(tSelected_SubBusinessUnit, tNone) != tNone ){

            String tSBU_ContactFlagField = '';
        
            if ( mapSubBusinessUnit_All.containsKey(tSelected_SubBusinessUnit) ){
                if ( !clsUtil.isBlank(mapSubBusinessUnit_All.get(tSelected_SubBusinessUnit).Contact_Flag__c) ){
                    tSBU_ContactFlagField = mapSubBusinessUnit_All.get(tSelected_SubBusinessUnit).Contact_Flag__c;
                }
            }

            if ( !String.isBlank(tSBU_ContactFlagField) ){
                if (tFilter_C2A != '') tFilter_C2A += ' AND ';
                tFilter_C2A += ' Affiliation_From_Contact__r.' + tSBU_ContactFlagField + ' = true';
            }

        }

        if (bSaveFilter) saveFilter();

        oSetCtrl_C2A = null;

    }

    public void showHideFilter(){

        bShowFilter =! bShowFilter;

    }
    //------------------------------------------------------------------------------


    //------------------------------------------------------------------------------
    // Actions - A2A
    //------------------------------------------------------------------------------
    public void sortData_A2A() {

        if (tSortField_A2A == tPrevSortField_A2A){
            if (tSortOrder_A2A == 'ASC'){
                tSortOrder_A2A = 'DESC';
            }else{
                tSortOrder_A2A = 'ASC';
            }
        }else{
            tSortOrder_A2A = 'ASC';
            tPrevSortField_A2A = tSortField_A2A;
        }

        oSetCtrl_A2A = new ApexPages.StandardSetController(Database.getQueryLocator(tBuildQuery('A2A', true)));
        oSetCtrl_A2A.setPageSize(iPageSize_A2A);

    }

    public PageReference viewAffiliation_A2A(){

        PageReference oPageRef;

        if ( !clsUtil.isBlank(id_AssignedAffiliation) ){

            oPageRef = new PageReference('/' + id_AssignedAffiliation);
            oPageRef.setRedirect(true);

        }

        return oPageRef;

    }   

    public PageReference createAffiliation_A2A(){

        PageReference oPageRef;

        Id id_RecordType_A2A = clsUtil.getRecordTypeByDevName('Affiliation__c', 'A2A').Id;

        //oPageRef = new PageReference('/apex/createA2AAffiliationPage');
        oPageRef = new PageReference('/apex/createAccountRelationshipA2A');
            oPageRef.getParameters().put('RecordType', id_RecordType_A2A);
            oPageRef.getParameters().put(FinalConstants.accountFromFieldId, id_Account);
            oPageRef.getParameters().put('retURL', '/apex/AccountAffiliationView');
            oPageRef.getParameters().put('retID', id_Account);
            oPageRef.getParameters().put('sfdc.override', '1');
        oPageRef.setRedirect(true);       

        return oPageRef;  

    }

    public PageReference editAffiliation_A2A(){

        PageReference oPageRef;

        if ( !clsUtil.isBlank(id_AssignedAffiliation) ){
            
            List<Affiliation__c> lstAffiliation_Tmp = [SELECT Id FROM Affiliation__c WHERE Affiliation_Primary__c = true AND Id = :id_AssignedAffiliation];
            
            if (lstAffiliation_Tmp.size() > 0){
            
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.You_can_only_change_primary_relationships_via_the_contact_detail_page));                
            
            }else{  
    
                //oPageRef = new PageReference('/' + id_AssignedAffiliation +'/e');
                //oPageRef.getParameters().put('retURL', '/apex/AccountAffiliationView?Id=' + id_Account);
                //oPageRef.setRedirect(true);
                
                oPageRef = new PageReference('/apex/createAccountRelationshipA2A');
            	oPageRef.getParameters().put('Id', id_AssignedAffiliation);
                oPageRef.getParameters().put('retURL', '/apex/AccountAffiliationView');
            	oPageRef.getParameters().put('retID', id_Account);
            	oPageRef.getParameters().put('sfdc.override', '1');
        		oPageRef.setRedirect(true);
            }                   
    
        }

        return oPageRef;
    }

    public void deleteAffiliation_A2A(){

        if ( !clsUtil.isBlank(id_AssignedAffiliation) ){

            try{

                clsUtil.bubbleException();
    
                List<Affiliation__c> lstAffiliation_Tmp = [SELECT Id, Affiliation_Primary__c FROM Affiliation__c WHERE  Id = :id_AssignedAffiliation];

                if (lstAffiliation_Tmp.size() == 1){

                    if (lstAffiliation_Tmp[0].Affiliation_Primary__c == true){

                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, Label.You_can_only_change_primary_relationships_via_the_contact_detail_page));              
        
                    }else{          

                        delete lstAffiliation_Tmp;
                        oSetCtrl_A2A = null;

                    }

                }    

            }catch(Exception oEX){

                if (oEX.getMessage().contains('INSUFFICIENT_ACCESS_OR_READONLY')){ 
                
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.Insufficient_Privileges));
                
                }else{            
                
                    ApexPages.addMessages(oEX); 
                
                } 
         
            }

        }

    }   
    //------------------------------------------------------------------------------



    //------------------------------------------------------------------------------
    // Private Methods
    //------------------------------------------------------------------------------
    @TestVisible private void initialize(){

        // Load User Related Data
        oUser = [SELECT Id, Relationship_Filter_Setting__c, Company_Code_Text__c FROM User WHERE Id = :UserInfo.getUserId()]; 

        lstPageSize = new List<SelectOption>();
            lstPageSize.add(new SelectOption('5', '5'));
            lstPageSize.add(new SelectOption('10', '10'));
            lstPageSize.add(new SelectOption('15', '15'));
            lstPageSize.add(new SelectOption('25', '25'));
            lstPageSize.add(new SelectOption('50', '50'));
            lstPageSize.add(new SelectOption('100', '100'));

        // Collect the Account Recordtype - Contact Recordtype mapping
        mapAccountRecordType_ContactRecordType = new Map<Id, Id>();
        Id idRecordType_Account;
        Id idRecordType_Contact;
        if (clsUtil.getRecordTypeByDevName('Account', 'MDT_Account') != null){
            idRecordType_Account = clsUtil.getRecordTypeByDevName('Account', 'MDT_Account').Id;
        }
        if (clsUtil.getRecordTypeByDevName('Contact', 'MDT_Employee') != null){
            idRecordType_Contact = clsUtil.getRecordTypeByDevName('Contact', 'MDT_Employee').Id;
        }
        if (idRecordType_Account != null && idRecordType_Contact != null){
            mapAccountRecordType_ContactRecordType.put(idRecordType_Account, idRecordType_Contact);
        }

        idRecordType_Account = null;
        idRecordType_Contact = null;
        if (clsUtil.getRecordTypeByDevName('Account', 'Distributor') != null){
            idRecordType_Account = clsUtil.getRecordTypeByDevName('Account', 'Distributor').Id;
        }
        if (clsUtil.getRecordTypeByDevName('Contact', 'MDT_Agent_Distributor') != null){
            idRecordType_Contact = clsUtil.getRecordTypeByDevName('Contact', 'MDT_Agent_Distributor').Id;
        }
        if (idRecordType_Account != null && idRecordType_Contact != null){
            mapAccountRecordType_ContactRecordType.put(idRecordType_Account, idRecordType_Contact);
        }

        id_RecordType_C2A = clsUtil.getRecordTypeByDevName('Affiliation__c', 'C2A').Id;
        id_RecordType_A2A = clsUtil.getRecordTypeByDevName('Affiliation__c', 'A2A').Id;

        tSortOrder_C2A = 'ASC';
        tWarning_C2A = '';
        tSortOrder_A2A = 'ASC';
        tWarning_A2A = '';

        bRenderAccountHierarchyButton = false;
        List<DIB_Country__c> lstDIBCountry = [SELECT Show_Account_Hierarchy_Button__c FROM DIB_Country__c WHERE Name = :oAccount.Account_Country_vs__c];
        if (lstDIBCountry.size() == 1){
            bRenderAccountHierarchyButton = lstDIBCountry[0].Show_Account_Hierarchy_Button__c;
        }

        initializeFilter();

    }

    private void initializeFilter(){

        bShowFilter = false;
        bSaveFilter = false;

        lstPrimarySpeciality_Selected = new List<String>();
        tSelected_BusinessUnit = tNone;
        tSelected_SubBusinessUnit = tNone;
        tSelected_Department = tNone;
        tSelected_ActiveInactive = tNone;

        // Initialize oAffiliation_Filter and set the End Date
        oAffiliation_Filter = new Affiliation__c();
//      oAffiliation_Filter.Affiliation_End_Date__c = Date.today();

        mapBusinessUnit_All = new Map<Id, Business_Unit__c>();
        mapSubBusinessUnit_All = new Map<Id, Sub_Business_Units__c>();

        loadMasterData();

        // Read Cookie Data
        loadFilter();   // Read User Default Filter Settings

        applyFilter();

    }

    @TestVisible private void loadFilter(){

        String tFilterSetting = oUser.Relationship_Filter_Setting__c;

        // Get the Filter Data from the Cookie
        if ( !clsUtil.isBlank(tFilterSetting) ){

            try{

                // Read the filter data and put it in a map
                Set<String> setFilterId = new Set<String>{'##0', '##1', '##2', '##A', '##B', '##C', '##D', '##E', '##F'};
                Map<String, String> mapFilterData = new Map<String, String>();
                for (String tFilterId : setFilterId){

                    Integer iIndex = tFilterSetting.indexOf(tFilterId);
                    if (iIndex >= 0){
                        Integer iNextIndex = tFilterSetting.indexOf('##', iIndex + tFilterId.length());
                        
                        Integer iLength = 0;
                        if (iNextIndex > 0){
                            iLength = iNextIndex - iIndex - tFilterId.length();
                        }else{ 
                            iLength = 100;
                        }
                        
                        mapFilterData.put(tFilterId, tFilterSetting.mid(iIndex + tFilterId.length(), iLength));
                    }

                }

                // Pagesize Grid C2A
                if (mapFilterData.containsKey('##0')){
                    try{ iPageSize_C2A = Integer.valueOf(mapFilterData.get('##0')); }catch(Exception oEX){}
                }
                // Pagesize Grid A2A
                if (mapFilterData.containsKey('##1')){
                    try{ iPageSize_A2A = Integer.valueOf(mapFilterData.get('##1')); }catch(Exception oEX){}
                }


                // Filter Data for Business Unit
                if (mapFilterData.containsKey('##A')){
                    tSelected_BusinessUnit = mapFilterData.get('##A');
                    bShowFilter = true;
                }

                // Filter Data for Sub Business Unit
                if (mapFilterData.containsKey('##B')){
                    tSelected_SubBusinessUnit = mapFilterData.get('##B');
                    bShowFilter = true;
                }

                // Filter Data for Contact Active Inactive
                if (mapFilterData.containsKey('##C')){
                    tSelected_ActiveInactive = mapFilterData.get('##C');
                    bShowFilter = true;
                }

                // Filter Data for Primary Speciality
                if (mapFilterData.containsKey('##D')){
                    lstPrimarySpeciality_Selected = mapFilterData.get('##D').split(';');
                    bShowFilter = true;
                }

                // Filter Data Department
                if (mapFilterData.containsKey('##E')){
                    tSelected_Department = mapFilterData.get('##E');
                    bShowFilter = true;
                }

                // Filter Data Date
                oAffiliation_Filter.Affiliation_End_Date__c = null;
                if (mapFilterData.containsKey('##F')){
                    String tContactDate = mapFilterData.get('##F');
                    if (tContactDate.length() == 8){
                        // The date in this Cookie field is always YYYYMMDD
                        oAffiliation_Filter.Affiliation_End_Date__c = Date.newInstance(Integer.valueOf(tContactDate.substring(0,4)), Integer.valueOf(tContactDate.substring(4,6)), Integer.valueOf(tContactDate.substring(6,8)));
                        bShowFilter = true;
                    }

                }

            }catch(Exception oEX){
            }

        }

    }

    @TestVisible private void saveFilter(){

        // Save the filter data
        String tFilterSetting = '';

        // Pagesize Grid C2A
        tFilterSetting += '##0' + iPageSize_C2A;
        // Pagesize Grid A2A
        tFilterSetting += '##1' + iPageSize_A2A;


        // Filter Data for Business Unit
        if ( clsUtil.isNull(tSelected_BusinessUnit, tNone) != tNone ){
            tFilterSetting += '##A' + tSelected_BusinessUnit;
        }

        // Filter Data for Sub Business Unit
        if ( clsUtil.isNull(tSelected_SubBusinessUnit, tNone) != tNone ){
            tFilterSetting += '##B' + tSelected_SubBusinessUnit;
        }

        // Filter Data for Contact Active Inactive
        if ( clsUtil.isNull(tSelected_ActiveInactive, tNone) != tNone ){
            tFilterSetting += '##C' + tSelected_ActiveInactive;
        }

        // Filter Data for Primary Speciality
        for (Integer i = 0; i < lstPrimarySpeciality_Selected.size(); i++){
            if ( clsUtil.isNull(lstPrimarySpeciality_Selected[i], tNone) == tNone ){
                lstPrimarySpeciality_Selected.remove(i);
                break;
            }
        }
        if (lstPrimarySpeciality_Selected.size() > 0){
            tFilterSetting += '##D' + String.join(lstPrimarySpeciality_Selected, ';');
        }


        // Filter Data Department
        if ( clsUtil.isNull(tSelected_Department, tNone) != tNone ){
            tFilterSetting += '##E' + tSelected_Department;
        }

        // Filter Data Date
        if (oAffiliation_Filter.Affiliation_End_Date__c != null){
            // The date in this Cookie field is always YYYYMMDD             
            tFilterSetting += '##F' + clsUtil.tFormatDate_YYYYMMDD(oAffiliation_Filter.Affiliation_End_Date__c, '-');
        }

            oUser.Relationship_Filter_Setting__c = tFilterSetting;
        update oUser;
        
    }

    private ApexPages.StandardSetController oSetCtrl_C2A {
        
        get {
            if (oSetCtrl_C2A == null) {
                
                oSetCtrl_C2A = new ApexPages.StandardSetController(Database.getQueryLocator(tBuildQuery('C2A', true)));
                if (oSetCtrl_C2A.getResultSize() == 10000){
                    tWarning_C2A = 'Only the first 10.000 records are selected and visible.  Please apply filters to select the other records.';
                }
                oSetCtrl_C2A.setPageSize(iPageSize_C2A);
            }
            
            return oSetCtrl_C2A;
        }
        set;

    }

    private ApexPages.StandardSetController oSetCtrl_A2A {
        
        get {
            if (oSetCtrl_A2A == null) {
                
                oSetCtrl_A2A = new ApexPages.StandardSetController(Database.getQueryLocator(tBuildQuery('A2A', true)));                
                if (oSetCtrl_A2A.getResultSize() == 10000){
                    tWarning_A2A = 'Only the first 10.000 records are selected and visible.';
                }
                oSetCtrl_A2A.setPageSize(iPageSize_A2A);
            }
            
            return oSetCtrl_A2A;
        }
        set;

    }

    private String tBuildQuery(String tType, Boolean bApplyFilter){

        String tSOQL = '';

        if (tType == 'C2A'){

            // BUILD C2A SOQL
            tSOQL = 'SELECT Name, Affiliation_Type__c, Affiliation_Position_In_Account__c, Job_Title_Description__c, Affiliation_Active__c,  Affiliation_Primary__c, Affiliation_From_Contact__r.lastname, Affiliation_From_Contact__r.Contact_Primary_Specialty__c, Affiliation_From_Contact__r.Contact_Active__c, RecordTypeId, Affiliation_From_Contact__c, Affiliation_Start_Date__c,  Affiliation_End_Date__c,Contact_Name__c, Department__c, Primary_Dealer__c, Therapy__c, Therapy__r.Name';
            tSOQL += ' FROM Affiliation__c';
            tSOQL += ' WHERE (Affiliation_To_Account__c = :id_Account AND RecordTypeId = :id_RecordType_C2A)';

            if (bApplyFilter){
                if (tFilter_C2A != ''){
                    tSOQL += ' AND (';
                    tSOQL += tFilter_C2A;
                    tSOQL += ' )';
                }
            }

            if (clsUtil.isNull(tSortField_C2A, '') != ''){
                tSOQL += ' ORDER BY ' + tSortField_C2A + ' ' + tSortOrder_C2A;
            }
            tSOQL += ' LIMIT 10000';


        }else if (tType == 'A2A'){


            // BUILD A2A SOQL
            //tSOQL = 'SELECT Name, Affiliation_Type__c, Affiliation_Active__c, Affiliation_To_Account__r.Type, RecordTypeId,  Affiliation_To_Account__c, Affiliation_Start_Date__c, Affiliation_Primary__c,Contact_Name__c,Affiliation_From_Contact__r.lastname, Affiliation_From_Account__c, Affiliation_From_Account__r.type, Affiliation_End_Date__c, A2ABusiness_Unit_Lookup__c, A2ABUGroup_Lookup__c, Sub_Business_Unit__c, Primary_Dealer__c';
            tSOQL = 'SELECT Name, Affiliation_Type__c, Affiliation_Active__c, Affiliation_To_Account__r.Type, RecordTypeId,  Affiliation_To_Account__c, Affiliation_Start_Date__c, Affiliation_Primary__c,Contact_Name__c,Affiliation_From_Contact__r.lastname, Affiliation_From_Account__c, Affiliation_From_Account__r.type, Affiliation_End_Date__c, Business_Unit_vs__c, Business_Unit_Group_vs__c, Sub_Business_Unit_vs__c, Primary_Dealer__c';
            tSOQL += ' FROM Affiliation__c';
            tSOQL += ' WHERE ( ';
                tSOQL += ' ( Affiliation_To_Account__c = :id_Account OR Affiliation_From_Account__c = :id_Account )';
                tSOQL += ' AND ( RecordTypeId = :id_RecordType_A2A AND Affiliation_Type__c NOT IN (\'Ship To\', \'Ship To/Sold To\') )';
            tSOQL += ')';
            if (clsUtil.isNull(tSortField_A2A, '') != ''){
                tSOQL += ' ORDER BY ' + tSortField_A2A + ' ' + tSortOrder_A2A;
            }
            tSOQL += ' LIMIT 10000';

        }

        System.debug('**BC** tBuildQuery - tSOQL (' + tType + ') : ' + tSOQL);

        return tSOQL;

    }


    private void loadMasterData(){

        mapBusinessUnit_All = new Map<Id, Business_Unit__c>(
            [
                SELECT 
                    Id, Name, Contact_Flag__c
                    , (SELECT Id, Name, Contact_Flag__c FROM Sub_Business_Units__r ORDER BY Name ASC) 
                FROM 
                    Business_Unit__c 
                WHERE 
                    Company__r.Company_Code_Text__c = :oUser.Company_Code_Text__c 
                ORDER BY 
                    Name ASC
            ]
        );

        lstSubBusinessUnit_All = new List<Sub_Business_Units__c>();
        for (Business_Unit__c oBusinessUnit : mapBusinessUnit_All.values()){
            lstSubBusinessUnit_All.addAll(oBusinessUnit.Sub_Business_Units__r);
        }
        lstSubBusinessUnit_All.sort();

        mapSubBusinessUnit_All.putAll(lstSubBusinessUnit_All);

    }

    private List<SelectOption> loadFilterData_PrimarySpeciality(){

        List<SelectOption> lstSO = new List<SelectOption>();
        lstSO.add(new SelectOption(tNone, tNone));

        Schema.DescribeFieldResult oDescribeFieldResult_ContactPrimarySpecialty = Contact.Contact_Primary_Specialty__c.getDescribe();
        List<Schema.PicklistEntry> lstPicklistEntry = oDescribeFieldResult_ContactPrimarySpecialty.getPicklistValues();

        for( Schema.PicklistEntry oPicklistEntry : lstPicklistEntry){
            lstSO.add(new SelectOption(oPicklistEntry.getLabel(), oPicklistEntry.getValue()));
        }  

        return lstSO;

    }

    private List<SelectOption> loadFilterData_Department(){

        List<SelectOption> lstSO = new List<SelectOption>();

        lstSO.add(new SelectOption(tNone, tNone));

        List<Company_Related_Setup__c> lstCompanyRelatedSetup = 
            [
                SELECT
                    Value_Text__c
                FROM 
                    Company_Related_Setup__c
                WHERE
                    Master_Data_MDRel__r.Company_Code_Text__c = :oUser.Company_Code_Text__c 
                    AND Object_Name_Text__c = 'Affiliation__c' 
                    AND Field_Name_Text__c = 'Department__c'
                    AND Active_Checkbox__c = true 
                ORDER BY
                    SeqOrder_Number__c
            ];

            for (Company_Related_Setup__c oComanyRelatedSetup : lstCompanyRelatedSetup){
                lstSO.add(new SelectOption(oComanyRelatedSetup.Value_Text__c, oComanyRelatedSetup.Value_Text__c));
            }
  
        return lstSO;
    }

    private List<SelectOption> loadFilterData_ActiveInactive(){

        List<SelectOption> lstSO = new List<SelectOption>();

        lstSO.add(new SelectOption(tNone, tNone));
        lstSO.add(new SelectOption('ACTIVE', Label.Active_r));
        lstSO.add(new SelectOption('INACTIVE', Label.Inactive_r));

        return lstSO;

    }

    private List<SelectOption> loadFilterData_BusinessUnit(){

        List<SelectOption> lstSO = new List<SelectOption>();

        lstSO.add(new SelectOption(tNone, tNone));
        for (Business_Unit__c oBusinessUnit : mapBusinessUnit_All.values()){
            lstSO.add(new SelectOption(oBusinessUnit.Id, oBusinessUnit.Name));
        }

        return lstSO;
   
    }

    public List<SelectOption> loadFilterData_SubBusinessUnit(){

        List<SelectOption> lstSO = new List<SelectOption>();

        lstSO.add(new SelectOption(tNone, tNone));
      
        if ( clsUtil.isNull(tSelected_BusinessUnit, tNone) == tNone ){
            
            for (Sub_Business_Units__c oSubBusinessUnit : lstSubBusinessUnit_All){
                lstSO.add(new SelectOption(oSubBusinessUnit.id, oSubBusinessUnit.Name));
            }

        }else{

            if (mapBusinessUnit_All.containsKey(tSelected_BusinessUnit)){

                for (Sub_Business_Units__c oSubBusinessUnit : mapBusinessUnit_All.get(tSelected_BusinessUnit).Sub_Business_Units__r){
                    lstSO.add(new SelectOption(oSubBusinessUnit.id, oSubBusinessUnit.Name));
                }

            }

        }

        return lstSO;

    }
    //------------------------------------------------------------------------------


}