//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 11-03-2016
//  Description      : APEX Test Class to test the logic in tr_SVMXC_WorkOrderOperation and bl_SVMXC_WorkOrderOperation_Trigger
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_tr_SVMXC_WorkOrderOperation {

    //----------------------------------------------------------------------------------------
    // CREATE TEST DATA
    //----------------------------------------------------------------------------------------
    @testSetup static void createTestData() {

        // Create Custom Setting Data
        clsTestData.createCustomSettingData(true);

        // Create Account Data
        clsTestData.tCountry_Account = 'United Kingdom';
        clsTestData.iRecord_Account = 1;
        clsTestData.createAccountData(true);

        // Create SVMXC_Work_Order_Operation__c Data
        clsTestData.iRecord_SVMXCWorkOrderOperation = 1;
        clsTestData.createSVMXCWorkOrderOperationData(false);
        	clsTestData.oMain_SVMXCWorkOrderOperation.SVMXC_SAP_Id__c = '12345';
        insert clsTestData.oMain_SVMXCWorkOrderOperation;

    }
    //----------------------------------------------------------------------------------------

	
    //----------------------------------------------------------------------------------------
    // Test Prevent Update on Restricted Fields
    //----------------------------------------------------------------------------------------
	@isTest static void test_preventUpdateOnRestrictedFields_System() {

		// Select Data
		List<SVMXC_Work_Order_Operation__c> lstData = [SELECT Id, SVMXC_SAP_Id__c FROM SVMXC_Work_Order_Operation__c WHERE SVMXC_SAP_Id__c = '12345'];
		SVMXC_Work_Order_Operation__c oData = lstData[0];

		System.assertEquals(clsUtil.isNull(oData.SVMXC_SAP_Id__c, ''), '12345');

		// UPDATE
		Test.startTest();

			oData.SVMXC_SAP_Id__c = 'T1E1S1T1';
		update oData;

		Test.stopTest();

		// Validate that the update has been performed
		lstData = [SELECT Id, SVMXC_SAP_Id__c FROM SVMXC_Work_Order_Operation__c WHERE Id = :oData.Id];
		oData = lstData[0];
		System.assertEquals(clsUtil.isNull(oData.SVMXC_SAP_Id__c, ''), 'T1E1S1T1');

	}

	@isTest static void test_preventUpdateOnRestrictedFields_NotSystem() {

		// Get the User Profile and Role
		Profile oProfile = [SELECT Id FROM Profile WHERE Name = 'EUR Field Service Engineer' LIMIT 1];
		UserRole oUserRole = [SELECT Id FROM UserRole WHERE DeveloperName = 'EUR_Service_Repair' LIMIT 1];

		// Select an Active User based on the collected Profile and Role
		//	If there is not Active User available, create one
		List<User> lstUser = [SELECT Id FROM User WHERE IsActive = true AND ProfileId = :oProfile.Id AND UserRoleId = :oUserRole.Id LIMIT 1];
		User oUser;
		if (lstUser.size() == 1){
			oUser = lstUser[0];
		}else{
			clsTestData.createUserData(clsUtil.getTimeStamp() + '@test.medtronic.com', 'United Kingdom', oProfile.Id, oUserRole.Id, true);
			oUser = clsTestData.oMain_User;
		}
		
		List<SVMXC__Service_Order__c> lstDataSO = [SELECT Id, SVMX_SAP_Service_Order_No__c FROM SVMXC__Service_Order__c];
		SVMXC__Service_Order__c oDataSO = lstDataSO[0];
		oDataSO.OwnerId = oUser.Id;
		update oDataSO;
		
		List<SVMXC_Work_Order_Operation__c> lstData;
		SVMXC_Work_Order_Operation__c oData;

		// UPDATE
		Test.startTest();
			System.runAs(oUser){

				// Select Data
				lstData = [SELECT Id, SVMXC_SAP_Id__c FROM SVMXC_Work_Order_Operation__c WHERE SVMXC_SAP_Id__c = '12345'];
				oData = lstData[0];

				System.assertEquals(clsUtil.isNull(oData.SVMXC_SAP_Id__c, ''), '12345');

					oData.SVMXC_SAP_Id__c = 'T1E1S1T1';
				update oData;
			}
		Test.stopTest();

		// Validate that the update has NOT been performed
		lstData = [SELECT Id, SVMXC_SAP_Id__c FROM SVMXC_Work_Order_Operation__c WHERE id = :oData.Id];
		oData = lstData[0];
		System.assertEquals(clsUtil.isNull(oData.SVMXC_SAP_Id__c, ''), '12345');

	}
    //----------------------------------------------------------------------------------------	

}
//--------------------------------------------------------------------------------------------------------------------