public without sharing class ctrl_SMAX_Notification_Monitor {
    
    public List<Outbound_Message__c> outboundMessages {get; set;}
    
    
    public ctrl_SMAX_Notification_Monitor(){
    	
    	Map<String, String> inputParams = ApexPages.currentPage().getParameters();
    	
    	Id itemSFDCId = inputParams.get('id');
    	
    	if(itemSFDCId != null){
    		
    		outboundMessages = [Select Id, Name, User__c, Operation__c, Request__c, Response__c, CreatedDate from Outbound_Message__c where Internal_ID__c = :itemSFDCId ORDER BY CreatedDate DESC];
    		
    	}else{    	
    		
    		outboundMessages = new List<Outbound_Message__c>();
    	}
    }
}