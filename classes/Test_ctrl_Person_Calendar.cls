@isTest
private class Test_ctrl_Person_Calendar {
    
    private static testmethod void testPersonalCalendar(){
    	
    	createTestData();
    	
    	Test.startTest();
    	
    	ctrl_Person_Calendar.CalendarView calendar = ctrl_Person_Calendar.getWeek(null, UserInfo.getUserId(), 500);    	
    	System.assert(calendar.days.size() == 7);
    }
    
    private static void createTestData(){
		
		Account acc = new Account();
		acc.Name = 'Test Account';
		acc.Account_Country_vs__c = 'NETHERLANDS';		
		insert acc;
		
		Implant_Scheduling_Team__c team = [Select Id from Implant_Scheduling_Team__c];
		
		Id caseActivitySchedulingRT = [Select Id from RecordType where SObjectType = 'Case' AND DeveloperName = 'Implant_Scheduling'].Id;
		
		List<Case> caseList = new List<Case>();
				
		Date startOfWeek = Date.today().toStartOfWeek();				
			
		for(Integer i = 0; i < 7; i++){
						
			Case assignedCase = new Case();		
			assignedCase.AccountId = acc.Id;			
			assignedCase.Start_of_Procedure__c = DateTime.newInstance(startOfWeek.addDays(i), Time.newInstance(20, 0, 0, 0));			 
			assignedCase.Procedure_Duration_Implants__c = '0' + (i + 1) + ':00';			
			assignedCase.RecordTypeId = caseActivitySchedulingRT;
			assignedCase.Activity_Scheduling_Team__c = team.Id;
			assignedCase.Assigned_To__c = UserInfo.getUserId();
			assignedCase.Type = Math.mod(i, 2) == 0 ? 'Implant Support Request' : 'Service Request';
			assignedCase.Status = 'Assigned';	
			
			caseList.add(assignedCase);
		}
				
		bl_Case_Trigger.runningSchedulingApp = true;
		
		insert caseList;
		
		List<Event> assignedEvents = [Select Id, StartDateTime, EndDateTime from Event];
				
		Event personalEvent = new Event();
		personalEvent.Subject = 'Test Subject';	
		personalEvent.StartDateTime = DateTime.now().addDays(-1); 
		personalEvent.EndDateTime = DateTime.now().addDays(1);
		personalEvent.RecordTypeId = [Select Id from RecordType where SObjectType = 'Event' AND DeveloperName = 'Activity_Scheduling'].Id;	
		
		insert personalEvent;
	}
	
	@TestSetup
	private static void setData() {
		
		Mobile_App__c app = new Mobile_App__c();
		app.Name = 'Activity Scheduling Tool';
		app.Active__c = true;
		app.Unique_Key__c = 'ACT_SCHDL';
		insert app;
		
		List<Implant_Scheduling_Team__c> teams = new List<Implant_Scheduling_Team__c>();
		
		Id activitySchedulingTeamRT = [Select Id from RecordType where SObjectType = 'Implant_Scheduling_Team__c' AND DeveloperName = 'Activity_Scheduling_Team'].Id;
		
		Implant_Scheduling_Team__c team = new Implant_Scheduling_Team__c();			
		team.Name = 'Test Team 1';
		team.Team_Color__c = 'rgb(99, 99, 99)';
		team.Work_Day_End__c = '18';
		team.Work_Day_Start__c = '8';
		team.Working_Days__c = '1:5';			
		team.RecordTypeId = activitySchedulingTeamRT;
		insert team;
		
		Implant_Scheduling_Team_Member__c member = new Implant_Scheduling_Team_Member__c();
		member.Team__c = team.Id;
		member.Member__c = UserInfo.getUserId();			
		insert member;		
	}  
}