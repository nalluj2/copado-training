@isTest
private class Test_ba_Contact_MMX_Flag {
	
	private static testMethod void testCorrectMMXFlag(){
		
		User currentUser = new User(Id = UserInfo.getUserId());
		
		System.runAs(currentUser){ 
    	 
	    	Integrate_With_MMX_Countries__c setting = new Integrate_With_MMX_Countries__c();
	    	setting.Name = 'UnitTest';
	    	insert setting; 
    	}
    	
    	// Relationship Type    	    	
    	Relationship_Type__c c2aRelType = new Relationship_Type__c();
    	c2aRelType.Relationship_Type__c = 'Employee';
    	c2aRelType.Sub_Type__c = 'C2A';
		
		Account acc = new Account();
		acc.Name = 'Unit Test Account';
		acc.RecordTypeId = [Select Id from RecordType where SObjectType = 'Account' AND DeveloperName = 'Non_SAP_Account'].Id;
		acc.Account_Country_vs__c = 'BELGIUM';
		insert acc;
		
		Contact cnt = new Contact();
		cnt.FirstName = 'Unit Test';
		cnt.LastName = 'Contact';
		cnt.AccountId = acc.Id ; 
        cnt.Contact_Department__c = 'Diabetes Adult'; 
        cnt.Contact_Primary_Specialty__c = 'ENT';
        cnt.Affiliation_To_Account__c = 'Employee';
        cnt.Primary_Job_Title_vs__c = 'Manager';
        cnt.Contact_Gender__c = 'Male'; 
		cnt.MailingCountry = 'UnitTest';
		cnt.RecordTypeId = [Select Id from RecordType where SObjectType = 'Contact' AND DeveloperName = 'Generic_Contact'].Id;
		insert cnt;
		
		cnt = [Select Id, Integrate_with_MMX__c from Contact where Id = :cnt.Id];
    	System.assert(cnt.Integrate_with_MMX__c == false);
    	
    	List<Affiliation__c> primaryAff = [Select Id, Affiliation_Type__c, Affiliation_Primary__c from Affiliation__c where Affiliation_From_Contact__c = :cnt.Id];
    	
    	System.assert(primaryAff.size() == 1);
    	System.assert(primaryAff[0].Affiliation_Type__c == 'Employee');
    	System.assert(primaryAff[0].Affiliation_Primary__c == true);
		
		bl_Relationship.contactMMXFlagAlreadyDone = new Set<Id>();
		
		Test.startTest();
		
		acc.SAP_ID__c = '0123456789';
		acc.SAP_Account_Type__c='Sold-to party';  
		update acc;
		
		cnt = [Select Id, Integrate_with_MMX__c from Contact where Id = :cnt.Id];
    	System.assert(cnt.Integrate_with_MMX__c == false);
    	
    	Database.executeBatch(new ba_Contact_MMX_Flag(), 1);
    	
    	Test.stopTest();
    	
    	cnt = [Select Id, Integrate_with_MMX__c from Contact where Id = :cnt.Id];
    	System.assert(cnt.Integrate_with_MMX__c == true);
	}
    
}