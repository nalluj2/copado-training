/*
 *      Description : This wrapper class is used to pass parameters to the RelatedList VF Component
 *		Version	= 1.0
 *      Author 	= Bart Caelen
 *		Date	= 03/2014
 * ---------------------------------------------------------------------------------------------------------------------
 *      Description : Added bShowExport as a parameter
 *		Version	= 2.0
 *      Author 	= Bart Caelen
 *		Date	= 10/2016
*/
public class wr_RelatedList {

	// Constructor
	public wr_RelatedList(){
		tSObjectName			= '';
		tMasterFieldName		= '';
		tMasterRecordID			= '';
		tAdditionalWherePart	= '';
		tTitle					= '';
		tSubTitle				= '';
		tTitleNewData			= '';
		tJSNewData				= '';
		tCompanyCode			= '';
		tBUGID					= '';
		tBUID					= '';
		tSBUID					= '';
		iRecordsPerPage			= 10;
		
		bShowTitle				= true;
		bShowBackToParent		= true;
		bShowDebug				= false;
		bShowAddNewButton		= false;
		bUseUserBU				= true;
		bShowExportButton		= true;
		bSelectPrimaryBU		= false;
		tExportType				= 'CSV';
	}

	public String tSObjectName				{ get;set; }	// Name of the sObject
	public String tMasterFieldName			{ get;set; }	// API Name of the Master Field
    public String tMasterRecordID			{ get;set; }	// ID of the Master Record
	public String tAdditionalWherePart		{ get;set; }	// An Additional Where part
	public String tTitle					{ get;set; }	// Title of the page
	public String tSubTitle					{ get;set; }	// Subtitle of the page
	public String tJSNewData				{ get;set; }	// Name of the JavaScript function that will be executed when Request new data is clicked.  This JS Function need to be created in the parent page.
	public String tTitleNewData				{ get;set; }	// Label of the New Data button.
	public Boolean bShowBackToParent		{ get;set; }	// Show/Hide the back to Parent button
	public Boolean bShowDebug				{ get;set; }	// Show/Hide the debug section and data like SOQL
	public Boolean bShowTitle				{ get;set; }	// Show/Hide the Title and Sub-Title in in the VF Component
	public Boolean bShowAddNewButton		{ get;set; }	// Show/Hide the Add New button which calls the tJSNewData function
	public Boolean bUseUserBU				{ get;set; }	// Use the UserBU object to determine the available Business Units
	public Boolean bShowExportButton		{ get;set; }	// Show the export button to export the data to CSV/EXCEL
	public Boolean bSelectPrimaryBU			{ get;set; }	// Select the Primary BU based on the User SBU records - if false, we select the BU based on tBUID
	public String tExportType				{ get;set; }	// Define the export type --> CSV, XLS, XLSX, PDF, HTML

	public String tCompanyCode				{ get;set; }	// The Company Code or Master Data Code (eg. EUR).
	public String tBUGID					{ get;set; }	// The ID of the Business Unit Group.
	public String tBUID						{ get;set; }	// The ID of the Business Unit.
	public String tSBUID					{ get;set; }	// The ID of the Sub Business Unit.

	public Integer iRecordsPerPage			{ get;set; }	// The number of records that are visible on each page of a StandardSetController

}