/**
 * Creation Date:   20131105
 * Description:     Controller for custom Lead conversion. The conversion runs under the System user.
 *              
 * Author:  Fabrizio Truzzu
 */
public class ctrl_LeadConvert {
 
 	private List<Account> accounts;
 	private string id;
	private String tShowError = '';
 	
  	public ctrl_LeadConvert(){
  		id = ApexPages.CurrentPage().getParameters().get('id');
		tShowError = ApexPages.CurrentPage().getParameters().get('showError');

  		currentLead = [select Name, Email
  					   from Lead 
  					   where Id =: id];
  					     					     		
  		getAssociatedAccount();
  		fillSelectList();

		tError = '';
  	}
  	  	 	
  	private void getAssociatedAccount(){
  		
  		if(currentLead.Email != null){
  			accounts = bl_Account_With_Sharing.getAccountByEmail(id);
  		}
  		
		if(accounts == null || accounts.size() == 0){
			accounts = bl_Account_With_Sharing.getAccountByName(id);
		}  	
  	}
  	
  	private void fillSelectList(){
  		  		
  		accountSelectList = new List<SelectOption>();
  		
  		addValuesToAccountSelectList(currentLead);
  	}
  	
  	private void addValuesToAccountSelectList(Lead lead){
  		if(accounts != null){
  			if(accounts.size() > 0){
  				accountSelectList.add(new SelectOption('','--None--'));
  			}
  			
  			for(Account acc : accounts){
  				accountSelectList.add(new SelectOption(acc.Id,'Attach to Existing: '+acc.Name+' ('+acc.SAP_ID__c+')'));
  			}
  		}
  		
  		accountSelectList.add(new SelectOption('New','Create New Account: '+ lead.Name));
  	}  	
  	
  	public PageReference getAccountOverviewUrl(){  		
  		if(selectedAccount == null || selectedAccount == '' || selectedAccount == 'New'){
	    	return null;
	    }
	     			    
  		PageReference pr = new PageReference('/'+selectedAccount+'/p');
  		return pr;	
  	}
  	 
  	public PageReference convertLead(){
	   	Database.LeadConvert lc = new Database.LeadConvert();
	    
	    if(selectedAccount == ''){
	    	return null;
	    }
	     
	    if(selectedAccount != 'New'){
	    	lc.setAccountId(selectedAccount);
	    }
	   	
	   	lc.setLeadId(id);
	    LeadStatus convertStatus = [SELECT Id, MasterLabel 
	                  FROM LeadStatus 
	                  WHERE IsConverted=true LIMIT 1];
	                  
	    lc.setConvertedStatus(convertStatus.MasterLabel);
	    
	    Database.LeadConvertResult lcr;
	    
	    try{
	        lcr = Database.convertLead(lc);
	    }
	    catch(System.DmlException e){
	    	displayWarning = true;
			if (!String.isBlank(tShowError)){
				tError = ' (' + e.getCause() + ' : ' + e.getMessage() + ')';
			}
	    	return null;
	    }
	    	
	    PageReference pr = new PageReference('/'+lcr.getAccountId());
	    //pr.setRedirect(true);
	    return pr;
  	}
  	
  	public PageReference goBackToLead(){
  		PageReference pr = new PageReference('/'+id);
	    pr.setRedirect(true);
	    return pr;
  	}
  	
  	public void searchAccount(){
  		accountSuggestions = bl_Account_With_Sharing.getAccountSuggestions(searchValue);
  	}
  	
  	public PageReference accountClicked(){
  		
  		Account accountToAdd = [select a.Name,a.Id, a.SAP_Id__c
  							  	from Account a
  							  	where a.Id =: selectedId];
  							  	
		Set<String> accountSet = new Set<String>();
		for(SelectOption accountOption : accountSelectList){
			accountSet.add(accountOption.getValue());
		}
		
		if(!accountSet.contains(accountToAdd.Id)){	  	
  			if(accountSelectList.size()>1){  			
  				accountSelectList.add(1,new SelectOption(accountToAdd.Id,'Attach to Existing: '+accountToAdd.Name+' ('+accountToAdd.SAP_ID__c+')'));
  			}else{
  				accountSelectList.add(0,new SelectOption(accountToAdd.Id,'Attach to Existing: '+accountToAdd.Name+' ('+accountToAdd.SAP_ID__c+')'));
  			}
		}
		
  		selectedAccount = accountToAdd.Id;
  		  		
        displayPopup = false;
  		return null;
  	}
  	
  	public void hidePopup(){
  		displayPopup = false;
  	}
 
    public void showPopup() {
    	displayPopup = true;
    }
 
    public boolean displayWarning{get;set;}
    public Lead currentLead{get;set;}
    public string selectedAccount{get;set;}
    public string selectedId {get;set;}
    public List<Account> accountSuggestions{get;set;}
    public List<SelectOption> accountSelectList{get;set;}
    public string searchValue{get;set;}
 	public boolean displayPopup {get; set;}
	public String tError { get; private set; }

	public class applicationException extends Exception {}   
 }