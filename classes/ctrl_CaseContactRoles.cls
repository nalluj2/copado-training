//--------------------------------------------------------------------------------------------------------------------------------
//  Author      :   Bart Caelen
//  Date        :   14/06/2017
//  Description :   CR-12431
//                      Update the logic because the standard object CaseContactRole is replaced by 
//                      a custom object Case_Contact_Roles__c.
//						Rewritten the APEX Controller and updated the name.
//	Change Log  :   
//--------------------------------------------------------------------------------------------------------------------------------
public with sharing class ctrl_CaseContactRoles {

    //----------------------------------------------------------------------
    // Private Variables
    //----------------------------------------------------------------------
	private Map<Integer, Case_Contact_Roles__c> mapCaseContactRoles = new  Map<Integer, Case_Contact_Roles__c>();
    //----------------------------------------------------------------------


    //----------------------------------------------------------------------
    // Constructor
    //----------------------------------------------------------------------
    public ctrl_CaseContactRoles(){

    }
    //----------------------------------------------------------------------


    //----------------------------------------------------------------------
    // Getters & Setters
    //----------------------------------------------------------------------
    public String tCaseId { get; set; }
    public Map<Integer, Case_Contact_Roles__c> getMapCaseContactRoles(){

        return loadCaseContactRolesData();

    }
    //----------------------------------------------------------------------


    //----------------------------------------------------------------------
    // Private Methods
    //----------------------------------------------------------------------
    private Map<Integer, Case_Contact_Roles__c> loadCaseContactRolesData(){

        Map<Integer, Case_Contact_Roles__c> mapCaseContactRoles = new Map<Integer, Case_Contact_Roles__c>();

        if (!String.isBlank(tCaseId)){

            List<Case_Contact_Roles__c> lstCaseContactRoles = [SELECT Contact__r.Name, Role__c FROM Case_Contact_Roles__c WHERE Case__c = :tCaseId];           

            Integer iCounter = 1; 
            for (Case_Contact_Roles__c oCaseContactRoles : lstCaseContactRoles){
                mapCaseContactRoles.put(iCounter, oCaseContactRoles);
                iCounter++;
            }

        } 

        return mapCaseContactRoles;

    }
    //----------------------------------------------------------------------

}
//--------------------------------------------------------------------------------------------------------------------------------