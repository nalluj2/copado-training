/*
 *      Description : This class exposes methods to create / update / delete a Account Plan Team Member
 *
 *      Author = Rudy De Coninck
 */

@RestResource(urlMapping='/AccountPlanTeamMemberService/*')
global with sharing class ws_AccountPlanTeamMemberService {
	@HttpPost
	global static String doPost() {
 		RestRequest req = RestContext.request;
			
		System.debug('post called '+req);
		System.debug('post requestBody '+req.requestBody);
			
		String body = req.requestBody.toString();
		IFAccountPlanTeamMemberRequest accountPlanTeamMemberRequest = (IFAccountPlanTeamMemberRequest)System.Json.deserialize(body, IFAccountPlanTeamMemberRequest.class);
			
		System.debug('post requestBody '+accountPlanTeamMemberRequest);
			
		Account_Plan_Team_Member__c accountPlanTeamMember = accountPlanTeamMemberRequest.accountPlanTeamMember;

		IFAccountPlanTeamMemberResponse resp = new IFAccountPlanTeamMemberResponse();

		//We catch all exception to assure that 
		try{
				
			resp.accountPlanTeamMember = bl_AccountPlanning.saveAccountPlanTeamMember(accountPlanTeamMember);
			resp.id = accountPlanTeamMemberRequest.id;
	
			resp.success = true;
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
		}
 
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'AccountPlanTeamMember' ,req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
  			
		return System.JSON.serialize(resp);		 			
	}

	@HttpDelete
	global static String doDelete(){
		RestRequest req = RestContext.request;
			
		System.debug('post called '+req);
		System.debug('post requestBody '+req.requestBody);
			
		String body = req.requestBody.toString();
		IFAccountPlanTeamMemberDeleteRequest accountPlanTeamMemberDeleteRequest = (IFAccountPlanTeamMemberDeleteRequest)System.Json.deserialize(body, IFAccountPlanTeamMemberDeleteRequest.class);
			
		System.debug('post requestBody '+accountPlanTeamMemberDeleteRequest);
			
		Account_Plan_Team_Member__c accountPlanTeamMember =accountPlanTeamMemberDeleteRequest.accountPlanTeamMember;
		IFAccountPlanTeamMemberDeleteResponse resp = new IFAccountPlanTeamMemberDeleteResponse();
		
		List<Account_Plan_Team_Member__c>accountPlanTeamMembersToDelete = [select id, mobile_id__c from Account_Plan_Team_Member__c where Mobile_ID__c = :accountPlanTeamMember.Mobile_ID__c];
		
		try{
			
			if (accountPlanTeamMembersToDelete !=null && accountPlanTeamMembersToDelete.size()>0 ){	
				
				Account_Plan_Team_Member__c accountPlanTeamMemberToDelete = accountPlanTeamMembersToDelete.get(0);
				delete accountPlanTeamMemberToDelete;
				resp.accountPlanTeamMember = accountPlanTeamMemberToDelete;
				resp.id = accountPlanTeamMemberDeleteRequest.id;
				resp.success = true;
			}else{
				resp.accountPlanTeamMember = accountPlanTeamMember;
				resp.id = accountPlanTeamMemberDeleteRequest.id;
				resp.message='Account Plan Team Member not found';
				resp.success = true;
			}
			
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
		}

		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'AccountPlanTeamMemberDelete',req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
			
		return System.JSON.serialize(resp);		
	}
	
	
	global class IFAccountPlanTeamMemberRequest{
		public Account_Plan_Team_Member__c accountPlanTeamMember {get;set;}
		public String id{get;set;}
	}
	
	global class IFAccountPlanTeamMemberDeleteRequest{
		public Account_Plan_Team_Member__c accountPlanTeamMember {get;set;}
		public String id{get;set;}
	}
		
	global class IFAccountPlanTeamMemberDeleteResponse{
		public Account_Plan_Team_Member__c accountPlanTeamMember {get;set;}
		public String id{get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}

	global class IFAccountPlanTeamMemberResponse{
		public Account_Plan_Team_Member__c accountPlanTeamMember {get;set;}
		public String id{get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}
	
}