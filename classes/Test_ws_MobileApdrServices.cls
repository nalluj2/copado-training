@IsTest(seeAllData=true)
public with sharing class Test_ws_MobileApdrServices { 

    public static testmethod void testAccountPlan2(){
        Account_Plan_2__c accountPlan = ws_mobileApdrRequestFactory.createAccountPlan();
    }

    public static testmethod void testAccountPlanObjective(){
        Account_Plan_2__c accountPlan = ws_mobileApdrRequestFactory.createAccountPlan();
        Account_Plan_Objective__c accountPlanObjective = ws_mobileApdrRequestFactory.createAccountPlanObjective(accountPlan);
    }

    public static testmethod void testAccountPlanStakeholder(){
        Account_Plan_2__c accountPlan = ws_mobileApdrRequestFactory.createAccountPlan();
        Account_Plan_Stakeholder__c accountPlanStakeholder = ws_mobileApdrRequestFactory.createAccountPlanStakeholder(accountPlan);
    }

    public static testmethod void testAccountPlanTeamMember(){
        Account_Plan_2__c accountPlan = ws_mobileApdrRequestFactory.createAccountPlan();
        Account_Plan_Team_Member__c accountPlanTeamMember = ws_mobileApdrRequestFactory.createAccountPlanTeamMember(accountPlan);
    }

    public static testmethod void testAccountStandard2(){
        Account_Plan_2__c accountPlan = ws_mobileApdrRequestFactory.createAccountPlan();
        Tender_Calendar_2__c tenderCalendar = ws_mobileApdrRequestFactory.createTenderCalendar2(accountPlan); 
    }

    public static testmethod void testTenderCalendar2(){
        Account_Plan_2__c accountPlan = ws_mobileApdrRequestFactory.createAccountPlan();
        Tender_Calendar_2__c tenderCalendar = ws_mobileApdrRequestFactory.createTenderCalendar2(accountPlan); 
    }

    public static testmethod void testAccountPerformance(){
        Account_Plan_2__c accountPlan = ws_mobileApdrRequestFactory.createAccountPlan();
        Account_Performance__c accountPerformance = ws_mobileApdrRequestFactory.createAccountPerformance(accountPlan);
    }
    
    public static testmethod void testAccountPerformanceLoad(){
        Account_Performance_Load__c accountPerformanceLoad = ws_mobileApdrRequestFactory.createAccountPerformanceLoad();
    }
    
    public static testmethod void testCreateTask(){
        Account_Plan_2__c accountPlan = ws_mobileApdrRequestFactory.createAccountPlan();
        Task task = ws_mobileApdrRequestFactory.createTask(accountPlan);
    }
    
    public static testmethod void testCVGProject(){
        
        Account testAccount = new Account();
        testAccount.name = 'test account for cvg opportunity';
        insert testAccount;
        
        Opportunity opp = new Opportunity();
        opp.name = 'Big Bets';
        opp.recordTypeId = RecordTypeMedtronic.Opportunity('CVG_Project').id;
        opp.Project_Start_Date__c = Date.today()-10;
        opp.Project_End_Date__c = Date.today()+5;
        opp.Sub_Business_Unit__c = 'Cardio Vascular';
        opp.StageName = 'Closed / Won';
        opp.CloseDate =  Date.today();
        opp.currencyisocode = 'EUR';
        
        RestRequest req = new RestRequest();  
        RestResponse res = new RestResponse();
        
        String id=GuidUtil.NewGuid();
        opp.Mobile_ID__c = id;
        
        JSONGenerator gen = JSON.createGenerator(true);
        
        gen.writeStartObject();
        gen.writeStringField('id', '12346'); 
        gen.writeObjectField('opportunity',opp);
        
        // Get the JSON string.
        String pretty = gen.getAsString();          
        System.debug('JSON '+pretty);

        req.requestbody = Blob.valueOf(pretty);
        
        req.requestURI = '/services/apexrest/CVGProjectOpportunityService';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        String resp = ws_CVGProjectOpportunityService.doPost();
        System.debug('Response'+resp);
        
        List<PricebookEntry> pricebookEntries = [SELECT Id,Name,Pricebook2.name,Product2.name,Product2Id, ProductCode 
        FROM PricebookEntry where Pricebook2.name = 'CVG Projects' and currencyisocode = 'EUR' limit 1];
        
        PricebookEntry prE = pricebookEntries.get(0);
        
        //List<Pricebook> prb = [select Id from Pricebook where name = 'CVG Projects' limit 1];
        
        //Pricebook pb = prb.get(0);
        
        List<OpportunityLineItem> lineItems =  new List<OpportunityLineItem>();
        OpportunityLineItem lineItem1 = new OpportunityLineItem();
        
        RestRequest reqL = new RestRequest();  
        RestResponse resL = new RestResponse();
        
        String idL=GuidUtil.NewGuid();
        lineItem1.Mobile_ID__c = idL;
        lineItem1.Quantity = 1;
        //lineItem1.currencyisocode = 'EUR';
        lineItem1.TotalPrice = 100;
        lineItem1.PricebookEntryId = prE.id;
        Opportunity relationshipOpportunity = new Opportunity(Mobile_id__c = opp.mobile_id__c);
        lineItem1.Opportunity = relationshipOpportunity;
        lineItems.add(lineItem1);
        
        JSONGenerator gen2 = JSON.createGenerator(true);
        
        gen2.writeStartObject();
        gen2.writeStringField('id', '12346'); 
        gen2.writeObjectField('opportunityLineItems',lineItems);
        
        // Get the JSON string.
        String pretty2 = gen2.getAsString();            
        System.debug('JSON '+pretty2);

        reqL.requestbody = Blob.valueOf(pretty2);
        
        reqL.requestURI = '/services/apexrest/CVGProjectOpportunityLineItemService';  
        reqL.httpMethod = 'POST';
        RestContext.request = reqL;
        RestContext.response = resL;
        
        String resp2 = ws_CVGProjectOpportunityLineItemService.doPost();
        System.debug('Response2'+resp2);
        
        // Delete opportunity
        req = new RestRequest();  
        res = new RestResponse();
        
        ws_CVGProjectOpportunityService.IFCVGProjectOpportunityDeleteRequest request = new ws_CVGProjectOpportunityService.IFCVGProjectOpportunityDeleteRequest();
        request.id = 'unittestid';
        request.Opportunity = opp; 
                
        req.requestbody = Blob.valueOf(JSON.serialize(request));
        
        req.requestURI = '/services/apexrest/CVGProjectOpportunityService';  
        req.httpMethod = 'DELETE';
        RestContext.request = req;
        RestContext.response = res;
        
        //First will delete
        resp = ws_CVGProjectOpportunityService.doDelete();
        System.debug('Response'+resp);
        
        //Second will not find the record
        resp = ws_CVGProjectOpportunityService.doDelete();
        System.debug('Response'+resp);        
    }
    
    
    public static testmethod void testOpportunity(){
        
        Account testAccount = new Account();
        testAccount.name = 'test account for rtg opportunity';
        insert testAccount;
        
        Opportunity opp = new Opportunity();
        opp.name = 'RTG opportunity';
        opp.recordTypeId = RecordTypeMedtronic.Opportunity('Service_Contract_Opportunity').id;
        opp.StageName = 'Closed / Won';
        opp.CloseDate =  Date.today();
        opp.currencyisocode = 'EUR';
        
        RestRequest req = new RestRequest();  
        RestResponse res = new RestResponse();
        
        String id=GuidUtil.NewGuid();
        opp.Mobile_ID__c = id;
        
        JSONGenerator gen = JSON.createGenerator(true);
        
        gen.writeStartObject();
        gen.writeStringField('id', '12346'); 
        gen.writeObjectField('opportunity',opp);
        
        // Get the JSON string.
        String pretty = gen.getAsString();          
        System.debug('JSON '+pretty);

        req.requestbody = Blob.valueOf(pretty);
        
        req.requestURI = '/services/apexrest/OpportunityService';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        ws_OpportunityService.IFOpportunityResponse resp = ws_OpportunityService.doPost();
        System.debug('Response'+resp);
        
        List<PricebookEntry> pricebookEntries = [SELECT Id,Name,Pricebook2.name,Product2.name,Product2Id, ProductCode 
        FROM PricebookEntry where IsActive=true and Pricebook2.name = 'EMEA Capital Pricebook' and currencyisocode = 'EUR' limit 1];
        
        PricebookEntry prE = pricebookEntries.get(0);
                
        OpportunityLineItem lineItem = new OpportunityLineItem();
        
        RestRequest reqL = new RestRequest();  
        RestResponse resL = new RestResponse();
        
        String idL=GuidUtil.NewGuid();
        lineItem.Mobile_ID__c = idL;
        lineItem.Quantity = 1;
        lineItem.TotalPrice = 100;
        lineItem.PricebookEntryId = prE.id;
        Opportunity relationshipOpportunity = new Opportunity(Mobile_id__c = opp.mobile_id__c);
        lineItem.Opportunity = relationshipOpportunity;     
        
        JSONGenerator gen2 = JSON.createGenerator(true);
        
        gen2.writeStartObject();
        gen2.writeStringField('id', '12346'); 
        gen2.writeObjectField('opportunityLineItem',lineItem);
        
        // Get the JSON string.
        String pretty2 = gen2.getAsString();            
        System.debug('JSON '+pretty2);

        reqL.requestbody = Blob.valueOf(pretty2);
        
        reqL.requestURI = '/services/apexrest/OpportunityLineItemService';  
        reqL.httpMethod = 'POST';
        RestContext.request = reqL;
        RestContext.response = resL;
        
        ws_OpportunityLineItemService.IFOpportunityLineItemResponse resp2 = ws_OpportunityLineItemService.doPost();
        System.debug('Response2'+resp2);
    }
    
    private static testmethod void testOpportunityGrowthDriver(){
        
        User oUser_System = clsTestData_User.createUser_SystemAdministratorMDT('tadm01', false);
            oUser_System.Primary_sBU__c = 'Brain Modulation';
        insert oUser_System;
        
        System.runAs(oUser_System){

            Account testAccount = new Account();
            testAccount.name = 'test account for rtg opportunity';
            insert testAccount;
            
            Opportunity opp = new Opportunity();
            opp.name = 'RTG opportunity';
            opp.recordTypeId = RecordTypeMedtronic.Opportunity('Service_Contract_Opportunity').id;
            opp.StageName = 'Closed / Won';
            opp.CloseDate =  Date.today();
            opp.currencyisocode = 'EUR';
            opp.Mobile_ID__c = GuidUtil.NewGuid();
            insert opp;
            
            Growth_Driver__c driver = new Growth_Driver__c();
            driver.Active__c = true;
            driver.Growth_Driver__c = 'TestDriver';
            driver.Description__c = 'Unit Test Growth Driver';
            insert driver;
            
            RestRequest req = new RestRequest();  
            RestResponse res = new RestResponse();
            
            Opportunity_Growth_Driver__c oppDriver = new Opportunity_Growth_Driver__c();
            oppDriver.Growth_Driver_Id__c = driver.Id;
            oppDriver.Opportunity__c = opp.Id;
            oppDriver.Mobile_ID__c = GuidUtil.NewGuid();
                            
            JSONGenerator gen = JSON.createGenerator(true);
            
            gen.writeStartObject();
            gen.writeStringField('id', '12346'); 
            gen.writeObjectField('oppGrowthDriver', oppDriver);
            
            // Get the JSON string.
            String pretty = gen.getAsString();          
            System.debug('JSON ' + pretty);

            req.requestbody = Blob.valueOf(pretty);
            
            req.requestURI = '/services/apexrest/OpportunityGrowthDriverService';  
            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response = res;
            
            ws_OpportunityGrowthDriverService.IFOppGrowthDriverResponse resp = ws_OpportunityGrowthDriverService.doPost();
            System.debug('Response' + resp);
            
            System.assert(resp.success == true);
            
            List<Opportunity_Growth_Driver__c> oppDriverList = [Select Id from Opportunity_Growth_Driver__c where Mobile_Id__c = : oppDriver.Mobile_ID__c];
            System.assert(oppDriverList.size() == 1);

        }

    }
    
    private static testmethod void testDeleteOpportunityGrowthDriver(){

        User oUser_System = clsTestData_User.createUser_SystemAdministratorMDT('tadm01', false);
            oUser_System.Primary_sBU__c = 'Brain Modulation';
        insert oUser_System;
        
        System.runAs(oUser_System){

            Account testAccount = new Account();
            testAccount.name = 'test account for rtg opportunity';
            insert testAccount;
            
            Opportunity opp = new Opportunity();
            opp.name = 'RTG opportunity';
            opp.recordTypeId = RecordTypeMedtronic.Opportunity('Service_Contract_Opportunity').id;
            opp.StageName = 'Closed / Won';
            opp.CloseDate =  Date.today();
            opp.currencyisocode = 'EUR';
            opp.Mobile_ID__c = GuidUtil.NewGuid();
            insert opp;
            
            Growth_Driver__c driver = new Growth_Driver__c();
            driver.Active__c = true;
            driver.Growth_Driver__c = 'TestDriver';
            driver.Description__c = 'Unit Test Growth Driver';
            insert driver;
                            
            Opportunity_Growth_Driver__c oppDriver = new Opportunity_Growth_Driver__c();
            oppDriver.Growth_Driver_Id__c = driver.Id;
            oppDriver.Opportunity__c = opp.Id;
            oppDriver.Mobile_ID__c = GuidUtil.NewGuid();
            insert oppDriver;
            
            RestRequest req = new RestRequest();  
            RestResponse res = new RestResponse();
                            
            JSONGenerator gen = JSON.createGenerator(true);
            
            gen.writeStartObject();
            gen.writeStringField('id', '12346'); 
            gen.writeObjectField('oppGrowthDriver', oppDriver);
            
            // Get the JSON string.
            String pretty = gen.getAsString();          
            System.debug('JSON ' + pretty);

            req.requestbody = Blob.valueOf(pretty);
            
            req.requestURI = '/services/apexrest/OpportunityGrowthDriverService';  
            req.httpMethod = 'DELETE';
            RestContext.request = req;
            RestContext.response = res;
            
            ws_OpportunityGrowthDriverService.IFOppGrowthDriverDeleteResponse resp = ws_OpportunityGrowthDriverService.doDelete();
            System.debug('Response' + resp);
            
            System.assert(resp.success == true);
            
            List<Opportunity_Growth_Driver__c> oppDriverList = [Select Id from Opportunity_Growth_Driver__c where Mobile_Id__c = : oppDriver.Mobile_ID__c];
            System.assert(oppDriverList.size() == 0);

        }

    }

    @isTest private static void testOpportunityStakeholderMapping_Insert(){
        
        User oUser_System = clsTestData_User.createUser_SystemAdministratorMDT('tadm01', false);
            oUser_System.Primary_sBU__c = 'Brain Modulation';
        insert oUser_System;
        
        System.runAs(oUser_System){

            //------------------------------------------------------------------------------------
            // Create Test Data
            //------------------------------------------------------------------------------------
            clsTestData_Account.iRecord_Account = 1;
            clsTestData_Account.createAccount();

            clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'Service_Contract_Opportunity').Id;
            clsTestData_Opportunity.iRecord_Opportunity = 1;
            List<Opportunity> lstOpportunity = clsTestData_Opportunity.createOpportunity(false);
            for (Opportunity oOpportunity : lstOpportunity){
                oOpportunity.StageName = 'Closed / Won';
                oOpportunity.CloseDate =  Date.today();
                oOpportunity.CurrencyISOCode = 'EUR';
                oOpportunity.Mobile_ID__c = GuidUtil.NewGuid();
            }
            insert lstOpportunity;

            List<Stakeholder_Mapping__c> lstStakeholderMapping = [SELECT Id FROM Stakeholder_Mapping__c WHERE Opportunity__c = :lstOpportunity];
            System.assertEquals(0, lstStakeholderMapping.size());
            //------------------------------------------------------------------------------------


            //------------------------------------------------------------------------------------
            // Testing
            //------------------------------------------------------------------------------------
            RestRequest oRequest = new RestRequest();  
            RestResponse oResponse = new RestResponse();
            
            Stakeholder_Mapping__c oStakeholderMapping = new Stakeholder_Mapping__c();
                oStakeholderMapping.Opportunity__c = lstOpportunity[0].Id;
                oStakeholderMapping.Mobile_ID__c = GuidUtil.NewGuid();
                            
            JSONGenerator oJSONGen = JSON.createGenerator(true);
                oJSONGen.writeStartObject();
                oJSONGen.writeStringField('id', '12346'); 
                oJSONGen.writeObjectField('oppStakeholderMapping', oStakeholderMapping);
            
            // Get the JSON string.
            String tJSON_Pretty = oJSONGen.getAsString();           
            System.debug('JSON ' + tJSON_Pretty);

                oRequest.requestbody = Blob.valueOf(tJSON_Pretty);
                oRequest.requestURI = '/services/apexrest/OpportunityStakeholderMappingService';  
                oRequest.httpMethod = 'POST';
            RestContext.request = oRequest;
            RestContext.response = oResponse;
            
            Test.startTest();
            
            ws_OpportunityStakeholderMappingService.IFOppStakeholderMappingResponse oIFOppStakeholderMappingResponse = ws_OpportunityStakeholderMappingService.doPost();
            System.debug('Response' + oIFOppStakeholderMappingResponse);
            
            Test.stopTest();
            //------------------------------------------------------------------------------------


            //------------------------------------------------------------------------------------
            // Validation
            //------------------------------------------------------------------------------------
            System.assert(oIFOppStakeholderMappingResponse.success == true);
            lstStakeholderMapping = [SELECT Id, Mobile_Id__c FROM Stakeholder_Mapping__c WHERE Opportunity__c = :lstOpportunity];
            System.assertEquals(1, lstStakeholderMapping.size());
            System.assertEquals(oStakeholderMapping.Mobile_ID__c, lstStakeholderMapping[0].Mobile_ID__c);
            //------------------------------------------------------------------------------------

        }

    }

    @isTest private static void testOpportunityStakeholderMapping_Delete(){

        User oUser_System = clsTestData_User.createUser_SystemAdministratorMDT('tadm01', false);
            oUser_System.Primary_sBU__c = 'Brain Modulation';
        insert oUser_System;
        
        System.runAs(oUser_System){

            //------------------------------------------------------------------------------------
            // Create Test Data
            //------------------------------------------------------------------------------------
            clsTestData_Account.iRecord_Account = 1;
            clsTestData_Account.createAccount();

            clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'Service_Contract_Opportunity').Id;
            clsTestData_Opportunity.iRecord_Opportunity = 1;
            List<Opportunity> lstOpportunity = clsTestData_Opportunity.createOpportunity(false);
            for (Opportunity oOpportunity : lstOpportunity){
                oOpportunity.StageName = 'Closed / Won';
                oOpportunity.CloseDate =  Date.today();
                oOpportunity.CurrencyISOCode = 'EUR';
                oOpportunity.Mobile_ID__c = GuidUtil.NewGuid();
            }
            insert lstOpportunity;

            clsTestData_Opportunity.iRecord_StakeholderMapping = 1;
            Stakeholder_Mapping__c oStakeholderMapping = clsTestData_Opportunity.createStakeholderMapping()[0];

            List<Stakeholder_Mapping__c> lstStakeholderMapping = [SELECT Id, Mobile_ID__c FROM Stakeholder_Mapping__c WHERE Opportunity__c = :lstOpportunity];
            System.assertEquals(1, lstStakeholderMapping.size());
            System.assertEquals(oStakeholderMapping.Mobile_ID__c, lstStakeholderMapping[0].Mobile_ID__c);
            //------------------------------------------------------------------------------------


            //------------------------------------------------------------------------------------
            // Testing
            //------------------------------------------------------------------------------------
            RestRequest oRequest = new RestRequest();  
            RestResponse oResponse = new RestResponse();
            
            JSONGenerator oJSONGen = JSON.createGenerator(true);
                oJSONGen.writeStartObject();
                oJSONGen.writeStringField('id', '12346'); 
                oJSONGen.writeObjectField('oppStakeholderMapping', oStakeholderMapping);
            
            // Get the JSON string.
            String tJSON_Pretty = oJSONGen.getAsString();           
            System.debug('JSON ' + tJSON_Pretty);

                oRequest.requestbody = Blob.valueOf(tJSON_Pretty);
                oRequest.requestURI = '/services/apexrest/OpportunityStakeholderMappingService';  
                oRequest.httpMethod = 'DELETE';
            RestContext.request = oRequest;
            RestContext.response = oResponse;

            
            Test.startTest();

            ws_OpportunityStakeholderMappingService.IFOppStakeholderMappingDeleteResponse oIFOppStakeholderMappingDeleteResponse = ws_OpportunityStakeholderMappingService.doDelete();
            System.debug('Response' + oIFOppStakeholderMappingDeleteResponse);
            
            Test.stopTest();
            //------------------------------------------------------------------------------------


            //------------------------------------------------------------------------------------
            // Validation
            //------------------------------------------------------------------------------------
            System.assert(oIFOppStakeholderMappingDeleteResponse.success == true);
            lstStakeholderMapping = [SELECT Id FROM Stakeholder_Mapping__c WHERE Mobile_Id__c = : oStakeholderMapping.Mobile_ID__c];
            System.assert(lstStakeholderMapping.size() == 0);
            //------------------------------------------------------------------------------------

        }

    }

    @isTest private static void testOpportunityStakeholderMapping_Delete_NotFound(){

        User oUser_System = clsTestData_User.createUser_SystemAdministratorMDT('tadm01', false);
            oUser_System.Primary_sBU__c = 'Brain Modulation';
        insert oUser_System;
        
        System.runAs(oUser_System){

            //------------------------------------------------------------------------------------
            // Create Test Data
            //------------------------------------------------------------------------------------
            clsTestData_Account.iRecord_Account = 1;
            clsTestData_Account.createAccount();

            clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'Service_Contract_Opportunity').Id;
            clsTestData_Opportunity.iRecord_Opportunity = 1;
            List<Opportunity> lstOpportunity = clsTestData_Opportunity.createOpportunity(false);
            for (Opportunity oOpportunity : lstOpportunity){
                oOpportunity.StageName = 'Closed / Won';
                oOpportunity.CloseDate =  Date.today();
                oOpportunity.CurrencyISOCode = 'EUR';
                oOpportunity.Mobile_ID__c = GuidUtil.NewGuid();
            }
            insert lstOpportunity;

            clsTestData_Opportunity.iRecord_StakeholderMapping = 1;
            Stakeholder_Mapping__c oStakeholderMapping = clsTestData_Opportunity.createStakeholderMapping(false)[0];
            //------------------------------------------------------------------------------------


            //------------------------------------------------------------------------------------
            // Testing
            //------------------------------------------------------------------------------------
            RestRequest oRequest = new RestRequest();  
            RestResponse oResponse = new RestResponse();
            
            JSONGenerator oJSONGen = JSON.createGenerator(true);
                oJSONGen.writeStartObject();
                oJSONGen.writeStringField('id', '12346'); 
                oJSONGen.writeObjectField('oppStakeholderMapping', oStakeholderMapping);
            
            // Get the JSON string.
            String tJSON_Pretty = oJSONGen.getAsString();           
            System.debug('JSON ' + tJSON_Pretty);

                oRequest.requestbody = Blob.valueOf(tJSON_Pretty);
                oRequest.requestURI = '/services/apexrest/OpportunityStakeholderMappingService';  
                oRequest.httpMethod = 'DELETE';
            RestContext.request = oRequest;
            RestContext.response = oResponse;

            
            Test.startTest();

            ws_OpportunityStakeholderMappingService.IFOppStakeholderMappingDeleteResponse oIFOppStakeholderMappingDeleteResponse = ws_OpportunityStakeholderMappingService.doDelete();
            System.debug('Response' + oIFOppStakeholderMappingDeleteResponse);
            
            Test.stopTest();
            //------------------------------------------------------------------------------------


            //------------------------------------------------------------------------------------
            // Validation
            //------------------------------------------------------------------------------------
            System.assert(oIFOppStakeholderMappingDeleteResponse.success == true);
            System.assertEquals(oIFOppStakeholderMappingDeleteResponse.message, 'Stakeholder Mapping not found');
            //------------------------------------------------------------------------------------

        }

    }

    @isTest private static void testOpportunityStakeholderMapping_Error(){

        User oUser_System = clsTestData_User.createUser_SystemAdministratorMDT('tadm01', false);
            oUser_System.Primary_sBU__c = 'Brain Modulation';
        insert oUser_System;
        
        System.runAs(oUser_System){

            //------------------------------------------------------------------------------------
            // Create Test Data
            //------------------------------------------------------------------------------------
            clsTestData_Account.iRecord_Account = 1;
            clsTestData_Account.createAccount();

            clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'Service_Contract_Opportunity').Id;
            clsTestData_Opportunity.iRecord_Opportunity = 1;
            List<Opportunity> lstOpportunity = clsTestData_Opportunity.createOpportunity(false);
            for (Opportunity oOpportunity : lstOpportunity){
                oOpportunity.StageName = 'Closed / Won';
                oOpportunity.CloseDate =  Date.today();
                oOpportunity.CurrencyISOCode = 'EUR';
                oOpportunity.Mobile_ID__c = GuidUtil.NewGuid();
            }
            insert lstOpportunity;

            clsTestData_Opportunity.iRecord_StakeholderMapping = 1;
            Stakeholder_Mapping__c oStakeholderMapping = clsTestData_Opportunity.createStakeholderMapping()[0];

            List<Stakeholder_Mapping__c> lstStakeholderMapping = [SELECT Id, Mobile_ID__c FROM Stakeholder_Mapping__c WHERE Opportunity__c = :lstOpportunity];
            System.assertEquals(1, lstStakeholderMapping.size());
            System.assertEquals(oStakeholderMapping.Mobile_ID__c, lstStakeholderMapping[0].Mobile_ID__c);
            //------------------------------------------------------------------------------------


            //------------------------------------------------------------------------------------
            // Testing
            //------------------------------------------------------------------------------------
            RestRequest oRequest_Delete = new RestRequest();  
            RestRequest oRequest_Post = new RestRequest();  
            RestResponse oResponse = new RestResponse();
            
            JSONGenerator oJSONGen = JSON.createGenerator(true);
                oJSONGen.writeStartObject();
                oJSONGen.writeStringField('id', '12346'); 
                oJSONGen.writeObjectField('oppStakeholderMapping', oStakeholderMapping);
            
            // Get the JSON string.
            String tJSON_Pretty = oJSONGen.getAsString();           
            System.debug('JSON ' + tJSON_Pretty);

            Test.startTest();

            clsUtil.hasException = true;
                oRequest_Delete.requestbody = Blob.valueOf(tJSON_Pretty);
                oRequest_Delete.requestURI = '/services/apexrest/OpportunityStakeholderMappingService';  
                oRequest_Delete.httpMethod = 'DELETE';
            RestContext.request = oRequest_Delete;
            RestContext.response = oResponse;
            ws_OpportunityStakeholderMappingService.IFOppStakeholderMappingDeleteResponse oIFOppStakeholderMappingDeleteResponse = ws_OpportunityStakeholderMappingService.doDelete();

                oRequest_Post.requestBody = Blob.valueOf(tJSON_Pretty);
                oRequest_Post.requestURI = '/services/apexrest/OpportunityStakeholderMappingService';  
                oRequest_Post.httpMethod = 'POST';
            RestContext.request = oRequest_Post;
            RestContext.response = oResponse;
            ws_OpportunityStakeholderMappingService.IFOppStakeholderMappingResponse oIFOppStakeholderMappingResponse = ws_OpportunityStakeholderMappingService.doPost();

            Test.stopTest();
            //------------------------------------------------------------------------------------


            //------------------------------------------------------------------------------------
            // Validation
            //------------------------------------------------------------------------------------
            System.assert(oIFOppStakeholderMappingDeleteResponse.success == false);
            //------------------------------------------------------------------------------------

        }
        
    }

}