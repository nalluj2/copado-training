public with sharing class ctrl_Calendar_Event {
	
	@AuraEnabled
    public static void assignCase(String caseId, String memberId) {
    	
    	try{
	    		    		    	
	    	Case selectedCase = new Case();
	    	selectedCase.Id = caseId;
	    	selectedCase.Assigned_To__c = memberId;
	    	
	    	bl_Case_Trigger.runningSchedulingApp = true;
	    	
	    	update selectedCase;
	    	
	    	bl_Case_Trigger.runningSchedulingApp = false;
	    	
	    }catch(Exception e){
    		
    		AuraHandledException auraEx = new AuraHandledException(e.getMessage()); 
    		auraEx.setMessage(e.getMessage());
    		throw auraEx;
    	}    	
    	
    }
    
}