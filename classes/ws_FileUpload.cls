@RestResource(urlMapping='/FileUpload/v1/*')
global class ws_FileUpload {

    @HttpPost
    global static String uploadAttachment(){

        RestRequest oRestRequest = RestContext.request;
        RestResponse oRestResult = Restcontext.response;
        
        String tFileName = oRestRequest.params.get('FileName'); 
        String tParentId = oRestRequest.params.get('ParentId');
        String tContentType = oRestRequest.params.get('ContentType');
        Blob oBlob_Content = oRestRequest.requestBody; 
          
        Attachment oAttachment = new Attachment(ParentId = tParentId, Body = oBlob_Content, Name = tFileName, ContentType = tContentType);
         
        insert oAttachment;
        return oAttachment.Id;
   
   }

}