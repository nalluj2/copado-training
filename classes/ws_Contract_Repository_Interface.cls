global class ws_Contract_Repository_Interface {
    
    webservice static UploadDocumentResponse documentUploadConfirmation(UploadDocumentResponse confirmation){
    	
    	System.debug('Input:' + confirmation);
    	
    	UploadDocumentResponse response = new UploadDocumentResponse();
    	response.attachmentId = confirmation.attachmentId;
    	response.documentumId = confirmation.documentumId;
    	
    	try{
    	
	    	Attachment att = [Select Id, ParentId from Attachment where Id = :confirmation.attachmentId];
	    	
	    	Contract_Repository_Document_Version__c version = [Select Id, Document__c, Marked_for_Deletion__c from Contract_Repository_Document_Version__c where Id = :att.ParentId];
	    	
	    	if(confirmation.isSuccess == true){
	    		
	    		version.Documentum_ID__c = confirmation.documentumId;
	    		version.Interface_Error__c = null;
	    		
	    		update version;    		
	    		delete att;
	    		
	    	}else{
	    		
	    		if(version.Document__c == null) delete version; 
	    		else{
	    			
	    			version.Interface_Error__c = confirmation.errorMessage;
	    			if(version.Marked_for_Deletion__c == true) version.Is_Deleted__c = true;
	    			update version;
	    		}
	    	}	
	    	
	    	response.isSuccess = true;
	    	
    	}catch(Exception e){
    		
    		response.isSuccess = false;
    		response.errorMessage = e.getMessage() + ' | ' + e.getStackTraceString();    		
    	}
    	
    	return response;
    }    
    
    global class UploadDocumentResponse{
        
        webservice String attachmentId;
        webservice String documentumId;
        webservice Boolean isSuccess;
        webservice String errorMessage;        
    }    
}