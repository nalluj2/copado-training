@isTest(seeAlldata=true)
    Private class Test_tr_Opty_AftrUpdateScheduleStartDate
    {    
        static testMethod void myUnitTest() 
        {   

            User oUser_System = clsTestData_User.createUser_SystemAdministratorMDT('tadm01', false);
                  oUser_System.Primary_sBU__c = 'Brain Modulation';
            insert oUser_System;

            System.runAs(oUser_System){

                  //Insert Company
                   Company__c cmpny = new Company__c();
                   cmpny.name='TestMedProd';
                   cmpny.CurrencyIsoCode = 'EUR';
                   cmpny.Current_day_in_Q1__c=56;
                   cmpny.Current_day_in_Q2__c=34; 
                   cmpny.Current_day_in_Q3__c=5; 
                   cmpny.Current_day_in_Q4__c= 0;   
                   cmpny.Current_day_in_year__c =200;
                   cmpny.Days_in_Q1__c=56;  
                   cmpny.Days_in_Q2__c=34;
                   cmpny.Days_in_Q3__c=13;
                   cmpny.Days_in_Q4__c=22;
                   cmpny.Days_in_year__c =250;
                   cmpny.Company_Code_Text__c = 'T27';
                   insert cmpny;
                   
                   Business_Unit__c BU1 = new Business_Unit__c();
                   BU1.Name='TestBUMedProd';
                   BU1.Company__c = cmpny.Id;
                   Insert BU1;

                   Sub_Business_Units__c SBU = new Sub_Business_Units__c();
                   SBU.Name='TestSBUMedProd';
                   SBU.Business_Unit__c = BU1.Id;
                   Insert SBU;         
          
                   Therapy_Group__c TG=new Therapy_Group__c();
                   TG.Name='TestTGMedProd';
                   TG.Sub_Business_Unit__c = SBU.id;
                   TG.Company__c = cmpny.Id;
                   insert TG;    
                  
                   Therapy__c th = new Therapy__c();
                   th.Name = 'th11';
                   th.Therapy_Group__c = TG.Id;
                   th.Sub_Business_Unit__c = SBU.id;
                   th.Business_Unit__c = BU1.id;
                   th.Therapy_Name_Hidden__c = 'th11'; 
                   insert th;
                   
                  RecordType oppRecType = clsUtil.getRecordTypeByDevName('Opportunity', 'Non_Capital_Opportunity');
                   List<Account> lstAccount=new list<Account>();
                   Account testAc1 = new Account() ;
                   testAc1.Name = 'Test Account Name1';  
                   testAc1.RecordTypeId = FinalConstants.sapAccountRecordTypeId ;
                   testAc1.Account_Country_vs__c='BELGIUM';
                   lstAccount.add(testAc1);
                   Account testAc2 = new Account() ;
                   testAc2.Name = 'Test Account Name2';  
                   testAc2.RecordTypeId = FinalConstants.sapAccountRecordTypeId ;
                   testAc2.Account_Country_vs__c='BELGIUM';
                   lstAccount.add(testAc2);
                   insert lstAccount;
                   
                   Product2 prd = new Product2();
              	prd.Name = 'Unit test product';
              	insert prd;
                   
                   Opportunity_Bundle__c opb=new Opportunity_Bundle__c(Account__c=testAc1.id);
                   insert opb;
                  
                   opportunity op=new opportunity(name='testopp',Deal_Category__c='Upgrade',Opportunity_Bundle__c=opb.id,CloseDate=system.today()+2,stagename='Qualification',accountid=testAc1.id,ForecastCategoryName='Pipeline',CurrencyIsoCode='EUR',type='Service Contract');
                   insert op;
                   //opportunity op1=new opportunity(name='testopp',Opportunity_Bundle__c=opb.id,Asset__c=testAsset1.id,CloseDate=system.today()+5,stagename='Qualification',accountid=testAc1.id,ForecastCategoryName='Pipeline',CurrencyIsoCode='EUR',type='Service Contract');
                   op.closedate=system.today()+5;
                   update op;
                   
                   /*OpportunityLineItemSchedule olis=new OpportunityLineItemSchedule();
                   olis.OpportunityLineItem.Opportunity.Id=op.id;
                   insert olis;
                   */

            }
            
      }
}