/*
 *      Description : This class exposes methods to create / update / delete an Opportunity 
 *
 *      Author = Rudy De Coninck
 */
@RestResource(urlMapping='/OpportunityService/*')
global class ws_OpportunityService{
	
	@HttpPost
	global static IFOpportunityResponse doPost() {
 		RestRequest req = RestContext.request;
			
		System.debug('post called '+req);
		System.debug('post requestBody '+req.requestBody);
			
		String body = req.requestBody.toString();
		IFOpportunityRequest opportunityRequest = (IFOpportunityRequest)System.Json.deserialize(body, IFOpportunityRequest.class);
			
		System.debug('post requestBody '+opportunityRequest);
		IFOpportunityResponse resp = new IFOpportunityResponse();	
		try{
				
			Opportunity opp = opportunityRequest.opportunity;
			
			resp.opportunity = bl_AccountPlanning.saveOpportunity(opp);
			resp.id = opportunityRequest.id;
	
			resp.success = true;
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
		}
 
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'Opportunity' ,req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
  			
		return resp;		
	}

	@HttpDelete
	global static IFOpportunityDeleteResponse doDelete(){
		RestRequest req = RestContext.request;
			
		System.debug('post called '+req);
		System.debug('post requestBody '+req.requestBody);
			
		String body = req.requestBody.toString();
		IFOpportunityDeleteResponse resp = new IFOpportunityDeleteResponse();
		
		IFOpportunityDeleteRequest opportunityDeleteRequest = (IFOpportunityDeleteRequest)System.Json.deserialize(body, IFOpportunityDeleteRequest.class);
			
		System.debug('post requestBody '+OpportunityDeleteRequest);
			
		Opportunity opp =OpportunityDeleteRequest.opportunity;
		
		List<Opportunity>oppToDelete;
		if(opp==null || opp.mobile_Id__c==null){
			oppToDelete = new List<Opportunity>();
		}else{
			oppToDelete = [select id, mobile_id__c from Opportunity where Mobile_ID__c = :opp.Mobile_ID__c];
		}
		
		try{
			
			if (oppToDelete !=null && oppToDelete.size()>0 ){	
				
				delete oppToDelete;
				resp.opportunity = oppToDelete.get(0);
				resp.id = OpportunityDeleteRequest.id;
				resp.success = true;
			}else{
				resp.opportunity = opp;
				resp.id = OpportunityDeleteRequest.id;
				resp.message='Opportunity not found';
				resp.success = true;
			}
			
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
		}
  			
			
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'OpportunityDelete',req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
		
		return resp;
	}
	
	global class IFOpportunityRequest{
		public String id {get;set;}
		public Opportunity opportunity {get;set;}
	}
	
	global class IFOpportunityDeleteRequest{
		public String id{get;set;}
		public Opportunity opportunity {get;set;}
	}
		
	global class IFOpportunityDeleteResponse{
		public String id{get;set;}
		public Opportunity opportunity {get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}

	global class IFOpportunityResponse{
		public String id {get;set;}
		public Opportunity opportunity {get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}
	

}