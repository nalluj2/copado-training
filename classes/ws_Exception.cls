/*
 * Description      : Exception class extension to generate custom error message.
 *					  Used for interfaces.
 * Author           : Patrick Brinksma
 * Created Date     : 23-07-2013
 */
public class ws_Exception extends Exception{
}