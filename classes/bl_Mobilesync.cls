/*
 *      Description : This class centralizes items related to mobileSync
 *
 *      Author = Rudy De Coninck
 */
public with sharing class bl_Mobilesync {

	public static void storeRequest(Id userId, String requestType, String body, String responseBody, boolean success){
		
		Mobile_Sync_Request__c request = new Mobile_Sync_Request__c();
		request.user__c = userId;
		request.type__c = requestType;
		
		CallRecordServiceSettings__c settings = CallRecordServiceSettings__c.getInstance('default');
		//by default, store request and response
		if (null==settings || settings.Log_request__c){
			request.body__c = body;
		}
		if (null == settings || settings.Log_response__c){
			request.responsebody__c = responseBody;
		}
		
		if (success){
			request.status__c = 'TRUE';
		}else{
			request.status__c = 'FALSE';
		}
		
		insert request;
		
	} 


}