/*
 *      Created Date : 20121001
 *      Description : This class centralizes all the logic link to the User Territory object
 *
 *      Author = Rudy De Coninck
 */
public with sharing class bl_UserTerritory {
	/*
	public static List<Territory2> getForecastTerritoriesForUser(Id userId){
		List<Id> userIds = new List<Id>();
		userIds.add(userId);
		return getForecastTerritories(userIds);
	}
	
	public static List<Territory2> getForecastTerritories(List<Id> userIdsToProcess){
		
		List<Territory2> directForecastTerritoriesForUser = [
			select Territory2Type.DeveloperName, ForecastUserId from Territory2 where ForecastUserId in: userIdsToProcess
		];
		
		List<Territory2> allTEforecaseTerritories = new List<Territory2>();
		
		
		for (Territory2 t : directForecastTerritoriesForUser){
			if (t.Territory2Type.DeveloperName.compareTo('Territory')==0){
				allTEforecaseTerritories.add(t);
			}else{
				List<Territory2> resultTerritories = bl_Territory.calculateChildrenForTerritory(t.id, 'Territory');
				allTEforecaseTerritories.addAll(resultTerritories); 
			}
		}
		return allTEforecaseTerritories;
	}
	*/
}