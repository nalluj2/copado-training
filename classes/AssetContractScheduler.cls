/**
 *      Created Date : 20121106
 *      Description : This class is used to disactivate contract when end date is reached 
 * 
 *      Author = Rudy De Coninck
 */

global class AssetContractScheduler implements Schedulable, Database.Batchable<SObject>{
	
	 //Only process Activated contracts
	global String query = 'select Id, EndDate from Contract where EndDate <= '+String.valueOf((Date.today()-1))+ ' and Status=\'Activated\'';
	
	global void execute(SchedulableContext SC) {  
   	  
		AssetContractScheduler oBatch = new AssetContractScheduler();
		Database.executebatch(oBatch, 1);        	
//		Database.executeBatch(new AssetContractScheduler());   

    }
   
   
    global Database.QueryLocator start(Database.BatchableContext ctx){    
        System.Debug('>>>>> query - ' + query);
        return Database.getQueryLocator(query);         
    }

    global void execute(Database.BatchableContext ctx, List<SObject> records){
      //Update contract where End date has been reached, trigger on contract will handle deactivation
		List<Contract> contracts = (List<Contract>)records;
		for (Contract contract : contracts){
			if (Date.today()>contract.EndDate){
				contract.Status='Expired';
			}
		}      
   		update contracts; 
    }
    
     global void finish(Database.BatchableContext ctx){
     
     }
}