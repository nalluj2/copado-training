public with sharing class ctrl_Lookup_Popup {
	
    //-BC - 20150616 - CR-5767 - START
	public Boolean bRemoveFilter { get;set; }
	public Boolean bShowRemoveFilter { get;set; }
    //-BC - 20150616 - CR-5767 - STOP

	public List<SObject> records {get;set;}
	public List<SObject> recentRecords {get; set;}
	
	public String searchString {get;set;}
	public Boolean hasResults {get;set;}
	
	public String componentId {get; set;}
	public String objectType {get; set;}
	public List<String> fieldNames {get;set;}
	private String fieldString;
	private String filter;
		
	//Controller used by ChatterLookupPopup
	public ctrl_Lookup_Popup(){
		
		searchString = '';
		hasResults = false;		
		//-BC - 20150616 - CR-5767 - START
		bShowRemoveFilter = false;
		bRemoveFilter = false;
		//-BC - 20150616 - CR-5767 - STOP


		String inputParams = ApexPages.currentPage().getParameters().get('p');
		
		if(inputParams!=null){
			
			String params = CryptoUtils.decryptText(inputParams); 
			
			for(String valuePair : params.split('&')){
				
				String[] pair = valuePair.split('=');
				
				if(pair.size() == 2){
					if(pair[0] == 'compId'){
						componentId = pair[1];
					}else if(pair[0] == 'sobject'){
						objectType = pair[1];	
					}else if(pair[0] == 'filter'){
						filter  = EncodingUtil.urlDecode(pair[1], 'UTF-8');
					}else if(pair[0] == 'fields'){
						fieldString = pair[1];
						fieldNames = fieldString.split(',');
					//-BC - 20150616 - CR-5767 - START
					}else if (pair[0] == 'overruleTargetFilter'){
						bShowRemoveFilter = Boolean.valueOf(pair[1]);

					}
					//-BC - 20150616 - CR-5767 - STOP
				}				 
			}
		}	
		
		String inputText = ApexPages.currentPage().getParameters().get('inputValue');

		if(inputText != null && inputText != ''){
			
			searchString = inputText;
			search();
			
		}else{
			
			getRecentlyViewed();
		}			
	}

	//-BC - 20150616 - CR-5767 - START
	public void toggleFilter(){
		if (bShowRemoveFilter){
			bRemoveFilter = !bRemoveFilter;
			String tSearch = searchString.replace('*', '');
			if (tSearch.length() == 0){
				getRecentlyViewed();
			}else{
				search();
			}
		}
	}
	//-BC - 20150616 - CR-5767 - STOP
				
	//perform search and return error or results.
	//-BC - 20150616 - CR-5767 - START
	public void search(){
		List<SObject> searchResult;

        // If the searchString is empty, perform a SOQL instead of a finc
		String tSearch = clsUtil.isNull(searchString, '').replace('*', '');
		String tFilter = clsUtil.isNull(filter, '');
		if (!bRemoveFilter && tSearch.length() == 0 && tFilter != ''){
			String tSOQL = 'SELECT Id FROM ' + objectType + tFilter;
			searchResult = Database.query('SELECT Id FROM ' + objectType + ' ' + tFilter);
		}else{
			if (tSearch.length() < 2){
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'search term must be at least two characters'));
				return;
			}
			String escapedInput = String.escapeSingleQuotes(searchString);
			if(escapedInput.endsWith('*')==false) escapedInput+='*';

			if (bRemoveFilter) tFilter = '';
			String searchquery ='FIND \'' + escapedInput + '\'IN ALL FIELDS RETURNING ' + objectType + '(id ' + tFilter + ')';

			List<List<SObject>> searchList = search.query(searchquery);
		
			searchResult = searchList[0];
		}

		if(searchResult.isEmpty()){
			hasResults = false;
			if (searchResult == null){
				searchResult = new List<SObject>();
			}
			records = searchResult;
		}else{
		
			records = Database.query('Select Id, '+fieldString+' from '+objectType+' where Id IN:searchResult ORDER BY Name ASC LIMIT 100');
			hasResults = true;
		}		
	}	

//	public void search(){
//		
//		if(searchString.length()<2){
//			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'search term must be at least two characters'));
//			return;
//		}
//		
//		String escapedInput = String.escapeSingleQuotes(searchString);
//		
//		if(escapedInput.endsWith('*')==false) escapedInput+='*';
//
//		String searchquery='FIND \''+escapedInput+'\'IN ALL FIELDS RETURNING '+objectType+'(id '+filter+')';
//		List<List<SObject>> searchList = search.query(searchquery);
//		
//		List<SObject> searchResult = searchList[0];
//		if(searchResult.isEmpty()){
//			hasResults = false;
//			if (searchResult == null){
//				searchResult = new List<SObject>();
//			}
//			records = searchResult;
//		}else{
//		
//			records = Database.query('Select Id, '+fieldString+' from '+objectType+' where Id IN:searchResult ORDER BY Name ASC LIMIT 100');
//			hasResults = true;
//		}		
//	}
	//-BC - 20150616 - CR-5767 - STOP


	
	private void getRecentlyViewed(){
		
		List<SObject> recents = [SELECT Id FROM RecentlyViewed WHERE Type=:objectType ORDER BY LastViewedDate DESC];
		
		Set<Id> recentIds = new Set<Id>();
		for(SObject recent : recents) recentIds.add(recent.Id);
		
		
        //-BC - 20150616 - CR-5767 - START
		String tSOQL = 'Select Id, ' + fieldString + ' from ' + objectType + ' where Id IN:recentIds';
		if ( (!bRemoveFilter) && (clsUtil.isNull(filter, '').Trim() != '') ){
			Integer iPos = filter.toUpperCase().indexOf('WHERE');

			String tFilter = filter.mid(0, iPos);
			tFilter += ' AND ' + filter.mid(iPos + 5, filter.Length());
			tSOQL += tFilter;
		}
		Map<Id, SObject> recentMap = new Map<Id, SObject>(Database.query(tSOQL));
//		Map<Id, SObject> recentMap = new Map<Id, SObject>(Database.query('Select Id, '+fieldString+' from '+objectType+' where Id IN:recentIds'));
        //-BC - 20150616 - CR-5767 - STOP
		
		recentRecords = new List<SObject>();
		
		for(Sobject recent : recents){
			
			SObject rec = recentMap.get(recent.Id);
			recentRecords.add(rec);			
		} 
	}
	
	//reset search, display all users
	public void clearResults(){
		searchString = '';
		hasResults = false;		
		records = null;
		getRecentlyViewed();
	}
			
}