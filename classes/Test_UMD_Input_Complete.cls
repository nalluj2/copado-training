@isTest
private class Test_UMD_Input_Complete {
    
    @TestSetup
    private static void createMasterData(){
    	
    	Company__c eur = new Company__c();		
        eur.Name = 'Europe';
        eur.CurrencyIsoCode = 'EUR';
        eur.Current_day_in_Q1__c = 56;
        eur.Current_day_in_Q2__c = 34; 
        eur.Current_day_in_Q3__c = 5; 
        eur.Current_day_in_Q4__c =  0;   
        eur.Current_day_in_year__c = 200;
        eur.Days_in_Q1__c = 56;  
        eur.Days_in_Q2__c = 34;
        eur.Days_in_Q3__c = 13;
        eur.Days_in_Q4__c = 22;
        eur.Days_in_year__c = 250;
        eur.Company_Code_Text__c = 'EUR';
		insert eur;
		
		Business_Unit_Group__c bug =  new Business_Unit_Group__c();
        bug.Master_Data__c = eur.id;
        bug.Abbreviated_Name__c = 'CVG';
        bug.Name = 'CVG';	                
        insert bug;
		
		Business_Unit__c bu =  new Business_Unit__c();
        bu.Company__c = eur.id;
        bu.Name = 'Vascular';
        bu.Business_Unit_Group__c = bug.Id;
        bu.Account_Plan_Activities__c = true;        
        insert bu;
        
        Sub_Business_Units__c sbuPV = new Sub_Business_Units__c();
        sbuPV.Name = 'Coro + PV';
        sbuPV.Business_Unit__c = bu.id;
        
        Sub_Business_Units__c sbuAortic = new Sub_Business_Units__c();
        sbuAortic.Name = 'Aortic';
        sbuAortic.Business_Unit__c = bu.id;
        
		insert new List<Sub_Business_Units__c>{sbuPV, sbuAortic};
    	
    	UMD_Purpose__c currentBehavioral = new UMD_Purpose__c();
    	currentBehavioral.Fiscal_Year_VS__c = 'FY2021';
    	currentBehavioral.Segmentation_Qualification__c = 'Behavioral';
    	currentBehavioral.Status__c = 'Open for input';
    	currentBehavioral.Name = 'Behavioral FY2021';
    	currentBehavioral.UMD_Purpose_Description__c = 'Behavioral FY2021';
    	
    	UMD_Purpose__c currentStrategical = new UMD_Purpose__c();
    	currentStrategical.Fiscal_Year_VS__c = 'FY2021';
    	currentStrategical.Segmentation_Qualification__c = 'Strategical';
    	currentStrategical.Status__c = 'Open for input';
    	currentStrategical.Name = 'Strategical FY2021';
    	currentStrategical.UMD_Purpose_Description__c = 'Strategical FY2021';
    	    	
    	UMD_Purpose__c lastBehavioral = new UMD_Purpose__c();
    	lastBehavioral.Fiscal_Year_VS__c = 'FY2020';
    	lastBehavioral.Segmentation_Qualification__c = 'Behavioral';
    	lastBehavioral.Status__c = 'Completed';
    	lastBehavioral.Name = 'Behavioral FY2020';
    	lastBehavioral.UMD_Purpose_Description__c = 'Behavioral FY2021';
    	
    	insert new List<UMD_Purpose__c>{currentBehavioral, currentStrategical, lastBehavioral};
    	
    	UMD_Question__c question1 = new UMD_Question__c();
    	question1.Question__c = 'Question 1';
    	question1.Helptext_Rich__c = 'Question 1';
    	question1.Source_of_answer__c = 'User';    	
    	question1.Type__c = 'Picklist (single value)';    	
    	question1.Answers__c = '1\n2\n3\n4\n5';
    	question1.Unique_identifier__c = 'QUESTION_1';
    	
    	UMD_Question__c question2 = new UMD_Question__c();
    	question2.Question__c = 'Question 2';
    	question2.Helptext_Rich__c = 'Question 2';
    	question2.Source_of_answer__c = 'User';    	
    	question2.Type__c = 'Picklist (multiple value)';    	
    	question2.Answers__c = '1\n2\n3\n4\n5';  	
    	question2.Unique_identifier__c = 'QUESTION_2';
    	
    	UMD_Question__c question3 = new UMD_Question__c();
    	question3.Question__c = 'Question 3';
    	question3.Helptext_Rich__c = 'Question 3';
    	question3.Source_of_answer__c = 'User';    	
    	question3.Type__c = 'Number';
    	question3.Min_Value__c = 0;
    	question3.Max_Value__c = 100;    	
    	question3.Unique_identifier__c = 'QUESTION_3';
    	
    	UMD_Question__c question4 = new UMD_Question__c();
    	question4.Question__c = 'Question 4';
    	question4.Helptext_Rich__c = 'Question 4';
    	question4.Source_of_answer__c = 'User';    	
    	question4.Type__c = 'Yes/No';   	
    	question4.Unique_identifier__c = 'QUESTION_4';
    	
    	UMD_Question__c question5 = new UMD_Question__c();
    	question5.Question__c = 'Question 5';
    	question5.Helptext_Rich__c = 'Question 5';
    	question5.Source_of_answer__c = 'System';    	
    	question5.Type__c = 'Number';   	
    	question5.Unique_identifier__c = 'QUESTION_5';
    	
    	insert new List<UMD_Question__c>{question1, question2, question3, question4, question5};
    	
    	UMD_Purpose_Question__c currentQuestion1 = new UMD_Purpose_Question__c();
    	currentQuestion1.UMD_Purpose__c = currentBehavioral.Id;
    	currentQuestion1.UMD_Question__c = question1.Id;
    	currentQuestion1.Sequence__c = 1;
    	
    	UMD_Purpose_Question__c currentQuestion2 = new UMD_Purpose_Question__c();
    	currentQuestion2.UMD_Purpose__c = currentBehavioral.Id;
    	currentQuestion2.UMD_Question__c = question2.Id;
    	currentQuestion2.Sequence__c = 2;
    	
    	UMD_Purpose_Question__c currentQuestion3 = new UMD_Purpose_Question__c();
    	currentQuestion3.UMD_Purpose__c = currentBehavioral.Id;
    	currentQuestion3.UMD_Question__c = question3.Id;
    	currentQuestion3.Sequence__c = 3;
    	
    	UMD_Purpose_Question__c currentQuestion4 = new UMD_Purpose_Question__c();
    	currentQuestion4.UMD_Purpose__c = currentBehavioral.Id;
    	currentQuestion4.UMD_Question__c = question4.Id;
    	currentQuestion4.Sequence__c = 4;
    	currentQuestion4.Sub_Business_Unit__c = 'Coro + PV';
    	
    	UMD_Purpose_Question__c currentQuestion5 = new UMD_Purpose_Question__c();
    	currentQuestion5.UMD_Purpose__c = currentBehavioral.Id;
    	currentQuestion5.UMD_Question__c = question5.Id;
    	currentQuestion5.Sequence__c = 5;
    	
    	UMD_Purpose_Question__c lastQuestion1 = new UMD_Purpose_Question__c();
    	lastQuestion1.UMD_Purpose__c = lastBehavioral.Id;
    	lastQuestion1.UMD_Question__c = question1.Id;
    	lastQuestion1.Sequence__c = 1;
    	
    	UMD_Purpose_Question__c lastQuestion2 = new UMD_Purpose_Question__c();
    	lastQuestion2.UMD_Purpose__c = lastBehavioral.Id;
    	lastQuestion2.UMD_Question__c = question2.Id;
    	lastQuestion2.Sequence__c = 2;
    	
    	UMD_Purpose_Question__c lastQuestion3 = new UMD_Purpose_Question__c();
    	lastQuestion3.UMD_Purpose__c = lastBehavioral.Id;
    	lastQuestion3.UMD_Question__c = question3.Id;
    	lastQuestion3.Sequence__c = 3;
    	
    	UMD_Purpose_Question__c currentStratQuestion1 = new UMD_Purpose_Question__c();
    	currentStratQuestion1.UMD_Purpose__c = currentStrategical.Id;
    	currentStratQuestion1.UMD_Question__c = question1.Id;
    	currentStratQuestion1.Sequence__c = 1;
    	
    	UMD_Purpose_Question__c currentStratQuestion2 = new UMD_Purpose_Question__c();
    	currentStratQuestion2.UMD_Purpose__c = currentStrategical.Id;
    	currentStratQuestion2.UMD_Question__c = question2.Id;
    	currentStratQuestion2.Business_Unit_Group__c = 'CVG';
    	currentStratQuestion2.Sequence__c = 2;
    	
    	insert new List<UMD_Purpose_Question__c>{currentQuestion1, currentQuestion2, currentQuestion3, currentQuestion4, currentQuestion5, lastQuestion1, lastQuestion2, lastQuestion3, currentStratQuestion1, currentStratQuestion2};
    	
    }
            
    private static testMethod void testTriggers(){
    	
    	UMD_Purpose__c currentPurpose = [Select Id, Segmentation_Qualification__c, (Select UMD_Question__c from UMD_Purpose_Question__r ORDER BY Sequence__c) from UMD_Purpose__c where Name = 'Behavioral FY2021'];
    	    	
    	Account acc = new Account();
    	acc.Name = 'Test Account';
    	acc.SAP_Id__c = '121231234';
    	acc.BillingCity = 'Paris';
    	insert acc;
    	
    	Customer_Segmentation__c accSegmentPV = new Customer_Segmentation__c();
    	accSegmentPV.Account__c = acc.Id;
    	accSegmentPV.Sub_Business_Unit__c = 'Coro + PV';
    	accSegmentPV.Behavioral_Segment__c = 'Clinical';
    	accSegmentPV.Fiscal_Year__c = 'FY2020'; 
    	
    	Customer_Segmentation__c accSegmentCVG = new Customer_Segmentation__c();
    	accSegmentCVG.Account__c = acc.Id;
    	accSegmentCVG.Business_Unit_Group__c = 'CVG';
    	accSegmentCVG.Strategical_Segment__c = 'Strategic';
    	accSegmentCVG.Fiscal_Year__c = 'FY2020'; 
    	   	
    	insert new List<Customer_Segmentation__c>{accSegmentPV, accSegmentCVG};
    	
    	UMD_Input_Complete__c currentInput = new UMD_Input_Complete__c();
    	currentInput.Account__c = acc.Id;
    	currentInput.UMD_Purpose__c = currentPurpose.Id;
    	currentInput.Sub_Business_Unit__c = 'Coro + PV';
    	currentInput.Assigned_To__c = UserInfo.getUserId();
    	currentInput.Approver_1__c = [Select Id from User where isActive = true and ProfileId = :UserInfo.getProfileId() LIMIT 1].Id;    	
    	insert currentInput;
    	
    	UMD_Input__c currentInputAnswer1 = new UMD_Input__c();
    	currentInputAnswer1.UMD_Input_Complete__c = currentInput.Id;
    	currentInputAnswer1.UMD_Question__c = currentPurpose.UMD_Purpose_Question__r[0].UMD_Question__c;
    	currentInputAnswer1.Answer__c = '1';
    	
    	UMD_Input__c currentInputAnswer2 = new UMD_Input__c();
    	currentInputAnswer2.UMD_Input_Complete__c = currentInput.Id;
    	currentInputAnswer2.UMD_Question__c = currentPurpose.UMD_Purpose_Question__r[1].UMD_Question__c;
    	currentInputAnswer2.Answer__c = '2';
    	
    	UMD_Input__c currentInputAnswer3 = new UMD_Input__c();
    	currentInputAnswer3.UMD_Input_Complete__c = currentInput.Id;
    	currentInputAnswer3.UMD_Question__c = currentPurpose.UMD_Purpose_Question__r[2].UMD_Question__c;
    	currentInputAnswer3.Answer__c = '80';
    	
    	insert new List<UMD_Input__c>{currentInputAnswer1, currentInputAnswer2, currentInputAnswer3};
    	
    	currentInput = [Select Id, Current_Segmentation__c from UMD_Input_Complete__c where Id = :currentInput.Id];
    	System.assert(currentInput.Current_Segmentation__c == accSegmentPV.Id);
    	    	
    	currentInput.Sub_Business_Unit__c = 'Aortic';
    	currentInput.Approver_1__c = null;
    	update currentInput;
    	
    	currentInput = [Select Id, Current_Segmentation__c from UMD_Input_Complete__c where Id = :currentInput.Id];
    	System.assert(currentInput.Current_Segmentation__c == null);
    	
    	Customer_Segmentation__c accSegmentAortic = new Customer_Segmentation__c();
    	accSegmentAortic.Account__c = acc.Id;
    	accSegmentAortic.Sub_Business_Unit__c = 'Aortic';
    	accSegmentAortic.Behavioral_Segment__c = 'Transactional';
    	accSegmentAortic.Fiscal_Year__c = 'FY2020';
    	insert accSegmentAortic;
    	
    	currentInput = [Select Id, Current_Segmentation__c from UMD_Input_Complete__c where Id = :currentInput.Id];
    	System.assert(currentInput.Current_Segmentation__c == accSegmentAortic.Id);
    	    	
    	currentPurpose.Fiscal_Year_VS__c = 'FY2022';
    	
    	Boolean hasError = false;
    	
    	try{
    		
    		update currentPurpose;
    		
    	}catch(Exception e){
    		
    		hasError = true;
    		System.assert(e.getMessage().contains('This purpose is in use by Segmentation input records'));
    	}
    	
    	System.assert(hasError);
    	    	
    	UMD_Purpose__c nextPurpose = currentPurpose.clone();
    	nextPurpose.Fiscal_Year_VS__c = 'FY2022';    	
    	nextPurpose.Status__c = 'Preparation';
    	nextPurpose.Name = 'Behavioral FY2022';
    	nextPurpose.UMD_Purpose_Description__c = 'Behavioral FY2022';
    	insert nextPurpose;   
    	
    	List<UMD_Purpose_Question__c> nextPurposeQuestions = [Select Id from UMD_Purpose_Question__c where UMD_Purpose__c = :nextPurpose.Id];
    	System.assert(nextPurposeQuestions.size() == 5);
    	
    	accSegmentAortic.Fiscal_Year__c = 'FY2019';
    	update accSegmentAortic;
    	
    	currentInput = [Select Id, Current_Segmentation__c from UMD_Input_Complete__c where Id = :currentInput.Id];
    	System.assert(currentInput.Current_Segmentation__c == null);
    }
    
    private static testMethod void testScreenController(){
    	
    	createTestData();
    	
    	UMD_Input_Complete__c currentInput = [Select Id from UMD_Input_Complete__c where UMD_Purpose__r.Fiscal_Year_vs__c = 'FY2021' AND UMD_Purpose__r.Segmentation_Qualification__c = 'Behavioral'];
    	
    	Test.startTest();
    	
    	ctrl_Account_Performance_Questions.Formulaire result = ctrl_Account_Performance_Questions.getUserFormulaire('Behavioral', 'Input' ,'Aortic');    	
    	System.assert(result.accountInputs.size() == 0);
    	System.assert(result.questions.size() == 3);
    	
    	result = ctrl_Account_Performance_Questions.getUserFormulaire('Strategical', 'Input' ,'CVG');    	
    	System.assert(result.accountInputs.size() == 1);
    	System.assert(result.questions.size() == 2);
    	
    	result = ctrl_Account_Performance_Questions.getUserFormulaire('Strategical', 'Input' ,'MITG');    	
    	System.assert(result.accountInputs.size() == 0);
    	System.assert(result.questions.size() == 1);
    	
    	result = ctrl_Account_Performance_Questions.getUserFormulaire('Behavioral', 'Approval' ,'Coro + PV');    	
    	System.assert(result.accountInputs.size() == 0);
    	System.assert(result.questions.size() == 4);
    	
    	result = ctrl_Account_Performance_Questions.getUserFormulaire('Behavioral', 'Input' ,'Coro + PV');    	
    	System.assert(result.accountInputs.size() == 1);
    	System.assert(result.questions.size() == 4);
    	
    	ctrl_Account_Performance_Questions.FormulaireAccountInput input = result.accountInputs[0];
    	System.assert(input.prevAccountAnswers.size() == 4);
    	input.changed = true;
    	input.record.Complete__c = true;
    	
    	System.assert(input.accountAnswers.size() == 4);
    	input.accountAnswers[0].changed = true;
    	input.accountAnswers[0].record.Answer__c = '2';
    	
    	input.accountAnswers[1].changed = true;
    	input.accountAnswers[1].record.Answer__c = '1';
    	
    	input.accountAnswers[2].changed = true;
    	input.accountAnswers[2].record.Answer__c = '60';
    	
    	input.accountAnswers[3].changed = true;
    	input.accountAnswers[3].record.Answer__c = 'No';
    	
    	ctrl_Account_Performance_Questions.saveUserFormulaire(result.accountInputs);
    	
    	result = ctrl_Account_Performance_Questions.getUserFormulaire('Behavioral', 'Input' ,'Coro + PV');
    	
    	List<UMD_Input__c> answers = [Select Id from UMD_Input__c where UMD_Input_Complete__c = :currentInput.Id];
    	System.assert(answers.size() == 4);
    	
    	result.accountInputs[0].accountAnswers[2].changed = true;
    	result.accountInputs[0].accountAnswers[2].record.Answer__c = null;
    	
    	ctrl_Account_Performance_Questions.saveUserFormulaire(result.accountInputs);
    	
    	answers = [Select Id from UMD_Input__c where UMD_Input_Complete__c = :currentInput.Id];
    	System.assert(answers.size() == 3);
    }
    
    private static testMethod void testScreenControllerBehavioralAdmin(){
    	
    	createTestData();
    	
    	Account acc = [Select Id, Name, BillingCountry from Account];
    	UMD_Input_Complete__c currentInput = [Select Id from UMD_Input_Complete__c where UMD_Purpose__r.Fiscal_Year_vs__c = 'FY2021' AND UMD_Purpose__r.Segmentation_Qualification__c = 'Behavioral'];
    	
    	Account_Segmentation_Validation__c accSegmentValSBU = new Account_Segmentation_Validation__c();
    	accSegmentValSBU.Account__c = acc.Id;
    	accSegmentValSBU.Sub_Business_Unit__c = 'Coro + PV';
    	accSegmentValSBU.Behavioral_Segment__c = 'Clinical';
    	accSegmentValSBU.Fiscal_Year__c = 'FY2021';
    	accSegmentValSBU.Segmentation_Level__c = 'Sub Business Unit';
    	insert accSegmentValSBU;
    	
    	Test.startTest();
    	
    	Map<String, List<ctrl_Account_Performance_Questions.PicklistValue>> mainResult = ctrl_Acc_Segmentation_Questions_Admin.getMainFilterOptions('Behavioral', null);    	
    	System.assert(mainResult.get('fiscalYearOptions').size() == 2);
    	System.assert(mainResult.get('fiscalYear').size() == 1);
    	System.assert(mainResult.get('fiscalYear')[0].value == 'FY2021');
    	System.assert(mainResult.get('scopeOptions').size() == 1);
    	
    	Map<String, List<ctrl_Account_Performance_Questions.PicklistValue>> secondaryResult = ctrl_Acc_Segmentation_Questions_Admin.getSecondaryFilterOptions('Behavioral', 'FY2021', 'Coro + PV', 'Clinical', null);
    	System.assert(secondaryResult.get('segmentOptions').size() == 2);
    	System.assert(secondaryResult.get('countryOptions').size() == 2);
    	 System.debug('============' + acc);   	
    	Integer recordNumber = ctrl_Acc_Segmentation_Questions_Admin.getRecordCount('Behavioral', 'FY2021', 'Coro + PV', acc.Id, 'FRANCE' , 'Clinical', UserInfo.getUserId(), 'Open');
    	System.assert(recordNumber == 1);
    	
    	ctrl_Account_Performance_Questions.Formulaire result = ctrl_Acc_Segmentation_Questions_Admin.getUserFormulaire('Behavioral', 'FY2021', 'Coro + PV', acc.Id, 'FRANCE', 'Clinical', UserInfo.getUserId(), 'Open');   	
    	System.assert(result.accountInputs.size() == 1);
    	System.assert(result.questions.size() == 4);
    	
    	ctrl_Account_Performance_Questions.FormulaireAccountInput input = result.accountInputs[0];
    	System.assert(input.prevAccountAnswers.size() == 4);
    	input.changed = true;
    	input.record.Complete__c = true;
    	
    	System.assert(input.accountAnswers.size() == 4);
    	input.accountAnswers[0].changed = true;
    	input.accountAnswers[0].record.Answer__c = '2';
    	
    	input.accountAnswers[1].changed = true;
    	input.accountAnswers[1].record.Answer__c = '1';
    	
    	input.accountAnswers[2].changed = true;
    	input.accountAnswers[2].record.Answer__c = '60';
    	
    	input.accountAnswers[3].changed = true;
    	input.accountAnswers[3].record.Answer__c = 'No';
    	
    	ctrl_Acc_Segmentation_Questions_Admin.saveUserFormulaire(result.accountInputs);
    	
    	result = ctrl_Acc_Segmentation_Questions_Admin.getUserFormulaire('Behavioral', 'FY2021', 'Coro + PV', acc.Id, 'FRANCE', 'Clinical', UserInfo.getUserId(), 'Completed');   	
    	System.assert(result.accountInputs.size() == 1);
    	System.assert(result.questions.size() == 4);
    	
    	List<UMD_Input__c> answers = [Select Id from UMD_Input__c where UMD_Input_Complete__c = :currentInput.Id];
    	System.assert(answers.size() == 4);
    	
    	result.accountInputs[0].accountAnswers[2].changed = true;
    	result.accountInputs[0].accountAnswers[2].record.Answer__c = null;
    	
    	ctrl_Acc_Segmentation_Questions_Admin.saveUserFormulaire(result.accountInputs);
    	
    	answers = [Select Id from UMD_Input__c where UMD_Input_Complete__c = :currentInput.Id];
    	System.assert(answers.size() == 3);
    }
    
    private static testMethod void testScreenControllerStrategicAdmin(){
    	
    	createTestData();
    	
    	Account acc = [Select Id, Name, BillingCountry from Account];
    	UMD_Input_Complete__c currentInput = [Select Id from UMD_Input_Complete__c where UMD_Purpose__r.Fiscal_Year_vs__c = 'FY2021' AND UMD_Purpose__r.Segmentation_Qualification__c = 'Strategical'];
    	
    	Account_Segmentation_Validation__c accSegmentValBUG = new Account_Segmentation_Validation__c();
    	accSegmentValBUG.Account__c = acc.Id;
    	accSegmentValBUG.Business_Unit_Group__c = 'CVG';
    	accSegmentValBUG.Strategical_Segment__c = 'Strategic';
    	accSegmentValBUG.Fiscal_Year__c = 'FY2021';
    	accSegmentValBUG.Segmentation_Level__c = 'Business Unit Group';
    	insert accSegmentValBUG;
    	
    	Test.startTest();
    	
    	Map<String, List<ctrl_Account_Performance_Questions.PicklistValue>> mainResult = ctrl_Acc_Segmentation_Questions_Admin.getMainFilterOptions('Strategical', null);    	
    	System.assert(mainResult.get('fiscalYearOptions').size() == 1);
    	System.assert(mainResult.get('fiscalYear').size() == 1);
    	System.assert(mainResult.get('fiscalYear')[0].value == 'FY2021');
    	System.assert(mainResult.get('scopeOptions').size() == 1);
    	
    	Map<String, List<ctrl_Account_Performance_Questions.PicklistValue>> secondaryResult = ctrl_Acc_Segmentation_Questions_Admin.getSecondaryFilterOptions('Strategical', 'FY2021', 'CVG', null, null);
    	System.assert(secondaryResult.get('segmentOptions').size() == 2);
    	System.assert(secondaryResult.get('countryOptions').size() == 2);
    	    	
    	Integer recordNumber = ctrl_Acc_Segmentation_Questions_Admin.getRecordCount('Strategical', 'FY2021', 'CVG', acc.Id, 'FRANCE' , '-', UserInfo.getUserId(), 'Open');
    	System.assert(recordNumber == 1);
    	
    	ctrl_Account_Performance_Questions.Formulaire result = ctrl_Acc_Segmentation_Questions_Admin.getUserFormulaire('Strategical', 'FY2021', 'CVG', acc.Id, 'FRANCE', '-', UserInfo.getUserId(), 'Open');   	
    	System.assert(result.accountInputs.size() == 1);
    	System.assert(result.questions.size() == 2);
    	
    	ctrl_Account_Performance_Questions.FormulaireAccountInput input = result.accountInputs[0];
    	System.assert(input.prevAccountAnswers.size() == 2);
    	input.changed = true;
    	input.record.Complete__c = true;
    	
    	System.assert(input.accountAnswers.size() == 2);
    	input.accountAnswers[0].changed = true;
    	input.accountAnswers[0].record.Answer__c = '2';
    	
    	input.accountAnswers[1].changed = true;
    	input.accountAnswers[1].record.Answer__c = '1';
    	
    	ctrl_Acc_Segmentation_Questions_Admin.saveUserFormulaire(result.accountInputs);
    	
    	result = ctrl_Acc_Segmentation_Questions_Admin.getUserFormulaire('Strategical', 'FY2021', 'CVG', acc.Id, 'FRANCE', '-', UserInfo.getUserId(), 'Completed');   	
    	System.assert(result.accountInputs.size() == 1);
    	System.assert(result.questions.size() == 2);
    	
    	List<UMD_Input__c> answers = [Select Id from UMD_Input__c where UMD_Input_Complete__c = :currentInput.Id];
    	System.assert(answers.size() == 2);
    	
    	result.accountInputs[0].accountAnswers[1].changed = true;
    	result.accountInputs[0].accountAnswers[1].record.Answer__c = null;
    	
    	ctrl_Acc_Segmentation_Questions_Admin.saveUserFormulaire(result.accountInputs);
    	
    	answers = [Select Id from UMD_Input__c where UMD_Input_Complete__c = :currentInput.Id];
    	System.assert(answers.size() == 1);
    }
    
    private static void createTestData(){
    	
    	UMD_Purpose__c currentPurpose = [Select Id from UMD_Purpose__c where Name = 'Behavioral FY2021'];
    	UMD_Purpose__c lastPurpose = [Select Id, (Select UMD_Question__c from UMD_Purpose_Question__r ORDER BY Sequence__c) from UMD_Purpose__c where Name = 'Behavioral FY2020'];
    	UMD_Purpose__c currentStratPurpose = [Select Id, (Select UMD_Question__c from UMD_Purpose_Question__r ORDER BY Sequence__c) from UMD_Purpose__c where Name = 'Strategical FY2021'];
    	
    	Account acc = new Account();
    	acc.Name = 'Test Account';
    	acc.SAP_Id__c = '121231234';
    	acc.Account_City__c = 'Paris';
    	acc.Account_Country_vs__c = 'FRANCE';
    	insert acc;
    	
    	Customer_Segmentation__c accSegmentSBU = new Customer_Segmentation__c();
    	accSegmentSBU.Account__c = acc.Id;
    	accSegmentSBU.Sub_Business_Unit__c = 'Coro + PV';
    	accSegmentSBU.Behavioral_Segment__c = 'Clinical';
    	accSegmentSBU.Fiscal_Year__c = 'FY2020';
    	    	
    	Customer_Segmentation__c accSegmentBUG = new Customer_Segmentation__c();
    	accSegmentBUG.Account__c = acc.Id;
    	accSegmentBUG.Business_Unit_Group__c = 'CVG';
    	accSegmentBUG.Strategical_Segment__c = 'Not Segmented';
    	accSegmentBUG.Fiscal_Year__c = 'FY2020';
    	
    	insert new List<Customer_Segmentation__c>{accSegmentSBU, accSegmentBUG};
    	
    	UMD_Input_Complete__c lastInput = new UMD_Input_Complete__c();
    	lastInput.Account__c = acc.Id;
    	lastInput.UMD_Purpose__c = lastPurpose.Id;
    	lastInput.Sub_Business_Unit__c = 'Coro + PV';
    	lastInput.Assigned_To__c = UserInfo.getUserId();
    	lastInput.Approver_1__c = [Select Id from User where isActive = true and Profile.Name = 'System Administrator' AND Id != :UserInfo.getUserId() LIMIT 1].Id;
    	lastInput.Complete__c = true;
    	lastInput.Approved__c = true;
    	
    	UMD_Input_Complete__c currentInput = new UMD_Input_Complete__c();
    	currentInput.Account__c = acc.Id;
    	currentInput.UMD_Purpose__c = currentPurpose.Id;
    	currentInput.Sub_Business_Unit__c = 'Coro + PV';
    	currentInput.Assigned_To__c = UserInfo.getUserId();
    	currentInput.Approver_1__c = lastInput.Approver_1__c;
    	
    	UMD_Input_Complete__c currentStratInput = new UMD_Input_Complete__c();
    	currentStratInput.Account__c = acc.Id;
    	currentStratInput.UMD_Purpose__c = currentStratPurpose.Id;
    	currentStratInput.Business_Unit_Group__c = 'CVG';
    	currentStratInput.Assigned_To__c = UserInfo.getUserId();    	    	
    	    	    	    	
    	insert new List<UMD_Input_Complete__c>{lastInput, currentInput, currentStratInput};
    	
    	currentInput = [Select Id, Current_Segmentation__c from UMD_Input_Complete__c where Id = :currentInput.Id];
    	System.assert(currentInput.Current_Segmentation__c == accSegmentSBU.Id);
    	
    	UMD_Input__c lastInputAnswer1 = new UMD_Input__c();
    	lastInputAnswer1.UMD_Input_Complete__c = lastInput.Id;
    	lastInputAnswer1.UMD_Question__c = lastPurpose.UMD_Purpose_Question__r[0].UMD_Question__c;
    	lastInputAnswer1.Answer__c = '1';
    	
    	UMD_Input__c lastInputAnswer2 = new UMD_Input__c();
    	lastInputAnswer2.UMD_Input_Complete__c = lastInput.Id;
    	lastInputAnswer2.UMD_Question__c = lastPurpose.UMD_Purpose_Question__r[1].UMD_Question__c;
    	lastInputAnswer2.Answer__c = '2';
    	
    	UMD_Input__c lastInputAnswer3 = new UMD_Input__c();
    	lastInputAnswer3.UMD_Input_Complete__c = lastInput.Id;
    	lastInputAnswer3.UMD_Question__c = lastPurpose.UMD_Purpose_Question__r[2].UMD_Question__c;
    	lastInputAnswer3.Answer__c = '80';
    	
    	
    	UMD_Input__c currentStratInputAnswer1 = new UMD_Input__c();
    	currentStratInputAnswer1.UMD_Input_Complete__c = currentStratInput.Id;
    	currentStratInputAnswer1.UMD_Question__c = currentStratPurpose.UMD_Purpose_Question__r[0].UMD_Question__c;
    	currentStratInputAnswer1.Answer__c = '1';
    	
    	UMD_Input__c currentStratInputAnswer2 = new UMD_Input__c();
    	currentStratInputAnswer2.UMD_Input_Complete__c = currentStratInput.Id;
    	currentStratInputAnswer2.UMD_Question__c = currentStratPurpose.UMD_Purpose_Question__r[1].UMD_Question__c;
    	currentStratInputAnswer2.Answer__c = '2';
    	
    	insert new List<UMD_Input__c>{lastInputAnswer1, lastInputAnswer2, lastInputAnswer3, currentStratInputAnswer1, currentStratInputAnswer2};
    	
    }
}