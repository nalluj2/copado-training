@isTest
private class Test_ctrl_Sales_Navigator {
    
    private static testmethod void testRedirectInsights(){
    	    	
    	SICA_Endpoints__c endpointOS = new SICA_Endpoints__c(Name = 'OutSystems', URL__c = 'https://outsystems.test.com');
    	insert endpointOS;
    	
    	Account testAcc = new Account();
    	testAcc.Name = 'Test Account';
    	testAcc.SAP_Id__c = '123456789';
    	testAcc.Account_Country_vs__c = 'BELGIUM'; 
    	insert testAcc;    	
    	
    	PageReference contextPage = Page.SICA_Insights;	
		Test.setCurrentPage(contextPage);
		
		Test.StartTest();
		
		ctrl_Sales_Navigator controller = new ctrl_Sales_Navigator();
		
		PageReference result = controller.openSalesNavigator();		
		System.assert(result == null);
		System.assert(controller.noAccessMessage.contains('None of your accounts are currently part of the SI Reboot 21 Capacity Pulse.'));
		
		contextPage.getParameters().put('AccountId', testAcc.id);
		
		result = controller.openSalesNavigator();		
		System.assert(result == null);
		System.assert(controller.noAccessMessage.contains('Your account is currently not part of the SI Reboot 21 Capacity Pulse initiative.'));
		
		testAcc.SICA_Sales_Rep__c = UserInfo.getUserId();
		update testAcc;
		
		result = controller.openSalesNavigator();		
		System.assert(result != null);		
    }
    
    private static testmethod void testRedirectSalesNavigator(){
    	    	    	
    	SICA_Endpoints__c endpointTableauBUD = new SICA_Endpoints__c(Name = 'TableauBUD', URL__c = 'https://tableau.bud.test.com');
    	SICA_Endpoints__c endpointTableauRSM = new SICA_Endpoints__c(Name = 'TableauRSM', URL__c = 'https://tableau.rsm.test.com');
    	SICA_Endpoints__c endpointTableauRS = new SICA_Endpoints__c(Name = 'TableauSR', URL__c = 'https://tableau.sr.test.com');
    	
    	insert new List<SICA_Endpoints__c>{endpointTableauBUD, endpointTableauRSM, endpointTableauRS};
    	
    	Account testAcc = new Account();
    	testAcc.Name = 'Test Account';
    	testAcc.SAP_Id__c = '123456789';
    	testAcc.Account_Country_vs__c = 'BELGIUM'; 
    	insert testAcc;
    	    	
    	PageReference contextPage = Page.Sales_Navigator;
		
		Test.setCurrentPage(contextPage);
		
		Test.StartTest();
		
		ctrl_Sales_Navigator controller = new ctrl_Sales_Navigator();
		
		PageReference result = controller.openSalesNavigator();		
		System.assert(result != null);		
    }
}