global class ba_Contract_Repository_Document_Version implements Schedulable, Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{
    
    private List<Id> versionIds;
	
	// Schedulable
    global void execute(SchedulableContext ctx){
        
        List<Contract_Repository_Document_Version__c> pendingDeleteionDocumentVersions = [Select Id from Contract_Repository_Document_Version__c where Marked_for_Deletion__c = true AND Is_Deleted__c = false];
        
        List<Id> verIds = new List<Id>();
        
        for(Contract_Repository_Document_Version__c version : pendingDeleteionDocumentVersions){
        	
        	verIds.add(version.Id);        	
        }
        
        if(verIds.size() > 0){
        	
        	ba_Contract_Repository_Document_Version batch = new ba_Contract_Repository_Document_Version(verIds); 
        	Database.executeBatch(batch, 1);
        }
    }
	
    public ba_Contract_Repository_Document_Version() {}
    
    public ba_Contract_Repository_Document_Version(List<Id> inputList) {
        
        this.versionIds = inputList;
    }
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		
		return Database.getQueryLocator('Select Id, Document__c from Contract_Repository_Document_Version__c where Id IN :versionIds');
	}
	
	global void execute(Database.BatchableContext BC, List<Contract_Repository_Document_Version__c> versions) {
		
		bl_Contract_Repository_Document_Version.deleteVersion(versions[0]);
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}    
}