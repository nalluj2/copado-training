/*
 *      Created Date : 12 -Mar-2013
 *      Description : This is the class is called by trigger trgUpdateBUCaseRecorTypeField on Case and 
                     it contains the logic to update business hours on case user's timezone.
 *
 *      Author = Kaushal Singh
 *      
 */
public class bl_tr_UpdateBusinessHours{
    
    public static void BusinessHoursLogic(List<case> lstcase){
        
        set<id> setCaseOwnerId=new set<id>();
        for(Case c:lstcase){
            setCaseOwnerId.add(c.OwnerId);
        }
        List<User> lstUser=[select id,TimeZoneSidKey from user where id in:setCaseOwnerId];
                             
        set<string> setTimeZone=new set<string>();
        map<id,string> mapUserIdAndTimeZone=new map<id,string>();
        for(User u:lstUser){
            setTimeZone.add(u.TimeZoneSidKey);
            mapUserIdAndTimeZone.put(u.id,u.TimeZoneSidKey);    
        }
        
        List<BusinessHours> lstBH = bl_CaseAgeCalculation.bHoursById.values();
        
        map<string,id> mapTimeZoneandTimeZoneId=new map<string,id>();
        for(BusinessHours BH:lstBH){
            mapTimeZoneandTimeZoneId.put(BH.TimeZoneSidKey,BH.Id);    
        }
        for(Case c:lstcase){
            if(mapUserIdAndTimeZone.keyset().size()>0 & mapTimeZoneandTimeZoneId.keyset().size()>0){
                c.BusinessHoursId=mapTimeZoneandTimeZoneId.get(mapUserIdAndTimeZone.get(c.OwnerId));
            }
        }
    }
}