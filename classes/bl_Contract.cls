/*
 * Author 		:	Bart Caelen
 * Date			:	20140623
 * Description	:	This class contains Business Logic for the Contract object
 */
public class bl_Contract {
	
	@TestVisible
	public static Boolean assetContractCalculationInProcess = false;
	
	// CR-4545
	// This functions calculates the SUM of the "PO_Amount2__c" field of the related AssetContract__c records for each Contract that is in the provided list ofContract
	// The return value is a List of Contracts with an updated field "PO_amount__c" which contains the calculated sum based on the related AssetContract__c records.
	public static List<Contract> getPOAmountSumFromRelatedAssetContract(List<Contract> lstContract){

		// Put the provided List of Contracts in a map for further processing
		// By default, clear the PO_Amount__c field (set the value 0)
		Map<Id, Contract> mapContract = new Map<Id, Contract>();
		for (Contract oContract : lstContract){
			oContract.PO_amount__c = 0;
			mapContract.put(oContract.Id, oContract);
		}

		// Calulcate the sum of PO_Ammount2__c of the related AssetContract__c records for the provided Contract records
		List<AggregateResult> lstAggResult = [SELECT Contract__c, SUM(PO_Amount2__c) sumPOAmount FROM AssetContract__c WHERE Contract__c =: lstContract GROUP BY Contract__c];

		// Update the map of provided Contract records by updating the field PO_Amount__c of the Contract with the calculated sum of the PO_Amount2__c field for the related AssetContract__c records
		for (AggregateResult oAggResult : lstAggResult){
			Id idContract = Id.valueOf(String.valueOf(oAggResult.get('Contract__c')));
			Contract oContract = mapContract.get(idContract);
			//Convert sum to contract currency
			CurrencyType currType = bl_Currency.getCurrencyType(oContract.currencyIsocode);
            Decimal poAmountCompanyCurrency = clsUtil.isDecimalNull((Decimal)oAggResult.get('sumPOAmount'), 0); 
            Double conversionRate;
            if (currType==null){
                conversionRate=1;
            }else{
                conversionRate=currType.ConversionRate;
            }
            System.debug('CurrencyType '+currType);
            System.debug('poAmountCompanyCurrency '+poAmountCompanyCurrency);
			oContract.PO_amount__c = (poAmountCompanyCurrency*conversionRate).round();
			mapContract.put(oContract.Id, oContract);
		}

		// Return the updated Contract records
		return mapContract.values();
	}

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //  Author       :   Bart Caelen
    //  Date         :   20150403
    //  Description  :   CR-7670
    //      For the provided list of Contract Records, select the Service_LeveL__c values of the related AssetContract__c records.
    //      The selected Service_LeveL__c will be concatenated and saved in the field Contract_Level__c on Contract (separated by a ;)
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    public static List<Contract> getServiceLevelFromRelatedAssetContract(List<Contract> lstContract){

        // Collect all related AssetContract__c records
        List<AssetContract__c> lstAssetContract = [SELECT Contract__c, Service_LeveL__c FROM AssetContract__c WHERE Contract__c =: lstContract AND Service_LeveL__c != '' ORDER BY Contract__c, Service_LeveL__c];

        // Loop the collected AssetContract__c records and create a map that holds the different Service_Level__c values for eacht Contract
        Map<Id, Set<String>> mapContractId_ServiceLevels = new Map<Id, Set<String>>();
        for (AssetContract__c oAssetContract : lstAssetContract){
            Set<String> setServiceLevel = new Set<String>();
            if (mapContractId_ServiceLevels.containsKey(oAssetContract.Contract__c)){
                setServiceLevel = mapContractId_ServiceLevels.get(oAssetContract.Contract__c);
            }
            setServiceLevel.add(oAssetContract.Service_Level__c);
            mapContractId_ServiceLevels.put(oAssetContract.Contract__c, setServiceLevel);
        }

        // Loop through the processing Contract__c records and update the Contract_Level__c field with the collected set of Service_Level__c values
        // If no Service_Level__c values are found, clear the Contract_Level__c field on the Contract
        for (Contract oContract : lstContract){
            if (mapContractId_ServiceLevels.containsKey(oContract.Id)){
                Set<String> setServiceLevel = mapContractId_ServiceLevels.get(oContract.Id);
                String tContractLevel = '';
                for (String tServiceLevel : setServiceLevel){
                    tContractLevel += tServiceLevel + ';';
                }
                tContractLevel = tContractLevel.left(tContractLevel.length() - 1);
                oContract.Contract_Level__c = tContractLevel;
            }else{
                oContract.Contract_Level__c = '';
            }

        }

        // Return the updated Contract records
        return lstContract;
    }

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //  Author       :   Christophe Saenen
    //  Date         :   20160510
    //  Description  :   CR-11045
    //      Copy opportunity assets to new created contract
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //  Author       :   Bart Caelen
    //  Date         :   20180326
    //  Description  :   CR-15405
    //      Copy the field Service_Level__c on Oopportunity_Asset__c to the new created AssetContract records
    //      Rewritten code to be more clear
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  	public static void copyAssets(List<Contract> lstContract) {

  							   	  
 		Set<Id> opportunityIds = new Set<Id>();
 		 		
 		for (Contract oContract : lstContract){
 			
 			if(oContract.Opportunity__c != null && oContract.Clone_From__c == null) opportunityIds.add(oContract.Opportunity__c); 											 	
 		}
 		
 		if(opportunityIds.size() > 0){
 		
	 		List<AssetContract__c> lstAssetContract_Insert = new List<AssetContract__c>();
	 		
	 		Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>([Select Id, (SELECT Id, Asset__c, Opportunity__c, Service_Level__c, DIEN_Code__c FROM Asset_Opportunities__r) from Opportunity where Id IN :opportunityIds]);
	  		
	  		for(Contract oContract : lstContract){
				
				if(oContract.Opportunity__c != null && oContract.Clone_From__c == null){
					
					for(Opportunity_Asset__c oppAsset : opportunityMap.get(oContract.Opportunity__c).Asset_Opportunities__r){
					
						AssetContract__c oAssetContract = new AssetContract__c();
		                oAssetContract.Asset__c = oppAsset.Asset__c;
		                oAssetContract.Contract__c = oContract.Id;
		                oAssetContract.Service_Level__c = oppAsset.Service_Level__c; //'Silver';  //- BC - 20180326 - CR-15405	
						oAssetContract.DIEN_Code__c = oppAsset.DIEN_Code__c;
						lstAssetContract_Insert.add(oAssetContract);		  					
					}
				}		
	
			}
	 
	  		if (lstAssetContract_Insert.size() > 0){
	            insert lstAssetContract_Insert;
	        }
 		}
	}    
	
	public static void calculateContractNotActive(Set<Id> setID_Asset){

		if (setID_Asset.size() == 0) return;

		// Clear the asset fields because it's linked to an inactive contract
		List<Asset> lstAsset_Update = new List<Asset>();

		List<Asset> lstAsset = [SELECT Id, Contract_duration__c, Agreement_level_Picklist__c, Contract_Start_Date__c, Contract_End_Date__c, Service_Level__c, hasActiveContract__c FROM Asset WHERE Id IN :setID_Asset];

		for (Asset oAsset : lstAsset){
				
			if (oAsset.Contract_Start_Date__c != null
				|| oAsset.Contract_End_Date__c != null
				|| oAsset.Agreement_level_Picklist__c != null
				|| oAsset.Contract_duration__c != null
				|| oAsset.Service_Level__c != null
				|| oAsset.hasActiveContract__c != false
			){
				oAsset.Contract_Start_Date__c = null;
				oAsset.Contract_End_Date__c = null;
				oAsset.Agreement_level_Picklist__c = null;
				oAsset.Contract_duration__c = null;
				oAsset.Service_Level__c = null;
				oAsset.hasActiveContract__c = false;
				lstAsset_Update.add(oAsset);
			}

		}

		if (lstAsset_Update.size() > 0) update lstAsset_Update;					

	}

	public static void calculateCurrentContract(Set<Id> setID_Asset){
		
		if (setID_Asset.size() == 0) return;
		
		Set<Id> setID_Asset_Processed = new Set<Id>();

		//create list for updates
		List<Asset> lstAsset_Update = new List<Asset>();
		
		if (setID_Asset.size() > 0){

			List<Asset> lstAsset = 
				[
					SELECT 
						Id, Contract_duration__c, Agreement_level_Picklist__c, Contract_Start_Date__c, Contract_End_Date__c, Service_Level__c, hasActiveContract__c
						, 
							(
								SELECT Id, Agreement_level__c, ContractTerm__c, PO_Amount2__c, Contract__c, Contract_Start_Date__c, Contract_End_Date__c, Contract_Status__c, Service_Level__c
								FROM AssetsContracts__r 
								WHERE Contract_Start_Date__c <= Today AND Contract_Status__c = 'Activated' AND Contract_End_Date__c >= Today
								ORDER BY Contract_End_Date__c DESC LIMIT 1
							) 
					FROM Asset 
					WHERE Id IN :setID_Asset
				];
				
			for (Asset oAsset : lstAsset){
				
				if (oAsset.AssetsContracts__r.size() == 0){
						
					if (oAsset.Contract_Start_Date__c != null
						|| oAsset.Contract_End_Date__c != null
						|| oAsset.Agreement_level_Picklist__c != null
						|| oAsset.Contract_duration__c != null
						|| oAsset.Service_Level__c != null
						|| oAsset.hasActiveContract__c != false
					){
						oAsset.Contract_Start_Date__c = null;
						oAsset.Contract_End_Date__c = null;
						oAsset.Agreement_level_Picklist__c = null;
						oAsset.Contract_duration__c = null;
						oAsset.Service_Level__c = null;
						oAsset.hasActiveContract__c = false;
						lstAsset_Update.add(oAsset);

						setID_Asset_Processed.add(oAsset.Id);
					}
			
				}else {
				
					//write date fields on asset, put asset in list to update
					AssetContract__c oAssetContract = oAsset.AssetsContracts__r[0];				
				
					if (
						oAsset.Contract_Start_Date__c != oAssetContract.Contract_Start_Date__c
						|| oAsset.Contract_End_Date__c != oAssetContract.Contract_End_Date__c
						|| oAsset.Agreement_level_Picklist__c != oAssetContract.Agreement_level__c
						|| oAsset.Contract_duration__c != oAssetContract.ContractTerm__c
						|| oAsset.Service_Level__c != oAssetContract.Service_Level__c
						|| oAsset.hasActiveContract__c != true
					){
						oAsset.Contract_Start_Date__c = oAssetContract.Contract_Start_Date__c;
						oAsset.Contract_End_Date__c = oAssetContract.Contract_End_Date__c;
						oAsset.Agreement_level_Picklist__c = oAssetContract.Agreement_level__c;
						oAsset.Contract_duration__c = oAssetContract.ContractTerm__c;
						oAsset.Service_Level__c = oAssetContract.Service_Level__c;
						oAsset.hasActiveContract__c = true;
						lstAsset_Update.add(oAsset);

						setID_Asset_Processed.add(oAsset.Id);
					}				
				}		
			}

		}

		if (lstAsset_Update.size() > 0) update lstAsset_Update;					

	}

}