/*
 *      Created Date 	: 27-06-2014
 *      Author 			: Bart Caelen
 *      Description 	: TEST Class for the APEX TRIGGER tr_Contact_sBU_Validation
 */
@isTest private class TEST_tr_Contact_sBU_Validation {
	
	
	@isTest
   	static void TEST_tr_Contact_sBU_Validation() {
		
		User testUser = clsTestData_User.createUser(false);
			testUser.Country_vs__c = 'BELGIUM';
			testUser.ProfileId = clsUtil.getUserProfileId('EUR Field Force CVG');
		insert testUser;
		
		System.runAs(testUser){
		
			Boolean bError = false;
	
			//---------------------------------------------------
			// Create Test Data
			//---------------------------------------------------
			clsTestData.ptCompanyCode = 'EUR';
			clsTestData.createCountryData();
			// Activate SBU Validation on DIB_Country__c
			DIB_Country__c oCountry = clsTestData.mapCode_Country.get('BE');
			System.assertEquals(oCountry.Contact_sBU_Validation__c, false);
				oCountry.Contact_sBU_Validation__c = true;
				oCountry.Contact_sBU_Validation_Excluded_JobTitle__c = 'Physician';
			update oCountry;
			System.assertEquals(oCountry.Contact_sBU_Validation__c, true);
		
			clsTestData.createSubBusinessUnitData();
			clsTestData.tCountry_Account = 'BELGIUM';
			//---------------------------------------------------
	
			Id idBusinessUnit_CRHF;
			for (Business_Unit__c oBU : clsTestData.lstBusinessUnit){
				if (oBU.Name == 'CRHF'){
					idBusinessUnit_CRHF = oBU.Id;
				}
				oBU.Contact_Flag__c = 'CRHF__c';
			}
			update clsTestData.lstBusinessUnit;
			List<Sub_Business_Units__c> lstSBU_CRHF = clsTestData.mapBusinessUnit_SubBusinessUnits.get(idBusinessUnit_CRHF);
			for (Sub_Business_Units__c oSBU : lstSBU_CRHF){
				oSBU.Contact_Flag__c = 'Implantables_Diagnostic__c';
			}
			update lstSBU_CRHF;
	
	
			Test.startTest();
	
			//---------------------------------------------------
			// TEST 1
			//---------------------------------------------------
			bError = false;
			clsTestData.oMain_Contact = null;
			try{
				
				clsTestData.createContactData(true);
				
			}catch(Exception oEX){
				clsUtil.debug('Error Test 1 : ' + oEX.getMessage());
				System.assert(oEX.getMessage().contains(Label.Sub_Business_Unit_is_required));
				bError = true;
			}
			System.assertEquals(bError, true);
			//---------------------------------------------------
		}
   	}
	
	@isTest
   	static void TEST_tr_Contact_sBU_Validation2() {
		
		User testUser = clsTestData_User.createUser(false);
			testUser.Country_vs__c = 'BELGIUM';
			testUser.ProfileId = clsUtil.getUserProfileId('EUR Field Force CVG');
		insert testUser;
		
		System.runAs(testUser){
		
			Boolean bError = false;
	
			//---------------------------------------------------
			// Create Test Data
			//---------------------------------------------------
			clsTestData.ptCompanyCode = 'EUR';
			clsTestData.createCountryData();
			// Activate SBU Validation on DIB_Country__c
			DIB_Country__c oCountry = clsTestData.mapCode_Country.get('BE');
			System.assertEquals(oCountry.Contact_sBU_Validation__c, false);
				oCountry.Contact_sBU_Validation__c = true;
				oCountry.Contact_sBU_Validation_Excluded_JobTitle__c = 'Physician';
			update oCountry;
			System.assertEquals(oCountry.Contact_sBU_Validation__c, true);
		
			clsTestData.createSubBusinessUnitData();
			clsTestData.tCountry_Account = 'BELGIUM';
			//---------------------------------------------------
	
			Id idBusinessUnit_CRHF;
			for (Business_Unit__c oBU : clsTestData.lstBusinessUnit){
				if (oBU.Name == 'CRHF'){
					idBusinessUnit_CRHF = oBU.Id;
				}
				oBU.Contact_Flag__c = 'CRHF__c';
			}
			update clsTestData.lstBusinessUnit;
			List<Sub_Business_Units__c> lstSBU_CRHF = clsTestData.mapBusinessUnit_SubBusinessUnits.get(idBusinessUnit_CRHF);
			for (Sub_Business_Units__c oSBU : lstSBU_CRHF){
				oSBU.Contact_Flag__c = 'Implantables_Diagnostic__c';
			}
			update lstSBU_CRHF;
			//---------------------------------------------------
			// TEST 2
			//---------------------------------------------------
			Test.startTest();
	        
	        bError = false;
			clsTestData.oMain_Contact = null;
			clsTestData.createContactData(false);
	
			clsTestData.oMain_Contact.Primary_Job_Title_vs__c = 'Physician';
	
			insert clsTestData.oMain_Contact;
	
			//---------------------------------------------------
			Test.stopTest();
		}
   	}

   	@isTest
   	static void TEST_tr_Contact_sBU_Validation3() {
		
		User testUser = clsTestData_User.createUser(false);
			testUser.Country_vs__c = 'BELGIUM';
			testUser.ProfileId = clsUtil.getUserProfileId('EUR Field Force CVG');
		insert testUser;

		System.runAs(testUser){
		
			Boolean bError = false;
	
			//---------------------------------------------------
			// Create Test Data
			//---------------------------------------------------
			clsTestData.ptCompanyCode = 'EUR';
			clsTestData.createCountryData();
			// Activate SBU Validation on DIB_Country__c
			DIB_Country__c oCountry = clsTestData.mapCode_Country.get('BE');
			System.assertEquals(oCountry.Contact_sBU_Validation__c, false);
				oCountry.Contact_sBU_Validation__c = true;
				oCountry.Contact_sBU_Validation_Excluded_JobTitle__c = 'Physician';
			update oCountry;
			System.assertEquals(oCountry.Contact_sBU_Validation__c, true);
		
			clsTestData.createSubBusinessUnitData();
			clsTestData.tCountry_Account = 'BELGIUM';
			//---------------------------------------------------
	
			Id idBusinessUnit_CRHF;
			for (Business_Unit__c oBU : clsTestData.lstBusinessUnit){
				if (oBU.Name == 'CRHF'){
					idBusinessUnit_CRHF = oBU.Id;
				}
				oBU.Contact_Flag__c = 'CRHF__c';
			}
			update clsTestData.lstBusinessUnit;
			List<Sub_Business_Units__c> lstSBU_CRHF = clsTestData.mapBusinessUnit_SubBusinessUnits.get(idBusinessUnit_CRHF);
			for (Sub_Business_Units__c oSBU : lstSBU_CRHF){
				oSBU.Contact_Flag__c = 'Implantables_Diagnostic__c';
			}
			update lstSBU_CRHF;
			
		
			//---------------------------------------------------
			// TEST 3
			//---------------------------------------------------
			Test.startTest();
	        bError = false;
	
				clsTestData.oMain_Contact = null;
				clsTestData.createContactData(false);
	
				clsTestData.oMain_Contact.Primary_Job_Title_vs__c = 'Head Nurse';
	
			try{
				
				insert clsTestData.oMain_Contact;
				
			}catch(Exception oEX){
				clsUtil.debug('Error Test 3 : ' + oEX.getMessage());
				System.assert(oEX.getMessage().contains(Label.Sub_Business_Unit_is_required));
				bError = true;
			}
			
			System.assertEquals(bError, true);
			//---------------------------------------------------
			Test.stopTest();
		}
   	}

   	@isTest
   	static void TEST_tr_Contact_sBU_Validation4() {
		
		User testUser = clsTestData_User.createUser(false);
			testUser.Country_vs__c = 'BELGIUM';
			testUser.ProfileId = clsUtil.getUserProfileId('EUR Field Force CVG');
		insert testUser;
		
		System.runAs(testUser){
		
			Boolean bError = false;
	
			//---------------------------------------------------
			// Create Test Data
			//---------------------------------------------------
			clsTestData.ptCompanyCode = 'EUR';
			clsTestData.createCountryData();
			// Activate SBU Validation on DIB_Country__c
			DIB_Country__c oCountry = clsTestData.mapCode_Country.get('BE');
			System.assertEquals(oCountry.Contact_sBU_Validation__c, false);
				oCountry.Contact_sBU_Validation__c = true;
				oCountry.Contact_sBU_Validation_Excluded_JobTitle__c = 'Physician';
			update oCountry;
			System.assertEquals(oCountry.Contact_sBU_Validation__c, true);
		
			clsTestData.createSubBusinessUnitData();
			clsTestData.tCountry_Account = 'BELGIUM';
			//---------------------------------------------------
	
			Id idBusinessUnit_CRHF;
			for (Business_Unit__c oBU : clsTestData.lstBusinessUnit){
				if (oBU.Name == 'CRHF'){
					idBusinessUnit_CRHF = oBU.Id;
				}
				oBU.Contact_Flag__c = 'CRHF__c';
			}
			update clsTestData.lstBusinessUnit;
			List<Sub_Business_Units__c> lstSBU_CRHF = clsTestData.mapBusinessUnit_SubBusinessUnits.get(idBusinessUnit_CRHF);
			for (Sub_Business_Units__c oSBU : lstSBU_CRHF){
				oSBU.Contact_Flag__c = 'Implantables_Diagnostic__c';
			}
			update lstSBU_CRHF;
	
			//---------------------------------------------------
			// TEST 4
			//---------------------------------------------------
			Test.startTest();
	        
			clsTestData.oMain_Contact = null;
			clsTestData.createContactData(false);
			clsTestData.oMain_Contact.Implantables_Diagnostic__c = true;
			clsTestData.oMain_Contact.Primary_Job_Title_vs__c = 'Head Nurse';
			
			insert clsTestData.oMain_Contact;
	
			//---------------------------------------------------
			Test.stopTest();	
			
			Contact cnt = [Select CRHF__c from Contact where Id = :clsTestData.oMain_Contact.Id];
			System.assert(cnt.CRHF__C == true);
		}
   	}   	
}