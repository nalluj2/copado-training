public with sharing class ctrl_Related_List_Simple {
 	
	public String inputFields {get; set;}
	public String inputObjectName {get; set;}
	public String inputQueryFilter {get; set;}
	
	public String searchString {get; set;}
	
	public Map<String, String> fieldLabels{
		
		get{
			if(fieldLabels == null){
				
				fieldLabels = new Map<String, String>();
				
				Schema.DescribeSobjectResult descRes = Schema.describeSObjects(new List<String>{inputObjectName})[0];
				Map<String, Schema.SObjectField> fieldTokens = descRes.fields.getMap();
				
				for(String field : fieldList){
					
					Schema.DescribeFieldResult fieldDescribe = fieldTokens.get(field).getDescribe();
					
					String label;
					
					if(fieldDescribe.getType().name() == 'Reference'){
                
		                if(field.endsWith('Id')){		                    
		                    String tempLabel = fieldDescribe.getLabel();
		                    label = tempLabel.substring(0, tempLabel.length()-3);
		                }else{		                    
		                    label = fieldDescribe.getLabel();
		                }
		                
		            }else{		                
		                label = fieldDescribe.getLabel();
		            }
		            
		            fieldLabels.put(field, label);
				}				
			}
						
			return fieldLabels;
		}
		private set;		
	}
	
	public List<String> fieldList{
		
		get{
			if(fieldList == null) fieldList = inputFields.split(',');
						
			return fieldList;
		}
		set;		
	}
	
	public Map<String, String> fieldSorts{
		
		get{
			if(fieldSorts == null){
				
				fieldSorts = new Map<String, String>();
				
				Schema.DescribeSobjectResult descRes = Schema.describeSObjects(new List<String>{inputObjectName})[0];
				Map<String, Schema.SObjectField> fieldTokens = descRes.fields.getMap();
				
				for(String field : fieldList){
					
					Schema.DescribeFieldResult fieldDescribe = fieldTokens.get(field).getDescribe();
					
					String fieldSort;
					
					if(fieldDescribe.getType().name() == 'Reference'){
                
		                if(field.endsWith('Id')){		                    
		                    fieldSort = field.substring(0, field.length() -2) + '.Name';
		                }else{		                    
		                    fieldSort = field.replace('__c', '__r.Name');
		                }
		                
		            }else{		                
		                fieldSort = field;
		            }
		            
		            fieldSorts.put(field, fieldSort);
				}				
			}
						
			return fieldSorts;
		}	
		private set;	
	}
	
	public List<String> searchableFields{
		
		get{
			if(searchableFields == null){
				
				searchableFields = new List<String>();
				
				Schema.DescribeSobjectResult descRes = Schema.describeSObjects(new List<String>{inputObjectName})[0];
				Map<String, Schema.SObjectField> fieldTokens = descRes.fields.getMap();
				
				for(String field : fieldList){
					
					Schema.DescribeFieldResult fieldDescribe = fieldTokens.get(field).getDescribe();
					
					String searchField;
					String fieldType = fieldDescribe.getType().name();
					
					if(fieldType == 'Reference'){
                
		                if(field.endsWith('Id')){		                    
		                    searchField = field.substring(0, field.length() -2) + '.Name';
		                }else{		                    
		                    searchField = field.replace('__c', '__r.Name');
		                }
		                
		            }else if(fieldType == 'Combobox' || fieldType == 'Email' || fieldType == 'MultiPicklist' || fieldType == 'Phone' || fieldType == 'Picklist' || fieldType == 'String' || fieldType == 'TextArea' || fieldType == 'URL'){		                
		                searchField = field;
		            }
		            
		            if(searchField != null) searchableFields.add(searchField);
				}				
			}
						
			return searchableFields;
		}	
		private set;	
	}
	
	private ApexPages.StandardSetController con {
		
		get {
            if(con == null) {
            	
                con = new ApexPages.StandardSetController(Database.getQueryLocator(buildQuery()));                
                con.setPageSize(10);
            }
            
            return con;
        }
        set;
	}
	
	private String buildQuery(){
		
		if(sortColumn == null) sortColumn = fieldList[0];
		if(sortDir == null) sortDir = 'ASC';
		
		String querySortField = fieldSorts.get(sortColumn);
		
		String queryFilter = inputQueryFilter;
		
		if(searchString != null && searchString != '' && searchableFields.size() > 0){
			
			List<String> searches = new List<String>();
			
			for(String searchableField : searchableFields){
				
				searches.add(searchableField + ' LIKE \'%' + searchString + '%\'');				
			}
			
			if (queryFilter != null && queryFilter != '') queryFilter += ' AND ';
			
			queryFilter += ' (' + String.join(searches, ' OR ') + ') ';
		}
		
		String query = 'SELECT ' + inputFields + ' FROM ' + inputObjectName + ' ' + queryFilter + ' ORDER BY ' + querySortField + ' ' + sortDir + ' NULLS LAST';
		System.debug('Query ===> ' + query);
		
		return query;
	}
		
	public List<SObject> getRecords(){
		
		return con.getRecords();
	}   
	
	public Boolean hasNext {
        get {
            return con.getHasNext();
        }
        set;
    }
    
    public Boolean hasPrevious {
        get {
            return con.getHasPrevious();
        }
        set;
    }
    
    public Integer pageNumber {
        get {
            return con.getPageNumber();
        }
        set{
        	con.setPageNumber(value);
        }
    }
        
    public Integer totalPages {
        get {
            
            if(con.getResultSize() == 0) return 1;
            
            Decimal recNumber =  con.getResultSize();
            Decimal recPage = con.getPageSize();
            
            Decimal total = recNumber.divide(recPage, 2);
            
            return Integer.valueOf(total.round(System.RoundingMode.UP));
        }
        set;
    }
    
    public Integer pageSize {
        get {
            return con.getPageSize();
        }
        set{
        	con.setPageSize(value);
        }
    }
        
    public Integer resultSize {
        get {
            return con.getResultSize();
        }
        set;
    }
        
    public void first() {
        con.first();
    }
     
	public void last() {
    	con.last();
    }
     
    public void previous() {
        con.previous();
    }
   
    public void next() {
        con.next();
    }
    
    public String sortColumn {get; set;}
    public String sortDir {get; set;}
    public String selectedColumn {get; set;} 
    
    public void sortData(){
    	
    	if(selectedColumn == sortColumn){
            
            if(sortDir == 'ASC') sortDir = 'DESC';
            else sortDir = 'ASC';
            
        }else{
            sortDir = 'ASC';
        }
        
        sortColumn = selectedColumn;    	
    	
    	Integer prevPSize = con.getPageSize();
    	
    	con = new ApexPages.StandardSetController(Database.getQueryLocator(buildQuery()));                
        con.setPageSize(prevPSize);
    }
    
    public void doSearch(){
    	
    	Integer prevPSize = con.getPageSize();
    	
    	con = new ApexPages.StandardSetController(Database.getQueryLocator(buildQuery()));                
        con.setPageSize(prevPSize);
    }
}