/**
 * @description       : 
 * @author            : tom.h.ansley@medtronic.com
 * @last modified on  : 10-30-2020
 * @last modified by  : tom.h.ansley@medtronic.com
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   10-28-2020   tom.h.ansley@medtronic.com   Initial Version
**/
@isTest
public with sharing class Test_ctrl_SMAX_STO_Status {

    static testMethod void testSTOStatus() 
    {
    	
    	SVMXC__Parts_Request__c order = new SVMXC__Parts_Request__c();    	
        order.SAP_STO_Number__c = '99999999';
        insert order;
        
        SVMXC__Parts_Request_Line__c orderLine = new SVMXC__Parts_Request_Line__c();
    	orderLine.SVMXC__Parts_Request__c = order.Id;
    	orderLine.SVMXC__Product__c = [Select Id from Product2 LIMIT 1].Id;
    	orderLine.Requested_Qty__c = 1;
    	orderLine.Unit_of_Measure__c = 'EA';
    	insert orderLine;

        ctrl_SMAX_STO_Status controller = new ctrl_SMAX_STO_Status(new ApexPages.StandardController(order));
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Test_STO_Service_Mock(true, null, 'update', orderLine.Id));
        
        controller.getStatus();
    }


    //----------------------------------------------------------------------------------------
    // CREATE TEST DATA
    //----------------------------------------------------------------------------------------
    @testSetup static void createTestData() {

        // Create Custom Setting Data
        clsTestData.createCustomSettingData(true);

        clsTestData.createProductData();        
    }
    //----------------------------------------------------------------------------------------
   
}