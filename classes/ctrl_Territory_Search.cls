public with sharing class ctrl_Territory_Search {
	
	private Set<Id> userTerritories;
	public String searchCriteria {get; set;}
	public List<Territory2> searchResults {get; set;}
	
	public ctrl_Territory_Search(){
		
		User currentUser = [Select Id, Territory_UID__c from User where Id = :UserInfo.getUserId()];
		
		if(currentUser.Territory_UID__c == null){
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.Error, 'Incorrect User setup: No Territory UID found. Please contact EU Help for Support'));
			return;
		}
		
		List<Territory2> userTerritory = [Select Id from Territory2 where Territory_UID2__c =:currentUser.Territory_UID__c];
		
		if(userTerritory.size() == 0){
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.Error, 'Incorrect User setup: No Territory found for UID \''+currentUser.Territory_UID__c+'\'. Please contact EU Help for Support'));
			return;
		}
				
		userTerritories = bl_Territory_Hierarchy.getTerritoryDescendants(userTerritory[0].Id);	
		userTerritories.add(userTerritory[0].Id);
	} 
	
	public void searchTerritory(){
		
		if(searchCriteria.length()<2){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Search term must be at least two characters'));
			return;
		}
		
		String searchInput = searchCriteria;
		
		if(searchInput.startsWith('%')==false) searchInput='%'+searchInput;
		if(searchInput.endsWith('%')==false) searchInput+='%';		
				
		searchResults = [Select Id, Name, Territory2Type.MasterLabel, External_Key__c from Territory2 where Id IN:userTerritories AND (Name LIKE :searchInput OR External_Key__c LIKE :searchInput) ORDER BY Name ASC];				
	}
		
	public void clearResults(){
		searchCriteria = '';				
		searchResults = null;		
	}
}