public with sharing class ctrl_Service_History_Report {
        
    public List<SelectOption> reports {get; set;}
    public String selectedReport {get; set;}
    public List<PromptParameter> promptParams {get; set;}
    
    private Account acc {get; set;}
    
    public ctrl_Service_History_Report(ApexPages.StandardController sc){
    	    	    	
    	acc = (Account) sc.getRecord();
    	
    	reports = new List<SelectOption>();
    	
    	if(acc.Id == null){
    		
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'No context Account found'));
    		return;
    	}
    	
    	Set<String> userSBUs = new Set<String>();
    	    	
    	for(User_Business_Unit__c userSBU : [Select Id, Sub_Business_Unit_name__c, Business_Unit_Text__c, Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__r.Name from User_Business_Unit__c where User__c = :UserInfo.getUserId()]){
    		
    		userSBUs.add(userSBU.Sub_Business_Unit_name__c);
    		userSBUs.add(userSBU.Business_Unit_Text__c);
    		userSBUs.add(userSBU.Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__r.Name);
    	}
    	
    	for(BO_Report_Definition__c reportDefinition : [Select Id, Name from BO_Report_Definition__c where Report_Family__c = 'Service History Report' ORDER BY Name]){
    		    		    		
    		if(userSBUs.contains(reportDefinition.Name)) reports.add(new SelectOption(reportDefinition.Id, reportDefinition.Name));    		    		
    	}  
    	
    	if(reports.size() == 0) ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'There is no report available for your business unit configuration'));
    	else{    	
    		selectedReport = reports[0].getValue();
    		reportSelected();
    	}
    }
    
    public PageReference autoSelectSingleOption(){
    	
    	if(reports.size() == 1 && promptParams.isEmpty()) return toReport();
    	    	
    	return null;
    }
    
    public void reportSelected(){
    	
    	promptParams = new List<PromptParameter>(); 
    	
    	for(BO_Report_Param_Definition__c reportParam : [Select Name, Label__c, Optional__c  from BO_Report_Param_Definition__c 
    																					where BO_Report_Definition__c = :selectedReport ORDER BY Sequence__c ASC]){
    		
    		PromptParameter promptParam = new PromptParameter();
    		promptParam.label = reportParam.Label__c;
    		promptParam.name = reportParam.Name;
    		promptParam.optional = reportParam.Optional__c;
    		promptParams.add(promptParam);
    	}    	
    	   	  	
    }
    
    public PageReference toReport(){
    	
    	LW_Configuration__c launchWorksSettings = LW_Configuration__c.getInstance();
    	BO_Report_Definition__c setting = [Select Id, Report_Id__c, Fix_Parameters__c from BO_Report_Definition__c where Id = :selectedReport];
    	
    	String url = launchWorksSettings.URL__c;
    	if(setting.Fix_Parameters__c != null && setting.Fix_Parameters__c != '') url += '&' + setting.Fix_Parameters__c;
    	
    	PageReference pr = new PageReference(url);
    	
    	Map<String, String> params = pr.getParameters();    	
    	params.put('iDocID', setting.Report_Id__c);
    	params.put('lsSpsCurrency' , UserInfo.getDefaultCurrency());
    	params.put('lsSpsAccountID' , acc.Id);
    	
    	for(PromptParameter param : promptParams){
    		
    		if(param.value != null && param.value != ''){
    			
    			params.put(param.name , param.value);
    			
    		}else{
    			
    			if(param.optional == false){
    				
    				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Prompt Parameter \'' + param.label + '\' is mandatory'));
    				return null;
    			}
    		}
    	}
    	
    	return pr;
    }
    
    public class PromptParameter{
    	
    	public String label {get; set;}
    	public String name {get; set;}
    	public String value {get; set;}
    	public Boolean optional {get; set;}    	    	
    }
}