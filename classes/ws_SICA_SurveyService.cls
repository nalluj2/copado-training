global class ws_SICA_SurveyService {
	
	webservice static UpsertResult upsertSurvey(Survey surveyData){
		
		UpsertResult result = new UpsertResult();
		
		System.SavePoint sp = Database.setSavepoint();
		
		try{
		
			SICA_Survey__c survey = new SICA_Survey__c();			
			survey.Account__c = surveyData.AccountId;
			survey.Survey_Number__c = surveyData.SurveyId;	
			survey.Survey_Name__c = surveyData.surveyName;
			survey.Status__c = surveyData.status;
			survey.Start_Date__c = surveyData.startDate;
			survey.Completion_Date__c = surveyData.completionDate;
			survey.Business_Unit_Group__c = surveyData.businessUnitGroup;
			survey.Business_Unit__c = surveyData.businessUnit;
			survey.Sub_Business_Unit__c = surveyData.subBusinessUnit;
					
			upsert survey Survey_Number__c;
			
			String surveyContent = surveyData.SurveyContent;
			
			List<String> headers = new List<String>();
			
			Map<String, Object> surveyMap = (Map<String, Object>) JSON.deserializeUntyped(surveyContent);
			
			List<SICA_Survey_Answer__c> answers = new List<SICA_Survey_Answer__c>();
						
			parseObject(surveyMap, survey,  new List<String>(), null, answers, false);
			
			upsert answers Unique_Key__c;
			
			delete [Select Id from SICA_Survey_Answer__c where Survey__c = :survey.Id AND Id NOT IN :answers];
									
			result.success = true;
			
		}catch(Exception e){
			
			Database.rollback(sp);
			
			result.success = false;
			result.errorMessage = e.getMessage() + ' | ' + e.getStacktraceString();
		}
		
		return result;
	}
	
	private static void parseObject(Map<String, Object> objectMap, SICA_Survey__c survey, List<String> headers, String procedure, List<SICA_Survey_Answer__c> answers, Boolean isProcedure){
		
		
		for(String key : objectMap.keySet()){
				
			Object field = objectMap.get(key);
			
			if(field instanceof Map<String, Object>){
				
				List<String> childHeaders = new List<String>(headers);
				
				if(key.equalsIgnoreCase('Procedures')){					
					
					childHeaders.add(key);
					
					parseObject( (Map<String, Object>) field, survey, childHeaders, null, answers, true);
					
				}else{
					
					if(isProcedure){
						
						parseObject((Map<String, Object>) field, survey, childHeaders, key , answers, false);
						
					}else{
											
						childHeaders.add(key);
					
						parseObject((Map<String, Object>) field, survey, childHeaders, null, answers, false);
					}					
				}				
				
			}else{
				
				SICA_Survey_Answer__c answer = new SICA_Survey_Answer__c();
				answer.Survey__c = survey.Id;				
				answer.Question__c = key;
				answer.Answer__c = String.valueOf(field);
				answer.Answer_Type__c = getType(field);
				answer.Procedure__c = procedure;
				
				for(Integer i = 1; i <= headers.size(); i++){
					
					answer.put('Header_' + i +'__c', headers[i - 1]);
				}
				
				answer.Unique_Key__c = getAnswerUniqueKey(answer);
				
				answers.add(answer);
				System.debug(answer);		
			}	
		}		
	}		
	
	
	private static String getType(Object o) {
         
	    if(o instanceof Boolean)            return 'Boolean';	    
	    if(o instanceof String)             return 'String';	    
	    if(o instanceof Date)               return 'Date';
	    if(o instanceof Datetime)           return 'Datetime';
	    if(o instanceof Time)               return 'Time';	    
	    if(o instanceof Integer)            return 'Integer';
	    if(o instanceof Long)               return 'Long';
	    if(o instanceof Decimal)            return 'Decimal';  // we can't distinguish between decimal and double	    
	    
	    return '';                    // actually we can't detect maps and sets and maps
  	}
  	
  	private static String getAnswerUniqueKey(SICA_Survey_Answer__c answer){
  		
  		String key = answer.Survey__c;
  		
  		for(Integer i = 1; i <= 5; i++){
  		 	
  		 	if(answer.get('Header_' + i + '__c') != null){
  		 		
  		 		key += ':' + String.valueOf(answer.get('Header_' + i + '__c'));
  		 	}
  		} 
  		
  		if(answer.Procedure__c != null) key += ':' + answer.Procedure__c;
  		
  		key += ':' + answer.Question__c;
  		
  		return key;
  	}
	
	webservice static Survey getSurvey(String surveyId){
		
		Survey result = new Survey();
			
		try{
			
			List<SICA_Survey__c> surveys = [Select Id, Account__c, Survey_Number__c, Survey_Name__c, Status__c, Start_Date__c, Completion_Date__c, Business_Unit_Group__c, Business_Unit__c, Sub_Business_Unit__c, 			
				(Select Question__c, Procedure__c, Answer__c, Answer_Type__c, Unique_Key__c from Survey_Answers__r)			
				From SICA_Survey__c where Survey_Number__c = :surveyId];
			
			if(surveys.size() == 0) return result;
			
			System.debug(surveys[0]);
			System.debug(surveys[0].Survey_Answers__r);
			
			SICA_Survey__c survey = surveys[0];
			result.surveyId = surveyId;
			result.accountId = survey.Account__c;
			result.surveyName = survey.Survey_Name__c;
	        result.status = survey.Status__c;
	        result.startDate = survey.Start_Date__c;
	        result.completionDate = survey.Completion_Date__c;
	        result.businessUnitGroup = survey.Business_Unit_Group__c;
	        result.businessUnit = survey.Business_Unit__c;
	        result.subBusinessUnit = survey.Sub_Business_Unit__c;    
					
			Map<String, Map<String, Object>> nodesMap = new Map<String, Map<String, Object>>();
			nodesMap.put(survey.Id, new Map<String, Object>());
			
			for(SICA_Survey_Answer__c answer : survey.Survey_Answers__r){
				
				List<String> nodes = answer.Unique_Key__c.split(':');
				
				String parentKey = nodes[0];
				
				for(Integer i = 1; i < nodes.size() - 1; i++){
					
					String node = nodes[i];
					
					String nodeKey = parentKey + ':' + node;
					
					Map<String, Object> nodeMap = nodesMap.get(nodeKey);
					if(nodeMap == null){
						
						nodeMap = new Map<String, Object>();
						nodesMap.put(nodeKey, nodeMap);
						
						Map<String, Object> parentNodeMap = nodesMap.get(parentKey);
						parentNodeMap.put(node, nodeMap);
					}
					
					parentKey = nodeKey;
				}
				
				Map<String, Object> parentNode = nodesMap.get(parentKey);
				parentNode.put(answer.Question__c, getAnswerValue(answer.Answer__c, answer.Answer_Type__c));
			}
			
			JSONGenerator gen = JSON.createGenerator(true);
			gen.writeObject(nodesMap.get(survey.Id));
			
			result.surveyContent = gen.getAsString();
			
			
		}catch(Exception e){
			
			result = new Survey();
			result.surveyContent = '{ "errorMessage" : "' + e.getMessage() + '"}';
		}
		
		return result;
	}
	
	private static Object getAnswerValue(String answer, String answerType){
		
		if(answer == null) return null;
		
		if(answerType == 'Boolean') return Boolean.valueOf(answer);
		if(answerType == 'String') return String.valueOf(answer);
		if(answerType == 'Date') return Date.valueOf(answer);
		if(answerType == 'Datetime') return Datetime.valueOf(answer);
		//if(answerType == 'Time') return Time.valueOf(answer);		
		if(answerType == 'Integer') return Integer.valueOf(answer);
		if(answerType == 'Long') return Long.valueOf(answer);
		if(answerType == 'Decimal') return Decimal.valueOf(answer);
		
		return null;
	}
	
    global class Survey{
        
        webservice String accountId;
        webservice String surveyId;
        webservice String surveyName;
        webservice String status;
        webservice DateTime startDate;
        webservice DateTime completionDate;
        webservice String businessUnitGroup;
        webservice String businessUnit;
        webservice String subBusinessUnit;        
        
        webservice String surveyContent;  
    }
    
    global class UpsertResult{
        
        webservice Boolean success;
        webservice String errorMessage;          
    }
}