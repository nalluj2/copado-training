/*	Integration Service related class
	
	This REST service is used by the target Org to request for the notified record data and to send back an acknowledge once the notification
	has been successfully processed in the target Org.
*/
@RestResource(urlMapping='/SynchronizationServiceSource/*')
global with sharing class ws_SynchronizationService_Source {

	//This method returns all the data related to the supported object record. The method only processes one record per invokation
	@HttpGet
	global static SynchronizationServiceResponse doGet() {
				
		String recordId = RestContext.request.params.get('recordId');
		String recordType = RestContext.request.params.get('recordObjectType');
							
		SynchronizationServiceResponse response = new SynchronizationServiceResponse();
		
		//The information of the record is created in the specific implementation class for the Object of the record
		//and returned as a serialized JSON (string)				
		response.recordInformation = bl_SynchronizationService_Source.generateRecordPayload(recordId, recordType);
		
		return response;
	}
	
	//This method processes the acknowledge signals from the target Org and updates the related notifications to set them as 'Acknowledged' and
	//therefore completed	
	@HttpPost
	global static SynchronizationServiceResponse doPost(List<Sync_Notification__c> notifications) {
		
		SynchronizationServiceResponse response = new SynchronizationServiceResponse();
		response.notifications = new List<Sync_Notification__c>();
		
		DateTime now = DateTime.now();
		
		List<Sync_Notification__c> outboundNotifications = new List<Sync_Notification__c>();
		
		for(Sync_Notification__c notification : notifications){
			
			Sync_Notification__c outboundNotification = new Sync_Notification__c();			
			outboundNotification.Status__c = 'Acknowledged';
			outboundNotification.Notification_Id__c = notification.Notification_Id__c;
			outboundNotification.Acknowledge_Time__c = now;
			
			outboundNotifications.add(outboundNotification);
		}
		
		//We use the Notification external Id field (Notification_Id__c) to update it. Notice that the flag AllOrNone is set to false, so individual
		//failures are allowed.
		List<Database.UpsertResult> upsertResult = Database.upsert(outboundNotifications, Sync_Notification__c.Fields.Notification_Id__c, false);
		
		//The method returns the list of Notifications that has been successfully updated	
		for(Integer i = 0; i < outboundNotifications.size(); i++){
			
			Database.UpsertResult result = upsertResult[i];
			
			if(result.isSuccess()) response.notifications.add(notifications[i]);
		}
		
		return response;
	}
	
	//Model class for the responses. To be used by the JSON serialize/deserialize methods	
	global class SynchronizationServiceResponse{
		global List<Sync_Notification__c> notifications {get;set;}
		global String recordInformation {get; set;}
	}
}