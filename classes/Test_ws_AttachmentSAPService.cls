@isTest
private class Test_ws_AttachmentSAPService {
    
    @TestSetup
    private static void createCustomSettings(){
    	
    	Attachment_Settings__c attachmentSettings = new Attachment_Settings__c();
        attachmentSettings.Name = 'Service Order Attachment';
        attachmentSettings.API_Object__c = 'SVMXC__Service_Order__c';
        attachmentSettings.File_Extensions_Allowed__c = 'pdf,docx';
        attachmentSettings.Filename_Max_Length__c = 40;
        attachmentSettings.Max_File_Size__c	 = 10;            
        insert attachmentSettings;    	
    }
    
    private static testmethod void testGetAttachmentDetails(){
    	
    	Case notification = new Case();
    	notification.AccountId = [Select Id from Account].Id;
    	notification.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Field Service').getRecordTypeId();
    	notification.Description = 'Test SAP Notification';
    	notification.Subject = 'Test SAP Notification';
    	notification.Status = 'New';    	
    	notification.SVMX_SAP_Notification_Number__c = '66666';
    	insert notification;
    	
    	SVMXC__Service_Order__c workorder = new SVMXC__Service_Order__c();
    	workorder.SVMXC__Company__c = notification.AccountId;
    	workorder.SVMXC__Case__c = notification.Id;
    	workorder.SVMXC__Component__c = [Select Id from SVMXC__Installed_Product__c].Id;
    	workorder.SVMXC__Country__c = 'Netherlands, The';
    	workorder.SVMX_Order_Category__c = 'ZRS2';
    	workorder.SVMXC__Order_Status__c = 'Scheduled';    	
    	workorder.SVMX_SAP_Notification_Number__c = '66666';    	
    	workorder.SVMX_SAP_Service_Order_No__c = '88888';
    	workorder.SVMXC__Scheduled_Date_Time__c = Datetime.now().addDays(3);    	
    	insert workorder;
    	
    	Attachment notAtt = new Attachment();
    	notAtt.name = 'test case attachment.pdf';
    	notAtt.description = 'test case attachment description';
    	notAtt.body = Blob.valueOf('test body');
    	notAtt.parentId = notification.Id;
    	
    	Attachment woAtt = new Attachment();
    	woAtt.name = 'test workorder attachment.pdf';
    	woAtt.description = 'test workorder attachment description';
    	woAtt.body = Blob.valueOf('test body');
    	woAtt.parentId = workorder.Id;
    	
    	insert new List<Attachment>{notAtt, woAtt};
    	
    	Test.startTest();
        
        User sapInterface = [Select Id from User where alias = 'sap'];
        
        System.runAs(sapInterface){
        	
        	ws_AttachmentSAPService.SAPAttachment response = ws_AttachmentSAPService.getAttachmentDetails(notAtt.Id);
        	
        	System.assert(response.DISTRIBUTION_STATUS == 'TRUE');
        	System.assert(response.PARENT_ID == '66666');        	
        	
        	response = ws_AttachmentSAPService.getAttachmentDetails(woAtt.Id);
        	
        	System.assert(response.DISTRIBUTION_STATUS == 'TRUE');
        	System.assert(response.PARENT_ID == '66666');
        	
        	List<String> notificationIds = ws_AttachmentSAPService.getAttachmentDetailsForNotification('66666');
        	
        	System.assert(notificationIds.size() == 1);
        	System.assert(notificationIds[0] == notification.Id);        	
        }
    }
    
    private static testmethod void testCreateSAPAttachmentAcknowledgement(){
    	
    	Case notification = new Case();
    	notification.AccountId = [Select Id from Account].Id;
    	notification.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Field Service').getRecordTypeId();
    	notification.Description = 'Test SAP Notification';
    	notification.Subject = 'Test SAP Notification';
    	notification.Status = 'New';    	
    	notification.SVMX_SAP_Notification_Number__c = '66666';
    	insert notification;
    	
    	Attachment notAtt = new Attachment();
    	notAtt.name = 'test case attachment.pdf';
    	notAtt.description = 'test case attachment description';
    	notAtt.body = Blob.valueOf('test body');
    	notAtt.parentId = notification.Id;
    	insert notAtt;
    	
    	Test.startTest();
    	
    	ws_AttachmentSAPService.SAPAttachmentAck attAck = new ws_AttachmentSAPService.SAPAttachmentAck();
    	attAck.SAP_ID = 'ATT_SAP_ID';
    	attAck.SFDC_IDS = notAtt.Id;
    	attAck.DISTRIBUTION_STATUS = 'TRUE';
    	
    	User sapInterface = [Select Id from User where alias = 'sap'];
        
        System.runAs(sapInterface){
        	
        	ws_AttachmentSAPService.createSAPAttachmentAcknowledgement(attAck);
        	
        	List<Sync_Record_Id_Mapping__c> mappingRecord = [Select Name, External_Id__c, Object_Type__c from Sync_Record_Id_Mapping__c];
        	
        	System.assert(mappingRecord.size() == 1);
        	System.assert(mappingRecord[0].Name == notAtt.Id);
        	System.assert(mappingRecord[0].External_Id__c == 'ATT_SAP_ID');
        }
    }
    
    private static testmethod void testCreateSFDCAttachmentAcknowledgement(){
    	    	    	
    	User sapInterface = [Select Id from User where alias = 'sap'];
        
        System.runAs(sapInterface){
        	
        	Case notification = new Case();
    		notification.AccountId = [Select Id from Account].Id;
	    	notification.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Field Service').getRecordTypeId();
	    	notification.Description = 'Test SAP Notification';
	    	notification.Subject = 'Test SAP Notification';
	    	notification.Status = 'New';    	
	    	notification.SVMX_SAP_Notification_Number__c = '66666';
	    	insert notification;
	    	
	    	Attachment prevAtt = new Attachment();
	    	prevAtt.name = 'test case attachment.pdf';
	    	prevAtt.description = 'test case attachment description';
	    	prevAtt.body = Blob.valueOf('test body');
	    	prevAtt.parentId = notification.Id;
	    	    	
	    	Attachment notAtt = new Attachment();
	    	notAtt.name = 'test case attachment.pdf';
	    	notAtt.description = 'test case attachment description';
	    	notAtt.body = Blob.valueOf('test body');
	    	notAtt.parentId = notification.Id;
	    	
	    	insert new List<Attachment>{prevAtt, notAtt};
	    	
	    	Sync_Record_Id_Mapping__c prevRecordMapping = new Sync_Record_Id_Mapping__c(); 
	    	prevRecordMapping.Name = prevAtt.Id;
	    	prevRecordMapping.External_Id__c = 'ATT_SAP_ID';
	    	prevRecordMapping.Object_Type__c = 'Attachment';
	    	insert prevRecordMapping;
	    	
	    	Test.startTest();
	    	
	    	ws_AttachmentSAPService.SFDCAttachmentAck attAck = new ws_AttachmentSAPService.SFDCAttachmentAck();
	    	attAck.SAP_ID = 'ATT_SAP_ID';
	    	attAck.SFDC_IDS = new List<String>{notAtt.Id};
	    	attAck.DISTRIBUTION_STATUS = 'TRUE';
        	
        	ws_AttachmentSAPService.createSFDCAttachmentAcknowledgement(attAck);
        	
        	List<Sync_Record_Id_Mapping__c> mappingRecord = [Select Name, External_Id__c, Object_Type__c from Sync_Record_Id_Mapping__c];
        	
        	System.assert(mappingRecord.size() == 1);
        	System.assert(mappingRecord[0].Name == notAtt.Id);
        	System.assert(mappingRecord[0].External_Id__c == 'ATT_SAP_ID');
        }
    }
    
    private static testmethod void testCreateSAPAttachmentAcknowledgementError(){
    	    	
    	Test.startTest();
    	
    	ws_AttachmentSAPService.SAPAttachmentAck attAck = new ws_AttachmentSAPService.SAPAttachmentAck();    	
    	attAck.SFDC_IDS = 'fakeId';
    	attAck.DISTRIBUTION_STATUS = 'TRUE';
    	
    	User sapInterface = [Select Id from User where alias = 'sap'];
        
        System.runAs(sapInterface){
        	
        	Boolean hasError = false;
            
            try{
        		
        		ws_AttachmentSAPService.createSAPAttachmentAcknowledgement(attAck);
        		
        	}catch(Exception e){
            	
            	hasError = true;            	
            }
            
            System.assert(hasError == true);        	
        }
    }
    
    private static testmethod void testCreateSFDCAttachmentAcknowledgementError(){
    	    	
    	Test.startTest();
    	
    	ws_AttachmentSAPService.SFDCAttachmentAck attAck = new ws_AttachmentSAPService.SFDCAttachmentAck();    	
    	attAck.SFDC_IDS = new List<String>{'fakeId'};
    	attAck.DISTRIBUTION_STATUS = 'TRUE';
    	
    	User sapInterface = [Select Id from User where alias = 'sap'];
        
        System.runAs(sapInterface){
        	
        	Boolean hasError = false;
            
            try{
        		
        		ws_AttachmentSAPService.createSFDCAttachmentAcknowledgement(attAck);
        		
        	}catch(Exception e){
            	
            	hasError = true;            	
            }
            
            System.assert(hasError == true);        	
        }
    }
    
    @testSetup
    private static void createSampleData(){
    	
    	Account sapAccount = new Account();
    	sapAccount.Name = 'Test Account';
    	sapAccount.SAP_Id__c = '11111'; // This will set the record type to SAP Account
    	sapAccount.Account_Country_vs__c = 'NETHERLANDS';
    	sapAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('SAP Account').getRecordTypeId();
    	insert sapAccount;
    	
    	Product2 installed = new Product2();
    	installed.IsActive = true; 
    	installed.Name = 'Test Installed Product';
    	installed.MPG_Code_Text__c = 'AA';
    	installed.RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('SAP Product').getRecordTypeId();
    	installed.ProductCode = '12345';    	    	    	
    	insert installed;
    	
    	SVMXC__Service_Group__c techGroup = new SVMXC__Service_Group__c();
    	techGroup.SVMXC__Active__c = true; 
    	techGroup.SVMXC__Country__c = 'Netherlands, The';
    	techGroup.SVMXC__Description__c = 'Test Technician Group';    	
    	insert techGroup;
    	
    	SVMXC__Service_Group_Members__c technician = new SVMXC__Service_Group_Members__c();
    	technician.SVMXC__Active__c = true;
    	technician.SVMXC__Country__c = 'Netherlands, The';
    	technician.SVMXC__Salesforce_User__c = UserInfo.getUserId();
    	technician.SVMX_SAP_Employee_Id__c = '55555';
    	technician.SVMXC__Service_Group__c = techGroup.Id;    	
    	insert technician;    	
    	
    	SVMXC__Installed_Product__c instProduct = new SVMXC__Installed_Product__c();
    	instProduct.SVMXC__Company__c = sapAccount.Id;
    	instProduct.SVMXC__Country__c = 'Netherlands, The';
    	instProduct.SVMXC__Product__c = installed.Id;
    	instProduct.SVMX_SAP_Equipment_ID__c = '99999';
    	instProduct.SVMXC__Serial_Lot_Number__c = '99999';
    	insert instProduct;
    }
}