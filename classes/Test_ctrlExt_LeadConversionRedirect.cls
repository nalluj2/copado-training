/*
 * Description		: Test class for Controller Extension ctrlExt_LeadConversionRedirect
 * Author        	: Patrick Brinksma
 * Created Date    	: 15-07-2013
 */
@isTest
private class Test_ctrlExt_LeadConversionRedirect {

    static testMethod void testLeadConversionRedirect() {
        // Create test Lead
        String randVal = Test_TestDataUtil.createRandVal();
        Lead tstLead = createLead(randVal, 1);
        Set<String> setOfRt = new Set<String>{'CAN_DiB_Lead'};
        Map<Id, String> mapOfRtIdToName = bl_Lead.getRecordTypeIdsByDeveloperNames(setOfRt);
        for (Id thisId : mapOfRtIdToName.keySet()){
        	tstLead.RecordTypeId = thisId;
        } 
        insert tstLead;
        
        Test.startTest();
		ApexPages.StandardController sc = new ApexPages.standardController(tstLead);        
		ctrlExt_LeadConversionRedirect lConvRedCtrl = new ctrlExt_LeadConversionRedirect(sc);
		PageReference thisPage = Page.LeadConversionRedirect;
		thisPage.getParameters().put('Id', tstLead.Id);
		Test.setCurrentPage(thisPage);
		PageReference retPage = lConvRedCtrl.redirectToLeadConversion();
        Test.stopTest();
        // Assert Result
        //System.assertEquals(retPage.getUrl(), '/lead/leadconvert.jsp?id=' + tstLead.Id + '&retURL=%2F' + tstLead.Id, 'Redirect Url incorrect!');
    }
    
    private static Lead createLead(String randVal, Integer i){
    	Lead tstLead = new Lead();
    	tstLead.LastName = randVal + i;
    	tstLead.Lead_Address_Line_1__c = randVal + i;
    	tstLead.Lead_Gender_Text__c = 'Male';
    	tstLead.Lead_City__c = 'Test City';
    	tstLead.Email = randVal + i + '@salesforcetesting.com';
    	tstLead.Lead_State_Province__c = 'Alberta';
    	tstLead.Lead_Zip_Postal_Code__c = '12345';
    	tstLead.Lead_Country_vs__c = 'CANADA';
    	tstLead.Primary_Health_Insurer_Web__c = randVal + i;
    	tstLead.Secondary_Health_Insurer_Web__c = randVal + i;    	
    	return tstLead;
    }    
}