//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 20-01-2016
//  Description      : APEX Test Class to test the logic in tr_SVMXC_ServiceOrderLine
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_tr_SVMXC_ServiceOrderLine {

    //----------------------------------------------------------------------------------------
    // CREATE TEST DATA
    //----------------------------------------------------------------------------------------
    @testSetup static void createTestData() {

        // Create Custom Setting Data
        clsTestData.createCustomSettingData(true);

        // Create Account Data
        clsTestData.tCountry_Account = 'United Kingdom';
        clsTestData.iRecord_Account = 1;
        clsTestData.createAccountData(true);

        // Create SVMXC__Service_Order_Line__c Data
    	clsTestData.idRecordType_SVMXCServiceOrderLine = clsUtil.getRecordTypeByDevName('SVMXC__Service_Order_Line__c', 'Estimate').Id;
        clsTestData.createSVMXCServiceOrderLineData(false);
        	clsTestData.oMain_SVMXCServiceOrderLine.SVMXC_Component_SAP_Id__c = '12345';
        	clsTestData.oMain_SVMXCServiceOrderLine.SAP_Id__c = '54321';
        insert clsTestData.oMain_SVMXCServiceOrderLine;

        clsTestData.oMain_SVMXCServiceOrderLine = null;
    	clsTestData.idRecordType_SVMXCServiceOrderLine = clsUtil.getRecordTypeByDevName('SVMXC__Service_Order_Line__c', 'RequestReceipt').Id;
        clsTestData.createSVMXCServiceOrderLineData(false);
        	clsTestData.oMain_SVMXCServiceOrderLine.SVMXC_Component_SAP_Id__c = '12345';
        	clsTestData.oMain_SVMXCServiceOrderLine.SAP_Id__c = '54321';
        insert clsTestData.oMain_SVMXCServiceOrderLine;

    }
    //----------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------
    // Basic Testing
    //----------------------------------------------------------------------------------------
	@isTest static void test_tr_SVMXC_ServiceOrderLine() {
	
		// Select Data
		List<SVMXC__Service_Order_Line__c> lstData = [SELECT Id FROM SVMXC__Service_Order_Line__c];
		SVMXC__Service_Order_Line__c oData = lstData[0];
		
		// Update Data
		update oData;

		// Delete Data
		delete oData;

		// Undelete Data
		Undelete oData;

	}
    //----------------------------------------------------------------------------------------

	
    //----------------------------------------------------------------------------------------
    // Test Prevent Update on Restricted Fields
    //----------------------------------------------------------------------------------------
	@isTest static void test_preventUpdateOnRestrictedFields_System() {

		// Select Data
		List<SVMXC__Service_Order_Line__c> lstData = [SELECT Id, SVMXC_Component_SAP_Id__c, SAP_Id__c FROM SVMXC__Service_Order_Line__c WHERE SVMXC_Component_SAP_Id__c = '12345' AND SAP_Id__c = '54321'];
		SVMXC__Service_Order_Line__c oData = lstData[0];

		System.assertNotEquals(clsUtil.isNull(oData.SVMXC_Component_SAP_Id__c, ''), 'T1E1S1T1');
		System.assertNotEquals(clsUtil.isNull(oData.SAP_Id__c, ''), 'T2E2S2T2');

		// UPDATE
		Test.startTest();

			oData.SVMXC_Component_SAP_Id__c = 'T1E1S1T1';
			oData.SAP_Id__c = 'T2E2S2T2';
		update oData;

		Test.stopTest();

		// Validate that the update has been performed
		lstData = [SELECT Id, SVMXC_Component_SAP_Id__c, SAP_Id__c FROM SVMXC__Service_Order_Line__c WHERE Id = :oData.Id];
		oData = lstData[0];
		System.assertEquals(oData.SVMXC_Component_SAP_Id__c, 'T1E1S1T1');
		System.assertEquals(oData.SAP_Id__c, 'T2E2S2T2');

	}

	@isTest static void test_preventUpdateOnRestrictedFields_NotSystem() {

		// Get the User Profile and Role
		Profile oProfile = [SELECT Id FROM Profile WHERE Name = 'EUR Field Service Engineer' LIMIT 1];
		UserRole oUserRole = [SELECT Id FROM UserRole WHERE DeveloperName = 'EUR_Service_Repair' LIMIT 1];

		// Select an Active User based on the collected Profile and Role
		//	If there is not Active User available, create one
		List<User> lstUser = [SELECT Id FROM User WHERE IsActive = true AND ProfileId = :oProfile.Id AND UserRoleId = :oUserRole.Id LIMIT 1];
		User oUser;
		if (lstUser.size() == 1){
			oUser = lstUser[0];
		}else{
			clsTestData.createUserData(clsUtil.getTimeStamp() + '@test.medtronic.com', 'United Kingdom', oProfile.Id, oUserRole.Id, true);
			oUser = clsTestData.oMain_User;
		}
		
		List<SVMXC__Service_Order__c> lstDataSO = [SELECT Id, SVMX_SAP_Service_Order_No__c FROM SVMXC__Service_Order__c];
		SVMXC__Service_Order__c oDataSO = lstDataSO[0];
		oDataSO.OwnerId = oUser.Id;
		update oDataSO;		
		
		List<SVMXC__Service_Order_Line__c> lstData = [SELECT Id, SVMXC_Component_SAP_Id__c, SAP_Id__c FROM SVMXC__Service_Order_Line__c WHERE SVMXC_Component_SAP_Id__c = '12345' AND SAP_Id__c = '54321'];
		SVMXC__Service_Order_Line__c oData = lstData[0];
		
		// UPDATE
		Test.startTest();
			System.runAs(oUser){
					
				System.assertNotEquals(clsUtil.isNull(oData.SVMXC_Component_SAP_Id__c, ''), 'T1E1S1T1');
				System.assertNotEquals(clsUtil.isNull(oData.SAP_Id__c, ''), 'T2E2S2T2');

					oData.SVMXC_Component_SAP_Id__c = 'T1E1S1T1';
					oData.SAP_Id__c = 'T2E2S2T2';
				update oData;
			}
		Test.stopTest();

		// Validate that the update has NOT been performed
		lstData = [SELECT Id, SVMXC_Component_SAP_Id__c, SAP_Id__c FROM SVMXC__Service_Order_Line__c WHERE Id = :oData.Id];
		oData = lstData[0];
		System.assertEquals(clsUtil.isNull(oData.SVMXC_Component_SAP_Id__c, ''), '12345');
		System.assertEquals(clsUtil.isNull(oData.SAP_Id__c, ''), '54321');

	}
    //----------------------------------------------------------------------------------------	
	
	private static testmethod void testDefaultingQtyTravelLabor(){
		
		for(SVMXC__Service_Order_Line__c orderLine : [SELECT Id, SVMXC__Actual_Quantity2__c FROM SVMXC__Service_Order_Line__c]){
			
			System.assert(orderLine.SVMXC__Actual_Quantity2__c == null);
		}
		
		Id orderLineRT = clsUtil.getRecordTypeByDevName('SVMXC__Service_Order_Line__c', 'UsageConsumption').Id;		
		SVMXC__Service_Order__c sOrder = [Select Id from SVMXC__Service_Order__c];		
		
		Test.startTest();
        
        SVMXC__Service_Order_Line__c travelLine = new SVMXC__Service_Order_Line__c();
        travelLine.RecordTypeID = orderLineRT;
        travelLine.SVMXC__Service_Order__c = sOrder.Id;
        travelLine.SVMXC__Line_Type__c = 'Travel';
        travelLine.SVMXC_Component_SAP_Id__c = '12345';
        travelLine.SAP_Id__c = '54321';
        insert travelLine;
        
        travelLine = [Select Id, SVMXC__Actual_Quantity2__c from SVMXC__Service_Order_Line__c where Id = :travelLine.Id];
        
        System.assert(travelLine.SVMXC__Actual_Quantity2__c == 0);
		
		SVMXC__Service_Order_Line__c laborLine = new SVMXC__Service_Order_Line__c();
        laborLine.RecordTypeID = orderLineRT;
        laborLine.SVMXC__Service_Order__c = sOrder.Id;
        laborLine.SVMXC__Line_Type__c = 'Labor';
        laborLine.SVMXC_Component_SAP_Id__c = '54321';
        laborLine.SAP_Id__c = '12345';
        insert laborLine;
        
        laborLine = [Select Id, SVMXC__Actual_Quantity2__c from SVMXC__Service_Order_Line__c where Id = :laborLine.Id];
        
        System.assert(laborLine.SVMXC__Actual_Quantity2__c == 0);
	}
}
//--------------------------------------------------------------------------------------------------------------------