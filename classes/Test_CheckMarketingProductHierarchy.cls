@isTest
private class Test_CheckMarketingProductHierarchy {
         	
 	private static testmethod void testCheckProductHierarchy(){
 		
 		Company__c cmpny = new Company__c();
       	cmpny.name='TestMedtronic';
       	cmpny.CurrencyIsoCode = 'EUR';
       	cmpny.Current_day_in_Q1__c=56;
       	cmpny.Current_day_in_Q2__c=34; 
       	cmpny.Current_day_in_Q3__c=5; 
       	cmpny.Current_day_in_Q4__c= 0;   
       	cmpny.Current_day_in_year__c =200;
       	cmpny.Days_in_Q1__c=56;  
       	cmpny.Days_in_Q2__c=34;
       	cmpny.Days_in_Q3__c=13;
       	cmpny.Days_in_Q4__c=22;
       	cmpny.Days_in_year__c =250;
       	cmpny.Company_Code_Text__c = 'T41';
       	insert cmpny;     

        Business_Unit__c bu = new Business_Unit__c();
        bu.name = 'Testing BU';
        bu.abbreviated_name__c = 'Abb Bu';
        bu.Company__c = cmpny.Id;
        insert bu; 
        
        RecordType mdtMarketingProduct = [Select Id from RecordType where SObjectType = 'Product2' AND DeveloperName = 'MDT_Marketing_Product'];
        
        Product2 prd1 = new Product2(Name = 'Test Product 1', Business_Unit_ID__c = bu.Id, RecordTypeId = mdtMarketingProduct.Id);
        Product2 prd2 = new Product2(Name = 'Test Product 2', Business_Unit_ID__c = bu.Id, RecordTypeId = mdtMarketingProduct.Id);
        Product2 prd3 = new Product2(Name = 'Test Product 3', Business_Unit_ID__c = bu.Id, RecordTypeId = mdtMarketingProduct.Id);
        Product2 prd4 = new Product2(Name = 'Test Product 4', Business_Unit_ID__c = bu.Id, RecordTypeId = mdtMarketingProduct.Id);
        Product2 prd5 = new Product2(Name = 'Test Product 5', Business_Unit_ID__c = bu.Id, RecordTypeId = mdtMarketingProduct.Id);  
        
        insert new List<Product2>{prd1, prd2, prd3, prd4, prd5}; 
        
        Test.startTest();	
        
        prd2.Parent_Product__c = prd1.Id;
        prd3.Parent_Product__c = prd1.Id;
        prd4.Parent_Product__c = prd2.Id;
        prd5.Parent_Product__c = prd2.Id;
        
        update new List<Product2>{prd2, prd3, prd4, prd5};
        
        prd1.Parent_Product__c = prd4.Id;
        
        String errorMessage;
        
        try{
        	
        	update prd1;
        	
        }catch(Exception e){
        	
        	errorMessage = e.getMessage();
        }
        
        System.assert(errorMessage != null && errorMessage.contains('A loop has been detected in the Product Hierarchy.'));
 	}   
}