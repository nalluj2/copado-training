@isTest
private class Test_tr_SVMXC_PMPlan {
    
    private static testmethod void setPMPlanCountry(){
    	
    	Account acc = new Account();
    	acc.Account_Country_vs__c = 'NETHERLANDS';
    	acc.Name = 'Test Account';
    	insert acc;
    	
    	Test.startTest();
    	
    	SVMXC__PM_Plan__c plan = new SVMXC__PM_Plan__c();
    	plan.SVMXC__Account__c = acc.Id;
    	insert plan;
    	
    	Test.stopTest();
    	
    	plan = [Select Id, Account_Country__c from SVMXC__PM_Plan__c where Id = :plan.Id];
    	
    	System.assert(plan.Account_Country__c == 'NETHERLANDS');
    }
}