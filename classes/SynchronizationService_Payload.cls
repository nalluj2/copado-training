public virtual class SynchronizationService_Payload {
    
    public List<SyncAccount> accounts {get; set;}
    public List<SyncContact> contacts {get; set;}
    
    private String houseAccountCode = '0000103101';
    
    public void generateAccountPayload(Set<Id> accountIds, Set<Id> contactIds){
        
        accounts = new List<SyncAccount>();
        contacts = new List<SyncContact>();
                        
        //First we start with Contacts so we can add their Accounts to the list too before processing
        if(contactIds != null && contactIds.size() > 0){
            
            for(Contact contact : [ Select Name, FirstName, LastName, AccountId, Account.SAP_ID__c, Account.AccountNumber, Account.RecordType.DeveloperName, 
                                    Account.ST_NAV_Non_SAP_Account__c, Phone, Email, MobilePhone,
                                    MailingCity, MailingCountry, MailingState, MailingStreet, MailingPostalCode, External_Id__c 
                                    from Contact where Id IN :contactIds]){
                
                SyncContact cnt = new SyncContact();
                cnt.Name = contact.Name;
                cnt.FirstName = contact.FirstName;
                cnt.LastName = contact.LastName;
                cnt.Phone = contact.Phone;              
                cnt.Email = contact.Email;
                cnt.MobilePhone = contact.MobilePhone;
                cnt.City = contact.MailingCity;
                cnt.Country = contact.MailingCountry;
                cnt.State = contact.MailingState;
                cnt.Street = contact.MailingStreet;
                cnt.PostalCode = contact.MailingPostalCode;
                cnt.ExternalId = contact.External_Id__c;
                
                if(contact.AccountId != null){
                    
                    if ( (contact.Account.RecordType.DeveloperName == 'SAP_Account') || (contact.Account.RecordType.DeveloperName == 'Distributor') ){
                        
                        accountIds.add(contact.AccountId);
                        cnt.AccountSAPId = contact.Account.SAP_ID__c;
                        
                    }else if(contact.Account.RecordType.DeveloperName == 'MDT_Account'){
                        
                        cnt.AccountSAPId = houseAccountCode;//Hardcoded code for Swiss Principal MNAV Account in US
                    
                    }else if(contact.Account.ST_NAV_Non_SAP_Account__c == true){
                        
                        accountIds.add(contact.AccountId);
                        cnt.AccountSAPId = contact.Account.AccountNumber;                       
                    }
                }
                
                contacts.add(cnt);
            }
        }
        
        if(accountIds != null && accountIds.size() > 0){
            
            for(Account account : 
                [
                    SELECT Name, SAP_ID__c, Phone, BillingCity, BillingCountry, BillingState, BillingStreet, BillingPostalCode, AccountNumber, RecordType.DeveloperName 
                    FROM Account 
                    WHERE 
                        Id IN :accountIds 
                        AND 
                            ( RecordType.DeveloperName = 'SAP_Account' OR RecordType.DeveloperName = 'Distributor' OR ST_NAV_Non_SAP_Account__c = true) 
                        AND 
                            SAP_ID__c != :houseAccountCode
                ]){
                
                SyncAccount acc = new SyncAccount();
                acc.Name = account.Name;
                
                if ( (account.RecordType.DeveloperName == 'SAP_Account') || (account.RecordType.DeveloperName == 'Distributor') ){
                    acc.SAPId = account.SAP_ID__c;
                }else{
                    acc.SAPId = account.AccountNumber;
                }
                
                acc.Phone = account.Phone;              
                acc.City = account.BillingCity;
                acc.Country = account.BillingCountry;
                acc.State = account.BillingState;
                acc.Street = account.BillingStreet;
                acc.PostalCode = account.BillingPostalCode;
                
                accounts.add(acc);
            }
        }       
    }
    
    public class SyncAccount{   
        
        public String SAPId {get; set;}
        public String name {get; set;}
        public String phone {get; set;}
        public String city {get; set;}
        public String country {get; set;}
        public String state {get; set;}
        public String street {get; set;}
        public String postalCode {get; set;}
        
    }
    
    public class SyncContact{   
        
        public String accountSAPId {get; set;}
        public String name {get; set;}
        public String firstName {get; set;}
        public String lastName {get; set;}
        public String phone {get; set;}
        public String email {get; set;}
        public String mobilePhone {get; set;}
        public String city {get; set;}
        public String country {get; set;}
        public String state {get; set;}
        public String street {get; set;}
        public String postalCode {get; set;}
        public String externalId {get; set;}
    }
}