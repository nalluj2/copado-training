/*
 *      Created Date : 20150224
 *      Description : This class tests the logic for the User Territory Change triggers
 *
 *      Author = Rudy De Coninck
 */
@IsTest(seeAllData=true)
public with sharing class Test_tr_UserTerritoryChanges {


		@IsTest
		public static void createUserTerritoryChange(){

			List<Territory2> territories = [select Id, name from Territory2  where external_key__c !=null limit 5];	

			Territory2 t = territories.get(0);

			User_Territory_Change__c utc = new User_Territory_Change__c();
			utc.User__c = Userinfo.getUserId();
			utc.Territory_Name__c = t.name;

			insert utc;
			
			
		}

		@IsTest
		public static void createUserTerritoryChangeWithExternalIds(){

			List<Territory2> territories = [select Id, name, external_key__c from Territory2  where external_key__c !=null limit 5];	
			Territory2 t = territories.get(0);

			List<User> users = [select Id, name, sap_id__c from User where sap_id__c!=null];
			User u = users.get(0);
			User_Territory_Change__c utc = new User_Territory_Change__c();
			utc.User_SAP_ID__c = u.sap_id__c;
			utc.Territory_Name__c = t.name;
			utc.Territory_External_Key__c = t.External_Key__c;

			insert utc;
		}

}