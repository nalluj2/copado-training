/**
 *      Created Date : 20121113
 *      Description : This class is used to archive remove Contact Call Topic Count records older than 1 year
 * 
 *      Authors = Dheeraj Gupta, Rudy De Coninck
 */
global class ba_ContactCallTopicArchive implements Schedulable, Database.Batchable<SObject>{

        global void execute(SchedulableContext ctx){        
            // Start Batch Apex job        
            Database.executeBatch(new ba_ContactCallTopicArchive(),200);    
        } 

        global Database.QueryLocator start(Database.BatchableContext ctx){    
			String query='Select a.ID,a.active__c,Call_Date__c From Contact_Call_Topic_Count__c a';
			System.debug(' query - ' + query);
            return Database.getQueryLocator(query);      
        } 

        global void execute(Database.BatchableContext ctx, List<SObject> records){  

            // Select all Account_Call_Topic_Count__c records which are older then 365 days
		 	Date currentdate = system.Today();                  
			currentdate = date.valueOf(currentdate - 365);         	

           Contact_Call_Topic_Count__c CCTCount;
           list<Contact_Call_Topic_Count__c> lstCCTCounts = new list<Contact_Call_Topic_Count__c>(); 
           // Get Query records one by one
           for(SObject record : records){                          
	            CCTCount =(Contact_Call_Topic_Count__c) record; 
           		System.Debug('CCTCount - ' + CCTCount.Id + ' Visit Date - ' + CCTCount.Call_Date__c);                     	            
	            if(CCTCount.Call_Date__c < currentdate){
					// Creating records so & make flag as false
	    	        Contact_Call_Topic_Count__c CCTC = new  Contact_Call_Topic_Count__c(id=CCTCount.Id);
	    	        lstCCTCounts.add(CCTC);
	            } 
           }
           
           // Updating the list if its size is > 0
           if(lstCCTCounts.size()>0){
           	delete lstCCTCounts;
           }
        	
        }	

        global void finish(Database.BatchableContext ctx){
        	    
        }
}