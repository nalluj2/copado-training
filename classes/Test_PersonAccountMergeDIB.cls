/*
 *      Description : This is the Test Class for the APEX Controller Extension ctrlExt_AccountAssetList
 *      Version     : 1.0
 *      Author      : Bart Caelen
 *      Date        : 201403
*/
@isTest private class Test_PersonAccountMergeDIB {

    @isTest static void Test_MergePatientaccounts_byEmail() {
		
		User sapInterfaceUser = [select id from user where profile.name = 'SAP Interface' limit 1]; 
        
		List<Account> nonSAPPatients = Test_TestDataUtil.createPersonAccounts('mergepatient', 1,'NETHERLANDS');
		Account nonSAPPatient = nonSAPPatients.get(0);
		nonSAPPatient.firstname='Test';
		nonSAPPatient.Account_Email__c = nonSAPPatient.firstname+'.'+nonSAPPatient.lastname+'@medtronictest.com';
		insert nonSAPPatient;
		
		Test.startTest();
		
		System.runAs(sapInterfaceUser) {
			
			//Auto Merge 
			List<Account> SAPPatients = Test_TestDataUtil.createSAPPersonAccounts('mergepatient', 1,'NETHERLANDS');
			Account SAPPatient = SAPPatients.get(0);
			SAPPatient.firstname='Test';
			SAPPatient.SAP_Email__c = SAPPatient.firstname+'.'+SAPPatient.lastname+'@medtronictest.com';
			insert SAPPatient;
		
		}		    	
		
		Test.stopTest();
		
		List<Account> nonSAPPatientsAfterMerge = [select Id, isDeleted, masterrecordid from account where id=:nonSAPPatient.id ALL ROWS];
		
		System.assert(nonSAPPatientsAfterMerge.size() == 1);
		System.assert(nonSAPPatientsAfterMerge[0].isDeleted == true);
		System.assert(nonSAPPatientsAfterMerge[0].MasterRecordId != null);		
    }
    
    @isTest static void Test_MergePatientaccounts_byName() {
		
		User sapInterfaceUser = [select id from user where profile.name = 'SAP Interface' limit 1]; 
        
		List<Account> nonSAPPatients = Test_TestDataUtil.createPersonAccounts('mergepatient', 1,'NETHERLANDS');
		Account nonSAPPatient = nonSAPPatients.get(0);		
		nonSAPPatient.firstname='Test';		
		insert nonSAPPatient;
		
		Test.startTest();
		
		System.runAs(sapInterfaceUser) {
			
			//Auto Merge 
			List<Account> SAPPatients = Test_TestDataUtil.createSAPPersonAccounts('mergepatient', 1,'NETHERLANDS');
			Account SAPPatient = SAPPatients.get(0);
			SAPPatient.firstname='Test';			
			insert SAPPatient;
		
		}		    	
		
		Test.stopTest();
		
		List<Account> nonSAPPatientsAfterMerge = [select Id, isDeleted, masterrecordid from account where id=:nonSAPPatient.id ALL ROWS];
		
		System.assert(nonSAPPatientsAfterMerge.size() == 1);
		System.assert(nonSAPPatientsAfterMerge[0].isDeleted == true);
		System.assert(nonSAPPatientsAfterMerge[0].MasterRecordId != null);		
    }
    
    @isTest static void Test_MergePatientaccounts_CannotMerge() {
		
		DIB_Country__c netherlands = new DIB_Country__c();
		netherlands.Name = 'NETHERLANDS';
		netherlands.Country_Iso_Code__c = 'NL';
		netherlands.Patient_auto_merge_mailing_userIds__c = UserInfo.getUserId();
		insert netherlands;
		
		User sapInterfaceUser = [select id from user where profile.name = 'SAP Interface' limit 1]; 
        
		List<Account> nonSAPPatients = Test_TestDataUtil.createPersonAccounts('non sap patient', 1,'NETHERLANDS');
		Account nonSAPPatient = nonSAPPatients.get(0);
		nonSAPPatient.firstname='Test';		
		insert nonSAPPatient;
		
		Test.startTest();
		
		System.runAs(sapInterfaceUser) {
			
			//Auto Merge 
			List<Account> SAPPatients = Test_TestDataUtil.createSAPPersonAccounts('sap patient', 1,'NETHERLANDS');
			Account SAPPatient = SAPPatients.get(0);
			SAPPatient.firstname='Test';			
			insert SAPPatient;
		
		}		    	
		
		Test.stopTest();
		
		List<Account> nonSAPPatientsAfterMerge = [select Id, masterrecordid, isDeleted from account where id=:nonSAPPatient.id ALL ROWS];
		
		System.assert(nonSAPPatientsAfterMerge.size() == 1);
		System.assert(nonSAPPatientsAfterMerge[0].isDeleted == false);
		System.assert(nonSAPPatientsAfterMerge[0].MasterRecordId == null);		
    }

	@isTest static void Test_MergePatientaccounts_MergeFailed() {
		
		DIB_Country__c netherlands = new DIB_Country__c();
		netherlands.Name = 'NETHERLANDS';
		netherlands.Country_Iso_Code__c = 'NL';
		netherlands.Patient_auto_merge_mailing_userIds__c = UserInfo.getUserId();
		insert netherlands;
		
		User sapInterfaceUser = [select id from user where profile.name = 'SAP Interface' limit 1]; 
        
		List<Account> SAPPatients = Test_TestDataUtil.createSAPPersonAccounts('sap patient', 1,'NETHERLANDS');
		Account SAPPatient1 = SAPPatients.get(0);
		SAPPatient1.firstname='Test';		
		SAPPatient1.SAP_ID__c = '111111';
		insert SAPPatient1;
		
		Test.startTest();
		
		System.runAs(sapInterfaceUser) {
			
			//Auto Merge 
			SAPPatients = Test_TestDataUtil.createSAPPersonAccounts('sap patient', 1,'NETHERLANDS');
			Account SAPPatient2 = SAPPatients.get(0);
			SAPPatient2.firstname='Test';			
			SAPPatient2.SAP_ID__c = '22222';
			insert SAPPatient2;
		
			bl_Account.autoMergeSAPPatient(SAPPatient2.Id, SAPPatient1.Id);
		}		    	
		
		Test.stopTest();
		
		List<Account> SAPPatientsAfterMerge = [select Id, masterrecordid, isDeleted from account where id=:SAPPatient1.id ALL ROWS];
		
		System.assert(SAPPatientsAfterMerge.size() == 1);
		System.assert(SAPPatientsAfterMerge[0].isDeleted == false);
		System.assert(SAPPatientsAfterMerge[0].MasterRecordId == null);		
    }
	
}