/*	Integration Service related class
	
	This REST service is used by the source Org to send the notifications for their processing in the target Org
*/
@RestResource(urlMapping='/SynchronizationServiceTarget/*')
global with sharing class ws_SynchronizationService_Target {
	
	//This method receives the Notifications from the source Org and stores local copies for its further processing. As a result, this method
	//sends back all the Notifications that were successfuly stored locally. Finally it schedules the batch that will take of processing the
	//notifications
	@HttpPost
	global static SynchronizationServiceResponse doPost(List<Sync_Notification__c> notifications) {
						
		List<Sync_Notification__c> inboundNotifications = new List<Sync_Notification__c>();
		
		for(Sync_Notification__c notification : notifications){
			
			Sync_Notification__c inboundNotification = new Sync_Notification__c();
			inboundNotification.Type__c = 'Inbound';
			inboundNotification.Status__c = 'New';
			inboundNotification.Notification_Id__c = notification.Notification_Id__c;
			inboundNotification.Record_Id__c = notification.Record_Id__c;
			inboundNotification.Record_Object_Type__c = notification.Record_Object_Type__c;
			
			inboundNotifications.add(inboundNotification);
		}
		
		//We use the Notification external Id field (Notification_Id__c) to insert/update it. This will avoid problems when a notification is sent
		//twice from the source Org (possible if multiple batches are run in parallel). Notice that the flag AllOrNone is set to false, so individual
		//failures are allowed.
		List<Database.UpsertResult> upsertResult = Database.upsert(inboundNotifications, Sync_Notification__c.Fields.Notification_Id__c, false);
		
		SynchronizationServiceResponse response = new SynchronizationServiceResponse();
		response.notifications = new List<Sync_Notification__c>();
		
		for(Integer i = 0; i < inboundNotifications.size(); i++){
			
			Database.UpsertResult result = upsertResult[i];
			
			if(result.isSuccess()) response.notifications.add(notifications[i]);
		}
		
		//Check if there is already a batch execution in the queue. If so, there is no need to create a new one.	
		List<AsyncApexJob> pendingJobs = [SELECT Id FROM AsyncApexJob where JobType = 'BatchApex' AND ApexClass.Name = 'ba_SynchronizationService_Inbound' AND Status IN ('Holding', 'Queued') LIMIT 1 ];
		
		if(pendingJobs.isEmpty()){
			ba_SynchronizationService_Inbound batch = new ba_SynchronizationService_Inbound();
			Database.executeBatch(batch, 1); // Batch size equal to 1 because we need to process each record individually for transaction control
		}
		
		return response;
	}
		
	global class SynchronizationServiceResponse{
		public List<Sync_Notification__c> notifications {get;set;}
	}
}