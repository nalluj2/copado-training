//--------------------------------------------------------------------------------------------------------------------
//  Description : This TEST APEX Class is used to test the logic in ba_Cvent
//  Version     : 1.0
//  Author      : Bart Caelen
//  Date        : 20150508
//--------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_ba_Cvent {

    static Integer iBatchSize = 100;

    @testSetup static void createTestData() {

        Id id_RecordType_Campaign_Cvent = clsUtil.getRecordTypeByDevName('Campaign', 'CVent').Id;

        List<Profile> lstProfile = [SELECT Id, Name FROM Profile ORDER BY Name];
        Map<String, String> mapProfileId = new Map<String, String>();
        for (Profile oProfile : lstProfile){
            mapProfileId.put(oProfile.Name.toUpperCase(), String.valueOf(oProfile.Id).left(15));
        }
        SystemAdministratorProfileId__c oSystemAdminstratorProfileId = new SystemAdministratorProfileId__c();
            oSystemAdminstratorProfileId.Name = 'Default';
            oSystemAdminstratorProfileId.CVent__c = mapProfileId.get('CVENT');
            oSystemAdminstratorProfileId.SystemAdministratorMDT__c = mapProfileId.get('SYSTEM ADMINISTRATOR MDT');
            oSystemAdminstratorProfileId.SystemAdministrator__c = mapProfileId.get('SYSTEM ADMINISTRATOR');
            oSystemAdminstratorProfileId.ITBusinessAnalyst__c = mapProfileId.get('IT BUSINESS ANALYST');
            oSystemAdminstratorProfileId.MMX_Interface__c = mapProfileId.get('MMX INTERFACE');
            oSystemAdminstratorProfileId.SAPInterface__c = mapProfileId.get('SAP INTERFACE');
        insert oSystemAdminstratorProfileId;


        // Create Account Data
        clsTestData.iRecord_Account = 1;
        clsTestData.createAccountData();

        // Create Contact Data
        clsTestData.iRecord_Contact = 10;
        clsTestData.createContactData();

        // Create Campaign Data
        clsTestData.createCampaignData(true, id_RecordType_Campaign_Cvent);
            clsTestData.oMain_Campaign.Send_to_CVent__c = true;
            clsTestData.oMain_Campaign.Sync_With_CVent__c = true;
        update clsTestData.oMain_Campaign;

        // Create CampaignMember Data
        clsTestData.iRecord_CampaignMember = 10;
        clsTestData.createCampaignMemberData(true);

    }

    @isTest static void test_ba_Cvent_Campaign() {

        Test.startTest();

        //---------------------------------------
        // TEST BATCH
        //---------------------------------------
        ba_Cvent oBA_Cvent_Campaign = new ba_Cvent();
            oBA_Cvent_Campaign.tProcessType = 'CAMPAIGN';
            oBA_Cvent_Campaign.idCampaignRecordtype_Cvent = clsUtil.getRecordTypeByDevName('Campaign', 'CVent').Id;
        Database.executeBatch(oBA_Cvent_Campaign, iBatchSize);    
        //---------------------------------------

        Test.stopTest();

    }   

    @isTest static void test_ba_Cvent_CampaignMember() {

        Test.startTest();

        //---------------------------------------
        // TEST BATCH
        //---------------------------------------
        ba_Cvent oBA_Cvent_CampaignMember = new ba_Cvent();
            oBA_Cvent_CampaignMember.tProcessType = 'CAMPAIGNMEMBER';
            oBA_Cvent_CampaignMember.idCampaignRecordtype_Cvent = clsUtil.getRecordTypeByDevName('Campaign', 'CVent').Id;
        Database.executeBatch(oBA_Cvent_CampaignMember, iBatchSize);    
        //---------------------------------------

        Test.stopTest();

    }   

    @isTest static void test_ba_Cvent_UpdateCventEvent() {

        Test.startTest();

        //---------------------------------------
        // TEST BATCH
        //---------------------------------------
        ba_Cvent oBA_Cvent_UpdateCventEvent = new ba_Cvent();
            oBA_Cvent_UpdateCventEvent.tProcessType = 'UPDATECVENTEVENT';
            oBA_Cvent_UpdateCventEvent.idCampaignRecordtype_Cvent = clsUtil.getRecordTypeByDevName('Campaign', 'CVent').Id;
        Database.executeBatch(oBA_Cvent_UpdateCventEvent, iBatchSize);    
        //---------------------------------------

        Test.stopTest();

    }   

    @isTest static void test_ba_Cvent_UpdateCventContact() {

        Test.startTest();

        List<String> lstCventContactId = new List<String>{'5379193b-3662-4752-8187-451eb080c53c','5379193b-3662-4752-8187-451eb080c53d','5379193b-3662-4752-8187-451eb080c53e'};
        Map<Integer, List<String>> mapData_CVentContactId = clsUtil.splitDataList(lstCventContactId, bl_Cvent.iCventAPILImit_Update);
        Integer iSOQLLimit = mapData_CVentContactId.keySet().size();

        //---------------------------------------
        // TEST BATCH
        //---------------------------------------
        ba_Cvent oBatch = new ba_Cvent();
            oBatch.tProcessType = 'UPDATECVENTCONTACT';
            oBatch.idCampaignRecordtype_Cvent = clsUtil.getRecordTypeByDevName('Campaign', 'CVent').Id;
            oBatch.iSOQLLimit = iSOQLLimit;
            oBatch.mapData_CVentContactId = mapData_CVentContactId;
        Database.executebatch(oBatch, 1);
        //---------------------------------------

        Test.stopTest();
    }   

}
//--------------------------------------------------------------------------------------------------------------------