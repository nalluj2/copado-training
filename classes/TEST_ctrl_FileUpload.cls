@isTest private class TEST_ctrl_FileUpload {

	static testMethod void uploadTest() {

		//----------------------------------------------------------------------------------------------------------------
		// Create Test Data
		//----------------------------------------------------------------------------------------------------------------
		Blob oBlob = Blob.valueOf('presents a dummy file');
		String tBase64BlobValue = EncodingUtil.base64Encode(oBlob);

		// Create a test accound the 'file' can be attached to.
		Account oAccount = clsTestData_Account.createAccount()[0];

		List<Attachment> lstAttachment = new List<Attachment>();
		Attachment oAttachment1 = new Attachment(
			ParentId = oAccount.Id,
			Body = EncodingUtil.Base64Decode(tBase64BlobValue),
			Name = 'test_file1.txt'
		);
		lstAttachment.add(oAttachment1);

		Attachment oAttachment2 = new Attachment(
			ParentId = oAccount.Id,
			Body = EncodingUtil.Base64Decode(tBase64BlobValue),
			Name = 'test_file2.txt'
		);
		lstAttachment.add(oAttachment2);
		insert lstAttachment;
		//----------------------------------------------------------------------------------------------------------------


		//----------------------------------------------------------------------------------------------------------------
		// Execute Logic
		//----------------------------------------------------------------------------------------------------------------
		Test.startTest();

		// Assert 2 files have been attached to the test account
		lstAttachment = [SELECT Id, Body FROM Attachment WHERE ParentId = :oAccount.Id];
		System.assert(lstAttachment.size() == 2);

		// Delete file 2
		ctrl_FileUpload.deleteAttachment(lstAttachment[0].Id);

		Test.stopTest();
		//----------------------------------------------------------------------------------------------------------------

		//----------------------------------------------------------------------------------------------------------------
		// Validate Result
		//----------------------------------------------------------------------------------------------------------------
		// Assert one file is still attached to the test account
		lstAttachment = [SELECT Id, Body FROM Attachment WHERE ParentId = :oAccount.Id];
		System.assert(lstAttachment.size() == 1);
		//----------------------------------------------------------------------------------------------------------------

	}

}