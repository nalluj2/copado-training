//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 30-03-2017
//  Description      : APEX Class - Business Logic for tr_Asset
//  Change Log       : 
//		- 20/03/2019 - Bart Caelen - Replaced MEACAT by CEMA
//--------------------------------------------------------------------------------------------------------------------
public class bl_Asset_Trigger {


	// If the Asset Status is updated to "Removed" we clear the field Service_Level__c
	public static void processAsset_ClearServiceLevel(List<Asset> lstTriggerNew, Map<Id, Asset> mapTriggerOld){

		for (Asset oAsset : lstTriggerNew){

			Asset oAsset_Old = mapTriggerOld.get(oAsset.Id);

			if ( (oAsset.Status == 'Removed') && (oAsset.Status != oAsset_Old.Status)){

				oAsset.Service_Level__c = null;

			}
		}

	}


	// If the Asset Status is updated to "Removed" we clear the field Service_Level__c on the related Asset Contract Connections
	public static void processAssetContract_ClearServiceLevel(List<Asset> lstTriggerNew, Map<Id, Asset> mapTriggerOld){

		Set<Id> setID_Asset = new Set<Id>();

		// Collect all Asset ID's for which the Status has been changed to 'Removed'
		for (Asset oAsset : lstTriggerNew){

			Asset oAsset_Old = mapTriggerOld.get(oAsset.Id);

			if ( (oAsset.Status == 'Removed') && (oAsset.Status != oAsset_Old.Status)){

				setID_Asset.add(oAsset.Id);

			}
		}

		// Select the Asset Contract Connections related to the collected Asset ID's so that we can clear the field Service_Level__c of these Asset Contract Connections
		List<AssetContract__c> lstAssetContract = [SELECT Id, Service_Level__c FROM AssetContract__c WHERE Asset__c = :setID_Asset AND Service_Level__c != null];
		for (AssetContract__c oAssetContract : lstAssetContract){

			oAssetContract.Service_Level__c = null;

		}

		if (lstAssetContract.size() > 0){

			update lstAssetContract;

		}

	}

	// Don't allow the creation of an Asset by a user if the CFN of the Asset or the CFN of the product of the asset is included in the mapping object CFN_Mapping_Asset__c
	public static void preventCreation(List<Asset> lstTriggerNew){
	
		if (bl_Trigger_Deactivation.isTriggerDeactivated('Asset_preventCreation')) return;

		// Exclude this logic for certain profiles
		Set<Id> setID_Profile = new Set<Id>();
			setID_Profile.add(clsUtil.getUserProfileId('System Administrator'));
			setID_Profile.add(clsUtil.getUserProfileId('System Administrator MDT'));
			setID_Profile.add(clsUtil.getUserProfileId('Devart Tool'));
		if (setID_Profile.contains(UserInfo.getProfileId())){
			if (!Test.isRunningTest()) return;
		}


		// Collect the CFN Code of the processing Assets and the CFN Code of the Products of the processing Assets
		Set<Id> setID_Product= new Set<Id>();
		Set<String> setCFN_Asset = new Set<String>();
		for (Asset oAsset : lstTriggerNew){
			
			//if (!String.isBlank(oAsset.Product2Id)) setID_Product.add(oAsset.Product2Id);
			if (!String.isBlank(oAsset.CFN_Text__c)) setCFN_Asset.add(oAsset.CFN_Text__c);
		}
		
		// Get the CFN Code of the collected Products
		/*Set<String> setCFN_Product = new Set<String>();
		Map<Id, Product2> mapProduct = new Map<Id, Product2>();
		if(setID_Product.size() > 0){

			mapProduct = new Map<Id, Product2>([SELECT Id, CFN_Code_Text__c FROM Product2 WHERE Id = :setID_Product]);
			
			for (Product2 oProduct : mapProduct.values()){
				setCFN_Product.add(oProduct.CFN_Code_Text__c);
			}
		}*/


		// Load the CFN Mapping Asset records based on the collected Asset and Product CFN Codes
		List<CFN_Mapping_Asset__c> lstCFNMappingAsset = 
			[
				SELECT Id, Name 
				FROM CFN_Mapping_Asset__c
				WHERE 
					Name in :setCFN_Asset
					//OR Name in :setCFN_Product
			];
		Set<String> setCFN_Mapping = new Set<String>();
		for (CFN_Mapping_Asset__c oCFNMappingAsset : lstCFNMappingAsset){
			setCFN_Mapping.add(oCFNMappingAsset.Name);
		}


		// Process the Assets and if the CFN Code of the Asset or the CFN Code of the Product of the Asset is in the CFN Mapping Asset we need to throw an error
		String tError = 'You can\'t create an Asset with an CFN Number that is interfaced.';
		for (Asset oAsset : lstTriggerNew){
			
			/*if (!String.isBlank(oAsset.Product2Id)){

				if (setCFN_Mapping.contains(mapProduct.get(oAsset.Product2Id).CFN_Code_Text__c)){
					oAsset.addError(tError);
					continue;
				}
			}*/

			if (!String.isBlank(oAsset.CFN_Text__c)){

				if (setCFN_Mapping.contains(oAsset.CFN_Text__c)){
					oAsset.addError(tError);
					continue;
				}
			}
		}
	}


	//------------------------------------------------------------------------------------------------------------------
	// Populate the CFN_Marketing_Product__c on Asset with MITG CFN level product based on CFN of the asset
	//------------------------------------------------------------------------------------------------------------------
	public static void populateCFNMarketingProduct(List<Asset> lstTriggerNew, Map<Id, Asset> mapTriggerOld){

		//----------------------------------------------------------------------------------------
		// Set the processing RecordTypes and Business Unit Groups
		//----------------------------------------------------------------------------------------
		Set<Id> setID_RecordType = new Set<Id>{clsUtil.getRecordTypeByDevName('Asset', 'General').Id};
		Set<String> setBUG = new Set<String>{'MITG'};
		//----------------------------------------------------------------------------------------


		//----------------------------------------------------------------------------------------
		// Collect the products that are related to the provided Business Unit Group and the Region CEMA (previous MEACAT)
		//----------------------------------------------------------------------------------------
		Set<Id> setID_Product = new Set<Id>();
		for (Asset oAsset : lstTriggerNew){

			// Skipp the record if the RecordType is not included
			if (!setID_RecordType.contains(oAsset.RecordTypeId)) continue;

			setID_Product.add(oAsset.Product2Id);

		}
		List<Product2> lstProduct = 
			[
				SELECT Id 
				FROM Product2 
				WHERE 
					Id = :setID_Product 
					AND Business_Unit_ID__r.Business_Unit_Group__r.Name = :setBUG
					AND Region_vs__c = 'CEMA'
			];
		Map<Id, Product2> mapProduct_Processing = new Map<Id, Product2>();
		mapProduct_Processing.putAll(lstProduct);
		//----------------------------------------------------------------------------------------


		//----------------------------------------------------------------------------------------
		// Collect the Asset records that should be processed
		// Collect the CFN Code of the processing Assets
		//----------------------------------------------------------------------------------------
		List<Asset> lstAsset_Processing = new List<Asset>();
		Map<String, Id> mapCFNCode_CFNProductId = new Map<String, Id>();
		for (Asset oAsset : lstTriggerNew){

			// Skip the record if the RecordType is not included
			if (!setID_RecordType.contains(oAsset.RecordTypeId) || !mapProduct_Processing.containsKey(oAsset.Product2Id)) continue;

			Boolean bProcessRecord = true;
			if (mapTriggerOld != null){

				// Update
				bProcessRecord = false;

				Asset oAsset_Old = mapTriggerOld.get(oAsset.Id);
				if ( 
					(oAsset.CFN_Marketing_Product__c == null) 
					|| (oAsset.CFN_Text__c != oAsset_Old.CFN_Text__c) 
				){
					bProcessRecord = true;
				}

			}

			if (bProcessRecord){

				lstAsset_Processing.add(oAsset);
				mapCFNCode_CFNProductId.put(oAsset.CFN_Text__c, null);
				setID_Product.add(oAsset.Product2Id);

			}
	
		}
		//----------------------------------------------------------------------------------------


		//----------------------------------------------------------------------------------------
		//	Get the CFN Marketing Product based on the collected CFN Codes
		//----------------------------------------------------------------------------------------
		List<Product2> lstProduct_CFN = 
			[
				SELECT Id, CFN_Code_Text__c
				FROM Product2
				WHERE
					RecordTypeId = :clsUtil.getRecordTypeByDevName('Product2', 'MDT_Marketing_Product').Id
					AND CFN_Code_Text__c in :mapCFNCode_CFNProductId.keySet()
					AND Marketing_Product_Level__c = 'CFN'
					AND Region_vs__c = 'CEMA'
			];

		for (Product2 oProduct : lstProduct_CFN){

			mapCFNCode_CFNProductId.put(oProduct.CFN_Code_Text__c, oProduct.Id);

		}
		//----------------------------------------------------------------------------------------


		//----------------------------------------------------------------------------------------

		//----------------------------------------------------------------------------------------
		for (Asset oAsset : lstAsset_Processing){

			if (String.isBlank(oAsset.CFN_Text__c)){

				oAsset.CFN_Marketing_Product__c = null;

			}else{

				if (mapCFNCode_CFNProductId.containsKey(oAsset.CFN_Text__c)){

					Id id_Product = mapCFNCode_CFNProductId.get(oAsset.CFN_Text__c);
					if (id_Product != null){
						oAsset.CFN_Marketing_Product__c = id_Product;
					}

				}

			}

		}
		//----------------------------------------------------------------------------------------

	}
	//------------------------------------------------------------------------------------------------------------------

}
//--------------------------------------------------------------------------------------------------------------------