//---------------------------------------------------------------------------------------------------------------------------------
//	Author 			: Bart Caelen
//  Created Date 	: 06-11-2018
//  Description 	: SCHEDULED BATCH APEX Class
//						Get the Contact_Primary_Specialty__c of the CampaignMembers and update the Campaigns with a concatenation 
//						of the different Contact_Primary_Specialty__c values.
//  Change Log 		: 
//---------------------------------------------------------------------------------------------------------------------------------
global class ba_UpdateSpecialtyOnCampaign implements Database.Batchable<SObject>, Schedulable, Database.Stateful {

	// Campaign Record Types that will be processed
	global Set<Id> setID_RecordType_Campaign = new Set<Id>{
            clsUtil.getRecordTypeByDevName('Campaign', 'Europe_xBU').Id
            , clsUtil.getRecordTypeByDevName('Campaign', 'CVG_Campaign').Id
            , clsUtil.getRecordTypeByDevName('Campaign', 'RTG_Campaign').Id
            , clsUtil.getRecordTypeByDevName('Campaign', 'Diabetes_Campaign').Id
        };

	// Campaign Statusses that will NOT be processed
	global Set<String> setCampaignStatus_Closed = new Set<String>{'Aborted', 'Completed', 'Cancelled'};

	global String tProcessType;

	// Store the different Speciality values per Campaign Id
	global Map<Id, Set<String>> mapCampaignId_Specialty;
	public List<Campaign> lstCampaign_Update;

	private Integer iRecord_Collected;
	private Integer iRecord_Updated;
	private Integer iCampaign_Updated;


	// Schedule APEX 
    global void execute(SchedulableContext ctx){

		ba_UpdateSpecialtyOnCampaign oBatch = new ba_UpdateSpecialtyOnCampaign();
			oBatch.tProcessType = 'COLLECTDATA';
		Database.executeBatch(oBatch, 2000);

    }

	// Batch APEX
	global ba_UpdateSpecialtyOnCampaign(){

		tProcessType = 'COLLECTDATA';

	}
	
	global Database.QueryLocator start(Database.BatchableContext context) {

		String tSOQL;
		if (tProcessType == 'COLLECTDATA'){

			// This logic will populate mapCampaignId_Specialty which will contain the different Specialty values for each Campaign ID
			mapCampaignId_Specialty = new Map<Id, Set<String>>();
			iRecord_Collected = 0;

			tSOQL = 'SELECT CampaignId, ContactId, Contact.Contact_Primary_Specialty__c';
			tSOQL += ' FROM CampaignMember';
			tSOQL += ' WHERE';
			tSOQL += ' Campaign.RecordTypeId in :setID_RecordType_Campaign';
			tSOQL += ' AND Campaign.Status != :setCampaignStatus_Closed';
			tSOQL += ' ORDER BY CampaignId, Contact.Contact_Primary_Specialty__c';
		
			System.debug('**BC** tSOQL COLLECTDATA : ' + tSOQL);

		}else if (tProcessType == 'UPDATEDATA'){

			// This logic will update the Campaigns that are collectec in mapCampaignId_Specialty
			lstCampaign_Update = new List<Campaign>();
			iRecord_Updated = 0;
			iCampaign_Updated = 0;
			
			Set<Id> setID_Campaign_Collected = mapCampaignId_Specialty.keySet();
			System.debug('**BC** setID_Campaign_Collected UPDATEDATA (' + setID_Campaign_Collected.size() + ') : ' + setID_Campaign_Collected);
			tSOQL = 'SELECT Id, Recipient_s_Specialties__c';
			tSOQL += ' FROM Campaign';
			tSOQL += ' WHERE Id in :setID_Campaign_Collected';

			System.debug('**BC** tSOQL UPDATEDATA : ' + tSOQL);

		}

		return Database.getQueryLocator(tSOQL);

	}

   	global void execute(Database.BatchableContext context, List<sObject> lstData){

		if (tProcessType == 'COLLECTDATA'){

			iRecord_Collected += lstData.size();
			for (CampaignMember oCampaignMember : (List<CampaignMember>)lstData){
		
				Set<String> setSpecialty = new Set<String>();
				if (mapCampaignId_Specialty.containsKey(oCampaignMember.CampaignId)){
					setSpecialty = mapCampaignId_Specialty.get(oCampaignMember.CampaignId);
				}

				if (oCampaignMember.Contact.Contact_Primary_Specialty__c != null){
					setSpecialty.add(oCampaignMember.Contact.Contact_Primary_Specialty__c);
				}
				mapCampaignId_Specialty.put(oCampaignMember.CampaignId, setSpecialty);
			}
	
		}else if (tProcessType == 'UPDATEDATA'){

			iRecord_Updated += lstData.size();

			if (mapCampaignId_Specialty != null){
			
				for (Campaign oCampaign : (List<Campaign>)lstData){

					Boolean bDoUpdate = false;
					
					System.debug('**BC** mapCampaignId_Specialty.containsKey(oCampaign.Id) : ' + mapCampaignId_Specialty.containsKey(oCampaign.Id));
					if (mapCampaignId_Specialty.containsKey(oCampaign.Id)){
						
						Set<String> setSpecialty = mapCampaignId_Specialty.get(oCampaign.Id);
						System.debug('**BC** setSpecialty (' + setSpecialty.size() + ') : ' + setSpecialty);
						System.debug('**BC** oCampaign.Recipient_s_Specialties__c 1 : ' + oCampaign.Recipient_s_Specialties__c);
						
						if (setSpecialty.size() > 0){
	
							List<String> lstSpecialty = new List<String>();
							lstSpecialty.addAll(setSpecialty);
							String tReceipientSpecialty = String.join(lstSpecialty, ';');
							
							if (oCampaign.Recipient_s_Specialties__c != tReceipientSpecialty){
								oCampaign.Recipient_s_Specialties__c = tReceipientSpecialty;
								bDoUpdate = true;
							}

	
						}else{

							if (oCampaign.Recipient_s_Specialties__c != null){
								oCampaign.Recipient_s_Specialties__c = null;
								bDoUpdate = true;
							}

						}

					}

					System.debug('**BC** oCampaign.Recipient_s_Specialties__c 2 : ' + oCampaign.Recipient_s_Specialties__c);
					if (bDoUpdate){
						lstCampaign_Update.add(oCampaign);
					}

					// If the DML Row Limit is reached - update the records and re-initalize the list
					if (lstCampaign_Update.size() == Limits.getLimitDmlRows()){
						
						update lstCampaign_Update;
						iCampaign_Updated += lstCampaign_Update.size();
						lstCampaign_Update = new List<Campaign>();

					}
				
				}
			
			}

		}
		
	}
	
	global void finish(Database.BatchableContext context) {

		if (tProcessType == 'COLLECTDATA'){
			
			if (mapCampaignId_Specialty != null && mapCampaignId_Specialty.size() > 0){

				System.debug('**BC** COLLECTDATA mapCampaignId_Specialty (' + mapCampaignId_Specialty.size() + ') : ' + mapCampaignId_Specialty);

				// Don't execute when running TESTS
				if (Test.isRunningTest()) return;

				ba_UpdateSpecialtyOnCampaign oBatch = new ba_UpdateSpecialtyOnCampaign();
					oBatch.mapCampaignId_Specialty = mapCampaignId_Specialty;
					oBatch.tProcessType = 'UPDATEDATA';
				Database.executeBatch(oBatch, 2000);

			}
		
		}else if (tProcessType == 'UPDATEDATA'){

			if (lstCampaign_Update.size() > 0){
						
				update lstCampaign_Update;
				iCampaign_Updated += lstCampaign_Update.size();

			}

			String tEmailBody = '';
		
			tEmailBody += 'Total Campaigns Updated : ' + iCampaign_Updated + ' <br/>';
			tEmailBody += 'Total Records Collected : ' + iRecord_Collected + ' <br/>';
			tEmailBody += 'Total Records Updated : ' + iRecord_Updated + ' <br/>';
		
			Messaging.SingleEmailMessage oEmail = new Messaging.SingleEmailMessage();
				oEmail.setTargetObjectId(UserInfo.getUserId());
				oEmail.setSaveAsActivity(false);
				oEmail.setSubject('ba_UpdateSpecialtyOnCampaign results ' + Date.today().format());
				oEmail.setHTMLBody(tEmailBody);
			Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{oEmail});		
		
		}
				
	}

}
//---------------------------------------------------------------------------------------------------------------------------------