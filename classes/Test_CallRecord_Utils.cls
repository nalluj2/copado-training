@isTest
public with sharing class Test_CallRecord_Utils {
	
	public static void initializeCallRecordRelatedData(){
		
		//Master Data
		Company__c cmpny = new Company__c();		
        cmpny.name='Europe';
        cmpny.CurrencyIsoCode = 'EUR';
        cmpny.Current_day_in_Q1__c=56;
        cmpny.Current_day_in_Q2__c=34; 
        cmpny.Current_day_in_Q3__c=5; 
        cmpny.Current_day_in_Q4__c= 0;   
        cmpny.Current_day_in_year__c =200;
        cmpny.Days_in_Q1__c=56;  
        cmpny.Days_in_Q2__c=34;
        cmpny.Days_in_Q3__c=13;
        cmpny.Days_in_Q4__c=22;
        cmpny.Days_in_year__c =250;
        cmpny.Company_Code_Text__c = 'EUR';
		insert cmpny;
		
		//Business Unit
		Business_Unit__c bu =  new Business_Unit__c();
        bu.Company__c =cmpny.id;
        bu.name='BUMedtronic Account Performance';
        bu.Account_Plan_Activities__c=true;        
        insert bu;
        
        //Sub-Business Unit
        Sub_Business_Units__c sbu = new Sub_Business_Units__c();
        sbu.name='SBUMedtronic1 Account Performance1236';
        sbu.Business_Unit__c=bu.id;
        sbu.Account_Plan_Default__c='Revenue';
        sbu.Account_Flag__c = 'AF_Solutions__c';
		insert sbu;	
		
		//User business Units
		User_Business_Unit__c uSBU = new User_Business_Unit__c();
		uSBU.User__c = UserInfo.getUserId();
		uSBU.Primary__c = true;
		uSBU.Sub_Business_Unit__c = sbu.Id;
		insert uSBU;
		
		//Activity Type
		Call_Activity_Type__c actType = new Call_Activity_Type__c();
		actType.Name = 'Test Activity';
		actType.Active__c = true;
		actType.Company_ID__c = cmpny.Id;		
		insert actType;
		
		//Category
		Call_Category__c cat = new Call_Category__c();
		cat.Company_ID__c = cmpny.Id;
		cat.Name = 'Test Category';		
		insert cat;
		
		//Activity Type - Business Unit
		Call_Topic_Settings__c setting = new Call_Topic_Settings__c();
		setting.Business_Unit__c = bu.Id;
		setting.Call_Activity_Type__c = actType.Id;
		setting.Call_Category__c = cat.Id;
		setting.Region_vs__c = 'Europe';
		insert setting;
		
		//Therapy Group
		Therapy_Group__c therGroup = new Therapy_Group__c();
		therGroup.Name = 'Test Therapy Group';
		therGroup.Company__c = cmpny.Id;
		therGroup.Sub_Business_Unit__c = sbu.Id;
		insert therGroup;
		
		//Therapy
		Therapy__c therapy = new Therapy__c();
		therapy.Active__c = true;
		therapy.Business_Unit__c = bu.Id;
		therapy.Name = 'Test Therapy';
		therapy.Sub_Business_Unit__c = sbu.Id;
		therapy.Therapy_Group__c = therGroup.Id;
		therapy.Therapy_Name_Hidden__c = 'whatever';
		insert therapy;
		
		//Subject
		Subject__c subject = new Subject__c();
		subject.Company_ID__c = cmpny.Id;
		subject.Name = 'Test subject';
		insert subject;
				
		//Activity Type Subject
		Call_topic_Setting_Details__c actTypeSubject = new Call_topic_Setting_Details__c();
		actTypeSubject.Call_Topic_Setting__c = setting.Id;
		actTypeSubject.Subject__c = subject.Id;
		insert actTypeSubject;
		
		//Product
		Product2 product = new Product2();
		product.Name = 'UnitTest Product';
		product.RecordTypeId = [Select Id from RecordType where sObjectType='Product2' AND developerName = 'MDT_Marketing_Product' AND isActive=true].Id;
		insert product;
		
		//Therapy Product
		Therapy_Product__c therProduct = new Therapy_Product__c();
		therProduct.Product__c = product.Id;
		therProduct.Therapy__c = therapy.Id;
		insert therProduct;		
	}
	
	public static void initializeXBUCallRecordRelatedData(RecordType xbuRT){
		
		//Master Data
		Company__c cmpny = new Company__c();		
        cmpny.name='Europe';
        cmpny.CurrencyIsoCode = 'EUR';
        cmpny.Current_day_in_Q1__c=56;
        cmpny.Current_day_in_Q2__c=34; 
        cmpny.Current_day_in_Q3__c=5; 
        cmpny.Current_day_in_Q4__c= 0;   
        cmpny.Current_day_in_year__c =200;
        cmpny.Days_in_Q1__c=56;  
        cmpny.Days_in_Q2__c=34;
        cmpny.Days_in_Q3__c=13;
        cmpny.Days_in_Q4__c=22;
        cmpny.Days_in_year__c =250;
        cmpny.Company_Code_Text__c = 'EUR';
		insert cmpny;
		
		//Business Unit
		Business_Unit__c bu =  new Business_Unit__c();
        bu.Company__c =cmpny.id;
        bu.name='BUMedtronic Account Performance';
        bu.Account_Plan_Activities__c=true;        
        insert bu;
						
		//Activity Type
		Call_Activity_Type__c actType = new Call_Activity_Type__c();
		actType.Name = 'Test Activity';
		actType.Active__c = true;
		actType.Company_ID__c = cmpny.Id;		
		insert actType;
		
		//Category
		Call_Category__c cat = new Call_Category__c();
		cat.Company_ID__c = cmpny.Id;
		cat.Name = 'Test Category';		
		insert cat;
		
		//Activity Type - Business Unit
		Call_Topic_Settings__c setting = new Call_Topic_Settings__c();
		setting.Call_Record_Record_Type__c = String.valueOf(xbuRT.Id).substring(0, 15);
		setting.Call_Activity_Type__c = actType.Id;
		setting.Call_Category__c = cat.Id;
		setting.Region_vs__c = 'Europe';
		insert setting;
						
		//Subject
		Subject__c subject = new Subject__c();
		subject.Company_ID__c = cmpny.Id;
		subject.Name = 'Test subject';
		insert subject;
				
		//Activity Type Subject
		Call_topic_Setting_Details__c actTypeSubject = new Call_topic_Setting_Details__c();
		actTypeSubject.Call_Topic_Setting__c = setting.Id;
		actTypeSubject.Subject__c = subject.Id;
		insert actTypeSubject;	
	}
}