//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 08-04-2016
//  Description      : APEX Class - Business Logic for tr_Lead
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public class bl_Lead_Trigger {

    //--------------------------------------------------------------------------------------------------------------------
    // Public variables coming from the trigger
    //--------------------------------------------------------------------------------------------------------------------
    public static List<Lead> lstTriggerNew = new List<Lead>();
    public static Map<Id, Lead> mapTriggerNew = new Map<Id, Lead>();
    public static Map<Id, Lead> mapTriggerOld = new Map<Id, Lead>();
    //--------------------------------------------------------------------------------------------------------------------
    

    //--------------------------------------------------------------------------------------------------------------------
    // Add the Lead to a Campaign
    //  MITGTODO
    //      - define addtional filtering for which Lead this logic should be executed (if needed)
    //--------------------------------------------------------------------------------------------------------------------
    public static void addLeadToCampaign_Insert(){

        List<CampaignMember> lstCampaignMember = new List<CampaignMember>();
        
        for (Lead oLead : lstTriggerNew){

            // Retrieve from LeadTrigger
            if (oLead.Campaign_Most_Recent__c != null){
                CampaignMember oCampaignMember = new CampaignMember();
                    oCampaignMember.LeadId = oLead.Id;
                    oCampaignMember.CampaignId = oLead.Campaign_Most_Recent__c;
                lstCampaignMember.add(oCampaignMember);
            }

        }

        if (lstCampaignMember.size() > 0){
            insert lstCampaignMember;
        }
    }
    //--------------------------------------------------------------------------------------------------------------------


    //--------------------------------------------------------------------------------------------------------------------
    // MITG - Set Account Affiliation
    //  MITGTODO
    //      - define addtional filtering for which Lead this logic should be executed (if needed)
    //--------------------------------------------------------------------------------------------------------------------
    public static void setAccountAffiliation(){

        Set<String> setID_EloquaAccountId = new Set<String>();
        Map<String, Account> mapAccount = new Map<String, Account>();
        
        for(Lead oLead : lstTriggerNew){

            if (clsUtil.isNull(oLead.Eloqua_Account_ID__c, '') != ''){
                setID_EloquaAccountId.add(oLead.Eloqua_Account_ID__c);
            }

        }

        for (Account oAccount : [SELECT Id, Eloqua_Account_ID__c FROM Account WHERE Eloqua_Account_ID__c IN :setID_EloquaAccountId]){
            mapAccount.put(oAccount.Eloqua_Account_ID__c, oAccount);
        }

        for (Lead oLead : lstTriggerNew){
            
            if (clsUtil.isNull(oLead.Eloqua_Account_ID__c, '') == ''){

                // Eloqua_Account_ID__c is NULL so the AccountAffiliation__c should ALSO be NULL
                if (oLead.AccountAffiliation__c != null){
                    oLead.AccountAffiliation__c = null;
                }

            }else if (mapAccount.containsKey(oLead.Eloqua_Account_ID__c)){

                // Eloqua_Account_ID__c is NOT NULL so the AccountAffiliation__c should be equal to the corresponding Account Id
                Id id_Account = mapAccount.get(oLead.Eloqua_Account_ID__c).Id;

                if (oLead.AccountAffiliation__c != id_Account){
                    oLead.AccountAffiliation__c = id_Account;
                }               

            }

        }

    }
    //--------------------------------------------------------------------------------------------------------------------


    //--------------------------------------------------------------------------------------------------------------------
    // Copy the field Salutation to the field Salutation_Text__c when 
    //  - a lead is created and the Salutation_Text__c is empty
    //  - a lead is updated and the Salutation of the lead is changed
    //--------------------------------------------------------------------------------------------------------------------
    public static void copyLeadSalutation(String tAction){

        if (tAction == 'UPDATE'){

            for (Lead oLead : lstTriggerNew){
                Lead oLead_Old = mapTriggerOld.get(oLead.Id);

                if (oLead.Salutation != oLead_Old.Salutation){
                    oLead.Salutation_Text__c = oLead.Salutation;
                    oLead.Salutation = null;                            
                }
            }

        }else if (tAction == 'INSERT'){

            for (Lead oLead : lstTriggerNew){
                if (clsUtil.isBlank(oLead.Salutation_Text__c)){
                    oLead.Salutation_Text__c = oLead.Salutation;
                    oLead.Salutation = null;
                }
            }
        }
    }
    //--------------------------------------------------------------------------------------------------------------------
}
//--------------------------------------------------------------------------------------------------------------------