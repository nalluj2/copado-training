@RestResource(urlMapping='/BoxBuilderProduct2Service/*')
global without sharing class ws_ProductCountryAvailabilityService {
	
	@HttpPost
	global static String doPost() {
		
		RestRequest req = RestContext.request;
		
		System.savePoint sp = Database.setSavepoint();
		
		ProdCountryAvailabilityResponse response = new ProdCountryAvailabilityResponse();
		
		try{
							
			System.debug('post called '+ req);
			System.debug('post requestBody '+ req.requestBody.toString());
				
			String body = req.requestBody.toString();
			ProdCountryAvailabilityRequest request = (ProdCountryAvailabilityRequest) System.Json.deserialize(body, ProdCountryAvailabilityRequest.class);
				
			System.debug('post requestBody ' + request);
			
			response.id = request.id;
				
			Product_Country_Availability__c prodCountry = request.productCountryAvailability;
			
			List<Product2> prods = [Select Id, KitableIndex__c from Product2 where Id = :prodCountry.Product__c];			
			if(prods.size() != 1) throw new ws_Exception('Invalid Product Id. No record or more than one record found');
			
			Product2 prod = prods[0];
			
			if(prod.KitableIndex__c == null || prod.KitableIndex__c == '' ||  prod.KitableIndex__c == 'Not Approved'){
				
				prod.KitableIndex__c = 'Approval Pending';
				update prod;
				
			}else if(prod.KitableIndex__c == 'Request Rejected') throw new ws_Exception('The Product has been rejected and cannot be requested again.');
			
			String uniqueKey = prodCountry.Product__c + ':' + prodCountry.Country__c;
			
			List<Product_Country_Availability__c> prodCountryAvailabilities = [Select Id, Product__c, Country__c, Unique_Key__c, Status__c from Product_Country_Availability__c where Product__c = :prodCountry.Product__c AND Country__c = :prodCountry.Country__c];
			
			if(prodCountryAvailabilities.size() == 0 || prodCountryAvailabilities[0].Status__c == 'Not Approved'){
      		
      			prodCountry.Status__c = 'Approval Pending';
      			prodCountry.Unique_Key__c = uniqueKey;
      
     	 		upsert prodCountry Unique_Key__c;
     	 		
     	 		response.success = true;
				response.productCountryAvailability = prodCountry;
     	 		
			}else if(prodCountryAvailabilities[0].Status__c == 'Request Rejected') throw new ws_Exception('The Product has been rejected for the selected Country and cannot be requested again.');
			else{
			
				response.success = true;
				response.productCountryAvailability = prodCountryAvailabilities[0];
			}
		
		}catch(Exception e){
			
			Database.rollback(sp);
			
			response.success = false;
			response.message = e.getMessage();
		}
		
		bl_Mobilesync.storeRequest(UserInfo.getUserId(), 'ProductCountryAvailability', req.requestBody.toString(), System.JSON.serializePretty(response), response.success);
  			
		return System.JSON.serialize(response);
	}
	
	global class ProdCountryAvailabilityRequest{
		
		public Product_Country_Availability__c productCountryAvailability {get;set;}
		public String id{get;set;}
	}
	
	global class ProdCountryAvailabilityResponse{
		
		public Product_Country_Availability__c productCountryAvailability {get;set;}
		public String id{get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}   
}