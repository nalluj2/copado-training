public with sharing class ctrlExt_Opportunity_BU_Involved {
    
    private Opportunity_BU_Involved__c oppBU {get; set;}
    
    public ctrlExt_Opportunity_BU_Involved(ApexPages.StandardController sc){
    	
    	oppBU = (Opportunity_BU_Involved__c) sc.getRecord();
    }
    
    public PageReference redirectToManageOppBU(){
    	
    	Id oppId = oppBU.Opportunity__c;
    	
    	if(oppId == null){
    		
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Opportunity BU Involved can only be managed from the Opportunity detail page.'));
    		return null;
    	}
    	
    	PageReference pr = new PageReference('/apex/Manage_Opportunity_BU_Involved');
    	pr.getParameters().put('id', oppId);
    	pr.setRedirect(true);
    	return pr;
    }
}