@isTest
private class Test_OpportunityCompetitor {

    static testMethod void myUnitTest() {
      
             Company__c cmpny = new Company__c();
             cmpny.name='TestMedProd';
             cmpny.CurrencyIsoCode = 'EUR';
             cmpny.Current_day_in_Q1__c=56;
             cmpny.Current_day_in_Q2__c=34; 
             cmpny.Current_day_in_Q3__c=5; 
             cmpny.Current_day_in_Q4__c= 0;   
             cmpny.Current_day_in_year__c =200;
             cmpny.Days_in_Q1__c=56;  
             cmpny.Days_in_Q2__c=34;
             cmpny.Days_in_Q3__c=13;
             cmpny.Days_in_Q4__c=22;
             cmpny.Days_in_year__c =250;
             cmpny.Company_Code_Text__c = 'T27';
             insert cmpny;
             
             Business_Unit__c BU1 = new Business_Unit__c();
             BU1.Name='TestBUMedProd';
             BU1.Company__c = cmpny.Id;
             Insert BU1;

             Sub_Business_Units__c SBU = new Sub_Business_Units__c();
             SBU.Name='TestSBUMedProd';
             SBU.Business_Unit__c = BU1.Id;
             Insert SBU;         
    
             Therapy_Group__c TG=new Therapy_Group__c();
             TG.Name='TestTGMedProd';
             TG.Sub_Business_Unit__c = SBU.id;
             TG.Company__c = cmpny.Id;
             insert TG;    
            
             Therapy__c th = new Therapy__c();
             th.Name = 'th11';
             th.Therapy_Group__c = TG.Id;
             th.Sub_Business_Unit__c = SBU.id;
             th.Business_Unit__c = BU1.id;
             th.Therapy_Name_Hidden__c = 'th11'; 
             insert th;
             
             RecordType oppRecType=[select id,name from Recordtype where sobjecttype='Account' and IsActive=true and name='Competitor'];
             List<Account> lstAccount=new list<Account>();
             Account testAc1 = new Account() ;
             testAc1.Name = 'Test Account Name1';  
             testAc1.RecordTypeId = oppRecType.Id ;
             testAc1.Account_Country_vs__c='BELGIUM';
             lstAccount.add(testAc1);
             Account testAc2 = new Account() ;
             testAc2.Name = 'Test Account Name2';  
             testAc2.RecordTypeId = oppRecType.Id ;
             testAc2.Account_Country_vs__c='BELGIUM';
             lstAccount.add(testAc2);
             insert lstAccount;
                          
			 Product2 prd = new Product2(Name = 'Unit Test Product');
             insert prd;
            
             Opportunity_Bundle__c opb=new Opportunity_Bundle__c(Account__c=testAc1.id);
             insert opb;
            
             opportunity op=new opportunity(name='testopp',Deal_Category__c='Upgrade',Opportunity_Bundle__c=opb.id,CloseDate=system.today()+2,stagename='Qualification',accountid=testAc1.id,ForecastCategoryName='Pipeline',CurrencyIsoCode='EUR',type='Service Contract');
             insert op;
             
             Opportunity_Competitor__c oc=New Opportunity_Competitor__c();
             oc.Competitor__c=lstAccount[0].id;
             oc.Opportunity__c=op.id;
             insert oc;
             delete oc;
             }
             
}