//------------------------------------------------------------------------------------------------------------------------------
//  Author      : Bart Caelen
//  Date        : 20150326
//  Description : CR-7212
//                  TEST Class for bl_User
//------------------------------------------------------------------------------------------------------------------------------
//  Author      : Bart Caelen
//  Date        : 20150408
//  Description : CR-8360
//                  TEST Class for bl_User
//------------------------------------------------------------------------------------------------------------------------------
//  Author      : Bart Caelen
//  Date        : 20151022
//  Description : CR-8006
//                  TEST Class for bl_User.updateUserFields

//------------------------------------------------------------------------------------------------------------------------------
@isTest (seeAllData=TRUE)
private class Test_bl_User {

	private static testmethod void test_processUserLastLoginReminder() {
	
		//----------------------------------------------------
		// Create Test Data
		//----------------------------------------------------
		List<User_Last_Login_Reminder__c> lstUserLastLoginReminder = new List<User_Last_Login_Reminder__c>();
		User_Last_Login_Reminder__c oULLR1 = new User_Last_Login_Reminder__c();
			oULLR1.Name = 'TEST 1';
			oULLR1.Country_Name__c = 'GERMANY';
			oULLR1.Profile_ID__c = '00e20000000nIqQAAU';    
			oULLR1.Inactive_Days__c = 45;
			oULLR1.Additional_CC__c = 'cc@medtronic.com';
			oULLR1.Additional_BCC__c = 'bcc@medtronic.com';
			oULLR1.Notify_Manager__c = true;
			oULLR1.Email_Template_Developer_Name__c = 'User_Last_Login_Reminder_CANADA_45';
			oULLR1.Organization_Wide_Email_Address__c = 'sfe@medtronic.com';
		lstUserLastLoginReminder.add(oULLR1);    
		User_Last_Login_Reminder__c oULLR2 = new User_Last_Login_Reminder__c();
			oULLR2.Name = 'TEST 2';
			oULLR2.Profile_ID__c = '00e20000000nIqQAAU';
			oULLR2.Inactive_Days__c = 25;
			oULLR2.Additional_CC__c = 'cc@medtronic.com';
			oULLR2.Additional_BCC__c = 'bcc@medtronic.com';
			oULLR2.Notify_Manager__c = true;
			oULLR2.Email_Template_Developer_Name__c = 'User_Last_Login_Reminder_EUR_SB_25';
			oULLR2.Organization_Wide_Email_Address__c = 'sfe@medtronic.com';
		lstUserLastLoginReminder.add(oULLR2);    
		User_Last_Login_Reminder__c oULLR3 = new User_Last_Login_Reminder__c();
			oULLR3.Name = 'TEST 3';
			oULLR3.Profile_ID__c = '00e20000000nIqQAAU';
			oULLR3.Inactive_Days__c = 60;
			oULLR3.Additional_CC__c = 'cc@medtronic.com';
			oULLR3.Additional_BCC__c = 'bcc@medtronic.com';
			oULLR3.Notify_Manager__c = true;
			oULLR3.Email_Template_Developer_Name__c = 'User_Last_Login_Reminder_EUR_SB_60';
			oULLR3.Organization_Wide_Email_Address__c = 'sfe@medtronic.com';
		lstUserLastLoginReminder.add(oULLR3);    
		insert lstUserLastLoginReminder;

		List<User> lstUser = 
		[
			SELECT Id, Last_Application_Login__c, Country, Manager.Email
			FROM User
			WHERE 
				IsActive = TRUE
				AND Last_Application_Login__c != null
				AND License_Name__c = 'Salesforce'
				AND Country = 'GERMANY'
				AND Profile.Name like 'EUR%'
			LIMIT 50
		];
		//----------------------------------------------------


		//---------------------------------------
		// TEST
		//---------------------------------------
		clsUtil.debug('lstUser.size() : ' + lstUser.size());
		for (User oUser : lstUser){
			oUser.Last_Application_Login__c = Date.today().addDays(-45);
		}
		update lstUser;
		
		Test.startTest();
		bl_User.processUserLastLoginReminder(lstUser, oULLR1);
		Test.stopTest();
		//---------------------------------------

	}

	private static testmethod void test_bl_User_updateUserFields() {

		//----------------------------------------------------
		// Create/SELECT Test Data
		//----------------------------------------------------
		List<User> lstUser = 
			[
				SELECT Id, Company_Code_Text__c, Country_vs__c
				FROM User
				WHERE 
					IsActive = TRUE
					AND License_Name__c = 'Salesforce'
					AND
					(
						Country_vs__c = 'GERMANY'
						OR 
						Country_vs__c = null
					)
					AND Profile.Name like 'EUR%'
				LIMIT 50
			];
		
		system.assertNotEquals(lstUser.size(), 0);
		//----------------------------------------------------
		
		
		//---------------------------------------   
		// TEST
		//---------------------------------------
		List<String> lstCountry = clsUtil.getUserCountry('NWE');
		for (User oUser : lstUser){
			oUser.Company_Code_Text__c = 'USA';
			oUser.Region_vs__c = 'Europe';
			oUser.Sub_Region__c = 'NWE';
			oUser.Country_vs__c = lstCountry[0];
		}
		update lstUser;
		
		Test.startTest();
			bl_User.updateUserFields(lstUser, true);
		Test.stopTest();
		
		lstUser = 
			[
				SELECT Id, Company_Code_Text__c, Country_vs__c
				FROM User
				WHERE 
					id = :lstUser
			];
		for (User oUser : lstUser){
			System.assertEquals(oUser.Company_Code_Text__c.toUpperCase(), 'EUR');
		}
		//---------------------------------------
	}    

}
//------------------------------------------------------------------------------------------------------------------------------