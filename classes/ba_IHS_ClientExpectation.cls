//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 13-02-2019
//  Description      : BATCH APEX Class related to IHS Opportunity + Client_Expectation__c
//  Change Log       : 
//      Update the At_Risk__c field on Opportunity based on these rules (only for IHS Managed Service Opportunities and EMEA IHS Consulting Opportunity):
//          At_Risk__c = TRUE
//              when 50% or more of the related Client Expectation records with Priority = YES have an Expectation Ready Date < Current Date
//              OR when 50% or more of ALL related Client Expectation records have an Expectation Ready Date < Current Date
//          Otherwise At_Risk__c = false
//--------------------------------------------------------------------------------------------------------------------
global class ba_IHS_ClientExpectation implements Schedulable, Database.Batchable<SObject>, Database.Stateful {
    
    global Set<String> setOpportunityStage = new Set<String>{'4a - Closed Won (Service Activation)', '4b - Closed Won (Service Delivery)'};
    global Set<String> setOpportunityRecordTypeDevName = new Set<String>{'EMEA_IHS_Managed_Services', 'EMEA_IHS_Consulting_Opportunity'};
    global String tSOQL;
	global String tAdditionalWhere;

    global String tError = '';
    global String tSuccess = '';
    global String tNotUpdated = '';


    // SCHEDULE Settings
    global void execute(SchedulableContext sc){
        ba_IHS_ClientExpectation oBatch = new ba_IHS_ClientExpectation();
        Database.executebatch(oBatch, 200);         
    } 
    

    // BATCH Settings
    global Database.QueryLocator start(Database.BatchableContext context) {

        if (String.isBlank(tSOQL)) tSOQL = tBuildSOQL();
        System.debug('**BC** tSOQL : ' + tSOQL);
        return Database.getQueryLocator(tSOQL);
    
    }

    global void execute(Database.BatchableContext context, List<Opportunity> lstOpportunity) {
    
        List<Opportunity> lstOpportunity_Update = new List<Opportunity>();

        for (Opportunity oOpportunity : lstOpportunity){

            if (oOpportunity.Client_Expectations__r.size() == 0){

                if (oOpportunity.At_Risk__c == true){
                    oOpportunity.At_Risk__c = false;
                    lstOpportunity_Update.add(oOpportunity);
                }
            
            }else{
            
                Decimal decRec_Priority_OldDate = 0;
                Decimal decRec_Priority_ALL = 0;

                Decimal decRec_OldDate = 0;
                Decimal decRec_ALL = 0;

                for (Client_Expectation__c oClientExpectation : oOpportunity.Client_Expectations__r){
            
                    if (oClientExpectation.Priority__c == 'Yes'){
                        decRec_Priority_ALL++;
                        if (oClientExpectation.Expectation_Ready_Date__c < Date.today()){
                            decRec_Priority_OldDate++;
                        }
                
                    }

                    decRec_ALL++;
                    if (oClientExpectation.Expectation_Ready_Date__c < Date.today()){
                        decRec_OldDate++;
                    }

                }

                Boolean bAtRisk = false;
                if (decRec_Priority_ALL > 0){
                    
                    Decimal decResult = decRec_Priority_OldDate / decRec_Priority_ALL;
                    if (decResult >= 0.5) bAtRisk = true;

                }

                if (bAtRisk == false){
                
                    Decimal decResult = (decRec_OldDate / decRec_ALL).setScale(2);
                    if (decResult >= 0.5) bAtRisk = true;

                }


                if (oOpportunity.At_Risk__c != bAtRisk){

                    oOpportunity.At_Risk__c = bAtRisk;
                    lstOpportunity_Update.add(oOpportunity);

                }else{
                
                    tNotUpdated += '<br/>Update not needed on field "At Risk" ("' + String.valueOf(oOpportunity.At_Risk__c) + '") for Opportunity <a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + oOpportunity.Id + '">' + oOpportunity.Name + '</a><br/>';
                }

            }

        }

        if (lstOpportunity_Update.size() > 0){
        
            List<Database.SaveResult> lstSaveResult_Update = Database.update(lstOpportunity_Update, false);

            Integer iCounter = 0;
            for (Database.SaveResult oSaveResult : lstSaveResult_Update){
                
                Opportunity oOpportunity = lstOpportunity_Update[iCounter];
                
                if (!oSaveResult.isSuccess()){              
                    
                    String tErrorDescription = '';
                    for (Database.Error oError : oSaveResult.getErrors()){
                        tErrorDescription += oError.getMessage() + '; ';
                    }
                    tError += '<br/>Failed to update the "At Risk" value to "' + String.valueOf(oOpportunity.At_Risk__c) + '" for Opportunity <a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + oOpportunity.Id + '">' + oOpportunity.Name + '</a> : ' + tErrorDescription + '<br/>';

                }else{
                    tSuccess += '<br/>Sucessfull update the "At Risk" value to "' + String.valueOf(oOpportunity.At_Risk__c) + '" for Opportunity <a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + oOpportunity.Id + '">' + oOpportunity.Name + '</a><br/>';
                }
                
                iCounter++;
            }

        }

    }
    
    global void finish(Database.BatchableContext context) {

        String tEmailBody = '';

        if (tError != '' || tSOQL != null){ 
        
            tEmailBody = 'Dear ' + UserInfo.getFirstName() + ' ' + UserInfo.getLastName() + '<br/><br/>';
            
            if (tError != '') tEmailBody += '<br/><b><u>Error</u></b><br/><hr/>' + tError + '<hr/>';
            if (tSuccess != '') tEmailBody += '<br/><b><u>Success</u></b><br/><hr/>' + tSuccess + '<hr/>';
            if (tNotUpdated!= '') tEmailBody += '<br/><b><u>Not Updated</u></b><br/><hr/>' + tNotUpdated + '<hr/>';

            Messaging.SingleEmailMessage oSingleEmailMessage = new Messaging.SingleEmailMessage();
                oSingleEmailMessage.setHtmlBody(tEmailBody);
                oSingleEmailMessage.setTargetObjectId(UserInfo.getUserId());
                oSingleEmailMessage.setSubject('ba_IHS_ClientExpectation - Overview');
                oSingleEmailMessage.setSaveAsActivity(false);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { oSingleEmailMessage });        

        }

    }


    private String tBuildSOQL(){

        tSOQL = 'SELECT Id, Name, At_Risk__c';
        tSOQL += ', (SELECT Id, Priority__c, Expectation_Ready_Date__c FROM Client_Expectations__r WHERE Expectation_Ready_Date__c != null)';
        tSOQL += ' FROM Opportunity';
        String tWhere = '';

        String tSOQL_Tmp = '';
        for (String tRecordTypeDevName : setOpportunityRecordTypeDevName){
            if (!String.isBlank(tSOQL_Tmp)) tSOQL_Tmp += ',';
            tSOQL_Tmp += '\'' + tRecordTypeDevName + '\'';
        }
        if (!String.isBlank(tSOQL_Tmp)){
            tWhere += ' (RecordType.DeveloperName in (' + tSOQL_Tmp + '))';
        }

        tSOQL_Tmp = '';
        for (String tStage : setOpportunityStage){
            if (!String.isBlank(tSOQL_Tmp)) tSOQL_Tmp += ',';
            tSOQL_Tmp += '\'' + tStage + '\'';
        }
        if (!String.isBlank(tSOQL_Tmp)){
            if (!String.isBlank(tWhere)) tWhere += ' AND ';
            tWhere += ' (StageName in (' + tSOQL_Tmp + '))';
        }

        if (!String.isBlank(tWhere)){
			tSOQL += ' WHERE ' + tWhere;
			if (!String.isBlank(tAdditionalWhere)) tSOQL += ' AND ' + tAdditionalWhere;
		}

        tSOQL += ' ORDER BY Id';
    
        return tSOQL;

    }

}
//--------------------------------------------------------------------------------------------------------------------