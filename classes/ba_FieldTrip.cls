/**
 * Created by dijkea2 on 26-9-2016.
 */

global class ba_FieldTrip implements Database.Batchable<SObject>, Database.Stateful {
    
    public String query;
    public String queryFilter;
      
    private Integer countRecords = 0;
    global Map<String, Integer> fieldMasterMap;    
    private String sObjectName;
    
    private Map<String, String> fieldTypeMap;

    global ba_FieldTrip(String objectName){
        
		sObjectName = objectName;
    }
	  
    global Database.QueryLocator start(Database.BatchableContext ctx){
        
        fieldMasterMap = new Map<String, Integer>();
        fieldTypeMap = new Map<String, String>();
                
        Map<String, Schema.SObjectField> mfields = Schema.getGlobalDescribe().get(sObjectName).getDescribe().fields.getMap();
        
        List<String> customFields = new List<String>();
        
        for(String fieldName : mfields.keySet()){
            
            Schema.DescribeFieldResult dfr = mfields.get(fieldName).getDescribe();
                        
            if(dfr.isCustom() && fieldName.endsWith('__c')){
            	
                customFields.add(fieldName);
                
                fieldTypeMap.put(dfr.getName(), String.valueOf(dfr.getType()));
                fieldMasterMap.put(dfr.getName(),0);                
            }
        }
        
        query = 'SELECT Id, ';
        query += String.join(customFields, ',');
        query += ' FROM ' + sObjectName;
        
        if(queryFilter != null) query += ' ' + queryFilter;
        
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext ctx, List<SObject> resultList){

        for(Sobject  s : resultList){

            countRecords++;

            Map<String, Object> fieldsToValue = s.getPopulatedFieldsAsMap();

            for(String field : fieldsToValue.keySet()){
				
				if(fieldMasterMap.containsKey(field)){
				
					String fieldType = fieldTypeMap.get(field);
					
					if(fieldType == 'BOOLEAN'){
						
						Boolean value = Boolean.valueOf(fieldsToValue.get(field));
						
						if(value == true){
							
							Integer c = fieldMasterMap.get(field);
	                		fieldMasterMap.put(field, c + 1);
						}
						
					}else{				
									                
	                	Integer c = fieldMasterMap.get(field);
	                	fieldMasterMap.put(field, c + 1);
					}                
				}
            }
        }
    }
    
    global void finish(Database.BatchableContext BC){

        AsyncApexJob a =[
                SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Id,CompletedDate
                FROM AsyncApexJob
                WHERE Id = :BC.getJobId()
        ];
        
        Integer errorcount = 0;

        String intro = 'Field trip is executed and has status: ' + a.Status;
        String msg = 'The batch apex job processed ' + a.TotalJobItems +
                ' batches with ' + a.NumberOfErrors + ' failures.';

        msg += '<br/>Total number of records: ' + countRecords;
                             
        Decimal total = Decimal.valueOf(countRecords);
        
        List<String> lineList = new List<String>();
        Set<String> searchSet = new Set<String>();

        for(String key : fieldMasterMap.keySet()){

            List<String> lineValueList = new List<String>();
            
            //Collumn I
            lineValueList.add(key);
            
            //Collumn II                
            lineValueList.add(fieldTypeMap.get(key));
            
            //Collumn III            
            lineValueList.add( String.valueOf(fieldMasterMap.get(key)) );
                        
            //Collumn IV
            lineValueList.add( String.valueOf(total) );
            
            //Collumn V
            Decimal counterOn =  Decimal.valueOf(fieldMasterMap.get(key));
            Double percentage =  ((counterOn / total) * 100).setScale(2);
            lineValueList.add( String.valueOf(percentage) );

            if(percentage <= 0.05 ){
                searchSet.add(key);
            }

            lineList.add(String.join(lineValueList, ','));
        }
 
        String body = 'Field Name,Type,Populated On, No ofrecords, Populated Percentage \n' + String.join(lineList, '\n');
        Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
        blob csvBlob = Blob.valueOf(body);
        String suffix = Datetime.now().format('yyyyMMdd_HH:mm:ss');
        string csvname= 'Fieldtrip_'+ sObjectName + '_' + suffix + '.csv';
        csvAttc.setFileName(csvname);
        csvAttc.setBody(csvBlob);

        Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();

        String[] toAddresses = new list<string> {UserInfo.getUserEmail()};

        String subject ='Fieldtrip '+ sObjectName;
        email.setSubject(subject);
        email.setToAddresses(toAddresses);
        email.setPlainTextBody(msg);
        email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});

        //Start new Batch
        if(Test.isRunningTest() == false){
            	
         	List<String> batchList = new List<String>{'ApexClass','ApexPage','ApexTrigger','ApexComponent'};
       	    ba_FieldTrip_CodeSearch batch = new ba_FieldTrip_CodeSearch(batchList, searchSet, null, sObjectName);   	                     
           	Database.executeBatch(batch, 200);
        }
        //END new Batch
    }
}