public with sharing class ControllerImplantCustomDelete {
    private final Implant__c delAcc;
    public static string warnText; 
    private String delId ; 
    public Boolean CanCloseWindow = false ;
    public string strException{get; set;}
    public Boolean ShowOkButton = true ;
    public boolean ChkDelRight=true;
    public boolean getChkDelRight(){return this.ChkDelRight;}
    public void setCanCloseWindow(Boolean st){  this.CanCloseWindow = st    ;   }
    public Boolean getCanCloseWindow(){         return this.CanCloseWindow  ;   }

    public void setShowOkButton(Boolean Val){   this.ShowOkButton = Val     ;   }
    public Boolean getShowOkButton(){           return this.ShowOkButton  ; }
        
    
    public ControllerImplantCustomDelete(ApexPages.StandardController impController) {
        this.delAcc = (Implant__c)impController.getRecord();

        if (ApexPages.currentPage().getParameters().get('id') != null){
            delId = ApexPages.currentPage().getParameters().get('id');
        }
        Implant__C delImplant=[select id,MMX_Implant_ID_Text__c,Call_Record_ID__c from Implant__C
         where id = :delId];
        
        if(delImplant.MMX_Implant_ID_Text__c!=null){
            warnText='MMX Implants can not be deleted.';
            ShowOkButton=false; 
        } else if(delImplant.Call_Record_ID__c!=null) {
            warnText='This Implant is linked to a Call record. <br />Deleting this Implant will delete the Call Record as well. <br />Are you sure?';               
        }
        else {
            warnText='Are you sure?';
        }               
    }   
    
// Show the message on the VF page.
    public string getWarntext(){
        return warntext;
    } 
public pagereference checkDeleteRights(){
 
     try{
            Savepoint sp = Database.setSavepoint(); 
             if (ApexPages.currentPage().getParameters().get('id') != null){
             Implant__c imp=[select id from implant__c where id=:ApexPages.currentPage().getParameters().get('id')];
             delete imp;  

             }
                 //this.delAcc = (Call_Records__c)impController.getRecord();
             Database.rollback(sp); 
          return null;   
        }
        catch(DmlException ex){
          ChkDelRight=false;
          //ApexPages.addMessages('Insufficient Previlleges');
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Insufficient Privileges'));
          return null;
        }  
    

}

    public PageReference DelImplant(){
        if (delId!=null){
        Implant__C delImplant=[select id from Implant__C where id = :delId];
         if(delImplant!=null){
            try{
                Delete delImplant;
                CanCloseWindow=true;    
            }
            catch (DmlException e){
                strException = e.getMessage();
                CanCloseWindow=false;
            }
                
         }          
        }   
        Pagereference pg=new pagereference('/a0N/o');
        pg.setRedirect(true);
        return pg;
    }
    
    Public Pagereference CnclImplant(){
        Pagereference pg=new Pagereference('/'+ApexPages.currentPage().getParameters().get('id'));
        pg.setRedirect(true);
        return pg;
    }   

}