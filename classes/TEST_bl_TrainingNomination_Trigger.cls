//--------------------------------------------------------------------------------------------------------------------------------
//  Author      :   Bart Caelen
//  Date        :   20160630
//  Description :   APEX TEST Class for the APEX Trigger tr_TrainingNomination and APEX Class bl_TrainingNomination_Trigger
//  Change Log  :    
//--------------------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_bl_TrainingNomination_Trigger {
	
	private static Training_Course__c oTrainingCourse;
	private static List<Training_Nomination__c> lstTrainingNomination;
	private static Map<String, Integer> mapStatus_Counter;
	private static Id id_PACEApprover;

	@isTest static void createTestData_TrainingCourse() {

		// Create Training Course Data
		oTrainingCourse = new Training_Course__c();
			oTrainingCourse.Course_Title__c = 'TEST TRAINING COURSE 001';
			oTrainingCourse.Start_Date__c = Date.today().addDays(20);
			oTrainingCourse.Last_Date_to_Register__c = Date.today().addDays(10);
			oTrainingCourse.Status__c = 'Open for Nomination';
			oTrainingCourse.PACE_Approver__c = id_PACEApprover;
			oTrainingCourse.Final_Follow_Up_Survey__c = 'http://www.finalfollowupsurvey.com';
			oTrainingCourse.Initial_Follow_Up_Survey__c = 'http://www.initialfollowupservey.com'; 
			oTrainingCourse.Logistics_Survey__c = 'http://www.logisticssurvey.com';
			oTrainingCourse.of_Seats_Available__c = 2;
		insert oTrainingCourse;

	}

	@isTest static void createTestData_TrainingNomination() {

		// Create Contact Data
		if (oTrainingCourse == null){
			createTestData_TrainingCourse();
		}
		clsTestData.iRecord_Contact = 6;
		clsTestData.createContactData();

		// Create Training Nomination Data
		lstTrainingNomination = new List<Training_Nomination__c>();
		for (Integer i = 0; i < 6; i++){
			Training_Nomination__c oTrainingNomination = new Training_Nomination__c();
				oTrainingNomination.Course__c = oTrainingCourse.Id;
				oTrainingNomination.Contact_Name__c = clsTestData.lstContact[i].Id;
				oTrainingNomination.Business_Unit_vs__c = 'Surgical Innovations';
				oTrainingNomination.Role__c = 'Trainee';
				oTrainingNomination.Status__c = 'Nominated';
			lstTrainingNomination.add(oTrainingNomination);	
		}

		insert lstTrainingNomination;

		System.assertEquals(lstTrainingNomination.size(), 6);

		mapStatus_Counter = new Map<String, Integer>();
		for (Training_Nomination__c oTrainingNomination : lstTrainingNomination){
			Integer iCounter = 0;
			if (mapStatus_Counter.containsKey(oTrainingNomination.Status__c)){
				iCounter = mapStatus_Counter.get(oTrainingNomination.Status__c);
			}
			iCounter++;
			mapStatus_Counter.put(oTrainingNomination.Status__c, iCounter);
		}

		System.assertEquals(mapStatus_Counter.get('Approved'), null);
		System.assertEquals(mapStatus_Counter.get('Evaluation'), null);
		System.assertEquals(mapStatus_Counter.get('Nominated'), 6);
		System.assertEquals(mapStatus_Counter.get('Approved - No Seats'), null);		
		System.assertEquals(mapStatus_Counter.get('Cancelled'), null);		
		System.assertEquals(mapStatus_Counter.get('Follow Up'), null);

	}	

	@isTest static void test_updateTrainingNomination_Url() {
        
        //Create Temp Custom Setting record
        EMEA_MITG_Opportunites__c recEMEAMITG = new EMEA_MITG_Opportunites__c();
        recEMEAMITG.Name = 'EMEA MITG Opportunities';
        recEMEAMITG.MITG_EMEA_Available__c = true;
        insert recEMEAMITG;

		// Create / Load Test Data
		createTestData_TrainingCourse();
		createTestData_TrainingNomination();

		
		Test.startTest();

			Integer iCounter = 0;
			for (Training_Nomination__c oTrainingNomination : lstTrainingNomination){
				if (iCounter < 2){
					oTrainingNomination.Status__c = 'Approved';
				}
				iCounter++;
			}
			update lstTrainingNomination;

		Test.stopTest();

		lstTrainingNomination = 
			[
				SELECT 
					Id, Status__c, Final_Follow_Up_Survey__c, Initial_Follow_Up_Survey__c, Logistics_Survey__c 
					, Course__r.Final_Follow_Up_Survey__c, Course__r.Initial_Follow_Up_Survey__c, Course__r.Logistics_Survey__c 
				FROM 
					Training_Nomination__c 
				WHERE 
					Course__c = :oTrainingCourse.Id
				];

		for (Training_Nomination__c oTrainingNomination : lstTrainingNomination){
			if (oTrainingNomination.Status__c == 'Approved'){
				System.assertEquals(oTrainingNomination.Final_Follow_Up_Survey__c, oTrainingNomination.Course__r.Final_Follow_Up_Survey__c);
				System.assertEquals(oTrainingNomination.Initial_Follow_Up_Survey__c, oTrainingNomination.Course__r.Initial_Follow_Up_Survey__c);
				System.assertEquals(oTrainingNomination.Logistics_Survey__c, oTrainingNomination.Course__r.Logistics_Survey__c);
			}else{
				System.assertEquals(clsUtil.isNull(oTrainingNomination.Final_Follow_Up_Survey__c, ''), '');
				System.assertEquals(clsUtil.isNull(oTrainingNomination.Initial_Follow_Up_Survey__c, ''), '');
				System.assertEquals(clsUtil.isNull(oTrainingNomination.Logistics_Survey__c, ''), '');
			}
		}

	}

	@isTest static void test_validateTrainingNominationApproval_OK() {
        
        //Create Temp Custom Setting record
        EMEA_MITG_Opportunites__c recEMEAMITG = new EMEA_MITG_Opportunites__c();
        recEMEAMITG.Name = 'EMEA MITG Opportunities';
        recEMEAMITG.MITG_EMEA_Available__c = true;
        insert recEMEAMITG;

		// Create / Load Test Data
		createTestData_TrainingCourse();
		createTestData_TrainingNomination();

		
		Test.startTest();

			Integer iCounter = 0;
			for (Training_Nomination__c oTrainingNomination : lstTrainingNomination){
				if (iCounter < 2){
					oTrainingNomination.Status__c = 'Approved';
				}
				iCounter++;
			}
			update lstTrainingNomination;

		Test.stopTest();

	}

	@isTest static void test_validateTrainingNominationApproval_ERROR() {
        
        //Create Temp Custom Setting record
        EMEA_MITG_Opportunites__c recEMEAMITG = new EMEA_MITG_Opportunites__c();
        recEMEAMITG.Name = 'EMEA MITG Opportunities';
        recEMEAMITG.MITG_EMEA_Available__c = true;
        insert recEMEAMITG;

		// Create / Load Test Data
		createTestData_TrainingCourse();
		createTestData_TrainingNomination();

		List<Database.SaveResult> lstSaveResult = new List<Database.SaveResult>();
		Test.startTest();

			for (Training_Nomination__c oTrainingNomination : lstTrainingNomination){
				oTrainingNomination.Status__c = 'Approved';
			}

			lstSaveResult = Database.update(lstTrainingNomination, false);


		Test.stopTest();

		System.assertEquals(lstTrainingNomination.size(), 6);
		System.assertEquals(lstSaveResult.size(), 6);

		lstTrainingNomination = [SELECT Id, Status__c FROM Training_Nomination__c WHERE Status__c = 'Approved'];

		System.assertEquals(lstTrainingNomination.size(), 2);

		Integer iCounter_Success = 0;
		Boolean bException = false;
		for (Database.SaveResult oSaveResult : lstSaveResult){
			if (oSaveResult.isSuccess()){
				iCounter_Success++;
			}else{
				List<Database.Error> lstDatabaseError = oSaveResult.getErrors();
				for (Database.Error oDatabaseError : lstDatabaseError){
					System.assert(oDatabaseError.getMessage().contains(Label.TrainingNominationApprovalError));
					bException = true;
				}

			}
		}

		System.assertEquals(iCounter_Success, 2);
		System.assertEquals(bException, true);

	}	
	

	@isTest static void test_preventDuplicateNomination() {

		// Create / Load Test Data
		createTestData_TrainingCourse();
		createTestData_TrainingNomination();

		Test.startTest();

		Training_Nomination__c oTrainingNomination = lstTrainingNomination[0].clone(false, true, false, false);

		Boolean bException = false;

		try{
			insert oTrainingNomination;
		}catch(Exception oEX){
			System.assert(oEX.getMessage().contains(Label.TrainingNominationContactError));
			bException = true;
		}

		System.assertEquals(bException, true);

		Test.stopTest();

	}

	@isTest static void test_updateTrainingNomination_PACEApprover() {

		// Create / Load Test Data
		createTestData_TrainingCourse();

		Test.startTest();

		createTestData_TrainingNomination();

		Test.stopTest();

		lstTrainingNomination = [SELECT Id, PACE_Approver__c, Course__r.PACE_Approver__c FROM Training_Nomination__c];

		for (Training_Nomination__c oTrainingNomination : lstTrainingNomination){
			System.assertEquals(oTrainingNomination.PACE_Approver__c, oTrainingNomination.Course__r.PACE_Approver__c);
		}

	}

	@isTest static void test_createFollowUpTask() {

        //Create Temp Custom Setting record
        EMEA_MITG_Opportunites__c recEMEAMITG = new EMEA_MITG_Opportunites__c();
        recEMEAMITG.Name = 'EMEA MITG Opportunities';
        recEMEAMITG.MITG_EMEA_Available__c = true;
        insert recEMEAMITG;
        
		// Create / Load Test Data
		createTestData_TrainingCourse();
		createTestData_TrainingNomination();

		List<Task> lstTask = [SELECT Id FROM Task WHERE WhatId = :lstTrainingNomination];
		System.assertEquals(lstTask.size(), 0);
		System.assert(lstTrainingNomination.size() > 0);
		
		Test.startTest();

			Integer iCounter = 0;
			for (Training_Nomination__c oTrainingNomination : lstTrainingNomination){
				if (iCounter < 2){
					oTrainingNomination.Status__c = 'Follow Up';
				}
				iCounter++;
			}
			update lstTrainingNomination;

		Test.stopTest();

		lstTask = [SELECT Id FROM Task WHERE WhatId = :lstTrainingNomination];
		System.assertEquals(lstTask.size(), 2);
		System.assert(lstTrainingNomination.size() > 2);

	}

}
//--------------------------------------------------------------------------------------------------------------------------------