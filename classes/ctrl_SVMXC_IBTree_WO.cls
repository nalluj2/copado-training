/**
 * @description       : Controller to display the Installed base hierarchy for a work order.
 * @author            : tom.h.ansley@medtronic.com
 * @last modified on  : 10-12-2020
 * @last modified by  : tom.h.ansley@medtronic.com
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   10-12-2020   tom.h.ansley@medtronic.com   Initial Version
**/
public with sharing class ctrl_SVMXC_IBTree_WO {

    public SVMXC__Service_Order__c workOrder {get; set;}
    public String topInstProdId              {get; set;}
    public String thisInstProdId             {get; set;}

    public ctrl_SVMXC_IBTree_WO(ApexPages.StandardController controller)  
    {
        workOrder = (SVMXC__Service_Order__c) controller.getRecord();

        workOrder = [SELECT Id, SVMXC__Component__r.SVMXC__Top_Level__c FROM SVMXC__Service_Order__c WHERE Id = :workOrder.Id];

        topInstProdId = workOrder.SVMXC__Component__r.SVMXC__Top_Level__c;
        thisInstProdId = workOrder.SVMXC__Component__c;

        System.debug(LoggingLevel.DEBUG, 'topInstProdId - ' + topInstProdId);
        System.debug(LoggingLevel.DEBUG, 'thisInstProdId - ' + thisInstProdId);

    }
}