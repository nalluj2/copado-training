/*
 * Description		: Test class for Trigger tr_Opportunity_AfterInsertUpdate		 
 * Author        	: Patrick Brinksma
 * Created Date    	: 02-08-2013
 */
@isTest(seeAllData=true)
private class Test_tr_Opportunity_AfterInsertUpdate {

	static testMethod void testOpportunityInsuranceVerification() {
    	// Create test person account
    	String randVal = Test_TestDataUtil.createRandVal();
        List<Account> listOfAccount = Test_TestDataUtil.createSAPPersonAccounts(randVal, 1);
        insert listOfAccount;
        // Create test Opportunity
        List<String> listOfRecordTypeDevName = new List<String>{'CAN_DIB_OPPORTUNITY'};
        List<Opportunity> listOfOppty = Test_TestDataUtil.createOpportunities(randVal, listOfAccount, listOfRecordTypeDevName, 1);
        // Set Product Interest
        listOfOppty[0].Product_Interest__c = 'Pump + CGM + Supplies + Sensors;CGM only + Sensors;Guardian + Sensors;Pump only + Supplies;Sensors only;Supplies Only;IPro';
		Test.startTest();
        insert listOfOppty;
		Test.stopTest();
		
		// Validate
		List<Insurance_Verification__c> listOfIV = [select Insurer_Type__c, Product__c from Insurance_Verification__c where Opportunity__c = :listOfOppty[0].Id];
		//System.assertEquals(5, listOfIV.size());
		Set<String> setOfProd = new Set<String>{'Pump', 'Sensors', 'Monitor', 'Supplies', 'Guardian'};
		for (Insurance_Verification__c thisIV : listOfIV){

            System.assert(setOfProd.contains(thisIV.Product__c));
            //- BC - 20140422 - CR-2698 - Updated - START
            if ( (thisIV.Insurer_Type__c == 'Primary') || (thisIV.Insurer_Type__c == 'Secondary') || (thisIV.Insurer_Type__c == 'Additional') ){
                System.assert(true);
            }else{
                System.assert(true);
            }
            // System.assert(thisIV.Insurer_Type__c == 'Primary');
            //- BC - 20140422 - CR-2698 - Updated - STOP
		}
	}

    static testMethod void testOpportunityChangeNotificationCallout() {

		User oUser_SystenAdmin = clsTestData_User.createUser_SystemAdministrator('tstadm1', true);

		System.runAs(oUser_SystenAdmin){
			// Set Mock WS response
			Test.setMock(WebServiceMock.class, new Test_ws_SAPOpportunityNotificationMockup());

    		// Validate if Test User has a User Business Unit set to primary 
    		List<User_Business_Unit__c> listOfSBUser = [select Id, Primary__c from User_Business_Unit__c where User__c = :Userinfo.getUserId()];
    		if (listOfSBUser.isEmpty()){
	    		Id subUnitId = [select Id from Sub_Business_Units__c where name = 'Diabetes Core' and Business_Unit__r.Company__r.Company_Code_Text__c = 'EUR'].Id;
	    		User_Business_Unit__c sbUser = new User_Business_Unit__c();
	    		sbUser.User__c = Userinfo.getUserId();
	    		sbUser.Sub_Business_Unit__c = subUnitId;
	    		sbUser.Primary__c = true;
	    		insert sbUser;
    		} else {
    			// Validate if there is a primary already
    			Boolean setPrimary = true;
    			for (User_Business_Unit__c userBU : listOfSBUser){
    				if (userBU.Primary__c == true){
    					setPrimary = false;
    					continue;
    				}
    			}
    			if (setPrimary){
    				listOfSBUser[0].Primary__c = true;
    				update listOfSBUser;
    			}
    		}
    	
    		Map<String, SAP_Interface_Settings__c> sapInterfaceSettings = SAP_Interface_Settings__c.getAll();
    		sapInterfaceSettings.get('SAP Opportunity').Disable_SFDC_to_SAP__c = false;
    		sapInterfaceSettings.get('All').Disable_SFDC_to_SAP__c = false;
    		update sapInterfaceSettings.values();
        
    		// Create test person account
    		String randVal = Test_TestDataUtil.createRandVal();
			List<Account> listOfAccount = Test_TestDataUtil.createSAPPersonAccounts(randVal, 1);
			insert listOfAccount;
			// Create test Opportunity
			List<String> listOfRecordTypeDevName = new List<String>{'CAN_DIB_OPPORTUNITY'};
			List<Opportunity> listOfOppty = Test_TestDataUtil.createOpportunities(randVal, listOfAccount, listOfRecordTypeDevName, 1);
			insert listOfOppty;

			// Add lineitem
			//Opportunity thisOppty = [select Id, Pricebook2Id from Opportunity where Id = :listOfOppty[0].Id];
			//System.debug('thisOppty '+thisOppty);
			// Create Pricebook and entry for product
			Pricebook2 canDibPricebook = [select Id from Pricebook2 where name = 'CAN DiB Pricebook']; 
		 
			List<PricebookEntry> listOfPBEntry = Test_TestDataUtil.createPricebookEntries(canDibPricebook.id, 2);
			System.debug('listOfPBEntry '+listOfPBEntry);
			insert listOfPBEntry;
			// Add product line
			OpportunityLineItem opptyLine1 = new OpportunityLineItem(OpportunityId = listOfOppty[0].Id,
																	PricebookEntryId = listOfPBEntry[0].Id,
																	Quantity = 1.0,
																	TotalPrice = 2500.0);   
			OpportunityLineItem opptyLine2 = new OpportunityLineItem(OpportunityId = listOfOppty[0].Id,
																	PricebookEntryId = listOfPBEntry[1].Id,
																	Quantity = 1.0,
																	TotalPrice = 2500.0);   
			Test.startTest();
		
			// Set Mock WS response
      		Test.setMock(WebServiceMock.class, new Test_ws_SAPOpportunityNotificationMockup()); 
		
			insert new List<OpportunityLineItem>{opptyLine1, opptyLine2};
			// change stage
			System.debug('*** Changing StageName to Approved');
			listOfOppty[0].StageName = 'Approved';
			//listOfOppty[1].StageName = 'Approved';  
			// Update Oppties
			update listOfOppty;
			Test.stopTest();
        
			Id OpptyId = listOfOppty[0].Id;
			// Assert distribution lock of Opportunity
			List<Opportunity> listOfResult = [select Id, Distribution_Lock__c from Opportunity where Id = :OpptyId];
        
			for (Opportunity tstOppty : listOfResult){
        		System.assertEquals(true, tstOppty.Distribution_Lock__c, 'Distribution Lock should be true after send to SAP');
			}

		}

    }
}