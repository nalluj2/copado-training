@isTest
private class Test_tr_Account_Plan_SWOT_Analysis {
    
    private static testmethod void testUniqueSWOT(){
    	
    	// Master Data
		Company__c europe = new Company__c();
		europe.Name = 'Europe';
		europe.Company_Code_Text__c = 'TST';
		insert europe;
		
		// Business Unit Groups		
		Business_Unit_Group__c diabetesBUG = new Business_Unit_Group__c();
		diabetesBUG.Master_Data__c = europe.id;
        diabetesBUG.name='Diabetes';        
        insert diabetesBUG;
        
        // Business Units        
        Business_Unit__c diabetesBU =  new Business_Unit__c();
        diabetesBU.Company__c = Europe.id;
        diabetesBU.name = 'Diabetes';
        diabetesBU.Business_Unit_Group__c = diabetesBUG.Id;  
        insert diabetesBU;
        
        // Sub-Business Units        
		Sub_Business_Units__c diabetesCoreSBU = new Sub_Business_Units__c();
        diabetesCoreSBU.name = 'Diabetes Core';
        diabetesCoreSBU.Business_Unit__c = diabetesBU.id;		
		insert diabetesCoreSBU;
		
		Account acc = new Account();
		acc.Name = 'Unit Test Account';
		acc.SAP_Id__c = '123456789';
		acc.Account_Country_vs__c = 'Belgium';
		insert acc;
		
		Account_Plan_2__c accPlan = new Account_Plan_2__c();
		accPlan.Account__c = acc.Id;
		accPlan.Account_Plan_Level__c = 'Sub Business Unit';
		accPlan.Business_Unit_Group__c = diabetesBUG.Id;
		accPlan.Business_Unit__c = diabetesBU.Id;
		accPlan.Sub_Business_Unit__c = diabetesCoreSBU.Id;
		insert accPlan;
		
		Department_Master__c adultPediatric = new Department_Master__c(Name = 'Adult/Pediatric');
		Department_Master__c adultT1 = new Department_Master__c(Name = 'Adult Type 1');
		Department_Master__c adultT2 = new Department_Master__c(Name = 'Adult Type 2');
		Department_Master__c pediatric = new Department_Master__c(Name = 'Pediatric');
		
		insert new List<Department_Master__c>{adultPediatric, adultT1, adultT2, pediatric};
		
		DIB_Department__c accAdultT1 = new DIB_Department__c();
		accAdultT1.Account__c = acc.Id;
		accAdultT1.Department_ID__c = adultT1.Id;
		
		DIB_Department__c accPed = new DIB_Department__c();
		accPed.Account__c = acc.Id;
		accPed.Department_ID__c = pediatric.Id;
		
		insert new List<DIB_Department__c>{accAdultT1, accPed};
		
		Test.startTest();
    	
    	Account_Plan_SWOT_Analysis__c swotAdult = new Account_Plan_SWOT_Analysis__c();
    	swotAdult.Account_Plan__c = accPlan.Id;
    	swotAdult.Department__c = 'Adult Type 1';
    	
    	Account_Plan_SWOT_Analysis__c swotPediatric = new Account_Plan_SWOT_Analysis__c();
    	swotPediatric.Account_Plan__c = accPlan.Id;
    	swotPediatric.Department__c = 'Pediatric';
    	
    	insert new List<Account_Plan_SWOT_Analysis__c>{swotAdult, swotPediatric};
    	
    	Account_Plan_SWOT_Analysis__c swotAdult2 = new Account_Plan_SWOT_Analysis__c();
    	swotAdult2.Account_Plan__c = accPlan.Id;
    	swotAdult2.Department__c = 'Adult Type 1';
    	
    	Boolean isError = false;
    	
    	try{
    		
    		insert swotAdult2;
    		
    	}catch(Exception e){
    		
    		System.assert(e.getMessage().contains('duplicate'));
    		isError = true;
    	}
    	
    	System.assert(isError == true);
    	
    	swotAdult2.Department__c = 'Adult Type 2';
    	
    	isError = false;
    	
    	try{
    		
    		insert swotAdult2;
    		
    	}catch(Exception e){
    		
    		System.assert(e.getMessage().contains('The selected Department is not available for this Account'));
    		isError = true;
    	}
    	
    	System.assert(isError == true);
    	
    	DIB_Department__c accAdultPed = new DIB_Department__c();
		accAdultPed.Account__c = acc.Id;
		accAdultPed.Department_ID__c = adultPediatric.Id;
		insert accAdultPed;
		
		insert swotAdult2;
		
		swotAdult2 = [Select Account_Department__c from Account_Plan_SWOT_Analysis__c where id = :swotAdult2.Id];
		System.assert(swotAdult2.Account_Department__c == accAdultPed.Id);
		
    }
}