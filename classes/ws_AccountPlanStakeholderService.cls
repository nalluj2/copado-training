/*
 *      Description : This class exposes methods to create / update / delete a Account Plan Stakeholder
 *
 *      Author = Rudy De Coninck
 */

@RestResource(urlMapping='/AccountPlanStakeholderService/*')
global with sharing class ws_AccountPlanStakeholderService {
	@HttpPost
	global static String doPost() {
 		RestRequest req = RestContext.request;
			
		System.debug('post called '+req);
		System.debug('post requestBody '+req.requestBody);
			
		String body = req.requestBody.toString();
		IFAccountPlanStakeholderRequest accountPlanStakeholderRequest = (IFAccountPlanStakeholderRequest)System.Json.deserialize(body, IFAccountPlanStakeholderRequest.class);
			
		System.debug('post requestBody '+accountPlanStakeholderRequest);
			
		Account_Plan_Stakeholder__c accountPlanStakeholder = accountPlanStakeholderRequest.accountPlanStakeholder;

		IFAccountPlanStakeholderResponse resp = new IFAccountPlanStakeholderResponse();

		//We catch all exception to assure that 
		try{
				
			resp.accountPlanStakeholder = bl_AccountPlanning.saveAccountPlanStakeholder(accountPlanStakeholder);
			resp.id = accountPlanStakeholderRequest.id;
	
			resp.success = true;
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
		}
 
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'AccountPlanStakeholder' ,req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
  			
		return System.JSON.serialize(resp);		 			
	}

	@HttpDelete
	global static String doDelete(){
		RestRequest req = RestContext.request;
			
		System.debug('post called '+req);
		System.debug('post requestBody '+req.requestBody);
			
		String body = req.requestBody.toString();
		IFAccountPlanStakeholderDeleteRequest accountPlanStakeholderDeleteRequest = (IFAccountPlanStakeholderDeleteRequest)System.Json.deserialize(body, IFAccountPlanStakeholderDeleteRequest.class);
			
		System.debug('post requestBody '+accountPlanStakeholderDeleteRequest);
			
		Account_Plan_Stakeholder__c accountPlanStakeholder =accountPlanStakeholderDeleteRequest.accountPlanStakeholder;
		IFAccountPlanStakeholderDeleteResponse resp = new IFAccountPlanStakeholderDeleteResponse();
		
		List<Account_Plan_Stakeholder__c>accountPlanStakeholdersToDelete = [select id, mobile_id__c from Account_Plan_Stakeholder__c where Mobile_ID__c = :accountPlanStakeholder.Mobile_ID__c];
		
		try{
			
			if (accountPlanStakeholdersToDelete !=null && accountPlanStakeholdersToDelete.size()>0 ){	
				
				Account_Plan_Stakeholder__c accountPlanStakeholderToDelete = accountPlanStakeholdersToDelete.get(0);
				delete accountPlanStakeholderToDelete;
				resp.accountPlanStakeholder = accountPlanStakeholderToDelete;
				resp.id = accountPlanStakeholderDeleteRequest.id;
				resp.success = true;
			}else{
				resp.accountPlanStakeholder = accountPlanStakeholder;
				resp.id = accountPlanStakeholderDeleteRequest.id;
				resp.message='Account Plan Stakeholder not found';
				resp.success = true;
			}
			
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
		}
		
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'AccountPlanStakeholderDelete',req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
			
		return System.JSON.serialize(resp);		
	}
	
	
	global class IFAccountPlanStakeholderRequest{
		public Account_Plan_Stakeholder__c accountPlanStakeholder {get;set;}
		public String id{get;set;}
	}
	
	global class IFAccountPlanStakeholderDeleteRequest{
		public Account_Plan_Stakeholder__c accountPlanStakeholder {get;set;}
		public String id{get;set;}
	}
		
	global class IFAccountPlanStakeholderDeleteResponse{
		public Account_Plan_Stakeholder__c accountPlanStakeholder {get;set;}
		public String id{get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}

	global class IFAccountPlanStakeholderResponse{
		public Account_Plan_Stakeholder__c accountPlanStakeholder {get;set;}
		public String id{get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}
	
}