@isTest
private class Test_tr_Territory_before {

	private static testmethod void testTherapyGroupsSync(){
		
		Territory2 ter = new Territory2();
		ter.Name = 'Unit Test Territory';
		ter.Description = 'Unit Test Territory';
		ter.DeveloperName = 'Unit_Test_Territory';
		ter.Business_Unit__c = 'Vascular';
		ter.Company__c = 'a0w11000001Q3y9';
		ter.Country_UID__c = 'NL';
		ter.Territory2TypeId = bl_territory.territoryTypeMap.get('Territory');
		ter.Devart_Therapy_Groups__c = 'Aortic';
		ter.Territory2ModelId = [Select Id from Territory2Model where DeveloperName = 'Medtronic'].Id;
		insert ter;
		
		ter = [Select Id, Therapy_Groups_Text__c, Exclude_from_sales_structure_alignment__c from Territory2 where Id = :ter.Id];		
		System.assert(ter.Therapy_Groups_Text__c == 'Aortic');
		System.assert(ter.Exclude_from_sales_structure_alignment__c == true);
		
		ter.Therapy_Groups_Text__c = 'Interventional Coronary';
		update ter;
		
		ter = [Select Id, Devart_Therapy_Groups__c from Territory2 where Id = :ter.Id];		
		System.assert(ter.Devart_Therapy_Groups__c == 'Interventional Coronary');
	}
}