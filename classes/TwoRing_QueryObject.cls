global class TwoRing_QueryObject {
    global List<List<String>> names;
    global List<List<String>> phones;
    global List<List<String>> emails;
    global List<string> ids;
    global List<String> entityTypes;
    global Map<string, List<TwoRing_CustomCriteria>> customCriterias;
    global Map<String, List<String>> fields;
}