/**
 * Test method to cover AssetBeforeDelete Trigger
 * Create By - Dheeraj  Date - 20110801
 */
@isTest
private class TestAssetBeforeDelete {

    static testMethod void myUnitTest() {
         
        id SAPProductId = [select id from RecordType where SobjectType='Product2' and name='SAP Product'].id;
        id NONSAPProductId = [select id from RecordType where SobjectType='Product2' and name='NON-SAP Product'].id;
        
        User u2 = [Select Id from User where Profile.NAme = 'IT Business Analyst' AND isActive = true AND UserRole.NAme = 'System Administrator' LIMIT 1];

        Product2 pr1 = new product2();
        pr1.Name='Test Activity';  
        pr1.RecordTypeId = NONSAPProductId;
        insert pr1;   

        Product2 pr2 = new product2();
        pr2.Name='Test Activity1';
        pr2.RecordTypeId = SAPProductId;
        insert pr2;   
        
        Account a = new Account() ;
        a.Name = 'Test Account Name ';  
        a.RecordTypeId = FinalConstants.sapAccountRecordTypeId ;
        a.Account_Country_vs__c='BELGIUM';
        a.CurrencyIsoCode ='EUR';
        a.OwnerId = u2.Id;
        insert a;
        
        Asset asset1 = new Asset();
        asset1.Product2Id = pr1.id; 
        asset1.Quantity = 10; 
        asset1.Name = 'asset1';
        asset1.accountid = a.id; 
        asset1.Serial_Nr__c = 'NONSAP';
                       
        Asset asset2 = new Asset();
        asset2.Product2Id = pr2.id; 
        asset2.Quantity = 40; 
        asset2.Name = 'asset1'; 
        asset2.accountid = a.id;
        asset2.Serial_Nr__c = 'SAP';
        
        insert new List<Asset>{asset1, asset2};      

        Test.startTest();
	
        System.runAs(u2) {
            
            Delete asset1;          
            
            Boolean error = false;
            try{
                Delete asset2;  // SAP Product Asset                        
            } catch(exception e){
            	
            	error = true;
            }
            
            System.assert(error == true);
        }               
        
    }
}