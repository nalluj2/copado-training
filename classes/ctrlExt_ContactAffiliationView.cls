public with sharing class ctrlExt_ContactAffiliationView {

    private final Contact oContact;
    private final Id id_Contact;

    private final String tAffiliationType_Referring = 'Referring';

    private Id id_RecordType_C2A;
    private Id id_RecordType_C2C;

    private Integer iPageSize_C2A = 5;
    private Integer iPageSize_C2C = 5;

    public ctrlExt_ContactAffiliationView(ApexPages.StandardController oStdController) {

        List<String> lstField = new List<String>{'RecordTypeId'};
        if (!Test.isRunningTest()){
            oStdController.addFields(lstField);
        }

        oContact = (Contact)oStdController.getRecord();
        id_Contact = oContact.Id;

        initialize();

    }


    //------------------------------------------------------------------------------
    // Getters & Setters - General
    //------------------------------------------------------------------------------
    public Id id_AssignedAffiliation { get; set; }

    public Boolean bShowFilter { get; set; }
    public Boolean bSaveFilter { get; set; }

    public List<SelectOption> lstPageSize { get; set; }
    //------------------------------------------------------------------------------



    //------------------------------------------------------------------------------
    // Getters & Setters - Contact Affiliations (C2A)
    //------------------------------------------------------------------------------
    public String tSortField_C2A { get; set; }
    public String tPrevSortField_C2A { get; set; }
    public String tSortOrder_C2A { get; set; }
    public String tWarning_C2A { get; private set; }

    public List<SObject> lstRecord_C2A {
        get {
            return oSetCtrl_C2A.getRecords();
        }
    }   

    public Boolean hasNext_C2A {
        get {
            return oSetCtrl_C2A.getHasNext();
        }
        set;
    }

    public Boolean hasPrevious_C2A {
        get {
            return oSetCtrl_C2A.getHasPrevious();
        }
        set;
    }

    public Integer pageNumber_C2A {
        get {
            return oSetCtrl_C2A.getPageNumber();
        }
        set{
            oSetCtrl_C2A.setPageNumber(value);
        }
    }

    public Integer totalPages_C2A {
        get {
            if (oSetCtrl_C2A.getResultSize() == 0) return 1;
            Decimal decRecNum =  oSetCtrl_C2A.getResultSize();
            Decimal decRecPage = oSetCtrl_C2A.getPageSize();
            Decimal decTotal = decRecNum.divide(decRecPage, 2);
            return Integer.valueOf(decTotal.round(System.RoundingMode.UP));
        }
        set;
    }

    public Integer pageSize_C2A {
        get {
            return oSetCtrl_C2A.getPageSize();
        }
        set{
            iPageSize_C2A = value;
            oSetCtrl_C2A.setPageSize(value);
        }
    }

    public Integer resultSize_C2A {
        get {
        return oSetCtrl_C2A.getResultSize();
        }
        set;
    }

    public void first_C2A() {
        oSetCtrl_C2A.first();
    }

    public void last_C2A() {
        oSetCtrl_C2A.last();
    }

    public void previous_C2A() {
        oSetCtrl_C2A.previous();
    }

    public void next_C2A() {
        oSetCtrl_C2A.next();
    }  
    //------------------------------------------------------------------------------



    //------------------------------------------------------------------------------
    // Getters & Setters - Contact Affiliations (C2C)
    //------------------------------------------------------------------------------
    public String tSortField_C2C { get; set; }
    public String tPrevSortField_C2C { get; set; }
    public String tSortOrder_C2C { get; set; }
    public String tWarning_C2C { get; private set; }

    public List<SObject> lstRecord_C2C {
        get {
            return oSetCtrl_C2C.getRecords();
        }
    }   

    public Boolean hasNext_C2C {
        get {
            return oSetCtrl_C2C.getHasNext();
        }
        set;
    }

    public Boolean hasPrevious_C2C {
        get {
            return oSetCtrl_C2C.getHasPrevious();
        }
        set;
    }

    public Integer pageNumber_C2C {
        get {
            return oSetCtrl_C2C.getPageNumber();
        }
        set{
            oSetCtrl_C2C.setPageNumber(value);
        }
    }

    public Integer totalPages_C2C {
        get {
            if (oSetCtrl_C2C.getResultSize() == 0) return 1;
            Decimal decRecNum =  oSetCtrl_C2C.getResultSize();
            Decimal decRecPage = oSetCtrl_C2C.getPageSize();
            Decimal decTotal = decRecNum.divide(decRecPage, 2);
            return Integer.valueOf(decTotal.round(System.RoundingMode.UP));
        }
        set;
    }

    public Integer pageSize_C2C {
        get {
            return oSetCtrl_C2C.getPageSize();
        }
        set{
            iPageSize_C2C = value;
            oSetCtrl_C2C.setPageSize(value);
        }
    }

    public Integer resultSize_C2C {
        get {
            return oSetCtrl_C2C.getResultSize();
        }
        set;
    }

    public void first_C2C() {
        oSetCtrl_C2C.first();
    }

    public void last_C2C() {
        oSetCtrl_C2C.last();
    }

    public void previous_C2C() {
        oSetCtrl_C2C.previous();
    }

    public void next_C2C() {
        oSetCtrl_C2C.next();
    }  
    //------------------------------------------------------------------------------


    //------------------------------------------------------------------------------
    // Actions - C2A
    //------------------------------------------------------------------------------
    public void sortData_C2A() {

        if (tSortField_C2A == tPrevSortField_C2A){
            if (tSortOrder_C2A == 'ASC'){
                tSortOrder_C2A = 'DESC';
            }else{
                tSortOrder_C2A = 'ASC';
            }
        }else{
            tSortOrder_C2A = 'ASC';
            tPrevSortField_C2A = tSortField_C2A;
        }

        oSetCtrl_C2A = new ApexPages.StandardSetController(Database.getQueryLocator(tBuildQuery('C2A')));
        oSetCtrl_C2A.setPageSize(iPageSize_C2A);

    }

    public PageReference viewAffiliation_C2A(){

        PageReference oPageRef;

        if ( !clsUtil.isBlank(id_AssignedAffiliation) ){

            oPageRef = new PageReference('/' + id_AssignedAffiliation);
            oPageRef.setRedirect(true);

        }

        return oPageRef;

    }   

    public PageReference createAffiliation_C2A(){

        PageReference oPageRef;

        oPageRef = new PageReference('/apex/createC2AAffiliationPage');
            oPageRef.getParameters().put('RecordType', id_RecordType_C2A);
            oPageRef.getParameters().put(FinalConstants.contactFromFieldId, id_Contact);
            oPageRef.getParameters().put('retURL', '/apex/ContactAffiliationView');
            oPageRef.getParameters().put('retID', id_Contact);
            oPageRef.getParameters().put('sfdc.override', '1');
        oPageRef.setRedirect(true);
        
        return oPageRef;

    }

    public PageReference editAffiliation_C2A(){

        PageReference oPageRef;

        if ( !clsUtil.isBlank(id_AssignedAffiliation) ){
            
            List<Affiliation__c> lstAffiliation_Tmp = [SELECT Id FROM Affiliation__c WHERE Affiliation_Primary__c = true AND Id = :id_AssignedAffiliation];
            
            if (lstAffiliation_Tmp.size() > 0){
            
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.You_can_only_change_primary_relationships_via_the_contact_detail_page));                
            
            }else{  
    
                //oPageRef = new PageReference('/' + id_AssignedAffiliation +'/e');
                //    oPageRef.getParameters().put('retURL', '/apex/ContactAffiliationView');
                //    oPageRef.getParameters().put('retID', id_Contact);
                //oPageRef.setRedirect(true);

                oPageRef = new PageReference('/apex/createC2AAffiliationPage');
            	oPageRef.getParameters().put('Id', id_AssignedAffiliation);
                oPageRef.getParameters().put('retURL', '/apex/ContactAffiliationView');
            	oPageRef.getParameters().put('retID', id_Contact);
            	oPageRef.getParameters().put('sfdc.override', '1');
        		oPageRef.setRedirect(true);
    
            }                   
    
        }

        return oPageRef;
    }

    public void deleteAffiliation_C2A(){

        if ( !clsUtil.isBlank(id_AssignedAffiliation) ){

            try{

                clsUtil.bubbleException();

                List<Affiliation__c> lstAffiliation_Tmp = [SELECT Id, Affiliation_Primary__c FROM Affiliation__c WHERE  Id = :id_AssignedAffiliation];

                if (lstAffiliation_Tmp.size() == 1){

                    if (lstAffiliation_Tmp[0].Affiliation_Primary__c == true){

                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, Label.You_can_only_change_primary_relationships_via_the_contact_detail_page));              
        
                    }else{          

                        delete lstAffiliation_Tmp;
                        oSetCtrl_C2A = null;

                    }

                }    

            }catch(Exception oEX){

                    if (oEX.getMessage().contains('INSUFFICIENT_ACCESS_OR_READONLY')){ 
                
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.Insufficient_Privileges));
                
                }else{            
                
                    ApexPages.addMessages(oEX); 
                
                } 
         
            }

        }

    }
    //------------------------------------------------------------------------------


    //------------------------------------------------------------------------------
    // Actions - C2C
    //------------------------------------------------------------------------------
    public void sortData_C2C() {

        if (tSortField_C2C == tPrevSortField_C2C){
            if (tSortOrder_C2C == 'ASC'){
                tSortOrder_C2C = 'DESC';
            }else{
                tSortOrder_C2C = 'ASC';
            }
        }else{
            tSortOrder_C2C = 'ASC';
            tPrevSortField_C2C = tSortField_C2C;
        }

        oSetCtrl_C2C = new ApexPages.StandardSetController(Database.getQueryLocator(tBuildQuery('C2C')));
        oSetCtrl_C2C.setPageSize(iPageSize_C2C);

    }

    public PageReference viewAffiliation_C2C(){

        PageReference oPageRef;

        if ( !clsUtil.isBlank(id_AssignedAffiliation) ){

            oPageRef = new PageReference('/' + id_AssignedAffiliation);
            oPageRef.setRedirect(true);

        }

        return oPageRef;

    }   

    public PageReference createAffiliation_C2C(){

        PageReference oPageRef;

        oPageRef = new PageReference('/apex/createC2CAffiliationPage');       
            oPageRef.getParameters().put('RecordType', id_RecordType_C2C);
            oPageRef.getParameters().put(FinalConstants.contactFromFieldId, id_Contact);
            oPageRef.getParameters().put(FinalConstants.affiliationTypeFieldLabel, 'Referring');
            oPageRef.getParameters().put('retURL', '/apex/ContactAffiliationView');
            oPageRef.getParameters().put('retID', id_Contact);
            oPageRef.getParameters().put('sfdc.override', '1');
        oPageRef.setRedirect(true);       

        return oPageRef;  

    }

    public PageReference editAffiliation_C2C(){

        PageReference oPageRef;

        if ( !clsUtil.isBlank(id_AssignedAffiliation) ){
            
            List<Affiliation__c> lstAffiliation_Tmp = [SELECT Id FROM Affiliation__c WHERE Affiliation_Primary__c = true AND Id = :id_AssignedAffiliation];
            
            if (lstAffiliation_Tmp.size() > 0){
            
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.You_can_only_change_primary_relationships_via_the_contact_detail_page));                
            
            }else{  
    
                oPageRef = new PageReference('/' + id_AssignedAffiliation +'/e');
                    oPageRef.getParameters().put('retURL', '/apex/ContactAffiliationView');
                    oPageRef.getParameters().put('retID', id_Contact);
                oPageRef.setRedirect(true);
    
            }                   
    
        }

        return oPageRef;
    }

    public void deleteAffiliation_C2C(){

        if ( !clsUtil.isBlank(id_AssignedAffiliation) ){

            try{
    
                List<Affiliation__c> lstAffiliation_Tmp = [SELECT Id, Affiliation_Primary__c FROM Affiliation__c WHERE  Id = :id_AssignedAffiliation];

                if (lstAffiliation_Tmp.size() == 1){

                    if (lstAffiliation_Tmp[0].Affiliation_Primary__c == true){

                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, Label.You_can_only_change_primary_relationships_via_the_contact_detail_page));              
        
                    }else{          

                        delete lstAffiliation_Tmp;
                        oSetCtrl_C2C = null;

                    }

                }    

            }catch(DmlException oEX){

                if (oEX.getMessage().contains('INSUFFICIENT_ACCESS_OR_READONLY')){ 
                
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.Insufficient_Privileges));
                
                }else{            
                
                    ApexPages.addMessages(oEX); 
                
                } 
         
            }

        }

    }   
    //------------------------------------------------------------------------------


    //------------------------------------------------------------------------------
    // Private Methods
    //------------------------------------------------------------------------------
    @TestVisible private void initialize(){

        id_RecordType_C2A = clsUtil.getRecordTypeByDevName('Affiliation__c', 'C2A').Id;
        id_RecordType_C2C = clsUtil.getRecordTypeByDevName('Affiliation__c', 'C2C').Id;

        tSortOrder_C2A = 'ASC';
        tWarning_C2A = '';
        tSortOrder_C2C = 'ASC';
        tWarning_C2C = '';


        lstPageSize = new List<SelectOption>();
            lstPageSize.add(new SelectOption('5', '5'));
            lstPageSize.add(new SelectOption('10', '10'));
            lstPageSize.add(new SelectOption('15', '15'));
            lstPageSize.add(new SelectOption('25', '25'));
            lstPageSize.add(new SelectOption('50', '50'));
            lstPageSize.add(new SelectOption('100', '100'));
    }


    private ApexPages.StandardSetController oSetCtrl_C2A {
        
        get {
            if (oSetCtrl_C2A == null) {
                
                oSetCtrl_C2A = new ApexPages.StandardSetController(Database.getQueryLocator(tBuildQuery('C2A')));
                if (oSetCtrl_C2A.getResultSize() == 10000){
                    tWarning_C2A = 'Only the first 10.000 records are selected and visible.';
                }
                oSetCtrl_C2A.setPageSize(iPageSize_C2A);

            }
            
            return oSetCtrl_C2A;
        }
        set;

    }

    private ApexPages.StandardSetController oSetCtrl_C2C {
        
        get {
            if (oSetCtrl_C2C == null) {
                
                oSetCtrl_C2C = new ApexPages.StandardSetController(Database.getQueryLocator(tBuildQuery('C2C')));                
                if (oSetCtrl_C2C.getResultSize() == 10000){
                    tWarning_C2A = 'Only the first 10.000 records are selected and visible.';
                }
                oSetCtrl_C2C.setPageSize(iPageSize_C2C);

            }
            
            return oSetCtrl_C2C;
        }
        set;

    }

    private String tBuildQuery(String tType){

        String tSOQL = '';

        if (tType == 'C2A'){

            // BUILD C2A SOQL
            tSOQL += 'SELECT Name, Affiliation_Active__c, Affiliation_To_Account__r.Type, RecordTypeId, Affiliation_Type__c, Affiliation_To_Account__c, Affiliation_Start_Date__c, Affiliation_Primary__c, Affiliation_From_Contact__c, Affiliation_End_Date__c,Department__c,Affiliation_Position_In_Account__c, Job_Title_Description__c, Therapy__c, Therapy__r.Name';
            tSOQL += ' FROM Affiliation__c';
            tSOQL += ' WHERE ( ';
                tSOQL += ' (Affiliation_From_Contact__c = :id_Contact)';
                tSOQL += ' AND (RecordTypeId =: id_RecordType_C2A)';
            tSOQL += ')';
            if (clsUtil.isNull(tSortField_C2A, '') != ''){
                tSOQL += ' ORDER BY ' + tSortField_C2A + ' ' + tSortOrder_C2A;
            }
            tSOQL += ' LIMIT 10000';


        }else if (tType == 'C2C'){

            // BUILD C2C SOQL
            tSOQL += 'SELECT Business_Unit__c, Name, RecordTypeId, Affiliation_Type__c, Therapy__c, Affiliation_To_Contact__c, Affiliation_From_Account__c, Affiliation_To_Account__c, Affiliation_Start_Date__c, Affiliation_Primary__c, Affiliation_From_Contact__c, Affiliation_End_Date__c';
            tSOQL += ' FROM Affiliation__c';
            tSOQL += ' WHERE (';
                tSOQL += ' (Affiliation_From_Contact__c = :id_Contact OR Affiliation_To_Contact__c = :id_Contact)';
                tSOQL += ' AND (RecordTypeId = :id_RecordType_C2C AND Affiliation_Type__c = :tAffiliationType_Referring)';
            tSOQL += ')';
            if (clsUtil.isNull(tSortField_C2C, '') != ''){
                tSOQL += ' ORDER BY ' + tSortField_C2C + ' ' + tSortOrder_C2C;
            }
            tSOQL += ' LIMIT 10000';

        }

        System.debug('**BC** tBuildQuery - tSOQL (' + tType + ') : ' + tSOQL);

        return tSOQL;

    }
    //------------------------------------------------------------------------------

}