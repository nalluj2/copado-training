/**
 * Created by dijkea2 on 27-1-2017.
 */

public with sharing class bl_StakeholderMapping_Trigger {

    public static void populateMobileId(List<Stakeholder_Mapping__c> triggerNew){

        if(bl_Trigger_Deactivation.isTriggerDeactivated('StakeholderMapping_populateMobileId')) return;

        List<Stakeholder_Mapping__c> listStakeholderMappings = new List<Stakeholder_Mapping__c>();

        Set<Id> setRTIds = new Set<Id>{
            RecordTypeMedtronic.getRecordTypeByDevName('Stakeholder_Mapping__c', 'CAN_Opportunity_Stakeholder_Mapping').Id,
            RecordTypeMedtronic.getRecordTypeByDevName('Stakeholder_Mapping__c', 'CVG_Stakeholder_Mapping').Id,
            RecordTypeMedtronic.getRecordTypeByDevName('Stakeholder_Mapping__c', 'EMEA_IHS_Stakeholder_Mapping').Id,
            RecordTypeMedtronic.getRecordTypeByDevName('Stakeholder_Mapping__c', 'General_Stakeholder_Mapping').Id
        };

        for (Stakeholder_Mapping__c thisStakeholderMapping : triggerNew){
            if(setRTIds.contains(thisStakeholderMapping.RecordTypeId)) listStakeholderMappings.add(thisStakeholderMapping);
        }

        if (!listStakeholderMappings.isEmpty()){
            GuidUtil.populateMobileId(listStakeholderMappings, 'Mobile_ID__c');
        }

    }


    //--------------------------------------------------------------------------------------------------------------------
	// Create a Task when a StakeholderMapping record is created for an IHS Managed Service Opportunity or IHS Consulting Opportunity in Stage 4a or 4b
    //--------------------------------------------------------------------------------------------------------------------
	public static void IHS_CreateTask(List<Stakeholder_Mapping__c> lstTriggerNew){

        if (bl_Trigger_Deactivation.isTriggerDeactivated('StakeholderMapping_IHS_CreateTask')) return;

		Set<Id> setID_RecordType_StakeholderMapping = new Set<Id>{clsUtil.getRecordTypeByDevName('Stakeholder_Mapping__c', 'EMEA_IHS_Stakeholder_Mapping').Id};
        Set<Id> setID_RecordType_Opportunity = new Set<Id>{clsUtil.getRecordTypeByDevName('Opportunity', 'EMEA_IHS_Managed_Services').Id, clsUtil.getRecordTypeByDevName('Opportunity', 'EMEA_IHS_Consulting_Opportunity').Id}; 
		Set<String> setOpportunityStageName = new Set<String>{'4a - Closed Won (Service Activation)', '4b - Closed Won (Service Delivery)'};

		// Get the Opportunity ID's of the Processing Stakeholder Mapping Records
		Set<Id> setID_Opportunity = new Set<Id>();
		for (Stakeholder_Mapping__c oStakeholderMapping : lstTriggerNew){
			if (setID_RecordType_StakeholderMapping.contains(oStakeholderMapping.RecordTypeId)){
				setID_Opportunity.add(oStakeholderMapping.Opportunity__c);
			}
		}

		// Get additional Opportunity fields of the collected Opportunity ID's
		//	We only need to process if the Opportunity is in a specific Stage
		Map<Id, Opportunity> mapOpportunity = new Map<Id, Opportunity>(
			[
				SELECT Id, StageName, Contract_Owner__c 
				FROM Opportunity 
				WHERE 
					Id in :setID_Opportunity 
					AND RecordTypeId in :setID_RecordType_Opportunity
					AND (StageName in :setOpportunityStageName) 
			]);

		if (mapOpportunity.size() == 0) return;

		List<Task> lstTask_Insert = new List<Task>();
		for (Stakeholder_Mapping__c oStakeholderMapping : lstTriggerNew){
		
			if (setID_RecordType_StakeholderMapping.contains(oStakeholderMapping.RecordTypeId)){

				if (mapOpportunity.containsKey(oStakeholderMapping.Opportunity__c)){

					Opportunity oOpportunity = mapOpportunity.get(oStakeholderMapping.Opportunity__c);

					if (oOpportunity.Contract_Owner__c == null){
						oStakeholderMapping.addError('The Contract Owner of the Opportunity is required when you create a new Stakeholder Mapping.');
						continue;
					}

					// Create a new Task related to the StakeholderMapping (WhatId), the Contact of the StakeholderMapping (WhoId) and the Opportunity Contract Owner (OwnerId)
					Task oTask = new Task();
						oTask.Subject = 'Plan a transition lite Meeting';
						oTask.WhatId = oStakeholderMapping.Id;
						oTask.WhoId = oStakeholderMapping.Contact__c;
						oTask.OwnerId = oOpportunity.Contract_Owner__c;
						oTask.ActivityDate = Date.today().addDays(14);
						oTask.ReminderDateTime = Datetime.now().addDays(7);
					lstTask_Insert.add(oTask);
				
				}
		
			}
		
		}

		if (lstTask_Insert.size() > 0){

			// Insert the Tasks and send the notification			
			Database.DMLOptions oDMLOptions = new Database.DMLOptions();
				oDMLOptions.EmailHeader.triggerUserEmail = true;
		
			Database.insert(lstTask_Insert, oDMLOptions);

		}
	
	}
    //--------------------------------------------------------------------------------------------------------------------

}