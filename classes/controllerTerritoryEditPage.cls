public with sharing class controllerTerritoryEditPage {
    public string terrId='';
    Territory2 terr;   
     
    //To get the selected district on territory edit page
    public string selectedDistrict{get;set;}
    
    public controllerTerritoryEditPage(ApexPages.StandardController controller){
        this.terr = (Territory2)controller.getRecord();
        terrId=ApexPages.currentPage().getParameters().get('Id');              
        if(ChangeButtonPressed == true)
        {
            ChangeButtonPressed=false;
        }
    }
     public boolean ChangeButtonPressed {        
         get { return ChangeButtonPressed; }        
         set { ChangeButtonPressed = value; }    
     }
              
                 

    // Get the detail information of the choosen Territory Node
    public Territory2 getTDetail(){
        Territory2 detail;
        if(ChangeButtonPressed != true)
        {       
            if (terrId != null){
                detail = [Select Id, Name,Business_Unit__c, Territory2Type.DeveloperName, Country_UID__c,Short_Description__c,Therapy_Groups_Text__c, ParentTerritory2Id  from Territory2 where Id=:terrId];
                
            }
            else{
                Id userId = UserInfo.getUserId();
                String userTerrUID =[Select Territory_UID__c from User where id=:userId].Territory_UID__c;
                detail = [Select Id, Name,Business_Unit__c, Territory2Type.DeveloperName, Country_UID__c,Short_Description__c,Therapy_Groups_Text__c, ParentTerritory2Id  from Territory2 where Territory_UID2__c =:userTerrUID];
            }    
        }
        return detail;
    }
    public String getCrumbs(){
        String crumbs ;
        if(ChangeButtonPressed != true)
        {               
            Boolean levelFound = false;
            crumbs ='';
            Id userId = UserInfo.getUserId();
            String userTerrUID =[Select Territory_UID__c from User where id=:userId].Territory_UID__c;
            Territory2 currTerrId = [Select Id, Territory2Type.DeveloperName from Territory2 where Territory_UID2__c =:userTerrUID];
            Territory2 allChildren = new Territory2();
            if (terrId != null){
                Id tempId = terrId;
                do{
                    allChildren = [Select Id, Name,Business_Unit__c, Territory2Type.DeveloperName, Country_UID__c,Short_Description__c,Therapy_Groups_Text__c, ParentTerritory2Id  from Territory2 where Id=:tempId];
                    
    
                    crumbs = allChildren.Name + ' »' + crumbs;
                    tempId = allChildren.ParentTerritory2Id;
    
                    
                    } while (allChildren.Territory2Type.DeveloperName != 'Region');
            
                // Remove the last >>
                integer crmbIndex = crumbs.lastIndexOf('»')  ;
                if (crmbIndex !=-1){
                    crumbs = crumbs.substring(0,crmbIndex);
                }          
            }
        }
    return crumbs;        
    }
    public List<String> getTerrGroups()
    {
        List<String> groups = new List<String>();
        String TGroups = [select Therapy_Groups_Text__c from Territory2 where id=:terrId].Therapy_Groups_Text__c;
        if (TGroups != null ){
            groups = TGroups.split(';');
        }
        else {
        groups.add('Empty list');
        }
        return groups;      
    }
    public pageReference Change()
    {
        String typeabbr;
        String sfDescription;
        Id selectedDistrictID;
        Territory2 currRecord = [select ParentTerritory2Id, Territory2Type.DeveloperName, Country_UID__c, Business_Unit__c,Company__c from Territory2 where id=:terrId];
        String terrType = currRecord.Territory2Type.DeveloperName;
        if (terrType == 'Territory'){
            String districtId = [select ParentTerritory2Id from Territory2 where id=:currRecord.ParentTerritory2Id].ParentTerritory2Id;
            sfDescription = [select Short_Description__c from Territory2 where id=:districtId].Short_Description__c;
            typeabbr = 'TE';
        }
        else{
            sfDescription = [select Short_Description__c from Territory2 where id=:currRecord.ParentTerritory2Id].Short_Description__c;
            typeabbr = 'DI';
        }
        String BUname = currRecord.Business_Unit__c;        
        String abbrName =[select abbreviated_name__c from Business_Unit__c where Name=:BUname and Company__c=:currRecord.Company__c].abbreviated_name__c;
        terr.name = terr.Short_Description__c + ' (' + typeabbr + ' ' + abbrName + ' ' + sfDescription + ' ' + currRecord.Country_UID__c + ')';
        System.Debug('>>>>selectedDistrict - ' + selectedDistrict);
        //To move territory from one district to another district..start
        if(selectedDistrict!='--None--' && selectedDistrict!=null){
            System.debug('here in if...');
            selectedDistrictID=[select id,name from territory2 where name=:selectedDistrict].id;
            terr.ParentTerritory2Id =selectedDistrictID;
        }
        //To move terriotory from one district to another district..end
        
        update terr;
        ChangeButtonPressed=true;
        return null;
    }
    //To populate District picklist on Territory edit page..start
    public List<SelectOption> getDistricts(){
        Territory2 currTerritory = [select id,ParentTerritory2Id,Territory2Type.DeveloperName
                                 from Territory2 where id=:terrId];
        String salesforceId='';
        List<SelectOption> options=new List<SelectOption>();
        options.add(new SelectOption('--None--','--None--'));
        if(currTerritory.Territory2Type.DeveloperName=='Territory'){
            salesforceId =[select ParentTerritory2Id from Territory2 where
                         id=:currTerritory.ParentTerritory2Id].ParentTerritory2Id;
            List<Territory2> disTerritory=[select Name,id,ParentTerritory2Id,Territory2Type.DeveloperName
                                         from Territory2 where Territory2Type.DeveloperName=:'District'
                                         and ParentTerritory2Id=:salesforceId and 
                                         id!=:currTerritory.ParentTerritory2Id order by Name];
            for(integer i=0;i<disTerritory.size();i++){          
                options.add(new SelectOption(disTerritory[i].Name,disTerritory[i].Name));
            }
        }
        return options;
        //To populate District picklist on Territory edit page...end
    }
}