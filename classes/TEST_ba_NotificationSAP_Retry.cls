//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 01-03-2016
//  Description      : APEX Test Class to test the logic in ba_NotificationSAP_Retry
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_ba_NotificationSAP_Retry {

    //----------------------------------------------------------------------------------------
    // CREATE TEST DATA
    //----------------------------------------------------------------------------------------
    @testSetup static void createTestData() {

        Test.startTest();

        // Create Custom Setting - WebService Setting Data
        clsTestData.createCustomSettingData_WebServiceSetting(true);

        // Create Case Data
        clsTestData.createCaseData();

        // Create Service Order Data
        clsTestData.createSVMXCServiceOrderData(true);

        Test.stopTest();

    }
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
    // TESTING
    //----------------------------------------------------------------------------------------
	@isTest static void test_ba_NotificationSAP_Retry() {

		// SELECT Data
		List<SVMXC__Service_Order__c> lstServiceOrder = [SELECT Id, SVMX_SAP_Service_Order_No__c FROM SVMXC__Service_Order__c];

		// CREATE NotificationSAPLog Data
		NotificationSAPLog__c oNotificationSAPLog = new NotificationSAPLog__c();
            oNotificationSAPLog.SFDC_Object_Name__c = 'SVMXC__Service_Order__c';
            oNotificationSAPLog.SFDC_Fieldname_ID__c = 'Id';
            oNotificationSAPLog.SFDC_Fieldname_SAPID__c = 'SVMX_SAP_Service_Order_No__c';
            oNotificationSAPLog.Record_ID__c = lstServiceOrder[0].Id;
            oNotificationSAPLog.Record_SAPID__c = lstServiceOrder[0].SVMX_SAP_Service_Order_No__c;
			oNotificationSAPLog.WebServiceName__c = 'ServiceOrder_NotificationSAP';
            oNotificationSAPLog.EndpointUrl__c = 'http://144.15.228.24:33500/ws/MdtAny_SfdcInt.SRServiceOrder.webservice.providers.ws.processServiceOrder_WSDL/MdtAny_SfdcInt_SRServiceOrder_webservice_providers_ws_processServiceOrder_WSDL_Port';
           	oNotificationSAPLog.Authorization_Header__c = 'BASIC U01BWDpTbWF4IUAjdFl1';
           	oNotificationSAPLog.Request__c = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:mdt="http://msplwa107-pr.wby1-dev.medtronic.com/MdtAny_SfdcInt.SRServiceOrder.webservice.providers.ws:processServiceOrder_WSDL"><soapenv:Header/><soapenv:Body><mdt:processServiceOrder><SRServiceOrderRequest><Object>ServiceOrder</Object><Operation>UPDATE</Operation><Process>ServiceOrderUpdate</Process><SFDC_ID>a88110000004EdZAAU</SFDC_ID><SAP_ID>000004598435</SAP_ID></SRServiceOrderRequest></mdt:processServiceOrder></soapenv:Body></soapenv:Envelope>';
           	oNotificationSAPLog.Response__c = '<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body><ser-root:processServiceOrderResponse xmlns:ser-root="http://msplwa107-pr.wby1-dev.medtronic.com/MdtAny_SfdcInt.SRServiceOrder.webservice.providers.ws:processServiceOrder_WSDL" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><SRServiceOrderResponse><isSuccess>TRUE</isSuccess><msg>Successfully processed ServiceOrderUpdateRequest from SMAX</msg></SRServiceOrderResponse></ser-root:processServiceOrderResponse></soapenv:Body></soapenv:Envelope>';
           	oNotificationSAPLog.Status__c = 'RETRY';
           	oNotificationSAPLog.Direction__c = 'Outbound';
           	oNotificationSAPLog.Error_Description__c = '';
           	oNotificationSAPLog.User__c = UserInfo.getUserId();
			oNotificationSAPLog.WM_Object_Name__c = 'ServiceOrder';
			oNotificationSAPLog.WM_Operation__c = 'UPDATE';
			oNotificationSAPLog.WM_Process__c = 'ServiceOrderUpdate';
			oNotificationSAPLog.Retries__c = 0;
			oNotificationSAPLog.Retry_Cron_Expression__c = '';
		insert oNotificationSAPLog;


		Test.startTest();

		// SELECT Data
        Map<Id, NotificationSAPLog__c> mapNotificationSAPLog = new Map<Id, NotificationSAPLog__c>(
            [
                SELECT
                    Id
                    , SFDC_Object_Name__c, SFDC_Fieldname_ID__c, SFDC_Fieldname_SAPID__c, Record_ID__c, Record_SAPID__c
                    , EndpointUrl__c, Authorization_Header__c, Request__c, Response__c, Status__c, Direction__c, Error_Description__c, User__c
                    , WM_Object_Name__c, WM_Operation__c, WM_Process__c
                    , Retries__c, Retry_Cron_Expression__c
                FROM 
                    NotificationSAPLog__c 
                WHERE 
                    Status__c in ('RETRY')
                ORDER BY 
                    SFDC_Object_Name__c ASC, Record_ID__c ASC, CreatedDate DESC
            ]);

        if (mapNotificationSAPLog.size() > 0){

            ba_NotificationSAP_Retry oBatch = new ba_NotificationSAP_Retry();
                oBatch.setId_Record = mapNotificationSAPLog.keySet();
            Database.executebatch(oBatch, 1);   // Batch Size needs to be 1 because we need to call the WebMethods Web Service record by record

        }

        Test.stopTest();

	}
    //----------------------------------------------------------------------------------------
	
}
//--------------------------------------------------------------------------------------------------------------------