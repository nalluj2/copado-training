/****************************************************************************************
 * Name    : Gizmo_InboundEmailHandler 
 * Author  : Bill Shan
 * Date    : 04/03/2015 
 * Purpose : Handle the inbound emails from Gizmo 
 * Dependencies: 
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR           CHANGE
 * 20/05/2016  Jesus Lozano		MITG migration
 *****************************************************************************************/
global class Gizmo_InboundEmailHandler implements Messaging.InboundEmailHandler {

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        
        try{
            
            List<User> users = [select Id, Country_vs__c from User where Email = :email.fromAddress AND isActive=true limit 1];
            
            //This service only accept emails from a SFDC user
            User fromUser;
            
            if(users.size()>0) 
                fromUser = users[0];
            else
                fromUser = [select Id, Country_vs__c from User where Id = :UserInfo.getUserId()];
                
            //Initial task
            Task contentSharing = new Task();
            contentSharing.OwnerId = fromUser.Id;
            contentSharing.Status = 'Completed';
            contentSharing.Priority = 'Low';
            contentSharing.IsReminderSet = false;
            contentSharing.ActivityDate = date.today();
            
            if(email.subject != null)
                contentSharing.subject = email.subject;
            else
                contentSharing.subject = 'Content Sharing';
            
            if(email.plainTextBody != null)
                contentSharing.Description = email.plainTextBody;
            else    
                contentSharing.Description = 'Content Sharing, no details from the email body';
            
            if(users.size()==0)
                contentSharing.Description = '\nFrom Email: ' + email.fromAddress + '\n###################################################\n' + contentSharing.Description;
                            
            Id contentSharingRTId = [Select Id from RecordType Where sObjectType = 'Task' and DeveloperName = 'Content_Sharing'].Id;
            contentSharing.RecordTypeId = contentSharingRTId; 
            
            //Matching Contacts with to and cc addresses
            List<String> toNccAddresses = new List<String>();
            if(email.toAddresses != null) toNccAddresses.addAll(email.toAddresses);
            if(email.ccAddresses != null) toNccAddresses.addAll(email.ccAddresses);
            
            Boolean noContactMatched = true;
            
            if(toNccAddresses.size()>0)
            {
                List<String> relatedEmails = new List<String>();
                //extract the email address from "to" and "cc" addresses
                for (String address : toNccAddresses) {
                    Matcher matcher = Pattern.compile('<.+>').matcher(address);
                    
                    // Parse addresses to emails
                    if (matcher.find()) {
                        relatedEmails.add(matcher.group().replaceAll('[<>]', ''));
                    } 
                    else {
                        relatedEmails.add(address);
                    }
                }   
                                
                List<Contact> relatedContacts = [Select Id, AccountId from Contact Where Email IN :relatedEmails OR Alternate_Email__c IN :relatedEmails];
                if(relatedContacts.size()>0){
                    noContactMatched = false;
                    contentSharing.WhoId = relatedContacts[0].Id;
                    contentSharing.WhatId = relatedContacts[0].AccountId;
                    insert contentSharing;
                    
                    //Link task to multiple contacts
                    if(relatedContacts.size()>1)
                    {
                        List<TaskRelation> taskRl = new List<TaskRelation>();
                        for(Integer i=1; i<relatedContacts.size(); i++)
                        {
                            taskRl.add(new TaskRelation(TaskId = contentSharing.Id, IsWhat = false, RelationId = relatedContacts[i].Id));
                        }
                        insert taskRl;
                    }
                }
            }
            
            //No Contact matched, link the task to the country account
            if(noContactMatched){
                               
                List<Account> countryAccounts = [Select Id, Name from Account Where RecordType.DeveloperName = 'MDT_Account' and Account_Country_vs__c = :fromUser.Country_vs__c];
                
                Id countryAccountId;
                
                if(countryAccounts.size() == 1) countryAccountId = countryAccounts[0].Id;
                else{
                	
                	for(Account countryAccount : countryAccounts){
                		
                		if(countryAccount.Name.toUpperCase().endsWith(fromUser.Country_vs__c)){
                			countryAccountId = countryAccount.Id;
                			break;
                		}                			
                	}                	
                }
                
                contentSharing.WhatId=countryAccountId;
                contentSharing.Description = Label.Unmatched_Email_Address + '\n###################################################\n' + contentSharing.Description;
                contentSharing.Status = 'Not Started';
                contentSharing.IsReminderSet = true;
                contentSharing.ReminderDateTime = System.now();
                insert contentSharing;  
            }
            
        }
        catch(Exception e) {
            result.success = false;
            result.message = 'Oops, Gizmo_InboundEmailHandler failed.' + e.getStackTraceString();
        }
        
        return result;
    }
}