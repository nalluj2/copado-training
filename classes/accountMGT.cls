/**
 * Creation Date :  20091124
 * Description : 	All the Business Logic / Methods behind Account Object (trigger calls)
 * Author : 		MC / ABSI
 */

public class accountMGT {

/**
 * Description : 	Before Delete Apex Trigger : Prevents the deletion of any account which is an SAP account
 * Specifications : New Rule --> NOBODY can delete SAP Accounts ; 
 */ 
 	public static void cannnotDeleteSAPAccount(Account[] accs){ 
	       for (Account a : accs){ 
		        if (a.SAP_ID__c != null || a.RecordTypeId == FinalConstants.sapAccountRecordTypeId){
		        	a.addError('You can\'t delete SAP Accounts.'); 
		        }  
 		   }
 	}
}