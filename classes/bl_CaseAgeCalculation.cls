/*
* Class with the business logic to calculate the Age of OMA Cases in Working Days
* Created on 11/04/2014 by Jesus
*/
public without sharing class bl_CaseAgeCalculation {
	
	// All Business Hours in the Org by Id	
	public static Map<Id, BusinessHours> bHoursById{
		
		get{
			if(bHoursById == null) bHoursById = new Map<Id, BusinessHours>([Select Id, TimeZoneSidKey, IsDefault from BusinessHours]);
						
			return bHoursById;
		}
		 
		 set;
	}
	// Default Business Hours. To be used on Cases without specified Business Hours
	public static BusinessHours defaultBHours{
		
		get{
			
			if(defaultBHours == null){
				
				for(BusinessHours bh : bHoursById.values()){
										
					if(bh.IsDefault == true){
						 
						 defaultBHours = bh;
						 break;
					}
				}				
			}
			
			return defaultBHours;
		}
		
		set;
	}
	
	// This static list is used by the Trigger on Case before Update to avoid re-calculating
	// for the same Case many times in a single execution context
	public static Set<Id> alreadyCalculated = new Set<Id>();
	// Record Type Ids for OMA Cases
	public static Set<Id> omaRecordTypes{
		
		get{
			
			if(omaRecordTypes == null){
				
				omaRecordTypes = new Set<Id>();
				
				Map<String, Schema.RecordTypeInfo> rtByDevName = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName();
		
				for(OMA_Record_Types__c omaRT : OMA_Record_Types__c.getAll().values()){
					
					if(rtByDevNAme.containsKey(omaRT.Developer_Name__c)){
						
						omaRecordTypes.add(rtByDevName.get(omaRT.Developer_Name__c).getRecordTypeId());
					}					
				}
			}
			
			return omaRecordTypes;
		}
		
		set;
	}
	
	
	public static Long getOpenCaseAgeInWorkingDays(Case inputCase){
		
		//We use default Business Hours if none specified
		if(inputCase.BusinessHoursId == null){
			inputCase.BusinessHoursId = defaultBHours.Id;
		}
		
		BusinessHours bHours = bHoursById.get(inputCase.BusinessHoursId);		
		Timezone caseTZ = Timezone.getTimeZone(bHours.TimeZoneSidKey);
						
		Date startDate;
		Date endDate;
		
		if(inputCase.Date_Received_Date__c != null){
			//This date is already in Case TZ
			startDate = inputCase.Date_Received_Date__c;
		}else{
			//This date is in User TZ, we need to get the date in Case TZ 
			String[] dateInCaseTZ = inputCase.CreatedDate.format('yyyy/MM/dd', caseTZ.getID()).split('/');
			
			Integer year = Integer.valueOf(dateInCaseTZ[0]);
			Integer month = Integer.valueOf(dateInCaseTZ[1]);
			Integer day = Integer.valueOf(dateInCaseTZ[2]);
			
			//This date is already in Case TZ
			startDate = Date.newInstance(year, month, day);	
		}
		
		//For Open cases
		endDate = Date.today();
		
		// We set a hardcoded time of 4AM and 9PM to count only full days. 
		//It could be other times out of working hours, but to make the code more safe againts possible bugs with the Timezones I chose this value
		Time startHour = Time.newInstance(4,0,0,0);
		Time endHour = Time.newInstance(21,0,0,0);
		
		DateTime startDateTime = DateTime.newInstanceGmt(startDate, startHour);
		DateTime endDateTime = DateTime.newInstanceGmt(endDate, endHour);
		
		Integer startOffset = caseTZ.getOffset(startDateTime);
		Integer endOffset = caseTZ.getOffset(endDateTime);
		
		//4am for each Date in the Case TZ		
		DateTime caseStart = DateTime.newInstance(startDateTime.getTime() - startOffset);
		DateTime caseEnd = DateTime.newInstance(endDateTime.getTime() - endOffset);
		
		Long ageInMilis = BusinessHours.diff(bHours.Id, caseStart, caseEnd);
		
		//We divide the result in milisecons by 60*60*1000*9, so milisecons per hour and 9 hours per working day
		Long result = Math.roundToLong(ageInMilis/32400000);
			
		return result;
	}
	
	public static Long getClosedCaseAgeInWorkingDays(Case inputCase){
		
		//We use default Business Hours if none specified
		if(inputCase.BusinessHoursId == null){
			inputCase.BusinessHoursId = defaultBHours.Id;
		}
		
		BusinessHours bHours = bHoursById.get(inputCase.BusinessHoursId);		
		Timezone caseTZ = Timezone.getTimeZone(bHours.TimeZoneSidKey);
						
		Date startDate;
		Date endDate;
		
		if(inputCase.Date_Received_Date__c != null){
			//This date is already in Case TZ
			startDate = inputCase.Date_Received_Date__c;
		}else{
			//This date is in User TZ, we need to get the date in Case TZ 
			String[] dateInCaseTZ = inputCase.CreatedDate.format('yyyy/MM/dd', caseTZ.getID()).split('/');
			
			Integer year = Integer.valueOf(dateInCaseTZ[0]);
			Integer month = Integer.valueOf(dateInCaseTZ[1]);
			Integer day = Integer.valueOf(dateInCaseTZ[2]);
			
			//This date is already in Case TZ
			startDate = Date.newInstance(year, month, day);
		}
		
		if(inputCase.Date_Closed_Date__c != null){
			//This date is already in Case TZ
			endDate = inputCase.Date_Closed_Date__c;
		}else{
			//This date is in User TZ, we need to get the date in Case TZ 
			String[] dateInCaseTZ = inputCase.ClosedDate.format('yyyy/MM/dd', caseTZ.getID()).split('/');
			
			Integer year = Integer.valueOf(dateInCaseTZ[0]);
			Integer month = Integer.valueOf(dateInCaseTZ[1]);
			Integer day = Integer.valueOf(dateInCaseTZ[2]);
			
			//This date is already in Case TZ
			endDate = Date.newInstance(year, month, day);
		}
		
		// We set a hardcoded time of 4AM and 9PM to count only full days. 
		//It could be other times out of working hours, but to make the code more safe againts possible bugs with the Timezones I chose this value
		Time startHour = Time.newInstance(4,0,0,0);
		Time endHour = Time.newInstance(21,0,0,0);
		
		DateTime startDateTime = DateTime.newInstanceGmt(startDate, startHour);
		DateTime endDateTime = DateTime.newInstanceGmt(endDate, endHour);
		
		Integer startOffset = caseTZ.getOffset(startDateTime);
		Integer endOffset = caseTZ.getOffset(endDateTime);
		
		//4am of each Date in the Case TZ		
		DateTime caseStart = DateTime.newInstance(startDateTime.getTime() - startOffset);
		DateTime caseEnd = DateTime.newInstance(endDateTime.getTime() - endOffset);
		
		Long ageInMilis = BusinessHours.diff(bHours.Id, caseStart, caseEnd);
		
		//We divide the result in milisecons by 60*60*1000*9, so milisecons per hour and 9 hours per working day
		Long result = Math.roundToLong(ageInMilis/32400000);
			
		return result;
	}


    // Bart Caelen - 14-02-2017 - CR-11545 - START
	public static Long getCaseMeanTimeToRepair(Case oCase, Date dStart, Date dEnd){

		//We use default Business Hours if none specified
		if(oCase.BusinessHoursId == null){
			oCase.BusinessHoursId = defaultBHours.Id;
		}
		
		BusinessHours oBusinessHours = bHoursById.get(oCase.BusinessHoursId);		
		Timezone oTimezone_Case = Timezone.getTimeZone(oBusinessHours.TimeZoneSidKey);
						
		// We set a hardcoded time of 4AM and 9PM to count only full days. 
		// It could be other times out of working hours, but to make the code more safe againts possible bugs with the Timezones I chose this value
		Time startHour = Time.newInstance(4,0,0,0);
		Time endHour = Time.newInstance(21,0,0,0);
		
		DateTime dtStart = DateTime.newInstanceGmt(dStart, startHour);
		DateTime dtEnd = DateTime.newInstanceGmt(dEnd, endHour);
		
		Integer iStartOffset = oTimezone_Case.getOffset(dtStart);
		Integer iEndOffset = oTimezone_Case.getOffset(dtEnd);
		
		//4am of each Date in the Case TZ		
		DateTime dtStart_Result = DateTime.newInstance(dtStart.getTime() - iStartOffset);
		DateTime dtEnd_Result = DateTime.newInstance(dtEnd.getTime() - iEndOffset);
		
		Long lAgeInMilis = BusinessHours.diff(oBusinessHours.Id, dtStart_Result, dtEnd_Result);
		
		//We divide the result in milisecons by 60*60*1000*9, so milisecons per hour and 9 hours per working day
		Long lResult = Math.roundToLong(lAgeInMilis/32400000);
			
		return lResult;

	}
    // Bart Caelen - 14-02-2017 - CR-11545 - STOP

}