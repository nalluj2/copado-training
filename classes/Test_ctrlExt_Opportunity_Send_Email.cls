@isTest
private class Test_ctrlExt_Opportunity_Send_Email {
    
    private static testmethod void testController(){
    	
    	Account acc = new Account();
    	acc.FirstName = 'Test';
    	acc.LastName = 'Account';
    	acc.Account_Country_vs__c = 'CANADA';
    	acc.SAP_Id__c = '0123456789';
    	acc.SAP_Channel__c = '30';
    	acc.RecordTypeId = [Select Id from RecordType where SObjectType = 'Account' AND DeveloperName = 'SAP_Patient'].Id;
    	insert acc;
    	
    	Opportunity opp = new Opportunity();
    	opp.Name = 'Test Opp';
    	opp.StageName = 'New';
    	opp.AccountId = acc.Id;
    	opp.CloseDate = Date.today().addMonths(2);
    	insert opp;
    	
    	CAN_DIB_Department_Emails__c emailSetting1 = new CAN_DIB_Department_Emails__c();
    	emailSetting1.Name = 'Inside Sales';
    	emailSetting1.Email__c = 'diabetes.is@medtronic.com';
    	emailSetting1.Department__c = 'Diabetes IS';
    	emailSetting1.Order__c = 1;
    	
    	CAN_DIB_Department_Emails__c emailSetting2 = new CAN_DIB_Department_Emails__c();
    	emailSetting2.Name = 'Credit Check';
    	emailSetting2.Email__c = 'diabetes.credit.check@medtronic.com';
    	emailSetting2.Department__c = 'Diabetes Credit Check';
    	emailSetting2.Order__c = 2;
    	
    	insert new List<CAN_DIB_Department_Emails__c>{emailSetting1, emailSetting2};
    	
    	Test.startTest();
    	
    	ctrlExt_Opportunity_Send_Email controller = new ctrlExt_Opportunity_Send_Email(new ApexPages.standardController(opp));
    	
    	System.assert(controller.options.size() == 2);
    	
    	PageReference pr = controller.goToEmail();
    	
    	System.assert( pr != null);
    }
}