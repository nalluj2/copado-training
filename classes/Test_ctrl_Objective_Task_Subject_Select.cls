@isTest
private class Test_ctrl_Objective_Task_Subject_Select {
	
	private static testmethod void testObjectiveSubjectSelection(){
		
		Company__c cmpny = new Company__c();		
        cmpny.name='Europe';
        cmpny.CurrencyIsoCode = 'EUR';
        cmpny.Current_day_in_Q1__c=56;
        cmpny.Current_day_in_Q2__c=34; 
        cmpny.Current_day_in_Q3__c=5; 
        cmpny.Current_day_in_Q4__c= 0;   
        cmpny.Current_day_in_year__c =200;
        cmpny.Days_in_Q1__c=56;  
        cmpny.Days_in_Q2__c=34;
        cmpny.Days_in_Q3__c=13;
        cmpny.Days_in_Q4__c=22;
        cmpny.Days_in_year__c =250;
        cmpny.Company_Code_Text__c = 'T35';
		insert cmpny;
		
		Business_Unit_Group__c bug =  new Business_Unit_Group__c();
        bug.Master_Data__c =cmpny.id;
        bug.name='RTG';      
        insert bug;
		
		Business_Unit__c bu =  new Business_Unit__c();
        bu.Company__c =cmpny.id;
        bu.name='RTG BU';
        bu.Business_Unit_Group__c = bug.Id;
        bu.Account_Plan_Activities__c=true;        
        insert bu;
        
        Sub_Business_Units__c sbu = new Sub_Business_Units__c();
        sbu.name='RTG sBU';
        sbu.Business_Unit__c=bu.id;
		insert sbu;
		
		DIB_Country__c country = new DIB_Country__c();
		country.Company__c = cmpny.Id;
		country.Name = 'Unit Test Country';
		country.Country_ISO_Code__c = 'ZZ';
		country.CurrencyIsoCode = 'EUR';
		insert country;
		
		Business_Objective__c sbuObjective = new Business_Objective__c();
		sbuObjective.Name = 'SBU Objective';
		sbuObjective.Explanation__c = 'test explanation';
		sbuObjective.Type__c = 'Quantitative';
		sbuObjective.Active__c = true;
		sbuObjective.Sub_Business_Unit__c = sbu.Id;
		insert sbuObjective;
		
		Objective_Subject__c subject = new Objective_Subject__c();
		subject.Name = 'Test Subject';
		subject.Business_Objective__c = sbuObjective.Id;
		insert subject;
				
		Account testAccount = new Account();
		testAccount.Name = 'RTG Test Account';
		insert testAccount;
		
		Account_Plan_2__c accPlan = new Account_Plan_2__c();
		accPlan.Account__c = testAccount.Id;
		accPlan.Account_Plan_Level__c = 'Sub Business Unit';
		accPlan.Sub_Business_Unit__c = sbu.Id;
		insert accPlan;
		
		Account_Plan_Objective__c objective = new Account_Plan_Objective__c();
		objective.Account_Plan__c = accPlan.Id;
		objective.Business_Objective__c = sbuObjective.Id;
		objective.Description__c = 'Unit Test AP Objective';
		objective.Status__c = 'Planned';
		objective.Target_Date__c = Date.today().addDays(7);		
		insert objective;
		
		Test.startTest();
				
		ApexPages.currentPage().getParameters().put('objId', objective.Id);
		ApexPages.currentPage().getParameters().put('retURL', '/' + accPlan.Id);
		
		ctrl_Objective_Task_Subject_Selection con = new ctrl_Objective_Task_Subject_Selection();
		
		System.assert(con.objectiveSubjects != null);
		System.assert(con.objectiveSubjects.size() == 2);
		System.assert(con.objectiveSubjects[0].getValue() == subject.Name);
		
		con.selectedSubject = subject.Name;
		
		PageReference taskRef = con.createTask();
		
		System.assert(taskRef.getURL().startsWith('/00T/e'));
		System.assert(taskRef.getParameters().get('what_id') == objective.Id);
		System.assert(taskRef.getParameters().get('tsk5') == subject.Name);
		
		con.cancel();
	} 	   
	
	private static testmethod void testObjectiveSubjectError(){
						
		ctrl_Objective_Task_Subject_Selection con = new ctrl_Objective_Task_Subject_Selection();
		
		System.assert(con.objectiveSubjects == null);
	}
}