/**
 * Creation Date :  20140130
 * Description : 	Service class to follow and unfollow chatter for the ProjectManagement requestor
 * Author : 		Bluewolf - Paul Rice
 * Change Log:
 */
public without sharing class ProjectManagementFollowService {
		
	public static Set<String> finalStatus { 
		get{
			Set<String> finals = new Set<String>();
			List<Project_Final_Stage__c> finalStages = Project_Final_Stage__c.getall().values();
			for (Project_Final_Stage__c finalStage:finalStages){
				finals.add(finalStage.stage__c.trim());
			}
			finalStatus = finals;
			return finalStatus;
		} 
		set;
	}

	//on after insert project insert add a chatter feed for the project requestor
	public static void addChatterFollow(Map<ID,Project_Management__c> newProjects){
		
	    List<EntitySubscription> addFollowersList=new List<EntitySubscription>();

	    For(Project_Management__c project:newProjects.values()){
	    	if (project.Requestor__c != null){
		    	EntitySubscription newFollower = new EntitySubscription(ParentId=project.id,SubscriberId=project.Requestor__c);
				addFollowersList.add(newFollower);
	    	}										
	    }
	    
	   	if(addFollowersList.size()>0){

			try {
				insert addFollowersList;
			}catch(DMLException exc){
	            for (Integer index = 0; index < exc.getNumDml(); index++) {
	                ID projectId = addFollowersList[exc.getDmlIndex(index)].parentid;
	                newProjects.get(projectId).addError(exc.getDmlMessage(index));
	            }
			}
	    }
	}
   
	// on before update, if the status is a finalStatus remove the chatter entry
	// on before update, if the status is not a finalStatus but the follower has changed insert the new requestor chatter feed and delete the old chatter feed
	public static void updateChatterFollow(Map<ID,Project_Management__c> newProjects,Map<ID,Project_Management__c> oldProjects){

		List<EntitySubscription> currentChatterFeeds = [Select SubscriberId, ParentId, Id From EntitySubscription where ParentId in: newProjects.keySet() limit 1000];
		List<EntitySubscription> newFollowerChatterFeeds = new List<EntitySubscription> ();
		List<EntitySubscription> removeChatterFeeds = new List<EntitySubscription> ();
	    Map<id,Set<id>> parentSubscribersMap = getParentSubscribersMap(currentChatterFeeds);

		// delete the old requestor and add the new requestor if they do not already exist
		
		for (EntitySubscription chatterFeed : currentChatterFeeds){
			if (oldProjects.get(chatterFeed.parentId).Requestor__c == chatterFeed.SubscriberId){
				removeChatterFeeds.add(chatterFeed);
				
				//check if a the project chatter feed for the new requestor already exists
		    	if (!isExistingProjectFollower(parentSubscribersMap,newProjects.get(chatterFeed.parentId))){
		    		// if not create a new feed
					newFollowerChatterFeeds.add(new EntitySubscription(  
            						ParentId=chatterFeed.parentId,
									SubscriberId=newProjects.get(chatterFeed.parentId).Requestor__c));
				}
			}
		}
		
		// EntitySubscriptions cannot be updated, only inserted and deleted		
		try {
			insert newFollowerChatterFeeds;
		}catch(DMLException exc){
           for (Integer index = 0; index < exc.getNumDml(); index++) {
                ID projectId = newFollowerChatterFeeds[exc.getDmlIndex(index)].parentid;
                newProjects.get(projectId).addError(exc.getDmlMessage(index));
            }
		}
		try {
			delete removeChatterFeeds;
		}catch(DMLException exc){
            for (Integer index = 0; index < exc.getNumDml(); index++) {
                ID projectId = removeChatterFeeds[exc.getDmlIndex(index)].parentid;
                newProjects.get(projectId).addError(exc.getDmlMessage(index));
            }			
		}         	
	}
	
	// for chatter subscriptions return a map of subscriber ids for each project id
	public static Map<id,Set<id>> getParentSubscribersMap(List<EntitySubscription> currentChatterFeeds){
		
		Map<id,Set<id>> parentIdsubscriberIdsChatterMap = new Map<id,Set<id>>();		
		for (EntitySubscription chatter : currentChatterFeeds){
			if (!parentIdsubscriberIdsChatterMap.containsKey(chatter.parentId)){
				parentIdsubscriberIdsChatterMap.put(chatter.parentId,new Set<id>());
			} 
			parentIdsubscriberIdsChatterMap.get(chatter.parentId).add(chatter.SubscriberId); 
		}
		return parentIdsubscriberIdsChatterMap; 		
	}
	
	private static boolean isExistingProjectFollower(Map<id,Set<id>> parentSubscribersMap,Project_Management__c project){
		if (parentSubscribersMap.get(project.id).contains(project.requestor__c)){
			return true;
		}
		return false;
	}
		
	// Return projects that have moved to a final status
	public static Map<ID,Project_Management__c> finalStatusChatterFilter(Map<ID,Project_Management__c> projectUpdates, Map<ID,Project_Management__c> oldProjects){
		
		Map<ID,Project_Management__c> chatterFinalProjects = new Map<ID,Project_Management__c>();
		// if the project stage has changed, the old stage was not final and the new stage is final include return the project
		
		for (Project_Management__c projectUpdate : projectUpdates.values()){
			System.debug(' projectUpdate.stage__c ='+projectUpdate.stage__c+'=');
			System.debug(' oldProjects.get(projectUpdate.id).stage__c ='+oldProjects.get(projectUpdate.id).stage__c+'=');
			System.debug(' finalStatus ='+finalStatus+'=');
			
			if ( projectUpdate.stage__c != oldProjects.get(projectUpdate.id).stage__c 			
				&& finalStatus.contains(projectUpdate.stage__c)
				&& !finalStatus.contains(oldProjects.get(projectUpdate.id).stage__c))
			{
				System.debug('final projectUpdate ='+projectUpdate+'=');
				
				chatterFinalProjects.put(projectUpdate.id,projectUpdate);
			}
		}
		return chatterFinalProjects;
	}
	
	// Return project requestor changes where the status is not final
	public static Map<ID,Project_Management__c> chatterFollowerChangeFilter(Map<ID,Project_Management__c> projectUpdates, Map<ID,Project_Management__c> oldProjects){
		
		Map<ID,Project_Management__c> chatterUpdateProjects = new Map<ID,Project_Management__c>();
		for (Project_Management__c projectUpdate : projectUpdates.values()){
			if ( projectUpdate.Requestor__c != null 
				&& projectUpdate.Requestor__c != oldProjects.get(projectUpdate.id).Requestor__c  			
				&& !finalStatus.contains(projectUpdate.stage__c)){
				chatterUpdateProjects.put(projectUpdate.id,projectUpdate);
			}
		}
		return chatterUpdateProjects;
	}

	// remove the project requestor only from the project chatter feed
	public static void removeChatterFollow(Map<ID,Project_Management__c> unfollowProjects){
		Set<Id> requestorIds = new Set<ID>();
		for (Project_Management__c unfollowProject : unfollowProjects.values()){
			requestorIds.add(unfollowProject.Requestor__c);
		}

		Map<String,EntitySubscription> keyChatterMap = new Map<String,EntitySubscription>();
		List<EntitySubscription> currentChatterFeeds = [Select SubscriberId, ParentId, Id From EntitySubscription where ParentId in: unfollowProjects.keySet() and SubscriberId in : requestorIds limit 1000];		
		for (EntitySubscription chatterFeed: currentChatterFeeds){
			keyChatterMap.put(''+chatterFeed.SubscriberId+chatterFeed.ParentId,chatterFeed);
		}
		
		List<EntitySubscription> removeChatterFeeds = new List<EntitySubscription> ();

		// delete the chatter feed for the project requestor		
		for (Project_Management__c unfollowProject : unfollowProjects.values()){
			if (keyChatterMap.containsKey(''+unfollowProject.Requestor__c+unfollowProject.id)){
				removeChatterFeeds.add(keyChatterMap.get(''+unfollowProject.Requestor__c+unfollowProject.id));
			}
		}
		try {
			delete removeChatterFeeds;
		}catch(DMLException exc){
			// add any Project Management update dml errors to the project management trigger map
            for (Integer index = 0; index < exc.getNumDml(); index++) {
                ID projectId = removeChatterFeeds[exc.getDmlIndex(index)].parentid;
                unfollowProjects.get(projectId).addError(exc.getDmlMessage(index));
            }
		}
	               	
	}

}