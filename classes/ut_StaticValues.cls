/*
 * Description		: Utility class to support static values 		 
 * Author        	: Patrick Brinksma
 * Created Date    	: 01-08-2013
 */
public class ut_StaticValues {

	// Interface Names - used to retrieve values from Custom Settings
	public static final String INTERFACE_NAME_SAP_OPPORTUNITY = 'SAP Opportunity';

	// RecordType Developer Names
	// For Relationship (Affiliations)
	public static final String RTDEVNAME_AFF_A2A = 'A2A';
	
	// For Opportunity
	public static final String RTDEVNAME_CAN_DIB_OPPORTUNITY = 'CAN_DIB_OPPORTUNITY';
	//public static final String RTDEVNAME_EUR_DIB_OPPORTUNITY = 'EUR_DIB_Opportunity';
	
	// For Account
	public static final String RTDEVNAME_SAP_PATIENT = 'SAP_Patient';
	
	// For Lead
	public static final String RTDEVNAME_CAN_DIB_LEAD = 'CAN_DiB_Lead';
	public static final String RTDEVNAME_CAN_DIB_LEAD_CONVERSION = 'CAN_DiB_Lead_Conversion';
	
	// BUCKET for Country Assignment Rules
	public static final String COUNTRY_ASSIGNMENT_BUCKET = 'BUCKET';
	
	// Company Names
	public static final String COMPANY_EUROPE = 'Europe';
	
	// Business Unit Group Names
	public static final String BUG_CVG = 'CVG';
	
	// Business Unit Names
	public static final String BU_CRHF = 'CRHF';

	// Account Plan Level Naming
	public static final String APLEVEL_BUG = 'Business Unit Group';
	public static final String APLEVEL_BU = 'Business Unit';
	public static final String APLEVEL_SBU = 'Sub Business Unit';
	
	// Account Team Member
	public static final String ACCOUNT_TEAM_MEMBER_ROLE_TEAM_LEADER = 'Acc Team Leader';
	
	// Insurance Verification Insurer Type
	public static final String INSURER_TYPE_PRIMARY = 'Primary';
	public static final String INSURER_TYPE_SECONDARY = 'Secondary';	//- BC - 20140422 - CR-2698 - ADDED
	public static final String INSURER_TYPE_ADDITIONAL = 'Additional';	//- BC - 20140422 - CR-2698 - ADDED
	
	// Account Type values
	public static final String ACCNT_TYPE_PER_BU = 'Per BU';
	public static final String ACCNT_TYPE_PER_SBU = 'Per SBU';

	// Set used to collect Ids of Opportunities that already have been sent to SAP
	// This is to prevent them from being sent 
	public static Set<Id> setOfOpportunityIdAlreadySent = new Set<Id>();
	
	// Prevent triggers to run again
	public static Set<String> setOfTriggersExecuted = new Set<String>();
	
	/*
	 * Description      : Method to get user profile name
	 * Author           : Patrick Brinksma
	 * Created Date     : 11-08-2013
	 */ 	
	static String userProfileName;
	public static String getUserProfileName(){
		if (userProfileName == null){
			userProfileName = [select Name from Profile where Id = :Userinfo.getProfileId()].Name;
		}
		return userProfileName;
	}
	
	/*
	 * Description      : Method to get Business Unit Id By Name + Company Name
	 * Author           : Patrick Brinksma
	 * Created Date     : 21-08-2013
	 */ 	
	static Map<String, Id> mapOfBusinessUnitNameToId;
//	public static Map<String, Id> getBusinessUnitIdByName(Set<String> setOfBUName, String companyName){
	public static Map<String, Id> getBusinessUnitIdByName(Set<String> setOfBUName, Set<String> setOfCompanyName){
		// Initiate Map if not done yet
		if (mapOfBusinessUnitNameToId == null){
			mapOfBusinessUnitNameToId = new Map<String, Id>();
		}
		// Validate which BU names are already retrieved
		for (String buName : setOfBUName){
			for (String companyName : setOfCompanyName){
				if (mapOfBusinessUnitNameToId.get(buName + companyName) != null){
					setOfBUName.remove(buName);
				} 
			}
		}
		// Retrieve any BU Name/Id if not already done
		if (!setOfBUName.isEmpty()){
			List<Business_Unit__c> listOfBU = [select Id, Name, Company__r.Name from Business_Unit__c where Name in :setOfBUName and Company__r.Name in :setOfCompanyName];
			for (Business_Unit__c thisBU : listOfBU){
						
					mapOfBusinessUnitNameToId.put(thisBU.Name + thisBU.Company__r.Name, thisBU.Id);
				
			}
		}
		return mapOfBusinessUnitNameToId;
	}

	/*
	 * Description      : Method to get Business Unit Group Id By Name + Company Name
	 * Author           : Patrick Brinksma
	 * Created Date     : 21-08-2013
	 *
	 * @deprecated 	Christophe Saenen	- CR-11736	20160502	- function is not used anymore
	 */ 	
	static Map<String, Id> mapOfBusinessUnitGroupNameToId;
	public static Map<String, Id> getBusinessUnitGroupIdByName(Set<String> setOfBUGName, Set<String> setOfCompanyName) {	
		// Initiate Map if not done yet
		if (mapOfBusinessUnitGroupNameToId == null){
			mapOfBusinessUnitGroupNameToId = new Map<String, Id>();
		}
		// Validate which BUG names are already retrieved
		for (String bugName : setOfBUGName){
			for (String companyName : setOfCompanyName){
				if (mapOfBusinessUnitGroupNameToId.get(bugName + companyName) != null){
					setOfBUGName.remove(bugName);
				} 	
			}	
		}
		// Retrieve any BU Name/Id if not already done
		if (!setOfBUGName.isEmpty()){
			List<Business_Unit_Group__c> listOfBUG = [select Id, Name from Business_Unit_Group__c where Name in :setOfBUGName and Master_Data__r.Name in :setOfCompanyName];
			for (Business_Unit_Group__c thisBUG : listOfBUG){
				for (String companyName : setOfCompanyName){			
					mapOfBusinessUnitGroupNameToId.put(thisBUG.Name + companyName, thisBUG.Id);
				}
			}
		}
		return mapOfBusinessUnitGroupNameToId;
	}
}