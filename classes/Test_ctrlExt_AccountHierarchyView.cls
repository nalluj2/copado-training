/*
 * Description		: Test class for ctrlExt_AccountHierarchyView
 *						IMPORTANT: Due to setup of data that the functionality is dependent on, seeAllData is true
 * Author        	: Patrick Brinksma
 * Created Date    	: 18-12-2013
 */
@isTest(seeAllData=true) private class Test_ctrlExt_AccountHierarchyView {

/*
	@isTest static void testAccountHierarchyView0(){


		// Create Test Data
		clsTestData_MasterData.tCountryCode = 'CA';
		clsTestData_MasterData.createCountry(); 

		clsTestData_Account.tCountry_Account = 'CANADA';
		clsTestData_Account.idRecordType_Account = clsUtil.getRecordTypeByDevName('Account', 'CAN_Buying_Group').Id;
		Account oAccount = clsTestData_Account.createAccount()[0];

		clsTestData_Affiliation.tAccountTypeCountry_ISOCode = 'CA';
		clsTestData_Affiliation.createAccountTypeCountry();

		clsTestData_Affiliation.iRecord_Affiliation_A2A = 10;
		clsTestData_Affiliation.createAffiliation_A2A();

		clsTestData_Affiliation.createRelationShipType();

		clsTestData_Affiliation.createAccountType();
		clsTestData_Affiliation.createAccountTypeCountry();

		Id id_AccountType = [SELECT Id, Account_Type__c FROM Account_Type__c WHERE Account_Type__c = 'Buying Group'][0].Id;

		// Start test		
		Test.startTest();
		
		ApexPages.Standardcontroller oCTRL_STD = new ApexPages.Standardcontroller(oAccount);
		ctrlExt_AccountHierarchyView oCTRL = new ctrlExt_AccountHierarchyView(oCTRL_STD);
		// call methods

		oCTRL.selectedAccntId = oAccount.Id;
		oCTRL.selectedParentAccntTypeId = id_AccountType;
		oCTRL.selectParent();

		oCTRL.refreshTree();
		oCTRL.getTopNode();
		oCTRL.getAccountTypeLegend();

		// Get tree data
		String treeData = oCTRL.getTreeData();

		oCTRL.selectedParentAccntTypeId = oAccount.Id;
		oCTRL.selectParent();
		treeData = oCTRL.getTreeData();

		Test.stopTest();
		
	}
*/
	@isTest static void testAccountHierarchyView01(){

		// Use Germany as country to test with, so DEPENDENCT ON DATA		
		Id countryId = [select Id from DIB_Country__c where Name = 'Canada'].Id;
		// For test make sure the Unique parent of the Sub Buying Group is set to No
		Account_Type_Country__c atc = [select Id, Unique_Parent__c from Account_Type_Country__c where Account_Type__r.Account_Type__c = 'Sub Buying Group' and country__c = :countryId];
		atc.Unique_Parent__c = 'No';
		update atc;
		
		// Setup some test accounts
		String randVal = Test_TestDataUtil.createRandVal();
		// Create BG, SBG and Non-SAP accounts
		//List<Account> listOfBG = Test_TestDataUtil.createBGAccounts(randVal, 'CANADA', 2);
		//List<Account> listOfSBG = Test_TestDataUtil.createSBGAccounts(randVal, 'CANADA', 1);
		//List<Account> listOfNS = Test_TestDataUtil.createNONSAPAccounts(randVal, 'CANADA', 1);
        
		List<Account> listOfBG = new List<Account>();
        List<Account> listOfSBG = new List<Account>();
        List<Account> listOfNS = new List<Account>();
        
        Account recBGAccount = new Account();
        Account recSBGAccount = new Account();
        Account recNonSAPAccount = new Account();
        
        recBGAccount.Name = 'Can Buying Group 1';
        recBGAccount.Type = 'Buying Group';
        recBGAccount.RecordTypeId = RecordTypeMedtronic.getRecordTypeByDevName('Account', 'CAN_Buying_Group').Id;
        recBGAccount.Account_Country_vs__c = 'CANADA';
        
        listOfBG.add(recBGAccount);
        
        recBGAccount = new Account();
        
        recBGAccount.Name = 'Can Buying Group 2';
        recBGAccount.Type = 'Buying Group';
        recBGAccount.RecordTypeId = RecordTypeMedtronic.getRecordTypeByDevName('Account', 'CAN_Buying_Group').Id;
        recBGAccount.Account_Country_vs__c = 'CANADA';
        
        listOfBG.add(recBGAccount);
        
        recSBGAccount.Name = 'Sub Buying Group 1';
        recSBGAccount.Type = 'Sub Buying Group';
        recSBGAccount.RecordTypeId = RecordTypeMedtronic.getRecordTypeByDevName('Account', 'CAN_Buying_Group').Id;
        recSBGAccount.Account_Country_vs__c = 'CANADA';
        
        listOfSBG.add(recSBGAccount);
        
		recNonSAPAccount.Name = 'Non SAP Account 1';
        recNonSAPAccount.RecordTypeId = RecordTypeMedtronic.getRecordTypeByDevName('Account', 'NON_SAP_Account').Id;
        recNonSAPAccount.Account_Country_vs__c = 'CANADA';
        
        listOfNS.add(recNonSAPAccount);
		
		List<Account> listOfA = new List<Account>();
		listOfA.addAll(listOfBG);
		listOfA.addAll(listOfSBG);
		listOfA.addAll(listOfNS);

		insert listOfA;

		// Create relationship records
		Affiliation__c thisAff1 = new Affiliation__c(
													Affiliation_From_Account__c = listOfSBG[0].Id,
													Affiliation_To_Account__c = listOfBG[0].Id,
													Affiliation_Type__c = 'SBG/CANBG',
													Affiliation_Start_Date__c = System.today() - 7
													);
		Affiliation__c thisAff2 = new Affiliation__c(
													Affiliation_From_Account__c = listOfSBG[0].Id,
													Affiliation_To_Account__c = listOfBG[1].Id,
													Affiliation_Type__c = 'SBG/CANBG',
													Affiliation_Start_Date__c = System.today() - 7
													);
		Affiliation__c thisAff3 = new Affiliation__c(
													Affiliation_From_Account__c = listOfNS[0].Id,
													Affiliation_To_Account__c = listOfSBG[0].Id,
													Affiliation_Type__c = 'Non-SAP/SBG',
													Affiliation_Start_Date__c = System.today() - 7
													);
	
		insert new List<Affiliation__c>{thisAff1, thisAff2, thisAff3};														 
		
		// Start test		
		Test.startTest();
		
		ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(listOfSBG[0]);
		ctrlExt_AccountHierarchyView ctrlAh = new ctrlExt_AccountHierarchyView(sc);
		// call methods
		ctrlAh.selectedAccntId = listOfSBG[0].Id;
		ctrlAh.refreshTree();
		ctrlAh.getTopNode();
		ctrlAh.getAccountTypeLegend();
		// Get tree data
		String treeData = ctrlAh.getTreeData();
		System.debug('*** treeData: ' + treeData);
		// Select parent
		ctrlAh.selectedParentAccntTypeId = listOfBG[0].Id;
		ctrlAh.selectParent();
		// Select other parent
		ctrlAh.selectedParentAccntTypeId = listOfBG[1].Id;
		ctrlAh.selectParent();

		Test.stopTest();
		
	}

	static testMethod void testAccountHierarchyView1(){

		// Use Germany as country to test with, so DEPENDENCT ON DATA		
		Id countryId = [select Id from DIB_Country__c where Name = 'Canada'].Id;
		// For test make sure the Unique parent of the Sub Buying Group is set to No
		Account_Type_Country__c atc = [select Id, Unique_Parent__c from Account_Type_Country__c where Account_Type__r.Account_Type__c = 'Sub Buying Group' and country__c = :countryId];
		atc.Unique_Parent__c = 'No';
		update atc;
		
		// Setup some test accounts
		String randVal = Test_TestDataUtil.createRandVal();
		// Create BG, SBG and Non-SAP accounts
		//List<Account> listOfBG = Test_TestDataUtil.createBGAccounts(randVal, 'CANADA', 2);
		//List<Account> listOfSBG = Test_TestDataUtil.createSBGAccounts(randVal, 'CANADA', 1);
		//List<Account> listOfNS = Test_TestDataUtil.createNONSAPAccounts(randVal, 'CANADA', 1);
        
        List<Account> listOfBG = new List<Account>();
        List<Account> listOfSBG = new List<Account>();
        List<Account> listOfNS = new List<Account>();
        
        Account recBGAccount = new Account();
        Account recSBGAccount = new Account();
        Account recNonSAPAccount = new Account();
        
        recBGAccount.Name = 'Can Buying Group 1';
        recBGAccount.Type = 'Buying Group';
        recBGAccount.RecordTypeId = RecordTypeMedtronic.getRecordTypeByDevName('Account', 'CAN_Buying_Group').Id;
        recBGAccount.Account_Country_vs__c = 'CANADA';
        
        listOfBG.add(recBGAccount);
        
        recBGAccount = new Account();
        
        recBGAccount.Name = 'Can Buying Group 2';
        recBGAccount.Type = 'Buying Group';
        recBGAccount.RecordTypeId = RecordTypeMedtronic.getRecordTypeByDevName('Account', 'CAN_Buying_Group').Id;
        recBGAccount.Account_Country_vs__c = 'CANADA';
        
        listOfBG.add(recBGAccount);
        
        recSBGAccount.Name = 'Sub Buying Group 1';
        recSBGAccount.Type = 'Sub Buying Group';
        recSBGAccount.RecordTypeId = RecordTypeMedtronic.getRecordTypeByDevName('Account', 'CAN_Buying_Group').Id;
        recSBGAccount.Account_Country_vs__c = 'CANADA';
        
        listOfSBG.add(recSBGAccount);
        
		recNonSAPAccount.Name = 'Non SAP Account 1';
        recNonSAPAccount.RecordTypeId = RecordTypeMedtronic.getRecordTypeByDevName('Account', 'NON_SAP_Account').Id;
        recNonSAPAccount.Account_Country_vs__c = 'CANADA';
        
        listOfNS.add(recNonSAPAccount);
        
		List<Account> listOfA = new List<Account>();
		listOfA.addAll(listOfBG);
		listOfA.addAll(listOfSBG);
		listOfA.addAll(listOfNS);

		insert listOfA;

		// Create relationship records
		Affiliation__c thisAff1 = new Affiliation__c(
													Affiliation_From_Account__c = listOfSBG[0].Id,
													Affiliation_To_Account__c = listOfBG[0].Id,
													Affiliation_Type__c = 'SBG/CANBG',
													Affiliation_Start_Date__c = System.today() - 7
													);
		Affiliation__c thisAff2 = new Affiliation__c(
													Affiliation_From_Account__c = listOfSBG[0].Id,
													Affiliation_To_Account__c = listOfBG[1].Id,
													Affiliation_Type__c = 'SBG/CANBG',
													Affiliation_Start_Date__c = System.today() - 7
													);
		Affiliation__c thisAff3 = new Affiliation__c(
													Affiliation_From_Account__c = listOfNS[0].Id,
													Affiliation_To_Account__c = listOfSBG[0].Id,
													Affiliation_Type__c = 'Non-SAP/SBG',
													Affiliation_Start_Date__c = System.today() - 7
													);
													
		insert new List<Affiliation__c>{thisAff1, thisAff2, thisAff3};
		
		// Start test		
		Test.startTest();
		
		ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(listOfSBG[0]);
		ctrlExt_AccountHierarchyView ctrlAh = new ctrlExt_AccountHierarchyView(sc);
		// call methods
		ctrlAh.selectedAccntId = listOfSBG[0].Id;
		ctrlAh.refreshTree();
		ctrlAh.getTopNode();
		ctrlAh.getAccountTypeLegend();
		// Get tree data
		String treeData = ctrlAh.getTreeData();
		System.debug('*** treeData: ' + treeData);
		// Select parent
		ctrlAh.selectedParentAccntTypeId = listOfBG[0].Id;
		ctrlAh.selectParent();
		// Select other parent
		ctrlAh.selectedParentAccntTypeId = listOfBG[1].Id;
		ctrlAh.selectParent();

		Test.stopTest();
		
	}
	/*
	static testMethod void testAccountHierarchyView2(){

		// Use Germany as country to test with, so DEPENDENCT ON DATA		
		Id countryId = [select Id from DIB_Country__c where Name = 'Canada'].Id;
		// For test make sure the Unique parent of the Sub Buying Group is set to No
		Account_Type_Country__c atc = [select Id, Unique_Parent__c from Account_Type_Country__c where Account_Type__r.Account_Type__c = 'Sub Buying Group' and country__c = :countryId];
		atc.Unique_Parent__c = 'Per BU';
		update atc;
		
		//Relationship_Type_Country__c rtc = [select Id, Master_Data_Level__c from Relationship_Type_Country__c where Country__c = :countryId and Relationship_Type_Name__c = 'SBG/CANBG'];
		//rtc.Master_Data_Level__c = 'Business Unit';
		
		// Setup some test accounts
		String randVal = Test_TestDataUtil.createRandVal();
		// Create BG, SBG and Non-SAP accounts
		//List<Account> listOfBG = Test_TestDataUtil.createBGAccounts(randVal, 'CANADA', 2);
		//List<Account> listOfSBG = Test_TestDataUtil.createSBGAccounts(randVal, 'CANADA', 1);
		//List<Account> listOfNS = Test_TestDataUtil.createNONSAPAccounts(randVal, 'CANADA', 1);
        
		List<Account> listOfBG = new List<Account>();
        List<Account> listOfSBG = new List<Account>();
        List<Account> listOfNS = new List<Account>();
        
        Account recBGAccount = new Account();
        Account recSBGAccount = new Account();
        Account recNonSAPAccount = new Account();
        
        recBGAccount.Name = 'Can Buying Group 1';
        recBGAccount.Type = 'Buying Group';
        recBGAccount.RecordTypeId = RecordTypeMedtronic.getRecordTypeByDevName('Account', 'CAN_Buying_Group').Id;
        recBGAccount.Account_Country_vs__c = 'CANADA';
        
        listOfBG.add(recBGAccount);
        
        recBGAccount = new Account();
        
        recBGAccount.Name = 'Can Buying Group 2';
        recBGAccount.Type = 'Buying Group';
        recBGAccount.RecordTypeId = RecordTypeMedtronic.getRecordTypeByDevName('Account', 'CAN_Buying_Group').Id;
        recBGAccount.Account_Country_vs__c = 'CANADA';
        
        listOfBG.add(recBGAccount);
        
        recSBGAccount.Name = 'Sub Buying Group 1';
        recSBGAccount.Type = 'Sub Buying Group';
        recSBGAccount.RecordTypeId = RecordTypeMedtronic.getRecordTypeByDevName('Account', 'CAN_Buying_Group').Id;
        recSBGAccount.Account_Country_vs__c = 'CANADA';
        
        listOfSBG.add(recSBGAccount);
        
		recNonSAPAccount.Name = 'Non SAP Account 1';
        recNonSAPAccount.RecordTypeId = RecordTypeMedtronic.getRecordTypeByDevName('Account', 'NON_SAP_Account').Id;
        recNonSAPAccount.Account_Country_vs__c = 'CANADA';
        
        listOfNS.add(recNonSAPAccount);
        
		List<Account> listOfA = new List<Account>();
		listOfA.addAll(listOfBG);
		listOfA.addAll(listOfSBG);
		listOfA.addAll(listOfNS);

		insert listOfA;

		Id bu1Id = [select Id from Business_Unit__c where Name = 'Diabetes'][0].Id;
		Id bu2Id = [select Id from Business_Unit__c where Name = 'CRHF'][0].Id;

		// Create relationship records
		Affiliation__c thisAff1 = new Affiliation__c(
													Affiliation_From_Account__c = listOfSBG[0].Id,
													Affiliation_To_Account__c = listOfBG[0].Id,
													Affiliation_Type__c = 'SBG/BG',
													Affiliation_Start_Date__c = System.today() - 7
													);
		Affiliation__c thisAff2 = new Affiliation__c(
													Affiliation_From_Account__c = listOfSBG[0].Id,
													Affiliation_To_Account__c = listOfBG[1].Id,
													Affiliation_Type__c = 'SBG/BG',
													Affiliation_Start_Date__c = System.today() - 7
													);
		Affiliation__c thisAff3 = new Affiliation__c(
													Affiliation_From_Account__c = listOfNS[0].Id,
													Affiliation_To_Account__c = listOfSBG[0].Id,
													Affiliation_Type__c = 'Non-SAP/SBG',
													Affiliation_Start_Date__c = System.today() - 7
													);
													
		insert new List<Affiliation__c>{thisAff1, thisAff2, thisAff3};
		
		Id accntTypeId = [select Id from Account_Type__c where Account_Type__c = 'CAN Buying Group'].Id;
		
		// Start test		
		Test.startTest();
		
		ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(listOfSBG[0]);
		ctrlExt_AccountHierarchyView ctrlAh = new ctrlExt_AccountHierarchyView(sc);
		// call methods
		ctrlAh.selectedAccntId = listOfSBG[0].Id;
		ctrlAh.refreshTree();
		ctrlAh.getTopNode();
		ctrlAh.getAccountTypeLegend();
		// Get tree data
		String treeData = ctrlAh.getTreeData();
		System.debug('*** treeData: ' + treeData);
		// Select parent
		ctrlAh.selectedParentAccntTypeId = accntTypeId;
		ctrlAh.selectParent();

		Test.stopTest();
		
	}	
	
	static testMethod void testAccountHierarchyView3(){

		// Use Germany as country to test with, so DEPENDENCT ON DATA		
		Id countryId = [select Id from DIB_Country__c where Name = 'Canada'].Id;
		// For test make sure the Unique parent of the Sub Buying Group is set to No
		Account_Type_Country__c atc = [select Id, Unique_Parent__c from Account_Type_Country__c where Account_Type__r.Account_Type__c = 'Sub Buying Group' and country__c = :countryId];
		atc.Unique_Parent__c = 'Per SBU';
		update atc;
		
		//Relationship_Type_Country__c rtc = [select Id, Master_Data_Level__c from Relationship_Type_Country__c where Country__c = :countryId and Relationship_Type_Name__c = 'SBG/CANBG'];
		//rtc.Master_Data_Level__c = 'Sub Business Unit';
		
		// Setup some test accounts
		String randVal = Test_TestDataUtil.createRandVal();
		// Create BG, SBG and Non-SAP accounts
		//List<Account> listOfBG = Test_TestDataUtil.createBGAccounts(randVal, 'CANADA', 2);
		//List<Account> listOfSBG = Test_TestDataUtil.createSBGAccounts(randVal, 'CANADA', 1);
		//List<Account> listOfNS = Test_TestDataUtil.createNONSAPAccounts(randVal, 'CANADA', 1);
        
        List<Account> listOfBG = new List<Account>();
        List<Account> listOfSBG = new List<Account>();
        List<Account> listOfNS = new List<Account>();
        
        Account recBGAccount = new Account();
        Account recSBGAccount = new Account();
        Account recNonSAPAccount = new Account();
        
        recBGAccount.Name = 'Can Buying Group 1';
        recBGAccount.Type = 'Buying Group';
        recBGAccount.RecordTypeId = RecordTypeMedtronic.getRecordTypeByDevName('Account', 'CAN_Buying_Group').Id;
        recBGAccount.Account_Country_vs__c = 'CANADA';
        
        listOfBG.add(recBGAccount);
        
        recBGAccount = new Account();
        
        recBGAccount.Name = 'Can Buying Group 2';
        recBGAccount.Type = 'Buying Group';
        recBGAccount.RecordTypeId = RecordTypeMedtronic.getRecordTypeByDevName('Account', 'CAN_Buying_Group').Id;
        recBGAccount.Account_Country_vs__c = 'CANADA';
        
        listOfBG.add(recBGAccount);
        
        recSBGAccount.Name = 'Sub Buying Group 1';
        recSBGAccount.Type = 'Sub Buying Group';
        recSBGAccount.RecordTypeId = RecordTypeMedtronic.getRecordTypeByDevName('Account', 'CAN_Buying_Group').Id;
        recSBGAccount.Account_Country_vs__c = 'CANADA';
        
        listOfSBG.add(recSBGAccount);
        
		recNonSAPAccount.Name = 'Non SAP Account 1';
        recNonSAPAccount.RecordTypeId = RecordTypeMedtronic.getRecordTypeByDevName('Account', 'NON_SAP_Account').Id;
        recNonSAPAccount.Account_Country_vs__c = 'CANADA';
        
        listOfNS.add(recNonSAPAccount);
		
		List<Account> listOfA = new List<Account>();
		listOfA.addAll(listOfBG);
		listOfA.addAll(listOfSBG);
		listOfA.addAll(listOfNS);

		insert listOfA;

		Id sbu1Id = [select Id from Sub_Business_Units__c where Name = 'Diabetes Core'][0].Id;
		Id sbu2Id = [select Id from Sub_Business_Units__c where Name = 'HCC'][0].Id;

		// Create relationship records
		Affiliation__c thisAff1 = new Affiliation__c(
													Affiliation_From_Account__c = listOfSBG[0].Id,
													Affiliation_To_Account__c = listOfBG[0].Id,
													Affiliation_Type__c = 'SBG/BG',
													Affiliation_Start_Date__c = System.today() - 7
													);
		Affiliation__c thisAff2 = new Affiliation__c(
													Affiliation_From_Account__c = listOfSBG[0].Id,
													Affiliation_To_Account__c = listOfBG[1].Id,
													Affiliation_Type__c = 'SBG/BG',
													Affiliation_Start_Date__c = System.today() - 7
													);
        
        
		Affiliation__c thisAff3 = new Affiliation__c(
													Affiliation_From_Account__c = listOfNS[0].Id,
													Affiliation_To_Account__c = listOfSBG[0].Id,
													Affiliation_Type__c = 'Non-SAP/SBG',
													Affiliation_Start_Date__c = System.today() - 7
													);
												
		insert new List<Affiliation__c>{thisAff1, thisAff2, thisAff3};															 
		
		Id accntTypeId = [select Id from Account_Type__c where Account_Type__c = 'CAN Buying Group'].Id;
		
		// Start test		
		Test.startTest();
		
		ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(listOfSBG[0]);
		ctrlExt_AccountHierarchyView ctrlAh = new ctrlExt_AccountHierarchyView(sc);
		// call methods
		ctrlAh.selectedAccntId = listOfSBG[0].Id;
		ctrlAh.refreshTree();
		ctrlAh.getTopNode();
		ctrlAh.getAccountTypeLegend();
		// Get tree data
		String treeData = ctrlAh.getTreeData();
		System.debug('*** treeData: ' + treeData);
		// Select parent
		ctrlAh.selectedParentAccntTypeId = accntTypeId;
		ctrlAh.selectParent();

		Test.stopTest();
		
	}	
*/
}