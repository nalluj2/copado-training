//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 06-08-2018
//  Description      : APEX Test Class to test the logic in tr_ChangeRequest and bl_ChangeRequest_Trigger
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_bl_ChangeRequest_Trigger  {

	@IsTest private static void test_calculateTotalCostOnBundle(){

		//------------------------------------------------------------------------------------------------
		// Create Test Data
		//------------------------------------------------------------------------------------------------
		clsTestData_MasterData.createCompany();

		List<Change_Request_Bundle__c> lstChangeRequestBundle = new List<Change_Request_Bundle__c>();
		Change_Request_Bundle__c oChangeRequestBundle1 = new Change_Request_Bundle__c();
			oChangeRequestBundle1.Name = 'CR BUNDLE 1';
			oChangeRequestBundle1.CurrencyIsoCode = 'EUR';
		lstChangeRequestBundle.add(oChangeRequestBundle1);
		Change_Request_Bundle__c oChangeRequestBundle2 = new Change_Request_Bundle__c();
			oChangeRequestBundle2.Name = 'CR BUNDLE 2';
			oChangeRequestBundle2.CurrencyIsoCode = 'EUR';
		lstChangeRequestBundle.add(oChangeRequestBundle2);
		insert lstChangeRequestBundle;

		List<Change_Request__c> lstChangeRequest = new List<Change_Request__c>();
		for (Integer iCounter = 0; iCounter < 5; iCounter++){
			Change_Request__c oCR = new Change_Request__c();
				oCR.RecordTypeId = clsUtil.getRecordTypeByDevName('Change_Request__c', 'General').Id;
				oCR.CurrencyIsoCode = 'EUR';
				oCR.Change_Request__c = 'CR ' + String.valueOf(iCounter);
				oCR.Request_Date__c = Date.today();
				oCR.Requestor_Lookup__c = UserInfo.getUserId();
				oCR.Requesting_BU__c = 'IT';
				oCR.Requesting_Department__c = 'Service & Solutions';
				oCR.Origin__c = 'Salesforce.com';
				oCR.Status__c = 'New';
				oCR.Deployment__c = 'Regular Release';
				oCR.Company__c = clsTestData_MasterData.oMain_Company.Id;
				oCR.Tool__c = 'Salesforce.com';
				oCR.Total_Cost__c = 1500;
				oCR.Change_Request_Bundle__c = oChangeRequestBundle1.Id;
			lstChangeRequest.add(oCR);
		}
		insert lstChangeRequest;			
		//------------------------------------------------------------------------------------------------


		//------------------------------------------------------------------------------------------------
		// Test Logic
		//------------------------------------------------------------------------------------------------
		Test.startTest();

			// TEST INSERT
			oChangeRequestBundle1 = [SELECT Id, Total_Cost__c FROM Change_Request_Bundle__c WHERE Id = :oChangeRequestBundle1.Id];
			Decimal decTotalCost = oChangeRequestBundle1.Total_Cost__c.round();
			System.assertEquals(decTotalCost, 1500*5);

			// TEST UPDATE - Update Total Cost of Change Request
			lstChangeRequest[0].Total_Cost__c += 500;
			update lstChangeRequest[0];
			oChangeRequestBundle1 = [SELECT Id, Total_Cost__c FROM Change_Request_Bundle__c WHERE Id = :oChangeRequestBundle1.Id];
			decTotalCost = oChangeRequestBundle1.Total_Cost__c.round();
			System.assertEquals(decTotalCost, (1500*5)+500);

			// TEST UPDATE - Update Status to Closed - exclude this CR in the summation (= -2000 = -1500-500)
			lstChangeRequest[0].Status__c = 'Closed';
			update lstChangeRequest[0];
			oChangeRequestBundle1 = [SELECT Id, Total_Cost__c FROM Change_Request_Bundle__c WHERE Id = :oChangeRequestBundle1.Id];
			decTotalCost = oChangeRequestBundle1.Total_Cost__c.round();
			System.assertEquals(decTotalCost, (1500*5)+500-2000);

			// TEST UPDATE - Update Status to New - include this CR in the summation (= +2000 = +1500+500)
			lstChangeRequest[0].Status__c = 'New';
			update lstChangeRequest[0];
			oChangeRequestBundle1 = [SELECT Id, Total_Cost__c FROM Change_Request_Bundle__c WHERE Id = :oChangeRequestBundle1.Id];
			decTotalCost = oChangeRequestBundle1.Total_Cost__c.round();
			System.assertEquals(decTotalCost, (1500*5)+500-2000+2000);

			// TEST DELETE
			delete lstChangeRequest[0];
			oChangeRequestBundle1 = [SELECT Id, Total_Cost__c FROM Change_Request_Bundle__c WHERE Id = :oChangeRequestBundle1.Id];
			decTotalCost = oChangeRequestBundle1.Total_Cost__c.round();
			System.assertEquals(decTotalCost, ((1500*5)+500) - 2000);

			// TEST UNDELETE
			undelete lstChangeRequest[0];
			oChangeRequestBundle1 = [SELECT Id, Total_Cost__c FROM Change_Request_Bundle__c WHERE Id = :oChangeRequestBundle1.Id];
			decTotalCost = oChangeRequestBundle1.Total_Cost__c.round();
			System.assertEquals(decTotalCost, (((1500*5)+500) - 2000) + 2000);

			// TEST UPDATE - Update Change Request Bundle
			lstChangeRequest[1].Change_Request_Bundle__c = oChangeRequestBundle2.Id;
			update lstChangeRequest[1];
			oChangeRequestBundle1 = [SELECT Id, Total_Cost__c FROM Change_Request_Bundle__c WHERE Id = :oChangeRequestBundle1.Id];
			decTotalCost = oChangeRequestBundle1.Total_Cost__c.round();
			System.assertEquals(decTotalCost, (((1500*5)+500) - 2000) + 2000 - 1500);

			oChangeRequestBundle2 = [SELECT Id, Total_Cost__c FROM Change_Request_Bundle__c WHERE Id = :oChangeRequestBundle2.Id];
			decTotalCost = oChangeRequestBundle2.Total_Cost__c.round();
			System.assertEquals(decTotalCost, 1500);

			// TEST UPDATE - Update Currency ISO Code
			lstChangeRequest[1].CurrencyIsoCode = 'USD';
			update lstChangeRequest[1];
			oChangeRequestBundle2 = [SELECT Id, Total_Cost__c FROM Change_Request_Bundle__c WHERE Id = :oChangeRequestBundle2.Id];
			decTotalCost = oChangeRequestBundle1.Total_Cost__c.round();
			System.assertNotEquals(decTotalCost, 1500);
			System.assert(decTotalCost > 0);

			// TEST UPDATE - Update Change Request Bundle 2
			lstChangeRequest[1].Change_Request_Bundle__c = oChangeRequestBundle1.Id;
			update lstChangeRequest[1];
			oChangeRequestBundle2 = [SELECT Id, Total_Cost__c FROM Change_Request_Bundle__c WHERE Id = :oChangeRequestBundle2.Id];
			decTotalCost = oChangeRequestBundle2.Total_Cost__c.round();
			System.assertEquals(decTotalCost, 0);

		Test.stopTest();
		//------------------------------------------------------------------------------------------------
	
	}

}
//--------------------------------------------------------------------------------------------------------------------