//----------------------------------------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 01-11-2016
//  Description      : APEX Class - Business Logic for tr_WorkorderSparepart
//  Change Log       : 
//----------------------------------------------------------------------------------------------------------------------------------------------------
public with sharing class bl_WorkorderSparepart_Trigger {

	//------------------------------------------------------------------------------------------------------------------------------------------------
	// CR-12966
	// When a Workorder_Sparepart__c is created or updated, we need to copy the Order_Lot_Number__c from the Workorder_Sparepart__c 
	//	to the field Computer_Serial_Number__c on the related Asset.
	//------------------------------------------------------------------------------------------------------------------------------------------------
    public static void updateRelatedAsset(List<Workorder_Sparepart__c> lstTriggerNew, Map<Id, Workorder_Sparepart__c> mapTriggerOld){

    	List<Asset> lstAsset_Update = new List<Asset>();

		Map<Id, Workorder_Sparepart__c> mapAssetID_WorkorderSparePart = new Map<Id, Workorder_Sparepart__c>();

		for (Workorder_Sparepart__c oWorkorderSparepart : lstTriggerNew){
			if (
				(
					oWorkorderSparepart.Return_Item_Status__c == 'Part Not Returned'
					|| oWorkorderSparepart.Return_Item_Status__c == 'Under Analysis'
					|| oWorkorderSparepart.Return_Item_Status__c == 'Analysis Complete'
				) 
				&& 
				(
					oWorkorderSparepart.Order_Status__c == 'International Ship'
					|| oWorkorderSparepart.Order_Status__c == 'Shipped'
				) 
				&&
				(
					oWorkorderSparepart.Asset_Product_Type__c == 'S8 Planning Station'
				)
				&&
				(
					oWorkorderSparepart.Order_Lot_Number__c != NULL
				)
			){

				mapAssetID_WorkorderSparePart.put(oWorkorderSparepart.Asset__c, oWorkorderSparepart);
			}
		}

		if (mapAssetID_WorkorderSparePart.size() > 0){
			List<Asset> lstAsset = 
				[
					SELECT Computer_Serial_Number__c 
					FROM Asset 
					WHERE ID =:mapAssetID_WorkorderSparePart.keySet()
				];

			for (Asset oAsset : lstAsset){
				// Only perform an update if needed (id Asset.Computer_Serial_Number__c is different from Workorder_Sparepart__c.Order_Lot_Number__c)
				if (oAsset.Computer_Serial_Number__c != mapAssetID_WorkorderSparePart.get(oAsset.Id).Order_Lot_Number__c){
					oAsset.Computer_Serial_Number__c = mapAssetID_WorkorderSparePart.get(oAsset.Id).Order_Lot_Number__c;
					lstAsset_Update.add(oAsset);
				}
			}

		}

		if (lstAsset_Update.size() > 0){
			update lstAsset_Update;
		}

    }
	//------------------------------------------------------------------------------------------------------------------------------------------------

}
//----------------------------------------------------------------------------------------------------------------------------------------------------