//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 16-08-2017
//  Description      : APEX TEST Class for bl_HistoryTracking
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_bl_HistoryTracking {
	
	private static List<Account_Extended_Profile__c> lstAccountExtendedProfile = new List<Account_Extended_Profile__c>();
	private static Account_Extended_Profile__c oAccountExtendedProfile1;
	private static Account_Extended_Profile__c oAccountExtendedProfile2;
	private static Date dToday = Date.today();
	private static Map<Id, String> mapRelatedData = new Map<Id, String>();
	private static List<User> lstUser = new List<User>();

	private static User oUser_SystemAdministrator;
	private static User oUser_ITBA1;
	private static User oUser_ITBA2;

	@isTest private static void createTestUser(){

		List<User> lstUser = new List<User>();
		oUser_SystemAdministrator = clsTestData_User.createUser_SystemAdministrator('tadm000', false);
		oUser_ITBA1 = clsTestData_User.createUser('test.itba1@medtronic.com.test', 'tadm001', 'BELGIUM', clsUtil.getUserProfileId('IT Business Analyst'), clsUtil.getUserRoleId('System Administrator'), false);
		oUser_ITBA2 = clsTestData_User.createUser('test.itba2@medtronic.com.test', 'tadm002', 'BELGIUM', clsUtil.getUserProfileId('IT Business Analyst'), clsUtil.getUserRoleId('System Administrator'), false);
		lstUser.add(oUser_ITBA1);
		lstUser.add(oUser_ITBA2);
		lstUser.add(oUser_SystemAdministrator);
		insert lstUser;

	}

	@isTest static void createTestData_AccountExtendedProfile() {

		if (oUser_SystemAdministrator == null) createTestUser();

		lstUser = [SELECT Id, Name FROM User WHERE Alias = 'tadm001' or Alias = 'tadm002'];
		for (User oUser : lstUser){
			mapRelatedData.put(oUser.Id, oUser.Name);
		}

		System.runAs(oUser_SystemAdministrator){
			// Create Custom Setting Data
			clsTestData_CustomSetting.createHistoryTracking_AccountExtendedProfile();

			// Create Account Data
			clsTestData_Account.iRecord_Account_SAPAccount = 2;
			clsTestData_Account.createAccount_SAPAccount();

			// Create Account Extended Profile
			oAccountExtendedProfile1  = new Account_Extended_Profile__c();
				oAccountExtendedProfile1.RecordTypeId = clsUtil.getRecordTypeByDevName('Account_Extended_Profile__c', 'EMEA_Account_Extended_Profile').Id;
				oAccountExtendedProfile1.Account__c = clsTestData_Account.oMain_Account_SAPAccount.Id;
				oAccountExtendedProfile1.Commencement_Date__c = dToday;
				oAccountExtendedProfile1.Section_Owner_SI_AST_Driven__c = lstUser[0].Id;
				oAccountExtendedProfile1.General_Surgery_procedures__c = 1;
				oAccountExtendedProfile1.Gynecologic_procedures__c = 2;
				oAccountExtendedProfile1.Head_Neck_procedures__c = 3;
				oAccountExtendedProfile1.Hemorrhoids_procedures__c = 4;
				oAccountExtendedProfile1.Delivery_Address__c = '123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890';
			lstAccountExtendedProfile.add(oAccountExtendedProfile1);
			oAccountExtendedProfile2  = new Account_Extended_Profile__c();
				oAccountExtendedProfile2.RecordTypeId = clsUtil.getRecordTypeByDevName('Account_Extended_Profile__c', 'EMEA_Account_Extended_Profile').Id;
				oAccountExtendedProfile2.Account__c = clsTestData_Account.oMain_Account_SAPAccount.Id;
				oAccountExtendedProfile2.Section_Owner_SI_AST_Driven__c = lstUser[1].Id;
				oAccountExtendedProfile2.Commencement_Date__c = dToday.addDays(10);
				oAccountExtendedProfile2.General_Surgery_procedures__c = 2;
				oAccountExtendedProfile2.Gynecologic_procedures__c = 3;
				oAccountExtendedProfile2.Head_Neck_procedures__c = 4;
				oAccountExtendedProfile2.Hemorrhoids_procedures__c = 5;
				oAccountExtendedProfile2.Delivery_Address__c = '123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789055555';
			insert lstAccountExtendedProfile;
		}

	}
	
	@isTest static void test_createHistoryTracking_AccountExtendedProfile() {

		createTestUser();

		// Create Test Data
		System.runAs(oUser_SystemAdministrator){
			createTestData_AccountExtendedProfile();
		}
		System.assertEquals(lstAccountExtendedProfile.size(), 1);
		List<History_Tracking__c> lstHistoryTracking = [SELECT Id FROM History_Tracking__c WHERE Account_Extended_Profile__c = :lstAccountExtendedProfile];
		System.assertEquals(lstHistoryTracking.size(), 1);

		Set<String> setFieldName = new Set<String>{'Section_Owner_SI_AST_Driven__c','Commencement_Date__c','General_Surgery_procedures__c','Gynecologic_procedures__c','Head_Neck_procedures__c','Hemorrhoids_procedures__c','Delivery_Address__c'};

		// Test/Execute Logic
		Test.startTest();

		System.runAs(oUser_SystemAdministrator){

			bl_HistoryTracking.createHistoryTracking('Account_Extended_Profile__c', 'Account_Extended_Profile__c', null, oAccountExtendedProfile2, setFieldName, mapRelatedData, true);
			bl_HistoryTracking.createHistoryTracking('Account_Extended_Profile__c', 'Account_Extended_Profile__c', oAccountExtendedProfile1, oAccountExtendedProfile2, setFieldName, mapRelatedData, true);

			clsUtil.tExceptionName = 'bl_HistoryTracking.createHistoryTracking_EXCEPTION';
			lstHistoryTracking = bl_HistoryTracking.createHistoryTracking('Account_Extended_Profile__c', 'Account_Extended_Profile__c', oAccountExtendedProfile1, oAccountExtendedProfile2, setFieldName, mapRelatedData, true);
			System.assertEquals(lstHistoryTracking.size(), 0);
		}

		Test.stopTest();

		// Validate Result
		lstHistoryTracking = [SELECT Id, FieldName__c, FieldLabel__c, OldValue__c, NewValue__c FROM History_Tracking__c];
		System.assertEquals(lstHistoryTracking.size(), 9);
		Integer iCounter = 0;
		for (History_Tracking__c oHistoryTracking : lstHistoryTracking){
			if (oHistoryTracking.FieldLabel__c == 'Created'){
				iCounter++;
			}else if (oHistoryTracking.FieldName__c == 'Commencement_Date__c'){
				System.assertEquals(oHistoryTracking.OldValue__c, String.valueOf(dToday.month()) + '/' + String.valueOf(dToday.day()) + '/' + String.valueOf(dToday.year()));
				System.assertEquals(oHistoryTracking.NewValue__c, String.valueOf(dToday.addDays(10).month()) + '/' + String.valueOf(dToday.addDays(10).day()) + '/' + String.valueOf(dToday.addDays(10).year()));
				iCounter++;
			}else if (oHistoryTracking.FieldName__c == 'General_Surgery_procedures__c'){
				System.assertEquals(oHistoryTracking.OldValue__c, '1');
				System.assertEquals(oHistoryTracking.NewValue__c, '2');
				iCounter++;
			}else if (oHistoryTracking.FieldName__c == 'Gynecologic_procedures__c'){
				System.assertEquals(oHistoryTracking.OldValue__c, '2');
				System.assertEquals(oHistoryTracking.NewValue__c, '3');
				iCounter++;
			}else if (oHistoryTracking.FieldName__c == 'Head_Neck_procedures__c'){
				System.assertEquals(oHistoryTracking.OldValue__c, '3');
				System.assertEquals(oHistoryTracking.NewValue__c, '4');
				iCounter++;
			}else if (oHistoryTracking.FieldName__c == 'Hemorrhoids_procedures__c'){
				System.assertEquals(oHistoryTracking.OldValue__c, '4');
				System.assertEquals(oHistoryTracking.NewValue__c, '5');
				iCounter++;
			}else if (oHistoryTracking.FieldName__c == 'Section_Owner_SI_AST_Driven__c'){
				System.assertEquals(oHistoryTracking.OldValue__c, lstUser[0].Name);
				System.assertEquals(oHistoryTracking.NewValue__c, lstUser[1].Name);
				iCounter++;
			}else if (oHistoryTracking.FieldName__c == 'Delivery_Address__c'){
				System.assertEquals(oHistoryTracking.OldValue__c, '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890 ...');
				System.assertEquals(oHistoryTracking.NewValue__c, '123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789055555');
				iCounter++;
			}
		}
		System.assertEquals(iCounter, 9);
	}


	@isTest static void test_loadRelatedData() {

		createTestUser();

		// Create Test Data
		createTestData_AccountExtendedProfile();
		System.assertEquals(lstAccountExtendedProfile.size(), 1);

		Map<Id, Account_Extended_Profile__c> mapAccountExtendProfiled_Old = new Map<Id, Account_Extended_Profile__c>();
		mapAccountExtendProfiled_Old.put(oAccountExtendedProfile1.Id, oAccountExtendedProfile2);

		// Test/Execute Logic
		Test.startTest();

		System.runAs(oUser_SystemAdministrator){

			mapRelatedData = bl_HistoryTracking.loadRelatedData('User', 'Name', lstAccountExtendedProfile, mapAccountExtendProfiled_Old, 'Section_Owner_SI_AST_Driven__c');

			clsUtil.tExceptionName = 'bl_HistoryTracking.loadRelatedData_EXCEPTION';
			bl_HistoryTracking.loadRelatedData('User', 'Name', lstAccountExtendedProfile, mapAccountExtendProfiled_Old, 'Section_Owner_SI_AST_Driven__c');

		}

		Test.stopTest();

		// Validate Result
		System.assertEquals(mapRelatedData.get(lstUser[0].Id), lstUser[0].Name);
		System.assertEquals(mapRelatedData.get(lstUser[1].Id), lstUser[1].Name);

	}	
}
//--------------------------------------------------------------------------------------------------------------------