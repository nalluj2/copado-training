@isTest
private class Test_Case_calculateAgeInWorkingDays {
		
	private static testmethod void testCalculation(){
		
		//Account acct = Test_TestDataUtil.createSBGAccounts('randVal', 'USA', 1)[0];
		
		//Contact cnt = 
		
		User omaUser = [Select Id from User where Profile.Name = 'US OMA Neuro' AND isActive = true LIMIT 1];
		
		OMA_Record_Types__c omaRTSetting = new OMA_Record_Types__c();
		omaRTSetting.Developer_Name__c = 'OMA_Neoro_MedInfo';
		omaRTSetting.Name = 'OMA Neuro MedInfo';
		insert omaRTSetting;
		
		System.runAs(omaUser){
			
			RecordType omaTR = [Select ID from RecordType where SObjectType = 'Case' AND DeveloperName = 'OMA_Neoro_MedInfo'];
		
			Case testCase = new Case();
		
			testCase.RecordTypeId = omaTR.Id;
			testCase.Status = 'New';
			testCase.Date_Received_Date__c = Date.today().addDays(-10);
			
			insert testCase;
			
			Test.startTest();
			
			ba_calculateCaseAgeInWorkingHours batch = new ba_calculateCaseAgeInWorkingHours();
			Database.executeBatch(batch);
			
			Test.stopTest();
			
			testCase = [Select Id, Status, Working_Days_Age__c from Case where Id=:testCase.Id];
			
			System.assert(testCase.Working_Days_Age__c!=null && testCase.Working_Days_Age__c!=-1);
			
			testCase.Status = 'Closed';
			testCase.Response_Method__c = 'Email' ;
			testCase.Response__c = 'test response';
			testCase.Quality_Check__c = 'N/A';
			
			update testCase;
			
		}
	}
	
	private static testmethod void testCalculationSpine(){
				
		User omaUser = [Select Id from User where Profile.Name = 'US OMA Neuro' AND isActive = true LIMIT 1];
		
		OMA_Record_Types__c omaRTSetting = new OMA_Record_Types__c();
		omaRTSetting.Developer_Name__c = 'OMA_Spine_Clinical_Support_Observation';
		omaRTSetting.Name = 'OMA_Spine_Clinical_Support_Observation';
		insert omaRTSetting;
		
		System.runAs(omaUser){
			
			RecordType omaTR = [Select ID from RecordType where SObjectType = 'Case' AND DeveloperName = 'OMA_Spine_Clinical_Support_Observation'];
		
			Case testCase = new Case();
		
			testCase.RecordTypeId = omaTR.Id;
			testCase.Status = 'New';
			
			insert testCase;
			
			Test.startTest();
			
			ba_calculateCaseAgeInWorkingHours batch = new ba_calculateCaseAgeInWorkingHours();
			batch.errors.add('Unit test error');//To make sure we trigger the Finish code :)
			Database.executeBatch(batch);
			
			Test.stopTest();
			
			testCase = [Select Id, Status, Working_Days_Age__c from Case where Id=:testCase.Id];
			
			System.assert(testCase.Working_Days_Age__c!=null && testCase.Working_Days_Age__c!=-1);
			
			testCase.Status = 'Closed';
			testCase.Response_Method__c = 'Email' ;
			testCase.Response__c = 'test response';
			
			update testCase;
			
		}
	}
	
	
	
}