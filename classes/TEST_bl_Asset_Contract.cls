@isTest 
private class TEST_bl_Asset_Contract {

	@isTest static void test_calculatePOAmountAndServiceLevel() {

		Decimal decSum = 0;

	    //---------------------------------------
        // Create Test Data
        //---------------------------------------
		RecordType oRT_Contract = RecordTypeMedtronic.getRecordTypeByDevName('Contract', 'RTG_Service_Contract');
		clsTestData.createContractData(true, oRT_Contract.Id);
		clsTestData.iRecord_AssetContract = 5;
		clsTestData.createAssetContractData(true);
        //---------------------------------------
		bl_Asset_Contract.alreadyProcessedPOAmount.clear();
		
        Test.startTest();

	    //---------------------------------------
        // TEST 0
        //---------------------------------------
        List<Contract> lstContract = [SELECT Id, PO_Amount_derived_from_linked_assets__c, PO_Amount__c FROM Contract WHERE ID = :clsTestData.oMain_Contract.Id];
        system.assertEquals(lstContract[0].PO_Amount_derived_from_linked_assets__c, false);
        system.assertEquals(lstContract[0].PO_Amount__c, 0);
        List<AssetContract__c> lstAssetContract = [SELECT Id, PO_Amount2__c FROM AssetContract__c WHERE Contract__c = :clsTestData.oMain_Contract.Id];
        decSum = 0;
        for (AssetContract__c oAssetContract : lstAssetContract){
        	decSum += clsUtil.isDecimalNull(oAssetContract.PO_Amount2__c, 0);
        }
        system.assertNotEquals(lstContract[0].PO_Amount__c, decSum);
        system.assertEquals(decSum, 500);

        clsTestData.oMain_AssetContract.PO_Amount2__c = 200;
        update clsTestData.oMain_AssetContract;
        bl_Asset_Contract.alreadyProcessedPOAmount.clear();
        
        lstContract = [SELECT Id, PO_Amount_derived_from_linked_assets__c, PO_Amount__c FROM Contract WHERE ID = :clsTestData.oMain_Contract.Id];
        system.assertEquals(lstContract[0].PO_Amount_derived_from_linked_assets__c, false);
        system.assertEquals(lstContract[0].PO_Amount__c, 0);
        lstAssetContract = [SELECT Id, PO_Amount2__c FROM AssetContract__c WHERE Contract__c = :clsTestData.oMain_Contract.Id];
        decSum = 0;
        for (AssetContract__c oAssetContract : lstAssetContract){
        	decSum += clsUtil.isDecimalNull(oAssetContract.PO_Amount2__c, 0);
        }
        system.assertNotEquals(lstContract[0].PO_Amount__c, decSum);
        system.assertEquals(decSum, 600);
        //---------------------------------------


	    //---------------------------------------
        // TEST 1
        //---------------------------------------
        clsTestData.oMain_Contract.PO_Amount_derived_from_linked_assets__c = true;
        update clsTestData.oMain_Contract;
		bl_Asset_Contract.alreadyProcessedPOAmount.clear();
		
        lstContract = [SELECT Id, PO_Amount_derived_from_linked_assets__c, PO_Amount__c FROM Contract WHERE ID = :clsTestData.oMain_Contract.Id];
        system.assertEquals(lstContract[0].PO_Amount_derived_from_linked_assets__c, true);
        system.assertEquals(lstContract[0].PO_Amount__c, 600);

        lstAssetContract = [SELECT Id, PO_Amount2__c FROM AssetContract__c WHERE Contract__c = :clsTestData.oMain_Contract.Id];
        decSum = 0;
        for (AssetContract__c oAssetContract : lstAssetContract){
        	decSum += clsUtil.isDecimalNull(oAssetContract.PO_Amount2__c, 0);
        }
        system.assertEquals(lstContract[0].PO_Amount__c, decSum);
        system.assertEquals(decSum, 600);
        //---------------------------------------

		
		
	    //---------------------------------------
        // TEST 2 - UPDATE
        //---------------------------------------
        clsTestData.oMain_AssetContract.PO_Amount2__c = 300;
        update clsTestData.oMain_AssetContract;
		bl_Asset_Contract.alreadyProcessedPOAmount.clear();
        lstContract = [SELECT Id, PO_Amount_derived_from_linked_assets__c, PO_Amount__c FROM Contract WHERE ID = :clsTestData.oMain_Contract.Id];
        system.assertEquals(lstContract[0].PO_Amount_derived_from_linked_assets__c, true);
        system.assertEquals(lstContract[0].PO_Amount__c, 700);

        lstAssetContract = [SELECT Id, PO_Amount2__c FROM AssetContract__c WHERE Contract__c = :clsTestData.oMain_Contract.Id];
        decSum = 0;
        for (AssetContract__c oAssetContract : lstAssetContract){
        	decSum += clsUtil.isDecimalNull(oAssetContract.PO_Amount2__c, 0);
        }
        system.assertEquals(lstContract[0].PO_Amount__c, decSum);
        system.assertEquals(decSum, 700);
        //---------------------------------------


	    //---------------------------------------
        // TEST 3 - DELETE
        //---------------------------------------
        delete clsTestData.oMain_AssetContract;

        lstContract = [SELECT Id, PO_Amount_derived_from_linked_assets__c, PO_Amount__c FROM Contract WHERE ID = :clsTestData.oMain_Contract.Id];
        system.assertEquals(lstContract[0].PO_Amount_derived_from_linked_assets__c, true);
        system.assertEquals(lstContract[0].PO_Amount__c, 400);

        lstAssetContract = [SELECT Id, PO_Amount2__c FROM AssetContract__c WHERE Contract__c = :clsTestData.oMain_Contract.Id];
        decSum = 0;
        for (AssetContract__c oAssetContract : lstAssetContract){
        	decSum += clsUtil.isDecimalNull(oAssetContract.PO_Amount2__c, 0);
        }
        system.assertEquals(lstContract[0].PO_Amount__c, decSum);
        system.assertEquals(decSum, 400);
        //---------------------------------------


        Test.stopTest();

	}


	@isTest static void test_updateDIENCode() {

		//------------------------------------------------------
        // Create Test Data
		//------------------------------------------------------
		// Create Test Data - Mapping_Asset_ServiceLevel_DIENCode__c
		Mapping_Asset_ServiceLevel_DIENCode__c oMappingDIENCode1 = new Mapping_Asset_ServiceLevel_DIENCode__c();
			oMappingDIENCode1.CFN_Code__c = 'AZERTY';
			oMappingDIENCode1.Service_Level__c = 'GOLD';
			oMappingDIENCode1.DIEN_Code__c = 'AZERTYGOLD';
		Mapping_Asset_ServiceLevel_DIENCode__c oMappingDIENCode2 = new Mapping_Asset_ServiceLevel_DIENCode__c();
			oMappingDIENCode2.CFN_Code__c = 'AZERTY';
			oMappingDIENCode2.Service_Level__c = 'SILVER';
			oMappingDIENCode2.DIEN_Code__c = 'AZERTYSILVER';
		insert new List<Mapping_Asset_ServiceLevel_DIENCode__c>{oMappingDIENCode1, oMappingDIENCode2};

		// Custom Setting
		clsTestData_CustomSetting.createCustomSettingData();

		// DIB_Country__c
		clsTestData_MasterData.createCountry();
		List<DIB_Country__c> lstCountry_Update = new List<DIB_Country__c>();
		DIB_Country__c oCountry_BE = clsTestData_MasterData.mapCountry.get('BE');
			oCountry_BE.Email_Service_Repair__c = 'belgium01@medtronic.com.test;belgium02@medtronic.com.test';
		lstCountry_Update.add(oCountry_BE);
		DIB_Country__c oCountry_NL = clsTestData_MasterData.mapCountry.get('NL');
			oCountry_NL.EmaiL_Service_Repair__c = 'netherlands01@medtronic.com.test';
		lstCountry_Update.add(oCountry_NL);
		update lstCountry_Update;

		// Product
		List<Product2> lstProduct = new List<Product2>();
		Product2 oProduct1 = new Product2();
			oProduct1.Name = 'PoleStar1';
			oProduct1.ProductCode = '000999';
			oProduct1.isActive = true;					
		lstProduct.add(oProduct1);
		Product2 oProduct2 = new Product2();
			oProduct2.Name = 'PoleStar';
			oProduct2.ProductCode = '009999';
			oProduct2.isActive = true;					
		lstProduct.add(oProduct2);
		insert lstProduct;

		// Account
		clsTestData_Account.iRecord_Account = 1;
		clsTestData_Account.idRecordType_Account = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non-SAP Account').getRecordTypeId();
		List<Account> lstAccount = clsTestData_Account.createAccount(false);
		for (Account oAccount : lstAccount){
			oAccount.AccountNumber = '0000000';
			oAccount.Name = 'Test Account';
			oAccount.Phone = '+123456789';
			oAccount.BillingCity = 'TEST CITY';
			oAccount.BillingCountry = 'BELGIUM';
			oAccount.BillingState = 'TEST STATE';
			oAccount.BillingStreet = 'TEST STREET';
			oAccount.BillingPostalCode = '123456';
			oAccount.ST_NAV_Non_SAP_Account__c = true;
		}
		insert lstAccount;
		Account oAccount = lstAccount[0];

		// Contact
		clsTestData_Contact.iRecord_Contact = 1;
		List<Contact> lstContact = clsTestData_Contact.createContact();

		// Asset
		List<Asset> lstAsset = new List<Asset>();
		Asset oAsset1 = new Asset();
			oAsset1.AccountId = oAccount.Id;
			oAsset1.Product2Id = oProduct1.Id;
			oAsset1.Asset_Product_Type__c = 'S8 Planning Station';
			oAsset1.Ownership_Status__c = 'Purchased';
			oAsset1.Name = '123456789';
			oAsset1.Serial_Nr__c = '123456789';
			oAsset1.Status = 'Installed';
			oAsset1.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('PoleStar').getRecordTypeId();
			oAsset1.CFN_Text__c = 'AZERTY';
		lstAsset.add(oAsset1);
		insert lstAsset;

		// Create Contract Data
		clsTestData_Contract.iRecord_Contract = 1;
		List<Contract> lstContract = clsTestData_Contract.createContract(false);
		for (Contract oContract : lstContract){
			oContract.PO_amount__c = 10000;
			oContract.ContractTerm = 36;
			oContract.BillingCountry = 'BELGIUM';
		}
		insert lstContract;
		
		// Create AssetContract Data
        AssetContract__c oAssetContract1 = new AssetContract__c();
			oAssetContract1.Asset__c = lstAsset[0].Id;
			oAssetContract1.Contract__c = lstContract[0].Id;
			oAssetContract1.Entitlements__c = '100% Coverage for Labor';
			oAssetContract1.PO_Amount2__c = 1000;
			oAssetContract1.Service_Level__c = 'Gold';
        AssetContract__c oAssetContract2 = new AssetContract__c();
			oAssetContract2.Asset__c = lstAsset[0].Id;
			oAssetContract2.Contract__c = lstContract[0].Id;
			oAssetContract2.Entitlements__c = '100% Coverage for Labor';
			oAssetContract2.PO_Amount2__c = 1000;
			oAssetContract2.Service_Level__c = '';
		insert new List<AssetContract__c>{oAssetContract1, oAssetContract2};
		//------------------------------------------------------


		//------------------------------------------------------
		// Test Logic
		//------------------------------------------------------
		Test.startTest();

			List<AssetContract__c> lstAssetContract = [SELECT Asset__r.CFN_Text__c, Service_Level__c, DIEN_Code__c FROM AssetContract__c];
			System.assertEquals(lstAssetContract.size(), 2);

			Integer iDIENCode_Populated = 0;
			Integer iDIENCode_Empty = 0;
			for (AssetContract__c oAssetContract : lstAssetContract){
			
				if (oAssetContract.Service_Level__c == 'GOLD'){
					System.assertEquals(oAssetContract.DIEN_Code__c, 'AZERTYGOLD');
					iDIENCode_Populated++;
					oAssetContract.Service_Level__c = 'GOLD+';
				}else{
					System.assert(String.isBlank(oAssetContract.DIEN_Code__c));
					iDIENCode_Empty++;
					oAssetContract.Service_Level__c = 'SILVER';
				}
			
			}

			System.assertEquals(iDIENCode_Populated, 1);
			System.assertEquals(iDIENCode_Empty, 1);

			update lstAssetContract;



		Test.stopTest();
		//------------------------------------------------------


		//------------------------------------------------------
		// Validate Result
		//------------------------------------------------------
		lstAssetContract = [SELECT Asset__r.CFN_Text__c, Service_Level__c, DIEN_Code__c FROM AssetContract__c];
		System.assertEquals(lstAssetContract.size(), 2);

		iDIENCode_Populated = 0;
		iDIENCode_Empty = 0;
		for (AssetContract__c oAssetContract : lstAssetContract){
			
			if (oAssetContract.Service_Level__c == 'GOLD+'){
				System.assert(String.isBlank(oAssetContract.DIEN_Code__c));
				iDIENCode_Empty++;
			}else if (oAssetContract.Service_Level__c == 'SILVER'){
				System.assertEquals(oAssetContract.DIEN_Code__c, 'AZERTYSILVER');
				iDIENCode_Populated++;
			}else{
				System.assert(String.isBlank(oAssetContract.DIEN_Code__c));
				iDIENCode_Empty++;
			}
			
		}

		System.assertEquals(iDIENCode_Populated, 1);
		System.assertEquals(iDIENCode_Empty, 1);
		//------------------------------------------------------
	}

}