//------------------------------------------------------------------------------------------------------
//  Description : This Class contains the Test logic for the APEX Class bl_Case
//  Version     : 1.0
//  Author      : Bart Caelen
//  Date        : 20150309
//------------------------------------------------------------------------------------------------------
@isTest private class Test_bl_Case {

    private static User oUser;
	
    @testSetup static void createTestData(){
        //---------------------------------------------
        // Create Test Data
        //---------------------------------------------
        clsTestData.idRecordType_Account    = FinalConstants.sapAccountRecordTypeId;
        clsTestData.tCountry_Account        = 'BELGIUM';
        clsTestData.ptCase_Therapy          = 'testClassTh';
        clsTestData.createCompanyData();
        clsTestData.createBusinessUnitData();
        clsTestData.createSubBusinessUnitData();
        clsTestData.createAccountData();
        clsTestData.createContactData();
        clsTestData.idRecordType_Case = clsUtil.getRecordTypeByDevName('Case', 'OMA_Spine_Scientific_Exchange_Product').Id;
        clsTestData.createCaseData();
        //---------------------------------------------
    }


	@isTest static void test_updateOwnership() {

        //---------------------------------------------
        // Load Test Data
        //---------------------------------------------
        Map<Id, Id> mapCaseID_OwnerID = new Map<Id, Id>();
        List<Case> lstCase = [SELECT Id, OwnerId, OMA_Case_Owner__c FROM Case]; 
        List<User> lstUser_OMA = [SELECT Id FROM User WHERE Profile.Name like 'US OMA%' and IsActive = true LIMIT 2];

        System.assertNotEquals(lstCase.size(), 0);
        System.assertEquals(lstUser_OMA.size(), 2);
        //---------------------------------------------


        //---------------------------------------------
        // Perform Testing
        //---------------------------------------------
        Test.startTest();

        for (Case oCase : lstCase){
            oCase.OMA_Case_Owner__c = lstUser_OMA[0].Id;
            mapCaseID_OwnerID.put(oCase.Id, lstUser_OMA[1].Id);
        }
        update lstCase;

        bl_Case.updateOwnership(mapCaseID_OwnerID);

        Test.stopTest();
        //---------------------------------------------
        
	}
	
}
//------------------------------------------------------------------------------------------------------