public class TwoRing_Condition
	{
		public string columnName;
		public string operator;
		public string value;
		public TwoRing_Condition(string columnName, string conditionOperator, string value) {
			this.columnName = columnName;
			this.operator = conditionOperator;
			this.value = value;
		}
	}