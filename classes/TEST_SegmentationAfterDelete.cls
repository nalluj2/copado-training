/**
 * The created class wil cover the SegmentationAfterDelete trigger
 * Creation Date :  20120328
 * Description :    Test coverage for Trigger SegmentationAfterDelete
 * 
 * Author :         Shweta/WIpro
 */
 
@isTest
private class TEST_SegmentationAfterDelete
{
    static testMethod void myUnitTest() 
    {

     Account ac = new account();
     ac.name = 'Account1';
     insert ac;

     Company__c cmpny = new Company__c();
     cmpny.name='TestMedtronic';
     cmpny.CurrencyIsoCode = 'EUR';
     cmpny.Current_day_in_Q1__c=56;
     cmpny.Current_day_in_Q2__c=34; 
     cmpny.Current_day_in_Q3__c=5; 
     cmpny.Current_day_in_Q4__c= 0;   
     cmpny.Current_day_in_year__c =200;
     cmpny.Days_in_Q1__c=56;  
     cmpny.Days_in_Q2__c=34;
     cmpny.Days_in_Q3__c=13;
     cmpny.Days_in_Q4__c=22;
     cmpny.Days_in_year__c =250;
     cmpny.Company_Code_Text__c = 'T33';
     insert cmpny;

     //Insert Business Unit
     Business_Unit__c bu =  new Business_Unit__c();
     bu.Company__c =cmpny.id;
     bu.name='BUMedtronic';
     bu.Account_Plan_Activities__c=true;
     insert bu;
   
     Business_Unit__c bu1 =  new Business_Unit__c();
     bu1.Company__c =cmpny.id;
     bu1.name='BU1Medtronic';
     bu1.Account_Plan_Activities__c=true;
     bu1.Company__c = cmpny.Id;
     insert bu1;

    //Insert SBU
    Sub_Business_Units__c sbu1 = new Sub_Business_Units__c();
    sbu1.name='SBUMedtronic1';
    sbu1.Business_Unit__c=bu1.id;
    sbu1.Account_Plan_Default__c='Revenue';
    sbu1.Account_Flag__c = 'AF_Solutions__c';
    insert sbu1;

    Sub_Business_Units__c sbu2 = new Sub_Business_Units__c();
    sbu2.name='SBUMedtronic2';
    sbu2.Business_Unit__c=bu.id;
    sbu2.Account_Plan_Default__c='Units';
    sbu2.Account_Flag__c = 'AF_Solutions__c';
    insert sbu2;
   
    //Insert Therapy Group
    Therapy_Group__c tg = new Therapy_Group__c();
    tg.Name='Biologics';
    tg.Sub_Business_Unit__c = sbu1.Id;
    tg.Company__c = cmpny.Id;
    insert tg;

    //Insert Therapy
    Therapy__c tp = new Therapy__c();
    tp.Name='Biologics';
    tp.Therapy_Group__c = tg.id;
    tp.Business_Unit__c=bu.id;
    tp.Sub_Business_Unit__c=sbu1.id;
    tp.Therapy_Name_Hidden__c = 'Biologics';
    insert tp;

    Segmentation__c sg= new Segmentation__c();
    sg.Account__c=ac.id;
    sg.Therapy__c=tp.id;
    sg.start__c=system.today();
    sg.Segment__c='Maintain';
    insert sg;

    delete sg;



    }

 }