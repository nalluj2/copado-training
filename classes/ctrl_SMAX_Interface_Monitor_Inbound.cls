public without sharing class ctrl_SMAX_Interface_Monitor_Inbound {
	
	transient public List<OutboundMessage> outboundMessages {get; set;}
    public Integer totalItems {get; private set;}
    
    public List<SelectOption> objectTypes {get; private set;}
    public String selectedObjectType {get; set;}
        
    public String filterId {get; set;}
          
    private final Integer MAX_ITEMS = 500;
    
    public ctrl_SMAX_Interface_Monitor_Inbound(){
    	
    	objectTypes = new List<SelectOption>();    	
    	objectTypes.add(new SelectOption('Case', 'Service Notification'));
    	objectTypes.add(new SelectOption('SVMXC__Service_Order__c', 'Service Order'));
    	
    	selectedObjectType = 'SVMXC__Service_Order__c';      	
    }
    
    public void loadReport(){
    	
    	String query = 'Select Id, Internal_ID__c, (Select Id, Name, Request__c, Response__c, External_ID__c, CreatedDate from Outbound_Messages__r ORDER BY CreatedDate DESC, Id DESC) FROM Notification_Grouping__c where Scope__c = \'' + selectedObjectType + '\'';
		
		if(filterId != null && filterId != '') query += ' AND External_ID__c = \'' + filterId + '\'';
		else query += ' AND Inbound_Status__c = \'Failure\'  AND Archived__c = false ';
				
		query += ' ORDER BY LastModifiedDate ASC LIMIT ' + MAX_ITEMS;
		
		outboundMessages = new List<OutboundMessage>();
		
		for(Notification_Grouping__c nGroup : Database.query(query)){
			
			List<Outbound_Message__c> messages;
			
			if(filterId == null || filterId == '') messages = new List<Outbound_Message__c>{nGroup.Outbound_Messages__r[0]};
			else messages = nGroup.Outbound_Messages__r;
			
			for(Outbound_Message__c mssg : messages){
				
				OutboundMessage oMessage = new OutboundMessage();
				oMessage.Id = mssg.Id;
				oMessage.Name = mssg.Name;
				oMessage.SFDC_Id = nGroup.Internal_ID__c;
				oMessage.SAP_Id = mssg.External_ID__c;
				oMessage.CreatedDate = mssg.createdDate.format();
				oMessage.Request = mssg.Request__c;
				
				ResponseMessage resp = (ResponseMessage) JSON.deserialize(mssg.Response__c, ResponseMessage.class);			
				oMessage.Error = resp.DIST_ERROR_DETAILS;
				
				outboundMessages.add(oMessage);	
			}
		}
				 	
		totalItems = outboundMessages.size();
		
		if(totalItems == MAX_ITEMS){
			
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Maximum number of messages to display reached'));
		}
    }
    
    public class OutboundMessage{
    	
    	public String id {get; set;}
    	public String name {get; set;}
    	public String SFDC_Id {get; set;}
    	public String SAP_Id {get; set;}
    	public String createdDate  {get; set;}
    	public String request {get; set;}
    	public String error {get; set;}    	   	
    }
    
    private class ResponseMessage{
    	
    	public String DIST_ERROR_DETAILS {get; set;}
    }
}