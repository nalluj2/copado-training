/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code 
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TEST_controllerDIB_Competitor_iBase {

    static testMethod void myUnitTest() {
    	
    	System.debug('################## BEGIN Test Coverage : TEST_controllerDIB_Competitor_iBase ########################## ') ;
    	//testAsAdmin(); 
		testAsRepUser() ; 
		System.debug('################## END Test Coverage : TEST_controllerDIB_Competitor_iBase ########################## ') ;	
        
    }
    
    
    private static void testAsRepUser(){
    	
    	Id prof1 = [select id from profile where name like '%DiB Sales Entry%' limit 1].Id;
        User u2 = new User(alias = 'repuser', email='usertest@absi.be', emailencodingkey='UTF-8', lastname='Testing', 
        		languagelocalekey='en_US',localesidkey='en_US', profileid = prof1, timezonesidkey='America/Los_Angeles', 
        		username='usertest@absi.be', Alias_unique__c='usertest1234',Company_Code_Text__c='EUR');        
        
        System.runAs(u2){
        	String month = '02' ; 
        	String year = '2012' ;
        	
	    	Account competitor = new Account() ; 
	    	competitor.name = 'ANIMAs' ;
	    	competitor.CGMs__c = true;
	    	competitor.Pumps__c = true;
	    	competitor.RecordTypeId = FinalConstants.ACCOUNT_COMPETITOR_RECORDTYPE_ID;
	    	insert competitor ; 
	    	   
			// Sales Force Acounnts 
			List<Account> sfaAccounts 		= new List<Account>{} ;
			
			// create Sales Force Accounts : 
			for (Integer i = 0; i < 1 ; i++){////10
				Account acc1 = new Account();
				acc1.Name = 'Test Coverage Sales Force Account ' + i ;
				acc1.isSales_Force_Account__c = true ;
				acc1.Account_Active__c = true; 
				sfaAccounts.add(acc1) ;	
			}
			
			insert sfaAccounts ; 
			
			// Create Departments 

			List <DIB_Department__c> departments = new List<DIB_Department__c>{} ; 
			//Department
			for (Integer i = 0; i < 1 ; i++){	////10	 	
				DIB_Department__c dep1 = new DIB_Department__c();
				dep1.Name = 'Test Coverage Department ' + i ;
				dep1.Account__c = sfaAccounts[i].Id;
				dep1.Active__c = true;
				departments.add(dep1);			 	
			}
			insert departments;
	    	   	    
	    	List<DIB_Competitor_iBase__c> compIBs = new List<DIB_Competitor_iBase__c>{} ; 
	    	for (Integer i = 0 ; i < 1 ; i++){ ////10
	    		DIB_Competitor_iBase__c comp = new DIB_Competitor_iBase__c() ; 
	    		comp.Current_IB__c = 10 ;   	    		
	    		comp.Competitor_Account_ID__c = competitor.Id ; 
	    		comp.Sales_Force_Account__c = sfaAccounts[i].Id ;    		 
	    		comp.Fiscal_year__c = year ; 
	    		comp.Fiscal_Month__c = month ;
				//comp.department__c = 'Adult' ;
				comp.IB_Change__c = 23 ;    
				/*for (DIB_Department__c d : departments){
					if (d.Account__c == 	comp.Sales_Force_Account__c ){
						comp.Account_Segmentation__c = d.Id; 
					}
				}*/
				comp.Account_Segmentation__c = departments[i].Id;
				/*if (i == 10){
					comp.department__c = 'Pediatric' ;  
				}*/
				comp.Type__c = 'Pumps';
				compIBs.add(comp) ; 
	    	}
	    	insert compIBs ; 
	    	
	    	
	    	
	    	Test.startTest();
	    	
	    	PageReference pPageReference = Page.DIB_Competitor_iBase;
        	Test.setCurrentPage(pPageReference);

	        //ApexPages.Standardcontroller sct = new ApexPages.Standardcontroller(competitor);
			controllerDIB_Competitor_iBase  controller = new controllerDIB_Competitor_iBase();
			
			controller.Init();				 
			controller.searchCriteria = '';
			controller.setPeriodChosen(month+'-' + year);  
			//controller.setSelectedAccountType('Adult'); 
			controller.displayCompetitors() ;
			
			//controller.getAccountTypes() ; 
			controller.getcanShowDG() ; 
			//controller.getCompetitors() ; 
			//controller.getDIB_Comp_Class_iBases() ; 
			controller.getDIB_Comp_iBase() ; 
			controller.getPeriodChosen() ; 
			controller.getPeriodItems() ; 
			//controller.getSelectedAccountType() ; 
			controller.getSelectedMonth();
			controller.getSelectedYear() ;
			controller.getshowSubmitButton(); 
			
			controller.checkShowHideSubmitButton() ; 
			controller.setshowSubmitButton(true); 
			List<DIB_Competitor_iBase__c> comp_iBaseIn = [Select name from DIB_Competitor_iBase__c limit 20] ;		
			controller.setDIB_Comp_iBase(comp_iBaseIn);
			//controller.setCompetitors(controller.getcompetitors()) ; 
			controller.save() ;			
			
			
			// 2nd part  : 					
			
			controller.setPeriodChosen(month+'-' + year);
			//controller.getAccountFilterOptions();
			controller.getPeriodChosen2(); 
			//controller.setsearchCriteria('');
			//controller.getsearchCriteria();
			//controller.setAccountFilter('All Accounts') ; 
			controller.getPeriodChosen2();  
			//controller.setSelectedAccountType('Adult'); 
			Boolean hasNext = controller.hasNext ; 
			Boolean hasPrevious = controller.hasPrevious;
			List<SelectOption> pages = controller.pages; 
			controller.SelectedPage = String.valueOf(1) ; 
			controller.changePage();
			controller.first();			
			controller.next();
			controller.previous();
			controller.last();				
			/* controller.SubmitcurrentPeriod(); is having a problem with the number of rows retrieved.
				Instead of calling the  SubmitcurrentPeriod() method directly, it will be done by calling the Class DIB_Manage_Task_Status. 
			*/		
			
			controller.SubmitcurrentPeriod();			 
			

			

			controller.getTerritoryFilterOptions();
			controller.getSegmentFilterOptions();
			controller.getProductFilterOptions();
			
			Test.stopTest();
			
        }
    	
    }
}