//---------------------------------------------------------------------------------------------------------------------------------
//	Author 			: Bart Caelen
//  Created Date 	: 15-02-2019
//  Description 	: APEX Class for used for the CSOD (CornerStone On Demand) integration
//  Change Log 		: 
//---------------------------------------------------------------------------------------------------------------------------------
public with sharing class bl_ContactCSODIntegration {

	private final static Map<String, String> mapCSOD_Language = new Map<String, String>{'en-US' => 'English (US)','en-GB' => 'English (UK)','fr-CA' => 'French (CA)','de-DE' => 'German (Germany)','ru-RU' => 'Russian (Russia)','ja-JP' => 'Japanese (Japan)','zh-CN' => 'Chinese (Simplified)','zh-SG' => 'Chinese (Singapore)','zh-HK' => 'Chinese (Hong Kong SAR)','it-IT' => 'Italian (Italy)','pl-PL' => 'Polish (Poland)','pt-BR' => 'Portugese (Brazil)','fr-FR' => 'French (France)','es-MX' => 'Spanish (Latin America)','es-ES' => 'Spanish (Spain)','pt-PT' => 'Portugese (Portugal)','th-TH' => 'Thai (Thailand)','nl-NL' => 'Dutch (The Netherlands)','tr-TR' => 'Turkish (Turkey)','ro-RO' => 'Romanian (Romania)'};

	//---------------------------------------------------------------------------------------------------------------------------
	// Corner Stone On Demand Integration
	// Make sure the processing Contact and his Manager exists in CSOD before we can create a Training Assignment
	//---------------------------------------------------------------------------------------------------------------------------
	public static Contact doCSODIntegration(Contact oContact, Contact_ED_Sync__c oSync){

		Boolean bUpdateContact = false;

		try{

			if (String.isBlank(oSync.Outcome_Details__c)) oSync.Outcome_Details__c = '';

			oSync.Outcome_Details__c += '\nStart CSOD Integration.';
		
			// If the SFDC Campaign has no Curriculum_ID__c, no further action is needed
			if (String.isBlank(oSync.Campaign__r.Curriculum_ID__c)){
			
				oSync.Outcome_Details__c += '\nThe Campaign has no Curriculum ID - no further processing possible.';
				return null;

			}
			oSync.Outcome_Details__c += '\nThe Campaign has a Curriculum ID : ' + oSync.Campaign__r.Curriculum_ID__c + '.';


			if (oContact.LMS_Activation_Date__c == null){

				oSync.Outcome_Details__c += '\nThe Contact LMS Activiation Date is empty.';

				System.debug('**BC** doCSODIntegration - oContact.LMS_Userid__c : ' + oContact.LMS_Userid__c);
				System.debug('**BC** doCSODIntegration - oContact.Id : ' + oContact.Id);

				// Get Employee from CSOD based on the LMS_Userid__c
				employeeResults oEmployeeResults = getEmployee(oContact.LMS_Userid__c);

				System.debug('**BC** doCSODIntegration - oEmployeeResults : ' + oEmployeeResults);

				// Verify that the Employee exists in CSOD
				if (!String.isBlank(oEmployeeResults.userIdentifier)){
			
					oSync.Outcome_Details__c += '\nEmployee User found in CSOD (' + oContact.LMS_Userid__c + ').';
	
					// The employee exists in CSOD - Update the Employee in CSOD
					updateEmployeeResult oUpdateEmployeeResult = updateEmployee(oContact, oEmployeeResults);

					if (oUpdateEmployeeResult == null){
	
						oSync.Outcome_Details__c += '\nUpdate User Employee in CSOD not needed.';
				
					}else{
				
						oSync.Outcome_Details__c += '\nUpdated Employee User in CSOD.';

					}
			
				}else{

					oSync.Outcome_Details__c += '\nEmployee User not found in CSOD (' + oContact.LMS_Userid__c + ').';

					String tEmployeeManagerId = '';

					// Get the Manager of the Employee in CSOD
					employeeResults oEmployee_Manager = getEmployee(oContact.LMS_Key_Customer_Contact__r.Alias_unique__c);

					System.debug('**BC** doCSODIntegration - oEmployee_Manager : ' + oEmployee_Manager);
				
					if (!String.isBlank(oEmployee_Manager.userIdentifier)){
				
						oSync.Outcome_Details__c += '\nManager User found in CSOD.';

					}else{
				
						oSync.Outcome_Details__c += '\nManager User not found in CSOD.';
	
						// Create the Manager of the Employee in CSOD
						createEmployeeResult oEmployeeManager_New = createEmployeeManager(oContact);

						oSync.Outcome_Details__c += '\nManager User created in CSOD.';

					}

					// Create the Employee in CSOD
					createEmployeeResult oEmployee_New = createEmployee(oContact);
			
					System.debug('**BC** doCSODIntegration - oEmployee_New : ' + oEmployee_New);

					oSync.Outcome_Details__c += '\nEmployee User created in CSOD.';

				}

				// Update the LMS Date Activation on the Contact Employee in SFDC
				oContact.LMS_Activation_Date__c = Date.today();
				bUpdateContact = true;

			}else{
		
				oSync.Outcome_Details__c += '\nThe Contact LMS Activiation Date is populated - continue with Training Assignments.';

			}

			// Create the Training Assignments
			if (oContact.LMS_Activation_Date__c != null){

				oSync.Outcome_Details__c += '\nThe Contact LMS Activiation Date is populated (' + oContact.LMS_Activation_Date__c + ') - continue with Training Assignments.';

				createTrainingAssignment(oSync.Campaign__r.Curriculum_ID__c, oContact.LMS_Userid__c);
				
				oSync.Outcome_Details__c += '\nThe Training Assignment in COSD is completed.';

			}

			oSync.Outcome_Details__c += '\nEnd CSOD Integration.';

			if (bUpdateContact){
				return oContact;
			}

		}catch(Exception oEX){
			
			throw new ws_Exception('Error in doCSODIntegration on line ' + oEX.getLineNumber() + ' : ' + oEX.getMessage());
		}


		return null;
		
	}
	//---------------------------------------------------------------------------------------------------------------------------


	//---------------------------------------------------------------------------------------------------------------------------
	// Retrieve the Employee from CSOD
	//	INPUT : tExternalUserId
	//	OUTPUT : employeeResults
	//---------------------------------------------------------------------------------------------------------------------------
	@TestVisible public static employeeResults getEmployee(String tUserIdentifier){
		
		Http oHTTP = new Http();
		HttpRequest oHTTPRequest = new HttpRequest();
			oHTTPRequest.setEndpoint(oAPISettings.Target_Server_URL__c + '/active-bpel/rt/provision_user?userIdentifier=' + tUserIdentifier);
			oHTTPRequest.setMethod('GET');
			oHTTPRequest.setHeader('X-MDT-Method', 'GET');
			oHTTPRequest.setHeader('Authorization', oAPISettings.Token_Authentication__c);
			oHTTPRequest.setHeader('Content-Length', '0');
			if (Test.isRunningTest()) oHTTPRequest.setHeader('WS_TYPE', 'GET_EMPLOYEE');
			oHTTPRequest.setTimeout(Integer.valueOf(oAPISettings.Timeout__c));
		HttpResponse oHTTPResponse = oHTTP.send(oHTTPRequest);

		System.debug('**BC** getEmployee - tUserIdentifier : ' + tUserIdentifier);
		System.debug('**BC** getEmployee - getStatusCode : ' + oHTTPResponse.getStatusCode());
		System.debug('**BC** getEmployee - getEndpoint : ' + oHTTPRequest.getEndpoint());
		System.debug('**BC** getEmployee - oHTTPRequest 1 : ' + oHTTPRequest.toString());
		System.debug('**BC** getEmployee - oHTTPRequest 2 : ' + String.valueOf(oHTTPRequest.getBody()));
		System.debug('**BC** getEmployee - oHTTPResponse 1 : ' + oHTTPResponse.toString());
		System.debug('**BC** getEmployee - oHTTPResponse 2 : ' + String.valueOf(oHTTPResponse.getBody()));
		
		if (oHTTPResponse.getStatusCode() != 200){

			if (oHTTPResponse.getBody().contains('No records found for the requested Id')) return new employeeResults();
		
			throw new ws_Exception('Error retrieving the Employee with User Indentifier "' + tUserIdentifier + '": ' + oHTTPResponse.getStatus() + ' (' + oHTTPResponse.getStatusCode() + ') (' + String.valueOf(oHTTPRequest.getBody()) + ') (' + String.valueOf(oHTTPResponse.getBody()) + ')');
		
		}

		ResponseHeader oResponseHeader = (ResponseHeader)JSON.deserialize(oHTTPResponse.getBody(), ResponseHeader.class);
		System.debug('**BC** getEmployee - oResponseHeader.employeeResults : ' + oResponseHeader.employeeResults);
    	return oResponseHeader.employeeResults;

	}
	//---------------------------------------------------------------------------------------------------------------------------


	//---------------------------------------------------------------------------------------------------------------------------
	// Create the Employee in CSOD
	//	INPUT : Contact
	//	OUTPUT : createEmployeeResult
	//---------------------------------------------------------------------------------------------------------------------------
	@TestVisible public static createEmployeeResult createEmployee(Contact oContact){

		EmployeeRequest oEmployeeRequest = createEmployeeRequest(oContact);

		Http oHTTP = new Http();
		HttpRequest oHTTPRequest = new HttpRequest();
			oHTTPRequest.setEndpoint(oAPISettings.Target_Server_URL__c + '/active-bpel/rt/provision_user');
			oHTTPRequest.setMethod('POST');
			oHTTPRequest.setHeader('X-MDT-Method', 'POST');
			oHTTPRequest.setHeader('Authorization', oAPISettings.Token_Authentication__c);
			oHTTPRequest.setHeader('Content-Length', '0');
			oHTTPRequest.setHeader('Content-Type', 'application/json');
			if (Test.isRunningTest()) oHTTPRequest.setHeader('WS_TYPE', 'CREATE_EMPLOYEE');
			oHTTPRequest.setTimeout(Integer.valueOf(oAPISettings.Timeout__c));
			oHTTPRequest.setBody(JSON.serialize(oEmployeeRequest));
		HttpResponse oHTTPResponse = oHTTP.send(oHTTPRequest);

		System.debug('**BC** createEmployee - oEmployeeRequest : ' + oEmployeeRequest);
		System.debug('**BC** createEmployee - oHTTPRequest 1 : ' + oHTTPRequest.toString());
		System.debug('**BC** createEmployee - oHTTPRequest 2 : ' + String.valueOf(oHTTPRequest.getBody()));
		System.debug('**BC** createEmployee - oHTTPResponse 1 : ' + oHTTPResponse.toString());
		System.debug('**BC** createEmployee - oHTTPResponse 2 : ' + String.valueOf(oHTTPResponse.getBody()));

		if (oHTTPResponse.getStatusCode() != 200) throw new ws_Exception('Error creating the Employee: ' + oHTTPResponse.getStatus() + ' (' + oHTTPResponse.getStatusCode() + ') (' + String.valueOf(oHTTPRequest.getBody()) + ') (' + String.valueOf(oHTTPResponse.getBody()) + ')');

		ResponseHeader oResponseHeader = (ResponseHeader)JSON.deserialize(oHTTPResponse.getBody(), ResponseHeader.class);
		System.debug('**BC** createEmployee - oResponseHeader.createEmployeeResult : ' + oResponseHeader.createEmployeeResult);
    	return oResponseHeader.createEmployeeResult;

	}
	//---------------------------------------------------------------------------------------------------------------------------


	//---------------------------------------------------------------------------------------------------------------------------
	// Create the Manager of the Employee in CSOD
	//	INPUT : Contact
	//	OUTPUT : createEmployeeResult
	//---------------------------------------------------------------------------------------------------------------------------
	@TestVisible private static createEmployeeResult createEmployeeManager(Contact oContact){

		EmployeeRequest oEmployeeRequest = new EmployeeRequest();

			oEmployeeRequest.userIdentifier = clsUtil.isNull(oContact.LMS_Key_Customer_Contact__r.Alias_unique__c, '');
			oEmployeeRequest.userName = clsUtil.isNull(oContact.LMS_Key_Customer_Contact__r.Alias_unique__c, '');

			oEmployeeRequest.firstName = clsUtil.isNull(oContact.LMS_Key_Customer_Contact__r.FirstName, '');
			oEmployeeRequest.lastName = clsUtil.isNull(oContact.LMS_Key_Customer_Contact__r.LastName, '');
			oEmployeeRequest.prefix = clsUtil.isNull(oContact.LMS_Key_Customer_Contact__r.Title, '');
			oEmployeeRequest.primaryEmail = clsUtil.isNull(oContact.LMS_Key_Customer_Contact__r.Email, '');

			oEmployeeRequest.countryName = clsUtil.getCountryISOCode3ByIsoCode(oContact.LMS_Country__c, 'NLD');

			oEmployeeRequest.isActive = 'true';
			oEmployeeRequest.displayLanguage = 'English (UK)';
			oEmployeeRequest.divisionId = 'EMEA_EXT';

			oEmployeeRequest.locationId = 'EXT_' + clsUtil.isNull(oContact.LMS_Country__c, 'NL');

		Http oHTTP = new Http();
		HttpRequest oHTTPRequest = new HttpRequest();
			oHTTPRequest.setEndpoint(oAPISettings.Target_Server_URL__c + '/active-bpel/rt/provision_user');
			oHTTPRequest.setMethod('POST');
			oHTTPRequest.setHeader('X-MDT-Method', 'POST');
			oHTTPRequest.setHeader('Authorization', oAPISettings.Token_Authentication__c);
			oHTTPRequest.setHeader('LearningManager', 'true');
			oHTTPRequest.setHeader('Content-Length', '0');
			oHTTPRequest.setHeader('Content-Type', 'application/json');
			if (Test.isRunningTest()) oHTTPRequest.setHeader('WS_TYPE', 'CREATE_EMPLOYEE_MANAGER');
			oHTTPRequest.setTimeout(Integer.valueOf(oAPISettings.Timeout__c));
			oHTTPRequest.setBody(JSON.serialize(oEmployeeRequest));
		HttpResponse oHTTPResponse = oHTTP.send(oHTTPRequest);

		if (Test.isRunningTest()){
			TEST_CSODIntegration_Mock oMock = new TEST_CSODIntegration_Mock();
			oMock.bCreateEmployeeManager = true;
			oMock.bError = false;
			oHTTPResponse = oMock.respond(oHTTPRequest);
		}

		System.debug('**BC** createEmployeeManager - oEmployeeRequest : ' + oEmployeeRequest);
		System.debug('**BC** createEmployeeManager - oHTTPRequest 1 : ' + oHTTPRequest.toString());
		System.debug('**BC** createEmployeeManager - oHTTPRequest 2 : ' + String.valueOf(oHTTPRequest.getBody()));
		System.debug('**BC** createEmployeeManager - oHTTPResponse 1 : ' + oHTTPResponse.toString());
		System.debug('**BC** createEmployeeManager - oHTTPResponse 2 : ' + String.valueOf(oHTTPResponse.getBody()));
		
		if (oHTTPResponse.getStatusCode() != 200) throw new ws_Exception('Error creating the Employee Manager: ' + oHTTPResponse.getStatus() + ' (' + oHTTPResponse.getStatusCode() + ') (' + String.valueOf(oHTTPRequest.getBody()) + ') (' + String.valueOf(oHTTPResponse.getBody()) + ')');

		ResponseHeader oResponseHeader = (ResponseHeader)JSON.deserialize(oHTTPResponse.getBody(), ResponseHeader.class);
		System.debug('**BC** createEmployeeManager - oResponseHeader.createEmployeeResult : ' + oResponseHeader.createEmployeeResult);
    	return oResponseHeader.createEmployeeResult;

	}
	//---------------------------------------------------------------------------------------------------------------------------


	//---------------------------------------------------------------------------------------------------------------------------
	// Update the Employee in CSOD
	//	INPUT : Contact
	//	OUTPUT : EmployeeResponse (Response Success or Response Error)
	//---------------------------------------------------------------------------------------------------------------------------
	@TestVisible public static updateEmployeeResult updateEmployee(Contact oContact, employeeResults oEmployee_Old){

		updateEmployeeResult oUpdateEmployeeResult;

		EmployeeRequest oEmployeeRequest = createEmployeeRequest(oContact);

		if (bEmployeeUpdateNeeded(oEmployee_Old, oEmployeeRequest)){

			Http oHTTP = new Http();
			HttpRequest oHTTPRequest = new HttpRequest();
				oHTTPRequest.setEndpoint(oAPISettings.Target_Server_URL_PATCH__c + '/UserProvision/UpdateEmployee');
				oHTTPRequest.setMethod('POST');
				oHTTPRequest.setHeader('X-MDT-Method', 'PATCH');
				oHTTPRequest.setHeader('Authorization', oAPISettings.Token_Authentication_PATCH__c);
				oHTTPRequest.setHeader('Content-Length', '0');
				oHTTPRequest.setHeader('Content-Type', 'application/json');
				if (Test.isRunningTest()) oHTTPRequest.setHeader('WS_TYPE', 'UPDATE_EMPLOYEE');
				oHTTPRequest.setTimeout(Integer.valueOf(oAPISettings.Timeout__c));
				oHTTPRequest.setBody(JSON.serialize(oEmployeeRequest));
			HttpResponse oHTTPResponse = oHTTP.send(oHTTPRequest);

			System.debug('**BC** updateEmployee - oHTTPRequest.getBodyDocument() : ' + oHTTPRequest.getBodyDocument());
			System.debug('**BC** updateEmployee - oHTTPRequest 1 : ' + oHTTPRequest.toString());
			System.debug('**BC** updateEmployee - oHTTPRequest 2 : ' + String.valueOf(oHTTPRequest.getBody()));
			System.debug('**BC** updateEmployee - oHTTPResponse 1 : ' + oHTTPResponse.toString());
			System.debug('**BC** updateEmployee - oHTTPResponse 2 : ' + String.valueOf(oHTTPResponse.getBody()));
			
		
			if (oHTTPResponse.getStatusCode() != 200) throw new ws_Exception('Error updating the Employee: ' + oHTTPResponse.getStatus() + ' (' + oHTTPResponse.getStatusCode() + ') (' + String.valueOf(oHTTPRequest.getBody()) + ') (' + String.valueOf(oHTTPResponse.getBody()) + ')');

			oUpdateEmployeeResult = (updateEmployeeResult)JSON.deserialize(oHTTPResponse.getBody(), updateEmployeeResult.class);
		
		}
    	
		System.debug('**BC** updateEmployee - oUpdateEmployeeResult : ' + oUpdateEmployeeResult);
    	return oUpdateEmployeeResult;

	}
	//---------------------------------------------------------------------------------------------------------------------------


	//---------------------------------------------------------------------------------------------------------------------------
	// Create the Training Assignment in CSOD
	//	INPUT : tTrainingID (Campaign Curriculum Id) + tTrainingAssignmentUserId (Contact Id)
	//	OUTPUT : TrainingAssignmentResponse (Response Success or Response Error)
	//---------------------------------------------------------------------------------------------------------------------------
	@TestVisible public static trainingAssignmentResult createTrainingAssignment(String tTrainingID, String tTrainingAssignmentUserId){

		TrainingAssignmentRequest oTrainingAssignmentRequest = new TrainingAssignmentRequest();
			oTrainingAssignmentRequest.trainingId = tTrainingID;
			oTrainingAssignmentRequest.trainingAssignmentUserId = tTrainingAssignmentUserId;

		Http oHTTP = new Http();
		HttpRequest oHTTPRequest = new HttpRequest();
			oHTTPRequest.setEndpoint(oAPISettings.Target_Server_URL__c + '/active-bpel/rt/provision_training');
			oHTTPRequest.setMethod('POST');
			oHTTPRequest.setHeader('X-MDT-Method', 'POST');
			oHTTPRequest.setHeader('Authorization', oAPISettings.Token_Authentication__c);
			oHTTPRequest.setHeader('Content-Length', '0');
			oHTTPRequest.setHeader('Content-Type', 'application/json');
			if (Test.isRunningTest()) oHTTPRequest.setHeader('WS_TYPE', 'CREATE_TRAINING_ASSIGNMENT');
			oHTTPRequest.setTimeout(Integer.valueOf(oAPISettings.Timeout__c));
			oHTTPRequest.setBody(JSON.serialize(oTrainingAssignmentRequest));
		HttpResponse oHTTPResponse = oHTTP.send(oHTTPRequest);

		System.debug('**BC** createTrainingAssignment - oTrainingAssignmentRequest : ' + oTrainingAssignmentRequest);
		System.debug('**BC** createTrainingAssignment - oHTTPRequest 1 : ' + oHTTPRequest.toString());
		System.debug('**BC** createTrainingAssignment - oHTTPRequest 2 : ' + String.valueOf(oHTTPRequest.getBody()));
		System.debug('**BC** createTrainingAssignment - oHTTPResponse 1 : ' + oHTTPResponse.toString());
		System.debug('**BC** createTrainingAssignment - oHTTPResponse 2 : ' + String.valueOf(oHTTPResponse.getBody()));
		
		if (oHTTPResponse.getStatusCode() != 200) throw new ws_Exception('Error creating the Training Assignment: ' + oHTTPResponse.getStatus() + ' (' + oHTTPResponse.getStatusCode() + ') (' + String.valueOf(oHTTPRequest.getBody()) + ') (' + String.valueOf(oHTTPResponse.getBody()) + ')');

    	ResponseHeader oResponseHeader = (ResponseHeader)JSON.deserializeStrict(oHTTPResponse.getBody(), ResponseHeader.class);
		System.debug('**BC** createTrainingAssignment - oResponseHeader.trainingAssignmentResult : ' + oResponseHeader.trainingAssignmentResult);
    	return oResponseHeader.trainingAssignmentResult;

	}
	//---------------------------------------------------------------------------------------------------------------------------


	//---------------------------------------------------------------------------------------------------------------------------
	// Logic to validate if the Existing Employee in CSOD (oEmployee_Old) needs to be updated if one of the processing fields is changed
	//---------------------------------------------------------------------------------------------------------------------------
	@TestVisible private static Boolean bEmployeeUpdateNeeded(employeeResults oEmployee_Old, EmployeeRequest oEmployee_New){
	
		Boolean bResult = false;

		if (
			(oEmployee_Old.userName != oEmployee_New.userName)
			|| (oEmployee_Old.firstName != oEmployee_New.firstName)
			|| (oEmployee_Old.lastName != oEmployee_New.lastName)
			|| (oEmployee_Old.prefix != oEmployee_New.prefix)
			|| (oEmployee_Old.primaryEmail != oEmployee_New.primaryEmail)
			|| (oEmployee_Old.countryName != oEmployee_New.countryName)
			|| (oEmployee_Old.isActive != oEmployee_New.isActive)
			|| (oEmployee_Old.displayLanguage != oEmployee_New.displayLanguage)
			|| (oEmployee_Old.divisionId != oEmployee_New.divisionId)
			|| (oEmployee_Old.hcpNumber != oEmployee_New.hcpNumber)
			|| (oEmployee_Old.locationId != oEmployee_New.locationId)

			|| (oEmployee_Old.learningManagerUserId != oEmployee_New.learningManagerUserId)
			|| (oEmployee_Old.learningManagerFirstName != oEmployee_New.learningManagerFirstName)
			|| (oEmployee_Old.learningManagerLastName != oEmployee_New.learningManagerLastName)
			|| (oEmployee_Old.learningManagerEmail != oEmployee_New.learningManagerEmail)
			|| (oEmployee_Old.learningManagerPhone != oEmployee_New.learningManagerPhone)
		){
			bResult = true;
		}
		bResult = true;
		return bResult;

	}
	//---------------------------------------------------------------------------------------------------------------------------



	//---------------------------------------------------------------------------------------------------------------------------
	// Convert a Contact into an EmployeeRequest Class
	//---------------------------------------------------------------------------------------------------------------------------
	@TestVisible private static EmployeeRequest createEmployeeRequest(Contact oContact){
	
		EmployeeRequest oEmployee = new EmployeeRequest();
			
			oEmployee.userIdentifier = clsUtil.isNull(oContact.LMS_Userid__c, '');
			oEmployee.userName = clsUtil.isNull(oContact.Email, '');
			oEmployee.firstName = clsUtil.isNull(oContact.FirstName, '');
			oEmployee.lastName = clsUtil.isNull(oContact.LastName, '');
			oEmployee.prefix = clsUtil.isNull(oContact.Title, '');
			oEmployee.primaryEmail = clsUtil.isNull(oContact.Email, '');

			oEmployee.countryName = clsUtil.getCountryISOCode3ByIsoCode(oContact.LMS_Country__c, 'NLD');

			if (oContact.LMS_isActive__c == true){
				oEmployee.isActive = 'true';
			}else{
				oEmployee.isActive = 'false';
			}

			String tLanguage = clsUtil.isNull(oContact.LMS_Language__c, '');
			if (mapCSOD_Language.containsKey(tLanguage)){
				tLanguage = mapCSOD_Language.get(tLanguage);
			}else{
				tLanguage = 'English (UK)';
			}
			oEmployee.displayLanguage = tLanguage;

			oEmployee.divisionId = clsUtil.isNull(oContact.LMS_Division__c, '');
			oEmployee.hcpNumber = clsUtil.isNull(oContact.Identification_Number__c, '');

			oEmployee.locationId = 'EXT_' + clsUtil.isNull(oContact.LMS_Country__c, 'NL');

			oEmployee.learningManagerUserId = clsUtil.isNull(oContact.LMS_Key_Customer_Contact__r.Alias_unique__c, '');
			oEmployee.learningManagerFirstName = clsUtil.isNull(oContact.LMS_Key_Customer_Contact__r.FirstName, '');
			oEmployee.learningManagerLastName = clsUtil.isNull(oContact.LMS_Key_Customer_Contact__r.LastName, '');
			oEmployee.learningManagerEmail = clsUtil.isNull(oContact.LMS_Key_Customer_Contact__r.Email, '');
			oEmployee.learningManagerPhone = clsUtil.isNull(oContact.LMS_Key_Customer_Contact__r.Phone, '');

		return oEmployee;

	}
	//---------------------------------------------------------------------------------------------------------------------------


	//---------------------------------------------------------------------------------------------------------------------------
	// Retrieve the API Settings based on the OrganizationId of the User
	//---------------------------------------------------------------------------------------------------------------------------
 	@TestVisible private static CSOD_API_Settings__c oAPISettings{
 		
 		get{
 			
 			if (oAPISettings == null){
 				
 				CSOD_API_Settings__c oSettings = CSOD_API_Settings__c.getInstance();
				Id id_OrgId = oSettings.Org_Id__c; // Making sure we use the 18 character version of the ID in the Custom Setting
 				
 				if ( 
					(oSettings.Active__c == false) 
					|| (id_OrgId != UserInfo.getOrganizationId())
				){
					throw new ws_Exception('No API Settings available (CSOD_API_Settings__c), please contact your System Administrator.');
				}
 				
 				oAPISettings = oSettings;

 			}
 			
 			return oAPISettings;

 		}
		
		set; 		

 	}
	//---------------------------------------------------------------------------------------------------------------------------


	

	//------------------------------------------------------------------------------------------
	// HELPER CLASSES
	//------------------------------------------------------------------------------------------
	// Request for CREATE EMPLOYEE / CREATE EMPLOYEE MANAGER adn UPDATE EMPLOYEE
	public class EmployeeRequest{

		public EmployeeRequest(){

			userIdentifier = '';
			userName = '';
			firstName = '';
			lastName = '';
			prefix = '';
			primaryEmail = '';
			countryName = '';
			isActive = '';
			displayLanguage = '';
			divisionId = '';
			hcpNumber = '';
			locationId = '';

			learningManagerUserId = '';
			learningManagerFirstName = '';
			learningManagerLastName = '';
			learningManagerEmail = '';
			learningManagerPhone = '';

		}

		public String userIdentifier { get; set; }
		public String userName { get; set; }
		public String firstName { get; set; }
		public String lastName { get; set; }
		public String prefix { get; set; }
		public String primaryEmail { get; set; }
		public String countryName { get; set; }
		public String isActive { get; set; }
		public String displayLanguage { get; set; }
		public String divisionId { get; set; }
		public String hcpNumber { get; set; }
		public String locationId { get; set; }

		public String learningManagerUserId { get; set; }
		public String learningManagerFirstName { get; set; }
		public String learningManagerLastName { get; set; }
		public String learningManagerEmail { get; set; }
		public String learningManagerPhone { get; set; }
	
	}

	// Used as Repsonse for GET EMPLOYEE (employeeResults), CREATE EMPLOYEE (createEmployeeResults) and TRAINING ASSIGNMENTS (trainingAssignmentResult)
	public class ResponseHeader{

		public ResponseHeader(){
			employeeResults = new employeeResults();
			createEmployeeResult = new createEmployeeResult();
			trainingAssignmentResult = new trainingAssignmentResult();
		}

		public employeeResults employeeResults { get; set; } 
		public createEmployeeResult createEmployeeResult { get; set; }
		public trainingAssignmentResult trainingAssignmentResult { get; set; }

	}

	// Response for ERRORS (status != 200)
	public class ErrorResponse{

		public ErrorResponse(){
			errorCode = '';
			errorType = '';
			errorDescription = '';
			supportContact = '';
		}

		// Response Error
		public String errorCode { get; set; }
	    public String errorType { get; set; }
	    public String errorDescription { get; set; }    
		public String supportContact { get; set; }

	}
	
	// Response for CREATE EMPLOYEE
	public class createEmployeeResult{

		public createEmployeeResult(){
			externalUserId = '';
			timestamp = '';
		}

		// Response Success
		public String externalUserId { get; set; }
		public String timestamp { get; set; }

	}


	// Response for UPDATE EMPLOYEE
	public class updateEmployeeResult{

		public updateEmployeeResult(){
			externalUserId = '';
			timestamp = '';
		}

		// Response Success
		public String externalUserId { get; set; }
		public String timestamp { get; set; }

	}


	// Response for GET EMPLOYEE
	public class employeeResults {

		public employeeResults(){
			externalUserId = '';
			userIdentifier= '';
			userName = '';
			firstName = '';
			lastName = '';
			middleName = '';
			prefix = '';
			suffix = '';
			primaryEmail = '';
			countryName = '';
			isActive = '';
			displayLanguage = '';
			learningManagerUserId = '';
			learningManagerFirstName = '';
			learningManagerLastName = '';
			divisionId = '';
			hcpNumber = '';
			locationId = '';
			learningManagerEmail = '';
			learningManagerPhone = '';
		}

		public String externalUserId { get; set; }
		public String userIdentifier { get; set; }
		public String userName { get; set; }
		public String firstName { get; set; }
		public String lastName { get; set; }
		public String middleName { get; set; }
		public String prefix { get; set; }
		public String suffix { get; set; }
		public String primaryEmail { get; set; }
		public String countryName { get; set; }
		public String isActive { get; set; }
		public String displayLanguage { get; set; }
		public String learningManagerUserId { get; set; }
		public String learningManagerFirstName { get; set; }
		public String learningManagerLastName { get; set; }
		public String divisionId { get; set; }
		public String hcpNumber { get; set; }
		public String locationId { get; set; }
		public String learningManagerEmail { get; set; }
		public String learningManagerPhone { get; set; }

	}

	// Response for TRAINING ASSIGNMENT
	public class trainingAssignmentResult{
	
		public trainingAssignmentResult(){
			status = '';
			Reason = '';
		}

		public String status { get; set; }
		public String Reason { get; set; }

	}

	// Request for TRAINING ASSIGNMENT
	public class TrainingAssignmentRequest{

		public TrainingAssignmentRequest(){
			trainingId = '';
			trainingAssignmentUserId = '';
		}
	
		public String trainingId { get; set; }
		public String trainingAssignmentUserId { get; set; }
	
	}
	//------------------------------------------------------------------------------------------

}
//---------------------------------------------------------------------------------------------------------------------------------