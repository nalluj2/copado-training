public with sharing class ctrlExt_Opportunity_Send_Email {
	
	private Opportunity opp;
	public List<RadioOption> options {get; set;}
	
	public ctrlExt_Opportunity_Send_Email(ApexPages.StandardController sc){
		
		if(Test.isRunningTest() == false) sc.addFields(new List<String>{'AccountId'});
		
		opp = (Opportunity) sc.getRecord();
		
		options = new List<RadioOption>();
		
		for(CAN_DIB_Department_Emails__c emailOption : [Select Email__c, Department__c from CAN_DIB_Department_Emails__c ORDER BY Order__c ASC]){
		
			RadioOption option = new RadioOption();
			option.value = emailOption.Email__c;
			option.label = emailOption.Department__c;
			option.selected = false;
		
			options.add(option);
		}
	}
	
	public pageReference goToEmail(){
		
		PageReference pr = new PageReference('/_ui/core/email/author/EmailAuthor');
		pr.getParameters().put('p3_lkid', opp.Id);
		pr.getParameters().put('rtype', '003');
		pr.getParameters().put('p2_lkid', opp.AccountId);
		pr.getParameters().put('retURL', '/' +  opp.Id);
		
		List<String> selectedOptions = new List<String>();
		
		for(RadioOption option : options){
			
			if(option.selected == true) selectedOptions.add(option.value); 
		}		
		
		if(selectedOptions.size() > 0) pr.getParameters().put('p24', String.join(selectedOptions, ';'));
		
		return pr;
	}
	
	public class RadioOption{
		
		public String value {get; set;}
		public String label {get; set;}
		public Boolean selected {get; set;}
	}
    
}