//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 03-03-2016
//  Description      : APEX Test Class to test the logic in ut_log
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_ut_Log {


    //----------------------------------------------------------------------------------------
    // CREATE TEST DATA
    //----------------------------------------------------------------------------------------
    @testSetup static void createTestData() {

        Test.startTest();

        // Create Custom Setting - WebService Setting Data
        clsTestData.createCustomSettingData_WebServiceSetting(true);

        // Create Case Data
        clsTestData.createCaseData();

        // Create Service Order Data
        clsTestData.createSVMXCServiceOrderData(true);

        // Create NotificationSAPLog__c Data
        List<NotificationSAPLog__c> lstNotificationSAPLog = new List<NotificationSAPLog__c>();
		NotificationSAPLog__c oNotificationSAPLog = new NotificationSAPLog__c();
            oNotificationSAPLog.SFDC_Object_Name__c = 'SVMXC__Service_Order__c';
            oNotificationSAPLog.SFDC_Fieldname_ID__c = 'Id';
            oNotificationSAPLog.SFDC_Fieldname_SAPID__c = 'SVMX_SAP_Service_Order_No__c';
            oNotificationSAPLog.Record_ID__c = clsTestData.oMain_SVMXCServiceOrder.Id;
            oNotificationSAPLog.Record_SAPID__c = clsTestData.oMain_SVMXCServiceOrder.SVMX_SAP_Service_Order_No__c;
			oNotificationSAPLog.WebServiceName__c = 'ServiceOrder_NotificationSAP';
            oNotificationSAPLog.EndpointUrl__c = 'http://144.15.228.24:33500/ws/MdtAny_SfdcInt.SRServiceOrder.webservice.providers.ws.processServiceOrder_WSDL/MdtAny_SfdcInt_SRServiceOrder_webservice_providers_ws_processServiceOrder_WSDL_Port';
           	oNotificationSAPLog.Authorization_Header__c = 'BASIC U01BWDpTbWF4IUAjdFl1';
           	oNotificationSAPLog.Request__c = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:mdt="http://msplwa107-pr.wby1-dev.medtronic.com/MdtAny_SfdcInt.SRServiceOrder.webservice.providers.ws:processServiceOrder_WSDL"><soapenv:Header/><soapenv:Body><mdt:processServiceOrder><SRServiceOrderRequest><Object>ServiceOrder</Object><Operation>UPDATE</Operation><Process>ServiceOrderUpdate</Process><SFDC_ID>a88110000004EdZAAU</SFDC_ID><SAP_ID>000004598435</SAP_ID></SRServiceOrderRequest></mdt:processServiceOrder></soapenv:Body></soapenv:Envelope>';
           	oNotificationSAPLog.Response__c = '<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body><ser-root:processServiceOrderResponse xmlns:ser-root="http://msplwa107-pr.wby1-dev.medtronic.com/MdtAny_SfdcInt.SRServiceOrder.webservice.providers.ws:processServiceOrder_WSDL" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><SRServiceOrderResponse><isSuccess>TRUE</isSuccess><msg>Successfully processed ServiceOrderUpdateRequest from SMAX</msg></SRServiceOrderResponse></ser-root:processServiceOrderResponse></soapenv:Body></soapenv:Envelope>';
           	oNotificationSAPLog.Status__c = 'NEW';
           	oNotificationSAPLog.Direction__c = 'Outbound';
           	oNotificationSAPLog.Error_Description__c = '';
           	oNotificationSAPLog.User__c = UserInfo.getUserId();
			oNotificationSAPLog.WM_Object_Name__c = 'ServiceOrder';
			oNotificationSAPLog.WM_Operation__c = 'UPDATE';
			oNotificationSAPLog.WM_Process__c = 'ServiceOrderUpdate';
			oNotificationSAPLog.Retries__c = 0;
			oNotificationSAPLog.Retry_Cron_Expression__c = '';
		lstNotificationSAPLog.add(oNotificationSAPLog);
		insert lstNotificationSAPLog;


        Test.stopTest();

    }
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
    // TESTING
    //----------------------------------------------------------------------------------------
	@isTest static void test_logOutboundMessage() {
		
		Test.startTest();

		ut_Log.logOutboundMessage('internalId', 'externalId', 'request', 'response', 'status', 'operation', 'tObjectName');

		Test.stopTest();

	}
	
	@isTest static void test_upsertNotificationSAPLog() {

		// SELECT Data
		List<SVMXC__Service_Order__c> lstServiceOrder = [SELECT Id, SVMX_SAP_Service_Order_No__c FROM SVMXC__Service_Order__c];
		NotificationSAPLog__c oNotificationSAPLog = [SELECT Authorization_Header__c, Direction__c, EndpointUrl__c, Error_Description__c, Id, IsDeleted, Name, Outbound_Message_Request__c, Outbound_Message_Response__c, Record_ID__c, Record_SAPID__c, Request__c, Response__c, Retries__c, Retry_Cron_Expression__c, Retry_Cron_Job_Name__c, Retry_DateTime__c, Send_Date__c, SFDC_Fieldname_ID__c, SFDC_Fieldname_SAPID__c, SFDC_Object_Name__c, Status__c, User__c, WebServiceName__c, WM_Object_Name__c, WM_Operation__c, WM_Process__c FROM NotificationSAPLog__c LIMIT 1];

		// CREATE NotificationSAP
		bl_NotificationSAP.NotificationSAP oNotificationSAP = new bl_NotificationSAP.NotificationSAP();
			oNotificationSAP.oNotificationSAPLog = oNotificationSAPLog;
			oNotificationSAP.tWM_Object = oNotificationSAPLog.WM_Object_Name__c;
			oNotificationSAP.tWM_Operation = oNotificationSAPLog.WM_Operation__c;
			oNotificationSAP.tWM_Process = oNotificationSAPLog.WM_Process__c;
			oNotificationSAP.tSFDC_Object = oNotificationSAPLog.SFDC_Object_Name__c;
			oNotificationSAP.tSFDC_FieldName_Id = oNotificationSAPLog.SFDC_Fieldname_ID__c;
			oNotificationSAP.tSFDC_FieldName_SAPID = oNotificationSAPLog.SFDC_Fieldname_SAPID__c;
			oNotificationSAP.tDirection = bl_NotificationSAP.DIRECTION;
			oNotificationSAP.tWebServiceName = oNotificationSAPLog.WebServiceName__c;

		Test.startTest();

		clsUtil.hasException = false;
		ut_Log.upsertNotificationSAPLog(oNotificationSAP);

		clsUtil.hasException = true;
		ut_Log.upsertNotificationSAPLog(oNotificationSAP);

		Test.stopTest();
	}

	@isTest static void test_updateNotificationSAPLogStatus() {

		// SELECT Data
		List<SVMXC__Service_Order__c> lstServiceOrder = [SELECT Id, SVMX_SAP_Service_Order_No__c FROM SVMXC__Service_Order__c];

		// Create an additional NotificationSAPLog
		NotificationSAPLog__c oNotificationSAPLog_Pending = new NotificationSAPLog__c();
            oNotificationSAPLog_Pending.SFDC_Object_Name__c = 'SVMXC__Service_Order__c';
            oNotificationSAPLog_Pending.SFDC_Fieldname_ID__c = 'Id';
            oNotificationSAPLog_Pending.SFDC_Fieldname_SAPID__c = 'SVMX_SAP_Service_Order_No__c';
            oNotificationSAPLog_Pending.Record_ID__c = lstServiceOrder[0].Id;
            oNotificationSAPLog_Pending.Record_SAPID__c = lstServiceOrder[0].SVMX_SAP_Service_Order_No__c;
			oNotificationSAPLog_Pending.WebServiceName__c = 'ServiceOrder_NotificationSAP';
            oNotificationSAPLog_Pending.EndpointUrl__c = 'http://144.15.228.24:33500/ws/MdtAny_SfdcInt.SRServiceOrder.webservice.providers.ws.processServiceOrder_WSDL/MdtAny_SfdcInt_SRServiceOrder_webservice_providers_ws_processServiceOrder_WSDL_Port';
           	oNotificationSAPLog_Pending.Authorization_Header__c = 'BASIC U01BWDpTbWF4IUAjdFl1';
           	oNotificationSAPLog_Pending.Request__c = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:mdt="http://msplwa107-pr.wby1-dev.medtronic.com/MdtAny_SfdcInt.SRServiceOrder.webservice.providers.ws:processServiceOrder_WSDL"><soapenv:Header/><soapenv:Body><mdt:processServiceOrder><SRServiceOrderRequest><Object>ServiceOrder</Object><Operation>UPDATE</Operation><Process>ServiceOrderUpdate</Process><SFDC_ID>a88110000004EdZAAU</SFDC_ID><SAP_ID>000004598435</SAP_ID></SRServiceOrderRequest></mdt:processServiceOrder></soapenv:Body></soapenv:Envelope>';
           	oNotificationSAPLog_Pending.Response__c = '<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body><ser-root:processServiceOrderResponse xmlns:ser-root="http://msplwa107-pr.wby1-dev.medtronic.com/MdtAny_SfdcInt.SRServiceOrder.webservice.providers.ws:processServiceOrder_WSDL" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><SRServiceOrderResponse><isSuccess>TRUE</isSuccess><msg>Successfully processed ServiceOrderUpdateRequest from SMAX</msg></SRServiceOrderResponse></ser-root:processServiceOrderResponse></soapenv:Body></soapenv:Envelope>';
           	oNotificationSAPLog_Pending.Status__c = 'PENDING';
           	oNotificationSAPLog_Pending.Direction__c = 'Outbound';
           	oNotificationSAPLog_Pending.Error_Description__c = '';
           	oNotificationSAPLog_Pending.User__c = UserInfo.getUserId();
			oNotificationSAPLog_Pending.WM_Object_Name__c = 'ServiceOrder';
			oNotificationSAPLog_Pending.WM_Operation__c = 'UPDATE';
			oNotificationSAPLog_Pending.WM_Process__c = 'ServiceOrderUpdate';
			oNotificationSAPLog_Pending.Retries__c = null;
			oNotificationSAPLog_Pending.Retry_Cron_Expression__c = '';
		insert oNotificationSAPLog_Pending;


		Test.startTest();

		ut_Log.updateNotificationSAPLogStatus(lstServiceOrder[0].Id, 'RETRY');

		clsUtil.hasException = false;
		ut_Log.updateNotificationSAPLogStatus(lstServiceOrder[0].Id, 'RETRY', 'REQUEST', 'RESPONSE');
		ut_Log.updateNotificationSAPLogStatus(lstServiceOrder[0].Id, 'TRUE', 'REQUEST', 'RESPONSE');
		ut_Log.updateNotificationSAPLogStatus(lstServiceOrder[0].Id, 'FALSE', 'REQUEST', 'RESPONSE');

		clsUtil.hasException = true;
		ut_Log.updateNotificationSAPLogStatus(lstServiceOrder[0].Id, 'RETRY', 'REQUEST', 'RESPONSE');

		Test.stopTest();
	}

	@isTest static void test_scheduleRetryNotificationSAP() {

		// SELECT Data
		NotificationSAPLog__c oNotificationSAPLog = [SELECT Authorization_Header__c, Direction__c, EndpointUrl__c, Error_Description__c, Id, IsDeleted, Name, Outbound_Message_Request__c, Outbound_Message_Response__c, Record_ID__c, Record_SAPID__c, Request__c, Response__c, Retries__c, Retry_Cron_Expression__c, Retry_Cron_Job_Name__c, Retry_DateTime__c, Send_Date__c, SFDC_Fieldname_ID__c, SFDC_Fieldname_SAPID__c, SFDC_Object_Name__c, Status__c, User__c, WebServiceName__c, WM_Object_Name__c, WM_Operation__c, WM_Process__c FROM NotificationSAPLog__c LIMIT 1];

		Test.startTest();

		clsUtil.hasException = false;
		ut_Log.scheduleRetryNotificationSAP(oNotificationSAPLog);

		clsUtil.hasException = true;
		ut_Log.scheduleRetryNotificationSAP(oNotificationSAPLog);

		Test.stopTest();
	}

	@isTest static void test_getNotificationSAPLog(){

		// SELECT Data
		NotificationSAPLog__c oNotificationSAPLog = [SELECT Authorization_Header__c, Direction__c, EndpointUrl__c, Error_Description__c, Id, IsDeleted, Name, Outbound_Message_Request__c, Outbound_Message_Response__c, Record_ID__c, Record_SAPID__c, Request__c, Response__c, Retries__c, Retry_Cron_Expression__c, Retry_Cron_Job_Name__c, Retry_DateTime__c, Send_Date__c, SFDC_Fieldname_ID__c, SFDC_Fieldname_SAPID__c, SFDC_Object_Name__c, Status__c, User__c, WebServiceName__c, WM_Object_Name__c, WM_Operation__c, WM_Process__c FROM NotificationSAPLog__c LIMIT 1];

		Test.startTest();

		clsUtil.hasException = false;
		ut_Log.getNotificationSAPLog(oNotificationSAPLog.Record_ID__c, oNotificationSAPLog.Status__c);

		clsUtil.hasException = true;
		ut_Log.getNotificationSAPLog(oNotificationSAPLog.Record_ID__c, oNotificationSAPLog.Status__c);

		Test.stopTest();
	}


	@isTest static void test_tSOQL_NotificationSAPLog(){

		Test.startTest();

		ut_Log.tSOQL_NotificationSAPLog();

		Test.stopTest();

	}

	@isTest static void test_loadNotificationSAPLog(){

		// SELECT Data
		NotificationSAPLog__c oNotificationSAPLog = [SELECT Authorization_Header__c, Direction__c, EndpointUrl__c, Error_Description__c, Id, IsDeleted, Name, Outbound_Message_Request__c, Outbound_Message_Response__c, Record_ID__c, Record_SAPID__c, Request__c, Response__c, Retries__c, Retry_Cron_Expression__c, Retry_Cron_Job_Name__c, Retry_DateTime__c, Send_Date__c, SFDC_Fieldname_ID__c, SFDC_Fieldname_SAPID__c, SFDC_Object_Name__c, Status__c, User__c, WebServiceName__c, WM_Object_Name__c, WM_Operation__c, WM_Process__c FROM NotificationSAPLog__c LIMIT 1];

		Test.startTest();

		clsUtil.hasException = false;
		ut_Log.loadNotificationSAPLog('Id = \'' + oNotificationSAPLog.Id + '\'', 'Id');

		clsUtil.hasException = true;
		ut_Log.loadNotificationSAPLog('Id = \'' + oNotificationSAPLog.Id + '\'', 'Id');
		
		Test.stopTest();

	}

	@isTest static void test_sendEmail() {

		// SELECT Data
		List<SVMXC__Service_Order__c> lstServiceOrder = [SELECT Id, SVMX_SAP_Service_Order_No__c FROM SVMXC__Service_Order__c];

		// Create an additional NotificationSAPLog
		NotificationSAPLog__c oNotificationSAPLog_Pending = new NotificationSAPLog__c();
            oNotificationSAPLog_Pending.SFDC_Object_Name__c = 'SVMXC__Service_Order__c';
            oNotificationSAPLog_Pending.SFDC_Fieldname_ID__c = 'Id';
            oNotificationSAPLog_Pending.SFDC_Fieldname_SAPID__c = 'SVMX_SAP_Service_Order_No__c';
            oNotificationSAPLog_Pending.Record_ID__c = lstServiceOrder[0].Id;
            oNotificationSAPLog_Pending.Record_SAPID__c = lstServiceOrder[0].SVMX_SAP_Service_Order_No__c;
			oNotificationSAPLog_Pending.WebServiceName__c = 'ServiceOrder_NotificationSAP';
            oNotificationSAPLog_Pending.EndpointUrl__c = 'http://144.15.228.24:33500/ws/MdtAny_SfdcInt.SRServiceOrder.webservice.providers.ws.processServiceOrder_WSDL/MdtAny_SfdcInt_SRServiceOrder_webservice_providers_ws_processServiceOrder_WSDL_Port';
           	oNotificationSAPLog_Pending.Authorization_Header__c = 'BASIC U01BWDpTbWF4IUAjdFl1';
           	oNotificationSAPLog_Pending.Request__c = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:mdt="http://msplwa107-pr.wby1-dev.medtronic.com/MdtAny_SfdcInt.SRServiceOrder.webservice.providers.ws:processServiceOrder_WSDL"><soapenv:Header/><soapenv:Body><mdt:processServiceOrder><SRServiceOrderRequest><Object>ServiceOrder</Object><Operation>UPDATE</Operation><Process>ServiceOrderUpdate</Process><SFDC_ID>a88110000004EdZAAU</SFDC_ID><SAP_ID>000004598435</SAP_ID></SRServiceOrderRequest></mdt:processServiceOrder></soapenv:Body></soapenv:Envelope>';
           	oNotificationSAPLog_Pending.Response__c = '<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body><ser-root:processServiceOrderResponse xmlns:ser-root="http://msplwa107-pr.wby1-dev.medtronic.com/MdtAny_SfdcInt.SRServiceOrder.webservice.providers.ws:processServiceOrder_WSDL" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><SRServiceOrderResponse><isSuccess>TRUE</isSuccess><msg>Successfully processed ServiceOrderUpdateRequest from SMAX</msg></SRServiceOrderResponse></ser-root:processServiceOrderResponse></soapenv:Body></soapenv:Envelope>';
           	oNotificationSAPLog_Pending.Status__c = 'PENDING';
           	oNotificationSAPLog_Pending.Direction__c = 'Outbound';
           	oNotificationSAPLog_Pending.Error_Description__c = '';
           	oNotificationSAPLog_Pending.User__c = UserInfo.getUserId();
			oNotificationSAPLog_Pending.WM_Object_Name__c = 'ServiceOrder';
			oNotificationSAPLog_Pending.WM_Operation__c = 'UPDATE';
			oNotificationSAPLog_Pending.WM_Process__c = 'ServiceOrderUpdate';
			oNotificationSAPLog_Pending.Retries__c = 0;
			oNotificationSAPLog_Pending.Retry_Cron_Expression__c = '';
		insert oNotificationSAPLog_Pending;


		Test.startTest();

		ut_log.sendEmail(ut_log.EMAIL_TYPE.MAX_RETRIES, oNotificationSAPLog_Pending);

		oNotificationSAPLog_Pending.Retries__c = 6;
		update oNotificationSAPLog_Pending;
		ut_log.sendEmail(ut_log.EMAIL_TYPE.MAX_RETRIES, oNotificationSAPLog_Pending);

		Test.stopTest();
	}
	
	@isTest static void test_updateNotificationSAPLogStatus_Retry() {

		// SELECT Data
		List<SVMXC__Service_Order__c> lstServiceOrder = [SELECT Id, SVMX_SAP_Service_Order_No__c FROM SVMXC__Service_Order__c];
		
		Interface_functional_error__c functionalError = new Interface_functional_error__c();
		functionalError.Name = 'Wrong status';
		functionalError.Equals_To__c = 'Specify a status ID';		
		insert functionalError;
			
		Test.startTest();

		String requestWithFunctionalError = '{"DIST_ERROR_DETAILS" : [ { "ERROR_TYPE" : "E", "ERROR_MESSAGE" : "Enter Batch"}]}';
		ut_Log.updateNotificationSAPLogStatus(lstServiceOrder[0].Id, 'FALSE', requestWithFunctionalError, 'RESPONSE');
		
		Test.stopTest();
		
		NotificationSAPLog__c notif = [Select Id, Status__c from NotificationSAPLog__c where Record_ID__c = :lstServiceOrder[0].Id];
		
		System.assertEquals('RETRY', notif.Status__c);
	}
	
	@isTest static void test_updateNotificationSAPLogStatus_noRetry() {

		// SELECT Data
		List<SVMXC__Service_Order__c> lstServiceOrder = [SELECT Id, SVMX_SAP_Service_Order_No__c FROM SVMXC__Service_Order__c];
		
		Interface_functional_error__c functionalError = new Interface_functional_error__c();
		functionalError.Name = 'Wrong status';
		functionalError.Equals_To__c = 'Specify a status ID';
		functionalError.Starts_With__c = 'Specify';
		functionalError.Contains__c = 'status';
		functionalError.Ends_With__c = 'status ID';
		insert functionalError;
			
		Test.startTest();

		String requestWithFunctionalError = '{"DIST_ERROR_DETAILS" : [ { "ERROR_TYPE" : "E", "ERROR_MESSAGE" : "Specify a status ID"}]}';
		ut_Log.updateNotificationSAPLogStatus(lstServiceOrder[0].Id, 'FALSE', requestWithFunctionalError, 'RESPONSE');
		
		Test.stopTest();
		
		NotificationSAPLog__c notif = [Select Id, Status__c from NotificationSAPLog__c where Record_ID__c = :lstServiceOrder[0].Id];
		
		System.assertEquals('FAILED', notif.Status__c);
	}
}