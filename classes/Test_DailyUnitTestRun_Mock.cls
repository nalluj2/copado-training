@isTest
global class Test_DailyUnitTestRun_Mock  implements HttpCalloutMock {
		    	
    global HTTPResponse respond(HTTPRequest req) {
		
		String enpoint = req.getEndpoint();
		
		String body = '';
		Integer statusCode = 200;
		
		//Get Coverage records
		if(enpoint.contains('/services/data/v45.0/tooling/query/')){
			
			bl_Check_UnitTest_Run_Results.QueryResponse queryResponse = new bl_Check_UnitTest_Run_Results.QueryResponse();
			
			queryResponse.records = new List<bl_Check_UnitTest_Run_Results.ApexCodeCoverageAggregate>(); 
			
			bl_Check_UnitTest_Run_Results.ApexCodeCoverageAggregate classCoverage = new bl_Check_UnitTest_Run_Results.ApexCodeCoverageAggregate();
			classCoverage.apexClassOrTrigger = new bl_Check_UnitTest_Run_Results.ApexClassOrTrigger();
			classCoverage.apexClassOrTrigger.Id = '01p';
			classCoverage.apexClassOrTrigger.Name = 'Test Class';
			classCoverage.numLinesCovered = 50; 
			classCoverage.numLinesUncovered = 50;
			
			queryResponse.records.add(classCoverage);
			
			bl_Check_UnitTest_Run_Results.ApexCodeCoverageAggregate triggerCoverage = new bl_Check_UnitTest_Run_Results.ApexCodeCoverageAggregate();
			triggerCoverage.apexClassOrTrigger = new bl_Check_UnitTest_Run_Results.ApexClassOrTrigger();
			triggerCoverage.apexClassOrTrigger.Id = '01q';
			triggerCoverage.apexClassOrTrigger.Name = 'Test Class';
			triggerCoverage.numLinesCovered = 50; 
			triggerCoverage.numLinesUncovered = 50;
			
			queryResponse.records.add(triggerCoverage);
			
			body = JSON.serializePretty(queryResponse);
			
		
		}else if(enpoint.contains('/services/data/v44.0/tooling/runTestsAsynchronous')){
			
			body = 'jobId';			
		}
		
        HttpResponse resp = new HttpResponse();
        resp.setHeader('Content-Type', 'application/json');       
        resp.setBody(body);
        resp.setStatusCode(statusCode);
                
        return resp;
    }    
}