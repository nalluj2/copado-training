/*
    Apex Class:bl_tr_InsertQuestionAndAnswers
    Description: Business Logic classs for tr_InsertQuestionAndAnswers trigger  
    Author - Kaushal/Dheeraj
    Created Date  - 31/05/2013
*/
public class bl_tr_InsertQuestionAndAnswers{
    
    public static void getInsertQAs(List<Case> lstCase){
        // GTutil.boolCaseQAFlag flag is use to check & prevent code to execute again & again
        if(GTutil.boolCaseQAFlag){        
            GTutil.boolCaseQAFlag = false;
            
            List<Questions_and_Answers__c> lstQA=new List<Questions_and_Answers__c>();
            list<case> lstcaseUpdate = new list<case>();

            // Start Check Record type from custom setting
            list<OMA_multiple_QA__c> lstOMAMultiQAs = OMA_multiple_QA__c.getAll().values();
            //lstOMAMultiQAs = [select name,Record_Type__c from OMA_multiple_QA__c];
            map<string,string> mapOMAMultiQAs = new map<string,string>();
            if(lstOMAMultiQAs.size()>0){
                for(OMA_multiple_QA__c OMARecType:lstOMAMultiQAs){
                    if(OMARecType.Record_Type__c!=null){
                        mapOMAMultiQAs.put(OMARecType.Record_Type__c,OMARecType.Record_Type__c);
                    }
                }
            }
            // End Check Record type from custom setting
            for(Case c:lstCase){
                if(mapOMAMultiQAs.get(c.Record_Type__c)!=null){
                    // Since Question is mandatory on Questions and Answers so not null check is mandtory 
                    if(c.OMA_Question__c!=null){
                        // Creating a list of Questions and Answers  
                        Questions_and_Answers__c QA=new Questions_and_Answers__c();
                        QA.Case__c=c.id;
                        QA.Answer__c=c.OMA_Answer__c;
                        QA.Question__c=c.OMA_Question__c;
                        lstQA.add(QA);

                        // update case, clearing Question, Anwser  fields.    
                        case updatecase=new case(id = c.id);
                        updatecase.OMA_Answer__c = '';
                        updatecase.OMA_Question__c = '';
                        lstcaseUpdate.add(updatecase);              


                    }
                }
            }
            
            // Insert data in Questions and Answers if list is not empty
            if(lstQA.size()>0){
                System.Debug('lstQA - ' + lstQA);
                insert lstQA;
            }
            
            // Updateing case to clear fields
            if(lstcaseUpdate.size()>0){
                update lstcaseUpdate;
            }
            
        }
    }
   
}