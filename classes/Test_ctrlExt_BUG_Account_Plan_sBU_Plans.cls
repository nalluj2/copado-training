@isTest
private class Test_ctrlExt_BUG_Account_Plan_sBU_Plans {
	
	private static testmethod void runRest(){	
		setData();
					
		Account_Plan_2__c bugPlan = new Account_Plan_2__c();
		bugPlan.Account__c = [Select Id from Account].Id;
		bugPlan.Account_Plan_Level__c = 'Business Unit Group';
		bugPlan.Business_Unit_Group__c = [Select Id from Business_Unit_Group__c].Id;
		insert bugPlan;
		
		Account_Plan_2__c sbuPlan = new Account_Plan_2__c();
		sbuPlan.Account__c = [Select Id from Account].Id;
		sbuPlan.Account_Plan_Level__c = 'Sub Business Unit';
		sbuPlan.Sub_Business_Unit__c = [Select Id from Sub_Business_Units__c].Id;
		sbuPlan.Business_Unit_Group__c = bugPlan.Business_Unit_Group__c;
		insert sbuPlan;
		
		ApexPages.StandardController sc = new ApexPages.StandardController(bugPlan);
		ctrlExt_BUG_Account_Plan_sBU_Plans controller = new ctrlExt_BUG_Account_Plan_sBU_Plans(sc);
		
		System.assert(controller.sbuAccountPlans.size() == 1);		
	}

	private static void setData() {
	
		Company__c cmpny = new Company__c();		
        cmpny.name='Europe';
        cmpny.CurrencyIsoCode = 'EUR';
        cmpny.Current_day_in_Q1__c=56;
        cmpny.Current_day_in_Q2__c=34; 
        cmpny.Current_day_in_Q3__c=5; 
        cmpny.Current_day_in_Q4__c= 0;   
        cmpny.Current_day_in_year__c =200;
        cmpny.Days_in_Q1__c=56;  
        cmpny.Days_in_Q2__c=34;
        cmpny.Days_in_Q3__c=13;
        cmpny.Days_in_Q4__c=22;
        cmpny.Days_in_year__c =250;
        cmpny.Company_Code_Text__c = 'T35';
		insert cmpny;
		
		Business_Unit_Group__c bug =  new Business_Unit_Group__c();
        bug.Master_Data__c =cmpny.id;
        bug.name='RTG';      
        insert bug;
		
		Business_Unit__c bu =  new Business_Unit__c();
        bu.Company__c =cmpny.id;
        bu.name='RTG BU';
        bu.Business_Unit_Group__c = bug.Id;
        bu.Account_Plan_Activities__c=true;        
        insert bu;
        
        Sub_Business_Units__c sbu = new Sub_Business_Units__c();
        sbu.name='RTG sBU';
        sbu.Business_Unit__c=bu.id;
		insert sbu;
					
		Account testAccount = new Account();
		testAccount.Name = 'RTG Test Account';
		insert testAccount;
	}
}