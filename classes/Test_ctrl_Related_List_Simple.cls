//--------------------------------------------------------------------------------------------------------------------
//  Description : This TEST APEX Class is used to test the logic in ctrl_Related_List_Simple
//  Version     : 1.0
//  Author      : Bart Caelen
//  Date        : 08/09/2016
//--------------------------------------------------------------------------------------------------------------------
@isTest private class Test_ctrl_Related_List_Simple {
	
    @testSetup static void createTestData() {

    	//---------------------------------------------------------------------------
    	// Create Test Data
    	//---------------------------------------------------------------------------
    	// Create Contact Data
    	clsTestData_Contact.iRecord_Contact = 100;
    	clsTestData_Contact.createContact();
    	//---------------------------------------------------------------------------

    }


	@isTest static void test_ctrl_Related_List_Simple() {

		ctrl_Related_List_Simple oCtrl_RelatedListSimple = new ctrl_Related_List_Simple();

		Test.startTest();

			oCtrl_RelatedListSimple.inputObjectName = 'Contact';
			oCtrl_RelatedListSimple.inputFields = 'Firstname,Lastname,Email,AccountId,Account_ID__c';
			oCtrl_RelatedListSimple.inputQueryFilter = 'WHERE Name != null';

			List<String> lstField = oCtrl_RelatedListSimple.fieldList;
			Map<String, String> mapFieldLabel = oCtrl_RelatedListSimple.FieldLabels;
			Map<String, String> mapFieldSort = oCtrl_RelatedListSimple.fieldSorts;
			List<String> lstFieldSearchable = oCtrl_RelatedListSimple.searchableFields;

			List<sObject> lstRecord = oCtrl_RelatedListSimple.getRecords();
			Boolean bHasNext = oCtrl_RelatedListSimple.hasNext;
			Boolean bHasPrevious = oCtrl_RelatedListSimple.hasPrevious;
			Integer iPageNumber = oCtrl_RelatedListSimple.pageNumber;
			Integer iTotalPages = oCtrl_RelatedListSimple.totalPages;
			Integer iPageSize = oCtrl_RelatedListSimple.pageSize;
			Integer iResultSize = oCtrl_RelatedListSimple.resultSize;

			String tSortColumn = oCtrl_RelatedListSimple.sortColumn;
			String tSortDir = oCtrl_RelatedListSimple.sortDir;
			String tSelectedColumn = oCtrl_RelatedListSimple.selectedColumn;

			// Actions
			oCtrl_RelatedListSimple.last();
			oCtrl_RelatedListSimple.previous();
			oCtrl_RelatedListSimple.next();
			oCtrl_RelatedListSimple.first();
			oCtrl_RelatedListSimple.sortData();
			oCtrl_RelatedListSimple.doSearch();

			oCtrl_RelatedListSimple.selectedColumn = 'Account_ID__c';
			oCtrl_RelatedListSimple.sortColumn = 'Account_ID__c';
			oCtrl_RelatedListSimple.sortDir = 'DESC';
			oCtrl_RelatedListSimple.sortData();

			oCtrl_RelatedListSimple.selectedColumn = 'Lastname';
			oCtrl_RelatedListSimple.sortColumn = 'Lastname';
			oCtrl_RelatedListSimple.searchString = 'Test';
			oCtrl_RelatedListSimple.doSearch();

			oCtrl_RelatedListSimple.pageSize = 5;
			oCtrl_RelatedListSimple.pageNumber = 2;

		Test.stopTest();

	}
	
}
//--------------------------------------------------------------------------------------------------------------------