@isTest
private class Test_ctrl_Account_Plan_Objective_Mobile {

	private static testmethod void BUG_AccountPlan_Objective(){
		
		setData();
		
		Account_Plan_2__c accPlan = new Account_Plan_2__c();
		accPlan.Account__c = [Select Id from Account].Id;
		accPlan.Account_Plan_Level__c = 'Business Unit Group';
		accPlan.Business_Unit_Group__c = [Select Id from Business_Unit_Group__c].Id;
		insert accPlan;
		
		Test.startTest();
						
		ApexPages.StandardController sc = new ApexPages.StandardController(accPlan);
		ctrl_Account_Plan_Objective_RTG_Mobile controller = new ctrl_Account_Plan_Objective_RTG_Mobile(sc);
		
		Account_Plan_Objective__c objective = controller.objective;
		
		System.assert(objective.RecordTypeId == [Select Id from RecordType where SObjectType = 'Account_Plan_Objective__c' AND DeveloperName = 'RTG'].Id);		
		System.assert(controller.businessObjectiveOptions.size() == 2);
		
		objective.Business_Objective__c = controller.businessObjectiveOptions[1].getValue();
		controller.businessObjectiveSelected();
		
		System.assert(controller.businessObjective != null);
		
		objective.Target_Date__c = Date.today().addDays(7);
		objective.Description__c = 'Test BUG Account Plan Objective';
		
		controller.upsertObjective();
		
		System.assert(controller.isSaveError == false);
		
		sc = new ApexPages.StandardController(objective);
		controller = new ctrl_Account_Plan_Objective_RTG_Mobile(sc);
		
		System.assert(controller.businessObjective != null);
	} 

	private static testmethod void BU_AccountPlan_Objective(){
		
		setData();
		
		Account_Plan_2__c accPlan = new Account_Plan_2__c();
		accPlan.Account__c = [Select Id from Account].Id;
		accPlan.Account_Plan_Level__c = 'Business Unit';
		accPlan.Business_Unit__c = [Select Id from Business_Unit__c].Id;
		insert accPlan;
		
		Test.startTest();
		
		ApexPages.StandardController sc = new ApexPages.StandardController(accPlan);
		ctrl_Account_Plan_Objective_RTG_Mobile controller = new ctrl_Account_Plan_Objective_RTG_Mobile(sc);
		
		Account_Plan_Objective__c objective = controller.objective;
		
		System.assert(objective.RecordTypeId == [Select Id from RecordType where SObjectType = 'Account_Plan_Objective__c' AND DeveloperName = 'RTG'].Id);
		System.assert(controller.businessObjectiveOptions.size() == 2);
				
		objective.Business_Objective__c = controller.businessObjectiveOptions[1].getValue();
		controller.businessObjectiveSelected();
		
		System.assert(controller.businessObjective != null);
		
		objective.Target_Date__c = Date.today().addDays(7);
		objective.Description__c = 'Test BU Account Plan Objective';
		
		controller.upsertObjective();
		
		System.assert(controller.isSaveError == false);
	} 
		
	private static testmethod void sBU_AccountPlan_Objective(){
		
		setData();
		
		Account_Plan_2__c accPlan = new Account_Plan_2__c();
		accPlan.Account__c = [Select Id from Account].Id;
		accPlan.Account_Plan_Level__c = 'Sub Business Unit';
		accPlan.Sub_Business_Unit__c = [Select Id from Sub_Business_Units__c].Id;
		insert accPlan;
		
		Test.startTest();
		
		ApexPages.StandardController sc = new ApexPages.StandardController(accPlan);
		ctrl_Account_Plan_Objective_RTG_Mobile controller = new ctrl_Account_Plan_Objective_RTG_Mobile(sc);
		
		Account_Plan_Objective__c objective = controller.objective;
		
		System.assert(objective.RecordTypeId == [Select Id from RecordType where SObjectType = 'Account_Plan_Objective__c' AND DeveloperName = 'RTG'].Id);
		System.assert(controller.businessObjectiveOptions.size() == 2);
				
		objective.Business_Objective__c = controller.businessObjectiveOptions[1].getValue();
		controller.businessObjectiveSelected();
		
		System.assert(controller.businessObjective != null);
		
		objective.Target_Date__c = Date.today().addDays(7);
		objective.Description__c = 'Test sBU Account Plan Objective';
		
		controller.upsertObjective();
		
		System.assert(controller.isSaveError == false);
	} 
	
	private static testmethod void AccountPlan_Objective_Errors(){
		
		setData();
		
		Account_Plan_2__c accPlan = new Account_Plan_2__c();
		accPlan.Account__c = [Select Id from Account].Id;
		accPlan.Account_Plan_Level__c = 'Sub Business Unit';
		accPlan.Sub_Business_Unit__c = [Select Id from Sub_Business_Units__c].Id;
		insert accPlan;
		
		Test.startTest();
		
		ApexPages.StandardController sc = new ApexPages.StandardController(accPlan);
		ctrl_Account_Plan_Objective_RTG_Mobile controller = new ctrl_Account_Plan_Objective_RTG_Mobile(sc);
		
		Account_Plan_Objective__c objective = controller.objective;
		
		controller.upsertObjective();	
		System.assert(controller.isSaveError == true);
		
		objective.Business_Objective__c = controller.businessObjectiveOptions[1].getValue();
		controller.businessObjectiveSelected();
		
		System.assert(controller.businessObjective != null);		
		controller.upsertObjective();	
		
		System.assert(controller.isSaveError == true);
		
		objective.Target_Date__c = Date.today().addDays(7);
		objective.Description__c = 'Test sBU Account Plan Objective';
		
		controller.upsertObjective();
		
		System.assert(controller.isSaveError == false);
						
		ApexPages.StandardController dupeSc = new ApexPages.StandardController(accPlan);
		ctrl_Account_Plan_Objective_RTG_Mobile dupeController = new ctrl_Account_Plan_Objective_RTG_Mobile(dupeSc);
		
		Account_Plan_Objective__c dupe = dupeController.objective;
		
		dupe.Business_Objective__c = objective.Business_Objective__c;		
		dupeController.businessObjectiveSelected();
		
		dupe.Target_Date__c = Date.today().addDays(7);
		dupe.Description__c = 'Test sBU Account Plan Objective';
		
		dupeController.upsertObjective();
		
		System.assert(dupeController.isSaveError == true);
	}		
	
	private static void setData() {
	
		Company__c cmpny = new Company__c();		
        cmpny.name='Europe';
        cmpny.CurrencyIsoCode = 'EUR';
        cmpny.Current_day_in_Q1__c=56;
        cmpny.Current_day_in_Q2__c=34; 
        cmpny.Current_day_in_Q3__c=5; 
        cmpny.Current_day_in_Q4__c= 0;   
        cmpny.Current_day_in_year__c =200;
        cmpny.Days_in_Q1__c=56;  
        cmpny.Days_in_Q2__c=34;
        cmpny.Days_in_Q3__c=13;
        cmpny.Days_in_Q4__c=22;
        cmpny.Days_in_year__c =250;
        cmpny.Company_Code_Text__c = 'T35';
		insert cmpny;
		
		Business_Unit_Group__c bug =  new Business_Unit_Group__c();
        bug.Master_Data__c =cmpny.id;
        bug.name='RTG';      
        insert bug;
		
		Business_Unit__c bu =  new Business_Unit__c();
        bu.Company__c =cmpny.id;
        bu.name='RTG BU';
        bu.Business_Unit_Group__c = bug.Id;
        bu.Account_Plan_Activities__c=true;        
        insert bu;
        
        Sub_Business_Units__c sbu = new Sub_Business_Units__c();
        sbu.name='RTG sBU';
        sbu.Business_Unit__c=bu.id;
		insert sbu;
		
		DIB_Country__c country = new DIB_Country__c();
		country.Company__c = cmpny.Id;
		country.Name = 'Unit Test Country';
		country.Country_ISO_Code__c = 'ZZ';
		country.CurrencyIsoCode = 'EUR';
		insert country;
		
		Business_Objective__c sbuObjective = new Business_Objective__c();
		sbuObjective.Name = 'SBU Objective';
		sbuObjective.Explanation__c = 'test explanation';
		sbuObjective.Type__c = 'Quantitative';
		sbuObjective.Active__c = true;
		sbuObjective.Sub_Business_Unit__c = sbu.Id;
		insert sbuObjective;
		
		Business_Objective__c buObjective = new Business_Objective__c();
		buObjective.Name = 'BU Objective';
		buObjective.Explanation__c = 'test explanation';
		buObjective.Type__c = 'Quantitative';
		buObjective.Active__c = true;
		buObjective.Business_Unit__c = bu.Id;
		insert buObjective;

		Business_Objective__c bugObjective = new Business_Objective__c();
		bugObjective.Name = 'BUG Objective';
		bugObjective.Explanation__c = 'test explanation';
		bugObjective.Type__c = 'Qualitative';
		bugObjective.Active__c = true;
		bugObjective.Business_Unit_Group__c = bug.Id;
		insert bugObjective;
				
		Account testAccount = new Account();
		testAccount.Name = 'RTG Test Account';
		insert testAccount;
	}
}