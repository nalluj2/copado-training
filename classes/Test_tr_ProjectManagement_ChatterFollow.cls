/**
 * Creation Date :  20140130
 * Description :    Test class for the tr_ProjectManagement_ChatterFollow trigger
 * Author :         Bluewolf - Paul Rice
 * Change Log:
 */
@isTest
private class Test_tr_ProjectManagement_ChatterFollow {
    static Integer NUM_OF_PROJECTS = 10;
    static Integer NUM_OF_USERS = 3;
    static User oUserAdmin;
    static User follower;
    static User secondFollower;
    static User manualFollower;
    static Project_Management__c newProject;
    static Project_Management__c finalProject;
    static List <Project_Management__c> projects = new List <Project_Management__c> ();
    static List<String> finalStatus = new List<String>();
    static Double conversionRate;
    static Map<String, User> mapOfUserNameToUser;

    static void datasetup() {

        // Create Test Users
        List<User> lstUser = new List<User>();
        lstUser.add(clsTestData_User.createUser_SystemAdministrator('tadm000', false));
        lstUser.add(clsTestData_User.createUser_SystemAdministrator('tadm001', false));
        lstUser.add(clsTestData_User.createUser_SystemAdministrator('tadm002', false));
        lstUser.add(clsTestData_User.createUser_SystemAdministrator('tadm003', false));
        insert lstUser;

        oUserAdmin = lstUser[0];
        follower = lstUser[1];
        secondFollower = lstUser[2];
        manualFollower = lstUser[3];

        System.runAs(oUserAdmin){

            List<String> values = new List<String>{'Convert to Change Request','Deployed','Project Abandoned'};
            
            List<Project_Final_Stage__c> finalStages = Test_TestDataUtil.createProjectFinalStage(values, true);
            for (Project_Final_Stage__c finalStage:finalStages){
                finalStatus.add(finalStage.stage__c);       
            }
            String randVal = Test_TestDataUtil.createRandVal();
            String atSuffix = '@salesforcetest.com';

            Time_Registration_Code__c oRegistrationCode = new Time_Registration_Code__c();
            oRegistrationCode.Name = 'Test Project';                    
            oRegistrationCode.Time_Registration_Category__c = 'Test Category';
            oRegistrationCode.Active__c = true;
            oRegistrationCode.Business_Unit__c = 'Test BU';
            oRegistrationCode.Time_Registration_ID__c = 'TRC';
            insert oRegistrationCode;
                    
            for (Integer i = 0; i <NUM_OF_PROJECTS; i++) {
                Project_Management__c projectInstance = new Project_Management__c();
                projectInstance.currencyIsoCode = 'USD';
                projectInstance.Stage__c = 'New';
                projectInstance.Requestor__c = follower.id;
                projectInstance.Time_Registration_Project__c = oRegistrationCode.Id;
                projects.add(projectInstance);
            }

        }
    }
    
    static testMethod void createChatterFollowProject() {

        // test the insert trigger
        datasetup();

        Test.startTest();

            System.runAs(oUserAdmin){
                insert projects;
            }

        Test.stopTest();

        System.runAs(oUserAdmin){

            List<Project_Management__c> projectResults = [Select id, Requestor__c from Project_Management__c where id = : projects];
            System.assertEquals(NUM_OF_PROJECTS, projectResults.size(), 'All projects have been inserted');
            List<EntitySubscription> subscribers = [Select id, subscriberId, parentId from EntitySubscription where parentId in: projectResults];
            Map<id,Set<id>> parentIdSubscriberIdMap = ProjectManagementFollowService.getParentSubscribersMap(subscribers);
            System.assertEquals(NUM_OF_PROJECTS, subscribers.size(), 'All projects have a subscriber');      
            for (Project_Management__c projectResult : projectResults   ){
                Set<id> subscriberUpdates = parentIdSubscriberIdMap.get(projectResult.id);
                System.assertEquals(1,subscriberUpdates.size(),'There is one subscriber');
                System.assert(subscriberUpdates.contains(projectResult.requestor__c),'The project requestor is subscribed to its chatter feed');
            }

        }

    }
    

    static testMethod void updateChatterFollower() {
        // test the update trigger
        datasetup(); 

        System.runAs(oUserAdmin){

            insert projects;
            for (Project_Management__c project : projects   ){
                project.Requestor__c = secondFollower.id;
            }

        }

        Test.startTest();

            System.runAs(oUserAdmin){
                update projects;
            }

        Test.stopTest();

        System.runAs(oUserAdmin){
    
            List<Project_Management__c> projectResults = [Select id,requestor__c from Project_Management__c where id = : projects ];
            System.assertEquals(NUM_OF_PROJECTS, projectResults.size(), 'All projects have been inserted');
            List<EntitySubscription> subscribers = [Select id, subscriberId, parentId from EntitySubscription where parentId in: projectResults];
            Map<id,Set<id>> parentIdSubscriberIdMap = ProjectManagementFollowService.getParentSubscribersMap(subscribers);

            List<EntitySubscription> subscribers2 = [Select id, subscriberId, parentId from EntitySubscription];

            System.assertEquals(NUM_OF_PROJECTS, subscribers.size(), 'All projects have a subscriber');
          
            for (Project_Management__c projectResult : projectResults   ){
                System.assert(parentIdSubscriberIdMap.get(projectResult.id).contains(projectResult.requestor__c),'The project requestor is subscribed to its chatter feed');
            }

        }

    }    

    // test that manually entered chatter followers are not affected by the trigger
    static testMethod void updateAutomatedChatterFollowersOnly() {
 
        datasetup(); 

        System.runAs(oUserAdmin){

            insert projects;

            List<ID> projectIds = new List<ID>();       
            for (Project_Management__c project : projects   ){
                project.Requestor__c = secondFollower.id;
                projectIds.add(project.id);
            }
            // manually enter a project chatter follower
            Test_TestDataUtil.createEntitySubscription(NUM_OF_PROJECTS, manualFollower.id, projectIds, true);

        }

        Test.startTest();

            System.runAs(oUserAdmin){
                update projects;
            }

        Test.stopTest();
        
        System.runAs(oUserAdmin){

            List <Project_Management__c> projectResults = [Select id,requestor__c from Project_Management__c where id = : projects ];
            System.assertEquals(NUM_OF_PROJECTS, projectResults.size(), 'All projects have been inserted');
            
            List<EntitySubscription> subscribers = [Select id, subscriberId, parentId from EntitySubscription where parentId in: projectResults];
            Map<id,Set<id>> parentIdSubscriberIdMap = ProjectManagementFollowService.getParentSubscribersMap(subscribers);
            
            System.assertEquals(NUM_OF_PROJECTS*2, subscribers.size(), 'All projects have the projector requestor and manual chatter subscriber');
          
            for (Project_Management__c projectResult : projectResults   ){
                System.assert(parentIdSubscriberIdMap.get(projectResult.id).contains(projectResult.requestor__c),'The new project requestor is subscribed to the project chatter feed');
                System.assert(parentIdSubscriberIdMap.get(projectResult.id).contains(manualFollower.id),'The manually entered chatter follower is subscribed to the project chatter feed');
                System.assert(!parentIdSubscriberIdMap.get(projectResult.id).contains(follower.id),'The old project requestor is not subscribed to the project chatter feed');
            }

        }

    }

    static testMethod void updateChatterStatus_final() {
        // test the insert trigger
        datasetup();
 
        System.runAs(oUserAdmin){

            insert projects;
            for (Project_Management__c project : projects   ){
                project.Stage__c = finalStatus[0];
                project.Abandon_Reason__c = 'cancelled';
            }

        }

        Test.startTest();

            System.runAs(oUserAdmin){
                update projects;
            }
        
        Test.stopTest();

        System.runAs(oUserAdmin){

            List <Project_Management__c> projectResults = [Select id,requestor__c from Project_Management__c where id = : projects];
            System.assertEquals(NUM_OF_PROJECTS, projectResults.size(), 'All projects have been inserted');
            List<EntitySubscription> subscribers = [Select id, subscriberId, parentId from EntitySubscription 
                                                    where parentId in: projectResults];
            System.assertEquals(0, subscribers.size(), 'All chatter subscriptions have been deleted');      

        }
        
    }

    // 
    static testMethod void updateChatterStatus_final_manualFollowersRetained() {
        // test the insert trigger
        datasetup();
 
        System.runAs(oUserAdmin){

            insert projects;
            List<ID> projectIds = new List<ID>();       
            
            for (Project_Management__c project : projects   ){
                project.Stage__c = finalStatus[2];
                project.Abandon_Reason__c = 'cancelled';
                projectIds.add(project.id);
            }
            // manually enter a project chatter follower
            Test_TestDataUtil.createEntitySubscription(NUM_OF_PROJECTS, manualFollower.id, projectIds, true);
        }

        Test.startTest();

            System.runAs(oUserAdmin){
                update projects;
            }

        Test.stopTest();

        System.runAs(oUserAdmin){

            List <Project_Management__c> projectResults = [Select id,requestor__c from Project_Management__c where id = : projects];
            System.assertEquals(NUM_OF_PROJECTS, projectResults.size(), 'All projects have been inserted');
            List<EntitySubscription> subscribers = [Select id, subscriberId, parentId from EntitySubscription 
                                                    where parentId in: projectResults];
            System.assertEquals(NUM_OF_PROJECTS, subscribers.size(), 'The project requestor subscriptions have been deleted and manual follower remain for each project');
            Map<id,Set<id>> parentIdSubscriberIdsMap = ProjectManagementFollowService.getParentSubscribersMap(subscribers);
                  
            for (Project_Management__c projectResult : projectResults   ){
                System.assertEquals(1,parentIdSubscriberIdsMap.get(projectResult.id).size(),'The manually entered chatter follower is subscribed to the project chatter feed');         
                System.assert(parentIdSubscriberIdsMap.get(projectResult.id).contains(manualFollower.id),'The manually entered chatter follower is subscribed to the project chatter feed');
            }

        }
        
    }

}