/*
 * Author 		:	Bart Caelen
 * Date			:	20140210
 * Description	:	CR-3663 - This batch/scheduled Class will collect all Call Records that need processing and perform the needed actions
 *						 count the distinct related Accounts & Contacts for a Call Record
 */
global class ba_CallRecord implements Schedulable,Database.Batchable<sObject> {

	global void execute(SchedulableContext sc){
    	ba_CallRecord oBatch = new ba_CallRecord();
        Database.executebatch(oBatch, 200);        	
    } 

    global database.querylocator start(Database.BatchableContext bc){
        string tQuery = 'SELECT Id FROM Call_Records__c WHERE Process_Batch__c = true';
    	return Database.getQueryLocator(tQuery);
    }

    global void execute(Database.BatchableContext bc, List<Call_Records__c> lstCallRecord){
    	
		// Get a SET of ID's from the passed list of records to process the data
		map<id, Call_Records__c> mapCallRecord = new map<id, Call_Records__c>();
		mapCallRecord.putAll(lstCallRecord); 
    	bl_CallRecord.updateAttendedAccountContact(mapCallRecord.keySet());

    }
    
    global void finish(Database.BatchableContext bc){
    }

}