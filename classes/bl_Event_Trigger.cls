//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 04-05-2016
//  Description      : APEX Class - Business Logic for tr_Event
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public with sharing class bl_Event_Trigger {

    @Testvisible private static Set<String> processedMobileIds = new Set<String>();
    
    public static void populateMobileId(List<Event> triggerNew){
    	
    	for(Event evnt : triggerNew){
    		
    		//In case a recurring series is edited the occurrences get the same mobile id at creation, to avoid this we nullify the mobile id 
    		if(evnt.mobile_id__c != null && processedMobileIds.contains(evnt.mobile_id__c)) evnt.mobile_id__c = null;    		 		    		
    	}
    	
    	GuidUtil.populateMobileId(triggerNew, 'Mobile_ID__c'); 
    	
    	for(Event evnt : triggerNew){
    		
    		processedMobileIds.add(evnt.mobile_id__c);   
    	}   	
    }
    
    public static Boolean runningActivitySchedulingEventSync = false;
    
    public static Id activityScheduling_RT = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get('Activity_Scheduling').getRecordTypeId();
    
    public static void activitySchedulingBlockOutsideApps(List<Event> triggerNew){
    	
    	for(Event record : triggerNew){
    			
    		if(record.RecordTypeId == activityScheduling_RT && ProfileMedtronic.isSystemAdministratorProfile(UserInfo.getProfileId()) == false){
    		    			
    			if(bl_Case_Trigger.runningSchedulingApp == false){
    			
    				record.addError('This type of Events can only be managed in the Activity Scheduling apps');
    				
    			}else if(record.WhatId != null && runningActivitySchedulingEventSync == false){
    				
    				record.addError('This type of Events can only be managed throw their attached Case in Activity Scheduling apps');
    			}
    		}    		
    	}  	
    }
}
//--------------------------------------------------------------------------------------------------------------------