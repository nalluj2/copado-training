//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 13-06-2016
//  Description      : TEST Class for APEX Controller Extension ctrlExt_LeadConversion and VF Page LeadConversion (MITG)
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
@isTest 
private class TEST_ctrlExt_LeadConversion {

	private static String tJSON_Lead = '';
	private static String tJSON_Account = '';
	private static String tJSON_Contact = '';
	private static String tJSON_Opportunity = '';
	private static String tJSON_Product = '';
	private static List<Pricebook2> lstPricebook;
	private static List<PricebookEntry> lstPricebookEntry;
	private static String tCountry = 'BE';
	private static String tCurrencyISOCode = 'EUR';

	private static User oUser_SystemAdmin;

	//---------------------------------------------------------------------------------
	// Load / Create Test Data
	//---------------------------------------------------------------------------------
	@isTest static void createTestData() {

		// Create Master Data - Company + Country
		clsTestData_MasterData.tCompanyCode = 'EUR';
		clsTestData_MasterData.createCompany();
		clsTestData_MasterData.createCountry();


		// Instantiate the Pricebook2 record first, setting the Id
		Pricebook2 oPricebook_Std = new Pricebook2(Id = Test.getStandardPricebookId(), IsActive = true);
		update oPricebook_Std;

		// Create the Product
		Product2 oProduct = new Product2(Name = 'Test Product', IsActive = true);
		insert oProduct;

		// Create the PricebookEntry
		PricebookEntry oPricebookEntry = new PricebookEntry(Pricebook2Id = oPricebook_Std.Id, Product2Id = oProduct.Id, UnitPrice = 100, IsActive = true, CurrencyISOCode = tCurrencyISOCode);
		insert oPricebookEntry;

		lstPricebook = new List<Pricebook2>();
		lstPricebook.add(oPricebook_Std);
		lstPricebookEntry = new List<PricebookEntry>();
		lstPricebookEntry.add(oPricebookEntry);

		// Create Lead Data
		clsTestData.idRecordType_Lead = clsUtil.getRecordTypeByDevName('Lead', 'MITG_Lead').Id;
		clsTestData.iRecord_Lead = 1;
		clsTestData.createLeadData(false);
			clsTestData.oMain_Lead.Phone = '000 11 22 33';
			clsTestData.oMain_Lead.Street = 'Lead Street';
			clsTestData.oMain_Lead.PostalCode = '1000';
			clsTestData.oMain_Lead.City = 'Brussel';
			clsTestData.oMain_Lead.State = 'Brussel';
			clsTestData.oMain_Lead.Country = tCountry;
			clsTestData.oMain_Lead.Salutation = 'Dr.';
			clsTestData.oMain_Lead.FirstName = 'Lead FirstName';
			clsTestData.oMain_Lead.LastName = 'Lead LastName';
			clsTestData.oMain_Lead.Email = 'test@medtronic.com';
			clsTestData.oMain_Lead.Department_Picklist__c = 'Administration';
			clsTestData.oMain_Lead.Connected_As__c = 'Manager'; 
			clsTestData.oMain_Lead.Title = 'Lead Title';
			clsTestData.oMain_Lead.CurrencyISOCode = tCurrencyISOCode;
		insert clsTestData.oMain_Lead;

		// Create Account Data
		clsTestData.idRecordType_Account = clsUtil.getRecordTypeByDevName('Account', 'NON_SAP_Account').Id;
		clsTestData.iRecord_Account = 1;
		clsTestData.createAccountData(false);
			clsTestData.oMain_Account.Name = clsTestData.oMain_Lead.Company;
			clsTestData.oMain_Account.BillingStreet = clsTestData.oMain_Lead.Street;
			clsTestData.oMain_Account.BillingPostalCode = clsTestData.oMain_Lead.PostalCode;
			clsTestData.oMain_Account.BillingCity = clsTestData.oMain_Lead.City;
			clsTestData.oMain_Account.BillingCountry = clsTestData.oMain_Lead.Country;
			clsTestData.oMain_Account.BillingState = clsTestData.oMain_Lead.State;
			clsTestData.oMain_Account.Phone = clsTestData.oMain_Lead.Phone;
			clsTestData.oMain_Account.CurrencyISOCode = tCurrencyISOCode;
		insert clsTestData.oMain_Account;

		// Create Contact Data
		clsTestData.idRecordType_Contact = clsUtil.getRecordTypeByDevName('Contact', 'Generic_Contact').Id; 
		clsTestData.iRecord_Contact = 1;
		clsTestData.createContactData(false);
		for (Contact oContact : clsTestData.lstContact){
        	oContact.AccountId = clsTestData.oMain_Account.Id;
			oContact.Salutation_Text__c = clsTestData.oMain_Lead.Salutation;
			oContact.FirstName = clsTestData.oMain_Lead.FirstName;
			oContact.LastName = clsTestData.oMain_Lead.LastName;
			oContact.Phone = clsTestData.oMain_Lead.Phone;
			oContact.Email = clsTestData.oMain_Lead.Email;
			oContact.Contact_Department__c = clsTestData.oMain_Lead.Department_Picklist__c;
			oContact.Affiliation_To_Account__c = 'Employee';
			oContact.Title = clsTestData.oMain_Lead.Title;
			oContact.Contact_Gender__c = 'Male';
			oContact.Contact_Primary_Specialty__c = 'Administration';
			oContact.Primary_Job_Title_vs__c = 'Administrator';
			oContact.CurrencyISOCode = tCurrencyISOCode;
		}
		insert clsTestData.lstContact;

		// Create Opportunity Data
		clsTestData.iRecord_Opportunity = 1;
		clsTestData.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'EUR_MITG_Standard_Opportunity').Id; 

		clsTestData.createOpportunityData(false);
			clsTestData.oMain_Opportunity.CurrencyIsoCode = tCurrencyISOCode;
			clsTestData.oMain_Opportunity.StageName = 'Identify';
        	clsTestData.oMain_Opportunity.CloseDate = Date.today().addDays(60);
        	clsTestData.oMain_Opportunity.Pricebook2Id = lstPricebook[0].Id;
        	clsTestData.oMain_Opportunity.AccountId = clsTestData.oMain_Account.Id;
			clsTestData.oMain_Opportunity.CurrencyISOCode = tCurrencyISOCode;
			clsTestData.oMain_Opportunity.Initiative__c = 'Hernia';
        insert clsTestData.oMain_Opportunity;

		// Create Note Data
		clsTestData.idNote_Parent = clsTestData.oMain_Lead.Id;
		clsTestData.iRecord_Note = 1;
		clsTestData.createNoteData(true);

		// Create Attachment Data
		clsTestData.idAttachment_Parent = clsTestData.oMain_Lead.Id;
		clsTestData.iRecord_Attachment = 1;
		clsTestData.createAttachmentData();
        
        
        EMEA_MITG_Opportunites__c recEMEAMITGOpportunites = new EMEA_MITG_Opportunites__c();
        recEMEAMITGOpportunites.Name = 'EMEA MITG Opportunities';
        recEMEAMITGOpportunites.MITG_EMEA_Available__c = false;
        insert recEMEAMITGOpportunites;
	}
	//---------------------------------------------------------------------------------


	//---------------------------------------------------------------------------------
	// TEST - EXISTING / NEW RECORDS - EUR
	//---------------------------------------------------------------------------------
	@isTest static void test_ctrlExt_LeadConversion_EU_NewRecords(){

		User oUser_SystemAdmin = clsTestData_User.createUser_SystemAdministratorMDT('tstadm1', true);

		System.runAs(oUser_SystemAdmin){
			test_ctrlExt_LeadConversion('BE', false, 'EUR', false);
		}

	}

	@isTest static void test_ctrlExt_LeadConversion_EU_ExistingRecords(){

		User oUser_SystemAdmin = clsTestData_User.createUser_SystemAdministratorMDT('tstadm1', true);

		System.runAs(oUser_SystemAdmin){
			test_ctrlExt_LeadConversion('BE', true, 'EUR', false);
		}

	}

	@isTest static void test_ctrlExt_LeadConversion_EU_NewRecords_Error(){

		User oUser_SystemAdmin = clsTestData_User.createUser_SystemAdministratorMDT('tstadm1', true);

		System.runAs(oUser_SystemAdmin){
			test_ctrlExt_LeadConversion('BE', false, 'EUR', true);
		}

	}
	//---------------------------------------------------------------------------------


	//---------------------------------------------------------------------------------
	// Private methods which executes the tests
	//---------------------------------------------------------------------------------
	private static void test_ctrlExt_LeadConversion(String atCountry, Boolean bUseExistingRecords, String atCurrencyISOCode, Boolean bHasException){

		// CREATE & LOAD TEST DATA
		tCountry = atCountry;
		tCurrencyISOCode = atCurrencyISOCode;
		createTestData();

		Test.startTest();


        PageReference oPageRef = Page.LeadConversion;
			ApexPages.currentPage().getParameters().put('id', clsTestData.oMain_Lead.Id);
        Test.setCurrentPageReference(oPageRef);
		
		ApexPages.StandardController oCTRLStd_Error = new ApexPages.StandardController(clsTestData.oMain_Account);
		ctrlExt_LeadConversion oCTRLExt_Error = new ctrlExt_LeadConversion(oCTRLStd_Error);

		ApexPages.StandardController oCTRLStd = new ApexPages.StandardController(clsTestData.oMain_Lead);
		ctrlExt_LeadConversion oCTRLExt = new ctrlExt_LeadConversion(oCTRLStd);

		oCTRLExt.oAccount = clsTestData.oMain_Account;
		oCTRLExt.oContact = clsTestData.oMain_Contact;
		oCTRLExt.oOpportunity = clsTestData.oMain_Opportunity;

		List<SelectOption> lstSO = oCTRLExt.lstOpportunityProductGroup;
		List<Schema.FieldSetMember> lstFSM_Account = oCTRLExt.lstFieldSet_Account;
		List<Schema.FieldSetMember> lstFSM_Contact = oCTRLExt.lstFieldSet_Contact;
		List<Schema.FieldSetMember> lstFSM_Opportunity = oCTRLExt.lstFieldSet_Opportunity;
		String tTest = oCTRLExt.tErrorMessage;
		List<ctrlExt_LeadConversion.Product> lstProduct = oCTRLExt.lstProduct;
		oCTRLExt.idOpportunityProductGroup = lstPricebook[0].Id;
		oCTRLExt.fetchProducts();

		Boolean bTest = oCTRLExt.bNewAccountAllowed;		
		bTest = oCTRLExt.bNewContactAllowed;		
		bTest = oCTRLExt.bNewOpportunityAllowed;		


		// Execute Convert
        Boolean bAddOpportunity = true;

        setJSONVariables(bUseExistingRecords);
		clsUtil.hasException = bHasException;
		ctrlExt_LeadConversion.Response oResponse = ctrlExt_LeadConversion.tryConvert(tJSON_Lead, tJSON_Account, tJSON_Contact, tJSON_Opportunity, tJSON_Product, bAddOpportunity, tCurrencyISOCode);
		if (bHasException == false){
			System.assertEquals(oResponse.message, null);
		}else{
			System.assertEquals(oResponse.success, false);
		}

		Test.stopTest();

	}
	//---------------------------------------------------------------------------------


	//---------------------------------------------------------------------------------
	// Prepare JSON variables to pass to the Remote Action in the Controller
	//---------------------------------------------------------------------------------
	private static void setJSONVariables(Boolean bUseExistingRecords){

		tJSON_Lead = '{';
			tJSON_Lead += '"Id":"' + clsTestData.oMain_Lead.Id + '"';
			tJSON_Lead += ',"FirstName":"' + clsTestData.oMain_Lead.FirstName + '"';
			tJSON_Lead += ',"LastName":"' + clsTestData.oMain_Lead.LastName + '"';
			tJSON_Lead += ',"RecordTypeId":"' + clsTestData.oMain_Lead.RecordTypeId + '"';
			tJSON_Lead += ',"OwnerId":"' + clsTestData.oMain_Lead.OwnerId + '"';
			tJSON_Lead += ',"Phone":"' + clsTestData.oMain_Lead.Phone + '"';
			tJSON_Lead += ',"Email":"' + clsTestData.oMain_Lead.Email + '"';
			tJSON_Lead += ',"Title":"' + clsTestData.oMain_Lead.Title + '"';
			tJSON_Lead += ',"CurrencyIsoCode":"' + clsTestData.oMain_Lead.CurrencyIsoCode + '"';
			tJSON_Lead += ',"Description":"' + clsTestData.oMain_Lead.Description + '"';
			tJSON_Lead += ',"Company":"' + clsTestData.oMain_Lead.Company + '"';
			tJSON_Lead += ',"Street":"' + clsTestData.oMain_Lead.Street + '"';
			tJSON_Lead += ',"City":"' + clsTestData.oMain_Lead.City + '"';
			tJSON_Lead += ',"State":"' + clsTestData.oMain_Lead.State + '"';
			tJSON_Lead += ',"PostalCode":"' + clsTestData.oMain_Lead.PostalCode + '"';
			tJSON_Lead += ',"Country":"' + clsTestData.oMain_Lead.Country + '"';
			tJSON_Lead += ',"Department_Picklist__c":"' + clsTestData.oMain_Lead.Department_Picklist__c + '"';
			tJSON_Lead += ',"Connected_As__c":"' + clsTestData.oMain_Lead.Connected_As__c + '"';
			tJSON_Lead += ',"Opportunity_Product_Group__c":"' + clsTestData.oMain_Lead.Opportunity_Product_Group__c + '"';
			tJSON_Lead += ',"Lead_Comments__c":"' + clsTestData.oMain_Lead.Lead_Comments__c + '"';
			if (bUseExistingRecords){
				tJSON_Lead += ',"Lead_Convert_Account__c":"' + clsTestData.oMain_Account.Id + '"';
				tJSON_Lead += ',"Lead_Convert_Contact__c":"' + clsTestData.oMain_Contact.Id + '"';
				tJSON_Lead += ',"Lead_Convert_Opportunity__c":"' + clsTestData.oMain_Opportunity.Id + '"';
			}
		tJSON_Lead += '}';

        tJSON_Account = '{';
        	tJSON_Account += '"Name":"' + clsTestData.oMain_Lead.Company + '"';
        	tJSON_Account += ',"Phone":"' + clsTestData.oMain_Lead.Phone + '"';
        	tJSON_Account += ',"BillingStreet":"' + clsTestData.oMain_Lead.Street + '"';
        	tJSON_Account += ',"BillingPostalCode":"' + clsTestData.oMain_Lead.PostalCode + '"';
        	tJSON_Account += ',"BillingCity":"' + clsTestData.oMain_Lead.City + '"';
        	tJSON_Account += ',"BillingState":"' + clsTestData.oMain_Lead.State + '"';
        	tJSON_Account += ',"BillingCountry":"' + clsTestData.oMain_Lead.Country + '"';
        tJSON_Account += '}';

        tJSON_Contact = '{';
        	tJSON_Contact += '"FirstName":"' + clsTestData.oMain_Lead.FirstName + '"';
        	tJSON_Contact += ',"LastName":"' + clsTestData.oMain_Lead.LastName + '"';
        	tJSON_Contact += ',"Contact_Gender__c":"Male"';
        	tJSON_Contact += ',"Contact_Primary_Specialty__c":"Administration"';
        	tJSON_Contact += ',"Contact_Department__c":"Administration"';
        	tJSON_Contact += ',"Affiliation_To_Account__c":"Employee"';
        	tJSON_Contact += ',"Primary_Job_Title_vs__c":"Administrator"';
    	tJSON_Contact += '}';

        tJSON_Opportunity = '{';
        	tJSON_Opportunity += '"Name":"TEST Opportunity"';
        	tJSON_Opportunity += ',"StageName":"Identify"';
        	tJSON_Opportunity += ',"CloseDate":"' + String.valueOf(Date.today().Year()) + '-12-31"';
        	tJSON_Opportunity += ',"Pricebook2Id":"' + lstPricebook[0].Id + '"';
        	tJSON_Opportunity += ',"AccountId":"' + clsTestData.oMain_Account.Id + '"';
        	tJSON_Opportunity += ',"Initiative__c":"Hernia"';
		tJSON_Opportunity += '}';


		tJSON_Product = '[';
		if (lstPricebookEntry.size() > 0){
			for (PricebookEntry oPricebookEntry : lstPricebookEntry){
				tJSON_Product += '{';
					tJSON_Product += '"Id":"' + oPricebookEntry.Id + '"';
					tJSON_Product += ',"Name":"' + oPricebookEntry.Name + ' (' + oPricebookEntry.CurrencyISOCode + ')"';
					tJSON_Product += ',"Quantity":"1"';
					tJSON_Product += ',"UnitPrice":"100"';
					tJSON_Product += ',"CurrencyISOCode":"' + oPricebookEntry.CurrencyISOCode + '"';
					tJSON_Product += ',"Description":""';
				tJSON_Product += '},';
			}
			tJSON_Product = tJSON_Product.left(tJSON_Product.length() - 1);
		}
		tJSON_Product += ']';
		clsUtil.debug('tJSON_Product : ' + tJSON_Product);		
	}	
	//---------------------------------------------------------------------------------
	
}