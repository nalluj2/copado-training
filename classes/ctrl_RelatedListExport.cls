public with sharing class ctrl_RelatedListExport {

    //------------------------------------------------------------------------------------------------------------------------------------
    // PRIVATE VARIABLES
    //------------------------------------------------------------------------------------------------------------------------------------
    private transient List<sObject> lstData = new List<sObject>();
    private transient String tSOQL = '';
    //------------------------------------------------------------------------------------------------------------------------------------


    //------------------------------------------------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //------------------------------------------------------------------------------------------------------------------------------------
	public ctrl_RelatedListExport() {

		// Get URL Parameters and Decrypt the URL Parameter
		String tURLParameter = ApexPages.currentPage().getParameters().get('p');
		if (!clsUtil.isBlank(tURLParameter)){
			bInitializePage(tURLParameter);
		}else{
			// Show Error
			 ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Invalid parameters (missing)'));
		}

	}
    //------------------------------------------------------------------------------------------------------------------------------------


    //------------------------------------------------------------------------------------------------------------------------------------
    // GETTER /  SETTER
    //------------------------------------------------------------------------------------------------------------------------------------
    public List<SObject> getRecords(){

    	return loadData();

    }

    public String tContentType { get; private set; }
    public List<String> lstFieldName { get; private set;}
    public Map<String, String> mapFieldLabel { get; private set;}
    public String tRenderAs { get; private set; }
    //------------------------------------------------------------------------------------------------------------------------------------


    //------------------------------------------------------------------------------------------------------------------------------------
    // PRIVATE METHODS
    //------------------------------------------------------------------------------------------------------------------------------------
    @TestVisible private List<sObject> loadData(){

        lstData = new List<sObject>();

        try{

			clsUtil.bubbleException();

        	if (!clsUtil.isBlank(tSOQL)){

	            // Execute SOQL
	            lstData = Database.query(tSOQL);

        	}else{
	            clsUtil.createVFError('Error in loadData - no SOQL available.'); 
        	}

        }catch(Exception oEX){
            clsUtil.createVFError(oEX, 'loadData'); 
        }
        
        return lstData;               

    }     

	@TestVisible private Boolean bInitializePage(String tURLParameter){

		Boolean bResult = true;

		try{

			clsUtil.bubbleException();

			String tBusinessUnitId;
			String tSObjectName;
			String tExportType;

			tURLParameter = EncodingUtil.base64Decode(tURLParameter).toString();
			
			if (tURLParameter!=null){
				// Split the URL Parameter on '&' to obtain the different encrypted parameters - decrypt all parameters and assign them to the correct variable			
				for (String tValue : tURLParameter.split('&')){
					
					List<String> lstPair = tValue.split('=');
					
					if (lstPair.size() == 2){
						if (lstPair[0] == 'soql'){
							tSOQL = EncodingUtil.base64Decode(lstPair[1]).toString();
						}else if (lstPair[0] == 'buid'){
							tBusinessUnitId = EncodingUtil.base64Decode(lstPair[1]).toString();	
						}else if (lstPair[0] == 'oname'){
							tSObjectName = EncodingUtil.base64Decode(lstPair[1]).toString();
						}else if(lstPair[0] == 'exporttype'){
							tExportType = EncodingUtil.base64Decode(lstPair[1]).toString();
						}
					}				 
				}
			}	

			if ( 
				(!clsUtil.isBlank(tSObjectName)) 
				&& (!clsUtil.isBlank(tBusinessUnitId))
			){

				Map<String, String> mapField_Additional = new Map<String, String>();

				List<BU_Related_List_Setup__c> lstBURelatedListSetup = 
					[
						SELECT
							Field_Name__c, Field_Name_Export__c, Label__c
						FROM 
							BU_Related_List_Setup__c
						WHERE 
							sObject_Name__c = :tSObjectName
							AND Business_Unit__c = :tBusinessUnitId
							AND Visible__c = true
						ORDER BY
							Field_Sequence__c
					];

				mapFieldLabel = new Map<String, String>();
				lstFieldName = new List<String>();
				for (BU_Related_List_Setup__c oBURelatedListSetup : lstBURelatedListSetup){
					String tFieldName = oBURelatedListSetup.Field_Name__c;
					if (!clsUtil.isBlank(oBURelatedListSetup.Field_Name_Export__c)){
						tFieldName = oBURelatedListSetup.Field_Name_Export__c;
						mapField_Additional.put(oBURelatedListSetup.Field_Name_Export__c, oBURelatedListSetup.Field_Name_Export__c);
					}

					lstFieldName.add(tFieldName);
					mapFieldLabel.put(tFieldName, oBURelatedListSetup.Label__c);
				}

				// Add Field_Name_Export__c fields to the SOQL
				String tSOQL_SELECT = tSOQL.left(tSOQL.indexOf('FROM') - 1);
				String tSOQL_FROM = tSOQL.mid(tSOQL.indexOf('FROM') - 1, tSOQL.length());

				for (String tFieldName_Additional : mapField_Additional.values()){
				    tSOQL_SELECT += ', ' + tFieldName_Additional;
				}
				tSOQL = tSOQL_SELECT + tSOQL_FROM;


				tRenderAs = '';
				if (tExportType.toUpperCase() == 'XLS'){
					tContentType = 'application/vnd.ms-excel#export.xls';
				}else if (tExportType.toUpperCase() == 'XLSX'){
					tContentType = 'application/vnd.ms-excel#export.xlsx';
				}else if (tExportType.toUpperCase() == 'CSV'){
					tContentType = 'text/csv#export.csv';
				}else if (tExportType.toUpperCase() == 'PDF'){
					tContentType = 'application/pdf#export.pdf';
					tRenderAs = 'PDF';
				}else{
					tContentType = '';
					tRenderAs = '';
				}
			}
		
		}catch(Exception oEX){
			// Show Error
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error occured : ' + oEX.getMessage()));
			bResult = false;			
		}

		return bResult;
	}
	//------------------------------------------------------------------------------------------------------------------------------------

}