public with sharing class ctrl_sf1Lookup {
	
	public String objectType {get; set;}
	public SObject recordInstance {get; set;}
	public String fieldName {get; set;}
	public String filter { get;set; }	//-BC - 20150616 - CR-5767
	
	private String inputName; 
	
	public String getInputName(){
		
		if(this.inputName == null && recordInstance.get(fieldName) !=null){
			SObject targetRecord = Database.query('Select Name from '+objectType+' where Id=\''+recordInstance.get(fieldName)+'\'');
			this.inputName = (String) targetRecord.get('Name');
		}
		
		return this.inputName;
	}
	
	public void setInputName(String value){
		this.inputName = value;
	}
	
	public List<SObject> getRecentList(){
		
		List<SObject> recents = [SELECT Id FROM RecentlyViewed WHERE Type=:objectType ORDER BY LastViewedDate DESC];
		
		Set<Id> recentIds = new Set<Id>();
		for(SObject recent : recents) recentIds.add(recent.Id);
		
		//-BC - 20150616 - CR-5767 - START
		String tFilter_Additional = clsUtil.isNull(filter, '');
		Map<Id, SObject> recentMap;
		String tSOQL = '';
		if (tFilter_Additional != ''){
			tFilter_Additional = tFilter_Additional.replace('WHERE', 'AND').replace('\\', '');
			tSOQL = 'Select Id, Name from '+objectType+' where Id IN:recentIds ' + tFilter_Additional;
		}else{
			tSOQL = 'Select Id, Name from '+objectType+' where Id IN:recentIds';
		}
		recentMap = new Map<Id, SObject>(Database.query(tSOQL));
//		Map<Id, SObject> recentMap = new Map<Id, SObject>(Database.query('Select Id, Name from '+objectType+' where Id IN:recentIds and ' + filter));
		//-BC - 20150616 - CR-5767 - STOP

		List<SObject> recentRecords = new List<SObject>();
		
		for(Sobject recent : recents){
			
			SObject rec = recentMap.get(recent.Id);
			recentRecords.add(rec);			
		} 
		
		return recentRecords;		
	}
	
	@RemoteAction
	public static List<SObject> searchObjects(String searchCriteria, String sobjectType, String filter){
		
		String escapedInput = String.escapeSingleQuotes(searchCriteria);
		
		if(escapedInput.endsWith('*')==false) escapedInput+='*';
		
		String searchquery='FIND \''+escapedInput+'\'IN NAME FIELDS RETURNING '+sobjectType+'(Id, Name '+filter+' ORDER BY Name ASC)';
				 
		List<List<SObject>> searchList = search.query(searchquery);
		
		List<SObject> searchResult = searchList[0];
		
		return searchResult;		
	}
}