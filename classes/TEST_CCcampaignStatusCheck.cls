//@isTest(seealldata=true)
@isTest private class TEST_CCcampaignStatusCheck{

    @isTest static void test_CCcampaignStatusCheck_AccountsApproved(){

        //--------------------------------------------------------
        // Create Test Data
        //--------------------------------------------------------
        // Create Campaign Data
        clsTestData_Campaign.iRecord_Campaign = 1;
        clsTestData_Campaign.createCampaign(false);
            clsTestData_Campaign.lstCampaign[0].Status = 'Accounts Approved';
        insert clsTestData_Campaign.lstCampaign;
        // Create Target Account Data
        List<Target_Account__c> lstTargetAccount = new List<Target_Account__c>();
        List<Target_Account__c> lstTargetAccount_Error = new List<Target_Account__c>();

        clsTestData_Campaign.iRecord_TargetAccount = 2;
        clsTestData_Campaign.createTargetAccount(false);
            clsTestData_Campaign.lstTargetAccount[0].Status__c = 'New';
            clsTestData_Campaign.lstTargetAccount[1].Status__c = 'Approved';
        insert clsTestData_Campaign.lstTargetAccount;
        //--------------------------------------------------------

        List<Database.SaveResult> lstSaveResult_Campaign_Error;
        List<Database.SaveResult> lstSaveResult_Campaign;

        //--------------------------------------------------------
        // Perform Test
        //--------------------------------------------------------
        Test.startTest();

        lstSaveResult_Campaign_Error = Database.update(clsTestData_Campaign.lstCampaign, false);

        clsTestData_Campaign.lstTargetAccount[0].Status__c = 'Approved';
        update clsTestData_Campaign.lstTargetAccount[0];

        lstSaveResult_Campaign = Database.update(clsTestData_Campaign.lstCampaign, false);

        Test.stopTest();
        //--------------------------------------------------------


        //--------------------------------------------------------
        // Validation
        //--------------------------------------------------------
        System.assertEquals(lstSaveResult_Campaign_Error.size(), 1);
        for (Database.SaveResult oSaveResult_Error : lstSaveResult_Campaign_Error){
            System.assert(!oSaveResult_Error.isSuccess());

            for (Database.Error oError : oSaveResult_Error.getErrors()){
                System.assert(oError.getMessage().contains('Please make sure that all target accounts are approved'));
            }
        }

        System.assertEquals(lstSaveResult_Campaign.size(), 1);
        for (Database.SaveResult oSaveResult : lstSaveResult_Campaign){
            System.assert(oSaveResult.isSuccess());
        }
        //--------------------------------------------------------

    }

    @isTest static void test_CCcampaignStatusCheck_ContactsApproved(){

        //--------------------------------------------------------
        // Create Test Data
        //--------------------------------------------------------
        // Create Campaign Data
        clsTestData_Campaign.iRecord_Campaign = 1;
        clsTestData_Campaign.createCampaign(false);
            clsTestData_Campaign.lstCampaign[0].Status = 'Contacts Approved';
        insert clsTestData_Campaign.lstCampaign;

        // Create CampaignMember Data
        List<CampaignMember> lstCampaignMember = new List<CampaignMember>();
        List<CampaignMember> lstCampaignMember_Error = new List<CampaignMember>();

        clsTestData_Campaign.oMain_CampaignMember = null;
        clsTestData_Campaign.iRecord_CampaignMember = 2;
        clsTestData_Campaign.oMain_Campaign = clsTestData_Campaign.lstCampaign[0];
        clsTestData_Campaign.createCampaignMember(false);
            clsTestData_Campaign.lstCampaignMember[0].CC_Status__c = 'Not Approved';
            clsTestData_Campaign.lstCampaignMember[1].CC_Status__c = 'Approved';
        lstCampaignMember_Error.add(clsTestData_Campaign.lstCampaignMember[0]);
        lstCampaignMember.add(clsTestData_Campaign.lstCampaignMember[1]);
        //--------------------------------------------------------

        List<Database.SaveResult> lstSaveResult;
        List<Database.SaveResult> lstSaveResult_Error;

        //--------------------------------------------------------
        // Perform Test
        //--------------------------------------------------------
        Test.startTest();

        lstSaveResult = Database.insert(lstCampaignMember, false);
        lstSaveResult_Error = Database.insert(lstCampaignMember_Error, false);

        Test.stopTest();
        //--------------------------------------------------------


        //--------------------------------------------------------
        // Validation
        //--------------------------------------------------------
        for (Database.SaveResult oSaveResult : lstSaveResult){
            System.assert(oSaveResult.isSuccess());
        }

        for (Database.SaveResult oSaveResult_Error : lstSaveResult_Error){
            System.assert(!oSaveResult_Error.isSuccess());

            for (Database.Error oError : oSaveResult_Error.getErrors()){
                System.assert(oError.getMessage().contains('Please make sure that all contacts are approved'));
            }

        }
        //--------------------------------------------------------

    }

    @isTest static void test_CCcampaignStatusCheck_InvitationCallersAssigned(){

        //--------------------------------------------------------
        // Create Test Data
        //--------------------------------------------------------
        List<User> lstUser = [SELECT Id FROM User WHERE IsActive = true LIMIT 1];

        // Create Campaign Data
        clsTestData_Campaign.iRecord_Campaign = 1;
        clsTestData_Campaign.createCampaign(false);
            clsTestData_Campaign.lstCampaign[0].Status = 'Invitation Callers Assigned';
        insert clsTestData_Campaign.lstCampaign;

        // Create CampaignMember Data
        List<CampaignMember> lstCampaignMember = new List<CampaignMember>();
        List<CampaignMember> lstCampaignMember_Error = new List<CampaignMember>();

        clsTestData_Campaign.oMain_CampaignMember = null;
        clsTestData_Campaign.iRecord_CampaignMember = 2;
        clsTestData_Campaign.oMain_Campaign = clsTestData_Campaign.lstCampaign[0];
        clsTestData_Campaign.createCampaignMember(false);
            clsTestData_Campaign.lstCampaignMember[0].Invitation_Caller__c = null;
            clsTestData_Campaign.lstCampaignMember[1].Invitation_Caller__c = lstUser[0].Id;
        lstCampaignMember_Error.add(clsTestData_Campaign.lstCampaignMember[0]);
        lstCampaignMember.add(clsTestData_Campaign.lstCampaignMember[1]);
        //--------------------------------------------------------

        List<Database.SaveResult> lstSaveResult;
        List<Database.SaveResult> lstSaveResult_Error;

        //--------------------------------------------------------
        // Perform Test
        //--------------------------------------------------------
        Test.startTest();

        lstSaveResult = Database.insert(lstCampaignMember, false);
        lstSaveResult_Error = Database.insert(lstCampaignMember_Error, false);

        Test.stopTest();
        //--------------------------------------------------------


        //--------------------------------------------------------
        // Validation
        //--------------------------------------------------------
        for (Database.SaveResult oSaveResult : lstSaveResult){
            System.assert(oSaveResult.isSuccess());
        }

        for (Database.SaveResult oSaveResult_Error : lstSaveResult_Error){
            System.assert(!oSaveResult_Error.isSuccess());

            for (Database.Error oError : oSaveResult_Error.getErrors()){
                System.assert(oError.getMessage().contains('Please make sure to assign an invitation caller to all contacts'));
            }

        }
        //--------------------------------------------------------

    }

    @isTest static void test_CCcampaignStatusCheck_InvitationCallsCompleted(){

        //--------------------------------------------------------
        // Create Test Data
        //--------------------------------------------------------
        // Create Campaign Data
        clsTestData_Campaign.iRecord_Campaign = 1;
        clsTestData_Campaign.createCampaign(false);
            clsTestData_Campaign.lstCampaign[0].Status = 'Invitation Calls Completed';
        insert clsTestData_Campaign.lstCampaign;

        // Create CampaignMember Data
        List<CampaignMember> lstCampaignMember = new List<CampaignMember>();
        List<CampaignMember> lstCampaignMember_Error = new List<CampaignMember>();

        clsTestData_Campaign.oMain_CampaignMember = null;
        clsTestData_Campaign.iRecord_CampaignMember = 2;
        clsTestData_Campaign.oMain_Campaign = clsTestData_Campaign.lstCampaign[0];
        clsTestData_Campaign.createCampaignMember(false);
            clsTestData_Campaign.lstCampaignMember[0].CC_Status__c = 'NOT Invitation Call Declined';
            clsTestData_Campaign.lstCampaignMember[1].CC_Status__c = 'Invitation Call Declined';
        lstCampaignMember_Error.add(clsTestData_Campaign.lstCampaignMember[0]);
        lstCampaignMember.add(clsTestData_Campaign.lstCampaignMember[1]);
        //--------------------------------------------------------

        List<Database.SaveResult> lstSaveResult;
        List<Database.SaveResult> lstSaveResult_Error;

        //--------------------------------------------------------
        // Perform Test
        //--------------------------------------------------------
        Test.startTest();

        lstSaveResult = Database.insert(lstCampaignMember, false);
        lstSaveResult_Error = Database.insert(lstCampaignMember_Error, false);

        Test.stopTest();
        //--------------------------------------------------------


        //--------------------------------------------------------
        // Validation
        //--------------------------------------------------------
        for (Database.SaveResult oSaveResult : lstSaveResult){
            System.assert(oSaveResult.isSuccess());
        }

        for (Database.SaveResult oSaveResult_Error : lstSaveResult_Error){
            System.assert(!oSaveResult_Error.isSuccess());

            for (Database.Error oError : oSaveResult_Error.getErrors()){
                System.assert(oError.getMessage().contains('Please make sure to complete all invitation calls'));
            }

        }
        //--------------------------------------------------------

    }   

    @isTest static void test_CCcampaignStatusCheck_FeedbackCallsCompleted(){

        //--------------------------------------------------------
        // Create Test Data
        //--------------------------------------------------------
        // Create Campaign Data
        clsTestData_Campaign.iRecord_Campaign = 1;
        clsTestData_Campaign.createCampaign(false);
            clsTestData_Campaign.lstCampaign[0].Status = 'Feedback Calls Completed';
        insert clsTestData_Campaign.lstCampaign;

        // Create CampaignMember Data
        List<CampaignMember> lstCampaignMember = new List<CampaignMember>();
        List<CampaignMember> lstCampaignMember_Error = new List<CampaignMember>();

        clsTestData_Campaign.oMain_CampaignMember = null;
        clsTestData_Campaign.iRecord_CampaignMember = 2;
        clsTestData_Campaign.oMain_Campaign = clsTestData_Campaign.lstCampaign[0];
        clsTestData_Campaign.createCampaignMember(false);
            clsTestData_Campaign.lstCampaignMember[0].CC_Status__c = 'Feedback Call Required';
            clsTestData_Campaign.lstCampaignMember[1].CC_Status__c = 'NOT Feedback Call Required';
        lstCampaignMember_Error.add(clsTestData_Campaign.lstCampaignMember[0]);
        lstCampaignMember.add(clsTestData_Campaign.lstCampaignMember[1]);
        //--------------------------------------------------------

        List<Database.SaveResult> lstSaveResult;
        List<Database.SaveResult> lstSaveResult_Error;

        //--------------------------------------------------------
        // Perform Test
        //--------------------------------------------------------
        Test.startTest();

        lstSaveResult = Database.insert(lstCampaignMember, false);
        lstSaveResult_Error = Database.insert(lstCampaignMember_Error, false);

        Test.stopTest();
        //--------------------------------------------------------


        //--------------------------------------------------------
        // Validation
        //--------------------------------------------------------
        for (Database.SaveResult oSaveResult : lstSaveResult){
            System.assert(oSaveResult.isSuccess());
        }

        for (Database.SaveResult oSaveResult_Error : lstSaveResult_Error){
            System.assert(!oSaveResult_Error.isSuccess());

            for (Database.Error oError : oSaveResult_Error.getErrors()){
                System.assert(oError.getMessage().contains('Please make sure to complete all feedback calls'));
            }

        }
        //--------------------------------------------------------

    }           

}