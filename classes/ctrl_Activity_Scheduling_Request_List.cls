public with sharing class ctrl_Activity_Scheduling_Request_List {
    
    @AuraEnabled
    public static List<ActivityRequest> getActivityRequests(String accountFilter, Date startFilter, String statusFilter, String typeFilter, String teamFilter, String assignedToFilter, Integer size, Integer offset, String orderDirection) {
    	
    	List<ActivityRequest> result = new List<ActivityRequest>();
    	
    	String query = 'Select Id, Account.Name, Type, Status, Procedure__c, Activity_Type_Picklist__c, Start_of_Procedure__c, Activity_Scheduling_Team__r.Name, Assigned_To__r.Name, Subject, Procedure_Duration_Implants__c, ';
    	query += ' (Select Attendee__r.Name from Activity_Scheduling_Attendees__r)';
    	query += ' FROM Case where RecordType.DeveloperName = \'Implant_Scheduling\' AND Is_Recurring__c = false' ;
    	    	
    	if(startFilter != null) query += ' AND Start_of_Procedure__c >= :startFilter ';
    	if(accountFilter != null && accountFilter != '') query += ' AND AccountId = :accountFilter ';
    	if(statusFilter != null && statusFilter != '') query += ' AND Status = :statusFilter ';
    	if(typeFilter != null && typeFilter != '') query += ' AND Type = :typeFilter ';
    	else query += ' AND Type IN (\'Implant Support Request\', \'Service Request\') ';
    	
    	if(teamFilter != null && teamFilter != '') query += ' AND Activity_Scheduling_Team__c = :teamFilter ';
    	else query += ' AND Activity_Scheduling_Team__c IN (Select Id from Implant_Scheduling_Team__c) ';
    	
    	List<Case> secondaryCases;
    	if(assignedToFilter != null && assignedToFilter != ''){
    		
    		secondaryCases = [Select Id from Case where Id IN (Select Case__c from Activity_Scheduling_Attendee__c where Attendee__c = :assignedToFilter)];
    		
    		query += ' AND (Assigned_To__c = :assignedToFilter OR Id IN :secondaryCases) ';
    	}
    	
    	query += ' ORDER BY Start_of_Procedure__c ' + orderDirection + ' LIMIT ' + size + ' OFFSET ' + offSet;
    	
    	System.debug('Query: ' + query);
    	
    	for(Case requestCase : Database.query(query)){
    		
    		ActivityRequest req = new ActivityRequest();
    		req.id = requestCase.Id;
    		req.account = requestCase.Account.Name;
    		req.status = requestCase.Status;
    		req.requestType = requestCase.Type;
    		req.activity = requestCase.Type == 'Implant Support Request'? requestCase.Procedure__c : requestCase.Activity_Type_Picklist__c;
    		req.start = requestCase.Start_of_Procedure__c;
    		req.duration = requestCase.Procedure_Duration_Implants__c;
    		req.team = requestCase.Activity_Scheduling_Team__r != null ? requestCase.Activity_Scheduling_Team__r.Name : '';
    		
    		List<String> assignedTo = new List<String>();
    		if(requestCase.Assigned_To__r != null) assignedTo.add(requestCase.Assigned_To__r.Name);    		
    		for(Activity_Scheduling_Attendee__c attendee : requestCase.Activity_Scheduling_Attendees__r) assignedTo.add(attendee.Attendee__r.Name);
    		
    		req.assignedTo = assignedTo.size() > 0 ? String.join(assignedTo, ',\n') : '';
    		req.subject = requestCase.Subject;    		
    		result.add(req);
    	}
    	
    	return result;    	
    }
    
    public class ActivityRequest {
    	
    	@AuraEnabled public String id {get; set;}
    	@AuraEnabled public String account {get; set;}
    	@AuraEnabled public String status {get; set;}
    	@AuraEnabled public String requestType {get; set;}
    	@AuraEnabled public String activity {get; set;}
    	@AuraEnabled public DateTime start {get; set;}
    	@AuraEnabled public String duration {get; set;}
    	@AuraEnabled public String priority {get; set;}
    	@AuraEnabled public String team {get; set;}
    	@AuraEnabled public String assignedTo {get; set;}
    	@AuraEnabled public String subject {get; set;}	
    }
}