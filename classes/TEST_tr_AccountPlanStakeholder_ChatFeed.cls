/*
 *      Created Date 	: 17-02-2014
 *      Author 			: Bart Caelen
 *      Description 	: TEST Class for the APEX TRIGGER tr_AccountPlanStakeholder_ChatFeed
 */
@isTest private class TEST_tr_AccountPlanStakeholder_ChatFeed {

    @isTest static void test_tr_AccountPlanStakeholder_ChatFeed() {
		//------------------------------------------------
		// CREATE TEST DATA
		//------------------------------------------------
		clsTestData.createAccountPlanStakeholderData();		// TEST Insert
		//------------------------------------------------

		// TEST Update
		update clsTestData.oMain_AccountPlanStakeholder;

		// TEST Delete
		delete clsTestData.oMain_AccountPlanStakeholder;
		
		// TEST Undelete
		undelete clsTestData.oMain_AccountPlanStakeholder;
    }
}