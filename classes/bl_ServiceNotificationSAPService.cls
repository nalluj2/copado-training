public with sharing class bl_ServiceNotificationSAPService {
    
    public static Case getCase(String caseId, Boolean create)
    {
        return getCase(caseId, 'Id', create);
    }
    
    public static Map<Id, Case> getCases(Set<Id> caseIds)
    {
    	return new Map<Id, Case>([SELECT SAP_Equipment_Number__c, Serial_Number__c,    Material_Code__c, 
					    				   Material_Description__c, Priority,            GCH_Group__c,
					    				   GCH_Code__c,             GCH_Question_1__c,   GCH_Question_2__c,
					    				   GCH_Question_3__c,       Question_Comment__c, Actual_Breakdown_Date_Time__c,
					    				   SVMXC__Actual_Restoration__c,Customer_Down__c, Contract_Number__c,
					    				   Description, SVMX_TSU_RECALL_Indicator__c, SVMX_TSU_Number__c
				FROM Case
				WHERE Id IN :caseIds]);
    }
    
    public static Case getCase(String tId, String tField, Boolean bCreate)
    {

        String tSOQL = 'SELECT';
            tSOQL += ' Id,                                  Subject,                            Description,';
            tSOQL += ' SVMX_SAP_Notification_Number__c,     SVMX_SAP_Notification_Type__c,      Contact.MailingStreet,';
            tSOQL += ' Contact.MailingPostalCode,           Contact.MailingCity,                Contact.MailingCountry,';
            tSOQL += ' Contact.Phone,                       Contact.Name,                       CreatedBy.Name,';
            tSOQL += ' CreatedDate,                         SVMX_PO_Number__c,                  SVMX_PO_Number_Date__c,';
            tSOQL += ' SVMXC__Preferred_Start_Time__c,      SVMXC__Preferred_End_Time__c,       Owner.Name,';
            tSOQL += ' Status,                              Milestone_Status__c,                Reported_By__c,';
            tSOQL += ' Breakdown_Duration__c,				SVMX_TSU_Number__c,					SVMX_TSU_RECALL_Indicator__c,';
            tSOQL += ' Breakdown_Unit__c,                   Business_Unit__c,                   Contract_Item__c,';
            tSOQL += ' Contract_Number__c,                  Customer_Down__c,                   Effect__c,';
            tSOQL += ' Equipment_Description__c,            GCH_Code__c,                        GCH_Group__c,';
            tSOQL += ' GCH_Question_1__c,                   GCH_Question_2__c,                  GCH_Question_3__c,';
            tSOQL += ' Material_Code__c,                    Material_Description__c,            Question_Comment__c,';
            tSOQL += ' SAP_Equipment_Number__c,             Serial_Number__c,                   SAP_Service_Order_No__c,';
            tSOQL += ' Notified_Date_Time__c,               Actual_Breakdown_Date_Time__c,      Breakdown_End_Date_Time__c';
        tSOQL += ' FROM Case ';
        tSOQL += ' WHERE ';
            tSOQL += clsUtil.isNull(tField, 'Id') + ' = :tId';

        List<Case> lstCase = Database.query(String.escapeSingleQuotes(tSOQL));

        if (lstCase.size() > 1) throw new ws_Exception('Multiple service notifications found for ' + tField + ': ' + tId);  	
        else if (lstCase.size() == 1) return lstCase[0];
        else if (lstCase.size() == 0 && bCreate) return new Case(); 
        else throw new ws_Exception('No service notification found for ' + tField + ': ' + tId);

    }

    public static Account mapSAPServiceNotificationToAccount(ws_ServiceNotificationSAPService.SAPServiceNotification sapServiceNotification)
    {
    	
    	//get the list of accounts
    	List<ws_ServiceNotificationSAPService.SAPServiceNotificationAccount> sapAccounts = sapServiceNotification.accounts;
    	
    	if (sapAccounts == null || sapAccounts.size() == 0)
    		throw new ws_Exception('No account SAP Id provided for the service notification');
    	
    	//get the SAP Id from the Sold To / Hospital account
		ws_ServiceNotificationSAPService.SAPServiceNotificationAccount accountSAP;
		
		for(ws_ServiceNotificationSAPService.SAPServiceNotificationAccount acc: sapAccounts){
			
			// Choose the account based on the following priority: 1) SoldTo(SP) 2) ShipTo(SH)
			if(acc.ACCOUNT_TYPE == 'SP') accountSAP = acc;
			if(acc.ACCOUNT_TYPE == 'SH' && accountSAP == null) accountSAP = acc;			
		}
		
		// This should never happen but we better avoid errors in case of changes or special cases
		if(accountSAP == null) throw new ws_Exception('Non supported Account Type/s. Valid Account Types are: SH and SP.');
		
		//get the account based on the SAP Id
		Account account = null;
    	List<Account> accounts = [SELECT Id 
    							  FROM Account
    							  WHERE SAP_ID__c = :accountSAP.ACCOUNT_NUMBER];
    							  
        if (accounts.size() > 1) throw new ws_Exception('Multiple accounts found for SAP Id: ' + accountSAP.ACCOUNT_NUMBER);  	
        else if (accounts.size() == 0) throw new ws_Exception('No account found for SAP Id: ' + accountSAP.ACCOUNT_NUMBER);
    	else account = accounts[0];
    	
    	return account;
    }

    public static Case mapSAPServiceNotificationToCase(ws_ServiceNotificationSAPService.SAPServiceNotification notification)
    {
    	
    	//map the fields        
        Case cse = new Case();        
        cse.SVMX_SAP_Notification_Number__c = notification.SAP_NOTIFICATION_NUMBER;
        cse.SVMX_SAP_Notification_Type__c   = notification.SAP_NOTIFICATION_TYPE;
        cse.Subject                         = notification.SUBJECT;
        cse.Description                     = notification.DESCRIPTION;
        
        String status = ws_SharedMethods.getSAPMapping('Case', 'Status', notification.STATUS);
        if(notification.STATUS != null && notification.STATUS != '' && status == null) throw new ws_Exception('Non supported Status value: ' + notification.STATUS);        
        cse.Status                          = status;
        
		cse.SVMX_PO_Number__c               = notification.CUSTOMER_PO;
		cse.GCH_Number__c					= notification.GCH_NUMBER;
        cse.RecordTypeId                    = clsUtil.getRecordTypeByDevName('Case', 'Field_Service').Id;
        cse.Milestone_Status__c             = notification.MILESTONE_STATUS;
        cse.Reported_By__c                  = notification.REPORTED_BY;        
        cse.Breakdown_Duration__c           = notification.BREAKDOWN_DURATION;
        cse.Breakdown_Unit__c               = notification.BREAKDOWN_UNIT;
        cse.Business_Unit__c                = notification.BUSINESS_UNIT;
        cse.Contract_Item__c                = notification.SERVICE_CONTRACT_ITEM;
        cse.Contract_Number__c              = notification.SERVICE_CONTRACT_DOC;
        cse.Effect__c                       = notification.EFFECT;
        cse.Equipment_Description__c        = notification.SAP_EQUIPMENT_DESCRIPTION;
        cse.GCH_Code__c                     = notification.CODING_CODE;
        cse.GCH_Group__c                    = notification.CODING_GROUP;
        cse.GCH_Question_1__c               = ws_SharedMethods.getSAPMapping('Case', 'GCH_Question_1__c', notification.QUESTION_1);
        cse.GCH_Question_2__c               = ws_SharedMethods.getSAPMapping('Case', 'GCH_Question_2__c', notification.QUESTION_2);
        cse.GCH_Question_3__c               = ws_SharedMethods.getSAPMapping('Case', 'GCH_Question_3__c', notification.QUESTION_3);
        cse.Material_Code__c                = notification.MATERIAL;
        cse.Material_Description__c         = notification.MATERIAL_DESC;
        cse.Question_Comment__c             = notification.COMMENT;
        cse.SAP_Equipment_Number__c         = notification.SAP_EQUIPMENT_NUMBER;
        cse.Serial_Number__c                = notification.SERIAL_LOT_NUMBER;
        cse.SAP_Service_Order_No__c         = notification.SAP_WORK_ORDER_REFERENCE;
        cse.SVMX_PO_Number_Date__c          = ut_String.stringToDate(notification.PO_DATE);
        cse.Notified_Date_Time__c           = ut_String.stringToDatetime(notification.NOTIFIED_DATE + notification.NOTIFIED_TIME);
        cse.Actual_Breakdown_Date_Time__c   = ut_String.stringToDatetime(notification.MALFUNCTION_START + notification.START_MALFUNCTION_TIME);
        cse.Breakdown_End_Date_Time__c      = ut_String.stringToDatetime(notification.MALFUNCTION_END + notification.MALFUNCTION_END_TIME);
        cse.SVMXC__Preferred_Start_Time__c  = ut_String.stringToDatetime(notification.PREFERRED_START_DATE + notification.PREFERRED_START_TIME);
        cse.SVMXC__Preferred_End_Time__c    = ut_String.stringToDatetime(notification.PREFERRED_END_DATE + notification.PREFERRED_END_TIME);
        cse.SVMX_TSU_Number__c				= notification.TSU_RECALL_NUMBER;
        cse.SVMX_TSU_RECALL_Indicator__c	= ws_SharedMethods.getSAPMapping('Case', 'SVMX_TSU_RECALL_Indicator__c', notification.TSU_RECALL_INDICATOR);
        
		if (notification.BREAKDOWN_INDICATOR != null)
			cse.Customer_Down__c                = ut_String.getBooleanValue(notification.BREAKDOWN_INDICATOR);
                
		//get the account associated to the case and set the Id of the lookup
		Account cseAccount = bl_ServiceNotificationSAPService.mapSapServiceNotificationToAccount(notification);

		cse.AccountId = cseAccount.Id;
		
		// Installed Product
    	if(notification.SAP_EQUIPMENT_NUMBER != null){
    	
	    	List<SVMXC__Installed_Product__c> instProduct = [Select Id, SVMXC__Product__c from SVMXC__Installed_Product__c where SVMX_SAP_Equipment_ID__c = :notification.SAP_EQUIPMENT_NUMBER];
	    	if(instProduct.size() == 0) throw new ws_Exception('No Installed Product was found for SAP Id: ' + notification.SAP_EQUIPMENT_NUMBER);  	
	    	if(instProduct.size() == 2) throw new ws_Exception('Multiple Installed Products were found for SAP Id: ' + notification.SAP_EQUIPMENT_NUMBER);
	    	
	    	cse.SVMXC__Component__c = instProduct[0].Id;
	    	cse.SVMXC__Product__c = instProduct[0].SVMXC__Product__c;
    	}

    	return cse;
    }
    
    public static List<SVMX_Service_Notification_Item__c> mapSAPServiceNotificationItems(List<ws_ServiceNotificationSAPService.SAPServiceNotificationItem> sapItems, Case cse){
    	
    	List<SVMX_Service_Notification_Item__c> notificationItems = new List<SVMX_Service_Notification_Item__c>();
    	
    	if(sapItems == null || sapItems.isEmpty()) return notificationItems;
    	
    	Set<String> damageCodeGroups = new Set<String>();
    	
    	for(ws_ServiceNotificationSAPService.SAPServiceNotificationItem sapItem : sapItems){
    		
    		if(sapItem.CODE_GROUP_PROBLEM != null) damageCodeGroups.add(sapItem.CODE_GROUP_PROBLEM);  
    		
    		if(sapItem.causes != null && sapItem.causes.size() > 0){
    			
    			for(ws_ServiceNotificationSAPService.SAPServiceNotificationItemCause sapCause : sapItem.causes){
    			
    				if(sapCause.CAUSE_GROUP !=null) damageCodeGroups.add(sapCause.CAUSE_GROUP); 
    			}
    		}  			
    	}
    	
    	Map<String, Id> damageCodeMap = new Map<String, Id>();
    	Map<String, Id> causeCodeMap = new Map<String, Id>();
    	
    	for(SVMX_Damage_Cause_Code__c damageCauseCode : [Select Id, SVMX_Code__c, SVMX_Damage_Code__c, SVMX_Cause_Code_Group__c, SVMX_Code_Type__c from SVMX_Damage_Cause_Code__c where SVMX_Cause_Code_Group__c IN :damageCodeGroups]){
    		
    		if(damageCauseCode.SVMX_Code_Type__c == 'Damage'){
    			
    			damageCodeMap.put(damageCauseCode.SVMX_Cause_Code_Group__c + ':' + damageCauseCode.SVMX_Damage_Code__c, damageCauseCode.Id);
    			
    		}else{
    			
    			causeCodeMap.put(damageCauseCode.SVMX_Cause_Code_Group__c + ':' + damageCauseCode.SVMX_Code__c, damageCauseCode.Id);
    		}	
    	}    	
    	   	
    	for(ws_ServiceNotificationSAPService.SAPServiceNotificationItem sapItem : sapItems){
    		
    		Id damageId = damageCodeMap.get(sapItem.CODE_GROUP_PROBLEM + ':' + sapItem.PROBLEM_OR_DAMAGE_CODE);    			
			if(damageId == null) throw new ws_Exception('No Damage Code definition was found for Group and Damage Code : ' + sapItem.CODE_GROUP_PROBLEM + ' / ' + sapItem.PROBLEM_OR_DAMAGE_CODE);
    		
    		if(sapItem.causes != null && sapItem.causes.size() > 0){
    			
    			for(ws_ServiceNotificationSAPService.SAPServiceNotificationItemCause sapCause : sapItem.causes){
    				
    				Id causeId = causeCodeMap.get(sapCause.CAUSE_GROUP + ':' + sapCause.CAUSE_CODE);    				
    				if(causeId == null) throw new ws_Exception('No Cause Code definition was found for Group and Cause Code : ' + sapCause.CAUSE_GROUP + ' / ' + sapCause.CAUSE_CODE);
    				
    				SVMX_Service_Notification_Item__c damageCauseCode = new SVMX_Service_Notification_Item__c();
	    			damageCauseCode.SVMX_Service_Notification__c = cse.Id;
	    			damageCauseCode.Item_SAP_Id__c = sapItem.ITEM_NUMBER;
	    			damageCauseCode.Cause_SAP_Id__c = sapCause.CAUSE_NUMBER;
	    			damageCauseCode.Unique_Key__c = cse.Id + ':' + sapItem.ITEM_NUMBER + ':' + sapCause.CAUSE_NUMBER;
	    			damageCauseCode.SVMX_Damage_Code__c = damageId;
	    			damageCauseCode.SVMX_Damage_Code_Text__c = sapItem.PROBLEM_DESC;
	    			damageCauseCode.SVMX_Cause_Code__c = causeId;
	    			damageCauseCode.SVMX_Code_Text__c = sapCause.CAUSE_TEXT;
	    			notificationItems.add(damageCauseCode);
    			}
    			
    		}else{
    			    			    			
    			SVMX_Service_Notification_Item__c damageCode = new SVMX_Service_Notification_Item__c();
    			damageCode.SVMX_Service_Notification__c = cse.Id;
    			damageCode.Unique_Key__c = cse.Id + ':' + sapItem.ITEM_NUMBER;
    			damageCode.Item_SAP_Id__c = sapItem.ITEM_NUMBER;
    			damageCode.SVMX_Damage_Code__c = damageId;
    			damageCode.SVMX_Damage_Code_Text__c = sapItem.PROBLEM_DESC;
    			notificationItems.add(damageCode);
    		}    		
    	}
    	
    	return notificationItems;    	
    }
    
    public static void updateServiceOrderSAPDamageCauses(Id caseId, List<SVMX_Service_Notification_Item__c> notificationItems){
    	
    	List<SVMXC__Service_Order__c> serviceOrder = [Select Id, SVMXC__Order_Status__c from SVMXC__Service_Order__c where SVMXC__Case__c = :caseId LIMIT 1];
    	
    	//If no WO yet or WO has been set as completed already, then do nothing
    	if(serviceOrder.isEmpty() || serviceOrder[0].SVMXC__Order_Status__c == bl_SVMXC_ServiceOrder_Trigger.SERVICEORDER_COMPLETED) return;
    	
    	List<SVMX_Fault_Code__c> damageCauseLines = new List<SVMX_Fault_Code__c>();
    	
    	for(SVMX_Service_Notification_Item__c notificationItem : notificationItems){
    		
    		SVMX_Fault_Code__c damageCauseLine = new SVMX_Fault_Code__c();
    		damageCauseLine.SVMX_Cause_Code__c = notificationItem.SVMX_Cause_Code__c;
    		damageCauseLine.SVMX_Code_Text__c = notificationItem.SVMX_Code_Text__c;
    		damageCauseLine.SVMX_Damage_Code__c = notificationItem.SVMX_Damage_Code__c;
    		damageCauseLine.SVMX_Damage_Code_Text__c = notificationItem.SVMX_Damage_Code_Text__c;
    		damageCauseLine.SVMX_From_SAP__c = true;
    		damageCauseLine.SAP_Id__c = notificationItem.Item_SAP_Id__c;
    		damageCauseLine.Cause_SAP_Id__c = notificationItem.Cause_SAP_Id__c;
    		damageCauseLine.SVMX_Work_Order__c = serviceOrder[0].Id;
    		damageCauseLine.Service_Notification_Item_ref__c = notificationItem.Id;
    		
    		damageCauseLines.add(damageCauseLine);
    	}
    	
    	if(damageCauseLines.size() > 0){
    		
    		upsert damageCauseLines Service_Notification_Item_ref__c;
    		delete [Select Id from SVMX_Fault_Code__c where SVMX_Work_Order__c = :serviceOrder[0].Id AND SVMX_From_SAP__c = true AND Id NOT IN :damageCauseLines];
    		
    	}else delete [Select Id from SVMX_Fault_Code__c where SVMX_Work_Order__c = :serviceOrder[0].Id AND SVMX_From_SAP__c = true];
    }
   
    public static ws_ServiceNotificationSAPService.SAPServiceNotification mapCaseToSAPServiceNotification(Case cse){
    	
    	ws_ServiceNotificationSAPService.SAPServiceNotification notification = new ws_ServiceNotificationSAPService.SAPServiceNotification();
     	
		notification.SAP_NOTIFICATION_NUMBER    = cse.SVMX_SAP_Notification_Number__c;
		notification.SAP_NOTIFICATION_TYPE      = cse.SVMX_SAP_Notification_Type__c;
		notification.SUBJECT                    = cse.Subject;
		notification.DESCRIPTION                = cse.Description;
		notification.STATUS                     = ws_SharedMethods.getSAPMapping('Case', 'Status', cse.Status);
		notification.REPORTED_BY                = cse.CreatedBy.Name;
		notification.NOTIFIED_DATE              = ut_String.datetimeToString(cse.CreatedDate);
		notification.NOTIFIED_TIME              = ut_String.datetimeToString(cse.CreatedDate); 
		notification.CUSTOMER_PO                = cse.SVMX_PO_Number__c;
		notification.PO_DATE                    = ut_String.dateToString(cse.SVMX_PO_Number_Date__c);
		notification.CASE_OWNER                 = cse.Owner.Name;
		notification.SHORT_TEXT                 = cse.Subject;
		notification.LONG_TEXT                  = cse.Description;
        notification.DISTRIBUTION_STATUS        = 'TRUE';
        notification.DIST_ERROR_DETAILS         = '';
		notification.MILESTONE_STATUS           = cse.Milestone_Status__c;
		notification.REPORTED_BY                = cse.Reported_By__c;
		notification.BREAKDOWN_DURATION         = cse.Breakdown_Duration__c;
		notification.BREAKDOWN_UNIT             = cse.Breakdown_Unit__c;
		notification.BUSINESS_UNIT              = cse.Business_Unit__c;
        notification.SERVICE_CONTRACT_ITEM      = cse.Contract_Item__c;
        notification.SERVICE_CONTRACT_DOC       = cse.Contract_Number__c;
        notification.EFFECT                     = cse.Effect__c;
        notification.SAP_EQUIPMENT_DESCRIPTION  = cse.Equipment_Description__c;
        notification.CODING_CODE                = cse.GCH_Code__c;
        notification.CODING_GROUP               = cse.GCH_Group__c;
        notification.QUESTION_1                 = ws_SharedMethods.getSAPMapping('Case', 'GCH_Question_1__c', cse.GCH_Question_1__c);
        notification.QUESTION_2                 = ws_SharedMethods.getSAPMapping('Case', 'GCH_Question_2__c', cse.GCH_Question_2__c);
        notification.QUESTION_3                 = ws_SharedMethods.getSAPMapping('Case', 'GCH_Question_3__c', cse.GCH_Question_3__c);
        notification.MATERIAL                   = cse.Material_Code__c;
        notification.MATERIAL_DESC              = cse.Material_Description__c;
        notification.COMMENT                    = cse.Question_Comment__c;
        notification.SAP_EQUIPMENT_NUMBER       = cse.SAP_Equipment_Number__c;
        notification.SERIAL_LOT_NUMBER          = cse.Serial_Number__c;
        notification.SAP_WORK_ORDER_REFERENCE   = cse.SAP_Service_Order_No__c;
        notification.NOTIFIED_DATE              = ut_String.datetimeToDateString(cse.Notified_Date_Time__c);
        notification.NOTIFIED_TIME              = ut_String.datetimeToTimeString(cse.Notified_Date_Time__c);
        notification.MALFUNCTION_START          = ut_String.datetimeToDateString(cse.Actual_Breakdown_Date_Time__c);
        notification.START_MALFUNCTION_TIME     = ut_String.datetimeToTimeString(cse.Actual_Breakdown_Date_Time__c);
        notification.MALFUNCTION_END            = ut_String.datetimeToDateString(cse.Breakdown_End_Date_Time__c);
        notification.MALFUNCTION_END_TIME       = ut_String.datetimeToTimeString(cse.Breakdown_End_Date_Time__c);		
		notification.PREFERRED_START_DATE       = ut_String.datetimeToDateString(cse.SVMXC__Preferred_Start_Time__c);
		notification.PREFERRED_START_TIME       = ut_String.datetimeToTimeString(cse.SVMXC__Preferred_Start_Time__c);
		notification.PREFERRED_END_DATE         = ut_String.datetimeToDateString(cse.SVMXC__Preferred_End_Time__c);
		notification.PREFERRED_END_TIME     	= ut_String.datetimeToTimeString(cse.SVMXC__Preferred_End_Time__c);
		
		if (cse.Customer_Down__c != null)
		{
        	notification.BREAKDOWN_INDICATOR       = ut_String.getStringValue(cse.Customer_Down__c);
		}
		
    	return notification;	 
    }
}