/**
 * Creation Date :  20090218
 * Description :    Test Coverage of the trigger 'ContactBeforeDelete' (for the Contact Object).
 *                  (To facilitate the development of robust, error-free code, Apex Code requires the creation and execution of unit tests.
 *                  Unit tests are comprised of test methods and classes that verify whether a particular piece of code is working properly.)
 * Author :         ABSI - MC
 *
 * Christophe Saenen	20160511	CR-11040
 */
@isTest
private class TEST_triggerContactBeforeDelete {
	
    static TestMethod void triggerContactBeforeDeleteAsNormalUser(){    
        
        User regularUser = new User(alias = 'DsUser', email='dsusertest@absi.be', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                    localesidkey='en_US', profileid = [Select Id from Profile where Name = 'EUR Field Force CVG'].Id , timezonesidkey='America/Los_Angeles', username='dsusertest@absi.be', Alias_unique__c='usertest1234',Company_Code_Text__c='EUR');
         
        System.runAs(regularUser) {
              
            Account acc = new Account();
            acc.Name = 'Test Account ' ;   
            acc.BillingCountry = 'Belgium';    
            acc.Account_Country_vs__c = 'Belgium';          
            insert acc;
            
            Contact cont = new Contact();
            cont.LastName = 'Test Contact';  
			cont.FirstName = 'test';
            cont.AccountId = acc.Id ; 
            cont.Contact_Department__c = 'Diabetes Adult'; 
            cont.Contact_Primary_Specialty__c = 'ENT';
            cont.Affiliation_To_Account__c = 'Employee';
            cont.Primary_Job_Title_vs__c = 'Manager';
            cont.Contact_Gender__c = 'Male'; 
            insert cont;         
             
            /*
             *  Test to delete a contact with one affiliation (add.ERROR NOT TRIGGER)
             *  The addError function will fire if there is any related object to Contact, so let's create one : 
             *  (Per ex.: a new affiliation)
            */
                         
            List<Affiliation__c> lstAff = [Select id from Affiliation__c where Affiliation_From_Contact__c = :cont.Id];       
            System.assert(lstAff.size() == 1);             
    		
    		// Delete Contact 
             Boolean error = false;
             
             try{
             	
                delete cont;
                
             } catch (Exception e) {
                
                System.assert(e.getMessage().contains('You can only change primary relationships via the contact detail page'));
                error = true;
             }      
             
             System.assert(error == true);
        }
    }
    
    private static testmethod void testDeleteContact_DataSteward(){
    	
    	User dataSteward = new User(alias = 'DsUser', email='dsusertest@absi.be', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                    localesidkey='en_US', profileid = [Select Id from Profile where Name = 'EUR Contact Data Steward'].Id , timezonesidkey='America/Los_Angeles', username='dsusertest@absi.be', Alias_unique__c='usertest1234',Company_Code_Text__c='EUR');
         
        System.runAs(dataSteward) {
              
            Account acc = new Account();
            acc.Name = 'Test Account ' ;   
            acc.BillingCountry = 'Belgium';    
            acc.Account_Country_vs__c = 'Belgium';          
            insert acc;
            
            Contact cont = new Contact();
            cont.LastName = 'Test Contact';  
			cont.FirstName = 'test';
            cont.AccountId = acc.Id ; 
            cont.Contact_Department__c = 'Diabetes Adult'; 
            cont.Contact_Primary_Specialty__c = 'ENT';
            cont.Affiliation_To_Account__c = 'Employee';
            cont.Primary_Job_Title_vs__c = 'Manager';
            cont.Contact_Gender__c = 'Male'; 
            insert cont;     
             
            /*
             *  Test to delete a contact with one affiliation (add.ERROR NOT TRIGGER)
             *  The addError function will fire if there is any related object to Contact, so let's create one : 
             *  (Per ex.: a new affiliation)
            */
                         
            List<Affiliation__c> lstAff = [Select id from Affiliation__c where Affiliation_From_Contact__c = :cont.Id];       
            System.assert(lstAff.size() == 1);             
    		
    		// Delete Contact              
            delete cont;
            
            lstAff = [Select id from Affiliation__c where Affiliation_From_Contact__c = :cont.Id];       
            System.assert(lstAff.size() == 0);
            
            undelete cont;
            
            lstAff = [Select id from Affiliation__c where Affiliation_From_Contact__c = :cont.Id];       
            System.assert(lstAff.size() == 1);
        }
    }
    
    private static testmethod void testDeleteContact_SystemAdmin(){
    	
    	User sysAdmin = new User(alias = 'DsUser', email='dsusertest@absi.be', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                    localesidkey='en_US', profileid = [Select Id from Profile where Name = 'System Administrator'].Id , timezonesidkey='America/Los_Angeles', username='dsusertest@absi.be', Alias_unique__c='usertest1234',Company_Code_Text__c='EUR');
         
        System.runAs(sysAdmin) {
              
            Account acc = new Account();
            acc.Name = 'Test Account ' ;   
            acc.BillingCountry = 'Belgium';    
            acc.Account_Country_vs__c = 'Belgium';          
            insert acc;
            
            Contact cont = new Contact();
            cont.LastName = 'Test Contact';  
			cont.FirstName = 'test';
            cont.AccountId = acc.Id ; 
            cont.Contact_Department__c = 'Diabetes Adult'; 
            cont.Contact_Primary_Specialty__c = 'ENT';
            cont.Affiliation_To_Account__c = 'Employee';
            cont.Primary_Job_Title_vs__c = 'Manager';
            cont.Contact_Gender__c = 'Male'; 
            insert cont;     
             
            /*
             *  Test to delete a contact with one affiliation (add.ERROR NOT TRIGGER)
             *  The addError function will fire if there is any related object to Contact, so let's create one : 
             *  (Per ex.: a new affiliation)
            */
                         
            List<Affiliation__c> lstAff = [Select id from Affiliation__c where Affiliation_From_Contact__c = :cont.Id];       
            System.assert(lstAff.size() == 1);             
    		
    		// Delete Contact              
            delete cont;
            
            lstAff = [Select id from Affiliation__c where Affiliation_From_Contact__c = :cont.Id];       
            System.assert(lstAff.size() == 0);
        }

    }

}