/**
 * Creation Date :  20090219
 * Description :    Test coverage for controller controllerCreateAffiliation
 * Author :         ABSI - BC
 * ------------------------------------------------------------------------
 * Modify Date :    20140624
 * Description :    Completly rewritten to cover most of the apex code
 * Author :         Bart Caelen
 */

@isTest
private class TEST_controllerCreateAffiliation {

    private static Map<String, String> dataMap = new Map<String, String>();

    @isTest
    static void testC2C(){
        
        //---------------------------------------------
        // CREATE TEST DATA
        //---------------------------------------------
        Affiliation__c oAffiliation = createTestData('C2C');
        //---------------------------------------------

        Test.startTest();

        //---------------------------------------------
        // TEST INSERT MODE
        //---------------------------------------------
        testAffiliation(oAffiliation);
        //---------------------------------------------

        //---------------------------------------------
        // TEST UPDATE MODE
        //---------------------------------------------
        testAffiliation(oAffiliation);

        oAffiliation.Affiliation_From_Contact__c = oAffiliation.Affiliation_To_Contact__c;
        oAffiliation.Affiliation_From_Account__c = null;
        oAffiliation.Affiliation_To_Account__c = null;
        oAffiliation.Therapy__c = null;
        testAffiliation(oAffiliation);
        //---------------------------------------------

        Test.stopTest();

    }

    @isTest
    static void testC2A(){
        
        //---------------------------------------------
        // CREATE TEST DATA
        //---------------------------------------------
        Affiliation__c oAffiliation = createTestData('C2A');
        //---------------------------------------------

        Test.startTest();

        //---------------------------------------------
        // TEST INSERT MODE
        //---------------------------------------------
        testAffiliation(oAffiliation);
        //---------------------------------------------

        //---------------------------------------------
        // TEST UPDATE MODE
        //---------------------------------------------
        testAffiliation(oAffiliation);
        //---------------------------------------------

        Test.stopTest();

    }

    @isTest
    static void testA2A(){
        
        //---------------------------------------------
        // CREATE TEST DATA
        //---------------------------------------------
        Affiliation__c oAffiliation = createTestData('A2A');
        //---------------------------------------------

        Test.startTest();

        //---------------------------------------------
        // TEST INSERT MODE
        //---------------------------------------------
        testAffiliation(oAffiliation);
        //---------------------------------------------

        //---------------------------------------------
        // TEST UPDATE MODE
        //---------------------------------------------
        testAffiliation(oAffiliation);
        //---------------------------------------------

        Test.stopTest();

    }


    private static Affiliation__c createTestData(String tType) {

        //---------------------------------------------
        // CREATE TEST DATA
        //---------------------------------------------
        list<Account> lstAc=new list<Account>();
        Account acc1 = new Account();
            acc1.Name = 'TEST_controllerCreateAffiliation TestAccount1' ; 
        lstAc.add(acc1);
        Account acc2 = new Account();
            acc2.Name = 'TEST_controllerCreateAffiliation TestAccount2' ;
        lstAc.add(acc2);
        insert lstAc;
        

        List<Contact> lstCon=new List<Contact>();
        Contact cont1 = new Contact();
            cont1.LastName = 'TEST_controllerCreateAffiliation TestCont1' ;  
            cont1.FirstName = 'Test Contact 1';  
            cont1.AccountId = lstAc[0].Id ;
            cont1.Contact_Active__c = false  ;
            cont1.Contact_Inactive_Reason__c = 'Inactive Reason is required to all inactve Contacts'; 
            cont1.Contact_Department__c = 'Diabetes Adult'; 
            cont1.Contact_Primary_Specialty__c = 'ENT';
            cont1.Affiliation_To_Account__c = 'Employee';
            cont1.Primary_Job_Title_vs__c = 'Manager';
            cont1.Contact_Gender__c = 'Male';         
        lstCon.add(cont1);
        
        Contact cont2 = new Contact();
            cont2.LastName = 'TEST_controllerCreateAffiliation TestCont2';  
            cont2.FirstName = 'Test Contact 2';  
            cont2.AccountId = acc1.Id ;
            cont2.Contact_Active__c = false;
            cont2.Contact_Inactive_Reason__c = 'Inactive Reason is required to all inactve Contacts'; 
            cont2.Contact_Department__c = 'Diabetes Adult'; 
            cont2.Contact_Primary_Specialty__c = 'ENT';
            cont2.Affiliation_To_Account__c = 'Employee';
            cont2.Primary_Job_Title_vs__c = 'Manager';
            cont2.Contact_Gender__c = 'Male'; 
        lstCon.add(cont2);
        insert lstCon;
        
        List<Contact> testContacts = new List<Contact>();
            testContacts.add(cont1);
            testContacts.add(cont2);
        
        List<Account> testAccounts = new List<Account>();
            testAccounts.add(acc1);
            testAccounts.add(acc2);
        
        Company__c oCompany = new Company__c();
            oCompany.Name = 'testcomp';
            oCompany.Company_Code_Text__c = 'jp';
        insert oCompany;
        
        Business_Unit__c oBU = new Business_Unit__c();
            oBU.name = 'CRHF';
            oBU.Company__c = oCompany.id;
        insert oBU;
        
        Sub_Business_Units__c oSBU = new Sub_Business_Units__c();
            oSBU.Name = 'CRHF';
            oSBU.Business_Unit__c = oBU.id;
        insert oSBU;
        
        Therapy_Group__c oTherapyGroup = new Therapy_Group__c();
            oTherapyGroup.Name = 'therapygroup';
            oTherapyGroup.Company__c = oCompany.id;
            oTherapyGroup.Sub_Business_Unit__c = oSBU.id;
        insert oTherapyGroup;
        
        Therapy__c oTherapy = new Therapy__c();
            oTherapy.Therapy_Group__c = oTherapyGroup.id;
            oTherapy.Name = 'therapy';
            oTherapy.Therapy_Name_Hidden__c = 'therapy';
            oTherapy.Business_Unit__c = oBU.id;
            oTherapy.Sub_Business_Unit__c = oSBU.id;
            oTherapy.Active__c = true;
        insert oTherapy;

        dataMap = new Map<String, String>();
        dataMap.put('contactToId',testContacts.get(0).Id);
        dataMap.put('contactFromId',testContacts.get(1).Id);
        dataMap.put('accountToId',testAccounts.get(0).Id);
        dataMap.put('accountFromId',testAccounts.get(1).Id);


        Affiliation__c oAffiliation;
        if (tType == 'C2C'){
            oAffiliation = new Affiliation__c();
                oAffiliation.RecordTypeId = FinalConstants.recordTypeIdC2C;        
                oAffiliation.Affiliation_From_Contact__c = testContacts.get(0).Id;
                oAffiliation.Affiliation_To_Contact__c = testContacts.get(1).Id;
                oAffiliation.Affiliation_From_Account__c=testAccounts.get(0).Id;
                oAffiliation.Affiliation_To_Account__c = testAccounts.get(1).Id; 
                oAffiliation.Therapy__c = oTherapy.id;
                oAffiliation.Business_Unit__c = oBU.id; 
                oAffiliation.Affiliation_Type__c = 'A';
        }else if (tType == 'C2A'){
            oAffiliation = new Affiliation__c();
                oAffiliation.RecordTypeId = FinalConstants.recordTypeIdC2A;
                oAffiliation.Affiliation_From_Contact__c = testContacts.get(0).Id;
                oAffiliation.Affiliation_To_Account__c = testAccounts.get(1).Id;
                oAffiliation.Business_Unit__c = oBU.id; 
                oAffiliation.Affiliation_Type__c = 'B';
        }else if (tType == 'A2A'){
            oAffiliation = new Affiliation__c();
                oAffiliation.RecordTypeId = FinalConstants.recordTypeIdA2A;
                oAffiliation.Affiliation_From_Contact__c = testContacts.get(0).Id;
                oAffiliation.Affiliation_To_Account__c = testAccounts.get(1).Id;
                oAffiliation.Affiliation_Type__c = 'C';
        }
        //---------------------------------------------

        return oAffiliation;            
    }



    private static void testAffiliation(Affiliation__c curAff) {
        System.currentPageReference().getparameters().put('id',curAff.Id);   
        System.currentPageReference().getparameters().put('retURL', '/'+curAff.Id);            
        ApexPages.StandardController newSct = new ApexPages.StandardController(curAff); //set up the standardcontroller     
        controllerCreateAffiliation newController = new controllerCreateAffiliation(newSct);
        newController.getJobTitles();
        newController.getDepartments();
        newController.getBuSize=1;
        newController.RedirectToDifferntPage();
        List<SelectOption> bus = newController.BUs;
        newController.getA2ABUs();
        newController.getA2ABUGs();
        newController.getDepartment();
        newController.cancelButton();
        Boolean bResult = newController.displayBUGList;

        newController.setDataMap(dataMap);
        if (curAff.Id == null)
            newController.initInsertMode();
        else
        newController.initUpdateMode();
        newController.refreshFromAccountNames();
        newController.refreshToAccountNames();
        newController.getAffFromAccountNames();
        newController.getAffToAccountNames();        
        newController.getTherapies();

        // different cases: 
        if (curAff.Id != null){
            newcontroller.setRecordTypeId(curAff.RecordTypeId);
          
        }       
        newcontroller.saveandnew();          
        newController.save();
        newController.setEditErrorMessage();               
        curAff.Affiliation_Type__c ='C2C'; 
        newController.checkPage();
  
        
        List<SelectOption> listOfRelType = newController.relationShipTypes;
        String curSelect = newController.currentRelType;
        Boolean displayBUList = newController.displayBUList;
        Boolean displaySBUList = newController.displaySBUList;
        newController.changeChildAccount();
        newController.changeRelationshipType();
        newController.getCAA2ABUs();
        newController.getA2ASBUs();

        curAff.Affiliation_Type__c ='C2A'; 
        curAff.Affiliation_Type__c ='Referring with details'; 
        newController.affiliationType_Changed();
        Boolean bIsReferringWithDetails = newController.bIsReferringWithDetails;

    }
    
    @testSetup
    static void dataSetup(){
//		List<controllerCreateAffiliation.BusinessUnitGroup> bugs = new List<controllerCreateAffiliation.BusinessUnitGroup>();
    }

}