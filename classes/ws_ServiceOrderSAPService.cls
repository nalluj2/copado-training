/*
 * Description      : Interface class to send/receive Workorder from SFDC to Service Order in SAP and back 
 * Author           : Jesus Lozano
 * Created Date     : 20-11-2015
 * Change Log       : 
*/
global class ws_ServiceOrderSAPService{

	public static final Integer TRIM_ERROR_MSG_LENGTH = 132; 

	/*
     * Description      : WS Method to Create / Update Workorder
     * Author           : Jesus Lozano
     * Created Date     : 20-11-2015
    */
    webservice static SAPServiceOrder upsertServiceOrder(SAPServiceOrder sapServiceOrderToUpsert){

		SAPServiceOrder response;
        
        
        //CR-27432: If the Notification Type is not supported, then we just ignore the request and return a fake success message.
		Set<String> supportedTypes = ws_SharedMethods.getSupportedTypes('ServiceOrder');			
		
		if(supportedTypes.contains(sapServiceOrderToUpsert.SAP_ORDER_TYPE) == false){
			
			response = sapServiceOrderToUpsert;
			response.DISTRIBUTION_STATUS = 'TRUE';	
			response.DIST_ERROR_DETAILS  = 'Order Type not supported. Action skipped.';		
			return response;				
		}	               
                
        // SFDC Id
        Id workOrderId;
        
        // Get current status of the Order. If the status is Job Completed or On Site, then ignore the upsert call.        
        List<SVMXC__Service_Order__c> serviceOrder = [Select SVMXC__Order_Status__c from SVMXC__Service_Order__c where SVMX_SAP_Service_Order_No__c = :sapServiceOrderToUpsert.SAP_ORDER_NUMBER];        
        if(serviceOrder.size() == 1 && 
        	(serviceOrder[0].SVMXC__Order_Status__c == bl_SVMXC_ServiceOrder_Trigger.SERVICEORDER_COMPLETED || serviceOrder[0].SVMXC__Order_Status__c == bl_SVMXC_ServiceOrder_Trigger.SERVICEORDER_ON_SITE)){
        	
        	response = sapServiceOrderToUpsert;
        	response.DISTRIBUTION_STATUS = 'TRUE';
        	workOrderId = serviceOrder[0].Id;
        	        	
        }else{

	        Savepoint sp = Database.setSavePoint();      
	        
	        // Inform the trigger logic that the Interface is running
	        bl_SVMXC_ServiceOrder_Trigger.isInterfaceRunning = true;
	                
	        // Wrap in try catch to construct error message in case of Exception
	        try{
	        	        	 
	        	// Validate if we need to update an Ad-Hoc created Service Order or create a new Service Order
		        /*List<SVMXC__Service_Order__c> lstServiceOrder_AdHoc = 
		        	[
		        		SELECT Id 
		        		FROM SVMXC__Service_Order__c 
		        		WHERE 
		        			SVMXC__Company__r.SAP_ID__c = :sapServiceOrderToUpsert.SOLD_TO_ACCOUNT	// Same Account
		        			AND SVMXC__Component__r.SVMX_SAP_Equipment_ID__c = :sapServiceOrderToUpsert.SAP_EQUIPMENT_NUMBER // Same Installed Product
		        			AND SVMXC__Group_Member__r.SVMX_SAP_Employee_Id__c = :sapServiceOrderToUpsert.TECHNICIAN	// Same Technician
		        			AND SVMXC__Purpose_of_Visit__c != 'Placeholder'AND SVMX_SAP_Service_Order_No__c = null // Indicator of Adhoc waiting to be match
		        	];
				*/
				
	            // Work Order
	            SVMXC__Service_Order__c workOrder;
				//if (lstServiceOrder_AdHoc.size() > 0){
				if(sapServiceOrderToUpsert.SAP_SHORT_TEXT != null && sapServiceOrderToUpsert.SAP_SHORT_TEXT.startsWith('WO-')){
					
					List<SVMXC__Service_Order__c> lstServiceOrder_AdHoc = [SELECT Id FROM SVMXC__Service_Order__c WHERE Name = :sapServiceOrderToUpsert.SAP_SHORT_TEXT];
								
					//if(lstServiceOrder_AdHoc.size() > 1) throw new ws_Exception('Multiple Ad-Hoc Service Orders have been identified for the same configuration. Please contact your Administrator to verify the data.');
					if(lstServiceOrder_AdHoc.size() == 0) throw new ws_Exception('No Ad-Hoc Work Order was found for reference: ' + sapServiceOrderToUpsert.SAP_SHORT_TEXT);
					
					// Ad-Hoc Service Order is found - update the record with the SAP ID
					workOrder = lstServiceOrder_AdHoc[0];
					workOrderId = workOrder.Id;
					workOrder.SVMX_SAP_Service_Order_No__c = sapServiceOrderToUpsert.SAP_ORDER_NUMBER;
					
					// Map the Service Notification (Case) that has been created for this Work Order in SAP
					if(sapServiceOrderToUpsert.SAP_NOTIFICATION_NUMBER != null){
    		
				    	List<Case> serviceNotification = [Select Id, GCH_Number__c from Case where SVMX_SAP_Notification_Number__c = :sapServiceOrderToUpsert.SAP_NOTIFICATION_NUMBER];    	
				    	if(serviceNotification.size() == 0) throw new ws_Exception('No Service Notification was found for SAP Id: ' + sapServiceOrderToUpsert.SAP_NOTIFICATION_NUMBER);  	
				    	if(serviceNotification.size() == 2) throw new ws_Exception('Multiple Service Notifications where found for SAP Id: ' + sapServiceOrderToUpsert.SAP_NOTIFICATION_NUMBER);
				    	
				    	workOrder.SVMXC__Case__c = serviceNotification[0].Id;
				    	workOrder.GCH_Number__c = serviceNotification[0].GCH_Number__c;
				    	workOrder.SVMX_SAP_Notification_Number__c = sapServiceOrderToUpsert.SAP_NOTIFICATION_NUMBER;
			    	}
					
					// Because in SAP the first Operation is automatically created, we need to link it to the existing default operation in SMAX side.
					if(sapServiceOrderToUpsert.SERVICEORDER_OPERATIONS != null && sapServiceOrderToUpsert.SERVICEORDER_OPERATIONS.size() > 0){
						
						List<SVMXC_Work_Order_Operation__c> woOperations = [Select Id, SVMXC_SAP_Id__c, SVMXC_Confirmation_Number__c, Unique_Key__c from SVMXC_Work_Order_Operation__c where SVMXC_Work_Order__c = :workOrder.Id ];
						
						if( woOperations.size() > 0){
							
							SAPServiceOrderOperation sapOperation = sapServiceOrderToUpsert.SERVICEORDER_OPERATIONS[0];
							
							SVMXC_Work_Order_Operation__c defOperation = woOperations[0];
							defOperation.SVMXC_SAP_Id__c = sapOperation.SAP_ID;
							defOperation.SVMXC_Confirmation_Number__c = sapOperation.CONFIRMATION_NUMBER;
							defOperation.Unique_Key__c = workOrder.Id + ':' + sapOperation.SAP_ID;
							defOperation.SVMXC_Control_Key__c = sapOperation.CONTROL_KEY;
							
							update defOperation;
						}
					}
					
					update workOrder;
					
					// Adhoc dummy Damage / Cause codes
					List<SVMX_Fault_Code__c> adhocCodes = [Select Id from SVMX_Fault_Code__c where SVMX_Work_Order__c = :workOrder.Id AND SVMX_Damage_Code__r.Damage_Code_Grouping_Lookup__r.Name = 'ADHOC'];					
					if(adhocCodes.size() > 0) delete adhocCodes;
					
					// Copy the Damage / Cause codes from the Notification
					bl_ServiceOrderSAPService.createServiceOrderSAPDamageCauses(workorder);	
					
				}else{
					
	            	workOrder = bl_ServiceOrderSAPService.mapSAPServiceOrderToWorkorder(sapServiceOrderToUpsert);
					            
		            List<Database.UpsertResult> upsertResult = Database.upsert(new List<SVMXC__Service_Order__c>{workOrder}, SVMXC__Service_Order__c.fields.SVMX_SAP_Service_Order_No__c);                    
	
		            workOrderId = workOrder.Id;
		            
		            // Operations            
		            List<SVMXC_Work_Order_Operation__c> operations = bl_ServiceOrderSAPService.mapSAPOperationsToOperations(workOrder, sapServiceOrderToUpsert.SERVICEORDER_OPERATIONS);
		            if(operations.size() > 0) upsert operations Unique_Key__c;
		            
		            // Check if default Operation is needed            
		            if(operations.size() == 0) bl_ServiceOrderSAPService.createDefaultOperation(workOrder.Id);
		            
		            // Components
		            //List<SVMXC__Service_Order_Line__c> components = bl_ServiceOrderSAPService.mapSAPComponentsToWorkDetails(workOrder, operations, sapServiceOrderToUpsert.SERVICEORDER_COMPONENTS);
		            //if(components.size() > 0) upsert components Unique_Key__c;
		                        
		            // If it is a new Service Order, check if the Notification has read-only Damage / Cause codes and copy them over 	            
		            if(upsertResult[0].isCreated() == true) bl_ServiceOrderSAPService.createServiceOrderSAPDamageCauses(workorder);	
				}            
	                       
	            response = bl_ServiceOrderSAPService.mapWorkOrderToSAPServiceOrder(bl_ServiceOrderSAPService.getServiceOrderInfo(workOrder.Id), false);                       
	            response.DISTRIBUTION_STATUS = 'TRUE';  
	
	        } catch (Exception ex){
	            
	            System.debug(ex);
	            System.debug('STACK TRACE - ' + ex.getStackTraceString());
	            System.debug('MESSAGE     - ' + ex.getMessage());
	            
	            Database.rollback(sp);
	            
	            // Construct error message 
	            String errMsg = (ex.getMessage()).left(TRIM_ERROR_MSG_LENGTH);
	            
	            // Return original Object including status and err message            
	            response = sapServiceOrderToUpsert;           
	            response.DISTRIBUTION_STATUS = 'FALSE';
	            response.DIST_ERROR_DETAILS = errMsg;
	            
	            if(Test.isRunningTest()) throw ex;
	        }   
        }

		ut_Log.logOutboundMessage(workOrderId, sapServiceOrderToUpsert.SAP_ORDER_NUMBER, JSON.serializePretty(sapServiceOrderToUpsert), JSON.serializePretty(response), response.DISTRIBUTION_STATUS, 'upsertServiceOrder', 'ServiceOrder');
       
        return response;
    }          
        
    /*
     * Description      : WS Method to Retrieve Service Order
     * Author           : Jesus Lozano
     * Created Date     : 24-11-2015
    */ 
    webservice static SAPServiceOrder getServiceOrderUpdate(String workorderId){
        
        SAPServiceOrder response;
        
        SVMXC__Service_Order__c workOrder;
        
        try{                                  
            
            // Query for Work Order
            workOrder = bl_ServiceOrderSAPService.getServiceOrderInfo(workOrderId);
                        
            // Map response message
            response = bl_ServiceOrderSAPService.mapWorkOrderToSAPServiceOrder(workOrder, false);
            response.DISTRIBUTION_STATUS = 'TRUE';  

        } catch (Exception ex){
            
            // Construct error message
            String errMsg = (ex.getMessage()).left(TRIM_ERROR_MSG_LENGTH);
            
            // As the error is thrown, need to construct a new Service Order to populate the response message
            response = new SAPServiceOrder();        	        	
        	response.DISTRIBUTION_STATUS = 'FALSE';
        	response.DIST_ERROR_DETAILS = errMsg; 
        	
        	if(Test.isRunningTest()) throw ex;    	  
        } 
        
        String sapID;
        if(workOrder != null) sapID = workOrder.SVMX_SAP_Service_Order_No__c;
        
        ut_Log.logOutboundMessage(workorderId, sapID, workorderId, JSON.serializePretty(response), response.DISTRIBUTION_STATUS, 'getServiceOrderUpdate', 'ServiceOrder');
               
        return response;        
    }
    
    /*
     * Description      : WS Method to receive the Service Order Update acknoledgement
     * Author           : Jesus Lozano
     * Created Date     : 24-11-2015
    */
    webservice static SAPServiceOrderAck updateServiceOrderAcknowledgement(SAPServiceOrderAck sOrderAck){
    	    	    	
    	SVMXC__Service_Order__c workOrder; 
    	   	
    	SAPServiceOrderAck response = new SAPServiceOrderAck();
        	response.WMTRANSACTION = sOrderAck.WMTRANSACTION;
        	response.TIMESTAMP = sOrderAck.TIMESTAMP;
            response.SFDC_ID = sOrderAck.SFDC_ID;
            response.SAP_ID = sOrderAck.SAP_ID;

    	Savepoint sp = Database.setSavepoint();
    	
	    try{
    	
            clsUtil.debug('updateServiceOrderAcknowledgement - sOrderAck : ' + sOrderAck);        
			
			//In case of partial success we need to update the child components with their SAP equivalent. Otherwise during retries duplicates will be created
			bl_ServiceOrderSAPService.updateChildComponentsWithAcknowledgement(sOrderAck);

	    	if(sOrderAck.DISTRIBUTION_STATUS == 'TRUE'){
	    		
                // SUCCESS	    			    		
	    		workOrder = bl_ServiceOrderSAPService.getServiceOrderInfo(sOrderAck.SFDC_ID);	    		
	    		
	    		if(workOrder.SVMXC__Order_Status__c == bl_SVMXC_ServiceOrder_Trigger.SERVICEORDER_COMPLETED && workOrder.Ready_To_Complete__c == false){
	    			
	    			//If the status is Completed but there is a pending update, we need to process the update before we send the ServiceOrderComplete call 
                    List<NotificationSAPLog__c> lstNotificationSAPLog_Pending = ut_Log.getNotificationSAPLog(workOrder.Id, 'PENDING');
      
                    if(lstNotificationSAPLog_Pending.size() == 0){
	    			
		    			SVMXC__Service_Order__c toComplete = new SVMXC__Service_Order__c(Id = workOrder.Id);
		    			toComplete.Ready_To_Complete__c = true;
		    			update toComplete;
                    }
	    		}                    

	    	}else{

                // ERROR OCCURED
                if(sOrderAck.PROCESS_STATUS != null){
                	
                	workOrder = bl_ServiceOrderSAPService.getServiceOrderInfo(sOrderAck.SFDC_ID);
                	workOrder.SVMXC_Process_Status__c = sOrderAck.PROCESS_STATUS;
                	update workOrder;
                }
				
            }
	    	
	    	response.DISTRIBUTION_STATUS = 'TRUE';

    	}catch (Exception ex){
	            
            Database.rollback(sp);
            
            // Construct error message
            String errMsg = (ex.getMessage()).left(TRIM_ERROR_MSG_LENGTH);
            
            //TODO: Update the Sync Notification record with the error occurred. Special case to take into account for the lock mechanism
                 	        	
        	response.DISTRIBUTION_STATUS = 'FALSE';
        	SAPErrorMessage msg = new SAPErrorMessage();
        	msg.ERROR_MESSAGE = errMsg;
        	response.DIST_ERROR_DETAILS = new List<SAPErrorMessage>{msg};   
        	
        	if(Test.isRunningTest()) throw ex;  	  
        }
    	
    	String internalId;
        if(workOrder != null) internalId = workOrder.Id;

        ut_Log.logOutboundMessage(sOrderAck.SFDC_ID, sOrderAck.SAP_ID, JSON.serializePretty(sOrderAck), JSON.serializePretty(response), response.DISTRIBUTION_STATUS, 'updateServiceOrderAcknowledgement', 'ServiceOrder');

        // Update the Notification SAP Log record with the Acknowledgement response
        ut_Log.updateNotificationSAPLogStatus(sOrderAck.SFDC_ID, sOrderAck.DISTRIBUTION_STATUS, JSON.serializePretty(sOrderAck), JSON.serializePretty(response));
            	
    	return response;
    }
    
    /*
     * Description      : WS Method to Retrieve Service Order in its Complete status
     * Author           : Jesus Lozano
     * Created Date     : 24-11-2015
    */
    webservice static SAPServiceOrder getServiceOrderComplete(String workorderId){
        
        SAPServiceOrder response;
        
        SVMXC__Service_Order__c workOrder;
        
        try{
        	                        
            // Query for Work Order
            workOrder = bl_ServiceOrderSAPService.getServiceOrderInfo(workOrderId);
                       
            // Map response message
            response = bl_ServiceOrderSAPService.mapWorkOrderToSAPServiceOrder(workOrder, true); 
            response.DISTRIBUTION_STATUS ='TRUE';
            
        } catch (Exception ex){
            
            // Construct error message
            String errMsg = (ex.getMessage()).left(TRIM_ERROR_MSG_LENGTH);
                        
            response = new SAPServiceOrder();                         
            response.DISTRIBUTION_STATUS ='FALSE';
            response.DIST_ERROR_DETAILS = errMsg;
              
            if(Test.isRunningTest()) throw ex;
        } 
        
        String externalId;
        if(workOrder != null){
        	externalId = workOrder.SVMX_SAP_Service_Order_No__c;
        }
        
        ut_Log.logOutboundMessage(workorderId, externalId, workorderId, JSON.serializePretty(response), response.DISTRIBUTION_STATUS, 'getServiceOrderComplete', 'ServiceOrder');
           
        return response;       
    }

         	
	/*
     * Description      : Structure for Service Order
    */     
    global class SAPServiceOrder{
        
        webservice String DISTRIBUTION_STATUS;
        webservice String DIST_ERROR_DETAILS;   
        
        webservice String SAP_IDOC_NUMBER;
        webservice String WM_TRANSACTION_NUMBER;    
                
		webservice String SFDC_ID;
				
		webservice String SAP_ORDER_TYPE;
		webservice String SAP_ORDER_NUMBER;
		webservice String SAP_SHORT_TEXT;
		webservice String PROBLEM_DESCRIPTION;
		webservice String STATUS;
		webservice String ORDER_SUBSTATUS;
		webservice String SOLD_TO_ACCOUNT;
		webservice String SOLD_TO_LOCATION;
		webservice String ORDER_LOCATION; 
		webservice String CUSTOMER_PO;
		webservice String PO_DATE;
		webservice String SERVICE_CONTRACT_DOC;
		webservice String SERVICE_CONTRACT_ITEM;
		webservice String BILLING_TYPE;
		webservice String MAIN_WORK_CENTER;
		webservice String PLANT;
		webservice String MNWRKCNTR_SHORT_TEXT;
		webservice String TECHNICIAN;
		webservice String TECHNICIAN_TEXT;
		webservice String SALESFORCE_USER;
		webservice String SAP_NOTIFICATION_NUMBER;
		webservice String ACT_TYPE;
		webservice String PREFERRED_START_TIME;
		webservice String PREFERRED_END_TIME;		
		webservice String PRIORITY;
		webservice String SAP_EQUIPMENT_NUMBER;
		webservice String SAP_EQUIPMENT_DESCRIPTION;
		webservice String SERIALLOT_NUMBER;
		webservice String MATERIAL;
		webservice String MATERIAL_DESC;
		webservice String MAINT_PLAN;
		webservice String CALL_NUMBER;
		webservice String MAIN_ITEM;
		webservice String COMPL_DATE;
		webservice String LAST_ORDER;
		webservice String SUBJECT;
		webservice String SUBJECT_CODE;
		webservice String SUBJECT_DESCRIPTION;
		
		webservice String PROCESS_STATUS;
		
        webservice List<SAPServiceOrderOperation> SERVICEORDER_OPERATIONS;
        webservice List<SAPServiceOrderComponent> SERVICEORDER_COMPONENTS;
        webservice List<SAPServiceOrderRemovedComponent> SERVICEORDER_REMOVED_COMPONENTS;        
        webservice List<SAPServiceOrderTimeConfirmation> SERVICEORDER_TIME_CONFIRMATIONS;
        webservice List<SAPServiceOrderGoodsMovement> SERVICEORDER_GOODS_MOVEMENTS;
        webservice List<SAPServiceOrderItem> SERVICEORDER_ITEMS;
        
        webservice List<SAPServiceOrderCause> SERVICEORDER_CAUSES;
        webservice List<SAPServiceOrderTestEquipmentUsed> SERVICEORDER_TEST_EQUIPMENT_USED;        
    }
    
    global class SAPServiceOrderTestEquipmentUsed{
        
        webservice String SAP_TEST_EQUIPMENT_ID;
        
    }
    
    /*
     * Description      : Structure for Service Order Operation
    */ 
    global class SAPServiceOrderOperation{
        
        webservice String SFDC_ID;
		webservice String SAP_ID;
		
		webservice String SAP_WORK_CENTER;
		webservice String PLANT;
		webservice String CONTROL_KEY;
		webservice String STTEXTK;
		webservice String SYSTEM_CONDITION;
		webservice String OPERATION_DESCRIPTION;
		webservice String OPERATION_DESCRIPTION_LONG;
		webservice String QUANTITY;
		webservice String PLANNED_WORK;
		webservice String WORK_UNIT;
		webservice String NUM_EMPLOYEES;
		webservice String DURATION;
		webservice String DURATION_UNIT;
		webservice String LINE_TYPE;
		webservice String CONFIRMATION_NUMBER;
		webservice String EARL_START_DATE;
		webservice String EARL_END_DATE;
		webservice String EARL_START_TIME;
		webservice String EARL_END_TIME;
		webservice String LINE_STATUS;		
		webservice String ITEM_DESCRIPTION;//????
		webservice String LINE_QUANTITY;//????
		webservice String SPECIAL_STOCK;//????
		webservice String RECIPIENT;//????
		webservice String UNLOADING_POINT;//????
		webservice String REQUIREMENT_DATE;//????
		webservice String DELIVERY_DATE;//????
		webservice String RECEIVED;//????  
		webservice String SCHEDULED_START_DATE;
		webservice String SCHEDULED_END_DATE;
		webservice String SCHEDULED_START_TIME;
		webservice String SCHEDULED_END_TIME;     
    }
    
    /*
     * Description      : Structure for Structure for Service Order Component
    */ 
    global class SAPServiceOrderComponent{
        
		webservice String SFDC_ID;		
		webservice String SAP_ID;
		
		webservice String OPERATION_NUMBER;
		webservice String OPERATION_SFDC_ID;
		webservice String PART;
		webservice String PART_DESCRIPTION_SHORT;
		webservice String PART_DESCRIPTION_LONG;		
		webservice String LINE_QUANTITY;
		webservice String UNIT_OF_MEASURE;
		webservice String ITEM_CATEGORY;		
		webservice String LOCATION_STORAGE;
		webservice String LOCATION_PLANT;
		webservice String SERIALLOT_NUMBER;
		webservice String RECIPIENT;
		webservice String UNLOADING_POINT;
		webservice String REQUIREMENT_DATE;
		webservice String DELIVERY_DATE;
		webservice String RECEIVED;
		webservice String RESERVATION_NUM;
		webservice String RESERVATION_ITEM_NUM;	
		webservice String WARRANTY_APPROVAL;
        webservice String REMOVED_PART_NUMBER;
        webservice String REMOVED_PART_SERIAL_NUMBER;
        webservice String RETURN_PARTS;
        webservice String PART_STATUS;	       
    }
    
    global class SAPServiceOrderRemovedComponent{

        webservice String SFDC_ID;      
        webservice String SAP_ID;

        webservice String ITEM_NUMBER;
        webservice String REMOVED_PART_NUMBER;
        webservice String REMOVED_PART_SERIAL_NUMBER;
        webservice String PART_DESCRIPTION_SHORT;
        webservice String PART_DESCRIPTION_LONG;
        webservice String PART_STATUS;
        webservice String UNIT_OF_MEASURE;
        webservice String RETURN_PARTS;
        webservice String LINE_QUANTITY;
        webservice String WARRANTY_APPROVAL;
        webservice String SAP_PARENT_EQUIP_NUMBER;        
    }
        
    /*
     * Description      : Structure for Structure for Service Order Time Confirmation
    */     
    global class SAPServiceOrderTimeConfirmation{
        
        webservice String SFDC_ID;
		webservice String SAP_ID;
		
		webservice String AIND;
		webservice String TECHNICIAN;		
		webservice String SAP_WORK_CENTER;
		webservice String LOCATION_PLANT;
		webservice String LINE_QUANTITY;
		webservice String WORK_UNIT;
		webservice String LINE_TYPE;
		webservice String OPERATION_NUMBER;
		webservice String CONFIRMATION_NUMBER;
		webservice String FINAL_CONFIRMATION;
		webservice String NO_REMAINING_WORK;
		webservice String POSTING_DATE;
		webservice String REMAINING_WORK;
		webservice String UNIT;
		webservice String WORK_START_DATE;
		webservice String WORK_START_TIME;
		webservice String WORK_FINISHED_DATE;
		webservice String WORK_FINISHED_TIME;
		webservice String CONFIRMATION_TEXT;
    }
	
	/*
     * Description      : Structure for Structure for Service Order Goods Movement
    */ 
    global class SAPServiceOrderGoodsMovement{
        
        webservice String SFDC_ID;
		webservice String SAP_ID;
		
		webservice String OPERATION_NUMBER;
		webservice String PART;
		webservice String PART_DESCRIPTION_LONG;
		webservice String LOCATION_STORAGE;
		webservice String LOCATION_PLANT;
		webservice String SERIALLOT_NUMBER;
		webservice String LINE_QUANTITY;
		webservice String UNIT_OF_MEASURE;
		webservice String RESERVATION_NUM;
		webservice String RESERVATION_ITEM_NUM;
		webservice String MOVEMENT_TYPE;  
		webservice String WARRANTY_APPROVAL;      
    }
    
    /*
     * Description      : Structure for Structure for Service Notification Item
    */ 
    global class SAPServiceOrderItem{
    	
    	webservice String SFDC_ID;
    	webservice String SAP_ID;
    	
    	webservice String CODE_GROUP;
    	webservice String PART_OF_OBJECT;
    	webservice String OBJECT_PART_TEXT;
    	webservice String CODE_GROUP_PROBLEM;
    	webservice String PROBLEM_OR_DAMAGE_CODE;
    	webservice String DAMAGE_DESCRIPTION;
    	webservice String DAMAGE_TEXT;   	
    }
    
	/*
     * Description      : Structure for Structure for Service Notification Cause
    */ 
    global class SAPServiceOrderCause{
    	
    	webservice String SFDC_ID;
    	webservice String SAP_ID;
    	
    	webservice String ITEM_SAP_ID;
    	webservice String ITEM_SFDC_ID;
    	webservice String CAUSE_GROUP;
    	webservice String CAUSE_CODE;
    	webservice String CAUSE_TEXT;
    	webservice String DESCRIPTION;    	
    }
    
    /*
     * Description      : Structure for ServiceOrder Acknowledgement to update x-ref Ids
    */     
    global class SAPServiceOrderAck{
        
        webservice String SAP_ID;
        webservice String SFDC_ID;
        
        webservice String WMTRANSACTION;
		webservice String TIMESTAMP;        
        webservice String DISTRIBUTION_STATUS;
        webservice String PROCESS_STATUS;
        webservice List<SAPErrorMessage> DIST_ERROR_DETAILS;
        
        webservice List<SAPServiceOrderOperationAck> SERVICEORDER_OPERATIONS;
        webservice List<SAPServiceOrderComponentAck> SERVICEORDER_COMPONENTS;        
        webservice List<SAPServiceOrderTimeConfirmationAck> SERVICEORDER_TIME_CONFIRMATIONS;
        webservice List<SAPServiceOrderGoodsMovementAck> SERVICEORDER_GOODS_MOVEMENTS;
        webservice List<SAPServiceOrderItemAck> SERVICEORDER_ITEMS;
        webservice List<SAPServiceOrderCauseAck> SERVICEORDER_CAUSES;
        webservice List<SAPServiceOrderRemovedComponent> SERVICEORDER_REMOVED_COMPONENTS;
    }
    
    /*
     * Description      : Structure for ServiceOrder error message
    */
    global class SAPErrorMessage{
    	
    	webservice String ERROR_TYPE;
        webservice String ERROR_ID;
        webservice String ERROR_NUMBER;
		webservice String ERROR_MESSAGE;
    }
    
    /*
     * Description      : Structure for ServiceOrder Operation Acknowledgement to update x-ref Ids
    */ 
    global class SAPServiceOrderOperationAck{
        
        webservice String SAP_ID;
        webservice String SFDC_ID;
        webservice String CONFIRMATION_NUMBER;        
    }
    
    /*
     * Description      : Structure for ServiceOrder Component Acknowledgement to update x-ref Ids
    */ 
    global class SAPServiceOrderComponentAck{
        
        webservice String SAP_ID;
        webservice String SFDC_ID;       
        webservice String RESERVATION_NUM;
		webservice String RESERVATION_ITEM_NUM;	    
    }	
            
    /*
     * Description      : Structure for ServiceOrder Tiem Confirmation Acknowledgement to update x-ref Ids
    */
    global class SAPServiceOrderTimeConfirmationAck{
    	
    	webservice String SAP_ID;
        webservice String SFDC_ID;
    }
    
    /*
     * Description      : Structure for ServiceOrder Goods Movement Acknowledgement to update x-ref Ids
    */
    global class SAPServiceOrderGoodsMovementAck{
    	
    	webservice String SAP_ID;
        webservice String SFDC_ID;
    }
    
    /*
     * Description      : Structure for ServiceOrder Item Acknowledgement to update x-ref Ids
    */
    global class SAPServiceOrderItemAck{
    	
    	webservice String SAP_ID;
        webservice String SFDC_ID;
    }
    
    /*
     * Description      : Structure for ServiceOrder Cause Acknowledgement to update x-ref Ids
    */
    global class SAPServiceOrderCauseAck{
    	
    	webservice String SAP_ID;
        webservice String SFDC_ID;
    }
}