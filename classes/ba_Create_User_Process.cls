//We decided to use a Stateful Batch to perform this logic because of the MIXED_DML exception.
//In this way we can have communication between up to 3 different execution contexts.
global with sharing class ba_Create_User_Process implements Database.Batchable<sObject>, Database.Stateful{
 	
 	//Scope record Ids
 	public List<Id> createUserRequestListIds {get; set;}
 	
 	//Stateful variables
 	private List<Create_User_Request__c> requests;
 	private Map<String, Create_User_Request__c> reqByUsername;
 	private Map<String,UserRole> mapUserRoles;
 	private Map<String,Profile> mapProfiles;
 	private Map<Id,User> usersById;
 	 	
	global Database.QueryLocator start(Database.BatchableContext BC){
		
		reqByUsername = new Map<String, Create_User_Request__c>();
		requests = new List<Create_User_Request__c>();
		usersById = new Map<Id, User>();
			
		mapUserRoles = new Map<String,UserRole>();
		mapProfiles = new Map<String,Profile>();
		
		for(UserRole ur : [select Id, name from UserRole]){
			mapUserRoles.put(ur.name,ur);
		}		
		for (Profile p : [select Id, name from Profile]){
			mapProfiles.put(p.name,p);
		}
		
		String tSOQL = '';
			tSOQL += 'SELECT Role__c, Profile__c, firstname__c,Lastname__c,alias__c, Federation_ID__c, username__c, Country__r.name, Email__c';
			tSOQL += ', Content_User__c, Marketing_User__c,Cost_Center__c, Created_User__c, CurrencyIsoCode, BI_Currency__c,District_Manager__c';
			tSOQL += ', Region__c, Region__r.Name, Peoplesoft_ID__c, mmx__c, Territory_UID__c,Nickname__c, User_SAP_Id__c';
			tSOQL += ', Geographical_Region_vs__c, Geographical_Subregion__c, Geographical_Country_vs__c, Company_Code__c, User_Business_Unit_vs__c, Primary_sBU__c, Jobtitle_vs__c';
			tSOQL += ', UserCreationError__c, Mobile_Apps__c';
			tSOQL += ', (';
				tSOQL += 'SELECT Sub_Business_Unit__c, Sub_Business_Unit__r.Name, Primary__c, Sub_Business_Unit__r.Business_Unit__c, Business_Unit_Name__c';
				tSOQL += ' FROM User_Request_BUs__r';
				tSOQL += ' ORDER BY Business_Unit_Name__c,Sub_Business_Unit__r.Name';
			tSOQL += ')';
			tSOQL += ' FROM Create_User_Request__c';
			tSOQL += ' WHERE Id in: createUserRequestListIds';	
			
		return Database.getQueryLocator(tSOQL);
   	}
   
	global void execute(Database.BatchableContext BC, List<sObject> scope){
		
		List<Create_User_Request__c> createUserRequestList = (List<Create_User_Request__c>) scope;
		
		Map<String, User> mapUserName_User = new Map<String, User>();
		Set<String> setUserName = new Set<String>();
		for (Create_User_Request__c oCreateUserRequest : createUserRequestList){
			setUserName.add(oCreateUserRequest.username__c);
		}
		String tSOQL = 'SELECT Id, Username FROM USER WHERE Username = :setUserName';
		for (User oUser : Database.query(tSOQL)){
			mapUserName_User.put(oUser.Username, oUser);
		}

		List<User> lstUser_Insert = new List<User>();
		List<User> lstUser_Update = new List<User>();
		for (Create_User_Request__c cur : createUserRequestList){
						
			User oUser;

			if (mapUserName_User.containsKey(cur.Username__c)){
				oUser = mapUserName_User.get(cur.Username__c);
			}else{
				oUser = new User();
			}

				oUser.isActive = true;
				oUser.User_Status__c = 'Current';			
				if (!Test.isRunningTest() && cur.Role__c != null) oUser.UserRoleId = mapUserRoles.get(cur.Role__c).id;
				oUser.ProfileId = mapProfiles.get(cur.Profile__c).id;
				oUser.FirstName = cur.firstname__c;
				oUser.LastName = cur.Lastname__c;

				String tAlias = clsUtil.isNull(cur.alias__c, '');
				oUser.alias = tAlias.left(8);
				oUser.CommunityNickname = cur.Nickname__c;
				oUser.Alias_unique__c = tAlias;

				oUser.Region_vs__c = cur.Geographical_Region_vs__c;
				oUser.Sub_Region__c = cur.Geographical_Subregion__c;
				oUser.Country_vs__c = cur.Geographical_Country_vs__c;
				oUser.Country = cur.Geographical_Country_vs__c.toUpperCase();
				oUser.CountryOR__c = cur.Geographical_Country_vs__c.toUpperCase();

				oUser.Company_Code_Text__c = cur.Company_Code__c;
				oUser.User_Business_Unit_vs__c = cur.User_Business_Unit_vs__c;
				oUser.Primary_sBU__c = cur.Primary_sBU__c;
				oUser.Job_Title_vs__c = cur.Jobtitle_vs__c;

				oUser.Username = cur.Username__c;
				oUser.Email = cur.Email__c;
				oUser.Peoplesoft_ID__c = cur.Peoplesoft_ID__c;
				oUser.Territory_UID__c = cur.Territory_UID__c==null ? null : String.valueOf(cur.Territory_UID__c.intValue());
				oUser.DefaultCurrencyIsoCode = cur.CurrencyIsoCode;
				oUser.BI_Currency__c = cur.BI_Currency__c;
				oUser.CostCenter__c = cur.Cost_Center__c;
				oUser.TimeZoneSidKey = 'Europe/Amsterdam'; 
				oUser.LocaleSidKey = 'en_US';
				oUser.EmailEncodingKey =  'ISO-8859-1';
				oUser.LanguageLocaleKey = 'en_US';
				oUser.UserPermissionsMarketingUser = cur.Marketing_User__c;
				oUser.UserPermissionsSFContentUser = cur.Content_User__c;
				oUser.MMX__c = cur.MMX__c;
				oUser.SAP_ID__c = cur.User_SAP_Id__c;
				oUser.ManagerId = cur.District_Manager__c;

				if (String.isBlank(cur.Federation_ID__c)){
					oUser.FederationIdentifier = tAlias;
				}else{
					oUser.FederationIdentifier = cur.Federation_ID__c;
				}

			if (mapUserName_User.containsKey(oUser.Username)){
				lstUser_Update.add(oUser);
			}else{
				lstUser_Insert.add(oUser);
			}
			
			reqByUsername.put(oUser.Username, cur);		
			requests.add(cur);
		}
				 

		// Execute INSERT
		if (lstUser_Insert.size() > 0){

			List<Database.SaveResult> lstSaveResult_Insert = Database.insert(lstUser_Insert, false);

			Integer iCounter = 0;
			for (Database.SaveResult oSaveResult : lstSaveResult_Insert){
				
				User oUser = lstUser_Insert[iCounter];
				Create_User_Request__c oCreateUserRequest = reqByUsername.get(oUser.Username);
				
				if(oSaveResult.isSuccess()){				
					oCreateUserRequest.Created_User__c = oUser.id;
					oCreateUserRequest.UserCreationError__c = null;
					oCreateUserRequest.Status__c = 'Completed';	
					oCreateUserRequest.Sub_Status__c = 'Solved (Permanently)';			
					usersById.put(oUser.id, oUser);		
				}else{
					
					String errorDesc='';
					for(Database.Error error : oSaveResult.getErrors()){
						errorDesc+= error.getMessage()+'; ';
					}
					oCreateUserRequest.UserCreationError__c = errorDesc;
					oCreateUserRequest.Status__c = 'Approved';
				}
				
				iCounter++;
			}

		}

		// Execute UPDATE
		if (lstUser_Update.size() > 0){

			List<Database.SaveResult> lstSaveResult_Update = Database.update(lstUser_Update, false);

			Integer iCounter = 0;
			for (Database.SaveResult oSaveResult : lstSaveResult_Update){
				
				User oUser = lstUser_Update[iCounter];
				Create_User_Request__c oCreateUserRequest = reqByUsername.get(oUser.Username);
				
				if(oSaveResult.isSuccess()){				
					oCreateUserRequest.Created_User__c = oUser.id;
					oCreateUserRequest.UserCreationError__c = null;
					oCreateUserRequest.Status__c = 'Completed';	
					oCreateUserRequest.Sub_Status__c = 'Solved (Permanently)';			
					usersById.put(oUser.id, oUser);		
				}else{
					
					String errorDesc='';
					for(Database.Error error : oSaveResult.getErrors()){
						errorDesc+= error.getMessage()+'; ';
					}
					oCreateUserRequest.UserCreationError__c = errorDesc;
					oCreateUserRequest.Status__c = 'Approved';
				}
				
				iCounter++;
			}

		}

	}
	
	global void finish(Database.BatchableContext BC){
		
		update requests;
				
		Map<Id,Company__c> regionBUs = new Map<Id, Company__c>([Select Id, (Select Id from Business_Units__r) from Company__c]);
		
		List<User_Business_Unit__c> userBUs = new List<User_Business_Unit__c>();
		
		List<User_Business_Unit__c> existingUBUs = [Select Id from User_Business_Unit__c where User__c IN:usersById.keySet()];
		if(!existingUBUs.isEmpty()) delete existingUBUs;
		
		String errorsToEmail='';
		String successfulUsers='';
						
		//Add Business Unit information
		for(Create_User_Request__c req : requests){
			
			if ( (req.Created_User__c!=null) && (usersById.containsKey(req.created_User__c)) ){

				if(req.User_Request_BUs__r.size() > 0){
					
					User oUser = usersById.get(req.created_User__c);
					oUser.User_Sub_Bus__c = null;
										
					Set<String> addedBUs = new Set<String>();
									
					for (User_Request_BU__c reqBU : req.User_Request_BUs__r){
						User_Business_Unit__c userBU = new User_Business_Unit__c();
							userBU.User__c = req.created_User__c;
							userBU.Sub_Business_Unit__c = reqBU.Sub_Business_Unit__c;
							userBU.Primary__c = reqBU.Primary__c;
						
						if(oUser.User_Sub_Bus__c==null) oUser.User_Sub_Bus__c=reqBU.Sub_Business_Unit__r.Name;
						else oUser.User_Sub_Bus__c+=','+reqBU.Sub_Business_Unit__r.Name;
						
						addedBUs.add(reqBU.Business_Unit_Name__c);
							
						userBUs.add(userBU);
					}
					
					if(addedBUs.size() == regionBUs.get(req.Region__c).Business_Units__r.size()){
						oUser.User_Business_Unit_vs__c = 'All';
					}
				}
				
				successfulUsers+= '<br/>'+req.Username__c;
					
			}else{
				errorsToEmail+='<br/>Execution of Request for User <a href="'+URL.getSalesforceBaseUrl().toExternalForm()+'/'+req.Id+'">'+req.Username__c+'</a> has failed: '+req.UserCreationError__c+'<br/>';
			}
		}
		
		if(!userBUs.isEmpty()){
			insert userBUs;
			update usersById.values();
		}


		String tMobileAppAssignment_Error = '';
		String tMobileAppAssignment_Success = '';
		if (String.isBlank(errorsToEmail)){

			// Add Mobile Apps
			List<Mobile_App_Assignment__c> lstMobileAppAssignment_Insert = new List<Mobile_App_Assignment__c>();

			// Get all Mobile Apps in a Map
			List<Mobile_App__c> lstMobileApp = [SELECT Id, Name FROM Mobile_App__c WHERE Active__c = true ORDER BY Name];
			Map<Id, Mobile_App__c> mapMobileAppId = new Map<Id, Mobile_App__c>();
			mapMobileAppId.putAll(lstMobileApp);
			Map<String, Mobile_App__c> mapMobileApp = new Map<String, Mobile_App__c>();
			for (Mobile_App__c oMobileApp : lstMobileApp) mapMobileApp.put(oMobileApp.Name, oMobileApp);

			for (Create_User_Request__c req : requests){
			
				if ( (req.Created_User__c!=null) && (usersById.containsKey(req.created_User__c)) ){

					if (!String.isBlank(req.Mobile_Apps__c)){

						List<String> lstMobileAppName = req.Mobile_Apps__c.split(';');

						for (String tMobileAppName : lstMobileAppName){
						
							if (mapMobileApp.containsKey(tMobileAppName)){
					
								Mobile_App_Assignment__c oMobileAppAssignment = new Mobile_App_Assignment__c();
									oMobileAppAssignment.Active__c = true;
									oMobileAppAssignment.User__c = req.Created_User__c;
									oMobileAppAssignment.Mobile_App__c = mapMobileApp.get(tMobileAppName).Id;
								lstMobileAppAssignment_Insert.add(oMobileAppAssignment);
								
							}else{

								// Error Mobile App not found
								tMobileAppAssignment_Error += '<br/>Error while assigning Mobile App "' + tMobileAppName + '" for user "' + req.Username__c + '" : Mobile App not found.  Please verify that the Mobile App exists and is active.';

							}
					
						}

					}
				
				}

			}

			if (lstMobileAppAssignment_Insert.size() > 0){

				List<Database.SaveResult> lstSaveResult_MobileAppAssignment = Database.insert(lstMobileAppAssignment_Insert, false);

				Integer iCounter = 0;
				for (Database.SaveResult oSaveResult : lstSaveResult_MobileAppAssignment){
					
					Mobile_App_Assignment__c oMobileAppAssignment = lstMobileAppAssignment_Insert[iCounter];				

					User oUser = usersById.get(oMobileAppAssignment.User__c);
					Create_User_Request__c oCreateUserRequest = reqByUsername.get(oUser.Username);

					if (!oSaveResult.isSuccess()){
					
						String tError = '';
						for (Database.Error oError : oSaveResult.getErrors()){
							tError += oError.getMessage() + '; ';
						}
						
						tMobileAppAssignment_Error += 'Error while assigning Mobile App "' + mapMobileAppId.get(oMobileAppAssignment.Mobile_App__c).Name + '" : ' + tError + '<br />';

					
					}				

					iCounter++;

				}
	
			}

		}

		
		if (errorsToEmail != ''){

			String emailBody = 'Dear '+USerInfo.getFirstName()+' '+UserInfo.getLastName()+',<br/>'+errorsToEmail;
			Messaging.Singleemailmessage mail = new Messaging.Singleemailmessage();
			mail.setHtmlBody(emailBody);
			mail.setTargetObjectId(UserInfo.getUserId());
			mail.setSubject('Error on User creation');
			mail.setSaveAsActivity(false);
			
			Messaging.sendEmail(new Messaging.Singleemailmessage[] { mail});

		}else{

			String emailBody = 'Dear '+USerInfo.getFirstName()+' '+UserInfo.getLastName()+',<br/><br/>The following Users where created successfully:<br/>'+successfulUsers;
			if (!String.isBlank(tMobileAppAssignment_Error)){
				emailBody += '<br /><br />' + tMobileAppAssignment_Error;
			}
			Messaging.Singleemailmessage mail = new Messaging.Singleemailmessage();
			mail.setHtmlBody(emailBody);
			mail.setTargetObjectId(UserInfo.getUserId());
			mail.setSubject('Users created successfully');
			mail.setSaveAsActivity(false);
			
			Messaging.sendEmail(new Messaging.Singleemailmessage[] { mail});

		}

	}

}