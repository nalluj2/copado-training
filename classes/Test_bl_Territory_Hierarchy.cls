@isTest
private class Test_bl_Territory_Hierarchy {
    
    private static testmethod void testMethods(){
        
        Territory2 regionTerritory = [Select Id, Company__c from Territory2 where name='EUR (R)'];
        
        Set<Id> result;
        
        result = bl_Territory_Hierarchy.getTerritoryCountries(new Set<Id>{regionTerritory.Id});     
        System.assert(result.size()>1);
        
        result = bl_Territory_Hierarchy.getTerritoryDistricts(new Set<Id>{regionTerritory.Id});     
        System.assert(result.size()>1);
        
        result = bl_Territory_Hierarchy.getTerritoryTerritories(new Set<Id>{regionTerritory.Id});       
        System.assert(result.size()>1);
        
        Territory2 leafTerritory = [Select Id from Territory2 where Territory2Type.DeveloperName = 'Territory' AND Company__c=:regionTerritory.Company__c LIMIT 1];
        
        result = bl_Territory_Hierarchy.getTerritoryCountries(new Set<Id>{leafTerritory.Id});       
        System.assert(result.size()==1);
        
        result = bl_Territory_Hierarchy.getTerritoryDistricts(new Set<Id>{leafTerritory.Id});       
        System.assert(result.size()==1);
        
        result = bl_Territory_Hierarchy.getTerritoryTerritories(new Set<Id>{leafTerritory.Id});     
        System.assert(result.size()==1);
    }
}