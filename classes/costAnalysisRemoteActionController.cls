global class costAnalysisRemoteActionController {

    public costAnalysisRemoteActionController(ngForceController controller) {

    }

    @RemoteAction
    global static List<SObject> covProducts(String region, String searchText){
        return [
            select id, Name, Business_Unit_ID__r.Name, Sub_Business_Unit__c, Product_Group__r.Name, Family, CFN_Code_Text__c from Product2 
            where 
                Region_vs__c = :region and 
                Name like :searchText 
                //and 
                //(not Name like '%(D)%') and 
                //(not Name like '%(K)%') and 
                //(not Name like '%(J)%')
            LIMIT 101
            ];
    }
    
    public String enDict {
        get {
            StaticResource sr = [
                    select Body
                    from StaticResource
                    where Name = 'SET_EN'
                    ];
            return sr.Body.toString();
        }
    }
    
    public String deDict {
        get {
            StaticResource sr = [
                    select Body
                    from StaticResource
                    where Name = 'SET_DE'
                    ];
            return sr.Body.toString();
        }
    }
    
    public String frDict {
        get {
            StaticResource sr = [
                    select Body
                    from StaticResource
                    where Name = 'SET_FR'
                    ];
            return sr.Body.toString();
        }
    }
    
    public String itDict {
        get {
            StaticResource sr = [
                    select Body
                    from StaticResource
                    where Name = 'SET_IT'
                    ];
            return sr.Body.toString();
        }
    }
    
    public String esDict {
        get {
            StaticResource sr = [
                    select Body
                    from StaticResource
                    where Name = 'SET_ES'
                    ];
            return sr.Body.toString();
        }
    }
    
    public String userLang {
        get {
            return UserInfo.getLanguage();
        }
    }
    
    public String userLocale {
        get {
            return UserInfo.getLocale();
        }
    }
}