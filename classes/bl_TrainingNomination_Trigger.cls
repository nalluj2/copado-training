//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 28-06-2016
//  Description      : APEX Class - Business Logic for tr_TrainingNomination
//  Change Log       : PMT-22787: MITG EMEA User Setup - Changed record type to MITG_Emerging_Markets_Task to MITG_EMEA_Task
//--------------------------------------------------------------------------------------------------------------------
public with sharing class bl_TrainingNomination_Trigger {

	//--------------------------------------------------------------------------------------------------------------------
	// When a Training Nomination record is updated to Approved,
	//	we need to copy the URL Fields from the related Training Course
	//--------------------------------------------------------------------------------------------------------------------
	public static void updateTrainingNomination_Url(List<Training_Nomination__c> lstTriggerNew, Map<Id, Training_Nomination__c> mapTriggerOld){

		List<Training_Nomination__c> lstTrainingNomination_Update = new List<Training_Nomination__c>();
		Set<Id> setID_TrainingCourse = new Set<Id>();

		for (Training_Nomination__c oTrainingNomination : lstTriggerNew){

			Training_Nomination__c oTrainingNomination_Old = mapTriggerOld.get(oTrainingNomination.Id);

			if (
				(oTrainingNomination.Status__c == 'Approved')
				&&
				(oTrainingNomination.Status__c != oTrainingNomination_Old.Status__c)
			){
				lstTrainingNomination_Update.add(oTrainingNomination);
				setID_TrainingCourse.add(oTrainingNomination.Course__c);
			}

		}

		Map<Id, Training_Course__c> mapTrainingCource = new Map<Id, Training_Course__c>(
			[
				SELECT 
					Id
					, Final_Follow_Up_Survey__c, Initial_Follow_Up_Survey__c, Logistics_Survey__c
				FROM
					Training_Course__c
				WHERE
					Id = :setID_TrainingCourse
			]
		);

		for (Training_Nomination__c oTrainingNomination : lstTrainingNomination_Update){

			Training_Course__c oTrainingCourse = mapTrainingCource.get(oTrainingNomination.Course__c);

			oTrainingNomination.Final_Follow_Up_Survey__c = oTrainingCourse.Final_Follow_Up_Survey__c;
			oTrainingNomination.Initial_Follow_Up_Survey__c = oTrainingCourse.Initial_Follow_Up_Survey__c;
			oTrainingNomination.Logistics_Survey__c = oTrainingCourse.Logistics_Survey__c;
		}

	}
	//--------------------------------------------------------------------------------------------------------------------


	//--------------------------------------------------------------------------------------------------------------------
	// Validate that the Training Nomination Approval is allowed and that there are still availabe/open of_Seats_Open__c
	//--------------------------------------------------------------------------------------------------------------------
	public static void validateTrainingNominationApproval(List<Training_Nomination__c> lstTriggerNew, Map<Id, Training_Nomination__c> mapTriggerOld){

		// of_Seats_Open__c
		List<Training_Nomination__c> lstTrainingNomination_Approved = new List<Training_Nomination__c>();
		Set<Id> setID_TrainingCourse = new Set<Id>();

		Map<Id, Decimal> mapTrainingCourseId_OpenSeats = new Map<Id, Decimal>();

		for (Training_Nomination__c oTrainingNomination : lstTriggerNew){
			Training_Nomination__c oTrainingNomination_Old = mapTriggerOld.get(oTrainingNomination.Id);

			if (
				(oTrainingNomination.Status__c != oTrainingNomination_Old.Status__c)
				&&
				(oTrainingNomination.Status__c == 'Approved')
			){
				lstTrainingNomination_Approved.add(oTrainingNomination);
				setID_TrainingCourse.add(oTrainingNomination.Course__c);
			}

		}

		if (setID_TrainingCourse.size() > 0){
			Map<Id, Training_Course__c> mapTrainingCourse = new Map<Id, Training_Course__c>(
				[
					SELECT 
						Id, of_Seats_Open__c
					FROM
						Training_Course__c
					WHERE
						Id = :setID_TrainingCourse
				]
			);

			for (Training_Course__c oTrainingCourse : mapTrainingCourse.values()){
				mapTrainingCourseId_OpenSeats.put(oTrainingCourse.Id, oTrainingCourse.of_Seats_Open__c);
			}

			for (Training_Nomination__c oTrainingNomination : lstTrainingNomination_Approved){

				Decimal decOpenSeats = mapTrainingCourseId_OpenSeats.get(oTrainingNomination.Course__c); 
				if (decOpenSeats <= 0){
					oTrainingNomination.addError(Label.TrainingNominationApprovalError);
				}else{
					mapTrainingCourseId_OpenSeats.put(oTrainingNomination.Course__c, decOpenSeats - 1);	
				}

			}
		}

	}
	//--------------------------------------------------------------------------------------------------------------------


	//--------------------------------------------------------------------------------------------------------------------
	// Prevent duplicate Training Nominations
	//	a Contact can only be linked to on Training Nomination of a Training Course
	//--------------------------------------------------------------------------------------------------------------------
	public static void preventDuplicateNomination(List<Training_Nomination__c> lstTriggerNew){

		Set<Id> setID_TrainingCourse = new Set<Id>();
		clsUtil.debug('preventDuplicateNomination is fired');

		for (Training_Nomination__c oTrainingNomination : lstTriggerNew){
			setID_TrainingCourse.add(oTrainingNomination.Course__c);
		}

		List<Training_Nomination__c> lstTrainingNomination_Existing = 
			[
				SELECT
					Id, Contact_Name__c, Course__c
				FROM
					Training_Nomination__c
				WHERE
					Course__c = :setID_TrainingCourse
			];


		if (lstTrainingNomination_Existing.size() > 0){
			Map<Id, Set<Id>> mapTrainingCourseId_ContactIds = new Map<Id, Set<Id>>();
			for (Training_Nomination__c oTrainingNomination : lstTrainingNomination_Existing){
				Set<Id> setID_Contact = new Set<Id>();
				if (mapTrainingCourseId_ContactIds.containsKey(oTrainingNomination.Course__c)){
					setID_Contact = mapTrainingCourseId_ContactIds.get(oTrainingNomination.Course__c);
				}
				setID_Contact.add(oTrainingNomination.Contact_Name__c);
				mapTrainingCourseId_ContactIds.put(oTrainingNomination.Course__c, setID_Contact);
			}

			Set<Id> setID_Contact_New = new Set<Id>();
			for (Training_Nomination__c oTrainingNomination : lstTriggerNew){

				Set<Id> setID_Contact_Existing = mapTrainingCourseId_ContactIds.get(oTrainingNomination.Course__c);

				clsUtil.debug('oTrainingNomination : ' + oTrainingNomination);
				clsUtil.debug('setID_Contact_Existing : ' + setID_Contact_Existing);
				clsUtil.debug('setID_Contact_New : ' + setID_Contact_New);

				if ( (setID_Contact_Existing.contains(oTrainingNomination.Contact_Name__c)) || (setID_Contact_New.contains(oTrainingNomination.Contact_Name__c)) ){
					oTrainingNomination.addError(Label.TrainingNominationContactError);
				}
				setID_Contact_New.add(oTrainingNomination.Contact_Name__c);

			}
		}

	}
	//--------------------------------------------------------------------------------------------------------------------


	//--------------------------------------------------------------------------------------------------------------------
	// When the Training_Nomination__c record is created we copy the PACE_Approver__c from the related Training_Course__c 
	//	to the new Training_Nomination__c record.
	//--------------------------------------------------------------------------------------------------------------------
	public static void updateTrainingNomination_PACEApprover(List<Training_Nomination__c> lstTriggerNew){

		Set<Id> setID_TrainingCourse = new Set<Id>();

		for (Training_Nomination__c oTrainingNomination : lstTriggerNew){
			setID_TrainingCourse.add(oTrainingNomination.Course__c);
		}

		Map<Id, Training_Course__c> mapTrainingCourse = new Map<Id, Training_Course__c>(
			[
				SELECT Id, PACE_Approver__c
				FROM Training_Course__c
				WHERE Id = :setID_TrainingCourse
			]
		);

		for (Training_Nomination__c oTrainingNomination : lstTriggerNew){
			Training_Course__c oTrainingCourse = mapTrainingCourse.get(oTrainingNomination.Course__c);
			oTrainingNomination.PACE_Approver__c = oTrainingCourse.PACE_Approver__c;
		}

	}
	//--------------------------------------------------------------------------------------------------------------------


	//--------------------------------------------------------------------------------------------------------------------
	// When the status of a Training Nomination is updated to "Follow Up" we need to create a Follow Up Task and 
	//	send out a email to the owner of the task (=Training Nomination Record Owner)
	//--------------------------------------------------------------------------------------------------------------------
	public static void createFollowUpTask(List<Training_Nomination__c> lstTriggerNew, Map<Id, Training_Nomination__c> mapTriggerOld){

		Set<Id> setID_TrainingCourse = new Set<Id>();
		List<Training_Nomination__c> lstTrainingNomination_Process = new List<Training_Nomination__c>();

		for (Training_Nomination__c oTrainingNomination : lstTriggerNew){

			Training_Nomination__c oTrainingNomination_Old = mapTriggerOld.get(oTrainingNomination.Id);

			if (
				(oTrainingNomination.Status__c != oTrainingNomination_Old.Status__c)
				&&
				(oTrainingNomination.Status__c == 'Follow Up')
			){
				setID_TrainingCourse.add(oTrainingNomination.Course__c);
				lstTrainingNomination_Process.add(oTrainingNomination);
			}

		}

		Map<Id, Training_Course__c> mapTrainingCourse = new Map<Id, Training_Course__c>([SELECT Id, Name, Start_Date__c, Course_Title__c FROM Training_Course__c WHERE Id = :setID_TrainingCourse]);

		List<Task> lstTask = new List<Task>();
        
        //PMT-22787: MITG EMEA User Setup - Changed record type to MITG_Emerging_Markets_Task to MITG_EMEA_Task
        EMEA_MITG_Opportunites__c recEMEAMITG = EMEA_MITG_Opportunites__c.getValues('EMEA MITG Opportunities');
        Id idRecordType_Task;
		
        if (recEMEAMITG.MITG_EMEA_Available__c){
            idRecordType_Task = clsUtil.getRecordTypeByDevName('Task', 'MITG_EMEA_Task').Id;
        }
        else{
			idRecordType_Task = clsUtil.getRecordTypeByDevName('Task', 'MITG_Emerging_Markets_Task').Id;  
        }
				
        for (Training_Nomination__c oTrainingNomination : lstTrainingNomination_Process){
			Training_Course__c oTrainingCourse = mapTrainingCourse.get(oTrainingNomination.Course__c);
			Date dCourseStartDate = oTrainingCourse.Start_Date__c;
			Date dActivity = dCourseStartDate.addDays(90);
			Datetime dtReminder = Datetime.newInstance(dCourseStartDate.year(), dCourseStartDate.month(), dCourseStartDate.day()).addDays(30);

			Task oTask = new Task();
				oTask.RecordTypeId = idRecordType_Task;
				oTask.WhatId = oTrainingNomination.Id;
				oTask.Activity_Type__c = 'PACE Follow Up';
				oTask.Subject = 'PACE follow up';
				oTask.Priority = 'High';
				oTask.OwnerId = oTrainingNomination.CreatedById;
				oTask.ActivityDate = dActivity;
				oTask.Description = 'Training Course ' + clsUtil.isNull(oTrainingCourse.Course_Title__c, oTrainingCourse.Name)  + ' is completed. Please make sure the final follow up survey is completed within 90 days through: ' + oTrainingNomination.Final_Follow_Up_Survey__c + '.';
				oTask.IsReminderSet = true;
				oTask.ReminderDateTime = dtReminder;
			lstTask.add(oTask);

		}

		if (lstTask.size() > 0){

			// Insert the Tasks and send out the standard Notification Email to the Owner of the Task
			Database.DMLOptions oDatabaseDMLOptions = new Database.DMLOptions(); 
				oDatabaseDMLOptions.EmailHeader.TriggerUserEmail = true; 
				oDatabaseDMLOptions.OptAllOrNone = true;
			List<Database.SaveResult> lstSaveResult = Database.Insert(lstTask, oDatabaseDMLOptions);

		}

	}
	//--------------------------------------------------------------------------------------------------------------------


}