@isTest(SeeAllData=true)
public class testContract_UPSERT_AFTER
{
    static testMethod void testContract()
    {
    
        Account acc=new Account();
        acc.AccountNumber='as';
        acc.Name='MNav House Account';
        acc.AccountNumber = '0001234567';
        acc.Account_Country_vs__c='USA';        
        acc.CurrencyIsoCode='USD';
        acc.Phone='24107954';
        insert acc; 
        
        Contract[] scList = new List<Contract>();
        
        Contract scr = new Contract();
        scr.AccountId = Acc.Id;
        scr.Contract_Type__c = 'MENT Agreement';
        scr.Name = 'Test12133';
        scr.StartDate = date.Today();
        scr.ContractTerm = 12;
        scList.add(scr);
        
        Contract scr2 = new Contract();
        scr2.AccountId = Acc.Id;
        scr2.Contract_Type__c = 'Surgical Support Pack';
        scr2.Name = 'Test12133';
        scr2.StartDate = date.Today();
        scr2.ContractTerm = 12;
        scList.add(scr2);
        
        insert scList;
                
        Contract RelatedScr = [Select id,Contract_Type__c,startDate, endDate,ContractTerm from Contract where id =: scr.id];
        RelatedScr.StartDate = System.Today()+100;
        RelatedScr.ContractTerm=12;
        RelatedScr.Anniversary__c=Null;
        upsert RelatedScr;        
    }
}