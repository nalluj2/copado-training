public with sharing class ctrlExt_Export_Opportunity_Lots {
	
	public String fileContent {get; set;}
	
	public ctrlExt_Export_Opportunity_Lots(ApexPages.StandardController sc){
		
		Opportunity opp = (Opportunity) sc.getRecord();  
		
		Map<String, Schema.SObjectField> describeMap = Schema.SObjectType.Opportunity_Lot__c.fields.getMap();
       
        List<String> templateFields = new List<String>();
        
        for(String oppLotField : describeMap.keySet()){
        	
        	Schema.DescribeFieldResult fDescribe = describeMap.get(oppLotfield).getDescribe();
        	
        	if(fDescribe.isCustom() && fDescribe.isCalculated() == false && fDescribe.getRelationshipOrder() == null) templateFields.add(oppLotField);      	
        }
        
        templateFields.add('id');
        templateFields.add('name');
        templateFields.add('currencyisocode');        
        templateFields.sort();
        
        List<String> existingLotsFile = new List<String>();
        existingLotsFile.add(String.join(templateFields, ',').remove('opportunity__r.'));
        
        String query = 'Select ' + String.join(templateFields, ',') + ', Account__r.Name, Product_Offered__r.Name, Sub_Business_Unit__r.Name, Lot_Owner__r.Alias_Unique__c FROM Opportunity_Lot__c where Opportunity__c = \'' + opp.Id + '\' ORDER BY Name';
        
        for(Opportunity_Lot__c oppLot : Database.query(query)){
        	
        	List<String> rowValues = new List<String>();
        	
        	for(String lotField : templateFields){
        		
        		String fieldValue;
        		
        		if(lotField.toUpperCase() == 'SUB_BUSINESS_UNIT__C'){
        			
        			if(oppLot.Sub_Business_Unit__r != null) fieldValue = oppLot.Sub_Business_Unit__r.Name.escapeCsv();
        			else fieldValue = '';
        			
        		}else if(lotField.toUpperCase() == 'ACCOUNT__C'){
        			
        			if(oppLot.Account__r != null) fieldValue = oppLot.Account__r.Name.escapeCsv();
        			else fieldValue = '';
        			
        		}else if(lotField.toUpperCase() == 'PRODUCT_OFFERED__C'){
        			
        			if(oppLot.Product_Offered__r != null) fieldValue = oppLot.Product_Offered__r.Name.escapeCsv();
        			else fieldValue = '';
        			
        		}else if(lotField.toUpperCase() == 'LOT_OWNER__C'){
        			
        			if(oppLot.Lot_Owner__r != null) fieldValue = oppLot.Lot_Owner__r.Alias_Unique__c.escapeCsv();
        			else fieldValue = '';
        			
        		}else fieldValue = convertToFieldFormat(oppLot.get(lotField));
        		
        		rowValues.add(fieldValue);        		
        	}
        	
        	existingLotsFile.add(String.join(rowValues, ','));
        }
                
        fileContent = String.join(existingLotsFile, '\n');
	}  
	
	private String convertToFieldFormat(Object inputFieldData){
		
		if(inputFieldData == null || inputFieldData == '') return '';
				
		return String.valueOf(inputFieldData).escapeCsv();
	}
}