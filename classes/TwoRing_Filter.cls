public class TwoRing_Filter
{
	public List<TwoRing_Condition> conditions;
	public List<TwoRing_Filter> filters;
	public String filterOperator;
	public TwoRing_Filter(String filterOperator)
	{
		this.filterOperator = filterOperator;
		this.conditions = new List<TwoRing_Condition>();
		this.filters = new List<TwoRing_Filter>();
	}
	public void AddCondition(TwoRing_Condition condition)
	{
		this.conditions.Add(condition);
	}
	public void AddFilter(TwoRing_Filter childFilter)
	{
		this.filters.Add(childFilter);
	}
	public TwoRing_Filter AddFilter(string filterOperator)
	{
		TwoRing_Filter newFilterExpression = new TwoRing_Filter(filterOperator);
		this.filters.Add(newFilterExpression);
		return newFilterExpression;
	}
}