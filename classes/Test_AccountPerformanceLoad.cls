/*
 *      Description : Test methods for Account Performance Load
 *
 *      Author = Rudy De Coninck
 */
@IsTest
public class Test_AccountPerformanceLoad {

	 public static testmethod void testCreation(){
		
		//Insert Company
	    Company__c cmpny = new Company__c();
	    cmpny.name='TestMedtronic';
	    cmpny.CurrencyIsoCode = 'EUR';
	    cmpny.Current_day_in_Q1__c=56;
	    cmpny.Current_day_in_Q2__c=34; 
	    cmpny.Current_day_in_Q3__c=5; 
	    cmpny.Current_day_in_Q4__c= 0;   
	    cmpny.Current_day_in_year__c =200;
	    cmpny.Days_in_Q1__c=57;  
	    cmpny.Days_in_Q2__c=35;
	    cmpny.Days_in_Q3__c=13;
	    cmpny.Days_in_Q4__c=22;
	    cmpny.Days_in_year__c =250;
	    cmpny.Company_Code_Text__c = 'T33';
	    insert cmpny;
		
		Account a = new Account() ;
        a.Name = 'Test Account Name 1';  
        a.RecordTypeId = FinalConstants.sapAccountRecordTypeId ;
        a.Account_Country_vs__c='BELGIUM';
        a.CurrencyIsoCode ='EUR' ;
        insert a;
        
        Business_Unit__c bu1 =  new Business_Unit__c();
	    bu1.Company__c =cmpny.id;
	    bu1.name='BU1Medtronic';
	    bu1.Account_Plan_Activities__c=true;
	    bu1.Company__c = cmpny.Id;
	    insert bu1;
	
	    //Insert SBU
	    Sub_Business_Units__c sbu1 = new Sub_Business_Units__c();
	    sbu1.name='SBUMedtronic1';
	    sbu1.Business_Unit__c=bu1.id;
	    sbu1.Account_Plan_Default__c='Revenue';
	    sbu1.Account_Flag__c = 'AF_Solutions__c';
	    insert sbu1;
	
		//Insert Therapy Group
    	Therapy_Group__c tg = new Therapy_Group__c();
    	tg.Name='Therapy Group';
    	tg.Sub_Business_Unit__c = sbu1.Id;
    	tg.Company__c = cmpny.Id;
    	insert tg;
	
        Therapy__c tp1 = new Therapy__c();
    	tp1.Name='Therapy1';
    	tp1.Therapy_Group__c = tg.id;
    	tp1.Business_Unit__c=bu1.id;
    	tp1.Sub_Business_Unit__c=sbu1.id;
    	tp1.Therapy_Name_Hidden__c = 'Therapy1';
    	insert tp1;
        
        Product_Group__c pg1 = new Product_Group__c();
   		pg1.Name = 'Product Group1';
    	pg1.Editable_in__c='Units';
    	pg1.Therapy_ID__c = tp1.id;
        insert pg1;
        
		Account_Performance_Load__c apl = new Account_Performance_Load__c();
		apl.Account__c = a.id;
		apl.Forecast_Q1_Revenue__c = 1000;
		apl.Forecast_Q1_Units__c = 1000;
		apl.Forecast_Q2_Revenue__c = 1000;
		apl.Forecast_Q2_Units__c = 1000;
		apl.CurrencyIsoCode = 'EUR';
		apl.Load_Source__c = 'SFDC';
		apl.Potential_Revenue__c = 1000;
		apl.Potential_Units__c = 1000;
		apl.Product_Group__c = pg1.id;
		insert apl;
	}

}