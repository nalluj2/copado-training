public with sharing class ctrlExt_AccountHierarchyView{
	
	// Map of Account Types and HTML Color to build-up legend
	public Map<String, Account_Type__c> mapOfTypeToAccntType{
		get{
			if (mapOfTypeToAccntType == null){
				mapOfTypeToAccntType = new Map<String, Account_Type__c>();
			}
			return mapOfTypeToAccntType;
		}
		private set;
	}
	
	// Positioning
	public Integer offSetYCorrection {get;set;}
	
	// Tree in JSON format for the Infovis Javascript Library
	transient String jData;

	// Current Account
	private Account thisAcc;
	
	private Affiliation__c affiliation;
	
	// Pointer to top Node of the Tree
	// Per AJAX request from the page this topNode is extended
	private treeNode topNode;	
	
	public string getTopNode(){
		//System.debug('String.valueOf(topNode.data.parentnodeid) '+String.valueOf(topNode.data.parentnodeid)+' topNode:'+topNode);
		return String.valueOf(topNode.id);
		/*
		String topNode='';
		if (affiliation!=null){
			System.debug('Getting topNode '+affiliation);
			if(null!=affiliation.Affiliation_From_Account__c){
				topNode+=affiliation.Affiliation_From_Account__c;
			}
			if(null!=affiliation.Affiliation_To_Account__c){
				topNode+=affiliation.Affiliation_To_Account__c;
			}
		}
		return topNode;
		*/
	}
	
	// Map of nodeId to Node
	// This is used to get the reference to the right position in topNode
	private Map<String, treeNode> mapOfNodeIdToNode = new Map<String, treeNode>();
	
	// Map of Account Id to a Map of Account properties
	Map<Id, bl_Relationship.accountProperties> mapOfIdToAccntProps = new Map<Id, bl_Relationship.accountProperties>();
	
	// Controller (standard)
	public ctrlExt_AccountHierarchyView(ApexPages.StandardController thisCtrl){
		try {
			// Positioning
			offSetYCorrection = 0;
			// Get this Account
			thisAcc = (Account) thisCtrl.getRecord();
			// Set Id parameter of page to 18 size Id
			ApexPages.currentPage().getParameters().put('Id', thisAcc.Id);
			// Initialize initial buildup of the tree
			// That means selected Account, Parent and Childern
			initTree();
		} catch (Exception e){
			 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage() + '<br/>' + e.getStackTraceString()));
		}
	}
	
	// Convert given treeNode into JSON string
	private String convertTreeToJSON(treeNode thisTree){
		System.debug('thisTree '+thisTree);
		System.debug('topNode '+topNode);
		System.debug('acc Id '+thisAcc.Id);
		jData = JSON.serializePretty(thisTree);
		// Replace color, width and height tags to something useful for the JIT library
		// this needs to be done as a class property cannot start with $ sign
		jData = jData.replaceAll('color_for_json', '\\$color');
		jData = jData.replaceAll('width_for_json', '\\$width');
		jData = jData.replaceAll('height_for_json', '\\$height');
		// return json as string
		return jData;
	}	
	
	// Get Node color from Account Type object for relationship type
	private String getNodeColor(String relType, Boolean isParent, Id accntId){
		// Default color is white
		String nodeColor = '#ffffff';
		Relationship_Type__c thisRT = bl_Relationship.mapOfRelToRelType.get(relType);
		if (thisRT != null){
			if (thisRT.Child_Account_Type__c != null && thisRT.Parent_Account_Type__c != null){
				// Account Type has been determined by relationship type
				// Get the HTML color and put in legend map
				if (isParent){
					nodeColor = thisRT.Parent_Account_Type__r.HTML_Color__c;
					mapOfTypeToAccntType.put(thisRT.Parent_Account_Type__r.Account_Type_Level__c + '' + thisRT.Parent_Account_Type__r.Account_Type__c, thisRT.Parent_Account_Type__r);
				} else {
					nodeColor = thisRT.Child_Account_Type__r.HTML_Color__c;
					mapOfTypeToAccntType.put(thisRT.Child_Account_Type__r.Account_Type_Level__c + '' + thisRT.Child_Account_Type__r.Account_Type__c, thisRT.Child_Account_Type__r);
				}
			} else {
				// Get account props for this Account
				if (getAccountProperties(accntId)){
		
					// Get the Account properties, and then the HTML color and put into legend map
					bl_Relationship.accountProperties thisAccntProps = mapOfIdToAccntProps.get(accntId);
					
					//For CANADA we have only 1 Account Type per level
					bl_Relationship.AccountTypeProperty accountType;					
					if(thisAccntProps.accountTypes.size()>0) accountType = thisAccntProps.accountTypes[0];
					
					if (accountType!=null && bl_Relationship.mapOfIdToAccntTypes.get(accountType.accountTypeId) != null){
						nodeColor = bl_Relationship.mapOfIdToAccntTypes.get(accountType.accountTypeId).HTML_Color__c;
						thisAccntProps.htmlColor = nodeColor;
						mapOfTypeToAccntType.put(accountType.accountTypeLevel + '' + accountType.accountType, bl_Relationship.mapOfIdToAccntTypes.get(accountType.accountTypeId));
						mapOfIdToAccntProps.put(accntId, thisAccntProps);
					}
					
				}
			}
		}
		// Add node color to Account props
		if (getAccountProperties(accntId)){
			// Get the Account properties, and then the HTML color and put into legend map
			bl_Relationship.accountProperties thisAccntProps = mapOfIdToAccntProps.get(accntId);
			thisAccntProps.htmlColor = nodeColor;
			mapOfIdToAccntProps.put(accntId, thisAccntProps);
		}
			
		return nodeColor;
	}

	// Get account properties for one account id
	private Boolean getAccountProperties(Id accntId){
		return getAccountProperties(new Set<Id>{accntId});
	}
	private Boolean getAccountProperties(List<Affiliation__c> listOfAff){
		Set<Id> setOfAccntId = new Set<Id>();
		for (Affiliation__c thisAff : listOfAff){
			setOfAccntId.add(thisAff.Affiliation_To_Account__c);
			setOfAccntId.add(thisAff.Affiliation_From_Account__c);
		}
		if (!setOfAccntId.isEmpty()){
			return getAccountProperties(setOfAccntId);
		} else {
			return false;
		} 
	}
	// Get account properties for give set of account ids
	private Boolean getAccountProperties(Set<Id> setOfAccntId){
		// Boolean to indicate all Account Properties have been found
		Boolean retVal = false;
		Set<Id> setOfNewAccntId = new Set<Id>();
		for (Id thisAccntId : setOfAccntId){
			// If Account properties have not been fetched, do it now
			if (mapOfIdToAccntProps.get(thisAccntId) == null){
				setOfNewAccntId.add(thisAccntId);
			}
		}
		if (!setOfNewAccntId.isEmpty()){
			// Get props for this account
			Map<Id, bl_Relationship.accountProperties> thisMap = bl_Relationship.getAccountProperties(setOfNewAccntId);
			// If props where fetched, put them in map for in memory storage for lifetime of class
			for (Id thisAccntId : thisMap.keySet()){
				mapOfIdToAccntProps.put(thisAccntId, thisMap.get(thisAccntId));
				retVal = true;
			} 
		} else {
			// Account props already fetched
			retVal = true;
		}	
		return retVal;
	}
	
	// Lookup BU or SBU name for display if required via Relationship Type and Child Account Country
	private String getMasterDataName(Affiliation__c thisAff, Id accntId){
		// Return string
		String masterDataName;
		// Get country Id from Map
		bl_Relationship.accountProperties thisAccntProps = mapOfIdToAccntProps.get(accntId);
		Id countryId = thisAccntProps.accountCountryId;
		// Validate if there is an entry in the map with key Relationship Type + Country Id
		if (bl_Relationship.mapOfRelToRelTypeCountry.get(thisAff.Affiliation_Type__c + '' + countryId) != null){
			// Get the Relationship Type Country instance from the Map to get Master Data Level
			Relationship_Type_Country__c thisRTC = bl_Relationship.mapOfRelToRelTypeCountry.get(thisAff.Affiliation_Type__c + '' + countryId);
			if (thisRTC != null){
				if (thisRTC.Master_Data_Level__c == 'Sub Business Unit'){
					//masterDataName = thisAff.Sub_Business_Unit__r.Name;
				} else if (thisRTC.Master_Data_Level__c == 'Business Unit'){
					//masterDataName = thisAff.A2ABusiness_Unit_Lookup__r.Name;
				}
			}
		}
		// Add node color to Account props
		thisAccntProps.masterDataName = masterDataName;
		mapOfIdToAccntProps.put(accntId, thisAccntProps);
		return masterDataName;
	}
	
	private void populateUniqueParent(Id accntId){
		// Get country Id from Map
		bl_Relationship.accountProperties thisAccntProps = mapOfIdToAccntProps.get(accntId);		
		
		//For CANADA we have only 1 Account Type per level
		bl_Relationship.AccountTypeProperty accountType;					
		if(thisAccntProps.accountTypes.size()>0) accountType = thisAccntProps.accountTypes[0];
		
		// Populate Unique parent attribute
		Account_Type_Country__c thisATC = bl_Relationship.mapOfAccntTypeCountry.get(accountType.accountTypeId + '' + thisAccntProps.accountCountryId);
		if (thisATC != null){
			thisAccntProps.uniqueParent = thisATC.Unique_Parent__c;
		}		
		mapOfIdToAccntProps.put(accntId, thisAccntProps);
	}
	
	// Get additional properties and create new Node
	private treeNode createNewNode(Affiliation__c thisAff, String nodeType, Boolean multipleParents){
		treeNode thisNode;
		Id accntId;
		// Select correct Account Id dependent on node type
		if (nodeType == 'topNode' || nodeType == 'parentNode'){
			accntId = thisAff.Affiliation_To_Account__c;
		} else if (nodeType == 'childNode'){
			accntId = thisAff.Affiliation_From_Account__c;
		}
		// First get all account properties
		getAccountProperties(accntId);
		// Get additional properties
		Boolean showInHierarchy = getAccountHierarchyProperties(accntId);
		// Determine the node color
		getNodeColor(thisAff.Affiliation_Type__c, nodeType == 'topNode', accntId);
		// Determine master data (BU/SBU) display
		getMasterDataName(thisAff, accntId);
		// Create new node
		thisNode = new treeNode(thisAff, mapOfIdToAccntProps.get(accntId), nodeType,showInHierarchy);
		//thisNode.id = thisnode.name;
		return thisNode;
	}
	
	// Determine Account Info Link and if Account can be shown in Hierarchy  
	private boolean getAccountHierarchyProperties(Id accntId){
		// By default show link
		Boolean retVal = true;
		Boolean showInHierarchy = true;
		// Retrieve value from 
		bl_Relationship.accountProperties thisAccntProps = mapOfIdToAccntProps.get(accntId);
		
		//For CANADA we have only 1 Account Type per level
		bl_Relationship.AccountTypeProperty accountType;					
		if(thisAccntProps.accountTypes.size()>0) accountType = thisAccntProps.accountTypes[0];
		
		System.debug('Account Type '+thisAccntProps.accountTypes);
		System.debug('mapOfIdToAccntProps '+mapOfIdToAccntProps);
		Account_Type_Country__c thisRTC = bl_Relationship.mapOfAccntTypeCountry.get(accountType.accountTypeId + '' + thisAccntProps.accountCountryId);
		if (thisRTC != null){
			thisAccntProps.showLink = thisRTC.Link_in_hierarchy__c;
			// If account is the selected account, always show in hierarhcy
			if (thisAcc.Id != accntId){
				thisAccntProps.showInHierarchy = thisRTC.Show_in_hierarchy__c;
				showInHierarchy = thisRTC.Show_in_hierarchy__c;
			}
			mapOfIdToAccntProps.put(accntId, thisAccntProps);
		}
		return showInHierarchy;
	}
	
	// Select options for radio buttons to select another parent account
	public List<SelectOption> parentAccountSelectOptions {
		get{
			if (parentAccountSelectOptions == null){
				parentAccountSelectOptions = new List<SelectOption>();
			}
			return parentAccountSelectOptions;
		}
		set;
	}
	
	// Selected parent account
	public String selectedParentAccntTypeId {get;set;}
	
	// If there are multiple parents, validate what to do to show correct radio buttons
	private void validateMultipleParents(List<Affiliation__c> listOfAff, Id selectedParentId, Id accntId){
		// multiple parents are found, let's validate the Account Types
		// Get all the parent account id's
		Set<Id> setOfAccntId = new Set<Id>{accntId};
		for (Affiliation__c thisAff : listOfAff){
			setOfAccntId.add(thisAff.Affiliation_To_Account__c);
		}
		// Get account properties of the parent Accounts
		getAccountProperties(setOfAccntId);
		// For the selected Account set attribute for rendering
		mapOfIdToAccntProps.get(accntId).multipleParents = true;
		// Get Unique Parent value from Account Type for selected AccntId
		populateUniqueParent(accntId);
					
		// What scenario to display
		// 1. Select Account (also show Account type)
		// 2. Select BU/SBU
		if (mapOfIdToAccntProps.get(accntId).uniqueParent == 'No'){
			// Build selection for different Account
			parentAccountSelectOptions = new List<SelectOption>();
			for (Affiliation__c thisAff : listOfAff){
				//For CANADA we have only one Account Type per level								
				parentAccountSelectOptions.add(new SelectOption(String.valueof(thisAff.Affiliation_To_Account__c), thisAff.Affiliation_To_Account__r.Name + ' (' + mapOfIdToAccntProps.get(thisAff.Affiliation_To_Account__c).accountTypes[0].accountTypeAbb + ')'));
			}		
			// Set the selected Account Id, so radio button on VF Page is selected
			//selectedParentAccntTypeId = selectedParentId;
			selectedParentAccntTypeId = selectedParentId;
		}
	}
	
	// Create the first treeNodes. One level up and one level down
	private void initTree(){
		// Reset topNode
		topNode = null;
		// Reset map
		mapOfNodeIdToNode = new Map<String, treeNode>();
		// Reference to current node
		treeNode curNode;
		// Boolean for multiple parents
		Boolean multipleParents;
		// Add selected Account Id to set for selection
		Set<Id> setOfAccntId = new Set<Id>{thisAcc.Id};
		// Get all parent relationships for this Account
		List<Affiliation__c> listOfAff = getParentRelationships(setOfAccntId);
		if (!listOfAff.isEmpty()){
			// Lets create the topNode
			Affiliation__c thisAff;
			// This is for when one account / type is selected when multiple parents
			if (selectedParentAccntTypeId == null){
				System.debug('No parent Acc Type ID');
				thisAff = listOfAff[0];
			} else {
				
				System.debug('Parent Acc Type ID');
				// Account selected via radio button on VF Page
				if (selectedParentAccntTypeId.left(3) == Schema.describeSObjects(new List<String>{'Account'})[0].keyPrefix){
					System.debug('Parent Acc Type ID, account selected');	
					String[] idArrayParent = selectedParentAccntTypeId.split(';');
					String accountId = idArrayParent[0];
					if (idArrayParent.size()>1){
						String sbuId = idArrayParent[1];
						String buId = idArrayParent[2];
						String bugId = idArrayParent[3];
							
						for (Affiliation__c valAff : listOfAff){
							if(sbuId!=null && sbuId.length()>0){

							}
							
							
							/*	
							if (valAff.Affiliation_To_Account__c == selectedParentAccntTypeId){
								thisAff = valAff;
							}
							*/
						}
					}else{
						for (Affiliation__c valAff : listOfAff){
							if (valAff.Affiliation_To_Account__c == accountId){
									thisAff = valAff;
							}
						}
					}
				// Account type selected via radio button on VF Page
				} else if (selectedParentAccntTypeId.left(3) == Schema.describeSObjects(new List<String>{'Account_Type__c'})[0].keyPrefix) {
					System.debug('Parent Acc Type ID, account Type selection');
					// Determine account properties for the list of parents
					// to get the right parent with the same Account Type
					getAccountProperties(listOfAff);
					for (Affiliation__c valAff : listOfAff){
						//For CANADA only one Account Type per level is defined
						String accountId = selectedParentAccntTypeId.split(';')[0];
						if (mapOfIdToAccntProps.get(valAff.Affiliation_To_Account__c).accountTypes[0].accountTypeId == accountId){
							thisAff = valAff;
						}
					} 
				}
			}
			// Create parent node, which is topNode
			topNode = createNewNode(thisAff, 'topNode', false);		
			affiliation = thisAff;	
			// Validate if topNode has multiple parents
			List<Affiliation__c> listOfAffTopMP = getParentRelationships(topNode.data.accid);
			// If result, add multiple parents flag on topNode
			if (listOfAffTopMP.size() > 1){
				topNode.data.multipleparents = true;
			}			
			// Add reference in map to this node
			mapOfNodeIdToNode.put(topNode.id, topNode);			
			// Validate for multiple parents
			if (listOfAff.size() > 1){
				validateMultipleParents(listOfAff, thisAff.Affiliation_To_Account__c, thisAff.Affiliation_From_Account__c);
			}			
			// Now add the selected Account as child!	
			curNode = createNewNode(thisAff, 'childNode', multipleParents);
			// Add reference to parent
			curNode.data.parentnodeid = topNode.id;
			// Fix id for node, as this is the current selected one
			curNode.id = thisAff.Affiliation_From_Account__c + '' + thisAff.Affiliation_From_Account__c;		
			// Add reference in map to this node
			mapOfNodeIdToNode.put(curNode.id, curNode);	
			// Add as child of top
			topNode.children = new List<treeNode>{curNode};
		}
		// Now let's add childern
		listOfAff = getChildRelationships(setOfAccntId);
		System.debug('listofAff for childern '+listOfAff);
		if (!listOfAff.isEmpty()){
			Set<Id> setOfAffId = new Set<Id>();
			Set<Id> setOfChildAccntId = new Set<Id>();	
			// Indicator if grandparent filtering has been applied
			Boolean gpFilterApplied = false;
			List<treeNode> listOfNodes = new List<treeNode>();			
			for (Affiliation__c thisAff : listOfAff){
				// First validate if topNode exists, otherwise create selected Account as topNode
				if (topNode == null){
					// Create topNode
					topNode = createNewNode(thisAff, 'topNode', false);
					// Fix top Node id
					topNode.id = thisAff.Affiliation_To_Account__c + '' + thisAff.Affiliation_To_Account__c;
					// Add reference in map to this node
					mapOfNodeIdToNode.put(topNode.id, topNode);		
					// Set selectedNodeId
					selectedNodeId = topNode.Id;	
					// Set reference to topNode
					curNode = topNode;						
				}
				// Lets add child Node, as this is the bottom level of the initial tree, validate if the node needs to be shown!
				treeNode thisNode = createNewChildNode(thisAff, 'childNode', false);
				// Add reference to parent node
				thisNode.data.parentnodeid = curNode.id;	
				// Validate if the node must be shown in the hierarchy
				String filterResult = grandparentFilter(thisNode);
				if (filterResult == 'hide' && !gpFilterApplied){
					gpFilterApplied = true;
				}
				if (mapOfIdToAccntProps.get(thisAff.Affiliation_From_Account__c).showInHierarchy && filterResult != 'hide'){
					// Add to set to validate if this child has multiple parents
					setOfAffId.add(thisAff.Id);
					setOfChildAccntId.add(thisAff.Affiliation_From_Account__c);					
					// Add reference in map to this node
					mapOfNodeIdToNode.put(thisNode.id, thisNode);											
					// Add childNode to topNode
					if (curNode.children == null){
						curNode.children = new List<treeNode>();
					}				
					curNode.children.add(thisNode);
					mapOfNodeIdToNode.put(curNode.id, curNode);
					listOfNodes.add(thisNode);
				}
			}
			// Set markers if grantparent filter has been applied
			if (gpFilterApplied && listOfNodes.size() > 1){
				for (treeNode thisTNode : listOfNodes){
					if (thisTNode.data.grandparentid == null){
						thisTNode.data.hasnograndparent = true;
					}
				}
			}			
			// Validate if any of the childs has multiple parents!
			if (!setOfAffId.isEmpty()){
				List<Affiliation__c> listOfAffMP = getParentRelationshipsExcludingAff(setOfChildAccntId, setOfAffId);
				for (Affiliation__c thisAff : listOfAffMP){
					// Get the node which just have been created
					treeNode thisNode = mapOfNodeIdToNode.get(thisAff.Affiliation_From_Account__c + '' + thisAcc.Id);
					if (thisNode != null){
						thisNode.data.multipleparents = true;
					}
				}
			}			
		}
	}
	
	
	
	
	private treeNode createNewChildNode(Affiliation__c thisAff, String nodeType, Boolean multipleParents){
		treeNode thisNode;
		Id accntId;
		// Select correct Account Id dependent on node type
		if (nodeType == 'topNode' || nodeType == 'parentNode'){
			accntId = thisAff.Affiliation_To_Account__c;
		} else if (nodeType == 'childNode'){
			accntId = thisAff.Affiliation_From_Account__c;
		}
		// First get all account properties
		getAccountProperties(accntId);
		// Get additional properties
		Boolean showInHierarchy = getChildAccountHierarchyProperties(accntId);
		// Determine the node color
		getNodeColor(thisAff.Affiliation_Type__c, nodeType == 'topNode', accntId);
		// Determine master data (BU/SBU) display
		getMasterDataName(thisAff, accntId);
		// Create new node
		thisNode = new treeNode(thisAff, mapOfIdToAccntProps.get(accntId), nodeType,showInHierarchy);
		//thisNode.id = thisnode.name;
		return thisNode;
	}
	
	// Determine Account Info Link and if Account can be shown in Hierarchy  
	private boolean getChildAccountHierarchyProperties(Id accntId){
		// By default show link
		Boolean retVal = true;
		Boolean showInHierarchy = true;
		// Retrieve value from 
		bl_Relationship.accountProperties thisAccntProps = mapOfIdToAccntProps.get(accntId);
		//For CANADA only one Account Type per level is defined
		Account_Type_Country__c thisRTC = bl_Relationship.mapOfAccntTypeCountry.get(thisAccntProps.accountTypes[0].accountTypeId + '' + thisAccntProps.accountCountryId);
		System.debug('thisRTC for childNode '+thisRTC);
		
		if (thisRTC != null){
			thisAccntProps.showLink = thisRTC.Link_in_hierarchy__c;
			// If account is the selected account, always show in hierarhcy
			System.debug('thisAcc Id for childNode '+thisAcc.Id +' accntId '+accntId);
			if (thisAcc.Id != accntId){
				thisAccntProps.showInHierarchy = thisRTC.Show_in_hierarchy__c;
				showInHierarchy = thisRTC.Show_in_hierarchy__c;
			}
			mapOfIdToAccntProps.put(accntId, thisAccntProps);
		}
		return showInHierarchy;
	}
	
	
	
	
	
	
	
	// Return sorted list (desc) to build up Account Type Legend
	public List<String> getAccountTypeLegend(){
		// Return list
		List<String> listOfLegend = new List<String>();
		// Get keys from Map and sort 
		List<String> listOfKey = new List<String>();
		listOfKey.addAll(mapOfTypeToAccntType.keySet());
		listOfKey.sort();
		// Loop through sorted list backwards and add to list
		for (Integer i = listOfKey.size() - 1; i >= 0; i--){
			if (mapOfTypeToAccntType.get(listOfKey[i]) != null){
				listOfLegend.add(listOfKey[i]);
			}
		}
		return listOfLegend;	
	}
	
	// Return JSON format of Tree
	public String getTreeData(){
		// Convert topNode to JSON format
		return convertTreeToJSON(topNode);		
	}

	// Parameters for traversing the tree
	public String selectedNodeId {get;set;}
	public String selectedAccntId {get;set;}
	public String selectedDir {get;set;}
	// Refresh tree	
	public PageReference refreshTree(){
		if (selectedDir == 'up'){
			addParentNode(selectedNodeId, selectedAccntId);
			if (topNode.id != selectedNodeId){
				offSetYCorrection = offSetYCorrection - 130;
			}
		} else {
			addChildNodes(selectedNodeId, selectedAccntId);
		}		
		return null;
	}
	
	// Select parent Account
	public PageReference selectParent(){
		initTree();
		return null;
	}
	
	// Add a parent node to the node tree
	private void addParentNode(String nodeId, String accntId){
		// The right part of the nodeId is the To Account
		Set<Id> setOfAccntId = new Set<Id>{accntId};
		// Get all parent relationships for this Account
		List<Affiliation__c> listOfAff = getParentRelationships(setOfAccntId);
		if (!listOfAff.isEmpty()){
			// Lets add the parent node, which will be the rootNode
			Affiliation__c thisAff = listOfAff[0];
			// Create parent node, this will always be the topNode
			treeNode thisNode = createNewNode(thisAff, 'topNode', false);
			// Get reference to child Node
			treeNode childNode = mapOfNodeIdToNode.get(nodeId);
			// Child node is no longer a root node		
			childNode.data.rootnode = false;
			// As the parent node holds the relationship for the child, transfer it
			childNode.data.reltype = thisNode.data.reltype;
			// Add to parent 
			thisNode.children = new List<treeNode>{childNode};
			topNode = thisNode;
			// Add reference in map to this node
			mapOfNodeIdToNode.put(thisNode.id, thisNode);	
			// Validate if Parent node has multiple parents in itself
			List<Affiliation__c> listOfAffMP = getParentRelationshipsExcludingAff(new Set<Id>{accntId}, new Set<Id>{thisAff.Id});
			if (!listOfAffMP.isEmpty() && listOfAffMP.size() > 1){
				thisNode.data.multipleparents = true;
			}							
		}
	}
	
	// Add a child nodes to the node tree
	private void addChildNodes(String nodeId, String accntId){
		// Get reference to parent node
		treeNode parentNode = mapOfNodeIdToNode.get(nodeId);
		// If there is no refrence, something went wrong, skip processing
		if (parentNode != null){
			// Get all child relationships for this Account
			List<Affiliation__c> listOfAff = getChildRelationships(new Set<Id>{accntId});
			if (!listOfAff.isEmpty()){
				// Sets for validation for multiple parents
				Set<Id> setOfAffId = new Set<Id>();
				Set<Id> setOfAccntId = new Set<Id>();
				// Indicator if grandparent filtering has been applied
				Boolean gpFilterApplied = false;
				List<treeNode> listOfNodes = new List<treeNode>();
				// Loop throught child nodes and add to parent
				for (Affiliation__c thisAff : listOfAff){
					// Add to set to validate if this child has multiple parents
					setOfAffId.add(thisAff.Id);
					setOfAccntId.add(thisAff.Affiliation_From_Account__c);
					// Only add the node if it doesn't already exist
					if (mapOfNodeIdToNode.get(thisAff.Affiliation_From_Account__c + '' + thisAff.Affiliation_To_Account__c) == null){
						// Lets create the child node
						treeNode thisNode = createNewNode(thisAff, 'childNode', false);
						// Add reference to parent
						thisNode.data.parentnodeid = parentNode.id;						
						// Validate if the node must be shown in the hierarchy
						String filterResult = grandparentFilter(thisNode);
						if (filterResult == 'hide' && !gpFilterApplied){
							gpFilterApplied = true;
						}
						if (mapOfIdToAccntProps.get(thisAff.Affiliation_From_Account__c).showInHierarchy && filterResult != 'hide'){						
							// Add reference in map to this node
							mapOfNodeIdToNode.put(thisNode.id, thisNode);			
							// Now add child node to parent (if parent does not have childs, create list first)
							if (parentNode.children == null){
								parentNode.children = new List<treeNode>();
							}
							parentNode.children.add(thisNode);
							listOfNodes.add(thisNode);
						}
					}
				}
				// Set markers if grantparent filter has been applied
				if (gpFilterApplied && listOfNodes.size() > 1){
					for (treeNode thisTNode : listOfNodes){
						if (thisTNode.data.grandparentid == null){
							thisTNode.data.hasnograndparent = true;
						}
					}
				}
				// Validate if any of the childs has multiple parents!
				if (!setOfAffId.isEmpty()){
					List<Affiliation__c> listOfAffMP = getParentRelationshipsExcludingAff(setOfAccntId, setOfAffId);
					for (Affiliation__c thisAff : listOfAffMP){
						// Get the node which just have been created
						treeNode thisNode = mapOfNodeIdToNode.get(thisAff.Affiliation_From_Account__c + '' + accntId);
						// Set multiple parents flag to true (except when node is rootnode, this is already done before)
						if (thisNode != null && !thisNode.data.rootnode){
							thisNode.data.multipleparents = true;
						}
					}
				}
			}	
			mapOfNodeIdToNode.put(parentNode.Id, parentNode);
		}	
	}	

	// Validate if node has a grandparent set, and see if filtering is required
	private String grandparentFilter(treeNode thisNode){
		// Set default to true, as if there is no grandparent, no filtering
		String retVal = 'none';
		// Nodes for parent and grandparent
		treeNode parentNode;
		treeNode grandparentNode;
		// Does node have a parent node?
		if (thisNode.data.grandparentid != null && thisNode.data.parentnodeid != null){
			// Validate if there is a grandparent node
			parentNode = mapOfNodeIdToNode.get(thisNode.data.parentnodeid);
			if (parentNode != null && parentNode.data.parentnodeid != null){
				grandparentNode = mapOfNodeIdToNode.get(parentNode.data.parentnodeid);	
				if (grandparentNode != null){
					// now validate if the grandparent node is the grandparent stored on the grandchild node
					if (grandparentNode.data.accid != thisNode.data.grandparentid){
						// return false, as in this context, child should not be shown
						retVal = 'hide';
					} else {
						retVal = 'show';
					}
				}
			}
		}
		return retVal;
	}

	// Get active relationships of Accounts being the Child Account (get parents), excluding a list of Ids
	private List<Affiliation__c> getParentRelationshipsExcludingAff(Set<Id> setOfAccntId, Set<Id> setOfAffId){
		return [select
					Affiliation_From_Account__c,
					Affiliation_To_Account__c
			   from
					Affiliation__c
			   where
			   		RecordType.Name = :ut_StaticValues.RTDEVNAME_AFF_A2A AND
			   		Id NOT IN :setOfAffId AND
					Affiliation_From_Account__c in :setOfAccntId AND
					(Affiliation_Start_Date__c <= :System.today() AND (Affiliation_End_Date__c >= :System.today() OR Affiliation_End_Date__c = null))
			   ];		
	}

	// Get active parent relationships of one Account
	private List<Affiliation__c> getParentRelationships(Id accntId){
		return [select
					Affiliation_From_Account__c,
					Affiliation_To_Account__c
			   from
					Affiliation__c
			   where
			   		RecordType.Name = :ut_StaticValues.RTDEVNAME_AFF_A2A AND
					Affiliation_From_Account__c = :accntId AND
					(Affiliation_Start_Date__c <= :System.today() AND (Affiliation_End_Date__c >= :System.today() OR Affiliation_End_Date__c = null))
			   ];		
	}
	
	// Get active relationships of Accounts being the Child Account (get parents)
	private List<Affiliation__c> getParentRelationships(Set<Id> setOfAccntId){
		return [select
					Affiliation_End_Date__c,
					Affiliation_From_Account__c,
					Affiliation_From_Account__r.Account_City__c,
					Affiliation_From_Account__r.Account_Country_vs__c,
					Affiliation_From_Account__r.Name,
					Affiliation_From_Account__r.SAP_ID__c,
					Affiliation_To_Account__c,
					Affiliation_To_Account__r.Account_City__c,
					Affiliation_To_Account__r.Name,
					Affiliation_To_Account__r.SAP_ID__c,
					Affiliation_Type__c,
					To_Grandparent__c
			   from
					Affiliation__c
			   where
			   		RecordType.Name = :ut_StaticValues.RTDEVNAME_AFF_A2A AND
					Affiliation_From_Account__c in :setOfAccntId AND
					(Affiliation_Start_Date__c <= :System.today() AND (Affiliation_End_Date__c >= :System.today() OR Affiliation_End_Date__c = null))
			   order by Affiliation_To_Account__r.Name
			   		
			   ];		
	}

	// get Active child relationships of give Account
	private List<Affiliation__c> getChildRelationships(Set<Id> setOfAccntId){
		return [select
					Affiliation_End_Date__c,
					Affiliation_From_Account__c,
					Affiliation_From_Account__r.Account_City__c,
					Affiliation_From_Account__r.Account_Country_vs__c,
					Affiliation_From_Account__r.Name,
					Affiliation_From_Account__r.SAP_ID__c,
					Affiliation_To_Account__c,
					Affiliation_To_Account__r.Account_City__c,
					Affiliation_To_Account__r.Name,
					Affiliation_To_Account__r.SAP_ID__c,
					Affiliation_Type__c,
					To_Grandparent__c
			    from
					Affiliation__c
			    where
			    	RecordType.Name = :ut_StaticValues.RTDEVNAME_AFF_A2A AND
					Affiliation_To_Account__c in :setOfAccntId AND
					(Affiliation_Start_Date__c <= :System.today() AND (Affiliation_End_Date__c >= :System.today() OR Affiliation_End_Date__c = null))
				order by 
					Affiliation_From_Account__r.Name
			   ];		
	}

	// Class for storing treeNode data
	public class treeNode{
		
		public String id {get;set;}
		public String name {get;set;}
		public treeNodeData data {get;set;}
		public List<treeNode> children {get;set;}

		public treeNode(Affiliation__c thisAff, bl_Relationship.accountProperties accntProps, String nodeType, boolean showInHierarchy){
			
			this.data = new treeNodeData();
			if (nodeType == 'topNode' || nodeType == 'parentNode'){
				this.Id = thisAff.Affiliation_From_Account__c + '' + thisAff.Affiliation_To_Account__c;
				System.debug('top node Id in constructor '+this.Id);
				this.name = thisAff.Affiliation_To_Account__r.Name;
				this.data.accid = thisAff.Affiliation_To_Account__c;
				this.data.accname = thisAff.Affiliation_To_Account__r.Name;
				this.data.accsapid = thisAff.Affiliation_To_Account__r.SAP_ID__c;
				this.data.acccity = thisAff.Affiliation_To_Account__r.Account_City__c == null ? '' : thisAff.Affiliation_To_Account__r.Account_City__c;
			} else if (nodeType == 'childNode'){
				this.Id = thisAff.Affiliation_From_Account__c + '' + thisAff.Affiliation_To_Account__c;
				this.name = thisAff.Affiliation_From_Account__r.Name;
				this.data.accid = thisAff.Affiliation_From_Account__c;
				this.data.accname = thisAff.Affiliation_From_Account__r.Name;
				this.data.accsapid = thisAff.Affiliation_From_Account__r.SAP_ID__c;
				this.data.acccity = thisAff.Affiliation_From_Account__r.Account_City__c == null ? '' : thisAff.Affiliation_From_Account__r.Account_City__c;
				this.data.grandparentid = thisAff.To_Grandparent__c;				
			}
			this.data.color_for_json = accntProps.htmlColor;
			this.data.reltype = thisAff.Affiliation_Type__c;
			this.data.rootNode = nodeType == 'topNode' ? true : false;
	        this.data.showinfolink = accntProps.showLink;
	        this.data.showatall = accntProps.showInHierarchy;				
			this.data.mdname = accntProps.masterDataName == null ? '' : accntProps.masterDataName;
			this.data.multipleparents = accntProps.multipleParents;
			this.data.showInHierarchy = showInHierarchy;
		}
	}
	
	// Class for storing tree node sub data
	public class treeNodeData{
		
		public String accid {get;set;}
		public String accname {get;set;}
		public String accsapid { get;set; }
		public String acccity {get;set;}
		// object will be formatted to json and then replace in string to get to $color
		public String color_for_json {get;set;}
		public String grandparentid {get;set;}
		public String mdname {get;set;}
		public String parentnodeid {get;set;}
		public Boolean hasnograndparent {get;set;}
		public String reltype {get;set;}
		public Boolean rootnode {get;set;}
        public Boolean showinfolink {get;set;}
        public Boolean showatall {get;set;}
        public Boolean multipleparents {get;set;}
        public Boolean showInHierarchy {get;set;}
		
		public treeNodeData(){}
	}

}