//---------------------------------------------------------------------------------------------------------------------------------------------------
//  Author		:    Bart Caelen
//  Date 		:    20150429
//  Description	:    CR-8047 - This batch/scheduled Class will get Campaign and Campaign Member data from Cvent
//---------------------------------------------------------------------------------------------------------------------------------------------------
//  Author		:    Bart Caelen
//  Date 		:    20150810
//  Description	:    CR-8897 - Update CVent Event with data from Salesforce.com Campaign
//						Execution logic:
//						- process CampaignMembers
//						- update CVent Events
//						- process Campaigns
//						- update CVent Contacts - Not Available For Registration
//---------------------------------------------------------------------------------------------------------------------------------------------------
//  Author		:    Bart Caelen
//  Date 		:    05/03/2018
//  Description	:    CR-15745 - Removed the scheduled logic and moved this to 2 separated scheduled APEX Classes (sc_Cvent & sc_Cvent_EMEA) to be able
//						to integratie with the 2 CVent Environments
//---------------------------------------------------------------------------------------------------------------------------------------------------
global class ba_Cvent implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
	
	global Integer iBatchSize = 100;
	global String tProcessType;
	global Id idCampaignRecordtype_Cvent;
	global Integer iSOQLLimit = 100;
	global Integer iBatchSeq = 0;
	global Map<Integer, List<String>> mapData_CVentContactId;

	global String tCventAPI = 'CVENT';

	global Date dFilterDate = System.Date.Today().addDays(-90);

	// BATCH Settings
	global Database.QueryLocator start(Database.BatchableContext BC) {

		String tSOQL = '';

		Boolean bRunningInASandbox = clsUtil.bRunningInASandbox();

		if (tProcessType == 'CAMPAIGN'){

			tSOQL = 'SELECT Id, Cvent_Event_Id__c, Status';
			tSOQL += ' FROM Campaign';
			tSOQL += ' WHERE RecordtypeId = :idCampaignRecordtype_Cvent';
			tSOQL += ' AND (';
				tSOQL += ' Status not in (\'Completed\', \'Aborted\', \'Cancelled\')';
				tSOQL += ' OR (';
					tSOQL += ' Status in (\'Completed\') AND EndDate >= :dFilterDate';
				tSOQL += ' )';
			tSOQL += ' )';

			if (bRunningInASandbox){
				tSOQL += ' AND Sync_With_Cvent__c = TRUE';
			}
			if (Test.isRunningTest()){
				tSOQL += ' LIMIT ' + String.valueOf(iBatchSize);
			}

		}else if (tProcessType == 'CAMPAIGNMEMBER'){

			tSOQL = 'SELECT Id, ContactId, Contact.Email, LeadId, Lead.Email, Registration_Type__c, CampaignId, Campaign.Cvent_Event_Id__c, Campaign.Status, Campaign.StartDate, Campaign.EndDate, Status ';
			tSOQL += ' FROM CampaignMember';
			tSOQL += ' WHERE Campaign.RecordtypeId = :idCampaignRecordtype_Cvent';
			tSOQL += ' AND (';
				tSOQL += ' Campaign.Status not in (\'Completed\', \'Aborted\', \'Cancelled\')';
				tSOQL += ' OR (';
					tSOQL += ' Campaign.Status in (\'Completed\') AND Campaign.EndDate >= :dFilterDate';
				tSOQL += ' )';
			tSOQL += ' )';

			if (bRunningInASandbox){
				tSOQL += ' AND Campaign.Sync_With_Cvent__c = TRUE';
			}
			tSOQL += ' ORDER BY CampaignId';
			if (Test.isRunningTest()){
				tSOQL += ' LIMIT ' + String.valueOf(iBatchSize);
			}

		}else if (tProcessType == 'UPDATECVENTEVENT'){
			tSOQL = 'SELECT Id, Cvent_Event_Id__c, Status, HCP_Seats__c, Staff_Seats__c, Total_Delegate__c, Total_MDT_Staff__c';
			tSOQL += ' FROM Campaign';
			tSOQL += ' WHERE RecordtypeId = :idCampaignRecordtype_Cvent';
			tSOQL += ' AND Send_to_CVent__c = true';
			if (bRunningInASandbox){
				tSOQL += ' AND Sync_With_Cvent__c = TRUE';
			}
			if (Test.isRunningTest()){
				tSOQL += ' LIMIT ' + String.valueOf(iBatchSize);
			}
		
		}else if (tProcessType == 'UPDATECVENTCONTACT'){

			// This is a dummy selection to be able to perform max x callouts (determined by iSOQLLimit) to update CVent Contacts.
			//	We can't select the corresponding SFDC Contacts so we just select x CVent Campaigns which will be "processed" (but not realy) in batches of 1.
			//	This makes it possible to perform x callouts to update CVent Contact Data with a max of 200 contacts per callout.
			tSOQL = 'SELECT Id';
			tSOQL += ' FROM Campaign';
			tSOQL += ' WHERE RecordtypeId = :idCampaignRecordtype_Cvent';
			if (Test.isRunningTest()){
				tSOQL += ' LIMIT ' + String.valueOf(iBatchSize);
			}else{
				tSOQL += ' LIMIT ' + iSOQLLimit;
			}

		}

		System.debug('**BC** tSOQL (' + tProcessType + ') : ' + tSOQL);

		return Database.getQueryLocator(tSOQL);

	}

	global void execute(Database.BatchableContext BC, List<sObject> scope) {

		if (tProcessType == 'CAMPAIGN'){

			List<Campaign> lstCampaign = (List<Campaign>)scope;
			if (!Test.isRunningTest()){
				bl_Cvent.processCampaign(lstCampaign, tCventAPI);
			}

		}else if (tProcessType == 'CAMPAIGNMEMBER'){

			List<CampaignMember> lstCampaignMember = (List<CampaignMember>)scope;
			if (!Test.isRunningTest()){
				bl_Cvent.processCampaignMember(lstCampaignMember, tCventAPI);
			}

		}else if (tProcessType == 'UPDATECVENTEVENT'){

			List<Campaign> lstCampaign = (List<Campaign>)scope;
			if (!Test.isRunningTest()){
				bl_Cvent.updateCventEvent(lstCampaign, tCventAPI);
			}

		}else if (tProcessType == 'UPDATECVENTCONTACT'){

			iBatchSeq++;

			if (!Test.isRunningTest()){
				bl_Cvent.updateCventContact_NotAvailableForRegistration(mapData_CVentContactId.get(iBatchSeq), tCventAPI);
			}

		}

	}
		
	global void finish(Database.BatchableContext BC) {

		if (tProcessType == 'CAMPAIGNMEMBER'){

			ba_Cvent oBatch = new ba_Cvent();
				oBatch.tProcessType = 'CAMPAIGN';
				oBatch.tCventAPI = tCventAPI;
				oBatch.idCampaignRecordtype_Cvent = idCampaignRecordtype_Cvent;
			if (!Test.isRunningTest()) Database.executebatch(oBatch, iBatchSize);

		}else if (tProcessType == 'CAMPAIGN'){

			ba_Cvent oBatch = new ba_Cvent();
				oBatch.tProcessType = 'UPDATECVENTEVENT';
				oBatch.tCventAPI = tCventAPI;
				oBatch.idCampaignRecordtype_Cvent = idCampaignRecordtype_Cvent;
			if (!Test.isRunningTest()) Database.executebatch(oBatch, iBatchSize);

		}else if (tProcessType == 'UPDATECVENTEVENT'){

			// Determine the number of CVent Contacts that we need to update - this performs 1 API Callout
			List<String> lstCVentId = new List<String>();
			if (!Test.isRunningTest()) lstCVentId = bl_Cvent.getCventContactIdToUpdate_NotAvailableForRegistration(tCventAPI);
	        mapData_CVentContactId = clsUtil.splitDataList(lstCVentId, bl_Cvent.iCventAPILImit_Update);

	        // The number of batches we need to execute to update all CVent Contacts
			iSOQLLimit = mapData_CVentContactId.keySet().size();

			// We pass the mapData_CVentContactId to the new Batch Job so that we don't need to get the Cvent Contacts that need to be update in each batch we execute.
			ba_Cvent oBatch = new ba_Cvent();
				oBatch.tProcessType = 'UPDATECVENTCONTACT';
				oBatch.tCventAPI = tCventAPI;
				oBatch.idCampaignRecordtype_Cvent = idCampaignRecordtype_Cvent;
				oBatch.iSOQLLimit = iSOQLLimit;
				oBatch.mapData_CVentContactId = mapData_CVentContactId;
			if (!Test.isRunningTest()) Database.executebatch(oBatch, 1);

		}else if (tProcessType == 'UPDATECVENTCONTACT'){

			// THE END

		}

	}

}