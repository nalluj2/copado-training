/**
 * Creation Date :  20090323
 * Description :    Test Coverage for trigger ImplantBeforeInsert
 * Author :         ABSI - BC
 * Modify Date :    20110803
 * Description :    Chnages related to Implant management
 * Author :         Wipro - Dheeraj
 */
 
@isTest
private class TEST_triggerImplantBeforeInsert {

    static testMethod void myUnitTest() {
        System.debug(' ################## ' + 'BEGIN TEST_triggerImplantBeforeInsert' + ' ################');
        // Create test sObjects 
        Account acc1 = new Account();
        acc1.Name = 'TEST_triggerImplantBeforeInsert TestAccount1' ; 
        insert acc1;

        Account acc2 = new Account();
        acc2.Name = 'TEST_triggerImplantBeforeInsert TestAccount2' ; 
        insert acc2;
        
        Contact cont1 = new Contact();
        cont1.LastName = 'TEST_triggerImplantBeforeInsert TestCont1' ;  
		cont1.FirstName = 'test';
        cont1.AccountId = acc1.Id ;
        cont1.Contact_Active__c = true  ;
        cont1.Contact_Department__c = 'Diabetes Adult'; 
        cont1.Contact_Primary_Specialty__c = 'ENT';
        cont1.Affiliation_To_Account__c = 'Employee';
        cont1.Primary_Job_Title_vs__c = 'Manager';
        cont1.Contact_Gender__c = 'Male'; 
        insert cont1;
        
        Contact cont2 = new Contact();
        cont2.LastName = 'TEST_triggerImplantBeforeInsert TestCont2';  
		cont2.FirstName = 'test';
        cont2.AccountId = acc1.Id ;
        cont2.Contact_Active__c = true;
        cont2.Contact_Department__c = 'Diabetes Adult'; 
        cont2.Contact_Primary_Specialty__c = 'ENT';
        cont2.Affiliation_To_Account__c = 'Employee';
        cont2.Primary_Job_Title_vs__c = 'Manager';
        cont2.Contact_Gender__c = 'Male'; 
        insert cont2;
        
        Contact cont3 = new Contact();
        cont3.LastName = 'TEST_triggerImplantBeforeInsert TestCont3';  
		cont3.FirstName = 'test';
        cont3.AccountId = acc1.Id ;
        cont3.Contact_Active__c = true;
        cont3.Contact_Department__c = 'Diabetes Adult'; 
        cont3.Contact_Primary_Specialty__c = 'ENT';
        cont3.Affiliation_To_Account__c = 'Employee';
        cont3.Primary_Job_Title_vs__c = 'Manager';
        cont3.Contact_Gender__c = 'Male'; 
        insert cont3;       
        
        List<Contact> testContacts = new List<Contact>();
        testContacts.add(cont1);
        testContacts.add(cont2);
        testContacts.add(cont3);
        
        List<Account> testAccounts = new List<Account>();
        testAccounts.add(acc1);
        testAccounts.add(acc2);
        
        Affiliation__c aff = new Affiliation__c();
        aff.Affiliation_To_Contact__c = testContacts.get(0).Id;
        aff.Affiliation_To_Account__c = testAccounts.get(0).Id;
        aff.Affiliation_From_Contact__c = testContacts.get(1).Id;
        aff.Affiliation_From_Account__c = testAccounts.get(1).Id;
        aff.Affiliation_Type__c = 'Referring';
        aff.RecordTypeId = FinalConstants.recordTypeIdC2A;
        insert aff;
        
        Affiliation_Details__c affDetails = new Affiliation_Details__c();
        affDetails.Affiliation_Details_Affiliation__c = aff.Id;
        affDetails.Affiliation_Details_Start_Date__c = Date.newInstance(Date.today().year(), 1, 1);
        affDetails.Affiliation_Details_End_Date__c = Date.newInstance(Date.today().year(), 12, 31);
        insert affDetails;

        //Insert Company
        Company__c cmpny = new Company__c();
        cmpny.name='TestMedtronic';
        cmpny.CurrencyIsoCode = 'EUR';
        cmpny.Current_day_in_Q1__c=56;
        cmpny.Current_day_in_Q2__c=34; 
        cmpny.Current_day_in_Q3__c=5; 
        cmpny.Current_day_in_Q4__c= 0;   
        cmpny.Current_day_in_year__c =200;
        cmpny.Days_in_Q1__c=56;  
        cmpny.Days_in_Q2__c=34;
        cmpny.Days_in_Q3__c=13;
        cmpny.Days_in_Q4__c=22;
        cmpny.Days_in_year__c =250;
        cmpny.Company_Code_Text__c = 'T30';
        insert cmpny; 

        Business_Unit__c bu = new Business_Unit__c();
        bu.name = 'Testing BU';
        bu.abbreviated_name__c = 'Abb Bu';
        bu.Company__c = cmpny.id;
        insert bu;

        //Insert SBU
        Sub_Business_Units__c sbu1 = new Sub_Business_Units__c();
        sbu1.name='SBUMedtronic1';
        sbu1.Business_Unit__c=bu.id;
        insert sbu1;

        //Insert Therapy Group
        Therapy_Group__c tg = new Therapy_Group__c();
        tg.Company__c=cmpny.id;
        tg.Name='Therapy Group';
        tg.Sub_Business_Unit__c=sbu1.id;
        insert tg;

        Therapy__c th = new Therapy__c();
        th.name='test ## th';
        th.Business_Unit__c = bu.id;
        th.Sub_Business_Unit__c = sbu1.Id;
        th.Therapy_Group__c = tg.id;
        th.Therapy_Name_Hidden__c = 'test ## th'; 
        insert th;

		Call_Records__c CR = clsTestData_CallRecord.createCallRecord()[0];
//        Call_Records__c CR = [select id,name from Call_Records__c limit 1]; 
        
        Implant__c MyImp = new Implant__c();
        MyImp.Implant_Implanting_Contact__c = testContacts.get(1).Id;
        MyImp.Implant_Implanting_Account__c = testAccounts.get(0).Id;
        MyImp.Implant_Therapy__c = th.Id;
        MyImp.Therapy__c = th.Id;
        MyImp.Attended_Implant_Text__c = 'UnAttended';
        MyImp.Duration_Nr__c = 10;
        MyImp.Call_Record_ID__c = CR.id;
        insert MyImp;
    
        MyImp.Attended_Implant_Text__c = 'Attended';
        MyImp.Duration_Nr__c = 20;
        update MyImp;
    
        ApexPages.currentPage().getParameters().put('id',MyImp.id);
        ApexPages.StandardController newSct = new ApexPages.Standardcontroller(MyImp);
        ControllerImplantCustomDelete impController = new ControllerImplantCustomDelete(newSct);

        try{
            impController.DelImplant();
            undelete MyImp;
        }
        catch (exception e) { }

        // Make implant as MMx Implant
        MyImp.MMX_Implant_ID_Text__c = 'MMX Imp';
        //update MyImp;

        ApexPages.currentPage().getParameters().put('id',MyImp.id);
        ApexPages.StandardController newSct1 = new ApexPages.Standardcontroller(MyImp);
        ControllerImplantCustomDelete impController1 = new ControllerImplantCustomDelete(newSct1);      
        try{
            impController1.DelImplant();
            undelete MyImp;
        }
        catch (exception e) { }


        // Remove MMx Implant & Call Record Reference
        Implant__c MyImp1 = new Implant__c();
        MyImp1.Implant_Implanting_Contact__c = testContacts.get(1).Id;
        MyImp1.Implant_Implanting_Account__c = testAccounts.get(0).Id;
        MyImp1.Implant_Therapy__c = th.Id;
        MyImp1.Therapy__c = th.Id;
        MyImp1.Attended_Implant_Text__c = 'UnAttended';
        MyImp1.Duration_Nr__c = 10;
        insert MyImp1;
        
        ApexPages.currentPage().getParameters().put('id',MyImp1.id);
        ApexPages.StandardController newSct2 = new ApexPages.Standardcontroller(MyImp1);
        ControllerImplantCustomDelete impController2 = new ControllerImplantCustomDelete(newSct2);      
        try{
            impController2.setCanCloseWindow(true);
            boolean canclose=impController2.getCanCloseWindow();
            impController2.setShowOkButton(true);
            boolean showButton = impController2.getShowOkButton();
            string getwartest = impController2.getWarntext();
            impController2.DelImplant();
            undelete MyImp;
        }
        catch (exception e) { }
        
        /*List<Implant__c> implants = new List<Implant__c>();
        
        Implant__c implant;
        implant = new Implant__c();
        implant.Implant_Implanting_Contact__c = testContacts.get(0).Id;
        implant.Implant_Implanting_Account__c = testAccounts.get(0).Id;
        //implant.Implant_Referring_Contact__c = testContacts.get(1).Id;
        //implant.Implant_Referring_Account__c = testAccounts.get(1).Id;
        implant.Implant_Therapy__c = th.Id;
        implant.Therapy__c = th.Id;
        implant.Therapy_Text__c = th.name;
        implant.Attended_Implant_Text__c = 'UnAttended';
        implant.Duration_Nr__c = 10;
        implant.Business_Unit__c = bu.Id;
        implant.Business_Unit_Text__c = bu.Name;
        
        implants.add(implant);
        
        implant = new Implant__c();
        implant.Implant_Implanting_Contact__c = testContacts.get(0).Id;
        implant.Implant_Implanting_Account__c = testAccounts.get(0).Id;
        //implant.Implant_Referring_Contact__c = testContacts.get(1).Id;
        //implant.Implant_Referring_Account__c = testAccounts.get(1).Id;
        implant.Implant_Therapy__c = th.Id;
        implant.Therapy__c = th.Id;
        implant.Therapy_Text__c = th.name;
        implant.Attended_Implant_Text__c = 'UnAttended';
        implant.Duration_Nr__c = 10;
        implant.Business_Unit__c = bu.Id;
        implant.Business_Unit_Text__c = bu.Name;        
        implants.add(implant);
        
        implant = new Implant__c();
        implant.Implant_Implanting_Contact__c = testContacts.get(1).Id;
        implant.Implant_Implanting_Account__c = testAccounts.get(1).Id;
        //implant.Implant_Referring_Contact__c = testContacts.get(0).Id;
        //implant.Implant_Referring_Account__c = testAccounts.get(0).Id;
        implant.Implant_Therapy__c = th.Id;
        implant.Therapy__c = th.Id;
        implant.Therapy_Text__c = th.name;
        implant.Attended_Implant_Text__c = 'UnAttended';
        implant.Duration_Nr__c = 10;
        implant.Business_Unit__c = bu.Id;
        implant.Business_Unit_Text__c = bu.Name;        
        implants.add(implant);      
        
        // TEST INSERTING MULTIPLE IMPLANTS
        insert implants;
        
        
        // TEST INSERTING 1 IMPLANT
        implant = new Implant__c();
        implant.Implant_Implanting_Contact__c = testContacts.get(0).Id;
        implant.Implant_Implanting_Account__c = testAccounts.get(0).Id;
        //implant.Implant_Referring_Contact__c = testContacts.get(1).Id;
        //implant.Implant_Referring_Account__c = testAccounts.get(1).Id;
        implant.Implant_Therapy__c = th.Id;
        implant.Therapy__c = th.Id;
        implant.Therapy_Text__c = th.name;
        implant.Attended_Implant_Text__c = 'UnAttended';
        implant.Duration_Nr__c = 10;
        implant.Business_Unit__c = bu.Id;
        implant.Business_Unit_Text__c = bu.Name;
        
        
        insert implant;
        delete implant;

        /*implant = new Implant__c();
        implant.Implant_Implanting_Contact__c = testContacts.get(1).Id;
        implant.Implant_Implanting_Account__c = testAccounts.get(1).Id;
        implant.Implant_Referring_Contact__c = testContacts.get(2).Id;
        implant.Implant_Referring_Account__c = testAccounts.get(1).Id;
        
        insert implant;
        delete implant;
        
        implant = new Implant__c();
        implant.Implant_Implanting_Contact__c = testContacts.get(0).Id;
        implant.Implant_Implanting_Account__c = testAccounts.get(0).Id;
    
        insert implant;
        delete implant;*/
        
        System.debug(' ################## ' + 'END TEST_triggerImplantBeforeInsert' + ' ################');
    }
}