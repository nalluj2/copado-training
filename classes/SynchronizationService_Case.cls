public without sharing class SynchronizationService_Case implements SynchronizationService_Object{
    
    //Set with the external Ids of the records already processed in this transaction. Used to avoid generating multiple notifications for the same change
    private Set<String> processedRecords = new Set<String>();
        
    //This method returns the External Ids of the records that have relevant modifications for the target Org. This includes created records or 
    //modified records where synchronized fields have changed. 
    public Set<String> hasChangeForNotification(List<SObject> records, Map<Id, SObject> oldRecords){
        
        Set<String> result = new Set<String>();
                
        for(SObject rawObject : records){
            
            Case record = (Case) rawObject;
            
            if(processedRecords.contains(record.External_Id__c) ) continue;
            
            //On Insert we always send notification
            if(oldRecords == null){
                
                result.add(record.External_Id__c);
                processedRecords.add(record.External_Id__c);
                
            }//If Update we only send notification when relevant fields are modified
            else{
                
                if( bl_SynchronizationService_Utils.hasChanged( record, oldRecords.get(record.Id), syncFields ) == true ){
                    
                    result.add(record.External_Id__c);
                    processedRecords.add(record.External_Id__c);
                }
            }
        }
        
        return result;
    }
    
    //This method collects and returns the complete information for a record. This includes the records itself as well as any related record that needs
    //to exist in the target Org too. The returned format is JSON serialized string.
    public String generatePayload(String externalId){
        
        CaseSyncPayload payload = new CaseSyncPayload();
        
        String query =  'SELECT ' + String.join( syncFields, ',') +
                            ',Account.SAP_Id__c, Account.AccountNumber, Account.RecordType.DeveloperName, Contact.External_Id__c, Asset.Serial_Nr__c, If_yes_select_Autoguide_Serial_Number__r.Serial_Nr__c' +
                            ',Surgeon__r.External_Id__c, Site_Contact_if_refused__r.External_Id__c, RecordType.DeveloperName,  Complaint__r.External_Id__c, Escalation_Owner__r.Alias' +
                            ',Initial_Reporter__r.External_Id__c ' + 
                        'FROM Case WHERE External_Id__c = :externalId LIMIT 2';     
        
        List<Case> cases = Database.query(query);
        
        //If no record or more than 1 is found, we return an empty response
        if(cases.size() == 1){
            
            Case caseRecord = cases[0]; 
            bl_SynchronizationService_Utils.prepareForJSON(caseRecord, syncFields);
                    
            caseRecord.Id = null;//The local SFDC Id is not relevant in the target Org
                                    
            payload.CaseRecord = caseRecord;    
            
            Set<Id> accountIds = new Set<Id>();
            Set<Id> contactIds = new Set<Id>();
            
            if(caseRecord.Account != null){
                
                accountIds.add(caseRecord.AccountId);               
                
                if ( (caseRecord.Account.RecordType.DeveloperName == 'SAP_Account') || (caseRecord.Account.RecordType.DeveloperName == 'Distributor') ){
                    payload.accountId = caseRecord.Account.SAP_Id__c; 
                }else{
                    payload.accountId = caseRecord.Account.AccountNumber;
                }
                caseRecord.Account = null;//If we send this reference to the target Org it may cause problems in the upsert dml
                caseRecord.AccountId = null;//The local SFDC Id is not relevant in the target Org
            }
            
            if(caseRecord.Contact != null){
                
                contactIds.add(caseRecord.ContactId);
                
                payload.ContactId = caseRecord.Contact.External_Id__c;              
                caseRecord.Contact = null;//If we send this reference to the target Org it may cause problems in the upsert dml
                caseRecord.ContactId = null;//The local SFDC Id is not relevant in the target Org
            }
            
            if(caseRecord.Initial_Reporter__r != null){
                
                contactIds.add(caseRecord.Initial_Reporter__c);
                
                payload.InitialReporterId = caseRecord.Initial_Reporter__r.External_Id__c;              
                caseRecord.Initial_Reporter__r = null;//If we send this reference to the target Org it may cause problems in the upsert dml
                caseRecord.Initial_Reporter__c = null;//The local SFDC Id is not relevant in the target Org
            }
            
            if(caseRecord.Surgeon__r != null){
                
                contactIds.add(caseRecord.Surgeon__c);
                
                payload.SurgeonId = caseRecord.Surgeon__r.External_Id__c;               
                caseRecord.Surgeon__r = null;//If we send this reference to the target Org it may cause problems in the upsert dml
                caseRecord.Surgeon__c = null;//The local SFDC Id is not relevant in the target Org
            }
            
            if(caseRecord.Site_Contact_if_refused__r != null){
                
                contactIds.add(caseRecord.Site_Contact_if_refused__c);
                
                payload.SiteContactId = caseRecord.Site_Contact_if_refused__r.External_Id__c;               
                caseRecord.Site_Contact_if_refused__r = null;//If we send this reference to the target Org it may cause problems in the upsert dml
                caseRecord.Site_Contact_if_refused__c = null;//The local SFDC Id is not relevant in the target Org
            }
            
            if(caseRecord.Asset != null){
                payload.AssetId = caseRecord.Asset.Serial_Nr__c;                
                caseRecord.Asset = null;//If we send this reference to the target Org it may cause problems in the upsert dml
                caseRecord.AssetId = null;//The local SFDC Id is not relevant in the target Org
            }
            
            if(caseRecord.If_yes_select_Autoguide_Serial_Number__r != null){
                payload.relatedSysId = caseRecord.If_yes_select_Autoguide_Serial_Number__r.Serial_Nr__c;                
                caseRecord.If_yes_select_Autoguide_Serial_Number__r = null;//If we send this reference to the target Org it may cause problems in the upsert dml
                caseRecord.If_yes_select_Autoguide_Serial_Number__c = null;//The local SFDC Id is not relevant in the target Org
            }
                                    
            if(caseRecord.Complaint__r != null){
                payload.ComplaintId = caseRecord.Complaint__r.External_Id__c;               
                caseRecord.Complaint__r = null;//If we send this reference to the target Org it may cause problems in the upsert dml
                caseRecord.Complaint__c = null;//The local SFDC Id is not relevant in the target Org
            }
            
            if(caseRecord.Escalation_Owner__r != null){
                payload.EscalationOwner = caseRecord.Escalation_Owner__r.Alias;             
                caseRecord.Escalation_Owner__r = null;//If we send this reference to the target Org it may cause problems in the upsert dml
                caseRecord.Escalation_Owner__c = null;//The local SFDC Id is not relevant in the target Org
            }
            
            payload.RecordType = caseRecord.RecordType.DeveloperName;
            caseRecord.RecordTypeId = null;
            caseRecord.RecordType = null;   
                                    
            payload.generateAccountPayload(accountIds, contactIds); 
            
            //Done in a separate query because we cannot clear the value from the CreatedDate field before sending it to the target
            Case caseDetails = [Select CaseNumber, CreatedDate, CreatedBy.Name, LastModifiedDate, LastModifiedBy.Name from Case where External_Id__c = :externalId];
            
            payload.CaseName = caseDetails.CaseNumber;          
            payload.CreatedDate = caseDetails.CreatedDate;          
            payload.CreatedBy = caseDetails.CreatedBy.Name;                 
            payload.LastModifiedDate = caseDetails.LastModifiedDate;            
            payload.LastModifiedBy = caseDetails.LastModifiedBy.Name;           
        }
        
        return JSON.serialize(payload);
    }
    
    //Implementation needed only in Target Org
    public String processPayload(String inputPayload){
        
        CaseSyncPayload payload = (CaseSyncPayload) JSON.deserialize(inputPayload, CaseSyncPayload.class);
        
        //If there was a problem in the source generating the information (no record or multiple records found) we finish here 
        if(payload.CaseRecord == null) return null;
        
        SavePoint sp = Database.setSavePoint();
        
        try{
            
            Case caseRecord = payload.caseRecord;
            
            Complaint__c caseComplaint;
            //Complaint related to the Case     
            if(payload.ComplaintId != null){
                
                //Manual External Id mapping
                List<Complaint__c> caseComplaints = [Select Id, Case__c from Complaint__c where External_Id__c = :payload.ComplaintId LIMIT 2];
                
                if(caseComplaints.size() == 1){
                    
                    caseComplaint = caseComplaints[0];
                    caseRecord.Complaint__c = caseComplaint.Id;
                    
                }else{
                    
                    if(caseComplaints.size() == 0) throw new bl_SynchronizationService_Utils.SyncService_Exception('No related Complaint found for External Id : ' + payload.ComplaintId);
                    else  throw new bl_SynchronizationService_Utils.SyncService_Exception('Multiple related Complaint found for External Id : ' + payload.ComplaintId);                 
                }
                
            }else if(caseRecord.Complaint_Case__c != null && caseRecord.Complaint_Case__c == 'Yes'){
                
                 throw new bl_SynchronizationService_Utils.SyncService_Exception('Complaint is a mandatory if field "Complaint Case?" is equal to "Yes"');
            }
            
            caseRecord.LastModifiedDate__c = payload.LastModifiedDate;
            caseRecord.LastModifiedBy__c = payload.LastModifiedBy;
            
            //Manual Upsert operation               
            upsert caseRecord External_Id__c;
            
            //Link back the Complaint to the Case if that is not the case already
            if(caseComplaint != null && caseComplaint.Case__c != caseRecord.Id){
                
                caseComplaint.Case__c = caseRecord.Id;
                update caseComplaint;
            }
            
            return caseRecord.Id;
            
        }catch(Exception e){
            
            Database.rollback(sp);
            
            throw e;
        }
        
        return null;
    }
    
    //Model classes for the requests and responses. To be used by the JSON serialize/deserialize
    public class CaseSyncPayload extends SynchronizationService_Payload{
        
        public Case caseRecord {get; set;}
        public String caseName {get; set;}
        public String accountId {get; set;}
        public String contactId {get; set;}
        public String initialReporterId {get; set;}        
        public String surgeonId {get; set;}
        public String siteContactId {get; set;}
        public String assetId {get; set;}
        public String relatedSysId {get; set;}       
        public String escalationOwner {get; set;}
        public String complaintId {get; set;}
        public String recordType {get; set;}
        public Datetime createdDate {get; set;}
        public String createdBy {get; set;}
        public Datetime lastModifiedDate {get; set;}
        public String lastModifiedBy {get; set;}
    }
    
    private static List<String> syncFields = new List<String>
    {
        'AccountId',
        'AssetId',
        'Asset_Record_Type__c',
        'Attachment_Last_Uploaded__c',
        'Case_Aborted__c',
        'Complaint_Case__c',
        'Complaint__c',
        'Complaint_Handling_Notified_Date__c',
        'ContactId',        
        'Corrective_Maintenance_Repair__c',
        'Customer_Communicate_Dissatisfaction__c',
        'Date_Complaint_Became_Known__c',        
        'Description', 
        'Estimated__c',         
        'Escalation_Action__c',
        'Escalation_Owner__c',
        'Escalation_Queue__c',
        'Escalation_Resolution_Action__c',
        'Escalation_Severity_US__c',
        'Event_Happen_During_Surgery__c',
        'External_Id__c',   
        'Has_Attachment__c',    
        'Initial_Reporter__c',
        'Initial_Reporter_Email__c',
        'Initial_Reporter_Occupation__c',
        'Initial_Reporter_Phone__c',
        'IsEscalated',
        'Length_of_Extended_Surgical_Time__c',
        'No_Work_Order_Justification__c',
        'Number_of_Unused_2D_Fluoro_Spins__c',          
        'Number_of_Unused_Enhanced_Spins__c', 
        'Number_of_Unused_HD_Spins__c',
        'Number_of_Unused_Standard_Spins__c',
        'Origin',  
        'Patient_Age_US__c',
        'Patient_Age_Units_US__c',
        'Patient_Date_of_Birth__c', 
        'Patient_Info_Status__c',
        'Patient_Gender_US__c',
        'Patient_ID_US__c',
        'Patient_Impact__c',
        'Patient_Weight_US__c',
        'Patient_Weight_Units_US__c',
        'PEI__c', 
        'Priority',
        'Probable_Cause__c', 
        'Reason_for_Delayed_Closure__c',
        'Reason_Not_Provided__c',
        'Reason',
        'RecordTypeId', 
        'Remote_Presence_or_V_Support_Utilized__c',
        'Rep_Present__c',
        'Resolution__c', 
        'Serious_Injury__c',
        'Site_Contact_if_refused__c',
        'Status',
        'Surgeon__c',       
        'Subject', 
        'SuppliedCompany',
        'SuppliedEmail',
        'SuppliedName',
        'SuppliedPhone',
        'System_Brought_Up__c',
        'System_Down_Start__c', 
        'Systems_Down__c', 
        'Troubleshooting__c', 
        'Type',
        'Type_of_Surgical_Procedure__c', 
        'Type_of_Surgical_Procedure_Other__c',      
        'Was_case_resolved_over_the_phone__c', 
        'Was_Medtronic_Imaging_Aborted__c', 
        'Was_Navigation_Aborted__c', 
        'Was_root_cause_or_req_part_identified__c', 
        'What_Software_Task_When_Issue_Occurred__c',
        'When_Issue_Occurred__c',

        'Camera_Cart_Serial_Number__c',
        'System_Not_Brought_Up_Reason__c',
        'Was_Stealth_Autoguide_used_during_case__c',
        'If_yes_select_Autoguide_Serial_Number__c'    
    };
}