public without sharing class bl_Support_Manage_User_SFDC {
	
	private ctrl_Support_Requests.SessionData session;
	
	public List<SelectOption> profileOptions {get; set;} 
	
	public bl_Support_Manage_User_SFDC( ctrl_Support_Requests.SessionData sData){
		
		this.session = sData;		
	}
	
	public PageReference requestUserChange(){
        
        newRequest('Change User Setup');
                
        profileOptions = new List<SelectOption>();
        
        for(Profile profile : [Select Name from Profile ORDER BY Name]){
            profileOptions.add(new SelectOption(profile.Name, profile.Name));           
        }
                        
        return Page.Support_Change_User_Setup;          
    }
    
    public PageReference requestResetPassword(){
        
        newRequest('Reset Password');
                        
        return Page.Support_Reset_Password;         
    }
    
    public PageReference requestMobileApp(){
        
        newRequest('Enable Mobile App');
                    
        return Page.Support_Enable_App;         
    }
    
    public PageReference requestReactivateUser(){
        
        newRequest('Re-activate User');
        
        return Page.Support_Reactivate_User;            
    }
    
    public PageReference requestDeactivation(){
        
        newRequest('De-activate User');
        session.request.Comment__c = 'Left Company'; // Default value set to Left Company
         
        return Page.Support_Deactivate_User;    
    }
    
    public void setupUserChanged(){
        
        if(session.request.Created_User__c !=null){
            
            User user = [Select Id, Profile.Name, CostCenter__c from User where Id=:session.request.Created_User__c];
                    
            session.request.Profile__c = user.Profile.Name;
            session.request.Cost_Center__c = user.CostCenter__c;
            
        }else{
            session.request.Profile__c = '';
            session.request.Cost_Center__c = '';
        }   
    }
    
    private void newRequest (String requestType)
    {
    	session.resetRequest();
        session.request.status__c='New';
        session.request.application__c='Salesforce.com';
        session.request.request_Type__c = requestType;
        
        session.comment = null;
        session.comments = null;
    }
    
    public void createSimpleUserRequest(){
                        
        if(session.request.Created_User__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'User is required'));
            return;
        }
        
        if(session.request.Request_Type__c == 'De-activate User' && session.request.Date_of_deactivation__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'De-activation Date is required'));
            return;
        }   
        
        if(session.request.Request_Type__c == 'Enable Mobile App' && (session.request.Mobile_App__c == null
            || session.request.Mobile_App__c == '')){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Mobile App is required'));
            return;
        }                               
                
        try{    
            
            session.request.Requestor_Email__c = session.uInfo.email;
            session.request.Requestor_Name__c = session.uInfo.firstName + ' ' + session.uInfo.lastName;
            insert session.request;
            
            session.comment = new User_Request_Comment__c();        
                        
        }catch(Exception e){
system.debug('### e = ' + e);        
            
            ApexPages.addMessages(e);
            if(Test.isRunningTest()) throw e;
            return;
        }
system.debug('### @ 4');        
        
        session.request = [Select Id, Name, Status__c, Assignee__c, Request_Type__c, created_User__c, Created_User__r.Name, Cost_Center__c, Profile__c,
                                                Date_of_Deactivation__c, Comment__c, Requestor_Email__c, Mobile_App__c, New_owner_of_opportunities__c from Create_User_Request__c where Id=:session.request.Id];
system.debug('### @ 5');        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request '+session.request.Name+' created successfully.'));  
system.debug('### @ 6');             
    }
    
    public void cancelDeactivateUser(){
        
        String prevStatus = session.request.Status__c;
        
        try{
            
            if(session.request.Status__c=='Scheduled' && session.request.Deactivation_Process_Id__c !=null)
            {                
            	System.abortJob(session.request.Deactivation_Process_Id__c);  
            }
            
            session.request.status__c='Cancelled';
            update session.request;
            
            session.saveCommentAndAttachment();
            
        }catch(Exception e){
            ApexPages.addMessages(e);
            session.request.status__c=prevStatus;
            if(Test.isRunningTest()) throw e;
            return;
        }
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.info, 'Request Cancelled successfully.'));        
    }
}