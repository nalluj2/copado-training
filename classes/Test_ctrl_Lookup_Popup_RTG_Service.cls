@isTest
private class Test_ctrl_Lookup_Popup_RTG_Service {
    
    @TestSetup
    private static void createSettings(){
    	
    	List<RTG_Service_Product_Codes__c> serviceProductSettings = new List<RTG_Service_Product_Codes__c>();
    	List<Product2> prods = new List<Product2>();
    	
    	for(Integer i = 0; i < 6; i++){
    		
    		
    		for(Integer j = 0; j < 4; j++){
    			
    			RTG_Service_Product_Codes__c serviceProduct = new RTG_Service_Product_Codes__c();
    			serviceProduct.Name = 'ProductCFN' + String.valueOf(i) + String.valueOf(j);
    			serviceProduct.Category__c = 'Category' + i;
    			serviceProduct.Order__c = j;
    			
    			serviceProductSettings.add(serviceProduct);
    			
    			Product2 prod = new Product2();
    			prod.Name = 'Test Product ' + String.valueOf(i) + String.valueOf(j);
    			prod.CFN_Code_Text__c = serviceProduct.Name;
    			prod.isActive = true;
    			prods.add(prod);	
    		}
    	}
    	
    	insert serviceProductSettings;
    	insert prods;
    }
    
    @isTest
    private static void testController(){
    	
    	Test.startTest();
    	
    	ctrl_Lookup lookupCon = new ctrl_Lookup();
		lookupCon.ObjectName = 'Product2';
		lookupCon.fieldNames = 'Name,CFN_Code_Text__c';
		lookupCon.filter = ' WHERE Id != null ';		
		lookupCon.compId = '123456';
		
		String params = lookupCon.getEncodedParams();
    	
    	Pagereference pr = Page.Picklist_Lookup_RTG_Service_Products;
		pr.getParameters().put('p', EncodingUtil.urlDecode(params, 'UTF-8'));
    	
    	Test.setCurrentPageReference(pr);
    	
    	ctrl_Lookup_Popup_RTG_Service ctrl = new ctrl_Lookup_Popup_RTG_Service();
    	System.assert(ctrl.categories.size() == 6);
    	System.assert(ctrl.componentId == '123456');
    }
}