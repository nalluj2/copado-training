/*
 *      Created Date : 20121001
 *      Description : This class tests the logic for the Account Territory Change triggers
 *
 *      Author = Rudy De Coninck
 */
@IsTest(seeAllData=true)
public with sharing class Test_tr_AccountTerritoryChanges {

	public static testmethod void testUpdateAccountTerritory(){
		Account_Territory_Change__c updateAssignment = new Account_Territory_Change__c();

		String buName ='Neurovascular';

		List<Territory2> territories = 
			[
				SELECT Id, Name, Business_Unit__c, Territory2Type.DeveloperName, Therapy_Groups_Text__c 
				FROM Territory2
				WHERE 
					Territory2Type.DeveloperName = 'Territory'
					AND Business_Unit__c = :buName
				LIMIT 2
			];
		
		/* Using existing data as creating territories and accounts in test class gives
			MIXED_DML_OPERATION, DML operation on setup object is not permitted after you have updated a non-setup object (or vice versa): Account, original object: Territory: []
			*/

		Territory2 oldTerr = territories.get(0);
		Territory2 newTerr = territories.get(1);
		
		Account account = new Account();
			account.Name='test Account';
			account.Account_Country_vs__c='GERMANY';
		insert account;

		Test.startTest();
	 
		//TODO create account separately
			updateAssignment.Account__c = account.Id;
			updateAssignment.Business_Unit_Name__c = buName;
			updateAssignment.Company_Name__c = 'Europe';
			updateAssignment.New_Territory_Name__c = newTerr.Name;
			updateAssignment.Operation__c = 'UPDATE';
		insert updateAssignment;
		
		Test.stopTest();
		
		System.debug('Updated Assignment ');
		
	}
	
	
	public static testmethod void testDeleteAccountTerritory(){
		
	}

}