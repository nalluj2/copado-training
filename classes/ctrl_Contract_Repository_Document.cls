public with sharing class ctrl_Contract_Repository_Document {
    
    @AuraEnabled
    public static ContractRepository getContractDocuments(Id contractId) {
        
        ContractRepository repository = new ContractRepository();       
        repository.documents = new List<ContractDocument>();
                                
        List<Contract_Repository__c> repositories = [SELECT Id, UserRecordAccess.HasReadAccess, UserRecordAccess.HasEditAccess, (Select Access_Level__c from Contract_Repository_Teams__r where Team_Member__c = :UserInfo.getUserId()) FROM Contract_Repository__c where Id = :contractId];        
        if(repositories.size() == 0 || repositories[0].UserRecordAccess.HasReadAccess == false){//No access
            
            repository.accessLevel = 'None';
            return repository;//finish here
                        
        }else if(repositories[0].UserRecordAccess.HasEditAccess == true){//Edit access
            
            repository.accessLevel = 'Edit';
            
        }else{//View and Edit access only
            
            if(repositories[0].Contract_Repository_Teams__r.size() == 0) repository.accessLevel = 'Read';//If the user doesn't have specified a Team record then we grant document access 
            else{
                
                String access = repositories[0].Contract_Repository_Teams__r[0].Access_Level__c;
                
                if(access == 'None' || access == 'Read Only - No Access to Documents'){
                    
                    repository.accessLevel = 'None';
                    if(access == 'None') return repository;
                    
                }else if(access == 'Read Only - Access to Documents') repository.accessLevel = 'Read';
                //CR-28989
                else if(access == 'Read Only - Upload Access to Documents'){
                    repository.accessLevel = 'Only Upload';
                }
            }           
        }       
        
        DocumentService service = new DocumentService();
        
        for(Contract_Repository_Document__c repositoryDoc : service.getDocuments(contractId)){
            
            ContractDocument doc = new ContractDocument();
            doc.id = repositoryDoc.Id;
            doc.name = repositoryDoc.Name;
            doc.documentType = repositoryDoc.Document_Type__c;
            doc.versions = new List<ContractDocumentVersion>();
            if(repositoryDoc.Contract_Repository_Document_Versions__r.size() > 0) doc.lastVersionDate = repositoryDoc.Contract_Repository_Document_Versions__r[0].CreatedDate;
            else doc.lastVersionDate = repositoryDoc.CreatedDate;
            
            for(Contract_Repository_Document_Version__c version : repositoryDoc.Contract_Repository_Document_Versions__r){
                
                ContractDocumentVersion docVersion = new ContractDocumentVersion();
                docVersion.id = version.Id;
                docVersion.name = version.File_Name__c;
                docVersion.fileType = version.File_Type__c;
                if(version.Size__c != null) docVersion.size = Integer.valueOf(version.Size__c);             
                docVersion.createdDate = version.CreatedDate.format() + ', ' + version.createdBy.Name;
                docVersion.version = version.Version__c;
                docVersion.documentumId = version.Documentum_ID__c;
                docVersion.interfaceError = version.Interface_Error__c;
                docVersion.markedDeletion = version.Marked_for_Deletion__c;
                
                doc.versions.add(docVersion);
            }
            
            repository.documents.add(doc);
        }
        
        repository.documents.sort();
        
        return repository;
    }
    
    @AuraEnabled
    public static void markVersionForDeletion(Id versionId) {
        
        DocumentService service = new DocumentService();
        service.markForDeletion(versionId);
    }
    
    @AuraEnabled
    public static void documentDeletion(Id documentId) {
        try {
			delete [Select Id from Contract_Repository_Document__c where Id = :documentId];
        } catch( Exception e) {
			throw new AuraHandledException(e.getmessage());
        }
    }
    
    @AuraEnabled
    public static List<DocumentType> getDocumentTypes() {
    
        List<DocumentType> options = new List<DocumentType>();
        
        for( Schema.PicklistEntry plValue : Contract_Repository_Document__c.Document_Type__c.getDescribe().getPicklistValues()){
            
            options.add(new DocumentType(plValue.getLabel(), plValue.getValue()));          
        }
        return options;
    }
        
    @AuraEnabled
    public static String getDocumentVersionURL(Id versionId) {
        
        Contract_Repository_Interface_Settings__c settings = Contract_Repository_Interface_Settings__c.getOrgDefaults();
                
        String url = settings.Documentum_Server_URL__c + '/mdtdctmapi/document?id=' + versionId + '&config_name=clm_sfdc&type=m_clm_doc';
        
        String securityCode = 'clmsfdc' + String.valueOf(Datetime.now().getTime());
        
        url += '&auth=' + EncodingUtil.urlEncode(EncodingUtil.base64Encode(Crypto.encrypt('AES128', Blob.valueOf(settings.Documentum_Key__c), Blob.valueOf('GbNLjNVN4yk8LjLx'), Blob.valueOf(securityCode))), 'UTF-8');      
        
        return url;
    }
    
    public class ContractRepository{
        
        @AuraEnabled public String accessLevel { get; set; }
        @AuraEnabled public List<ContractDocument> documents {get; set;}        
    }
    
    public class ContractDocument implements Comparable{
        
        @AuraEnabled public String id { get; set; }
        @AuraEnabled public String name { get; set; }
        @AuraEnabled public String documentType { get; set; }
        @AuraEnabled public DateTime lastVersionDate { get; set; }
        @AuraEnabled public List<ContractDocumentVersion> versions { get; set; }    
        
        public Integer compareTo(Object other) {
            ContractDocument compareTo = (ContractDocument) other;
            if (this.documentType < compareTo.documentType)
            return -1;  
            else if(this.documentType > compareTo.documentType)
            return 1;
            else{
                if(this.lastVersionDate > compareTo.lastVersionDate) return -1;
                else if(this.lastVersionDate < compareTo.lastVersionDate) return 1;
                return 0;
            }
        }       
    }           
    
    public class ContractDocumentVersion {
        
        @AuraEnabled public String id { get; set; }
        @AuraEnabled public String name { get; set; }       
        @AuraEnabled public String fileType { get; set; }
        @AuraEnabled public Integer size { get; set; }
        @AuraEnabled public Decimal version { get; set; }
        @AuraEnabled public String documentumId { get; set; }
        @AuraEnabled public String interfaceError { get; set; }
        @AuraEnabled public Boolean markedDeletion { get; set; }        
        @AuraEnabled public String createdDate { get; set; }    
    }
    
    public class DocumentType {
        
        @AuraEnabled public String value { get; set; }
        @AuraEnabled public String label { get; set; }
        
        public DocumentType(String l, String v){
            
            this.label = l;
            this.value = v;         
        }
    }
    
    public without sharing class DocumentService{
        
        public List<Contract_Repository_Document__c> getDocuments(String contractId){
            
            return [Select Id, Name, Document_Type__c, CreatedDate, (Select Id, File_Name__c, Version__c, Marked_for_Deletion__c, File_Type__c, Size__c, Documentum_ID__c, Interface_Error__c, CreatedBy.Name, CreatedDate from Contract_Repository_Document_Versions__r ORDER By Version__c DESC) from Contract_Repository_Document__c where Contract_Repository__c = :contractId ORDER BY Name];
        }
        
        public void markForDeletion(String versionId){
            
            Contract_Repository_Document_Version__c version = [Select Id, Documentum_Id__c, Interface_Error__c from Contract_Repository_Document_Version__c where Id = :versionId];
            
            version.marked_for_Deletion__c = true;
            
            if(version.Documentum_Id__c == null && version.Interface_Error__c != null){
                
                version.Is_Deleted__c = true;
            }   
            try {
				update version;
        	} catch( Exception e) {
				throw new AuraHandledException(e.getmessage());
        	}           
        }       
    }
}