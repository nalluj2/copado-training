/*

This test class is use to test the functionaility of tr_Opportunity_UpdatePhysicianAndHealthInsurer trigger 

Author        : Dheeraj Gupta
Created Date    : 25-05-2013

*/
@isTest(seeAllData=True)
private class test_tr_Opportunity_UpdatePhysician {

	static Account A;
	static Account A1;
	static Contact con;
	static Id recordTypeId;

	static {
		// Set Mock WS response
        Test.setMock(WebServiceMock.class, new Test_ws_SAPOpportunityNotificationMockup());
    	// Validate if Test User has a User Business Unit set to primary 
    	List<User_Business_Unit__c> listOfSBUser = [select Id, Primary__c from User_Business_Unit__c where User__c = :Userinfo.getUserId()];
    	if (listOfSBUser.isEmpty()){
	    	Id subUnitId = [select Id from Sub_Business_Units__c where name = 'Diabetes Core' and Business_Unit__r.Company__r.Company_Code_Text__c = 'EUR'].Id;
	    	User_Business_Unit__c sbUser = new User_Business_Unit__c();
	    	sbUser.User__c = Userinfo.getUserId();
	    	sbUser.Sub_Business_Unit__c = subUnitId;
	    	sbUser.Primary__c = true;
	    	insert sbUser;
    	} else {
    		// Validate if there is a primary already
    		Boolean setPrimary = true;
    		for (User_Business_Unit__c userBU : listOfSBUser){
    			if (userBU.Primary__c == true){
    				setPrimary = false;
    				continue;
    			}
    		}
    		if (setPrimary){
    			listOfSBUser[0].Primary__c = true;
    			update listOfSBUser;
    		}
    	}
    	
        A1 = new Account();
        A1.Name = 'My New Test Account';
        A1.Type = 'Health Insurance';
        insert A1;

		con = new Contact();
		con.LastName = 'Last Name';
		con.FirstName = 'Test';
		con.Contact_Department__c = 'Test';
		con.Contact_Primary_Specialty__c = 'Test';
		con.AccountId = A1.Id;
		con.Affiliation_To_Account__c = 'Employee';
		con.Primary_Job_Title_vs__c = 'Director';
		con.Contact_Gender__c = 'Male';
		insert con;

        RecordType rt = [select id from recordType where SobjectType='Account' and IsPersonType=true 
        					and IsActive=true limit 1];
		                
        A = new Account();
        A.LastName = 'My Test Account';
        A.PersonBirthdate = system.today();
        A.RecordTypeId = rt.Id;
        insert A;
        
        recordTypeId = [select Id from RecordType where DeveloperName = 'CAN_DIB_OPPORTUNITY' and sObjectType = 'Opportunity' and isActive = true].Id;

		
	}

    static testMethod void testInsertUpdateOpportunity() {
    	
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = recordTypeId;
        opp.AccountId = A.Id; 
        opp.Name = 'Test Opp';
        opp.CloseDate = System.today();
        opp.StageName = 'Won';
        opp.Primary_Health_Insurer__c = A1.Id;
        opp.Contact_ID__c = con.Id;
        //opp.Account_ID__c = A1.id;
        opp.Deal_Category__c = 'My Deal';
                
        Test.startTest();
        
        insert opp;
        
        opp.Name = 'Test Opp Update';
        update opp;     
        
        Test.stopTest();
        
    }

    static testMethod void testInsertOpportunity() {
    	
        Opportunity opp2 = new Opportunity();
        opp2.RecordTypeId = recordTypeId;
        opp2.AccountId = A.Id; 
        opp2.Name = 'Test Opp';
        opp2.CloseDate = System.today();
        opp2.StageName = 'Won';
        opp2.Primary_Health_Insurer__c = A1.Id;
        opp2.Contact_ID__c = con.Id;
        //opp2.Account_ID__c = A1.id;
        opp2.Deal_Category__c = 'My Deal';
                
        Test.startTest();
        
        insert opp2;
        
        Test.stopTest();
        
    }

}