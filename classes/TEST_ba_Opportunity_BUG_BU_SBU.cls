@isTest 
private class TEST_ba_Opportunity_BUG_BU_SBU {

	@isTest static void test_setOpportunity_BUG_BU_SBU_INSERT(){

		//------------------------------------------------
		// Create Test Data
		//------------------------------------------------
		// Create Master Data
		clsTestData_MasterData.tCompanyCode = 'EUR';
		clsTestData_MasterData.createCompany();
		clsTestData_MasterData.tBusinessUnitGroupName = 'Restorative';
		clsTestData_MasterData.createBusinessUnit();
		clsTestData_MasterData.createSubBusinessUnit();

		// Create DIB_Fiscal_Period__c data
//		clsTestData_System.createDIBFiscalPeriod();
		// Create Account Data
		clsTestData_Account.createAccount();
		// Create Opportunity Data
		clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'Capital_Opportunity').Id;
		List<Opportunity> lstOpportunity = clsTestData_Opportunity.createOpportunity();
		// Create Product Data
		clsTestData_Product.createProductGroup();
		clsTestData_Product.iRecord_Product = 2;
		clsTestData_Product.createProduct(false);
			clsTestData_Product.lstProduct[0].Product_Group__c = clsTestData_Product.oMain_ProductGroup.Id;
			clsTestData_Product.lstProduct[1].Business_Unit_ID__c = null;
			clsTestData_Product.lstProduct[1].Business_Unit_Group__c = clsTestData_MasterData.oMain_BusinessUnitGroup.Id;
		insert clsTestData_Product.lstProduct;

		List<Product2> lstProduct = [SELECT Id, Business_Unit_Group__c, Business_Unit_ID__c, Product_Group__r.Therapy_ID__r.Name, Sub_Business_Unit__c FROM Product2];
		System.debug('**BC** lstProduct (' + lstProduct.size() + ') : ' + lstProduct);

		// Create PricebookEntry
		clsTestData_Opportunity.createPricebookEntry(true);
			
		// Create Opportunity Line Items
		clsTestData_Opportunity.iRecord_OpportunityLineItem = 2;
		clsTestData_Opportunity.createOpportunityLineItem(true);

		for (Opportunity oOpportunity : lstOpportunity){

			oOpportunity.Business_Unit_Group__c = null;
			oOpportunity.Business_Unit_msp__c = null;
			oOpportunity.Sub_Business_Unit__c = null;
			oOpportunity.Therapies__c = null;

		}
		update lstOpportunity;

		lstOpportunity = [SELECT Id, Business_Unit_Group__c, Business_Unit_msp__c, Sub_Business_Unit__c, Therapies__c FROM Opportunity];
		for (Opportunity oOpportunity : lstOpportunity){

			System.assert(String.isBlank(oOpportunity.Business_Unit_Group__c));
			System.assert(String.isBlank(oOpportunity.Business_Unit_msp__c));
			System.assert(String.isBlank(oOpportunity.Sub_Business_Unit__c));
			System.assert(String.isBlank(oOpportunity.Therapies__c));

		}
		//------------------------------------------------
		
		//------------------------------------------------
		// Perform Test
		//------------------------------------------------
		Test.startTest();

		ba_Opportunity_BUG_BU_SBU oBatch = new ba_Opportunity_BUG_BU_SBU();
			oBatch.tSOQL = 'SELECT Id, Name, Business_Unit_Group__c, Business_Unit_msp__c, Sub_Business_Unit__c, Therapies__c FROM Opportunity LIMIT 200';
		Database.executebatch(oBatch, 200);
		 
		Test.stopTest();
		//------------------------------------------------


		//------------------------------------------------
		// Validate Test Result
		//------------------------------------------------
		lstOpportunity = [SELECT Id, Business_Unit_Group__c, Business_Unit_msp__c, Sub_Business_Unit__c, Therapies__c FROM Opportunity];

		System.assertEquals(lstOpportunity.size(), 1);
		for (Opportunity oOpportunity : lstOpportunity){

			System.assert(!String.isBlank(oOpportunity.Business_Unit_Group__c));
			System.assert(!String.isBlank(oOpportunity.Business_Unit_msp__c));
			System.assert(!String.isBlank(oOpportunity.Sub_Business_Unit__c));
			System.assert(!String.isBlank(oOpportunity.Therapies__c));

		}
		//------------------------------------------------

	}

}