public with sharing class ctrlExt_BUG_Account_Plan_sBU_Plans {
	
	public List<Account_Plan_2__c> sbuAccountPlans {get; set;}
	
	public ctrlExt_BUG_Account_Plan_sBU_Plans (ApexPages.StandardController sc){
		
		if(!Test.isRunningTest()){
			
			sc.addFields(new List<String>{'Account__c', 'Business_Unit_Group__c'});			
		}
		
		Account_Plan_2__c bugAccountPlan = (Account_Plan_2__c) sc.getRecord();	
		
		sbuAccountPlans = [Select Id, Name, Business_Unit__c, Business_Unit__r.Name, Sub_Business_Unit__c, Sub_Business_Unit__r.Name, Start_Date__c from Account_Plan_2__c 
							where Account__c = :bugAccountPlan.Account__c AND Account_Plan_Level__c = 'Sub Business Unit' 
							AND Business_Unit_Group__c = :bugAccountPlan.Business_Unit_Group__c];
	}	
	
}