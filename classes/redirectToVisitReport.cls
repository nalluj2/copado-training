/**
 *  Creation Date : 28/05/2009
 *  Description : Redirect Buttons to pages for Visit Report Like Edit button and New Button ! 
 *  Author : ABSI / Miguel Coimbra
 *
 */

public with sharing class redirectToVisitReport {
    public Contact_Visit_Report__c contactVR ; 
    
    public redirectToVisitReport(ApexPages.StandardController controller ) {
        this.contactVR = (Contact_Visit_Report__c) controller.getRecord();
    }
    
    public PageReference GoToEditPage(){
        String cVRID = this.contactVR.Id ;
        String vrId = [Select id, Call_Records__c from Contact_Visit_Report__c where id =: cVRID limit 1].Call_Records__c ; 
        try{
            Savepoint sp = Database.setSavepoint();
            Call_Records__c cr= [Select Id from Call_Records__c where Id =: vrId ];
            update cr;
            Database.rollback(sp);
        } catch(DmlException ex){
          ApexPages.addMessages(ex);
          return null;
        }  
        PageReference pageRef = new PageReference('/apex/visitReportPage?retURL=%2F'+vrId + '&id='+ vrId +'&sfdc.override=1');
        pageRef.setRedirect(true);
        return pageRef;
    }
    
}