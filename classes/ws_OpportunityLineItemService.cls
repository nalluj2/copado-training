/*
 *      Description : This class exposes methods to create / update / delete a Opportunity Line Item
 *
 *      Author = Rudy De Coninck
 */
@RestResource(urlMapping='/OpportunityLineItemService/*')
global class ws_OpportunityLineItemService{
	
	@HttpPost
	global static IFOpportunityLineItemResponse doPost() {
 		RestRequest req = RestContext.request;
			
		System.debug('post called '+req);
		System.debug('post requestBody '+req.requestBody);
			
		String body = req.requestBody.toString();
		IFOpportunityLineItemRequest opportunityLineItemRequest = (IFOpportunityLineItemRequest)System.Json.deserialize(body, IFOpportunityLineItemRequest.class);
			
		System.debug('post requestBody '+opportunityLineItemRequest);
		IFOpportunityLineItemResponse resp = new IFOpportunityLineItemResponse();	
		try{
				
			OpportunityLineItem oppLineItem = opportunityLineItemRequest.opportunityLineItem;
			
			resp.opportunityLineItem = bl_AccountPlanning.saveOpportunityLineItems(new List<OpportunityLineItem>{oppLineItem})[0]; 
			resp.id = opportunityLineItemRequest.id;
	
			resp.success = true;
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
		}
 
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'OpportunityLineItem' ,req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
  			
		return resp;		
	}

	@HttpDelete
	global static IFOpportunityLineItemDeleteResponse doDelete(){
		RestRequest req = RestContext.request;
			
		System.debug('post called '+req);
		System.debug('post requestBody '+req.requestBody);
			
		String body = req.requestBody.toString();
		IFOpportunityLineItemDeleteResponse resp = new IFOpportunityLineItemDeleteResponse();
		
		IFOpportunityLineItemDeleteRequest opportunityLineItemDeleteRequest = (IFOpportunityLineItemDeleteRequest)System.Json.deserialize(body, IFOpportunityLineItemDeleteRequest.class);
			
		System.debug('post requestBody '+opportunityLineItemDeleteRequest);
			
		OpportunityLineItem oppLineItem =opportunityLineItemDeleteRequest.opportunityLineItem;
			
		List<OpportunityLineItem> oppLineItemsToDelete = [select id, mobile_id__c from OpportunityLineItem where Mobile_ID__c =:oppLineItem.mobile_id__c];
		
		try{
			
			if (oppLineItemsToDelete !=null && oppLineItemsToDelete.size()>0 ){	
				
				delete oppLineItemsToDelete;
				resp.opportunityLineItem = oppLineItemsToDelete[0];
				resp.id = opportunityLineItemDeleteRequest.id;
				resp.success = true;
			}else{
				resp.opportunityLineItem = oppLineItem;
				resp.id = opportunityLineItemDeleteRequest.id;
				resp.message='Opportunity Line Items not found';
				resp.success = true;
			}
			
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
		}
  			
			
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'OpportunityLineItemDelete',req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
		
		return resp;
	}
	
	global class IFOpportunityLineItemRequest{
		public String id{get;set;}		
		public OpportunityLineItem opportunityLineItem {get;set;}
	}
	
	global class IFOpportunityLineItemDeleteRequest{
		public String id{get;set;}
		public OpportunityLineItem opportunityLineItem {get;set;}
	}
		
	global class IFOpportunityLineItemDeleteResponse{
		public String id{get;set;}
		public OpportunityLineItem opportunityLineItem {get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}

	global class IFOpportunityLineItemResponse{
		public String id {get;set;}
		public OpportunityLineItem opportunityLineItem {get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}
	

}