@isTest
private class TEST_triggerCallActivityBeforeUpsert {
    static TestMethod void triggerCallActivityBeforeUpsert(){
    	
	       //Insert Company
	       Company__c cmpny = new Company__c();
	       cmpny.name='TestMedtronic';
	       cmpny.CurrencyIsoCode = 'EUR';
	       cmpny.Current_day_in_Q1__c=56;
	       cmpny.Current_day_in_Q2__c=34; 
	       cmpny.Current_day_in_Q3__c=5; 
	       cmpny.Current_day_in_Q4__c= 0;   
	       cmpny.Current_day_in_year__c =200;
	       cmpny.Days_in_Q1__c=56;  
	       cmpny.Days_in_Q2__c=34;
	       cmpny.Days_in_Q3__c=13;
	       cmpny.Days_in_Q4__c=22;
	       cmpny.Days_in_year__c =250;
	       cmpny.Company_Code_Text__c = 'T41';
	       insert cmpny;        
    	
    	   
         Call_Activity_Type__c CAT = new Call_Activity_Type__c();
         CAT.Name='Test Activity';
         CAT.Active__c=true;
         CAT.Company_ID__c = cmpny.Id;
         insert CAT;   
         
         delete CAT;
     }
}