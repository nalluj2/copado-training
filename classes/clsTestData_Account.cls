@isTest public class clsTestData_Account {
    
    public static Integer iRecord_Account = 1;
    public static String tCountry_Account = 'BELGIUM';
    public static Id idRecordType_Account = clsUtil.getRecordTypeByDevName('Account', 'NON_SAP_Account').Id;
    public static Account oMain_Account;
    public static List<Account> lstAccount = new List<Account>();

    public static Integer iRecord_Account_SAPAccount = 1;
    public static String tCountry_Account_SAPAccount = 'BELGIUM';
    public static Id idRecordType_Account_SAPAccount = clsUtil.getRecordTypeByDevName('Account', 'SAP_Account').Id;
    public static Account oMain_Account_SAPAccount;
    public static List<Account> lstAccount_SAPAccount = new List<Account>();

    public static Integer iRecord_NonSAPPersonAccount = 1;
    public static String tCountry_NonSAPPersonAccount = 'CANADA';
    public static Id idRecordType_NonSAPPersonAccount = clsUtil.getRecordTypeByDevName('Account', 'Non_SAP_Patient').Id;
    public static Account oMain_NonSAPPersonAccount;
    public static List<Account> lstNonSAPPersonAccount = new List<Account>();

    public static Integer iRecord_SAPPersonAccount = 1;
    public static String tCountry_SAPPersonAccount = 'CANADA';
    public static Id idRecordType_SAPPersonAccount = clsUtil.getRecordTypeByDevName('Account', 'SAP_Patient').Id;
    public static Account oMain_SAPPersonAccount;
    public static List<Account> lstSAPPersonAccount = new List<Account>();

    public static Integer iRecord_HealthInsurerAccount = 1;
    public static String tCountry_HealthInsurerAccount = 'CANADA';
    public static Id idRecordType_HealthInsurerAccount = clsUtil.getRecordTypeByDevName('Account', 'SAP_Account').Id;
    public static Account oMain_HealthInsurerAccount;
    public static List<Account> lstHealthInsurerAccount = new List<Account>();

    public static Integer iRecord_Competitor = 1;
    public static String tCountry_Competitor = 'BELGIUM';
    public static Id idRecordType_Competitor = clsUtil.getRecordTypeByDevName('Account', 'Competitor').Id;
    public static Account oMain_Competitor;
    public static List<Account> lstCompetitor = new List<Account>();

    public static Integer iRecord_MDTAccount = 1;
    public static String tCountry_MDTAccount = 'BELGIUM';
    public static Id idRecordType_MDTAccount = clsUtil.getRecordTypeByDevName('Account', 'MDT_Account').Id;
    public static Account oMain_MDTAccount;
    public static List<Account> lstMDTAccount = new List<Account>();


    //---------------------------------------------------------------------------------------------------
    // Create Account Data
    //---------------------------------------------------------------------------------------------------
    public static List<Account> createAccount() {
        return createAccount(true);
    }
    public static List<Account> createAccount(Boolean bInsert) {

        if (oMain_Account == null){

            lstAccount = new List<Account>();
            for (Integer i=0; i<iRecord_Account; i++){
                Account oAccount = new Account();
                    oAccount.Name = 'TEST Account ' + i; 
                    oAccount.Account_Country_vs__c = tCountry_Account;
                    oAccount.RecordTypeId = idRecordType_Account;
                    // csa addition for dib test
                    oAccount.Account_Active__c = true;
                    oAccount.isSales_Force_Account__c = true;
                lstAccount.add(oAccount);
            }
            if (bInsert){
                insert lstAccount;
            }
            oMain_Account = lstAccount[0];  

        }

        return lstAccount;
    }   
    //---------------------------------------------------------------------------------------------------


    //---------------------------------------------------------------------------------------------------
    // Create Account Data
    //---------------------------------------------------------------------------------------------------
    public static List<Account> createAccount_SAPAccount() {
        return createAccount_SAPAccount(true);
    }
    public static List<Account> createAccount_SAPAccount(Boolean bInsert) {

        if (oMain_Account_SAPAccount == null){

            lstAccount_SAPAccount = new List<Account>();
            for (Integer i=0; i<iRecord_Account_SAPAccount; i++){
                Account oAccount = new Account();
                    oAccount.Name = 'TEST Account SAPAccount ' + i; 
                    oAccount.Account_Country_vs__c = tCountry_Account_SAPAccount;
                    oAccount.RecordTypeId = idRecordType_Account_SAPAccount;
                    oAccount.Account_Active__c = true;
                    oAccount.isSales_Force_Account__c = true;
                    oAccount.SAP_ID__c = 'SAP' + i;
                lstAccount_SAPAccount.add(oAccount);
            }
            if (bInsert){
                insert lstAccount_SAPAccount;
            }
            oMain_Account_SAPAccount = lstAccount_SAPAccount[0];  

        }

        return lstAccount_SAPAccount;
    }   
    //---------------------------------------------------------------------------------------------------


    //---------------------------------------------------------------------------------------------------
    // Create Competitor Data
    //---------------------------------------------------------------------------------------------------
    public static List<Account> createCompetitor() {
        return createCompetitor(true);
    }
    public static List<Account> createCompetitor(Boolean bInsert) {

        if (oMain_Competitor == null){

            lstCompetitor = new List<Account>();
            for (Integer i=0; i<iRecord_Competitor; i++){
                Account oCompetitor = new Account();
                    oCompetitor.Name = 'TEST Account ' + i; 
                    oCompetitor.Account_Country_vs__c = tCountry_Competitor;
                    oCompetitor.RecordTypeId = idRecordType_Competitor;
                    // csa addition for dib test
                    oCompetitor.Account_Active__c = true;
                    oCompetitor.isSales_Force_Account__c = true;
                lstCompetitor.add(oCompetitor);
            }
            if (bInsert){
                insert lstCompetitor;
            }
            oMain_Competitor = lstCompetitor[0];  

        }

        return lstCompetitor;
    }   
    //---------------------------------------------------------------------------------------------------


    //---------------------------------------------------------------------------------------------------
    // Create SAP Person Account Data
    //---------------------------------------------------------------------------------------------------
    public static List<Account> createSAPPersonAccount(){
        return createSAPPersonAccount(true);
    }
    public static List<Account> createSAPPersonAccount(Boolean bInsert){

        if (oMain_SAPPersonAccount == null){

            lstSAPPersonAccount = new List<Account>();
            for (Integer i=0; i<iRecord_SAPPersonAccount; i++){
                Account oAccount = new Account();
                    oAccount.RecordTypeId = idRecordType_SAPPersonAccount;
                    oAccount.Contact_Gender__pc = 'Male';
                    oAccount.LastName = 'Test SAP Person Account ' + i;
                    oAccount.SAP_Account_Name__c = 'Test SAP Person Account ' + i;
                    oAccount.SAP_ID__c = 'SAPA' + i;
                    oAccount.SAP_Channel__c = '30';
                    oAccount.PersonBirthdate = System.today() - (20 * 365);
                    oAccount.Account_Country_vs__c = tCountry_SAPPersonAccount;
                    oAccount.DIB_Diabetes_Type__pc = 'Type 1';
					oAccount.DIB_SAP_SR_Name__c = UserInfo.getLastName() + ', ' + UserInfo.getFirstName();
                lstSAPPersonAccount.add(oAccount);
            }

            if (bInsert){
                insert lstSAPPersonAccount;
            }

            oMain_SAPPersonAccount = lstSAPPersonAccount[0];
        } 

        return lstSAPPersonAccount;
    }
    //---------------------------------------------------------------------------------------------------


    //---------------------------------------------------------------------------------------------------
    // Create SAP Person Account Data
    //---------------------------------------------------------------------------------------------------
    public static List<Account> createHealthInsurerAccount(){
        return createHealthInsurerAccount(true);
    }
    public static List<Account> createHealthInsurerAccount(Boolean bInsert){

        if (oMain_HealthInsurerAccount == null){

            lstHealthInsurerAccount = new List<Account>();
            for (Integer i=0; i<iRecord_HealthInsurerAccount; i++){
                Account oAccount = new Account();
                    oAccount.RecordTypeId = idRecordType_HealthInsurerAccount;
                    oAccount.Account_Country_vs__c = tCountry_HealthInsurerAccount;
                    oAccount.Type = 'Health Insurance';
                    oAccount.Name = 'Health Insurer ' + i;
                    oAccount.SAP_ID__c = 'SAPHI' + i;
                    //oAccount.SAP_Channel__c = '30';
                lstHealthInsurerAccount.add(oAccount);
            }

            if (bInsert){
                insert lstHealthInsurerAccount;
            }

            oMain_HealthInsurerAccount = lstHealthInsurerAccount[0];
        } 

        return lstHealthInsurerAccount;
    }
    //---------------------------------------------------------------------------------------------------


    //---------------------------------------------------------------------------------------------------
    // Create Non SAP Person Account Data
    //---------------------------------------------------------------------------------------------------
    public static List<Account> createNonSAPPersonAccount(){
        return createNonSAPPersonAccount(true);
    }
    public static List<Account> createNonSAPPersonAccount(Boolean bInsert){

        if (oMain_NonSAPPersonAccount == null){

            lstNonSAPPersonAccount = new List<Account>();
            for (Integer i=0; i<iRecord_NonSAPPersonAccount; i++){
                Account oAccount = new Account();
                    oAccount.RecordTypeId = idRecordType_NonSAPPersonAccount;
                    oAccount.Contact_Gender__pc = 'Male';
                    oAccount.LastName = 'Test Non SAP Person Account ' + i;
                    //oAccount.SAP_ID__c = 'NONSAP' + i;
                    //oAccount.SAP_Channel__c = '30';
                    oAccount.PersonBirthdate = System.today() - (20 * 365);
                    oAccount.Account_Country_vs__c = tCountry_NonSAPPersonAccount;
                    oAccount.DIB_Diabetes_Type__pc = 'Type 1';
                lstNonSAPPersonAccount.add(oAccount);
            }

            if (bInsert){
                insert lstNonSAPPersonAccount;
            }

            oMain_NonSAPPersonAccount = lstNonSAPPersonAccount[0];
        } 

        return lstNonSAPPersonAccount;
    }
    //---------------------------------------------------------------------------------------------------    


    //---------------------------------------------------------------------------------------------------
    // Create MDT Account
    //---------------------------------------------------------------------------------------------------
    public static List<Account> createMDTAccount(){
        return createMDTAccount(true);
    }
    public static List<Account> createMDTAccount(Boolean bInsert){

        if (oMain_MDTAccount == null){

            lstMDTAccount = new List<Account>();
            for (Integer i=0; i<iRecord_MDTAccount; i++){
                Account oAccount = new Account();
                    oAccount.RecordTypeId = idRecordType_MDTAccount;
                    oAccount.Name = 'Test MDT Account ' + i;
                    oAccount.Account_Country_vs__c = tCountry_Account;
                lstMDTAccount.add(oAccount);
            }

            if (bInsert){
                insert lstMDTAccount;
            }

            oMain_MDTAccount = lstMDTAccount[0];
        } 

        return lstMDTAccount;
    }
    //---------------------------------------------------------------------------------------------------    


    //---------------------------------------------------------------------------------------------------    

}