/**
 * Creation Date :  20110331
 * Description :    Test Coverage of the trigger 'ImplantBeforeInsertUpdate' (for the Implant__c Object).
 *                  (To facilitate the development of robust, error-free code, Apex Code requires the creation and execution of unit tests.
 *                  Unit tests are comprised of test methods and classes that verify whether a particular piece of code is working properly.)
 * Author :         MDT - JaKe
 */
@isTest
private class Test_TriggerImplantBeforeInsertUpdate {
    
    static TestMethod void triggerImplantBeforeInsertUpdate(){
         //Insert Company
         Company__c cmpny = new Company__c();
         cmpny.name='TestMedtronic';
         cmpny.CurrencyIsoCode = 'EUR';
         cmpny.Current_day_in_Q1__c=56;
         cmpny.Current_day_in_Q2__c=34; 
         cmpny.Current_day_in_Q3__c=5; 
         cmpny.Current_day_in_Q4__c= 0;   
         cmpny.Current_day_in_year__c =200;
         cmpny.Days_in_Q1__c=56;  
         cmpny.Days_in_Q2__c=34;
         cmpny.Days_in_Q3__c=13;
         cmpny.Days_in_Q4__c=22;
         cmpny.Days_in_year__c =250;
         cmpny.Company_Code_Text__c = 'T36';
         insert cmpny;
         
        Business_Unit__c bu1 = new Business_Unit__c();
        bu1.Business_Unit__c = 'TestBusiness1';
        bu1.Company__c = cmpny.Id;
        insert bu1;

          //Insert SBU
          Sub_Business_Units__c sbu1 = new Sub_Business_Units__c();
          sbu1.name='SBUMedtronic1';
          sbu1.Business_Unit__c=bu1.id;
          insert sbu1;
        
        Account acc1 = new Account();
        acc1.Name = 'TestAccount1';
        insert acc1;
        
        Contact con1 =  new Contact();
        con1.LastName = 'TestContact1';
        con1.FirstName = 'TEST';
        con1.AccountId = acc1.Id;
        con1.Contact_Department__c = 'Diabetes Adult'; 
        con1.Contact_Primary_Specialty__c = 'ENT';
        con1.Affiliation_To_Account__c = 'Employee';
        con1.Primary_Job_Title_vs__c = 'Manager';
        con1.Contact_Gender__c = 'Male';                
        insert con1;

        Therapy_Group__c TG=new Therapy_Group__c();
        TG.Name='Test Therapy Group';
        TG.Sub_Business_Unit__c =sbu1.Id;
        TG.Company__c = cmpny.Id;
        insert TG;    
        
        
        Therapy__c[] ther = new Therapy__c[]{};
        Therapy__c ther1 = new Therapy__c();
        ther1.Business_Unit__c = bu1.id;
        ther1.Therapy__c = 'TestTherapy1';
        ther1.Sub_Business_Unit__c = sbu1.Id;
        ther1.Therapy_Group__c = TG.Id;
        ther1.Therapy_Name_Hidden__c = 'TestTherapy1'; 
        ther.add(ther1);
        /*Therapy__c ther2 = new Therapy__c();
        ther2.Business_Unit__c = bu1.id;
        ther2.Therapy__c = 'TestTherapy2';
        ther2.Sub_Business_Unit__c = sbu1.Id;
        ther2.Therapy_Group__c = TG.Id;
        ther2.Therapy_Name_Hidden__c = 'TestTherapy2'; 
        ther.add(ther2);*/
        insert ther;
        
        Implant__c impl1 = new Implant__c();
        impl1.Therapy__c = ther[0].id;
        impl1.Implant_Implanting_Contact__c = acc1.Id;
        impl1.Implant_Implanting_Contact__c = con1.Id;
        impl1.Implant_Date_Of_Surgery__c = date.today();
        insert impl1;
        
        impl1.Therapy__c = ther[0].id;
        update impl1;
        
        Implant__c[] impl200 = new Implant__c[]{};
        for (Integer i=0; i<100; i++){
            Implant__c impl0 = new Implant__c();
            impl0.Therapy__c = ther[0].id;
            impl0.Implant_Implanting_Contact__c = acc1.Id;
            impl0.Implant_Implanting_Contact__c = con1.Id;
            impl0.Implant_Date_Of_Surgery__c = date.today();
            impl200.add(impl0);         
        
        }
        insert impl200;
        
        
        
    }   

}