public class bl_MRIScanner_Trigger {

	public static void populateMobileId(List<MRI_Scanner__c> lstTriggerNew){

		if (bl_Trigger_Deactivation.isTriggerDeactivated('MRIScanner_populateMobileId')) return;

		GuidUtil.populateMobileId(lstTriggerNew, 'External_Id__c');

	}


	public static void SynchronizationService_createNotificationIfChanged(List<MRI_Scanner__c> lstTriggerNew, Map<Id, MRI_Scanner__c> mapTriggerOld){

		if (bl_Trigger_Deactivation.isTriggerDeactivated('MRIScanner_createNotificationIfChanged')) return;

		if (bl_SynchronizationService_Utils.isSyncProcess == false){
			bl_SynchronizationService_Source.createNotificationIfChanged(lstTriggerNew, mapTriggerOld);		
		}

	}

}