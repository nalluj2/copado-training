@isTest
private class Test_es_Procedural_Solutions_Kits {


	private static testmethod void testApproveBox(){
		
  		Procedural_Solutions_Kit__c box = new Procedural_Solutions_Kit__c();		
		box.Status__c = 'Open';		
		insert box;
		
		// Submit Box
		box.Status__c = 'Approval Pending';		
		update box;
		
		box = [Select Name from Procedural_Solutions_Kit__c where id = :box.Id];
					
		Messaging.InboundEmail email = new Messaging.InboundEmail() ;   	
   		email.Subject = 'Approve/' + box.Name;
   		email.fromAddress = 'test@medtronic.com';
   		
   		es_Procedural_Solutions_Kits  testInbound = new es_Procedural_Solutions_Kits ();
  		testInbound.handleInboundEmail(email, new Messaging.InboundEnvelope());
		
		box = [Select Status__c from Procedural_Solutions_Kit__c where Id = :box.Id];		
		System.assert(box.Status__c == 'Approved');
	}
	
	private static testmethod void testRejection(){
		
  		Procedural_Solutions_Kit__c box = new Procedural_Solutions_Kit__c();		
		box.Status__c = 'Open';		
		insert box;
		
		// Submit Box
		box.Status__c = 'Approval Pending';		
		update box;
		
		box = [Select Name from Procedural_Solutions_Kit__c where id = :box.Id];
					
		Messaging.InboundEmail email = new Messaging.InboundEmail() ;   	
   		email.Subject = 'Reject/' + box.Name;
   		email.PlainTextBody = '<========== Test rejection reason ==========>';
   		email.fromAddress = 'test@medtronic.com';
   		
   		es_Procedural_Solutions_Kits  testInbound = new es_Procedural_Solutions_Kits ();
  		testInbound.handleInboundEmail(email, new Messaging.InboundEnvelope());
		
		box = [Select Status__c from Procedural_Solutions_Kit__c where Id = :box.Id];		
		System.assert(box.Status__c == 'Open');
	} 
	
	private static testmethod void testAvailable(){
		
  		Procedural_Solutions_Kit__c box = new Procedural_Solutions_Kit__c();		
		box.Status__c = 'Approved';		
		insert box;
		
		box = [Select Name from Procedural_Solutions_Kit__c where id = :box.Id];
					
		Messaging.InboundEmail email = new Messaging.InboundEmail() ;   	
   		email.Subject = 'A New BOX Deliverable has been Released';
   		email.PlainTextBody = 'New Deliverable Item Code :  ' + box.Name;
   		email.fromAddress = 'test@medtronic.com';
   		
   		es_Procedural_Solutions_Kits  testInbound = new es_Procedural_Solutions_Kits ();
  		testInbound.handleInboundEmail(email, new Messaging.InboundEnvelope());
		
		box = [Select Status__c from Procedural_Solutions_Kit__c where Id = :box.Id];		
		System.assert(box.Status__c == 'Available');
	} 
	
	private static testmethod void testApproveFail(){
				
		Procedural_Solutions_Kit__c box = new Procedural_Solutions_Kit__c();		
		box.Status__c = 'Open';		
		insert box;
		
		// Submit Box
		box.Status__c = 'Approval Pending';		
		update box;
				
		Messaging.InboundEmail email = new Messaging.InboundEmail() ;   	
   		email.Subject = 'Approve/FakeName';
   		email.fromAddress = 'test@medtronic.com';
   		
   		es_Procedural_Solutions_Kits  testInbound = new es_Procedural_Solutions_Kits ();
  		Messaging.InboundEmailResult result = testInbound.handleInboundEmail(email, new Messaging.InboundEnvelope());
		System.assert(result.success == false);
		
		box = [Select Status__c from Procedural_Solutions_Kit__c where Id = :box.Id];		
		System.assert(box.Status__c == 'Approval Pending');		
	} 
	
	private static testmethod void testAvailableFail(){
		
  		Procedural_Solutions_Kit__c box = new Procedural_Solutions_Kit__c();		
		box.Status__c = 'Approved';		
		insert box;
		
		box = [Select Name from Procedural_Solutions_Kit__c where id = :box.Id];
					
		Messaging.InboundEmail email = new Messaging.InboundEmail() ;   	
   		email.Subject = 'A New BOX Deliverable has been Released';
   		email.PlainTextBody = box.Name;
   		email.fromAddress = 'test@medtronic.com';
   		
   		es_Procedural_Solutions_Kits  testInbound = new es_Procedural_Solutions_Kits ();
  		Messaging.InboundEmailResult result = testInbound.handleInboundEmail(email, new Messaging.InboundEnvelope());
  		
  		System.assert(result.success == true);
		
		box = [Select Status__c from Procedural_Solutions_Kit__c where Id = :box.Id];		
		System.assert(box.Status__c == 'Approved');
	}   
	
	@TestSetup
    private static void setupSettings(){
    	
    	SystemAdministratorProfileId__c ids = SystemAdministratorProfileId__c.getInstance('Default');
    	
    	if(ids == null){
    		ids = new SystemAdministratorProfileId__c();
    		ids.Name = 'Default';
    	}
    	
    	if(ids.SystemAdministrator__c == null){
    		
    		ids.SystemAdministrator__c = String.valueOf([Select Id from Profile where Name = 'System Administrator'].Id).substring(0,15);
    		upsert ids;
    	}
    	
    	Procedural_Solutions_Kit_Email__c email = new Procedural_Solutions_Kit_Email__c();
    	email.Name = 'RS EMEA';
    	email.Email__c = 'test@medtronic.com';
    	insert email;
    }
}