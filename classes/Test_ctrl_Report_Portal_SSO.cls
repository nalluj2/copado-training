@IsTest
private class Test_ctrl_Report_Portal_SSO {
    
    @testSetup
    private static void createCustomSettings(){
        
        Support_Portal_URL__c settings = Support_Portal_URL__c.getOrgDefaults();
        settings.Base_URL__c = 'https://salesforce.com/';
        settings.Instance_URL__c = 'https://salesforce.com/';
        upsert settings;
    }

    @IsTest
    private static void testRedirection(){

        User oUser_Admin = clsTestData_User.createUser_SystemAdministratorMDT('tstadm1', false);
        System.runAs(oUser_Admin) {

            PageReference pr = Page.Report_Portal_SSO;
            pr.getParameters().put('BUG', 'MITG');

            Test.setCurrentPage(pr);

            Test.startTest();

            ctrl_Report_Portal_SSO con = new ctrl_Report_Portal_SSO();

            PageReference redirection = con.redirectToPortal();

            System.assert(redirection.getUrl().startsWith('https://salesforce.com/apex/report_portal_MITG?'));
            System.assert(redirection.getParameters().get('user') != null);
            System.assert(redirection.getParameters().get('token') != null);

        }
    }
}