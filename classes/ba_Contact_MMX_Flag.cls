global without sharing class ba_Contact_MMX_Flag implements Schedulable, Database.Batchable<sObject>, Database.Stateful{
	
	private Integer success = 0;		
	private List<String> errors = new List<String>();
		
	/**
	 *	Schedulable	 
	 */
	global void execute(SchedulableContext ctx){
				
		Database.executeBatch(new ba_Contact_MMX_Flag(), 1);
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC){
		
		Set<String> mmxCountryList = Integrate_With_MMX_Countries__c.getAll().keySet();
		
		String query = 'SELECT Id FROM Affiliation__c WHERE ';
		query += 'Affiliation_Type__c IN (\'Owner\',\'Employee\',\'Contractor\',\'Associate\',\'Member\',\'Intern\',\'Partner\') ';		
		query += 'AND Affiliation_To_Account__r.SAP_Account_Type__c = \'Sold-to party\' ';
		query += 'AND Affiliation_From_Contact__r.Integrate_with_MMX__c = false ';
		query += 'AND Affiliation_From_Contact__r.MailingCountry IN :mmxCountryList';
 		 
		return Database.getQueryLocator(query);
	}
 	
 	global void execute(Database.BatchableContext BC, List<Affiliation__c> relationship){
 		
 		try{
 			
 			update relationship;
 			success++;
 			
 		}catch(Exception e){
 			
 			String errorMessage = 'Error updating the Relationship with Id: ' + relationship[0].Id + '. Message: ' + e.getMessage();
 			errors.add(errorMessage);
 		}
 	}   
 	
 	global void finish(Database.BatchableContext BC){
 		
 		String emailBody = '';
		
		emailBody += 'Total Success: ' + success + ' <br/>';		
		emailBody += 'Total Errors: ' + errors.size() + ' <br/><br/>';	
		
		for(String errorMessage : errors){	
			
			emailBody += '- ' + errorMessage + ' <br/>';
		}
		
		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
		email.setTargetObjectId(USerInfo.getUserId());
		email.setSaveAsActivity(false);
		email.setSubject('Contacts without MMX flag ' + Date.today().format());
		email.setHTMLBody(emailBody);
				
		Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{email});	
 	}
}