//----------------------------------------------------------------------------
// Author		: Bart Caelen
// Date			: 15/10/2018
// Description	: This class is the Controller Extension for the VF Pages "CallRecordingOverviewAccount" and "CallRecordingOverviewContact" 
//----------------------------------------------------------------------------
public class ctrlExt_CallRecordingOverview {

	//------------------------------------------------------------------------
	// Constructor
	//------------------------------------------------------------------------
	public ctrlExt_CallRecordingOverview(ApexPages.StandardController stdCTRL){

		bFullPage = false;
		Map<String, String> mapURLParameter = ApexPages.currentPage().getParameters();
		if (mapURLParameter.containsKey('fullPage')){
			if (mapURLParameter.get('fullPage') == '1') bFullPage = true;
		}

	}
	//------------------------------------------------------------------------


	//------------------------------------------------------------------------
	// Getters & Setters
	//------------------------------------------------------------------------
	public Boolean bFullPage { get; private set; }
	//------------------------------------------------------------------------

}