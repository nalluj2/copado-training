public with sharing class ctrlExt_Tender_Opportunity_BU_Info {
    
    private Opportunity opp;
    
    public List<BusinessUnitGroup> bugs {get; set;}
    public Map<String, SubBusinessUnit> sbuMap {get; set;}
        
    public ctrlExt_Tender_Opportunity_BU_Info(ApexPages.StandardController sc){
    	
    	if(Test.isRunningTest() == false){
    		
    		sc.addFields(new List<String>{'Business_Unit_Group__c', 'Business_Unit_msp__c', 'Sub_Business_Unit__c'});
    	}
    	
    	opp = (Opportunity) sc.getRecord();
    	
    	User currentUser = [Select Company_Code_Text__c from User where Id = :UserInfo.getUserId()];
    	
    	Set<String> selectedBugs = new Set<String>();
    	Set<String> selectedBus = new Set<String>();
    	Set<String> selectedSbus = new Set<String>();
    	
    	if(opp.Business_Unit_Group__c != null) selectedBugs = new Set<String>(opp.Business_Unit_Group__c.split(';'));
    	if(opp.Business_Unit_msp__c != null) selectedBus = new Set<String>(opp.Business_Unit_msp__c.split(';'));
    	if(opp.Sub_Business_Unit__c != null) selectedSbus = new Set<String>(opp.Sub_Business_Unit__c.split(';'));
    	
    	bugs = new List<BusinessUnitGroup>();
    	sbuMap = new Map<String, SubBusinessUnit>();
        	
        Map<Id, Business_Unit__c> buMap = new Map<Id, Business_Unit__c>([Select Id, (Select Name from Sub_Business_Units__r ORDER BY Name) from Business_Unit__c where Company__r.Company_Code_Text__c = :currentUser.Company_Code_Text__c]);
        	
    	for(Business_Unit_Group__c bGroup : [Select Name, (Select Id, Name from Business_Units__r ORDER BY Name) from Business_Unit_Group__c where Master_Data__r.Company_Code_Text__c = :currentUser.Company_Code_Text__c ORDER BY Name]){
    		    		
    		BusinessUnitGroup bug = new BusinessUnitGroup();
    		bug.Name = bGroup.Name;
    		
    		if(selectedBugs.contains(bug.Name)) bug.selected = true;
    		
    		bug.BusinessUnits = new List<BusinessUnit>();
    		
    		Integer bugRowSpan = 0;
    		
    		for(Business_Unit__c bUnit : bGroup.Business_Units__r){
    			
    			BusinessUnit bu = new BusinessUnit();
    			bu.Name = bUnit.Name;
    			    			    			
    			if(selectedBus.contains(bu.Name)) bu.selected = true;
    			
    			bu.SubBusinessUnits = new List<SubBusinessUnit>();
    			
    			for(Sub_Business_Units__c sbUnit : buMap.get(bUnit.Id).Sub_Business_Units__r){
    				
    				SubBusinessUnit sbu = new SubBusinessUnit();
    				sbu.Name = sbUnit.Name;
					    				    				
    				if(selectedSbus.contains(sbu.Name)) sbu.selected = true;
    				
    				bu.SubBusinessUnits.add(sbu);	
    				sbuMap.put(sbu.Name, sbu);
    			}  
    			
    			if(bu.SubBusinessUnits.size() > 0) bu.SubBusinessUnits[0].firstRow = true; 
    			bu.rowSpan = bu.SubBusinessUnits.size();
    			bugRowSpan += bu.SubBusinessUnits.size();
    			
    			bug.BusinessUnits.add(bu);
    		}
    		
    		if(bug.BusinessUnits.size() > 0) bug.BusinessUnits[0].firstRow = true;
    		bug.rowSpan = bugRowSpan;
    		
    		bugs.add(bug);    		
    	}
    }
            
    public void sbuSelected(){
    	
    	for(BusinessUnitGroup bug : bugs){
    		
    		Boolean buSelected = false;
    		
    		for(BusinessUnit bu : bug.BusinessUnits){
    			
    			Boolean sbuSelected = false;
    			
    			for(SubBusinessUnit sbu : bu.SubBusinessUnits){
    				
    				if(sbu.selected == true) sbuSelected = true;
    			}
    			
    			bu.selected = sbuSelected;
    			
    			if(bu.Selected == true) buSelected = true;
    		}	
    		
    		bug.selected = buSelected;
    	}
    }
    
    public PageReference save(){
    	
    	Set<String> selectedBugs = new Set<String>();
    	Set<String> selectedBus = new Set<String>();
    	Set<String> selectedSbus = new Set<String>();
    	
    	for(BusinessUnitGroup bug : bugs){
    		    		    		
    		for(BusinessUnit bu : bug.BusinessUnits){
    		    			
    			for(SubBusinessUnit sbu : bu.SubBusinessUnits){
    				
    				if(sbu.selected == true){
    					
    					selectedSbus.add(sbu.Name);
    					selectedBus.add(bu.Name);
    					selectedBugs.add(bug.Name);
    				}
    			}
    		}    		
    	}
    	
    	opp.Business_Unit_Group__c = String.join(new List<String>(selectedBugs), ';');
    	opp.Business_Unit_msp__c = String.join(new List<String>(selectedBus), ';');
    	opp.Sub_Business_Unit__c = String.join(new List<String>(selectedSbus), ';');
    	
    	try{
    		
    		update opp;
    		
    	}catch(Exception e){
    		
    		ApexPages.addMessages(e);
    		return null;
    	}
    	
    	PageReference pr = new PageReference('/' + opp.Id);
    	pr.setRedirect(true);
    	return pr;
    }
    
    public class BusinessUnitGroup {
    	
    	public String name {get; set;}
    	public Boolean selected {get; set;}
    	public Integer rowSpan {get; set;}
    	
    	public List<BusinessUnit> businessUnits {get; set;}
    }
    
    public class BusinessUnit {
    	
    	public String name {get; set;}
    	public Boolean selected {get; set;}
    	public Boolean firstRow {get; set;}
    	public Integer rowSpan {get; set;}
    	
    	public List<SubBusinessUnit> subBusinessUnits {get; set;}
    }
    
    public class SubBusinessUnit {
    	
    	public String name {get; set;}
    	public Boolean selected {get; set;}
    	public Boolean firstRow {get; set;}   	  	    
    }    
}