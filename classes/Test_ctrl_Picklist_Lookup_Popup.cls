@isTest
private class Test_ctrl_Picklist_Lookup_Popup {
	
	private testmethod static void testCommunication(){
		
		Time_Registration_Code__c regCode = new Time_Registration_Code__c();
		regCode.Name='projCode';
		regCode.Time_Registration_Category__c = 'catCode';
		regCode.Active__c = true;
		regCode.Business_Unit__c = 'Test BU';
		regCode.Time_Registration_ID__c = 'TRC';
				
		insert regCode;
		
		Project__c release = new Project__c();
		release.Name='Test Release';
		release.Start_Date__c=Date.today();
		release.Project_Type__c='Minor Release';
		release.Status__c='Scheduled';
		release.Project_Manager__c=UserInfo.getUserId();
		release.End_Date__c=Date.today().addDays(10);
		release.Time_Registration_Project__c = regCode.Id;
		//release.Kronos_Category__c='test category Id';
		//release.Kronos_Project__c='test project Id';
		release.Total_Budget__c=1000;
		
		insert release;
					
		Test.startTest();
				
		ctrl_Lookup lookupCon = new ctrl_Lookup();
		lookupCon.ObjectName = 'Project__c';
		lookupCon.fieldNames = 'Name,Project_Type__c';
		lookupCon.filter = ' WHERE Id != null ';		
		lookupCon.compId = 'release';
		
		String params = lookupCon.getEncodedParams();
		
		Pagereference pr = Page.Lookup_Popup;
		pr.getParameters().put('p', EncodingUtil.urlDecode(params, 'UTF-8'));
		
		Test.setCurrentPageReference(pr);
		
		ctrl_Picklist_Lookup_Popup popupCon = new ctrl_Picklist_Lookup_Popup();
				
		System.assert(popupCon.records.size() == 1);
		
		popupCon.selectedColumn = 'Project_Type__c';
		//Sort ASC
		popupCon.sortTable();
		//Sort DESC
		popupCon.sortTable();
				
		Test.stopTest();
	}
}