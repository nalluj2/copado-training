public with sharing class Filter{
        public List<SelectOption> ObjectFields {get;set;}
        public List<SelectOption> Operators {get;set;}
        public string SelectedObjectField{get;set;}
        public string SelectedOperator{get;set;}
                        
        public String Value{get;set;}
        public integer Id{get;set;}
        
        public Filter(List<SelectOption> objectFields,List<SelectOption> operators, integer id,string selectedObjectField){
            this.Operators = operators;
            this.ObjectFields = objectFields;
            this.Id = id;
            this.SelectedObjectField = selectedObjectField;
        }
    }