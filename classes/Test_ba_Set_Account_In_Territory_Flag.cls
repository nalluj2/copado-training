@isTest
private class Test_ba_Set_Account_In_Territory_Flag {
    
    private static testMethod void testBatch(){
    	
    	List<Account> accounts = new List<Account>();
    	
    	for(Integer i = 0; i < 1000; i++){
    		
    		Account acc = new Account();
    		acc.Name = 'Unit Test Account ' + i;
    		acc.SAP_Id__c = '999' + i;
    		if(Math.mod(i, 2) != 0) acc.Territory_Assigned__c = true;
    		accounts.add(acc);
    	}
    	
    	insert accounts;
    	
    	Territory2 ter = [Select Id from Territory2 where DeveloperName = 'Unit_Test_Territory'];
    	
    	Account acc1 = accounts.get(0);
    	Account acc500 = accounts.get(499);
    	Account acc1000 = accounts.get(999);
    	
    	List<ObjectTerritory2Association> accShares = new List<ObjectTerritory2Association>();
    	
    	ObjectTerritory2Association acc1Share = new ObjectTerritory2Association();
    	acc1Share.ObjectId = acc1.Id;
    	acc1Share.Territory2Id = ter.Id;
    	acc1Share.AssociationCause = 'Territory2Manual';
    	accShares.add(acc1Share);
    	
    	ObjectTerritory2Association acc500Share = new ObjectTerritory2Association();
    	acc500Share.ObjectId = acc500.Id;
    	acc500Share.Territory2Id = ter.Id;
    	acc500Share.AssociationCause = 'Territory2Manual';
    	accShares.add(acc500Share);
    	
    	ObjectTerritory2Association acc1000Share = new ObjectTerritory2Association();
    	acc1000Share.ObjectId = acc1000.Id;
    	acc1000Share.Territory2Id = ter.Id;
    	acc1000Share.AssociationCause = 'Territory2Manual';
    	accShares.add(acc1000Share);
    	
    	insert accShares;
    	
    	Test.startTest();
    	
    	ba_Set_Account_In_Territory_Flag batch = new ba_Set_Account_In_Territory_Flag();
    	batch.accounts = accounts;
    	
    	Database.executeBatch(batch, 2000);
    	
    	Test.stopTest();
    	
    	for(Account acc : [Select Id, Territory_Assigned__c from Account where Id IN :accounts]){
    		
    		if(acc.Id == acc1.Id || acc.Id == acc500.Id || acc.Id == acc1000.Id) System.assert(acc.Territory_Assigned__c == true);
    		else System.assert(acc.Territory_Assigned__c == false);
    	}
    }
    
    
    @TestSetup
    private static void createData(){
    	
    	Territory2 ter = new Territory2();
		ter.Name = 'Unit Test Territory';
		ter.Description = 'Unit Test Territory';
		ter.DeveloperName = 'Unit_Test_Territory';
		ter.Business_Unit__c = 'Vascular';
		ter.Company__c = 'a0w11000001Q3y9';
		ter.Country_UID__c = 'NL';
		ter.Territory2TypeId = bl_territory.territoryTypeMap.get('Territory');
		ter.Devart_Therapy_Groups__c = 'Aortic';
		ter.Territory2ModelId = [Select Id from Territory2Model where DeveloperName = 'Medtronic'].Id;
		insert ter;
    	
    }
}