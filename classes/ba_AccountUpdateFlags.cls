/**
 *	@package	classes
 *	@name 		ba_AccountUpdateFlags
 *	@description
 *
 *	@author		alwin
 *	@date 		01/18/2017

 * @copyright   Copyright (C) 2017 7Dots. All rights reserved.
 */

global class ba_AccountUpdateFlags implements Schedulable, Database.Batchable<SObject>, Database.Stateful {
	
	private Integer totalUpdated = 0;	
	private Integer totalUntouched = 0;
		
	// Master data	
	private Map<String, Therapy_Group__c> companyTherapyGroupMap;
			
	// Business Unit	
	Map<Id, Business_Unit__c> businessUnitMap;	
	Map<Id, Sub_Business_Units__c>	subBusinessUnitMap;
	
	List<String> accountFieldNames;
   
    // Schedulable
    global void execute(SchedulableContext ctx){

        Database.executeBatch(new ba_AccountUpdateFlags(), 2000);
    }

    global Database.QueryLocator start(Database.BatchableContext ctx){

		// Set Master Data		
		companyTherapyGroupMap = new Map<String, Therapy_Group__c>();
		for(Therapy_Group__c tg : [SELECT Id, Name, Company__c, Sub_Business_Unit__r.Business_Unit__c FROM Therapy_Group__c]) companyTherapyGroupMap.put(tg.Name + ':' + String.valueOf(tg.Company__c).subString(0,15), tg);			
								
		// Business Unit Data
		businessUnitMap = new Map<Id, Business_Unit__c>([SELECT Id, Account_Flag__c FROM Business_Unit__c WHERE Account_Flag__c != null]);		
		subBusinessUnitMap = new Map<Id, Sub_Business_Units__c>([SELECT Id, Account_Flag__c FROM Sub_Business_Units__c WHERE Account_Flag__c != null]);
		
		Set<String> accountFieldNameSet = new Set<String>();
		
		for(Business_Unit__c bu : businessUnitMap.values()) accountFieldNameSet.add(bu.Account_Flag__c);
		for(Sub_Business_Units__c sbu : subBusinessUnitMap.values()) accountFieldNameSet.add(sbu.Account_Flag__c);
		
		accountFieldNames = new List<String>(accountFieldNameSet);
		
		Set<String> setExcludedCountries = SharedMethods.retExcludefromAccountFlagging();
		
		String query = 'SELECT Id,SAP_Account_Type__c,' + String.join(accountFieldNames,',');		
		query += ' FROM Account';
		query += ' WHERE RecordType.DeveloperName NOT IN (\'Competitor\', \'DIB_Competitor\', \'Buying_Entity\')';
		query += ' AND Account_Country_vs__c NOT IN :setExcludedCountries';
		
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext ctx, List<Account> accounts){
        
		Map<Id, Account> accountMap = new Map<Id, Account>(accounts);
		        
        List<Account> accountsWithTerritories = [Select Id, (Select Territory2.Id, Territory2.Name, Territory2.Therapy_Groups_Text__c, Territory2.Business_Unit__c, Territory2.Company__c from ObjectTerritory2Associations) from Account where Id IN :accounts];
        		
        List<Account> toUpdate = new List<Account>();
       
        for(Account accTerritories : accountsWithTerritories){
			
			Account account = accountMap.get(accTerritories.Id);
			
			// Get the status before setting the flags
			Integer accountHash = System.hashCode(account);
			
			// Clear all the Account Flag fields except Diabetes or Ship to Accounts without Territories
			if(accTerritories.ObjectTerritory2Associations.size() > 0 || account.SAP_Account_Type__c  != 'Ship to Party'){
				
				for(String fieldName : accountFieldNames){
					
					if(fieldName != 'Diabetes__c' && fieldName != 'Diabetes_Core__c') account.put(fieldName, false);
				}
			}
				
			for(ObjectTerritory2Association accShare : accTerritories.ObjectTerritory2Associations){
            
            	Territory2 accTer = accShare.Territory2;
            	            	
            	// If the Account Territory is of type 'Territory'            	
            	if(accTer != null && accTer.Therapy_Groups_Text__c != null && accTer.Company__c != null){				
	
		            for(String thearapyGroupName : accTer.Therapy_Groups_Text__c.split(';')){
                                                              
                    	String key = thearapyGroupName.trim() + ':' + accTer.Company__c.subString(0,15);

                        Therapy_Group__c therapyGroup = companyTherapyGroupMap.get(key);

						// If the Therapy Group + Company combination doesn't exist we skip it
                        if(therapyGroup == null) continue;
                        
                        Sub_Business_Units__c sbu = subBusinessUnitMap.get(therapyGroup.Sub_Business_Unit__c);
                        // If the Sub Business Unit has a Flag Field we set it to true
                        if(sbu != null) account.put(sbu.Account_Flag__c, true);								
						                        
                        Business_Unit__c bu =  businessUnitMap.get(therapyGroup.Sub_Business_Unit__r.Business_Unit__c);
                        // If the Business Unit has a Flag Field we set it to true                        
						if(bu != null) account.put(bu.Account_Flag__c, true);					
                    }
                }			
            }
            
            // If the Account field values has changed from the original state we update it
            if(System.hashCode(account) != accountHash){
            	
            	System.debug('====== To update: ' + account.Id);
            	toUpdate.add(account);
            	totalUpdated++;            	
            	
            }else{
            	
            	System.debug('====== Not changed: ' + account.Id);
            	totalUntouched++;
            }
        }
        
		if(toUpdate.isEmpty() == false) update toUpdate;	 
    }

    global void finish(Database.BatchableContext BC){
		
		String emailBody = '';
				
		emailBody += 'Total Updated: ' + totalUpdated + ' <br/>';
		emailBody += 'Total Untouched: ' + totalUntouched + ' <br/><br/>';	
								
		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
		email.setTargetObjectId(USerInfo.getUserId());
		email.setSaveAsActivity(false);
		email.setSubject('Account Update Flags batch results ' + Date.today().format());
		email.setHTMLBody(emailBody);
				
		Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{email});		
		
    }
}