public with sharing class ctrl_MultiSelectComponent {

	//------------------------------------------------------
	// Private Variables
	//------------------------------------------------------
	private Map<String, sObject> mapLeftOption = new Map<String, sObject>();
	private Map<String, sObject> mapRightOption = new Map<String, sObject>();
	private Boolean bInitialSearchExecuted = false;
	//------------------------------------------------------


	//------------------------------------------------------
	// Constructor
	//------------------------------------------------------
	public ctrl_MultiSelectComponent() {
		
		lstLeft_Selected = new List<String>();
		lstRight_Selected = new List<String>();

	}
	//------------------------------------------------------
  

	//------------------------------------------------------
	// Getters & Setters
	//------------------------------------------------------
	public String tSObject_Name { get; set; }
	public String tFieldName_Id { get; set; }
	public String tFieldName_Value { get; set; }
	public String tAdditional_Where { get; set; }
	public Integer iRecord_Limit { get; set; }
	public String tSearchMessage { get ; private set; }

	public Boolean bExecuteInitialSearch { get; set; }

	public List<String> lstRight_Initial { get; set; }
	public List<String> lstRight_Current { get; set; }
	public String tSearchText { get; set; }
	public List<String> lstLeft_Selected { get; set; }
	public List<String> lstRight_Selected { get; set; }

	// lstLeft_Option - return SelectOptions for the left/unselected box
	public List<SelectOption> lstLeft_Option {
	
		get{
			List<SelectOption> lstOption = new list<SelectOption>();
			List<sObject> lstValue; 

			if ( (bExecuteInitialSearch == true) && (bInitialSearchExecuted == false) ) InitialSearch();
			
			lstValue = mapLeftOption.values();
			lstValue.sort();  // sort by name
			for (sObject oSObject : lstValue){ 
				lstOption.add(new SelectOption(String.valueOf(oSObject.get(tFieldName_Id)), String.valueOf(oSObject.get(tFieldName_Value))));
			}
			return lstOption;
		}

	}

 	// lstRight_Option - return SelectOptions for the right/selected box
	public List<SelectOption> lstRight_Option {

		get{
			List<SelectOption> lstOption = new List<SelectOption>();
			List<sObject> lstValue;
			List<sObject> lstData;

			// clear is used instead of new list, so the list maintains the pointer to the VFPage Controller list
			if (lstRight_Current != null) lstRight_Current.clear();  

			// load initially selected records into the right box 
			if (lstRight_Initial != null && lstRight_Initial.size() > 0) {
				
				String tSOQL_Tmp = 'SELECT ' + tFieldName_Id + ',' + tFieldName_Value + ' FROM ' + tSObject_Name;
				tSOQL_Tmp += ' WHERE Id IN :lstRight_Initial';
				if (!String.isBlank(tAdditional_Where)){
					tSOQL_Tmp += ' AND (' + tAdditional_Where + ')';
				}
				tSOQL_Tmp += ' ORDER BY ' + tFieldName_Value;
				tSOQL_Tmp += ' LIMIT ' + iRecord_Limit;
				
				System.debug('**BC** tSOQL_Tmp : ' + tSOQL_Tmp);
				mapRightOption = new Map<String, sObject>(Database.query(tSOQL_Tmp));

				lstRight_Initial.clear();
			}

			lstValue = mapRightOption.values();
			lstValue.sort();  // sort by Value Field
			for (sObject oSObject : lstValue) { 
				lstOption.add(new SelectOption(String.valueOf(oSObject.get(tFieldName_Id)), String.valueOf(oSObject.get(tFieldName_Value))));
				lstRight_Current.add(String.valueOf(oSObject.get(tFieldName_Id)));
			}   
			return lstOption;
		}

	}
	//------------------------------------------------------


	//------------------------------------------------------
	// Actions
	//------------------------------------------------------
	// ClickRight - Right pointing arrow was clicked. Move selected options to the right box.
	public PageReference ClickRight(){
		
		lstRight_Selected.clear();
		for(String tSelected : lstLeft_Selected){
			if (mapLeftOption.containsKey(tSelected)){
				mapRightOption.put(tSelected, mapLeftOption.get(tSelected));
			}
			mapLeftOption.remove(tSelected);
		}
		return null;

	}

	// ClickLeft - Left pointing arrow was clicked. Move selected options to the left box.
	public PageReference ClickLeft(){

		lstLeft_Selected.clear();
		for(String tSelected : lstRight_Selected){
			if (mapRightOption.containsKey(tSelected)) {
				mapLeftOption.put(tSelected, mapRightOption.get(tSelected));        
			}
			mapRightOption.remove(tSelected);
		}
		return null;

	}

	// Search for records by the Name Field and add them to the left box
	public PageReference Search(){
	
		String tSearchValue = '%' + tSearchText + '%';
		Set<String> setRightOption = mapRightOption.keySet();

		String tSOQL_Tmp = 'SELECT ' + tFieldName_Id + ',' + tFieldName_Value + ' FROM ' + tSObject_Name;
		tSOQL_Tmp += ' WHERE ' + tFieldName_Value + ' like :tSearchValue';
		tSOQL_Tmp += ' AND ' + tFieldName_Id + ' != :setRightOption';
		if (!String.isBlank(tAdditional_Where)){
			tSOQL_Tmp += ' AND (' + tAdditional_Where + ')';
		}
		tSOQL_Tmp += ' ORDER BY ' + tFieldName_Value;
		tSOQL_Tmp += ' LIMIT ' + iRecord_Limit;
		
		mapLeftOption = new Map<String, sObject>(Database.query(tSOQL_Tmp));

		tSearchMessage = '';
		if (mapLeftOption.size() == 0){
			tSearchMessage = 'No records found!';
		}

		return null;

	}	
	//------------------------------------------------------


	//------------------------------------------------------
	// Private
	//------------------------------------------------------
	private void InitialSearch(){

		Set<String> setRightOption = mapRightOption.keySet();

		String tSOQL_Tmp = 'SELECT ' + tFieldName_Id + ',' + tFieldName_Value + ' FROM ' + tSObject_Name;
		tSOQL_Tmp += ' WHERE ' + tFieldName_Id + ' != :setRightOption';
		if (!String.isBlank(tAdditional_Where)){
			tSOQL_Tmp += ' AND (' + tAdditional_Where + ')';
		}
		tSOQL_Tmp += ' ORDER BY ' + tFieldName_Value;
		tSOQL_Tmp += ' LIMIT ' + iRecord_Limit;
		
		mapLeftOption = new Map<String, sObject>(Database.query(tSOQL_Tmp));

		tSearchMessage = '';
		if (mapLeftOption.size() == 0){
			tSearchMessage = 'No records found!';
		}

		bInitialSearchExecuted = true;
	}
	//------------------------------------------------------

}