public with sharing class ctrlExt_Account_Plan_Objective {
	

	private Account_Plan_Objective__c oAccountPlanObjective;
			
	public ctrlExt_Account_Plan_Objective(ApexPages.StandardController oStdCtrl){
		
		oAccountPlanObjective = (Account_Plan_Objective__c)oStdCtrl.getRecord();					
	
	}
	

	public PageReference redirectWithRecordType(){
		
		PageReference oPageReference;
		
		Id id_RecordType_RTG = RecordTypeMedtronic.getRecordTypeByDevName('Account_Plan_Objective__c' , 'RTG').Id;
		Id id_RecordType_General = RecordTypeMedtronic.getRecordTypeByDevName('Account_Plan_Objective__c' , 'General').Id;
		
		System.debug('**BC** ctrlExt_Account_Plan_Objective - oAccountPlanObjective.Id : ' + oAccountPlanObjective.Id);
		System.debug('**BC** ctrlExt_Account_Plan_Objective - oAccountPlanObjective.RecordTypeId : ' + oAccountPlanObjective.RecordTypeId + ' - ' + id_RecordType_RTG);

		if (oAccountPlanObjective.Id != null){
			
			if (oAccountPlanObjective.RecordTypeId == id_RecordType_RTG){
				
				oPageReference = new PageReference('/apex/Account_Plan_Objective_Edit_RTG');
					oPageReference.getParameters().putAll(ApexPages.currentpage().getparameters());				
					oPageReference.getParameters().remove('save_new');
					oPageReference.getParameters().put('saveURL', oPageReference.getParameters().get('retURL'));
				
			}else{
				
				oPageReference = new PageReference('/' + oAccountPlanObjective.Id + '/e');
					oPageReference.getParameters().putAll(ApexPages.currentpage().getparameters());
					oPageReference.getParameters().remove('sfdc.override');
					oPageReference.getParameters().remove('save_new');				
					oPageReference.getParameters().put('saveURL', oPageReference.getParameters().get('retURL'));
					oPageReference.getParameters().put('nooverride', '1');
			}
			
		}else{
				
			if (oAccountPlanObjective.Account_Plan__c == null) return null;
					
			Account_Plan_2__c oAccountPlan = [SELECT RecordTypeId, RecordType.DeveloperName, Business_Unit__c, Sub_Business_Unit__c, Business_Unit_Group__c, Account_Plan_Level__c FROM Account_Plan_2__c WHERE Id = :oAccountPlanObjective.Account_Plan__c];		

			Id id_RecordType;

			if (oAccountPlan.RecordType.DeveloperName == 'SAM_Account_Plan'){

				id_RecordType = id_RecordType_General;

			}else{

				String tSOQL = 'SELECT Account_Plan_Objective_Record_Type__c FROM Account_Plan_Level__c';
			
				if (oAccountPlan.Account_Plan_Level__c == 'Business Unit Group' || oAccountPlan.Account_Plan_Level__c == 'Business Unit'){
				
					tSOQL += ' WHERE Account_Plan_Level__c = \'Business Unit Group\'  AND Business_Unit__r.Business_Unit_Group__c = \'' + oAccountPlan.Business_Unit_Group__c + '\'';
				
				}else if (oAccountPlan.Account_Plan_Level__c == 'Sub Business Unit'){
				
					tSOQL += ' WHERE Account_Plan_Level__c = \'Sub Business Unit\'  AND Business_Unit__c = \'' + oAccountPlan.Business_Unit__c + '\'';			

				}

			
				tSOQL += ' LIMIT 1';
			
				List<Account_Plan_Level__c> lstAccountPlanLevel = Database.query(tSOQL);

						
				if (lstAccountPlanLevel.size() == 1){
				
					id_RecordType = lstAccountPlanLevel[0].Account_Plan_Objective_Record_Type__c;

				}
			
				if (id_RecordType == null){
				
					id_RecordType = id_RecordType_General;			
			
				}

			}
									
			
			if (id_RecordType == id_RecordType_RTG){								
				
				oPageReference = new PageReference('/apex/Account_Plan_Objective_Edit_RTG');
					oPageReference.getParameters().putAll(ApexPages.currentpage().getparameters());
					oPageReference.getParameters().remove('save_new');
					oPageReference.getParameters().put('saveURL', oPageReference.getParameters().get('retURL'));
				
			}else{	
									
				String keyPrefix = Account_Plan_Objective__c.sObjectType.getDescribe().getKeyPrefix();
								
				oPageReference = new PageReference('/' + keyPrefix + '/e');
					oPageReference.getParameters().putAll(ApexPages.currentpage().getparameters());
					oPageReference.getParameters().remove('sfdc.override');
					oPageReference.getParameters().remove('save_new');
					oPageReference.getParameters().put('RecordType', id_RecordType);
					oPageReference.getParameters().put('saveURL', '/' + oAccountPlan.Id );
					oPageReference.getParameters().put('nooverride', '1');
			}
		}
								
		oPageReference.setRedirect(true);
		
		return oPageReference;

	}


}