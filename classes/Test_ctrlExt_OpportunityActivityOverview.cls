/*
 *      Created Date    : 2014-04-17
 *      Author          : Bart Caelen
 *      Description     : TEST Class for the APEX Controller Extension ctrlExt_OpportunityActivityOverview
 */
@isTest private class Test_ctrlExt_OpportunityActivityOverview {
    
    // SeeAllData is needed to be able to SOQL the OpenActivity and ActivityHistory records
    @isTest (SeeAllData=true) 
    static void test_ctrlExt_OpportunityActivityOverview() {
        //------------------------------------------------
        // Variables
        //------------------------------------------------
        string tTest = '';
        List<ctrlExt_OpportunityActivityOverview.wr_Activity> lstWRAct = new List<ctrlExt_OpportunityActivityOverview.wr_Activity>();
        //------------------------------------------------

        clsTestData.iRecord_Task    = 5;
        clsTestData.iRecord_Event   = 5;

        User oUser_System = clsTestData_User.createUser_SystemAdministratorMDT('tadm01', false);
            oUser_System.Primary_sBU__c = 'Brain Modulation';
        insert oUser_System;

        //------------------------------------------------
        // CREATE TEST DATA - OPPORTUNITY AND RELATED DATA
        //------------------------------------------------
        System.runAs(oUser_System){
            clsTestData.createOpportunityData(false);
                clsTestData.oMain_Opportunity.Business_Unit_msp__c = 'Cranial Spinal';
            insert clsTestData.oMain_Opportunity;

            // Tasks for Opportunity
            clsTestData.idTask_What     = clsTestData.oMain_Opportunity.Id;
            clsTestData.tTask_Status    = 'Not Started';
            clsTestData.oMain_Task      = null;
            clsTestData.createTaskData();

            clsTestData.tTask_Status    = 'Completed';
            clsTestData.oMain_Task      = null;
            clsTestData.createTaskData();

            // Events for Opportunity
            clsTestData.idEvent_What            = clsTestData.oMain_Opportunity.Id;
            clsTestData.dtEventStartDateTime    = DateTime.now().addDays(1);
            clsTestData.dtEventEndDateTime      = DateTime.now().addDays(2);
            clsTestData.oMain_Event             = null;
            clsTestData.createEventData();

            clsTestData.idEvent_What            = clsTestData.oMain_Opportunity.Id;
            clsTestData.dtEventStartDateTime    = DateTime.now().addDays(-2);
            clsTestData.dtEventEndDateTime      = DateTime.now().addDays(-1);
            clsTestData.oMain_Event             = null;
            clsTestData.createEventData();
        }
        //------------------------------------------------

        //------------------------------------------------
        // CREATE TEST DATA - Stakeholder_Mapping__c AND RELATED DATA
        //------------------------------------------------
        System.runAs(oUser_System){
            clsTestData.createStakeHolderMappingData();
        }

        // Tasks for Stakeholder_Mapping__c
        System.runAs(oUser_System){
            clsTestData.idTask_What     = clsTestData.oMain_StakeHolderMapping.Id;
            clsTestData.tTask_Status    = 'Not Started';
            clsTestData.oMain_Task      = null;
            clsTestData.createTaskData();

            clsTestData.idTask_What     = clsTestData.oMain_StakeHolderMapping.Id;
            clsTestData.tTask_Status    = 'Completed';
            clsTestData.oMain_Task      = null;
            clsTestData.createTaskData();

            // Events for Stakeholder_Mapping__c
            clsTestData.idEvent_What            = clsTestData.oMain_StakeHolderMapping.Id;
            clsTestData.dtEventStartDateTime    = DateTime.now().addDays(1);
            clsTestData.dtEventEndDateTime      = DateTime.now().addDays(2);
            clsTestData.oMain_Event             = null;
            clsTestData.createEventData();

            clsTestData.idEvent_What            = clsTestData.oMain_StakeHolderMapping.Id;
            clsTestData.dtEventStartDateTime    = DateTime.now().addDays(-2);
            clsTestData.dtEventEndDateTime      = DateTime.now().addDays(-1);
            clsTestData.oMain_Event             = null;
            clsTestData.createEventData();
        }
        //------------------------------------------------

        //------------------------------------------------
        // CREATE TEST DATA - Summary_Of_My_Position_Today__c AND RELATED DATA
        //------------------------------------------------
        System.runAs(oUser_System){
            clsTestData.createSummaryOfMyPositionToday();

            // Tasks for Summary_Of_My_Position_Today__c
            clsTestData.idTask_What     = clsTestData.oMain_SummaryOfMyPositionToday.Id;
            clsTestData.tTask_Status    = 'Not Started';
            clsTestData.oMain_Task      = null;
            clsTestData.createTaskData();

            clsTestData.idTask_What     = clsTestData.oMain_SummaryOfMyPositionToday.Id;
            clsTestData.tTask_Status    = 'Completed';
            clsTestData.oMain_Task      = null;
            clsTestData.createTaskData();

            // Events for Summary_Of_My_Position_Today__c
            clsTestData.idEvent_What            = clsTestData.oMain_SummaryOfMyPositionToday.Id;
            clsTestData.dtEventStartDateTime    = DateTime.now().addDays(1);
            clsTestData.dtEventEndDateTime      = DateTime.now().addDays(2);
            clsTestData.oMain_Event             = null;
            clsTestData.createEventData();

            clsTestData.idEvent_What            = clsTestData.oMain_SummaryOfMyPositionToday.Id;
            clsTestData.dtEventStartDateTime    = DateTime.now().addDays(-2);
            clsTestData.dtEventEndDateTime      = DateTime.now().addDays(-1);
            clsTestData.oMain_Event             = null;
            clsTestData.createEventData();
        }
        //------------------------------------------------

        //------------------------------------------------
        // Perform Testing
        //------------------------------------------------
        test.startTest();

        System.runAs(oUser_System){

            ApexPages.StandardController oSTDCTRL = new ApexPages.StandardController(clsTestData.oMain_Opportunity);
            ctrlExt_OpportunityActivityOverview oCTRL = new ctrlExt_OpportunityActivityOverview(oSTDCTRL);

            lstWRAct = oCTRL.lstWROpenActivity;        
            lstWRAct = oCTRL.lstWRActivityHistory;        

            tTest = oCTRL.tURL_Redirect;

        }

        test.stopTest();
        //------------------------------------------------
 
    }
    
}