/*	Integration Service related class
	
	This batch takes care to process the notifications received from the source Org. On each execution it takes in scope all the notifications
	with status New (not processed yet) or Processed (executed but failed to inform source Org) so each notification will be re-tried after any 
	possible error in previous processing. Please notice that only notificatons of type Inbound are processed (to avoid conflicts when the same 
	Org can be target and source of notifications). 
*/
global class ba_SynchronizationService_Inbound implements Database.Batchable<SObject>, Database.Stateful, Database.AllowsCallouts {
			
	//List of Notifications that were processed successfully. To be used in the Finish method
	private List<Sync_Notification__c> toAcknowledge = new List<Sync_Notification__c>();
	
	private bl_SynchronizationService_Utils.SessionInfo sessionInfo;
	
	//List of errors occured
	private List<String> errors = new List<String>();
	
	global Database.Querylocator start(Database.BatchableContext BC){
      	      	
      	sessionInfo = bl_SynchronizationService_Utils.getTargetOrgSession();
      	      	
      	//Limited to 2000 because if we get more, then we are not able to send back that many confirmations      	      	     	
      	String query = 'Select Id, Status__c, Type__c, Notification_Id__c, Record_Id__c, Record_Object_Type__c, Process_Time__c, Acknowledge_Time__c from Sync_Notification__c where Status__c IN (\'New\', \'Processed\') AND Type__c = \'Inbound\' ORDER BY CreatedDate ASC LIMIT 2000';
      	
      	return Database.getQuerylocator(query);
   	}
   	
   	//Because we need to handle each record as an atomic unit, even when it has related objects, we will process one notification per execution.
   	//Performance wise is not the best solution, but taking into account that in 95% of the cases this batch will be triggered by a single notification
   	//the overall performance is almost not affected
   	global void execute(Database.BatchableContext BC, List<Sync_Notification__c> notifications){
        
        //If this batch is executed with a size bigger than 1 record, we notify the admins about it but we continue its execution with only the
        //first record. The rest will be executed in future executions of the batch
        if(notifications.size() > 1){
        	
        	errors.add('Error in batch execution. Target batch was executed with a Batch Size bigger than 1.');        	
        }
        
        //We only process 1 per time
        Sync_Notification__c notification = notifications[0];
        
        //For processing
        if(notification.Status__c == 'New'){
          	
          	//Because is a GET method, we cannot use the Body of the request. Instead we send the info as parameters
          	String serviceURL = '/services/apexrest/SynchronizationServiceSource?recordId=' + EncodingUtil.URLEncode(notification.Record_Id__c, 'UTF-8') + '&recordObjectType=' + EncodingUtil.URLEncode(notification.Record_Object_Type__c, 'UTF-8');
          		     		    		    
		    SynchronizationServiceResponse response;
		    
		    try {
	    		
	    		try{
	    			
	    			response = (SynchronizationServiceResponse) bl_SynchronizationService_Utils.executeService(serviceURL, 'GET', null, SynchronizationServiceResponse.class, sessionInfo);
	    		
	    		}catch(Exception ex) {
	    			
	    			if(ex.getMessage().contains('INVALID_SESSION_ID')){
	    				
	    				sessionInfo = bl_SynchronizationService_Utils.getTargetOrgSession();
	    				
	    				response = (SynchronizationServiceResponse) bl_SynchronizationService_Utils.executeService(serviceURL, 'GET', null, SynchronizationServiceResponse.class, sessionInfo);
	    				
	    			}else throw ex;
	    		}	    			
	    		
	    	}catch(Exception e) {
	        	
	        	errors.add('Error during Rest call: ' + e.getMessage() + ' - ' + e.getStacktraceString());
        	
        		//If we get an error during the call, we stop here. The notifications will keep in 'New' status so they will be picked up in next execution of the batch
        		return;
	    	}
	     	     	
	     	//We send the payload for processing in its specific implementation. If the processing is successful it will return the external id of 
	     	//the record. Otherwise it will return null
     		String processResult;
     		
     		try{
     				
     			processResult = bl_SynchronizationService_Target.processRecordPayload(response.recordInformation, notification.Record_Object_Type__c);     		     
	     	
	     	}catch(Exception e) {
	        	
	        	errors.add(notification.Record_Object_Type__c + ' (' + notification.Record_Id__c + '). Message: ' +  e.getMessage());
        	
        		//If we get an error during the call, we stop here. The notifications will keep in 'New' status so they will be picked up in next execution of the batch
        		return;
	    	}
	    	
	     	//If no error in processing we update the local notification so it won't be processed again
	     	if(processResult != null){     	
	     		
	     		notification.Status__c = 'Processed';
	     		notification.Process_Time__c = DateTime.now(); 
	     		     
	    		update notification;
	    		
	     	}else{
	     		
	     		errors.add('Error during record processing. Record External Id =  ' + notification.Record_Id__c + '. Record Type = ' + notification.Record_Object_Type__c);
        	
        		//If we get an error during processing, we stop here. The notifications will keep in 'New' status so they will be picked up in next execution of the batch
        		return;
	     	}
        }
     	
     	//For Acknowledge in source Org. Because we are not allowed to do Callouts once a DML operation has been performed, the execution of acknowledging
     	//the notification is done in the Finish method.
     	if(notification.Status__c == 'Processed'){
     		
     		toAcknowledge.add(notification);    		
     	}         	     
    }
 	
 	//Finish with the acknowledge of the successfuly processed notifications and informing the Admins about possible errors
   	global void finish(Database.BatchableContext BC){
   		
   		if(toAcknowledge.size() > 0){
   		   					    
	    	SynchronizationServiceRequest syncRequest = new SynchronizationServiceRequest();
	    	syncRequest.notifications = toAcknowledge;
  			
  			//The REST service returns only the Notifications that were successfully acknoledged in the source Org.
	    	SynchronizationServiceResponse response;	   	    
	    		    	
	    	try {
	    		
	    		try{
	    			
	    			response = (SynchronizationServiceResponse) bl_SynchronizationService_Utils.executeService('/services/apexrest/SynchronizationServiceSource', 'POST', JSON.serialize(syncRequest), SynchronizationServiceResponse.class, sessionInfo);	
				
				}catch(Exception ex) {
	    			
	    			if(ex.getMessage().contains('INVALID_SESSION_ID')){
	    				
	    				sessionInfo = bl_SynchronizationService_Utils.getTargetOrgSession();
	    				
	    				response = (SynchronizationServiceResponse) bl_SynchronizationService_Utils.executeService('/services/apexrest/SynchronizationServiceSource', 'POST', JSON.serialize(syncRequest), SynchronizationServiceResponse.class, sessionInfo);
	    				
	    			}else throw ex;
	    		}	 
	    			    			
	    	} catch(Exception e) {
	        	
				errors.add('Error during Rest call: ' + e.getMessage() + ' - ' + e.getStacktraceString());
	    	}
     	    
     	    //If there were no errors
     	    if(response != null){
     	        		
	     		DateTime now = DateTime.now();
	     		
	     		//Update the successfully acknowledged notifications localy so they are not processed again
	     		for(Sync_Notification__c respNotification : response.notifications){
	     	
	     			respNotification.Status__c = 'Acknowledged';
	     			respNotification.Acknowledge_Time__c = now; 
	     		}
	     
	    		update response.notifications;  	
     	    }	
   		}
   		
   		//Notify the Sync Service Admins of possible errors
   		if(errors.size() > 0){
   			
   			bl_SynchronizationService_Utils.notifySyncErrors(errors); 
   		}
   		
   		//If the interface is enabled
   		if(bl_SynchronizationService_Utils.isSyncEnabled == true){ 
   		
	   		//Some new notifications may have been created as a result of the sync execution. If so we need to launch the batch to send out the notification here
	   		List<Sync_Notification__c> newNotifications = [Select Id from Sync_Notification__c where Status__c = 'New' AND Type__c = 'Outbound' LIMIT 1 ];
	   		
	   		if(newNotifications.size() > 0){
	   			
	   			//Check if there is already a batch execution in the queue. If so, there is no need to create a new one.	
				List<AsyncApexJob> pendingJobs = [SELECT Id FROM AsyncApexJob where JobType = 'BatchApex' AND ApexClass.Name = 'ba_SynchronizationService_Outbound' AND Status IN ('Holding', 'Queued') LIMIT 1 ];
					
				if(pendingJobs.isEmpty()){
					ba_SynchronizationService_Outbound batch = new ba_SynchronizationService_Outbound();
					Database.executeBatch(batch, 200);
				}
	   		}   		   		
   		}
   	}
   	   	   	
   	//Model classes for the requests and responses. To be used by the JSON serialize/deserialize
   	public class SynchronizationServiceRequest{
		public List<Sync_Notification__c> notifications {get;set;}
	}
	
	public class SynchronizationServiceResponse{
		public List<Sync_Notification__c> notifications {get;set;}
		public String recordInformation {get; set;}
	}
}