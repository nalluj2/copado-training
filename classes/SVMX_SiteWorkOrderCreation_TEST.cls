@isTest private class SVMX_SiteWorkOrderCreation_TEST {
    //----------------------------------------------------------------------------------------
    // CREATE TEST DATA
    //----------------------------------------------------------------------------------------
    @testSetup static void createTestData() {

        // Create Custom Setting Data
        clsTestData.createCustomSettingData(true);

        // Create Account Data
        clsTestData.iRecord_Account = 1;
        clsTestData.tCountry_Account = 'United Kingdom';
        clsTestData.createAccountData(false);
        clsTestData.oMain_Account.SAP_Id__c = 'SAPA001';
        insert clsTestData.oMain_Account;

        // Create SVMXC__Service_Order__c Data
        clsTestData.createSVMXCServiceOrderData(false);
        clsTestData.oMain_SVMXCServiceOrder.SVMX_SAP_Service_Order_No__c = 'T12345T';
        clsTestData.oMain_SVMXCServiceOrder.SVMX_Send_Back_to_Service_Center__c = true;
        insert clsTestData.oMain_SVMXCServiceOrder;

        // Create SVMXC__Site__c Data
        clsTestData.iRecord_SVMXCSite = 2;
        clsTestData.createSVMXCSite(true);

        // Create AdHoc related Data
        // Service Order
        clsTestData.oMain_SVMXCServiceOrder = null;
        clsTestData.createSVMXCServiceOrderData(false);
        clsTestData.oMain_SVMXCServiceOrder.SVMXC__Purpose_of_Visit__c = 'Placeholder';
        clsTestData.oMain_SVMXCServiceOrder.SVMX_SAP_Service_Order_No__c = null;
        insert clsTestData.oMain_SVMXCServiceOrder;
    }
    //----------------------------------------------------------------------------------------
    @isTest static void runTest() {
  
        // Select Data
        SVMXC__Service_Order__c wo = [SELECT Id, SVMXC__Order_Status__c FROM SVMXC__Service_Order__c LIMIT 1];

        Test.startTest();

        PageReference pageRef = Page.SVMX_SiteWorkOrderCreation; 
        Test.setCurrentPage(pageRef);

        // Add parameters to page URL
        ApexPages.currentPage().getParameters().put('id', wo.Id);

        SVMX_SiteWorkOrderCreation controller = new SVMX_SiteWorkOrderCreation(new ApexPages.StandardController(wo));

        System.debug('controller.relatedWorkOrders.size: ' + controller.relatedWorkOrders.size());
        controller.onClickSortBy();

        controller.applyChangesAndClose();

        Test.stopTest();
    }
}