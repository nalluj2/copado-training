@isTest 
private class TEST_ctrl_CallRecordingOverview {

	@testSetup static void createTestData() {

		//---------------------------------------------
		// Create Test Data
		//---------------------------------------------
		clsTestData_MasterData.createSubBusinessUnit();
		clsTestData_MasterData.createUserBusinessUnit();

		List<Account> lstAccount = clsTestData_Account.createAccount(false);
		for (Account oAccount : lstAccount){
			oAccount.Country_Region__c = 'Europe';
		}
		insert lstAccount;
		clsTestData_Contact.createContact();

		List<Sub_Business_Units__c> lstSBU = [SELECT Id, Business_Unit__c, Business_Unit__r.Business_Unit_Group__c FROM Sub_Business_Units__c];

		List<Call_Recording_Overview__c> lstCallRecordingOverview = new List<Call_Recording_Overview__c>();
		Integer iCounterSBU = 0;
		for (Sub_Business_Units__c oSBU : lstSBU){

			if (iCounterSBU ==0){
				iCounterSBU++;
				continue;
			}
			for (Integer iCounter = 0; iCounter < 6; iCounter++){

				Call_Recording_Overview__c oCallRecordingOverview_Account = new Call_Recording_Overview__c();
					oCallRecordingOverview_Account.Account__c = clsTestData_Account.oMain_Account.Id;
					if (iCounter < 2){
						oCallRecordingOverview_Account.Sub_Business_Unit__c = oSBU.Id;
					}else if (iCounter < 4){
						oCallRecordingOverview_Account.Business_Unit__c = oSBU.Business_Unit__c;
					}else if (iCounter < 6){
						oCallRecordingOverview_Account.Business_Unit_Group__c = oSBU.Business_Unit__r.Business_Unit_Group__c;
					}
					if (iCounter == 0){
						oCallRecordingOverview_Account.Last_Call_Date__c = null;
					}else{
						oCallRecordingOverview_Account.Last_Call_Date__c = Date.today().addDays(-30 * iCounter);
					}
					if (iCounter == 0){
						oCallRecordingOverview_Account.Number_Of_Calls__c = null;
					}else{
						oCallRecordingOverview_Account.Number_Of_Calls__c = iCounter * 5;
					}
				lstCallRecordingOverview.add(oCallRecordingOverview_Account);

				Call_Recording_Overview__c oCallRecordingOverview_Contact = new Call_Recording_Overview__c();
					oCallRecordingOverview_Contact.Contact__c = clsTestData_Contact.oMain_Contact.Id;
					if (iCounter < 2){
						oCallRecordingOverview_Account.Sub_Business_Unit__c = oSBU.Id;
					}else if (iCounter < 4){
						oCallRecordingOverview_Account.Business_Unit__c = oSBU.Business_Unit__c;
					}else if (iCounter < 6){
						oCallRecordingOverview_Account.Business_Unit_Group__c = oSBU.Business_Unit__r.Business_Unit_Group__c;
					}
					if (iCounter == 0){
						oCallRecordingOverview_Account.Last_Call_Date__c = null;
					}else{
						oCallRecordingOverview_Account.Last_Call_Date__c = Date.today().addDays(-30 * iCounter);
					}
					if (iCounter == 0){
						oCallRecordingOverview_Account.Number_Of_Calls__c = null;
					}else{
						oCallRecordingOverview_Account.Number_Of_Calls__c = iCounter * 5;
					}
				lstCallRecordingOverview.add(oCallRecordingOverview_Contact);

			}
		
		}
		insert lstCallRecordingOverview;
		//---------------------------------------------

	}

	 @isTest static void TEST_ctrl_CallRecordingOverview() {

		//---------------------------------------------
		// Load Test Data
		//---------------------------------------------
		Account oAccount = [SELECT Id FROM Account LIMIT 1];
		Contact oContact = [SELECT Id FROM Contact LIMIT 1];

	 
		//---------------------------------------------
		// Test Logic
		//---------------------------------------------
		Test.startTest();

			ctrl_CallRecordingOverview oCTRL = new ctrl_CallRecordingOverview();


			ctrl_CallRecordingOverview.wData owData = new ctrl_CallRecordingOverview.wData();
			ctrl_CallRecordingOverview.wData_BUG owData_BUG = new ctrl_CallRecordingOverview.wData_BUG();
			ctrl_CallRecordingOverview.wData_BU owData_BU = new ctrl_CallRecordingOverview.wData_BU();
			ctrl_CallRecordingOverview.wData_SBU owData_SBU = new ctrl_CallRecordingOverview.wData_SBU();


			System.currentPageReference().getParameters().put('id', oAccount.Id);
			System.currentPageReference().getParameters().put('fullPage', '1');
			System.currentPageReference().getParameters().put('selectedView', 'ALL');
			oCTRL = new ctrl_CallRecordingOverview();
			executeTest(oCTRL);
			oCTRL.tSelectedView = 'PRIMARYSBUONLY';
			executeTest(oCTRL);
			oCTRL.tSelectedView = 'USERSBUONLY';
			executeTest(oCTRL);


			System.currentPageReference().getParameters().put('id', oContact.Id);
			System.currentPageReference().getParameters().put('fullPage', '1');
			System.currentPageReference().getParameters().put('selectedView', 'ALL');
			oCTRL = new ctrl_CallRecordingOverview();
			oCTRL.tSelectedView = 'PRIMARYSBUONLY';
			executeTest(oCTRL);
			oCTRL.tSelectedView = 'USERSBUONLY';
			executeTest(oCTRL);

		Test.stopTest();
		//---------------------------------------------

	 }

	 private static void executeTest(ctrl_CallRecordingOverview oCTRL){
	 
		Boolean bTest = false;
		List<SelectOption> lstSO;

		// Actions
		oCTRL.refreshData();
		oCTRL.redirectToMaster();


		// Getters & Setters
		bTest = oCTRL.bFullPage;
		bTest = oCTRL.bShowActions;
		bTest = oCTRL.bShowBUGData;
		bTest = oCTRL.bShowCallRecordingOverview;
		oCTRL.bShowColorMapping = true;
		bTest = oCTRL.bShowColorMapping;
		bTest = oCTRL.bShowSelectView;
		lstSO = oCTRL.lstSO_SelectView;
	 
	 }

}