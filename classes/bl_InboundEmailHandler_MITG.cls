//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Name    : bl_InboundEmailHandler_MITG 
// Author  : Bart Caelen
// Date    : 29/06/2017
// Purpose : Business Logic to handle inbound emails to create MITG Leads
// Dependencies: 
//
// ==================================================
// = MODIFICATION HISTORY 							=
// ==================================================
// DATE        	AUTHOR           	CHANGE
// 29/06/2017	Bart Caelen 		CR-14226 - Merged the logic in MITG_EmailToLeadHandler into bl_InboundEmailHandler_MITG.handleInboundEmail_ConnectMe
// 29/06/2017	Bart Caelen 		CR-14226 - Added new logic to process MITG AdobeConnect Emails in bl_InboundEmailHandler_MITG.handleInboundEmail_AdobeConnect
// 29/06/2017	Bart Caelen 		CR-14226 - Added new logic to process MITG ManualEmail Emails in bl_InboundEmailHandler_MITG.handleInboundEmail_ManualEmail
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
public with sharing class bl_InboundEmailHandler_MITG{

	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// Process incoming MITG ConnectMe emails with an Email address that starts with "mitg_email_to_lead@"
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public static Messaging.InboundEmailResult handleInboundEmail_ConnectMe(Messaging.inboundEmail oInboundEmail, Messaging.InboundEnvelope oInboundEnvelope){

		Messaging.InboundEmailResult oInboundEmailResult = new Messaging.InboundEmailresult();

		Savepoint oSavepoint = Database.setSavepoint();


		try{

			wHelper oHelper = new wHelper();
			oHelper.tMessage += ' $$ handleInboundEmail_ConnectMe';


			// Get the FROM Email Address
			Set<String> setFromAddress = new Set<String>();
			setFromAddress.add(oInboundEmail.fromAddress);


			// Get the Owner ID
			oHelper.tMessage += ' $$ handleInboundEmail_ConnectMe - getUserId : ';
			oHelper.id_Owner = getUserId(oHelper, oInboundEmail.fromAddress);


			// Create New Lead based on email
			oHelper.tMessage += ' $$ handleInboundEmail_ConnectMe - createLead : ';
			createLead(oHelper, oInboundEmail, setFromAddress, 'Connect.me', 'Inbound Email', 'FROM', true);


			// Save Attachments for Lead
			oHelper.tMessage += ' $$ handleInboundEmail_ConnectMe - Attachment : ';
			if (oHelper.setID_Lead.size() > 0 ){
				saveAttachment(oHelper, oInboundEmail, oHelper.setID_Lead, null, true);
			}


			// Update Onwer of the Lead, Task and / or Attachments 
			oHelper.tMessage += ' $$ handleInboundEmail_ConnectMe - updateOwner : ';
			updateOwner(oHelper);


			oInboundEmailResult.success = oHelper.bSuccess;

		}catch(Exception oEX){
            oInboundEmailResult.success = false;
            oInboundEmailResult.message = 'bl_InboundEmailHandler_MITG.handleInboundEmail_ConnectMe failed : ' + oEX.getStackTraceString() + ' (' + oEX.getMessage() + ')';
            Database.Rollback(oSavepoint);
		}

		return oInboundEmailResult;

	}
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------



	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// Process incoming MITG AdobeConnect emails with an Email address that starts with "mitg_email_to_lead@"
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public static Messaging.InboundEmailResult handleInboundEmail_AdobeConnect(Messaging.inboundEmail oInboundEmail, Messaging.InboundEnvelope oInboundEnvelope){

		Messaging.InboundEmailResult oInboundEmailResult = new Messaging.InboundEmailresult();

		wHelper oHelper = processInboundEmail(oInboundEmail, 'Adobe Connect Invite', 'Connect.me Rep');

		oInboundEmailResult.success = oHelper.bSuccess;
		if (oHelper.bSuccess == false){
			oInboundEmailResult.message = oHelper.tMessage;
		}

		return oInboundEmailResult;

	}
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------



	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// Process incoming MITG ManualEmails emails with an Email address that starts with "mitg_email_to_lead@"
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public static Messaging.InboundEmailResult handleInboundEmail_ManualEmail(Messaging.inboundEmail oInboundEmail, Messaging.InboundEnvelope oInboundEnvelope){


		Messaging.InboundEmailResult oInboundEmailResult = new Messaging.InboundEmailresult();

		wHelper oHelper = processInboundEmail(oInboundEmail, 'Outgoing Email', 'Connect.me Rep');

		oInboundEmailResult.success = oHelper.bSuccess;
		if (oHelper.bSuccess == false){
			oInboundEmailResult.message = oHelper.tMessage;
		}

		return oInboundEmailResult;

	}
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------



	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// Shared logic to process incoming MITG ConnectMe and MITG ManualEmails emails with an Email address that starts with "mitg_email_to_lead@"
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	private static wHelper processInboundEmail(Messaging.inboundEmail oInboundEmail, String tLeadSourceType, String tLeadTypeMITG){

		wHelper oHelper = new wHelper();

		Savepoint oSavepoint = Database.setSavepoint();

		try{

			clsUtil.bubbleException();

			if (oInboundEmail.toAddresses == null){
				oHelper.bSuccess = true;
				return oHelper;
			}

			// Get the Owner ID
			oHelper.tMessage += ' $$ processInboundEmail (' + tLeadSourceType + ') - getUserId : ';
			getUserId(oHelper, oInboundEmail.fromAddress);


			// Get the different TO Addresses from the email
			Set<String> setToAddress_All = new Set<String>();
				setToAddress_All.addAll(oInboundEmail.toAddresses);

			// Check for each collected TO Address if there is a contact
			Map<String, Contact> mapToAddress_Contact = getContactWithEmail(setToAddress_All);
				oHelper.lstContact = mapToAddress_Contact.values();

			// For the TO Addresses that don't have a Contact in SFDC, we need to create a new lead
			Set<String> setToAddress_Lead = new Set<String>();
			Set<Id> setID_Contact = new Set<Id>();
			for (String tToAddress : setToAddress_All){

				if (mapToAddress_Contact.containsKey(tToAddress)){
					oHelper.setID_Contact.add(mapToAddress_Contact.get(tToAddress).Id);
				}else{
					setToAddress_Lead.add(tToAddress);
				}

			}

			// Create new Lead(s)
			if (setToAddress_Lead.size() > 0){

				oHelper.tMessage += ' $$ processInboundEmail (' + tLeadSourceType + ') - createLead : ';
				createLead(oHelper, oInboundEmail, setToAddress_Lead, tLeadSourceType, tLeadTypeMITG, 'TO', false);

			}


			// Create a new Task related to the found Contact or created Lead
			oHelper.tMessage += ' $$ processInboundEmail (' + tLeadSourceType + ') - createTask : ';
			createTask(oHelper, oInboundEmail, tLeadSourceType, true);
			

			// Save Attachments for Lead, Contact and Task
			if (oHelper.setID_Lead.size() > 0 ){
				oHelper.tMessage += ' $$ processInboundEmail (' + tLeadSourceType + ') - saveAttachment (Lead) : ';
				saveAttachment(oHelper, oInboundEmail, oHelper.setID_Lead, null, false);
			}
			if (oHelper.setID_Contact.size() > 0 ){
				oHelper.tMessage += ' $$ processInboundEmail (' + tLeadSourceType + ') - saveAttachment (Contact) : ';
				saveAttachment(oHelper, oInboundEmail, oHelper.setID_Contact, null, false);
			}
			if (oHelper.setID_Task.size() > 0 ){
				oHelper.tMessage += ' $$ processInboundEmail (' + tLeadSourceType + ') - saveAttachment (Task) : ';
				saveAttachment(oHelper, oInboundEmail, oHelper.setID_Task, null, false);
			}

			if (oHelper.lstAttachment.size() > 0){
				insert oHelper.lstAttachment;
			}


			// Update Onwer of the Lead, Task and / or Attachments 
			oHelper.tMessage += ' $$ processInboundEmail (' + tLeadSourceType + ') - updateOwner : ';
			updateOwner(oHelper);


			oHelper.bSuccess = true;

		}catch(Exception oEX){
			oHelper.bSuccess = false;
            oHelper.tMessage += 'bl_InboundEmailHandler_MITG.processInboundEmail for "' + tLeadSourceType + '" failed : ' + oEX.getStackTraceString() + ' (' + oEX.getMessage() + ')';
            Database.Rollback(oSavepoint);
		}

		return oHelper;

	}
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------



	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// Get Contact based on Emeil address
	//	If we find multiple Contacts, we use the first contact
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	private static Map<String, Contact> getContactWithEmail(Set<String> setToAddress){

		Map<String, Contact> mapToAddress_Contact = new Map<String, Contact>();

		List<Contact> lstContact = [SELECT Id, Name, Email FROM Contact WHERE Email = :setToAddress ORDER BY Email];
		for (Contact oContact : lstContact){
			if (!mapToAddress_Contact.containsKey(oContact.Email)){
				mapToAddress_Contact.put(oContact.Email, oContact);
			}
		}

		return mapToAddress_Contact;

	}
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------



	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// Get the Id of the UserId based on an email 
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	private static Id getUserId(wHelper oHelper, String tEmail){

		String tMessage = '';	// For debugging purpose

		// Get the owner of the records based on the From Email Address of the incoming Email - if no user is found we use the actual user
		List<User> lstUser = [SELECT Id FROM User WHERE IsActive = true and Email = :tEmail LIMIT 1];

		Id id_Owner = UserInfo.getUserId();
		if (lstUser.size() == 1){
			id_Owner = lstUser[0].Id;
		}

		oHelper.id_Owner = id_Owner;
		oHelper.tMessage += tMessage;
		return id_Owner;

	}
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------



	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// Creata a MITG lead based on the incoming Email and the provided Lead Source Type.
	//	If bLinkContact = true, we will also search for availabel contacts based on the TO / FROM Email Address of the incoming email and link these Contacts to the created Lead.
	//	We will also use the first found Contact to populate data on the Lead.
	//	All attachments of the incoming email and the incoming email itself, will be saved as attachments to the created Lead.
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	private static wHelper createLead(wHelper oHelper, Messaging.inboundEmail oInboundEmail, Set<String> setEmailAddress, String tLeadSourceType, String tLeadTypeMITG, String tEmailType, Boolean bLinkContact){

		String tMessage = '';	// For debugging purpose

		Map<String, Lead> mapEmail_Lead = new Map<String, Lead>();

		String tSubject = clsUtil.isNull(oInboundEmail.subject, '');
		String tEmailBody = clsUtil.isNull(oInboundEmail.htmlBody, clsUtil.isNull(oInboundEmail.plainTextBody, ''));
		Id id_RecordType_Lead = clsUtil.getRecordTypeByDevName('Lead', 'MITG_Lead').Id;

		for (String tEmail : setEmailAddress){

			Lead oLead = new Lead();

				oLead.Email = tEmail;
				oLead.RecordTypeId = id_RecordType_Lead;

				// Set Required fields on lead with default values
				oLead.Company = 'UNKNOWN';
				oLead.LastName = 'UNKNOWN';
				oLead.Country = 'UNKNOWN';

			    try{ oLead.Company = oLead.Email.split('\\@')[1].split('\\.')[0].capitalize(); }catch(Exception oEX){}

			    oLead.Lead_Type_MITG__c = tLeadTypeMITG;
			    oLead.Lead_Source_MITG__c = tLeadSourceType;
			    oLead.Status = 'Open';
			    oLead.Connected_As__c = 'Other';

				oLead.Product_Solution_of_Interest__c = tSubject;

			    if (tEmailType == 'FROM'){

			    	// Retrieve the FirstName and LastName from the FROM name of the Email
					try{ oLead.FirstName = oInboundEmail.fromName.subString(0 ,oInboundEmail.fromName.indexOf(' ')); }catch(Exception oEX){}
				    try{ oLead.LastName = oInboundEmail.fromName.subString(oInboundEmail.fromName.indexOf(' ')); }catch(Exception oEX){}

				}else if (tEmailType == 'TO'){

			    	// Retrieve the FirstName and LastName from the EMAIL address in the TO of the Email
			    	try{
					    List<String> lstNamePart = new List<String>();
					    lstNamePart = tEmail.split('\\@')[0].split('\\.');
					    if (lstNamePart.size() > 0){
						    if (lstNamePart.size() == 1){
								oLead.LastName = lstNamePart[0];
						    }else{
						    	oLead.FirstName = lstNamePart[0];
						    	oLead.LastName = '';
						    	for (Integer i=1; i<lstNamePart.size(); i++){
							    	oLead.LastName += lstNamePart[i] + ' ';
						    	}
						    }
						    oLead.LastName = oLead.LastName.trim();
						}
					}catch(Exception oEX){}

					if (String.isBlank(oLead.LastName)) oLead.LastName = 'UNKNOWN';

				}

			mapEmail_Lead.put(tEmail, oLead);

		}

		Map<String, List<Contact>> mapEmail_Contacts = new Map<String, List<Contact>>();
		if (bLinkContact){
			// Find all Contacts with an email address equal to the From Email Address of the processing Email.
			// Populate fields on the Lead with the data of the found Contact.
			// If more Contacts are found, we use the most recent Contact.
			// If no Contact is found, we use some default values for the Lead fields.
			// All found Contacts will be linked to this new Lead and become visible in the related list on lead.  
			// The Contact that is used to populate this lead will be indicated.
	        Map<String, String> mapLeadField_AccountContactField = new Map<String, String>();
	            mapLeadField_AccountContactField.put('Company', 'Account.Name');
	            mapLeadField_AccountContactField.put('FirstName', 'FirstName');
	            mapLeadField_AccountContactField.put('LastName', 'LastName');
	            mapLeadField_AccountContactField.put('Salutation_Text__c', 'Salutation_Text__c');
	            mapLeadField_AccountContactField.put('Title', 'Title');
	            mapLeadField_AccountContactField.put('Lead_Specialty__c', 'Contact_Primary_Specialty__c');
	            mapLeadField_AccountContactField.put('Street', 'MailingStreet');
	            mapLeadField_AccountContactField.put('PostalCode', 'MailingPostalCode');
	            mapLeadField_AccountContactField.put('City', 'MailingCity');
	            mapLeadField_AccountContactField.put('State', 'MailingState');
	            mapLeadField_AccountContactField.put('Country', 'MailingCountry');
	            mapLeadField_AccountContactField.put('MobilePhone', 'MobilePhone');
	            mapLeadField_AccountContactField.put('Phone', 'Phone');
	            mapLeadField_AccountContactField.put('Website', 'Account.Website');
			mapEmail_Contacts = bl_Lead.findContactAndPopulateLead(mapEmail_Lead, mapLeadField_AccountContactField);
		}

		// Insert the Lead(s) - execute default assignemtn rule
		if (mapEmail_Lead.size() > 0){

			for (Lead oLead : mapEmail_Lead.values()){
				Database.DMLOptions oDMLOptions = new Database.DMLOptions();
					oDMLOptions.assignmentRuleHeader.useDefaultRule = true;
				oLead.setOptions(oDMLOptions);
			}

			insert mapEmail_Lead.values();
		}


		// Insert the Lead_Contact__c records (link the Contacts with the same email address to the lead)
		if (mapEmail_Contacts.size() > 0){

			List<Lead_Contact__c> lstLeadContact = new List<Lead_Contact__c>();
			for (String tEmail : mapEmail_Contacts.keySet()){

				List<Contact> lstContact = mapEmail_Contacts.get(tEmail);

				if (!mapEmail_Lead.containsKey(tEmail)) continue;
				Lead oLead = mapEmail_Lead.get(tEmail);

				Integer iCounter = 0;
				for (Contact oContact : lstContact){

					Lead_Contact__c oLeadContact = new Lead_Contact__c();
						oLeadContact.Lead__c = oLead.Id;
						oLeadContact.Contact__c = oContact.Id;
						if (iCounter == 0){
							oLeadContact.Used_on_Lead__c = true;
						}else{
							oLeadContact.Used_on_Lead__c = false;
						}
					lstLeadContact.add(oLeadContact);
					iCounter++;
				}

			}

			if (lstLeadContact.size() > 0){
				insert lstLeadContact;
			}

		}

		// Save all Attachments of the incoming email and the incoming email itself as Attachments to the lead
		Set<Id> setID_Lead = new Set<Id>();
		for (Lead oLead : mapEmail_Lead.values()){
			setID_Lead.add(oLead.Id);
		}

		oHelper.lstLead = mapEmail_Lead.values();
		oHelper.setID_Lead = setID_Lead;
		oHelper.tMessage += tMessage;
		oHelper.bSuccess = true;

		return oHelper;

	}
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------



	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// Create a Task related to the found Contact or created Lead
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	private static wHelper createTask(wHelper oHelper, Messaging.inboundEmail oInboundEmail, String tEmailType, Boolean bInsert){

		String tMessage = '';	// For debugging purpose

		Set<Id> setID_All = new Set<Id>();
		setID_All.addAll(oHelper.setID_Lead);
		setID_All.addAll(oHelper.setID_Contact);

		List<Task> lstTask = new List<Task>();
		for (Id id_Record : setID_All){

			Task oTask = new Task();

				oTask.WhoId = id_Record;
				oTask.Subject = clsUtil.isNull(oInboundEmail.subject, 'No Subject').left(255);
				oTask.Description = clsUtil.isNull(oInboundEmail.plainTextBody, clsUtil.isNull(oInboundEmail.htmlBody, 'No Email Body')).left(32000);
				oTask.Status = 'Completed';
				oTask.Priority = 'Low';
				oTask.IsReminderSet = false;
				oTask.ActivityDate = Date.today();
				oTask.RecordTypeId = clsUtil.getRecordTypeByDevName('Task', 'Content_Sharing').Id;
				oTask.Email_Type__c = tEmailType;

			lstTask.add(oTask);

		}

		Set<Id> setID_Task = new Set<Id>();
		if (bInsert && (lstTask.size() > 0 )){
			insert lstTask;
			for (Task oTask : lstTask){
				setID_Task.add(oTask.Id);
			}
		}

		oHelper.lstTask = lstTask;
		oHelper.setID_Task = setID_Task;
		oHelper.tMessage += tMessage;

		return oHelper;

	}
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------



	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// Update the Owner of the Task and Attachments related to the Task
	// Setting this owner in the insert generates an error "INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY"
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	private static wHelper updateOwner(wHelper oHelper){

		String tMessage = '';	// For debugging purpose

		if ( (oHelper.id_Owner == null) || (oHelper.id_Owner == UserInfo.getUserId() ) ){
	
			List<QueueSobject> lstQueueSobject = [SELECT QueueId FROM QueueSobject WHERE Queue.DeveloperName = 'MITG_Connect_me_general' LIMIT 1];

			if (lstQueueSobject.size() == 1){
			
				for (Lead oLead : oHelper.lstLead){
					oLead.OwnerId = lstQueueSobject[0].QueueId;
				}
			
			}

		}else{

			if (oHelper.lstLead.size() > 0){
				for (Lead oLead : oHelper.lstLead){
					oLead.OwnerId = oHelper.id_Owner;
				}
			}

			if (oHelper.lstTask.size() > 0){
				for (Task oTask : oHelper.lstTask){
					oTask.OwnerId = oHelper.id_Owner;
				}
			}

			if (oHelper.lstAttachment.size() > 0){
				for (Attachment oAttachment : oHelper.lstAttachment){
					oAttachment.OwnerId = oHelper.id_Owner;
				}
			}

		}

		// Update the Owner of the Task and Attachments (setting this owner in the insert generates an error "INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY")
		try{

			if (oHelper.lstLead.size() > 0 ) update oHelper.lstLead;
			if (oHelper.lstTask.size() > 0 ) update oHelper.lstTask;
			if (oHelper.lstAttachment.size() > 0 ) update oHelper.lstAttachment;

		}catch(Exception oEX){
			tMessage += 'Error in bl_InboundEmailHandler_MITG.updateOwner while updating the owner of the Lead, Task and/or related Attachments : ' + oEX.getMessage();
			System.debug(tMessage);
		}

		oHelper.tMessage += tMessage;

		return oHelper;

	}
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------



	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// Save all Attachments of the incoming email and the incoming email itselfs as attachments related to the provided record Id
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	private static wHelper saveAttachment(wHelper oHelper, Messaging.inboundEmail oInboundEmail, Set<Id> setID, Id id_Owner, Boolean bInsert){

		String tMessage = '';	// For debugging purpose

		List<Attachment> lstAttachment = new List<Attachment>();

		// Text Attachment(s)
		if (oInboundEmail.textAttachments != null){

			for (Messaging.Inboundemail.TextAttachment oAttachment_Text : oInboundEmail.textAttachments) {

				for (Id id_Record : setID){

					Attachment oAttachment = new Attachment();
						oAttachment.ParentId = id_Record;
						oAttachment.Name = oAttachment_Text.fileName;
						oAttachment.Body = Blob.valueOf(oAttachment_Text.body);
					lstAttachment.Add(oAttachment);

				}
	
			}

		}

		// Binary Attachment(s)
		if (oInboundEmail.binaryAttachments != null){

			for (Messaging.Inboundemail.BinaryAttachment oAttachment_Binary : oInboundEmail.binaryAttachments) {

				for (Id id_Record : setID){

					Attachment oAttachment = new Attachment();
						oAttachment.ParentId = id_Record;
						oAttachment.Name = oAttachment_Binary.fileName;
						oAttachment.Body = oAttachment_Binary.body;
					lstAttachment.add(oAttachment);

				}

			}

		}

		// Email Message Attachment
		String tEmailBody = clsUtil.isNull(oInboundEmail.htmlBody, clsUtil.isNull(oInboundEmail.plainTextBody, ''));
		if (tEmailBody != ''){

			for (Id id_Record : setID){

				Attachment oAttachment_Email = new Attachment();
					oAttachment_Email.ParentId = id_Record;
					oAttachment_Email.Name = clsUtil.isNull(oInboundEmail.subject, 'email') + '.html';
					oAttachment_Email.Body = Blob.valueOf(tEmailBody);
				lstAttachment.add(oAttachment_Email);

			}

		}

		if ( bInsert && (lstAttachment.size() > 0) ){
			insert lstAttachment;
		}

		oHelper.lstAttachment.addAll(lstAttachment);
		oHelper.tMessage += tMessage;

		return oHelper;

	}
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------



	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// Helper Class
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	private class wHelper{

		public wHelper(){
			bSuccess = false;
			tMessage = '';
			lstLead = new List<Lead>(); 
			setID_Lead = new Set<Id>();
			lstContact = new List<Contact>();
			setID_Contact = new Set<Id>();
			lstTask = new List<Task>();
			setID_Task = new Set<Id>();
			lstAttachment = new List<Attachment>();
			id_Owner = null;
		}

		public Boolean bSuccess;
		public String tMessage;
		public List<Lead> lstLead;
		public Set<Id> setID_Lead;
		public List<Contact> lstContact;
		public Set<Id> setID_Contact;
		public List<Task> lstTask;
		public Set<Id> setID_Task;
		public List<Attachment> lstAttachment;
		public Id id_Owner;

	}
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

}
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------