public class ProfileMedtronic {

	private static Map<String, Id> profilesByName{
		
		get{
			
			if(profilesByName == null){
				
				profilesByName = new Map<String, Id>();
				
				for(Profile profile : [Select Id, Name from Profile where Name IN(
														'System Administrator MDT', 
														'SAP Interface', 
														'Devart Tool',
														'Devart Tool - Digic',
														'Devart Account Interface', 
														'IT Business Analyst', 
														'IT Business Analyst (Read-only)', 
														'IT Business Analyst - Mobile Team', 
														'CVent', 
														'MMX Interface')]){
					
					profilesByName.put(profile.Name, profile.Id);
				}
			}	
			
			return profilesByName;
		}
		
		set;
	}

	private static SystemAdministratorProfileId__c ids{
		 
		 get{
		 	
		 	if(ids == null) ids = SystemAdministratorProfileId__c.getInstance('Default');
		 	
		 	return ids;
		 }
		 
		 set;
	}

	public static boolean isSystemAdministratorProfile(ID profileId){
		
		Id systemAdmin = ids != null ? ids.SystemAdministrator__c : null;
		Id systemAdminMDT = profilesByName.get('System Administrator MDT');
		
		return profileId == systemAdmin || profileId == systemAdminMDT;
	}
	
	public static boolean isCVentProfile(ID profileId){
		
		Id cvent = profilesByName.get('CVent');

		return profileId == cvent;
	}
	
	public static boolean isDevartProfile(ID profileId){
		
		Id devart = profilesByName.get('Devart Tool');
		Id devartDigic = profilesByName.get('Devart Tool - Digic');
		
		return profileId == devart || profileId == devartDigic;
	}
	
	public static boolean isBusinessAnalystProfile(ID profileId){
		
		Id ITBusinessAnalyst = profilesByName.get('IT Business Analyst');
		Id ITBusinessAnalystRO = profilesByName.get('IT Business Analyst (Read-only)');
		Id ITBusinessAnalystMobile = profilesByName.get('IT Business Analyst - Mobile Team');

		return profileId == ITBusinessAnalyst || profileId == ITBusinessAnalystRO || profileId == ITBusinessAnalystMobile;
	}
	
	public static boolean isSAPInterface(ID profileId){
		
		Id SAPInterface = profilesByName.get('SAP Interface');
		Id DevartAccountInterface = profilesByName.get('Devart Account Interface');
		
		return profileId == SAPInterface || profileId == DevartAccountInterface;
	}

	public static boolean isMMXInterface(ID profileID){
		
		Id MMXInterface = profilesByName.get('MMX Interface');

		return profileId == MMXInterface;
	}
}