//----------------------------------------------------------------------------------------
// Name    : TEST_bl_Lead 
// Author  : Bart Caelen
// Date    : 20/01/2017
// Purpose : APEX Test Class for APEX Class bl_Lead
// Dependencies: 
//
// ========================
// = MODIFICATION HISTORY =
// ========================
// DATE        AUTHOR           CHANGE
//
//----------------------------------------------------------------------------------------
@isTest private class TEST_bl_Lead {
	
	private static Integer iContactRecord = 5;
	private static List<Contact> lstContact;
	private static String tEmailAddress = 'test@medtronic.com.test';

	@isTest static void createTestData(){

		// Create Contact Data - all with the same email address
		clsTestData_Contact.iRecord_Contact = iContactRecord;
		lstContact = clsTestData_Contact.createContact(false);
		for (Contact oContact : lstContact){
			oContact.Email = tEmailAddress;
			oContact.MailingStreet = 'Mailing Street Data';
			oContact.MailingPostalCode = 'Mailing Zip';
			oContact.MailingCity = 'Mailing City Data';
			oContact.MailingState = 'Mailing State Data';
			oContact.MailingCountry = 'Belgium';
		}
		insert lstContact;

	}	


	@isTest static void test_findContactAndPopulateLead(){

		// Create Test Data
		createTestData();

		// Create Lead
		Lead oLead = new Lead();

		// Create Mapping between Lead Field and Account/Contact Field
        Map<String, String> mapLeadField_AccountContactField = new Map<String, String>();
            mapLeadField_AccountContactField.put('Company', 'Account.Name');
            mapLeadField_AccountContactField.put('FirstName', 'FirstName');
            mapLeadField_AccountContactField.put('LastName', 'LastName');
            mapLeadField_AccountContactField.put('Street', 'MailingStreet');
            mapLeadField_AccountContactField.put('PostalCode', 'MailingPostalCode');
            mapLeadField_AccountContactField.put('City', 'MailingCity');
            mapLeadField_AccountContactField.put('State', 'MailingState');
            mapLeadField_AccountContactField.put('Country', 'MailingCountry');

		Map<String, Lead> mapEmail_Lead = new Map<String, Lead>();
       		mapEmail_Lead.put(tEmailAddress, oLead);

        Test.startTest();    	

		Map<String, List<Contact>> mapEmail_Contacts = bl_Lead.findContactAndPopulateLead(mapEmail_Lead, mapLeadField_AccountContactField);
		lstContact = mapEmail_Contacts.get(tEmailAddress);

		Test.stopTest();

		System.assertEquals(lstContact.size(), iContactRecord);
		System.assertEquals(oLead.Company, lstContact[0].Account.Name);
		System.assertEquals(oLead.FirstName, lstContact[0].FirstName);
		System.assertEquals(oLead.LastName, lstContact[0].LastName);
		System.assertEquals(oLead.Street, lstContact[0].MailingStreet);
		System.assertEquals(oLead.PostalCode, lstContact[0].MailingPostalCode);
		System.assertEquals(oLead.City, lstContact[0].MailingCity);
		System.assertEquals(oLead.State, lstContact[0].MailingState);
		System.assertEquals(oLead.Country, lstContact[0].MailingCountry);

	}
	
}