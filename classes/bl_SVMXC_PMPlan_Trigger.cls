//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 05-01-2016
//  Description      : APEX Class - Business Logic for tr_SVMXC_PMPlan	
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public class bl_SVMXC_PMPlan_Trigger {

	//------------------------------------------------------------------------------------------------
	// Public variables coming from the trigger
	//------------------------------------------------------------------------------------------------
	public static List<SVMXC__PM_Plan__c> lstTriggerNew = new List<SVMXC__PM_Plan__c>();
	public static Map<Id, SVMXC__PM_Plan__c> mapTriggerNew = new Map<Id, SVMXC__PM_Plan__c>();
	public static Map<Id, SVMXC__PM_Plan__c> mapTriggerOld = new Map<Id, SVMXC__PM_Plan__c>();
	//------------------------------------------------------------------------------------------------


	//------------------------------------------------------------------------------------------------
	// Update Account_Country__c on Service Order with the Country of the Account
	//------------------------------------------------------------------------------------------------
	public static void copyAccountCountry(String tAction){

		List<SVMXC__PM_Plan__c> lstPMPlan = new List<SVMXC__PM_Plan__c>();
		Set<Id> setID_Account = new Set<Id>();

		for (SVMXC__PM_Plan__c oData : lstTriggerNew){

    		if (tAction == 'INSERT'){

        		lstPMPlan.add(oData);
        		if (oData.SVMXC__Account__c != null){
    	    		setID_Account.add(oData.SVMXC__Account__c);
    	    	}

    		}else if (tAction == 'UPDATE'){

    			SVMXC__PM_Plan__c oData_Old = mapTriggerOld.get(oData.Id);

    			if (oData.SVMXC__Account__c != oData_Old.SVMXC__Account__c){
	        		lstPMPlan.add(oData);
	        		if (oData.SVMXC__Account__c != null){
    	    			setID_Account.add(oData.SVMXC__Account__c);
    	    		}
    			}

    		}

        }

        // Get the Account_Country_vs__c of the collected Account ID's
        Map<Id, Account> mapAccount = new Map<Id, Account>([SELECT Id, Account_Country_vs__c FROM Account WHERE Id = :setID_Account]);

        for (SVMXC__PM_Plan__c oPMPlan : lstPMPlan){

        	if (oPMPlan.SVMXC__Account__c == null){

        		// If the processing record doesn't have a related Account, clear the Account_Country__c on the processing record
        		oPMPlan.Account_Country__c = null;

        	}else if (mapAccount.containsKey(oPMPlan.SVMXC__Account__c)){

        		oPMPlan.Account_Country__c = mapAccount.get(oPMPlan.SVMXC__Account__c).Account_Country_vs__c;

        	}
        
        }

	}
	//------------------------------------------------------------------------------------------------
}
//--------------------------------------------------------------------------------------------------------------------