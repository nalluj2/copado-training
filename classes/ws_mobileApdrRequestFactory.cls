@isTest
public class ws_mobileApdrRequestFactory {

	public static Account_Performance__c createAccountPerformance(Account_Plan_2__c accountPlan){
		RestRequest req = new RestRequest();  
    	RestResponse res = new RestResponse();
    	
		String id=GuidUtil.NewGuid();

		Account_Performance__c accountPerformance = new Account_Performance__c();
		accountPerformance.Account_Plan_ID__c= accountPlan.id; 
		accountPerformance.mobile_id__c = id;
		
    	JSONGenerator gen = JSON.createGenerator(true);
    	
		gen.writeStartObject();
        gen.writeStringField('id', '12346');
	    gen.writeObjectField('accountPerformance',accountPerformance);
	   
        // Get the JSON string.
        String pretty = gen.getAsString();	    	
		System.debug('JSON '+pretty);

        req.requestbody = Blob.valueOf(pretty);
    	
     	req.requestURI = '/services/apexrest/AccountPerformanceService';  
	    req.httpMethod = 'POST';
    	RestContext.request = req;
    	RestContext.response = res;
		
		String resp = ws_AccountPerformanceService.doPost();
		System.debug('Response'+resp);
		
		return accountPerformance;
		
	}
	
	public static Account_Plan_2__c createAccountPlan(){
		RestRequest req = new RestRequest();  
    	RestResponse res = new RestResponse();
    	
		String id=GuidUtil.NewGuid();

		Account a = new Account();
		a.Name = 'test account';
		insert a;
	
		Account_Plan_2__c accountPlan = new Account_Plan_2__c();
		accountPlan.Account__c = a.id; 
		accountPlan.mobile_id__c = id;
		accountPlan.account_plan_level__c = 'Business Unit Group';
		accountPlan.Business_Unit_Group__c = [Select Id from Business_Unit_Group__c where Name = 'Restorative' AND Master_Data__r.NAme = 'Europe'].Id;
		
    	JSONGenerator gen = JSON.createGenerator(true);
    	
		gen.writeStartObject();
        gen.writeStringField('id', '12346');
	    gen.writeObjectField('accountPlan',accountPlan);
	   
        // Get the JSON string.
        String pretty = gen.getAsString();	    	
		System.debug('JSON '+pretty);

        req.requestbody = Blob.valueOf(pretty);
    	
     	req.requestURI = '/services/apexrest/AccountPlan2Service';  
	    req.httpMethod = 'POST';
    	RestContext.request = req;
    	RestContext.response = res;
		
		String resp = ws_AccountPlan2Service.doPost();
		System.debug('Response'+resp);
		ws_AccountPlan2Service.IFAccountPlan2Response responseObject = (ws_AccountPlan2Service.IFAccountPlan2Response)JSON.deserialize(resp, ws_AccountPlan2Service.IFAccountPlan2Response.class);
		Account_Plan_2__c ap = responseObject.accountPlan;
		System.debug('JSON Response'+JSON.serializePretty(responseObject));
		return ap;
		
	}
	
	public static Account_Performance_Load__c createAccountPerformanceLoad(){
		RestRequest req = new RestRequest();  
    	RestResponse res = new RestResponse();
    	
		String id=GuidUtil.NewGuid();

		Account a = new Account();
		a.Name = 'test account';
		insert a;

		Account_Performance_Load__c accountPerformanceLoad = new Account_Performance_Load__c();
		accountPerformanceLoad.Account__c = a.id;
		
    	JSONGenerator gen = JSON.createGenerator(true);
    	
		gen.writeStartObject();
        gen.writeStringField('id', '12346');
	    gen.writeObjectField('accountPerformanceLoad',accountPerformanceLoad);
	   
        // Get the JSON string.
        String pretty = gen.getAsString();	    	
		System.debug('JSON '+pretty);

        req.requestbody = Blob.valueOf(pretty);
    	
     	req.requestURI = '/services/apexrest/AccountPerformanceLoadService';  
	    req.httpMethod = 'POST';
    	RestContext.request = req;
    	RestContext.response = res;
		
		String resp = ws_AccountPerformanceLoadService.doPost();
		System.debug('Response'+resp);
		
		return accountPerformanceLoad;
		
	}

	public static Account_Plan_Objective__c createAccountPlanObjective(Account_Plan_2__c accountPlan){
		RestRequest req = new RestRequest();  
    	RestResponse res = new RestResponse();
    	
		String id=GuidUtil.NewGuid();

		Account_Plan_Objective__c accountPlanObjective = new Account_Plan_Objective__c();
		accountPlanObjective.Account_Plan__c = accountPlan.id;
		accountPlanObjective.mobile_id__c = id;
		
    	JSONGenerator gen = JSON.createGenerator(true);
    	
		gen.writeStartObject();
        gen.writeStringField('id', '12346');
	    gen.writeObjectField('accountPlanObjective',accountPlanObjective);
	   
        // Get the JSON string.
        String pretty = gen.getAsString();	    	
		System.debug('JSON '+pretty);

        req.requestbody = Blob.valueOf(pretty);
    	
     	req.requestURI = '/services/apexrest/AccountPlanObjectiveService';  
	    req.httpMethod = 'POST';
    	RestContext.request = req;
    	RestContext.response = res;
		
		String resp = ws_AccountPlanObjectiveService.doPost();
		System.debug('Response'+resp);
		
		return accountPlanObjective;
	}

	public static Account_Plan_Stakeholder__c createAccountPlanStakeholder(Account_Plan_2__c accountPlan){
		RestRequest req = new RestRequest();  
    	RestResponse res = new RestResponse();
    	
		String id=GuidUtil.NewGuid();

		Account_Plan_Stakeholder__c accountPlanStakeholder = new Account_Plan_Stakeholder__c();
		accountPlanStakeholder.Account_Plan__c = accountPlan.id; 
		accountPlanStakeholder.mobile_id__c = id;
		
    	JSONGenerator gen = JSON.createGenerator(true);
    	
		gen.writeStartObject();
        gen.writeStringField('id', '12346');
	    gen.writeObjectField('accountPlanStakeholder',accountPlanStakeholder);
	   
        // Get the JSON string.
        String pretty = gen.getAsString();	    	
		System.debug('JSON '+pretty);

        req.requestbody = Blob.valueOf(pretty);
    	
     	req.requestURI = '/services/apexrest/AccountPlanStakeholderService';  
	    req.httpMethod = 'POST';
    	RestContext.request = req;
    	RestContext.response = res;
		
		String resp = ws_AccountPlanStakeholderService.doPost();
		System.debug('Response'+resp);
		
		return accountPlanStakeholder;
	}

	public static Account_Plan_Team_Member__c createAccountPlanTeamMember(Account_Plan_2__c accountPlan){
		RestRequest req = new RestRequest();  
    	RestResponse res = new RestResponse();
    	
		String id=GuidUtil.NewGuid();

		Account_Plan_Team_Member__c accountPlanTeamMember = new Account_Plan_Team_Member__c();
		accountPlanTeamMember.Account_Plan__c = accountPlan.id; 
		accountPlanTeamMember.mobile_id__c = id;
		
    	JSONGenerator gen = JSON.createGenerator(true);
    	
		gen.writeStartObject();
        gen.writeStringField('id', '12346');
	    gen.writeObjectField('accountPlanTeamMember',accountPlanTeamMember);
	   
        // Get the JSON string.
        String pretty = gen.getAsString();	    	
		System.debug('JSON '+pretty);

        req.requestbody = Blob.valueOf(pretty);
    	
     	req.requestURI = '/services/apexrest/AccountPlanTeamMemberService';  
	    req.httpMethod = 'POST';
    	RestContext.request = req;
    	RestContext.response = res;
		
		String resp = ws_AccountPlanTeamMemberService.doPost();
		System.debug('Response'+resp);
		
		return accountPlanTeamMember;
	}

	public static Tender_Calendar_2__c createTenderCalendar2(Account_Plan_2__c accountPlan){
		RestRequest req = new RestRequest();  
    	RestResponse res = new RestResponse();
    	
		String id=GuidUtil.NewGuid();

		Tender_Calendar_2__c tenderCalendar = new Tender_Calendar_2__c();
		tenderCalendar.Account_Plan_ID__c = accountPlan.id;
		tenderCalendar.Mobile_ID__c = id;
		
    	JSONGenerator gen = JSON.createGenerator(true);
    	
		gen.writeStartObject();
        gen.writeStringField('id', '12346');
	    gen.writeObjectField('tenderCalendar2',tenderCalendar);
	   
        // Get the JSON string.
        String pretty = gen.getAsString();	    	
		System.debug('JSON '+pretty);

        req.requestbody = Blob.valueOf(pretty);
    	
     	req.requestURI = '/services/apexrest/ContactActionPlan2Service';  
	    req.httpMethod = 'POST';
    	RestContext.request = req;
    	RestContext.response = res;
				
		return tenderCalendar;
	}

	public static Task createTask(Account_Plan_2__c accountPlan){
		RestRequest req = new RestRequest();  
    	RestResponse res = new RestResponse();
    	
		String id=GuidUtil.NewGuid();

		Task task = new Task();
		task.WhatId = accountPlan.id;
		task.Mobile_ID__c = id;
		
    	JSONGenerator gen = JSON.createGenerator(true);
    	
		gen.writeStartObject();
        gen.writeStringField('id', '12346');
	    gen.writeObjectField('task',task);
	   
        // Get the JSON string.
        String pretty = gen.getAsString();	    	
		System.debug('JSON '+pretty);

        req.requestbody = Blob.valueOf(pretty);
    	
     	req.requestURI = '/services/apexrest/TaskService';  
	    req.httpMethod = 'POST';
    	RestContext.request = req;
    	RestContext.response = res;
		
		String resp = ws_Task.doPost(); 
		System.debug('Response'+resp);
		
		return task;
	}

}