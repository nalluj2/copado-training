global with sharing class ctrl_CVG_Opportunity_Template {
    
    public Opportunity opp {get; private set;}
    public OpportunityLineItem oppProd {get; private set;}
    public List<Opportunity_Growth_Driver__c> oppGrowths {get; private set;}
    
    public String oppId {
    	
    	get;
    	 
    	set{
    		if(value != null && value != ''){
    			
    			opp = [Select Id, Name, StageName, CloseDate, Amount, CurrencyISOCode, Sub_Business_Unit__c, CreatedBy.Name, Account.Name, Account_Plan__r.Name, 
    					(Select Product2.Name from OpportunityLineItems), 
    					(Select Growth_Driver_Description__c from Opportunity_Growth_Drivers__r) 
    					from Opportunity where Id = :value];
    			
    			if(opp.OpportunityLineItems.size() > 0)	oppProd = opp.OpportunityLineItems[0];    			
    			oppGrowths = opp.Opportunity_Growth_Drivers__r;
    		}	    		
    	}
    }     
}