/*
 *      Created Date : 6 -June-2013
 *      Description : This is the class is used to return the list of oma record types..
 *
 *      Author = Kaushal Singh
 *      
 */
public class bl_OMARecordTypes{

    public static set<string> OMARecordType(){
        set<String> setRTNames=new set<String>();
        List<OMA_Record_Types__c> lstOMART=[
                                            Select Name,Developer_Name__c 
                                            from OMA_Record_Types__c
                                           ];
        for(OMA_Record_Types__c OMRT:lstOMART){
            setRTNames.add(OMRT.Developer_Name__c);
        }
        return setRTNames;
    }
}