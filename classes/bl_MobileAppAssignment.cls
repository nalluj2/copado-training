/**
 * Creation Date:   20140220
 * Description:     Class that centralises all logic for Mobile App Assignments
 * Author:          Rudy De Coninck
 */

public class bl_MobileAppAssignment {

    static Map<Id,Mobile_App__c> mapApplications;

    static {
        mapApplications = new Map<Id,Mobile_App__c>{};
        init();      
    }
     
    public static void init(){
        List<Mobile_App__c> apps = [select Id, name from Mobile_App__c];
        for (Mobile_App__c ma : apps){
            mapApplications.put(ma.id, ma);
        }
        
    }

    public static void updateUserFromAssignment(List<Mobile_App_Assignment__c> appAssignments){
        
        List<User> usersToUpdate = new List<User>();
        for (Mobile_App_Assignment__c appAssignment : appAssignments){
            System.debug('Processing App assignment '+appAssignment);
            Mobile_app__c ma = mapApplications.get(appAssignment.Mobile_App__c);
            if(ma.name.equals('APDR')){
                usersToUpdate.add(new User(Id=appAssignment.User__c,mobile_apdr__c=appAssignment.Active__c));
            }
        }
        if (usersToUpdate.size()>0){
            update usersToUpdate;
        }
    }
    
    private static Mobile_App__c ma{
        get{
            
            if(ma == null) ma = [Select id from Mobile_App__c where name = 'Call Recording' limit 1];
            return ma;
        }
        set;
    }

    public static void addtoCallRecording(Map<Id,Mobile_App_Assignment__c> newMap) {
        list<Mobile_App_Assignment__c> insList = new list<Mobile_App_Assignment__c>();
        
        List<Id> userList = new List<Id>();
    
        // get the parent context Mobile_App__c 
        //Map<Id, Mobile_App__c> prntMap = new Map<Id, Mobile_App__c>([Select Id, Name  from Mobile_App__c]);
        
        // filter new map
        // on insert/update and mobile app is APDR or APDR for RTG
        // CR-16249 - added logic for the mobile app "DIB"
        for(Mobile_App_Assignment__c mRec : newMap.values()) {
            If ( (mapApplications.get(mRec.Mobile_App__c).Name == 'APDR' || mapApplications.get(mRec.Mobile_App__c).Name == 'APDR for RTG' || mapApplications.get(mRec.Mobile_App__c).Name == 'DIB') 
                && (Trigger.isInsert || Trigger.isUpdate)) {
                    userList.add(mRec.User__c);
            }
        }
        
        if(userList.size() > 0){
        
            Map<Id, User> userMap = new Map<Id, User>([Select Id, Username From User where Id = :userList]);                                                    
                                                                                             
            // Mobile_App_Assignment__c records with all inactive users assigned to a 'Call Recording' app
            List<Mobile_App_Assignment__c> inaList = new List<Mobile_App_Assignment__c>([Select Id, 
                                                                                                User__c,
                                                                                                User__r.Id, 
                                                                                                Mobile_App__r.Name 
                                                                                           from Mobile_App_Assignment__c 
                                                                                                where Mobile_App__r.Name = 'Call Recording'
                                                                                                and Active__c = false 
                                                                                                and User__r.Id = :userMap.keyset()]);       
        
            // activate users assigned to a 'Call Recording' app
            List<Mobile_App_Assignment__c> updList = new List<Mobile_App_Assignment__c>();
            for(Mobile_App_Assignment__c mas : inaList) {
                mas.Active__c = true;
                updList.add(mas);
            }
        
            // Mobile_App_Assignment__c records with all users assigned to a 'Call Recording' app       
            Map<Id, Mobile_App_Assignment__c> exiMap = new Map<Id, Mobile_App_Assignment__c>([Select Id, 
                                                                                                 User__c,
                                                                                                 User__r.Id, 
                                                                                                 Mobile_App__r.Name,
                                                                                                 Active__c 
                                                                                            from Mobile_App_Assignment__c 
                                                                                           where Mobile_App__r.Name = 'Call Recording' 
                                                                                             and User__r.Id = :userMap.keyset()]);

            // put all 'Call Recording' Users in a map
            map<Id,User> idCallRecMap = new map<Id,User>();
            for(Mobile_App_Assignment__c maa : exiMap.Values()) idCallRecMap.put(maa.User__r.Id, maa.User__r);
        
            // get the delta from both maps and register a 'Call Recording' record
            for(User uId : userMap.Values()) {
                if(!idCallRecMap.containsKey(uId.Id)) {
                    // create
                    inslist.add(new Mobile_App_Assignment__c(
                                Active__c = true,
                                Mobile_App__c = ma.Id,
                                User__c = uId.Id
                    )); 
                }
            }
        
            insert insList; 
            update updList;
        }
    }
    
    Public static void performCrossCheck(List<Mobile_App_Assignment__c> triggerNew) {
        
        Set<String> appMatricsSet= new Set<String>();
        Set<Id> appIdSet = new Set<Id>();
        Map<Id,Set<String>> appIncompMetricsMap = New Map<Id,Set<String>> ();
        Map<Id, String> appIdNameMap = New Map<Id, String> ();
        //map for user assignments (both old and new)
        Map<Id, Set<String>> userAllAppAssignsMap = New Map<Id, Set<String>> ();
        for(Mobile_App_Assignment__c newAppAssign: triggerNew){
            //Skip the logic if inactivating the assignments
            if(newAppAssign.Active__c){
                appIdSet.add(newAppAssign.Mobile_App__c);
                //User all Apps
                if(userAllAppAssignsMap.isEmpty() || !userAllAppAssignsMap.containsKey(newAppAssign.User__c))
                userAllAppAssignsMap.put(newAppAssign.User__c,New Set<String>{newAppAssign.Mobile_App__c});
                else if (userAllAppAssignsMap.containsKey(newAppAssign.User__c))
                userAllAppAssignsMap.get(newAppAssign.User__c).add(newAppAssign.Mobile_App__c);
                
            }
        }   
        
        if(userAllAppAssignsMap.isEmpty() || appIdSet.isEmpty())
        return;
        //Fetching the Mobile app records to identify the incompatibility app names
        for(Mobile_App__c app: [SELECT Id,App_Incompatibility_Metrics__c,Name FROM Mobile_App__c WHERE Id IN: appIdSet]){
            if(!String.isBlank(app.App_Incompatibility_Metrics__c)){
                appIncompMetricsMap.put(app.Id,New Set<String>(app.App_Incompatibility_Metrics__c.split(';')));
                appMatricsSet.addAll(New Set<String>(app.App_Incompatibility_Metrics__c.split(';')));
                appIdNameMap.put(app.Id,app.Name);
            }    
        }
        
        //If the app doesn't have incompatibility app skip the remaining logic 
        if(appIncompMetricsMap.isEmpty())
        return;
        
        //Fetching the user existing mobile app assignments        
        for(Mobile_App_Assignment__c assign : [Select Id, User__c,Mobile_App__c,Mobile_App__r.Name from Mobile_App_Assignment__c where 
                                                    User__c IN :userAllAppAssignsMap.keySet() AND Active__c = true AND Mobile_App__r.Name IN:appMatricsSet
                                                        AND Mobile_App__r.Active__c = true]){
            if(userAllAppAssignsMap.isEmpty() || !userAllAppAssignsMap.containsKey(assign.User__c))
            userAllAppAssignsMap.put(assign.User__c,New Set<String>{assign.Mobile_App__r.Name});
            else if (userAllAppAssignsMap.containsKey(assign.User__c))
            userAllAppAssignsMap.get(assign.User__c).add(assign.Mobile_App__r.Name);
        }    
        
        for(Mobile_App_Assignment__c mRec : triggerNew){
            //Check if user already has assignments or not and the mobile app has incompatibility matrcis
            if(!userAllAppAssignsMap.isEmpty() && userAllAppAssignsMap.containsKey(mRec.User__c) 
                        && !appIncompMetricsMap.isEmpty() && appIncompMetricsMap.containsKey(mRec.Mobile_App__c)){
                //dupCheckSet.addAll(userAssignmentMap.get(mRec.User__c));
                Set<String> dupCheckSet = New Set<String> (userAllAppAssignsMap.get(mRec.User__c));   
                //system.debug('dupCheckSet ........'+dupCheckSet);
                dupCheckSet.retainAll(appIncompMetricsMap.get(mRec.Mobile_App__c));
                
                if(dupCheckSet.size() > 0)
                mRec.adderror('This user can\'t be assigned to '+ appIdNameMap.get(mRec.Mobile_App__c) +', Because this user is already assigned or trying to assign to its incompatible apps '+dupCheckSet.toString().removeStart('{').removeEnd('}'));
            }
        }
    }

    public static void preventDelete(List<Mobile_App_Assignment__c> lstTriggerOld){

        if (bl_Trigger_Deactivation.isTriggerDeactivated('MobileAppAssignment_preventDelete')) return;

        for (Mobile_App_Assignment__c oMobileAppAssignment : lstTriggerOld){

            oMobileAppAssignment.addError('You can\'t delete a Mobile App Assignement - pleae de-activate the Mobile App Assignment.');

        }

    }
}