@isTest private class Test_ctrl_CreateAssetRequest {
	
	@isTest static void createTestData() {

	    //---------------------------------------
        // Create Test Data
        //---------------------------------------
        clsTestData_MasterData.createCompany();
        clsTestData_MasterData.createBusinessUnit();
        clsTestData_MasterData.createSubBusinessUnit();
        clsTestData_MasterData.tCountryCode = 'BE';
        clsTestData_MasterData.createCountry();
        clsTestData_Account.tCountry_Account = 'BELGIUM';
        clsTestData_Account.createAccount();
        clsTestData_Contact.createContact();
        clsTestData_MasterData.createBURelatedListSetupData_Asset();
        clsTestData_Product.createProduct();		
        clsTestData_MasterData.createAssetRequestEmail();
   		clsTestData_AccountPlan.createAccountPlan2();
	    //---------------------------------------

	}
	
	@isTest static void test_ctrl_CreateAssetRequest_Account() {
		
		String tTest = '';
		Boolean bTest = false;
		Blob blobTest;
		Asset_Request__c oAssetRequest = new Asset_Request__c();


	    //---------------------------------------
		// Create Test Data
	    //---------------------------------------
		createTestData();
	    //---------------------------------------


	    //---------------------------------------
		// Perform Test
	    //---------------------------------------
		Test.startTest();

        PageReference oPageReference = new PageReference('/apex/CreateAssetRequest?id=' + clsTestData_Account.oMain_Account.Id);
        Test.setCurrentPage(oPageReference);

		ctrl_CreateAssetRequest oCTRL = new ctrl_CreateAssetRequest();

		oCTRL.save();

		oCTRL.oAssetRequest = oAssetRequest;
		oAssetRequest = oCTRL.oAssetRequest;

		oCTRL.tSelectedBusinessUnit = clsTestData_MasterData.oMain_BusinessUnit.Id;
		tTest = oCTRL.tSelectedBusinessUnit;

		tTest = oCTRL.tEmailMessage;
		bTest = oCTRL.bShowType;
		bTest = oCTRL.bShowAssetChangeType;
		bTest = oCTRL.bIsSmallCapital;
		bTest = oCTRL.bShowAttachment;

		oCTRL.attachment_Body = Blob.valueOf('TEST BODY');
		blobTest = oCTRL.attachment_Body;

		oCTRL.attachment_Name = 'TEST FILE NAME';
		tTest = oCTRL.attachment_Name;

		oCTRL.tSelectedBusinessUnit = 'IDONTEXIST';
		oCTRL.changedBusinessUnit();
		oCTRL.tSelectedBusinessUnit = clsTestData_MasterData.oMain_AssetRequestEmail.Business_Unit__c;
		oCTRL.changedBusinessUnit();

		oAssetRequest = oCTRL.oAssetRequest;
		oAssetRequest.Action__c = 'Create';
		oAssetRequest.Type__c = 'Big Capital';
		oCTRL.oAssetRequest = oAssetRequest;
		oCTRL.changedType();

		oAssetRequest = oCTRL.oAssetRequest;
		oAssetRequest.Type__c = 'Small Capital';
		oCTRL.oAssetRequest = oAssetRequest;
		oCTRL.changedType();

		oCTRL.save();

		oAssetRequest = oCTRL.oAssetRequest;
		oAssetRequest.Asset_Change_Type__c = clsTestData_MasterData.oMain_AssetRequestEmail.Asset_Change_Type__c;
		oCTRL.oAssetRequest = oAssetRequest;
		oCTRL.changedAssetChangeType();

		tTest = oCTRL.tEmailMessage;

		oCTRL.save();

		oCTRL.cancel();

		Test.stopTest();
	    //---------------------------------------


	    //---------------------------------------
		// Validate Test Result
	    //---------------------------------------

	    //---------------------------------------

	}


	@isTest static void test_ctrl_CreateAssetRequest_AccountPlan2() {

		String tTest = '';
		Boolean bTest = false;
		Blob blobTest;
		Asset_Request__c oAssetRequest = new Asset_Request__c();
		
	    //---------------------------------------
		// Create Test Data
	    //---------------------------------------
		createTestData();
	    //---------------------------------------


	    //---------------------------------------
		// Perform Test
	    //---------------------------------------
		Test.startTest();

        PageReference oPageReference = new PageReference('/apex/CreateAssetRequest?id=' + clsTestData_AccountPlan.oMain_AccountPlan2.Id);
        Test.setCurrentPage(oPageReference);

		ctrl_CreateAssetRequest oCTRL = new ctrl_CreateAssetRequest();

		oCTRL.save();

		oCTRL.oAssetRequest = oAssetRequest;
		oAssetRequest = oCTRL.oAssetRequest;

		oCTRL.tSelectedBusinessUnit = clsTestData_MasterData.oMain_BusinessUnit.Id;
		tTest = oCTRL.tSelectedBusinessUnit;

		tTest = oCTRL.tEmailMessage;
		bTest = oCTRL.bShowType;
		bTest = oCTRL.bShowAssetChangeType;
		bTest = oCTRL.bIsSmallCapital;
		bTest = oCTRL.bShowAttachment;

		oCTRL.attachment_Body = Blob.valueOf('TEST BODY');
		blobTest = oCTRL.attachment_Body;

		oCTRL.attachment_Name = 'TEST FILE NAME';
		tTest = oCTRL.attachment_Name;

		oCTRL.tSelectedBusinessUnit = 'IDONTEXIST';
		oCTRL.changedBusinessUnit();
		oCTRL.tSelectedBusinessUnit = clsTestData_MasterData.oMain_AssetRequestEmail.Business_Unit__c;
		oCTRL.changedBusinessUnit();

		oAssetRequest = oCTRL.oAssetRequest;
		oAssetRequest.Action__c = 'Create';
		oAssetRequest.Type__c = 'Big Capital';
		oCTRL.oAssetRequest = oAssetRequest;
		oCTRL.changedType();

		oAssetRequest = oCTRL.oAssetRequest;
		oAssetRequest.Type__c = 'Small Capital';
		oCTRL.oAssetRequest = oAssetRequest;
		oCTRL.changedType();

		oCTRL.save();

		oAssetRequest = oCTRL.oAssetRequest;
		oAssetRequest.Asset_Change_Type__c = clsTestData_MasterData.oMain_AssetRequestEmail.Asset_Change_Type__c;
		oCTRL.oAssetRequest = oAssetRequest;
		oCTRL.changedAssetChangeType();

		tTest = oCTRL.tEmailMessage;

		oCTRL.save();

		oCTRL.cancel();

		Test.stopTest();
	    //---------------------------------------


	    //---------------------------------------
		// Validate Test Result
	    //---------------------------------------

	    //---------------------------------------

	}
	
}