//--------------------------------------------------------------------------------------------------------------------
//  Author           : 

//  Description      : APEX Test Class for bl_User_Trigger
//  Change Log       : 
//    - 20/03/2019 - Bart Caelen - Replaced MEACAT by CEMA
//    PMT-22787: MITG EMEA User Setup - Changed profile from EUR Field Force MITG to EMEA Field Force MITG
//--------------------------------------------------------------------------------------------------------------------

@isTest private class TEST_bl_User_Trigger {

  private static User oUser;
  private static String tCountry1 = 'Belgium';
  private static String tCountry2 = 'Austria';
  private static String tPrimarySBU1 = '';
  private static String tPrimarySBU2 = '';
  private static String tJobTitle = 'District Manager';
  private static String tGroupName1_1;
  private static String tGroupName1_2;
  private static String tGroupName2_1;
  private static String tGroupName2_2;
  private static Set<String> setGroupName = new Set<String>();

  private static List<User> lstUser_SystemAdmin = new List<User>();

  @isTest static void createTestData(){


        lstUser_SystemAdmin.add(clsTestData_User.createUser_SystemAdministrator('tadm000', false));
        lstUser_SystemAdmin.add(clsTestData_User.createUser_SystemAdministrator('tadm001', false));
        lstUser_SystemAdmin.add(clsTestData_User.createUser_SystemAdministrator('tadm002', false));
        lstUser_SystemAdmin.add(clsTestData_User.createUser_SystemAdministrator('tadm003', false));
        insert lstUser_SystemAdmin;

        System.runAs(lstUser_SystemAdmin[0]){
      clsTestData.createCompanyData();
      clsTestData.createSubBusinessUnitData();
      clsTestData.createCountryData();
    }

    DIB_Country__c oCountry_BE = clsTestData.mapCode_Country.get('BE');
    DIB_Country__c oCountry_AT = clsTestData.mapCode_Country.get('AT');

    List<Sub_Business_Units__c> lstSBU = [SELECT Id, Name, Business_Unit__c FROM Sub_Business_Units__c WHERE Company_Text__c = 'Europe' ORDER BY Name ASC LIMIT 2];

    tPrimarySBU1 = lstSBU[0].Name;
    tPrimarySBU2 = lstSBU[1].Name;

    System.debug('** tPrimarySBU1 : ' + tPrimarySBU1);
    System.debug('** tPrimarySBU2 : ' + tPrimarySBU2);

    tGroupName1_1 = tCountry1 + '_ALL_' + tPrimarySBU1.replaceAll('&', '').replaceAll('  ', ' ').replaceAll(' ', '_');
    tGroupName1_2 = tCountry1 + '_ALL_' + tPrimarySBU2.replaceAll('&', '').replaceAll('  ', ' ').replaceAll(' ', '_');
    tGroupName2_1 = tCountry2 + '_ALL_' + tPrimarySBU1.replaceAll('&', '').replaceAll('  ', ' ').replaceAll(' ', '_');
    tGroupName2_2 = tCountry2 + '_ALL_' + tPrimarySBU2.replaceAll('&', '').replaceAll('  ', ' ').replaceAll(' ', '_');
    setGroupName.add(tGroupName1_1);
    setGroupName.add(tGroupName1_2);
    setGroupName.add(tGroupName2_1);
    setGroupName.add(tGroupName2_2);

    System.debug('** tGroupName1_1 : ' + tGroupName1_1);
    System.debug('** tGroupName1_2 : ' + tGroupName1_2);
    System.debug('** tGroupName2_1 : ' + tGroupName2_1);
    System.debug('** tGroupName2_2 : ' + tGroupName2_2);

    // Create Queue_Mapping__c
    List<Queue_Mapping__c> lstQM = new List<Queue_Mapping__c>();
    Queue_Mapping__c oQM1_1 = new Queue_Mapping__c();
      oQM1_1.Sub_Business_Unit__c = lstSBU[0].Id;
      oQM1_1.Country__c = oCountry_BE.Id;
      oQM1_1.Job_Title__c = tJobTitle;
      oQM1_1.Queue_Name__c = tGroupName1_1;
    lstQM.add(oQM1_1);
    Queue_Mapping__c oQM1_2 = new Queue_Mapping__c();
      oQM1_2.Sub_Business_Unit__c = lstSBU[1].Id;
      oQM1_2.Country__c = oCountry_BE.Id;
      oQM1_2.Job_Title__c = tJobTitle;
      oQM1_2.Queue_Name__c = tGroupName1_2;
    lstQM.add(oQM1_2);

    Queue_Mapping__c oQM2_1 = new Queue_Mapping__c();
      oQM2_1.Sub_Business_Unit__c = lstSBU[0].Id;
      oQM2_1.Country__c = oCountry_AT.Id;
      oQM2_1.Job_Title__c = tJobTitle;
      oQM2_1.Queue_Name__c = tGroupName2_1;
    lstQM.add(oQM2_1);
    Queue_Mapping__c oQM2_2 = new Queue_Mapping__c();
      oQM2_2.Sub_Business_Unit__c = lstSBU[1].Id;
      oQM2_2.Country__c = oCountry_AT.Id;
      oQM2_2.Job_Title__c = tJobTitle;
      oQM2_2.Queue_Name__c = tGroupName2_2;
    lstQM.add(oQM2_2);

        System.runAs(lstUser_SystemAdmin[0]){
      insert lstQM;
    }


    //Profile oProfile = [SELECT Id FROM Profile WHERE Name = 'EUR Field Force MITG'];
        Profile oProfile = [SELECT Id FROM Profile WHERE Name = 'EMEA Field Force MITG'];
    UserRole oUserRole = [SELECT Id FROM UserRole WHERE Name = 'Netherlands (MITG)'];

    List<Group> lstGroup = new List<Group>();
    Group oGroup1_1 = new Group();
      oGroup1_1.Name = tGroupName1_1;
      oGroup1_1.DeveloperName = tGroupName1_1;
    lstGroup.add(oGroup1_1);
    Group oGroup1_2 = new Group();
      oGroup1_2.Name = tGroupName1_2;
      oGroup1_2.DeveloperName = tGroupName1_2;
    lstGroup.add(oGroup1_2);
    Group oGroup2_1 = new Group();
      oGroup2_1.Name = tGroupName2_1;
      oGroup2_1.DeveloperName = tGroupName2_1;
    lstGroup.add(oGroup2_1);
    Group oGroup2_2 = new Group();
      oGroup2_2.Name = tGroupName2_2;
      oGroup2_2.DeveloperName = tGroupName2_2;
    lstGroup.add(oGroup2_2);

        System.runAs(lstUser_SystemAdmin[0]){
      insert lstGroup;
    }
    System.assertEquals(lstGroup.size(), 4);

    clsTestData.createUserData('MITG000G_TEST@Medtronic.com', 'MITG000', tCountry1, oProfile.Id, oUserRole.Id, false);
      oUser = clsTestData.oMain_User;
      oUser.Job_Title_vs__c = tJobTitle;
      oUser.Region_vs__c = 'Europe';
      oUser.Primary_sBU__c = tPrimarySBU1;

    System.runAs(lstUser_SystemAdmin[1]){
      insert oUser;
    }

  }

  @isTest static void test_setPublicGroupAssignment_Update_1() {

    createTestData();

    Test.startTest();

    System.runAs(lstUser_SystemAdmin[3]){

        oUser.Primary_sBU__c = tPrimarySBU2;
      update oUser;

    }

    Test.stopTest();

    List<GroupMember> lstGroupMember = [SELECT Id, UserOrGroupId FROM GroupMember WHERE Group.DeveloperName = :tGroupName1_2];
    System.assertEquals(lstGroupMember.size(), 1);
    System.assertEquals(lstGroupMember[0].UserOrGroupId, oUser.Id);

  }

  @isTest static void test_setPublicGroupAssignment_Update_2() {

    createTestData();

    Test.startTest();

    System.runAs(lstUser_SystemAdmin[2]){
        oUser.Sub_Region__c = 'GIGA';
        oUser.Country_vs__c = tCountry2;
        oUser.Primary_sBU__c = tPrimarySBU1;
      update oUser;

    }

    Test.stopTest();

    List<GroupMember> lstGroupMember = [SELECT Id, UserOrGroupId FROM GroupMember WHERE Group.DeveloperName = :tGroupName2_1];
    System.assertEquals(lstGroupMember.size(), 1);
    System.assertEquals(lstGroupMember[0].UserOrGroupId, oUser.Id);

  }

  @isTest static void test_setPublicGroupAssignment_Update_3() {

    createTestData();

    Test.startTest();

    System.runAs(lstUser_SystemAdmin[2]){
        oUser.Sub_Region__c = 'GIGA';
        oUser.Country_vs__c = tCountry2;
        oUser.Primary_sBU__c = tPrimarySBU2;
      update oUser;

    }

    Test.stopTest();

    List<GroupMember> lstGroupMember = [SELECT Id, UserOrGroupId FROM GroupMember WHERE Group.DeveloperName = :tGroupName2_2];
    System.assertEquals(lstGroupMember.size(), 1);
    System.assertEquals(lstGroupMember[0].UserOrGroupId, oUser.Id);

  }  
/*
  @isTest static void test_setPublicGroupAssignment_Update_4() {

    createTestData();

    Test.startTest();

    System.runAs(lstUser_SystemAdmin[2]){

        oUser.Job_Title_vs__c = 'NOT_A_REAL_JOB_TITLE';
      update oUser;

    }

    Test.stopTest();

    List<GroupMember> lstGroupMember = [SELECT Id, UserOrGroupId FROM GroupMember WHERE Group.DeveloperName = :setGroupName AND UserOrGroupId = :oUser.Id];
    System.assertEquals(lstGroupMember.size(), 0);

  }    
*/
  @isTest static void test_setPublicGroupAssignment_Update_5() {

    createTestData();

    Test.startTest();

    System.runAs(lstUser_SystemAdmin[2]){

        oUser.Region_vs__c = 'CEMA';
      update oUser;

    }

    Test.stopTest();

    List<GroupMember> lstGroupMember = [SELECT Id, UserOrGroupId FROM GroupMember WHERE Group.DeveloperName = :setGroupName AND UserOrGroupId = :oUser.Id];
    System.assertEquals(lstGroupMember.size(), 0);

  }    

  @isTest static void test_setPublicGroupAssignment_Update_Error() {

    Boolean bError = false;
    createTestData();

    Test.startTest();

    System.runAs(lstUser_SystemAdmin[2]){

      clsUtil.hasException = true;

        oUser.Region_vs__c = 'CEMA';
      try{ 
        update oUser; 
      }catch(Exception oEX){
        bError = true;
      }

    }

    Test.stopTest();

    System.assertEquals(bError, true);

  }    

}