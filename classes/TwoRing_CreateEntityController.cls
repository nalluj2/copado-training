@RestResource(urlMapping='/CreateEntity')
global class TwoRing_CreateEntityController {
	@HttpPost
	global static String createEntity(TwoRing_CreateOrUpdateEntityObject entityObject) {
		return TwoRing_Repository.createEntity(entityObject);
	}
}