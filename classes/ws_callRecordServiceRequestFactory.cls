public class ws_callRecordServiceRequestFactory {

	public static void deleteRecord(String mobileId){
		RestRequest req = new RestRequest();  
    	RestResponse res = new RestResponse();
		
		Call_Records__c callRecord = new Call_Records__c();
		callRecord.Mobile_ID__c = mobileId;
		
		JSONGenerator gen = JSON.createGenerator(true);
    	
		gen.writeStartObject();
        gen.writeStringField('id', '12346');
	    gen.writeObjectField('callRecord', callRecord);
	   
        
        // Get the JSON string.
        String pretty = gen.getAsString();	    	
		System.debug('test '+pretty);

        req.requestbody = Blob.valueOf(pretty);
    	
     	req.requestURI = '/services/apexrest/CallRecordService';  
	    req.httpMethod = 'DELETE';
    	RestContext.request = req;
    	RestContext.response = res;
		
		String test = ws_CallRecordService.doDelete();
		System.debug('test '+test);
		
	}

	public static Call_Records__c createBUCallRecord(){
		RestRequest req = new RestRequest();  
    	RestResponse res = new RestResponse();
    	
		String id=GuidUtil.NewGuid();
		List<Call_Topics__c> callTopics = new List<Call_Topics__c>();
		List<Call_Topic_Subject__c> callTopicSubjects  = new List<Call_Topic_Subject__c>();
		List<Call_Topic_Products__c> callTopicProducts  = new List<Call_Topic_Products__c>();
		List<Call_Topic_Business_Unit__c> callTopicBusinessUnit  = new List<Call_Topic_Business_Unit__c>();

		Company__c company = [SELECT Id, name, Company_Code_Text__c FROM Company__c WHERE Company_Code_Text__c = 'EUR'].get(0);
		Business_Unit__c bu = [SELECT Company__c FROM Business_Unit__c where Company__r.Company_Code_Text__c ='EUR' AND Business_Unit_Group__r.Name IN ('Restorative', 'CVG', 'Diabetes') AND Name NOT IN ('Neurovascular') AND (NOT Name LIKE 'OLD_%')].get(0);
		List<Therapy_Product__c> therapyProducts =  [SELECT Id,Product__c,Therapy__c FROM Therapy_Product__c where Therapy__r.Business_Unit__c =:  bu.id]; 
		
		Call_Records__c callRecordMobileId = new Call_Records__c();
	
		Call_Records__c callRecord = new Call_Records__c();
		callRecord.Business_Unit__c = bu.id;
		callRecord.Mobile_ID__c = GuidUtil.NewGuid();
		callRecord.Call_Date__c = Date.today();
		callRecord.Call_Channel__c = '';
		/*
		callRecord.Call_Duration_Time_Minutes__c = 10;
		callRecord.Call_Travel_Time_Minutes__c = 5;
		callRecord.Call_Venue__c = '';
		callRecord.RTG_Comment__c = '';
		*/
		callRecordMobileId.Mobile_ID__c = callRecord.Mobile_ID__c;
		
    	List<Contact_Visit_Report__c> attendedContacts = new List<Contact_Visit_Report__c>();
    	
    	List<Affiliation__c> affs = [SELECT Affiliation_To_Account__c, Affiliation_From_Contact__c
                                 FROM Affiliation__c where RecordType.name = 'C2A' and Affiliation_Active__c = true
                                        and (Affiliation_Type__c <> 'Referring') limit 10];
    	
    	Affiliation__c aff = affs.get(0);
    	Contact_Visit_Report__c cvr1 = new Contact_Visit_Report__c();
    	cvr1.Attending_Affiliated_Account__c = aff.Affiliation_To_Account__c;
    	cvr1.Attending_Contact__c = aff.Affiliation_From_Contact__c; //'003W000000I7wQrIAJ' to test deleted item
    	cvr1.Call_Records__r = callRecordMobileId;
    	cvr1.Mobile_ID__c = GuidUtil.NewGuid();
    	
    	attendedContacts.add(cvr1);

    	Affiliation__c aff2 = affs.get(1);
    	Contact_Visit_Report__c cvr2 = new Contact_Visit_Report__c();
    	cvr2.Attending_Affiliated_Account__c = aff2.Affiliation_To_Account__c;
    	cvr2.Attending_Contact__c = aff2.Affiliation_From_Contact__c;
    	cvr2.Call_Records__r = callRecordMobileId;
    	cvr2.Mobile_ID__c = GuidUtil.NewGuid();
		
    	attendedContacts.add(cvr2);

		Call_Activity_Type__c callActType = [SELECT Id,Name FROM Call_Activity_Type__c where Company_ID__r.Company_code_text__c = 'EUR'].get(0);
		
        Call_Topics__c ct1 = new Call_topics__c();
        ct1.Call_Records__r = callRecordMobileId;
        ct1.Call_Activity_Type__c = callActType.id;
        ct1.Mobile_ID__c = GuidUtil.NewGuid();
        
        Call_Topics__c ctMobileID = new Call_topics__c();
        ctMobileID.Mobile_ID__c = ct1.Mobile_ID__c;

        Call_Topic_Subject__c subject1 = new Call_Topic_Subject__c();
        subject1.Call_Topic__r = ctMobileID;
        subject1.Mobile_ID__c = GuidUtil.NewGuid();
		callTopicSubjects.add(subject1);

		Therapy_Product__c thp1 = therapyProducts.get(0);
		
		Call_Topic_Products__c product1 = new Call_Topic_Products__c();
		product1.Mobile_ID__c = GUIDUtil.NewGuid();
		product1.Product__c = thp1.Product__c;
		product1.Call_Topic__r = ctMobileID;

		callTopicProducts.add(product1);
    	
    	callTopics.add(ct1);
    	
        Call_Topics__c ct2 = new Call_topics__c();
        ct2.Call_Records__r = callRecordMobileId;
        ct2.Call_Activity_Type__c = callActType.id;
        ct2.Mobile_ID__c = GuidUtil.NewGuid();
        
        Call_Topics__c ctMobileID2 = new Call_topics__c();
        ctMobileID2.Mobile_ID__c = ct2.Mobile_ID__c;

        Call_Topic_Subject__c subject2 = new Call_Topic_Subject__c();
        subject2.Call_Topic__r = ctMobileID2;
        subject2.Mobile_ID__c = GuidUtil.NewGuid();
		callTopicSubjects.add(subject2);

		Therapy_Product__c thp2 = therapyProducts.get(1);
		
		Call_Topic_Products__c product2 = new Call_Topic_Products__c();
		product2.Mobile_ID__c = GUIDUtil.NewGuid();
		product2.Product__c = thp2.Product__c;
		product2.Call_Topic__r = ctMobileID2;

		callTopicProducts.add(product2);
    	
    	callTopics.add(ct2);

    	//Create call record
    	
    	JSONGenerator gen = JSON.createGenerator(true);
    	
		gen.writeStartObject();
        gen.writeStringField('id', '12346');
	    gen.writeObjectField('callTopicSubjects',callTopicSubjects);
	    gen.writeObjectField('callTopics',callTopics);
	    gen.writeObjectField('callTopicProducts',callTopicProducts);
	    gen.writeObjectField('callTopicBusinessUnit',callTopicBusinessUnit);
	    gen.writeObjectField('attendedContacts',attendedContacts);
	    gen.writeObjectField('callRecord', callRecord);
	   
        
        // Get the JSON string.
        String pretty = gen.getAsString();	    	
		System.debug('test '+pretty);

        req.requestbody = Blob.valueOf(pretty);
    	
     	req.requestURI = '/services/apexrest/CallRecordService';  
	    req.httpMethod = 'POST';
    	RestContext.request = req;
    	RestContext.response = res;
		
		String test = ws_CallRecordService.doPost();
		System.debug('test '+test);
		
		//Update call record
		//remove attended contact 
    	attendedContacts.remove(0);
    	
    	//remove call Topic
		callTopics.remove(0);
		
				
		RestRequest req2 = new RestRequest(); 
    	RestResponse res2 = new RestResponse();
		
    	JSONGenerator gen2 = JSON.createGenerator(true);
    	

    	
		gen2.writeStartObject();
        gen2.writeStringField('id', '123467');
	    gen2.writeObjectField('callTopicSubjects',callTopicSubjects);
	    gen2.writeObjectField('callTopics',callTopics);
	    gen2.writeObjectField('callTopicProducts',callTopicProducts);
	    gen2.writeObjectField('callTopicBusinessUnit',callTopicBusinessUnit);

	    gen2.writeObjectField('attendedContacts',attendedContacts);
	    gen2.writeObjectField('callRecord', callRecord);
	   
        
        // Get the JSON string.
        String pretty2 = gen2.getAsString();	    	
		System.debug('test 2 '+pretty2);

        req2.requestbody = Blob.valueOf(pretty2);
    	
     	req2.requestURI = '/services/apexrest/CallRecordService';  
	    req2.httpMethod = 'POST';
    	RestContext.request = req2;
    	RestContext.response = res2;
		
		//Update after removing attended contacts
		String test2 = ws_CallRecordService.doPost();
		//System.debug('test update'+test2);
		
		return callRecord;
		
	}

}