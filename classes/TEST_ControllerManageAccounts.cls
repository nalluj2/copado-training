@isTest(seealldata=true)
private class TEST_ControllerManageAccounts 
    {
    static testMethod void ControllerManageAccountsMethod() 
        {
        //ControllerManageAccounts cma=new ControllerManageAccounts();
          
            List<Account> accounts = new List<Account>{} ; 
            for (Integer i = 0 ; i < 5 ; i++)
            {
                Account a = new Account() ;
                a.Name = 'Test Account Name ' + i  ;  
                a.RecordTypeId = FinalConstants.sapAccountRecordTypeId ;
                a.Account_Country_vs__c='BELGIUM';
                //a.CurrencyIsoCode =DIBcntry.CurrencyIsoCode ;
                accounts.add(a); 
            } 
            insert accounts;
            
            campaign cmp = new campaign();
            // cmp.Business_Unit__c=bu.id;
            cmp.name='Campaign1';
            cmp.Status='Accounts Approved';
            cmp.isactive=true;
            cmp.Business_Unit_Picklist__c = 'Diabetes';
            cmp.recordtypeid=clsUtil.getRecordTypeByDevName('Campaign', 'Diabetes_Campaign').Id;
            insert cmp;
        
            Target_Account__c tacc=new Target_Account__c();
            tacc.Campaign__c=cmp.id;
            tacc.Status__c='Approved';
            //tacc.createdDate=system.today();
            insert tacc;
            ApexPages.currentPage().getParameters().put('cid',cmp.id);
            ApexPages.StandardSetController ctrl = new ApexPages.StandardSetController(accounts);    
            ControllerManageAccounts controller = new ControllerManageAccounts(ctrl);
            // controller.AcccountId=accounts[0].id;
          //  controller.fieldtype='Currency';
          //   controller.FieldName1='City__c';
            // controller.OutName1 ='e'; 
            //controller.filterAccountExisting();
            
            controller.FieldName1='City__c';
            controller.OutName1 ='e'; 
             controller.FieldName2='City__c';
            controller.OutName2 ='n'; 
             controller.FieldName3='City__c';
            controller.OutName3 ='s'; 
             controller.FieldName4='City__c';
            controller.OutName4='c'; 
              controller.FieldName5='City__c';
            controller.OutName5='l'; 
           
            controller.FilterAccount(); 
            controller.refresh();
            controller.getcolumnNamesNew();
            controller.getOperatorNames();
             controller.filterAccountExisting();
            
          
             
            controller.getlstwrap();
            //controller.wrapperClass();
            //controller.wrapclass();
            controller.AddingTargetAccount();
            controller.AddingApprovedTargetAccount();
            controller.RemoveTargetAccount();
            controller.clearfilters();  
           // controller.getOprater('a','b','c'); 
          
        }
    }