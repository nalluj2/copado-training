//---------------------------------------------------------------------------------------------------------------------------------
//	Author 			: Bart Caelen
//  Created Date 	: 21/04/2020
//  Description 	: APEX TEST Class for bl_PatientTraining_Trigger and the Trigger tr_PatientTraining
//  Change Log 		: 
//---------------------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_bl_PatientTraining_Trigger{

	@TestSetUp
	private static void createTestData(){

		// Create Opportunity
		clsTestData_Opportunity.iRecord_Opportunity_CANDIB = 1;
		List<Opportunity> lstOpportunity = clsTestData_Opportunity.createOpportunity_CANDIB();
		
		// Create Patient Training
		List<Patient_Training__c> lstPatientTraining = new List<Patient_Training__c>();
		for (Integer iCounter = 0; iCounter < 4; iCounter++){
			Patient_Training__c oPatientTraining = new Patient_Training__c();
				oPatientTraining.Opportunity__c = lstOpportunity[0].Id;
			lstPatientTraining.add(oPatientTraining);
		}
		insert lstPatientTraining;

	}


	@IsTest private static void test_calculateCompensation_Amount(){


		//-------------------------------------------------------------------------------------------------------------------------
		// GET TEST DATA
		//-------------------------------------------------------------------------------------------------------------------------
		List<Patient_Training__c> lstPatientTraining = [SELECT Id, Training_Compensation__c, Training_Category__c, Training_Type__c, Training_Method__c, New_Pump_Additional_Support_Total_Hrs__c, Auto_Mode_Total_Hrs__c FROM Patient_Training__c];
		
		Map<String, Decimal> mapTrainingCategory_Type_Method_Compensation = bl_PatientTraining_Trigger.loadCompensationAmount();
		List<String> lstKey = new List<String>(mapTrainingCategory_Type_Method_Compensation.keySet());
		//-------------------------------------------------------------------------------------------------------------------------


		//-------------------------------------------------------------------------------------------------------------------------
		// EXECUTE TEST
		//-------------------------------------------------------------------------------------------------------------------------
		List<Decimal> lstCompensationAmount = new List<Decimal>();
		Test.startTest();

			Integer iCounter = 0;
			for (Patient_Training__c oPatientTraining : lstPatientTraining){

				lstCompensationAmount.add(mapTrainingCategory_Type_Method_Compensation.values()[iCounter]);
				String tKey = lstKey[iCounter];
				List<String> lstKeyPart = tKey.split('\\|');

				oPatientTraining.Training_Category__c = lstKeyPart[0];
				oPatientTraining.Training_Type__c = lstKeyPart[1];
				oPatientTraining.Training_Method__c = lstKeyPart[2];

				iCounter++;
			}

			update lstPatientTraining;

		Test.stopTest();
		//-------------------------------------------------------------------------------------------------------------------------


		//-------------------------------------------------------------------------------------------------------------------------
		// VALIDATE RESULT
		//-------------------------------------------------------------------------------------------------------------------------
		lstPatientTraining = [SELECT Id, Training_Compensation__c FROM Patient_Training__c];

		iCounter = 0;
		for (Patient_Training__c oPatientTraining : lstPatientTraining){

			System.assertEquals(oPatientTraining.Training_Compensation__c, lstCompensationAmount[iCounter]);

			iCounter++;

		}
		//-------------------------------------------------------------------------------------------------------------------------


	}


	@IsTest private static void test_calculateCompensation_HourRate(){


		//-------------------------------------------------------------------------------------------------------------------------
		// GET TEST DATA
		//-------------------------------------------------------------------------------------------------------------------------
		List<Patient_Training__c> lstPatientTraining = [SELECT Id, Training_Compensation__c, Training_Category__c, Training_Type__c, Training_Method__c, New_Pump_Additional_Support_Total_Hrs__c, Auto_Mode_Total_Hrs__c FROM Patient_Training__c];

		Map<String, Decimal> mapTrainingCategory_Type_HourPrice = bl_PatientTraining_Trigger.loadCompensationHourPrice();
		List<String> lstKey = new List<String>(mapTrainingCategory_Type_HourPrice.keySet());
		//-------------------------------------------------------------------------------------------------------------------------


		//-------------------------------------------------------------------------------------------------------------------------
		// EXECUTE TEST
		//-------------------------------------------------------------------------------------------------------------------------
		List<Decimal> lstHourRate = new List<Decimal>();
		Test.startTest();

			Integer iCounter = 0;
			for (Patient_Training__c oPatientTraining : lstPatientTraining){

				lstHourRate.add(mapTrainingCategory_Type_HourPrice.values()[iCounter]);
				String tKey = lstKey[iCounter];
				List<String> lstKeyPart = tKey.split('\\|');

				oPatientTraining.Training_Category__c = lstKeyPart[0];
				oPatientTraining.Training_Type__c = lstKeyPart[1];
				oPatientTraining.New_Pump_Additional_Support_Total_Hrs__c = 50;
				oPatientTraining.Auto_Mode_Total_Hrs__c = 100;

				iCounter++;
			}

			update lstPatientTraining;

		Test.stopTest();
		//-------------------------------------------------------------------------------------------------------------------------


		//-------------------------------------------------------------------------------------------------------------------------
		// VALIDATE RESULT
		//-------------------------------------------------------------------------------------------------------------------------
		lstPatientTraining = [SELECT Id, Training_Type__c, Training_Compensation__c FROM Patient_Training__c];

		iCounter = 0;
		for (Patient_Training__c oPatientTraining : lstPatientTraining){

			if (oPatientTraining.Training_Type__c == 'Auto Mode'){
			
				System.assertEquals(oPatientTraining.Training_Compensation__c, lstHourRate[iCounter] * 100);
			
			}else{

				System.assertEquals(oPatientTraining.Training_Compensation__c, lstHourRate[iCounter] * 50);

			}

			iCounter++;

		}
		//-------------------------------------------------------------------------------------------------------------------------


	}

}