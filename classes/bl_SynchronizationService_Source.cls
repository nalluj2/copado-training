/*	Integration Service related class
	
	This class contains methods to generate notifications in the source Organization. To add new objects to the synchronization process you
	need to modify this class to add the specific object handler to the list of Supported Objects. 
*/
public without sharing class bl_SynchronizationService_Source {
	
	//Map with the supported objects (API name as key and specific implementation as value)
	private static Map<String, String> supportedObjects = new Map<String, String>{
						
		'Asset' => 'SynchronizationService_Asset',	
		'Installed_Software__c' => 'SynchronizationService_InstSoftware',
		'Case' => 'SynchronizationService_Case',
		'Complaint__c' => 'SynchronizationService_Complaint',	
		'Case_Comment__c' => 'SynchronizationService_Comment',	
		'Workorder__c' => 'SynchronizationService_Workorder',
		'Workorder_Sparepart__c' => 'SynchronizationService_Sparepart',
		'Parts_Shipment__c' => 'SynchronizationService_PartShipment',
		'Attachment' => 'SynchronizationService_Attachment',
		'MRI_Scanner__c' => 'SynchronizationService_MRIScanner'
	};
	
	//Map with the class instances for the supported objects. This will help to create a single instance per execution and avoid creating duplicate Notifications
	private static Map<String, SynchronizationService_Object> supportedObjectInstances = new Map<String, SynchronizationService_Object>();
				
	//Method to create Synchronization Notifications when needed. To know if a notification should be created or not this method delegates the 
	//functionality to the specific implementation for each Object. This method should be invoked directly from a trigger and with all the records
	//being of the same type.
	public static void createNotificationIfChanged(List<SObject> records, Map<Id, SObject> oldRecords){
		
		//Get the type of object from the Id of the first record in the list
		String objectType = records[0].Id.getSobjectType().getDescribe().getName();
					
		//If the object type is not found in the list of supported object, we finish here.
		String objectImplClass = supportedObjects.get(objectType);		
		if(objectImplClass == null) return;
		
		//Get the instance for the Object Type		
		SynchronizationService_Object objectImpl = supportedObjectInstances.get(objectType);
		
		//If no instance was created before, we create a new one and add it to the instance map
		if(objectImpl == null){
			objectImpl = (SynchronizationService_Object) System.Type.forName(objectImplClass).newInstance();
			supportedObjectInstances.put(objectType, objectImpl);	
		}
		
		//The specific implementation for the Object will return the list of External Ids for the records that need to be notified to the target Org.	
		Set<String> externalIds = objectImpl.hasChangeForNotification(records, oldRecords);
		
		if(externalIds.size() > 0){
			 
			List<Sync_Notification__c> notifications = new List<Sync_Notification__c>();
		
			for(String recordId : externalIds){
				
				Sync_Notification__c notification = new Sync_Notification__c();
				notification.Record_Id__c = recordId;
				notification.Record_Object_Type__c = objectType;
				notification.Status__c = 'New';
				notification.Notification_Id__c = objectType + '-'+ GuidUtil.NewGuid();		
				notification.Type__c = 'Outbound';// Type is Outbound in the source Org. This is used by the Batch processes to know if we should send it or process it.
				
				notifications.add(notification);			
			}
									
			insert notifications;
			
			//If we are during a sync execution we cannot launch another batch here. A similar check will be done in the original sync batch and launch this process if needed
			if(bl_SynchronizationService_Utils.isSyncProcess == false && bl_SynchronizationService_Utils.isSyncEnabled == true){
				
				//Check if there is already a batch execution in the queue. If so, there is no need to create a new one.	
				List<AsyncApexJob> pendingJobs = [SELECT Id FROM AsyncApexJob where JobType = 'BatchApex' AND ApexClass.Name = 'ba_SynchronizationService_Outbound' AND Status IN ('Holding', 'Queued') LIMIT 1 ];
					
				if(pendingJobs.isEmpty()){
					ba_SynchronizationService_Outbound batch = new ba_SynchronizationService_Outbound();
					Database.executeBatch(batch, 200);
				}			
			}
		}
	}
	
	//Generic method to dispatch the generation of the information to be sent to the target Org. Each Object has its own implementation that returns
	//a String with all the information serialized in JSON format. This method supports a single record at a time (restriction on the target Org)
	public static String generateRecordPayload(String recordId, String objectType){
				
		//If the object type is not found in the list of supported object, we finish here.
		String objectImplClass = supportedObjects.get(objectType);		
		if(objectImplClass == null) return null;
		
		//Get the instance for the Object Type		
		SynchronizationService_Object objectImpl = supportedObjectInstances.get(objectType);
			
		//If no instance was created before, we create a new one and add it to the instance map
		if(objectImpl == null){
			objectImpl = (SynchronizationService_Object) System.Type.forName(objectImplClass).newInstance();
			supportedObjectInstances.put(objectType, objectImpl);	
		}
		
		return objectImpl.generatePayload(recordId);
	}		
}