@isTest public class clsTestData_Affiliation {


    public static Integer iRecord_Affiliation_C2A = 1;
    public static Id idRecordType_Affiliation_C2A = clsUtil.getRecordTypeByDevName('Affiliation__c', 'C2A').Id;
    public static Affiliation__c oMain_Affiliation_C2A;
    public static List<Affiliation__c> lstAffiliation_C2A = new List<Affiliation__c>();

    public static Integer iRecord_Affiliation_A2A = 1;
    public static Id idRecordType_Affiliation_A2A = clsUtil.getRecordTypeByDevName('Affiliation__c', 'A2A').Id;
    public static Affiliation__c oMain_Affiliation_A2A;
    public static List<Affiliation__c> lstAffiliation_A2A = new List<Affiliation__c>();

    public static Integer iRecord_Affiliation_C2C = 1;
    public static Id idRecordType_Affiliation_C2C = clsUtil.getRecordTypeByDevName('Affiliation__c', 'C2C').Id;
    public static Affiliation__c oMain_Affiliation_C2C;
    public static List<Affiliation__c> lstAffiliation_C2C = new List<Affiliation__c>();

    public static Relationship_Type__c oMain_RelationShipType;
    public static List<Relationship_Type__c> lstRelationShipType = new List<Relationship_Type__c>();

    public static Account_Type__c oMain_AccountType;
    public static List<Account_Type__c> lstAccountType = new List<Account_Type__c>();

    public static String tAccountTypeCountry_ISOCode = 'BE';
    public static List<String> lstAccountTypeCountry_UniqueParent = new List<String>{'Yes', 'Per BU', 'Per SBU', 'Per BUG'};
    public static Account_Type_Country__c oMain_AccountTypeCountry;
    public static List<Account_Type_Country__c> lstAccountTypeCountry = new List<Account_Type_Country__c>();

    //---------------------------------------------------------------------------------------------------    
    // Create Affiliation C2A Data
    //---------------------------------------------------------------------------------------------------    
    public static List<Affiliation__c> createAffiliation_C2A(){
        return createAffiliation_C2A(true);
    }
    public static List<Affiliation__c> createAffiliation_C2A(Boolean bInsert){

        if (oMain_Affiliation_C2A == null){

            if (clsTestData_Account.oMain_Account == null) clsTestData_Account.createAccount();
            if ( (clsTestData_Contact.oMain_Contact == null) || (clsTestData_Contact.lstContact.Size() < iRecord_Affiliation_C2A) ){
                clsTestData_Contact.iRecord_Contact = iRecord_Affiliation_C2A;
                clsTestData_Contact.createContact();
            }
            lstAffiliation_C2A = new List<Affiliation__c>();

            for (Integer i=0; i<iRecord_Affiliation_C2A; i++){
                Affiliation__c oAffiliation = new Affiliation__c();
                    oAffiliation.Affiliation_To_Account__c = clsTestData_Account.oMain_Account.Id;
                    oAffiliation.Affiliation_From_Contact__c = clsTestData_Contact.lstContact[i].Id;
                    oAffiliation.RecordTypeId = idRecordType_Affiliation_C2A;
                    if (i == 0){
                        oAffiliation.Affiliation_Primary__c = true;
                    }
                lstAffiliation_C2A.add(oAffiliation);
            }

            if (bInsert) insert lstAffiliation_C2A;

            oMain_Affiliation_C2A = lstAffiliation_C2A[0];
        }

        return lstAffiliation_C2A;

    }
    public static List<Affiliation__c> createAffiliation_C2A(Account oAccount, Contact oContact, Boolean bPrimary, Boolean bInsert){

        if (oAccount == null){
            clsTestData_Account.oMain_Account = null;
            clsTestData_Account.iRecord_Account = 1;
            oAccount = clsTestData_Account.createAccount()[0];
        }
        if (oContact == null){
            clsTestData_Contact.oMain_Contact = null;
            clsTestData_Contact.iRecord_Contact = 1;
            oContact = clsTestData_Contact.createContact()[0];
        }

        List<Affiliation__c> lstAffiliation_C2A_Single = new List<Affiliation__c>();

        Affiliation__c oAffiliation = new Affiliation__c();
            oAffiliation.Affiliation_To_Account__c = oAccount.Id;
            oAffiliation.Affiliation_From_Contact__c = oContact.Id;
            oAffiliation.RecordTypeId = idRecordType_Affiliation_C2A;
            oAffiliation.Affiliation_Primary__c = bPrimary;
        lstAffiliation_C2A_Single.add(oAffiliation);

        if (bInsert) insert lstAffiliation_C2A_Single;

        return lstAffiliation_C2A_Single;

    }    
    //---------------------------------------------------------------------------------------------------    


    //---------------------------------------------------------------------------------------------------    
    // Create Affiliation A2A Data
    //---------------------------------------------------------------------------------------------------    
    public static List<Affiliation__c> createAffiliation_A2A(){
        return createAffiliation_A2A(true);
    }
    public static List<Affiliation__c> createAffiliation_A2A(Boolean bInsert){

        if (oMain_Affiliation_A2A == null){

            if (clsTestData_Account.oMain_Account == null) clsTestData_Account.createAccount();
            Account oAccount_From = clsTestData_Account.oMain_Account;

            clsTestData_Account.oMain_Account = null;
            clsTestData_Account.iRecord_Account = iRecord_Affiliation_A2A;
            clsTestData_Account.createAccount();
        
            lstAffiliation_A2A = new List<Affiliation__c>();

            for (Integer i=0; i<iRecord_Affiliation_A2A; i++){
                Boolean bPrimary = false;
                if (i==0) bPrimary = true;
                Affiliation__c oAffiliation = createAffiliation_A2A(oAccount_From, clsTestData_Account.lstAccount[i], bPrimary, false)[0];
                lstAffiliation_A2A.add(oAffiliation);
            }

            if (bInsert) insert lstAffiliation_A2A;

        }

        return lstAffiliation_A2A;

    }    

    public static List<Affiliation__c> createAffiliation_A2A(Account oAccount_From, Account oAccount_To, Boolean bPrimary, Boolean bInsert){

        if (oAccount_From == null){
            clsTestData_Account.oMain_Account = null;
            clsTestData_Account.iRecord_Account = 1;
            oAccount_From = clsTestData_Account.createAccount()[0];
        }
        if (oAccount_To == null){
            clsTestData_Account.oMain_Account = null;
            clsTestData_Account.iRecord_Account = 1;
            oAccount_To = clsTestData_Account.createAccount()[0];
        }

        List<Affiliation__c> lstAffiliation_A2A_Single = new List<Affiliation__c>();

        Affiliation__c oAffiliation = new Affiliation__c();
            oAffiliation.Affiliation_To_Account__c = oAccount_From.Id;
            oAffiliation.Affiliation_From_Account__c = oAccount_To.Id;
            oAffiliation.RecordTypeId = idRecordType_Affiliation_A2A;
            oAffiliation.Primary_Dealer__c = bPrimary;
            oAffiliation.Affiliation_Start_Date__c = Date.today();
            oAffiliation.Affiliation_Type__c = 'Member';
        lstAffiliation_A2A_Single.add(oAffiliation);

        if (bInsert) insert lstAffiliation_A2A_Single;

        return lstAffiliation_A2A_Single;

    }    
    //---------------------------------------------------------------------------------------------------   


    //---------------------------------------------------------------------------------------------------    
    // Create Affiliation C2C Data
    //---------------------------------------------------------------------------------------------------    
    public static List<Affiliation__c> createAffiliation_C2C(){
        return createAffiliation_C2C(true);
    }
    public static List<Affiliation__c> createAffiliation_C2C(Boolean bInsert){

        if (oMain_Affiliation_C2C == null){

            if (clsTestData_Contact.oMain_Contact == null) clsTestData_Contact.createContact();

            Contact oContact_From = clsTestData_Contact.oMain_Contact;

            clsTestData_Contact.oMain_Contact = null;
            clsTestData_Contact.iRecord_Contact = iRecord_Affiliation_C2C;
            clsTestData_Contact.createContact();
        
            lstAffiliation_C2C = new List<Affiliation__c>();

            for (Integer i=0; i<iRecord_Affiliation_C2C; i++){
                Boolean bPrimary = false;
                if (i==0) bPrimary = true;
                Affiliation__c oAffiliation = createAffiliation_C2C(oContact_From, clsTestData_Contact.lstContact[i], bPrimary, false)[0];
                lstAffiliation_C2C.add(oAffiliation);
            }

            if (bInsert) insert lstAffiliation_C2C;

        }

        return lstAffiliation_C2C;

    }    

    public static List<Affiliation__c> createAffiliation_C2C(Contact oContact_From, Contact oContact_To, Boolean bPrimary, Boolean bInsert){

        if (oContact_From == null){
            clsTestData_Contact.oMain_Contact = null;
            clsTestData_Contact.iRecord_Contact = 1;
            oContact_From = clsTestData_Contact.createContact()[0];
        }
        if (oContact_To == null){
            clsTestData_Contact.oMain_Contact = null;
            clsTestData_Contact.iRecord_Contact = 1;
            oContact_To = clsTestData_Contact.createContact()[0];
        }

        List<Affiliation__c> lstAffiliation_C2C_Single = new List<Affiliation__c>();

        Affiliation__c oAffiliation = new Affiliation__c();
            oAffiliation.Affiliation_To_Contact__c = oContact_To.Id;
            oAffiliation.Affiliation_From_Contact__c = oContact_From.Id;
            oAffiliation.RecordTypeId = idRecordType_Affiliation_A2A;
            oAffiliation.Primary_Dealer__c = bPrimary;
            oAffiliation.Affiliation_Start_Date__c = Date.today();
            oAffiliation.Affiliation_Type__c = 'Member';
        lstAffiliation_C2C_Single.add(oAffiliation);

        if (bInsert) insert lstAffiliation_C2C_Single;

        return lstAffiliation_C2C_Single;

    }    
    //---------------------------------------------------------------------------------------------------   

    //---------------------------------------------------------------------------------------------------
    // RelationShip Type
    //---------------------------------------------------------------------------------------------------    
    public static List<Relationship_Type__c> createRelationShipType(){
    	return createRelationShipType(true);
    }
    public static List<Relationship_Type__c> createRelationShipType(Boolean bInsert){

		if (oMain_RelationShipType == null){
			lstRelationShipType = new List<Relationship_Type__c>();
	    	Relationship_Type__c oRelationShipType_01 = new Relationship_Type__c();
		    	oRelationShipType_01.Relationship_Type__c = 'Referring';
		    	oRelationShipType_01.Sub_Type__c = 'A2A';
		    lstRelationShipType.add(oRelationShipType_01);

	    	Relationship_Type__c oRelationShipType_02 = new Relationship_Type__c();
		    	oRelationShipType_02.Relationship_Type__c = 'Employee';
		    	oRelationShipType_02.Sub_Type__c = 'A2A';
		    lstRelationShipType.add(oRelationShipType_02);

		    if (bInsert) insert lstRelationShipType;

		    oMain_RelationShipType = lstRelationShipType[0];
		}

    	return lstRelationShipType;

    }
    //---------------------------------------------------------------------------------------------------


    //---------------------------------------------------------------------------------------------------
    // Account_Type__c
    //---------------------------------------------------------------------------------------------------    
    public static List<Account_Type__c> createAccountType(){
    	return createAccountType(true);
    }
    public static List<Account_Type__c> createAccountType(Boolean bInsert){

    	if (oMain_AccountType == null){
    		lstAccountType = new List<Account_Type__c>();


			Account_Type__c oAccountType_01 = new Account_Type__c();
				oAccountType_01.Account_Type__c = 'Sold To';
				oAccountType_01.Account_Type_Level__c = '1';
				oAccountType_01.Hierarchy_Type__c = 'Standard';
				oAccountType_01.Identify_Field__c = 'SAP_Account_Type__c';
				oAccountType_01.Identify_Operator__c = 'Equals';
				oAccountType_01.Identify_Value__c = 'Sold-to party';
			lstAccountType.add(oAccountType_01);

			Account_Type__c oAccountType_02 = new Account_Type__c();
				oAccountType_02.Account_Type__c = 'Ship To';
				oAccountType_02.Account_Type_Level__c = '0';
				oAccountType_02.Hierarchy_Type__c = 'Standard';
				oAccountType_02.Identify_Field__c = 'SAP_Account_Type__c';
				oAccountType_02.Identify_Operator__c = 'Equals';
				oAccountType_02.Identify_Value__c = 'Ship to party';
			lstAccountType.add(oAccountType_02);

			Account_Type__c oAccountType_03 = new Account_Type__c();
				oAccountType_03.Account_Type__c = 'Buying Group';
				oAccountType_03.Account_Type_Level__c = '6';
				oAccountType_03.Hierarchy_Type__c = 'Standard';
				oAccountType_03.Identify_Field__c = 'Type';
				oAccountType_03.Identify_Operator__c = 'Equals';
				oAccountType_03.Identify_Value__c = 'Buying Group';    
			lstAccountType.add(oAccountType_03);

			Account_Type__c oAccountType_04 = new Account_Type__c();
				oAccountType_04.Account_Type__c = 'Buying Group';
				oAccountType_04.Account_Type_Level__c = '1';
				oAccountType_04.Hierarchy_Type__c = 'Medtronic';
				oAccountType_04.Identify_Field__c = 'SAP_Customer_Group__c';
				oAccountType_04.Identify_Operator__c = 'Contains';
				oAccountType_04.Identify_Value__c = 'DISTRIBUTOR / RE-SEL'; 
			lstAccountType.add(oAccountType_04);

			if (bInsert) insert lstAccountType;

			oMain_AccountType = lstAccountType[0];
    	}


    	return lstAccountType;
    }
    //---------------------------------------------------------------------------------------------------


    //---------------------------------------------------------------------------------------------------
    // Account_Type_Country__c
    //---------------------------------------------------------------------------------------------------    
    public static List<Account_Type_Country__c> createAccountTypeCountry(){
    	return createAccountTypeCountry(true);
    }
    public static List<Account_Type_Country__c> createAccountTypeCountry(Boolean bInsert){

    	if (oMain_AccountTypeCountry == null){

    		if (oMain_AccountType == null) createAccountType();
    		if (clsTestData_MasterData.oMain_Country == null) clsTestData_MasterData.createCountry(); 

    		lstAccountTypeCountry = new List<Account_Type_Country__c>();

    		Integer iCounter = 0;
    		for (Account_Type__c oAccountType : lstAccountType){

    			String tUniqueParent = 'Yes';

    			if (lstAccountTypeCountry_UniqueParent.size() > iCounter){
    				tUniqueParent = lstAccountTypeCountry_UniqueParent[iCounter];
    			}

				Account_Type_Country__c oAccountTypeCountry = new Account_Type_Country__c();
					oAccountTypeCountry.Account_Type__c = oAccountType.Id;
					oAccountTypeCountry.Country__c = clsTestData_MasterData.mapCountry.get(tAccountTypeCountry_ISOCode).Id;
					oAccountTypeCountry.Unique_Parent__c = tUniqueParent;
				lstAccountTypeCountry.add(oAccountTypeCountry);

    		}

			if (bInsert) insert lstAccountTypeCountry;

			oMain_AccountTypeCountry = lstAccountTypeCountry[0];
    	}


    	return lstAccountTypeCountry;
    }
    //---------------------------------------------------------------------------------------------------    


}