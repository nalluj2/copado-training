//------------------------------------------------------------------------------------------------------------------------------------------------------
//  Author      : Bart Caelen
//  Date        : 23/06/2015
//  Description : CR-5767
//      This class is the Controller Extension for the VF Page QuickCallRecord_setOpp which is used in SF1 to set the Opportunity of the Call Record
//          where the list of available Opportunities is filtered based on the related Accounts of the Call Record.
//      The applied filter is optional and can be removed by the end-user.
//------------------------------------------------------------------------------------------------------------------------------------------------------
public with sharing class ctrlExt_quickCallRecord_setOpp {

    //---------------------------------------------------------------------------
    // Constructor
    //---------------------------------------------------------------------------
    public ctrlExt_quickCallRecord_setOpp(ApexPages.StandardController stdController) {

        bSaveError = false;
        bRemoveFilter = false;

        oCallRecord = (Call_Records__c)stdController.getRecord();
        oCallRecord = [SELECT Id, Opportunity_ID__c FROM Call_Records__c WHERE Id = :oCallRecord.Id];

        tOpportunityLookupFilter = getOpportunityLookupFilter();

    }
    //---------------------------------------------------------------------------


    //---------------------------------------------------------------------------
    // Getters & Setters
    //---------------------------------------------------------------------------
    public Call_Records__c oCallRecord { get;set; }
    public String tOpportunityLookupFilter { 
        get;
        set{
            tOpportunityLookupFilter = value;
            tOpportunityLookupFilter_Escaped = String.escapeSingleQuotes(tOpportunityLookupFilter);
        }
    }
    public String tOpportunityLookupFilter_Escaped { get;set; }

    public Boolean bSaveError { get;set; }

    public Boolean bRemoveFilter { get;set; }
    //---------------------------------------------------------------------------


    //---------------------------------------------------------------------------
    // Actions
    //---------------------------------------------------------------------------
    public PageReference save(){

        bSaveError = false;
        try{
            clsUtil.bubbleException();

            update oCallRecord;

        }catch(Exception oEX){

            ApexPages.addMessages(oEX);
            bSaveError=true;
            if(Test.isRunningTest() == true) throw oEX;
        }

        return null;

    }

    public void toggleFilter(){

        bRemoveFilter = !bRemoveFilter;

        if (bRemoveFilter){
            tOpportunityLookupFilter = '';
        }else{
            tOpportunityLookupFilter = getOpportunityLookupFilter();
        }

    }
    //---------------------------------------------------------------------------


    //---------------------------------------------------------------------------
    // Private methods
    //---------------------------------------------------------------------------
    private String getOpportunityLookupFilter(){

        tOpportunityLookupFilter = '';

        // Get all Contact Visit Reports of the selected Call Record to be able to retrieve a set of Account ID's.
        //  The collected Account ID's will be used to filter down the available Opportunities
        List<Contact_Visit_Report__c> lstContactVisitReport = 
            [
                SELECT
                    Id, Attending_Contact__c, Attending_Affiliated_Account__c
                FROM
                    Contact_Visit_Report__c
                WHERE Call_Records__r.Id = :oCallRecord.Id
            ];
        Set<Id> setID_Account = new Set<Id>();
        for (Contact_Visit_Report__c oContactVisitReport : lstContactVisitReport){
            setID_Account.add(oContactVisitReport.Attending_Affiliated_Account__c);
        }

        for (Id id_Account : setID_Account){
            tOpportunityLookupFilter += '\'' + id_Account + '\',';
        }
        if (tOpportunityLookupFilter != ''){
            tOpportunityLookupFilter = tOpportunityLookupFilter.left(tOpportunityLookupFilter.length()-1);
            tOpportunityLookupFilter = ' WHERE AccountId in (' + tOpportunityLookupFilter + ')';
        }

        return tOpportunityLookupFilter;
            
    }
    //---------------------------------------------------------------------------

}
//------------------------------------------------------------------------------------------------------------------------------------------------------