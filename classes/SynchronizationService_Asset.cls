/*  Integration Service related class
    
    This class implements the specific logic for the synchronization of Assets in the Source Org.
*/
public without sharing class SynchronizationService_Asset implements SynchronizationService_Object{
    
    //Set with the external Ids of the records already processed in this transaction. Used to avoid generating multiple notifications for the same change
    private Set<String> processedRecords = new Set<String>();
    private Set<Id> rtInScope {
        
        get{    
            
            if(rtInScope == null){
                
                rtInScope = new Set<Id>();

                Map<String,Schema.RecordTypeInfo> assetRecordTypeInfo = Schema.SObjectType.Asset.getRecordTypeInfosByName();            
                rtInScope.add(assetRecordTypeInfo.get('ENT').getRecordTypeId());
                rtInScope.add(assetRecordTypeInfo.get('Midas').getRecordTypeId());              
                rtInScope.add(assetRecordTypeInfo.get('Navigation').getRecordTypeId());
                rtInScope.add(assetRecordTypeInfo.get('O-Arm').getRecordTypeId());
                rtInScope.add(assetRecordTypeInfo.get('PoleStar').getRecordTypeId());   
                rtInScope.add(assetRecordTypeInfo.get('Visualase').getRecordTypeId());
                rtInScope.add(assetRecordTypeInfo.get('Mazor').getRecordTypeId());   
                rtInScope.add(assetRecordTypeInfo.get('NewTom').getRecordTypeId());
                rtInScope.add(assetRecordTypeInfo.get('Pain').getRecordTypeId());
                rtInScope.add(assetRecordTypeInfo.get('Accessories').getRecordTypeId());
            }
            
            return rtInScope;
        }
        
        set;
    }
    
    //This method returns the External Ids of the records that have relevant modifications for the target Org. This includes created records or 
    //modified records where synchronized fields have changed. 
    public Set<String> hasChangeForNotification(List<SObject> records, Map<Id, SObject> oldRecords){
        
        Set<String> result = new Set<String>();
                
        for(SObject rawObject : records){
            
            Asset record = (Asset) rawObject;
            
            if( !rtInScope.contains(record.RecordTypeId) || processedRecords.contains(record.Serial_Nr__c) ) continue;
            
            //On Insert we always send notification
            if(oldRecords == null){
                result.add(record.Serial_Nr__c);
                processedRecords.add(record.Serial_Nr__c);
            }//If Update we only send notification when relevant fields are modified
            else{
                if( bl_SynchronizationService_Utils.hasChanged( record, oldRecords.get(record.Id), syncFields ) == true ){
                    result.add(record.Serial_Nr__c);
                    processedRecords.add(record.Serial_Nr__c);
                }
            }
        }
        
        return result;
    }
    
    //This method collects and returns the complete information for a record. This includes the records itself as well as any related record that needs
    //to exist in the target Org too. The returned format is JSON serialized string.
    public String generatePayload(String externalId){
        
        AssetSyncPayload payload = new AssetSyncPayload();
        
        String query =  'SELECT ' + String.join( syncFields, ',') + 
                        ',Account.SAP_Id__c, Account.AccountNumber, Account.RecordType.DeveloperName, Related_MazorX_SE_Camera_Cart__r.Serial_Nr__c' + 
                        ',Product2.CFN_Code_Text__c, RecordType.DeveloperName, Last_PM_Completed_By__r.Alias, Last_PM_Completed_By_Contact__r.External_Id__c, Hospital_Point_of_Contact__r.External_Id__c ' +
                        ' FROM Asset WHERE Serial_Nr__c = :externalId AND RecordType.DeveloperName IN (\'ENT\', \'Midas\', \'Navigation\', \'Accessories\', \'O_Arm\', \'PoleStar\', \'Visualase\', \'Pain\', \'Mazor\', \'NewTom\') LIMIT 2';
                        
        List<Asset> assets = Database.query(query);

        //If no record or more than 1 is found, we return an empty response
        if(assets.size() == 1){
            
            Asset asset = assets[0];    
            bl_SynchronizationService_Utils.prepareForJSON(asset, syncFields);      
            asset.Id = null;

            payload.SystemAsset = asset;
            
            Set<Id> accountIds = new Set<Id>();
            Set<Id> contactIds = new Set<Id>();
                    
            if(asset.Account != null){
                
                accountIds.add(asset.AccountId);
                
                if ( (asset.Account.RecordType.DeveloperName == 'SAP_Account') || (asset.Account.RecordType.DeveloperName == 'Distributor') ){
                    payload.AccId = asset.Account.SAP_Id__c;
                }else{
                    payload.AccId = asset.Account.AccountNumber;
                }
                asset.Account = null;//The local SFDC Id is not relevant in the target Org              
                asset.AccountId = null;             
            }
                        
            if(asset.Product2 != null){
                
                payload.ProductId = asset.Product2.CFN_Code_Text__c;            
                asset.Product2 = null;
                asset.Product2Id = null;    
            }
            
            if(asset.Last_PM_Completed_By__r != null){
                
                payload.LastPMById = asset.Last_PM_Completed_By__r.Alias;
                asset.Last_PM_Completed_By__r = null;
                asset.Last_PM_Completed_By__c = null;
            }
            
            if(asset.Last_PM_Completed_By_Contact__r != null){
                
                contactIds.add(asset.Last_PM_Completed_By_Contact__c);
                
                payload.LastPMByContactId = asset.Last_PM_Completed_By_Contact__r.External_Id__c;
                asset.Last_PM_Completed_By_Contact__r = null;
                asset.Last_PM_Completed_By_Contact__c = null;
            }
            
            if(asset.Hospital_Point_of_Contact__r != null){
                
                contactIds.add(asset.Hospital_Point_of_Contact__c);
                
                payload.hospitalPointOfContactId = asset.Hospital_Point_of_Contact__r.External_Id__c;
                asset.Hospital_Point_of_Contact__r = null;
                asset.Hospital_Point_of_Contact__c = null;
            }
            
            if(asset.Related_MazorX_SE_Camera_Cart__r != null){
                
                payload.relatedCameraCartId = asset.Related_MazorX_SE_Camera_Cart__r.Serial_Nr__c;
                asset.Related_MazorX_SE_Camera_Cart__r = null;
                asset.Related_MazorX_SE_Camera_Cart__c = null;
            }

            //Record Type                       
            payload.RecordType = asset.RecordType.DeveloperName;
            asset.RecordTypeId = null;
            asset.RecordType = null;    
            
            //Done in a separate query because we cannot clear the value from the CreatedDate field before sending it to the target
            Asset assetDetails = [Select CreatedDate, CreatedBy.Name, LastModifiedDate, LastModifiedBy.Name from Asset where Serial_Nr__c = :externalId AND RecordType.DeveloperName IN ('ENT', 'Midas', 'Navigation', 'Accessories', 'O_Arm', 'PoleStar', 'Visualase', 'Pain', 'Mazor', 'NewTom')];
                        
            payload.CreatedDate = assetDetails.CreatedDate;         
            payload.CreatedBy = assetDetails.CreatedBy.Name;                    
            payload.LastModifiedDate = assetDetails.LastModifiedDate;           
            payload.LastModifiedBy = assetDetails.LastModifiedBy.Name;                  
            
            payload.generateAccountPayload(accountIds, contactIds);
        }
        
        return JSON.serialize(payload);
    }
    
    //This methods takes the serialized record information and applies it localy to keep the records in sync between source and target
	public String processPayload(String inputPayload){
		
		String processedPayload = bl_SynchronizationService_Utils.translatePayload(inputPayload, fieldMapping);
		
		AssetSyncPayload payload = (AssetSyncPayload) JSON.deserialize(processedPayload, AssetSyncPayload.class);
		
		//If there was a problem in the source generating the information (no record or multiple records found) we finish here 
		if(payload.SystemAsset == null) return null;
						
		//Manual External Id mapping
		List<Asset> assets = [Select Id from Asset where Serial_Nr__c = :payload.systemAsset.Serial_Nr__c 
										AND RecordType.DeveloperName IN ('Navigation', 'Accessories', 'O_Arm', 'PoleStar', 'Pending_RTG_Asset', 'Visualase', 'Pain', 'Mazor', 'NewTom') LIMIT 2];
		
		//Because US side sends all the Assets independently of their region, we filter out the ones that are connected to Assets not found in INTL side.
		if(assets.size() == 0) return 'Asset not in INTL'; // Finish as fake success
		else if(assets.size() > 1) throw new bl_SynchronizationService_Utils.SyncService_Exception('Multiple Assets found for Serial Number : ' + payload.systemAsset.Serial_Nr__c );
			
		SavePoint sp = Database.setSavePoint();
		
		try{
			
			Asset asset = payload.SystemAsset;
			asset.Id = assets[0].Id;						
			
			asset.LastModifiedDate__c = payload.LastModifiedDate;
			asset.LastModifiedBy__c = payload.LastModifiedBy;
								
			upsert asset;
			
			return asset.Id;
									
		}catch(Exception e){
									
			Database.rollback(sp);
			
			throw e;
		}
		
		return null;		
	}
    
    //Model classes for the requests and responses. To be used by the JSON serialize/deserialize
    public class AssetSyncPayload extends SynchronizationService_Payload{
        
        public Asset systemAsset {get; set;}
        public String accId {get; set;}
        public String productId {get; set;}
        public String lastPMById {get; set;}   
        public String LastPMByContactId {get; set;}
        public String hospitalPointOfContactId {get; set;} 
        public String recordType {get; set;}
        public String relatedCameraCartId {get; set;}
        public Datetime createdDate {get; set;}
        public String createdBy {get; set;}
        public Datetime lastModifiedDate {get; set;}
        public String lastModifiedBy {get; set;}    
    }   
    
    private static Map<String, String> fieldMapping = new Map<String, String>		
	{		
		'System__c' => 'Asset',		
		'Name' => 'Serial_Nr__c',
		'System_Type__c' => 'Asset_Product_Type__c'
	};
    
    private static List<String> syncFields = new List<String>
    {
        'AccountId',
        'Accreditation_Consultant__c',
        'Active_Compensation_System__c',
        'Advanced_Viewing__c',
        'Asset_Product_Type__c',
        'Auto_Application_Restart__c',
        'AxiEM_Included__c',
        'AxiEM_Type__c',
        'Camera_Serial_Number__c',
        'Cedera_License__c',
        'Chiller_Serial_Number__c',
        'Chiller_Style__c',
        'Collimator_Serial_Number__c',
        'Computer_Name__c',  
        'Computer_Serial_Number__c',
        'Computer_Type__c',      
        'Contract_End_Date__c',
        'Contract_Manufacturer_Transfer_Date__c',
        'Cost_Per_Incident__c',
        'CurrencyISOCode',
        'Customer_Warranty_Start_Date__c',
        'Customer_Warranty_End_Date__c',
        'Default_Gateway__c',
        'Delivery_Date__c',
        'Description',
        'Detector_Serial_Number__c',
        'Digitzer_Serial_Number__c',
        'DNS_Server_1__c',
        'DNS_Server_2__c',
        'DNS_Server_3__c',
        'Dose_Reporting__c',
        'EM_Expiration_Date__c',
        'EM_Purchase_Date__c',
        'EM_Serial_Number__c',
        'Enhanced_Cranial_3D__c',
        'EOGS_Date__c',
        'EOL_Notes__c',
        'EOL_Return_Status__c',
        'EOL_Upgrading_to__c',
        'Flight_Requierd__c',
        'Floor_Material__c',
        'Gantry_Style__c',
        'Generator_Serial_Number__c',
        'Generator_Serial__c',
        'GFS_P_Code__c',
        'Gradient_Amps_Serial_Number__c',
        'Grid_Type__c',        
        'HD3D__c',
        'Head_Holder_Type__c',
        'Hospital_IP__c',
        'Hospital_Point_of_Contact__c',
        'Hospital_subnet_mask__c',
        'Hospital_URL__c',
        'Hub_Black__c',
        'Hub_Serial_Blue_Enhanced__c',
        'Image_Stitching__c',
        'InstallDate',  
        'IP_Address__c',      
        'ISO_Wag__c',
        'Language__c',
        'Laser_Alignment__c',
        'Last_PM_Completed_By__c',
        'Last_PM_Completed_By_Contact__c',
        'Last_PM_Completed_Date__c',
        'Last_PM_Was_Due_Date__c',
        'Low_Dose_Settings__c',   
        'MAC_Address__c',     
        'Mag_Collimated_3D__c',
        'Magnetic_Floor__c',
        'Medical_Physics_Provided_By__c',
        'Monitor_Serial_Number__c',
        'Multiple_Field_of_View__c',
        'MVS_Computer_Type__c',
        'Navigation_Server_Selection__c',
        'Navigation_Type__c',
        'NewTom_Warranty_Term__c',
        'Number_of_PM_s_per_year__c',
        'PurchaseDate',
        'OR_Phone_Number__c',
        'OR_Table_Type__c',
        'Other_Accreditation_Consultant__c',
        'Other_Medical_Physics_Provided_By__c',
        'Ownership_Status__c',
        'PAXSCAN_Serial_Number__c',
        'Phantom_Style__c',
        'PM_Notes__c',
        'Printer__c',
        'Product2Id',
        'Radiology_Interface__c',
        'Radius_Customer__c',
        'Radius_Product__c',
        'RBT_Serial__c',
        'RecordTypeId',
        'Related_MazorX_SE_Camera_Cart__c',
        'Remote_Presence_Enabled__c',
        'Remote_Presence_Notes__c',
        'Removal_Date__c',
        'RF_Amps_Serial_Number__c',
        'RF_Frequency__c',
        'Sales_price__c',
        'Serial_Nr__c',
        'Shield_Type__c',
        'Shipment_Date__c',
        'Site_Constructor__c',
        'SNAP__c',
        'Sold_as_Refurbished__c',
        'Software_Version_US__c',
        'Status',
        'StealthLink__c',
        'Stereotaxy__c',
        'Subnet_Mask__c',
        'Surgical_Arm_Serial_Number__c',
        'Surgical_Arm_Type__c',
        'Surgical_System_Serial_Number__c',
        'Surgical_System_Type__c',
        'System_Alert__c',
        'SW_Licenses__c',
        'Table_Adapter_Style__c',
        'TCA_Serial_Number__c',
        'Torque_Driver_Serial_Number__c',
        'Tube_Serial_Number__c',
        'Upgraded_MazorX__c',
        'Upgraded_Date_MazorX__c',
        'VGA_Out__c',
        'Voltage_and_Frequency__c',

        'Ethernet_Location__c',
        'Laser_Serial_Number__c',
        'Laser_Type__c',
        'Monitor_2_Serial_Number__c',
        'Pump_Serial_Number__c',
        'Storage_Area__c',
        'SW_CAPA_Justification__c',

        'Camera_Cart__c',
        'Camera_Cart_Expiration_Date__c',
        'Camera_Cart_Purchase_Date__c',
        'Camera_Cart_Serial_Number__c',
        'Camera_Type__c',
        'Checkpoint_Firewall_Router__c',
        'SCU__c',
        'Storage_Bin__c',
        'Leica_ARveo_HUD__c',
        'Leica_ARveo_Pointer_Only__c',
        'Leica_OH4_HUD__c',
        'Leica_OH4_Pointer_Only__c',
        'Leica_OH5_HUD__c',
        'Leica_OH5_Pointer_Only__c',
        'Leica_OH6_OHX_HUD__c',
        'Leica_OH6_OHX_Pointer_Only__c',
        'StealthRobotics__c',
        'Zeiss_Pentero__c',
        'Aloka__c',
        'Sonosite__c',

        'Hospital_Users_Public_IP__c',
        'Hospital_users_Private_IP__c',
        'LDAP_Private_IP__c',
        'LDAP_Public_IP__c',
        'PACS_Private_IP__c',
        'PACS_Public_IP__c',
        'PACs_Name_AE_Title__c',
        'Networking_Notes__c',
        
        'CU_Serial_Number__c',
        'Firmware_Version__c',
        'TP_Serial_Number__c',

		'Surgical_System_Part_number__c',
		'Surgical_Arm_Part_number__c',
		'Electrical_Panel_Part_number__c',
		'Pneumatic_Panel_Part_number__c',
		'Camera_Part_number__c',
		'Computer_Part_number__c',
		'RBT_Part_number__c',
		'Controller_Part_number__c',
		'Used_for_Brain__c',
		
		'X2D_Long_Film__c'

    };
}