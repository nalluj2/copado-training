@isTest
private class Test_ctrl_TimeReportingKronosView {
		
	private static testmethod void testKronosDisplay(){
		
		Project__c release = createSampleRelease();
		Reporting_Activity__c activity = createSampleActivity(false);						
		Time_Reporting__c report = createSampleReport(release, activity);
		
		Test.startTest();
		
		ctrl_TimeReportingKronosView controller = new ctrl_TimeReportingKronosView();
		
		System.assert(controller.kronosKeys.size()==1);
		System.assert(controller.weekTotal==8);
						
		controller.nextWeek();
		System.assert(controller.kronosKeys.size()==0);
		
		controller.prevWeek();
		
		controller.changeContextUser();		
	}
	
	/*
		HELPER METHODS
	*/
	private static Project__c createSampleRelease(){
		
		Time_Registration_Code__c regCode = new Time_Registration_Code__c();
		regCode.Name='projCode';
		regCode.Time_Registration_Category__c = 'catCode';
		regCode.Active__c = true;
		regCode.Business_Unit__c = 'Test BU';
		regCode.Time_Registration_ID__c = 'TRC';
				
		insert regCode;
		
		Project__c release = new Project__c();
		release.Name='Test Release';
		release.Start_Date__c=Date.today();
		release.Project_Type__c='Minor Release';
		release.Status__c='Scheduled';
		release.Project_Manager__c=UserInfo.getUserId();
		release.End_Date__c=Date.today().addDays(10);
		release.Time_Registration_Project__c = regCode.Id;
		//release.Kronos_Category__c='test category Id';
		//release.Kronos_Project__c='test project Id';
		release.Total_Budget__c=1000;
		
		insert release;
		
		return release;
	}
	
	private static Reporting_Activity__c createSampleActivity(Boolean manualInput){
		
		Reporting_Activity__c activity = new Reporting_Activity__c();
		activity.Name = 'Test Activity';
		activity.Requires_Manual_Input__c = manualInput;
		activity.chargeable__c = true;
		
		insert activity;
		
		return activity;
	}
	
	private static Time_Reporting__c createSampleReport(Project__c release, Reporting_Activity__c activity){
		
		Time_Reporting__c report = new Time_Reporting__c();
		report.Reporting_Activity__c = activity.Id;
		report.Release__c = release.Id;
		report.Hours__c = 8;
		report.OwnerId = UserInfo.getUserId();
		report.Date__c = Date.today();
		
		insert report;
		
		return report;
	}
}