public class bl_Contract_Repository_Document_Version {

	@TestVisible
	private static Boolean unitTestRunInterface = false;
	
	public static void sendDeletionNotification(List<Contract_Repository_Document_Version__c> triggerNew, Map<Id, Contract_Repository_Document_Version__c> oldMap){
		
		List<Id> toSend = new List<Id>();
		
		for(Contract_Repository_Document_Version__c version : triggerNew){
			
			if(version.Marked_for_Deletion__c == true && oldMap.get(version.Id).Marked_for_Deletion__c != true && version.Documentum_Id__c != null){
				
				toSend.add(version.Id);
			}
		}
		
		if(Test.isRunningTest() && unitTestRunInterface == false) return;
				
		if(toSend.size() == 1) sendNotificationDelete(toSend[0]);
		else if(toSend.size() > 1){
			
			ba_Contract_Repository_Document_Version batch = new ba_Contract_Repository_Document_Version(toSend); 
        	Database.executeBatch(batch, 1);
		}
	}    
	
	@Future(callout = true)
	public static void sendNotificationDelete(String versionId){
		
		Contract_Repository_Document_Version__c version = [Select Id, Document__c from Contract_Repository_Document_Version__c where Id = :versionId];
		
		deleteVersion(version);
	}
	
	public static void deleteVersion(Contract_Repository_Document_Version__c version){
		
		Contract_Repository_Interface_Settings__c settings = Contract_Repository_Interface_Settings__c.getOrgDefaults();
		
		//If the attachment to be send is not the last version for the document, then error
		if(settings == null || Id.valueOf(settings.Org_Id__c) != UserInfo.getOrganizationId()){
			
			version.Interface_Error__c = 'No valid interface settings found';
			update version;
			
			return;			
		}
					
		try{
			
			Http service = new Http();
            
            HttpRequest request= new HttpRequest();
            request.setMethod('POST');
            request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
            request.setEndpoint(settings.End_Point_URL_Server__c + '/ws/MdtSalesforce_Contracts.pub.providers.ws:deleteContracts_WSDL/MdtSalesforce_Contracts_pub_providers_ws_deleteContracts_WSDL_Port');
            request.setTimeout(60000);
            request.setHeader('Authorization', 'BASIC ' + EncodingUtil.base64Encode(Blob.valueOf(settings.Username__c + ':' + settings.Password__c)));
            request.setBody(getMessageBody(version));

            // Send the request
            HttpResponse resp = service.send(request);

            // Get the response of the Callout and process it
            System.debug(resp.getBody());
            
            DeleteResponse deleteResp = parseResponse(resp);
                        
            if(deleteResp.isSuccess == true){
	    			    		    		
	    		if(version.Document__c == null) delete version;
	    		else{
	    			
	    			version.Is_Deleted__c = true;
	    			update version;
	    		}	    		
	    		
	    	}else{
	    		
	    		version.Interface_Error__c = deleteResp.errorMessage;
	    		update version;	    		
	    	}
        
		}catch(Exception e){
			
			version.Interface_Error__c = e.getMessage() + ' | ' + e.getStackTraceString();
			update version;
		}		
    }
    
    private static String getMessageBody(Contract_Repository_Document_Version__c version){
    	
    	String body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:mdt="http://msplwa115-pr.wby1-dev.medtronic.com/MdtSalesforce_Contracts.pub.providers.ws:deleteContracts_WSDL">';
		body +=	'<soapenv:Header/>';
		body +=	'<soapenv:Body>';
		body +=	'<mdt:deleteContracts>';
		body +=	'<deleteContractsInput>';			            
		body +=	'<id>' + version.Id + '</id>';						            
		body +=	'<config_name>clm_sfdc</config_name>';					            
		body +=	'<type>m_clm_doc</type>';
		body +=	'<userid/>';
		body +=	'</deleteContractsInput>';
		body +=	'</mdt:deleteContracts>';
		body +=	'</soapenv:Body>';
		body +=	'</soapenv:Envelope>';
		
		System.debug(body);
		
		return body;    	
    }
    
    private static DeleteResponse parseResponse(HttpResponse httpResp){
    	
    	DeleteResponse resp = new DeleteResponse();
    	
    	Dom.Document oDomDoc = new Dom.Document();
        oDomDoc.load(httpResp.getBody());
        Dom.XMLNode oDomXMLRoot = oDomDoc.getRootElement();

        if (httpResp.getStatusCode() == 200){
            // SUCCESS
            for (Dom.XMLNode oDomXMLnode_Body : oDomXMLRoot.getChildren()) {
                for (Dom.XMLNode oDomXMLnode_Response : oDomXMLnode_Body.getChildren()) {
                    for (Dom.XMLNode oDomXMLnode_Result : oDomXMLnode_Response.getChildren()) {
                        
                        String isSuccess = oDomXMLnode_Result.getChildElement('isSuccess', null).getText();
                        String msg = oDomXMLnode_Result.getChildElement('msg', null).getText();
						
						resp.isSuccess = Boolean.valueOf(isSuccess);
						resp.errorMessage = msg;                     
                    }
                }
            }

        }else{
            // FAILURE
            resp.isSuccess = false;
            resp.errorMessage = httpResp.getStatus();
        }
    	
    	return resp;
    }
    
    private class DeleteResponse{
    	
    	public Boolean isSuccess {get; set;}
    	public String errorMessage {get; set;}
    }
}