public without sharing class controllerDIB_StatusV2 {
	
	// Fiscal Months PickList values 
	private String months {get; set;}
	private String years  {get; set;}
	
	public DIBSubmitSummaryDTO [] completed_status {set; get;}

	public controllerDIB_StatusV2(ApexPages.Standardcontroller stdController){
		this.fillCompleted_status();	 
	}
	public String getMonths() {							            return months;					    	}
    public void setMonths(String ms) {			            		this.months = ms;					    }
        
    public String getYears() {							            return years;					    	}
    public void setYears(String ms) {			            			this.years = ms;					    }
	
	
	private Id currentUserId = System.Userinfo.getUserId();
		
	public class DIBSubmitSummaryDTO{
		public DIB_Submit_Summary__c dibSubmitSummary {set; get;}
		public String mySalesLink {set; get;}
		public String competitorsLink {set; get;}
		public String campaignsLink {set; get;}
		public DIBSubmitSummaryDTO(){
		
		}
		public DIBSubmitSummaryDTO(DIB_Submit_Summary__c dibSubmitSummary){
			this.dibSubmitSummary = dibSubmitSummary;
		}
	}
	
	/**
	 *
	 *		0.	Show the current quarter and the last quarter Period (Fiscal Month & Fiscal Year)	
	 *		1 a. Get all the existing status for the selected User, selected Fiscal Year for Allocations, for Competitors and for Campaigns. 
	 *		1 b. Get all the Accounts that should exist. for each module : - Comeptitor & Campaigns : getAccountsFromSegmentation
	 *													  				   - Allocation : submitSales 	 
	 *		2. Logic For each Fiscal Month, check if everything is done or not. Only check if the accounts that shouls exist really exist or not. 
	 *		3. Pick the right color = RED : No accounts found, ORANGE : at least one account is found ; GREEN : All the accounts are done. 
	 *		4. Draw the matrix. 1 records per row. Just need to create all the records virtually. 		
	 *		5. Assign it to Completed Status List and show it. 
	 *
	 **/
	
	public void fillCompleted_status( ) {
		
		/**
		 CASE(Campaign__c, "Done", "https://emea.salesforce.com/servlet/servlet.ImageServer?id=015R00000005xnw&oid=00DR0000000Dhvd&lastMod=1251989518000", 
		 "Started", "https://emea.salesforce.com/servlet/servlet.ImageServer?id=015R00000005xo6&oid=00DR0000000Dhvd&lastMod=1251989793000", 
		 "Notstarted", "https://emea.salesforce.com/servlet/servlet.ImageServer?id=015R00000005xo1&oid=00DR0000000Dhvd&lastMod=1251989793000", "/s.gif")
		**/
		 	
		final String GREEN_FLAG  	= FinalConstants.GREEN_FLAG;
		final String ORANGE_FLAG 	= FinalConstants.ORANGE_FLAG; 
		final string RED_FLAG 		= FinalConstants.RED_FLAG; 
		
		// Get the current fiscal months and current fiscal years. 
		List<SelectOption> periods = DIB_AllocationSharedMethods.getPeriodLines()	;		
		system.debug('##########################################Period ' + periods ) ; 
		String[] fiscalPeriod = new String[]{} ; 		
		for (SelectOption so : periods){
			fiscalPeriod.add(so.getValue());				 	
		}
			
		system.debug('##########################################fiscalPeriod ' + fiscalPeriod ) ;
		
		// Query the existing Status for the selected Period ( each matched fiscal Month & Fiscal Year )
		String query = 'SELECT d.Sales__c, d.Sales_Rep__c, d.Fiscal_Year__c, d.Fiscal_Month__c, d.Competitor_Sales__c, d.Campaigns__c '; 
		query += ' FROM DIB_Submit_Summary__c d	WHERE Sales_Rep__c =\'' +  currentUserId + '\'';     
	    Boolean firstTime = true ; 
		for (string s : fiscalPeriod){					
			if (firstTime){
				query += ' AND (' ; 
			}else{
				query += ' OR ' ; 
			}
			query += '(d.fiscal_Month__c =\'' + s.substring(0,2) +'\''; 
			query += ' AND d.Fiscal_Year__c =\'' + s.substring(3,7) + '\')'; 
			firstTime = false ; 
		}	
		if (fiscalPeriod.size() > 0 ){	
			query +=')';
		} 		
		
		System.debug('########### query' + query ) ; 
		DIB_Submit_Summary__c[] allStatus = Database.query(query) ;
		
		System.debug(' ################## ' + 'controllerDIB_Status_fillCompleted_allStatus: ' + allStatus + ' ################'); 
		System.debug(' ################## ' + 'controllerDIB_Status_fillCompleted_allStatus.size(): ' + allStatus.size() + ' ################');
		
		List <DIBSubmitSummaryDTO> buildStatus = new List<DIBSubmitSummaryDTO>();			
		for (string s : fiscalPeriod){
			system.debug('##########################################s ' + s ) ;
			// New row in the list : Create a dummy record with default values. 
			DIB_Submit_Summary__c st = new DIB_Submit_Summary__c() ; 
			DIBSubmitSummaryDTO stDTO = new DIBSubmitSummaryDTO();
			String strFiscalMonth = s.substring(0,2); 
			String strFiscalYear = s.substring(3,7) ; 
			Integer intFiscalMonth = Integer.valueOf(strFiscalMonth);
			Integer intFiscalYear = Integer.valueOf(strFiscalYear);
			st.Fiscal_Month__c = strFiscalMonth; 
			st.Fiscal_Year__c = strFiscalYear; 
			st.System_Field_Sales_Flag__c = RED_FLAG ;
			st.System_Field_Competitor_Flag__c = RED_FLAG ; 
			st.System_Field_Campaign_Flag__c = RED_FLAG ; 
			st.System_Field_Month_Name__c = DIB_AllocationSharedMethods.getFiscalMonthName(st.Fiscal_Month__c);
			st.Sales_Rep__c = currentUserId;

			for (DIB_Submit_Summary__c dss :allStatus){
				system.debug('########################################## dss: ' + dss) ;
				if (st.Fiscal_Month__c == dss.Fiscal_Month__c && st.Fiscal_Year__c == dss.Fiscal_Year__c && st.Sales_Rep__c == dss.Sales_Rep__c) {
					if (dss.Sales__c == true) {
						st.System_Field_Sales_Flag__c = GREEN_FLAG;
						st.Sales__c = true;
						system.debug('########################################## Sales__c_true') ;
					}
					if (dss.Campaigns__c == true) {
						st.System_Field_Campaign_Flag__c = GREEN_FLAG;
						st.Campaigns__c = true;
						system.debug('########################################## Campaigns__c_true') ;
					}
					if (dss.Competitor_Sales__c == true) {
						st.System_Field_Competitor_Flag__c = GREEN_FLAG;
						st.Competitor_Sales__c = true;
						system.debug('########################################## Competitor_Sales__c_true') ;
					}
				}
			}
			stDTO.dibSubmitSummary = st;

			String chosenPeriod = strFiscalMonth + '-' + strFiscalYear;

			String strProfileId = Userinfo.getProfileId();
			System.Debug('########################################## strProfileId ' + strProfileId);
			Boolean showTab1 = false;
			for(String profileId:FinalConstants.SECURITY_MYSALES_TAB_01){
				//System.Debug('########################################## profileId ' + profileId);
				if(strProfileId.equals(profileId)){
					showTab1 = true;
					break;
				}
			}
			if(showTab1){
				stDTO.mySalesLink = '/apex/DIB_AllocationScreen_V1?' + FinalConstants.PARAM_DEFAULT_CHOSEN_PERIOD + '=' + chosenPeriod;
			}else{
				stDTO.mySalesLink = '/apex/AllocationAggregatedScreen?' + FinalConstants.PARAM_DEFAULT_CHOSEN_PERIOD + '=' + chosenPeriod;
			} 
			stDTO.competitorsLink  = '/apex/DIB_Competitor_iBase?' + FinalConstants.PARAM_DEFAULT_CHOSEN_PERIOD + '=' + chosenPeriod;
			stDTO.campaignsLink  = '/apex/DIB_Campaign?' + FinalConstants.PARAM_DEFAULT_CHOSEN_PERIOD + '=' + chosenPeriod;
			
			buildStatus.add(stDTO);
		}
		system.debug('##########################################buildStatus ' + buildStatus ) ;
		this.Completed_status = buildStatus ; 				
	}	
	
}