//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 29-03-2017
//  Description      : APEX Test Class for tr_UserRequestComment and bl_UserRequestComment_Trigger
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_bl_UserRequestComment_Trigger {
	
	private static Create_User_Request__c oCreateUserRequest;

	@isTest static void createTestData() {

		// Create Create_User_Request__c (Support Request)
		oCreateUserRequest = new Create_User_Request__c();          
			oCreateUserRequest.Status__c = 'New';
			oCreateUserRequest.Application__c = 'Salesforce.com';
			oCreateUserRequest.Request_Type__c = 'Reset Password';          
			oCreateUserRequest.Requestor_Email__c = UserInfo.getUserEmail();     
		insert oCreateUserRequest;

	}
	
	@isTest static void test_updateCreateUserRequest() {
		
		//---------------------------------------------
		// Create Test Data
		//---------------------------------------------
		createTestData();
        Map<Id, User> mapUser = bl_CreateUserRequest.loadAdminUsers('Support_Portal_Admins');
        User oUser_Admin = mapUser.values()[0];

			oCreateUserRequest.Status__c = 'In Progress';
			oCreateUserRequest.Sub_Status__c = 'Assigned';
			oCreateUserRequest.Assignee__c = oUser_Admin.Id;
		update oCreateUserRequest;
		//---------------------------------------------


		//---------------------------------------------
		// Perform Test
		//---------------------------------------------
		Test.startTest();

		List<User_Request_Comment__c> lstUserRequestComment = new List<User_Request_Comment__c>();
		User_Request_Comment__c oUserReqeustComment1 = new User_Request_Comment__c();
			oUserReqeustComment1.User_Request__c = oCreateUserRequest.Id;
			oUserReqeustComment1.Comment__c = 'Test Comment 1';
			oUserReqeustComment1.From__c = 'Test User';
			oUserReqeustComment1.From_Email__c = 'test@medtronic.test';
		lstUserRequestComment.add(oUserReqeustComment1);
		User_Request_Comment__c oUserReqeustComment2 = new User_Request_Comment__c();
			oUserReqeustComment2.User_Request__c = oCreateUserRequest.Id;
			oUserReqeustComment2.Comment__c = 'Test Comment 2';
			oUserReqeustComment2.From__c = 'Test User';
			oUserReqeustComment2.From_Email__c = 'test@medtronic.test';
		lstUserRequestComment.add(oUserReqeustComment2);
		insert lstUserRequestComment;

		oCreateUserRequest = [SELECT Id, Sub_Status__c, User_Comment_Received__c FROM Create_User_Request__c WHERE Id = :oCreateUserRequest.Id];

		System.assertEquals(oCreateUserRequest.Sub_Status__c, 'Work in progress');
		System.assertEquals(oCreateUserRequest.User_Comment_Received__c, Date.today());

		System.runAs(oUser_Admin){
			User_Request_Comment__c oUserReqeustComment3 = new User_Request_Comment__c();
				oUserReqeustComment3.User_Request__c = oCreateUserRequest.Id;
				oUserReqeustComment3.Comment__c = 'Test Comment 3';
				oUserReqeustComment3.From__c = 'Test User';
				oUserReqeustComment3.From_Email__c = 'test@medtronic.test';
			insert oUserReqeustComment3;

			oCreateUserRequest = [SELECT Id, Sub_Status__c, User_Comment_Received__c FROM Create_User_Request__c WHERE Id = :oCreateUserRequest.Id];

			System.assertEquals(oCreateUserRequest.User_Comment_Received__c, null);
		}

		Test.stopTest();
		//---------------------------------------------


		//---------------------------------------------
		// Validate Result
		//---------------------------------------------
		//---------------------------------------------

	}
	
}
//--------------------------------------------------------------------------------------------------------------------