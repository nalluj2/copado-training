//------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 26-03-2019
//  Description      : APEX TEST Class for the APEX Controller ctrlExt_AddContractAccount
//  Change Log       : 
//------------------------------------------------------------------------------------------------------------
@isTest private class TEST_ctrlExt_AddContractAccount {

	@TestSetup static void createTestData() {

		// CREATE Account / Buying Group
		clsTestData_Account.iRecord_Account_SAPAccount = 5;
		clsTestData_Account.oMain_Account_SAPAccount = null;
		List<Account> lstAccountSAP1 = clsTestData_Account.createAccount_SAPAccount();

		clsTestData_Account.iRecord_Account = 10;
		clsTestData_Account.idRecordType_Account = clsUtil.getRecordTypeByDevName('Account','NON_SAP_Account').Id;
		clsTestData_Account.oMain_Account = null;
		List<Account> lstAccount = clsTestData_Account.createAccount();
		System.assertEquals(lstAccount.size(), 10);

		clsTestData_Account.idRecordType_Account = clsUtil.getRecordTypeByDevName('Account','CAN_Buying_Group').Id;
		clsTestData_Account.oMain_Account = null;
		clsTestData_Account.iRecord_Account = 1;
		clsTestData_Account.tCountry_Account = 'CANADA';
		List<Account> lstBuyingGroup = clsTestData_Account.createAccount(true);
		lstBuyingGroup[0].EUR_Buying_Group_ID_Text__c = 'BG999888777';
		update lstBuyingGroup;

		// Create Buying Group Affiliations
		List<Affiliation__c> lstAffiliation = new List<Affiliation__c>();
		for (Account oAccount : lstAccount){

			Affiliation__c oAffiliation = new Affiliation__c();
				oAffiliation.Affiliation_To_Account__c = lstBuyingGroup[0].Id;
				oAffiliation.Affiliation_From_Account__c = oAccount.Id;
				oAffiliation.RecordTypeId = clsUtil.getRecordTypeByDevName('Affiliation__c', 'A2A').Id;
				oAffiliation.Primary_Dealer__c = false;
				oAffiliation.Affiliation_Start_Date__c = Date.today();
				oAffiliation.Affiliation_Type__c = 'Member';
			lstAffiliation.add(oAffiliation);

		}
		insert lstAffiliation;
		System.assertEquals(lstAffiliation.size(), 10);


		// Create Contract XBU
		Contract_xBU__c oContract = new Contract_xBU__c();
			oContract.RecordTypeId = clsUtil.getRecordTypeByDevName('Contract_xBU__c', 'German_Contract').Id;
			oContract.Account__c = lstAccount[0].Id;
			oContract.Status__c = 'In Process C&P';
		insert oContract;


	}
	
	@isTest private static void test_ctrlExt_AddContractAccount() {

		//----------------------------------------------------------------
		// Create / Load Test Data
		//----------------------------------------------------------------
		List<Contract_xBU__c> lstContract = [SELECT Id, (SELECT Id FROM Contract_Accounts__r) FROM Contract_xBU__c];
		System.assertEquals(lstContract.size(), 1);
		System.assertEquals(lstContract[0].Contract_Accounts__r.size(), 0);

		List<Account> lstAccount_SAP = [SELECT Id, SAP_ID__c FROM Account WHERE RecordTypeId = :clsUtil.getRecordTypeByDevName('Account', 'SAP_Account').Id];
		System.assertEquals(lstAccount_SAP.size(), 5);
		String tSearchID = '';
		List<String> lstSAPID = new List<Id>();
		for (Account oAccount : lstAccount_SAP){
			tSearchID += oAccount.SAP_ID__c + ';';
		}
		tSearchID += 'BG999888777;ABCDEFG';
		//----------------------------------------------------------------


		//----------------------------------------------------------------
		// Perform Test
		//----------------------------------------------------------------
		Test.startTest();

		PageReference oPageRef = Page.addContractAccount;
        Test.setCurrentPage(oPageRef);

	        ApexPages.currentPage().getParameters().put('id', lstContract[0].Id);
		ctrlExt_AddContractAccount oCTRL = new ctrlExt_AddContractAccount(new ApexPages.Standardcontroller(lstContract[0]));

		oCTRL.tProcessingId = tSearchID;
		oCTRL.searchAccount();

		oCTRL.addAccountToContract();

		oCTRL.backToContract();

		Test.stopTest();
		//----------------------------------------------------------------


		//----------------------------------------------------------------
		// Validate Test
		//----------------------------------------------------------------
		lstContract = [SELECT Id, (SELECT Id FROM Contract_Accounts__r) FROM Contract_xBU__c];
		System.assertEquals(lstContract.size(), 1);
		System.assertEquals(lstContract[0].Contract_Accounts__r.size(), 15);
		//----------------------------------------------------------------

	}
}
//------------------------------------------------------------------------------------------------------------