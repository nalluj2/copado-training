/** * This class contains unit tests for TMA Apex classes.
 *  Created : 20111612 
 *  Description : Test Coverage for TMA Apex Classes. 
 *  Classes Covered : controllerTerritoryManagement, controllerTerritoryAssignAccounts, AddEditterritory 
 *  Author : Wipro / Dheeraj
 */
 @isTest (SeeAllData = true)
Private class Test_TMA {    
    static testMethod void Test_controllerTerritoryManagement() {
        System.debug('####################### BEGIN Test Coverage for controllerTerritoryManagement ######################### ') ;
                 

        // Get Company
        Company__c comp = [select id,name from Company__c where name='Europe' limit 1];                            
                             
        Territory2 Ter = [Select Id, Company__c, Territory_UID2__c from Territory2 where Country_UID__c = 'NL' AND Territory2Type.DeveloperName = 'Territory' AND Business_Unit__c = 'Neurovascular' LIMIT 1];

        User oUser_Admin = clsTestData_User.createUser_SystemAdministratorMDT('tstadm1', true);
        User oUser1 = clsTestData_User.createUser('tstusr1', false);
            oUser1.Territory_UID__c = Ter.Territory_UID2__c;
        insert oUser1;
        
        User currentUser = new User(Id = UserInfo.getUserId());
        
        List<Account> accounts = new List<Account>{} ;        
        System.runAs(oUser_Admin){
        	
            	ter.Company__c = String.valueOf(comp.Id).substring(0, 15);
        	update ter;        
                     
                DIB_Country__c country1 = new DIB_Country__c(Name='Country1',Country_ISO_Code__c='C1');
                DIB_Country__c country2 = new DIB_Country__c(Name='Country2',Country_ISO_Code__c='C2');
                DIB_Country__c[] countries = new DIB_Country__c[]{country1, country2};
            insert countries;
                    
            // Create Accounts
            for (Integer i = 0 ; i < 10 ; i++){           
                Account a = new Account() ;            
                a.Name = 'controllerTerritoryManagement ' + i  ;            
                a.SAP_ID__c = '04556665'+i ;            
                a.RecordTypeId = FinalConstants.sapAccountRecordTypeId ;           
                accounts.add(a) ;         
            }
            insert accounts ; 
            // End Create Accounts

            // Inserting Accounts to Account Share                   
            
            List <ObjectTerritory2Association> ToAssign = new List <ObjectTerritory2Association>();            
            for(Account Acc:accounts){                          
                ObjectTerritory2Association Accshare=new ObjectTerritory2Association();                
                Accshare.ObjectId = Acc.Id;                        
                Accshare.Territory2Id = Ter.Id;
                Accshare.AssociationCause ='Territory2Manual';                
                ToAssign.add(Accshare);            
            }              
            insert ToAssign;                        
            // End Inserting Accounts to Account Share
            
            Territory_Level_Configuration__c tlc = new Territory_Level_Configuration__c();  
            tlc.Job_Title_vs__c='Sales Rep';  
            tlc.Name='Territory';  
            tlc.Territory_Level__c=8;  
            tlc.Territory_Abbreviation__c='TR';  
            tlc.Message_No_Accounts__c='No Accounts';  
            tlc.Message_No_Child_Territories__c='No Child Territories';  
            tlc.Message_No_Employees__c='No Employees'; 
            tlc.Company__c  = ter.Company__c;
            insert tlc;       

        }  

        System.runAs(oUSer1){
            
            PageReference pageRef = Page.TerritoryManagement;  
            Test.setCurrentPage(pageRef);
            
            // Creating an instance of the controller class
            ApexPages.currentPage().getParameters().put('id', null);
                
            // Call controllerTerritoryManagement where TerId Parameters is null
            ApexPages.StandardController sct = new ApexPages.StandardController(Ter); //set up the standardcontroller              
            controllerTerritoryManagement  controller = new controllerTerritoryManagement(sct);
            controller.terrId = null;
            controller.getTDetail();
            controller.getTChildren();
            controller.getCrumbs();
            controller.getSiblings();
            controller.AssignedAccount();
            controller.AssignedEmployees();
            controller.setSearchText('Test1');
            controller.getSearchText();
            controller.GetEmpRecSize();
            controller.AssignedEmployees();
            controller.getTotalPagesEmp();
            controller.previousEmp();
            controller.nextEmp();
            controller.firstEmp();
            controller.lastEmp();
            controller.getHasNextEmp();
            controller.getHasPreviousEmp(); 
            // ApexPages.currentPage().getParameters().put('DelEmpId', u.Id);
            // controller.DeleteEmployee();
            controller.getAllTerritoriesIds();
            controller.setAllTerritoriesIds('abc');
           	controller.AssignFM();
           	controller.removeFM();
           	controller.removedEmployee();
            // Creating an instance of the controller class
            ApexPages.currentPage().getParameters().put('id', Ter.Id);
            
            // Call controllerTerritoryManagement with TerId Parameters
            ApexPages.StandardController sct1 = new ApexPages.StandardController(Ter); //set up the standardcontroller              
            controllerTerritoryManagement  controller1 = new controllerTerritoryManagement(sct1);
            
            controller1.GetAccRecSize();
            controller1.AccRecords = '10';
            controller1.GetAccRecSize();
            
            controller1.gettmaConfig();
            controller1.getTDetail();
            controller1.getTChildren();
            controller1.getCrumbs();
            controller1.getSiblings();
            controller1.DelTerritory();
            controller1.AssignedAccount();
            controller1.setSearchText1('Test');
            controller1.getSearchText1();
            controller1.AssignedAccount();
            controller1.getTotalPages();
            controller1.previous();
            controller1.next();
            controller1.first();
            controller1.last();
            controller1.getHasNext();
            controller1.getHasPrevious();
            controller1.doSearch();
            controller1.setSearchText('Test1');
            controller1.getSearchText();
            controller1.GetEmpRecSize();
            controller1.AssignedEmployees();
            controller1.getTotalPagesEmp();
            controller1.previousEmp();
            controller1.nextEmp();
            controller1.firstEmp();
            controller1.lastEmp();
            controller1.getHasNextEmp();
            controller1.getHasPreviousEmp(); 
            // Remove Account from Territory
            controller1.DelAccId = accounts[0].id;
//            controller1.DeleteAccount();        //- Too many SOQL Queries

        }
                        
    } // End Test_controllerTerritoryManagement
    
    static testMethod void Test_controllerTerritoryAssignAccounts(){
        PageReference pageRef = Page.TerritoryAssignAccounts;  
        Test.setCurrentPage(pageRef);
        Company__c comp = [select id,name from Company__c where name='Europe' limit 1];                            
        
        Territory2 ter = [Select Id, Company__c from Territory2 where Country_UID__c = 'NL' AND Territory2Type.DeveloperName = 'Territory' AND Business_Unit__c = 'Neurovascular' LIMIT 1];
        
        User currentUser = new User(Id = UserInfo.getUserId());
        
        System.runAs(currentUser){
        	
        	ter.Company__c = String.valueOf(comp.Id).substring(0, 15);
        	update ter;        
        }
        
        DIB_Country__c country1 = new DIB_Country__c(Name='Country1',Country_ISO_Code__c='C1');
        DIB_Country__c country2 = new DIB_Country__c(Name='Country2',Country_ISO_Code__c='C2');
        DIB_Country__c[] countries = new DIB_Country__c[]{country1, country2};
        insert countries;

         List<Account> accounts = new List<Account>{} ;        
         for (Integer i = 0 ; i < 10 ; i++){           
            Account a = new Account() ;            
            a.Name = 'controllerTerritoryManagement ' + i  ;            
            a.SAP_ID__c = '04556665'+i ;            
            a.RecordTypeId = FinalConstants.sapAccountRecordTypeId ;           
            accounts.add(a) ;         
        }
        insert accounts;
        
        ApexPages.currentPage().getParameters().put('TerId', Ter.Id);
        
        // Inserting Accounts to Account Share
            
        List <ObjectTerritory2Association> ToAssign = new List <ObjectTerritory2Association>();            
        for(Account Acc:accounts){                          
            ObjectTerritory2Association Accshare=new ObjectTerritory2Association();                
            Accshare.ObjectId = Acc.Id;                        
            Accshare.Territory2Id = Ter.Id;
            Accshare.AssociationCause ='Territory2Manual';                
            ToAssign.add(Accshare);           
        }              
        insert ToAssign;                        
        // End Inserting Accounts to Account Share

        Id profId = [select Id from Profile where name='System Administrator'].Id;
         
        Territory_Level_Configuration__c tlc = new Territory_Level_Configuration__c();  
        tlc.Job_Title_vs__c='Sales Rep';  tlc.Name='Territory';  
        tlc.Territory_Level__c=8;  tlc.Territory_Abbreviation__c='CN';  
        tlc.Message_No_Accounts__c='No Accounts';  
        tlc.Message_No_Child_Territories__c='No Child Territories';  
        tlc.Message_No_Employees__c='No Employees';  
        insert tlc;  
                   
        ApexPages.StandardController sct = new ApexPages.StandardController(Ter); //set up the standardcontroller              
        controllerTerritoryAssignAccounts  controller = new controllerTerritoryAssignAccounts(sct);
        controller.AssignedButtonPressed = false;  
        controller.getCrumbs();
        controller.GetAccRecSize();
        controller.getCountry();
        //controller.getCountries();
        controller.getTDetail();
        controller.getAvailableAccount();
        controller.getTotalPages();
        controller.previous();
        controller.next();
        controller.first();
        controller.last();
        controller.getHasNext();
        controller.getHasPrevious();
        controller.getSelectAccount();
        controller.SelAccId = accounts[0].Id;
        controller.SelectedAccount();

        controller.SelAccId = accounts[1].Id;
        controller.SelectedAccount();

        controller.RemoveSelAccId = accounts[1].Id;
        controller.RemoveSelectedAccount();

        controller.AccName='Test Account';
        controller.ShipCity='NL';
        controller.ShipPost='12345';
        controller.SapId='12345';
        controller.AvailableAccount1();
                
        controller.SelectAllAccounts();
        controller.AssignAccounts();
        
        controller.AccName='';
        controller.ShipCity='';
        controller.ShipPost='';
        controller.SapId='';
        controller.AvailableAccount1();
                
    } // End Test_controllerTerritoryAssignAccounts 
    
    static testMethod void Test_AddEditterritory(){
        User u, u2;
        Territory2 Ter=new Territory2(); 
        Territory2 Ter1=new Territory2();
        // Get Company
        Company__c comp = [select id,name from Company__c where name='Europe' limit 1];                            
        
        PageReference pageRef = Page.TerritoryManagement;  
        Test.setCurrentPage(pageRef);
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];    
        System.runAs ( thisUser ) {    
            Profile p = [select id from profile where name='System Administrator'];       
            
            Id MDT_Terr_Model = [Select Id from Territory2Model where DeveloperName = 'Medtronic'].Id;
                            
            // Create Territory           
              Ter.Territory2TypeId = bl_Territory.territoryTypeMap.get('Region');   
              Ter.Business_Unit__c='CRHF';
              Ter.Country_UID__c='C1'; 
              Ter.CurrencyIsoCode='EUR';
              Ter.Therapy_Groups_Text__c = 'CardioInsight; Conventional EP';
              Ter.Short_Description__c='CRHF';
              Ter.name = 'Test territory';
              Ter.DeveloperName = 'Unit_Test_territory';
              Ter.Company__c = string.valueOf(comp.id).substring(0,15);    
              Ter.Territory2ModelId = MDT_Terr_Model;          
              Insert Ter;   
              
            // Create Territory           
              Ter1.Territory2TypeId = bl_Territory.territoryTypeMap.get('SalesForce');   
              Ter1.Business_Unit__c='CRHF';
              Ter1.Country_UID__c='C1'; 
              Ter1.CurrencyIsoCode='EUR';
              Ter1.Therapy_Groups_Text__c = 'CardioInsight; Conventional EP';
              Ter1.Short_Description__c='CRHF';
              Ter1.name = 'Test territory';
              Ter1.DeveloperName = 'Unit_Test_territory_1';
              Ter1.Company__c = string.valueOf(comp.id).substring(0,15);     
              ter1.Territory2ModelId = MDT_Terr_Model;          
              Insert Ter1;  
              
            string t = [select Territory_UID2__c from Territory2 where id =:Ter.id].Territory_UID2__c;                              
            u = new User(alias = 'standt1', email='standarduser@testorg.com',             
                        emailencodingkey='UTF-8', lastname='AddEditterritory_Testing',             
                        languagelocalekey='en_US',             
                        localesidkey='en_US', profileid = p.Id,            
                        timezonesidkey='America/Los_Angeles',             
                        CountryOR__c='Country1',          
                        Country= 'Country1',            
                        username='AddEditterritory_standarduser1@testorg.com',
                        Territory_UID__c=t,
                        Alias_unique__c='standarduser_Alias_unique__c456',Company_Code_Text__c='EUR');         
                
            // End Create Territory
        }
        System.runAs(u) {
        // Creating Test Data Like BU, SBU, Therapy & TherapyGroup etc.
            //Insert Company
            Company__c cmpny = new Company__c();
            cmpny.name='TestMedtronic';
            cmpny.CurrencyIsoCode = 'EUR';
            cmpny.Current_day_in_Q1__c=56;
            cmpny.Current_day_in_Q2__c=34; 
            cmpny.Current_day_in_Q3__c=5; 
            cmpny.Current_day_in_Q4__c= 0;   
            cmpny.Current_day_in_year__c =200;
            cmpny.Days_in_Q1__c=56;  
            cmpny.Days_in_Q2__c=34;
            cmpny.Days_in_Q3__c=13;
            cmpny.Days_in_Q4__c=22;
            cmpny.Days_in_year__c =250;
            cmpny.Company_Code_Text__c = 'T18';
            insert cmpny;        
        
            Business_Unit__c BU = new Business_Unit__c();
            BU.Name = 'CRHF123'; 
            BU.abbreviated_name__c='Test1';
            bu.Company__c =comp.id;//cmpny.id;
            insert BU;
            
            Sub_Business_Units__c SBU = new Sub_Business_Units__c();
            SBU.Name = 'CRHF123'; 
            SBU.Business_Unit__c=BU.Id;
            insert SBU;
            
            Therapy_Group__c THG = New Therapy_Group__c();
            THG.Name='ENT';
            
            THG.Sub_Business_Unit__c = SBU.Id;
            THG.Company__c = comp.id;//cmpny.id;      
            Insert THG;
            
            Therapy__c TH = New Therapy__c();
            TH.Name='ENT';
            TH.Active__c=true;
            
            TH.Business_Unit__c=BU.Id;
            TH.Sub_Business_Unit__c=SBU.ID;
            TH.Therapy_Group__c=THG.Id;
            TH.Therapy_Name_Hidden__c ='ENTTest'; 
            Insert TH;
            
            Therapy_Group__c THG1 = New Therapy_Group__c();
            THG1.Name='Test';
            THG1.Sub_Business_Unit__c = SBU.Id;            
            THG1.Company__c = cmpny.id;      
            Insert THG1;
            
            Therapy__c TH1 = New Therapy__c();
            TH1.Name='Test';
            TH1.Active__c=true;
            TH1.Business_Unit__c=BU.Id;
            TH1.Sub_Business_Unit__c=SBU.ID;
            TH1.Therapy_Group__c=THG.Id;
            TH1.Therapy_Name_Hidden__c = 'Test123';
            Insert TH1;

            Territory_Level_Configuration__c tlc = new Territory_Level_Configuration__c();  
            tlc.Job_Title_vs__c='Sales Rep';  tlc.Name='Country8';  
            tlc.Territory_Level__c=8;  tlc.Territory_Abbreviation__c='CN';  
            tlc.Message_No_Accounts__c='No Accounts';  
            tlc.Message_No_Child_Territories__c='No Child Territories';  
            tlc.Message_No_Employees__c='No Employees';  
            tlc.Company__c  = comp.id;            
            insert tlc;  
                        
        // End: Creating Test Data
        ApexPages.currentPage().getParameters().put('TerId', Ter.Id);
        // Test Coverage for Add Territory as Edit is null
        ApexPages.currentPage().getParameters().put('Edit', null);
        
        ApexPages.StandardController sct = new ApexPages.StandardController(Ter); //set up the standardcontroller              
        AddEditterritory  controller = new AddEditterritory(sct);
        controller.getCrumbs();
 
        ApexPages.StandardController sct2 = new ApexPages.StandardController(Ter1); //set up the standardcontroller              
        AddEditterritory  controller2 = new AddEditterritory(sct2);
        
        controller2.getAvailableTherapy();
        controller2.getSeletedTherapy();        
        controller2.setSeletedTherapies(THG.Id);
        controller2.RemoveTherapy();
        controller2.getAvailableTherapy(); 
        controller2.getSeletedTherapy();

        controller2.setAvailableTherapies(THG1.Id);
        controller2.AddTherapy();
        controller2.getAvailableTherapy();
        controller2.getSeletedTherapy();
        User thisUser1 = [ select Id from User where Id = :UserInfo.getUserId() ];    
        System.runAs ( thisUser1 ) {
            try{        
                Controller2.CreateTerritory();
            } catch(exception e){ }
        }
        
        ApexPages.currentPage().getParameters().put('TerId', Ter.Id);
        // Test Coverage for Add Territory as Edit is not null
        ApexPages.currentPage().getParameters().put('Edit', '1');

        ApexPages.StandardController sct1 = new ApexPages.StandardController(Ter1); //set up the standardcontroller              
        AddEditterritory  controller1 = new AddEditterritory(sct1);
        Controller1.getUnAvailableTherapy();
        
        controller1.getAvailableTherapy();
        controller1.getSeletedTherapy();
        
        controller1.setSeletedTherapies(THG.Id);
        controller1.RemoveTherapy();
        controller1.getAvailableTherapy();
        controller1.getSeletedTherapy();

        controller1.setAvailableTherapies(THG1.Id);
        controller1.AddTherapy();
        controller1.getAvailableTherapy();
        controller1.getSeletedTherapy();
        User thisUser2 = [ select Id from User where Id = :UserInfo.getUserId() ];    
        System.runAs ( thisUser2 ) {
            try {                
                Controller1.CreateTerritory();
            } catch(exception e){ }
            
        }
        // Misc Code
        controller1.getTerr1(); 
        controller1.setTerr1(Ter);
        controller1.getAvailableTherapies();
      }      
        
    } // End Test_AddEditterritory  
    
    
} // End Test_TMA