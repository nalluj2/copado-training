/*
 *      Description : This class is the Controller for the VF Page AccountAssetList which will display the Assets for the selected Account.
 *          It uses the VF Component RelatedList.
 *      Version  =  1.0
 *      Author   =  Bart Caelen
 *      Date     =  01/2014
 * ----------------------------------------------------------------------------------------------------------------------------------
 *      Description : CR-3776 
 *      Version  =  1.1
 *      Author   =  Bart Caelen
 *      Date     =  2014-07-31
*/
public class ctrlExt_AccountAssetList {

    //------------------------------------------------------------------------------------------------------------------------------------
    // PRIVATE VARIABLES
    //------------------------------------------------------------------------------------------------------------------------------------
    private Account oAccount;
    private boolean bUserIsAgent = false;
    private string tUser_BU = '';
    private string tUser_SBU = '';
    //------------------------------------------------------------------------------------------------------------------------------------
    

    //------------------------------------------------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //------------------------------------------------------------------------------------------------------------------------------------
    public ctrlExt_AccountAssetList(ApexPages.StandardController oStdController){
        
        if (!Test.isRunningTest()){
            List<String> lstField = new List<String>{'Account_Country_vs__c', 'Name'};
            oStdController.addFields(lstField);
        }
        oAccount = (Account)oStdController.getRecord();
        
        owrRelatedList = new wr_RelatedList();
            owrRelatedList.tSObjectName = 'Asset';
            owrRelatedList.tMasterFieldName = 'AccountID';
            owrRelatedList.tMasterRecordID = oAccount.Id;

            owrRelatedList.tTitle = 'Assets';
            owrRelatedList.tSubTitle = oAccount.Name;

            owrRelatedList.tTitleNewData = Label.Asset_Request_Button;
            owrRelatedList.tJSNewData = 'jsAddNewData';

            owrRelatedList.bShowDebug = false;
            owrRelatedList.bShowAddNewButton = true;
            owrRelatedList.tAdditionalWherePart = '';

            owrRelatedList.bShowExportButton = true;
            owrRelatedList.tExportType = 'XLS';
            
            owrRelatedList.bShowBackToParent = true;
        
        bUserIsAgent = (Userinfo.getUserType() == 'PowerPartner') ? true : false;
        if (bUserIsAgent){
            User oUser = [SELECT UserType, User_Business_Unit_vs__c, User_Sub_Bus__c, Email FROM User WHERE Id = :UserInfo.getUserId()];     
            
            tUser_BU = clsUtil.isNull(oUser.User_Business_Unit_vs__c, '');
            tUser_SBU = '';
            
            tUser_SBU = '(';
            string tUser_SBUs = clsUtil.isNull(oUser.User_Sub_Bus__c, '');
            list<string> lstSplittedSBU = tUser_SBUs.split(';');
            for (integer i = 0 ; i < lstSplittedSBU.size() ; i++){
                tUser_SBU += '\'' + lstSplittedSBU[i] + '\',';                     
            }
            tUser_SBU = tUser_SBU.substring(0, tUser_SBU.length()-1);   // remove the last ","
            tUser_SBU += tUser_SBU + ')';
            
            owrRelatedList.tAdditionalWherePart += '('; 
                owrRelatedList.tAdditionalWherePart += '( Business_Unit_Name_Text__c = ' + '\'' + tUser_BU + '\'' + ' AND Sub_Business_Unit__c IN ' + tUser_SBU + ')';
                owrRelatedList.tAdditionalWherePart += ' OR ( Business_Unit_Name_Text__c = null AND Sub_Business_Unit__c = null)';
            owrRelatedList.tAdditionalWherePart += ')';                     
        }

    }
    //------------------------------------------------------------------------------------------------------------------------------------


    //------------------------------------------------------------------------------------------------------------------------------------
    // GETTERS & SETTERS
    //------------------------------------------------------------------------------------------------------------------------------------
    public wr_RelatedList owrRelatedList { get; set; }
    //------------------------------------------------------------------------------------------------------------------------------------


    //------------------------------------------------------------------------------------------------------------------------------------
    // ACTIONS
    //------------------------------------------------------------------------------------------------------------------------------------
    public PageReference createNewData(){

        PageReference oPageReference;

            oPageReference = new PageReference('/apex/CreateAssetRequest?id=' + oAccount.Id);
            oPageReference.setRedirect(true);

        return oPageReference;

    }
    //------------------------------------------------------------------------------------------------------------------------------------
}
//------------------------------------------------------------------------------------------------------------------------------------