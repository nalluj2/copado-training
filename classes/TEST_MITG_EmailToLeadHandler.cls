//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Name    : TEST_MITG_EmailToLeadHandler 
// Author  : Bart Caelen
// Date    : 20/01/2017
// Purpose : APEX Test Class for APEX Class MITG_EmailToLeadHandler and bl_InboundEmailHandler_MITG
// Dependencies: 
//
// ==================================================
// = MODIFICATION HISTORY 							=
// ==================================================
// DATE        	AUTHOR           	CHANGE
// 29/06/2017	Bart Caelen 		CR-14226 - Added test for AdobeConnect and ManualEmail Emails
//
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_MITG_EmailToLeadHandler {

	private static List<Contact> lstContact;
	private static Set<Id> setID_Contact;

	private static String tEmailAddress = 'test@medtronic.com.test';
	private static String tEmailAddress_NotExisting = 'test2@medtronic.com.test';

	private static String tEmailAddress_User1 = '';	// This email addres will be of a SFDC User and a Contact in SFDC
	private static String tEmailAddress_User2 = ''; // This email addres will be of a SFDC User but not of a Contact in SFDC

	private static Contact oContact_User;

	@isTest static void createTestData(){

		// Create Contact Data - all with the same email address
		clsTestData_Contact.iRecord_Contact = 5;
		lstContact = clsTestData_Contact.createContact(false);
		for (Contact oContact : lstContact){
			oContact.Email = tEmailAddress;
			oContact.MailingCountry = 'Belgium';
		}
		insert lstContact;

		setID_Contact = new Set<Id>();
		for (Contact oContact : lstContact){
			setID_Contact.add(oContact.Id);
		}

		List<User> lstUser = [SELECT Email FROM User WHERE IsActive = true and Profile.Name in ('System Administrator MDT', 'IT Business Analyst') LIMIT 2];
		if (lstUser.size() == 2){
			tEmailAddress_User1 = lstUser[0].Email;
			tEmailAddress_User2 = lstUser[1].Email;
		}

		System.assertEquals(lstUser.size(), 2);

		oContact_User = new Contact();
		clsTestData_Contact.oMain_Contact = null;
		clsTestData_Contact.iRecord_Contact = 1;
		oContact_User = clsTestData_Contact.createContact(false)[0];
			oContact_User.Email = tEmailAddress_User1;
		insert oContact_User;

	}	

	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// TEST ConnectMe Logic
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	@isTest static void test_MITG_EmailToLeadHandler_ConnectMe() {
		
		// Create Test Data
		createTestData();

		String tToEmailAddress = 'mitg_email_to_lead@medtronic.com.test';

		// Get Test User
        User oUser_System = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true LIMIT 1];
        
        System.runAs(oUser_System){
        
	        // Create a new Email, Envelope Object and Attachment
	        Messaging.InboundEmail oInboundEmail = new Messaging.InboundEmail();
	        Messaging.InboundEnvelope oInboundEnvelope = new Messaging.InboundEnvelope();

			// Setup the data for the Envelope
	        oInboundEnvelope.fromAddress = tEmailAddress;
	        oInboundEnvelope.toAddress = tToEmailAddress;

			// Setup the data for the Email
	        oInboundEmail.fromAddress = tEmailAddress;
	        oInboundEmail.toAddresses = new List<String>{tToEmailAddress};
			oInboundEmail.subject = 'Some Subject Goes Here';
			oInboundEmail.plainTextBody = 'email body line 1\nemail body line 2\nemail body line 3';
			oInboundEmail.htmlBody = 'email body line 1<br />email body line 2<br />email body line 3';

			// Add Binary Attachment
			Messaging.InboundEmail.BinaryAttachment oAttachment_Binary = new Messaging.InboundEmail.BinaryAttachment();
				oAttachment_Binary.body = blob.valueOf('Binary Attachment Content');
				oAttachment_Binary.fileName = 'binaryAttachment.txt';
				oAttachment_Binary.mimeTypeSubType = 'text/plain';
			oInboundEmail.binaryAttachments = new List<Messaging.inboundEmail.BinaryAttachment>{ oAttachment_Binary };


			// Add Text Attachment
			Messaging.InboundEmail.TextAttachment oAttachment_Text = new Messaging.InboundEmail.TextAttachment();
				oAttachment_Text.body = 'Text Attachment Content';
				oAttachment_Text.fileName = 'textAttachment.txt';
				oAttachment_Text.mimeTypeSubType = 'texttwo/plain';
			oInboundEmail.textAttachments = new List<Messaging.inboundEmail.TextAttachment>{ oAttachment_Text };


			Messaging.InboundEmailResult oInboundEmailResult;

			Test.startTest();

				// Call the Email Service Class
				MITG_EmailToLeadHandler oMITG_EmailToLeadHandler =new MITG_EmailToLeadHandler();
				oInboundEmailResult = oMITG_EmailToLeadHandler.handleInboundEmail(oInboundEmail, oInboundEnvelope);

			Test.stopTest();

			clsUtil.debug('test_MITG_EmailToLeadHandler_ConnectMe - oInboundEmailResult.message : ' + oInboundEmailResult.message);

			System.assertEquals(oInboundEmailResult.success, true);

			List<Lead> lstLead = 
				[
					SELECT 
						Id, Email
						, (SELECT Id, Name FROM Attachments) 
						, (SELECT Id, Contact__r.Email FROM Lead_Contacts__r) 
					FROM 
						Lead
				];

			System.assertEquals(lstLead.size(), 1);
			System.assertEquals(lstLead[0].Attachments.size(), 3);

			System.assertEquals(lstLead[0].Lead_Contacts__r.size(), 5);

	    }

	}


	@isTest static void test_MITG_EmailToLeadHandler_ConnectMe_Error() {
		
		// Create Test Data
		createTestData();

		String tToEmailAddress = 'mitg_email_to_lead@medtronic.com.test';

		// Get Test User
        User oUser_System = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true LIMIT 1];
        
        System.runAs(oUser_System){
        
	        // Create a new Email, Envelope Object and Attachment
	        Messaging.InboundEmail oInboundEmail = new Messaging.InboundEmail();
	        Messaging.InboundEnvelope oInboundEnvelope = new Messaging.InboundEnvelope();

			// Setup the data for the Envelope
	        oInboundEnvelope.fromAddress = tEmailAddress;
	        oInboundEnvelope.toAddress = tToEmailAddress;

			// Setup the data for the Email
	        oInboundEmail.fromAddress = tEmailAddress;
	        oInboundEmail.toAddresses = new List<String>{tToEmailAddress};
			oInboundEmail.subject = 'Some Subject Goes Here';
			oInboundEmail.plainTextBody = 'email body line 1\nemail body line 2\nemail body line 3';
			oInboundEmail.htmlBody = 'email body line 1<br />email body line 2<br />email body line 3';

			// Add Binary Attachment
			Messaging.InboundEmail.BinaryAttachment oAttachment_Binary = new Messaging.InboundEmail.BinaryAttachment();
				oAttachment_Binary.body = blob.valueOf('Binary Attachment Content');
				oAttachment_Binary.fileName = 'binaryAttachment.txt';
				oAttachment_Binary.mimeTypeSubType = 'text/plain';
			oInboundEmail.binaryAttachments = new List<Messaging.inboundEmail.BinaryAttachment>{ oAttachment_Binary };


			// Add Text Attachment
			Messaging.InboundEmail.TextAttachment oAttachment_Text = new Messaging.InboundEmail.TextAttachment();
				oAttachment_Text.body = 'Text Attachment Content';
				oAttachment_Text.fileName = 'textAttachment.txt';
				oAttachment_Text.mimeTypeSubType = 'texttwo/plain';
			oInboundEmail.textAttachments = new List<Messaging.inboundEmail.TextAttachment>{ oAttachment_Text };


			Messaging.InboundEmailResult oInboundEmailResult;

			Test.startTest();

				// Call the Email Service Class
				MITG_EmailToLeadHandler oMITG_EmailToLeadHandler = new MITG_EmailToLeadHandler();
				clsUtil.hasException = true;
				oInboundEmailResult = oMITG_EmailToLeadHandler.handleInboundEmail(oInboundEmail, oInboundEnvelope);

			Test.stopTest();

			System.assertEquals(oInboundEmailResult.success, false);

	    }

	}	
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	

	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// TEST AdobeConnect Logic
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	@isTest static void test_MITG_EmailToLeadHandler_AdobeConnect_Contact() {
		
		// Create Test Data
		createTestData();

		String tToEmailAddress = 'mitg_email_to_lead_adobe_connect@medtronic.com.test';

		// Get Test User
        User oUser_System = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true LIMIT 1];
        
        System.runAs(oUser_System){
        
	        // Create a new Email, Envelope Object and Attachment
	        Messaging.InboundEmail oInboundEmail = new Messaging.InboundEmail();
	        Messaging.InboundEnvelope oInboundEnvelope = new Messaging.InboundEnvelope();

			// Setup the data for the Envelope
	        oInboundEnvelope.fromAddress = tEmailAddress_User1;
	        oInboundEnvelope.toAddress = tToEmailAddress;

			// Setup the data for the Email
	        oInboundEmail.fromAddress = tEmailAddress_User1;
	        oInboundEmail.toAddresses = new List<String>{tEmailAddress_User1};
			oInboundEmail.subject = 'Some Subject Goes Here';
			oInboundEmail.plainTextBody = 'email body line 1\nemail body line 2\nemail body line 3';
			oInboundEmail.htmlBody = 'email body line 1<br />email body line 2<br />email body line 3';

			// Add Binary Attachment
			Messaging.InboundEmail.BinaryAttachment oAttachment_Binary = new Messaging.InboundEmail.BinaryAttachment();
				oAttachment_Binary.body = blob.valueOf('Binary Attachment Content');
				oAttachment_Binary.fileName = 'binaryAttachment.txt';
				oAttachment_Binary.mimeTypeSubType = 'text/plain';
			oInboundEmail.binaryAttachments = new List<Messaging.inboundEmail.BinaryAttachment>{ oAttachment_Binary };


			// Add Text Attachment
			Messaging.InboundEmail.TextAttachment oAttachment_Text = new Messaging.InboundEmail.TextAttachment();
				oAttachment_Text.body = 'Text Attachment Content';
				oAttachment_Text.fileName = 'textAttachment.txt';
				oAttachment_Text.mimeTypeSubType = 'texttwo/plain';
			oInboundEmail.textAttachments = new List<Messaging.inboundEmail.TextAttachment>{ oAttachment_Text };


			Messaging.InboundEmailResult oInboundEmailResult;

			Test.startTest();

				// Call the Email Service Class
				MITG_EmailToLeadHandler oMITG_EmailToLeadHandler =new MITG_EmailToLeadHandler();
				oInboundEmailResult = oMITG_EmailToLeadHandler.handleInboundEmail(oInboundEmail, oInboundEnvelope);

			Test.stopTest();

			clsUtil.debug('test_MITG_EmailToLeadHandler_AdobeConnect_Contact - oInboundEmailResult.message : ' + oInboundEmailResult.message);

			System.assertEquals(oInboundEmailResult.success, true);

			List<Lead> lstLead = 
				[
					SELECT 
						Id, Email
						, (SELECT Id, Name FROM Attachments) 
						, (SELECT Id, Contact__r.Email FROM Lead_Contacts__r) 
					FROM 
						Lead
				];

			System.assertEquals(lstLead.size(), 0);

			List<Task> lstTask = 
				[
					SELECT 
						Id, WhoId
						, (SELECT Id, Name FROM Attachments)
					FROM 
						Task
				];
			System.assertEquals(lstTask.size(), 1);
			System.assertEquals(lstTask[0].WhoId, oContact_User.Id);
			System.assertEquals(lstTask[0].Attachments.size(), 3);



	    }

	}

	@isTest static void test_MITG_EmailToLeadHandler_AdobeConnect_MultipleContact() {
		
		// Create Test Data
		createTestData();

		String tToEmailAddress = 'mitg_email_to_lead_adobe_connect@medtronic.com.test';

		// Get Test User
        User oUser_System = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true LIMIT 1];
        
        System.runAs(oUser_System){
        
	        // Create a new Email, Envelope Object and Attachment
	        Messaging.InboundEmail oInboundEmail = new Messaging.InboundEmail();
	        Messaging.InboundEnvelope oInboundEnvelope = new Messaging.InboundEnvelope();

			// Setup the data for the Envelope
	        oInboundEnvelope.fromAddress = tEmailAddress_User1;
	        oInboundEnvelope.toAddress = tToEmailAddress;

			// Setup the data for the Email
	        oInboundEmail.fromAddress = tEmailAddress_User1;
	        oInboundEmail.toAddresses = new List<String>{tEmailAddress_User1, tEmailAddress, tEmailAddress_NotExisting};
			oInboundEmail.subject = 'Some Subject Goes Here';
			oInboundEmail.plainTextBody = 'email body line 1\nemail body line 2\nemail body line 3';
			oInboundEmail.htmlBody = 'email body line 1<br />email body line 2<br />email body line 3';

			// Add Binary Attachment
			Messaging.InboundEmail.BinaryAttachment oAttachment_Binary = new Messaging.InboundEmail.BinaryAttachment();
				oAttachment_Binary.body = blob.valueOf('Binary Attachment Content');
				oAttachment_Binary.fileName = 'binaryAttachment.txt';
				oAttachment_Binary.mimeTypeSubType = 'text/plain';
			oInboundEmail.binaryAttachments = new List<Messaging.inboundEmail.BinaryAttachment>{ oAttachment_Binary };


			// Add Text Attachment
			Messaging.InboundEmail.TextAttachment oAttachment_Text = new Messaging.InboundEmail.TextAttachment();
				oAttachment_Text.body = 'Text Attachment Content';
				oAttachment_Text.fileName = 'textAttachment.txt';
				oAttachment_Text.mimeTypeSubType = 'texttwo/plain';
			oInboundEmail.textAttachments = new List<Messaging.inboundEmail.TextAttachment>{ oAttachment_Text };


			Messaging.InboundEmailResult oInboundEmailResult;

			Test.startTest();

				// Call the Email Service Class
				MITG_EmailToLeadHandler oMITG_EmailToLeadHandler =new MITG_EmailToLeadHandler();
				oInboundEmailResult = oMITG_EmailToLeadHandler.handleInboundEmail(oInboundEmail, oInboundEnvelope);

			Test.stopTest();

			clsUtil.debug('test_MITG_EmailToLeadHandler_AdobeConnect_Contact - oInboundEmailResult.message : ' + oInboundEmailResult.message);

			System.assertEquals(oInboundEmailResult.success, true);

			List<Lead> lstLead = 
				[
					SELECT 
						Id, Email
						, (SELECT Id, Name FROM Attachments) 
						, (SELECT Id, Contact__r.Email FROM Lead_Contacts__r) 
					FROM 
						Lead
				];

			System.assertEquals(lstLead.size(), 1);

			List<Task> lstTask = 
				[
					SELECT 
						Id, WhoId
						, (SELECT Id, Name FROM Attachments)
					FROM 
						Task
				];
			System.assertEquals(lstTask.size(), 3);
			System.assert( (lstTask[0].WhoId == oContact_User.Id) || (lstTask[0].WhoId == lstLead[0].Id) );
			System.assertEquals(lstTask[0].Attachments.size(), 3);



	    }

	}	


	@isTest static void test_MITG_EmailToLeadHandler_AdobeConnect_Lead() {
		
		// Create Test Data
		createTestData();

		String tToEmailAddress = 'mitg_email_to_lead_adobe_connect@medtronic.com.test';

		// Get Test User
        User oUser_System = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true LIMIT 1];
        
        System.runAs(oUser_System){
        
	        // Create a new Email, Envelope Object and Attachment
	        Messaging.InboundEmail oInboundEmail = new Messaging.InboundEmail();
	        Messaging.InboundEnvelope oInboundEnvelope = new Messaging.InboundEnvelope();

			// Setup the data for the Envelope
	        oInboundEnvelope.fromAddress = tEmailAddress_User2;
	        oInboundEnvelope.toAddress = tToEmailAddress;

			// Setup the data for the Email
	        oInboundEmail.fromAddress = tEmailAddress_User2;
	        oInboundEmail.toAddresses = new List<String>{tToEmailAddress};
			oInboundEmail.subject = 'Some Subject Goes Here';
			oInboundEmail.plainTextBody = 'email body line 1\nemail body line 2\nemail body line 3';
			oInboundEmail.htmlBody = 'email body line 1<br />email body line 2<br />email body line 3';

			// Add Binary Attachment
			Messaging.InboundEmail.BinaryAttachment oAttachment_Binary = new Messaging.InboundEmail.BinaryAttachment();
				oAttachment_Binary.body = blob.valueOf('Binary Attachment Content');
				oAttachment_Binary.fileName = 'binaryAttachment.txt';
				oAttachment_Binary.mimeTypeSubType = 'text/plain';
			oInboundEmail.binaryAttachments = new List<Messaging.inboundEmail.BinaryAttachment>{ oAttachment_Binary };


			// Add Text Attachment
			Messaging.InboundEmail.TextAttachment oAttachment_Text = new Messaging.InboundEmail.TextAttachment();
				oAttachment_Text.body = 'Text Attachment Content';
				oAttachment_Text.fileName = 'textAttachment.txt';
				oAttachment_Text.mimeTypeSubType = 'texttwo/plain';
			oInboundEmail.textAttachments = new List<Messaging.inboundEmail.TextAttachment>{ oAttachment_Text };


			Messaging.InboundEmailResult oInboundEmailResult;

			Test.startTest();

				// Call the Email Service Class
				MITG_EmailToLeadHandler oMITG_EmailToLeadHandler =new MITG_EmailToLeadHandler();
				oInboundEmailResult = oMITG_EmailToLeadHandler.handleInboundEmail(oInboundEmail, oInboundEnvelope);

			Test.stopTest();

			clsUtil.debug('test_MITG_EmailToLeadHandler_AdobeConnect_Lead - oInboundEmailResult.message : ' + oInboundEmailResult.message);

			System.assertEquals(oInboundEmailResult.success, true);

			List<Lead> lstLead = 
				[
					SELECT 
						Id, Email
						, (SELECT Id, Name FROM Attachments) 
						, (SELECT Id, Contact__r.Email FROM Lead_Contacts__r) 
					FROM 
						Lead
				];

			System.assertEquals(lstLead.size(), 1);
			System.assertEquals(lstLead[0].Attachments.size(), 3);
			System.assertEquals(lstLead[0].Lead_Contacts__r.size(), 0);

			List<Task> lstTask = 
				[
					SELECT 
						Id, WhoId
						, (SELECT Id, Name FROM Attachments)
					FROM 
						Task
				];
			System.assertEquals(lstTask.size(), 1);
			System.assertEquals(lstTask[0].WhoId, lstLead[0].Id);
			System.assertEquals(lstTask[0].Attachments.size(), 3);

	    }

	}


	@isTest static void test_MITG_EmailToLeadHandler_AdobeConnect_Error() {
		
		// Create Test Data
		createTestData();

		String tToEmailAddress = 'mitg_email_to_lead_adobe_connect@medtronic.com.test';

		// Get Test User
        User oUser_System = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true LIMIT 1];
        
        System.runAs(oUser_System){
        
	        // Create a new Email, Envelope Object and Attachment
	        Messaging.InboundEmail oInboundEmail = new Messaging.InboundEmail();
	        Messaging.InboundEnvelope oInboundEnvelope = new Messaging.InboundEnvelope();

			// Setup the data for the Envelope
	        oInboundEnvelope.fromAddress = tEmailAddress_User1;
	        oInboundEnvelope.toAddress = tToEmailAddress;

			// Setup the data for the Email
	        oInboundEmail.fromAddress = tEmailAddress_User1;
	        oInboundEmail.toAddresses = new List<String>{tEmailAddress_User1};
			oInboundEmail.subject = 'Some Subject Goes Here';
			oInboundEmail.plainTextBody = 'email body line 1\nemail body line 2\nemail body line 3';
			oInboundEmail.htmlBody = 'email body line 1<br />email body line 2<br />email body line 3';

			// Add Binary Attachment
			Messaging.InboundEmail.BinaryAttachment oAttachment_Binary = new Messaging.InboundEmail.BinaryAttachment();
				oAttachment_Binary.body = blob.valueOf('Binary Attachment Content');
				oAttachment_Binary.fileName = 'binaryAttachment.txt';
				oAttachment_Binary.mimeTypeSubType = 'text/plain';
			oInboundEmail.binaryAttachments = new List<Messaging.inboundEmail.BinaryAttachment>{ oAttachment_Binary };


			// Add Text Attachment
			Messaging.InboundEmail.TextAttachment oAttachment_Text = new Messaging.InboundEmail.TextAttachment();
				oAttachment_Text.body = 'Text Attachment Content';
				oAttachment_Text.fileName = 'textAttachment.txt';
				oAttachment_Text.mimeTypeSubType = 'texttwo/plain';
			oInboundEmail.textAttachments = new List<Messaging.inboundEmail.TextAttachment>{ oAttachment_Text };


			Messaging.InboundEmailResult oInboundEmailResult;

			Test.startTest();

				// Call the Email Service Class
				MITG_EmailToLeadHandler oMITG_EmailToLeadHandler = new MITG_EmailToLeadHandler();
				clsUtil.hasException = true;
				oInboundEmailResult = oMITG_EmailToLeadHandler.handleInboundEmail(oInboundEmail, oInboundEnvelope);

			Test.stopTest();

			System.assertEquals(oInboundEmailResult.success, false);

	    }

	}		
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------



	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// TEST ManualEmail Logic
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	@isTest static void test_MITG_EmailToLeadHandler_ManualEmail_Contact() {
		
		// Create Test Data
		createTestData();

		String tToEmailAddress = 'mitg_email_to_lead_manual_email@medtronic.com.test';

		// Get Test User
        User oUser_System = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true LIMIT 1];
        
        System.runAs(oUser_System){
        
	        // Create a new Email, Envelope Object and Attachment
	        Messaging.InboundEmail oInboundEmail = new Messaging.InboundEmail();
	        Messaging.InboundEnvelope oInboundEnvelope = new Messaging.InboundEnvelope();

			// Setup the data for the Envelope
	        oInboundEnvelope.fromAddress = tEmailAddress_User1;
	        oInboundEnvelope.toAddress = tToEmailAddress;

			// Setup the data for the Email
	        oInboundEmail.fromAddress = tEmailAddress_User1;
	        oInboundEmail.toAddresses = new List<String>{tEmailAddress_User1};
			oInboundEmail.subject = 'Some Subject Goes Here';
			oInboundEmail.plainTextBody = 'email body line 1\nemail body line 2\nemail body line 3';
			oInboundEmail.htmlBody = 'email body line 1<br />email body line 2<br />email body line 3';

			// Add Binary Attachment
			Messaging.InboundEmail.BinaryAttachment oAttachment_Binary = new Messaging.InboundEmail.BinaryAttachment();
				oAttachment_Binary.body = blob.valueOf('Binary Attachment Content');
				oAttachment_Binary.fileName = 'binaryAttachment.txt';
				oAttachment_Binary.mimeTypeSubType = 'text/plain';
			oInboundEmail.binaryAttachments = new List<Messaging.inboundEmail.BinaryAttachment>{ oAttachment_Binary };


			// Add Text Attachment
			Messaging.InboundEmail.TextAttachment oAttachment_Text = new Messaging.InboundEmail.TextAttachment();
				oAttachment_Text.body = 'Text Attachment Content';
				oAttachment_Text.fileName = 'textAttachment.txt';
				oAttachment_Text.mimeTypeSubType = 'texttwo/plain';
			oInboundEmail.textAttachments = new List<Messaging.inboundEmail.TextAttachment>{ oAttachment_Text };


			Messaging.InboundEmailResult oInboundEmailResult;

			Test.startTest();

				// Call the Email Service Class
				MITG_EmailToLeadHandler oMITG_EmailToLeadHandler =new MITG_EmailToLeadHandler();
				oInboundEmailResult = oMITG_EmailToLeadHandler.handleInboundEmail(oInboundEmail, oInboundEnvelope);

			Test.stopTest();

			clsUtil.debug('test_MITG_EmailToLeadHandler_ManualEmail_Contact - oInboundEmailResult.message : ' + oInboundEmailResult.message);

			System.assertEquals(oInboundEmailResult.success, true);

			List<Lead> lstLead = 
				[
					SELECT 
						Id, Email
						, (SELECT Id, Name FROM Attachments) 
						, (SELECT Id, Contact__r.Email FROM Lead_Contacts__r) 
					FROM 
						Lead
				];

			System.assertEquals(lstLead.size(), 0);

			List<Task> lstTask = 
				[
					SELECT 
						Id, WhoId
						, (SELECT Id, Name FROM Attachments)
					FROM 
						Task
				];
			System.assertEquals(lstTask.size(), 1);
			System.assertEquals(lstTask[0].WhoId, oContact_User.Id);
			System.assertEquals(lstTask[0].Attachments.size(), 3);

	    }

	}


	@isTest static void test_MITG_EmailToLeadHandler_ManualEmail_Lead() {
		
		// Create Test Data
		createTestData();

		String tToEmailAddress = 'mitg_email_to_lead_manual_email@medtronic.com.test';

		// Get Test User
        User oUser_System = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true LIMIT 1];
        
        System.runAs(oUser_System){
        
	        // Create a new Email, Envelope Object and Attachment
	        Messaging.InboundEmail oInboundEmail = new Messaging.InboundEmail();
	        Messaging.InboundEnvelope oInboundEnvelope = new Messaging.InboundEnvelope();

			// Setup the data for the Envelope
	        oInboundEnvelope.fromAddress = tEmailAddress_User2;
	        oInboundEnvelope.toAddress = tToEmailAddress;

			// Setup the data for the Email
	        oInboundEmail.fromAddress = tEmailAddress_User2;
	        oInboundEmail.toAddresses = new List<String>{tEmailAddress_User2};
			oInboundEmail.subject = 'Some Subject Goes Here';
			oInboundEmail.plainTextBody = 'email body line 1\nemail body line 2\nemail body line 3';
			oInboundEmail.htmlBody = 'email body line 1<br />email body line 2<br />email body line 3';

			// Add Binary Attachment
			Messaging.InboundEmail.BinaryAttachment oAttachment_Binary = new Messaging.InboundEmail.BinaryAttachment();
				oAttachment_Binary.body = blob.valueOf('Binary Attachment Content');
				oAttachment_Binary.fileName = 'binaryAttachment.txt';
				oAttachment_Binary.mimeTypeSubType = 'text/plain';
			oInboundEmail.binaryAttachments = new List<Messaging.inboundEmail.BinaryAttachment>{ oAttachment_Binary };


			// Add Text Attachment
			Messaging.InboundEmail.TextAttachment oAttachment_Text = new Messaging.InboundEmail.TextAttachment();
				oAttachment_Text.body = 'Text Attachment Content';
				oAttachment_Text.fileName = 'textAttachment.txt';
				oAttachment_Text.mimeTypeSubType = 'texttwo/plain';
			oInboundEmail.textAttachments = new List<Messaging.inboundEmail.TextAttachment>{ oAttachment_Text };


			Messaging.InboundEmailResult oInboundEmailResult;

			Test.startTest();

				// Call the Email Service Class
				MITG_EmailToLeadHandler oMITG_EmailToLeadHandler =new MITG_EmailToLeadHandler();
				oInboundEmailResult = oMITG_EmailToLeadHandler.handleInboundEmail(oInboundEmail, oInboundEnvelope);

			Test.stopTest();

			clsUtil.debug('test_MITG_EmailToLeadHandler_ManualEmail_Lead - oInboundEmailResult.message : ' + oInboundEmailResult.message);

			System.assertEquals(oInboundEmailResult.success, true);

			List<Lead> lstLead = 
				[
					SELECT 
						Id, Email
						, (SELECT Id, Name FROM Attachments) 
						, (SELECT Id, Contact__r.Email FROM Lead_Contacts__r) 
					FROM 
						Lead
				];

			System.assertEquals(lstLead.size(), 1);
			System.assertEquals(lstLead[0].Attachments.size(), 3);
			System.assertEquals(lstLead[0].Lead_Contacts__r.size(), 0);

			List<Task> lstTask = 
				[
					SELECT 
						Id, WhoId
						, (SELECT Id, Name FROM Attachments)
					FROM 
						Task
				];
			System.assertEquals(lstTask.size(), 1);
			System.assertEquals(lstTask[0].WhoId, lstLead[0].Id);
			System.assertEquals(lstTask[0].Attachments.size(), 3);

	    }

	}


	@isTest static void test_MITG_EmailToLeadHandler_ManualEmail_Error() {
		
		// Create Test Data
		createTestData();

		String tToEmailAddress = 'mitg_email_to_lead_manual_email@medtronic.com.test';

		// Get Test User
        User oUser_System = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true LIMIT 1];
        
        System.runAs(oUser_System){
        
	        // Create a new Email, Envelope Object and Attachment
	        Messaging.InboundEmail oInboundEmail = new Messaging.InboundEmail();
	        Messaging.InboundEnvelope oInboundEnvelope = new Messaging.InboundEnvelope();

			// Setup the data for the Envelope
	        oInboundEnvelope.fromAddress = tEmailAddress_User1;
	        oInboundEnvelope.toAddress = tToEmailAddress;

			// Setup the data for the Email
	        oInboundEmail.fromAddress = tEmailAddress_User1;
	        oInboundEmail.toAddresses = new List<String>{tEmailAddress_User1};
			oInboundEmail.subject = 'Some Subject Goes Here';
			oInboundEmail.plainTextBody = 'email body line 1\nemail body line 2\nemail body line 3';
			oInboundEmail.htmlBody = 'email body line 1<br />email body line 2<br />email body line 3';

			// Add Binary Attachment
			Messaging.InboundEmail.BinaryAttachment oAttachment_Binary = new Messaging.InboundEmail.BinaryAttachment();
				oAttachment_Binary.body = blob.valueOf('Binary Attachment Content');
				oAttachment_Binary.fileName = 'binaryAttachment.txt';
				oAttachment_Binary.mimeTypeSubType = 'text/plain';
			oInboundEmail.binaryAttachments = new List<Messaging.inboundEmail.BinaryAttachment>{ oAttachment_Binary };


			// Add Text Attachment
			Messaging.InboundEmail.TextAttachment oAttachment_Text = new Messaging.InboundEmail.TextAttachment();
				oAttachment_Text.body = 'Text Attachment Content';
				oAttachment_Text.fileName = 'textAttachment.txt';
				oAttachment_Text.mimeTypeSubType = 'texttwo/plain';
			oInboundEmail.textAttachments = new List<Messaging.inboundEmail.TextAttachment>{ oAttachment_Text };


			Messaging.InboundEmailResult oInboundEmailResult;

			Test.startTest();

				// Call the Email Service Class
				MITG_EmailToLeadHandler oMITG_EmailToLeadHandler = new MITG_EmailToLeadHandler();
				clsUtil.hasException = true;
				oInboundEmailResult = oMITG_EmailToLeadHandler.handleInboundEmail(oInboundEmail, oInboundEnvelope);

			Test.stopTest();

			System.assertEquals(oInboundEmailResult.success, false);

	    }

	}			
}