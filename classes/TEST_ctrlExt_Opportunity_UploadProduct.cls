//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 10-04-2017
//  Description      : TEST Class for APEX Controller Extension ctrlExt_Opportunity_UploadProduct and VF Page Opportunity_UploadProduct
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_ctrlExt_Opportunity_UploadProduct {

	private static String tProductCode_Prefix = 'ABCDE';
	private static Opportunity oOpportunity;

	private static Map<String, ctrlExt_Opportunity_UploadProduct.FieldMapping> mapFieldMapping;

	private static Blob oBlob_OK;
	private static Blob oBlob_Error_1;
	private static Blob oBlob_Error_2;
	private static Blob oBlob_Error_3;


	@isTest private static void createTestData(){

		// Create Product Data
		clsTestData_Product.iRecord_Product = 5;
		clsTestData_Product.createProductData_NoBU(false);
		Integer iCounter = 1;
		for (Product2 oProduct : clsTestData_Product.lstProduct){
			oProduct.Identifier__c = tProductCode_Prefix + String.valueOf(iCounter);
			iCounter++;
		}
		insert clsTestData_Product.lstProduct;
		System.assertEquals(clsTestData_Product.lstProduct.size(), 5);

	  
		// Create PricebookEntry
		clsTestData_Opportunity.createPricebookEntry(true);
		System.assertEquals(clsTestData_Opportunity.lstPricebookEntry.size(), 5);


		// Create Opportunity Data
		clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'EMEA_IHS_Managed_Services').Id;
		clsTestData_Opportunity.createOpportunity(false);
			clsTestData_Opportunity.oMain_Opportunity.CloseDate = System.Today();
    		clsTestData_Opportunity.oMain_Opportunity.StageName = '0 - Tracking';
    		clsTestData_Opportunity.oMain_Opportunity.CurrencyISOCode = 'EUR';
			clsTestData_Opportunity.oMain_Opportunity.Pricebook2Id = clsTestData_Opportunity.oMain_Pricebook.Id;
    	insert clsTestData_Opportunity.oMain_Opportunity;

    	oOpportunity = clsTestData_Opportunity.oMain_Opportunity;


    	// Define Field Mapping
		mapFieldMapping = new Map<String, ctrlExt_Opportunity_UploadProduct.FieldMapping>();
        ctrlExt_Opportunity_UploadProduct.FieldMapping oFieldMapping1 = new ctrlExt_Opportunity_UploadProduct.FieldMapping();
            oFieldMapping1.tFieldNameFile = 'Revenue';
            oFieldMapping1.tFieldNameSFDC = 'UnitPrice';
            oFieldMapping1.tFieldType = 'Decimal';
        mapFieldMapping.put('Revenue', oFieldMapping1);
        ctrlExt_Opportunity_UploadProduct.FieldMapping oFieldMapping2 = new ctrlExt_Opportunity_UploadProduct.FieldMapping();
            oFieldMapping2.tFieldNameFile = 'BB Code';
            oFieldMapping2.tFieldNameSFDC = 'Identifier__c';
            oFieldMapping2.tFieldType = 'Text';
            oFieldMapping2.bProductIdentifier = true;
        mapFieldMapping.put('BB Code', oFieldMapping2);
        ctrlExt_Opportunity_UploadProduct.FieldMapping oFieldMapping3 = new ctrlExt_Opportunity_UploadProduct.FieldMapping();
            oFieldMapping3.tFieldNameFile = 'Currency';
            oFieldMapping3.tFieldNameSFDC = 'CurrencyIsoCode';
            oFieldMapping3.tFieldType = 'Text';
            oFieldMapping3.bCurrencyField = true;
        mapFieldMapping.put('Currency', oFieldMapping3);


    	// Create Test file
    	String tDelimiter = ';';
		String tNextLine = '\n';

		String tDataFile_OK = 'Revenue' + tDelimiter + 'BB Code' + tDelimiter + 'Currency' + tNextLine;
		tDataFile_OK += '200' + tDelimiter + 'ABCDE1' + tDelimiter + 'EUR' + tNextLine;
		tDataFile_OK += '400' + tDelimiter + 'ABCDE2' + tDelimiter + 'EUR' + tNextLine;
		oBlob_OK = Blob.valueOf(tDataFile_OK);

		String tDataFile_Error_1 = 'Revenue' + tDelimiter + 'BB Code' + tDelimiter + 'Currency' + tDelimiter + 'Additional Column' + tNextLine;
		tDataFile_Error_1 += '200' + tDelimiter + 'ABCDE1' + tDelimiter + 'EUR' + tNextLine;
		tDataFile_Error_1 += '400' + tDelimiter + 'ABCDE2' + tDelimiter + 'EUR' + tNextLine;
		oBlob_Error_1 = Blob.valueOf(tDataFile_Error_1);

		String tDataFile_Error_2 = 'Revenue' + tDelimiter + 'BB Code' + tDelimiter + 'Currency' + tNextLine;
		tDataFile_Error_2 += '200' + tDelimiter + 'ABCDE1' + tDelimiter + 'EUR' + tNextLine;
		tDataFile_Error_2 += '400' + tDelimiter + 'ABCDE2' + tDelimiter + 'EUR' + tNextLine;
		tDataFile_Error_2 += '200' + tDelimiter + 'ABCDE3' + tDelimiter + 'EUR' + tDelimiter + 'TEST' + tNextLine;
		tDataFile_Error_2 += '400' + tDelimiter + 'ABCDE10' + tDelimiter + 'EUR' + tNextLine;
		tDataFile_Error_2 += '500' + tDelimiter + 'ABCDE1' + tDelimiter + 'USD' + tNextLine;
		oBlob_Error_2 = Blob.valueOf(tDataFile_Error_2);

		String tDataFile_Error_3 = 'Revenue' + tDelimiter + 'BB Code' + tDelimiter + 'Currency' + tNextLine;
		tDataFile_Error_3 += '0'  + tDelimiter + 'ABCDE1' + tDelimiter + 'EUR' + tNextLine;
		tDataFile_Error_3 += '' + tDelimiter + 'ABCDE2' + tDelimiter + 'EUR' + tNextLine;
		oBlob_Error_3 = Blob.valueOf(tDataFile_Error_3);

	}

	@isTest static void test_ctrlExt_Opportunity_UploadProduct_OK(){

		//------------------------------------------------------------------------
		// Create Test Data
		//------------------------------------------------------------------------
		createTestData();
		//------------------------------------------------------------------------

		//------------------------------------------------------------------------
		// Perform Test
		//------------------------------------------------------------------------
		Test.startTest();

        System.currentPageReference().getParameters().put('Id', null);
        ctrlExt_Opportunity_UploadProduct oCtrlExt = new ctrlExt_Opportunity_UploadProduct(new ApexPages.StandardController(oOpportunity));

        System.currentPageReference().getParameters().put('Id', oOpportunity.Id);
        oCtrlExt = new ctrlExt_Opportunity_UploadProduct(new ApexPages.StandardController(oOpportunity));


        String tTest = oCtrlExt.tPageTitle;
        tTest = oCtrlExt.tPageSubTitle;
        Boolean bTest = oCtrlExt.bRenderPage;
        tTest = oCtrlExt.tFileName;
        bTest = oCtrlExt.bFileProcessed;

        oCtrlExt.oFile = oBlob_OK;

        oCtrlExt.processFile();

        Boolean bHasValidRecords = oCtrlExt.bHasValidRecords;
        Boolean bHasInvalidRecords = oCtrlExt.bHasInvalidRecords;
        Boolean bHasSkippedRecords = oCtrlExt.bHasSkippedRecords;

        oCtrlExt.backToOpportunity();

		Test.stopTest();
		//------------------------------------------------------------------------


		//------------------------------------------------------------------------
		// Validate Test Result
		//------------------------------------------------------------------------

		System.assertEquals(bHasValidRecords, true);
		System.assertEquals(bHasInvalidRecords, false);
		System.assertEquals(bHasSkippedRecords, false);

		System.assertEquals(oCtrlExt.lstDataRecord.size(), 2);
		System.assertEquals(oCtrlExt.lstDataRecord_Error.size(), 0);
		System.assertEquals(oCtrlExt.lstDataRecord_Skipped.size(), 0);

		//------------------------------------------------------------------------

	}


	@isTest static void test_ctrlExt_Opportunity_UploadProduct_ERROR(){

		//------------------------------------------------------------------------
		// Create Test Data
		//------------------------------------------------------------------------
		createTestData();
		//------------------------------------------------------------------------

		//------------------------------------------------------------------------
		// Perform Test
		//------------------------------------------------------------------------
		Test.startTest();

        System.currentPageReference().getParameters().put('Id', null);
        ctrlExt_Opportunity_UploadProduct oCtrlExt = new ctrlExt_Opportunity_UploadProduct(new ApexPages.StandardController(oOpportunity));

        System.currentPageReference().getParameters().put('Id', oOpportunity.Id);
        oCtrlExt = new ctrlExt_Opportunity_UploadProduct(new ApexPages.StandardController(oOpportunity));


        String tTest = oCtrlExt.tPageTitle;
        tTest = oCtrlExt.tPageSubTitle;
        Boolean bTest = oCtrlExt.bRenderPage;
        tTest = oCtrlExt.tFileName;
        bTest = oCtrlExt.bFileProcessed;

        oCtrlExt.processFile();

        Boolean bHasValidRecords = oCtrlExt.bHasValidRecords;
        Boolean bHasInvalidRecords = oCtrlExt.bHasInvalidRecords;
        Boolean bHasSkippedRecords = oCtrlExt.bHasSkippedRecords;


		Test.stopTest();
		//------------------------------------------------------------------------


		//------------------------------------------------------------------------
		// Validate Test Result
		//------------------------------------------------------------------------

		System.assertEquals(bHasValidRecords, false);
		System.assertEquals(bHasInvalidRecords, false);
		System.assertEquals(bHasSkippedRecords, false);

		System.assertEquals(oCtrlExt.lstDataRecord.size(), 0);
		System.assertEquals(oCtrlExt.lstDataRecord_Error.size(), 0);
		System.assertEquals(oCtrlExt.lstDataRecord_Skipped.size(), 0);

		//------------------------------------------------------------------------

	}

	@isTest static void test_ctrlExt_Opportunity_UploadProduct_ERROR_1(){

		//------------------------------------------------------------------------
		// Create Test Data
		//------------------------------------------------------------------------
		createTestData();
		//------------------------------------------------------------------------

		//------------------------------------------------------------------------
		// Perform Test
		//------------------------------------------------------------------------
		Test.startTest();

        System.currentPageReference().getParameters().put('Id', null);
        ctrlExt_Opportunity_UploadProduct oCtrlExt = new ctrlExt_Opportunity_UploadProduct(new ApexPages.StandardController(oOpportunity));

        System.currentPageReference().getParameters().put('Id', oOpportunity.Id);
        oCtrlExt = new ctrlExt_Opportunity_UploadProduct(new ApexPages.StandardController(oOpportunity));


        String tTest = oCtrlExt.tPageTitle;
        tTest = oCtrlExt.tPageSubTitle;
        Boolean bTest = oCtrlExt.bRenderPage;
        tTest = oCtrlExt.tFileName;
        bTest = oCtrlExt.bFileProcessed;

        oCtrlExt.oFile = oBlob_Error_1;

        oCtrlExt.processFile();

        Boolean bHasValidRecords = oCtrlExt.bHasValidRecords;
        Boolean bHasInvalidRecords = oCtrlExt.bHasInvalidRecords;
        Boolean bHasSkippedRecords = oCtrlExt.bHasSkippedRecords;


		Test.stopTest();
		//------------------------------------------------------------------------


		//------------------------------------------------------------------------
		// Validate Test Result
		//------------------------------------------------------------------------

		System.assertEquals(bHasValidRecords, false);
		System.assertEquals(bHasInvalidRecords, false);
		System.assertEquals(bHasSkippedRecords, false);

		System.assertEquals(oCtrlExt.lstDataRecord.size(), 0);
		System.assertEquals(oCtrlExt.lstDataRecord_Error.size(), 0);
		System.assertEquals(oCtrlExt.lstDataRecord_Skipped.size(), 0);

		//------------------------------------------------------------------------

	}


	@isTest static void test_ctrlExt_Opportunity_UploadProduct_ERROR_2(){

		//------------------------------------------------------------------------
		// Create Test Data
		//------------------------------------------------------------------------
		createTestData();
		//------------------------------------------------------------------------

		//------------------------------------------------------------------------
		// Perform Test
		//------------------------------------------------------------------------
		Test.startTest();

        System.currentPageReference().getParameters().put('Id', null);
        ctrlExt_Opportunity_UploadProduct oCtrlExt = new ctrlExt_Opportunity_UploadProduct(new ApexPages.StandardController(oOpportunity));

        System.currentPageReference().getParameters().put('Id', oOpportunity.Id);
        oCtrlExt = new ctrlExt_Opportunity_UploadProduct(new ApexPages.StandardController(oOpportunity));


        String tTest = oCtrlExt.tPageTitle;
        tTest = oCtrlExt.tPageSubTitle;
        Boolean bTest = oCtrlExt.bRenderPage;
        tTest = oCtrlExt.tFileName;
        bTest = oCtrlExt.bFileProcessed;

        oCtrlExt.oFile = oBlob_Error_2;

        oCtrlExt.processFile();

        Boolean bHasValidRecords = oCtrlExt.bHasValidRecords;
        Boolean bHasInvalidRecords = oCtrlExt.bHasInvalidRecords;
        Boolean bHasSkippedRecords = oCtrlExt.bHasSkippedRecords;


		Test.stopTest();
		//------------------------------------------------------------------------


		//------------------------------------------------------------------------
		// Validate Test Result
		//------------------------------------------------------------------------

		System.assertEquals(bHasValidRecords, true);
		System.assertEquals(bHasInvalidRecords, true);
		System.assertEquals(bHasSkippedRecords, false);

		System.assertEquals(oCtrlExt.lstDataRecord.size(), 2);
		System.assertEquals(oCtrlExt.lstDataRecord_Error.size(), 3);
		System.assertEquals(oCtrlExt.lstDataRecord_Skipped.size(), 0);

		//------------------------------------------------------------------------

	}	


	@isTest static void test_ctrlExt_Opportunity_UploadProduct_ERROR_3(){

		//------------------------------------------------------------------------
		// Create Test Data
		//------------------------------------------------------------------------
		createTestData();
		//------------------------------------------------------------------------

		//------------------------------------------------------------------------
		// Perform Test
		//------------------------------------------------------------------------
		Test.startTest();

        System.currentPageReference().getParameters().put('Id', null);
        ctrlExt_Opportunity_UploadProduct oCtrlExt = new ctrlExt_Opportunity_UploadProduct(new ApexPages.StandardController(oOpportunity));

        System.currentPageReference().getParameters().put('Id', oOpportunity.Id);
        oCtrlExt = new ctrlExt_Opportunity_UploadProduct(new ApexPages.StandardController(oOpportunity));


        String tTest = oCtrlExt.tPageTitle;
        tTest = oCtrlExt.tPageSubTitle;
        Boolean bTest = oCtrlExt.bRenderPage;
        tTest = oCtrlExt.tFileName;
        bTest = oCtrlExt.bFileProcessed;

        oCtrlExt.oFile = oBlob_Error_3;

        oCtrlExt.processFile();

        Boolean bHasValidRecords = oCtrlExt.bHasValidRecords;
        Boolean bHasInvalidRecords = oCtrlExt.bHasInvalidRecords;
        Boolean bHasSkippedRecords = oCtrlExt.bHasSkippedRecords;


		Test.stopTest();
		//------------------------------------------------------------------------


		//------------------------------------------------------------------------
		// Validate Test Result
		//------------------------------------------------------------------------

		System.assertEquals(bHasValidRecords, false);
		System.assertEquals(bHasInvalidRecords, false);
		System.assertEquals(bHasSkippedRecords, true);

		System.assertEquals(oCtrlExt.lstDataRecord.size(), 0);
		System.assertEquals(oCtrlExt.lstDataRecord_Error.size(), 0);
		System.assertEquals(oCtrlExt.lstDataRecord_Skipped.size(), 2);

		//------------------------------------------------------------------------

	}		

}
//--------------------------------------------------------------------------------------------------------------------