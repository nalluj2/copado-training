@isTest
private class Test_Support_Requests_BI {
	
	private static User currentUser = [Select Id, alias, countryOR__c from User where Id = :UserInfo.getUserId()];
	
	private static testmethod void createUser() {
		
		User oUser_Admin = clsTestData_User.createUser_SystemAdministratorMDT('tstadm1', false);
			oUser_Admin.Primary_sBU__c = 'AF Solutions';
		insert oUser_Admin;

		System.runAs(oUser_Admin){
			createBusinessData(true);
		}
			
		ctrl_Support_Requests controller;							
		Test.startTest();
		
			Test.setMock(WebServiceMock.class, new ws_adUserServiceMockImpl());
		
			controller = new ctrl_Support_Requests();			
			System.assert(controller.userLoggedIn==true);
									
			controller.helper_Request_BI.requestNewUser();
								
			System.assert(controller.helper_Request_BI.hasSFDCUser == false);
			System.assert(controller.helper_Request_BI.lstSO_CapabilityGroup.size() > 1);
			System.assert(controller.helper_Request_BI.lstSO_ContentGroup.size() > 1);
			System.assert(controller.session.request.BI_Capability_Group__c != null);
		
			controller.helper_Request_BI.searchAlias = oUser_Admin.alias;
			controller.helper_Request_BI.searchByAlias();
		
			System.assert(controller.helper_Request_BI.hasSFDCUser == true);
			System.assert(controller.session.request.Jobtitle_vs__c != null);
			System.assert(controller.session.request.Company_Code__c != null);
			System.assert(controller.session.request.User_Business_Unit_vs__c != null);
			System.assert(controller.session.request.Geographical_Region_vs__c != null);
			System.assert(controller.session.request.Geographical_Subregion__c != null);
			System.assert(controller.session.request.Geographical_Country_vs__c != null);
			System.assert(controller.session.request.Primary_sBU__c != null);
		
	//		controller.helper_Request_BI.searchAliasSameAsUser = currentUser.alias;
	//		controller.helper_Request_BI.searchByAliasSameAsUser();
							
			controller.session.request.BI_Content_Group__c = controller.helper_Request_BI.lstSO_ContentGroup[1].getValue();		
			controller.session.request.Description_Long__c = 'blabla';
			controller.session.request.User_Business_Unit_vs__c = 'CRHF';

			System.runAs(oUser_Admin){
				
				controller.helper_Request_BI.createUserRequest();

				List<String> lstSelectedBusinessUnit = new List<String>();
				lstSelectedBusinessUnit.add('CRHF');
				controller.helper_Request_BI.lstSelectedBusinessUnit = lstSelectedBusinessUnit;
				controller.helper_Request_BI.BUSelectionChanged();

				lstSelectedBusinessUnit = new List<String>();
				lstSelectedBusinessUnit.add('CRHF');
				lstSelectedBusinessUnit.add('Diabetes');
				controller.helper_Request_BI.lstSelectedBusinessUnit = lstSelectedBusinessUnit;
				controller.helper_Request_BI.BUSelectionChanged();
		
				System.assert(controller.session.request.Name != null);	


				controller.helper_Request_BI.refreshCompanyCodeData();
				controller.helper_Request_BI.geographicalRegionChanged();
				controller.helper_Request_BI.geographicalSubRegionChanged();
				controller.helper_Request_BI.userBusinessUnitChanged();
				controller.helper_Request_BI.clearCompanyCodeData();
				controller.helper_Request_BI.countrySelectionChanged();
			}

		Test.stopTest();

	}
	
	private static testmethod void createUser2() {
		
		User oUser_Admin = clsTestData_User.createUser_SystemAdministratorMDT('tstadm1', false);
			oUser_Admin.Primary_sBU__c = 'AF Solutions';
		insert oUser_Admin;

		System.runAs(oUser_Admin){
			createBusinessData(false);
		}
										
		ctrl_Support_Requests controller;

		Test.startTest();
		
			Test.setMock(WebServiceMock.class, new ws_adUserServiceMockImpl());
		
			controller = new ctrl_Support_Requests();			
			System.assert(controller.userLoggedIn==true);
									
			controller.helper_Request_BI.requestNewUser();
								
			System.assert(controller.helper_Request_BI.hasSFDCUser == false);
			System.assert(controller.helper_Request_BI.lstSO_CapabilityGroup.size() >= 1);
			System.assert(controller.helper_Request_BI.lstSO_ContentGroup.size() >= 1);
			System.assert(controller.session.request.BI_Capability_Group__c == null);
		
			controller.helper_Request_BI.searchAlias = oUser_Admin.alias;
			controller.helper_Request_BI.searchByAlias();
		
			System.assert(controller.helper_Request_BI.hasSFDCUser == true);
			System.assert(controller.session.request.Jobtitle_vs__c != null);
			System.assert(controller.session.request.Company_Code__c != null);
			System.assert(controller.session.request.User_Business_Unit_vs__c != null);
			System.assert(controller.session.request.Geographical_Region_vs__c != null);
			System.assert(controller.session.request.Geographical_Subregion__c != null);
			System.assert(controller.session.request.Geographical_Country_vs__c != null);
			System.assert(controller.session.request.Primary_sBU__c != null);

			controller.helper_Request_BI.searchAliasSameAsUser = oUser_Admin.alias;
			controller.helper_Request_BI.searchByAliasSameAsUser();
		
			System.assert(!String.isBlank(controller.session.request.Lastname_same_as_user__c));
								
	//		controller.session.request.BI_Content_Group__c = controller.helper_Request_BI.lstSO_ContentGroup[1].getValue();		
			controller.session.request.Description_Long__c = 'blabla';
			controller.session.request.User_Business_Unit_vs__c = 'CRHF';
				

			System.runAs(oUser_Admin){
				controller.helper_Request_BI.createUserRequest();
				System.assert(controller.session.request.Name != null);	
			}
			
		Test.stopTest();

	}	

	private static testmethod void createUser_Error() {
		
		User oUser_Admin = clsTestData_User.createUser_SystemAdministratorMDT('tstadm1', false);
			oUser_Admin.Primary_sBU__c = 'AF Solutions';
		insert oUser_Admin;

		System.runAs(oUser_Admin){
			createBusinessData(false);
		}
			
		ctrl_Support_Requests controller;							

		Test.startTest();
		
			Test.setMock(WebServiceMock.class, new ws_adUserServiceMockImpl());
		
			controller = new ctrl_Support_Requests();			
			System.assert(controller.userLoggedIn==true);
									
			controller.helper_Request_BI.requestNewUser();
								
			System.assert(controller.helper_Request_BI.hasSFDCUser == false);
			System.assert(controller.helper_Request_BI.lstSO_CapabilityGroup.size() >= 1);
			System.assert(controller.helper_Request_BI.lstSO_ContentGroup.size() >= 1);
			System.assert(controller.session.request.BI_Capability_Group__c == null);
		
			controller.helper_Request_BI.searchAlias = oUser_Admin.alias;
			controller.helper_Request_BI.searchByAlias();
		
			System.assert(controller.helper_Request_BI.hasSFDCUser == true);
			System.assert(controller.session.request.Jobtitle_vs__c != null);
			System.assert(controller.session.request.Company_Code__c != null);
			System.assert(controller.session.request.User_Business_Unit_vs__c != null);
			System.assert(controller.session.request.Geographical_Region_vs__c != null);
			System.assert(controller.session.request.Geographical_Subregion__c != null);
			System.assert(controller.session.request.Geographical_Country_vs__c != null);
			System.assert(controller.session.request.Primary_sBU__c != null);

			controller.helper_Request_BI.searchAliasSameAsUser = oUser_Admin.alias;
			controller.helper_Request_BI.searchByAliasSameAsUser();
		
			System.assert(!String.isBlank(controller.session.request.Lastname_same_as_user__c));
								
	//		controller.session.request.BI_Content_Group__c = controller.helper_Request_BI.lstSO_ContentGroup[1].getValue();		
			controller.session.request.Description_Long__c = 'blabla';
			controller.session.request.User_Business_Unit_vs__c = 'CRHF';

			System.runAs(oUser_Admin){
				String tData = controller.session.request.Firstname__c;
				controller.session.request.Firstname__c = null;
				controller.helper_Request_BI.createUserRequest();
				controller.session.request.Firstname__c = tData;

				tData = controller.session.request.Lastname__c;
				controller.session.request.Lastname__c = null;
				controller.helper_Request_BI.createUserRequest();
				controller.session.request.Lastname__c = tData;

				tData = controller.session.request.Email__c;
				controller.session.request.Email__c = null;
				controller.helper_Request_BI.createUserRequest();
				controller.session.request.Email__c = tData;


				controller.helper_Request_BI.hasSFDCUser = false;

				String tJobtitle = controller.session.request.Jobtitle_vs__c;
				String tUserBusinessUnit = controller.session.request.User_Business_Unit_vs__c;
				String tCompanyCode = controller.session.request.Company_Code__c;
				String tGeographicalCountry = controller.session.request.Geographical_Country_vs__c;
				String tGeographicalSubregion = controller.session.request.Geographical_Subregion__c;
				String tGeographicalRegion = controller.session.request.Geographical_Region_vs__c;


				controller.session.request.Jobtitle_vs__c = null;
				controller.helper_Request_BI.createUserRequest();
				controller.session.request.Jobtitle_vs__c = tJobtitle;
				controller.session.request.User_Business_Unit_vs__c = tUserBusinessUnit;
				controller.session.request.Company_Code__c = tCompanyCode;
				controller.session.request.Geographical_Country_vs__c = tGeographicalCountry;
				controller.session.request.Geographical_Subregion__c = tGeographicalSubregion;
				controller.session.request.Geographical_Region_vs__c = tGeographicalRegion;


				controller.session.request.User_Business_Unit_vs__c = null;
				controller.helper_Request_BI.createUserRequest();
				controller.session.request.Jobtitle_vs__c = tJobtitle;
				controller.session.request.User_Business_Unit_vs__c = tUserBusinessUnit;
				controller.session.request.Company_Code__c = tCompanyCode;
				controller.session.request.Geographical_Country_vs__c = tGeographicalCountry;
				controller.session.request.Geographical_Subregion__c = tGeographicalSubregion;
				controller.session.request.Geographical_Region_vs__c = tGeographicalRegion;


				controller.session.request.Company_Code__c = null;
				controller.helper_Request_BI.createUserRequest();
				controller.session.request.Jobtitle_vs__c = tJobtitle;
				controller.session.request.User_Business_Unit_vs__c = tUserBusinessUnit;
				controller.session.request.Company_Code__c = tCompanyCode;
				controller.session.request.Geographical_Country_vs__c = tGeographicalCountry;
				controller.session.request.Geographical_Subregion__c = tGeographicalSubregion;
				controller.session.request.Geographical_Region_vs__c = tGeographicalRegion;


				controller.session.request.Geographical_Country_vs__c = null;
				controller.helper_Request_BI.createUserRequest();
				controller.session.request.Jobtitle_vs__c = tJobtitle;
				controller.session.request.User_Business_Unit_vs__c = tUserBusinessUnit;
				controller.session.request.Company_Code__c = tCompanyCode;
				controller.session.request.Geographical_Country_vs__c = tGeographicalCountry;
				controller.session.request.Geographical_Subregion__c = tGeographicalSubregion;
				controller.session.request.Geographical_Region_vs__c = tGeographicalRegion;


				controller.session.request.Geographical_Subregion__c = null;
				controller.helper_Request_BI.createUserRequest();
				controller.session.request.Jobtitle_vs__c = tJobtitle;
				controller.session.request.User_Business_Unit_vs__c = tUserBusinessUnit;
				controller.session.request.Company_Code__c = tCompanyCode;
				controller.session.request.Geographical_Country_vs__c = tGeographicalCountry;
				controller.session.request.Geographical_Subregion__c = tGeographicalSubregion;
				controller.session.request.Geographical_Region_vs__c = tGeographicalRegion;


				controller.session.request.Geographical_Region_vs__c = null;
				controller.helper_Request_BI.createUserRequest();
				controller.session.request.Jobtitle_vs__c = tJobtitle;
				controller.session.request.User_Business_Unit_vs__c = tUserBusinessUnit;
				controller.session.request.Company_Code__c = tCompanyCode;
				controller.session.request.Geographical_Country_vs__c = tGeographicalCountry;
				controller.session.request.Geographical_Subregion__c = tGeographicalSubregion;
				controller.session.request.Geographical_Region_vs__c = tGeographicalRegion;

				controller.helper_Request_BI.hasSFDCUser = true;

				controller.helper_Request_BI.createUserRequest();
				
				System.assert(controller.session.request.Name != null);	
			}

		Test.stopTest();

	}	
	
	private static testmethod void genericRequest() {
						
		createBusinessData(true);
		
		User guestUser = [Select Id From User where UserType = 'Guest' AND IsActive = true LIMIT 1];		
		
		System.runAs(guestUser){
			
			Test.startTest();
			Test.setMock(WebServiceMock.class, new ws_adUserServiceMockImpl());
			
			ctrl_Support_Requests controller = new ctrl_Support_Requests();
			
			controller.session.username = 'testuser';
			controller.session.password = 'testpassword';
			controller.doLogin();
			
			System.assert(controller.userLoggedIn==true);
														
			controller.helper_Request_BI.requestServiceRequest();
									
			System.assert(controller.helper_Request_BI.hasSFDCUser == false);						
			System.assert(controller.helper_Request_BI.lstSO_ContentGroup.size() > 1);
						
			controller.helper_Request_BI.searchAlias = 'system';
			controller.helper_Request_BI.searchByAlias();
			
			System.assert(controller.session.request.Email__c != null);
			
			controller.session.request.Area_SR__c = 'Other';
			controller.session.request.Report_Type__c = 'Other';
			controller.session.request.Company_Code__c = 'EUR';			
			controller.session.request.Jobtitle_vs__c = 'Other';
			
			List<SelectOption> regionOptions = controller.helper_Request_BI.lstSO_Region;
			String europe;			
			for(SelectOption region : regionOptions) if(region.getLabel() == 'Europe') europe = region.getValue();
			
			controller.session.request.Region__c = europe;
			controller.helper_Request_BI.regionSelectionChanged();
			
			System.assert(controller.helper_Request_BI.lstSO_Country.size() > 1);
			
			controller.session.request.Country__c = controller.helper_Request_BI.lstSO_Country[1].getValue();
			
			controller.session.request.Business_Unit__c = '[Neuromodulation,Vascular]';
											
			controller.session.request.BI_Content_Group__c = controller.helper_Request_BI.lstSO_ContentGroup[1].getValue();		
			controller.session.request.Description_Long__c = 'blabla';
			controller.session.request.User_Business_Unit_vs__c = 'CRHF';
					
			controller.helper_Request_BI.createGenericServiceRequest();
			
			System.assert(controller.session.request.Name != null);		
		}
	}
	
	private static testmethod void changeUser() {
						
		createBusinessData(true);
										
		Test.startTest();
		
		Test.setMock(WebServiceMock.class, new ws_adUserServiceMockImpl());
		
		ctrl_Support_Requests controller = new ctrl_Support_Requests();			
		System.assert(controller.userLoggedIn==true);
									
		controller.helper_Request_BI.requestChangeUser();
			
		System.assert(controller.helper_Request_BI.lstSO_CapabilityGroup.size() > 1);
		System.assert(controller.helper_Request_BI.lstSO_ContentGroup.size() > 1);
		System.assert(controller.session.request.BI_Capability_Group__c != null);
		
		controller.helper_Request_BI.searchAlias = currentUser.alias;
		controller.helper_Request_BI.searchByAlias();
				
		System.assert(controller.session.request.Email__c != null);
				
		controller.session.request.Change_Type__c = 'Add/modify User Application';
											
		controller.session.request.BI_Content_Group__c = controller.helper_Request_BI.lstSO_ContentGroup[1].getValue();		
						
		controller.helper_Request_BI.createChangeUserRequest();
		
		System.assert(controller.session.request.Name != null);	
	}
		
	private static testmethod void deactivateRequest() {
														
		Test.startTest();
		
		Test.setMock(WebServiceMock.class, new ws_adUserServiceMockImpl());
		
		ctrl_Support_Requests controller = new ctrl_Support_Requests();			
		System.assert(controller.userLoggedIn==true);
									
		controller.helper_Request_BI.requestDeactivateUser();
		
		System.assert(controller.session.request.Date_of_deactivation__c != null);
						
		controller.helper_Request_BI.searchAlias = currentUser.alias;
		controller.helper_Request_BI.searchByAlias();
				
		System.assert(controller.session.request.Email__c != null);
				
		controller.session.request.Deactivation_Reason__c = 'Left Company';
					
		controller.helper_Request_BI.createDeactivateRequest();
		
		System.assert(controller.session.request.Name != null);			
	}
	
	private static void createBusinessData(boolean createGroups) {
		
		Company__c cmpny = new Company__c();		
        cmpny.name='Europe';
        cmpny.CurrencyIsoCode = 'EUR';
        cmpny.Current_day_in_Q1__c=56;
        cmpny.Current_day_in_Q2__c=34; 
        cmpny.Current_day_in_Q3__c=5; 
        cmpny.Current_day_in_Q4__c= 0;   
        cmpny.Current_day_in_year__c =200;
        cmpny.Days_in_Q1__c=56;  
        cmpny.Days_in_Q2__c=34;
        cmpny.Days_in_Q3__c=13;
        cmpny.Days_in_Q4__c=22;
        cmpny.Days_in_year__c =250;
        cmpny.Company_Code_Text__c = 'EUR';
		insert cmpny;
		
		Business_Unit__c bu =  new Business_Unit__c();
        bu.Company__c =cmpny.id;
        bu.name='CRHF';
        bu.Account_Plan_Activities__c=true;        
        insert bu;
        
        Sub_Business_Units__c sbu = new Sub_Business_Units__c();
        sbu.name='Implantables & Diagnostic';
        sbu.Business_Unit__c=bu.id;
        sbu.Account_Plan_Default__c='Revenue';
        sbu.Account_Flag__c = 'AF_Solutions__c';
		insert sbu;
		
		User_Business_Unit__c userSBU = new User_Business_Unit__c();
		userSBU.User__c = UserInfo.getUserId();
		userSBU.Sub_Business_Unit__c = sbu.Id;
		userSBU.Primary__c = true;
		insert userSBU;
		
		DIB_Country__c country = new DIB_Country__c();
		country.Company__c = cmpny.Id;
		country.Name = currentUser.CountryOR__c;
		country.Country_ISO_Code__c = 'XX';
		country.CurrencyIsoCode = 'EUR';
		insert country;
		
		if(createGroups) {
			Support_Request_Parameter__c contentGroup = new Support_Request_Parameter__c();
			contentGroup.Type__c = 'BI - Content Group';
			contentGroup.Value__c = 'Test Content Group';
			contentGroup.Active__c = true;
			
			Support_Request_Parameter__c capabilityGroup = new Support_Request_Parameter__c();
			capabilityGroup.Type__c = 'BI - Capability Group';
			capabilityGroup.Value__c = 'Information Consumer';
			capabilityGroup.Active__c = true;
		
			insert new List<Support_Request_Parameter__c>{contentGroup, capabilityGroup};
		}
		
	}
}