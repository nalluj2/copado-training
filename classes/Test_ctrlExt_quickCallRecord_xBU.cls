@isTest
private class Test_ctrlExt_quickCallRecord_xBU {
	
	private static testmethod void createCallRecord(){
				
		Account acc = new Account();
    	acc.RecordTypeId = [select Id from RecordType where DeveloperName = 'CAN_Buying_Group' and sObjectType = 'Account' and isActive = true].Id;
    	acc.Name = 'UnitTest Account Name';
    	acc.Type = 'Buying Group';
    	acc.Account_Country_vs__c = 'CANADA';
    	
    	insert acc;
		
		Contact cnt = new Contact();
		cnt.AccountId = acc.Id;
		cnt.FirstName = 'Unit Test';
		cnt.LastName = 'Contact';		 
		cnt.Contact_Department__c = 'Cardiology';
		cnt.Contact_Primary_Specialty__c = 'Cardiology'; 
		cnt.Affiliation_To_Account__c = 'Employee';
		cnt.Primary_Job_Title_vs__c = 'Surgeon';
		cnt.Contact_Gender__c = 'Male';
		cnt.RecordTypeId = [select Id from RecordType where DeveloperName = 'Generic_Contact' and sObjectType = 'Contact' and isActive = true].Id;				
		insert cnt;
		
		RecordType xbuRT = [Select Id from RecordType where SobjectType = 'Call_Records__c' AND DeveloperName = 'xBU'];
						
		Test_CallRecord_Utils.initializeXBUCallRecordRelatedData(xbuRT);
		
		cnt = [Select Id, AccountId, Name from Contact where Id=:cnt.Id];
		
		ApexPages.standardController sc = new ApexPages.StandardController(cnt);
		
		ctrlExt_quickCallRecord_xBU controller = new ctrlExt_quickCallRecord_xBU(sc);
		
		System.assert(controller.activityTypes.size()==2);		
		controller.callTopic.Call_Activity_Type__c = controller.activityTypes[1].getValue();				
		controller.activityTypeChanged();
		
		System.assert(controller.subjects.size()==1);
		controller.selectedSubjects = new List<String>{controller.subjects[0].getValue()};
		
		System.assert(controller.bus.size()==1);		
		controller.selectedBUs = new List<String>{controller.bus[0].getValue()};

		controller.callTopic.Call_Topic_Duration__c = 50;
		controller.callTopic.Call_Topic_Lobbying__c  = 'Selling';
				
		controller.save();

		System.assert(controller.isSaveError == false);
	}
	
	private static testmethod void testValidation(){
		
		Account acc = new Account();
    	acc.RecordTypeId = [select Id from RecordType where DeveloperName = 'CAN_Buying_Group' and sObjectType = 'Account' and isActive = true].Id;
    	acc.Name = 'UnitTest Account Name';
    	acc.Type = 'Buying Group';
    	acc.Account_Country_vs__c = 'CANADA';
    	
    	insert acc;
		
		Contact cnt = new Contact();
		cnt.AccountId = acc.Id;
		cnt.FirstName = 'Unit Test';
		cnt.LastName = 'Contact';		 
		cnt.Contact_Department__c = 'Cardiology';
		cnt.Contact_Primary_Specialty__c = 'Cardiology'; 
		cnt.Affiliation_To_Account__c = 'Employee';
		cnt.Primary_Job_Title_vs__c = 'Surgeon';
		cnt.Contact_Gender__c = 'Male';
		cnt.RecordTypeId = [select Id from RecordType where DeveloperName = 'Generic_Contact' and sObjectType = 'Contact' and isActive = true].Id;				
		insert cnt;
		
		RecordType xbuRT = [Select Id from RecordType where SobjectType = 'Call_Records__c' AND DeveloperName = 'xBU'];
						
		Test_CallRecord_Utils.initializeXBUCallRecordRelatedData(xbuRT);
		
		cnt = [Select Id, AccountId, Name from Contact where Id=:cnt.Id];
		
		ApexPages.standardController sc = new ApexPages.StandardController(cnt);
		
		ctrlExt_quickCallRecord_xBU controller = new ctrlExt_quickCallRecord_xBU(sc);
				
		controller.callRecord.Call_Date__c = null;
		controller.callTopic.Call_Topic_Duration__c = -1;
		controller.callContact.Attending_Affiliated_Account__c = null;
		controller.lobbyingMandatory = false;


				
		controller.save();
		
		System.assert(ApexPages.getMessages().size()>0);
	}
	
}