/*
 * Description	 	: This is the class is used to centralize logic around Leads
 * Author        	: Patrick Brinksma
 * Created Date		: 15-07-2013
 */
public class bl_Lead{

	/*
	 * Description		: Populate Id's of Primary and Secondary Health Insurer using the picklist values
	 * Author			: Patrick Brinksma
	 * Created Date		: 15-07-2013
	 */
    public static void populatePrimaryAndSecondaryInsurer(List<Lead> listOfLead){
        Set<String> setOfAccountName = new Set<String>();
        Set<String> setOfCountry = new Set<String>();
        // populate a set of filter criteria to execute SOQL on Account
        for (Lead thisLead : listOfLead){
            if (thisLead.Primary_Health_Insurer_Web__c != null){
                setOfAccountName.add(thisLead.Primary_Health_Insurer_Web__c);
                setOfCountry.add(thisLead.Lead_Country_vs__c);
            }
            if (thisLead.Secondary_Health_Insurer_Web__c!= null){
                setOfAccountName.add(thisLead.Secondary_Health_Insurer_Web__c);
                setOfCountry.add(thisLead.Lead_Country_vs__c);
            }
        }
        if (!setOfAccountName.isEmpty()){
        	// Query for all Health Insurance Accounts filtered by name and country
            List<Account> listOfAccount = [select Id, Name from Account where Name in :setOfAccountName and Account_Country_vs__c in :setOfCountry and Type = 'Health Insurance'];
            // Populate a Map of Account Name to Id
            Map<String, Id> mapOfAccountNameToId = new Map<String, Id>();
            for (Account thisAccount : listOfAccount){
                mapOfAccountNameToId.put(thisAccount.Name, thisAccount.Id);
            }
            // Populate the Ids of the Primary and Secondary Health Insurer
            for (Lead thisLead : listOfLead){
                if (thisLead.Primary_Health_Insurer_Web__c != null){
                    thisLead.Primary_Health_Insurer_ID__c = mapOfAccountNameToId.get(thisLead.Primary_Health_Insurer_Web__c);
                }    
                if (thisLead.Secondary_Health_Insurer_Web__c!= null){
                    thisLead.Secondary_Health_Insurer_ID__c= mapOfAccountNameToId.get(thisLead.Secondary_Health_Insurer_Web__c);
                }    
            }
        }
    }

	/*
	 * Description		: Assign Leads via Assignment Rule object
	 * Author			: Patrick Brinksma
	 * Created Date		: 15-07-2013
	 */    
    public static void assignLeads(List<Lead> listOfLead, Set<String> setOfPostalCode){
		// Map of Postal Code to Assignment Rule Object
	    Map<string,Country_Assignment_Rule__c> mapOFPostalCoadandCAR = new Map<string,Country_Assignment_Rule__c>();  
    	// Get all assignment rules for the postal / zip codes and bucket
        List<Country_Assignment_Rule__c> listOFCar = [Select Assigned_To_1__c, Assigned_To_2__c,Assignment_Value__c from Country_Assignment_Rule__c where Assignment_Value__c in :setOfPostalCode];
        if(!listOFCar.isEmpty()){
        	// Put result in Map for easy searching
            for(Country_Assignment_Rule__c carObj : listOFCar){      
            	// Make it uppercase for comparison     
                mapOFPostalCoadandCAR.put(carObj.Assignment_Value__c.toUpperCase(),carObj);
            }  
            for(Lead leadobj : listOfLead){
                String strLeadPostalCode = leadobj.Lead_Zip_Postal_Code__c;
                if (strLeadPostalCode != null){
                	strLeadPostalCode = strLeadPostalCode.toUpperCase();
                }
                // Let's see if we can find postal / zip code in rules
                Country_Assignment_Rule__c carObj = mapOFPostalCoadandCAR.get(strLeadPostalCode.left(3));
                // If not found, use bucket
                if (carObj == null){
                	// Every map item was made Uppercase, so use BUCKET to get entry
                	carObj = mapOFPostalCoadandCAR.get(ut_StaticValues.COUNTRY_ASSIGNMENT_BUCKET);
                }
                if (carObj != null){
	                leadobj.Assigned_To_ID__c = carObj.Assigned_To_2__c;
	                leadObj.OwnerId = carObj.Assigned_To_1__c;
	                leadObj.DiB_Sales_Rep_Selector__c = carObj.Assigned_To_1__c;
                }
            }           
        }
	}

	/*
	 * Description		: Return Map of RecordType Id to DeveloperName based on Set of RecordType DeveloperNames for Lead
	 * Author			: Patrick Brinksma
	 * Created Date		: 15-07-2013
	 */    
    public static Map<Id, String> getRecordTypeIdsByDeveloperNames(Set<String> setOfLeadRecordTypeName){
    	Map<Id, String> mapOfLeadRecordTypeIdToDeveloperName = new Map<Id, String>();
    	List<RecordType> listOfRecordType = [select Id, DeveloperName from RecordType where sObjectType = 'Lead' and DeveloperName in :setOfLeadRecordTypeName];
    	for (RecordType thisRecordType : listOfRecordType){
    		mapOfLeadRecordTypeIdToDeveloperName.put(thisRecordType.Id, thisRecordType.DeveloperName);
    	}
    	return mapOfLeadRecordTypeIdToDeveloperName;
    }


    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Author       : Bart Caelen
    // Date         : 20150209
    // Description  : CR-7099
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    public static void verifyAndSetPhysicianAccountId(List<Lead> lstLead){

        // Collect all Physicians (Contact_ID__c) of the processing Leads
        Set<Id> setID_Contact = new Set<Id>();
        for (Lead oLead : lstLead){
            setID_Contact.add(oLead.Contact_ID__c);
        }

        // Collect all relationships for the collected Physicians (Contacts)
        // This results in a map of Contact ID and a set of ALL the related Account ID's.
        // The Primary Account ID of the Contact is also in a separated map.
        List<Affiliation__c> lstAffiliation =
            [
                SELECT 
                    Id
                    , Name
                    , Affiliation_From_Contact__c
                    , Affiliation_To_Account__c
                    , Affiliation_Active__c
                    , Affiliation_Primary__c
                FROM
                    Affiliation__c
                WHERE 
                    RecordType.Name = 'C2A'
                    AND Affiliation_Active__c = true
                    AND Affiliation_From_Contact__c = :setID_Contact
                ORDER BY 
                    Affiliation_From_Contact__c
            ];

        Map<Id, Set<Id>> mapContactID_AccountIDs = new Map<Id, Set<Id>>();
        Map<Id, Id> mapContactID_PrimaryAccountId = new Map<Id, Id>();
        for (Affiliation__c oAffiliation : lstAffiliation){
            Set<Id> setId_Account = new Set<Id>();
            if (mapContactID_AccountIDs.containsKey(oAffiliation.Affiliation_From_Contact__c)){
                setId_Account = mapContactID_AccountIDs.get(oAffiliation.Affiliation_From_Contact__c);
            }
            setId_Account.add(oAffiliation.Affiliation_To_Account__c);
            mapContactID_AccountIDs.put(oAffiliation.Affiliation_From_Contact__c, setId_Account);

            if (oAffiliation.Affiliation_Primary__c){
                mapContactID_PrimaryAccountId.put(oAffiliation.Affiliation_From_Contact__c, oAffiliation.Affiliation_To_Account__c);
            }
        }

        // Loop through the processing Leads
        for (Lead oLead : lstLead){

            Id idContact = oLead.Contact_ID__c;
            Id idAccount = oLead.Account_ID__c;

            if (idContact != null){
                if (idAccount == null){
                    // If the Account_ID__c is empty, we need to set the Primary Account of the Physician (Contact)
                    if (mapContactID_PrimaryAccountId.containsKey(idContact)){
                        oLead.Account_ID__c = mapContactID_PrimaryAccountId.get(idContact);
                    }else{
                        // No Primary Account found - should never happen, but just in case.....
                        oLead.Account_ID__c.addError('There is no primary account specified for this Physician.');
                    }
                }else{
                    // If the Account_ID__c is not empty, we need to validate that this Account is related to the Physician (Contact)
                    if (mapContactID_AccountIDs.containsKey(idContact)){
                        Set<Id> setID_Account = mapContactID_AccountIDs.get(idContact);
                        if (!setID_Account.contains(idAccount)){
                            // Show/Throw an ERROR when there is no relationship between the Physician_Account_ID__c and the Contact
                            oLead.Account_ID__c.addError('Physician is not related to the chosen account. Leave account blank to automatically retrieve the physicians primary account.');
                        }
                    }
                }
            }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------


    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Author       : Bart Caelen
    // Date         : 20/01/2017
    // Description  : Find all Contacts based on teh provided email address and use the first Contact to populate fields on the provided Lead.
    // Input        : 
    //                  - A Map of Email Address and a correspoding Lead that will be used to find the Contacts and the provided Lead will be used to populate fields based on the first found Contact
    //                  - Map of Lead Field and Account/Contact Field (API names)
    // Output       :
    //                  A Map of Email Address and a corresonding list of found Contacts
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    public static Map<String, List<Contact>> findContactAndPopulateLead(Map<String, Lead> mapEmail_Lead, Map<String, String> mapLeadField_AccountContactField){

        Map<String, List<Contact>> mapEmail_Contacts = new Map<String, List<Contact>>();

        Set<String> setField = new Set<String>();
            setField.add('Id');
            setField.add('Name');
            setField.add('AccountId');
            setField.add('Email');
            setField.addAll(mapLeadField_AccountContactField.values());

        Set<String> setEmail = mapEmail_Lead.keySet();

        // Create SOQL to select the Contacts with the same email address as the Lead
        String tSOQL = '';
        for (String tField : setField){
            tSOQL += tField + ',';
        }
        tSOQL = tSOQL.left(tSOQL.length()-1);
        tSOQL = 'SELECT ' + tSOQL;
        tSOQL +=' FROM Contact';
        tSOQL += ' WHERE Email in :setEmail';
        tSOQL += ' ORDER BY Email ASC, CreatedDate DESC';

        List<Contact> lstContact = Database.query(tSOQL);

        // Get a map of Email and all Contacts with this email
        for (Contact oContact : lstContact){

            List<Contact> lstContact_Tmp = new List<Contact>();
            if (mapEmail_Contacts.containsKey(oContact.Email)){
                lstContact_Tmp = mapEmail_Contacts.get(oContact.Email);
            }
            lstContact_Tmp.add(oContact);
            mapEmail_Contacts.put(oContact.Email, lstContact_Tmp);

        }

        for (String tEmail : mapEmail_Contacts.keySet()){

            Lead oLead = mapEmail_Lead.get(tEmail);

            // At least 1 Contact is found - use the first contact to populate the lead fields but only copy data if not null
            List<Contact> lstContact_Tmp = mapEmail_Contacts.get(tEmail);
            for (String tField : mapLeadField_AccountContactField.keySet()){

                String tAccountContactField = mapLeadField_AccountContactField.get(tField);
                List<String> lstFieldPart = tAccountContactField.split('\\.');
                if (lstFieldPart.size() == 1){
                    if (lstContact_Tmp[0].get(lstFieldPart[0]) != null){
                        oLead.put(tField, lstContact_Tmp[0].get(lstFieldPart[0]));
                    }
                }else if (lstFieldPart.size() == 2){
                    if (lstContact_Tmp[0].getSobject(lstFieldPart[0]).get(lstFieldPart[1]) != null){
                        oLead.put(tField, lstContact_Tmp[0].getSobject(lstFieldPart[0]).get(lstFieldPart[1]));
                    }
                }

            }

            mapEmail_Lead.put(tEmail, oLead);

        }

        return mapEmail_Contacts;

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------

}