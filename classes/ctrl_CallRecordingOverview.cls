//----------------------------------------------------------------------------
// Author		: Bart Caelen
// Date			: 15/10/2018
// Description	: This class is the Controller for the VF Component  "CallRecordingOverview" 
//					and it displays the Call Recording Fields related to the Account or Contact.
//----------------------------------------------------------------------------
public class ctrl_CallRecordingOverview {

	//------------------------------------------------------------------------
	// Private variables
	//------------------------------------------------------------------------
	private Boolean bShowUserSBUOnly = false;
	private Boolean bShowPrimarySBUOnly = false;

	private Id id_PrimaryBUG;
	private Id id_PrimaryBU;
	private Id id_PrimarySBU;

	private String tMasterDataRegion = 'Europe';
	//------------------------------------------------------------------------


	//------------------------------------------------------------------------
	// Constructor
	//------------------------------------------------------------------------
	public ctrl_CallRecordingOverview(){

		bShowCallRecordingOverview = true;
		bShowSelectView = false;
		bShowBUGData = true;
		bShowColorMapping = true;
		bFullPage = false;
		bShowActions = true;
		tSelectedView = 'PRIMARYSBUONLY';
		lstwData = new List<wData>();

		Map<String, String> mapURLParameter = ApexPages.currentPage().getParameters();

		if (mapURLParameter.containsKey('id')){
			id_Master = mapURLParameter.get('Id');
		}

		if (String.isBlank(id_Master)){
			ApexPages.Message oMessage = new ApexPages.Message(ApexPages.Severity.Error, 'No correct ID specified');
			ApexPages.addMessage(oMessage);

			wData owData = new wData();
			lstwData.add(owData);
			bShowSelectView = false;
			bShowActions = false;
			bShowCallRecordingOverview = false;
		}else{
	
			if (mapURLParameter.containsKey('selectedView')){
				tSelectedView = mapURLParameter.get('selectedView');
			}
			if (mapURLParameter.containsKey('fullPage')){
				if (mapURLParameter.get('fullPage') == '1'){
					bFullPage = true;
					bShowSelectView = true;
				}
			}

			if (String.valueOf(id_Master).startsWith('001')){
		
				Account oAccount = loadAccount(id_Master);
													 
				tRedirectLabel = 'Back to Account "' + oAccount.Name + '"';
				tObjectName = 'Account';
			
			}else if (String.valueOf(id_Master).startsWith('003')){
		
				Contact oContact = loadContact(id_Master);
															 
				tRedirectLabel = 'Back to Contact "' + oContact.Name + '"';
				tObjectName = 'Contact';

			}

			lstwData = loadData(tMasterDataRegion);
		
		}
	}
	//------------------------------------------------------------------------


	//------------------------------------------------------------------------
	// Getters & Setters
	//------------------------------------------------------------------------
	public Id id_Master { get; private set; }
	public List<wData> lstwData { get; private set; }
	public Boolean bShowCallRecordingOverview { get; private set; }
	public Boolean bShowSelectView { get; private set; }
	public Boolean bShowBUGData { get; private set; }
	public Boolean bShowColorMapping  { get; set; }

	public Boolean bFullPage { get; private set; }
	public Boolean bShowActions  { get; private set; }

	public String tRedirectLabel { get; private set;}
	public String tObjectName { get; private set; }

	public String tSelectedView { get; set; }
	public List<SelectOption> lstSO_SelectView {
		get {
			List<SelectOption> lstSO = new List<SelectOption>();
				lstSO.add(new SelectOption('PRIMARYSBUONLY','My Primary Business Unit'));
				lstSO.add(new SelectOption('USERSBUONLY','My Business Units'));
				lstSO.add(new SelectOption('ALL','All Business Units'));
			return lstSO;
		}
	}
	//------------------------------------------------------------------------


	//------------------------------------------------------------------------
	// Private Methods
	//------------------------------------------------------------------------
	private Account loadAccount(Id id_Account){

		Account oAccount = [SELECT Id, Name, SAP_ID__c, Country_Region__c FROM Account WHERE Id = :id_Account];
		return oAccount;

	}

	private Contact loadContact(Id id_Contact){

		Contact oContact = [SELECT Id, Name, Account.Country_Region__c FROM Contact WHERE Id = :id_Contact];
		return oContact;

	}

	private List<wData> loadData(String tMasterDataRegion){
	
		List<wData> lstwData = new List<wData>();
		bShowPrimarySBUOnly = false;
		bShowUserSBUOnly = false;

		if (id_Master != null){


			if (!String.isBlank(tMasterDataRegion)){

				if (tSelectedView == 'ALL'){
						
					Map<Id, Call_Recording_Overview__c> mapCallRecordOverview = loadCallRecordOverview(id_Master);
					lstwData = loadMasterData(tMasterDataRegion, mapCallRecordOverview);
					
				}else if (tSelectedView == 'USERSBUONLY'){
						
					bShowUserSBUOnly = true;
					Map<Id, Call_Recording_Overview__c> mapCallRecordOverview = loadCallRecordOverview(id_Master);
					lstwData = loadMasterData_UserSBU(tMasterDataRegion, mapCallRecordOverview);

				}else if (String.isBlank(tSelectedView) || tSelectedView == 'PRIMARYSBUONLY'){
						
					bShowPrimarySBUOnly = true;
					Map<Id, Call_Recording_Overview__c> mapCallRecordOverview = loadCallRecordOverview(id_Master);
					lstwData = loadMasterData_UserSBU(tMasterDataRegion, mapCallRecordOverview);

				}

			}

		}

		return lstwData;

	}

	private List<wData> loadMasterData_UserSBU(String tMasterDataRegion, Map<Id, Call_Recording_Overview__c> mapCallRecordOverview){
	 
		String tSOQL = 'SELECT Primary__c';
		tSOQL += ', Sub_Business_Unit__r.Id, Sub_Business_Unit__r.Name';
		tSOQL += ', Sub_Business_Unit__r.Business_Unit__r.Id, Sub_Business_Unit__r.Business_Unit__r.Name';
		tSOQL += ', Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__r.Id, Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__r.Name';
		tSOQL += ' FROM User_Business_Unit__c';
		tSOQL += ' WHERE User__c = \'' + UserInfo.getUserId() + '\'';
		tSOQL += ' AND Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__r.Master_Data__r.Name = :tMasterDataRegion';
		if (bShowPrimarySBUOnly){
			tSOQL += ' AND Primary__c = true';
		}
		tSOQL += ' ORDER BY Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__r.Name';
		tSOQL += ', Sub_Business_Unit__r.Business_Unit__r.Name';
		tSOQL += ', Sub_Business_Unit__r.Name';

		List<User_Business_Unit__c> lstUserBusinessUnit = Database.query(tSOQL);

		Set<Id> setID_BUG = new Set<Id>();
		Set<Id> setID_BU = new Set<Id>();
		Set<Id> setID_SBU = new Set<Id>();

		for (User_Business_Unit__c oUserBusinessUnit : lstUserBusinessUnit){
			setID_BUG.add(oUserBusinessUnit.Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__r.Id);
			setID_BU.add(oUserBusinessUnit.Sub_Business_Unit__r.Business_Unit__r.Id);
			setID_SBU.add(oUserBusinessUnit.Sub_Business_Unit__r.Id);

			if (oUserBusinessUnit.Primary__c){
				id_PrimaryBUG = oUserBusinessUnit.Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__r.Id;
				id_PrimaryBU = oUserBusinessUnit.Sub_Business_Unit__r.Business_Unit__r.Id;
				id_PrimarySBU = oUserBusinessUnit.Sub_Business_Unit__r.Id;
			}
		}

		return loadMasterData(tMasterDataRegion, setID_BUG, setID_BU, setID_SBU, mapCallRecordOverview);
	
	}

	private List<wData> loadMasterData(String tMasterDataRegion, Map<Id, Call_Recording_Overview__c> mapCallRecordOverview){
		return loadMasterData(tMasterDataRegion, null, null, null, mapCallRecordOverview);
	}
	private List<wData> loadMasterData(String tMasterDataRegion, Set<Id> setID_BUG, Set<Id> setID_BU, Set<Id> setID_SBU, Map<Id, Call_Recording_Overview__c> mapCallRecordOverview){

		List<wData> lstwData = new List<wData>();

		// Load BUG with BU's
		String tSOQL = 'SELECT Id, Name';
			tSOQL += ', (';
				tSOQL += ' SELECT Id, Name ';
				tSOQL += ' FROM Business_Units__r';
				if (setID_BU != null){
					tSOQL += ' WHERE Id in :setID_BU';
				}
				tSOQL += ' ORDER BY Name';
			tSOQL += ' )';
			tSOQL += ' FROM Business_Unit_Group__c';
			tSOQL += ' WHERE Master_Data__r.Name = :tMasterDataRegion';
			if (setID_BUG != null){
				tSOQL += ' AND Id in :setID_BUG';
			}
			tSOQL += ' ORDER BY Name';

			System.debug('**BC** tSOQL : ' + tSOQL);

		List<Business_Unit_Group__c> lstBUG = Database.Query(tSOQL);

		// Load BU with SBU's
		tSOQL = 'SELECT Id, Name';
			tSOQL += ', (';
				tSOQL += ' SELECT Id, Name ';
				tSOQL += ' FROM Sub_Business_Units__r';
				if (setID_BU != null){
					tSOQL += ' WHERE Id in :setID_SBU';
				}
				tSOQL += ' ORDER BY Name';
			tSOQL += ' )';
			tSOQL += ' FROM Business_Unit__c';
			tSOQL += ' WHERE Business_Unit_Group__r.Master_Data__r.Name = :tMasterDataRegion';
			if (setID_BUG != null){
				tSOQL += ' AND Id in :setID_BU';
			}
			tSOQL += ' ORDER BY Name';

		Map<Id, Business_Unit__c> mapBU = new Map<Id, Business_Unit__c>((List<Business_Unit__c>)Database.Query(tSOQL));

		for (Business_Unit_Group__c oBUG : lstBUG){

			wData owData = new wData();

			Call_Recording_Overview__c oCallRecordingOverview_BUG = new Call_Recording_Overview__c();
			if (mapCallRecordOverview.containsKey(oBUG.Id)) oCallRecordingOverview_BUG = mapCallRecordOverview.get(oBUG.Id);
																												
			
			Map<Id, List<wData_BU>> mapwData_BU = new Map<Id, List<wData_BU>>();
			Map<Id, List<wData_SBU>> mapwData_SBU = new Map<Id, List<wData_SBU>>();
			for (Business_Unit__c oBU : oBUG.Business_Units__r){

				oBU = mapBU.get(oBU.Id);

				Call_Recording_Overview__c oCallRecordingOverview_BU = null;
				if (mapCallRecordOverview.containsKey(oBU.Id)){
	
					oCallRecordingOverview_BU = mapCallRecordOverview.get(oBU.Id);
					
					if (oCallRecordingOverview_BUG.Number_Of_Calls__c == null) oCallRecordingOverview_BUG.Number_Of_Calls__c = 0;
					oCallRecordingOverview_BUG.Number_Of_Calls__c += oCallRecordingOverview_BU.Number_Of_Calls__c;
					if (oCallRecordingOverview_BUG.Last_Call_Date__c == null){
						oCallRecordingOverview_BUG.Last_Call_Date__c = oCallRecordingOverview_BU.Last_Call_Date__c;
					}else{
						if (oCallRecordingOverview_BU.Last_Call_Date__c > oCallRecordingOverview_BUG.Last_Call_Date__c){
							oCallRecordingOverview_BUG.Last_Call_Date__c = oCallRecordingOverview_BU.Last_Call_Date__c;
						}
					}
	
				}else{

					oCallRecordingOverview_BU = new Call_Recording_Overview__c();
						oCallRecordingOverview_BU.Account__c = id_Master;
						oCallRecordingOverview_BU.Business_Unit_Group__c = oBUG.Id;
						oCallRecordingOverview_BU.Business_Unit__c  = oBU.Id;
						oCallRecordingOverview_BU.Number_Of_Calls__c = 0;
						oCallRecordingOverview_BU.Last_Call_Date__c = null;

				}
				wData_BU owData_BU = new wData_BU(oBU, oCallRecordingOverview_BU, oBU.Sub_Business_Units__r.size(), false);

				List<wData_BU> lstwData_BU = new List<wData_BU>();
				if (mapwData_BU.containsKey(oBUG.Id)) lstwData_BU = mapwData_BU.get(oBUG.Id);
				lstwData_BU.add(owData_BU);
				mapwData_BU.put(oBUG.Id, lstwData_BU);

				Integer iCounter = 1;
				Integer iMaxCounter = oBU.Sub_Business_Units__r.size();
				for (Sub_Business_Units__c oSBU : oBU.Sub_Business_Units__r){

					Call_Recording_Overview__c oCallRecordingOverview_SBU = null;
					if (mapCallRecordOverview.containsKey(oSBU.Id)){
	
						oCallRecordingOverview_SBU = mapCallRecordOverview.get(oSBU.Id);
	
					}else{

						oCallRecordingOverview_SBU = new Call_Recording_Overview__c();
							oCallRecordingOverview_SBU.Account__c = id_Master;
							oCallRecordingOverview_SBU.Business_Unit_Group__c = oBUG.Id;
							oCallRecordingOverview_SBU.Business_Unit__c  = oBU.Id;
							oCallRecordingOverview_SBU.Sub_Business_Unit__c  = oSBU.Id;
							oCallRecordingOverview_SBU.Number_Of_Calls__c = 0;
							oCallRecordingOverview_SBU.Last_Call_Date__c = null;

					}
					wData_SBU owData_SBU = new wData_SBU(oSBU, oCallRecordingOverview_SBU, false);

					List<wData_SBU> lstwData_SBU = new List<wData_SBU>();
					if (mapwData_SBU.containsKey(oBU.Id)) lstwData_SBU = mapwData_SBU.get(oBU.Id);
					if (iCounter == iMaxCounter) owData_SBU.bIsLast = true;
					lstwData_sBU.add(owData_SBU);
					mapwData_SBU.put(oBU.Id, lstwData_SBU);


					iCounter++;
				}

			}

			owData.owData_BUG = new wData_BUG(oBUG, oCallRecordingOverview_BUG, oBUG.Business_Units__r.size(), false);
			owData.mapwData_BU = mapwData_BU;
			owData.mapwData_SBU = mapwData_SBU;

			lstwData.add(owData);

		}

		return lstwData;
	}

	private Map<Id, Call_Recording_Overview__c> loadCallRecordOverview(Id id_Master){

		Map<Id, Call_Recording_Overview__c> mapId_CallRecordOverview = new Map<Id, Call_Recording_Overview__c>();

		List<Call_Recording_Overview__c> lstCallRecordOverview =
		[
			SELECT 
				Id, Business_Unit_Group__c, Business_Unit__c, Sub_Business_Unit__c
				, Last_Call_Date__c, Number_Of_Calls__c
			FROM 
				Call_Recording_Overview__c				
			WHERE 
				(Account__c = :id_Master) OR (Contact__c = :id_Master)
			ORDER BY
				Business_Unit_Group__r.Name, Business_Unit__r.Name, Sub_Business_Unit__r.Name
		];

		for (Call_Recording_Overview__c oCallRecordingOverview : lstCallRecordOverview){
		
			if (oCallRecordingOverview.Sub_Business_Unit__c != null){

				// Add Data on SBU level
				mapId_CallRecordOverview.put(oCallRecordingOverview.Sub_Business_Unit__c, oCallRecordingOverview);

			}else if (oCallRecordingOverview.Business_Unit__c != null){

				// Add Data on BU level
				mapId_CallRecordOverview.put(oCallRecordingOverview.Business_Unit__c, oCallRecordingOverview);

			}else if (oCallRecordingOverview.Business_Unit_Group__c != null){

				// Add Data on BUG level
				mapId_CallRecordOverview.put(oCallRecordingOverview.Business_Unit_Group__c, oCallRecordingOverview);

			}

		}

		System.debug('**BC** loadCallRecordOverview - mapId_CallRecordOverview : ' + mapId_CallRecordOverview);
		return mapId_CallRecordOverview;
	}
	//------------------------------------------------------------------------


	//------------------------------------------------------------------------
	// ACTIONS
	//------------------------------------------------------------------------
	public void refreshData(){

		lstwData = loadData(tMasterDataRegion);

	}

	public PageReference redirectToMaster(){
	
		PageReference oPageReference = new PageReference('/' + id_Master);
		    oPageReference.setRedirect(true);
		return oPageReference;

	}
	//------------------------------------------------------------------------


	//------------------------------------------------------------------------
	// Wrappers
	//------------------------------------------------------------------------
	public class wData {
	
		public wData_BUG owData_BUG { get; set; }
		public Map<Id, List<wData_BU>> mapwData_BU { get; set; }
		public Map<Id, List<wData_SBU>> mapwData_SBU { get; set; }

		Public wData(){
			owData_BUG = new wData_BUG();
			mapwData_BU = new Map<Id, List<wData_BU>>();
			mapwData_SBU = new Map<Id, List<wData_SBU>>();
        }

	}

	public class wData_BUG {

		public Business_Unit_Group__c oBUG  { get; set; }
		public Call_Recording_Overview__c oCallRecordingOverview { get; set; }
		public Integer iRecordCountChild { get; set; }
		public Boolean bIsPrimary { get; set; }
		public Boolean bShowData { get; set; }
		public Boolean bNoData { get; set; }
		public String tColor_Call { get; set; }
		public String tColor_Date { get; set; }
		public String tDateNA { get; set; }

		private String tColor_Red = '#FF0000';
		private String tColor_Orange = '#FFA500';

        Public wData_BUG(){
			oBUG = null;
			oCallRecordingOverview = new Call_Recording_Overview__c();
			iRecordCountChild = 0;
			bIsPrimary = false;
			bShowData = true;
			bNoData = false;
			tColor_Call = '';
			tColor_Date = '';
			tDateNA = '';
        }

        Public wData_BUG(Business_Unit_Group__c aoBUG, Call_Recording_Overview__c aoCallRecordingOverview, Integer aiRecordCountChild, Boolean abIsPrimary){
			oBUG = aoBUG;
			oCallRecordingOverview = aoCallRecordingOverview;
			iRecordCountChild = aiRecordCountChild;
			bIsPrimary = abIsPrimary;
			bShowData = true;
			bNoData = false;
			tColor_Call = '';
			tColor_Date = '';
			tDateNA = '';
			if ( (oCallRecordingOverview == null) ){//|| (oCallRecordingOverview.Id == null) ){
				bNoData  = true;
				tColor_Call = tColor_Red;
				tColor_Date = tColor_Red;
				tDateNA = 'N/A';
			}else{
				if (oCallRecordingOverview.Number_Of_Calls__c == null) oCallRecordingOverview.Number_Of_Calls__c = 0;
				if (oCallRecordingOverview.Number_Of_Calls__c == 0) tColor_Call = tColor_Red;
				if (oCallRecordingOverview.Last_Call_Date__c == null){
					tColor_Date = tColor_Red;
					tDateNA = 'N/A';
				}else{
					Integer iNumberDaysDue = oCallRecordingOverview.Last_Call_Date__c.daysBetween(Date.today());
					if (iNumberDaysDue > 90){
						tColor_Date = tColor_Orange;
					}
				}
			}
        }

	}

	public class wData_BU {

		public Business_Unit__c oBU  { get; set; }
		public Call_Recording_Overview__c oCallRecordingOverview { get; set; }
		public Integer iRecordCountChild { get; set; }
		public Boolean bIsPrimary { get; set; }
		public Boolean bShowData { get; set; }
		public Boolean bNoData { get; set; }
		public String tColor_Call { get; set; }
		public String tColor_Date { get; set; }
		public String tDateNA { get; set; }

		private String tColor_Red = '#FF0000';
		private String tColor_Orange = '#FFA500';

        Public wData_BU(){
			oBU = null;
			oCallRecordingOverview = new Call_Recording_Overview__c();
			iRecordCountChild = 0;
			bIsPrimary = false;
			bShowData = true;
			bNoData = false;
			tColor_Call = '';
			tColor_Date = '';
			tDateNA = '';
        }

	    Public wData_BU(Business_Unit__c aoBU, Call_Recording_Overview__c aoCallRecordingOverview, Integer aiRecordCountChild, Boolean abIsPrimary){
			oBU = aoBU;
			oCallRecordingOverview = aoCallRecordingOverview;
			iRecordCountChild = aiRecordCountChild;
			bIsPrimary = abIsPrimary;
			bShowData = true;
			bNoData = false;
			tColor_Call = '';
			tColor_Date = '';
			tDateNA = '';
			if ( (oCallRecordingOverview == null) || (oCallRecordingOverview.Id == null) ){
				bNoData  = true;
				tColor_Call = tColor_Red;
				tColor_Date = tColor_Red;
				tDateNA = 'N/A';
			}else{
				if (oCallRecordingOverview.Number_Of_Calls__c == null) oCallRecordingOverview.Number_Of_Calls__c = 0;
				if (oCallRecordingOverview.Number_Of_Calls__c == 0) tColor_Call = tColor_Red;
				if (oCallRecordingOverview.Last_Call_Date__c == null){
					tColor_Date = tColor_Red;
					tDateNA = 'N/A';
				}else{
					Integer iNumberDaysDue = oCallRecordingOverview.Last_Call_Date__c.daysBetween(Date.today());
					if (iNumberDaysDue > 90){
						tColor_Date = tColor_Orange;
					}
				}
			}
        }

	}

	public class wData_SBU {

		public Sub_Business_Units__c oSBU  { get; set; }
		public Call_Recording_Overview__c oCallRecordingOverview{ get; set; }
		public Boolean bIsPrimary { get; set; }
		public Boolean bShowData { get; set; }
		public Boolean bNoData { get; set; }
		public String tColor_Call { get; set; }
		public String tColor_Date { get; set; }
		public String tDateNA { get; set; }
		public Boolean bIsLast { get; set; }

		private String tColor_Red = '#FF0000';
		private String tColor_Orange = '#FFA500';

        Public wData_SBU(){
			oSBU = null;
			oCallRecordingOverview = new Call_Recording_Overview__c();
			bIsPrimary = false;
			bShowData = true;
			bNoData = false;
			tColor_Call = '';
			tColor_Date = '';
			tDateNA = '';
			bIsLast = false;
        }

	    Public wData_SBU(Sub_Business_Units__c aoSBU, Call_Recording_Overview__c aoCallRecordingOverview, Boolean abIsPrimary){
			oSBU = aoSBU;
			oCallRecordingOverview = aoCallRecordingOverview;
			bIsPrimary = abIsPrimary;
			bShowData = true;
			bNoData = false;
			tColor_Call = '';
			tColor_Date = '';
			tDateNA = '';
			bIsLast = false;
			if ( (oCallRecordingOverview == null) || (oCallRecordingOverview.Id == null) ){
				bNoData  = true;
				tColor_Call = tColor_Red;
				tColor_Date = tColor_Red;
				tDateNA = 'N/A';
			}else{
				if (oCallRecordingOverview.Number_Of_Calls__c == null) oCallRecordingOverview.Number_Of_Calls__c = 0;
				if (oCallRecordingOverview.Number_Of_Calls__c == 0) tColor_Call = tColor_Red;
				if (oCallRecordingOverview.Last_Call_Date__c == null){
					tColor_Date = tColor_Red;
					tDateNA = 'N/A';
				}else{
					Integer iNumberDaysDue = oCallRecordingOverview.Last_Call_Date__c.daysBetween(Date.today());
					if (iNumberDaysDue > 90){
						tColor_Date = tColor_Orange;
					}
				}
			}
        }
	}
	//------------------------------------------------------------------------

}