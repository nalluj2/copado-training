@isTest public class clsTestData_Therapy {
	
	public static Integer iRecord_TherapyGroup = 1;
	public static List<Therapy_Group__c> lstTherapyGroup = new List<Therapy_Group__c>();
	public static Map<Id, Therapy_Group__c> mapTherapyGroup_Main = new Map<Id, Therapy_Group__c>();
	public static Therapy_Group__c oMain_TherapyGroup;

	public static Integer iRecord_Therapy = 1;
	public static List<Therapy__c> lstTherapy = new List<Therapy__c>();
	public static Map<Id, Therapy__c> mapTherapy_Main = new Map<Id, Therapy__c>();
	public static Therapy__c oMain_Therapy;


	//---------------------------------------------------------------------------------------------------
    // Create Therapy Group Data
    //---------------------------------------------------------------------------------------------------
    public static List<Therapy_Group__c> createTherapyGroup() {
    	return createTherapyGroup(true);
    }
    public static List<Therapy_Group__c> createTherapyGroup(Boolean bInsert) {

        if (oMain_TherapyGroup == null){

            if (clsTestData_MasterData.oMain_Company == null) clsTestData_MasterData.createCompany();
            if (clsTestData_MasterData.oMain_BusinessUnitGroup == null) clsTestData_MasterData.createBusinessUnitGroup();
            if (clsTestData_MasterData.oMain_BusinessUnit == null) clsTestData_MasterData.createBusinessUnit();
            if (clsTestData_MasterData.oMain_SubBusinessUnit == null) clsTestData_MasterData.createSubBusinessUnit();

            for (Sub_Business_Units__c oSubBusinessUnit : clsTestData_MasterData.lstSubBusinessUnit){
                for (Integer i=0; i<iRecord_TherapyGroup; i++){
                    Therapy_Group__c oTherapyGroup = new Therapy_Group__c();
                        oTherapyGroup.Name = 'TEST TG-' +  clsTestData_MasterData.mapBusinessUnit_Main.get(oSubBusinessUnit.Business_Unit__c).Name + '-' + oSubBusinessUnit.Name;
                        oTherapyGroup.Sub_Business_Unit__c = oSubBusinessUnit.Id;
                        oTherapyGroup.Company__c = clsTestData_MasterData.mapBusinessUnit_Main.get(oSubBusinessUnit.Business_Unit__c).Company__c;
                    lstTherapyGroup.add(oTherapyGroup);
                }
            }
            if (bInsert){
	            insert lstTherapyGroup;
	            mapTherapyGroup_Main.putAll(lstTherapyGroup);
	        }

            for (Therapy_Group__c oTG : lstTherapyGroup){
                if (oTG.Sub_Business_Unit__c == clsTestData_MasterData.oMain_SubBusinessUnit.Id){
                    oMain_TherapyGroup = oTG;
                    break;
                }
            }
        }

        return lstTherapyGroup;
    }
    //---------------------------------------------------------------------------------------------------



    //---------------------------------------------------------------------------------------------------
    // Create Therapy Data
    //---------------------------------------------------------------------------------------------------
    public static List<Therapy__c> createTherapy(){
        return createTherapy(true);
    }
    public static List<Therapy__c> createTherapy(Boolean bInsert){

        if (oMain_Therapy == null){
            if (oMain_TherapyGroup == null) createTherapyGroup();

            lstTherapy = new List<Therapy__c>();

            for (Integer i=0; i<iRecord_Therapy; i++){
                Therapy__c oTherapy = new Therapy__c();
                    oTherapy.Name = 'TEST TH ' + String.valueOf(i);
                    oTherapy.Business_Unit__c = clsTestData_MasterData.oMain_BusinessUnit.id;
                    oTherapy.Sub_Business_Unit__c = clsTestData_MasterData.oMain_SubBusinessUnit.id;
                    oTherapy.Therapy_Group__c = oMain_TherapyGroup.id;
                    oTherapy.Therapy_Name_Hidden__c = 'TEST TH ' + String.valueOf(i);
                lstTherapy.add(oTherapy);
            }
            if (bInsert){
            	insert lstTherapy;
            	mapTherapy_Main.putAll(lstTherapy);
            }

            oMain_Therapy = lstTherapy[0];
        }

        return lstTherapy;
    }	
    //---------------------------------------------------------------------------------------------------

}