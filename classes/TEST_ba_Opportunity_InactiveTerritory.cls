@isTest
private class TEST_ba_Opportunity_InactiveTerritory {
	
	private static User oUser_Admin;
	private static Id id_Territory_Active;
	private static Id id_User_Active;

	@isTest static void createTestData() {
		
		oUser_Admin = clsTestData_User.createUser_SystemAdministrator('tstadm01', true);
		User oUser_CVG = clsTestData_User.createUser(true);

		System.runAs(oUser_Admin){

			// Create Master Data - Company
			clsTestData_MasterData.tCompanyCode = 'EUR';
			clsTestData_MasterData.createCompany();
			String tCompanyId = String.valueOf(clsTestData_MasterData.oMain_Company.Id).left(15);

			// Create Master Data - BUG (CVG)
			clsTestData_MasterData.tBusinessUnitGroupName = ('CVG');
			clsTestData_MasterData.createBusinessUnitGroup();

			// Create Master Data - BU (Vascular)
			clsTestData_MasterData.tBusinessUnitName = 'Vascular';
			Map<String, String> mapBusinessUnit_BusinessUnitGroup = new Map<String, String>();
	    		mapBusinessUnit_BusinessUnitGroup.put('Vascular', 'CVG');
	    	clsTestData_MasterData.mapBusinessUnit_BusinessUnitGroup = mapBusinessUnit_BusinessUnitGroup;
			clsTestData_MasterData.createBusinessUnit();

			// Create Master Data - SBU (Coro + PV)
			clsTestData_MasterData.tSubBusinessUnitName = 'Coro + PV';
			Map<String, String> mapSubBusinessUnit_BusinessUnit = new Map<String, String>();
	    		mapSubBusinessUnit_BusinessUnit.put('Coro + PV', 'Vascular');
			clsTestData_MasterData.mapSubBusinessUnit_BusinessUnit = mapSubBusinessUnit_BusinessUnit;
			clsTestData_MasterData.createSubBusinessUnit();

			// Create Territory
			Territory2 oTerritory_Inactive = new Territory2();
				oTerritory_Inactive.Name = 'Vascular_Interventional_Coronary_TERRITORY_TEST_1';
				oTerritory_Inactive.Therapy_Groups_Text__c = 'Interventional Coronary';
				oTerritory_Inactive.Business_Unit__c = 'Vascular';
				oTerritory_Inactive.Company__c = tCompanyId;
				//oTerritory_Inactive.TerritoryType = 'Territory';
				oTerritory_Inactive.Short_Description__c = 'Unit Test Vascular Territory 1';
				oTerritory_Inactive.Territory2TypeId = bl_Territory.territoryTypeMap.get('Territory');
				oTerritory_Inactive.DeveloperName = 'Unit_Test_Vascular_Territory_1';
				oTerritory_Inactive.Territory2ModelId = [Select Id from Territory2Model where DeveloperName = 'Medtronic'].Id;
			insert oTerritory_Inactive;


			// Get Territory with Active UserTerritory			
			List<UserTerritory2Association> lstUserTerritory = [SELECT Id, UserId, Territory2Id FROM UserTerritory2Association WHERE Territory2.Therapy_Groups_Text__c in ('Interventional Coronary') AND User.isActive = true AND IsActive = true LIMIT 1];
			
			for (UserTerritory2Association oUserTerritory : lstUserTerritory){
				
					id_Territory_Active = oUserTerritory.Territory2Id;
					id_User_Active = oUserTerritory.UserId;
				
			}
			System.assert(id_Territory_Active != null);
	
			// Create Account Data
			List<Account> lstAccount = clsTestData_Account.createAccount();

			List<ObjectTerritory2Association> lstAccountShare = new List<ObjectTerritory2Association>();
			for (Account oAccount : lstAccount){
					
				ObjectTerritory2Association oAccountShare = new ObjectTerritory2Association();
					oAccountShare.ObjectId = oAccount.Id;
					oAccountShare.Territory2Id = id_Territory_Active;
					oAccountShare.AssociationCause = 'Territory2Manual';
				lstAccountShare.add(oAccountShare);

			}
			insert lstAccountShare;

			// Create CVG Opportuniy & OpportunityLineItem Data
			clsTestData_Opportunity.iRecord_Opportunity = 1;
			clsTestData_Opportunity.tOpportunityCurrencyIsoCode = 'EUR';
			clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'CVG_Project').Id;
			clsTestData_Opportunity.createOpportunity(false);
			for (Opportunity oOpportunity : clsTestData_Opportunity.lstOpportunity){
				oOpportunity.Territory2Id = oTerritory_Inactive.Id;
			}
			insert clsTestData_Opportunity.lstOpportunity;
			clsTestData_Opportunity.iRecord_OpportunityLineItem = 1;
			clsTestData_Opportunity.createOpportunityLineItem(true);


		}

	}
	

    @isTest static void test_Scheduling() {

        Test.startTest();

        //---------------------------------------
        // TEST SCHEDULING
        //---------------------------------------
        string tCRON_EXP = '0 0 0 3 9 ? 2099';
        
        string tJobId = System.schedule('TEST_ba_Opportunity_InactiveTerr', tCRON_EXP, new ba_Opportunity_InactiveTerritory());

        // Get the information from the CronTrigger API object
        CronTrigger oCronTrigger = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :tJobId];

        // Verify the expressions are the same
        System.assertEquals(tCRON_EXP, oCronTrigger.CronExpression);

        // Verify the job has not run
        System.assertEquals(0, oCronTrigger.TimesTriggered);

        // Verify the next time the job will run
        System.assertEquals('2099-09-03 00:00:00', String.valueOf(oCronTrigger.NextFireTime));
        //---------------------------------------

        Test.stopTest();

    }	


	@isTest static void test_BatchAPEX() {

		//--------------------------------------------
		// Create Test Data
		//--------------------------------------------
		createTestData();

		List<Opportunity> lstOpportunity = [SELECT Id, Territory2Id, (SELECT Id, Who__c FROM OpportunityLineItems) FROM Opportunity];

		System.assertEquals(lstOpportunity.size(), 1);
		System.assertNotEquals(lstOpportunity[0].Territory2Id, id_Territory_Active);
		System.assertEquals(lstOpportunity[0].OpportunityLineItems.size(), 1);
		System.assertNotEquals(lstOpportunity[0].OpportunityLineItems[0].Who__c, id_User_Active);
		//--------------------------------------------


		//--------------------------------------------
		// Execute Logic
		//--------------------------------------------
		Test.startTest();

		System.runAs(oUser_Admin){

			ba_Opportunity_InactiveTerritory oBatch = new ba_Opportunity_InactiveTerritory();
			Database.executebatch(oBatch, 200);
		
		}

		Test.stopTest();
		//--------------------------------------------


		//--------------------------------------------
		// Validate Result
		//--------------------------------------------
		lstOpportunity = [SELECT Id, Territory2Id, (SELECT Id, Who__c FROM OpportunityLineItems) FROM Opportunity];

		System.assertEquals(lstOpportunity.size(), 1);
		System.assertEquals(lstOpportunity[0].Territory2Id, id_Territory_Active);
		System.assertEquals(lstOpportunity[0].OpportunityLineItems.size(), 1);
		System.assertEquals(lstOpportunity[0].OpportunityLineItems[0].Who__c, id_User_Active);
		//--------------------------------------------

	}


}