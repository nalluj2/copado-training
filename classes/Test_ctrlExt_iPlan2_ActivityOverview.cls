//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//	Created Date    : 09/10/2018
//	Author          : Bart Caelen
//	Description     : TEST Class for the APEX Controller Extension ctrlExt_iPlan2_ActivityOverview
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@isTest private class Test_ctrlExt_iPlan2_ActivityOverview  {
	
    // SeeAllData is needed to be able to SOQL the OpenActivity and ActivityHistory records
    @isTest (SeeAllData=true) 
    static void test_ctrlExt_iPlan2_ActivityOverview() {
        //------------------------------------------------
        // Variables
        //------------------------------------------------
        String tTest = '';
        List<ctrlExt_iPlan2_ActivityOverview.wr_Activity> lstWRAct = new List<ctrlExt_iPlan2_ActivityOverview.wr_Activity>();
        //------------------------------------------------

        clsTestData.iRecord_Task    = 5;
        clsTestData.iRecord_Event   = 5;

        //------------------------------------------------
        // CREATE / LOAD TEST DATA
        //		- Account_Plan_2__c
        //		- Account_Plan_Objective__c
        //		- Activities for Account_Plan_Objective__c
        //------------------------------------------------
        clsTestData.oMain_Company = 
        	[
        		SELECT
        			Account_Performance_Fullload_in_progress__c, Company_Code_Text__c, CreatedById, CreatedDate, CurrencyIsoCode, Current_day_in_Q1__c, Current_day_in_Q2__c, Current_day_in_Q3__c, Current_day_in_Q4__c, Current_day_in_year__c, Days_in_Q1__c, Days_in_Q2__c, Days_in_Q3__c, Days_in_Q4__c, Days_in_year__c, Generic_Data_Owner_ID__c, Id, Name, OwnerId, SystemModstamp
				FROM
					Company__c
				WHERE
					Company_Code_Text__c = 'EUR'
			];
		clsTestData.oMain_BusinessUnit = 
			[
				SELECT 
					Id, Name
					, Business_Unit_Group__c, Business_Unit_Group__r.Id, Business_Unit_Group__r.Name
        			, (SELECT Id, Name FROM Sub_Business_Units__r)
				FROM Business_Unit__c
				WHERE 
					Name = 'Neurovascular'
					And Company__c = :clsTestData.oMain_Company.Id
			];
		clsTestData.oMain_BusinessUnitGroup = clsTestData.oMain_BusinessUnit.Business_Unit_Group__r;
		clsTestData.oMain_SubBusinessUnit = clsTestData.oMain_BusinessUnit.Sub_Business_Units__r[0];

        clsTestData.createAccountData();
        Account_Plan_2__c oAccountPlan2 = new Account_Plan_2__c();
	        oAccountPlan2.Account__c = clsTestData.oMain_Account.Id;
	        oAccountPlan2.Account_Plan_Level__c = 'Business Unit';
	        oAccountPlan2.Business_Unit__c = clsTestData.oMain_BusinessUnit.Id;
	        oAccountPlan2.Sub_Business_Unit__c = clsTestData.oMain_SubBusinessUnit.Id;
	        oAccountPlan2.Business_Unit_Group__c = clsTestData.oMain_BusinessUnitGroup.Id;
	        oAccountPlan2.Start_Date__c = Date.today();
	        oAccountPlan2.End_Date__c = Date.today().addDays(10);
        clsTestData.lstAccountPlan2.add(oAccountPlan2);
		insert clsTestData.lstAccountPlan2;

		clsTestData.oMain_AccountPlan2 = oAccountPlan2;
		clsTestData.createAccountPlanObjectiveData();
		clsTestData.createAccountPlanObjectiveData();
		
		//Needed to avoid Too many SOQL errors
		Test.startTest();
		
        // Tasks for Account Plan Objective
        clsTestData.idTask_What     = clsTestData.oMain_AccountPlanObjective.Id;
        clsTestData.tTask_Status    = 'Not Started';
        clsTestData.oMain_Task      = null;
        clsTestData.createTaskData();

        clsTestData.tTask_Status    = 'Completed';
        clsTestData.oMain_Task      = null;
        clsTestData.createTaskData();

        // Events for Account Plan Objective
        clsTestData.idEvent_What            = clsTestData.oMain_AccountPlanObjective.Id;
        clsTestData.dtEventStartDateTime    = DateTime.now().addDays(1);
        clsTestData.dtEventEndDateTime      = DateTime.now().addDays(2);
        clsTestData.oMain_Event             = null;
        clsTestData.createEventData();

        clsTestData.idEvent_What            = clsTestData.oMain_AccountPlanObjective.Id;
        clsTestData.dtEventStartDateTime    = DateTime.now().addDays(-2);
        clsTestData.dtEventEndDateTime      = DateTime.now().addDays(-1);
        clsTestData.oMain_Event             = null;
        clsTestData.createEventData();
        //------------------------------------------------

        //------------------------------------------------
        // Perform Testing
        //------------------------------------------------
        ApexPages.StandardController oSTDCTRL = new ApexPages.StandardController(clsTestData.oMain_AccountPlan2);
        ctrlExt_iPlan2_ActivityOverview oCTRL = new ctrlExt_iPlan2_ActivityOverview(oSTDCTRL);

        lstWRAct = oCTRL.lstWROpenActivity;        
        lstWRAct = oCTRL.lstWRActivityHistory;        

        tTest = oCTRL.tURL_Redirect;
        tTest = oCTRL.tURL_LogACall;
        tTest = oCTRL.tURL_NewEvent;
        tTest = oCTRL.tURL_NewTask;
        tTest = oCTRL.tURL_ViewAll;
        tTest = oCTRL.tURL_SendAnEmail;

        Test.stopTest();
        //------------------------------------------------
    }
    
}