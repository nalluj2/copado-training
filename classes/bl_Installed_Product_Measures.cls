public with sharing class bl_Installed_Product_Measures {
    
    private static String currentUserAlias{
    	
    	get{
    		
    		if(currentUserAlias == null) currentUserAlias = [Select alias from User where Id = :UserInfo.getUserId()].Alias;
    		    		
    		return currentUserAlias;
    	}
    	
    	set;
    }
    
    public static void setReadBy(List<Installed_Product_Measures__c> triggerNew, Map<Id,Installed_Product_Measures__c> oldMap){
    	
    	for(Installed_Product_Measures__c prodMeasure : triggerNew){	
    		
    		if(prodMeasure.New_Recorded_Value__c != null && prodMeasure.New_Recorded_Value__c != oldMap.get(prodMeasure.Id).New_Recorded_Value__c){
    			
    			prodMeasure.Read_By__c = currentUserAlias; 
    		}
    	}
    }

	// Set the Account_from_IP__c on Installed_Product_Measures__c equal to the SVMXC__Company__c of the related SVMXC__Installed_Product__c because this is not provided by the interface
    public static void setAccountFromIP(List<Installed_Product_Measures__c> lstTriggerNew){

		Set<Id> setID_InstalledProduct = new Set<Id>();
		for (Installed_Product_Measures__c oInstalledProductMeasures : lstTriggerNew){

			setID_InstalledProduct.add(oInstalledProductMeasures.Installed_Product__c);
		
		}

		Map<Id, SVMXC__Installed_Product__c> mapInstalledProduct = new Map<Id, SVMXC__Installed_Product__c>([SELECT Id, SVMXC__Company__c FROM SVMXC__Installed_Product__c WHERE Id = :setID_InstalledProduct]);
		for (Installed_Product_Measures__c oInstalledProductMeasures : lstTriggerNew){
			oInstalledProductMeasures.Account_from_IP__c = mapInstalledProduct.get(oInstalledProductMeasures.Installed_Product__c).SVMXC__Company__c;
		}
	
	}
    
    public static void notificationSAP(List<Installed_Product_Measures__c> triggerNew, Map<Id,Installed_Product_Measures__c> oldMap){
		
		// If the change is done by any of the interfaces users then we do not trigger the notification
		if (UserInfo.getProfileId() == clsUtil.getUserProfileId('SAP Interface') || UserInfo.getProfileId() == clsUtil.getUserProfileId('Devart Tool') || UserInfo.getProfileId() == clsUtil.getUserProfileId('SMAX Interface')) return;
		
		List<Installed_Product_Measures__c> lstData_Processing = new List<Installed_Product_Measures__c>();

		for (Installed_Product_Measures__c oData : triggerNew){
		    
			if(oData.New_Recorded_Value__c != oldMap.get(oData.Id).New_Recorded_Value__c) lstData_Processing.add(oData);            
		}

	    if (lstData_Processing.size() > 0) bl_NotificationSAP.sendNotificationToSAP_InstalledProductCounter(lstData_Processing, 'UPDATE');
    }
}