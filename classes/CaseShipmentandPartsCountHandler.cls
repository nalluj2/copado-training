/***********************************************************        
Author-- Pavan Hegde        
Date-- 05/16/2015       
Description--- This is a handler, for an "After Insert" trigger on Case part and Part Shipment.         
               The object of this handler is to get the count of Case Parts on Case record, associated with that case, having a record type "hardware" and shipment is " shipped " or " International Ship"     
                                                get the count of Partshipment on Case record, associated with that case     
************************************************************/


public class CaseShipmentandPartsCountHandler {
    
    //Added by Pavan to avoid recursive trigger issue
    static boolean isupdated = FALSE;
    
    public static void updatecaseRollup(string objname){

// Initialization of sets and maps

    list<case> casescol = new list<case>();
    set<id> caseset = new set<id>();
    set<id> caseOrderset = new set<id>();
    map<id,integer> mapcase = new map<id,integer>();
    map<id,integer> mapcasecount = new map<id,integer>();
    list<case> colcase;
    list<Parts_Shipment__c> colParts_Shipment;
    list<Workorder_Sparepart__c> colOrder_Return_Item;


 // Get the recordtype id of the "Hardware" recordtype and Object is " Workorder_Sparepart__c " which is Case Parts Object.

    list<recordtype> colrectype = [select id from recordtype where name='Hardware' and SobjectType='Workorder_Sparepart__c' and isActive=true limit 1];

 // get the case ids from the Part Shipment records, by checking the object name as Part_shipment__c  
        
    for(sobject objSobject : trigger.new){
    	
    	if(objname=='Parts_Shipment__c'){  
        	Parts_Shipment__c objobject=(Parts_Shipment__c)objSobject;
            
            if(Trigger.isInsert && objobject.Case__c!= null){   
            	caseset.add(objobject.Case__c);   
            }
            
            if(Trigger.isUpdate){
            		            
	            Parts_Shipment__c objobject1=(Parts_Shipment__c)trigger.oldmap.get(objobject.id);
	            
	            if(objobject1.Case__c != objobject.Case__c){
	                	                
		            if(objobject.Case__c!= null){
		            	caseset.add(objobject.Case__c); 
		            }	            
		            
		            if(objobject1.Case__c!= null){
		            	caseset.add(objobject1.Case__c); 
		            }	            
	            }
            }
        }
// get the case ids from the Case Parts records, by checking the object name as Workorder_Sparepart__c  

        if(objname=='Workorder_Sparepart__c'){
        	system.debug('entered in the case part');
            Workorder_Sparepart__c objobject=(Workorder_Sparepart__c)objSobject;
            system.debug('objobject123-->'+objobject);
            
            if(Trigger.isInsert){
            	 
            	if(objobject.Case__c!= null && !colrectype.isEmpty() && objobject.recordtypeid ==colrectype[0].id &&(objobject.Order_Status__c=='Shipped' || objobject.Order_Status__c=='International Ship')){  
                	caseOrderset.add(objobject.Case__c); 
                }
            }
            
            system.debug('caseOrderset123-->'+caseOrderset);
// if the case id on the case lookup field of parts shipment or case parts gets changed, then the value of the count should be updated

            if(Trigger.isUpdate){
            	Workorder_Sparepart__c objobject1= (Workorder_Sparepart__c)trigger.oldmap.get(objobject.id);
            	
                //Only update if there was a change in the fields for Case, Status or Record Type
                if(objobject.Case__c != objobject1.Case__c || objobject.Order_Status__c != objobject1.Order_Status__c || objobject.recordtypeid != objobject1.recordtypeid){
                
	                system.debug('objobjectabc-->'+objobject);
	                system.debug('objobject1abc-->'+objobject1);
	                
	                if(objobject.Case__c!= null && !colrectype.isEmpty() && objobject.recordtypeid ==colrectype[0].id &&(objobject.Order_Status__c=='Shipped' || objobject.Order_Status__c=='International Ship')){  
                		caseOrderset.add(objobject.Case__c); 
                	}
	                
	                if(objobject1.Case__c != null && !colrectype.isEmpty() && objobject1.recordtypeid ==colrectype[0].id &&(objobject1.Order_Status__c=='Shipped' || objobject1.Order_Status__c=='International Ship')){	                                    	
	                    caseOrderset.add(objobject1.Case__c);                    
	                }
                }
            }
                        
        }
    }

 // Get the id and case number from the Part Shipment records, where case exists in the set above       
        
  // based on the ids, set a count and increment it, if more than one records are available 

        if(!caseset.isEmpty()){
            colParts_Shipment = [select id,Case__c from Parts_Shipment__c where Case__c in : caseset];
        
            if(colParts_Shipment != null && !colParts_Shipment.isEmpty()){
                integer count=0;
                for(Parts_Shipment__c objcase : colParts_Shipment){
                    if(mapcase.containskey(objcase.Case__c)){
                        count = mapcase.get(objcase.Case__c);
                    }
                    else{
                               count=0;
                    }
                    count = count+1;
                    mapcase.put(objcase.Case__c,count);
                }
                for(id objid : mapcase.keyset()){
                    
                    case objcase;
                    if(objname=='Parts_Shipment__c'){   
                    objcase = new case(id=objid, Parts_Shipment_Count__c=mapcase.get(objid));  
                    }
                    
                    if(objcase!=null){
                        casescol.add(objcase);
                    }

                }
            }
            for(id objcasenot : caseset){
                if(colParts_Shipment == null && colParts_Shipment.isEmpty() || mapcase == null || mapcase.isEmpty() || !mapcase.containskey(objcasenot)){
                    case objcase;
                    if(objname=='Parts_Shipment__c'){   
                    objcase = new case(id=objcasenot, Parts_Shipment_Count__c=0);  
                    }
                    
                    if(objcase!=null){
                        casescol.add(objcase);
                    }
                }
            }
        }

//if(!caseOrderset.isEmpty()){
//commented above line and modified that line as below to add few conditions to the search, if recordttype is null or not--->Pavan Hegde
if(!caseOrderset.isEmpty() && !colrectype.isEmpty() && colrectype[0].id!=null){
            colOrder_Return_Item = [select id,Case__c from Workorder_Sparepart__c where Case__c in : caseOrderset and recordtypeid = :colrectype[0].id and (Order_Status__c='Shipped' or Order_Status__c='International Ship')];
        if(colOrder_Return_Item != null && !colOrder_Return_Item.isEmpty()){
            integer count=0;
            for(Workorder_Sparepart__c objcase : colOrder_Return_Item){
               if(objcase != null && objcase.Case__c!=null){
                    if(mapcasecount.containskey(objcase.Case__c)){
                        count = mapcasecount.get(objcase.Case__c);
                    }
                    else{
                               count=0;
                    }
                    count = count+1;
                    mapcasecount.put(objcase.Case__c,count);
                }
            }
            for(id objid : mapcasecount.keyset()){
                
                case objcase;
                if(objname=='Workorder_Sparepart__c'){   
                    if(objid != null){
                        objcase = new case(id=objid, Case_Parts_Count__c=mapcasecount.get(objid));
                    }
                }
                if(objcase!=null){
                    casescol.add(objcase);
                }
            }
        }
        for(id objcasenot : caseOrderset){
                if(colOrder_Return_Item == null || colOrder_Return_Item.isEmpty() || mapcasecount == null || mapcasecount.isEmpty() || !mapcasecount.containskey(objcasenot)){
                    case objcase;
                    if(objname=='Workorder_Sparepart__c'){   
                    if(objcasenot!= null){
                        objcase = new case(id=objcasenot, Case_Parts_Count__c=0);
                    }
                }
                    
                    if(objcase!=null){
                        casescol.add(objcase);
                    }
                    system.debug('casescolabc-->'+casescol);
                }
            }
        }
        system.debug('casescolabcxyz-->'+casescol);
        if(!casescol.isEmpty()){
            if(isupdated == FALSE){
            	isupdated = TRUE;
            	update casescol;
          	}
        }
    }
    
}