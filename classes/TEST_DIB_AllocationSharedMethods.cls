/**
 * Creation Date :  20091209
 * Description :    Test Coverage of the controller 'DIB_AllocationSharedMethods'.
 *
 *
 * Author :         ABSI - Bob Ceuppens
 */
@isTest
private class TEST_DIB_AllocationSharedMethods {
    
    private datetime currentDateTime;
    private Date currentDate;
    private Date startDate;
    private Date endDate;
    private User currentUser;
    private DIB_AllocationSharedMethods myDIB_AllocationSharedMethods;
    private String months;
    private String year;
    private id terrID;
    private id terrID2;
    private Account acc2 = new Account();
    private list<Affiliation__c> lstAff = new list<Affiliation__c>();
    private String country;
    private String accountType = 'Adult';
    private Account myTestAccount;
    private DIB_Invoice_Line__c myTestInvoiceLine;
    private List<DIB_Invoice_Line__c> invoiceLines0109;
    private DIB_Aggregated_Allocation__c myAggregated_Allocation;
    List<DIB_Aggregated_Allocation__c> myAggregated_Allocations;
    List<DIB_Class_AccountAggregation> myDIB_Class_AccountAggregations;

    static testMethod void myUnitTest() {
            System.debug(' ################## ' + 'BEGIN TEST_controllerAllocationAggregated' + ' ################');
                Id myProfile = clsUtil.getUserProfileId('EUR DiB Sales Entry');
//                Id myProfile = [select id from profile where name like '%DiB Sales Entry%' limit 1].Id;
                //User myUser = [Select Id,Country from User where profileId =:myProfile and IsActive=true limit 1];
                User myUser = new User(alias = 'repuser', email='usertest@absi.be', emailencodingkey='UTF-8', lastname='Testing', 
                languagelocalekey='en_US',localesidkey='en_US', profileid = myProfile, timezonesidkey='America/Los_Angeles', 
                username='usertest@absi.be', Alias_unique__c='usertest1234',Company_Code_Text__c='EUR');        

                User oUser_Admin = clsTestData_User.createUser_SystemAdministratorMDT('tstadm1', true);

                System.runAs(oUser_Admin){
                    clsTestData_System.createDIBFiscalPeriod();
                    clsTestData_MasterData.createCountry();
                }
                
                System.runAs(myUser) {
                    System.debug('Current User: ' + UserInfo.getUserName());
                    System.debug('Current Profile: ' + UserInfo.getProfileId());
                    TEST_DIB_AllocationSharedMethods myTestController = new TEST_DIB_AllocationSharedMethods();
                    myTestController.init(myUser);
                    myTestController.createRecords();
                    Test.startTest();
                    myTestController.doTests();
                }
            System.debug(' ################## ' + 'END TEST_controllerAllocationAggregated' + ' ################'); 
    }
    public void init(User myUser) {
        System.debug(' ################## ' + 'TEST_controllerAllocationAggregated_init' + ' ################');
        datetime currentDateTime = datetime.now();
        currentDate = currentDateTime.date();
        startDate = currentDateTime.date().addYears(-2);
        endDate = currentDateTime.date().addYears(+2);
        
        // Get the current fiscal months and current fiscal year. 
        List<SelectOption> periods = DIB_AllocationSharedMethods.getPeriodLines()   ;       
        system.debug('##########################################Period ' + periods ) ; 
        String[] fiscalPeriod = new String[]{} ;        
        for (SelectOption so : periods){
            fiscalPeriod.add(so.getValue());                    
        }
        months = fiscalPeriod.get(0).substring(0,2);
        year = fiscalPeriod.get(0).substring(3,7); 
        
        year = String.valueOf(Integer.valueOf(year) + 10);
        
        country = myUser.Country;
        
        currentUser = myUser;   
    }
    set<id> setAcc= new set<id>();
    map<string,set<string>> testmap=new map<string,set<string>>();
    public void doTests() {
        
        System.debug(' ################## ' + 'TEST_DIB_AllocationSharedMethods_doTests' + ' ################');
        myDIB_Class_AccountAggregations = new List<DIB_Class_AccountAggregation>();
        String periodChosen = months + '-' + year;
        myDIB_AllocationSharedMethods =  new DIB_AllocationSharedMethods();

        //DIB_AllocationSharedMethods.getAllAffiliations();
        DIB_AllocationSharedMethods.getCurrentPeriod();
        for (Integer i=0;i<=12;i++) {
             String m ;
            if(i<10){
                 m = '0' + String.valueOf(i);
            }else{
                m=String.valueOf(i);
            }
            System.debug(' ################## ' + 'TEST_DIB_AllocationSharedMethods getCivilMonthName: ' + m + ' ################');
            DIB_AllocationSharedMethods.getCivilMonthName(m);
            DIB_AllocationSharedMethods.getFiscalMonthName(m);
            DIB_AllocationSharedMethods.getPeriod(i, Integer.valueOf(year));
            DIB_AllocationSharedMethods.getPeriodLines(i, Integer.valueOf(year));
            DIB_AllocationSharedMethods.getAllocatedPeriodLines(i, Integer.valueOf(year));
            DIB_AllocationSharedMethods.Format_MonthAnd_Year(m, year);
        }

        DIB_AllocationSharedMethods.getInvoiceLinesPerPeriode('Jan','2012',true,true);
        DIB_AllocationSharedMethods.getAllTerritories(terrID);
        DIB_AllocationSharedMethods.getAllTerritories();
        DIB_AllocationSharedMethods.getFilteredAccounts(terrID);
        DIB_AllocationSharedMethods.getFilteredAccounts(terrID,terrID2);
        DIB_AllocationSharedMethods.getFilteredAccountsUsers(terrID,terrID2);
        DIB_AllocationSharedMethods.getAllInvoiceLinesPerPeriode('Jan','2012',true,true, 'Acc1', setAcc);
        DIB_AllocationSharedMethods.checkAccount(acc2.id);
        DIB_AllocationSharedMethods.getAllAffiliations(invoiceLines0109);
        DIB_AllocationSharedMethods.getAllDIBDepartments(invoiceLines0109, lstAff);
        DIB_AllocationSharedMethods.getPeriodLines(12,2012);
        DIB_AllocationSharedMethods.getAllocatedPeriodLines('12-2013');
        DIB_AllocationSharedMethods.getDepartmentFilterOptions();
        DIB_AllocationSharedMethods.getSoldToAccountsList(invoiceLines0109,'a','b');
        DIB_AllocationSharedMethods.getSoldToShipToAccountsList(invoiceLines0109,'a','b');
        DIB_AllocationSharedMethods.getAllInvoiceLinesPerPeriode('12','2013',true,true,'xyz',testmap);
    }
    
   
      public void createRecords() {
            
            DIB_Country__c nl = [SELECT Id,  BU_Not_Mandatory_on_Relationship__c, Hide_Therapy_on_Relationship__c FROM DIB_Country__c where Country_ISO_Code__c = 'NL'];
            nl.BU_Not_Mandatory_on_Relationship__c = true;
            nl.Hide_Therapy_on_Relationship__c = true;
            update nl;
            
            String accountType = 'Adult';
            
            // Sold To Accounts 
            terrID = [select id from territory2 limit 1].id;
            terrID2= [select id from territory2 order by name desc limit 1].id;
            List<Account> soldToAccounts    = new List<Account>{} ;
            
            // Sales Force Accounts 
            List<Account> sfaAccounts       = new List<Account>{} ;
            
            // Create a few Sold to Accounts ; 
            for (Integer i = 0; i < 10 ; i++){
                Account acc1 = new Account();
                acc1.Name = 'TEST_DIB_AllocationSharedMethods Aatest Coverage Sold to or ship To Account ' + i ;
                acc1.Account_Active__c = true;
                soldToAccounts.add(acc1) ;
                setAcc.add(acc1.id);
            }
            insert soldToAccounts ;
            
            
            // create Sales Force Accounts : 
            for (Integer i = 0; i < 10 ; i++){
                Account acc1 = new Account();
                acc1.Name = 'TEST_DIB_AllocationSharedMethods Aatest Coverage Sales Force Account ' + i ;
                acc1.isSales_Force_Account__c = true ; 
                acc1.Account_Active__c = true;
                sfaAccounts.add(acc1) ;
            }
            insert sfaAccounts ;
            
            myTestAccount = sfaAccounts.get(0);
            
            System.debug(' ################## ' + 'controllerDIB_Campaign_createRecords_sfaAccounts: ' + sfaAccounts + ' ################');
            
            Department_Master__c department = new Department_Master__c();
            department.Name = accountType;
            insert department;
            
            //Department
            DIB_Department__c myDepartment = new DIB_Department__c();
            myDepartment.Account__c = sfaAccounts.get(0).Id;
            myDepartment.Department_ID__c = department.Id;
            
            insert myDepartment;
            
            invoiceLines0109 = new List<DIB_Invoice_Line__c>{} ;        
            for (Integer i = 0; i < 10 ; i++){
                DIB_Invoice_Line__c invl  = new DIB_Invoice_Line__c() ; 
                invl.Distribution_No__c = 'DN' ; 
                invl.Name = 'Invoice Name' ;  
                invl.Fiscal_Month_Code__c = months;
                invl.Fiscal_Year__c = String.valueOf(Integer.valueOf(year) + 10);
                invl.Allocated_Fiscal_Month_Code__c=months;
                invl.Allocated_Fiscal_Year__c=String.valueOf(Integer.valueOf(year) + 10); 
                invl.Sales_Force_Account__c=sfaAccounts.get(0).Id;
                invl.Department_ID__c=myDepartment.Id;
                // This field must have a value otherwise can have a problem saying Argument 1 cannot be null Line 101 on Method SharedMethods. 
                invl.Quantity_Invoiced_Number__c = 1 ; 
                invl.Sold_To__c = sfaAccounts.get(i).Id;
                invl.Ship_To_Name_Text__c = sfaAccounts.get(i).Name;
                invl.Is_Allocated__c = false ;  
                if (i > 5 && i < 20){
                    invl.Is_Allocated__c = true ; 
                }
                invoiceLines0109.add(invl) ; 
            }
            insert invoiceLines0109 ;
            
            myTestInvoiceLine = invoiceLines0109.get(0);
            
            myAggregated_Allocations = new List<DIB_Aggregated_Allocation__c>();
            for (Integer i = 0; i < 10 ; i++){
                DIB_Aggregated_Allocation__c myAggregated_Allocation = new DIB_Aggregated_Allocation__c();
                myAggregated_Allocation.Sales_Force_Account__c = sfaAccounts.get(i).Id;
                myAggregated_Allocation.Fiscal_Month__c = months;
                myAggregated_Allocation.Fiscal_Year__c = year;
                myAggregated_Allocations.add(myAggregated_Allocation);
            }
            //insert myAggregated_Allocations;
            
            myAggregated_Allocation = myAggregated_Allocations.get(0);


            Account acc1 = new Account();
            acc1.Name = 'TEST_controllerAffiliationsRTView Test Coverage Account 1 ' ; 
            acc1.Account_Country_vs__c = 'NETHERLANDS';
            insert acc1 ;
            
            //Account acc2 = new Account();
            acc2.Name = 'TEST_controllerAffiliationsRTView Test Coverage Account 2 ' ; 
            acc2.SAP_ID__c =  '00067898';     
            acc1.Account_Country_vs__c = 'NETHERLANDS';
            insert acc2 ;  
            
            Contact cont1 = new Contact();
            cont1.LastName = 'TEST_controllerAffiliationsRTView TestCont1' ;  
			cont1.FirstName = 'Test Contact 1';
            cont1.AccountId = acc1.Id ;
            cont1.Contact_Active__c = false  ;
            cont1.Contact_Inactive_Reason__c = 'Inactive Reason is required to all inactve Contacts'; 
            cont1.Contact_Department__c = 'Diabetes Adult'; 
            cont1.Contact_Primary_Specialty__c = 'ENT';
            cont1.Affiliation_To_Account__c = 'Employee';
            cont1.Primary_Job_Title_vs__c = 'Manager';
            cont1.Contact_Gender__c = 'Male';         
			insert cont1;
            
            Contact cont2 = new Contact();
            cont2.LastName = 'TEST_controllerAffiliationsRTView TestCont2';  
            cont2.FirstName = 'Test Contact 2';  
            cont2.AccountId = acc1.Id ;
            cont2.Contact_Active__c = false;
            cont2.Contact_Inactive_Reason__c = 'Inactive Reason is required to all inactve Contacts';
            cont2.Contact_Department__c = 'Diabetes Adult'; 
            cont2.Contact_Primary_Specialty__c = 'ENT';
            cont2.Affiliation_To_Account__c = 'Employee';
            cont2.Primary_Job_Title_vs__c = 'Manager';
            cont2.Contact_Gender__c = 'Male';          
            insert cont2;

            // Create test Affiliation    
            Affiliation__c insertAff=new Affiliation__c();
            insertAff.Affiliation_From_Contact__c = cont1.Id;
            insertAff.Affiliation_From_Account__c = acc1.Id ; 
            insertAff.Affiliation_To_Contact__c = cont2.Id;
            insertAff.Affiliation_To_Account__c = acc2.Id ;
            insertAff.RecordTypeId = FinalConstants.recordTypeIdC2C;
            insert insertAff;
            lstAff.add(insertAff);
            
            Affiliation__c insertAff2 =new Affiliation__c();
            insertAff2.Affiliation_From_Contact__c = cont1.Id;
            insertAff2.Affiliation_To_Account__c = acc2.Id ;
            insertAff2.RecordTypeId = FinalConstants.recordTypeIdC2A;
            insert insertAff2;
            lstAff.add(insertAff2);
        }

}