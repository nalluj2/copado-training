global class ba_Admin_Request_Termination implements Schedulable,Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts{
		    
    private String username;
    private String password;
    private Map<String, Session> sessionMap;
    
    private Boolean isSandbox;
    private List<String> errors;
    private List<Admin_Request__c> reqToClose;
    
    public Boolean inDevelopment;
    //For Test execution only
    @TestVisible
	private HttpCalloutMock calloutMock;
    
    public ba_Admin_Request_Termination(String un, String pw){
    	
    	if(un == null || pw == null) throw new IllegalArgumentException('Cannot schedule / execute without a context Username and Password');
    	
    	username = un;
    	password = pw;   
    	
    	inDevelopment = false; 	
    }
    
    global void execute(SchedulableContext ctx){        
                               
        Database.executeBatch(new ba_Admin_Request_Termination(username, password), 1);    
    }     
    
    global Database.QueryLocator start(Database.BatchableContext ctx){    
		
		// Check if we are running in Production
		isSandbox = [Select IsSandbox from Organization limit 1].isSandbox;
		
		errors = new List<String>();
		reqToClose = new List<Admin_Request__c>();
		
		sessionMap = new Map<String, Session>();
		               
        Date todayDate = Date.today();
        
        String query = 'Select Id, Name, User__c, User__r.Name, Permissions__c, Environment__c from Admin_Request__c where End_Date__c = :todayDate AND Status__c = \'Approved\' AND  Permissions__c INCLUDES (\'Org Configuration\',\'Apex Development\', \'SMAX Administrator\',\'Patient Data Access\') AND Admin_Request_Revoked__c = false';
        
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext ctx, List<Admin_Request__c> records){
    	
    	if(records.size() != 1) throw new IllegalArgumentException('This Job can only be executed with a Job size equal to 1');
    	
    	Admin_Request__c adminReq = records[0];
    	
		try{ 		
    	
	    	Boolean containsManual = false;
	    	List<String> permissions = new List<String>();
	    			
	    	for(String permission : adminReq.Permissions__c.split(';')){
    				
	    		if(permission == 'Org Configuration') permissions.add('Org_Configuration');
	    		else if(permission == 'Apex Development') permissions.add('Apex_Development');    			
	    		else if(permission == 'SMAX Administrator') permissions.add('SMAX_Administrator');    			
	    		else if(permission == 'Patient Data Access') permissions.add('View_Patient_Data');    			
	    		else containsManual = true;    					
	    	}    	
	    	    	    	
	    	// Executed in Production only
	    	if(isSandbox == false || Test.isRunningTest() || inDevelopment){
	    		
	    		if(adminReq.Environment__c == 'Production' ){
    			
	    			// Execute removal in Apex
	    			List<PermissionSetAssignment> userAssignments = [Select Id from PermissionSetAssignment where AssigneeId = :adminReq.User__c AND PermissionSet.Name IN :permissions];		
					delete userAssignments;
	    			    			    		
				}else{
					
					// Execute removal through API call				
					executeRemoteRemovalPermission(adminReq.Environment__c, adminReq.User__c, permissions);			
				}
			
				if(containsManual == false) reqToClose.add(adminReq);    		
    		}  	 
    	
    	}catch(Exception e){
					
			// Capture error for send email at the end and re-throw it
			String errorMessage = 'Error executing Admin Request ' + adminReq.Name + ' (' + adminReq.Id + ') for user ' + adminReq.User__r.Name + '. Message: ' + e.getMessage();
			errors.add(errorMessage);
			
			if(Test.isRunningTest() == false) throw e;
		}	
    }
    
    global void finish(Database.BatchableContext ctx){
    	
    	if(reqToClose.size() > 0){
    	
	    	for(Admin_Request__c adminReq : reqToClose){
	    		
	    		adminReq.Admin_Request_Revoked__c = true;
	    	}
	    	
	    	update reqToClose;
    	}
    	
    	if(errors.size() > 0){
    		
    		List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();
	   				
   			String emailBody = 'The following Admin Requests failed to be terminated automatically: <br/><br/> ';
   				
   			for(String errorMessage : errors){
   				emailBody += errorMessage + ' <br/><br/> ';
   			}
   			
   			Messaging.SingleEmailMessage requestorEmail = new Messaging.SingleEmailMessage();
			requestorEmail.setTargetObjectId(UserInfo.getUserId());
			requestorEmail.setSaveAsActivity(false);
			requestorEmail.setSubject('Admin Request termination failures');
			requestorEmail.setHTMLBody(emailBody);
				
			messages.add(requestorEmail);
    		
    		//Send email to admins
    		List<GroupMember> groupAdmins = [Select UserOrGroupId from GroupMember where Group.DeveloperName = 'Admin_Request_Approvers'];
    		
    		if(groupAdmins.size() > 0){    		
	   				
	   			for(GroupMember userMember : groupAdmins){
	   				
		   			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
					mail.setTargetObjectId(userMember.UserOrGroupId);
					mail.setSaveAsActivity(false);
					mail.setSubject('Admin Request termination failures');
					mail.setHTMLBody(emailBody);
						
					messages.add(mail);
	   			}			
    		}
    		
    		Messaging.sendEmail(messages);   
    	}
    }
    
    private void executeRemoteRemovalPermission(String environment, Id userId, List<String> permissions){
    	
    	Session session = getSessionId(environment);
    	
    	//Create the HTTP request to be sent to the target Org  	
     	HttpRequest req = new HttpRequest();    	    	
	    req.setEndpoint(session.serverURL + 'apexrest/AdminRequestService');
	    req.setMethod('POST');
	    req.setHeader('Authorization', 'Bearer ' + session.sessionId);
	    req.setHeader('Content-Type', 'application/json'); 	   
	    req.setTimeout(120000); 
	    
	    ws_Admin_Request_Service.Request request = new ws_Admin_Request_Service.Request();
	    request.UserId = userId;
	    request.PermissionSets = permissions;	    
	    req.setBody(JSON.serialize(request));
	    
    	Http http = new Http();
        HttpResponse res;
        
        if(Test.isRunningTest() == false) res = http.send(req);
        else res = calloutMock.respond(req); 
            	     	
     	if(res.getStatusCode() < 200 || res.getStatusCode() >= 300){
     		
     		throw new ws_Exception('Error during Rest call: ' + res.getStatus() + ' ('+ res.getStatusCode() + '). Response received : ' + res.getBody());        	
     	}
     	     	
     	ws_Admin_Request_Service.Response response = (ws_Admin_Request_Service.Response) JSON.deserialize(res.getBody(), ws_Admin_Request_Service.Response.class);
     	
     	if(response.isSuccess == false) throw new ws_Exception(response.errorMessage);
    }
        
    private Session getSessionId(String environment){
    	
    	if(sessionMap.containsKey(environment)) return sessionMap.get(environment);
    		   		   		   		
   		HttpRequest req = new HttpRequest();    	    	
    	req.setEndpoint('https://test.salesforce.com/services/Soap/c/35.0/');   	
	    req.setMethod('POST');
	    req.setHeader('Content-Type',  'text/xml');
	    req.setHeader('SOAPAction', '""');
	    
	    String body  = '<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/" >';   
   		body += '<Body>';
      	body += '<login xmlns="urn:enterprise.soap.sforce.com">';
        body += '<username>' + username + '.' + environment + '</username>';
        body += '<password>' + password + '</password>';
      	body += '</login>';
   		body += '</Body>';
		body += '</Envelope>';	    	
	    req.setBody(body);
	       		
    	Http http = new Http();
        HttpResponse res;
        
        if(Test.isRunningTest() == false) res = http.send(req);
        else res = calloutMock.respond(req);
    	   		
   		if(res.getStatusCode() < 200 || res.getStatusCode() >= 300){
     		   
        	throw new ws_Exception('Error during login call: ' + res.getStatus() + ' ('+ res.getStatusCode() + '). Response received : ' + res.getBody());
     	}
   		
   		Dom.Document doc = res.getBodyDocument();
        Dom.XMLNode loginResponse = doc.getRootElement().getChildElement('Body', 'http://schemas.xmlsoap.org/soap/envelope/').getChildElement('loginResponse', 'urn:enterprise.soap.sforce.com').getChildElement('result', 'urn:enterprise.soap.sforce.com');
		    	
    	Session session = new Session();
    	String serverURL = loginResponse.getChildElement('serverUrl', 'urn:enterprise.soap.sforce.com').getText();
    	session.ServerURL = serverURL.subString(0, serverURL.indexOf('Soap/'));
    	session.SessionId = loginResponse.getChildElement('sessionId', 'urn:enterprise.soap.sforce.com').getText();
    	
    	sessionMap.put(environment, session);
    	
    	return session;
    }
    
    private class Session{
    	
    	public String serverURL {get; set;}
    	public String sessionId {get; set;}
    }    
}