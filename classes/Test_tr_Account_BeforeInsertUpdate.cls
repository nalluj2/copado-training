/*
 * Description	 	: Test class for Trigger tr_Account_BeforeInsertUpdate
 * Author        	: Patrick Brinksma
 * Created Date		: 17-07-2013
 */
@isTest
private class Test_tr_Account_BeforeInsertUpdate {

    static testMethod void testTrigger() {
    	String randVal = Test_TestDataUtil.createRandVal();
    	String atSuffix = '@salesforcetest.com';
    	// Create 3 users (2 for assignment, 1 admin for runas)
    	Map<String, User> mapOfUserNameToUser = Test_TestDataUtil.createUsers(randVal, atSuffix, new List<String>{'System Administrator', 'CAN DiB IS', 'CAN DiB IS'}, new List<String>{'System Administrator', 'CAN (DIB)', 'CAN (DIB)'});
    	mapOfUserNameToUser.get(randVal + 1 + atSuffix).SAP_ID__c = 'SAPSAPSAP'; //-BC - 20160215 - Replaced '0123456789'  by 'SAPSAPSAP' because '0123456789' exists on a user
    	mapOfUserNameToUser.get(randVal + 1 + atSuffix).FirstName = randVal;
    	insert mapOfUserNameToUser.values();
    	// Run followin as new sys admin user to prevent mixed DML
    	System.runAs(mapOfUserNameToUser.get(randVal + 0 + atSuffix)){
    		List<Account> listOfAccount = Test_TestDataUtil.createHealthInsurers(randVal, 'CANADA', 10);
    		Integer i = 0;
    		for (Account a : listOfAccount){
    			if (i < 5){
    				a.Name = a.Name + 'TRUE';
    				a.DIB_Sales_Rep__c = mapOfUserNameToUser.get(randVal + 1 + atSuffix).Id;
    			} else if (i > 5 && i < 10) {
    				a.DIB_Sales_Rep__c = mapOfUserNameToUser.get(randVal + 2 + atSuffix).Id;
    			}
    		}
    		insert listOfAccount;
    	}
    	// Assert Result
    	String searchString = randVal + '%';
    	List<Account> listOfResult = [select Name, DIB_SAP_Sales_Rep_Id__c, DIB_Sales_Rep_Manually__c, DIB_SAP_SR_Name__c, DIB_Sales_Rep__c from Account where Name like :searchString];
    	for (Account a : listOfResult){
    		if (a.Name.right(4) == 'TRUE'){
    			System.assertEquals(a.DIB_Sales_Rep__c, null);
    			System.assertEquals(a.DIB_SAP_SR_Name__c, mapOfUserNameToUser.get(randVal + 1 + atSuffix).LastName + ', ' + mapOfUserNameToUser.get(randVal + 1 + atSuffix).FirstName);
                //-BC - 20141210 - CR-6139 - START
//    			System.assertEquals(a.DIB_Sales_Rep_Manually__c, true);
                //-BC - 20141210 - CR-6139 - STOP
    			System.assertEquals(a.DIB_SAP_Sales_Rep_Id__c, mapOfUserNameToUser.get(randVal + 1 + atSuffix).SAP_ID__c);
    		}
    	}
        
    }
    
    private static testmethod void testCopyFields_SAPInterface(){
    	    	    	
    	User sapInterface = [Select Id from User where Alias = 'sap'];
    	
    	System.runAs(sapInterface){
    		
    		Account patient = new Account();
    		patient.RecordTypeId = clsUtil.getRecordTypeByDevName('Account', 'SAP_Patient').Id;
    		patient.SAP_Fax__c = '+31 555 555 555';
    		patient.SAP_Phone__c = '+31 777 777 777';
    		patient.SAP_Region__c = 'Test';
    		patient.SAP_Email__c = 'unittest@medtronic.com';
    		patient.SAP_Preferred_Communication_Channel__c = 'Phone';
    		patient.SAP_Language__c = 'English';
    		patient.SAP_Name_Org2__c = 'Unit Test Name 2';
    		patient.Mail_Opt_In__pc = 'TRUE';
    		patient.Phone_Opt_In__pc = 'FALSE';
    		patient.Email_Opt_In__pc = 'TRUE';
    		patient.Newsletters_Opt_In__pc = 'TRUE';
    		patient.OrderReminders_Opt_In__pc = 'FALSE';
    		patient.Promotions_Opt_In__pc = 'FALSE';
    		patient.Surveys_Opt_In__pc = 'TRUE';
    		patient.Educational_Calls_Opt_In__pc = 'TRUE';    		
    		patient.LastName = 'Tobereplaced';
    		patient.SAP_Account_Name__c = 'Unit Test Name';
    		patient.SAP_Mobile_Phone__c = '+31 666 666 666';
    		patient.SAP_Date_Of_Birth__c = Date.today().addYears(-40);
    		patient.SAP_Gender__c = 'Male';
    		patient.SAP_ID__c = '0123456789';
    		patient.SAP_Channel__c = '30';
    		
    		insert patient;
    		
    		patient = [Select Id, isPersonAccount, Fax, Phone, Account_Province__c, Account_Email__c, Contact_Preferred_Contact_Method__pc,
    							Contact_Preferred_Language__pc, Account_Name2_Text__c, Mail_Opt_In__pc, Phone_Opt_In__pc, Email_Opt_In__pc, Newsletters_Opt_In__pc,
    							OrderReminders_Opt_In__pc, Promotions_Opt_In__pc, Surveys_Opt_In__pc, Educational_Calls_Opt_In__pc,
    							FirstName, LastName, PersonMobilePhone, PersonBirthdate, Contact_Gender__pc from Account];
    	
	    	System.assertEquals('+31 555 555 555', patient.Fax);
	    	System.assertEquals('+31 777 777 777', patient.Phone);
	    	System.assertEquals('Test', patient.Account_Province__c);
	    	System.assertEquals('unittest@medtronic.com', patient.Account_Email__c);
	    	System.assertEquals('Phone', patient.Contact_Preferred_Contact_Method__pc);
	    	System.assertEquals('English', patient.Contact_Preferred_Language__pc);
	    	System.assertEquals('Unit Test Name 2', patient.Account_Name2_Text__c);
	    	System.assertEquals('Yes', patient.Mail_Opt_In__pc);
	    	System.assertEquals('No', patient.Phone_Opt_In__pc);
	    	System.assertEquals('Yes', patient.Email_Opt_In__pc);
	    	System.assertEquals('Yes', patient.Newsletters_Opt_In__pc);
	    	System.assertEquals('No', patient.OrderReminders_Opt_In__pc);
	    	System.assertEquals('No', patient.Promotions_Opt_In__pc);
	    	System.assertEquals('Yes', patient.Surveys_Opt_In__pc);
	    	System.assertEquals('Yes', patient.Educational_Calls_Opt_In__pc);
	    	System.assertEquals('Unit', patient.FirstName);
	    	System.assertEquals('Test Name', patient.LastName);
	    	System.assertEquals('+31 666 666 666', patient.PersonMobilePhone);
	    	System.assertEquals(Date.today().addYears(-40), patient.PersonBirthdate);
	    	System.assertEquals('Male', patient.Contact_Gender__pc);
    	}   	
    }
}