global class ba_Contract_Repository_Auto_Team implements Schedulable, Database.Batchable<SObject>, Database.Stateful {
    
    private Map<String, Contract_Repository_Setting__c> countrySettings;
    
    // Schedulable
    global void execute(SchedulableContext ctx){
        
        Database.executeBatch(new ba_Contract_Repository_Auto_Team(), 1000);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        countrySettings = bl_ContractRepository.getCountrySettings();
                
        String query = 'Select Id, Account__c, Account__r.Account_Country_vs__c, Primary_Contract_Type__c, MPG_Code__c from Contract_Repository__c ORDER BY Account__c ';
        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Contract_Repository__c> contracts) {
        
        bl_ContractRepository.createDefaultContractTeam(contracts, countrySettings);
    }

    global void finish(Database.BatchableContext BC) {}
}