public class bl_Opportunity_Account {
    
    public static void populateUniqueKey(List<Opportunity_Account__c> triggerNew){
    	
    	for(Opportunity_Account__c oppAccount : triggerNew){
    		
    		oppAccount.Unique_Key__c = oppAccount.Opportunity__c + ':' + oppAccount.Account__c;
    	}
    
	}

	public static Opportunity_Account__c saveOpportunityAccount(Opportunity_Account__c oOpportunityAccount){
		
		if (null != oOpportunityAccount){
			upsert oOpportunityAccount Mobile_ID__c;
		}
		return oOpportunityAccount;
		
	} 


}