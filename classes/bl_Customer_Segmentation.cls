public with sharing class bl_Customer_Segmentation {
	
	public static Boolean fromApp = false;
    
    public static Boolean isDMLAllowed(List<SObject> triggerNew){
    	
    	System.debug('=====>' + UserInfo.getProfileId());
    	
    	if(fromApp == false && ProfileMedtronic.isSystemAdministratorProfile(UserInfo.getProfileId()) == false && ProfileMedtronic.isDevartProfile(UserInfo.getProfileId()) == false && PermissionSetMedtronic.hasPermissionSet('Segmentation_GTM_Super_User') == false){
    		
    		for(SObject record : triggerNew){
    			
    			record.addError('This record can only be managed through the Customer Segmentation app or by designated administrators');	
    		}    	
    		
    		return false;		
    	}    	
    	
    	return true;
    }    
}