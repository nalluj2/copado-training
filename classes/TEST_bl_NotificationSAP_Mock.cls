//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 04-12-2015
//  Description      : APEX Test Class class that contains the HTTPCalloutMock for use in TEST_bl_NotificationSAP.
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
@isTest public class TEST_bl_NotificationSAP_Mock implements HttpCalloutMock {

    //----------------------------------------------------------------------------------------
    // VARIABLES / PARAMETERS
    //----------------------------------------------------------------------------------------
    protected Integer iStatusCode;
    protected String tStatus;
    protected String tIsSuccess;
    protected String tMsg;
    protected Map<String, String> mapResponseHeader;
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------------------------------
    public TEST_bl_NotificationSAP_Mock() {
        // By default create a SUCCESSFULL HTTPCalloutMock
        this.iStatusCode = 200;
        this.tStatus = 'OK';
        this.mapResponseHeader = new Map<String, String>();
        this.tIsSuccess = 'TRUE';
        this.tMsg = 'Record processed successful';
    }
    public TEST_bl_NotificationSAP_Mock(Integer iStatusCode, String tStatus, Map<String, String> mapResponseHeader, String tIsSuccess, String tMsg) {
        // Creaet a HTTPCalloutMock using the provided parameters
        this.iStatusCode = iStatusCode;
        this.tStatus = tStatus;
        this.mapResponseHeader = mapResponseHeader;
        this.tIsSuccess = tIsSuccess;
        this.tMsg = tMsg;
    }
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
    // GET A HTTPRESPONSE FOR USE IN TEST APEX
    //----------------------------------------------------------------------------------------
    public HTTPResponse respond(HTTPRequest oHTTPRequest) {

        String tBody = '';

        // Create the Response Body
        if (tStatus == 'ERROR'){
            tBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body><ser-root:processServiceOrderResponse xmlns:ser-root="http://msplwa107-pr.wby1-dev.medtronic.com/MdtAny_SfdcInt.SRServiceOrder.webservice.providers.ws:processServiceOrder_WSDL" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><SRServiceOrderResponse><isSuccess>' + this.tIsSuccess + '</isSuccess><msg>' + this.tMsg + '</msg></SRServiceOrderResponse></ser-root:processServiceOrderResponse></soapenv:Body></soapenv:Envelope>';
        }else if (tStatus == 'ERROR_BAD_RESPONSE'){
//            tBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body><ser-root:processServiceOrderResponse xmlns:ser-root="http://msplwa107-pr.wby1-dev.medtronic.com/MdtAny_SfdcInt.SRServiceOrder.webservice.providers.ws:processServiceOrder_WSDL" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><SRServiceOrderResponse><isSuccess>' + this.tIsSuccess + '</isSuccess><msg>' + this.tMsg + '</msg></SRServiceOrderResponse></ser-root:processServiceOrderResponse><SRServiceOrderResponse><isSuccess>' + this.tIsSuccess + '</isSuccess><msg>' + this.tMsg + '</msg></SRServiceOrderResponse></ser-root:processServiceOrderResponse><SRServiceOrderResponse><isSuccess>' + this.tIsSuccess + '</isSuccess><msg>' + this.tMsg + '</msg></SRServiceOrderResponse></ser-root:processServiceOrderResponse></soapenv:Body></soapenv:Envelope>';
            tBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body></soapenv:Body></soapenv:Envelope>';
//            tBody = '<webM:exception xmlns:webM="http://www.webMethods.com/2001/10/soap/encoding"><webM:className>com.wm.app.b2b.server.ServiceException</webM:className><webM:message xml:lang="">[ISS.0086.9311] Could not save pipeline to file MdtAny_SfdcInt.SRServiceOrder.webservice.providers.impl:processServiceOrder_%SRServiceOrderRequest/SFDC_ID%.</webM:message><webM:stackTrace xml:lang="">com.wm.app.b2b.server.ServiceException: [ISS.0086.9311] Could not save pipeline to file MdtAny_SfdcInt.SRServiceOrder.webservice.providers.impl:processServiceOrder_%SRServiceOrderRequest/SFDC_ID%</webM:stackTrace></webM:exception>';
        }else{
            tBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body><ser-root:processServiceOrderResponse xmlns:ser-root="http://msplwa107-pr.wby1-dev.medtronic.com/MdtAny_SfdcInt.SRServiceOrder.webservice.providers.ws:processServiceOrder_WSDL" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><SRServiceOrderResponse><isSuccess>' + this.tIsSuccess + '</isSuccess><msg>' + this.tMsg + '</msg></SRServiceOrderResponse></ser-root:processServiceOrderResponse></soapenv:Body></soapenv:Envelope>';
        }

        
        // Create a fake HTTP Response
        HttpResponse oHTTPResponse = new HttpResponse();
            oHTTPResponse.setHeader('Content-Type', 'text/xml; charset=utf-8');
            for (String tKey : this.mapResponseHeader.keySet()) {
                oHTTPResponse.setHeader(tKey, this.mapResponseHeader.get(tKey));
            }
            oHTTPResponse.setBody(tBody);
            oHTTPResponse.setStatusCode(this.iStatusCode);
            oHTTPResponse.setStatus(this.tStatus);
        return oHTTPResponse;
    }
    //----------------------------------------------------------------------------------------

}
//--------------------------------------------------------------------------------------------------------------------