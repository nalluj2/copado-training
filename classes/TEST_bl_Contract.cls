/*
 *      Description : This is the Test Class for the APEX Class bl_Contract
 *      Version     : 1.0
 *      Author      : Bart Caelen
 *      Date        : 20140623
*/
@IsTest
private class TEST_bl_Contract {
	
	@isTest static void bl_Contract_getPOAmountSumFromRelatedAssetContract() {

	    //---------------------------------------
        // Create Test Data
        //---------------------------------------
		RecordType oRT_Contract = RecordTypeMedtronic.getRecordTypeByDevName('Contract', 'RTG_Service_Contract');
		clsTestData.createContractData(true, oRT_Contract.Id);
		clsTestData.createAssetContractData(true);
        //---------------------------------------

        Test.startTest();
		bl_Contract.getPOAmountSumFromRelatedAssetContract(clsTestData.lstContract);
        Test.stopTest();
	}

    @isTest static void bl_Contract_getServiceLevelFromRelatedAssetContract_Insert() {
        
        Set<String> setContractLevel = new Set<String>();
        Map<Integer, String> mapServiceLevel = new Map<Integer, String>();
            mapServiceLevel.put(1, 'Silver');
            mapServiceLevel.put(2, 'Gold');
            mapServiceLevel.put(3, 'Platinum');
            mapServiceLevel.put(4, 'ESP');
            mapServiceLevel.put(5, 'ESP+');
            mapServiceLevel.put(6, 'Diamond');
            mapServiceLevel.put(7, 'Diamond+');

        //---------------------------------------
        // Create Test Data
        //---------------------------------------
        RecordType oRT_Contract = RecordTypeMedtronic.getRecordTypeByDevName('Contract', 'RTG_Service_Contract');
        clsTestData.iRecord_Contract = 1;
        clsTestData.createContractData(true, oRT_Contract.Id);
        clsTestData.iRecord_AssetContract = 7;
        clsTestData.createAssetContractData(false);
        Integer iCounter = 1;
        for (AssetContract__c oAssetContract : clsTestData.lstAssetContract){
            oAssetContract.Service_level__c = mapServiceLevel.get(iCounter);
            iCounter++;
        }
        //---------------------------------------

        List<Contract> lstContract = [SELECT Id, Contract_Level__c FROM Contract WHERE Id = :clsTestData.lstContract];
        System.assertEquals(lstContract.size(), 1);
        System.assertEquals(lstContract[0].Contract_Level__c, null);

        Test.startTest();
        insert clsTestData.lstAssetContract;
        System.assertEquals(clsTestData.lstAssetContract.size(), 7);

        lstContract = [SELECT Id, Contract_Level__c FROM Contract WHERE Id = :clsTestData.lstContract];
        System.assertEquals(lstContract.size(), 1);
        System.assertNotEquals(lstContract[0].Contract_Level__c, null);
        
        bl_Contract.getServiceLevelFromRelatedAssetContract(lstContract);
        Test.stopTest();

        String tContractLevel = lstContract[0].Contract_Level__c;
        setContractLevel.addAll(tContractLevel.split(';')); 
        System.assert(setContractLevel.contains(mapServiceLevel.get(1)));
        System.assert(setContractLevel.contains(mapServiceLevel.get(2)));
        System.assert(setContractLevel.contains(mapServiceLevel.get(3)));
        System.assert(setContractLevel.contains(mapServiceLevel.get(4)));
        System.assert(setContractLevel.contains(mapServiceLevel.get(5)));
        System.assert(setContractLevel.contains(mapServiceLevel.get(6)));
        System.assert(setContractLevel.contains(mapServiceLevel.get(7)));

    }


    @isTest static void bl_Contract_getServiceLevelFromRelatedAssetContract_UpdateDelete() {
        
        Set<String> setContractLevel = new Set<String>();
        Map<Integer, String> mapServiceLevel = new Map<Integer, String>();
            mapServiceLevel.put(1, 'Silver');
            mapServiceLevel.put(2, 'Gold');
            mapServiceLevel.put(3, 'Platinum');
            mapServiceLevel.put(4, 'ESP');
            mapServiceLevel.put(5, 'ESP+');
            mapServiceLevel.put(6, 'Diamond');
            mapServiceLevel.put(7, 'Diamond+');

        //---------------------------------------
        // Create Test Data
        //---------------------------------------
        RecordType oRT_Contract = RecordTypeMedtronic.getRecordTypeByDevName('Contract', 'RTG_Service_Contract');
        clsTestData.iRecord_Contract = 1;
        clsTestData.createContractData(true, oRT_Contract.Id);
        clsTestData.iRecord_AssetContract = 7;
        clsTestData.createAssetContractData(false);
        Integer iCounter = 1;
        for (AssetContract__c oAssetContract : clsTestData.lstAssetContract){
            oAssetContract.Service_level__c = mapServiceLevel.get(iCounter);
            iCounter++;
        }
        //---------------------------------------

        List<Contract> lstContract = [SELECT Id, Contract_Level__c FROM Contract WHERE Id = :clsTestData.lstContract];
        System.assertEquals(lstContract.size(), 1);
        System.assertEquals(lstContract[0].Contract_Level__c, null);
        
        insert clsTestData.lstAssetContract;
        bl_Asset_Contract.alreadyProcessedServiceLevel.clear();
        
        Test.startTest();
        
        System.assertEquals(clsTestData.lstAssetContract.size(), 7);

        bl_Contract.getServiceLevelFromRelatedAssetContract(lstContract);

        System.assertEquals(lstContract.size(), 1);
        System.assertNotEquals(lstContract[0].Contract_Level__c, null);
        
        for (AssetContract__c oAssetContract : clsTestData.lstAssetContract){
            if (oAssetContract.Service_Level__c == 'Diamond+'){
                delete oAssetContract;
                bl_Asset_Contract.alreadyProcessedServiceLevel.clear();
            }

            if (oAssetContract.Service_Level__c == 'Diamond'){
                oAssetContract.Service_Level__c = 'Diamond+';
                update oAssetContract;
                bl_Asset_Contract.alreadyProcessedServiceLevel.clear();
            }

        }

        bl_Contract.getServiceLevelFromRelatedAssetContract(lstContract);

        Test.stopTest();

        lstContract = [SELECT Id, Contract_Level__c FROM Contract WHERE Id = :clsTestData.lstContract];
        System.assertEquals(lstContract.size(), 1);
        System.assertNotEquals(lstContract[0].Contract_Level__c, null);

        String tContractLevel = lstContract[0].Contract_Level__c;
        setContractLevel.addAll(tContractLevel.split(';')); 
        System.assert(setContractLevel.contains(mapServiceLevel.get(1)));
        System.assert(setContractLevel.contains(mapServiceLevel.get(2)));
        System.assert(setContractLevel.contains(mapServiceLevel.get(3)));
        System.assert(setContractLevel.contains(mapServiceLevel.get(4)));
        System.assert(setContractLevel.contains(mapServiceLevel.get(5)));
        System.assert(!setContractLevel.contains(mapServiceLevel.get(6)));
        System.assert(setContractLevel.contains(mapServiceLevel.get(7)));

    }


    @IsTest(SeeAllData=false) static void bl_Contract_copyAssets() {


        //---------------------------------------
        // CREATE TEST DATA
        //---------------------------------------
        // Create Opportunity
        clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'Service_Contract_Opportunity').Id;
        clsTestData_Opportunity.createOpportunity();

        // Create Contract
        clsTestData_Contract.iRecord_Contract = 1;
        clsTestData_Contract.idRecordType_Contract = clsUtil.getRecordTypeByDevName('Contract', 'RTG_Service_Contract').Id;
        clsTestData_Contract.createContract(false);
        clsTestData_Contract.oMain_Contract.Opportunity__c = clsTestData_Opportunity.oMain_Opportunity.Id;
        insert clsTestData_Contract.oMain_Contract;

        // Create Products & Assets
        clsTestData_Product.iRecord_Product = 5;
        clsTestData_Product.createProduct();
        clsTestData_Asset.createAsset();
        List<String> lstServiceLevel = new List<String>{'Platinum', 'ESP', 'ESP+', 'Diamond', 'Diamond K'};
        Map<Id, String> mapAssetId_ServiceLevel = new Map<Id, String>();
        Integer iCounter = 0;
        for (Asset oAsset : clsTestData_Asset.lstAsset){
            mapAssetId_ServiceLevel.put(oAsset.Id, lstServiceLevel[iCounter]);
            iCounter++;
            if (iCounter == 5) iCounter = 0;
        }

        // Create Opportunity Assets
        List<Opportunity_Asset__c> lstOpportunityAsset = new List<Opportunity_Asset__c>();
        iCounter = 0;
        for (Asset oAsset : clsTestData_Asset.lstAsset){
            Opportunity_Asset__c oOpportunityAsset = new Opportunity_Asset__c();
                oOpportunityAsset.Asset__c = oAsset.Id;
                oOpportunityAsset.Opportunity__c = clsTestData_Opportunity.oMain_Opportunity.Id;
                oOpportunityAsset.Service_Level__c = mapAssetId_ServiceLevel.get(oAsset.Id);    
            lstOpportunityAsset.add(oOpportunityAsset);

            iCounter++;
            if (iCounter == 5) break;
        }
        insert lstOpportunityAsset;
        //---------------------------------------


        //---------------------------------------
        // EXECUTE LOGIC
        //---------------------------------------
        Test.startTest();

        bl_Contract.copyAssets(clsTestData_Contract.lstContract);

        Test.stopTest();
        //---------------------------------------


        //---------------------------------------
        // VALIDATE RESULT
        //---------------------------------------
        List<AssetContract__c> lstAssetContract = [SELECT Id, Asset__c, Service_Level__c FROM AssetContract__c];
        System.assertEquals(lstAssetContract.size(), lstOpportunityAsset.size());
        for (AssetContract__c oAssetContract : lstAssetContract){

            System.assertEquals(oAssetContract.Service_Level__c, mapAssetId_ServiceLevel.get(oAssetContract.Asset__c));

        }
        //---------------------------------------


    }

}