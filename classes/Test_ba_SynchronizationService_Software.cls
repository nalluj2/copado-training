@isTest
private class Test_ba_SynchronizationService_Software {
	
	private static testMethod void synchSoftwareChanges(){
		
		Synchronization_Service_Setting__c syncSettings = new Synchronization_Service_Setting__c();
		syncSettings.username__c = 'test@medtronic.com';
		syncSettings.password__c = 'password';
		syncSettings.client_id__c = '123456789';
		syncSettings.client_secret__c = 'XXXXX';
		syncSettings.is_sandbox__c = true;
		syncSettings.Sync_Enabled__c = true;
		insert syncSettings;
		
		//Products
		Product2 testProduct = new Product2();
		testProduct.Name = 'Test Product';
		testProduct.ProductCode = '000999';
		testProduct.isActive = true;					
		insert testProduct;
		
		Test.startTest();
		
		ba_SynchronizationService_Software batch = new ba_SynchronizationService_Software();
		batch.execute(null);
		
		CalloutMock mockImpl = new CalloutMock();
		
		Test.setMock(HttpCalloutMock.class, mockImpl);
		bl_SynchronizationService_Utils.calloutMock = mockImpl;	
		
		Test.stopTest();
	}
	
	public class CalloutMock implements HttpCalloutMock {

        public HTTPResponse respond(HTTPRequest req) {
            
            HttpResponse resp = new HttpResponse();
            resp.setStatusCode(200);
           	resp.setStatus('Complete');
            	           
            if(req.getEndpoint().endsWith('services/oauth2/token')){
            	
            	String respBody =  '{' +
					            '"id" : "1",' +
					            '"issued_at" : "1",' +
					            '"instance_url" : "1",' +
					            '"signature" : "1",' +
					            '"access_token" : "1",' +
					            '"token_type" : "1"' +
				            '}';
				resp.setBody(respBody);
				            
            }else if(req.getEndpoint().contains('updated/?start=')){
            	
            	String respBody = 
        		'{' +
        			'"latestDateCovered": "latestDate",' +
					'"ids": [' +
        				'"softwareId"' +    
        			']' +
        		'}';
        		
        		resp.setBody(respBody);	
            	
            }else{
            	
				String respBody = 
        		'{' +
        			'"totalSize": 1,' +
					'"done": true,' +
					'"records": [' +
        				'{' +
	        				'"attributes": {' +
	            				'"type": "Software__c",' +
	            				'"url": "/services/data/v32.0/sobjects/Software__c/"' +
	        				'},' +        
	        				'"Name": "Test Software",' +
	        				'"Accessory__c": false,' +
	        				'"Active__c": true,' +
	        				'"Computer_Type__c": "Test",' +
	        				'"FCA__c": false,' +
	        				'"PMM_Notes__c": "Notes",' +
	        				'"Revision__c": "2",' +
	        				'"Software_Name__c": "Test Software",' +
	        				'"System_Type__c": "i7",' +
	        				'"Version__c": "1.1",' +
	        				'"External_Id__c": "Text_Software_Id",' +
	        				'"Software_Product__r": {' +
	        						'"attributes": {' +
	            					'"type": "Product2",' +
	            					'"url": "/services/data/v32.0/sobjects/Product2/"' +
	        					'},' +
	        					'"Name": "Test Product"' +
	        				'}' +        					        					        				
        				'}' +            
        			']' +
        		'}';
        		
        		resp.setBody(respBody);		
            	
            } 							
                        
            return resp;
        }
	} 
}