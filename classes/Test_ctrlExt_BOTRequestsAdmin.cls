@isTest
private class Test_ctrlExt_BOTRequestsAdmin {
	
	private static User currentUser = [Select Id, Email from User where Id=:UserInfo.getUserId()];
	
	private static testMethod void sfdcDispatcher(){
                       
        Create_User_Request__c userRequest = createRequest('Create User');
        userRequest.Description_Long__c = 'descr';
        insert userRequest;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(userRequest);
        ctrlExt_Support_Request_Dispatcher controller = new ctrlExt_Support_Request_Dispatcher(sc);
        
        controller.redirect();
    }
    
    private static testMethod void completeRequest(){
    	
    	System.runAs(currentUser){
            assignUserToAdminGroup();
        }	
        
        Create_User_Request__c userRequest = createRequest('Create User');
            userRequest.Firstname__c = 'test';
            userRequest.Lastname__c = 'user';
            userRequest.Email__c = 'test.user@medtronic.com';
            userRequest.Sub_Status__c = 'Solved (Permanently)';
            userRequest.Time_Spent__c = 60;
        insert userRequest;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(userRequest);
        ctrlExt_BOTRequestsAdmin controller = new ctrlExt_BOTRequestsAdmin(sc);
        
        controller.assignToMe();
        userRequest.Sub_Status__c = 'Solved (Permanently)';
        userRequest.Time_Spent__c = 100;

        Boolean bTest = controller.bIsUpdatable;
        bTest = controller.bIsAssigned;

        controller.completeRequest();

        System.assert(userRequest.Status__c == 'Completed');

        bTest = controller.bIsUpdatable;
        bTest = controller.bIsAssigned;

    }

    private static testMethod void completeRequest_Error(){
        
        System.runAs(currentUser){
            assignUserToAdminGroup();
        }   
        
        Create_User_Request__c userRequest = createRequest('Create User');
        userRequest.Firstname__c = 'test';
        userRequest.Lastname__c = 'user';
        userRequest.Email__c = 'test.user@medtronic.com';
        insert userRequest;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(userRequest);
        ctrlExt_BOTRequestsAdmin controller = new ctrlExt_BOTRequestsAdmin(sc);
        
        controller.assignToMe();
        controller.completeRequest();
        
        System.assert(userRequest.Status__c != 'Completed');

        userRequest.Sub_Status__c = 'Solved (Permanently)';
        update userRequest;
        
        sc = new ApexPages.StandardController(userRequest);
        controller = new ctrlExt_BOTRequestsAdmin(sc);
        
        controller.assignToMe();
        controller.completeRequest();
        
        System.assert(userRequest.Status__c != 'Completed');

        userRequest.Time_Spent__c = 60;
        update userRequest;
        
        sc = new ApexPages.StandardController(userRequest);
        controller = new ctrlExt_BOTRequestsAdmin(sc);
        
        controller.assignToMe();
        userRequest.Sub_Status__c = 'Solved (Permanently)';
        userRequest.Time_Spent__c = 100;
        controller.completeRequest();
        
        System.assert(userRequest.Status__c == 'Completed');

    }    

    private static testMethod void saveRequest(){
               
        Create_User_Request__c userRequest = createRequest('Generic Service');
        userRequest.Description_Long__c = 'descr';                
        insert userRequest;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(userRequest);
        ctrlExt_BOTRequestsAdmin controller = new ctrlExt_BOTRequestsAdmin(sc);
                
        controller.saveRequest();


        userRequest.Internal_Notes__c = 'Internal Notes';                
        userRequest.Time_Spent__c = 999999;
        try{
            sc = new ApexPages.StandardController(userRequest);
            controller = new ctrlExt_BOTRequestsAdmin(sc);
                    
            controller.saveRequest();
        }catch(Exception oEX){

        }
        
    }

    private static testMethod void assignToOtherUser(){

        System.runAs(currentUser){
            assignUserToAdminGroup();
        }
                        
        Create_User_Request__c userRequest = createRequest('Data Extract');
        userRequest.Description_Long__c = 'descr';                
        userRequest.BOT_File_Type__c = 'xls';
        insert userRequest;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(userRequest);
        ctrlExt_BOTRequestsAdmin controller = new ctrlExt_BOTRequestsAdmin(sc);

        System.assert(controller.isAdminUser == true);

        Id id_AdminUser;
        for (Id id_User : mapUser_Admin.keySet()){
            if (id_User != UserInfo.getUserId()){
                id_AdminUser = id_User;
                break;
            }
        }
        if (id_AdminUser != null){
            controller.tAdminUserId = id_AdminUser;
            controller.saveRequest();
        }

    }   

    private static Map<Id, User> mapUser_Admin = new Map<Id, User>();
    private static void assignUserToAdminGroup(){
        
        List<GroupMember> memberships = [Select UserOrGroupId from GroupMember where Group.DeveloperName = 'Support_Portal_BOT' AND UserOrGroupId = :UserInfo.getUserId()];
        
        if(memberships.isEmpty()){
        
            Group adminGroup = [Select Id from Group where DeveloperName = 'Support_Portal_BOT'];
            
            GroupMember membership = new GroupMember();
            membership.GroupId = adminGroup.Id;
            membership.UserOrGroupId = UserInfo.getUserId();
            
            insert membership;
        }  

        mapUser_Admin = bl_CreateUserRequest.loadAdminUsers('Support_Portal_BOT');
             
    }

    
    private static Create_User_Request__c createRequest(String requestType){
        
        Create_User_Request__c userRequest = new Create_User_Request__c();          
        userRequest.Status__c = 'New';
        userRequest.Application__c = 'BOT';
        userRequest.Request_Type__c = requestType;          
        userRequest.Requestor_Email__c = currentUser.Email;     
        
        return userRequest;
    }
}