/*
  *  Class Name    : CasePartLink
  *  VF Page Name  : CasePartLink
  *  Description   : This class include the logic to attach CaseParts related to the Work-Order AND Part-Shipment being viewed,this class related to Custom Button Find Part                
  *  Created Date  : 28/8/2013
  *  Author        : Wipro Tech. 
*/

public class CasePartLink {
    public Id currentRecordId;
    Public Workorder_Sparepart__c CasePart{get;set;}
    Public String ObjectName;
    public id currentCaseId;
    public id currentCaseId1;
    //public Id currentComplaintId;
    Public Id CurrentWoId;
    public String PONumber; // Added by Wipro-- 4/20/2014
    public boolean selected{get;set;} 
    public List<WrapperCls> CAsePartList{get;set;}
    public CasePartLink(ApexPages.StandardController controller) {
        currentRecordId=ApexPages.currentPage().getParameters().get('id');
        ObjectName = currentRecordId.getSObjectType().getDescribe().getName();
        
        System.Debug('Obj name'+ObjectName );
        //List<sObject> ListWo ;
        //Checking whether Case Part related to Work_Order
        if(ObjectName == 'Workorder__c'){
            List<workorder__c> ListWo = new List<workorder__c>();
            CasePart = new Workorder_Sparepart__c(); //Modified by Niha
            ListWo = [Select Case__c from workorder__c where Id =: CurrentRecordId];
            currentCaseId = ListWo[0].case__c;
            //currentComplaintId = ListWo[0].complaint__c;
 
        }
        //Checking whether Case Part related to Part_Shipment
        if(ObjectName == 'Parts_Shipment__c'){
            List<Parts_Shipment__c>ListPs = new List<Parts_Shipment__c>();
            CasePart = new Workorder_Sparepart__c(); //Modified by Niha
            ListPs = [Select Case__c,workorder__c,PO_Number__c from Parts_Shipment__c where Id =: CurrentRecordId];// Modified by Wipro-- 4/20/2014
            System.Debug('lst ps'+ListPs);
            currentCaseId = ListPs[0].case__c;
            //currentComplaintId = ListPs[0].complaint__c;
            CurrentWoId = ListPS[0].workorder__c;
           PONumber=ListPS[0].PO_Number__c;// Added by Wipro-- 4/20/2014
        }
      
        
    }
    
   //Binding all Case Part related to Work-Order which follows the below condition
    public List<WrapperCls> getCaseParts(){
       if(CAsePartList == null) {       
       CAsePartList = new List<WrapperCls>();
           if(ObjectName == 'Workorder__c'){
               for(Workorder_Sparepart__c o:[Select Id,Name,RI_Part__c,RecordType.Name,Software__c,Case__r.CaseNumber,Order_Status__c,Order_Part__c,Return_Item_Status__c,owner.name FROM Workorder_Sparepart__c WHERE workorder__c !=: currentRecordId AND case__c =: currentCaseId AND case__c !='']){
                  CAsePartList.add(new WrapperCls(o)); 
               }
           }  
           //Binding all Case Part related to Part-Shipment which follows the below condition 
           if(ObjectName == 'Parts_Shipment__c'){
               system.debug('inside if ->>>>>>>>'+CAsePartList);
               for(Workorder_Sparepart__c o:[Select Id,Name,RI_Part__c,RecordType.Name,Software__c,Case__r.CaseNumber,Order_Status__c,Order_Part__c,Return_Item_Status__c,owner.name FROM Workorder_Sparepart__c WHERE Parts_Shipment__c !=:currentRecordId AND ((workorder__c =:CurrentWoId AND workorder__c!='') OR  (case__c =: currentCaseId AND case__c !=''))]){
                  System.Debug('value returned '+o);
                  CAsePartList.add(new WrapperCls(o)); 
               }
           }   
       System.Debug('cpl' +CasePartList);
       }
       return CAsePartList;
    }
    //After clicking Find part button,VF page will open,Clicking on FindLink button Selecting CasePart automatically retuen to main Page
    public PageReference CasePartSelected() {
    
        List<Workorder_Sparepart__c> selectedCaseParts = new List<Workorder_Sparepart__c>();
        for(WrapperCls wc:getCaseParts()) {
            if(wc.selected == true) {
                if(ObjectName == 'workorder__c'){
                    wc.cpInstance.workorder__c = currentRecordId;
                    selectedCaseParts.add(wc.cpInstance);
                }
                if(ObjectName == 'Parts_Shipment__c'){
                    wc.cpInstance.Parts_Shipment__c = currentRecordId;
                  // Added by Wipro-- 4/20/2014
                    if(PONumber != null)
                    {
                    	wc.cpInstance.PO_Number__c =PONumber;
                    }
                    // Ended by Wipro-- 4/20/2014
                    
                    selectedCaseParts.add(wc.cpInstance);
                }
                
             }
         }    
         System.Debug('chek'+selectedCaseParts);
         Update selectedCaseParts;
         PageReference pageRef = new PageReference('/'+currentRecordId);

         return pageRef; 
    }
    
    Public PageReference CancelLink(){
    //If click on Cancel Button
    PageReference pageRefCancel = new PageReference('/'+currentRecordId);

         return pageRefCancel; 
    }
    public PageReference redirectToCasePart() {
    					
		Map<String, String> technicalIds = new Map<String, String>();
		
		for(Technical_Ids__c technicalId : [Select Primary_Id__c, Key__c from Technical_Ids__c where Key__c LIKE 'Workorder_Sparepart__c%']){
			technicalIds.put(technicalId.Key__c, technicalId.Primary_Id__c);
		}
		
        String casePartObjectId = technicalIds.get('Workorder_Sparepart__c');
		
		String caseFieldId = technicalIds.get('Workorder_Sparepart__c.Case__c');
		String partsShipmentFieldId = technicalIds.get('Workorder_Sparepart__c.Parts_Shipment__c');
		String workorderFieldId = technicalIds.get('Workorder_Sparepart__c.Workorder__c');
		  
        Schema.DescribeSObjectResult describeObj = Workorder_Sparepart__c.sObjectType.getDescribe();
        PageReference pageRef;    
        System.debug('ObjectName----->'+ObjectName);
        System.debug('CurrentRecordId----->'+CurrentRecordId);
       /* if(test.isRunningTest())
        {
        	ObjectName = 'Parts_Shipment__c';
        }*/
        if(ObjectName == 'Parts_Shipment__c'){       
           Parts_Shipment__c objPS=new Parts_Shipment__c(); 
           
           objPS = [Select id,name,Case__c,workorder__c from Parts_Shipment__c where Id =: CurrentRecordId];        
           pageRef = new PageReference('/setup/ui/recordtypeselect.jsp?ent='+casePartObjectId+'&retURL=%2F'+objPS.id+'&save_new_url=%2F'+describeObj.keyPrefix+'%2Fe%3FCF'+partsShipmentFieldId+'%3D'+objPS.Name);
        }
        
       /*  if(test.isRunningTest())
        {
        	ObjectName = 'workorder__c';
        }*/
        
        if(ObjectName == 'WorkOrder__c'){       
            WorkOrder__c objWo=new WorkOrder__c();
            objWo = [Select id,name from workorder__c where Id =: CurrentRecordId];        
            pageRef = new PageReference('/setup/ui/recordtypeselect.jsp?ent='+casePartObjectId+'&retURL=%2F'+objWo.id+'&save_new_url=%2F'+describeObj.keyPrefix+'%2Fe%3FCF'+workorderFieldId+'%3D'+objWo.Name);
        }
        
      /*  if(test.isRunningTest())
        {
        	ObjectName = 'Case';
        }*/
        if(ObjectName == 'Case'){       
            Case objCase=new Case();
            objCase = [Select id,CaseNumber from Case where Id =: CurrentRecordId];        
            pageRef = new PageReference('/setup/ui/recordtypeselect.jsp?ent='+casePartObjectId+'&retURL=%2F'+objCase.id+'&save_new_url=%2F'+describeObj.keyPrefix+'%2Fe%3FCF'+caseFieldId+'%3D'+objCase.CaseNumber);
        }
        
        WrapperCls objWrapperCls = new WrapperCls(null);
        return pageRef ; 
        return null;
    }
    
    Public Class WrapperCls{
        Public Workorder_Sparepart__c cpInstance {get;set;}
        Public Boolean selected{get;set;}
        Public WrapperCls(Workorder_Sparepart__c orc){
            cpInstance = orc;
            selected = false;
        }
    }
    
   

}