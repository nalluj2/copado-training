@isTest
private class Test_bl_Segmentation {
    
    @testSetup
    private static void createTestData(){
    	
		Company__c cmpny = new Company__c();
	    cmpny.name='TestMedtronic';
	    cmpny.CurrencyIsoCode = 'EUR';
	    cmpny.Current_day_in_Q1__c=56;
	    cmpny.Current_day_in_Q2__c=34; 
	    cmpny.Current_day_in_Q3__c=5; 
	    cmpny.Current_day_in_Q4__c= 0;   
	    cmpny.Current_day_in_year__c =200;
	    cmpny.Days_in_Q1__c=56;  
	    cmpny.Days_in_Q2__c=34;
	    cmpny.Days_in_Q3__c=13;
	    cmpny.Days_in_Q4__c=22;
	    cmpny.Days_in_year__c =250;
	    cmpny.Company_Code_Text__c = 'T33';
	    insert cmpny;
	
	    //Insert Business Unit
	    Business_Unit__c bu =  new Business_Unit__c();
	    bu.Company__c =cmpny.id;
	    bu.name='BUMedtronic';
	    bu.Account_Plan_Activities__c=true;
	    insert bu;
	   
		//Insert SBU
	    Sub_Business_Units__c sbu = new Sub_Business_Units__c();
	    sbu.name='SBUMedtronic1';
	    sbu.Business_Unit__c=bu.id;
	    sbu.Account_Plan_Default__c='Revenue';
	    sbu.Account_Flag__c = 'AF_Solutions__c';
	    insert sbu;
		    	   
	    //Insert Therapy Group
	    Therapy_Group__c tg = new Therapy_Group__c();
	    tg.Name='Biologics';
	    tg.Sub_Business_Unit__c = sbu.Id;
	    tg.Company__c = cmpny.Id;
	    insert tg;
	
	    //Insert Therapies
	    Therapy__c tp1 = new Therapy__c();
	    tp1.Name='Biologics';
	    tp1.Therapy_Group__c = tg.id;
	    tp1.Business_Unit__c=bu.id;
	    tp1.Sub_Business_Unit__c=sbu.id;
	    tp1.Therapy_Name_Hidden__c = 'Biologics';
	    	    
	    Therapy__c tp2 = new Therapy__c();
	    tp2.Name='Cervical';
	    tp2.Therapy_Group__c = tg.id;
	    tp2.Business_Unit__c=bu.id;
	    tp2.Sub_Business_Unit__c=sbu.id;
	    tp2.Therapy_Name_Hidden__c = 'Cervical';
	    	    
	    Therapy__c tp3 = new Therapy__c();
        tp3.Therapy_Group__c = tg.id;
        tp3.Name='Implantable Diagnostics';
	    tp3.Business_Unit__c=bu.id;
	    tp3.Sub_Business_Unit__c=sbu.id;
	    tp3.Therapy_Name_Hidden__c = 'Implantable Diagnostics';
	    
	    insert new List<Therapy__c>{tp1, tp2, tp3};
    	
    }
    
    private static testmethod void testCreateSegmentation(){
    	
    	Therapy__c biologics = [Select Id, Name from Therapy__c where Name = 'Biologics'];
    	Therapy__c cervical = [Select Id, Name from Therapy__c where Name = 'Cervical'];
    	Therapy__c diagnostic = [Select Id, Name from Therapy__c where Name = 'Implantable Diagnostics'];
    	
    	Account acc1 = new account();
     	acc1.name = 'Account1';
     	     
     	Account acc2 = new account();
     	acc2.name = 'Account2';
     	
     	insert new List<Account>{acc1, acc2};
     	
     	Test.startTest();
     	
     	Segmentation__c acc1Biologics= new Segmentation__c();
	    acc1Biologics.Account__c = acc1.Id;
	    acc1Biologics.Therapy__c = biologics.Id;
	    acc1Biologics.Start__c = System.today().addDays(-60);
	    acc1Biologics.Segment__c='Top';
	    
	    Segmentation__c acc1Cervical= new Segmentation__c();
	    acc1Cervical.Account__c = acc1.Id;
	    acc1Cervical.Therapy__c = cervical.Id;
	    acc1Cervical.Start__c = System.today().addDays(-40);
	    acc1Cervical.Segment__c='Top';
	    
	    Segmentation__c acc2Diagnostic= new Segmentation__c();
	    acc2Diagnostic.Account__c = acc2.Id;
	    acc2Diagnostic.Therapy__c = diagnostic.Id;
	    acc2Diagnostic.Start__c = System.today().addDays(-7);
	    acc2Diagnostic.Segment__c='Maintain';
	    
	    Segmentation__c acc2Cervical= new Segmentation__c();
	    acc2Cervical.Account__c = acc2.Id;
	    acc2Cervical.Therapy__c = cervical.Id;
	    acc2Cervical.Start__c = System.today().addDays(-14);
	    acc2Cervical.End__c = System.today();
	    acc2Cervical.Segment__c='Maintain';
	    
	    insert new List<Segmentation__c>{acc1Biologics, acc1Cervical, acc2Diagnostic, acc2Cervical};
	    	    
	    Segmentation__c acc1BiologicsNew = new Segmentation__c();
	    acc1BiologicsNew.Account__c = acc1.Id;
	    acc1BiologicsNew.Therapy__c = biologics.Id;
	    acc1BiologicsNew.Start__c = System.today().addDays(-10);
	    acc1BiologicsNew.Segment__c='Let Go On';
	    insert acc1BiologicsNew;
	    
	    acc1 = [Select Id from Account where Id = :acc1.Id];
	    
	    acc1Biologics = [Select End__c from Segmentation__c where Id = :acc1Biologics.Id];
	    System.assert(acc1Biologics.End__c == System.today().addDays(-11));
	    
	    Segmentation__c acc2CervicalNew= new Segmentation__c();
	    acc2CervicalNew.Account__c = acc2.Id;
	    acc2CervicalNew.Therapy__c = cervical.Id;
	    acc2CervicalNew.Start__c = System.today();	    
	    acc2CervicalNew.Segment__c='Maintain';
	    
	    Boolean isError = false;
	    
	    try{
	    	
	    	insert acc2CervicalNew;
	    	
	    }catch(Exception e){
	    	
	    	isError = true;
	    	
	    	System.assert(e.getMessage().contains('This Segmentation overlaps with the record'));
	    }
	    
	    System.assert(isError == true);	    
    }
    
    private static testmethod void testUpdateSegmentation(){
    	
    	Therapy__c biologics = [Select Id, Name from Therapy__c where Name = 'Biologics'];
    	Therapy__c cervical = [Select Id, Name from Therapy__c where Name = 'Cervical'];
    	Therapy__c diagnostic = [Select Id, Name from Therapy__c where Name = 'Implantable Diagnostics'];
    	
    	Account acc = new account();
     	acc.name = 'Account1';
     	insert acc;
     	
     	Test.startTest();
     	
     	Segmentation__c accBiologics= new Segmentation__c();
	    accBiologics.Account__c = acc.Id;
	    accBiologics.Therapy__c = biologics.Id;
	    accBiologics.Start__c = System.today().addDays(-60);
	    accBiologics.Segment__c='Top';
	    
	    Segmentation__c accCervical= new Segmentation__c();
	    accCervical.Account__c = acc.Id;
	    accCervical.Therapy__c = cervical.Id;
	    accCervical.Start__c = System.today().addDays(-40);
	    accCervical.Segment__c='Maintain';
	    
	    Segmentation__c accDiagnostic= new Segmentation__c();
	    accDiagnostic.Account__c = acc.Id;
	    accDiagnostic.Therapy__c = diagnostic.Id;
	    accDiagnostic.Start__c = System.today().addDays(-7);
	    accDiagnostic.Segment__c='Let Go On';
	    	    	    
	    insert new List<Segmentation__c>{accBiologics, accCervical, accDiagnostic};
	    	    
	    // Update a current Segmentation
	    accDiagnostic.Segment__c='Maintain';
	    update accDiagnostic;
	    	    
	    // Create a new current Segmentation
	    Segmentation__c accCervicalNew= new Segmentation__c();
	    accCervicalNew.Account__c = acc.Id;
	    accCervicalNew.Therapy__c = cervical.Id;
	    accCervicalNew.Start__c = System.today().addDays(-14);	    
	    accCervicalNew.Segment__c='Top';
	    insert accCervicalNew;
	    	    
	    accCervical = [Select End__c from Segmentation__c where Id = :accCervical.Id];
	    System.assert(accCervical.End__c == System.today().addDays(-15));
	    
	    // Update old Segmentation to make it current
	    accCervical.End__c = null;
	    
	    Boolean isError = false;
	    
	    try{
	    	
	    	update accCervical;
	    	
	    }catch(Exception e){
	    	
	    	isError = true;
	    	
	    	System.assert(e.getMessage().contains('This Segmentation overlaps with the record'));
	    }
	    
	    System.assert(isError == true);	 
	    
    }
    
    private static testmethod void testDeleteUndeleteSegmentation(){
    	
    	Therapy__c biologics = [Select Id, Name from Therapy__c where Name = 'Biologics'];
    	Therapy__c cervical = [Select Id, Name from Therapy__c where Name = 'Cervical'];
    	Therapy__c diagnostic = [Select Id, Name from Therapy__c where Name = 'Implantable Diagnostics'];
    	
    	Account acc = new account();
     	acc.name = 'Account1';
     	insert acc;
     	
     	Test.startTest();
     	
     	Segmentation__c accBiologics= new Segmentation__c();
	    accBiologics.Account__c = acc.Id;
	    accBiologics.Therapy__c = biologics.Id;
	    accBiologics.Start__c = System.today().addDays(-60);
	    accBiologics.Segment__c='Top';
	    
	    Segmentation__c accCervical= new Segmentation__c();
	    accCervical.Account__c = acc.Id;
	    accCervical.Therapy__c = cervical.Id;
	    accCervical.Start__c = System.today().addDays(-40);
	    accCervical.Segment__c='Maintain';
	    
	    Segmentation__c accDiagnostic= new Segmentation__c();
	    accDiagnostic.Account__c = acc.Id;
	    accDiagnostic.Therapy__c = diagnostic.Id;
	    accDiagnostic.Start__c = System.today().addDays(-7);
	    accDiagnostic.Segment__c='Let Go On';
	    	    	    
	    insert new List<Segmentation__c>{accBiologics, accCervical, accDiagnostic};
	    
	    delete new List<Segmentation__c>{accCervical, accDiagnostic};
	    	    	    	    
	    // Undelete Segmentation
	    undelete accDiagnostic;
	    
	    // Create a new current Segmentation
	    Segmentation__c accCervicalNew= new Segmentation__c();
	    accCervicalNew.Account__c = acc.Id;
	    accCervicalNew.Therapy__c = cervical.Id;
	    accCervicalNew.Start__c = System.today().addDays(-14);	    
	    accCervicalNew.Segment__c='Top';
	    insert accCervicalNew;
	    	    	    	    
	    // Undelete Segmentation with duplicate
	    Boolean isError = false;
	    
	    try{
	    	
	    	undelete accCervical;
	    	
	    }catch(Exception e){
	    	
	    	isError = true;
	    	
	    	System.assert(e.getMessage().contains('This Segmentation overlaps with the record'));
	    }
	    
	    System.assert(isError == true);	 
    }
}