@isTest
private class Test_ba_Admin_Request_Termination {
	
	private static testmethod void testBatchAdminRequestTermination_Production(){
		
	 	User testUser = [Select Id from User where isActive = true AND Profile.Name = 'IT Business Analyst' AND Id != :UserInfo.getUserId() LIMIT 1];
		
		User currentUser = new User(Id = UserInfo.getUserId());
		
		System.runAs(currentUser){
			
			List<PermissionSetAssignment> perms = [Select Id from PermissionSetAssignment where AssigneeId = :testUser.Id AND PermissionSet.Name = 'Org_Configuration'];
			
			if(perms.size() == 0){
		 		
		 		PermissionSet perm = [Select ID from PermissionSet where Name = 'Org_Configuration'];
		 	
		 		PermissionSetAssignment userPerm = new PermissionSetAssignment();
		 		userPerm.AssigneeId = testUser.Id;
		 		userPerm.PermissionSetId = perm.Id;
		 		insert userPerm;
			}
		}
	 	
	 	Admin_Request__c adminReq = new Admin_Request__c();
		adminReq.User__c = testUser.Id;
		adminReq.Environment__c = 'Production';
		adminReq.Permissions__c = 'Org Configuration;Patient Data Access';
		adminReq.Start_Date__c = Date.today().addDays(-2);
		adminReq.End_Date__c = Date.today();
		adminReq.Status__c = 'Approved';
		adminReq.Reason__c = 'Test';
		insert adminReq;
	 		 			 	
		Test.startTest();
				
		Database.executeBatch(new ba_Admin_Request_Termination('system@medtronic.com', 'fakePassword'), 1);  
			
		Test.stopTest();
			
		List<PermissionSetAssignment> assignments = [Select Id from PermissionSetAssignment where AssigneeId = :testUser.Id AND PermissionSet.Name = 'Org_Configuration'];
	    System.assert(assignments.size() == 0);
			
		adminReq = [Select Admin_Request_Revoked__c from Admin_Request__c where Id = :adminReq.Id];		
		System.assert(adminReq.Admin_Request_Revoked__c == true);
	 }
	 
	 private static testmethod void testBatchAdminRequestTermination_ProductionManual(){
		
	 	User testUser = [Select Id from User where isActive = true AND Profile.Name = 'IT Business Analyst' AND Id != :UserInfo.getUserId() LIMIT 1];
		
		User currentUser = new User(Id = UserInfo.getUserId());
		
		System.runAs(currentUser){
			
			List<PermissionSetAssignment> perms = [Select Id from PermissionSetAssignment where AssigneeId = :testUser.Id AND PermissionSet.Name = 'Org_Configuration'];
			
			if(perms.size() == 0){
		 		
		 		PermissionSet perm = [Select ID from PermissionSet where Name = 'Org_Configuration'];
		 	
		 		PermissionSetAssignment userPerm = new PermissionSetAssignment();
		 		userPerm.AssigneeId = testUser.Id;
		 		userPerm.PermissionSetId = perm.Id;
		 		insert userPerm;
			}
		}
	 	
	 	Admin_Request__c adminReq = new Admin_Request__c();
		adminReq.User__c = testUser.Id;
		adminReq.Environment__c = 'Production';
		adminReq.Permissions__c = 'Org Configuration;Other (specify below)';
		adminReq.Permissions_details__c = 'Test';
		adminReq.Start_Date__c = Date.today().addDays(-2);
		adminReq.End_Date__c = Date.today();
		adminReq.Status__c = 'Approved';
		adminReq.Reason__c = 'Test';
		insert adminReq;
	 		 			 	
		Test.startTest();
				
		Database.executeBatch(new ba_Admin_Request_Termination('system@medtronic.com', 'fakePassword'), 1);  
			
		Test.stopTest();
			
		List<PermissionSetAssignment> assignments = [Select Id from PermissionSetAssignment where AssigneeId = :testUser.Id AND PermissionSet.Name = 'Org_Configuration'];
	    System.assert(assignments.size() == 0);
			
		adminReq = [Select Admin_Request_Revoked__c from Admin_Request__c where Id = :adminReq.Id];		
		System.assert(adminReq.Admin_Request_Revoked__c == false);
	}
	
	private static testmethod void testBatchAdminRequestTermination_Sandbox(){
													
		User testUser = [Select Id from User where isActive = true AND Profile.Name = 'IT Business Analyst' AND Id != :UserInfo.getUserId() LIMIT 1];
		
		User currentUser = new User(Id = UserInfo.getUserId());
		
		System.runAs(currentUser){
			
			List<PermissionSetAssignment> perms = [Select Id from PermissionSetAssignment where AssigneeId = :testUser.Id AND PermissionSet.Name = 'Org_Configuration'];
			
			if(perms.size() == 0){
		 		
		 		PermissionSet perm = [Select ID from PermissionSet where Name = 'Org_Configuration'];
		 	
		 		PermissionSetAssignment userPerm = new PermissionSetAssignment();
		 		userPerm.AssigneeId = testUser.Id;
		 		userPerm.PermissionSetId = perm.Id;
		 		insert userPerm;
			}
		}
	 	
	 	Admin_Request__c adminReq = new Admin_Request__c();
	 	adminReq.User__c = testUser.Id;
	 	adminReq.Environment__c = 'Devcore3';
		adminReq.Permissions__c = 'Org Configuration;Patient Data Access';
		adminReq.Start_Date__c = Date.today().addDays(-2);
		adminReq.End_Date__c = Date.today();
		adminReq.Status__c = 'Approved';
		adminReq.Reason__c = 'Test';	 		
 		
		insert adminReq;
		
		Test.startTest();
		
		CalloutMock mock = new CalloutMock();		
		Test.setMock(HttpCalloutMock.class, mock);
			
		ba_Admin_Request_Termination batch = new ba_Admin_Request_Termination('system@medtronic.com', 'fakePassword');
		batch.calloutMock = mock;	
		
		Database.executeBatch(batch, 1);  
			
		Test.stopTest();
			
		// The local Permissions should not be affected
		List<PermissionSetAssignment> assignments = [Select Id from PermissionSetAssignment where AssigneeId = :testUser.Id AND PermissionSet.Name = 'Org_Configuration'];
	    System.assert(assignments.size() == 1);
		
		adminReq = [Select Admin_Request_Revoked__c from Admin_Request__c where Id = :adminReq.Id];		
		System.assert(adminReq.Admin_Request_Revoked__c == true);	
	}
	
	private static testmethod void testBatchAdminRequestTermination_Sandbox_LoginError(){
													
		User testUser = [Select Id from User where isActive = true AND Profile.Name = 'IT Business Analyst' AND Id != :UserInfo.getUserId() LIMIT 1];
		 	
	 	Admin_Request__c adminReq = new Admin_Request__c();
	 	adminReq.User__c = testUser.Id;
	 	adminReq.Environment__c = 'Devcore3';
		adminReq.Permissions__c = 'Org Configuration;Patient Data Access';
		adminReq.Start_Date__c = Date.today().addDays(-2);
		adminReq.End_Date__c = Date.today();
		adminReq.Status__c = 'Approved';
		adminReq.Reason__c = 'Test';	 		
 		
		insert adminReq;
		
		Test.startTest();
		
		CalloutMock mock = new CalloutMock();	
		mock.forceErrorLogin = true;	
		Test.setMock(HttpCalloutMock.class, mock);
			
		ba_Admin_Request_Termination batch = new ba_Admin_Request_Termination('system@medtronic.com', 'fakePassword');
		batch.calloutMock = mock;	
		
		Database.executeBatch(batch, 1);  
			
		Test.stopTest();
		
		// The Request should still be open as it failed to execute				
		adminReq = [Select Admin_Request_Revoked__c from Admin_Request__c where Id = :adminReq.Id];		
		System.assert(adminReq.Admin_Request_Revoked__c == false);	
	}
	
	private static testmethod void testBatchAdminRequestTermination_Sandbox_ServiceError(){
													
		User testUser = [Select Id from User where isActive = true AND Profile.Name = 'IT Business Analyst' AND Id != :UserInfo.getUserId() LIMIT 1];
		 	
	 	Admin_Request__c adminReq = new Admin_Request__c();
	 	adminReq.User__c = testUser.Id;
	 	adminReq.Environment__c = 'Devcore3';
		adminReq.Permissions__c = 'Org Configuration;Patient Data Access';
		adminReq.Start_Date__c = Date.today().addDays(-2);
		adminReq.End_Date__c = Date.today();
		adminReq.Status__c = 'Approved';
		adminReq.Reason__c = 'Test';	 		
 		
		insert adminReq;
		
		Test.startTest();
		
		CalloutMock mock = new CalloutMock();	
		mock.forceErrorService = true;	
		Test.setMock(HttpCalloutMock.class, mock);
			
		ba_Admin_Request_Termination batch = new ba_Admin_Request_Termination('system@medtronic.com', 'fakePassword');
		batch.calloutMock = mock;	
		
		Database.executeBatch(batch, 1);  
			
		Test.stopTest();
			
		// The Request should still be open as it failed to execute	
		adminReq = [Select Admin_Request_Revoked__c from Admin_Request__c where Id = :adminReq.Id];		
		System.assert(adminReq.Admin_Request_Revoked__c == false);	
	}
	
	public class CalloutMock implements HttpCalloutMock {
		
		public Boolean forceErrorLogin {get; set;}
		public Boolean forceErrorService {get; set;}
		
        public HTTPResponse respond(HTTPRequest req) {
            
            HttpResponse resp = new HttpResponse();
                          
            if(req.getEndpoint() == 'https://test.salesforce.com/services/Soap/c/35.0/'){
            	
            	if(forceErrorLogin == true){
            		
            		resp.setStatusCode(400);
            		resp.setStatus('Error');
            		
            	}else{
            		
            		resp.setStatusCode(200);
            		resp.setStatus('Complete');
            		
            		String respBody =  '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns="urn:enterprise.soap.sforce.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' +
										   '<soapenv:Body>' +
										      '<loginResponse>' +
										         '<result>' +
										            '<serverUrl>https://salesforce.com/Soap/</serverUrl>' +
										            '<sessionId>fakesessionId</sessionId>' +
										         '</result>' +
										      '</loginResponse>' +
										   '</soapenv:Body>' +
										'</soapenv:Envelope>';
            	
            		resp.setBody(respBody);
            	}
				
            }else{
            	
            	resp.setStatusCode(200);
            	resp.setStatus('Complete');
            	
            	String respBody;
            	
            	if(forceErrorService == true){
            	           		
            		respBody =  '{' +            	
    								'"isSuccess" : false,' +
    								'"errorMessage" : "Fake error message."' +
    							'}';
    	            	            		       
            	}else{
            		
            		respBody =  '{' +            	
    								'"isSuccess" : true,' +
    								'"errorMessage" : ""' +
    							'}';            	
            	}
            	
            	resp.setBody(respBody);
            }            
            
            return resp;
        }
	}	    
}