public with sharing class ctrlExt_ImplantSchedulingTeam_New {

	public ctrlExt_ImplantSchedulingTeam_New(ApexPages.standardController sc){
		
	}
	/*
	private Implant_Scheduling_Team__c team;
	
	public Boolean usesExtendedTeam {get; set;}
	public Boolean isCoreTeam {get; set;}

	public ctrlExt_ImplantSchedulingTeam_New(ApexPages.standardController sc){
		
		team = (Implant_Scheduling_Team__c) sc.getRecord();
		
		usesExtendedTeam = false;
		isCoreTeam = false;
		
		if(team.Country__c != null){
			
			//We are in the Clone mode
			team.Id = null;
			
			loadCountryInfo();
			
			if(usesExtendedTeam == true){
				
				RecordType coreTeamRT = [Select Id from RecordType where SObjectType = 'Implant_Scheduling_Team__c' AND DeveloperName = 'Core_Team'];
				
				if(team.RecordTypeId == coreTeamRT.Id){
					isCoreTeam = true;
				}else{
					isCoreTeam = false;
				}
			}
		}
		
		//We are creating an Extended Team from the Core Team page
		if(team.Id == null && team.Core_Team__c != null){
			
			Implant_Scheduling_Team__c coreTeam = [Select Country__c, Business_Unit__c, Sub_Business_Unit__c, (Select Extended_Team_Level__c from Extended_Teams__r ORDER BY Extended_Team_Level__c DESC LIMIT 1) from Implant_Scheduling_Team__c where Id = :team.Core_Team__c];
			
			team.Business_Unit__c = coreTeam.Business_Unit__c;
			team.Sub_Business_Unit__c = coreTeam.Sub_Business_Unit__c;
			team.Country__c = coreTeam.Country__c;
			
			if(coreTeam.Extended_Teams__r.size()>0){
				
				team.Extended_Team_Level__c = coreTeam.Extended_Teams__r[0].Extended_Team_Level__c + 1;
				
			}else{
				
				team.Extended_Team_Level__c = 1;
			}
			
			usesExtendedTeam = true;
			isCoreTeam = false;
		}
	}
	
	public void countrySelected(){
						
		usesExtendedTeam = false;
				
		isCoreTeam = false;
		team.Core_Team__c = null;
		
		if(team.Country__c != null){
			
			loadCountryInfo();
			
			if(usesExtendedTeam == true) isCoreTeam = true;
		}
		
		if(team.Display_Case_fields__c == null){		
			team.Display_Case_fields__c = 'Subject;AccountId;ContactId;Start_of_Procedure__c;End_of_Procedure__c;Procedure_Duration_Implants__c';
		}
	}
	
	private void loadCountryInfo(){
		
		DIB_Country__c country = [Select Id, Country_ISO_Code__c from DIB_Country__c where Id = :team.Country__c];
		
		Implant_Scheduling_Team_Model__c countrySettings = Implant_Scheduling_Team_Model__c.getInstance(country.Country_ISO_Code__c);			
		if(countrySettings == null) countrySettings = Implant_Scheduling_Team_Model__c.getInstance('Default');
		
		usesExtendedTeam = countrySettings.Uses_Extended_Team__c;
	}
	
	public PageReference save(){
		
		Boolean isError = false;
		
		if(team.Name == null || team.Name == ''){
			
			isError = true;
			team.Name.addError('This field is mandatory');
		}
				
		if( (team.Business_Unit__c != null && team.Sub_Business_Unit__c != null) || (team.Business_Unit__c == null && team.Sub_Business_Unit__c == null)){
			
			isError = true;
			team.addError('Either a Business Unit or Sub Business Unit must be selected');
		}
		
		if(team.Country__c == null){
			
			isError = true;
			team.Country__c.addError('Country is mandatory');
		}
		
		if(usesExtendedTeam == false || (usesExtendedTeam == true && isCoreTeam == true)){
			
			if(team.Display_Case_fields__c == null || team.Display_Case_fields__c == ''){
				isError = true;
				team.Display_Case_fields__c.addError('This field is mandatory');
			}			
		}else{
			team.Display_Case_fields__c = null;
		}
		
		if(usesExtendedTeam == true && isCoreTeam == false){
			
			if(team.Core_Team__c == null){
				isError = true;
				team.Core_Team__c.addError('This field is mandatory');
			}
			
			if(team.Extended_Team_Level__c == null){
				isError = true;
				team.Extended_Team_Level__c.addError('This field is mandatory');
			}
		}
		
		if(isError)	return null;
		
		RecordType teamRT;
		
		if(usesExtendedTeam == false){
			
			//Set Record type for Territory Team
			teamRT = [Select Id from RecordType where SObjectType = 'Implant_Scheduling_Team__c' AND DeveloperName = 'Territory_Team'];		
			
		}else{
			
			if(isCoreTeam == true){
				
				//Set Record type for Core Team
				teamRT = [Select Id from RecordType where SObjectType = 'Implant_Scheduling_Team__c' AND DeveloperName = 'Core_Team'];	
				
			}else{
								
				//Set Record type for Extended Team
				teamRT = [Select Id from RecordType where SObjectType = 'Implant_Scheduling_Team__c' AND DeveloperName = 'Extended_Team'];	
			}			
		}
		
		team.RecordTypeId = teamRT.Id;
		
		try{
		
			insert team;
			
		}catch(Exception e){
			
			ApexPages.addMessages(e);
			return null;
		}
		
		PageReference pr = new PageReference('/'+team.Id);
		pr.setRedirect(true);
		
		return pr;
	}
	*/
}