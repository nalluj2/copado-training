public class bl_SupportRequest {

    public static Map<String, List<Support_Request_Parameter__c>> mapSupportRequestParameterType_SupportRequestParameter = new Map<String, List<Support_Request_Parameter__c>>();

    public static Create_User_Request__c loadSupportRequestDetailData(id idSupportRequest){
        Create_User_Request__c oSupportRequest;

        String tWhere = 'Id =\'' + String.escapeSingleQuotes(idSupportRequest) + '\'';
        List<Create_User_Request__c> lstSupportRequest = loadSupportRequestDetailData(tWhere);
        if ( (lstSupportRequest != null) && (lstSupportRequest.size() == 1) ){
            oSupportRequest = lstSupportRequest[0];
        }

        return oSupportRequest;
    }

    public static List<Create_User_Request__c> loadSupportRequestDetailData(String tWhere){
        String tSOQL =  'SELECT ';
            tSOQL += 'Id, Name, FirstName__c, LastName__c, Email__c,Peoplesoft_ID__c,Manager__c, Manager_Email__c, Status__c';
            tSOQL += ', Cost_Center__c, Region__c, Region__r.Name, Country__c,Country__r.Name, Comment__c, License_Type__c';
            tSOQL += ', District_Manager__c,District_Manager__r.Name, Same_as_user__c,Same_as_user__r.Name, Alias__c, Request_Type__c';
            tSOQL += ', created_User__c, Created_User__r.Name,profile__c, Date_of_Deactivation__c, Deactivation_Process_Id__c, Deactivation_Reason__c, Mobile_App__c, Mobile_Apps__c';
            tSOQL += ', Requestor_Email__c,Requestor_Name__c,Description_Long__c, Data_Type__c, Service_Request_Type__c, CR_Type__c, Business_Rationale__c';
            tSOQL += ', Priority__c,Data_Load_Approvers__c, User_SAP_Id__c,Data_Load_Result__c, Assignee__c, Business_Unit__c, PO_Number__c';
            tSOQL += ', Application__c, Sub_Application__c, BI_Content_Group__c, BI_Content_Group__r.Value__c, BI_Capability_Group__c, BI_Capability_Group__r.Value__c ';
            tSOQL += ', Alias_same_as_user__c, Firstname_same_as_user__c, Lastname_same_as_user__c, Email_same_as_user__c';
            tSOQL += ', Federation_ID__c, Business_Unit_Group_Cvent__c, Business_Unit_Cvent__c, Event_Name__c';
            tSOQL += ', Area_CR__c, Area_SR__c, Report_Type__c, Change_Type__c';
            tSOQL += ', Geographical_Region_vs__c, Geographical_SubRegion__c, Geographical_Country_vs__c, User_Business_Unit_vs__c, Company_Code__c, Primary_sBU__c, Jobtitle_vs__c ';
            tSOQL += ', BOT_Countries__c, BOT_File_Type__c, BOT_Major_Classes__c, BOT_MPG_Codes__c, BOT_User_Role__c';
            tSOQL += ', (Select Id, Name, CreatedDate from Attachments ORDER BY CreatedDate ASC)'; 
        tSOQL += ' FROM Create_User_Request__c';
        if (clsUtil.isNull(tWhere, '') != ''){
            tSOQL += ' WHERE ' + tWhere;
        }

        List<Create_User_Request__c> lstSupportRequest = Database.query(tSOQL);

        return lstSupportRequest;
    }

    public static Map<String, List<Support_Request_Parameter__c>> loadSupportRequestParameter(){
        return loadSupportRequestParameter('');
    }
    public static Map<String, List<Support_Request_Parameter__c>> loadSupportRequestParameter(String tType){

        if (mapSupportRequestParameterType_SupportRequestParameter != null && mapSupportRequestParameterType_SupportRequestParameter.size() > 0){
            if (tType != ''){
                if (mapSupportRequestParameterType_SupportRequestParameter.containsKey(tType)){
                    return mapSupportRequestParameterType_SupportRequestParameter;
                } 
            }
        }
        mapSupportRequestParameterType_SupportRequestParameter = new Map<String, List<Support_Request_Parameter__c>>();

        String tSOQL = 'SELECT Id, Name, Key__c, Sequence__c, Active__c, Type__c, Value__c';
        tSOQL += ' FROM Support_Request_Parameter__c';
        tSOQL += ' WHERE Active__c = true';
        if (tType != ''){
            tSOQL += ' AND Type__c = \'' + String.escapeSingleQuotes(tType) + '\'';
        }
        tSOQL += ' ORDER BY Sequence__c';

        List<Support_Request_Parameter__c> lstSupportRequestParameter = Database.query(tSOQL);

        for (Support_Request_Parameter__c oSRP : lstSupportRequestParameter){
            List<Support_Request_Parameter__c> lstSRP = new List<Support_Request_Parameter__c>();
            if (mapSupportRequestParameterType_SupportRequestParameter.containsKey(oSRP.Type__c)){
                lstSRP = mapSupportRequestParameterType_SupportRequestParameter.get(oSRP.Type__c);
            }
            lstSRP.add(oSRP);
            mapSupportRequestParameterType_SupportRequestParameter.put(oSRP.Type__c, lstSRP);
        }
        return mapSupportRequestParameterType_SupportRequestParameter;
    }


    public static ws_adUserService.userinfo searchByAlias(String tSearchAlias){
        
        ws_adUserService.userinfo oUserInfo;

        Pattern alphaNumeric = Pattern.compile('^[A-Za-z0-9]+$');
        Matcher matcher = alphaNumeric.matcher(tSearchAlias);
        
        if(!matcher.matches()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Only alphanumeric characters are allowed'));
            return null;
        }
        
        ws_adUserService.Service1Soap userService = new ws_adUserService.Service1Soap();
        userService.timeout_x=120000;
        
        oUserInfo = userService.getUserInfo(tSearchAlias);          
            
        if(oUserInfo.firstName==null && oUserInfo.lastName==null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'User not found'));
            return null;
        }
        
        return oUserInfo;
    }

    public static List<Create_User_Request__c> loadSupportRequestOverviewData(String tWhere){
        String tSOQL =  'SELECT ';
            tSOQL += 'Id, Name, Status__c, Request_Type__c, CreatedDate, Completed_Date__c, Date_of_deactivation__c';
            tSOQL += ', Created_User__r.Name, CR_Type__c, Priority__c, Application__c';
            tSOQL += ', Service_Request_Type__c, Data_Type__c, Alias__c, Firstname__c, Lastname__c, Mobile_App__c';
            tSOQL += ', Area_CR__c, Area_SR__c';
        tSOQL += ' FROM Create_User_Request__c';
        if (clsUtil.isNull(tWhere, '') != ''){
            tSOQL += ' WHERE ' + tWhere;
        }

        List<Create_User_Request__c> lstSupportRequest = Database.query(tSOQL);

        return lstSupportRequest;
    }

}