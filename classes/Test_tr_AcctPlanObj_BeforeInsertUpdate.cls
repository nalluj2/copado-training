@isTest
private class Test_tr_AcctPlanObj_BeforeInsertUpdate {
	
	@isTest static void test_tr_AccountPlanObjective_BeforeInsertUpdate() {

		clsTestData_MasterData.createSubBusinessUnit();

		clsTestData_Account.createAccount();

        List<Business_Objective__c> lstBusinessObjective = new List<Business_Objective__c>();
        Business_Objective__c oBusinessObjective_SBU_Quantitive = new Business_Objective__c();
            oBusinessObjective_SBU_Quantitive.Name = 'SBU Objective Quantitative';
            oBusinessObjective_SBU_Quantitive.Explanation__c = 'Explanation Quantitative';
            oBusinessObjective_SBU_Quantitive.Type__c = 'Quantitative';
            oBusinessObjective_SBU_Quantitive.Active__c = true;
            oBusinessObjective_SBU_Quantitive.Sub_Business_Unit__c = clsTestData_MasterData.lstSubBusinessUnit[0].id;
        lstBusinessObjective.add(oBusinessObjective_SBU_Quantitive);
        Business_Objective__c oBusinessObjective_SBU_Qualitive = new Business_Objective__c();
            oBusinessObjective_SBU_Qualitive.Name = 'SBU Objective Quantitative';
            oBusinessObjective_SBU_Qualitive.Explanation__c = 'Explanation Quantitative';
            oBusinessObjective_SBU_Qualitive.Type__c = 'Qualitative';
            oBusinessObjective_SBU_Qualitive.Active__c = true;
            oBusinessObjective_SBU_Qualitive.Sub_Business_Unit__c = clsTestData_MasterData.lstSubBusinessUnit[0].id;
        lstBusinessObjective.add(oBusinessObjective_SBU_Qualitive);
        insert lstBusinessObjective;

        Account_Plan_2__c oAccountPlan = new Account_Plan_2__c();
            oAccountPlan.RecordTypeId = clsUtil.getRecordTypeByDevName('Account_Plan_2__c', 'EUR_RTG_BUG').Id;
            oAccountPlan.Account__c = clsTestData_Account.lstAccount[0].id;
            oAccountPlan.Business_Unit_Group__c = clsTestData_MasterData.lstBusinessUnitGroup[0].Id;
            oAccountPlan.Business_Unit__c = clsTestData_MasterData.lstBusinessUnit[0].Id;
            oAccountPlan.Sub_Business_Unit__c = clsTestData_MasterData.lstSubBusinessUnit[0].Id;
            oAccountPlan.Country__c = 'Belgium';
            oAccountPlan.Opportunities__c = 'Other';
            oAccountPlan.Account_Plan_Level__c = 'Sub Business Unit';
        insert oAccountPlan;

        Test.startTest();

        List<Account_Plan_Objective__c> lstAccountPlanObjective = new List<Account_Plan_Objective__c>();

        Account_Plan_Objective__c oAccountPlanObjective_Quantitive = new Account_Plan_Objective__c(Account_Plan__c = oAccountPlan.Id, Business_Objective__c = oBusinessObjective_SBU_Quantitive.Id, Description__c  = 'Test Objective RTG', Status__c = 'Planned', Target_Date__c = System.today() + 7);
        lstAccountPlanObjective.add(oAccountPlanObjective_Quantitive);

        Account_Plan_Objective__c oAccountPlanObjective_Qualitive = new Account_Plan_Objective__c(Account_Plan__c = oAccountPlan.Id, Business_Objective__c = oBusinessObjective_SBU_Qualitive.Id, Description__c = 'Test Objective RTG', Status__c = 'Planned', Target_Date__c = System.today() + 7);
        lstAccountPlanObjective.add(oAccountPlanObjective_Qualitive);

        Account_Plan_Objective__c oAccountPlanObjective = new Account_Plan_Objective__c(Account_Plan__c = oAccountPlan.Id, Description__c = 'Test Objective RTG', Status__c = 'Planned', Target_Date__c = System.today() + 7);
        lstAccountPlanObjective.add(oAccountPlanObjective);

        insert lstAccountPlanObjective;

        Test.stopTest();
	}
	
	
}