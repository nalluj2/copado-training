//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 07-11-2017
//  Description      : APEX TEST Class for the APEX Controller Class ctrl_ExportData
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_ctrl_ExportData {
		
	@isTest static void test_ExportData_OpportunityBusinessCriticalTender() {
		
		//-------------------------------------------------------
		// Create Test Data
		//-------------------------------------------------------
		clsTestData_MasterData.createSubBusinessUnit();


		clsTestData_Account.iRecord_Account = 2;
		List<Account> lstAccount = clsTestData_Account.createAccount(false);
			lstAccount[0].SAP_Sales_Org__c = 'ABC1';
			lstAccount[0].SAP_Sales_Office__c = 'ABC2';
			lstAccount[1].SAP_Sales_Org__c = 'ABC3';
			lstAccount[1].SAP_Sales_Office__c = 'ABC4';
		insert lstAccount;

		Account oAccount = lstAccount[0];
		Account oAccount_LOT = lstAccount[1];

		clsTestData_Opportunity.iRecord_Opportunity = 1;
		clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'Business_Critical_Tender').Id;
		Opportunity oOpportunityTender = clsTestData_Opportunity.createOpportunity(false)[0];
	        oOpportunityTender.CloseDate = Date.today().addDays(30);
	        oOpportunityTender.StageName = 'Prospecting/Lead';
			oOpportunityTender.Contract_Number__c = '123456789';
			oOpportunityTender.Current_Extended_To_Date__c = Date.today().addYears(1);
			oOpportunityTender.Alt_Payment_Terms_2__c = 'M006 - Due 120 Days after End of Month';
			oOpportunityTender.IHS_Tender_Number__c = 'IHS TENDER NUMNER';
		insert oOpportunityTender;
    	
		Opportunity_Lot__c oOpportunityLot = new Opportunity_Lot__c();
			oOpportunityLot.Opportunity__c = oOpportunityTender.Id;
			oOpportunityLot.Sub_Business_Unit__c = clsTestData_MasterData.oMain_SubBusinessUnit.Id;
			oOpportunityLot.Account__c = oAccount_LOT.Id;
			oOpportunityLot.CurrencyISOCode = 'EUR';
			oOpportunityLot.Name = 'Unit test opp lot';
			oOpportunityLot.Status__c = 'Won';
			oOpportunityLot.Award_Type__c = 'Multi Award';
			oOpportunityLot.Lot_Owner__c = UserInfo.getUserId();
			oOpportunityLot.Price_Weight__c = 25;
			oOpportunityLot.Quality_Weight__c = 25;
			oOpportunityLot.Other_Weight__c = 50;
		insert oOpportunityLot;
		//-------------------------------------------------------


		//-------------------------------------------------------
		// Test Logic
		//-------------------------------------------------------
		Test.startTest();
		

		// No input param
		Pagereference oPageRef1 = Page.ExportData;
		Test.setCurrentPageReference(oPageRef1);

		ctrl_ExportData oCtrlExportData1 = new ctrl_ExportData();
		System.assert(String.isBlank(oCtrlExportData1.tDataExport));

	
		// Input param - Id
		Pagereference oPageRef2 = Page.ExportData;
		Test.setCurrentPageReference(oPageRef2);
		oPageRef2.getParameters().put('id', oOpportunityTender.Id);

		ctrl_ExportData oCtrlExportData2 = new ctrl_ExportData();
		System.assert(String.isBlank(oCtrlExportData2.tDataExport));


		// Input param - exportType
		Pagereference oPageRef3 = Page.ExportData;
		Test.setCurrentPageReference(oPageRef3);
		oPageRef3.getParameters().put('exportType', clsUtil.getRecordTypeByDevName('Opportunity', 'Business_Critical_Tender').Id);

		ctrl_ExportData oCtrlExportData3 = new ctrl_ExportData();
		System.assert(String.isBlank(oCtrlExportData3.tDataExport));


		// Input param - id & exportType
		Pagereference oPageRef4 = Page.ExportData;
		Test.setCurrentPageReference(oPageRef4);
		oPageRef4.getParameters().put('id', oOpportunityTender.Id);
		oPageRef4.getParameters().put('exportType', clsUtil.getRecordTypeByDevName('Opportunity', 'Business_Critical_Tender').Id);

		ctrl_ExportData oCtrlExportData4 = new ctrl_ExportData();
		System.assert(!String.isBlank(oCtrlExportData4.tDataExport));


		// Error Handling 
		Pagereference oPageRef5 = Page.ExportData;
		Test.setCurrentPageReference(oPageRef5);
		oPageRef5.getParameters().put('id', oOpportunityTender.Id);
		oPageRef5.getParameters().put('exportType', clsUtil.getRecordTypeByDevName('Opportunity', 'Business_Critical_Tender').Id);

		clsUtil.tExceptionName = 'ctrl_ExportData.createDataExport_OpportunityBusinessCriticalTender';
  		ctrl_ExportData oCtrlExportData5 = new ctrl_ExportData();
		System.assert(!String.isBlank(oCtrlExportData5.tDataExport));
		System.debug('**BC** Error : ' + oCtrlExportData5.tDataExport);
		System.assert(oCtrlExportData5.tDataExport.contains(clsUtil.tExceptionName));


		Test.stopTest();
		//-------------------------------------------------------


		//-------------------------------------------------------
		// Verify Result
		//-------------------------------------------------------
		//-------------------------------------------------------
	}

	@isTest static void test_ExportData_OpportunityDealVIDNonTender() {
		
		//-------------------------------------------------------
		// Create Test Data
		//-------------------------------------------------------
		clsTestData_MasterData.createSubBusinessUnit();

		clsTestData_Opportunity.iRecord_Opportunity = 1;
		clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'Deal_VID_Non_tender').Id;
		Opportunity oOpportunity = clsTestData_Opportunity.createOpportunity(false)[0];
	        oOpportunity.CloseDate = Date.today().addDays(30);
	        oOpportunity.StageName = 'Prospecting/Lead';
			oOpportunity.Contract_Number__c = '123456789';
			oOpportunity.Current_Extended_To_Date__c = Date.today().addYears(1);
			oOpportunity.Alt_Payment_Terms_2__c = 'M006 - Due 120 Days after End of Month';
		insert oOpportunity;
		//-------------------------------------------------------


		//-------------------------------------------------------
		// Test Logic
		//-------------------------------------------------------
		Test.startTest();
		

		// No input param
		Pagereference oPageRef1 = Page.ExportData;
		Test.setCurrentPageReference(oPageRef1);

		ctrl_ExportData oCtrlExportData1 = new ctrl_ExportData();
		System.assert(String.isBlank(oCtrlExportData1.tDataExport));

	
		// Input param - Id
		Pagereference oPageRef2 = Page.ExportData;
		Test.setCurrentPageReference(oPageRef2);
		oPageRef2.getParameters().put('id', oOpportunity.Id);

		ctrl_ExportData oCtrlExportData2 = new ctrl_ExportData();
		System.assert(String.isBlank(oCtrlExportData2.tDataExport));


		// Input param - exportType
		Pagereference oPageRef3 = Page.ExportData;
		Test.setCurrentPageReference(oPageRef3);
		oPageRef3.getParameters().put('exportType', clsUtil.getRecordTypeByDevName('Opportunity', 'Deal_VID_Non_tender').Id);

		ctrl_ExportData oCtrlExportData3 = new ctrl_ExportData();
		System.assert(String.isBlank(oCtrlExportData3.tDataExport));


		// Input param - id & exportType
		Pagereference oPageRef4 = Page.ExportData;
		Test.setCurrentPageReference(oPageRef4);
		oPageRef4.getParameters().put('id', oOpportunity.Id);
		oPageRef4.getParameters().put('exportType', clsUtil.getRecordTypeByDevName('Opportunity', 'Deal_VID_Non_tender').Id);

		ctrl_ExportData oCtrlExportData4 = new ctrl_ExportData();
		System.assert(!String.isBlank(oCtrlExportData4.tDataExport));


		// Error Handling 
		Pagereference oPageRef5 = Page.ExportData;
		Test.setCurrentPageReference(oPageRef5);
		oPageRef5.getParameters().put('id', oOpportunity.Id);
		oPageRef5.getParameters().put('exportType', clsUtil.getRecordTypeByDevName('Opportunity', 'Deal_VID_Non_tender').Id);

		clsUtil.tExceptionName = 'ctrl_ExportData.createDataExport_OpportunityDealVIDNonTender';
  		ctrl_ExportData oCtrlExportData5 = new ctrl_ExportData();
		System.assert(!String.isBlank(oCtrlExportData5.tDataExport));
		System.debug('**BC** Error : ' + oCtrlExportData5.tDataExport);
		System.assert(oCtrlExportData5.tDataExport.contains(clsUtil.tExceptionName));


		Test.stopTest();
		//-------------------------------------------------------


		//-------------------------------------------------------
		// Verify Result
		//-------------------------------------------------------
		//-------------------------------------------------------
	}
	
}
//--------------------------------------------------------------------------------------------------------------------