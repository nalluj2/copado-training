/*
* Description      : Class to implement rest interface for events
* Author           : Rudy De Coninck
* Created Date     : 24-12-2013
*/
@RestResource(urlMapping='/EventService/*')
global class ws_Event {


	@HttpPost
  	global static IFScheduledImplantEventResponse updateEvent() {
  		
  		RestRequest req = RestContext.request;
  		String body = req.requestBody.toString();
  		IFScheduledImplantEventStatusRequest request = (IFScheduledImplantEventStatusRequest)System.Json.deserialize(body, IFScheduledImplantEventStatusRequest.class);
  		IFScheduledImplantEventResponse response = new IFScheduledImplantEventResponse();
  		
  		try{
	  		Event updateEvent = new Event(id=request.id);
	  		String message=request.message;
	  		Outbound_Message__c outboundMessage = new Outbound_Message__c();
	  		if (message!=null && message== 'OK'){
	  			updateEvent.Distribution_Status__c = 'Success';
	  			outboundMessage.status__c ='TRUE';
	  		}else{
	  			updateEvent.Distribution_Status__c = 'Error';
	  			outboundMessage.status__c='FALSE';
	  		}

	  		updateEvent.Distribution_Error_Details__c = request.message;

	  		
	  		outboundMessage.request__c = body;
	  		outboundMessage.Object__c = 'Event';
	  		outboundMessage.User__c = UserInfo.getUserId();
	  		outboundMessage.internal_id__c = updateEvent.id;
	  		outboundMessage.operation__c = 'Status Update';
	  		insert outboundMessage;

	  		update updateEvent;
	  		response.success=true;
	  		response.Message=request.message;
  		}catch(Exception e){
  			response.success=false;
  			response.Message=e.getMessage();
  		}
  		
  		
  		System.debug('Error for Id '+request.id+ ' message: '+request.message);
  		
  		return response;
  		
  	}	

	@HttpGet 
    global static IFScheduledImplantEventResponse GET()
    {
        IFScheduledImplantEventResponse response = new IFScheduledImplantEventResponse();
       
       System.debug('Get request '+RestContext.request);
       
       //Read the Request from the URL
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String requestId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1); 
        
        if(requestId!=null)
        {
            response.success = false;
            response.implantScheduleEvent = getScheduledImplantEvent(requestId);
            response.Message = 'Success : Found Event';
        }
        
        else
        {
            response.success = false;
            response.Message = 'Fail : Event Not Found';
            
        }
        
        System.debug('Response '+response);
        
        return response;
    }
	
	/*
	 * Description      : Method to query for Implant Schedule event details given an Id
	 * Author           : Rudy De Coninck
	 * Created Date     : 24-12-2013
	 */
	public static IFScheduledImplantEvent getScheduledImplantEvent(Id id){
		
		IFScheduledImplantEvent response = new IFScheduledImplantEvent();
		
		List<Event> eventList = [select Id, WhoId, WhatId, description, Type, subject, StartDateTime, EndDateTime, Location, ActivityDate, 
			CreatedBy.name, CreatedBy.Email , OwnerId
			from Event where Id =:id];
		
		List<Id> whatIds = new List<Id>();
		for (Event e : eventList){
			whatIds.add(e.whatId);
		}
		
		List<Case> caseList = [select Id, Requestor__c, Preferred_Start_of_Procedure__c, Start_of_Procedure__c, 
			Type, Reason, recordType.name, End_of_Procedure__c, ContactId , AccountId, Subject, Procedure__c, Previous_Product_Type__c,
			Manufacturer__c, Replacement_Reason__c 
			from Case where id =:whatIds];
		
		Case c;
		Event e;
		User owner;
		
		
		if (eventList.size()>0){
			e = eventList.get(0);
			owner= [select Id, alias_unique__c, name from user where id =:e.ownerId];
		}
		
		if (caseList.size()>0){
			c =caseList.get(0);
		}
		

		//Event field mapping
		response.creator = e.CreatedBy.name;
		response.creatorEmail = e.CreatedBy.Email;
		response.activityDate = e.ActivityDate;
		response.owner = owner.name;
		response.ownerAlias = owner.alias_unique__c;
		response.ownerId = owner.Id;
		response.description = e.Description;
		response.eventId = e.id;
		response.startTime = e.StartDateTime;
		response.endTime = e.EndDateTime;

		//Case field mapping		
		response.accountId = c.AccountId;
		response.caseId = c.id;
		response.subject = c.Subject;
		response.contactId = c.ContactId;
		response.previousProductType = c.Previous_Product_Type__c;
		response.manufacturerId = c.Manufacturer__c;
		response.procedure = c.Procedure__c;
		response.replacementReason = c.Replacement_Reason__c;
		
		return response;
	}

	global class IFScheduledImplantEventRequest{
   		public Event implantScheduleEvent {get;set;}
	}

	global class IFScheduledImplantEventStatusRequest{
   		public String Id {get;set;}
   		public String Message {get;set;}
	}

	global class IFScheduledImplantEventResponse{
    	public boolean success {get; set;}
    	public String Message {get;set;}
		public IFScheduledImplantEvent implantScheduleEvent {get;set;}
	}
	
	global class IFScheduledImplantEvent{
		public String caseId;
		public String eventId;
		public String creator;
		public String creatorEmail;
		public String accountId;
		public String contactId;
		public Date activityDate;		
		public String subject;
		public String description;
		public String ownerId;
		public String owner;
		public String ownerAlias;
		public DateTime startTime;
		public DateTime endTime;
		public String manufacturerId;
		public String procedure;
		public String previousProductType;
		public String replacementReason;	
		 
	}
	
	
}