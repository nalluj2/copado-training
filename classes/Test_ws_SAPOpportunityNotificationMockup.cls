/*
 * Description		: Webservice Mockup for SAP Opportunity Change Notification Test		 
 * Author        	: Patrick Brinksma
 * Created Date    	: 02-08-2013
 */
@isTest
global class Test_ws_SAPOpportunityNotificationMockup implements WebServiceMock {

	global void doInvoke(
	Object stub,
	Object request,
	Map<String, Object> response,
	String endpoint,
	String soapAction,
	String requestName,
	String responseNS,
	String responseName,
	String responseType) {

		// Generate response message
		ws_ChangeNotificationOpportunity.notifyChangeResponse resObj = new ws_ChangeNotificationOpportunity.notifyChangeResponse();
		resObj.Message = 'Successfully processed';
		resObj.ObjectName = 'Opportunity';
		resObj.Operation = 'Update';
		resObj.SAP_ID = 'SAPID001';
		resObj.SFDC_ID = '006W00000046piA'; 
		resObj.Status = 'true';
		ws_ChangeNotificationOpportunity.processOpportunityChangeNotificationResponse wsResponseObj = new ws_ChangeNotificationOpportunity.processOpportunityChangeNotificationResponse();
		wsResponseObj.notifyChangeResponse = resObj; 
		response.put('response_x', wsResponseObj); 
	}
	
}