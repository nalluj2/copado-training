/*
 *      Description : This is the Test Class for the APEX Trigger trgUpdateBUCaseRecorTypeField (mind the type in the trigger name)
 *      Version     : 1.0
 *      Author      : Bart Caelen
 *      Date        : 201403
*/
@isTest
private class TEST_trgUpdateBUCaseRecordTypeField {
	
    @isTest static void test_Method1(){
        
        //---------------------------------------------
        // Create Test Data
        //---------------------------------------------
        clsTestData.idRecordType_Account    = FinalConstants.sapAccountRecordTypeId;
        clsTestData.tCountry_Account        = 'BELGIUM';
        clsTestData.ptCase_Therapy          = 'testClassTh';
        clsTestData.createCompanyData();
        clsTestData.createBusinessUnitData();
        clsTestData.createSubBusinessUnitData();
        clsTestData.createAccountData();
        clsTestData.createContactData();
        clsTestData.createCaseData();
        //---------------------------------------------

        Test.startTest();

        clsTestData.oMain_SubBusinessUnit.Case_Record_Type_ID__c = clsTestData.oMain_Case.Id;
        update clsTestData.oMain_SubBusinessUnit;

        Therapy_Group__c tgrp=new Therapy_Group__c(Name='testClassTG',Sub_Business_Unit__c=clsTestData.oMain_SubBusinessUnit.Id,Company__c=clsTestData.oMain_Company.Id);
        insert tgrp;
        Therapy__c t=new Therapy__c(Name='testClassTh',Business_Unit__c=clsTestData.oMain_BusinessUnit.Id,Sub_Business_Unit__c=clsTestData.oMain_SubBusinessUnit.Id,Therapy_Group__c=tgrp.id,Active__c=true,Therapy_Name_Hidden__c='testClassTh');
        insert t;

        update clsTestData.oMain_Case;

        Test.stopTest();
    }
	
}