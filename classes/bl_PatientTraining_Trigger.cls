//---------------------------------------------------------------------------------------------------------------------------------
//	Author 			: Bart Caelen
//  Created Date 	: 20/04/2020
//  Description 	: APEX Class - Business Logic for the APEX Trigger tr_PatientTraining
//  Change Log 		: 
//---------------------------------------------------------------------------------------------------------------------------------
public class bl_PatientTraining_Trigger{


	//-----------------------------------------------------------------------------------------------------------------------------
	// Calculate the Compensation based on the provided parameters:
	//	- Training_Category__c
	//	- Training_Type__c
	//	- Training_Method__c
	//	- New_Pump_Additional_Support_Total_Hrs__c
	//	- Auto_Mode_Total_Hrs__c
	//-----------------------------------------------------------------------------------------------------------------------------
	public static void calculateCompensation(List<Patient_Training__c> lstTriggerNew, Map<Id, Patient_Training__c> mapTriggerOld){	


        if (bl_Trigger_Deactivation.isTriggerDeactivated('pt_calculateCompensation')) return;

		Map<String, Decimal> mapTrainingCategory_Type_HourPrice = loadCompensationHourPrice();
		Map<String, Decimal> mapTrainingCategory_Type_Method_Compensation = loadCompensationAmount();

		for (Patient_Training__c oPatientTraining : lstTriggerNew){

			Boolean bProcessRecord = false;

			if (mapTriggerOld == null){

				if (
					( 
						(oPatientTraining.Training_Category__c != null) && (oPatientTraining.Training_Type__c != null) && (oPatientTraining.Training_Method__c != null) 
					) || (
						(oPatientTraining.Training_Type__c == 'New Pump Additional support') && (oPatientTraining.New_Pump_Additional_Support_Total_Hrs__c != null)
					) || (
						(oPatientTraining.Training_Type__c == 'Auto Mode') && (oPatientTraining.Auto_Mode_Total_Hrs__c != null)
					)

				){
				
					bProcessRecord = true;
				
				}

			}else{

				Patient_Training__c oPatientTraining_Old = mapTriggerOld.get(oPatientTraining.Id);

				if (
					(oPatientTraining.Training_Category__c != oPatientTraining_Old.Training_Category__c) || (oPatientTraining.Training_Type__c != oPatientTraining_Old.Training_Type__c) || (oPatientTraining.Training_Method__c != oPatientTraining_Old.Training_Method__c)
					|| (
						(oPatientTraining.Training_Type__c == 'New Pump Additional support') && (oPatientTraining.New_Pump_Additional_Support_Total_Hrs__c != oPatientTraining_Old.New_Pump_Additional_Support_Total_Hrs__c)
					)
					|| (
						(oPatientTraining.Training_Type__c == 'Auto Mode') && (oPatientTraining.Auto_Mode_Total_Hrs__c != oPatientTraining_Old.Auto_Mode_Total_Hrs__c)
					)

				){

					bProcessRecord = true;
				
				}

			}

			if (bProcessRecord){

				if ( (oPatientTraining.Training_Type__c == 'New Pump Additional support') || (oPatientTraining.Training_Type__c == 'Auto Mode') ){

					String tKey = oPatientTraining.Training_Category__c + '|' + oPatientTraining.Training_Type__c;

					if (mapTrainingCategory_Type_HourPrice.containsKey(tKey)){

						Decimal decHourPrice = mapTrainingCategory_Type_HourPrice.get(tKey);
						
						Decimal decTotalHours = 0;

						if (oPatientTraining.Training_Type__c == 'New Pump Additional support'){
							decTotalHours = clsUtil.isDecimalNull(oPatientTraining.New_Pump_Additional_Support_Total_Hrs__c, 0);
						}else if (oPatientTraining.Training_Type__c == 'Auto Mode'){
							decTotalHours = clsUtil.isDecimalNull(oPatientTraining.Auto_Mode_Total_Hrs__c, 0);
						}

						Decimal decCompensation = decTotalHours * decHourPrice;

						if (oPatientTraining.Training_Compensation__c != decCompensation) oPatientTraining.Training_Compensation__c = decCompensation;

					}

				}else{
				
					String tKey = oPatientTraining.Training_Category__c + '|' + oPatientTraining.Training_Type__c + '|' + oPatientTraining.Training_Method__c;

					if (mapTrainingCategory_Type_Method_Compensation.containsKey(tKey)){

						Decimal decCompensation = mapTrainingCategory_Type_Method_Compensation.get(tKey);

						if (oPatientTraining.Training_Compensation__c != decCompensation) oPatientTraining.Training_Compensation__c = decCompensation;

					}
						
				}

			}

		}
	
	}
	//-----------------------------------------------------------------------------------------------------------------------------


	//-----------------------------------------------------------------------------------------------------------------------------
	// Load Setting Data related to Compensation Amount
	//-----------------------------------------------------------------------------------------------------------------------------
	@TestVisible private static Map<String, Decimal> loadCompensationAmount(){

		Map<String, Decimal> mapData = new Map<String, Decimal>();

        List<Patient_Training_Compensation_Calc__mdt> lstPatientTrainingCompensationCalc = 
            [
                SELECT 
					Training_Category__c, Training_Type__c, Training_Method__c, Compensation_Amount__c
                FROM 
                   Patient_Training_Compensation_Calc__mdt
				WHERE
					Compensation_Amount__c != null
					
            ];

		for (Patient_Training_Compensation_Calc__mdt oPatientTrainingCompensationCalc : lstPatientTrainingCompensationCalc){

			String tKey = oPatientTrainingCompensationCalc.Training_Category__c + '|' + oPatientTrainingCompensationCalc.Training_Type__c + '|' + oPatientTrainingCompensationCalc.Training_Method__c;
			mapData.put(tKey,  oPatientTrainingCompensationCalc.Compensation_Amount__c);

		}

		return mapData;

	}
	//-----------------------------------------------------------------------------------------------------------------------------


	//-----------------------------------------------------------------------------------------------------------------------------
	// Load Setting Data related to Compensation Hour Price
	//-----------------------------------------------------------------------------------------------------------------------------
	@TestVisible private static Map<String, Decimal> loadCompensationHourPrice(){

		Map<String, Decimal> mapData = new Map<String, Decimal>();

        List<Patient_Training_Compensation_Calc__mdt> lstPatientTrainingCompensationCalc = 
            [
                SELECT 
					Training_Category__c, Training_Type__c, Compensation_Hour_Price__c
                FROM 
                   Patient_Training_Compensation_Calc__mdt
				WHERE
					Compensation_Hour_Price__c != null
					
            ];

		for (Patient_Training_Compensation_Calc__mdt oPatientTrainingCompensationCalc : lstPatientTrainingCompensationCalc){

			String tKey = oPatientTrainingCompensationCalc.Training_Category__c + '|' + oPatientTrainingCompensationCalc.Training_Type__c;
			mapData.put(tKey,  oPatientTrainingCompensationCalc.Compensation_Hour_Price__c);

		}

		return mapData;

	}
	//-----------------------------------------------------------------------------------------------------------------------------

}
//---------------------------------------------------------------------------------------------------------------------------------