global class ba_create_technician_adhoc_orders implements Schedulable, Database.Batchable<sObject>, Database.Stateful {
	
	public Integer noAdhocs = 10;
	
	global void execute(SchedulableContext ctx)
    {
        Database.executeBatch(new ba_create_technician_adhoc_orders(), 500);
    } 
        
    global database.querylocator start(Database.BatchableContext BC)
    {   	
       	string query = 'SELECT Id, SVMXC__Inventory_Location__c, SVMXC__Salesforce_User__c, SVMXC__Service_Group__c, SVMXC__Service_Group__r.SVMX_Service_Team_Location__c, SVMX_SAP_Employee_Id__c FROM SVMXC__Service_Group_Members__c '+
       				   ' WHERE SVMXC__Active__c = true AND SVMXC__Salesforce_User__c != null AND SVMXC__Salesforce_User__r.isActive = true AND SVMX_SAP_Employee_Id__c != null '; 
                 
  		return Database.getQueryLocator(query);        
    }
    
    global void execute(Database.BatchableContext BC, List<SVMXC__Service_Group_Members__c> technicians)
    {	
    	Set<Id> techSFDCIds = new Set<Id>();    	
    	for(SVMXC__Service_Group_Members__c technician : technicians) techSFDCIds.add(technician.SVMXC__Salesforce_User__c);
    	
    	Map<Id, List<SVMXC__Service_Order__c>> existingAdhocWOs = new Map<Id, List<SVMXC__Service_Order__c>>();
    	
    	for(SVMXC__Service_Order__c adHocWO : [Select Id, OwnerId from SVMXC__Service_Order__c 
    											where OwnerId IN :techSFDCIds AND SVMXC__Order_Status__c = 'On Site' AND SVMXC__Purpose_of_Visit__c = 'Placeholder']){
    		
    		List<SVMXC__Service_Order__c> techAdhocs = existingAdhocWOs.get(adHocWO.OwnerId);
    		
    		if(techAdhocs == null){
    			techAdhocs = new List<SVMXC__Service_Order__c>();
    			existingAdhocWOs.put(adHocWO.OwnerId, techAdhocs);	
    		}
    		
    		techAdhocs.add(adHocWO);
    	}
    	
    	List<Damage_Code_Grouping_Master__c> adhocCode = [Select Id from Damage_Code_Grouping_Master__c where Name = 'ADHOC'];
    	List<Damage_Code_Grouping_Master__c> adhocCauseCode = [Select Id from Damage_Code_Grouping_Master__c where Name = 'CSR00001'];

    	List<SVMXC__Service_Order__c> newAdhocs = new List<SVMXC__Service_Order__c>();

    	for(SVMXC__Service_Group_Members__c technician : technicians){
    		
    		List<SVMXC__Service_Order__c> techAdhocs = existingAdhocWOs.get(technician.SVMXC__Salesforce_User__c);
    		
    		if(techAdhocs == null || techAdhocs.size() < noAdhocs){
    			
    			Integer newAdhocsNo = noAdhocs;
    			if(techAdhocs != null) newAdhocsNo =  noAdhocs - techAdhocs.size();
    			
    			for(Integer i = 0; i < newAdhocsNo; i++){
    				
    				SVMXC__Service_Order__c adhocWO = new SVMXC__Service_Order__c();
    				adhocWO.SVMXC__Order_Status__c = 'On Site';
    				adhocWO.SVMXC__Purpose_of_Visit__c = 'Placeholder';
    				adhocWO.SVMX_Damage_Code_Group_del__c = 'ADHOC';
					adhocWO.SVMX_Cause_Code_Group_del__c = 'CSR00001';
    				if (adhocCode.size() > 0) adhocWO.Damage_Code_Grouping_Lookup__c = adhocCode[0].Id;
    				if (adhocCauseCode.size() > 0) adhocWO.Cause_Code_Grouping_Lookup__c = adhocCauseCode[0].Id;
    				adhocWO.SVMXC__Group_Member__c = technician.Id;
    				adhocWO.SVMXC__Service_Group__c = technician.SVMXC__Service_Group__c;
    				adhocWO.SVMX_FSE_Stock_Location__c = technician.SVMXC__Inventory_Location__c;
    				adhocWO.SVMX_Service_Team_Location__c = technician.SVMXC__Service_Group__r.SVMX_Service_Team_Location__c;
    				adhocWO.SVMX_SAP_Employee_Id__c = technician.SVMX_SAP_Employee_Id__c;
    				adhocWO.OwnerId = technician.SVMXC__Salesforce_User__c;
    				
    				newAdhocs.add(adhocWO);
    			}    			
    		}	
    	}
    	
    	if(newAdhocs.size() > 0) insert newAdhocs;
   	}
    
    global void finish(Database.BatchableContext BC)
    {

    }       
}