/**
 * Creation Date: 	20090226
 * Description: 	Create New Task : Overwrite of the New task page / Edit Task Page and Close Task Page of salesforce; 
 *					The main request is to improve the possibility to add affiliated accounts to the Task !! 
 *					All the Rest should be as default ! 
 * Author: 	ABSI - MC
 */
public class controllerCreateNewTask {	
	/* Arguments */
	// Current record		
	public Task task; 
	ApexPages.StandardController con;
	
	public Boolean isNewTask ; 
	
	//	List of Affiliated Accounts (dependent on the choosed contact ) 
	public List<SelectOption> selFromAccountNames ;
	
	// The user has 2 different choices : Affiliation Account or Normal 
	public List<SelectOption> selOptions ;		
	public Boolean isAffAccDisabled = false ; 
	
	// The result of the choice of the user (Radio button result)
	public String relatedTo ;		
	
	// The first radio option is Affiliation and second is others  
	public final String stOption1 		= 'Affiliated Account';
	public final String stOption2		= 'All';
		
	// Send Notification Email CheckBox : 
	public String sendNotificationEmailCheckBox{ 	get;set;		}
	
	// Reminder :
	public List<SelectOption> selReminderOptions ;
	
	//Contact Phone & Email
	public String contactPhone ; 
	public String contactEmail ;
	
	// String containing the selected option of the Account PickList 
	public String accountsToList {			get;set;				}
	public String reminderList {			get;set;				}
	public Boolean getisAffAccDisabled(){	return this.isAffAccDisabled;  }
	
	//Radio Button Options 
	public String getstOption1(){			return this.stOption1 ;	}
	public String getstOption2(){			return this.stOption2 ;	}
	
	//Contact phone & Email
   	public void setcontactPhone(String s){	this.contactPhone = s ;		}
   	public String getcontactPhone(){		return this.contactPhone;	}
   	public void setcontactEmail(String s){	this.contactEmail = s ;		}
   	public String getcontactEmail(){		return this.contactEmail;	}
   	
	//Reminder Methods 
	public void setremiderList(String s){	this.reminderList = s;	}
	public String getreminderList(){		return this.reminderList;}	
	
	public PageReference listaccounts(){	return null ;			}
	public String getrelatedto() {      	return this.relatedTo;  	 	}   
  	public void setrelatedTo(String rl) { 	this.relatedTo = rl; 	}	
   	public List<SelectOption> getselFromAccountNames() {	return selFromAccountNames;   	}
   	
   	//Used to parse all the url parameters :
   	public Map<String, String> urlDataMap = new Map<String,String>{};
   	
   	/*
   	 * 	main Methods  
   	 */
   	 
   	// Constructor    	
	public controllerCreateNewTask(ApexPages.StandardController stdController) {
        this.task = (Task)stdController.getRecord();
        con = stdController; 
        
        // Parse URl elements to the Data Map : 
        urlDataMap.put('id', ApexPages.currentPage().getParameters().get('id'));
 		urlDataMap.put('whatid', ApexPages.currentPage().getParameters().get('whatid'));     
       	urlDataMap.put('whoid', ApexPages.currentPage().getParameters().get('whoid'));
        urlDataMap.put('close',ApexPages.currentPage().getParameters().get('close'));
        urlDataMap.put('tsk5',ApexPages.currentPage().getParameters().get('tsk5'));
        
        // The New Task & Edit Page are called by this controller. To distinct these pages isNewTask is a boolen ; 
        if (urlDataMap.get('id') == null){
        	isNewTask = true ; 
        	task.status = 'Not Started';				        			
    		task.priority = 'Normal'; 
    		task.ownerid = UserInfo.getUserid();
    		task.IsReminderSet = true; 
			task.Task_Reminder_Date_Hide__c = System.today();
			this.relatedTo = stOption1 ;
        }else{
        	isNewTask = false ;	      	
        }
               
        
		// Set Related To (All) :	
				
    	if ( urlDataMap.get('whatid') != null )
    		task.whatid = urlDataMap.get('whatid') ;
    		
    	//Contact/Lead (Name)
    	if ( urlDataMap.get('whoid') != null ) 
    		task.whoid = urlDataMap.get('whoid') ; 
    		
    	// Close Case (Cls)	 If equals to 1 then it's a close page : 
    	String isClose  = urlDataMap.get('close'); 
    	if (isClose == '1'){
    		task.Status = 'Completed' ;
    	}
    	
    	//Follow-up task : 
    	if (urlDataMap.get('tsk5') != null){
    		task.Subject = urlDataMap.get('tsk5') ;
    	}
    	
    	// All the Affiliated accounts of the actual Contact ! 
    	this.setAffiliatedAccounts(Task.WhoId);
    	
    	// Reminder : Hours:Min !! 
    	this.getselReminderOptions();
    	
    	// Phone & Email related to the selected Contact/Lead
    	this.getContactLeadDetails(task.WhoId);
    	
    	//Check if there is an Affiliated account = Task.whatId : 
    	this.accountsToList = Task.whatId ;
    	if (accountsToList != null){
	    	if (accountsToList.substring(0, 3) == '001'){ // 001 -> Account 
	    		task.whatId = null ; 
    		}
    	}
    	
    	// Reminder (Edit Task)
    	if (isNewTask == false){
		    Task.ReminderDateTime = [Select ReminderDatetime from Task where id =: Task.Id limit 1].ReminderDateTime;
	    	if (Task.ReminderDateTime == null){
	    		// if date is empty, fill in today's date : 
	    		task.Task_Reminder_Date_Hide__c = System.today();
	    		setremiderList('8:00');
	    	}else{
	    		// Date not empty : build the datetime from the date field and hour/min field : 
	    		String  dtTime  ; 
		    	Integer dtHour = Task.ReminderdateTime.hour();
		    	Integer dtMin = Task.ReminderdateTime.minute();
		    	if (dtMin == 0){
		    		String sdtMin = '00' ; 
		    		dtTime = String.valueOf(dtHour) + ':' + sdtMin ; 
		    	}else{
		    		 dtTime = String.valueOf(dtHour) + ':' + String.valueOf(dtMin) ;
		    	} 
		    	setremiderList(dtTime); 
	    	}
	    // Reminder (New Task)
    	} else {
    		// By default, for new tasks, the hour is 8:00 ! 
    		setremiderList('8:00');
    	}
    }
    
   // Radio Button : Affiliated Account & All 
	public List<SelectOption> getItems() {
          selOptions = new List<SelectOption>(); 
          // Affiliated Account : 
          selOptions.add(new SelectOption(stOption1,stOption1));
          //All :  
          selOptions.add(new SelectOption(stOption2,stOption2));  
          return selOptions;     
    } 
    
    // Insert in the ReminderDate List the Time : Hours:Min 
    public List<SelectOption> getselReminderOptions(){
	  	selReminderOptions = new List<SelectOption>();
	  	String val = '' ; 
  		for (Integer i = 0 ; i < 24 ; i++){
  			// Draw the options to add to the reminder picklist 
  			val = i+':'+'00';  			
  			selReminderOptions.add(new SelectOption(val,val));
   			val = i+':'+'30';
  			selReminderOptions.add(new SelectOption(val,val));
  		}
        return selReminderOptions;    
    }    
	  
	  
	/*
	 * Refresh & Action Methods : 
	 *
	 */
	 
	// Dynamically insert the "Affiliated Accounts" into the Pick Lists 
    public PageReference refreshAffiliatedAccounts() {  	
    	// refresh Phone & email 
    	this.getContactLeadDetails(task.WhoId);
	    this.selFromAccountNames =new List<SelectOption>();
   		this.setAffiliatedAccounts(task.WhoId);
   		return null; 
   	}   	

	//manage the visability of the field in the VisualForce Page (AffiliatedAccount & All & ... )
	public void manageDisabled(){
		// If the second option of the radio button is choosed, disable the Affiliated account picklist : 		
		String choiceRT = getrelatedto();
   		if (choiceRT == stOption2 ){
			isAffAccDisabled = true ;
		}else{			
			isAffAccDisabled = false ; 
   		}
	}
	
	// Insert in the List alls the Account items of the PickList  (Value = Account Id ) (Label = Account Name)
   	public void setAffiliatedAccounts(String cId) {
   		selFromAccountNames = new List<SelectOption>();
   		selFromAccountNames.add(new SelectOption('','--None--'));
   		manageDisabled();
   		// iscontact Id ok ?
   		if (cId==null) {	
   			return;
   		}
   		//is a Lead or a Contact 
   		if (cId.subString(0,3) != '003'){ // Contact Prefix
   			return ;  
   		}	
   		// Get all the Accounts where it the Contact From is the contact selected in the Task Page :
		List<Affiliation__c> affs = [SELECT Affiliation_To_Account__r.Name, Affiliation_To_Account__r.Id FROM Affiliation__c where Affiliation_From_Contact__c =:cId];
		if (affs.size()==0){
			return; 
		}else{
			for(Affiliation__c aff : affs){
				// Fill in all the items : 
                String id = aff.Affiliation_To_Account__r.Id;
                String name = aff.Affiliation_To_Account__r.Name;
            	if (id == null) {
                 	continue;
            	}
                selFromAccountNames.add(new SelectOption(id,name));	                
			}			
		}
		manageDisabled();			
   	}
   	
   	// get the phone and email from the selected contact or Lead ! 
   	public void getContactLeadDetails(String cId){		
   		if (cId == null){
   			return ; 
   		}
	   	// Get the 3 first chars from the Contact or Lead Id : Object Prefix !  	
	   	String clidType = cId.subString(0,3); 
	   	String cphone = '  '  ;
	   	String cemail = '  ' ; 	
	   	if (clidType == '003'){		
	   		Contact[] 	pecont = new Contact[]{} ; 
	   		pecont = [Select id, Phone, Email from Contact where id=: cId limit 1];
	   		if (pecont.size() ==1){	   				
	   			if (pecont[0].Email != null) 	cemail  = pecont[0].Email ;   
	   			if (pecont[0].Phone != null)	cphone = pecont[0].Phone ;		   		
	   		}	
	   	}else{
	   	// get phone & mail from Lead  
	   		Lead[] pelead = new Lead[]{} ; 
	   		pelead = [Select id, Phone, Email from Lead where id=: cId limit 1];
	   		if (pelead.size() == 1){
	   			if (pelead[0].Email != null) 	cemail  = pelead[0].Email ;
		   		if (pelead[0].Phone != null)	cphone  = pelead[0].Phone ;
	   		}	
	   	}
	   	this.contactEmail = cphone ; 
	   	this.contactPhone = cemail ; 
   	}   
   		
   	/*
   	 * 	Save Task methods : 
   	 *
   	 */
   	 
   	// Save/Update a task (Called by different functions ! )
   	public Boolean saveTask(){
		String choiceRT = getrelatedto();
		System.debug('¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨SAVE¨¨¨¨¨¨' + choiceRT);
		if (choiceRT == stOption1 && accountsToList != null){
			task.whatid = accountsToList;
		}
		
		// If a lead is choosed. Do not let the Related to filled out with any information ! 
		String clid = task.WhoId ; 
		if (clid.subString(0,3) == '00Q'){ // Lead
			task.whatid = null ;
		}
		
		// If set a reminder is checked, add reminder date time !		
		doAddReminder();
		
		try {
			Database.Upsertresult upsr = Database.upsert(task);
	       if(upsr.isSuccess()){	          
			// If the save is successfull and SendNotification checked then send email !
	        	if (sendNotificationEmailCheckBox == 'True'){
					sendEmail();
				}
				// Go to the new Task Page : 
				return true ; 
      		}else{
      			return false ;
	       }
		}catch(Exception e){
			// Add Message to VisualForce and stay in the page :
            ApexPages.addMessages(e);
            return false ; 
		}     		
	}
	
	// when the 'Save' button is pressed on the VisualForce Page ! 
	public PageReference rsave () {
		// save it 
		if (saveTask()){
			// and go to the new Task Page : 
			PageReference pageRef = new PageReference('/' + task.Id);
	   		pageRef.setRedirect(true);
	   		return pageRef;
		}else{
			return null ;
		}
	}
	
	// when the 'Save & New Task' button is pressed on the VisualForce Page ! 
	public PageReference savenewtask(){
		//save it 
		if (saveTask()){ 
			// and go to the Create new Task Page : 
			PageReference pageRef = new PageReference('/00T/e?who_id=' + Task.whoId + '&retURL=/' + Task.WhoId) ; 
	   		pageRef.setRedirect(true);
	   		return pageRef;
		}else{
			return null ;
		} 	
	}	
	
	// when the 'Save & New Event' button is pressed on the VisualForce Page ! 
	public PageReference savenewevent(){
		//save it 
		if (saveTask()){ 
			// and go to the Create new Task Page : 
			PageReference pageRef = new PageReference('/00U/e?who_id=' + Task.whoId + '&retURL=/' + Task.WhoId) ; 
	   		pageRef.setRedirect(true);
	   		return pageRef;
		}else{
			return null ;
		}
	}	
	
	//	If the SetReminder is checked, then add a new Reminder to the task :	 
   	private void doAddReminder(){
   		// Only ih the checkBox ReminderSet is set to true !! 
   		system.debug('***********************************' + task.Task_Reminder_Date_Hide__c);
   		if (task.Task_Reminder_Date_Hide__c == null){
   			return ; 
   		}
   		// checkbox : 
        if(!task.IsReminderSet){
        	return ;
        }else{
        	String reminderTime = getreminderList();
           	// <!> Need to be a String !!!()+ ' ')         	
            task.ReminderDateTime = dateTime.valueOf(task.Task_Reminder_Date_Hide__c + ' '); 
            // Add Hours : 
            Integer nrHours = Integer.valueOf(reminderTime.substring(0,reminderTime.indexof(':',0)));
            if (nrHours>0){
				 task.ReminderdateTime = task.ReminderdateTime.addHours(nrHours);
            }
            // Add Minutes if it exists (nb.: It's 0 or 30)
			Integer nrMinutes = Integer.valueOf(reminderTime.substring(reminderTime.length()-2)) ;
			if (nrMinutes > 0 ){
				 task.ReminderdateTime = task.ReminderdateTime.addMinutes(nrMinutes);
			}	
        }
    }
    
	/*
	 * 	Send Email
	 */
	 
	public void sendEmail(){
		// Get the Assigned User Id and ToAdress Email !
		String sendEmail = getAssignedUserEmail(Task.OwnerId);		
		if (sendEmail == null){
			return ;
		}else{
			// Create a new single email message object that will send out a single email to the addresses in the To, CC & BCC list.
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();		
			// Strings to hold the email addresses to which you are sending the email and assign !! (TO, CC, BCC)
			String[] toAddresses = new String[] {sendEmail};			  
			mail.setToAddresses(toAddresses);		
			
			// Specify the address used when the recipients reply to the email. 
			mail.setReplyTo(sendEmail);		
			
			// Specify the name used as the display name.
			mail.setSenderDisplayName('Salesforce Support');
					
			// Specify the subject line for your email address.
			mail.setSubject('New task : ' + task.subject);	
			
			// Set to True if you want to BCC yourself on the email.
			mail.setBccSender(false);
			
			// Optionally append the salesforce.com email signature to the email. The email address of the user executing the Apex Code will be used.
			mail.setUseSignature(false);
				
			// Specify the text content of the email.
			mail.setPlainTextBody('Salesforce.com : New task ' + task.subject);				
			// Set the Email body
			String htmlBody = 'Salesforce.com <br /><br />' ; 
				   htmlBody += Userinfo.getName() + ' has assigned a new task to you: ' + Task.subject + ' <br />';
				   htmlBody += 'For more details on this task, click on the link below:' + '<br /><br />';
				   htmlBody += 'http://na1.salesforce.com/' + Task.Id ;
			mail.setHtmlBody(htmlBody);
		
			// Send the emailcreated.
			Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
		}
	}
	 
	//called by the sendemail to get the email from the Assigned User (OwnerId)
	private String getAssignedUserEmail(Id userId){
		return [Select email from User where id=:userId limit 1].email;	
	}	
}