@isTest
public class Test_Notification_Grouping {
    
    private static testmethod void testInboundNotification(){
    	
    	Notification_Grouping__c grouping = new Notification_Grouping__c();
    	grouping.External_ID__c = '000300125662';
    	grouping.Scope__c = 'Case';
    	grouping.Unique_Key__c = 'Case:000300125662';
    	grouping.Inbound_Status__c = 'Failure';
    	    	    	
    	insert grouping;
    	
    	Test.startTest();
    	
    	Outbound_Message__c om1 = new Outbound_Message__c();
    	om1.External_ID__c = '000300125662';
    	om1.Internal_ID__c = 'XXXXXXXXXX';
    	om1.Operation__c = 'upsertServiceNotification';
    	om1.Status__c = 'TRUE';
    	om1.Request__c = '{"SAP_NOTIFICATION_TYPE" : "ZC","SALES_ORG_TEXT" : "MDT Italy"}';
    	
    	Outbound_Message__c om2 = new Outbound_Message__c();
    	om2.External_ID__c = '000400125662';
    	om2.Internal_ID__c = 'YYYYYYYYYY';
    	om2.Operation__c = 'upsertServiceOrder';
    	om2.Status__c = 'FALSE';
    	om2.Request__c = '{"MNWRKCNTR_SHORT_TEXT" : "Service Center TR","SAP_ORDER_TYPE" : "ZRS3"}';
    	
    	insert new List<Outbound_Message__c>{om1, om2};
    	
    	om1 = [Select Notification_Grouping__c from Outbound_Message__c where Id = :om1.Id];
    	System.assert(om1.Notification_Grouping__c == grouping.Id); 
    	
    	grouping = [Select Inbound_Status__c, Internal_ID__c, Related_Record_Type__c from Notification_Grouping__c where Id = :grouping.Id];
    	System.assert(grouping.Inbound_Status__c == 'Success');
    	System.assert(grouping.Related_Record_Type__c == 'ZC' );
    	
    	List<Notification_Grouping__c> grouping2 = [Select Inbound_Status__c, Internal_ID__c, Related_Record_Type__c from Notification_Grouping__c where External_Id__c = '000400125662' AND Scope__c = 'SVMXC__Service_Order__c'];
    	System.assert(grouping2.size() == 1);
    	System.assert(grouping2[0].Inbound_Status__c == 'Failure');
    	System.assert(grouping2[0].Related_Record_Type__c == 'ZRS3');
    	
    	om2 = [Select Notification_Grouping__c from Outbound_Message__c where Id = :om2.Id];
    	System.assert(om2.Notification_Grouping__c == grouping2[0].Id);
    	
		Test.stopTest();
    }
    
    private static testmethod void testOutboundNotification(){
    	
    	Notification_Grouping__c grouping = new Notification_Grouping__c();
    	grouping.External_ID__c = '000300125662';
    	grouping.Scope__c = 'Case';
    	grouping.Unique_Key__c = 'Case:000300125662';    	
    	grouping.Outbound_Status__c = 'Failure';
    	    	    	
    	insert grouping;
    	
    	SVMXC__Service_Order__c sOrder = new SVMXC__Service_Order__c();
    	sOrder.SVMX_SAP_Service_Order_No__c = '000400125662';
    	sOrder.SVMXC__Order_Status__c = 'Scheduled';
    	insert sOrder;
    	
    	Test.startTest();
    	
    	NotificationSAPLog__c notSAPLog1 = new NotificationSAPLog__c();
    	notSAPLog1.Record_SAPID__c = '000300125662';
    	notSAPLog1.Record_ID__c = 'XXXXXXXXXX';
    	notSAPLog1.SFDC_Object_Name__c = 'Case';
    	notSAPLog1.WebServiceName__c = 'Case_NotificationSAP';
    	notSAPLog1.Status__c = 'NEW';
    	    	
    	NotificationSAPLog__c notSAPLog2 = new NotificationSAPLog__c();
    	notSAPLog2.Record_SAPID__c = '000400125662';
    	notSAPLog2.Record_ID__c = 'YYYYYYYYYY';
    	notSAPLog2.SFDC_Object_Name__c = 'SVMXC__Service_Order__c';
    	notSAPLog2.WebServiceName__c = 'ServiceOrder_NotificationSAP';
    	notSAPLog2.Status__c = 'NEW';
    	
    	insert new List<NotificationSAPLog__c>{notSAPLog1, notSAPLog2};
    	
    	notSAPLog1 = [Select Notification_Grouping__c from NotificationSAPLog__c where Id = :notSAPLog1.Id];
    	System.assert(notSAPLog1.Notification_Grouping__c == grouping.Id); 
    	
    	grouping = [Select Outbound_Status__c, Internal_ID__c, Related_Record_Type__c from Notification_Grouping__c where Id = :grouping.Id];
    	System.assert(grouping.Outbound_Status__c == 'Failure');
    	    	
    	List<Notification_Grouping__c> grouping2 = [Select Outbound_Status__c, Internal_ID__c, Related_Record_Type__c from Notification_Grouping__c where External_Id__c = '000400125662' AND Scope__c = 'SVMXC__Service_Order__c'];
    	System.assert(grouping2.size() == 1);
    	System.assert(grouping2[0].Outbound_Status__c == 'Failure');
    	    	
    	notSAPLog2 = [Select Notification_Grouping__c from NotificationSAPLog__c where Id = :notSAPLog2.Id];
    	System.assert(notSAPLog2.Notification_Grouping__c == grouping2[0].Id);
    	
    	notSAPLog1.Status__c = 'COMPLETED';
    	notSAPLog2.Status__c = 'COMPLETED';
    	
    	update new List<NotificationSAPLog__c>{notSAPLog1, notSAPLog2};
    	    	
    	grouping = [Select Outbound_Status__c, Internal_ID__c, Related_Record_Type__c from Notification_Grouping__c where Id = :grouping.Id];
    	System.assert(grouping.Outbound_Status__c == 'Success');
    	
    	grouping2 = [Select Outbound_Status__c, Internal_ID__c, Related_Record_Type__c from Notification_Grouping__c where External_Id__c = '000400125662' AND Scope__c = 'SVMXC__Service_Order__c'];
    	System.assert(grouping2.size() == 1);    	
    	System.assert(grouping2[0].Outbound_Status__c == 'Success');
    	
    	sOrder.SVMXC__Order_Status__c = 'Job Completed';
    	update sOrder;
    	
    	NotificationSAPLog__c notSAPLog3 = new NotificationSAPLog__c();
    	notSAPLog3.Record_SAPID__c = '000400125662';
    	notSAPLog3.Record_ID__c = 'YYYYYYYYYY';
    	notSAPLog3.SFDC_Object_Name__c = 'SVMXC__Service_Order__c';
    	notSAPLog3.WebServiceName__c = 'ServiceOrder_NotificationSAP';
    	notSAPLog3.Status__c = 'NEW';
    	
    	insert notSAPLog3;
    	
    	grouping2 = [Select Outbound_Status__c, Internal_ID__c, Related_Record_Type__c from Notification_Grouping__c where External_Id__c = '000400125662' AND Scope__c = 'SVMXC__Service_Order__c'];
    	System.assert(grouping2.size() == 1);    	
    	System.assert(grouping2[0].Outbound_Status__c == 'Failure');
    	
    	notSAPLog3.Status__c = 'COMPLETED';
    	update notSAPLog3;
    	
    	grouping2 = [Select Outbound_Status__c, Internal_ID__c, Related_Record_Type__c from Notification_Grouping__c where External_Id__c = '000400125662' AND Scope__c = 'SVMXC__Service_Order__c'];
    	System.assert(grouping2.size() == 1);    	
    	System.assert(grouping2[0].Outbound_Status__c == 'Failure');
    	
    	NotificationSAPLog__c notSAPLog4 = new NotificationSAPLog__c();
    	notSAPLog4.Record_SAPID__c = '000400125662';
    	notSAPLog4.Record_ID__c = 'YYYYYYYYYY';
    	notSAPLog4.SFDC_Object_Name__c = 'SVMXC__Service_Order__c';
    	notSAPLog4.WebServiceName__c = 'ServiceOrderComplete_NotificationSAP';
    	notSAPLog4.Status__c = 'NEW';    	
    	insert notSAPLog4;
    	
    	notSAPLog4.Status__c = 'COMPLETED';
    	update notSAPLog4;
    	
    	grouping2 = [Select Outbound_Status__c, Internal_ID__c, Related_Record_Type__c from Notification_Grouping__c where External_Id__c = '000400125662' AND Scope__c = 'SVMXC__Service_Order__c'];
    	System.assert(grouping2.size() == 1);    	
    	System.assert(grouping2[0].Outbound_Status__c == 'Success');
    	
    	delete notSAPLog4;
    	
    	grouping2 = [Select Outbound_Status__c, Internal_ID__c, Related_Record_Type__c from Notification_Grouping__c where External_Id__c = '000400125662' AND Scope__c = 'SVMXC__Service_Order__c'];
    	System.assert(grouping2.size() == 1);    	
    	System.assert(grouping2[0].Outbound_Status__c == 'Failure');
    	
    	undelete notSAPLog4;
    	
    	grouping2 = [Select Outbound_Status__c, Internal_ID__c, Related_Record_Type__c from Notification_Grouping__c where External_Id__c = '000400125662' AND Scope__c = 'SVMXC__Service_Order__c'];
    	System.assert(grouping2.size() == 1);    	
    	System.assert(grouping2[0].Outbound_Status__c == 'Success');
    	
		Test.stopTest();
    }
}