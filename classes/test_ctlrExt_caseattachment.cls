/*
 *   Test Class name: test_ctlrExt_caseattachment
 *   Class Name : ctlrExt_caseattachment
 *   Author : Priti Jaiswal
 *   Description: This class is written to improve the code coverage for class ctlrExt_caseattachment     
*/

@isTest
private class test_ctlrExt_caseattachment{
    static testMethod void myUnitTest()
    {
        Case caseObj = new Case();        
        caseObj.Status='New';
        caseObj.Origin='Patient';
        insert caseObj;
        
        Attachment attachObj=new Attachment();
        attachObj.parentid=caseObj.id;
        attachObj.name='testattachObjment';
        Blob bodyBlob=Blob.valueOf('Attachment Body');
        attachObj.body=bodyBlob;
        attachObj.body=bodyBlob;
        insert attachObj;
        
        System.CurrentPageReference().getParameters().put('id',caseObj.id);
        ApexPages.StandardController cont = new ApexPages.StandardController(attachObj);       
        ctlrExt_caseattachment controller1 = new ctlrExt_caseattachment(cont);
        controller1.attchFiles();
        controller1.changeCount();
        
    }
}