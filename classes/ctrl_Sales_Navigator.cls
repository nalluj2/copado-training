public with sharing class ctrl_Sales_Navigator {
	
	public String noAccessMessage {get; set;}
		
	public PageReference openSalesNavigator(){
		
		String url;
		
		Map<String, SICA_Endpoints__c> endpoints = SICA_Endpoints__c.getall();
				
		String inputAccountId = ApexPages.currentPage().getParameters().get('AccountId');
		
		if(ApexPages.currentPage().getUrl().containsIgnoreCase('SICA_Insights')){
			
			url = endpoints.get('OutSystems').URL__c;
			
			List<Account> userAccounts = [Select Id from Account where SICA_Sales_Rep__c = :UserInfo.getUserId() LIMIT 1];
			
			if(inputAccountId != null){
				
				url += '?AccountId=' + inputAccountId;
				
				Account acc = [Select SICA_Sales_Rep__c from Account where Id = :inputAccountId];
				
				if(acc.SICA_Sales_Rep__c == null){
					
					noAccessMessage = '<br/<br/><b>Your account is currently not part of the SI Reboot 21 Capacity Pulse initiative.</b><br/><br/><b>If you would like to add your account to this initiative reach out to</b>&nbsp;<a href="mailto:rs.sicommercialanalytics@medtronic.com">rs.sicommercialanalytics@medtronic.com</a>';
					return null;
					
				}else if(acc.SICA_Sales_Rep__c != UserInfo.getUserId()){
			
					noAccessMessage = '<br/<br/><b>This account has been assigned to another user for the SI Reboot 21 Capacity Pulse initiative.</b><br/><br/><b>For any problem or question, please reach out to</b>&nbsp;<a href="mailto:rs.sicommercialanalytics@medtronic.com">rs.sicommercialanalytics@medtronic.com</a>';
					return null;
				}
					
			}else{
				
				if(userAccounts.isEmpty()){
					
					noAccessMessage = '<br/<br/><b>None of your accounts are currently part of the SI Reboot 21 Capacity Pulse.</b><br/><br/><b>If you would like to participate in it please let us know at</b>&nbsp;<a href="mailto:rs.sicommercialanalytics@medtronic.com">rs.sicommercialanalytics@medtronic.com</a>';
					return null;					
				}				
			}
			
		}else if(ApexPages.currentPage().getUrl().containsIgnoreCase('Sales_Navigator')){
			
			User currentUser = [Select Id, FederationIdentifier, Job_Title_vs__c from User where Id = :UserInfo.getUserId()];
			
			if(currentUser.Job_title_vs__c.endsWith('BU Manager')) url = endpoints.get('TableauBUD').URL__c;
			else if(currentUser.Job_title_vs__c.endsWith('Manager')) url = endpoints.get('TableauRSM').URL__c;
			else url = endpoints.get('TableauSR').URL__c;
			
			url += '&AliasParameter=' + currentUser.FederationIdentifier;		
		}
					
		PageReference retURL = new PageReference(url);
      	retURL.setRedirect(true);
      	return retURL;
	}    
}