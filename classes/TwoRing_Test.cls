@isTest
Private Class TwoRing_Test {

	Static testMethod void test_PerformLookupController() {
		//Set up the Account record
		Account testAcct = New Account(Name = 'TestAccount');
		insert testAcct;
		
		//Perform Lookup
		TwoRing_QueryObject query = New TwoRing_QueryObject();
		query.entityTypes = New List<String> {TwoRing_EntityType.ACCOUNT, TwoRing_EntityType.CONTACT, TwoRing_EntityType.LEAD};
		query.ids = New List<string> {testAcct.Id};
		List<SObject> result = TwoRing_PerformLookupController.performLookup(query);

		//Verify that the controller has returned the correct account
		If (result.size() > 0 && result.get(0).Id == testAcct.id) {
			System.assert(true, 'Data Received Successfully!');
		}
		Else {
			System.assert(false, 'Data Failed To Receive!');
		}
	}

	Static testMethod void test_PerformQueryController() {
		//Set up the Account record
		Account testAcct = New Account(Name = 'TestAccount');
		insert testAcct;
		
		//Perform Query
		String query = 'SELECT Id FROM Account WHERE Name = \'TestAccount\'';
		List < sObject > result = TwoRing_PerformQueryController.performQuery(query);

		//Verify that the repository has returned the correct account
		If (result.get(0).Id == testAcct.id) { 
			System.assert(true, 'Data Received Successfully!');
		}
		Else {
			System.assert(false, 'Data Failed To Receive!');
		}
	}

	Static testMethod void test_PerformQueryControllerInvalidQuery() {
		Try {
			//Perform Query
			List<sObject> result = TwoRing_PerformQueryController.performQuery('SELECT');
			System.assert(false, 'Exception was expected!');
		}
		Catch(TwoRing_ApplicationException ex) {
			System.assert(true, 'Exception was thrown!');
		}
	}

	Static testMethod void test_DeleteEntity() {
		Account testAcct = New Account(Name = 'TestAccount');
		insert testAcct;
		TwoRing_Repository.deleteEntity(testAcct.Id);
		List<sObject> result = TwoRing_PerformQueryController.performQuery('SELECT Id FROM Account WHERE Id=\'' + testAcct.Id+'\'');
		If (result.size() == 0){
			System.assert(true, 'Account deleted');
		}
		Else {
			System.assert(false, 'Account was not deleted');
		}
	}

	Static testMethod void test_DeleteEntityInvalidId() {
		Try {
			TwoRing_Repository.deleteEntity('########');
			System.assert(false, 'Exception was expected!');
		}
		Catch(TwoRing_ApplicationException ex) {
			System.assert(true, 'Exception was thrown!');
		}
	}

	Static testMethod void test_ReadEntity() {
		Account testAcct = New Account(Name = 'TestAccount');
		insert testAcct;
		SObject sobj = TwoRing_Repository.readEntity(testAcct.Id);
		If (testAcct.Id == sobj.get('Id')){
			System.assert(True, 'Record Readed');
        }
		Else {
			System.assert(false, 'Record was not readed');
		}
	}

	Static testMethod void test_ReadEntityInvalidId() {
		Try {
			TwoRing_Repository.readEntity('########');
			System.assert(false, 'Exception was expected!');
		}
		Catch(TwoRing_ApplicationException ex) {
			System.assert(true, 'Exception was thrown!');
		}
	}

	Static testMethod void test_LookupById() {
		//Set up the Account record
		Account testAcct = New Account(Name = 'TestAccount');
		insert testAcct;
		
		//Perform Lookup
		TwoRing_QueryObject query = New TwoRing_QueryObject();
		query.entityTypes = New List<String> {TwoRing_EntityType.ACCOUNT, TwoRing_EntityType.CONTACT, TwoRing_EntityType.LEAD};
		query.ids = New List<string> {testAcct.Id};
		List<SObject> result = TwoRing_Repository.performLookup(query);

		//Verify that the repository has returned the correct account
		If (result.size() > 0 && result.get(0).Id == testAcct.id) {
			System.assert(true, 'Data Received Successfully!');
		}
		Else {
			System.assert(false, 'Data Failed To Receive!');
		}
	}

	Static testMethod void test_LookupByCustomCriteria() {
		//Set up the Account record
		Account testAcct = clsTestData_Account.createAccount(false)[0];
		testAcct.Name = 'TestAccount';
		insert testAcct;
		Contact testContact = clsTestData_Contact.createContact(false)[0];
		testContact.LastName = 'TestContact';
		insert testContact;

		//Perform Lookup
		TwoRing_QueryObject query = New TwoRing_QueryObject();
		query.entityTypes = New List<String> {TwoRing_EntityType.ACCOUNT, TwoRing_EntityType.CONTACT, TwoRing_EntityType.LEAD};
		query.fields = New Map<string, List<string>>();
		query.fields.put('contact', new List<String> {'Account.Id', 'Id'});
		TwoRing_CustomCriteria customCriteria = New TwoRing_CustomCriteria();
		customCriteria.fields = New List<String>{'Account.Name'};
		customCriteria.isTextField = true;
		customCriteria.values = New List<List<String>>();
		customCriteria.values.add(New List<String> {'TestAccount'});
		query.customCriterias = New Map<String, List<TwoRing_CustomCriteria>>();
		query.customCriterias.put('contact', new List<TwoRing_CustomCriteria> {customCriteria});

		List<SObject> result = TwoRing_Repository.performLookup(query);

		//Verify that the repository has returned the correct account
		If (result.size() > 0 && result.get(0).Id == testContact.id && ((Contact)result.get(0)).Account.Id == testAcct.id) {
			System.assert(true, 'Data Received Successfully!');
		}
		Else {
			System.assert(false, 'Data Failed To Receive!');
		}
	}

	Static testMethod void test_LookupByMultipleCustomCriteriaAND() {
		//Set up the Account And Contact record
		Account testAcct = clsTestData_Account.createAccount(false)[0];
		testAcct.Name = 'TestAccount';
		insert testAcct;
		Contact testContact = clsTestData_Contact.createContact(false)[0];
		testContact.LastName = 'TestContact';
		insert testContact;
		
		//Perform Lookup
		TwoRing_QueryObject query = New TwoRing_QueryObject();
		query.entityTypes = New List<String> {TwoRing_EntityType.ACCOUNT, TwoRing_EntityType.CONTACT, TwoRing_EntityType.LEAD};
		query.fields = New Map<string, List<string>>();
		query.fields.put('contact', new List<String> {'Account.Id', 'Id'});
		TwoRing_CustomCriteria customCriteria = New TwoRing_CustomCriteria();
		customCriteria.fields = New List<String>{'Account.Name'};
		customCriteria.isTextField = true;
		customCriteria.values = New List<List<String>>();
		customCriteria.values.add(New List<String> {'%Test%', '%Account%'});
		query.customCriterias = New Map<String, List<TwoRing_CustomCriteria>>();
		query.customCriterias.put('contact', new List<TwoRing_CustomCriteria> {customCriteria});

		List<SObject> result = TwoRing_Repository.performLookup(query);

		//Verify that the repository has returned the correct account And contact
		If (result.size() > 0 && result.get(0).Id == testContact.id && ((Contact)result.get(0)).Account.Id == testAcct.id) {
			System.assert(true, 'Data Received Successfully!');
		}
		Else {
			System.assert(false, 'Data Failed To Receive!');
		}
	}

	Static testMethod void test_LookupByMultipleCustomCriteriaOR() {
		//Set up the Account And Contact record
		Account testAcct = clsTestData_Account.createAccount(false)[0];
		testAcct.Name = 'TestAccount';
		insert testAcct;
		Contact testContact = clsTestData_Contact.createContact(false)[0];
		testContact.LastName = 'TestContact';
		insert testContact;
		
		//Perform Lookup
		TwoRing_QueryObject query = New TwoRing_QueryObject();
		query.entityTypes = New List<String> {TwoRing_EntityType.ACCOUNT, TwoRing_EntityType.CONTACT, TwoRing_EntityType.LEAD};
		query.fields = New Map<string, List<string>>();
		query.fields.put('contact', new List<String> {'Account.Id', 'Id'});
		TwoRing_CustomCriteria customCriteria = New TwoRing_CustomCriteria();
		customCriteria.fields = New List<String>{'Account.Name'};
		customCriteria.isTextField = true;
		customCriteria.values = New List<List<String>> { New List<String> {'%Test%'}, new List<String> {'%Account%'} };
		query.customCriterias = New Map<String, List<TwoRing_CustomCriteria>>();
		query.customCriterias.put('contact', new List<TwoRing_CustomCriteria> {customCriteria});

		List<SObject> result = TwoRing_Repository.performLookup(query);

		//Verify that the repository has returned the correct account And contact
		If (result.size() > 0 && result.get(0).Id == testContact.id && ((Contact)result.get(0)).Account.Id == testAcct.id) {
			System.assert(true, 'Data Received Successfully!');
		}
		Else {
			System.assert(false, 'Data Failed To Receive!');
		}
	}
	
	Static testMethod void test_LookupByMultipleCriteria() {
		//Set up the Account record
		Account testAcct = New Account(Name = 'TestAccount');
		insert testAcct;
		Id [] fixedSearchResults= New Id[1];
		fixedSearchResults[0] = testAcct.Id;
		Test.setFixedSearchResults(fixedSearchResults);
		
		//Perform Lookup
		TwoRing_QueryObject query = New TwoRing_QueryObject();
		query.entityTypes = New List<String> {TwoRing_EntityType.ACCOUNT, TwoRing_EntityType.CONTACT, TwoRing_EntityType.LEAD};
		query.phones = New List<List<string>> {New List<string> {'Phone'}};
		query.names = New List<List<string>> {New List<string> {'Name'}};
		query.emails = New List<List<string>> {New List<string> {'Email'}};
		query.ids = New List<string> {testAcct.Id};
		List<SObject> result = TwoRing_Repository.performLookup(query);

		//Verify that the repository has returned the correct account
		If (result.size() > 0 && result.get(0).Id == testAcct.id) {
			System.assert(true, 'Data Received Successfully!');
		}
		Else {
			System.assert(false, 'Data Failed To Receive!');
		}
	}
	
	Static testMethod void test_LookupWihSpecificFieldsById() {
		//Set up the Account record        
		Account testAcct = New Account(Name = 'TestAccount', Phone = '12345');
		insert testAcct;        
	
		//Perform Lookup
		TwoRing_QueryObject query = New TwoRing_QueryObject();
		query.entityTypes = New List<String> {TwoRing_EntityType.ACCOUNT, TwoRing_EntityType.CONTACT, TwoRing_EntityType.LEAD};
		query.fields = New Map<string, List<string>>();
		query.fields.put('account', new List<String> {'Name'});
		query.ids = New List<string> {testAcct.Id};
		List<SObject> result = TwoRing_Repository.performLookup(query);

		//Verify that the repository has returned record with specified fieds
		If (result.size() > 0 && hasSObjectField('Name', result.get(0)) && !hasSObjectField('Phone', result.get(0))) {
			System.assert(True, 'Data Received Successfully!');
        }
		Else {
			System.assert(false, 'Data Failed To Receive!');
		}
	}

	Static testMethod void test_LookupWithSpecificFieldsByPhone() {
		//Set up the Account record
		Account testAcct = New Account(Name = 'TestAccount', Phone = '12345');
		insert testAcct;
		Id [] fixedSearchResults= New Id[1];
		fixedSearchResults[0] = testAcct.Id;
		Test.setFixedSearchResults(fixedSearchResults);
		
		//Perform Lookup
		TwoRing_QueryObject query = New TwoRing_QueryObject();
		query.entityTypes = New List<String> {TwoRing_EntityType.ACCOUNT, TwoRing_EntityType.CONTACT, TwoRing_EntityType.LEAD};
		query.fields = New Map<string, List<string>>();
		query.fields.put('account', new List<String> {'Name'});
		query.phones = New List<List<string>> {New List<string> {'Phone'}};
		List<SObject> result = TwoRing_Repository.performLookup(query);

		//Verify that the repository has returned record with specified fieds
		If (result.size() > 0 && hasSObjectField('Name', result.get(0)) && !hasSObjectField('Phone', result.get(0))) {
			System.assert(True, 'Data Received Successfully!');
        }
		Else {
			System.assert(false, 'Data Failed To Receive!');
		}
	}
	
	Static testMethod void test_CreateEntityController() {
		//Create Account
		TwoRing_CreateOrUpdateEntityObject createObject = New TwoRing_CreateOrUpdateEntityObject();
		createObject.name = 'Account';
		createObject.data = New Map<String, String>{'Name' => 'TestAccount', 'Phone' => '12345'};
		TwoRing_CreateEntityController.createEntity(createObject);

		//Query Account
		List<Account> result = [SELECT id FROM Account
		WHERE Name = 'TestAccount' And Phone = '12345'];

		//Verify that the repository has returned record with specified fieds
		If (result.size() > 0) {
			System.assert(true, 'Data Created Successfully!');
		}
		Else {
			System.assert(false, 'Failed To Create Data!');
		}
	}

	Static testMethod void test_CreateEntity() {
		//Create Account
		TwoRing_CreateOrUpdateEntityObject createObject = New TwoRing_CreateOrUpdateEntityObject();
		createObject.name = 'Account';
		createObject.data = New Map<String, String>{'Name' => 'TestAccount', 'Phone' => '12345'};
		TwoRing_Repository.createEntity(createObject);

		//Query Account
		List<Account> result = [SELECT id FROM Account
		WHERE Name = 'TestAccount' And Phone = '12345'];

		//Verify that the repository has returned record with specified fieds
		If (result.size() > 0) {
			System.assert(true, 'Data Created Successfully!');
		}
		Else {
			System.assert(false, 'Failed To Create Data!');
		}
	}

	Static testMethod void test_CreateEntityOfNonExistingType() {
		Try {
			//Create Account
			TwoRing_CreateOrUpdateEntityObject createObject = New TwoRing_CreateOrUpdateEntityObject();
			createObject.name = 'NonExistingType';
			createObject.data = New Map<String, String>{'Name' => 'TestAccount', 'Phone' => '12345'};
			TwoRing_Repository.createEntity(createObject);

			System.assert(false, 'Exception was expected!');
		}
		Catch(TwoRing_ApplicationException ex) {
			System.assert(true, 'Exception was thrown!');
		}
	}

	Static testMethod void test_UpdateEntityController()
		{
			Account testAcct = New Account(Name = 'TestAccount', Phone = '12345');
			insert testAcct;
			TwoRing_CreateOrUpdateEntityObject updateObject = New TwoRing_CreateOrUpdateEntityObject();
				updateObject.id = testAcct.Id;
				updateObject.name = 'Account';
				updateObject.data = New Map<String, String>{'Phone' => '456789'};
			TwoRing_UpdateEntityController.updateEntity(updateObject);

			//Query Account
			List<Account> result = [SELECT id FROM Account
			WHERE Name = 'TestAccount' And Phone = '456789'];

			//Verify that the repository has returned record with specified fieds
			If (result.size() > 0) {
				System.assert(true, 'Data Updated Successfully!');
			}
			Else {
				System.assert(false, 'Failed To Update data!');
			}
	}

	Static testMethod void test_UpdateEntity() {
		//Create Account
		Account testAcct = New Account(Name = 'TestAccount', Phone = '12345');
		insert testAcct;
		Id [] fixedSearchResults = New Id[1];
		fixedSearchResults[0] = testAcct.Id;
		Test.setFixedSearchResults(fixedSearchResults);

		//Update Account
		TwoRing_CreateOrUpdateEntityObject updateObject = New TwoRing_CreateOrUpdateEntityObject();
		updateObject.id = testAcct.Id;
		updateObject.name = 'Account';
		updateObject.data = New Map<String, String>{'Phone' => '456789'};
		TwoRing_Repository.updateEntity(updateObject);

		//Query Account
		List<Account> result = [SELECT id FROM Account
		WHERE Name = 'TestAccount' And Phone = '456789'];

		//Verify that the repository has returned record with specified fieds
		If (result.size() > 0) {
			System.assert(true, 'Data Updated Successfully!');
		}
		Else {
			System.assert(false, 'Failed To Update data!');
		}
	}

	Static testMethod void test_UpdateEntityOfNonExistingType() {
		Try {
			//Create Account
			Account testAcct = New Account(Name = 'TestAccount', Phone = '12345');
			insert testAcct;
			Id [] fixedSearchResults = New Id[1];
			fixedSearchResults[0] = testAcct.Id;
			Test.setFixedSearchResults(fixedSearchResults);

			//Update Account
			TwoRing_CreateOrUpdateEntityObject updateObject = New TwoRing_CreateOrUpdateEntityObject();
			updateObject.id = testAcct.Id;
			updateObject.name = 'NonExistingType';
			updateObject.data = New Map<String, String>{'Phone' => '456789'};
			TwoRing_Repository.updateEntity(updateObject);

			System.assert(false, 'Exception was expected!');
		}
		Catch(TwoRing_ApplicationException ex) {
			System.assert(true, 'Exception was thrown!');
		}
	}

	Static testMethod void test_UpdateEntityWithNonExistingId() {
		Try {
			//Create Account
			Account testAcct = New Account(Name = 'TestAccount', Phone = '12345');
			insert testAcct;
			Id [] fixedSearchResults = New Id[1];
			fixedSearchResults[0] = testAcct.Id;
			Test.setFixedSearchResults(fixedSearchResults);

			//Update Account
			TwoRing_CreateOrUpdateEntityObject updateObject = New TwoRing_CreateOrUpdateEntityObject();
			updateObject.id = 'NonExistingId';
			updateObject.name = 'Account';
			updateObject.data = New Map<String, String>{'Phone' => '456789'};
			TwoRing_Repository.updateEntity(updateObject);

			System.assert(false, 'Exception was expected!');
		}
		Catch(TwoRing_ApplicationException ex) {
			System.assert(true, 'Exception was thrown!');
		}
	}

	/**
	* check if an Sobject has a field
	*/
	Private Static Boolean hasSObjectField(String fieldName, SObject so){
		String s = JSON.serialize(so);
		// Deserialize it back into a key/value map
		Map<String,Object> obj = (Map<String,Object>) JSON.deserializeUntyped(s);
		// Build a set containing the fields present on our SObject
		Set<String> fieldsPresent = obj.keyset().clone();
		Return fieldsPresent.contains(fieldName);
	}
}