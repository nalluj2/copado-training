@isTest
private class Test_ctrlExt_ImplantSchedulingTeam_New {
	/*
	private static testmethod void createTerritoryTeam(){
		
		Implant_Scheduling_Team_Model__c countrySettings = new Implant_Scheduling_Team_Model__c();
		countrySettings.Name = 'ZZ';
		countrySettings.Uses_Extended_Team__c = false;
		insert countrySettings;
		
		User currentUser = [Select Id from User where Id = :UserInfo.getUserId()];
		
		System.runAs(currentUSer){
			
			setData();
			
			Test.startTest();
			
			Implant_Scheduling_Team__c team = new Implant_Scheduling_Team__c();
			
			ApexPages.StandardController sc = new ApexPages.StandardController(team);
			ctrlExt_ImplantSchedulingTeam_New controller = new ctrlExt_ImplantSchedulingTeam_New(sc);
			
			team.Country__c = [Select Id from DiB_Country__c].Id;
			
			controller.countrySelected();
			
			System.assert(controller.usesExtendedTeam == false);
			
			team.Business_Unit__c = [Select Id from Business_Unit__c].Id;
			team.Name = 'Test Territory Team';
			team.Display_Case_fields__c = 'Subject,AccountId';
			
			PageReference pr = controller.save();
			
			System.assert(pr != null);
		}		
	}
	
	private static testmethod void createCoreTeam(){
					
		Implant_Scheduling_Team_Model__c countrySettings = new Implant_Scheduling_Team_Model__c();
		countrySettings.Name = 'ZZ';
		countrySettings.Uses_Extended_Team__c = true;
		insert countrySettings;
		
		User currentUser = [Select Id from User where Id = :UserInfo.getUserId()];
		
		System.runAs(currentUSer){
			
			setData();
			
			Test.startTest();
			
			Implant_Scheduling_Team__c team = new Implant_Scheduling_Team__c();
			
			ApexPages.StandardController sc = new ApexPages.StandardController(team);
			ctrlExt_ImplantSchedulingTeam_New controller = new ctrlExt_ImplantSchedulingTeam_New(sc);
			
			team.Country__c = [Select Id from DiB_Country__c].Id;
			
			controller.countrySelected();			
			System.assert(controller.usesExtendedTeam == true);
			System.assert(controller.isCoreTeam == true);
			
			team.Business_Unit__c = [Select Id from Business_Unit__c].Id;
			team.Name = 'Test Core Team';
			team.Display_Case_fields__c = 'Subject,AccountId';
			
			PageReference pr = controller.save();
			
			System.assert(pr != null);
		}		
	}
	
	private static testmethod void createExtendedTeam(){
		
		Implant_Scheduling_Team_Model__c countrySettings = new Implant_Scheduling_Team_Model__c();
		countrySettings.Name = 'ZZ';
		countrySettings.Uses_Extended_Team__c = true;
		insert countrySettings;
		
		User currentUser = [Select Id from User where Id = :UserInfo.getUserId()];
		
		System.runAs(currentUSer){
			
			setData();
			
			Test.startTest();
			
			Implant_Scheduling_Team__c coreTeam = new Implant_Scheduling_Team__c();
			coreTeam.Display_Case_fields__c = 'Subject,AccountId';
			coreTeam.Name = 'Test Core Team';
			coreTeam.Business_Unit__c = [Select Id from Business_Unit__c].Id;
			coreTeam.Country__c = [Select Id from DiB_Country__c].Id;
			coreTeam.RecordTypeId = [Select Id from RecordType where SObjectType = 'Implant_Scheduling_Team__c' AND DeveloperName = 'Core_Team'].Id;
			
			insert coreTeam;
			
			Implant_Scheduling_Team__c team = new Implant_Scheduling_Team__c();
			team.Core_Team__c = coreTeam.Id;
			
			ApexPages.StandardController sc = new ApexPages.StandardController(team);
			ctrlExt_ImplantSchedulingTeam_New controller = new ctrlExt_ImplantSchedulingTeam_New(sc);
							
			System.assert(controller.usesExtendedTeam == true);
			System.assert(controller.isCoreTeam == false);
						
			team.Name = 'Test Extended Team';
									
			PageReference pr = controller.save();
			
			System.assert(pr != null);
		}		
	}
	
	private static testmethod void createCloneTeam(){
		
		Implant_Scheduling_Team_Model__c countrySettings = new Implant_Scheduling_Team_Model__c();
		countrySettings.Name = 'ZZ';
		countrySettings.Uses_Extended_Team__c = true;
		insert countrySettings;
		
		User currentUser = [Select Id from User where Id = :UserInfo.getUserId()];
		
		System.runAs(currentUSer){
			
			setData();
			
			Test.startTest();
			
			Implant_Scheduling_Team__c team = new Implant_Scheduling_Team__c();
			team.Display_Case_fields__c = 'Subject,AccountId';
			team.Name = 'Test Core Team';
			team.Business_Unit__c = [Select Id from Business_Unit__c].Id;
			team.Country__c = [Select Id from DiB_Country__c].Id;
			team.RecordTypeId = [Select Id from RecordType where SObjectType = 'Implant_Scheduling_Team__c' AND DeveloperName = 'Core_Team'].Id;
			
			insert team;
			
			ApexPages.StandardController sc = new ApexPages.StandardController(team);
			ctrlExt_ImplantSchedulingTeam_New controller = new ctrlExt_ImplantSchedulingTeam_New(sc);
			
			System.Assert(controller.isCoreTeam == true);		
		}
	}
	
	private static testmethod void createTerritoryTeamError(){
		
		Implant_Scheduling_Team_Model__c countrySettings = new Implant_Scheduling_Team_Model__c();
		countrySettings.Name = 'ZZ';
		countrySettings.Uses_Extended_Team__c = false;
		insert countrySettings;
		
		User currentUser = [Select Id from User where Id = :UserInfo.getUserId()];
		
		System.runAs(currentUSer){
			
			setData();
			
			Test.startTest();
			
			Implant_Scheduling_Team__c team = new Implant_Scheduling_Team__c();
			
			ApexPages.StandardController sc = new ApexPages.StandardController(team);
			ctrlExt_ImplantSchedulingTeam_New controller = new ctrlExt_ImplantSchedulingTeam_New(sc);
			
			PageReference pr = controller.save();
			
			System.assert(pr == null);
			
			team.Country__c = [Select Id from DiB_Country__c].Id;
			
			controller.countrySelected();
			
			pr = controller.save();
			
			System.assert(pr == null);			
		}		
	}
	
	private static testmethod void createExtendedTeamError(){
		
		Implant_Scheduling_Team_Model__c countrySettings = new Implant_Scheduling_Team_Model__c();
		countrySettings.Name = 'ZZ';
		countrySettings.Uses_Extended_Team__c = true;
		insert countrySettings;
		
		User currentUser = [Select Id from User where Id = :UserInfo.getUserId()];
		
		System.runAs(currentUSer){
			
			setData();
			
			Test.startTest();
					
			Implant_Scheduling_Team__c team = new Implant_Scheduling_Team__c();
						
			ApexPages.StandardController sc = new ApexPages.StandardController(team);
			ctrlExt_ImplantSchedulingTeam_New controller = new ctrlExt_ImplantSchedulingTeam_New(sc);
			
			team.Country__c = [Select Id from DiB_Country__c].Id;
			
			controller.countrySelected();			
			System.assert(controller.usesExtendedTeam == true);
			
			controller.isCoreTeam = false;
															
			PageReference pr = controller.save();
			
			System.assert(pr == null);
		}		
	}
	
	private static void setData() {
	
		Company__c cmpny = new Company__c();		
        cmpny.name='Europe';
        cmpny.CurrencyIsoCode = 'EUR';
        cmpny.Current_day_in_Q1__c=56;
        cmpny.Current_day_in_Q2__c=34; 
        cmpny.Current_day_in_Q3__c=5; 
        cmpny.Current_day_in_Q4__c= 0;   
        cmpny.Current_day_in_year__c =200;
        cmpny.Days_in_Q1__c=56;  
        cmpny.Days_in_Q2__c=34;
        cmpny.Days_in_Q3__c=13;
        cmpny.Days_in_Q4__c=22;
        cmpny.Days_in_year__c =250;
        cmpny.Company_Code_Text__c = 'T35';
		insert cmpny;
		
		Business_Unit_Group__c bug =  new Business_Unit_Group__c();
        bug.Master_Data__c =cmpny.id;
        bug.name='RTG';      
        insert bug;
		
		Business_Unit__c bu =  new Business_Unit__c();
        bu.Company__c =cmpny.id;
        bu.name='RTG BU';
        bu.Business_Unit_Group__c = bug.Id;
        bu.Account_Plan_Activities__c=true;        
        insert bu;
        
        Sub_Business_Units__c sbu = new Sub_Business_Units__c();
        sbu.name='RTG sBU';
        sbu.Business_Unit__c=bu.id;
		insert sbu;
		
		DIB_Country__c country = new DIB_Country__c();
		country.Company__c = cmpny.Id;
		country.Name = 'Unit Test Country';
		country.Country_ISO_Code__c = 'ZZ';
		country.CurrencyIsoCode = 'EUR';
		insert country;
									
		Account testAccount = new Account();
		testAccount.Name = 'RTG Test Account';
		insert testAccount;
	}
	*/
}