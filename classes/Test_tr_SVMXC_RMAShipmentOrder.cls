@isTest(seeAllData = true)
private class Test_tr_SVMXC_RMAShipmentOrder {
    
    private static testmethod void setShipmentOrderCountry(){
    	   	
    	Account acc = new Account();
    	acc.Account_Country_vs__c = 'NETHERLANDS';
    	acc.Name = 'Test Account';
    	insert acc;
    	
    	Contact cnt = new Contact();
    	cnt.AccountId = acc.Id;
    	cnt.LastNAme = 'Test';
    	cnt.FirstName = 'Test';
    	cnt.Contact_Department__c = 'Diabetes Adult'; 
        cnt.Contact_Primary_Specialty__c = 'ENT';
        cnt.Affiliation_To_Account__c = 'Employee';
        cnt.Primary_Job_Title_vs__c = 'Manager';
        cnt.Contact_Gender__c = 'Male'; 
    	insert cnt;
    	
    	SVMXC__Service_Order__c workorder = new SVMXC__Service_Order__c();	
		insert workorder;
    	
    	Test.startTest();
    	
    	SVMXC__RMA_Shipment_Order__c shipmentOrder = new SVMXC__RMA_Shipment_Order__c();
    	shipmentOrder.SVMXC__Company__c = acc.Id;
    	shipmentOrder.SVMXC__Contact__c = cnt.Id;
    	shipmentOrder.SVMXC__Service_Order__c = workorder.Id;
    	shipmentOrder.SVMXC__Order_Status__c = 'Open';
    	shipmentOrder.SVMXC__Expected_Receive_Date__c = Date.today();
    	shipmentOrder.RecordTypeId = [Select Id from RecordType where SObjectType = 'SVMXC__RMA_Shipment_Order__c' AND Name = 'Shipment'].Id;
    	insert shipmentOrder;
    	
    	Test.stopTest();
    	
    	shipmentOrder = [Select Id, Account_Country__c from SVMXC__RMA_Shipment_Order__c where Id = :shipmentOrder.Id];
    	
    	System.assert(shipmentOrder.Account_Country__c == 'NETHERLANDS');
    }
}