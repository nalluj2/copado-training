//---------------------------------------------------------------------------------------------------------------------------------------------------
//  Author		:    Bart Caelen
//  Date 		:    24/10/2016
//  Description	:    This class exposes methods to create / update / delete a (Opportunity) Stakeholder Mapping
//---------------------------------------------------------------------------------------------------------------------------------------------------
@RestResource(urlMapping='/OpportunityStakeholderMappingService/*')
global class ws_OpportunityStakeholderMappingService {
    
    @HttpPost
	global static IFOppStakeholderMappingResponse doPost() {
 		
 		RestRequest oRequest = RestContext.request;
			
		System.debug('post called ' + oRequest);
		System.debug('post requestBody ' + oRequest.requestBody);
			
		String tBody = oRequest.requestBody.toString();
		IFOppStakeholderMappingRequest oStakeholderMappingRequest = (IFOppStakeholderMappingRequest)System.Json.deserialize(tBody, IFOppStakeholderMappingRequest.class);
			
		System.debug('post requestBody ' + oStakeholderMappingRequest);
		
		IFOppStakeholderMappingResponse oResponse = new IFOppStakeholderMappingResponse();	
		
		try{
			clsUtil.bubbleException();
				
			Stakeholder_Mapping__c oStakeholderMapping = oStakeholderMappingRequest.oppStakeholderMapping;
			oResponse.oppStakeholderMapping = bl_StakeholderMapping.saveStakeholderMapping(oStakeholderMapping);
			oResponse.id = oStakeholderMappingRequest.id;
			oResponse.success = true;

		}catch (Exception oEX){
			
			oResponse.message = oEX.getMessage();
			oResponse.success = false;
		}
 
		bl_Mobilesync.storeRequest(UserInfo.getUserId(), 'StakeholderMapping', oRequest.requestBody.toString(), System.JSON.serializePretty(oResponse), oResponse.success);
  			
		return oResponse;		
	}

	@HttpDelete
	global static IFOppStakeholderMappingDeleteResponse doDelete(){

		RestRequest oRequest = RestContext.request;
			
		System.debug('post called ' + oRequest);
		System.debug('post requestBody ' + oRequest.requestBody);
			
		String tBody = oRequest.requestBody.toString();
		IFOppStakeholderMappingDeleteResponse oResponse = new IFOppStakeholderMappingDeleteResponse();
		
		IFOppStakeholderMappingDeleteRequest oStakeholderMappingDeleteRequest = (IFOppStakeholderMappingDeleteRequest)System.Json.deserialize(tBody, IFOppStakeholderMappingDeleteRequest.class);
			
		System.debug('post requestBody ' + oStakeholderMappingDeleteRequest);
			
		Stakeholder_Mapping__c oStakeholderMapping = oStakeholderMappingDeleteRequest.oppStakeholderMapping;
		
		List<Stakeholder_Mapping__c> lstStakeholderMapping_Delete = new List<Stakeholder_Mapping__c>();
		
		if ( (oStakeholderMapping != null) && (oStakeholderMapping.mobile_Id__c != null) ){
			lstStakeholderMapping_Delete = [SELECT Id, mobile_id__c FROM Stakeholder_Mapping__c WHERE Mobile_ID__c = :oStakeholderMapping.Mobile_ID__c];
		}
		
		try{

			clsUtil.bubbleException();
			
			if (lstStakeholderMapping_Delete.size() > 0){	
				delete lstStakeholderMapping_Delete;
				oResponse.oppStakeholderMapping = lstStakeholderMapping_Delete.get(0);
				oResponse.id = oStakeholderMappingDeleteRequest.id;
				oResponse.success = true;
			}else{
				oResponse.oppStakeholderMapping = oStakeholderMapping;
				oResponse.id = oStakeholderMappingDeleteRequest.id;
				oResponse.message = 'Stakeholder Mapping not found';
				oResponse.success = true;
			}
			
		}catch (Exception oEX){
			
			oResponse.message = oEX.getMessage();
			oResponse.success = false;
		}
  			
			
		bl_Mobilesync.storeRequest(UserInfo.getUserId(), 'StakeholderMappingDelete', oRequest.requestBody.toString(), System.JSON.serializePretty(oResponse), oResponse.success);
		
		return oResponse;
	}
	
	global class IFOppStakeholderMappingRequest{
		public String id { get;set; }
		public Stakeholder_Mapping__c oppStakeholderMapping { get;set; }
	}
	
	global class IFOppStakeholderMappingResponse{
		public String id { get;set; }
		public Stakeholder_Mapping__c oppStakeholderMapping { get;set; }
		public Boolean success { get;set; }
		public String message { get;set; } 
	}
	
	global class IFOppStakeholderMappingDeleteRequest{
		public String id { get;set; }
		public Stakeholder_Mapping__c oppStakeholderMapping { get;set; }
	}
		
	global class IFOppStakeholderMappingDeleteResponse{
		public String id { get;set; }
		public Stakeholder_Mapping__c oppStakeholderMapping { get;set; }
		public boolean success { get;set; }
		public String message { get;set; }
	}	

}
//---------------------------------------------------------------------------------------------------------------------------------------------------