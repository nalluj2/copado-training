/**
 * @description       : 
 * @author            : tom.h.ansley@medtronic.com
 * @last modified on  : 10-12-2020
 * @last modified by  : tom.h.ansley@medtronic.com
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   07-13-2020   tom.h.ansley@medtronic.com   Initial Version
**/
public with sharing class ctrl_SVMXC_IBTree {
    
    public InstalledProductTree hierarchyTree      {get; set;}
    public SVMXC__Installed_Product__c topInstProd {get; set;}
    public String topIBId 
    {
        get;
    	set
        {
            if (value != null && hierarchyTree == null)
            buildHierarchy(value);
       	}

    }

    public String thisIBId
    {
        get;
    	set
        {
            if (value != null && hierarchyTree == null)
            buildHierarchy(value);
       	}

    }

    private void buildHierarchy(String compId)
    {
        if (compId != null)
        {

            //get the work orders installed product
            topInstProd = [SELECT Id, 
                                 SVMXC__Product_Name__c, 
                                 SVMXC__Serial_Lot_Number__c, 
                                 IB_Component_Type__c, 
                                 Linked_Date__c, 
                                 Valid_From__c,
                                 Last_PM_Date__c,
                                 Installation_Date__c ,
                                 IB_Software_Version__r.Software_Version__c,
                                 SVMXC__Parent__c
                          FROM SVMXC__Installed_Product__c WHERE Id = :compId];

            System.debug(LoggingLevel.DEBUG, 'Top Installed Product - ' + topInstProd);

            Set<Id> instProdIds = new Set<Id>();
            instProdIds.add(topInstProd.Id);
            
            Boolean getMore = true;

            hierarchyTree = new InstalledProductTree(topInstProd);

            System.debug(LoggingLevel.DEBUG, 'Hierarchy Created - ' + hierarchyTree);

            //note that we are using a SOQL inside a do/while. We only have 5 or 6 levels to worry about so should not be a problem....but use with care!
            do {

                //now we need to look for any children of the installed product
                Map<Id, SVMXC__Installed_Product__c> instProds = new Map<Id, SVMXC__Installed_Product__c>([SELECT SVMXC__Product_Name__c, 
                                                                                                                  SVMXC__Serial_Lot_Number__c, 
                                                                                                                  IB_Component_Type__c, 
                                                                                                                  Linked_Date__c, 
                                                                                                                  Valid_From__c,
                                                                                                                  Last_PM_Date__c,
                                                                                                                  Installation_Date__c ,
                                                                                                                  IB_Software_Version__r.Software_Version__c,
                                                                                                                  SVMXC__Parent__c
                                                                                                           FROM SVMXC__Installed_Product__c WHERE SVMXC__Parent__c IN :instProdIds]);

                //if we have children
                if (!instProds.isEmpty())
                {
                    System.debug(LoggingLevel.DEBUG, 'We have children - ' + instProds);
                    
                    //go through the children adding them to the tree
                    for (SVMXC__Installed_Product__c instProd: instProds.values())
                    {
                        hierarchyTree.addInstalledProduct(instProd);
                    }

                    //then get all Ids to look for the childrens children
                    instProdIds = instProds.keySet();

                //if there are no more children then bug out.
                } else {
                    getMore = false;
                }

            } while (getMore);  

        }

    }

    public class InstalledProductTree
    {
        public InstalledProduct topInstalledProduct {get; set;}

        public InstalledProductTree(SVMXC__Installed_Product__c topInstalledProduct)
        {
            this.topInstalledProduct = new InstalledProduct(topInstalledProduct, 0);
        }

        public void addInstalledProduct(SVMXC__Installed_Product__c addedInstProd)
        {
            System.debug(LoggingLevel.DEBUG, 'Adding installed product to hierarchy - ' + addedInstProd);
            topInstalledProduct.addInstalledProduct(addedInstProd);
        }

        public List<InstalledProduct> getFlattenedTree()
        {
            List<InstalledProduct> flattenedTree = new List<InstalledProduct>();
            topInstalledProduct.getHierarchy(flattenedTree);

            return flattenedTree;
        }
    }

    public class InstalledProduct
    {
        public SVMXC__Installed_Product__c installedProduct {get; set;}
        public Integer level                                {get; set;} //identifies the level that this installed product is on.
        public List<InstalledProduct> children              {get; set;} //this installed products children
        public Boolean isWoInstProd                         {get; set;} //identifies if this is the installed product associated to the work order.

        public InstalledProduct(SVMXC__Installed_Product__c instProd, Integer level)
        {
            System.debug(LoggingLevel.DEBUG, 'Adding inst prod - ' + instProd.SVMXC__Product_Name__c + ', at level ' + level);
            children = new List<InstalledProduct>();
            this.level = level;
            this.installedProduct = instProd;
            isWoInstProd = false;
        }

        public String getSpacerStr()
        {
            String spacerStr = '';
            for (Integer i = 0; i < level; i++)
                spacerStr += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

            return spacerStr;
        }

        public void addInstalledProduct(SVMXC__Installed_Product__c addedInstProd)
        {
            //if this installed product is the parent of the added installed product then add as a child.
            if (this.installedProduct.Id == addedInstProd.SVMXC__Parent__c)
            {
                System.debug(LoggingLevel.DEBUG, 'Found the parent!');
                children.add(new InstalledProduct(addedInstProd, this.level + 1));

            //if its not then go through the children looking for its parent
            } else {
                System.debug(LoggingLevel.DEBUG, 'No parent found, going to children!');
                for (InstalledProduct child: children)
                {
                    child.addInstalledProduct(addedInstProd);
                }
            }
        }

        public List<InstalledProduct> getHierarchy(List<InstalledProduct> flattenedTree)
        {
            //always add THIS before looking for children
            flattenedTree.add(this);

            for (InstalledProduct child: children)
            {
                child.getHierarchy(flattenedTree);
            }

            return flattenedTree;
        }
        
    }
}