//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 02-04-2019
//  Description      : Batch APEX to get an overview of the field usage of a specific Object
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
global class ba_Review_Field_Usage implements Database.Batchable<SObject>, Database.Stateful {
	
	global String tSObjectName;
	global List<String> lstFieldName;

	global Integer iCounter;
	global String tlog;
	global List<String> lstEmail_TO = new List<String>{UserInfo.getUserEmail(), 'bart.caelen@medtronic.com'};

	private Integer iRecordCount_Total;

	global Database.QueryLocator start(Database.BatchableContext context) {

		iCounter = 0;
		tlog = '';

		String tSOQL = 'SELECT count() FROM ' + tSObjectName;
		iRecordCount_Total = Database.CountQuery(tSOQL);

		if (lstFieldName == null){
			
			lstFieldName = new List<String>();
			Map<String, Schema.SObjectField> mapField = clsUtil.getFieldsForSobject(tSObjectName);
			System.debug('** mapField (' + mapField.size() + ') : ' + mapField.keySet());
			for (String tFieldName : mapField.keySet()){
				if (tFieldName.endsWith('__c')){
					lstFieldName.add(tFieldName);
				}
			}
	
			lstFieldName.sort();

		}

		if (Test.isRunningTest()){
			// Make sure only 1 Field will be processed
			lstFieldName = new List<String>{lstFieldName[0]};
		}

		// Select a number of accounts that maches the number of processing fields - the batch size will be 1 so we have 1 processing account which will process 1 field
		return Database.getQueryLocator('SELECT Id FROM Account LIMIT ' + lstFieldName.size());

	}

	global void execute(Database.BatchableContext context, List<sObject> lstData){
		
		String tFieldName = lstFieldName[iCounter];
		String tSOQL = 'SELECT count() FROM ' + tSObjectName + ' WHERE ' + tFieldName + ' != null';

		try{

			Integer iRecordCount = Database.CountQuery(tSOQL);
	        tLog += '<BR />' + tFieldName + ' : ' + iRecordCount + ' / ' + iRecordCount_Total;

		}catch(Exception oEX){

        	tLog += '<BR />' + tFieldName + ' : Error : ' + oEX.getMessage();

		}

		iCounter++;
	
	}
	
	global void finish(Database.BatchableContext context) {

		String tEmailBody = tLog;

		Messaging.SingleEmailMessage oEmail = new Messaging.SingleEmailMessage();
			oEmail.setHTMLBody(tEmailBody);
			oEmail.setTargetObjectId(UserInfo.getUserId());
			oEmail.setTreatTargetObjectAsRecipient(false);
			if (lstEmail_TO.size() > 0) oEmail.setTOAddresses(lstEmail_TO);
			oEmail.setSubject('ba_Review_Field_Population : ' + tSObjectName);
			oEmail.setSaveAsActivity(false);
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { oEmail });

	}

}
//--------------------------------------------------------------------------------------------------------------------