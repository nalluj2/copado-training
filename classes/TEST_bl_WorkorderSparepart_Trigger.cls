//----------------------------------------------------------------------------------------------------------------------------------------------------
//  Author      :   Bart Caelen
//  Date        :   01-11-2016
//  Description :   APEX TEST Class for the APEX Trigger tr_WorkorderSparepart and APEX Class bl_WorkorderSparepart_Trigger
//  Change Log  :    
//----------------------------------------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_bl_WorkorderSparepart_Trigger {
	
	@isTest static void createTestData() {

		// Product
		List<Product2> lstProduct = new List<Product2>();
		Product2 oProduct1 = new Product2();
			oProduct1.Name = 'PoleStar1';
			oProduct1.ProductCode = '000999';
			oProduct1.isActive = true;					
		lstProduct.add(oProduct1);
		Product2 oProduct2 = new Product2();
			oProduct2.Name = 'PoleStar';
			oProduct2.ProductCode = '009999';
			oProduct2.isActive = true;					
		lstProduct.add(oProduct2);
		insert lstProduct;

		// Account
		Account oAccount = new Account();	
			oAccount.AccountNumber = '0000000';
			oAccount.Name = 'Test Account';
			oAccount.Phone = '+123456789';
			oAccount.BillingCity = 'Minneapolis';
			oAccount.BillingCountry = 'UNITED STATES';
			oAccount.BillingState = 'Minnesota';
			oAccount.BillingStreet = 'street';
			oAccount.BillingPostalCode = '123456';
			oAccount.ST_NAV_Non_SAP_Account__c = true;
			oAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non-SAP Account').getRecordTypeId();	
		insert oAccount;
		
		// Contact
		Contact oContact = new Contact();
			oContact.FirstName = 'Test';
			oContact.LastName = 'Contact';
			oContact.Phone = '+123456789';		
			oContact.Email = 'test.contact@gmail.com';
			oContact.MobilePhone = '+123456789';		
			oContact.MailingCity = 'Minneapolis';
			oContact.MailingCountry = 'UNITED STATES';
			oContact.MailingState = 'Minnesota';
			oContact.MailingStreet = 'street';
			oContact.MailingPostalCode = '123456';
			oContact.External_Id__c = 'Test_Contact_Id';
			oContact.Contact_Department__c = 'Cardiology';
			oContact.Contact_Primary_Specialty__c = 'Cardiovascular'; 
			oContact.Affiliation_To_Account__c = 'Employee'; 
			oContact.Primary_Job_Title_vs__c = 'Surgeon'; 
			oContact.Contact_Gender__c = 'Male'; 
			oContact.AccountId = oAccount.Id;
			oContact.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Generic Contact').getRecordTypeId();				
		insert oContact;
		
		// Asset
		List<Asset> lstAsset = new List<Asset>();
		Asset oAsset1 = new Asset();
			oAsset1.AccountId = oAccount.Id;
			oAsset1.Product2Id = oProduct1.Id;
			oAsset1.Asset_Product_Type__c = 'S8 Planning Station';
			oAsset1.Ownership_Status__c = 'Purchased';
			oAsset1.Name = '123456789';
			oAsset1.Serial_Nr__c = '123456789';
			oAsset1.Status = 'Installed';
			oAsset1.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('PoleStar').getRecordTypeId();
		lstAsset.add(oAsset1);
		Asset oAsset2 = new Asset();
			oAsset2.AccountId = oAccount.Id;
			oAsset2.Product2Id = oProduct2.Id;
			oAsset2.Asset_Product_Type__c = 'PoleStar N-10';
			oAsset2.Ownership_Status__c = 'Purchased';
			oAsset2.Name = '987654321';
			oAsset2.Serial_Nr__c = '987654321';
			oAsset2.Status = 'Installed';
			oAsset2.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('PoleStar').getRecordTypeId();
		lstAsset.add(oAsset2);
		insert lstAsset;
				
		// Case
		List<Case> lstCase = new List<Case>();
		Case oCase1 = new Case();
			oCase1.AccountId = oAccount.Id;
			oCase1.AssetId = oAsset1.Id;
			oCase1.RecordTypeId = clsUtil.getRecordTypeByDevName('Case', 'New_Case').Id;
			oCase1.External_Id__c = 'Test_Case_Id_1';
		lstCase.add(oCase1);
		Case oCase2 = new Case();
			oCase2.AccountId = oAccount.Id;
			oCase2.AssetId = oAsset2.Id;
			oCase2.RecordTypeId = clsUtil.getRecordTypeByDevName('Case', 'New_Case').Id;
			oCase2.External_Id__c = 'Test_Case_Id_2';
		lstCase.add(oCase2);
		insert lstCase;
	
		// Workorder
		List<Workorder__c> lstWorkorder = new List<Workorder__c>();
		Workorder__c oWorkorder1 = new Workorder__c();
			oWorkorder1.Account__c = oAccount.Id;
			oWorkorder1.Asset__c = oAsset1.Id;
			oWorkorder1.Status__c = 'In Process';
			oWorkorder1.External_Id__c = 'Test_Work_Order_Id_1';
			oWorkorder1.RecordTypeId = Schema.SObjectType.Workorder__c.getRecordTypeInfosByName().get('PoleStar System Checkout').getRecordTypeId();
		lstWorkorder.add(oWorkorder1);
		Workorder__c oWorkorder2 = new Workorder__c();
			oWorkorder2.Account__c = oAccount.Id;
			oWorkorder2.Asset__c = oAsset2.Id;
			oWorkorder2.Status__c = 'In Process';
			oWorkorder2.External_Id__c = 'Test_Work_Order_Id_2';
			oWorkorder2.RecordTypeId = Schema.SObjectType.Workorder__c.getRecordTypeInfosByName().get('PoleStar System Checkout').getRecordTypeId();
		lstWorkorder.add(oWorkorder2);
		insert lstWorkorder;
		
		// Workorder Sparepart
		List<Workorder_Sparepart__c> lstWorkorderSparepart = new List<Workorder_Sparepart__c>();
		Workorder_Sparepart__c oWorkorderSparepart1 = new Workorder_Sparepart__c();
			oWorkorderSparepart1.Case__c = oCase1.Id;
			oWorkorderSparepart1.Account__c = oAccount.Id;
			oWorkorderSparepart1.Asset__c = oAsset1.Id;
			oWorkorderSparepart1.External_Id__c = 'Test_SparePart_Id_1';		
			oWorkorderSparepart1.Amount_of_Inaccuracy__c = 1000;
			oWorkorderSparepart1.RecordTypeId = Schema.SObjectType.Workorder_Sparepart__c.getRecordTypeInfosByName().get('Inaccuracy').getRecordTypeId();
			oWorkorderSparepart1.Return_Item_Status__c = 'Part Not Returned';
			oWorkorderSparepart1.Order_Status__c = 'Shipped';
			oWorkorderSparepart1.Order_Lot_Number__c = '0011223344';
		lstWorkorderSparepart.add(oWorkorderSparepart1);
		Workorder_Sparepart__c oWorkorderSparepart2 = new Workorder_Sparepart__c();
			oWorkorderSparepart2.Case__c = oCase2.Id;
			oWorkorderSparepart2.Account__c = oAccount.Id;
			oWorkorderSparepart2.Asset__c = oAsset2.Id;
			oWorkorderSparepart2.External_Id__c = 'Test_SparePart_Id_2';		
			oWorkorderSparepart2.Amount_of_Inaccuracy__c = 1000;
			oWorkorderSparepart2.RecordTypeId = Schema.SObjectType.Workorder_Sparepart__c.getRecordTypeInfosByName().get('Inaccuracy').getRecordTypeId();
			oWorkorderSparepart2.Return_Item_Status__c = 'Part Not Returned';
			oWorkorderSparepart2.Order_Status__c = 'Shipped';
			oWorkorderSparepart2.Order_Lot_Number__c = '4433221100';
		lstWorkorderSparepart.add(oWorkorderSparepart2);		
		insert lstWorkorderSparepart;


	}
	
	@isTest static void test_updateRelatedAsset_Insert() {
		
		//---------------------------------------------
		// Create Test Data		
		//---------------------------------------------
		//---------------------------------------------


		//---------------------------------------------
		// Test
		//---------------------------------------------
		Test.startTest();

		createTestData();

		Test.stopTest();
		//---------------------------------------------


		//---------------------------------------------
		// Validation
		//---------------------------------------------
		List<Workorder_Sparepart__c> lstWorkorderSparepart = [SELECT Id, External_Id__c, Asset_Product_Type__c, Order_Lot_Number__c, Asset__r.Computer_Serial_Number__c FROM Workorder_Sparepart__c];
		for (Workorder_Sparepart__c oWorkorderSparepart : lstWorkorderSparepart){

			if (oWorkorderSparepart.External_Id__c == 'Test_SparePart_Id_1'){

				System.assertEquals(oWorkorderSparepart.Asset_Product_Type__c, 'S8 Planning Station');
				System.assertEquals(oWorkorderSparepart.Order_Lot_Number__c, oWorkorderSparepart.Asset__r.Computer_Serial_Number__c, '0011223344');

			}else if (oWorkorderSparepart.External_Id__c == 'Test_SparePart_Id_2'){

				System.assertNotEquals(oWorkorderSparepart.Asset_Product_Type__c, 'S8 Planning Station');
				System.assertEquals(oWorkorderSparepart.Order_Lot_Number__c, '4433221100');
				System.assertEquals(oWorkorderSparepart.Asset__r.Computer_Serial_Number__c, null);

			}

		}
		//---------------------------------------------

	}
	

	@isTest static void test_updateRelatedAsset_Update() {

		//---------------------------------------------
		// Create Test Data		
		//---------------------------------------------
		createTestData();
		List<Workorder_Sparepart__c> lstWorkorderSparepart = [SELECT Id, External_Id__c, Asset_Product_Type__c, Order_Lot_Number__c, Asset__r.Computer_Serial_Number__c FROM Workorder_Sparepart__c];
		for (Workorder_Sparepart__c oWorkorderSparepart : lstWorkorderSparepart){

			if (oWorkorderSparepart.External_Id__c == 'Test_SparePart_Id_1'){

				System.assertEquals(oWorkorderSparepart.Asset_Product_Type__c, 'S8 Planning Station');
				System.assertEquals(oWorkorderSparepart.Order_Lot_Number__c, oWorkorderSparepart.Asset__r.Computer_Serial_Number__c, '0011223344');

			}else if (oWorkorderSparepart.External_Id__c == 'Test_SparePart_Id_2'){

				System.assertNotEquals(oWorkorderSparepart.Asset_Product_Type__c, 'S8 Planning Station');
				System.assertEquals(oWorkorderSparepart.Order_Lot_Number__c, '4433221100');
				System.assertEquals(oWorkorderSparepart.Asset__r.Computer_Serial_Number__c, null);

			}

		}
		//---------------------------------------------


		//---------------------------------------------
		// Test
		//---------------------------------------------
		Test.startTest();

		for (Workorder_Sparepart__c oWorkorderSparepart : lstWorkorderSparepart){
			oWorkorderSparepart.Order_Lot_Number__c = '9988776655';
		}
		update lstWorkorderSparepart;

		Test.stopTest();
		//---------------------------------------------


		//---------------------------------------------
		// Validation
		//---------------------------------------------
		lstWorkorderSparepart = [SELECT Id, External_Id__c, Asset_Product_Type__c, Order_Lot_Number__c, Asset__r.Computer_Serial_Number__c FROM Workorder_Sparepart__c];
		for (Workorder_Sparepart__c oWorkorderSparepart : lstWorkorderSparepart){

			if (oWorkorderSparepart.External_Id__c == 'Test_SparePart_Id_1'){
		
				System.assertEquals(oWorkorderSparepart.Asset_Product_Type__c, 'S8 Planning Station');
				System.assertEquals(oWorkorderSparepart.Order_Lot_Number__c, oWorkorderSparepart.Asset__r.Computer_Serial_Number__c, '9988776655');
		
			}else if (oWorkorderSparepart.External_Id__c == 'Test_SparePart_Id_2'){
		
				System.assertNotEquals(oWorkorderSparepart.Asset_Product_Type__c, 'S8 Planning Station');
				System.assertEquals(oWorkorderSparepart.Order_Lot_Number__c, '9988776655');
				System.assertEquals(oWorkorderSparepart.Asset__r.Computer_Serial_Number__c, null);
		
			}
		
		}
		//---------------------------------------------

	}

}
//----------------------------------------------------------------------------------------------------------------------------------------------------