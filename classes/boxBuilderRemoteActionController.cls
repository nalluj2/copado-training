global without sharing class boxBuilderRemoteActionController {
/*
    public boxBuilderRemoteActionController(ngForceController controller) {

    }
        
    @RemoteAction
    global static void requestProductKitable(Id prodId) {
    	
    	Product2 prod = new Product2();
    	prod.KitableIndex__c = 'Approval Pending';
    	prod.Id = prodId;
    	
    	update prod;	
    }
    
    @RemoteAction
    global static  User requestUser() {

        User oUser = [
            SELECT
                Id, Name,  Profile.Name, Manager.FirstName, Manager.LastName, Job_Title__c, Country__c, LanguageLocaleKey, LocaleSidKey
            FROM User
            WHERE Id = :UserInfo.getUserId()
        ];

        return oUser;
        //return JSON.serialize(oUser);
    }
    
    @RemoteAction
    global static String matchingConfigurations(String boxId, String country){
    	
    	Procedural_Solutions_Kit__c boxConfig = [Select Id, (Select Id, Product__c, Quantity__c from Procedural_Solutions_Kit_Products__r ORDER BY Product__c ASC) from Procedural_Solutions_Kit__c where Id = :boxId];
    	    	    			
		String configHash = null;
		String configHashGlobal = null;
		    	    	    			
    	if(boxConfig.Procedural_Solutions_Kit_Products__r.size() > 0){
    				
    		List<String> config = new List<String>();
    				
    		for(Procedural_Solutions_Kit_Product__c boxProd : boxConfig.Procedural_Solutions_Kit_Products__r){
    					
    			config.add(boxProd.Product__c + ':' + boxProd.Quantity__c);
    		}
    	   	
    	   	String configStringGlobal = String.join(config, ';');
    		String configString = country + '-' + configStringGlobal;
    		    	   				
    		configHash = EncodingUtil.convertToHex(Crypto.generateDigest('SHA-256', Blob.valueOf(configString)));
    		configHashGlobal = EncodingUtil.convertToHex(Crypto.generateDigest('SHA-256', Blob.valueOf(configStringGlobal)));    				
    	}
    	    	    	
    	if(configHash != null){
    	 		
    		List<Procedural_Solutions_Kit__c> sameConfigGlobal = [Select Id, Procedure__c, Country__c, Opportunity__r.Account.Name, Owner.Name, Box_Description__c, Submission_Date__c from Procedural_Solutions_Kit__c where Configuration_Hash_Global__c = :configHashGlobal];
    		
    		List<Procedural_Solutions_Kit__c> sameConfig = [Select Id from Procedural_Solutions_Kit__c where Configuration_Hash__c = :configHash];
    		
    		String matchingConfigs = 'There are ' + sameConfigGlobal.size() + ' (' + sameConfig.size() + ' in your country) Boxes with the same configuration. <br><br>';
    		
    		if(sameConfigGlobal.size() > 0){
	    		
	    		matchingConfigs += '<table style="width : 100%; text-align:left; table-layout : fixed; border-spacing: 5px; border-collapse: separate;"><tr><th width="30%"><b>Description</b></th><th width="20%"><b>Procedure</b></th><th width="15%"><b>Submitted Date</b></th><th width="15%"><b>Account</b></th><th width="10%"><b>Country</b></th><th width="10%"><b>Owner</b></th></tr>';
	    		
	    		for(Procedural_Solutions_Kit__c matchingBox : sameConfigGlobal){
	    			
	    			matchingConfigs += '<tr>';
	    			matchingConfigs += '<td>' + matchingBox.Box_Description__c + '</td>';	    			
	    			matchingConfigs += '<td >' + matchingBox.Procedure__c + '</td>';
	    			matchingConfigs += '<td >' + (matchingBox.Submission_Date__c != null ? matchingBox.Submission_Date__c.format() : '') + '</td>';
	    			matchingConfigs += '<td >' + (matchingBox.Opportunity__r != null ? matchingBox.Opportunity__r.Account.Name : '-') + '</td>';
	    			matchingConfigs += '<td >' + matchingBox.Country__c + '</td>';
	    			matchingConfigs += '<td >' + matchingBox.Owner.Name + '</td>';
	    			matchingConfigs += '</tr>';
	    		}
    		
    			matchingConfigs += '</table>';
    		}
    		
    		return matchingConfigs;
    	}
    	
    	return 'You need to have at least 1 product in the basket in order to match the configuration.';
    }

    @RemoteAction
    global static void requestProductCountryAvailable(Id prodId, Id countryId) {
		
		Product_Country_Availability__c prodCountry = new Product_Country_Availability__c();
      	prodCountry.Product__c = prodId;
      	prodCountry.Country__c = countryId;
      	prodCountry.Status__c = 'Approval Pending';
      	prodCountry.Unique_Key__c = prodId + ':' + countryId;
      
      	upsert prodCountry Unique_Key__c; 
    }
    
    @RemoteAction
    global static void sendShareEmail(Map<String,String> params){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        String target_object_id = params.get('target_object_id');
        String what_id = params.get('what_id');
        String template_id = params.get('template_id');
        
        mail.setTargetObjectId(target_object_id);
        mail.setWhatId(what_id);
        mail.setTemplateId(template_id);
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    */
}