public with sharing class ctrl_LW_Sonar {
    
    public String level{get; set;}  
    public String view {get; set;}  
    public List<SelectOption> viewOptions {get; private set;}
    public List<String> buFilter {get; set;}
    public List<SelectOption> buOptions {get; private set;}
    public List<String> sbuFilter {get; set;}   
    public List<SelectOption> sbuOptions {get; private set;}
    public List<String> therapyFilter {get; set;}
    public List<SelectOption> therapyOptions {get; private set;}
    public List<String> countryFilter {get; set;}
    public List<SelectOption> countryOptions {get; private set;}
    public List<String> districtFilter {get; set;}
    public List<SelectOption> districtOptions {get; private set;}
    public List<String> territoryFilter {get; set;}
    public List<SelectOption> territoryOptions {get; private set;}
    public List<String> segmentFilter {get; set;}
    public List<SelectOption> segmentOptions {get; private set;}
        
    public String lwURL {get; private set;}
    public String filterSummary {get; private set;}
    private Map<String, String> valueLabels;
    private String baseURL {get; set;}
    public String cur {get; set;}
    public List<SelectOption> currencyOptions {get; private set;}
    public String fiscalPeriod {get; set;}
    public List<SelectOption> fpOptions {get; private set;}
    public String format {get; set;}
    
    private Map<String, List<SelectOption>> defaultViewsByLevel = new Map<String, List<SelectOption>>();
    private Map<String, String> levelCodes = new Map<String, String>();
    
    //User Profile
    private Set<Id> userBUGs = new Set<Id>(); //-BC - 20150609 - CR-8652 - We store the Id because this filtering is only used on the controller and will not be visible on the VF page
    private Set<String> userBUs = new Set<String>(); //We store the names because independenlty of the User Region we will finally send the Ids of BU and SBU of Europe
    private Set<String> userSBUs = new Set<String>(); //We store the names because independenlty of the User Region we will finally send the Ids of BU and SBU of Europe
    private Set<Id> userTerritories = new Set<Id>();
    private String userCurrency;
    
    /*
    * Constructor
    */
    public ctrl_LW_Sonar(){
        
        User currentUser = [Select Id, BI_Currency__c, (Select Business_Unit_Text__c, Sub_Business_Unit_name__c, Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__c from User_Business_Units1__r ) from User where Id=:UserInfo.getUserId()]; //-BC - 20150609 - CR-8652 - Added Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__c to the SOQL
        
        //Get User BU information for its possible use in Filters
        for(User_Business_Unit__c uBU : currentUser.User_Business_Units1__r){
            userBUGs.add(uBU.Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__c);     //-BC - 20150609 - CR-8652            
        }
        
        //All BUs and sBUs of the User's BUG are available in the Sonar panel (CR-8652)
        for(Sub_Business_Units__c sbu : [Select Name, Business_Unit__r.Name from Sub_Business_Units__c where Business_Unit__r.Business_Unit_Group__c IN :userBUGs]){
        	
        	userBUs.add(sbu.Business_Unit__r.Name);
        	userSBUs.add(sbu.Name);
        }
        
        //Get User Territory information for its possible use in Filters
        for(UserTerritory2Association uTer : [Select Territory2Id from UserTerritory2Association where UserId =:UserInfo.getUserId()]){
            
            userTerritories.add(uTer.Territory2Id);          
        }
        
        //User default currency
        cur = currentUser.BI_Currency__c;
        userCurrency  = currentUser.BI_Currency__c;
        
        getValueLabels();               
        generateDefaultViewsMap();
        generateCurrencyOptions();
        generateFiscalYearOptions();
        loadConfiguration();
        
        //Load stored config if any
        List<LaunchWorks_view_config__c> config = [Select Config_JSON__c from LaunchWorks_view_config__c where User__c =: UserInfo.getUserId() LIMIT 1];
        
        if(config.size()>0){
            
            LWConfiguration viewConfig = (LWConfiguration) JSON.deserialize(config[0].Config_JSON__c, LWConfiguration.class);
            
            level = viewConfig.level;
            view = viewConfig.view;
            cur = viewConfig.cur;
            fiscalPeriod = viewConfig.fiscalPeriod;
            buFilter = viewConfig.buFilter;
            sbuFilter = viewConfig.sbuFilter;
            therapyFilter = viewConfig.therapyFilter;
            countryFilter = viewConfig.countryFilter;
            districtFilter = viewConfig.districtFilter;
            territoryFilter = viewConfig.territoryFilter;
            segmentFilter = viewConfig.segmentFilter;
            
            loadOptions();
        }else{
            
            level = 'geography';
            levelSelected();
        }
    }
                
    public void generateLwURL(){
        
        filterSummary = '<b>View</b> = <label style="color : blue;">'+valueLabels.get(level)+'</label> / <label style="color : blue;">'+valueLabels.get(view)+'</label>';               
        String url = baseURL;
                
        if(level == 'segment' && view == 'A') url += '&iDocID='+levelCodes.get('account');
        else url += '&iDocID='+levelCodes.get(level);
                
        url += '&lsSpsDefaultView=' + view;
        url += '&lsSpsCurrency=' + cur;
        url += '&lsSpsSelect%20Fiscal%20Period=' + fiscalPeriod;
        url += '&sOutputFormat='+format;
        
        if(format == 'P' || format == 'E'){
        	
        	url = url.replace('&noDetailsPanel=true', '');
        	url = url.replace('&mode=Report', '');        	
        }
        
        filterSummary += ' > <b>Fiscal Period</b> = <label style="color : blue;">'+valueLabels.get(fiscalPeriod)+'</label>';
        
        List<String> selectedCountries = filterDiabetesCountries();
                        
        if(level == 'geography'){
            
            if(!selectedCountries.isEmpty()){
                url += '&lsMCountry%20SFDC%20Id='+listToCSVString(selectedCountries);  
                if(selectedCountries.size()==1){
                    filterSummary += ' > <b>Country</b> = <label style="color : blue;">'+   idToName(countryFilter[0], 'Territory2') + '</label>';
                }else{
                    filterSummary += ' > <b>Countries</b> = <label style="color : blue;">'+ selectedCountries.size() + ' values</label>';
                }
            }else{
                url += '&lsMCountry%20SFDC%20Id=ALL';
            }
            if(!buFilter.isEmpty()){
                url += '&lsMBusiness%20Unit%20Id='+listToCSVString(buFilter);
                if(buFilter.size()==1){
                    filterSummary += ' > <b>Business Unit</b> = <label style="color : blue;">'+ idToName(buFilter[0], 'Business_Unit__c') + '</label>';
                }else{
                    filterSummary += ' > <b>Business Units</b> = <label style="color : blue;">'+ buFilter.size() + ' values</label>';
                }
            }else{
                url += '&lsMBusiness%20Unit%20Id=ALL';
            }
            if(!sbuFilter.isEmpty()){
                url += '&lsMSub%20Business%20Unit%20Id='+listToCSVString(sbuFilter);
                if(sbuFilter.size()==1){
                    filterSummary += ' > <b>Sub-Business Unit</b> = <label style="color : blue;">'+ idToName(sbuFilter[0], 'Sub_Business_Units__c') + '</label>';
                }else{
                    filterSummary += ' > <b>Sub-Business Units</b> = <label style="color : blue;">'+ sbuFilter.size() + ' values</label>';
                }
            }else{
                url += '&lsMSub%20Business%20Unit%20Id=ALL';
            }
            if(!therapyFilter.isEmpty()){
                url += '&lsMTherapy%20Id='+listToCSVString(therapyFilter);
                if(therapyFilter.size()==1){
                    filterSummary += ' > <b>Therapy</b> = <label style="color : blue;">'+   idToName(therapyFilter[0], 'Therapy__c') + '</label>';
                }else{
                    filterSummary += ' > <b>Therapies</b> = <label style="color : blue;">'+ therapyFilter.size() + ' values</label>';
                }
            }else{
                url += '&lsMTherapy%20Id=ALL';
            }
            
        }else if(level == 'therapy'){
            
            if(!selectedCountries.isEmpty()){
                url += '&lsMCountry%20SFDC%20Id='+listToCSVString(selectedCountries);  
                if(selectedCountries.size()==1){
                    filterSummary += ' > <b>Country</b> = <label style="color : blue;">'+   idToName(countryFilter[0], 'Territory2') + '</label>';
                }else{
                    filterSummary += ' > <b>Countries</b> = <label style="color : blue;">'+ selectedCountries.size() + ' values</label>';
                }
            }else{
                url += '&lsMCountry%20SFDC%20Id=ALL';
            }
            if(!districtFilter.isEmpty()){
                url += '&lsMDistrict%20SFDC%20Id='+listToCSVString(districtFilter);
                if(districtFilter.size()==1){
                    filterSummary += ' > <b>District</b> = <label style="color : blue;">'+  idToName(districtFilter[0], 'Territory2') + '</label>';
                }else{
                    filterSummary += ' > <b>Districts</b> = <label style="color : blue;">'+ districtFilter.size() + ' values</label>';
                }
            }else{
                url += '&lsMDistrict%20SFDC%20Id=ALL';
            }
            if(!territoryFilter.isEmpty()){
                url += '&lsMTerritory%20SFDC%20Id='+listToCSVString(territoryFilter);
                if(territoryFilter.size()==1){
                    filterSummary += ' > <b>Territory</b> = <label style="color : blue;">'+ idToName(territoryFilter[0], 'Territory2') + '</label>';
                }else{
                    filterSummary += ' > <b>Territories</b> = <label style="color : blue;">'+ territoryFilter.size() + ' values</label>';
                }
            }else{
                url += '&lsMTerritory%20SFDC%20Id=ALL';
            }
            if(!therapyFilter.isEmpty()){
                url += '&lsMTherapy%20Id='+listToCSVString(therapyFilter);
                if(therapyFilter.size()==1){
                    filterSummary += ' > <b>Therapy</b> = <label style="color : blue;">'+   idToName(therapyFilter[0], 'Therapy__c') + '</label>';
                }else{
                    filterSummary += ' > <b>Therapies</b> = <label style="color : blue;">'+ therapyFilter.size() + ' values</label>';
                }
            }else{
                url += '&lsMTherapy%20Id=ALL';
            }
        
        }else if(level == 'segment'){
            
            if(!selectedCountries.isEmpty()){
                url += '&lsMCountry%20SFDC%20Id='+listToCSVString(selectedCountries);  
                if(selectedCountries.size()==1){
                    filterSummary += ' > <b>Country</b> = <label style="color : blue;">'+   idToName(countryFilter[0], 'Territory2') + '</label>';
                }else{
                    filterSummary += ' > <b>Countries</b> = <label style="color : blue;">'+ selectedCountries.size() + ' values</label>';
                }
            }else{
                url += '&lsMCountry%20SFDC%20Id=ALL';
            }   
            if(!segmentFilter.isEmpty()){
                url += '&lsMSegment='+EncodingUtil.urlEncode( listToCSVString(segmentFilter), 'UTF-8');
                if(segmentFilter.size()==1){
                    filterSummary += ' > <b>Segment</b> = <label style="color : blue;">'+ segmentFilter[0]+ '</label>';
                }else{
                    filterSummary += ' > <b>Segments</b> = <label style="color : blue;">'+ segmentFilter.size() + ' values</label>';
                }
            }else{
                url += '&lsMSegment=ALL';
            }               
            if(!districtFilter.isEmpty()){
                url += '&lsMDistrict%20SFDC%20Id='+listToCSVString(districtFilter);
                if(districtFilter.size()==1){
                    filterSummary += ' > <b>District</b> = <label style="color : blue;">'+  idToName(districtFilter[0], 'Territory2') + '</label>';
                }else{
                    filterSummary += ' > <b>Districts</b> = <label style="color : blue;">'+ districtFilter.size() + ' values</label>';
                }
            }else{
                url += '&lsMDistrict%20SFDC%20Id=ALL';
            }
            if(!territoryFilter.isEmpty()){
                url += '&lsMTerritory%20SFDC%20Id='+listToCSVString(territoryFilter);
                if(territoryFilter.size()==1){
                    filterSummary += ' > <b>Territory</b> = <label style="color : blue;">'+ idToName(territoryFilter[0], 'Territory2') + '</label>';
                }else{
                    filterSummary += ' > <b>Territories</b> = <label style="color : blue;">'+ territoryFilter.size() + ' values</label>';
                }
            }else{
                url += '&lsMTerritory%20SFDC%20Id=ALL';
            }
            if(!therapyFilter.isEmpty()){
                url += '&lsMTherapy%20Id='+listToCSVString(therapyFilter);
                if(therapyFilter.size()==1){
                    filterSummary += ' > <b>Therapy</b> = <label style="color : blue;">'+   idToName(therapyFilter[0], 'Therapy__c') + '</label>';
                }else{
                    filterSummary += ' > <b>Therapies</b> = <label style="color : blue;">'+ therapyFilter.size() + ' values</label>';
                }
            }else{
                url += '&lsMTherapy%20Id=ALL';
            }                  
        }
        
        /*if(format == 'PDF' || format == 'XLS'){
            url+= '&popup=Y';
        }*/
                    
        lwURL =  url;
    }
    
    private String listToCSVString(List<String> inputList){
        
        String result = '';
        
        for(String s : inputList){
            result += (result==''?'':';')+s;
        }
        
        return result;
    }
    
    private String idToName(String recordId, String objectName){
        
        String result = '';
        
        List<SObject> objects = Database.query('Select Id, Name from '+objectName+' where Id =\''+recordId+'\'');
        
        if(objects.size()>0) result = String.valueOf(objects[0].get('Name'));
        
        return result;
    }
    
    private List<String> filterDiabetesCountries(){
    	
    	if(countryFilter.isEmpty()) return new List<String>();
    	
    	Set<String> filteredCountries = new Set<String>();
    	
    	Map<Id, Territory2> countryFilterTerritories = new Map<Id, Territory2>([Select Id, Territory2Type.DeveloperName, Business_Unit__c, Country_UID__c from Territory2 where Id IN :countryFilter]); 
    	
    	Set<Id> diabetesTerritories = new Set<Id>();
    	Set<String> diabetesCountries = new Set<String>();
    	
    	for(String terrId : countryFilter){
    		
    		Territory2 terr = countryFilterTerritories.get(terrId);
    		
    		if(terr.Business_Unit__c == 'Diabetes'){
    			diabetesTerritories.add(terrId);
    			diabetesCountries.add(terr.Country_UID__c);
    		}else{
    			filteredCountries.add(terrId);
    		}
    	}
    	
    	//If there are territories for the Diabetes BU
    	if(diabetesTerritories.size() > 0){
    		
    		//Search for the equivalent Non-Diabetes Country Territories
    		Map<String, Territory2> nonDiabetesCountries = new Map<String, Territory2>();
    		
    		for(Territory2 nonDiabetesCountry : [Select Id, Country_UID__c from Territory2 where Territory2Type.DeveloperName = 'Country' AND Business_Unit__c != 'Diabetes' AND Country_UID__c IN :diabetesCountries]){
    			
    			nonDiabetesCountries.put(nonDiabetesCountry.Country_UID__c , nonDiabetesCountry);
    		}
    		
    		//If found, use the Id of the non-diabetes territory
    		for(String terrId : diabetesTerritories){
    			
    			Territory2 diabTerritory = countryFilterTerritories.get(terrId);
    			Territory2 nonDiabTerritory = nonDiabetesCountries.get(diabTerritory.Country_UID__c);
    			
    			if(nonDiabTerritory!= null) filteredCountries.add(nonDiabTerritory.Id);
    		}
    	}
    	    	    	
    	return new List<String>(filteredCountries);
    }
    
    public void clearConfig(){
                
        view = null;        
        viewOptions = new List<SelectOption>();
        buFilter = null;
        sbuFilter = null;
        therapyFilter = null;
        countryFilter = null;
        districtFilter = null;
        territoryFilter = null;
        segmentFilter = null;
        cur = userCurrency;
        fiscalPeriod = null;
        
        level = 'geography';
        levelSelected();
    }
    
    public void saveConfig(){
        
        LWConfiguration viewConfig = new LWConfiguration();
        viewConfig.level = level;
        viewConfig.view = view;
        viewConfig.cur = cur;
        viewConfig.fiscalPeriod = fiscalPeriod;
        viewConfig.buFilter = buFilter;
        viewConfig.sbuFilter = sbuFilter;
        viewConfig.therapyFilter = therapyFilter;
        viewConfig.countryFilter = countryFilter;
        viewConfig.districtFilter = districtFilter;
        viewConfig.territoryFilter = territoryFilter;
        viewConfig.segmentFilter = segmentFilter;       
        
        LaunchWorks_view_config__c config = new LaunchWorks_view_config__c();
        config.User__c = UserInfo.getUserId();
        config.Unique_Key__c = UserInfo.getUserId();
        config.Config_JSON__c = JSON.serialize(viewConfig);
        upsert config Unique_Key__c;
    }
    
    public void levelSelected(){
            
        buFilter = new List<String>(); 
        sbuFilter = new List<String>();
        therapyFilter = new List<String>();
        countryFilter = new List<String>(); 
        districtFilter = new List<String>();
        territoryFilter = new List<String>();
        segmentFilter = new List<String>();
        
        loadOptions();
    }
    
    private void loadOptions(){
        
        viewOptions = defaultViewsByLevel.get(level);
        
        if(level == 'geography'){       
            
            loadCountryOptions();
            loadBUOptions();
            loadSBUOptions();
            loadTherapyOptions();
            
        }else if(level == 'therapy'){
                        
            loadCountryOptions();
            loadDistrictOptions();
            loadTerritoryOptions();
            loadTherapyOptions();
            
        }else if(level == 'segment'){
            
            loadCountryOptions();                        
            loadDistrictOptions();
            loadTerritoryOptions();
            loadTherapyOptions();
            loadSegmentOptions();            
        }       
    }
    
    public void businessUnitSelected(){
        
        sbuFilter = new List<String>();
        therapyFilter = new List<String>();
        
        loadSBUOptions();
        loadTherapyOptions();       
    }
    
    public void subBusinessUnitSelected(){
        
        therapyFilter = new List<String>();     
        loadTherapyOptions();       
    }
    
    public void countrySelected(){
        
        districtFilter = new List<String>();
        territoryFilter = new List<String>();
        buFilter = new List<String>();
        sbuFilter = new List<String>();
        therapyFilter = new List<String>();
        
        if(level == 'geography'){
        	loadBUOptions();
        	loadSBUOptions();
        }else{
        	loadDistrictOptions();
        	loadTerritoryOptions();
        }  
        
        loadTherapyOptions();   
    }
    
    public void districtSelected(){
        
        territoryFilter = new List<String>();
        therapyFilter = new List<String>();
        
        loadTerritoryOptions(); 
        loadTherapyOptions();           
    }
    
    public void territorySelected(){
        
        therapyFilter = new List<String>();
        loadTherapyOptions();       
    }
    
    private void loadBUOptions(){
        
        buOptions = new List<SelectOption>();
                
        String buQuery = 'Select Id, Name from Business_Unit__c where Name IN:userBUs AND Company__r.Name = \'Europe\' ';
  
        Set<String> territoryBUs = new Set<String>();

        if(!countryFilter.isEmpty()){
        	
        	for(Territory2 ter : [Select Id, Name, Business_Unit__c From Territory2 where Id IN :countryFilter]){
        		
        		territoryBUs.add(ter.Business_Unit__c);        		
        	}
        	
        	if(!territoryBUs.isEmpty() && !territoryBUs.contains('All')){
        		buQuery += ' AND Name IN :territoryBUs';
        	}
        }	        
	    
	    buQuery += ' ORDER BY NAME';
	    
	    for(Business_Unit__c bu : Database.query(buQuery)){
	            
	    	buOptions.add(new SelectOption(bu.Id , bu.Name));   

	    }           
    }
    
    private void loadSBUOptions(){

        sbuOptions = new List<SelectOption>();
        
        if(!buFilter.isEmpty()){
                            
            for(Sub_Business_Units__c sbu : [Select Id, Name from Sub_Business_Units__c where Name IN:userSBUs AND Business_Unit__c IN:buFilter ORDER BY NAME]){
                sbuOptions.add(new SelectOption(sbu.Id , sbu.Name));    
            }
            
        }else{
            
            Set<Id> countryBUs = new Set<Id>();
            
            for(SelectOption countryBU : buOptions){
            	countryBUs.add(countryBU.getValue());
            }
               
            for(Sub_Business_Units__c sbu : [Select Id, Name from Sub_Business_Units__c where Name IN:userSBUs AND Business_Unit__c IN:countryBUs ORDER BY NAME]){
                    
                sbuOptions.add(new SelectOption(sbu.Id , sbu.Name));    
            }                        
        }   
    }
    
    private void loadTherapyOptions(){
        
        therapyOptions = new List<SelectOption>();
        
        if(level == 'geography'){
        	
        	List<String> subBusinessUnits;
        	
	        if(!sbuFilter.isEmpty()){
	            
	            subBusinessUnits = sbuFilter;
	            	            
	        }else{
	             
	            subBusinessUnits = new List<String>();
	            
	            for(SelectOption sbu : sbuOptions){
            		subBusinessUnits.add(sbu.getValue());
            	}           
	        }
	        
	        for(Therapy__c ther : [Select Id, Name from Therapy__c where Therapy_Group__r.Sub_Business_Unit__c IN:subBusinessUnits ORDER BY NAME]){
	                
                therapyOptions.add(new SelectOption(ther.Id , ther.Name));  
            }	
	        
        }else{
             
			if(!territoryFilter.isEmpty()){
            
	        	Set<String> terTherapyGroups = new Set<String>();
	            
	            for(Territory2 ter : [Select Id, Therapy_Groups_Text__c from Territory2 where Id IN:territoryFilter]){
	                
	                List<String> therGroups = ter.Therapy_Groups_Text__c.split(';');
	                
	                if(therGroups.size()>0) terTherapyGroups.addAll(therGroups);
	            }
	            
	            for(Therapy__c ther : [Select Id, Name from Therapy__c where Therapy_Group__r.Sub_Business_Unit__r.Name IN:userSBUs AND Therapy_Group__r.Name IN:terTherapyGroups AND Business_Unit__r.Company__r.Name = 'Europe' ORDER BY NAME]){
	                
	                therapyOptions.add(new SelectOption(ther.Id , ther.Name));  
	            }
	            
	        }else if(!districtFilter.isEmpty()){
	            
	            Set<String> terTherapyGroups = new Set<String>();
	            
	            for(Territory2 ter : [Select Id, Therapy_Groups_Text__c from Territory2 where Id IN:districtFilter]){
	                
	                if(ter.Therapy_Groups_Text__c == null) terTherapyGroups.add('All');
	                else{
	                  List<String> therGroups = ter.Therapy_Groups_Text__c.split(';');
	                
	                  if(therGroups.size()>0) terTherapyGroups.addAll(therGroups);
	                }
	            }
	            
	            String therapyQuery = 'Select Id, Name from Therapy__c where Therapy_Group__r.Sub_Business_Unit__r.Name IN:userSBUs AND Business_Unit__r.Company__r.Name = \'Europe\' ';
	            
	            if(!terTherapyGroups.isEmpty() && !terTherapyGroups.contains('All') ){
	              therapyQuery += ' AND Therapy_Group__r.Name IN:terTherapyGroups ';
	            }
	            
	            therapyQuery += ' ORDER BY NAME';
	            
	            for(Therapy__c ther : Database.query(therapyQuery) ){
	                
	                therapyOptions.add(new SelectOption(ther.Id , ther.Name));  
	            }
	            
	        }else if(!countryFilter.isEmpty()){
	            
	            Set<String> terTherapyGroups = new Set<String>();
	            
	            for(Territory2 ter : [Select Id, Therapy_Groups_Text__c from Territory2 where Id IN:countryFilter]){
	                
	                if(ter.Therapy_Groups_Text__c == null) terTherapyGroups.add('All');
	                else{
	                  List<String> therGroups = ter.Therapy_Groups_Text__c.split(';');
	                
	                  if(therGroups.size()>0) terTherapyGroups.addAll(therGroups);
	                }
	            }
	            
	            String therapyQuery = 'Select Id, Name from Therapy__c where Therapy_Group__r.Sub_Business_Unit__r.Name IN:userSBUs AND Business_Unit__r.Company__r.Name = \'Europe\' ';
	            
	            if(!terTherapyGroups.isEmpty() && !terTherapyGroups.contains('All') ){
	              therapyQuery += ' AND Therapy_Group__r.Name IN:terTherapyGroups ';
	            }
	            
	            therapyQuery += ' ORDER BY NAME';
	            
	            for(Therapy__c ther : Database.query(therapyQuery) ){
	                
	                therapyOptions.add(new SelectOption(ther.Id , ther.Name));  
	            }
	            
	        }else{
	            
	            for(Therapy__c ther : [Select Id, Name from Therapy__c where Therapy_Group__r.Sub_Business_Unit__r.Name IN:userSBUs AND Business_Unit__r.Company__r.Name = 'Europe' ORDER BY NAME]){
	                
	                therapyOptions.add(new SelectOption(ther.Id , ther.Name));  
	            }
	        }   
        }   
    }
    
    private void loadCountryOptions(){
        
        countryOptions = new List<SelectOption>();
        
        Set<Id> userCountries = bl_Territory_Hierarchy.getTerritoryCountries(userTerritories);
            
        for(Territory2 country : [Select Id, Name from Territory2 where Id IN:usercountries ORDER BY NAME]){
            
            countryOptions.add(new SelectOption(country.Id , country.Name));
        }   
    }
    
    private void loadDistrictOptions(){
        
        districtOptions = new List<SelectOption>();
        
        Set<Id> districtIds;
        
        if(!countryFilter.isEmpty()){
            
            Set<Id> countryDistrictIds = bl_Territory_Hierarchy.getTerritoryDistricts(new Set<Id>((List<Id>)countryFilter));
            Set<Id> userDistrictIds = bl_Territory_Hierarchy.getTerritoryDistricts(userTerritories);
            
            districtIds = new Set<Id>();
            
            for(Id districtId : countryDistrictIds){
            	
            	if(userDistrictIds.contains(districtId)) districtIds.add(districtId);	
            }            
            
        }else{
            
            districtIds = bl_Territory_Hierarchy.getTerritoryDistricts(userTerritories);
        }   
        
        for(Territory2 district : [Select Id, Name from Territory2 where  Id IN:districtIds ORDER BY NAME LIMIT 1000]){
            
            districtOptions.add(new SelectOption(district.Id , district.Name));
        }
            
    }
    
    private void loadTerritoryOptions(){
        
        territoryOptions = new List<SelectOption>();
        
        Set<Id> territoryIds;
        
        if(!districtFilter.isEmpty()){
            
            Set<Id> districtTerritoryIds = bl_Territory_Hierarchy.getTerritoryTerritories(new Set<Id>((List<Id>)districtFilter));
            Set<Id> userTerritoryIds = bl_Territory_Hierarchy.getTerritoryTerritories(userTerritories);
            
            territoryIds = new Set<Id>();
            
            for(Id territoryId : districtTerritoryIds){
            	
            	if(userTerritoryIds.contains(territoryId)) territoryIds.add(territoryId);	
            } 
            
        }else if(!countryFilter.isEmpty()){
            
            Set<Id> countryTerritoryIds = bl_Territory_Hierarchy.getTerritoryTerritories(new Set<Id>((List<Id>)countryFilter));
            Set<Id> userTerritoryIds = bl_Territory_Hierarchy.getTerritoryTerritories(userTerritories);
            
            territoryIds = new Set<Id>();
            
            for(Id territoryId : countryTerritoryIds){
            	
            	if(userTerritoryIds.contains(territoryId)) territoryIds.add(territoryId);	
            } 
            
        }else{
        
            territoryIds = bl_Territory_Hierarchy.getTerritoryTerritories(userTerritories);
        }
        
        //-BC - 20150609 - CR-8651 - START
        // Exclude certain territories from the selection
//        for(Territory ter : [Select Id, Name from Territory where  Id IN:territoryIds ORDER BY NAME LIMIT 1000]){
        for(Territory2 ter : [Select Id, Name from Territory2 where Id IN:territoryIds AND (NOT Name like 'RR%') AND (NOT Name like '(RR)%') AND (NOT Name like '%Trunk Stock%') ORDER BY NAME LIMIT 1000]){
        //-BC - 20150609 - CR-8651 - STOP
            
            territoryOptions.add(new SelectOption(ter.Id , ter.Name));
        }   
    }
    
    private void loadSegmentOptions(){
        
        if(segmentOptions == null){
        
            segmentOptions = new List<SelectOption>();
        
            segmentOptions.add(new SelectOption('Top', 'Top'));
            segmentOptions.add(new SelectOption('Develop', 'Develop'));
            segmentOptions.add(new SelectOption('Maintain', 'Maintain'));
            segmentOptions.add(new SelectOption('Let Go On', 'Let Go On'));
            segmentOptions.add(new SelectOption('Not Segmented', 'Not Segmented'));
        }       
    }
    
    private void generateDefaultViewsMap(){
        
        List<SelectOption> geoViews = new List<SelectOption>();
        geoViews.add(new SelectOption('SR', 'Subregion'));
        geoViews.add(new SelectOption('CG', 'Country Group'));
        geoViews.add(new SelectOption('C', 'Country'));
        geoViews.add(new SelectOption('D', 'District'));
        geoViews.add(new SelectOption('T', 'Territory'));
        defaultViewsByLevel.put('geography', geoViews);
        
        List<SelectOption> prodViews = new List<SelectOption>();
        prodViews.add(new SelectOption('BU', 'Business Unit'));
        prodViews.add(new SelectOption('SBU', 'Sub Business Unit'));
        prodViews.add(new SelectOption('TH', 'Therapy'));
        defaultViewsByLevel.put('therapy', prodViews);
        
        List<SelectOption> segViews = new List<SelectOption>();
        segViews.add(new SelectOption('S', 'Segment'));
        segViews.add(new SelectOption('A', 'Account'));
        defaultViewsByLevel.put('segment', segViews);                           
    }
    
    private void generateCurrencyOptions(){

        currencyOptions = new List<SelectOption>();
        currencyOptions.add(new SelectOption('EUR', 'EUR'));
        currencyOptions.add(new SelectOption('USD', 'USD'));
        currencyOptions.add(new SelectOption('CHF', 'CHF'));
        currencyOptions.add(new SelectOption('GBP', 'GBP'));
        currencyOptions.add(new SelectOption('NOK', 'NOK'));
        currencyOptions.add(new SelectOption('SEK', 'SEK'));
        currencyOptions.add(new SelectOption('DKK', 'DKK'));
        currencyOptions.add(new SelectOption('CAD', 'CAD'));
        currencyOptions.add(new SelectOption('ZAR', 'ZAR'));
    }
    
    private void generateFiscalYearOptions(){
    	
    	fpOptions = new List<SelectOption>();
        fpOptions.add(new SelectOption('MTD', 'Month-To-Date'));
        fpOptions.add(new SelectOption('QTD', 'Quarter-To-Date'));
        fpOptions.add(new SelectOption('YTD', 'Year-To-Date'));
        fpOptions.add(new SelectOption('Rolling12', 'Rolling 12 Months'));    	
    }
    
    private void loadConfiguration(){
        
        LW_Configuration__c config = LW_Configuration__c.getInstance();
        
        levelCodes.put('geography', config.Geography_Id__c);
        levelCodes.put('therapy', config.Product_Id__c);
        levelCodes.put('segment', config.Segment_Id__c);
        levelCodes.put('account', config.Account_Id__c);
                
        baseURL = config.URL__c;
    }
    
    private void getValueLabels(){

        valueLabels = new Map<String, String>();
        valueLabels.put('geography', 'Geography');
        valueLabels.put('therapy', 'Therapy');
        valueLabels.put('segment', 'Segment');        
        valueLabels.put('SR', 'Sub-Region');
        valueLabels.put('CG', 'Country Group');
        valueLabels.put('C', 'Country');
        valueLabels.put('D', 'District');
        valueLabels.put('T', 'Territory');
        valueLabels.put('BU', 'Business Unit');
        valueLabels.put('SBU', 'Sub-Business Unit');
        valueLabels.put('TH', 'Therapy');
        valueLabels.put('S', 'Segment');
        valueLabels.put('A', 'Account');
        valueLabels.put('MTD', 'Month-To-Date');
        valueLabels.put('QTD', 'Quarter-To-Date');
        valueLabels.put('YTD', 'Year-To-Date');
        valueLabels.put('Rolling12', 'Rolling 12 Months');   
    }
    
    private class LWConfiguration{
        
        public String level {get; set;}
        public String view {get; set;}
        public String fiscalPeriod {get; set;}
        public String cur {get; set;}
        public List<String> buFilter {get; set;}
        public List<String> sbuFilter {get; set;}
        public List<String> therapyFilter {get; set;}
        public List<String> countryFilter {get; set;}
        public List<String> districtFilter {get; set;}
        public List<String> territoryFilter {get; set;}
        public List<String> segmentFilter {get; set;}       
    }
}