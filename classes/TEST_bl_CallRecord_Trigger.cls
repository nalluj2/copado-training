//--------------------------------------------------------------------------------------------------------------------------------
//  Author      :   Bart Caelen
//  Date        :   19-08-2016
//  Description :   APEX TEST Class for the APEX Trigger tr_CallRecord and APEX Class bl_CallRecord_Trigger
//  Change Log  :    
//--------------------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_bl_CallRecord_Trigger {
	
	@isTest static void test_updateFiscalPeriod_Insert() {

		// Create Test Data
		clsTestData.createDIBFiscalPeriodData();
    
    	// Create Call Record Data
    	clsTestData.iRecord_CallRecord = 1;
		clsTestData.createCallRecordData(false);

		Test.startTest();

		for (Call_Records__c oCallRecord : clsTestData.lstCallRecord){
			oCallRecord.Call_Date__c = Date.newInstance(Date.today().Year(), 3, 25);
		}
		insert clsTestData.lstCallRecord;

		Test.stopTest();

		List<Call_Records__c> lstCallRecord = [SELECT Id, Fiscal_Period__c, Call_Date__c FROM Call_Records__c WHERE Id = :clsTestData.lstCallRecord];

		for (Call_Records__c oCallRecord : lstCallRecord){
			System.assertEquals(oCallRecord.Fiscal_Period__c, String.valueOf(Date.today().year()) + '-Period 11');
		}

	}


	@isTest static void test_updateFiscalPeriod_Update() {

		// Create Test Data
		clsTestData.createDIBFiscalPeriodData();

    	// Create Call Record Data
    	clsTestData.iRecord_CallRecord = 7;
		clsTestData.createCallRecordData(false);
		clsTestData.oMain_CallRecord.Call_Date__c = Date.newInstance(Date.today().Year() - 1, 7, 25);
        for (Call_Records__c oCallRecord : clsTestData.lstCallRecord){
			oCallRecord.Call_Date__c = Date.newInstance(Date.today().Year() - 1, 7, 25);
        }
		insert clsTestData.lstCallRecord;

		Test.startTest();

			clsTestData.lstCallRecord[0].Call_Date__c = Date.newInstance(Date.today().Year() - 1, 8, 25);
		update clsTestData.lstCallRecord[0];

			clsTestData.lstCallRecord[1].Fiscal_Period__c = 'WRONG PERIOD';
		update clsTestData.lstCallRecord[1];

			clsTestData.lstCallRecord[2].Fiscal_Period__c = 'WRONG PERIOD';
			clsTestData.lstCallRecord[2].Call_Date__c = Date.newInstance(Date.today().Year() - 10, 8, 25); // No DIBFiscalPeriod Data available
		update clsTestData.lstCallRecord[2];

		List<Call_Records__c> lstCallRecord_Update = new List<Call_Records__c>();
			clsTestData.lstCallRecord[3].Call_Date__c = Date.newInstance(Date.today().Year() - 1, 8, 25);
			clsTestData.lstCallRecord[4].Call_Date__c = Date.newInstance(Date.today().Year() - 1, 8, 25);
			clsTestData.lstCallRecord[5].Fiscal_Period__c = 'WRONG PERIOD';
			clsTestData.lstCallRecord[5].Call_Date__c = Date.newInstance(Date.today().Year() - 10, 8, 25); // No DIBFiscalPeriod Data available
			clsTestData.lstCallRecord[6].Call_Date__c = null;
		lstCallRecord_Update.add(clsTestData.lstCallRecord[3]);
		lstCallRecord_Update.add(clsTestData.lstCallRecord[4]);
		lstCallRecord_Update.add(clsTestData.lstCallRecord[5]);
		lstCallRecord_Update.add(clsTestData.lstCallRecord[6]);
		update lstCallRecord_Update;


		Test.stopTest();

		List<Call_Records__c> lstCallRecord = [SELECT Id, Fiscal_Period__c, Call_Date__c FROM Call_Records__c WHERE Id = :clsTestData.oMain_CallRecord.Id];

		for (Call_Records__c oCallRecord : lstCallRecord){

			if (oCallRecord.Call_Date__c == Date.newInstance(Date.today().Year() - 1, 7, 25)){
				System.assertEquals(oCallRecord.Fiscal_Period__c, String.valueOf(Date.today().year()) + '-Period 3');
			}else if (oCallRecord.Call_Date__c == Date.newInstance(Date.today().Year() - 1, 8, 25)){
				System.assertEquals(oCallRecord.Fiscal_Period__c, String.valueOf(Date.today().year()) + '-Period 5');
			}else if (oCallRecord.Call_Date__c == Date.newInstance(Date.today().Year() - 10, 8, 25)){
				System.assertEquals(oCallRecord.Fiscal_Period__c, null);
			}else if (oCallRecord.Call_Date__c == null){
				System.assertEquals(oCallRecord.Fiscal_Period__c, null);
			}

		}

	}	
}
//--------------------------------------------------------------------------------------------------------------------------------