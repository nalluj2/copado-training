/*  
    Test Class Name - Test_ctrlExt_AccPerformanceDispatcher 
    Description  –  The written class will cover Account Plan Classes:
                    1. ctrlExt_AccountPerformanceDispatcher
                    
    Author - Rudy De Coninck
    Created Date  - 06-05-2015 
*/

@isTest(seeAllData=true) //seeAllData set to true as Record type can not be inserted from test class
private class Test_ctrlExt_AccPerformanceDispatcher{

    static testMethod void myUnitTest() {

		User oUser_Admin = clsTestData_User.createUser_SystemAdministratorMDT('tstadm1', false);
			oUser_Admin.Job_Title_vs__c = 'Technical Consultant';
		insert oUser_Admin;



		System.runAs(oUser_Admin){
			 //Insert Company
			 Company__c cmpny = [select Id, name from Company__c where Company_Code_Text__c = 'EUR'].get(0);

			 DIB_Country__c DIBcntry = new DIB_Country__c();//[select CurrencyIsoCode from DIB_Country__c limit 1];
			 DIBcntry.Company__c = cmpny.id;
			 DIBcntry.Name = 'Test Account Performance Country';
			 DIBcntry.T_Country__c = 'Test Account Performance Country';
			 DIBcntry.Country_ISO_Code__c = 'BL';
			 DIBcntry.CurrencyIsoCode = 'EUR';
			 insert DIBcntry;
        
			 //Insert Accounts
			 List<Account> accounts = new List<Account>{} ; 
			 for (Integer i = 0 ; i < 5 ; i++)
			 {
				 Account a = new Account() ;
				 a.Name = 'Test Account Name Account Performance' + i  ;  
				 a.RecordTypeId = FinalConstants.sapAccountRecordTypeId ;
				 a.Account_Country__c='BELGIUM';
				 a.CurrencyIsoCode =DIBcntry.CurrencyIsoCode ;
				 accounts.add(a); 
			 } 
			 insert accounts;

			BUSINESS_UNIT_GROUP__C BUG = new BUSINESS_UNIT_GROUP__C();
			bug.Account_Performance_View__c = 'iPlan1 Clone';
			BUG.Master_Data__c = cmpny.id;
        
			insert bug;
                  
			 //Insert Business Unit
			List<Business_Unit__c> lstBU=new List<Business_Unit__c>();
          
			Business_Unit__c bu =  new Business_Unit__c();
			bu.Company__c =cmpny.id;
			bu.name='BUMedtronic Account Performance';
			bu.Account_Plan_Activities__c=true;
			bu.BUSINESS_UNIT_GROUP__C = bug.Id;
			lstBU.add(bu);
			Business_Unit__c bu1 =  new Business_Unit__c();
			bu1.Company__c =cmpny.id;
			bu1.name='BU1Medtronic Account Performance';
			bu1.Account_Plan_Activities__c=true;
			bu1.Company__c = cmpny.Id;
			bu1.BUSINESS_UNIT_GROUP__C = bug.Id;
			lstBU.add(bu1);

			Business_Unit__c bu2 =  new Business_Unit__c();
			bu2.Company__c =cmpny.id;
			bu2.name='BU1Medtronic Account Performance BU2';
			bu2.Account_Plan_Activities__c=true;
			bu2.Company__c = cmpny.Id;
			bu2.BUSINESS_UNIT_GROUP__C = bug.Id;
			lstBU.add(bu2);

			Business_Unit__c bu3 =  new Business_Unit__c();
			bu3.Company__c =cmpny.id;
			bu3.name='BU1Medtronic Account Performance123';
			bu3.Account_Plan_Activities__c=true;
			bu3.Company__c = cmpny.Id;
			bu3.BUSINESS_UNIT_GROUP__C = bug.Id;
			lstBU.add(bu3);

			Business_Unit__c bu4 =  new Business_Unit__c();
			bu4.Company__c =cmpny.id;
			bu4.name='BU1Medtronic Account Performance1234';
			bu4.Account_Plan_Activities__c=true;
			bu4.Company__c = cmpny.Id;
			bu4.BUSINESS_UNIT_GROUP__C = bug.Id;
			lstBU.add(bu4);

			Business_Unit__c bu5 =  new Business_Unit__c();
			bu5.Company__c =cmpny.id;
			bu5.name='BU1Medtronic Account Performance1235';
			bu5.Account_Plan_Activities__c=true;
			bu5.Company__c = cmpny.Id;
			bu5.BUSINESS_UNIT_GROUP__C = bug.Id;
			lstBU.add(bu5);
         
			Business_Unit__c bu6 =  new Business_Unit__c();
			bu6.Company__c =cmpny.id;
			bu6.name='BU1Medtronic Account PerformanceBU6';
			bu6.Account_Plan_Activities__c=true;
			bu6.Company__c = cmpny.Id;
			bu6.BUSINESS_UNIT_GROUP__C = bug.Id;
			lstBU.add(bu6);
         
			Business_Unit__c bu7 =  new Business_Unit__c();
			bu7.Company__c =cmpny.id;
			bu7.name='BU1Medtronic Account PerformanceBU7';
			bu7.Account_Plan_Activities__c=true;
			bu7.Company__c = cmpny.Id;
			bu7.BUSINESS_UNIT_GROUP__C = bug.Id;
			lstBU.add(bu7);
         
			insert lstBU;
    
			//Insert SBU
			List<Sub_Business_Units__c> lstSBU=new List<Sub_Business_Units__c>();
        
			Sub_Business_Units__c sbu = new Sub_Business_Units__c();
			sbu.name='SBUMedtronic1 Account Performance1236';
			sbu.Business_Unit__c=bu.id;
			sbu.Account_Plan_Default__c='Revenue';
			sbu.Account_Flag__c = 'AF_Solutions__c';
			lstSBU.add(sbu);
    
			Sub_Business_Units__c sbu1 = new Sub_Business_Units__c();
			sbu1.name='Diabetes Core0';
			sbu1.Business_Unit__c=bu1.id;
			sbu1.Account_Plan_Default__c='Units';
			sbu1.Account_Flag__c = 'AF_Solutions__c';
			lstSBU.add(sbu1);

			Sub_Business_Units__c sbu2 = new Sub_Business_Units__c();
			sbu2.name='Diabetes Core1';
			sbu2.Business_Unit__c=bu2.id;
			sbu2.Account_Plan_Default__c='Units';
			sbu2.Account_Flag__c = 'AF_Solutions__c';
			lstSBU.add(sbu2);

			Sub_Business_Units__c sbu3 = new Sub_Business_Units__c();
			sbu3.name='Diabetes Core2';
			sbu3.Business_Unit__c=bu3.id;
			sbu3.Account_Plan_Default__c='Units';
			sbu3.Account_Flag__c = 'AF_Solutions__c';
			lstSBU.add(sbu3);

			Sub_Business_Units__c sbu4 = new Sub_Business_Units__c();
			sbu4.name='Diabetes Core3';
			sbu4.Business_Unit__c=bu4.id;
			sbu4.Account_Plan_Default__c='Units';
			sbu4.Account_Flag__c = 'AF_Solutions__c';
			lstSBU.add(sbu4);

			Sub_Business_Units__c sbu5 = new Sub_Business_Units__c();
			sbu5.name='Diabetes Core4';
			sbu5.Business_Unit__c=bu4.id;
			sbu5.Account_Plan_Default__c='Units';
			sbu5.Account_Flag__c = 'AF_Solutions__c';
			lstSBU.add(sbu5);
        
			Sub_Business_Units__c sbu6 = new Sub_Business_Units__c();
			sbu6.name='Diabetes Core5';
			sbu6.Business_Unit__c=bu6.id;
			sbu6.Account_Plan_Default__c='Units';
			sbu6.Account_Flag__c = 'AF_Solutions__c';
			lstSBU.add(sbu6);
        
			Sub_Business_Units__c sbu7 = new Sub_Business_Units__c();
			sbu7.name='Diabetes Core6';
			sbu7.Business_Unit__c=bu7.id;
			sbu7.Account_Plan_Default__c='Units';
			sbu7.Account_Flag__c = 'AF_Solutions__c';
			lstSBU.add(sbu7);
        
			insert lstSBU;

			//Insert Therapy Group
			Therapy_Group__c tg = new Therapy_Group__c();
			tg.Name='Therapy Group Account Performance';
			tg.Sub_Business_Unit__c = sbu1.Id;
			tg.Company__c = cmpny.Id;
			insert tg;
    
			//Insert Therapy
			Therapy__c tp = new Therapy__c();
			tp.Name='Therapy Account Performance';
			tp.Therapy_Group__c = tg.id;
			tp.Business_Unit__c=bu.id;
			tp.Sub_Business_Unit__c=sbu1.id;
			tp.Therapy_Name_Hidden__c = 'Therapy';
			insert tp;
    
			Therapy__c tp1 = new Therapy__c();
			tp1.Name='Therapy1 Account Performance';
			tp1.Therapy_Group__c = tg.id;
			tp1.Business_Unit__c=bu.id;
			tp1.Sub_Business_Unit__c=sbu2.id;
			tp1.Therapy_Name_Hidden__c = 'Therapy1';
			insert tp1;
    
			//Insert Product Group
			Product_Group__c pg = new Product_Group__c();
			pg.Name = 'Product Group Account Performance';
			pg.Therapy_ID__c = tp.id;
			pg.Editable_in__c='Revenue';
			insert pg;
    
			Product_Group__c pg1 = new Product_Group__c();
			pg1.Name = 'Product Group1 Account Performance';
			pg.Editable_in__c='Units';
			pg1.Therapy_ID__c = tp1.id;
			insert pg1;

			Account_Plan__c ap = new Account_Plan__c();
			ap.Account_ID__c=accounts[0].id;
			ap.Sub_Business_Unit_ID__c=sbu1.id;
			ap.Country__c='Belgium';
			ap.Account_Opportunities__c='Other';
			ap.Other_Opportunity_Text__c ='testOB';
			ap.Account_Barriers__c='Price pressure';
			ap.FY_Objectives_Text__c='FY Objectives Text';
			ap.Long_Term_Objectives_Text__c='Long_Term_Objectives_Text';
			insert ap;

			Account_Plan_2__c AccPlan2 = new Account_Plan_2__c();
			AccPlan2.Account__c = accounts[0].id;
			//AccPlan2.Account_Plan__c = ap.id;
			AccPlan2.Account_Plan_Level__c = 'Business Unit';
			AccPlan2.Business_Unit__c = BU.id;
			AccPlan2.Sub_Business_Unit__c = sbu2.id;
			AccPlan2.Business_Unit_Group__c = BUG.id;
			insert AccPlan2;

			//Create Account Perfromace Rule
			List<Account_Performance_Rule__c> lstAPRule=new List<Account_Performance_Rule__c>();
        
			Account_Performance_Rule__c APRule = new Account_Performance_Rule__c ();
			APRule.Business_Unit__c = bu1.Id;
			APRule.Company__c = cmpny.id;
			APRule.Highest_level__c = 'Business Unit'; 
			APRule.Job_Title_vs__c = 'Technical Consultant';
			APRule.Security_Restriction_Message__c = 'Test message'; 
			APRule.Show_Account_for__c = 'User\'s Country';
			APRule.View_Restriction__c = 'Business Unit Group';
			//insert APRule; 
			lstAPRule.add(APRule);

			Account_Performance_Rule__c APRule1 = new Account_Performance_Rule__c ();
			APRule1.Business_Unit__c = bu2.Id;
			APRule1.Company__c = cmpny.id;
			APRule1.Highest_level__c = 'Business Unit'; 
			APRule1.Job_Title_vs__c = 'Technical Consultant';
			APRule1.Security_Restriction_Message__c = 'Test message'; 
			APRule1.Show_Account_for__c = 'User\'s Country';
			APRule1.View_Restriction__c = 'XBU';
			//insert APRule1; 
			lstAPRule.add(APRule1);

			Account_Performance_Rule__c APRule2 = new Account_Performance_Rule__c ();
			APRule2.Business_Unit__c = bu3.Id;
			APRule2.Company__c = cmpny.id;
			APRule2.Highest_level__c = 'Sub Business Unit'; 
			APRule2.Job_Title_vs__c = 'Technical Consultant';
			APRule2.Security_Restriction_Message__c = 'Test message'; 
			APRule2.Show_Account_for__c = 'User\'s Country';
			APRule2.View_Restriction__c = 'Business Unit';
			//insert APRule2; 
			lstAPRule.add(APRule2);

			Account_Performance_Rule__c APRule3 = new Account_Performance_Rule__c ();
			APRule3.Business_Unit__c = bu4.Id;
			APRule3.Company__c = cmpny.id;
			APRule3.Highest_level__c = 'Sub Business Unit'; 
			APRule3.Job_Title_vs__c = 'Technical Consultant';
			APRule3.Security_Restriction_Message__c = 'Test message'; 
			APRule3.Show_Account_for__c = 'User\'s Country';
			APRule3.View_Restriction__c = 'XBU';
			//insert APRule3; 
			lstAPRule.add(APRule3);

			Account_Performance_Rule__c APRule4 = new Account_Performance_Rule__c ();
			APRule4.Business_Unit__c = bu5.Id;
			APRule4.Company__c = cmpny.id;
			APRule4.Highest_level__c = 'Sub Business Unit'; 
			APRule4.Job_Title_vs__c = 'Technical Consultant';
			APRule4.Security_Restriction_Message__c = 'Test message'; 
			APRule4.Show_Account_for__c = 'User\'s Country';
			APRule4.View_Restriction__c = 'Business Unit Group';
			//insert APRule4; 
			lstAPRule.add(APRule4);

			Account_Performance_Rule__c APRule5 = new Account_Performance_Rule__c ();
			APRule5.Business_Unit__c = bu.Id;
			APRule5.Company__c = cmpny.id;
			APRule5.Highest_level__c = 'Sub Business Unit'; 
			APRule5.Job_Title_vs__c = 'Technical Consultant';
			APRule5.Security_Restriction_Message__c = 'Test message'; 
			APRule5.Show_Account_for__c = 'User\'s Country';
			APRule5.View_Restriction__c = 'Therapy';
			//insert APRule5; 
			lstAPRule.add(APRule5);
        
			Account_Performance_Rule__c APRule6 = new Account_Performance_Rule__c ();
			APRule6.Business_Unit__c = bu6.Id;
			APRule6.Company__c = cmpny.id;
			APRule6.Highest_level__c = 'Therapy'; 
			APRule6.Job_Title_vs__c = 'Technical Consultant';
			APRule6.Security_Restriction_Message__c = 'Test message'; 
			APRule6.Show_Account_for__c = 'User\'s Country';
			APRule6.View_Restriction__c = 'Therapy';
			lstAPRule.add(APRule6);
        
			Account_Performance_Rule__c APRule7 = new Account_Performance_Rule__c ();
			APRule7.Business_Unit__c = bu7.Id;
			APRule7.Company__c = cmpny.id;
			APRule7.Highest_level__c = 'Product Group'; 
			APRule7.Job_Title_vs__c = 'Technical Consultant';
			APRule7.Security_Restriction_Message__c = 'Test message'; 
			APRule7.Show_Account_for__c = 'User\'s Country';
			APRule7.View_Restriction__c = 'Therapy';
			lstAPRule.add(APRule7);
        
			insert lstAPRule;


			// Create Account Performance data
			Account_Performance__c accPer = new Account_Performance__c();
			accper.Account_Plan_ID__c = ap.Id;
			accper.Actual_Q1_revenue__c = 1000;
			accper.Actual_Q1_units__c = 100;
			accper.Actual_Q2_revenue__c = 1000;
			accper.Actual_Q2_units__c = 100;        
			accper.Actual_Q3_revenue__c = 1000;
			accper.Actual_Q3_units__c = 100;
			accper.Actual_Q4_revenue__c = 1000;
			accper.Actual_Q4_units__c = 100;
			accper.Business_Unit__c = BU1.id;
			accper.BI_Account_ID__c = accounts[0].Id;
			accper.Potential_Revenue__c = 100;
			accper.Last12_Revenue__c = 1000;
			accper.Prev12_Revenue__c = 1000;
			accper.YTD_Revenue__c = 1000;
			accper.YTD_1_Revenue__c = 1000;  
			insert accper;
		
/*
        Profile p = [select id from profile where name='System Administrator MDT'];    
        User u = new User(alias = 'standt1', email='standarduser@testorg.com',             
                        emailencodingkey='UTF-8', lastname='TestCAP1',             
                        languagelocalekey='en_US',             
                        localesidkey='en_US', profileid = p.Id,            
                        timezonesidkey='America/Los_Angeles',             
                        Country_vs__c='BELGIUM',                                   
                        username='NoBUUser1@testorg.com',
                        Alias_unique__c='standarduser_Alias_unique__c',Company_Code_Text__c='EUR',
                        User_Business_Unit_vs__c = 'CRHF', Job_title_vs__c = 'Technical Consultant');  
        insert u; 
*/
  
            
                List<User_Business_Unit__c> lstUserSBU =new List<User_Business_Unit__c>();
                
                User_Business_Unit__c UserSBU = new User_Business_Unit__c();
                UserSBU.Primary__c = true;
                UserSBU.Sub_Business_Unit__c = sbu1.id;
                UserSBU.User__c = oUser_Admin.Id;
                //insert UserSBU;
                lstUserSBU.add(UserSBU);
                

                User_Business_Unit__c UserSBU1 = new User_Business_Unit__c();
                UserSBU1.Sub_Business_Unit__c = sbu.id;
                UserSBU1.User__c = oUser_Admin.Id;
                //insert UserSBU1;
                lstUserSBU.add(UserSBU1);

                User_Business_Unit__c UserSBU2 = new User_Business_Unit__c();
                UserSBU2.Sub_Business_Unit__c = sbu2.id;
                UserSBU2.User__c = oUser_Admin.Id;
                //insert UserSBU2;
               lstUserSBU.add(UserSBU2);

                User_Business_Unit__c UserSBU3 = new User_Business_Unit__c();
                UserSBU3.Sub_Business_Unit__c = sbu3.id;
                UserSBU3.User__c = oUser_Admin.Id;
                //insert UserSBU3;
                lstUserSBU.add(UserSBU3);
                                
                User_Business_Unit__c UserSBU4 = new User_Business_Unit__c();
                UserSBU4.Sub_Business_Unit__c = sbu4.id;
                UserSBU4.User__c = oUser_Admin.Id;
                lstUserSBU.add(UserSBU4);

                User_Business_Unit__c UserSBU5 = new User_Business_Unit__c();
                UserSBU5.Sub_Business_Unit__c = sbu5.id;
                UserSBU5.User__c = oUser_Admin.Id;
                lstUserSBU.add(UserSBU5);
                
                User_Business_Unit__c UserSBU6 = new User_Business_Unit__c();
                UserSBU6.Sub_Business_Unit__c = sbu6.id;
                UserSBU6.User__c = oUser_Admin.Id;
                lstUserSBU.add(UserSBU6);
                
                User_Business_Unit__c UserSBU7 = new User_Business_Unit__c();
                UserSBU7.Sub_Business_Unit__c = sbu7.id;
                UserSBU7.User__c = oUser_Admin.Id;
                lstUserSBU.add(UserSBU7);
                
                insert lstUserSBU;

                bl_AccountPerformance.getUserAccess();
                //bl_AccountPerformance.getUserTeritory();
        
                ApexPages.StandardController con1 = new ApexPages.StandardController(AccPlan2);       
                ctrlExt_AccountPerformanceDispatcher controller1 = new ctrlExt_AccountPerformanceDispatcher(con1);
				Boolean bTest = controller1.boolxBU;
				Id idTest = controller1.idUserCompany;
				Date dateTest = controller1.dtAccPerOverviewsSalesUpdateUntil;
				String tTest = controller1.onVpnUrl;
				tTest = controller1.reportSearchArguments;
				tTest = controller1.getTimeStamp();


                controller1.getShowDataPer();
                controller1.strHighestLevel='Sub Business Unit';
                controller1.strChangeType='Business Unit';
                controller1.refreshData();
                controller1.strHighestLevel='Therapy';
                controller1.strChangeType='Sub Business Unit';
                controller1.refreshData();
                controller1.strHighestLevel='Product group';
                controller1.strChangeType='Therapy';
                controller1.refreshData1();

				controller1.resetData();
                
                controller1.redirect();
                controller1.redirectAP();
                controller1.backToAccount();
                controller1.backToAccountPlan();
        }
    }

}