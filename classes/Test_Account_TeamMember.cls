@isTest
private class Test_Account_TeamMember {
                
    static testmethod private void redirectToStandard(){
        
		clsTestData_MasterData.tCompanyCode = 'EUR';
		clsTestData_MasterData.createCompany();
		clsTestData_MasterData.tCountryCode = 'BE';
		List<DIB_Country__c> lstCountry = clsTestData_MasterData.createCountry(false);
		for (DIB_Country__c oCountry : lstCountry){
			oCountry.Allow_Inactive_Users_as_Members__c = false;
		}
		insert lstCountry;
		        
        User testUser = new User();
        testUser.alias = 'standt1';
        testUser.email='standarduser@testorg.com';
        testUser.emailencodingkey='UTF-8';
        testUser.lastname='TestCAP1';
        testUser.languagelocalekey='en_US';
        testUser.localesidkey='en_US';
        testUser.profileid = UserInfo.getProfileId();
        testUser.timezonesidkey='America/Los_Angeles';
        testUser.Region_vs__c = 'Europe';
		testUser.Sub_Region__c = 'NWE';
		testUser.Country_vs__c = 'BELGIUM';     
        testUser.username='ContactActionPlan@testorg.com';
        testUser.Alias_unique__c='standarduser_Alias_unique__c';
        testUser.Company_Code_Text__c='T45';
            
        System.runAs(testUser){
			
			Test.startTest();
			            
            Account_Team_Member__c teamMember = new Account_Team_Member__c();
            
            ctrlExt_Account_TeamMember_New controller = new ctrlExt_Account_TeamMember_New(new ApexPages.Standardcontroller(teamMember));
            
            PageReference pr = controller.redirectToStandardPage();
            
            System.assert(pr != null);
        }
                    
    }
    
    static testmethod private void noRedirectToStandard(){
        
		clsTestData_MasterData.tCompanyCode = 'EUR';
		clsTestData_MasterData.createCompany();
		clsTestData_MasterData.tCountryCode = 'BE';
		List<DIB_Country__c> lstCountry = clsTestData_MasterData.createCountry(false);
		for (DIB_Country__c oCountry : lstCountry){
			oCountry.Allow_Inactive_Users_as_Members__c = true;
		}
		insert lstCountry;

        User testUser = new User();
        testUser.alias = 'standt1';
        testUser.email='standarduser@testorg.com';
        testUser.emailencodingkey='UTF-8';
        testUser.lastname='TestCAP1';
        testUser.languagelocalekey='en_US';
        testUser.localesidkey='en_US';
        testUser.profileid = UserInfo.getProfileId();
        testUser.timezonesidkey='America/Los_Angeles';
        testUser.Region_vs__c = 'Europe';
		testUser.Sub_Region__c = 'NWE';
        testUser.Country_vs__c = 'BELGIUM';        
        testUser.username='ContactActionPlan@testorg.com';
        testUser.Alias_unique__c='standarduser_Alias_unique__c';
        testUser.Company_Code_Text__c='T45';
            
        System.runAs(testUser){
            
            Account_Team_Member__c teamMember = new Account_Team_Member__c();
            
            ctrlExt_Account_TeamMember_New controller = new ctrlExt_Account_TeamMember_New(new ApexPages.Standardcontroller(teamMember));
            
            PageReference pr = controller.redirectToStandardPage();
            
            System.assert(pr == null);
        }
                    
    } 
}