@isTest
private class Test_ws_Activity_Scheduling {
	
	private static testmethod void testCaseWS(){
		
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();
	
		req.requestURI = '/services/apexrest/ActivitySchedulingCaseService';  
		req.httpMethod = 'POST';
		
		Account acc = [Select Id from Account];
		Implant_Scheduling_Team__c team = [Select Id, Team_Group__c from Implant_Scheduling_Team__c];
				
		Case requestCase = new Case();		
		requestCase.AccountId = acc.Id;			
		requestCase.Start_of_Procedure__c = DateTime.now(); 
		requestCase.Procedure_Duration_Implants__c = '03:00';
		requestCase.RecordTypeId = [Select Id from RecordType where SObjectType = 'Case' AND DeveloperName = 'Implant_Scheduling'].Id;
		requestCase.Activity_Scheduling_Team__c = team.Id;
		requestCase.Status = 'Open';			
				
		String JSON_Message = JSON.serialize(requestCase);
		req.requestBody = Blob.valueOf(JSON_Message);
		
		RestContext.request = req;
		RestContext.response = res;	
			
		ws_ActivityScheduling_Case.doUpsert();
		
		List<Case> caseList = [Select Id from Case where Activity_Scheduling_Team__c = :team.Id];
		System.assert(caseList.size() == 1);
		
		//UPDATE
		req = new RestRequest(); 
		res = new RestResponse();
	
		req.requestURI = '/services/apexrest/ActivitySchedulingCaseService';  
		req.httpMethod = 'POST';	
		
		RestContext.request = req;
		RestContext.response = res;	
		
		requestCase.Id = caseList[0].Id;
		requestCase.Procedure_Duration_Implants__c = '04:00';
			
		JSON_Message = JSON.serialize(requestCase);
		req.requestBody = Blob.valueOf(JSON_Message);
		
		RestContext.request = req;
		RestContext.response = res;	
			
		ws_ActivityScheduling_Case.doUpsert();
		
		caseList = [Select Id, Procedure_Duration_Implants__c from Case where Activity_Scheduling_Team__c = :team.Id];
		System.assert(caseList.size() == 1);
		System.assert(caseList[0].Procedure_Duration_Implants__c == '04:00');	
		
		//DELETE
		req = new RestRequest(); 
		res = new RestResponse();
	
		req.requestURI = '/services/apexrest/ActivitySchedulingCaseService';
		req.params.put('id', caseList[0].Id);  
		req.httpMethod = 'DELETE';	
		
		RestContext.request = req;
		RestContext.response = res;	
			
		ws_ActivityScheduling_Case.doDelete();
		
		caseList = [Select Id from Case where Activity_Scheduling_Team__c = :team.Id];
		System.assert(caseList.size() == 0);	
	}
	
	private static testmethod void testCaseWS_Error(){
		
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();
	
		req.requestURI = '/services/apexrest/ActivitySchedulingCaseService';  
		req.httpMethod = 'POST';
		
		Account acc = [Select Id from Account];
		Implant_Scheduling_Team__c team = [Select Id, Team_Group__c from Implant_Scheduling_Team__c];
				
		Case requestCase = new Case();		
		requestCase.AccountId = acc.Id;			
		requestCase.Start_of_Procedure__c = DateTime.now(); 
		requestCase.Procedure_Duration_Implants__c = 'Invalid value';
		requestCase.RecordTypeId = [Select Id from RecordType where SObjectType = 'Case' AND DeveloperName = 'Implant_Scheduling'].Id;
		requestCase.Activity_Scheduling_Team__c = team.Id;
		requestCase.Status = 'Open';			
				
		String JSON_Message = JSON.serialize(requestCase);
		req.requestBody = null;
		
		RestContext.request = req;
		RestContext.response = res;	
			
		ws_ActivityScheduling_Case.doUpsert();
		
		System.assert(res.statusCode == 400);
		ws_ActivityScheduling_Case.SalesforceError result = (ws_ActivityScheduling_Case.SalesforceError) JSON.deserialize(res.responseBody.toString(), ws_ActivityScheduling_Case.SalesforceError.class);
				
		//DELETE		
		req = new RestRequest(); 
		res = new RestResponse();
	
		req.requestURI = '/services/apexrest/ActivitySchedulingCaseService';
		req.params.put('id', null);  
		req.httpMethod = 'DELETE';	
		
		RestContext.request = req;
		RestContext.response = res;	
			
		ws_ActivityScheduling_Case.doDelete();
		
		System.assert(res.statusCode == 400);		
		result = (ws_ActivityScheduling_Case.SalesforceError) JSON.deserialize(res.responseBody.toString(), ws_ActivityScheduling_Case.SalesforceError.class);		
	}
	
	private static testmethod void testEventWS(){
		
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();
	
		req.requestURI = '/services/apexrest/ActivitySchedulingEventService';  
		req.httpMethod = 'POST';
				
		Event evnt = new Event();			
		evnt.Subject = 'Test Subject';	
		evnt.StartDateTime = DateTime.now(); 
		evnt.EndDateTime = DateTime.now().addHours(5);
		evnt.RecordTypeId = [Select Id from RecordType where SObjectType = 'Event' AND DeveloperName = 'Activity_Scheduling'].Id;					
				
		String JSON_Message = JSON.serialize(evnt);
		req.requestBody = Blob.valueOf(JSON_Message);
		
		RestContext.request = req;
		RestContext.response = res;	
			
		ws_ActivityScheduling_Event.doUpsert();
		
		List<Event> eventList = [Select Id from Event where OwnerId = :UserInfo.getUserId()];
		System.assert(eventList.size() == 1);
		
		//UPDATE
		req = new RestRequest(); 
		res = new RestResponse();
	
		req.requestURI = '/services/apexrest/ActivitySchedulingEventService';  
		req.httpMethod = 'POST';	
		
		RestContext.request = req;
		RestContext.response = res;	
		
		evnt.Id = eventList[0].Id;
		evnt.Subject = 'Modified Subject';
			
		JSON_Message = JSON.serialize(evnt);
		req.requestBody = Blob.valueOf(JSON_Message);
		
		RestContext.request = req;
		RestContext.response = res;	
			
		ws_ActivityScheduling_Event.doUpsert();
		
		eventList = [Select Id, Subject from Event where OwnerId = :UserInfo.getUserId()];
		System.assert(eventList.size() == 1);
		System.assert(eventList[0].Subject == 'Modified Subject');	
		
		//DELETE
		req = new RestRequest(); 
		res = new RestResponse();
	
		req.requestURI = '/services/apexrest/ActivitySchedulingEventService';
		req.params.put('id', eventList[0].Id);  
		req.httpMethod = 'DELETE';	
		
		RestContext.request = req;
		RestContext.response = res;	
			
		ws_ActivityScheduling_Event.doDelete();
		
		eventList = [Select Id from Event where OwnerId = :UserInfo.getUserId()];
		System.assert(eventList.size() == 0);	
	}
	
	private static testmethod void testEventWS_Error(){
		
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();
	
		Event evnt = new Event();			
		evnt.Subject = 'Test Subject';	
		evnt.StartDateTime = DateTime.now(); 
		evnt.EndDateTime = DateTime.now().addHours(-1);
		evnt.RecordTypeId = [Select Id from RecordType where SObjectType = 'Event' AND DeveloperName = 'Activity_Scheduling'].Id;					
				
		String JSON_Message = JSON.serialize(evnt);
		req.requestBody = Blob.valueOf(JSON_Message);
		
		RestContext.request = req;
		RestContext.response = res;	
			
		ws_ActivityScheduling_Event.doUpsert();
		
		System.assert(res.statusCode == 400);
		ws_ActivityScheduling_Event.SalesforceError result = (ws_ActivityScheduling_Event.SalesforceError) JSON.deserialize(res.responseBody.toString(), ws_ActivityScheduling_Event.SalesforceError.class);
						
		//DELETE		
		
		req = new RestRequest(); 
		res = new RestResponse();
		
		req.requestURI = '/services/apexrest/ActivitySchedulingEventService';
		req.params.put('id', null);  
		req.httpMethod = 'DELETE';	
		
		RestContext.request = req;
		RestContext.response = res;	
			
		ws_ActivityScheduling_Event.doDelete();
			
		System.assert(res.statusCode == 400);		
		result = (ws_ActivityScheduling_Event.SalesforceError) JSON.deserialize(res.responseBody.toString(), ws_ActivityScheduling_Event.SalesforceError.class);			
	}
	
	private static testmethod void testQueryWS(){
		
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();
	
		req.requestURI = '/services/apexrest/ActivitySchedulingService';  
		req.httpMethod = 'GET';
		req.params.put('query', 'Select Id, Name from Account');  
		
		RestContext.request = req;
		RestContext.response = res;
		
		ws_Activity_Scheduling.doGet();
		
		List<Account> result = (List<Account>) JSON.deserialize(res.responseBody.toString(), List<Account>.class);
		
		System.assert(result.size() == 1);
		System.assert(result[0].Name == 'Test Account');
	}
	
	private static testmethod void testQueryWS_Error(){
		
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();
	
		req.requestURI = '/services/apexrest/ActivitySchedulingService';  
		req.httpMethod = 'GET';
		req.params.put('query', 'Select Id, Name from Acc');  
		
		RestContext.request = req;
		RestContext.response = res;
		
		ws_Activity_Scheduling.doGet();
		
		System.assert(res.statusCode == 400);
		ws_Activity_Scheduling.SalesforceError result = (ws_Activity_Scheduling.SalesforceError) JSON.deserialize(res.responseBody.toString(), ws_Activity_Scheduling.SalesforceError.class);
	}
	
	@TestSetup
	private static void setData() {
		
		Mobile_App__c app = new Mobile_App__c();
		app.Name = 'Activity Scheduling Tool';
		app.Active__c = true;
		app.Unique_Key__c = 'ACT_SCHDL';
		insert app;
		
		Implant_Scheduling_Team__c team = new Implant_Scheduling_Team__c();			
		team.Name = 'Test Team';
		team.Team_Color__c = 'rgb(99, 99, 99)';
		team.Work_Day_End__c = '18';
		team.Work_Day_Start__c = '8';
		team.Working_Days__c = '1:5';			
		team.RecordTypeId = [Select Id from RecordType where SObjectType = 'Implant_Scheduling_Team__c' AND DeveloperName = 'Activity_Scheduling_Team'].Id;			
		insert team;
		
		User otherUser = [Select Id from User where Profile.Name LIKE 'System Administrator%' AND Id != :UserInfo.getUserId() AND isActive = true LIMIT 1];
		
		Implant_Scheduling_Team_Member__c member = new Implant_Scheduling_Team_Member__c();
		member.Team__c = team.Id;
		member.Member__c = otherUser.Id;				
		insert member;
		
		Account acc = new Account();
		acc.Name = 'Test Account';
		acc.Account_Country_vs__c = 'NETHERLANDS';
		insert acc;
	}
    
}