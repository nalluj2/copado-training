/**
 * Creation Date:   2013115
 *
 * Description:     Custom helper class which looks for accounts by a given Lead id. This search
 *				    uses the sharing rules for the current user.
 *              
 * Author:  Fabrizio Truzzu
 */

public with sharing class bl_Account_With_Sharing {
	public static List<Account> getAccountByEmail(string id){
  		return [select a.Name,a.Id, a.SAP_ID__c
  				from Account a
  				where a.Account_Email__c =: [select l.Email
		  			   		  			from Lead l
		  								where l.Id =: id].Email];
  	}
  	
  	public static List<Account> getAccountByName(string id){  		
  		return [select a.Name,a.Id, a.SAP_ID__c
  				from Account a
  				where a.Name =: [select l.Name
  				                 from Lead l
		  						 where l.Id =: id].Name];
  	}
  	
  	public static List<Account> getAccountSuggestions(string searchValue){
  		return [select a.Name,a.Id,a.SAP_Id__c,a.Account_City__c,a.Type
  				from Account a
  				where a.Name like : '%'+searchValue+'%' 
  				and a.IspersonAccount = true];
  	}
}