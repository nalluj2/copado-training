@RestResource(urlMapping='/ReadEntity/*')
global class TwoRing_ReadEntityController {
	@HttpGet
	global static SObject readEntity() {
		Pattern listTeamMemberPattern = Pattern.compile('/ReadEntity/(.*)');
		Matcher match = listTeamMemberPattern.matcher(RestContext.request.requestURI);
		if(match.matches()) {
			return TwoRing_Repository.readEntity(match.group(1));
		}
		else {
			throw new TwoRing_ApplicationException('Invalid url');
		}
		
	}
}