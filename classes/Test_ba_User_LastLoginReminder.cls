//------------------------------------------------------------------------------------------------------------------------------
//  Author      : Bart Caelen
//  Date        : 20150408
//  Description : TEST Class for ba_User_LastLoginReminder
//------------------------------------------------------------------------------------------------------------------------------
@isTest(SeeAllData=true)
private class Test_ba_User_LastLoginReminder {
	
    private static List<User> lstUser = new List<User>();
    private static List<User_Last_Login_Reminder__c> lstUserLastLoginReminder = new List<User_Last_Login_Reminder__c>();

    private static void createTestData() {

        //----------------------------------------------------
        // Create Test Data
        //----------------------------------------------------
        lstUserLastLoginReminder = new List<User_Last_Login_Reminder__c>();
        User_Last_Login_Reminder__c oULLR1 = new User_Last_Login_Reminder__c();
            oULLR1.Name = 'TEST 1';
            oULLR1.Country_Name__c = 'GERMANY';
            oULLR1.Profile_ID__c = '00e20000000nIqQAAU';    
            oULLR1.Inactive_Days__c = 45;
            oULLR1.Additional_CC__c = 'cc@medtronic.com';
            oULLR1.Additional_BCC__c = 'bcc@medtronic.com';
            oULLR1.Notify_Manager__c = true;
            oULLR1.Email_Template_Developer_Name__c = 'User_Last_Login_Reminder_CANADA_45';
            oULLR1.Organization_Wide_Email_Address__c = 'sfe@medtronic.com';
        lstUserLastLoginReminder.add(oULLR1);    
        User_Last_Login_Reminder__c oULLR2 = new User_Last_Login_Reminder__c();
            oULLR2.Name = 'TEST 2';
            oULLR2.Country_Name__c = 'GERMANY';
            oULLR2.Inactive_Days__c = 25;
            oULLR2.Additional_CC__c = 'cc@medtronic.com';
            oULLR2.Additional_BCC__c = 'bcc@medtronic.com';
            oULLR2.Notify_Manager__c = true;
            oULLR2.Email_Template_Developer_Name__c = 'User_Last_Login_Reminder_EUR_SB_25';
            oULLR2.Organization_Wide_Email_Address__c = 'sfe@medtronic.com';
        lstUserLastLoginReminder.add(oULLR2);    
        User_Last_Login_Reminder__c oULLR3 = new User_Last_Login_Reminder__c();
            oULLR3.Name = 'TEST 3';
            oULLR3.Profile_ID__c = '00e20000000nIqQAAU';
            oULLR3.Inactive_Days__c = 60;
            oULLR3.Additional_CC__c = 'cc@medtronic.com';
            oULLR3.Additional_BCC__c = 'bcc@medtronic.com';
            oULLR3.Notify_Manager__c = true;
            oULLR3.Email_Template_Developer_Name__c = 'User_Last_Login_Reminder_EUR_SB_60';
            oULLR3.Organization_Wide_Email_Address__c = 'sfe@medtronic.com';
        lstUserLastLoginReminder.add(oULLR3);    
        insert lstUserLastLoginReminder;

        lstUser = 
            [
                SELECT Id, Last_Application_Login__c, Country, Manager.Email
                FROM User
                WHERE IsActive = TRUE
                    AND Last_Application_Login__c != null
                    AND License_Name__c = 'Salesforce'
                    AND Country = 'GERMANY'
                LIMIT 50
            ];
        //----------------------------------------------------

    }      
	
    @isTest static void test_ba_User_LastLoginReminder_Scheduling() {

        //---------------------------------------
        // TEST SCHEDULING
        //---------------------------------------
        String tCRON_EXP = '0 0 0 3 9 ? 2099';
        
        String tJobId = System.schedule('ba_User_LastLoginReminder_TEST', tCRON_EXP, new ba_User_LastLoginReminder());

        // Get the information from the CronTrigger API object
        CronTrigger oCronTrigger = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :tJobId];

        // Verify the expressions are the same
        System.assertEquals(tCRON_EXP, oCronTrigger.CronExpression);

        // Verify the job has not run
        System.assertEquals(0, oCronTrigger.TimesTriggered);

        // Verify the next time the job will run
        System.assertEquals('2099-09-03 00:00:00', String.valueOf(oCronTrigger.NextFireTime));
        //---------------------------------------


	}

    @isTest static void test_ba_User_LastLoginReminder_Batch1() {
        
        createTestData();

        //---------------------------------------
        // TEST BATCH
        //---------------------------------------
        Test.startTest();
        ba_User_LastLoginReminder oBatch1 = new ba_User_LastLoginReminder();
        oBatch1.lstUserLastLoginReminder = lstUserLastLoginReminder;
        oBatch1.iCounter = 0;
        ID idBatchProcessid = Database.executeBatch(oBatch1);
        Test.stopTest();
        //---------------------------------------

    }
	
    @isTest static void test_ba_User_LastLoginReminder_Batch2() {
        
        createTestData();

        //---------------------------------------
        // TEST BATCH
        //---------------------------------------
        Test.startTest();
        ba_User_LastLoginReminder oBatch2 = new ba_User_LastLoginReminder();
        oBatch2.lstUserLastLoginReminder = lstUserLastLoginReminder;
        oBatch2.iCounter = 1;
        oBatch2.execute(null);
        Test.stopTest();
        //---------------------------------------

    }

    @isTest static void test_ba_User_LastLoginReminder_Batch3() {

        createTestData();
        
        //---------------------------------------
        // TEST BATCH
        //---------------------------------------
        Test.startTest();
        ba_User_LastLoginReminder oBatch3 = new ba_User_LastLoginReminder();
        oBatch3.lstUserLastLoginReminder = lstUserLastLoginReminder;
        oBatch3.iCounter = 2;
        oBatch3.execute(null, lstUser);      
        Test.stopTest();
        //---------------------------------------

    }

}
//------------------------------------------------------------------------------------------------------------------------------