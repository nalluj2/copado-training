public with sharing class ctrlExt_OpportunityGrowthDriverNew {
	
	private Opportunity_Growth_Driver__c oppGrowthDriver;
	private Opportunity opp;
	
	public String growthDriverFilter {get; set;}
	
	public ctrlExt_OpportunityGrowthDriverNew(ApexPages.StandardController sc){
		
		if( !Test.isRunningTest()){
			sc.addFields(new List<String>{'Growth_Driver_Id__c'});
		}	
		
		oppGrowthDriver = (Opportunity_Growth_Driver__c) sc.getRecord();
		
		if(oppGrowthDriver.Opportunity__c == null){
			
			ApexPages.addMessage(new ApexPages.Message( ApexPages.Severity.Error, 'Opportunity Growth Driver can only be created from an Opportunity page'));
			return;
		}
		
		opp = [Select Id, Sub_Business_Unit_Id__c, Business_Unit_Group_Id__c from Opportunity where Id = :oppGrowthDriver.Opportunity__c];
		
		growthDriverFilter = ' WHERE Active__c = true';
		
		if(opp.Sub_Business_Unit_Id__c != null){
			
			growthDriverFilter += ' AND Sub_Business_Unit__c = \''+ opp.Sub_Business_Unit_Id__c + '\'';
			
		}else if(opp.Business_Unit_Group_Id__c != null){
			
			growthDriverFilter += ' AND Business_Unit_Group__c = \''+ opp.Business_Unit_Group_Id__c + '\'';			
		}
	}
}