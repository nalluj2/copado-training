/*
 *      Description : This is the APEX Class that holds Business Logic for the Asset_Request__c Object
 *      Version     : 1.0
 *      Author      : Bart Caelen
 *      Date        : 2014-07-31
*/
public  class bl_AssetRequest {

	//-BC - 20141128 - CR-6700 - START
	// Creates a new Asset Request record and (if wanted) it sends an Email to the responsible user
	//	This logic is used in the ctrlExt_AccountAssetList and ctrlExt_iPlan2AssetList
	public static boolean createAssetRequest(Asset_Request__c oAssetRequest, Asset_Request_Email__c oAssetRequestEmail, List<Attachment> lstAttachment, Boolean bSendEmail){

		Boolean bResult = true;
		Savepoint oSavepoint = Database.setSavepoint();

		try{

		    string tProductName = '';
		    string tProductCode = '';
		    string tProductFamily = '';
		    string tProductBusinessUnit = '';

	        if (bSendEmail){
	            // Check if we can send an email
	            try{
	                Messaging.reserveSingleEmailCapacity(2);
	            }catch(Exception oEX){ 
	                clsUtil.createVFError(oEX, 'createAssetRequest');  
	                return false;
	            }
	        }

	        // Get the Email Template
	        EmailTemplate oEmailTemplate;
        	List<EmailTemplate> lstEmailTemplate = [SELECT Id, HTMLValue FROM EmailTemplate WHERE DeveloperName = :oAssetRequestEmail.Email_Template_Name__c];
        	if (lstEmailTemplate.size() == 0){
	            clsUtil.createVFError('Email Template "' + oAssetRequestEmail.Email_Template_Name__c + '" not found.  Please contact your System Administrator.');  
                return false;
            }else{
            	oEmailTemplate = lstEmailTemplate[0];
            }

	        string body = oEmailTemplate.htmlvalue;

	        if (oAssetRequest.Product__c !=  null){
	            Product2 oProduct = [SELECT Name, ProductCode, Family, Business_Unit_ID__r.Name FROM Product2 WHERE Id = :oAssetRequest.Product__c];
	            tProductName = oProduct.name;
	            tProductCode = oProduct.ProductCode;
	            tProductFamily = oProduct.Family;
	            tProductBusinessUnit = oProduct.Business_Unit_ID__r.Name;
	        }

	        // Insert the Asset Request record
	            oAssetRequest.Product_Code__c = tProductCode;
	            oAssetRequest.Product_Family__c = tProductFamily;
	            oAssetRequest.Product_BU__c = tProductBusinessUnit;
	        insert oAssetRequest;

	        // Insert the Attachment
	        if (lstAttachment.size() > 0){
		        for (Attachment oAttachment : lstAttachment){
		        	oAttachment.ParentId = oAssetRequest.Id;
		        }
		        insert lstAttachment;
		    }
	            
	        if (bSendEmail){

	            // Select a Contact that will be use to "send" the Email with an Email Template
				Contact oContact = [Select Id from Contact where email LIKE '%@medtronic.com%' and SFDC_User__r.isActive = true LIMIT 1];

				Messaging.SingleEmailMessage oEmail_Dummy = new Messaging.SingleEmailMessage();
	                oEmail_Dummy.setTemplateId(oEmailTemplate.Id);
					oEmail_Dummy.setWhatId(oAssetRequest.Id);
					oEmail_Dummy.setTargetObjectId(oContact.Id);

				// Send the email but rollback the Savepoint so that the email is not send buyt we can use the generated email bodies
				Savepoint oSavePoint_Dummy = Database.setSavepoint();
					Messaging.sendEmail(new Messaging.SingleEmailMessage[] { oEmail_Dummy });
				Database.rollback(oSavePoint_Dummy);// Email will not send as it is rolled Back

	            List<String> lstAddresses_TO = new List<String>{oAssetRequestEmail.Email_Address__c};
	            List<String> lstAddresses_CC = new List<String>{Userinfo.getUserEmail()};   
		        
	            Messaging.SingleEmailMessage oEmail = new Messaging.SingleEmailMessage();
	                oEmail.setReplyTo(Userinfo.getUserEmail());
	                oEmail.setTOAddresses(lstAddresses_TO);
	                oEmail.setCCAddresses(lstAddresses_CC);
	                oEmail.setBCCSender(false);
	                oEmail.setUseSignature(false);
					oEmail.setPlainTextBody(oEmail_Dummy.getPlainTextBody());
					oEmail.setHTMLBody(oEmail_Dummy.getHTMLBody());
					oEmail.setSubject(oEmail_Dummy.getSubject());
                	oEmail.saveAsActivity = false;

                // Include Attachment in Email
                if (lstAttachment.size() > 0){
					
					List<Messaging.EmailFileAttachment> lstEmailFileAttachment = new List<Messaging.EmailFileAttachment>();
					
					for (Attachment oAttachment : lstAttachment){
						
						Messaging.EmailFileAttachment oEmailFileAttachment = new Messaging.EmailFileAttachment();
							oEmailFileAttachment.setFileName(oAttachment.Name);
							oEmailFileAttachment.setBody(oAttachment.Body);
						lstEmailFileAttachment.add(oEmailFileAttachment);
					}
					oEmail.setFileAttachments(lstEmailFileAttachment);	                

                }	

	            try{
	                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { oEmail });
	            }catch(Exception oEX){ 
	                clsUtil.createVFError(oEX, 'saveNewData');  
	                bResult = false;
	            }
	                
	        }	

        }catch(Exception oEX){
        	Database.rollback(oSavepoint);
        	bResult = false;
        }

        return bResult;
	
	}
	//-BC - 20141128 - CR-6700 - STOP


	public static List<SelectOption> lstSO_BusinessUnit = new List<SelectOption>();
	public static Map<String, Business_Unit__c> mapBusinessUnit = new Map<String, Business_Unit__c>();	//-BC - 20141128 - CR-6700 - Added

	// Loads the Business Unit data based on the provided Country and 
	//	- it populates the lstSO_BusinessUnit that is a List of Select Options that holds all Business Units that are related to the provided Country
	//	This logic is used in the ctrlExt_AccountAssetList and ctrlExt_iPlan2AssetList
    public static void loadBusinessUnitData(String tCountryName){

        // Initialize the Business Unit PickList
        lstSO_BusinessUnit = new List<SelectOption>();
        mapBusinessUnit = new Map<String, Business_Unit__c>();	//-BC - 20141128 - CR-6700 - Added

        // Get a list of Business Units related to the Master Data of the Country of the Account
        List<DIB_Country__c> lstCountry = [SELECT Id, Name, Company__c FROM DIB_Country__c WHERE Name =: tCountryName];
        if (lstCountry.size() > 0){
	        DIB_Country__c oDIBCountry = lstCountry[0];
            Id idCompany = oDIBCountry.Company__c;
            List<Business_Unit__c> lstBusinessUnit = [SELECT Id, Name FROM Business_Unit__c WHERE Company__c =: idCompany ORDER BY Name];

            lstSO_BusinessUnit.add(new SelectOption('NONE', '---NONE---'));
            for (Business_Unit__c oBU : lstBusinessUnit){
                mapBusinessUnit.put(oBU.Id, oBU);	//-BC - 20141128 - CR-6700 - Added
                lstSO_BusinessUnit.add(new SelectOption(oBU.Id, oBU.Name));

                if (setBUName_TypeVisible.contains(oBU.Name)){
                	setBUID_TypeVisible.add(oBU.Id);
                }
            }
	    }

    }

    private static Set<String> setBUName_TypeVisible = new Set<String>{'Cranial Spinal', 'ENT'};
    public static Set<Id> setBUID_TypeVisible = new Set<Id>();


    public static Asset_Request_Email__c getAssetRequestEmail(Asset_Request__c oAssetRequest, String tCountryName){

    	Asset_Request_Email__c oAssetRequestEmail;

    	System.debug('**BC** getAssetRequestEmail - oAssetRequest ' + oAssetRequest );

    	String tSOQL = 'SELECT Email_Address__c, Email_Template_Name__c, Country__c FROM Asset_Request_Email__c';

    	tSOQL += ' WHERE ';
    	if (String.isBlank(oAssetRequest.Business_Unit__c)){
    		tSOQL += ' (Business_Unit__c = null) ';
		}else{
			tSOQL += ' (Business_Unit__c = \'' + oAssetRequest.Business_Unit__c + '\') ';	
		}
		if (String.isBlank(oAssetRequest.Asset_Change_Type__c)){
			tSOQL += ' AND (Asset_Change_Type__c = null) ';
		}else{
			tSOQL += ' AND (Asset_Change_Type__c = \'' + oAssetRequest.Asset_Change_Type__c + '\') ';
		}

		System.debug('**BC** getAssetRequestEmail - tSOQL : ' + tSOQL);
    	List<Asset_Request_Email__c> lstAssetRequestEmail = Database.query(tSOQL);

    	if (lstAssetRequestEmail.size() > 0){
	    
	    	if (lstAssetRequestEmail.size() == 1){
	    
	    		oAssetRequestEmail = lstAssetRequestEmail[0];
	    
	    	}else{

	    		for (Asset_Request_Email__c oAssetRequestEmail_Temp : lstAssetRequestEmail){
	    			if (oAssetRequestEmail_Temp.Country__c == tCountryName){
	    				oAssetRequestEmail = oAssetRequestEmail_Temp;
	    				break;
	    			}
	    		}
	    	}
	    }

	    return oAssetRequestEmail;
    }


}