/**
 *      Created Date : 20091001
 *      Description : Class called after Subject Discussed are saved, updated or deleted 
 *          The Subjects discussed are shown in the Visit Report related list in the Contact screen.
 * 
 *      Author = Tuan Abdeen / ABSI 
 */
 
public class SubjectsDiscussed {
        
    private static final Integer ACTION_DELETE = 0;
    
    private static final Integer ACTION_UNDELETE = 1;
    
    private static final String SUBJECTS_SEPERATOR = '; ';
    
    private static String THERAPY_SEPERATOR= ';';

    /**
     * Update Subjec
     * @param Subject_Discussed__c
     */
     
    //public static String theraphy ='';
     
    public static void updateSubjectDiscussed(Call_Topics__c[] subjectsDiscussed, Integer action) {
        
        // Unique visit report IDs for the subjects discussed
        Map<Id, Set<Id>> subjectDiscussedPerVisitReport = new Map<Id, Set<Id>>();
        Map<Id, Set<Id>> CallCategoryConcate = new Map<Id, Set<Id>>();
        Map<Id, Set<Id>> therapyPerVisitReport = new Map<Id, Set<Id>>();
        Map<Id,Integer> SBUIds= new Map<Id,Integer>();                
        SBUIds.clear();
        Id BuId;
        
        Set<Id> AccountIds = new Set<Id>();
        Set<Id> ContactIds = new Set<Id>();
        Date Calldate;
        
        // itirate through the subjects discussed and add the unique visit reports to the set
        //for(Call_Topics__c subjectDiscussed : subjectsDiscussed) {
            
            Id visitReportId = subjectsDiscussed[0].Call_Records__c;
            
            if(visitReportId <> null) {
                
                // Unique Subject IDs for the subjects discussed
                Set<Id> subjectIds = subjectDiscussedPerVisitReport.get(visitReportId);
                Set<Id> CallCateIds = CallCategoryConcate.get(visitReportId);
                Set<Id> therapyIds = therapyPerVisitReport.get(visitReportId);
           
                // if the collection is empty, construc a new one
                if(subjectIds == null) {
                    subjectIds = new Set<Id>();
                }
                if(CallCateIds == null){
                	CallCateIds = new Set<id>();
                }
                if (therapyIds == null){
                   therapyIds=new Set<ID>(); 
                }
                
                // adding subjects discussed to the collection
                //subjectIds.add(subjectDiscussed.Subject__c);
                
                Call_Topics__c[] arrSubDiscussed = [Select s.Call_Topic_Therapy__r.name, Call_Topic_Therapy__r.id, Call_Activity_Type__c,s.Call_Topic_Therapy__r.Sub_Business_Unit__c,s.Call_Topic_Therapy__r.Business_Unit__c,Call_Category__c From Call_Topics__c s WHERE s.Call_Records__c =: visitReportId];  
                //System.Debug('arrSubDiscussed - ' + arrSubDiscussed.size());                          
                for(Call_Topics__c SubDiss : arrSubDiscussed) {                
                    subjectIds.add(SubDiss.Call_Activity_Type__c);
                    CallCateIds.add(SubDiss.Call_Category__c);
                    therapyIds.add(SubDiss.Call_Topic_Therapy__c);
                    BuId=SubDiss.Call_Topic_Therapy__r.Business_Unit__c;
                    //System.Debug('Inside');
                    if(SBUIds.get(SubDiss.Call_Topic_Therapy__r.Sub_Business_Unit__c)!=null)
                    {
                        //System.Debug('My If');
                        integer i=SBUIds.get(SubDiss.Call_Topic_Therapy__r.Sub_Business_Unit__c);
                        if(SubDiss.Call_Topic_Therapy__r.Sub_Business_Unit__c!=null)
                        {
                            //SBUIds.put(SubDiss.Call_Topic_Therapy__r.Sub_Business_Unit__c,i+1);                        
                            SBUIds.put(SubDiss.Call_Topic_Therapy__r.Sub_Business_Unit__c,1);                                                    
                        }
                        else
                        {
                            SBUIds.put(SubDiss.Call_Topic_Therapy__r.Sub_Business_Unit__c,0);                        
                        }                            
                    }                        
                    else
                    {
                        //System.Debug('My Else');
                        if(SubDiss.Call_Topic_Therapy__r.Sub_Business_Unit__c!=null)
                        {                        
                            SBUIds.put(SubDiss.Call_Topic_Therapy__r.Sub_Business_Unit__c,1);    
                        }                            
                        else
                        {
                            SBUIds.put(SubDiss.Call_Topic_Therapy__r.Sub_Business_Unit__c,0);                        
                        }                                                    
                    }
                }
                Contact_Visit_Report__c[] arrContact = [Select Attending_Contact__c,Attending_Affiliated_Account__c,Call_Date__c From Contact_Visit_Report__c WHERE Call_Records__c =: visitReportId];  
                for(Contact_Visit_Report__c ConDiss : arrContact) {                
                    AccountIds.add(ConDiss.Attending_Affiliated_Account__c);
                    ContactIds.add(ConDiss.Attending_Contact__c);
                    Calldate=ConDiss.Call_Date__c;                 
                }
                // putting Subjets discussed per Visit Report
                subjectDiscussedPerVisitReport.put(visitReportId, subjectIds);
                CallCategoryConcate.put(visitReportId, CallCateIds);
                therapyPerVisitReport.put(visitReportId,therapyIds);
            }
        
        // Calll Topic Insertions...
        System.Debug('Before If');
        if(action==2)
        {
            //System.Debug('After If');
            Boolean DateWithInYear=false;
            if(Calldate!=null){
                integer DaysBetween=Calldate.daysBetween(System.today()-365);
                System.Debug('DaysBetween - ' + DaysBetween);            
                if(DaysBetween<0)
                {
                    DateWithInYear=true;    
                }
                // Existing Account Deletion
                Account_Call_Topic_Count__c[] DelAccCallTopic = [Select Id From Account_Call_Topic_Count__c WHERE Call_Recording__c =: visitReportId];
                try {
                    Delete DelAccCallTopic;
                } catch (DmlException e){
                    System.debug('##############" Account Delete Error : "' + e); 
                }            
                // Existing Contact Deletion
                Contact_Call_Topic_Count__c[] DelConCallTopic = [Select Id From Contact_Call_Topic_Count__c WHERE Call_Recording__c =: visitReportId];
                try {
                    Delete DelConCallTopic;
                } catch (DmlException e){
                    System.debug('##############" Contact Delete Error : "' + e); 
                }            
    
                Set <Id> SBUId = new Set<Id>();
                SBUId = SBUIds.keySet();
                // Account Call Topic Count Insertion
                List<Account_Call_Topic_Count__c> AccCallTopic = new List<Account_Call_Topic_Count__c>();
                //List<Account> accCallTopicUpdate = new List<Account>(); 
                for(Id Acc:AccountIds) {
                    for(Id SBU : SBUId) {
                        Account_Call_Topic_Count__c AccCall = new Account_Call_Topic_Count__c
                        (Account__c = Acc, Call_Recording__c = visitReportId
                        ,Business_Unit__c=BuId,
                        Sub_Business_Unit__c=SBU,Total_Calls__c=SBUIds.get(SBU),active__c=DateWithInYear);
    
                        AccCallTopic.add(AccCall);
                    }
                }            
                try {
                    insert AccCallTopic;
                    //update accCallTopicUpdate;
                } catch (DmlException e){
                    System.debug('##############" Account Insert Error : "' + e); 
                }       
                // Contact Call Topic Count Insertion     
                List<Contact_Call_Topic_Count__c> ConCallTopic = new List<Contact_Call_Topic_Count__c>();
                //List<Contact> conCallTopicUpdate = new List<Contact>();                 
                for(Id cc:ContactIds) {
                    for(Id SBU : SBUId) {
                        Contact_Call_Topic_Count__c ConCall = new Contact_Call_Topic_Count__c
                        (Contact__c = cc, Call_Recording__c = visitReportId
                        ,Business_Unit__c=BuId,
                        Sub_Business_Unit__c=SBU,Total_Calls__c=SBUIds.get(SBU),active__c=DateWithInYear);
    
                        ConCallTopic.add(ConCall);                            
                    }        
                }            
                try {
                    insert ConCallTopic;
                    //update conCallTopicUpdate;
                } catch (DmlException e){
                    System.debug('##############" Contact Insert Error : "' + e); 
                }                        
            }
        }
        // the collection that holds the updated visit reports
        List<Call_Records__c> visitReports = new List<Call_Records__c>();
        
        // get the formatterd subjects discussed
        Map<Id, String> subjectDesc = getSubjectsDiscussed(subjectDiscussedPerVisitReport); 
        Map<Id, String> CallCateDesc = getCallCategory(CallCategoryConcate);         
        Map<Id, String> therapyLst= getTherapy(therapyPerVisitReport); 
        // Unique visit report IDs for the subjects discussed
        Set<Id> visitReportIds = subjectDiscussedPerVisitReport.keySet();
        
        if(ACTION_DELETE == action) {
            String findSubject ='';
            String findTheraphy='';
            for(Call_Topics__c subjectDiscussed : subjectsDiscussed) {
                Call_Topics__c[] arrSubDiscussed = [Select Call_Topic_Therapy__r.name,Call_Activity_Type__r.Name From Call_Topics__c WHERE id =: subjectDiscussed.id];            
                String subjects = arrSubDiscussed[0].Call_Activity_Type__r.Name;
                String theraphyName;
                if(arrSubDiscussed[0].Call_Topic_Therapy__c!=null)
                theraphyName =arrSubDiscussed[0].Call_Topic_Therapy__r.name;                
                //System.Debug('Before - ' + subjects);
                findSubject = subjects.trim()+SUBJECTS_SEPERATOR.trim();
                if(theraphyName!=null && Therapy_Seperator!=null)
                findTheraphy =theraphyName.trim()+THERAPY_SEPERATOR.trim();
                //System.Debug('After - ' + findSubject);                
            }    
            Call_Records__c[] arrVisitReports = [Select v.Therapy__c, v.Call_ActTypes_Concatenated__c From Call_Records__c v WHERE v.Id in: visitReportIds];
            
            for(Call_Records__c visitReport : arrVisitReports) {
                String strSubDiscussed = visitReport.Call_ActTypes_Concatenated__c;
                String strTherapy = visitReport.Therapy__c;
                
                String subjects = subjectDesc.get(visitReport.Id);
                String theraphy=''; 
                
                if (strTherapy<> null) {
                
                theraphy = strTherapy.replace(findTheraphy,'');
                visitReport.Therapy__c=theraphy;
                
                }
                
                if(strSubDiscussed <> null) {
                    System.Debug('strSubDiscussed - ' + strSubDiscussed);                                    
                    subjects = strSubDiscussed.replace(findSubject, '');
                    
                    System.Debug('After Delete - ' + subjects);                                    
                    
                    visitReport.Call_ActTypes_Concatenated__c = subjects;
                    
                    // adding the update
                    //visitReports.add(visitReport);
                }
                
                 visitReports.add(visitReport);
            }
            
        } else if(ACTION_UNDELETE == action) {
            
            Call_Records__c[] arrVisitReports = [Select v.Therapy__c, v.Call_ActTypes_Concatenated__c From Call_Records__c v WHERE v.Id in: visitReportIds];
            
            for(Call_Records__c visitReport : arrVisitReports) {
                
                String strSubDiscussed = visitReport.Call_ActTypes_Concatenated__c;
                String subjects = subjectDesc.get(visitReport.Id);
                String theraphy = visitReport.Therapy__c;
                if(strSubDiscussed <> null) {
                    //subjects = strSubDiscussed + ' ' + subjects;
                    subjects = subjects;
                } else {
                    subjects = subjects;
                }
                visitReport.Call_ActTypes_Concatenated__c = subjects;
                
                if (theraphy<>null) {
                    visitReport.Therapy__c=theraphy;
                }    
                // adding the update
                visitReports.add(visitReport);
            }
            
        } else {
            
            // itirating throught the visit reports
            for(Id visitReportId1 : visitReportIds) {
      
                String subjects = subjectDesc.get(visitReportId1);
                string strCallCategory = CallCateDesc.get(visitReportId1);
                String therapys = therapyLst.get(visitReportId1);
                
                Call_Records__c visitReport = new Call_Records__c(Therapy__c=therapys,Call_ActTypes_Concatenated__c = subjects, Id = visitReportId1, Call_Category_Concate__c=strCallCategory);    
                // adding the update
                visitReports.add(visitReport);
            }
        }
        
        try {
            // update the visit reports to the database
            update visitReports;
            
        } catch (DmlException e){
            System.debug('##############" THIS IS THE ERROR : "' + e); 
        }
        
        // the collection that holds the updated contact visit reports
        List<Contact_Visit_Report__c> contactVisitReports = new List<Contact_Visit_Report__c>();
        
        // getting all the contact visit reports by visit report Ids
        Contact_Visit_Report__c[] conVisitReports = [SELECT c.Id, c.Therapy__c, c.Call_Records__c From Contact_Visit_Report__c c WHERE c.Call_Records__c IN: visitReportIds];
        
        // itirating throught the contact visit reports
        for(Contact_Visit_Report__c conVisitReport : conVisitReports) {
            
            String subjects = subjectDesc.get(conVisitReport.Call_Records__c);
            String therapyNew =therapyLst.get(conVisitReport.Call_Records__c);
            // update the contact visit report with the subject discussed
            Contact_Visit_Report__c contactVisitReport = new Contact_Visit_Report__c(Therapy__c=therapyNew,Subjects_Discussed__c = subjects, 
                Id = conVisitReport.Id);
                
            // adding the update
            contactVisitReports.add(contactVisitReport);
        }
        
        try {
            // update the contact visit reports to the database
            update contactVisitReports;
            
        } catch (DmlException e){
            System.debug('##############" THIS IS THE ERROR : "' + e); 
        } 
            
    }
    
    
    /**
     * getting the Subjects Discussed seperated by semi-colon
     * @param Map<Id, Set<Id>> subjectDiscussedPerVisitReport
     * @return Map<Id, String> - the formatted string
     */
     
    private static Map<Id, String> getSubjectsDiscussed(Map<Id, Set<Id>> subjectDiscussedPerVisitReport) {
        
        Map<Id, String> subjectDesc = new Map<Id, String>();
        Set<Id> allSubjectIds = new Set<Id>();
        
        // get the Visit Report IDs
        Set<Id> visitReportIds = subjectDiscussedPerVisitReport.keySet();
        
        // Itirate the IDs and get the Subject Discussed IDs
        for(Id visitReportId : visitReportIds) {
            allSubjectIds.addAll(subjectDiscussedPerVisitReport.get(visitReportId));
        }
        
        // get all the subjects by name and ID
        Call_Activity_Type__c[] subjects = [SELECT s.Name, s.Id FROM Call_Activity_Type__c s WHERE s.Id IN: allSubjectIds];
        
        // Itirate the IDs and get the Subject Discussed IDs
        for(Id visitReportId : visitReportIds) {
            Set<Id> subjectIds = subjectDiscussedPerVisitReport.get(visitReportId);
            
            String strSubjects = '';
            for(Id subjectId : subjectIds) {
                for(Call_Activity_Type__c subject : subjects) {
                    if(subject.Id == subjectId) {
                        
                        strSubjects += subject.Name + SUBJECTS_SEPERATOR;
            
                        subjectDesc.put(visitReportId, strSubjects);
                        break;
                    }
                }
            }
        }
        
        // return the final formatted string
        return subjectDesc;
    }

    private static Map<Id, String> getCallCategory(Map<Id, Set<Id>> CallCategoryConcate) {
        
        Map<Id, String> Callcategory = new Map<Id, String>();
        Set<Id> allCallCatIds = new Set<Id>();
        
        // get the Visit Report IDs
        Set<Id> visitReportIds = CallCategoryConcate.keySet();
        
        // Itirate the IDs and get the Subject Discussed IDs
        for(Id visitReportId : visitReportIds) {
            allCallCatIds.addAll(CallCategoryConcate.get(visitReportId));
        }
        
        // get all the subjects by name and ID
        Call_Category__c[] CallCate = [SELECT c.Name, c.Id FROM Call_Category__c c WHERE c.Id IN: allCallCatIds];
        
        // Itirate the IDs and get the Subject Discussed IDs
        for(Id visitReportId : visitReportIds) {
            Set<Id> CallCatIds = CallCategoryConcate.get(visitReportId);
            
            String strCallCategory = '';
            for(Id CalCateg : allCallCatIds) {
                for(Call_Category__c Callcat : CallCate) {
                    if(Callcat.Id == CalCateg) {
                        
                        strCallCategory += Callcat.Name + SUBJECTS_SEPERATOR;
            
                        Callcategory.put(visitReportId, strCallCategory);
                        break;
                    }
                }
            }
        }
        
        // return the final formatted string
        return Callcategory;
    }    
     private static Map<Id, String> getTherapy(Map<Id, Set<Id>> therapyPerVisitReport) {
        
        Map<Id, String> therapyLst = new Map<Id, String>();
        Set<Id> allTherapyIds = new Set<Id>();
        Set<Id> visitReportIds = therapyPerVisitReport.keySet();
        
         for(Id visitReportId : visitReportIds) {
            allTherapyIds.addAll(therapyPerVisitReport.get(visitReportId));
        }
        List<Therapy__c> therapies = [Select t.Id, t.Name From Therapy__c t where t.Id IN: allTherapyIds];
        for(Id visitReportId : visitReportIds) {
            Set<Id> therapyIds = therapyPerVisitReport.get(visitReportId);
            
            String strTherapy = '';
            for(Id therapyId : therapyIds) {
                for(Therapy__c therapy : therapies) {
                    if(therapy.Id == therapyId) {
                        
                        strTherapy += therapy.Name + THERAPY_SEPERATOR;
            
                        therapyLst.put(visitReportId, strTherapy);
                        break;
                    }
                }
            }
        }
        return therapyLst;
    }
    
}