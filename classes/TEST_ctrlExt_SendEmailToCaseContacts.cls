//--------------------------------------------------------------------------------------------------------------------------------
//  Author      :   Bart Caelen
//  Date        :   14/06/2017
//  Description :   APEX Test Class for the APEX Controller Extension "ctrlExt_SendEmailToCaseContacts".
//  Change Log  :    
//--------------------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_ctrlExt_SendEmailToCaseContacts {
	
	private static Case oCase;

	@isTest static void createTestData() {
		
		// Create Contact Data
		clsTestData_Contact.iRecord_Contact = 5;
		clsTestData_Contact.createContact();

		// Create Case Data
		clsTestData.createCaseData();
		oCase = clsTestData.oMain_Case;
		
		// Create Case Contact Roles Data
		List<Case_Contact_Roles__c> lstCaseContactRoles = new List<Case_Contact_Roles__c>();
		for (Contact oContact : clsTestData_Contact.lstContact){

			Case_Contact_Roles__c oCaseContactRoles = new Case_Contact_Roles__c();
				oCaseContactRoles.Case__c = clsTestData.oMain_Case.Id;
				oCaseContactRoles.Contact__c = oContact.Id;
				oCaseContactRoles.Role__c = 'Other';
			lstCaseContactRoles.add(oCaseContactRoles);

		}
		insert lstCaseContactRoles;

	}
	
	@isTest static void test_ctrlExt_SendEmailToCaseContacts() {

		//--------------------------------------------------
		// Create Test Data
		//--------------------------------------------------
		createTestData();
		//--------------------------------------------------


		//--------------------------------------------------
		// Perform Test
		//--------------------------------------------------
		Test.startTest();

    	PageReference oPageRef = Page.SendEmailToCaseContacts;
        Test.setCurrentPage(oPageRef);

        System.currentPageReference().getParameters().put('Id', oCase.Id);
        ctrlExt_SendEmailToCaseContacts oCtrlExt = new ctrlExt_SendEmailToCaseContacts(new ApexPages.StandardController(oCase));


        PageReference oPageRef_Back = oCtrlExt.backToCase();

        String tURL = oCtrlExt.tBuildURL();
        PageReference oPageRef_RedirectPage = oCtrlExt.redirectPage();
		
		Test.stopTest();
		//--------------------------------------------------


		//--------------------------------------------------
		// Validate Test
		//--------------------------------------------------
		System.assertNotEquals(oPageRef_Back, null);
		System.assert(oPageRef_Back.getUrl().containsIgnoreCase(oCase.Id));

		System.assert(tURL.startsWithIgnoreCase('/_ui/core/email/author/EmailAuthor?p3_lkid='));
		System.assertNotEquals(oPageRef_RedirectPage, null);
		//--------------------------------------------------

	}

	@isTest static void test_ctrlExt_SendEmailToCaseContacts_Error1() {

		//--------------------------------------------------
		// Create Test Data
		//--------------------------------------------------
		createTestData();
		//--------------------------------------------------


		//--------------------------------------------------
		// Perform Test
		//--------------------------------------------------
		Test.startTest();

		clsUtil.hasException1 = true;

    	PageReference oPageRef = Page.SendEmailToCaseContacts;
        Test.setCurrentPage(oPageRef);

        System.currentPageReference().getParameters().put('Id', oCase.Id);
        ctrlExt_SendEmailToCaseContacts oCtrlExt = new ctrlExt_SendEmailToCaseContacts(new ApexPages.StandardController(oCase));

		Test.stopTest();
		//--------------------------------------------------


		//--------------------------------------------------
		// Validate Test
		//--------------------------------------------------
        System.assertEquals(oCtrlExt.bError, true);
		//--------------------------------------------------

	}

	@isTest static void test_ctrlExt_SendEmailToCaseContacts_Error2() {

		//--------------------------------------------------
		// Create Test Data
		//--------------------------------------------------
		createTestData();
		//--------------------------------------------------


		//--------------------------------------------------
		// Perform Test
		//--------------------------------------------------
		Test.startTest();

        clsUtil.hasException2 = true;

    	PageReference oPageRef = Page.SendEmailToCaseContacts;
        Test.setCurrentPage(oPageRef);

        System.currentPageReference().getParameters().put('Id', oCase.Id);
        ctrlExt_SendEmailToCaseContacts oCtrlExt = new ctrlExt_SendEmailToCaseContacts(new ApexPages.StandardController(oCase));

        PageReference oPageRef_Back = oCtrlExt.backToCase();

        String tURL = oCtrlExt.tBuildURL();
        PageReference oPageRef_RedirectPage = oCtrlExt.redirectPage();
		
		Test.stopTest();
		//--------------------------------------------------


		//--------------------------------------------------
		// Validate Test
		//--------------------------------------------------
		System.assertNotEquals(oPageRef_Back, null);
		System.assert(oPageRef_Back.getUrl().containsIgnoreCase(oCase.Id));

		System.assert(String.isBlank(tURL));
		System.assertEquals(oPageRef_RedirectPage, null);
		//--------------------------------------------------
	}	
	
}
//--------------------------------------------------------------------------------------------------------------------------------