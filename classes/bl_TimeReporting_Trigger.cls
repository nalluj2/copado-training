//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 28-07-2016
//  Description      : APEX Class - Business Logic for tr_TimeReporting
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public without sharing class bl_TimeReporting_Trigger {

    //----------------------------------------------------------------------------------------------------------------
    // Copy fields on Time Reporting related to Kronos
    //----------------------------------------------------------------------------------------------------------------
    public static void copyTimeReportFieldsRelatedToKronos(List<Time_Reporting__c> lstTriggerNew){

		for (Time_Reporting__c oTimeReporting : lstTriggerNew){
    
        	oTimeReporting.K_Project__c = oTimeReporting.Kronos_Project__c;
        	oTimeReporting.K_Category__c = oTimeReporting.Kronos_Category__c;
    	
    	}    	
   
    }
    //----------------------------------------------------------------------------------------------------------------

/* CR-18226: Remove this functionality
    //----------------------------------------------------------------------------------------------------------------
    // Update the field Actual_Hours__c on Project__c when a Time_Reporting__c record related to a Project__c is "changed" :
    //	INSERTED - UNDELETED
    //	UPDATED (Hours__c, Change_Request__c, Project__c)
    //	DELETED 
    //----------------------------------------------------------------------------------------------------------------
    public static void updateActualHoursOnProject(List<Time_Reporting__c> lstTriggerNew, Map<Id, Time_Reporting__c> mapTriggerOld, String tAction){

    	Set<Id> setID_ChangeRequest_Processing = new Set<Id>();
    	Set<Id> setID_Project_Processing = new Set<Id>();

    	if (tAction == 'INSERT' || tAction == 'UNDELETE'){

    		for (Time_Reporting__c oTimeReporting : lstTriggerNew){

    			if (!clsUtil.isBlank(oTimeReporting.Project__c)){
    				setID_Project_Processing.add(oTimeReporting.Project__c);
    			}else if (!clsUtil.isBlank(oTimeReporting.Change_Request__c)){
    				setID_ChangeRequest_Processing.add(oTimeReporting.Change_Request__c);
    			}

    		}

		}else if (tAction == 'UPDATE'){

    		for (Time_Reporting__c oTimeReporting : lstTriggerNew){

    			Time_Reporting__c oTimeReporting_Old = mapTriggerOld.get(oTimeReporting.Id);

    			if (
    				(oTimeReporting.hours__c != oTimeReporting_Old.hours__c)
    				||
    				(oTimeReporting.Change_Request__c != oTimeReporting_Old.Change_Request__c)
    				||
    				(oTimeReporting.Project__c != oTimeReporting_Old.Project__c)
				){

    				if (oTimeReporting.Project__c != oTimeReporting_Old.Project__c){
		    			if (!clsUtil.isBlank(oTimeReporting.Project__c)){
		    				setID_Project_Processing.add(oTimeReporting.Project__c);
	    				}
		    			if (!clsUtil.isBlank(oTimeReporting_Old.Project__c)){
		    				setID_Project_Processing.add(oTimeReporting_Old.Project__c);
	    				}
	    			}

	    			if (oTimeReporting.Change_Request__c != oTimeReporting_Old.Change_Request__c){
		    			if (!clsUtil.isBlank(oTimeReporting.Change_Request__c)){
	    					setID_ChangeRequest_Processing.add(oTimeReporting.Change_Request__c);
		    			}
		    			if (!clsUtil.isBlank(oTimeReporting_Old.Change_Request__c)){
	    					setID_ChangeRequest_Processing.add(oTimeReporting_Old.Change_Request__c);
		    			}
	    			}

	    			if (oTimeReporting.hours__c != oTimeReporting_Old.hours__c){
		    			if (!clsUtil.isBlank(oTimeReporting.Project__c)){
		    				setID_Project_Processing.add(oTimeReporting.Project__c);
	    				}else if (!clsUtil.isBlank(oTimeReporting.Change_Request__c)){
	    					setID_ChangeRequest_Processing.add(oTimeReporting.Change_Request__c);
		    			}
	    			}

	    		}

    		}

		}else if (tAction == 'DELETE'){

			for (Time_Reporting__c oTimeReporting_Old : mapTriggerOld.values()){
    			if (!clsUtil.isBlank(oTimeReporting_Old.Project__c)){
    				setID_Project_Processing.add(oTimeReporting_Old.Project__c);
    			}else if (!clsUtil.isBlank(oTimeReporting_Old.Change_Request__c)){
    				setID_ChangeRequest_Processing.add(oTimeReporting_Old.Change_Request__c);
    			}
    		}

		}

		// Get all Project ID based on the collected Projects and Change Requests
		if (setID_ChangeRequest_Processing.size() > 0){
			List<Change_Request__c> lstChangeRequest = [SELECT Id, Project_Management__c FROM Change_Request__c WHERE Id = :setID_ChangeRequest_Processing AND Project_Management__c != null];
			for (Change_Request__c oChangeRequest : lstChangeRequest){
				setID_Project_Processing.add(oChangeRequest.Project_Management__c);
			}
		}

		// Get all Timesheeet records related to the collected Project ID's 
		// Loop through the collected Time_Reporting__c records and create a map with the Project Id and the sum of the hours related to that project
		Map<Id, Decimal> mapProjectId_Hours = new Map<Id, Decimal>();
		for (Time_Reporting__c oTimeReporting :  
			[
				SELECT Id, hours__c, Project__c, Change_Request__r.Project_Management__c
				FROM Time_Reporting__c
				WHERE hours__c != null AND (Project__c = :setID_Project_Processing OR Change_Request__r.Project_Management__c = :setID_Project_Processing)
				ORDER BY Project__c, Change_Request__r.Project_Management__c
			]
		){

			Id id_Project;
			if (!clsUtil.isBlank(oTimeReporting.Project__c)){
				id_Project = oTimeReporting.Project__c;
			}else{
				id_Project = oTimeReporting.Change_Request__r.Project_Management__c;
			}

			Decimal decHours = 0;
			if (mapProjectId_Hours.containsKey(id_Project)){
				decHours = mapProjectId_Hours.get(id_Project);
			}
			decHours += oTimeReporting.hours__c;
			mapProjectId_Hours.put(id_Project, decHours);
		}

		// Update the Actual_Hours__c on the Projects
		List<Project_Management__c> lstProject_Update = new List<Project_Management__c>(); 
		for (Id id_Project : mapProjectId_Hours.keySet()){

			Project_Management__c oProject = new Project_Management__c(id = id_Project, Actual_Hours__c = mapProjectId_Hours.get(id_Project));
			lstProject_Update.add(oProject);

		}

		update lstProject_Update;

    }
    //----------------------------------------------------------------------------------------------------------------
*/
}
//--------------------------------------------------------------------------------------------------------------------