@isTest
private with sharing class Test_ctrl_LayoutPreference {
	
	private static testmethod void testAutoLayoutPreferenceSF1(){
		
		User currentUser = new User(Id=UserInfo.getUserId());
		
		Profile nonMobileProfile = [Select Id, Name from Profile where Name = 'EUR Field Force RTG'];
		Profile mobileProfile = [Select Id, Name from Profile where Name ='EUR Field Force RTG - Mobile'];
		
		Mobile_App__c mApp = new Mobile_App__c();
		mApp.Name = 'UnitTest App';
		mApp.Active__c = true;		
		mApp.Unique_Key__c = 'UT-TEST';
		insert mApp;
		
		Profile_Mapping__c mapping = new Profile_Mapping__c();
		mapping.Mobile_App__c = mApp.Id;
		mapping.Source_Profile__c = nonMobileProfile.Name;
		mapping.Target_Profile__c = mobileProfile.Name;		
		insert mapping;
		
		CollaborationGroup sf1ChatterGroup = new CollaborationGroup();
		sf1ChatterGroup.Name = 'Salesforce1 - UnitTest';
		sf1ChatterGroup.CollaborationType = 'Public';
		sf1ChatterGroup.Description = 'Unit Test Group';
		
		insert sf1ChatterGroup;
		
		User testUser = new User();
		testUser.FirstName = 'Unit Test';
		testUser.LastName = 'User';
		testUser.ProfileId = nonMobileProfile.Id;
		testUser.CommunityNickname = 'uniTUser';
		testUser.Alias_unique__c = 'uniTUser123456';
		testUser.Username = 'unit.test.user@medtronic.com.unittest'; 
		testUser.Email = 'unit.test.user@medtronic.com';
		testUser.Alias = 'uniTUser';
		testUser.TimeZoneSidKey = 'Europe/Amsterdam'; 
		testUser.LocaleSidKey = 'en_US';
		testUser.EmailEncodingKey =  'ISO-8859-1';
		testUser.LanguageLocaleKey = 'en_US';
		testUser.CostCenter__c = '123456';
		
		insert testUser;
		
		SiteUrls__c config = new SiteUrls__c();
		config.Name = 'Support';
		config.Url__c = 'testurl';
		
		insert config;
		
		System.runAs(testUser){
			
			PageReference pr = Page.SwitchAutoLayoutPreference;
			pr.getParameters().put('isdtp', 'p1');			
			Test.setCurrentPageReference(pr);
			
			ctrl_AutoLayoutPreference controller = new ctrl_AutoLayoutPreference();
			
			System.assert(controller.offerSwitch == true);
			
			System.runAs(currentUser){
				testUser.ProfileId = mobileProfile.Id;
				update testUser;
			}
			
			controller.switchProfile();
			
			System.assert(controller.offerSwitch == false);
		}
	}
	
	private static testmethod void testAutoLayoutPreferenceSFDC(){
		
		User currentUser = new User(Id=UserInfo.getUserId());
		
		Profile nonMobileProfile = [Select Id, Name from Profile where Name = 'EUR Field Force RTG'];
		Profile mobileProfile = [Select Id, Name from Profile where Name ='EUR Field Force RTG - Mobile'];
						
		User testUser = new User();
		testUser.FirstName = 'Unit Test';
		testUser.LastName = 'User';
		testUser.ProfileId = mobileProfile.Id;
		testUser.Previous_Profile_Id__c = nonMobileProfile.Id;
		testUser.CommunityNickname = 'uniTUser';
		testUser.Alias_unique__c = 'uniTUser123456';
		testUser.Username = 'unit.test.user@medtronic.com.unittest'; 
		testUser.Email = 'unit.test.user@medtronic.com';
		testUser.Alias = 'uniTUser';
		testUser.TimeZoneSidKey = 'Europe/Amsterdam'; 
		testUser.LocaleSidKey = 'en_US';
		testUser.EmailEncodingKey =  'ISO-8859-1';
		testUser.LanguageLocaleKey = 'en_US';
		testUser.CostCenter__c = '123456';
		
		insert testUser;
		
		SiteUrls__c config = new SiteUrls__c();
		config.Name = 'Support';
		config.Url__c = 'testurl';
		
		insert config;
		
		System.runAs(testUser){
									
			ctrl_AutoLayoutPreference controller = new ctrl_AutoLayoutPreference();
			
			System.assert(controller.offerSwitch == true);
			
			System.runAs(currentUser){
				testUser.ProfileId = nonMobileProfile.Id;
				update testUser;
			}
			
			controller.switchProfile();
			
			System.assert(controller.offerSwitch == false);
		}
	}
	
	
	private static testmethod void testLayoutPreference(){
		
		Profile nonMobileProfile = [Select Id, Name from Profile where Name = 'EUR Field Force RTG'];
		Profile mobileProfile = [Select Id, Name from Profile where Name ='EUR Field Force RTG - Mobile'];
				
		User testUser = new User();
		testUser.FirstName = 'Unit Test';
		testUser.LastName = 'User';
		testUser.ProfileId = nonMobileProfile.Id;
		testUser.CommunityNickname = 'uniTUser';
		testUser.Alias_unique__c = 'uniTUser123456';
		testUser.Username = 'unit.test.user@medtronic.com.unittest'; 
		testUser.Email = 'unit.test.user@medtronic.com';
		testUser.Alias = 'uniTUser';
		testUser.TimeZoneSidKey = 'Europe/Amsterdam'; 
		testUser.LocaleSidKey = 'en_US';
		testUser.EmailEncodingKey =  'ISO-8859-1';
		testUser.LanguageLocaleKey = 'en_US';
		testUser.CostCenter__c = '123456';
		
		insert testUser;
		
		PageReference pr = Page.SwitchAutoLayoutPreference;
		
		String params = mobileProfile.Id+'&'+testUser.Id;
		pr.getParameters().put('params', CryptoUtils.encryptText(params));			
		
		Test.setCurrentPageReference(pr);
		
		ctrl_LayoutPreference controller = new ctrl_LayoutPreference();
		controller.initialize();
		
		testUser = [Select ProfileId, Previous_Profile_Id__c from User where Id=:testUser.Id];
		
		System.assert(testUser.profileId == mobileProfile.Id);
		System.assert(testUser.previous_Profile_Id__c == nonMobileProfile.Id);
		
	}	
	
}