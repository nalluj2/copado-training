public with sharing class ctrlExt_quickC2CRelationship {
	
	public Affiliation__c relationship {get; set;}
		
	public List<SelectOption> buOptions {get; set;}
	public List<SelectOption> therapyOptions {get; set;}
	public List<SelectOption> fromAccountOptions {get; set;}
	public List<SelectOption> toAccountOptions {get; set;}
	
	public Boolean isBUMandatory {get; set;}
	public Boolean isTherapyVisible {get; set;}	
	
	public Boolean isSaveError {get; set;}
	
	public ctrlExt_quickC2CRelationship(ApexPages.StandardController sc){
		
		User currentUser = [Select Id, Name, CountryOR__c from User where Id =: UserInfo.getUserId()];
				
		relationship = new Affiliation__c();
		
		relationship.Affiliation_From_Contact__c = sc.getId();
		loadFromAccountNames();
		
		relationship.Affiliation_Start_Date__c = Date.today();
		
		RecordType rt = [Select Id from RecordType where SObjectType = 'Affiliation__c' AND DeveloperName = 'C2C'];
		relationship.RecordTypeId = rt.Id;
				
		isBUMandatory = !bl_Relationship.isBusinessUnitNotMandatory(currentUser.CountryOR__c);	
		isTherapyVisible = !bl_Relationship.hideTherapy(currentUser.CountryOR__c);
		
		if( Schema.sObjectType.Affiliation__c.fields.Business_Unit__c.isCreateable()){
			loadBUs();
			
			if(Schema.sObjectType.Affiliation__c.fields.Therapy__c.isCreateable() && isTherapyVisible){			
				onBUChange();
			}
		}
	}
	
	public void onBUChange(){
		
		relationship.Therapy__c = null;
		loadTherapies();
	}
	
	private void loadFromAccountNames() {
                                
        fromAccountOptions = new List<SelectOption>();        
        fromAccountOptions.add(new SelectOption('', '--None--'));
                 
        for(Affiliation__c aff : [SELECT Affiliation_To_Account__r.Name, Affiliation_To_Account__c, Affiliation_Primary__c FROM Affiliation__c
                                        where RecordTypeId =: FinalConstants.recordTypeIdC2A
                                        and Affiliation_Active__c = true
                                        and (Affiliation_Type__c <> 'Referring')
                                        and Affiliation_From_Contact__c=:relationship.Affiliation_From_Contact__c
                                        and Affiliation_To_Account__r.Id !=null ORDER BY Affiliation_To_Account__r.Name]){
                                        	
            String id = aff.Affiliation_To_Account__c;
            String name = aff.Affiliation_To_Account__r.Name;
                            
            fromAccountOptions.add(new SelectOption(id,name));
            
            if(aff.Affiliation_Primary__c == true){
            	relationship.Affiliation_From_Account__c = id;
            }
        }                        
    }
    
    public void loadToAccountNames() {
    	
        toAccountOptions = new List<SelectOption>();
        toAccountOptions.add(new SelectOption('', '--None--'));
        
        for(Affiliation__c aff : [SELECT Affiliation_To_Account__r.Name, Affiliation_To_Account__c, Affiliation_Primary__c FROM Affiliation__c
                                        where RecordTypeId =: FinalConstants.recordTypeIdC2A
                                        and Affiliation_Active__c = true
                                        and (Affiliation_Type__c <> 'Referring')
                                        and Affiliation_From_Contact__c=:relationship.Affiliation_To_Contact__c
                                        and Affiliation_To_Account__r.Id !=null ORDER BY Affiliation_To_Account__r.Name]){
                                        	
            String id = aff.Affiliation_To_Account__c;
            String name = aff.Affiliation_To_Account__r.Name;
                            
            toAccountOptions.add(new SelectOption(id,name));
            
            if(aff.Affiliation_Primary__c == true){
            	relationship.Affiliation_To_Account__c = id;
            }
        }
    } 
	
	private void loadBUs() {
        
        buOptions = new List<SelectOption>();   
        relationship.Business_Unit__c = null;
        
        // Check User BU
        Map<Id, String> userBUs = SharedMethods.getUserBusinessUnits();
        
        buOptions.add(new SelectOption('','--None--'));  
    
        if(userBUs.size()>0){
                        
            for(Id BuId : userBUs.keySet()){
                buOptions.add(new SelectOption(BuId,UserBUs.get(BuId)));                
            }
            
            Id IdUserPrimaryBU = SharedMethods.getUserPrimaryBU(); 
            
            if(IdUserPrimaryBU!=null){
            	relationship.Business_Unit__c = IdUserPrimaryBU;  
            }                       
        }
        
        SelectOptionSorter.doSort(BUOptions, SelectOptionSorter.FieldToSort.Label);   
    }
    
    private void loadTherapies() {
               
        therapyOptions = SharedMethods.getTherapies(relationship.Business_Unit__c);
        
        // If only one therapy, set it by default ! 
        if (therapyOptions.size() == 2){
            relationship.Therapy__c = therapyOptions[1].getValue() ; 
        }           
    }
	
	public void save(){
		
		isSaveError = false;
		
		if(relationship.Affiliation_Start_Date__c == null){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Start Date is mandatory'));
			isSaveError = true;
		}
		
		if(relationship.Affiliation_Type__c == null){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Relationship Type is mandatory'));
			isSaveError = true;
		}
		
		if(relationship.Affiliation_From_Contact__c == null || String.valueOf(relationship.Affiliation_From_Contact__c) == ''){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'From Contact is mandatory'));
			isSaveError = true;
		}
		
		if(relationship.Affiliation_From_Account__c == null || String.valueOf(relationship.Affiliation_From_Account__c) == ''){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'From/Child Account is mandatory'));
			isSaveError = true;
		}
		
		if(relationship.Affiliation_To_Contact__c == null || String.valueOf(relationship.Affiliation_To_Contact__c) == ''){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'To Contact is mandatory'));
			isSaveError = true;
		}
		
		if(relationship.Affiliation_To_Account__c == null || String.valueOf(relationship.Affiliation_To_Account__c) == ''){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'To/Parent Account is mandatory'));
			isSaveError = true;
		}
		
		if( Schema.sObjectType.Affiliation__c.fields.Business_Unit__c.isCreateable() && isBUMandatory == true &&  (relationship.Business_Unit__c == null || String.valueOf(relationship.Business_Unit__c) == '')){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Business Unit is mandatory'));
			isSaveError = true;
		}
		
		if( Schema.sObjectType.Affiliation__c.fields.Therapy__c.isCreateable() && isTherapyVisible && (relationship.Therapy__c == null || String.valueOf(relationship.Therapy__c) == '')){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Therapy is mandatory'));
			isSaveError = true;
		}
		
		if (relationship.Affiliation_To_Contact__c != null && relationship.Affiliation_From_Contact__c == relationship.Affiliation_To_Contact__c) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,Label.Error_Message_when_from_contact_and_to_Conatct_are_same));
			isSaveError = true;
        }	
        
        if(isSaveError == false){
        	             
            List<Affiliation__c> AffListSize = [Select Id from Affiliation__c where Affiliation_From_Contact__c=:relationship.Affiliation_From_Contact__c 
            										AND Affiliation_From_Account__c=:relationship.Affiliation_From_Account__c 
            										AND Affiliation_To_Contact__c=:relationship.Affiliation_To_Contact__c 
            										ANd Affiliation_To_Account__c=:relationship.Affiliation_To_Account__c 
            										AND Business_Unit__c=:relationship.Business_Unit__c AND Therapy__c=:relationship.Therapy__c 
            										AND Affiliation_Type__c=:relationship.Affiliation_Type__c LIMIT 1];    
            
            if(affListSize.size()>0){
            	
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.This_Relationship_is_already_existing));
                isSaveError = true;                
            }
        }	
		
		if(isSaveError) return;
		
		try{
		
			insert relationship;
			
		}catch(Exception e){
			ApexPages.addMessages(e);
			isSaveError = true;
		}
	}
}