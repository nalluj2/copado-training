/**
 * Created by dijkea2 on 26-4-2017.
 */

global class ba_Equipment_IHS implements Database.Batchable<SObject>, Schedulable, Database.Stateful  {

    global Integer iBatchSize = 2000;
        
    public Boolean notification_per_email = false;
    public String emailToAddress = UserInfo.getUserEmail();
    public Integer countRecords = 0;
    public Integer iCasesCreated = 0;

    // SCHEDULE Settingsa
    global void execute(SchedulableContext sc){

        ba_Equipment_IHS oBatch = new ba_Equipment_IHS();

        Id batchJobId = Database.executebatch(oBatch, iBatchSize);
        System.Debug('Batch Job Id: ' + batchJobId);
    }

    global Database.QueryLocator start(Database.BatchableContext ctx){
		
		// We create the Cases for Equipment with their next due dates within the next 3 weeks
        Date dStart = Date.today();
        Date dEnd = Date.today().addDays(21);

        String query = '';
        query += 'SELECT Id,Name, Account__c, Account__r.Account_Country_vs__c, Supplier__c,Supplier_Contract__c, Serial_Number__c, Next_EST_Date__c, Next_PM_Date__c ';
        query += ', (SELECT Id, Failure_Type__c, IHS_Eqmt_Next_EST_Date__c, IHS_Eqmt_Next_PM_Date__c FROM Cases__r WHERE Status = \'New\' AND Failure_Type__c IN (\'Preventive Maintenance\', \'Electrical Safety Inspection\') AND Origin = \'Internal\') ';
        //query += ', (SELECT Id,Name, Contract_IHS__r.Start_Date__c, Contract_IHS__r.End_Date__c, Contract_IHS__r.Active__c, Contract_IHS__r.Contract_Type__c FROM Contract_Equipment_Details_IHS__r)';
        query += 'FROM Equipment_IHS__c ';
        query += 'WHERE( ';
            query += '(Next_PM_Date__c >= :dStart AND Next_PM_Date__c <= :dEnd) ';
            query += 'OR ';
            query += '(Next_EST_Date__c >=  :dStart AND Next_EST_Date__c <= :dEnd) ';
        query += ') ';
        query += 'AND Active__c = true ';

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext ctx, List<Equipment_IHS__c> lstEquipment){
        
        List<Case> lstCase = new List<Case>();
        
        Date dStart = Date.today();
        Date dEnd = Date.today().addDays(21);
                
        for(Equipment_IHS__c oEquipment : lstEquipment){
        	        	
        	Boolean bCreatePMCase = false;
        	Boolean bCreateESTCase = false;
        	
            countRecords++;
            
            // PM Date within scope date range
            if(oEquipment.Next_PM_Date__c != null && oEquipment.Next_PM_Date__c >= dStart && oEquipment.Next_PM_Date__c <= dEnd){
            	
            	// We create a Case unless we find an existing Case for PM in this Equipment
            	bCreatePMCase = true;
            	
            	for(Case oCaseRecord : oEquipment.Cases__r){
            		
            		// We found an existing open Case for PM
            		if(oCaseRecord.Failure_Type__c == 'Preventive Maintenance') bCreatePMCase = false;                    
            	}
            }
            
            // EST Date within scope date range
            if(oEquipment.Next_EST_Date__c != null && oEquipment.Next_EST_Date__c >= dStart && oEquipment.Next_EST_Date__c <= dEnd){
            	
            	// We create a Case unless we find an existing Case for EST in this Equipment
            	bCreateESTCase = true;
            	
            	for(Case oCaseRecord : oEquipment.Cases__r){
            		
            		// We found an existing open Case for EST
            		if(oCaseRecord.Failure_Type__c == 'Electrical Safety Inspection') bCreateESTCase = false;                    
            	}
            }
            
            if(bCreatePMCase){
            	
                Case oCase = createCase(oEquipment, 'PM');
                oCase.IHS_Eqmt_Next_PM_Date__c = oEquipment.Next_PM_Date__c;
                lstCase.add(oCase);
            }
            
            if(bCreateESTCase){
                
                Case oCase = createCase(oEquipment, 'EST');
                oCase.IHS_Eqmt_Next_EST_Date__c = oEquipment.Next_EST_Date__c;
                lstCase.add(oCase);
            }            
        }

        if(lstCase.size() > 0){
                                    
            insert lstCase;
            
            iCasesCreated += lstCase.size();
        }
    }

    global void finish(Database.BatchableContext BC){
        
        String msg = getMessage(BC);

        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        String[] toAddresses = new List<String>{emailToAddress}; // By default current User

        email.setSubject('Created Cases for IHS Equipment');
        email.setToAddresses( toAddresses );
        email.setPlainTextBody(msg);

        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{email});
	}


    //
    // Private
    //

    private static Id idRecordType_Case_IHS = clsUtil.getRecordTypeByDevName('Case', 'IHS_Temporary_Solution').Id;
    private static Id idCaseQueueOther = [select Id, Name from Group where Name = 'IHS Equipment Mgmt Queue' AND Type = 'Queue' LIMIT 1].Id;
    private static Id idCaseQueueItaly = [select Id, Name from Group where Name = 'IHS Equipment Mgmt Italy Queue' AND Type = 'Queue' LIMIT 1].Id;

    private static Case createCase(Equipment_IHS__c oEquipment, String type){
		
		/*
        Map<String, List<Contract_IHS__c>> mapContractsByType = new Map<String, List<Contract_IHS__c>>();

        for(Contract_Equipment_Detail_IHS__c oContractEquipment : oEquipment.Contract_Equipment_Details_IHS__r){
            
            System.Debug('Contract Id: ' + oContractEquipment.Contract_IHS__r.Id);
            System.Debug('Contract_Type__c: ' + oContractEquipment.Contract_IHS__r.Contract_Type__c);
            System.Debug('Active__c: ' + oContractEquipment.Contract_IHS__r.Active__c);
            System.Debug('Start_Date__c: ' + oContractEquipment.Contract_IHS__r.Start_Date__c);
            System.Debug('End_Date__c: ' + oContractEquipment.Contract_IHS__r.End_Date__c);

            String key = oContractEquipment.Contract_IHS__r.Contract_Type__c;
            
            Contract_IHS__c obj = oContractEquipment.Contract_IHS__r;
            
            if(mapContractsByType.containsKey(key)){
                mapContractsByType.get(key).add(obj);
            } else {
                mapContractsByType.put(key, new List<Contract_IHS__c>{obj});
            }
        }

        System.Debug('mapContractsByType: ' + mapContractsByType);
        System.Debug('mapContractsByType Size: ' + mapContractsByType.size());
		*/
		
        Case oCase = new Case();

        oCase.RecordTypeId = idRecordType_Case_IHS;
        oCase.AccountId = oEquipment.Account__c;
        oCase.Equipment_IHS__c = oEquipment.Id;
        oCase.Status = 'New';        
        oCase.Origin = 'Internal';
        oCase.Priority = 'Medium';
        
        // Failure Type
        if(type == 'PM') oCase.Failure_Type__c = 'Preventive Maintenance';
        else oCase.Failure_Type__c = 'Electrical Safety Inspection';
        
        // Owner Queue               
        if(oEquipment.Account__r.Account_Country_vs__c != null && oEquipment.Account__r.Account_Country_vs__c.toUpperCase() == 'ITALY') oCase.OwnerId = idCaseQueueItaly;
        else oCase.OwnerId = idCaseQueueOther;

// TODO: Select contract functionality
		/*
        List<Contract_IHS__c> lstContract;
        String dateField;

        if(type == 'PM'){
            lstContract = mapContractsByType.get('Maintenance');
            dateField = 'Next_PM_Date__c';
        } else if(type == 'EST'){
            lstContract = mapContractsByType.get('Electrical Safety');
            dateField = 'Next_EST_Date__c';
        }


        if(lstContract == null){
            lstContract = mapContractsByType.get('Maintenance incl Electrical Safety');
        }

        if(lstContract != null){
            for(Contract_IHS__c oContract : lstContract){
                Date dCaseDate = (Date)oEquipment.get(dateField);
                Date dStart = oContract.Start_Date__c;
                Date dEnd = oContract.End_Date__c;
                // look for the contract that expands the expends the case date
                if(dCaseDate > dStart && dCaseDate < dEnd){
                    // Make sure that the contract is active.
                    if(oContract.Active__c){
                    	oCase.Action_Type__c = 'Contract';
                        oCase.Supplier_Contract_Number__c = oContract.Id;
                    }
                }
            }
        } 
		*/
		
        return oCase;
    }

    private String getMessage(Database.BatchableContext BC){
        
        List<String> msgList = new List<String>();
        AsyncApexJob a =[
                SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Id,CompletedDate
                FROM AsyncApexJob
                WHERE Id = :BC.getJobId()
        ];
        Integer errorcount = 0;
        
        msgList.add('Case Report.');
        msgList.add('');

        msgList.add('We created Cases based on the next PM Date and next EST Data in Equipment IHS.');
        Date dStart = Date.today();
        Date dEnd   = Date.today().addDays(21);
        msgList.add('The period is from '+ dStart.format() + ' until ' + dEnd.format()  + '.');
        msgList.add('');

        msgList.add('The batch Job is executed and has status: '+a.Status);
        msgList.add('The batch apex job processed '+a.TotalJobItems + ' batches with '+a.NumberOfErrors + ' failures.');

        msgList.add('The Batch executed the following:');
        msgList.add('');

        msgList.add('Number of Records: '+ countRecords);
        msgList.add('Number of Cases created: '+ iCasesCreated);

        return String.join(msgList, '\n');
    }

}