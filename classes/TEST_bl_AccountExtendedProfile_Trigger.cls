//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 16-08-2017
//  Description      : APEX TEST Class for tr_AccountExtendedProfile and bl_AccountExtendedProfile_Trigger
//  Change Log       : PMT-22787: MITG EMEA User Setup - Added record type EMEA_SWOT_Analysis
//--------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_bl_AccountExtendedProfile_Trigger {
	
	private static List<Account_Extended_Profile__c> lstAccountExtendedProfile = new List<Account_Extended_Profile__c>();
	private static Set<String> setFieldName = new Set<String>{'Section_Owner_SI_AST_Driven__c','Commencement_Date__c','General_Surgery_procedures__c','Gynecologic_procedures__c','Head_Neck_procedures__c','Hemorrhoids_procedures__c'};
	private static Date dToday = Date.today();
	private static Account_Extended_Profile__c oAccountExtendedProfile;
	private static Account_Extended_Profile__c oAccountExtendedProfile_SWOT;
	private static User oUser_System = [SELECT Id, Manager_Email__c FROM User WHERE Alias = 'system'];

	//----------------------------------------------------------------------------------------------------------------
	// TEST DATA
	//----------------------------------------------------------------------------------------------------------------
	@isTest static void createTestData() {
		
		// Create Custom Setting Data
		clsTestData_CustomSetting.setFieldName_HistoryTracking_AccountExtendedProfile = setFieldName;
		clsTestData_CustomSetting.createHistoryTracking_AccountExtendedProfile();

		// Create Account Data
		clsTestData_Account.iRecord_Account_SAPAccount = 2;
		clsTestData_Account.createAccount_SAPAccount();

		// Create Account Extended Profile
		oAccountExtendedProfile  = new Account_Extended_Profile__c();
			oAccountExtendedProfile.RecordTypeId = clsUtil.getRecordTypeByDevName('Account_Extended_Profile__c', 'EMEA_Account_Extended_Profile').Id;
			oAccountExtendedProfile.Account__c = clsTestData_Account.oMain_Account_SAPAccount.Id;
			oAccountExtendedProfile.Commencement_Date__c = dToday;
			oAccountExtendedProfile.General_Surgery_procedures__c = 1;
			oAccountExtendedProfile.Gynecologic_procedures__c = 2;
			oAccountExtendedProfile.Head_Neck_procedures__c = 3;
			oAccountExtendedProfile.Hemorrhoids_procedures__c = 4;
			oAccountExtendedProfile.Section_Owner_Facility_Info__c = oUser_System.Id;
			oAccountExtendedProfile.Section_Owner_SI_AST_Driven__c = oUser_System.Id;
			oAccountExtendedProfile.Section_Owner_SI_GSP_Driven__c = oUser_System.Id;
			oAccountExtendedProfile.Section_Owner_SI_Hernia_Driven__c = oUser_System.Id;

		oAccountExtendedProfile_SWOT  = new Account_Extended_Profile__c();
			oAccountExtendedProfile_SWOT.RecordTypeId = clsUtil.getRecordTypeByDevName('Account_Extended_Profile__c', 'EMEA_SWOT_Analysis').Id;
			oAccountExtendedProfile_SWOT.Account__c = clsTestData_Account.oMain_Account_SAPAccount.Id;
			oAccountExtendedProfile_SWOT.Sub_Business_Unit_vs__c = 'ACC';
			oAccountExtendedProfile_SWOT.Strengths_Hernia__c = 'TEST Strengths Hernia';
			oAccountExtendedProfile_SWOT.Weaknesses_Hernia__c = 'TEST Weaknesses Hernia';
			oAccountExtendedProfile_SWOT.Threats_Hernia__c = 'TEST Threats Hernia';
			oAccountExtendedProfile_SWOT.Opportunities_Hernia__c = 'TEST Opportunities Hernia';

		lstAccountExtendedProfile.add(oAccountExtendedProfile);
		lstAccountExtendedProfile.add(oAccountExtendedProfile_SWOT);

	}
	//----------------------------------------------------------------------------------------------------------------


	//----------------------------------------------------------------------------------------------------------------
	// HISTORY TRACKING
	//----------------------------------------------------------------------------------------------------------------
	@isTest static void test_createHistoryTracking_INSERT() {

		// Create Test Data
		createTestData();
		List<History_Tracking__c> lstHistoryTracking = [SELECT Id FROM History_Tracking__c];
		System.assertEquals(lstHistoryTracking.size(), 0);

		// Test/Execute Logic
		Test.startTest();


		System.runAs(oUser_System){

			insert oAccountExtendedProfile;

		}

		Test.stopTest();

		// Validate Result
		lstHistoryTracking = [SELECT Id, FieldName__c, FieldLabel__c, OldValue__c, NewValue__c FROM History_Tracking__c];
		System.assertEquals(lstHistoryTracking.size(), 1);
		System.assertEquals(lstHistoryTracking[0].FieldLabel__c, 'Created');

	}

	@isTest static void test_createHistoryTracking_UPDATE() {

		// Create Test Data
		createTestData();
		insert oAccountExtendedProfile;

		List<History_Tracking__c> lstHistoryTracking = [SELECT Id FROM History_Tracking__c];
		System.assertEquals(lstHistoryTracking.size(), 1);

		// Test/Execute Logic
		Test.startTest();


		System.runAs(oUser_System){

				oAccountExtendedProfile.Commencement_Date__c = dToday.addDays(10);
				oAccountExtendedProfile.General_Surgery_procedures__c = 2;
				oAccountExtendedProfile.Gynecologic_procedures__c = 3;
				oAccountExtendedProfile.Head_Neck_procedures__c = 4;
				oAccountExtendedProfile.Hemorrhoids_procedures__c = 5;
			update oAccountExtendedProfile;

		}

		Test.stopTest();

		// Validate Result
		lstHistoryTracking = [SELECT Id, FieldName__c, FieldLabel__c, OldValue__c, NewValue__c FROM History_Tracking__c];
		System.assertEquals(lstHistoryTracking.size(), 6);
		Integer iCounter = 0;
		for (History_Tracking__c oHistoryTracking : lstHistoryTracking){
			clsUtil.debug('**BC** ' + iCounter + ' : ' + oHistoryTracking.FieldLabel__c + ' = ' + oHistoryTracking.NewValue__c + '(' + oHistoryTracking.OldValue__c + ')');

			if (oHistoryTracking.FieldLabel__c == 'Created'){
				iCounter++;
			}else if (oHistoryTracking.FieldName__c == 'Commencement_Date__c'){
				System.assertEquals(oHistoryTracking.OldValue__c, dToday.format());
				System.assertEquals(oHistoryTracking.NewValue__c, dToday.addDays(10).format());
				iCounter++;
			}else if (oHistoryTracking.FieldName__c == 'General_Surgery_procedures__c'){
				System.assertEquals(oHistoryTracking.OldValue__c, '1');
				System.assertEquals(oHistoryTracking.NewValue__c, '2');
				iCounter++;
			}else if (oHistoryTracking.FieldName__c == 'Gynecologic_procedures__c'){
				System.assertEquals(oHistoryTracking.OldValue__c, '2');
				System.assertEquals(oHistoryTracking.NewValue__c, '3');
				iCounter++;
			}else if (oHistoryTracking.FieldName__c == 'Head_Neck_procedures__c'){
				System.assertEquals(oHistoryTracking.OldValue__c, '3');
				System.assertEquals(oHistoryTracking.NewValue__c, '4');
				iCounter++;
			}else if (oHistoryTracking.FieldName__c == 'Hemorrhoids_procedures__c'){
				System.assertEquals(oHistoryTracking.OldValue__c, '4');
				System.assertEquals(oHistoryTracking.NewValue__c, '5');
				iCounter++;
			}
		}
		System.assertEquals(iCounter, 6);

	}
	//----------------------------------------------------------------------------------------------------------------


	//----------------------------------------------------------------------------------------------------------------
	// SET UNIQUE KEY
	//----------------------------------------------------------------------------------------------------------------
	@isTest static void test_setUniqueKey_INSERT() {

		// Create Test Data
		createTestData();

		// Test/Execute Logic
		Test.startTest();

		System.runAs(oUser_System){

			insert lstAccountExtendedProfile;

		}

		Test.stopTest();

		// Validate Result
		lstAccountExtendedProfile = [SELECT Id, Account__c, RecordTypeId, Unique_Key__c FROM Account_Extended_Profile__c];
		System.assertEquals(lstAccountExtendedProfile.size(), 2);
		System.assert(!String.isBlank(lstAccountExtendedProfile[0].Unique_Key__c));
		System.assert(!String.isBlank(lstAccountExtendedProfile[1].Unique_Key__c));


	}

	@isTest static void test_setUniqueKey_UPDATE() {

		// Create Test Data
		createTestData();
		insert lstAccountExtendedProfile;

		// Test/Execute Logic
		Test.startTest();

		System.runAs(oUser_System){

			for (Account_Extended_Profile__c oAccountExtendedProfile : lstAccountExtendedProfile){

				oAccountExtendedProfile.Unique_Key__c = null;

			}

			update lstAccountExtendedProfile;

		}

		Test.stopTest();

		// Validate Result
		lstAccountExtendedProfile = [SELECT Id, Account__c, RecordTypeId, Unique_Key__c FROM Account_Extended_Profile__c];
		System.assertEquals(lstAccountExtendedProfile.size(), 2);
		System.assert(!String.isBlank(lstAccountExtendedProfile[0].Unique_Key__c));
		System.assert(!String.isBlank(lstAccountExtendedProfile[1].Unique_Key__c));

	}	

	@isTest static void test_setUniqueKey_ERROR() {

		String tError_EMEA = '';
		String tError_EURSWOT = '';

		// Create Test Data
		createTestData();
		insert lstAccountExtendedProfile;

		Account_Extended_Profile__c oAccountExtendedProfile2  = new Account_Extended_Profile__c();
			oAccountExtendedProfile2.RecordTypeId = clsUtil.getRecordTypeByDevName('Account_Extended_Profile__c', 'EMEA_Account_Extended_Profile').Id;
			oAccountExtendedProfile2.Account__c = clsTestData_Account.oMain_Account_SAPAccount.Id;
			oAccountExtendedProfile2.Commencement_Date__c = dToday;
			oAccountExtendedProfile2.General_Surgery_procedures__c = 1;
			oAccountExtendedProfile2.Gynecologic_procedures__c = 2;
			oAccountExtendedProfile2.Head_Neck_procedures__c = 3;
			oAccountExtendedProfile2.Hemorrhoids_procedures__c = 4;


		Account_Extended_Profile__c oAccountExtendedProfile_SWOT2  = new Account_Extended_Profile__c();
			//oAccountExtendedProfile_SWOT2.RecordTypeId = clsUtil.getRecordTypeByDevName('Account_Extended_Profile__c', 'EUR_SWOT_Analysis').Id;
        	oAccountExtendedProfile_SWOT2.RecordTypeId = clsUtil.getRecordTypeByDevName('Account_Extended_Profile__c', 'EMEA_SWOT_Analysis').Id;
			oAccountExtendedProfile_SWOT2.Account__c = clsTestData_Account.oMain_Account_SAPAccount.Id;
			oAccountExtendedProfile_SWOT2.Sub_Business_Unit_vs__c = 'ACC';
			oAccountExtendedProfile_SWOT2.Strengths_Hernia__c = 'TEST Strengths Hernia';
			oAccountExtendedProfile_SWOT2.Weaknesses_Hernia__c = 'TEST Weaknesses Hernia';
			oAccountExtendedProfile_SWOT2.Threats_Hernia__c = 'TEST Threats Hernia';
			oAccountExtendedProfile_SWOT2.Opportunities_Hernia__c = 'TEST Opportunities Hernia';

		// Test/Execute Logic
		Test.startTest();

		System.runAs(oUser_System){

			try{

				insert oAccountExtendedProfile2;

			}catch(Exception oEX){

				tError_EMEA = oEX.getMessage();

			}

			try{

				insert oAccountExtendedProfile_SWOT2;

			}catch(Exception oEX){

				tError_EURSWOT = oEX.getMessage();

			}

		}

		Test.stopTest();

		// Validate Result
		System.debug('**BC** tError_EMEA : ' + tError_EMEA);
		System.assert(tError_EMEA.toLowerCase().contains('duplicate value found'));
		System.debug('**BC** tError_EURSWOT : ' + tError_EURSWOT);
		System.assert(tError_EURSWOT.toLowerCase().contains('duplicate value found'));

	}
	//----------------------------------------------------------------------------------------------------------------


	//----------------------------------------------------------------------------------------------------------------
	// RESET APPROVAL STATUS
	//----------------------------------------------------------------------------------------------------------------
	@isTest static void test_resetApprovalStatus() {

		// Create Test Data
		createTestData();
			oAccountExtendedProfile.Approval_Status_Facility_Info__c = 'Approved';
			oAccountExtendedProfile.Approval_Status_SI_AST_Driven__c = 'Approved';
			oAccountExtendedProfile.Approval_Status_SI_GSP_Driven__c = 'Approved';
			oAccountExtendedProfile.Approval_Status_SI_Hernia_Driven__c = 'Approved';
		insert oAccountExtendedProfile;


		// Test/Execute Logic
		Test.startTest();

		System.runAs(oUser_System){

				oAccountExtendedProfile.Total_Account_Beds__c = 100;
				oAccountExtendedProfile.Bariatric_procedures__c = 100;
				oAccountExtendedProfile.General_Surgery_procedures__c = 100;
				oAccountExtendedProfile.Inguinal_hernia_procedures__c = 100;
			update oAccountExtendedProfile;
		}

		Test.stopTest();

		// Validate Result
		lstAccountExtendedProfile = [SELECT Id, Approval_Status_Facility_Info__c, Approval_Status_SI_AST_Driven__c, Approval_Status_SI_GSP_Driven__c, Approval_Status_SI_Hernia_Driven__c FROM Account_Extended_Profile__c WHERE Id = :oAccountExtendedProfile.Id];
		System.assertEquals('Open', lstAccountExtendedProfile[0].Approval_Status_Facility_Info__c);
		System.assertEquals('Open', lstAccountExtendedProfile[0].Approval_Status_SI_AST_Driven__c);
		System.assertEquals('Open', lstAccountExtendedProfile[0].Approval_Status_SI_GSP_Driven__c);
		System.assertEquals('Open', lstAccountExtendedProfile[0].Approval_Status_SI_Hernia_Driven__c);

	}
	//----------------------------------------------------------------------------------------------------------------


	//----------------------------------------------------------------------------------------------------------------
	// SET APPROVER EMAIL
	//----------------------------------------------------------------------------------------------------------------
	@isTest static void test_setApproverEmail() {

		// Create Test Data
		User oUser_WithManager = [SELECT Id, Manager_Email__c FROM User WHERE Profile.Name = 'IT Business Analyst' AND isActive = true AND Manager_Email__c != null LIMIT 1];
		createTestData();
			oAccountExtendedProfile.Section_Owner_Facility_Info__c = oUser_WithManager.Id;
			oAccountExtendedProfile.Section_Owner_SI_AST_Driven__c = oUser_WithManager.Id;
			oAccountExtendedProfile.Section_Owner_SI_GSP_Driven__c = oUser_WithManager.Id;
			oAccountExtendedProfile.Section_Owner_SI_Hernia_Driven__c = oUser_WithManager.Id;


		// Test/Execute Logic
		Test.startTest();

		System.runAs(oUser_System){

			insert oAccountExtendedProfile;
	
		}

		Test.stopTest();

		// Validate Result

		lstAccountExtendedProfile = [SELECT Id, Section_Facility_Info_Approver_Email__c, Section_SI_AST_Driven_Approver_Email__c, Section_SI_GSP_Driven_Approver_Email__c, Section_SI_Hernia_Driven_Approver_Email__c FROM Account_Extended_Profile__c WHERE Id = :oAccountExtendedProfile.Id];
		System.assertEquals(oUser_WithManager.Manager_email__c, lstAccountExtendedProfile[0].Section_Facility_Info_Approver_Email__c);
		System.assertEquals(oUser_WithManager.Manager_email__c, lstAccountExtendedProfile[0].Section_SI_AST_Driven_Approver_Email__c);
		System.assertEquals(oUser_WithManager.Manager_email__c, lstAccountExtendedProfile[0].Section_SI_GSP_Driven_Approver_Email__c);
		System.assertEquals(oUser_WithManager.Manager_email__c, lstAccountExtendedProfile[0].Section_SI_Hernia_Driven_Approver_Email__c);

	}
	//----------------------------------------------------------------------------------------------------------------


	//----------------------------------------------------------------------------------------------------------------
	// SET LAST UPDATED
	//----------------------------------------------------------------------------------------------------------------
	@isTest static void test_setLastUpdated_INSERT() {

		// Create Test Data
		createTestData();

		// Test/Execute Logic
		Test.startTest();

		System.runAs(oUser_System){

			insert oAccountExtendedProfile_SWOT;
	
		}

		Test.stopTest();

		// Validate Result

		lstAccountExtendedProfile = [SELECT Id, Last_Updated__c FROM Account_Extended_Profile__c WHERE Id = :oAccountExtendedProfile_SWOT.Id];
		System.assertEquals(lstAccountExtendedProfile[0].Last_Updated__c, Date.today());

	}

	@isTest static void test_setLastUpdated_UPDATE() {

		// Create Test Data
		createTestData();
		insert oAccountExtendedProfile_SWOT;

		// Test/Execute Logic
		Test.startTest();

		System.runAs(oUser_System){

				oAccountExtendedProfile_SWOT.Last_Updated__c = Date.today().addDays(-365);
			update oAccountExtendedProfile_SWOT;

			lstAccountExtendedProfile = [SELECT Id, Last_Updated__c FROM Account_Extended_Profile__c WHERE Id = :oAccountExtendedProfile_SWOT.Id];
			System.assertEquals(lstAccountExtendedProfile[0].Last_Updated__c, Date.today().addDays(-365));

				oAccountExtendedProfile_SWOT.Strengths_Hernia__c = 'TEST UPDATED';
				oAccountExtendedProfile_SWOT.Weaknesses_Hernia__c = 'TEST UPDATED';
				oAccountExtendedProfile_SWOT.Threats_Hernia__c = 'TEST UPDATED';
				oAccountExtendedProfile_SWOT.Opportunities_Hernia__c = 'TEST UPDATED';
			update oAccountExtendedProfile_SWOT;
	
		}

		Test.stopTest();

		// Validate Result

		lstAccountExtendedProfile = [SELECT Id, Last_Updated__c FROM Account_Extended_Profile__c WHERE Id = :oAccountExtendedProfile_SWOT.Id];
		System.assertEquals(lstAccountExtendedProfile[0].Last_Updated__c, Date.today());

	}
	//----------------------------------------------------------------------------------------------------------------

}
//--------------------------------------------------------------------------------------------------------------------