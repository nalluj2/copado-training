/*      Class Name: bl_AccountPlanSharingToTeamMembers
 *       Test Class Name: Test_ContactActionPlanNew
 *      Created Date : 28/10/2012
 *      Description : This class is called by trigger 'tr_AccountPlan' and used to insert and delete Account Plan Sharing records based on Account Plan team members for a account plan
 *      Author = Manish Kumar Srivastava
 */

public class bl_AccountPlanSharingToTeamMembers {
	
    public static void AccountPlanSharingToAddedTeamMembers(map<id,set<id>> mapAccPlanidWithUsers){
    	
    	List<Account_Plan_2__Share> lstAPS=new List<Account_Plan_2__Share>();
      
        for(Id apid : mapAccPlanidWithUsers.keyset())
        {
            for(Id userid : mapAccPlanidWithUsers.get(apid))
            {                
                Account_Plan_2__Share aps=new Account_Plan_2__Share(ParentId=apid,UserOrGroupId=userid,AccessLevel='edit',RowCause=Schema.Account_Plan_2__Share.RowCause.manual);
                lstAPS.add(aps);
            }
        
        }
        
        insert lstAPS;
    }
    
    public static void AccountPlanDeleteSharingForRemovedTeamMembers(map<id,set<id>> mapAccPlanidWithUsers){
	    
	    List<Account_Plan_2__Share> lstDelAPS=new List<Account_Plan_2__Share>();	    
	    	    
	    Map<String, Account_Plan_2__Share> mapExistingAPS = new Map<String,Account_Plan_2__Share>();
	    
        for(Account_Plan_2__Share aps: [select Id, ParentId, UserOrGroupId from Account_Plan_2__Share where Parentid IN:mapAccPlanidWithUsers.keyset() and RowCause=:Schema.Account_Plan_2__Share.RowCause.manual and AccessLevel='edit']){
        	
        	mapExistingAPS.put(aps.parentId + ':' + aps.UserOrGroupId, aps);
        }
        
        if(mapExistingAPS.size()>0){
        	     
            for(id apid:mapAccPlanidWithUsers.keyset())
            {
                for(id userid:mapAccPlanidWithUsers.get(apid))
                {
                	Account_Plan_2__Share existingAPS = mapExistingAPS.get(apid + ':' + userId);
                    
                    if(existingAPS != null) lstDelAPS.add(existingAPS);                       
                }                
            }
        }
        
        if(lstDelAPS.size()>0) delete lstDelAPS;        
    }
}