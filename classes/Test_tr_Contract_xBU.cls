@isTest
private class Test_tr_Contract_xBU {
    
    private static testMethod void check_HistoryTracking(){
    	
    	Account acc = new Account();
    	acc.Name = 'Unit Test Account 1';
    	acc.SAP_Id__c = '0123456789';
    	
    	Account acc2 = new Account();
    	acc2.Name = 'Unit Test Account 2';
    	acc2.SAP_Id__c = '9876543210';
    	
    	insert new List<Account>{acc, acc2};
    	
    	Test.startTest();
    	
    	Contract_xBU__c contract = new Contract_xBU__c();
    	contract.Account__c = acc.Id;
    	contract.Status__c = 'In Process C&P';
    	insert contract;

		contract = [SELECT Id, Contract_Type__c FROM Contract_xBU__c WHERE Id = :contract.Id];
		System.debug('**BC** contract : ' + contract);    	
    	List<History_Tracking__c> history = [Select Id, FieldLabel__c, FieldName__c, NewValue__c, OldValue__c from History_Tracking__c where Contract_xBU__c = :contract.Id];
    	System.assert(history.size() == 1);
    	System.assert(history[0].FieldLabel__c == 'Created');
    	
    	contract.Status__c = 'Sent to Customer/ Sales Rep';
    	contract.Account__c = acc2.Id;
    	
    	update contract;
    	
    	history = [Select Id, FieldLabel__c, FieldName__c, NewValue__c, OldValue__c from History_Tracking__c where Contract_xBU__c = :contract.Id];
		System.debug('**BC** history.size() : ' + history.size());
		System.debug('**BC** history : ' + history);
    	System.assert(history.size() == 3);
    	
    	History_Tracking__c historyStatus = [Select Id, FieldLabel__c, FieldName__c, NewValue__c, OldValue__c from History_Tracking__c where Contract_xBU__c = :contract.Id AND FieldName__c = 'Status__c'];
    	System.assert(historyStatus.OldValue__c == 'In Process C&P');
    	System.assert(historyStatus.NewValue__c == 'Sent to Customer/ Sales Rep');
    	
    	History_Tracking__c historyAccount = [Select Id, FieldLabel__c, FieldName__c, NewValue__c, OldValue__c from History_Tracking__c where Contract_xBU__c = :contract.Id AND FieldName__c = 'Account__c'];
    	System.assert(historyAccount.OldValue__c == 'Unit Test Account 1');
    	System.assert(historyAccount.NewValue__c == 'Unit Test Account 2');   	
    }
    
    private static testMethod void check_PopulateContractNumber(){
    	
    	Account acc = new Account();
    	acc.Name = 'Unit Test Account 1';
    	acc.SAP_Id__c = '0123456789';    	    	
    	insert acc;
    	
    	Test.startTest();
    	
    	Contract_xBU__c contract_lastYear = new Contract_xBU__c();
    	contract_lastYear.Account__c = acc.Id;
    	contract_lastYear.Status__c = 'In Process C&P';
    	contract_lastYear.Internal_Contract_Number__c = 101;
    	contract_lastYear.Contract_Number__c = '101-' + String.valueOf(Date.today().addYears(-1)).substring(2,4);
    	    	
    	Contract_xBU__c contract_thisYear = new Contract_xBU__c();
    	contract_thisYear.Account__c = acc.Id;
    	contract_thisYear.Status__c = 'In Process C&P';
    	contract_thisYear.Internal_Contract_Number__c = 11;
    	contract_thisYear.Contract_Number__c = '11-' + String.valueOf(Date.today()).substring(2,4);
    	    	
    	Contract_xBU__c contract_nextYear = new Contract_xBU__c();
    	contract_nextYear.Account__c = acc.Id;
    	contract_nextYear.Status__c = 'In Process C&P';
    	contract_nextYear.Internal_Contract_Number__c = 202;
    	contract_nextYear.Contract_Number__c = '202-' + String.valueOf(Date.today().addYears(1)).substring(2,4);
    	
    	insert new List<Contract_xBU__c>{contract_lastYear, contract_thisYear, contract_nextYear};
    	
    	//Assert it hasn't change as it was provided
    	contract_lastYear = [Select Id, Contract_Number__c, Internal_Contract_Number__c from Contract_xBU__c where Id = :contract_lastYear.Id];
    	System.assert(contract_lastYear.Contract_Number__c == '101-' + String.valueOf(Date.today().addYears(-1)).substring(2,4));
    	System.assert(contract_lastYear.Internal_Contract_Number__c == 101);
    	
    	contract_thisYear = [Select Id, Contract_Number__c, Internal_Contract_Number__c from Contract_xBU__c where Id = :contract_thisYear.Id];
    	System.assert(contract_thisYear.Contract_Number__c == '11-' + String.valueOf(Date.today()).substring(2,4));
    	System.assert(contract_thisYear.Internal_Contract_Number__c == 11);
    	
    	contract_nextYear = [Select Id, Contract_Number__c, Internal_Contract_Number__c from Contract_xBU__c where Id = :contract_nextYear.Id];
    	System.assert(contract_nextYear.Contract_Number__c == '202-' + String.valueOf(Date.today().addYears(1)).substring(2,4));
    	System.assert(contract_nextYear.Internal_Contract_Number__c == 202);
    	    	
    	Contract_xBU__c contract = new Contract_xBU__c();
    	contract.Account__c = acc.Id;
    	contract.Status__c = 'In Process C&P';
    	
    	insert contract;
    	
       	contract = [Select Id, Contract_Number__c, Internal_Contract_Number__c from Contract_xBU__c where Id = :contract.Id];
    	System.assert(contract.Contract_Number__c == '12-' + String.valueOf(Date.today()).substring(2,4));
    	System.assert(contract.Internal_Contract_Number__c == 12);
    }


    private static testMethod void check_UpdateOldContract(){
    	
		//--------------------------------------------------------
		// Create Test Daat
		//--------------------------------------------------------
    	Account oAccount = new Account();
    		oAccount.Name = 'Unit Test Account 1';
    		oAccount.SAP_Id__c = '0123456789';    	    	
    	insert oAccount;
    	
    	Contract_xBU__c oContract_Old_1 = new Contract_xBU__c();
    		oContract_Old_1.Account__c = oAccount.Id;
    		oContract_Old_1.Status__c = 'In Process C&P';
    		oContract_Old_1.Internal_Contract_Number__c = 101;
    		oContract_Old_1.Contract_Number__c = '101-' + String.valueOf(Date.today().addYears(-1)).substring(2,4);

    	Contract_xBU__c oContract_Old_2 = new Contract_xBU__c();
    		oContract_Old_2.Account__c = oAccount.Id;
    		oContract_Old_2.Status__c = 'In Process C&P';
    		oContract_Old_2.Internal_Contract_Number__c = 102;
    		oContract_Old_2.Contract_Number__c = '102-' + String.valueOf(Date.today().addYears(-1)).substring(2,4);

    	Contract_xBU__c oContract_New_1 = new Contract_xBU__c();
    		oContract_New_1.Account__c = oAccount.Id;
    		oContract_New_1.Status__c = 'In Process C&P';
    		oContract_New_1.Internal_Contract_Number__c = 201;
    		oContract_New_1.Contract_Number__c = '201-' + String.valueOf(Date.today()).substring(2,4);

		insert new List<Contract_xBU__c>{oContract_Old_1, oContract_Old_2, oContract_New_1};

		List<Contract_xBU__c> lstContract = [SELECT Id, Internal_Contract_Number__c, Old_Contract__c, New_Contract__c, Reminder_Stop__c FROM Contract_xBU__c ORDER BY Internal_Contract_Number__c];
		System.assertEquals(lstContract.size(), 3);
		for (Contract_xBU__c oContract : lstContract){
			System.assertEquals(oContract.Old_Contract__c, oContract.New_Contract__c, String.isBlank(oContract.Old_Contract__c));
		}
		//--------------------------------------------------------


		//--------------------------------------------------------
		// Execute Logic
		//--------------------------------------------------------
    	Test.startTest();
    	    	    	    	
    	Contract_xBU__c oContract_New_2 = new Contract_xBU__c();
    		oContract_New_2.Account__c = oAccount.Id;
    		oContract_New_2.Status__c = 'In Process C&P';
    		oContract_New_2.Internal_Contract_Number__c = 202;
    		oContract_New_2.Contract_Number__c = '202-' + String.valueOf(Date.today()).substring(2,4);
			oContract_New_2.Old_Contract__c = oContract_Old_2.Id;
    	insert oContract_New_2;

			oContract_New_1.Old_Contract__c = oContract_Old_1.Id;
		update oContract_New_1;

		lstContract = [SELECT Id, Internal_Contract_Number__c, Old_Contract__c, New_Contract__c, Reminder_Stop__c FROM Contract_xBU__c ORDER BY Internal_Contract_Number__c];
			oContract_Old_1 = lstContract[0];
			oContract_Old_2 = lstContract[1];
			oContract_New_1 = lstContract[2];
			oContract_New_2 = lstContract[3];
		System.assertEquals(oContract_Old_1.New_Contract__c, oContract_New_1.Id);
		System.assertEquals(oContract_Old_1.Reminder_Stop__c, true);
		System.assertEquals(oContract_Old_2.New_Contract__c, oContract_New_2.Id);
		System.assertEquals(oContract_Old_2.Reminder_Stop__c, true);

		
		System.debug('**BC** START CLEAR OLD_CONTRACT__c');
			oContract_New_1.Old_Contract__c = null;
		update oContract_New_1;
		
		Test.stopTest();
		//--------------------------------------------------------


		//--------------------------------------------------------
		// Validate Result
		//--------------------------------------------------------
		lstContract = [SELECT Id, Internal_Contract_Number__c, Old_Contract__c, New_Contract__c, Reminder_Stop__c FROM Contract_xBU__c ORDER BY Internal_Contract_Number__c];
			oContract_Old_1 = lstContract[0];
			oContract_Old_2 = lstContract[1];
			oContract_New_1 = lstContract[2];
			oContract_New_2 = lstContract[3];
		System.assertEquals(oContract_Old_1.New_Contract__c, null);
		System.assertEquals(oContract_Old_1.Reminder_Stop__c, false);
		System.assertEquals(oContract_Old_2.New_Contract__c, oContract_New_2.Id);
		System.assertEquals(oContract_Old_2.Reminder_Stop__c, true);
		System.assertEquals(oContract_New_1.Old_Contract__c, null);
		//--------------------------------------------------------
    	
    }
    
    @testSetup
    private static void createTestData(){
    	
    	HistoryTracking_ContractxBU__c field1 = new HistoryTracking_ContractxBU__c();
    	field1.Name = 'Account__c';
    	field1.ReferenceTo__c = 'Account';
    	field1.ReferenceToField__c = 'Name';
    	
    	HistoryTracking_ContractxBU__c field2 = new HistoryTracking_ContractxBU__c();
    	field2.Name = 'Status__c';
    	
    	HistoryTracking_ContractxBU__c field3 = new HistoryTracking_ContractxBU__c();
    	field3.Name = 'Contract_Type__c';
    	
    	HistoryTracking_ContractxBU__c field4 = new HistoryTracking_ContractxBU__c();
    	field4.Name = 'Contract_Period__c';
    	
    	HistoryTracking_ContractxBU__c field5 = new HistoryTracking_ContractxBU__c();
    	field5.Name = 'Contract_Start_Date__c';
    	
    	HistoryTracking_ContractxBU__c field6 = new HistoryTracking_ContractxBU__c();
    	field6.Name = 'Contract_End_Date__c';
    	
    	insert new List<HistoryTracking_ContractxBU__c>{field1, field2, field3, field4, field5, field6}; 
    }
}