/*
 *      Created Date    : 2014-04-17
 *      Author          : Bart Caelen
 *      Description     : Controller Extension Class for the VisualForce Page OpportunityActivityOverview which displays 2 overviews of related Activities:
 *          1. Open Activity
 *              - contains all Open Activities (Task & Event) which are related to the Opportunity or are related to Summary_Of_My_Position_Today__c or Stakeholder_Mapping__c which are both related to the Opportunity
 *          2. Activity History
 *              - contains all Closed/Completed Activities (Task & Event) which are related to the Opportunity or are related to Summary_Of_My_Position_Today__c or Stakeholder_Mapping__c which are both related to the Opportunity
 */
public class ctrlExt_OpportunityActivityOverview {

    //------------------------------------------------------------------------------------
    // Private Variables
    //------------------------------------------------------------------------------------
    private Opportunity oOpportunity;
    private String tQuery_OpenActivity = '';
    private String tQuery_ActivityHistory = '';
    private List<OpenActivity> lstOpenActivity = new List<OpenActivity>();
    private List<ActivityHistory> lstActivityHistory = new List<ActivityHistory>();
    //------------------------------------------------------------------------------------


    //------------------------------------------------------------------------------------
    // Constructor
    //------------------------------------------------------------------------------------
    public ctrlExt_OpportunityActivityOverview(ApexPages.StandardController stdController) {
        this.oOpportunity   = (Opportunity)stdController.getRecord();
        tURL_Redirect       = stdController.view().getUrl();

        initialize();

        loadData();
    }
    //------------------------------------------------------------------------------------


    //------------------------------------------------------------------------------------
    // GETTER / SETTER
    //------------------------------------------------------------------------------------
    public List<wr_Activity> lstWROpenActivity      { get;private set; }
    public List<wr_Activity> lstWRActivityHistory   { get;private set; }
    public String tURL_Redirect                     { get;private set; }
    //public String tURL_NewTask                      { get;private set; }
    //public String tURL_NewEvent                     { get;private set; }
    //public String tURL_LogACall                     { get;private set; }
    //public String tURL_MailMerge                    { get;private set; }
    //public String tURL_SendAnEmail                  { get;private set; }
    //public String tURL_ViewAll                      { get;private set; }
    //------------------------------------------------------------------------------------


    //------------------------------------------------------------------------------------
    // Private functions
    //------------------------------------------------------------------------------------
    private void initialize(){

        this.lstOpenActivity        = new List<OpenActivity>();
        this.lstActivityHistory     = new List<ActivityHistory>();
        this.lstWROpenActivity      = new List<wr_Activity>();
        this.lstWRActivityHistory   = new List<wr_Activity>();

        //this.tURL_NewTask           = '/00T/e?who_id=' + UserInfo.getUserId() + '&what_id=' + oOpportunity.Id + '&retURL=%2F' + oOpportunity.Id;
        //this.tURL_NewEvent          = '/00U/e?who_id=' + UserInfo.getUserId() + '&what_id=' + oOpportunity.Id + '&retURL=%2F' + oOpportunity.Id;
        //this.tURL_LogACall          = '/00T/e?title=Call&who_id=' + UserInfo.getUserId() + '&what_id=' + oOpportunity.Id + '&followup=1&tsk5=Call&retURL=%2F' + oOpportunity.Id;
        //this.tURL_MailMerge         = '/mail/mmchoose.jsp?id=' + oOpportunity.Id + '&1=' + oOpportunity.Name + '&retURL=%2F' + oOpportunity.Id;
        //this.tURL_SendAnEmail       = '/_ui/core/email/author/EmailAuthor?p2_lkid=' + oOpportunity.AccountId + '&rtype=003&p3_lkid=' + oOpportunity.Id + '&retURL=%2F' + oOpportunity.Id;
        //this.tURL_ViewAll           = '/ui/core/activity/ViewAllActivityHistoryPage?retURL=%2F' + oOpportunity.Id + '&id=' + oOpportunity.Id;

        tQuery_OpenActivity         = 'SELECT';
        tQuery_OpenActivity         += ' Id, Subject, WhoId, WhatID, isTask, ActivityDate, Status, Priority, OwnerID, LastModifiedDate, StartDateTime, EndDateTime';
        tQuery_OpenActivity         += ' FROM OpenActivities';
        tQuery_OpenActivity         += ' ORDER BY ActivityDate ASC NULLS LAST, LastModifiedDate DESC';
        tQuery_OpenActivity         += ' LIMIT 499';

        tQuery_ActivityHistory      = 'SELECT';
        tQuery_ActivityHistory      += ' Id, Subject, WhoId, WhatID, isTask, ActivityDate, Status, Priority, OwnerID, LastModifiedDate, StartDateTime, EndDateTime';
        tQuery_ActivityHistory      += ' FROM ActivityHistories';
        tQuery_ActivityHistory      += ' ORDER BY ActivityDate ASC NULLS LAST, LastModifiedDate DESC';
        tQuery_ActivityHistory      += ' LIMIT 499';

    }

    private void loadData(){
        
        // SELECT Opportunity WHITH THE RELATED DATA (ActivityHistory & OpenActivity)
        String tQuery_Opportunity = 'SELECT';
        tQuery_Opportunity += ' Id, Name';
        tQuery_Opportunity += ' , (';
        tQuery_Opportunity += tQuery_ActivityHistory;
        tQuery_Opportunity += ' )';
        tQuery_Opportunity += ' , (';
        tQuery_Opportunity += tQuery_OpenActivity;
        tQuery_Opportunity += ' )';
        tQuery_Opportunity += ' FROM Opportunity';
        tQuery_Opportunity += ' WHERE ID = \'' + oOpportunity.Id + '\'';

        Opportunity oOpp = Database.query(tQuery_Opportunity);

        Map<Id, OpenActivity> mapID_OpenActivity = new Map<Id, OpenActivity>();
        Map<Id, ActivityHistory> mapID_ActivityHistory = new Map<Id, ActivityHistory>();
        mapID_OpenActivity.putAll(oOpp.OpenActivities);
        mapID_ActivityHistory.putAll(oOpp.ActivityHistories);

        lstOpenActivity.addAll(oOpp.OpenActivities);
        lstActivityHistory.addAll(oOpp.ActivityHistories);

        // SELECT Summary_Of_My_Position_Today__c WHITH THE RELATED DATA (ActivityHistory & OpenActivity)
        String tQuery_SOMPT = 'SELECT';
        tQuery_SOMPT += ' ID';
        tQuery_SOMPT += ', (';
        tQuery_SOMPT += tQuery_ActivityHistory;
        tQuery_SOMPT += ')';
        tQuery_SOMPT += ', (';
        tQuery_SOMPT += tQuery_OpenActivity;
        tQuery_SOMPT += ')';
        tQuery_SOMPT += ' FROM Summary_Of_My_Position_Today__c';
        tQuery_SOMPT += ' WHERE Opportunity__c = \'' + oOpportunity.Id + '\'';

        List<Summary_Of_My_Position_Today__c> lstSOMPT = Database.query(tQuery_SOMPT);

        for (Summary_Of_My_Position_Today__c oSOMPT : lstSOMPT){
            mapID_OpenActivity.putAll(oSOMPT.OpenActivities);
            mapID_ActivityHistory.putAll(oSOMPT.ActivityHistories);

            lstOpenActivity.addAll(oSOMPT.OpenActivities);
            lstActivityHistory.addAll(oSOMPT.ActivityHistories);
        }


        // SELECT Stakeholder_Mapping__c WHITH THE RELATED DATA (ActivityHistory & OpenActivity)
        String tQuery_SM = 'SELECT';
        tQuery_SM += ' ID';
        tQuery_SM += ', (';
        tQuery_SM += tQuery_ActivityHistory;
        tQuery_SM += ')';
        tQuery_SM += ', (';
        tQuery_SM += tQuery_OpenActivity;
        tQuery_SM += ')';
        tQuery_SM += ' FROM Stakeholder_Mapping__c';
        tQuery_SM += ' WHERE Opportunity__c = \'' + oOpportunity.Id + '\'';

        List<Stakeholder_Mapping__c> lstSM = Database.query(tQuery_SM);

        for (Stakeholder_Mapping__c oSM : lstSM){
            mapID_OpenActivity.putAll(oSM.OpenActivities);
            mapID_ActivityHistory.putAll(oSM.ActivityHistories);

            lstOpenActivity.addAll(oSM.OpenActivities);
            lstActivityHistory.addAll(oSM.ActivityHistories);
        }

        // Sort the Collected Lists of Data
        lstOpenActivity = clsUtil.sortList(lstOpenActivity, 'ActivityDate', 'ASC');
        lstActivityHistory = clsUtil.sortList(lstActivityHistory, 'LastModifiedDate', 'ASC');

        string tPrefix_Opportunity = clsUtil.getPrefixFromSObjectName('Opportunity');
        string tPrefix_StakeholdMapping = clsUtil.getPrefixFromSObjectName('Stakeholder_Mapping__c');
        string tPrefix_SummaryOfMyPositionToday = clsUtil.getPrefixFromSObjectName('Summary_Of_My_Position_Today__c');

        lstWROpenActivity = new List<wr_Activity>();
        for (OpenActivity oAct : lstOpenActivity){
            wr_Activity oWR_Act = new wr_Activity();
                if (String.valueOf(oAct.WhatID).startsWith(tPrefix_Opportunity)){
                    oWR_Act.tType           = 'Opportunity';
                }else if (String.valueOf(oAct.WhatID).startsWith(tPrefix_StakeholdMapping)){
                    oWR_Act.tType           = 'Stakeholder Mapping';
                }else if (String.valueOf(oAct.WhatID).startsWith(tPrefix_SummaryOfMyPositionToday)){
                    oWR_Act.tType           = 'SOMPT';
                }
                oWR_Act.oOpenActivity   = oAct;
                oWR_Act.tLink_Detail    = '/' + oAct.Id + '?retURL=%2F' + oOpportunity.Id; 
                oWR_Act.tLink_Edit      = '/' + oAct.Id + '/e?retURL=%2F' + oOpportunity.Id; 
                oWR_Act.tLink_Close     = '/' + oAct.Id + '/e?close=1&retURL=%2F' + oOpportunity.Id; 
            lstWROpenActivity.add(oWR_Act);
        }

        lstWRActivityHistory = new List<wr_Activity>();
        for (ActivityHistory oAct : lstActivityHistory){
            wr_Activity oWR_Act = new wr_Activity();
                if (String.valueOf(oAct.WhatID).startsWith(tPrefix_Opportunity)){
                    oWR_Act.tType           = 'Opportunity';
                }else if (String.valueOf(oAct.WhatID).startsWith(tPrefix_StakeholdMapping)){
                    oWR_Act.tType           = 'Stakeholder Mapping';
                }else if (String.valueOf(oAct.WhatID).startsWith(tPrefix_SummaryOfMyPositionToday)){
                    oWR_Act.tType           = 'SOMPT';
                }
                oWR_Act.oActivityHistory    = oAct;
                oWR_Act.tLink_Detail        = '/' + oAct.Id + '?retURL=%2F' + oOpportunity.Id; 
                oWR_Act.tLink_Edit          = '/' + oAct.Id + '/e?retURL=%2F' + oOpportunity.Id; 
            lstWRActivityHistory.add(oWR_Act);
        }
    }
    //------------------------------------------------------------------------------------


    //------------------------------------------------------------------------------------
    // Wrapper Class
    //------------------------------------------------------------------------------------
    public class wr_Activity{

        public wr_Activity(){
            this.tType              = '';
            this.tLink_Detail       = '';
            this.tLink_Edit         = '';
            this.tLink_Close        = '';
            this.oOpenActivity      = new OpenActivity();
            this.oActivityHistory   = new ActivityHistory();
        }

        public String tType                     { get;set; }
        public String tLink_Detail              { get;set; }
        public String tLink_Edit                { get;set; }
        public String tLink_Close               { get;set; }
        public OpenActivity oOpenActivity       { get;set; }
        public ActivityHistory oActivityHistory { get;set; }
    }
    //------------------------------------------------------------------------------------

}