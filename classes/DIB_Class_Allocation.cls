/**
 * Creation Date: 	20090928
 * Description: 	This class will construct an Object type.  Composed by with different Arguments. 
 *					-  It will be called by the Allocation Visual force Pages to draw the Allocations 
 * Author: 	ABSI - MC
 */	 
public class DIB_Class_Allocation {
	
	public Id id ; 
	public String sectionName ;
	public DIB_Class_Allocation_Core [] toAllocate  ;
	public DIB_Invoice_Line__c massUpdate ; 
	
	public String getsectionName() {
		return this.sectionName;
	}
	
	public Id getId(){
		return this.Id ; 
	}
	
	public DIB_Invoice_Line__c getmassUpdate(){
		return this.massUpdate ; 
	}
		
	public  DIB_Class_Allocation_Core [] gettoAllocate() {
		return this.toAllocate;
	}
	
	public  void settoAllocate(DIB_Class_Allocation_Core []  s) {
		this.toAllocate = s; 
	}
	
	public String getDepartement(Id ilid){
		String s ;
		/*for ( Integer i = 0 ; i < this.toAllocate.size() ; i ++){
			if (this.toAllocate[i].Id == ilid){
				s = toAllocate[i].Department__c ; 
			}
		}*/
		return s ; 
	}
	
	public DIB_Class_Allocation(){
		
	}
	
	public DIB_Class_Allocation(DIB_Invoice_Line__c [] ils, List<Affiliation__c>  affs, List <DIB_Department__c>  dibDepartments){
		this.Id = ils[0].Id ;
		
		DIB_Class_Allocation_Core [] result = new DIB_Class_Allocation_Core []{} ; 
		for (Integer i=0 ; i < ils.size() ; i++){
			DIB_Class_Allocation_Core cac = new DIB_Class_Allocation_Core (ils[i], affs, dibDepartments) ; 
			result.add(cac) ; 			 
		}			
		this.toAllocate =  result ; 
		
		this.sectionName = ils[0].Sold_To__r.Name ;
		
		this.massUpdate = new DIB_Invoice_Line__c(Id=ils[0].Id) ; 	
		
		// Query all the Affiliations and keep them saved in a global var : 					 		
	}
	
	public void initSoldToShipTo(DIB_Invoice_Line__c [] ils, List<Affiliation__c>  affs, List <DIB_Department__c>  dibDepartments) {
		this.Id = ils[0].Id ;
		DIB_Class_Allocation_Core [] result = new DIB_Class_Allocation_Core []{} ; 
		for (Integer i=0 ; i < ils.size() ; i++){
			DIB_Class_Allocation_Core cac = new DIB_Class_Allocation_Core (ils[i], affs, dibDepartments) ; 
			result.add(cac) ; 			 
		}			
		this.toAllocate =  result ; 
		
		if (ils[0].Ship_To_Name_Text__c != null) {
			this.sectionName = ils[0].Sold_To__r.Name + ' / ' + ils[0].Ship_To_Name_Text__c;
		}
		else {
			this.sectionName = ils[0].Sold_To__r.Name;
		}
		
		this.massUpdate = new DIB_Invoice_Line__c(Id=ils[0].Id) ; 			
	}
	
	
}