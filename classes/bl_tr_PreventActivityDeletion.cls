/*
 *      Created Date : 07 -Mar-2013
 *      Description : This is the class is called by trigger tr_PreventActivityDeletion on task and tr_PreventEmailMessageDeletion onEmail Message to prevent the deletion of
                     activity and email message if they are related to case and the case record type is one of the OMA record types and user profile 
                     is OMA profile.
 *
 *      Author = Kaushal Singh
 *      
 */
public class bl_tr_PreventActivityDeletion{
    
    public static void PreventDeleteTask(List<Task> lstTask){
        
        set<id> setTaskWhatIds=new set<id>();
        boolean ProfileCheck;
        map<id,id> mapCaseIdAndCaseId=new map<id,id>();
        List<Case> lstOfCase=new List<Case>();
        
        for(Task t:lstTask){
            setTaskWhatIds.add(t.whatid);    
        }
        if(setTaskWhatIds.size()>0){
            
            lstOfCase=lstCase(setTaskWhatIds);
            if(lstOfCase.size()>0){
                for(Case c:lstOfCase){
                    mapCaseIdAndCaseId.put(c.id,c.id);        
                }
            }
        }
        if(mapCaseIdAndCaseId.keyset().size()>0){    
            ProfileCheck=ProfileCheck();
        }    
            
        for(Task t:lstTask){
            if(mapCaseIdAndCaseId.keyset().size()>0){
                if(ProfileCheck==true && mapCaseIdAndCaseId.get(t.WhatId) !=null && t.status=='Completed'){
                    t.adderror('You are not allowed to delete this activity.');
                }
            }
        }
    }
    public static boolean ProfileCheck(){
        
        Profile ProfileName=[
                                 Select id ,Name 
                                 from profile 
                                 where id=:userinfo.getProfileId()
                                ];
        
        List<OMA_Profiles__c> lstOMAprfle=[
                                           Select Name,Profile_Name__c 
                                           from OMA_Profiles__c 
                                           where Profile_Name__c =:ProfileName.Name
                                          ];
        if(lstOMAprfle.size()>0){
            return true;
        }else{
            return false;
        }
    
    }
    
    public static List<Case> lstCase(set<id> setwhatId) {
        
        set<String> setRTNames=new set<String>();
        List<OMA_Record_Types__c> lstOMART=[
                                            Select Name,Developer_Name__c 
                                            from OMA_Record_Types__c
                                           ];
        for(OMA_Record_Types__c OMRT:lstOMART){
            setRTNames.add(OMRT.Developer_Name__c);
        }
        List<Case> lstCase=new list<case>();
        if(setRTNames.size()>0){
            lstCase=[
                     select id,RecordTypeid,RecordType.DeveloperName 
                     from Case 
                     where id in:setwhatId and RecordType.DeveloperName in:setRTNames
                    ];
        }
        //map<id,id> mapCaseIdAndCaseId=new map<id,id>();
        //if(lstCase.size()>0){
           // for(Case c:lstCase){
              //  mapCaseIdAndCaseId.put(c.id,c.id);        
           // }
       // }
        return lstCase;
    }
    public static void PreventDeleteEmailMessage(List<EmailMessage> lstEM){
        set<id> setCaseid=new set<id>();
        Boolean PFCheck;
        for(EmailMessage EM:lstEM){
            setCaseid.add(EM.ParentId);        
        }
        List<Case> lstCase=new List<Case>();
        if(setCaseid.size()>0){
            lstCase=lstCase(setCaseid);
        }
        if(lstCase.size()>0){
            PFCheck=ProfileCheck();    
        
            for(EmailMessage EM:lstEM){
                if(PFCheck==true ){
                    EM.adderror('You are not allowed to delete this Email Message.');
                }
            }
        }    
    }
}