public class bl_NotificationSAPLog {
    
    public static Set<String> supportedObjects = new Set<String>{'SVMXC__Service_Order__c', 'Case', 'Attachment', 'SVMXC__Installed_Product__c', 'Installed_Product_Measures__c'};
    
    public static void populateNotificationGrouping(List<NotificationSAPLog__c> triggerNew){
    	
    	Map<String, Notification_Grouping__c> groupingMap = new Map<String, Notification_Grouping__c>();
    	
    	for(NotificationSAPLog__c notif : triggerNew){
    		
    		// Only process notifications for supported objects
    		if(supportedObjects.contains(notif.SFDC_Object_Name__c)){
    		
	    		Notification_Grouping__c grouping = new Notification_Grouping__c();
	    		
	    		if(notif.SFDC_Object_Name__c == 'Attachment') grouping.External_ID__c = notif.Record_ID__c;
	    		else grouping.External_ID__c = notif.Record_SAPID__c;
	    		
	    		grouping.Scope__c = notif.SFDC_Object_Name__c;
	    		grouping.Unique_Key__c = notif.SFDC_Object_Name__c + ':' + grouping.External_ID__c;
	    			
	    		groupingMap.put(grouping.Unique_Key__c, grouping);    	
    		}	
    	}
    	
    	if(groupingMap.isEmpty()) return;
    	   	
    	for(Notification_Grouping__c existingGrouping : [Select Id, Unique_Key__c from Notification_Grouping__c where Unique_Key__c IN :groupingMap.keySet()]){
    		
    		Notification_Grouping__c grouping = groupingMap.get(existingGrouping.Unique_Key__c);
    		grouping.Id = existingGrouping.Id;
    	}
    	
    	List<Notification_Grouping__c> toInsert = new List<Notification_Grouping__c>();
    	
    	for(Notification_Grouping__c grouping : groupingMap.values()){
    		
    		if(grouping.Id == null) toInsert.add(grouping);
    	}
    	
    	if(toInsert.size() > 0) insert toInsert;
    	
    	for(NotificationSAPLog__c notif : triggerNew){
    		
    		// Only process notifications for supported objects
    		if(supportedObjects.contains(notif.SFDC_Object_Name__c)){
    			
    			String key = notif.SFDC_Object_Name__c + ':';
    			
    			if(notif.SFDC_Object_Name__c == 'Attachment') key += notif.Record_ID__c; 
    			else key += notif.Record_SAPID__c;
    			
    			Notification_Grouping__c grouping = groupingMap.get(key);
    			
    			notif.Notification_Grouping__c = grouping.Id;    			
    		}    		
    	}
    }
    
    public static void updateNotificationGrouping(List<NotificationSAPLog__c> triggerNew){
    	
    	Map<Id, Notification_Grouping__c> groupingMap = new Map<Id, Notification_Grouping__c>();
    	
    	for(NotificationSAPLog__c notif : triggerNew){
    		
    		if(notif.Notification_Grouping__c != null && groupingMap.containsKey(notif.Notification_Grouping__c) == false){
    			
    			Notification_Grouping__c grouping = new Notification_Grouping__c(Id = notif.Notification_Grouping__c);
    			groupingMap.put(notif.Notification_Grouping__c, grouping);    			    			 
    		}
    	}
    	
    	if(groupingMap.size() > 0) update groupingMap.values();
    }
}