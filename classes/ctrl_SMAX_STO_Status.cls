/**
 * @description       : Controller for handling STO status update logic.
 * @author            : tom.h.ansley@medtronic.com
 * @last modified on  : 11-12-2020
 * @last modified by  : tom.h.ansley@medtronic.com
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   09-18-2020   tom.h.ansley@medtronic.com   Initial Version
**/public with sharing class ctrl_SMAX_STO_Status {
    
    public SVMXC__Parts_Request__c order                   {get; set;}
    public Id orderId                                      {get; set;}
    public String pageError                                {get; set;}

    public ctrl_SMAX_STO_Status(ApexPages.StandardController controller) 
    {
        orderId = controller.getId();
    }

    public PageReference getStatus()
    {
        try {

            //get the latest STO with all information
            SVMXC__Parts_Request__c tmpOrder = bl_STOSAPService.getPartsRequest(orderId);
            
            //get the status of the STO from SAP and update the database
            bl_STOSAPService.createCalloutSTOStatus(tmpOrder);

        } catch (Exception e) {
            pageError = e.getMessage() + '&nbsp;&nbsp;' + e.getStackTraceString();
            System.debug(LoggingLevel.ERROR, 'PAGE ERROR - ' + pageError);
            
            order = new SVMXC__Parts_Request__c(Id = orderId);
            order.SVMXC__Status__c = 'Error in Processing';
            order.SAP_Last_Sync_Date_Time__c     = System.now();
            order.SAP_Last_Sync_Error_Message__c = pageError;
            order.SAP_Last_Sync_Message_Type__c  = 'Status';
            order.SAP_Last_Sync_Status__c        = 'Failure';
            
            update order;
        }

        if (pageError != null)
            return null;

        return new PageReference('/' + orderId);
    }

}