/*
 * Author		: Bart Caelen
 * Date			: 2014-01
 * Action		: UPDATED
 * Description	: CR-3014 - Add functionality to clone an implant as Attended or Unattended Implant
*/
public with sharing class controllerImplantCloneWithDetails {

	private String tImplantAttendedType = '';	        //-BC - 201401 - CR-3014 - Added to be able to clone an implant as Attended or Unattended
	private String ImplantId ; 
	private String returnId;  
	private final Implant__c ImplToClone;
	public static String warnText;   
	public static String backText;
	//public boolean enableClone;
	public controllerImplantCloneWithDetails(ApexPages.StandardController implController) {

		this.ImplToClone = (Implant__c)implController.getRecord();
		ImplantId = ApexPages.currentPage().getParameters().get('id');
		system.debug('the pagename = ' + ApexPages.currentPage().getUrl() );
		returnId = ImplantId;
		backText = 'Back to Implant';
		warnText = 'Do you want to clone this implant?';

	}  

	public boolean disableClone { get; set; }

    public PageReference CheckImplant(){
		// Get the original Implant
		Implant__c orgImpl = 
			[
				SELECT 
					Therapy__c, User_ID__c, Therapy_Text__c, Referring_Department_Text__c, Implant_Procedure_Type__c, Procedure_Text__c
					, Implant_Referring_Contact__c, Implant_Referring_Account__c, Implant_Implanting_Contact__c, Implant_Implanting_Account__c
					, Implant_Date_Of_Surgery__c, Duration_Nr__c, Business_Unit__c, Business_Unit_Text__c, Attended_Implant_Text__c, OwnerId
					, Implant_Technique_Text__c, Implant_Therapy__c, MMX_Implant_ID_Text__c 
				FROM 
					Implant__c
				WHERE 
					Id = :ImplantId 
				LIMIT 1
			];

        if (orgImpl.MMX_Implant_ID_Text__c == null || orgImpl.MMX_Implant_ID_Text__c == ''){

			warnText = 'Do you want to clone this implant?';
			backText = 'Back to Implant';
			disableClone = false;
			return null;

        }else{

            warnText = 'MMX implants cannot be cloned'; 
            backText = 'Back to Implant';
            disableClone =true;
            return null;

		}
	}
    
	public PageReference cloneWithDetails(){

		// Get the original Implant
		Implant__c orgImpl = 
			[
				SELECT 
					Therapy__c, User_ID__c, Therapy_Text__c, Referring_Department_Text__c, Implant_Procedure_Type__c, Procedure_Text__c
					, Implant_Referring_Contact__c, Implant_Referring_Account__c, Implant_Implanting_Contact__c, Implant_Implanting_Account__c
					, Implant_Date_Of_Surgery__c, Duration_Nr__c, Business_Unit__c, Business_Unit_Text__c, Attended_Implant_Text__c
					, OwnerId, Implant_Technique_Text__c, Implant_Therapy__c, MMX_Implant_ID_Text__c, Product_Group__c 
				FROM Implant__c
				WHERE
					Id = :ImplantId
				LIMIT 1
			]; //-BC - 20160404 - SREQ-11536 - Added missing field Product_Group__c
		
		Implant__c cloneImpl = new Implant__c();
        // copy the field values of the original record to the clone record 
        cloneImpl.OwnerId = UserInfo.getUserId();
        cloneImpl.Therapy__c = orgImpl.Therapy__c;
        cloneImpl.User_ID__c = orgImpl.User_ID__c;
        cloneImpl.Therapy_Text__c = orgImpl.Therapy_Text__c;
        cloneImpl.Implant_Therapy__c = orgImpl.Implant_Therapy__c;
        cloneImpl.Referring_Department_Text__c = orgImpl.Referring_Department_Text__c;
		cloneImpl.Implant_Procedure_Type__c = orgImpl.Implant_Procedure_Type__c;
        cloneImpl.Procedure_Text__c = orgImpl.Procedure_Text__c;
        cloneImpl.Implant_Referring_Contact__c = orgImpl.Implant_Referring_Contact__c;
        cloneImpl.Implant_Referring_Account__c = orgImpl.Implant_Referring_Account__c;
        cloneImpl.Implant_Implanting_Contact__c = orgImpl.Implant_Implanting_Contact__c;
        cloneImpl.Implant_Implanting_Account__c = orgImpl.Implant_Implanting_Account__c;
        cloneImpl.Implant_Date_Of_Surgery__c = orgImpl.Implant_Date_Of_Surgery__c;
        cloneImpl.Business_Unit__c = orgImpl.Business_Unit__c;
        cloneImpl.Business_Unit_Text__c = orgImpl.Business_Unit_Text__c;
        cloneImpl.Product_Group__c   = orgImpl.Product_Group__c; //-BC - 20160404 - SREQ-11536 - Added missing field
        //-BC - 201401 - CR-3014 - Added to be able to clone an implant as Attended or Unattended - START
        cloneImpl.Attended_Implant_Text__c = tImplantAttendedType;
        if (tImplantAttendedType == 'Attended'){
        	cloneImpl.Duration_Nr__c = clsUtil.isDecimalNull(orgImpl.Duration_Nr__c, 60);
        }else{
        	cloneImpl.Duration_Nr__c = null;
        }
        //-BC - 201401 - CR-3014 - Added to be able to clone an implant as Attended or Unattended - STOP
        cloneImpl.Implant_Technique_Text__c = orgImpl.Implant_Technique_Text__c;
      
		try{
			insert cloneImpl;
		}catch(DmlException e ){
			System.assert(e.getMessage().contains('Insert failed'), e.getMessage());
		}  

		// Clone the detail records  
		List<Implant_Detail_Junction__c> cloneImpldetails = new List<Implant_Detail_Junction__c>();
		for (Implant_Detail_Junction__c orgImpldetails : [SELECT Type_Text__c, Implant_Option_ID__c, Implant_ID__c, CurrencyIsoCode FROM Implant_Detail_Junction__c WHERE Implant_ID__c = :orgImpl.Id]){
	        Implant_Detail_Junction__c implDet = new Implant_Detail_Junction__c();
			implDet.Implant_Option_ID__c = orgImpldetails.Implant_Option_ID__c;
			implDet.Implant_ID__c = cloneImpl.Id;
			implDet.CurrencyIsoCode = orgImpldetails.CurrencyIsoCode;
			cloneImpldetails.add(implDet);
	      }    
		if (cloneImpldetails.size() > 0){

			try{
				insert cloneImpldetails ;
			}catch(DmlException e ){
				System.assert(e.getMessage().contains('Insert failed'), e.getMessage());
			}    

		}
        // Clone the Assets  
        List<Implanted_Product__c> cloneImplAssets = new List<Implanted_Product__c>();

        for (Implanted_Product__c orgImplAssets : [SELECT SerialNumber__c, Quantity__c, Product2__c, Price__c, Name, Implant_ID__c, CurrencyIsoCode, Business_Unit_Name_Text__c, Account__c FROM Implanted_Product__c WHERE Implant_ID__c = :orgImpl.Id]){
			Implanted_Product__c implAsset = new Implanted_Product__c();
				implAsset.SerialNumber__c = orgImplAssets.SerialNumber__c;
				implAsset.Quantity__c = orgImplAssets.Quantity__c;
				implAsset.Product2__c = orgImplAssets.Product2__c;
				implAsset.Price__c = orgImplAssets.Price__c;
				implAsset.Implant_ID__c = cloneImpl.Id;
				implAsset.CurrencyIsoCode = orgImplAssets.CurrencyIsoCode;
				implAsset.Account__c = orgImplAssets.Account__c;
			cloneImplAssets.add(implAsset);
		}    
		if (cloneImplAssets.size() > 0){
			try{
				insert cloneImplAssets ;
			}catch(DmlException e ){
				System.assert(e.getMessage().contains('Insert failed'), e.getMessage());
			}    
		}

        returnId = ImplantId;         
      
		if (cloneImpl.Id != null){
			returnId = cloneImpl.Id;
		}
    
        warnText = 'The implant is successfully cloned';
        backText = 'Go to Implant';

		system.debug('return id = ' + returnId);
		PageReference p = new PageReference('/' + returnId); 
			p.setRedirect(true);
		return p;
	}

	// Show the message on the VF page.
	public string getWarntext(){
		return warntext;
	} 

	// Show the message on the VF page.
	public string getBacktext(){
		return backtext;
	}   

	// Back method will go back to the to deleted Account, because it is not possible to delete
	public PageReference Back(){
		system.debug('return id = ' + returnId);
		PageReference p = new PageReference('/'+ returnId);    
			p.setRedirect(true);
		return p;
	}

	//------------------------------------------
	// ACTIONS    
	//------------------------------------------
    //-BC - 201401 - CR-3014 - Added to be able to clone an implant as Attended or Unattended - START
	public PageReference cloneWithDetails_Attended(){
		tImplantAttendedType = 'Attended';
		return cloneWithDetails();
	}
	public PageReference cloneWithDetails_UnAttended(){
		tImplantAttendedType = 'Unattended';
		return cloneWithDetails();
	}
    //-BC - 201401 - CR-3014 - Added to be able to clone an implant as Attended or Unattended - STOP
	//------------------------------------------
}