global class ba_CVG_Agent_Team_Member implements Schedulable, Database.Batchable<sObject>, Database.Stateful{
	
	/**
   	*  Schedulable   
   	*/
	global void execute(SchedulableContext ctx){
	        
		Database.executeBatch(new ba_CVG_Agent_Team_Member(), 50);
	}
	  
	global Database.QueryLocator start(Database.BatchableContext ctx){
	  	
	  	String query = 'Select Id from User where Profile.Name = \'EUR Field Agent CVG\' and isActive = true';        
	    	        
	    return Database.getQueryLocator(query);
	} 
	  
	global void execute(Database.BatchableContext ctx, List<User> records){
	  		  		  													
	  	Map<Id, Map<Id, List<Opportunity>>> accountOpportunitiesMap = new Map<Id, Map<Id, List<Opportunity>>>();
	  	Map<Id, Map<Id, Account_Plan_2__c>> accountIPlansMap = new Map<Id, Map<Id, Account_Plan_2__c>>();
	  	
	  	for(Account acc : [Select Id, 
	  						(Select Id, Sub_Business_Unit__c, OwnerId from Account_Plan__r where RecordType.DeveloperName IN ('EUR_CVG_sBU', 'MEA_CVG_sBU')), 
	  						(Select Id, Sub_Business_Unit_Id__c, OwnerId from Opportunities where RecordType.DeveloperName = 'CVG_Project' AND Sub_Business_Unit_Id__c != null) 
	  						from Account where Id IN (Select Account__c from Account_Team_Member__c where Primary__c = true and User__c IN :records)]){
	  		
	  		//Opportunities
	  		Map<Id, List<Opportunity>> accOpportunities = new Map<Id, List<Opportunity>>();
	  		accountOpportunitiesMap.put(acc.Id, accOpportunities);
	  		
	  		for(Opportunity accOpp : acc.Opportunities){
	  			
	  			List<Opportunity> accSBUOpportunities = accOpportunities.get(accOpp.Sub_Business_Unit_Id__c);
	  			
	  			if(accSBUOpportunities == null){
	  				
	  				accSBUOpportunities = new List<Opportunity>();
	  				accOpportunities.put(accOpp.Sub_Business_Unit_Id__c, accSBUOpportunities);
	  			}
	  			
	  			accSBUOpportunities.add(accOpp);
	  		}
	  			  		
	  		//Account Plans
	  		Map<Id, Account_Plan_2__c> accIPlans = new Map<Id, Account_Plan_2__c>();
	  		accountIPlansMap.put(acc.Id, accIPlans);
	  		
	  		for(Account_Plan_2__c accIPlan : acc.Account_Plan__r){
	  			
	  			accIPlans.put(accIPlan.Sub_Business_Unit__c, accIPlan);
	  		}
	  	}
	  	
	  	Set<String> uniqueKeys = new Set<String>();
	  	
	  	List<OpportunityTeamMember> toInsertOpp = new List<OpportunityTeamMember>();
	  	List<OpportunityTeamMember> toDeleteOpp = new List<OpportunityTeamMember>();
	  	List<Account_Plan_Team_Member__c> toInsertIPlanMember = new List<Account_Plan_Team_Member__c>();
	  	List<Account_Plan_Team_Member__c> toDeleteIPlanMember = new List<Account_Plan_Team_Member__c>();
	  	
	  	for(User CVGAgent : [Select Id, 
  									(Select Account__c, Therapy_Group__r.Sub_Business_Unit__c from Account_Team_Members__r where Primary__c = true), 
  									(Select Account_Plan__c, Auto_Created__c from Account_Plan_Teams__r), 
  									(Select Id, OpportunityId, Auto_Created__c from OpportunityTeams) 
  								from User where Id IN :records]){
  			
  			Map<Id, OpportunityTeamMember> agentOpportunities = new Map<Id, OpportunityTeamMember>();  			
  			for(OpportunityTeamMember agentOpp : CVGAgent.OpportunityTeams) agentOpportunities.put(agentOpp.OpportunityId, agentOpp);
  			
  			Map<Id, Account_Plan_Team_Member__c> agentIplans = new Map<Id, Account_Plan_Team_Member__c>();
  			for(Account_Plan_Team_Member__c agentIplan : CVGAgent.Account_Plan_Teams__r) agentIPlans.put(agentIplan.Account_Plan__c, agentIPlan);
  			  			
			for(Account_Team_Member__c agentSBUAccount : CVGAgent.Account_Team_Members__r){
				
				String uniqueKey = agentSBUAccount.Account__c + ':' + agentSBUAccount.Therapy_Group__r.Sub_Business_Unit__c;
				
				if(uniqueKeys.add(uniqueKey)){//Only when this combination has not been checked yet as Account Team Member records are on Therapy Group level
					
					List<Opportunity> accSBUOpportunities = accountOpportunitiesMap.get(agentSBUAccount.Account__c).get(agentSBUAccount.Therapy_Group__r.Sub_Business_Unit__c);
					
					if(accSBUOpportunities != null){
						
						for(Opportunity accSBUOpp : accSBUOpportunities){
							
							if(agentOpportunities.remove(accSBUOpp.Id) == null && accSBUOpp.OwnerId != CVGAgent.Id){//If Opportunity not in User's list and User is not owner, create Team Member
								
								OpportunityTeamMember oppTeamMember = new OpportunityTeamMember();
								oppTeamMember.UserId = CVGAgent.Id;
								oppTeamMember.OpportunityId = accSBUOpp.Id;
								oppTeamMember.TeamMemberRole = 'Sales';
								oppTeamMember.Auto_Created__c = true;//Used to identify these type records								
								toInsertOpp.add(oppTeamMember);
							}
						}						
					}
					
					Account_Plan_2__c accSBUIPlan = accountIPlansMap.get(agentSBUAccount.Account__c).get(agentSBUAccount.Therapy_Group__r.Sub_Business_Unit__c);
					
					if(accSBUIplan != null){
						
						if(agentIPlans.remove(accSBUIplan.Id) == null && accSBUIplan.OwnerId != CVGAgent.Id){
							
							Account_Plan_Team_Member__c iplanMember = new Account_Plan_Team_Member__c();
							iplanMember.Account_Plan__c = accSBUIPlan.Id;
							iplanMember.Team_Member__c = CVGAgent.Id;
							iplanMember.Role__c = 'Extended Acc Team';	
							iplanMember.Auto_Created__c = true;//Used to identify these type records	
							toInsertIPlanMember.add(iplanMember);
						}
					}
				}
			}
			
			if(agentOpportunities.size() > 0){
				
				for(OpportunityTeamMember oppTeamMember : agentOpportunities.values()){
					
					if(oppTeamMember.Auto_Created__c == true) toDeleteOpp.add(oppTeamMember);//If it was created automatically, then delete				
				}

			}
			if(agentIPlans.size() > 0){
				
				for(Account_Plan_Team_Member__c iplanTeamMember : agentIPlans.values()){
					
					if(iplanTeamMember.Auto_Created__c == true) toDeleteIPlanMember.add(iplanTeamMember);//If it was created automatically, then delete
				}
			}
  		}
  		
  		if(toInsertOpp.size() > 0) insert toInsertOpp;
  		if(toInsertIPlanMember.size() > 0) insert toInsertIPlanMember;
  		
  		if(toDeleteOpp.size() > 0) delete toDeleteOpp;
  		if(toDeleteIPlanMember.size() > 0) delete toDeleteIPlanMember; 
	} 
	  
	global void finish(Database.BatchableContext BC){
	 	
	}  
}