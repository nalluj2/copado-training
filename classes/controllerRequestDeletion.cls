/**
 * Creation Date :  20090212
 * Description : 	Apex Controller called by the page 'requestDeletion.page' as an extension of the standard controller 'Contact'!
 *					(A Visualforce controller is a set of instructions that specify what happens when a user interacts with the components specified 
 *					 in associated Visualforce markup, such as when a user clicks a button or link. Controllers also provide access to the data that
 *					 should be displayed in a page, and can modify component behavior.)	 
 * Author : 		ABSI - MC
 */
public class controllerRequestDeletion {
		
	public Contact_Deletion_Request__c deletionRequest {get; set;}
	
	private Contact cnt;
	private Account accountToMergeFrom;
	public Account accountToMergeWith {get; set;}	
	
	public Boolean canCloseWindow {get; set;} 
	public String primaryAccountName {get;set;}
	
	public controllerRequestDeletion(ApexPages.StandardController stdController) {
		
		cnt = (Contact) stdController.getRecord();
		
		deletionRequest = new Contact_Deletion_Request__c(); 
		deletionRequest.Contact__c = cnt.Id;            	
		
		accountToMergeFrom = getPrimaryAccount(cnt.Id);
		deletionRequest.Account__c = accountToMergeFrom.Id;
		
		canCloseWindow = false;
	}		

	public List<SelectOption> getAccountNames(){
		
		List<SelectOption> accountOptions = new List<SelectOption>();
		
		if (accountToMergeFrom != null) accountOptions.add(new SelectOption(accountToMergeFrom.Name, accountToMergeFrom.Name));		
		if (accountToMergeWith != null) accountOptions.add(new SelectOption(accountToMergeWith.Name, accountToMergeWith.Name));		
		
		return accountOptions;
	}
	
	public void contactMergeWithChanged(){
				
		if(deletionRequest.Contact_to_Merge_with__c != null) accountToMergeWith = getPrimaryAccount(deletionRequest.Contact_to_Merge_with__c);			
		else accountToMergeWith = null;	
	}
	
	public void send(){
						
		if(deletionRequest.Contact_Deletion_Action__c == 'Delete' && deletionRequest.Contact_Deletion_Reason__c == null){
						
			ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The Reason is required '));
			return;
		}
		
		if(deletionRequest.Contact_Deletion_Action__c == 'Merge'){
			
			if(deletionRequest.Contact_to_Merge_with__c == null){
				
				ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The Contact to merge with is required '));
				return;
			}

			if (deletionRequest.Prim_Acc_name_of_Contact_to_merge_with__c == null || deletionRequest.Prim_Acc_name_of_Contact_to_merge_with__c == ''){
								
				ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The Account to merge with is required '));
				return;
			}
		}
		
		try{
		
			deletionRequest.Contact_Deletion_Requested__c = UserInfo.getName() + ': ' + Datetime.now();									
			insert deletionRequest;
						
			canCloseWindow = true ; 
			
		}catch (DmlException e){
			
		  	ApexPages.addMessages(e);			
		}		  
	}
	
	private Account getPrimaryAccount(Id idContact){
		
		Account acc;

		List<Contact> contacts = [SELECT Id, Account.Id, Account.Name FROM Contact WHERE Id = :idContact];
		
		if (contacts.size() == 1) acc = contacts[0].Account;
		else acc = null;
		
		return acc;
	}
}