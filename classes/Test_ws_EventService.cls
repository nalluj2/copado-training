@isTest
private class Test_ws_EventService {
    
    @TestSetup
    private static void createTestData(){
    	
		User oUser_Admin = clsTestData_User.createUser_SystemAdministratorMDT('tstadm1', false);
		User oUser = clsTestData_User.createUser('tstsusr1', clsUtil.getUserProfileId('EUR Field Force CVG'), clsUtil.getUserRoleId('UK FF (CVG)'), false);
		List<User> lstUser = new List<User>();
			lstUser.add(oUser_Admin);
			lstUser.add(oUser);
		insert lstUser;

		System.runAs(oUser_Admin){

    		Account acc = new Account();
    		acc.Name = 'UnitTest Account';
    		acc.SAP_Id__c = '0123456789';
    		insert acc;
    	
    		// Contact        
			Contact cnt = new Contact();
			cnt.LastName = 'Contact';
			cnt.FirstName = 'UnitTest';
			cnt.AccountId = acc.Id ; 
			cnt.Contact_Department__c = 'Diabetes Adult'; 
			cnt.Contact_Primary_Specialty__c = 'ENT';
			cnt.Affiliation_To_Account__c = 'Employee';
			cnt.Primary_Job_Title_vs__c = 'Manager';
			cnt.Contact_Gender__c = 'Male';
			insert cnt;

		}
    }
    
    private static testmethod void testCreateEvent(){
    	
    	Account what = [Select Id from Account];
        Contact who = [Select Id from Contact];    
    	User assignee = [Select Id from User where isActive = true AND Profile.Name = 'EUR Field Force CVG' LIMIT 1];
    	
    	Test.startTest();
    	
    	String mobileID = GuidUtil.NewGuid();
        
        Event event = new Event();
        event.WhatId = what.Id;
        event.WhoId = who.Id;
        event.OwnerId = assignee.Id;
        event.Mobile_ID__c = mobileID;
        event.Subject = 'Unit Test';
        event.StartDateTime = DateTime.now();
        event.EndDateTime = event.StartDateTime.addHours(2);
        
        JSONGenerator gen = JSON.createGenerator(true);
    	
		gen.writeStartObject();
        gen.writeStringField('id', '12346');
	    gen.writeObjectField('event', event);
	   
        // Get the JSON string.
        String pretty = gen.getAsString();	    	
		System.debug('JSON ' + pretty);
		
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		    			
        req.requestbody = Blob.valueOf(pretty);
    	
     	req.requestURI = '/services/apexrest/CalendarEventService';  
	    req.httpMethod = 'POST';
    	
    	RestContext.request = req;
    	RestContext.response = res;
		
		String resp = ws_EventService.doPost();
		
		System.debug('Response' + resp);
		
		ws_EventService.IFEventResponse responseObject = (ws_EventService.IFEventResponse) JSON.deserialize(resp, ws_EventService.IFEventResponse.class);
		Event evnt = responseObject.event;
		
		System.debug('JSON Response' + JSON.serializePretty(responseObject));
		
		System.assert(responseObject.success == true, responseObject.message);
		
		List<Event> events = [Select Id from Event];// where Mobile_ID__c = :mobileID];
		
		System.assert(events.size() == 1);
    }
    
    private static testmethod void testUpdateEvent(){
    	
    	Account what = [Select Id from Account];
        Contact who = [Select Id from Contact];    
    	User assignee = [Select Id from User where isActive = true AND Profile.Name = 'EUR Field Force CVG' LIMIT 1];
    	    	    	
    	String mobileID = GuidUtil.NewGuid();
        
        Event existingEvent = new Event();
        existingEvent.WhatId = what.Id;        
        existingEvent.OwnerId = assignee.Id;
        existingEvent.Mobile_ID__c = mobileID;
        existingEvent.Subject = 'Unit Test';
        existingEvent.StartDateTime = DateTime.now();
        existingEvent.EndDateTime = existingEvent.StartDateTime.addHours(1);
        insert existingEvent;
        
        Test.startTest();
        
        JSONGenerator gen = JSON.createGenerator(true);
        
        Event event = new Event();
        event.WhatId = what.Id;
        event.WhoId = who.Id;
        event.OwnerId = assignee.Id;
        event.Mobile_ID__c = mobileID;
        event.Subject = 'Unit Test Modified';
        event.StartDateTime = DateTime.now();
        event.EndDateTime = event.StartDateTime.addHours(2);
    	
		gen.writeStartObject();
        gen.writeStringField('id', '12346');
	    gen.writeObjectField('event', event);
	   
        // Get the JSON string.
        String pretty = gen.getAsString();	    	
		System.debug('JSON ' + pretty);
		
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		    			
        req.requestbody = Blob.valueOf(pretty);
    	
     	req.requestURI = '/services/apexrest/CalendarEventService';  
	    req.httpMethod = 'POST';
    	
    	RestContext.request = req;
    	RestContext.response = res;
		
		String resp = ws_EventService.doPost();
		
		System.debug('Response' + resp);
		
		ws_EventService.IFEventResponse responseObject = (ws_EventService.IFEventResponse) JSON.deserialize(resp, ws_EventService.IFEventResponse.class);
		Event evnt = responseObject.event;
		
		System.debug('JSON Response' + JSON.serializePretty(responseObject));
		
		System.assert(responseObject.success == true, responseObject.message);
		
		List<Event> events = [Select Id, Subject, WhoId, DurationInMinutes, Mobile_ID__c from Event];// where Mobile_ID__c = :mobileID];
		
		System.assertEquals(events.size(), 1);
		System.assert(events[0].Subject == 'Unit Test Modified');
		System.assert(events[0].WhoId != null);
		System.assert(events[0].DurationInMinutes == 120);
    }
    
    private static testmethod void testDeleteEvent(){
    	
    	Account what = [Select Id from Account];
        Contact who = [Select Id from Contact];    
    	User assignee = [Select Id from User where isActive = true AND Profile.Name = 'EUR Field Force CVG' LIMIT 1];
    	    	    	
    	String mobileID = GuidUtil.NewGuid();
        
        Event existingEvent = new Event();
        existingEvent.WhatId = what.Id;
        existingEvent.WhoId = who.Id;
        existingEvent.OwnerId = assignee.Id;
        existingEvent.Mobile_ID__c = mobileID;
        existingEvent.Subject = 'Unit Test';
        existingEvent.StartDateTime = DateTime.now();
        existingEvent.EndDateTime = existingEvent.StartDateTime.addHours(2);
        insert existingEvent;
        
        Test.startTest();
        
        Event event = new Event();        
        event.Mobile_ID__c = mobileID;
                
        JSONGenerator gen = JSON.createGenerator(true);
            	
		gen.writeStartObject();
        gen.writeStringField('id', '12346');
	    gen.writeObjectField('event', event);
	   
        // Get the JSON string.
        String pretty = gen.getAsString();	    	
		System.debug('JSON ' + pretty);
		
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		    			
        req.requestbody = Blob.valueOf(pretty);
    	
     	req.requestURI = '/services/apexrest/CalendarEventService';  
	    req.httpMethod = 'DELETE';
    	
    	RestContext.request = req;
    	RestContext.response = res;
		
		String resp = ws_EventService.doDelete();
		
		System.debug('Response' + resp);
		
		ws_EventService.IFEventDeleteResponse responseObject = (ws_EventService.IFEventDeleteResponse) JSON.deserialize(resp, ws_EventService.IFEventDeleteResponse.class);
		Event evnt = responseObject.event;
		
		System.debug('JSON Response' + JSON.serializePretty(responseObject));
		
		System.assert(responseObject.success == true, responseObject.message);
		
		List<Event> events = [Select Id from Event];// where Mobile_ID__c = :mobileID];
		
		System.assert(events.size() == 0);		
    }    
    
    private static testmethod void testCreateEvent_Error(){
    	
    	Account what = [Select Id from Account];
        Contact who = [Select Id from Contact];    
    	User assignee = [Select Id from User where isActive = true AND Profile.Name = 'EUR Field Force CVG' LIMIT 1];
    	
    	Test.startTest();
    	
    	String mobileID = GuidUtil.NewGuid();
        
        Event event = new Event();
        event.WhatId = what.Id;
        event.WhoId = who.Id;
        event.OwnerId = assignee.Id;
        event.Mobile_ID__c = mobileID;        
        event.StartDateTime = DateTime.now();
                
        JSONGenerator gen = JSON.createGenerator(true);
    	
		gen.writeStartObject();
        gen.writeStringField('id', '12346');
	    gen.writeObjectField('event', event);
	   
        // Get the JSON string.
        String pretty = gen.getAsString();	    	
		System.debug('JSON ' + pretty);
		
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		    			
        req.requestbody = Blob.valueOf(pretty);
    	
     	req.requestURI = '/services/apexrest/CalendarEventService';  
	    req.httpMethod = 'POST';
    	
    	RestContext.request = req;
    	RestContext.response = res;
		
		String resp = ws_EventService.doPost();
		
		System.debug('Response' + resp);
		
		ws_EventService.IFEventResponse responseObject = (ws_EventService.IFEventResponse) JSON.deserialize(resp, ws_EventService.IFEventResponse.class);
		Event evnt = responseObject.event;
		
		System.debug('JSON Response' + JSON.serializePretty(responseObject));
		
		System.assert(responseObject.success == false);
//		System.assert(responseObject.message.contains('Required fields are missing')); --> SVMX.Event_Trigger1 null error 
		
		List<Event> events = [Select Id from Event];// where Mobile_ID__c = :mobileID];
		
		System.assert(events.size() == 0);
    }
    
    private static testmethod void testDeleteEvent_Error(){
    	    	    	    	    	
    	String mobileID = GuidUtil.NewGuid();
        
        Test.startTest();
        
        Event event = new Event();        
        event.Mobile_ID__c = mobileID;
                
        JSONGenerator gen = JSON.createGenerator(true);
            	
		gen.writeStartObject();
        gen.writeStringField('id', '12346');
	    gen.writeObjectField('event', event);
	   
        // Get the JSON string.
        String pretty = gen.getAsString();	    	
		System.debug('JSON ' + pretty);
		
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		    			
        req.requestbody = Blob.valueOf(pretty);
    	
     	req.requestURI = '/services/apexrest/CalendarEventService';  
	    req.httpMethod = 'DELETE';
    	
    	RestContext.request = req;
    	RestContext.response = res;
		
		String resp = ws_EventService.doDelete();
		
		System.debug('Response' + resp);
		
		ws_EventService.IFEventDeleteResponse responseObject = (ws_EventService.IFEventDeleteResponse) JSON.deserialize(resp, ws_EventService.IFEventDeleteResponse.class);
		Event evnt = responseObject.event;
		
		System.debug('JSON Response' + JSON.serializePretty(responseObject));
		
		System.assert(responseObject.success == true, responseObject.message);
		System.assert(responseObject.message == 'event not found');				
    }  
}