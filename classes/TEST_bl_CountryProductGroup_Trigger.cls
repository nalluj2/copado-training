@isTest private class TEST_bl_CountryProductGroup_Trigger {
	
	private static List<DIB_Country__c> lstCountry;
	private static List<Product_Group__c> lstProductGroup;
	private static List<Country_ProductGroup__c> lstCountryProductGroup;

	private static User oUser_Devart;

	@isTest static void createTestData() {
		
		// Get the Devart Tool User (Has Write Access to the field CountryProductGroup__c)
		oUser_Devart = clsTestData_User.createUser('test.user.001@medtronic.com.test', 'tu001', 'BELGIUM', clsUtil.getUserProfileId('Devart Tool'), clsUtil.getUserRoleId('EUR Data Steward'), true);

		System.runAs(oUser_Devart){

			// Create DIB_Country__c Data
			clsTestData_MasterData.tCountryCode = 'BE';
			lstCountry = clsTestData_MasterData.createCountry(false);
			clsTestData_MasterData.oMain_Country = null;
			clsTestData_MasterData.tCountryCode = 'NL';
			lstCountry.addAll(clsTestData_MasterData.createCountry(false));
			insert lstCountry;

			// Create Product_Group__c Data
			lstProductGroup = clsTestData_Product.createProductGroup(false);
			clsTestData_Product.oMain_ProductGroup = null;
			lstProductGroup.addAll(clsTestData_Product.createProductGroup(false));
			insert lstProductGroup;

			// Create Country_ProductGroup__c Data
			lstCountryProductGroup = new List<Country_ProductGroup__c>();
			Country_ProductGroup__c oCountryProductGroup = new Country_ProductGroup__c();
				oCountryProductGroup.Country__c = lstCountry[0].Id;
				oCountryProductGroup.Product_Group__c = lstProductGroup[0].Id;
				oCountryProductGroup.Country_ASP__c = 0.0;
			lstCountryProductGroup.add(oCountryProductGroup);

		}

	}
	
	@isTest static void test_populateExternalId_Insert() {
		
		// Create Test Data
		createTestData();

		// Perform Test
		Test.startTest();

		System.runAs(oUser_Devart){

			insert lstCountryProductGroup;

		}

		Test.stopTest();


		// Validate Result
		lstCountryProductGroup = [SELECT Id, Country__c, Product_Group__c, CountryProductGroup__c FROM Country_ProductGroup__c];
		for (Country_ProductGroup__c oCountryProductGroup : lstCountryProductGroup){

			System.assertEquals(oCountryProductGroup.CountryProductGroup__c, oCountryProductGroup.Country__c + '-' + oCountryProductGroup.Product_Group__c);
			System.assertEquals(oCountryProductGroup.CountryProductGroup__c, lstCountry[0].Id + '-' + lstProductGroup[0].Id);

		}


	}

	@isTest static void test_populateExternalId_Update() {

		// Create Test Data
		createTestData();

		System.runAs(oUser_Devart){
			insert lstCountryProductGroup;
		}

		// Perform Test
		Test.startTest();

		System.runAs(oUser_Devart){

			lstCountryProductGroup[0].Country__c = lstCountry[1].Id;
			update lstCountryProductGroup;

		}

		Test.stopTest();


		// Validate Result
		lstCountryProductGroup = [SELECT Id, Country__c, Product_Group__c, CountryProductGroup__c FROM Country_ProductGroup__c];
		for (Country_ProductGroup__c oCountryProductGroup : lstCountryProductGroup){

			System.assertEquals(oCountryProductGroup.CountryProductGroup__c, oCountryProductGroup.Country__c + '-' + oCountryProductGroup.Product_Group__c);
			System.assertEquals(oCountryProductGroup.CountryProductGroup__c, lstCountry[1].Id + '-' + lstProductGroup[0].Id);

		}

	}
	
}