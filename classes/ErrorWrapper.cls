public with sharing class ErrorWrapper {
	public ErrorWrapper(boolean error,List<Work_Item__c> items){
		this.hasError = error;
		this.workItems = items;
	}
	
	public List<Work_Item__c> workItems{get;set;}
	public boolean hasError{get;set;}
}