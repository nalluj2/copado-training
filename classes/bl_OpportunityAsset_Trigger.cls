//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 28-11-2018
//  Description      : APEX Class - Business Logic for tr_OpportunityAsset
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public class bl_OpportunityAsset_Trigger {

	//-----------------------------------------------------------------------------------------------------------
	// Update the DIEN_Code__c on Opportunity_Asset__c based on the Asset CFN Code and Service Level
	//-----------------------------------------------------------------------------------------------------------
	public static void updateDIENCode(List<Opportunity_Asset__c> lstTriggerNew, Map<Id, Opportunity_Asset__c> mapTriggerOld){
	
        if (bl_Trigger_Deactivation.isTriggerDeactivated('opp_updateDIENCode')) return;

		Set<Id> setID_Asset = new Set<Id>();
		List<Opportunity_Asset__c> lstOpportunityAsset_Processing = new List<Opportunity_Asset__c>();
		for (Opportunity_Asset__c oOpportunityAsset : lstTriggerNew){
			
			if (String.isBlank(oOpportunityAsset.DIEN_Code__c)){
			
				setID_Asset.add(oOpportunityAsset.Asset__c);
				lstOpportunityAsset_Processing.add(oOpportunityAsset);
			
			}else{
			
				if (mapTriggerOld != null){
					
					Opportunity_Asset__c oOpportunityAsset_OLD = mapTriggerOld.get(oOpportunityAsset.Id);

					if (oOpportunityAsset.Service_Level__c != oOpportunityAsset_OLD.Service_Level__c){
						
						setID_Asset.add(oOpportunityAsset.Asset__c);
						lstOpportunityAsset_Processing.add(oOpportunityAsset);
		
					}
				
				}

			}

		}

		if (lstOpportunityAsset_Processing.size() == 0) return;

		Map<Id, Asset> mapAsset = new Map<Id, Asset>([SELECT Id, CFN_Text__c FROM Asset WHERE Id = :setID_Asset]);

		for (Opportunity_Asset__c oOpportunityAsset : lstOpportunityAsset_Processing){
			
			String tCFNCode = mapAsset.get(oOpportunityAsset.Asset__c).CFN_Text__c;

			String tDIENCode = clsUtil.getDIENCode(tCFNCode, oOpportunityAsset.Service_Level__c);
			
			oOpportunityAsset.DIEN_Code__c = tDIENCode;

		}

	}
	//-----------------------------------------------------------------------------------------------------------

}