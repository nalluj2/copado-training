//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 15-12-2015
//  Description      : Batch APEX to RETRY the callouts to WebMethods record by record.  Each record will be a job.
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
global class ba_NotificationSAP_Retry implements Database.Batchable<sObject>, Database.AllowsCallouts {
	
    //----------------------------------------------------------------------------------------
	// GENERAL PARAMETERS
    //----------------------------------------------------------------------------------------
	global Set<Id> setId_Record = new Set<Id>();
	global String tSOQL = '';
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
	// CONSTRUCTOR
    //----------------------------------------------------------------------------------------
	global ba_NotificationSAP_Retry() {

	}
    //----------------------------------------------------------------------------------------
	

    //----------------------------------------------------------------------------------------
    // START - Create SOQL Query
    //----------------------------------------------------------------------------------------
	global Database.QueryLocator start(Database.BatchableContext BC) {
		
        // Select the data that needs reprocessing
        if (clsUtil.isNull(tSOQL, '') == ''){

			tSOQL = ut_Log.tSOQL_NotificationSAPLog();

			String tWhere = '';
			if (setId_Record.size() > 0){
				String tIDs = '';
				for (Id id_Record : setId_Record){
		            tIDs += '\'' + id_Record + '\',';
				}
		        tIDs = tIDs.left(tIDs.length() - 1);
	        	
	        	tWhere = ' WHERE Id  in (' + tIDs + ')';
			}else{
				// Make sure no data is selected if no ID's are passed as a parameter
				tWhere = ' WHERE Id = null';
	        }
			tSOQL += tWhere;

			tSOQL += ' ORDER BY';
				tSOQL += ' SFDC_Object_Name__c';
		}
		if (Test.isRunningTest()){
			tSOQL += ' LIMIT 1';
		}

		return Database.getQueryLocator(tSOQL);
	}
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
    // EXECUTE - Process the selected data in batches
    //----------------------------------------------------------------------------------------
   	global void execute(Database.BatchableContext BC, List<NotificationSAPLog__c> lstNotificationSAPLog) {

   		// RETRY CALLOUT
   		//	- reprocess the whole record based on the actual data
   		for (NotificationSAPLog__c oNotificationSAPLog : lstNotificationSAPLog){

			String tSOQL = 'SELECT ' + oNotificationSAPLog.SFDC_Fieldname_ID__c;
			if (clsUtil.isNull(oNotificationSAPLog.SFDC_Fieldname_SAPID__c, '') != ''){
				tSOQL += ', ' + oNotificationSAPLog.SFDC_Fieldname_SAPID__c;
			}
			tSOQL += ' FROM ' + oNotificationSAPLog.SFDC_Object_Name__c;
			tSOQL += ' WHERE ' + oNotificationSAPLog.SFDC_Fieldname_ID__c + ' = \'' + oNotificationSAPLog.Record_ID__c + '\'';

			List<sObject> lstData = Database.query(tSOQL);

			for (sObject oData : lstData){
				bl_NotificationSAP.NotificationSAP oNotificationSAP = new bl_NotificationSAP.NotificationSAP();
					oNotificationSAP.oNotificationSAPLog = oNotificationSAPLog;
					oNotificationSAP.tWM_Object = oNotificationSAPLog.WM_Object_Name__c;
					oNotificationSAP.tWM_Operation = oNotificationSAPLog.WM_Operation__c;
					oNotificationSAP.tWM_Process = oNotificationSAPLog.WM_Process__c;
					oNotificationSAP.tSFDC_Object = oNotificationSAPLog.SFDC_Object_Name__c;
					oNotificationSAP.tSFDC_FieldName_Id = oNotificationSAPLog.SFDC_Fieldname_ID__c;
					oNotificationSAP.tSFDC_FieldName_SAPID = oNotificationSAPLog.SFDC_Fieldname_SAPID__c;
					oNotificationSAP.tDirection = bl_NotificationSAP.DIRECTION;
					oNotificationSAP.tWebServiceName = oNotificationSAPLog.WebServiceName__c;

				bl_NotificationSAP.callWebMethod(oData, oNotificationSAP);
			}

   		}

	}
    //----------------------------------------------------------------------------------------

	
    //----------------------------------------------------------------------------------------
    // FINISH - Perform logic after all batch jobs are completed
    //----------------------------------------------------------------------------------------
	global void finish(Database.BatchableContext BC) {

	}
    //----------------------------------------------------------------------------------------
	
}
//--------------------------------------------------------------------------------------------------------------------