@isTest public class clsTestData_OSS {

    public static Integer iRecord_OSS = 2;
    public static Set<String> setOSSName = new Set<String>{'CVG', 'RTG'};
    public static One_Stop_Shop__c oMain_OSS;
    public static List<One_Stop_Shop__c> lstOSS = new List<One_Stop_Shop__c>();

    public static Integer iRecord_OSSSection = 1;
    public static OSS_Section__c oMain_OSSSection;
    public static List<OSS_Section__c> lstOSSSection = new List<OSS_Section__c>();

    public static Integer iRecord_OSSContent = 1;
    public static OSS_Content__c oMain_OSSContent;
    public static List<OSS_Content__c> lstOSSContent = new List<OSS_Content__c>();

    public static Integer iRecord_OSSPermission = 1;
    public static OSS_Permission__c oMain_OSSPermission;
    public static List<OSS_Permission__c> lstOSSPermission = new List<OSS_Permission__c>();

    public static Integer iRecord_OSSUserOverride = 1;
    public static OSS_User_Override__c oMain_OSSUserOverride;
    public static List<OSS_User_Override__c> lstOSSUserOverride = new List<OSS_User_Override__c>();


    //---------------------------------------------------------------------------------------------------
    // Create All Data related to One Stop Shop
    //---------------------------------------------------------------------------------------------------
    public static void createAllData(){

        if (oMain_OSS == null) createOSS();
        if (oMain_OSSSection == null) createOSSSection();
        if (oMain_OSSContent == null) createOSSContent();
        if (oMain_OSSPermission == null) createOSSPermission();
        if (oMain_OSSUserOverride == null) createOSSUserOverride();

    }
    //---------------------------------------------------------------------------------------------------


    //---------------------------------------------------------------------------------------------------
    // Create One Stop Shop Data
    //---------------------------------------------------------------------------------------------------
    public static List<One_Stop_Shop__c> createOSS(){
        return createOSS(true);
    }
    public static List<One_Stop_Shop__c> createOSS(Boolean bInsert){

        if (oMain_OSS == null){

            lstOSS = new List<One_Stop_Shop__c>();
            if (setOSSName.size() == iRecord_OSS){
                for (String tOSSName : setOSSName){
                    One_Stop_Shop__c oOSS = new One_Stop_Shop__c();
                        oOSS.Name = tOSSName;
                    lstOSS.add(oOSS);
                }
            }else{
                for (Integer i=0; i<iRecord_OSS; i++){
                    One_Stop_Shop__c oOSS = new One_Stop_Shop__c();
                        oOSS.Name = 'Test One Stop Shop ' + i;
                    lstOSS.add(oOSS);
                }
            }
            oMain_OSS = lstOSS[0];

            if (bInsert) insert lstOSS;
        }

        return lstOSS;
    }
    //---------------------------------------------------------------------------------------------------


    //---------------------------------------------------------------------------------------------------
    // Create OSS Section Data
    //---------------------------------------------------------------------------------------------------
    public static List<OSS_Section__c> createOSSSection(){
        return createOSSSection(true);
    }
    public static List<OSS_Section__c> createOSSSection(Boolean bInsert){

        if (oMain_OSSSection == null){

            if (oMain_OSS == null) createOSS();

            lstOSSSection = new List<OSS_Section__c>();
            for (One_Stop_Shop__c oOSS : lstOSS){
                for (Integer i=0; i<iRecord_OSSSection; i++){
                    OSS_Section__c oOSSSection = new OSS_Section__c();
                        oOSSSection.Active__c = true;
                        oOSSSection.Description__c = '';
                        oOSSSection.Logo__c = '';
                        oOSSSection.One_Stop_Shop__c = oOSS.Id;
                        oOSSSection.Section_Label__c = 'Label ' + i;
                        oOSSSection.Sequence__c = i;
                    lstOSSSection.add(oOSSSection);
                }               
            }

            oMain_OSSSection = lstOSSSection[0];

            if (bInsert) insert lstOSSSection;
        }

        return lstOSSSection;

    }
    //---------------------------------------------------------------------------------------------------


    //---------------------------------------------------------------------------------------------------
    // Create OSS Content Data
    //---------------------------------------------------------------------------------------------------
    public static List<OSS_Content__c> createOSSContent(){
        return createOSSContent(true);
    }
    public static List<OSS_Content__c> createOSSContent(Boolean bInsert){

        if (oMain_OSSContent == null){

            if (oMain_OSSSection == null) createOSSSection();

            lstOSSContent = new List<OSS_Content__c>();
            for (OSS_Section__c oOSSSection : lstOSSSection){
                for (Integer i=0; i<iRecord_OSSContent; i++){
                    OSS_Content__c oOSSContent = new OSS_Content__c();
                        oOSSContent.Active__c = true;
                        oOSSContent.Button_Color__c = '#00C4B3';
                        oOSSContent.Description__c = 'Test Description Content';
                        oOSSContent.Link__c = 'https://www.google.be';
                        oOSSContent.Section_Name__c = oOSSSection.Id;
                        oOSSContent.Sequence__c = i;
                        oOSSContent.Sub_Group__c = null;
                        oOSSContent.Target__c = null;
                        oOSSContent.Text__c = 'Test OSS Content ' + i;
                        oOSSContent.Title__c = 'Test OSS Content Title ' + i;
                    lstOSSContent.add(oOSSContent);
                }               
            }

            oMain_OSSContent = lstOSSContent[0];

            if (bInsert) insert lstOSSContent;
        }

        return lstOSSContent;

    }
    //---------------------------------------------------------------------------------------------------


    //---------------------------------------------------------------------------------------------------
    // Create OSS Permission Data
    //---------------------------------------------------------------------------------------------------
    public static List<OSS_Permission__c> createOSSPermission(){
        return createOSSPermission(true);
    }
    public static List<OSS_Permission__c> createOSSPermission(Boolean bInsert){

        if (oMain_OSSPermission == null){

            if (oMain_OSSContent == null) createOSSContent();

            lstOSSPermission = new List<OSS_Permission__c>();
            for (OSS_Content__c oOSSContent : lstOSSContent){
                for (Integer i=0; i<iRecord_OSSPermission; i++){
                    OSS_Permission__c oOSSPermission = new OSS_Permission__c();
                        oOSSPermission.Active__c = true;
                        oOSSPermission.Business_Unit_vs__c = null;
                        oOSSPermission.Company_Code__c = 'EUR';
                        oOSSPermission.Country_vs__c = null;
                        oOSSPermission.Job_Title_vs__c = null;
                        oOSSPermission.Operator__c = 'Include';
                        oOSSPermission.OSS_Content__c = oOSSContent.Id;
                        oOSSPermission.Region__c = 'Europe';
                        oOSSPermission.Sub_Business_Unit__c = null;
                        oOSSPermission.Sub_Region__c = null;    
                    lstOSSPermission.add(oOSSPermission);
                }               
            }

            oMain_OSSPermission = lstOSSPermission[0];

            if (bInsert) insert lstOSSPermission;
        }

        return lstOSSPermission;

    }
    //---------------------------------------------------------------------------------------------------


    //---------------------------------------------------------------------------------------------------
    // Create OSS Permission Data
    //---------------------------------------------------------------------------------------------------
    public static List<OSS_User_Override__c> createOSSUserOverride(){
        return createOSSUserOverride(true);
    }
    public static List<OSS_User_Override__c> createOSSUserOverride(Boolean bInsert){

        if (oMain_OSSUserOverride == null){

            if (oMain_OSSContent == null) createOSSContent();

            lstOSSUserOverride = new List<OSS_User_Override__c>();
            for (OSS_Content__c oOSSContent : lstOSSContent){
                for (Integer i=0; i<iRecord_OSSUserOverride; i++){
                    OSS_User_Override__c oOSSUserOverride = new OSS_User_Override__c();
                        oOSSUserOverride.User_Operator__c = 'Include';
                        oOSSUserOverride.OSS_Content__c = oOSSContent.Id;
                        oOSSUserOverride.User__c = UserInfo.getUserId();
                    lstOSSUserOverride.add(oOSSUserOverride);
                }               
            }

            oMain_OSSUserOverride = lstOSSUserOverride[0];

            if (bInsert) insert lstOSSUserOverride;
        }

        return lstOSSUserOverride;

    }
    //---------------------------------------------------------------------------------------------------

}