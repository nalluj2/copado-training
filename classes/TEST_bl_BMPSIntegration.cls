@isTest 
private class TEST_bl_BMPSIntegration {

	@TestSetup
	private static void generateCustomSettings(){
		
		BPMS_API_Settings__c oBPMSAPISettings= new BPMS_API_Settings__c();
			oBPMSAPISettings.Active__c = true;
			oBPMSAPISettings.Token_Authentication__c = '123546789';
			oBPMSAPISettings.Org_Id__c = UserInfo.getOrganizationId();
			oBPMSAPISettings.Target_Server_URL__c  = 'https://fakeURL.com';		
			oBPMSAPISettings.Timeout__c = 60000;
			oBPMSAPISettings.Maximum_Records__c = 50;
		insert oBPMSAPISettings;

	}


	@isTest
	private static void test_initiateOnboarding() {

		//-------------------------------------------------------
		// Create Test Data
		//-------------------------------------------------------
		clsTestData_MasterData.createCountry();

		Id id_RecordType_Campaign = clsUtil.getRecordTypeByDevName('Campaign', 'LEARNING').Id;

		clsTestData_CustomSetting.mapCampaignMemberStatus_Responded = new Map<String, Boolean>{'Start enrollment'=>true};
		clsTestData_CustomSetting.createCampaignMemberStatus(true, 'RecordTypeID', id_RecordType_Campaign);


		clsTestData_Campaign.idRecordType_Campaign_LMS = id_RecordType_Campaign;
		List<Campaign> lstCampaign = clsTestData_Campaign.createCampaignLMS(false);
			lstCampaign[0].Campaign_Language__c = 'Dutch';
			lstCampaign[0].LMS_Division__c = 'Division';
			lstCampaign[0].IsActive = true;
		insert lstCampaign;
		lstCampaign = [SELECT Id, RecordTypeID, Campaign_Country_vs__c, LMS_Language_ISO__c, LMS_Division__c FROM Campaign WHERE Id = :lstCampaign];
		System.assertEquals(lstCampaign.size(), 1);
		System.assertEquals(lstCampaign[0].RecordTypeID, id_RecordType_Campaign);

		clsTestData_Contact.createMDTEmployee();

		clsTestData_Contact.iRecord_Contact = 2;
		List<Contact> lstContact = clsTestData_Contact.createContact(false);
			lstContact[0].MailingCountry = 'Belgium';
			lstContact[0].LMS_Key_Customer_Contact__c = clsTestData_Contact.oMain_MDTEmployee.Id;
			lstContact[1].MailingCountry = 'Netherlands';
			lstContact[1].LMS_Key_Customer_Contact__c = clsTestData_Contact.oMain_MDTEmployee.Id;
		insert lstContact;

		clsTestData_Campaign.iRecord_CampaignMember = 2;
		List<CampaignMember> lstCampaignMember = clsTestData_Campaign.createCampaignMember(lstCampaign[0].Id, false);

		lstContact = [SELECT Id, Email, MailingCountry, LMS_IsActive__c, LMS_Language__c, LMS_Country__c, LMS_Division__c, LMS_Userid__c FROM Contact WHERE Id = :clsTestData_Contact.lstContact];
		System.assertEquals(lstContact.size(), 2);
		for (Contact oContact : lstContact){
			System.assertEquals(oContact.LMS_isActive__c, false);
			System.assertEquals(oContact.LMS_Division__c, null);
		}
		//-------------------------------------------------------


		//-------------------------------------------------------
		// Test Logic
		//-------------------------------------------------------
		Test.startTest();

		Test.setMock(HttpCalloutMock.class, new TEST_BPMSIntegration_Mock());

		for (CampaignMember oCampaignMember : lstCampaignMember){
			oCampaignMember.Status = 'Start enrollment';
		}
		insert lstCampaignMember;
		
		//-------------------------------------------------------
		// Verify Result
		//-------------------------------------------------------
		List<Contact_ED_Sync__c> syncRecords = [Select Id, Outcome__c from Contact_ED_Sync__c];
		System.assertEquals(syncRecords.size(), 2);
		
		Test.stopTest();
		
		syncRecords = [Select Id, Outcome__c from Contact_ED_Sync__c];
		System.assertEquals(syncRecords.size(), 2);
		
		for(Contact_ED_Sync__c syncRecord : syncRecords){
			System.assertEquals(syncRecord.Outcome__c, 'Success');
		}
		
	}
}