@isTest
private class Test_ctrl_massSendEmail_WOSP_ReqInvoice {
    
    private static String tCountryEmail = 'test@medtronic.com.test';

	@TestSetup
	static void createTestData() {

		//----------------------------------------------------------------
		// Create Test Data
		//----------------------------------------------------------------
		// Custom Setting
		clsTestData_CustomSetting.createCustomSettingData();

		// DIB_Country__c
		clsTestData_MasterData.createCountry();
		List<DIB_Country__c> lstCountry_Update = new List<DIB_Country__c>();
		DIB_Country__c oCountry_BE = clsTestData_MasterData.mapCountry.get('BE');
			oCountry_BE.EmaiL_Service_Repair__c = 'belgium01@medtronic.com.test;belgium02@medtronic.com.test';
			oCountry_BE.EmaiL_Inside_Sales__c = 'belgium01@medtronic.com.test;belgium02@medtronic.com.test';
		lstCountry_Update.add(oCountry_BE);
		DIB_Country__c oCountry_NL = clsTestData_MasterData.mapCountry.get('NL');
			oCountry_NL.EmaiL_Service_Repair__c = 'netherlands01@medtronic.com.test';
			oCountry_NL.EmaiL_Inside_Sales__c = 'belgium01@medtronic.com.test;belgium02@medtronic.com.test';
		lstCountry_Update.add(oCountry_NL);
		update lstCountry_Update;

		// Product
		List<Product2> lstProduct = new List<Product2>();
		Product2 oProduct1 = new Product2();
			oProduct1.Name = 'PoleStar1';
			oProduct1.ProductCode = '000999';
			oProduct1.isActive = true;					
		lstProduct.add(oProduct1);
		Product2 oProduct2 = new Product2();
			oProduct2.Name = 'O-arm';
			oProduct2.ProductCode = '1111000';
			oProduct2.isActive = true;					
		lstProduct.add(oProduct2);
		insert lstProduct;

		// Account
		clsTestData_Account.iRecord_Account = 1;
		clsTestData_Account.idRecordType_Account = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non-SAP Account').getRecordTypeId();
		List<Account> lstAccount = clsTestData_Account.createAccount(false);
		for (Account oAccount : lstAccount){
			oAccount.AccountNumber = '0000000';
			oAccount.Name = 'Test Account';
			oAccount.Phone = '+123456789';
			oAccount.BillingCity = 'TEST CITY';
			oAccount.BillingCountry = 'BELGIUM';
			oAccount.BillingState = 'TEST STATE';
			oAccount.BillingStreet = 'TEST STREET';
			oAccount.BillingPostalCode = '123456';
			oAccount.ST_NAV_Non_SAP_Account__c = true;
		}
		insert lstAccount;
		Account oAccount = lstAccount[0];

		// Contact
		clsTestData_Contact.iRecord_Contact = 2;
		List<Contact> lstContact = clsTestData_Contact.createContact();

		// Asset
		List<Asset> lstAsset = new List<Asset>();
		Asset oAsset1 = new Asset();
			oAsset1.AccountId = oAccount.Id;
			oAsset1.Product2Id = oProduct1.Id;
			oAsset1.Asset_Product_Type__c = 'S8 Planning Station';
			oAsset1.Ownership_Status__c = 'Purchased';
			oAsset1.Name = '123456789';
			oAsset1.Serial_Nr__c = '123456789';
			oAsset1.Status = 'Installed';
			oAsset1.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('PoleStar').getRecordTypeId();
		lstAsset.add(oAsset1);
		Asset oAsset2 = new Asset();
			oAsset2.AccountId = oAccount.Id;
			oAsset2.Product2Id = oProduct2.Id;
			oAsset2.Asset_Product_Type__c = 'PoleStar N-10';
			oAsset2.Ownership_Status__c = 'Purchased';
			oAsset2.Name = '987654321';
			oAsset2.Serial_Nr__c = '987654321';
			oAsset2.Status = 'Installed';
			oAsset2.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('PoleStar').getRecordTypeId();
		lstAsset.add(oAsset2);
		insert lstAsset;
				
		// Case		
		Case oCase1 = new Case();
			oCase1.AccountId = oAccount.Id;
			oCase1.AssetId = oAsset1.Id;
			oCase1.RecordTypeId = clsUtil.getRecordTypeByDevName('Case', 'New_Case').Id;
			oCase1.External_Id__c = 'Test_Case_Id_1';		
		insert oCase1;
	
		// Workorder		
		Workorder__c oWorkorder = new Workorder__c();
			oWorkorder.Account__c = oAccount.Id;
			oWorkorder.Asset__c = oAsset1.Id;
			oWorkorder.Status__c = 'In Process';
			oWorkorder.External_Id__c = 'Test_Work_Order_Id_1';
			oWorkorder.Case__c = oCase1.Id;
			oWorkorder.RecordTypeId = Schema.SObjectType.Workorder__c.getRecordTypeInfosByName().get('PoleStar System Checkout').getRecordTypeId();		
		insert oWorkorder;
		
		// Workorder Sparepart
		List<Workorder_Sparepart__c> lstWorkorderSparepart = new List<Workorder_Sparepart__c>();
		Workorder_Sparepart__c oWorkorderSparepart1 = new Workorder_Sparepart__c();
			oWorkorderSparepart1.Case__c = oCase1.Id;
			oWorkorderSparepart1.Account__c = oAccount.Id;
			oWorkorderSparepart1.Asset__c = oAsset1.Id;
			oWorkorderSparepart1.External_Id__c = 'Test_SparePart_Id_1';		
			oWorkorderSparepart1.Amount_of_Inaccuracy__c = 1000;
			oWorkorderSparepart1.RecordTypeId = Schema.SObjectType.Workorder_Sparepart__c.getRecordTypeInfosByName().get('Inaccuracy').getRecordTypeId();
			oWorkorderSparepart1.Return_Item_Status__c = 'Returned Unused';
			oWorkorderSparepart1.Order_Status__c = 'Shipped';
			oWorkorderSparepart1.Order_Lot_Number__c = '0011223344';
			oWorkorderSparepart1.Workorder__c = oWorkorder.Id;
		lstWorkorderSparepart.add(oWorkorderSparepart1);
		Workorder_Sparepart__c oWorkorderSparepart2 = new Workorder_Sparepart__c();
			oWorkorderSparepart2.Case__c = oCase1.Id;
			oWorkorderSparepart2.Account__c = oAccount.Id;
			oWorkorderSparepart2.Asset__c = oAsset2.Id;
			oWorkorderSparepart2.External_Id__c = 'Test_SparePart_Id_2';		
			oWorkorderSparepart2.Amount_of_Inaccuracy__c = 1000;
			oWorkorderSparepart2.RecordTypeId = Schema.SObjectType.Workorder_Sparepart__c.getRecordTypeInfosByName().get('Inaccuracy').getRecordTypeId();
			oWorkorderSparepart2.Return_Item_Status__c = 'Part Not Returned';
			oWorkorderSparepart2.Order_Status__c = 'Shipped';
			oWorkorderSparepart2.Order_Lot_Number__c = '4433221100';
			oWorkorderSparepart2.Workorder__c = oWorkorder.Id;
		lstWorkorderSparepart.add(oWorkorderSparepart2);		
		insert lstWorkorderSparepart;		
	}
	
	@isTest static void test_ctrl_massSendEmail_WOSP_RequestPQ() {

		Boolean bTest = false;
		String tTest = '';
		
		List<Case> lstCase = 
			[
				SELECT 
					Id, CaseNumber
		            , ShippingStreet__c
		            , ShippingPostalCode__c
		            , ShippingCity__c
		            , ShippingState__c
		            , ShippingCountry__c
		            , Account.BillingStreet
		            , Account.BillingPostalCode
		            , Account.BillingCity
		            , Account.BillingState
		            , Account.BillingCountry
				FROM 
					Case				
			];
		System.assertEquals(lstCase.size(), 1);
		Case oCase = lstCase[0];

		List<User> lstUser = [SELECT Id, Email FROM User WHERE IsActive = true AND License_Name__c = 'Salesforce' AND Id != :UserInfo.getUserId() LIMIT 2];
		List<Contact> lstContact = [SELECT Id, Name FROM Contact];

        List<OrgWideEmailAddress> lstOrgWideEmailAddress = [SELECT Id, DisplayName, Address FROM OrgWideEmailAddress ORDER BY DisplayName];
		List<Apexpages.Message> lstAPEXPageMessage = new List<Apexpages.Message>();
		//----------------------------------------------------------------


		//----------------------------------------------------------------
		// Perform Test
		//----------------------------------------------------------------
		Test.startTest();

		PageReference oPageRef = Page.massSendEmail_WOSP_RequestInvoice;
        Test.setCurrentPage(oPageRef);

	        ApexPages.currentPage().getParameters().put('id', oCase.Id);	        
			ApexPages.currentPage().getParameters().put('additionalemailto', 'test@medtronic.com.test');
			
		ctrl_massSendEmail_WOSP_RequestInvoice oCTRL = new ctrl_massSendEmail_WOSP_RequestInvoice();

		oCTRL.mapCountry_EmailAddresses = new Map<String, List<String>>();
			oCTRL.mapCountry_EmailAddresses.put('BELGIUM', new List<String>{'belgium01@medtronic.com.test', 'belgium02@medtronic.com.test'});
			oCTRL.mapCountry_EmailAddresses.put('POLAND', new List<String>{'poland01@medtronic.com.test'});
		
		oCTRL.mapEmailAddresses_Country = new Map<String, List<String>>();
			oCTRL.mapEmailAddresses_Country.put('belgium01@medtronic.com.test', new List<String>{'BELGIUM'});
			oCTRL.mapEmailAddresses_Country.put('belgium02@medtronic.com.test', new List<String>{'BELGIUM'});
			oCTRL.mapEmailAddresses_Country.put('poland01@medtronic.com.test', new List<String>{'POLAND'});			
			
		oCTRL.mapCountry_EmailAddresses_IS = new Map<String, List<String>>();
			oCTRL.mapCountry_EmailAddresses_IS.put('BELGIUM', new List<String>{'belgium01@medtronic.com.test', 'belgium02@medtronic.com.test'});
			oCTRL.mapCountry_EmailAddresses_IS.put('POLAND', new List<String>{'poland01@medtronic.com.test'});
		
	

		//--------------------------------------------
		// ACTIONS - NO ERRORS
		//--------------------------------------------
		// Populate all required values
		System.assertEquals(oCTRL.lstwrWOSP.size(), 1);		
		oCTRL.lstwrWOSP[0].bSelected = true;		
		oCTRL.owrData.tPONumber = '123456789';
		oCTRL.owrData.dLaborHours = 5;
		oCTRL.owrData.dTravelHours = 1;
		
		//--------------------------------------------
		// ACTION - saveData (No Error)
		//--------------------------------------------
		oCTRL.saveData();
		
		//--------------------------------------------
		// ACTION - processWOSP (No Error)
		//--------------------------------------------
		oCTRL.lstId_CC.add(lstContact[0].Id);
		oCTRL.lstId_CC.add(lstContact[1].Id);
		oCTRL.id_EmailFrom = UserInfo.getUserId();	// To make sure we don't use Org Wide Email address as from because the running user might not have access to it		
		oCTRL.processWOSP();
		lstAPEXPageMessage = ApexPages.getMessages();
		System.assert(lstAPEXPageMessage.size() > 0);
		bTest = false;
		for (Apexpages.Message oMessage : lstAPEXPageMessage){
    		if (oMessage.getDetail().contains('The selected records are process successfully')) bTest = true;
    		System.debug('**BC** CASE oMessage.getDetail() : ' + oMessage.getDetail());
		}
		System.assert(bTest);

		oCTRL.backToRecord();

		Test.stopTest();
	}	
}