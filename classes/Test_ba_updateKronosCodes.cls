@isTest
private class Test_ba_updateKronosCodes {
	
	static testmethod void testBatchApex(){
		
		Time_Registration_Code__c regCode = createSampleRegistrationcode();
		Project_Management__c proj = createSampleProject(regCode);
		Project__c rel = createSampleRelease(regCode);
		Change_Request__c cr = createSampleCR(regCode);
		Reporting_Activity__c act = createSampleActivity();
		
		List<Time_Reporting__c> reports = new List<Time_Reporting__c>();
		
		Time_Reporting__c report1 = new Time_Reporting__c();
		report1.Reporting_Activity__c = act.Id;
		report1.Release__c = rel.Id;
		report1.Hours__c = 2;
		report1.OwnerId = UserInfo.getUserId();
		report1.Date__c = Date.today();		
		reports.add(report1);
		
		Time_Reporting__c report2 = new Time_Reporting__c();
		report2.Reporting_Activity__c = act.Id;
		report2.Change_Request__c = cr.Id;
		report2.Hours__c = 2;
		report2.OwnerId = UserInfo.getUserId();
		report2.Date__c = Date.today();		
		reports.add(report2);
		
		Time_Reporting__c report3 = new Time_Reporting__c();
		report3.Reporting_Activity__c = act.Id;
		report3.Project__c = proj.Id;
		report3.Hours__c = 2;
		report3.OwnerId = UserInfo.getUserId();
		report3.Date__c = Date.today();		
		reports.add(report3);
		
		insert reports;
		
		Test.startTest();
		
		regCode.Name = 'projCode changed';
		regCode.Time_Registration_Category__c = 'catCode changed';
		update regCode;
		
		//rel.Kronos_Category__c = 'catCode changed';
		//rel.Kronos_Project__c = 'projCode changed';
		//update rel;
		
		
		ba_updateKronosCodes batch = new ba_updateKronosCodes();
		batch.changeRequestIds.add(cr.Id);
		batch.projectIds.add(proj.Id);
		batch.releaseIds.add(rel.Id);
		Database.executeBatch(batch);
		
		Test.stopTest();
		
		for(Time_Reporting__c report : [Select K_Category__c, K_Project__c from Time_Reporting__c where Id IN:reports]){
						
			System.assert(report.K_Category__c == 'catCode changed');
			System.assert(report.K_Project__c == 'projCode changed');
		}		
	}
	
	/*
		HELPER METHODS
	*/
	
	private static Time_Registration_Code__c createSampleRegistrationcode(){
		
		Time_Registration_Code__c regCode = new Time_Registration_Code__c();
		regCode.Name='projCode';
		regCode.Time_Registration_Category__c = 'catCode';
		regCode.Active__c = true;
		regCode.Business_Unit__c = 'Test BU';
		regCode.Time_Registration_ID__c = 'TRC';
				
		insert regCode;
		
		return regCode;
	}
	private static Project__c createSampleRelease(Time_Registration_Code__c regCode){
		
		Project__c release = new Project__c();
		release.Name='Test Release';
		release.Start_Date__c=Date.today();
		release.Project_Type__c='Minor Release';
		release.Status__c='Scheduled';
		release.Project_Manager__c=UserInfo.getUserId();
		release.End_Date__c=Date.today().addDays(10);
		//release.Kronos_Category__c='catCode';
		//release.Kronos_Project__c='projCode';
		release.Time_Registration_Project__c = regCode.Id;
		release.Total_Budget__c=1000;
		
		insert release;
		
		return release;
	}
	
	private static Change_Request__c createSampleCR(Time_Registration_Code__c regCode){
		
		Change_Request__c cr = new Change_Request__c();
		
		cr.Change_Request__c = 'Unit test sample CR';
		cr.Details__c = 'Unit Test details';
		cr.Status__c = 'New';
		cr.Origin__c='Support Portal';			
		cr.Request_Date__c = Date.today();
		cr.Priority__c = 'Medium';		
		cr.Requestor_Lookup__c = UserInfo.getUserId();
		cr.Time_Registration_Project__c = regCode.Id;
		//cr.Kronos_Category__c='catCode';
		//cr.Kronos_Project__c='projCode';
								
		insert cr;
		
		return cr;
	}
	
	private static Project_Management__c createSampleProject(Time_Registration_Code__c regCode){
		
		Project_Management__c project = new Project_Management__c();
		project.Project_Name__c='Test Project';
		project.Requestor__c = UserInfo.getUserId();
		project.Region__c = 'Western Europe and Canada';
		project.Project_Description__c = 'Unit Test Description';
		project.Business_Rationale__c = 'Increase Revenue';
		project.Background_Information__c = 'Unit Test information';
		project.Problem_Statement__c = 'No Problem';
		project.Time_Registration_Project__c = regCode.Id;
		//project.Kronos_Category__c='catCode';
		//project.Kronos_Project__c='projCode';
		
		insert project;
		
		return project;
	}
	
	private static Reporting_Activity__c createSampleActivity(){
		
		Reporting_Activity__c activity = new Reporting_Activity__c();
		activity.Name = 'Test Activity';
		activity.Requires_Manual_Input__c = false;
		activity.chargeable__c = true;
		
		insert activity;
		
		return activity;
	}	
}