//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 15-04-2016
//  Description      : APEX Class - Business Logic for tr_OpportunityLineItem
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public class bl_OpportunityLineItem_Trigger {

    public static void setPriceBasedOnAllLost(List<OpportunityLineItem> lstTriggerNew){
        setPriceBasedOnAllLost(lstTriggerNew, null);
    }
    public static void setPriceBasedOnAllLost(List<OpportunityLineItem> lstTriggerNew, Map<Id, OpportunityLineItem> mapTriggerOld){

        if (Trigger.isInsert){

            for (OpportunityLineItem oOLI : lstTriggerNew){

                if (oOLI.All_Lost__c){
                    oOLI.Original_Price__c = oOLI.UnitPrice;
                    oOLI.UnitPrice = 0;
                }

            }


        }else if (Trigger.isUpdate){

            for (OpportunityLineItem oOLI : lstTriggerNew){

                if (oOLI.All_Lost__c && !mapTriggerOld.get(oOLI.Id).All_Lost__c){
                    oOLI.Original_Price__c = oOLI.UnitPrice;
                    oOLI.UnitPrice = 0;
                }else if (!oOLI.All_Lost__c && mapTriggerOld.get(oOLI.Id).All_Lost__c){
                    oOLI.UnitPrice = oOLI.Original_Price__c;
                }

            }

        }

    }

    public static void deleteOpportunityLineItemSchedule(List<OpportunityLineItem> lstTriggerNew){

        Set<Id> setID_OLI = new Set<Id>();
        for (OpportunityLineItem oOLI : lstTriggerNew){

            if (oOLI.All_Lost__c){
                setID_OLI.add(oOLI.Id);
            }

        }
        
        if (setID_OLI.size() > 0){
            List<OpportunityLineItemSchedule> lstOLI_Schedule = [SELECT Id FROM OpportunityLineItemSchedule WHERE OpportunityLineItemId = :setID_OLI];
            if (lstOLI_Schedule.size() > 0){
                delete lstOLI_Schedule;     
            }
        }

    }


    //--------------------------------------------------------------------------------------------------------------------
    // FOR IHS (EUR, MEA) 
    //  Update Opportunity fields based on Opportunity Line Item data (RLS = Roll-Up-Summary)
    //      - IHS_Service_Amount__c
    //--------------------------------------------------------------------------------------------------------------------
    public static void updateOpportunityField_RLS_IHS(List<OpportunityLineItem> lstTriggerNew, Map<Id, OpportunityLineItem> mapTriggerOld){

        Set<Id> setID_RecordType = new Set<Id>();
            setID_RecordType.add(clsUtil.getRecordTypeByDevName('Opportunity', 'EMEA_IHS_Managed_Services').Id);
            setID_RecordType.add(clsUtil.getRecordTypeByDevName('Opportunity', 'EMEA_IHS_Consulting_Opportunity').Id);

        Set<String> setProductName_Exclude = new Set<String>();
            setProductName_Exclude.add('R1 CV Products');
            setProductName_Exclude.add('R2 NV Products');
            setProductName_Exclude.add('R3 SI Products');
            setProductName_Exclude.add('R4 Diabetes Products');
            setProductName_Exclude.add('R6 CV Products Incremental');
            setProductName_Exclude.add('R7 NV Products Incremental');
            setProductName_Exclude.add('R8 SI Products Incremental');
            setProductName_Exclude.add('R9 Diabetes Products Incremental');
            setProductName_Exclude.add('R10 CV Equipment Incremental');
            setProductName_Exclude.add('R11 NV Equipment Incremental');
            setProductName_Exclude.add('R12 SI Equipment Incremental');


        Set<String> setProductName_Parent_M5 = new Set<String>();
            setProductName_Parent_M5.add('M5 CCN');

        Set<Id> setID_Opportunity = new Set<Id>();
        if (lstTriggerNew != null){
            for (OpportunityLineItem oOLI : lstTriggerNew){
                setID_Opportunity.add(oOLI.OpportunityId);
            }
        }
        if (mapTriggerOld != null){
            for (OpportunityLineItem oOLI : mapTriggerOld.values()){
                setID_Opportunity.add(oOLI.OpportunityId);
            }
        }

        List<Opportunity> lstOpportunity =
            [
                SELECT 
                    Id, IHS_Service_Amount__c, CCN_Amount__c
                    , (SELECT Product2.Name, Product2.Parent_Product__r.Name, TotalPrice, Quantity, UnitPrice FROM OpportunityLineItems)
                FROM
                    Opportunity
                WHERE
                    Id = :setID_Opportunity
                    AND RecordTypeId = :setID_RecordType
            ];

        Map<Id, Opportunity> mapOpportunity_Update = new Map<Id, Opportunity>();

        for (Opportunity oOpportunity : lstOpportunity){

            Decimal decIHSServiceAmount = 0;
            Decimal decCCNOverview = 0;

            for (OpportunityLineItem oOLI : oOpportunity.OpportunityLineItems){

                if (setProductName_Parent_M5.contains(oOLI.Product2.Name)) continue;    // don't include this parent product in any calculations

                // Calculate the Child products
                if (setProductName_Parent_M5.contains(oOLI.Product2.Parent_product__r.Name)){

                    decCCNOverview  += clsUtil.isDecimalNull(oOLI.TotalPrice, 0);           // Quantity * UnitPrice

                }

                // Calculate IHS_Service_Amount__c
                if (!setProductName_Exclude.contains(oOLI.Product2.Name)){

                    decIHSServiceAmount += clsUtil.isDecimalNull(oOLI.TotalPrice, 0);       // Quantity * UnitPrice

                }


            }

            oOpportunity.IHS_Service_Amount__c = decIHSServiceAmount;
            oOpportunity.CCN_Amount__c = decCCNOverview;

        }

        if (lstOpportunity.size() > 0){

            update lstOpportunity;

        }

    }
    //--------------------------------------------------------------------------------------------------------------------


    //--------------------------------------------------------------------------------------------------------------------
    // Author       : Bart Caelen
    // Date         : 21-03-2018
    // Description  : CR-16105
    // Update the Business_Unit_Group__c, Business_Unit_msp__c, Sub_Business_Unit__c and Therapy__c fields on Opportunity 
    //  with the data on the Product of the OpportunityLineItem
    //--------------------------------------------------------------------------------------------------------------------
    public static void setOpportunity_BUG_BU_SBU(List<OpportunityLineItem> lstTriggerNew){

        if (bl_Trigger_Deactivation.isTriggerDeactivated('OLI_setOpportunity_BUG_BU_SBU')) return;

        // Set Opportunity RecordTypes for which we need to process this logic
        Set<Id> setID_RecordType = new Set<Id>();
        setID_RecordType.add(clsUtil.getRecordTypeByDevName('Opportunity', 'Capital_Opportunity').Id);
        setID_RecordType.add(clsUtil.getRecordTypeByDevName('Opportunity', 'Non_Capital_Opportunity').Id);
        setID_RecordType.add(clsUtil.getRecordTypeByDevName('Opportunity', 'Service_Contract_Opportunity').Id);
        setID_RecordType.add(clsUtil.getRecordTypeByDevName('Opportunity', 'Strategic_Account_Management').Id);
        System.debug('** setOpportunity_BUG_BU_SBU - setID_RecordType (' + setID_RecordType.size() + ') : ' + setID_RecordType); 

        // Get all Opportunity records that should be processed based on the processing OpportunityLineItems and the provided Opportunity RecordTypes
        Set<Id> setID_Opportunity = new Set<Id>();
        for (OpportunityLineItem oOLI : lstTriggerNew){
            setID_Opportunity.add(oOLI.OpportunityId);
        }
        System.debug('** setOpportunity_BUG_BU_SBU - setID_Opportunity (' + setID_Opportunity.size() + ') : ' + setID_Opportunity); 

        Map<Id, Opportunity> mapOpportunity = new Map<Id, Opportunity>(
            [
                SELECT Id, Name, Business_Unit_Group__c, Business_Unit_msp__c, Sub_Business_Unit__c
                FROM Opportunity
                WHERE 
                    Id = :setID_Opportunity
                    AND RecordTypeId = :setID_RecordType
                    AND IsClosed = false
            ]
        );
        System.debug('** setOpportunity_BUG_BU_SBU - mapOpportunity (' + mapOpportunity.size() + ') : ' + mapOpportunity); 

        // Get the OpportunityLineItes that are related the collect Opportunities that need processing
        List<OpportunityLineItem> lstOLI = 
            [
                SELECT 
                    Id, OpportunityId
                    , PriceBookEntry.Product2.Business_Unit_Group__c, PriceBookEntry.Product2.Business_Unit_ID__c, PriceBookEntry.Product2.Sub_Business_Unit__c
                    , PricebookEntry.Product2.Product_Group__r.Therapy_ID__r.Name
                    , Products_Therapies__c
                FROM 
                    OpportunityLineItem
                WHERE 
                    OpportunityId in :mapOpportunity.keySet()
            ];
        System.debug('** setOpportunity_BUG_BU_SBU - lstOLI (' + lstOLI.size() + ') : ' + lstOLI); 


        // Get a Map of Opportunity ID and the related OpportunityLineItems
        // Collect also the Business Units ID and Business Unit Group ID of the Products on the OpportunityLineItems
        Set<Id> setID_BUG = new Set<Id>();
        Set<Id> setID_BU = new Set<Id>();
        Map<Id, List<OpportunityLineItem>> mapOpportunityId_OLI = new Map<Id, List<OpportunityLineItem>>();
        for (OpportunityLineItem oOLI : lstOLI){

            List<OpportunityLineItem> lstOLI_Tmp = new List<OpportunityLineItem>();
            if (mapOpportunityId_OLI.containsKey(oOLI.OpportunityId)){
                lstOLI_Tmp = mapOpportunityId_OLI.get(oOLI.OpportunityId);
            }
            lstOLI_Tmp.add(oOLI);
            mapOpportunityId_OLI.put(oOLI.OpportunityId, lstOLI_Tmp);

            if (oOLI.PriceBookEntry.Product2.Business_Unit_ID__c != null){
                setID_BU.add(oOLI.PriceBookEntry.Product2.Business_Unit_ID__c);
            }

            if (oOLI.PricebookEntry.Product2.Business_Unit_Group__c != null){
                setID_BUG.add(oOLI.PriceBookEntry.Product2.Business_Unit_Group__c);
            }


        }
        System.debug('** setOpportunity_BUG_BU_SBU - mapOpportunityId_OLI (' + mapOpportunityId_OLI.size() + ') : ' + mapOpportunityId_OLI); 
        System.debug('** setOpportunity_BUG_BU_SBU - setID_BU (' + setID_BU.size() + ') : ' + setID_BU); 
        System.debug('** setOpportunity_BUG_BU_SBU - setID_BUG (' + setID_BUG.size() + ') : ' + setID_BUG); 


        // Get the Business Unit Group, Business Unit and Sub Business Unit based on the collected Sub Business Unit ID's
        Map<Id, Business_Unit__c> mapBU = new Map<Id, Business_Unit__c>(
            [
                SELECT 
                    Id, Name
                    , Business_Unit_Group__c, Business_Unit_Group__r.Name
                FROM 
                    Business_Unit__c
                WHERE 
                    Id = :setID_BU
            ]
        ); 
        System.debug('** setOpportunity_BUG_BU_SBU - mapBU (' + mapBU.size() + ') : ' + mapBU); 


        // Get the Business Unit Group, Business Unit and Sub Business Unit based on the collected Sub Business Unit ID's
        Map<Id, Business_Unit_Group__c> mapBUG = new Map<Id, Business_Unit_Group__c>(
            [
                SELECT 
                    Id, Name
                FROM 
                    Business_Unit_Group__c
                WHERE 
                    Id = :setID_BUG
            ]
        ); 
        System.debug('** setOpportunity_BUG_BU_SBU - mapBUG (' + mapBUG.size() + ') : ' + mapBUG); 


        // Loop through the processing Opportunities and for each related OpportunityLineItem get the name of the Business Unit Group, Business Unit and Sub Business Unit
        //  and concatenate each one in a field on Opportunity
        List<Opportunity> lstOpportunity_Update = new List<Opportunity>();
        for (Id id_Opportunity : mapOpportunity.keySet()){

            if (mapOpportunityId_OLI.containsKey(id_Opportunity)){

                // Opportunity has OpportunityLineItems - populate fields on Opportunity

                Set<String> setBUG = new Set<String>();
                Set<String> setBU = new Set<String>();
                Set<String> setSBU = new Set<String>();
                Set<String> setTherapy = new Set<String>();

                for (OpportunityLineItem oOLI : mapOpportunityId_OLI.get(id_Opportunity)){

                    if (oOLI.PriceBookEntry.Product2.Business_Unit_ID__c != null){

                        Business_Unit__c oBU = mapBU.get(oOLI.PriceBookEntry.Product2.Business_Unit_ID__c);
                        System.debug('** setOpportunity_BUG_BU_SBU - oBU : ' + oBU); 

                        String tBUG = '';
                        String tBU = '';
                        String tSBU = '';
                        String tTherapy = '';
                        try{ tBUG = oBU.Business_Unit_Group__r.Name; }catch(Exception oEX){}
                        try{ tBU = oBU.Name; }catch(Exception oEX){}
                        try{ tSBU = oOLI.PriceBookEntry.Product2.Sub_Business_Unit__c; }catch(Exception oEX){}
                        try{ tTherapy = oOLI.PricebookEntry.Product2.Product_Group__r.Therapy_ID__r.Name; }catch(Exception oEX){}

                        System.debug('** setOpportunity_BUG_BU_SBU - tBUG : ' + tBUG); 
                        System.debug('** setOpportunity_BUG_BU_SBU - tBU : ' + tBU); 
                        System.debug('** setOpportunity_BUG_BU_SBU - tSBU : ' + tSBU); 
                        System.debug('** setOpportunity_BUG_BU_SBU - tTherapy : ' + tTherapy); 

                        if (!String.isBlank(tBUG)) setBUG.add(tBUG);
                        if (!String.isBlank(tBU)) setBU.add(tBU);
                        if (!String.isBlank(tSBU)) setSBU.add(tSBU);
                        if (!String.isBlank(tTherapy)) setTherapy.add(tTherapy);
                    
                    }else if (oOLI.PriceBookEntry.Product2.Business_Unit_Group__c != null){

                        Business_Unit_Group__c oBUG = mapBUG.get(oOLI.PriceBookEntry.Product2.Business_Unit_Group__c);
                        System.debug('** setOpportunity_BUG_BU_SBU - oBUG : ' + oBUG); 

                        String tBUG = '';
                        try{ tBUG = oBUG.Name; }catch(Exception oEX){}

                        System.debug('** setOpportunity_BUG_BU_SBU - tBUG : ' + tBUG); 

                        if (!String.isBlank(tBUG)) setBUG.add(tBUG);

                    }

                }
                System.debug('** setOpportunity_BUG_BU_SBU - setBUG : ' + setBUG); 
                System.debug('** setOpportunity_BUG_BU_SBU - setBU : ' + setBU); 
                System.debug('** setOpportunity_BUG_BU_SBU - setSBU : ' + setSBU); 
                System.debug('** setOpportunity_BUG_BU_SBU - setTherapy : ' + setTherapy); 

                List<String> lstBUG = new List<String>();
                lstBUG.addAll(setBUG);
                String tBUG = String.join(lstBUG, ';');

                List<String> lstBU = new List<String>();
                lstBU.addAll(setBU);
                String tBU = String.join(lstBU, ';');

                List<String> lstSBU = new List<String>();
                lstSBU.addAll(setSBU);
                String tSBU = String.join(lstSBU, ';');

                List<String> lstTherapy = new List<String>();
                lstTherapy.addAll(setTherapy);
                String tTherapy = String.join(lstTherapy, ';');

                System.debug('** setOpportunity_BUG_BU_SBU - tBUG 2 : ' + tBUG); 
                System.debug('** setOpportunity_BUG_BU_SBU - tBU 2 : ' + tBU); 
                System.debug('** setOpportunity_BUG_BU_SBU - tSBU 2 : ' + tSBU); 
                System.debug('** setOpportunity_BUG_BU_SBU - tTherapy 2 : ' + tTherapy); 

                Opportunity oOpportunity_Update = mapOpportunity.get(id_Opportunity);
                    oOpportunity_Update.Business_Unit_Group__c = tBUG;
                    oOpportunity_Update.Business_Unit_msp__c = tBU;
                    oOpportunity_Update.Sub_Business_Unit__c = tSBU;
                    oOpportunity_Update.Therapies__c = tTherapy;
                lstOpportunity_Update.add(oOpportunity_Update);

            }else{

                // Opportunity has NO OpportunityLineItems - clear fields on Opportunity
                Opportunity oOpportunity_Update = mapOpportunity.get(id_Opportunity);
                    oOpportunity_Update.Business_Unit_Group__c = null;
                    oOpportunity_Update.Business_Unit_msp__c = null;
                    oOpportunity_Update.Sub_Business_Unit__c = null;
                    oOpportunity_Update.Therapies__c = null;
                lstOpportunity_Update.add(oOpportunity_Update);

            }

        }

        System.debug('** setOpportunity_BUG_BU_SBU - lstOpportunity_Update (' + lstOpportunity_Update.size() + ') : ' + lstOpportunity_Update); 
        if (lstOpportunity_Update.size() > 0){

            List<Database.SaveResult> lstSaveResult_Update = Database.update(lstOpportunity_Update, false);
            System.debug('** setOpportunity_BUG_BU_SBU - lstSaveResult_Update (' + lstSaveResult_Update.size() + ') : ' + lstSaveResult_Update); 

            Integer iCounter = 0;
            for (Database.SaveResult oSaveResult : lstSaveResult_Update){
                
                if (!oSaveResult.isSuccess()){                                    
                    String tError = '';
                    for (Database.Error oError : oSaveResult.getErrors()){
                        tError += 'Field : ' + oError.getFields() + ' - ' + oError.getMessage() + '; ';
                    }
                    System.debug('** Error while saving Opportunity "' + lstOpportunity_Update[iCounter].Name + '" (' + lstOpportunity_Update[iCounter].Id + ') : ' + tError);
                }
                iCounter++;
            }

        }

    }
    //--------------------------------------------------------------------------------------------------------------------
    
        
    @TestVisible
    private static Set<Id> alreadyScheduled = new Set<Id>();
    
    public static void setLineItemScheduleRevenue(List<OpportunityLineItem> inScope){
        
        List<OpportunityLineItem> oppLineItems = [Select Id, TotalPrice, Opportunity.Contract_Length_in_months__c, Opportunity.CloseDate from OpportunityLineItem 
                                                    where Opportunity.RecordType.DeveloperName IN ('Service_Repair', 'Service_Contract_Opportunity')
                                                    AND Opportunity.StageName = 'Closed Won'  
                                                    AND Opportunity.Contract_Length_in_months__c != null
                                                    AND Id IN :inScope];
                                                    
        if(oppLineItems.size() > 0){
    
            List<OpportunityLineItemSchedule> newScheduleObjects = new List<OpportunityLineItemSchedule>();
            
            Set<Id> processedLineItems = new Set<Id>();
                        
            for (OpportunityLineItem item : oppLineItems) { 
                
                //If it was already in the list of processed, then we skip
                if(alreadyScheduled.add(item.Id) == false) continue;                
                processedLineItems.add(Item.Id);                

                Decimal totalPrice = item.TotalPrice;
                
                Integer numberPayments = Integer.valueOf(item.Opportunity.Contract_Length_in_months__c);                
                Decimal paymentAmount = totalPrice.divide(numberPayments,0,System.RoundingMode.DOWN);
                Date currentDate = item.Opportunity.CloseDate;
                
                Decimal totalPaid = 0;
                    
                for(Integer i = 1;i < numberPayments;i++) {
                    
                    OpportunityLineItemSchedule revenueSchedule = new OpportunityLineItemSchedule();
                    revenueSchedule.Revenue = paymentAmount;
                    revenueSchedule.ScheduleDate = currentDate;
                    revenueSchedule.OpportunityLineItemId = item.id;
                    revenueSchedule.Type = 'Revenue';
                    
                    newScheduleObjects.add(revenueSchedule);
                    
                    totalPaid += paymentAmount;                    
                    currentDate = currentDate.addMonths(1);
                }
                    
                //Now Calulate the last payment!
                paymentAmount = item.TotalPrice - totalPaid;
                
                OpportunityLineItemSchedule revenueSchedule = new OpportunityLineItemSchedule();
                revenueSchedule.Revenue = paymentAmount;
                revenueSchedule.ScheduleDate = currentDate;
                revenueSchedule.OpportunityLineItemId = item.id;
                revenueSchedule.Type = 'Revenue';
                
                newScheduleObjects.add(revenueSchedule);                             
            }
            
            if(processedLineItems.size() > 0){
                
                delete [Select Id from OpportunityLineItemSchedule where OpportunityLineItemId IN :processedLineItems];
        
                insert newScheduleObjects;
            }   
        }               
    }

}
//--------------------------------------------------------------------------------------------------------------------