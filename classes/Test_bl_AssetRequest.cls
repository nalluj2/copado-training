/*
 *      Description : This is the Test Class for the APEX Class bl_AssetRequest
 *      Version     : 1.0
 *      Author      : Bart Caelen
 *      Date        : 2014-07-31
*/
@isTest private class Test_bl_AssetRequest {
	
	@isTest static void test_bl_AssetRequest() {
		Integer iCounter = 0;
		Boolean bTest = false;
        String tTest = '';
		
	    //---------------------------------------
        // Create Test Data
        //---------------------------------------
        clsTestData_MasterData.createCompany();
        clsTestData_MasterData.createBusinessUnit();
        clsTestData_MasterData.createSubBusinessUnit();
        clsTestData_MasterData.tCountryCode = 'BE';
        clsTestData_MasterData.createCountry();
        clsTestData_Account.tCountry_Account = 'BELGIUM';
        clsTestData_Account.createAccount();
        clsTestData_Contact.createContact();
        clsTestData_MasterData.createBURelatedListSetupData_Asset();
        clsTestData_Product.createProduct();		
        clsTestData_MasterData.createAssetRequestEmail();
	    //---------------------------------------


	    //---------------------------------------
	    // GET TEST USER
	    //---------------------------------------
        Profile oProfile = [SELECT ID FROM Profile WHERE Name = 'System Administrator MDT' LIMIT 1];

        User oUser = new User(
            profileid = oProfile.Id            
        	, alias = 'test1'
        	, email = 'test1sfdc@medtronic.com'             
            , emailencodingkey = 'UTF-8'
            , lastname = 'test1'             
            , languagelocalekey = 'en_US'             
            , localesidkey = 'en_US'
            , timezonesidkey = 'America/Los_Angeles'             
            , CountryOR__c = clsTestData_Account.tCountry_Account        
            , Country = clsTestData_Account.tCountry_Account
            , username = 'test1sfdc@testorg.medtronic.com'
            , Alias_unique__c = 'test1_alias'
            , Company_Code_Text__c = 'Eur'
            , User_Business_Unit_vs__c = 'CRHF'
            , Job_Title_vs__c = 'Technical Consultant'
        );  
        insert oUser; 
        
        Contact userContact = new Contact();        
        userContact.LastName = 'test1';        
        userContact.SFDC_USer__c = oUser.Id;
        userContact.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('MDT_Employee').getRecordTypeId();
        insert userContact;
        
        
	    //---------------------------------------

	    //---------------------------------------
	    // Perform Testing
	    //---------------------------------------
        System.runAs(oUser){

        	bl_AssetRequest.loadBusinessUnitData(clsTestData_Account.tCountry_Account);
        	system.assertNotEquals(bl_AssetRequest.lstSO_BusinessUnit, null);
        	system.assertNotEquals(bl_AssetRequest.lstSO_BusinessUnit.size(), 0);

            Asset_Request__c oAssetRequest = new Asset_Request__c();
                oAssetRequest.Account__c = clsTestData_Account.oMain_Account.Id;
                oAssetRequest.Product__c = clsTestData_Product.oMain_Product.Id;
                oAssetRequest.Business_Unit__c = clsTestData_MasterData.oMain_BusinessUnit.Id;
                oAssetRequest.Type__c = 'Small Capital';
                oAssetRequest.Asset_Change_Type__c = 'Move Consignment Asset';
                oAssetRequest.Purchase_Date__c = Date.today();
                oAssetRequest.Serial_Number__c = '001';
                oAssetRequest.Software_version__c = '1.0';
                oAssetRequest.Action__c = 'Create';

            List<Attachment> lstAttachment = new List<Attachment>();
            Attachment oAttachment = new Attachment();
                oAttachment.ParentId= clsTestData_Account.oMain_Account.Id;
                oAttachment.Name = 'TEST Attachment';
                oAttachment.Body = Blob.valueOf('TEST BODY');
            lstAttachment.add(oAttachment);

            Test.startTest();

            Asset_Request_Email__c oAssetRequestEmail = bl_AssetRequest.getAssetRequestEmail(oAssetRequest, 'BELGIUM');

            System.assertNotEquals(oAssetRequestEmail, null);

            bTest = bl_AssetRequest.createAssetRequest(oAssetRequest, oAssetRequestEmail, lstAttachment, true);

            System.assertEquals(bTest, true);

            oAssetRequest.Business_Unit__c = null;
            oAssetRequest.Asset_Change_Type__c = null;
            Asset_Request_Email__c oAssetRequestEmail2 = bl_AssetRequest.getAssetRequestEmail(oAssetRequest, '');

            System.assertEquals(oAssetRequestEmail2, null);

            Test.stopTest();

		}
	    //---------------------------------------

 	}
	
}