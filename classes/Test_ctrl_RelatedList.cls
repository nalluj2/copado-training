/*
 *      Description : This is the Test Class for the APEX Controller ctrl_RelatedList
 *      Version     : 1.0
 *      Author      : Bart Caelen
 *      Date        : 201403
*/
@isTest private class Test_ctrl_RelatedList {
	
	private static Asset_Support_Mail__c oAssetSupportMail; 

	private static User oUser_System;
	private static User oUser;

	@isTest static void createTestData(){

		// Create Users
        oUser_System = clsTestData_User.createUser_SystemAdministratorMDT('tadm01', true);
        oUser = clsTestData_User.createUser('test.user.001@medtronic.com.test', 'tu001', 'NETHERLANDS', clsUtil.getUserProfileId('EUR Field Force RTG'), clsUtil.getUserRoleId('EUR xBU'), false);
        	oUser.User_Business_Unit_vs__c = 'Neurovascular';
        insert oUser;

        //---------------------------------------
        // Create Test Data
        //---------------------------------------
  		System.runAs(oUser_System){

	        clsTestData_MasterData.tCompanyCode = 'EUR';
	        clsTestData_MasterData.createCompany();
	        clsTestData_MasterData.tBusinessUnitGroupName = 'Restorative';
	        clsTestData_MasterData.createBusinessUnitGroup();
	        clsTestData_MasterData.tBusinessUnitName = 'Neurovascular';
	        clsTestData_MasterData.createBusinessUnit();
	        clsTestData_MasterData.tSubBusinessUnitName = 'Neurovascular';
	        clsTestData_MasterData.createSubBusinessUnit();
	        clsTestData_MasterData.id_User = oUser.ID;
	        clsTestData_MasterData.createUserBusinessUnit();
			clsTestData_Asset.iRecord_Asset = 10;
			clsTestData_Asset.createAsset();
	        clsTestData_MasterData.createBURelatedListSetupData_Asset();

			// Asset Support Mail
			if (oAssetSupportMail == null){            
		        oAssetSupportMail = new Asset_Support_Mail__c();
		            oAssetSupportMail.name = 'ITALY';
		            oAssetSupportMail.Email__c = 'info@medtronic.com';
		        insert oAssetSupportMail;
			}

		}
        //---------------------------------------

	}

    @isTest static void test_ctrl_RelatedList_BUG() {

		Integer iCounter = 0;
		List<sObject> lstSObject;
		sObject oSObject;
		String tTest = '';


        //---------------------------------------
		// Create Test Dta    
        //---------------------------------------
		createTestData();
        //---------------------------------------

 
        //---------------------------------------
		// Perform Testing    
        //---------------------------------------
    	Test.startTest();

    	System.runAs(oUser){

			// -- TEST WITH NO User Business Unit -- //

			// -- TEST WITH Business Unit Group -- //
			ctrl_RelatedList oCTRL = new ctrl_RelatedList();
			
			wr_RelatedList owrRelatedList = new wr_RelatedList();
				owrRelatedList.tSObjectName 			= 'Asset';
				owrRelatedList.tMasterFieldName 		= 'AccountID';
				owrRelatedList.tMasterRecordID 			= clsTestData_Account.oMain_Account.Id;
	//			owrRelatedList.tAdditionalWherePart 	= '1=1';
				
				owrRelatedList.tSubTitle				= 'TEST SubTitle';
				owrRelatedList.tTitle					= 'TEST Title';
				owrRelatedList.tTitleNewData			= 'TEST New Data';

				owrRelatedList.bUseUserBU				= false;
				owrRelatedList.tBUGID					= clsTestData_MasterData.oMain_BusinessUnitGroup.Id;
				owrRelatedList.tBUID					= '';
				owrRelatedList.tSBUID					= '';

				owrRelatedList.tJSNewData				= 'jsTest';

				owrRelatedList.bShowBackToParent		= true;
				owrRelatedList.bShowDebug				= true;
				owrRelatedList.bShowTitle				= true;
				owrRelatedList.bShowExportButton 		= true;
				owrRelatedList.tExportType				= 'XLS';
		
			oCTRL.owrRelatedList = owrRelatedList;
				
			oCTRL.tSortBy 		= 'Name';
			oCTRL.tSortDir		= 'DESC';
			lstSObject 			= oCTRL.getRecords();
			oCTRL.filterBUData();
			oCTRL.filterData();

			Boolean bTest = oCTRL.hasNext;
			oCTRL.hasNext = bTest;
			bTest = oCTRL.hasPrevious;
			oCTRL.hasPrevious = bTest;
			Integer iTest = oCTRL.pageNumber;
			oCTRL.pageNumber = iTest;
			iTest = oCTRL.totalPages;
			oCTRL.totalPages = iTest;
			iTest = oCTRL.pageSize;
			oCTRL.pageSize = 1;
			iTest = oCTRL.totalPages;
			iTest = oCTRL.resultSize;
			oCTRL.resultSize = iTest;
			oCTRL.first();
			oCTRL.next();
			oCTRL.previous();
			oCTRL.last();
			oCTRL.searchString = 'te';
			oCTRL.doSearch();
			oCTRL.searchString = '';
			oCTRL.doSearch();
			iTest = oCTRL.totalPages;

			lstSObject 			= oCTRL.getRecords();
			oCTRL.mapFilterFieldName_Value.put('Asset_Type_Picklist__c', '3');				
			oCTRL.filterData();
			lstSObject 			= oCTRL.getRecords();
			oSObject 			= oCTRL.oNewData;
			oCTRL.sortData();
			
			clsUtil.hasException = true;
			try{
				oCTRL.tBuildQuery();
			}catch(Exception oEX){}

			try{
				oCTRL.loadData();
			}catch(Exception oEX){}
			clsUtil.hasException = false;

			tTest = oCTRL.tWarning;

			oCTRL.getExportDataURL();

			oCTRL.backToMaster();
		}

		Test.stopTest();
        //---------------------------------------
			
    }  


    @isTest static void test_ctrl_RelatedList_BU() {

		Integer iCounter = 0;
		List<sObject> lstSObject;
		sObject oSObject;


        //---------------------------------------
		// Create Test Dta    
        //---------------------------------------
		createTestData();
        //---------------------------------------

		
        //---------------------------------------
		// Perform Testing    
        //---------------------------------------
    	Test.startTest();

    	System.runAs(oUser){

			// -- TEST WITH User Business Unit -- //
			ctrl_RelatedList oCTRL = new ctrl_RelatedList();
				
			wr_RelatedList owrRelatedList = new wr_RelatedList();
				owrRelatedList.tSObjectName 			= 'Asset';
				owrRelatedList.tMasterFieldName 		= 'AccountID';
				owrRelatedList.tMasterRecordID 			= clsTestData_Account.oMain_Account.Id;
	//			owrRelatedList.tAdditionalWherePart 	= '1=1';
				
				owrRelatedList.tSubTitle				= 'TEST SubTitle';
				owrRelatedList.tTitle					= 'TEST Title';
				owrRelatedList.tTitleNewData			= 'TEST New Data';

				owrRelatedList.bUseUserBU				= true;
				owrRelatedList.tBUGID					= '';
				owrRelatedList.tBUID					= clsTestData_MasterData.oMain_BusinessUnit.Id;
				owrRelatedList.tSBUID					= '';

				owrRelatedList.tJSNewData				= 'jsTest';

				owrRelatedList.bShowBackToParent		= true;
				owrRelatedList.bShowDebug				= true;
				owrRelatedList.bShowTitle				= true;
			
			oCTRL.owrRelatedList = owrRelatedList;
				
			oCTRL.tSortBy 		= 'Name';
			oCTRL.tSortDir		= 'DESC';
			lstSObject 			= oCTRL.getRecords();

			oCTRL.filterBUData();
			oCTRL.filterData();
			lstSObject 			= oCTRL.getRecords();


			Boolean bTest = oCTRL.hasNext;
			oCTRL.hasNext = bTest;
			bTest = oCTRL.hasPrevious;
			oCTRL.hasPrevious = bTest;
			Integer iTest = oCTRL.pageNumber;
			oCTRL.pageNumber = iTest;
			iTest = oCTRL.totalPages;
			oCTRL.totalPages = iTest;
			iTest = oCTRL.pageSize;
			oCTRL.pageSize = 1;
			iTest = oCTRL.totalPages;
			iTest = oCTRL.resultSize;
			oCTRL.resultSize = iTest;
			oCTRL.first();
			oCTRL.next();
			oCTRL.previous();
			oCTRL.last();
			oCTRL.searchString = 'te';
			oCTRL.doSearch();
			oCTRL.searchString = '';
			oCTRL.doSearch();
			iTest = oCTRL.totalPages;

			oCTRL.mapFilterFieldName_Value.put('Asset_Type_Picklist__c', '3');				
			oCTRL.filterData();
			lstSObject 			= oCTRL.getRecords();
			oSObject 			= oCTRL.oNewData;
			oCTRL.bIsInitialized_Data = false;
			oSObject 			= oCTRL.oNewData;
			oCTRL.sortData();

			oCTRL.tSortBy 		= 'Name';
			oCTRL.tSortDir		= 'DESC';
			lstSObject 			= oCTRL.getRecords();

			oCTRL.tSortBy 		= 'Name';
			oCTRL.tSortDir		= 'ASC';
			lstSObject 			= oCTRL.getRecords();
			oCTRL.filterBUData();
			oCTRL.filterData();
			lstSObject 			= oCTRL.getRecords();
			oCTRL.mapFilterFieldName_Value.put('Asset_Type_Picklist__c', '3');				
			oCTRL.filterData();
			lstSObject 			= oCTRL.getRecords();
			oSObject 			= oCTRL.oNewData;
			oCTRL.sortData();

			String tTest = oCTRL.inputQueryFilter;

			oCTRL.backToMaster();

		}

		Test.stopTest();
        //---------------------------------------
			
    }    
  

    @isTest static void test_ctrl_RelatedList_SBU() {

		Integer iCounter = 0;
		List<sObject> lstSObject;
		sObject oSObject;


        //---------------------------------------
		// Create Test Dta    
        //---------------------------------------
		createTestData();
        //---------------------------------------

		
        //---------------------------------------
		// Perform Testing    
        //---------------------------------------
    	Test.startTest();

    	System.runAs(oUser){

			// -- TEST WITH Sub Business Unit -- //
			ctrl_RelatedList oCTRL = new ctrl_RelatedList();
				
			wr_RelatedList owrRelatedList = new wr_RelatedList();

				owrRelatedList.tSObjectName 			= 'Asset';
				owrRelatedList.tMasterFieldName 		= 'AccountID';
				owrRelatedList.tMasterRecordID 			= clsTestData_Account.oMain_Account.Id;
				owrRelatedList.tAdditionalWherePart 	= '';
				
				owrRelatedList.tSubTitle				= 'TEST SubTitle';
				owrRelatedList.tTitle					= 'TEST Title';
				owrRelatedList.tTitleNewData			= 'TEST New Data';

				owrRelatedList.bUseUserBU				= false;
				owrRelatedList.tBUGID					= '';
				owrRelatedList.tBUID					= '';
				owrRelatedList.tSBUID					= clsTestData_MasterData.oMain_SubBusinessUnit.Id;

				owrRelatedList.tJSNewData				= 'jsTest';

				owrRelatedList.bShowBackToParent		= true;
				owrRelatedList.bShowDebug				= true;
				owrRelatedList.bShowTitle				= true;
			
			oCTRL.owrRelatedList = owrRelatedList;
				
			oCTRL.tSortBy 		= 'Name';
			oCTRL.tSortDir		= 'ASC';
			lstSObject 			= oCTRL.getRecords();
			oCTRL.filterBUData();
			oCTRL.filterData();

			Boolean bTest = oCTRL.hasNext;
			oCTRL.hasNext = bTest;
			bTest = oCTRL.hasPrevious;
			oCTRL.hasPrevious = bTest;
			Integer iTest = oCTRL.pageNumber;
			oCTRL.pageNumber = iTest;
			iTest = oCTRL.totalPages;
			oCTRL.totalPages = iTest;
			iTest = oCTRL.pageSize;
			oCTRL.pageSize = 1;
			iTest = oCTRL.totalPages;
			iTest = oCTRL.resultSize;
			oCTRL.resultSize = iTest;
			oCTRL.first();
			oCTRL.next();
			oCTRL.previous();
			oCTRL.last();
			oCTRL.searchString = 'te';
			oCTRL.doSearch();
			oCTRL.searchString = '';
			oCTRL.doSearch();
			iTest = oCTRL.totalPages;

			lstSObject 			= oCTRL.getRecords();
			oCTRL.mapFilterFieldName_Value.put('Asset_Type_Picklist__c', '3');				
			oCTRL.filterData();
			lstSObject 			= oCTRL.getRecords();
			oSObject 			= oCTRL.oNewData;
			oCTRL.sortData();
			String tTest = oCTRL.inputQueryFilter;

			oCTRL.backToMaster();	
		}

		Test.stopTest();
        //---------------------------------------
			
    }    


    @isTest static void test_ctrl_RelatedList_BUG_BU_SBU() {

		Integer iCounter = 0;
		List<sObject> lstSObject;
		sObject oSObject;


        //---------------------------------------
		// Create Test Dta    
        //---------------------------------------
		createTestData();
        //---------------------------------------

		
        //---------------------------------------
		// Perform Testing    
        //---------------------------------------
    	Test.startTest();

    	System.runAs(oUser){

			// -- TEST WITH Business Unit Group & Business Unit & Sub Business Unit -- //
			ctrl_RelatedList oCTRL = new ctrl_RelatedList();
				
			wr_RelatedList owrRelatedList = new wr_RelatedList();

				owrRelatedList.tSObjectName 			= 'Asset';
				owrRelatedList.tMasterFieldName 		= 'AccountID';
				owrRelatedList.tMasterRecordID 			= clsTestData_Account.oMain_Account.Id;
				owrRelatedList.tAdditionalWherePart 	= '';
				
				owrRelatedList.tSubTitle				= 'TEST SubTitle';
				owrRelatedList.tTitle					= 'TEST Title';
				owrRelatedList.tTitleNewData			= 'TEST New Data';

				owrRelatedList.bUseUserBU				= false;
				owrRelatedList.tBUGID					= clsTestData_MasterData.oMain_BusinessUnitGroup.Id;
				owrRelatedList.tBUID					= clsTestData_MasterData.oMain_BusinessUnit.Id;
				owrRelatedList.tSBUID					= clsTestData_MasterData.oMain_SubBusinessUnit.Id;

				owrRelatedList.tJSNewData				= 'jsTest';

				owrRelatedList.bShowBackToParent		= true;
				owrRelatedList.bShowDebug				= true;
				owrRelatedList.bShowTitle				= true;
			
			oCTRL.owrRelatedList = owrRelatedList;
				
			oCTRL.tSortBy 		= 'Name';
			oCTRL.tSortDir		= 'ASC';
			lstSObject 			= oCTRL.getRecords();
			oCTRL.filterBUData();
			oCTRL.filterData();

			Boolean bTest = oCTRL.hasNext;
			oCTRL.hasNext = bTest;
			bTest = oCTRL.hasPrevious;
			oCTRL.hasPrevious = bTest;
			Integer iTest = oCTRL.pageNumber;
			oCTRL.pageNumber = iTest;
			iTest = oCTRL.totalPages;
			oCTRL.totalPages = iTest;
			iTest = oCTRL.pageSize;
			oCTRL.pageSize = 1;
			iTest = oCTRL.totalPages;
			iTest = oCTRL.resultSize;
			oCTRL.resultSize = iTest;
			oCTRL.first();
			oCTRL.next();
			oCTRL.previous();
			oCTRL.last();
			oCTRL.searchString = 'te';
			oCTRL.doSearch();
			oCTRL.searchString = '';
			oCTRL.doSearch();
			iTest = oCTRL.totalPages;

			lstSObject 			= oCTRL.getRecords();
			oCTRL.mapFilterFieldName_Value.put('Asset_Type_Picklist__c', '3');				
			oCTRL.filterData();
			lstSObject 			= oCTRL.getRecords();
			oSObject 			= oCTRL.oNewData;

			oCTRL.bIsInitialized_Data = false;
			oSObject 			= oCTRL.oNewData;

			oCTRL.bIsInitialized_Data = false;
			oCTRL.owrRelatedList = owrRelatedList;

			oCTRL.sortData();
			String tTest = oCTRL.inputQueryFilter;

			oCTRL.backToMaster();	
		}		
			
		Test.stopTest();
        //---------------------------------------
			
    }  

    @isTest static void test_ctrl_RelatedList_BUG_Access() {

		Integer iCounter = 0;
		List<sObject> lstSObject;
		sObject oSObject;
		

        //---------------------------------------
		// Create Test Dta    
        //---------------------------------------
		createTestData();

		Id idBU_ForLoadFieldTest;
		System.runAs(oUser_System){
	        List<User_Business_Unit__c> lstUBU = 
	        	[
	        		SELECT 
	        			Id, 
	        			Sub_Business_Unit__c, Sub_Business_Unit__r.Name
	        			, Sub_Business_Unit__r.Business_Unit__c, Sub_Business_Unit__r.Business_Unit__r.Name, Sub_Business_Unit__r.Business_Unit__r.Access_BUG__c
	    				, Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__c, Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__r.Name
	    			FROM 
	    				User_Business_Unit__c 
				];
			Map<Id, Business_Unit__c> mapBU_AccessBUG = new Map<Id, Business_Unit__c>();	
			for (User_Business_Unit__c oUBU : lstUBU){
				Business_Unit__c oBU = oUBU.Sub_Business_Unit__r.Business_Unit__r;
					oBU.Access_BUG__c = true;
				mapBU_AccessBUG.put(oBU.Id, oBU);	
			}	
	        update mapBU_AccessBUG.values();

			// Asset Support Mail
			if (oAssetSupportMail == null){            
		        oAssetSupportMail = new Asset_Support_Mail__c();
		            oAssetSupportMail.name = 'ITALY';
		            oAssetSupportMail.Email__c = 'info@medtronic.com';
		        insert oAssetSupportMail;
			}

			List<BU_Related_List_Setup__c> lstBURelatedListSetup = 
				[
					SELECT 
						Id, Name
						, Business_Unit__c, Business_Unit__r.Id, Default_Sort__c, Field_Name__c, Field_Sequence__c, Filter__c, Filter_ID_Field__c, Filter_Sequence__c, Filter_Value_Field__c
						, ID_Link_To_Detail__c, Is_BU__c, Is_SBU__c, Label__c, Master_Filter_Name__c, sObject_Name__c, UniqueField__c, Use_Business_Unit_Group__c, Visible__c
					FROM 
						BU_Related_List_Setup__c
					ORDER BY 
						Business_Unit__c
				];
			idBU_ForLoadFieldTest = lstBURelatedListSetup[0].Business_Unit__r.Id;
			for (BU_Related_List_Setup__c oBURLS : lstBURelatedListSetup){
				clsUtil.debug('oBURLS.Business_Unit__c : ' + oBURLS.Business_Unit__c);
			}
			clsUtil.debug('idBU_ForLoadFieldTest : ' + idBU_ForLoadFieldTest);
		}
        //---------------------------------------


        //---------------------------------------
		// Perform Testing    
        //---------------------------------------
    	Test.startTest();

    	System.runAs(oUser){

			// -- TEST WITH Business Unit Group & Business Unit & Sub Business Unit -- //
			ctrl_RelatedList oCTRL = new ctrl_RelatedList();
				
			wr_RelatedList owrRelatedList = new wr_RelatedList();

				owrRelatedList.tSObjectName 			= 'Asset';
				owrRelatedList.tMasterFieldName 		= 'AccountID';
				owrRelatedList.tMasterRecordID 			= clsTestData_Account.oMain_Account.Id;
				owrRelatedList.tAdditionalWherePart 	= '';
				
				owrRelatedList.tSubTitle				= 'TEST SubTitle';
				owrRelatedList.tTitle					= 'TEST Title';
				owrRelatedList.tTitleNewData			= 'TEST New Data';

				owrRelatedList.bUseUserBU				= true;
				owrRelatedList.tBUGID					= '';
				owrRelatedList.tBUID					= '';
				owrRelatedList.tSBUID					= '';

				owrRelatedList.tJSNewData				= 'jsTest';

				owrRelatedList.bShowBackToParent		= true;
				owrRelatedList.bShowDebug				= true;
				owrRelatedList.bShowTitle				= true;
			
			oCTRL.owrRelatedList = owrRelatedList;
				
			oCTRL.tSortBy 		= 'Name';
			oCTRL.tSortDir		= 'ASC';
			lstSObject 			= oCTRL.getRecords();
			oCTRL.filterBUData();
			oCTRL.filterData();
	        
			Boolean bTest = oCTRL.hasNext;
			oCTRL.hasNext = bTest;
			bTest = oCTRL.hasPrevious;
			oCTRL.hasPrevious = bTest;
			Integer iTest = oCTRL.pageNumber;
			oCTRL.pageNumber = iTest;
			iTest = oCTRL.totalPages;
			oCTRL.totalPages = iTest;
			iTest = oCTRL.pageSize;
			oCTRL.pageSize = 1;
			iTest = oCTRL.totalPages;
			iTest = oCTRL.resultSize;
			oCTRL.resultSize = iTest;
			oCTRL.first();
			oCTRL.next();
			oCTRL.previous();
			oCTRL.last();
			oCTRL.searchString = 'te';
			oCTRL.doSearch();
			oCTRL.searchString = '';
			oCTRL.doSearch();
			iTest = oCTRL.totalPages;

			lstSObject 			= oCTRL.getRecords();
			oCTRL.mapFilterFieldName_Value.put('Asset_Type_Picklist__c', '3');				
			oCTRL.filterData();
			lstSObject 			= oCTRL.getRecords();
			oSObject 			= oCTRL.oNewData;

			oCTRL.bIsInitialized_Data = false;
			oSObject 			= oCTRL.oNewData;

			oCTRL.mapFilterFieldName_Value.put('BU', idBU_ForLoadFieldTest);
			oCTRL.filterBUData();

			oCTRL.bIsInitialized_Data = false;
			oCTRL.owrRelatedList = owrRelatedList;

			oCTRL.sortData();
			String tTest = oCTRL.inputQueryFilter;

			oCTRL.backToMaster();		
		}	
			
		Test.stopTest();
        //---------------------------------------
			
    }      

}