@isTest
private class Test_ctrlExt_quickCallRecord_addContact {
	
	private static testmethod void addCallRecordContact(){
				
		Account acc = new Account();
    	acc.RecordTypeId = [select Id from RecordType where DeveloperName = 'CAN_Buying_Group' and sObjectType = 'Account' and isActive = true].Id;
    	acc.Name = 'UnitTest Account Name';
    	acc.Type = 'Buying Group';
    	acc.Account_Country_vs__c = 'CANADA';
    	
    	insert acc;
		
		Contact cnt = new Contact();
		cnt.AccountId = acc.Id;
		cnt.FirstName = 'Unit Test';
		cnt.LastName = 'Contact';		 
		cnt.Contact_Department__c = 'Cardiology';
		cnt.Contact_Primary_Specialty__c = 'Cardiology'; 
		cnt.Affiliation_To_Account__c = 'Employee';
		cnt.Primary_Job_Title_vs__c = 'Surgeon';
		cnt.Contact_Gender__c = 'Male';
		cnt.RecordTypeId = [select Id from RecordType where DeveloperName = 'Generic_Contact' and sObjectType = 'Contact' and isActive = true].Id;				
		insert cnt;
		
		Test_CallRecord_Utils.initializeCallRecordRelatedData();
		
		Call_Records__c callRecord = new Call_Records__c();
		callRecord.Business_Unit__c = [Select Id from Business_Unit__c LIMIT 1].Id;
		callRecord.Call_Date__c = Date.today();
		insert callRecord;
		
		ApexPages.standardController sc = new ApexPages.StandardController( callRecord );
		
		ctrlExt_quickCallRecord_addContact controller = new ctrlExt_quickCallRecord_addContact(sc);
		
		System.assert(controller.contactControlled == false);
		System.assert(controller.accountControlled == false);
		
		controller.callContact.Attending_Contact__c = cnt.Id;
		controller.selectedContact();
		
		System.assert(controller.contactControlled == true);
		System.assert(controller.possibleAccounts.size() == 2);
		
		controller.callContact.Attending_Contact__c = null;
		controller.selectedContact();
		System.assert(controller.contactControlled == false);
		
		controller.callContact.Attending_Affiliated_Account__c = acc.Id;
		controller.selectedAccount();
		
		System.assert(controller.accountControlled == true);
		System.assert(controller.possibleContacts.size() == 2);
		
		controller.callContact.Attending_Affiliated_Account__c = null;
		controller.selectedAccount();
		System.assert(controller.accountControlled == false);
		
		controller.callContact.Attending_Contact__c = cnt.Id;
		controller.callContact.Attending_Affiliated_Account__c = acc.Id;
		
		controller.save();
	} 
	
	private static testmethod void testValidation(){
		
		Test_CallRecord_Utils.initializeCallRecordRelatedData();
		
		Call_Records__c callRecord = new Call_Records__c();
		callRecord.Business_Unit__c = [Select Id from Business_Unit__c LIMIT 1].Id;
		callRecord.Call_Date__c = Date.today();
		insert callRecord;
		
		ApexPages.standardController sc = new ApexPages.StandardController( callRecord );
		
		ctrlExt_quickCallRecord_addContact controller = new ctrlExt_quickCallRecord_addContact(sc);
				
		controller.save();
		
		System.assert(ApexPages.getMessages().size()>0);
	}
}