global with sharing class ReleaseDeploymentComponentWrapper implements Comparable{
	public Release_Deployment_Component__c rdc;
	
	public ReleaseDeploymentComponentWrapper(Release_Deployment_Component__c rd){
		this.rdc = rd;
	}
	
	global Integer compareTo(Object compareTo) {
        
        ReleaseDeploymentComponentWrapper compareToOppy = (ReleaseDeploymentComponentWrapper)compareTo;
        string compareName = compareToOppy.rdc.Meta_data_type__c != null ? compareToOppy.rdc.Meta_data_type__c : '';
             
        if(this.rdc.Meta_data_type__c == null) return -1;
        return this.rdc.Meta_data_type__c.compareTo(CompareName);  
    }
}