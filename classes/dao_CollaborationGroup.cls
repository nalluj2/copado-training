public with sharing class dao_CollaborationGroup {
    
    /*public static List<CollaborationGroup> getCollaborationGroupByName(String groupName){
        system.debug('******'+groupName+'******');
        return  [
            select Id ,
                   Name 
            from CollaborationGroup 
            where Name = :groupName];
    }*/
    public static List<CollaborationGroup> getCollaborationGroupByNames(List<String> groupNames){
        return  [
            select Id ,
                   Name 
            from CollaborationGroup 
            where Name in :groupNames];
    }
    /*public static List<CollaborationGroup> getCollaboGroupsByUserId(String id){
        List<CollaborationGroup> collaboGroups = [
            Select (Select MemberId From GroupMembers where MemberId = :id)
            From CollaborationGroup c
        ];
        return collaboGroups;
    }*/
}