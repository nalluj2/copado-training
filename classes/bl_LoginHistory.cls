//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 09-01-2018
//  Description      : APEX Class - Business Logic for LoginHistory
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public class bl_LoginHistory {


    public static String tGetSource(wr_LoginHistory oLoginHistory){

        String tSource = '';

        if ( (oLoginHistory.LoginType != null) && (oLoginHistory.Application != null) && (oLoginHistory.Status == 'Success') ){ // Successful Login

            if (oLoginHistory.LoginType == 'Application'){ // Application Login

                if (oLoginHistory.Application == 'Browser'){ // Browser login
                    
                    tSource = 'SFDC';

                }

            }else if (oLoginHistory.LoginType == 'Partner Portal'){ // Partner Portal Login

                if (oLoginHistory.Application == 'Browser'){ // Browser login
                    
                    tSource = 'SFDC';

                }

            }else if (oLoginHistory.LoginType == 'SAML Sfdc Initiated SSO'){ // SSO Login

                if (oLoginHistory.Application == 'Browser'){ // Browser login
                    
                    tSource = 'SFDC';

                }

            }else if (oLoginHistory.LoginType == 'Chatter Communities External User'){ // Chatter Communities External User

                if (oLoginHistory.Application == 'Browser'){ // Browser login
                    
                    tSource = 'SFDC';

				}

            }else if (oLoginHistory.LoginType == 'Remote Access 2.0'){ // Remote Access 2.0 Login

                if (oLoginHistory.Application == 'Browser'){ // Browser login
                    
                    tSource = 'SFDC';

                }else if (oLoginHistory.Application == 'Salesforce Partner Community'){ // Partner Community login

                    tSource = 'SFDC';

                }else if (oLoginHistory.Application == 'Salesforce Marketing Cloud'){ // Marketing Cloud login

                    tSource = 'SFDC';
					
                }else if (oLoginHistory.Application == 'Salesforce for Android'){ // Salesforce for Android login (Mobile)

                    tSource = 'SF1';

                }else if (oLoginHistory.Application == 'Salesforce for iOS'){ // Salesforce for iOS login (Mobile)
                    
                    tSource = 'SF1';

                }else if (oLoginHistory.Application.startsWith('Salesforce1')){ // Salesforce1 login (Mobile)
                    
                    tSource = 'SF1';

                }else if (oLoginHistory.Application == 'ServiceMax'){ // ServiceMax login
                    
                    tSource = 'SFDC';

                }else if (oLoginHistory.Application == 'ServiceMax Mobile'){ // ServiceMax Mobile login
                    
                    tSource = 'SFDC';

                }

            }

        }

        return tSource;

    }

}
//--------------------------------------------------------------------------------------------------------------------