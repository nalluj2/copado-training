@isTest(seealldata=false)
private class TEST_tr_PreventActivityEmailMsgDeletion {
        
	static testMethod void myUnitTest() {

		Id id_RecordType_Case = clsUtil.getRecordTypeByDevName('Case', 'OMA_Spine_Scientific_Exchange_Product').Id;
			
		User oUser_System1 = clsTestData_User.createUser_SystemAdministrator('tstadm1', false);
		User oUser_System2 = clsTestData_User.createUser_SystemAdministrator('tstadm2', false);
		User oUser = clsTestData_User.createUser('tst001', clsUtil.getUserProfileId('US OMA Spine MedInfo'), null, false);
			oUser.Region_vs__c = 'AMERICAS';
			oUser.Sub_Region__c = 'USA';
			oUser.Country_vs__c = 'USA';
		insert new List<User>{oUser, oUser_System1, oUser_System2};

	            
		Case oCase;
		EmailMessage oEmailMessage;
		Task oTask;
		System.runAs(oUser_System1){

			OMA_Profiles__c oOMAProfile = new OMA_Profiles__c(Name = 'US OMA Spine MedInfo', Profile_Name__c = 'US OMA Spine MedInfo');
			insert new List<OMA_Profiles__c>{oOMAProfile};

			OMA_Record_Types__c oOMARecordType = new OMA_Record_Types__c(Name = 'OMA Spine - Scientific Exchange/Prod', Developer_Name__c = 'OMA_Spine_Scientific_Exchange_Product');
			insert oOMARecordType;

			List<Account> lstacc=new List<Account>();
			Account a = new Account() ; 
				a.Name = 'TestAccountOMA'; 
				a.SAP_ID__c = '04556665'; 
				a.RecordTypeId = FinalConstants.sapAccountRecordTypeId ;
			lstacc.add(a);
			Account a1 = new Account() ; 
				a1.Name = 'TestAccountOMA1'; 
				a1.SAP_ID__c = '04556250'; 
				a1.RecordTypeId = FinalConstants.sapAccountRecordTypeId ;
			lstacc.add(a1);
			insert lstacc;
			contact con=new contact(accountid=a.id,LastName='last');
				con.FirstName = 'test';
				con.Phone = '0714820303'; 
				con.Email ='test1@contact.com';
				con.Contact_Department__c = 'Diabetes Adult';
				con.Contact_Primary_Specialty__c = 'ENT';
				con.Affiliation_To_Account__c = 'Employee';
				con.Primary_Job_Title_vs__c = 'Manager';
				con.Contact_Gender__c ='Male';  
			insert con;
			system.debug('++++++++1');

			oCase = new Case(Status='New',Origin='Patient',Therapy_Picklist__c='testClassTh', OwnerId = oUser.Id);
			insert oCase;

			system.debug('++++++++2' + oCase.status);

				oCase.status = 'First Response';
				oCase.Type = 'On Label';
				oCase.RecordTypeId = id_RecordType_Case;
			update oCase;
			
        
			//Start:Manish:To Cover bl_tr_CaseTriggerActions
			List<case> lstCase=new List<Case>();
			lstCase.add(oCase);
			bl_tr_CaseTriggerActions.validateAccountContactAreC2ARelationship(lstCase);
			bl_tr_CaseTriggerActions.updateOMAFields(lstCase, null);
			oCase.accountid = a1.id;
            
			oEmailMessage = new EmailMessage();
				oEmailMessage.ParentId = oCase.Id;
			Insert oEmailMessage;
            
			oTask = new task();
				oTask.whatid = oCase.id;
				oTask.Status = 'Completed';
				oTask.Priority = 'Normal';
				oTask.Subject = 'Not Specified';
				oTask.OwnerId = oUser.id;
			insert oTask;
		}
          
        System.RunAs(oUser){           
			try{ delete oEmailMessage; }catch(Exception e){}
			try{ delete oTask; }catch(Exception e){} 

			try{ bl_tr_PreventActivityDeletion.PreventDeleteEmailMessage(new List<EmailMessage>{oEmailMessage}); }catch(Exception e){} 
        }    
    }
}