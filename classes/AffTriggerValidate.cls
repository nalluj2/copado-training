global class AffTriggerValidate {
	
	@TestVisible private static boolean blnAffAlreadyDone = false;

	public static boolean hasAlreadyDone(){ 
	    return blnAffAlreadyDone;
	}

	public static void setAlreadyDone() {		            
	   blnAffAlreadyDone = true;	
	}
}