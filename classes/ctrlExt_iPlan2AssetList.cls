/*
 *      Description : This class is the Controller Extension for the VF Page iPlan2AssetList which will display the Assets for the selected Account.
 *          It uses the VF Component RelatedList.
 *      Version  =  1.0
 *      Author   =  Bart Caelen
 *      Date     =  03/2014
 * ----------------------------------------------------------------------------------------------------------------------------------
 *      Description : CR-3776 
 *      Version  =  1.1
 *      Author   =  Bart Caelen
 *      Date     =  2014-07-31
**/
public class ctrlExt_iPlan2AssetList {

    //------------------------------------------------------------------------------------------------------------------------------------
    // PRIVATE VARIABLES
    //------------------------------------------------------------------------------------------------------------------------------------
    private Account_Plan_2__c oiPlan2;
    private boolean bUserIsAgent = false;
    private string tUser_BU = '';
    private string tUser_SBU = '';
    //------------------------------------------------------------------------------------------------------------------------------------
    

    //------------------------------------------------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //------------------------------------------------------------------------------------------------------------------------------------
    public ctrlExt_iPlan2AssetList(ApexPages.StandardController oStdController){

        if (!Test.isRunningTest()){
            List<String> lstField = new List<String>{'Account__c', 'Account__r.Name', 'Account__r.Account_Country_vs__c', 'Sub_Business_Unit__c', 'Business_Unit__c', 'Business_Unit_Group__c'};
            oStdController.addFields(lstField);
        }

        oiPlan2 = (Account_Plan_2__c)oStdController.getRecord();
        
        owrRelatedList = new wr_RelatedList();
            owrRelatedList.tSObjectName = 'Asset';
            owrRelatedList.tMasterFieldName = 'AccountID';
            owrRelatedList.tMasterRecordID = oiPlan2.Account__c;

            owrRelatedList.tTitle = 'Assets';
            owrRelatedList.tSubTitle = oiPlan2.Account__r.Name;

            owrRelatedList.tTitleNewData = 'Request new Asset';
            owrRelatedList.tJSNewData = 'jsAddNewData';

            owrRelatedList.bShowDebug = false;
            owrRelatedList.tAdditionalWherePart = '';
            
            owrRelatedList.bShowBackToParent = false;
            owrRelatedList.bShowAddNewButton = false;
            owrRelatedList.bShowTitle = false;
            owrRelatedList.bUseUserBU = false;
			owrRelatedList.bSelectPrimaryBU = false;
			owrRelatedList.tCompanyCode = 'EUR';
            
            owrRelatedList.tSBUID = clsUtil.isNull(oiPlan2.Sub_Business_Unit__c, '');
            owrRelatedList.tBUID = clsUtil.isNull(oiPlan2.Business_Unit__c, '');
            owrRelatedList.tBUGID = clsUtil.isNull(oiPlan2.Business_Unit_Group__c, '');
        
    }
    //------------------------------------------------------------------------------------------------------------------------------------


    //------------------------------------------------------------------------------------------------------------------------------------
    // GETTERS & SETTERS
    //------------------------------------------------------------------------------------------------------------------------------------
    public wr_RelatedList owrRelatedList    { get;set; }
    //------------------------------------------------------------------------------------------------------------------------------------


    //------------------------------------------------------------------------------------------------------------------------------------
    // ACTIONS
    //------------------------------------------------------------------------------------------------------------------------------------
    public PageReference createNewData(){

        PageReference oPageReference;

            oPageReference = new PageReference('/apex/CreateAssetRequest?id=' + oiPlan2.Id);
            oPageReference.setRedirect(true);

        return oPageReference;

    }
    //------------------------------------------------------------------------------------------------------------------------------------

}
//------------------------------------------------------------------------------------------------------------------------------------