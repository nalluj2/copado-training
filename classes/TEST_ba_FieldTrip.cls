/**
 * Created by dijkea2 on 26-9-2016.
 */

@IsTest
private with sharing class TEST_ba_FieldTrip {

    @testSetup
    static void createTestData() {

        //Insert Accounts
        List<Account> accounts = new List<Account>{} ;
        for (Integer i = 0 ; i < 5 ; i++) {
            Account a = new Account() ;
            a.Name = 'Test Account Name ' + i  ;
            a.RecordTypeId = FinalConstants.sapAccountRecordTypeId ;
            a.Account_Country_vs__c='BELGIUM';
            a.CurrencyIsoCode = 'EUR';
            accounts.add(a);
        }
        insert accounts;
    }

    static testMethod void test_batch() {

		Test.startTest();
		                
        ba_FieldTrip handler = new ba_FieldTrip('Account');
                
        Database.executeBatch(handler, 500);

        Test.stopTest();
    }
    
    static testMethod void test_batch_codeApexClass() {
		
		Test.startTest();
		
		Set<String> searchSet = new Set<String>{'Account_Country_vs__c', 'Vascular__c', 'Pumps__c'};	
		List<String> batchList = new List<String>{'ApexClass'};
		
       	ba_FieldTrip_CodeSearch batch = new ba_FieldTrip_CodeSearch(batchList, searchSet, null, 'Account');
    			
        Database.executeBatch(batch, 1000);

        Test.stopTest();
    }
    
    static testMethod void test_batch_codeApexPage() {
		
		Test.startTest();
		
		Set<String> searchSet = new Set<String>{'Account_Country_vs__c', 'Vascular__c', 'Pumps__c'};	
		List<String> batchList = new List<String>{'ApexPage'};
		
       	ba_FieldTrip_CodeSearch batch = new ba_FieldTrip_CodeSearch(batchList, searchSet, null, 'Account');
    	
        Database.executeBatch(batch, 1000);

        Test.stopTest();
    }
    
    static testMethod void test_batch_codeApexTrigger() {
		
		Test.startTest();
		
		Set<String> searchSet = new Set<String>{'Account_Country_vs__c', 'Vascular__c', 'Pumps__c'};	
		List<String> batchList = new List<String>{'ApexTrigger'};
		
       	ba_FieldTrip_CodeSearch batch = new ba_FieldTrip_CodeSearch(batchList, searchSet, null, 'Account');
    	
        Database.executeBatch(batch, 1000);

        Test.stopTest();
    }
    
    static testMethod void test_batch_codeApexComponent() {
		
		Test.startTest();
		
		Set<String> searchSet = new Set<String>{'Account_Country_vs__c', 'Vascular__c', 'Pumps__c'};	
		List<String> batchList = new List<String>{'ApexComponent'};
		
       	ba_FieldTrip_CodeSearch batch = new ba_FieldTrip_CodeSearch(batchList, searchSet, null, 'Account');
    	
        Database.executeBatch(batch, 500);

        Test.stopTest();
    }
}