@isTest
private class Test_bl_Contract_Repository {
    
    @TestSetup
    private static void createContract(){
        
        Territory2 testTerritory = [Select Id, ParentTerritory2Id, Therapy_Groups_Text__c from Territory2 where Territory2Type.DeveloperName = 'Territory' AND ParentTerritory2.Territory2Type.DeveloperName = 'District' AND Country_UID__c = 'NL' AND Therapy_Groups_Text__c != null AND Business_Unit__c = 'CRHF' LIMIT 1];
                
        List<User> testUsers;
        
        System.runAs(new User(Id = UserInfo.getUserId())){
            
            testUsers = createTestUsers(testTerritory);
        }
            
        Account testAcc = new Account();
        testAcc.Name = 'Test Account';
        testAcc.Account_Country_vs__c = 'BELGIUM';
        insert testAcc;
        
        ObjectTerritory2Association accTerritory = new ObjectTerritory2Association();
        accTerritory.Territory2Id = testTerritory.Id;
        accTerritory.ObjectId = testAcc.Id;
        accTerritory.AssociationCause = 'Territory2Manual'; 
        insert accTerritory;    
        
        createMasterData(testTerritory);
                                
        Contract_Repository_Setting__c beSettings = new Contract_Repository_Setting__c();
        beSettings.Country__c = [Select Id from DIB_Country__c where Name = 'BELGIUM'].Id;
        beSettings.Contract_Type__c = 'Rebate agreement';
        beSettings.Default_Access_Level__c = 'Read Only - No Access to Documents';
        beSettings.Default_Access_Level_DM__c = 'Read Only - Access to Documents';
        beSettings.Default_Email_Reminder_Sales_Rep__c = '1';
        beSettings.Default_Email_Reminder_DM__c = '2';
        insert beSettings;
        
        Contract_Repository__c contract = new Contract_Repository__c();
        contract.Account__c = testAcc.Id;
        contract.Contract_nr__c = '11111111';
        contract.Primary_Contract_Type__c = 'Rebate agreement';
        contract.Rebate_Type__c = 'Straight Unit';
        contract.MPG_Code__c = 'A1';
        //contract.Payment_Options__c = 'Credit Note';
        contract.put('Payment_Options__c','Credit Note');
        contract.Pre_Payment_Terms__c = 'Final Payment';
        contract.Payment_Period__c = 'Monthly';     
        contract.Original_Valid_From_Date__c = Date.today();
        contract.Original_Valid_To_Date__c = Date.today().addYears(1);      
        insert contract;
    } 
    
    private static testmethod void testAutoTeamMembers(){
        
        Contract_Repository__c contract = [Select Id from Contract_Repository__c where Contract_nr__c = '11111111'];
        
        List<Contract_Repository_Team__c> contractTeam = [Select Id from Contract_Repository_Team__c where Contract_Repository__c = :contract.Id];
        
        System.assert(contractTeam.size() >= 3);
        
        Contract_Repository_Team__c salesRep = [Select Id, Access_Level__c, Auto_Aligned_with_Territory__c, Email_Reminder__c, Contract_Repository_Owner__c from Contract_Repository_Team__c where Contract_Repository__c = :contract.Id AND Team_Member__r.Alias = 'uniTTSR'];
        System.assert(salesRep.Access_Level__c == 'Read Only - No Access to Documents');
        System.assert(salesRep.Email_Reminder__c == '1');
        System.assert(salesRep.Auto_Aligned_with_Territory__c == true);
        System.assert(salesRep.Contract_Repository_Owner__c == false);
        
        Contract_Repository_Team__c dm = [Select Id, Access_Level__c, Auto_Aligned_with_Territory__c, Email_Reminder__c, Contract_Repository_Owner__c from Contract_Repository_Team__c where Contract_Repository__c = :contract.Id AND Team_Member__r.Alias = 'uniTTDM'];
        System.assert(dm.Access_Level__c == 'Read Only - Access to Documents');
        System.assert(dm.Email_Reminder__c == '2');
        System.assert(dm.Auto_Aligned_with_Territory__c == true);
        System.assert(dm.Contract_Repository_Owner__c == false);
    }    
    
    private static testmethod void testBatchAutoTeamMembers(){
        
        Test.startTest();
        
        User salesRep = [Select ID from User where Alias = 'uniTTSR'];
        
        salesRep.IsActive = false;
        salesRep.User_Status__c = 'Left Company';
        salesRep.Deactivation_Date__c = Date.today();
        update salesRep;
        
        ba_Contract_Repository_Auto_Team batch = new ba_Contract_Repository_Auto_Team();
        batch.execute(null);
        
        Test.stopTest();
        
        Contract_Repository__c contract = [Select Id from Contract_Repository__c where Contract_nr__c = '11111111'];
        
        List<Contract_Repository_Team__c> salesRepTeam = [Select Id, Access_Level__c, Auto_Aligned_with_Territory__c, Email_Reminder__c, Contract_Repository_Owner__c from Contract_Repository_Team__c where Contract_Repository__c = :contract.Id AND Team_Member__r.Alias = 'uniTTSR'];
        System.assert(salesRepTeam.size() == 0);        
    }
    
    private static testmethod void testButtonAutoTeamMembers(){
        
        User salesRep = [Select ID from User where Alias = 'uniTTSR'];
        
        System.runAs(new User(Id = UserInfo.getUserId())){
        
            salesRep.IsActive = false;
            salesRep.User_Status__c = 'Left Company';
            salesRep.Deactivation_Date__c = Date.today();
            update salesRep;
        }
        
        Contract_Repository__c contract = [Select Id from Contract_Repository__c where Contract_nr__c = '11111111'];
        
        bl_ContractRepository.createDefaultContractTeam(contract.Id);
        
        List<Contract_Repository_Team__c> salesRepTeam = [Select Id, Access_Level__c, Auto_Aligned_with_Territory__c, Email_Reminder__c, Contract_Repository_Owner__c from Contract_Repository_Team__c where Contract_Repository__c = :contract.Id AND Team_Member__r.Alias = 'uniTTSR'];
        System.assert(salesRepTeam.size() == 0);        
    }
    
    private static testmethod void testBlockModifyTeamMembers(){
        
        User salesRep;
                
        System.runAs(new USer(Id = UserInfo.getUserId())){
            
            salesRep = Test_Contract_Repository_Bundle.createTestUser();
        }
        
        Contract_Repository__c contract = [Select Id from Contract_Repository__c where Contract_nr__c = '11111111'];
        
        Contract_Repository_Team__c userTeamMember = new Contract_Repository_Team__c();
        userTeamMember.Team_Member__c = salesRep.Id;
        userTeamMember.Contract_Repository__c = contract.Id;
        userTeamMember.Access_Level__c = 'Read/Write';
        insert userTeamMember;
           
        System.runAs(salesRep){
            
            Contract_Repository_Team__c salesRepTeamMember = [Select Id from Contract_Repository_Team__c where Contract_Repository__c = :contract.Id AND Team_Member__r.Alias = 'uniTTSR'];
            
            Boolean failed = false;
            
            try{
                
                salesRepTeamMember.Auto_Aligned_with_Territory__c = false;
                update salesRepTeamMember;
                
            }catch(Exception e){
                
                failed = true;
                System.assert(e.getMessage().contains('Automatically added Team members cannot be modified or deleted'));
            }
            
            System.assert(failed == true);
            
            failed = false;
            
            try{
                                
                delete salesRepTeamMember;
                
            }catch(Exception e){
                
                failed = true;
                System.assert(e.getMessage().contains('Automatically added Team members cannot be modified or deleted'));
            }
            
            System.assert(failed == true);          
        }
    }
    
    private static List<User> createTestUsers(Territory2 territory){
        
        List<User> testUsers = new List<User>();
        
        Profile cvgSR = [Select Id from Profile where Name = 'EUR Field Force CVG'];
        
        User testSalesRep = new User();     
        testSalesRep.FirstName = 'Unit Test';
        testSalesRep.LastName = 'Sales Rep';
        testSalesRep.ProfileId = cvgSR.Id;
        testSalesRep.CommunityNickname = 'uniTTSR';
        testSalesRep.Alias_unique__c = 'uniTTSR';
        testSalesRep.Username = 'unit.test.sales.rep@medtronic.com.unittest'; 
        testSalesRep.Email = 'unit.test.sales.rep@medtronic.com';
        testSalesRep.Alias = 'uniTTSR';
        testSalesRep.TimeZoneSidKey = 'Europe/Amsterdam'; 
        testSalesRep.LocaleSidKey = 'en_US';
        testSalesRep.EmailEncodingKey =  'ISO-8859-1';
        testSalesRep.LanguageLocaleKey = 'en_US';
        testSalesRep.CostCenter__c = '123456';
        testSalesRep.Company_Code_Text__c = 'EUR';
        testSalesRep.User_Business_Unit_vs__c = 'CRHF';
        testSalesRep.Job_Title_vs__c = 'Sales Rep';
        testUsers.add(testSalesRep);
        
        User testDM = new User();       
        testDM.FirstName = 'Unit Test';
        testDM.LastName = 'DM';
        testDM.ProfileId = cvgSR.Id;
        testDM.CommunityNickname = 'uniTTDM';
        testDM.Alias_unique__c = 'uniTTDM';
        testDM.Username = 'unit.test.dm@medtronic.com.unittest'; 
        testDM.Email = 'unit.test.dm@medtronic.com';
        testDM.Alias = 'uniTTDM';
        testDM.TimeZoneSidKey = 'Europe/Amsterdam'; 
        testDM.LocaleSidKey = 'en_US';
        testDM.EmailEncodingKey =  'ISO-8859-1';
        testDM.LanguageLocaleKey = 'en_US';
        testDM.CostCenter__c = '123456';
        testDM.Company_Code_Text__c = 'EUR';
        testDM.User_Business_Unit_vs__c = 'CRHF';
        testDM.Job_Title_vs__c = 'District Manager';
        testUsers.add(testDM);
        
        insert testUsers;
        
        UserTerritory2Association srTerritory = new UserTerritory2Association();
        srTerritory.Territory2Id = territory.Id;
        srTerritory.UserId = testSalesRep.Id;
        
        UserTerritory2Association dmTerritory = new UserTerritory2Association();
        dmTerritory.Territory2Id = territory.ParentTerritory2Id;
        dmTerritory.UserId = testDM.Id;
        
        insert new List<UserTerritory2Association>{srTerritory, dmTerritory};
        
        return testUsers;
    }
    
    private static void createMasterData(Territory2 territory){
        
        Company__c cmpny = new Company__c();
        cmpny.name='TestMedtronic';
        cmpny.CurrencyIsoCode = 'EUR';
        cmpny.Current_day_in_Q1__c=56;
        cmpny.Current_day_in_Q2__c=34; 
        cmpny.Current_day_in_Q3__c=5; 
        cmpny.Current_day_in_Q4__c= 0;   
        cmpny.Current_day_in_year__c =200;
        cmpny.Days_in_Q1__c=56;  
        cmpny.Days_in_Q2__c=34;
        cmpny.Days_in_Q3__c=13;
        cmpny.Days_in_Q4__c=22;
        cmpny.Days_in_year__c =250;
        cmpny.Company_Code_Text__c ='TST';
        insert cmpny;        

        //Insert Business Unit
        Business_Unit__c bu =  new Business_Unit__c();
        bu.Company__c =cmpny.id;
        bu.name='BUMedtronic';
        insert bu;
        
        //Insert SBU
        Sub_Business_Units__c sbu = new Sub_Business_Units__c();
        sbu.name='SBUMedtronic1';
        sbu.Business_Unit__c = bu.id;
        sbu.Account_Plan_Default__c='Revenue';
        sbu.Account_Flag__c = 'AF_Solutions__c';
        sbu.Contact_Flag__c = 'Coro_PV__c';
        insert sbu;
       
        //Insert Therapy Group
        Therapy_Group__c tg = new Therapy_Group__c();
        tg.Name= territory.Therapy_Groups_Text__c.split(';')[0];
        tg.Company__c = cmpny.Id;
        tg.Sub_Business_Unit__c = sbu.Id;
        insert tg;
  
        //Insert Therapy
        Therapy__c tp = new Therapy__c();
        tp.Name='Therapy';
        tp.Therapy_Group__c = tg.id;
        tp.Business_Unit__c=bu.id;
        tp.Sub_Business_Unit__c=sbu.id;
        tp.Therapy_Name_Hidden__c = 'test';      
        insert tp;
        
        //Insert Product Group
        Product_Group__c pg = new Product_Group__c();
        pg.Name = 'Product Group';
        pg.Therapy_ID__c = tp.id;
        insert pg;
        
        DIB_Country__c oCountry = new DIB_Country__c();
        oCountry.Acc_Perf_Relevant__c = true;
        oCountry.Call_Record_Chatter_Post_on_Contact__c = true;     
        oCountry.Country_ISO_Code__c = 'BE';                            
        oCountry.CurrencyIsoCode = 'EUR';                               
        oCountry.Exclude_From_Batch_Code_Checkbox__c = true;        
        oCountry.Name = 'BELGIUM';                  
        oCountry.T_Country__c = 'Belgium';                              
        oCountry.Company__c = cmpny.Id;             
        oCountry.Follow_Task__c = true;                         
        oCountry.Country_ISO_Code_3__c = 'BEL';
        insert oCountry;
        
        Product2 prod = new Product2();
        prod.Name = 'Test Product';
        prod.Business_Unit_ID__c = bu.Id;
        prod.Product_Group__c = pg.Id;
        prod.MPG_Code_Text__c = 'A1';       
        prod.RecordTypeId = [Select Id from RecordType where SObjectType = 'Product2' AND DeveloperName = 'SAP_Product'].Id;
        insert prod;
    }
}