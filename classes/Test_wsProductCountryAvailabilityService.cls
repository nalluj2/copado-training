@isTest
private without sharing class Test_wsProductCountryAvailabilityService {
    
    @TestSetup
    private static void generateData(){
    	
    	RecordType prodRT = [Select Id from RecordType where SobjectType = 'Product2' AND DeveloperName = 'SAP_Product'];
    	
    	Product2 testProd1 = new Product2();
    	testProd1.Name = 'Test Product 1';
    	testProd1.RecordTypeId = prodRT.Id;
    	
    	Product2 testProd2 = new Product2();
    	testProd2.Name = 'Test Product 2';
    	testProd2.KitableIndex__c = 'Kitable';
    	testProd2.RecordTypeId = prodRT.Id;
    	
    	insert new List<Product2>{testProd1, testProd2};
    	
    	DIB_Country__c netherlands = new DIB_Country__C();
    	netherlands.Name = 'NETHERLANDS';
    	netherlands.Country_ISO_Code__c = 'NL';
    	netherlands.Box_Builder_Enabled__c = true;
    	
    	DIB_Country__c belgium = new DIB_Country__C();
    	belgium.Name = 'BELGIUM';
    	belgium.Country_ISO_Code__c = 'BE';
    	belgium.Box_Builder_Enabled__c = true;
    	
    	DIB_Country__c germany = new DIB_Country__C();
    	germany.Name = 'GERMANY';
    	germany.Country_ISO_Code__c = 'DE';
    	germany.Box_Builder_Enabled__c = true;
    	
    	insert new List<DIB_Country__c>{netherlands, belgium, germany};
    	
    	Product_Country_Availability__c prod2BE = new Product_Country_AVailability__c();
    	prod2BE.Product__c = testPRod2.Id;
    	prod2BE.Country__c = belgium.Id;
    	prod2BE.Status__c = 'Available';
    	
    	Product_Country_Availability__c prod2NL = new Product_Country_AVailability__c();
    	prod2NL.Product__c = testPRod2.Id;
    	prod2NL.Country__c = netherlands.Id;
    	prod2NL.Status__c = 'Not Approved';
    	
    	Product_Country_Availability__c prod2DE = new Product_Country_AVailability__c();
    	prod2dE.Product__c = testPRod2.Id;
    	prod2DE.Country__c = germany.Id;
    	prod2DE.Status__c = 'Request Rejected';
    	prod2DE.Rejected_Reason__c = 'Test rejection';
    	insert new List<Product_Country_Availability__c>{prod2BE, prod2NL, prod2DE};
    }
    
    private static testmethod void test_RequestNoneKitable(){
    	
    	Product2 prod1 = [Select Id from Product2 where Name = 'Test Product 1'];
    	DIB_Country__c netherlands = [Select Id from DIB_Country__c where Name = 'NETHERLANDS'];
    	
    	RestRequest req = new RestRequest();
        
        req.requestURI = '/services/apexrest/BoxBuilderProduct2Service';
        req.httpMethod = 'POST';
        
        Product_Country_Availability__c prod1NL = new Product_Country_Availability__c();
        prod1NL.Product__c = prod1.Id;
        prod1NL.Country__c = netherlands.Id;
        
        ws_ProductCountryAvailabilityService.ProdCountryAvailabilityRequest request = new ws_ProductCountryAvailabilityService.ProdCountryAvailabilityRequest();
        request.productCountryAvailability = prod1NL;
        
        req.requestbody = Blob.valueOf(JSON.serializePretty(request));
        
        RestContext.request = req;
        RestContext.response = new RestResponse();
    	
    	String resp = ws_ProductCountryAvailabilityService.doPost();
    	
    	ws_ProductCountryAvailabilityService.ProdCountryAvailabilityResponse response = (ws_ProductCountryAvailabilityService.ProdCountryAvailabilityResponse) JSON.deserialize(resp, ws_ProductCountryAvailabilityService.ProdCountryAvailabilityResponse.class);
    	
    	System.assert(response.success == true);
    	System.assert(response.productCountryAvailability.Id != null);
    	
    	prod1 = [Select Id, KitableIndex__c from Product2 where Id = :prod1.Id];    	
    	System.assert(prod1.KitableIndex__c == 'Approval Pending');
    	
    	prod1NL = [Select Id, Status__c from Product_Country_Availability__c where Product__c = :prod1.Id AND Country__c = :netherlands.Id];    	
    	System.assert(prod1NL.status__c == 'Approval Pending');
    }
    
    private static testmethod void test_RequestKitable(){
    	
    	Product2 prod2 = [Select Id from Product2 where Name = 'Test Product 2'];
    	DIB_Country__c netherlands = [Select Id from DIB_Country__c where Name = 'NETHERLANDS'];
    	
    	RestRequest req = new RestRequest();
        
        req.requestURI = '/services/apexrest/BoxBuilderProduct2Service';
        req.httpMethod = 'POST';
        
        Product_Country_Availability__c prod2NL = new Product_Country_Availability__c();
        prod2NL.Product__c = prod2.Id;
        prod2NL.Country__c = netherlands.Id;
        
        ws_ProductCountryAvailabilityService.ProdCountryAvailabilityRequest request = new ws_ProductCountryAvailabilityService.ProdCountryAvailabilityRequest();
        request.productCountryAvailability = prod2NL;
        
        req.requestbody = Blob.valueOf(JSON.serializePretty(request));
        
        RestContext.request = req;
        RestContext.response = new RestResponse();
    	
    	String resp = ws_ProductCountryAvailabilityService.doPost();
    	
    	ws_ProductCountryAvailabilityService.ProdCountryAvailabilityResponse response = (ws_ProductCountryAvailabilityService.ProdCountryAvailabilityResponse) JSON.deserialize(resp, ws_ProductCountryAvailabilityService.ProdCountryAvailabilityResponse.class);
    	
    	System.assert(response.success == true);
    	System.assert(response.productCountryAvailability.Id != null);
    	
    	prod2 = [Select Id, KitableIndex__c from Product2 where Id = :prod2.Id];    	
    	System.assert(prod2.KitableIndex__c == 'Kitable');
    	
    	prod2NL = [Select Id, Status__c from Product_Country_Availability__c where Product__c = :prod2.Id AND Country__c = :netherlands.Id];    	
    	System.assert(prod2NL.status__c == 'Approval Pending');
    }
    
    private static testmethod void test_RequestKitable2(){
    	
    	Product2 prod2 = [Select Id from Product2 where Name = 'Test Product 2'];
    	DIB_Country__c belgium = [Select Id from DIB_Country__c where Name = 'BELGIUM'];
    	
    	RestRequest req = new RestRequest();
        
        req.requestURI = '/services/apexrest/BoxBuilderProduct2Service';
        req.httpMethod = 'POST';
        
        Product_Country_Availability__c prod2BE = new Product_Country_Availability__c();
        prod2BE.Product__c = prod2.Id;
        prod2BE.Country__c = belgium.Id;
        
        ws_ProductCountryAvailabilityService.ProdCountryAvailabilityRequest request = new ws_ProductCountryAvailabilityService.ProdCountryAvailabilityRequest();
        request.productCountryAvailability = prod2BE;
        
        req.requestbody = Blob.valueOf(JSON.serializePretty(request));
        
        RestContext.request = req;
        RestContext.response = new RestResponse();
    	
    	String resp = ws_ProductCountryAvailabilityService.doPost();
    	
    	ws_ProductCountryAvailabilityService.ProdCountryAvailabilityResponse response = (ws_ProductCountryAvailabilityService.ProdCountryAvailabilityResponse) JSON.deserialize(resp, ws_ProductCountryAvailabilityService.ProdCountryAvailabilityResponse.class);
    	
    	System.assert(response.success == true);
    	System.assert(response.productCountryAvailability.Id != null);
    	
    	prod2 = [Select Id, KitableIndex__c from Product2 where Id = :prod2.Id];    	
    	System.assert(prod2.KitableIndex__c == 'Kitable');
    	
    	prod2BE = [Select Id, Status__c from Product_Country_Availability__c where Product__c = :prod2.Id AND Country__c = :belgium.Id];    	
    	System.assert(prod2BE.status__c == 'Available');
    }
    
    private static testmethod void test_RequestRejected(){
    	
    	Product2 prod2 = [Select Id from Product2 where Name = 'Test Product 2'];
    	DIB_Country__c germany = [Select Id from DIB_Country__c where Name = 'GERMANY'];
    	
    	RestRequest req = new RestRequest();
        
        req.requestURI = '/services/apexrest/BoxBuilderProduct2Service';
        req.httpMethod = 'POST';
        
        Product_Country_Availability__c prod2DE = new Product_Country_Availability__c();
        prod2DE.Product__c = prod2.Id;
        prod2DE.Country__c = germany.Id;
        
        ws_ProductCountryAvailabilityService.ProdCountryAvailabilityRequest request = new ws_ProductCountryAvailabilityService.ProdCountryAvailabilityRequest();
        request.productCountryAvailability = prod2DE;
        
        req.requestbody = Blob.valueOf(JSON.serializePretty(request));
        
        RestContext.request = req;
        RestContext.response = new RestResponse();
    	
    	String resp = ws_ProductCountryAvailabilityService.doPost();
    	
    	ws_ProductCountryAvailabilityService.ProdCountryAvailabilityResponse response = (ws_ProductCountryAvailabilityService.ProdCountryAvailabilityResponse) JSON.deserialize(resp, ws_ProductCountryAvailabilityService.ProdCountryAvailabilityResponse.class);
    	
    	System.assert(response.success == false);
    	    	
    	prod2 = [Select Id, KitableIndex__c from Product2 where Id = :prod2.Id];    	
    	System.assert(prod2.KitableIndex__c == 'Kitable');
    	
    	prod2DE = [Select Id, Status__c from Product_Country_Availability__c where Product__c = :prod2.Id AND Country__c = :germany.Id];    	
    	System.assert(prod2DE.status__c == 'Request Rejected');
    }
}