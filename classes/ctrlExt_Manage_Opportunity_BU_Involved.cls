public with sharing class ctrlExt_Manage_Opportunity_BU_Involved {
    
    private Opportunity opp {get; set;}
    public List<BUInvolved> oppBUs {get; set;}
    private Integer tempCounter = 0;
    public Boolean isBUDetailsMandatory {get; set;}
    public String columnsWidth {get; set;}
	public Boolean bShowAnnualPullThroughRevenue { get;set; }
    
    private List<String> buList {get; set;}
    private Map<String, List<String>> sbuMapList {get; set;}

	private Set<String> setStage_ShowAnnualPullThroughRevenue = new Set<String>{'2 - Proposing','3a - Pre-Contracting','3b - Won (Pending Contract Signature)','4 - Closed won','4a - Closed Won (Service Activation)','4b - Closed Won (Service Delivery)','4c - Closed Won (Contract delivered)','5a - Closed Lost','5b - Closed Amended','6 - On hold'};
        
    public ctrlExt_Manage_Opportunity_BU_Involved(ApexPages.StandardController sc){
    	
    	Id oppId = sc.getId();
    	
    	if(oppId == null){
    	
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'No context Opportunity found'));
    		return;
    	}
    	
    	buList = new List<String>();
    	sbuMapList = new Map<String, List<String>>();
		bShowAnnualPullThroughRevenue = false;
    	
    	for(Business_Unit_Group__c bug : [Select Id, Abbreviated_Name__c, (Select Id, Name from Business_Units__r ORDER BY Name) from Business_Unit_Group__c where Master_Data__r.Name = 'Europe' AND Name != 'IHS' ORDER BY Abbreviated_Name__c]){
    		buList.add(bug.Abbreviated_Name__c);
    		
    		List<String> sbuList = new List<String>();
    		
    		for(Business_Unit__c bu : bug.Business_Units__r){
    			sbuList.add(bu.Name);
    		}
    		
    		sbuMapList.put(bug.Abbreviated_Name__c, sbuList);
    	}
    	
    	opp = [Select Id, RecordType.DeveloperName, StageName from Opportunity where Id = :oppId];
    	
    	if(opp.RecordType.DeveloperName == 'EMEA_IHS_Managed_Services' || opp.RecordType.DeveloperName == 'EMEA_IHS_Consulting_Opportunity' ){
			
			isBUDetailsMandatory = true;
			if (setStage_ShowAnnualPullThroughRevenue.contains(opp.StageName)) bShowAnnualPullThroughRevenue = true;

    	}else{
		
			isBUDetailsMandatory = false;

		}
    	
    	if(isBUDetailsMandatory) columnsWidth = '50px,100px,100px';
    	else columnsWidth = '50px,100px';
    	   	    	
    	oppBUs = new List<BUInvolved>();
    	
    	Map<String, Opportunity_BU_Involved__c> existingBUs = new Map<String, Opportunity_BU_Involved__c>();
    	Map<String, Opportunity_sBU_Involved__c> existingsBUs = new Map<String, Opportunity_sBU_Involved__c>(); 
    	
    	for(Opportunity_BU_Involved__c oppBUInvolved : [Select Id, BU_Involved__c, BU_Percentage__c, Annual_Pull_Through_Revenue__c, Unique_Key__c from Opportunity_BU_Involved__c where Opportunity__c = :oppId]){
    														
    		existingBUs.put(oppBUInvolved.BU_Involved__c, oppBUInvolved); 														
		}
		
		for(Opportunity_sBU_Involved__c oppsBUInvolved : [Select Id, Name, Percentage__c, Unique_Key__c, Opportunity_BU_Involved__c	from Opportunity_sBU_Involved__c where Opportunity_BU_Involved__r.Opportunity__c = :oppId]){
    	 													 
			existingsBUs.put(oppsBUInvolved.Name, oppsBUInvolved);
		}
		
    	for(String bu : buList){
    		
    		BUInvolved buInvolved = new BUInvolved();
    		
    		if(existingBUs.get(bu) != null){
    			
    			buInvolved.record = existingBUs.get(bu);
    			buInvolved.selected = true; 
    			
    		}else{
    			
    			buInvolved.record = new Opportunity_BU_Involved__c();
    			buInvolved.record.Opportunity__c = opp.Id;
    			buInvolved.record.BU_Involved__c = bu;
    			buInvolved.record.Unique_Key__c = opp.Id + ':' + bu;    			    			
    		}
    		
    		for(String sbu : sbuMapList.get(bu)){
    			
    			sBUInvolved sbuInvolved = new sBUInvolved();
    		
	    		if(existingsBUs.get(sbu) != null){
	    			
	    			sbuInvolved.record = existingsBUs.get(sbu);
	    			sbuInvolved.selected = true; 
	    			
	    		}else{
	    			
	    			sbuInvolved.record = new Opportunity_sBU_Involved__c();
	    			sbuInvolved.record.Name = sbu;
	    			sbuInvolved.record.Unique_Key__c = opp.Id + ':' + sbu;    			    			
	    		}
	    		
	    		buInvolved.sBUInvolved.add(sbuInvolved);
    		}
    		
    		oppBUs.add(buInvolved);
    	}    	
    }
       
    public String selectedBU {get; set;}
    
    public void selectBU(){
    	
    	for(BUInvolved oppBU : oppBUs){
    		
    		if(oppBU.record.BU_Involved__c == selectedBU){
    			
    			oppBU.selected = true;	
    			break;
    		}    		
    	}	
    }
    
    public void unselectBU(){
    	
    	for(BUInvolved oppBU : oppBUs){
    		
    		if(oppBU.record.BU_Involved__c == selectedBU){
    		
    			for(sBUInvolved oppSBU : oppBU.sBUInvolved){
    				
    				oppSBU.selected = false;
    				oppSBU.record.Percentage__c = null;
    			}
    			
    			oppBU.record.BU_Percentage__c = null;
				oppBU.record.Annual_Pull_Through_Revenue__c = null;
    			
    			break;
    		}    		
    	}	
    }
       
    public PageReference save(){
    	
    	System.SavePoint sp = Database.setSavepoint();
    	
    	try{
    		
    		Decimal oppTotal = 0;
    		
    		List<Opportunity_BU_Involved__c> listBUInvolved = new List<Opportunity_BU_Involved__c>();
    		Boolean mandatoryFilled = true;
    		    		
	    	for(BUInvolved oppBU : oppBUs){
	    			    			    		
	    		if(oppBU.selected == true){

					if (bShowAnnualPullThroughRevenue){

						if (oppBU.record.Annual_Pull_Through_Revenue__c == null){

	    					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, oppBU.record.BU_Involved__c + ' :  Annual Pull Through Revenue value is mandatory'));
	    					mandatoryFilled = false;

						}

					}
	    			
	    			if(isBUDetailsMandatory){
	    				
	    				if(oppBU.record.BU_Percentage__c == null){
	    					
	    					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, oppBU.record.BU_Involved__c + ' : percentage value is mandatory'));
	    					mandatoryFilled = false;
	    					
	    				}else oppTotal += oppBU.record.BU_Percentage__c;
	    				
	    				Decimal buTotal = 0;
	    				Integer withPercentage = 0;
	    				Integer withoutPercentage = 0;
	    				
	    				for(sBUInvolved oppSBU : oppBU.sBUInvolved){
	    					
	    					if(oppSBU.selected == true){
	    						
	    						if(oppSBU.record.Percentage__c != null){
	    							
	    							buTotal += oppSBU.record.Percentage__c;
	    							withPercentage++;
	    							
	    						}else{
	    							
	    							withoutPercentage++;
	    						}
	    					}    					
	    				}
	    				
	    				if(withPercentage == 0 && withoutPercentage == 0){
	    					
	    					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, oppBU.record.BU_Involved__c + ' : At least one Sub-Business Unit must be selected'));
	    					mandatoryFilled = false;
	    					
	    				}else if(withPercentage > 0 && withoutPercentage > 0){
	    					
	    					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, oppBU.record.BU_Involved__c + ' : If at least one of the Sub-Business Units has a percentage value, all the other Sub-Business Units of the same Business Unit must have a percentage value too'));	    					
	    					mandatoryFilled = false;
	    				
	    				}else if(withPercentage > 0 && oppBU.record.BU_Percentage__c != null && oppBU.record.BU_Percentage__c != buTotal){
	    					
	    					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, oppBU.record.BU_Involved__c + ' : The sum of the Sub-Business Units percentages must be equal to the Business Unit percentage'));	    					
	    					mandatoryFilled = false;
	    				}
	    			}
	    			
	    			listBUInvolved.add(oppBU.record);
	    		}
	    	}
	    	
	    	if(mandatoryFilled == false) return null;	    	
	    	
	    	if(oppTotal != 100 && isBUDetailsMandatory){
	    	
	    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'The total percentage must be 100%'));
	    		return null;
	    	}
	    	
	    	bl_Opportunity_BU_Involved.setValidateTotal(false);
	    	
	    	List<Opportunity_BU_Involved__c> toDelete = [Select Id from Opportunity_BU_Involved__c where Opportunity__c = :opp.Id AND Id NOT IN :listBUInvolved];
	    	if(toDelete.size() > 0) delete toDelete;
	    	
	    	upsert listBUInvolved;    	
	    	
	    	List<Opportunity_sBU_Involved__c> listsBUInvolved = new List<Opportunity_sBU_Involved__c>();
	    	
	    	for(BUInvolved oppBU : oppBUs){
	    			    			    		
	    		if(oppBU.selected == true){
	    			
	    			for(sBUInvolved oppSBU : oppBU.sBUInvolved){
	    					
	    				if(oppSBU.selected == true){
	    					
	    					if(oppSBU.record.Opportunity_BU_Involved__c == null) oppSBU.record.Opportunity_BU_Involved__c = oppBU.record.Id;	    					
	    					listsBUInvolved.add(oppSBU.record);
	    				}
	    			}
	    		}
	    	}
	    	
	    	bl_Opportunity_sBU_Involved.skipValidation = true;
	    	
	    	List<Opportunity_sBU_Involved__c> toDeleteSBU = [Select Id from Opportunity_sBU_Involved__c where Opportunity_BU_Involved__r.Opportunity__c = :opp.Id AND Id NOT IN :listsBUInvolved];
	    	if(toDeleteSBU.size() > 0) delete toDeleteSBU;
	    	
	    	upsert listsBUInvolved;
	    	
	    	PageReference pr = new PageReference('/' + opp.Id);
	    	pr.setRedirect(true);
	    	return pr;
    	
    	}catch(Exception e){
			System.debug('**BC - SAVE** Error on line ' + e.getLineNumber() + ' : ' + e.getMessage());
    		
    		Database.rollback(sp);
    		
    		ApexPages.addMessages(e);
    	}
    	
    	return null;
    }
    
    public class BUInvolved{
    	
    	public Opportunity_BU_Involved__c record {get; set;}    	
    	public Boolean selected {get; set;}    	
    	public List<sBUInvolved> sBUInvolved {get; set;} 
    	
    	public BUInvolved(){
    		
    		selected = false;
    		sBUInvolved = new List<sBUInvolved>();

    	} 
		 	    	 	
    }
    
    public class sBUInvolved{
    	
    	public Opportunity_sBU_Involved__c record {get; set;}	
    	public Boolean selected {get; set;}    	   	
    }
}