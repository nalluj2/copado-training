/*
 *      Created Date : 20150224
 *      Description : This class tests the logic for the Territory Change triggers
 *
 *      Author = Rudy De Coninck
 */
@IsTest(seeAllData=true)
public with sharing class Test_tr_TerritoryChanges {


		@IsTest
		public static void createTerritoryChange(){

			List<Territory2> territories = [select Id, name from Territory2 where Territory2Type.DeveloperName='District' limit 5];	

			Territory2 t = territories.get(0);

			Territory_Change__c tc = new Territory_Change__c();
			tc.Business_Unit_Name__c = 'CRHF';
			tc.Company_Name__c = 'Europe';
			tc.Country_UID__c = 'BE';
			tc.Label__c = 'test';
			tc.Name__c = 'test';
			tc.Parent_Name__c = t.name;
			tc.Short_Description__c ='Short';
			tc.Operation__c = 'CREATE';
			tc.Therapy_Groups_Text__c = 'Gastro Uro; GU Enterra';
			tc.Type__c = 'Territory';

			System.debug('Insert territory change'+tc);

			insert tc;

			System.debug('Inserted territory change '+tc);

		}
}