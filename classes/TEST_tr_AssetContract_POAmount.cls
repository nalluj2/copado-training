/*
 *      Description : This is the Test Class for the APEX Trigger tr_AssetContract_POAmount
 *      Version     : 1.0
 *      Author      : Bart Caelen
 *      Date        : 20140623
*/
@isTest private class TEST_tr_AssetContract_POAmount {
	
	@isTest static void test_tr_AssetContract_POAmount() {

		Decimal decSum = 0;

	    //---------------------------------------
        // Create Test Data
        //---------------------------------------
		RecordType oRT_Contract = RecordTypeMedtronic.getRecordTypeByDevName('Contract', 'RTG_Service_Contract');
		clsTestData.createContractData(true, oRT_Contract.Id);
		clsTestData.iRecord_AssetContract = 5;
		clsTestData.createAssetContractData(true);
        //---------------------------------------
		bl_Asset_Contract.alreadyProcessedPOAmount.clear();
		
        Test.startTest();

	    //---------------------------------------
        // TEST 0
        //---------------------------------------
        List<Contract> lstContract = [SELECT Id, PO_Amount_derived_from_linked_assets__c, PO_Amount__c FROM Contract WHERE ID = :clsTestData.oMain_Contract.Id];
        system.assertEquals(lstContract[0].PO_Amount_derived_from_linked_assets__c, false);
        system.assertEquals(lstContract[0].PO_Amount__c, 0);
        List<AssetContract__c> lstAssetContract = [SELECT Id, PO_Amount2__c FROM AssetContract__c WHERE Contract__c = :clsTestData.oMain_Contract.Id];
        decSum = 0;
        for (AssetContract__c oAssetContract : lstAssetContract){
        	decSum += clsUtil.isDecimalNull(oAssetContract.PO_Amount2__c, 0);
        }
        system.assertNotEquals(lstContract[0].PO_Amount__c, decSum);
        system.assertEquals(decSum, 500);

        clsTestData.oMain_AssetContract.PO_Amount2__c = 200;
        update clsTestData.oMain_AssetContract;
        bl_Asset_Contract.alreadyProcessedPOAmount.clear();
        
        lstContract = [SELECT Id, PO_Amount_derived_from_linked_assets__c, PO_Amount__c FROM Contract WHERE ID = :clsTestData.oMain_Contract.Id];
        system.assertEquals(lstContract[0].PO_Amount_derived_from_linked_assets__c, false);
        system.assertEquals(lstContract[0].PO_Amount__c, 0);
        lstAssetContract = [SELECT Id, PO_Amount2__c FROM AssetContract__c WHERE Contract__c = :clsTestData.oMain_Contract.Id];
        decSum = 0;
        for (AssetContract__c oAssetContract : lstAssetContract){
        	decSum += clsUtil.isDecimalNull(oAssetContract.PO_Amount2__c, 0);
        }
        system.assertNotEquals(lstContract[0].PO_Amount__c, decSum);
        system.assertEquals(decSum, 600);
        //---------------------------------------


	    //---------------------------------------
        // TEST 1
        //---------------------------------------
        clsTestData.oMain_Contract.PO_Amount_derived_from_linked_assets__c = true;
        update clsTestData.oMain_Contract;
		bl_Asset_Contract.alreadyProcessedPOAmount.clear();
		
        lstContract = [SELECT Id, PO_Amount_derived_from_linked_assets__c, PO_Amount__c FROM Contract WHERE ID = :clsTestData.oMain_Contract.Id];
        system.assertEquals(lstContract[0].PO_Amount_derived_from_linked_assets__c, true);
        system.assertEquals(lstContract[0].PO_Amount__c, 600);

        lstAssetContract = [SELECT Id, PO_Amount2__c FROM AssetContract__c WHERE Contract__c = :clsTestData.oMain_Contract.Id];
        decSum = 0;
        for (AssetContract__c oAssetContract : lstAssetContract){
        	decSum += clsUtil.isDecimalNull(oAssetContract.PO_Amount2__c, 0);
        }
        system.assertEquals(lstContract[0].PO_Amount__c, decSum);
        system.assertEquals(decSum, 600);
        //---------------------------------------

		
		
	    //---------------------------------------
        // TEST 2 - UPDATE
        //---------------------------------------
        clsTestData.oMain_AssetContract.PO_Amount2__c = 300;
        update clsTestData.oMain_AssetContract;
		bl_Asset_Contract.alreadyProcessedPOAmount.clear();
        lstContract = [SELECT Id, PO_Amount_derived_from_linked_assets__c, PO_Amount__c FROM Contract WHERE ID = :clsTestData.oMain_Contract.Id];
        system.assertEquals(lstContract[0].PO_Amount_derived_from_linked_assets__c, true);
        system.assertEquals(lstContract[0].PO_Amount__c, 700);

        lstAssetContract = [SELECT Id, PO_Amount2__c FROM AssetContract__c WHERE Contract__c = :clsTestData.oMain_Contract.Id];
        decSum = 0;
        for (AssetContract__c oAssetContract : lstAssetContract){
        	decSum += clsUtil.isDecimalNull(oAssetContract.PO_Amount2__c, 0);
        }
        system.assertEquals(lstContract[0].PO_Amount__c, decSum);
        system.assertEquals(decSum, 700);
        //---------------------------------------


	    //---------------------------------------
        // TEST 3 - DELETE
        //---------------------------------------
        delete clsTestData.oMain_AssetContract;

        lstContract = [SELECT Id, PO_Amount_derived_from_linked_assets__c, PO_Amount__c FROM Contract WHERE ID = :clsTestData.oMain_Contract.Id];
        system.assertEquals(lstContract[0].PO_Amount_derived_from_linked_assets__c, true);
        system.assertEquals(lstContract[0].PO_Amount__c, 400);

        lstAssetContract = [SELECT Id, PO_Amount2__c FROM AssetContract__c WHERE Contract__c = :clsTestData.oMain_Contract.Id];
        decSum = 0;
        for (AssetContract__c oAssetContract : lstAssetContract){
        	decSum += clsUtil.isDecimalNull(oAssetContract.PO_Amount2__c, 0);
        }
        system.assertEquals(lstContract[0].PO_Amount__c, decSum);
        system.assertEquals(decSum, 400);
        //---------------------------------------


        Test.stopTest();

	}
	
}