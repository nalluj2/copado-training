@RestResource(urlMapping='/SETCaseProductService/*')
global with sharing class ws_SETCaseProductService {
	
	@HttpPost
	global static String doPost() {
 		
 		RestRequest req = RestContext.request;
			
		System.debug('post called '+ req);
		System.debug('post requestBody '+ req.requestBody.toString());
			
		String body = req.requestBody.toString();
		SETCaseProductRequest SETCaseProductRequest = (SETCaseProductRequest) System.Json.deserialize(body, SETCaseProductRequest.class);
			
		System.debug('post requestBody ' + SETCaseProductRequest);
			
		Cost_Analysis_Related_Product__c SETCaseProduct = SETCaseProductRequest.SETCaseProduct;

		SETCaseProductResponse resp = new SETCaseProductResponse();

		//We catch all exception to assure that 
		try{
				
			resp.SETCaseProduct = bl_SETCase.saveSETCaseProduct(SETCaseProduct);
			resp.id = SETCaseProductRequest.id;
	
			resp.success = true;
			
		}catch (Exception e){
			
			resp.message = e.getMessage();
			resp.success = false;
		}
 
		bl_Mobilesync.storeRequest(UserInfo.getUserId(), 'SETCaseProduct', req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
  			
		return System.JSON.serialize(resp);		
	}

	@HttpDelete
	global static String doDelete(){
		RestRequest req = RestContext.request;
			
		System.debug('post called ' + req);
		System.debug('post requestBody ' + req.requestBody.toString());
			
		String body = req.requestBody.toString();
		SETCaseProductDeleteRequest SETCaseProductDeleteRequest = (SETCaseProductDeleteRequest) System.Json.deserialize(body, SETCaseProductDeleteRequest.class);
			
		System.debug('post requestBody ' + SETCaseProductDeleteRequest);
			
		Cost_Analysis_Related_Product__c SETCaseProduct = SETCaseProductDeleteRequest.SETCaseProduct;
		
		SETCaseProductDeleteResponse resp = new SETCaseProductDeleteResponse();
		
		List<Cost_Analysis_Related_Product__c> SETCaseProductsToDelete = [select id, mobile_id__c from Cost_Analysis_Related_Product__c where Mobile_ID__c = :SETCaseProduct.Mobile_ID__c];
		
		try{
			
			if (SETCaseProductsToDelete != null && SETCaseProductsToDelete.size() > 0){	
				
				Cost_Analysis_Related_Product__c SETCaseProductToDelete = SETCaseProductsToDelete.get(0);
				bl_SETCase.deleteSETCaseProduct(SETCaseProductToDelete);
				resp.SETCaseProduct = SETCaseProductToDelete;
				resp.id = SETCaseProductDeleteRequest.id;
				resp.success = true;
				
			}else{
				
				resp.SETCaseProduct = SETCaseProduct;
				resp.id = SETCaseProductDeleteRequest.id;
				resp.message='SETCaseProduct not found';
				resp.success = true;
			}
			
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
		}
			
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'SETCaseProductDelete', req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
			
		return System.JSON.serialize(resp);
	}

	global class SETCaseProductRequest{
		public Cost_Analysis_Related_Product__c SETCaseProduct {get;set;}
		public String id{get;set;}
	}
	
	global class SETCaseProductDeleteRequest{
		public Cost_Analysis_Related_Product__c SETCaseProduct {get;set;}
		public String id{get;set;}
	}
		
	global class SETCaseProductDeleteResponse{
		public Cost_Analysis_Related_Product__c SETCaseProduct {get;set;}
		public String id{get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}

	global class SETCaseProductResponse{
		public Cost_Analysis_Related_Product__c SETCaseProduct {get;set;}
		public String id{get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}

}