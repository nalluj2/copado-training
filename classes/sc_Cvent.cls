//---------------------------------------------------------------------------------------------------------------------------------------------------
//  Author		:    Bart Caelen
//  Date 		:    05/03/2018
//  Description	:    This scheduled Class will retrieve Campaign and Campaign Member data from Cvent
//---------------------------------------------------------------------------------------------------------------------------------------------------
global class sc_Cvent implements Schedulable {

	global void execute(SchedulableContext sc) {

		// Get Cvent API Settings
        List<Cvent_API_Setting__mdt> lstCventAPISetting = [SELECT APEX_Batch_Size__c FROM Cvent_API_Setting__mdt WHERE DeveloperName = 'CVENT' AND Active__c = true];
        if (lstCventAPISetting.size() == 0) return;
        Cvent_API_Setting__mdt oCventAPISetting = lstCventAPISetting[0];

		Integer iBatchSize = 100;

        if ( (clsUtil.isDecimalNull(oCventAPISetting.APEX_Batch_Size__c, 100) > 0) && (clsUtil.isDecimalNull(oCventAPISetting.APEX_Batch_Size__c, 100) <= 200) ){
        	iBatchSize = clsUtil.isDecimalNull(oCventAPISetting.APEX_Batch_Size__c, 100).intValue();
        }

		ba_Cvent oBatch = new ba_Cvent();
			oBatch.tCventAPI = 'CVENT';
			oBatch.tProcessType = 'CAMPAIGNMEMBER';
			oBatch.iBatchSize = iBatchSize;
			oBatch.idCampaignRecordtype_Cvent = clsUtil.getRecordTypeByDevName('Campaign', 'CVent').Id;
		Database.executebatch(oBatch, iBatchSize);


	}

}
//---------------------------------------------------------------------------------------------------------------------------------------------------