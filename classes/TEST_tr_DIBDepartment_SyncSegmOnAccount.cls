@isTest
private class TEST_tr_DIBDepartment_SyncSegmOnAccount {
	
    private static Id id_Account;
    private static void createTestData() {

        //--------------------------------------------------------
        // CREATE TEST DATA
        //--------------------------------------------------------
        // Create Master Data
        clsTestData.createBusinessUnitData();
        clsTestData.createSubBusinessUnitData();

        // Create Account Data
        clsTestData.idRecordType_Account = RecordTypeMedtronic.Account('SAP_Account').Id;
        clsTestData.createAccountData();
        id_Account = clsTestData.oMain_Account.Id;

        // Create Department Master Data
        clsTestData.createDepartmentMasterData_Default(true);

        // Create DIB_Department__c Data
        clsTestData.iRecord_DIBDepartment = 5;
        clsTestData.createDIBDepartmentData();
        //--------------------------------------------------------

    }


	@isTest static void test_tr_DIBDepartment_SyncSegmOnAccount_INSERT() {

        //--------------------------------------------------------
        // PERFOM TESTING
        //--------------------------------------------------------
        Test.startTest();

        createTestData();

        Test.stopTest();


        List<Account> lstAccount = [SELECT Id, Pediatric_Segment__c, Adult_Pediatric_Segment__c, Other_Segment__c, Adult_Type_1_Segment__c, Adult_Type_2_Segment__c FROM Account WHERE Id = :id_Account];
        System.assertEquals(lstAccount.size(), 1);
        for (Account oAccount : lstAccount){
            System.assertEquals(oAccount.Pediatric_Segment__c.toUpperCase(), 'LET GO ON');
            System.assertEquals(oAccount.Adult_Pediatric_Segment__c.toUpperCase(), 'LET GO ON');
            System.assertEquals(oAccount.Other_Segment__c.toUpperCase(), 'LET GO ON');
            System.assertEquals(oAccount.Adult_Type_1_Segment__c.toUpperCase(), 'LET GO ON');
            System.assertEquals(oAccount.Adult_Type_2_Segment__c.toUpperCase(), 'LET GO ON');
        }
        //--------------------------------------------------------


	}
	
    @isTest static void test_tr_DIBDepartment_SyncSegmOnAccount_UPDATE_1() {

        //--------------------------------------------------------
        // CREATE TEST DATA
        //--------------------------------------------------------
        createTestData();
        //--------------------------------------------------------

        Integer iCounter = 0;

        List<DIB_Department__c> lstDIBDepartment = [SELECT Id, Account__c, Department_ID__c, DiB_Segment__c FROM DIB_Department__c];
        List<Department_Master__c> lstDepartmentMaster = [SELECT Id, Name FROM Department_Master__c];
        for (DIB_Department__c oDIBDepartment : lstDIBDepartment){
            oDIBDepartment.DiB_Segment__c = 'TEST SEGMENT';
            oDIBDepartment.DiB_Segment_Explanation__c = 'TEST SEGMENT EXPLANATION';
        }

        Test.startTest();

        update lstDIBDepartment;

        Test.stopTest();

        List<Account> lstAccount = [SELECT Id, Pediatric_Segment__c, Adult_Pediatric_Segment__c, Other_Segment__c, Adult_Type_1_Segment__c, Adult_Type_2_Segment__c, Pediatric_Segment_Explanation__c, Adult_Pediatric_Segment_Explanation__c, Other_Segment_Explanation__c, Adult_Type_1_Segment_Explanation__c, Adult_Type_2_Segment_Explanation__c FROM Account LIMIT 1];
        for (Account oAccount : lstAccount){
            System.assertEquals(oAccount.Pediatric_Segment__c, 'TEST SEGMENT');
            System.assertEquals(oAccount.Adult_Pediatric_Segment__c, 'TEST SEGMENT');
            System.assertEquals(oAccount.Other_Segment__c, 'TEST SEGMENT');
            System.assertEquals(oAccount.Adult_Type_1_Segment__c, 'TEST SEGMENT');
            System.assertEquals(oAccount.Adult_Type_2_Segment__c, 'TEST SEGMENT');

            System.assertEquals(oAccount.Pediatric_Segment_Explanation__c, 'TEST SEGMENT EXPLANATION');
            System.assertEquals(oAccount.Adult_Pediatric_Segment_Explanation__c, 'TEST SEGMENT EXPLANATION');
            System.assertEquals(oAccount.Other_Segment_Explanation__c, 'TEST SEGMENT EXPLANATION');
            System.assertEquals(oAccount.Adult_Type_1_Segment_Explanation__c, 'TEST SEGMENT EXPLANATION');
            System.assertEquals(oAccount.Adult_Type_2_Segment_Explanation__c, 'TEST SEGMENT EXPLANATION');
        }


	}

    @isTest static void test_tr_DIBDepartment_SyncSegmOnAccount_UPDATE_2() {

        //--------------------------------------------------------
        // CREATE TEST DATA
        //--------------------------------------------------------
        createTestData();
        //--------------------------------------------------------

        Integer iCounter = 0;

        List<Department_Master__c> lstDepartmentMaster = [SELECT Id, Name FROM Department_Master__c WHERE Name = 'Adult/Pediatric'];
        List<DIB_Department__c> lstDIBDepartment = [SELECT Id, Account__c, Department_ID__c, DiB_Segment__c FROM DIB_Department__c WHERE Department_ID__c = :lstDepartmentMaster];
        delete lstDIBDepartment;

        Id id_DepartmentMaster = lstDepartmentMaster[0].Id;

        lstDIBDepartment = [SELECT Id, Account__c, Department_ID__c, Department_ID__r.Name, DiB_Segment__c FROM DIB_Department__c WHERE Department_ID__r.Name = 'Pediatric'];
        for (DIB_Department__c oDIBDepartment : lstDIBDepartment){
            oDIBDepartment.Department_ID__c = id_DepartmentMaster;
            oDIBDepartment.DiB_Segment__c = 'TEST SEGMENT';
            oDIBDepartment.DiB_Segment_Explanation__c = 'TEST SEGMENT EXPLANATION';
        }

        Test.startTest();

        update lstDIBDepartment;

        Test.stopTest();

        List<Account> lstAccount = [SELECT Id, Pediatric_Segment__c, Adult_Pediatric_Segment__c, Other_Segment__c, Adult_Type_1_Segment__c, Adult_Type_2_Segment__c, Pediatric_Segment_Explanation__c, Adult_Pediatric_Segment_Explanation__c, Other_Segment_Explanation__c, Adult_Type_1_Segment_Explanation__c, Adult_Type_2_Segment_Explanation__c FROM Account LIMIT 1];
        for (Account oAccount : lstAccount){
            System.assertEquals(oAccount.Pediatric_Segment__c, null);
            System.assertEquals(oAccount.Adult_Pediatric_Segment__c, 'TEST SEGMENT');
            System.assertEquals(oAccount.Other_Segment__c.toUpperCase(), 'LET GO ON');
            System.assertEquals(oAccount.Adult_Type_1_Segment__c.toUpperCase(), 'LET GO ON');
            System.assertEquals(oAccount.Adult_Type_2_Segment__c.toUpperCase(), 'LET GO ON');

            System.assertEquals(oAccount.Pediatric_Segment_Explanation__c, null);
            System.assertEquals(oAccount.Adult_Pediatric_Segment_Explanation__c, 'TEST SEGMENT EXPLANATION');
            System.assertEquals(oAccount.Other_Segment_Explanation__c, null);
            System.assertEquals(oAccount.Adult_Type_1_Segment_Explanation__c, null);
            System.assertEquals(oAccount.Adult_Type_2_Segment_Explanation__c, null);
        }
        

    }    

    @isTest static void test_tr_DIBDepartment_SyncSegmOnAccount_DELETE() {

        //--------------------------------------------------------
        // CREATE TEST DATA
        //--------------------------------------------------------
        createTestData();
        //--------------------------------------------------------

        List<DIB_Department__c> lstDIBDepartment = [SELECT Id, Account__c, Department_ID__c, DiB_Segment__c FROM DIB_Department__c];

        Test.startTest();

        delete lstDIBDepartment;

        Test.stopTest();

        List<Account> lstAccount = [SELECT Id, Pediatric_Segment__c, Adult_Pediatric_Segment__c, Other_Segment__c, Adult_Type_1_Segment__c, Adult_Type_2_Segment__c, Pediatric_Segment_Explanation__c, Adult_Pediatric_Segment_Explanation__c, Other_Segment_Explanation__c, Adult_Type_1_Segment_Explanation__c, Adult_Type_2_Segment_Explanation__c FROM Account LIMIT 1];
        for (Account oAccount : lstAccount){
            System.assertEquals(oAccount.Pediatric_Segment__c, null);
            System.assertEquals(oAccount.Adult_Pediatric_Segment__c, null);
            System.assertEquals(oAccount.Other_Segment__c, null);
            System.assertEquals(oAccount.Adult_Type_1_Segment__c, null);
            System.assertEquals(oAccount.Adult_Type_2_Segment__c, null);

            System.assertEquals(oAccount.Pediatric_Segment_Explanation__c, null);
            System.assertEquals(oAccount.Adult_Pediatric_Segment_Explanation__c, null);
            System.assertEquals(oAccount.Other_Segment_Explanation__c, null);
            System.assertEquals(oAccount.Adult_Type_1_Segment_Explanation__c, null);
            System.assertEquals(oAccount.Adult_Type_2_Segment_Explanation__c, null);
        }


    }
	
}