global class es_Run_Internal_Unit_Tests implements Messaging.InboundEmailHandler{
	
	global Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email, Messaging.InboundEnvelope env){
                                                       	
    	Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
    	
    	try{
    		
    		if(email.subject.contains('Run All Internal Unit Tests')) bl_Run_All_Internal_Tests.runAllInternalTests();        		        		
    		else if(email.subject.contains('Check Unit Test Results')) bl_Check_UnitTest_Run_Results.getTestResults(null);
    		else throw new IllegalArgumentException('Unknown action');
    		
    		result.success = true;
    			
    	}catch(Exception e){
    		
    		result.success = false;
    		result.message = e.getMessage();
    	}
    	
    	return result;                                                   	
	}    
}