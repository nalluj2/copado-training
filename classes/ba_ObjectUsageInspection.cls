global class ba_ObjectUsageInspection implements Database.Batchable<SObject>, Database.Stateful {

	global Set<String> setExcludedObject = new Set<String>{'Vote','Attachment','CaseComment','ApexTestQueueItem','CaseArticle','AccountBrandShare','AccountShare','Activity','CaseSolution','CaseTeamTemplateRecord','ContentDocumentLink','ContentWorkspaceDoc','ContentWorkspaceMember','NetworkUserHistoryRecent','Partner','PartnerFundAllocation','PartnerFundClaim','PartnerFundRequest','PartnerMarketingBudget','PartnerMarketingBudgetFeed','PartnerMarketingBudgetHistory','PartnerMarketingBudgetShare','PartnerRole','PartyConsent','PartyConsentChangeEvent','PartyConsentFeed','PartyConsentHistory','PartyConsentShare'};
	global Set<String> setIncludedObject = new Set<String>{};//{'Account','Contact','Lead','Opportunity','OpportunityLineItem','OpportunityTeamMember','Case','Task','Event','Campaign','Asset','CampaignMember','CaseContactRole','Contract','ContractContactRole','Pricebook2','PricebookEntry','Product2','User','Territory2'};
	global Integer iBatchsize = 500;
	global Date dStartDate;
	global String tSObjectName;
	global List<String> lstSObjectName;
	global String tAction;
	global Integer iCounter;
	global Map<String, Map<String, String>> mapCompanyCode_SBU_BU;
	global String tProcessingObjectType;	// ALL - CUSTOM - STANDARD - INCLUDE (Only process setIncludedObject)
	global Boolean bLogProcessingObject = false;	// CREATE A RECORD FOR EACH OBJECT / ACTION WITH Record Count = -100
	global Boolean bUseSystemModStamp = false;	// Use SystemModStamp instead of LastModifiedDate --> in big objects filtering on LastModifiedDate might result in a timeout error

	global Map<String, Integer> mapRecCounter_Created;
	global Map<String, Integer> mapRecCounter_Updated;

	private Boolean bHasRecords;
	private Boolean bHasErrors;

	global Database.QueryLocator start(Database.BatchableContext context) {

		String tSOQL = '';

		if (tAction == 'Delete_All'){

			tSOQL = 'SELECT Id FROM Object_Usage_Inspection__c';

			return Database.getQueryLocator(tSOQL);

		}

		// Initialize Variables
		if (dStartDate == null) dStartDate = Date.today().addMonths(-6);
		if (String.isBlank(tProcessingObjectType)){
			tProcessingObjectType = 'ALL';
			if (setIncludedObject != null && setIncludedObject.size() > 0){
				tProcessingObjectType = 'INCLUDE';
			}
		}
		if (String.isBlank(tAction)) tAction = 'Created';
		if (iCounter == null) iCounter = 0;
		if (lstSObjectName == null) lstSObjectName = loadSObjectNames();
		if (String.isBlank(tSObjectName)) tSObjectName = lstSObjectName[0];
		mapRecCounter_Created = new Map<String, Integer>();
		mapRecCounter_Updated = new Map<String, Integer>();

		// Get SBU - BU Mapping so that we can translate the SBU on user into the corresponding BU
		if (mapCompanyCode_SBU_BU == null) mapCompanyCode_SBU_BU = loadCompany_BU_SBU_Mapping();

		
		// Delete the Object_Usage_Inspection__c of the processing sObject
		List<Object_Usage_Inspection__c> lstObjectUsageInspection_Delete = [SELECT Id FROM Object_Usage_Inspection__c WHERE Object_Name__c = :tSObjectName AND Action__c = :tAction];
		if (lstObjectUsageInspection_Delete.size() > 0) delete lstObjectUsageInspection_Delete;

		if (tAction == 'Created'){
		
			tSOQL = 'SELECT CreatedBy.Country, CreatedBy.Company_Code_Text__c, CreatedBy.Primary_sBU__c, CreatedById FROM ' + tSObjectName + ' WHERE CreatedDate >= ' + DateTime.newInstance(dStartDate, Time.newInstance(0, 0, 0, 0)).format('yyyy-MM-dd\'T\'hh:mm:ss\'z\'') + ' ORDER BY CreatedBy.Country, CreatedBy.Company_Code_Text__c, CreatedBy.Primary_sBU__c, CreatedById';

		}else if (tAction == 'Updated'){
			
			if (bUseSystemModStamp){

				tSOQL = 'SELECT CreatedDate, LastModifiedDate, LastModifiedBy.Country, LastModifiedBy.Company_Code_Text__c, LastModifiedBy.Primary_sBU__c, LastModifiedById FROM ' + tSObjectName + ' WHERE SystemModstamp >= ' + DateTime.newInstance(dStartDate, Time.newInstance(0, 0, 0, 0)).format('yyyy-MM-dd\'T\'hh:mm:ss\'z\'') + ' ORDER BY LastModifiedBy.Country, LastModifiedBy.Company_Code_Text__c, LastModifiedBy.Primary_sBU__c, LastModifiedById';

			}else{

				tSOQL = 'SELECT CreatedDate, LastModifiedDate, LastModifiedBy.Country, LastModifiedBy.Company_Code_Text__c, LastModifiedBy.Primary_sBU__c, LastModifiedById FROM ' + tSObjectName + ' WHERE LastModifiedDate >= ' + DateTime.newInstance(dStartDate, Time.newInstance(0, 0, 0, 0)).format('yyyy-MM-dd\'T\'hh:mm:ss\'z\'') + ' ORDER BY LastModifiedBy.Country, LastModifiedBy.Company_Code_Text__c, LastModifiedBy.Primary_sBU__c, LastModifiedById';
			}

		}

		bHasRecords = false;
		bHasErrors = false;

		if (bLogProcessingObject){
			Object_Usage_Inspection__c oObjectUsageInspection = new Object_Usage_Inspection__c();
				oObjectUsageInspection.Object_Name__c = tSObjectName;
				oObjectUsageInspection.Action__c = tAction;
				oObjectUsageInspection.NumRecords__c = -100;
			insert oObjectUsageInspection;
		}

		Database.QueryLocator oQueryLocator;
		try{
		
			clsUtil.bubbleException('EX_QueryLocator');
			oQueryLocator = Database.getQueryLocator(tSOQL);
		
		}catch(Exception oEX){
			oQueryLocator = Database.getQueryLocator('SELECT Id FROM Account LIMIT 0');
			bHasErrors = true;
			Object_Usage_Inspection__c oObjectUsageInspection = new Object_Usage_Inspection__c();
				oObjectUsageInspection.Object_Name__c = tSObjectName;
				oObjectUsageInspection.Action__c = tAction;
				oObjectUsageInspection.NumRecords__c = -1;
			insert oObjectUsageInspection;

		}

		return oQueryLocator;

//		return Database.getQueryLocator(tSOQL);

	}

	global void execute(Database.BatchableContext context, List<sObject> lstData){
		
		if (tAction == 'Delete_All'){
				
			delete lstData;
			Database.emptyRecycleBin(lstData); 

		}

		for (SObject oData : lstData){

			if (tAction == 'Created'){

				String tKey = '';//(String)oData.getSobject('CreatedBy').get('Country') + '$$$' + (String)oData.getSobject('CreatedBy').get('Company_Code_Text__c') + '$$$' + (String)oData.getSobject('CreatedBy').get('Primary_sBU__c') + '$$$' + (String)oData.get('CreatedById');

				String tKeyPart = 'N/A';
				if (!String.isBlank((String)oData.getSobject('CreatedBy').get('Country'))) tKeyPart = (String)oData.getSobject('CreatedBy').get('Country');
				tKey += tKeyPart + '$$$';

				tKeyPart = 'N/A';
				if (!String.isBlank((String)oData.getSobject('CreatedBy').get('Company_Code_Text__c'))) tKeyPart = (String)oData.getSobject('CreatedBy').get('Company_Code_Text__c');
				tKey += tKeyPart + '$$$';

				tKeyPart = 'N/A';
				if (!String.isBlank((String)oData.getSobject('CreatedBy').get('Primary_sBU__c'))) tKeyPart = (String)oData.getSobject('CreatedBy').get('Primary_sBU__c');
				tKey += tKeyPart + '$$$';

				tKey += (String)oData.get('CreatedById');

				

				Integer iCounter = 0;
				if (mapRecCounter_Created.containsKey(tKey)) iCounter = mapRecCounter_Created.get(tKey);
				iCounter++;
				bHasRecords = true;	
				mapRecCounter_Created.put(tKey, iCounter);

			}else if (tAction == 'Updated'){

				String tKey = '';//(String)oData.getSobject('LastModifiedBy').get('Country') + '$$$' + (String)oData.getSobject('LastModifiedBy').get('Company_Code_Text__c') + '$$$' + (String)oData.getSobject('LastModifiedBy').get('Primary_sBU__c') + '$$$' + (String)oData.get('LastModifiedById');

				String tKeyPart = 'N/A';
				if (!String.isBlank((String)oData.getSobject('LastModifiedBy').get('Country'))) tKeyPart = (String)oData.getSobject('LastModifiedBy').get('Country');
				tKey += tKeyPart + '$$$';

				tKeyPart = 'N/A';
				if (!String.isBlank((String)oData.getSobject('LastModifiedBy').get('Company_Code_Text__c'))) tKeyPart = (String)oData.getSobject('LastModifiedBy').get('Company_Code_Text__c');
				tKey += tKeyPart + '$$$';

				tKeyPart = 'N/A';
				if (!String.isBlank((String)oData.getSobject('LastModifiedBy').get('Primary_sBU__c'))) tKeyPart = (String)oData.getSobject('LastModifiedBy').get('Primary_sBU__c');
				tKey += tKeyPart + '$$$';

				tKey += (String)oData.get('LastModifiedById');


				if ( (Datetime)oData.get('LastModifiedDate') != (Datetime)oData.get('CreatedDate') ){

					Integer iCounter = 0;
					if (mapRecCounter_Updated.containsKey(tKey)) iCounter = mapRecCounter_Updated.get(tKey);
					iCounter++;		
					bHasRecords = true;	
					mapRecCounter_Updated.put(tKey, iCounter);

				}

			}
		
		}

	}
	
	global void finish(Database.BatchableContext context) {

		if (bHasRecords == false){
			
			if (!bHasErrors){

				Object_Usage_Inspection__c oObjectUsageInspection = new Object_Usage_Inspection__c();
					oObjectUsageInspection.Object_Name__c = tSObjectName;
					oObjectUsageInspection.Action__c = tAction;
					oObjectUsageInspection.NumRecords__c = 0;
				insert oObjectUsageInspection;

			}

		}else{

			List<Object_Usage_Inspection__c> lstObjectUsageInspection_Insert = new List<Object_Usage_Inspection__c>();	
			if (tAction == 'Created'){

				for (String tKey : mapRecCounter_Created.keySet()){

					List<String> lstKeyPart = tKey.split('\\$\\$\\$');

					Object_Usage_Inspection__c oObjectUsageInspection = new Object_Usage_Inspection__c();

						oObjectUsageInspection.Object_Name__c = tSObjectName;
						try{ oObjectUsageInspection.User__c = (Id)lstKeyPart[3]; }catch(Exception oEX){}
						oObjectUsageInspection.Action__c = tAction;
						oObjectUsageInspection.NumRecords__c = mapRecCounter_Created.get(tKey);
						oObjectUsageInspection.Sub_Business_Unit__c = (String)lstKeyPart[2];

						if ( mapCompanyCode_SBU_BU.containsKey(lstKeyPart[1]) ){
							Map<String, String> mapSBU_BU = mapCompanyCode_SBU_BU.get(lstKeyPart[1]);
							if (mapSBU_BU.containsKey((String)lstKeyPart[2])){
								oObjectUsageInspection.Business_Unit__c = mapSBU_BU.get((String)lstKeyPart[2]);
							}
						}

					lstObjectUsageInspection_Insert.add(oObjectUsageInspection);


				}

			}else if (tAction == 'Updated'){

				for (String tKey : mapRecCounter_Updated.keySet()){
			
					List<String> lstKeyPart = tKey.split('\\$\\$\\$');

					Object_Usage_Inspection__c oObjectUsageInspection = new Object_Usage_Inspection__c();

						oObjectUsageInspection.Object_Name__c = tSObjectName;
						try{ oObjectUsageInspection.User__c = (Id)lstKeyPart[3]; }catch(Exception oEX){}
						oObjectUsageInspection.Action__c = tAction;
						oObjectUsageInspection.NumRecords__c = mapRecCounter_Updated.get(tKey);
						oObjectUsageInspection.Sub_Business_Unit__c = (String)lstKeyPart[2];

						if ( mapCompanyCode_SBU_BU.containsKey(lstKeyPart[1]) ){
							Map<String, String> mapSBU_BU = mapCompanyCode_SBU_BU.get(lstKeyPart[1]);
							if (mapSBU_BU.containsKey((String)lstKeyPart[2])){
								oObjectUsageInspection.Business_Unit__c = mapSBU_BU.get((String)lstKeyPart[2]);
							}
						}
						
					lstObjectUsageInspection_Insert.add(oObjectUsageInspection);

				}

			}

			if (lstObjectUsageInspection_Insert.size() > 0){

				insert lstObjectUsageInspection_Insert;

			}

		}

		if (tAction == 'Created'){

			tAction = 'Updated';

			ba_ObjectUsageInspection oBatch = new ba_ObjectUsageInspection();
				oBatch.setExcludedObject = setExcludedObject;
				oBatch.setIncludedObject = setIncludedObject;
				oBatch.iBatchsize = iBatchsize;
				oBatch.dStartDate = dStartDate;
				oBatch.tSObjectName = tSObjectName;
				oBatch.lstSObjectName = lstSObjectName;
				oBatch.tAction = tAction;
				oBatch.iCounter = iCounter;
				oBatch.mapCompanyCode_SBU_BU = mapCompanyCode_SBU_BU;
				oBatch.tProcessingObjectType = tProcessingObjectType;
				oBatch.bLogProcessingObject = bLogProcessingObject;
				oBatch.bUseSystemModStamp = bUseSystemModStamp;
			if (!Test.isRunningTest()) Database.executebatch(oBatch, iBatchsize);

		}else if (tAction == 'Updated'){

			if (iCounter + 1 < lstSObjectName.size()){
			
				iCounter++;
				tSObjectName = lstSObjectName[iCounter];
				tAction = 'Created';

				ba_ObjectUsageInspection oBatch = new ba_ObjectUsageInspection();
					oBatch.setExcludedObject = setExcludedObject;
					oBatch.setIncludedObject = setIncludedObject;
					oBatch.iBatchsize = iBatchsize;
					oBatch.dStartDate = dStartDate;
					oBatch.tSObjectName = tSObjectName;
					oBatch.lstSObjectName = lstSObjectName;
					oBatch.tAction = tAction;
					oBatch.iCounter = iCounter;
					oBatch.mapCompanyCode_SBU_BU = mapCompanyCode_SBU_BU;
					oBatch.tProcessingObjectType = tProcessingObjectType;
					oBatch.bLogProcessingObject = bLogProcessingObject;
					oBatch.bUseSystemModStamp = bUseSystemModStamp;
				if (!Test.isRunningTest()) Database.executebatch(oBatch, iBatchsize);

			}else{
				
				// THE END

			}

		}

	}


	@TestVisible private List<String> loadSObjectNames(){

		Set<String> setSObjectName = new Set<String>();

		// Get all sObjects in a list
		if (tProcessingObjectType != 'INCLUDE'){

			for (Schema.SObjectType oSObjectType : Schema.getGlobalDescribe().values()){

				if (oSObjectType.getDescribe().isQueryable() && oSObjectType.getDescribe().isCreateable()){

					String tSObjectName_Tmp = oSObjectType.getDescribe().getName();

					if (tSObjectName_Tmp.endsWith('Share')) continue;
					
					if (!setExcludedObject.contains(tSObjectName_Tmp)){

						if (oSObjectType.getDescribe().isCustom()){

							if (tProcessingObjectType == 'ALL' || tProcessingObjectType == 'CUSTOM'){

								setSObjectName.add(oSObjectType.getDescribe().getName());

							}

						}else{

							if (tProcessingObjectType == 'ALL' || tProcessingObjectType == 'STANDARD'){

								setSObjectName.add(oSObjectType.getDescribe().getName());

							}

						}

					}

				}

			}

		}

		if (setIncludedObject.size() > 0) setSObjectName.addAll(setIncludedObject);
		
		List<String> lstSObjectName_Unique = new List<String>();
		lstSObjectName_Unique.addAll(setSObjectName);
		lstSObjectName_Unique.sort();
		return lstSObjectName_Unique;

	}


	@TestVisible private Map<String, Map<String, String>> loadCompany_BU_SBU_Mapping(){

		Map<String, Map<String, String>> mapCompanyCode_SBU_BU = new Map<String, Map<String, String>>();
		List<Sub_Business_Units__c> lstSBU = [SELECT Id, Name, Business_Unit__r.Name, Business_Unit__r.Company__r.Company_Code_Text__c FROM Sub_Business_Units__c];
		for (Sub_Business_Units__c oSBU : lstSBU){

			Map<String, String> mapSBU_BU = new Map<String, String>();
			if (mapCompanyCode_SBU_BU.containsKey(oSBU.Business_Unit__r.Company__r.Company_Code_Text__c)){
				mapSBU_BU = mapCompanyCode_SBU_BU.get(oSBU.Business_Unit__r.Company__r.Company_Code_Text__c);
			}

			mapSBU_BU.put(oSBU.Name, oSBU.Business_Unit__r.Name);
			mapCompanyCode_SBU_BU.put(oSBU.Business_Unit__r.Company__r.Company_Code_Text__c, mapSBU_BU);

		}

		return mapCompanyCode_SBU_BU;

	}	
}