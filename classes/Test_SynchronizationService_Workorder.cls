@isTest
private class Test_SynchronizationService_Workorder {
	
	private static testMethod void syncWorkorder(){

		clsTestData_MasterData.tCompanyCode = 'EUR';
		clsTestData_MasterData.tBusinessUnitName = 'Cranial Spinal';
		clsTestData_MasterData.createBusinessUnit(true);
		
		Synchronization_Service_Setting__c syncSettings = new Synchronization_Service_Setting__c();
			syncSettings.username__c = 'test@medtronic.com';
			syncSettings.password__c = 'password';
			syncSettings.client_id__c = '123456789';
			syncSettings.client_secret__c = 'XXXXX';
			syncSettings.is_sandbox__c = true;
			syncSettings.sync_Enabled__c = true;
		insert syncSettings;
		
		//Products
		Product2 testProduct = new Product2();
			testProduct.RecordTypeId = clsUtil.getRecordTypeByDevName('Product2', 'SAP_Product').Id;
			testProduct.Name = 'PoleStar';
			testProduct.ProductCode = '000999';
			testProduct.isActive = true;
			testProduct.Business_Unit_ID__c = clsTestData_MasterData.oMain_BusinessUnit.Id;
				
		Product2 testDispProduct1 = new Product2();
			testDispProduct1.RecordTypeId = clsUtil.getRecordTypeByDevName('Product2', 'SAP_Product').Id;
			testDispProduct1.Name = 'PoleStar N-10';
			testDispProduct1.ProductCode = '000888';
			testDispProduct1.isActive = true;
			testDispProduct1.Business_Unit_ID__c = clsTestData_MasterData.oMain_BusinessUnit.Id;
		
		Product2 testDispProduct2 = new Product2();
			testDispProduct2.RecordTypeId = clsUtil.getRecordTypeByDevName('Product2', 'SAP_Product').Id;
			testDispProduct2.Name = 'PoleStar N-20';
			testDispProduct2.ProductCode = '000777';
			testDispProduct2.isActive = true;
			testDispProduct2.Business_Unit_ID__c = clsTestData_MasterData.oMain_BusinessUnit.Id;
				
		insert new List<Product2>{testProduct, testDispProduct1, testDispProduct2};
		
		//Software
		Software__c soft = new Software__c();
			soft.Name = 'Test Software';
			soft.Active__c = true;
			soft.External_Id__c = 'Test_Software_Id';
			soft.Software_Name__c = 'Test Software';
			soft.Version__c = '1.0';
			soft.Asset_Product_Type__c = 'PoleStar N-10';
		
		Software__c altSoft = new Software__c();
			altSoft.Name = 'Test Alternative Software';
			altSoft.Active__c = true;
			altSoft.External_Id__c = 'Test_Alt_Software_Id';
			altSoft.Software_Name__c = 'Test Alternative Software';
			altSoft.Version__c = '1.0';
			altSoft.Asset_Product_Type__c = 'PoleStar N-10';
		
		insert new List<Software__c>{soft, altSoft};
		
		//Account
		Account acc = new Account();	
			acc.AccountNumber = '0000000';
			acc.Name = 'Syn Test Account';
			acc.Phone = '+123456789';
			acc.BillingCity = 'Minneapolis';
			acc.BillingCountry = 'UNITED STATES';
			acc.BillingState = 'Minnesota';
			acc.BillingStreet = 'street';
			acc.BillingPostalCode = '123456';
			acc.ST_NAV_Non_SAP_Account__c = true;
			acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non-SAP Account').getRecordTypeId();	
		insert acc;
		
		//Contact
		Contact cnt = new Contact();
			cnt.FirstName = 'Test';
			cnt.LastName = 'Contact';
			cnt.Phone = '+123456789';		
			cnt.Email = 'test.contact@gmail.com';
			cnt.MobilePhone = '+123456789';		
			cnt.MailingCity = 'Minneapolis';
			cnt.MailingCountry = 'UNITED STATES';
			cnt.MailingState = 'Minnesota';
			cnt.MailingStreet = 'street';
			cnt.MailingPostalCode = '123456';
			cnt.External_Id__c = 'Test_Contact_Id';
			cnt.Contact_Department__c = 'Cardiology';
			cnt.Contact_Primary_Specialty__c = 'Cardiovascular'; 
			cnt.Affiliation_To_Account__c = 'Employee'; 
			cnt.Primary_Job_Title_vs__c = 'Surgeon'; 
			cnt.Contact_Gender__c = 'Male'; 
			cnt.AccountId = acc.Id;
			cnt.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Generic Contact').getRecordTypeId();				
		insert cnt;
		
		//Asset
		Asset asset = new Asset();
			asset.AccountId = acc.Id;
			asset.Product2Id = testProduct.Id;
			asset.Asset_Product_Type__c = 'PoleStar N-10';
			asset.Ownership_Status__c = 'Purchased';
			asset.Name = '123456789';
			asset.Serial_Nr__c = '123456789';
			asset.Status = 'Installed';
			asset.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('PoleStar').getRecordTypeId();
		insert asset;
		
		//Case
		Case newCase = new Case();
			newCase.AccountId = acc.Id;
			newCase.AssetId = asset.Id;
			newCase.RecordTypeId = clsUtil.getRecordTypeByDevName('Case', 'New_Case').Id;
			newCase.External_Id__c = 'Test_Case_Id';				
		insert newCase;
		
		//Complaint				
		Complaint__c comp = new Complaint__c();
			comp.Account_Name__c = acc.Id;
			comp.Asset__c = asset.Id;
			comp.Status__c = 'Open';
			comp.External_Id__c = 'Test_Complaint_Id';
		insert comp;
		
		//Related Workorder
		Workorder__c relWorkOrder = new Workorder__c();
			relWorkOrder.Account__c = acc.Id;
			relWorkOrder.Asset__c = asset.Id;
			relWorkOrder.Status__c = 'In Process';
			relWorkOrder.External_Id__c = 'Test_Rel_Work_Order_Id';
			relWorkOrder.RecordTypeId = Schema.SObjectType.Workorder__c.getRecordTypeInfosByName().get('PoleStar System Checkout').getRecordTypeId();
		insert relWorkOrder;
		
		Test.startTest();
		
		Test_bl_SynchronizationService_Utils.CalloutMock mockImpl = new Test_bl_SynchronizationService_Utils.CalloutMock();
		
		Test.setMock(HttpCalloutMock.class, mockImpl);
		bl_SynchronizationService_Utils.calloutMock = mockImpl;
		
		//Workorder
		Workorder__c workOrder = new Workorder__c();
			workOrder.Account__c = acc.Id;
			workOrder.Destination_Account__c = acc.Id;
			workOrder.Contact__c = cnt.Id;
			workOrder.Surgeon_Trainee__c = cnt.Id;
			workOrder.Status__c = 'In Process';
			workOrder.Asset__c = asset.Id;		
			workOrder.Disposable_PN_1__c = testDispProduct1.Id;
			workOrder.Disposable_PN_2__c = testDispProduct2.Id;		
			workOrder.T_M_Product_Part__c = testProduct.Id;
			workOrder.Case__c = newCase.Id;	
			workOrder.Complaint__c = comp.Id;
			workOrder.Related_Workorder__c = relWorkOrder.ID;
			workOrder.Software_1__c = soft.Id;
			workOrder.Software_2__c = soft.Id;
			workOrder.Software_3__c = soft.Id;
			workOrder.Software_4__c = soft.Id;
			workOrder.Software_5__c = altSoft.Id;
			workOrder.Software_6__c = altSoft.Id;
			workOrder.Software_7__c = altSoft.Id;
			workOrder.Software_8__c = altSoft.Id;
			workOrder.External_Id__c = 'Test_Work_Order_Id';
			workOrder.RecordTypeId = Schema.SObjectType.Workorder__c.getRecordTypeInfosByName().get('PoleStar System Checkout').getRecordTypeId();
		insert workOrder;
			
		SynchronizationService_Workorder woService = new SynchronizationService_Workorder();
		String payload = woService.generatePayload('Test_Work_Order_Id');
		
		System.assert(payload != null);
		
		woService.processPayload(null);	
	}	
}