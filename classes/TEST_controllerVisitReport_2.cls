/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TEST_controllerVisitReport_2 {

    static testMethod void myUnitTest() {
        System.debug(' ################## ' + 'BEGIN TEST_controllerVisitReport' + ' ################');        
        Account acc1 = new Account();
        acc1.Name = 'TEST_controllerVisitReport_2 Test Coverage Account 1 ' ; 
        Test.startTest();
        insert acc1 ;               
        
        Contact cont1 = new Contact();
        cont1.LastName = 'TEST_controllerVisitReport_2 Test Coverage' ;  
        cont1.FirstName = 'Test Contact';
        cont1.AccountId = acc1.Id ;  
        cont1.Phone = '0095696991729'; 
        cont1.Email ='test@coverage.com';
	    cont1.Contact_Department__c = 'Diabetes Adult'; 
	    cont1.Contact_Primary_Specialty__c = 'ENT';
	    cont1.Affiliation_To_Account__c = 'Employee';
	    cont1.Primary_Job_Title_vs__c = 'Manager';
	    cont1.Contact_Gender__c = 'Male';         
        insert cont1 ;      
                
        Contact_Visit_Report__c cvr1 = new Contact_Visit_Report__c ();
        
        Call_Records__c vr1 = new Call_Records__c(); 
        //vr1.Type__c = '1:1 visit' ; 
        vr1.Call_Date__c = system.today(); 
        insert vr1 ;    
            
        Event evt1 = new Event();
        evt1.ActivityDate = system.today(); 
        evt1.WhatId = vr1.Id ; 
        evt1.StartDateTime = System.now() ; 
        evt1.EndDateTime = System.now() + 3 ;
        evt1.whoId = cont1.Id ;
        insert evt1 ;   
            
        Map<String,String> dataMap = new Map<String, String>{};
        dataMap.put('eventid', evt1.Id); 
        dataMap.put('id', vr1.Id); 
        dataMap.put('contacSelectedId', cont1.Id );     
        
        ApexPages.Standardcontroller sct = new ApexPages.Standardcontroller(vr1);
        controllerVisitReport  controller = new controllerVisitReport(sct);
        controller.dataMap = dataMap ;
        controller.vr = vr1 ;  
        controller.InitVR();        
        
        controller.getisUpdateMode();
        controller.getisNewMode();
        controller.setisNewMode(true); 
        controller.getcanSendTask(); 
        controller.getTask();
        
        controller.changeisNewMode();
        
        controller.save();
        controller.sendEmail();
        controller.doAddReminder();
        
        Test.stopTest();
        System.debug(' ################## ' + 'END TEST_controllerVisitReport' + ' ################');
    }
}