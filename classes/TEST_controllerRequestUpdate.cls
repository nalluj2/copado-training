/**
 * Creation Date :  20090213
 * Description :    Test Coverage of the controller 'Request Deletion' (Button in Contact Object).
 *                  (To facilitate the development of robust, error-free code, Apex Code requires the creation and execution of unit tests.
 *                  Unit tests are comprised of test methods and classes that verify whether a particular piece of code is working properly.)
 * Author :         ABSI - MC
 *
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */

@isTest
private class TEST_controllerRequestUpdate {

    static TestMethod void testController(){
        /* This Test Method will cover the controler : controllerRequestUpdate (On contact page called by the Request Update button) */
        System.debug(' ################## ' + 'BEGIN TEST_controllerRequestUpdate' + ' ################');      
        /* The controller is in the Contact Page, to create a contact, an account is required */
        Account acc1 = new Account();
        acc1.Name = 'TEST_controllerRequestUpdate Test Coverage Account 1 ' ; 
        insert acc1 ;       
        
        Contact cont1 = new Contact();
        cont1.LastName = 'TEST_controllerRequestUpdate Test Coverage' ;  
        cont1.FirstName = 'Test Contact';
        cont1.AccountId = acc1.Id ; 
        cont1.Contact_Update_Reason__c= null ; 
	    cont1.Contact_Department__c = 'Diabetes Adult'; 
	    cont1.Contact_Primary_Specialty__c = 'ENT';
	    cont1.Affiliation_To_Account__c = 'Employee';
	    cont1.Primary_Job_Title_vs__c = 'Manager';
	    cont1.Contact_Gender__c = 'Male'; 
        insert cont1 ;      
        
        //System.assertEquals('Test Coverage', [select LastName from Contact where id =:cont1.id].LastName);

        /* Creation of an instance of the controller to be able to call     */  
        ApexPages.StandardController sct = new ApexPages.StandardController(cont1); //set up the standardcontroller     
        controllerRequestUpdate  controller = new controllerRequestUpdate(sct);
        
        controller.setcontact(cont1);
        Contact cont2 = controller.getContact();
        System.assertEquals(cont1, cont2);
        
        controller.setCanCloseWindow(true);
        controller.setcanclosewindow(false);
        Boolean resB = controller.getCanCloseWindow();
        System.assertEquals(resB,false);
        
        //<!> Main Method : 
        controller.Send();
                
        // Changement of a var :
        cont1.Contact_Update_Reason__c= 'Request Update' ; 
        update cont1 ;        
        controller.Send();
                
        /* DML Exception coverage : By deleting the contact, it will show up a DML exception */ 
        List<Affiliation__c> lstAff = [Select i.id from Affiliation__c i where i.Affiliation_From_Contact__c =:cont1.Id or i.Affiliation_To_Contact__c =: cont1.Id];       
        Delete lstAff;
                
        //delete cont1 ;
        controller.Send();
        
        System.debug(' ################## ' + 'END TEST_controllerRequestUpdate' + ' ################');        
            
    } 
    
}