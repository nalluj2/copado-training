//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 03-08-2018
//  Description      : APEX Class - Business Logic for tr_ChangeRequest
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public class bl_ChangeRequest_Trigger  {


	//----------------------------------------------------------------------------------------------------------------
	// Calculate the Total Cost on Change Request Bundle which is the sum of the Total Cost of each relate Change Request.
	//	The Total Cost of the relate Change Request will be converted to the Currency of the Change Request Bundle.
	//----------------------------------------------------------------------------------------------------------------
	public static void calculateTotalCostOnBundle(List<Change_Request__c> lstChangeRequest, Map<Id, Change_Request__c> mapChangeRequest){
	
		Set<Id> setID_ChangeRequestBundle = new Set<Id>();

		if (mapChangeRequest == null){

			// INSERT - DELETE - UNDELETE
			// Get the Change Request Bundle of the processing Change Request if the Total_Cost__c is different from 0 (or null) and perform calculation of the Cost on all Change Requests of each collected Change Request Bundle
			for (Change_Request__c oChangeRequest : lstChangeRequest){

				if (oChangeRequest.Change_Request_Bundle__c != null){

					if ( (oChangeRequest.Total_Cost__c != null) && (oChangeRequest.Total_Cost__c != 0) ){
	
						if (oChangeRequest.Change_Request_Bundle__c != null){
							setID_ChangeRequestBundle.add(oChangeRequest.Change_Request_Bundle__c);
						}
	
					}

				}

			}

		}else{

			// UPDATE
			// Get the Change Request Bundle of the processing Change Request if the Total_Cost__c or the Currency ISO Code is changed and perform calculation of the Cost on all Change Requests of each collected Change Request Bundle
			// If the Change Request Bundle on the Change Request is updated, we need to process the old and new Change Request Bundle.
			for (Change_Request__c oChangeRequest : lstChangeRequest){

				if (
					(oChangeRequest.Total_Cost__c != mapChangeRequest.get(oChangeRequest.Id).Total_Cost__c)
					|| (oChangeRequest.CurrencyIsoCode != mapChangeRequest.get(oChangeRequest.Id).CurrencyIsoCode)
					|| (oChangeRequest.Status__c != mapChangeRequest.get(oChangeRequest.Id).Status__c)
				){

					if (oChangeRequest.Change_Request_Bundle__c != null){
						setID_ChangeRequestBundle.add(oChangeRequest.Change_Request_Bundle__c);
					}

				}

				if (oChangeRequest.Change_Request_Bundle__c != mapChangeRequest.get(oChangeRequest.Id).Change_Request_Bundle__c){

					if (oChangeRequest.Change_Request_Bundle__c != null){
						setID_ChangeRequestBundle.add(oChangeRequest.Change_Request_Bundle__c);
					}
					if (mapChangeRequest.get(oChangeRequest.Id).Change_Request_Bundle__c != null){
						setID_ChangeRequestBundle.add(mapChangeRequest.get(oChangeRequest.Id).Change_Request_Bundle__c);
					}

				}

			}
					
		}

		if (setID_ChangeRequestBundle.size() == 0) return;

		bl_ChangeRequest.calculateTotalCostOnBundle(setID_ChangeRequestBundle, true);

	}
	//----------------------------------------------------------------------------------------------------------------

}
//--------------------------------------------------------------------------------------------------------------------