@isTest 
private class TEST_ctrl_massSendEmail_WOSP_ReturnOrder {

	@testSetup static void createTestData() {

		//----------------------------------------------------------------
		// Create Test Data
		//----------------------------------------------------------------
		// Custom Setting
		clsTestData_CustomSetting.createCustomSettingData();

		// DIB_Country__c
		clsTestData_MasterData.createCountry();
		List<DIB_Country__c> lstCountry_Update = new List<DIB_Country__c>();
		DIB_Country__c oCountry_BE = clsTestData_MasterData.mapCountry.get('BE');
			oCountry_BE.EmaiL_Service_Repair__c = 'belgium01@medtronic.com.test;belgium02@medtronic.com.test';
			oCountry_BE.EmaiL_Inside_Sales__c = 'belgium01@medtronic.com.test;belgium02@medtronic.com.test';
		lstCountry_Update.add(oCountry_BE);
		DIB_Country__c oCountry_NL = clsTestData_MasterData.mapCountry.get('NL');
			oCountry_NL.EmaiL_Service_Repair__c = 'netherlands01@medtronic.com.test';
			oCountry_NL.EmaiL_Inside_Sales__c = 'belgium01@medtronic.com.test;belgium02@medtronic.com.test';
		lstCountry_Update.add(oCountry_NL);
		update lstCountry_Update;

		// Product
		List<Product2> lstProduct = new List<Product2>();
		Product2 oProduct1 = new Product2();
			oProduct1.Name = 'PoleStar1';
			oProduct1.ProductCode = '000999';
			oProduct1.isActive = true;					
		lstProduct.add(oProduct1);
		Product2 oProduct2 = new Product2();
			oProduct2.Name = 'PoleStar';
			oProduct2.ProductCode = '009999';
			oProduct2.isActive = true;					
		lstProduct.add(oProduct2);
		insert lstProduct;

		// Account
		clsTestData_Account.iRecord_Account = 1;
		clsTestData_Account.idRecordType_Account = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non-SAP Account').getRecordTypeId();
		List<Account> lstAccount = clsTestData_Account.createAccount(false);
		for (Account oAccount : lstAccount){
			oAccount.AccountNumber = '0000000';
			oAccount.Name = 'Test Account';
			oAccount.Phone = '+123456789';
			oAccount.BillingCity = 'TEST CITY';
			oAccount.BillingCountry = 'BELGIUM';
			oAccount.BillingState = 'TEST STATE';
			oAccount.BillingStreet = 'TEST STREET';
			oAccount.BillingPostalCode = '123456';
			oAccount.ST_NAV_Non_SAP_Account__c = true;
		}
		insert lstAccount;
		Account oAccount = lstAccount[0];

		// Contact
		clsTestData_Contact.iRecord_Contact = 2;
		List<Contact> lstContact = clsTestData_Contact.createContact();

		// Asset
		List<Asset> lstAsset = new List<Asset>();
		Asset oAsset1 = new Asset();
			oAsset1.AccountId = oAccount.Id;
			oAsset1.Product2Id = oProduct1.Id;
			oAsset1.Asset_Product_Type__c = 'S8 Planning Station';
			oAsset1.Ownership_Status__c = 'Purchased';
			oAsset1.Name = '123456789';
			oAsset1.Serial_Nr__c = '123456789';
			oAsset1.Status = 'Installed';
			oAsset1.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('PoleStar').getRecordTypeId();
		lstAsset.add(oAsset1);
		Asset oAsset2 = new Asset();
			oAsset2.AccountId = oAccount.Id;
			oAsset2.Product2Id = oProduct2.Id;
			oAsset2.Asset_Product_Type__c = 'PoleStar N-10';
			oAsset2.Ownership_Status__c = 'Purchased';
			oAsset2.Name = '987654321';
			oAsset2.Serial_Nr__c = '987654321';
			oAsset2.Status = 'Installed';
			oAsset2.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('PoleStar').getRecordTypeId();
		lstAsset.add(oAsset2);
		insert lstAsset;
				
		// Case
		List<Case> lstCase = new List<Case>();
		Case oCase1 = new Case();
			oCase1.AccountId = oAccount.Id;
			oCase1.AssetId = oAsset1.Id;
			oCase1.RecordTypeId = clsUtil.getRecordTypeByDevName('Case', 'New_Case').Id;
			oCase1.External_Id__c = 'Test_Case_Id_1';
		lstCase.add(oCase1);
		insert lstCase;
	
		// Workorder
		List<Workorder__c> lstWorkorder = new List<Workorder__c>();
		Workorder__c oWorkorder = new Workorder__c();
			oWorkorder.Account__c = oAccount.Id;
			oWorkorder.Asset__c = oAsset1.Id;
			oWorkorder.Status__c = 'In Process';
			oWorkorder.External_Id__c = 'Test_Work_Order_Id_1';
			oWorkorder.Case__c = oCase1.Id;
			oWorkorder.RecordTypeId = Schema.SObjectType.Workorder__c.getRecordTypeInfosByName().get('PoleStar System Checkout').getRecordTypeId();
		lstWorkorder.add(oWorkorder);
		insert lstWorkorder;
		
		// Workorder Sparepart
		List<Workorder_Sparepart__c> lstWorkorderSparepart = new List<Workorder_Sparepart__c>();
		Workorder_Sparepart__c oWorkorderSparepart1 = new Workorder_Sparepart__c();
			oWorkorderSparepart1.Case__c = oCase1.Id;
			oWorkorderSparepart1.Account__c = oAccount.Id;
			oWorkorderSparepart1.Asset__c = oAsset1.Id;
			oWorkorderSparepart1.External_Id__c = 'Test_SparePart_Id_1';		
			oWorkorderSparepart1.Amount_of_Inaccuracy__c = 1000;
			oWorkorderSparepart1.RecordTypeId = Schema.SObjectType.Workorder_Sparepart__c.getRecordTypeInfosByName().get('Inaccuracy').getRecordTypeId();
			oWorkorderSparepart1.Return_Item_Status__c = 'Part Not Returned';
			oWorkorderSparepart1.Order_Status__c = 'Shipped';
			oWorkorderSparepart1.Order_Lot_Number__c = '0011223344';
			oWorkorderSparepart1.Workorder__c = oWorkorder.Id;
		lstWorkorderSparepart.add(oWorkorderSparepart1);
		Workorder_Sparepart__c oWorkorderSparepart2 = new Workorder_Sparepart__c();
			oWorkorderSparepart2.Case__c = oCase1.Id;
			oWorkorderSparepart2.Account__c = oAccount.Id;
			oWorkorderSparepart2.Asset__c = oAsset2.Id;
			oWorkorderSparepart2.External_Id__c = 'Test_SparePart_Id_2';		
			oWorkorderSparepart2.Amount_of_Inaccuracy__c = 1000;
			oWorkorderSparepart2.RecordTypeId = Schema.SObjectType.Workorder_Sparepart__c.getRecordTypeInfosByName().get('Inaccuracy').getRecordTypeId();
			oWorkorderSparepart2.Return_Item_Status__c = 'Part Not Returned';
			oWorkorderSparepart2.Order_Status__c = 'Shipped';
			oWorkorderSparepart2.Order_Lot_Number__c = '4433221100';
			oWorkorderSparepart2.Workorder__c = oWorkorder.Id;
		lstWorkorderSparepart.add(oWorkorderSparepart2);		
		insert lstWorkorderSparepart;		
		//----------------------------------------------------------------

	}
	//--------------------------------------------------------------------



	//--------------------------------------------------------------------
	// TEST Related to Case - No Error
	//--------------------------------------------------------------------
	@isTest static void test_ctrl_massSendEmail_WOSP_ReturnOrder() {

		Boolean bTest = false;
		String tTest = '';

		//----------------------------------------------------------------
		// Load Test Data
		//----------------------------------------------------------------
		List<Case> lstCase = 
			[
				SELECT 
					Id, CaseNumber
		            , ShippingStreet__c
		            , ShippingPostalCode__c
		            , ShippingCity__c
		            , ShippingState__c
		            , ShippingCountry__c
		            , Account.BillingStreet
		            , Account.BillingPostalCode
		            , Account.BillingCity
		            , Account.BillingState
		            , Account.BillingCountry
				FROM 
					Case 
			];
		System.assertEquals(lstCase.size(), 1);
		Case oCase = lstCase[0];

		List<User> lstUser = [SELECT Id, Email FROM User WHERE IsActive = true AND License_Name__c = 'Salesforce' AND Id != :UserInfo.getUserId() LIMIT 2];
		List<Contact> lstContact = [SELECT Id, Name FROM Contact];

        List<OrgWideEmailAddress> lstOrgWideEmailAddress = [SELECT Id, DisplayName, Address FROM OrgWideEmailAddress ORDER BY DisplayName];
		List<Apexpages.Message> lstAPEXPageMessage = new List<Apexpages.Message>();
		//----------------------------------------------------------------


		//----------------------------------------------------------------
		// Perform Test
		//----------------------------------------------------------------
		Test.startTest();

		PageReference oPageRef = Page.massSendEmail_WOSP_ReturnOrder;
        Test.setCurrentPage(oPageRef);

	        ApexPages.currentPage().getParameters().put('id', oCase.Id);
	        ApexPages.currentPage().getParameters().put('emailtemplatefixed', 'true');
			ApexPages.currentPage().getParameters().put('additionalemailto', 'test@medtronic.com.test');
			ApexPages.currentPage().getParameters().put('additionalemailcc', 'test@medtronic.com.test');
			ApexPages.currentPage().getParameters().put('additionalemailbcc', 'test@medtronic.com.test');

		ctrl_massSendEmail_WOSP_ReturnOrder oCTRL = new ctrl_massSendEmail_WOSP_ReturnOrder();

		List<SelectOption> lstSO_Country = oCTRL.populateCountryEmail();
		System.assertEquals(lstSO_Country.size(), 2);
		for(SelectOption oSO_Country : lstSO_Country) System.debug('**BC** lstSO_Country values 1 : ' + oSO_Country.getValue() + ' - ' + oSO_Country.getLabel());

			oCase.ShippingCountry__c = 'Belgium';
		update oCase;

		lstCase = 
			[
				SELECT 
					Id, CaseNumber
		            , ShippingStreet__c
		            , ShippingPostalCode__c
		            , ShippingCity__c
		            , ShippingState__c
		            , ShippingCountry__c
		            , Account.BillingStreet
		            , Account.BillingPostalCode
		            , Account.BillingCity
		            , Account.BillingState
		            , Account.BillingCountry
				FROM 
					Case 
			];
		System.assertEquals(lstCase.size(), 1);
		oCase = lstCase[0];


		lstSO_Country = oCTRL.populateCountryEmail();
		System.assertEquals(lstSO_Country.size(), 2);
		for(SelectOption oSO_Country : lstSO_Country) System.debug('**BC** lstSO_Country values 2 : ' + oSO_Country.getValue() + ' - ' + oSO_Country.getLabel());

		oCTRL.mapCountry_EmailAddresses = new Map<String, List<String>>();
			oCTRL.mapCountry_EmailAddresses.put('BELGIUM', new List<String>{'belgium01@medtronic.com.test', 'belgium02@medtronic.com.test'});
			oCTRL.mapCountry_EmailAddresses.put('POLAND', new List<String>{'poland01@medtronic.com.test'});
		
		oCTRL.mapEmailAddresses_Country = new Map<String, List<String>>();
			oCTRL.mapEmailAddresses_Country.put('belgium01@medtronic.com.test', new List<String>{'BELGIUM'});
			oCTRL.mapEmailAddresses_Country.put('belgium02@medtronic.com.test', new List<String>{'BELGIUM'});
			oCTRL.mapEmailAddresses_Country.put('poland01@medtronic.com.test', new List<String>{'POLAND'});			
			
		oCTRL.mapCountry_EmailAddresses_IS = new Map<String, List<String>>();
			oCTRL.mapCountry_EmailAddresses_IS.put('BELGIUM', new List<String>{'belgium01@medtronic.com.test', 'belgium02@medtronic.com.test'});
			oCTRL.mapCountry_EmailAddresses_IS.put('POLAND', new List<String>{'poland01@medtronic.com.test'});
		


		//--------------------------------------------
		// ACTIONS
		//--------------------------------------------
		oCTRL.tCountryEmail = 'belgium01@medtronic.com.test';


		//--------------------------------------------
		// ACTION - processWOSP
		//--------------------------------------------
		System.assertEquals(oCTRL.lstwrWOSP.size(), 2);


		clsUtil.tExceptionName = 'processWOSP_EXCEPTION';
		oCTRL.processWOSP();
		lstAPEXPageMessage = ApexPages.getMessages();
		System.assert(lstAPEXPageMessage.size() > 0);
		bTest = false;
		for (Apexpages.Message oMessage : lstAPEXPageMessage){
    		if (oMessage.getDetail().contains('processWOSP_EXCEPTION')) bTest = true;
		}
		System.assert(bTest);
		clsUtil.tExceptionName = '';


		oCTRL.lstId_CC.add(lstContact[0].Id);
		oCTRL.lstId_CC.add(lstContact[1].Id);
		oCTRL.id_EmailFrom = UserInfo.getUserId();	// To make sure we don't use Org Wide Email address as from because the running user might not have access to it

		oCTRL.owrCase.tPickupAddress = '';

		Test.clearApexPageMessages();
		oCTRL.processWOSP();

		lstAPEXPageMessage = ApexPages.getMessages();
		System.assert(lstAPEXPageMessage.size() > 0);
		Integer iError = 0;
		for (Apexpages.Message oMessage : lstAPEXPageMessage){
    		if (oMessage.getDetail().contains('Please select 1 or more WOSP records to process')) iError++;
    		if (oMessage.getDetail().contains('Please provide a "Pick-up Address"')) iError++;
    		if (oMessage.getDetail().contains('Please provide a "Pick-up Department"')) iError++;
    		if (oMessage.getDetail().contains('Please provide a "Pick-up Contact Name')) iError++;
		}
		System.assertEquals(iError, 4, lstAPEXPageMessage.size());

		Test.clearApexPageMessages();

		oCTRL.owrCase.tPickupAddress = 'TEST';
		oCTRL.owrCase.tPickupDepartment = 'TEST';
		oCTRL.owrCase.tPickupContactName = 'TEST';

		System.debug('**BC** oCTRL.owrCase 1 : ' + oCTRL.owrCase);
		ctrl_massSendEmail_WOSP_ReturnOrder.wrWOSP oWRWOSP1 = oCTRL.lstwrWOSP[0];
			oWRWOSP1.bSelected = true;
			oWRWOSP1.oWOSP.Return_Item_Status__c = null;

		System.assertEquals(oWRWOSP1.bDisplay_WOSP, true);
		System.assertEquals(oWRWOSP1.bDisplay_Error, false);

		System.debug('**BC** oCTRL.owrCase 2 : ' + oCTRL.owrCase);

		Test.clearApexPageMessages();
		oCTRL.processWOSP();
		System.debug('**BC** oCTRL.owrCase 3 : ' + oCTRL.owrCase);
		lstAPEXPageMessage = ApexPages.getMessages();

		System.debug('**BC** lstAPEXPageMessage (' + lstAPEXPageMessage.size() + ') : ' + lstAPEXPageMessage);
		System.assertEquals(0, lstAPEXPageMessage.size());

		Test.clearApexPageMessages();
		oCTRL.processWOSP();
		System.assertEquals(oWRWOSP1.bDisplay_Error, true);
		System.assertEquals(oWRWOSP1.lstErrorMessage.size(), 4);

		oWRWOSP1.oWOSP.RI_Part_Quantity__c = 1;

		Test.clearApexPageMessages();
		oCTRL.processWOSP();
		System.assertEquals(oWRWOSP1.bDisplay_Error, true);
		System.assertEquals(oCTRL.lstwrWOSP[0].lstErrorMessage.size(), 3);

		oWRWOSP1.oWOSP.Lot_Number__c = 'test';

		Test.clearApexPageMessages();
		oCTRL.processWOSP();
		System.assertEquals(oWRWOSP1.bDisplay_Error, true);
		System.assertEquals(oCTRL.lstwrWOSP[0].lstErrorMessage.size(), 2);

		oWRWOSP1.oWOSP.SAP_Return__c = 'test';

		Test.clearApexPageMessages();
		oCTRL.processWOSP();
		System.assertEquals(oWRWOSP1.bDisplay_Error, true);
		System.assertEquals(oCTRL.lstwrWOSP[0].lstErrorMessage.size(), 1);

		oWRWOSP1.oWOSP.Return_Item_Status__c = 'Ready for pick-up';

		Test.clearApexPageMessages();
		oCTRL.processWOSP();

		System.debug('**BC** oCTRL.lstwrWOSP[0].lstErrorMessage (' + oCTRL.lstwrWOSP[0].lstErrorMessage.size() + ') : ' + oCTRL.lstwrWOSP[0].lstErrorMessage);
		System.assertEquals(oWRWOSP1.bDisplay_Error, true);
		System.assertEquals(oCTRL.lstwrWOSP[0].lstErrorMessage.size(), 1);

		oWRWOSP1.oWOSP.Return_Item_Status__c = 'Ready for pick-up';
		update oWRWOSP1.oWOSP;
		oWRWOSP1.tAdditionalInformation = 'TEST';
		oCTRL.owrCase.tAdditionalInformation = 'TEST';
		Test.clearApexPageMessages();
		oCTRL.processWOSP();

		System.assertEquals(oWRWOSP1.bDisplay_Error, false);
		System.assertEquals(oCTRL.lstwrWOSP[0].lstErrorMessage.size(), 0);


		lstAPEXPageMessage = ApexPages.getMessages();

		System.assert(lstAPEXPageMessage.size() > 0);
		bTest = false;
		for (Apexpages.Message oMessage : lstAPEXPageMessage){
			System.debug('**BC** test_ctrl_massSendEmail_WOSP_ReturnOrder - oMessage.getDetail() : ' + oMessage.getDetail());
    		if (oMessage.getDetail().contains('The selected records are process successfully')) bTest = true;
		}
		System.assert(bTest);

		oCTRL.backToRecord();

		Test.stopTest();
		//----------------------------------------------------------------


		//----------------------------------------------------------------
		// Validate Test
		//----------------------------------------------------------------
		//----------------------------------------------------------------

	}
	//--------------------------------------------------------------------

}
//------------------------------------------------------------------------------------------------------------