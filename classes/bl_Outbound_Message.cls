public class bl_Outbound_Message {
    
    public static void populateNotificationGrouping(List<Outbound_Message__c> triggerNew){
    	
    	Map<String, Notification_Grouping__c> groupingMap = new Map<String, Notification_Grouping__c>();
    	
    	for(Outbound_Message__c om : triggerNew){
    		
    		if(om.Operation__c == 'upsertServiceNotification'){
    			
    			Notification_Grouping__c grouping = new Notification_Grouping__c();
    			grouping.External_ID__c = om.External_ID__c;
    			grouping.Scope__c = 'Case';
    			grouping.Unique_Key__c = 'Case:' + om.External_ID__c;
    			
    			groupingMap.put(grouping.Unique_Key__c, grouping);
    			 
    		}else if(om.Operation__c == 'upsertServiceOrder'){
    			
    			Notification_Grouping__c grouping = new Notification_Grouping__c();
    			grouping.External_ID__c = om.External_ID__c;
    			grouping.Scope__c = 'SVMXC__Service_Order__c';
    			grouping.Unique_Key__c = 'SVMXC__Service_Order__c:' + om.External_ID__c;
    			
    			groupingMap.put(grouping.Unique_Key__c, grouping);    			
    		}
    	}
    	
    	if(groupingMap.isEmpty()) return;
    	   	
    	for(Notification_Grouping__c existingGrouping : [Select Id, Unique_Key__c from Notification_Grouping__c where Unique_Key__c IN :groupingMap.keySet()]){
    		
    		Notification_Grouping__c grouping = groupingMap.get(existingGrouping.Unique_Key__c);
    		grouping.Id = existingGrouping.Id;
    	}
    	
    	List<Notification_Grouping__c> toInsert = new List<Notification_Grouping__c>();
    	
    	for(Notification_Grouping__c grouping : groupingMap.values()){
    		
    		if(grouping.Id == null) toInsert.add(grouping);
    	}
    	
    	if(toInsert.size() > 0) insert toInsert;
    	
    	for(Outbound_Message__c om : triggerNew){
    		
    		if(om.Operation__c == 'upsertServiceNotification'){
    			
    			String key = 'Case:' + om.External_ID__c;
    			Notification_Grouping__c grouping = groupingMap.get(key);
    			
    			om.Notification_Grouping__c = grouping.Id;   			
    			 
    		}else if(om.Operation__c == 'upsertServiceOrder'){
    			
    			String key = 'SVMXC__Service_Order__c:' + om.External_ID__c;
    			Notification_Grouping__c grouping = groupingMap.get(key);
    			
    			om.Notification_Grouping__c = grouping.Id; 			
    		}
    	}
    }
    
    public static void updateNotificationGrouping(List<Outbound_Message__c> triggerNew){
    	
    	Map<Id, Notification_Grouping__c> groupingMap = new Map<Id, Notification_Grouping__c>();
    	
    	for(Outbound_Message__c om : triggerNew){
    		
    		if(om.Notification_Grouping__c != null && groupingMap.containsKey(om.Notification_Grouping__c) == false){
    			
    			Notification_Grouping__c grouping = new Notification_Grouping__c(Id = om.Notification_Grouping__c);
    			groupingMap.put(om.Notification_Grouping__c, grouping);    			    			 
    		}
    	}
    	
    	if(groupingMap.size() > 0) update groupingMap.values();
    }
}