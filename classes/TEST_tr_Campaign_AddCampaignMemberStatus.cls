/*
 *      Created Date    : 2014-04-25
 *      Author          : Bart Caelen
 *      Description     : TEST Class for the APEX Trigger tr_Campaign_AddCampaignMemberStatus
 */
@isTest
private class TEST_tr_Campaign_AddCampaignMemberStatus {
    
	static Integer iDefaultNumberOfCampaignMemberStatus = 0;	// The default Campaign Member Statuses are not stored in the CampaignMemberStatus sObject --> therefor the default number of Campaign Members Statuses id 0


    @isTest static void test_method_NoAdditionalCampaignMemberStatus() {

        Id idRecordType = RecordTypeMedtronic.getRecordTypeByDevName('Campaign', 'CVent').Id;

        Test.startTest();

        clsTestData.createCampaignData(true, idRecordType);
        Id idCampaign1 = clsTestData.oMain_Campaign.Id;

        Test.stopTest();

        List<CampaignMemberStatus> lstCampaignMemberStatus = 
            [
                SELECT 
                    Id, CampaignId, HasResponded, IsDefault, IsDeleted, Label, SortOrder 
                FROM 
                    CampaignMemberStatus
                WHERE
                    CampaignId = :idCampaign1
            ];

        system.assertEquals(lstCampaignMemberStatus.size(), iDefaultNumberOfCampaignMemberStatus);

    }
    
    @isTest static void test_method_AdditionalCampaignMemberStatus() {

        Id idRecordType = RecordTypeMedtronic.getRecordTypeByDevName('Campaign', 'CVent').Id;

        Test.startTest();

        clsTestData.createCampaignMemberStatusData(true, 'RecordTypeID', idRecordType);
        Integer iAdditionalNumberOfCampaignMemberStatus = clsTestData.lstCampaignMemberStatus.size();

        system.assertNotEquals(iAdditionalNumberOfCampaignMemberStatus, 0);

        clsTestData.oMain_Campaign = null;
        clsTestData.createCampaignData(true, idRecordType);
        Id idCampaign2 = clsTestData.oMain_Campaign.Id;

        Test.stopTest();

        List<CampaignMemberStatus> lstCampaignMemberStatus = 
            [
                SELECT 
                    Id, CampaignId, HasResponded, IsDefault, IsDeleted, Label, SortOrder 
                FROM 
                    CampaignMemberStatus
                WHERE
                    CampaignId = :idCampaign2
            ];

        system.assertEquals(lstCampaignMemberStatus.size(), iDefaultNumberOfCampaignMemberStatus + iAdditionalNumberOfCampaignMemberStatus);
        //------------------------------------------------
    }
    
}