@isTest
    Public Class testCasePartLink{

    static testMethod void myTest()
  	{
        Account acc=new Account();
		acc.AccountNumber='as';
		acc.Name='MNav House Account';
		acc.Account_Country_vs__c='USA';
		acc.CurrencyIsoCode='USD';
		acc.Phone='24107954';
		insert acc; 
       
       	//Exlude execution of OMA triggers
    	clsUtil.bDoNotExecute = true;
    	
    	List<Case> cases = new List<Case>();
    	
        Case cse=new Case();
        cases.add(cse);
        
        Case cse2=new Case();
        cases.add(cse2);
        
        insert cases;
        
       	Product2 pro=new Product2();
       	pro.Name='abc';
       	pro.Tracking__c = TRUE ;
       	pro.Maintenance_Frequency__c= 12;
       	insert pro;
       
       	Contact objContact=new Contact();
       	objContact.LastName='Saha';
		objContact.FirstName = 'TEST';
       	objContact.AccountId = acc.id;       
       	objContact.Contact_Department__c = 'Pediatric';
	    objContact.Contact_Primary_Specialty__c = 'Neurology';
	    objContact.Affiliation_To_Account__c = 'Employee';
	    objContact.Primary_Job_Title_vs__c = 'Nurse';
	    objContact.Contact_Gender__c = 'Female';
       	insert objContact;
        
       	Asset sys=new Asset();
        sys.Name='Niha';
        sys.Serial_Nr__c='XXXXX';
        sys.recordtypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('O-Arm').getRecordTypeId(); 
        sys.Asset_Product_Type__c='O-Arm 1000';
        sys.AccountId=acc.Id;
        sys.Ownership_Status__c='EOL';	
		insert sys;
        
       	Complaint__c objcomplaint=new Complaint__c();
       	objcomplaint.Status__c='Open';
       	objcomplaint.Account_Name__c=acc.id;
	    objcomplaint.Asset__c=sys.id;
    	objcomplaint.Medtronic_Aware_Date__c = system.today();
       	objcomplaint.Formal_Investigation_Required__c='No';
       	objcomplaint.Formal_Investigation_Justification__c='abc';
       	objcomplaint.Formal_Investigation_Reference__c ='abc';
       	objcomplaint.Methods__c='abc';
       	objcomplaint.Results__c='abcc';
       	objcomplaint.Case__c=cse.id;
       	insert objcomplaint;
   		
   		Test.startTest();
   		
        Workorder__c WO =new Workorder__c();
        WO.Account__c=acc.id;
        WO.Asset__c=sys.id;
        WO.complaint__c = objcomplaint.Id;
        WO.CurrencyIsoCode='EUR';
        WO.Bio_Med_FSE_Visit__c='No';
        WO.Date_Completed__c=system.today();
        WO.Status__c='In Process';
        WO.Case__c=cse2.id;
        insert WO;
        
        Parts_Shipment__c objPartShipment =  new Parts_Shipment__c();
        objPartShipment.Account__c = acc.id;
        objPartShipment.Asset__c = sys.id;
        objPartShipment.complaint__c = objcomplaint.Id;
        objPartShipment.Workorder__c = WO.id;
        objPartShipment.CurrencyIsoCode ='EUR';
        objPartShipment.Ship_Via__c = 'Eagle';
        insert objPartShipment;
        
        Parts_Shipment__c objPartShipment1 =  new Parts_Shipment__c();
        objPartShipment1.Account__c = acc.id;
        objPartShipment1.Asset__c = sys.id;
       	objPartShipment1.Case__c=cse.id;        
        objPartShipment1.complaint__c = objcomplaint.Id;
        objPartShipment1.CurrencyIsoCode ='EUR';
        objPartShipment1.Ship_Via__c = 'Eagle';
        insert objPartShipment1;        
        
      	//Recordtype Objrecordtype2=[Select id,DeveloperName from Recordtype Where DeveloperName='Software' and SObjectType = 'Workorder_Sparepart__c' ]; 
       	Workorder_Sparepart__c objCPart= new Workorder_Sparepart__c();
       	objCPart.Owner__c = objContact.id;
       	objCPart.Order_Part__c = pro.id;
       	objCPart.Order_Status__c = 'Pending';
       	objCPart.RI_Part__c = pro.id;       
       	objCPart.Return_Item_Status__c = 'Pending Return';
       	objCPart.Workorder__c=WO.id;
       	objCPart.Complaint__c=objcomplaint.id;
       	objCPart.Parts_Shipment__c=objPartShipment.Id ;
       	insert objCPart;	

   	   	objPartShipment.complaint__c = null;
       	update objPartShipment ;

       	objCPart.Workorder__c=null;
       	objCPart.Parts_Shipment__c=objPartShipment.Id ;       
       	update objCPart;

       	objCPart.Parts_Shipment__c=objPartShipment1.Id ; 
       	objCPart.Case__c=cse.Id ;      
       	objCPart.Complaint__c=null ;   
       	update objCPart;

	   	Technical_Ids__c objObjectIds = new Technical_Ids__c(key__c='Test1', Name='Test1', Primary_Id__c = 'Test1');
	   	insert objObjectIds;

       	ApexPages.StandardController controller = new ApexPages.StandardController(WO);
       	ApexPages.currentPage().getParameters().put('id',WO.id);
       
       	CasePartLink testCasePartLink = new  CasePartLink(controller);
       	testCasePartLink.CasePartSelected() ;
       	testCasePartLink.CancelLink();
       	testCasePartLink.redirectToCasePart();
       
       	ApexPages.currentPage().getParameters().put('id',WO.id);
       //Test.setCurrentPageReference('/apex/ShipCAsePartLink?id='+WO.id);   
       	ApexPages.Standardcontroller std = new ApexPages.Standardcontroller(WO);
       	CasePartLink controller2 = new CasePartLink(std);       
       	controller2.redirectToCasePart();    
       
         
       	ApexPages.StandardController controller1 = new ApexPages.StandardController(objPartShipment);
       	ApexPages.currentPage().getParameters().put('id',objPartShipment.id);
       	CasePartLink testCasePartLink2 = new  CasePartLink(controller1);
       	testCasePartLink2.CasePartSelected() ;
       	testCasePartLink2.CancelLink();
       	testCasePartLink2.redirectToCasePart();
       
       	ApexPages.StandardController controller3 = new ApexPages.StandardController(objPartShipment);
       	ApexPages.currentPage().getParameters().put('id',cse.id);
       	CasePartLink testCasePartLink3 = new  CasePartLink(controller3);
       	testCasePartLink3.CasePartSelected() ;
       	testCasePartLink3.CancelLink();
       	testCasePartLink3.redirectToCasePart();
       
        ApexPages.StandardController controller4 = new ApexPages.StandardController(objPartShipment);
       	ApexPages.currentPage().getParameters().put('id',objcomplaint.id);
       	CasePartLink testCasePartLink4 = new  CasePartLink(controller4);
       	testCasePartLink4.CasePartSelected() ;
       	testCasePartLink4.CancelLink();
       	testCasePartLink4.redirectToCasePart();
	}
}