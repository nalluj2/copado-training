@isTest
public class testContract_Logic
{
    static testMethod void testContract_logicload()
    {
    
        Account acc=new Account();
        acc.AccountNumber='as';
        acc.Name='MNav House Account';
        acc.AccountNumber = '0001234567';
        acc.Account_Country_vs__c='USA';        
        acc.CurrencyIsoCode='USD';
        acc.Phone='24107954';
        insert acc; 
        
        Contract[] scList = new List<Contract>();
        
        Contract scr = new Contract();
        scr.AccountId = Acc.Id;
        scr.Contract_Type__c = 'MENT Agreement';
        scr.Name = 'Test12133';
        scr.StartDate = date.Today();
        scr.ContractTerm = 12;        
        scr.Anniversary__c=date.Today().addDays(10);
        scList.add(scr);
        
        Contract Scr1 = new Contract(
        AccountId =Acc.Id,
        StartDate = date.today(),        
        ContractTerm = 12,
        Name = '00000000',
        Contract_Type__c = 'Service Agreement',
        Approval_Status__c = 'Draft',
    	Anniversary__c=date.Today());
        scList.add(scr1);
        
        Contract Scr2 = new Contract(
        AccountId =Acc.Id,
        StartDate = date.today(),
        
        ContractTerm = 12,
        Name = '00000000',
        Contract_Type__c = 'Surgical Support Pack',
        Approval_Status__c = 'Activated',
        of_FSE_Visits_Used__c = null,
        of_Surgery_Coverages_Used__c = null,
    	Anniversary__c=date.Today(),
        entitlement_anniversary_check__c = true);
        scList.add(scr2);
        
        Contract Scr3 = new Contract(
        AccountId =Acc.Id,
        StartDate = date.today(),
        ContractTerm = 12,
        Name = '00000000',
        Contract_Type__c = 'Surgical Support Pack',
        Approval_Status__c = 'Activated',
        of_FSE_Visits_Used__c = null,
        of_Surgery_Coverages_Used__c = null,
    	Anniversary__c=null,
    	entitlement_anniversary_check__c = true);
        scList.add(scr3);
        
        Contract Scr4 = new Contract(
        AccountId =Acc.Id,
        Anniversary__c=null,
        StartDate = Date.today().addDays(-415),
        ContractTerm = 12,
        Name = '00000000',
        Contract_Type__c = 'Surgical Support Pack',
        Approval_Status__c = 'Activated',
        of_FSE_Visits_Used__c = null,
        of_Surgery_Coverages_Used__c = null,
    	entitlement_anniversary_check__c = true);
        scList.add(scr4);
        
        Contract Scr5 = new Contract(
        AccountId =Acc.Id,
        Anniversary__c=null,
        StartDate = Date.today(),
        ContractTerm = 12,
        Name = '00000000',
        Contract_Type__c = 'Surgical Support Pack',
        Approval_Status__c = 'Activated',
        of_FSE_Visits_Used__c = null,
        of_Surgery_Coverages_Used__c = null,
    	entitlement_anniversary_check__c = true);
        scList.add(scr5);
        
        insert scList;
        
        Id oarmRTId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('O-Arm').getRecordTypeId();                         
        Asset Sys = new Asset(Name='test',Asset_Product_Type__c='Gen II',AccountId=Acc.Id,Ownership_Status__c='EOL', recordTypeId = oarmRTId, Serial_Nr__c = 'XXXXX');
        insert Sys;
        
        Contract RelatedScr = [Select id,Contract_Type__c,startDate, endDate,ContractTerm from Contract where id =: scr.id];
        RelatedScr.StartDate = System.Today()+100;
        RelatedScr.ContractTerm=12;
        RelatedScr.Anniversary__c=System.Today();
        upsert RelatedScr;
        
        List<AssetContract__c> entlist=new List<AssetContract__c>();
        AssetContract__c ent =new AssetContract__c(
            Asset__c=Sys.Id,Contract__c=scList[0].Id,of_Surgery_Coverages__c=6,of_FSE_Visits__c=2);
        entlist.add(ent);
        AssetContract__c ent1 =new AssetContract__c(
            Asset__c=Sys.Id,Contract__c=scList[1].Id,of_Surgery_Coverages__c=6,of_FSE_Visits__c=2);
        entlist.add(ent1);
        AssetContract__c ent2 =new AssetContract__c(
            Asset__c=Sys.Id,Contract__c=scList[2].Id,of_Surgery_Coverages__c=6,of_FSE_Visits__c=2);
        entlist.add(ent2);
        insert entlist;
        
        Contract[] scLoad = new List<Contract>();
        scLoad = [SELECT id, Initial_FSE_Visits__c, Initial_Surgery_Coverages__c,Total_of_FSE_Visits_Purchased__c,
                  Total_of_Surgery_Coverages_Purchased__c,of_FSE_Visits_Used__c,of_Surgery_Coverages_Used__c,
                  FSE_Visits_Expired__c,Surgery_Coverages_Expired__c,Anniversary__c FROM Contract WHERE id in:scList];
                
        Contract_Logic.LoadContractData(scLoad);
             
    }
    
    private static testmethod void testContractCloneConnections(){
    	
    	Account acc=new Account();
        acc.AccountNumber='as';
        acc.Name='MNav House Account';
        acc.AccountNumber = '0001234567';
        acc.Account_Country_vs__c='USA';        
        acc.CurrencyIsoCode='USD';
        acc.Phone='24107954';
        insert acc; 
                
        Id oarmRT = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('O-Arm').getRecordTypeId();
         
        Asset testAsset = new Asset();
        testAsset.Serial_Nr__c = 'ZZZZ';
        testAsset.RecordTypeId = oarmRT;
        testAsset.Sold_as_Refurbished__c=true;
        testAsset.Name='Blr2';
        testAsset.Asset_Product_Type__c='O-Arm 1000';
        testAsset.AccountId=Acc.Id;
        testAsset.Ownership_Status__c='MSB O-arm Program';
        testAsset.CFN_Text__c= 'EM100-A';        
        insert testAsset;	
        
        Contract contr1 = new Contract();
        contr1.AccountId = acc.Id;
        contr1.Contract_Type__c = 'MENT Agreement';
        contr1.Name = 'Test12133';
        contr1.StartDate = date.Today();
        contr1.ContractTerm = 12;
        contr1.RecordTypeId = [Select Id from RecordType where SObjectType = 'Contract' AND DeveloperName = 'RTG_Service_Contract'].Id;
                
        Contract contr2 = new Contract();
        contr2.AccountId = acc.Id;
        contr2.Contract_Type__c = 'MENT Agreement';
        contr2.Name = 'Test12133';
        contr2.StartDate = date.Today().addMonths(-6);
        contr2.ContractTerm = 12;
        contr2.RecordTypeId = [Select Id from RecordType where SObjectType = 'Contract' AND DeveloperName = 'MITG_Contract'].Id;
                
        insert new List<Contract>{contr1, contr2};
        
        AssetContract__c assetConnection1 = new AssetContract__c();
        assetConnection1.Asset__c = testAsset.Id;
        assetConnection1.Contract__c = contr1.Id;
        assetConnection1.Entitlements__c = '100% Coverage for Labor';
        assetConnection1.PO_Amount2__c = 1000;
        assetConnection1.Service_Level__c = 'Gold';
        
        AssetContract__c assetConnection2 = new AssetContract__c();
        assetConnection2.Asset__c = testAsset.Id;
        assetConnection2.Contract__c = contr2.Id;
        assetConnection2.Entitlements__c = '100% Coverage for Labor';
        assetConnection2.PO_Amount2__c = 1000;
        assetConnection2.Service_Level__c = 'Gold';
        
        insert new List<AssetContract__c>{assetConnection1, assetConnection2};
        
        Attachment attachment1 = new Attachment();
		attachment1.ParentId = contr1.Id;
		attachment1.Name = 'Unit Test Attachment 1';
		attachment1.Description = 'Unit Test Attachment 1';
		attachment1.Body = Blob.valueOf('Unit Test Attachment 1');
		
		Attachment attachment2 = new Attachment();
		attachment2.ParentId = contr2.Id;
		attachment2.Name = 'Unit Test Attachment 2';
		attachment2.Description = 'Unit Test Attachment 2';
		attachment2.Body = Blob.valueOf('Unit Test Attachment 2');
		
		insert new List<Attachment>{attachment1, attachment2};
        
        Test.startTest();
        
        Contract clonedContr1 = contr1.clone(false);        
        clonedContr1.Clone_From__c = contr1.Id;        
        insert clonedContr1;
        
        List<AssetContract__c> clonedConnections = [Select Id from AssetContract__c where Contract__c = :clonedContr1.Id];        
        System.assert(clonedConnections.size() == 1);
        
        List<Attachment> clonedAtatchments = [Select Id from Attachment where ParentId = :clonedContr1.Id];        
        System.assert(clonedAtatchments.size() == 1);
        
        Contract clonedContr2 = contr2.clone(false);        
        clonedContr2.Clone_From__c = contr2.Id;        
        insert clonedContr2;
        
        clonedConnections = [Select Id from AssetContract__c where Contract__c = :clonedContr2.Id];        
        System.assert(clonedConnections.size() == 1);
        
        clonedAtatchments = [Select Id from Attachment where ParentId = :clonedContr2.Id];        
        System.assert(clonedAtatchments.size() == 0);
        
        Test.stopTest();
    }
}