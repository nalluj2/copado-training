//--------------------------------------------------------------------------------------------------------------------
//  Description : This TEST APEX Class is used to test the logic in ws_ChangeNotificationToMMX
//  Version     : 1.0
//  Author      : Bart Caelen
//  Date        : 14/09/2016
//--------------------------------------------------------------------------------------------------------------------
@isTest private class Test_ws_ChangeNotificationToMMX {
	
	@isTest static void test_ws_ChangeNotificationToMMX() {

		Test.startTest();

        // Set WebService Mock class 
        Test.setMock(WebServiceMock.class, new Test_ws_ChangeNotificationToMMX_WSMock());  

		ws_ChangeNotificationToMMX.Service1Soap port = new ws_ChangeNotificationToMMX.Service1Soap();
			port.timeout_x = 60000;
			port.endpoint_x = 'https://crossworld.medtronic.com/post/excommanddmz.asmx';
		
		port.ChangePassword('username', 'oldpass', 'newpass');
		port.deldata('userid');
		port.getMobileApps('username', 'versionnr', 'url', 'imp');
		port.GetProfile('userid', 'url', 'CrossWorldVersion');
		port.getRequestProgress('userid', 'url');
		port.getRequestProgressMMX('env', 'userid');
		port.getUserInfo('userid');
		port.setAccountPerformance('userid', 'password', 'url', 'crossworldVersion', 'DataToSubmit', 'checksum');
		port.setImplantScheduling('jobid', 'requestvalue', 'environment', 'action');
		port.SyncRequest('userid', 'password', 'ManagerId', 'impersonationId', true, 'appversion', 'url');
		port.SyncRequestMMX('uid', 'act', 'env', 'pwd', 'ad', 'imp', 'recnr', 'versionnr');
		port.valUser('userid', 'password');
		port.valUserinfo('userid', 'password', true);

		Test.stopTest();

	}

}
//--------------------------------------------------------------------------------------------------------------------