public without sharing class SynchronizationService_Complaint implements SynchronizationService_Object{
	
	//Set with the external Ids of the records already processed in this transaction. Used to avoid generating multiple notifications for the same change
	private Set<String> processedRecords = new Set<String>();
	
	//This method returns the External Ids of the records that have relevant modifications for the target Org. This includes created records or 
	//modified records where synchronized fields have changed. 
	public Set<String> hasChangeForNotification(List<SObject> records, Map<Id, SObject> oldRecords){
		
		Set<String> result = new Set<String>();
				
		for(SObject rawObject : records){
			
			Complaint__c record = (Complaint__c) rawObject;
			
			if(processedRecords.contains(record.External_Id__c)) continue;
			
			//On Insert we always send notification
			if(oldRecords == null){
				result.add(record.External_Id__c);
				processedRecords.add(record.External_Id__c);
			}//For Complaints we only sync new Records from EU to US.
		}
		
		return result;
	}
	
	//This method collects and returns the complete information for a record. This includes the records itself as well as any related record that needs
	//to exist in the target Org too. The returned format is JSON serialized string.
	public String generatePayload(String externalId){
		
		ComplaintSyncPayload payload = new ComplaintSyncPayload();
		
		String query = 	'SELECT ' + String.join( syncFields, ',') +
		 					',Account_Name__r.SAP_Id__c, Account_Name__r.AccountNumber, Account_Name__r.RecordType.DeveloperName, Asset__r.Serial_Nr__c' +
		 					',Contact_Name__r.External_Id__c, Initial_Reporter__r.External_Id__c, Surgeon1__r.External_Id__c, Site_Contact_if_refused__r.External_Id__c, RecordType.DeveloperName ' +											 
  						'FROM Complaint__c WHERE External_Id__c = :externalId LIMIT 2';		
		
		List<Complaint__c> complaints = Database.query(query);
		
		//If no record or more than 1 is found, we return an empty response
		if(complaints.size() == 1){
			
			Complaint__c complaint = complaints[0];
			bl_SynchronizationService_Utils.prepareForJSON(complaint, syncFields);			
			complaint.Id = null;//The local SFDC Id is not relevant in the target Org
			
			payload.Complaint = complaint;	
			
			Set<Id> accountIds = new Set<Id>();
			Set<Id> contactIds = new Set<Id>();
			
			if(complaint.Account_Name__r != null){
				
				accountIds.add(complaint.Account_Name__c);
				
				if ( (complaint.Account_Name__r.RecordType.DeveloperName == 'SAP_Account') || (complaint.Account_Name__r.RecordType.DeveloperName == 'Distributor') ){
					payload.AccountId = complaint.Account_Name__r.SAP_Id__c;
				}else{
					payload.AccountId = complaint.Account_Name__r.AccountNumber;
				}
				complaint.Account_Name__r = null;//If we send this reference to the target Org it may cause problems in the upsert dml
				complaint.Account_Name__c = null;//The local SFDC Id is not relevant in the target Org
			}
			
			if(complaint.Contact_Name__r != null){
				
				contactIds.add(complaint.Contact_Name__c);
				
				payload.ComplainantId = complaint.Contact_Name__r.External_Id__c;				
				complaint.Contact_Name__r = null;//If we send this reference to the target Org it may cause problems in the upsert dml
				complaint.Contact_Name__c = null;//The local SFDC Id is not relevant in the target Org
			}
			
			if(complaint.Initial_Reporter__r != null){
                
                contactIds.add(complaint.Initial_Reporter__c);
                
                payload.InitialReporterId = complaint.Initial_Reporter__r.External_Id__c;               
                complaint.Initial_Reporter__r = null;//If we send this reference to the target Org it may cause problems in the upsert dml
                complaint.Initial_Reporter__c = null;//The local SFDC Id is not relevant in the target Org
            }
			
			if(complaint.Surgeon1__r != null){
				
				contactIds.add(complaint.Surgeon1__c);
				
				payload.SurgeonId = complaint.Surgeon1__r.External_Id__c;				
				complaint.Surgeon1__r = null;//If we send this reference to the target Org it may cause problems in the upsert dml
				complaint.Surgeon1__c = null;//The local SFDC Id is not relevant in the target Org
			}
			
			if(complaint.Site_Contact_if_refused__r != null){
                
                contactIds.add(complaint.Site_Contact_if_refused__c);
                
                payload.SiteContactId = complaint.Site_Contact_if_refused__r.External_Id__c;               
                complaint.Site_Contact_if_refused__r = null;//If we send this reference to the target Org it may cause problems in the upsert dml
                complaint.Site_Contact_if_refused__c = null;//The local SFDC Id is not relevant in the target Org
            }
			
			if(complaint.Asset__r != null){
				payload.AssetId = complaint.Asset__r.Serial_Nr__c;				
				complaint.Asset__r = null;//If we send this reference to the target Org it may cause problems in the upsert dml
				complaint.Asset__c = null;//The local SFDC Id is not relevant in the target Org
			}	
			
			//Record Type
			payload.RecordType = complaint.RecordType.DeveloperName;
			complaint.RecordTypeId = null;
			complaint.RecordType = null;						
			
			payload.generateAccountPayload(accountIds, contactIds);
			
			//Done in a separate query because we cannot clear the value from the CreatedDate field before sending it to the target
			Complaint__c comp = [Select Name, CreatedDate, CreatedBy.Name  from Complaint__c where External_Id__c = :externalId];
			
			payload.ComplaintName = comp.Name;
			payload.CreatedDate = comp.CreatedDate;			
			payload.CreatedBy = comp.CreatedBy.Name;
		}
		
		return JSON.serialize(payload);
	}
	
	//Implementation needed only in Target Org
	public String processPayload(String inputPayload){
		
		String processedPayload = bl_SynchronizationService_Utils.translatePayload(inputPayload, fieldMapping);
				
		ComplaintSyncPayload payload = (ComplaintSyncPayload) JSON.deserialize(processedPayload, ComplaintSyncPayload.class);
		
		//If there was a problem in the source generating the information (no record or multiple records found) we finish here 
		if(payload.Complaint == null) return null;
		
		SavePoint sp = Database.setSavePoint();
		
		try{
			
			Complaint__c complaint = payload.Complaint;
			
			//Account related to the Complaint		
			if(payload.AccountId != null){
				
				//Manual External Id mapping
				List<Account> complaintAccount = [Select Id from Account where SAP_Id__c = :payload.AccountId 
													OR (AccountNumber = :payload.AccountId AND ST_NAV_Non_SAP_Account__c = true) LIMIT 2];
				
				if(complaintAccount.size() == 1) complaint.Account_Name__c = complaintAccount[0].Id;
				else{
					
					if(complaintAccount.size() == 0) throw new bl_SynchronizationService_Utils.SyncService_Exception('No related Account found for Id : ' + payload.AccountId);
					else  throw new bl_SynchronizationService_Utils.SyncService_Exception('Multiple related Accounts found for Id : ' + payload.AccountId);					
				}			
			}
			
			//Contact related to the Complaint			
			if(payload.ComplainantId != null){
				
				//Manual External Id mapping
				List<Contact> complaintContact = [Select Id from Contact where External_Id__c = :payload.ComplainantId LIMIT 2];
				
				if(complaintContact.size() == 1) complaint.Contact_Name__c = complaintContact[0].Id;
				else{
					
					if(complaintContact.size() == 0) throw new bl_SynchronizationService_Utils.SyncService_Exception('No related Contact found for Name : ' + payload.ComplainantId);
					else  throw new bl_SynchronizationService_Utils.SyncService_Exception('Multiple related Contacts found for Name : ' + payload.ComplainantId);					
				}													
			}
			
			//Initial Reporter related to the Complaint			
			if(payload.InitialReporterId != null){
				
				//Manual External Id mapping
				List<Contact> complaintInitialReporter = [Select Id from Contact where External_Id__c = :payload.InitialReporterId LIMIT 2];
				
				if(complaintInitialReporter.size() == 1) complaint.Initial_Reporter__c = complaintInitialReporter[0].Id;
				else{
					
					if(complaintInitialReporter.size() == 0) throw new bl_SynchronizationService_Utils.SyncService_Exception('No related Initial Reporter Contact found for Name : ' + payload.InitialReporterId);
					else  throw new bl_SynchronizationService_Utils.SyncService_Exception('Multiple related Initial Reporter Contacts found for Name : ' + payload.InitialReporterId);					
				}													
			}
			
			//Surgeon contact related to the Complaint		
			if(payload.SurgeonId != null){
				
				//Manual Upsert operation
				List<Contact> complaintSurgeon = [Select Id from Contact where External_Id__c = :payload.SurgeonId LIMIT 2];
				
				if(complaintSurgeon.size() == 1) complaint.Surgeon1__c = complaintSurgeon[0].Id;
				else{
					
					if(complaintSurgeon.size() == 0) throw new bl_SynchronizationService_Utils.SyncService_Exception('No related Contact found for Name : ' + payload.SurgeonId);
					else  throw new bl_SynchronizationService_Utils.SyncService_Exception('Multiple Contact found for Name : ' + payload.SurgeonId);					
				}							
			}
			
			//Site Contact related to the Case		
			if(payload.SiteContactId != null){
				
				//Manual Upsert operation
				List<Contact> complaintSiteContact = [Select Id from Contact where External_Id__c = :payload.SiteContactId LIMIT 1];
				
				if(complaintSiteContact.size() == 1) complaint.Site_Contact_if_refused__c = complaintSiteContact[0].Id;
				else{
					
					if(complaintSiteContact.size() == 0) throw new bl_SynchronizationService_Utils.SyncService_Exception('No Site Contact found for Name : ' + payload.SiteContactId);
					else  throw new bl_SynchronizationService_Utils.SyncService_Exception('Multiple Site Contacts found for Name : ' + payload.SiteContactId);					
				}	
			}
			
			
			//Asset related to the Complaint			
			if(payload.AssetId != null){
				
				//Manual External Id mapping
				List<Asset> complaintAsset = [Select Id from Asset where Serial_Nr__c = :payload.assetId 
												AND RecordType.DeveloperName IN ('ENT', 'Midas', 'Navigation', 'Accessories', 'O_Arm', 'PoleStar', 'Mazor', 'Visualase', 'Pain') LIMIT 2];
				
				if(complaintAsset.size() == 1) complaint.Asset__c = complaintAsset[0].Id;
				else{
					
					if(complaintAsset.size() == 0) throw new bl_SynchronizationService_Utils.SyncService_Exception('No related System found for Serial Number : ' + payload.assetId);
					else  throw new bl_SynchronizationService_Utils.SyncService_Exception('Multiple related Systems found for Serial Number : ' + payload.assetId);					
				}				
			}
			
			//Record Type
			List<RecordType> rt = [Select Id from RecordType where SObjectType = 'Complaint__c' AND DeveloperName = :payload.RecordType];
			
			if(rt.size() > 0) complaint.RecordTypeId = rt[0].Id;				
			else throw new bl_SynchronizationService_Utils.SyncService_Exception('Invalid Record type in the target Org: ' + payload.RecordType);
			
			complaint.US_Identifier__c = payload.ComplaintName;
			complaint.CreatedDate__c = payload.CreatedDate;
			complaint.CreatedBy__c = payload.CreatedBy;
			complaint.LastModifiedDate__c = payload.LastModifiedDate;
			complaint.LastModifiedBy__c = payload.LastModifiedBy;			
											
			//Manual Upsert operation				
			upsert complaint External_Id__c;
			
			return complaint.Id;
									
		}catch(Exception e){
			
			Database.rollback(sp);
			
			throw e;
		}
		
		return null;	
	}
	
	//Model classes for the requests and responses. To be used by the JSON serialize/deserialize
	public class ComplaintSyncPayload extends SynchronizationService_Payload{
		
		public Complaint__c complaint {get; set;}
		public String complaintName {get; set;}
		public String accountId {get; set;}
		public String complainantId {get; set;}
		public String initialReporterId {get; set;}
		public String surgeonId {get; set;}
		public String siteContactId {get; set;}
		public String assetId {get; set;}	
		public String recordType {get; set;}
		public Datetime createdDate {get; set;}
		public String createdBy {get; set;}
		public Datetime lastModifiedDate {get; set;}
		public String lastModifiedBy {get; set;}		
	}
	
	private static List<String> syncFields = new List<String>
	{
		'Account_Name__c',
		'Asset__c',
		'Aware_Date_Confirmed__c',
		'Brief_Description__c',
		'Complainant_Name__c',
		'Complaint_Handling_Notified_Date__c',
		'Contact_Name__c',
		'Conclusions__c',
		'Date_Complaint_Closed__c',
		'Date_FIR_Set_to_Yes__c',
		'Death_Serious_Injury1__c',
		'Did_device_meet_specifications__c',
		'Event_Date__c',
		'Event_Summary__c',
		'External_Id__c',
		'FIR_Category__c',
		'FIR_Next_Actions__c',
		'FIR_Next_Actions_Other__c',
		'FIR_Sub_Category__c',
		'Formal_Investigation_Justification__c',
		'Formal_Investigation_Reference__c',
		'Formal_Investigation_Required__c',
		'GCH_Record_Number__c',
		'Initial_Review_Complete__c',
		'Initial_Reporter__c',
		'Investigation_summary_sent_to_complaint__c',
		'Length_of_Extended_Surgical_Time__c',
		'Medtronic_Aware_Date__c',
		'Methods__c',
		'Next_Action_Resolution__c',
		'No_Work_Order_Justification__c',
		'Number_of_Unused_2D_Fluoro_Spins__c',
		'Number_of_Unused_Enhanced_Spins__c',
		'Number_of_Unused_HD_Spins__c',
		'Number_of_Unused_Standard_Spins__c',	
		'Patient_Age__c',
		'Patient_Age_Units__c',
		'Patient_Gender__c',
		'Patient_ID__c',
		'Patient_Info_Status__c',
		'Patient_Present__c',
		'Patient_Weight__c',
		'Patient_Weight_Units__c',
		'PEI1__c',
		'Probable_Cause__c',
		'Procedure_Group__c',
		'Reason_Not_Provided__c',
		'RecordTypeId',
		'Relationship_to_device__c',
		'Rep_Present__c',
		'Reportable_Complaint__c',
		'Results__c',
		'RFR_Code__c',
		'Secondary_Reportability_Review__c',
		'Service_Repair_Complaint__c',
		'Site_Contact_if_refused__c',
		'Status__c',
		'Sub_RFR_Other__c',
		'Sub_RFR_Code_Nav__c',
		'Sub_RFR_Code_O_Arm__c',
		'Sub_RFR_Code_PoleStar__c',
		'Surgeon1__c',
		'Surgery_Aborted1__c',		
		'Troubleshooting__c',
		'Type_of_Surgical_Procedure__c',
		'Type_of_Surgical_Procedure_Other__c',
		'Was_device_used_in_diagnosis__c',
		'Was_device_used_in_treatment__c',
		'Was_Medtronic_Imaging_Aborted1__c',
		'Was_Navigation_Aborted1__c',
		'What_Software_Task_When_Issue_Occurred__c',
		'When_Issue_Occurred__c',

		'Camera_Cart_Serial_Number__c'
	};
	
	private static Map<String, String> fieldMapping = new Map<String, String>		
	{
		'System__c' => 'Asset__c',
		'System__r' => 'Asset__r'			
	};
}