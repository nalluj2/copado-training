/*
 *      Description : This class centralizes business logic related to Call Management
 *
 *      Author = Rudy De Coninck
 */
 
 public with sharing class bl_CallManagement {

	//service can override check on delete callTopic
	public static boolean forceDeleteCallTopic = false;


	public static List<Contact_Visit_Report__c> saveCallContacts(Call_Records__c callRecord, List<Contact_Visit_Report__c> attendedContacts){
		
		/*
		* get existing attendedContacts for call record
		* if not in list new, remove existing ones (delete on mobile app)
		*/
		
		Set<String> mobileIds = new Set<String>();
		for (Contact_Visit_Report__c cvr : attendedContacts){
			mobileIds.add(cvr.mobile_id__c); 
		}
		
		List<Contact_Visit_Report__c> existingAttendedContacts = [select Id, Mobile_Id__c from Contact_Visit_Report__c where Call_Records__r.mobile_id__c = :callRecord.mobile_id__c ];
		List<Contact_Visit_Report__c> toRemove = new List<Contact_Visit_Report__c>();
		
		for (Contact_Visit_Report__c cvr : existingAttendedContacts){
			if (!mobileIds.contains(cvr.mobile_id__c)){
				toRemove.add(cvr);
			}
		}
		
		delete toRemove;
		System.debug('Delete attendedContacts '+toRemove);
		
		if (null!=attendedContacts){
			upsert attendedContacts Mobile_ID__c;
		}
		return attendedContacts;
		
	}
	
	public static List<Call_Topic_Subject__c> saveCallTopicSubjects(Call_Records__c callRecord, List<Call_Topic_Subject__c> callTopicSubjects){
		
		if (null!=callTopicSubjects){
			upsert callTopicSubjects Mobile_ID__c;
		}
		return callTopicSubjects;
		
	} 
	
	public static List<Call_topics__c> saveCallTopics(Call_Records__c callRecord, List<Call_topics__c> callTopics){

		Set<String> mobileIds = new Set<String>();
		for (Call_topics__c ct : callTopics){
			mobileIds.add(ct.mobile_id__c); 
		}
		
		List<Call_topics__c> existingCallTopics = [select Id, Mobile_Id__c from Call_topics__c where Call_Records__r.mobile_id__c = :callRecord.mobile_id__c ];
		List<Call_topics__c> toRemove = new List<Call_topics__c>();
		
		
		
		System.debug('Call Record '+callRecord+' Call Topics '+callTopics);
		for (Call_topics__c ct : existingCallTopics){

			if (!mobileIds.contains(ct.mobile_id__c)){
				toRemove.add(ct);
			}
		
			/*
			//validate
			if (ct.Call_Topic_Duration__c <=0){
				Exception e = new ValidationException();
				e.setMessage('Time spent cannot be less than 0.');
				throw e;
			}*/
			

		}
		forceDeleteCallTopic = true;
		deleteCallTopicCascade(toRemove);
		
		//System.debug('Delete callTopics '+toRemove);
		
		if (null!=callTopics){
			upsert callTopics Mobile_ID__c;
		}
		return callTopics;
	}
	
	
	public static void deleteCallTopicCascade(List<Call_topics__c> callTopics){
		
		List<Call_Topic_Business_Unit__c> ctbu = [select id from Call_Topic_Business_Unit__c where Call_Topic__c in :callTopics];
		List<Call_Topic_Products__c> ctp = [select id from Call_Topic_Products__c where Call_Topic__c in :callTopics];
		List<Call_Topic_Subject__c> cts = [select id from Call_Topic_Subject__c where Call_Topic__c in :callTopics];
		delete ctbu;
		delete ctp;
		delete cts;
		delete callTopics;
		
	}
	
	
	public static List<Call_Topic_Products__c> saveCallTopicProducts(Call_Records__c callRecord, List<Call_Topic_Products__c> callTopicProducts){
		
		if (null!=callTopicProducts){
			upsert callTopicProducts Mobile_ID__c;
		}
		return callTopicProducts;
	}

	public static List<Call_Topic_Business_Unit__c> saveCallTopicBusinessUnits(Call_Records__c callRecord, List<Call_Topic_Business_Unit__c> callTopicBusinessUnits){
		List<Call_Topic_Business_Unit__c> toProcess = new List<Call_Topic_Business_Unit__c>();
		for (Call_Topic_Business_Unit__c ct : callTopicBusinessUnits){
			//validate
			toProcess.add(ct.clone());
		}
		
		upsert toProcess Mobile_ID__c;
		return toProcess;
	}
	
	public static void updateProductDemonstrationOpportunity(List<Id> callTopicIds){
		List<Call_Topics__c> callTopicProductDemonstrations = [SELECT Call_Records__c, Call_Records__r.call_date__c, Call_Records__r.Opportunity_ID__c FROM Call_Topics__c where id in : callTopicIds];
		Map<Id,Call_Topics__c> mapCallTopicsPerOpportunity = new Map<Id,Call_Topics__c>();
		for (Call_Topics__c ct : callTopicProductDemonstrations){
			mapCallTopicsPerOpportunity.put(ct.Call_Records__r.Opportunity_ID__c,ct);
		}
		List<Opportunity> opportunitiesToUpdate = [select Id from Opportunity where id in : mapCallTopicsPerOpportunity.keySet()];
		for (Opportunity opp : opportunitiesToUpdate){
			Call_topics__c ct = mapCallTopicsPerOpportunity.get(opp.id);
			opp.Product_Demo_Complete__c = ct.Call_Records__r.call_date__c;
		}
		update opportunitiesToUpdate;
	}

}