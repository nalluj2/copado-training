@isTest 
private class TEST_bl_StakeholderMapping_Trigger {

	@isTest
	private static void test_IHS_CreateTask() {

    	User currentUser = [Select Alias_Unique__c from User where Id = :UserInfo.getUserId()];
    	
		List<Trigger_Deactivation__c> lstTriggerDeactivation = new List<Trigger_Deactivation__c>();
		Trigger_Deactivation__c oTriggerDeactivation1 = new Trigger_Deactivation__c();
			oTriggerDeactivation1.Name = 'Opp_populateOwnerManagerEmail';
			oTriggerDeactivation1.Deactivate__c = true;
			oTriggerDeactivation1.User_Ids__c = null;
		lstTriggerDeactivation.add(oTriggerDeactivation1);
		Trigger_Deactivation__c oTriggerDeactivation2 = new Trigger_Deactivation__c();
			oTriggerDeactivation2.Name = 'Opp_setBUG_BU_SBU';
			oTriggerDeactivation2.Deactivate__c = true;
			oTriggerDeactivation2.User_Ids__c = null;
		lstTriggerDeactivation.add(oTriggerDeactivation2);
		insert lstTriggerDeactivation;
		bl_Trigger_Deactivation.bIgnoreWhenRunningTest = false;


		clsTestData_Opportunity.idRecordType_StakeholderMapping = clsUtil.getRecordTypeByDevName('Stakeholder_Mapping__c', 'EMEA_IHS_Stakeholder_Mapping').Id;

		Opportunity oOpportunity = clsTestData_Opportunity.createOpportunity_IHS_Stage('MANAGEDSERVICE', '4a', true);

		oOpportunity = [SELECT Id, StageName, RecordType.DeveloperName FROM Opportunity WHERE Id = :oOpportunity.Id];

		System.debug('**BC** oOpportunity.StageName : ' + oOpportunity.StageName);
		System.debug('**BC** oOpportunity.RecordType.DeveloperName : ' + oOpportunity.RecordType.DeveloperName);

	}
}