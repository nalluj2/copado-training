//------------------------------------------------------------------------------------------------------
//	Description : This Class contains the Business Logic that is used on Cases
//	Version 	: 1.0
//	Author 		: Bart Caelen
// 	Date 		: 20150309
//------------------------------------------------------------------------------------------------------
public class bl_Case {

	public static Boolean bCaseDoNotUpdateOwner = false;    // This is used in the bl_Case.updateOwnership @Future method to prevent multiple executions
	
	public static List<Business_Unit__c> lstBu {
		
		get{
			if(lstBu == null){
				
				lstBu = [Select id,Company_Text__c,Case_Record_Types__c,Company__c from Business_Unit__c];
			}	
			
			return lstBu;
		}
		
		set;
	} 
	
	public static List<Sub_Business_Units__c> lstSBU {
		
		get{
			if(lstSBU == null){
				
				lstSBU = [SELECT id, Case_Record_Type_ID__c FROM Sub_Business_Units__c];
			}	
			
			return lstSBU;
		}
		
		set;
	} 
	
	
	//------------------------------------------------------------------------------------------------------
	// This @Future methods allows us to prevent the sending of the default Email Notification 
	//	when the Case Owner is changed.  This is possible by using DMLOptions - EmailHeader settings 
	//	but not without using @Future.
	//	More info: https://help.salesforce.com/apex/HTViewSolution?id=000176854&language=en_US
	//------------------------------------------------------------------------------------------------------
	@Future public static void updateOwnership(Map<Id, Id> mapCaseID_OwnerID) { 
		Database.DMLOptions oDMLOptions = new Database.DMLOptions(); 
		oDMLOptions.EmailHeader.triggerUserEmail = false; 

		List<Case> lstCase_Update = [SELECT Id, OwnerId, OMA_Case_Owner__c FROM Case WHERE Id in :mapCaseID_OwnerID.keyset()]; 

		for(Case oCase : lstCase_Update) { 
			oCase.OwnerId = mapCaseID_OwnerID.get(oCase.Id); 
			oCase.OMA_Case_Owner__c =  mapCaseID_OwnerID.get(oCase.Id); 
		} 
		bl_Case.bCaseDoNotUpdateOwner = true;

		Database.update(lstCase_Update, oDMLOptions); // Actual DML operation is performed instead of implicit update
	} 
	//------------------------------------------------------------------------------------------------------

}
//------------------------------------------------------------------------------------------------------