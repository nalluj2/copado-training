/*
    Vf Page Name : page_TransferContacts
    Description : This controller inckude the logic to transfer contact from one account to other Account, dectivate existing
                relationship and cretae new relationship.
    Author      : Priti jaiswal
    Date        : 07/07/2013
    
*/

public class ctrlExt_TransferAccounts {
    
    public Id accountID; 
    public Affiliation__c newAccount{get;set;}
    public List<wrpContacts> lstwrpRelationship;
    public id recTypeID;    
    public ctrlExt_TransferAccounts(ApexPages.StandardController controller) {
        accountID=ApexPages.currentPage().getParameters().get('id');       
        newAccount=[select Affiliation_To_Account__c from Affiliation__c where Affiliation_To_Account__c=:accountID limit 1];
        newAccount.Affiliation_To_Account__c=null;  
        recTypeID=[select id,DeveloperName from RecordType where DeveloperName=:'C2A' and SobjectType=:'Affiliation__c'].id;
        lstwrpRelationship =new List<wrpContacts>();
    }
    //wrapper clas to associate the Relatioship record with checkbox..
    public class wrpContacts{
        public Boolean selected{get;set;}
        public Affiliation__c contactRelationships{get;set;}
        
        public wrpContacts(Affiliation__c relationship){
            contactRelationships=relationship;
            selected=false;
        }
    } 

    //get the list of relationship record to dipsplay...
    public List<wrpContacts> getRelationships(){        
        lstwrpRelationship.clear();  
        for(Affiliation__c aff:[select Affiliation_To_Account__c,Contact_Name__c,Affiliation_From_Contact__c,Affiliation_Type__c,Affiliation_Position_In_Account__c,
                    Department__c,Contact_Primary_Specialty__c,Affiliation_Start_Date__c,Affiliation_End_Date__c,Affiliation_From_Contact__r.Contact_Active__c, 
                    Affiliation_From_Contact__r.firstname, Affiliation_From_Contact__r.lastname,
                    Affiliation_Primary__c,Affiliation_Active__c from Affiliation__c where Affiliation_To_Account__c=:accountID and RecordTypeID=:recTypeID 
                    order BY Affiliation_From_Contact__r.lastname, Affiliation_From_Contact__r.firstname //Affiliation_Type__c
                    ]){
            lstwrpRelationship.add(new wrpContacts(aff));
        }
        return lstwrpRelationship;      
    }
    
    map<id,boolean> mapContactAndPrimaryAff=new Map<id,boolean>();
    map<id,Affiliation__c> mapContactAndAff=new Map<id,Affiliation__c>();
    //tranfer selected contact....
    public PageReference transferSelectedContacts(){ 
        Set<ID> setSelectedContactId=new Set<Id>();
        List<Affiliation__c> lstSelectedRelationship=new List<Affiliation__c>();
        if(lstwrpRelationship.size()>0){
            for(wrpContacts con:lstwrpRelationship){        
                if(con.selected==true){
                    setSelectedContactId.add(con.contactRelationships.Affiliation_From_Contact__c);                 
                    mapContactAndPrimaryAff.put(con.contactRelationships.Affiliation_From_Contact__c,con.contactRelationships.Affiliation_Primary__c);
                    mapContactAndAff.put(con.contactRelationships.Affiliation_From_Contact__c,con.contactRelationships);
                    lstSelectedRelationship.add(con.contactRelationships);
                }
            }
        }
        List<Affiliation__c> lstExistingRelationship=[select Affiliation_Active__c,Affiliation_From_Contact__r.name from Affiliation__c where Affiliation_From_Contact__c in:setSelectedContactId 
                                                    and Affiliation_To_Account__c=:newAccount.Affiliation_To_Account__c and RecordTypeID=:recTypeID];                                                  
       
        boolean boolexistRleationship=false;
        String selectedConatactName=null;        
        
        //if relationship already exist set boolean to true and get the name of those contact..
        if(lstExistingRelationship.size()>0){
            for(Affiliation__c existAff:lstExistingRelationship){
                boolexistRleationship=true;               
                if(selectedConatactName==null){
                    selectedConatactName=existAff.Affiliation_From_Contact__r.name;
                }
                else{
                    selectedConatactName=selectedConatactName+','+existAff.Affiliation_From_Contact__r.name;
                }
            }
        }       
        
        //list of all selected contact...    
        List<Contact> lstSelectedContacts=[select id,name,Accountid,Affiliation_To_Account__c,Primary_Job_Title_vs__c,Phone,Contact_Department__c, Contact_Active__c from Contact where id in:setSelectedContactId];
        List<Contact> lstConatctToCreateRelationship=new List<Contact>();
        for(Contact con:lstSelectedContacts){
            lstConatctToCreateRelationship.add(con);
        }
        //remove those contact from list(lstConatctToCreateRelationship) for which relatioship is already existing...
        if(lstExistingRelationship.size()>0){
            for(Affiliation__c aff:lstExistingRelationship){               
                for(integer k=0;k<lstConatctToCreateRelationship.size();k++){  
                    if(lstConatctToCreateRelationship[k].id==aff.Affiliation_From_Contact__c){
                        lstConatctToCreateRelationship.remove(k);
                    }
                }
            }
        }
        
        List<Contact> lstPrimaryContactToCreateRelationship=new List<Contact>();
        List<Contact> lstNonPrimaryContactToCreateRelationship=new List<Contact>();      
        
        //replace the primary account of contact with new account if relationship is primary otherwise don't change account...
        if(lstSelectedContacts.size()>0 && newAccount.Affiliation_To_Account__c!=null){
            if(lstConatctToCreateRelationship.size()>0){
                for(Contact con:lstConatctToCreateRelationship){            
                    if(mapContactAndPrimaryAff.get(con.id)==true){
                        con.Accountid=newAccount.Affiliation_To_Account__c;
                        lstPrimaryContactToCreateRelationship.add(con);
                    }
                    else{
                        lstNonPrimaryContactToCreateRelationship.add(con);
                    }
                }
            }
        }
        
        //update all selected contact if there is no existing relationship... 
        
        if(lstSelectedContacts.size()>0 && newAccount.Affiliation_To_Account__c!=null){ 
            if(lstPrimaryContactToCreateRelationship.size()>0){
                update lstPrimaryContactToCreateRelationship;//the exisitng trigger will automatically create the relationship for updated contact  
                
            }               
            //creating relationship for non primary contact and put end date and active to false for selected relationship....
            if(lstNonPrimaryContactToCreateRelationship.size()>0){                           
                List<Affiliation__c> affsToInsert=new List<Affiliation__c>();
                List<Affiliation__c> affsToUpdate=new List<Affiliation__c>();
                for(contact con:lstNonPrimaryContactToCreateRelationship){
                    Affiliation__c aff=new Affiliation__c();
                    aff.Affiliation_From_Contact__c=con.id;
                    aff.Affiliation_To_Account__c=newAccount.Affiliation_To_Account__c;
                    aff.Affiliation_Type__c=mapContactAndAff.get(con.id).Affiliation_Type__c;
                    aff.Affiliation_Position_In_Account__c=mapContactAndAff.get(con.id).Affiliation_Position_In_Account__c;
                    aff.Department__c=mapContactAndAff.get(con.id).Department__c; 
                    aff.Affiliation_Start_Date__c=System.today();
                    aff.RecordTypeId=recTypeID;
                    affsToInsert.add(aff);  
                    for(Affiliation__c aff1:lstSelectedRelationship){                       
                        if(aff1.Affiliation_From_Contact__c==con.id){                           
                            aff1.Affiliation_Active__c=false;
                            aff1.Affiliation_End_Date__c=System.today();
                            affsToUpdate.add(aff1);
                        }
                    }
                }
                insert affsToInsert;
                update affsToUpdate;
            }
            if(boolexistRleationship==false){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.info,'Selected Contacts have been transferred to the New Account');
                ApexPages.addMessage(myMsg); 
                newAccount.Affiliation_To_Account__c=null;
                return null;
            }
            if(boolexistRleationship==true){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,'Contact ' +selectedConatactName+' already has an existing relationship with the New Account.');
                ApexPages.addMessage(myMsg);
                newAccount.Affiliation_To_Account__c=null;
                return null;
            }
            
        }
        
        //show error message if no Contact/Account is selected and transfer button is clicked...
        else if(lstSelectedContacts.size()==0 || newAccount.Affiliation_To_Account__c==null){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select a New Account and/or Contacts');
            ApexPages.addMessage(myMsg);                                       
            return null;        
        }
        return null;
    }

}