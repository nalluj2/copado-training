public with sharing class bl_Contact_LearningManagement_Provision {

	
	public static Map<String, String> countryMapping{
 		
 		get{
 			
 			if(countryMapping == null){
 				
 				countryMapping = new Map<String, String>();
 				
 				for(DIB_Country__c country : [Select Country_ISO_Code__c, Name from DIB_Country__c]) countryMapping.put(country.Country_ISO_Code__c, country.Name);
 			}
 			
 			return countryMapping;
 		}
 		
 		set;
	}
	
	@future(callout = true)
	public static void doUserProvision(Id syncId){

		Contact_ED_Sync__c sync = 
			[
				SELECT
					Contact__c
					, Campaign__c, Campaign__r.Curriculum_ID__c
					, CSOD_Integration__c, Outcome__c, Outcome_Details__c
				FROM 
					Contact_ED_Sync__c 
				WHERE 
					Id = :syncId
			];
		
		Contact cnt = 
			[
				SELECT 
					Id, FirstName, LastName, LMS_Country__c, LMS_Language__c, LMS_Division__c, LMS_IsActive__c, Email, LMS_Userid__c, Title, MailingCountry, Identification_Number__c, LMS_Activation_Date__c
					, LMS_Key_Customer_Contact__r.Alias_unique__c, LMS_Key_Customer_Contact__r.FirstName, LMS_Key_Customer_Contact__r.LastName, LMS_Key_Customer_Contact__r.Title, LMS_Key_Customer_Contact__r.Email, LMS_Key_Customer_Contact__r.Phone, LMS_Key_Customer_Contact__r.Account.Account_Country_vs__c
					, (Select Id, Status, Campaign.LMS_ED_Group__c from CampaignMembers where Status IN ('Start enrollment', 'Enrolled') AND Campaign.RecordType.DeveloperName = 'LEARNING')
				FROM 
					Contact
				WHERE
					Id = :sync.Contact__c
			];
		
		Contact updateContact;
		Boolean bUpdateContact = false;
		
		try{
			
			if (Test.isRunningTest()) clsUtil.bubbleException('bl_ContactEDProvisioning.doUserProvision_EXCEPTION');
										
			updateContact = bl_ContactEDProvisioning.doEDUserProvision(cnt, sync);					

			if (updateContact != null) bUpdateContact = true;
		
		}catch(Exception e){
			
			sync.Outcome__c = 'Failure';
			sync.Outcome_Details__c += '\n' + e.getMessage() + '\n\n' + e.getStackTraceString();
			update sync;
			
			return;//If we fail in External Directory we stop here, there is nothing else to update

		}
		
		try{
			
			//Do CSOD integration
			if (Test.isRunningTest()) clsUtil.bubbleException('bl_ContactCSODIntegration.doCSODIntegration_EXCEPTION');

			if (sync.CSOD_Integration__c == true){
										
				updateContact = bl_ContactCSODIntegration.doCSODIntegration(cnt, sync);					

				if ( updateContact != null) bUpdateContact = true;
			}
			
			sync.Outcome__c = 'Success';
			
		}catch(Exception e){
			
			sync.Outcome__c = 'Failure';
			sync.Outcome_Details__c += '\n' + e.getMessage() + '\n\n' + e.getStackTraceString();			

		}
				
		try{

			if (sync.Outcome__c == 'Success'){

				if (bUpdateContact) update cnt; // If ED + CSOD integration was successfull and we need to update the Contact

				// Update the Status of the Campaign Member to "Enrolled" but only for the CampaignMembers linked to the processing Campaign
				List<CampaignMember> lstCampaignMember_Update = new List<CampaignMember>();
				for (CampaignMember oCampaignMember : cnt.CampaignMembers){
					if ( (oCampaignMember.CampaignId == sync.Campaign__c) && (oCampaignMember.Status != 'Enrolled') ){
						oCampaignMember.Status = 'Enrolled';
						lstCampaignMember_Update.add(oCampaignMember);
					}
				}
				if (lstCampaignMember_Update.size() > 0) update lstCampaignMember_Update;

			}						

			
		}catch(Exception e){
			
			sync.Outcome__c = 'Failure';
			sync.Outcome_Details__c += '\n' + e.getMessage() + '\n\n' + e.getStackTraceString();			

		}
			
		update sync;			
	}       
}