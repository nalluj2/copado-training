//---------------------------------------------------------------------------------------------------------------------------------------------------
//  Author      :    Rudy De Coninck
//  Date        :    20140313
//  Description :    Batch to process Application Usage Details
//---------------------------------------------------------------------------------------------------------------------------------------------------
//  Author      :    Bart Caelen
//  Date        :    04/05/2018
//  Description :    Re-Written logic because in batch apex the ORDER BY in a SOQL Statement doesn't match the sequence of the batches
//---------------------------------------------------------------------------------------------------------------------------------------------------
global class ba_Application_Usage_Detail implements Schedulable,Database.Batchable<sObject>, Database.Stateful{

	private Set<Id> setID_User_Processed;
	private List<User> lstUser_Update;
	global Datetime dtStart = Datetime.now().addMonths(-1);	// Only process the data in the last month because this process runs every day
	global String tSOQL = 'SELECT User_ID__c, User_ID__r.Id, User_ID__r.Last_Application_Login__c, Action_DateTime__c FROM Application_Usage_Detail__c WHERE Capability__c = \'login\' AND Action_DateTime__c >= :dtStart ORDER BY User_ID__c, Action_Datetime__c DESC';
    global Integer iBatchSize = 2000;

    // SCHEDULE Settings
    global void execute(SchedulableContext ctx){        

        ba_Application_Usage_Detail oBatch = new ba_Application_Usage_Detail();
            oBatch.iBatchSize = iBatchSize;
        Database.executebatch(oBatch, iBatchSize);

    } 

    
    // BATCH Settings
    global Database.QueryLocator start(Database.BatchableContext ctx){    

		setID_User_Processed = new Set<Id>();
		lstUser_Update = new List<User>();

        return Database.getQueryLocator(tSOQL);         

    }

    global void execute(Database.BatchableContext ctx, List<SObject> lstData){
        
		List<Application_Usage_Detail__c> lstApplicationUsageDetail = (List<Application_Usage_Detail__c>)lstData;
        
		for (Application_Usage_Detail__c oApplicationUsageDetail : lstApplicationUsageDetail){
			
			if (setID_User_Processed.contains(oApplicationUsageDetail.User_Id__r.Id)) continue;

			if (oApplicationUsageDetail.User_Id__r.Last_Application_Login__c != oApplicationUsageDetail.Action_DateTime__c){
				User oUser = new User(Id = oApplicationUsageDetail.User_Id__r.Id);
					oUser.Last_Application_Login__c = oApplicationUsageDetail.Action_DateTime__c;
				lstUser_Update.add(oUser);
			}

			setID_User_Processed.add(oApplicationUsageDetail.User_Id__r.Id);

		}
        
    }

    global void finish(Database.BatchableContext ctx){
    	
		if (lstUser_Update.size() > 0) update lstUser_Update;

    }

}
//---------------------------------------------------------------------------------------------------------------------------------------------------