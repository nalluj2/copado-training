/*  
        Class Name -  test_DIB_Class_Allocations  
        Description  –  The written class will be used for the coverage of following 5 classes:
                        DIB_Class_Allocation
                        DIB_Class_Allocation_Core
                        implantAffiliationChecker
                        ctrlExt_TransferDiabetesSegmentation
                        
        Author - Shweta Singh(WIPRO) 
        Created Date - 17-1-2011 
    */
@isTest 
private class test_DIB_Class_Allocations {
    
	static testMethod void testDIBClassAllocation() {       
         
         DIB_Country__c nl = [SELECT Id,  BU_Not_Mandatory_on_Relationship__c, Hide_Therapy_on_Relationship__c FROM DIB_Country__c where Country_ISO_Code__c = 'NL'];
         nl.BU_Not_Mandatory_on_Relationship__c = true;
         nl.Hide_Therapy_on_Relationship__c = true;
     	 update nl;
         
         String months;
         String years; 
         datetime currentDateTime;
         Date currentDate;
         Date startDate;
         Date endDate;
         String accountType = 'Adult';
         User currentUser;
         currentDateTime = datetime.now();
         currentDate = currentDateTime.date();
         startDate = currentDateTime.date().addYears(-2);
         endDate = currentDateTime.date().addYears(+2);
        
        // Get the current fiscal months and current fiscal years. 
        List<SelectOption> periods = DIB_AllocationSharedMethods.getPeriodLines();       
		if (periods.size() == 0) clsTestData_System.createDIBFiscalPeriod();
		periods = DIB_AllocationSharedMethods.getPeriodLines();
        String[] fiscalPeriod = new String[]{};        
        for (SelectOption so : periods)
        {
            fiscalPeriod.add(so.getValue());                    
        }
        months = fiscalPeriod.get(0).substring(0,2);
        years = fiscalPeriod.get(0).substring(3,7); 
        Account acc1 = new Account();
        acc1.Name = 'TEST_controllerAffiliationsRTView Test Coverage Account 1';
        acc1.Account_Country_vs__c = 'NETHERLANDS'; 
        insert acc1 ;
        
        Account acc2 = new Account();
        acc2.Name = 'TEST_controllerAffiliationsRTView Test Coverage Account 2'; 
        acc2.SAP_ID__c =  '00067898'; 
        acc2.isSales_Force_Account__c=true;    
        acc2.Account_Country_vs__c = 'NETHERLANDS';
        insert acc2 ;  
        
        Contact cont1 = new Contact();
        cont1.LastName = 'TEST_controllerAffiliationsRTView TestCont1' ;  
        cont1.FirstName = 'TEST' ;  
        cont1.AccountId = acc1.Id ;
        cont1.Contact_Active__c = false  ;
        cont1.Contact_Inactive_Reason__c = 'Inactive Reason is required to all inactve Contacts'; 
        cont1.Contact_Department__c = 'Diabetes Adult'; 
        cont1.Contact_Primary_Specialty__c = 'ENT';
        cont1.Affiliation_To_Account__c = 'Employee';
        cont1.Primary_Job_Title_vs__c = 'Manager';
        cont1.Contact_Gender__c = 'Male';         
        insert cont1;
        
        Contact cont2 = new Contact();
        cont2.LastName = 'TEST_controllerAffiliationsRTView TestCont2';  
        cont2.FirstName = 'TEST' ;  
        cont2.AccountId = acc1.Id ;
        cont2.Contact_Active__c = false;
        cont2.Contact_Inactive_Reason__c = 'Inactive Reason is required to all inactve Contacts';
        cont2.Contact_Department__c = 'Diabetes Adult'; 
        cont2.Contact_Primary_Specialty__c = 'ENT';
        cont2.Affiliation_To_Account__c = 'Employee';
        cont2.Primary_Job_Title_vs__c = 'Manager';
        cont2.Contact_Gender__c = 'Male';          
        insert cont2;
        
        Department_Master__c department1 = new Department_Master__c();
        department1.Name = accountType;
        insert department1;
        
        //Department
        DIB_Department__c myDepartment1 = new DIB_Department__c();
        myDepartment1.Account__c = acc2.Id;
        myDepartment1.Department_ID__c = department1.Id;
        
        insert myDepartment1;
        
        // Create test Affiliation      
        list <Affiliation__c> lstAff  = new list <Affiliation__c>();

        Affiliation__c insertAff=new Affiliation__c();
        insertAff.Affiliation_From_Contact__c = cont1.Id;
        insertAff.Affiliation_From_Account__c = acc1.Id ;
        insertAff.Affiliation_To_Contact__c = cont2.Id;
        insertAff.Affiliation_To_Account__c = acc2.Id ; 
        insertAff.RecordTypeId = FinalConstants.recordTypeIdC2C;
        //insert insertAff;
        lstAff.add(insertAff);
        
        Affiliation__c insertAff2 =new Affiliation__c();
        insertAff2.Affiliation_From_Contact__c = cont1.Id;
        insertAff2.Affiliation_To_Account__c = acc2.Id ;
        insertAff2.RecordTypeId = FinalConstants.recordTypeIdC2A;
        //insert insertAff2;
        lstAff.add(insertAff2);
        insert lstAff;

           DIB_Invoice_Line__c invOne  = new DIB_Invoice_Line__c() ; 
            invOne.Distribution_No__c = 'DN' ; 
            invOne.Name = 'Invoice Name' ;  
            invOne.Fiscal_Month_Code__c = months;  
            invOne.Fiscal_Year__c = years ; 
            invOne.Allocated_Fiscal_Month_Code__c=months;
            invOne.Allocated_Fiscal_Year__c=years; 
            invOne.Sales_Force_Account__c=acc2.Id;
            invOne.Department_ID__c=myDepartment1.Id;
            // This field must have a value otherwise can have a problem saying Argument 1 cannot be null Line 101 on Method SharedMethods. 
            invOne.Quantity_Invoiced_Number__c = 1; 
            invOne.Sold_To__c = acc1.Id;
            invOne.Ship_To_Name_Text__c = acc1.Name;
            invOne.Is_Allocated__c = false ;  
            invOne.Is_Allocated__c = true ; 
            insert invOne;

        
        List<DIB_Invoice_Line__c> invoiceLines = new List<DIB_Invoice_Line__c>{} ;      
        for (Integer i = 0; i < 10 ; i++)
        {
            DIB_Invoice_Line__c invl  = new DIB_Invoice_Line__c() ; 
            invl.Distribution_No__c = 'DN' ; 
            invl.Name = 'Invoice Name' ;  
            invl.Fiscal_Month_Code__c = months;  
            invl.Fiscal_Year__c = years ; 
            invl.Allocated_Fiscal_Month_Code__c=months;
            invl.Allocated_Fiscal_Year__c=years; 
            invl.Sales_Force_Account__c=acc2.Id;
            invl.Department_ID__c=myDepartment1.Id;
            // This field must have a value otherwise can have a problem saying Argument 1 cannot be null Line 101 on Method SharedMethods. 
            invl.Quantity_Invoiced_Number__c = 1; 
            invl.Sold_To__c = acc1.Id;
            invl.Ship_To_Name_Text__c = acc1.Name;
            invl.Is_Allocated__c = false ;  
            if (i > 5 && i < 20)
            {
                invl.Is_Allocated__c = true ; 
            }
            invoiceLines.add(invl) ; 
        }
        insert invoiceLines;
        
        list<DIB_Department__c> lstDepartment = new list<DIB_Department__c>();   
        Department_Master__c department = new Department_Master__c();
        department.Name = accountType;
        insert department;

        DIB_Department__c myDepartment = new DIB_Department__c();
        myDepartment.Account__c = acc1.Id;
        myDepartment.Department_ID__c = department.Id;
        myDepartment.To_Account__c = acc2.Id;        
        lstDepartment.add(myDepartment);
        insert lstDepartment;

        DIB_Class_Allocation controller = new DIB_Class_Allocation();
        controller.getsectionName();
        controller.getId();
        controller.getmassUpdate();
        controller.gettoAllocate();
        controller.getsectionName();
        controller.initSoldToShipTo(invoiceLines,lstAff,lstDepartment);
        List<DIB_Class_Allocation_Core> lstDIBClassAllocationCore = new List<DIB_Class_Allocation_Core>();
        controller.settoAllocate(lstDIBClassAllocationCore);
        controller.getDepartement(null);
        DIB_Class_Allocation controller1 = new DIB_Class_Allocation(invoiceLines,lstAff,lstDepartment);


        DIB_Class_Allocation_Core controller2 = new DIB_Class_Allocation_Core(invOne,lstAff,lstDepartment);
        controller2.getsfaPL();
        controller2.getinvl();
        controller2.getDepartement(myDepartment.id);
//      controller2.selectedSfaId(); 

        controller2.setinvl(invoiceLines[1]);
        controller2.getlineSelected();
        controller2.drawPicklListValuesDIBDepartment(lstDepartment);
        controller2.drawPicklListValues(acc1.id,acc1,lstAff,true);


        ApexPages.currentPage().getParameters().put('id',myDepartment.id);
        ApexPages.StandardController IController = new ApexPages.StandardController(myDepartment);
        ctrlExt_TransferDiabetesSegmentation ctlr=new ctrlExt_TransferDiabetesSegmentation(IController);  
        ctlr.transferDiabetes();
    }
}