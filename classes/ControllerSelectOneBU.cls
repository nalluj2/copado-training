public with sharing class ControllerSelectOneBU {

    //------------------------------------------------------------------------
    // Variables
    //------------------------------------------------------------------------
    private User_Business_Unit__c userBUs;
    private String retURL = '';
    private Map<String, String> URLParams = new Map<String, String>(); 

    private Map<Id, String> mapUserBU = new Map<Id, String>();

    private Boolean bIsImplantRelated = false;
    //------------------------------------------------------------------------

    
    //------------------------------------------------------------------------
    // Constructor
    //------------------------------------------------------------------------
    public ControllerSelectOneBU(ApexPages.StandardController controller){

        URLParams = ApexPages.currentPage().getParameters();
        userBUs = (User_Business_Unit__c)controller.getRecord();
        if (ApexPages.currentPage().getParameters().get('mainPage') != null){
            retURL = ApexPages.currentPage().getParameters().get('mainPage');
        }

        bIsImplantRelated = false;
        if (URLParams.containsKey('action')){
            if (URLParams.get('action') == 'Implant'){
                bIsImplantRelated = true;
            }
        }

        // Load User Business Units
        loadBU_User();

    }
    //------------------------------------------------------------------------


    //------------------------------------------------------------------------
    // Getters & Setters
    //------------------------------------------------------------------------
    public Boolean bShowTherapy { 
        get{
            Boolean bResult = false;
            if (bIsImplantRelated){
                String tBUName = mapUserBU.get(BU);
                bResult = bl_Implant.setBU_RTG.contains(mapUserBU.get(BU));
            }
            return bResult;
        }
        private set; 
    }
    public String tTherapyId { get; set; } 
    public List<SelectOption> lstSO_Therapy { 
        get {
            List<SelectOption> lstSO = new List<SelectOption>();
            if (bIsImplantRelated){
                lstSO = loadTherapy_SO();
            }
            return lstSO;
        }
        private set; 
    }

    public String BU { get; set; }
    public List<SelectOption> lstSO_BU {
        get {
            return loadBU_SO();
        }
        private set;
    }
    //------------------------------------------------------------------------


    //------------------------------------------------------------------------
    // Actions
    //------------------------------------------------------------------------
    public pageReference continueSave(){

        if (!bValidateRequiredFields()) return null;
		
		PageReference pageRef = new PageReference(retURL);
		pageRef.getParameters().putAll(URLParams);
		pageRef.getParameters().put('BU', BU);
        
        if (bIsImplantRelated) pageRef.getParameters().put('TherapyId', tTherapyId);
          
        pageRef.setRedirect(true);
        return pageRef;                     

    }

    public pageReference cancelButton(){

        PageReference pageRef = new PageReference('/home/home.jsp');
            pageRef.setRedirect(true);
        return pageRef;             

    }
    
    public pageReference RedirectToNoBU(){
    
        PageReference pageRef = new PageReference('/apex/NoBusinessUnitPage');
            pageRef.setRedirect(true);
        return pageRef;                 

    }
    //------------------------------------------------------------------------


    //------------------------------------------------------------------------
    // Private Methods
    //------------------------------------------------------------------------
    private void loadBU_User(){

        mapUserBU = SharedMethods.getUserBusinessUnits();
        if (mapUserBU.size() > 0){
            // Get User's Primary Business Unit
            BU = SharedMethods.getUserPrimaryBU(); 
        }

    }
    private List<SelectOption> loadBU_SO(){

        List<SelectOption> lstSO = new List<SelectOption>();

        lstSO.add(new SelectOption('','----Select----'));     
        if (mapUserBU.size() > 0){
            Set<Id> setID_BU = mapUserBU.keySet();
            for (Id id_BU : setID_BU){
                lstSO.add(new SelectOption(id_BU, mapUserBU.get(id_BU)));
            }
        }
        return lstSO;

    }
    private List<SelectOption> loadTherapy_SO(){

        List<SelectOption> lstSO = new List<SelectOption>();

        // Check if selected BU is an RTG BU
        String tBUName = mapUserBU.get(BU);

        if (bShowTherapy){

            lstSO.add(new SelectOption('','----Select----'));     
            List<Therapy__c> lstTherapy = [SELECT Id, Name FROM Therapy__c WHERE Active__c = true AND Sub_Business_Unit__r.Business_Unit__c = :BU ORDER BY Name];
            for (Therapy__c oTherapy : lstTherapy){
                lstSO.add(new SelectOption(oTherapy.Id, oTherapy.Name));
            }

        }

        return lstSO;

    }

    private Boolean bValidateRequiredFields(){

        Boolean bResult = true;

        if (String.isBlank(BU)){
            userBUs.addError('Please select a Business Unit.');
            bResult = false;
        }

        if ( (bShowTherapy) && (String.isBlank(tTherapyId)) ){
            userBUs.addError('Please select a Therapy.') ;
            bResult = false;
        }

        return bResult;

    }
    //------------------------------------------------------------------------
 

}