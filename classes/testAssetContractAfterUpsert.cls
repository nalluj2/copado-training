@isTest(SeeAllData=true)
public class testAssetContractAfterUpsert{

 	static testMethod void testentitlements()//b-
    {       	    	    
        Account acc=new Account();
        acc.AccountNumber='as';
        acc.Name='MNav House Account';
        acc.AccountNumber = '0001234567';
        acc.Account_Country_vs__c='USA';        
        acc.CurrencyIsoCode='USD';
        acc.Phone='24107954';
        insert acc; 
        
        Id oarmRTId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('O-Arm').getRecordTypeId();        
        List<Asset> syslist=new List<Asset>();
        Asset Sys = new Asset(Name='Pune',Asset_Product_Type__c='Gen II',AccountId=Acc.Id,Ownership_Status__c='EOL', recordTypeId = oarmRTId, Serial_Nr__c = 'XXXXX');
        syslist.add(Sys);
        Asset Sys1 = new Asset(Name='Gndc',Asset_Product_Type__c='Gen II',AccountId=Acc.Id,Ownership_Status__c='EOL', recordTypeId = oarmRTId, Serial_Nr__c = 'YYYYY');
        syslist.add(Sys1);
        insert Syslist;
        //- Create a New PM type Site Activity record
        
        Contract Scr = new Contract(AccountId =Acc.Id,StartDate = System.today(),ContractTerm = 7,Name = '00000000',Contract_Type__c = 'Service Agreement', Approval_Status__c = 'Draft');
        insert Scr;
        
        List<AssetContract__c> entlist=new List<AssetContract__c>();
        AssetContract__c ent =new AssetContract__c(Asset__c=Sys.Id,Contract__c=Scr.Id);
        entlist.add(ent);
        AssetContract__c ent1 =new AssetContract__c(Asset__c=Sys.Id,Contract__c=Scr.Id);
        entlist.add(ent1);
        AssetContract__c ent2 =new AssetContract__c(Asset__c=Sys.Id,Contract__c=Scr.Id);
        entlist.add(ent2);
        insert entlist;        
        
        List<AssetContract__c> en=[select id,Asset__c from AssetContract__c  where id in:entlist];
        delete en;
        
        Test.startTest();
        
        Test_bl_SynchronizationService_Utils.CalloutMock mockImpl = new Test_bl_SynchronizationService_Utils.CalloutMock();
		
		Test.setMock(HttpCalloutMock.class, mockImpl);
		bl_SynchronizationService_Utils.calloutMock = mockImpl;
	}

}