//  Change Log       : PMT-22787: MITG EMEA User Setup - Changed profile from EUR Field Force MITG to EMEA Field Force MITG
@isTest
private class Test_tr_Procedural_Solutions_Kit {
    /*
    private static testmethod void test_BoxConfigurationCheck(){
    	
    	Id sapProductRTId = [Select Id from RecordType where SObjectType = 'Product2' AND DeveloperName = 'SAP_Product'].Id;
    	
    	Product2 prod1 = new Product2();
		prod1.Name = 'Test Product 1';
		prod1.RecordTypeId = sapProductRTId;
		prod1.KitableIndex__c = 'Kitable';
		
		Product2 prod2 = new Product2();
		prod2.Name = 'Test Product 2';
		prod2.RecordTypeId = sapProductRTId;
		prod2.KitableIndex__c = 'Kitable';
		
		insert new List<Product2>{prod1, prod2};
		
		Procedural_Solutions_Kit__c boxNL1 = new Procedural_Solutions_Kit__c();
		boxNL1.Country__c = 'Netherlands';
		boxNL1.Status__c = 'Open';
		
		Procedural_Solutions_Kit__c boxNL2 = new Procedural_Solutions_Kit__c();
		boxNL2.Country__c = 'Netherlands';
		boxNL2.Status__c = 'Open';
		
		Procedural_Solutions_Kit__c boxNL3 = new Procedural_Solutions_Kit__c();
		boxNL3.Country__c = 'Netherlands';
		boxNL3.Status__c = 'Open';
		
		Procedural_Solutions_Kit__c boxBE = new Procedural_Solutions_Kit__c();
		boxBE.Country__c = 'Belgium';
		boxBE.Status__c = 'Open'; 
		
		insert new List<Procedural_Solutions_Kit__c>{boxNL1, boxNL2, boxNL3, boxBE};
		
		// NL Box with Prod 1
		Procedural_Solutions_Kit_Product__c boxNL1Prod = new Procedural_Solutions_Kit_Product__c();
		boxNL1Prod.Procedural_Solutions_Kit__c = boxNL1.Id;
		boxNL1Prod.Product__c = prod1.Id;
		boxNL1Prod.Quantity__c = 1;
		
		// NL Box with Prod 2
		Procedural_Solutions_Kit_Product__c boxNL2Prod = new Procedural_Solutions_Kit_Product__c();
		boxNL2Prod.Procedural_Solutions_Kit__c = boxNL2.Id;
		boxNL2Prod.Product__c = prod2.Id;
		boxNL2Prod.Quantity__c = 1;
		
		// NL Box with Prod 1
		Procedural_Solutions_Kit_Product__c boxNL3Prod = new Procedural_Solutions_Kit_Product__c();
		boxNL3Prod.Procedural_Solutions_Kit__c = boxNL3.Id;
		boxNL3Prod.Product__c = prod1.Id;
		boxNL3Prod.Quantity__c = 1;
		
		// BE Box with Prod 1
		Procedural_Solutions_Kit_Product__c boxBEProd = new Procedural_Solutions_Kit_Product__c();
		boxBEProd.Procedural_Solutions_Kit__c = boxBE.Id;
		boxBEProd.Product__c = prod1.Id;
		boxBEProd.Quantity__c = 1;
		
		insert new List<Procedural_Solutions_Kit_Product__c>{boxNL1Prod, boxNL2Prod, boxNL3Prod, boxBEProd};
		
		Test.startTest();
		
		// Submit NL Box 1
		boxNL1.Status__c = 'Approved';
		update boxNL1;
		
		boxNL1 = [Select Id, Configuration_Hash__c, Configuration_Hash_Global__c, Boxes_with_Same_Config__c from Procedural_Solutions_Kit__c where Id = :boxNL1.Id];
		
		System.assert(boxNL1.Configuration_Hash__c != null);
		System.assert(boxNL1.Configuration_Hash_Global__c != null);
		System.assert(boxNL1.Boxes_with_Same_Config__c == 0);
		
		// Submit NL Box 2
		boxNL2.Status__c = 'Approved';
		update boxNL2;
		
		boxNL2 = [Select Id, Configuration_Hash__c, Configuration_Hash_Global__c, Boxes_with_Same_Config__c from Procedural_Solutions_Kit__c where Id = :boxNL2.Id];
		
		System.assert(boxNL2.Configuration_Hash__c != null);
		System.assert(boxNL2.Configuration_Hash_Global__c != null);
		System.assert(boxNL2.Boxes_with_Same_Config__c == 0);
		
		// Submit NL Box 3
		boxNL3.Status__c = 'Approved';
		update boxNL3;
		
		boxNL3 = [Select Id, Configuration_Hash__c, Configuration_Hash_Global__c, Boxes_with_Same_Config__c from Procedural_Solutions_Kit__c where Id = :boxNL3.Id];
		
		System.assert(boxNL3.Configuration_Hash__c != null);
		System.assert(boxNL3.Configuration_Hash_Global__c == boxNL1.Configuration_Hash_Global__c);
		System.assert(boxNL3.Boxes_with_Same_Config__c == 1);
		
		// Submit BE Box 1
		boxBE.Status__c = 'Approved';
		update boxBE;
		
		boxBE = [Select Id, Configuration_Hash__c, Configuration_Hash_Global__c, Boxes_with_Same_Config__c from Procedural_Solutions_Kit__c where Id = :boxBE.Id];
		
		System.assert(boxBE.Configuration_Hash__c != null);
		System.assert(boxBE.Configuration_Hash_Global__c == boxNL1.Configuration_Hash_Global__c);
		System.assert(boxBE.Boxes_with_Same_Config__c == 0);
    }
    
    private static testmethod void testAutopopulateCountry(){
    	
    	User currentUser = [Select Id, Country_vs__c from User where Id = :UserInfo.getUserId()];
    	
    	Procedural_Solutions_Kit__c box1 = new Procedural_Solutions_Kit__c();
		box1.Country__c = 'Canary Islands';
		box1.Status__c = 'Open';
		
		Procedural_Solutions_Kit__c box2 = new Procedural_Solutions_Kit__c();		
		box2.Status__c = 'Open';
		
		insert new List<Procedural_Solutions_Kit__c>{box1, box2};
		
		box1 = [Select Id, Country__c from Procedural_Solutions_Kit__c where Id = :box1.Id];
		System.assert(box1.Country__c == 'Canary Islands');
		
		box2 = [Select Id, Country__c from Procedural_Solutions_Kit__c where Id = :box2.Id];
		System.assert(box2.Country__c == currentUser.Country__c.toUpperCase());
		
    }
    */
    
    private static testmethod void testSendEmailRSM(){
    	 			
		User testUser = createTestUser();
		
		User currentUser = new User(Id = UserInfo.getUserId());
		
		System.runAs(currentUser){				
			
			PermissionSetASsignment assignment = new PermissionSetASsignment();
	    	assignment.AssigneeId = testUser.Id;
	    	assignment.PermissionSetId = [Select Id from PermissionSet where name = 'Box_Builder_Admin'].Id;
	    	insert assignment;
		}
		
		Test.startTest();
		
		Id sapProductRTId = [Select Id from RecordType where SObjectType = 'Product2' AND DeveloperName = 'SAP_Product'].Id;
    	
    	Product2 prod = new Product2();
		prod.Name = 'Unit Test Product';
		prod.RecordTypeId = sapProductRTId;
		prod.KitableIndex__c = 'Kitable';
		insert prod;
		
		System.runAs(testUser){	
		
			Procedural_Solutions_Kit__c boxNL = new Procedural_Solutions_Kit__c();
			boxNL.Country__c = 'Netherlands';
			boxNL.Status__c = 'Open';
			boxNL.Box_Description__c = 'Unit Test Box';
			boxNL.No_Of_Boxes__c = 4;
			insert boxNL;
					
			// NL Box with Prod
			Procedural_Solutions_Kit_Product__c boxNLProd = new Procedural_Solutions_Kit_Product__c();
			boxNLProd.Procedural_Solutions_Kit__c = boxNL.Id;
			boxNLProd.Product__c = prod.Id;
			boxNLProd.Quantity__c = 1;
			insert boxNLProd;
			
			// Submit NL Box 1
			boxNL.Status__c = 'Approval Pending';
			update boxNL;		
		}
		
		Test.stopTest();		
    }
    
    private static User createTestUser(){
    	//PMT-22787: MITG EMEA User Setup - Changed profile from EUR Field Force MITG to EMEA Field Force MITG
    	Profile eurFF_MITG = [Select Id from Profile where Name = 'EUR Field Force MITG'];
        //Profile eurFF_MITG = [Select Id from Profile where Name = 'EMEA Field Force MITG'];
    	    	
    	User testUser = new User();    	
        testUser.alias = 'uttuser';
        testUser.email='unit_test_user@medtronic.com';
        testUser.emailencodingkey='UTF-8';
        testUser.lastname='Test User';
        testUser.languagelocalekey='en_US';
        testUser.localesidkey='en_US';
        testUser.profileid = eurFF_MITG.Id;
        testUser.timezonesidkey='America/Los_Angeles';
        testUser.Region_vs__c = 'Europe';
        testUser.Sub_Region__c = 'NWE';
        testUser.Country_vs__c = 'NETHERLANDS';   
        testUser.username='unit_test_user@medtronic.com';
        testUser.Alias_unique__c='uttuser';
        testUser.Company_Code_Text__c='EUR';        
        insert testUser;
                        
        return testUser;
    }
    
    private static testmethod void testCreateBoxInSFDC(){
    	//PMT-22787: MITG EMEA User Setup - Changed profile from EUR Field Force MITG to EMEA Field Force MITG
    	User mitgUser = [Select Id from User where Profile.Name in ('EUR Field Force MITG', 'EMEA Field Force MITG') AND IsActive = true AND Id NOT IN (Select AssigneeId from PermissionSetASsignment where PermissionSet.Name = 'Box_Builder_Admin') Limit 1];
        //User mitgUser = [Select Id from User where Profile.Name = 'EMEA Field Force MITG' AND IsActive = true AND Id NOT IN (Select AssigneeId from PermissionSetASsignment where PermissionSet.Name = 'Box_Builder_Admin') Limit 1];
    	
    	Procedural_Solutions_Kit__c box = new Procedural_Solutions_Kit__c();
		
		Boolean isError = false;
		    	
    	System.runAs(mitgUser){
	    	
	    	try{
	    		
	    		insert box;
	    		
	    	}catch(Exception e){
	    		
	    		isError = true;
	    		System.assert(e.getMessage().contains('This record can only be managed in Crossworld or by designated administrators'));			
	    	}
    	}
    	
    	System.assert(isError == true);
    	
    	insert box;
    	
    	isError = false;
    	
	    Procedural_Solutions_Kit_Product__c boxProduct = new Procedural_Solutions_Kit_Product__c();
	    boxProduct.Procedural_Solutions_Kit__c = box.Id;
	    
	    System.runAs(mitgUser){
	    	
	    	try{
	    			    	
	    		insert boxProduct;
	    		
	    	}catch(Exception e){
	    		
	    		isError = true;
	    		System.assert(e.getMessage().contains('This record can only be managed in Crossworld or by designated administrators'));	
	    	}
	    }
		
		System.assert(isError == true);
							    	
	    Procedural_Solutions_Kit_Share__c boxShare = new Procedural_Solutions_Kit_Share__c();
	    boxShare.Procedural_Solutions_Kit__c = box.Id;
	    	    
	    isError = false;
	    
	    System.runAs(mitgUser){
	    	
	    	try{
	    		
	    		insert boxShare;
	    		
	    	}catch(Exception e){
	    		
	    		isError = true;
	    		System.assert(e.getMessage().contains('This record can only be managed in Crossworld or by designated administrators'));	
	    	}
    	}
    	
    	System.assert(isError == true);
    }    
    
    private static testmethod void testCreateSBoxInSFDC_SystemAdmin(){
    	   	
    	Test.startTest();
    	
    	Account acc = new Account();
    	acc.Name = 'Test Account';
    	acc.Account_Country_vs__c = 'BELGIUM';
    	insert acc;
    	
    	Procedural_Solutions_Kit__c box = new Procedural_Solutions_Kit__c();
    	box.Account__c = acc.Id;
    	insert box;
    	
    	Procedural_Solutions_Kit_Product__c boxProduct = new Procedural_Solutions_Kit_Product__c();
    	boxProduct.Procedural_Solutions_Kit__c = box.Id;
    	insert boxProduct;
		    	
    	Procedural_Solutions_Kit_Share__c boxShare = new Procedural_Solutions_Kit_Share__c();
	    	boxShare.Procedural_Solutions_Kit__c = box.Id;
			boxShare.User__c = UserInfo.getUserId();
		insert boxShare;
    }
    
    private static testmethod void testCreateBoxInSFDC_SETAdmin(){
    	
    	User mitgAdmin;

		User currentUser = new User(Id = UserInfo.getUserId());
    	
    	List<PermissionSetAssignment> boxBuilderAdmins = [Select AssigneeId from PermissionSetASsignment where PermissionSet.Name = 'Box_Builder_Admin' AND Assignee.IsActive = true];
    	
    	if(boxBuilderAdmins.size() > 0){
    		
    		mitgAdmin = new User(Id = boxBuilderAdmins[0].AssigneeId);
    		
    	}else{
    		//PMT-22787: MITG EMEA User Setup - Changed profile from EUR Field Force MITG to EMEA Field Force MITG
    		//mitgAdmin = [Select Id from User where Profile.Name = 'EUR Field Force MITG' AND IsActive = true Limit 1];
            mitgAdmin = [Select Id from User where Profile.Name = 'EMEA Field Force MITG' AND IsActive = true Limit 1];
    		
    		PermissionSetASsignment assignment = new PermissionSetASsignment();
    		assignment.AssigneeId = mitgAdmin.Id;
    		assignment.PermissionSetId = [Select Id from PermissionSet where name = 'Box_Builder_Admin'].Id;
    		insert assignment;	
    	}
    	    	   	
    	Test.startTest();
    	
    	System.runAs(mitgAdmin){
    	
	    	Procedural_Solutions_Kit__c box = new Procedural_Solutions_Kit__c();
	    	insert box;
	    	
	    	Procedural_Solutions_Kit_Product__c boxProduct = new Procedural_Solutions_Kit_Product__c();
	    	boxProduct.Procedural_Solutions_Kit__c = box.Id;
	    	insert boxProduct;
			    	
	    	Procedural_Solutions_Kit_Share__c boxShare = new Procedural_Solutions_Kit_Share__c();
	    		boxShare.Procedural_Solutions_Kit__c = box.Id;
				boxShare.User__c = currentUser.Id;
	    	insert boxShare;
	    	
	    	update boxShare;
	    	update boxProduct;
	    	update box;
	    	
	    	delete boxShare;
	    	delete boxProduct;
	    	delete box;
    	}
    }
    
    private static testmethod void testBoxNameGenerator(){
    	
    	Test.startTest();  	
    	    	
    	Procedural_Solutions_Kit__c box = new Procedural_Solutions_Kit__c();    	
    	insert box;
    	
    	box = [Select Name, Name__c, Name_Number_Generator__c from Procedural_Solutions_Kit__c where Id = :box.Id];
    	System.assert(box.Name == box.Name__c);
    	System.assert(box.Name == (box.Name_Number_Generator__c + 'V1'));
    	
    	Procedural_Solutions_Kit__c box2 = new Procedural_Solutions_Kit__c();  
    	box2.Version_of__c = box.Id;  	
    	insert box2;
    	
    	box2 = [Select Name, Name__c, Name_Number_Generator__c from Procedural_Solutions_Kit__c where Id = :box2.Id];
    	System.assert(box2.Name == box2.Name__c);
    	System.assert(box2.Name == (box.Name_Number_Generator__c + 'V2'));
    	
    	Procedural_Solutions_Kit__c box3 = new Procedural_Solutions_Kit__c();  
    	box3.Version_of__c = box2.Id;  	
    	insert box3;
    	
    	box3 = [Select Name, Name__c, Name_Number_Generator__c, Version_of__c from Procedural_Solutions_Kit__c where Id = :box3.Id];
    	System.assert(box3.Version_of__c == box.Id);
    	System.assert(box3.Name == box3.Name__c);
    	System.assert(box3.Name == (box.Name_Number_Generator__c + 'V3'));
    }
    
    @TestSetup
    private static void setupSettings(){
    	
    	SystemAdministratorProfileId__c ids = SystemAdministratorProfileId__c.getInstance('Default');
    	
    	if(ids == null){
    		ids = new SystemAdministratorProfileId__c();
    		ids.Name = 'Default';
    	}
    	
    	if(ids.SystemAdministrator__c == null){
    		
    		ids.SystemAdministrator__c = String.valueOf([Select Id from Profile where Name = 'System Administrator'].Id).substring(0,15);
    		upsert ids;
    	}
    	
    	Procedural_Solutions_Kit_Email__c email = new Procedural_Solutions_Kit_Email__c();
    	email.Name = 'RS EMEA';
    	email.Email__c = 'test@medtronic.com';
    	insert email;
    }
}