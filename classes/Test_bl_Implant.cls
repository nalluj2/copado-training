/*
 *      Created Date : 201401
 *      Description : This class tests the logic in the bl_Implant class
 *
 *      Author = Bart Caelen
 */
@IsTest(seeAllData=true)
private class Test_bl_Implant {

    private static Map<String, String> dataMap = new Map<String, String>();
	private static Implant__c oImplant = new Implant__c();
	
	private static RecordType oRT_AFS_Cryo_AF;
	private static RecordType oRT_AFS_Focal_Cryo;
	private static RecordType oRT_AFS_Phased_RF;


    @isTest static void test_All() {
    	
		//- CREATE TEST DATA
		createTestData();
		
		//- PERFORM TESTS
		Test.startTest();
    	bl_Implant.bIsAFS(oRT_AFS_Cryo_AF.Id);
    	bl_Implant.convertSFDCID(oImplant.Id);
    	bl_Implant.convertSFDCID('WRONGID');
    	bl_Implant.loadTherapyData(oImplant.Business_Unit__c);
    	bl_Implant.tTherapyForRecordTypeId(oRT_AFS_Cryo_AF.Id);
		Test.stopTest();
    	
    }
	
	// !! The creation of this test data should be centralised so that it can be easily reused in other test classes !! //
	private static void createTestData(){

    	//- LOAD TEST DATA
    	oRT_AFS_Cryo_AF = RecordTypeMedtronic.getRecordTypeByDevName('Implant__c', 'AFS_Cryo_AF');
     	oRT_AFS_Focal_Cryo = RecordTypeMedtronic.getRecordTypeByDevName('Implant__c', 'AFS_Focal_Cryo');
     	oRT_AFS_Phased_RF = RecordTypeMedtronic.getRecordTypeByDevName('Implant__c', 'AFS_Phased_RF');
    	
    	
    	//- CREATE TEST DATA
        // Create test sObjects 
        Recordtype accRT=[select id,name from recordtype where sobjecttype='Account' and name=:'Competitor'];
        Account acc1 = new Account();
        acc1.Name = 'TEST_triggerImplantBeforeInsert TestAccount1' ;
        acc1.recordtypeid=accRT.id; 
        //insert acc1;

        Account acc2 = new Account();
        acc2.Name = 'TEST_triggerImplantBeforeInsert TestAccount2' ; 
        //insert acc2;
        
        List<Account> testAccounts = new List<Account>();
        testAccounts.add(acc1);
        testAccounts.add(acc2);
        
        insert testAccounts;

        
        Contact cont1 = new Contact();
        cont1.LastName = 'TEST_triggerImplantBeforeInsert TestCont1' ;  
        cont1.FirstName = 'TEST' ;  
        cont1.AccountId = acc1.Id ;
        cont1.Contact_Active__c = true  ;
        cont1.Integrate_with_MMX__c = true;
        cont1.Contact_Department__c = 'Diabetes Adult'; 
        cont1.Contact_Primary_Specialty__c = 'ENT';
        cont1.Affiliation_To_Account__c = 'Employee';
        cont1.Primary_Job_Title_vs__c = 'Manager';
        cont1.Contact_Gender__c = 'Male'; 
        //insert cont1;
        
        Contact cont2 = new Contact();
        cont2.LastName = 'TEST_triggerImplantBeforeInsert TestCont2';  
        cont2.FirstName = 'TEST' ;  
        cont2.AccountId = acc1.Id ;
        cont2.Contact_Active__c = true;
        cont2.Integrate_with_MMX__c = true;
        cont2.Contact_Department__c = 'Diabetes Adult'; 
        cont2.Contact_Primary_Specialty__c = 'ENT';
        cont2.Affiliation_To_Account__c = 'Employee';
        cont2.Primary_Job_Title_vs__c = 'Manager';
        cont2.Contact_Gender__c = 'Male';         
        //insert cont2;
        
        List<Contact> testContacts = new List<Contact>();
        testContacts.add(cont1);
        testContacts.add(cont2);
        insert testContacts;
                
        Affiliation__c aff = new Affiliation__c();
        aff.Affiliation_To_Contact__c = testContacts.get(0).Id;
        aff.Affiliation_To_Account__c = testAccounts.get(0).Id;
        aff.Affiliation_From_Contact__c = testContacts.get(1).Id;
        aff.Affiliation_From_Account__c = testAccounts.get(1).Id;
        aff.Affiliation_Type__c = 'Member';
        aff.RecordTypeId = FinalConstants.recordTypeIdC2A;
        insert aff;
        
        Affiliation_Details__c affDetails = new Affiliation_Details__c();
        affDetails.Affiliation_Details_Affiliation__c = aff.Id;
        affDetails.Affiliation_Details_Start_Date__c = Date.newInstance(Date.today().year(), 1, 1);
        affDetails.Affiliation_Details_End_Date__c = Date.newInstance(Date.today().year(), 12, 31);
        insert affDetails;
        string DATE_OF_SURGERY='';         
        dataMap.put('contactToId',testContacts.get(0).Id);
        dataMap.put('contactFromId',testContacts.get(1).Id);
        dataMap.put('accountToId',testAccounts.get(0).Id);
        dataMap.put('accountFromId',testAccounts.get(1).Id);
        dataMap.put('retURL', '/');
        dataMap.put('action', 'Implant');
        dataMap.put(DATE_OF_SURGERY,'2011-08-04 14:55');

       //Insert Company
       Company__c cmpny = new Company__c();
       cmpny.name='TestMedtronic';
       cmpny.CurrencyIsoCode = 'EUR';
       cmpny.Current_day_in_Q1__c=56;
       cmpny.Current_day_in_Q2__c=34; 
       cmpny.Current_day_in_Q3__c=5; 
       cmpny.Current_day_in_Q4__c= 0;   
       cmpny.Current_day_in_year__c =200;
       cmpny.Days_in_Q1__c=56;  
       cmpny.Days_in_Q2__c=34;
       cmpny.Days_in_Q3__c=13;
       cmpny.Days_in_Q4__c=22;
       cmpny.Days_in_year__c =250;
       cmpny.Company_Code_Text__c = 'T41';
       insert cmpny;        

        Business_Unit__c bu = new Business_Unit__c();
        bu.name = 'Testing BU';
        bu.abbreviated_name__c = 'Abb Bu';
        bu.Company__c = cmpny.Id;
        insert bu;

        //Insert SBU
        Sub_Business_Units__c sbu1 = new Sub_Business_Units__c();
        sbu1.name='SBUMedtronic1';
        sbu1.Business_Unit__c=bu.id;
        insert sbu1;
        
        
        //Insert Therapy Group
        Therapy_Group__c tg = new Therapy_Group__c();
        tg.Name='Therapy Group';
        tg.Company__c = cmpny.Id;
        tg.Sub_Business_Unit__c = sbu1.Id;
        insert tg;

        map<String, String> RecrodTypeMapping = new map<String, String>();
        list<Recordtype> rt = [Select Id,Name From RecordType where SobjectType='product2' and IsActive=true and name='MDT Marketing Product'];
        for(RecordType r:rt){
            RecrodTypeMapping.put(string.ValueOf(r.Name),string.ValueOf(r.Id));
        }
        String RecType=RecrodTypeMapping.get('MDT Marketing Product');

        
        List<Product2> prodList=new List<Product2>();
        
        Product2 pro1 = new Product2();
        pro1.Name = 'Test Product';
        pro1.Business_Unit_ID__c = bu.id;
        pro1.Consumable_Bool__c = true;
        pro1.IsActive = true;
        pro1.recordtypeid = RecType;
        pro1.Manufacturer_Account_ID__c = acc1.id;//'001P000000R0kKd'; 
        //insert pro1;
        prodList.add(pro1);
        
        
        Product2 pro2 = new Product2();
        pro2.Name = 'Test Product1';
        pro2.Business_Unit_ID__c = bu.id;
        pro2.Consumable_Bool__c = true;
        //pro1.IsActive = true;
        pro2.recordtypeid = RecType;
        pro2.Manufacturer_Account_ID__c = acc1.id;//'001P000000R0kKd';        
        prodList.add(pro2);
        insert prodList;
            
        Implant__c implant=new Implant__c();
        
        dataMap.put('URL','/apex/implantSelectorPage?core.apexpages.devmode.url=1&retURL=%2Fhome%2Fhome.jsp&save_new=1&sfdc.override=1');
        dataMap.put('DATE_OF_SURGERY','2011-08-04 14:55');        
        //this.testController(implant, false, pro1);


        Therapy__c th = new Therapy__c();
        th.name='test ## th';
        th.Business_Unit__c = bu.id;
        th.Sub_Business_Unit__c = sbu1.Id;
        th.Therapy_Group__c = tg.Id;
        th.Therapy_Name_Hidden__c = 'test ## th';
        insert th;

        implant.Business_Unit__c=bu.id;
        implant.Implant_Implanting_Contact__c = testContacts.get(1).Id;
        implant.Implant_Implanting_Account__c = testAccounts.get(0).Id;
        implant.Implant_Date_Of_Surgery__c = System.today() - 1;
        implant.Therapy__c = th.Id;
        implant.Attended_Implant_Text__c = 'UnAttended';
        implant.Duration_Nr__c = 10;
        implant.Procedure_Text__c='Replacement';
        implant.Manufacturer_ID__c = acc1.id;//'0012000000kvvDU';  // 001P000000R0kKd // Staging Medtronic Competitor Account Id
        implant.Implant_Manufacturer_ID__c=acc1.id;        
        //try{
            insert implant ;            
       // } catch (Exception e) { }
        
        

        Implant_Option__c IO = new Implant_Option__c();
        IO.Type_Text__c = 'Cause';
        insert IO;
        
        Implant_Option_Therapy__c IOT = new Implant_Option_Therapy__c();
        IOT.Implant_Option_ID__c = IO.id;
        IOT.Therapy_ID__c = th.id;
        insert IOT;
        
        Implant_Detail_Junction__c IDJ = new Implant_Detail_Junction__c();
        IDJ.Implant_ID__c = implant.id;
        IDJ.Implant_Option_ID__c = IO.id;       
        insert IDJ;

        Product2 pro = new Product2();
        pro.Name = 'Test Product11';
        pro.Business_Unit_ID__c = bu.id;
        pro.Consumable_Bool__c = true;
        pro.IsActive = true;
        pro.recordtypeid = RecType;
        insert pro;
        
        Therapy_Product__c TP = new Therapy_Product__c();
        TP.Therapy__c = th.id;
        TP.Product__c = pro.id;
        Insert TP;
        
        Asset A = new Asset();
        A.Name = 'Test Asset';
        A.AccountId = acc1.id;
        //A.Implant_ID__c = implant.id;
        A.Quantity = 10;
        A.Product2Id = pro.Id; 
        A.Serial_Nr__c = clsUtil.getNewGUID();
        insert A;
        
        dataMap.put('URL','/apex/implantSelectorPage?core.apexpages.devmode.url=1&retURL=%2Fhome%2Fhome.jsp&save_new=1&sfdc.override=1&id='+implant.Id);
        dataMap.put('id', implant.Id);
        dataMap.put('retURL',null);
        dataMap.put('DATE_OF_SURGERY','2011-08-04 14:55');
           
       	oImplant = implant;    
            			
	}


}