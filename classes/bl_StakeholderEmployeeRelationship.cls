public with sharing class bl_StakeholderEmployeeRelationship {
    
    public static void calculateStakeholderMappingRelationship(List<Stakeholder_Employee_Relationship__c> triggerNew, Map<Id, Stakeholder_Employee_Relationship__c> oldMap){
    	
    	Set<Id> stakeholderMappingIds = new Set<Id>();
    	
    	for(Stakeholder_Employee_Relationship__c employeeRel : triggerNew){
    		
    		//Created, deleted or undeleted
    		if(oldMap == null){
    			
    			stakeholderMappingIds.add(employeeRel.MDT_Contact__c);
    			
    		//Updated	
    		}else{
    			
    			Stakeholder_Employee_Relationship__c oldEmployeeRel = oldMap.get(employeeRel.Id);
    			
    			if(employeeRel.MDT_Contact__c != oldEmployeeRel.MDT_Contact__c ||
    				employeeRel.Relationship_Quality__c != oldEmployeeRel.Relationship_Quality__c){
    				
    				stakeholderMappingIds.add(employeeRel.MDT_Contact__c);		
    				stakeholderMappingIds.add(oldEmployeeRel.MDT_Contact__c);
    			}
    		}    		
    	}
    	
    	if(stakeholderMappingIds.size() > 0){
    		
    		List<Stakeholder_Mapping__c> stakeholderMappings = [Select Id, Relationship_with_Medtronic_IHS__c , (Select Relationship_Quality__c from Stakeholder_Employee_Relationships__r) from Stakeholder_Mapping__c where Id IN :stakeholderMappingIds];
    		
    		for(Stakeholder_Mapping__c stakeholderMapping : stakeholderMappings){
    			
    			Decimal totalQuality = 0;
    			
    			for(Stakeholder_Employee_Relationship__c employeeRel : stakeholderMapping.Stakeholder_Employee_Relationships__r){
    				
    				totalQuality += Integer.valueOf(employeeRel.Relationship_Quality__c);
    			}
    			
    			if(stakeholderMapping.Stakeholder_Employee_Relationships__r.size() == 0){
    				
    				stakeholderMapping.Relationship_with_Medtronic_IHS__c = null;
    				
    			}else{
    			
    				Decimal averageQuality = totalQuality.divide(stakeholderMapping.Stakeholder_Employee_Relationships__r.size(), 0, System.RoundingMode.HALF_UP);
    			
    				stakeholderMapping.Relationship_with_Medtronic_IHS__c = String.valueOf(averageQuality);
    			}
    		}
    		
    		update stakeholderMappings;
    	}
    }
}