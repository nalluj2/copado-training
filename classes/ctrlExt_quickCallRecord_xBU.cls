public with sharing class ctrlExt_quickCallRecord_xBU {
	
	public Call_Records__c callRecord {get; set;}
    public Call_Topics__c callTopic {get; set;}
    public Contact_Visit_Report__c callContact {get; set;}
        
    public Boolean isSaveError {get; set;}
    
    public String[] selectedBUs {get; set;}
    public String[] selectedSubjects {get; set;}
    
	public List<SelectOption> contacts {get; set;}
    public List<SelectOption> accounts {get; set;}
    public List<SelectOption> activityTypes {get; set;}
    public List<SelectOption> bus {get; set;}
    public List<SelectOption> subjects {get; set;}

	public Boolean lobbyingMandatory {get; set;}
	public Boolean lobbyingRender {get; set;}
    
    public Company__c userCompany {get;set;}
    
    private String xBURecordTypeId;
	
	public ctrlExt_quickCallRecord_xBU(ApexPages.StandardController sc){
		
		if(Test.isRunningTest() == false) sc.addFields(new List<String>{'Name', 'AccountId'});
    	
        User currentUser = [select Company_Code_Text__c from User where id = :UserInfo.getUserId()];
		//if (Test.isRunningTest()){
		//	userCompany = [SELECT Id FROM Company__c where Company_Code_Text__c = 'T35'];
		//}else{
			userCompany = [SELECT Id FROM Company__c where Company_Code_Text__c = :currentUser.Company_Code_Text__c];
		//}

    	Contact contact = (Contact) sc.getRecord();
    	
    	contacts = new List<SelectOption>();
        contacts.add(new SelectOption(contact.Id, contact.Name));
        
        callRecord = new Call_Records__c();
        RecordType xbuRT = [Select Id from RecordType where SobjectType = 'Call_Records__c' AND DeveloperName = 'xBU'];
        callRecord.RecordTypeId = xbuRT.Id;
        xBURecordTypeId = String.valueOf(xbuRT.Id).substring(0, 15);
        callRecord.Call_Date__c = System.today();
                
        callTopic = new Call_Topics__c();
        
        callContact = new Contact_Visit_Report__c();
        callContact.Attending_Contact__c = contact.Id;
        callContact.Attending_Affiliated_Account__c = contact.AccountId;
                 
        loadContactAccounts();
        loadActivities();

		// The lobbying is for both: BU & xBU
		if(Schema.SObjectType.Call_Topics__c.fields.Call_Topic_Lobbying__c.Accessible){
			lobbyingMandatory = true;
			lobbyingRender = true;
		} else {
			lobbyingMandatory = false;
			lobbyingRender = false;
		}
	}
	
	private void loadContactAccounts() {
            
        accounts = new List<SelectOption>();
                
        for(Affiliation__c aff : [SELECT Affiliation_To_Account__c, Affiliation_To_Account__r.Name FROM Affiliation__c
                                        where RecordType.DeveloperName = 'C2A'
                                        and Affiliation_Active__c = true
                                        and (Affiliation_Type__c <> 'Referring')
                                        and Affiliation_To_Account__r.Id != null
                                        and Affiliation_From_Contact__c=:callContact.Attending_Contact__c
                                        ORDER BY Affiliation_To_Account__r.Name ASC]){
                                        	
            String id = aff.Affiliation_To_Account__c;
            String name = aff.Affiliation_To_Account__r.Name;
                
        	accounts.add(new SelectOption(id,name));
        }               
    }
    
    private void loadActivities(){
    	
    	activityTypes = new List<SelectOption>();
    	activityTypes.add(new SelectOption('','--None--'));
    	
    	for(Call_Topic_Settings__c cs : [ SELECT Call_activity_type__c, Call_activity_type__r.name                                       
                        					FROM Call_Topic_Settings__c WHERE Call_activity_type__c<>null
                            				AND Call_activity_type__c<>''
                            				AND Call_Record_Record_Type__c=:xBURecordTypeId 
                            				AND Call_activity_type__r.Company_ID__c = :userCompany.id
                            				ORDER BY Call_activity_type__r.name ASC ]){  

	        String id = cs.Call_activity_type__c;
	        String name=cs.Call_activity_type__r.Name;
	        activityTypes.add(new SelectOption(id, Name));
		}    	    
    }
    
    private void loadBUs(){
    	
    	Set<id> companyIds = new Set<id>();
    	
    	bus = new List<SelectOption>();
                           
		for(Call_Topic_Settings__c cs : [Select Company_ID__c from Call_Topic_Settings__c where 
	                                         Company_ID__c<>null AND Company_ID__c<>''
	                                         AND Call_Record_Record_Type__c=:xBURecordTypeId and Call_activity_type__c=:callTopic.Call_Activity_Type__c ]){

	        companyIds.add(cs.Company_ID__c);                                
        }
        
        if(companyIds.size()>0){ 
			for(Business_Unit__c bu:[select Id, Name from Business_Unit__c where Company__c IN:companyIds ORDER BY Name ASC]){
				bus.add(new SelectOption(bu.Id,bu.Name));
          	}
		}            	
    }
            
    private void loadSubjects(){
    	
    	subjects = new List<SelectOption>();
              
        for (Call_topic_Setting_Details__c subject: [Select Subject__c,Subject__r.Name From Call_topic_Setting_Details__c where Call_Topic_Setting__r.Business_Unit__c = NULL AND Call_Topic_Setting__r.Call_activity_type__c =:callTopic.Call_Activity_Type__c ORDER BY Subject__r.Name]) {
        
             String Id = subject.Subject__c;
             String name=subject.Subject__r.Name;
                                 
             subjects.add(new SelectOption(Id,name));     
        }    	
    }
    
    public void activityTypeChanged(){
    	    	
    	selectedSubjects = null;
    	selectedBUs = null;    	    	   	
        loadSubjects(); 
		loadBUs();         	
    }
    
    public PageReference save(){
    	
    	isSaveError = false;
    	    	
    	if (callContact.Attending_Affiliated_Account__c == null) {    		
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Related Account is required'));            
            isSaveError=true;
    	}
    	
    	if (callTopic.Call_Activity_Type__c == null) {    		
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Activity Type is required'));            
            isSaveError=true;
    	}
    	
    	if ((callRecord.Call_Channel__c == 'Phone call' || callRecord.Call_Channel__c == 'Skype') && callRecord.Inbound_Outbound__c == null) {    		
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'When Call Channel is \'Phone call\' or \'Skype\' you must indicate if it was Inbound or Outbound'));            
            isSaveError=true;
    	}
    	    	    	
    	if (callRecord.Call_Date__c == null) {            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Call Date is required'));            
            isSaveError=true;
        }else if (callRecord.Call_Date__c > system.today()) {            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Call Date cannot be in the future'));            
            isSaveError=true;
        }
        
        if (callTopic.Call_Topic_Duration__c != null && callTopic.Call_Topic_Duration__c < 0) {           
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Duration cannot be a negative value'));
        	isSaveError = true ;           
        }

		if(lobbyingMandatory == true && (callTopic.Call_Topic_Lobbying__c == '' || callTopic.Call_Topic_Lobbying__c == null)){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Lobbying is required'));
			isSaveError = true ;
		}
    	    	
    	if(isSaveError == true) return null;
    	
    	SavePoint sp = Database.setSavepoint();
    	
    	try{
    		
    		callRecord.Mobile_created_timestamp__c = DateTime.now();    		
    		callRecord.SF1_Call_Recording__c = true;
    		insert callRecord;
    		
    		//Usage detail
    		Application_Usage_Detail__c usage = new Application_Usage_Detail__c();
    		usage.Process_Area__c = 'Call Management';
    		usage.Source__c = 'SF1';
    		usage.Action_Type__c = 'Create';
    		usage.Capability__c = 'Call Record';
    		usage.Object__c = 'Call_Records__c';
    		usage.Action_Date__c = Date.today();
    		usage.Action_DateTime__c = DateTime.now();
    		usage.Internal_Record_Id__c = callRecord.Id;
    		usage.External_Record_Id__c = callRecord.Id;
    		usage.User_Id__c = UserInfo.getUserId();
    		
    		insert usage;
    		
    		callContact.Call_Records__c = callRecord.Id;
    		insert callContact;
    		
    		Call_Topic_Settings__c activity = [Select Call_Category__c from Call_Topic_Settings__c where Business_Unit__c =:callRecord.Business_Unit__c and Call_activity_type__c =:callTopic.Call_Activity_Type__c limit 1];
            callTopic.Call_Category__c=activity.Call_Category__c;    		
    		callTopic.Call_Records__c= callRecord.Id;
    		insert callTopic;
    		
    		//Topic Subjects
    		List<Call_Topic_Subject__c> topicSubjects = new List<Call_Topic_Subject__c>();
    		
    		for(String subjectId : selectedSubjects){
    			
    			Call_Topic_Subject__c topicSubject = new Call_Topic_Subject__c();
    			topicSubject.Call_Topic__c = callTopic.Id;
    			topicSubject.Subject__c = subjectId;
    			
    			topicSubjects.add(topicSubject);
    		}
    		
    		if(topicSubjects.size()>0) insert topicSubjects;
    		
    		//Topic Business Units
    		List<Call_Topic_Business_Unit__c> topicBUs = new List<Call_Topic_Business_Unit__c>();
    		
    		for(String buId : selectedBUs){
    			
    			Call_Topic_Business_Unit__c topicBU = new Call_Topic_Business_Unit__c();
    			topicBU.Call_Topic__c = callTopic.Id;
    			topicBU.Business_Unit__c = buId;
    			
    			topicBUs.add(topicBU);
    		}
    		
    		if(topicBUs.size()>0) insert topicBUs;
    		
    	}catch(Exception e){
    		
    		ApexPages.addMessages(e);
    		
    		callRecord.Id = null;
    		callContact.Id = null;
    		callTopic.Id = null;
    		
    		isSaveError=true;
    		
    		Database.rollback(sp);
    		
    		if(Test.isRunningTest() == true) throw e;
    		
    		return null;
    	}
    	    	    	
    	return null;
    }      
}