/**
 *      @author -       Servicemax, Inc
 *      @date   -       04/09/2020 (dd/mm/yyyy)
 *      @description    Apex class clone service report reference from parent to child work orders
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------            
        Adrian Modolea                 04/09/2020         Created the class
        Test coverage                  dd/mm/2020         TODO class is covering test coverage for this class.
        Adrian Modolea                 16/04/2021         Add logic to generate PDF on child WOs
*/
global class SVMX_ServiceReportReplicationBatch implements Database.Batchable<sObject>, Schedulable, Database.Stateful
{
    public List<Attachment> allNewAttachments = new List<Attachment>();

    /****************************************************************************
     * Batch constructor method
     ****************************************************************************/ 
    global SVMX_ServiceReportReplicationBatch() {} 

    /****************************************************************************
     * Method called at the beginning of a batch Apex job
     * @param bc context contains the job ID
     * @return Database.QueryLocator object that contains the records being passed into the job
     ****************************************************************************/     
    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator([SELECT Id, SVMX_PS_Service_Report_Date_Time__c,
                                            (SELECT Id, SVMXC__Order_Status__c, SVMX_PS_Language__c, SVMXC__Completed_Date_Time__c 
                                                FROM Work_Orders__r WHERE SVMX_Debrief_Complete__c=true),
                                            (SELECT Id FROM Attachments 
                                                WHERE Name LIKE '%Site_Visit_Service_Report%' OR Name LIKE 'SFM%'
                                                ORDER BY CreatedDate DESC LIMIT 1)
                                        FROM SVMXC__Service_Order__c
                                        WHERE SVMX_Parent_Work_Order__c=null
                                            AND SVMX_PS_Service_Report_To_Share__c=true]); 
    }

    /****************************************************************************
     * Method to process each chunk of records in the batch job
     * @param   bc context contains the job ID
     *          scope list of sObjects to process for a specific chunk
     ****************************************************************************/  
    global void execute(Database.BatchableContext bc, List<SVMXC__Service_Order__c> scope)
    {
        Set<Id> attachmentIds = new Set<Id>();
        Map<Id, List<SVMXC__Service_Order__c>> workOrders = new Map<Id, List<SVMXC__Service_Order__c>>();
        
        List<Attachment> attachmentsToCreate = new List<Attachment>();
        List<SVMXC__Service_Order__c> workOrdersToUpdate = new List<SVMXC__Service_Order__c>();

        for(SVMXC__Service_Order__c parentWO : scope)
        {
            workOrders.put(parentWO.Id, new List<SVMXC__Service_Order__c>());
            for(SVMXC__Service_Order__c childWO : parentWO.Work_Orders__r) {
                workOrders.get(parentWO.Id).add(childWO);

                // PDF Service Report on child Work Orders
                String language = childWO.SVMX_PS_Language__c;
				Blob contentPDF;
                // Generate the PDF content
                PageReference vfPage = Page.SVMX_PS_Service_Report_Standalone;
                if(language == 'iw')
                    vfPage = Page.SVMX_PS_Service_Report_Standalone_Hebrew;
                vfPage.getParameters().put('id', childWO.Id);
                 
                if(Test.isRunningTest()) { 
  				    contentPDF = blob.valueOf('Test Class');
            	}
                else{
                    contentPDF = vfPage.getContentAsPDF();
                }
                
                // Create a new attachment
                Attachment attach = new Attachment();
                attach.Body = contentPDF;
                attach.ContentType = 'application/pdf';
                attach.ParentId = childWO.Id;
                DateTime completedDate = parentWO.SVMX_PS_Service_Report_Date_Time__c;
                if(completedDate == null)
                    completedDate = childWO.SVMXC__Completed_Date_Time__c;
                String formattedDate = completedDate.formatGMT('yyyyMMddHHmmss');
                attach.Name = 'SVMX_Service_Report_'+language+'_'+childWO.Id+'_'+formattedDate+'.pdf';
                attachmentsToCreate.add(attach);
            }
            
            for(Attachment att : parentWO.Attachments)
                attachmentIds.add(att.Id);
        }

        for(Attachment att : [SELECT Id, ParentId, Name, Body, Description FROM Attachment WHERE Id IN :attachmentIds])
        {
            for(SVMXC__Service_Order__c childWO : workOrders.get(att.ParentId))
            {
                Attachment newAtt = att.clone(false, false, false, false);
                newAtt.ParentId = childWO.Id;
                attachmentsToCreate.add(newAtt);
                if(childWO.SVMXC__Order_Status__c == 'On Hold' || childWO.SVMXC__Order_Status__c == 'Awaiting Parts') {
                    childWO.SVMX_Parent_Work_Order__c = null;
                    workOrdersToUpdate.add(childWO);
                }
            }

            SVMXC__Service_Order__c parentWO = new SVMXC__Service_Order__c(Id=att.ParentId);
            parentWO.SVMX_PS_Service_Report_To_Share__c = false;
            workOrdersToUpdate.add(parentWO);
        }

        if(attachmentsToCreate.size() > 0) {
            insert attachmentsToCreate;
            for(Attachment att : attachmentsToCreate)
                allNewAttachments.add(new Attachment(Id=att.Id, ParentId=att.ParentId));
        }
        if(workOrdersToUpdate.size() > 0)
            update workOrdersToUpdate;
    }

    /****************************************************************************
     * Method called after all batches are processed to execute post-processing operations
     * @param bc context contains the job ID
     ****************************************************************************/ 
    global void finish(Database.BatchableContext bc) {
        if(allNewAttachments.size() > 0) {
    	    bl_NotificationSAP.sendNotificationToSAP_Attachment(allNewAttachments, 'INSERT');
    	}
    }

    /****************************************************************************
     * Method to instantiate and schedule an apex batch class 
     * @param sc schedulable context contains the job ID
     ****************************************************************************/
    global void execute(SchedulableContext sc)
    {
        SVMX_ServiceReportReplicationBatch batch = new SVMX_ServiceReportReplicationBatch();
        Database.executeBatch(batch, 50);
    }    
}