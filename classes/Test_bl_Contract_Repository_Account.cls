@isTest private class Test_bl_Contract_Repository_Account {
    
    private static testMethod void testContractRepositoryAccount(){
        
        Account testAcc1 = new Account();
        testAcc1.Name = 'Test Account 1';
        testAcc1.Account_Country_vs__c = 'BELGIUM';
        
        Account testAcc2 = new Account();
        testAcc2.Name = 'Test Account 2';
        testAcc2.Account_Country_vs__c = 'BELGIUM';
        
        insert new List<Account>{testAcc1, testAcc2};
        
        Contract_Repository__c contract = new Contract_Repository__c();
        contract.Account__c = testAcc1.Id;
        contract.Primary_Contract_Type__c = 'Rebate agreement';
        contract.Rebate_Type__c = 'Straight Unit';
        contract.MPG_Code__c = 'A1;A2;A3';
        //contract.Payment_Options__c = 'Credit Note';
        contract.put('Payment_Options__c','Credit Note');
        contract.Pre_Payment_Terms__c = 'Final Payment';
        contract.Payment_Period__c = 'Monthly';     
        contract.Original_Valid_From_Date__c = Date.today();
        contract.Original_Valid_To_Date__c = Date.today().addYears(1);      
        insert contract;
                        
        Test.startTest();
                
        List<Contract_Repository_Account__c> contractAcc = [Select Id, Account__c, Unique_Key__c from Contract_Repository_Account__c where Contract_Repository__c = :contract.Id];
        
        System.assert(contractAcc.size() == 1);
        System.assert(contractAcc[0].Account__c == testAcc1.Id);
        System.assert(contractAcc[0].Unique_Key__c == contract.Id + ':' + testAcc1.Id);
        
        Boolean isError = false;
        
        try{
            
            delete contractAcc;
            
        }catch(Exception e){
            
            isError = true;
            System.assert(e.getMessage().contains('You cannot delete or modify the record for the Contract\'s primary account'));
        }
        
        System.assert(isError == true);
                
        isError = false;
        
        try{
            
            contractAcc[0].Account__c = testAcc2.Id;
            update contractAcc;
            
        }catch(Exception e){
            
            isError = true;
            System.assert(e.getMessage().contains('You cannot delete or modify the record for the Contract\'s primary account'));
        }
        
        System.assert(isError == true);
        
        isError = false;
        
        try{
            
            Contract_Repository_Account__c duplicate = new Contract_Repository_Account__c();
            duplicate.Account__c = testAcc1.Id;
            duplicate.Contract_Repository__c = contract.Id;
            insert duplicate;
            
        }catch(Exception e){
            
            isError = true;
            System.assert(e.getMessage().containsIgnoreCase('duplicate'));
        }
        
        System.assert(isError == true);
        
    }
}