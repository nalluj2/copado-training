public with sharing class ctrl_Account_Plan_Objective_RTG_Mobile {
	
	public Boolean isSaveError {get; set;}
	
	public Account_Plan_Objective__c objective {get; set;}
	public Business_Objective__c businessObjective {get; set;}
	
	public List<SelectOption> businessObjectiveOptions {get; set;}
	
	public ctrl_Account_Plan_Objective_RTG_Mobile(ApexPages.StandardController sc){
		
		isSaveError = false;
		
		Id recordId = sc.getId();
		
		if(Schema.Account_Plan_2__c.SObjectType == recordId.getSobjectType()){
		
			Account_Plan_2__c plan = (Account_Plan_2__c) sc.getRecord();
									
			objective = new Account_Plan_Objective__c();						
			objective.Account_Plan__c = plan.Id;
			objective.RecordTypeId = [Select Id from RecordType where SObjectType = 'Account_Plan_Objective__c' AND DeveloperName = 'RTG'].Id;
			
		}else{
			
			if(!Test.isrunningTest()){
				sc.addFields(new List<String>{'Account_Plan__c'});	
			}
			
			objective = (Account_Plan_Objective__c) sc.getRecord();					
		}
				
		Account_Plan_2__c accPlan = [Select Id, Account_Plan_Level__c, Business_Unit__c, Sub_Business_Unit__c, Business_Unit_Group__c from Account_Plan_2__c where Id = :objective.Account_Plan__c];
		
		String businessObjectiveFilter;
		
		if(accPlan.Account_Plan_Level__c == 'Business Unit Group'){
			
			businessObjectiveFilter = ' WHERE Business_Unit_Group__c = \''+ accPlan.Business_Unit_Group__c + '\' AND Active__c = true ';

		}else if(accPlan.Account_Plan_Level__c == 'Business Unit'){
			
			businessObjectiveFilter = ' WHERE Business_Unit__c = \''+ accPlan.Business_Unit__c + '\' AND Active__c = true ';
			
		}else if(accPlan.Account_Plan_Level__c == 'Sub Business Unit'){
			
			businessObjectiveFilter = ' WHERE Sub_Business_Unit__c = \''+ accPlan.Sub_Business_Unit__c + '\' AND Active__c = true ';
		}
		
		businessObjectiveOptions = new List<SelectOption>();		
		businessObjectiveOptions.add( new SelectOption('', '--None--'));
		
		for(SObject businessObjective : Database.query('Select Id, Name from Business_Objective__c ' + businessObjectiveFilter + ' ORDER BY Name')){
			
			businessObjectiveOptions.add( new SelectOption( businessObjective.Id, String.valueOf(businessObjective.get('Name'))));			
		}
		
		if(objective.Id != null) businessObjectiveSelected();
	}
	
	public void businessObjectiveSelected(){
		
		businessObjective = null;
				
		if(objective.Business_Objective__c != null){
			
			List<Business_Objective__c> businessObjectives = Database.query('Select Id, Explanation__c, Type__c, Target_Date__c from Business_Objective__c WHERE Id = \'' + objective.Business_Objective__c + '\'');
			
			if(businessObjectives.size() > 0) businessObjective = businessObjectives[0];
			else objective.Business_Objective__c = null;
		}	
	}
	
	public void upsertObjective(){
		
		if(objective.Business_Objective__c == null){
			ApexPages.addMessage(new ApexPages.Message( ApexPages.Severity.Error, 'Required field is missing: Objective'));
			isSaveError = true;
			return;
		}
		
		try{
			
			upsert objective;
			
		}catch(Exception e){
			ApexPages.addMessages(e);
			isSaveError = true;
			return;
		}	
		
		isSaveError = false;	
	}	
}