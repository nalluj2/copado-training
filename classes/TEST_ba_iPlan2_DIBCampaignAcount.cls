@isTest
private class TEST_ba_iPlan2_DIBCampaignAcount {
	
	@testSetup
	static void generateMasterData(){
		
		// Master Data
		Company__c europe = new Company__c();
		europe.Name = 'Europe';
		europe.Company_Code_Text__c = 'TST';
		insert europe;
		
		// Business Unit Group			
		Business_Unit_Group__c diabetesBUG = new Business_Unit_Group__c();
		diabetesBUG.Master_Data__c = europe.id;
        diabetesBUG.name='Diabetes';       
        insert diabetesBUG;
        
        // Business Unit        
        Business_Unit__c diabetesBU =  new Business_Unit__c();
        diabetesBU.Company__c = Europe.id;
        diabetesBU.name = 'Diabetes';
        diabetesBU.Business_Unit_Group__c = diabetesBUG.Id;        
        insert diabetesBU;
        
        // Sub-Business Unit       
		Sub_Business_Units__c diabetesCoreSBU = new Sub_Business_Units__c();
        diabetesCoreSBU.name = 'Diabetes Core';
        diabetesCoreSBU.Business_Unit__c = diabetesBU.id;		
		insert diabetesCoreSBU;
		
		// Departments
		Department_Master__c pediatric = new Department_Master__c();
		pediatric.Name = 'Pediatric';
				
		Department_Master__c adult1 = new Department_Master__c();
		adult1.Name = 'Adult Type 1';
				
		Department_Master__c adult2 = new Department_Master__c();
		adult2.Name = 'Adult Type 2';
		
		insert new List<Department_Master__c>{pediatric, adult1, adult2};
	}
	
	@isTest static void testAssignAccountPlan() {

		Account acc1 = new Account();
		acc1.Name = 'Unit Test Account 1';
		acc1.SAP_Id__c = '123456789';
		acc1.Account_Country_vs__c = 'Belgium';
		
		Account acc2 = new Account();
		acc2.Name = 'Unit Test Account 2';
		acc2.SAP_Id__c = '987654321';
		acc2.Account_Country_vs__c = 'Belgium';
		
		Account acc3 = new Account();
		acc3.Name = 'Unit Test Account 3';
		acc3.SAP_Id__c = '192837645';
		acc3.Account_Country_vs__c = 'Belgium';
		
		insert new List<Account>{acc1, acc2, acc3};
		
		Business_Unit_Group__c bug = [Select Id from Business_Unit_Group__c where Name = 'Diabetes'];
		Business_Unit__c bu = [Select Id from Business_Unit__c where Name = 'Diabetes'];
		Sub_Business_Units__c sbu = [Select Id from Sub_Business_Units__c where Name = 'Diabetes Core'];
		RecordType eurDibSbu = [Select Id from RecordType where sObjectType = 'Account_Plan_2__c' AND DeveloperName = 'EUR_DiB_sBU'];
		
		Account_Plan_2__c accPlan1 = new Account_Plan_2__c();
		accPlan1.Account__c = acc1.Id;
		accPlan1.Business_Unit_Group__c = bug.Id;
		accPlan1.Business_Unit__c = bu.Id;
		accPlan1.Account_Plan_Level__c = 'Sub Business Unit';
		accPlan1.Sub_Business_Unit__c = sbu.Id;		
		accPlan1.RecordTypeId = eurDibSbu.Id;
		
		Account_Plan_2__c accPlan2 = new Account_Plan_2__c();
		accPlan2.Account__c = acc2.Id;
		accPlan2.Business_Unit_Group__c = bug.Id;
		accPlan2.Business_Unit__c = bu.Id;
		accPlan2.Account_Plan_Level__c = 'Sub Business Unit';
		accPlan2.Sub_Business_Unit__c = sbu.Id;		
		accPlan2.RecordTypeId = eurDibSbu.Id;
				
		Account_Plan_2__c accPlan3 = new Account_Plan_2__c();
		accPlan3.Account__c = acc3.Id;
		accPlan3.Business_Unit_Group__c = bug.Id;
		accPlan3.Business_Unit__c = bu.Id;
		accPlan3.Account_Plan_Level__c = 'Sub Business Unit';
		accPlan3.Sub_Business_Unit__c = sbu.Id;		
		accPlan3.RecordTypeId = eurDibSbu.Id;
				
		insert new List<Account_Plan_2__c>{accPlan1, accPlan2, accPlan3};
		
		DIB_Country__c country = new DIB_Country__c();
		country.Name = 'Belgium';
		country.Country_ISO_Code__c = 'BE';
		country.Company__c = [Select Id from Company__c].Id;
		insert country;
		
		DIB_Department__c acc1Pediatric = new DIB_Department__c();
        acc1Pediatric.Account__c = acc1.Id;
		acc1Pediatric.Active__c = true;
		acc1Pediatric.Department_ID__c = [Select Id from Department_Master__c where Name = 'Pediatric'].Id;
		
		DIB_Department__c acc1Adult1 = new DIB_Department__c();
        acc1Adult1.Account__c = acc1.Id;
		acc1Adult1.Active__c = true;
		acc1Adult1.Department_ID__c = [Select Id from Department_Master__c where Name = 'Adult Type 1'].Id;
		
		DIB_Department__c acc2Pediatric = new DIB_Department__c();
        acc2Pediatric.Account__c = acc2.Id;
		acc2Pediatric.Active__c = true;
		acc2Pediatric.Department_ID__c = [Select Id from Department_Master__c where Name = 'Pediatric'].Id;
		
		DIB_Department__c acc2Adult1 = new DIB_Department__c();
        acc2Adult1.Account__c = acc2.Id;
		acc2Adult1.Active__c = true;
		acc2Adult1.Department_ID__c = [Select Id from Department_Master__c where Name = 'Adult Type 1'].Id;
		
		DIB_Department__c acc3Adult1 = new DIB_Department__c();
        acc3Adult1.Account__c = acc3.Id;
		acc3Adult1.Active__c = true;
		acc3Adult1.Department_ID__c = [Select Id from Department_Master__c where Name = 'Adult Type 1'].Id;
		
		insert new List<DIB_Department__c>{acc1Pediatric, acc1Adult1, acc2Pediatric, acc2Adult1, acc3Adult1};
		
		DIB_Campaign_Program__c campaignProgram = new DIB_Campaign_Program__c();
        campaignProgram.Name = 'TEST CAMPAIGN PROGRAM';
		insert campaignProgram;
		
		DIB_Global_Campaign__c globalCampaign = new DIB_Global_Campaign__c();
        globalCampaign.Name = 'TEST GLOBAL CAMPAIGN';
        globalCampaign.DIB_Campaign_Program__c = campaignProgram.Id;
        insert globalCampaign;
		
		DIB_Campaign__c dibCampaign = new DIB_Campaign__c();
	    dibCampaign.Name = 'TEST LOCAL CAMPAIGN';
	    dibCampaign.Country__c = country.Id;
	    dibCampaign.Global_Campaign__c = globalCampaign.Id;
	    dibCampaign.Start_Fiscal_Year__c = '2018';
	    dibCampaign.End_Fiscal_Year__c = '2018';	    
		insert dibCampaign;
		
		DIB_Campaign_Account__c campaignAcc1Pediatric = new DIB_Campaign_Account__c();
        campaignAcc1Pediatric.Account__c = acc1.Id;
        campaignAcc1Pediatric.Campaign__c = dibCampaign.Id;
        campaignAcc1Pediatric.Department_ID__c = acc1Pediatric.Id;
        
        DIB_Campaign_Account__c campaignAcc1Adult1 = new DIB_Campaign_Account__c();
        campaignAcc1Adult1.Account__c = acc1.Id;
        campaignAcc1Adult1.Campaign__c = dibCampaign.Id;
        campaignAcc1Adult1.Department_ID__c = acc1Adult1.Id;
        
        DIB_Campaign_Account__c campaignAcc2Pediatric = new DIB_Campaign_Account__c();
        campaignAcc2Pediatric.Account__c = acc2.Id;
        campaignAcc2Pediatric.Campaign__c = dibCampaign.Id;
        campaignAcc2Pediatric.Department_ID__c = acc2Pediatric.Id;
        
        DIB_Campaign_Account__c campaignAcc2Adult1 = new DIB_Campaign_Account__c();
        campaignAcc2Adult1.Account__c = acc2.Id;
        campaignAcc2Adult1.Campaign__c = dibCampaign.Id;
        campaignAcc2Adult1.Department_ID__c = acc2Adult1.Id;
        
        DIB_Campaign_Account__c campaignAcc3Adult1 = new DIB_Campaign_Account__c();
        campaignAcc3Adult1.Account__c = acc3.Id;
        campaignAcc3Adult1.Campaign__c = dibCampaign.Id;
        campaignAcc3Adult1.Department_ID__c = acc3Adult1.Id;
                
        insert new List<DIB_Campaign_Account__c>{campaignAcc1Pediatric, campaignAcc1Adult1, campaignAcc2Pediatric, campaignAcc2Adult1, campaignAcc3Adult1};
        		
		Test.startTest();

		// Execute the batch
        ba_iPlan2_DIBCampaignAcount oBAiPlan2DIBCampaignAcount = new ba_iPlan2_DIBCampaignAcount();
        Database.executeBatch(oBAiPlan2DIBCampaignAcount,100);  

		Test.stopTest();
				
		campaignAcc1Pediatric = [SELECT ID, Account_Plan_2__c FROM DIB_Campaign_Account__c where Id = :campaignAcc1Pediatric.Id];		
		System.assert(campaignAcc1Pediatric.Account_Plan_2__c == accPlan1.Id);
		
		campaignAcc1Adult1 = [SELECT ID, Account_Plan_2__c FROM DIB_Campaign_Account__c where Id = :campaignAcc1Adult1.Id];		
		System.assert(campaignAcc1Adult1.Account_Plan_2__c == accPlan1.Id);
		
		campaignAcc2Pediatric = [SELECT ID, Account_Plan_2__c FROM DIB_Campaign_Account__c where Id = :campaignAcc2Pediatric.Id];		
		System.assert(campaignAcc2Pediatric.Account_Plan_2__c == accPlan2.Id);
		
		// No account plan for this department
		campaignAcc2Adult1 = [SELECT ID, Account_Plan_2__c FROM DIB_Campaign_Account__c where Id = :campaignAcc2Adult1.Id];		
		System.assert(campaignAcc2Adult1.Account_Plan_2__c == accPlan2.Id);
		
		campaignAcc3Adult1 = [SELECT ID, Account_Plan_2__c FROM DIB_Campaign_Account__c where Id = :campaignAcc3Adult1.Id];		
		System.assert(campaignAcc3Adult1.Account_Plan_2__c == accPlan3.Id);
	}
	
	@isTest static void testAssignAccountPlan_CAN() {

		Account acc1 = new Account();
		acc1.Name = 'Unit Test Account 1';
		acc1.SAP_Id__c = '123456789';
		acc1.Account_Country_vs__c = 'Canada';
		
		Account acc2 = new Account();
		acc2.Name = 'Unit Test Account 2';
		acc2.SAP_Id__c = '987654321';
		acc2.Account_Country_vs__c = 'Canada';
		
		Account acc3 = new Account();
		acc3.Name = 'Unit Test Account 3';
		acc3.SAP_Id__c = '192837645';
		acc3.Account_Country_vs__c = 'Canada';
		
		insert new List<Account>{acc1, acc2, acc3};
		
		Business_Unit_Group__c bug = [Select Id from Business_Unit_Group__c where Name = 'Diabetes'];
		Business_Unit__c bu = [Select Id from Business_Unit__c where Name = 'Diabetes'];
		Sub_Business_Units__c sbu = [Select Id from Sub_Business_Units__c where Name = 'Diabetes Core'];
		RecordType eurDibSbu = [Select Id from RecordType where sObjectType = 'Account_Plan_2__c' AND DeveloperName = 'EUR_DiB_sBU'];
		
		Account_Plan_2__c accPlan1 = new Account_Plan_2__c();
		accPlan1.Account__c = acc1.Id;
		accPlan1.Business_Unit_Group__c = bug.Id;
		accPlan1.Business_Unit__c = bu.Id;
		accPlan1.Account_Plan_Level__c = 'Sub Business Unit';
		accPlan1.Sub_Business_Unit__c = sbu.Id;		
		accPlan1.RecordTypeId = eurDibSbu.Id;
				
		Account_Plan_2__c accPlan2 = new Account_Plan_2__c();
		accPlan2.Account__c = acc2.Id;
		accPlan2.Business_Unit_Group__c = bug.Id;
		accPlan2.Business_Unit__c = bu.Id;
		accPlan2.Account_Plan_Level__c = 'Sub Business Unit';
		accPlan2.Sub_Business_Unit__c = sbu.Id;		
		accPlan2.RecordTypeId = eurDibSbu.Id;
		
		Account_Plan_2__c accPlan3 = new Account_Plan_2__c();
		accPlan3.Account__c = acc3.Id;
		accPlan3.Business_Unit_Group__c = bug.Id;
		accPlan3.Business_Unit__c = bu.Id;
		accPlan3.Account_Plan_Level__c = 'Sub Business Unit';
		accPlan3.Sub_Business_Unit__c = sbu.Id;		
		accPlan3.RecordTypeId = eurDibSbu.Id;
				
		insert new List<Account_Plan_2__c>{accPlan1, accPlan2, accPlan3};
		
		DIB_Country__c country = new DIB_Country__c();
		country.Name = 'Canada';
		country.Country_ISO_Code__c = 'CA';
		country.Company__c = [Select Id from Company__c].Id;
		insert country;
		
		DIB_Department__c acc1Pediatric = new DIB_Department__c();
        acc1Pediatric.Account__c = acc1.Id;
		acc1Pediatric.Active__c = true;
		acc1Pediatric.Department_ID__c = [Select Id from Department_Master__c where Name = 'Pediatric'].Id;
		
		DIB_Department__c acc1Adult1 = new DIB_Department__c();
        acc1Adult1.Account__c = acc1.Id;
		acc1Adult1.Active__c = true;
		acc1Adult1.Department_ID__c = [Select Id from Department_Master__c where Name = 'Adult Type 1'].Id;
		
		DIB_Department__c acc2Pediatric = new DIB_Department__c();
        acc2Pediatric.Account__c = acc2.Id;
		acc2Pediatric.Active__c = true;
		acc2Pediatric.Department_ID__c = [Select Id from Department_Master__c where Name = 'Pediatric'].Id;
		
		DIB_Department__c acc2Adult1 = new DIB_Department__c();
        acc2Adult1.Account__c = acc2.Id;
		acc2Adult1.Active__c = true;
		acc2Adult1.Department_ID__c = [Select Id from Department_Master__c where Name = 'Adult Type 1'].Id;
		
		DIB_Department__c acc3Adult1 = new DIB_Department__c();
        acc3Adult1.Account__c = acc3.Id;
		acc3Adult1.Active__c = true;
		acc3Adult1.Department_ID__c = [Select Id from Department_Master__c where Name = 'Adult Type 1'].Id;
		
		insert new List<DIB_Department__c>{acc1Pediatric, acc1Adult1, acc2Pediatric, acc2Adult1, acc3Adult1};
		
		DIB_Campaign_Program__c campaignProgram = new DIB_Campaign_Program__c();
        campaignProgram.Name = 'TEST CAMPAIGN PROGRAM';
		insert campaignProgram;
		
		DIB_Global_Campaign__c globalCampaign = new DIB_Global_Campaign__c();
        globalCampaign.Name = 'TEST GLOBAL CAMPAIGN';
        globalCampaign.DIB_Campaign_Program__c = campaignProgram.Id;
        insert globalCampaign;
		
		DIB_Campaign__c dibCampaign = new DIB_Campaign__c();
	    dibCampaign.Name = 'TEST LOCAL CAMPAIGN';
	    dibCampaign.Country__c = country.Id;
	    dibCampaign.Global_Campaign__c = globalCampaign.Id;
	    dibCampaign.Start_Fiscal_Year__c = '2018';
	    dibCampaign.End_Fiscal_Year__c = '2018';	    
		insert dibCampaign;
		
		DIB_Campaign_Account__c campaignAcc1Pediatric = new DIB_Campaign_Account__c();
        campaignAcc1Pediatric.Account__c = acc1.Id;
        campaignAcc1Pediatric.Campaign__c = dibCampaign.Id;
        campaignAcc1Pediatric.Department_ID__c = acc1Pediatric.Id;
        
        DIB_Campaign_Account__c campaignAcc1Adult1 = new DIB_Campaign_Account__c();
        campaignAcc1Adult1.Account__c = acc1.Id;
        campaignAcc1Adult1.Campaign__c = dibCampaign.Id;
        campaignAcc1Adult1.Department_ID__c = acc1Adult1.Id;
        
        DIB_Campaign_Account__c campaignAcc2Pediatric = new DIB_Campaign_Account__c();
        campaignAcc2Pediatric.Account__c = acc2.Id;
        campaignAcc2Pediatric.Campaign__c = dibCampaign.Id;
        campaignAcc2Pediatric.Department_ID__c = acc2Pediatric.Id;
        
        DIB_Campaign_Account__c campaignAcc2Adult1 = new DIB_Campaign_Account__c();
        campaignAcc2Adult1.Account__c = acc2.Id;
        campaignAcc2Adult1.Campaign__c = dibCampaign.Id;
        campaignAcc2Adult1.Department_ID__c = acc2Adult1.Id;
        
        DIB_Campaign_Account__c campaignAcc3Adult1 = new DIB_Campaign_Account__c();
        campaignAcc3Adult1.Account__c = acc3.Id;
        campaignAcc3Adult1.Campaign__c = dibCampaign.Id;
        campaignAcc3Adult1.Department_ID__c = acc3Adult1.Id;
                
        insert new List<DIB_Campaign_Account__c>{campaignAcc1Pediatric, campaignAcc1Adult1, campaignAcc2Pediatric, campaignAcc2Adult1, campaignAcc3Adult1};
        		
		Test.startTest();

		// Execute the batch
        ba_iPlan2_DIBCampaignAcount oBAiPlan2DIBCampaignAcount = new ba_iPlan2_DIBCampaignAcount();
        Database.executeBatch(oBAiPlan2DIBCampaignAcount,100);  

		Test.stopTest();
				
		campaignAcc1Pediatric = [SELECT ID, Account_Plan_2__c FROM DIB_Campaign_Account__c where Id = :campaignAcc1Pediatric.Id];		
		System.assert(campaignAcc1Pediatric.Account_Plan_2__c == accPlan1.Id);
		
		campaignAcc1Adult1 = [SELECT ID, Account_Plan_2__c FROM DIB_Campaign_Account__c where Id = :campaignAcc1Adult1.Id];		
		System.assert(campaignAcc1Adult1.Account_Plan_2__c == accPlan1.Id);
		
		campaignAcc2Pediatric = [SELECT ID, Account_Plan_2__c FROM DIB_Campaign_Account__c where Id = :campaignAcc2Pediatric.Id];		
		System.assert(campaignAcc2Pediatric.Account_Plan_2__c == accPlan2.Id);
		
		campaignAcc2Adult1 = [SELECT ID, Account_Plan_2__c FROM DIB_Campaign_Account__c where Id = :campaignAcc2Adult1.Id];		
		System.assert(campaignAcc2Adult1.Account_Plan_2__c == accPlan2.Id);
		
		campaignAcc3Adult1 = [SELECT ID, Account_Plan_2__c FROM DIB_Campaign_Account__c where Id = :campaignAcc3Adult1.Id];		
		System.assert(campaignAcc3Adult1.Account_Plan_2__c == accPlan3.Id);
	}
	
	@isTest static void test_method_Scheduling() {

		Test.startTest();

		//---------------------------------------
		// TEST 1
		// SCHEDULING
		//---------------------------------------
		string tCRON_EXP = '0 0 0 3 9 ? 2099';
		
		string tJobId = System.schedule('ba_iPlan2_DIBCampaignAcount_TEST', tCRON_EXP, new ba_iPlan2_DIBCampaignAcount());

		// Get the information from the CronTrigger API object
		CronTrigger oCronTrigger = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :tJobId];

		// Verify the expressions are the same
		System.assertEquals(tCRON_EXP, oCronTrigger.CronExpression);

		// Verify the job has not run
		System.assertEquals(0, oCronTrigger.TimesTriggered);

		// Verify the next time the job will run
		System.assertEquals('2099-09-03 00:00:00', String.valueOf(oCronTrigger.NextFireTime));
		//---------------------------------------

		Test.stopTest();
	}
}