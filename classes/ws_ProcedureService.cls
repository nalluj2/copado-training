/*
 *      Description : This class exposes methods to create / update / delete a Account Plan 2
 *
 *      Author = Rudy De Coninck
 */
@RestResource(urlMapping='/ProcedureService/*')
global with sharing class ws_ProcedureService {
    
    @HttpPost
	global static String doPost() {
 		
 		RestRequest req = RestContext.request;
			
		System.debug('post called '+req);
		System.debug('post requestBody '+req.requestBody);
			
		String body = req.requestBody.toString();
		IFProcedureRequest procedureRequest = (IFProcedureRequest) System.Json.deserialize(body, IFProcedureRequest.class);
			
		System.debug('post requestBody ' + procedureRequest);
			
		Account_Procedure__c procedure = procedureRequest.procedure;

		IFProcedureResponse resp = new IFProcedureResponse();

		//We catch all exception to assure that 
		try{
				
			resp.procedure = bl_Procedure.saveProcedure(procedure);
			resp.id = procedureRequest.id;
	
			resp.success = true;
		}catch (Exception e){
			
			resp.message = e.getMessage();
			resp.success = false;
		}
 
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'Procedure' ,req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
  			
		return System.JSON.serialize(resp);		
	}
	
	global class IFProcedureRequest{
		public Account_Procedure__c procedure {get;set;}
		public String id{get;set;}
	}
	
	global class IFProcedureResponse{
		public Account_Procedure__c procedure {get;set;}
		public String id{get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}
}