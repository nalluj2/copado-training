/*
 *      Description : This class centralizes business logic related to Account Planning
 *
 *      Author = Rudy De Coninck
 */
 
public class bl_AccountPlanning {
	
	public static Boolean escapeDupValidation = false;
	
	public static Account_Plan_2__c saveAccountPlan2(Account_Plan_2__c accountPlan){
		
		if (null!=accountPlan){
			
			/* CR-7889 - Extend the length of these fields to 5000 chars in both, mobile app and SFDC
			//Truncate values in case size larger than 200
			if (accountPlan.Other_Opportunities__c != null && accountPlan.Other_Opportunities__c.length()>200){
				accountPlan.Other_Opportunities__c = accountPlan.Other_Opportunities__c.subString(0,200);
			}
			if (accountPlan.Other_Strengths__c != null && accountPlan.Other_Strengths__c.length()>200){
				accountPlan.Other_Strengths__c = accountPlan.Other_Strengths__c.subString(0,200);
			}
			if (accountPlan.Other_Threats__c != null && accountPlan.Other_Threats__c.length()>200){
				accountPlan.Other_Threats__c = accountPlan.Other_Threats__c.subString(0,200);
			}
			if (accountPlan.Other_Weaknesses__c != null && accountPlan.Other_Weaknesses__c.length()>200){
				accountPlan.Other_Weaknesses__c = accountPlan.Other_Weaknesses__c.subString(0,200);
			}
			*/
			
			upsert accountPlan Mobile_ID__c;
		}
		return accountPlan;
		
	} 

	public static Account_Plan_Objective__c saveAccountPlanObjective(Account_Plan_Objective__c accountPlanObjective){
		
		bl_UserAction.childAccountPlanOperation = true;
		if (null!=accountPlanObjective){
			upsert accountPlanObjective Mobile_ID__c;
		}
		return accountPlanObjective;
		
	} 

	public static Account_Plan_Stakeholder__c saveAccountPlanStakeholder(Account_Plan_Stakeholder__c accountPlanStakeholder){
		
		bl_UserAction.childAccountPlanOperation = true;
		if (null!=accountPlanStakeholder){
			upsert accountPlanStakeholder Mobile_ID__c;
		}
		return accountPlanStakeholder;
		
	} 
	
	public static Account_Plan_SWOT_Analysis__c saveAccountPlanSWOT(Account_Plan_SWOT_Analysis__c accountPlanSWOT){
		
		bl_UserAction.childAccountPlanOperation = true;
		if (null != accountPlanSWOT){
			upsert accountPlanSWOT Mobile_ID__c;
		}
		return accountPlanSWOT;
		
	} 
	
	public static Diabetes_Performance__c saveDiabetesPerformance(Diabetes_Performance__c diabetesPerformance){
		
		bl_UserAction.childAccountPlanOperation = true;
		
		if (null != diabetesPerformance){
		
			List<Diabetes_Performance__c> dbPerformance = [Select Id from Diabetes_Performance__c where Mobile_Id__c = :diabetesPerformance.Mobile_ID__c];
		
			if(dbPerformance.size() == 0) throw new ws_Exception('No Diabetes Performance record found for Id \'' + diabetesPerformance.Mobile_ID__c + '\'' );		
		
			upsert diabetesPerformance Mobile_ID__c;
		}
		
		return diabetesPerformance;		
	} 

	public static Account_Plan_Team_Member__c saveAccountPlanTeamMember(Account_Plan_Team_Member__c accountPlanTeamMember){
		
		bl_UserAction.childAccountPlanOperation = true;
		if (null!=accountPlanTeamMember){
			upsert accountPlanTeamMember Mobile_ID__c;
		}
		return accountPlanTeamMember;
		
	} 

	public static Tender_Calendar_2__c saveTenderCalendar2(Tender_Calendar_2__c tenderCalendar){
		
		bl_UserAction.childAccountPlanOperation = true;
		if (null!=tenderCalendar){
			upsert tenderCalendar Mobile_ID__c;
		}
		return tenderCalendar;
		
	} 
	
	public static Opportunity saveCVGProjectOpportunity(Opportunity opp){
		
		bl_UserAction.childAccountPlanOperation = true;
		if (null!=opp){
			upsert opp Mobile_ID__c;
		}
		return opp;
	}
	
	public static List<OpportunityLineItem> saveCVGProjectOpportunity(List<OpportunityLineItem> oppLineItems){
		
		bl_UserAction.childAccountPlanOperation = true;
		if (null!=oppLineItems && oppLineItems.size()>0){
			
			for(OpportunityLineItem oppLineItem : oppLineItems){
				
				List<OpportunityLineItem> existingLines = [select Id from OpportunityLineItem where mobile_id__c = : oppLineItem.mobile_id__c];
				
				if(oppLineItem.UnitPrice==null){
					oppLineItem.UnitPrice=0;
				}
				
				if(existingLines!=null && existingLines.size()>0){
					//update do nothing
					System.debug('Update '+oppLineItem);
				}else{
					System.debug('Create '+oppLineItem);
					//create fill totalprice based on input unit price
					oppLineItem.TotalPrice = oppLineItem.Quantity * oppLineItem.UnitPrice;
					oppLineItem.UnitPrice = null;
				}
			}
			
			upsert oppLineItems Mobile_ID__c;
		}
		return oppLineItems;
	}
	
	public static Opportunity saveOpportunity(Opportunity opp){
		
		bl_UserAction.childAccountPlanOperation = true;
		if (null!=opp){
			upsert opp Mobile_ID__c;
		}
		return opp;
	}
	
	public static List<OpportunityLineItem> saveOpportunityLineItems(List<OpportunityLineItem> oppLineItems){
		
		bl_UserAction.childAccountPlanOperation = true;
		if (null!=oppLineItems && oppLineItems.size()>0){
			
			for(OpportunityLineItem oppLineItem : oppLineItems){
				
				List<OpportunityLineItem> existingLines = [select Id from OpportunityLineItem where mobile_id__c = : oppLineItem.mobile_id__c];
				
				if(oppLineItem.UnitPrice==null){
					oppLineItem.UnitPrice=0;
				}
				
				if(existingLines!=null && existingLines.size()>0){
					//update do nothing
					System.debug('Update '+oppLineItem);
				}else{
					System.debug('Create '+oppLineItem);
					//create fill totalprice based on input unit price
					oppLineItem.TotalPrice = oppLineItem.Quantity * oppLineItem.UnitPrice;
					oppLineItem.UnitPrice = null;
				}
			}
			
			upsert oppLineItems Mobile_ID__c;
		}
		return oppLineItems;
	}
	
	public static Opportunity_Growth_Driver__c saveOpportunityGrowthDriver(Opportunity_Growth_Driver__c oppGrowthDriver){
		
		bl_UserAction.childAccountPlanOperation = true;
		
		if (null != oppGrowthDriver){
			upsert oppGrowthDriver Mobile_ID__c;
		}
		
		return oppGrowthDriver;
	}

	//------------------------------------------------------------------------------------------
	//	Description		: This method will update the DIB_Campaign_Account__c records and fill the Account_Plan_2__c with the Active Account_Plan_2__c of the Account
	//						This logic is used for DIB.
	//	Parameters		: 
	//						List<Account_Plan_2__c> 	--> list of active Account_Plan__c for DIB related Accounts
	//	Data 			: 20140326
	// 	Author			: Bart Caelen
	// 	Version 		: 1.0
	//------------------------------------------------------------------------------------------
	public static void setActiveAccountPlan2OnDIBCampaignAccount(List<Account_Plan_2__c> lstAccountPlan2){
   		   		   		
   		Map<Id, Account_Plan_2__c> mapAccountID_AccountPlan2 = new Map<Id, Account_Plan_2__c>();

   		for(Account_Plan_2__c oAccountPlan2 : lstAccountPlan2){
  			  			  			
  			mapAccountID_AccountPlan2.put(oAccountPlan2.Account__c, oAccountPlan2);
   		}

   		List<DIB_Campaign_Account__c> lstDIBCampaignAccount = 
   			[
   				SELECT 
   					Id, Account__c, Account__r.Account_Country_vs__c, Account_Plan_2__c, CreatedDate 
   					, Campaign__r.Start_Fiscal_Year__c, Campaign__r.Start_Fiscal_Month__c
   					, Campaign__r.End_Fiscal_Year__c, Campaign__r.End_Fiscal_Month__c
				FROM
					DIB_Campaign_Account__c
				WHERE
					Account__c =: mapAccountID_AccountPlan2.keySet()
			];
/* Getting rid of Dates in Diabetes Account Plans

		Map<string, DIB_Fiscal_Period__c> mapDIBFiscalPeriod = new Map<String, DIB_Fiscal_Period__c>();
		for (DIB_Fiscal_Period__c oDIBFiscalPeriod : [SELECT Id, Start_Date__c, End_Date__c, Fiscal_Month__c, Fiscal_Year__c FROM DIB_Fiscal_Period__c ORDER BY Fiscal_Year__c, Fiscal_Month__c]){
			String tKey = oDIBFiscalPeriod.Fiscal_Year__c + oDIBFiscalPeriod.Fiscal_Month__c;
			mapDIBFiscalPeriod.put(tKey, oDIBFiscalPeriod);
		}
*/
   		List<DIB_Campaign_Account__c> lstDIBCampaignAccount_Update = new List<DIB_Campaign_Account__c>();
   		for (DIB_Campaign_Account__c oDIBCampaignAccount : lstDIBCampaignAccount){

   			//String tCampaign_Fisc_StartDate = clsUtil.isNull(oDIBCampaignAccount.Campaign__r.Start_Fiscal_Year__c, '') + clsUtil.isNull(oDIBCampaignAccount.Campaign__r.Start_Fiscal_Month__c, '');
   			//String tCampaign_Fisc_EndDate = clsUtil.isNull(oDIBCampaignAccount.Campaign__r.End_Fiscal_Year__c, '') + clsUtil.isNull(oDIBCampaignAccount.Campaign__r.End_Fiscal_Month__c, '');

			/*
   			Date dCampaign_Cal_StartDate;
   			if (mapDIBFiscalPeriod.containsKey(tCampaign_Fisc_StartDate)){
   				dCampaign_Cal_StartDate =  mapDIBFiscalPeriod.get(tCampaign_Fisc_StartDate).Start_Date__c;
			}
   			Date dCampaign_Cal_EndDate;
   			if (mapDIBFiscalPeriod.containsKey(tCampaign_Fisc_EndDate)){
	   			dCampaign_Cal_EndDate = mapDIBFiscalPeriod.get(tCampaign_Fisc_EndDate).End_Date__c;
	   		}
			*/
   			
   			// Get the Active iPlan for the Account
   			Account_Plan_2__c oAP = mapAccountID_AccountPlan2.get(oDIBCampaignAccount.Account__c);
							
			
   			/*
   			Boolean bAssignIplan2 = false;
   			if ( ( dCampaign_Cal_StartDate >= oAP.Start_Date__c) && ( (dCampaign_Cal_StartDate <= oAP.End_Date__c) || (oAP.End_Date__c == null) ) ){
   				bAssignIplan2 = true;
   			}else if( (dCampaign_Cal_StartDate < oAP.Start_Date__c) && ( (dCampaign_Cal_EndDate >= oAP.Start_Date__c) || (dCampaign_Cal_EndDate == null) ) ){
	   			bAssignIplan2 = true;
   			}
			*/
				
   			if (oAP != null){
	   				
	   			if (oDIBCampaignAccount.Account_Plan_2__c != oAP.Id){
	   					
	   				oDIBCampaignAccount.Account_Plan_2__c = oAP.Id;
	   				lstDIBCampaignAccount_Update.add(oDIBCampaignAccount);
	   			}
	   				
   			}else{
   					
   				if (oDIBCampaignAccount.Account_Plan_2__c != null){
	   					
	   				oDIBCampaignAccount.Account_Plan_2__c = null;
	   				lstDIBCampaignAccount_Update.add(oDIBCampaignAccount);
	   			}
   			}
			
// 			if ( (oAP.Start_Date__c >= dCampaign_Cal_StartDate) || (oAP.End_Date__c <= dCampaign_Cal_EndDate) ){
//	   			if (oDIBCampaignAccount.Account_Plan_2__c != oAP.Id){
//	   				oDIBCampaignAccount.Account_Plan_2__c = oAP.Id;
//	   				lstDIBCampaignAccount_Update.add(oDIBCampaignAccount);
//	   			}
 //			}

   		}

   		if (lstDIBCampaignAccount_Update.size() > 0){
   			update lstDIBCampaignAccount_Update;
   		}
	}
	//------------------------------------------------------------------------------------------

	public static void simpleUpdateParentAccountPlan2(Set<Id> accountPlan2Ids){
		
		List<Account_Plan_2__c> accountPlans = new List<Account_Plan_2__c>();
		
		for(Id iplan2Id : accountPlan2Ids){
			accountPlans.add(new Account_Plan_2__c(Id=iplan2Id));
		}
		
		Triggers_Soft_Deactivation.tr_AccountPlan2_AfterInsertUpdate = true;
		bl_AccountPlanning.escapeDupValidation = true;
		update accountPlans;
		Triggers_Soft_Deactivation.tr_AccountPlan2_AfterInsertUpdate = false;
		bl_AccountPlanning.escapeDupValidation = false;
	}
	
	
	
/*
 *		Christophe Saenen
 *	 	CR-11736	20160426 // Populate Country and Business Unit name
 */ 
	public static void updateFields(List<Account_Plan_2__c> lstAccountPlan2) {
				
		Set<Id> buSet = new Set<Id>(); 
								
		for(Account_Plan_2__c accAp : lstAccountPlan2) {
			
			accAp.Country__c = accAp.Country_form__c; // Copy Account Country to Country field;
			
			if(accAp.Business_Unit__c != null) buSet.add(accAp.Business_Unit__c);		
		}
						
		Map<Id, Business_Unit__c> buMap;
		
		if(buSet.size() > 0){
			
			buMap = new Map<Id, Business_Unit__c>([Select Id, Name from Business_Unit__c where Id IN :buSet]);			
		}
		
		for(Account_Plan_2__c accAp : lstAccountPlan2) {
			
			if(accAp.Business_Unit__c != null) accAp.Business_Unit_Text__c = buMap.get(accAp.Business_Unit__c).Name;		
		}		
	}	
	
	public static void setBUGAccountPlan(List<Account_Plan_2__c> lstAccountPlan2){
		
		Set<Id> accSet = new Set<Id>();
		Set<Id> bugSet = new Set<Id>();		
										
		for(Account_Plan_2__c accAp : lstAccountPlan2) {
						
			if(accAp.Account_Plan_Level__c == 'Sub Business Unit') {
				
				accSet.add(accAp.Account__c);
				bugSet.add(accAp.Business_Unit_Group__c);
			}
		}
		
		Map<String, Account_Plan_2__c> apBugMap;
		
		if(accSet.size() > 0){
		
			// get all business unit groups for account plans in trigger new
			apBugMap= new Map<String, Account_Plan_2__c>();
			
			for(Account_Plan_2__c ap : [Select Id,
					 						   Name,
					 						   Account__c,
					 						   Account_Plan__c,
					 						   Business_Unit_Group__c 
									      From Account_Plan_2__c
										 Where Account__c = :accSet
										   And Business_Unit_Group__c = :bugSet
										   And Account_Plan_Level__c = :'Business Unit Group'])  {
				
				string keyConcat = string.valueof(ap.Account__c) + string.valueof(ap.Business_Unit_Group__c);		 			   	
				apBugMap.put(keyConcat, ap);			 									  				     	
			}	
		}
						
		for(Account_Plan_2__c ap2 : lstAccountPlan2) {
						
			if(ap2.Account_Plan_Level__c == 'Sub Business Unit') {
				
				string keyConcat = string.valueof(ap2.Account__c) + string.valueof(ap2.Business_Unit_Group__c);
	
				if(apBugMap.containsKey(keyConcat)) {
					ap2.Account_Plan__c = apBugMap.get(keyConcat).Id;
				}
			}
		}				
	}

	public static void setBUGAccountPlan_BU(List<Account_Plan_2__c> lstAccountPlan2){
		
		Set<Id> accSet = new Set<Id>();
		Set<Id> bugSet = new Set<Id>();		
										
		for(Account_Plan_2__c accAp : lstAccountPlan2) {
						
			if(accAp.Account_Plan_Level__c == 'Business Unit') {
				
				accSet.add(accAp.Account__c);
				bugSet.add(accAp.Business_Unit_Group__c);
			}
		}
		
		Map<String, Account_Plan_2__c> apBugMap;
		
		if(accSet.size() > 0){
		
			// get all business unit groups for account plans in trigger new
			apBugMap= new Map<String, Account_Plan_2__c>();
			
			for(Account_Plan_2__c ap : [Select Id,
					 						   Name,
					 						   Account__c,
					 						   Account_Plan__c,
					 						   Business_Unit_Group__c 
									      From Account_Plan_2__c
										 Where Account__c = :accSet
										   And Business_Unit_Group__c = :bugSet
										   And Account_Plan_Level__c = :'Business Unit Group'])  {
				
				string keyConcat = string.valueof(ap.Account__c) + string.valueof(ap.Business_Unit_Group__c);		 			   	
				apBugMap.put(keyConcat, ap);			 									  				     	
			}	
		}
						
		for(Account_Plan_2__c ap2 : lstAccountPlan2) {
						
			if(ap2.Account_Plan_Level__c == 'Business Unit') {
				
				string keyConcat = string.valueof(ap2.Account__c) + string.valueof(ap2.Business_Unit_Group__c);
	
				if(apBugMap.containsKey(keyConcat)) {
					ap2.Account_Plan__c = apBugMap.get(keyConcat).Id;
				}
			}
		}				
	}
	
	public static void doAccPlanTeamMembersProcess(List<Account_Plan_2__c> triggerNew, Map<id, Account_Plan_2__c> triggerOldMap) {
		// Map for Insert
		Map<Id, Id> mapOfAccntPlanIdToMemberIdForInsert = new Map<Id, Id>();
		// Set of Account Plan Ids for CVG
		Set<Id> setOfCVGAccntPlanId = new Set<Id>();
		// Map for Delete
		Map<Id, Id> mapOfAccntPlanIdToMemberIdForDelete = new Map<Id, Id>();
		// Get Business Unit Group Id
//		Id cvgBUId;
		
		Map<Id, Business_Unit_Group__c> buGroupMap = new Map<Id, Business_Unit_Group__c>(
			[
				SELECT 
					Id, name 
				FROM 
					Business_Unit_Group__c 
				WHERE 
					Name = :ut_StaticValues.BUG_CVG 
					AND Master_Data__r.Name in :new Set<string>{ut_StaticValues.COMPANY_EUROPE}
			]
		);

//			if (!listOfBUGroup.isEmpty()){
//				cvgBUId = listOfBUGroup[0].Id;
//			}
			
		for (Account_Plan_2__c thisAccntPlan : triggerNew){
//				System.debug('*** cvgBUId: ' + cvgBUId);
			System.debug('*** thisAccntPlan: ' + thisAccntPlan);
			// If insert, owner is new, so insert required
			if (Trigger.isInsert){
				mapOfAccntPlanIdToMemberIdForInsert.put(thisAccntPlan.Id, thisAccntPlan.OwnerId);
			}
			// On update validate if Owner has changed and add to both insert and delete map
			if (Trigger.isUpdate){
				// Validate if Owner has changed
				if (thisAccntPlan.OwnerId != triggeroldMap.get(thisAccntPlan.Id).OwnerId){
					mapOfAccntPlanIdToMemberIdForInsert.put(thisAccntPlan.Id, thisAccntPlan.OwnerId);
					mapOfAccntPlanIdToMemberIdForDelete.put(thisAccntPlan.Id, triggeroldMap.get(thisAccntPlan.Id).OwnerId);
				}
			}
			if (buGroupMap.containsKey(thisAccntPlan.Business_Unit_Group__c)){
				setOfCVGAccntPlanId.add(thisAccntPlan.Id);
			}
		}
		
		if(mapOfAccntPlanIdToMemberIdForInsert.size() >0){
		
			// Add new Team Members, but first validate if they already exists as Team Member
			List<Account_Plan_Team_Member__c> listOfAccntPlanTM = [select Id, Account_Plan__c, Team_Member__c, Account_Plan_Owner__c from Account_Plan_Team_Member__c where Account_Plan__c in :mapOfAccntPlanIdToMemberIdForInsert.keySet() and Team_Member__c in :mapOfAccntPlanIdToMemberIdForInsert.values()];
			
			List<Account_Plan_Team_Member__c> listOfAccntPlanTMForUpdate = new List<Account_Plan_Team_Member__c>(); 
			// Remove items for insert if they already exists, and update Owner flag for team member
			for (Account_Plan_Team_Member__c accntPlanTM : listOfAccntPlanTM){
				if (mapOfAccntPlanIdToMemberIdForInsert.get(accntPlanTM.Account_Plan__c) == accntPlanTM.Team_Member__c){
					accntPlanTM.Account_Plan_Owner__c = true;
					listOfAccntPlanTMForUpdate.add(accntPlanTM);
					// Remove from map so it does not get inserted!
					mapOfAccntPlanIdToMemberIdForInsert.remove(accntPlanTM.Account_Plan__c);
				}
			}
			// Update Team Members list (flag is updated)
			if (!listOfAccntPlanTMForUpdate.isEmpty()){
				update listOfAccntPlanTMForUpdate;
			}
				
			// Insert Team members and set owner flag to true
			List<Account_Plan_Team_Member__c> listOfAccntPlanTMForInsert = new List<Account_Plan_Team_Member__c>();
			for (Id accntPlanId : mapOfAccntPlanIdToMemberIdForInsert.keySet()){
				Account_Plan_Team_Member__c accntPlanTM = new Account_Plan_Team_Member__c();
				accntPlanTM.Account_Plan__c = accntPlanId;
				accntPlanTM.Team_Member__c = mapOfAccntPlanIdToMemberIdForInsert.get(accntPlanId);
				accntPlanTM.Account_Plan_Owner__c = true;
				if (setOfCVGAccntPlanId.contains(accntPlanId)){
					accntPlanTM.Role__c = ut_StaticValues.ACCOUNT_TEAM_MEMBER_ROLE_TEAM_LEADER;
				}
				listOfAccntPlanTMForInsert.add(accntPlanTM);
			}
			if (!listOfAccntPlanTMForInsert.isEmpty()){
				System.debug('*** listOfAccntPlanTMForInsert: ' + listOfAccntPlanTMForInsert);
				insert listOfAccntPlanTMForInsert;
			}
		}
		
		if(mapOfAccntPlanIdToMemberIdForDelete.size() >0){
		
			// Remove old owners
			List<Account_Plan_Team_Member__c> listOfAccntPlanTMForDelete = new List<Account_Plan_Team_Member__c>();
			
			for(Account_Plan_Team_Member__c member : [select Id, Account_Plan__c, Team_Member__c from Account_Plan_Team_Member__c where Account_Plan__c in :mapOfAccntPlanIdToMemberIdForDelete.keySet() and Team_Member__c in :mapOfAccntPlanIdToMemberIdForDelete.values()]){
				
				if(mapOfAccntPlanIdToMemberIdForDelete.get(member.Account_Plan__c) == member.Team_Member__c) listOfAccntPlanTMForDelete.add(member);
			}
			
			if (!listOfAccntPlanTMForDelete.isEmpty()){
				delete listOfAccntPlanTMForDelete;
			}		
		}
	}
	
	// update fields in after insert event
	public static void doAccPlanBugSbuProcess(List<Account_Plan_2__c> triggerNew) {
				
		Map<String, Account_Plan_2__c> accBugMap = new Map<String, Account_Plan_2__c>();
		Set<Id> accSet = new Set<Id>();
		Set<Id> bugSet = new Set<Id>();
		
		for(Account_Plan_2__c ap2 : triggerNew)	{
			
			if(ap2.Account_Plan_Level__c == 'Business Unit Group') {						    	
				accSet.add(ap2.Account__c);	
				bugSet.add(ap2.Business_Unit_Group__c);
									    	
				string keyConcat = string.valueof(ap2.Account__c) + string.valueof(ap2.Business_Unit_Group__c);
				accBugMap.put(keyConcat, ap2);
			}
		}
		
		if(accSet.size() > 0){
			
			
			// Update SBU Acc Plans reference to BUG
			List<Account_Plan_2__c> updList = new List<Account_Plan_2__c>();
						
			for(Account_Plan_2__c ap2 : [Select Id,
											Account__c,
											Account_Plan__c,
											Business_Unit_Group__c
									   From Account_Plan_2__c
									  Where Account__c = :accSet 
									    And Account_Plan_Level__c = :'Sub Business Unit'
									    And Business_Unit_Group__c = :bugSet]) {
											
				string keyConcat = string.valueof(ap2.Account__c) + string.valueof(ap2.Business_Unit_Group__c);

				if(accBugMap.containsKey(keyConcat)) {
					ap2.Account_Plan__c = accBugMap.get(keyConcat).Id;
					updList.add(ap2);
				}
			}

			if(updList.size() >0){
				
				update updList;
								
				// Update Tender refernce to Account Plan
				Map<Id, Account_Plan_2__c> sbuAPMap  = new Map<Id, Account_Plan_2__c>(updList);
				
				List<Tender_Calendar_2__c> listOfTC2 = [select Id, 
													   BUG_iPlan2__c,
													   Account_Plan_ID__c 
												  from Tender_Calendar_2__c 
												 where Account_Plan_ID__c in :updList];
												 
				for (Tender_Calendar_2__c tc : listOfTC2) {
					tc.BUG_iPlan2__c = sbuAPMap.get(tc.Account_Plan_ID__c).Account_Plan__c;
			    }
						
				if(listOfTC2.size() > 0) update listOfTC2;
				
			}
		}		
	}


	public static void doAccPlanBugBuProcess(List<Account_Plan_2__c> triggerNew) {
				
		Map<String, Account_Plan_2__c> accBugMap = new Map<String, Account_Plan_2__c>();
		Set<Id> accSet = new Set<Id>();
		Set<Id> bugSet = new Set<Id>();
		
		for(Account_Plan_2__c ap2 : triggerNew)	{
			
			if(ap2.Account_Plan_Level__c == 'Business Unit Group') {						    	
				accSet.add(ap2.Account__c);	
				bugSet.add(ap2.Business_Unit_Group__c);
									    	
				string keyConcat = string.valueof(ap2.Account__c) + string.valueof(ap2.Business_Unit_Group__c);
				accBugMap.put(keyConcat, ap2);
			}
		}
		
		if(accSet.size() > 0){
			
			
			// Update BU Acc Plans reference to BUG
			List<Account_Plan_2__c> updList = new List<Account_Plan_2__c>();
						
			for(Account_Plan_2__c ap2 : [Select Id,
											Account__c,
											Account_Plan__c,
											Business_Unit_Group__c
									   From Account_Plan_2__c
									  Where Account__c = :accSet 
									    And Account_Plan_Level__c = :'Business Unit'
									    And Business_Unit_Group__c = :bugSet]) {
											
				string keyConcat = string.valueof(ap2.Account__c) + string.valueof(ap2.Business_Unit_Group__c);

				if(accBugMap.containsKey(keyConcat)) {
					ap2.Account_Plan__c = accBugMap.get(keyConcat).Id;
					updList.add(ap2);
				}
			}

			if(updList.size() >0){
				
				update updList;
								
				// Update Tender refernce to Account Plan
				Map<Id, Account_Plan_2__c> buAPMap  = new Map<Id, Account_Plan_2__c>(updList);
				
				List<Tender_Calendar_2__c> listOfTC2 = [select Id, 
													   BUG_iPlan2__c,
													   Account_Plan_ID__c 
												  from Tender_Calendar_2__c 
												 where Account_Plan_ID__c in :updList];
												 
				for (Tender_Calendar_2__c tc : listOfTC2) {
					tc.BUG_iPlan2__c = buAPMap.get(tc.Account_Plan_ID__c).Account_Plan__c;
			    }
						
				if(listOfTC2.size() > 0) update listOfTC2;
				
			}
		}		
	}
			
	public static void doAppUsageProcess(List<Account_Plan_2__c> triggerNew, List<Account_Plan_2__c> triggerOld) {
		String udAction;
		if (trigger.isInsert){		
			udAction = 'Create';
		}else if (trigger.isDelete){
			udAction = 'Delete';		
		}else if (trigger.isUpdate){
			udAction = 'Update';			
		}
		
		List<Account_Plan_2__c> records;
					
		if(Trigger.isInsert || Trigger.isUpdate) records = triggerNew;
		else records = triggerOld; 
		
		//Application usage details	
		if(udAction == null) return;
		
		List<Application_Usage_Detail__c> usageDetails = new List<Application_Usage_Detail__c>();
		
		for(Account_Plan_2__c record : records){
								
			Application_Usage_Detail__c usageDetail = new Application_Usage_Detail__c();			
			usageDetail.Action_Date__c = Date.today();
			usageDetail.Action_DateTime__c = Datetime.now();
			usageDetail.Action_Type__c = udAction;
			usageDetail.Capability__c = 'Account Plan';
			usageDetail.External_Record_Id__c = record.Id;
			usageDetail.Internal_Record_Id__c = record.Id;
			usageDetail.Object__c = 'Account_Plan_2__c';
			usageDetail.Process_Area__c = 'Account Planning';
			usageDetail.Source__c = 'SFDC';
			usageDetail.User_Id__c = UserInfo.getUserId();
						
			usageDetails.add(usageDetail);
		}	
			
		if(usageDetails.size()>0) insert usageDetails;  			
	}



	public static iPlanResultWrapper doesIplanExist(Account_Plan_2__c accPlan){
		if(accPlan.Business_Unit__c != null){
			return doesIplanExistForBU(accPlan);
		}
		
		if(accPlan.Business_Unit_Group__c != null){
			return doesIplanExistForBUGroup(accPlan);
		}
		
		if(accPlan.Sub_Business_Unit__c != null){
			return doesIplanExistForSBU(accPlan);
		}
		
		return new iPlanResultWrapper();
	}
	
	private static iPlanResultWrapper doesIplanExistForBU(Account_Plan_2__c accPlan){
		List<Account_Plan_2__c> accPlanFromDb = [select apl.Business_Unit__r.name, apl.Name, apl.Mobile_ID__c,
														apl.Owner.Name
												 from Account_Plan_2__c apl
												 where apl.Account__c =: accPlan.Account__c
												 and apl.Business_Unit__c =: accPlan.Business_Unit__c and apl.Account_Plan_Level__c = 'Business Unit'];
		if(accPlanFromDb.size() > 0){
			string message = 'There is an existing iPlan for ' +accPlanFromDb[0].Business_Unit__r.Name +'.|| Please contact '+accPlanFromDb[0].owner.name+' to add you to the accountplan team members.';		
			return returnIplanExists(accPlanFromDb,accPlan,message);
		}
		
		iPlanResultWrapper result = new iPlanResultWrapper();
		result.isFound = false;
		result.errorMessage = 'There is no iPlan found.';
		
		return result;
	}
	
	private static iPlanResultWrapper doesIplanExistForBUGroup(Account_Plan_2__c accPlan){
		List<Account_Plan_2__c> accPlanFromDb = [select apl.Business_Unit__c, apl.Name, apl.Mobile_ID__c,
														apl.Business_Unit_Group__r.name,apl.Owner.Name
												 from Account_Plan_2__c apl
												 where apl.Account__c =: accPlan.Account__c
												 and apl.Business_Unit_Group__c =: accPlan.Business_Unit_Group__c and apl.Account_Plan_Level__c = 'Business Unit Group'];
		if(accPlanFromDb.size() > 0){ System.debug('accPLanFromDb '+accPlanFromDb);
			string message = 'There is an existing iPlan for ' +accPlanFromDb[0].Business_Unit_Group__r.Name +'.|| Please contact '+accPlanFromDb[0].owner.name+' to add you to the accountplan team members.';		
			return returnIplanExists(accPlanFromDb,accPlan,message);
		}
		
		iPlanResultWrapper result = new iPlanResultWrapper();
		result.isFound = false;
		result.errorMessage = 'There is no iPlan found.';
		
		return result;
	}
	
	private static iPlanResultWrapper doesIplanExistForSBU(Account_Plan_2__c accPlan){
		List<Account_Plan_2__c> accPlanFromDb = [select apl.Business_Unit__c, apl.Name, apl.Mobile_ID__c,
														apl.Sub_Business_Unit__r.name,apl.Owner.Name
												 from Account_Plan_2__c apl
												 where apl.Account__c =: accPlan.Account__c
												 and apl.Sub_Business_Unit__c =: accPlan.Sub_Business_Unit__c and apl.Account_Plan_Level__c = 'Sub Business Unit'];
		if(accPlanFromDb.size() > 0){	
			string message = 'There is an existing iPlan for ' +accPlanFromDb[0].Sub_Business_Unit__r.Name +'.|| Please synchronize.';		
			return returnIplanExists(accPlanFromDb,accPlan,message);
		}
		
		iPlanResultWrapper result = new iPlanResultWrapper();
		result.isFound = false;
		result.errorMessage = 'There is no iPlan found.';
		return result;
	}

	private static iPlanResultWrapper returnIplanExists(List<Account_Plan_2__c> accPlanFromDb,Account_Plan_2__c accPlan,string message){
			string errorMessage = message;
			iPlanResultWrapper result = new iPlanResultWrapper();
			result.errorMessage = errorMessage;
			result.iPlan = accPlanFromDb[0];
			result.isFound = true;
			return result;
	}

}