global class ba_TestEquipment_CalibrationStatus implements Schedulable,Database.Batchable<sObject> {
 
	global void execute(SchedulableContext ctx)
    {
        Database.executeBatch(new ba_TestEquipment_CalibrationStatus(),2000);
    } 
        
    global database.querylocator start(Database.BatchableContext BC)
    {
       	string query = 'SELECT Id, SVMX_Calibration_Status__c FROM SVMX_Test_Equipment__c '+
       				   ' WHERE SVMX_Calibration_Due_Date__c < TODAY AND SVMX_Calibration_Status__c = \'In Calibration\''; 
                 
  		return Database.getQueryLocator(query);        
    }
    
    global void execute(Database.BatchableContext BC, List<SVMX_Test_Equipment__c> scope)
    {
    	for(SVMX_Test_Equipment__c testEq : scope){
    		
    		testEq.SVMX_Calibration_Status__c = 'Out of Calibration';
    	}
    	
    	Database.update(scope, false); // Allow individual success / failure
   	}
    
    global void finish(Database.BatchableContext BC)
    {

    }   
}