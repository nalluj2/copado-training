@isTest(seealldata=true)
private class Test_AccountMergeClasses{
	    
    static testMethod void myUnitTest(){
    	       
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];    
                
        User u;
        
        System.runAs ( thisUser ){    
			
			Territory2 ter = new Territory2(); 			                      
            ter.Territory2TypeId = bl_Territory.territoryTypeMap.get('Region');   
            ter.Business_Unit__c='CRHF';
            ter.Country_UID__c='BE'; 
            ter.CurrencyIsoCode='EUR';
            ter.Therapy_Groups_Text__c = 'CardioInsight';
            ter.Short_Description__c='CRHF';
            ter.name = 'Unit Test territory';
            ter.DeveloperName = 'UnitTest_territory_id';
            ter.Territory2ModelId = [Select Id from Territory2Model where DeveloperName = 'Medtronic'].Id;
            ter.Company__c = String.valueOf([Select Id from Company__c where Name = 'Europe'].Id).substring(0,15);
            insert ter;   
                                        
            Profile p = [select id from profile where name='EUR Field Force CVG'];    
            
            String t = [select Territory_UID2__c from Territory2 where id =:ter.id].Territory_UID2__c;
            
            u = new User(alias = 'standt1', email='standarduser@testorg.com',             
                        emailencodingkey='UTF-8', lastname='TerritoryManagement_Testing',             
                        languagelocalekey='en_US',             
                        localesidkey='en_US', profileid = p.Id,            
                        timezonesidkey='America/Los_Angeles',             
                        Region_vs__c = 'Europe',
                        Sub_Region__c = 'NWE',          
                        Country_vs__c= 'BELGIUM',                                    
                        username='TerritoryManagement_standarduser1@testorg.com',
                        Territory_UID__c=t,
                        Alias_unique__c='standarduser_Alias_unique__c',Company_Code_Text__c='EUR');        
                
            // End Create Territory
		}  
            
        Territory2 terr1=[select id,name,Business_Unit__c,Territory2Type.DeveloperName,Therapy_Groups_Text__c from Territory2 where Territory2Type.DeveloperName='Territory' limit 1];
                                  
        //Insert Account
        Account a = new Account() ;
        a.Name = 'Test Account Name ';  
        a.RecordTypeId = FinalConstants.sapAccountRecordTypeId ;
        a.Account_Country_vs__c='BELGIUM';
        
        System.runAs ( U ){
            insert a;
        }
                
        Account a1 = new Account() ;
        a1.Name = 'Test Account Name ';
        a1.Account_Country_vs__c='BELGIUM';
        insert a1;
                
                
        ObjectTerritory2Association ass = new ObjectTerritory2Association();
        ass.Territory2Id = terr1.id;
        ass.ObjectId=a.id;
        ass.AssociationCause ='Territory2Manual';
        insert ass;
                
        ObjectTerritory2Association ass1 = new ObjectTerritory2Association();
        ass1.Territory2Id=terr1.id;
        ass1.ObjectId=a1.id;        
        ass1.AssociationCause ='Territory2Manual';        
        insert ass1;
                        
        Test.startTest();
        merge a a1;
                
        Test.stopTest();
	}
}