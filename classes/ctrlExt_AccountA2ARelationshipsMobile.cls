public with sharing class ctrlExt_AccountA2ARelationshipsMobile {
	
	public List<Affiliation__c> relationships {get; set;}
	
	public ctrlExt_AccountA2ARelationshipsMobile(ApexPages.StandardController sc){
		
		Account acc = (Account) sc.getRecord();
				
		relationships = [Select Id, Affiliation_To_Account__c, Affiliation_To_Account__r.Name, Affiliation_From_Account__c,
							Affiliation_From_Account__r.Name,Affiliation_Type__c,Affiliation_Start_Date__c ,Affiliation_End_Date__c from Affiliation__c 
							where RecordType.DeveloperName = 'A2A' AND (Affiliation_From_Account__c =:acc.Id OR Affiliation_To_Account__c =:acc.Id)
							AND Affiliation_Type__c NOT IN ('Ship To','Ship To/Sold To')
							ORDER BY Affiliation_Start_Date__c ];
		
		for(Affiliation__c aff : relationships){
			
			if(aff.Affiliation_To_Account__c == acc.Id){
				
				aff.Affiliation_To_Account__r = aff.Affiliation_From_Account__r;
				aff.Affiliation_To_Account__c = aff.Affiliation_From_Account__c;				
			}			
		}
	}
}