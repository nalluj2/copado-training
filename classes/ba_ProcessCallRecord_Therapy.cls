global class ba_ProcessCallRecord_Therapy implements Database.Batchable<SObject>, Database.Stateful {

    global String tAdditionalEmail = '';
    global String tSOQL = 'SELECT Id, Therapy__c FROM Call_Records__c WHERE Business_Unit__r.Name in (\'Vascular\',\'CRHF\',\'CS & SH\')';

    global Map<Id, LogRecord> mapLogRecord;

    global Database.QueryLocator start(Database.BatchableContext context) {

        mapLogRecord = new Map<Id, LogRecord>();
        return Database.getQueryLocator(tSOQL);

    }

    global void execute(Database.BatchableContext oBC, List<Call_Records__c> lstData){
        
        Map<Id, List<Call_Topics__c>> mapCallRecordId_CallTopics = new Map<Id, List<Call_Topics__c>>();
        Map<Id, List<Contact_Visit_Report__c>> mapCallRecordId_CallContacts = new Map<Id, List<Contact_Visit_Report__c>>();
        
        for (Call_Records__c oCallRecord : [Select Id, 
        										(SELECT Call_Records__c, Call_Topic_Therapy__r.Name FROM Performed_Actions__r),
        										(SELECT Id, Therapy__c FROM AttendedContacts__r) 
        										from Call_Records__c where Id IN :lstData]){
                    
            mapCallRecordId_CallTopics.put(oCallRecord.Id, oCallRecord.Performed_Actions__r);
            mapCallRecordId_CallContacts.put(oCallRecord.Id, oCallRecord.AttendedContacts__r);
        }

        List<Call_Records__c> lstData_Update = new List<Call_Records__c>();
        List<Contact_Visit_Report__c> lstCallContacts_Update = new List<Contact_Visit_Report__c>();
        
        for (Call_Records__c oCallRecord : lstData){
        
            List<Call_Topics__c> lstCallTopic_Tmp = mapCallRecordId_CallTopics.get(oCallRecord.Id);

            String tTherapy_Old = oCallRecord.Therapy__c;
            String tTherapy_New = '';

            Set<String> setTherapy_New = new Set<String>();
            for (Call_Topics__c oCallTopic : lstCallTopic_Tmp){
            	setTherapy_New.add(oCallTopic.Call_Topic_Therapy__r.Name);
            }

	        List<String> lstTherapy_New = new List<String>(setTherapy_New);
            lstTherapy_New.sort();    
            tTherapy_New = String.join(lstTherapy_New, ';');

            if ( (tTherapy_New.length() > 0) && (tTherapy_New.right(1) != ';') ) tTherapy_New += ';';

            System.debug('**BC** tTherapy_Old : ' + tTherapy_Old);
            System.debug('**BC** tTherapy_New : ' + tTherapy_New);

            if (tTherapy_New != tTherapy_Old){
            	
            	oCallRecord.Therapy__c = tTherapy_New;
                lstData_Update.add(oCallRecord);
                
                for(Contact_Visit_Report__c callContact : mapCallRecordId_CallContacts.get(oCallRecord.Id)){
                	
                	callContact.Therapy__c = tTherapy_New;
                	lstCallContacts_Update.add(callContact);
                }
                
                mapLogRecord.put(oCallRecord.Id, new LogRecord(oCallRecord.Id, 'Therapy__c', tTherapy_Old, tTherapy_New));
            }
        }

        System.debug('**BC** lstData_Update (' + lstData_Update.size() + ') : ' + lstData_Update);
        if (lstData_Update.size() > 0){

            for (Database.SaveResult oSaveResult : Database.update(lstData_Update, false)){
                
                Id id_Record = oSaveResult.getId();
                LogRecord oLogRecord = mapLogRecord.get(id_Record);
                oLogRecord.tStatus = 'Success';
                oLogRecord.tError = '';

                if (oSaveResult.isSuccess() == false){
                    
                    oLogRecord.tStatus = 'Fail';
                    String tError = '';
                    
                    for (Database.Error oError : oSaveResult.getErrors()){
                        tError += oError.getMessage();                      
                    }   
                    oLogRecord.tError = tError;

                }           

            }

        }
        
        if (lstCallContacts_Update.size() > 0){

            Database.update(lstCallContacts_Update, false);
        }


        if (mapLogRecord.size() > 9500) saveLogRecord(oBC);
    
    }
    
    global void finish(Database.BatchableContext oBC) {

        saveLogRecord(oBC);
        
    }

    public void saveLogRecord(Database.BatchableContext oBC){

        List<Data_Migration_Log__c> lstDataMigrationLog = new List<Data_Migration_Log__c>();
        for (LogRecord oLogRecord : mapLogRecord.values()){

            Data_Migration_Log__c oDataMigrationLog = new Data_Migration_Log__c();
                oDataMigrationLog.Type__c = 'ba_ProcessCallRecord_Therapy';
                oDataMigrationLog.Record_Id__c = oLogRecord.id_Record;
                oDataMigrationLog.Fieldname__c = oLogRecord.tFieldName;
                oDataMigrationLog.New_Value__c = oLogRecord.tValue_New;
                oDataMigrationLog.Old_Value__c = oLogRecord.tValue_Old;
                oDataMigrationLog.Status__c = oLogRecord.tStatus;
                oDataMigrationLog.Error__c = oLogRecord.tError;
            lstDataMigrationLog.add(oDataMigrationLog);

        }

        if (lstDataMigrationLog.size() > 0) insert lstDataMigrationLog;

        sendEmail(oBC);
        
        // Reset the Log Record Collection
        mapLogRecord = new Map<Id, LogRecord>();

    }

    public void sendEmail(Database.BatchableContext oBC){

        System.debug('**BC** mapLogRecord (' + mapLogRecord.size() + ') : ' + mapLogRecord);

        String tEmailBody = getMessage(oBC);

        Blob oBlob_CSV = Blob.valueOf( getAttachmentBody() );
        String tSuffix = Datetime.now().format('yyyyMMdd_HH:mm:ss');
        String tName_CSV = 'ba_ProcessCallRecord_Therapy' + tSuffix + '.csv';

        Messaging.EmailFileAttachment oEmailFileAttachment = new Messaging.EmailFileAttachment();
            oEmailFileAttachment.setFileName(tName_CSV);
            oEmailFileAttachment.setBody(oBlob_CSV);

        List<String> lstTO = new List<String>{UserInfo.getUserEmail()};
        List<String> lstCC = tAdditionalEmail.split(',');
        
        Messaging.SingleEmailMessage oEmail = new Messaging.SingleEmailMessage();
            oEmail.setSubject('ba_ProcessCallRecord_Therapy' + tSuffix);
            oEmail.setToAddresses(lstTO);
            if (lstCC != null && lstCC.size() > 0 && lstCC[0] != '') oEmail.setCcAddresses(lstCC);
            oEmail.setPlainTextBody(tEmailBody);
            oEmail.setFileAttachments(new List<Messaging.EmailFileAttachment>{oEmailFileAttachment});
        List<Messaging.SendEmailResult> lstSendEmailResult = Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{oEmail});

        // Reset the Log Record Collection
        mapLogRecord = new Map<Id, LogRecord>();

    }

    public String getAttachmentBody(){

        List<String> lstFieldName = new List<String>();
        List<String> lstLine = new List<String>();

        lstFieldName.add('Record Id');
        lstFieldName.add('Fieldname');
        lstFieldName.add('Old Value');
        lstFieldName.add('New Value');
        lstFieldName.add('Status');
        lstFieldName.add('Error');        

        String tHeader = String.join(lstFieldName, ',');
        lstLine.add(tHeader);

        for (LogRecord oLogRecord : mapLogRecord.values()){
            
            List<String> lstValue = new List<String>();

            lstValue.add(oLogRecord.id_Record);
            lstValue.add(oLogRecord.tFieldName);
            lstValue.add(oLogRecord.tValue_Old);
            lstValue.add(oLogRecord.tValue_New);
            lstValue.add(oLogRecord.tStatus);
            lstValue.add(oLogRecord.tError);
            
            lstLine.add(String.join(lstValue, ','));

        }

        return String.join(lstLine, '\n');
    }

    public String getMessage(Database.BatchableContext oBC){

        List<String> lstMessageLine = new List<String>();

        AsyncApexJob oAsyncApexJob =
            [
                SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Id,CompletedDate
                FROM AsyncApexJob
                WHERE Id = :oBC.getJobId()
            ];
        
        lstMessageLine.add('Hi ' + UserInfo.getFirstName() + ', ');
        lstMessageLine.add('');

        lstMessageLine.add('ba_ProcessCallRecord_Therapy Report.');
        lstMessageLine.add('');

        lstMessageLine.add('Batch APEX Execution Status : '+ oAsyncApexJob.Status);
        lstMessageLine.add('The batch apex job processed '+ oAsyncApexJob.JobItemsProcessed + ' / ' + oAsyncApexJob.TotalJobItems + ' batches with '+ oAsyncApexJob.NumberOfErrors + ' failures.');

        lstMessageLine.add('In the attachment you will find a full report.');
        lstMessageLine.add('');
        lstMessageLine.add('');

        return String.join(lstMessageLine, '\n');
    }


    global class LogRecord {
        
        public Id id_Record;
        Public String tFieldName;
        Public String tValue_Old;
        Public String tValue_New;
        Public String tStatus;
        Public String tError;

        public LogRecord(Id aID_Record, String atFieldName, String atValue_Old, String atValue_New){
            id_Record = aID_Record;
            tFieldName = atFieldName;
            tValue_New = atValue_New;
            tValue_Old = atValue_Old;
        }

    }

}
//--------------------------------------------------------------------------------------------------------------------