@isTest
public with sharing class Test_ctrlExt_CreateUserRequestAdmin {
    
    private static User currentUser = [Select Id, Email from User where Id=:UserInfo.getUserId()];
 
    private static testMethod void sfdcDispatcher(){
                       
        Create_User_Request__c userRequest = createNewUserRequest('Salesforce.com', true);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(userRequest);
        ctrlExt_Support_Request_Dispatcher controller = new ctrlExt_Support_Request_Dispatcher(sc);
        
        controller.redirect();
    }
    
    private static testMethod void submitUserRequest(){
        
        System.runAs(currentUser){
            assignUserToAdminGroup();
        }
        
        Create_User_Request__c userRequest = createNewUserRequest('Salesforce.com', true);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(userRequest);
        ctrlExt_CreateUserRequestAdmin controller = new ctrlExt_CreateUserRequestAdmin(sc);
        
        System.assert(controller.isAdminUser == true) ;
        
        controller.submit();
    }
    
    private static testMethod void completeUserRequest(){
        
        System.runAs(currentUser){
            assignUserToAdminGroup();
        }
        
        Create_User_Request__c userRequest = createNewUserRequest('Salesforce.com', true);

        userRequest.Time_Spent__c = 90;
        userRequest.Assignee__c = currentUser.Id;
        userRequest.Sub_Status__c = 'Solved (Permanently)';

        ApexPages.StandardController sc = new ApexPages.StandardController(userRequest);
        ctrlExt_CreateUserRequestAdmin controller = new ctrlExt_CreateUserRequestAdmin(sc);
        
        System.assert(controller.isAdminUser == true) ;

        Boolean bTest = controller.bIsUpdatable;
        bTest = controller.bIsAssigned;

        controller.completeRequest();

        System.assert(userRequest.Status__c == 'Completed');

        bTest = controller.bIsUpdatable;
        bTest = controller.bIsAssigned;

    }

    private static testMethod void completeUserRequest_Error(){

        System.runAs(currentUser){
            assignUserToAdminGroup();
        }
        
        Create_User_Request__c userRequest = createNewUserRequest('Salesforce.com', true);

        ApexPages.StandardController sc = new ApexPages.StandardController(userRequest);
        ctrlExt_CreateUserRequestAdmin controller = new ctrlExt_CreateUserRequestAdmin(sc);
        
        System.assert(controller.isAdminUser == true) ;

        controller.completeRequest();


        userRequest.Time_Spent__c = 90;

        sc = new ApexPages.StandardController(userRequest);
        controller = new ctrlExt_CreateUserRequestAdmin(sc);
        
        controller.completeRequest();

        userRequest.Sub_Status__c = 'Solved (Permanently)';

        sc = new ApexPages.StandardController(userRequest);
        controller = new ctrlExt_CreateUserRequestAdmin(sc);
        
        controller.completeRequest();

    }
    
    private static testMethod void ResubmitUserRequest(){
                                    
        Create_User_Request__c userRequest = createNewUserRequest('Lighthouse', true);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(userRequest);
        ctrlExt_CreateUserRequestAdmin controller = new ctrlExt_CreateUserRequestAdmin(sc);
                
        controller.resubmit();
    }
    
    private static testMethod void configureAndExecuteUserRequest(){
        
        System.runAs(currentUser){
            assignUserToAdminGroup();
        }
                
        Test.startTest();
        
        Create_User_Request__c userRequest = createNewUserRequest('Salesforce.com', true);
        
        userRequest.Status__c = 'Approved';
        update userRequest;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(userRequest);
        ctrlExt_CreateUserRequestAdmin controller = new ctrlExt_CreateUserRequestAdmin(sc);
        
        System.assert(controller.isAdminUser == true) ;
        System.assert(userRequest.Username__c == 'mock@medtronic.com');
        
        userRequest.Username__c = 'mock@medtronic.com.test'; //Make sure it doesn't exist
        userRequest.Alias__c = 'utMockA';
        userRequest.Nickname__c = 'unitTestMockUser';
        userRequest.Profile__c = 'System Administrator';
//        userRequest.Federation_ID__c

        userRequest.Time_Spent__c = 60;

        userRequest.New_Alias__c = 'utMockAA';
                                
        controller.saveConfiguration();
        controller.saveAndApply();
        
        Test.stopTest();
        
        List<User> createdUser = [Select Id, FederationIdentifier, Alias from User where Username = 'mock@medtronic.com.test'];
        
        System.assert(createdUser.size()>0);
        System.assertEquals(createdUser[0].FederationIdentifier, userRequest.Alias__c);
        System.assertEquals(createdUser[0].Alias, userRequest.Alias__c, 'utMockA');
        System.assert(String.isBlank(userRequest.New_Alias__c));

        clsUtil.hasException = true;

        try{
            controller.saveConfiguration();
        }catch(Exception oEX){}

    }

    private static testMethod void configureAndExecuteUserRequest_ExistingUser(){
        
        System.runAs(currentUser){
            assignUserToAdminGroup();
        }
                
        Test.startTest();
        
        Create_User_Request__c userRequest = createNewUserRequest('Salesforce.com', true);
        
        userRequest.Status__c = 'Approved';
        update userRequest;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(userRequest);
        ctrlExt_CreateUserRequestAdmin controller = new ctrlExt_CreateUserRequestAdmin(sc);
        
        System.assert(controller.isAdminUser == true) ;
        System.assert(userRequest.Username__c == 'mock@medtronic.com');
        
        userRequest.Username__c = 'mock@medtronic.com.test'; //Make sure it doesn't exist
        userRequest.Alias__c = 'utMockA';
        userRequest.Nickname__c = 'unitTestMockUser';
        userRequest.Profile__c = 'System Administrator';
//        userRequest.Federation_ID__c

        userRequest.Time_Spent__c = 60;

        userRequest.New_Alias__c = 'utMockAA';
                                
        controller.saveConfiguration();
        controller.saveAndApply();


        Create_User_Request__c userRequest2 = createNewUserRequest('Salesforce.com', false);
        
        userRequest2.Status__c = 'Approved';
        update userRequest2;
        
        ApexPages.StandardController sc2 = new ApexPages.StandardController(userRequest2);
        ctrlExt_CreateUserRequestAdmin controller2 = new ctrlExt_CreateUserRequestAdmin(sc2);
        
        System.assert(controller2.isAdminUser == true) ;
        System.assert(userRequest2.Username__c == 'mock@medtronic.com');
        
        userRequest2.Username__c = 'mock@medtronic.com.test'; //Make sure it doesn't exist
        userRequest2.Alias__c = 'utMockA';
        userRequest.Nickname__c = 'unitTestMockUser';
        userRequest2.Profile__c = 'System Administrator';
//        userRequest.Federation_ID__c

        userRequest2.Time_Spent__c = 60;

        userRequest2.New_Alias__c = 'utMockAA';
                                
        controller2.saveConfiguration();
        controller2.saveAndApply();

        
        Test.stopTest();
        
        List<User> createdUser = [Select Id, FederationIdentifier, Alias from User where Username = 'mock@medtronic.com.test'];
        
        System.assert(createdUser.size()>0);
        System.assertEquals(createdUser[0].FederationIdentifier, userRequest.Alias__c);
        System.assertEquals(createdUser[0].Alias, userRequest.Alias__c, 'utMockA');
        System.assert(String.isBlank(userRequest.New_Alias__c));

        clsUtil.hasException = true;

        try{
            controller2.saveConfiguration();
        }catch(Exception oEX){}

    }    
    
    private static testMethod void checkAvailability(){
        
        System.runAs(currentUser){
            assignUserToAdminGroup();
        }
        
        Create_User_Request__c userRequest = new Create_User_Request__c();          
        userRequest.Status__c = 'New';
        userRequest.Application__c = 'Salesforce.com';
        userRequest.Request_Type__c = 'Create User';            
        userRequest.Requestor_Email__c = currentUser.Email;     
        userRequest.Status__c = 'Approved';
        userRequest.Alias__c = 'utMockA';
        userRequest.Firstname__c = 'test';
        userRequest.Lastname__c = 'user';
        userRequest.Email__c = 'mock@medtronic.com';
        userRequest.Manager_Email__c = currentUser.Email;
        userRequest.Cost_Center__c = '123456';
        userRequest.Peoplesoft_ID__c = '123456';
        userRequest.Profile__c = 'Admin';
        userRequest.Role__c = 'System Administrator';
        userRequest.Nickname__c = 'unitTestMockUser';
        userRequest.Username__c = 'mock@medtronic.com.test'; //Make sure it doesn't exist
        
        Test.startTest();
        
        ApexPages.StandardController sc = new ApexPages.StandardController(userRequest);
        ctrlExt_CreateUserRequestAdmin controller = new ctrlExt_CreateUserRequestAdmin(sc);
        
        System.assert(controller.isAdminUser == true);
        
        controller.checkUsernameAvailability();
        System.assert(ApexPages.getMessages().size()==1);
        System.assert(ApexPages.getMessages()[0].getSummary()=='Username is available');        

        clsUtil.hasException = false;

        controller.checkUsernameAvailability();

        clsUtil.hasException = true;
        try{
            controller.checkUsernameAvailability();
        }catch(Exception oEX){}

    }
    
    private static testmethod void scheduleDeactivateUserRequest(){
        
        Create_User_Request__c deactivateRequest = createUserRequest('De-activate User');
        deactivateRequest.Created_User__c = [Select Id from User where isActive=true AND Id!=:UserInfo.getUserId() LIMIT 1].Id;
        deactivateRequest.Date_of_deactivation__c = DateTime.now().addMinutes(5);
        deactivateRequest.De_activated_in_Staging__c = true;
        
        insert deactivateRequest;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(deactivateRequest);
        ctrlExt_CreateUserRequestAdmin controller = new ctrlExt_CreateUserRequestAdmin(sc);
        
        controller.scheduleDeactivation();
        
        System.assert(controller.deactivationProcess!=null);
        controller = new ctrlExt_CreateUserRequestAdmin(sc);        
    }
    
    private static testmethod void deactivateUserRequest(){
        
    User oUser_Admin1 = clsTestData_User.createUser_SystemAdministratorMDT('tstadm1', false);
    User oUser_Admin2 = clsTestData_User.createUser_SystemAdministratorMDT('tstadm2', false);

    List<User> lstUser = new List<User>{ oUser_Admin1, oUser_Admin2 };
    insert lstUser;

    Create_User_Request__c deactivateRequest;

    List<Opportunity> lstOpportunity = new List<Opportunity>();

    System.runAs(oUser_Admin1){

      lstOpportunity = clsTestData_Opportunity.createOpportunity();
      lstOpportunity = [SELECT Id, Name, OwnerId FROM Opportunity];
      System.assertEquals(lstOpportunity[0].OwnerId, oUser_Admin1.Id);

      deactivateRequest = createUserRequest('De-activate User');
        deactivateRequest.Created_User__c = oUser_Admin1.Id;
        deactivateRequest.Date_of_deactivation__c = DateTime.now();
//        deactivateRequest.New_owner_of_opportunities__c = oUser_Admin2.Id;
        deactivateRequest.De_activated_in_Staging__c = true;
      insert deactivateRequest;

    }
        
        Test.startTest();
        
      System.runAs(oUser_Admin1){

        ApexPages.StandardController sc = new ApexPages.StandardController(deactivateRequest);
        ctrlExt_CreateUserRequestAdmin controller = new ctrlExt_CreateUserRequestAdmin(sc);
        
        controller.deactivateUser();

      }
        
        Test.stopTest();

    System.runAs(oUser_Admin2){

      ba_Deactivate_User_Process oBatch = new ba_Deactivate_User_Process();
      oBatch.oRequest = deactivateRequest;

      oBatch.processOpportunity(lstOpportunity, oUser_Admin2.Id);

      oBatch.bUserDeactivated = false;
      oBatch.iError_Opportunity = 1;
      oBatch.updateSupportTicket();

      oBatch.bUserDeactivated = true;
      oBatch.iError_Opportunity = 0;
      oBatch.updateSupportTicket();

      oBatch.iError_Opportunity = 1;
      oBatch.iSuccess_Opportunity = 1;
      oBatch.tError_Opportunity = 'error opportunity';
      oBatch.tSuccess_Opportunity = 'success opportunity';
      oBatch.tError_Request = 'error request';
      List<Attachment> lstAttachment = oBatch.createAttachment();
      oBatch.sendEmail(lstAttachment);

    }
        //Because of the MIXED_DML exception this operation fails. There is no ways to execute the whole logic in a test method due to
        //that restriction. Anyway, when it fails in gives better code coverage so I leave it like it is :D
        //User deactivatedUser = [Select Id, isActive from User where Id=:deactivateRequest.Created_User__c];
        //System.assert(deactivatedUser.isActive == true);
        
        //deactivateRequest = [Select Id, Status__c from Create_User_Request__c where Id=:deactivateRequest.Id];
        //System.assert(deactivateRequest.Status__c == 'New');      
    }
    
    private static testmethod void resetPasswordRequest(){
        
        Create_User_Request__c resetPasswordRequest = createUserRequest('Reset Password');
        resetPasswordRequest.Time_Spent__c = 60;
        resetPasswordRequest.Created_User__c = [Select Id from User where isActive=true AND Id!=:UserInfo.getUserId() LIMIT 1].Id;
                
        insert resetPasswordRequest;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(resetPasswordRequest);
        ctrlExt_CreateUserRequestAdmin controller = new ctrlExt_CreateUserRequestAdmin(sc);
        
        controller.resetUserPassword();         
    }
    
    private static testmethod void reactivateUserRequest(){
        
        User oUser = [SELECT Id, Name, ProfileId, Profile.Name FROM User WHERE IsActive = TRUE AND Profile.Name = 'System Administrator MDT' LIMIT 1];

        User testUSer = [Select Id from User where isActive=false AND UserRoleId=null AND Id!=:UserInfo.getUserId() LIMIT 1];
                
        Create_User_Request__c reactivateUserRequest = createUserRequest('Re-activate User');
        reactivateUserRequest.Created_User__r = testUser;
        reactivateUserRequest.Created_User__c = testUser.Id;
                
        insert reactivateUserRequest;
        
        Test.startTest();
        
        System.runAs(oUser){
            ApexPages.StandardController sc = new ApexPages.StandardController(reactivateUserRequest);
            ctrlExt_CreateUserRequestAdmin controller = new ctrlExt_CreateUserRequestAdmin(sc);
            
            try{
                clsUtil.hasException = false;
                controller.reactivateUser();
            }catch(Exception oEX){
            }


            try{
                clsUtil.hasException = true;
                controller.reactivateUser();
            }catch(Exception oEX){
            }

        }
    }
    
    private static testMethod void configureUserRequestBUS(){
        
        System.runAs(currentUser){
            assignUserToAdminGroup();
        }
                        
        Create_User_Request__c userRequest = createNewUserRequest('Salesforce.com', true);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(userRequest);
        ctrlExt_CreateUserRequestAdmin controller = new ctrlExt_CreateUserRequestAdmin(sc);
        
        System.assert(controller.isAdminUser == true) ;
        
        Sub_Business_Units__c sbu = [Select Id, Business_Unit__c from Sub_Business_Units__c];
        
        controller.selectAll = false;
        controller.selectAllBU();
        
        controller.selectedItem = sbu.Business_Unit__c;
        controller.selectBU();
        
        controller.selectedItem = sbu.Id;
        controller.userSBUById.get(sbu.Id).selected=true;
        controller.userSBUById.get(sbu.Id).primary=true;
        controller.selectSBU();
        controller.makePrimary();
    }

    private static testMethod void assigenToMe(){

        System.runAs(currentUser){
            assignUserToAdminGroup();
        }
                        
        Create_User_Request__c userRequest = createNewUserRequest('Salesforce.com', true);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(userRequest);
        ctrlExt_CreateUserRequestAdmin controller = new ctrlExt_CreateUserRequestAdmin(sc);

        System.assert(controller.isAdminUser == true);

        controller.assignToMe();

    }

    private static testMethod void assignToOtherUser(){

        System.runAs(currentUser){
            assignUserToAdminGroup();
        }
                        
        Create_User_Request__c userRequest = createNewUserRequest('Salesforce.com', true);
        userRequest.Created_User__c = UserInfo.getUserId();
        update userRequest;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(userRequest);
        ctrlExt_CreateUserRequestAdmin controller = new ctrlExt_CreateUserRequestAdmin(sc);

        System.assert(controller.isAdminUser == true);

        Id id_AdminUser;
        for (Id id_User : mapUser_Admin.keySet()){
            if (id_User != UserInfo.getUserId()){
                id_AdminUser = id_User;
                break;
            }
        }
        if (id_AdminUser != null){
            controller.tAdminUserId = id_AdminUser;
            controller.saveConfiguration();
        }

    }

    
    //Need to seeAllData because of the Email templates used in the code to test
    @isTest(seeAllData = true)
    private static void userRequestApproverNotifications(){
                
        System.runAs( currentUser ){
                
            Support_application__c appConfig = new Support_application__c();        
            appConfig.Name = 'UnitTestApplication';
            appConfig.Emails_of_approvers__c = currentUser.Email;
            insert appConfig;
        }
        
        Test.startTest();
            
        Create_User_Request__c sfdcUserRequest = new Create_User_Request__c();
        sfdcUserRequest.Status__c = 'New';
        sfdcUserRequest.Request_Type__c = 'Create User';    
        sfdcUserRequest.Manager_Email__c = currentUser.Email;
        sfdcUserRequest.Application__c = 'Salesforce.com';
        
        Create_User_Request__c testAppUserRequest = new Create_User_Request__c();
        testAppUserRequest.Status__c = 'New';
        testAppUserRequest.Request_Type__c = 'Create User'; 
        testAppUserRequest.Application__c = 'UnitTestApplication';
        
        bl_CreateUserRequest.notifyUserRequestApprovers(new List<Create_User_Request__c>{sfdcUserRequest, testAppUserRequest});
    }
    
    //Need to seeAllData because of the Email templates used in the code to test
    @isTest(seeAllData = true)
    private static void dataLoadApproverNotifications(){
                        
        System.runAs(currentUser){
        
            Support_application__c appConfig = Support_application__c.getValues('Data Load Request');       
            
            if(appConfig == null){
                appConfig = new Support_Application__c();
                appConfig.Name = 'Data Load Request';
                appConfig.Emails_of_approvers__c = currentUser.Email;
                insert appConfig;
            }else{
                appConfig.Emails_of_approvers__c = currentUser.Email;
                update appConfig;
            }
        }
        
        Test.startTest();
        
        Create_User_Request__c dataLoadRequest = new Create_User_Request__c();
        dataLoadRequest.Status__c = 'New';
        dataLoadRequest.Application__c = 'Salesforce.com';
        dataLoadRequest.Request_Type__c = 'Data Load';  
        dataLoadRequest.Data_Type__c = 'Account';
        dataLoadRequest.Data_Load_Approvers__c = 'system@medtronic.com;';
        
        insert dataLoadRequest;
                
        bl_CreateUserRequest.notifyDataLoadApprovers(dataLoadRequest);
    }
        
    private static testmethod void bugRequestView(){
                
        Create_User_Request__c bugRequest = new Create_User_Request__c();
        bugRequest.Status__c = 'New';
        bugRequest.Application__c = 'Salesforce.com';
        bugRequest.Request_Type__c = 'Bug'; 
        bugRequest.Description_Long__c = 'Unit Test bug description';
        
        insert bugRequest;
                        
        Blob b = Blob.valueOf('Test Data');  
      
        Attachment attachment = new Attachment();  
        attachment.ParentId = bugRequest.Id;  
        attachment.Name = 'Test Attachment for Parent';  
        attachment.Body = b;  
          
        insert attachment; 
        
        bugRequest = [Select Id, Assignee__c, CreatedDate, Description_Long__c, Status__c, Request_Type__c from Create_User_Request__c where Id =:bugRequest.Id];
        
        Test.startTest();
        
        ApexPages.Standardcontroller sc = new ApexPAges.Standardcontroller(bugRequest);
        ctrlExt_CreateUserRequestAdmin controller = new ctrlExt_CreateUserRequestAdmin(sc);
                        
        controller.convertToSR();
        
        System.assert( bugRequest.Request_Type__c == 'Generic Service');

        clsUtil.hasException = true;
        try{
            controller.convertToSR();
        }catch(Exception oEX){}

    }
    
    private static testmethod void changeRequestView(){
                                    
        Create_User_Request__c changeRequest = new Create_User_Request__c();
        changeRequest.Status__c = 'New';
        changeRequest.Application__c = 'Salesforce.com';
        changeRequest.Request_Type__c = 'Change Request';   
        changeRequest.Description_Long__c = 'Unit Test CR description';
        changeRequest.Priority__c = 'High';
        changeRequest.Business_Rationale__c = 'Business Rationale test';
        changeRequest.CR_Type__c = 'Application functionality Change';
        changeRequest.Time_Spent__c = 10;
                
        insert changeRequest;
                        
        Blob b = Blob.valueOf('Test Data');  
      
        Attachment attachment = new Attachment();  
        attachment.ParentId = changeRequest.Id;  
        attachment.Name = 'Test Attachment for Parent';  
        attachment.Body = b;  
          
        insert attachment; 
        
        changeRequest = [Select Id, Assignee__c, Name, CreatedDate, Description_Long__c, Priority__c, Business_Rationale__c, CR_Type__c, Status__c, Sub_Status__c, Request_Type__c, OwnerId, Application__c, Time_Spent__c, Internal_Notes__c from Create_User_Request__c where Id =:changeRequest.Id];
        
        Test.startTest();
        
        ApexPages.Standardcontroller sc = new ApexPAges.Standardcontroller(changeRequest);
        ctrlExt_CreateUserRequestAdmin controller = new ctrlExt_CreateUserRequestAdmin(sc);
                
        controller.createCR();

        clsUtil.hasException = true;

        try{
            controller.createCR();
        }catch(Exception oEX){}

    }
    
    //Need to seeAllData because of the custom settings for Data Load approvers
    @isTest(seeAllData = true)
    private static void dataLoadView(){
                                                    
        Create_User_Request__c dataLoadRequest = new Create_User_Request__c();
        dataLoadRequest.Status__c = 'New';
        dataLoadRequest.Application__c = 'Salesforce.com';
        dataLoadRequest.Request_Type__c = 'Data Load';
        dataLoadRequest.Data_Type__c = 'Account';       
        insert dataLoadRequest;
        
        Test.startTest();
        
        ApexPages.Standardcontroller sc = new ApexPAges.Standardcontroller(dataLoadRequest);
        ctrlExt_CreateUserRequestAdmin controller = new ctrlExt_CreateUserRequestAdmin(sc);
        
        controller.reject();        
        controller.completeRequest();
    }
  
    private static Map<Id, User> mapUser_Admin = new Map<Id, User>();
    private static void assignUserToAdminGroup(){
        
        List<GroupMember> memberships = [Select UserOrGroupId from GroupMember where Group.DeveloperName = 'Support_Portal_Admins' AND UserOrGroupId = :UserInfo.getUserId()];
        
        if(memberships.isEmpty()){
        
            Group adminGroup = [Select Id from Group where DeveloperName = 'Support_Portal_Admins'];
            
            GroupMember membership = new GroupMember();
            membership.GroupId = adminGroup.Id;
            membership.UserOrGroupId = UserInfo.getUserId();
            
            insert membership;
        }       

        mapUser_Admin = bl_CreateUserRequest.loadAdminUsers('Support_Portal_Admins');

    }

    private static Create_User_Request__c createNewUserRequest(String application, Boolean bGenerateBUInformation){
        
        if (bGenerateBUInformation) Test_Support_Requests.generateBUInformation();
        
        DIB_Country__c country = [Select Id, Name, Company__c, Company__r.Name from DIB_Country__c LIMIT 1];
        
        Create_User_Request__c userRequest = createUserRequest('Create User');
        userRequest.region__c = country.Company__c;
        userRequest.Geographical_Region_vs__c = country.Company__r.Name;
        userRequest.Geographical_SubRegion__c = 'NWE';
        userRequest.Geographical_Country_vs__c = country.Name;
        userRequest.Alias__c = 'alias';
        userRequest.Firstname__c = 'test';
        userRequest.Lastname__c = 'user';
        userRequest.Email__c = 'mock@medtronic.com';
        userRequest.Manager_Email__c = currentUser.Email;
        userRequest.Cost_Center__c = '123456';
        userRequest.Peoplesoft_ID__c = '123456';
        userRequest.Profile__c = 'Admin';
        userRequest.Role__c = 'System Administrator';
        userRequest.User_Business_Unit_vs__c = 'CRHF';
        userRequest.Company_Code__c = 'EUR';
        userRequest.Jobtitle_vs__c = 'Sales Rep';
        userRequest.Application__c = application;
        userRequest.Country__c = country.Id;
    userRequest.Mobile_Apps__c = 'Test';
        insert userRequest;
        
        if(application == 'Salesforce.com'){
            
            Sub_Business_Units__c sbu = [Select Id from Sub_Business_Units__c LIMIT 1]; 
            
            User_Request_BU__c userBU =  new User_Request_BU__c();
            userBU.Sub_Business_Unit__c = sbu.Id;
            userBU.Primary__c = true;
            userBU.User_Request__c = userRequest.Id;
            insert userBU;          
        }
        
        return userRequest;
    }
            
    private static Create_User_Request__c createUserRequest(String requestType){
        
        Create_User_Request__c userRequest = new Create_User_Request__c();          
        userRequest.Status__c = 'New';
        userRequest.Application__c = 'Salesforce.com';
        userRequest.Request_Type__c = requestType;          
        userRequest.Requestor_Email__c = currentUser.Email;     
        
        return userRequest;
    }
}