@isTest
private class Test_ctrl_Territory_Search {
	
	private static testmethod void searchTerritory(){
		
		Territory2 wecanTerritory = [Select Id, Territory_UID2__c from Territory2 where name='EUR (R)'];
		
		User contextUser = new User();
		contextUser.profileId = UserInfo.getProfileId();
		contextUser.Company_Code_Text__c = 'TST';
		contextUser.Alias_unique__c = 'unitT123';
		contextUser.Username = 'unit.test@medtronic.com.test';
		contextUser.LastName = 'test';
		contextUser.Email = 'unit.test@medtronic.com';
		contextUser.Alias = 'unitT123';
		contextUser.CommunityNickname = 'unitTestMDT';
		contextUser.TimeZoneSidKey = 'Europe/Amsterdam';
		contextUser.LocaleSidKey = 'en_US';
		contextUser.EmailEncodingKey = 'UTF-8';
		contextUser.LanguageLocaleKey = 'en_US';
		contextUser.Territory_UID__c = wecanTerritory.Territory_UID2__C;
		
		System.runAs(contextUser){
			
			ctrl_Territory_Search controller = new ctrl_Territory_Search();
			
			controller.SearchCriteria = 'Spain';
			controller.searchTerritory();
			
			System.assert(controller.searchResults.size() > 0);
			
			controller.clearResults();
			
			System.assert(controller.searchResults == null);
		}
		
	}

}