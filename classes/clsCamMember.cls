public class clsCamMember {
public list<CampaignMember> lstCM{get;set;}
public id Campaignid{get;set;}
public id CamMmbrRemid{get;set;}
public boolean propRemoveMembers{get; set;}

    public clsCamMember(ApexPages.StandardController controller){
    Campaignid=System.currentPageReference().getParameters().get('id');
    lstCM=new List<CampaignMember>();
    lstCM=[select id,status,Invitation_Callers_Assigned__c,Target_Account__c,Invitation_Caller_Formula__c ,Invitation_Caller__c,Invitation_Caller__r.name,CC_Status__c,Feedback_Caller__c,Feedback_Caller__r.name,Contact.Primary_Job_Title_vs__c,Contact.Contact_Primary_Specialty__c,Contact.name,Contact.FirstName,Contact.LastName,Target_Account__r.Account__r.Name,lastmodifiedDate,NPS_Value__c,Feedback_Caller_Formula__c  from CampaignMember where Campaignid=:Campaignid];
    }
	public pagereference EditCamMmbr(){
	id CamMmbrEditid=System.currentPageReference().getParameters().get('CamMmbrEditid');
	pagereference pr=new pagereference('/'+CamMmbrEditid+'/e?retURL=%2F'+Campaignid);
	return pr;
	}    
	public pagereference RemoveCamMmbr(){
		
	id CamMmbrRemid=System.currentPageReference().getParameters().get('CamMmbrRemid');
	system.debug('@@'+CamMmbrRemid);
	
	CampaignMember camMmbr=[select id from CampaignMember where id=:CamMmbrRemid];
	if(camMmbr!=null)
	{
		delete camMmbr;
	}
		
	propRemoveMembers = true;
	//pagereference pr=new pagereference('/'+Campaignid);
	return null;
	} 


}