@isTest(SeeAllData = true)// Needed for the UserTerritory assignments
private class Test_ba_AccountTeamMember {
	
	private static testmethod void test_createAccountTeamMembersBatch(){
		
		User systemAdmin = [Select Id from User where Alias = 'system'];
		
			System.runAs(systemAdmin){
							
			Map<Id, List<Id>> territoryUsersMap = new Map<Id, List<Id>>();
			Set<Id> userIds = new Set<Id>();
					
			for(UserTerritory2Association userTer : [Select UserId, Territory2Id from UserTerritory2Association where isActive = true AND Territory2Id IN (Select Id from Territory2 where Territory2Type.DeveloperName = 'Territory' AND Country_UID__c = 'DE' AND Therapy_Groups_Text__c INCLUDES ('Brain Modulation'))]){
				
				List<Id> territoryUsers = territoryUsersMap.get(userTer.Territory2Id);
				
				if(territoryUsers == null){
					
					territoryUsers = new List<Id>();
					territoryUsersMap.put(userTer.Territory2Id, territoryUsers);				
				}
				
				territoryUsers.add(userTer.userId);
				userIds.add(userTer.userId);
			}
			
			Map<Id, User> userMap = new Map<Id, User>([Select Id, Job_Title_vs__c from User where Id IN :userIds]);
			
			List<Id> territoryIds = new List<Id>(territoryUsersMap.keySet());
			
			List<Account> accounts = new List<Account>();
			
			for(Integer i = 0; i < territoryIds.size(); i++){
				
				Account acc = new Account();
				acc.Name = 'Unit Test Account ' + i;
				acc.SAP_Id__c = '0000' + i;
				
				accounts.add(acc);
			}
					
			insert accounts;
			
			List<ObjectTerritory2Association> accShares = new List<ObjectTerritory2Association>();
			
			for(Integer i = 0; i < territoryIds.size(); i++){
				
				Account acc = accounts[i];
				Id terrId = territoryIds[i];
				
				ObjectTerritory2Association accTer = new ObjectTerritory2Association();
				accTer.ObjectId = acc.Id;
				accTer.Territory2Id = terrId;
				accTer.AssociationCause ='Territory2Manual';
				
				accShares.add(accTer);
			}
			
			insert accShares;
			
			Therapy_Group__c pumps = [Select Id from Therapy_Group__c where Name = 'Brain Modulation' AND Company__r.Name = 'Europe'];
			
			Account_Team_Member__c toDeleteATM = new Account_Team_Member__c();
			toDeleteATM.Account__c = accounts[0].Id;
			toDeleteATM.User__c = UserInfo.getUserId();
			toDeleteATM.Therapy_Group__c = pumps.Id;
			toDeleteATM.Primary__c = true;
			
			Account_Team_Member__c duplicateATM = new Account_Team_Member__c();
			duplicateATM.Account__c = accounts[0].Id;
			duplicateATM.User__c = UserInfo.getUserId();
			duplicateATM.Therapy_Group__c = pumps.Id;
			duplicateATM.Primary__c = true;
			
			Account_Team_Member__c toUpdateATM = new Account_Team_Member__c();
			toUpdateATM.Account__c = accounts[0].Id;
			toUpdateATM.User__c = territoryUsersMap.get(territoryIds[0])[0];
			toUpdateATM.Therapy_Group__c = pumps.Id;
			toUpdateATM.Primary__c = true;
						
			Account_Team_Member__c untouchedATM = new Account_Team_Member__c();
			untouchedATM.Account__c = accounts[1].Id;
			untouchedATM.User__c = territoryUsersMap.get(territoryIds[1])[0];
			untouchedATM.Therapy_Group__c = pumps.Id;
			untouchedATM.Primary__c = true;
						
			insert new List<Account_Team_Member__c>{toDeleteATM, duplicateATM, toUpdateATM, untouchedATM};
			
			User toUpdateUser = userMap.get(territoryUsersMap.get(territoryIds[0])[0]);
			toUpdateUser.Job_Title_vs__c = 'Marketing';
			update toUpdateUser;
			
			Test.startTest();
			
			ba_AccountTeamMember batch = new ba_AccountTeamMember();
			batch.accounts = accounts;
			Database.executeBatch(batch);
			
			Test.stopTest();
			
			accounts = [Select Id, (Select Id from Account_Teams__r where Therapy_Group__r.Name = 'Brain Modulation') FROM Account where Id IN :accounts];
			
			for(Integer i = 0; i < accounts.size(); i++){
				
				List<Id> terUsers = territoryUsersMap.get(territoryIds[i]);
				
				System.assert(accounts[i].Account_Teams__r.size() == terUsers.size());
				
			}
		}
	}
		    
}