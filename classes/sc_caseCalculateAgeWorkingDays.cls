/*
* Schedulable class to launch the recalcualtion of Open Case ages in Working Days
*/
global class sc_caseCalculateAgeWorkingDays implements Schedulable{

   	global void execute(SchedulableContext SC) {
      
		ba_calculateCaseAgeInWorkingHours batch = new ba_calculateCaseAgeInWorkingHours();
		Database.executeBatch(batch);	 
   	}
}