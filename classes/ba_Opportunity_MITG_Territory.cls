global class ba_Opportunity_MITG_Territory implements Database.Batchable<SObject>, Database.Stateful {
	
	global Set<String> setID_Opportunity_OwnerIsNotActive = new Set<String>(); 
	global Set<String> setID_Opportunity_OwnerHasNoActiveTerritory = new Set<String>(); 
	global Set<String> setID_Opportunity_TerritoryDiffOwnerTerritory = new Set<String>(); 
	global Set<String> setID_Opportunity_AccountHasNoActiveTerritory = new Set<String>(); 

	global Set<String> setID_Opportunity_OwnerTerritoryEqualAccountTerritory = new Set<String>(); 
	global Set<String> setID_Opportunity_OwnerTerritoryNotEqualAccountTerritory = new Set<String>(); 

	global String tWhere = ' WHERE RecordType.DeveloperName like \'%MITG%\'';
	global String tAdditionalWhere = '';

	global Integer iRecordsProcessed = 0;
	global Boolean bIncludeDetailsInMail = true;

	//------------------------------------------------------------------------
	// BATCH Settings
	//------------------------------------------------------------------------
	global Database.QueryLocator start(Database.BatchableContext context) {

		String tSOQL = 'SELECT Id, OwnerId, Territory2Id, AccountId';
		tSOQL += ' FROM Opportunity';
		tSOQL += tWhere;
		tSOQL += tAdditionalWhere;
		tSOQL += ' ORDER BY OwnerId, Territory2Id, AccountId';
		if (Test.isRunningTest()){
			tSOQL += ' LIMIT 200';
		}

		return Database.getQueryLocator(tSOQL);

	}
	//------------------------------------------------------------------------

   	global void execute(Database.BatchableContext context, List<Opportunity> lstOpportunity) {

		iRecordsProcessed += lstOpportunity.size();

		// Collect all OwnerId's of the processing Opportunities
		Set<Id> setID_Owner = new Set<Id>();
		for (Opportunity oOpportunity : lstOpportunity){
			setID_Owner.add(oOpportunity.OwnerId);
		}

		// Get Active Users based on the collected Owners
		Map<Id, User> mapUser_Active = new Map<Id, User>([SELECT Id FROM User WHERE Id = :setID_Owner AND IsActive = TRUE]);

		// Get the Territories of the Owners
		List<UserTerritory2Association> lstUserTerritory = [SELECT UserId, Territory2Id FROM UserTerritory2Association WHERE IsActive = TRUE AND UserId = :setID_Owner ORDER BY UserId, Territory2Id];
		Map<Id, Set<Id>> mapUserId_TerritoryIds = new Map<Id, Set<Id>>();
		for (UserTerritory2Association oUserTerritory : lstUserTerritory){
			
			Set<Id> setID_Territory = new Set<Id>();
			if (mapUserId_TerritoryIds.containsKey(oUserTerritory.UserId)){
				setID_Territory = mapUserId_TerritoryIds.get(oUserTerritory.UserId);
			}
			setID_Territory.add(oUserTerritory.Territory2Id);
			mapUserId_TerritoryIds.put(oUserTerritory.UserId, setID_Territory);

		}

		// Verify if the Territory of the Opportunity is the same as the Territory of the Owner
		List<Opportunity> lstOpportunity_Processing = new List<Opportunity>();
		Set<Id> setID_Account = new Set<Id>();
		for (Opportunity oOpportunity : lstOpportunity){
		
			Id idOwner = oOpportunity.OwnerId;

			if (!mapUser_Active.containsKey(idOwner)){

				setID_Opportunity_OwnerIsNotActive.add(oOpportunity.Id);

			}else{

				if (mapUserId_TerritoryIds.containsKey(idOwner)){
				

					lstOpportunity_Processing.add(oOpportunity);
					setID_Account.add(oOpportunity.AccountId);

					Set<Id> setID_Territory_Owner = mapUserId_TerritoryIds.get(oOpportunity.OwnerId);
					if (!setID_Territory_Owner.contains(oOpportunity.Territory2Id)){
					
						// The Opportunity Owner has a Territory that is different from the Opportunity
						setID_Opportunity_TerritoryDiffOwnerTerritory.add(oOpportunity.Id);

					}
			
				}else{
			
					// Opportunity Owner doesn't have an Active Territory
					setID_Opportunity_OwnerHasNoActiveTerritory.add(oOpportunity.Id);
				}

			}

		}

		// Get Territories of the collected Accounts
		if (setID_Account.size() > 0){
	
			Map<Id, Set<Territory2>> mapAccountId_Territory = getActiveTerritoryForAccountId(setID_Account);

			for (Opportunity oOpportunity : lstOpportunity_Processing){

				// Get Territory IDs of Opportunity Owner			
				Set<Id> setID_Territory_Owner = new Set<Id>();
				if (mapUserId_TerritoryIds.containsKey(oOpportunity.OwnerId)){
					setID_Territory_Owner = mapUserId_TerritoryIds.get(oOpportunity.OwnerId);
				}

				// Get Territory IDs of Opportunity Account
				Set<Id> setID_Territory_Account = new Set<Id>();
				ID id_Account = oOpportunity.AccountId;
				if (mapAccountId_Territory.containsKey(id_Account)){

					Set<Territory2> setTerritory = mapAccountId_Territory.get(id_Account);

					for (Territory2 oTerritory : setTerritory){
						setID_Territory_Account.add(oTerritory.Id);
					}

				}else{
					


				}

				if ( (setID_Territory_Account.size() > 0) && (setID_Territory_Owner.size() > 0) ){
				
					Boolean bTerritoryFound = false;
					for (Id idTerritory_User : setID_Territory_Owner){
						
						if (setID_Territory_Account.contains(idTerritory_User)){
							bTerritoryFound = true;
							break;

						}
					}

					if (bTerritoryFound){
						
						setID_Opportunity_OwnerTerritoryEqualAccountTerritory.add(oOpportunity.Id);
					
					}else{
					
						setID_Opportunity_OwnerTerritoryNotEqualAccountTerritory.add(oOpportunity.Id);

					}
				
				}else{

					if (setID_Territory_Account.size() == 0){
						setID_Opportunity_AccountHasNoActiveTerritory.add(oOpportunity.Id);
					}
					if (setID_Territory_Owner.size() == 0){
						setID_Opportunity_OwnerHasNoActiveTerritory.add(oOpportunity.Id);
					}
				
				}
			
			}

		}
	
	}
	

	global void finish(Database.BatchableContext context) {
		
		List<String> lstID_Opportunity_OwnerIsNotActive = new List<String>();
			lstID_Opportunity_OwnerIsNotActive.addAll(setID_Opportunity_OwnerIsNotActive);
		List<String> lstID_Opportunity_OwnerHasNoActiveTerritory = new List<String>();
			lstID_Opportunity_OwnerHasNoActiveTerritory.addAll(setID_Opportunity_OwnerHasNoActiveTerritory);
		List<String> lstID_Opportunity_TerritoryDiffOwnerTerritory = new List<String>();
			lstID_Opportunity_TerritoryDiffOwnerTerritory.addAll(setID_Opportunity_TerritoryDiffOwnerTerritory);
		List<String> lstID_Opportunity_AccountHasNoActiveTerritory = new List<String>();
			lstID_Opportunity_AccountHasNoActiveTerritory.addAll(setID_Opportunity_AccountHasNoActiveTerritory);

		List<String> lstID_Opportunity_OwnerTerritoryEqualAccountTerritory = new List<String>();
			lstID_Opportunity_OwnerTerritoryEqualAccountTerritory.addAll(setID_Opportunity_OwnerTerritoryEqualAccountTerritory);
		List<String> lstID_Opportunity_OwnerTerritoryNotEqualAccountTerritory = new List<String>();
			lstID_Opportunity_OwnerTerritoryNotEqualAccountTerritory.addAll(setID_Opportunity_OwnerTerritoryNotEqualAccountTerritory);
		

		String tResult = '<BR />';

			tResult += '<HR />';
			tResult += ' OPPORTUNITY RECORDS PROCESSED  (' + iRecordsProcessed + ')';
			tResult += '<BR />';
			tResult += ' OPPORTUNITY OWNER TERRITORY <> ACCOUNT TERRITORY (VALIDATION NEEDED)  (' + lstID_Opportunity_OwnerTerritoryNotEqualAccountTerritory.size() + ')';
			tResult += '<BR />';
			tResult += ' OPPORTUNITY OWNER TERRITORY = ACCOUNT TERRITORY (OK) (' + lstID_Opportunity_OwnerTerritoryEqualAccountTerritory.size() + ')';
			tResult += '<BR />';
			tResult += ' OPPORTUNITY OWNER IS NOT ACTIVE (' + lstID_Opportunity_OwnerIsNotActive.size() + ')';
			tResult += '<BR />';
			tResult += ' OPPORTUNITY OWNER - NO ACTIVE TERRITORY (' + lstID_Opportunity_OwnerHasNoActiveTerritory.size() + ')';
			tResult += '<BR />';
			tResult += ' OPPORTUNITY TERRITORY <> OPPORTUNITY OWNER TERRITORY (' + lstID_Opportunity_TerritoryDiffOwnerTerritory.size() + ')';
			tResult += '<BR />';
			tResult += ' ACCOUNT HAS NO ACTIVE TERRITORY (' + lstID_Opportunity_AccountHasNoActiveTerritory.size() + ')';
			tResult += '<BR />';
			tResult += '<HR />';

			if (bIncludeDetailsInMail){

				tResult += '<BR />';
				tResult += '<BR />';

				tResult += '<HR />';
				tResult += ' OPPORTUNITY RECORDS PROCESSED  (' + iRecordsProcessed + ')';
				tResult += '<HR />';
				tResult += '<BR />';
				tResult += '<BR />';

				tResult += '<HR />';
				tResult += ' OPPORTUNITY OWNER TERRITORY <> ACCOUNT TERRITORY (VALIDATION NEEDED)  (' + lstID_Opportunity_OwnerTerritoryNotEqualAccountTerritory.size() + ')';
				tResult += '<HR />';
				tResult += '<BR />';
				tResult += '<BR />';
				tResult += String.join(lstID_Opportunity_OwnerTerritoryNotEqualAccountTerritory, '<BR />');
				tResult += '<BR />';
		
				tResult += '<HR />';
				tResult += ' OPPORTUNITY OWNER TERRITORY = ACCOUNT TERRITORY (OK) (' + lstID_Opportunity_OwnerTerritoryEqualAccountTerritory.size() + ')';
				tResult += '<HR />';
				tResult += '<BR />';
				tResult += '<BR />';
				tResult += String.join(lstID_Opportunity_OwnerTerritoryEqualAccountTerritory, '<BR />');
				tResult += '<BR />';

				tResult += '<HR />';
				tResult += ' OPPORTUNITY OWNER IS NOT ACTIVE (' + lstID_Opportunity_OwnerIsNotActive.size() + ')';
				tResult += '<HR />';
				tResult += '<BR />';
				tResult += '<BR />';
				tResult += String.join(lstID_Opportunity_OwnerIsNotActive, '<BR />');
				tResult += '<BR />';

				tResult += '<HR />';
				tResult += ' OPPORTUNITY OWNER - NO ACTIVE TERRITORY (' + lstID_Opportunity_OwnerHasNoActiveTerritory.size() + ')';
				tResult += '<HR />';
				tResult += '<BR />';
				tResult += '<BR />';
				tResult += String.join(lstID_Opportunity_OwnerHasNoActiveTerritory, '<BR />');
				tResult += '<BR />';

				tResult += '<HR />';
				tResult += ' OPPORTUNITY TERRITORY <> OPPORTUNITY OWNER TERRITORY (' + lstID_Opportunity_TerritoryDiffOwnerTerritory.size() + ')';
				tResult += '<HR />';
				tResult += '<BR />';
				tResult += '<BR />';
				tResult += String.join(lstID_Opportunity_TerritoryDiffOwnerTerritory, '<BR />');
				tResult += '<BR />';

				tResult += '<HR />';
				tResult += ' ACCOUNT HAS NO ACTIVE TERRITORY (' + lstID_Opportunity_AccountHasNoActiveTerritory.size() + ')';
				tResult += '<HR />';
				tResult += '<BR />';
				tResult += '<BR />';
				tResult += String.join(lstID_Opportunity_AccountHasNoActiveTerritory, '<BR />');
				tResult += '<BR />';
			}

		String tEmailBody = tResult;
		Messaging.Singleemailmessage oEmail = new Messaging.Singleemailmessage();
			oEmail.setHTMLBody(tEmailBody);
			oEmail.setTargetObjectId(UserInfo.getUserId());
			oEmail.setSubject('ba_Opportunity_MITG_Territory');
			oEmail.setSaveAsActivity(false);
		Messaging.sendEmail(new Messaging.Singleemailmessage[] { oEmail});

		System.debug('** ba_Opportunity_MITG_Territory - OPPORTUNITY OWNER TERRITORY <> ACCOUNT TERRITORY (VALIDATION NEEDED)  (' + lstID_Opportunity_OwnerTerritoryNotEqualAccountTerritory.size() + ')');
		System.debug('** ba_Opportunity_MITG_Territory - OPPORTUNITY OWNER TERRITORY = ACCOUNT TERRITORY (OK) (' + lstID_Opportunity_OwnerTerritoryEqualAccountTerritory.size() + ')');
		System.debug('** ba_Opportunity_MITG_Territory - OPPORTUNITY OWNER IS NOT ACTIVE (' + lstID_Opportunity_OwnerIsNotActive.size() + ')');
		System.debug('** ba_Opportunity_MITG_Territory - OPPORTUNITY OWNER - NO ACTIVE TERRITORY (' + lstID_Opportunity_OwnerHasNoActiveTerritory.size() + ')');
		System.debug('** ba_Opportunity_MITG_Territory - OPPORTUNITY TERRITORY <> OPPORTUNITY OWNER TERRITORY (' + lstID_Opportunity_TerritoryDiffOwnerTerritory.size() + ')');
		System.debug('** ba_Opportunity_MITG_Territory - ACCOUNT HAS NO ACTIVE TERRITORY (' + lstID_Opportunity_AccountHasNoActiveTerritory.size() + ')');

		
	}


	global Map<Id, Set<Territory2>> getActiveTerritoryForAccountId(Set<Id> setID_Account){

		Map<Id, Set<Territory2>> mapAccountID_ActiveTerritory = new Map<Id, Set<Territory2>>();

		// Get the Territories for the prcoessing Accounts
		List<ObjectTerritory2Association> lstAcccountShare = 
			[
				SELECT 
					ObjectId, Territory2Id, Territory2.Id, Territory2.Therapy_Groups_Text__c 
				FROM 
					ObjectTerritory2Association 
				WHERE 
					ObjectId = :setID_Account
			];

		Map<Id, Set<Territory2>> mapAccountID_TerritoryID = new Map<Id, Set<Territory2>>();
		Set<Id> setID_Territory_All = new Set<Id>();
		for (ObjectTerritory2Association oAccountShare : lstAcccountShare){

			Set<Territory2> setID_Territory = new Set<Territory2>();
			if (mapAccountID_TerritoryID.containsKey(oAccountShare.ObjectId)){
				setID_Territory = mapAccountID_TerritoryID.get(oAccountShare.ObjectId);
			}
			setID_Territory.add(oAccountShare.Territory2);
			mapAccountID_TerritoryID.put(oAccountShare.ObjectId, setID_Territory);

			setID_Territory_All.add(oAccountShare.Territory2Id);
		}

		List<UserTerritory2Association> lstUserTerritory = 
			[
				SELECT 
					UserId, Territory2Id
				FROM 
					UserTerritory2Association
				WHERE 
					Territory2Id = :setID_Territory_All
					AND IsActive = true
					AND User.isActive = true
			];
		
		Set<Id> setID_Territory_Active = new Set<Id>();
		for (UserTerritory2Association oUserTerritory : lstUserTerritory){
			
			setID_Territory_Active.add(oUserTerritory.Territory2Id);
		}

		for (Id id_Account : mapAccountID_TerritoryID.keySet()){
			
			Set<Territory2> setTerritory = new Set<Territory2>();
			
			for (Territory2 accTerritory : mapAccountID_TerritoryID.get(id_Account)){
				
				if (setID_Territory_Active.contains(accTerritory.Id)){
					setTerritory.add(accTerritory);		
				}
			}

			// This Map contains all Active Territories of the Account
			mapAccountID_ActiveTerritory.put(id_Account, setTerritory);

		}

		return mapAccountID_ActiveTerritory;

	}
}