/*
 *      Created Date 	: 17-02-2014
 *      Author 			: Bart Caelen
 *      Description 	: TEST Class for the APEX Class bl_Chatter
 */
@isTest private class TEST_bl_Chatter {

    @isTest static void test_bl_Chatter() {

		map<id, set<id>> mapRecordIdWithUserIds = new map<id, set<id>>();
		set<string> setFieldsToMonitor = new set<string>();
		//------------------------------------------------
		// CREATE TEST DATA
		//------------------------------------------------
        Account oAccount = new Account();
        	oAccount.Name 				= 'TEST Account'; 
        	oAccount.Account_Country_vs__c = 'BELGIUM';
        insert oAccount;               
        Account oAccount_Old = new Account();
        	oAccount_Old.Name 				= 'TEST Account Old'; 
        	oAccount_Old.Account_Country_vs__c = 'BELGIUM';
        insert oAccount_Old;        
        
        map<id, Account> mapAccount_Old = new map<id, Account>();
        map<id, Account> mapAccount_New = new map<id, Account>();
       	mapAccount_New.put(oAccount.id, oAccount);        
       	mapAccount_Old.put(oAccount.id, oAccount_Old);
       	setFieldsToMonitor.add('Name');        
		//------------------------------------------------

		//------------------------------------------------
		// PREPARE TEST
		//------------------------------------------------
		set<id> setID = new set<id>();
		setID.add(Userinfo.getUserID());
		mapRecordIdWithUserIds.put(oAccount.Id, setID);
		//------------------------------------------------


		//------------------------------------------------
		// PERFORM TEST
		//------------------------------------------------
		list<FeedItem> lstFeedItem = [SELECT ID FROM FeedItem WHERE ParentID = :oAccount.Id OR ParentID = :oAccount_old.Id];
		integer iRecCount = lstFeedItem.size();
		system.assertEquals(lstFeedItem.size(), 0);
		
		
		test.startTest();

		bl_Chatter.CreateFollower(mapRecordIdWithUserIds);
		lstFeedItem = [SELECT ID FROM FeedItem WHERE ParentID = :oAccount.Id];

		bl_Chatter.createChatterFeed(mapAccount_Old, mapAccount_New, 'INSERT', 'id', setFieldsToMonitor, 'Name');
		bl_Chatter.createChatterFeed(mapAccount_Old, mapAccount_New, 'UPDATE', 'id', setFieldsToMonitor, 'Name');
		bl_Chatter.createChatterFeed(mapAccount_Old, mapAccount_New, 'DELETE', 'id', setFieldsToMonitor, 'Name');
		bl_Chatter.createChatterFeed(mapAccount_Old, mapAccount_New, 'UNDELETE', 'id', setFieldsToMonitor, 'Name');

		test.stopTest();

		lstFeedItem = [SELECT ID FROM FeedItem WHERE ParentID = :oAccount.Id OR ParentID = :oAccount_old.Id];
		system.assertEquals(lstFeedItem.size(), 4);
		//------------------------------------------------

    }

    @isTest static void test_createFeedItemInput() {

		//------------------------------------------------
		// Create Test Data
		//------------------------------------------------
		clsTestData_Account.createAccount();
		List<EntitySubscription> lstEntitySubscription_Existing = [SELECT ParentId, SubscriberId FROM EntitySubscription WHERE ParentId = :clsTestData_Account.oMain_Account.Id ORDER BY ParentId];
		System.assertEquals(lstEntitySubscription_Existing.size(), 0);
		//------------------------------------------------


		//------------------------------------------------
		// Test Logic
		//------------------------------------------------
		Test.startTest();

		ConnectApi.FeedItemInput oFeedItemInput = bl_Chatter.createFeedItemInput(clsTestData_Account.oMain_Account.Id, UserInfo.getUserId(), 'TEST Chatter Post');

		Test.stopTest();
		//------------------------------------------------


		//------------------------------------------------
		// Validate Result
		//------------------------------------------------
		//------------------------------------------------


    }

}