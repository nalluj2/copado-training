@isTest private class TEST_ctrlExt_ContactAffiliationView {
	
	@isTest static void createTestData() {

		//-----------------------------------------------------
		// CREATE TEST DATA
		//-----------------------------------------------------
		String tCountry = 'BELGIUM';

		clsTestData_MasterData.createCompany();
		clsTestData_MasterData.createCompanyRelatedSetup_Department();

    	clsTestData_MasterData.tCountryCode = 'BE';
    	clsTestData_MasterData.createCountry();

		clsTestData_MasterData.createSubBusinessUnit();

		clsTestData_Affiliation.tAccountTypeCountry_ISOCode = 'BE';
		clsTestData_Affiliation.createAccountTypeCountry();

		clsTestData_Account.iRecord_Account = 10;
		clsTestData_Account.tCountry_Account = tCountry;
		clsTestData_Account.createAccount();

		clsTestData_Contact.iRecord_Contact = 11;
		clsTestData_Contact.createContact();


		clsTestData_Affiliation.iRecord_Affiliation_C2A = 10;
		clsTestData_Affiliation.createAffiliation_C2A();

		clsTestData_Affiliation.iRecord_Affiliation_C2C = 10;
		clsTestData_Affiliation.createAffiliation_C2C();
		//-----------------------------------------------------

	}
	
	@isTest static void test_ctrlExt_ContactAffiliationView() {


		String tTest = '';
		Integer iTest = 0;
		Id idTest;
		Date dTest;
		Boolean bTest = false;
		List<SelectOption> lstSO_Test = new List<SelectOption>();
		List<SObject> lstSObject_Test = new List<SObject>();
		List<String> lstString_Test = new List<String>();


		// CREATE TEST DATA
		createTestData();

		// TESTING
		Test.startTest();

        ApexPages.StandardController oCTRL = new ApexPages.StandardController(clsTestData_Contact.oMain_Contact);
        ctrlExt_ContactAffiliationView oCTRLEXT = new ctrlExt_ContactAffiliationView(oCTRL);

        oCTRLEXT.initialize();


        // TEST GETTERS & SETTERS - GENERAL
		idTest = oCTRLEXT.id_AssignedAffiliation;
		lstSO_Test = oCTRLEXT.lstPageSize;

		oCTRLEXT.bSaveFilter = false;
		bTest = oCTRLEXT.bSaveFilter;
		oCTRLEXT.bSaveFilter = true;
		bTest = oCTRLEXT.bSaveFilter;
		
        oCTRLEXT.bShowFilter = false;
        bTest = oCTRLEXT.bShowFilter;

        
        // TEST GETTERS & SETTERS - C2A
        oCTRLEXT.tSortField_C2A = '';
        tTest = oCTRLEXT.tSortOrder_C2A;
        tTest = oCTRLEXT.tPrevSortField_C2A;
        tTest = oCTRLEXT.tSortOrder_C2A;

        oCTRLEXT.tSortField_C2A = 'Affiliation_From_Contact__r.lastname';
        oCTRLEXT.sortData_C2A();
        tTest = oCTRLEXT.tSortOrder_C2A;
        oCTRLEXT.sortData_C2A();
        tTest = oCTRLEXT.tSortOrder_C2A;
        oCTRLEXT.sortData_C2A();
        tTest = oCTRLEXT.tSortOrder_C2A;

        tTest = oCTRLEXT.tWarning_C2A;

        lstSObject_Test = oCTRLEXT.lstRecord_C2A;
        bTest = oCTRLEXT.hasNext_C2A;
        bTest = oCTRLEXT.hasPrevious_C2A;
        iTest = oCTRLEXT.pageNumber_C2A;
        iTest = oCTRLEXT.totalPages_C2A;
        iTest = oCTRLEXT.pageSize_C2A;
        iTest = oCTRLEXT.resultSize_C2A;

        oCTRLEXT.pageSize_C2A = 1;
        oCTRLEXT.pageNumber_C2A = 1;

        oCTRLEXT.first_C2A();
        oCTRLEXT.next_C2A();
        oCTRLEXT.last_C2A();
        oCTRLEXT.previous_C2A();


        // TEST GETTERS & SETTERS - C2C
        oCTRLEXT.tSortField_C2C = '';
        tTest = oCTRLEXT.tSortOrder_C2C;
        tTest = oCTRLEXT.tPrevSortField_C2C;
        tTest = oCTRLEXT.tSortOrder_C2C;

        oCTRLEXT.tSortField_C2C = 'Business_Unit__c';
        oCTRLEXT.sortData_C2C();
        tTest = oCTRLEXT.tSortOrder_C2C;
        oCTRLEXT.sortData_C2C();
        tTest = oCTRLEXT.tSortOrder_C2C;
        oCTRLEXT.sortData_C2C();
        tTest = oCTRLEXT.tSortOrder_C2C;

        tTest = oCTRLEXT.tWarning_C2C;

        lstSObject_Test = oCTRLEXT.lstRecord_C2C;
        bTest = oCTRLEXT.hasNext_C2C;
        bTest = oCTRLEXT.hasPrevious_C2C;
        iTest = oCTRLEXT.pageNumber_C2C;
        iTest = oCTRLEXT.totalPages_C2C;
        iTest = oCTRLEXT.pageSize_C2C;
        iTest = oCTRLEXT.resultSize_C2C;

        oCTRLEXT.pageSize_C2C = 1;
        oCTRLEXT.pageNumber_C2C = 1;

        oCTRLEXT.first_C2C();
        oCTRLEXT.next_C2C();
        oCTRLEXT.last_C2C();
        oCTRLEXT.previous_C2C();  


        // TEST ACTION - C2A
        oCTRLEXT.id_AssignedAffiliation = clsTestData_Affiliation.lstAffiliation_C2A[1].Id;
        oCTRLEXT.viewAffiliation_C2A();

        oCTRLEXT.createAffiliation_C2A();

        oCTRLEXT.id_AssignedAffiliation = clsTestData_Affiliation.lstAffiliation_C2A[0].Id;	// Primary Affiliation
		oCTRLEXT.editAffiliation_C2A();
        oCTRLEXT.id_AssignedAffiliation = clsTestData_Affiliation.lstAffiliation_C2A[1].Id;
		oCTRLEXT.editAffiliation_C2A();

        oCTRLEXT.id_AssignedAffiliation = clsTestData_Affiliation.lstAffiliation_C2A[0].Id;	// Primary Affiliation
		oCTRLEXT.deleteAffiliation_C2A();
        
        clsUtil.hasException = true;
        oCTRLEXT.id_AssignedAffiliation = clsTestData_Affiliation.lstAffiliation_C2A[1].Id;
		try{ oCTRLEXT.deleteAffiliation_C2A(); }catch(Exception oEX){}
        clsUtil.hasException = false;
		oCTRLEXT.deleteAffiliation_C2A();



        // TEST ACTION - C2C
        oCTRLEXT.id_AssignedAffiliation = clsTestData_Affiliation.lstAffiliation_C2C[1].Id;
        oCTRLEXT.viewAffiliation_C2C();

        oCTRLEXT.createAffiliation_C2C();

        oCTRLEXT.id_AssignedAffiliation = clsTestData_Affiliation.lstAffiliation_C2C[0].Id;	// Primary Affiliation
		oCTRLEXT.editAffiliation_C2C();
        oCTRLEXT.id_AssignedAffiliation = clsTestData_Affiliation.lstAffiliation_C2C[1].Id;
		oCTRLEXT.editAffiliation_C2C();

        oCTRLEXT.id_AssignedAffiliation = clsTestData_Affiliation.lstAffiliation_C2C[0].Id;	// Primary Affiliation
		oCTRLEXT.deleteAffiliation_C2C();
        
        clsUtil.hasException = true;
        oCTRLEXT.id_AssignedAffiliation = clsTestData_Affiliation.lstAffiliation_C2C[1].Id;
		try{ oCTRLEXT.deleteAffiliation_C2C(); }catch(Exception oEX){}
        clsUtil.hasException = false;
		oCTRLEXT.deleteAffiliation_C2C();

		Test.stopTest();


	}
	
}