public with sharing class ControllerNoBusinessUnitPage {

    public ControllerNoBusinessUnitPage()
    {
            
    }

    
    public User_Business_Unit__c userBUs;
    public String Company {get;set;} 
    public string retURL='';
    
    public ControllerNoBusinessUnitPage(ApexPages.StandardController controller)
    {
        userBUs = (User_Business_Unit__c)controller.getRecord();
    }
	
	map<id,string> AllCompanies = new map<id,string>(); 
    public List<SelectOption> getCompanies()
    {
        List<SelectOption> options = new List<SelectOption>();
        AllCompanies = SharedMethods.getCompanies();
        options.add(new SelectOption('','----Select----'));     
        if (AllCompanies.size()>0){
            set<id> setCompanyIds=AllCompanies.keySet();
            for(Id CompanyId:setCompanyIds){
                options.add(new SelectOption(CompanyId,AllCompanies.get(CompanyId)));               
            }
        }
        return options;
    }
    
    public String[] BUSBU {get;set;} 
    public List<SelectOption> getBUSBUList()
    {
        List<SelectOption> options = new List<SelectOption>();
        if(Company==null){
            userBUs.addError('Please select a Company.') ;          
            //return null;            
        } else {
            List<Business_Unit__c> lstBUs = [Select id From Business_Unit__c where Company__c=:Company order by name];
            if(lstBUs.size()>0){
                List<Sub_Business_Units__c> lstSBUs = [Select id,Business_Unit__r.Name,Name From Sub_Business_Units__c where Business_Unit__c in:lstBUs order by Business_Unit__r.Name,Name];
                if(lstSBUs.size()>0){
                    for(Sub_Business_Units__c SBU:lstSBUs){
                        string BUSBUName = SBU.Business_Unit__r.Name + ' - ' + SBU.Name; 
                        options.add(new SelectOption(SBU.Name,BUSBUName));                                        
                    }
                }
                                
            }
        }
        return options;        
    }

    public pageReference Submit(){
    	boolean isError=false;
    	if(Company==null || Company==''){
    		userBUs.addError('Please select Region.') ;
    		isError=true;
    	}
    	System.Debug('BUSBU Size - ' + BUSBU.size() + 'BUSBU - ' + BUSBU);
    	if(BUSBU==null || BUSBU.size()==0){
    		userBUs.addError('Please select BU - SBU.') ;
    		isError=true;
    	}
    	if(isError){
    		return null;    		    		
    	}
    	// Start: Email Functionaility Here
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		String[] toAddresses = new String[] {'rs.mite-euhelp@medtronic.com'}; 
		//String[] toAddresses = new String[] {'dheeraj.g2005@gmail.com'};
		mail.setToAddresses(toAddresses);
		string strCompanyName='';
		if(AllCompanies!=null && AllCompanies.size()>0){
			strCompanyName = AllCompanies.get(Company);
		}
		mail.setSubject('SFDC '+ strCompanyName +': User BU Change Request');
		mail.setBccSender(false);
		mail.setUseSignature(false);
		string strUserName = UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
		string htmlBody='';
		htmlBody = htmlBody + 'Dear SFDC Support Team, <br /><br />';
		htmlBody = htmlBody + 'This is an automatic email message generated from SFDC. <br /><br />';
		htmlBody = htmlBody + 'User ' + strUserName + ' requested a change in his BU setup. <br /><br />';
		htmlBody = htmlBody + 'SBU : '+ BUSBU +' <br /><br />';
        mail.setHtmlBody(htmlBody);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    	// End:  Email Functionaility Here
    	PageReference pageRef = new PageReference('/home/home.jsp');
        pageRef.setRedirect(true);
        return pageRef;            	
    }

    public pageReference cancelButton(){
    	PageReference pageRef = new PageReference('/home/home.jsp');
        pageRef.setRedirect(true);
        return pageRef;            	
    }
    
    
}