/*
* This class contains methods to work with the Territory Hierarchy
* Author: Jesus Lozano
*/
public with sharing class bl_Territory_Hierarchy{
	
	private static Map<Id, Territory2> territoriesById;
	private static Map<Id, List<Territory2>> territoriesByParentId;
	
	private static void initializeData(){
		
		territoriesById = new Map<Id, Territory2>([Select Id, Territory2Type.DeveloperName, ParentTerritory2Id from Territory2]);
		
		territoriesByParentId = new Map<Id, List<Territory2>>();
		
		for(Territory2 ter : territoriesById.values()){
			
			List<Territory2> children = territoriesByParentId.get(ter.ParentTerritory2Id);
			
			if(children == null){
				
				children = new List<Territory2>();
				territoriesByParentId.put(ter.ParentTerritory2Id, children);
			}
			
			children.add(ter);
		}		
	}
	
	private static Territory2 getTerritory(Id terId){
		
		if(territoriesById == null){
			initializeData();
		}
		
		return territoriesById.get(terId);
	}
	
	private static List<Territory2> getTerritoryChildren(Id terId){
		
		if(territoriesById == null){
			initializeData();
		}
		
		return territoriesByParentId.get(terId);	
	}
	
	//Get the Countries for the given Territory. If the Territory is in Country level or below it will return only 1 value.
	//Otherwise it may return multiple values.
	public static Set<Id> getTerritoryCountries(Set<Id> terIds){
		
		Set<Id> result = new Set<Id>();
		
		for(Id terId : terIds){
		
			Territory2 ter = getTerritory(terId);
					
			//If Territory is above or at Country level we search for all its descendants Countries	
			if(new Set<String>{'Business Unit Region', 'Region', 'Subregion', 'Country Group', 'Country'}.contains(ter.Territory2Type.DeveloperName)){
				
				Set<Id> terCountries = getDescendantsOfType(ter.Id, 'Country');
				
				if(terCountries.size()>0) result.addAll(terCountries);
			
			//If Territory is below Country level we return the ancestor Country	
			}else{
				
				Id parentId = ter.ParentTerritory2Id;
				
				while(parentId != null){
					
					Territory2 parentTer = getTerritory(parentId);
					if(parentTer.Territory2Type.DeveloperName == 'Country'){
						result.add(parentTer.Id);
						parentId = null;
					}else{
						parentId = parentTer.ParentTerritory2Id;
					}	
				}
			}
		}
		
		return result;
	}
	
	//Get the Districts for the given Territory. If the Territory is in District level or below it will return only 1 value.
	//Otherwise it may return multiple values.
	public static Set<Id> getTerritoryDistricts(Set<Id> terIds){
		
		Set<Id> result = new Set<Id>();
		
		for(Id terId : terIds){
			
			Territory2 ter = getTerritory(terId);
					
			//If Territory is below District level we return the ancestor District
			if(ter.Territory2Type.DeveloperName == 'Territory'){
				
				Id parentId = ter.ParentTerritory2Id;
				
				while(parentId != null){
					
					Territory2 parentTer = getTerritory(parentId);
					if(parentTer.Territory2Type.DeveloperName == 'District'){
						result.add(parentTer.Id);
						parentId = null;
					}else{
						parentId = parentTer.ParentTerritory2Id;
					}	
				}
					
			//If Territory is above or at District level we search for all its descendants Districts		
			}else{
				
				Set<Id> terDistricts = getDescendantsOfType(ter.Id, 'District');
				
				if(terDistricts.size()>0) result.addAll(terDistricts);			
			}
		}
		
		return result;
	}
	
	//Get the Territories for the given Territory. If the Territory is in Territory level it will return only 1 value.
	//Otherwise it may return multiple values.
	public static Set<Id> getTerritoryTerritories(Set<Id> terIds){
		
		Set<Id> result = new Set<Id>();
				 
		for(Id terId : terIds){				 
			
			Territory2 ter = getTerritory(terId);
					
		 	Set<Id> terTerritories = getDescendantsOfType(ter.Id, 'Territory');
		 	
		 	if(terTerritories.size()>0) result.addAll(terTerritories);
		}
		
		return result;
	}
	
	//Returns all the descendants of a Territory of a given Type. It returns itself if the given type matches his own type
	public static Set<Id> getDescendantsOfType(Id terId, String terType){
		
		Set<Id> result = new Set<Id>();
				
		Territory2 ter = getTerritory(terId);
		
		if(ter.Territory2Type.DeveloperName == terType){
			
			result.add(ter.Id);
		}else{
			List<Territory2> children = getTerritoryChildren(terId);
			
			if(children!=null){
				
				for(Territory2 child : children){
					
					Set<Id> childrenOfType = getDescendantsOfType(child.Id, terType);
					if(childrenOfType.size()>0) result.addAll(childrenOfType);	
				}			
			}
		}
		
		return result;
	}	
	
	public static Set<Id> getTerritoryDescendants(Id terId){
		
		Set<Id> result = new Set<Id>();
				
		List<Territory2> children = getTerritoryChildren(terId);
		
		if(children!=null){
			
			for(Territory2 child : children){
				
				result.add(child.Id);
				Set<Id> childrenIds = getTerritoryDescendants(child.Id);
				if(childrenIds.size()>0) result.addAll(childrenIds);	
			}			
		}		
		
		return result;
	}
	
}