@RestResource(urlMapping='/SETCaseService/*')
global with sharing class ws_SETCaseService {
	
	@HttpPost
	global static String doPost() {
 		
 		RestRequest req = RestContext.request;
			
		System.debug('post called '+ req);
		System.debug('post requestBody '+ req.requestBody.toString());
			
		String body = req.requestBody.toString();
		SETCaseRequest SETCaseRequest = (SETCaseRequest) System.Json.deserialize(body, SETCaseRequest.class);
			
		System.debug('post requestBody ' + SETCaseRequest);
			
		Cost_Analysis_Case__c SETCase = SETCaseRequest.SETCase;

		SETCaseResponse resp = new SETCaseResponse();

		//We catch all exception to assure that 
		try{
				
			resp.SETCase = bl_SETCase.saveSETCase(SETCase);
			resp.id = SETCaseRequest.id;
	
			resp.success = true;
			
		}catch (Exception e){
			
			resp.message = e.getMessage();
			resp.success = false;
		}
 
		bl_Mobilesync.storeRequest(UserInfo.getUserId(), 'SETCase', req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
  			
		return System.JSON.serialize(resp);		
	}

	@HttpDelete
	global static String doDelete(){
		RestRequest req = RestContext.request;
			
		System.debug('post called ' + req);
		System.debug('post requestBody ' + req.requestBody.toString());
			
		String body = req.requestBody.toString();
		SETCaseDeleteRequest SETCaseDeleteRequest = (SETCaseDeleteRequest) System.Json.deserialize(body, SETCaseDeleteRequest.class);
			
		System.debug('post requestBody ' + SETCaseDeleteRequest);
			
		Cost_Analysis_Case__c SETCase = SETCaseDeleteRequest.SETCase;
		
		SETCaseDeleteResponse resp = new SETCaseDeleteResponse();
		
		List<Cost_Analysis_Case__c> SETCasesToDelete = [select id, mobile_id__c from Cost_Analysis_Case__c where Mobile_ID__c = :SETCase.Mobile_ID__c];
		
		try{
			
			if (SETCasesToDelete != null && SETCasesToDelete.size() > 0){	
				
				Cost_Analysis_Case__c SETCaseToDelete = SETCasesToDelete.get(0);
				bl_SETCase.deleteSETCase(SETCaseToDelete);
				resp.SETCase = SETCaseToDelete;
				resp.id = SETCaseDeleteRequest.id;
				resp.success = true;
				
			}else{
				
				resp.SETCase = SETCase;
				resp.id = SETCaseDeleteRequest.id;
				resp.message='SETCase not found';
				resp.success = true;
			}
			
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
		}
			
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'SETCaseDelete', req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
			
		return System.JSON.serialize(resp);
	}

	global class SETCaseRequest{
		public Cost_Analysis_Case__c SETCase {get;set;}
		public String id{get;set;}
	}
	
	global class SETCaseDeleteRequest{
		public Cost_Analysis_Case__c SETCase {get;set;}
		public String id{get;set;}
	}
		
	global class SETCaseDeleteResponse{
		public Cost_Analysis_Case__c SETCase {get;set;}
		public String id{get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}

	global class SETCaseResponse{
		public Cost_Analysis_Case__c SETCase {get;set;}
		public String id{get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}

}