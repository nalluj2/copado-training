//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 10-04-2017
//  Description      : TEST Class for APEX Controller Extension ctrlExt_Opportunity_UploadProd_MITG and VF Page Opportunity_UploadProd_MITH (MITG)
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
@isTest
private class TEST_ctrlExt_Opportunity_UploadProd_MITG {
	
	private static List<Blob> lstBlobCSV = new List<Blob>();
	private static Blob blobCSV_1;
	private static Blob blobCSV_2;
	private static Blob blobCSV_3;
	private static Blob blobCSV_4;
	private static Blob blobCSV_5;
	private static Blob blobCSV_6;
	private static Blob blobCSV_7;
	private static Blob blobCSV_8;
	private static Blob blobCSV_9;

	private static User oUser_SystemAdmin = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true LIMIT 1];

	@isTest private static void createTestData(){

		// Create Business Unit
		clsTestData_MasterData.createSubBusinessUnit();
		
		Sub_Business_Units__c herniaSBU = [Select Id, Business_Unit__c from Sub_Business_Units__c where Name = 'Hernia' AND Business_Unit__r.Company__r.Name = 'Europe'];
		clsTestData_MasterData.oMain_SubBusinessUnit = herniaSBU;
		
		clsTestData_Product.createProductGroup(true);
		
		// Create Account
		clsTestData_Account.createAccount();

		// Create Product
		clsTestData_Product.iRecord_Product = 5;
		clsTestData_Product.createProductData_NoBU(false);
	    Id id_RecordType_Product = clsUtil.getRecordTypeByDevName('Product2', 'MDT_Marketing_Product').Id;

		String tCFNCode = 'CFNCode';
		Integer iCounter = 1;
		for (Product2 oProduct : clsTestData_Product.lstProduct){
			oProduct.CFN_Code_Text__c = tCFNCode + iCounter;
			oProduct.RecordTypeId = id_RecordType_Product;
			oProduct.Business_Unit_ID__c = herniaSBU.Business_Unit__c;
			oProduct.Region_vs__c = 'Europe';
			oProduct.Product_Group__c = clsTestData_Product.oMain_ProductGroup.Id;
			iCounter++;
		}
		insert clsTestData_Product.lstProduct;
		System.assertEquals(clsTestData_Product.lstProduct.size(), 5);

		// Create Pricebook
		clsTestData_Opportunity.createPricebook();

		// Create PricebookEntry
		clsTestData_Opportunity.createPricebookEntry(true);
		System.assertEquals(clsTestData_Opportunity.lstPricebookEntry.size(), 5);

		List<PricebookEntry> lstPricebookEntry = 
            [
                SELECT 
                	Id, Product2.CFN_Code_Text__c, UnitPrice 
                FROM 
                	PricebookEntry 
                WHERE 
                	ID = :clsTestData_Opportunity.lstPricebookEntry
            ];

		// Create Opportunity
		clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'EUR_MITG_Standard_Opportunity').Id;
		clsTestData_Opportunity.createOpportunity(false);
			clsTestData_Opportunity.oMain_Opportunity.CurrencyISOCode = 'EUR';
			clsTestData_Opportunity.oMain_Opportunity.Stagename = 'Develop';
			clsTestData_Opportunity.oMain_Opportunity.Pricebook2Id = clsTestData_Opportunity.oMain_Pricebook.Id;
		insert clsTestData_Opportunity.oMain_Opportunity;

		// Create Test Data Files
        Set<String> setCFNCode = new Set<String>();
		for(PricebookEntry oPriceBookEntry : lstPricebookEntry){
	        setCFNCode.add(oPriceBookEntry.Product2.CFN_Code_Text__c);
		}
		List<String> lstCFNCode = new List<String>();
		lstCFNCode.addAll(setCFNCode);

		clsUtil.debug('createTestData - setCFNCode (' + setCFNCode.size() + ') : ' + setCFNCode);

		// Create Data Files
		String tDelimiter = '\t'; // TAB Delimter
		String tNextLine = '\n';
		String tDataFile_1 = 'SKU' + tDelimiter + 'Quantity' + tDelimiter + 'Price' + tNextLine;
		tDataFile_1 += lstCFNCode[0] + tDelimiter + '10' + tDelimiter + '100' + tNextLine;
		tDataFile_1 += lstCFNCode[1] + tDelimiter + '20' + tDelimiter + '200' + tNextLine;
		tDataFile_1 += lstCFNCode[2] + tDelimiter + '30' + tDelimiter + '300' + tNextLine;

		String tDataFile_2 = 'SKU' + tDelimiter + 'Quantity' + tDelimiter + 'Price' + tNextLine;
		tDataFile_2 += lstCFNCode[0] + tDelimiter + '10' + tDelimiter + '-100' + tNextLine;
		tDataFile_2 += lstCFNCode[1] + tDelimiter + '20' + tDelimiter + '-200' + tNextLine;
		tDataFile_2 += lstCFNCode[2] + tDelimiter + '30' + tDelimiter + '-300' + tNextLine;

		String tDataFile_3 = 'SKU' + tDelimiter + 'Quantity' + tDelimiter + 'Price' + tNextLine;
		tDataFile_3 += lstCFNCode[0] + tDelimiter + '10' + tNextLine;
		tDataFile_3 += lstCFNCode[1] + tDelimiter + '20' + tNextLine;
		tDataFile_3 += lstCFNCode[2] + tDelimiter + '30' + tNextLine;

		String tDataFile_4 = 'SKU' + tDelimiter + 'Quantity' + tDelimiter + 'Price' + tNextLine;
		tDataFile_4 += lstCFNCode[0] + tNextLine;
		tDataFile_4 += lstCFNCode[1] + tNextLine;
		tDataFile_4 += lstCFNCode[2] + tNextLine;

		String tDataFile_5 = 'SKU' + tDelimiter + 'Quantity' + tDelimiter + 'Price' + tDelimiter + 'BIDDINGNUMBER' + tNextLine;
		tDataFile_5 += lstCFNCode[0] + tDelimiter + '10' + tDelimiter + '100' + tDelimiter + '100' + tNextLine;
		tDataFile_5 += lstCFNCode[1] + tDelimiter + '20' + tDelimiter + '200' + tDelimiter + '200' + tNextLine;
		tDataFile_5 += lstCFNCode[2] + tDelimiter + '30' + tDelimiter + '300' + tDelimiter + '300' + tNextLine;

		String tDataFile_6 = 'SKU' + tDelimiter + 'Quantity' + tDelimiter + 'Price' + tDelimiter + 'BIDDINGNUMBER' + tNextLine;
		tDataFile_6 += lstCFNCode[0] + tDelimiter + '10' + tDelimiter + '100' + tDelimiter + '-100' + tNextLine;
		tDataFile_6 += lstCFNCode[1] + tDelimiter + '20' + tDelimiter + '200' + tDelimiter + '-200' + tNextLine;
		tDataFile_6 += lstCFNCode[2] + tDelimiter + '30' + tDelimiter + '300' + tDelimiter + '-300' + tNextLine;

		String tDataFile_7 = 'SKU' + tDelimiter + 'Quantity' + tDelimiter + 'Price' + tDelimiter + 'DUMMY1' + tDelimiter + 'DUMMY2' + tNextLine;
		tDataFile_7 += lstCFNCode[0] + tDelimiter + '10' + tDelimiter + '100' + tDelimiter + 'DUMMY1' + tDelimiter + 'DUMMY2' + tNextLine;
		tDataFile_7 += lstCFNCode[1] + tDelimiter + '20' + tDelimiter + '200' + tDelimiter + 'DUMMY1' + tDelimiter + 'DUMMY2' + tNextLine;
		tDataFile_7 += lstCFNCode[2] + tDelimiter + '30' + tDelimiter + '300' + tDelimiter + 'DUMMY1' + tDelimiter + 'DUMMY2' + tNextLine;

		String tDataFile_8 = 'SKU' + tNextLine;

		blobCSV_1 = Blob.valueOf(tDataFile_1);
		blobCSV_2 = Blob.valueOf(tDataFile_2);
		blobCSV_3 = Blob.valueOf(tDataFile_3);
		blobCSV_4 = Blob.valueOf(tDataFile_4);
		blobCSV_5 = Blob.valueOf(tDataFile_5);
		blobCSV_6 = Blob.valueOf(tDataFile_6);
		blobCSV_7 = Blob.valueOf(tDataFile_7);
		blobCSV_8 = Blob.valueOf(tDataFile_8);
		blobCSV_9 = null;

		lstBlobCSV.add(blobCSV_1);
		lstBlobCSV.add(blobCSV_2);
		lstBlobCSV.add(blobCSV_3);
		lstBlobCSV.add(blobCSV_4);
		lstBlobCSV.add(blobCSV_5);
		lstBlobCSV.add(blobCSV_6);
		lstBlobCSV.add(blobCSV_7);
		lstBlobCSV.add(blobCSV_8);
		lstBlobCSV.add(blobCSV_9);

	}

	@isTest static void test_ctrlExt_Opportunity_UploadProd_MITG_Helper(){

		Test.startTest();

		ctrlExt_Opportunity_UploadProd_MITG.IssueRecord oIssueRecord1 = new ctrlExt_Opportunity_UploadProd_MITG.IssueRecord('PARAM1', 'PARAM2', 'PARAM3');
		ctrlExt_Opportunity_UploadProd_MITG.IssueRecord oIssueRecord2 = new ctrlExt_Opportunity_UploadProd_MITG.IssueRecord('PARAM1', 'PARAM2', 'PARAM3', 'PARAM4', 'PARAM5');

		String tTest = oIssueRecord2.SKU;
		tTest = oIssueRecord2.Quantity;
		tTest = oIssueRecord2.SalesPrice;
		tTest = oIssueRecord2.BiddingNumber;
		tTest = oIssueRecord2.Error;

		Test.stopTest();
	}

	@isTest static void test_ctrlExt_Opportunity_UploadProd_MITG_File(){

		createTestData();

		Test.startTest();

		for (Blob blobCSV : lstBlobCSV){
			try{ test_UploadProduct_File(blobCSV); }catch(Exception oEX){}
		}

		Test.stopTest();

	}

	@isTest static void test_ctrlExt_Opportunity_UploadProd_MITG_AtRisk(){
		
		createTestData();

			clsTestData_Opportunity.oMain_Opportunity.Type = 'At Risk';
		update clsTestData_Opportunity.oMain_Opportunity;

    	Test.startTest();

		for (Blob blobCSV : lstBlobCSV){
			try{ test_UploadProduct_File(blobCSV); }catch(Exception oEX){}
		}

        Test.stopTest();


	}

	@isTest static void test_ctrlExt_Opportunity_UploadProd_MITG_Error(){
		
		createTestData();

    	Test.startTest();

    	clsUtil.clearAllHasExceptions();
    	clsUtil.hasException = true;
		for (Blob blobCSV : lstBlobCSV){
			try{ test_UploadProduct_File(blobCSV); }catch(Exception oEX){}
		}


    	Test.stopTest();

	}

	@isTest static void test_ctrlExt_Opportunity_UploadProd_MITG_Error1(){
		
		createTestData();

    	Test.startTest();

    	clsUtil.clearAllHasExceptions();
    	clsUtil.hasException1 = true;
		for (Blob blobCSV : lstBlobCSV){
			try{ test_UploadProduct_File(blobCSV); }catch(Exception oEX){}
		}

    	Test.stopTest();

	}	

	@isTest static void test_ctrlExt_Opportunity_UploadProd_MITG_Error2(){
		
		createTestData();

    	Test.startTest();

    	clsUtil.clearAllHasExceptions();
    	clsUtil.hasException2 = true;
		for (Blob blobCSV : lstBlobCSV){
			try{ test_UploadProduct_File(blobCSV); }catch(Exception oEX){}
		}

    	Test.stopTest();

	}	

	@isTest static void test_ctrlExt_Opportunity_UploadProd_MITG_Error3(){
		
		createTestData();

    	Test.startTest();

    	clsUtil.clearAllHasExceptions();
    	clsUtil.hasException3 = true;
		for (Blob blobCSV : lstBlobCSV){
			try{ test_UploadProduct_File(blobCSV); }catch(Exception oEX){}
		}

    	Test.stopTest();

	}			

	@isTest static void test_ctrlExt_Opportunity_UploadProd_MITG_Error4(){
		
		createTestData();

    	Test.startTest();

    	clsUtil.clearAllHasExceptions();
    	clsUtil.hasException4 = true;
		for (Blob blobCSV : lstBlobCSV){
			try{ test_UploadProduct_File(blobCSV); }catch(Exception oEX){}
		}

    	Test.stopTest();

	}			

	private static void test_UploadProduct_File(Blob aoBlobCSV){

    	PageReference oPageRef = Page.Opportunity_UploadProduct;
        Test.setCurrentPage(oPageRef);

        System.currentPageReference().getParameters().put('OppId', null);
        ctrlExt_Opportunity_UploadProd_MITG oCtrlExt = new ctrlExt_Opportunity_UploadProd_MITG(new ApexPages.StandardController(clsTestData_Opportunity.oMain_Opportunity));

        System.currentPageReference().getParameters().put('OppId', clsTestData_Opportunity.oMain_Opportunity.Id);
        oCtrlExt = new ctrlExt_Opportunity_UploadProd_MITG(new ApexPages.StandardController(clsTestData_Opportunity.oMain_Opportunity));

        Pagereference oPageRef_File = oCtrlExt.ReadFile();

		oCtrlExt.oFile = aoBlobCSV;
        oPageRef_File = oCtrlExt.ReadFile();

        Boolean bTest = oCtrlExt.hasInvalidRecords;
        String tTest = oCtrlExt.tFileName;

        PageReference oPageRef_Test = oCtrlExt.goBacktoOpp();

	}

}
//--------------------------------------------------------------------------------------------------------------------