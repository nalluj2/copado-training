//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 03-05-2016
//  Description      : APEX TEST Class - Test APEX for :
//						- tr_DemoProduct (APEX Trigger)
//					 	- bl_DemoProduct_Trigger (APEX Class)
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_bl_DemoProduct_Trigger {

	private static List<Demo_Product__c> lstDemoProduct;

	@isTest static void createTestData(){

		clsTestData.iRecord_Account = 4;
		clsTestData.createAccountData();

		clsTestData.iRecord_Contact = 2;
		clsTestData.createContactData();

		clsTestData.iRecord_Opportunity = 2;
		clsTestData.createOpportunityData();

		clsTestData.createProductData();

		// Create Demo_Product__c data
		lstDemoProduct = new List<Demo_Product__c>();
		for (Product2 oProduct : clsTestData.lstProduct){
			Demo_Product__c oDemoProduct = new Demo_Product__c();
				oDemoProduct.Asset_Name__c = oProduct.Id;
				oDemoProduct.Physically_Inspected__c = true;
				oDemoProduct.Current_Location__c = 'Hospital';
			lstDemoProduct.add(oDemoProduct);
		}
		insert lstDemoProduct;

		// Create Movement_Request__c data
		List<Movement_Request__c> lstMovementRequest = new List<Movement_Request__c>();
		for (Demo_Product__c oDemoProduct : lstDemoProduct){
			Movement_Request__c oMovementRequest = new Movement_Request__c();
				oMovementRequest.Demo_Product_Name__c = oDemoProduct.Id;
				oMovementRequest.Hospital__c = clsTestData.lstAccount[0].Id;
				oMovementRequest.Collect_product_at_Hospital__c = clsTestData.lstAccount[1].Id;
				oMovementRequest.Contact_Name__c = clsTestData.lstContact[0].Id;
				oMovementRequest.Approval_Status__c = 'Approved';
				oMovementRequest.Start_Date__c = Date.today().addDays(-10);
				oMovementRequest.End_Date__c = Date.today().addDays(10);
				oMovementRequest.Start_Date_and_Time__c = Datetime.now().addDays(-10);
				oMovementRequest.End_Date_and_Time__c = Datetime.now().addDays(10);
				oMovementRequest.Reloan__c = true;
			lstMovementRequest.add(oMovementRequest);
		}
		insert lstMovementRequest;

	}

	@isTest static void test_setLocationOfDemoProduct_Insert() {

		Test.startTest();

		createTestData();

		Test.stopTest();

	}
	
	@isTest static void test_setLocationOfDemoProduct_Update() {

		createTestData();

		Test.startTest();

		update lstDemoProduct;
		
		Test.stopTest();

	}
	
}
//--------------------------------------------------------------------------------------------------------------------