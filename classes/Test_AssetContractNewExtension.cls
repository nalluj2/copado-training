@isTest
private class Test_AssetContractNewExtension {
	
	static testmethod void testEntSys()
    {                    
        Account acc=new Account();
        acc.AccountNumber='as';
        acc.Name='MNav House Account';
        acc.AccountNumber = '0001234567';
        acc.Account_Country_vs__c='USA';        
        acc.CurrencyIsoCode='USD';
        acc.Phone='24107954';
        insert acc; 
        
        Asset Sys = new Asset();
        Sys.Serial_Nr__c = 'XXXX';
        Sys.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Navigation').getRecordTypeId();
        Sys.Sold_as_Refurbished__c=true;
        Sys.Name='Hyd';
        Sys.Asset_Product_Type__c='Fusion';
        Sys.Delivery_Date__c=System.today();
        Sys.AccountId=Acc.Id;
        Sys.Ownership_Status__c='FPU';
        Sys.Contract_End_Date__c=system.Today();
        Sys.AxiEM_Included__c=false;        
        insert sys;
        
        Contract theContract = new Contract();
        theContract.AccountId = acc.ID;
        theContract.ContractTerm = 12;
        theContract.StartDate = Date.today().addMonths(-2);
        insert theContract;
        
        AssetContract__c esRecord = new AssetContract__c();
        esRecord.Contract__c = theContract.Id;
                    
        AssetContractNewExtension controller = new AssetContractNewExtension(new ApexPages.StandardController(esRecord));       
    }
}