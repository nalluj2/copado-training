/**
 * Creation Date :  20130628
 * Description :    Test method for tr_Opp_DontDeleteSAPOpportunity trigger
 * Specifications : Test method for tr_Opp_DontDeleteSAPOpportunity trigger
 * Author :         Dheeraj Gupta
 */
@isTest(SeeAllData=true)
private class test_tr_Opp_DontDeleteSAPOpportunity {

    static testMethod void myUnitTest() {

    	Id p2 = [select id from profile where name like '%CAN DiB IS Super User%' limit 1].Id; 
        User u2 = new User(alias = 'DsUser', email='dsusertest@absi.be', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
           		localesidkey='en_US', profileid = p2, timezonesidkey='America/Los_Angeles', username='dsusertest@absi.be', Alias_unique__c='usertest1234',Company_Code_Text__c='EUR');

		System.runAs(u2) {
	        Account A = new Account();
	        A.LastName = 'My Test Account';
	        A.PersonBirthdate = system.today();
	        a.DIB_Sales_Rep__c = u2.id;
	        a.Diabetes__c  = true;
	        insert A;
	        
	        Opportunity opp = new Opportunity();
	        opp.AccountId = A.Id; 
	        opp.Name = 'Test Opp';
	        opp.CloseDate = System.today();
	        opp.StageName = 'Won';
			opp.SAP_ID_Text__c = 'Test SAP';
			opp.Deal_Category__c = 'My Deal';
	        insert opp;

 	    	try{
    			delete opp;
 	    	}catch(Exception e){ }
		}
        
    }
}