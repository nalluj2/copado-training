//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 08-04-2016
//  Description      : APEX Test Class to test the logic in tr_Lead and bl_Lead_Trigger
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_bl_Lead_Trigger {

	private static String tEloquaAccountID_1 = 'ELOQUA_ACCOUNT_ID_1';
	private static String tEloquaAccountID_2 = 'ELOQUA_ACCOUNT_ID_2';
	
	//----------------------------------------------------------------------------------------
    // CREATE TEST DATA
    //----------------------------------------------------------------------------------------
    @testSetup static void createTestData() {

    	// Create Account Test Data
    	clsTestData.iRecord_Account = 1;
    	clsTestData.tCountry_Account = 'BELGIUM';
    	clsTestData.idRecordType_Account = clsUtil.getRecordTypeByDevName('Account', 'NON_SAP_Account').Id;
    	clsTestData.createAccountData(false);

    	for (Account oAccount : clsTestData.lstAccount){
    		oAccount.Eloqua_Account_ID__c = tEloquaAccountID_1;
    	}

    	insert clsTestData.lstAccount;


    	// Create Campaign Test Data
    	clsTestData.idRecordType_Campaign = clsUtil.getRecordTypeByDevName('Campaign', 'Europe_xBU').Id;
    	clsTestData.createCampaignData(true);

    	// Create Lead Test Data
    	clsTestData.idRecordType_Lead = clsUtil.getRecordTypeByDevName('Lead', 'MITG_Lead').Id;
    	clsTestData.iRecord_Lead = 1;
    	clsTestData.tCountry_Lead = 'BELGIUM';
    	clsTestData.createLeadData(false);

    	for (Lead oLead : clsTestData.lstLead){
    		oLead.Campaign_Most_Recent__c = clsTestData.oMain_Campaign.Id;
    		oLead.Eloqua_Account_ID__c = tEloquaAccountID_1;
            oLead.Salutation = 'TEST SALUTATION';
    	}

    	insert clsTestData.lstLead;

    }
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
    // TEST addLeadToCampaign
    //----------------------------------------------------------------------------------------
	@isTest static void test_addLeadToCampaign_Insert() {
		
		List<Campaign> lstCampaign = [SELECT Id FROM Campaign];
		System.assertEquals(lstCampaign.size(), 1);

		List<CampaignMember> lstCampaignMember = [SELECT Id FROM CampaignMember WHERE CampaignId = :lstCampaign[0].Id];
		System.assertEquals(lstCampaignMember.size(), 1);

	}
    //----------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------
    // TEST setAccountAffiliation
    //----------------------------------------------------------------------------------------
	@isTest static void test_setAccountAffiliation_Insert() {
		
		Test.startTest();

		List<Lead> lstLead = [SELECT Id, Eloqua_Account_ID__c, AccountAffiliation__c FROM Lead];
		System.assertEquals(lstLead.size(), 1);

		List<Account> lstAccount = [SELECT Id, Eloqua_Account_ID__c FROM Account];
		System.assertEquals(lstAccount.size(), 1);

		Test.stopTest();


		System.assertEquals(lstLead[0].AccountAffiliation__c, lstAccount[0].Id);

	}

	@isTest static void test_setAccountAffiliation_Update_1() {

		// Create additional Account ID		
		clsTestData.oMain_Account = null;
    	clsTestData.iRecord_Account = 1;
    	clsTestData.tCountry_Account = 'BELGIUM';
    	clsTestData.idRecordType_Account = clsUtil.getRecordTypeByDevName('Account', 'NON_SAP_Account').Id;
		clsTestData.createAccountData(false);

    	for (Account oAccount : clsTestData.lstAccount){
    		oAccount.Eloqua_Account_ID__c = tEloquaAccountID_2;
			oAccount.isSales_Force_Account__c = false;
    	}

    	insert clsTestData.lstAccount;
		Id id_Account = clsTestData.lstAccount[0].Id;

		List<Lead> lstLead = [SELECT Id, Eloqua_Account_ID__c, AccountAffiliation__c FROM Lead];
		System.assertEquals(lstLead.size(), 1);

		List<Account> lstAccount = [SELECT Id, Eloqua_Account_ID__c FROM Account];
		System.assertEquals(lstAccount.size(), 2);

		System.assertNotEquals(lstLead[0].AccountAffiliation__c, id_Account);

		Test.startTest();

			lstLead[0].Eloqua_Account_ID__c = tEloquaAccountID_2;
		update lstLead;

        lstLead = [SELECT Id, Eloqua_Account_ID__c, AccountAffiliation__c FROM Lead];

        System.assertEquals(lstLead.size(), 1);
        System.assertEquals(lstLead[0].AccountAffiliation__c, id_Account);

            lstLead[0].Eloqua_Account_ID__c = null;
        update lstLead;

		Test.stopTest();

        lstLead = [SELECT Id, Eloqua_Account_ID__c, AccountAffiliation__c FROM Lead];

        System.assertEquals(lstLead.size(), 1);
        System.assertEquals(lstLead[0].AccountAffiliation__c, null);

	}

    @isTest static void test_setAccountAffiliation_Update_2() {

        // Create additional Account ID     
        clsTestData.oMain_Account = null;
        clsTestData.iRecord_Account = 1;
        clsTestData.tCountry_Account = 'BELGIUM';
        clsTestData.idRecordType_Account = clsUtil.getRecordTypeByDevName('Account', 'NON_SAP_Account').Id;
        clsTestData.createAccountData(false);

        for (Account oAccount : clsTestData.lstAccount){
            oAccount.Eloqua_Account_ID__c = tEloquaAccountID_2;
			oAccount.isSales_Force_Account__c = false;
        }

        insert clsTestData.lstAccount;
        Id id_Account = clsTestData.lstAccount[0].Id;

        List<Lead> lstLead = [SELECT Id, Eloqua_Account_ID__c, AccountAffiliation__c FROM Lead];
        System.assertEquals(lstLead.size(), 1);
        System.assertNotEquals(lstLead[0].AccountAffiliation__c, null);

        Test.startTest();

            lstLead[0].Eloqua_Account_ID__c = null;
        update lstLead;

        Test.stopTest();

        lstLead = [SELECT Id, Eloqua_Account_ID__c, AccountAffiliation__c FROM Lead];

        System.assertEquals(lstLead.size(), 1);
        System.assertEquals(lstLead[0].AccountAffiliation__c, null);

    }
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
    // TEST setAccountAffiliation
    //----------------------------------------------------------------------------------------
    @isTest static void test_copyLeadSalutation_Insert() {

        Test.startTest();

        Test.stopTest();

        List<Lead> lstLead = [SELECT Id, Salutation, Salutation_Text__c FROM Lead];
        for (Lead oLead : lstLead){
            System.assertEquals(oLead.Salutation_Text__c, 'TEST SALUTATION');
            System.assertEquals(oLead.Salutation, null);
        }
    
    }

    @isTest static void test_copyLeadSalutation_Update() {

        List<Lead> lstLead = [SELECT Id, Salutation, Salutation_Text__c FROM Lead];
        for (Lead oLead : lstLead){
            System.assertEquals(oLead.Salutation_Text__c, 'TEST SALUTATION');
            System.assertEquals(oLead.Salutation, null);
        }

        Test.startTest();

        for (Lead oLead : lstLead){
            oLead.Salutation = 'TEST SALUTATION 2';
        }

        update lstLead;

        Test.stopTest();

        lstLead = [SELECT Id, Salutation, Salutation_Text__c FROM Lead];
        for (Lead oLead : lstLead){
            System.assertEquals(oLead.Salutation_Text__c, 'TEST SALUTATION 2');
            System.assertEquals(oLead.Salutation, null);
        }

    }
    //----------------------------------------------------------------------------------------
}
//--------------------------------------------------------------------------------------------------------------------