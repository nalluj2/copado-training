@isTest
private class Test_tr_Diabetes_Performance {
    
    private static testmethod void testPopulateMobileId() {

		Account acc = new Account();
		acc.Name = 'Unit Test Account 1';
		acc.SAP_Id__c = '123456789';
		acc.Account_Country_vs__c = 'Belgium';
		insert acc;
				
		Department_Master__c pediatric = new Department_Master__c();
		pediatric.Name = 'Pediatric';
		insert pediatric;
		
		Test.startTest();
		
		Diabetes_Performance__c accPediatric = new Diabetes_Performance__c();        		
		accPediatric.Department_Lookup__c = pediatric.Id; 
		insert accPediatric;
				
		accPediatric = [Select Id, Mobile_ID__c from Diabetes_Performance__c where Id = :accPediatric.Id];
		System.assert(accPediatric.Mobile_ID__c != null);
	}
}