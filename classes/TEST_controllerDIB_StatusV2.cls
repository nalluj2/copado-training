/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TEST_controllerDIB_StatusV2 {
    
    private datetime currentDateTime;
    private Date currentDate;
    private Date startDate;
    private Date endDate;
    private User currentUser;
    private controllerDIB_StatusV2 myControllerDIB_Status;
    private String months;
    private String years;   

    static testMethod void myUnitTest() {
        System.debug(' ################## ' + 'BEGIN TEST_controllerDIB_Status' + ' ################');
		Id myProfile = clsUtil.getUserProfileId('EUR DiB Sales Entry');
        //User myUser = [Select id from User where profileId =:myProfile and IsActive=true limit 1];
         User myUser = new User(alias = 'repuser', email='usertest@absi.be', emailencodingkey='UTF-8', lastname='Testing', 
                languagelocalekey='en_US',localesidkey='en_US', profileid = myProfile, timezonesidkey='America/Los_Angeles', 
                username='usertest@absi.be', Alias_unique__c='usertest1234',Company_Code_Text__c='EUR');        

		// Create DIB Fiscal Period Test Data - if needed
		List<SelectOption> periods = DIB_AllocationSharedMethods.getPeriodLines();
		if (periods.size() == 0) clsTestData_System.createDIBFiscalPeriod();
                
        System.runAs(myUser) {
            System.debug('Current User: ' + UserInfo.getUserName());
            System.debug('Current Profile: ' + UserInfo.getProfileId());
            TEST_controllerDIB_StatusV2 myTestController = new TEST_controllerDIB_StatusV2();
            myTestController.init(myUser);
            myTestController.createRecords();
            myTestController.doTests();
        }
        System.debug(' ################## ' + 'END TEST_controllerDIB_Status' + ' ################');
    }
      
    public void init(User myUser) {
        System.debug(' ################## ' + 'TEST_controllerDIB_Status_init' + ' ################');
        datetime currentDateTime = datetime.now();
        currentDate = currentDateTime.date();
        startDate = currentDateTime.date().addYears(-2);
        endDate = currentDateTime.date().addYears(+2);
        
        // Get the current fiscal months and current fiscal years. 
        List<SelectOption> periods = DIB_AllocationSharedMethods.getPeriodLines()   ;       
        system.debug('##########################################Period ' + periods ) ; 
        String[] fiscalPeriod = new String[]{} ;        
        for (SelectOption so : periods){
            fiscalPeriod.add(so.getValue());                    
        }
        months = fiscalPeriod.get(0).substring(0,2);
        years = fiscalPeriod.get(0).substring(3,7); 
        
        currentUser = myUser;   
    }
    
    public void doTests() {
        System.debug(' ################## ' + 'TEST_controllerDIB_Status_doTests' + ' ################');
        
        List<DIB_Submit_Summary__c> myDIB_summaries = new List<DIB_Submit_Summary__c>{} ;
        DIB_Submit_Summary__c myDIB_Summary = new DIB_Submit_Summary__c();
        ApexPages.Standardcontroller sct = new ApexPages.Standardcontroller(myDIB_Summary);
        myControllerDIB_Status =  new controllerDIB_StatusV2(sct);
        
        myControllerDIB_Status.getMonths();
        
        myControllerDIB_Status.getYears();

        myControllerDIB_Status.setYears(this.years);
        myControllerDIB_Status.setMonths(String.valueOf(Integer.valueOf(months) + 1));
        myControllerDIB_Status.fillCompleted_status();
        
    }
      
       public void createRecords() {
        
            String accountType = 'Adult';
            
            // Sold To Accounts 
            List<Account> soldToAccounts    = new List<Account>{} ;
            
            // Sales Force Accounts 
            List<Account> sfaAccounts       = new List<Account>{} ;
            
            // Create a few Sold to Accounts ; 
            for (Integer i = 0; i < 10 ; i++){
                Account acc1 = new Account();
                acc1.Name = 'TEST_controllerDIB_Status Aatest Coverage Sold to or ship To Account 111 ' + i ;
                acc1.Account_Active__c = true;
                soldToAccounts.add(acc1) ;
            }
            insert soldToAccounts ;
            
            // create Sales Force Accounts : 
            for (Integer i = 0; i < 10 ; i++){
                Account acc1 = new Account();
                acc1.Name = 'TEST_controllerDIB_Status Aatest Coverage Sales Force Account 222' + i ;
                acc1.isSales_Force_Account__c = true ; 
                acc1.Account_Active__c = true;
                sfaAccounts.add(acc1) ;
            }
            insert sfaAccounts ;
            
            System.debug(' ################## ' + 'controllerDIB_Campaign_createRecords_sfaAccounts: ' + sfaAccounts + ' ################');
            
            //Department
            Department_Master__c department = new Department_Master__c();
            department.Name = accountType;
            insert department;
            
            DIB_Department__c myDepartment = new DIB_Department__c();
            myDepartment.Account__c = sfaAccounts.get(0).Id;
            myDepartment.Department_ID__c = department.Id;
            
            insert myDepartment;
            
            List<DIB_Submit_Summary__c> myDIB_Statuses = new List<DIB_Submit_Summary__c>{} ;
            
            DIB_Submit_Summary__c myDIB_Status;
            
            myDIB_Status = new DIB_Submit_Summary__c();
            myDIB_Status.Fiscal_Month__c = months;
            myDIB_Status.Fiscal_Year__c = years;
            myDIB_Status.Sales_Rep__c = currentUser.Id;
            myDIB_Status.Sales__c = true;
            myDIB_Statuses.add(myDIB_Status);

            myDIB_Status = new DIB_Submit_Summary__c();
            myDIB_Status.Fiscal_Month__c = months;
            myDIB_Status.Fiscal_Year__c = years;
            myDIB_Status.Sales_Rep__c = currentUser.Id;
            myDIB_Status.Sales__c = true;
            myDIB_Statuses.add(myDIB_Status);
            
            myDIB_Status = new DIB_Submit_Summary__c();
            myDIB_Status.Fiscal_Month__c = String.valueOf(Integer.valueOf(months) + 1);
            myDIB_Status.Fiscal_Year__c = years;
            myDIB_Status.Sales_Rep__c = currentUser.Id;
            myDIB_Status.Sales__c = true;
            myDIB_Statuses.add(myDIB_Status);           
                        
            insert myDIB_Statuses;
            
            System.debug(' ################## ' + 'controllerDIB_Campaign_createRecords_myDIB_Statuses: ' + myDIB_Statuses + ' ################');
            
            List<DIB_Invoice_Line__c> invoiceLines0109 = new List<DIB_Invoice_Line__c>{} ;      
            for (Integer i = 0; i < 10 ; i++){
                DIB_Invoice_Line__c invl  = new DIB_Invoice_Line__c() ; 
                invl.Distribution_No__c = 'DN' ; 
                invl.Name = 'Invoice Name' ;  
                invl.Fiscal_Month_Code__c = months ;  
                invl.Fiscal_Year__c = years ;
                invl.Allocated_Fiscal_Month_Code__c=months;
                invl.Allocated_Fiscal_Year__c=years; 
                invl.Sales_Force_Account__c=sfaAccounts.get(0).Id;
                invl.Department_ID__c=myDepartment.Id; 
                // This field must have a value otherwise can have a problem saying Argument 1 cannot be null Line 101 on Method SharedMethods. 
                invl.Quantity_Invoiced_Number__c = 1 ; 
                invl.Sold_To__c = sfaAccounts.get(i).Id;
                invl.Ship_To_Name_Text__c = sfaAccounts.get(i).Name;
                invl.Is_Allocated__c = false ;  
                if (i > 5 && i < 20){
                    invl.Is_Allocated__c = true ; 
                }
                invoiceLines0109.add(invl) ; 
            }

        insert invoiceLines0109 ; 
       }
}