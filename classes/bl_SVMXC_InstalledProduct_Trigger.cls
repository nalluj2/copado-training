//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 05-01-2016
//  Description      : APEX Class - Business Logic for tr_SVMXC_InstalledProduct
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public class bl_SVMXC_InstalledProduct_Trigger {

	//------------------------------------------------------------------------------------------------
	// Public variables coming from the trigger
	//------------------------------------------------------------------------------------------------
	public static List<SVMXC__Installed_Product__c> lstTriggerNew = new List<SVMXC__Installed_Product__c>();
	public static Map<Id, SVMXC__Installed_Product__c> mapTriggerNew = new Map<Id, SVMXC__Installed_Product__c>();
	public static Map<Id, SVMXC__Installed_Product__c> mapTriggerOld = new Map<Id, SVMXC__Installed_Product__c>();
	//------------------------------------------------------------------------------------------------


	//------------------------------------------------------------------------------------------------
	// Public Methods that will process the records
	//------------------------------------------------------------------------------------------------
	// CGEN2 - Send Notification to SAP / WebMethods
	public static void notificationSAP(String tAction){

		if (UserInfo.getProfileId() != clsUtil.getUserProfileId('SAP Interface') && UserInfo.getProfileId() != clsUtil.getUserProfileId('Devart Tool') && UserInfo.getProfileId() != clsUtil.getUserProfileId('SMAX Interface')){

	        List<SVMXC__Installed_Product__c> lstData_Processing = new List<SVMXC__Installed_Product__c>();

			for (SVMXC__Installed_Product__c oData : lstTriggerNew){

				if (tAction == 'UPDATE'){

				    // Define the fields that we need to validate to indicate if the record needs further processing
	    	        Set<String> setField_Processing = new Set<String>();
						setField_Processing.add('SVMX_SAP_Equipment_ID__c');
						setField_Processing.add('SVMX_Software_Version__c');
						setField_Processing.add('SVMXC__Asset_Tag__c');
						setField_Processing.add('SVMX_Equipment_Use_Status__c');
						setField_Processing.add('SVMX_Optional_Software_Installed__c');
						setField_Processing.add('Equipment_Room__c');
						setField_Processing.add('Last_Disinfection_Date__c');
						setField_Processing.add('Last_PM_Date__c');
						

	                sObject oData_Old = mapTriggerOld.get(oData.Id);

	                for (String tField : setField_Processing){
	                    if (oData.get(tField) != oData_Old.get(tField)){
							lstData_Processing.add(oData);
	                        break;  // Record is added to list of processing record - continue with next record
	                    }
	                }

	            }

			}

	        if (lstData_Processing.size() > 0){
	        	bl_NotificationSAP.sendNotificationToSAP_InstalledProduct(lstData_Processing, tAction);
	        }
		
		}
	}
	//------------------------------------------------------------------------------------------------


	//------------------------------------------------------------------------------------------------
	// Update Account_Country__c on Service Order with the Country of the Account
	//------------------------------------------------------------------------------------------------
	public static void copyAccountCountry(String tAction){

		List<SVMXC__Installed_Product__c> lstInstalledProduct = new List<SVMXC__Installed_Product__c>();
		Set<Id> setID_Account = new Set<Id>();

		for (SVMXC__Installed_Product__c oData : lstTriggerNew){

    		if (tAction == 'INSERT'){

        		lstInstalledProduct.add(oData);
        		if (oData.SVMXC__Company__c != null){
    	    		setID_Account.add(oData.SVMXC__Company__c);
    	    	}

    		}else if (tAction == 'UPDATE'){

    			SVMXC__Installed_Product__c oData_Old = mapTriggerOld.get(oData.Id);

    			if (oData.SVMXC__Company__c != oData_Old.SVMXC__Company__c){
	        		lstInstalledProduct.add(oData);
	        		if (oData.SVMXC__Company__c != null){
    	    			setID_Account.add(oData.SVMXC__Company__c);
    	    		}
    			}

    		}

        }

        // Get the Account_Country_vs__c of the collected Account ID's
        Map<Id, Account> mapAccount = new Map<Id, Account>([SELECT Id, Account_Country_vs__c FROM Account WHERE Id = :setID_Account]);

        for (SVMXC__Installed_Product__c oInstalledProduct : lstInstalledProduct){

        	if (oInstalledProduct.SVMXC__Company__c == null){

        		// If the processing record doesn't have a related Account, clear the Account_Country__c on the processing record
        		oInstalledProduct.Account_Country__c = null;

        	}else if (mapAccount.containsKey(oInstalledProduct.SVMXC__Company__c)){

        		oInstalledProduct.Account_Country__c = mapAccount.get(oInstalledProduct.SVMXC__Company__c).Account_Country_vs__c;

        	}
        
        }

	}
	//------------------------------------------------------------------------------------------------
	
	public static void processKeyValueField(){

		Map<String, String> fieldMapping = new Map<String, String>();
		
		for (SAP_Interface_Mappings__c mapping : [Select From__c, To__c from SAP_Interface_Mappings__c where API_Object_Name__c = 'SVMXC__Installed_Product__c' AND API_Field_Name__c = '-']){
			
			fieldMapping.put(mapping.From__c, mapping.To__c);
		}
		
		for (SVMXC__Installed_Product__c instProduct : lstTriggerNew){
			
			String rawString = instProduct.Key_value_data__c;
			
			if(rawString == null || rawString == '') continue;
			
			Map<String, List<String>> fieldValueMap = new Map<String, List<String>>();
				
			for(String keyValue : rawString.split(';')){
					
				if(keyValue == null || keyValue == '') continue;
				
				String[] tokens = keyValue.split(':');
												
				String key = tokens[0].trim();
				String value = tokens.size() > 1 ? tokens[1].trim() : '';
				
				String fieldName = fieldMapping.get(key);
				
				if(fieldName == null) continue;
				
				List<String> fieldValues = fieldValueMap.get(fieldName);
				
				if(fieldValues == null){				
					
					fieldValues = new List<String>();
					fieldValueMap.put(fieldName, fieldValues);
				}
				
				fieldValues.add(value);
			}
			
			if(fieldValueMap.keySet().size() > 0){
				
				for(String fieldName : fieldValueMap.keySet()){
					
					List<String> fieldValues = fieldValueMap.get(fieldName);
					
					instProduct.put(fieldName, String.join(fieldValues, ';'));					
				}				
			}
		}
	}
}
//--------------------------------------------------------------------------------------------------------------------