/*  
    Class Name -    ctrlExt_AddDefaultAccountPlanTeam
    Test Class Name - Test_ContactActionPlanNew
    Description  –  The written class will used to Add Default Account Plan Team Member for a Account Plan based on available 'Default Opportunity Team'(UserTeam) available on User Detail page.
    Author - Manish Kumar Srivastava
    Created Date  - 29/10/2012 
*/


public class ctrlExt_AddDefaultAccountPlanTeam {
    public List<Account_Plan_Team_Member__c> lstAPTeam=new List<Account_Plan_Team_Member__c>();
    public pagereference CreateAccountPlanTeam(){
        string AccPlanid;
        if(ApexPages.currentPage().getParameters().get('accpid')!=null){
        AccPlanid=ApexPages.currentPage().getParameters().get('accpid');
        List<Account_Plan_Team_Member__c> lstExistingAPTM=[
        	Select a.Team_Member__c, a.Id, a.Account_Plan__c 
        	From Account_Plan_Team_Member__c a 
        	where a.Account_Plan__c=:AccPlanid
        	];
        map<id,id> mapToAvoidDupUTM=new map<id,id>();
        if(lstExistingAPTM.size()>0){
            for(Account_Plan_Team_Member__c APT:lstExistingAPTM){
            mapToAvoidDupUTM.put(APT.Team_member__c,APT.Team_member__c);
            }   
        }
        List<Account_Plan_2__c> lstAPL=[Select a.Id, a.Account__c From Account_Plan_2__c a where id=:AccPlanid];
		if(lstAPL!=null && lstAPL[0].Account__c!=null){
		List<UserTeamMember> lstUserTeamMembers=[
			Select u.userid,u.id,u.ownerid From UserTeamMember u 
			where u.ownerid=:userinfo.getuserid()
			];   
			if(lstUserTeamMembers!=null && lstUserTeamMembers.size()>0){
                for(UserTeamMember UTM:lstUserTeamMembers){
                    if(mapToAvoidDupUTM==null || mapToAvoidDupUTM.get(UTM.userid)==null){
                        lstAPTeam.add(new Account_Plan_Team_Member__c(Team_Member__c=UTM.Userid,Account_Plan__c=AccPlanid));    
                    }   
                }
                if(lstAPTeam!=null && lstAPTeam.size()>0){
                    insert lstAPTeam;
                }
            }
		}
	}
return new Pagereference('/'+AccPlanid);
}

public ctrlExt_AddDefaultAccountPlanTeam(ApexPages.StandardController controller) {
}

}