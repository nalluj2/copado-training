/*
 *      Created Date 	: 2014-06-19
 *      Author 			: Bart Caelen
 *      Description 	: TEST Class for the APEX Controller Extension ctrlExt_Case_GenerateCalendarEvent
 */
@isTest private class TEST_ctrlExt_Case_GenerateCalendarEvent {
	
	@isTest static void test_ctrlExt_Case_GenerateCalendarEvent() {
		
		//------------------------------------------------
		// CREATE TEST DATA
		//------------------------------------------------
		clsTestData.createCaseData();
			clsTestData.oMain_Case.Start_of_Procedure__c			= Datetime.now();
			clsTestData.oMain_Case.Procedure_Duration_Implants__c	= '01:00';
			clsTestData.oMain_Case.Status 							= 'New';
		update clsTestData.oMain_Case;
		//------------------------------------------------


		//------------------------------------------------
		// PERFORM TESTING
		//------------------------------------------------
		Test.startTest();

		PageReference oPage = new PageReference('/Case_GenerateCalendarEvent?Id=' + clsTestData.oMain_Case.Id);

        ApexPages.StandardController oSTDCTRL = new ApexPages.StandardController(clsTestData.oMain_Case);
        ctrlExt_Case_GenerateCalendarEvent oCTRL = new ctrlExt_Case_GenerateCalendarEvent(oSTDCTRL);

        oCTRL.redirect();

        Test.stopTest();

        Case oCase = [SELECT Id, Status FROM Case WHERE Id = :clsTestData.oMain_Case.Id];
        system.assertEquals(oCase.Status, 'Closed');
		//------------------------------------------------
	}
}