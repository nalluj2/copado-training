@RestResource(urlMapping='/SETCaseAnswerService/*')
global with sharing class ws_SETCaseAnswerService {
	
	@HttpPost
	global static String doPost() {
 		
 		RestRequest req = RestContext.request;
			
		System.debug('post called '+ req);
		System.debug('post requestBody '+ req.requestBody.toString());
			
		String body = req.requestBody.toString();
		SETCaseAnswerRequest SETCaseAnswerRequest = (SETCaseAnswerRequest) System.Json.deserialize(body, SETCaseAnswerRequest.class);
			
		System.debug('post requestBody ' + SETCaseAnswerRequest);
			
		Cost_Analysis_Case_Answer__c SETCaseAnswer = SETCaseAnswerRequest.SETCaseAnswer;

		SETCaseAnswerResponse resp = new SETCaseAnswerResponse();

		//We catch all exception to assure that 
		try{
				
			resp.SETCaseAnswer = bl_SETCase.saveSETCaseAnswer(SETCaseAnswer);
			resp.id = SETCaseAnswerRequest.id;
	
			resp.success = true;
			
		}catch (Exception e){
			
			resp.message = e.getMessage();
			resp.success = false;
		}
 
		bl_Mobilesync.storeRequest(UserInfo.getUserId(), 'SETCaseAnswer', req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
  			
		return System.JSON.serialize(resp);		
	}

	@HttpDelete
	global static String doDelete(){
		RestRequest req = RestContext.request;
			
		System.debug('post called ' + req);
		System.debug('post requestBody ' + req.requestBody.toString());
			
		String body = req.requestBody.toString();
		SETCaseAnswerDeleteRequest SETCaseAnswerDeleteRequest = (SETCaseAnswerDeleteRequest) System.Json.deserialize(body, SETCaseAnswerDeleteRequest.class);
			
		System.debug('post requestBody ' + SETCaseAnswerDeleteRequest);
			
		Cost_Analysis_Case_Answer__c SETCaseAnswer = SETCaseAnswerDeleteRequest.SETCaseAnswer;
		
		SETCaseAnswerDeleteResponse resp = new SETCaseAnswerDeleteResponse();
		
		List<Cost_Analysis_Case_Answer__c> SETCaseAnswersToDelete = [select id, mobile_id__c from Cost_Analysis_Case_Answer__c where Mobile_ID__c = :SETCaseAnswer.Mobile_ID__c];
		
		try{
			
			if (SETCaseAnswersToDelete != null && SETCaseAnswersToDelete.size() > 0){	
				
				Cost_Analysis_Case_Answer__c SETCaseAnswerToDelete = SETCaseAnswersToDelete.get(0);
				bl_SETCase.deleteSETCaseAnswer(SETCaseAnswerToDelete);
				resp.SETCaseAnswer = SETCaseAnswerToDelete;
				resp.id = SETCaseAnswerDeleteRequest.id;
				resp.success = true;
				
			}else{
				
				resp.SETCaseAnswer = SETCaseAnswer;
				resp.id = SETCaseAnswerDeleteRequest.id;
				resp.message='SETCaseAnswer not found';
				resp.success = true;
			}
			
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
		}
			
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'SETCaseAnswerDelete', req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
			
		return System.JSON.serialize(resp);
	}

	global class SETCaseAnswerRequest{
		public Cost_Analysis_Case_Answer__c SETCaseAnswer {get;set;}
		public String id{get;set;}
	}
	
	global class SETCaseAnswerDeleteRequest{
		public Cost_Analysis_Case_Answer__c SETCaseAnswer {get;set;}
		public String id{get;set;}
	}
		
	global class SETCaseAnswerDeleteResponse{
		public Cost_Analysis_Case_Answer__c SETCaseAnswer {get;set;}
		public String id{get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}

	global class SETCaseAnswerResponse{
		public Cost_Analysis_Case_Answer__c SETCaseAnswer {get;set;}
		public String id{get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}

}