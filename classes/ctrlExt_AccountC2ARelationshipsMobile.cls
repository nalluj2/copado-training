public with sharing class ctrlExt_AccountC2ARelationshipsMobile {
	
	public List<Affiliation__c> relationships {get; set;}
	
	public ctrlExt_AccountC2ARelationshipsMobile(ApexPages.StandardController sc){
		
		Id relatedId = sc.getId();
		
		if(relatedId.getSObjectType().getDescribe().getName() == 'Contact'){
			
			relationships = [Select Id, Affiliation_To_Account__c, Affiliation_To_Account__r.Name, Affiliation_Type__c, Affiliation_Start_Date__c, Affiliation_End_Date__c from Affiliation__c 
							where RecordType.DeveloperName = 'C2A' AND Affiliation_From_Contact__c =:relatedId
							ORDER BY Affiliation_Start_Date__c ];
		}else{	
			
			relationships = [Select Id, Affiliation_From_Contact__c, Affiliation_From_Contact__r.Name, Affiliation_Type__c, Affiliation_Start_Date__c, Affiliation_End_Date__c from Affiliation__c 
							where RecordType.DeveloperName = 'C2A' AND Affiliation_To_Account__c =:relatedId
							ORDER BY Affiliation_Start_Date__c ];
		}				
	}
}