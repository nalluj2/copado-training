/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_triggerDepartment {

    static testMethod void departmentTest() {
        
        List <Account> sfaAccounts = new List<Account>{} ; 
        
        // create Sales Force Accounts : 
        for (Integer i = 0; i < 50 ; i++){
            Account acc1 = new Account();
            acc1.Name = 'Test_triggerDepartment Test Coverage Sales Force Account ' + i ;
            acc1.isSales_Force_Account__c = true ; 
            sfaAccounts.add(acc1) ; 
        }
        
        insert sfaAccounts ; 
        
        // Create Departments 
        List <DIB_Department__c> departments = new List<DIB_Department__c>{} ; 
        
        Department_Master__c department = new Department_Master__c();
        department.Name = 'Adult';
        insert department;
        
        for (Account a : sfaAccounts){
            DIB_Department__c d = new  DIB_Department__c() ; 
            d.Department_ID__c = department.Id; 
            d.Account__c = a.Id ;
            departments.add(d); 
        }
        
        insert departments  ; 
        
        // try to insert a department that exists already. 
        DIB_Department__c d2 = new  DIB_Department__c() ; 
        d2.Department_ID__c = department.Id; 
        d2.Account__c = sfaAccounts[0].Id ;
        try{
            insert d2 ; 
        }catch(DMLException e){
             System.assert( e.getMessage().contains(FinalConstants.N0_MULTIPLE_DEPARTMENTS_ERROR_MSG), 
                e.getMessage() );
        }
        
        // dummy mass  update : 
        update departments ; 
        
        // unique update
        departments[1].Department_ID__c = department.Id; 
        update departments[1] ; 
        
        
    }
}