/*
 *      Description : This class exposes methods to create / update / delete a Tender Calendar 2
 *
 *      Author = Rudy De Coninck
 */
@RestResource(urlMapping='/TenderCalendar2Service/*')
global with sharing class ws_TenderCalendar2Service {
	@HttpPost
	global static String doPost() {
 		RestRequest req = RestContext.request;
			
		System.debug('post called '+req);
		System.debug('post requestBody '+req.requestBody);
			
		String body = req.requestBody.toString();
		IFTenderCalendar2Request tenderCalendarRequest = (IFTenderCalendar2Request)System.Json.deserialize(body, IFTenderCalendar2Request.class);
			
		System.debug('post requestBody '+tenderCalendarRequest);
			
		Tender_Calendar_2__c tenderCalendar = tenderCalendarRequest.tenderCalendar2;

		IFTenderCalendar2Response resp = new IFTenderCalendar2Response();

		//We catch all exception to assure that 
		try{
				
			resp.tenderCalendar2 = bl_AccountPlanning.saveTenderCalendar2(tenderCalendar);
			resp.id = tenderCalendarRequest.id;
	
			resp.success = true;
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
		}
 
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'TenderCalendar2' ,req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
  			
		return System.JSON.serialize(resp);	 			
	}

	@HttpDelete
	global static String doDelete(){
		RestRequest req = RestContext.request;
			
		System.debug('post called '+req);
		System.debug('post requestBody '+req.requestBody);
			
		String body = req.requestBody.toString();
		IFTenderCalendar2DeleteRequest tenderCalendarDeleteRequest = (IFTenderCalendar2DeleteRequest)System.Json.deserialize(body, IFTenderCalendar2DeleteRequest.class);
			
		System.debug('post requestBody '+tenderCalendarDeleteRequest);
			
		Tender_Calendar_2__c tenderCalendar = tenderCalendarDeleteRequest.tenderCalendar2;

		IFTenderCalendar2Response resp = new IFTenderCalendar2Response();

		List<Tender_Calendar_2__c> tenderCalendarsExisting = [select id, mobile_id__c from Tender_Calendar_2__c where Mobile_ID__c = :tenderCalendar.Mobile_ID__c];
			
		//We catch all exception to assure that 
		try{
				
			if (tenderCalendarsExisting !=null && tenderCalendarsExisting.size()>0 ){
				
				Tender_Calendar_2__c toDelete = tenderCalendarsExisting.get(0);
				delete toDelete;
				resp.tenderCalendar2 = toDelete;
				resp.id = tenderCalendarDeleteRequest.id;
				resp.success = true;
			}else{
				resp.tenderCalendar2 = tenderCalendar;
				resp.id = tenderCalendarDeleteRequest.id;
				resp.message='Tender Calendar not found';
				resp.success = true;
			}
				
		}catch (Exception e){
				
			resp.message=e.getMessage();
			resp.success=false;
		}
  			
			
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'TenderCalendar2Delete',req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
			
		return System.JSON.serialize(resp);
	}
	
	global class IFTenderCalendar2Request{
		public Tender_Calendar_2__c tenderCalendar2 {get;set;}
		public String id{get;set;}
	}
	
	global class IFTenderCalendar2DeleteRequest{
		public Tender_Calendar_2__c tenderCalendar2 {get;set;}
		public String id{get;set;}
	}
		
	global class IFTenderCalendar2DeleteResponse{
		public Tender_Calendar_2__c tenderCalendar2 {get;set;}
		public String id{get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}

	global class IFTenderCalendar2Response{
		public Tender_Calendar_2__c tenderCalendar2 {get;set;}
		public String id{get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}
}