@isTest
public class SETCase_Test {

    static testMethod void testrun() {
        
        Account a = new Account();        
        a.Name = 'New Account Test Name 1';
        insert a;
        
        Test.startTest();
                        
        Cost_Analysis_Case__c c = new Cost_Analysis_Case__c(Account__c = a.Id, Start_Date__c=System.Today(), Status__c = 'Specifying Details');
        insert c;
        
        List<Task> accTask = [Select Id from Task where WhatId = :a.Id AND RecordType.DeveloperName = 'SET_Activity'];
        
        System.assert(accTask.size() == 0);
                       
        c.Status__c = 'Closed without HCP approval';
        update c;
        
        accTask = [Select Id from Task where WhatId = :a.Id AND RecordType.DeveloperName = 'SET_Activity'];
        
        System.assert(accTask.size() == 1);
        
    }
}