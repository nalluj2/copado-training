@isTest
private class Test_ctrlExt_Support_XW_Admin {
	
	private static User currentUser = [Select Id, Email from User where Id=:UserInfo.getUserId()];
	
	private static testMethod void sfdcDispatcher(){
                       
        Create_User_Request__c userRequest = createRequest('Bug');
        userRequest.Description_Long__c = 'descr';
        insert userRequest;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(userRequest);
        ctrlExt_Support_Request_Dispatcher controller = new ctrlExt_Support_Request_Dispatcher(sc);
        
        controller.redirect();
    }
    
    private static testMethod void completeRequest(){
    	
    	System.runAs(currentUser){
            assignUserToAdminGroup();
        }	
        
        Create_User_Request__c userRequest = createRequest('Enable Mobile App');
       
        userRequest.Created_User__c = UserInfo.getUserId();
        userRequest.Mobile_App__c = 'APDR';
        
        insert userRequest;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(userRequest);
        ctrlExt_Support_XW_Admin controller = new ctrlExt_Support_XW_Admin(sc);
        
        controller.assignToMe();

        Boolean bTest = controller.bIsUpdatable;
        bTest = controller.bIsAssigned;

        controller.completeRequest();

        userRequest.Time_Spent__c = 100;
        controller.completeRequest();

        userRequest.Sub_Status__c = 'Solved (Permanently)';
        controller.completeRequest();

        System.assert(userRequest.Status__c == 'Completed');

        bTest = controller.bIsUpdatable;
        bTest = controller.bIsAssigned;

    }

    private static testMethod void saveRequest(){
        
        System.runAs(currentUser){
            assignUserToAdminGroup();
        }   
        
        Create_User_Request__c userRequest = createRequest('Enable Mobile App');
       
        userRequest.Created_User__c = UserInfo.getUserId();
        userRequest.Mobile_App__c = 'APDR';
        
        insert userRequest;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(userRequest);
        ctrlExt_Support_XW_Admin controller = new ctrlExt_Support_XW_Admin(sc);
        
        controller.assignToMe();
        userRequest.Sub_Status__c = 'Solved (Permanently)';
        userRequest.Time_Spent__c = 100;

        controller.saveRequest();

    }    

    private static testMethod void assignToOtherUser(){

        System.runAs(currentUser){
            assignUserToAdminGroup();
        }
                        
        Create_User_Request__c userRequest = createRequest('Enable Mobile App');

        userRequest.Created_User__c = UserInfo.getUserId();
        insert userRequest;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(userRequest);
        ctrlExt_Support_XW_Admin controller = new ctrlExt_Support_XW_Admin(sc);

        System.assert(controller.isAdminUser == true);

        Id id_AdminUser;
        for (Id id_User : mapUser_Admin.keySet()){
            if (id_User != UserInfo.getUserId()){
                id_AdminUser = id_User;
                break;
            }
        }
        if (id_AdminUser != null){
            controller.tAdminUserId = id_AdminUser;
            controller.saveRequest();
        }

    }   

    private static testMethod void saveRequest_Error(){
        
        System.runAs(currentUser){
            assignUserToAdminGroup();
        }   
        
        Create_User_Request__c userRequest = createRequest('Enable Mobile App');
       
        userRequest.Created_User__c = UserInfo.getUserId();
        userRequest.Mobile_App__c = 'APDR';
        
        insert userRequest;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(userRequest);
        ctrlExt_Support_XW_Admin controller = new ctrlExt_Support_XW_Admin(sc);
        
        controller.assignToMe();
        userRequest.Sub_Status__c = 'Solved (Permanently)';
        userRequest.Time_Spent__c = 99999999; //--> Error

        Boolean bError = false;
        try{
            controller.saveRequest();
        }catch(Exception oEX){
            bError = true;
        }

        System.assertEquals(bError, true);

    }    
        
    private static testMethod void rejectRequest(){
    	       
        Create_User_Request__c userRequest = createRequest('Bug');
        userRequest.Description_Long__c = 'descr';                
        insert userRequest;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(userRequest);
        ctrlExt_Support_XW_Admin controller = new ctrlExt_Support_XW_Admin(sc);
                
        controller.reject();
        
        System.assert(userRequest.Status__c == 'Rejected');
    }
    
    private static testMethod void convertChangeRequest(){
    	
    	System.runAs(currentUser){
            assignUserToAdminGroup();
        }	        
        Create_User_Request__c userRequest = createRequest('Change Request');
        userRequest.Description_Long__c = 'descr';
        userRequest.CR_Type__c = 'Bug'; 
        userRequest.Priority__c = 'High';    
        userRequest.Business_Rationale__c = 'business rationale';                 
        insert userRequest;
        
        Blob b = Blob.valueOf('Test Data');  
      
        Attachment attachment = new Attachment();  
        attachment.ParentId = userRequest.Id;  
        attachment.Name = 'Test Attachment for Parent';  
        attachment.Body = b;  
          
        insert attachment; 
        
        userRequest = [Select Id, Name, Assignee__c, CreatedDate, Description_Long__c, Business_Rationale__c, Priority__c, CR_Type__c, Status__c, Request_Type__c, OwnerId from Create_User_Request__c where Id =:userRequest.Id];
                
        ApexPages.StandardController sc = new ApexPages.StandardController(userRequest);
        ctrlExt_Support_XW_Admin controller = new ctrlExt_Support_XW_Admin(sc);
                
        controller.createCR();
        
        System.assert(userRequest.Created_Change_Request__c != null);
        System.assert(userRequest.Status__c == 'Completed');
    }

    private static testMethod void convertChangeRequest_Error(){
        
        System.runAs(currentUser){
            assignUserToAdminGroup();
        }           
        Create_User_Request__c userRequest = createRequest('Change Request');
        userRequest.Description_Long__c = 'descr';
        userRequest.CR_Type__c = 'Bug'; 
        userRequest.Priority__c = 'High';    
        userRequest.Business_Rationale__c = 'business rationale';                 
        insert userRequest;
        
        Blob b = Blob.valueOf('Test Data');  
      
        Attachment attachment = new Attachment();  
        attachment.ParentId = userRequest.Id;  
        attachment.Name = 'Test Attachment for Parent';  
        attachment.Body = b;  
          
        insert attachment; 
        
        userRequest = [Select Id, Assignee__c, CreatedDate, Description_Long__c, Business_Rationale__c, Priority__c, CR_Type__c, Status__c, Request_Type__c, OwnerId from Create_User_Request__c where Id =:userRequest.Id];
                
        ApexPages.StandardController sc = new ApexPages.StandardController(userRequest);
        ctrlExt_Support_XW_Admin controller = new ctrlExt_Support_XW_Admin(sc);
        

        clsUtil.hasException = true;

        Boolean bError = false;
        try{
            controller.createCR();
        }catch(Exception oEX){
            bError = true;
        }

        System.assertEquals(bError, true);

    }    
    
    private static testMethod void convertToServiceRequest(){
    	       
        Create_User_Request__c userRequest = createRequest('Bug');
        userRequest.Description_Long__c = 'descr';                
        insert userRequest;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(userRequest);
        ctrlExt_Support_XW_Admin controller = new ctrlExt_Support_XW_Admin(sc);
                
        controller.convertToSR();
        
        System.assert(userRequest.Request_Type__c == 'Generic Service');
    }

    private static testMethod void convertToServiceRequest_Error(){
               
        Create_User_Request__c userRequest = createRequest('Bug');
        userRequest.Description_Long__c = 'descr';                
        insert userRequest;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(userRequest);
        ctrlExt_Support_XW_Admin controller = new ctrlExt_Support_XW_Admin(sc);
                
        
        clsUtil.hasException = true;

        Boolean bError = false;
        try{
            controller.convertToSR();
        }catch(Exception oEX){
            bError = true;
        }

        System.assertEquals(bError, true);

    }
    
    private static Map<Id, User> mapUser_Admin = new Map<Id, User>();
    private static void assignUserToAdminGroup(){
        
        List<GroupMember> memberships = [Select UserOrGroupId from GroupMember where Group.DeveloperName = 'Support_Portal_Crossworld' AND UserOrGroupId = :UserInfo.getUserId()];
        
        if(memberships.isEmpty()){
        
            Group adminGroup = [Select Id from Group where DeveloperName = 'Support_Portal_Crossworld'];
            
            GroupMember membership = new GroupMember();
            membership.GroupId = adminGroup.Id;
            membership.UserOrGroupId = UserInfo.getUserId();
            
            insert membership;
        } 

        mapUser_Admin = bl_CreateUserRequest.loadAdminUsers('Support_Portal_BI');

    }
    
    private static Create_User_Request__c createRequest(String requestType){
        
        Create_User_Request__c userRequest = new Create_User_Request__c();          
        userRequest.Status__c = 'New';
        userRequest.Application__c = 'Crossworld';
        userRequest.Request_Type__c = requestType;          
        userRequest.Requestor_Email__c = currentUser.Email;     
        
        return userRequest;
    }
}