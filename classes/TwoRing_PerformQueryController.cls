@RestResource(urlMapping='/PerformQuery')
global with sharing class TwoRing_PerformQueryController {
    @HttpPost
    global static List<sObject> performQuery(String query) {
    	try {
    		return Database.query(query);
		}
		catch(QueryException qe) {
    		throw new TwoRing_ApplicationException('Invalid query: ' + qe.getMessage());
		}
		catch(Exception e) {
    		throw new TwoRing_ApplicationException('Error while perfoming query: ' + e.getMessage());
    	}
    }
}