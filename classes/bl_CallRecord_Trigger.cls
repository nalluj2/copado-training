//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 19-08-2016
//  Description      : APEX Class - Business Logic for tr_CallRecord
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public with sharing class bl_CallRecord_Trigger {

    //--------------------------------------------------------------------------------------------------------------------
    // Set the Fiscal_Period__c on Call_Records__c based on the Call_Records__c Call_Date__c and the data in DIB_Fiscal_Period__c
    // This logic replace the multiple Workflows that were created to set the Fiscal_Period__c on Call_Records__c. 
    //--------------------------------------------------------------------------------------------------------------------
    public static void updateFiscalPeriod(List<Call_Records__c> lstTriggerNew, Map<Id, Call_Records__c> mapTriggerOld){

        if (bl_Trigger_Deactivation.isTriggerDeactivated('CallRec_updateFiscalPeriod')) return;

        // Populate a map with the Call Record Call_Date__c and all Call_Records__c with this Call_Date__c
        Map<Date, List<Call_Records__c>> mapCloseDate_CallRecord = new Map<Date, List<Call_Records__c>>();

        if (Trigger.isInsert){

            for (Call_Records__c oCallRecord : lstTriggerNew){

                if (oCallRecord.Call_Date__c != null){
                    List<Call_Records__c> lstCallRecord_Tmp = new List<Call_Records__c>();
                    if (mapCloseDate_CallRecord.containsKey(oCallRecord.Call_Date__c)){
                        lstCallRecord_Tmp = mapCloseDate_CallRecord.get(oCallRecord.Call_Date__c);
                    }
                    lstCallRecord_Tmp.add(oCallRecord);
                    mapCloseDate_CallRecord.put(oCallRecord.Call_Date__c, lstCallRecord_Tmp);
                }

            }

        }else if (Trigger.isUpdate){

            for (Call_Records__c oCallRecord : lstTriggerNew){

                Call_Records__c oCallRecord_Old = mapTriggerOld.get(oCallRecord.Id);

                if (oCallRecord.Call_Date__c == null){

                    oCallRecord.Fiscal_Period__c = null;

                }else if ( 
                    (oCallRecord.Call_Date__c != oCallRecord_Old.Call_Date__c) 
                    || (oCallRecord.Fiscal_Period__c != oCallRecord_Old.Fiscal_Period__c) 
                    || (clsUtil.isBlank(oCallRecord.Fiscal_Period__c)) 
                ){

                    if (oCallRecord.Call_Date__c != null){
                        List<Call_Records__c> lstCallRecord_Tmp = new List<Call_Records__c>();
                        if (mapCloseDate_CallRecord.containsKey(oCallRecord.Call_Date__c)){
                            lstCallRecord_Tmp = mapCloseDate_CallRecord.get(oCallRecord.Call_Date__c);
                        }
                        lstCallRecord_Tmp.add(oCallRecord);
                        mapCloseDate_CallRecord.put(oCallRecord.Call_Date__c, lstCallRecord_Tmp);
                    }

                }
            }
        }

        if (mapCloseDate_CallRecord.keySet().size() > 0){

            Map<Date, String> mapDate_FiscalPeriodLabel = clsUtil.getFiscalPeriodLabel(mapCloseDate_CallRecord.keySet());

            if (mapDate_FiscalPeriodLabel.size() == 0){

                // No DIB Fiscal Period data found for the collected close dates
                // This is executed when all processing Call Records have a CloseData that is not in DIB Fiscal Period
                for (Date dDate : mapCloseDate_CallRecord.keySet()){
                    List<Call_Records__c> lstCallRecord = mapCloseDate_CallRecord.get(dDate);
                    for (Call_Records__c oCallRecord : lstCallRecord){
                        oCallRecord.Fiscal_Period__c = null;
                    }
                }

            }else{

                //  Loop through the collected Call_Date__c and determine the corresponding Fiscal Period Label.
                //  Set for the related Call_Records__c the new Fiscal Period.
                for (Date dDate : mapCloseDate_CallRecord.keySet()){

                    List<Call_Records__c> lstCallRecord = mapCloseDate_CallRecord.get(dDate);

                    if (mapDate_FiscalPeriodLabel.containsKey(dDate)){

                        for (Call_Records__c oCallRecord : lstCallRecord){
                            oCallRecord.Fiscal_Period__c = mapDate_FiscalPeriodLabel.get(dDate);
                        }

                    }else{

                        // No DIB Fiscal Period data found for the collected close dates
                        for (Call_Records__c oCallRecord : lstCallRecord){
                            oCallRecord.Fiscal_Period__c = null;
                        }

                    }

                }

            }

        }

    }    
    //--------------------------------------------------------------------------------------------------------------------
	
	public static void copyProductsToCallContacts(List<Call_Records__c> triggerNew, Map<Id, Call_Records__c> oldMap){
		
		Set<Id> modified = new Set<Id>();
		
		for(Call_Records__c cr : triggerNew){
			
			if(cr.Products_Concatenated__c != oldMap.get(cr.Id).Products_Concatenated__c) modified.add(cr.Id);
		}
		
		if(modified.size() > 0){
			
			List<Contact_Visit_Report__c> callContacts = new List<Contact_Visit_Report__c>();
						
			for(Contact_Visit_Report__c callContact : [Select Id, Products_Concatenated__c, Call_Records__r.Products_Concatenated__c from Contact_Visit_Report__c where Call_Records__c IN :modified]){
				
				callContact.Products_Concatenated__c = callContact.Call_Records__r.Products_Concatenated__c;
				callContacts.add(callContact);
			}
			
			if(callContacts.size() > 0) update callContacts;
		}
	}
}
//--------------------------------------------------------------------------------------------------------------------