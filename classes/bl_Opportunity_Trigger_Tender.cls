public class bl_Opportunity_Trigger_Tender {
    
	private static Set<Id> setID_RecordType_Tender {
	
		get {

			if (setID_RecordType_Tender == null){
			
				setID_RecordType_Tender = new Set<Id>();
				setID_RecordType_Tender.add(clsUtil.getRecordTypeByDevName('Opportunity', 'Business_Critical_Tender').Id);
				setID_RecordType_Tender.add(clsUtil.getRecordTypeByDevName('Opportunity', 'Deal_VID_Non_tender').Id);
				setID_RecordType_Tender.add(clsUtil.getRecordTypeByDevName('Opportunity', 'Small_Tender').Id);

			}
			return setID_RecordType_Tender;
			
		}

		set;
	}
    
    public static void copyBUInfoOnPrevOpportunity(List<Opportunity> triggerNew){
    	
    	if (bl_Trigger_Deactivation.isTriggerDeactivated('Opp_copyBUInfoOnPrevOpportunity')) return;

    	Set<Id> prevOppIds = new Set<Id>();
    	    	
    	for(Opportunity opp : triggerNew){
    		
    		if (!setID_RecordType_Tender.contains(opp.RecordTypeId)) continue;   
    		    		
    		if(opp.Previous_Opportunity__c != null){
    			
    			prevOppIds.add(opp.Previous_Opportunity__c);
    		}    		
    	}
    	
    	if(prevOppIds.size() > 0){
    		
    		Map<Id, Opportunity> prevOppMap = new Map<Id, Opportunity>([Select Id, Business_Unit_Group__c, Business_Unit_msp__c, Sub_Business_Unit__c from Opportunity where Id IN :prevOppIds]);
    		
    		for(Opportunity opp : triggerNew){
    		    		
	    		if(opp.Previous_Opportunity__c != null){
	    			
	    			Opportunity prevOpp = prevOppMap.get(opp.Previous_Opportunity__c);
	    			
	    			opp.Business_Unit_Group__c = prevOpp.Business_Unit_Group__c;
    				opp.Business_Unit_msp__c = prevOpp.Business_Unit_msp__c;
    				opp.Sub_Business_Unit__c = prevOpp.Sub_Business_Unit__c;
	    		}    		
	    	}
    	}	
    }

    public static void copyDataFromPreviousOpportunity(List<Opportunity> lstTriggerNew){
        
    	if (bl_Trigger_Deactivation.isTriggerDeactivated('Opp_copyDataFromPreviousOpportunity')) return;

        Set<Id> setID_PreviousOpportunity = new Set<Id>();
                
        for (Opportunity oOpportunity : lstTriggerNew){
            
            if (!setID_RecordType_Tender.contains(oOpportunity.RecordTypeId)) continue;
                        
            if (oOpportunity.Previous_Opportunity__c != null){
                setID_PreviousOpportunity.add(oOpportunity.Previous_Opportunity__c);
            }           

        }
        
        if (setID_PreviousOpportunity.size() > 0){
            
            Map<Id, Opportunity> mapOpportunity_Previous = new Map<Id, Opportunity>([SELECT Id, Type_of_Tender__c, Type, Indicator__c, Business_Owner__c FROM Opportunity WHERE Id IN :setID_PreviousOpportunity]);
            
            for (Opportunity oOpportunity : lstTriggerNew){
                        
                if (oOpportunity.Previous_Opportunity__c != null){
                    
                    Opportunity oOpportunity_Previous = mapOpportunity_Previous.get(oOpportunity.Previous_Opportunity__c);
                        oOpportunity.Type_of_Tender__c = oOpportunity_Previous.Type_of_Tender__c;
                        oOpportunity.Type = oOpportunity_Previous.Type;
//                        oOpportunity.Indicator__c = oOpportunity_Previous.Indicator__c;
                        oOpportunity.Business_Owner__c = oOpportunity_Previous.Business_Owner__c;

                }           

            }

        }   

    }

                
    public static void updateOnPrevOpportunityChange(List<Opportunity> triggerNew, Map<Id, Opportunity> oldMap){
    	
    	if (bl_Trigger_Deactivation.isTriggerDeactivated('opp_updateOnPrevOpportunityChange')) return;
    	
		Set<Id> setID_RecordType_DealVIDNonTender = new Set<Id>{clsUtil.getRecordTypeByDevName('Opportunity', 'Deal_VID_Non_tender').Id};

    	Set<Id> prevOppIds = new Set<Id>();
    	List<Opportunity> updatedOpps = new List<Opportunity>();
    	
    	for(Opportunity opp : triggerNew){
			
			if (!setID_RecordType_Tender.contains(opp.RecordTypeId)) continue;
			
    		Opportunity oldOpp = oldMap.get(opp.Id);
    		
    		if(opp.Previous_Opportunity__c != oldOpp.Previous_Opportunity__c && opp.Previous_Opportunity__c != null){
    			
    			prevOppIds.add(opp.Previous_Opportunity__c);
    			updatedOpps.add(opp);
    		}   
			 		
    	}
    	
    	if(prevOppIds.size() > 0){
    		
    		Map<Id, Opportunity> prevOppMap = new Map<Id, Opportunity>(
                [
                    SELECT 
                        Id, Contract_Number__c, Current_Extended_To_Date__c, Actual_Last_Possible_Valid_To_Date__c, Current_Potential_Text__c, Estimated_Revenue_Won_Text__c, Opportunity_Country_vs__c, Business_Unit_Group__c, Business_Unit_msp__c, Sub_Business_Unit__c
                        , Type_of_Tender__c, Type, Indicator__c, Business_Owner__c, Contract_End_Date__c
                    FROM 
                        Opportunity 
                    WHERE 
                        Id IN :prevOppIds
                ]);

    		for(Opportunity opp : updatedOpps){
    			
    			Opportunity prevOpp = prevOppMap.get(opp.Previous_Opportunity__c);
    			
    			opp.Current_Contract_Number__c = prevOpp.Contract_Number__c;
    			opp.Current_Potential_Last_Tender__c = prevOpp.Current_Potential_Text__c;
    			opp.Estimated_Revenue_Won_Last_Tender__c = prevOpp.Estimated_Revenue_Won_Text__c;
    			opp.Opportunity_Country_vs__c = prevOpp.Opportunity_Country_vs__c;
    			opp.Business_Unit_Group__c = prevOpp.Business_Unit_Group__c;
    			opp.Business_Unit_msp__c = prevOpp.Business_Unit_msp__c;
    			opp.Sub_Business_Unit__c = prevOpp.Sub_Business_Unit__c;  

                opp.Type_of_Tender__c = prevOpp.Type_of_Tender__c;
                opp.Indicator__c = prevOpp.Indicator__c;
                opp.Business_Owner__c = prevOpp.Business_Owner__c;
                opp.Type = prevOpp.Type;
    			
    			if(prevOpp.Current_Extended_To_Date__c != null){
    				
    				opp.Next_Tender_Expected_Announcement_Date__c = prevOpp.Current_Extended_To_Date__c - 150;
    				opp.Expected_Effective_Date__c = prevOpp.Current_Extended_To_Date__c + 1;
    				opp.CloseDate = prevOpp.Current_Extended_To_Date__c + 1;
    				
    			}else{
    				
    				opp.Next_Tender_Expected_Announcement_Date__c = null;
    				opp.Expected_Effective_Date__c = null;    				
    			}
    			
    			if(prevOpp.Actual_Last_Possible_Valid_To_Date__c != null){
    				
    				opp.Last_Poss_Valid_to_Date_of_Last_Contract__c = prevOpp.Actual_Last_Possible_Valid_To_Date__c + 1;
    				
    			}else{
    				
    				opp.Last_Poss_Valid_to_Date_of_Last_Contract__c = null;
    			}

				if (setID_RecordType_DealVIDNonTender.contains(opp.RecordTypeId)){

    				if (prevOpp.Contract_End_Date__c != null){
    				
    					opp.Next_Tender_Expected_Announcement_Date__c = prevOpp.Contract_End_Date__c.addMonths(-6);
    					opp.Expected_Effective_Date__c = prevOpp.Contract_End_Date__c.addDays(1);
    					opp.CloseDate = prevOpp.Contract_End_Date__c.addDays(1);
    				
    				}else{
					
    					opp.Next_Tender_Expected_Announcement_Date__c = null;
    					opp.Expected_Effective_Date__c = null;

					}

				}

    		}    			

    	}    	

    }
    
    public static void updateNextOpportuniesOnChange(List<Opportunity> triggerNew, Map<Id, Opportunity> oldMap){
    	
        if (bl_Trigger_Deactivation.isTriggerDeactivated('Opp_updateNextOpportuniesOnChange')) return;

    	List<Opportunity> updatedOpps = new List<Opportunity>();
    	
    	for(Opportunity opp : triggerNew){
    		
    		if (!setID_RecordType_Tender.contains(opp.RecordTypeId)) continue;
    		
    		Opportunity oldOpp = oldMap.get(opp.Id);
    		
    		if( (opp.Contract_Number__c != oldOpp.Contract_Number__c && opp.Contract_Number__c != null) ||
    			(opp.Current_Extended_To_Date__c != oldOpp.Current_Extended_To_Date__c && opp.Current_Extended_To_Date__c != null) ||
    			(opp.Actual_Last_Possible_Valid_To_Date__c != oldOpp.Actual_Last_Possible_Valid_To_Date__c && opp.Actual_Last_Possible_Valid_To_Date__c != null) ||
    			(opp.Current_Potential_Text__c != oldOpp.Current_Potential_Text__c && opp.Current_Potential_Text__c != null) ||
    			(opp.Estimated_Revenue_Won_Text__c != oldOpp.Estimated_Revenue_Won_Text__c && opp.Estimated_Revenue_Won_Text__c != null) ||
    			(opp.Opportunity_Country_vs__c != oldOpp.Opportunity_Country_vs__c && opp.Opportunity_Country_vs__c != null) ||
    			(opp.Business_Unit_Group__c != oldOpp.Business_Unit_Group__c && opp.Business_Unit_Group__c != null) ||
    			(opp.Business_Unit_msp__c != oldOpp.Business_Unit_msp__c && opp.Business_Unit_msp__c != null) ||
    			(opp.Sub_Business_Unit__c != oldOpp.Sub_Business_Unit__c && opp.Sub_Business_Unit__c != null) ||

                (opp.Type_of_Tender__c != oldOpp.Type_of_Tender__c && opp.Type_of_Tender__c != null) ||
                (opp.Indicator__c != oldOpp.Indicator__c && opp.Indicator__c != null) ||
                (opp.Business_Owner__c != oldOpp.Business_Owner__c && opp.Business_Owner__c != null) ||
                (opp.Type != oldOpp.Type && opp.Type != null)

    			){
    			    			
    			updatedOpps.add(opp);
    		}    		
    	}
    	
    	if(updatedOpps.size() > 0){
    		
    		Map<Id, Opportunity> oppWithNextOpps = new Map<Id, Opportunity>([Select Id, (Select Id from Opportunities2__r) from Opportunity where Id IN :updatedOpps]);
    		
    		List<Opportunity> toUpdate = new List<Opportunity>();
    		
    		for(Opportunity opp : updatedOpps){
    		
	    		Opportunity oldOpp = oldMap.get(opp.Id);
	    		
	    		List<Opportunity> nextOpps = oppWithNextOpps.get(opp.Id).Opportunities2__r;
	    		
	    		if(nextOpps.isEmpty()) continue;
	    		
	    		if(opp.Contract_Number__c != oldOpp.Contract_Number__c && opp.Contract_Number__c != null){
	    			
	    			for(Opportunity nextOpp : nextOpps){
	    				
	    				nextOpp.Current_Contract_Number__c = opp.Contract_Number__c;
	    			}
	    		}
	    		
	    		if(opp.Current_Extended_To_Date__c != oldOpp.Current_Extended_To_Date__c && opp.Current_Extended_To_Date__c != null){
	    			    			
	    			for(Opportunity nextOpp : nextOpps){
	    					    				
	    				nextOpp.Next_Tender_Expected_Announcement_Date__c = opp.Current_Extended_To_Date__c - 150;
    					nextOpp.Expected_Effective_Date__c = opp.Current_Extended_To_Date__c + 1;
    					nextOpp.CloseDate = opp.Current_Extended_To_Date__c + 1;
	    			}
	    		}
	    		
	    		if(opp.Actual_Last_Possible_Valid_To_Date__c != oldOpp.Actual_Last_Possible_Valid_To_Date__c && opp.Actual_Last_Possible_Valid_To_Date__c != null){
	    			    			
	    			for(Opportunity nextOpp : nextOpps){
	    				   				
    					nextOpp.Last_Poss_Valid_to_Date_of_Last_Contract__c = opp.Actual_Last_Possible_Valid_To_Date__c + 1;    					
	    			}
	    		}
	    		
	    		if(opp.Current_Potential_Text__c != oldOpp.Current_Potential_Text__c && opp.Current_Potential_Text__c != null){
	    			
	    			for(Opportunity nextOpp : nextOpps){
	    				
	    				nextOpp.Current_Potential_Last_Tender__c = opp.Current_Potential_Text__c;
	    			}
	    		}
	    		
	    		if(opp.Estimated_Revenue_Won_Text__c != oldOpp.Estimated_Revenue_Won_Text__c && opp.Estimated_Revenue_Won_Text__c != null){
	    			
	    			for(Opportunity nextOpp : nextOpps){
	    				
	    				nextOpp.Estimated_Revenue_Won_Last_Tender__c = opp.Estimated_Revenue_Won_Text__c;
	    			}
	    		}
	    		
	    		if(opp.Opportunity_Country_vs__c != oldOpp.Opportunity_Country_vs__c && opp.Opportunity_Country_vs__c != null){
	    			
	    			for(Opportunity nextOpp : nextOpps){
	    				
	    				nextOpp.Opportunity_Country_vs__c = opp.Opportunity_Country_vs__c;
	    			}
	    		}
	    		
	    		if(opp.Business_Unit_Group__c != oldOpp.Business_Unit_Group__c && opp.Business_Unit_Group__c != null){
	    			
	    			for(Opportunity nextOpp : nextOpps){
	    				
	    				nextOpp.Business_Unit_Group__c = opp.Business_Unit_Group__c;
	    			}
	    		}
	    		
	    		if(opp.Business_Unit_msp__c != oldOpp.Business_Unit_msp__c && opp.Business_Unit_msp__c != null){
	    			
	    			for(Opportunity nextOpp : nextOpps){
	    				
	    				nextOpp.Business_Unit_msp__c = opp.Business_Unit_msp__c;
	    			}
	    		}
	    		
	    		if(opp.Sub_Business_Unit__c != oldOpp.Sub_Business_Unit__c && opp.Sub_Business_Unit__c != null){
	    			
	    			for(Opportunity nextOpp : nextOpps){
	    				
	    				nextOpp.Sub_Business_Unit__c = opp.Sub_Business_Unit__c;
	    			}
	    		}

                if(opp.Type_of_Tender__c != oldOpp.Type_of_Tender__c && opp.Type_of_Tender__c != null){
                    
                    for(Opportunity nextOpp : nextOpps){
                        nextOpp.Type_of_Tender__c = opp.Type_of_Tender__c;
                    }

                }

                if(opp.Indicator__c != oldOpp.Indicator__c && opp.Indicator__c != null){
                    
                    for(Opportunity nextOpp : nextOpps){
                        nextOpp.Indicator__c = opp.Indicator__c;
                    }

                }

                if(opp.Business_Owner__c != oldOpp.Business_Owner__c && opp.Business_Owner__c != null){
                    
                    for(Opportunity nextOpp : nextOpps){
                        nextOpp.Business_Owner__c = opp.Business_Owner__c;
                    }

                }
	    		
                if(opp.Type != oldOpp.Type && opp.Type != null){
                    
                    for(Opportunity nextOpp : nextOpps){
                        nextOpp.Type = opp.Type;
                    }

                }

	    		toUpdate.addAll(nextOpps);    		
	    	}	
	    	
	    	update toUpdate;
    	}
    }


    public static void copyOppCountryToText(List<Opportunity> triggerNew){
    	
    	if (bl_Trigger_Deactivation.isTriggerDeactivated('opp_tender_copyOppCountryToText')) return;

    	Set<String> countryNames = new Set<String>();
    	   	    	
    	for(Opportunity opp : triggerNew){
    		
    		if (!setID_RecordType_Tender.contains(opp.RecordTypeId)) continue;
    		
    		if(opp.Opportunity_Country_vs__c != null) countryNames.addAll(opp.Opportunity_Country_vs__c.split(';'));
    		else opp.Opportunity_Country_Text__c = null;
    	}
    	
    	if(countryNames.size() > 0){
    		
    		Map<String, String> countryCodeMapping = new Map<String, String>();
    		
    		for(Dib_Country__c country : [Select Name, Country_ISO_Code__c from Dib_Country__c where Name IN :countryNames]){
    			
    			countryCodeMapping.put(country.Name.toUpperCase(), country.Country_ISO_Code__c);
    		}
    		
    		for(Opportunity opp : triggerNew){
    		
	    		if (!setID_RecordType_Tender.contains(opp.RecordTypeId)) continue;
	    		
	    		if(opp.Opportunity_Country_vs__c != null){
	    			
	    			List<String> countryCodes = new List<String>();
	    			
	    			for(String countryName : opp.Opportunity_Country_vs__c.split(';')){
	    			
	    				String countryCode = countryCodeMapping.get(countryName.toUpperCase());
	    				if(countryCode != null) countryCodes.add(countryCode);
	    			}
	    			
	    			opp.Opportunity_Country_Text__c = String.join(countryCodes, ';');
	    		}
	    	}
    	}
    }
    
    private static List<String> oppLotFields {
    	
    	get{
    		
    		if(oppLotFields == null){
	    	
	    		oppLotFields = new List<String>();
		        oppLotFields.add('name');
		        oppLotFields.add('currencyisocode');
		        
	    		Map<String, Schema.SObjectField> describeMap = Schema.SObjectType.Opportunity_Lot__c.fields.getMap();
	        	
		        for(String oppLotField : describeMap.keySet()){
		        	
		        	Schema.DescribeFieldResult fDescribe = describeMap.get(oppLotfield).getDescribe();
		        	
		        	if(fDescribe.isCustom() && fDescribe.isCalculated() == false) oppLotFields.add(oppLotField);      	
		        }        	
    		}
    		
    		return oppLotFields;
    	}
    	
    	set;
    }
    
    private static List<String> oppAccountFields {
    	
    	get{
    		
    		if(oppAccountFields == null){
	    	
	    		oppAccountFields = new List<String>();		        
		        oppAccountFields.add('currencyisocode');
		        
	    		Map<String, Schema.SObjectField> describeMap = Schema.SObjectType.Opportunity_Account__c.fields.getMap();
	        	
		        for(String oppAccountField : describeMap.keySet()){
		        	
		        	Schema.DescribeFieldResult fDescribe = describeMap.get(oppAccountField).getDescribe();
		        	
		        	if(fDescribe.isCustom() && fDescribe.isCalculated() == false) oppAccountFields.add(oppAccountField);      	
		        }        	
    		}
    		
    		return oppAccountFields;
    	}
    	
    	set;
    }
    
    private static List<String> oppTeamFields {
    	
    	get{
    		
    		if(oppTeamFields == null){
	    	
	    		oppTeamFields = new List<String>();
		        oppTeamFields.add('UserId');
		        oppTeamFields.add('TeamMemberRole');
		        oppTeamFields.add('OpportunityAccessLevel');
		      		        
	    		Map<String, Schema.SObjectField> describeMap = Schema.SObjectType.OpportunityTeamMember.fields.getMap();
	        	
		        for(String oppTeamField : describeMap.keySet()){
		        	
		        	Schema.DescribeFieldResult fDescribe = describeMap.get(oppTeamField).getDescribe();
		        	
		        	if(fDescribe.isCustom() && fDescribe.isCalculated() == false) oppTeamFields.add(oppTeamField);      	
		        }        	
    		}
    		
    		return oppTeamFields;
    	}
    	
    	set;
    }
    
    private static List<String> stakeholderFields {
    	
    	get{
    		
    		if(stakeholderFields == null){
	    	
	    		stakeholderFields = new List<String>();
		        stakeholderFields.add('CurrencyIsoCode');
		        stakeholderFields.add('RecordTypeId');
		        		      		        
	    		Map<String, Schema.SObjectField> describeMap = Schema.SObjectType.Stakeholder_Mapping__c.fields.getMap();
	        	
		        for(String stakeholderField : describeMap.keySet()){
		        	
		        	Schema.DescribeFieldResult fDescribe = describeMap.get(stakeholderField).getDescribe();
		        	
		        	if(fDescribe.isCustom() && fDescribe.isCalculated() == false) stakeholderFields.add(stakeholderField);      	
		        }        	
    		}
    		
    		return stakeholderFields;
    	}
    	
    	set;
    }
    
    public static void deepCloneTenderOpportunity(List<Opportunity> triggerNew){
    	
    	if (bl_Trigger_Deactivation.isTriggerDeactivated('Opp_deepCloneTenderOpportunity')) return;

    	Set<Id> clonedFromIds = new Set<Id>();
    	
    	for(Opportunity opp : triggerNew){
    		
    		// Only Tender Opportunities with a value in Cloned From field 
			if (!setID_RecordType_Tender.contains(opp.RecordTypeId)) continue;

    		if (opp.Cloned_From__c != null){
    		
    			clonedFromIds.add(opp.Cloned_From__c);

    		}
    	}
    	
    	if(clonedFromIds.size() > 0){
    		
    		String oppQuery = 'Select Id, ';
    		oppQuery += '(Select ' + String.join(oppLotFields, ',') + ' from Opportunity_Lots__r), ';
    		oppQuery += '(Select ' + String.join(oppTeamFields, ',') + ' from OpportunityTeamMembers), ';
    		oppQuery += '(Select ' + String.join(oppAccountFields, ',') + ' from Opportunity_Accounts__r), ';
    		oppQuery += '(Select ' + String.join(stakeholderFields, ',') + ' from Stakeholder_Mapping__r), ';
    		oppQuery += '(Select Body, Title, IsPrivate from Notes where isPrivate = false), ';
    		oppQuery += '(Select Id from Attachments where BodyLength <= 5242880 AND IsPrivate = false) ';
    		oppQuery += 'from Opportunity where Id IN :clonedFromIds';
    		
    		Map<Id, Opportunity> clonedFromMap = new Map<Id, Opportunity>((List<Opportunity>) Database.query(oppQuery));
    		Map<Id, Attachment> cloneFromAttachmentsMap = new Map<Id, Attachment>([Select Body, ContentType, Description, IsPrivate, Name from Attachment where ParentId IN :clonedFromIds AND BodyLength <= 5242880 AND IsPrivate = false]);
    		
    		List<Opportunity_Lot__c> clonedOppLots = new List<Opportunity_Lot__c>();
    		List<OpportunityTeamMember> clonedTeamMembers = new List<OpportunityTeamMember>();
    		List<Opportunity_Account__c> clonedOppAccounts = new List<Opportunity_Account__c>();
    		List<Stakeholder_Mapping__c> clonedStokeholders = new List<Stakeholder_Mapping__c>();
    		List<Note> clonedNotes = new List<Note>();
    		List<Attachment> clonedAttachments = new List<Attachment>();
    		
    		for(Opportunity opp : triggerNew){
    		
    			// Only Tender Opportunities with a value in Cloned From field 
				if (!setID_RecordType_Tender.contains(opp.RecordTypeId)) continue;
    			
				if (opp.Cloned_From__c != null){
    				
    				Opportunity clonedFrom = clonedFromMap.get(opp.Cloned_From__c);
    				
    				for(Opportunity_Lot__c oppLot : clonedFrom.Opportunity_Lots__r){
    					
    					Opportunity_Lot__c clonedOppLot = oppLot.clone();
    					clonedOppLot.Opportunity__c = opp.Id;
    					    					
    					clonedOppLots.add(clonedOppLot);
    				}
    				
    				for(OpportunityTeamMember oppTeamMember : clonedFrom.OpportunityTeamMembers){
    					
    					OpportunityTeamMember clonedOppTeamMember = oppTeamMember.clone();
    					clonedOppTeamMember.OpportunityId = opp.Id;
    					clonedOppTeamMember.MITG_Legacy_ID__c = null;
    					
    					clonedTeamMembers.add(clonedOppTeamMember);
    				}
    				
    				for(Opportunity_Account__c oppAccount : clonedFrom.Opportunity_Accounts__r){
    					
    					Opportunity_Account__c clonedOppAccount = oppAccount.clone();
    					clonedOppAccount.Opportunity__c = opp.Id;
    					clonedOppAccount.Unique_Key__c = null;
						clonedOppAccount.Mobile_ID__c = null;
    					
    					clonedOppAccounts.add(clonedOppAccount);
    				}
    				
    				for(Stakeholder_Mapping__c stakeholderMapping : clonedFrom.Stakeholder_Mapping__r){
    					
    					Stakeholder_Mapping__c clonedStakeholder = stakeholderMapping.clone();
    					clonedStakeholder.Opportunity__c = opp.Id;
    					clonedStakeholder.MITG_Legacy_ID__c = null;
    					clonedStakeholder.Mobile_ID__c = null;
    					
    					
    					clonedStokeholders.add(clonedStakeholder);
    				}
    				
    				for(Note oppNote : clonedFrom.Notes){
    					
    					Note clonedNote = oppNote.clone();
    					clonedNote.ParentId = opp.Id;
    					
    					clonedNotes.add(clonedNote);
    				}
    				
    				for(Attachment oppAttachment : clonedFrom.Attachments){
    					
    					Attachment fullAttachment = cloneFromAttachmentsMap.get(oppAttachment.Id);
    					Attachment clonedAttachment = fullAttachment.clone();
    					clonedAttachment.ParentId = opp.Id;
    					
    					clonedAttachments.add(clonedAttachment);
    				}
    			}
    		}
    		
    		if(clonedOppLots.size() > 0) insert clonedOppLots;
    		if(clonedTeamMembers.size() > 0) insert clonedTeamMembers;
    		if(clonedOppAccounts.size() > 0) insert clonedOppAccounts;
    		if(clonedStokeholders.size() > 0) insert clonedStokeholders;
    		if(clonedNotes.size() > 0) insert clonedNotes;
    		if(clonedAttachments.size() > 0) insert clonedAttachments;
    	}
    }


	public static void copyExpectedEffectiveDate_To_CloseDate(List<Opportunity> triggerNew){
		
        if (bl_Trigger_Deactivation.isTriggerDeactivated('opp_copyExpectedEffectiveDate')) return;

	    for (Opportunity oOpportunity : triggerNew){
	    		
			if (!setID_RecordType_Tender.contains(oOpportunity.RecordTypeId)) continue;

			if ( (oOpportunity.Expected_Effective_Date__c != null) && (oOpportunity.Expected_Effective_Date__c != oOpportunity.CloseDate) ){

				oOpportunity.CloseDate = oOpportunity.Expected_Effective_Date__c;

			}

		}
		
	}

}