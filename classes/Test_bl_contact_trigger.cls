@IsTest
private class Test_bl_contact_trigger {
    
   private static testmethod void testCreateContact_Vascular(){
        
		clsTestData_User.tUserBusinessUnit = 'Vascular';
        User oUser = clsTestData_User.createUser('tstusr1', true);
        
        System.runAs(oUser){
            
            Contact cnt = createDummyContact();
            
            cnt = [Select Vascular__c from Contact where Id = :cnt.Id];
            System.assert(cnt.Vascular__c == true);

        }

    }

    private static testmethod void testCreateContact_CS_SH(){
        
		clsTestData_User.tUserBusinessUnit = 'CS & SH';
        User oUser = clsTestData_User.createUser('tstusr1', true);
        
        System.runAs(oUser){
            
            Contact cnt = createDummyContact();
            
            cnt = [Select CS_SH__c from Contact where Id = :cnt.Id];
            System.assert(cnt.CS_SH__c == true);

        }

    }

    private static testmethod void testCreateContact_CranialSpinal(){
        
		clsTestData_User.tUserBusinessUnit = 'Cranial Spinal';
        User oUser = clsTestData_User.createUser('tstusr1', true);
        
        System.runAs(oUser){
            
            Contact cnt = createDummyContact();
            
            cnt = [Select Cranial_and_Spinal__c from Contact where Id = :cnt.Id];
            System.assert(cnt.Cranial_and_Spinal__c == true);

        }

    }
            
    private static testmethod void testCreateContact_CRHF(){
        
		clsTestData_User.tUserBusinessUnit = 'CRHF';
        User oUser = clsTestData_User.createUser('tstusr1', true);
        
        System.runAs(oUser){
            
            Contact cnt = createDummyContact();
            
            cnt = [Select CRHF__c from Contact where Id = :cnt.Id];
            System.assert(cnt.CRHF__c == true);         

        }

    }
    
    private static testmethod void testCreateContact_Diabetes(){
        
		clsTestData_User.tUserBusinessUnit = 'Diabetes';
        User oUser = clsTestData_User.createUser('tstusr1', true);
        
        System.runAs(oUser){
            
            Contact cnt = createDummyContact();
            
            cnt = [Select Diabetes__c from Contact where Id = :cnt.Id];
            System.assert(cnt.Diabetes__c == true);         

        }

    }
    
    private static testmethod void testCreateContact_EarlyTechnologies(){
        
		clsTestData_User.tUserBusinessUnit = 'Gastrointestinal & Hepatology';
        User oUser = clsTestData_User.createUser('tstusr1', true);
        
        System.runAs(oUser){
            
            Contact cnt = createDummyContact();
            
            cnt = [Select Early_Technologies__c from Contact where Id = :cnt.Id];
            System.assert(cnt.Early_Technologies__c == true);

        }

    }

    private static testmethod void testCreateContact_ENT(){
        
		clsTestData_User.tUserBusinessUnit = 'ENT';
        User oUser = clsTestData_User.createUser('tstusr1', true);
        
        System.runAs(oUser){
            
            Contact cnt = createDummyContact();
            
            cnt = [Select ENT__c from Contact where Id = :cnt.Id];
            System.assert(cnt.ENT__c == true);

        }

    }
    
    private static testmethod void testCreateContact_IHS(){
        
		clsTestData_User.tUserBusinessUnit = 'IHS';
        User oUser = clsTestData_User.createUser('tstusr1', true);
        
        System.runAs(oUser){
            
            Contact cnt = createDummyContact();
            
            cnt = [Select IHS__c from Contact where Id = :cnt.Id];
            System.assert(cnt.IHS__c == true);

        }

    }

    private static testmethod void testCreateContact_Neurovascular(){
        
		clsTestData_User.tUserBusinessUnit = 'Neurovascular';
        User oUser = clsTestData_User.createUser('tstusr1', true);
        
        System.runAs(oUser){
            
            Contact cnt = createDummyContact();
            
            cnt = [Select Neurovascular__c from Contact where Id = :cnt.Id];
            System.assert(cnt.Neurovascular__c == true);

        }

    }

    private static testmethod void testCreateContact_Pain(){
        
		clsTestData_User.tUserBusinessUnit = 'Pain';
        User oUser = clsTestData_User.createUser('tstusr1', true);
        
        System.runAs(oUser){
            
            Contact cnt = createDummyContact();
            
            cnt = [Select Pain__c from Contact where Id = :cnt.Id];
            System.assert(cnt.Pain__c == true);

        }

    }
    
    private static testmethod void testCreateContact_PMR(){
        
		clsTestData_User.tUserBusinessUnit = 'Respiratory & Monitoring Solutions';
        User oUser = clsTestData_User.createUser('tstusr1', true);
        
        System.runAs(oUser){
            
            Contact cnt = createDummyContact();
            
            cnt = [Select Patient_Monitoring_Recovery__c from Contact where Id = :cnt.Id];
            System.assert(cnt.Patient_Monitoring_Recovery__c == true);

        }

    }
    
    private static testmethod void testCreateContact_PelvicHealth(){
        
		clsTestData_User.tUserBusinessUnit = 'Pelvic Health';
        User oUser = clsTestData_User.createUser('tstusr1', true);
        
        System.runAs(oUser){
            
            Contact cnt = createDummyContact();
            
            cnt = [Select Pelvic_Health__c from Contact where Id = :cnt.Id];
            System.assert(cnt.Pelvic_Health__c == true);

        }

    }

    private static testmethod void testCreateContact_RTG_IHS(){
        
		clsTestData_User.tUserBusinessUnit = 'RTG IHS';
        User oUser = clsTestData_User.createUser('tstusr1', true);
        
        System.runAs(oUser){
            
            Contact cnt = createDummyContact();
            
            cnt = [Select RTG_IHS__c from Contact where Id = :cnt.Id];
            System.assert(cnt.RTG_IHS__c == true);

        }

    }

    private static testmethod void testCreateContact_RenalCareSolutions(){
        
		clsTestData_User.tUserBusinessUnit = 'Renal Care Solutions';
        User oUser = clsTestData_User.createUser('tstusr1', true);
        
        System.runAs(oUser){
            
            Contact cnt = createDummyContact();
            
            cnt = [Select Renal_Care_Solutions__c from Contact where Id = :cnt.Id];
            System.assert(cnt.Renal_Care_Solutions__c == true);

        }

    }
    
    private static testmethod void testCreateContact_SurgicalInnovations(){
        
		clsTestData_User.tUserBusinessUnit = 'Surgical Innovations';
        User oUser = clsTestData_User.createUser('tstusr1', true);
        
        System.runAs(oUser){
            
            Contact cnt = createDummyContact();
            
            cnt = [Select Surgical_Innovations__c from Contact where Id = :cnt.Id];
            System.assert(cnt.Surgical_Innovations__c == true);

        }

    }

    private static testmethod void testCreateContact_Robotics(){
        
		clsTestData_User.tUserBusinessUnit = 'Robotics';
        User oUser = clsTestData_User.createUser('tstusr1', true);
        
        System.runAs(oUser){
            
            Contact cnt = createDummyContact();
            
            cnt = [Select Robotics__c from Contact where Id = :cnt.Id];
            System.assert(cnt.Robotics__c == true);

        }

    }

            
    private static testmethod void testContactCheckBUFlags(){
        
		clsTestData_User.tUserBusinessUnit = 'All';
        User oUser = clsTestData_User.createUser('tstusr1', true);
        
        System.runAs(oUser){
            
            Contact cnt = createDummyContact();
            
            cnt = 
                [
                    SELECT 
                        Cranial_and_Spinal__c, CRHF__c, Diabetes__c, Early_Technologies__c, ENT__c, IHS__c, Neurovascular__c, Pain__c
                        , Patient_Monitoring_Recovery__c, Pelvic_Health__c, Renal_Care_Solutions__c, RTG_IHS__c, Surgical_Innovations__c 
						, Vascular__c, CS_SH__c, Robotics__c
                    FROM Contact 
                    WHERE Id = :cnt.Id
                ];
            System.assert(cnt.Cranial_and_Spinal__c == false);
            System.assert(cnt.CRHF__c == false);
            System.assert(cnt.Diabetes__c == false);
            System.assert(cnt.Early_Technologies__c == false);
            System.assert(cnt.ENT__c == false);
            System.assert(cnt.IHS__c == false);         
            System.assert(cnt.Neurovascular__c == false);
            System.assert(cnt.Pain__c == false);
            System.assert(cnt.Patient_Monitoring_Recovery__c == false);
            System.assert(cnt.Pelvic_Health__c == false);
            System.assert(cnt.Renal_Care_Solutions__c == false);
            System.assert(cnt.RTG_IHS__c == false);
            System.assert(cnt.Surgical_Innovations__c == false);
            System.assert(cnt.Vascular__c == false);
            System.assert(cnt.CS_SH__c == false);
			System.assert(cnt.Robotics__c == false);

            
                cnt.Cranial_and_Spinal__c = true;
                cnt.CRHF__c = true;
                cnt.Diabetes__c = true;
                cnt.Early_Technologies__c = true;
                cnt.ENT__c = true;
                cnt.IHS__c = true;  
                cnt.Neurovascular__c = true;
                cnt.Pain__c = true;
                cnt.Patient_Monitoring_Recovery__c = true;
                cnt.Pelvic_Health__c = true;
				cnt.Renal_Care_Solutions__c = true;
                cnt.RTG_IHS__c = true;
                cnt.Surgical_Innovations__c = true;
                cnt.Vascular__c = true;
                cnt.CS_SH__c = true;
				cnt.Robotics__c = true;

            update cnt;
            
            cnt = 
                [
                    SELECT 
                        Cranial_and_Spinal__c, CRHF__c, Diabetes__c, Early_Technologies__c, ENT__c, IHS__c, Neurovascular__c, Pain__c
                        , Patient_Monitoring_Recovery__c, Pelvic_Health__c, Renal_Care_Solutions__c, RTG_IHS__c, Surgical_Innovations__c 
						, Vascular__c, CS_SH__c, Robotics__c
                    FROM Contact 
                    WHERE Id = :cnt.Id
                ];
            System.assert(cnt.Cranial_and_Spinal__c == true);
            System.assert(cnt.CRHF__c == true);
            System.assert(cnt.Diabetes__c == true);
            System.assert(cnt.Early_Technologies__c == true);
            System.assert(cnt.ENT__c == true);
            System.assert(cnt.IHS__c == true);         
            System.assert(cnt.Neurovascular__c == true);
            System.assert(cnt.Pain__c == true);
            System.assert(cnt.Patient_Monitoring_Recovery__c == true);
            System.assert(cnt.Pelvic_Health__c == true);
			System.assert(cnt.Renal_Care_Solutions__c == true);
            System.assert(cnt.RTG_IHS__c == true);
            System.assert(cnt.Surgical_Innovations__c == true);
            System.assert(cnt.Vascular__c == true);
            System.assert(cnt.CS_SH__c == true);
			System.assert(cnt.Robotics__c == true);

        }
    }
    
    private static testmethod void testANZContact(){
        
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.SAP_Active__c = true;
        acc.SAP_Id__c = '123456789';            
        insert acc;
        
        Contact cnt = new Contact();
        cnt.RecordTypeId = [Select Id from RecordType where SObjectType = 'Contact' AND DeveloperName = 'ANZ_Contact'].Id;
        cnt.Salutation = 'Mr';
        cnt.LastName = 'TEST Contact ';  
        cnt.FirstName = 'TEST';  
        cnt.AccountId = acc.Id;  
        cnt.Phone = '001222333'; 
        cnt.Email ='unit.test@medtronic.com';
        cnt.Contact_Department__c = 'Diabetes Adult'; 
        cnt.Contact_Primary_Specialty__c = 'ENT';
        cnt.Affiliation_To_Account__c = 'Employee';
        cnt.Primary_Job_Title_vs__c = 'Manager';
        cnt.Contact_Gender__c = 'Male';
        insert cnt; 
        
        cnt = [Select Title from Contact where Id = :cnt.Id];
        System.assert(cnt.Title == 'Mr');
    }
    
    private static testmethod void testEmployeeContact(){
        
        Account acc = new Account();
        acc.Name = 'Medtronic';     
        acc.RecordTypeId = [Select Id from RecordType where SObjectType = 'Account' AND DeveloperName = 'MDT_Account'].Id;          
        insert acc;
        
        Contact cnt = new Contact();
        cnt.RecordTypeId = [Select Id from RecordType where SObjectType = 'Contact' AND DeveloperName = 'MDT_Employee'].Id;        
        cnt.LastName = 'TEST Contact ';  
        cnt.FirstName = 'TEST';  
        cnt.AccountId = acc.Id;
        cnt.SFDC_User__c = UserInfo.getUserId();
        insert cnt; 
        
        cnt = [Select Email, Job_Title_Description__c, Manager__c, MobilePhone, Phone from Contact where Id = :cnt.Id];
        User currentUser = [Select Email, Job_Title_vs__c, Manager.FirstName, Manager.LastName, MobilePhone, Phone from User where Id = :UserInfo.getUserId()];
        System.assert(cnt.Email == currentUser.Email);
        System.assert(cnt.Job_Title_Description__c == currentUser.Job_Title_vs__c);
        if(currentUser.Manager != null) System.assert(cnt.Manager__c == currentUser.Manager.FirstNAme + ' ' + currentUser.Manager.LastName);
        else System.assert(cnt.Manager__c == null);
        System.assert(cnt.MobilePhone == currentUser.MobilePhone);
        System.assert(cnt.Phone == currentUser.Phone);
    }
    
    private static testmethod void testHCP_Id(){
        
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.SAP_Active__c = true;
        acc.SAP_Id__c = '123456789';            
        insert acc;
        
        Contact cnt = new Contact();
        cnt.RecordTypeId = [Select Id from RecordType where SObjectType = 'Contact' AND DeveloperName = 'Generic_Contact'].Id;
        cnt.LastName = 'TEST Contact ';  
        cnt.FirstName = 'TEST';  
        cnt.AccountId = acc.Id;  
        cnt.Phone = '001222333'; 
        cnt.Email ='unit.test@medtronic.com';
        cnt.Contact_Department__c = 'Diabetes Adult'; 
        cnt.Contact_Primary_Specialty__c = 'ENT';
        cnt.Affiliation_To_Account__c = 'Employee';
        cnt.Primary_Job_Title_vs__c = 'Manager';
        cnt.Contact_Gender__c = 'Male';
        insert cnt;
        
        cnt = [Select Id, HCP_Unique_ID__c from Contact where Id = :cnt.Id];
        System.assert(cnt.HCP_Unique_ID__c == cnt.Id);
        
        Account MDTacc = new Account();
        MDTacc.Name = 'Medtronic';      
        MDTacc.RecordTypeId = [Select Id from RecordType where SObjectType = 'Account' AND DeveloperName = 'MDT_Account'].Id;           
        insert MDTacc;
        
        cnt.RecordTypeId = [Select Id from RecordType where SObjectType = 'Contact' AND DeveloperName = 'MDT_Employee'].Id;
        cnt.AccountId = MDTacc.Id;  
        cnt.Primary_Job_Title_vs__c = 'District Manager';
        cnt.SFDC_User__c = UserInfo.getUserId();
        update cnt;
        
        cnt = [Select Id, HCP_Unique_ID__c from Contact where Id = :cnt.Id];
        System.assert(cnt.HCP_Unique_ID__c == null);
    }
        
    private static testmethod void testContactDeleteInfo_Delete(){
        
        Integrate_With_MMX_Countries__c belgium = new Integrate_With_MMX_Countries__c(Name = 'BELGIUM');
        insert belgium;
                   
        User dataSteward = [Select Id from User where Profile.Name = 'EUR Contact Data Steward' AND isactive = true LIMIT 1];          
        
        System.runAs(dataSteward){
                        
            Contact cnt = createDummyContact();
        
            Test.startTest();
        
            delete cnt;
            
            Contact_Delete_Info__c deleteInfo = [Select Id, Type__c, Undeleted_Bool__c  from Contact_Delete_Info__c where Deleted_Contact_SFDC_ID__c = :cnt.Id];
            System.assert(deleteInfo.Type__c == 'Delete');
            System.assert(deleteInfo.Undeleted_Bool__c == false);
            
            undelete cnt;
            
            deleteInfo = [Select Id, Undeleted_Bool__c from Contact_Delete_Info__c where Id = :deleteInfo.Id];
            System.assert(deleteInfo.Undeleted_Bool__c == true);
            
            delete cnt;
            
            deleteInfo = [Select Id, Type__c, Undeleted_Bool__c  from Contact_Delete_Info__c where Deleted_Contact_SFDC_ID__c = :cnt.Id];
            System.assert(deleteInfo.Type__c == 'Delete');
            System.assert(deleteInfo.Undeleted_Bool__c == false);
        }
    }
    
    private static testmethod void testContactDeleteInfo_Merge(){
        
        Integrate_With_MMX_Countries__c belgium = new Integrate_With_MMX_Countries__c(Name = 'BELGIUM');
        insert belgium;
        
        User dataSteward = [Select Id from User where Profile.Name = 'EUR Contact Data Steward' AND isactive = true LIMIT 1];          
        
        System.runAs(dataSteward){
            
            Account acc = new Account();
            acc.Name = 'Test Account';
            acc.SAP_Active__c = true;
            acc.SAP_Id__c = '123456789';
            acc.Account_Country_vs__c = 'BELGIUM';         
            insert acc;
            
            Contact cnt1 = new Contact();
            cnt1.RecordTypeId = [Select Id from RecordType where SObjectType = 'Contact' AND DeveloperName = 'Generic_Contact'].Id;
            cnt1.LastName = 'TEST Contact 1';  
            cnt1.FirstName = 'TEST';  
            cnt1.AccountId = acc.Id;  
            cnt1.Phone = '001222333'; 
            cnt1.Email ='unit.test1@medtronic.com';
            cnt1.Contact_Department__c = 'Diabetes Adult'; 
            cnt1.Contact_Primary_Specialty__c = 'ENT';
            cnt1.Affiliation_To_Account__c = 'Employee';
            cnt1.Primary_Job_Title_vs__c = 'Manager';
            cnt1.Contact_Gender__c = 'Male';
            
            Contact cnt2 = new Contact();
            cnt2.RecordTypeId = [Select Id from RecordType where SObjectType = 'Contact' AND DeveloperName = 'Generic_Contact'].Id;
            cnt2.LastName = 'TEST Contact 2';  
            cnt2.FirstName = 'TEST';  
            cnt2.AccountId = acc.Id;  
            cnt2.Phone = '001222333'; 
            cnt2.Email ='unit.test2@medtronic.com';
            cnt2.Contact_Department__c = 'Diabetes Adult'; 
            cnt2.Contact_Primary_Specialty__c = 'ENT';
            cnt2.Affiliation_To_Account__c = 'Employee';
            cnt2.Primary_Job_Title_vs__c = 'Manager';
            cnt2.Contact_Gender__c = 'Male';
            
            insert new List<Contact>{cnt1, cnt2};
            
            Test.startTest();
            
            delete cnt2;
            
            Contact_Delete_Info__c deleteInfo = [Select Id, Type__c, Undeleted_Bool__c  from Contact_Delete_Info__c where Deleted_Contact_SFDC_ID__c = :cnt2.Id];
            System.assert(deleteInfo.Type__c == 'Delete');
            System.assert(deleteInfo.Undeleted_Bool__c == false);
            
            undelete cnt2;
            
            deleteInfo = [Select Id, Type__c, Undeleted_Bool__c  from Contact_Delete_Info__c where Deleted_Contact_SFDC_ID__c = :cnt2.Id];
            System.assert(deleteInfo.Type__c == 'Delete');
            System.assert(deleteInfo.Undeleted_Bool__c == true);
            
            merge cnt1 cnt2;
            
            deleteInfo = [Select Id, Type__c, Undeleted_Bool__c  from Contact_Delete_Info__c where Deleted_Contact_SFDC_ID__c = :cnt2.Id];
            System.assert(deleteInfo.Type__c == 'Merge');
            System.assert(deleteInfo.Undeleted_Bool__c == false);
            
            Test.stopTest();
            
        }
    }
    
    private static testmethod void testContactDeleteInfo_DeleteAndMerge(){
        
        Integrate_With_MMX_Countries__c belgium = new Integrate_With_MMX_Countries__c(Name = 'BELGIUM');
        insert belgium;
        
        User dataSteward = [Select Id from User where Profile.Name = 'EUR Contact Data Steward' AND isactive = true LIMIT 1];          
        
        System.runAs(dataSteward){
            
            Account acc = new Account();
            acc.Name = 'Test Account';
            acc.SAP_Active__c = true;
            acc.SAP_Id__c = '123456789';                
            insert acc;
            
            Contact cnt1 = new Contact();
            cnt1.RecordTypeId = [Select Id from RecordType where SObjectType = 'Contact' AND DeveloperName = 'Generic_Contact'].Id;
            cnt1.LastName = 'TEST Contact 1';  
            cnt1.FirstName = 'TEST';  
            cnt1.AccountId = acc.Id;  
            cnt1.Phone = '001222333'; 
            cnt1.Email ='unit.test1@medtronic.com';
            cnt1.Contact_Department__c = 'Diabetes Adult'; 
            cnt1.Contact_Primary_Specialty__c = 'ENT';
            cnt1.Affiliation_To_Account__c = 'Employee';
            cnt1.Primary_Job_Title_vs__c = 'Manager';
            cnt1.Contact_Gender__c = 'Male';
            
            Contact cnt2 = new Contact();
            cnt2.RecordTypeId = [Select Id from RecordType where SObjectType = 'Contact' AND DeveloperName = 'Generic_Contact'].Id;
            cnt2.LastName = 'TEST Contact 2';  
            cnt2.FirstName = 'TEST';  
            cnt2.AccountId = acc.Id;  
            cnt2.Phone = '001222333'; 
            cnt2.Email ='unit.test2@medtronic.com';
            cnt2.Contact_Department__c = 'Diabetes Adult'; 
            cnt2.Contact_Primary_Specialty__c = 'ENT';
            cnt2.Affiliation_To_Account__c = 'Employee';
            cnt2.Primary_Job_Title_vs__c = 'Manager';
            cnt2.Contact_Gender__c = 'Male';
            cnt2.MailingCountry = 'BELGIUM';       
            
            insert new List<Contact>{cnt1, cnt2};
            
            Test.startTest();
            
            merge cnt1 cnt2;
            
            Contact_Delete_Info__c deleteInfo = [Select Id, Type__c, Undeleted_Bool__c  from Contact_Delete_Info__c where Deleted_Contact_SFDC_ID__c = :cnt2.Id];
            System.assert(deleteInfo.Type__c == 'Merge');
            System.assert(deleteInfo.Undeleted_Bool__c == false);
            
            Test.stopTest();
            
        }
    }
    
    public static testMethod void testMMXFlag(){
        
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.SAP_Active__c = true;
        acc.SAP_Id__c = '123456789';    
        acc.Account_Country_vs__c = 'BELGIUM';         
        insert acc;
        
        Contact cnt = new Contact();
        cnt.RecordTypeId = [Select Id from RecordType where SObjectType = 'Contact' AND DeveloperName = 'Generic_Contact'].Id;
        cnt.LastName = 'TEST Contact ';  
        cnt.FirstName = 'TEST';  
        cnt.AccountId = acc.Id;  
        cnt.Phone = '001222333'; 
        cnt.Email ='unit.test@medtronic.com';
        cnt.Contact_Department__c = 'Diabetes Adult'; 
        cnt.Contact_Primary_Specialty__c = 'ENT';
        cnt.Affiliation_To_Account__c = 'Employee';
        cnt.Primary_Job_Title_vs__c = 'Manager';
        cnt.Contact_Gender__c = 'Male';
        cnt.Integrate_with_MMX__c = true;
        insert cnt;
        
        Test.startTest();
        
        cnt.Integrate_with_MMX__c = false;
        update cnt;
        
        cnt = [Select Integrate_with_MMX__c from Contact where Id = :cnt.Id];
        System.assert(cnt.Integrate_with_MMX__c == true);
    }
        
    private static Contact createDummyContact(){
        
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.SAP_Active__c = true;
        acc.SAP_Id__c = '123456789';    
        acc.Account_Country_vs__c = 'BELGIUM';
		acc.RecordTypeId = clsUtil.getRecordTypeByDevName('Account', 'SAP_Account').Id;         
        insert acc;
        
        Contact cnt = new Contact();
        cnt.RecordTypeId = clsUtil.getRecordTypeByDevName('Contact', 'Generic_Contact').Id;
        cnt.LastName = 'TEST Contact ';  
		cnt.FirstName = 'TEST';
        cnt.AccountId = acc.Id;  
        cnt.Phone = '001222333'; 
        cnt.Email ='unit.test@medtronic.com';
        cnt.Contact_Department__c = 'Diabetes Adult'; 
        cnt.Contact_Primary_Specialty__c = 'ENT';
        cnt.Affiliation_To_Account__c = 'Employee';
        cnt.Primary_Job_Title_vs__c = 'Manager';
        cnt.Contact_Gender__c = 'Male';
        insert cnt;
        
        return cnt;
    }    
    
    @isTest  public static void test_syncJobTitle_JobTitlevs(){
        
        Test.startTest();
        
		Contact oContact = clsTestData_Contact.createContact()[0];
		oContact = [SELECT Id, Primary_Job_Title_vs__c, Contact_Job_Title__c FROM COntact WHERE Id = :oContact.Id];
		System.assertEquals(oContact.Primary_Job_Title_vs__c, oContact.Contact_Job_Title__c);

		oContact.Primary_Job_Title_vs__c = 'Cardiologist';
		oContact = [SELECT Id, Primary_Job_Title_vs__c, Contact_Job_Title__c FROM COntact WHERE Id = :oContact.Id];
		System.assertEquals(oContact.Primary_Job_Title_vs__c, oContact.Contact_Job_Title__c);

		Test.stopTest();

    }

    public static testMethod void testContactOwner(){
        
        User usOwner = [Select Id from User where Alias = 'gdataUS'];
        
        Contact_Owner__c setting = new Contact_Owner__c();
        setting.Name = 'USA';
        setting.Owner_Id__c = usOwner.Id;
        insert setting;
        
        Account accUSA = new Account();
        accUSA.Name = 'Test Account US';
        accUSA.SAP_Active__c = true;
        accUSA.SAP_Id__c = '123456789';    
        accUSA.Account_Country_vs__c = 'USA';
        
        Account accBE = new Account();
        accBE.Name = 'Test Account BE';
        accBE.SAP_Active__c = true;
        accBE.SAP_Id__c = '987654321';    
        accBE.Account_Country_vs__c = 'BELGIUM';
                 
        insert new List<Account>{accUSA, accBE};
        
        Id genericContactRTid = [Select Id from RecordType where SObjectType = 'Contact' AND DeveloperName = 'Generic_Contact'].Id;
        
        Contact cntUSA = new Contact();
        cntUSA.RecordTypeId = genericContactRTid;
        cntUSA.LastName = 'TEST Contact USA';  
        cntUSA.FirstName = 'TEST';  
        cntUSA.AccountId = accUSA.Id;  
        cntUSA.Phone = '001222333'; 
        cntUSA.Email ='unit.test@medtronic.com';
        cntUSA.Contact_Department__c = 'Diabetes Adult'; 
        cntUSA.Contact_Primary_Specialty__c = 'ENT';
        cntUSA.Affiliation_To_Account__c = 'Employee';
        cntUSA.Primary_Job_Title_vs__c = 'Manager';
        cntUSA.Contact_Gender__c = 'Male';
        cntUSA.MailingCountry = 'USA';
        
        Contact cntBE = new Contact();
        cntBE.RecordTypeId = genericContactRTid;
        cntBE.LastName = 'TEST Contact BE';  
        cntBE.FirstName = 'TEST';  
        cntBE.AccountId = accbe.Id;  
        cntBE.Phone = '001222333'; 
        cntBE.Email ='unit.test@medtronic.com';
        cntBE.Contact_Department__c = 'Diabetes Adult'; 
        cntBE.Contact_Primary_Specialty__c = 'ENT';
        cntBE.Affiliation_To_Account__c = 'Employee';
        cntBE.Primary_Job_Title_vs__c = 'Manager';
        cntBE.Contact_Gender__c = 'Male';
        cntBE.MailingCountry = 'BELGIUM';
        
        Test.startTest();
                
        insert new List<Contact>{cntUSA, cntBE};
        
        cntUSA = [Select OwnerId from Contact where Id = :cntUSA.Id];
        System.assert(cntUSA.OwnerId == usOwner.Id);        
        
        cntBE = [Select OwnerId from Contact where Id = :cntBE.Id];
        System.assert(cntBE.OwnerId == UserInfo.getUserId());
    }

}