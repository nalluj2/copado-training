public with sharing class ctrlExt_Support_XW_Admin {
	
	private Create_User_Request__c userCreateRequest;
    private User currentUser ;

    public String tAdminUserId { 
        get; 
        set{
            tAdminUserId = value;
            if ( (value != null) && (value != bl_CreateUserRequest.tNONE) ){
                userCreateRequest.Assignee__c = tAdminUserId;
                if ( (lstSO_AdminUser[0].getLabel() == bl_CreateUserRequest.tNONE) ){
                    lstSO_AdminUser.remove(0);
                }
            }
        }
    }
    public List<SelectOption> lstSO_AdminUser { get; set; }

    
    public List<Attachment> attachments {get; private set;}
        
    public Boolean isAdminUser {get;set;}
        
    public ctrlExt_Support_XW_Admin(ApexPages.StandardController sc){
        

        if (!Test.isRunningTest()){
            sc.addFields(new List<String>{'Assignee__c', 'OwnerId', 'CreatedDate'});
        }
        userCreateRequest = (Create_User_Request__c) sc.getRecord();  
        
        currentUser = [Select name, Profile.Name, Email from User where Id =:UserInfo.getUserId()];


        lstSO_AdminUser = new List<SelectOption>();
        lstSO_AdminUser.add(new SelectOption(bl_CreateUserRequest.tNONE, bl_CreateUserRequest.tNONE));
        Map<Id, User> mapUser_Admin = bl_CreateUserRequest.loadAdminUsers('Support_Portal_Crossworld');
        for (User oUser : mapUser_Admin.values()){
            lstSO_AdminUser.add(new SelectOption(oUser.Id, oUser.Name));
        }
        tAdminUserId = userCreateRequest.Assignee__c;
        isAdminUser = false;
        if (mapUser_Admin.containsKey(currentUser.Id)){
            isAdminUser = true;
        }
                
        attachments = [Select Id, Name, BodyLength, CreatedDate, CreatedById 
                            from Attachment where ParentId=:userCreateRequest.Id ORDER BY CreatedDate ASC]; 
    }

    public Boolean bIsUpdatable {
        get{
            Boolean bResult = false;
            if (
                (isAdminUser) && (userCreateRequest.Status__c != 'Cancelled') && (userCreateRequest.Status__c != 'Completed')
            ){
                bResult = true;
            }
            return bResult;
        }
        private set;
    }

    public Boolean bIsAssigned {
        get{
            Boolean bResult = false;
            if (userCreateRequest.Assignee__c != null){
                bResult = true;
            }
            return bResult;
        }
        private set;
    }
            
    public void completeRequest(){
              
        if (tAdminUserId != UserInfo.getUserId()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Only the Assignee can complete this Request.'));         
            return;
        }


        if (clsUtil.isDecimalNull(userCreateRequest.Time_Spent__c, 0) == 0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Please, provide the time spent to complete the Request'));         
            return;
        }


        if (!clsUtil.isNull(userCreateRequest.Sub_Status__c, '').contains('Solved')){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Please, provide a "Solved" Sub Status to complete the Request'));         
            return;
        } 
          
        String prevStatus = userCreateRequest.status__c;        
        userCreateRequest.Status__c = 'Completed';
        userCreateRequest.Completed_By__c = currentUser.Name;
            
        if(save()){
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request Completed successfully'));          
            return;
        }
        
        userCreateRequest.Status__c = prevStatus;
    }
    
    public void reject(){
        
        String prevStatus = userCreateRequest.status__c;        
        userCreateRequest.status__c='Rejected';
        userCreateRequest.Approved_Rejected_By__c = currentUser.Name;
            
        if(save()){
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request has been Rejected'));           
            return;
        }
        
        userCreateRequest.Status__c = prevStatus;                           
    }
           
    public void assignToMe(){
        
        userCreateRequest.Assignee__c = UserInfo.getUserId();
        tAdminUserId = UserInfo.getUserId();
        if ( (userCreateRequest.Status__c != 'Approved') && (userCreateRequest.Status__c != 'Submitted for approval') ){
            userCreateRequest.Status__c = 'In Progress';
            userCreateRequest.Sub_Status__c = 'Assigned';
        }
        if(save()) ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request assigned successfully'));

    }
    
    public void saveRequest(){
        
        if(save()) ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request updated successfully')); 
    }

    private Boolean save(){
        
        SavePoint sp = Database.setSavepoint(); 
        
        try{

            if (userCreateRequest.Assignee__c != tAdminUserId){

                if (tAdminUserId != bl_CreateUserRequest.tNONE){
                    userCreateRequest.Assignee__c = tAdminUserId;
                    if (userCreateRequest.Status__c == 'New'){
                        userCreateRequest.Status__c = 'In Progress';
                    }
                    if (userCreateRequest.Sub_Status__c == null){
                        userCreateRequest.Sub_Status__c = 'Assigned';
                    }
                }else{
                    userCreateRequest.Assignee__c = null;
                }

            }
                        
            //Made at the end because it will trigger the execution
            update userCreateRequest;
            
        }catch(Exception e){
            
            Database.rollback(sp);          
            ApexPages.addMessages(e);
            if(Test.isRunningTest()) throw e;
            return false;
        }
        
        return true;
    }
           
    public PageReference createCR(){
        
        Savepoint sp = Database.setSavepoint();
        
        try{
                    
            clsUtil.bubbleException();

            Change_Request__c cr = new Change_Request__c();
            
            if(userCreateRequest.Request_Type__c == 'Bug') cr.Bug_Fix__c = true;
            else cr.Bug_Fix__c = false;
            
            if(userCreateRequest.Description_Long__c.length() > 255){
                cr.Change_Request__c = userCreateRequest.Description_Long__c.substring(0, 252)+'...';
            }else{
                cr.Change_Request__c = userCreateRequest.Description_Long__c;
            }
            cr.Details__c = userCreateRequest.Description_Long__c;
            
            if(userCreateRequest.Business_Rationale__c!=null) cr.Business_Rationale__c = userCreateRequest.Business_Rationale__c;
            
            cr.Status__c = 'New';
            cr.Origin__c='Support Portal';          
            cr.Request_Date__c = userCreateRequest.CreatedDate.date();
            
            if(userCreateRequest.Priority__c!=null) cr.Priority__c = userCreateRequest.Priority__c;
            else cr.Priority__c = 'Medium';         
            
            User requestor = [Select Id, UserType from User where Id=:userCreateRequest.OwnerId];
            if(requestor.UserType != 'Guest'){
                cr.Requestor_Lookup__c = userCreateRequest.OwnerId;
                
            }else{
                cr.Requestor_Lookup__c = UserInfo.getUserId();
            }

            cr.Related_Ticket__c = userCreateRequest.Name;            
            
            insert cr;
                                    
            userCreateRequest.Status__c = 'Completed';
            userCreateRequest.Sub_Status__c = 'Solved (CR)';
            userCreateRequest.Completed_By__c = currentUser.Name;
            userCreateRequest.Created_Change_Request__c = cr.Id;
            
            update userCreateRequest;
            
            AggregateResult[] attachmentSize = [Select SUM(BodyLength) Total from Attachment where ParentId = :userCreateRequest.Id];
            
            if( attachmentSize.size()>0){
                
                List<Attachment> crAttach = new List<Attachment>();
                
                Integer totalSize = Integer.valueOf(attachmentSize[0].get('Total'));
                //We only clone if the attachments are smaller than 5MB in total, to avoid Heap Size limits
                if(totalSize<5242880){
                    
                    for(Attachment attach : [Select Name, Body from Attachment where ParentId = :userCreateRequest.Id]){
                    
                        Attachment cloneAttach = new Attachment();
                        cloneAttach.name = attach.name;
                        cloneAttach.body = attach.body;
                        cloneAttach.ParentId = cr.Id;
                        
                        crAttach.add(cloneAttach);
                    }
                    
                    insert crAttach;
                }
            } 
            
            PageReference pr = new ApexPages.StandardController(cr).edit();
            pr.getParameters().put('retURL', '/'+cr.Id);
            pr.setRedirect(true);           
            
            return pr; 
            
        }catch(Exception e){
            ApexPages.addMessages(e);           
            Database.rollback(sp);
            if(Test.isRunningTest() == true) throw e;
            return null;
        }   
        
        return null;    
    }
    
    public void convertToSR(){
        
        try{
            clsUtil.bubbleException();
            
            if(userCreateRequest.Request_Type__c == 'Bug') userCreateRequest.Service_Request_Type__c = 'Problem';
            else userCreateRequest.Service_Request_Type__c = 'Other';
                        
            userCreateRequest.Request_Type__c = 'Generic Service';      
        
            update userCreateRequest;
        
        }catch(Exception e){
            ApexPages.addMessages(e);
            if(Test.isRunningTest() == true) throw e;           
        }
    }
}