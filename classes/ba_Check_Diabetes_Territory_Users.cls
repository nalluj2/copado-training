global class ba_Check_Diabetes_Territory_Users implements Database.Batchable<sObject>, Schedulable, Database.Stateful{
	
	global Set<Id> dibRoleIds;	
	global List<User> nonDibUsers;
	
	public Id testTerritoryId;
		
	global void execute(SchedulableContext sc){
    	   	
        ba_Check_Diabetes_Territory_Users batch = new ba_Check_Diabetes_Territory_Users();
        Database.executebatch(batch, 500);         
    } 

    // BATCH Settings
    global Database.QueryLocator start(Database.BatchableContext BC) {
    	
    	dibRoleIds = new Set<Id>();    	
    	nonDibUsers = new List<User>();    	
        	
    	UserRole dibRole = [Select Id, Name from UserRole where Name = 'DIABETES Data Steward (DIB)'];
		dibRoleIds.add(dibRole.Id);
			
		addChildRolesToList(new List<UserRole>{dibRole});
		
		Set<Id> dibTerritoryIds = new Set<Id>();
		
		List<Territory2> dibTerritories = [Select Id, Name from Territory2 where DeveloperName IN ('ANZ_Diabetes_R', 'DiabetesBU')];
		for(Territory2 dibTerritory : dibTerritories) dibTerritoryIds.add(dibTerritory.Id);		
		
		addChildTerritoriesToList(dibTerritories, dibTerritoryIds);
        
    	// Select all Diabetes Territories
        String tQuery = 'SELECT Id FROM Territory2 where ';
        
        if(Test.isRunningTest()) tQuery += ' Id = \'' + testTerritoryId + '\'';
        else tQuery += ' Id IN :dibTerritoryIds';
                
    	return Database.getQueryLocator(tQuery);    	
    }
    
    global void execute(Database.BatchableContext BC, List<Territory2> territories) {
		
		Set<Id> territoryUsers = new Set<Id>();
			
		for(UserTerritory2Association territoryUser : [Select UserId from UserTerritory2Association where isActive = true AND Territory2Id IN :territories]){
			
			territoryUsers.add(territoryUser.UserId);
		}
		
		for(User territoryUser : [Select Id, Name, UserRole.Name, UserRoleId from User where Id IN :territoryUsers]){
			
			if(dibRoleIds.contains(territoryUser.UserRoleId) == false){
				
				nonDibUsers.add(territoryUser);
			}
		}            	
  	}
	
  	global void finish(Database.BatchableContext BC) {
  		  		  		
		if(nonDibUsers.size() > 0){  		  		  		
	  		
	  		String emailBody = 'List of Users without Diabetes roles linked to Diabetes Territorys. <br/><br/>';
	  		
	  		for(User nonDibUser : nonDibUsers){
	  			
	  			String roleName = '';
	  			if(nonDibUser.UserRole != null) roleName = nonDibUser.UserRole.Name;
	  			
	  			emailBody += ' - ' + nonDibUser.Name + ' (' + nonDibUser.Id + ') - ' + roleName + ' <br/>';
	  		}
	  		
	  		List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
	  		
	  		for(User admin : [Select Id from User where isActive = true AND (Profile.Name = 'System Administrator' OR Id = :UserInfo.getUserId())]){
	  				
				Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
				email.setTargetObjectId(admin.Id);
				email.setSaveAsActivity(false);
				email.setSubject('Non-Diabetes Users in Diabetes Territories');
				email.setHTMLBody(emailBody);
				
				emails.add(email);
	  		}
	  		
			Messaging.sendEmail(emails);
		}	  		
  	}
  	
  	private void addChildRolesToList(List<UserRole> parentRoles){
  		
  		List<UserRole> childRoles = [Select Id, Name from UserRole where ParentRoleId IN :parentRoles];
  		
  		for(UserRole childRole : childRoles){
  			
  			dibRoleIds.add(childRole.Id);
  		}
  		
  		if(childRoles.size() > 0) addChildRolesToList(childRoles);
  	}
  	
  	private void addChildTerritoriesToList(List<Territory2> parentTerritories, Set<Id> dibTerritoryIds){
  		
  		List<Territory2> childTerritories = [Select Id, Name from Territory2 where ParentTerritory2Id IN :parentTerritories];
  		
  		for(Territory2 childTerritory : childTerritories){
  			
  			dibTerritoryIds.add(childTerritory.Id);
  		}
  		
  		if(childTerritories.size() > 0) addChildTerritoriesToList(childTerritories, dibTerritoryIds);
  	}
}