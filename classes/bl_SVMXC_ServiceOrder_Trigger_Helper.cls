public without sharing class bl_SVMXC_ServiceOrder_Trigger_Helper {
    
    public static void updateRejectionReason(List<SVMXC__Service_Order__c> serviceOrders)
    {
    	for (SVMXC__Service_Order__c serviceOrder: serviceOrders)
    	{
    		if (serviceOrder.SVMXC__Order_Status__c == 'Ready to Plan')
    		{
    			serviceOrder.SVMX_Rejection_Reason__c = '';
    		}
    	}
    }
    
    public static void updateWarrantyInfo(List<SVMXC__Service_Order__c> serviceOrders)
    {
    	//get the set of SAP equipment Ids
    	Set<Id> installBaseIds = new Set<Id>();
    	for (SVMXC__Service_Order__c serviceOrder: serviceOrders)
    		installBaseIds.add(serviceOrder.SVMXC__Component__c);
    		
    	//get all installed product data
		Map<String, SVMXC__Installed_Product__c> installedProducts = bl_InstallBaseSAPService.getInstallProductInfo(installBaseIds);
		
		//for each service order
    	for (SVMXC__Service_Order__c serviceOrder: serviceOrders)
    	{
    		
    		//get the associated installed product
    		SVMXC__Installed_Product__c installedProduct = installedProducts.get(serviceOrder.SVMXC__Component__c);
    		
    		if (installedProduct != null)
    		{
    			serviceOrder.SVMX_Contract_Start_Date__c = installedProduct.SVMXC__Service_Contract_Start_Date__c;
    			serviceOrder.SVMX_Contract_End_Date__c   = installedProduct.SVMXC__Service_Contract_End_Date__c;
    			serviceOrder.SVMX_Warranty_Number__c     = installedProduct.SVMXC__Warranty__c;
    			serviceOrder.SVMX_Warranty_Start_Date__c = installedProduct.SVMXC__Warranty_Start_Date__c;
    			serviceOrder.SVMX_Warranty_End_Date__c   = installedProduct.SVMXC__Warranty_End_Date__c;
    			serviceOrder.SVMX_Contract_Number__c     = installedProduct.SVMX_Contract_Number__c;
    			serviceOrder.SVMX_Contract_Type__c       = installedProduct.SVMX_Contract_Type__c;
    		}
    		
    	}
		
    }

    public static void updateSOFromNotification(List<SVMXC__Service_Order__c> newRecords)
    {
    	
    	//get all associated notification Ids
    	Set<Id> notificationIds = new Set<Id>();
    	for (SVMXC__Service_Order__c serviceOrder: newRecords)
    		notificationIds.add(serviceOrder.SVMXC__Case__c);
    	
    	//get related notifications
    	Map<Id, Case> notifications = new Map<Id, Case>([SELECT SAP_Equipment_Number__c, Serial_Number__c,    Material_Code__c, Contract_Number__c,
								    				   Material_Description__c, Priority,            GCH_Group__c,		GCH_Number__c,
								    				   GCH_Code__c,             GCH_Question_1__c,   GCH_Question_2__c,
								    				   GCH_Question_3__c,       Question_Comment__c, Actual_Breakdown_Date_Time__c,
								    				   SVMXC__Actual_Restoration__c,Customer_Down__c, Description, SVMX_TSU_RECALL_Indicator__c, SVMX_TSU_Number__c
													FROM Case
													WHERE Id IN :notificationIds]);
    	
    	for (SVMXC__Service_Order__c serviceOrder: newRecords)
    	{
    		
    		Case notification = notifications.get(serviceOrder.SVMXC__Case__c);
    		
    		if (notification != null)
    		{
				serviceOrder.SVMX_Contract_Number__c      = notification.Contract_Number__c;
				serviceOrder.SVMX_SAP_Equipment_Number__c = notification.SAP_Equipment_Number__c;
				serviceOrder.SVMX_Serial_Number__c        = notification.Serial_Number__c;
				serviceOrder.SVMX_Material_Code__c        = notification.Material_Code__c;
				serviceOrder.SVMX_Material_Description__c = notification.Material_Description__c;
				serviceOrder.SVMXC__Priority__c           = notification.Priority;
				serviceOrder.GCH_Number__c				  = notification.GCH_Number__c;
				serviceOrder.SVMX_GCH_Group__c            = notification.GCH_Group__c;
				serviceOrder.SVMX_GCH_Code__c             = notification.GCH_Code__c;
				serviceOrder.SVMX_GCH_Question_1__c       = notification.GCH_Question_1__c;
				serviceOrder.SVMX_GCH_Question_2__c       = notification.GCH_Question_2__c;
				serviceOrder.SVMX_GCH_Question_3__c       = notification.GCH_Question_3__c;
				serviceOrder.SVMX_GCH_Question_Comment__c = notification.Question_Comment__c;
				serviceOrder.SVMX_Actual_Breakdown__c     = ut_String.datetimeToDate(notification.Actual_Breakdown_Date_Time__c);
				serviceOrder.SVMXC__Actual_Restoration__c = notification.SVMXC__Actual_Restoration__c;
				serviceOrder.SVMXC__Customer_Down__c      = notification.Customer_Down__c;
				serviceOrder.SVMXC__Problem_Description__c= notification.Description;
				serviceOrder.SVMX_TSU_RECALL_Indicator__c = notification.SVMX_TSU_RECALL_Indicator__c; 
				serviceOrder.SVMX_TSU_Number__c           = notification.SVMX_TSU_Number__c;
    		}
    	}
    	
    }
    
    public static void updateNotificationFromSO(Map<Id, SVMXC__Service_Order__c> newRecords)
    {
    	//get all associated notification Ids
    	Set<Id> notificationIds = new Set<Id>();
    	for (SVMXC__Service_Order__c serviceOrder: newRecords.values())
    		notificationIds.add(serviceOrder.SVMXC__Case__c);
    	
    	//get related notifications
    	Map<Id, Case> notifications = bl_ServiceNotificationSAPService.getCases(notificationIds);
    	List<Case> updatedNotifications = new List<Case>();
    	
    	for (SVMXC__Service_Order__c serviceOrder: newRecords.values())
    	{
    		
    		Case notification = notifications.get(serviceOrder.SVMXC__Case__c);
    		
    		if (notification != null)
    		{
    			
    			Boolean isChanged = false;
    			
    			if (notification.Contract_Number__c != serviceOrder.SVMX_Contract_Number__c)           { isChanged = true; notification.Contract_Number__c = serviceOrder.SVMX_Contract_Number__c; }
				if (notification.SAP_Equipment_Number__c != serviceOrder.SVMX_SAP_Equipment_Number__c) { isChanged = true; notification.SAP_Equipment_Number__c = serviceOrder.SVMX_SAP_Equipment_Number__c; }
				if (notification.Serial_Number__c != serviceOrder.SVMX_Serial_Number__c)               { isChanged = true; notification.Serial_Number__c = serviceOrder.SVMX_Serial_Number__c; }
				if (notification.Material_Code__c != serviceOrder.SVMX_Material_Code__c)               { isChanged = true; notification.Material_Code__c = serviceOrder.SVMX_Material_Code__c; }
				if (notification.Material_Description__c != serviceOrder.SVMX_Material_Description__c) { isChanged = true; notification.Material_Description__c = serviceOrder.SVMX_Material_Description__c; }
				if (notification.Priority != serviceOrder.SVMXC__Priority__c)                          { isChanged = true; notification.Priority = serviceOrder.SVMXC__Priority__c; }
				if (notification.GCH_Group__c != serviceOrder.SVMX_GCH_Group__c)                       { isChanged = true; notification.GCH_Group__c = serviceOrder.SVMX_GCH_Group__c; }
				if (notification.GCH_Code__c != serviceOrder.SVMX_GCH_Code__c)                         { isChanged = true; notification.GCH_Code__c = serviceOrder.SVMX_GCH_Code__c; }
				if (notification.GCH_Question_1__c != serviceOrder.SVMX_GCH_Question_1__c)             { isChanged = true; notification.GCH_Question_1__c = serviceOrder.SVMX_GCH_Question_1__c; }
				if (notification.GCH_Question_2__c != serviceOrder.SVMX_GCH_Question_2__c)             { isChanged = true; notification.GCH_Question_2__c = serviceOrder.SVMX_GCH_Question_2__c; }
				if (notification.GCH_Question_3__c != serviceOrder.SVMX_GCH_Question_3__c)             { isChanged = true; notification.GCH_Question_3__c = serviceOrder.SVMX_GCH_Question_3__c; }
				if (notification.Question_Comment__c != serviceOrder.SVMX_GCH_Question_Comment__c)     { isChanged = true; notification.Question_Comment__c = serviceOrder.SVMX_GCH_Question_Comment__c; }
				//notification.Actual_Breakdown_Date_Time__c = 
				//serviceOrder.SVMX_Actual_Breakdown__c = ut_String.datetimeToDate();
				if (notification.SVMXC__Actual_Restoration__c != serviceOrder.SVMXC__Actual_Restoration__c) { isChanged = true; notification.SVMXC__Actual_Restoration__c = serviceOrder.SVMXC__Actual_Restoration__c; }
				if (notification.Customer_Down__c != serviceOrder.SVMXC__Customer_Down__c)                  { isChanged = true; notification.Customer_Down__c = serviceOrder.SVMXC__Customer_Down__c; }
				if (notification.Description != serviceOrder.SVMXC__Problem_Description__c)     	{ isChanged = true; notification.Description = serviceOrder.SVMXC__Problem_Description__c; }
				
				//only update notification if a change has occurred
				if (isChanged)
					updatedNotifications.add(notification);
				
    		}
    	}
    	
    	if (updatedNotifications.size() > 0)
    		update updatedNotifications;
    	
    }    
}