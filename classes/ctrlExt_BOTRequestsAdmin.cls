public with sharing class ctrlExt_BOTRequestsAdmin {

	private Create_User_Request__c request;
	private User currentUser ;

    public String tAdminUserId { 
        get; 
        set{
            tAdminUserId = value;
            if ( (value != null) && (value != bl_CreateUserRequest.tNONE) ){
                request.Assignee__c = tAdminUserId;
                if ( (lstSO_AdminUser[0].getLabel() == bl_CreateUserRequest.tNONE) ){
                    lstSO_AdminUser.remove(0);
                }
            }
        }
    }
    public List<SelectOption> lstSO_AdminUser { get; set; }

	
	public Boolean isAdminUser {get;set;}
	
	public ctrlExt_BOTRequestsAdmin(ApexPages.StandardController sc){
		
		if (!Test.isRunningTest()){
            sc.addFields(new List<String>{'Assignee__c', 'OwnerId', 'CreatedDate'});
        }
		request = (Create_User_Request__c) sc.getRecord();	
		
		currentUser = [Select name, Profile.Name, Email from User where Id =:UserInfo.getUserId()];
		
        lstSO_AdminUser = new List<SelectOption>();
        lstSO_AdminUser.add(new SelectOption(bl_CreateUserRequest.tNONE, bl_CreateUserRequest.tNONE));
        Map<Id, User> mapUser_Admin = bl_CreateUserRequest.loadAdminUsers('Support_Portal_BOT');
        for (User oUser : mapUser_Admin.values()){
            lstSO_AdminUser.add(new SelectOption(oUser.Id, oUser.Name));
        }
        tAdminUserId = request.Assignee__c;
        isAdminUser = false;
        if (mapUser_Admin.containsKey(currentUser.Id)){
            isAdminUser = true;
        }

	}

    public Boolean bIsUpdatable {
        get{
            Boolean bResult = false;
            if (
                (isAdminUser) && (request.Status__c != 'Cancelled') && (request.Status__c != 'Completed')
            ){
                bResult = true;
            }
            return bResult;
        }
        private set;
    }

    public Boolean bIsAssigned {
        get{
            Boolean bResult = false;
            if (request.Assignee__c != null){
                bResult = true;
            }
            return bResult;
        }
        private set;
    }
	
	public void completeRequest(){


        if (request.Application__c == 'BOT'){

            if (tAdminUserId != UserInfo.getUserId()){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Only the Assignee can complete this Request.'));         
                return;
            }


            if (clsUtil.isDecimalNull(request.Time_Spent__c, 0) == 0){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Please, provide the time spent to complete the Request'));         
                return;
            }


            if (!clsUtil.isNull(request.Sub_Status__c, '').contains('Solved')){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Please, provide a "Solved" Sub Status to complete the Request'));         
                return;
            } 

        }
                        
        String prevStatus = request.Status__c;        
        request.Status__c = 'Completed';
        request.Completed_By__c = currentUser.Name;
            
        if(save()){
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request Completed successfully'));          
            return;
        }
        
        request.Status__c = prevStatus;
    }
    
    public void assignToMe(){
        
        request.Assignee__c = UserInfo.getUserId();
        tAdminUserId = request.Assignee__c;
        if ( (request.Status__c != 'Approved') && (request.Status__c != 'Submitted for approval') ){
            request.Status__c = 'In Progress';
            request.Sub_Status__c = 'Assigned';
        }
        if(save()) ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request assigned successfully'));

    }
    
    public void saveRequest(){

        if(save()) ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request updated successfully')); 

    }

    private Boolean save(){
        
        SavePoint sp = Database.setSavepoint(); 
        
        try{                        

            if (request.Assignee__c != tAdminUserId){

                if (tAdminUserId != bl_CreateUserRequest.tNONE){
                    request.Assignee__c = tAdminUserId;
                    if (request.Status__c == 'New'){
                        request.Status__c = 'In Progress';
                    }
                    if (request.Sub_Status__c == null){
                        request.Sub_Status__c = 'Assigned';
                    }
                }else{
                    request.Assignee__c = null;
                }

            }


            update request;
            
        }catch(Exception e){
            
            Database.rollback(sp);          
            ApexPages.addMessages(e);
            if(Test.isRunningTest()) throw e;
            return false;
        }
        
        return true;
    }
    
}