public with sharing class bl_Check_UnitTest_Run_Results {
	
	@testVisible
    private static List<ApexTestRunResult> lastRun;

    @future(callout=true)
    public static void getTestResults(DateTime runDate){
    	
    	Organization orgInfo = [Select IsSandbox from Organization LIMIT 1];
        
        if(runDate == null) runDate = Datetime.newInstance(Date.today(), Time.newInstance(0,0,0,0));
                                      
        if(Test.isRunningTest() == false) lastRun = [Select AsyncApexJobId, createdDate from ApexTestRunResult WHERE ClassesEnqueued > 100 AND CreatedDate >= :runDate ORDER BY CreatedDate DESC Limit 1];
        
        String subject;        
        String htmlBody;
        
		subject = 'Unit Test Results';
				
		if(orgInfo.isSandbox == true) htmlBody = UserInfo.getUserName().substringAfterLast('.').toUpperCase();
		else htmlBody = 'PRODUCTION';
		
        List<Messaging.EmailFileAttachment> attachments = new List<Messaging.EmailFileAttachment>();
        
        if(lastRun.size() > 0){
			
			if(Test.isRunningTest() == false) htmlBody += ' ' + lastRun[0].createdDate.format('yyyy/MM/dd') + '<br/><br/>';
			        	
        	String lastRunId = lastRun[0].AsyncApexJobId;
        	
        	htmlBody += getUnitTestResults(lastRunId, attachments);
        	htmlBody += getCodeCoverage(attachments);  	
        	
        }else{
        	  	
        	htmlBody += '<br/><br/>No recent Unit Tests Run found.';
        } 
        
        
        List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();
        
        Set<Id> targetUserIds = new Set<Id>();
        
        for(GroupMember developer  : [Select UserOrGroupId from GroupMember where Group.DeveloperName = 'Development_Team']){
        	
        	targetUserIds.add(developer.UserOrGroupId);
        }
        
        if(targetUserIds.size() == 0) targetUserIds.add(UserInfo.getUserId());
        
        for(String userId : targetUserIds){
        
	        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
			email.setTargetObjectId(userId);		
			email.setSaveAsActivity(false);
			email.setSubject(subject);
			email.setHTMLBody(htmlBody);
			email.setFileAttachments(attachments);
			
	        messages.add(email);
        }
				
		Messaging.sendEmail(messages); 
    } 
    
    @testVisible
    private static List<ApexTestResult> testResults;
    
    private static String getUnitTestResults(Id lastRunId, List<Messaging.EmailFileAttachment> attachments){
    	
    	String passFile = '"ApexClass","Method","EndTime","Id"\n';
    	String failFile = '"ApexClass","Method","Message","StackTrace","EndTime","Id"\n';
    	
    	Integer passed = 0;
    	Integer failed = 0;
    	
    	if(Test.isRunningTest() == false) testResults = [SELECT ApexClass.Name, Id, Message, MethodName, Outcome, StackTrace, SystemModstamp FROM ApexTestResult WHERE AsyncApexJobId = :lastRunId ORDER BY ApexClass.Name, MethodName];
    	
    	for(ApexTestResult testResult : testResults){
    		
    		if(testResult.Outcome == 'Pass'){
    			
    			passFile += '"' + testResult.ApexClass.Name + '"';
    			passFile += ',"' + testResult.MethodName + '"';
    			passFile += ',"' + testResult.SystemModstamp + '"';
    			passFile += ',"' + testResult.Id + '"\n';
    			
    			passed++;
    			
    		}else{
    			
    			failFile += '"' + testResult.ApexClass.Name + '"';
    			failFile += ',"' + testResult.MethodName + '"';
    			failFile += ',"' + testResult.Message + '"';
    			failFile += ',"' + testResult.StackTrace + '"';
    			failFile += ',"' + testResult.SystemModstamp + '"';
    			failFile += ',"' + testResult.Id + '"\n';      
    			
    			failed++;        			        			
    		}	        		
    	} 
    	
    	String emailBody = 'Failed: ' + failed + '<br/>';
    	emailBody += 'Passed: ' + passed + '<br/>';
    	   	    	
    	if(passed > 0){
    		
    		Messaging.EmailFileAttachment passAttachment = new Messaging.EmailFileAttachment();
			passAttachment.setFileName('pass.csv');
			passAttachment.setBody(Blob.valueOf(passFile));
			attachments.add(passAttachment);
    	}
    	
    	if(failed > 0){
		
			Messaging.EmailFileAttachment failAttachment = new Messaging.EmailFileAttachment();
			failAttachment.setFileName('fail.csv');
			failAttachment.setBody(Blob.valueOf(failFile));
			attachments.add(failAttachment);
    	}
    	
    	return emailBody;
    }   
    
    public static String getCodeCoverage(List<Messaging.EmailFileAttachment> attachments){
    	
    	String sessionId = UserInfo.getSessionId();
    	
    	String query = 'SELECT ApexClassOrTrigger.Id, ApexClassOrTrigger.Name, NumLinesCovered, NumLinesUncovered FROM ApexCodeCoverageAggregate';
        
        HttpRequest req = new HttpRequest();    	    	
		req.setEndpoint(URL.getSalesforceBaseUrl().toExternalForm()+'/services/data/v45.0/tooling/query/?q=' + EncodingUtil.URLEncode(query, 'UTF-8'));
		req.setMethod('GET');
		req.setHeader('Authorization', 'Bearer ' + sessionId);
		req.setHeader('Content-Type', 'application/json');
		req.setTimeout(120000);
				    
	    Http http = new Http();
	    HttpResponse res = http.send(req);
	    	     	
	    //if(res.getStatusCode() < 200 || res.getStatusCode() >= 300) throw new Scheduled_Exception('Error during Rest call: ' + res.getStatus() + ' ('+ res.getStatusCode() + '). Response received : ' + res.getBody());	             	
	    	     	     	
	    QueryResponse response = (QueryResponse) JSON.deserialize(res.getBody(), QueryResponse.class);
	   
	    String coverageFile = '"ApexClassOrTrigger","Type","TotalLines","Coverage"\n';
	    
	    Decimal totalCoveredLines = 0;
	    Decimal totalLines = 0;
	    
	    String coverageErrors = '';
	    
	    for(ApexCodeCoverageAggregate coverageInfo : response.records){
	    	
	    	Decimal itemCoveredLines = coverageInfo.numLinesCovered;
	    	Decimal itemTotalLines = coverageInfo.numLinesCovered + coverageInfo.numLinesUncovered;    		    		    			
	    		    	    	
	    	if(itemTotalLines > 0){
	    		
	    		Decimal itemCoverage = itemCoveredLines.divide(itemTotalLines, 4, System.RoundingMode.DOWN) * 100;
	    		    	
		    	String itemType = coverageInfo.apexClassOrTrigger.Id.startsWith('01p') ? 'ApexClass' : 'ApexTrigger';
		    	
		    	coverageFile += '"' + coverageInfo.apexClassOrTrigger.Name + '","' + itemType + '","' + itemTotalLines.setScale(0) + '","' + itemCoverage + '"\n';
		    	
		    	totalCoveredLines += itemCoveredLines;
		    	totalLines += itemTotalLines;
		    	
		    	if(itemType == 'ApexTrigger' && itemCoverage < 1) coverageErrors += 'Trigger ' + coverageInfo.apexClassOrTrigger.Name + ' does not have at least 1% of coverage <br/>';
	    	}   	
	    }
	    
	    Decimal totalCoverage = totalCoveredLines.divide(totalLines, 4, System.RoundingMode.DOWN) * 100;
	    
	    String emailBody = 'Total Code Coverage: ' + totalCoverage.setScale(2) + '%<br/><br/>';
	    emailBody += coverageErrors;
	    
	    Messaging.EmailFileAttachment coverageAttachment = new Messaging.EmailFileAttachment();
		coverageAttachment.setFileName('coverage.csv');
		coverageAttachment.setBody(Blob.valueOf(coverageFile));
		attachments.add(coverageAttachment);
		
		return emailBody;
    }
    
    public class QueryResponse{
    	
		public List<ApexCodeCoverageAggregate> records {get;set;}		
	}
	
	public class ApexCodeCoverageAggregate{
		
		public String id {get; set;}
		public ApexClassOrTrigger apexClassOrTrigger {get; set;}
		public Integer numLinesCovered {get; set;}
		public Integer numLinesUncovered {get; set;}
	}
	
	public class ApexClassOrTrigger{		
		
		public String id {get; set;}
		public String name {get; set;}
	}    
}