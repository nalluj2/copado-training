@isTest
private class Test_ws_Task {
    
    private static testmethod void createTaskWithContacts(){
    	
    	Account acc = new Account();
        acc.Name = 'testAccount';
        acc.Account_Country_vs__c = 'NETHERLANDS';        
        acc.RecordTypeId = RecordTypeMedtronic.Account('Pending_Account').Id;
        insert acc;        
        
        List<Contact> accContacts = new List<Contact>();
        
        for(Integer i = 0; i < 3; i++){
        	
	        Contact cnt = new Contact();
			cnt.RecordTypeId = RecordTypeMedtronic.Contact('Generic_Contact').Id;
			cnt.LastName = 'Test Contact ' + i;
			cnt.FirstName = 'TEST';
			cnt.Contact_Gender__c = 'Male';
			cnt.Contact_Primary_Specialty__c = 'Physiology';
			cnt.Contact_Department__c = 'Physiotherapy';
			cnt.AccountId = acc.Id;
			cnt.Affiliation_To_Account__c = 'Employee';
			cnt.Primary_Job_Title_vs__c = 'Physician';
			cnt.MailingCountry = 'NETHERLANDS';
			
			accContacts.add(cnt);
        }
          
        insert accContacts;
    	
    	Test.startTest();
    	
    	RestRequest req = new RestRequest();
        
        req.requestURI = '/services/apexrest/TaskService';
        req.httpMethod = 'POST';
        
        Task tsk = new Task();
        tsk.Mobile_ID__c = GuidUtil.NewGuid();
        tsk.WhoId = accContacts[0].Id;
        
        ws_Task.IFTaskRequest request = new ws_Task.IFTaskRequest();
        request.Task = tsk;
        request.ContactIds = new List<Id>{accContacts[1].Id, accContacts[2].Id};
        
        req.requestbody = Blob.valueOf(JSON.serializePretty(request));
        
        RestContext.request = req;
        RestContext.response = new RestResponse();
    	
    	String resp = ws_Task.doPost(); 
		System.debug('Response: ' + resp);
		
		ws_Task.IFTaskResponse response = (ws_Task.IFTaskResponse) JSON.deserialize(resp, ws_Task.IFTaskResponse.class);
		
		System.assert(response.success == true);
		System.assert(response.task.id != null);
		System.assert(response.task.WhoId == accContacts[1].Id);
		
		tsk = [Select Id, WhoId from Task where Id = :response.task.Id];
		System.assertEquals(response.task.WhoId, tsk.WhoId);
		
		List<TaskRelation> relations = [Select Id, RelationId from TaskRelation where TaskId = : response.task.Id AND isWhat = false];
		System.assertEquals(2, relations.size());
    }
    
    private static testmethod void updateTaskWithContacts(){
    	
    	Account acc = new Account();
        acc.Name = 'testAccount';
        acc.Account_Country_vs__c = 'NETHERLANDS';        
        acc.RecordTypeId = RecordTypeMedtronic.Account('Pending_Account').Id;
        insert acc;        
        
        List<Contact> accContacts = new List<Contact>();
        
        for(Integer i = 0; i < 5; i++){
        	
	        Contact cnt = new Contact();
			cnt.RecordTypeId = RecordTypeMedtronic.Contact('Generic_Contact').Id;
			cnt.LastName = 'Test Contact ' + i;
			cnt.FirstName = 'TEST';
			cnt.Contact_Gender__c = 'Male';
			cnt.Contact_Primary_Specialty__c = 'Physiology';
			cnt.Contact_Department__c = 'Physiotherapy';
			cnt.AccountId = acc.Id;
			cnt.Affiliation_To_Account__c = 'Employee';
			cnt.Primary_Job_Title_vs__c = 'Physician';
			cnt.MailingCountry = 'NETHERLANDS';
			
			accContacts.add(cnt);
        }
          
        insert accContacts;
    	
    	Task tsk = new Task();
        tsk.Mobile_ID__c = GuidUtil.NewGuid();       
        
        insert tsk;
        
        List<TaskRelation> taskContacts = new List<TaskRelation>();
        
        for(Integer i = 0; i < 3; i++){
        	
        	TaskRelation taskContact = new TaskRelation();
        	taskContact.TaskId = tsk.Id;
        	taskContact.RelationId = accContacts[i].Id;
        	taskContacts.add(taskContact);
        }
        
        insert taskContacts;
    	
    	List<TaskRelation> relations = [Select Id, RelationId from TaskRelation where TaskId = : tsk.Id AND isWhat = false];
		System.assertEquals(3, relations.size());
    	
    	Test.startTest();
    	
    	RestRequest req = new RestRequest();
        
        req.requestURI = '/services/apexrest/TaskService';
        req.httpMethod = 'POST';
                       
        ws_Task.IFTaskRequest request = new ws_Task.IFTaskRequest();
        request.Task = tsk;
        request.ContactIds = new List<Id>{accContacts[3].Id, accContacts[4].Id};
        
        req.requestbody = Blob.valueOf(JSON.serializePretty(request));
        
        RestContext.request = req;
        RestContext.response = new RestResponse();
    	
    	String resp = ws_Task.doPost(); 
		System.debug('Response: ' + resp);
		
		ws_Task.IFTaskResponse response = (ws_Task.IFTaskResponse) JSON.deserialize(resp, ws_Task.IFTaskResponse.class);
		
		System.assert(response.success == true);
		System.assert(response.task.id != null);
		
		relations = [Select Id, RelationId from TaskRelation where TaskId = : response.task.Id AND isWhat = false];
		System.assertEquals(2, relations.size());
		
		tsk = [Select Id, WhoId from Task where Id = :tsk.Id];
		System.assertEquals(accContacts[3].Id, tsk.WhoId);
    }
    
    private static testmethod void deleteTask(){
    	                    	
    	Task tsk = new Task();
        tsk.Mobile_ID__c = GuidUtil.NewGuid();                
        insert tsk;
        
    	Test.startTest();
    	
    	RestRequest req = new RestRequest();
        
        req.requestURI = '/services/apexrest/TaskService';
        req.httpMethod = 'DELETE';
                       
        ws_Task.IFTaskDeleteRequest request = new ws_Task.IFTaskDeleteRequest();
        request.Task = tsk;
                
        req.requestbody = Blob.valueOf(JSON.serializePretty(request));
        
        RestContext.request = req;
        RestContext.response = new RestResponse();
    	
    	String resp = ws_Task.doDelete(); 
		System.debug('Response: ' + resp);
		
		ws_Task.IFTaskDeleteResponse response = (ws_Task.IFTaskDeleteResponse) JSON.deserialize(resp, ws_Task.IFTaskDeleteResponse.class);
		
		System.assert(response.success == true);				
    }
}