//------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 23-05-2019
//  Description      : APEX Controller for VF Page addCampaignAccount
//  Change Log       : 
//------------------------------------------------------------------------------------------------------------
public class ctrlExt_AddCampaignAccount{

	//------------------------------------------------------------------------------------------
	// Private variables
	//------------------------------------------------------------------------------------------
	private static final Integer iQueryLimit_MAX = 10000;
	private static final Integer iPageSize = 10;
	private static final String tBaseQuery = 'SELECT Id, Name, Account_Country_vs__c, Account_City__c, AF_Solutions__c, Implantables_Diagnostic__c, MCS__c, ECT__c, HST__c, Structural_Heart__c, Aortic__c, Coro_PV__c, Deep__c, Embolization__c, Superficial__c FROM Account';

	private static Integer iQueryLimit = 0;
	private String tSortBy = 'Name';
	private Map<Id, wrAccount> mapWRAccount_Selected;
	private Set<Id> setID_Account_LinkedToCampaign;
	private Map<Id, wrAccount> mapHoldingSelectedRecords;	// Keeps the selected records selected during usage of pagination
	//------------------------------------------------------------------------------------------


	//------------------------------------------------------------------------------------------
	// CONSTRUCTOR
	//------------------------------------------------------------------------------------------
    public ctrlExt_AddCampaignAccount(ApexPages.StandardController oStdCTRL) {

		tSortBy = 'Name';
		tSortDir = 'ASC';

		oCampaign = (Campaign)oStdCTRL.getRecord();
		oCampaign = [SELECT Id, Name FROM Campaign WHERE Id = :oCampaign.Id];

		oCampaignAccountStatus = new Campaign_Account__c();

		loadAccountLinkedToCampaign();

		mapHoldingSelectedRecords = new Map<Id, wrAccount>();
		mapWRAccount_Selected = new Map<Id, wrAccount>();
		init();
	
	}
	//------------------------------------------------------------------------------------------

    
	//------------------------------------------------------------------------------------------
	// GETTERS & SETTERS
	//------------------------------------------------------------------------------------------
	// Instantiate the StandardSetController from a query locater
	public ApexPages.StandardSetController stdSetController_Account{

		get{
			if (stdSetController_Account == null){
				
				iQueryLimit = iQueryLimit_MAX;
				if (String.isBlank(tAccountFilterId) && !Test.isRunningTest()) iQueryLimit = 0;

				String tSOQL = tBaseQuery + ' ' + ' ORDER BY ' + tSortBy + ' ' + tSortDir + ' LIMIT ' + iQueryLimit;

				stdSetController_Account = new ApexPages.StandardSetController(Database.getQueryLocator(tSOQL));
 
				// sets the number of records to show in each page view
				stdSetController_Account.setPageSize(iPageSize);

				lstAccountExistingView = stdSetController_Account.getListViewOptions();
				stdSetController_Account.setFilterId(tAccountFilterId);
			
			}
			return stdSetController_Account;
		}
		set;

	}
	
	public Campaign oCampaign { get; private set; }
	public Campaign_Account__c oCampaignAccountStatus { get; set; }

	public Integer iSelectedRecords { get{ return mapHoldingSelectedRecords.size(); } }
	
	// Indicates whether there are more records after the current page set
	public Boolean bHasNext{

		get{
			if (stdSetController_Account != null){
				return stdSetController_Account.getHasNext();
			}else{
				return false;
			}
		}
		set;

	}
 
	// Indicates whether there are more records before the current page set
	public Boolean bHasPrevious{

		get{
			if (stdSetController_Account != null){
				return stdSetController_Account.getHasPrevious();
			}else{
				return false;
			}
		}
		set;

	}

	// Returns the page number of the current page set
	public Integer iPageNumber{

		get{
			Integer iPage = 0;
			if (stdSetController_Account != null){
				if (iTotalPages > 0) iPage = stdSetController_Account.getPageNumber();
			}
			return iPage;
		}
		set;

	}
 
	// Returns the total number of pages for page set
	Public Integer iTotalPages{

		get{
			if (stdSetController_Account != null){
				Decimal decTotalSize = stdSetController_Account.getResultSize();
				Decimal decPageSize = stdSetController_Account.getPageSize();
				Decimal decPages = decTotalSize / decPageSize;
				return (Integer)decPages.round(System.RoundingMode.CEILING);
			}else{
				return 0;
			}
		}

	}

	public List<wrAccount> lstWRAccount{ get; set; }
	public Boolean bMainSelected { get; set; }

	public String tSortDir { get; set; }
	public String tSortField { get; set; }
	public List<wrAccount> lstWRAccount_Selected { get{ return mapWRAccount_Selected.values(); } }

	public Id idAccount_Delete { get; set; }

	public List<SelectOption> lstAccountExistingView { get; private set; }
	public String tAccountFilterId { get; set; }
	//------------------------------------------------------------------------------------------


	//------------------------------------------------------------------------------------------
	// ACTIONS
	//------------------------------------------------------------------------------------------
	private void init() {

		lstWRAccount = new List<wrAccount>();

		if (stdSetController_Account != null){

			if (tAccountFilterId != null){
				stdSetController_Account.setFilterId(tAccountFilterId);
			}
			for (Account oAccount : (List<Account>)stdSetController_Account.getRecords()){

				wrAccount oWRAccount = new wrAccount(oAccount);
				if (setID_Account_LinkedToCampaign.contains(oAccount.Id)){
					oWRAccount.bReadOnly = true;
					oWRAccount.tResult = 'This Account is already linked to the Campaign';
				}
				lstWRAccount.add(oWRAccount);

			}

		}

	}


	private void loadAccountLinkedToCampaign(){

		List<Campaign_Account__c> lstCampaignAccount_Existing = [SELECT Id, Account__c, Campaign__c, Status__c FROM Campaign_Account__c WHERE Campaign__c = :oCampaign.Id];

		setID_Account_LinkedToCampaign = new Set<Id>();
		for (Campaign_Account__c oCampaignAccount : lstCampaignAccount_Existing){
			setID_Account_LinkedToCampaign.add(oCampaignAccount.Account__c);
		}

	}


	// This is the method which manages to remove the deselected records, and keep the records which are selected in map.
	public void updateSearchItemsMap() {

		for (wrAccount oWRAccount : lstWRAccount){

			if (oWRAccount.bSelected){
				mapHoldingSelectedRecords.put(oWRAccount.oAccount.Id, oWRAccount);
			}
			if (oWRAccount.bSelected == false && mapHoldingSelectedRecords.containsKey(oWRAccount.oAccount.Id)){
				mapHoldingSelectedRecords.remove(oWRAccount.oAccount.Id);
			}

		}

	}

		
	// Reset List View
	public PageReference resetFilter(){

		bMainSelected = false;
		stdSetController_Account = null;
		stdSetController_Account.setFilterId(tAccountFilterId);
        stdSetController_Account.setPageNumber(1);
		mapHoldingSelectedRecords.clear();
		init();
        return null;
	
	}

	
	public void selectAccount(){
		
		for (wrAccount oWRAccount : mapHoldingSelectedRecords.values()){
			mapWRAccount_Selected.put(oWRAccount.oAccount.Id, oWRAccount);
		}

	}


	public void deleteSelectedAccount(){
	
		if (idAccount_Delete != null){
			mapWRAccount_Selected.remove(idAccount_Delete);
		}

	}


	public void deleteSelectedAccount_All(){

		mapWRAccount_Selected.clear();

	}


	public void addAccountToCampaign(){

		List<wrAccount> lstWRAccount_Saved = new List<wrAccount>();
		List<Campaign_Account__c> lstCampaignAccount_Insert = new List<Campaign_Account__c>();
		for (wrAccount oWRAccount : mapWRAccount_Selected.values()){
		
				
			if (setID_Account_LinkedToCampaign.contains(oWRAccount.oAccount.Id)){

				oWRAccount.tResult = 'This Account is already linked to the Campaign';

			}else{

				if (String.isBlank(oWRAccount.oCampaignAccount.Status__c)){

					oWRAccount.tResult = 'Please select a status';

				}else{

					Campaign_Account__c oCampaignAccount = new Campaign_Account__c();
						oCampaignAccount.Campaign__c = oCampaign.Id;
						oCampaignAccount.Account__c = oWRAccount.oAccount.Id;
						oCampaignAccount.Status__c = oWRAccount.oCampaignAccount.Status__c;
					lstCampaignAccount_Insert.add(oCampaignAccount);

					lstWRAccount_Saved.add(oWRAccount);

				}

			}

		}

		if (lstCampaignAccount_Insert.size() > 0){
		
			List<Database.SaveResult> lstSaveResult = Database.insert(lstCampaignAccount_Insert, false);

			Integer iCounter = 0;
			for (Database.SaveResult oSaveResult : lstSaveResult){
				
				wrAccount oWRAccount = lstWRAccount_Saved[iCounter];
				
				if (oSaveResult.isSuccess()){

					oWRAccount.tResult = 'Record created';

				}else{
					
					String tError = '';
					for(Database.Error oError : oSaveResult.getErrors()){
						tError += oError.getMessage()+'; ';
					}
					oWRAccount.tResult = 'Error while creating the record : ' + tError;

				}
				
				iCounter++;
			}

		}

		loadAccountLinkedToCampaign();

		bMainSelected = false;
		updateSearchItemsMap();
		init();

	}

	public void applyStatusToAll(){

		for (wrAccount oWRAccount : mapWRAccount_Selected.values()){
			oWRAccount.oCampaignAccount.Status__c = oCampaignAccountStatus.Status__c;
		}

	}
	

	// Pagination Actions
	// Returns the first page of the page set
	public void firstPage(){

		bMainSelected = false;
		updateSearchItemsMap();
		stdSetController_Account.first();
		init();

	}
 
	// Returns the last page of the page set
	public void lastPage(){

		bMainSelected = false;
		updateSearchItemsMap();
		stdSetController_Account.last();
		init();

	}
 
	// Returns the previous page of the page set
	public void previousPage(){

		bMainSelected = false;
		updateSearchItemsMap();
		stdSetController_Account.previous();
		init();

	}
 
	// Returns the next page of the page set
	public void nextPage(){

		bMainSelected = false;
		updateSearchItemsMap();
		stdSetController_Account.next();
		init();

	}
	//------------------------------------------------------------------------------------------


	//------------------------------------------------------------------------------------------
	// WRAPPER
	//------------------------------------------------------------------------------------------
    public class wrAccount{

        public Boolean bSelected { get; set; }
		public Boolean bAllowDelete { get; set; }
		public Boolean bReadOnly { get; set; }
		public Account oAccount { get; set; }
		public Campaign_Account__c oCampaignAccount { get; set; }

        public String tBGColor { get; set; }
        public String tColor { get; set; }
		public String tResult { get; set; }

        public wrAccount(Account aoAccount){

            bSelected = false; 
			bReadOnly = false; 
			bAllowDelete = true;
			oAccount = aoAccount;
			oCampaignAccount = new Campaign_Account__c();

			tBGColor = '';
			tColor = '';
            tResult = '';

        }

    }	
	//------------------------------------------------------------------------------------------


}
//------------------------------------------------------------------------------------------------------------