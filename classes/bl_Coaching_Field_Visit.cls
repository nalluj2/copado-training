//  Change Log       : PMT-22787: MITG EMEA User Setup - Added record type MITG EMEA Field Visit
public class bl_Coaching_Field_Visit {
    
    public static void createShares(List<Coaching_Field_Visit__c> newRecords, Map<Id, Coaching_Field_Visit__c> oldRecords){
    	
    	Set<Id> userIds = new Set<Id>();
    	
    	Map<Id,Id> managerSharesToCreate = new Map<Id,Id>();
		Map<Id,Id> salesRepSharesToCreate = new Map<Id,Id>();
		Map<Id,Id> managerSharesToDelete = new Map<Id,Id>();	
		Map<Id,Id> salesRepSharesToDelete = new Map<Id,Id>();
	   	   
		if (Trigger.isInsert) {
	   
			// Add all Managers and SalesReps to map of Shares To create
	   
			for (Coaching_Field_Visit__c fv : newRecords) {
				
				if (fv.Manager_Mentor__c != null){
					managerSharesToCreate.put(fv.Id, fv.Manager_Mentor__c);
					userIds.add(fv.Manager_Mentor__c);
				}
				if (fv.Sales_Rep__c != null){
					salesRepSharesToCreate.put(fv.Id, fv.Sales_Rep__c);
					userIds.add(fv.Sales_Rep__c);
				}			
			}
	   
		} else {  // Trigger is Update
	      
			for(Coaching_Field_Visit__c fv : newRecords) {
	         
				// If either the manager or the sales rep has changed add the old info to a list
				// of shares to delete, and the new info to a list of shares to create
				         
				Coaching_Field_Visit__c oldFv = oldRecords.get(fv.id);
			 
				if (oldFv.Manager_Mentor__c != fv.Manager_Mentor__c){
					if(fv.Manager_Mentor__c != null){
						managerSharesToCreate.put(fv.Id, fv.Manager_Mentor__c);
						userIds.add(fv.Manager_Mentor__c);
					}
					if(oldFv.Manager_Mentor__c != null) managerSharesToDelete.put(fv.Id, oldFv.Manager_Mentor__c);
				}
				if (oldFv.Sales_Rep__c != fv.Sales_Rep__c){
					if(fv.Sales_Rep__c != null){
						salesRepSharesToCreate.put(fv.Id, fv.Sales_Rep__c);
						userIds.add(fv.Sales_Rep__c);
					}
					if(oldFv.Sales_Rep__c != null) salesRepSharesToDelete.put(fv.Id, oldFv.Sales_Rep__c);
				}		
			}
		}
		
		//DELETE SHARES
		List<Coaching_Field_Visit__Share> sharesToDelete = new List<Coaching_Field_Visit__Share>();
		
		if (managerSharesToDelete.size() > 0) {
	    	
			Map<String, Coaching_Field_Visit__Share> managerSharings = new Map<String, Coaching_Field_Visit__Share>();
			
			for(Coaching_Field_Visit__Share managerShare : [Select id, parentId, userOrGroupId from Coaching_Field_Visit__Share where parentId IN :managerSharesToDelete.keySet() AND userOrGroupId IN :managerSharesToDelete.values() AND RowCause = 'Manager_Mentor_Access__c']){
				
				String key = managerShare.parentId + ':' + managerShare.userOrGroupId;
				managerSharings.put(key, managerShare);
			}
			
			for(Id fvId : managerSharesToDelete.keySet()){
				
				Id userId = managerSharesToDelete.get(fvId);
				
				Coaching_Field_Visit__Share existingShare = managerSharings.get(fvId + ':' + userId);
				
				if(existingShare != null) sharesToDelete.add(existingShare);
			}
		}
		
		if (salesRepSharesToDelete.size() > 0) {
	    	
			Map<String, Coaching_Field_Visit__Share> salesRepSharings = new Map<String, Coaching_Field_Visit__Share>();
			
			for(Coaching_Field_Visit__Share salesRepShare : [Select id, parentId, userOrGroupId from Coaching_Field_Visit__Share where parentId IN :salesRepSharesToDelete.keySet() AND userOrGroupId IN :salesRepSharesToDelete.values() AND RowCause = 'Sales_Rep_Access__c']){
				
				String key = salesRepShare.parentId + ':' + salesRepShare.userOrGroupId;
				salesRepSharings.put(key, salesRepShare);
			}
			
			for(Id fvId : salesRepSharesToDelete.keySet()){
				
				Id userId = salesRepSharesToDelete.get(fvId);
				
				Coaching_Field_Visit__Share existingShare = salesRepSharings.get(fvId + ':' + userId);
				
				if(existingShare != null) sharesToDelete.add(existingShare);
			}
		}
		
		if(sharesToDelete.size() > 0) delete sharesToDelete;
		
		// INSERT SHARES
		List<Coaching_Field_Visit__Share> sharesToInsert = new List<Coaching_Field_Visit__Share>();
		
		Map<Id, User> activeUsers;
		
		if(userIds.size() > 0) activeUsers = new Map<Id, User>([Select Id from User where Id IN :userIds AND isActive = true]);
		else activeUsers = new Map<Id, User>();
		
		if(managerSharesToCreate.size() > 0){	
									
			for (Id fvId : managerSharesToCreate.keySet()) {
				
				Id userId = managerSharesToCreate.get(fvId);
				
				if(activeUsers.containsKey(userId)){
				
					Coaching_Field_Visit__Share share = new Coaching_Field_Visit__Share();
					share.parentId = fvId;
					share.UserOrGroupId = userId;
					share.RowCause = 'Manager_Mentor_Access__c';
					share.AccessLevel = 'Edit';
					sharesToInsert.add(share);
				}
			}
		}
	
		if(salesRepSharesToCreate.size() > 0){	
			
			for (Id fvId : salesRepSharesToCreate.keySet()) {
				
				Id userId = salesRepSharesToCreate.get(fvId);
				
				if(activeUsers.containsKey(userId)){
				
					Coaching_Field_Visit__Share share = new Coaching_Field_Visit__Share();
					share.parentId = fvId;
					share.UserOrGroupId = userId;
					share.RowCause = 'Sales_Rep_Access__c';
					share.AccessLevel = 'Edit';
					sharesToInsert.add(share);
				}
			}
		}	
		
		if(sharesToInsert.size() > 0) insert sharesToInsert;	
    }
        
    public static void checkMEATasksOnClosure(List<Coaching_Field_Visit__c> newRecords, Map<Id, Coaching_Field_Visit__c> newRecordsMap){
    	
    	//If we check the flag in the Custom Settings we skip the execution of this logic
		if(bl_Trigger_Deactivation.isTriggerDeactivated('PreventFieldVisitClosure')) return;
	
		Set<Id> newlyClosedfvIds = new Set<Id>();
    
        //PMT-22787: MITG EMEA User Setup - Added record type MITG EMEA Field Visit
		RecordType fieldVisitMEA = [Select Id from RecordType where SObjectType = 'Coaching_Field_Visit__c' AND DeveloperName = 'MITG_MEA_Field_Visit'];
        RecordType fieldVisitEMEA = [Select Id from RecordType where SObjectType = 'Coaching_Field_Visit__c' AND DeveloperName = 'MITG_EMEA_Field_Visit'];
    
	    for (Coaching_Field_Visit__c fv : newRecords){
	        
	        if(fv.RecordTypeId == fieldVisitMEA.Id && fv.Field_Visit_Status__c == 'Closed') {
				newlyClosedfvIds.add(fv.Id);
	        }
            
            if(fv.RecordTypeId == fieldVisitEMEA.Id && fv.Field_Visit_Status__c == 'Closed') {
				newlyClosedfvIds.add(fv.Id);
	        }
	    }
	
		if(newlyClosedfvIds.size() > 0){
			
			//query open tasks related to Field_Visit__c and populate map
			for (AggregateResult aggResult : [ Select Count(Id), WhatId From Task where WhatId In :newlyClosedfvIds AND IsClosed = false
												Group by WhatId Having Count(Id) > 0]) {
	
				Id fvId = (Id) aggResult.get('WhatId');
				Coaching_Field_Visit__c errorfv = newRecordsMap.get(fvId);
	
				// change error message as appropriate...
				errorfv.adderror('Cannot close Coaching/Field visit record since there are open tasks');
			}    
		}
    }
}