public without sharing class ctrl_Team_Day_Calendar {
	
	private static List<String> months = new List<String>{'January','February','March','April','May','June','July','August','September','October','November','December'};
	
	@AuraEnabled
    public static CalendarView getDay(String teamId, Date sampleDate, Decimal width, Boolean includeTravelTime) {
        
        if(width < 900) width = 900;        
        if(sampleDate == null) sampleDate = Date.today();
        
        CalendarView result = new CalendarView();
        result.selectedDay = sampleDate;
        
        Implant_Scheduling_Team__c selectedTeam = [Select Work_Day_Start__c, Work_Day_End__c, (Select Member__c, Member__r.Name from Team_Members__r ORDER BY Member__r.Name) from Implant_Scheduling_Team__c where Id = :teamId];
        
        List<TeamMember> members = new List<TeamMember>();
     	Map<String, TeamMember> memberMap = new Map<String, TeamMember>();
     	Map<String, List<CalendarItem>> events = new Map<String, List<CalendarItem>>();
     	 
     	TeamMember unassignedRequests = new TeamMember('unassigned', 'Unassigned Requests');
     	unassignedRequests.height = 50;	    		
     	members.add(unassignedRequests);
     	memberMap.put('unassigned', unassignedRequests);     	
     	events.put('unassigned', new List<CalendarItem>());
     	
     	Integer counter = 0;
     	
     	for(Implant_Scheduling_Team_Member__c teamMember : selectedTeam.Team_Members__r){
     		
     		events.put(teamMember.Member__c, new List<CalendarItem>());
     		
     		TeamMember member = new TeamMember(teamMember.Member__c, teamMember.Member__r.Name);  	
     		member.className = Math.mod(counter++, 2) == 0 ? 'row-even' : 'row-odd';	
     		member.height = 50;	
     		members.add(member);
     		memberMap.put(teamMember.Member__c, member);
     	}
     	
     	result.members = members;
     	
     	Map<Id, String> memberAddress = new Map<Id, String>();
     	
     	for(Activity_Scheduling_User_Settings__c userSettings : [Select Country__c, Post_Code__c, OwnerId from Activity_Scheduling_User_Settings__c where OwnerId IN :memberMap.keySet()]){
     		
     		memberAddress.put(userSettings.OwnerId, userSettings.Post_Code__c + ' ' + userSettings.Country__c);
     	}
        
        Integer teamStartHour = Integer.valueOf(selectedTeam.Work_Day_Start__c);
        Integer teamEndHour = Integer.valueOf(selectedTeam.Work_Day_End__c);
        
        DateTime startDateTime = DateTime.newInstance(sampleDate, Time.newInstance(0, 0, 0, 0));
     	DateTime endDateTime = DateTime.newInstance(sampleDate, Time.newInstance(23, 59, 59, 0));
     	        
        List<Event> eventList = [Select Subject, WhatId, Id, StartDateTime, EndDateTime, IsAllDayEvent, OwnerId from Event where OwnerId IN :events.keySet() AND StartDateTime < :endDateTime AND EndDateTime >= :startDateTime AND isRecurrence = false ORDER BY StartDateTime];
        List<Case> caseList = [Select Subject, Id, Status, AccountId, Account.Name, Account.BillingCity, Start_of_Procedure__c, End_of_Procedure__c, Type, Activity_Type_Picklist__c, Procedure__c, Activity_Scheduling_Team__r.Name, Activity_Scheduling_Team__r.Team_Color__c, Product_Group__r.Name from Case where Activity_Scheduling_Team__c = :teamId AND Status IN ('Open', 'Cancelled', 'Rejected') AND Is_Recurring__c = false AND Start_of_Procedure__c < :endDateTime AND End_of_Procedure__c > :startDateTime ORDER BY Start_of_Procedure__c];
        
        Integer earliestStartHour = teamStartHour;
        Integer latestEndHour = teamEndHour;
        
        Set<Id> meetingIds = new Set<Id>();
        
        for(Case caseRecord : caseList){
        	
        	if(caseRecord.Status == 'Open' && caseRecord.Type != 'Meeting'){
	        	
	        	Integer caseStart;
	        	Integer caseEnd;
	        	
	        	if(caseRecord.Start_of_Procedure__c.date() < sampleDate) caseStart = 0;
	        	else caseStart = caseRecord.Start_of_Procedure__c.hour();
	        	
	        	if(caseStart < earliestStartHour) earliestStartHour = caseStart;
	        	
	        	if(caseRecord.End_of_Procedure__c.date() > sampleDate) caseEnd = 24;
	        	else caseEnd = caseRecord.End_of_Procedure__c.minute() > 0 ? caseRecord.End_of_Procedure__c.hour() + 1 : caseRecord.End_of_Procedure__c.hour();
	        	
	        	if(caseEnd > latestEndHour) latestEndHour = caseEnd;
        	}	
        	
        	if(caseRecord.Type == 'Meeting') meetingIds.add(caseRecord.Id);     	
        }
                     	     		
     	Set<Id> caseIds = new Set<Id>();
     	
     	Map<Id, List<Event>> eventMapList = new Map<Id, List<Event>>();
     	        
        for(Event event : eventList){
        	
        	if(event.WhatId != null && String.valueOf(event.whatId).startsWith('500')){
        		
        		caseIds.add(event.WhatId);
        		
        		if(meetingIds.contains(event.WhatId)) continue;// Exclude meetings from this logic
        		
        		List<Event> memberEvents = eventMapList.get(event.OwnerId);
        		
        		if(memberEvents == null){
        			
        			memberEvents = new List<Event>();
        			eventMapList.put(event.OwnerId, memberEvents);
        		}
        		
        		memberEvents.add(event);
        	}
        }
        
        Map<Id, Case> cases = new Map<Id, Case>();
     	
     	if(caseIds.size() > 0){
     		
     		cases = new Map<Id, Case>([Select Id, Type, Subject, Procedure__c, Activity_Type_Picklist__c, AccountId, Account.Name, Account.BillingCity, Activity_Scheduling_Team__r.Name, Product_Group__r.Name, Activity_Scheduling_Team__r.Team_Color__c from Case where Id IN :caseIds]);
     	}
     	
     	Map<Id, Integer> goTravelTimeMap = new Map<Id, Integer>();
     	Map<Id, Integer> returnTravelTimeMap = new Map<Id, Integer>();
     		     		     	     	
     	DateTime currentTime = DateTime.now();     	
     		
 		for(Id memberId : eventMapList.keySet()){
 			
 			List<Event> memberEvents = eventMapList.get(memberId);
 			String memberHomeAddress = memberAddress.get(memberId);
 					     				
 			for(Integer i = 0; i < memberEvents.size(); i++){
 				
 				Event viewEvent = memberEvents[i];
 				Case eventCase = cases.get(viewEvent.Id);
 				
 				Integer goTimeMins = 0;
 				Integer returnTimeMins = 0;
 				
     			if(includeTravelTime == true){     	
 				
	 				if(viewEvent.StartDateTime > currentTime){
	 				
	     				if(i == 0){//Calculate from Home Address
	     					
	     					if(memberHomeAddress != null) goTimeMins = Integer.valueOf((Math.random() * 60).round()) + 15;
	     					
	     				}else{//Calculate from previous event
	     					
	     					Case prevCase = cases.get(memberEvents[i - 1].Id);
	     					
	     					goTimeMins = Integer.valueOf((Math.random() * 60).round()) + 15;
	     				}
	 				}
	 				
	 				if(i == (memberEvents.size() - 1) && memberHomeAddress != null && viewEvent.EndDateTime > currentTime){//Calculate trip back home
	 					
	 					returnTimeMins = Integer.valueOf((Math.random() * 60).round()) + 15;
	 				}
     			}	
 				
 				goTravelTimeMap.put(viewEvent.Id, goTimeMins);
 				returnTravelTimeMap.put(viewEvent.Id, returnTimeMins);     					
 			} 
 		}     		
     	
        for(Event event : eventList){
        	
        	if(event.WhatId != null && String.valueOf(event.whatId).startsWith('500') && meetingIds.contains(event.WhatId) == false){
        		
	        	Integer eventStart;
	        	Integer eventEnd;
	        	
	        	Integer goTimeMins = goTravelTimeMap.get(event.Id);
	        	Integer returnTimeMins = returnTravelTimeMap.get(event.Id);
	        	
	        	if(event.StartDateTime.addMinutes(-goTimeMins).date() < sampleDate) eventStart = 0;
	        	else eventStart = event.StartDateTime.addMinutes(-goTimeMins).hour();
	        	
	        	if(eventStart < earliestStartHour) earliestStartHour = eventStart;
	        	
	        	if(event.EndDateTime.addMinutes(returnTimeMins).date() > sampleDate) eventEnd = 24;
	        	else eventEnd = event.EndDateTime.addMinutes(returnTimeMins).minute() > 0 ? event.EndDateTime.addMinutes(returnTimeMins).hour() + 1 : event.EndDateTime.addMinutes(returnTimeMins).hour();
	        	
	        	if(eventEnd > latestEndHour) latestEndHour = eventEnd;		
        	}
        }
                
        List<Hour> hours = new List<Hour>();
        for(Integer i = earliestStartHour; i < latestEndHour; i++){
        	
        	Hour hour = new Hour();
        	hour.hour = i;
                	
        	if(i < teamStartHour || i >= teamEndHour) hour.isOWH = true;
        	else hour.isOWH = false;
        	
        	hours.add(hour);
        } 
                                
        Decimal pixelsPerMinute = width / ((latestEndHour - earliestStartHour) * 60);
        Integer pixelsPerHour = Integer.valueOf(pixelsPerMinute * 60);
        width = (latestEndHour - earliestStartHour) * pixelsPerHour;
        pixelsPerMinute = Decimal.valueOf(pixelsPerHour) / 60;
                       
        result.hourWidth = pixelsPerHour;
        result.hours = hours;
                
        result.headerLabel = sampleDate.day() + ' ' + months[sampleDate.month() - 1] + ' ' + sampleDate.year();
     	
     	startDateTime = DateTime.newInstance(sampleDate, Time.newInstance(earliestStartHour, 0, 0, 0));
     	if(latestEndHour == 24) endDateTime = DateTime.newInstance(sampleDate, Time.newInstance(23, 59, 59, 0));
     	else endDateTime = DateTime.newInstance(sampleDate, Time.newInstance(latestEndHour, 0, 0, 0));
     	
     	for(Event viewEvent : eventList){
     		
     		Case eventCase;     		
     		if(viewEvent.WhatId != null && String.valueOf(viewEvent.whatId).startsWith('500')) eventCase = cases.get(viewEvent.WhatId);
     			
	     	List<CalendarItem> memberEvents = events.get(viewEvent.OwnerId);
	     		     		
	     	CalendarItem calItem = new CalendarItem();
     		calItem.id = viewEvent.Id;     		
     		
     		if(eventCase == null){
     			
     			calItem.subject = viewEvent.subject;
     			calItem.teamName = 'Time-off';
     			calItem.teamColor = 'rgb(' + ctrl_Activity_Scheduling_App.timeOffColor + ')';
     			calItem.color = 'rgba(' + ctrl_Activity_Scheduling_App.timeOffColor + ', 0.8)';
     			calItem.caseType = 'event';
     			     			
     			if(viewEvent.IsAllDayEvent == true){
	     			
	     			viewEvent.StartDateTime = DateTime.newInstance(viewEvent.StartDateTime.date(), Time.newInstance(0, 0, 0, 0));
	 				viewEvent.EndDateTime = DateTime.newInstance(viewEvent.EndDateTime.date().addDays(1), Time.newInstance(0, 0, 0, 0));
	     		}
     		
     		}else{
     			
     			calItem.caseId = viewEvent.WhatId;
     			calItem.teamName = eventCase.Activity_Scheduling_Team__r.Name;
     			calItem.teamColor = eventCase.Activity_Scheduling_Team__r.Team_Color__c;
     			calItem.subject = (eventCase.Type == 'Meeting' ? eventCase.Subject : eventCase.Account.Name);
     			calItem.accountId = eventCase.AccountId;
     			calItem.location = (eventCase.AccountId != null ? eventCase.Account.BillingCity : null);
     					     					
     			if(eventCase.Type == 'Implant Support Request'){
     						
     				calItem.color = 'rgba(' + ctrl_Activity_Scheduling_App.implantColor + ', 0.8)';
     				calItem.caseType = 'implant';    
     				calItem.subject += ' / ' + eventCase.Procedure__c + ' / ' + eventCase.Product_Group__r.Name; 	
     				 
     				calItem.procedure = eventCase.Procedure__c;					
     							
     			}else if(eventCase.Type == 'Service Request'){
     					
     				calItem.color = 'rgba(' + ctrl_Activity_Scheduling_App.serviceColor + ', 0.8)';
     				calItem.caseType = 'service';     	
     				calItem.subject += ' / ' + eventCase.Activity_Type_Picklist__c;     				 	
     				calItem.activityType = eventCase.Activity_Type_Picklist__c;		
     						
     			}else{
 					
 					calItem.color = 'rgba(' + ctrl_Activity_Scheduling_App.meetingColor + ', 0.8)';
 					calItem.caseType = 'meeting';
 				}
     		} 
     		
     		calItem.timeframe = getTimeFrame(viewEvent.StartDateTime, viewEvent.EndDateTime);
     		
     		Integer goTimeMins = goTravelTimeMap.get(viewEvent.Id);
     		Integer returnTimeMins = returnTravelTimeMap.get(viewEvent.Id);
     		
     		if(goTimeMins == null) goTimeMins = 0;
     		if(returnTimeMins == null) returnTimeMins = 0;	
     			     		    		
     		Time startTime;
     		if(viewEvent.StartDateTime.addMinutes(-goTimeMins) < startDateTime) startTime = startDateTime.time();
     		else startTime = viewEvent.StartDateTime.addMinutes(-goTimeMins).Time();
     		     		
     		Decimal left = ((startTime.hour() - startDateTime.time().hour()) * pixelsPerHour) + (startTime.minute() * pixelsPerMinute);
     		calItem.left = Integer.valueOf(left);
     		
     		if(startTime < viewEvent.StartDateTime.time()){
     			
     			Time eventTime = viewEvent.StartDateTime.time();
     			calItem.goTimeWidth = Integer.valueOf((((eventTime.hour() - startTime.hour()) * 60) + eventTime.minute() - startTime.minute()) * pixelsPerMinute);
     			calItem.goTime = timeToString(goTimeMins);
     			
     		}else{
     			
     			calItem.goTimeWidth = 0;
     		}
     		
     		Time endTime;
     		if(viewEvent.EndDateTime.addMinutes(returnTimeMins) > endDateTime) endTime = endDateTime.time();
     		else endTime = viewEvent.EndDateTime.addMinutes(returnTimeMins).Time();
     		     		     		     			
     		Decimal eventwidth = ((endTime.hour() - startTime.hour()) * pixelsPerHour) + ((endTime.minute() - startTime.minute()) * pixelsPerMinute); 			
     		calItem.width = Integer.valueOf(eventwidth) - 1;
     		    		
     		if(endTime > viewEvent.EndDateTime.time()){
     			
     			Time eventTime = viewEvent.EndDateTime.time();
     			calItem.returnTimeWidth = Integer.valueOf((((endTime.hour() - eventTime.hour()) * 60) + endTime.minute() - eventTime.minute()) * pixelsPerMinute);
     			
     			calItem.returnTime = timeToString(returnTimeMins);
     			
     		}else{
     			
     			calItem.returnTimeWidth = 0;
     		}
     		          		
     		calItem.height = 44;     		
     		calItem.zIndex = (memberEvents.size() + 1);
     		
     		if(calItem.width <= 0 ) continue;
     		
     		memberEvents.add(calItem);	     	
     	}
     	
     	Boolean hasUnassigned = false;
     	
     	for(Case viewCase : caseList){
     		
     		if(viewCase.Status == 'Open' && viewCase.Type != 'Meeting') hasUnassigned = true;
     			
	     	List<CalendarItem> memberEvents = events.get('unassigned');
	     	
	     	caseIds.add(viewCase.Id);
	     		     		
	     	CalendarItem calItem = new CalendarItem();
     		calItem.id = viewCase.Id;
     		calItem.caseId = viewCase.Id;
     		calItem.subject = (viewCase.Type == 'Meeting' ? viewCase.Subject : viewCase.Account.Name); 
     		calItem.timeframe = getTimeFrame(viewCase.Start_of_Procedure__c, viewCase.End_of_Procedure__c);  
     		calItem.accountId = viewCase.AccountId;
     		calItem.location = (viewCase.AccountId != null ? viewCase.Account.BillingCity : null); 	
     		     		 		
     		if(viewCase.Status == 'Open'){
     		
     			calItem.teamName = viewCase.Activity_Scheduling_Team__r.Name;
 				calItem.teamColor = viewCase.Activity_Scheduling_Team__r.Team_Color__c;
 				
     		}else{
     			
     			calItem.subject = '[' + viewCase.Status + '] ' + calItem.subject;
     			calItem.teamName = viewCase.Status;
 				calItem.teamColor = 'rgb(' + ctrl_Activity_Scheduling_App.cancelRejectedColor + ')';
     		}
 			 					     					
 			if(viewCase.Type == 'Implant Support Request'){
 						
 				if(viewCase.Status == 'Open') calItem.color = 'rgba(' + ctrl_Activity_Scheduling_App.implantColor + ', 0.8)';
 				else  calItem.color = 'rgba(' + ctrl_Activity_Scheduling_App.cancelRejectedColor + ', 0.8)';
 				calItem.caseType = 'implant';    
 				calItem.subject += ' / ' + viewCase.Procedure__c + ' / ' + viewCase.Product_Group__r.Name; 				 	
 				calItem.procedure = viewCase.Procedure__c;					
 							
 			}else if(viewCase.Type == 'Service Request'){
 						
 				if(viewCase.Status == 'Open') calItem.color = 'rgba(' + ctrl_Activity_Scheduling_App.serviceColor + ', 0.8)';
 				else calItem.color = 'rgba(' + ctrl_Activity_Scheduling_App.cancelRejectedColor + ', 0.8)';
 				calItem.caseType = 'service';     	
 				calItem.subject += ' / ' + viewCase.Activity_Type_Picklist__c; 				
 				calItem.activityType = viewCase.Activity_Type_Picklist__c;
 									
 			}else{
 				
 				calItem.color = 'rgba(' + ctrl_Activity_Scheduling_App.meetingColor + ', 0.8)'; 				
 				calItem.caseType = 'meeting';				
 			} 
     		
     		Time startTime;
     		if(viewCase.Start_of_Procedure__c < startDateTime) startTime = startDateTime.time();
     		else startTime = viewCase.Start_of_Procedure__c.Time();
     		     		
     		Decimal left = ((startTime.hour() - startDateTime.time().hour()) * pixelsPerHour) + (startTime.minute() * pixelsPerMinute);
     		calItem.left = Integer.valueOf(left);
     		
     		Time endTime;
     		if(viewCase.End_of_Procedure__c > endDateTime) endTime = endDateTime.time();
     		else endTime = viewCase.End_of_Procedure__c.Time();
     		     		     		     			
     		Decimal eventwidth = ((endTime.hour() - startTime.hour()) * pixelsPerHour) + ((endTime.minute() - startTime.minute()) * pixelsPerMinute); 			
     		calItem.width = Integer.valueOf(eventwidth) - 1;     		
     		
     		calItem.height = 44;     		
     		calItem.zIndex = (memberEvents.size() + 1);
     		
     		if(calItem.width <= 0 ) continue;			     					
			
     		memberEvents.add(calItem);	     	
     	}
     	
     	if(hasUnassigned) memberMap.get('unassigned').className = 'unassigned-pending';
     	else memberMap.get('unassigned').className = 'unassigned-clear';
     	
     	for(String memberId : events.keySet()){
     		
     		List<CalendarItem> memberItems = events.get(memberId);
     		TeamMember member = memberMap.get(memberId);
     		
     		if(memberItems.size() == 0) continue;
     		
     		if(hasOverlaps(memberItems)){
     		
	     		Integer maxOverlaps = 0;
	     		
	     		for(Integer i = 0; i < width; i++){
	     		
	     			List<CalendarItem> pixelItems = getOverlaps(i, memberItems);
	     		
	     			if(pixelItems.size() > maxOverlaps) maxOverlaps = pixelItems.size();     			     		
	     		}
	     		
	     		member.height = (maxOverlaps * 45) + 5;	     		
	     			     		
	     		for(Integer i = 0; i < width; i++){
     		
	     			List<CalendarItem> pixelItems = getOverlaps(i, memberItems);
	     			
	     			Set<Integer> usedSlots = new Set<Integer>();
	     			
	     			for(CalendarItem item : pixelItems){
	     				
	     				if(item.top == null){
	     					
	     					Integer top = 0;
	     					
	     					while(true){
	     						
	     						if(usedSlots.contains(top) == false){
	     							
	     							item.top = top;
	     							usedSlots.add(top);
	     							break;
	     						}
	     						
	     						top += 45;
	     					}
	     					
	     				}else{
	     					     					     						
	     					usedSlots.add(item.top);    					
	     				}
	     			}     		
     			}     		
	     		
     		}else{
     			
     			member.height = 50;
     			
     			for(CalendarItem item : memberItems){
     				
     				item.top = 0;
     			}
     		}
     	}
     	   	
     	result.events = events;
     	
     	return result;   
    }
    
    private static String getTimeFrame(DateTime startDT, DateTime endDT){
    	
    	if(endDT.Year() != startDT.Year()){
    		
    		return startDT.format('dd MMM yyyy H:mm') + ' - ' + endDT.format('dd MMM yyyy H:mm');
    		
    	}else if(endDT.Month() != startDT.Month() || endDT.Day() != startDT.Day()){
    		
    		return startDT.format('dd MMM H:mm') + ' - ' + endDT.format('dd MMM H:mm');
    		
    	}else{
    		
    		return startDT.format('H:mm') + ' - ' + endDT.format('H:mm');
    	}
    }
    
    private static String timeToString(Integer timeMins){
    	
    	String hours = String.valueOf(timeMins / 60);
    	if(hours.length() == 1) hours = '0'+hours;
    	
    	String mins = String.valueOf(Math.mod(timeMins, 60));
    	if(mins.length() == 1) mins = '0'+mins;
    	
    	return hours + ':' + mins;
    }
    
    private static Boolean hasOverlaps(List<CalendarItem> items){
    	
    	for(Integer i = 0; i < items.size(); i++){
    		
    		CalendarItem item = items[i];
    		
    		for(Integer j = (i + 1); j < items.size(); j++){
    				
				CalendarItem otherItem = items[j];
    				
				if(otherItem.left <= (item.left + item.width) && (otherItem.left + otherItem.width) >= item.left) return true;
    		}	    		
    	}
    	
    	return false;
    }
    
    private static List<CalendarItem> getOverlaps(Integer pixel, List<CalendarItem> items){
    	
    	List<CalendarItem> pixelItems = new List<CalendarItem>();
    	
    	for(CalendarItem item : items){
    		
    		if(pixel >= item.left && pixel <= (item.left + item.width)){
    			
    			pixelItems.add(item);
    		}    		
    	}
    	
    	return pixelItems;
    }
        
    public class CalendarView {
    	
    	@AuraEnabled public String headerLabel {get; set;}
    	@AuraEnabled public Date selectedDay {get; set;}
    	@AuraEnabled public Integer hourWidth {get; set;}
    	@AuraEnabled public List<Hour> hours {get; set;}    	
    	@AuraEnabled public List<TeamMember> members {get; set;}
    	@AuraEnabled public Map<String, List<CalendarItem>> events {get; set;}
    }
    
    
    public class TeamMember {
    	
    	@AuraEnabled public String id { get; set; }
    	@AuraEnabled public String name { get; set; }
    	@AuraEnabled public String className { get; set; }
    	@AuraEnabled public Integer height { get; set; }
    	
    	public TeamMember(String inputId, String inputName){
    		
    		id = inputId;
    		name = inputName;    		
    	}
    }
    
    public class Hour {
    	
    	@AuraEnabled public Integer hour { get; set; }
    	@AuraEnabled public Boolean isOWH { get; set; }
    }
    
    public class CalendarItem {
    	
    	@AuraEnabled public String id { get; set; }
    	@AuraEnabled public String subject { get; set; }
    	@AuraEnabled public String location { get; set; }
    	@AuraEnabled public String timeframe { get; set; }
    	@AuraEnabled public Integer width { get; set; }
    	@AuraEnabled public Integer top { get; set; }
    	@AuraEnabled public Integer left { get; set; }
    	@AuraEnabled public Integer goTimeWidth { get; set; }
    	@AuraEnabled public String goTime { get; set; }
    	@AuraEnabled public Integer returnTimeWidth { get; set; }
    	@AuraEnabled public String returnTime { get; set; }
    	@AuraEnabled public Integer height { get; set; }
    	@AuraEnabled public Integer zIndex { get; set; }
    	@AuraEnabled public String color { get; set; }
    	@AuraEnabled public String teamName { get; set; }
    	@AuraEnabled public String teamColor { get; set; }
    	@AuraEnabled public Integer maxOverlap { get; set; }
    	@AuraEnabled public String caseId { get; set; }
    	@AuraEnabled public String caseType { get; set; }
    	@AuraEnabled public String accountId { get; set; }
    	@AuraEnabled public String procedure { get; set; }
    	@AuraEnabled public String activityType { get; set; }
    }    
}