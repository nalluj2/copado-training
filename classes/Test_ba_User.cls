//--------------------------------------------------------------------------------------------------------------------
//  Description : This TEST APEX Class is used to test the logic in Test_ba_User
//  Version     : 1.0
//  Author      : Bart Caelen
//  Date        : 07/09/2016
//--------------------------------------------------------------------------------------------------------------------
@isTest private class Test_ba_User {
	
    static Integer iBatchSize = 1;
    static String tUserName = clsUtil.getTimeStamp() + '@test.medtronic.com';

    @testSetup static void createTestData() {

    	//---------------------------------------------------------------------------
    	// Create Test Data
    	//---------------------------------------------------------------------------
    	// Create User Data
    	User oUser = clsTestData_User.createUser(false);
    		oUser.Username = tUserName;
    		oUser.Last_Application_Login__c = Datetime.now();
    		oUser.Last_Application_Login_Status__c = 'Not Logged In Last 90 Days';
    	insert oUser;
    	//---------------------------------------------------------------------------

    }

    @isTest static void test_ba_User_Scheduling() {

        Test.startTest();

        //---------------------------------------
        // TEST SCHEDULING
        //---------------------------------------
        String tCRON_EXP = '0 0 0 3 9 ? 2099';
        
        String tJobId = System.schedule('ba_User_TEST', tCRON_EXP, new ba_User());

        // Get the information from the CronTrigger API object
        CronTrigger oCronTrigger = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :tJobId];

        // Verify the expressions are the same
        System.assertEquals(tCRON_EXP, oCronTrigger.CronExpression);

        // Verify the job has not run
        System.assertEquals(0, oCronTrigger.TimesTriggered);

        // Verify the next time the job will run
        System.assertEquals('2099-09-03 00:00:00', String.valueOf(oCronTrigger.NextFireTime));
        //---------------------------------------

        Test.stopTest();
    
    }
    

    @isTest static void test_ba_User() {

        Test.startTest();

        //---------------------------------------
        // TEST BATCH
        //---------------------------------------
		String tQuery = 'SELECT Id, Last_Application_Login__c, Last_Application_Login_Status__c, CreatedDate FROM User WHERE Username = \'' + tUserName + '\'';
    	ba_User oBatch = new ba_User(tQuery, iBatchSize);
        Database.executebatch(oBatch, iBatchSize);        	
        //---------------------------------------

        Test.stopTest();
    }   

}
//--------------------------------------------------------------------------------------------------------------------