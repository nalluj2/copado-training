public with sharing class ctrl_Custom_Lookup {
	
	@AuraEnabled
    public static List<SObject> searchAccount(String searchText, String objectType) {
    	
    	List<SObject> result;
    	
    	if(objectType == 'Account'){
    		
    		result = [FIND :searchText IN ALL FIELDS RETURNING Account(Id, Name, SAP_Id__c, BillingStreet, BillingCity, BillingPostalCode, BillingCountry,Buying_Group_Number__c LIMIT 10)][0];
    		
    	}else if(objectType == 'User'){
    		
    		result = [FIND :searchText IN ALL FIELDS RETURNING User(Id, Name, FederationIdentifier, Country, Job_Title_vs__c LIMIT 10)][0];
    		
    	} 
    	
    	return result;
    }    
}