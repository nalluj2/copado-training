public with sharing class ControllerCallRecordCustomDelete {
    private final Call_Records__c delAcc;
    public static string warnText; 
    private String delId ; 
    public Boolean CanCloseWindow = false ;
    public string strException{get; set;}   
    public Boolean ShowOkButton = true ;
    public boolean ChkDelRight=true;
    public void setCanCloseWindow(Boolean st){  this.CanCloseWindow = st    ;   }
    public Boolean getCanCloseWindow(){         return this.CanCloseWindow  ;   }

    public void setShowOkButton(Boolean Val){   this.ShowOkButton = Val     ;   }
    public Boolean getShowOkButton(){           return this.ShowOkButton  ; }
        
    public boolean getChkDelRight(){return this.ChkDelRight;}
    public ControllerCallRecordCustomDelete(ApexPages.StandardController impController) {
        this.delAcc = (Call_Records__c)impController.getRecord();

        if (ApexPages.currentPage().getParameters().get('id') != null){
            delId = ApexPages.currentPage().getParameters().get('id');
        }
        Call_Records__c delImplant=[select id,Implant__c,Implant__r.MMX_Implant_ID_Text__c from Call_Records__c
         where id = :delId];
        
        if(delImplant.Implant__r.MMX_Implant_ID_Text__c!=null){
            warnText='This Call Record is linked to MMX Implant so it can not be deleted.';
            ShowOkButton=false; 
        } else if(delImplant.Implant__c!=null) {            
            warnText='This Call Record is linked to an Implant.<br /> Deleting this Call Record will delete the Implant as well.<br /> Are you sure?';              
        }
        else {
            warnText='Are you sure?';
        }               
    }   

public pagereference checkDeleteRights(){
 
     try{
            Savepoint sp = Database.setSavepoint(); 
             if (ApexPages.currentPage().getParameters().get('id') != null){
             Call_Records__c cr=[select id from Call_Records__c where id=:ApexPages.currentPage().getParameters().get('id')];
             delete cr;  

             }
                 //this.delAcc = (Call_Records__c)impController.getRecord();
             Database.rollback(sp); 
          return null;   
        }
        catch(DmlException ex){
          ChkDelRight=false;
          //ApexPages.addMessages('Insufficient Previlleges');
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Insufficient Privileges'));
          return null;
        }  
    

}


    
// Show the message on the VF page.
    public string getWarntext(){
        return warntext;
    } 

    public PageReference DelImplant(){
        if (delId!=null){
         list<Contact> conUpdate = new list<Contact>();
         list<Account> accUpdate = new list<Account>();
         set<Id> setAccIds = new set<id>();
         list<Contact_Visit_Report__c> conVisit = new list<Contact_Visit_Report__c>();
         conVisit = [select Attending_Contact__c, Attending_Affiliated_Account__c from Contact_Visit_Report__c where Call_Records__c=:delId];
         if(conVisit!=null && conVisit.size()>0){
            for(Contact_Visit_Report__c CV:conVisit){
                if(CV.Attending_Contact__c!=null){
                    Contact c = new Contact(id=CV.Attending_Contact__c);
                    conUpdate.add(c);
                }
                if(CV.Attending_Affiliated_Account__c!=null){
                	setAccIds.add(CV.Attending_Affiliated_Account__c);
                }
            }
         }  
         for(id AccId:setAccIds){
	        Account a = new Account(id=AccId);
	        accUpdate.add(a);         	
         }
         Call_Records__c delImplant=[select id from Call_Records__c where id = :delId];
         if(delImplant!=null){
            try{
                Delete delImplant;
                CanCloseWindow=true;    
            }
            catch (DmlException e){
                strException = e.getMessage();
                CanCloseWindow=false;
            }
            try{
                if(conUpdate!=null && conUpdate.size()>0){
                    update conUpdate;
                }
            } catch (DmlException e){
                
            }

            try{
                if(accUpdate!=null && accUpdate.size()>0){
                    update accUpdate;
                }
            } catch (DmlException e){
                
            }

         }          
        }   
        Pagereference pg=new Pagereference('/a0Q/o');
        pg.setRedirect(true);
        return pg;
    }   
   public PageReference CnclImplant(){
        Pagereference pg=new Pagereference('/'+ApexPages.currentPage().getParameters().get('id'));
        pg.setRedirect(true);
        return pg;
   }
}