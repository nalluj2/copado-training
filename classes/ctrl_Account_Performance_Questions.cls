public with sharing class ctrl_Account_Performance_Questions {
		    
	@AuraEnabled
    public static Formulaire getUserFormulaire(String purpose, String role, String filter) {
    	
    	try{
    	
	    	Formulaire result = new Formulaire();    	
	    	result.accountInputs = new List<FormulaireAccountInput>();
	    	result.questions = new List<FormulaireQuestion>();
	    	
	    	List<UMD_Purpose__c> questionPurposes = [Select Id, Fiscal_Year_VS__c from UMD_Purpose__c where Unique_Qualification_Key__c = :purpose];
	    	
	    	if(questionPurposes.size() == 1){
	    		
	    		UMD_Purpose__c questionPurpose = questionPurposes[0];
	    			    			    		    		
	    		List<UMD_Question__c> questions = new List<UMD_Question__c>();
	    		Set<Id> questionIds = new Set<Id>();
	    		
	    		for(UMD_Purpose_Question__c purposeQuestion : [Select Sub_Business_Unit__c, Business_Unit_Group__c, UMD_Question__c, UMD_Question__r.Answers__c, UMD_Question__r.Helptext_Rich__c, UMD_Question__r.Max_Value__c, UMD_Question__r.Min_Value__c, UMD_Question__r.Question__c, UMD_Question__r.Type__c 
												from UMD_Purpose_Question__c 
												Where UMD_Purpose__c = :questionPurpose.Id AND UMD_Question__r.Source_of_answer__c = 'User' ORDER BY Sequence__c ASC]){
					
					if(purposeQuestion.Business_Unit_Group__c !=null && purposeQuestion.Business_Unit_Group__c.contains(filter) == false) continue;					
					if(purposeQuestion.Sub_Business_Unit__c != null && purposeQuestion.Sub_Business_Unit__c.contains(filter) == false) continue;
						
					//If the Question was not added already				
					if(questionIds.add(purposeQuestion.UMD_Question__c)){
										
						result.questions.add(new FormulaireQuestion(purposeQuestion.UMD_Question__r));
						questions.add(purposeQuestion.UMD_Question__r);
					}
				}
	    		    		
	    		String query = 'SELECT Id, Account__c, Account__r.Name, Account__r.BillingCity , Account__r.SAP_ID__c, CreatedBy.Name, CreatedDate, Complete__c, Approved__c, Assigned_To__c, Assigned_To__r.Name, Approved_2__c, Approver_1__c, Approver_1__r.Name, Approver_2__c,  Approver_2__r.Name, UMD_Purpose__r.Fiscal_Year_VS__c, Sub_Business_Unit__c, Business_Unit_Group__c,';
	    		query += ' Current_Segmentation__r.Behavioral_Segment__c, Current_Segmentation__r.Strategical_Segment__c,';    		
	    		query += ' (SELECT Field, CreatedBy.Name, CreatedDate FROM Histories ORDER BY CreatedDate DESC)';
	    		query += ' from UMD_Input_Complete__c where UMD_Purpose__c = \'' + questionPurpose.Id + '\' ';
	    		
	    		String queryAnswer = 'SELECT Id, Answer__c, UMD_Input_Complete__c, UMD_Question__c, Unique_Key__c, CreatedBy.Name, CreatedDate, (SELECT Field, CreatedBy.Name, CreatedDate FROM Histories ORDER BY CreatedDate DESC) from UMD_Input__c where UMD_Input_Complete__r.UMD_Purpose__c = \'' + questionPurpose.Id + '\' ';
	    		
	    		if(role == 'Input'){
	    			
	    			query += ' AND Assigned_To__c = \'' + UserInfo.getUserId() + '\' ';
	    			queryAnswer += ' AND UMD_Input_Complete__r.Assigned_To__c = \'' + UserInfo.getUserId() + '\' ';
	    			
	    		}else{
	    			
	    			query += ' AND (Approver_1__c = \'' + UserInfo.getUserId() + '\' OR  Approver_2__c = \'' + UserInfo.getUserId() + '\') ';
	    			queryAnswer += ' AND ( UMD_Input_Complete__r.Approver_1__c = \'' + UserInfo.getUserId() + '\' OR  UMD_Input_Complete__r.Approver_2__c = \'' + UserInfo.getUserId() + '\') ';    			
	    		}
	    		
	    		if(purpose == 'Behavioral'){
	    			    			
	    			query += 'AND Sub_Business_Unit__c = :filter';
	    			queryAnswer += 'AND UMD_Input_Complete__r.Sub_Business_Unit__c = :filter';
	    			
	    		}else if(purpose == 'Strategical'){
	    			
	    			query += 'AND Business_Unit_Group__c = :filter';
	    			queryAnswer += 'AND UMD_Input_Complete__r.Business_Unit_Group__c = :filter';
	    		}
	    		
	    		query += ' ORDER BY Account__r.Name, Account__c';
	    		
	    		Map<Id, List<UMD_Input__c>> inputAnswerMap = new Map<Id, List<UMD_Input__c>>();
	    		
	    		for(UMD_Input__c inputAnswer : Database.query(queryAnswer)){
	    			
	    			List<UMD_Input__c> inputAnswers = inputAnswerMap.get(inputAnswer.UMD_Input_Complete__c);
					
					if(inputAnswers == null){
						
						inputAnswers = new List<UMD_Input__c>();
						inputAnswerMap.put(inputAnswer.UMD_Input_Complete__c, inputAnswers);
					}
					
					inputAnswers.add(inputAnswer);	
	    		}    		
	    		
	    		Integer fiscalYear = Integer.valueOf(questionPurpose.Fiscal_Year_VS__c.split('FY')[1]);
	    		String prevFiscalYear = 'FY' + (fiscalYear - 1);
	    		
	    		Set<String> prevKeys = new Set<String>();
	    		
	    		for(UMD_Input_Complete__c inputComplete : Database.query(query)){
	    													
					FormulaireAccountInput accInput = new FormulaireAccountInput();
					accInput.record = inputComplete;
					accInput.fiscalYear = fiscalYear;
					
					if(inputComplete.Current_Segmentation__r != null){
						
						if(purpose == 'Behavioral') accInput.accountSegment = inputComplete.Current_Segmentation__r.Behavioral_Segment__c;
						else if(purpose == 'Strategical') accInput.accountSegment = inputComplete.Current_Segmentation__r.Strategical_Segment__c;
					}
					
					if(accInput.accountSegment == null || accInput.accountSegment == 'Not Segmented') accInput.accountSegment = '-';
					
					if(inputComplete.Complete__c == true) accInput.completedTS = getTimeStamp(inputComplete, 'Complete__c');
		    		else accInput.completedTS = inputComplete.Assigned_To__r.Name;	    		
		    		if(inputComplete.Approved__c == true) accInput.approved1TS = getTimeStamp(inputComplete, 'Approved__c');
		    		else if(inputComplete.Approver_1__c != null) accInput.approved1TS = inputComplete.Approver_1__r.Name;	    		
		    		if(inputComplete.Approved_2__c == true) accInput.approved2TS = getTimeStamp(inputComplete, 'Approved_2__c');
		    		else if(inputComplete.Approver_2__c != null) accInput.approved2TS = inputComplete.Approver_2__r.Name;
		    						
					Map<Id, UMD_Input__c> savedAnswers = new Map<Id, UMD_Input__c>();
					
					List<UMD_Input__c> inputAnswers = inputAnswerMap.get(inputComplete.Id);
					
					if(inputAnswers != null){
						for(UMD_Input__c savedInput : inputAnswers){
							
							savedAnswers.put(savedInput.UMD_Question__c, savedInput);
						}
					}
					
					accInput.accountAnswers = new List<FormulaireAccountInputAnswer>();
					
					for(UMD_Question__c question : questions){
						
						FormulaireAccountInputAnswer inputAnswer = new FormulaireAccountInputAnswer();
						
						UMD_Input__c input = savedAnswers.get(question.Id);
						
						if(input == null){
							
							input = new UMD_Input__c();
							input.UMD_Question__c = question.Id;
							input.UMD_Input_Complete__c = inputComplete.Id;
							input.Unique_Key__c = inputComplete.Id + ':' + question.Id;
							
						}else{
							
							inputAnswer.answerTS = getTimeStamp(input, 'Answer__c');
						}					
						
						inputAnswer.record = input;
						
						accInput.accountAnswers.add(inputAnswer);
					}
					
					result.accountInputs.add(accInput);
					
					prevKeys.add(inputComplete.Account__c + ':' + filter + ':' + prevFiscalYear);
				}
				
	    		Map<String, UMD_Input__c> prevAnswers = new Map<String, UMD_Input__c>();
	    		    		    		    			
	    		String prevQuery = 'Select Answer__c, UMD_Question__c, UMD_Input_Complete__r.Account__c from UMD_Input__c where UMD_Input_Complete__r.Unique_Key__c IN :keys';
		    	    		
	    		for(SObject record : bl_Without_Sharing_Service.queryByKeys(prevQuery, prevKeys)){
	    			
	    			UMD_Input__c inputAnswer = (UMD_Input__c) record;
	    			prevAnswers.put(inputAnswer.UMD_Input_Complete__r.Account__c + ':' + inputAnswer.UMD_Question__c, inputAnswer);
	    		}
				
	    		for(FormulaireAccountInput accInput : result.accountInputs){
	    			
	    			accInput.prevAccountAnswers = new List<FormulaireAccountInputPrevAnswer>();
	    			Id accountId = accInput.record.Account__c;
	    			    			
	    			for(UMD_Question__c question : questions){
						
						FormulaireAccountInputPrevAnswer inputAnswer = new FormulaireAccountInputPrevAnswer();
						inputAnswer.questionType = question.Type__c;
						
						UMD_Input__c input = prevAnswers.get(accountId + ':' + question.Id);
						
						if(input != null){
							
							inputAnswer.value = input.Answer__c;
							
							if(question.Type__c == 'Picklist (single value)' || question.Type__c == 'Yes/No'){
								
								inputAnswer.formattedValue = input.Answer__c;
								
							}else if(question.Type__c == 'Number'){
								
								inputAnswer.formattedValue = input.Answer__c;
								
							}else if(question.Type__c == 'Picklist (multiple value)'){
								
								inputAnswer.formattedValue = '';
								
								for(String answerValue : input.Answer__c.split(';')){
									
									inputAnswer.formattedValue += '- ' + answerValue + '<br/>';	
								}							
							}										
						}					 
						
						accInput.prevAccountAnswers.add(inputAnswer);
					}
	    		}			
	    	}
	    	
	    	return result;
    	
    	}catch(Exception e){
    		
    		System.debug(e.getMessage() + ' | ' + e.getStackTraceString());
    		AuraHandledException auraEx = new AuraHandledException(e.getMessage()); 
    		if(test.isRunningTest()) auraEx.setMessage(e.getMessage());
    		throw auraEx;
    	}
    }
    
    public static String getTimeStamp(UMD_Input_Complete__c accInput, String fieldName){
    	
    	String timeStamp;
    	
    	for(UMD_Input_Complete__History fieldHistory : accInput.Histories){
    		
    		if(fieldHistory.Field == fieldName){
    			
    			timeStamp = fieldHistory.CreatedBy.Name + ', ' + fieldHistory.CreatedDate.format('d MMM yyyy HH:mm');
    			break;
    		}
    	}
    	
    	if(timeStamp == null) timeStamp = accInput.CreatedBy.Name + ', ' + accInput.CreatedDate.format('d MMM yyyy HH:mm');
    	
    	return timeStamp;
    }
    
    public static String getTimeStamp(UMD_Input__c inputAnswer, String fieldName){
    	
    	String timeStamp;
    	
    	for(UMD_Input__History fieldHistory : inputAnswer.Histories){
    		
    		if(fieldHistory.Field == fieldName){
    			
    			timeStamp = fieldHistory.CreatedBy.Name + ', ' + fieldHistory.CreatedDate.format('d MMM yyyy HH:mm');
    			break;
    		}
    	}
    	
    	if(timeStamp == null) timeStamp = inputAnswer.CreatedBy.Name + ', ' + inputAnswer.CreatedDate.format('d MMM yyyy HH:mm');
    	
    	return timeStamp;
    }
    
    @AuraEnabled
    public static List<FormulaireAccountInput> saveUserFormulaire(List<FormulaireAccountInput> formulaire) {
    	
    	System.debug('Input size: ' + formulaire.size());
    	
    	bl_Customer_Segmentation.fromApp = true;
    	
    	System.SavePoint sp = Database.setSavepoint();
    	
    	try{
    		
    		List<UMD_Input_Complete__c> accountInputCompletes = new List<UMD_Input_Complete__c>();
    		List<UMD_Input__c> accountInputs_Upsert = new List<UMD_Input__c>();
    		List<UMD_Input__c> accountInputs_Delete = new List<UMD_Input__c>();
    		
    		for(FormulaireAccountInput formAccountInput : formulaire){
    			
    			if(formAccountInput.changed == true){
    				
    				accountInputCompletes.add(formAccountInput.record);    				    				
    			}
    			
    			for(FormulaireAccountInputAnswer inputAnswer : formAccountInput.accountAnswers){
    				
    				if(inputAnswer.changed == true){
    					
    					if(inputAnswer.record.Answer__c != null && inputAnswer.record.Answer__c != '') accountInputs_Upsert.add(inputAnswer.record);
    					else if(inputAnswer.record.Id != null){
    						
    						UMD_Input__c toDelete = inputAnswer.record;
    						accountInputs_Delete.add(toDelete);
    						
    						inputAnswer.record = new UMD_Input__c();
							inputAnswer.record.UMD_Question__c = toDelete.UMD_Question__c;
							inputAnswer.record.UMD_Input_Complete__c = toDelete.UMD_Input_Complete__c;
							inputAnswer.record.Unique_Key__c = toDelete.Unique_Key__c;
							inputAnswer.answerTS = null; 
    					}
    				}
    			}
    		}
    		
    		if(accountInputCompletes.size() > 0){
    			
    			Map<Id, UMD_Input_Complete__c> updatedMap = new Map<Id, UMD_Input_Complete__c>(
    				[Select Id, Complete__c, Approved__c, Approved_2__c, Assigned_To__r.Name, Approver_1__r.Name, Approver_2__r.Name				
					from UMD_Input_Complete__c where Id = :accountInputCompletes]
    				);
    			
    			update accountInputCompletes;
    			
    			String ts = UserInfo.getFirstName() + ' ' + UserInfo.getLastName() + ', ' + DateTime.now().format('d MMM yyyy HH:mm');
    			
    			for(FormulaireAccountInput formAccountInput : formulaire){
    			
    				if(formAccountInput.changed == true){
    					
    					UMD_Input_Complete__c inputComplete = updatedMap.get(formAccountInput.record.Id);
    					
		    			if(formAccountInput.record.Complete__c == true && formAccountInput.record.Complete__c != inputComplete.Complete__c) formAccountInput.completedTS = ts;
		    			else if(formAccountInput.record.Complete__c == false) formAccountInput.completedTS = inputComplete.Assigned_To__r.Name;
		    			
						if(formAccountInput.record.Approved__c == true && formAccountInput.record.Approved__c != inputComplete.Approved__c) formAccountInput.approved1TS = ts;
		    			else if(formAccountInput.record.Approved__c == false) formAccountInput.approved1TS = inputComplete.Approver_1__r.Name;		    			
		    			
		    			if(formAccountInput.record.Approved_2__c == true && formAccountInput.record.Approved_2__c != inputComplete.Approved_2__c) formAccountInput.approved2TS = ts;
		    			else if(formAccountInput.record.Approved_2__c == false) formAccountInput.approved2TS = inputComplete.Approver_2__r.Name;
    				}
    			}
    		}
    		
    		if(accountInputs_Upsert.size() > 0){
    			
    			Map<Id, UMD_Input__c> updatedMap = new Map<Id, UMD_Input__c>(
    				[Select Id, Answer__c from UMD_Input__c where Id = :accountInputs_Upsert]
    				);
    			
    			upsert accountInputs_Upsert Unique_Key__c;
    			
    			String ts = UserInfo.getFirstName() + ' ' + UserInfo.getLastName() + ', ' + DateTime.now().format('d MMM yyyy HH:mm');
    			
    			for(FormulaireAccountInput formAccountInput : formulaire){
    				
    				for(FormulaireAccountInputAnswer inputAnswer : formAccountInput.accountAnswers){
    				
	    				if(inputAnswer.changed == true){
	    					
	    					UMD_Input__c input = updatedMap.get(inputAnswer.record.Id);
    						
    						if(inputAnswer.record.Answer__c != null && inputAnswer.record.Answer__c != '' && (input == null || inputAnswer.record.Answer__c != input.Answer__c)) inputAnswer.answerTS = ts;
    						else if(inputAnswer.record.Answer__c == null || inputAnswer.record.Answer__c == '') inputAnswer.answerTS = null;    	
	    				}
    				}    				
    			}
    		}
    		
    		if(accountInputs_Delete.size() > 0) delete accountInputs_Delete;
    		
    		return formulaire;
    		
    	}catch(Exception e){
    		
    		System.debug(e.getMessage() + ' | ' + e.getStackTraceString());
    		
    		Database.rollback(sp);
    		AuraHandledException auraEx = new AuraHandledException(e.getMessage()); 
    		if(test.isRunningTest()) auraEx.setMessage(e.getMessage());
    		throw auraEx;
    	}    	
    }
    
    public class Formulaire{
    	
    	@AuraEnabled public List<FormulaireAccountInput> accountInputs {get; set;}
    	@AuraEnabled public List<FormulaireQuestion> questions {get; set;}    	
    }    
            
    public class FormulaireAccountInput{
    	    	
    	@AuraEnabled public boolean changed {get; set;}
    	@AuraEnabled public Integer fiscalYear {get; set;}
    	@AuraEnabled public String accountSegment {get; set;}
    	@AuraEnabled public UMD_Input_Complete__c record {get; set;}
    	@AuraEnabled public Account_Segmentation_Validation__c currentSegmentation {get; set;}
    	@AuraEnabled public String completedTS {get; set;}
    	@AuraEnabled public String approved1TS {get; set;}
    	@AuraEnabled public String approved2TS {get; set;}   
    	@AuraEnabled public List<FormulaireAccountInputAnswer> accountAnswers {get; set;}
    	@AuraEnabled public List<FormulaireAccountInputPrevAnswer> prevAccountAnswers {get; set;}    	
    }
    
    public class FormulaireAccountInputAnswer{
    	
    	@AuraEnabled public UMD_Input__c record {get; set;}
    	@AuraEnabled public boolean changed {get; set;}
    	@AuraEnabled public String answerTS {get; set;}
    }
    
    public class FormulaireAccountInputPrevAnswer{
    	    	
    	@AuraEnabled public String value {get; set;}
    	@AuraEnabled public String questionType {get; set;}
    	@AuraEnabled public String formattedValue {get; set;}
    }
    
    public class FormulaireQuestion{
    	
    	@AuraEnabled public List<PicklistValue> answers {get; set;}
		@AuraEnabled public String helptext {get; set;}
		@AuraEnabled public Integer max_Value {get; set;}
		@AuraEnabled public Integer min_Value {get; set;}
		@AuraEnabled public String questionText {get; set;}
		@AuraEnabled public String questionType {get; set;}
    	
    	public FormulaireQuestion(UMD_Question__c question){
    		
    		answers = new List<PicklistValue>();
    		
    		helptext = question.Helptext_Rich__c;
    		max_Value = Integer.valueOf(question.Max_Value__c);
    		min_Value = Integer.valueOf(question.Min_Value__c);
    		questionText = question.Question__c;
    		questionType = question.Type__c;  
    		
    		if(questionType == 'Yes/No'){
    			
    			answers.add(new PicklistValue('Yes', 'Yes'));
    			answers.add(new PicklistValue('No', 'No'));
    				
    		}else if(question.Answers__c != null){
    			
    			for(String answer : question.Answers__c.split('\n')){
    				
    				answers.add(new PicklistValue(answer.trim(), answer.trim()));	
    			}   			
    		}   		
    	}
    }
    
    public class PicklistValue{
    	
    	@AuraEnabled public String label {get; set;}
    	@AuraEnabled public String value {get; set;}
    	
    	public PicklistValue(String inputValue, String inputLabel){
    		
    		label = inputLabel;
    		value = inputValue;
    	}    	
    }
}