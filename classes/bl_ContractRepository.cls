global with sharing class bl_ContractRepository {
	
	public static Map<String, Contract_Repository_Setting__c> getCountrySettings(){
		
		Map<String, Contract_Repository_Setting__c> countrySettings = new Map<String, Contract_Repository_Setting__c>();
        
        for(Contract_Repository_Setting__c countrySetting : [Select Country__r.Name, Contract_Type__c, Default_Access_Level__c, Default_Access_Level_DM__c, Default_Email_Reminder_Sales_Rep__c, Default_Email_Reminder_DM__c from Contract_Repository_Setting__c]){
        	
        	countrySettings.put(countrySetting.Country__r.Name.toUpperCase() + ':' + countrySetting.Contract_Type__c, countrySetting);
        }
		
		return countrySettings;
	}
	
	@Future
	public static void createDefaultContractTeam(List<Id> contractIds){
		
		Map<String, Contract_Repository_Setting__c> countrySettings = getCountrySettings();
		
		List<Contract_Repository__c> contracts = [Select Id, Account__c, Account__r.Account_Country_vs__c, Primary_Contract_Type__c, MPG_Code__c from Contract_Repository__c where Id IN: contractIds];
		
		createDefaultContractTeam(contracts, countrySettings);
	}
	
	webservice static void createDefaultContractTeam(Id contractId) {
		
        Map<String, Contract_Repository_Setting__c> countrySettings = getCountrySettings();
		
		List<Contract_Repository__c> contracts = [Select Id, Account__c, Account__r.Account_Country_vs__c, Primary_Contract_Type__c, MPG_Code__c from Contract_Repository__c where Id = :contractId];
		
		createDefaultContractTeam(contracts, countrySettings);
    }	
	
	public static void createDefaultContractTeam(List<Contract_Repository__c> contracts, Map<String, Contract_Repository_Setting__c> countrySettings){
		
		Set<Id> accountIds = new Set<Id>();
        Set<String> mpgCodes = new Set<String>();
        
        for(Contract_Repository__c contract : contracts){
        	
        	accountIds.add(contract.Account__c);        	
        	if(contract.MPG_Code__c != null) for(String mpgCode : contract.MPG_Code__c.split(';')) mpgCodes.add(mpgCode);
        }
        
        System.debug('accountIds: ' + accountIds);
        System.debug('mpgCodes: ' + mpgCodes);
        
        Map<String, List<String>> mpgCodeTherapyGroupMap = new Map<String, List<String>>();
        Set<String> allTherapyGroups = new Set<String>();
        
        if(mpgCodes.size() > 0){
        
	        for(AggregateResult mpgCodeTherapyGroup : [Select MPG_Code_Text__c MPG, Product_Group__r.Therapy_ID__r.Therapy_Group__r.Name TherapyGroup from Product2 where RecordType.DeveloperName = 'SAP_Product' AND Product_Group__c != null AND MPG_Code_Text__c IN :mpgCodes group by MPG_Code_Text__c, Product_Group__r.Therapy_ID__r.Therapy_Group__r.Name]){
	        	
	        	String mpgCode = String.valueOf(mpgCodeTherapyGroup.get('MPG'));
	        	String therapyGroup = String.valueOf(mpgCodeTherapyGroup.get('TherapyGroup'));
	        		
	        	List<String> therapyGroups = mpgCodeTherapyGroupMap.get(mpgCode);
	        	
	        	if(therapyGroups == null){
	        		
	        		therapyGroups = new List<String>();
	        		mpgCodeTherapyGroupMap.put(mpgCode, therapyGroups);
	        	}
	        	
	        	therapyGroups.add(therapyGroup);
	        	allTherapyGroups.add(therapyGroup);	        	
	        }
        }
        
        System.debug('mpgCodeTherapyGroupMap: ' + mpgCodeTherapyGroupMap);
        System.debug('allTherapyGroups: ' + allTherapyGroups);
                
        Map<String, List<Id>> accountTerritoryMap = new Map<String, List<Id>>();
        Set<Id> territoryIds = new Set<Id>();
        
        if(allTherapyGroups.size() > 0){
        
        	List<String> allTherapyGroupsList = new List<String>(allTherapyGroups);
        
        	String query = 'Select ObjectId, Territory2.Id, Territory2.Therapy_Groups_Text__c, Territory2.ParentTerritory2Id from ObjectTerritory2Association where objectId IN :accountIds AND Territory2.Territory2Type.DeveloperName = \'Territory\' AND Territory2.Therapy_Groups_Text__c INCLUDES (\'' + String.join(allTherapyGroupsList, '\',\'') + '\')';
        	
        	for(ObjectTerritory2Association accTerritory : Database.query(query)){
        		
        		territoryIds.add(accTerritory.Territory2.Id);
        		if(accTerritory.Territory2.ParentTerritory2Id != null) territoryIds.add(accTerritory.Territory2.ParentTerritory2Id);
        		
        		for(String therapyGroup : accTerritory.Territory2.Therapy_Groups_Text__c.split(';')){
        			
        			String key = accTerritory.ObjectId + ':' + therapyGroup;
        			
        			List<Id> tgTerritoryIds = accountTerritoryMap.get(key);
        			
        			if(tgTerritoryIds == null){
        				
        				tgTerritoryIds = new List<Id>();
        				accountTerritoryMap.put(key, tgTerritoryIds);
        			}
        			
        			tgTerritoryIds.add(accTerritory.Territory2.Id);
        		}
        	}
        }
		
		System.debug('territoryIds: ' + territoryIds);
		        
        Map<Id, Territory2> territoryMap = new Map<Id, Territory2>();        
        if(territoryIds.size() > 0) territoryMap = new Map<Id, Territory2>([Select Id, Therapy_Groups_Text__c, ParentTerritory2Id, (Select User.Id, User.Job_Title_vs__c from UserTerritory2Associations where IsActive = true AND User.isActive = true AND User.Job_Title_vs__c IN ('Sales Rep', 'District Manager')) from Territory2 where Id IN :territoryIds]);
        
		Map<Id, Contract_Repository__c> repoTeams = new Map<Id, Contract_Repository__c>([Select Id, (Select Id, Auto_Aligned_with_Territory__c, Team_Member__c, Access_Level__c from Contract_Repository_Teams__r) from Contract_Repository__c where Id IN :contracts]);
		
		List<Contract_Repository_Team__c> toInsert = new List<Contract_Repository_Team__c>();
		List<Contract_Repository_Team__c> toDelete = new List<Contract_Repository_Team__c>();
		
		for(Contract_Repository__c contract : contracts){
						
			Map<Id, Contract_Repository_Team__c> contractTeam = new Map<Id, Contract_Repository_Team__c>();
			
			for(Contract_Repository_Team__c teamMember : repoTeams.get(contract.Id).Contract_Repository_Teams__r){
				
				contractTeam.put(teamMember.Team_Member__c, teamMember);
			}
			
			Set<Id> accountSalesReps = new Set<Id>();
			Set<Id> accountDMs = new Set<Id>();
						
			if(contract.MPG_Code__c != null){
				
				for(String mpgCode : contract.MPG_Code__c.split(';')){
				
					List<String> mpgCodeTherapyGroups = mpgCodeTherapyGroupMap.get(mpgCode);
					
					if(mpgCodeTherapyGroups != null){
													
						for(String therapyGroup : mpgCodeTherapyGroups){
							
							String key = contract.Account__c + ':' + therapyGroup;
							
							List<Id> accountTerritories = accountTerritoryMap.get(key);
							
							if(accountTerritories != null){
								
								for(Id territoryId : accountTerritories){
									
									//Sales Rep							
									Territory2 territory = territoryMap.get(territoryId);				
									
									for(UserTerritory2Association userTerritory : territory.UserTerritory2Associations){
										
										if(userTerritory.User.Job_Title_vs__c == 'Sales Rep') accountSalesReps.add(userTerritory.User.Id);
									}	
									
									//District Manager
									Territory2 parentTerritory = territoryMap.get(territory.ParentTerritory2Id);				
									
									for(UserTerritory2Association userTerritory : parentTerritory.UserTerritory2Associations){
										
										if(userTerritory.User.Job_Title_vs__c == 'District Manager') accountDMs.add(userTerritory.User.Id);
									}							
								} 
							}
						}
					}					
				}				
			}
			
			Contract_Repository_Setting__c countrySetting = countrySettings.get(contract.Account__r.Account_Country_vs__c.toUpperCase() + ':' + contract.Primary_Contract_Type__c);
			
			for(Id userId : accountSalesReps){
				
				Contract_Repository_Team__c contractMember = contractTeam.get(userId);			 
				
				if(contractMember == null){
					
					contractMember = new Contract_Repository_Team__c();
					contractMember.Contract_Repository__c = contract.Id;
					contractMember.Team_Member__c = userId;
					contractMember.Auto_Aligned_with_Territory__c = true;
					contractMember.Access_Level__c = countrySetting != null ? countrySetting.Default_Access_Level__c : 'Read Only - No Access to Documents';
					contractMember.Email_Reminder__c = countrySetting != null ? countrySetting.Default_Email_Reminder_Sales_Rep__c : null;
					
					toInsert.add(contractMember);											
				}		
			}
			
			for(Id userId : accountDMs){
				
				Contract_Repository_Team__c contractMember = contractTeam.get(userId);			 
				
				if(contractMember == null){
					
					contractMember = new Contract_Repository_Team__c();
					contractMember.Contract_Repository__c = contract.Id;
					contractMember.Team_Member__c = userId;
					contractMember.Auto_Aligned_with_Territory__c = true;
					contractMember.Access_Level__c = countrySetting != null ? countrySetting.Default_Access_Level_DM__c : 'Read Only - No Access to Documents';
					contractMember.Email_Reminder__c = countrySetting != null ? countrySetting.Default_Email_Reminder_DM__c : null;
					
					toInsert.add(contractMember);											
				}		
			}	
			
			for(Contract_Repository_Team__c existingTeam : contractTeam.values()){
				
				if(existingTeam.Auto_Aligned_with_Territory__c == true && accountSalesReps.contains(existingTeam.Team_Member__c) == false && accountDMs.contains(existingTeam.Team_Member__c) == false) toDelete.add(existingTeam);				
			}		
		}
		
		bl_Contract_Repository_Team.isAutoProcessRunning = true;
		
		if(toInsert.size() > 0) insert toInsert;
		if(toDelete.size() > 0) delete toDelete;
		
		bl_Contract_Repository_Team.isAutoProcessRunning = false;
		
	}    
}