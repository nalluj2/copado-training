@isTest
Private class Test_tr_Oppty_OwnerAndAssignedToUpdate{    
	static testMethod void testOpptyOwnerAndAssignedUpdate(){
		
		// Create users
		Profile p = [select id from profile where name='System Administrator'];    
		user u1 = new User(	alias = 'tasgn2', email='standarduser1@testorg.com',             
							emailencodingkey='UTF-8', lastname='TerritoryManagement_Testing',             
							languagelocalekey='en_US',             
							localesidkey='en_US', profileid = p.Id,            
							timezonesidkey='America/Los_Angeles',             
							CountryOR__c='Country1',          
							Country= 'Country1',            
							username='testingintlownerassign1@medtronic.com.intl',
							Alias_unique__c='tasgn1',Company_Code_Text__c='EUR'
						 );  
		user u2 = new User(	alias = 'tasgn2', email='standarduser2@testorg.com',             
							emailencodingkey='UTF-8', lastname='TerritoryManagement_Testing',             
							languagelocalekey='en_US',             
							localesidkey='en_US', profileid = p.Id,            
							timezonesidkey='America/Los_Angeles',             
							CountryOR__c='Country1',          
							Country= 'Country1',            
							username='testingintlownerassign2@medtronic.com.intl',
							Alias_unique__c='tasgn2',Company_Code_Text__c='EUR'
						 );  

		insert new List<User>{u1, u2};
		
		RecordType RT=[select id,developername from RecordType where sobjecttype='Opportunity' and name='CAN DIB Opportunity'];

		Record_Type_CAN_DIB_Opportunity__c RTCAN=new Record_Type_CAN_DIB_Opportunity__c();
		RTCAN.Name='1';
		RTCAN.RT_Developer_Name__c='CAN_DIB_OPPORTUNITY';
		insert RTCAN;

		Country_Assignment_Rule__c CAR =new Country_Assignment_Rule__c();
		CAR.Assigned_To_1__c=u1.id;
		CAR.Assigned_To_2__c=u1.id;
		CAR.Assignment_Value__c='12345';

		Country_Assignment_Rule__c CAR1 =new Country_Assignment_Rule__c();
		CAR1.Assigned_To_1__c=u2.id;
		CAR1.Assigned_To_2__c=u2.id;
		CAR1.Assignment_Value__c='bucket';
		
		insert new List<Country_Assignment_Rule__c>{CAR, CAR1};         

		Account testAc1 = new Account() ;
		testAc1.Name = 'Test Account Name1';  
		testAc1.RecordTypeId = FinalConstants.sapAccountRecordTypeId ;
		testAc1.Account_Country_vs__c='BELGIUM'; 
		testAc1.DIB_SAP_Sales_Rep_Id__c='12345';

		Account testAc2 = new Account() ;
		testAc2.Name = 'Test Account Name12';  
		testAc2.RecordTypeId = FinalConstants.sapAccountRecordTypeId ;
		testAc2.Account_Country_vs__c='BELGIUM'; 
		testAc2.Account_Postal_Code__c='123';

		Account testAc3 = new Account() ;
		testAc3.Name = 'Test Account Name12';  
		testAc3.RecordTypeId = FinalConstants.sapAccountRecordTypeId ;
		testAc3.Account_Country_vs__c='BELGIUM'; 
		testAc3.Account_Postal_Code__c='456';
		
		insert new List<Account>{testAc1, testAc2, testAc3}; 

		Test.startTest();

		opportunity op=new opportunity(name='testopp',ownerid=u1.id,Deal_Category__c='Upgrade',CloseDate=system.today()+2,stagename='Qualification',accountid=testAc1.id,ForecastCategoryName='Pipeline',CurrencyIsoCode='EUR',type='Service Contract',RecordTypeId=RT.Id);
		opportunity op1=new opportunity(name='testopp',ownerid=u1.id,Deal_Category__c='Upgrade',CloseDate=system.today()+2,stagename='Qualification',accountid=testAc2.id,ForecastCategoryName='Pipeline',CurrencyIsoCode='EUR',type='Service Contract',RecordTypeId=RT.Id);
		opportunity op2=new opportunity(name='testopp',ownerid=u1.id,Deal_Category__c='Upgrade',CloseDate=system.today()+2,stagename='Qualification',accountid=testAc3.id,ForecastCategoryName='Pipeline',CurrencyIsoCode='EUR',type='Service Contract',RecordTypeId=RT.Id);         

		insert new List<Opportunity>{op, op1, op2}; 

		Test.stopTest();   

	}
}