@isTest
private class Test_Contact_Master_ShipTo {
    
    private static testmethod void testContactCreationOnMasterShipTo(){
    	    
    	//Region    	
        Company__c masterData = new Company__c();
        masterData.name='TestMedtronic';
        masterData.CurrencyIsoCode = 'EUR';
        masterData.Current_day_in_Q1__c=56;
        masterData.Current_day_in_Q2__c=34; 
        masterData.Current_day_in_Q3__c=5; 
        masterData.Current_day_in_Q4__c= 0;   
        masterData.Current_day_in_year__c =200;
        masterData.Days_in_Q1__c=56;  
        masterData.Days_in_Q2__c=34;
        masterData.Days_in_Q3__c=13;
        masterData.Days_in_Q4__c=22;
        masterData.Days_in_year__c =250;
        masterData.Company_Code_Text__c = 'MDT';
        insert masterData;        
		
		//Country
		DIB_Country__c country = new DIB_Country__c();
		country.name= 'NETHERLANDS';
		country.Company__c = masterData.Id;
		country.BU_Not_Mandatory_on_Relationship__c = true;
		country.Hide_Therapy_on_Relationship__c = true;
		country.Country_ISO_Code__c = 'NL';
		insert country;
    	
    	//Account Types
    	Account_Type__c soldtoType = new Account_Type__c();
    	soldtoType.Account_Type__c = 'Sold To';
    	soldtoType.Account_Type_Level__c = '1';
    	soldtoType.Identify_Field__c = 'SAP_Account_Type__c';
    	soldtoType.Identify_Operator__c = 'Equals';
    	soldtoType.Identify_Value__c = 'Sold-to party';
    	
    	Account_Type__c mShiptoType = new Account_Type__c();
    	mShiptoType.Account_Type__c = 'Sold To';
    	mShiptoType.Account_Type_Level__c = '2';
    	mShiptoType.Identify_Field__c = 'SAP_Account_Type__c';
    	mShiptoType.Identify_Operator__c = 'Equals';
    	mShiptoType.Identify_Value__c = 'Ship to party';
    	mShiptoType.Identify_Field2__c = 'SAP_Search_Term1__c';
    	mShiptoType.Identify_Operator2__c = 'Equals';
    	mShiptoType.Identify_Value2__c = 'MASTER';
    	
    	insert new List<Account_Type__c>{soldtoType, mShiptoType};
    	
    	//Account Type Country
    	Account_Type_Country__c soldtoAccTypeCountry = new Account_Type_Country__c();
    	soldtoAccTypeCountry.Account_Type__c = soldtoType.Id;
    	soldtoAccTypeCountry.Country__c = country.Id;
    	
    	Account_Type_Country__c mshiptoAccTypeCountry = new Account_Type_Country__c();
    	mshiptoAccTypeCountry.Account_Type__c = mShiptoType.Id;
    	mshiptoAccTypeCountry.Country__c = country.Id;
    	    	    	
    	insert new List<Account_Type_Country__c>{soldtoAccTypeCountry, mshiptoAccTypeCountry};
    	
    	//Relationship Type
    	Relationship_Type__c mShiptoSoldtoRelType = new Relationship_Type__c();
    	mShiptoSoldtoRelType.Relationship_Type__c = 'Master Ship To/Sold To';
    	mShiptoSoldtoRelType.Child_Account_Type__c = mShiptoType.Id;
    	mShiptoSoldtoRelType.Parent_Account_Type__c = soldtoType.Id;
    	
    	insert mShiptoSoldtoRelType;
    	
    	//Relationship Type Country
    	Relationship_Type_Country__c relTypeCountry = new Relationship_Type_Country__c();
    	relTypeCountry.Relationship_Type__c = mShiptoSoldtoRelType.Id;
    	relTypeCountry.Country__c = country.Id;
    	
    	insert relTypeCountry;
    	
    	//Account
    	Account soldTo = new Account();
    	soldTo.Name = 'SoldTo';
    	soldTo.Account_Country_vs__c = 'NETHERLANDS';
    	soldTo.SAP_Id__c = '111111111';
    	soldTo.SAP_Account_Type__c = 'Sold-to party';
    	soldTo.SAP_Search_Term1__c = 'SOLD TO';    	
    	
    	Account mShipto = new Account();
    	mShipto.Name = 'Master ShipTo';
    	mShipto.Account_Country_vs__c = 'NETHERLANDS';
    	mShipto.SAP_Id__c = '222222222';
    	mShipto.SAP_Account_Type__c = 'Ship to Party';
    	mShipto.SAP_Search_Term1__c = 'MASTER';
    	mShipto.SAP_Search_Term2__c = 'SHIP TO';
    	
    	insert new List<Account>{soldTo, mShipTo};
    	
    	//Relationship
    	Affiliation__c mShiptoSoldto = new Affiliation__c();
    	mShiptoSoldto.Affiliation_From_Account__c = mShipto.Id;
    	mShiptoSoldto.Affiliation_To_Account__c = soldTo.Id;
    	mShiptoSoldto.Affiliation_Type__c = 'Master Ship To/Sold To';
    	mShiptoSoldto.Affiliation_Start_Date__c = Date.today().addDays(-10);
    	mShiptoSoldto.RecordTypeId = Schema.SObjectType.Affiliation__c.getRecordTypeInfosByName().get('A2A').getRecordTypeId();    	
    	
    	insert mShiptoSoldto;
    	
    	Test.startTest();
    	
    	Contact cnt = new Contact();    	
    	cnt.AccountId = mShipto.Id;
    	cnt.FirstName = 'Test';
		cnt.LastName = 'Contact';
		cnt.Phone = '+123456789';		
		cnt.Email = 'test.contact@gmail.com';
		cnt.MobilePhone = '+123456789';		
		cnt.MailingCity = 'Minneapolis';
		cnt.MailingCountry = 'UNITTEST';
		cnt.MailingState = 'Minnesota';
		cnt.MailingStreet = 'street';
		cnt.MailingPostalCode = '123456';		
		cnt.Contact_Department__c = 'Cardiology';
		cnt.Contact_Primary_Specialty__c = 'Cardiovascular'; 
		cnt.Affiliation_To_Account__c = 'Employee'; 
		cnt.Primary_Job_Title_vs__c = 'Surgeon'; 
		cnt.Contact_Gender__c = 'Male'; 
		cnt.AccountId = mShipto.Id;
		cnt.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Generic Contact').getRecordTypeId();	
    	insert cnt;
    	
    	Test.stopTest();
    	
    	cnt = [Select Id, AccountId, Master_ShipTo__c from Contact where Id = :cnt.Id];
    	
    	System.assert(cnt.AccountId == soldTo.Id);
    	System.assert(cnt.Master_ShipTo__c == mShipto.Id);
    	
    	List<Affiliation__c> mShipToRel = [Select Id from Affiliation__c where Affiliation_From_Contact__c = :cnt.Id AND Affiliation_To_Account__c = :mShipto.Id];
    	
    	System.assert(mShipToRel.size() == 1);
    }
}