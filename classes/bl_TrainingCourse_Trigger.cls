//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 28-06-2016
//  Description      : APEX Class - Business Logic for tr_TrainingCourse
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public without sharing class bl_TrainingCourse_Trigger {

	//--------------------------------------------------------------------------------------------------------------------
	// Public variables coming from the trigger
	//--------------------------------------------------------------------------------------------------------------------
	public static List<Training_Course__c> lstTriggerNew = new List<Training_Course__c>();
	public static Map<Id, Training_Course__c> mapTriggerNew = new Map<Id, Training_Course__c>();
	public static Map<Id, Training_Course__c> mapTriggerOld = new Map<Id, Training_Course__c>();
	//--------------------------------------------------------------------------------------------------------------------


	//--------------------------------------------------------------------------------------------------------------------
	// If an URL field on Training Course is updated, 
	//	we also need to update the URL field on the related Approved Training Nomination
	//--------------------------------------------------------------------------------------------------------------------
	public static void updateTrainingNomination_Url(){

		Map<Id, String> mapTrainingCourseId_Url_FinalSurvey = new Map<Id, String>();
		Map<Id, String> mapTrainingCourseId_Url_InitialSurvey = new Map<Id, String>();
		Map<Id, String> mapTrainingCourseId_Url_LogisticSurvey = new Map<Id, String>();

		Set<Id> setID_TrainingCourse = new Set<Id>();

		for (Training_Course__c oTrainingCourse : lstTriggerNew){

			Training_Course__c oTrainingCourse_Old = mapTriggerOld.get(oTrainingCourse.Id);

			if (oTrainingCourse.Final_Follow_Up_Survey__c != oTrainingCourse_Old.Final_Follow_Up_Survey__c){
				mapTrainingCourseId_Url_FinalSurvey.put(oTrainingCourse.Id, oTrainingCourse.Final_Follow_Up_Survey__c);
				setID_TrainingCourse.add(oTrainingCourse.Id);
			}

			if (oTrainingCourse.Initial_Follow_Up_Survey__c != oTrainingCourse_Old.Initial_Follow_Up_Survey__c){
				mapTrainingCourseId_Url_InitialSurvey.put(oTrainingCourse.Id, oTrainingCourse.Initial_Follow_Up_Survey__c);
				setID_TrainingCourse.add(oTrainingCourse.Id);
			}

			if (oTrainingCourse.Logistics_Survey__c != oTrainingCourse_Old.Logistics_Survey__c){
				mapTrainingCourseId_Url_LogisticSurvey.put(oTrainingCourse.Id, oTrainingCourse.Logistics_Survey__c);
				setID_TrainingCourse.add(oTrainingCourse.Id);
			}

		}

		if (setID_TrainingCourse.size() > 0){

			List<Training_Nomination__c> lstTrainingNomination = 
				[
					SELECT
						Id, Course__c
						, Final_Follow_Up_Survey__c, Initial_Follow_Up_Survey__c, Logistics_Survey__c
					FROM 
						Training_Nomination__c
					WHERE
						Status__c = 'Approved'
						AND Course__c = :setID_TrainingCourse
				];

			for (Training_Nomination__c oTrainingNomination : lstTrainingNomination){

				if (mapTrainingCourseId_Url_FinalSurvey.containsKey(oTrainingNomination.Course__c)){
					oTrainingNomination.Final_Follow_Up_Survey__c = mapTrainingCourseId_Url_FinalSurvey.get(oTrainingNomination.Course__c);
				}

				if (mapTrainingCourseId_Url_InitialSurvey.containsKey(oTrainingNomination.Course__c)){
					oTrainingNomination.Initial_Follow_Up_Survey__c = mapTrainingCourseId_Url_InitialSurvey.get(oTrainingNomination.Course__c);
				}

				if (mapTrainingCourseId_Url_LogisticSurvey.containsKey(oTrainingNomination.Course__c)){
					oTrainingNomination.Logistics_Survey__c = mapTrainingCourseId_Url_LogisticSurvey.get(oTrainingNomination.Course__c);
				}

			}

			update lstTrainingNomination;
		}

	}
	//--------------------------------------------------------------------------------------------------------------------


	//--------------------------------------------------------------------------------------------------------------------
	// Id the Status of a Training Course is updated,
	//	we also need to update the Status on the related Training Nomination records.
	//	- Course Status is changed to 'Nomination Closed' --> Training Nomination Status 'Evaluation' =>'Not Approved'
	//	- Course Status is changed to 'Cancelled' --> Training Nomination Status => 'Cancelled'
	//	- Course Status is changed to 'Completed' --> Training Nomination Status 'Approved' => 'Follow Up'
	//--------------------------------------------------------------------------------------------------------------------
	public static void updateTrainingNomination_Status(){

		Set<String> setTrainingCourseStatus_New = new Set<String>{'Nomination Closed','Cancelled','Completed'};

		Map<Id, String> mapTrainingCourseId_Status = new Map<Id, String>();

		for (Training_Course__c oTrainingCourse : lstTriggerNew){

			Training_Course__c oTrainingCourse_Old = mapTriggerOld.get(oTrainingCourse.Id);
			if (oTrainingCourse.Status__c != oTrainingCourse_Old.Status__c){
				if (setTrainingCourseStatus_New.contains(oTrainingCourse.Status__c)){
					mapTrainingCourseId_Status.put(oTrainingCourse.Id, oTrainingCourse.Status__c);
				}
			}

		}

		if (mapTrainingCourseId_Status.size() > 0){

			List<Training_Nomination__c> lstTrainingNomination = 
				[
					SELECT
						Id, Course__c, Status__c
						, Final_Follow_Up_Survey__c, Initial_Follow_Up_Survey__c, Logistics_Survey__c
					FROM 
						Training_Nomination__c
					WHERE
						Course__c = :mapTrainingCourseId_Status.keySet()
				];

			List<Training_Nomination__c> lstTrainingNomination_Update = new List<Training_Nomination__c>();
			Set<Id> setID_TrainingNomination_CancelApprovals = new Set<Id>();

			for (Training_Nomination__c oTrainingNomination : lstTrainingNomination){

				String tTrainingCourseStatus = mapTrainingCourseId_Status.get(oTrainingNomination.Course__c);

				if (tTrainingCourseStatus == 'Nomination Closed'){

					if (oTrainingNomination.Status__c == 'Evaluation'){
						oTrainingNomination.Status__c = 'Not Approved - No Seats';
						lstTrainingNomination_Update.add(oTrainingNomination);
					}

				}else if (tTrainingCourseStatus == 'Cancelled'){

					oTrainingNomination.Status__c = 'Cancelled';
					lstTrainingNomination_Update.add(oTrainingNomination);
					setID_TrainingNomination_CancelApprovals.add(oTrainingNomination.Id);

				}else if (tTrainingCourseStatus == 'Completed'){

					if (oTrainingNomination.Status__c == 'Approved'){
						oTrainingNomination.Status__c = 'Follow Up';
						lstTrainingNomination_Update.add(oTrainingNomination);
					}
					setID_TrainingNomination_CancelApprovals.add(oTrainingNomination.Id);

				}
			}

			if (lstTrainingNomination_Update.size() > 0){
				update lstTrainingNomination_Update;
			}

			if (setID_TrainingNomination_CancelApprovals.size() > 0){

				// Cancel/Reject all Approvals
				List<Approval.ProcessResult> lstProcessResult = clsUtil.processApproval(setID_TrainingNomination_CancelApprovals, 'REJECT');

			}
		}

	}
	//--------------------------------------------------------------------------------------------------------------------


	//--------------------------------------------------------------------------------------------------------------------
	// Prevent the closure of a Training Course when there are still open task for the related Training Nomation records
	//--------------------------------------------------------------------------------------------------------------------
	public static void validateTrainingCourseClosure(){

		Set<String> setTrainingCourseStatus_New = new Set<String>{'Nomination Closed'};
		Set<Id> setID_TrainingCourse = new Set<Id>();

		for (Training_Course__c oTrainingCourse : lstTriggerNew){

			Training_Course__c oTrainingCourse_Old = mapTriggerOld.get(oTrainingCourse.Id);

			if (oTrainingCourse.Status__c != oTrainingCourse_Old.Status__c){
				if (setTrainingCourseStatus_New.contains(oTrainingCourse.Status__c)){
					setID_TrainingCourse.add(oTrainingCourse.Id);
				}
			}

		}

		// Select Training_Nomination__c records with the related Open Activities
		List<Training_Nomination__c> lstTrainingNomination = 
			[
				SELECT 
					Id, Course__c
					, (SELECT Id FROM OpenActivities ORDER BY ActivityDate ASC NULLS LAST, LastModifiedDate DESC LIMIT 1)
				FROM 
					Training_Nomination__c
				WHERE 
					Course__c = :setID_TrainingCourse
				ORDER BY 
					Course__c
			];

		Set<Id> setID_TrainingCourse_OpenActivities = new Set<Id>();
		for (Training_Nomination__c oTrainingNomination : lstTrainingNomination){
			if (oTrainingNomination.OpenActivities.size() > 0){
				setID_TrainingCourse_OpenActivities.add(oTrainingNomination.Course__c);
			}
		}

		for (Training_Course__c oTrainingCourse : lstTriggerNew){

			if (setID_TrainingCourse_OpenActivities.contains(oTrainingCourse.Id)){
				oTrainingCourse.addError(Label.TrainingCourseOpenTaskError);
			}
		}

	}
	//--------------------------------------------------------------------------------------------------------------------


	//--------------------------------------------------------------------------------------------------------------------
	// When the PACE_Approver__c of the Training_Course__c is updated, 
	//	the PACE_Approver__c of all related Training_Nomination__c records is also updated with this new value 
	//--------------------------------------------------------------------------------------------------------------------
	public static void updateTrainingNomination_PACEApprover(){

		Map<Id, Training_Course__c> mapTrainingCourse = new Map<Id, Training_Course__c>();

		for (Training_Course__c oTrainingCourse : lstTriggerNew){

			Training_Course__c oTrainingCourse_Old = mapTriggerOld.get(oTrainingCourse.Id);

			if (oTrainingCourse.PACE_Approver__c != oTrainingCourse_Old.PACE_Approver__c){
				mapTrainingCourse.put(oTrainingCourse.Id, oTrainingCourse);
			}

		}

		if (mapTrainingCourse.size() > 0){

			List<Training_Nomination__c> lstTrainingNomination = 
				[
					SELECT 
						Id, Course__c, PACE_Approver__c
					FROM 
						Training_Nomination__c
					WHERE 
						Course__c =: mapTrainingCourse.keySet()
				];

			if (lstTrainingNomination.size() > 0){
				for (Training_Nomination__c oTrainingNomination : lstTrainingNomination){
					oTrainingNomination.PACE_Approver__c = mapTrainingCourse.get(oTrainingNomination.Course__c).PACE_Approver__c;
				}

				update lstTrainingNomination;
			}

		}

	}
	//--------------------------------------------------------------------------------------------------------------------

}
//--------------------------------------------------------------------------------------------------------------------