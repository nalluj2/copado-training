@isTest
private class Test_ws_UserActivityService {

    static testMethod void logActivityShouldAddTheActivity() {
    	
    	Application_Usage_Detail__c aud = new Application_Usage_Detail__c();
    	
        ws_UserActivityService.UserActivityRequest uaRequest = new ws_UserActivityService.UserActivityRequest();
    	uaRequest.applicationUsageDetail = aud;
    	String JsonMsg=JSON.serialize(uaRequest);    	
    	
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();

		req.requestURI = '/services/apexrest/UserActivityService';  
		req.httpMethod = 'POST';
		req.requestBody = Blob.valueOf(JsonMsg);
		RestContext.request = req;
		RestContext.response = res;
		
		string result =  ws_UserActivityService.logActivity();
		ws_UserActivityService.UserActivityResponse response =(ws_UserActivityService.UserActivityResponse) JSON.deserialize(result, ws_UserActivityService.UserActivityResponse.class);
		
		System.assertEquals(false, response.hasError);
    }
    
    static testMethod void logActivityShouldNotAddTheActivity() {
    	
    	Application_Usage_Detail__c aud = new Application_Usage_Detail__c();
    	
        ws_UserActivityService.UserActivityRequest uaRequest = new ws_UserActivityService.UserActivityRequest();
    	uaRequest.applicationUsageDetail = null;
    	String JsonMsg=JSON.serialize(uaRequest);    	
    	
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();

		req.requestURI = '/services/apexrest/UserActivityService';  
		req.httpMethod = 'POST';
		req.requestBody = Blob.valueOf(JsonMsg);
		RestContext.request = req;
		RestContext.response = res;
		
		string result =  ws_UserActivityService.logActivity();
		ws_UserActivityService.UserActivityResponse response =(ws_UserActivityService.UserActivityResponse) JSON.deserialize(result, ws_UserActivityService.UserActivityResponse.class);
		
		System.assertEquals(true, response.hasError);
    }
}