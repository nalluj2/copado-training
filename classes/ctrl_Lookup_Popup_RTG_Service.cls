public with sharing class ctrl_Lookup_Popup_RTG_Service {
    
    public List<RTG_Service_Category> categories {get; set;}
    public String componentId {get; set;}
    
	public ctrl_Lookup_Popup_RTG_Service(){
		
		String inputParams = ApexPages.currentPage().getParameters().get('p');
		
		if(inputParams!=null){
					
			String params = CryptoUtils.decryptText(inputParams); 
			
			for(String valuePair : params.split('&')){
				
				String[] pair = valuePair.split('=');
				
				if(pair.size() == 2){
					if(pair[0] == 'compId'){
						componentId = pair[1];
						break;
					}
				}				 
			}
		
			categories = new List<RTG_Service_Category>();
			
			List<RTG_Service_Product_Codes__c> serviceProducts = [SELECT Name, Category__c FROM RTG_Service_Product_Codes__c ORDER BY Category__c, Order__c];
			
			Set<String> productCFNs = new Set<String>();
				
			for(RTG_Service_Product_Codes__c serviceProduct : serviceProducts){
				
				productCFNs.add(serviceProduct.Name);
			} 
			
			Map<String, Product2> productMap = new Map<String, Product2>();
			
			for(Product2 serviceProduct : [SELECT Id, CFN_Code_Text__c, Name FROM Product2 WHERE CFN_Code_Text__c IN :productCFNs]){
				
				productMap.put(serviceProduct.CFN_Code_Text__c, serviceProduct);
			}
			
			RTG_Service_Category lastCategory;
			
			for(RTG_Service_Product_Codes__c serviceProduct : serviceProducts){
				
				if(lastCategory == null || lastCategory.categoryName != serviceProduct.Category__c){
					
					
					lastCategory = new RTG_Service_Category();
					lastCategory.categoryName = serviceProduct.Category__c;
					categories.add(lastCategory);
				}
				
				Product2 product = productMap.get(serviceProduct.Name);
				
				RTG_Service_Product categoryProduct = new RTG_Service_Product();
				categoryProduct.productId = product.Id;
				categoryProduct.productCFN = product.CFN_Code_Text__c;
				categoryProduct.productName = product.Name;
				
				lastCategory.categoryProducts.add(categoryProduct);
			}
		}
						
	}
	
	public class RTG_Service_Category{
		
		public String categoryName {get; set;}
		public List<RTG_Service_Product> categoryProducts {get; set;}
		
		public RTG_Service_Category(){
			
			categoryProducts = new List<RTG_Service_Product>();
		}
		
	}
	
	public class RTG_Service_Product{
		
		public String productId {get; set;}
		public String productCFN {get; set;}
		public String productName {get; set;}		
	}
}