//---------------------------------------------------------------------------------------------------------------------------------
//	Author 			: Bart Caelen
//  Created Date 	: 08-05-2018
//  Description 	: APEX Class - Business Logic for the APEX Trigger tr_Implant
//  Change Log 		: 
//---------------------------------------------------------------------------------------------------------------------------------
public class bl_Implant_Trigger {

	private static Europe_Call_Record__c oEuropeCallRecord;
    private static Id idCallActivityImplantSupport = null;
    private static Id idCallActivityCaseSupport = null;    //-BC - 20151020 - CR-9560 
    private static Id idCallCategorySupport = null;
    private static Id idCallActivityProductDemonstration = null;
    private static Id idCallCategoryPromotion = null;
    private static String tBusinessUnit_ST = 'Europe - Surgical Technologies';

	public static void countImplants(String tAction, List<Implant__c> lstTriggerData){

        if (bl_Trigger_Deactivation.isTriggerDeactivated('impl_countImplants')) return;

		if (tAction == 'INSERT'){

			if (GTutil.chkInsertImplantAfterAll) totalImplantsCounter.countImplants(lstTriggerData);

		}else if (tAction == 'UPDATE'){

			if (GTutil.chkUpdateImplantAfterAll) totalImplantsCounter.countImplants(lstTriggerData);

		}else if (tAction == 'UNDELETE'){

			totalImplantsCounter.countImplants(Trigger.new);

		}else if (tAction == 'DELETE'){

			totalImplantsCounter.countImplants(Trigger.old);

		}

	}        


	public static void createCallRecord(String tAction, List<Implant__c> lstTriggerNew){

        if (bl_Trigger_Deactivation.isTriggerDeactivated('impl_createCallRecord')) return;

		if ( 
			((tAction == 'INSERT') && (!GTutil.chkInsertImplantAfterAll))
			|| ((tAction == 'UPDATE') && (!GTutil.chkUpdateImplantAfterAll))
		){
			return;
		}


		loadSettingData();

        List<Implant__c> impNew = lstTriggerNew;
        List<Call_Records__c> CallRecords = new list<Call_Records__c>();
        Map<id,decimal> ImpDuration=new Map<id,decimal>();
        Map<Id, Decimal> mapImplantId_ProcedureTime = new Map<Id, Decimal>();   //-BC - 20151020 - CR-9560 
        Map<Id, Boolean> mapImplantId_IsAFS = new Map<Id, Boolean>();           //-BC - 20151020 - CR-9560 
        Map<id,id> impContacts=new Map<id,id>();
        Map<id,id> impAccounts=new Map<id,id>();
        Map<id,id> impTherapy=new Map<id,id>();
        Map<id,string> impRTGComments=new Map<id,String>();
        Map<id,String> impBU=new Map<id,String>();
        
        for(integer i=0;i<impNew.size();i++){
            //-BC - 20151020 - CR-9560 - Also create a Call Record for AFS Implants - START
//          if (!bl_Implant.bIsAFS(impNew[i].RecordTypeId)){        //-BC - 201401 - Added for the AFS/CRDM/CRHF logic - do not create a Call Record when AFS/CRDM/CRHF logic is executed
                if(impNew[i].Call_Record_ID__c==null && impNew[i].Attended_Implant_Text__c=='Attended'){
                    Call_Records__c oCallRecord = new Call_Records__c();
                        oCallRecord.Call_Date__c = impNew[i].Implant_Date_Of_Surgery__c; 
                        oCallRecord.Business_Unit__c = impNew[i].Business_Unit__c;
                        oCallRecord.OwnerId = impNew[i].OwnerId;  
                        oCallRecord.Implant__c = impNew[i].Id;                
                        oCallRecord.RTG_Comment__c=impNew[i].RTG_Comments__c; 
                        oCallRecord.Wait_Time__c=impNew[i].Wait_Time__c;
                        oCallRecord.Call_Travel_Time_Minutes__c=impNew[i].Travel_Time__c;
                        oCallRecord.Start_Time__c = impNew[i].Start_Time__c;    //-BC - 201401 - CR-2611 - Added   
                        oCallRecord.Scheduled_Activity__c = impNew[i].Source_case__c;             
                    CallRecords.add(oCallRecord); 

                    ImpDuration.put(impNew[i].Id,impNew[i].Duration_Nr__c); 
                    impContacts.put(impNew[i].Id,impNew[i].Implant_Implanting_Contact__c);
                    impAccounts.put(impNew[i].Id,impNew[i].Implant_Implanting_Account__c);                                                                  
                    impTherapy.put(impNew[i].Id,impNew[i].Therapy__c); 
                    impBU.put(impNew[i].Id,impNew[i].Business_Unit_Text__c);

                    mapImplantId_ProcedureTime.put(impNew[i].Id, impNew[i].Procedure_time_in_min__c);   //-BC - 20151020 - CR-9560 
                    mapImplantId_IsAFS.put(impNew[i].Id, bl_Implant.bIsAFS(impNew[i].RecordTypeId));    //-BC - 20151020 - CR-9560 
                }
//            }
            //-BC - 20151020 - CR-9560 - Also create a Call Record for AFS Implants - STOP
        }

        if(CallRecords.size()>0){
            Insert CallRecords;         
        }
        
        list<Call_Topics__c> CallTopics = new list<Call_Topics__c>();
        list<Implant__c> Implants = new list<Implant__c>();
        list<Contact_Visit_Report__c> CallContacts = new list<Contact_Visit_Report__c>();       
        for(integer i=0;i<CallRecords.size();i++){
            Implant__c Implant = new Implant__c(id=CallRecords[i].Implant__c);
            if(Implant.Call_Record_ID__c==null || Implant.Call_Record_ID__c!=CallRecords[i].id){
                Implant.Call_Record_ID__c = CallRecords[i].id;
                Implants.add(Implant);
                
                // Creating Call topics
                Call_Topics__c CallTopic = new Call_Topics__c();
                CallTopic.Call_Records__c = CallRecords[i].Id;
                CallTopic.Call_Topic_Therapy__c = impTherapy.get(CallRecords[i].Implant__c);                

                //-BC - 20151020 - CR-9560 - Apply different data for AFS Implants - START
                if (mapImplantId_IsAFS.get(CallRecords[i].Implant__c)){
                    CallTopic.Call_Topic_Duration__c = mapImplantId_ProcedureTime.get(CallRecords[i].Implant__c);
                    CallTopic.Call_Activity_Type__c = idCallActivityCaseSupport;
                    CallTopic.Call_Category__c =idCallCategorySupport;
                }else{
                    CallTopic.Call_Topic_Duration__c = ImpDuration.get(CallRecords[i].Implant__c);

                    if(impBU.get(CallRecords[i].Implant__c) == tBusinessUnit_ST){
                        CallTopic.Call_Activity_Type__c =idCallActivityProductDemonstration;
                        CallTopic.Call_Category__c =idCallCategoryPromotion;
                    }  
                    else{
                        CallTopic.Call_Activity_Type__c = idCallActivityImplantSupport ;
                        CallTopic.Call_Category__c =idCallCategorySupport; 
                    } 
                }
                //-BC - 20151020 - CR-9560 - Apply different data for AFS Implants - STOP
                CallTopics.add(CallTopic);                         

                // Creating Call Contacts
                Contact_Visit_Report__c CallContact = new Contact_Visit_Report__c();
                    CallContact.Call_Records__c =  CallRecords[i].Id;
                    CallContact.Attending_Contact__c = impContacts.get(CallRecords[i].Implant__c);
                    CallContact.Attending_Affiliated_Account__c = impAccounts.get(CallRecords[i].Implant__c);
                CallContacts.add(CallContact);                                  
            }
        }

        if(CallContacts.size()>0){
            Insert CallContacts;                    
        }

        if(CallTopics.size()>0){
            Insert CallTopics;                  
        }

        if(Implants.size()>0){
            update Implants;                    
        }

	}


	public static void updateCallRecord(List<Implant__c> lstTriggerNew, Map<Id, Implant__c> mapTriggerOld){

        if (bl_Trigger_Deactivation.isTriggerDeactivated('impl_updateCallRecord')) return;

		if (!GTutil.chkUpdateImplantAfterAll) return;

		loadSettingData();

        // If Implant Type is unattended & previously it was attaended then delete the Call Records
        list<Implant__c> ImpNew = Trigger.new;
        list<Implant__c> ImpOld = Trigger.old;        
        
        list<Call_Records__c> UpdateCallRecords = new list<Call_Records__c>();
        list<Call_Records__c> AllUpdateCallRecords = new list<Call_Records__c>();
        
        map<id,decimal> ImpDuration=new map<id,decimal>();
        Map<Id, Decimal> mapImplantId_ProcedureTime = new Map<Id, Decimal>();   //-BC - 20151020 - CR-9560 
        Map<Id, Boolean> mapImplantId_IsAFS = new Map<Id, Boolean>();           //-BC - 20151020 - CR-9560 
        Map<Id, Id> mapCallRecordId_ImplantId = new Map<Id, Id>();              //-BC - 20151020 - CR-9560 - Added 
        map<id,id> impContacts=new map<id,id>();
        map<id,id> impAccounts=new map<id,id>();
        map<id,id> impTherapy=new map<id,id>();
        map<id,String> impBU=new map<id,String>();
       
                
        for(integer i=0;i<ImpNew.size();i++)
        {   
            Implant__c impTemp = new Implant__c();
            impTemp = ImpOld[i];

            // Update Call Records If Imaplant data chnaged
            if (
                impNew[i].Call_Record_ID__c != null 
                && impNew[i].Attended_Implant_Text__c=='Attended'
            ){

                mapCallRecordId_ImplantId.put(impNew[i].Call_Record_ID__c, impNew[i].Id);   //-BC - 20151020 - CR-9560 - Added 
                if (
                    impNew[i].Implant_Date_Of_Surgery__c != impTemp.Implant_Date_Of_Surgery__c 
                    || impNew[i].OwnerId != impTemp.OwnerId
                    || impNew[i].Therapy__c != impTemp.Therapy__c
                    || impNew[i].Duration_Nr__c != impTemp.Duration_Nr__c 
                    || impNew[i].Procedure_time_in_min__c != impTemp.Procedure_time_in_min__c   //-BC - 20151020 - CR-9560 - Added 
                ){
                    Call_Records__c oCallRecord = new Call_Records__c(
                        id=impNew[i].Call_Record_ID__c
                        , Call_Date__c = impNew[i].Implant_Date_Of_Surgery__c
                        , OwnerId = impNew[i].OwnerId
                        , RTG_Comment__c = impNew[i].RTG_Comments__c
                        , Wait_Time__c = impNew[i].Wait_Time__c
                        , Call_Travel_Time_Minutes__c = impNew[i].Travel_Time__c
                        , Implant__c = impNew[i].Id     //-BC - 20151020 - CR-9560 - Added 
                        //, Business_Unit__c = impNew[i].Business_Unit__c
                    );   
                    UpdateCallRecords.add(oCallRecord); 
                    Call_Records__c CallRecord1 = new Call_Records__c(
                        id = impNew[i].Call_Record_ID__c
                    );
                    AllUpdateCallRecords.add(CallRecord1);                                     
                }
                ImpDuration.put(impNew[i].Id,impNew[i].Duration_Nr__c); 
                impContacts.put(impNew[i].Id,impNew[i].Implant_Implanting_Contact__c);
                impAccounts.put(impNew[i].Id,impNew[i].Implant_Implanting_Account__c);                                                                  
                impTherapy.put(impNew[i].Id,impNew[i].Therapy__c);
                impBU.put(impNew[i].Id,impNew[i].Business_Unit_Text__c);                                                                                  

                mapImplantId_ProcedureTime.put(impNew[i].Id, impNew[i].Procedure_time_in_min__c);   //-BC - 20151020 - CR-9560 
                mapImplantId_IsAFS.put(impNew[i].Id, bl_Implant.bIsAFS(impNew[i].RecordTypeId));    //-BC - 20151020 - CR-9560 

            }            
            
        }

        List<Call_Records__c> lstCallRecord_AFS = new List<Call_Records__c>();  //-BC - 20151020 - CR-9560 
        List<Call_Records__c> lstSTCallRecord=new List<Call_Records__c>();
        List<Call_Records__c> lstOtherCallRecord=new List<Call_Records__c>();
        for(integer i=0;i<AllUpdateCallRecords.size();i++){

            Id id_Implant = mapCallRecordId_ImplantId.get(AllUpdateCallRecords[i].Id);
            if (mapImplantId_IsAFS.containsKey(id_Implant)){
                if (mapImplantId_IsAFS.get(id_Implant)){
                    lstCallRecord_AFS.add(AllUpdateCallRecords[i]);
                }
            }else{
                if(impBU.get(id_Implant) == tBusinessUnit_ST){
                    lstSTCallRecord.add(AllUpdateCallRecords[i]);
                }else{
                    lstOtherCallRecord.add(AllUpdateCallRecords[i]);
                }
            }

        }   

        if(lstSTCallRecord.size()>0){
            list<Call_Topics__c> UpateCallTopic = [select id,Call_Topic_Duration__c,Call_Records__c, Call_Records__r.Implant__c from Call_Topics__c 
            where Call_Records__c in : lstSTCallRecord and Call_Activity_Type__c =: idCallActivityProductDemonstration 
            and Call_Category__c =: idCallCategoryPromotion]; 
            list<Call_Topics__c> UpateCallTopics = new list<Call_Topics__c>();          
            for(Call_Topics__c CallTopic: UpateCallTopic){
                if(ImpDuration.get(CallTopic.Call_Records__r.Implant__c)!=null){
                    Call_Topics__c c = new Call_Topics__c(id=CallTopic.id,
                    Call_Topic_Therapy__c = impTherapy.get(CallTopic.Call_Records__r.Implant__c),
                    Call_Topic_Duration__c=ImpDuration.get(CallTopic.Call_Records__r.Implant__c));
                    UpateCallTopics.add(c);
                }       
            } 
            if(UpateCallTopics.size()>0){
                update UpateCallTopics;
            }
        }  
        if(lstOtherCallRecord.size()>0){
            list<Call_Topics__c> UpateCallTopic = [select id,Call_Topic_Duration__c,Call_Records__c, Call_Records__r.Implant__c from Call_Topics__c 
            where Call_Records__c in : lstOtherCallRecord and Call_Activity_Type__c =:idCallActivityImplantSupport  
            and Call_Category__c =: idCallCategorySupport]; // Replaced these ids with Staging Ids
            list<Call_Topics__c> UpateCallTopics = new list<Call_Topics__c>();          
            for(Call_Topics__c CallTopic: UpateCallTopic){
                if(ImpDuration.get(CallTopic.Call_Records__r.Implant__c)!=null){
                    Call_Topics__c c = new Call_Topics__c(id=CallTopic.id,
                    Call_Topic_Therapy__c = impTherapy.get(CallTopic.Call_Records__r.Implant__c),
                    Call_Topic_Duration__c=ImpDuration.get(CallTopic.Call_Records__r.Implant__c));
                    UpateCallTopics.add(c);
                }       
            } 
            if(UpateCallTopics.size()>0){
                update UpateCallTopics;
            }
        }

        if (lstCallRecord_AFS.size() > 0){
            List<Call_Topics__c> lstCallTopic = 
                [
                    SELECT 
                        Id, Call_Topic_Duration__c, Call_Records__c, Call_Records__r.Implant__c 
                    FROM 
                        Call_Topics__c 
                    WHERE 
                        Call_Records__c in : lstCallRecord_AFS 
                        AND Call_Activity_Type__c = :idCallActivityCaseSupport  
                        AND Call_Category__c = :idCallCategorySupport
                ];

            List<Call_Topics__c> lstCallTopic_Update = new List<Call_Topics__c>();
            for(Call_Topics__c oCallTopic : lstCallTopic){
                if (mapImplantId_ProcedureTime.containsKey(oCallTopic.Call_Records__r.Implant__c)){
                        oCallTopic.Call_Topic_Therapy__c = impTherapy.get(oCallTopic.Call_Records__r.Implant__c);
                        oCallTopic.Call_Topic_Duration__c = mapImplantId_ProcedureTime.get(oCallTopic.Call_Records__r.Implant__c);
                    lstCallTopic_Update.add(oCallTopic);
                }
            } 

            if (lstCallTopic_Update.size() > 0 ){
                update lstCallTopic_Update;
            }
        }

        if(UpdateCallRecords.size()>0){
            update UpdateCallRecords;
        }   		

	}

	public static void setExecutionVariable(String tAction, Boolean bValue){

        if (bl_Trigger_Deactivation.isTriggerDeactivated('impl_setExecutionVariable')) return;

		if (tAction == 'INSERT'){

	        GTutil.chkInsertImplantAfterAll = bValue;

		}else if (tAction == 'INSERT'){

	        GTutil.chkUpdateImplantAfterAll = bValue;

		}

	}


	private static void loadSettingData(){

	    if (oEuropeCallRecord == null){
	    	oEuropeCallRecord = Europe_Call_Record__c.getInstance();
	    }

       idCallActivityImplantSupport = oEuropeCallRecord.Implant_Support__c;  
       idCallActivityCaseSupport = oEuropeCallRecord.Case_Support__c;  //-BC - 20151020 - CR-9560   
       idCallCategorySupport = oEuropeCallRecord.Support__c;
       idCallActivityProductDemonstration = oEuropeCallRecord.Product_Demonstration__c;
       idCallCategoryPromotion = oEuropeCallRecord.Promotion__c;

	}


}
//---------------------------------------------------------------------------------------------------------------------------------