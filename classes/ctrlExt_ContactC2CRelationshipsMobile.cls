public with sharing class ctrlExt_ContactC2CRelationshipsMobile {
	
	public List<Affiliation__c> relationships {get; set;}
	
	public ctrlExt_ContactC2CRelationshipsMobile(ApexPages.StandardController sc){
		
		Contact contact = (Contact) sc.getRecord();
			
		relationships = [Select Id, Affiliation_Type__c, Affiliation_Start_Date__c, Affiliation_End_Date__c, 
							Affiliation_To_Account__c, Affiliation_To_Account__r.Name, Affiliation_From_Account__c, Affiliation_From_Account__r.Name,
							Affiliation_To_Contact__c, Affiliation_To_Contact__r.Name, Affiliation_From_Contact__c, Affiliation_From_Contact__r.Name
							from Affiliation__c	where RecordType.DeveloperName = 'C2C' AND 
							(Affiliation_From_Contact__c =:contact.Id OR Affiliation_To_Contact__c =:contact.Id)
							AND Affiliation_Type__c = 'Referring'
							ORDER BY Affiliation_Start_Date__c ];
		
		for(Affiliation__c rel : relationships){
			
			if(rel.Affiliation_To_contact__c == contact.Id){
								
				Contact contactFrom = rel.Affiliation_From_Contact__r;
				Account accountFrom = rel.Affiliation_From_Account__r;
				
				rel.Affiliation_From_Account__r = rel.Affiliation_To_Account__r;
				rel.Affiliation_From_Contact__r = rel.Affiliation_To_Contact__r;
				rel.Affiliation_To_Contact__r = contactFrom;
				rel.Affiliation_To_Account__r = accountFrom;
			}			
		}
	}
}