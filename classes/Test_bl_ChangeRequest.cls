@isTest private class Test_bl_ChangeRequest {

    @isTest static void test_bl_ChangeRequest() {

		//------------------------------------------------------------------------------
		// Variables
		//------------------------------------------------------------------------------
		boolean bRunAssert = true;
		list<Release_Test__c> lstReleaseTest = new list<Release_Test__c>();
		list<Release_Test_Step__c> lstReleaseTestStep = new list<Release_Test_Step__c>();
		list<Change_Request__c> lstCR = new list<Change_Request__c>();
		id idProcessArea_Tested;
		Change_Request__c oCR_Processed;
		Project__c oRelease_Main = new Project__c();

        integer iCounter = 0;
		integer iRecordCount = 0;
		integer iRecordCount1 = 0;
		integer iRecordCount2 = 0;
		
		// Number of Test Records
		integer iNumberOfProject = 2;
		integer iNumberOfProcessArea = 2;
		integer iNumberOfTest_Optional = 5;
		integer iNumberOfTest_Mandatory = 5;
		integer iNumberOfTestStep = 5;
		integer iNumberOfTest = iNumberOfTest_Optional + iNumberOfTest_Mandatory;
		//------------------------------------------------------------------------------

        //------------------------------------------------------------------------------
        // Create Test Data
        //------------------------------------------------------------------------------
		// Project__c (Release) Data
		list<Project__c> lstProject = new list<Project__c>();
		for (integer i=0; i<iNumberOfProject; i++){
			Project__c oProject = new Project__c();
				oProject.Name = 'TEST PROJECT ' + i;
				oProject.Project_Manager__c = Userinfo.getUserId();
				oProject.End_Date__c = date.today().addDays(15);
			lstProject.add(oProject);
		}
		insert lstProject;
		oRelease_Main = lstProject[0];
		
		//Time Registration Code
		Time_Registration_Code__c oRegistrationCode = new Time_Registration_Code__c();
		oRegistrationCode.Name = 'Test Project';
		oRegistrationCode.Time_Registration_Category__c = 'Test Category';
		oRegistrationCode.Active__c = true;
		oRegistrationCode.Business_Unit__c = 'Test BU';
		oRegistrationCode.Time_Registration_ID__c = 'TRC';
		insert oRegistrationCode;
        
		// Process_Area__c Data
        list<Process_Area__c> lstProcessArea = new list<Process_Area__c>();
        for (integer i=0; i<iNumberOfProcessArea; i++){
	        Process_Area__c oProcessArea = new Process_Area__c();
	        	oProcessArea.Name = 'TEST PA 1';
	    	lstProcessArea.add(oProcessArea);
        }
        insert lstProcessArea;
        
		// Test__c Data
        list<Test__c> lstTest = new list<Test__c>();
        iCounter = 0;
    	for (Process_Area__c oProcessArea : lstProcessArea){
    		for (integer i=0; i<iNumberOfTest_Mandatory; i++){
    			iCounter++;
    			Test__c oTest = new Test__c();
    				oTest.Process_Area__c = oProcessArea.Id;
    				oTest.Status__c = 'Published';
    				oTest.Type__c = 'Mandatory';
    				oTest.Execution__c = 'Automated';
    				oTest.Description__c = 'TEST Description ' + iCounter;
    			lstTest.add(oTest);
    		}

    		for (integer i=0; i<iNumberOfTest_Optional; i++){
    			iCounter++;
    			Test__c oTest = new Test__c();
    				oTest.Process_Area__c = oProcessArea.Id;
    				oTest.Status__c = 'Published';
    				oTest.Type__c = 'Optional';
    				oTest.Execution__c = 'Automated';
    				oTest.Description__c = 'TEST Description ' + iCounter;
    			lstTest.add(oTest);
    		}
    	}
        insert lstTest;
        
        // Test_Step__c Data
        list<Test_Step__c> lstTestStep = new list<Test_Step__c>();
        for (Test__c oTest : lstTest){
        	for (integer i=0; i<iNumberOfTestStep; i++){
        		Test_Step__c oTestStep = new Test_Step__c();
        			oTestStep.Test__c = oTest.Id;
        			oTestStep.Description__c = 'Step Description ' + i;
        			oTestStep.Expected_Result__c = 'Step Expected Result ' + i;
        			oTestStep.Step__c = string.valueOf(i);
        		lstTestStep.add(oTestStep);
        	}
        }
        insert lstTestStep;
        
		// Change_Request__c Data
        list<Change_Request__c> lstChangeRequest_Major = new list<Change_Request__c>();
        list<Change_Request__c> lstChangeRequest_Minor = new list<Change_Request__c>();
        iCounter = 0;
        for (Project__c oProject : lstProject){
        	for (Process_Area__c oProcessArea : lstProcessArea){
        		for (integer i=0; i<2; i++){
	        		iCounter++;
					Change_Request__c oChangeRequest_Major1 = new Change_Request__c();
						oChangeRequest_Major1.Project__c = oProject.Id;
						oChangeRequest_Major1.Process_Area__c = oProcessArea.Id;
						oChangeRequest_Major1.Requestor_Lookup__c = Userinfo.getUserId();
						oChangeRequest_Major1.Request_Date__c = Date.today();
						oChangeRequest_Major1.Change_Request__c = 'TEST ChangeRequest ' + iCounter;
						oChangeRequest_Major1.Change_Weight__c = 'Major';
						oChangeRequest_Major1.Status__c = 'Can be planned';
						oChangeRequest_Major1.Validation_Checked__c = true;						 
						oChangeRequest_Major1.Deployment__c = 'Regular Release';
						oChangeRequest_Major1.Action_Taker_Lookup__c = Userinfo.getUserId();
						oChangeRequest_Major1.Reviewed_by_core_team__c = true;	// Needed to set the status to Can be planned
						oChangeRequest_Major1.Total_Cost_Approved__c = true;		// Needed to set the status to Can be planned
						oChangeRequest_Major1.Time_Registration_Project__c = oRegistrationCode.Id; 
					lstChangeRequest_Major.add(oChangeRequest_Major1);
	        		iCounter++;
					Change_Request__c oChangeRequest_Minor = new Change_Request__c();
						oChangeRequest_Minor.Project__c = oProject.Id;
						oChangeRequest_Minor.Process_Area__c = oProcessArea.Id;
						oChangeRequest_Minor.Requestor_Lookup__c = Userinfo.getUserId();
						oChangeRequest_Minor.Request_Date__c = Date.today();
						oChangeRequest_Minor.Change_Request__c = 'TEST ChangeRequest ' + iCounter;
						oChangeRequest_Minor.Change_Weight__c = 'Minor';
						oChangeRequest_Minor.Status__c = 'Can be planned';
						oChangeRequest_Minor.Validation_Checked__c = true;		
						oChangeRequest_Minor.Deployment__c = 'Regular Release';
						oChangeRequest_Minor.Action_Taker_Lookup__c = Userinfo.getUserId();
						oChangeRequest_Minor.Reviewed_by_core_team__c = true;	// Needed to set the status to Can be planned
						oChangeRequest_Minor.Total_Cost_Approved__c = true;		// Needed to set the status to Can be planned
						oChangeRequest_Minor.Time_Registration_Project__c = oRegistrationCode.Id; 
					lstChangeRequest_Minor.add(oChangeRequest_Minor);
        		}
        	} 
        }
		insert lstChangeRequest_Major;
		insert lstChangeRequest_Minor;

		// Map with Process Area ID and the related Test records
		map<id, list<Test__c>> mapProcessAreaID_Tests = new map<id, list<Test__c>>();
		map<id, list<Test__c>> mapProcessAreaID_Tests_Mandatory = new map<id, list<Test__c>>();
		map<id, list<Test__c>> mapProcessAreaID_Tests_Optional = new map<id, list<Test__c>>();
		for (Test__c oTest : lstTest){
			list<Test__c> lstTmp = new list<Test__c>();
			if (mapProcessAreaID_Tests.containsKey(oTest.Process_Area__c)){
				lstTmp = mapProcessAreaID_Tests.get(oTest.Process_Area__c);
			}
			lstTmp.add(oTest);
			mapProcessAreaID_Tests.put(oTest.Process_Area__c, lstTmp);

			list<Test__c> lstTmp2 = new list<Test__c>();
			if (oTest.Type__c == 'Mandatory'){
				if (mapProcessAreaID_Tests_Mandatory.containsKey(oTest.Process_Area__c)){
					lstTmp2 = mapProcessAreaID_Tests_Mandatory.get(oTest.Process_Area__c);
				}
				lstTmp2.add(oTest);
				mapProcessAreaID_Tests_Mandatory.put(oTest.Process_Area__c, lstTmp2);
			}else if (oTest.Type__c == 'Optional'){
				if (mapProcessAreaID_Tests_Optional.containsKey(oTest.Process_Area__c)){
					lstTmp2 = mapProcessAreaID_Tests_Optional.get(oTest.Process_Area__c);
				}
				lstTmp2.add(oTest);
				mapProcessAreaID_Tests_Optional.put(oTest.Process_Area__c, lstTmp2);
			}
		}
		
		// Map with Process Area ID and the related Change Records records
		map<id, list<Change_Request__c>> mapProcessAreaID_ChangeRequests_Major = new map<id, list<Change_Request__c>>();
		map<id, list<Change_Request__c>> mapProcessAreaID_ChangeRequests_Minor = new map<id, list<Change_Request__c>>();
		for (Change_Request__c oCR : lstChangeRequest_Major){
			list<Change_Request__c> lstTmp = new list<Change_Request__c>();
			if (mapProcessAreaID_ChangeRequests_Major.containsKey(oCR.Process_Area__c)){
				lstTmp = mapProcessAreaID_ChangeRequests_Major.get(oCR.Process_Area__c);
			}
			lstTmp.add(oCR);
			mapProcessAreaID_ChangeRequests_Major.put(oCR.Process_Area__c, lstTmp);
		}
		for (Change_Request__c oCR : lstChangeRequest_Minor){
			list<Change_Request__c> lstTmp = new list<Change_Request__c>();
			if (mapProcessAreaID_ChangeRequests_Minor.containsKey(oCR.Process_Area__c)){
				lstTmp = mapProcessAreaID_ChangeRequests_Minor.get(oCR.Process_Area__c);
			}
			lstTmp.add(oCR);
			mapProcessAreaID_ChangeRequests_Minor.put(oCR.Process_Area__c, lstTmp);
		}
        //------------------------------------------------------------------------------

		Test.startTest();

        //------------------------------------------------------------------------------
        // INITIAL TEST
        //------------------------------------------------------------------------------
		if (bRunAssert){
			lstReleaseTest = [SELECT ID FROM Release_Test__c];
			system.assertEquals(lstReleaseTest.size(), 0);
		}
        //------------------------------------------------------------------------------

		list<Change_Request__c> lstCR_Processed = new list<Change_Request__c>();
		
        //------------------------------------------------------------------------------
        // TEST LOGIC 1
        //------------------------------------------------------------------------------
		//	If Status is set to “Planned” and  there are no other planned CR’s within the same process area linked to the release
		//		- If the CR is marked as Major change (Weight)
		//			--> Add all tests related to the process area
        //------------------------------------------------------------------------------
		clsUtil.debug('START TEST 1');

		for (id idProcessArea : mapProcessAreaID_ChangeRequests_Major.keySet()){
			idProcessArea_Tested = idProcessArea;
			lstCR = mapProcessAreaID_ChangeRequests_Major.get(idProcessArea);

			for (Change_Request__c oCR : lstCR){

				if (oCR.Project__c == oRelease_Main.Id){				
						oCR_Processed = oCR;
						oCR_Processed.Status__c = 'Planned';
						oCR_Processed.Process_Area__c = idProcessArea_Tested;
						oCR_Processed.Go_Live_Date__c = date.today();		// Needed to set the status to Planned
					update oCR_Processed;

					lstCR_Processed.add(oCR_Processed);
					break;

				}
			}
			break;	
		}
		
		if (bRunAssert){
			lstProcessArea = [SELECT ID, (SELECT ID, Status__c FROM Change_Requests__r WHERE Status__c = 'Planned') FROM Process_Area__c WHERE ID = :idProcessArea_Tested];
			system.assertEquals(lstProcessArea[0].Change_Requests__r.size(), 1);

			lstReleaseTest = [SELECT ID, Test__c, Test__r.Process_Area__c, Test__r.Type__c FROM Release_Test__c WHERE Release__c = :oRelease_Main.Id];
			iRecordCount = lstReleaseTest.size();
			system.assertEquals(mapProcessAreaID_Tests.get(idProcessArea_Tested).size(), iRecordCount);
			for (Release_Test__c oReleaseTest : lstReleaseTest){
				system.assertEquals(oReleaseTest.Test__r.Process_Area__c, idProcessArea_Tested);
			}
			lstReleaseTestStep = [SELECT ID FROM Release_Test_Step__c WHERE Release_Test__c = :lstReleaseTest];
			system.assertEquals(mapProcessAreaID_Tests.get(idProcessArea_Tested).size() * iNumberOfTestStep, lstReleaseTestStep.size());
		}

		clsUtil.debug('PASSED TEST 1');
        //------------------------------------------------------------------------------


        //------------------------------------------------------------------------------
        // TEST LOGIC 2a
        //------------------------------------------------------------------------------
		//	If Status changes from “Planned” to “Completed”
		//		--> do nothing
        //------------------------------------------------------------------------------
		clsUtil.debug('START TEST 2a');

		oCR_Processed.Status__c = 'Completed';
		oCR_Processed.release_notes__c = 'test';
		
		update oCR_Processed;
		
		if (bRunAssert){
			lstReleaseTest = [SELECT ID, Test__c, Test__r.Process_Area__c, Test__r.Type__c FROM Release_Test__c WHERE Release__c = :oRelease_Main.Id];
			system.assertEquals(lstReleaseTest.size(), mapProcessAreaID_Tests.get(oCR_Processed.Process_Area__c).size());		// no Release_Test__c records are created so the number of records should not be changed

			lstReleaseTestStep = [SELECT ID FROM Release_Test_Step__c WHERE Release_Test__c = :lstReleaseTest];
			system.assertEquals(lstReleaseTestStep.size(), mapProcessAreaID_Tests.get(oCR_Processed.Process_Area__c).size() * iNumberOfTestStep);		// no Release_Test_Step__c records are created so the number of records should not be changed
		}

		clsUtil.debug('PASSED TEST 2a');
        //------------------------------------------------------------------------------


        //------------------------------------------------------------------------------
        // TEST LOGIC 2b
        //------------------------------------------------------------------------------
		//	If Status changes from “Completed” to “Planned”
		//		--> do nothing
        //------------------------------------------------------------------------------
		clsUtil.debug('START TEST 2b');

		if (bRunAssert){
			system.assertEquals(oCR_Processed.Status__c, 'Completed');
		}
		
			oCR_Processed.Status__c = 'Planned';
		update oCR_Processed;

		if (bRunAssert){
			lstProcessArea = [SELECT ID, (SELECT ID, Status__c FROM Change_Requests__r WHERE Status__c = 'Planned') FROM Process_Area__c WHERE ID = :idProcessArea_Tested];
			system.assertEquals(lstProcessArea[0].Change_Requests__r.size(), 1);
		
			lstReleaseTest = [SELECT ID, Test__c, Test__r.Process_Area__c, Test__r.Type__c FROM Release_Test__c WHERE Release__c = :oRelease_Main.Id];
			system.assertEquals(lstReleaseTest.size(), mapProcessAreaID_Tests.get(idProcessArea_Tested).size());		// no Release_Test__c records are created so the number of records should not be changed
	
			lstReleaseTestStep = [SELECT ID FROM Release_Test_Step__c WHERE Release_Test__c = :lstReleaseTest];
			system.assertEquals(lstReleaseTestStep.size(), mapProcessAreaID_Tests.get(idProcessArea_Tested).size() * iNumberOfTestStep);		// no Release_Test_Step__c records are created so the number of records should not be changed
		}

		clsUtil.debug('PASSED TEST 2b');
        //------------------------------------------------------------------------------
		
		
        //------------------------------------------------------------------------------
        // TEST LOGIC 3
        //------------------------------------------------------------------------------
		//	If Status is set to “Planned” and there are other planned "Major" CR’s within the same process area linked to the release
		//		--> do nothing
        //------------------------------------------------------------------------------
		clsUtil.debug('START TEST 3');

		if (bRunAssert){
			lstProcessArea = [SELECT ID, (SELECT ID, Status__c, Change_Weight__c FROM Change_Requests__r WHERE Status__c = 'Planned') FROM Process_Area__c WHERE ID = :idProcessArea_Tested];
			system.assertEquals(lstProcessArea[0].Change_Requests__r.size(), 1);
	
			lstCR = [SELECT ID, Process_Area__c FROM Change_Request__c WHERE Project__c = :oRelease_Main.Id AND Status__c != 'Planned' AND Process_Area__c = :idProcessArea_Tested];
			system.assertEquals(lstCR.size(), 3);
		}

		lstCR = mapProcessAreaID_ChangeRequests_Major.get(idProcessArea_Tested);
		for (Change_Request__c oCR : lstCR){
			if (oCR.Project__c == oRelease_Main.Id){
				if (oCR.Status__c != 'Planned'){
					oCR_Processed = oCR;
					oCR_Processed.Status__c = 'Planned';	
					oCR_Processed.Go_Live_Date__c = date.today();		// Needed to set the status to Planned
					update oCR_Processed;
					lstCR_Processed.add(oCR_Processed);
					break;
				}
			}
		}

		if (bRunAssert){
			system.assertEquals(lstCR_Processed.size(), 2);

			lstCR = [SELECT ID FROM Change_Request__c WHERE Project__c = :oRelease_Main.Id AND Status__c = 'Planned'];
			system.assertEquals(lstCR.size(), 2);
	
			lstReleaseTest = [SELECT ID, Test__c, Test__r.Process_Area__c, Test__r.Type__c FROM Release_Test__c WHERE Release__c = :oRelease_Main.Id];
			system.assertEquals(lstReleaseTest.size(), mapProcessAreaID_Tests.get(oCR_Processed.Process_Area__c).size());		// no Release_Test__c records are created so the number of records should not be changed
	
			lstReleaseTestStep = [SELECT ID FROM Release_Test_Step__c WHERE Release_Test__c = :lstReleaseTest];
			system.assertEquals(lstReleaseTestStep.size(), mapProcessAreaID_Tests.get(oCR_Processed.Process_Area__c).size() * iNumberOfTestStep);		// no Release_Test_Step__c records are created so the number of records should not be changed
		}

		clsUtil.debug('PASSED TEST 3');
        //------------------------------------------------------------------------------

		
        //------------------------------------------------------------------------------
        // TEST LOGIC 4a
        //------------------------------------------------------------------------------
		//	If Status changes from “Planned” to anything else then “Completed”; 
		//		- If there are other MAJOR "Planned" CR’s within the same process area linked to the release
		//			--> do nothing. 
        //------------------------------------------------------------------------------
		clsUtil.debug('START TEST 4a');

		if (bRunAssert){
			lstCR = [SELECT ID FROM Change_Request__c WHERE Project__c = :oRelease_Main.Id AND Status__c = 'Planned'];
			system.assertEquals(lstCR.size(), 2);
		}
		
		oCR_Processed = lstCR_Processed[0]; 
			oCR_Processed.Status__c = 'Can be planned';
		update oCR_Processed;			

		if (bRunAssert){
			lstReleaseTest = [SELECT ID, Test__c FROM Release_Test__c WHERE Release__c = :oRelease_Main.Id];
			system.assertEquals(lstReleaseTest.size(), mapProcessAreaID_Tests.get(oCR_Processed.Process_Area__c).size());		// no Release_Test__c records are created so the number of records should not be changed

			lstReleaseTestStep = [SELECT ID FROM Release_Test_Step__c WHERE Release_Test__c = :lstReleaseTest];
			system.assertEquals(lstReleaseTestStep.size(), mapProcessAreaID_Tests.get(oCR_Processed.Process_Area__c).size() * iNumberOfTestStep);		// no Release_Test_Step__c records are created so the number of records should not be changed
		}

		clsUtil.debug('PASSED TEST 4a');
        //------------------------------------------------------------------------------


        //------------------------------------------------------------------------------
        // TEST LOGIC 4c
        //------------------------------------------------------------------------------
		//	If Status changes from “Planned” to anything else then “Completed”; 
		//  	- If there are no other "Planned" CR’s within the same process area linked to the release
		//			--> delete the release test records
        //------------------------------------------------------------------------------
		clsUtil.debug('START TEST 4c');

			lstCR_Processed[1].Status__c = 'Can be planned';
		update lstCR_Processed[1];			
		
		if (bRunAssert){
			lstReleaseTest = [SELECT ID, Test__c FROM Release_Test__c WHERE Release__c = :oRelease_Main.Id];
			iRecordCount = lstReleaseTest.size();
			system.assertEquals(iRecordCount, 0);

			lstReleaseTestStep = [SELECT ID FROM Release_Test_Step__c WHERE Release_Test__c = :lstReleaseTest];
			system.assertEquals(lstReleaseTestStep.size(), 0);
			lstReleaseTestStep = [SELECT ID FROM Release_Test_Step__c];
			system.assertEquals(lstReleaseTestStep.size(), 0);
		}

		clsUtil.debug('PASSED TEST 4c');
        //------------------------------------------------------------------------------


        //------------------------------------------------------------------------------
        // TEST LOGIC 5
        //------------------------------------------------------------------------------
		//	If Status is set to “Planned” and  there are no other planned CR’s within the same process area linked to the release
		//		- If the CR is market as Minor change (Weight)
		//			--> Add only those tests related to the process area that are marked as “Mandatory”
        //------------------------------------------------------------------------------
		clsUtil.debug('START TEST 5');

		lstReleaseTest = [SELECT ID, Test__c, Test__r.Process_Area__c, Test__r.Type__c FROM Release_Test__c];
		iRecordCount = lstReleaseTest.size();

		for (id idProcessArea : mapProcessAreaID_ChangeRequests_Minor.keySet()){
			idProcessArea_Tested = idProcessArea;
			lstCR = mapProcessAreaID_ChangeRequests_Minor.get(idProcessArea);
			for (Change_Request__c oCR : lstCR){
				if (oCR.Project__c == oRelease_Main.Id){
					oCR_Processed = oCR;
					oCR_Processed.Status__c = 'Planned';	
					oCR_Processed.Process_Area__c = idProcessArea_Tested;
					oCR_Processed.Go_Live_Date__c = date.today();		// Needed to set the status to Planned
					update oCR_Processed;
					lstCR_Processed.add(oCR_Processed);
					break;
				}
			}
			break;	
		}
		
		if (bRunAssert){
			lstReleaseTest = [SELECT ID, Test__c, Test__r.Process_Area__c, Test__r.Type__c FROM Release_Test__c];
			iRecordCount = lstReleaseTest.size();
	
			lstReleaseTest = [SELECT ID, Test__c, Test__r.Process_Area__c, Test__r.Type__c FROM Release_Test__c WHERE Release__c =: oRelease_Main.Id];
			iRecordCount = lstReleaseTest.size();

			system.assertEquals(mapProcessAreaID_Tests_Mandatory.get(idProcessArea_Tested).size(), iRecordCount);
			for (Release_Test__c oReleaseTest : lstReleaseTest){
				system.assertEquals(oReleaseTest.Test__r.Process_Area__c, idProcessArea_Tested);
				system.assertEquals(oReleaseTest.Test__r.Type__c, 'Mandatory');
			}
			
			lstReleaseTestStep = [SELECT ID FROM Release_Test_Step__c WHERE Release_Test__c = :lstReleaseTest];
			system.assertEquals(lstReleaseTestStep.size(), mapProcessAreaID_Tests_Mandatory.get(idProcessArea_Tested).size() * iNumberOfTestStep);
		}		
        
		clsUtil.debug('PASSED TEST 5');
        //------------------------------------------------------------------------------

		
        //------------------------------------------------------------------------------
        // TEST LOGIC 6
        //------------------------------------------------------------------------------
		//	If Status is set to “Planned”, weight is Minor and  there are other planned CR’s within the same process area linked to the release
		//		--> do nothing        
        //------------------------------------------------------------------------------
		clsUtil.debug('START TEST 6');

		for (id idProcessArea : mapProcessAreaID_ChangeRequests_Minor.keySet()){
			idProcessArea_Tested = idProcessArea;
			lstCR = mapProcessAreaID_ChangeRequests_Minor.get(idProcessArea);
			for (Change_Request__c oCR : lstCR){
				if (oCR.Project__c == oRelease_Main.Id){
					if (oCR.Status__c != 'Planned'){
						oCR_Processed = oCR;
						oCR_Processed.Status__c = 'Planned';	
						oCR_Processed.Process_Area__c = idProcessArea_Tested;
						oCR_Processed.Go_Live_Date__c = date.today();		// Needed to set the status to Planned
						update oCR_Processed;
						lstCR_Processed.add(oCR_Processed);
						break;
					}
				}
			}
			break;	
		}
		if (bRunAssert){
			lstReleaseTest = [SELECT ID, Test__c, Test__r.Process_Area__c, Test__r.Type__c FROM Release_Test__c WHERE Release__c =: oRelease_Main.Id];
			system.assertEquals(lstReleaseTest.size(), iRecordCount);		// no Release_Test__c records are created so the number of records should not be changed

			lstReleaseTestStep = [SELECT ID FROM Release_Test_Step__c WHERE Release_Test__c = :lstReleaseTest];
			system.assertEquals(lstReleaseTestStep.size(), mapProcessAreaID_Tests_Mandatory.get(idProcessArea_Tested).size() * iNumberOfTestStep);	// no Release_Test_Step_c records are created so the number of records should not be changed
		}
		
		clsUtil.debug('PASSED TEST 6');
        //------------------------------------------------------------------------------


        //------------------------------------------------------------------------------
        // TEST LOGIC 7
        //------------------------------------------------------------------------------
		//	If Status is set to “Planned”, weight is Major and  there are no other planned Major CR’s but there are Minor CR’s within the same process area linked to the release; 
		//		--> Only add the tests that are marked as “Optional” for that process area
        //------------------------------------------------------------------------------
		clsUtil.debug('START TEST 7');

		if (bRunAssert) {
			lstCR = [SELECT ID, Change_Weight__c FROM Change_Request__c WHERE Project__c = :oRelease_Main.Id AND Status__c = 'Planned'];
			integer iCounter_Major = 0;
			integer iCounter_Minor = 0;
			for (Change_Request__c oCR : lstCR){
				if (oCR.Change_Weight__c == 'Major'){
					iCounter_Major++;
				}else if (oCR.Change_Weight__c == 'Minor'){
					iCounter_Minor++;
				}
			}
			system.assertEquals(iCounter_Major, 0);
			system.assertNotEquals(iCounter_Minor, 0);
		}
		
		for (id idProcessArea : mapProcessAreaID_ChangeRequests_Major.keySet()){
			idProcessArea_Tested = idProcessArea;
			lstCR = mapProcessAreaID_ChangeRequests_Major.get(idProcessArea);
			for (Change_Request__c oCR : lstCR){
				if ( (oCR.Project__c == oRelease_Main.Id) && (oCR.Status__c == 'Can be planned') ){
					oCR_Processed = oCR;
					oCR_Processed.Status__c = 'Planned';	
					oCR_Processed.Process_Area__c = idProcessArea_Tested;
					oCR_Processed.Go_Live_Date__c = date.today();		// Needed to set the status to Planned
					update oCR_Processed;
					lstCR_Processed.add(oCR_Processed);
					break;
				}
			}
			break;	
		}

		if (bRunAssert){
			lstReleaseTest = [SELECT ID, Test__c, Test__r.Process_Area__c, Test__r.Type__c FROM Release_Test__c WHERE Release__c = :oRelease_Main.Id];
			iRecordCount = lstReleaseTest.size(); 
			integer iCounter_Optional = 0;
			integer iCounter_Mandatory = 0;
			for (Release_Test__c oRT : lstReleaseTest){
				if (oRT.Test__r.Type__c == 'Optional'){
					iCounter_Optional++;
				}else if (oRT.Test__r.Type__c == 'Mandatory'){
					iCounter_Mandatory++;
				}
			}
			system.assertEquals(iCounter_Optional, mapProcessAreaID_Tests_Optional.get(idProcessArea_Tested).size());
			system.assertEquals(iCounter_Mandatory, mapProcessAreaID_Tests_Mandatory.get(idProcessArea_Tested).size());
			
			system.assertEquals(mapProcessAreaID_Tests.get(idProcessArea_Tested).size(), iRecordCount);	// = 5 + 5 = 10
			for (Release_Test__c oReleaseTest : lstReleaseTest){
				system.assertEquals(oReleaseTest.Test__r.Process_Area__c, idProcessArea_Tested);
			}
	
			lstReleaseTestStep = [SELECT ID FROM Release_Test_Step__c WHERE Release_Test__c = :lstReleaseTest];
			system.assertEquals(lstReleaseTestStep.size(), mapProcessAreaID_Tests.get(idProcessArea_Tested).size() * iNumberOfTestStep);
		}

		clsUtil.debug('PASSED TEST 7');
        //------------------------------------------------------------------------------


        //------------------------------------------------------------------------------
        // TEST LOGIC 4b
        //------------------------------------------------------------------------------
		//	If Status changes from “Planned” to anything else then “Completed”; 
		//		- If there are ONLY other MINOR planned CR’s within the same process area linked to the release
		//			--> delete the optional release test records. 
        //------------------------------------------------------------------------------
		clsUtil.debug('START TEST 4b');

			oCR_Processed.Status__c = 'Can be planned';
		update oCR_Processed;

		if (bRunAssert){
			lstReleaseTest = [SELECT ID, Test__c, Test__r.Process_Area__c, Test__r.Type__c FROM Release_Test__c WHERE Release__c = :oRelease_Main.Id];
			iRecordCount = lstReleaseTest.size(); 
			
			system.assertEquals(mapProcessAreaID_Tests_Mandatory.get(idProcessArea_Tested).size(), iRecordCount);	// Only Mandatory Test are still there
			for (Release_Test__c oReleaseTest : lstReleaseTest){
				system.assertEquals(oReleaseTest.Test__r.Process_Area__c, idProcessArea_Tested);
				system.assertEquals(oReleaseTest.Test__r.Type__c, 'Mandatory');
			}
	
			lstReleaseTestStep = [SELECT ID FROM Release_Test_Step__c WHERE Release_Test__c = :lstReleaseTest];
			system.assertEquals(lstReleaseTestStep.size(), mapProcessAreaID_Tests_Mandatory.get(idProcessArea_Tested).size() * iNumberOfTestStep);
			lstReleaseTestStep = [SELECT ID FROM Release_Test_Step__c];
			system.assertEquals(lstReleaseTestStep.size(), mapProcessAreaID_Tests_Mandatory.get(idProcessArea_Tested).size() * iNumberOfTestStep);
		}

		clsUtil.debug('PASSED TEST 4b');
        //------------------------------------------------------------------------------


        //------------------------------------------------------------------------------
        // TEST LOGIC 8 
        //------------------------------------------------------------------------------
		//	If the Release linked to the Change Request is changed AND the status is NOT “Can be planned” 
		//		--> the record cannot be saved and an error will become visible.
        //------------------------------------------------------------------------------
		clsUtil.debug('START TEST 8');

		oCR_Processed = [SELECT ID, Status__c, Project__c FROM Change_Request__c WHERE Project__c = :oRelease_Main.Id AND Status__c = 'Planned' AND Process_Area__c = :idProcessArea_Tested LIMIT 1];
		if (bRunAssert){ 
			system.assertEquals(lstProject.size(), 2);	
		}

		for (Project__c oProject : lstProject){
			if (oProject.id != oCR_Processed.Project__c){
				oCR_Processed.Project__c = oProject.Id;
				break;
			}
		}
		boolean bError = false;
		try{
			update oCR_Processed;
		}catch(Exception oEX){
			bError = true;
		}
		
		if (bRunAssert){
			system.assert(bError);
		}

		clsUtil.debug('PASSED TEST 8');
        //------------------------------------------------------------------------------


        //------------------------------------------------------------------------------
        // TEST LOGIC 9
        //------------------------------------------------------------------------------
		//	DETELE
		//		If there are still "Planned" Change Request within the same Process Area linked to the Release
		//			--> do nothing
        //------------------------------------------------------------------------------
		clsUtil.debug('START TEST 9');

		lstCR = [SELECT ID, Status__c, Project__c FROM Change_Request__c WHERE Project__c = :oRelease_Main.Id AND Status__c = 'Planned' AND Process_Area__c = :idProcessArea_Tested ];
		if (bRunAssert){
			system.assertEquals(lstCR.size(), 2);

			lstReleaseTest = [SELECT ID, Test__c, Test__r.Process_Area__c, Test__r.Type__c FROM Release_Test__c WHERE Release__c = :oRelease_Main.Id];
			iRecordCount1 = lstReleaseTest.size();
			system.assertNotEquals(iRecordCount1, 0); 
		}
				
		delete lstCR[0];

		if (bRunAssert){
			lstReleaseTest = [SELECT ID, Test__c, Test__r.Process_Area__c, Test__r.Type__c FROM Release_Test__c WHERE Release__c = :oRelease_Main.Id];
			iRecordCount2 = lstReleaseTest.size();
			system.assertEquals(iRecordCount1, iRecordCount2); 
		}

		clsUtil.debug('PASSED TEST 9');
        //------------------------------------------------------------------------------


        //------------------------------------------------------------------------------
        // TEST LOGIC 10
        //------------------------------------------------------------------------------
		//	DETELE
		//		If there are NO "Planned" Change Request within the same Process Area linked to the Release
		//			--> Delete all Release Tests for that Process Area and for that release
        //------------------------------------------------------------------------------
		clsUtil.debug('START TEST 10');

		lstCR = [SELECT ID, Status__c, Project__c FROM Change_Request__c WHERE Project__c = :oRelease_Main.Id AND Status__c = 'Planned' AND Process_Area__c = :idProcessArea_Tested ];
		if (bRunAssert){
			system.assertEquals(lstCR.size(), 1);
	
			lstReleaseTest = [SELECT ID, Test__c, Test__r.Process_Area__c, Test__r.Type__c FROM Release_Test__c WHERE Release__c = :oRelease_Main.Id];
			iRecordCount1 = lstReleaseTest.size();
			system.assertNotEquals(iRecordCount1, 0); 
			
			lstReleaseTestStep = [SELECT ID FROM Release_Test_Step__c];
			system.assertNotEquals(lstReleaseTestStep.size(), 0);
		}		
		
		delete lstCR[0];

		if (bRunAssert){
			lstReleaseTest = [SELECT ID, Test__c, Test__r.Process_Area__c, Test__r.Type__c FROM Release_Test__c WHERE Release__c = :oRelease_Main.Id];
			iRecordCount2 = lstReleaseTest.size();
			system.assertNotEquals(iRecordCount1, iRecordCount2); 
			system.assert(iRecordCount1 > iRecordCount2); 
			system.assertEquals(iRecordCount2, 0); 

			lstReleaseTestStep = [SELECT ID FROM Release_Test_Step__c];
			system.assertEquals(lstReleaseTestStep.size(), 0);
		}

		clsUtil.debug('PASSED TEST 10');
        //------------------------------------------------------------------------------


        //------------------------------------------------------------------------------
        // TEST LOGIC ADDITIONAL 1
        //------------------------------------------------------------------------------
		//	INSERT
		//		When a Change Request is created with the status "Planned"
		//			--> the Release Tests should also be created
        //------------------------------------------------------------------------------
		clsUtil.debug('START TEST ADDITIONAL 1');
		list<Change_Request__c> lstChangeRequest_Major_Planned = new list<Change_Request__c>();
        iCounter = 0;
	    for (Project__c oProject : lstProject){
        	for (Process_Area__c oProcessArea : lstProcessArea){
				Change_Request__c oChangeRequest_Major_Planned = new Change_Request__c();
					oChangeRequest_Major_Planned.Project__c = oProject.Id;
					oChangeRequest_Major_Planned.Process_Area__c = oProcessArea.Id;
					oChangeRequest_Major_Planned.Requestor_Lookup__c = Userinfo.getUserId();
					oChangeRequest_Major_Planned.Request_Date__c = Date.today();
					oChangeRequest_Major_Planned.Change_Request__c = 'TEST ChangeRequest ' + iCounter;
					oChangeRequest_Major_Planned.Change_Weight__c = 'Major';
					oChangeRequest_Major_Planned.Status__c = 'Planned';
					oChangeRequest_Major_Planned.Validation_Checked__c = true;
					oChangeRequest_Major_Planned.Deployment__c = 'Regular Release';
					oChangeRequest_Major_Planned.Go_Live_Date__c = date.today();
					oChangeRequest_Major_Planned.Action_Taker_Lookup__c = Userinfo.getUserId();
					oChangeRequest_Major_Planned.Reviewed_by_core_team__c = true;		// Needed to set the status to Can be planned
					oChangeRequest_Major_Planned.Total_Cost_Approved__c = true;		// Needed to set the status to Can be planned
					oChangeRequest_Major_Planned.Time_Registration_Project__c = oRegistrationCode.Id; 
				lstChangeRequest_Major_Planned.add(oChangeRequest_Major_Planned);
        		iCounter++;
        	}
	    }
	    insert lstChangeRequest_Major_Planned;

		if (bRunAssert){
			lstReleaseTestStep = [SELECT ID FROM Release_Test_Step__c];
			system.assertNotEquals(lstReleaseTestStep.size(), 0);
		}
		clsUtil.debug('STOP TEST ADDITIONAL 1');

		Test.stopTest();
    }


	@IsTest static void test_calculateTotalCostOnBundle(){

		//------------------------------------------------------------------------------------------------
		// Create Test Data
		//------------------------------------------------------------------------------------------------
		clsTestData_MasterData.createCompany();

		List<Change_Request_Bundle__c> lstChangeRequestBundle = new List<Change_Request_Bundle__c>();
		Change_Request_Bundle__c oChangeRequestBundle1 = new Change_Request_Bundle__c();
			oChangeRequestBundle1.Name = 'CR BUNDLE 1';
			oChangeRequestBundle1.CurrencyIsoCode = 'EUR';
		lstChangeRequestBundle.add(oChangeRequestBundle1);
		Change_Request_Bundle__c oChangeRequestBundle2 = new Change_Request_Bundle__c();
			oChangeRequestBundle2.Name = 'CR BUNDLE 2';
			oChangeRequestBundle2.CurrencyIsoCode = 'EUR';
		lstChangeRequestBundle.add(oChangeRequestBundle2);
		insert lstChangeRequestBundle;
		Set<Id> setID_ChangeRequestBundle = new Set<Id>{lstChangeRequestBundle[0].Id, lstChangeRequestBundle[1].Id};

		List<Change_Request__c> lstChangeRequest = new List<Change_Request__c>();
		for (Integer iCounter = 0; iCounter < 5; iCounter++){
			Change_Request__c oCR = new Change_Request__c();
				oCR.RecordTypeId = clsUtil.getRecordTypeByDevName('Change_Request__c', 'General').Id;
				oCR.CurrencyIsoCode = 'EUR';
				oCR.Change_Request__c = 'CR ' + String.valueOf(iCounter);
				oCR.Request_Date__c = Date.today();
				oCR.Requestor_Lookup__c = UserInfo.getUserId();
				oCR.Requesting_BU__c = 'IT';
				oCR.Requesting_Department__c = 'Service & Solutions';
				oCR.Origin__c = 'Salesforce.com';
				oCR.Status__c = 'New';
				oCR.Deployment__c = 'Regular Release';
				oCR.Company__c = clsTestData_MasterData.oMain_Company.Id;
				oCR.Tool__c = 'Salesforce.com';
				oCR.Total_Cost__c = 1500;
				oCR.Change_Request_Bundle__c = oChangeRequestBundle1.Id;
			lstChangeRequest.add(oCR);
		}
		insert lstChangeRequest;			
		//------------------------------------------------------------------------------------------------


		//------------------------------------------------------------------------------------------------
		// Test Logic
		//------------------------------------------------------------------------------------------------
		Test.startTest();

			bl_ChangeRequest.calculateTotalCostOnBundle(setID_ChangeRequestBundle, true);

		Test.stopTest();
		//------------------------------------------------------------------------------------------------


		//------------------------------------------------------------------------------------------------
		// Validate Result
		//------------------------------------------------------------------------------------------------
		oChangeRequestBundle1 = [SELECT Id, Total_Cost__c FROM Change_Request_Bundle__c WHERE Id = :oChangeRequestBundle1.Id];

		Decimal decTotalCost = oChangeRequestBundle1.Total_Cost__c.round();
		System.assertEquals(decTotalCost, 1500*5);

		oChangeRequestBundle2 = [SELECT Id, Total_Cost__c FROM Change_Request_Bundle__c WHERE Id = :oChangeRequestBundle2.Id];
		decTotalCost = oChangeRequestBundle2.Total_Cost__c.round();
		System.assertEquals(decTotalCost, 0);
		//------------------------------------------------------------------------------------------------

	}

}