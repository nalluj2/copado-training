public with sharing class controllerAllocationAggregated {
/**
 * Creation Date: 	20091001
 * Description: 	controllerAllocationAggregated controls the Visualforce Page AllocationScreen. 
 					This screen will let the users manage the allocation ( edit & view)  of the different Invoice Lines dispatched trough the 
 					different sales rep ( Per Territory, per account, ... )
 					(depending on the Fiscal Year, Fiscal Month, and the account choosed) 
 					As a first step, the user will be able to allocate per order line. A second one will be to build 
 					a solution with the possibility to allocate per aggregation. 
 					
 * Author: 	ABSI - Miguel Coimbra & Bob Ceuppens
 */

	// Period 
	public String periodChosen {set;get;} 
	//Territories
	public String territoryFilter{set; get;}
	//Segments
	public String departmentSegment {set; get;}
	//Account Name Filter	
	public String accountName {set; get;}
	//Used to avoid the trigger execution from this page
	public static Map<String,set<Id>> mAccountAggregationUpsert = null;

	// Period 2
	public String periodChosen2 ; 	
	public String getPeriodChosen2()	{							
		String s = this.periodChosen ;
		String result = DIB_AllocationSharedMethods.getFiscalMonthName(s.substring(0,2)) + ' / ' +  s.substring(3,7) ;  	
		return result ; 			
	}
	
	public String selectedAccountType; 
	
	private Set <String> filteredAccountIds = null;
	
	private Map<String, Set<String>> AccountUserMap = null;
	
	
	public String getSelectedAccountType(){
		return selectedAccountType;
	}
	
	public void setSelectedAccountType(String selectedAccountTypeIn)	{
		this.selectedAccountType = selectedAccountTypeIn;
	}
	
	private DIB_Aggregated_Allocation__c curDIB_Aggregated_Allocation;
	
	public DIB_Aggregated_Allocation__c getDIB_Aggregated_Allocation() {
		return curDIB_Aggregated_Allocation;
	}

	public void setDIB_Aggregated_Allocation(DIB_Aggregated_Allocation__c curDIB_Aggregated_AllocationIn) {
		curDIB_Aggregated_Allocation = curDIB_Aggregated_AllocationIn;
	}
	
	private Account[] SFAccounts = new Account[]{};
	
	// Constants 
	public final String ERRORMSG = 'You must enter a value'; 
	
	private List<DIB_Class_AccountAggregation> myDIB_Class_AccountAggregations;
	
	private List<DIB_Class_AccountAggregation> allDIB_Class_AccountAggregations;
	
	public List<DIB_Class_AccountAggregation> getMyDIB_Class_AccountAggregations() {
		return myDIB_Class_AccountAggregations;
	}

	public void setMyDIB_Class_AccountAggregations(List<DIB_Class_AccountAggregation> myDIB_Class_AccountAggregationsIn) {
		myDIB_Class_AccountAggregations = myDIB_Class_AccountAggregationsIn;
	}
		
	/**
	 *	Section : Period : 
	 */
	 public List<SelectOption> getPeriodItems() {
	 	String paramChosenValue = Apexpages.currentPage().getParameters().get(FinalConstants.PARAM_DEFAULT_CHOSEN_PERIOD);
	 	if(paramChosenValue != null && paramChosenValue != ''){
	 		this.periodChosen = paramChosenValue;
	 	}else{	 	
			this.periodChosen = DIB_AllocationSharedMethods.getCurrentPeriod();
	 	}
		return DIB_AllocationSharedMethods.getPeriodLines() ;  
	}
	
	public List<SelectOption> getAccountTypes() {
		return SharedMethods.getAccountTypes();
	}

	// 1. All Invoice lines that still need to be allocated. (for the FY & FM & Account chosen)
	//private DIB_Invoice_Line__c[] ResultToAllocateItems {set; get;}
	
	// 2. All Invoice lines that are already allocated (for the FY & FM & Account chosen)
	//public DIB_Invoice_Line__c[] ResultAllocatedItems {get;set;} 
	
	//assignedAccountItem is a global var assigned by the VF page depending on which link the user clicks ! 
	private Id assignedAccountItem ; 	
	private Id assignedToAllocateItem ; 
	private Id assignedToEditItem ; 
	
	public Id userId ;
	 
	public DIB_Invoice_Line__c invLine = new DIB_Invoice_Line__c() ;
	
	// Fiscal Months PickList values 
	public String months {set;get;}
	public String years {set;get;}
	public String pumpcgmsChoosen {set;get;}
 	
 	private String country;

	// 0. left side accounts available for that fiscal Month and year 
	public DIB_Class_AccountAggregation[] ResultPUMPAccounts {set; get;}
	public DIB_Class_AccountAggregation[] ResultCGMSAccounts {set; get;}
	public DIB_Class_AccountAggregation[] ResultAllAccounts {set; get;}
	
	private DIB_Class_AccountAggregation[] AggregatedDepartmentLines ;
		
	// get the substring of the period wich will be the first 2 characters 
	public String getSelectedMonth(){
		String pickedPeriod = this.PeriodChosen;    	
		return pickedPeriod.substring(0, 2);	
	}
		
	// get the substring of year which will be the last 4 characters. 
	public String getSelectedYear(){
		String pickedPeriod = this.PeriodChosen;   
		return pickedPeriod.substring(3, 7); 	
	}

	// Accounts 
	public String accountFilter {get; set;} 


	//SubmitResult
	public String submitResult{get;set;}	
	
	public DIB_Invoice_Line__c[] dibILsALL = new DIB_Invoice_Line__c[]{} ;
	
    /*public PageReference displaySoldToAccounts(){

		// Get all the invoice Lines for the chosen month & year : 
		dibILsALL = DIB_AllocationSharedMethods.getInvoiceLinesPerPeriode(getSelectedMonth(), getSelectedYear(), true, false);
		  
		// PUMPS :
		DIB_Class_AccountAggregation[] pumps = new DIB_Class_AccountAggregation[]{};
	 	System.debug('################# ' + 'displaySoldToAccounts dibILsALL: ' +  dibILsALL + ' ################') ;
	
		pumps = DIB_AllocationSharedMethods.getSoldToAccountsList(dibILsALL, FinalConstants.IL_TYPE_PUMPS,accountFilter);
		setResultPUMPAccounts(pumps);
				
		// CGMS 
		DIB_Class_AccountAggregation[] cgms = new DIB_Class_AccountAggregation[]{};					
		cgms = DIB_AllocationSharedMethods.getSoldToAccountsList(dibILsALL, FinalConstants.IL_TYPE_CGMS,accountFilter);					
		setResultCGMSAccounts(cgms) ;				
		
		
    	return null ;     	
    }*/
    
    public void displaySoldToShipToAccounts(){

		// Get all the invoice Lines for the chosen month & year :		
		//dibILsALL = DIB_AllocationSharedMethods.getInvoiceLinesPerPeriode(getSelectedMonth(), getSelectedYear(), true, false);
		dibILsALL = DIB_AllocationSharedMethods.getAllInvoiceLinesPerPeriode(getSelectedMonth(), getSelectedYear(), true, false, this.accountName, AccountUserMap);
		
		// PUMPS : 
		DIB_Class_AccountAggregation [] pumps = new DIB_Class_AccountAggregation[]{};
	 	System.debug('################# ' + 'displaySoldToAccounts dibILsALL size: ' +  dibILsALL.size() + ' ################') ;
	
		pumps = DIB_AllocationSharedMethods.getSoldToShipToAccountsList(dibILsALL, FinalConstants.IL_TYPE_PUMPS, accountFilter);
		ResultPUMPAccounts = pumps;
		System.debug('################# ' + 'pumps size: ' +  pumps.size() + ' ################') ;
				
		// CGMS 
		DIB_Class_AccountAggregation[] cgms = new DIB_Class_AccountAggregation[]{};					
		cgms = DIB_AllocationSharedMethods.getSoldToShipToAccountsList(dibILsALL, FinalConstants.IL_TYPE_CGMS, accountFilter);		
		ResultCGMSAccounts = cgms;				
		
    }    


    
	/**
	 * Constructor & get/set Methods 
	 *
	 */
	/*public controllerAllocationAggregated(ApexPages.Standardcontroller stdController){
		ResultPUMPAccounts = new List<DIB_Class_AccountAggregation>();
	    ResultCGMSAccounts = new List<DIB_Class_AccountAggregation>();
		ResultAllAccounts = new List<DIB_Class_AccountAggregation>();
		getPeriodItems();
	}*/
		 
	/*public controllerAllocationAggregated(ApexPages.Standardcontroller stdController, String periodChosenIn){
		ResultPUMPAccounts = new List<DIB_Class_AccountAggregation>();
	    ResultCGMSAccounts = new List<DIB_Class_AccountAggregation>();
		ResultAllAccounts = new List<DIB_Class_AccountAggregation>();		
		setPeriodChosen(periodChosenIn);
	}*/
	
	public controllerAllocationAggregated(){
		ResultPUMPAccounts = new List<DIB_Class_AccountAggregation>();
	    ResultCGMSAccounts = new List<DIB_Class_AccountAggregation>();
		ResultAllAccounts = new List<DIB_Class_AccountAggregation>();		
		getPeriodItems();
	}	
	   
  	public String getCountry() {       			return country;    		}                    
    public void setCountry(String country) { 	this.country = country; }
  
     /**
     * initiateAggregatedAllocation
     *
     * This method does the initialization of the aggregated allocation
     *
    */
 	
	public PageReference initiateAggregatedAllocation() {

		// Clear submitResult
		submitResult = '';		
	
		//Get filtered accounts for segment and terrotory
		this.AccountUserMap = DIB_AllocationSharedMethods.getFilteredAccountsUsers(this.departmentSegment, this.territoryFilter);
		
		this.filteredAccountIds = this.AccountUserMap.get('Account');
		//System.debug(' ################## ' + 'filteredAccountIds.size: ' + this.filteredAccountIds.size() + ' ################');
	
		// Calculate the aggragated totals for this account
		displaySoldToShipToAccounts();
	
		String currentUserSAPId = DIB_AllocationSharedMethods.getCurrentUserSAPId(System.Userinfo.getUserId());	
	
		allDIB_Class_AccountAggregations = new List<DIB_Class_AccountAggregation>();
		
		String SelectedMonth = getSelectedMonth();
		String SelectedYear = getSelectedYear();
		
		System.debug(' ################## ' + 'initiateAggregatedAllocation Variables: getSelectedMonth: ' + getSelectedMonth() + ' ################');
		System.debug(' ################## ' + 'initiateAggregatedAllocation Variables: getSelectedYear: ' + getSelectedYear() + ' ################');
		
	  	Map<Id,List<DIB_Department__c>> myAccountsDepartments = new Map<Id,List<DIB_Department__c>>();
	  	
	  	/*String strQuery = 'SELECT d.Department_ID__c,d.Department_ID__r.Name, d.Account__r.Id,d.Account__r.Name,d.Account__r.System_Account__c, d.Department_Master_Name__c';
	  	strQuery += ' FROM DIB_Department__c d';
	  	strQuery += ' WHERE d.Account__r.isSales_Force_Account__c = true';
	  	//Segments Filter
	  	if(this.departmentSegment != null && this.departmentSegment != ''){
	  		strQuery += ' AND d.DiB_Segment__c = \'' + this.departmentSegment + '\'';
	  	}
	  	//Account Name Filter
	  	if(this.accountName != null && this.accountName.trim() != ''){
	  		strQuery += ' AND d.Account__r.Name like \'%' + this.accountName.trim() + '%\'';
	  	}
	  	strQuery += ' AND d.Active__c = true ';
	  	strQuery += ' Order BY d.Account__r.Name, d.Account__c, d.Department_ID__c ';
	  	List<DIB_Department__c> mySegmentations = Database.query(strQuery);
	  	
	  	System.debug(' ################## ' + 'initiateAggregatedAllocation_mySegmentations.size: ' + mySegmentations.size() + ' ################');*/
	  	//To maintain the order in the result list
	  	List <Id> orderedAccounts = new List<Id>();
	  	
	  	//Group the same account record together
	  	//Reason - Accounts have different departments
	    //Account Name Filter
	  	String strAccountName = '%';	
	  	if(this.accountName != null && this.accountName.trim() != ''){
	  		strAccountName += this.accountName.trim() + '%';
	  	}
	  	for(List<DIB_Department__c> mySegmentations:
	  		[
	  			SELECT 
	  				d.DiB_Segment__c,d.Department_ID__c,d.Department_ID__r.Name, d.Account__r.Id,d.Account__r.Name,d.Account__r.System_Account__c, d.Department_Master_Name__c
	  	 		FROM 
	  	 			DIB_Department__c d 
	  	 		WHERE 
					d.Account__r.isSales_Force_Account__c = true
					AND d.Active__c = true 
					AND d.Account__r.Name like :strAccountName 
	  			 ORDER BY 
	  			 	d.Account__r.Name, d.Account__c, d.Department_ID__c
	 	]){
		  	for (DIB_Department__c mySegmentation:mySegmentations) {
		  		
		  		//Segments Filter
			  	if(this.departmentSegment != null && this.departmentSegment != '' 
			  				&& mySegmentation.DiB_Segment__c != this.departmentSegment){
			  		continue;
			  	}
		  		
		  		List <DIB_Department__c> lDIBDepartment = myAccountsDepartments.get(mySegmentation.Account__r.Id);
		  		if(lDIBDepartment == null){
		  			orderedAccounts.add(mySegmentation.Account__r.Id);
		  			lDIBDepartment = new List<DIB_Department__c>();
		  		}
		  		lDIBDepartment.add(mySegmentation);
		  		myAccountsDepartments.put(mySegmentation.Account__r.Id, lDIBDepartment);
				//System.debug(' ################## ' + 'DIB_Department__c records: Key: ' + mySegmentation.Account__r.Id + mySegmentation.Department__c + ' Account_Name: '  + mySegmentation.Account__r.Name + ' ################');
		  	}	  	 
	  	 }
	  	
	  	// Get a list of DepartmentNames from the Department picklist on DIB_Department__c
	  	// This list will be used afterwards to automatically generate aggregated lines
	  	/* Existing logic commented
	  	Schema.DescribeFieldResult fieldResult = DIB_Department__c.Department__c.getDescribe();
		List<Schema.PicklistEntry> departmentsTemp = fieldResult.getPicklistValues();
		List<String> departmentNames = new List<String>();
		for (Schema.PicklistEntry departmentTemp:departmentsTemp) {
			departmentNames.add(departmentTemp.getValue());
		}*/
		
	  	
	  	System.debug(' ################## ' + 'initiateAggregatedAllocation_SelectedMonth: ' + SelectedMonth + ' SelectedYear: ' + SelectedYear + ' ################');
	  	
	  	// Select all existing DIB_Aggregated_Allocation__c
	  	DIB_Aggregated_Allocation__c[] myExisting_DIB_Aggregated_Allocations = 
	  		[
	  			SELECT 
	  				d.Id, d.SystemModstamp, d.Sales_Force_Account__c, /*d.Pumps_No__c,*/ 
		  			d.OwnerId, d.Non_Identified_Placement_No__c, d.Name, d.NPNP_No__c, d.LastModifiedDate, d.LastModifiedById, d.IsDeleted, d.Fiscal_Year__c, 
		  			d.Fiscal_Month__c, d.EPNP_No__c, d.CurrencyIsoCode, d.CreatedDate, d.CreatedById, d.Competitor_Upgrade_without_Animas_No__c, d.Animas_No__c, d.CGMS_No__c, 
	  				d.Sales_Force_Account__r.Id, d.DIB_Department_ID__c, d.DIB_Department_ID__r.Department_Master_Name__c , d.Attrition__c 
	  				, Confidence_level__c
	  			FROM
	  				DIB_Aggregated_Allocation__c d
	  			WHERE
	  				d.Fiscal_Month__c =: SelectedMonth and d.Fiscal_Year__c =: SelectedYear
	  			ORDER BY 
	  				d.Sales_Force_Account__r.Name,d.DIB_Department_ID__c
			];
	  	
	  	System.debug(' ################## ' + 'initiateAggregatedAllocation_myExisting_DIB_Aggregated_Allocations.size: ' + myExisting_DIB_Aggregated_Allocations.size() + ' ################');
	  	
	  	// The difficulty is that we need to display lines on the screen that may not be in the database
	   	// The idea is to create a map of DIB_Aggregated_Allocation__c
	  	// Key for the map is a concatenation of the Sales_Force_Account__r and Department
	  	// Afterwards we will very easily be able to check if there is a value for a combination of Sales_Force_Account__r / Department
	  	/* New Comment - Group allocations for the department (All above is exising logic)*/
	  	Map<Id,DIB_Aggregated_Allocation__c> myExisting_DIB_Aggregated_AllocationsMap = new Map<Id,DIB_Aggregated_Allocation__c>();
	  	for (DIB_Aggregated_Allocation__c myExisting_DIB_Aggregated_Allocation : myExisting_DIB_Aggregated_Allocations) {
	  		//uniqueAggregationKey = myExisting_DIB_Aggregated_Allocation.Sales_Force_Account__r.Id + myExisting_DIB_Aggregated_Allocation.Department__c;
	  		myExisting_DIB_Aggregated_AllocationsMap.put(myExisting_DIB_Aggregated_Allocation.DIB_Department_ID__c, myExisting_DIB_Aggregated_Allocation);
	  	}
	  	
	  	System.debug(' ################## ' + 'initiateAggregatedAllocation myExisting_DIB_Aggregated_Allocations.size(): ' + myExisting_DIB_Aggregated_Allocations.size() + ' ################');
	  	
	  	
	  	// Loop over all Accounts and create lines for all Departments
		// Check in the myExisting_DIB_Aggregated_AllocationsMap if there is already a value for this Sales_Force_Account__r / Department combination
		// If so, use that DIB_Aggregated_Allocation__c, if not, create an empty one
		Integer i = 0;
	  	myDIB_Class_AccountAggregations = new List<DIB_Class_AccountAggregation>();
	  	ResultAllAccounts = new List<DIB_Class_AccountAggregation>();
	  	Boolean accountSelected = true;
	  	//Generate the result list
	  	System.debug('patientAccountId : ' + FinalConstants.patientAccountId);
		for (Id currentAccountId:orderedAccounts) {
			// Check to see if the account was already processed.
			// If so, jump out, otherwise we will have duplicates
			//Check if the account is available in the filter
			System.debug('current account: ' + currentAccountId);
			if(filteredAccountIds != null && !filteredAccountIds.contains(currentAccountId) && currentAccountId != FinalConstants.patientAccountId){
				continue;
			}
			//System.debug(' ################## ' + 'Setting up departments for current Account: ' + currentAccount.Name + ' ################');
			List<DIB_Aggregated_Allocation__c> myDIB_Aggregated_Allocations = new List<DIB_Aggregated_Allocation__c>();
			DIB_Class_AccountAggregation myDIB_Class_AccountAggregationTemp = new DIB_Class_AccountAggregation();
			Double cgmsAllocationCount = 0;
			Double otherAllocationCount = 0;
			List <DIB_Department__c> lDIBDepartment = myAccountsDepartments.get(currentAccountId);
			Account currentAccount = null;
			for (DIB_Department__c mySegmentation:lDIBDepartment) {
				currentAccount = mySegmentation.Account__r;
				System.debug(' ################## CurrentAccount: ' + currentAccount +  ' ################');
				DIB_Aggregated_Allocation__c myDIB_Aggregated_Allocation;
		  		myDIB_Aggregated_Allocation = myExisting_DIB_Aggregated_AllocationsMap.get(mySegmentation.Id);
		  		if (myDIB_Aggregated_Allocation == null) {
		  			myDIB_Aggregated_Allocation  = new DIB_Aggregated_Allocation__c();
		  			myDIB_Aggregated_Allocation.Sales_Force_Account__c = currentAccount.Id;
		  			//myDIB_Aggregated_Allocation.Department__c = mySegmentation.Department_ID__r.Name;
		  			myDIB_Aggregated_Allocation.Fiscal_Month__c = getSelectedMonth();
		  			myDIB_Aggregated_Allocation.Fiscal_Year__c = getSelectedYear();
		  			myDIB_Aggregated_Allocation.DIB_Department_ID__c = mySegmentation.Id;
		  			myDIB_Aggregated_Allocation.DIB_Department_ID__r = mySegmentation;
		  			myDIB_Aggregated_Allocation.Confidence_level__c = '100%';
		  		}else{
		  			if(myDIB_Aggregated_Allocation.CGMS_No__c != null){
		  				cgmsAllocationCount += myDIB_Aggregated_Allocation.CGMS_No__c;
		  			}
		  			if(myDIB_Aggregated_Allocation.NPNP_No__c != null){
		  				otherAllocationCount += myDIB_Aggregated_Allocation.NPNP_No__c;
		  			}
		  			if(myDIB_Aggregated_Allocation.EPNP_No__c != null){
		  				otherAllocationCount += myDIB_Aggregated_Allocation.EPNP_No__c;
		  			}
		  			if(myDIB_Aggregated_Allocation.Animas_No__c != null){
		  				otherAllocationCount += myDIB_Aggregated_Allocation.Animas_No__c;
		  			}
		  			if(myDIB_Aggregated_Allocation.Competitor_Upgrade_without_Animas_No__c != null){
		  				otherAllocationCount += myDIB_Aggregated_Allocation.Competitor_Upgrade_without_Animas_No__c;
		  			}
		  			if(myDIB_Aggregated_Allocation.Non_Identified_Placement_No__c != null){
		  				otherAllocationCount += myDIB_Aggregated_Allocation.Non_Identified_Placement_No__c;
		  			}		  					  			
		  		}
		  		myDIB_Aggregated_Allocations.add(myDIB_Aggregated_Allocation);
			}
	 		myDIB_Class_AccountAggregationTemp.setSales_Force_Account(currentAccount);
	  		myDIB_Class_AccountAggregationTemp.setAggregated_Allocation_Per_Department(myDIB_Aggregated_Allocations);
	  		allDIB_Class_AccountAggregations.add(myDIB_Class_AccountAggregationTemp);
	  		System.debug(' ################## ' + 'myDIB_Class_AccountAggregations.size() ' + allDIB_Class_AccountAggregations.size() + ' ################ ');
	  		
	  		DIB_Class_AccountAggregation accountSelectList = new DIB_Class_AccountAggregation(currentAccount.Id, cgmsAllocationCount, otherAllocationCount, accountSelected);
	  		accountSelected = false;
	  		ResultAllAccounts.add(accountSelectList);
	  	}
	  	
	  	//Filter the allocation records by accounts that are selected
	  	filterAllocations();
	  	
	  	// Submit button display or not 
		checkShowHideSubmitButton(); 
		
  		return null;
	}
	/**
	* Show allocations based on account filter
	*/
	public void filterAllocations(){
		Set <Id> sAccountIds = new Set<Id>();
		myDIB_Class_AccountAggregations = new List<DIB_Class_AccountAggregation>();
		for(DIB_Class_AccountAggregation dibClass_AccountAggregation:ResultAllAccounts){
			if(dibClass_AccountAggregation.selected){
				sAccountIds.add(dibClass_AccountAggregation.agg_account.Sold_To__c);
			}
		}	
		for(DIB_Class_AccountAggregation myDIB_Class_AccountAggregationTemp:allDIB_Class_AccountAggregations){
			if(sAccountIds.contains(myDIB_Class_AccountAggregationTemp.getSales_Force_Account().Id)){
				myDIB_Class_AccountAggregations.add(myDIB_Class_AccountAggregationTemp);
			}
		}
	}
	
	 // Submit button : Submit current Month & Year and all accounts for the current user : 
    public void SubmitcurrentPeriod(){
    	if (DIB_Manage_Task_Status.submitSalesEntry(this.getSelectedMonth(), this.getSelectedYear())) {
    		submitResult = FinalConstants.DiB_submitResultSuccess;
    	}
    	else {
    		submitResult = FinalConstants.DiB_submitResultFailure;
    	}
    }
  	
  	// Show or Hide the button Submit Depeding on the selected Fiscal Period 
	private Boolean showSubmitButton; 
	public Boolean getshowSubmitButton(){					return this.showSubmitButton ; 	}
	public void setshowSubmitButton(Boolean b){				this.showSubmitButton = b;	 	}	
	
  	public void checkShowHideSubmitButton(){
		System.debug('######################## showHideSubmitButton call method : ' ) ; 
		System.debug('##############$$$$$$$$ - ' + DIB_Manage_Task_Status.showHideSubmitButton(getSelectedMonth(), getSelectedYear()) + '- #################');
		System.debug('##############$$$$$$$$ - ' + getSelectedMonth() + ' / ' + getSelectedYear() + '- #################');
		setshowSubmitButton(DIB_Manage_Task_Status.showHideSubmitButton(getSelectedMonth(), getSelectedYear())) ; 
	}
  
  
	// Save all current page : 
	public pageReference save()	{
		// Clear submitResult
		submitResult = '';
		set<Id> sUpdateIds = new Set<Id>();
		// Loop over the DIB_Class_AccountAggregation object and put all the DIB_Aggregated_Allocation__c in a List
		List<DIB_Aggregated_Allocation__c> allDIB_Aggregated_Allocations = new List<DIB_Aggregated_Allocation__c>(); 
		Boolean bError = false;
		for (DIB_Class_AccountAggregation myDIB_Class_AccountAggregationTemp:myDIB_Class_AccountAggregations) {
			List<DIB_Aggregated_Allocation__c> myDIB_Aggregated_Allocations = myDIB_Class_AccountAggregationTemp.getAggregated_Allocation_Per_Department();
			for (DIB_Aggregated_Allocation__c myDIB_Aggregated_Allocation:myDIB_Aggregated_Allocations) {
				if (
						myDIB_Aggregated_Allocation.NPNP_No__c <> null 
						|| myDIB_Aggregated_Allocation.EPNP_No__c <> null 
						|| myDIB_Aggregated_Allocation.Animas_No__c <> null 
						|| myDIB_Aggregated_Allocation.Competitor_Upgrade_without_Animas_No__c <> null 
						|| myDIB_Aggregated_Allocation.Non_Identified_Placement_No__c <> null 
						|| myDIB_Aggregated_Allocation.CGMS_No__c <> null 
						|| myDIB_Aggregated_Allocation.Attrition__c <> null
				) {
					if (myDIB_Aggregated_Allocation.NPNP_No__c == null) {
						myDIB_Aggregated_Allocation.NPNP_No__c = 0;
					}else if (myDIB_Aggregated_Allocation.NPNP_No__c < 0) {
						// Error
						myDIB_Aggregated_Allocation.NPNP_No__c.addError('NPNP can not be negative.');
						bError = true;
					}

					if (myDIB_Aggregated_Allocation.EPNP_No__c == null) {
						myDIB_Aggregated_Allocation.EPNP_No__c = 0;
					}else if (myDIB_Aggregated_Allocation.EPNP_No__c < 0) {
						// Error
						myDIB_Aggregated_Allocation.EPNP_No__c.addError('MDT Upgrade can not be negative.');
						bError = true;
					}

					if (myDIB_Aggregated_Allocation.Animas_No__c == null) {
						myDIB_Aggregated_Allocation.Animas_No__c = 0;
					}else if (myDIB_Aggregated_Allocation.Animas_No__c < 0) {
						// Error
						myDIB_Aggregated_Allocation.Animas_No__c.addError('Animas can not be negative.');
						bError = true;
					}
					
					if (myDIB_Aggregated_Allocation.Competitor_Upgrade_without_Animas_No__c == null) {
						myDIB_Aggregated_Allocation.Competitor_Upgrade_without_Animas_No__c = 0;
					}else if (myDIB_Aggregated_Allocation.Competitor_Upgrade_without_Animas_No__c < 0) {
						// Error
						myDIB_Aggregated_Allocation.Competitor_Upgrade_without_Animas_No__c.addError('Competitor Upgrade without Animas can not be negative.');
						bError = true;
					}

					if (myDIB_Aggregated_Allocation.Non_Identified_Placement_No__c == null) {
						myDIB_Aggregated_Allocation.Non_Identified_Placement_No__c = 0;
					}else if (myDIB_Aggregated_Allocation.Non_Identified_Placement_No__c < 0) {
						// Error
						myDIB_Aggregated_Allocation.Non_Identified_Placement_No__c.addError('Not Known can not be negative.');
						bError = true;
					}

					if (myDIB_Aggregated_Allocation.CGMS_No__c == null) {
						myDIB_Aggregated_Allocation.CGMS_No__c = 0;
					}else if (myDIB_Aggregated_Allocation.CGMS_No__c < 0) {
						// Error
						//-BC - 20150223 - CR-7282 - Remove the validation on negative value - START
//						myDIB_Aggregated_Allocation.CGMS_No__c.addError('CGMS can not be negative.');
//						bError = true;
						//-BC - 20150223 - CR-7282 - Remove the validation on negative value - STOP
					}

					if (clsUtil.IsDecimalNull(myDIB_Aggregated_Allocation.Attrition__c, 0) < 0){
						// Error
						myDIB_Aggregated_Allocation.Attrition__c.addError('Attrition can not be negative.');
						bError = true;
					}

					if (myDIB_Aggregated_Allocation.Confidence_leveL__c == null) {
						myDIB_Aggregated_Allocation.Confidence_level__c = '100%';
					}

					System.debug('myDIB_Aggregated_Allocation *** '+myDIB_Aggregated_Allocation);
					sUpdateIds.add(myDIB_Aggregated_Allocation.Id);
					system.debug('Added ids'+sUpdateIds);
					allDIB_Aggregated_Allocations.add(myDIB_Aggregated_Allocation);
				}
			}
		}
		if(!allDIB_Aggregated_Allocations.isEmpty() && !bError){
			//Following map is used to skip the trigger execution
			if(mAccountAggregationUpsert == null){
				mAccountAggregationUpsert = new Map<String,set<Id>>(); 
			}
			mAccountAggregationUpsert.put(Userinfo.getUserId(), sUpdateIds);
			System.debug('########################-delete permission##################');
			system.debug(sUpdateIds);
			try{
				upsert allDIB_Aggregated_Allocations;
//				controllerAllocationAggregated.mAccountAggregationUpsert.remove(Userinfo.getUserId());
			}catch(DmlException oEX){
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, oEX.getMessage()));
//				Apexpages.addMessages(oEX.getMessage());
				bError = true;
			}

			if (!bError){
				try{
					controllerAllocationAggregated.mAccountAggregationUpsert.remove(Userinfo.getUserId());
				}catch(Exception oEX){
					Apexpages.addMessages(oEX);
				}
			}
		}
		return null;
	} 
     
   	/**
 	*	############################# Section : Territory Filter : ##################################################### 
 	*/
	public List<SelectOption> getTerritoryFilterOptions() {
		return DIB_AllocationSharedMethods.getTerritoryFilterOptions();
	}

   	/**
 	*	############################# Section : Segment Filter : ##################################################### 
 	*/
	public List<SelectOption> getSegmentFilterOptions() {
		return DIB_AllocationSharedMethods.getSegmentFilterOptions();
	}
      
}