@isTest
public class Test_bl_SynchronizationService_Utils {
	
	@testSetup
	private static void setupData(){
		
		Synchronization_Service_Setting__c syncSettings = new Synchronization_Service_Setting__c();
		syncSettings.username__c = 'test@medtronic.com';
		syncSettings.password__c = 'password';
		syncSettings.client_id__c = '123456789';
		syncSettings.client_secret__c = 'XXXXX';
		syncSettings.is_sandbox__c = true;
		syncSettings.sync_Enabled__c = true;
		insert syncSettings;			
	}
	
	private static testMethod void testExecuteService(){
		
		Test.startTest();
		
		CalloutMock mockImpl = new CalloutMock();
		
		Test.setMock(HttpCalloutMock.class, mockImpl);
		bl_SynchronizationService_Utils.calloutMock = mockImpl;
		
		bl_SynchronizationService_Utils.SessionInfo sessionInfo = bl_SynchronizationService_Utils.getTargetOrgSession();
		
		bl_SynchronizationService_Utils.executeService('testService', 'GET', 'testBody', ServiceResponse.class, sessionInfo);	
	}
	
	private static testMethod void testGetAttachmentBody(){
		
		Test.startTest();
		
		Test.setMock(HttpCalloutMock.class, new CalloutMock());
		
		bl_SynchronizationService_Utils.SessionInfo sessionInfo = bl_SynchronizationService_Utils.getTargetOrgSession();
				
		bl_SynchronizationService_Utils.GetAttachmentBody('Test_Attachment_Id', sessionInfo);
	}
	
	private static testMethod void testExecuteQuery(){
		
		Test.startTest();
		
		Test.setMock(HttpCalloutMock.class, new CalloutMock());
		
		bl_SynchronizationService_Utils.SessionInfo sessionInfo = bl_SynchronizationService_Utils.getTargetOrgSession();
				
		bl_SynchronizationService_Utils.executeQuery('TestQuery', null, sessionInfo);
	}
	
	private static testMethod void testNotifySyncErrors(){
		
		Group adminsGroup = [Select Id, (Select Id from GroupMembers) from Group where Name = 'Synchronization Service Admins'];
		
		if(adminsGroup.GroupMembers.size() == 0){
			
			GroupMember adminMember = new GroupMember();
			adminMember.UserOrGroupId = UserInfo.getUserId();
			adminMember.GroupId = adminsGroup.Id;
			insert adminMember;
		}
		
		Test.startTest();
		
		bl_SynchronizationService_Utils.notifySyncErrors(new List<String>{'TestError'});
	}
	
	private static testMethod void testHasChanged(){
		
		Case oldCase = new Case();
		oldCase.Subject = 'New value';
		
		Case newCase = new Case();
		newCase.Subject = 'Old value';
		
		List<String> fieldList = new List<String>{'Subject'};
		
		Boolean hasChanged = bl_SynchronizationService_Utils.hasChanged(newCase, oldCase, fieldList);
		
		System.assert(hasChanged == true);
		
		hasChanged = bl_SynchronizationService_Utils.hasChanged(newCase, newCase, fieldList);
		
		System.assert(hasChanged == false);
	}
	
	private static testMethod void testTranslatePayload(){
		
		String payload = 'Test_"payload"_to_transtale';
		
		Map<String, String> mapping = new Map<String, String>{'payload' => 'text'};
			
		String result = bl_SynchronizationService_Utils.translatePayload(payload, mapping);
		
		System.assert( result.contains('payload') == false);		
	}
	
	private static testMethod void testPrepareForJSON(){
		
		Case testCase = new Case();
		
		List<String> fieldList = new List<String>{'Subject'};
		
		bl_SynchronizationService_Utils.prepareForJSON(testCase, fieldList);		
	}
		
	private class ServiceResponse{
		
		public String responseBody {get; set;} 
	}
	
	public class CalloutMock implements HttpCalloutMock {

        public HTTPResponse respond(HTTPRequest req) {
            
            HttpResponse resp = new HttpResponse();
            resp.setStatusCode(200);
            resp.setStatus('Complete');
                       
            if(req.getEndpoint().endsWith('services/oauth2/token')){
            	
            	String respBody =  '{' +
					            '"id" : "1",' +
					            '"issued_at" : "1",' +
					            '"instance_url" : "1",' +
					            '"signature" : "1",' +
					            '"access_token" : "1",' +
					            '"token_type" : "1"' +
				            '}';
				            
				resp.setBody(respBody);
				
            }else if(req.getEndpoint().endsWith('testService')){
            	
            	ServiceResponse sResp = new ServiceResponse();
            	sResp.responseBody = 'test';
            	
            	resp.setBody(JSON.serialize(sResp));
            	            	          	
            }else if(req.getEndpoint().endsWith('/body')){
            	
            	resp.setBodyAsBlob(Blob.valueOf('testBlobBody'));
            	
            }else if(req.getEndpoint().endsWith('q=TestQuery')){
            	
            	String queryResponse = '{' +
					            '"records" : [],' +
					            '"totalSize" : 0,' +
					            '"done" : false,' +
					            '"nextRecordsUrl" : "/queryMore"' +					            
				            '}';
            	
            	resp.setBody(queryResponse);
            	
            }else if(req.getEndpoint().endsWith('/queryMore')){
            	
            	String queryMoreResponse = '{' +
					            '"records" : [],' +
					            '"totalSize" : 0,' +
					            '"done" : true,' +
					            '"nextRecordsUrl" : null' +					            
				            '}';
            	
            	resp.setBody(queryMoreResponse);
            }
            
            
            return resp;
        }
	}	
}