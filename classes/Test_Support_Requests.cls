@isTest
public without sharing class Test_Support_Requests {

  private static testmethod void loginFail(){
        
    createTestUser();
    
    System.runAs(oUser_Guest){
      
      Test.startTest();
      ws_adUserServiceMockImpl mockup = new ws_adUserServiceMockImpl();
      mockup.forceLoginFail = true;    
      Test.setMock(WebServiceMock.class, mockup);
      
      ctrl_Support_Requests controller = new ctrl_Support_Requests();
      
      controller.session.username = 'testuser';
      controller.session.password = 'testpassword';
      PageReference pr = controller.doLogin();
      
      System.assert(pr == null);
      System.assert(controller.userLoggedIn==false);      
    }    
  }  

  private static testmethod void login(){
        
    createTestUser();
    
    System.runAs(oUser_Guest){
      
      Test.startTest();
      Test.setMock(WebServiceMock.class, new ws_adUserServiceMockImpl());
      
      ctrl_Support_Requests controller = new ctrl_Support_Requests();
      
      controller.session.username = 'testuser';
      controller.session.password = 'testpassword';
      controller.doLogin();
      
      System.assert(controller.userLoggedIn==true);    
    }    
  }  

  
  private static testmethod void createUserRequest(){
    
    createTestUser();
  
    System.runAs(oUser){  
  
      generateBUInformation();
  
    }  
    
    Sub_Business_Units__c sbu = [SELECT Id, Business_Unit__c FROM Sub_Business_Units__c];      
      
    System.runAs(oUser){

      User_Business_Unit__c oUserBusinessUnit_SameAs = new User_Business_Unit__c();
        oUserBusinessUnit_SameAs.User__c = oUser_SameAs.Id;
        oUserBusinessUnit_SameAs.Sub_Business_Unit__c = sbu.Id;
        oUserBusinessUnit_SameAs.Primary__c = true;
      insert oUserBusinessUnit_SameAs;
    
    Test.startTest();    
    Test.setMock(WebServiceMock.class, new ws_adUserServiceMockImpl());
    
    List<Id> lstID_CreateUserRequest = new List<Id>();
  
    ctrl_Support_Requests controller = new ctrl_Support_Requests();      
    System.assert(controller.userLoggedIn==true);
      
    controller.helper_sfdcNewUser.requestCreateUser();
    
    controller.helper_sfdcNewUser.initializeInputRequestId();
    controller.helper_sfdcNewUser.loadRegionOptions();
      
    controller.helper_sfdcNewUser.searchAlias = 'testAlias';
    controller.helper_sfdcNewUser.searchByAlias();
    
    List<SelectOption> regionOptions = controller.helper_sfdcNewUser.RegionOptions;
    String europe;      
    for(SelectOption region : regionOptions) if(region.getLabel() == 'Europe') europe = region.getValue();
          
    controller.session.request.region__c = europe;
    controller.session.request.Geographical_Region_vs__c = 'Europe';      
    controller.session.request.Geographical_Country_vs__c = 'BELGIUM';
    
    controller.helper_sfdcNewUser.countrySelectionChanged();
//    controller.session.request.Same_as_user__c = oUser_SameAs.Id;
    controller.session.request.Region__c = europe;
    controller.helper_sfdcNewUser.countrySelectionChanged();
      
    controller.helper_sfdcNewUser.geographicalRegionChanged();
    controller.helper_sfdcNewUser.geographicalSubRegionChanged();
    controller.helper_sfdcNewUser.userBusinessUnitChanged();
    controller.helper_sfdcNewUser.clearCompanyCodeData();
    controller.helper_sfdcNewUser.refreshCompanyCodeData();

    List<SelectOption> lstSO_Region = controller.helper_sfdcNewUser.regionOptions;
    List<SelectOption> lstSO_Country = controller.helper_sfdcNewUser.countryOptions;
    controller.session.request.Same_as_user__c = oUser_SameAs.Id;
    controller.helper_sfdcNewUser.loadSameAsUserInfo();    
    // Set Required fields
    controller.session.request.Geographical_Region_vs__c = 'Europe';
    controller.session.request.Geographical_SubRegion__c = 'NWE';
    controller.session.request.Geographical_Country_vs__c = 'BELGIUM';
    controller.session.request.Company_Code__c = 'EUR';
    controller.session.request.User_Business_Unit_vs__c = 'CRHF';
    controller.session.request.Jobtitle_vs__c = 'Other';
    controller.helper_sfdcNewUser.initializeBUInformation();
      controller.helper_sfdcNewUser.selectedItem = sbu.Id;
    controller.helper_sfdcNewUser.userSBUById.get(sbu.Id).selected=true;
    controller.helper_sfdcNewUser.userSBUById.get(sbu.Id).primary=true;
    controller.helper_sfdcNewUser.selectSBU();
    controller.helper_sfdcNewUser.makePrimary();
    
    controller.helper_sfdcNewUser.selectAll=true;
    controller.helper_sfdcNewUser.selectAllBU();
    controller.helper_sfdcNewUser.selectedItem = sbu.Business_Unit__c;
    controller.helper_sfdcNewUser.selectBU();      
    
    controller.session.request.Comment__c = 'Create User Test Comment';
    
//  controller.helper_sfdcNewUser.createNewUserrequest();

    controller.session.request.Geographical_Region_vs__c = null;
    controller.helper_sfdcNewUser.geographicalRegionChanged();
    controller.helper_sfdcNewUser.createNewUserrequest();

    controller.session.request.Geographical_Region_vs__c = 'Europe';
    controller.helper_sfdcNewUser.createNewUserrequest();
    controller.session.request.Geographical_SubRegion__c = 'NWE';
    controller.helper_sfdcNewUser.createNewUserrequest();
    controller.session.request.Geographical_Country_vs__c = 'Belgium';
    controller.helper_sfdcNewUser.createNewUserrequest();

    controller.session.request.Company_Code__c = 'EUR';
    controller.helper_sfdcNewUser.createNewUserrequest();
    controller.session.request.User_Business_Unit_vs__c = 'CRHF';
    controller.helper_sfdcNewUser.createNewUserrequest();
    controller.session.request.Jobtitle_vs__c = 'Sales Rep';
    controller.session.request.License_Type__c = 'Full Salesforce';
    controller.helper_sfdcNewUser.initializeBUInformation();
    controller.helper_sfdcNewUser.selectedItem = sbu.Id;
    controller.helper_sfdcNewUser.userSBUById.get(sbu.Id).selected=true;
    controller.helper_sfdcNewUser.userSBUById.get(sbu.Id).primary=true;
    controller.helper_sfdcNewUser.selectSBU();
    controller.helper_sfdcNewUser.makePrimary();
    
    controller.helper_sfdcNewUser.selectAll=true;
    controller.helper_sfdcNewUser.selectAllBU();
    controller.helper_sfdcNewUser.selectedItem = sbu.Business_Unit__c;
    controller.helper_sfdcNewUser.selectBU();      
    controller.session.request.User_SAP_Id__c = '001';
    Boolean bResult = controller.helper_sfdcNewUser.bValidateSAPIdMandatory();
    System.assertEquals(bResult, true);
    bResult = controller.helper_sfdcNewUser.bValidateSAPIdLength(false);
    System.assertEquals(bResult, false);
    controller.session.request.User_SAP_Id__c = '0123456789';
    bResult = controller.helper_sfdcNewUser.bValidateSAPIdLength(false);
    System.assertEquals(bResult, true);
    controller.helper_sfdcNewUser.lstSelectedMobileApp = new List<String>{'A', 'B', 'C'};
    controller.helper_sfdcNewUser.createNewUserrequest();

    System.assert(controller.session.request.Id != null);
    lstID_CreateUserRequest.add(controller.session.request.Id);
  }
}
  private static User oUser_Affected;
  private static User oUser_SameAs;
  private static User oUser;
  private static User oUser_Guest;
  
  private static void createTestUser(){

    List<User> lstUser = new List<User>();
    oUser_Affected = clsTestData_User.createUser_SystemAdministrator('tadm000', false);
    oUser_SameAs = clsTestData_User.createUser_SystemAdministrator('tadm001', false);
    oUser = clsTestData_User.createUser_SystemAdministrator('tadm002', false);
    oUser_Guest = clsTestData_User.createUser_Guest('tg000', false);  
    lstUser.add(oUser_Affected);
    lstUser.add(oUser_SameAs);
    lstUser.add(oUser);
    lstUser.add(oUser_Guest);
    insert lstUser;

  }

  private static testmethod void createChangeUserSetupRequestAndupdateRequest(){
                
    createTestUser();

    ctrl_Support_Requests controller = new ctrl_Support_Requests();      
    System.assert(controller.userLoggedIn==true);
      
    Test.startTest();
                  
    controller.helper_sfdcManageUser.requestUserChange();
        
    System.runAs(oUser){            
    
      controller.session.request.Created_User__c = oUser_Affected.Id;
      controller.helper_sfdcManageUser.setupUserChanged();
      
      controller.session.request.Comment__c = 'Change name';
      
      controller.helper_sfdcManageUser.createSimpleUserRequest();
      
      System.assert(controller.session.request.Id != null);
      
      controller.session.comment.Comment__c = 'Test comment to add';
      controller.updateRequest();    
    
    }

  }
  
  private static testmethod void createDeactivateUser(){
                
    createTestUser();

    ctrl_Support_Requests controller = new ctrl_Support_Requests();      
    System.assert(controller.userLoggedIn==true);
      
    Test.startTest();
                  
    controller.helper_sfdcManageUser.requestDeactivation();

    System.runAs(oUser){            
        
      controller.session.request.Created_User__c = oUser_Affected.Id;
      controller.session.request.Date_of_deactivation__c = Datetime.now().addDays(1);    
      controller.session.request.Comment__c = 'Left Company';
      
      controller.helper_sfdcManageUser.createSimpleUserRequest();    
      System.assert(controller.session.request.Id != null);  
      
      controller.helper_sfdcManageUser.cancelDeactivateUser();  

    }  
  
  }
  
  private static testmethod void createReactivateUser(){

    createTestUser();

    ctrl_Support_Requests controller = new ctrl_Support_Requests();      
    System.assert(controller.userLoggedIn==true);
      
    Test.startTest();
                  
    controller.helper_sfdcManageUser.requestReactivateUser();

    System.runAs(oUser){            
        
      controller.session.request.Created_User__c = oUser_Affected.Id;        
      controller.session.request.Comment__c = 'Bring him back to life!';
      
      controller.helper_sfdcManageUser.createSimpleUserRequest();    
      System.assert(controller.session.request.Id != null);  
          
      ApexPages.currentPage().getParameters().put('requestId', controller.session.request.Id);
      controller = new ctrl_Support_Requests();
      
      System.assert(controller.session.request.Id!=null);
      System.assert(controller.session.request.Request_Type__c == 'Re-activate User');
      
      controller.cancel();
      
      System.assert(controller.session.request.Status__c == 'Cancelled');  

    }
  
  }
  
  private static testmethod void createChangeRequestAndCancel(){
                
    ctrl_Support_Requests controller = new ctrl_Support_Requests();      
    System.assert(controller.userLoggedIn==true);
      
    Test.startTest();
                  
    controller.helper_sfdcRequests.requestChangeRequest();
              
    controller.session.request.Description_Long__c = 'Create new Portal';
    controller.session.request.Priority__c = 'Medium';
    controller.session.request.Business_Rationale__c = 'Business Rationale Field';
    controller.session.request.CR_Type__c = 'Change application functionality';
    
    controller.session.inputFile.body = Blob.valueOf('attachment 1 body');
    controller.session.inputFile.Name = 'attachment1';
    
    controller.session.inputFile2.body = Blob.valueOf('attachment 2 body');
    controller.session.inputFile2.Name = 'attachment2';
    
    controller.session.inputFile3.body = Blob.valueOf('attachment 3 body');
    controller.session.inputFile3.Name = 'attachment3';
    
    controller.helper_sfdcRequests.createChangeRequest();    
    System.assert(controller.session.request.Id != null);
    
    ApexPages.currentPage().getParameters().put('requestId', controller.session.request.Id);
    controller = new ctrl_Support_Requests();
    
    System.assert(controller.session.request.Id!=null);
    System.assert(controller.session.request.Request_Type__c == 'Change Request');
    
    controller.cancel();
    
    System.assert(controller.session.request.Status__c == 'Cancelled');    

  }
  
  private static testmethod void createDataLoadRequest(){
    
    createTestUser();

    System.runAs(oUser){            

      Support_application__c dataLoadConfig = new Support_application__c();
      dataLoadConfig.Name = 'Data Load Request';
      dataLoadconfig.Emails_of_approvers__c = 'mock@medtronic.com' ;
      insert dataLoadConfig;

    }
    
    Test.startTest();
    
    System.runAs(oUser){
    
      ctrl_Support_Requests controller = new ctrl_Support_Requests();
      System.assert(controller.userLoggedIn==true);
          
      controller.helper_sfdcRequests.requestDataLoad();
      
      controller.session.request.Description_Long__c = 'Data load description';
      controller.session.request.Data_Type__c = 'Accounts';
              
      controller.session.inputFile.body = Blob.valueOf('attachment 1 body');
      controller.session.inputFile.Name = 'attachment1';
        
      controller.session.inputFile2.body = Blob.valueOf('attachment 2 body');
      controller.session.inputFile2.Name = 'attachment2';
        
      controller.session.inputFile3.body = Blob.valueOf('attachment 3 body');
      controller.session.inputFile3.Name = 'attachment3';
        
      controller.helper_sfdcRequests.createDataLoadRequest();
      
      System.assert(controller.session.readOnlyAttachments.size()==3);  
        
      controller.approve();
      controller.reject();  
        
      controller.session.attachmentId = controller.session.readOnlyAttachments[0].Id;
      controller.session.deleteAttachment();
        
      controller.session.request.Data_Load_Approvers__c = 'system@medtronic.com;';
      controller.helper_sfdcRequests.resubmitDataLoad();  
        
      controller.goBack();
    }
    
  }
  
  private static testmethod void createOtherRequestAndCancel(){
    
    ctrl_Support_Requests controller = new ctrl_Support_Requests();      
    System.assert(controller.userLoggedIn==true);
      
    Test.startTest();
    
    controller.helper_sfdcRequests.requestServiceRequest();
    
    controller.session.request.Description_Long__c = 'Bug description';
    controller.session.request.Service_Request_Type__c = 'Problem';
    
    controller.session.inputFile.body = Blob.valueOf('attachment 1 body');
    controller.session.inputFile.Name = 'attachment1';
          
    controller.helper_sfdcRequests.createServiceRequest();
    
    System.assert(controller.session.readOnlyAttachments.size()==1);
      
    PageReference pr = Page.Support_Other_Request;
    pr.getParameters().put('requestId' , controller.session.request.Id);
    Test.setCurrentPageReference(pr);
    
    controller = new ctrl_Support_Requests();      
    
    controller.cancel();  

  }
  
  private static testmethod void createBugRequestAndupdateRequestWithAttachment(){
    
    ctrl_Support_Requests controller = new ctrl_Support_Requests();      
    System.assert(controller.userLoggedIn==true);
      
    Test.startTest();
                  
    controller.helper_sfdcRequests.requestBug();
    
    controller.session.request.Description_Long__c = 'Bug description';
    
    controller.session.inputFile.body = Blob.valueOf('attachment 1 body');
    controller.session.inputFile.Name = 'attachment1';
    
    controller.session.inputFile2.body = Blob.valueOf('attachment 2 body');
    controller.session.inputFile2.Name = 'attachment2';
    
    controller.session.inputFile3.body = Blob.valueOf('attachment 3 body');
    controller.session.inputFile3.Name = 'attachment3';
    
    controller.helper_sfdcRequests.createBugRequest();
    
    System.assert(controller.session.readOnlyAttachments.size()==3);
    
    controller.session.comment = new User_Request_Comment__c();
    controller.session.comment.Comment__c = 'Bug Request Comment';
    controller.session.inputFile.body = Blob.valueOf('extra attachment body');
    controller.session.inputFile.Name = 'extra attachment';
    
    controller.updateRequest();    
    
    System.assert(controller.session.readOnlyAttachments.size()==4);
    System.assert(controller.session.comments.size()==1);
  }
    
  private static testmethod void navigateThroughRequests(){
    
    User currentUser = [SELECT Id, Email FROM User WHERE Id = :UserInfo.getUserId()];

    generateBUInformation();
    
    Company__c oCompany = [Select Id, Name from Company__c WHERE Name ='Europe' LIMIT 1];
    Create_User_Request__c userRequest = new Create_User_Request__c();
      userRequest.region__c = oCompany.Id;
      userRequest.Alias__c = 'alias';
      userRequest.Firstname__c = 'test';
      userRequest.Lastname__c = 'user';
      userRequest.Email__c = 'mock@medtronic.com';
      userRequest.Manager_Email__c = currentUser.Email;
      userRequest.Cost_Center__c = '123456';
      userRequest.Peoplesoft_ID__c = '123456';
      userRequest.Profile__c = 'Admin';
      userRequest.Role__c = 'System Administrator';  
      userRequest.Jobtitle_vs__c = 'Other';
      userRequest.Status__c = 'Submitted for Approval';
      userRequest.Request_Type__c = 'Create User';
      userRequest.Application__c = 'Salesforce.com';  
      userRequest.Requestor_Email__c = currentUser.Email;     
      userRequest.Company_Code__c = 'EUR';
      userRequest.User_Business_Unit_vs__c = 'CRHF';
      userRequest.Jobtitle_vs__c = 'Other';
      userRequest.Geographical_Region_vs__c = oCompany.Name;
      userRequest.Geographical_SubRegion__c = 'NWE';
      userRequest.Geographical_Country_vs__c = 'Belgium';
    insert userRequest;

    System.assert(userRequest.Id != null);
    
    Sub_Business_Units__c sbu = [Select Id from Sub_Business_Units__c];  
    
    User_Request_BU__c userBU =  new User_Request_BU__c();
    userBU.Sub_Business_Unit__c = sbu.Id;
    userBU.Primary__c = true;
    userBU.User_Request__c = userRequest.Id;
    insert userBU;
    
    Create_User_Request__c dataLoadRequest = new Create_User_Request__c();
    dataLoadRequest.Description_Long__c = 'Data load description';
    dataLoadRequest.Data_Type__c = 'Accounts';
    dataLoadRequest.Request_Type__c = 'Data Load';
    dataLoadRequest.Status__c = 'New';
    dataLoadRequest.Application__c = 'Salesforce.com';
    dataLoadRequest.Requestor_Name__c = UserInfo.getFirstName()+' '+UserInfo.getLastName();
    dataLoadRequest.Requestor_Email__c = currentUser.Email;  
    insert dataLoadRequest;
          
    Test.startTest();
    Test.setMock(WebServiceMock.class, new ws_adUserServiceMockImpl());
      
    ctrl_Support_Requests controller = new ctrl_Support_Requests();      
    System.assert(controller.userLoggedIn==true);
        
    controller.seeAllRequests();
    
    System.assert(controller.requests.size()==2);
    
    controller.selectedItem = userRequest.Id;
    controller.viewRequest();
    
    controller.session.inputRequestId = dataLoadRequest.Id;
    controller.helper_sfdcRequests.initializeInputRequestId();  
    
    controller.approve();
    controller.reject();
    
    controller.goBack();
    
    controller.helper_sfdcManageUser.requestReactivateUser();
    controller.helper_sfdcManageUser.requestResetPassword();
    controller.helper_sfdcManageUser.requestMobileApp();        
  }
  
  private static testmethod void testTableSorting(){
    
    ctrl_Support_Requests.Request req1 = new ctrl_Support_Requests.Request(DateTime.now().addDays(-2), null);
    req1.name = 'UR-0001';
    req1.reqType = 'Create User';
    req1.status = 'New';
    req1.requestor = 'Jan Kowalski';
    req1.getCreatedDate();
    
    ctrl_Support_Requests.Request req2 = new ctrl_Support_Requests.Request(DateTime.now().addDays(-1), null);
    req2.name = '0000123';
    req2.reqType = 'Data Load Request';
    req2.status = 'Approved';
    req2.requestor = 'Maria Kowalska';
    
    ctrl_Support_Requests.Request req3 = new ctrl_Support_Requests.Request(DateTime.now(), null);
    req3.name = 'UR-0002';
    req3.reqType = 'Re-Activate User';
    req3.status = 'Pending Approval';
    req3.requestor = 'Piotr Kowalski';
    
    ctrl_Support_Requests controller = new ctrl_Support_Requests();
    
    controller.requests = new List<ctrl_Support_Requests.Request>{req1, req2, req3};
    controller.pendingRequests = new List<ctrl_Support_Requests.Request>{req1, req2, req3};
        
    controller.selectedOpenReqColumn = 'CreatedDate';    
    controller.sortOpenRequests();
    
    controller.selectedOpenReqColumn = 'RequestId';    
    controller.sortOpenRequests();
    
    controller.selectedOpenReqColumn = 'RequestType';
    controller.sortOpenRequests();
    
    controller.selectedOpenReqColumn = 'Status';
    controller.sortOpenRequests();
    
    controller.selectedOpenReqColumn = 'Requestor';
    controller.sortOpenRequests();    
    
    controller.selectedPendingReqColumn = 'CreatedDate';    
    controller.sortPendingRequests();
  }

  public static void generateBUInformation(){
    
    Company__c oCompany = new Company__c();    
          oCompany.Name = 'Europe';
          oCompany.CurrencyIsoCode = 'EUR';
          oCompany.Current_day_in_Q1__c = 56;
          oCompany.Current_day_in_Q2__c = 34; 
          oCompany.Current_day_in_Q3__c = 5; 
          oCompany.Current_day_in_Q4__c =  0;   
          oCompany.Current_day_in_year__c = 200;
          oCompany.Days_in_Q1__c = 56;  
          oCompany.Days_in_Q2__c = 34;
          oCompany.Days_in_Q3__c = 13;
          oCompany.Days_in_Q4__c = 22;
          oCompany.Days_in_year__c = 250;
          oCompany.Company_Code_Text__c = 'EUR';
    insert oCompany;
    
    Business_Unit_Group__c oBusinessUnitGroup =  new Business_Unit_Group__c();
          oBusinessUnitGroup.Master_Data__c = oCompany.id;
          oBusinessUnitGroup.Name = 'CVG';
        insert oBusinessUnitGroup;

    Business_Unit__c oBusinessUnit =  new Business_Unit__c();
          oBusinessUnit.Company__c = oCompany.id;
      oBusinessUnit.Business_Unit_Group__c = oBusinessUnitGroup.Id;
          oBusinessUnit.Name = 'CRHF';
          oBusinessUnit.Account_Plan_Activities__c = true;        
        insert oBusinessUnit;
        
        Sub_Business_Units__c oSubBusinessUnit = new Sub_Business_Units__c();
            oSubBusinessUnit.Name = 'Implantables & Diagnostic';
            oSubBusinessUnit.Business_Unit__c = oBusinessUnit.id;
          oSubBusinessUnit.Account_Plan_Default__c = 'Revenue';
          oSubBusinessUnit.Account_Flag__c = 'AF_Solutions__c';
    insert oSubBusinessUnit;
    
    DIB_Country__c oDIBCountry = new DIB_Country__c();
      oDIBCountry.Company__c = oCompany.Id;
      oDIBCountry.Name = 'Belgium';
      oDIBCountry.Country_ISO_Code__c = 'BE';
      oDIBCountry.CurrencyIsoCode = 'EUR';
    insert oDIBCountry;
  }
}