public without sharing class ctrl_MITG_Product_Console {
    
    private Product_Country_Availability__c productHelper;
    public Product2 contextProd {get; set;}
    
    public String kitableValue {get; set;}
    public List<SelectOption> kitableValueList {get; set;}    
    public String rejectedReason {get; set;}
    
    private List<DIB_Country__c> countryList {get; set;}
    public Map<String, List<Product_Country_availability__c>> productCountryMap {get; set;}
    
    public Map<String, Product_Country_availability__c> bulkStatus {get; set;}    
    public String bulkRegion {get; set;}
    
    public List<Product_Procedure_Availability__c> procedures {get; set;}
    
    public ctrl_MITG_Product_Console(ApexPages.StandardController sc){
        
        productHelper = (Product_Country_Availability__c) sc.getRecord();
        
        kitableValueList = new List<SelectOption>();
        kitableValueList.add(new SelectOption('Kitable', 'Kitable'));
        kitableValueList.add(new SelectOption('Approval Pending', 'Approval Pending'));
        kitableValueList.add(new SelectOption('Not Approved', 'Not Approved'));
        kitableValueList.add(new SelectOption('Request Rejected', 'Request Rejected'));
            
        countryList = [Select Id, Name, Region_vs__c from DIB_Country__c where Box_Builder_enabled__c = true ORDER BY Name];  
        
        String productId = ApexPages.currentPage().getParameters().get('productId');
        if(productId != null && productId != ''){
            
            productHelper.Product__c = productId;
            productSelected();
        }                               
    }
    
    public List<String> getRegions(){
        
        if(productCountryMap == null) return null;
        
        List<String> regions = new List<String>(productCountryMap.keySet());
        regions.sort();
        
        return regions;
    }
    
    public void productSelected(){
        
        Id prodId = productHelper.Product__c;
        
        if(prodId != null){
            
            contextProd = [Select Id, Name, Description, CFN_Code_Text__c, KitableIndex__c, Rejected_Reason__c, Product_Image__c, Product_Group__r.Name, Family, Business_Unit_ID__r.Name, Sub_Business_Unit__c, 
            					(Select Id, Country__c, Status__c, Rejected_Reason__c from Product_Country_Availabilities__r),
            					(Select Id, Procedure__c, Is_Available__c from Product_Procedure_Availabilities__r) 
            					from Product2 where Id = :prodId];
            					
            kitableValue = contextProd.KitableIndex__c;
            rejectedReason = contextProd.Rejected_Reason__c;
            
            Map<Id, Product_Country_Availability__c> existingAvailabilities = new Map<Id, Product_Country_Availability__c>();
            
            for(Product_Country_Availability__c prodAvail : contextProd.Product_Country_Availabilities__r){
                
                existingAvailabilities.put(prodAvail.Country__c, prodAvail);
            }
            
            productCountryMap = new Map<String, List<Product_Country_availability__c>>();
            bulkStatus = new Map<String, Product_Country_availability__c>();
            
            for(DIB_Country__c country : countryList){
                
                List<Product_Country_Availability__c> regionCountries = productCountryMap.get(country.Region_vs__c);
            
                if(regionCountries == null){
                    
                    regionCountries = new List<Product_Country_Availability__c>();
                    productCountryMap.put(country.Region_vs__c, regionCountries);
                    
                    bulkStatus.put(country.Region_vs__c, new Product_Country_Availability__c());
                }
                
                Product_Country_Availability__c prodAvail = new Product_Country_Availability__c();
                prodAvail.Product__c = contextProd.Id;
                prodAvail.Country__c = country.Id;
                prodAvail.Country__r = country;
                prodAvail.Unique_Key__c = contextProd.Id + ':' + country.Id;
                
                Product_Country_Availability__c existingAvail = existingAvailabilities.get(country.Id);
                if(existingAvail != null){
                	
                	prodAvail.Status__c = existingAvail.Status__c;
                	prodAvail.Rejected_Reason__c = existingAvail.Rejected_Reason__c;
                }else prodAvail.Status__c = 'Not Approved';
                
                regionCountries.add(prodAvail);
            }
            
            Map<Id, Product_Procedure_Availability__c> existingProcedures = new Map<Id, Product_Procedure_Availability__c>();            
            for(Product_Procedure_Availability__c prodProcedure : contextProd.Product_Procedure_Availabilities__r){
                
                existingProcedures.put(prodProcedure.Procedure__c, prodProcedure);
            }
            
            procedures = new List<Product_Procedure_Availability__c>();
            
            for(MITG_App_Procedure__c procedure : [Select Id, Name from MITG_App_Procedure__c where App__c = 'Procedural Solutions Builder' ORDER BY Name]){
            	
            	Product_Procedure_Availability__c prodProcedure;
            	
            	if(existingProcedures.get(procedure.Id) != null){
            		
            		prodProcedure = existingProcedures.get(procedure.Id);
            		
            	}else{
            		
            		prodProcedure = new Product_Procedure_Availability__c();
            		prodProcedure.Product__c = prodId;
            		prodProcedure.Procedure__c = procedure.Id;            		
            	}
            	
            	prodProcedure.Procedure__r = procedure;
            	procedures.add(prodProcedure);
            }
            
        }else{
            
            contextProd = null;
            kitableValue = null;
            productCountryMap = null;
        }
    }
    
    public void bulkStatusChanged(){
        
        Product_Country_Availability__c bulkAvail = bulkStatus.get(bulkRegion);
        
        for(Product_Country_Availability__c prodAvail : productCountryMap.get(bulkRegion)){
            
            prodAvail.Status__c = bulkAvail.Status__c;
        }
        
        bulkAvail.Status__c = null;
    }
    
    public void selectAllProcedures(){
    	
    	for(Product_Procedure_Availability__c procedure : procedures){
            
            procedure.Is_Available__c = true;
        }
    }
    
    public void saveConfig(){
        
        SavePoint sp = Database.setSavePoint();
        
        try{
            
            contextProd.KitableIndex__c = kitableValue;
            contextProd.Rejected_Reason__c = rejectedReason;
            
            if(kitableValue != 'Request Rejected') contextProd.Rejected_Reason__c = null;
            update contextProd;
            
            List<Product_Procedure_Availability__c> allProductProcedures = new List<Product_Procedure_Availability__c>();
            List<Product_Country_Availability__c> allProductCountries = new List<Product_Country_Availability__c>();
            
            if(kitableValue != 'Not Approved' && kitableValue != 'Request Rejected'){
                
                allProductProcedures.addAll(procedures);
                
                for(List<Product_Country_Availability__c> productCountries : productCountryMap.values()){
                    allProductCountries.addAll(productCountries);   
                }
            }
                        
            upsert allProductProcedures;            
            upsert allProductCountries Unique_Key__c;
            
            delete [Select Id from Product_Procedure_Availability__c where Product__c = :contextProd.Id AND Id NOT IN :allProductProcedures];            
            delete [Select Id from Product_Country_Availability__c where Product__c = :contextProd.Id AND Id NOT IN :allProductCountries];
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Changes saved correctly'));
            
            productSelected();
                                    
        }catch(Exception e){
            
            ApexPages.addMessages(e);
            Database.rollback(sp);
        }       
    }
}