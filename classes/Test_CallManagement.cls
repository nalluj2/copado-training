/*
 *      Description : Test methods for Call Management
 *
 *      Author = Rudy De Coninck
 */
@IsTest(SeeAllData=true)
public class Test_CallManagement {
	
	
	public static testmethod void testMobileSyncCreate(){
		Mobile_Sync_Request__c req = new Mobile_Sync_Request__c();
		req.Body__c = 'test';
		insert req; 
		
		Profile p = [select id from profile where name='System Administrator'];
		
        User u= new User(alias = 'standt1', email='mobilesynctest@medtronic.com',             
                        emailencodingkey='UTF-8', lastname='mobile sync_Testing',             
                        languagelocalekey='en_US',             
                        localesidkey='en_US', profileid = p.Id,            
                        timezonesidkey='America/Los_Angeles',             
                        CountryOR__c='Country1',          
                        Country= 'Country1',            
                        username='mobilesynctest@medtronic.com',
                        Territory_UID__c='123456',
                        Alias_unique__c='standarduser_Alias_unique__c',Company_Code_Text__c='EUR');        
                
            
		
		insert u;
		
		String requestType = 'Call Record';
		String body = 'test';
		
		bl_Mobilesync.storeRequest(u.Id, requestType, body,'test repsponse', false);
		
	}
	
	public static testmethod void testValidationException(){
		ValidationException ve = new ValidationException();
		ve.setMessage('Time spent cannot be less than 0.');
		
		
		
	}
	
	public static testmethod void testCreateCallRecord(){
		
		Call_Records__c callRecord = new Call_Records__c();
		callRecord.Mobile_ID__c = GUIDUtil.NewGuid();
		insert callRecord;
		
		Contact_Visit_Report__c cvr = new Contact_Visit_Report__c();
		cvr.Call_Records__c =callRecord.id;
		cvr.Mobile_ID__c = GuidUtil.NewGuid();
		insert cvr;
		
		List<Contact_Visit_Report__c> attendedContacts = new List<Contact_Visit_Report__c>();
		attendedContacts.add(cvr);
		
		bl_CallManagement.saveCallContacts(callRecord, attendedContacts);
		
		List<Call_Topic_Business_Unit__c> callTopicBusinessUnits = new List<Call_Topic_Business_Unit__c>();
		
		
		bl_CallManagement.saveCallTopicBusinessUnits(callRecord, callTopicBusinessUnits);
		
		List<Call_Topic_Products__c> callTopicProducts = new List<Call_Topic_Products__c>();
		
		bl_CallManagement.saveCallTopicProducts(callRecord, callTopicProducts);
		
		List<Call_Topics__c> callTopics = new List<Call_Topics__c>();
		
		bl_CallManagement.saveCallTopics(callRecord, callTopics);
		
		List<Call_Topic_Subject__c> callTopicSubjects = new List<Call_Topic_Subject__c>();
		
		bl_CallManagement.saveCallTopicSubjects(callRecord, callTopicSubjects);
		
		
	}

}