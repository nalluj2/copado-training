@isTest
private class Test_ControllerManageMembers {

    static testMethod void UnitTest_ControllerManageMembers () {
    // DIB_Country__c DIBcntry = [select CurrencyIsoCode from DIB_Country__c where name = 'Belgium' limit 1];
        
    //Insert Accounts
    List<Account> accounts = new List<Account>{} ; 
    for (Integer i = 0 ; i < 5 ; i++)
    {
        Account a = new Account() ;
        a.Name = 'Test Account Name ' + i  ;  
        a.RecordTypeId = FinalConstants.sapAccountRecordTypeId ;
        a.Account_Country_vs__c='BELGIUM';
        // a.CurrencyIsoCode =DIBcntry.CurrencyIsoCode ;
        accounts.add(a); 
    } 
    insert accounts;
         
        List<Contact> Contacts = new List<Contact>{} ; 
        for(Integer i = 0 ; i < accounts.size() ; i++){
            Contact cont1 = new Contact();
            cont1.LastName = 'Test_ContactActionPlan '+i ;  
            cont1.FirstName = 'TEST';  
            cont1.AccountId = accounts[0].Id ;
            cont1.Contact_Active__c = false  ;
            cont1.Contact_Inactive_Reason__c = 'Inactive Reason is required to all inactve Contacts';
            cont1.MailingCountry = 'BELGIUM';
            cont1.Contact_Department__c = 'Diabetes Adult'; 
            cont1.Contact_Primary_Specialty__c = 'ENT';
            cont1.Affiliation_To_Account__c = 'Employee';
            cont1.Primary_Job_Title_vs__c = 'Manager';
            cont1.Contact_Gender__c = 'Male';                 
            Contacts.add(cont1);
         }
         insert Contacts;
         
         //Insert Company
         Company__c cmpny = new Company__c();
         cmpny.name='TestMedtronic';
         cmpny.CurrencyIsoCode = 'EUR';
         cmpny.Current_day_in_Q1__c=56;
         cmpny.Current_day_in_Q2__c=34; 
         cmpny.Current_day_in_Q3__c=5; 
         cmpny.Current_day_in_Q4__c= 0;   
         cmpny.Current_day_in_year__c =200;
         cmpny.Days_in_Q1__c=56;  
         cmpny.Days_in_Q2__c=34;
         cmpny.Days_in_Q3__c=13;
         cmpny.Days_in_Q4__c=22;
         cmpny.Days_in_year__c =250;
         cmpny.Company_Code_Text__c = 'T22';
         insert cmpny;
    
         //Insert Business Unit
         Business_Unit__c bu =  new Business_Unit__c();
         bu.Company__c =cmpny.id;
         bu.name='BUMedtronic';
         bu.Contact_Flag__c='Cranial_and_Spinal__c';
         insert bu;
         
         Affiliation__c aff=new Affiliation__c();
         aff.Affiliation_Active__c=true;
         aff.Business_Unit__c=bu.id;
         aff.Department__c='Test';
         aff.recordtypeID=FinalConstants.recordTypeIdC2A;
         aff.Affiliation_From_Account__c=accounts[1].id;
         aff.Affiliation_From_Contact__c=Contacts[0].id;
         insert aff; 
         

            campaign cmpgn=new campaign();
            cmpgn.name='Campaign1';
            cmpgn.Status='Accounts Approved';
            cmpgn.Business_Unit__c=bu.id;
            cmpgn.isactive=true;
            cmpgn.Business_Unit_Picklist__c = 'Diabetes';
            cmpgn.recordtypeid=clsUtil.getRecordTypeByDevName('Campaign', 'Diabetes_Campaign').Id;
            insert cmpgn;  
            
            Target_Account__c tacc=new Target_Account__c();
            tacc.Campaign__c=cmpgn.id;
            tacc.Status__c='Approved';
            insert tacc; 
            
                       Profile p = [select id from profile where name='System Administrator'];    
           user u = new User(alias = 'standt3', email='standarduser@testorg.com',             
                        emailencodingkey='UTF-8', lastname='TerritoryManagement_Testing',             
                        languagelocalekey='en_US',             
                        localesidkey='en_US', profileid = p.Id,            
                        timezonesidkey='America/Los_Angeles',             
                        CountryOR__c='Australia',          
                        Country_vs__c= 'NETHERLANDS',
                        Region_vs__c = 'Europe',
                        Sub_Region__c = 'NWE',           
                        username='NoBUUser123@testorg.com',
                        Alias_unique__c='standarduser_Alias_unique__c',
                        Company_Code_Text__c='EUR',                        
                        User_Business_Unit_vs__c = 'Cranial Spinal',                        
                        Job_Title_vs__c='Sales Rep');
                insert u;        
                            
        Account_Team_Member__c atm=new Account_Team_Member__c();
        atm.account__c=accounts[0].id;
        atm.user__c=u.id;
        //atm.Job_Title__c='sales rep';
        insert atm; 
        
        Campaignmember cmbr=new Campaignmember();
        cmbr.campaignID=cmpgn.id;
        cmbr.contactID=contacts[0].id;
        
        cmbr.Feedback_Caller__c=u.id;
        cmbr.Invitation_Caller__c=u.id;
        cmbr.NPS_Value__c='Detractor';
        cmbr.CC_Status__c='Feedback Call Required';
        cmbr.Feedback_task_generated__c=false;
        insert cmbr;
        
             
      
        ApexPages.currentPage().getParameters().put('aid',cmpgn.id);
        ApexPages.StandardController ctrl = new ApexPages.StandardController(cmpgn);    
        ControllerManageMembers controller = new ControllerManageMembers(ctrl);
       
        //controller.getlstSelectedContact();
        controller.getlstSelectedCampaignMember();
        //controller.refresh();
        //controller.setConContact=[select id,name from contact limit 100];
        controller.FilterAccount();
        controller.getlstSelectedCampaignMember();
        controller.getcolumnNamesNew();
        controller.getOperatorNames() ;
           
           controller.FieldName1='Name';
        controller.OutName1 ='e'; 
        controller.FieldName2='Name';
        controller.OutName2 ='n'; 
        controller.FieldName3='Name';
        controller.OutName3 ='s'; 
        controller.FieldName4='Name';
        controller.OutName4='c'; 
        controller.FieldName5='Name';
        controller.OutName5='l';
        
        controller.filterContact();
        controller.FieldName1='NPS_Value__c';
        controller.OutName1 ='e'; 
        controller.FieldName2='NPS_Value__c';
        controller.OutName2 ='n'; 
        controller.FieldName3='NPS_Value__c';
        controller.OutName3 ='s'; 
        controller.FieldName4='NPS_Value__c';
        controller.OutName4='c'; 
        controller.FieldName5='NPS_Value__c';
        controller.OutName5='l'; 
          
      
        //controller.getOprater();
        controller.getcolumnNamesNewCmpgn();
        controller.filterContactExisting();
        //controller.getOprater1();
       ////controller.AddContactswithStatusPendingApproval();
        controller.getPageNumber();
        controller.getPageSize();
       // controller.BindData(1);
        controller.nextBtnClick();
        controller.previousBtnClick() ;
        controller.getTotalPageNumber();
        controller.getPreviousButtonEnabled();
        controller.getNextButtonDisabled();
        controller.Remove();
        controller.Approved();
        controller.UserWindow();
        controller.CancelWindow();
        controller.OkWindow();
        controller.clearfilters();
    }
}