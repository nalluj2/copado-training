@isTest
private class Test_User_Actions {

    private static testmethod void AssetHistory(){
        
        Account acc = createTestAccount();
        
        Product2 prd = new Product2();
        prd.Name = 'Unit test product';
        insert prd;
                
        Asset asset = new Asset();
        asset.Name = 'Unit Test Asset';
        asset.AccountId = acc.Id;
        asset.Serial_Nr__c = '123456789';
        asset.Product2Id = prd.Id;
        insert asset;
        
        Asset_History__c aHistory = new Asset_History__c();
        aHistory.Asset__c = asset.Id;
        insert aHistory;            
    }

    private static testmethod void WorkOrder(){
        
        Account acc = createTestAccount();
        
        Case testCase = new Case();
        testCase.AccountId = acc.Id;
        insert testCase;
        
        Workorder__c workOrder = new Workorder__c();
        workOrder.Case__c = testCase.Id;
        workOrder.Account__c = acc.Id;
        insert workOrder;   
    }
    
    private static Account createTestAccount(){
        
        Account acc = new Account();
        acc.Name = 'Unit Test Account';
        
        insert acc;
        
        return acc;
        
    }
}