@isTest
private class Test_ctrl_ConvertLead {

	private static Lead leadToAdd;
	private static Account accountToAdd;

    static testMethod void displayPopupMethodShouldShowThePopup() {       
    	addLeadAndAccount();
        ApexPages.currentPage().getParameters().put('id', leadToAdd.Id);   
        
        ctrl_LeadConvert controller = new ctrl_LeadConvert();    
        
        controller.showPopup();
        
        System.assert(true, controller.displayPopup);
    }
    
    static testMethod void accountClickedShouldCloseThePopup(){
    	addLeadAndAccount();
    	
    	PageReference pageRef = Page.CustomLeadConvert;
    	pageRef.getParameters().put('id', leadToAdd.Id);
        Test.setCurrentPage(pageRef);
    	       
        ctrl_LeadConvert controller = new ctrl_LeadConvert();    
        controller.selectedId = accountToadd.Id;
        controller.accountSelectList = new List<SelectOption>();
        controller.accountSelectList.add(new SelectOption('',''));
        
        controller.accountClicked();
        
        System.assert(true, controller.displayPopup);
    }    
    
    static testMethod void searchAccountShouldAdd1Record() {       
    	addLeadAndPersonAccount();
        ApexPages.currentPage().getParameters().put('id', leadToAdd.Id);   
        
        ctrl_LeadConvert controller = new ctrl_LeadConvert();    
        controller.accountSuggestions = new List<Account>();
        controller.searchValue='tes';
        
        controller.searchAccount();
        
        System.assertEquals(1, controller.accountSuggestions.size());

		controller.getAccountOverviewUrl();
		controller.convertLead();
		controller.hidePopup();
		controller.goBackToLead();
    }
    
    private static void addLeadAndAccount(){
    	leadToAdd = new Lead();
        leadToAdd.Email = 'test@test.com';
        leadToAdd.PostalCode = '2590';
        leadToAdd.Lead_Zip_Postal_Code__c = '2590';
        leadToAdd.LastName = 'test';
        leadToAdd.Company = 'testCompany';
        insert(leadToAdd);
        
        accountToAdd = new Account();
        accountToAdd.RecordTypeId = RecordTypeMedtronic.Account('NON_SAP_Account').Id;
        accountToAdd.Name='test';
        accountToAdd.Account_Email__c = 'test@test.com';
        insert(accountToAdd);       
    }
    
    private static void addLeadAndPersonAccount(){
    	System.debug('komt in add methode');
    	
    	leadToAdd = new Lead();
        leadToAdd.Email = 'test@test.com';
        leadToAdd.PostalCode = '2590';
        leadToAdd.Lead_Zip_Postal_Code__c = '2590';
        leadToAdd.LastName = 'test';
        leadToAdd.Company = 'testCompany';
        insert(leadToAdd);
        
        RecordType recordType = [ select Id, Name, DeveloperName from RecordType where SObjectType = 'Account' and IsPersonType = true limit 1 ];
        system.debug( recordType );

        Account newAccount = new Account( Salutation='Mr.', FirstName='test', LastName='Schmoe', RecordTypeId = recordType.id );
        insert newAccount;
        
        System.debug('personaccount added '+accountToAdd);
    }
}