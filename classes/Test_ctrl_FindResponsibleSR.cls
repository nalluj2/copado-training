@isTest
private class Test_ctrl_FindResponsibleSR {
	
	private static testmethod void testFindSalesRep(){
		
		List<User> testUsers = [Select Id from User where isActive = true AND Profile.Name = 'EUR Field Force CVG' AND Id != :UserInfo.getUserId() ORDER BY Name LIMIT 4];
		
		clsTestData_MasterData.createSubBusinessUnit();
		
		Sub_Business_Units__c  AFSolutions = [Select Id, Business_Unit__c, Business_Unit__r.Business_Unit_Group__c, Business_Unit__r.Company__c from Sub_Business_Units__c where Name = 'AF Solutions'];
		Sub_Business_Units__c  MCS = [Select Id, Business_Unit__r.Company__c from Sub_Business_Units__c where Name = 'MCS'];
		Sub_Business_Units__c  aortic = [Select Id, Business_Unit__r.Company__c from Sub_Business_Units__c where Name = 'Aortic'];
		Sub_Business_Units__c  neurosurgery = [Select Id, Business_Unit__r.Company__c from Sub_Business_Units__c where Name = 'Neurosurgery'];
		
		Therapy_Group__c phasedRF = new Therapy_Group__c();
        phasedRF.Name = 'Phased RF';
        phasedRF.Sub_Business_Unit__c = AFSolutions.Id;
        phasedRF.Company__c = AFSolutions.Business_Unit__r.Company__c;
        
        Therapy_Group__c MCS_TG = new Therapy_Group__c();
        MCS_TG.Name = 'Mechanical Circulatory Support';
        MCS_TG.Sub_Business_Unit__c = MCS.Id;
        MCS_TG.Company__c = MCS.Business_Unit__r.Company__c;
        
        Therapy_Group__c aortic_TG = new Therapy_Group__c();
        aortic_TG.Name = 'Aortic';
        aortic_TG.Sub_Business_Unit__c = aortic.Id;
        aortic_TG.Company__c = aortic.Business_Unit__r.Company__c;
        
        Therapy_Group__c polestar = new Therapy_Group__c();
        polestar.Name = 'Polestar';
        polestar.Sub_Business_Unit__c = neurosurgery.Id;
        polestar.Company__c = neurosurgery.Business_Unit__r.Company__c;
                
        insert new List<Therapy_Group__c>{phasedRF, MCS_TG, aortic_TG, polestar};
                        
        Therapy__c phasedRFTherapy = new Therapy__c();
        phasedRFTherapy.Name = 'Phased RF';
        phasedRFTherapy.Business_Unit__c = AFSolutions.Business_Unit__c;
        phasedRFTherapy.Sub_Business_Unit__c = AFSolutions.id;
        phasedRFTherapy.Therapy_Group__c = phasedRF.id;  
        phasedRFTherapy.Therapy_Name_Hidden__c = 'Phased RF';             
        insert phasedRFTherapy;
		
		Product_Group__c productGroup = new Product_Group__c();
        productGroup.Name = 'Hardware pRF';
        productGroup.Therapy_ID__c = phasedRFTherapy.Id;
        insert productGroup;
		
		Product2 prod = new Product2();
		prod.Name = 'Test Product';
		prod.RecordTypeId = [Select Id from RecordType where DeveloperName = 'SAP_Product' AND SObjectType = 'Product2'].Id;
		prod.Product_Group__c = productGroup.Id;
		prod.Business_Unit_ID__c = AFSolutions.Business_Unit__c;
		prod.Business_Unit_Group__c = AFSolutions.Business_Unit__r.Business_Unit_Group__c;
		insert prod;
		
		Account acc = new Account();
		acc.Name = 'Test Account';
		acc.SAP_Id__c = '123456789';
		acc.Account_Country_vs__c = 'BELGIUM';
		insert acc;
		
		Account_Team_Member__c accTMPhasedRF = new Account_Team_Member__c();
		accTMPhasedRF.Account__c = acc.Id;
		accTMPhasedRF.User__c = testUsers[0].Id;
		accTMPhasedRF.Therapy_Group__c = phasedRF.Id;
		accTMPhasedRF.Primary__c = true;
		
		Account_Team_Member__c accTMMCS = new Account_Team_Member__c();
		accTMMCS.Account__c = acc.Id;
		accTMMCS.User__c = testUsers[1].Id;
		accTMMCS.Therapy_Group__c = MCS_TG.Id;
		accTMMCS.Primary__c = true;
		
		Account_Team_Member__c accTMAortic = new Account_Team_Member__c();
		accTMAortic.Account__c = acc.Id;
		accTMAortic.User__c = testUsers[2].Id;
		accTMAortic.Therapy_Group__c = aortic_TG.Id;
		accTMAortic.Primary__c = true;
		
		Account_Team_Member__c accTMPolestar = new Account_Team_Member__c();
		accTMPolestar.Account__c = acc.Id;
		accTMPolestar.User__c = testUsers[3].Id;
		accTMPolestar.Therapy_Group__c = polestar.Id;
		accTMPolestar.Primary__c = true;
		
		insert new List<Account_Team_Member__c>{accTMPhasedRF, accTMMCS, accTMAortic, accTMPolestar};
		
		Account_Plan_2__c accPlan = new Account_Plan_2__c();
		accPlan.Account__c = acc.Id;
		accPlan.Account_Plan_Level__c = 'Sub Business Unit';
		accPlan.Business_Unit_Group__c = AFSolutions.Business_Unit__r.Business_Unit_Group__c;
		accPlan.Business_Unit__c = AFSolutions.Business_Unit__c;
		accPlan.Sub_Business_Unit__c = AFSolutions.Id;
		insert accPlan;
		
		Test.startTest();
		
		List<Id> fixedSearchResults = new List<Id>{prod.Id};
       	Test.setFixedSearchResults(fixedSearchResults);
		
		ctrl_FindResponsibleSR.searchProducts('Test search');
		ctrl_FindResponsibleSR.getProduct(prod.Id);
		
		fixedSearchResults[0] = acc.Id;
		Test.setFixedSearchResults(fixedSearchResults);
		
		ctrl_FindResponsibleSR.searchAccounts('Test search');
		ctrl_FindResponsibleSR.getAccount(acc.Id);
		
		List<ctrl_FindResponsibleSR.SubBusinessUnit> subBusinessUnits = ctrl_FindResponsibleSR.findSalesRep(productGroup.Id, AFSolutions.Business_Unit__c, AFSolutions.Business_Unit__r.Business_Unit_Group__c, acc.Id);		
		System.assert(subBusinessUnits.size() == 1);
		System.assert(subBusinessUnits[0].Name == 'AF Solutions');
		System.assert(subBusinessUnits[0].AccountPlanId == accPlan.Id);
		System.assert(subBusinessUnits[0].SalesReps.size() == 1);
		System.assert(subBusinessUnits[0].SalesReps[0].Id == testUsers[0].Id);
		
		subBusinessUnits = ctrl_FindResponsibleSR.findSalesRep(null, AFSolutions.Business_Unit__c, AFSolutions.Business_Unit__r.Business_Unit_Group__c, acc.Id);		
		System.assert(subBusinessUnits.size() == 2);
		System.assert(subBusinessUnits[1].Name == 'MCS');
		System.assert(subBusinessUnits[1].AccountPlanId == null);
		System.assert(subBusinessUnits[1].SalesReps.size() == 1);
		System.assert(subBusinessUnits[1].SalesReps[0].Id == testUsers[1].Id);
				
		subBusinessUnits = ctrl_FindResponsibleSR.findSalesRep(null, null, AFSolutions.Business_Unit__r.Business_Unit_Group__c, acc.Id);		
		System.assert(subBusinessUnits.size() == 3);
		System.assert(subBusinessUnits[1].Name == 'Aortic');
		System.assert(subBusinessUnits[1].AccountPlanId == null);
		System.assert(subBusinessUnits[1].SalesReps.size() == 1);
		System.assert(subBusinessUnits[1].SalesReps[0].Id == testUsers[2].Id);		
		
		subBusinessUnits = ctrl_FindResponsibleSR.findSalesRep(null, null, null, acc.Id);		
		System.assert(subBusinessUnits.size() == 4);
		System.assert(subBusinessUnits[3].Name == 'Neurosurgery');
		System.assert(subBusinessUnits[3].AccountPlanId == null);
		System.assert(subBusinessUnits[3].SalesReps.size() == 1);
		System.assert(subBusinessUnits[3].SalesReps[0].Id == testUsers[3].Id);				
	}    
}