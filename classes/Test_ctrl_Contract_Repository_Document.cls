@isTest
private class Test_ctrl_Contract_Repository_Document {
    
    private static testmethod void testGetContractDocuments(){
        
        Contract_Repository_Interface_Settings__c settings = new Contract_Repository_Interface_Settings__c();
        settings.Documentum_Key__c = 'abcdefghijklmnop';
        settings.Documentum_Server_URL__c = 'https://medtronic.com';
        settings.End_Point_URL_Server__c = 'https://medtronic.com';
        settings.Org_Id__c = UserInfo.getOrganizationId().left(15);
        settings.Password__c = 'testuser';
        settings.Username__c = 'testpassword';      
        insert settings;
        
        Account testAcc = new Account();
        testAcc.Name = 'Test Account';
        testAcc.Account_Country_vs__c = 'BELGIUM';
        insert testAcc;
        
        Contract_Repository__c contract = new Contract_Repository__c();
        contract.Account__c = testAcc.Id;
        contract.Primary_Contract_Type__c = 'Rebate agreement';
        contract.Rebate_Type__c = 'Straight Unit';
        //contract.Payment_Options__c = 'Credit Note';
        contract.put('Payment_Options__c','Credit Note');
        contract.Pre_Payment_Terms__c = 'Final Payment';
        contract.Payment_Period__c = 'Monthly';     
        contract.Original_Valid_From_Date__c = Date.today();
        contract.Original_Valid_To_Date__c = Date.today().addYears(1);
        insert contract;
                
        List<User> otherUsers = [Select Id from User where Profile.Name IN ('EUR Field Force CVG', 'EUR Field Force RTG', 'EUR Field Force MITG') AND isActive = true and Id != :UserInfo.getUserId() LIMIT 4];
        
        Contract_Repository_Team__c contractTeam2 = new Contract_Repository_Team__c();
        contractTeam2.Contract_Repository__c = contract.Id;
        contractTeam2.Team_Member__c = otherUsers[1].Id;
        contractTeam2.Access_Level__c = 'Read Only - No Access to Documents';
        
        Contract_Repository_Team__c contractTeam3 = new Contract_Repository_Team__c();
        contractTeam3.Contract_Repository__c = contract.Id;
        contractTeam3.Team_Member__c = otherUsers[2].Id;
        contractTeam3.Access_Level__c = 'Read Only - Access to Documents';
        
        Contract_Repository_Team__c contractTeam4 = new Contract_Repository_Team__c();
        contractTeam4.Contract_Repository__c = contract.Id;
        contractTeam4.Team_Member__c = otherUsers[3].Id;
        contractTeam4.Access_Level__c = 'None';
        
        insert new List<Contract_Repository_Team__c>{contractTeam2, contractTeam3, contractTeam4};
                
        Contract_Repository_Document__c doc1 = new Contract_Repository_Document__c();       
        doc1.Contract_Repository__c = contract.Id;
        doc1.Name = 'Document 1';
        doc1.Document_Type__c = '2. Contract';
        
        Contract_Repository_Document__c doc2 = new Contract_Repository_Document__c();       
        doc2.Contract_Repository__c = contract.Id;
        doc2.Name = 'Document 2';
        doc2.Document_Type__c = '4. Changes';
        
        Contract_Repository_Document__c doc3 = new Contract_Repository_Document__c();       
        doc3.Contract_Repository__c = contract.Id;
        doc3.Name = 'Document 3';
        doc3.Document_Type__c = '4. Changes';
                
        insert new List<Contract_Repository_Document__c>{doc1, doc2,doc3};
        
        Contract_Repository_Document_Version__c doc1Version1 = new Contract_Repository_Document_Version__c();
        doc1Version1.Document__c = doc1.Id;
        doc1Version1.File_Name__c = 'File_name v1';
        doc1Version1.Version__c = 1;
        doc1Version1.File_Type__c = 'csv';
        doc1Version1.Size__c = 1024;
        doc1Version1.Documentum_ID__c = 'Doc11';
        
        Contract_Repository_Document_Version__c doc1Version2 = new Contract_Repository_Document_Version__c();
        doc1Version2.Document__c = doc1.Id;
        doc1Version2.File_Name__c = 'File_name v2';
        doc1Version2.Version__c = 2;
        doc1Version2.File_Type__c = 'csv';
        doc1Version2.Size__c = 1024;
        doc1Version2.Documentum_ID__c = 'Doc12';
        
        Contract_Repository_Document_Version__c doc2Version1 = new Contract_Repository_Document_Version__c();
        doc2Version1.Document__c = doc2.Id;
        doc2Version1.File_Name__c = 'File_name v1';
        doc2Version1.Version__c = 1;
        doc2Version1.File_Type__c = 'csv';
        doc1Version1.Size__c = 1024;
        doc2Version1.Documentum_ID__c = 'Doc21';
        
        insert new List<Contract_Repository_Document_Version__c>{doc1Version1, doc1Version2, doc2Version1};
        
        Test.startTest();
        
        ctrl_Contract_Repository_Document.ContractRepository contractRepo = ctrl_Contract_Repository_Document.getContractDocuments(contract.Id);
        
        System.assert(contractRepo.AccessLevel == 'Edit');
        System.assert(contractRepo.documents.size() == 3);
        
        System.runAs(otherUsers[0]){
            
            contractRepo = ctrl_Contract_Repository_Document.getContractDocuments(contract.Id);
            
            System.assert(contractRepo.AccessLevel == 'None');
            System.assert(contractRepo.documents.size() == 0);
        
        }
        
        System.runAs(otherUsers[1]){
            
            contractRepo = ctrl_Contract_Repository_Document.getContractDocuments(contract.Id);
            
            System.assert(contractRepo.AccessLevel == 'None');
            System.assert(contractRepo.documents.size() == 3);
        
        }
        
        System.runAs(otherUsers[2]){
            
            contractRepo = ctrl_Contract_Repository_Document.getContractDocuments(contract.Id);
            
            System.assert(contractRepo.AccessLevel == 'Read');
            System.assert(contractRepo.documents.size() == 3);
        
        }
        
        System.runAs(otherUsers[3]){
            
            contractRepo = ctrl_Contract_Repository_Document.getContractDocuments(contract.Id);
            
            System.assert(contractRepo.AccessLevel == 'None');
            System.assert(contractRepo.documents.size() == 0);
        
        }
        
        List<ctrl_Contract_Repository_Document.DocumentType> types = ctrl_Contract_Repository_Document.getDocumentTypes();
        
        System.assert(types.size() > 0);
        
        String documentumURL  = ctrl_Contract_Repository_Document.getDocumentVersionURL(doc1Version1.Id);
        System.assert(documentumURL != null);
        
        contractTeam4.Access_Level__c = 'Read Only - Upload Access to Documents';
        Update contractTeam4;
        System.runAs(otherUsers[3]){
            contractRepo = ctrl_Contract_Repository_Document.getContractDocuments(contract.Id);
            System.assert(contractRepo.AccessLevel == 'Only Upload');
            System.assert(contractRepo.documents.size() == 3);
        }
        
        System.runAs(otherUsers[2]){
            string errorMsg;
            try{
            	ctrl_Contract_Repository_Document.documentDeletion(doc2.Id);
            }
            catch(exception exp){
                errorMsg = exp.getMessage();
            }
            System.assert(String.isNotBlank(errorMsg));
            contractRepo = ctrl_Contract_Repository_Document.getContractDocuments(contract.Id);
            System.assert(contractRepo.documents.size() == 3);
        }
    }
    
    private static testmethod void testDeletion(){
        
        Account testAcc = new Account();
        testAcc.Name = 'Test Account';
        testAcc.Account_Country_vs__c = 'BELGIUM';
        insert testAcc;
        
        Contract_Repository__c contract = new Contract_Repository__c();
        contract.Account__c = testAcc.Id;
        contract.Primary_Contract_Type__c = 'Rebate agreement';
        contract.Rebate_Type__c = 'Straight Unit';
        //contract.Payment_Options__c = 'Credit Note';
        contract.put('Payment_Options__c','Credit Note');        
        contract.Pre_Payment_Terms__c = 'Final Payment';
        contract.Payment_Period__c = 'Monthly';     
        contract.Original_Valid_From_Date__c = Date.today();
        contract.Original_Valid_To_Date__c = Date.today().addYears(1);
        insert contract;
                                
        Contract_Repository_Document__c doc1 = new Contract_Repository_Document__c();       
        doc1.Contract_Repository__c = contract.Id;
        doc1.Name = 'Document 1';
        doc1.Document_Type__c = '2. Contract';
        
        Contract_Repository_Document__c doc2 = new Contract_Repository_Document__c();       
        doc2.Contract_Repository__c = contract.Id;
        doc2.Name = 'Document 2';
        doc2.Document_Type__c = '4. Changes';
                
        insert new List<Contract_Repository_Document__c>{doc1, doc2};
        
        Contract_Repository_Document_Version__c doc1Version1 = new Contract_Repository_Document_Version__c();
        doc1Version1.Document__c = doc1.Id;
        doc1Version1.File_Name__c = 'File_name v1';
        doc1Version1.Version__c = 1;
        doc1Version1.File_Type__c = 'csv';
        doc1Version1.Size__c = 1024;
        doc1Version1.Documentum_ID__c = 'Doc11';
        
        Contract_Repository_Document_Version__c doc1Version2 = new Contract_Repository_Document_Version__c();
        doc1Version2.Document__c = doc1.Id;
        doc1Version2.File_Name__c = 'File_name v2';
        doc1Version2.Version__c = 2;
        doc1Version2.File_Type__c = 'csv';
        doc1Version2.Size__c = 1024;        
        doc1Version2.Interface_Error__c = 'Error during sync';
        
        Contract_Repository_Document_Version__c doc2Version1 = new Contract_Repository_Document_Version__c();
        doc2Version1.Document__c = doc2.Id;
        doc2Version1.File_Name__c = 'File_name v1';
        doc2Version1.Version__c = 1;
        doc2Version1.File_Type__c = 'csv';
        doc2Version1.Size__c = 1024;
        doc2Version1.Documentum_ID__c = 'Doc21';
        
        Contract_Repository_Document_Version__c doc2Version2 = new Contract_Repository_Document_Version__c();
        doc2Version2.Document__c = doc2.Id;
        doc2Version2.File_Name__c = 'File_name v2';
        doc2Version2.Version__c = 2;
        doc2Version2.File_Type__c = 'csv';
        doc2Version2.Size__c = 1024;        
        doc2Version2.Interface_Error__c = 'Error during sync';
                
        insert new List<Contract_Repository_Document_Version__c>{doc1Version1, doc1Version2, doc2Version1, doc2Version2};
        
        Test.startTest();
        
        ctrl_Contract_Repository_Document.ContractRepository contractRepo = ctrl_Contract_Repository_Document.getContractDocuments(contract.Id);
        
        System.assert(contractRepo.AccessLevel == 'Edit');
        System.assert(contractRepo.documents.size() == 2);
        
        ctrl_Contract_Repository_Document.markVersionForDeletion(doc1Version1.Id);
        
        doc1Version1 = [Select Id, Marked_for_Deletion__c, Is_Deleted__c from Contract_Repository_Document_Version__c where Id = :doc1Version1.Id];
        
        System.assert(doc1Version1.Marked_for_Deletion__c == true);
        System.assert(doc1Version1.Is_Deleted__c == false);
        
        ctrl_Contract_Repository_Document.markVersionForDeletion(doc1Version1.Id);
        
        doc1Version1 = [Select Id, Marked_for_Deletion__c, Is_Deleted__c from Contract_Repository_Document_Version__c where Id = :doc1Version1.Id];
        
        System.assert(doc1Version1.Marked_for_Deletion__c == true);
        System.assert(doc1Version1.Is_Deleted__c == false);
        
        ctrl_Contract_Repository_Document.markVersionForDeletion(doc2Version2.Id);
        
        doc2Version2 = [Select Id, Marked_for_Deletion__c, Is_Deleted__c from Contract_Repository_Document_Version__c where Id = :doc2Version2.Id];
        
        System.assert(doc2Version2.Marked_for_Deletion__c == true);
        System.assert(doc2Version2.Is_Deleted__c == true);
        
        ctrl_Contract_Repository_Document.documentDeletion(doc1.Id);
        List<Id> deletedDocVersions = new List<Id>{doc1Version1.Id, doc1Version2.Id};
        
        List<Contract_Repository_Document_Version__c> doc1Versions = [Select Id, Marked_for_Deletion__c, Is_Deleted__c from Contract_Repository_Document_Version__c where ID IN :deletedDocVersions];
        System.assert(doc1Versions.size() == 1);
        
        System.assert(doc1Versions[0].Marked_for_Deletion__c == true);
        System.assert(doc1Versions[0].Is_Deleted__c == false);      
    }
}