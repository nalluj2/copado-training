//--------------------------------------------------------------------------------------------------------------------
//  Description : This TEST APEX Class is used to test the logic in bl_AccountPerformance
//  Version     : 1.0
//  Author      : Bart Caelen
//  Date        : 16/09/2016
//--------------------------------------------------------------------------------------------------------------------
@isTest private class Test_bl_AccountPerformance {
	
	private static User oUser_SystemAdmin_MDT;
	private static User oUser_SystemAdmin;

	@isTest static void createTestUserData(){

		//------------------------------------------------------------------------------------
		// Create Test User Data
		//------------------------------------------------------------------------------------
		List<User> lstUser = new List<User>();
		
		oUser_SystemAdmin = clsTestData_User.createUser_SystemAdministrator('tadm000', false);
		lstUser.add(oUser_SystemAdmin);

		oUser_SystemAdmin_MDT = clsTestData_User.createUser_SystemAdministratorMDT('tadm001', false);
			oUser_SystemAdmin_MDT.Job_Title_vs__c = 'Technical Consultant';
			oUser_SystemAdmin_MDT.User_Business_Unit_vs__c = 'CRHF';
		lstUser.add(oUser_SystemAdmin_MDT);

		insert lstUser;

		System.runAs(oUser_SystemAdmin){
//			clsTestData_User.createUserZone(oUser_SystemAdmin_MDT);
		}
		//------------------------------------------------------------------------------------

	}


	@isTest static void createTestData(){

		//------------------------------------------------------------------------------------
		// Create Test Data
		//------------------------------------------------------------------------------------
		// Create Account Data
		clsTestData_Account.idRecordType_Account = clsUtil.getRecordTypeByDevName('Account', 'SAP_Account').Id;
		clsTestData_Account.tCountry_Account = 'Belgium';
		clsTestData_Account.createAccount();

		// Create Therapy Data
		clsTestData_Therapy.createTherapy();

		// Create Product Group Data
		clsTestData_Product.createProductGroup();

		// Create Account Plan 2
		clsTestData_AccountPlan.createAccountPlan2();
		//------------------------------------------------------------------------------------


	}
	
	@isTest static void test_bl_AccountPerformance(){

		List<String> lstHighestLevel = new List<String>{'Business Unit', 'Sub Business Unit', 'Therapy', 'Product Group'};

		createTestUserData();

		System.runAs(oUser_SystemAdmin){
			createTestData();
		}

		Test.startTest();

		System.runAs(oUser_SystemAdmin_MDT){
			
			User oUser_Tmp = bl_AccountPerformance.userData;
			List<Account_Performance_Rule__c> lstAccountPerformanceRules = bl_AccountPerformance.getUserAccess();

			bl_AccountPerformance.getUserAccess();
			clsTestData_AccountPerformance.createCS_ACC_PER_xBU_Job_Title();
			bl_AccountPerformance.getUserAccess();

			//------------------------------------------------------------------------------------
 	

			bl_AccountPerformance.getAccountCountry(clsTestData_Account.oMain_Account.Id);
			bl_AccountPerformance.getBUCompany();
			bl_AccountPerformance.getBUGroup();
			bl_AccountPerformance.getPGCompany();
			bl_AccountPerformance.getPGGroup();
			bl_AccountPerformance.getSBUBUs();
			bl_AccountPerformance.getSBUCompany();
			bl_AccountPerformance.getSBUGroup();

			for (String tHighestLevel : lstHighestLevel){
				bl_AccountPerformance.getShowDataPer(tHighestLevel);
			}

			bl_AccountPerformance.getTherapyBU();
			bl_AccountPerformance.getTherapyCompany();
			bl_AccountPerformance.getTherapyGroup();
			Set<Id> setID_SBU = new Set<Id>();
			for (Sub_Business_Units__c oSBU : clsTestData_MasterData.lstSubBusinessUnit){
				setID_SBU.add(oSBU.Id);
			}
			bl_AccountPerformance.getTherapyUserSBUs(setID_SBU);
			bl_AccountPerformance.getPGSBUs(setID_SBU);

			bl_AccountPerformance.getUserSUB();
			//bl_AccountPerformance.getUserTeritory();
			bl_AccountPerformance.getuserCountry();


				clsTestData_AccountPlan.oMain_AccountPlan2.Business_Unit_Group__c = null;
				clsTestData_AccountPlan.oMain_AccountPlan2.Business_Unit__c = null;
				clsTestData_AccountPlan.oMain_AccountPlan2.Sub_Business_Unit__c = null;
			iPlanResultWrapper oIPlanResultWrapper = bl_AccountPerformance.doesIplanExist(clsTestData_AccountPlan.oMain_AccountPlan2);
				clsTestData_AccountPlan.oMain_AccountPlan2.Business_Unit_Group__c = clsTestData_MasterData.oMain_BusinessUnitGroup.Id;
				clsTestData_AccountPlan.oMain_AccountPlan2.Business_Unit__c = null;
				clsTestData_AccountPlan.oMain_AccountPlan2.Sub_Business_Unit__c = null;
			oIPlanResultWrapper = bl_AccountPerformance.doesIplanExist(clsTestData_AccountPlan.oMain_AccountPlan2);
				clsTestData_AccountPlan.oMain_AccountPlan2.Business_Unit_Group__c = null;
				clsTestData_AccountPlan.oMain_AccountPlan2.Business_Unit__c = clsTestData_MasterData.oMain_BusinessUnit.Id;
				clsTestData_AccountPlan.oMain_AccountPlan2.Sub_Business_Unit__c = null;
			oIPlanResultWrapper = bl_AccountPerformance.doesIplanExist(clsTestData_AccountPlan.oMain_AccountPlan2);
				clsTestData_AccountPlan.oMain_AccountPlan2.Business_Unit_Group__c = null;
				clsTestData_AccountPlan.oMain_AccountPlan2.Business_Unit__c = null;
				clsTestData_AccountPlan.oMain_AccountPlan2.Sub_Business_Unit__c = clsTestData_MasterData.oMain_SubBusinessUnit.Id;
			oIPlanResultWrapper = bl_AccountPerformance.doesIplanExist(clsTestData_AccountPlan.oMain_AccountPlan2);

		}

		Test.stopTest();
	}

}
//--------------------------------------------------------------------------------------------------------------------