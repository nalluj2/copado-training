//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 16-08-2017
//  Description      : APEX Class - Business Logic for tr_AccountExtendedProfile
//  Change Log       : PMT-22787: MITG EMEA User Setup - Added record type EMEA_SWOT_Analysis
//--------------------------------------------------------------------------------------------------------------------
public with sharing class bl_AccountExtendedProfile_Trigger {

	//------------------------------------------------------------------------------------------------------------------------------------------
	// Set the Unique_Key__field on Account_Extended_Profile__c equal to "Account__c"_"RecordTypeId for Record Type "EMEA_Account_Extended_Profile".
	// This logic makes it possible to have only 1 Account_Extended_Profile__c record per Account and Account_Extended_Profile__c RecordType
	//
	// Set the Unique_Key__field on Account_Extended_Profile__c equal to "Account__c"_"RecordTypeId"_"Sub_Business_Unit__c" for Record Type "EUR_SWOT_Analysis" & "MEA_SWOT_Analysis".
	// This logic makes it possible to have only 1 Account_Extended_Profile__c record per Account and Account_Extended_Profile__c RecordType and Account_Extended_Profile__c Sub_Business_Unit__c 
	//------------------------------------------------------------------------------------------------------------------------------------------
	public static void setUniqueKey(List<Account_Extended_Profile__c> lstTriggerNew, Map<Id, Account_Extended_Profile__c> mapTriggerOld){

        if (bl_Trigger_Deactivation.isTriggerDeactivated('accExtProfile_setUniqueKey')) return;

		// Define the RecordTypes for which this logic should be executed
		Set<Id> setID_RecordType_EMEA = new Set<Id>();
			setID_RecordType_EMEA.add(clsUtil.getRecordTypeByDevName('Account_Extended_Profile__c', 'EMEA_Account_Extended_Profile').Id);

		Set<Id> setID_RecordType_SWOT = new Set<Id>();
			//setID_RecordType_SWOT.add(clsUtil.getRecordTypeByDevName('Account_Extended_Profile__c', 'EUR_SWOT_Analysis').Id);
			setID_RecordType_SWOT.add(clsUtil.getRecordTypeByDevName('Account_Extended_Profile__c', 'MEA_SWOT_Analysis').Id);
        	setID_RecordType_SWOT.add(clsUtil.getRecordTypeByDevName('Account_Extended_Profile__c', 'EMEA_SWOT_Analysis').Id);


		for ( Account_Extended_Profile__c oAccountExtendedProfile : lstTriggerNew){

			String tUniqueKey = '';
			Boolean bIsEURSWOT = false;

			if (setID_RecordType_EMEA.contains(oAccountExtendedProfile.RecordTypeId)){

				tUniqueKey = oAccountExtendedProfile.Account__c + '_' + oAccountExtendedProfile.RecordTypeId;

			}else if (setID_RecordType_SWOT.contains(oAccountExtendedProfile.RecordTypeId)){

				tUniqueKey = oAccountExtendedProfile.Account__c + '_' + oAccountExtendedProfile.RecordTypeId + '_' + oAccountExtendedProfile.Sub_Business_Unit_vs__c;

			}else{

				continue;
			}

			if (mapTriggerOld == null){

				oAccountExtendedProfile.Unique_Key__c = tUniqueKey;

			}else{

				Account_Extended_Profile__c oAccountExtendedProfile_Old = mapTriggerOld.get(oAccountExtendedProfile.Id);

				if (oAccountExtendedProfile.Unique_Key__c != tUniqueKey){

					oAccountExtendedProfile.Unique_Key__c = tUniqueKey;

				}

			}

		}

	}
	//------------------------------------------------------------------------------------------------------------------------------------------



	//------------------------------------------------------------------------------------------------------------------------------------------
	// Create History_Tracking__c records based on the changes on the Account_Extended_Profile__c record
	//------------------------------------------------------------------------------------------------------------------------------------------
	public static void createHistoryTracking(List<Account_Extended_Profile__c> lstTriggerNew, Map<Id, Account_Extended_Profile__c> mapTriggerOld){

        if (bl_Trigger_Deactivation.isTriggerDeactivated('accExtProfile_createHistoryTracking')) return;

		List<History_Tracking__c> lstHistoryTracking = new List<History_Tracking__c>();

		// Define the RecordTypes for which this logic should be executed
		Set<Id> setID_RecordType = new Set<Id>();
			setID_RecordType.add(clsUtil.getRecordTypeByDevName('Account_Extended_Profile__c', 'EMEA_Account_Extended_Profile').Id);

		// The field name's that should be tracked

		// Load Related Data
		Map<Id, String> mapRelatedData = new Map<Id, String>();
		Map<String, HistoryTracking_AccountExtendedProfile__c> mapHistoryTracking = HistoryTracking_AccountExtendedProfile__c.getAll();
		for (String tKey : mapHistoryTracking.keySet()){

			HistoryTracking_AccountExtendedProfile__c oHistoryTracking = mapHistoryTracking.get(tKey);
			String tReferenceTo = oHistoryTracking.ReferenceTo__c;
			String tReferenceToField = oHistoryTracking.ReferenceToField__c;

			if ( (!String.isBlank(tReferenceTo)) && (!String.isBlank(tReferenceToField)) ){

				mapRelatedData.putAll(bl_HistoryTracking.loadRelatedData(tReferenceTo, tReferenceToField, lstTriggerNew, mapTriggerOld, String.valueOf(oHistoryTracking.get('Name'))));

			}

		}

		Set<String> setFieldName_HistoryTracking = HistoryTracking_AccountExtendedProfile__c.getAll().keySet();
		if (setFieldName_HistoryTracking.size() == 0) return;

		for (Account_Extended_Profile__c oAccountExtendedProfile : lstTriggerNew){

			if (!setID_RecordType.contains(oAccountExtendedProfile.RecordTypeId)) continue;

			Account_Extended_Profile__c oAccountExtendedProfile_Old;
			if (mapTriggerOld != null){

				oAccountExtendedProfile_Old = mapTriggerOld.get(oAccountExtendedProfile.Id);

			}

			lstHistoryTracking.addAll(bl_HistoryTracking.createHistoryTracking('Account_Extended_Profile__c', 'Account_Extended_Profile__c', oAccountExtendedProfile_Old, oAccountExtendedProfile, setFieldName_HistoryTracking, mapRelatedData, false));

		}

		if (lstHistoryTracking.size() > 0){

			insert lstHistoryTracking;
	
		}

	}
	//------------------------------------------------------------------------------------------------------------------------------------------


	//------------------------------------------------------------------------------------------------------------------------------------------
	// For Account Extended Profile records with the record type "EMEA_Account_Extended_Profile" we need to 
	//	reset the Approval Status of a section to "Open" if the Approval Status is not "Open" and 1 or more fields in that section are changed.
	//------------------------------------------------------------------------------------------------------------------------------------------
	public static void resetApprovalStatus(List<Account_Extended_Profile__c> lstTriggerNew, Map<Id, Account_Extended_Profile__c> mapTriggerOld){

		Set<Id> setID_RecordType = new Set<Id>{clsUtil.getRecordTypeByDevName('Account_Extended_Profile__c', 'EMEA_Account_Extended_Profile').Id};

		Map<String, List<String>> mapApprovalStatusField_RelatedFields = new Map<String, List<String>>();
			mapApprovalStatusField_RelatedFields.put('Approval_Status_Facility_Info__c', new List<String>{'Total_Account_Beds__c','ORs__c','Anesthesia_Procedures__c','CABG_Procedures__c','Beds__c','NCU__c','PCU__c'});
			mapApprovalStatusField_RelatedFields.put('Approval_Status_SI_AST_Driven__c', new List<String>{'Bariatric_procedures__c','of_lap_bariatric_procedures__c','Thoracic_procedures__c','of_lap_thoracic_procedures__c','Colon_procedures__c','of_lap_colon_procedures__c','Hemorrhoids_procedures__c','Rectum_procedures__c','of_lap_rectum_procedures__c','Head_Neck_procedures__c','of_lap_head_neck_procedures__c'});
			mapApprovalStatusField_RelatedFields.put('Approval_Status_SI_GSP_Driven__c', new List<String>{'General_Surgery_procedures__c','of_lap_general_surgery_procedures__c','Gynecologic_procedures__c','of_lap_gynecologic_procedures__c','Urology_procedures__c','of_lap_urology_procedures__c', 'Pediatric_procedures__c', 'ENT_procedures__c'});
			mapApprovalStatusField_RelatedFields.put('Approval_Status_SI_Hernia_Driven__c', new List<String>{'Inguinal_hernia_procedures__c','of_lap_Inguinal_hernia_procedures__c','Ventral_hernia_procedures__c','of_lap_ventral_hernia_procedures__c'});

		for (Account_Extended_Profile__c oAccountExtendedProfile : lstTriggerNew){

			if (!setID_RecordType.contains(oAccountExtendedProfile.RecordTypeId)) continue;

			Account_Extended_Profile__c oAccountExtendedProfile_Old = mapTriggerOld.get(oAccountExtendedProfile.Id);

			for (String tApprovalStatusField : mapApprovalStatusField_RelatedFields.keySet()){

				if ( (oAccountExtendedProfile.get(tApprovalStatusField) != oAccountExtendedProfile_Old.get(tApprovalStatusField) ) && (oAccountExtendedProfile.get(tApprovalStatusField) != 'Open') ){
					continue;
				}

				if (oAccountExtendedProfile.get(tApprovalStatusField) != 'Open'){

					Boolean bResetApprovalStatus = false;
					for (String tFieldName : mapApprovalStatusField_RelatedFields.get(tApprovalStatusField)){

						if (oAccountExtendedProfile.get(tFieldName) != oAccountExtendedProfile_Old.get(tFieldName)){
							bResetApprovalStatus = true;
							break;
						}

					}

					if (bResetApprovalStatus){

						oAccountExtendedProfile.put(tApprovalStatusField, 'Open');

					}

				}

			}	

		}

	}
	//------------------------------------------------------------------------------------------------------------------------------------------


	//------------------------------------------------------------------------------------------------------------------------------------------
	// For Account Extended Profile records with the record type "EMEA_Account_Extended_Profile" we need to set the Approver Email based on the 
	//	Section owner.  The Approver Email is the email of the Manager of the Section Owner. This logic needs to be executed for all (4) sections.
	//------------------------------------------------------------------------------------------------------------------------------------------
	public static void setApproverEmail(List<Account_Extended_Profile__c> lstTriggerNew, Map<Id, Account_Extended_Profile__c> mapTriggerOld){

		Set<Id> setID_RecordType = new Set<Id>{clsUtil.getRecordTypeByDevName('Account_Extended_Profile__c', 'EMEA_Account_Extended_Profile').Id};

		Map<Id, String> mapUserId_ManagerEmail = new Map<Id, String>();

		Map<String, String> mapSectionOwner_ApproverEmail = new Map<String, String>{'Section_Owner_Facility_Info__c'=>'Section_Facility_Info_Approver_Email__c', 'Section_Owner_SI_AST_Driven__c'=>'Section_SI_AST_Driven_Approver_Email__c', 'Section_Owner_SI_GSP_Driven__c'=>'Section_SI_GSP_Driven_Approver_Email__c', 'Section_Owner_SI_Hernia_Driven__c'=>'Section_SI_Hernia_Driven_Approver_Email__c'};

		for (Account_Extended_Profile__c oAccountExtendedProfile : lstTriggerNew){

			if (!setID_RecordType.contains(oAccountExtendedProfile.RecordTypeId)) continue;

			for (String tFieldName : mapSectionOwner_ApproverEmail.keySet()){
				if (oAccountExtendedProfile.get(tFieldName) != null) mapUserId_ManagerEmail.put(String.valueOf(oAccountExtendedProfile.get(tFieldName)), '');
			}

		}

		if (mapUserId_ManagerEmail.size() == 0) return;

		Map<Id, User> mapUser = new Map<Id, User>([SELECT Id, Manager_email__c FROM User WHERE Id = :mapUserId_ManagerEmail.keySet()]);

		for (Account_Extended_Profile__c oAccountExtendedProfile : lstTriggerNew){

			if (!setID_RecordType.contains(oAccountExtendedProfile.RecordTypeId)) continue;

			for (String tSectionOwner : mapSectionOwner_ApproverEmail.keySet()){

				if (oAccountExtendedProfile.get(tSectionOwner) != null){

					if ( (mapTriggerOld == null) || (oAccountExtendedProfile.get(tSectionOwner) != mapTriggerOld.get(oAccountExtendedProfile.Id).get(tSectionOwner)) ){

						oAccountExtendedProfile.put(mapSectionOwner_ApproverEmail.get(tSectionOwner), mapUser.get(String.valueOf(oAccountExtendedProfile.get(tSectionOwner))).Manager_email__c);

					}
		
				}

			}

		}

	}
	//------------------------------------------------------------------------------------------------------------------------------------------


	//------------------------------------------------------------------------------------------------------------------------------------------
	// For Account Extended Profile records with the record type "EUR_SWOT_Analysis" or "MEA_SWOT_Analysis" we need to set the field
	//	Last_Updated__c to the current date if one or more of the defined fields are changed.
	//------------------------------------------------------------------------------------------------------------------------------------------
	public static void setLastUpdated(List<Account_Extended_Profile__c> lstTriggerNew, Map<Id, Account_Extended_Profile__c> mapTriggerOld){

		Set<Id> setID_RecordType_SWOT = new Set<Id>();
			//setID_RecordType_SWOT.add(clsUtil.getRecordTypeByDevName('Account_Extended_Profile__c', 'EUR_SWOT_Analysis').Id);
			setID_RecordType_SWOT.add(clsUtil.getRecordTypeByDevName('Account_Extended_Profile__c', 'MEA_SWOT_Analysis').Id);
        	setID_RecordType_SWOT.add(clsUtil.getRecordTypeByDevName('Account_Extended_Profile__c', 'EMEA_SWOT_Analysis').Id);

		for (Account_Extended_Profile__c oAccountExtendedProfile : lstTriggerNew){

			if (!setID_RecordType_SWOT.contains(oAccountExtendedProfile.RecordTypeId)) continue;

			if (mapTriggerOld == null){

				if (
					oAccountExtendedProfile.Strengths_Hernia__c != null || oAccountExtendedProfile.Weaknesses_Hernia__c != null || oAccountExtendedProfile.Threats_Hernia__c != null || oAccountExtendedProfile.Opportunities_Hernia__c != null
				){

					oAccountExtendedProfile.Last_Updated__c = Date.today();

				}

			}else{

				Account_Extended_Profile__c oAccountExtendedProfile_Old = mapTriggerOld.get(oAccountExtendedProfile.Id);

				if (
					oAccountExtendedProfile.Strengths_Hernia__c != oAccountExtendedProfile_Old.Strengths_Hernia__c || oAccountExtendedProfile.Weaknesses_Hernia__c != oAccountExtendedProfile_Old.Weaknesses_Hernia__c || oAccountExtendedProfile.Threats_Hernia__c != oAccountExtendedProfile_Old.Threats_Hernia__c || oAccountExtendedProfile.Opportunities_Hernia__c != oAccountExtendedProfile_Old.Opportunities_Hernia__c
				){

					oAccountExtendedProfile.Last_Updated__c = Date.today();

				}

			}

		}

	}
	//----------------------------------------------------------------------------------------------------------------

}
//--------------------------------------------------------------------------------------------------------------------