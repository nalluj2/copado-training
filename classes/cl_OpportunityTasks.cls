public with sharing class cl_OpportunityTasks {
	/*
	FOR ANZ: Create a task for an opportunity when some fields are filled out
	Author 			:Bart Debruyne
	Creation Date	:22-10-2012
	*/
	
	/*initializations*/
	public static boolean inFutureContext = false;
	
	
	public static boolean TaskExistAlready(Date ActivityDate, String Subject, List<Task> OppsTasks, ID oppsId)
	{
		boolean TaskFound = false;
		system.debug(oppsTasks);
		for(Task ts :OppsTasks){

			if
			(
			ts.WhatId == oppsId 
			&& ts.ActivityDate == ActivityDate
			&& TaskFound == false
			)

				TaskFound = true;
				system.debug('ActivityDate Opp' + ActivityDate + ' TASK:' + ts.ActivityDate + ' aaaaaaaaaaaaaaaaaa');
				system.debug('WhatId Opp' + oppsId + ' TASK:' + ts.WhatId + ' aaaaaaaaaaaaaaaaaa');
				system.debug('TaskFound ' + TaskFound + ' aaaaaaaaaaaaaaaaaa');
				//break;// if we have a true then stop loop and return true value				
		}
		return TaskFound;
	}
	
	
	/*call by trigger tr_Opportunity_After_Upsert*/
	public static void CreatePendingTasks(List<Opportunity> OppsToHandle){
		
		
		
		//go through opportunity list and create a task for each opportunity
		List<Task> tasks = new List<Task>();
		for(Opportunity opp:OppsToHandle){
			if(opp.Follow_up_Date__c!=null && opp.ANZ_DIB_Follow_up_Subject__c!=null)
			{
				Task tsk = new Task();
				tsk.whatID = Opp.ID;
				tsk.Ownerid = Opp.OwnerId;
				tsk.Description = opp.Follow_up_Actions__c;
				tsk.ActivityDate = opp.Follow_up_Date__c;
				tsk.Status = 'Not Started';
				tsk.Priority = 'Normal';
				
				tasks.add(tsk);
			}
		}
		
		insert tasks;
		
		
			/*update ANZ_AutoTaskCreated to true for each opportunity*/	
			
			//Set<Id> ids = OppsToHandle.keySet();
			
			List<Opportunity> oppsToUpdate=[select id,ANZ_AutoTaskCreated__c from  Opportunity where id IN :OppsToHandle];
			List<Opportunity> DML_OppsToUpdate = new List<Opportunity>();
			
			For(Opportunity o:oppsToUpdate)
			{
				o.ANZ_AutoTaskCreated__c = true;
				DML_OppsToUpdate.add(o);
			}
			
			//set it to true for not recurring execution 
			inFutureContext = true;
			
			
			Update DML_OppsToUpdate;

}

}