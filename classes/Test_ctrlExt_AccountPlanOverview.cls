/*  
    Test Class Name - Test_ctrlExt_AccountPlanOverview 
    Description  –  The written class will cover Account Plan Classes:
                    1.ctrlExt_AccountPlanOverview
                    
    Author - Manish Kumar Srivastava
    Created Date  - 29/10/2012 
*/

@isTest(seeAllData=true)//seeAllData set to true as Record type can not be inserted from test class
private class Test_ctrlExt_AccountPlanOverview{
    static testMethod void testAccountPlan() {
        Company__c c = new Company__c(Name='Acc Plan Comp',Company_Code_Text__c = 'T45'); 
        Insert C;
        user u;
        User thisUser = [ select Id from User where Id = :userinfo.getuserid() ];    
        System.runAs ( thisUser ) {
        Profile p = [select id from profile where name='System Administrator'];    
        u = new User(alias = 'standt1', email='standarduser@testorg.com',             
                        emailencodingkey='UTF-8', lastname='TerritoryManagement_Testing',             
                        languagelocalekey='en_US',             
                        localesidkey='en_US', profileid = p.Id,            
                        timezonesidkey='America/Los_Angeles',             
                        CountryOR__c='Netherlands',          
                        Country= 'Netherlands',            
                        username='AccountPlanOverview@testorg.com',
                        Alias_unique__c='standarduser_Alias_unique__c',Company_Code_Text__c='T45');  
                        insert u;          
        }   
        system.runas(u){ 
        List<Business_Unit__c> lstBU=new list<Business_Unit__c>();    
        Business_Unit_Group__c BUG=new Business_Unit_Group__c(Master_Data__c=c.id);
        insert BUG;
        Business_Unit__c BU = new Business_Unit__c(Name='Acc Plan BU',Business_Unit_Group__c=BUG.id,Company__c = C.Id);
        
        lstBU.add(BU);
        Business_Unit__c BU1 = new Business_Unit__c(Name='Acc Plan BU1',Business_Unit_Group__c=BUG.id,Company__c = C.Id);
        lstBU.add(BU1);
        Insert lstBU;
        
        List<Sub_Business_Units__c> lstSBU=new List<Sub_Business_Units__c>();
        Sub_Business_Units__c SBU = new Sub_Business_Units__c(Name='Acc Plan SBU',Business_Unit__c = BU.Id);
        lstSBU.add(SBU);
        
        Sub_Business_Units__c SBU1 = new Sub_Business_Units__c(Name='Acc Plan SBU1',Business_Unit__c = BU1.Id);
        lstSBU.add(SBU1);     
        Sub_Business_Units__c SBU2 = new Sub_Business_Units__c(Name='Acc Plan SBU2',Business_Unit__c = BU1.Id);
        lstSBU.add(SBU2);     
        Insert lstSBU;
        
        List<User_Business_Unit__c> lstUBU=new list<User_Business_Unit__c>();
        User_Business_Unit__c USBU = new User_Business_Unit__c(Sub_Business_Unit__c = SBU.Id,user__c=u.id);
        lstUBU.add(USBU);
        User_Business_Unit__c USBU1 = new User_Business_Unit__c(Sub_Business_Unit__c = SBU1.Id,user__c=u.id);
        lstUBU.add(USBU1);
        User_Business_Unit__c USBU2 = new User_Business_Unit__c(Sub_Business_Unit__c = SBU2.Id,user__c=u.id);
        lstUBU.add(USBU2);
        Insert lstUBU;
        
        List<RecordType> lstrt=[select id from Recordtype where sobjecttype='Account_Plan_2__c'];
        
        List<Account_Plan_Level__c> lstApl=new list<Account_Plan_Level__c>();
        Account_Plan_Level__c apl=new Account_Plan_Level__c(Business_Unit__c=bu1.id,Account_Plan_Level__c='Business Unit',Company__c=c.id,Account_Plan_Record_Type__c=lstrt[0].id);
        lstAPl.add(apl);
//        Account_Plan_Level__c apl1=new Account_Plan_Level__c(Business_Unit__c=bu1.id,Account_Plan_Level__c='Sub Business Unit',Company__c=c.id,Account_Plan_Record_Type__c=lstrt[0].id);
//        lstAPl.add(apl1);
//        Account_Plan_Level__c apl2=new Account_Plan_Level__c(Business_Unit__c=bu1.id,Account_Plan_Level__c='Business Unit Group',Company__c=c.id,Account_Plan_Record_Type__c=lstrt[0].id);
//        lstAPl.add(apl2);
        Account_Plan_Level__c apl3=new Account_Plan_Level__c(Business_Unit__c=bu.id,Account_Plan_Level__c='Business Unit Group',Company__c=c.id,Account_Plan_Record_Type__c=lstrt[0].id);
        lstAPl.add(apl3);
        insert lstAPl;
        
         
      
        Account a = new Account() ; 
        a.Name = 'Test Acc Plan'; 
        insert a;
        
        Account_Plan_2__c ap = new Account_Plan_2__c();
        ap.Account__c=a.id;
        
        Test.startTest();

/*        
        ApexPages.StandardController sct = new ApexPages.StandardController(ap);
        ApexPages.currentPage().getParameters().put('save_new','1'); 
        ApexPages.currentPage().getParameters().put('retURL','/011a23b6783a501b023');
        ctrlExt_AccountPlanOverview controller = new ctrlExt_AccountPlanOverview(sct);
        
        controller.selectedBU = bu1.id;
        controller.getLstSBUs();
        controller.getLstAccPlanLevels();
        controller.nextmethod();
        controller.backmethod();
        controller.changeBU();
        controller.selectedAPL = 'Business Unit Group';
        controller.BUGroup=BUG.id;
        controller.init();
        clsUtil.debug('getLstAccPlanLevels.size() : ' + controller.getLstAccPlanLevels());
        controller.createAPmethod();
*/
		         
		//-BC - 20140312 - CR-2521 - START
        ApexPages.StandardController sct = new ApexPages.StandardController(ap);
        ApexPages.currentPage().getParameters().put('save_new','1'); 
        ApexPages.currentPage().getParameters().put('retURL','/011a23b6783a501b023');
        ctrlExt_AccountPlanOverview controller = new ctrlExt_AccountPlanOverview(sct);
        
        controller.selectedAPL = 'Business Unit';
        controller.selectedBU = bu1.id;
        controller.init();

        controller.getLstSBUs();
        controller.getLstAccPlanLevels();
        controller.nextmethod();
        controller.backmethod();
        controller.changeBU();
        controller.createAPmethod();
		
		
        List<Account_Plan_Level__c> lstAccountPlanLevel = new list<Account_Plan_Level__c>();
        Account_Plan_Level__c apl1=new Account_Plan_Level__c(Business_Unit__c=bu1.id,Account_Plan_Level__c='Sub Business Unit',Company__c=c.id,Account_Plan_Record_Type__c=lstrt[0].id);
        lstAccountPlanLevel.add(apl1);
        Account_Plan_Level__c apl2=new Account_Plan_Level__c(Business_Unit__c=bu1.id,Account_Plan_Level__c='Business Unit Group',Company__c=c.id,Account_Plan_Record_Type__c=lstrt[0].id);
        lstAccountPlanLevel.add(apl2);
		insert lstAccountPlanLevel;

        controller.sortField = 'Business Unit Group';
        controller.doSortAps();
        controller.doSortAps();
        controller.sortField = 'Business Unit';
        controller.doSortAps();
        controller.doSortAps();
        controller.sortField = 'Sub Business Unit';
        controller.doSortAps();
        controller.doSortAps();
        controller.sortField = 'Start Date';
        controller.doSortAps();
        controller.doSortAps();
        controller.sortField = 'End Date';
        controller.doSortAps();
        controller.doSortAps();
        controller.sortField = 'Account Plan Level';
        controller.doSortAps();
        controller.doSortAps();
        controller.sortField = 'Owner';
        controller.doSortAps();
        controller.doSortAps();

		controller.getconfirmDupAP();

        controller = new ctrlExt_AccountPlanOverview(sct);
        controller.init();
        controller.selectedAPL = 'Sub Business Unit';
        controller.selectedBU = bu1.id;
        controller.selectedSBU = SBU1.id;
        controller.selectedSBU = '--None--';
        controller.selectedBU = '--None--';
        controller.selectedBU = bu1.id;
        controller.selectedSBU = SBU1.id;
		controller.getconfirmDupAP();
//		controller.createAPmethod();
        controller.getLstBUs();
        controller.getLstSBUs();
		
        controller = new ctrlExt_AccountPlanOverview(sct);
        controller.selectedAPL = 'Business Unit';
        controller.selectedBU = bu1.id;
        controller.selectedSBU = '--None--';
		controller.getconfirmDupAP();
//		controller.createAPmethod();
        controller.getLstBUs();
        controller.getLstSBUs();

        controller = new ctrlExt_AccountPlanOverview(sct);
        controller.selectedAPL = 'Business Unit Group';
        controller.selectedBU = bu1.id;
		controller.getconfirmDupAP();
//		controller.createAPmethod();
        controller.getLstBUs();
        controller.getLstSBUs();

        controller = new ctrlExt_AccountPlanOverview(sct);
        controller.init();

        controller.selectedAPL = 'Sub Business Unit';
        controller.selectedBU = bu1.id;
        controller.selectedSBU = SBU1.id;
        controller.selectedSBU = '--None--';
        controller.selectedBU = '--None--';

        controller.selectedAPL = 'Business Unit';
        controller.selectedBU = bu1.id;
        controller.selectedBU = '--None--';

        controller.selectedAPL = 'Business Unit Group';
        controller.selectedBU = bu1.id;
        controller.selectedBU = '--None--';

		//-BC - 20140312 - CR-2521 - STOP
        
        /*
        ap=new Account_Plan_2__c(Account__c=a.id);
        sct = new ApexPages.StandardController(ap);
        controller = new ctrlExt_AccountPlanOverview(sct);
        controller.selectedBU=bu1.id;
        controller.selectedAPL='Business Unit';
        controller.BUGroup=BUG.id;
        controller.init();
        controller.createAPmethod();
        
        ap=new Account_Plan_2__c(Account__c=a.id);
        sct = new ApexPages.StandardController(ap);
        controller = new ctrlExt_AccountPlanOverview(sct);
        
        controller.selectedBU=bu1.id;
        controller.selectedAPL='Sub Business Unit';
        controller.BUGroup=BUG.id;
        controller.init();
        controller.selectedSBU=sbu1.id;
        controller.createAPmethod();
        
        controller.selectedBU=bu1.id;
        controller.selectedAPL='Business Unit';
        controller.getconfirmDupAP();
        controller.selectedBU=bu1.id;
        controller.selectedAPL='Sub Business Unit';
        controller.selectedSBU=SBU1.id;
        controller.getconfirmDupAP();
        controller.selectedAPL='Business Unit Group';
        controller.mapidWithBU.put(bu1.id,bu1);
        controller.getconfirmDupAP();
        ap=new Account_Plan_2__c(Account__c=a.id);
        sct = new ApexPages.StandardController(ap);
        controller = new ctrlExt_AccountPlanOverview(sct);
        controller.redirect();
        bu1.Account_Plan_Start_End_Date__c=true;
        update bu1;
        
        controller.mapBuidWithAPStartAndEndDateCheck.put(string.valueof(bu1.id).substring(0,15),true);
        controller.getLstBUs();
        controller.getLstSBUs();
        controller.getLstAccPlanLevels();
        controller.init();
        controller.changeBU();
        controller.selectedAPL='Sub Business Unit';
        controller.selectedBU='--None--';
        controller.CreateAPMethod();
        controller.selectedBU='Business Unit';
        controller.selectedSBU='--None--';
        controller.CreateAPMethod();
        */
        /*
        Account a1 = new Account() ; 
        a1.Name = 'Test Acc Plan1'; 
        ap=new Account_Plan_2__c(Account__c=a1.id);
        sct = new ApexPages.StandardController(ap);
        controller = new ctrlExt_AccountPlanOverview(sct);
        controller.init();
        */
        Test.stopTest();

        }
    }
}