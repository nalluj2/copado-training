//---------------------------------------------------------------------------------------------------------------------------------------------------
//  Author      :   Bart Caelen
//  Date        :   23/07/2020
//  Description :   This is the APEX Test Class for the Batch/Scheduled APEX Class ba_ContractRepository_EmailReminder
//---------------------------------------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_ba_ContractRepository_EmailReminder {

    private static User oUser_Admin1;

    @isTest private static void createTestData() {

        // Create Users Data
        oUser_Admin1 = clsTestData_User.createUser_SystemAdministratorMDT('tstadm1', false);
        User oUser_Admin2 = clsTestData_User.createUser_SystemAdministratorMDT('tstadm2', false);
        User oUser_Admin3 = clsTestData_User.createUser_SystemAdministratorMDT('tstadm3', false);
        List<User> lstUser = new List<User>{oUser_Admin1, oUser_Admin2, oUser_Admin3};
        insert lstUser;

        System.runAs(oUser_Admin1){

            // Create Master Data
            clsTestData_MasterData.tCountryCode = 'BE';
            clsTestData_MasterData.createCountry();


            // Create Contract Repository Setting Data
            Contract_Repository_Setting__c oContractRepositorySetting = new Contract_Repository_Setting__c();
                oContractRepositorySetting.Country__c = clsTestData_MasterData.oMain_Country.Id;
                oContractRepositorySetting.Contract_Type__c = 'CareLink';
                oContractRepositorySetting.Email_Template_Name_1__c = 'Contract_xBU_End_Date_30';
                oContractRepositorySetting.Email_Template_Name_2__c = 'Contract_xBU_End_Date_60';
                oContractRepositorySetting.Email_Template_Name_3__c = 'Contract_xBU_End_Date_90';
                oContractRepositorySetting.BCC_Email_Address__c = 'test@medtronic.com.invalid';
                oContractRepositorySetting.Default_Access_Level__c = 'Read/Write';
                oContractRepositorySetting.Interface_Active__c = 'No';
                oContractRepositorySetting.Sales_Office__c = null;
            insert oContractRepositorySetting;


            // Create Account Data
            clsTestData_Account.iRecord_Account_SAPAccount = 3;
            clsTestData_Account.tCountry_Account_SAPAccount = 'BELGIUM';
            clsTestData_Account.createAccount_SAPAccount();


            // Create Contract Repository Data
            List<Contract_Repository__c> lstContractRepository = new List<Contract_Repository__c>();
            Contract_Repository__c oContractRepository1 = new Contract_Repository__c();
                oContractRepository1.Account__c = clsTestData_Account.lstAccount_SAPAccount[0].Id;
                oContractRepository1.Contract_Status__c = 'In Progress';
                oContractRepository1.Primary_Contract_Type__c = 'CareLink';
                oContractRepository1.Comments__c = 'A1';
                oContractRepository1.VDV_Deal_ID__c = 'B1';
                oContractRepository1.VDV_Deal_Type__c = 'Standard';
                oContractRepository1.Payment_Terms__c = 'Net 3 Days';
                oContractRepository1.Delivery_Terms__c = '24 Hours';
                oContractRepository1.Email_Reminder_Date_1__c = Date.today();
                oContractRepository1.Email_Reminder_Date_2__c = Date.today();
                oContractRepository1.Email_Reminder_Date_3__c = Date.today();
            lstContractRepository.add(oContractRepository1);
            Contract_Repository__c oContractRepository2 = new Contract_Repository__c();
                oContractRepository2.Account__c = clsTestData_Account.lstAccount_SAPAccount[1].Id;
                oContractRepository2.Contract_Status__c = 'In Progress';
                oContractRepository2.Primary_Contract_Type__c = 'CareLink';
                oContractRepository2.Comments__c = 'A1';
                oContractRepository2.VDV_Deal_ID__c = 'B1';
                oContractRepository2.VDV_Deal_Type__c = 'Standard';
                oContractRepository2.Payment_Terms__c = 'Net 3 Days';
                oContractRepository2.Delivery_Terms__c = '24 Hours';
                oContractRepository2.Email_Reminder_Date_1__c = Date.today();
                oContractRepository2.Email_Reminder_Date_2__c = Date.today();
                oContractRepository2.Email_Reminder_Date_3__c = Date.today();
            lstContractRepository.add(oContractRepository2);
            Contract_Repository__c oContractRepository3 = new Contract_Repository__c();
                oContractRepository3.Account__c = clsTestData_Account.lstAccount_SAPAccount[2].Id;
                oContractRepository3.Contract_Status__c = 'In Progress';
                oContractRepository3.Primary_Contract_Type__c = 'CareLink';
                oContractRepository3.Comments__c = 'A1';
                oContractRepository3.VDV_Deal_ID__c = 'B1';
                oContractRepository3.VDV_Deal_Type__c = 'Standard';
                oContractRepository3.Payment_Terms__c = 'Net 3 Days';
                oContractRepository3.Delivery_Terms__c = '24 Hours';
                oContractRepository3.Email_Reminder_Date_1__c = Date.today();
                oContractRepository3.Email_Reminder_Date_2__c = Date.today();
                oContractRepository3.Email_Reminder_Date_3__c = Date.today();
            lstContractRepository.add(oContractRepository3);
            insert lstContractRepository;


            // Create Contract Repository Team Data
            List<Contract_Repository_Team__c> lstContractRepositoryTeam = new List<Contract_Repository_Team__c>();
            Contract_Repository_Team__c oContractRepositoryTeam1 = new Contract_Repository_Team__c();
                oContractRepositoryTeam1.Contract_Repository__c = oContractRepository1.Id;
                oContractRepositoryTeam1.Auto_Aligned_with_Territory__c = false;
                oContractRepositoryTeam1.Contract_Repository_Owner__c = false;
                oContractRepositoryTeam1.Email_Reminder__c = '1;2;3';
                oContractRepositoryTeam1.Team_Member__c  = lstUser[1].Id;
            lstContractRepositoryTeam.add(oContractRepositoryTeam1);
            Contract_Repository_Team__c oContractRepositoryTeam2 = new Contract_Repository_Team__c();
                oContractRepositoryTeam2.Contract_Repository__c = oContractRepository1.Id;
                oContractRepositoryTeam2.Auto_Aligned_with_Territory__c = false;
                oContractRepositoryTeam2.Contract_Repository_Owner__c = false;
                oContractRepositoryTeam2.Email_Reminder__c = '1;2;3';
                oContractRepositoryTeam2.Team_Member__c  = lstUser[2].Id;
            lstContractRepositoryTeam.add(oContractRepositoryTeam2);
            insert lstContractRepositoryTeam;

        }

    }


    //----------------------------------------------------------------------------------------------------------------
    // TEST APEX Scheduling
    //----------------------------------------------------------------------------------------------------------------
    @isTest static void test_scheduling() {

        Test.startTest();

            //---------------------------------------
            // TEST SCHEDULING
            //---------------------------------------
            string tCRON_EXP = '0 0 0 3 9 ? 2099';
        
            string tJobId = System.schedule('ba_ContractRepository_EmailReminder_Test', tCRON_EXP, new ba_ContractRepository_EmailReminder());

            // Get the information from the CronTrigger API object
            CronTrigger oCronTrigger = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :tJobId];

            // Verify the expressions are the same
            System.assertEquals(tCRON_EXP, oCronTrigger.CronExpression);

            // Verify the job has not run
            System.assertEquals(0, oCronTrigger.TimesTriggered);

            // Verify the next time the job will run
            System.assertEquals('2099-09-03 00:00:00', String.valueOf(oCronTrigger.NextFireTime));
            //---------------------------------------

        Test.stopTest();

    }
    //----------------------------------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------------------------------
    // TEST Batch APEX
    //----------------------------------------------------------------------------------------------------------------
    @isTest static void test_ba_ContractRepository_EmailReminder() {


        createTestData();

        Test.startTest();

            System.runAs(oUser_Admin1){

                ba_ContractRepository_EmailReminder oBatch = new ba_ContractRepository_EmailReminder();
                Database.executebatch(oBatch, 200);

            }
            
        Test.stopTest();


    }
    //----------------------------------------------------------------------------------------------------------------
        

}
//---------------------------------------------------------------------------------------------------------------------------------------------------