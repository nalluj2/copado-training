global without sharing class ba_ContactDuplicatesPerCountry implements Database.Batchable<SObject>, Database.Stateful{
	
	global Integer noCountryMatch;
	global Map<String, Integer> contactsPerCountry;
	global Map<String, Integer> duplicatesPerCountry;
		
	public void setContactsPerCountry(Map<String, Integer> input){
		this.contactsPerCountry = input;
	}

	global Database.QueryLocator start(Database.BatchableContext BC){
		
		noCountryMatch = 0;
		duplicatesPerCountry = new Map<String, Integer>(); 
		
		for(String countryName : contactsPerCountry.keySet()){
      		duplicatesPerCountry.put(countryName, 0);
      	}	  
		
		//We get all the Matching results which are not False Duplicates, or merged already or at least one of the Contacts is missing 
		
		String query = 'Select TRIMDA__Country__c, TRIMDA__Contact1__r.MailingCountry, TRIMDA__Contact1__r.Account.Account_Country_vs__c, TRIMDA__Contact2__r.MailingCountry, TRIMDA__Contact2__r.Account.Account_Country_vs__c, TRIMDA__Country_1__c, TRIMDA__Country_2__c from TRIMDA__TRIMDA_MatchingResult__c '+
						'where TRIMDA__ObjectName__c IN (\'Acc+Contact-Acc+Contact\',\'Contact-Contact\') AND TRIMDA__False_Dupe__c = false AND TRIMDA__Is_Merged__c = \'No\' AND TRIMDA__Contact1__c != null AND TRIMDA__Contact2__c != null and TRIMDA__DupeAlertRef__c in (' + Trimda_Config__c.getvalues('TRIMDA Config').Included_Ids__c + ')';
 		 
		return Database.getQueryLocator(query);
	}
	 
	global void execute(Database.BatchableContext BC, List<SObject> scope){
		
		List<TRIMDA__TRIMDA_MatchingResult__c> matchings = (List<TRIMDA__TRIMDA_MatchingResult__c>) scope;
		
		for(TRIMDA__TRIMDA_MatchingResult__c match : matchings){
			
			String matchCountry;
			
			//We go in order to try to find a Country. We start with Matching Country values, and if nothing is found, we look into Contacts and Account country values
			if(match.TRIMDA__Country__c != null && contactsPerCountry.containsKey(match.TRIMDA__Country__c.toUpperCase())){
				matchCountry = match.TRIMDA__Country__c.toUpperCase();
			}else if(match.TRIMDA__Country_2__c != null && contactsPerCountry.containsKey(match.TRIMDA__Country_2__c.toUpperCase())){
				matchCountry = match.TRIMDA__Country_2__c.toUpperCase();
			}else if(match.TRIMDA__Country_1__c != null && contactsPerCountry.containsKey(match.TRIMDA__Country_1__c.toUpperCase())){
				matchCountry = match.TRIMDA__Country_1__c.toUpperCase();
			}else if(match.TRIMDA__Contact2__r.MailingCountry != null && contactsPerCountry.containsKey(match.TRIMDA__Contact2__r.MailingCountry.toUpperCase())){
				matchCountry = match.TRIMDA__Contact2__r.MailingCountry.toUpperCase();
			}else if(match.TRIMDA__Contact2__r.Account!=null && match.TRIMDA__Contact2__r.Account.Account_Country_vs__c != null && contactsPerCountry.containsKey(match.TRIMDA__Contact2__r.Account.Account_Country_vs__c.toUpperCase())){
				matchCountry = match.TRIMDA__Contact2__r.Account.Account_Country_vs__c.toUpperCase();
			}else if(match.TRIMDA__Contact1__r.MailingCountry != null && contactsPerCountry.containsKey(match.TRIMDA__Contact1__r.MailingCountry.toUpperCase())){
				matchCountry = match.TRIMDA__Contact1__r.MailingCountry.toUpperCase();
			}else if(match.TRIMDA__Contact1__r.Account!=null && match.TRIMDA__Contact1__r.Account.Account_Country_vs__c != null && contactsPerCountry.containsKey(match.TRIMDA__Contact1__r.Account.Account_Country_vs__c.toUpperCase())){
				matchCountry = match.TRIMDA__Contact1__r.Account.Account_Country_vs__c.toUpperCase();
			}
			
			if(matchCountry == null){
				noCountryMatch++;
			}else{
				Integer countryDuplicates = duplicatesPerCountry.get(matchCountry);
				countryDuplicates++;
				duplicatesPerCountry.put(matchCountry, countryDuplicates);
			}				 
		}
	}
	
	global void finish(Database.BatchableContext BC){
					
		List<Messaging.SingleEmailMessage> notifications = new List<Messaging.SingleEmailMessage>();		
		
		Map<String, List<Contact>> dStewards = getDataStewardContacts();
		
		EmailTemplate duplicatesTemplate = [select Id from EmailTemplate where DeveloperName='Duplicate_Contacts_Notification'];
		
		List<DIB_Country__c> countries = [Select Name, Number_of_Contacts__c, Duplicate_Contacts__c, Percent_of_duplicates__c from DIB_Country__c]; 
		
		for(DIB_Country__c country : countries){
      		
      		String countryName = country.Name.toUpperCase();
      		
      		Decimal contacts = contactsPerCountry.get(countryName);
			Decimal duplicates = duplicatesPerCountry.get(countryName);
			
			Decimal percentage;
			
			if(contacts > 0) percentage = (duplicates.divide(contacts, 4) * 100).setScale(2);
			else percentage = 0.00;
			
			country.Number_of_Contacts__c = contacts;
			country.Duplicate_Contacts__c = duplicates;
			country.Percent_of_duplicates__c = percentage;
						
			if(percentage > 0.5){
				
				List<Contact> countryStewards = dStewards.get(countryName);
				
				if(countryStewards != null){
					
					for(Contact countrySteward : countryStewards){
						
						Messaging.Singleemailmessage mail = new Messaging.Singleemailmessage();
		           
			            mail.setTargetObjectId(countrySteward.Id);
			            mail.setTemplateId(duplicatesTemplate.Id);           
			            mail.setUseSignature(false);
			            mail.setSaveAsActivity(false);
			            mail.setWhatId(country.Id);
			            mail.setSenderDisplayName(country.Name + ' duplicate Contacts');
			            
			            notifications.add(mail); 
					}
				}					
			}
      	}	
		
		update countries;
			
		Messaging.sendEmail(notifications);   
	}
	
	private Map<String, List<Contact>> getDataStewardContacts(){
		
		Map<String, Set<Id>> countryStewards = new Map<String, Set<Id>>();
		Set<Id> dStewardIds = new Set<Id>();
		
		for(Country_Data_Stewards__c countryConfig : Country_Data_Stewards__c.getAll().values()){
			
			Set<Id> stewardIds = new Set<Id>();
			
			for(String stewardId : countryConfig.Data_Steward_Ids__c.split(';')){	
				
				if(stewardId != null && stewardId.trim() != ''){
					stewardIds.add(Id.valueOf(stewardId.trim()));
					dStewardIds.add(Id.valueOf(stewardId.trim()));
				}
			}
			
			countryStewards.put(countryConfig.Name.toUpperCase(), stewardIds);
		}
		
		Map<Id, Contact> dStewardContacts = new Map<Id, Contact>();

		//-BC - 20150908 - CR-9250 - Updated the SOQL to exclude the MDT_Employee__c field which is updated to represent the MDT Employees and MDT Agent/Distributor records - START		
		for(Contact steward : [Select Id, SFDC_User__c from Contact where Contact_Active__c = true AND SFDC_User__c IN :dStewardIds AND Recordtype.DeveloperName = 'MDT_Employee']){
//		for(Contact steward : [Select Id, SFDC_User__c from Contact where MDT_Employee__c = true AND Contact_Active__c = true AND SFDC_User__c IN :dStewardIds]){
		//-BC - 20150908 - CR-9250 - Updated the SOQL to exclude the MDT_Employee__c field which is updated to represent the MDT Employees and MDT Agent/Distributor records - STOP
			dStewardContacts.put(steward.SFDC_User__c, steward);
		}
		
		Map<String, List<Contact>> result = new  Map<String, List<Contact>>();
		
		for(String country : countryStewards.keySet()){
						
			List<Contact> stewardContacts = new List<Contact>();
			
			for(Id stewardId : countryStewards.get(country)){
				
				Contact stewardContact = dStewardContacts.get(stewardId);
				
				if(stewardContact != null) stewardContacts.add(stewardContact);				
			}
			
			if(stewardContacts.size()>0){
				result.put(country, stewardContacts);
			}
		}
		
		return result;
	}
}