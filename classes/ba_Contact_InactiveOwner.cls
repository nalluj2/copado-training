/*
 * Author 		:	Bart Caelen
 * Date			:	20140724
 * Description	:	CR-2603 - This batch/scheduled Class will collect all Contacts with an Inactive Owner and it will update 
 *						the Contact Owner with the Account Owner but only if the Account Owner is Active
 */
global class ba_Contact_InactiveOwner implements Database.Batchable<sObject>, Schedulable {
	
	// Select all inactive contacts - we don't need to include any specific fields in the SOQL because 
    //  another SOQL will be executed in the processContactWithInactiveOwner method to be sure all necessary fields are selected
    String tQuery = 'SELECT Id FROM Contact WHERE Owner.IsActive = FALSE AND IsPersonAccount = FALSE';

	global void execute(SchedulableContext sc){
    	ba_Contact_InactiveOwner oBatch = new ba_Contact_InactiveOwner();
        Database.executebatch(oBatch, 200);        	
    } 

    global database.querylocator start(Database.BatchableContext bc){
    	return Database.getQueryLocator(tQuery);
    }

    global void execute(Database.BatchableContext bc, List<Contact> lstContact){
		bl_Contact.processContactWithInactiveOwner(lstContact);
    }
    
    global void finish(Database.BatchableContext bc){
    }
	
}