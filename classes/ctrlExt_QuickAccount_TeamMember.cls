public with sharing class ctrlExt_QuickAccount_TeamMember {
	
	public Account_Team_Member__c teamMember {get; set;}

	public Boolean isSaveError {get; set;}
	
	public ctrlExt_QuickAccount_TeamMember(ApexPages.StandardController sc){
				
		teamMember = new Account_Team_Member__c();		
		teamMember.Account__c = sc.getId();
	}
	
	public void save(){
		
		isSaveError = false;
		
		try{
			insert teamMember;
		}catch(Exception e){
			ApexPages.addMessages(e);
			isSaveError = true;
		}
	}
}