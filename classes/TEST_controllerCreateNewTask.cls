public class TEST_controllerCreateNewTask {     
    
    static Lead lead1;
    static Contact cont1;
    static Account acc1;
    
    static testMethod void unitTest() {  
    	setupTestData();   
        TEST_controllerCreateNewTask taskTest = new TEST_controllerCreateNewTask();
        taskTest.testControllerTask();
    }
    
    static testMethod void unitTest2() {     
        setupTestData();
        TEST_controllerCreateNewTask taskTest = new TEST_controllerCreateNewTask();
        taskTest.testControllerTask2();
    }

    static testMethod void unitTest3() {     
        setupTestData();
        TEST_controllerCreateNewTask taskTest = new TEST_controllerCreateNewTask();
        taskTest.testControllerTask3();
    }

    static testMethod void unitTest4() {     
        setupTestData();
        TEST_controllerCreateNewTask taskTest = new TEST_controllerCreateNewTask();
        taskTest.testControllerTask();
    }

    private static void setupTestData(){
        lead1 = new Lead();
        cont1 = new Contact();
        acc1 = new Account();
    
        acc1.Name = 'TEST_controllerCreateNewTask Test Coverage Account 1 ' ; 
        insert acc1 ;               
        
        cont1.LastName = 'TEST_controllerCreateNewTask Test Coverage' ;  
        cont1.FirstName = 'Test Contact' ;  
        cont1.AccountId = acc1.Id ;        
        cont1.Phone = '009569699'; 
        cont1.Email ='test@coverage.com';
        cont1.Contact_Department__c = 'Diabetes Adult'; 
        cont1.Contact_Primary_Specialty__c = 'ENT';
        cont1.Affiliation_To_Account__c = 'Employee';
        cont1.Primary_Job_Title_vs__c = 'Manager';
        cont1.Contact_Gender__c = 'Male'; 
        insert cont1 ;  
        
        Id Recordtype = [Select Id from Recordtype where DeveloperName = 'MITG_Lead' LIMIT 1].Id;
        
		clsTestData.createLeadData(false);
		Lead1 = clsTestData.oMain_Lead;
			Lead1.RecordTypeId = Recordtype;
			lead1.LastName = 'PAtrick';
			lead1.LastName ='LastName';
			lead1.Status ='Open';
			lead1.phone = '345432456';
			Lead1.Relation_to_Company_Picklist__c = 'Intern';
			Lead1.Lead_Specialty__c = 'Internist';
			Lead1.Department_Picklist__c = 'Administration';
			Lead1.Lead_Gender_Text__c = 'Male';
			lead1.company = 'TESTCOVERAGECOMPANY';
			lead1.email = 'lead@testcoverage.com';
        insert lead1;
    	
    }
    
    
    private void testControllerTask(){
        System.debug(' ################## ' + 'BEGIN TEST_controllerCreateNewTask' + ' ################');
                
        /* different tasks */
        
        // Task with a contact and an account : 
        Task tk1 = new Task();
        tk1.whoId = cont1.Id;
        tk1.whatId = acc1.Id ;
        tk1.OwnerId = Userinfo.getUserId(); 
        this.testTask(tk1, true, cont1, lead1, acc1); 
    }
                    
    private void testControllerTask2(){
        //Test Edit Task : 
        Task tk4 = new Task();
        tk4.whoId = cont1.Id ; 
        tk4.whatId = acc1.Id ;
        tk4.OwnerId = Userinfo.getUserId();
        tk4.status = 'Normal';
        tk4.IsReminderSet = true ; 
        DateTime dt = System.now();
        tk4.ReminderDateTime = dt;
        insert tk4 ; 
        this.testTask(tk4, false,cont1, lead1, acc1);
    }
        
    private void testControllerTask3(){    
        //Test Edit Task : 
        Task tk5 = new Task();
        tk5.whoId = cont1.Id ; 
        tk5.whatId = acc1.Id ;
        tk5.OwnerId = Userinfo.getUserId();
        tk5.IsReminderSet = false ;
        insert tk5 ;
        this.testTask(tk5, false,cont1, lead1, acc1);
    } 
        
    private void testControllerTask4(){
        Task tk6 = new Task();
        tk6.whoId = lead1.Id ;
        tk6.OwnerId = Userinfo.getUserId();
        tk6.IsReminderSet = true ;
        insert tk6 ; 
        this.testTask(tk6, false,cont1, lead1, acc1);
    }
        
    
    private void testTask(Task t1, Boolean isFirst, Contact cont1i, Lead lead1i, Account acc1i){
    /* Creation of an instance of the controller to be able to call     */  
        ApexPages.StandardController sct = new ApexPages.StandardController(t1); //set up the standardcontroller        
        controllerCreateNewTask  controller = new controllerCreateNewTask(sct);
                
        String currPage = '/apex/newTaskPage?id=' + t1.Id + 'sfdc.override=1' + '&close=1' ; 
                if (t1.whoId != null)   currPage += '&whoid=' +  t1.whoId  ;
                if (t1.whatId != null)  currPage += '&whatid='+ t1.whatId ;  
        PageReference pageRef = new PageReference(currPage);            
        Test.setCurrentPage(pageRef); 
        
        controller.getContactLeadDetails(t1.whoId);
        controller.getContactLeadDetails(lead1.Id);
        controller.getItems();
        controller.getselFromAccountNames();
        controller.getselReminderOptions();
        controller.getisAffAccDisabled();
        controller.listaccounts();
        controller.getContactLeadDetails(null);
        String temail           = controller.getcontactEmail();
        String tphone           = controller.getcontactPhone();
        String trelatedto       = controller.getrelatedto();
        String treminderList    = controller.getreminderList() ;
        String tstOption1       = controller.getstOption1();
        String tstOption2       = controller.getstOption2();
        controller.setremiderList('8:00');
        controller.setcontactEmail('test2@coverage.com');
        controller.setcontactPhone('056542346543');
        controller.setAffiliatedAccounts(cont1.Id);
        controller.setAffiliatedAccounts(null);
        controller.setrelatedTo(acc1.Id);
        controller.refreshAffiliatedAccounts();
        
        // do not send email 
        controller.sendNotificationEmailCheckBox = 'False'; 
         
        //Delete existing affiliations of the contact : 
        Affiliation__c[] affsRelatedtoContact  = [Select id from Affiliation__c where Affiliation_from_contact__c =:cont1.Id or Affiliation_to_Contact__c =:cont1i.Id];
        if (affsRelatedtoContact.size() > 0 ){
            delete affsRelatedtoContact ;
        } 
        // No affiliations linked : 
        controller.setAffiliatedAccounts(cont1i.Id);
        controller.setAffiliatedAccounts(lead1i.Id); 
         
        // If there is no WhoId or if it's a lead ;
        controller.getcontactLeadDetails(lead1i.Id);
        
        controller.saveTask();
        controller.savenewevent();
        controller.savenewtask();
        
        
        //Send email test (only test 1 time for limits):  
        if (isFirst == true){
            controller.sendEmail();
            // Save a task with a SendNotification email : 
            controller.sendNotificationEmailCheckBox = 'True'; 
            controller.saveTask();          
        }   
                    
        //Save a task with a reminder : 
        t1.IsReminderSet = true ; 
        controller.saveTask();
        
        controller.relatedto = controller.getstOption2() ;
        controller.manageDisabled(); 
        controller.rsave();             
        
        controller.relatedto = controller.getstOption1() ; 
        controller.accountsToList = Acc1i.Id ;
        controller.rsave();     
                
    }
}