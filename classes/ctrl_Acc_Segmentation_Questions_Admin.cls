public with sharing class ctrl_Acc_Segmentation_Questions_Admin {
    
    @AuraEnabled
    public static Map<String, List<ctrl_Account_Performance_Questions.PicklistValue>> getMainFilterOptions(String purpose, String fiscalYear) {
    	
    	Map<String, List<ctrl_Account_Performance_Questions.PicklistValue>> result = new Map<String, List<ctrl_Account_Performance_Questions.PicklistValue>>();
    	
    	UMD_Purpose__c selectedPurpose;
    	
    	//Fiscal Year Options
    	List<ctrl_Account_Performance_Questions.PicklistValue> fiscalYearOptions = new List<ctrl_Account_Performance_Questions.PicklistValue>();
    	
    	for(UMD_Purpose__c UMDPurpose : [Select Id, Fiscal_Year_VS__c, Status__c from UMD_Purpose__c where Segmentation_Qualification__c = :purpose ORDER BY Fiscal_Year_VS__c DESC]){
    		
    		fiscalYearOptions.add(new ctrl_Account_Performance_Questions.PicklistValue(UMDPurpose.Fiscal_Year_VS__c, UMDPurpose.Fiscal_Year_VS__c));
    		
    		if(fiscalYear == null && (selectedPurpose == null || UMDPurpose.Status__c == 'Open for input')) selectedPurpose = UMDPurpose;    		
    		else if(fiscalYear != null && UMDPurpose.Fiscal_Year_VS__c == fiscalYear) selectedPurpose = UMDPurpose;
    	}
    	
    	result.put('fiscalYearOptions', fiscalYearOptions);
    	
    	//Default or selected Fiscal Year
    	result.put('fiscalYear', new List<ctrl_Account_Performance_Questions.PicklistValue>{new ctrl_Account_Performance_Questions.PicklistValue(selectedPurpose.Fiscal_Year_VS__c, selectedPurpose.Fiscal_Year_VS__c)});
    	
    	List<ctrl_Account_Performance_Questions.PicklistValue> readOnlyFY = new List<ctrl_Account_Performance_Questions.PicklistValue>();
    	    	
    	for(AggregateResult record : [Select Fiscal_Year__c fy from Customer_Segmentation__c GROUP BY Fiscal_Year__c]){
    		
    		String fy = String.valueOf(record.get('fy'));    		
    		readOnlyFY.add(new ctrl_Account_Performance_Questions.PicklistValue(fy, fy));
    	}
    	
    	result.put('readOnlyFY', readOnlyFY);
    	
    	//Scope options
    	List<ctrl_Account_Performance_Questions.PicklistValue> scopeOptions = new List<ctrl_Account_Performance_Questions.PicklistValue>();
    	
    	String query;    	
    	if(purpose == 'Behavioral') query = 'SELECT Sub_Business_Unit__c scope from UMD_Input_Complete__c where UMD_Purpose__c = \'' + selectedPurpose.Id + '\' GROUP BY Sub_Business_Unit__c ORDER BY Sub_Business_Unit__c';
    	else query = 'SELECT Business_Unit_Group__c scope from UMD_Input_Complete__c where UMD_Purpose__c = \'' + selectedPurpose.Id + '\' GROUP BY Business_Unit_Group__c ORDER BY Business_Unit_Group__c';
    	
    	for(AggregateResult record : Database.query(query)){
    		
    		String scope = String.valueOf(record.get('scope'));    		
    		scopeOptions.add(new ctrl_Account_Performance_Questions.PicklistValue(scope, scope));
    	}
    	
    	result.put('scopeOptions', scopeOptions);
    	    	
    	//Default BUG for Strategic
    	if(purpose == 'Strategical'){
    		
    		List<User_Business_Unit__c> userPrimarySBU = [Select Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__r.Name from User_Business_Unit__c where User__c = :UserInfo.getUserId() AND Primary__c = true];
				
			if(userPrimarySBU.size() > 0){
				
				String defaultBUG = userPrimarySBU[0].Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__r.Name;
				
				result.put('defaultBUG', new List<ctrl_Account_Performance_Questions.PicklistValue>{new ctrl_Account_Performance_Questions.PicklistValue(defaultBUG, defaultBUG)});
			}
    	}
    	    	
    	return result;
    }
    
    @AuraEnabled
    public static Map<String, List<ctrl_Account_Performance_Questions.PicklistValue>> getSecondaryFilterOptions(String purpose, String fiscalYear, String scope, String segment, String accountCountry) {
    	
    	Map<String, List<ctrl_Account_Performance_Questions.PicklistValue>> result = new Map<String, List<ctrl_Account_Performance_Questions.PicklistValue>>();
    	
    	UMD_Purpose__c selectedPurpose = [Select Id from UMD_Purpose__c where Segmentation_Qualification__c = :purpose AND Fiscal_Year_VS__c = :fiscalYear];
    	
    	String purposeClause = ' UMD_Purpose__c = \'' + selectedPurpose.Id + '\' ';
    	
    	String scopeClause = ' AND ';
    	if(purpose == 'Behavioral') scopeClause += 'Sub_Business_Unit__c = \'' + scope + '\' ';
    	else scopeClause += 'Business_Unit_Group__c = \'' + scope + '\' ';
    	
    	String segmentClause = '';
    	if(segment != null && segment != ''){
    		
    		if(segment == '-') segment = 'Not Segmented';
    		
    		segmentClause += ' AND ';
    		
    		if(segment == 'Not Segmented'){
				
				if(purpose == 'Behavioral') segmentClause += '(Current_Segmentation__r.Behavioral_Segment__c = null OR ';
				else segmentClause += '(Current_Segmentation__r.Strategical_Segment__c = null OR ';					
			}
			
			if(purpose == 'Behavioral') segmentClause += 'Current_Segmentation__r.Behavioral_Segment__c = \'' + segment + '\' ';				
			else segmentClause += 'Current_Segmentation__r.Strategical_Segment__c = \'' + segment + '\' ';				
			
			if(segment == 'Not Segmented') segmentClause += ') ';    		
    	}
    	
    	String countryClause = '';
    	if(accountCountry != null && accountCountry != '') countryClause = ' AND Account__r.BillingCountry = \'' + accountCountry + '\' ';
    	    	    	
    	//Segment options
    	List<ctrl_Account_Performance_Questions.PicklistValue> segmentOptions = new List<ctrl_Account_Performance_Questions.PicklistValue>();
    	segmentOptions.add(new ctrl_Account_Performance_Questions.PicklistValue('', 'All'));
    	
    	String segmentQuery = 'SELECT Current_Segmentation__r.';
    	if(purpose == 'Behavioral') segmentQuery += 'Behavioral_Segment__c';
    	else segmentQuery += 'Strategical_Segment__c';
    	
    	segmentQuery += ' segment from UMD_Input_Complete__c where ' + purposeClause + scopeClause;
    	    	    	
    	segmentQuery += ' GROUP BY Current_Segmentation__r.';
    	if(purpose == 'Behavioral') segmentQuery += 'Behavioral_Segment__c';
    	else segmentQuery += 'Strategical_Segment__c';
    	    	
    	segmentQuery += ' ORDER BY Current_Segmentation__r.';
    	if(purpose == 'Behavioral') segmentQuery += 'Behavioral_Segment__c';
    	else segmentQuery += 'Strategical_Segment__c';
    	
    	segmentQuery += ' NULLS FIRST';
    	
    	for(AggregateResult record : Database.query(segmentQuery)){
    		
    		String segmentOption = String.valueOf(record.get('segment'));    		
    		if(segmentOption == null) segmentOption = 'Not Segmented';
    		
    		if(segmentOption == 'Not Segmented'){
    			 
    			 if(segmentOptions.size() == 1) segmentOptions.add(new ctrl_Account_Performance_Questions.PicklistValue('-', 'Not Segmented'));
    			 
    		}else segmentOptions.add(new ctrl_Account_Performance_Questions.PicklistValue(segmentOption, segmentOption));
    	}
    	
    	result.put('segmentOptions', segmentOptions);
    	
    	//Country options
    	List<ctrl_Account_Performance_Questions.PicklistValue> countryOptions = new List<ctrl_Account_Performance_Questions.PicklistValue>();
    	countryOptions.add(new ctrl_Account_Performance_Questions.PicklistValue('', 'All'));
    	
    	String countryQuery = 'SELECT Account__r.BillingCountry country from UMD_Input_Complete__c where ' + purposeClause + scopeClause + segmentClause + ' GROUP BY Account__r.BillingCountry ORDER BY Account__r.BillingCountry';
    	
    	for(AggregateResult record : Database.query(countryQuery)){
    		
    		String country = String.valueOf(record.get('country'));    		
    		countryOptions.add(new ctrl_Account_Performance_Questions.PicklistValue(country, country));
    	}
    	
    	result.put('countryOptions', countryOptions);
    	    	
    	return result;
    }
    
    @AuraEnabled
    public static Integer getRecordCount(String purpose, String fiscalYear, String scope, String accoundId, String accountCountry, String segmentation, String assignedTo, String status) {
    	
    	List<UMD_Purpose__c> questionPurposes = [Select Id, Fiscal_Year_VS__c from UMD_Purpose__c where Segmentation_Qualification__c = :purpose AND Fiscal_Year_VS__c = :fiscalYear];
    	
    	String query = 'SELECT COUNT() from UMD_Input_Complete__c ';				
		query += getQueryClause(true, purpose, questionPurposes[0].Id, scope, accoundId, accountCountry, segmentation, assignedTo, status);		
    	System.debug(query);
    	return Integer.valueOf(Database.countQuery(query));
    }
    
    @AuraEnabled
    public static ctrl_Account_Performance_Questions.Formulaire getUserFormulaire(String purpose, String fiscalYear, String scope, String accoundId, String accountCountry, String segmentation, String assignedTo, String status) {
    	
    	try{
    	
	    	ctrl_Account_Performance_Questions.Formulaire result = new ctrl_Account_Performance_Questions.Formulaire();    	
	    	result.accountInputs = new List<ctrl_Account_Performance_Questions.FormulaireAccountInput>();
	    	result.questions = new List<ctrl_Account_Performance_Questions.FormulaireQuestion>();
	    	
	    	List<UMD_Purpose__c> questionPurposes = [Select Id, Fiscal_Year_VS__c from UMD_Purpose__c where Segmentation_Qualification__c = :purpose AND Fiscal_Year_VS__c = :fiscalYear];
	    	
	    	if(questionPurposes.size() == 1){
	    		
	    		UMD_Purpose__c questionPurpose = questionPurposes[0];
	    			    		
	    		List<UMD_Question__c> questions = new List<UMD_Question__c>();
	    		Set<Id> questionIds = new Set<Id>();
	    		
	    		for(UMD_Purpose_Question__c purposeQuestion : [Select Sub_Business_Unit__c, Business_Unit_Group__c, UMD_Question__c, UMD_Question__r.Answers__c, UMD_Question__r.Helptext_Rich__c, UMD_Question__r.Max_Value__c, UMD_Question__r.Min_Value__c, UMD_Question__r.Question__c, UMD_Question__r.Type__c 
												from UMD_Purpose_Question__c 
												Where UMD_Purpose__c = :questionPurpose.Id AND UMD_Question__r.Source_of_answer__c = 'User' ORDER BY Sequence__c ASC]){
					
					if(purposeQuestion.Business_Unit_Group__c !=null && purposeQuestion.Business_Unit_Group__c.contains(scope) == false) continue;					
					if(purposeQuestion.Sub_Business_Unit__c != null && purposeQuestion.Sub_Business_Unit__c.contains(scope) == false) continue;
						
					//If the Question was not added already				
					if(questionIds.add(purposeQuestion.UMD_Question__c)){
										
						result.questions.add(new ctrl_Account_Performance_Questions.FormulaireQuestion(purposeQuestion.UMD_Question__r));
						questions.add(purposeQuestion.UMD_Question__r);
					}
				}
	    		    		
	    		String query = 'SELECT Id, Unique_Key__c, Account__c, Account__r.Name, Account__r.BillingCity , Account__r.SAP_ID__c, CreatedBy.Name, CreatedDate, Complete__c, Approved__c, Assigned_To__c, Assigned_To__r.Name, Approved_2__c, Approver_1__c, Approver_1__r.Name, Approver_2__c,  Approver_2__r.Name, UMD_Purpose__r.Fiscal_Year_VS__c, Sub_Business_Unit__c, Business_Unit_Group__c,';
				query += ' Current_Segmentation__r.Behavioral_Segment__c, Current_Segmentation__r.Strategical_Segment__c,';    		
				query += ' (SELECT Field, CreatedBy.Name, CreatedDate FROM Histories ORDER BY CreatedDate DESC)';
				query += ' from UMD_Input_Complete__c ';				
				query += getQueryClause(true, purpose, questionPurpose.Id, scope, accoundId, accountCountry, segmentation, assignedTo, status);				
				query += ' ORDER BY Account__r.Name, Account__c';
	    		
	    		String queryAnswer = 'SELECT Id, Answer__c, UMD_Input_Complete__c, UMD_Question__c, Unique_Key__c, CreatedBy.Name, CreatedDate, (SELECT Field, CreatedBy.Name, CreatedDate FROM Histories ORDER BY CreatedDate DESC) from UMD_Input__c ';
	    		queryAnswer += getQueryClause(false, purpose, questionPurpose.Id, scope, accoundId, accountCountry, segmentation, assignedTo, status);
	    		
	    		Map<Id, List<UMD_Input__c>> inputAnswerMap = new Map<Id, List<UMD_Input__c>>();
	    		System.debug(queryAnswer);
	    		for(UMD_Input__c inputAnswer : Database.query(queryAnswer)){
	    			
	    			List<UMD_Input__c> inputAnswers = inputAnswerMap.get(inputAnswer.UMD_Input_Complete__c);
					
					if(inputAnswers == null){
						
						inputAnswers = new List<UMD_Input__c>();
						inputAnswerMap.put(inputAnswer.UMD_Input_Complete__c, inputAnswers);
					}
					
					inputAnswers.add(inputAnswer);	
	    		}    		
	    			    		
	    		Integer fiscalYear_Int = Integer.valueOf(fiscalYear.split('FY')[1]);	    		
	    		String prevFiscalYear = 'FY' + (fiscalYear_Int - 1);
	    		
	    		Set<String> prevKeys = new Set<String>();
	    		Set<String> segmentationKeys = new Set<String>();	  
	    		
	    		System.debug(query);
	    		
	    		for(UMD_Input_Complete__c inputComplete : Database.query(query)){
	    													
					ctrl_Account_Performance_Questions.FormulaireAccountInput accInput = new ctrl_Account_Performance_Questions.FormulaireAccountInput();
					accInput.record = inputComplete;
					accInput.fiscalYear = fiscalYear_Int;
					
					if(inputComplete.Current_Segmentation__r != null){
						
						if(purpose == 'Behavioral') accInput.accountSegment = inputComplete.Current_Segmentation__r.Behavioral_Segment__c;
						else if(purpose == 'Strategical') accInput.accountSegment = inputComplete.Current_Segmentation__r.Strategical_Segment__c;
					}
					
					if(accInput.accountSegment == null || accInput.accountSegment == 'Not Segmented') accInput.accountSegment = '-';
					
					if(inputComplete.Complete__c == true) accInput.completedTS = ctrl_Account_Performance_Questions.getTimeStamp(inputComplete, 'Complete__c');
		    		else accInput.completedTS = inputComplete.Assigned_To__r.Name;	    		
		    		if(inputComplete.Approved__c == true) accInput.approved1TS = ctrl_Account_Performance_Questions.getTimeStamp(inputComplete, 'Approved__c');
		    		else if(inputComplete.Approver_1__c != null) accInput.approved1TS = inputComplete.Approver_1__r.Name;	    		
		    		if(inputComplete.Approved_2__c == true) accInput.approved2TS = ctrl_Account_Performance_Questions.getTimeStamp(inputComplete, 'Approved_2__c');
		    		else if(inputComplete.Approver_2__c != null) accInput.approved2TS = inputComplete.Approver_2__r.Name;
		    						
					Map<Id, UMD_Input__c> savedAnswers = new Map<Id, UMD_Input__c>();
					
					List<UMD_Input__c> inputAnswers = inputAnswerMap.get(inputComplete.Id);
					
					if(inputAnswers != null){
						for(UMD_Input__c savedInput : inputAnswers){
							
							savedAnswers.put(savedInput.UMD_Question__c, savedInput);
						}
					}
					
					accInput.accountAnswers = new List<ctrl_Account_Performance_Questions.FormulaireAccountInputAnswer>();
					
					for(UMD_Question__c question : questions){
						
						ctrl_Account_Performance_Questions.FormulaireAccountInputAnswer inputAnswer = new ctrl_Account_Performance_Questions.FormulaireAccountInputAnswer();
						
						UMD_Input__c input = savedAnswers.get(question.Id);
						
						if(input == null){
							
							input = new UMD_Input__c();
							input.UMD_Question__c = question.Id;
							input.UMD_Input_Complete__c = inputComplete.Id;
							input.Unique_Key__c = inputComplete.Id + ':' + question.Id;
							
						}else{
							
							inputAnswer.answerTS = ctrl_Account_Performance_Questions.getTimeStamp(input, 'Answer__c');
						}					
						
						inputAnswer.record = input;
						
						accInput.accountAnswers.add(inputAnswer);
					}
					
					result.accountInputs.add(accInput);
					
					prevKeys.add(inputComplete.Account__c + ':' + scope + ':' + prevFiscalYear);
					segmentationKeys.add(inputComplete.Unique_Key__c);
				}
				
	    		Map<String, UMD_Input__c> prevAnswers = new Map<String, UMD_Input__c>();
	    		    		    		    			
	    		String prevQuery = 'Select Answer__c, UMD_Question__c, UMD_Input_Complete__r.Account__c from UMD_Input__c where UMD_Input_Complete__r.Unique_Key__c IN :keys';
		    	    		
	    		for(SObject record : bl_Without_Sharing_Service.queryByKeys(prevQuery, prevKeys)){
	    			
	    			UMD_Input__c inputAnswer = (UMD_Input__c) record;
	    			prevAnswers.put(inputAnswer.UMD_Input_Complete__r.Account__c + ':' + inputAnswer.UMD_Question__c, inputAnswer);
	    		}
	    		
	    		Map<String, Account_Segmentation_Validation__c> segmentationMap = new Map<String, Account_Segmentation_Validation__c>();
				
				String segmentationQuery = 'Select Id, Unique_Key__c, ';
				if(purpose == 'Behavioral') segmentationQuery += 'Behavioral_Segment__c, Behavioral_Segment_Override__c';
				else  segmentationQuery += 'Strategical_Segment__c, Strategical_Segment_Override__c';
				segmentationQuery += ' from Account_Segmentation_Validation__c where Unique_Key__c IN :segmentationKeys';
				
				for(SObject record : Database.query(segmentationQuery)){
					
		    		Account_Segmentation_Validation__c currentSegmentation = (Account_Segmentation_Validation__c) record;
		    		if(purpose == 'Behavioral'){
		    			
		    			if(currentSegmentation.Behavioral_Segment__c == 'Not Segmented') currentSegmentation.Behavioral_Segment__c = '-';
		    			if(currentSegmentation.Behavioral_Segment_Override__c == null) currentSegmentation.Behavioral_Segment_Override__c = currentSegmentation.Behavioral_Segment__c;
		    			
		    		}else{
		    			
		    			if(currentSegmentation.Strategical_Segment__c == 'Not Segmented') currentSegmentation.Strategical_Segment__c = '-';
		    			if(currentSegmentation.Strategical_Segment_Override__c == null) currentSegmentation.Strategical_Segment_Override__c = currentSegmentation.Strategical_Segment__c;
		    		}
		    		
		    		segmentationMap.put(currentSegmentation.Unique_Key__c, currentSegmentation);
				}	
				
	    		for(ctrl_Account_Performance_Questions.FormulaireAccountInput accInput : result.accountInputs){
	    			
	    			accInput.prevAccountAnswers = new List<ctrl_Account_Performance_Questions.FormulaireAccountInputPrevAnswer>();
	    			Id accountId = accInput.record.Account__c;
	    			    			
	    			for(UMD_Question__c question : questions){
						
						ctrl_Account_Performance_Questions.FormulaireAccountInputPrevAnswer inputAnswer = new ctrl_Account_Performance_Questions.FormulaireAccountInputPrevAnswer();
						inputAnswer.questionType = question.Type__c;
						
						UMD_Input__c input = prevAnswers.get(accountId + ':' + question.Id);
						
						if(input != null){
							
							inputAnswer.value = input.Answer__c;
							
							if(question.Type__c == 'Picklist (single value)' || question.Type__c == 'Yes/No'){
								
								inputAnswer.formattedValue = input.Answer__c;
								
							}else if(question.Type__c == 'Number'){
								
								inputAnswer.formattedValue = input.Answer__c;
								
							}else if(question.Type__c == 'Picklist (multiple value)'){
								
								inputAnswer.formattedValue = '';
								
								for(String answerValue : input.Answer__c.split(';')){
									
									inputAnswer.formattedValue += '- ' + answerValue + '<br/>';	
								}							
							}										
						}					 
						
						accInput.prevAccountAnswers.add(inputAnswer);
					}
					
					//Current Segmentation
	    			Account_Segmentation_Validation__c currentSegment = segmentationMap.get(accInput.record.Unique_Key__c);
	    			if(currentSegment == null){
	    				currentSegment = new Account_Segmentation_Validation__c();
	    				if(purpose == 'Behavioral') currentSegment.Behavioral_Segment_Override__c = '-';
	    				else currentSegment.Strategical_Segment_Override__c = '-';
	    			}
	    			accInput.currentSegmentation = currentSegment;
	    		}			
	    	}
	    	
	    	return result;
    	
    	}catch(Exception e){
    		
    		System.debug(e.getMessage() + ' | ' + e.getStackTraceString());
    		AuraHandledException auraEx = new AuraHandledException(e.getMessage()); 
    		if(test.isRunningTest()) auraEx.setMessage(e.getMessage());
    		throw auraEx;
    	}
    }
    
    private static String getQueryClause(Boolean isInput, String purpose, Id questionPurposeId, String scope, String accoundId, String accountCountry, String segmentation, String assignedTo, String status){
    	
    	String query = '';
    	
    	if(isInput == true) query += ' where UMD_Purpose__c = \'' + questionPurposeId + '\' ';
		else query += ' where UMD_Input_Complete__r.UMD_Purpose__c = \'' + questionPurposeId + '\' ';
		
		if(scope != null && scope != ''){
		
			if(purpose == 'Behavioral'){
				    			
				if(isInput == true) query += ' AND Sub_Business_Unit__c = \'' + scope + '\'';
				else query += ' AND UMD_Input_Complete__r.Sub_Business_Unit__c = \'' + scope + '\'';
				
			}else if(purpose == 'Strategical'){
				
				if(isInput == true) query += ' AND Business_Unit_Group__c = \'' + scope + '\'';
				else query += ' AND UMD_Input_Complete__r.Business_Unit_Group__c = \'' + scope + '\'';
			}		
		}
		
		if(accoundId != null && accoundId != ''){
			
			if(isInput == true) query += ' AND Account__c = \'' + accoundId + '\' ';
			else query += ' AND UMD_Input_Complete__r.Account__c = \'' + accoundId + '\' ';
			
		}
		
		if(accountCountry != null && accountCountry != ''){
			
			if(isInput == true) query += ' AND Account__r.BillingCountry = \'' + accountCountry + '\' ';
			else query += ' AND UMD_Input_Complete__r.Account__r.BillingCountry = \'' + accountCountry + '\' ';
			
		}
		
		if(segmentation != null && segmentation != ''){
			
			if(isInput == true) query += ' AND ';
			else query += ' AND ';
			
			if(segmentation == '-') segmentation = 'Not Segmented';
			
			if(segmentation == 'Not Segmented'){
				
				if(purpose == 'Behavioral'){
					
					if(isInput == true) query += '(Current_Segmentation__r.Behavioral_Segment__c = null OR ';
					else query += '(UMD_Input_Complete__r.Current_Segmentation__r.Behavioral_Segment__c = null OR ';
					
				}else{
					
					if(isInput == true) query += '(Current_Segmentation__r.Strategical_Segment__c = null OR ';
					else query += '(UMD_Input_Complete__r.Current_Segmentation__r.Strategical_Segment__c = null OR ';
				}
			}
			
			if(purpose == 'Behavioral'){
				
				if(isInput == true) query += 'Current_Segmentation__r.Behavioral_Segment__c = \'' + segmentation + '\' ';
				else query += 'UMD_Input_Complete__r.Current_Segmentation__r.Behavioral_Segment__c = \'' + segmentation + '\' ';
				
			}else{
				
				if(isInput == true) query += 'Current_Segmentation__r.Strategical_Segment__c = \'' + segmentation + '\' ';
				else query += 'UMD_Input_Complete__r.Current_Segmentation__r.Strategical_Segment__c = \'' + segmentation + '\' ';
			}
			
			if(segmentation == 'Not Segmented'){
				
				if(isInput == true) query += ')';
				else query += ')';
			}
		}
		
		if(assignedTo != null && assignedTo != ''){
			
			if(isInput == true) query += ' AND (Assigned_To__c = \'' + assignedTo + '\' OR Approver_1__c = \'' + assignedTo + '\' OR Approver_2__c = \'' + assignedTo + '\')';
			else query += ' AND (UMD_Input_Complete__r.Assigned_To__c = \'' + assignedTo + '\' OR UMD_Input_Complete__r.Approver_1__c = \'' + assignedTo + '\' OR UMD_Input_Complete__r.Approver_2__c = \'' + assignedTo + '\')';
		}
				
		if(status != null && status != ''){
		
			if(status == 'Open'){
				    			
				if(isInput == true) query += ' AND Complete__c = false';
				else query += ' AND UMD_Input_Complete__r.Complete__c = false';
				
			}else if(status == 'Completed'){
				
				if(isInput == true) query += ' AND Complete__c = true ';
				else query += ' AND UMD_Input_Complete__r.Complete__c = true';
				
			}else if(status == 'Approved'){
				
				if(isInput == true) query += ' AND Complete__c = true AND (Approved__c = true OR Approver_1__c = null) AND (Approved_2__c = true OR Approver_2__c = null)';
				else query += ' AND UMD_Input_Complete__r.Complete__c = true AND (UMD_Input_Complete__r.Approved__c = true OR UMD_Input_Complete__r.Approver_1__c = null) AND (UMD_Input_Complete__r.Approved_2__c = true OR UMD_Input_Complete__r.Approver_2__c = null)';				
			}		
		}	
		
		return query;
    }   
    
    @AuraEnabled
    public static List<ctrl_Account_Performance_Questions.FormulaireAccountInput> saveUserFormulaire(List<ctrl_Account_Performance_Questions.FormulaireAccountInput> formulaire) {
    	
    	return ctrl_Account_Performance_Questions.saveUserFormulaire(formulaire);
    	
    }
}