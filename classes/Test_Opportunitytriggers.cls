@isTest(seeAlldata=true)
    Private class Test_Opportunitytriggers 
    {    
        static testMethod void myUnitTest() 
        {   //Insert Company
             Company__c cmpny = new Company__c();
             cmpny.name='TestMedProd';
             cmpny.CurrencyIsoCode = 'EUR';
             cmpny.Current_day_in_Q1__c=56;
             cmpny.Current_day_in_Q2__c=34; 
             cmpny.Current_day_in_Q3__c=5; 
             cmpny.Current_day_in_Q4__c= 0;   
             cmpny.Current_day_in_year__c =200;
             cmpny.Days_in_Q1__c=56;  
             cmpny.Days_in_Q2__c=34;
             cmpny.Days_in_Q3__c=13;
             cmpny.Days_in_Q4__c=22;
             cmpny.Days_in_year__c =250;
             cmpny.Company_Code_Text__c = 'T27';
             insert cmpny;
             Business_Unit__c BU1 = new Business_Unit__c();
            BU1.Name='TestBUMedProd';
            BU1.Company__c = cmpny.Id;
            Insert BU1;

            Sub_Business_Units__c SBU = new Sub_Business_Units__c();
            SBU.Name='TestSBUMedProd';
            SBU.Business_Unit__c = BU1.Id;
            Insert SBU;         
    
            Therapy_Group__c TG=new Therapy_Group__c();
            TG.Name='TestTGMedProd';
            TG.Sub_Business_Unit__c = SBU.id;
            TG.Company__c = cmpny.Id;
            insert TG;    
            
            Therapy__c th = new Therapy__c();
            th.Name = 'th11';
            th.Therapy_Group__c = TG.Id;
            th.Sub_Business_Unit__c = SBU.id;
            th.Business_Unit__c = BU1.id;
            th.Therapy_Name_Hidden__c = 'th11'; 
            insert th;
            RecordType oppRecType = clsUtil.getRecordTypeByDevName('Opportunity', 'Non_Capital_Opportunity');
            List<Account> lstAccount=new list<Account>();
            Account testAc1 = new Account() ;
            testAc1.Name = 'Test Account Name1';  
            testAc1.RecordTypeId = FinalConstants.sapAccountRecordTypeId ;
            testAc1.Account_Country_vs__c='BELGIUM';
            lstAccount.add(testAc1);
            Account testAc2 = new Account() ;
            testAc2.Name = 'Test Account Name2';  
            testAc2.RecordTypeId = FinalConstants.sapAccountRecordTypeId ;
            testAc2.Account_Country_vs__c='BELGIUM';
            lstAccount.add(testAc2);
            insert lstAccount;
            
            Product2 prd = new Product2(Name = 'Unit Test Product');
            insert prd;
            
            Opportunity_Bundle__c opb=new Opportunity_Bundle__c(Account__c=testAc1.id);
            insert opb;
            List<opportunity> lstOp=new list<opportunity>();
            opportunity op=new opportunity(name='testopp',Deal_Category__c='Upgrade',Opportunity_Bundle__c=opb.id,CloseDate=system.today()+2,stagename='Qualification',accountid=testAc1.id,ForecastCategoryName='Pipeline',CurrencyIsoCode='EUR',type='Service Contract');
            lstOp.add(op);
            Pricebook2 pb2=new  Pricebook2(name='testPB');
            insert pb2;
            opportunity op1=new opportunity(name='testopp1',Deal_Category__c='Upgrade',recordtypeid=oppRectype.id,Opportunity_Bundle__c=opb.id,CloseDate=system.today()+2,stagename='Qualification',accountid=testAc1.id,ForecastCategoryName='Pipeline',CurrencyIsoCode='EUR',type='Service Contract',pricebook2id=pb2.id);
            lstOp.add(op1);
            opportunity op2=new opportunity(name='testopp2',Deal_Category__c='Upgrade',Opportunity_Bundle__c=opb.id,CloseDate=system.today()+2,stagename='Qualification',accountid=testAc1.id,ForecastCategoryName='Pipeline',CurrencyIsoCode='EUR',type='Service Contract');
            lstOp.add(op2);
            //insert lstOp;
            //delete op;
            Product_Group__c pgrp=new Product_Group__c(Therapy_ID__c=Th.id);
            insert pgrp;
            product2 p=new product2(name='testPr',isActive=true,Product_Group__c=pgrp.id);
            insert p;
            //Pricebook2 pb=new  Pricebook2(name='testPB');
            //insert pb;
            Pricebook2 pb= [select id from Pricebook2 where IsStandard = true limit 1];
            //Opportunity existingOpp=[select id,Pricebook2Id from Opportunity where id=:op1.id];
            PricebookEntry pbe=new PricebookEntry(Pricebook2Id=pb.id,Product2Id=p.id,UseStandardPrice=false,unitPrice=100,isActive=true,CurrencyIsoCode='EUR');
            insert pbe;
            //PricebookEntry pbe2=new PricebookEntry(Pricebook2Id=existingOpp.pricebook2id,Product2Id=p.id,UseStandardPrice=true,unitPrice=100,isActive=true );
            //insert pbe2;
            /*
            List<OpportunityLineItem> lstopl=new List<OpportunityLineItem>();
            OpportunityLineItem opl=new OpportunityLineItem(PricebookEntryid=pbe2.id,Opportunityid=op1.id,quantity=5,unitprice=100);
            lstOpl.add(opl);
            OpportunityLineItem opl1=new OpportunityLineItem(PricebookEntryid=pbe2.id,Opportunityid=op1.id,quantity=5,unitprice=100);
            lstOpl.add(opl1);
            OpportunityLineItem opl2=new OpportunityLineItem(PricebookEntryid=pbe2.id,Opportunityid=op1.id,quantity=5,unitprice=100);
            lstOpl.add(opl2);
            insert lstOpl;
            */
            
            //update opl;
            //delete opl;
            
            //cl_OpportunityTasks clop=new cl_OpportunityTasks();
            cl_OpportunityTasks.CreatePendingTasks(lstOp);
            
        }
    }