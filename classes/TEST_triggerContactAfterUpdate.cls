/**
 * Creation Date :  20090218
 * Description :    Test Coverage of the trigger 'ContactAfterUpdate' (for the Contact Object).
 *                  (To facilitate the development of robust, error-free code, Apex Code requires the creation and execution of unit tests.
 *                  Unit tests are comprised of test methods and classes that verify whether a particular piece of code is working properly.)
 * Author :         ABSI - MC
 */
@isTest
private class TEST_triggerContactAfterUpdate {
    
    private static testMethod void testUpdateMainAccount(){
                
        Account acc1 = new Account();
        acc1.Name = 'Test Account 1';
                
        Account acc2 = new Account();
        acc2.Name = 'Test Account 2';
        
        insert new List<Account>{acc1, acc2};
        
        Contact cont = new Contact();
        cont.LastName = 'Contact'; 
        cont.FirstName = 'Test';
        cont.AccountId = acc1.Id ; 
        cont.Contact_Department__c = 'Administration';       
        cont.Primary_Job_Title_vs__c = 'Head Nurse';
		cont.Job_Title_Description__c = 'TEST';
     	cont.Contact_Primary_Specialty__c = 'ENT';
     	cont.Affiliation_To_Account__c = 'Employee';
     	cont.Contact_Gender__c = 'Male';                               
        insert cont;  

        Test.startTest();
                       
        Affiliation__c contactAff = [Select  Affiliation_To_Account__c, Affiliation_Primary__c, Affiliation_End_Date__c  from Affiliation__c where Affiliation_From_Contact__c  =: cont.Id] ;
                
        System.assert(contactAff.Affiliation_End_Date__c == null);
        System.assert(contactAff.Affiliation_Primary__c == true);
        System.assert(contactAff.Affiliation_To_Account__c == acc1.Id);
        
        /*
         * Description 2 : Test if the Account is changed : 
         */
        
        cont.AccountId = acc2.Id ;        
        update cont ;// it will create another Relationship !
        
        Affiliation__c contactAffNew = [Select  Affiliation_To_Account__c, Affiliation_Primary__c, Affiliation_End_Date__c  from Affiliation__c where Affiliation_From_Contact__c  =: cont.Id AND Affiliation_Primary__c = true] ;
                
        System.assert(contactAffNew.Affiliation_End_Date__c == null);
        System.assert(contactAffNew.Affiliation_To_Account__c == acc2.Id);
        
        contactAff = [Select  Affiliation_To_Account__c, Affiliation_Primary__c, Affiliation_End_Date__c  from Affiliation__c where Id =: contactAff.Id] ;
        
        System.assert(contactAff.Affiliation_End_Date__c != null);
        System.assert(contactAff.Affiliation_Primary__c == false);
        System.assert(contactAff.Affiliation_To_Account__c == acc1.Id);
        
        cont.AccountId = acc1.Id ;
        update cont ;  // it will not create another Relationship
        
        contactAff = [Select  Affiliation_To_Account__c, Affiliation_Primary__c, Affiliation_End_Date__c  from Affiliation__c where Id =: contactAff.Id] ;
        
        System.assert(contactAff.Affiliation_End_Date__c == null);
        System.assert(contactAff.Affiliation_Primary__c == true);
        System.assert(contactAff.Affiliation_To_Account__c == acc1.Id);
        
        contactAffNew = [Select  Affiliation_To_Account__c, Affiliation_Primary__c, Affiliation_End_Date__c  from Affiliation__c where Id =: contactAffNew.Id];
                
        System.assert(contactAffNew.Affiliation_End_Date__c != null);
        System.assert(contactAffNew.Affiliation_Primary__c == false);
        System.assert(contactAffNew.Affiliation_To_Account__c == acc2.Id);        
    }
    
    private static testmethod void testUpdateContactDetails(){
    	
    	Account acc = new Account();
        acc.Name = 'Test Account';               
        insert acc;
        
        Contact cont = new Contact();
        cont.LastName = 'Contact'; 
        cont.FirstName = 'Test';
        cont.AccountId = acc.Id ; 
        cont.Contact_Department__c = 'Administration';       
        cont.Primary_Job_Title_vs__c = 'Head Nurse';    	 
     	cont.Contact_Primary_Specialty__c = 'ENT';
     	cont.Affiliation_To_Account__c = 'Employee';
     	cont.Contact_Gender__c = 'Male';                               
        insert cont;  
    	    	               
        Affiliation__c contactAff = [Select  Affiliation_To_Account__c, Affiliation_Primary__c, Department__c, Affiliation_Position_In_Account__c, Affiliation_Type__c from Affiliation__c where Affiliation_From_Contact__c  =: cont.Id];
               
        System.assert(contactAff.Affiliation_Primary__c == true);
        System.assert(contactAff.Affiliation_To_Account__c == acc.Id);
        System.assert(contactAff.Department__c == 'Administration');
        System.assert(contactAff.Affiliation_Position_In_Account__c == 'Head Nurse');
        System.assert(contactAff.Affiliation_Type__c == 'Employee');
    	
    	cont.Contact_Department__c = 'Neurology';    	        
        update cont;
        
        contactAff = [Select Department__c from Affiliation__c where Affiliation_From_Contact__c =: cont.Id];
        System.assert(contactAff.Department__c == 'Neurology');            
    }
}