/** 
  Description - This test class covers the trigger tr_Contact_updateAddress
  Author - Priti Jaiswal
  Created Date - 22-01-2013  
*/


@isTest
public class Test_tr_Contact_updateAddress{
	
    private static testmethod void conatctUpdateAddress(){
        
        List<Account> accList=new List<Account>();
        
        Account acc1 = new Account();
        acc1.name = 'test Account 1';
        acc1.Account_Address_Line_1__c = 'add line1';
        acc1.Account_Address_Line_2__c = 'add line2';
        acc1.Account_Address_Line_3__c = 'add line3';        
        acc1.Account_City__c = 'Maastricht';
        acc1.Account_Country_vs__c = 'NETHERLANDS';        
        acc1.Account_Province__c = 'Limburg';
        acc1.Account_Postal_Code__c = '6400';                
                        
        Account acc2=new Account();
        acc2.name='test Account 2';
        acc2.Account_Address_Line_1__c = 'add line1';
        acc2.Account_Address_Line_2__c = 'add line2';
        acc2.Account_Address_Line_3__c = 'add line3';        
        acc2.Account_City__c = 'Heerlen';
        acc2.Account_Country_vs__c = 'NETHERLANDS';        
        acc2.Account_Province__c = 'Limburg';
        acc2.Account_Postal_Code__c = '6411';
        accList.add(acc2);
        
        insert new List<Account>{acc1, acc2};        
        
        Contact con = new Contact();
        con.Accountid = acc1.id;
        con.LastName = 'test Contact';
        con.FirstName = 'TEST';
        con.Contact_Department__c = 'Diabetes Adult';
        con.Contact_Primary_Specialty__c = 'ENT'; 
        con.Affiliation_To_Account__c = 'Employee';
        con.Primary_Job_Title_vs__c = 'Manager';
        con.Contact_Gender__c = 'Male';
        con.MailingStreet = acc1.Account_Address_Line_1__c+ acc1.Account_Address_Line_2__c+acc1.Account_Address_Line_3__c;
        con.MailingCity = acc1.Account_City__c;
        con.MailingCountry = acc1.Account_Country_vs__c;
        con.MailingPostalCode = acc1.Account_Postal_Code__c;
        con.MailingState = acc1.Account_Province__c;
        insert con; 
        
        con = [Select MailingCity, MailingPostalCode from Contact where Id = :con.Id];
        System.assert(con.MailingCity == 'Maastricht');
        System.assert(con.MailingPostalCode == '6400');
        
        con.AccountId = acc2.id;
        update con;
        
        con = [Select MailingCity, MailingPostalCode from Contact where Id = :con.Id];
        System.assert(con.MailingCity == 'Heerlen');
        System.assert(con.MailingPostalCode == '6411');
    }
    
    private static testmethod void conatctNoUpdateAddress(){
        
        List<Account> accList=new List<Account>();
        
        Account acc1 = new Account();
        acc1.name = 'test Account 1';
        acc1.Account_Address_Line_1__c = 'add line1';
        acc1.Account_Address_Line_2__c = 'add line2';
        acc1.Account_Address_Line_3__c = 'add line3';        
        acc1.Account_City__c = 'Maastricht';
        acc1.Account_Country_vs__c = 'NETHERLANDS';        
        acc1.Account_Province__c = 'Limburg';
        acc1.Account_Postal_Code__c = '6400';                
                        
        Account acc2=new Account();
        acc2.name='test Account 2';
        acc2.Account_Address_Line_1__c = 'add line1';
        acc2.Account_Address_Line_2__c = 'add line2';
        acc2.Account_Address_Line_3__c = 'add line3';        
        acc2.Account_City__c = 'Heerlen';
        acc2.Account_Country_vs__c = 'NETHERLANDS';        
        acc2.Account_Province__c = 'Limburg';
        acc2.Account_Postal_Code__c = '6411';
        accList.add(acc2);
        
        insert new List<Account>{acc1, acc2};        
        
        Contact con = new Contact();
        con.Accountid = acc1.id;
        con.LastName = 'test Contact';
        con.FirstName = 'TEST';
        con.Contact_Department__c = 'Diabetes Adult';
        con.Contact_Primary_Specialty__c = 'ENT'; 
        con.Affiliation_To_Account__c = 'Employee';
        con.Primary_Job_Title_vs__c = 'Manager';
        con.Contact_Gender__c = 'Male';
        con.MailingStreet = acc1.Account_Address_Line_1__c+ acc1.Account_Address_Line_2__c+acc1.Account_Address_Line_3__c;
        con.MailingCity = acc1.Account_City__c;
        con.MailingCountry = acc1.Account_Country_vs__c;
        con.MailingPostalCode = acc1.Account_Postal_Code__c;
        con.MailingState = acc1.Account_Province__c;
        insert con; 
        
        con = [Select MailingCity, MailingPostalCode from Contact where Id = :con.Id];
        System.assert(con.MailingCity == 'Maastricht');
        System.assert(con.MailingPostalCode == '6400');
        
        con.MailingStreet = 'Endepolsdomein 5';
        update con;
        
        con.AccountId = acc2.id;
        update con;
        
        con = [Select MailingCity, MailingPostalCode from Contact where Id = :con.Id];
        System.assert(con.MailingCity == 'Maastricht');
        System.assert(con.MailingPostalCode == '6400');
    }    
}