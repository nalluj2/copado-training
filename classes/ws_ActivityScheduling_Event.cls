@RestResource(urlMapping='/ActivitySchedulingEventService/*')
global with sharing class ws_ActivityScheduling_Event {

	@HttpPost
    global static void doUpsert() {
 		
		try{
			
			Event activitySchedulingEvent = (Event) System.Json.deserialize(RestContext.request.requestBody.toString(), Event.class);
			
			bl_Case_Trigger.runningSchedulingApp = true;
			
			if(activitySchedulingEvent.Id == null) activitySchedulingEvent.RecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get('Activity_Scheduling').getRecordTypeId();	
			upsert activitySchedulingEvent;
		
			bl_Case_Trigger.runningSchedulingApp = false;
			
			RestContext.response.responseBody = Blob.valueOf(JSON.serialize(activitySchedulingEvent));
				
		}catch (Exception e){
						
			RestResponse res = RestContext.response;
			res.statusCode = 400;
			
			SalesforceError err = new SalesforceError();
			err.errorCode = 'BAD_REQUEST';
			err.message =  e.getMessage();
			
			RestContext.response.responseBody = Blob.valueOf(JSON.serialize(err));
		}   				
	}

	@HttpDelete
	global static void doDelete(){
		
		String toDeleteId = RestContext.request.params.get('id');
		
		try{
			
			Event activitySchedulingEventToDelete = [select Id from Event where Id = :toDeleteId];	
				
			bl_Case_Trigger.runningSchedulingApp = true;
				
			delete activitySchedulingEventToDelete;
		
			bl_Case_Trigger.runningSchedulingApp = false;								
			
			
		}catch (Exception e){
						
			RestResponse res = RestContext.response;
			res.statusCode = 400;
			
			SalesforceError err = new SalesforceError();
			err.errorCode = 'BAD_REQUEST';
			err.message =  e.getMessage();
			
			RestContext.response.responseBody = Blob.valueOf(JSON.serialize(err));
		}			
	}
	
	global class SalesforceError {
    	
    	public String errorCode;
    	public String message;    	
	}    
}