public with sharing class controllerDIB_Campaign {
	
	/**
	 * Creation Date: 	20090922
	 * Description: 	controllerDIB_Campaign 
	 * 
	 * Author: 	ABSI - Bob Ceuppens & Miguel Coimbra
	 */	

	
	/**
	 *	Section : Period & Departion  
	 */
	 
	public Integer PAGE_SIZE {get{return 25;} set;} 
	public static Map<String,Set<Id>> updateAccountCampainMap = null;
	 // Period 
	private String periodChosen ;		
	public String getPeriodChosen()	{											return this.periodChosen ;							}
	public void setPeriodChosen(String s )	{									this.periodChosen = s;								}
	//Territories
	public String territoryFilter{set; get;}
	//Segments
	public String departmentSegment {set; get;}
	
	// Period 2
	public String periodChosen2 { 	
		get{
			String s = this.periodChosen ;
			String result = DIB_AllocationSharedMethods.getFiscalMonthName(s.substring(0,2)) + ' / ' +  s.substring(3,7) ;  	
			return result ;
		} 			
		set ; 
	}	
	
	public String selectedProgram; 	
	public String getSelectedProgram(){										return selectedProgram;							}	
	public void setSelectedProgram(String selectedProgramIn)	{		this.selectedProgram = selectedProgramIn;	}	

	public String [] selectedPrograms = new String[]{};
	public String [] getSelectedPrograms(){										return selectedPrograms;							}	
	public void setSelectedPrograms(String [] selectedProgramsIn)	{		this.selectedPrograms = selectedProgramsIn;	}	
	
	// Accounts 
	
	public String searchCriteria ; 	
	public String getsearchCriteria()	{						return this.searchCriteria ;		}	
	public void setsearchCriteria(String s )	{					this.searchCriteria = s;			}	
	
	private Boolean showError1; 
	public Boolean getshowError1(){									return this.showError1 ; 			}
	public void setshowError1(Boolean a ){							this.showError1 = a ; 				}	
	
	private String countryUser;
	
	public String errorMessage1; 
	public String geterrorMessage1(){									
		return FinalConstants.SFACCOUNT_ERROR_MSG_1 ; 	
	}	
	
	public String errorMessage2; 
	public String geterrorMessage2(){
		return FinalConstants.SFACCOUNT_ERROR_MSG_2 ; 
	}	
	
	// Period 	
	private Date startPeriod ; 
	public Date getStartPeriod(){												return this.startPeriod ;							}
	public void setStartPeriod(Date d) {  										this.startPeriod = d ; 								}
	
	private Date endPeriod ; 
	public Date getEndPeriod(){												return this.endPeriod ;								}
	public void setEndPeriod(Date d) {  										this.endPeriod = d ; 								}
	
	
	 public List<SelectOption> getPeriodItems() {
	 	if (periodChosen == null) { 	
		 	String paramChosenValue = Apexpages.currentPage().getParameters().get(FinalConstants.PARAM_DEFAULT_CHOSEN_PERIOD);
		 	if(paramChosenValue != null && paramChosenValue != ''){
		 		this.periodChosen = paramChosenValue;
		 	}else{	 	
				this.periodChosen = DIB_AllocationSharedMethods.getCurrentPeriod();
		 	}
	 	}
		return DIB_AllocationSharedMethods.getPeriodLines() ;  
	}
	
	 public List<SelectOption> getEmptySelectOption() {
	 	List<SelectOption> options = new List<SelectOption>();
		return options;  
	}	
		
	// Show or Hide the button Submit Depeding on the selected Fiscal Period 
	public Boolean showSubmitButton; 
	public Boolean getshowSubmitButton(){					return this.showSubmitButton ; 	}
	public void setshowSubmitButton(Boolean b){				this.showSubmitButton = b;	 	}	
	
	public void checkShowHideSubmitButton(){
		System.debug('######################## showHideSubmitButton call method : ' ) ; 
		setshowSubmitButton(DIB_Manage_Task_Status.showHideSubmitButton(getSelectedMonth(), getSelectedYear())) ; 
	}
	
	// Pagination 
	
	public Integer pageNr {get;set;}
	
	public Integer NrOfItems{get;set;}
	
	public Integer totalPages{get;set;}
	
	//SubmitResult
	public String submitResult{get;set;}	
	
	 public List<SelectOption> getProgramNames() {

	 	Map<Id,DIB_Campaign_Program__c> myCampaignProgramsMap = new Map<Id,DIB_Campaign_Program__c>();
	 	List<DIB_Campaign__c> myCampaigns = [SELECT d.Global_Campaign__r.DIB_Campaign_Program__r.Id,
	 							d.Global_Campaign__r.DIB_Campaign_Program__r.Name
								FROM DIB_Campaign__c d 
								WHERE
								d.Start_Date_FF__c <=: getSelectedPeriod() AND (d.End_Date_FF__c >=: getSelectedPeriod() OR d.End_Date_FF__c=null)
								AND (d.Country__r.Name =: countryUser
								OR d.isGlobal__c =: true)
								order by d.Global_Campaign__r.DIB_Campaign_Program__r.Name];

	 	for (DIB_Campaign__c myCampaign:myCampaigns) {
	 		myCampaignProgramsMap.put(myCampaign.Global_Campaign__r.DIB_Campaign_Program__r.Id,myCampaign.Global_Campaign__r.DIB_Campaign_Program__r);
	 	}
		
		List<DIB_Campaign_Program__c> myCampaignPrograms = [Select d.Name, d.Id From DIB_Campaign_Program__c d where d.Id IN:myCampaignProgramsMap.values() order by d.Name];
		
		List<SelectOption> options = new List<SelectOption>();
	 	for (DIB_Campaign_Program__c myCampaignProgram:myCampaignPrograms) {
	 		options.add(new SelectOption(myCampaignProgram.Id,myCampaignProgram.Name));
	 	}
		return options;  
	}	
		
	/**
	 *	Data Grid Section 
	 */
	 
	
	// This var handles the visibility of the DataGrid : 
	private Boolean canShowDG = false ; 	
	public void setcanShowDG(Boolean b){										this.canShowDG = b;									}
	public Boolean getcanShowDG(){												return this.canShowDG; 								}
	
	// PageBlock repeats :  
	private DIB_Department__c [] DIBDepartments = new DIB_Department__c[]{};
	private List<DIB_Class_Campaign> DIB_Campaign_Class = new List<DIB_Class_Campaign>{};
	private List<DIB_Campaign__c> DIB_Campaigns = new List<DIB_Campaign__c>{};
	
	// getDIB_Campaigns Used from VF
	public List<DIB_Campaign__c> getDIB_Campaigns() {							return DIB_Campaigns;								}
	
	// setDIB_Campaigns Used from VF
	public void setDIB_Campaigns(List<DIB_Campaign__c> DIB_CampaignsIn) {		this.DIB_Campaigns = DIB_CampaignsIn;				}
	
	// getDIB_Campaign_Class Used from VF	
	public List<DIB_Class_Campaign> getDIB_Campaign_Class() {					return DIB_Campaign_Class;							}
	
	// 	setDIB_Campaign_Class Used from VF
	public void setDIB_Campaign_Class(List<DIB_Class_Campaign> DIB_Campaign_ClassIn) {
				this.DIB_Campaign_Class = DIB_Campaign_ClassIn; 
	}	
	
	// All the Existing Campaign Accounts for the Accounts & Campaigns of the current datagrid 
	private List<DIB_Campaign_Account__c> myExisting_DIB_Campaigns_Accounts = new List<DIB_Campaign_Account__c>{} ;
	public List<DIB_Campaign_Account__c> getmyExisting_DIB_Campaigns_Accounts (){ return this.myExisting_DIB_Campaigns_Accounts ; } 
	public void  setmyExisting_DIB_Campaigns_Accounts (List<DIB_Campaign_Account__c> l){ this.myExisting_DIB_Campaigns_Accounts = l ;  }
	
	/**
	  * Init Methods :
	  */ 
	public void Init() {
		// Clear submitResult
		submitResult = '';		
		getPeriodItems();
		// Only the campaigns available for the user's country can be shown.
		this.init2();
		pageNr = 1 ; 
		this.loadCampaigns();
		// do not show the first time here : 
		setcanShowDG(false);
	}
	
	// get the substring of the period wich will be the first 2 characters 
	public String getSelectedMonth(){
		String pickedPeriod = getPeriodChosen();
		return pickedPeriod.substring(0, 2);	
	}
		
	// get the substring of year which will be the last 4 characters. 
	public String getSelectedYear(){
		String pickedPeriod = getPeriodChosen();    	
		return pickedPeriod.substring(3, 7); 	
	}

	// get the substring of the period wich will be the first 2 characters 
	public Double getSelectedPeriod(){
		String pickedPeriod = getPeriodChosen();
		String myPeriod = pickedPeriod.substring(3, 7) + pickedPeriod.substring(0, 2);
		System.debug('############## getSelectedPeriod:' + myPeriod); 
		return Double.valueOf(myPeriod);
	}
	
	// Get the Campaign Countries associated : 
	
	private Id[] availableCampaignIdsToShow = new Id[]{} ; 
	 
	public void init2() {
		// Get The country of the user :
		Id userId = UserInfo.getUserid();
		countryUser = [Select CountryOR__c from User where id=: userId limit 1].CountryOR__c;	
	}
	
	/**
	 *	Controller  
	 */
	 
	public ApexPages.StandardSetController con {	
		get {
			con.setPageSize(PAGE_SIZE);
			return con; 
		}
		set;
	}
	
	
	/**
	 *	Section : Pagination ( First / Previous / Next / Last)
	 *
	 */
	
	
	// indicates whether there are more records after the current page set.
	public Boolean hasNext {
		get {
			return con.getHasNext();
		}
		set;
	}
 
	// indicates whether there are more records before the current page set.
	public Boolean hasPrevious {
		get {
			return this.con.getHasPrevious();
		}
		set;
	}
 
	// returns the page number of the current page set
	public Integer pageNumber {
		get {
			return this.con.getPageNumber();
		}
		set;
	}
 
 	// PickList Selection of the Page
	
	public String SelectedPage{get;set;}	
	
	public List<SelectOption> pages {
		get{
			List<SelectOption> options = new List<SelectOption>();
			for (Integer i=1 ; i <= this.totalPages ; i++){
		 	 	options.add(new SelectOption(''+i,'' + i));
			}
 	 		return options;
		}
		set;
  	}
  	
  	public void changePage(){
 		pageNr = Integer.valueOf(this.selectedPage) ; 
 		if (pageNr == 50){ 	
 			loadCampaigns();
 		}else{
 			loadCampaigns();
 		}
  	}
 
 
	// returns the first page of records
 	public void first() {
 		pageNr = 1 ; 
 		SelectedPage = String.valueOf(pageNr) ;
 		loadCampaigns();
 	}
 
 	// returns the last page of records
 	public void last() {
 		pageNr = Integer.valueOf(this.NrOfItems / PAGE_SIZE)  ;
 		SelectedPage = String.valueOf(pageNr) ;
 		loadCampaigns();
 	}
 
 	// returns the previous page of records
 	public void previous() {
 		pageNr-- ; 	
 		SelectedPage = String.valueOf(pageNr) ;
 		loadCampaigns(); 
 	} 
 
 	// returns the next page of records
 	public void next() {
 		pageNr++ ;  	
 		SelectedPage = String.valueOf(pageNr) ;	
 		loadCampaigns(); 
 	}
	
	 
	
	/** 
	 *	Draw Campaigns Acounts DataGrid 
	 *	@islast : When the user clicks on the Last link. 
	 */
	 
	public void loadCampaigns(){		
		// Clear submitResult
		submitResult = '';
				
		// Selected Fiscal Period : 
		String SelectedMonth = getSelectedMonth();
		String SelectedYear = getSelectedYear();
		
		Integer selectedPeriod = Integer.valueOf(SelectedYear + SelectedMonth);
		

		
		//#### IMPORTANT : Build a start Date & End Date based on Selected Period with a Date Format and not String ( month & Year)
		// Exemple : "March" "2009"  will become --> Start Date 01/03/2009 & End Date 31/3/2009
		
		string year = getSelectedYear();
		string month = getSelectedMonth();
		string day = '1';
		string stringDate = year + '-' + month + '-' + day ;
		this.startPeriod = date.valueOf(stringDate);
		system.debug('######### Start Date : ' +  startPeriod) ; 
		
		// Last Day of the month : 
		this.endPeriod = startPeriod.addMonths(1) ;		
		this.endPeriod = this.endPeriod - 1 ;
		system.debug('######### End Date : ' +  endPeriod ) ; 
		//#####
		
		
		// Selected Department 
		//String department = getSelectedAccountType() ; 
		
		// Limit put in place to prevent errors when opening the page as administrator
		
		// Account Filter : 
		String currentUserSAPId = DIB_AllocationSharedMethods.getCurrentUserSAPId(System.Userinfo.getUserId());
		
		//DIB_Department__c[] Segments = new DIB_Department__c[]{} ; 
											
		/*if (accountFilter == 'MyAccounts') {			
			Segments = [SELECT d.Department__c, d.Account__r.isSales_Force_Account__c, d.Account__c 
											FROM DIB_Department__c d
											WHERE d.Account__r.isSales_Force_Account__c = true
											AND d.Account__r.DIB_SAP_Sales_Rep_Id__c =: currentUserSAPId
											AND d.Account__r.DIB_SAP_Sales_Rep_Id__c != null
											AND d.Account__r.Name like :searchCriteriaEntered	
											limit 1000	 	
											];
		}else{*/
		/*if(departmentSegment != null && departmentSegment != ''){
						Segments = [SELECT d.Department__c, d.Account__r.isSales_Force_Account__c, d.Account__c 
											FROM DIB_Department__c d
											WHERE d.Account__r.isSales_Force_Account__c = true
											AND d.Account__r.Name like :searchCriteriaEntered
											AND d.DiB_Segment__c = :departmentSegment
											limit 1000	 	
											];
		}else{
			Segments = [SELECT d.Department__c, d.Account__r.isSales_Force_Account__c, d.Account__c 
											FROM DIB_Department__c d
											WHERE d.Account__r.isSales_Force_Account__c = true
											AND d.Account__r.Name like :searchCriteriaEntered	
											limit 1000	 	
											];
		}*/
		//}				
		
		//System.debug(' ################## ' + 'controllerDIB_Campaign_loadCampaigns_Segments size: ' + Segments.size() + ' ################');
		//All filtered account ids
		Set <Id> filteredAccountIds = null;										
		// Get The Accounts with a matched Segmentation :
		Set<Id> SegmentsAccountIds = new Set<Id>{} ;
		// Dedup Account Ids : 
		/*for (DIB_Department__c seg : Segments){
			SegmentsAccountIds.add(seg.Account__c) ; 
		}*/
		
		//Territory Filter for Accounts
		//Null will be returned if no territory selected 
		filteredAccountIds = DIB_AllocationSharedMethods.getFilteredAccounts(this.territoryFilter);		
		
		// Get the Accounts with detailed Information 
		// Limit of 25 in place to make it more workable
		
		/*con = new ApexPages.StandardSetController(Database.getQueryLocator([Select a.isSales_Force_Account__c, a.System_Account__c, a.Name 
							FROM Account a 
							WHERE a.isSales_Force_Account__c = true 
							AND a.Account_Active__c = true
							AND id in: filteredAccountIds order by Name limit 1000]));*/
		String searchCriteriaEntered = '%' + getsearchCriteria() + '%'; 
		
		if(filteredAccountIds != null && departmentSegment != null && departmentSegment != ''){
			con = new ApexPages.StandardSetController(Database.getQueryLocator([Select d.Account__c, 
					d.Account__r.isSales_Force_Account__c, d.Account__r.System_Account__c, d.Account__r.Name, 
					d.Department_ID__r.Name, d.Department_ID__c, d.Department_Master_Name__c From DIB_Department__c d
					WHERE d.Account__r.isSales_Force_Account__c = true 
					AND d.Account__r.Account_Active__c = true
					AND d.Account__c in: filteredAccountIds 
					AND d.DiB_Segment__c = :departmentSegment
					AND d.Account__r.Name like :searchCriteriaEntered
					AND d.Active__c = true
					order by d.Account__r.Name,d.Account__c,d.Department_ID__r.Name limit 1000]));				
		}else if(filteredAccountIds != null){
			con = new ApexPages.StandardSetController(Database.getQueryLocator([Select d.Account__c, 
					d.Account__r.isSales_Force_Account__c, d.Account__r.System_Account__c, d.Account__r.Name, 
					d.Department_ID__r.Name, d.Department_ID__c, d.Department_Master_Name__c From DIB_Department__c d
					WHERE d.Account__r.isSales_Force_Account__c = true 
					AND d.Account__r.Account_Active__c = true
					AND d.Account__c in: filteredAccountIds 
					AND d.Account__r.Name like :searchCriteriaEntered
					AND d.Active__c = true
					order by d.Account__r.Name,d.Account__c,d.Department_ID__r.Name limit 1000]));				
		}else if(departmentSegment != null && departmentSegment != ''){
			con = new ApexPages.StandardSetController(Database.getQueryLocator([Select d.Account__c, 
					d.Account__r.isSales_Force_Account__c, d.Account__r.System_Account__c, d.Account__r.Name, 
					d.Department_ID__r.Name, d.Department_ID__c, d.Department_Master_Name__c From DIB_Department__c d
					WHERE d.Account__r.isSales_Force_Account__c = true 
					AND d.Account__r.Account_Active__c = true
					AND d.DiB_Segment__c = :departmentSegment
					AND d.Account__r.Name like :searchCriteriaEntered
					AND d.Active__c = true
					order by d.Account__r.Name,d.Account__c,d.Department_ID__r.Name limit 1000]));				
		}else{
			con = new ApexPages.StandardSetController(Database.getQueryLocator([Select d.Account__c, 
					d.Account__r.isSales_Force_Account__c, d.Account__r.System_Account__c, d.Account__r.Name, 
					d.Department_ID__r.Name, d.Department_ID__c, d.Department_Master_Name__c From DIB_Department__c d
					WHERE d.Account__r.isSales_Force_Account__c = true 
					AND d.Account__r.Account_Active__c = true
					AND d.Account__r.Name like :searchCriteriaEntered
					AND d.Active__c = true
					order by d.Account__r.Name,d.Account__c,d.Department_ID__r.Name limit 1000]));		
		}
									
		// Manage Page number 
		System.debug('####################### pageNr ' +  pageNr ) ;
		
		this.NrOfItems = con.getResultSize()	;
		
		// *** count the number of pages 
		con.last(); 
		this.totalPages = con.getPageNumber();
		// *** 
		
				
		con.setPageNumber(this.pageNr);
		// ***** 
		
		this.DIBDepartments = (List<DIB_Department__c>) con.getRecords();
		
		System.debug(' ################## ' + 'controllerDIB_Campaign_loadCampaigns_getSelectedPrograms(): ' + getSelectedPrograms() + ' ################');
		
		//only show campaigns with a start / End date within the selected period and for the available countries
		System.debug(' ################## Get the selected period : ' + getSelectedPeriod());
		this.DIB_Campaigns = [SELECT d.isGlobal__c,d.OwnerId, d.System_Campaign__c, d.Name, d.Id,
								d.Campaign_Name_NoBlanks__c, d.Start_Fiscal_Year__c, d.Start_Fiscal_Month__c, d.End_Fiscal_Year__c, 
								d.End_Fiscal_Month__c ,d.Global_Campaign__c, d.Global_Campaign__r.DIB_Campaign_Program__r.Id, d.Global_Campaign__r.DIB_Campaign_Program__r.Name,d.Global_Campaign__r.Id, d.Global_Campaign__r.Name
								FROM DIB_Campaign__c d 
								WHERE
								d.Start_Date_FF__c <=: getSelectedPeriod() AND (d.End_Date_FF__c >=: getSelectedPeriod() OR d.End_Date_FF__c=null) 
								AND
								d.Global_Campaign__r.DIB_Campaign_Program__r.Id IN:getSelectedPrograms() 
								AND (d.Country__r.Name =: countryUser OR d.isGlobal__c =: true) 
								order by d.Name
								limit 15 ];
		System.debug(' ################## ' + 'controllerDIB_Campaign_loadCampaigns_DIB_Campaigns size: ' + DIB_Campaigns.size() + ' ################');
		
		// DISPLAY 
		
		
		List<DIB_Class_Campaign> DIB_Campaign_ClassTemp = new List<DIB_Class_Campaign>();
		DIB_Class_Campaign curDIB_Class_Campaign = new DIB_Class_Campaign();
		
		// Get all the existing campaigns : 
		this.myExisting_DIB_Campaigns_Accounts = [
								SELECT d.Id, d.isMember__c, d.Name, d.Campaign__c, d.Account__c,d.End_Fiscal_Year__c,d.End_Fiscal_Month__c,
								d.Start_Fiscal_Month__c,d.Start_Fiscal_Year__c, d.System_Field_Start_Date__c, d.System_Field_End_Date__c,
								d.Department_ID__c									 
								FROM DIB_Campaign_Account__c d 
								WHERE d.Department_ID__c  in: this.DIBDepartments
								AND	d.Campaign__c in: this.DIB_Campaigns
								];
	
		System.debug(' ################## ' + 'controllerDIB_Campaign_loadCampaigns_myExisting_DIB_Campaigns_Accounts size: ' + this.myExisting_DIB_Campaigns_Accounts.size() + ' ################');								
		Integer existingCampAccStartPeriod = 000001;
		Integer existingCampAccEndPeriod = 999912;
		System.debug(' ################## ' + 'Searched DIBDepartments size: ' + DIBDepartments.size() + ' ################');
		// For all the Current Accounts	
		for (DIB_Department__c currentDIBDepartment:DIBDepartments) {
			
			Account currentAccount = currentDIBDepartment.Account__r;
			
			System.debug(' ################## ' + 'controllerDIB_Campaign_loadCampaigns_currentAccount: ' + currentAccount + ' ################');
			
			List<DIB_Campaign_Account__c> tempDIB_Campaign = new List<DIB_Campaign_Account__c>();
			curDIB_Class_Campaign = new DIB_Class_Campaign();
			curDIB_Class_Campaign.setSFAccount(currentAccount);
			
			//STEP 1 : First create an empty matrix of Campaigns	
			for (DIB_Campaign__c currentCampaign:DIB_Campaigns) {
				System.debug(' ################## ' + 'controllerDIB_Campaign_loadCampaigns_currentCampaign: ' + currentCampaign + ' ################');
				
				DIB_Campaign_Account__c curDIB_Campaign_Account = new DIB_Campaign_Account__c();
				curDIB_Campaign_Account.Account__c = currentAccount.Id;
				curDIB_Campaign_Account.Department_ID__c = currentDIBDepartment.Id;	
				currentCampaign.System_Campaign__c = currentCampaign.Id;					// Only to show up the Hover mini page. 
				curDIB_Campaign_Account.Campaign__c = currentCampaign.Id;
				curDIB_Campaign_Account.Campaign_Name__c = currentCampaign.Name;
				curDIB_Campaign_Account.isMember__c = false;
				//curDIB_Campaign_Account.Department__c = department ; 
				Integer i=0;
				for (DIB_Campaign_Account__c existingCampAcc: myExisting_DIB_Campaigns_Accounts){
					
					//** change MC 201003 : Because these values need to be reinitialized again.
					existingCampAccStartPeriod = 000001;
					existingCampAccEndPeriod = 999912;
					//***
					
					if (curDIB_Campaign_Account.Department_ID__c == existingCampAcc.Department_ID__c && curDIB_Campaign_Account.Campaign__c == existingCampAcc.Campaign__c) {
						if (existingCampAcc.Start_Fiscal_Year__c != null && existingCampAcc.Start_Fiscal_Month__c != null) {
							existingCampAccStartPeriod = Integer.valueOf(existingCampAcc.Start_Fiscal_Year__c + existingCampAcc.Start_Fiscal_Month__c);
						}
						if (existingCampAcc.End_Fiscal_Year__c != null && existingCampAcc.End_Fiscal_Month__c != null) {
							existingCampAccEndPeriod =  Integer.valueOf(existingCampAcc.End_Fiscal_Year__c + existingCampAcc.End_Fiscal_Month__c);
						}
						System.debug('#### selectedPeriod @'+ selectedPeriod + '@' + ' >= existingCampAccStartPeriod @' + existingCampAccStartPeriod +'@'); 
						System.debug('#### selectedPeriod @' + selectedPeriod + '@' + ' <= existingCampAccEndPeriod @' + existingCampAccEndPeriod + '@'); 
						
						if (selectedPeriod >= existingCampAccStartPeriod && selectedPeriod <= existingCampAccEndPeriod){
							System.debug('############ isMember !! ');
							curDIB_Campaign_Account.isMember__c = true;
							break ; 
						}
					}
				}
				tempDIB_Campaign.add(curDIB_Campaign_Account);
			}
			curDIB_Class_Campaign.setCamp_account(tempDIB_Campaign);
			curDIB_Class_Campaign.dibDepartment = currentDIBDepartment;
			DIB_Campaign_ClassTemp.add(curDIB_Class_Campaign);
		}
		System.debug(' ################## ' + 'Result campain records size: ' + DIB_Campaign_ClassTemp.size() + ' ################');
		// If no results, do not show the main table. 
		if (DIB_Campaign_ClassTemp.size() == 0){		 			
			setcanShowDG(false);	
			return ; 		
		}else{
			setcanShowDG(true);
		}		
		this.setDIB_Campaign_Class(DIB_Campaign_ClassTemp);		
		
	}

	
	/**
	 * Action Behaviours / Buttons 
	 */
	 
	 
	// This method is called when the user clicks on the "Display" button : 
	public void displayCampaigns()	{
		this.first();
		this.checkShowHideSubmitButton() ; 		
	}
	
	// This methos is called when the user clicks on the "Save" button :
	public pageReference save()	{
		// Clear submitResult
		submitResult = '';
				
		// Only updates if necessary. do not update records tat do not participate in the campaign. 
			
		List<List<DIB_Campaign_Account__c>> campAccToUpdateList = new List<List<DIB_Campaign_Account__c>>{};
		
		for(DIB_Class_Campaign tempDIB_Class_Campaign:DIB_Campaign_Class) {
			List<DIB_Campaign_Account__c> campAccToUpdate = new List<DIB_Campaign_Account__c>{}  ; 
			List<DIB_Campaign_Account__c> camp_account = tempDIB_Class_Campaign.getCamp_account();
			System.debug(' ################## ' + 'controllerDIB_Campaign_save_camp_account size: ' + camp_account.size() + ' ################');
			for (DIB_Campaign_Account__c cacc : camp_account){
				// TO Do : only send those that have changed !! 
				//if (cacc.isMember__c == true || cacc.Id != null)
				campAccToUpdate.add(cacc);
				if (campAccToUpdate.size()>999) {
					System.debug(' ################## ' + 'controllerDIB_Campaign_save_campAccToUpdateList size: ' + campAccToUpdate.size() + ' ################');
					campAccToUpdateList.add(campAccToUpdate);
					campAccToUpdate = new List<DIB_Campaign_Account__c>{}; 
				}
			}
			campAccToUpdateList.add(campAccToUpdate);
		}
		
		for (List<DIB_Campaign_Account__c> campAccToUpdate: campAccToUpdateList ) {
			ManageSaveCampaigns(campAccToUpdate) ; 	
		}
   		//String gotoURL ; 
   		//gotoURL = '/apex/DIB_Campaign';
   		//PageReference pageRef = new PageReference(gotoURL);
   		//pageRef.setRedirect(true);
   		displayCampaigns();
	   	return null;		
	}
	
	// Submit button : Submit current Month & Year and all accounts for the current user : 
    public void SubmitcurrentPeriod(){
    	if (DIB_Manage_Task_Status.submitCampaign(this.getSelectedMonth(), this.getSelectedYear())) {
    		submitResult = FinalConstants.DiB_submitResultSuccess;
    	}
    	else {
    		submitResult = FinalConstants.DiB_submitResultFailure;
    	}
    	
    }
	
	/**
	 * 	Business Logic : 
	 */
	 
	  
	public void ManageSaveCampaigns(List<DIB_Campaign_Account__c> AllToUpdateAccCampaigns ){
		
		List<DIB_Campaign_Account__c> dibCampAccountsToBeUpserted = new List<DIB_Campaign_Account__c>{} ; 
		List<DIB_Campaign_Account__c> dibCampAccountsToBeDeleted = new List<DIB_Campaign_Account__c>{} ; 
		Set<Id> updateAccountCampainList = new Set<Id>();
		//String department = getSelectedAccountType() ; 	
		
		for (DIB_Campaign_Account__c toUpdateCampAcc : AllToUpdateAccCampaigns){																		 
				
				Boolean isPresent 				= false ; 					
				Boolean isPresentInSamePeriod 	= false ;  
				
				List<DIB_Campaign_Account__c> machedCampAcc	= new List<DIB_Campaign_Account__c> {} ; 					
				
				for (DIB_Campaign_Account__c existingCampAcc: this.myExisting_DIB_Campaigns_Accounts){
				
					if (existingCampAcc.Department_ID__c == toUpdateCampAcc.Department_ID__c && existingCampAcc.Campaign__c == toUpdateCampAcc.Campaign__c ){
							// at least one item exists already exists even if it is not in the good period. : 
							isPresent = true ; 	
							machedCampAcc.add(existingCampAcc); 
							
							if (this.startPeriod >= existingCampAcc.System_Field_Start_Date__c 
								&&  (this.endPeriod   <= existingCampAcc.System_Field_End_Date__c || existingCampAcc.System_Field_End_Date__c == null )){
									
									isPresentInSamePeriod = true ;
									// Do nothing ! 									
									break ; 
							}							
					}					
				}		
				System.debug(' ################## ' + 'controllerDIB_Campaign_ManageSaveCampaigns_machedCampAcc: ' + machedCampAcc + ' ################');
				if (toUpdateCampAcc.isMember__c){
						// ######### CHECKED ############### 			
					if (isPresent == false){
						// ### CHECKED -> DOES NOT EXIST : Create new line and End date = infinite;###   
						toUpdateCampAcc.System_Field_Start_Date__c = this.startPeriod ; 						
						toUpdateCampAcc.Start_Fiscal_Month__c = this.getFiscalFormat(this.startPeriod.month()) ; 
						toUpdateCampAcc.Start_Fiscal_Year__c = this.getFiscalFormat(this.startPeriod.year()) ;
						
						toUpdateCampAcc.System_Field_End_Date__c = null ;
						updateAccountCampainList.add(toUpdateCampAcc.Id);
						dibCampAccountsToBeUpserted.add(toUpdateCampAcc); 
						
					}else{						
						if (isPresentInSamePeriod == false){
							// ### CHECKED -> EXISTS & CURRENT PERIOD NOT COVERED : create new line and adjust End Date ###  							
							// Get the last Date in the calendar to be able to know which will be the End Date of the item :
							/*Date endDatetoSet = machedCampAcc[0].System_Field_Start_Date__c ;												
							for (DIB_Campaign_Account__c cacc : machedCampAcc){								
								if (cacc.System_Field_Start_Date__c < endDatetoSet){
									// Last Date : 
									endDatetoSet =cacc.System_Field_Start_Date__c;  
								}						
							}
							// Date - 1 day = Last day of the Previous month
							endDatetoSet = endDatetoSet - 1 ; 
							
							toUpdateCampAcc.System_Field_Start_Date__c = this.startPeriod ; 
							toUpdateCampAcc.Start_Fiscal_Month__c = this.getFiscalFormat(this.startPeriod.month()) ; 
							toUpdateCampAcc.Start_Fiscal_Year__c = this.getFiscalFormat(this.startPeriod.year()) ;
													
							if (this.endPeriod > endDatetoSet){
								toUpdateCampAcc.System_Field_End_Date__c = null ;
							}else{
								toUpdateCampAcc.System_Field_End_Date__c = endDatetoSet ; 
								toUpdateCampAcc.End_Fiscal_Month__c = this.getFiscalFormat(endDatetoSet.month()) ; 
								toUpdateCampAcc.End_Fiscal_Year__c = this.getFiscalFormat(endDatetoSet.year()) ;
							}
							updateAccountCampainList.add(toUpdateCampAcc.Id);
							dibCampAccountsToBeUpserted.add(toUpdateCampAcc);*/						
							// ### CHECKED -> EXISTS & CURRENT PERIOD NOT COVERED : edit existing line and reset the End Date ###  							
							DIB_Campaign_Account__c existingCamAcc = machedCampAcc[0];
							existingCamAcc.System_Field_Start_Date__c = this.startPeriod ; 						
							existingCamAcc.Start_Fiscal_Month__c = this.getFiscalFormat(this.startPeriod.month()) ; 
							existingCamAcc.Start_Fiscal_Year__c = this.getFiscalFormat(this.startPeriod.year()) ;
							
							existingCamAcc.System_Field_End_Date__c = null ;
							existingCamAcc.End_Fiscal_Month__c = null ; 
							existingCamAcc.End_Fiscal_Year__c = null ;
							updateAccountCampainList.add(existingCamAcc.Id);
							dibCampAccountsToBeUpserted.add(existingCamAcc); 
						}else{
							// ### CHECKED -> EXISTS & CURRENT PERIOD COVERED: Nothing ####  	
						}						
					}
				}else{
					// ########### UNCHECKED ############## 
					if (isPresent == false){						
						// ### UNCHECKED -> DOES NOT EXIST : Do Nothing;###   			
					}else{						
						if (isPresentInSamePeriod == false){
							// ### UNCHECKED -> EXISTS & CURRENT PERIOD NOT COVERED : Do Nothing ; ###  													
						}else{
							// ### UNCHECKED -> EXISTS & CURRENT PERIOD COVERED: Update End Date ####
							// Get the last Date in the calendar to be able to know which will be the End Date of the item : 
							
							DIB_Campaign_Account__c itemToUpdate ; 
							Date endDatetoSet = machedCampAcc[0].System_Field_Start_Date__c ; 	
																		
							for (DIB_Campaign_Account__c cacc : machedCampAcc){								
								if (cacc.System_Field_Start_Date__c >= endDatetoSet){
									itemToUpdate = cacc ;
								}						
							}
							Date endDateOfThePeriod = this.startPeriod - 1 ; 
							
							itemToUpdate.System_Field_End_Date__c = endDateOfThePeriod ;
							itemToUpdate.End_Fiscal_Month__c = this.getFiscalFormat(endDateOfThePeriod.month()) ; 
							itemToUpdate.End_Fiscal_Year__c = this.getFiscalFormat(endDateOfThePeriod.year()) ;
							
							// Exception : If the current moth is the last month of the period, the line must be deleted: 
							updateAccountCampainList.add(itemToUpdate.Id);
							if (itemToUpdate.System_Field_End_Date__c.month() < itemToUpdate.System_Field_Start_Date__c.month()){
								dibCampAccountsToBeDeleted.add(itemToUpdate) ; 
							}else{														 
								dibCampAccountsToBeUpserted.add(itemToUpdate); 
							}   	
						}						
					}		
					
				}
		}
		if(!updateAccountCampainList.isEmpty()){
			if(updateAccountCampainMap == null){
				updateAccountCampainMap = new Map<String,Set<Id>>();
			}
			updateAccountCampainMap.put(Userinfo.getUserId(), updateAccountCampainList);
			// Upsert action 	
			try{
				upsert dibCampAccountsToBeUpserted ;
			}catch(DMLException e){
				ApexPages.AddMessages(e) ; 
			}
			
			updateAccountCampainMap.put(Userinfo.getUserId(), updateAccountCampainList);
			// Delete Action 
			try{
				delete dibCampAccountsToBeDeleted;
			}catch(DMLException e){
				ApexPages.AddMessages(e) ; 
			}
			//1. get the updated campaignAccounts items.
			 
			//2. If checked : get all the lines for the same Combination Account & Campaign . 
			
			// 3. a) If at least one is checked in this combination, then do nothing. And not equals to the one we are updating. 
			
			// 3. b) If none are checked. Check them all and update them as well. 		
		}	

	}
	
	
	/**
	 *	Dummy Methods  : Method transforming an integer to a string & add '0' if under 10
	 */
	 
	public String getFiscalFormat(Integer m){
		if (m < 10){
			return '0' + String.valueOf(m);			
		}else{
			return String.valueOf(m);
		}
	}

    /**
 	*	############################# Section : Territory Filter : ##################################################### 
 	*/
	public List<SelectOption> getTerritoryFilterOptions() {
		return DIB_AllocationSharedMethods.getTerritoryFilterOptions();
	}

   	/**
 	*	############################# Section : Segment Filter : ##################################################### 
 	*/
	public List<SelectOption> getSegmentFilterOptions() {
		return DIB_AllocationSharedMethods.getSegmentFilterOptions();
	}
	
}