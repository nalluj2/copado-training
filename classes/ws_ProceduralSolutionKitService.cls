@RestResource(urlMapping='/ProceduralSolutionKitService/*')
global with sharing class ws_ProceduralSolutionKitService {
    
    @HttpPost
	global static String doPost() {
 		
 		RestRequest req = RestContext.request;
			
		System.debug('post called '+ req);
		System.debug('post requestBody '+ req.requestBody.toString());
			
		String body = req.requestBody.toString();
		ProceduralSolutionKitRequest ProceduralSolutionKitRequest = (ProceduralSolutionKitRequest) System.Json.deserialize(body, ProceduralSolutionKitRequest.class);
			
		System.debug('post requestBody ' + ProceduralSolutionKitRequest);
			
		Procedural_Solutions_Kit__c proceduralSolutionKit = ProceduralSolutionKitRequest.ProceduralSolutionKit;

		proceduralSolutionKitResponse resp = new ProceduralSolutionKitResponse();

		//We catch all exception to assure that 
		try{
				
			resp.ProceduralSolutionKit = bl_ProceduralSolutionKit.saveProceduralSolutionKit(proceduralSolutionKit);
			resp.id = ProceduralSolutionKitRequest.id;
	
			resp.success = true;
			
		}catch (Exception e){
			
			resp.message = e.getMessage();
			resp.success = false;
			RestResponse res = RestContext.response;
			res.statusCode = 400;
		}
 
		bl_Mobilesync.storeRequest(UserInfo.getUserId(), 'ProceduralSolutionKit', req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
  			
		return System.JSON.serialize(resp);		
	}

	@HttpDelete
	global static String doDelete(){
		RestRequest req = RestContext.request;
			
		System.debug('post called ' + req);
		System.debug('post requestBody ' + req.requestBody.toString());
			
		String body = req.requestBody.toString();
		ProceduralSolutionKitDeleteRequest ProceduralSolutionKitDeleteRequest = (ProceduralSolutionKitDeleteRequest) System.Json.deserialize(body, ProceduralSolutionKitDeleteRequest.class);
			
		System.debug('post requestBody ' + ProceduralSolutionKitDeleteRequest);
			
		Procedural_Solutions_Kit__c ProceduralSolutionKit = ProceduralSolutionKitDeleteRequest.ProceduralSolutionKit;
		
		ProceduralSolutionKitDeleteResponse resp = new ProceduralSolutionKitDeleteResponse();
		
		List<Procedural_Solutions_Kit__c> ProceduralSolutionKitsToDelete = [select id, mobile_id__c from Procedural_Solutions_Kit__c where Mobile_ID__c = :ProceduralSolutionKit.Mobile_ID__c];
		
		try{
			
			if (ProceduralSolutionKitsToDelete != null && ProceduralSolutionKitsToDelete.size() > 0){	
				
				Procedural_Solutions_Kit__c ProceduralSolutionKitToDelete = ProceduralSolutionKitsToDelete.get(0);
				bl_ProceduralSolutionKit.deleteProceduralSolutionKit(ProceduralSolutionKitToDelete);
				resp.ProceduralSolutionKit = ProceduralSolutionKitToDelete;
				resp.id = ProceduralSolutionKitDeleteRequest.id;
				resp.success = true;
				
			}else{
				
				resp.ProceduralSolutionKit = ProceduralSolutionKit;
				resp.id = ProceduralSolutionKitDeleteRequest.id;
				resp.message='ProceduralSolutionKit not found';
				resp.success = true;
			}
			
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
			RestResponse res = RestContext.response;
			res.statusCode = 400;
		}
			
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'ProceduralSolutionKitDelete', req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
			
		return System.JSON.serialize(resp);
	}

	global class ProceduralSolutionKitRequest{
		public Procedural_Solutions_Kit__c ProceduralSolutionKit {get;set;}
		public String id{get;set;}
	}
	
	global class ProceduralSolutionKitDeleteRequest{
		public Procedural_Solutions_Kit__c ProceduralSolutionKit {get;set;}
		public String id{get;set;}
	}
		
	global class ProceduralSolutionKitDeleteResponse{
		public Procedural_Solutions_Kit__c ProceduralSolutionKit {get;set;}
		public String id{get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}

	global class ProceduralSolutionKitResponse{
		public Procedural_Solutions_Kit__c ProceduralSolutionKit {get;set;}
		public String id{get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}
}