/**
    Apex Class:tr_InsertQuestionAndAnswers
    Description: This class contains unit tests for validating the behavior of Apex classes 
    Author - Dheeraj
    Created Date  - 03/06/2013
 */
@isTest
private class test_InsertQuestionAndAnswers {

    static testMethod void TestInsertQuestionAndAnswers() {
    	OMA_multiple_QA__c OMAMultiQAs = new OMA_multiple_QA__c();
    	OMAMultiQAs.Name = 'Test';  
    	OMAMultiQAs.Record_Type__c = 'OMA_Spine_Scientific_Exchange_Product';
    	insert OMAMultiQAs;
    	
    	RecordType rt = [select id, DeveloperName from RecordType where DeveloperName =: OMAMultiQAs.Record_Type__c limit 1];
    	
    	Case c = new case();
    	c.OMA_Question__c = 'Test Questions';
    	c.OMA_Answer__c = 'Test Anwser';
    	c.RecordTypeId = rt.Id;
    	insert c;
        
    }
}