public class bl_Contract_Repository_Bundle {
    
    public static void protectInterfacedBundles(List<Contract_Repository_Bundle__c> triggerNew){
    	
    	if(bl_Trigger_Deactivation.isTriggerDeactivated('contractBundle_protectBundles')) return;
    	
    	if(ProfileMedtronic.isSystemAdministratorProfile(UserInfo.getProfileId()) || ProfileMedtronic.isDevartProfile(UserInfo.getProfileId())) return;
    	
    	Set<Id> createdByIds = new Set<Id>();
    	
    	for(Contract_Repository_Bundle__c contractBundle : triggerNew){
    		
    		createdByIds.add(contractBundle.CreatedById);
    	}
    	
    	Map<Id, User> createdByMap = new Map<Id, User>([Select Id, Profile.Name from User where Id IN :createdByIds]);
    	
    	for(Contract_Repository_Bundle__c contractBundle : triggerNew){
    		
    		User createdBy = createdByMap.get(contractBundle.CreatedById);
    		
    		if(createdBy.Profile.Name.startsWith('Devart')) contractBundle.addError('This is an interfaced Bundle and cannot be modified.');
    	}    	
    }     
}