@isTest 
Private class TEST_PcodeRealignment {    
    static testMethod void myUnitTest(){
    Company__c cmpny = new Company__c();       
    cmpny.name='TestMedtronic';       
    cmpny.CurrencyIsoCode = 'EUR';       
    cmpny.Current_day_in_Q1__c=56;       
    cmpny.Current_day_in_Q2__c=34;        
    cmpny.Current_day_in_Q3__c=5;        
    cmpny.Current_day_in_Q4__c= 0;          
    cmpny.Current_day_in_year__c =200;       
    cmpny.Days_in_Q1__c=56;         
    cmpny.Days_in_Q2__c=34;       
    cmpny.Days_in_Q3__c=13;       
    cmpny.Days_in_Q4__c=22;       
    cmpny.Days_in_year__c =250;       
    cmpny.Company_Code_Text__c = 'T41';       
    insert cmpny; 
    
    Business_Unit__c bu = new Business_Unit__c();       
    bu.name = 'Testing BU';        
    bu.abbreviated_name__c = 'Abb Bu';        
    bu.Company__c = cmpny.Id;        
    insert bu;         
    //Insert SBU        
    Sub_Business_Units__c sbu1 = new Sub_Business_Units__c();        
    sbu1.name='SBUMedtronic1';        
    sbu1.Business_Unit__c=bu.id;        
    insert sbu1; 
    
            
    Product2 pro1 = new Product2();        
    pro1.Name = 'Test Product';        
    pro1.Business_Unit_ID__c = bu.id;        
    pro1.Consumable_Bool__c = true;        
    pro1.IsActive = true;
    pro1.P_Code__c='1234';        
    //pro1.DefaultPrice=12.0;        
    pro1.recordtypeid = RecordTypeMedtronic.Product('SAP_Product').id;      
    insert pro1;
    
    Product2 mktProduct = new Product2();        
    mktProduct.Name = 'Test Product2';        
    mktProduct.Business_Unit_ID__c = bu.id;        
    mktProduct.Consumable_Bool__c = true;        
    mktProduct.IsActive = true;
    mktProduct.recordtypeid = RecordTypeMedtronic.Product('MDT_Marketing_Product').id;      
    insert mktProduct;   
        
    pro1.Name = 'Test Product2';        
    pro1.Business_Unit_ID__c = bu.id;        
    pro1.Consumable_Bool__c = true;        
    pro1.IsActive = true;
    pro1.P_Code__c='15234';        
    pro1.recordtypeid = RecordTypeMedtronic.Product('SAP_Product').id;   
    update pro1;
    
    delete pro1;
    
}
}