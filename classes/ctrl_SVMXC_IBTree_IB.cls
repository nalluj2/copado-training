/**
 * @description       : 
 * @author            : tom.h.ansley@medtronic.com
 * @last modified on  : 10-12-2020
 * @last modified by  : tom.h.ansley@medtronic.com
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   10-12-2020   tom.h.ansley@medtronic.com   Initial Version
**/
public with sharing class ctrl_SVMXC_IBTree_IB {
    
    public SVMXC__Installed_Product__c instProd {get; set;}
    public String topInstProdId                 {get; set;}
    public String thisInstProdId                {get; set;}

    public ctrl_SVMXC_IBTree_IB(ApexPages.StandardController controller)  {
        
        instProd = (SVMXC__Installed_Product__c) controller.getRecord();

        instProd = [SELECT Id, SVMXC__Top_Level__c FROM SVMXC__Installed_Product__c WHERE Id = :instProd.Id];

        topInstProdId = instProd.SVMXC__Top_Level__c;
        thisInstProdId = instProd.Id;

    }
}