@isTest
private class TEST_ctrl_FieldHistory {
	
	private static Create_User_Request__c oCreateUserRequest;
	private static List<Create_User_Request__History> lstCreateUserRequestHistory;

	@isTest static void createTestData() {

		// Create Create_User_Request__c (Support Request)
		oCreateUserRequest = new Create_User_Request__c();          
			oCreateUserRequest.Status__c = 'New';
			oCreateUserRequest.Application__c = 'Salesforce.com';
			oCreateUserRequest.Request_Type__c = 'Reset Password';          
			oCreateUserRequest.Requestor_Email__c = UserInfo.getUserEmail();     
		insert oCreateUserRequest;

		// Create Create_User_Request__History data because this data will not be created automatically in Test Classes (Spring 17)
		lstCreateUserRequestHistory = new List<Create_User_Request__History>();
		Create_User_Request__History oCreateUserRequest_History1 = new Create_User_Request__History();
			oCreateUserRequest_History1.ParentId = oCreateUserRequest.Id;
			oCreateUserRequest_History1.Field = 'Created';
		lstCreateUserRequestHistory.add(oCreateUserRequest_History1);
		Create_User_Request__History oCreateUserRequest_History2 = new Create_User_Request__History();
			oCreateUserRequest_History2.ParentId = oCreateUserRequest.Id;
			oCreateUserRequest_History2.Field = 'Status__c';
		lstCreateUserRequestHistory.add(oCreateUserRequest_History2);
		Create_User_Request__History oCreateUserRequest_History3 = new Create_User_Request__History();
			oCreateUserRequest_History3.ParentId = oCreateUserRequest.Id;
			oCreateUserRequest_History3.Field = 'Status__c';
		lstCreateUserRequestHistory.add(oCreateUserRequest_History3);
		insert lstCreateUserRequestHistory;

	}
	
	@isTest static void test_ctrl_FieldHistory() {
		
		//---------------------------------------------
		// Create Test Data
		//---------------------------------------------
		createTestData();
		//---------------------------------------------


		//---------------------------------------------
		// Perform Test
		//---------------------------------------------
		Test.startTest();

		List<ctrl_FieldHistory.cFieldHistory> lstCFieldHistory;

        ctrl_FieldHistory oCTRL = new ctrl_FieldHistory();
			oCTRL.id_Record = oCreateUserRequest.Id;
			oCTRL.tSObjectName = 'Create_User_Request__History';       	
			oCTRL.tTitle_PageBlock = 'Title goes here';
			oCTRL.iNumberOfVisibleRecords = 1;
			oCTRL.iRecordStep = 1;

			lstCFieldHistory = oCTRL.getFieldHistory();
			System.assertEquals(lstCFieldHistory.size(), 1);

			oCTRL.showMoreRecords();
			lstCFieldHistory = oCTRL.getFieldHistory();
			System.assertEquals(lstCFieldHistory.size(), 2);

			oCTRL.showLessRecords();
			lstCFieldHistory = oCTRL.getFieldHistory();
			System.assertEquals(lstCFieldHistory.size(), 1);

			oCTRL.showLessRecords();
			lstCFieldHistory = oCTRL.getFieldHistory();
			System.assertEquals(lstCFieldHistory.size(), 1);

			oCTRL.showMoreRecords();
			lstCFieldHistory = oCTRL.getFieldHistory();
			System.assertEquals(lstCFieldHistory.size(), 2);

			oCTRL.showAllRecords();
			lstCFieldHistory = oCTRL.getFieldHistory();
			System.assertEquals(lstCFieldHistory.size(), 3);

			oCTRL.showLessRecords();
			lstCFieldHistory = oCTRL.getFieldHistory();
			System.assertEquals(lstCFieldHistory.size(), 2);

		Test.stopTest();
		//---------------------------------------------


		//---------------------------------------------
		// Validate Result
		//---------------------------------------------
		//---------------------------------------------

	}

}