public class bl_AccountPerformanceLoad{
	
	
	public static Account_Performance_Load__c saveAccountPerformanceLoad(Account_Performance_Load__c accountPerformanceLoad){
		
		if (null!=accountPerformanceLoad){
			insert accountPerformanceLoad;
		}
		return accountPerformanceLoad;
		
	} 
	
	public static Account_Performance_Load__c saveAccountPerformanceChangeMobile(Account_Performance__c accountPerformance, boolean CompNullified){
		Account_Performance_Load__c accountPerformanceLoad;
		if (null!=accountPerformance){
			accountPerformanceLoad = convertAccountPerformanceToLoad(accountPerformance,'APDR mobile');
			accountPerformanceLoad.Primary_Comp_Nullified__c = CompNullified;
			insert accountPerformanceLoad;
		}
		return accountPerformanceLoad;
		
	} 
	
	public static Account_Performance_Load__c convertAccountPerformanceToLoad(Account_Performance__c ap, String loadSource){
		Account_Performance_Load__c accPerfLoad = new Account_Performance_Load__c();
		accPerfLoad.Account__c = ap.BI_Account_ID__c;
		if(ap.Forecast_Q1_Revenue__c!=null){
			accPerfLoad.Forecast_Q1_Revenue__c = ap.Forecast_Q1_Revenue__c;
		}
		if(ap.Forecast_Q1_Units__c!=null){
			accPerfLoad.Forecast_Q1_Units__c = ap.Forecast_Q1_Units__c;
		}
		if(ap.Forecast_Q2_Revenue__c!=null){
			accPerfLoad.Forecast_Q2_Revenue__c = ap.Forecast_Q2_Revenue__c;
		}
		if(ap.Forecast_Q2_Units__c!=null){
			accPerfLoad.Forecast_Q2_Units__c = ap.Forecast_Q2_Units__c;
		}
		if(ap.Forecast_Q3_Revenue__c!=null){
			accPerfLoad.Forecast_Q3_Revenue__c = ap.Forecast_Q3_Revenue__c;
		}
		if(ap.Forecast_Q3_Units__c!=null){
			accPerfLoad.Forecast_Q3_Units__c = ap.Forecast_Q3_Units__c;
		}
		if(ap.Forecast_Q4_Revenue__c!=null){
			accPerfLoad.Forecast_Q4_Revenue__c = ap.Forecast_Q4_Revenue__c;
		}
		if(ap.Forecast_Q4_Units__c!=null){
			accPerfLoad.Forecast_Q4_Units__c = ap.Forecast_Q4_Units__c;
		}
		accPerfLoad.Load_Source__c = loadSource;
		accPerfLoad.Potential_Units__c = ap.Potential_Units__c;
		accPerfLoad.Potential_Revenue__c = ap.Potential_revenue__c;
		accPerfLoad.Primary_Competitor__c = ap.Primary_Competitor__c;
		accPerfLoad.Product_Group__c = ap.Product_Group__c;
		accPerfLoad.CurrencyIsoCode = ap.CurrencyIsoCode;
		return accPerfLoad;
	}
	
	

	static System.Roundingmode roundingM = System.RoundingMode.UP;
	static Integer scale = 3;
	public static List<Account_Performance_Load__c> convertToUSD(List<Account_Performance_Load__c> accountPerformanceLoadList){
		for (Account_Performance_Load__c apl : accountPerformanceLoadList){
		
			if(apl.CurrencyIsoCode!=null && apl.CurrencyIsoCode.compareTo('USD')!=0){
				CurrencyType currType = bl_Currency.getCurrencyType(apl.CurrencyIsoCode);
				apl.CurrencyIsoCode = 'USD';
				System.debug('APL '+apl+' currType '+currType );
				if(apl.Forecast_Q1_Revenue__c!=null){
					apl.Forecast_Q1_Revenue__c = apl.Forecast_Q1_Revenue__c/currType.ConversionRate;
				}
				if(apl.Forecast_Q2_Revenue__c!=null){
					apl.Forecast_Q2_Revenue__c = apl.Forecast_Q2_Revenue__c/currType.ConversionRate;
				}
				if(apl.Forecast_Q3_Revenue__c!=null){
					apl.Forecast_Q3_Revenue__c = apl.Forecast_Q3_Revenue__c/currType.ConversionRate;
				}
				if(apl.Forecast_Q4_Revenue__c!=null){
					apl.Forecast_Q4_Revenue__c = apl.Forecast_Q4_Revenue__c/currType.ConversionRate;
				}
				if(apl.Potential_Revenue__c!=null){
					apl.Potential_Revenue__c = apl.Potential_Revenue__c/currType.ConversionRate;
				}
			}else{
				//force currencyIsocode to USD (all records need to have this currCode)
				apl.CurrencyIsoCode = 'USD';
			}
		
		}
		return accountPerformanceLoadList;
	}
	

}