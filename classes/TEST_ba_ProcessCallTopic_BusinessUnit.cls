@isTest 
private class TEST_ba_ProcessCallTopic_BusinessUnit {

	@isTest
	private static void test_ba_ProcessCallTopic_BusinessUnit() {

		//----------------------------------------------------
		// Create Test Data
		//----------------------------------------------------
		clsTestData_MasterData.createSubBusinessUnit();
		clsTestData_CallRecord.iRecord_CallRecord = 10;
		clsTestData_CallRecord.createCallRecord();

        Call_Activity_Type__c oCallActivityType = new Call_Activity_Type__c();
	        oCallActivityType.name = 'test C-A-T 1';
	        oCallActivityType.Company_ID__c = clsTestData_MasterData.oMain_Company.Id;
        insert oCallActivityType;

        // Call_Category__c
		Call_Category__c oCallCategory = new Call_Category__c();
			oCallCategory.Company_ID__c = clsTestData_MasterData.oMain_Company.Id;
			oCallCategory.Name = 'test C-C 1';		
		insert oCallCategory;
	
		List<Therapy__c> lstTherapy = clsTestData_Therapy.createTherapy();
		List<Call_Topics__c> lstCallTopic = new List<Call_Topics__c>();
		for (Call_Records__c oCallRecord : clsTestData_CallRecord.lstCallRecord){
					
			Call_Topics__c oCallTopic = new Call_Topics__c();
				oCallTopic.Call_Records__c = oCallRecord.Id;
				oCallTopic.Call_Activity_Type__c = oCallActivityType.Id;
				oCallTopic.Call_Category__c = oCallCategory.Id;
				oCallTopic.Call_Topic_Therapy__c = lstTherapy[0].Id;
			lstCallTopic.add(oCallTopic);

		}
		insert lstCallTopic; 

		List<Call_Topic_Business_Unit__c> lstCallTopicBusinessUnit = new List<Call_Topic_Business_Unit__c>();
		for (Call_Topics__c oCallTopic : lstCallTopic){
			Integer iCounter = 0;
			for (Business_Unit__c oBU : clsTestData_MasterData.lstBusinessUnit){

				if (iCounter == 2) break;
				
				Call_Topic_Business_Unit__c oCallTopicBusinessUnit = new Call_Topic_Business_Unit__c();				
					oCallTopicBusinessUnit.Business_Unit__c = oBU.Id;
					oCallTopicBusinessUnit.Call_Topic__c = oCallTopic.Id;
				lstCallTopicBusinessUnit.add(oCallTopicBusinessUnit);

			}
			
		}
		insert lstCallTopicBusinessUnit;

		for (Call_Topics__c oCallTopic : lstCallTopic) oCallTopic.Call_Topic_Business_Units_Concatendated__c = null;
		update lstCallTopic;

		lstCallTopic = [SELECT Id, Call_Topic_Business_Units_Concatendated__c FROM Call_Topics__c];
		for (Call_Topics__c oCallTopic : lstCallTopic){
			System.assert(String.isBlank(oCallTopic.Call_Topic_Business_Units_Concatendated__c));
		}
		//----------------------------------------------------


		//----------------------------------------------------
		// Execute Logic
		//----------------------------------------------------
		Test.startTest();

			ba_ProcessCallTopic_BusinessUnit oBatch = new ba_ProcessCallTopic_BusinessUnit();
				oBatch.tSOQL = 'SELECT Id, Call_Topic_Business_Units_Concatendated__c FROM Call_Topics__c';
				oBatch.setID_CallTopic = new Set<Id>();
				oBatch.setID_CallTopic_Production = new Set<Id>();
				oBatch.tAdditionalEmail = 'bart.caelen@medtronic.com';
			Database.executeBatch(oBatch, 200);

		Test.stopTest();
		//----------------------------------------------------


		//----------------------------------------------------
		// Validate Result
		//----------------------------------------------------
		lstCallTopic = [SELECT Id, Call_Topic_Business_Units_Concatendated__c FROM Call_Topics__c];
		for (Call_Topics__c oCallTopic : lstCallTopic){
			System.assert(!String.isBlank(oCallTopic.Call_Topic_Business_Units_Concatendated__c));
		}
		//----------------------------------------------------
		

	}
}