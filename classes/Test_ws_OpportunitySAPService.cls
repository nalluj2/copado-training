/*
 * Description      : test class for web service class ws_OpportunitySAPService
 * Author           : Patrick Brinksma
 * Created Date     : 02-08-2013
 */
@isTest(seeAllData=true)
private class Test_ws_OpportunitySAPService {

    private static User oUser_SystemAdmin = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true LIMIT 1];

    static testMethod void testUpsertOpportunity(){
        // Create Person Account
        String randVal = Test_TestDataUtil.createRandVal();
        Map<String, User> mapOfUserNameToUser = Test_TestDataUtil.createUsers(randVal, '@testingmedtronic.com', new List<String>{'System Administrator'}, new List<String>{'CEO'});
        mapOfUserNameToUser.values()[0].DefaultCurrencyIsoCode = 'CAD';
        insert mapOfUserNameToUser.values();

        System.runAs(mapOfUserNameToUser.values()[0]){
            List<Account> listOfAccount = Test_TestDataUtil.createSAPPersonAccounts(randVal, 1);
            listOfAccount[0].Account_Country_vs__c = 'Canada';
            insert listOfAccount;
            // Create test Health Insurance Account
            List<Account> listOfHIAccnt = Test_TestDataUtil.createHealthInsurers(randVal, 'CANADA', 1);
            insert listOfHIAccnt;
                    
            // Create Pricebook and entry for product 
            List<PricebookEntry> listOfPBEntry = Test_TestDataUtil.createPricebookEntries([select Id from Pricebook2 where Name = 'CAN DiB Pricebook'].Id, 2);
            insert listOfPBEntry;
            Set<Id> setOfProductId = new Set<Id>{listOfPBEntry[0].Product2Id, listOfPBEntry[1].Product2Id};
            List<Product2> listOfProd = [select Id, ProductCode from Product2 where Id in :setOfProductId];
            
            // Create structure for upsertOpportunity
            List<ws_OpportunitySAPService.SAPOpportunityItem> listOfSapOppItem = new List<ws_OpportunitySAPService.SAPOpportunityItem>();
            Integer i = 1;
            for (Product2 thisProd : listOfProd){
                ws_OpportunitySAPService.SAPOpportunityItem sapOppItem = new ws_OpportunitySAPService.SAPOpportunityItem();
                sapOppItem.PRODUCTCODE = listOfProd[0].ProductCode; 
                sapOppItem.QUANTITY = String.valueof(1);
                sapOppItem.SAP_ITEM_CATEGORY = '1234';
                sapOppItem.SAP_ITEM_GUID = randVal + thisProd.Id;
                sapOppItem.SAP_ITEM_ID = 'SAPITEMID' + i;
                i++;
                sapOppItem.SAP_WARRANTY_START_DATE = System.today();
                sapOppItem.SAP_WARRANTY_END_DATE = System.today() + 100;
                sapOppItem.SAP_CREATED_DATE = System.now();
                sapOppItem.SAP_LASTMODIFIED_DATE = System.now();
                sapOppItem.SAP_ITEM_SERIALNO = 'serial00' + i;
                sapOppItem.SALES_PRICE = String.valueOf(1500);
                listOfSapOppItem.add(sapOppItem);
            }
            ws_OpportunitySAPService.SAPOpportunity sapOpp = new ws_OpportunitySAPService.SAPOpportunity();
            sapOpp.AUTOMATIC_CREATION = 'OTC-BATCH';
            sapOpp.CLOSEDATE = System.today() + 60;
            sapOpp.CREATED_DATE = System.now();
            sapOpp.CURRENCYISOCODE = 'CAD';
            sapOpp.DEPARTMENT = 'Adult';
            sapOpp.DESCRIPTION = 'Opportunity Description';
            sapOpp.LASTMODIFIED_DATE = System.now();
            sapOpp.NAME = randVal + ' Test Opportunity';
            sapOpp.NAME2 = randVal + ' Test Opportunity 2';
            sapOpp.OPPORTUNITY_ITEMS = listOfSapOppItem;
            sapOpp.ORIGIN = 'MDT initiated';
            sapOpp.REQUESTED_DELIVERY_DATE = System.today() + 90;
            sapOpp.SAP_CREATED_DATE = System.now();
            sapOpp.SAP_HEALTH_INSURANCE = listOfHIAccnt[0].SAP_ID__c;
            sapOpp.SAP_ID = 'SAPIDXXX';
            sapOpp.SAP_LASTMODIFIED_DATE = System.now();
            sapOpp.SAP_ORDER_GUID = '012345678901234567890123456789';
            sapOpp.SAP_PHASE = 'New';
            sapOpp.SAP_PHYSICIAN = '';
            sapOpp.SAP_REASON = '';
            sapOpp.SAP_SALESREP = mapOfUserNameToUser.values()[0].SAP_ID__c;
            sapOpp.SAP_SHIP_TO_ID = listOfAccount[0].SAP_ID__c;
            sapOpp.SAP_SOLD_TO_ID = listOfAccount[0].SAP_ID__c;
            sapOpp.SAP_STATUS = 'Open';
            sapOpp.TRAINING_DATE = System.today() + 7;
            sapOpp.TRIAL_START_DATE = System.today() + 14;
            sapOpp.TRIAL_END_DATE = System.today() + 21;
            // This value is set in Test_TestDataUtil.createSAPPersonAccounts
            // First test wrong Id
            sapOpp.SAP_ACCOUNTID = 'SAPXXX';
            
            Test.startTest();
            
            Test.setMock(WebServiceMock.class, new Test_ws_SAPOpportunityNotificationMockup());
            
            System.debug('*** testUpsertOpportunity - Upsert Opportunity with incorrect SAP ID');
            ws_OpportunitySAPService.SAPOpportunity sapOppUpserted = ws_OpportunitySAPService.upsertOpportunity(sapOpp);
            
            // Now test with correct Account Id
            System.debug('*** testUpsertOpportunity - Upsert Opportunity with correct SAP ID');
            sapOpp.SAP_ACCOUNTID = listOfAccount[0].SAP_ID__c;
            sapOppUpserted = ws_OpportunitySAPService.upsertOpportunity(sapOpp);
            
            Test.stopTest();
        } 
    }

    static testMethod void testNotificationGetOpportunityAndAcknowledgement(){

        System.runAs(oUser_SystemAdmin){

            // Set Mock WS response
            Test.setMock(WebServiceMock.class, new Test_ws_SAPOpportunityNotificationMockup());
            // Create test person account
            String randVal = Test_TestDataUtil.createRandVal();
            List<Account> listOfAccount = Test_TestDataUtil.createSAPPersonAccounts(randVal, 1);
            insert listOfAccount;
            // Create test Opportunity
            List<String> listOfRecordTypeDevName = new List<String>{ut_StaticValues.RTDEVNAME_CAN_DIB_OPPORTUNITY};
            Opportunity tstOppty = Test_TestDataUtil.createOpportunities(randVal, listOfAccount, listOfRecordTypeDevName, 1)[0];  
            // Opportuntiy with no SAP ID and Sales Stage New should be send out
            tstOppty.StageName = 'New';
            insert tstOppty;
            Id tstOpptyId = tstOppty.Id;
            // Retrieve inserted Opportunity to obtain pricebookid
            Opportunity thisOppty = [select Id, Pricebook2Id from Opportunity where Id = :tstOppty.Id];

            //-BC - 20150629 - Added to be sure that the Test Class succeeds - START
            List<Pricebook2> lstPricebook = [SELECT ID FROM Pricebook2 WHERE Name = 'CAN DiB Pricebook'];
            // Create Pricebook and entry for product 
            List<PricebookEntry> listOfPBEntry = Test_TestDataUtil.createPricebookEntries(lstPricebook[0].Id, 1);
            insert listOfPBEntry;

            // Create Pricebook and entry for product 
    //      List<PricebookEntry> listOfPBEntry = Test_TestDataUtil.createPricebookEntries(thisOppty.Pricebook2Id, 1);
    //      insert listOfPBEntry;
            //-BC - 20150629 - Added to be sure that the Test Class succeeds - STOP
            
            System.debug('*** thisOppty.Pricebook2Id: ' + thisOppty.Pricebook2Id);
            System.debug('*** listOfPBEntry[0].Id: ' + listOfPBEntry[0].Pricebook2Id);

            // Add product line
            OpportunityLineItem opptyLine = new OpportunityLineItem(OpportunityId = tstOppty.Id,
                                                                    PricebookEntryId = listOfPBEntry[0].Id,
                                                                    Quantity = 1.0,
                                                                    TotalPrice = 2500.0);   
            
            // Also
            Test.startTest();
            insert opptyLine;
            
            System.debug('*** testNotificationGetOpportunityAndAcknowledgement - get Opportunity without Id');
            ws_OpportunitySAPService.SAPOpportunity wsResponse = ws_OpportunitySAPService.getOpportunity(null);
            //System.assertEquals('Error in process', wsResponse.DISTRIBUTION_STATUS);
            //System.assertEquals('Opportunity Id is required!', wsResponse.DIST_ERROR_DETAILS);
            
            // reset
            wsResponse.DISTRIBUTION_STATUS = null;
            wsResponse.DIST_ERROR_DETAILS = null;
            
            System.debug('*** testNotificationGetOpportunityAndAcknowledgement - get Opportunity with Id');
            wsResponse = ws_OpportunitySAPService.getOpportunity(tstOppty.Id);
            //System.assertEquals('Successfully processed', wsResponse.DISTRIBUTION_STATUS);
            //System.assertEquals(null, wsResponse.DIST_ERROR_DETAILS);
            
            ws_OpportunitySAPService.SAPOpportunityAck sapOpp = new ws_OpportunitySAPService.SAPOpportunityAck();
            sapOpp.SAP_ID = 'SAPIDXXX';
            sapOpp.SAP_ORDER_GUID = '012345678901234567890123456789';
            
            List<ws_OpportunitySAPService.SAPOpportunityItemAck> listOfSapOppItem = new List<ws_OpportunitySAPService.SAPOpportunityItemAck>();
            ws_OpportunitySAPService.SAPOpportunityItemAck sapOppItem = new ws_OpportunitySAPService.SAPOpportunityItemAck();
            sapOppItem.OPPORTUNITYPRODUCTID = opptyLine.Id;
            sapOppItem.SAP_ITEM_GUID = '012345678901234567890123456789I';
            sapOppItem.SAP_ITEM_ID = 'ITEMXXX';
            listOfSapOppItem.add(sapOppItem);
            
            sapOpp.OPPORTUNITY_ITEMS = listOfSapOppItem;
            
            // First with no Opportunity Id
            System.debug('*** testNotificationGetOpportunityAndAcknowledgement - without Opportunity Id');
            ws_OpportunitySAPService.SAPOpportunityAck sapOppAck = ws_OpportunitySAPService.updateOpportunityAcknowledgement(sapOpp);
            //System.assertEquals('Error in process', sapOppAck.DISTRIBUTION_STATUS);
            //System.assertEquals('Opportunity Id is required!', sapOppAck.DIST_ERROR_DETAILS);     
            
            // reset error
            sapOppAck.DIST_ERROR_DETAILS = null;
            sapOppAck.DISTRIBUTION_STATUS = 'Successfully processed';
            
            // Now with Opportunity Id
            System.debug('*** testNotificationGetOpportunityAndAcknowledgement - with Opportunity Id');
            sapOpp.OPPORTUNITYID = tstOppty.Id;
            sapOppAck = ws_OpportunitySAPService.updateOpportunityAcknowledgement(sapOpp);
            //System.assertEquals('Successfully processed', sapOppAck.DISTRIBUTION_STATUS);
            //System.assertEquals(null, sapOppAck.DIST_ERROR_DETAILS);      
            
            // reset error
            sapOppAck.DIST_ERROR_DETAILS = null;
            sapOppAck.DISTRIBUTION_STATUS = null;       
            
            // Delete Opportunity and again
            System.debug('*** testNotificationGetOpportunityAndAcknowledgement - delete Opportunity');
            delete tstOppty;
            sapOppAck = ws_OpportunitySAPService.updateOpportunityAcknowledgement(sapOpp);
            //System.assertEquals('Error in process', sapOppAck.DISTRIBUTION_STATUS);
            //System.assertEquals('Opportunity with Id \'' + tstOpptyId + '\' could not be found!', sapOppAck.DIST_ERROR_DETAILS);      
            
            // reset
            wsResponse.DISTRIBUTION_STATUS = null;
            wsResponse.DIST_ERROR_DETAILS = null;       
            
            System.debug('*** testNotificationGetOpportunityAndAcknowledgement - get Opportunity after delete');
            wsResponse = ws_OpportunitySAPService.getOpportunity(tstOppty.Id);
            //System.assertEquals('Error in process', sapOppAck.DISTRIBUTION_STATUS);
            //System.assertEquals('Opportunity with Id \'' + tstOpptyId + '\' could not be found!', sapOppAck.DIST_ERROR_DETAILS);      
                   
            Test.stopTest();

        }
    
    }

}