//---------------------------------------------------------------------------------------------------------------------------------------------------
//  Author		:    Bart Caelen
//  Date 		:    05/03/2018
//  Description	:    APEX Test Class for sc_Cvent_EMEA
//---------------------------------------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_sc_Cvent_EMEA {
	
	@isTest static void test_scheduling() {

        Test.startTest();

        //---------------------------------------
        // TEST SCHEDULING
        //---------------------------------------
        string tCRON_EXP = '0 0 0 3 9 ? 2099';
        
        string tJobId = System.schedule('ba_Cvent_Test_Campaign', tCRON_EXP, new sc_Cvent_EMEA());

        // Get the information from the CronTrigger API object
        CronTrigger oCronTrigger = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :tJobId];

        // Verify the expressions are the same
        System.assertEquals(tCRON_EXP, oCronTrigger.CronExpression);

        // Verify the job has not run
        System.assertEquals(0, oCronTrigger.TimesTriggered);

        // Verify the next time the job will run
        System.assertEquals('2099-09-03 00:00:00', String.valueOf(oCronTrigger.NextFireTime));
        //---------------------------------------

        Test.stopTest();

	}
	
}
//---------------------------------------------------------------------------------------------------------------------------------------------------