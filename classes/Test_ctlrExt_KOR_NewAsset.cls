/*  Test Class Name  - Test_ctlrExt_KOR_NewAsset
    Description  –  The class is written to cover 'ctlrExt_KOR_NewAsset'.
    Author - Peng Han
    Created Date  - 05/03/2014   
*/   
                    
//@isTest(seeAllData=true)
@isTest public with sharing class Test_ctlrExt_KOR_NewAsset {

    static testMethod void testKORAssetPage() {

    	//-BC - 20170621 - Replaced RunAs by an existing user by a RunAs by a new User - START
    	Id id_Profile = [SELECT Id FROM Profile WHERE Name = 'KOR Field Force Surgical Technologies' LIMIT 1][0].Id;
    	Id id_Role = [SELECT Id FROM UserRole WHERE DeveloperName = 'KOR_ST_ENT_FF' LIMIT 1][0].Id;

    	User testUser = clsTestData_User.createUser('testKOR001@medtronic.com.test', 'TestUser', 'Other Asia Pacific', 'South Korea', 'South Korea', id_Profile, id_Role, true);
//        User testUser = [Select Id from User where Profile.Name = 'KOR Field Force Surgical Technologies' AND isActive = true LIMIT 1];
    	//-BC - 20170621 - Replaced RunAs by an existing user by a RunAs by a new User - STOP

        System.runAs(testUser){
        
	        Asset a = new Asset();
		        a.name = 'test asset';

	        Account acc = new Account();
		        acc.name='test account new asset';
	        insert acc;
	        
	        Department_Info__c di = new Department_Info__c();
		        di.Account__c = acc.id;
	        insert di;
	        
	        ApexPages.StandardController sct = new ApexPages.StandardController(a);
	        ctlrExt_KOR_NewAsset controller = new ctlrExt_KOR_NewAsset(sct);
	        controller.getProducts();
	        controller.asset.name='test';
	        controller.dept = di;
	        controller.saveAsset();
	        controller.saveAndNewAsset();
        }      

    }

}