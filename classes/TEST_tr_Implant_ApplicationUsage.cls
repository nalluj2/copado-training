//--------------------------------------------------------------------------------------------------------------------
//  Author      : Bart Caelen
//  Version     : 1.0
//  Date        : 20150814
//  Description : This is the APEX Test class for the APEX Trigger tr_Implant_ApplicationUsage
//--------------------------------------------------------------------------------------------------------------------
@isTest
private class TEST_tr_Implant_ApplicationUsage {

    @TestSetup static void createTestData() {

        // Get System Administrator who will create the Test Data
        Profile oProfile = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' limit 1];
        System.assertNotEquals(oProfile, null);
        User oUser_SystemAdministrator = [SELECT Id FROM User WHERE ProfileId = :oProfile.Id AND IsActive = true limit 1];
        System.assertNotEquals(oUser_SystemAdministrator, null);

        // Get MMX Interface User
        User oUser_MMX = [SELECT Id FROM User WHERE Name = 'MMX Interface' limit 1];
        System.assertNotEquals(oUser_MMX, null);
        
        Business_Account_Owner__c settings = new Business_Account_Owner__c();
        settings.NAme = 'BELGIUM';
        settings.Owner_Id__c = '00520000002aG5m';
        insert settings;

        System.runAs(oUser_SystemAdministrator){
            // Company Data
            clsTestData.createCompanyData();

            // Business Unit Data
            clsTestData.createBusinessUnitData();

            // Sub Business Unit Data
            clsTestData.createSubBusinessUnitData();

            // Theraphy Group Data
            clsTestData.createTherapyGroupData();

            // Account Data
            clsTestData.iRecord_Account = 2;
            clsTestData.createAccountData();

            // Contact Data
            clsTestData.iRecord_Contact = 2;
            clsTestData.createContactData();

            // Call_Activity_Type__c Data
            clsTestData.iRecord_CallActivityType = 2;
            clsTestData.createCallActivityTypeData();

            // Call_Category__c Data
            clsTestData.iRecord_CallCategory = 2;
            clsTestData.createCallCategoryData();

            // Europe_Call_Record__c Data
            clsTestData.iRecord_EuropeCallRecord = 1;
            clsTestData.createEuropeCallRecordData();

            // Therapy Data
            clsTestData.iRecord_Therapy = 1;
            clsTestData.createTherapyData();

        }
    }   
	
	@isTest static void test_tr_Implant_ApplicationUsage() {
		
		User oUser_Admin = clsTestData_User.createUser_SystemAdministrator('tstadm1', false);
		User oUser = clsTestData_User.createUser('tstsusr1', clsUtil.getUserProfileId('EUR Field Force CVG'), clsUtil.getUserRoleId('UK FF (CVG)'), false);
		List<User> lstUser = new List<User>();
			lstUser.add(oUser);
			lstUser.add(oUser_Admin);
		insert lstUser;

		// Load Test Data
		List<Contact> lstContact = [SELECT Id, AccountId FROM Contact];
		List<Therapy__c> lstTherapy = [SELECT Id FROM Therapy__c];

		// Get MMX Interface User
		User oUser_MMX = [SELECT Id FROM User WHERE Name = 'MMX Interface' limit 1];
		System.assertNotEquals(oUser_MMX, null);

		// Verify that there are NO Application_Usage_Detail__c records
		List<Application_Usage_Detail__c> lstApplicationUsageDetail = [SELECT ID, Capability__c, Source__c, User_ID__c FROM Application_Usage_Detail__c];
		System.assertEquals(lstApplicationUsageDetail.size(), 0);

		Test.startTest();

		// Create an Implate like it was created by the MMX User Interface
		Implant__c oImplant = new Implant__c();

		System.runAs(oUser_MMX){
				oImplant.Implant_Implanting_Contact__c = lstContact[0].Id;
				oImplant.Implant_Implanting_Account__c = lstContact[0].AccountId;
				oImplant.Implant_Referring_Contact__c = lstContact[1].Id;
				oImplant.Implant_Referring_Account__c = lstContact[1].AccountId;
				oImplant.Implant_Therapy__c = lstTherapy[0].Id;
				oImplant.Therapy__c = lstTherapy[0].Id;
				oImplant.Attended_Implant_Text__c = 'Attended'; 
				oImplant.Duration_Nr__c = 10;
				oImplant.OwnerId = oUser.Id;
				oImplant.MMX_Implant_ID_Text__c = 'MMX_IMPLANT_ID';
			insert oImplant;
		}

		Test.stopTest();

		oImplant = [SELECT Id, CreatedDate FROM Implant__c WHERE Id = :oImplant.Id];

        
        // Validate as MMX user to avoid issues with the timezone discrepancies between System and MMX user
        System.runAs(oUser_MMX){
	        
	        // Verify that the Application_Usage_Detail__c is created
	        lstApplicationUsageDetail = 
	            [
	                SELECT 
	                    ID, Capability__c, Source__c, User_ID__c, Action_DateTime__c, Action_Date__c, Action_Type__c, Object__c 
	                FROM 
	                    Application_Usage_Detail__c
	            ];
	        System.assertEquals(lstApplicationUsageDetail.size(), 1);
	        System.assertEquals(lstApplicationUsageDetail[0].Capability__c, 'Login');
	        System.assertEquals(lstApplicationUsageDetail[0].Source__c, 'MMX');
	        System.assertEquals(lstApplicationUsageDetail[0].User_ID__c, oUser.Id);
	        System.assertEquals(lstApplicationUsageDetail[0].Action_DateTime__c, oImplant.CreatedDate);
	        System.assertEquals(lstApplicationUsageDetail[0].Action_Date__c, oImplant.CreatedDate.Date());
	        System.assertEquals(lstApplicationUsageDetail[0].Action_Type__c, 'Create');
	        System.assertEquals(lstApplicationUsageDetail[0].Object__c, 'Implant');        
        }
	}
	
}