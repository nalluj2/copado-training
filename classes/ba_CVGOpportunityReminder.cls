global class ba_CVGOpportunityReminder implements Schedulable, Database.Batchable<SObject>{
    
     // Schedulable
    global void execute(SchedulableContext ctx){

        Database.executeBatch(new ba_CVGOpportunityReminder(), 100);
    }

    global Database.QueryLocator start(Database.BatchableContext ctx){
    	
    	Date targetDate = Date.today().addDays(30);
    	
    	String query = 'Select Id, Name, OwnerId, Owner.isActive, Owner.Name, Account.Name, Account_Plan__r.Name from Opportunity where RecordType.DeveloperName = \'CVG_Project\' AND StageName = \'Closed Won\' AND Project_End_Date__c = :targetDate';
    	
    	return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext ctx, List<Opportunity> CVGopps){
    	
    	String backupAddress = Email_Addresses__c.getInstance('CVG Opportunity reminder').Email__c;
    	
    	List<Messaging.SingleEmailMessage> notifications = new List<Messaging.SingleEmailMessage>();
    	
    	for(Opportunity opp : CVGopps){
    	
    		Messaging.Singleemailmessage mail = new Messaging.Singleemailmessage();
		           
            mail.setTargetObjectId(opp.OwnerId);            
            mail.setSaveAsActivity(false);
                        
            if(opp.Owner.isActive == false){
            	
            	mail.setTreatTargetObjectAsRecipient(false);
            	mail.setToAddresses(new List<String>{backupAddress});
            }
            
            mail.setSubject('APDR Notification: Potential Sales opportunity');
            mail.setHTMLBody(generateEmailBody(opp));
                        
            notifications.add(mail);
    	}
    	
    	List<Messaging.SendEmailResult> results = Messaging.sendEmail(notifications, false);
    	
    	List<Messaging.SingleEmailMessage> retry = new List<Messaging.SingleEmailMessage>();
    	
    	for(Integer i = 0; i < results.size(); i++){
    		
    		Messaging.SendEmailResult res = results[i];
    		
    		if(res.isSuccess() == false){
    			
    			Messaging.SingleEmailMessage mail = notifications[i];
    			
    			mail.setToAddresses(new List<String>{backupAddress});
    			mail.setTreatTargetObjectAsRecipient(false);
    			
    			retry.add(mail);
    		}    		
    	}    
    	
    	if(retry.size() > 0) Messaging.sendEmail(retry, false);
    }
    
    global void finish(Database.BatchableContext BC){
    	
    }
    
    private static String generateEmailBody(Opportunity opp){
    	
    	String emailBody = '';
    	emailBody += 'Dear ' + opp.Owner.Name + ',<br/><br/>';

		emailBody += 'Your closed won Opportunity “'+ opp.Name +'” related to the Account “' + opp.Account.Name + '”';
		if(opp.Account_Plan__r != null) emailBody += ' (iPlan Number: “' + opp.Account_Plan__r.Name +'”)';	
		emailBody += ' is about to expire in the next 30 days.<br/>';
		emailBody += 'It might be the right time to work on the potential contract renewal and develop a new Sales opportunity in APDR.<br/><br/>';
		
		emailBody += 'You can click on the link below if you want to retrieve more information about your expiring opportunity directly in Salesforce.com:<br/>';
		emailBody += '<a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + opp.Id + '" target="_blank" >' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + opp.Id +'</a><br/><br/>';
		
		emailBody += 'Best Regards,<br/>';
		emailBody += 'Your CVG Commercial Excellence team';
    	
    	return emailBody;
    }
}