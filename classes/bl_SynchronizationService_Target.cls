/*	Integration Service related class
	
	This class contains a method to process the information received from the source Org.
*/
public without sharing class bl_SynchronizationService_Target {
	
	//Map with the supported objects (API name as key and specific implementation as value)
	private static Map<String, String> supportedObjects = new Map<String, String>{
						
		'System__c' => 'SynchronizationService_Asset',	
		'Installed_Software__c' => 'SynchronizationService_InstSoftware',
		'Case' => 'SynchronizationService_Case',
		'Complaint__c' => 'SynchronizationService_Complaint',	
		'Case_Comment__c' => 'SynchronizationService_Comment',	
		'Workorder__c' => 'SynchronizationService_Workorder',
		'Order_Return_Item__c' => 'SynchronizationService_Sparepart',
		'Parts_Shipment__c' => 'SynchronizationService_PartShipment',
		'Attachment' => 'SynchronizationService_Attachment'

	};
	
	//This method takes the JSON serialized information for a record and passes it to the corresponding implementation depending on the record type.
	//As a result it will return the Id of the record if it succeeded or null if there was an error.
	public static String processRecordPayload(String payload, String objectType){
				
		//If the object type is not found in the list of supported object, we finish here.
		String objectImplClass = supportedObjects.get(objectType);		
		if(objectImplClass == null) return null;
			
		SynchronizationService_Object objectImpl = (SynchronizationService_Object) System.Type.forName(objectImplClass).newInstance();		
		
		bl_SynchronizationService_Utils.isSyncProcess = true;
		
		return objectImpl.processPayload(payload);
	}
}