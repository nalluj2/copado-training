//@isTest(seeAllData = true)
@isTest private class TEST_AssetAfterInsert 
{

    static testMethod void myUnitTest()
    {
        
        Account a = new Account();
        a.name='Account';
        a.SAP_ID__c='Standard P';
        a.SAP_Customer_Group__c='DISTRIBUTOR';
        insert a;
         //Insert Company
       Company__c cmpny = new Company__c();
       cmpny.name='TestMedtronic';
       cmpny.CurrencyIsoCode = 'EUR';
       cmpny.Current_day_in_Q1__c=56;
       cmpny.Current_day_in_Q2__c=34; 
       cmpny.Current_day_in_Q3__c=5; 
       cmpny.Current_day_in_Q4__c= 0;   
       cmpny.Current_day_in_year__c =200;
       cmpny.Days_in_Q1__c=56;  
       cmpny.Days_in_Q2__c=34;
       cmpny.Days_in_Q3__c=13;
       cmpny.Days_in_Q4__c=22;
       cmpny.Days_in_year__c =250;
       cmpny.Company_Code_Text__c = 'T41';
       insert cmpny;     


         Business_Unit__c bu = new Business_Unit__c();
        bu.name = 'Testing BU';
        bu.abbreviated_name__c = 'Abb Bu';
        bu.Company__c = cmpny.Id;
        insert bu;

         //Insert SBU
        Sub_Business_Units__c sbu1 = new Sub_Business_Units__c();
        sbu1.name='SBUMedtronic1';
        sbu1.Business_Unit__c=bu.id;
        insert sbu1;

        //Insert Therapy Group
        Therapy_Group__c tg = new Therapy_Group__c();
        tg.Name='Therapy Group';
        tg.Company__c = cmpny.Id;
        tg.Sub_Business_Unit__c = sbu1.Id;
        insert tg;

        map<String, String> RecrodTypeMapping = new map<String, String>();
        list<Recordtype> rt = [Select Id,Name From RecordType where SobjectType='product2' and IsActive=true and name='MDT Marketing Product'];
        for(RecordType r:rt)
        {
            RecrodTypeMapping.put(string.ValueOf(r.Name),string.ValueOf(r.Id));
        }

        String RecType=RecrodTypeMapping.get('MDT Marketing Product');
        Product2 pro1 = new Product2();
        pro1.Name = 'Test Product';
        pro1.Business_Unit_ID__c = bu.id;
        pro1.Consumable_Bool__c = true;
        pro1.IsActive = true;
        //pro1.DefaultPrice=12.0;
        pro1.recordtypeid = RecType;
      //  pro1.Manufacturer_Account_ID__c = '001P000000R0kKd'; 
        insert pro1;

        Therapy__c th = new Therapy__c();
        th.name='test ## th';
        th.Business_Unit__c = bu.id;
        th.Sub_Business_Unit__c = sbu1.Id;
        th.Therapy_Group__c = tg.Id;
        th.Therapy_Name_Hidden__c = 'test ## th';
        insert th;

       /// implant.Implant_Implanting_Contact__c = testContacts.get(1).Id;
       // implant.Implant_Implanting_Account__c = testAccounts.get(0).Id;
        implant__c implant = new implant__c();
        implant.Implant_Date_Of_Surgery__c = System.today() - 1;
        implant.Therapy__c = th.Id;
        implant.Attended_Implant_Text__c = 'UnAttended';
        implant.Duration_Nr__c = 10;
        implant.distributor__c=a.id;
        implant.Procedure_Text__c='Replacement';
        //implant.Manufacturer_ID__c = '0012000000kvvDU';  // 001P000000R0kKd // Staging Medtronic Competitor Account Id
        insert implant ;            
        
        //Pricebook2 pb = new Pricebook2();
        //pb.name = a.SAP_ID__c;
        //pb.isStandard=true;
        //insert pb;
//        list<Pricebook2> pb = [select id from Pricebook2 where name =:'Standard Price Book' limit 1];
        Id id_Pricebook_Std = Test.getStandardPricebookId();


        PricebookEntry pbe = new PricebookEntry();
        pbe.Product2Id = pro1.id;
        pbe.UseStandardPrice=false;
        pbe.UnitPrice=20.0;
//        pbe.Pricebook2Id ='01s200000001NG6AAM';
//        pbe.Pricebook2Id =pb[0].id;
        pbe.Pricebook2Id = id_Pricebook_Std;
        pbe.IsActive = true;  
        insert pbe;

//PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = standardPB.Id, Product2Id = prod.Id, UnitPrice = 10000, IsActive = true, UseStandardPrice = false);
       // insert standardPrice;
        asset ast = new asset();
        ast.Product2Id = pro1.id;
        ast.name='assset1';
        ast.accountid=a.id;
        ast.Serial_Nr__c = clsUtil.getNewGUID();
        //ast.Implant_ID__c=implant.id;
        insert ast;

        ast.price=pbe.UnitPrice;
        ast.CurrencyIsoCode = pbe.CurrencyIsoCode;
        update ast;
        }

}