/**
 *  Creation Date : 27/05/2009  
 *	Description :  Trigger : After Insert a campaign, link the Campaign Business Unit of the user to it.
 * 	Author : ABSI / Miguel Coimbra
 *
 **/

public without sharing class triggerCampaign {
	public static void beforeInsertUpdate(Campaign[] firedCampaigns){
		// For all fired Campaigns, change the Business Unit : 
		for (Campaign c : firedCampaigns){
			c.Business_Unit__c = SharedMethods.getBUlookup();
		}		
	}
}