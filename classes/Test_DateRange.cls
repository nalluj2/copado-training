/*
Description: This class is used to test the DateRange class
Author - Rudy De Coninck
Created Date  - 9/10/2014
*/
@isTest
private class Test_DateRange {

    static testMethod void testOverlap1() {
    	
    	DateRange d1 = new DateRange(DateTime.newInstance(2012,12,1,0,0,0).date(),DateTime.newInstance(2012,12,10,0,0,0).date());
    	DateRange d2 = new DateRange(DateTime.newInstance(2012,12,9,0,0,0).date(),DateTime.newInstance(2012,12,20,0,0,0).date());
    	System.debug('D1'+d1.toAString());
    	System.debug('D2'+d2.toAString());
    	System.assert(d2.overlaps(d1)); 
    	
    }
    
    static testMethod void testOverlap2() {
    	
    	DateRange d1 = new DateRange(DateTime.newInstance(2012,12,1,0,0,0).date(),null);
    	DateRange d2 = new DateRange(DateTime.newInstance(2012,12,9,0,0,0).date(),DateTime.newInstance(2012,12,20,0,0,0).date());
    	System.debug('D1'+d1.toAString());
    	System.debug('D2'+d2.toAString());
    	System.assert(d2.overlaps(d1)); 
    	
    }
    
    static testMethod void testNoOverlap1() {
    	
    	DateRange d1 = new DateRange(DateTime.newInstance(2012,12,21,0,0,0).date(),null);
    	DateRange d2 = new DateRange(DateTime.newInstance(2012,12,9,0,0,0).date(),DateTime.newInstance(2012,12,20,0,0,0).date());
    	System.debug('D1'+d1.toAString());
    	System.debug('D2'+d2.toAString());
    	System.assert(!d2.overlaps(d1)); 
    	
    }
}