@isTest private class TEST_ctrlExt_ManageStakeholderEmployee {

	@isTest private static void testName() {

		// Create Test Data
		clsTestData_Account.createAccount();
		clsTestData_Contact.createContact();

		clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'EMEA_IHS_Managed_Services').Id;
		clsTestData_Opportunity.iRecord_Opportunity = 1;
		List<Opportunity> lstOpportunity1 = clsTestData_Opportunity.createOpportunity();

		clsTestData_Opportunity.idRecordType_StakeholderMapping = clsUtil.getRecordTypeByDevName('Stakeholder_Mapping__c', 'EMEA_IHS_Stakeholder_Mapping').Id;
		clsTestData_Opportunity.iRecord_StakeholderMapping = 5;
		List<Stakeholder_Mapping__c> lstStakeholderMapping1 = clsTestData_Opportunity.createStakeholderMapping();

		clsTestData_Opportunity.iRecord_Opportunity = 1;
		clsTestData_Opportunity.oMain_Opportunity = null;
		List<Opportunity> lstOpportunity2 = clsTestData_Opportunity.createOpportunity();

		System.assert(lstOpportunity1.size() == 1);
		System.assert(lstOpportunity2.size() == 1);
		
		List<Stakeholder_Employee_Relationship__c> lstEmployeeRelationship = [SELECT Id FROM Stakeholder_Employee_Relationship__c];
		System.assert(lstEmployeeRelationship.size() == 0);

		lstStakeholderMapping1 = [SELECT Id FROM Stakeholder_Mapping__c WHERE Opportunity__c = :lstOpportunity1[0].Id];
		System.assert(lstStakeholderMapping1.size() == 5);
		List<Stakeholder_Mapping__c> lstStakeholderMapping2 = [SELECT Id FROM Stakeholder_Mapping__c WHERE Opportunity__c = :lstOpportunity2[0].Id];
		System.assert(lstStakeholderMapping2.size() == 0);

		// Execute Logic
		Test.startTest();

			// Existing Stakeholder Mapping - new Employee Relationships
            PageReference oPageReference1 = new PageReference('/ManageStakeholderEmployee?Id=' + lstOpportunity1[0].Id + '&smid=' + lstStakeholderMapping1[0].Id);
            Test.setCurrentPage(oPageReference1);
				oPageReference1.getParameters().put('id', lstOpportunity1[0].Id);
				oPageReference1.getParameters().put('smid', lstStakeholderMapping1[0].Id);
				oPageReference1.getParameters().put('returnid', lstOpportunity1[0].Id);

	        ApexPages.StandardController oSTDCTRL1 = new ApexPages.StandardController(lstOpportunity1[0]);
	        ctrlExt_ManageStakeholderEmployee oCTRL1 = new ctrlExt_ManageStakeholderEmployee(oSTDCTRL1);

			// Update Stakeholder Mapping fields
			oCTRL1.oStakeholderMapping.Contact__c = clsTestData_Contact.oMain_Contact.Id;
			oCTRL1.oStakeholderMapping.Role__c = 'Buyer';
			oCTRL1.oStakeholderMapping.Unqualified_Referral__c = true;
			oCTRL1.oStakeholderMapping.Transition_Lite_Meeting__c = Date.today();
			oCTRL1.oStakeholderMapping.Influence_in_purchasing_decision__c = '1';
			oCTRL1.oStakeholderMapping.Relationship_with_Medtronic_IHS__c = '1';
			oCTRL1.oStakeholderMapping.Stakeholder_Type__c = 'Clinical';

			// Populate Employee Relationship fields
			oCTRL1.addEmployeeRelationship();
			System.assertEquals(oCTRL1.mapEmployeeRelationship.size(), 1);

			oCTRL1.save();
			
			oCTRL1.mapEmployeeRelationship.values()[0].oEmployeeRelationship.Relationship_Quality__c = '1';

			oCTRL1.saveAndNew();

			lstEmployeeRelationship = [SELECT Id FROM Stakeholder_Employee_Relationship__c WHERE MDT_Contact__c = :lstStakeholderMapping1[0].Id];
			System.assertEquals(lstEmployeeRelationship.size(), oCTRL1.lstEmployeeRelationship.size(), 1);
	


			// New Stakeholder Mapping - new Employee Relationships
            PageReference oPageReference2 = new PageReference('/ManageStakeholderEmployee?Id=' + lstOpportunity2[0].Id);
            Test.setCurrentPage(oPageReference2);
				oPageReference2.getParameters().put('id', lstOpportunity1[0].Id);

	        ApexPages.StandardController oSTDCTRL2 = new ApexPages.StandardController(lstOpportunity2[0]);
	        ctrlExt_ManageStakeholderEmployee oCTRL2 = new ctrlExt_ManageStakeholderEmployee(oSTDCTRL2);

			// Populate Stakeholder Mapping fields
			oCTRL2.oStakeholderMapping.Contact__c = clsTestData_Contact.oMain_Contact.Id;
			oCTRL2.oStakeholderMapping.Role__c = 'Buyer';
			oCTRL2.oStakeholderMapping.Unqualified_Referral__c = true;
			oCTRL2.oStakeholderMapping.Transition_Lite_Meeting__c = Date.today();
			oCTRL2.oStakeholderMapping.Influence_in_purchasing_decision__c = '1';
			oCTRL2.oStakeholderMapping.Relationship_with_Medtronic_IHS__c = '1';
			oCTRL2.oStakeholderMapping.Stakeholder_Type__c = 'Clinical';

			// Populate Employee Relationship fields
			oCTRL2.addEmployeeRelationship();
			oCTRL2.addEmployeeRelationship();
			oCTRL2.addEmployeeRelationship();
			System.assertEquals(oCTRL2.mapEmployeeRelationship.size(), 3);
			oCTRL2.mapEmployeeRelationship.values()[0].oEmployeeRelationship.Relationship_Quality__c = '1';
			oCTRL2.mapEmployeeRelationship.values()[1].oEmployeeRelationship.Relationship_Quality__c = '2';
			oCTRL2.mapEmployeeRelationship.values()[2].oEmployeeRelationship.Relationship_Quality__c = '3';

			// Delete the first Employee Relationship
			oCTRL2.iIndex_EmployeeRelationship = 1;
			oCTRL2.deleteSoft();

			System.assertEquals(oCTRL2.mapEmployeeRelationship.size(), 2);

			oCTRL2.save();

			lstStakeholderMapping2 = [SELECT Id FROM Stakeholder_Mapping__c WHERE Opportunity__c = :lstOpportunity2[0].Id];
			System.assert(lstStakeholderMapping2.size() == 1);

			lstEmployeeRelationship = [SELECT Id FROM Stakeholder_Employee_Relationship__c WHERE MDT_Contact__c = :lstStakeholderMapping2[0].Id];
			System.assert(lstEmployeeRelationship.size() == 2);

			oCTRL2.cancel();



			// Existing Stakeholder Mapping - Existing Employee Relationships
            PageReference oPageReference3 = new PageReference('/ManageStakeholderEmployee?Id=' + lstOpportunity2[0].Id + '&smid=' + lstStakeholderMapping2[0].Id);
            Test.setCurrentPage(oPageReference3);
				oPageReference3.getParameters().put('id', lstOpportunity2[0].Id);
				oPageReference3.getParameters().put('smid', lstStakeholderMapping2[0].Id);
				oPageReference3.getParameters().put('returnid', lstOpportunity2[0].Id);

	        ApexPages.StandardController oSTDCTRL3 = new ApexPages.StandardController(lstOpportunity2[0]);
	        ctrlExt_ManageStakeholderEmployee oCTRL3 = new ctrlExt_ManageStakeholderEmployee(oSTDCTRL3);

			// Update Employee Relationship fields
			System.assertEquals(oCTRL3.mapEmployeeRelationship.size(), 2);
			System.assertEquals(oCTRL3.lstEmployeeRelationship_Delete.size(), 0);

			// Delete the first Employee Relationship
			oCTRL3.iIndex_EmployeeRelationship = 1;
			oCTRL3.deleteSoft();
			System.assertEquals(oCTRL3.mapEmployeeRelationship.size(), 1);
			System.assertEquals(oCTRL3.lstEmployeeRelationship_Delete.size(), 1);

			oCTRL3.mapEmployeeRelationship.values()[0].oEmployeeRelationship.Relationship_Quality__c = '5';

			oCTRL3.save();

			List<Stakeholder_Mapping__c> lstStakeholderMapping3 = [SELECT Id FROM Stakeholder_Mapping__c WHERE Opportunity__c = :lstOpportunity2[0].Id];
			System.assert(lstStakeholderMapping3.size() == 1);

			lstEmployeeRelationship = [SELECT Id FROM Stakeholder_Employee_Relationship__c WHERE MDT_Contact__c = :lstStakeholderMapping3[0].Id];
			System.assert(lstEmployeeRelationship.size() == 1);

		Test.stopTest();


		// Validate Result
	}
}