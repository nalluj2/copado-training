/**
 *	Created Date : 20091005
 *	Description : This class will be called by the different Submit Buttons. On VF pages & Dashboard. 
 *					Sales, Competitors & Campaigns ...
 *	Author : Miguel Coimbra / ABSI
 * 	 
 */
 
 
 public with sharing class DIB_Manage_Task_Status {
	
	
	private enum toCheckType {SALES, COMPETITOR, CAMPAIGN}
	
	
	/**
	 *	Submit Sales Allocation for a certain Fiscal Month & a certain Fiscal Year 
	 */
	public static Set<Id> getAccountsFromInvoiceLines (String month, String year){				
		// Get all the invoice Lines ( with all the accounts) for the chosen month & year :		
		DIB_Invoice_Line__c[] dibILsALL = DIB_AllocationSharedMethods.getInvoiceLinesPerPeriode(month, year, true, false);		
		
		// Get current User SAP Id 
		String 		currentUserId 		= System.Userinfo.getUserId();
		String 		currentUserSAPId 	; 
		currentUserSAPId = [ Select u.SAP_ID__c From User u where id =:currentUserId limit 1].SAP_ID__c ; 
		
		// Dedup all the Account Ids : 
		Set<Id> sold_To_Accounts = new Set<Id>{} ;	
		
		if (currentUserSAPId != null){ 	
			for (DIB_Invoice_Line__c il : dibILsALL){
				System.debug('#################### Submit InvoiceLine il.Sold_To__r.DIB_SAP_Sales_Rep_Id__c:' + il.Sold_To__r.DIB_SAP_Sales_Rep_Id__c +  '######################################') ;
				System.debug('#################### Submit InvoiceLine currentUserSAPId:' + currentUserSAPId +  '######################################') ;
				//TO DO --> ENABLE THIS AFTER ( not enable for TESTING ): Get only the accounts where the Primary Sales Rep = Curent user
				if (il.Sold_To__r.DIB_SAP_Sales_Rep_Id__c == currentUserSAPId){					
					sold_To_Accounts.add(il.Sold_To__c) ;
				}  
			}
		}
		return sold_To_Accounts ; 		
	}	
	
	
	/**
	 *	Submit Competitor  & Campaign for a certain Fiscal Month & a certain Fiscal Year 
	 */
	 
	
	public static Set<Id> getAccountsFromSegmentation (String month, String year){
		
		Set<Id> sold_To_Accounts = new Set<Id>{} ;
		// Get current User SAP Id 
		String 		currentUserId 		= System.Userinfo.getUserId();
		String 		currentUserSAPId 	; 
		currentUserSAPId = [ Select u.SAP_ID__c From User u where id =:currentUserId limit 1].SAP_ID__c ; 
		
		for (
		DIB_Department__c seg : [SELECT d.Account__r.isSales_Force_Account__c, d.Account__c, d.Account__r.DIB_Sales_Rep__c, 
											d.Account__r.DIB_SAP_Sales_Rep_Id__c
											FROM DIB_Department__c d
											//WHERE d.department__c =: SelectedDepartment 
											WHERE  d.Account__r.isSales_Force_Account__c = true
											AND Account__r.DIB_SAP_Sales_Rep_Id__c =: currentUserSAPId
											LIMIT 1000
											]) {
												if (seg.Account__r.DIB_SAP_Sales_Rep_Id__c == currentUserSAPId){
													sold_To_Accounts.add(seg.Account__c) ;
												}	
											}			
		System.debug('#################### getAccountsFromSegmentation sold_To_Accounts:' + sold_To_Accounts.size() +  '######################################') ;		
		return sold_To_Accounts;
	}
	
	
	public static Boolean submitCompetitor(String month, String year){	
		Set<Id> sold_To_Accounts = getAccountsFromSegmentation(month, year) ; 
		// This will upsert all the related Submit Status : 		
		return manageStatus(month, year, sold_To_Accounts, toCheckType.COMPETITOR);
		
		
	}
	 
	public static Boolean submitCampaign(String month, String year){
		Set<Id> sold_To_Accounts = getAccountsFromSegmentation(month, year) ; 
		// This will upsert all the related Submit Status : 		
		return manageStatus(month, year, sold_To_Accounts, toCheckType.CAMPAIGN) ; 	
	}
	
	
	/**
	 *	Submit Sales Entry Aggregated Allocation for a certain Fiscal Month & a certain Fiscal Year 
	 */
	
	public static Boolean submitSalesEntry(String month, String Year){
		Set<Id> sold_To_Accounts = getAccountsFromSegmentation(month, year) ; 
		return manageStatus(month, year, sold_To_Accounts, toCheckType.SALES) ; 			
	}
	
	public static Boolean submitSales(String month, String Year){	
		//Set<Id> sold_To_Accounts = getAccountsFromInvoiceLines(month, year);
		Set<Id> sold_To_Accounts = getAccountsFromSegmentation(month, year);
		// This will upsert all the related Submit Status : 		
		return manageStatus(month, year, sold_To_Accounts, toCheckType.SALES) ; 			
	}	
	
	
	public static Boolean manageStatus(String month, String year, Set<Id> accounts, toCheckType f){
		Boolean result;
		//manageStatusPerAccount(month, year,accounts,f);
		result = manageStatusSummary(month, year,f);
		return result;
	}
	
	/**
	 *	A task status has 3 checkboxes SALES, COMPETITOR, CAMPAIGNS
	 *  This method will create a nem Task status when it's not available yet ( The first time ) or update the existing status. 
	 *
	 *	@parameters : 
	 *			month : Fiscal month 
	 *			year = Fical Year
	 *			Account = Account ID
	 *			f = toCheckType is an enum with the following available values : (SALES, CPMPETITOR, CAMPAIGN) 
	 *			Depending on which type, it will check the checkbox. ;  
	 */	
	
	/*public static Boolean manageStatusPerAccount(String month, String year, Set<Id> accounts, toCheckType f){
		
		// Get the Id of the Current User : 
		Id currentUserId = System.Userinfo.getUserId();		
		
		DIB_Submit_Status__c[] taskStatusToUpsert = new DIB_Submit_Status__c[]{} ; 
		DIB_Submit_Status__c[] newTaskStatus= new DIB_Submit_Status__c[]{} ; 
		
		List<DIB_Submit_Status__c> existingStatus = new List<DIB_Submit_Status__c>{} ;
		
		// query the existing Task Status : 
		existingStatus = [SELECT d.Sales__c, d.Sales_Rep__c, d.Id, d.Fiscal_Year__c, d.Fiscal_Month__c, d.Competitor_Sales__c, d.Campaigns__c, d.Account__c
							FROM DIB_Submit_Status__c d
							WHERE d.Account__c in : accounts
							AND d.Fiscal_Month__c =: month 
							AND d.Fiscal_Year__c =: year
							AND d.Sales_Rep__c =: currentUserId] ; 
		 
		// Create new Task Status (to be able to insert them if it doesn't exist yet) These items will only be created if they don't exist yet ! 
		for (Id a : accounts){
			DIB_Submit_Status__c ts = new DIB_Submit_Status__c() ; 
			ts.Sales_Rep__c = currentUserId ;   
			ts.Fiscal_Year__c = year ; 
			ts.Fiscal_Month__c = month ; 					
			ts.Account__c = a ;			
			if (f == toCheckType.SALES)
				ts.Sales__c = true ; 
			if (f == toCheckType.COMPETITOR)
				ts.Competitor_Sales__c = true ; 
			if (f == toCheckType.CAMPAIGN)
				ts.Campaigns__c = true ;
												
			newTaskStatus.add(ts) ; 
		}
		
		// If already exists, update the item and add it to the list taskStatusToUpsert, if it doesn't exists yet, add the dummy previously created. 
		for (DIB_Submit_Status__c newST :  newTaskStatus ){
			Boolean existsAlready = false ; 
			for (DIB_Submit_Status__c existST : existingStatus ){
				if (existST.Account__c == newST.Account__c	&& existST.Sales_Rep__c == newST.Sales_Rep__c 
					&& existST.Fiscal_Year__c == newST.Fiscal_Year__c && existST.Fiscal_Month__c == newST.Fiscal_Month__c ){
						// ### exists already, update the item Add it to be upserted
						if (f == toCheckType.SALES)
							existST.Sales__c = true ; 
						if (f == toCheckType.COMPETITOR)
							existST.Competitor_Sales__c = true ; 
						if (f == toCheckType.CAMPAIGN)
							existST.Campaigns__c = true ;						
						 
						taskStatusToUpsert.add(existST) ; 	
						existsAlready= true ;						 
						break ; 					
					}					
			}
			if (existsAlready == false){
				// ### doesn't exists yet. Therefore create a new one. 
				taskStatusToUpsert.add(newST) ;
			}			
		}
		
		try {
			System.debug('#################### taskStatusToUpsert.size(): ' + taskStatusToUpsert.size() +  '######################################') ;
			upsert taskStatusToUpsert  ; 
		}catch(DMLException e){
			ApexPages.addMessages(e);
			return false;
		}
		return true;
	}*/

	
	/**
	 *	A status summary has 3 checkboxes SALES, COMPETITOR, CAMPAIGNS
	 *  This method will create a nem Task status when it's not available yet ( The first time ) or update the existing status. 
	 *
	 *	@parameters : 
	 *			month : Fiscal month 
	 *			year = Fical Year
	 *			f = toCheckType is an enum with the following available values : (SALES, COMPETITOR, CAMPAIGN) 
	 *			Depending on which type, it will check the checkbox. ;  
	 */	
	
	public static Boolean manageStatusSummary(String month, String year, toCheckType f){
		
		// Get the Id of the Current User : 
		Id currentUserId = System.Userinfo.getUserId();
		
		DIB_Submit_Summary__c submitSummaryToUpsert = new DIB_Submit_Summary__c();
		List<DIB_Submit_Summary__c> existingSubmitSummaryList = new List<DIB_Submit_Summary__c>{};
		
		// query the existing Submit Summary: 
		existingSubmitSummaryList = [SELECT d.Sales__c, d.Sales_Rep__c, d.OwnerId, d.Name, d.Id, d.Fiscal_Year__c, 
							d.Fiscal_Start_Date__c, d.Fiscal_Month__c,
							d.Competitor_Sales__c, d.Campaigns__c
							FROM DIB_Submit_Summary__c d
							WHERE d.Fiscal_Month__c =: month 
							AND d.Fiscal_Year__c =: year
							AND d.Sales_Rep__c =: currentUserId] ; 
		
		// Get the first one from the list
		if (existingSubmitSummaryList.size() > 0) {
			submitSummaryToUpsert = existingSubmitSummaryList.get(0);
		}
		else {
			submitSummaryToUpsert.Fiscal_Month__c = month;
			submitSummaryToUpsert.Fiscal_Year__c = year;
			submitSummaryToUpsert.Sales_Rep__c = currentUserId;			
		}
		
		if (f == toCheckType.SALES)
			submitSummaryToUpsert.Sales__c = true;
		if (f == toCheckType.COMPETITOR)
			submitSummaryToUpsert.Competitor_Sales__c = true; 
		if (f == toCheckType.CAMPAIGN)
			submitSummaryToUpsert.Campaigns__c = true;
		
		
		// get the civil calendar date that indicates when the fiscal period starts
		List <DIB_Fiscal_Period__c> fiscalPeriods = [
				SELECT d.Start_Date__c, d.Id, d.Fiscal_Year__c, d.Fiscal_Month__c, d.End_Date__c 
				FROM DIB_Fiscal_Period__c d	
				WHERE d.Fiscal_Month__c =: month
				AND d.Fiscal_Year__c =: year];
			
		System.debug('#####fiscalPeriods :' + fiscalPeriods); 
		if (fiscalPeriods.size() > 0) {
			DIB_Fiscal_Period__c myFiscal_Period = fiscalPeriods.get(0);
			submitSummaryToUpsert.Fiscal_Start_Date__c = myFiscal_Period.Start_Date__c;
		}

		try {
			System.debug('#################### submitSummaryToUpsert: ' + submitSummaryToUpsert +  '######################################') ;
			upsert submitSummaryToUpsert  ; 
		} catch(DMLException e){
			ApexPages.addMessages(e);
			return false;
		}
		return true;				
	}


	/**
	 * 	Show / Hide the button Submit 
	 *	Submit button only available once fiscal month closed +5 Day
	 *												   CHANGED TO +1 DAY 10072010 BC
	 *		
	 */

	public static Boolean showHideSubmitButton(String fMonth, String fYear){
		System.debug('################## Begin showHideSubmitButton #################') ;
		System.debug('################## fMonth ' + fMonth + ' #################') ;
		System.debug('################## fYear ' + fYear + ' #################') ;
		System.debug('################## Begin showHideSubmitButton #################') ;
		Integer fs = [Select count() From DIB_Fiscal_Period__c d
				where d.Fiscal_Month__c =: fMonth 
				and d.Fiscal_Year__c =: fYear
				and d.End_Date__c <=:  System.today() - 1
				]; 
		System.debug('################## fs : '+ fs + '#################') ;
		System.debug('################## Begin showHideSubmitButton #################') ;
		if (fs > 0){
			// can show
			return true ; 
		}else{
			// cannot show
			return false ; 
		}
		
	}
	

}