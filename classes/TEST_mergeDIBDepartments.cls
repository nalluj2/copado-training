/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TEST_mergeDIBDepartments {

    static testMethod void myUnitTest() {
    	
        Account account1 = new Account();
        account1.Name = 'Test Account 1';
        account1.isSales_Force_Account__c = true;
        
        Account account2 = new Account();
        account2.Name = 'Test Account 2';
        account2.isSales_Force_Account__c = true;
        
        Account competitor = new Account() ; 
    	competitor.name = 'ANIMAs' ;
    	competitor.CGMs__c = true;
    	competitor.Pumps__c = true;
    	competitor.RecordTypeId = FinalConstants.ACCOUNT_COMPETITOR_RECORDTYPE_ID;
        
        insert new List<Account>{account1, account2, competitor};
      
    	DIB_Campaign_Program__c campaignProgram = new DIB_Campaign_Program__c();
    	campaignProgram.Name = 'Test Campain Program';
    	insert campaignProgram;
    	
    	DIB_Global_Campaign__c globalCampain = new DIB_Global_Campaign__c();
    	globalCampain.Name = 'Test Global Campain';
    	globalCampain.DIB_Campaign_Program__c = campaignProgram.Id;
    	insert globalCampain;
    	
    	DIB_Campaign__c localCampain = new DIB_Campaign__c();
    	localCampain.Name = 'Test Local Campain';
    	localCampain.Global_Campaign__c = globalCampain.Id;
    	localCampain.Start_Fiscal_Year__c = '2019';
    	localCampain.Start_Fiscal_Month_FullMonthName__c = 'January';
    	localCampain.End_Fiscal_Year__c = '2020';
    	localCampain.End_Fiscal_Month_FullMonthName__c = 'January';
    	insert localCampain;
      
    	DIB_Campaign__c localCampain2 = new DIB_Campaign__c();
    	localCampain2.Name = 'Test Local Campain 2';
    	localCampain2.Global_Campaign__c = globalCampain.Id;
    	localCampain2.Start_Fiscal_Year__c = '2019';
    	localCampain2.Start_Fiscal_Month_FullMonthName__c = 'January';
    	localCampain2.End_Fiscal_Year__c = '2020';
    	localCampain2.End_Fiscal_Month_FullMonthName__c = 'January';
    	insert localCampain2;      
      
        Department_Master__c departmentMaster1 = new Department_Master__c();
        departmentMaster1.Name = 'Adult Type 1';
        
        Department_Master__c departmentMaster2 = new Department_Master__c();
        departmentMaster2.Name = 'Adult Type 2';
        
        Department_Master__c departmentMaster3 = new Department_Master__c();
        departmentMaster3.Name = 'Pediatric';
        
        insert new List<Department_Master__c>{departmentMaster1, departmentMaster2, departmentMaster3}; 
        
        DIB_Department__c dibDepartment1 = new DIB_Department__c();
        dibDepartment1.Account__c = account1.Id;
        dibDepartment1.Active__c = true;
        dibDepartment1.Department_ID__c = departmentMaster1.Id;
                
        DIB_Department__c dibDepartment2 = new DIB_Department__c();
        dibDepartment2.Account__c = account1.Id;
        dibDepartment2.Active__c = true;
        dibDepartment2.Department_ID__c = departmentMaster2.Id;
        
        DIB_Department__c dibDepartment3 = new DIB_Department__c();
        dibDepartment3.Account__c = account2.Id;
        dibDepartment3.Active__c = true;
        dibDepartment3.Department_ID__c = departmentMaster1.Id;
        
        DIB_Department__c dibDepartment4 = new DIB_Department__c();
        dibDepartment4.Account__c = account2.Id;
        dibDepartment4.Active__c = true;
        dibDepartment4.Department_ID__c = departmentMaster3.Id;
        
        insert new List<DiB_Department__c>{dibDepartment1, dibDepartment2, dibDepartment3, dibDepartment4};
        
        //Aggregated Allocation
        DIB_Aggregated_Allocation__c dibAggregatedAllocation = new DIB_Aggregated_Allocation__c();
        dibAggregatedAllocation.DIB_Department_ID__c = dibDepartment3.Id;
        dibAggregatedAllocation.Fiscal_Year__c = '2011';
        dibAggregatedAllocation.Fiscal_Month__c = '11';     
        insert dibAggregatedAllocation;		
				
		//Competitor Installed Base        
        DIB_Competitor_iBase__c dibCompetitoriBase = new DIB_Competitor_iBase__c();
        dibCompetitoriBase.Account_Segmentation__c = dibDepartment3.Id;
		dibCompetitoriBase.Current_IB__c = 10 ;   	    		
		dibCompetitoriBase.Competitor_Account_ID__c = competitor.Id ; 
		dibCompetitoriBase.Sales_Force_Account__c = account2.Id ;    		 
		dibCompetitoriBase.Fiscal_year__c = '2011' ; 
		dibCompetitoriBase.Fiscal_Month__c = '11' ;
		dibCompetitoriBase.IB_Change__c = 23 ;
		dibCompetitoriBase.Type__c = 'Pumps';	        
        insert dibCompetitoriBase;
		
		//Invoice Line
      	DIB_Invoice_Line__c dibInvoiceLine = new DIB_Invoice_Line__c();
        dibInvoiceLine.Department_ID__c = dibDepartment3.Id;
        dibInvoiceLine.Sold_To__c = account2.Id;
        insert dibInvoiceLine;
        
		//Campaign Accounts
        DIB_Campaign_Account__c dibCampaignAccount = new DIB_Campaign_Account__c();
        dibCampaignAccount.Department_ID__c = dibDepartment3.Id;
        dibCampaignAccount.Campaign__c = localCampain.Id;
        dibCampaignAccount.Account__c = account2.Id;
        dibCampaignAccount.Start_Fiscal_Month__c = '01';
        dibCampaignAccount.Start_Fiscal_Year__c = '2011';
        
        DIB_Campaign_Account__c dibCampaignAccount2 = new DIB_Campaign_Account__c();
        dibCampaignAccount2.Department_ID__c = dibDepartment3.Id;
        dibCampaignAccount2.Campaign__c = localCampain.Id;
        dibCampaignAccount2.Account__c = account2.Id;
        dibCampaignAccount2.Start_Fiscal_Month__c = '02';
        dibCampaignAccount2.Start_Fiscal_Year__c = '2011';
        
        DIB_Campaign_Account__c dibCampaignAccount3 = new DIB_Campaign_Account__c();
        dibCampaignAccount3.Department_ID__c = dibDepartment3.Id;
        dibCampaignAccount3.Campaign__c = localCampain2.Id;
        dibCampaignAccount3.Account__c = account2.Id;
        dibCampaignAccount3.Start_Fiscal_Month__c = '01';
        dibCampaignAccount3.Start_Fiscal_Year__c = '2011';
        
        DIB_Campaign_Account__c dibCampaignAccount4 = new DIB_Campaign_Account__c();
        dibCampaignAccount4.Department_ID__c = dibDepartment1.Id;
        dibCampaignAccount4.Campaign__c = localCampain.Id;
        dibCampaignAccount4.Account__c = account1.Id;
        dibCampaignAccount4.Start_Fiscal_Month__c = '03';
        dibCampaignAccount4.Start_Fiscal_Year__c = '2011';
        
        insert new List<DiB_Campaign_Account__c>{dibCampaignAccount, dibCampaignAccount2, dibCampaignAccount3, dibCampaignAccount4};
    	
        Test.startTest();
        
        merge account1 account2;
        
        dibAggregatedAllocation = [Select DiB_Department_Id__c from DIB_Aggregated_Allocation__c where id = :dibAggregatedAllocation.Id];
        System.assert(dibAggregatedAllocation.DIB_Department_ID__c == dibDepartment1.Id);
        
        dibInvoiceLine = [Select Department_ID__c from DIB_Invoice_Line__c where id = :dibInvoiceLine.Id];
        System.assert(dibInvoiceLine.Department_ID__c == dibDepartment1.Id);
        
        DIB_Competitor_iBase__c clonedCompetitoriBase = [Select Sales_Force_Account__c, Account_Segmentation__c, Current_IB__c, Competitor_Account_ID__c from DIB_Competitor_iBase__c];
        System.assert(clonedCompetitoriBase.Id != dibCompetitoriBase.Id);
        System.assert(clonedCompetitoriBase.Current_IB__c == 10);
        System.assert(clonedCompetitoriBase.Competitor_Account_ID__c == competitor.Id);        
        System.assert(clonedCompetitoriBase.Account_Segmentation__c == dibDepartment1.Id);
        System.assert(clonedCompetitoriBase.Sales_Force_Account__c == account1.Id);
        
        dibCampaignAccount = [Select Department_ID__c, Account__c from DIB_Campaign_Account__c where id = :dibCampaignAccount.Id];
        System.assert(dibCampaignAccount.Department_ID__c == null);
        System.assert(dibCampaignAccount.Account__c == account1.Id);
        
        dibCampaignAccount2 = [Select Department_ID__c, Account__c from DIB_Campaign_Account__c where id = :dibCampaignAccount2.Id];
        System.assert(dibCampaignAccount2.Department_ID__c == null);
        System.assert(dibCampaignAccount2.Account__c == account1.Id);
        
        dibCampaignAccount3 = [Select Department_ID__c, Account__c from DIB_Campaign_Account__c where id = :dibCampaignAccount3.Id];
        System.assert(dibCampaignAccount3.Department_ID__c == dibDepartment1.Id);
        System.assert(dibCampaignAccount3.Account__c == account1.Id);
        
        dibCampaignAccount4 = [Select Department_ID__c, Account__c, Start_Fiscal_Month__c from DIB_Campaign_Account__c where id = :dibCampaignAccount4.Id];
        System.assert(dibCampaignAccount4.Department_ID__c == dibDepartment1.Id);
        System.assert(dibCampaignAccount4.Account__c == account1.Id);
        System.assert(dibCampaignAccount4.Start_Fiscal_Month__c == '01');
        Test.stopTest();
    }
}