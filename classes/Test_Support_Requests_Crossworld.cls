@isTest
private class Test_Support_Requests_Crossworld {
			
	private static testmethod void changeRequest(){
						
		Test.startTest();
		
		ctrl_Support_Requests controller = new ctrl_Support_Requests();			
		System.assert(controller.userLoggedIn==true);
									
		controller.helper_Request_XW.requestChangeRequest();
								
		System.assert(controller.session.request.Priority__c == 'Medium');
										
		controller.session.request.Area_CR__c = 'Test';
		controller.session.request.Description_Long__c = 'blabla';
		controller.session.request.CR_Type__c = 'Problem';
		controller.session.request.Priority__c = 'High';
		controller.session.request.Business_Rationale__c = 'blabla';
				
		controller.helper_Request_XW.createChangeRequest();
		
		System.assert(controller.session.request.Name != null);		
	}
	
	private static testmethod void serviceRequest(){
						
		Test.startTest();
		
		ctrl_Support_Requests controller = new ctrl_Support_Requests();			
		System.assert(controller.userLoggedIn==true);
									
		controller.helper_Request_XW.requestServiceRequest();
		System.assert(controller.session.request.request_Type__c == 'Generic Service');
			
		controller.session.request.Service_Request_Type__c = 'Test';
		controller.session.request.Description_Long__c = 'Test Description';
						
		controller.helper_Request_XW.createServiceRequest();

		System.assert(controller.session.request.Name != null);		
	}
	
	private static testmethod void bugRequest(){
						
		Test.startTest();
		
		ctrl_Support_Requests controller = new ctrl_Support_Requests();			
		System.assert(controller.userLoggedIn==true);
									
		controller.helper_Request_XW.requestBug();
								
		System.assert(controller.session.request.Request_Type__c == 'Bug');
							
		controller.session.request.Description_Long__c = 'blabla';	
				
		controller.helper_Request_XW.createBugRequest();
		
		System.assert(controller.session.request.Name != null);		
	}
	
	private static testmethod void enableAppRequest(){
						
		Test.startTest();
		
		ctrl_Support_Requests controller = new ctrl_Support_Requests();			
		System.assert(controller.userLoggedIn==true);
									
		controller.helper_Request_XW.requestEnableApp();
								
		System.assert(controller.session.request.Request_Type__c == 'Enable Mobile App');
							
		controller.session.request.Mobile_App__c = 'APDR';	
		controller.session.request.Created_User__c = UserInfo.getUserId();
				
		controller.helper_Request_XW.createEnableAppRequest();
		
		System.assert(controller.session.request.Name != null);		
	}
			
	private static testmethod void changeRequestError(){
						
		Test.startTest();
		
		ctrl_Support_Requests controller = new ctrl_Support_Requests();			
		
		controller.helper_Request_XW.requestChangeRequest();													
				
		controller.helper_Request_XW.createChangeRequest();
		System.assert(controller.session.request.Name == null);
		
		controller.session.request.Description_Long__c = 'blabla';
		
		controller.helper_Request_XW.createChangeRequest();
		System.assert(controller.session.request.Name == null);
		
		controller.session.request.Priority__c = 'blabla';
		
		controller.helper_Request_XW.createChangeRequest();
		System.assert(controller.session.request.Name == null);
		
		controller.session.request.Business_Rationale__c = 'blabla';
				
		controller.helper_Request_XW.createChangeRequest();
		System.assert(controller.session.request.Name == null);
		
		controller.session.request.CR_Type__c = 'Error';
		controller.session.request.Application__c = null;

		Boolean isError = false;
		
		try{
			controller.helper_Request_XW.createChangeRequest();
		}catch(Exception e){
			isError = true;
		}
		
		System.assert(isError == true);
		
	}
	
	private static testmethod void serviceRequestError(){
						
		Test.startTest();
		
		ctrl_Support_Requests controller = new ctrl_Support_Requests();			
									
		controller.helper_Request_XW.requestServiceRequest();
								
		// TEST 1		
		controller.helper_Request_XW.createServiceRequest();
		System.assert(controller.session.request.Name == null);
		
		// TEST 2
		controller.session.request.Description_Long__c = 'Test Description';
		controller.helper_Request_XW.createServiceRequest();
		System.assert(controller.session.request.Name == null);

		// TEST 3
		controller.session.request.Service_Request_Type__c = 'Error';
		controller.session.request.Application__c = null;

		Boolean isError = false;
		
		try{
			controller.helper_Request_XW.createServiceRequest();
		}catch(Exception e){
			isError = true;
		}
		
		System.assert(isError == true);	
	}
	
	private static testmethod void bugRequestError(){
						
		Test.startTest();
		
		ctrl_Support_Requests controller = new ctrl_Support_Requests();			
		System.assert(controller.userLoggedIn==true);
									
		controller.helper_Request_XW.requestBug();
								
		controller.helper_Request_XW.createBugRequest();
		System.assert(controller.session.request.Name == null);
									
		controller.session.request.Description_Long__c = 'blabla';	
		controller.session.request.Application__c = null;
						
		Boolean isError = false;
		
		try{
			controller.helper_Request_XW.createBugRequest();
		}catch(Exception e){
			isError = true;
		}
		
		System.assert(isError == true);	
	}
	
	private static testmethod void enableAppError(){
						
		Test.startTest();
		
		ctrl_Support_Requests controller = new ctrl_Support_Requests();			
		System.assert(controller.userLoggedIn==true);
									
		controller.helper_Request_XW.requestEnableApp();
		
		controller.helper_Request_XW.createEnableAppRequest();		
		System.assert(controller.session.request.Name == null);	
			
		controller.session.request.Created_User__c = UserInfo.getUserId();
		controller.helper_Request_XW.createEnableAppRequest();		
		System.assert(controller.session.request.Name == null);
		
		controller.session.request.Mobile_App__c = 'APDR';
		controller.session.request.Application__c = null;
						
		Boolean isError = false;
		
		try{
			controller.helper_Request_XW.createEnableAppRequest();
		}catch(Exception e){
			isError = true;
		}
		
		System.assert(isError == true);	
	}
}