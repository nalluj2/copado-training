//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 09-01-2018
//  Description      : APEX Wrapper Class for LoginHistory
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public with sharing class wr_LoginHistory {

    public String Status;
    public String LoginType;
    public String Application;

    public wr_LoginHistory(String atStatus, String atLoginType, String atApplication){
        Status = atStatus;
        LoginType = atLoginType;
        Application = atApplication;
    }

}
//--------------------------------------------------------------------------------------------------------------------