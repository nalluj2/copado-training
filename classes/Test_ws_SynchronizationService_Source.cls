@isTest
private class Test_ws_SynchronizationService_Source {
	
	private static testMethod void testDoGet(){
		
		//Products
		Product2 testProduct = new Product2();
		testProduct.Name = 'PoleStar';
		testProduct.ProductCode = '000999';
		testProduct.isActive = true;					
		insert testProduct;
		
		//Account
		Account acc = new Account();	
		acc.AccountNumber = '0000000';
		acc.Name = 'Syn Test Account';
		acc.Phone = '+123456789';
		acc.BillingCity = 'Minneapolis';
		acc.BillingCountry = 'UNITED STATES';
		acc.BillingState = 'Minnesota';
		acc.BillingStreet = 'street';
		acc.BillingPostalCode = '123456';
		acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non-SAP Account').getRecordTypeId();	
		acc.SAP_Id__c = 'SAP001';
		insert acc;
		
		//Contact
		Contact cnt = new Contact();
		cnt.FirstName = 'Test';
		cnt.LastName = 'Contact';
		cnt.Phone = '+123456789';		
		cnt.Email = 'test.contact@gmail.com';
		cnt.MobilePhone = '+123456789';		
		cnt.MailingCity = 'Minneapolis';
		cnt.MailingCountry = 'UNITED STATES';
		cnt.MailingState = 'Minnesota';
		cnt.MailingStreet = 'street';
		cnt.MailingPostalCode = '123456';
		cnt.External_Id__c = 'Test_Contact_Id';
		cnt.Contact_Department__c = 'Cardiology';
		cnt.Contact_Primary_Specialty__c = 'Cardiovascular'; 
		cnt.Affiliation_To_Account__c = 'Employee'; 
		cnt.Primary_Job_Title_vs__c = 'Surgeon'; 
		cnt.Contact_Gender__c = 'Male'; 
		cnt.AccountId = acc.Id;
		cnt.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Generic Contact').getRecordTypeId();				
		insert cnt;
		
		//Asset
		Asset asset = new Asset();
		asset.AccountId = acc.Id;
		asset.Product2Id = testProduct.Id;
		asset.Asset_Product_Type__c = 'PoleStar N-10';
		asset.Ownership_Status__c = 'Purchased';
		asset.Name = '123456789';
		asset.Serial_Nr__c = '123456789';
		asset.Status = 'Installed';
		asset.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('PoleStar').getRecordTypeId();
		insert asset;
				
		//Complaint
		Complaint__c comp = new Complaint__c();
		comp.Account_Name__c = acc.Id;
		comp.Asset__c = asset.Id;
		comp.Status__c = 'Open';
		comp.External_Id__c = 'Test_Complaint_Id';
		insert comp;
		
		//Case
		Case newCase = new Case();
		newCase.AccountId = acc.Id;
		newCase.ContactId = cnt.Id;
		newCase.AssetId = asset.Id;
		newCase.RecordTypeId = clsUtil.getRecordTypeByDevName('Case', 'New_Case').Id;
		newCase.External_Id__c = 'Test_Case_Id';					
		insert newCase;
		
		Test.startTest();
		
		RestRequest  req = new RestRequest();
		req.params.put('recordId' , 'Test_Case_Id');
		req.params.put('recordObjectType', 'Case');		
		RestContext.request = req;
		
		ws_SynchronizationService_Source.SynchronizationServiceResponse response = ws_SynchronizationService_Source.doGet();
		
		System.assert(response.recordInformation != null);
	}
	
	private static testMethod void testDoPost(){
						
		Sync_Notification__c notification = new Sync_Notification__c();
		notification.Notification_Id__c = 'Test_Notification_Id';
		notification.Status__c = 'Processed';	
		notification.Type__c = 'Outbound';		
		notification.Record_Id__c = 'Test_Record_Id';
		notification.Record_Object_Type__c = 'Case';
		insert notification;
				
		List<Sync_Notification__c> notifications = new List<Sync_Notification__c>{notification};
		
		ws_SynchronizationService_Source.SynchronizationServiceResponse response = ws_SynchronizationService_Source.doPost(notifications);
		
		System.assert(response.notifications.size() == notifications.size());
	}
}