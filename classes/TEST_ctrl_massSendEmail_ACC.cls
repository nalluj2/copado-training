@isTest private class TEST_ctrl_massSendEmail_ACC {

	@testSetup private static void createTestData() {

		//------------------------------------------------------
		// Create Test Data
		//------------------------------------------------------
		// Custom Setting
		clsTestData_CustomSetting.createCustomSettingData();
		
		// DIB_Country__c
		clsTestData_MasterData.createCountry();
		List<DIB_Country__c> lstCountry_Update = new List<DIB_Country__c>();
		DIB_Country__c oCountry_BE = clsTestData_MasterData.mapCountry.get('BE');
			oCountry_BE.Email_Service_Repair_Contract__c = 'belgium01@medtronic.com.test;belgium02@medtronic.com.test';
		lstCountry_Update.add(oCountry_BE);
		DIB_Country__c oCountry_NL = clsTestData_MasterData.mapCountry.get('NL');
			oCountry_NL.Email_Service_Repair_Contract__c = 'netherlands01@medtronic.com.test';
		lstCountry_Update.add(oCountry_NL);
		update lstCountry_Update;

		// Product
		List<Product2> lstProduct = new List<Product2>();
		Product2 oProduct1 = new Product2();
			oProduct1.Name = 'PoleStar1';
			oProduct1.ProductCode = '000999';
			oProduct1.isActive = true;					
		lstProduct.add(oProduct1);
		Product2 oProduct2 = new Product2();
			oProduct2.Name = 'PoleStar';
			oProduct2.ProductCode = '009999';
			oProduct2.isActive = true;					
		lstProduct.add(oProduct2);
		Product2 oProduct3 = new Product2();
			oProduct3.Name = 'PoleStar3';
			oProduct3.ProductCode = '099999';
			oProduct3.isActive = true;					
		lstProduct.add(oProduct3);
		insert lstProduct;

		lstProduct = [SELECT Id, ProductCode FROM Product2];
		List<RTG_Product_Code_Child_Product_Code__c> lstProductCode = new List<RTG_Product_Code_Child_Product_Code__c>();
		RTG_Product_Code_Child_Product_Code__c oProductCode1 = new RTG_Product_Code_Child_Product_Code__c();
			oProductCode1.Name = lstProduct[0].ProductCode;
			oProductCode1.Child_Product_Code__c = lstProduct[1].ProductCode + ',' + lstProduct[2].ProductCode;
		lstProductCode.add(oProductCode1);
		RTG_Product_Code_Child_Product_Code__c oProductCode2 = new RTG_Product_Code_Child_Product_Code__c();
			oProductCode2.Name = lstProduct[1].ProductCode;
			oProductCode2.Child_Product_Code__c = lstProduct[2].ProductCode;
		lstProductCode.add(oProductCode2);
		insert lstProductCode;


		// Account
		clsTestData_Account.iRecord_Account = 1;
		clsTestData_Account.idRecordType_Account = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non-SAP Account').getRecordTypeId();
		List<Account> lstAccount = clsTestData_Account.createAccount(false);
		for (Account oAccount : lstAccount){
			oAccount.AccountNumber = '0000000';
			oAccount.Name = 'Test Account';
			oAccount.Phone = '+123456789';
			oAccount.BillingCity = 'TEST CITY';
			oAccount.BillingCountry = 'BELGIUM';
			oAccount.BillingState = 'TEST STATE';
			oAccount.BillingStreet = 'TEST STREET';
			oAccount.BillingPostalCode = '123456';
			oAccount.ST_NAV_Non_SAP_Account__c = true;
		}
		insert lstAccount;
		Account oAccount = lstAccount[0];

		// Contact
		clsTestData_Contact.iRecord_Contact = 2;
		List<Contact> lstContact = clsTestData_Contact.createContact();

		// Asset
		List<Asset> lstAsset = new List<Asset>();
		Asset oAsset1 = new Asset();
			oAsset1.AccountId = oAccount.Id;
			oAsset1.Product2Id = oProduct1.Id;
			oAsset1.Asset_Product_Type__c = 'S8 Planning Station';
			oAsset1.Ownership_Status__c = 'Purchased';
			oAsset1.Name = '123456789';
			oAsset1.Serial_Nr__c = '123456789';
			oAsset1.Status = 'Installed';
			oAsset1.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('PoleStar').getRecordTypeId();
		lstAsset.add(oAsset1);
		Asset oAsset2 = new Asset();
			oAsset2.AccountId = oAccount.Id;
			oAsset2.Product2Id = oProduct2.Id;
			oAsset2.Asset_Product_Type__c = 'PoleStar N-10';
			oAsset2.Ownership_Status__c = 'Purchased';
			oAsset2.Name = '987654321';
			oAsset2.Serial_Nr__c = '987654321';
			oAsset2.Status = 'Installed';
			oAsset2.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('PoleStar').getRecordTypeId();
		lstAsset.add(oAsset2);
		insert lstAsset;

		// Create Contract Data
		clsTestData_Contract.iRecord_Contract = 2;
		List<Contract> lstContract = clsTestData_Contract.createContract(false);
		Integer iCounter = 0;
		for (Contract oContract : lstContract){
			oContract.PO_amount__c = 10000;
			oContract.ContractTerm = 36;
			if (iCounter == 0){
				oContract.BillingCountry = 'BELGIUM';
			}else{
				oContract.BillingCountry = '';
			}
			iCounter++;
		}
		insert lstContract;
		
		// Create AssetContract Data
        AssetContract__c oAssetContract1 = new AssetContract__c();
			oAssetContract1.Asset__c = lstAsset[0].Id;
			oAssetContract1.Contract__c = lstContract[0].Id;
			oAssetContract1.Entitlements__c = '100% Coverage for Labor';
			oAssetContract1.PO_Amount2__c = 1000;
			oAssetContract1.Service_Level__c = 'Gold';
			oAssetContract1.DIEN_Code__c = 'DIEN1';
        AssetContract__c oAssetContract2 = new AssetContract__c();
			oAssetContract2.Asset__c = lstAsset[1].Id;
			oAssetContract2.Contract__c = lstContract[0].Id;
			oAssetContract2.Entitlements__c = '100% Coverage for Labor';
			oAssetContract2.PO_Amount2__c = 1000;
			oAssetContract2.Service_Level__c = 'Gold';
			oAssetContract2.DIEN_Code__c = 'DIEN1';
        AssetContract__c oAssetContract3 = new AssetContract__c();
			oAssetContract3.Asset__c = lstAsset[0].Id;
			oAssetContract3.Contract__c = lstContract[1].Id;
			oAssetContract3.Entitlements__c = '100% Coverage for Labor';
			oAssetContract3.PO_Amount2__c = 1000;
			oAssetContract3.Service_Level__c = 'Gold';
			oAssetContract3.DIEN_Code__c = 'DIEN1';
        AssetContract__c oAssetContract4 = new AssetContract__c();
			oAssetContract4.Asset__c = lstAsset[1].Id;
			oAssetContract4.Contract__c = lstContract[1].Id;
			oAssetContract4.Entitlements__c = '100% Coverage for Labor';
			oAssetContract4.PO_Amount2__c = 1000;
			oAssetContract4.Service_Level__c = 'Gold';
			oAssetContract4.DIEN_Code__c = 'DIEN2';
		insert new List<AssetContract__c>{oAssetContract1, oAssetContract2, oAssetContract3, oAssetContract4};
		//------------------------------------------------------

	}


	@isTest private static void test_ctrl_massSendEmail_ACC(){

		//------------------------------------------------------------------------
		// Load Test Data
		//------------------------------------------------------------------------
		// Get Contract Data
		List<Contract> lstContract = [SELECT Id FROM Contract];

		// Get Contact Data
		List<Contact> lstContact = [SELECT Id, Name FROM Contact];
		//------------------------------------------------------------------------
		

		//------------------------------------------------------------------------
		// Test Logic
		//------------------------------------------------------------------------
		Test.startTest();

		// Get Org Wide Email Addresses
        List<OrgWideEmailAddress> lstOrgWideEmailAddress = [SELECT Id, DisplayName, Address FROM OrgWideEmailAddress ORDER BY DisplayName];
		List<Apexpages.Message> lstAPEXPageMessage = new List<Apexpages.Message>();
		
		// Set Visual Force Page for Contract 1
		PageReference oPageRef1 = Page.massSendEmail_ACC;
		Test.setCurrentPage(oPageRef1);
	        ApexPages.currentPage().getParameters().put('id', lstContract[0].Id);
	        ApexPages.currentPage().getParameters().put('initialto', lstContact[0].Id);
	        ApexPages.currentPage().getParameters().put('initialcc', lstContact[0].Id);
	        ApexPages.currentPage().getParameters().put('initialbcc', UserInfo.getUserId());
	        ApexPages.currentPage().getParameters().put('orgwideemailid', lstOrgWideEmailAddress[0].Id);

		// Initiate the Controller for Contract 1
		ctrl_massSendEmail_ACC oCTRL1 = new ctrl_massSendEmail_ACC();
			oCTRL1.processACC();
				

		// Set Visual Force Page for Contract 2
		PageReference oPageRef2 = Page.massSendEmail_ACC;
		Test.setCurrentPage(oPageRef2);
	        ApexPages.currentPage().getParameters().put('id', lstContract[0].Id);
	        ApexPages.currentPage().getParameters().put('initialto', lstContact[0].Id);
	        ApexPages.currentPage().getParameters().put('initialcc', lstContact[0].Id);
	        ApexPages.currentPage().getParameters().put('initialbcc', UserInfo.getUserId());
	        ApexPages.currentPage().getParameters().put('orgwideemailid', lstOrgWideEmailAddress[0].Id);

		// Initiate the Controller for Contract 1
		ctrl_massSendEmail_ACC oCTRL2 = new ctrl_massSendEmail_ACC();
			oCTRL2.processACC();

			oCTRL2.bBulkSelected_DEL = true;
			oCTRL2.bulkSelectedDelChange();
			oCTRL2.deleteData();
			oCTRL2.reloadData();
			oCTRL2.sortData();
			oCTRL2.backToRecord();

		Test.stopTest();
		//------------------------------------------------------------------------


		//------------------------------------------------------------------------
		// Validate Result
		//------------------------------------------------------------------------

		//------------------------------------------------------------------------

	}

}