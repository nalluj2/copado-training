/*
 *      Created Date : 20131127
 *      Description : This class exposes methods to log user activity 
 *
 *      Author = Fabrizio Truzzu 
 */

@RestResource(urlMapping='/UserActivityService/*')
global class ws_UserActivityService {
    @HttpPost
    global static String logActivity() {
        RestRequest req = RestContext.request;
                        
        String body = req.requestBody.toString();
        UserActivityRequest uaRequest = (UserActivityRequest)System.Json.deserialize(body, UserActivityRequest.class);
               
        UserActivityResponse resp = new UserActivityResponse();     
                                                  
        try{
        	insert(uaRequest.applicationUsageDetail);
        }
        catch(Exception e){
        	resp.message = 'An error occured while inserting the activity.';
        }                                      
                                                     
       return Json.serialize(resp);
    }
    
    global class UserActivityRequest{
        public Application_Usage_Detail__c applicationUsageDetail{get;set;}
    }
    
    global class UserActivityResponse{
        public boolean hasError{get{return !string.isEmpty(message);}}
        public string message{get;set;}
    }
}