public with sharing class bl_SupportRequest_XW {
	
	private ctrl_Support_Requests.SessionData session;
	
	public bl_SupportRequest_XW(ctrl_Support_Requests.SessionData sData){        
        this.session = sData;       
    }
    
    public PageReference requestEnableApp(){
                    
        session.resetRequest();
    	
        session.request.Status__c = 'New';
        session.request.Application__c = 'Crossworld';
        session.request.Request_Type__c = 'Enable Mobile App';
        
        session.clearAttachments();        
        session.readOnlyAttachments = new List<Attachment>();
        
        session.comment = null;
        session.comments = null;
        
        return Page.Support_XW_Enable_App;       
    }
        
    public PageReference requestChangeRequest(){
        
        session.resetRequest();
        
        session.request.status__c='New';
        session.request.application__c='Crossworld';
        session.request.request_Type__c = 'Change request';
        session.request.priority__c = 'Medium';
        
        session.clearAttachments();        
        session.readOnlyAttachments = new List<Attachment>();
                               
        session.comment = null;
        session.comments = null;
        		                                        
        return Page.Support_XW_Change_Request;         
    }
    
    public PageReference requestServiceRequest(){
        
        session.resetRequest();
        
        session.request.status__c='New';
        session.request.application__c='Crossworld';
        session.request.request_Type__c = 'Generic Service';
               
        session.clearAttachments();        
        session.readOnlyAttachments = new List<Attachment>();
                               
        session.comment = null;
        session.comments = null;
                                               
        return Page.Support_XW_Generic_Service;         
    }
    
	public PageReference requestBug(){
   	
    	session.resetRequest();
    	
        session.request.Status__c = 'New';
        session.request.Application__c = 'Crossworld';
        session.request.Request_Type__c = 'Bug';
        
        session.clearAttachments();        
        session.readOnlyAttachments = new List<Attachment>();
        
        session.comment = null;
        session.comments = null;
        
        return Page.Support_XW_Bug;      
    }
    
    public void createEnableAppRequest(){
                        
        if(session.request.Created_User__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'User is required'));
            return;
        }
               
        if(session.request.Mobile_App__c == null || session.request.Mobile_App__c == ''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Mobile App is required'));
            return;
        }                               
                
        try{    
            
            session.request.Requestor_Email__c = session.uInfo.email;
            session.request.Requestor_Name__c = session.uInfo.firstName + ' ' + session.uInfo.lastName;
            insert session.request;
            
            session.comment = new User_Request_Comment__c();        
                        
        }catch(Exception e){
            
            ApexPages.addMessages(e);
            if(Test.isRunningTest()) throw e;
            return;
        }
        
        session.request = [Select Id, Name, Status__c, Assignee__c, Request_Type__c, created_User__c, Created_User__r.Name, 
                                                Comment__c, Requestor_Email__c, Mobile_App__c from Create_User_Request__c where Id=:session.request.Id];
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request '+session.request.Name+' created successfully.'));       
    }
    
    public void createServiceRequest(){
        
        if(session.request.Description_Long__c == null || session.request.Description_Long__c==''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'A Description is required'));
            return; 
        }
        
        if(session.request.Service_Request_Type__c == null || session.request.Service_Request_Type__c==''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Service Request Type is required'));
            return; 
        }
        
        Create_User_Request__c toInsertRequest = session.request.clone(false);

        SavePoint sp = Database.setSavepoint();
        
        try{
                                
            toInsertRequest.Requestor_Name__c = session.uInfo.firstName+' '+session.uInfo.lastName;
            toInsertRequest.Requestor_Email__c = session.uInfo.email;                       
            insert toInsertRequest;
            
            session.saveAttachments(toInsertRequest.Id);
                        
            session.request.Id = toInsertRequest.Id;
            
            session.comment = new User_Request_Comment__c();
                        
        } catch (Exception e) {
            
            ApexPages.addMessages(e);
            Database.rollback(sp);
            
            if(Test.isRunningTest()) throw e;
            
            return ;        
        }finally{           
            session.clearAttachments();           
        }
        
        session.request = [Select Id, Name, Description_Long__c, Assignee__c, Status__c, Service_Request_Type__c from Create_User_Request__c where Id=:session.request.Id];
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request '+session.request.Name+' created successfully.'));
    }
    
    public void createBugRequest(){
        
        if(session.request.Description_Long__c == null || session.request.Description_Long__c==''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'A Description is required'));
            return; 
        }
                
        Create_User_Request__c toInsertRequest = session.request.clone(false);

        SavePoint sp = Database.setSavepoint();
        
        try{
                                
            toInsertRequest.Requestor_Name__c = session.uInfo.firstName+' '+session.uInfo.lastName;
            toInsertRequest.Requestor_Email__c = session.uInfo.email;                     
            insert toInsertRequest;
            
            session.saveAttachments(toInsertRequest.Id);
                        
            session.request.Id = toInsertRequest.Id;
            
            session.comment = new User_Request_Comment__c();
                        
        } catch (Exception e) {
                                    
            ApexPages.addMessages(e);
            Database.rollback(sp);
            
            if(Test.isRunningTest()) throw e;
            
            return ;
        }finally{
            session.clearAttachments();
        }
        
        session.request = [Select Id, Name, Description_Long__c, Assignee__c, Status__c from Create_User_Request__c where Id=:session.request.Id];
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request '+session.request.Name+' created successfully.'));
    }
    
    public void createChangeRequest(){
        
        if(session.request.Description_Long__c == null || session.request.Description_Long__c==''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'A Description is required'));
            return; 
        }else if(session.request.Priority__c == null || session.request.Priority__c==''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Priority is required'));
            return; 
        }else if(session.request.Business_Rationale__c == null || session.request.Business_Rationale__c==''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Business Rationale is required'));
            return; 
        }else if(session.request.CR_Type__c == null || session.request.CR_Type__c==''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Change Request Type is required'));
            return; 
        }       
                
        Create_User_Request__c toInsertRequest = session.request.clone(false);

        SavePoint sp = Database.setSavepoint();
        
        try{
                                
            toInsertRequest.Requestor_Name__c = session.uInfo.firstName+' '+session.uInfo.lastName;
            toInsertRequest.Requestor_Email__c = session.uInfo.email;
            insert toInsertRequest;
            
            session.saveAttachments(toInsertRequest.Id);
                        
            session.request.Id = toInsertRequest.Id;
            
            session.comment = new User_Request_Comment__c();
                        
        } catch (Exception e) {
                                    
            ApexPages.addMessages(e);
            Database.rollback(sp);
            
            if(Test.isRunningTest()) throw e;
            
            return ;
        }finally{
            session.clearAttachments(); 
        }
        
        session.request = [Select Id, Name, Assignee__c, CR_Type__c, Description_Long__c, Status__c, Priority__c, Business_Rationale__c from Create_User_Request__c where Id=:session.request.Id];
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request '+session.request.Name+' created successfully.'));
    }
}