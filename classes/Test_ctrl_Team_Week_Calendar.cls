@isTest
private class Test_ctrl_Team_Week_Calendar {
    
    private static testmethod void testTeamDayCalendar(){
    	
    	createTestData();
    	
    	Test.startTest();
    	
    	Implant_Scheduling_Team__c team = [Select Id from Implant_Scheduling_Team__c];
    	
    	ctrl_Team_Week_Calendar.CalendarView calendar = ctrl_Team_Week_Calendar.getWeek(team.Id, Date.today(), 1000);    	    	
    	System.assert(calendar.members.size() > 0);
    }
    
    private static void createTestData(){
		
		Account acc = new Account();
		acc.Name = 'Test Account';
		acc.Account_Country_vs__c = 'NETHERLANDS';		
		insert acc;
		
		Implant_Scheduling_Team__c team = [Select Id from Implant_Scheduling_Team__c];
		
		Id caseActivitySchedulingRT = [Select Id from RecordType where SObjectType = 'Case' AND DeveloperName = 'Implant_Scheduling'].Id;
		
		List<Case> caseList = new List<Case>();
		
		for(Integer i = 0; i < 7; i++){
		
			Case openCase = new Case();		
			openCase.AccountId = acc.Id;			
			openCase.Start_of_Procedure__c = DateTime.newInstance(Date.today().addDays(i), Time.newInstance(10, 0, 0, 0)); 
			openCase.Procedure_Duration_Implants__c = '0' + (i + 1) + ':00';
			openCase.RecordTypeId = caseActivitySchedulingRT;
			openCase.Activity_Scheduling_Team__c = team.Id;
			openCase.Type = Math.mod(i, 2) == 0 ? 'Implant Support Request' : 'Service Request';
			openCase.Status = 'Open';	
			
			caseList.add(openCase);
		}
		
		List<Implant_Scheduling_Team_Member__c> members = [Select Member__c, Team__c from Implant_Scheduling_Team_Member__c];
				
		for(Implant_Scheduling_Team_Member__c member : members){
						
			for(Integer i = 0; i < 7; i++){						
				
				Case assignedCase = new Case();		
				assignedCase.AccountId = acc.Id;			
				assignedCase.Start_of_Procedure__c = DateTime.newInstance(Date.today().addDays(i), Time.newInstance(20, 0, 0, 0)); 
				assignedCase.Procedure_Duration_Implants__c = '0' + (i + 1) + ':00';
				assignedCase.RecordTypeId = caseActivitySchedulingRT;
				assignedCase.Activity_Scheduling_Team__c = team.Id;
				assignedCase.Assigned_To__c = member.Member__c;
				assignedCase.Type =  Math.mod(i, 2) == 0 ? 'Implant Support Request' : 'Service Request';
				assignedCase.Status = 'Assigned';	
				
				caseList.add(assignedCase);
			}
		}
				
		bl_Case_Trigger.runningSchedulingApp = true;
		
		insert caseList;
		
		Id eventActivitySchedulingRT = [Select Id from RecordType where SObjectType = 'Event' AND DeveloperName = 'Activity_Scheduling'].Id;
		List<Event> personalEvents = new List<Event>();
		
		for(Implant_Scheduling_Team_Member__c member : members){
								
			Event personalEvent = new Event();
			personalEvent.Subject = 'Test Subject';	
			personalEvent.StartDateTime = DateTime.now().addDays(-1); 
			personalEvent.EndDateTime = DateTime.now().addDays(1);
			personalEvent.RecordTypeId = eventActivitySchedulingRT;
			personalEvent.OwnerId = member.Member__c;
			
			personalEvents.add(personalEvent);			
		}
				
		insert personalEvents;
	}
	
	@TestSetup
	private static void setData() {
		
		Mobile_App__c app = new Mobile_App__c();
		app.Name = 'Activity Scheduling Tool';
		app.Active__c = true;
		app.Unique_Key__c = 'ACT_SCHDL';
		insert app;
		
		List<Implant_Scheduling_Team__c> teams = new List<Implant_Scheduling_Team__c>();
		
		Id activitySchedulingTeamRT = [Select Id from RecordType where SObjectType = 'Implant_Scheduling_Team__c' AND DeveloperName = 'Activity_Scheduling_Team'].Id;
		
		Implant_Scheduling_Team__c team = new Implant_Scheduling_Team__c();			
		team.Name = 'Test Team 1';
		team.Team_Color__c = 'rgb(99, 99, 99)';
		team.Work_Day_End__c = '18';
		team.Work_Day_Start__c = '8';
		team.Working_Days__c = '1:5';			
		team.RecordTypeId = activitySchedulingTeamRT;
		insert team;
		
		List<User> otherUsers = [Select Id from User where Profile.Name LIKE 'EUR Field Force CVG%' AND isActive = true LIMIT 10];
		
		List<Implant_Scheduling_Team_Member__c> members = new List<Implant_Scheduling_Team_Member__c>();
		
		Integer i = 0;
		
		for(User otherUser : otherUsers){
			
			Implant_Scheduling_Team_Member__c member = new Implant_Scheduling_Team_Member__c();
			member.Team__c = team.Id;
			member.Member__c = otherUser.Id;				
			
			members.add(member);
			i++;
		}
		
		insert members;	
	} 
}