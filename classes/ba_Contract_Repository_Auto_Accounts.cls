global class ba_Contract_Repository_Auto_Accounts implements Schedulable, Database.Batchable<SObject> {
  
    // Schedulable
    global void execute(SchedulableContext ctx){
        Database.executeBatch(new ba_Contract_Repository_Auto_Accounts (true));
    }
    
    Boolean skipExpiredContractsinQuery;
    global ba_Contract_Repository_Auto_Accounts (boolean skipExpiredContracts){
        skipExpiredContractsinQuery = skipExpiredContracts;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        Set<Id> historyTrackingSet = New Set<Id>();
        for(History_Tracking__c historyTracking: [SELECT Contract_Repository__c FROM History_Tracking__c WHERE FieldName__c = 'Account__c' AND CreatedDate = YESTERDAY])
        {
            historyTrackingSet.add(historyTracking.Contract_Repository__c);
        }
        
        String query = 'SELECT Id,Account__c,Account__r.RecordType.DeveloperName FROM Contract_Repository__c WHERE';
        
        if(historyTrackingSet.isEmpty()){
            query += ' Account__r.RecordType.DeveloperName = \'Buying_Entity\'';
            
        }
        else{
            query += ' (Account__r.RecordType.DeveloperName = \'Buying_Entity\' OR Id IN:historyTrackingSet)';
        }
        
        if(skipExpiredContractsinQuery){
            query += ' AND Contract_Status__c != \'Expired\'';
        }
        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Contract_Repository__c> contracts) {

        Set<Id> primaryBEAccIdSet = New Set<Id> ();
        Set<Id> secondaryBEAccIdSet = New Set<Id> ();
        Map<Id, Set<Id>> primaryBERelatedAccsMap = New Map<Id, Set<Id>>();
        Map<Id, Set<Id>> secondaryBERelatedAccsMap = New Map<Id, Set<Id>>();
        Map<Id,Set<String>> existingConRepoAccsMap = New Map<Id,Set<String>> ();
        Set<String> allConRepoAccsSet = New Set<String> ();
        Map<Id,Set<Id>> totalConRepoAccsMap = New Map<Id,Set<Id>> ();
        List<Contract_Repository_Account__c> upsertConRepoAccLst = New List<Contract_Repository_Account__c> ();
        
        for(Contract_Repository__c conRepo: contracts){
            if(conRepo.Account__r.RecordType.DeveloperName == 'Buying_Entity')
            primaryBEAccIdSet.add(conRepo.Account__c); 
        }
        
        for(Contract_Repository_Account__c conRepoAccount:[SELECT Id,Unique_Key__c,Contract_Repository__c FROM Contract_Repository_Account__c WHERE Contract_Repository__c IN:contracts]){
            if(existingConRepoAccsMap.containsKey(conRepoAccount.Contract_Repository__c)){
                existingConRepoAccsMap.get(conRepoAccount.Contract_Repository__c).add(conRepoAccount.Unique_Key__c);    
            }
            else
            existingConRepoAccsMap.put(conRepoAccount.Contract_Repository__c, New Set<String>{conRepoAccount.Unique_Key__c});        
        }
        if(!primaryBEAccIdSet.isEmpty())
        for(Affiliation__c relationships:[SELECT Name, Affiliation_Type__c, Affiliation_Active__c, 
                                    RecordType.Name,  Affiliation_To_Account__c,Affiliation_From_Account__c,Affiliation_From_Account__r.RecordType.DeveloperName FROM Affiliation__c WHERE Affiliation_Active__c = true
                                        AND RecordType.Name = 'A2A' AND Affiliation_To_Account__c =:primaryBEAccIdSet]){

            if(relationships.Affiliation_From_Account__r.RecordType.DeveloperName == 'Buying_Entity')
            secondaryBEAccIdSet.add(relationships.Affiliation_From_Account__c);

            if(primaryBERelatedAccsMap.containsKey(relationships.Affiliation_To_Account__c)){
                primaryBERelatedAccsMap.get(relationships.Affiliation_To_Account__c).add(relationships.Affiliation_From_Account__c);    
            }
            else
            primaryBERelatedAccsMap.put(relationships.Affiliation_To_Account__c, New Set<Id>{relationships.Affiliation_From_Account__c});
        }
        
        if(!secondaryBEAccIdSet.isEmpty())
        for(Affiliation__c relationships:[SELECT Name, Affiliation_Type__c, Affiliation_Active__c, 
                                    RecordTypeId,  Affiliation_To_Account__c,Affiliation_From_Account__c,Affiliation_From_Account__r.RecordTypeId FROM Affiliation__c WHERE Affiliation_Active__c = true
                                        AND RecordType.Name = 'A2A' AND Affiliation_To_Account__c =:secondaryBEAccIdSet]){
            if(secondaryBERelatedAccsMap.containsKey(relationships.Affiliation_To_Account__c)){
                secondaryBERelatedAccsMap.get(relationships.Affiliation_To_Account__c).add(relationships.Affiliation_From_Account__c);    
            }
            else
            secondaryBERelatedAccsMap.put(relationships.Affiliation_To_Account__c, New Set<Id>{relationships.Affiliation_From_Account__c});                                    
        }
        
        for(Contract_Repository__c contract: contracts){
            totalConRepoAccsMap.put(contract.Id,New Set<Id>{contract.Account__c});
            if(primaryBERelatedAccsMap.containsKey(contract.Account__c)){
                totalConRepoAccsMap.get(contract.Id).addAll(primaryBERelatedAccsMap.get(contract.Account__c));
                for(Id accId: primaryBERelatedAccsMap.get(contract.Account__c)){
                    if(secondaryBERelatedAccsMap.containsKey(accId))
                    {
                        totalConRepoAccsMap.get(contract.Id).addAll(secondaryBERelatedAccsMap.get(accId));
                    }
                }
            }    
        }
        
        for(Contract_Repository__c contract: contracts){
            for(Id accId: totalConRepoAccsMap.get(contract.Id)){
                allConRepoAccsSet.add(contract.Id + ':' + accId);
                if(existingConRepoAccsMap.containsKey(contract.Id) && !existingConRepoAccsMap.get(contract.Id).contains(contract.Id + ':' + accId)){
                    Contract_Repository_Account__c contractAcc = new Contract_Repository_Account__c();
                    contractAcc.Contract_Repository__c = contract.Id;
                    contractAcc.Account__c = accId;
                    contractAcc.Unique_Key__c = contract.Id + ':' + accId;
                    contractAcc.Created_By_Auto_Assignment_Job__c = true;
                    upsertConRepoAccLst.add(contractAcc);
                }
            }
        }

        if(!upsertConRepoAccLst.isEmpty())
        Database.upsert (upsertConRepoAccLst,false);

        List<Contract_Repository_Account__c> deleteConRepoAccounts=  [SELECT Id FROM Contract_Repository_Account__c 
                    WHERE Created_By_Auto_Assignment_Job__c = true AND Contract_Repository__c IN:totalConRepoAccsMap.keySet() AND Unique_Key__c NOT IN:allConRepoAccsSet];
        if(!deleteConRepoAccounts.isEmpty())
        Database.delete (deleteConRepoAccounts,false);           
    }

    global void finish(Database.BatchableContext BC) {}
}