/**
 *  @package  classes
 *  @name     ba_AccountTeamMember
 *  @description
 *        
 */

global class ba_AccountTeamMember implements Schedulable, Database.Batchable<sObject>, Database.Stateful {
  
  	private Integer totalInserted = 0;
  	private Integer totalUpdated = 0;
  	private Integer totalDeleted = 0;
  	private Integer totalUntouched = 0;
  
  	@TestVisible
  	private List<Account> accounts;
    
  	// Master data  
  	private Map<String, Therapy_Group__c> companyTherapyGroupMap;
        
  	/**
   	*  Schedulable   
   	*/
  	global void execute(SchedulableContext ctx){
        
		Database.executeBatch(new ba_AccountTeamMember(), 200);
  	}

  	global Database.QueryLocator start(Database.BatchableContext ctx){
    
	    // Set Master Data    
	    companyTherapyGroupMap = new Map<String, Therapy_Group__c>();
	    for(Therapy_Group__c tg : [SELECT Id, Name, Company__c, Sub_Business_Unit__r.Business_Unit__c FROM Therapy_Group__c]) companyTherapyGroupMap.put(tg.Name + ':' + String.valueOf(tg.Company__c).subString(0,15), tg);      
	    
	    // Build main query
	    Set<String> setExcludedCountries = SharedMethods.retExcludefromAccountFlagging();
	
	    String query = 'SELECT Id ';        
	    query += 'FROM Account ';
	    query += 'WHERE RecordType.DeveloperName NOT IN (\'Competitor\', \'DIB_Competitor\') ';
	    query += 'AND Account_Country__c NOT IN :setExcludedCountries ';
	    
	    if(accounts != null) query += 'AND Id IN :accounts ';
	        
	    return Database.getQueryLocator(query);
  	}

  	global void execute(Database.BatchableContext ctx, List<Account> records){
    
	    List<Account_Team_Member__c> toInsert = new List<Account_Team_Member__c>();
	    List<Account_Team_Member__c> toUpdate = new List<Account_Team_Member__c>();
	    List<Account_Team_Member__c> toDelete = new List<Account_Team_Member__c>();
	    
	    // Accounts with Share
	    List<Account> accounts = [Select Id, (Select Territory2.Id, Territory2.Company__c, Territory2.Therapy_Groups_Text__c, Territory2.Territory2Type.DeveloperName from ObjectTerritory2Associations) from Account where Id IN :records];
	    
	    Set<Id> territoryIds = new Set<Id>();
	    
	    for(Account acc : accounts){
	    	
	    	for(ObjectTerritory2Association accShare : acc.ObjectTerritory2Associations){
	    		
	    		territoryIds.add(accShare.Territory2.Id);
	    	}	    	
	    }
	    
	    Map<Id, List<User>> territoryUsersMap = new Map<Id, List<User>>();
	    
	    for(Territory2 terr : [Select Id, (Select User.Id, User.Job_Title_vs__c, User.IsActive from UserTerritory2Associations where IsActive = true) from Territory2 where Id IN :territoryIds ]){
	    		    	
	    	List<User> terrUsers = new List<User>();
	    	territoryUsersMap.put(terr.Id, terrUsers);
	    	
	    	for(UserTerritory2Association terrUser : terr.UserTerritory2Associations){
	    		
	    		terrUsers.adD(terrUser.User);
	    	}
	    }
	   	    
	    Map<String, Account_Team_Member__c> existingATM = new Map<String, Account_Team_Member__c>();
	    
	    for(Account_Team_Member__c atm : [SELECT Id, Account__c, Primary__c, Therapy_Group__c, User__c, Role__c FROM Account_Team_Member__c Where Account__c IN :accounts AND Primary__c = true]){
	      
	    	String key = atm.Account__c + ':' + atm.User__c + ':' + atm.Therapy_Group__c;        
	      
	     	Account_Team_Member__c duplicateATM = existingATM.put(key, atm);
	      
	      	// If this key has been found already, then we have multiple records for the same combination of Account / User / Therapy Group. Let's delete the duplicates
	      	if(duplicateATM != null){
	        
	        	toDelete.add(duplicateATM);
	        	totalDeleted++;        
	      	}  
	    }
	    
	    Set<String> alreadyProcessed = new Set<String>();
	    
	    // Get List off Accounts
	    for(Account acc : accounts){
	            
	      	for(ObjectTerritory2Association accShare : acc.ObjectTerritory2Associations){
	        
	        	// It can be that the Territory is not of Territory Type = 'Territory'
				if(accShare.Territory2.Territory2Type.DeveloperName == 'Territory'){
	        	
		        	Territory2 accTerritory = accShare.Territory2;
		                              
		          	List<User> territoryUsers = territoryUsersMap.get(accTerritory.Id);
		          	List<String> therapyGroups = accTerritory.Therapy_Groups_Text__c.split(';');
		          
		          	// If Territory has Active Users 
		          	if(territoryUsers != null){
		                      
		            	for(User user : territoryUsers){
		              
		              		for(String therapyGroupNameRaw : therapyGroups){
		                
		                		String therapyGroupName = therapyGroupNameRaw.trim();
		                                                            
		               			Therapy_Group__c therapyGroup = companyTherapyGroupMap.get(therapyGroupName + ':' + accTerritory.Company__c.subString(0,15));  
		                
		                		// If the Therapy Group is not valid we ignore it (very rare case)
		                		if(therapyGroup == null) continue;
		                              
		                		String key = acc.Id + ':' + user.Id + ':' + therapyGroup.Id;
		                
		                		// If this key has been already found, do not create a duplicate
		                		if(alreadyProcessed.add(key) == false) continue;
		                
		                		if(existingATM.containsKey(key)){
			                  
		    		            	Account_Team_Member__c atm = existingATM.get(key);
		                  
		                  			// If the data for this Account Team Member should be different we do an update
		                  			if(atm.Role__c != user.Job_Title_vs__c){
		                                        
		                    			atm.Role__c = user.Job_Title_vs__c;
		                    
		                    			toUpdate.add(atm);
		                    			totalUpdated++;
		                    
		                  			}else{
		                    
		                    			totalUntouched++;
		                  			}
		                  
		                  			// If the Account Team Member combination already exists, we remove it from the map. In this way we know which one should stay and which one should be deleted
		                  			existingATM.remove(key);
		                    
		                		}else{
		                  
			                  		// If the Account Team Member combination doesn't exist yet then we create a new one
			                  		Account_Team_Member__c atm = new Account_Team_Member__c();
			                  		atm.Account__c = acc.Id;                  
				                  	atm.Primary__c = true;                  
					                atm.Therapy_Group__c = therapyGroup.Id;
					                atm.Business_Unit__c = therapyGroup.Sub_Business_Unit__r.Business_Unit__c;
					                atm.User__c = user.Id;
					                atm.Role__c = user.Job_Title_vs__c;
					                  
					                toInsert.add(atm);
					                totalInserted ++;                  
			                	}              
		              		}
		            	}            
					}          
	        	}
			}            
	    }
    
    	// All the existing Account Team Members that has not being removed from the Map are set to Delete
    	if(existingATM.isEmpty() == false){
    		toDelete.addAll(existingATM.values());
    		totalDeleted += existingATM.size();      
    	}

    	if(toInsert.size() > 0) insert toInsert;
    	if(toUpdate.size() > 0) update toUpdate;    
    	if(toDelete.size() > 0) delete toDelete;            
  	}

  	global void finish(Database.BatchableContext BC){
    
    String emailBody = '';
    
    emailBody += 'Total Inserted: ' + totalInserted + ' <br/>';
    emailBody += 'Total Deleted: ' + totalDeleted + ' <br/>';
    emailBody += 'Total Updated: ' + totalUpdated + ' <br/>';
    emailBody += 'Total Untouched: ' + totalUntouched + ' <br/><br/>';    
    
    Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
    email.setTargetObjectId(USerInfo.getUserId());
    email.setSaveAsActivity(false);
    email.setSubject('Account Team Member batch results ' + Date.today().format());
    email.setHTMLBody(emailBody);
        
    Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{email});    
  }
}