/*
 * Work item		: WI-171
 * Description		: Controller for LeadConversionRedirect Visualforce Page
 *					  Updates field on Lead so that time-based workflow items are removed and Lead conversion will continu 
 * Author        	: Patrick Brinksma
 * Created Date    	: 15-07-2013
 */
public with sharing class ctrlExt_LeadConversionRedirect {
	// empty constructor
	public ctrlExt_LeadConversionRedirect(ApexPages.StandardController ctrl){}
	
	/*
	 * Description		: Update Force Convert field, so time-based workflow rules are invalid and removed
	 *					  Then redirect to standard lead conversion page
	 * Author			: Patrick Brinksma
	 * Created Date		: 15-07-2013
	 */			
	public PageReference redirectToLeadConversion(){
		Id leadId = ApexPages.currentPage().getParameters().get('Id');
		Lead thisLead = [select Id, Force_Convert__c from Lead where Id = :leadId];
		thisLead.Force_Convert__c = true;
		update thisLead;
		//PageReference retPage = new PageReference('/lead/leadconvert.jsp?retURL=%2F' + leadId + '&id=' + leadId);
		PageReference retPage = new PageReference('/apex/CustomLeadConvert?id='+leadId);
		retPage.setRedirect(true);
		return retPage;
	}

}