public class bl_Task {

	public static Set<String> processedMobileIds = new Set<String>();

	public static Task saveTask(Task task, List<Id> contactIds){
		
		if (null!=task){
						
			upsert task Mobile_ID__c;
			
			if((contactIds == null || contactIds.isEmpty()) && task.WhoId != null) contactIds = new List<Id>{task.WhoId};
			
			List<TaskRelation> toDelete= [Select Id, RelationId from TaskRelation where TaskId = :task.Id AND IsWhat = false];						
			if(toDelete.size() > 0) delete toDelete;				
									
			List<TaskRelation> toInsert = new List<TaskRelation>();
			
			if(contactIds != null){
				
				for(Id contactId : contactIds){
					
					TaskRelation taskContact = new TaskRelation();
					taskContact.TaskId = task.Id;
					taskContact.RelationId = contactId;
					
					toInsert.add(taskContact);
				}
			}
			
			if(toInsert.size() > 0){
				
				insert toInsert;
				task.WhoId = contactIds[0];
				
			}else task.WhoId = null;
		}
		
		return task;		
	}
	
	public static void sendCVGTaskEmail(List<Task> triggerNew, Map<Id, Task> oldMap){
		
		List<Id> taskIds = new List<Id>();
		
		for(Task tsk : TriggerNew){
		    	
	    	if(tsk.Created_in_Mobile_CVG__c  == true){ 		    	
	    		
	    		if(oldMap == null) taskIds.add(tsk.Id);
	    		else if(tsk.OwnerId != oldMap.get(tsk.Id).OwnerId) taskIds.add(tsk.Id);	    		
	    	}
	    }		
	    
	    if(taskIds.size() > 0) bl_Task.sendCVGEmail(taskIds);
	}	
	
	//Needs to be executed in future because otherwise the multipe related contacts table is not reflected
	@future
	public static void sendCVGEmail(List<Id> taskIds){
		
		List<Task> tsks = [Select Id, OwnerId from Task where Id IN :taskIds];
		
		List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
						
		for(Task tsk : tsks){						
			
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	        
	        String target_object_id = tsk.OwnerId;
	        String what_id = tsk.Id;
	        String template_id = [Select Id from EmailTemplate where Name = 'Task Notification template CVG VF'].Id;
	        
	        mail.setTargetObjectId(target_object_id);
	        mail.setWhatId(what_id);
	        mail.setTemplateId(template_id);
	        mail.setSaveAsActivity(false);
	        
	        emails.add(mail);        
		}
		
        Messaging.sendEmail(emails);
	}
}