//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 04/12/2014
//  Description      : APEX Test Class for the APEX Trigger tr_Account_Region (CR-3753)
//  Change Log       : 
//		- 20/03/2019 - Bart Caelen - Replaced MEACAT by CEMA
//--------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_tr_Account_Region {
	
	@isTest static void test_tr_Account_Region() {

		String tRegion1 = 'Europe';
		String tSubRegion1 = 'NWE';

		String tRegion2 = 'CEMA';
		String tSubRegion2 = 'Turkey';

		//------------------------------------------------
		// CREATE TEST DATA
		//------------------------------------------------
		clsTestData.createCountryData();
		DIB_Country__c oCountry = clsTestData.mapCode_Country.get('BE');
			oCountry.Region_vs__c = tRegion1;
			oCountry.Sub_Region__c = tSubRegion1;
		update oCountry;

		clsTestData.tCountry_Account = 'BELGIUM';
		clsTestData.createAccountData();
		//------------------------------------------------


		//------------------------------------------------
		// PERFORM TESTING
		//------------------------------------------------
		Test.startTest();

		// Validate the copy of data after the Account is created
		Account oAccount = [SELECT Id, Account_Country_vs__c, Country_Region__c, Country_Sub_Region__c FROM Account WHERE Id = :clsTestData.oMain_Account.Id];

		System.assertEquals(oAccount.Country_Region__c, tRegion1);
		System.assertEquals(oAccount.Country_Sub_Region__c, tSubRegion1);


		// Validate the copy of data after the Account is updated
			oCountry.Region_vs__c = tRegion2;
			oCountry.Sub_Region__c = tSubRegion2;
		update oCountry;

		update oAccount;

		oAccount = [SELECT Id, Account_Country_vs__c, Country_Region__c, Country_Sub_Region__c FROM Account WHERE Id = :clsTestData.oMain_Account.Id];

		System.assertEquals(oAccount.Country_Region__c, tRegion2);
		System.assertEquals(oAccount.Country_Sub_Region__c, tSubRegion2);

		Test.stopTest();
		//------------------------------------------------

	}
	
}