/*
 *      Created Date    : 20140811
 *      Author          : Bart Caelen
 *      Description     : TEST Class for the APEX Trigger tr_Task_UpdateParent
 */
@isTest private class Test_tr_Task_UpdateParent {


    private static List<Account_Plan_2__c> lstIPlan2 = new List<Account_Plan_2__c>();
    private static List<Task> lstTask_Insert   = new List<Task>();

    private static void createTestData(){

        //---------------------------------------------------
        // CREATE TEST DATA
        //---------------------------------------------------
        clsTestData.createSubBusinessUnitData();
        clsTestData.createAccountData();
        
        RecordType eurDibSbu = [Select Id from RecordType where sObjectType = 'Account_Plan_2__c' AND DeveloperName = 'EUR_DIB_SBU'];
        
        List<Account_Plan_2__c> lstAccountPlan = new List<Account_Plan_2__c>();
        Account_Plan_2__c oAccountPlan_1 = new Account_Plan_2__c();
            oAccountPlan_1.Account__c = clsTestData.oMain_Account.Id;
            oAccountPlan_1.Account_Plan_Level__c = 'Sub Business Unit';
            oAccountPlan_1.Sub_Business_Unit__c = [Select Id from Sub_Business_Units__c where Name = 'Brain Modulation' AND Business_Unit__r.Company__r.Name = 'Europe'].Id;
            oAccountPlan_1.Start_Date__c = Date.today();            
            oAccountPlan_1.RecordTypeId = eurDibSbu.Id;
        lstAccountPlan.add(oAccountPlan_1);

        Account_Plan_2__c oAccountPlan_2 = new Account_Plan_2__c();
            oAccountPlan_2.Account__c = clsTestData.oMain_Account.Id;
            oAccountPlan_2.Account_Plan_Level__c = 'Sub Business Unit';
            oAccountPlan_2.Sub_Business_Unit__c = [Select Id from Sub_Business_Units__c where Name = 'Coro + PV' AND Business_Unit__r.Company__r.Name = 'Europe'].Id;
            oAccountPlan_2.Start_Date__c = Date.today();            
            oAccountPlan_2.RecordTypeId = eurDibSbu.Id;
        lstAccountPlan.add(oAccountPlan_2);
        insert lstAccountPlan;


        lstIPlan2 = [SELECT Id FROM Account_Plan_2__c];
        
        clsTestData.iRecord_Task    = 5;

        clsTestData.oMain_Task      = null;
        clsTestData.idTask_What     = lstIPlan2[0].Id;
        clsTestData.tTask_Status    = 'New';
        clsTestData.createTaskData(false);
        lstTask_Insert.addAll(clsTestData.lstTask);

        clsTestData.oMain_Task      = null;
        clsTestData.idTask_What     = lstIPlan2[0].Id;
        clsTestData.tTask_Status    = 'Completed';
        clsTestData.createTaskData(false);
        lstTask_Insert.addAll(clsTestData.lstTask);
        //---------------------------------------------------
    }

    @isTest static void test_tr_Task_UpdateParent_Insert() {


        //---------------------------------------------------
        // CREATE TEST DATA
        //---------------------------------------------------
        createTestData();
        //---------------------------------------------------


        //---------------------------------------------------
        // TEST 1 
        //---------------------------------------------------
        List<Account_Plan_2__c> lstAccountPlan = 
            [
                SELECT 
                    Id, Number_of_Activities__c
                FROM
                    Account_Plan_2__c
                WHERE
                    Id = :lstIPlan2
            ];

        for (Account_Plan_2__c oAccountPlan : lstAccountPlan){
            System.assertEquals(oAccountPlan.Number_of_Activities__c, 0);
        }
        //---------------------------------------------------


        Test.startTest();

        insert lstTask_Insert;

        Test.stopTest();


        //---------------------------------------------------
        // TEST 2
        //---------------------------------------------------
        List<Task> lstTask0 = [SELECT Id, WhatId FROM Task WHERE WhatId = :lstIPlan2[0].Id];
        List<Task> lstTask1 = [SELECT Id, WhatId FROM Task WHERE WhatId = :lstIPlan2[1].Id];
        System.assertEquals(lstTask0.size(), 10);
        System.assertEquals(lstTask1.size(), 0);

        lstAccountPlan = 
            [
                SELECT 
                    Id, Number_of_Activities__c
                FROM
                    Account_Plan_2__c
                WHERE
                    Id = :lstIPlan2
            ];

        for (Account_Plan_2__c oAccountPlan : lstAccountPlan){
            if (oAccountPlan.Id == lstIPlan2[0].Id){
                System.assertEquals(oAccountPlan.Number_of_Activities__c, 10);
            }else if (oAccountPlan.Id == lstIPlan2[1].Id){
                System.assertEquals(oAccountPlan.Number_of_Activities__c, 0);
            }
        }
        //---------------------------------------------------
    }


    @isTest static void test_tr_Task_UpdateParent_Update() {

        List<Task> lstTask_Update   = new List<Task>();

        //---------------------------------------------------
        // CREATE TEST DATA
        //---------------------------------------------------
        createTestData();
        insert lstTask_Insert;
        //---------------------------------------------------

        List<Task> lstTask = [SELECT Id, WhatId FROM Task WHERE WhatId = :lstIPlan2];
        for (Task oTask : lstTask){
            if (oTask.WhatId == lstIPlan2[0].Id){
                oTask.WhatId = lstIPlan2[1].Id;
                lstTask_Update.add(oTask);
            }
        }

        Test.startTest();

        update lstTask_Update;

        Test.stopTest();

        //---------------------------------------------------
        // TEST
        //---------------------------------------------------
        List<Account_Plan_2__c> lstAccountPlan = 
            [
                SELECT 
                    Id, Number_of_Activities__c
                FROM
                    Account_Plan_2__c
                WHERE
                    Id = :lstIPlan2
            ];

        for (Account_Plan_2__c oAccountPlan : lstAccountPlan){
            if (oAccountPlan.Id == lstIPlan2[0].Id){
                System.assertEquals(oAccountPlan.Number_of_Activities__c, 0);
            }else if (oAccountPlan.Id == lstIPlan2[1].Id){
                System.assertEquals(oAccountPlan.Number_of_Activities__c, 10);
            }
        }
        //---------------------------------------------------
    }    

    @isTest static void test_tr_Task_UpdateParent_Delete() {

        List<Task> lstTask_Delete = new List<Task>();

        //---------------------------------------------------
        // CREATE TEST DATA
        //---------------------------------------------------
        createTestData();
        insert lstTask_Insert;
        //---------------------------------------------------

        List<Task> lstTask = [SELECT Id, WhatId FROM Task WHERE WhatId = :lstIPlan2];
        for (Task oTask : lstTask){
            if (oTask.WhatId == lstIPlan2[0].Id){
                lstTask_Delete.add(oTask);
            }
        }

        Test.startTest();

        delete lstTask_Delete;

        Test.stopTest();

        //---------------------------------------------------
        // TEST
        //---------------------------------------------------
        List<Account_Plan_2__c> lstAccountPlan = 
            [
                SELECT 
                    Id, Number_of_Activities__c
                FROM
                    Account_Plan_2__c
                WHERE
                    Id = :lstIPlan2
            ];

        for (Account_Plan_2__c oAccountPlan : lstAccountPlan){
            if (oAccountPlan.Id == lstIPlan2[0].Id){
                System.assertEquals(oAccountPlan.Number_of_Activities__c, 0);
            }else if (oAccountPlan.Id == lstIPlan2[1].Id){
                System.assertEquals(oAccountPlan.Number_of_Activities__c, 0);
            }
        }
        //---------------------------------------------------
    }       

}