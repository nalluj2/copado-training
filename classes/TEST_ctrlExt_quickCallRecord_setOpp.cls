//------------------------------------------------------------------------------------------------------------------------------------------------------
//  Author      : Bart Caelen
//  Date        : 23/06/2015
//  Description : CR-5767
//      This is the Test Class for the APEX Class ctrlExt_quickCallRecord_setOpp.
//------------------------------------------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_ctrlExt_quickCallRecord_setOpp {
	
    @testSetup static void createTestData() {

        clsTestData.createAccountData();
        clsTestData.createContactData();
        clsTestData.createOpportunityData();
        clsTestData.createCallRecordData();
        clsTestData.createContactVisitReportData();
    
    }

    @isTest static void test_quickCallRecord_SetOpp() {

        Boolean bTest = false;
        String tTest = '';
        Call_Records__c oCallRecord;

        List<Call_Records__c> lstCallRecord = [SELECT Id, Name FROM Call_Records__c];

        ApexPages.StandardController oStdCtrl = new ApexPages.StandardController(lstCallRecord[0]);
        ctrlExt_quickCallRecord_setOpp oCtrlExt = new ctrlExt_quickCallRecord_setOpp(oStdCtrl);

        oCallRecord = oCtrlExt.oCallRecord;
        
        bTest = oCtrlExt.bRemoveFilter;
        System.assertEquals(bTest, false);

        oCtrlExt.toggleFilter();

        bTest = oCtrlExt.bRemoveFilter;
        System.assertEquals(bTest, true);

        oCtrlExt.toggleFilter();

        bTest = oCtrlExt.bRemoveFilter;
        System.assertEquals(bTest, false);

        bTest = oCtrlExt.bSaveError;
        System.assertEquals(bTest, false);

        tTest = oCtrlExt.tOpportunityLookupFilter;
        tTest = oCtrlExt.tOpportunityLookupFilter_Escaped;

        clsUtil.hasException = false;
        oCtrlExt.save();
        clsUtil.hasException = true;
        try{
            oCtrlExt.save();
        }catch(Exception oEX){
        }

	}
	
}
//------------------------------------------------------------------------------------------------------------------------------------------------------