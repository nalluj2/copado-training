/*
 * Description		: Test class for Trigger tr_Opportunity_BeforeInsertUpdate		 
 * Author        	: Patrick Brinksma
 * Created Date    	: 16-07-2013
 */
@isTest(SeeAllData=true)
//See all data true as access to User Sbu is needed (Patient affiliation creation)
private class Test_tr_Opportunity_BeforeInsertUpdate {

	static testMethod void testPopulatePhysicianAccount() {
    	// Create test person account
    	String randVal = Test_TestDataUtil.createRandVal();
        List<Account> listOfAccount = Test_TestDataUtil.createSAPPersonAccounts(randVal, 1);
        insert listOfAccount;
        // Create test Opportunity
        List<String> listOfRecordTypeDevName = new List<String>{'CAN_DIB_OPPORTUNITY'};
        List<Opportunity> listOfOppty = Test_TestDataUtil.createOpportunities(randVal, listOfAccount, listOfRecordTypeDevName, 2);
        // Empty physician account id for test
        listOfOppty[0].Physician_Account_ID__c = null;
        // Create a physician account + contact for update test
        Account physAccount = Test_TestDataUtil.createHealthInsurers('Physician' + randVal, 'CANADA', 1)[0];
        physAccount.SAP_ID__c = 'SAPHIX';
	 	insert physAccount;
	 	Contact physContact = Test_TestDataUtil.createAccountContacts('Physician' + randVal, 1, new List<Account>{physAccount})[0];
	 	insert physContact;        
        
		Test.startTest();
        insert listOfOppty;
        
        //-BC - 20150209 - CR-7099 - START
        List<Affiliation__c> lstAffiliation =
            [
                SELECT 
                    Id
                    , Name
                    , Affiliation_From_Contact__c
                    , Affiliation_To_Account__c
                    , Affiliation_Active__c
                    , Affiliation_Primary__c
                FROM
                    Affiliation__c
                WHERE 
                    RecordType.Name = 'C2A'
                    AND Affiliation_Active__c = true
                    AND Affiliation_From_Contact__c = :physContact.Id
                ORDER BY 
                    Affiliation_From_Contact__c
            ];

        Map<Id, Set<Id>> mapContactID_AccountIDs = new Map<Id, Set<Id>>();
        for (Affiliation__c oAffiliation : lstAffiliation){
            Set<Id> setId_Account = new Set<Id>();
            if (mapContactID_AccountIDs.containsKey(oAffiliation.Affiliation_From_Contact__c)){
                setId_Account = mapContactID_AccountIDs.get(oAffiliation.Affiliation_From_Contact__c);
            }
            setId_Account.add(oAffiliation.Affiliation_To_Account__c);
            mapContactID_AccountIDs.put(oAffiliation.Affiliation_From_Contact__c, setId_Account);
        }


        Set<Id> setID_Account = mapContactID_AccountIDs.get(physContact.Id);
        if ( (listOfOppty[1].Physician_Account_ID__c != null) && (!setID_Account.contains(listOfOppty[1].Physician_Account_ID__c)) ){
            try{
                listOfOppty[1].Contact_ID__c = physContact.Id;
                update listOfOppty;
            }catch(DmlException oEX){
                system.assert(oEX.getMessage().contains('Physician is not related to the chosen account'));
            }
        }

        listOfOppty[1].Physician_Account_ID__c = null;
        listOfOppty[1].Contact_ID__c = physContact.Id;
        update listOfOppty;
//        listOfOppty[1].Contact_ID__c = physContact.Id;
//        update listOfOppty;
        //-BC - 20150209 - CR-7099 - STOP
		Test.stopTest();
		
	}

    static testMethod void testPopulatePersonContactId() {
    	// Create test person accounts
    	String randVal = Test_TestDataUtil.createRandVal();
        List<Account> listOfAccount = Test_TestDataUtil.createPersonAccounts(randVal, 10);
        insert listOfAccount;
        Map<Id, Id> mapOfAccountIdToPersonContactId = new Map<Id, Id>();
        for (Account a : listOfAccount){
        	mapOfAccountIdToPersonContactId.put(a.Id, a.PersonContactId);
        }
        // Create test Opportunities with different record types (trigger includes CAN_DIB_OPPORTUNITY, use CVG_Project for other)
        List<String> listOfRecordTypeDevName = new List<String>();
        for (Integer i=0; i<5; i++){
        	listOfRecordTypeDevName.add('CAN_DIB_OPPORTUNITY');
        	listOfRecordTypeDevName.add('CVG_Project');
        }
        List<Opportunity> listOfOppty = Test_TestDataUtil.createOpportunities(randVal, listOfAccount, listOfRecordTypeDevName, 1);
        
        Test.startTest();
        insert listOfOppty;
        Test.stopTest();
        
        // Assert results
        String searchString = randVal + '%';
        List<Opportunity> listOfResult = [select Id, RecordType.DeveloperName, SAP_Status__c, SAP_Phase__c, AccountId, Person_Account_Contact__c from Opportunity where Name like :searchString];
        for (Opportunity thisOppty : listOfResult){
            if (thisOppty.RecordType.DeveloperName == 'CAN_DIB_OPPORTUNITY'){
            	System.assertEquals(thisOppty.Person_Account_Contact__c, mapOfAccountIdToPersonContactId.get(thisOppty.AccountId));  
            	System.assertEquals(thisOppty.SAP_Status__c, 'Open');
            	System.assertEquals(thisOppty.SAP_Phase__c, thisOppty.StageName);
            } else {
                System.assertEquals(thisOppty.Person_Account_Contact__c, null);    
            }
        }
        
    }
}