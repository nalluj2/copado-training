/*
 *  Author : Bart Caelen
 *  Date : 20141217
 * Description : CR-5245 - Test Class for ba_Opportunity_BusinessLeaderFollow
 */
@isTest private class Test_ba_Opportunity_BusinessLeaderFollow {
        
        @isTest static void test_ba_Opportunity_BusinessLeaderFollow() {

                Test.startTest();

                //---------------------------------------
                // TEST SCHEDULING
                //---------------------------------------
                string tCRON_EXP = '0 0 0 3 9 ? 2099';
                
                string tJobId = System.schedule('ba_Opportunity_BusinessLeaderFollow_TEST', tCRON_EXP, new ba_Opportunity_BusinessLeaderFollow());

                // Get the information from the CronTrigger API object
                CronTrigger oCronTrigger = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :tJobId];

                // Verify the expressions are the same
                System.assertEquals(tCRON_EXP, oCronTrigger.CronExpression);

                // Verify the job has not run
                System.assertEquals(0, oCronTrigger.TimesTriggered);

                // Verify the next time the job will run
                System.assertEquals('2099-09-03 00:00:00', String.valueOf(oCronTrigger.NextFireTime));
                //---------------------------------------

                //---------------------------------------
                // TEST BATCH
                //---------------------------------------
                ba_Opportunity_BusinessLeaderFollow oBA_Opportunity_BusinessLeaderFollow = new ba_Opportunity_BusinessLeaderFollow();
                Database.executeBatch(oBA_Opportunity_BusinessLeaderFollow,100);    
                //---------------------------------------

                Test.stopTest();

        }
        
}