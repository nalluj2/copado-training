/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 *
 *	Creation Date : 27/05/2009
 *  Description : Test Coverage for triggerCampaign 
 *  Author : ABSI/ Miguel Coimbra
 */
@isTest
private class Test_triggerCampaign {
    static testMethod void myUnitTest() {
        System.debug('######################### BEGIN : Test_triggerCampaign ######################');
        Test.startTest();
        Campaign [] listCamps = new Campaign[]{} ; 
               
        Campaign camp1 = new Campaign ();
        camp1.Name = 'Test Coverage '; 
        camp1.Type = 'Type';
        camp1.Business_Unit_Picklist__c = 'Diabetes';
        camp1.recordtypeid=clsUtil.getRecordTypeByDevName('Campaign', 'Diabetes_Campaign').Id;
        insert camp1 ; 
                
        listCamps.add(camp1);      
        triggerCampaign.beforeInsertUpdate(listCamps);
        
        camp1.Name = 'test update ';
        update camp1 ; 
        
        camp1.Type = 'Another Type' ;
        triggerCampaign.beforeInsertUpdate(listCamps);
        Test.stopTest();        
        System.debug('######################### END : Test_triggerCampaign ######################'); 
    }
}