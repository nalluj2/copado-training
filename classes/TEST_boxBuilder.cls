//--------------------------------------------------------------------------------------------------------------------
//  Author           : dijkea2
//  Created Date     : 26/09/2016
//  Description      : APEX Test Class for boxBuilder
//  Change Log       : 
//		- 20/03/2019 - Bart Caelen - Replaced MEACAT by CEMA
//--------------------------------------------------------------------------------------------------------------------
@IsTest private class TEST_boxBuilder {

    @testSetup
    static void createTestData() {

		Product2 testMITGProd = new Product2();
		testMITGProd.Name = 'Test Product';
		testMITGProd.RecordTypeId = [Select Id from RecordType where SObjectType = 'Product2' AND DeveloperName = 'SAP_Product'].Id;
		testMITGProd.KitableIndex__c = 'Not Approved';
		insert testMITGProd;
		
		List<DIB_Country__c> countries = new List<DIB_Country__c>();
		
		for(Integer i = 0; i < 10; i++){
			
			DIB_Country__c country = new DIB_Country__c();
			country.Name = 'Test EUR Country ' + i;
			country.Region_vs__c = 'Europe';
			country.Box_Builder_enabled__c = true;
			country.Country_ISO_Code__c = 'E' + i;
			
			countries.add(country);
		}
		
		for(Integer i = 0; i < 10; i++){
			
			DIB_Country__c country = new DIB_Country__c();
			country.Name = 'Test MEA Country ' + i;
			country.Region_vs__c = 'CEMA';
			country.Box_Builder_enabled__c = true;
			country.Country_ISO_Code__c = 'M' + i;
			
			countries.add(country);
		}
		
		insert countries;
		
		List<MITG_App_Procedure__c> procedures = new List<MITG_App_Procedure__c>();
		
		for(Integer i = 0; i < 10; i++){
			
			MITG_App_Procedure__c procedure = new MITG_App_Procedure__c();
			procedure.Name = 'Test Procedure ' + i;
			procedure.Region__c = 'EUR;MEA';
			procedure.App__c = 'Procedural Solutions Builder';
						
			procedures.add(procedure);
		}
		
		insert procedures;
    }
	
	static private testmethod void testProductConsole(){
		
		Product2 testProd = [Select Id from Product2];
		
		PageReference pr = Page.MITG_Product_Console;
		pr.getParameters().put('productId', testProd.Id);
		Test.setCurrentPage(pr);
		
		ctrl_MITG_Product_Console controller = new ctrl_MITG_Product_Console(new ApexPages.StandardController(new Product_Country_Availability__c()));
		
		System.assert(controller.contextProd != null);		
		System.assertEquals('Not Approved', controller.contextProd.KitableIndex__c);
		System.assertEquals(2, controller.getRegions().size());
		System.assertEquals(10, controller.procedures.size());
									
		controller.bulkStatus.get('Europe').Status__c = 'Available';
		controller.bulkRegion = 'Europe';
		
		controller.bulkStatusChanged();
		
		controller.kitableValue = 'Kitable';
		
		controller.selectAllProcedures();
		
		controller.saveConfig();
		
		testProd = [Select Id, KitableIndex__c from Product2 where Id = :testProd.Id];
		
		System.assertEquals('Kitable', testProd.KitableIndex__c);
		
		List<Product_Country_Availability__c> productCountry = [Select Id from Product_Country_Availability__c where Product__c = :testProd.Id];		
		System.assertEquals(20, productCountry.size());		
		
		List<Product_Procedure_Availability__c> productProcedures = [Select Id from Product_Procedure_Availability__c where Product__c = :testProd.Id];		
		System.assertEquals(10, productProcedures.size());		
	}  
	
	/*
	private static testmethod void testKitableFlagValidation(){
		
		Product2 testMITGProd = [Select Id, KitableIndex__c from Product2];
		
		testMITGProd.KitableIndex__c = 'Kitable';
		update testMITGProd;
		
		Procedural_Solutions_Template_Kit__c template = new Procedural_Solutions_Template_Kit__c();
		template.Box__c = true;
		template.Box_Description__c = 'Unit Test Box';
		insert template;
		
		Procedural_Solutions_Template_Kit_Prod__c templateProd = new Procedural_Solutions_Template_Kit_Prod__c();
		templateProd.Name = 'Test Prod';
		templateProd.Procedural_Solutions_Template_Kit__c = template.Id;
		templateProd.Product__c = testMITGProd.Id;
		templateProd.Quantity__c = 1;
		insert templateProd;
		
		Test.startTest();
		
		Boolean isError = false;
		
		try{
			
			testMITGProd.KitableIndex__c = 'Not Approved';
			update testMITGProd;
			
		}catch(Exception e){
			
			isError = true;
			System.assert(e.getMessage().contains('This Product is being used in Pre-configured Boxes.'));	
		}
		
		System.assert(isError == true);
		
		delete templateProd;
		
		update testMITGProd;
		
		testMITGProd = [Select Id, KitableIndex__c from Product2];
		
		System.assertEquals('Not Approved', testMITGProd.KitableIndex__c);
	}
	*/
}