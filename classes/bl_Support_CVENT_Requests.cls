public without sharing class bl_Support_CVENT_Requests {
	
	private ctrl_Support_Requests.SessionData session;
	
	public List<SelectOption> buOptions {get; set;}
    public String searchAlias {get; set;}

	public bl_Support_CVENT_Requests( ctrl_Support_Requests.SessionData sData){
		
		this.session = sData;		
	}
	    
    public PageReference requestCVentChangeRequest(){
        
        session.resetRequest();
        
        session.request.status__c='New';
        session.request.application__c='CVent';
        session.request.ownerId = session.internalUserId;
        session.request.request_Type__c = 'Change request';
        session.request.priority__c = 'Medium';
        
        session.clearAttachments();        
        session.readOnlyAttachments = new List<Attachment>();
                               
        session.comment = null;
        session.comments = null;
        		                                        
        return Page.Support_CVent_Change_Request;         
    }
    
    public PageReference requestCVentServiceRequest(){
        
        session.resetRequest();
        
        session.request.status__c='New';
        session.request.application__c='CVent';
        session.request.ownerId = session.internalUserId;
        session.request.request_Type__c = 'Generic Service';
               
        session.clearAttachments();        
        session.readOnlyAttachments = new List<Attachment>();
                               
        session.comment = null;
        session.comments = null;
        		                                        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.info, 'Please, include an example if possible; e.g. which event, user, contact, etc...'));
        		                                        
        return Page.Support_CVent_Generic_Service;         
    }
        
    public void createCVentRequest(){
    	
        if(session.request.request_Type__c == 'Change request' && session.request.Area_CR__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Area is required'));
            return;
        }

        if(session.request.request_Type__c == 'Generic Service' && session.request.Area_SR__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Area is required'));
            return;
        }

    	if(session.request.Description_Long__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Description is required'));
            return;
        }

        
        Create_User_Request__c toInsertRequest = session.request.clone(false);

        SavePoint sp = Database.setSavepoint();
        
        try{
                                
            toInsertRequest.Requestor_Name__c = session.uInfo.firstName+' '+session.uInfo.lastName;
            toInsertRequest.Requestor_Email__c = session.uInfo.email;
            insert toInsertRequest;
            
            session.saveAttachments(toInsertRequest.Id);
                        
            session.request.Id = toInsertRequest.Id;
            
            session.comment = new User_Request_Comment__c();
                        
        } catch (Exception e) {
            clsUtil.debug('Error in bl_Support_CVENT_Requests.createCVentRequest on line ' + e.getLineNumber() + ' : ' + e.getMessage());
            ApexPages.addMessages(e);
            Database.rollback(sp);
            
            if(Test.isRunningTest()) throw e;
            
            return ;
        }finally{
            session.clearAttachments(); 
        }
        
        session.request = bl_SupportRequest.loadSupportRequestDetailData(session.request.Id);
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request '+session.request.Name+' created successfully.'));    
    }

}