/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_redirectToVisitReport {
    static testMethod void myUnitTest() {
        System.debug(' ################## ' + 'BEGIN Test_redirectToVisitReport' + ' ################');        
        Account acc1 = new Account();
        acc1.Name = 'Test_redirectToVisitReport Test Coverage Account 1 ' ; 
        Test.startTest();
        insert acc1 ;               
        
        Contact cont1 = new Contact();
        cont1.LastName = 'Test_redirectToVisitReport Test Coverage' ;  
        cont1.FirstName = 'TEST';  
        cont1.AccountId = acc1.Id ;  
        cont1.Phone = '009569699'; 
        cont1.Email ='test@coverage.com';
	    cont1.Contact_Department__c = 'Diabetes Adult'; 
	    cont1.Contact_Primary_Specialty__c = 'ENT';
	    cont1.Affiliation_To_Account__c = 'Employee';
	    cont1.Primary_Job_Title_vs__c = 'Manager';
	    cont1.Contact_Gender__c = 'Male'; 
        insert cont1 ;          

        //Insert Company
        Company__c cmpny = new Company__c();
        cmpny.name='TestMedtronic';
        cmpny.CurrencyIsoCode = 'EUR';
        cmpny.Current_day_in_Q1__c=56;
        cmpny.Current_day_in_Q2__c=34; 
        cmpny.Current_day_in_Q3__c=5; 
        cmpny.Current_day_in_Q4__c= 0;   
        cmpny.Current_day_in_year__c =200;
        cmpny.Days_in_Q1__c=56;  
        cmpny.Days_in_Q2__c=34;
        cmpny.Days_in_Q3__c=13;
        cmpny.Days_in_Q4__c=22;
        cmpny.Days_in_year__c =250;
        cmpny.Company_Code_Text__c = 'T18';
        insert cmpny;        
        
        Call_Records__c vr1 = new Call_Records__c(); 
        //vr1.Type__c = '1:1 visit' ; 
        vr1.Call_Date__c = system.today(); 
        insert vr1 ;    
                
        Contact_Visit_Report__c cvr1 = new Contact_Visit_Report__c();
        cvr1.Attending_Contact__c = cont1.Id ;
        cvr1.Attending_Affiliated_Account__c = acc1.Id ;  
        cvr1.Call_Records__c = vr1.Id ; 
        insert cvr1 ;           
        
        Call_Activity_Type__c subj1 = new Call_Activity_Type__c();
        subj1.name = 'Test Subject 11';
        subj1.Company_ID__c = cmpny.id;
        insert subj1;
        
        Call_Topics__c sb1 = new Call_Topics__c (); 
        sb1.Call_Records__c = vr1.Id; 
        sb1.Call_Activity_Type__c = subj1.Id;
        sb1.Call_Topic_Duration__c=1;
        insert sb1;
        
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(cvr1); 
        redirectToVisitReport controller = new redirectToVisitReport(sc); 
        controller.GoToEditPage();

        ApexPages.Standardcontroller sc1 = new ApexPages.Standardcontroller(sb1); 
        redirectCallTopicToVisitReport controller1 = new redirectCallTopicToVisitReport(sc1); 
        controller1.GoToEditPage();

        ApexPages.Standardcontroller sc2 = new ApexPages.Standardcontroller(vr1); 
        redirectToCallRecord controller2 = new redirectToCallRecord(sc2); 
        controller2.GoToEditPage();
        Test.stopTest();  
        
        System.debug(' ################## ' + 'END Test_redirectToVisitReport' + ' ################');       
    }
}