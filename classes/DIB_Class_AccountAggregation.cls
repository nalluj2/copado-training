public with sharing class DIB_Class_AccountAggregation {
	/**
	 * Creation Date: 	20090915
	 * Description: 	This class will construct an Object type.  Composed by with different Arguments. 
	 *					-  It will be called by the Allocation Visual force Pages to show up the accounts to be allocated 
	 *					for the chosen period / type. 
	 * Author: 	ABSI - MC
	 */	 
 	
	public DIB_Invoice_Line__c  agg_account ;	
	private Integer done_nr ; 	
	private Integer todo_nr ;
	private Integer total_nr ; 
	public DIB_Invoice_Line__c  completed;
	private String city; 
	private DIB_Aggregated_Allocation__c[] Aggregated_Allocation_Per_Department;
	private Account Sales_Force_Account;

	public Integer cgmsAggregatedCount {set;get;} 
	public Integer otherAggregatedCount {set;get;}
	public Boolean selected {set;get;}
		
	// Sold  to / Ship to  reference of type Account
	public DIB_Invoice_Line__c  getAgg_Account(){		return this.agg_account; 	}
	public void setAgg_Account(DIB_Invoice_Line__c  a){	this.agg_account = a ; 	}
	
	// Total number of Invoice lines that are allocated 
	public Integer getDone_nr(){						return this.done_nr; 		}
	public void setDone_nr(Integer x){						this.done_nr = x; 		}
	
	// Total number of Invoice Lines that are left to be done. 
	public Integer getTodo_nr(){						return this.todo_nr; 		}
	public void setTodo_nr(Integer x){						this.todo_nr = x; 		}
	
	// Total number of Invoice Lines in his total
	public Integer getTotal_nr(){						return this.total_nr; 		}
	public void setTotal_nr(Integer x){						this.total_nr = x; 		}
	
	// Flag to know if the Account is completed or not for that period.
	public DIB_Invoice_Line__c  getCompleted(){			return this.completed; 		}
	public void setCompleted(DIB_Invoice_Line__c  t){		this.completed = t;		}
	
	public String getCity(){								return this.city ; 			}
	public void setCity(String s){							this.city = s ; 			}
	
	
	public void setAggregated_Allocation_Per_Department(List<DIB_Aggregated_Allocation__c> Aggregated_Allocation_Per_DepartmentIn) {
		Aggregated_Allocation_Per_Department = Aggregated_Allocation_Per_DepartmentIn;
	}

	public List<DIB_Aggregated_Allocation__c> getAggregated_Allocation_Per_Department() {
		return Aggregated_Allocation_Per_Department;
	}	
		
	public void setSales_Force_Account(Account Sales_Force_AccountIn) {
		Sales_Force_Account = Sales_Force_AccountIn;
	}
	
	public Account getSales_Force_Account() {
		return Sales_Force_Account;
	}	
	
	/** 
	 *	Constructors 
	 */
	 
	public DIB_Class_AccountAggregation(){ 
	}

	public DIB_Class_AccountAggregation(Id accountId, Double cgmsAggregatedCount, Double otherAggregatedCount, Boolean aggregateSelected){
		DIB_Invoice_Line__c  newFakeIL = new DIB_Invoice_Line__c(); 
		newFakeIL.Sold_To__c = accountId;
		newFakeIL.Is_Allocated__c = aggregateSelected; 
		selected = aggregateSelected;
		this.agg_account =  newFakeIL;
		
		this.cgmsAggregatedCount = cgmsAggregatedCount.intValue();
		this.otherAggregatedCount = otherAggregatedCount.intValue();
	}
	
	public DIB_Class_AccountAggregation(Id accId, Integer total, Integer toDo, Integer done, Boolean isCompleted, String stCity) { 
			DIB_Invoice_Line__c  newFakeIL = new DIB_Invoice_Line__c(); 
			newFakeIL.Sold_To__c = accId ; 
			this.agg_account =  newFakeIL;
			this.done_nr = done ; 
			this.todo_nr = toDo ;  
			this.total_nr = total ; 
			newFakeIL.Is_Allocated__c = isCompleted;
			selected = isCompleted;  
			this.completed =   newFakeIL;
			this.city = stCity ; 
	}
	
	public DIB_Class_AccountAggregation(Id accSoldToId,Id accShipToId, Integer total, Integer toDo, Integer done, Boolean isCompleted, String stCity) { 
			DIB_Invoice_Line__c  newFakeIL = new DIB_Invoice_Line__c(); 
			newFakeIL.Sold_To__c = accSoldToId;
			//newFakeIL.Ship_To__c = accShipToId;
			this.agg_account =  newFakeIL;
			this.done_nr = done ; 
			this.todo_nr = toDo ;  
			this.total_nr = total ; 
			newFakeIL.Is_Allocated__c = isCompleted;
			selected = isCompleted;  
			this.completed =   newFakeIL;
			this.city = stCity ; 
	}	

	public DIB_Class_AccountAggregation(Id accSoldToId, String currentShipToSapId, String currentShipToName, String currentShipToCity, Integer total, Integer toDo, Integer done, Boolean isCompleted, String stCity) { 
			DIB_Invoice_Line__c  newFakeIL = new DIB_Invoice_Line__c(); 
			newFakeIL.Sold_To__c = accSoldToId;
			newFakeIL.Ship_To_SAP_ID__c = currentShipToSapId;
			newFakeIL.Ship_To_Name_Text__c = currentShipToName;
			newFakeIL.Ship_To_City_Text__c = currentShipToCity;
			this.agg_account =  newFakeIL;
			this.done_nr = done ; 
			this.todo_nr = toDo ;  
			this.total_nr = total ; 
			newFakeIL.Is_Allocated__c = isCompleted;
			selected = isCompleted;  
			this.completed =   newFakeIL;
			this.city = stCity ; 
	}	
	
}