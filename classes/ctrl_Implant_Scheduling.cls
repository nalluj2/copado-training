public with sharing class ctrl_Implant_Scheduling {
    
    
    public ctrl_Implant_Scheduling(){
    	
    }
    /*
    public List<SelectOption> userTeams {get; set;}
    public String selectedTeam {get; set;}
    private String teamCountry {get; set;}
    private List<String> teamSubBusiness {get; set;}
    
    public Boolean usesExtendedTeam {get; set;}
    public List<SelectOption> extendedTeams {get; set;}
    public Integer selectedExtendedLevel {get; set;}
    
    public List<String> caseFields {get; set;}
    public Map<String, CaseField> caseFieldInfo {get; set;}
    public List<Case> teamCases {get; set;}
    public String selectedCase {get; set;}
    
    private Set<String> caseClosedStatuses;
    
    private List<User> teamMembers {get; set;}
    public String selectedMember {get; set;}
    
    public List<TeamMemberEventData> teamMemberData {get; set;}
     
    public ctrl_Implant_Scheduling(){
                        
        teamCases = new List<Case>();
        
        caseClosedStatuses = new Set<String>();
        
        for(CaseStatus status : [Select MasterLabel from CaseStatus where IsClosed = true]){
            
            caseClosedStatuses.add(status.MasterLabel);
        }
        
        userTeams = new List<SelectOption>();       
        userTeams.add(new SelectOption('', '--None--'));
        
        for(Implant_Scheduling_Team_Admin__c teamAdmin : [Select Team__c, Team__r.Name, Team__r.Business_Unit__r.Name, Team__r.Sub_Business_Unit__r.Name, Team__r.Country__r.Country_ISO_Code__c 
                                                                from Implant_Scheduling_Team_Admin__c 
                                                                where Admin__c = :UserInfo.getUserId()
                                                                ORDER BY Team__r.Country__r.Country_ISO_Code__c, Team__r.Business_Unit__r.Name, Team__r.Sub_Business_Unit__r.Name, Team__r.Name ASC]){
            
            String teamBusiness = teamAdmin.Team__r.Business_Unit__r != null ? teamAdmin.Team__r.Business_Unit__r.Name : teamAdmin.Team__r.Sub_Business_Unit__r.Name;
            
            userTeams.add(new SelectOption(teamAdmin.Team__c, teamBusiness + '(' + teamAdmin.Team__r.Country__r.Country_ISO_Code__c + ') - ' + teamAdmin.Team__r.Name));
        }
        
        if(userTeams.size() == 2)
        {
            selectedTeam = userTeams[1].getValue();
            teamSelected();
        }   
    }
    
    public void teamSelected(){
        
        // Clear data
        selectedCase = null;
        selectedExtendedLevel = 0;
        
        if(selectedTeam == null || selectedTeam == '') return;
                
        Implant_Scheduling_Team__c team = [Select Business_Unit__c, Sub_Business_Unit__c, Country__r.Name, Country__r.Country_ISO_Code__c, Display_Case_fields__c from Implant_Scheduling_Team__c where Id= :selectedTeam];
        
        caseFields = team.Display_Case_fields__c.split(';');        
        teamCountry = team.Country__r.Name;
        
        teamSubBusiness = new List<String>();
        
        if(team.Sub_Business_Unit__c != null){
            
            teamSubBusiness.add(team.Sub_Business_Unit__c);
                    
        }else{
            
            for(Sub_Business_Units__c teamSBU : [Select Id from Sub_Business_Units__c where Business_Unit__c = :team.Business_Unit__c]){
                teamSubBusiness.add(teamSBU.Id);
            }
        }
        
        getTeamModel(team.Country__r.Country_ISO_Code__c);
        
        getCaseFieldInformation();
        
        caseSortColumn = 'Start_of_Procedure__c';
        caseSortDir = 'ASC';
        
        getTeamCases();
        
        getTeamMembers();
        
        Map<String, TeamMemberEventData> teamData = initializeTeamMemberData();
        teamMemberData = teamData.Values();
        
        teamSortColumn = 'Name';
        teamSortDir = 'ASC';
        teamMemberData.sort();
    }
    
    private void getCaseFieldInformation(){
        
        Map<String, Schema.SObjectField> caseFieldTokens = Case.sObjectType.getDescribe().fields.getMap();
        
        caseFieldInfo = new Map<String, CaseField>();
        
        for(String fieldName : caseFields){
            
            Schema.DescribeFieldResult fieldDescribe = caseFieldTokens.get(fieldName).getDescribe();
            
            CaseField fieldInfo = new CaseField();
                        
            if(fieldDescribe.getType().name() == 'Reference'){
                
                if(fieldName.endsWith('Id')){
                    fieldInfo.sortName = fieldName.substring(0, fieldName.length() -2) + '.Name';
                    String tempLabel = fieldDescribe.getLabel();
                    fieldInfo.label = tempLabel.substring(0, tempLabel.length()-3);
                }else{
                    fieldInfo.sortName = fieldName.replace('__c', '__r.Name');
                    fieldInfo.label = fieldDescribe.getLabel();
                }
                
            }else{
                fieldInfo.sortName = fieldName;
                fieldInfo.label = fieldDescribe.getLabel();
            }
            
            caseFieldInfo.put(fieldName, fieldInfo);
        }       
    }
    
    private void getTeamModel(String countryCode){
                
        Implant_Scheduling_Team_Model__c countrySettings = Implant_Scheduling_Team_Model__c.getInstance(countryCode);           
        if(countrySettings == null) countrySettings = Implant_Scheduling_Team_Model__c.getInstance('Default');
        
        usesExtendedTeam = countrySettings.Uses_Extended_Team__c;
        
        if(usesExtendedTeam == true){   
            
            extendedTeams = new List<SelectOption>();
            extendedTeams.add(new SelectOption('0', '--None--'));
            
            for(Implant_Scheduling_Team__c extendedTeam : [Select Id, Name, Extended_Team_Level__c from Implant_Scheduling_Team__c where Core_Team__c = :selectedTeam ORDER BY Extended_Team_Level__c ASC]){
                
                extendedTeams.add(new SelectOption(String.valueOf(extendedTeam.Extended_Team_Level__c), extendedTeam.Extended_Team_Level__c + ' (' + extendedTeam.Name + ')'));
            }
        }
    }
    
    private void getTeamCases(){
                
        String caseQuery = 'Select Id, CaseNumber,';
        caseQuery += String.join(caseFields, ',');
        
        //If the Team uses Territory Team model this variable is used to store the Ids of all the Accounts in the Team Territories
        Set<Id> teamAccounts;
    
        if(usesExtendedTeam == true){
                
            caseQuery += ' from Case where Sub_Business_Unit_Lookup__c IN :teamSubBusiness AND Account.Account_Country_vs__c= :teamCountry AND RecordType.DeveloperName = \'Implant_Scheduling\'';
             
        }else{
        
            //Get Cases based on Territories of their Account   
            teamAccounts = getTeamAccounts();
            caseQuery += ' from Case where Sub_Business_Unit_Lookup__c IN :teamSubBusiness AND AccountId IN :teamAccounts AND RecordType.DeveloperName = \'Implant_Scheduling\'';
        }
        
        caseQuery += ' AND Start_of_Procedure__c != null AND Status NOT IN :caseClosedStatuses';
                
        String sortingColumn;
        
        if(caseFieldInfo.get(caseSortColumn) != null){
            sortingColumn = caseFieldInfo.get(caseSortColumn).sortName;
        }else{
            sortingColumn = caseSortColumn;
        }
        
        caseQuery += ' ORDER BY '+sortingColumn+' '+caseSortDir;
   system.debug('### caseQuery = ' + caseQuery);     
        teamCases = Database.query(caseQuery);
    }
    
    private Set<Id> getTeamAccounts(){
        
        List<Id> teamTerritories = new List<Id>();
        
        for(Implant_Scheduling_Team_Territory__c teamTerritory : [Select Territory_Id__c from Implant_Scheduling_Team_Territory__c where Team__c = :selectedTeam]){
            
            teamTerritories.add(teamTerritory.Territory_Id__c);
        }
        
        return bl_Territory.calculateAccountsBelowTerritory(teamTerritories);
    }
    
    private void getTeamMembers(){
        
        teamMembers = [Select Id, Name, ContactId, Job_Title_vs__c, Level_of_Training__c from User where Id IN (Select Member__c from Implant_Scheduling_Team_Member__c where (Team__c= :selectedTeam OR (Team__r.Core_Team__c = :selectedTeam AND Team__r.Extended_Team_Level__c <= : selectedExtendedLevel)))];
    }
       
    public void extendedLevelSelected(){
        
        getTeamMembers();
                        
        Map<String, TeamMemberEventData> teamData = initializeTeamMemberData();
        
        if(selectedCase != null){
            
            Set<String> oldMembers = new Set<String>();
            
            //Keep the data of the members that were loaded before (i.e core Team Members)
            for(TeamMemberEventData memberData : teamMemberData){
                
                oldMembers.add(memberData.memberId);
                
                if(teamData.get(memberData.memberId) != null){
                    teamData.put(memberData.memberId, memberData);
                }
            }
            
            //Get the list of the new Member and load their data
            List<User> newMembers = new List<User>();
            for(String memberId : teamData.keySet()){
                
                if(oldMembers.contains(memberId) == false){
                    newMembers.add(new User(Id = memberId));
                }
            }
            
            if(newMembers.size()>0){            
                
                loadCaseDataForUsers(teamData, newMembers);
            }
        }
        
        //Update the list
        teamMemberData = teamData.Values();
                
        teamMemberData.sort();              
    }
    
    public void caseSelected(){
        
        Map<String, TeamMemberEventData> teamData = initializeTeamMemberData(); 
        
        loadCaseDataForUsers(teamData, teamMembers);
        
        teamMemberData = teamData.Values();
        
        sortField = teamSortColumn = 'Availability';
        sortDir = teamSortDir = 'ASC';
        teamMemberData.sort();
    }
    
    private void loadCaseDataForUsers(Map<String, TeamMemberEventData> teamData, List<User> users){
        
        Case selCase = [Select Id, Start_of_Procedure__c, End_of_Procedure__c, AccountId, Sub_Business_Unit_Lookup__c from Case where Id= :selectedCase];
        
        Map<Id, Driving_Distance__c> ddMap = new Map<Id, Driving_Distance__c>();

        List<Driving_Distance__c> ddList = [Select Id,
   				    					           Driving_Distance__c,
   				    					           User__c	
					    					  From Driving_Distance__c
					    					 Where User__c = :teamMembers
					    					   And Account__c = :selCase.AccountId
		];
		    						   
    	if(ddList!=null) {
    		for(Driving_Distance__c dd : ddList) { 
    			if(!ddMap.containsKey(dd.User__c)) {
    				ddMap.put(dd.User__c, dd);
    			}
    		}
    	}

		System.debug('### Implant_Scheduling_Team_Model__c.getValues(Default).Number_of_Days__c' + Implant_Scheduling_Team_Model__c.getValues('Default').Number_of_Days__c);
		Date implDate = System.today() - Integer.valueof(Implant_Scheduling_Team_Model__c.getValues('Default').Number_of_Days__c);
        Map<Id, Integer> aggMap = new Map<Id, Integer>();
        
    	AggregateResult[] aggList = [Select User_ID__c,
		    					 		    count(Id) Total
		    						   From Implant__c 
		    						  Where User_ID__c = :teamMembers 
		    							And Implant_Implanting_Account__c = :selCase.AccountId
		    							And Therapy__r.Sub_Business_Unit__c = :selCase.Sub_Business_Unit_Lookup__c
		    							And Implant_Date_Of_Surgery__c >= :implDate
		    							And Attended_Implant_Text__c = :'Attended'
		    					   Group By User_ID__c 								 
    	];
    	
        
    	if(aggList!=null) {
    		for(AggregateResult agg : aggList) {
    			if(!aggMap.containsKey((Id)agg.get('User_ID__c'))) aggMap.put((Id)agg.get('User_ID__c'), (Integer)agg.get('Total'));
    		}
    	}

        UserEventData userEventService = new UserEventData(); 
        
        Map<Id, Event> busyUserEvents = userEventService.getBusyUserEvents(users, selCase.Start_of_Procedure__c, selCase.End_of_Procedure__c);
                
        //The Events are ordered by StartDateTime. This is important to determine what is the previous and next Event per Member 
        for(User teamMember : users){
            
            //Get the Team Member Event Data for the selected Case
            TeamMemberEventData memberData = teamData.get(teamMember.Id);
                        
            // If the User has an Event that overlaps with the selected Case, we know this User is not available for assignment
            Event overlapEvent = busyUserEvents.get(teamMember.Id);
            
            if(overlapEvent != null){                   
                memberData.isAvailable = false;
                if(overlapEvent.isPrivate == false) memberData.overlapEventId = overlapEvent.Id;
                memberData.overlapEventName = getEventTitle(overlapEvent);
            }           
                                    
            // Previous Event
            Event prevEvent = userEventService.getUserPreviousEvent(teamMember.Id, selCase.Start_of_Procedure__c);
            
            if(prevEvent != null){
                if(prevEvent.isPrivate == false) memberData.previousEventId = prevEvent.Id;
                memberData.previousEventName = getEventTitle(prevEvent);
                //We store the end of the last Event before the Implant start. To be used for sorting later on
                if(prevEvent.isAllDayEvent == true) memberData.millisBeforeStart = selCase.Start_of_Procedure__c.getTime() - prevEvent.EndDateTime.addDays(1).getTime();
                else memberData.millisBeforeStart = selCase.Start_of_Procedure__c.getTime() - prevEvent.EndDateTime.getTime();
            }
            
            // Next Event
            Event nextEvent = userEventService.getUserNextEvent(teamMember.Id, selCase.End_of_Procedure__c);
            
            if(nextEvent != null){
                if(nextEvent.isPrivate == false) memberData.nextEventId = nextEvent.Id;
                memberData.nextEventName = getEventTitle(nextEvent);
                //We store the start of the first Event after the Implant end. To be used for sorting later on
                memberData.millisAfterEnd = nextEvent.StartDateTime.getTime() - selCase.End_of_Procedure__c.getTime();
            }     

 			try {		    	
	            if(ddMap!=null && ddMap.get(teamMember.Id).Driving_Distance__c != null) {	            	
	            	memberData.drivingDistance = ddMap.get(teamMember.Id).Driving_Distance__c.round(System.RoundingMode.HALF_UP).format();
	            } else {
            		memberData.drivingDistance = '';
            	}   
 			} catch(Exception ex0) {
 				memberData.drivingDistance = '';
 			}                  

			try {	
	            if(aggMap!=null && aggMap.get(teamMember.Id) != null) {
	            	memberData.nbrOfImplants = String.valueOf(aggMap.get(teamMember.Id));
	            } else {
	            	memberData.nbrOfImplants = '';
	            }
			} catch(Exception ex1) {
				memberData.nbrOfImplants = '';
			}                       
        }
    }
     
    public void assignCase(){
        
        Case selCase = [Select Id, Start_of_Procedure__c, ContactId, End_of_Procedure__c, Account.Name, Account.BillingCity, Subject, Description from Case where Id= :selectedCase];
        
        Savepoint sp = Database.setSavepoint();
        
        try{
            
            selCase.Status = 'Closed';
            update selCase;
            
            Event assignment = new Event();
            assignment.OwnerId = selectedMember;
            assignment.WhatId = selectedCase;
            assignment.WhoId = selCase.ContactId;
            assignment.Type = 'Implant Support';
            assignment.StartDateTime = selCase.Start_of_Procedure__c;
            assignment.EndDateTime = selCase.End_of_Procedure__c;
            assignment.Location = selCase.Account.Name;
            if(selCase.Account.BillingCity != null)  assignment.Location += ', ' + selCase.Account.BillingCity;
            assignment.Subject = selCase.Subject;
            assignment.Description = selCase.Description;
            assignment.ShowAs = 'Busy';
            
            insert assignment;
                            
        }catch(Exception e){
            
            Database.rollback(sp);  
            ApexPages.addMessages(e);
            return;                 
        }
        
        // Re-load all the data (the Event should not be in the list anymore)
        teamSelected();     
    }
    
    private String getEventTitle(Event evt){
        
        String eventName = '';
                
        if(evt.isAllDayEvent){ 
            
            eventName += evt.StartDateTime.date().format();
            if(evt.StartDateTime != evt.EndDateTime){
                
                if(eventName.contains('/')) eventName += ' - ';
                else eventName += ' / ';
                
                eventName += evt.EndDateTime.date().format();
            }
                    
        }else{
            
            eventName += evt.StartDateTime.format();
            if(eventName.contains('/')) eventName += ' - ';
            else eventName += ' / ';
            eventName += evt.EndDateTime.format();              
        }
        
        eventName += ' (';
        
        if(evt.isPrivate) eventName += 'Private appointment';
        else{
            if(evt.Type != null) eventName += evt.Type;
            if(evt.Location != null){ 
                if(evt.Type != null) eventName += ' / ';
                eventName += evt.Location;
            }
        }
            
        eventName += ')';
        
        return eventName;
    }
    
    private Map<String, TeamMemberEventData> initializeTeamMemberData(){
        
        Map<String, TeamMemberEventData> teamData = new Map<String, TeamMemberEventData>();
        
        for(User member : teamMembers){
            
            TeamMemberEventData memberData = new TeamMemberEventData();
            memberData.memberId = member.Id;
            memberData.memberName = member.Name;
            memberData.jobTitle = member.Job_Title_vs__c;
            memberData.trainingLevel = member.Level_of_Training__c;
            
            teamData.put(member.Id, memberData);
        }
        
        return teamData;
    }
    
    private without sharing class UserEventData{
        
        public Map<Id, Event> getBusyUserEvents(List<User> users, DateTime startDateTime, DateTime endDateTime){
                        
            Map<Id, Event> overlapEventsByUser = new Map<Id, Event>();
                        
            for(Event overlapEvent : [Select Id, StartDateTime, EndDateTime, Location, isPrivate, WhatId, Type, isAllDayEvent, OwnerId 
                        from Event 
                        where isRecurrence = false AND StartDateTime <= :endDateTime AND ShowAs IN ('Busy', 'OutOfOffice') AND
                        ( (isAllDayEvent = false AND EndDateTime >= :startDateTime) OR ((isAllDayEvent = true AND DAY_ONLY(EndDateTime) >= :startDateTime.date())) ) 
                        AND OwnerId IN :users
                        ORDER BY StartDateTime DESC]){
                
                overlapEventsByUser.put(overlapEvent.OwnerId, overlapEvent);                
            }
            System.debug(overlapEventsByUser);
            return overlapEventsByUser;
        }
        
        public Event getUserPreviousEvent(Id forUser, DateTime startDateTime){
                                    
            List<Event> userPrevEvent = [Select Id, StartDateTime, EndDateTime, Location, isPrivate, WhatId, Type, isAllDayEvent, OwnerId 
                                            from Event 
                                            where isRecurrence = false AND ShowAs IN ('Busy', 'OutOfOffice') AND 
                                            ((isAllDayEvent = false AND EndDateTime < :startDateTime) OR (isAllDayEvent = true AND DAY_ONLY(EndDateTime) < :startDateTime.date())) AND OwnerId = :forUser
                                            ORDER BY StartDateTime DESC LIMIT 1];
            
            if(userPrevEvent.isEmpty()) return null;
            
            return userPrevEvent[0];            
        }   
        
        public Event getUserNextEvent(Id forUser, DateTime endDateTime){
                                    
            List<Event> userNextEvent = [Select Id, StartDateTime, EndDateTime, Location, isPrivate, WhatId, Type, isAllDayEvent, OwnerId 
                                            from Event 
                                            where isRecurrence = false AND ShowAs IN ('Busy', 'OutOfOffice') AND StartDateTime > :endDateTime AND OwnerId = :forUser
                                            ORDER BY StartDateTime ASC LIMIT 1];    
            
            if(userNextEvent.isEmpty()) return null;
            
            return userNextEvent[0];        
        }       
    } 
    
    // Sorting functionality    
    public String teamSortColumn {get; set;}
    public String teamSortDir {get; set;}
    public String selectedTeamColumn {get; set;}    
    
    public String caseSortColumn {get; set;}
    public String caseSortDir {get; set;}
    public String selectedCaseColumn {get; set;}    
    
    public static String sortField = 'Name';
    public static String sortDir = 'ASC'; 
    
    public void sortTeamData(){
system.debug('### selectedTeamColumn=' + selectedTeamColumn);        
system.debug('### teamSortColumn=' + teamSortColumn);        
        if(selectedTeamColumn == teamSortColumn){
            
            if(teamSortDir == 'ASC') teamSortDir = 'DESC';
            else teamSortDir = 'ASC';
            
        }else{
            teamSortDir = 'ASC';
        }
        
        teamSortColumn = selectedTeamColumn;
        
        sortField = teamSortColumn;
        sortDir = teamSortDir;
        
        teamMemberData.sort();
    }   
    
    public void sortCaseData(){
        
        if(selectedCaseColumn == caseSortColumn){
            
            if(caseSortDir == 'ASC') caseSortDir = 'DESC';
            else caseSortDir = 'ASC';
            
        }else{
            caseSortDir = 'ASC';
        }
        
        caseSortColumn = selectedCaseColumn;
                      
        getTeamCases();
    }   
    
    private class TeamMemberEventData implements Comparable{
    	    	        
        public Boolean isAvailable {get; set;}
        public Long millisBeforeStart {get; set;}
        public Long millisAfterEnd {get; set;}
        
        public String memberId {get; set;}
        public String memberName {get; set;}
        public String jobTitle {get; set;}
        public String trainingLevel {get; set;}
        public String drivingDistance {get; set;}
    	public String nbrOfImplants {get; set;}
        
        public String overlapEventName {get; set;}
        public Id overlapEventId {get; set;}
        
        public String previousEventName {get; set;}
        public Id previousEventId {get; set;}
        
        public String nextEventName {get; set;}
        public Id nextEventId {get; set;}

		public String nextImplantNbrName{get; set;}
		public String nextDistanceName{get; set;}
        
        public TeamMemberEventData(){
            
            isAvailable = true;
        }
        
        public Integer compareTo(Object compareTo) {
            
            TeamMemberEventData other = (TeamMemberEventData) compareTo;
            
            sortField = ctrl_Implant_Scheduling.sortField;
            sortDir = ctrl_Implant_Scheduling.sortDir;
system.debug('### sortField=' + sortField);            
            if(sortField == 'Name'){
	            system.debug('### Name');            
                            
                if(this.memberName > other.memberName) {
                    if(sortDir == 'ASC') return 1;
                    else return -1;
                }
                                
                if(this.memberName < other.memberName) {
                    if(sortDir == 'ASC') return -1;
                    else return 1;
                }             
                
            } else if(sortField == 'JobTitle'){
                            

                 if(this.jobTitle == other.jobTitle) {
                    return 0;
                }
                            
                if(this.jobTitle > other.jobTitle) {
                     if(sortDir == 'ASC') return 1;
                    else return -1;
                }
                                
                if(this.jobTitle < other.jobTitle) {
                    if(sortDir == 'ASC') return -1;
                    else return 1;
                } 
                              
            } else if(sortField == 'LevelOfTraining'){
                            

                 if(this.trainingLevel == other.trainingLevel) {
                    return 0;
                }
                            
                if(this.trainingLevel > other.trainingLevel) {
                     if(sortDir == 'ASC') return 1;
                    else return -1;
                }
                                
                if(this.trainingLevel < other.trainingLevel) {
                    if(sortDir == 'ASC') return -1;
                    else return 1;
                } 
                              
            } else if(sortField == 'NbrOfImplants'){
                            

                 if(this.nbrOfImplants == other.nbrOfImplants) {
                    return 0;
                }
                            
                if(this.nbrOfImplants > other.nbrOfImplants) {
                     if(sortDir == 'ASC') return 1;
                    else return -1;
                }
                                
                if(this.nbrOfImplants < other.nbrOfImplants) {
                    if(sortDir == 'ASC') return -1;
                    else return 1;
                } 
                              
            } else if(sortField == 'DrivingDistance'){
                if(this.drivingDistance == other.drivingDistance) {
                    return 0;
                }
                            
                if(this.drivingDistance > other.drivingDistance) {
                    if(sortDir == 'ASC') return 1;
                    else return -1;
                }
                                
                if(this.drivingDistance < other.drivingDistance) {
                    if(sortDir == 'ASC') return -1;
                    else return 1;
                }       
                        
            } else if(sortField == 'Availability'){
                system.debug('### Availability'); 
                System.debug('this => '+this.isAvailable);
                System.debug('other => '+other.isAvailable);
                
                if(this.isAvailable && !other.isAvailable){
                    if(sortDir == 'ASC') return -1;
                    else return 1;
                }
                
                if(!this.isAvailable && other.isAvailable) {
                    if(sortDir == 'ASC') return 1;
                    else return -1;
                }
                
                //Same availability condition. If both are not available, we order by the overlaping event text
                if(!this.isAvailable && !other.isAvailable) {
                    
                    if(this.overlapEventName > other.overlapEventName) {
                        if(sortDir == 'ASC') return 1;
                        else return -1;
                    }
                                    
                    if(this.overlapEventName < other.overlapEventName) {
                        if(sortDir == 'ASC') return -1;
                        else return 1;
                    }                
                }
                
                // If both are available, we try to order by 'better' availability. For example, if one has an event starting just 20 min after the Implant
                // while the other has more than 1 hour free before and after the implant.
                
                System.debug('this.millisBeforeStart => '+this.millisBeforeStart);
                System.debug('this.millisAfterEnd => '+this.millisAfterEnd);
                System.debug('other.millisBeforeStart => '+other.millisBeforeStart);
                System.debug('other.millisAfterEnd => '+other.millisAfterEnd);
                    
                Integer resultBefore = compareTimeDifference(this.millisBeforeStart, other.millisBeforeStart);                  
                Integer resultAfter = compareTimeDifference(this.millisAfterEnd, other.millisAfterEnd);
                
                Integer result = resultBefore + resultAfter;
                
                System.debug('resultBefore => '+resultBefore);
                System.debug('resultAfter => '+resultAfter);
                
                System.debug('result => '+result);
                
                if(result > 0){
                    if(sortDir == 'ASC') return -1;
                    else return 1;
                }
                
                if(result < 0){
                    if(sortDir == 'ASC') return 1;
                    else return -1;
                }
                
                //Both are in the same level. Lets try to resolve it by the total time difference around the Implant.
                Long myDiff = getTotalDifference(this.millisBeforeStart, this.millisAfterEnd);
                Long otherDiff = getTotalDifference(other.millisBeforeStart, other.millisAfterEnd);
                
                System.debug('myDiff => '+myDiff);
                System.debug('otherDiff => '+otherDiff);
                
                if(myDiff > otherDiff){
                    if(sortDir == 'ASC') return -1;
                    else return 1;
                }
                
                if(myDiff < otherDiff){
                    if(sortDir == 'ASC') return 1;
                    else return -1;
                }               
                                
            }else if(sortField == 'PreviousEvent'){
                system.debug('### PreviousEvent');            
                if(this.previousEventName > other.previousEventName) {
                    if(sortDir == 'ASC') return 1;
                    else return -1;
                }
                                
                if(this.previousEventName < other.previousEventName) {
                    if(sortDir == 'ASC') return -1;
                    else return 1;
                }               
            }else if(sortField == 'NextEvent'){
                system.debug('### NextEvent');            
                if(this.nextEventName > other.nextEventName) {
                    if(sortDir == 'ASC') return 1;
                    else return -1;
                }
                                
                if(this.nextEventName < other.nextEventName) {
                    if(sortDir == 'ASC') return -1;
                    else return 1;
                }               
            }

            //If the values are the same, by default we decide which goes first by the Member Name
            if(this.memberName > other.memberName) return 1;
            if(this.memberName < other.memberName) return -1;
            
            return 0;
        } 
        
        private Integer compareTimeDifference(Long myMillis, Long otherMillis){
            
            //If I am bigger than 1h. and the other is not, super positive
            if((myMillis == null || myMillis >= 3600000) && (otherMillis !=null && otherMillis < 3600000)){
                return 2;
            }
            //If the other is bigger than 1h. and I am not, super negative
            if((myMillis != null && myMillis < 3600000) && (otherMillis == null || otherMillis >= 3600000)){
                return -2;
            }           
            if(myMillis == null && otherMillis != null){
                return 1;
            }
            if(myMillis != null && otherMillis == null){
                return -1;
            }
            if(myMillis == null && otherMillis == null){
                return 0;
            }
            if(myMillis > otherMillis){
                return 1;
            }
            if(myMillis < otherMillis){
                return -1;
            }
            
            return 0;
        }  
        
        private Long getTotalDifference(Long beforeMillis, Long afterMillis){
            
            Long result;
            
            //If null, then 10h.
            if(beforeMillis == null) result = 36000000;
            else result = beforeMillis;
            
            //If null, then 10h.
            if(afterMillis == null) result += 36000000;
            else result += afterMillis;
            
            return result;
        }
    }
    
	private class CaseField{
        
        public String label {get; set;}
        public String sortName {get; set;}      
    }
    */
}