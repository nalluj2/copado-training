/*
 *      Created Date :4-12-2012
 *      Description : This is the class is used to centralize logic around opportunities but methods that need to run without sharing
 *
 *      Author = Rudy De Coninck
 *      
 */
public class bl_OpportunityNoSharing {


	public static List<Opportunity> updateOpportunityWithManagerofOwner(List<Opportunity> opportunities){
		
		List<Id> ownerIds = new List<Id>();
		for(Opportunity opp : opportunities){
			ownerIds.add(opp.ownerId);
		}
		
		List<User> owners = [select Id, name, Manager.email from User where id in :ownerIds]; 

		Map<Id,String> ownerManagerMap = new Map<Id,String>();
		for (User u : owners){
			if (!ownerManagerMap.containsKey(u.id)){
				ownerManagerMap.put(u.id,u.Manager.email);
			}
		}
		
		for(Opportunity opp : opportunities){
			String managerEmail = ownerManagerMap.get(opp.ownerId);
			if(null!=managerEmail){
				opp.Opportunity_Owner_Manager_Email__c = managerEmail;
			}else{
				System.debug('none found '+ownerManagerMap);
			} 
			ownerIds.add(opp.ownerId);
		}
		System.debug('Updates opportunities with manager '+opportunities);
		return opportunities;
	}

}