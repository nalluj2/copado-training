@isTest public class clsTestData_Contract {
	
	public static Integer iRecord_Contract = 1;
    public static Contract oMain_Contract;
    public static List<Contract> lstContract = new List<Contract>();
    public static String tContract_Status = 'Activated';
    public static Id idRecordType_Contract;


    //---------------------------------------------------------------------------------------------------
    // Create Contract Data
    //---------------------------------------------------------------------------------------------------
    public static List<Contract> createContract(){
    	return createContract(true);
    }
    public static List<Contract> createContract(Boolean bInsert){

        if (oMain_Contract == null){

            if (clsTestData_Account.oMain_Account == null) clsTestData_Account.createAccount();

            lstContract = new List<Contract>();

            for (Integer i=0; i<iRecord_Contract; i++){
                Contract oContract = new Contract();
		        	oContract.AccountId = clsTestData_Account.oMain_Account.Id;
		        	oContract.Agreement_level_Picklist__c = 'Test';
		        	oContract.StartDate = Date.Today() - 30;
		        	oContract.ContractTerm = 12;
//                    oContract.Status = tContract_Status;
                    if (idRecordType_Contract != null) oContract.RecordTypeId = idRecordType_Contract;
                lstContract.add(oContract);
            }
            if (bInsert) insert lstContract; 

            oMain_Contract = lstContract[0];           
        }

        return lstContract;
    } 	
    //---------------------------------------------------------------------------------------------------

}