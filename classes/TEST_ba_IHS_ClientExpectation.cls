//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 14-02-2019
//  Description      : APEX Test Class for ba_IHS_ClientExpectation
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
@isTest  private class TEST_ba_IHS_ClientExpectation{

	@isTest static void test_scheduling(){

        Test.startTest();

        //---------------------------------------
        // TEST SCHEDULING
        //---------------------------------------
        string tCRON_EXP = '0 0 0 3 9 ? 2099';
        
        string tJobId = System.schedule('ba_IHS_ClientExpectation_TEST', tCRON_EXP, new ba_IHS_ClientExpectation());

        // Get the information from the CronTrigger API object
        CronTrigger oCronTrigger = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :tJobId];

        // Verify the expressions are the same
        System.assertEquals(tCRON_EXP, oCronTrigger.CronExpression);

        // Verify the job has not run
        System.assertEquals(0, oCronTrigger.TimesTriggered);

        // Verify the next time the job will run
        System.assertEquals('2099-09-03 00:00:00', String.valueOf(oCronTrigger.NextFireTime));
        //---------------------------------------

        Test.stopTest();

	}
	
	
	@isTest private static void test_ba_IHS_ClientExpectation(){

		// Create Test Data
		User oUser_Admin1 = clsTestData_User.createUser_SystemAdministrator('tstadm1', true);


		Opportunity oOpportunity1;
		Opportunity oOpportunity2;
		
		System.runAs(oUser_Admin1){

			Trigger_Deactivation__c oTriggerDeactivation = new Trigger_Deactivation__c();
				oTriggerDeactivation.Name = 'Opp_updateStageCompletion';
				oTriggerDeactivation.Deactivate__c = true;
				oTriggerDeactivation.User_Ids__c = oUser_Admin1.Id;
			insert oTriggerDeactivation;

			clsTestData_Opportunity.tOpportunityCurrencyIsoCode = 'EUR';
			oOpportunity1 = clsTestData_Opportunity.createOpportunity_IHS_Stage('MANAGEDSERVICE', '0', true);
				oOpportunity1.At_Risk__c = true;
			update oOpportunity1;

			oOpportunity2 = oOpportunity1.clone();
				oOpportunity2.At_Risk__c = true;
			insert oOpportunity2;

		}

		List<Client_Expectation__c> lstClientExpectation = new List<Client_Expectation__c>();
		Client_Expectation__c oClientExpectation1 = new Client_Expectation__c();
			oClientExpectation1.Opportunity__c = oOpportunity1.Id;
			oClientExpectation1.Priority__c = 'Yes';
			oClientExpectation1.Progress__c = 'On track';
			oClientExpectation1.Expectation__c = 'TEST Expectation';
			oClientExpectation1.Expectation_Owner__c = UserInfo.getUserName();
			oClientExpectation1.Expectation_Source__c = 'Other';

			oClientExpectation1.Expectation_Source_Date__c = Date.today();
			oClientExpectation1.Expectation_Realization_days__c = '30';
			oClientExpectation1.Expectation_Ready_Date__c = Date.today();
		lstClientExpectation.add(oClientExpectation1);

		Client_Expectation__c oClientExpectation2 = new Client_Expectation__c();
			oClientExpectation2.Opportunity__c = oOpportunity1.Id;
			oClientExpectation2.Priority__c = 'Yes';
			oClientExpectation2.Progress__c = 'On track';
			oClientExpectation2.Expectation__c = 'TEST Expectation';
			oClientExpectation2.Expectation_Owner__c = UserInfo.getUserName();
			oClientExpectation2.Expectation_Source__c = 'Other';

			oClientExpectation2.Expectation_Source_Date__c = Date.today();
			oClientExpectation2.Expectation_Realization_days__c = '30';
		lstClientExpectation.add(oClientExpectation2);

		System.runAs(oUser_Admin1){
			insert lstClientExpectation;
		}

		// Execute Logic
		Test.startTest();

        //---------------------------------------
        // TEST BATCH
        //---------------------------------------
		System.runAs(oUser_Admin1){

			ba_IHS_ClientExpectation oBatch = new ba_IHS_ClientExpectation();
				oBatch.setOpportunityStage = new Set<String>{'0 - Tracking'};
			Database.executeBatch(oBatch, 200);    

		}
        //---------------------------------------

		Test.stopTest();

		// Validate Result		

	}

}