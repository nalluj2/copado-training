/*
 *      Created Date : 20131122
 *      Description : This class exposes methods to check if an iPlan2 exists
 *
 *      Author = Fabrizio Truzzu 
 */

@RestResource(urlMapping='/AccountPlanQueryService/*')
global class ws_AccountPlanQueryService {
	@HttpPost
	global static String doesIplanExist() {
		RestRequest req = RestContext.request;
						
		String body = req.requestBody.toString();
		AccountPerformanceRequest accPerfReq = (AccountPerformanceRequest)System.Json.deserialize(body, AccountPerformanceRequest.class);
												     
		List<string> buIds = ut_SharedMethods.getUserBuIds();
		
		List<Account_Plan_Level__c> accLevels = [select apl.Account_Plan_Level__c
												 from Account_Plan_Level__c apl
												 where apl.Business_Unit__c in : buIds];
		
		boolean canCreateAccountPlan = canUserCreateAccountPlan(accLevels,accPerfReq);
		
		AccountPerformanceResponse resp = new AccountPerformanceResponse();
		if(!canCreateAccountPlan){
			resp.iPlanFound= false;
			resp.message ='Insufficient rights to create an Account Plan.';
			
			return System.JSON.serialize(resp);
		}
		
		iPlanResultWrapper iplanResultWrapper = bl_AccountPlanning.doesIplanExist(accPerfReq.accountPlan);
			
		if(iPlanResultWrapper.isFound){
			resp.accountPlan = iPlanResultWrapper.iPlan;
			resp.message = iplanResultWrapper.errorMessage;
			resp.iPlanFound = iplanResultWrapper.isFound;
			resp.id = iplanResultWrapper.iPlan.Id;
		}else{
			resp.message = iPlanResultWrapper.errorMessage;
			resp.iPlanFound = iplanResultWrapper.isFound;
		}
			System.debug('request '+body);
			System.debug('response '+System.JSON.serialize(resp));
		return System.JSON.serialize(resp);
	}	
	
	private static boolean canUserCreateAccountPlan(List<Account_Plan_Level__c> accLevels,AccountPerformanceRequest accPerfReq){
		boolean canCreateAccountPlan = false;
		for(Account_Plan_Level__c accLevel : accLevels){
			if(accPerfReq.accountPlan.Account_Plan_Level__c==accLevel.Account_Plan_Level__c){
				canCreateAccountPlan = true;
			}
		}
		
		return canCreateAccountPlan;
	}
	
	global class AccountPerformanceRequest{
		public Account_Plan_2__c accountPlan{get;set;}
	}
	
	global class AccountPerformanceResponse{
		public String id{get;set;}
		public boolean iPlanFound{get;set;}
		public String message {get;set;}	
		public Account_Plan_2__c accountPlan{get;set;}
	}
}