/*
 * Description      : Interface class to send/receive Cases from SFDC to Service Notification in SAP and back 
 * Author           : Rudy Coninck
 * Created Date     : 04-10-2015
 * Change Log       : 
 */
global class ws_ServiceNotificationSAPService {

	public static final Integer TRIM_ERROR_MSG_LENGTH = 132;
	public static final String RESULT_ACTION_CREATE = 'CREATE';
	public static final String RESULT_ACTION_UPDATE = 'UPDATE';

	/*
	 * Description      : WS Method to Create / Update Case
	 * Author           : Rudy Coninck
	 * Created Date     : 04-10-2015
	 */
	webservice static SAPServiceNotification upsertServiceNotification(SAPServiceNotification sapServiceNotification)
	{

		SAPServiceNotification response = null;

		System.debug('UPSERT SERVICE NOTIFICATION REQUEST - ' + sapServiceNotification);		
		String resultAction = RESULT_ACTION_UPDATE;
		
		Case cse;
		
		Savepoint sp = Database.setSavepoint();
		
		// Wrap in try catch to construct error message in case of Exception
		try
		{	
			//CR-27432: If the Notification Type is not supported, then we just ignore the request and return a fake success message.
			Set<String> supportedTypes = ws_SharedMethods.getSupportedTypes('ServiceNotification');			
			
			if(supportedTypes.contains(sapServiceNotification.SAP_NOTIFICATION_TYPE) == false){
				
				response = sapServiceNotification;
				response.DISTRIBUTION_STATUS = 'TRUE';
				response.RESULT_ACTION = RESULT_ACTION_CREATE;
				response.DIST_ERROR_DETAILS  = 'Notification Type not supported. Action skipped.';	
				return response;				
			}	
			
			//get the case from the request
			cse = bl_ServiceNotificationSAPService.mapSapServiceNotificationToCase(sapServiceNotification);
			
			//insert or update as necessary based on SAP Id
			List<Database.UpsertResult> upsertResult = Database.upsert(new List<Case>{cse}, Case.fields.SVMX_SAP_Notification_Number__c);
			
			//set the result action based on if we are creating or updating
			if(upsertResult[0].isCreated()) resultAction = RESULT_ACTION_CREATE;
			
			// Service Notification Items
			List<SVMX_Service_Notification_Item__c> notificationItems = bl_ServiceNotificationSAPService.mapSAPServiceNotificationItems(sapServiceNotification.items, cse);
						
			if(notificationItems.size() > 0) upsert notificationItems Unique_Key__c;
			
			if(resultAction == RESULT_ACTION_UPDATE){
				
				delete [Select Id from SVMX_Service_Notification_Item__c where SVMX_Service_Notification__c = :cse.Id AND Id NOT IN :notificationItems];
				bl_ServiceNotificationSAPService.updateServiceOrderSAPDamageCauses(cse.Id, notificationItems);
			}
			
			//get the response from the case
			response = bl_ServiceNotificationSAPService.mapCaseToSAPServiceNotification(cse);
			response.RESULT_ACTION = resultAction;			

		} catch (Exception ex){
			
			Database.rollback(sp);
			
			// Construct error message 
			String errMsg = (ex.getMessage()).left(TRIM_ERROR_MSG_LENGTH);
			// Full stack trace
			String errTrace = ex.getMessage() + ' :Stack Trace: ' +  ex.getStackTraceString();
			//Return original Object including status and err message
			
			response = new SAPServiceNotification();
			response.SFDC_ERR_STACKTRACE = errTrace;
			response.DISTRIBUTION_STATUS = 'FALSE';
			response.DIST_ERROR_DETAILS  = errMsg;			
		}   
		
		String internalId = '';
		if(cse != null) internalId = cse.Id;
		
		ut_Log.logOutboundMessage(internalId, sapServiceNotification.SAP_NOTIFICATION_NUMBER, JSON.serializePretty(sapServiceNotification), JSON.serializePretty(response), response.DISTRIBUTION_STATUS, 'upsertServiceNotification', 'Case');

		// Debug return object        
		System.debug('SERVICE NOTIFICATION RESPONSE - ' + response);

		return response; 
	}

	webservice static SAPServiceNotification getServiceNotificationUpdate(String caseId)
	{
		System.debug('GET SERVICE NOTIFICATION REQUEST - ' + caseId);
		SAPServiceNotification response = null;
		
		try {
			
			//get Case from SAP external Id
			Case cse = bl_ServiceNotificationSAPService.getCase(caseId, false);
	
			//map case object to web service notification object.
			response = bl_ServiceNotificationSAPService.mapCaseToSAPServiceNotification(cse);
					
		} catch (Exception ex)
		{
			
			// Construct error message 
			String errMsg = (ex.getMessage()).left(TRIM_ERROR_MSG_LENGTH);
			// Full stack trace
			String errTrace = ex.getMessage() + ' :Stack Trace: ' +  ex.getStackTraceString();
			//Return original Object including status and err message
			
			response = new SAPServiceNotification();
			response.SFDC_ERR_STACKTRACE = errTrace;
			response.DISTRIBUTION_STATUS = 'FALSE';
			response.DIST_ERROR_DETAILS  = errMsg;
			
		}	
		
		ut_Log.logOutboundMessage(caseId, response.SAP_NOTIFICATION_NUMBER, caseId, JSON.serializePretty(response), response.DISTRIBUTION_STATUS, 'getServiceNotificationUpdate', 'Case');

		// Debug return object        
		System.debug('GET SERVICE NOTIFICATION RESPONSE - ' + response);

		return response;
	}

	webservice static SAPServiceNotificationAck updateServiceNotificationAcknowledgement(SAPServiceNotificationAck sapServiceNotificationAck)
	{

    	SAPServiceNotificationAck response = new SAPServiceNotificationAck();

    	Savepoint sp = Database.setSavepoint();
    	
    	String tNotificationSAPLogStatus = 'FALSE';
	    try{
    	
            clsUtil.debug('updateServiceNotificationAcknowledgement - sapServiceNotificationAck : ' + sapServiceNotificationAck);        

            response.SFDC_ID = sapServiceNotificationAck.SFDC_ID;
            response.SAP_ID = sapServiceNotificationAck.SAP_ID;

	    	if (sapServiceNotificationAck.DISTRIBUTION_STATUS == 'TRUE'){
	    		
	    		// PERFORM SOME ADDITIONAL LOGIC IF NEEDED

                // SUCCESS
				tNotificationSAPLogStatus = 'TRUE';

	    	}else{

                // ERROR OCCURED
				tNotificationSAPLogStatus = 'FALSE';
            }
	    	
	    	response.DISTRIBUTION_STATUS = 'TRUE';

    	}catch (Exception ex){

			tNotificationSAPLogStatus = 'FALSE';

            Database.rollback(sp);
            
            // Construct error message
            String errMsg = (ex.getMessage()).left(TRIM_ERROR_MSG_LENGTH);
            
            //TODO: Update the Sync Notification record with the error occurred. Special case to take into account for the lock mechanism
                 	        	
        	response.DISTRIBUTION_STATUS = 'FALSE';
        	SAPServiceNotificationItemAck msg = new SAPServiceNotificationItemAck();
        	msg.ERROR_MESSAGE = errMsg;
        	response.DIST_ERROR_DETAILS = new List<SAPServiceNotificationItemAck>{msg};     	  

        }

		ut_Log.logOutboundMessage(sapServiceNotificationAck.SFDC_ID, sapServiceNotificationAck.SAP_ID, JSON.serializePretty(sapServiceNotificationAck), JSON.serializePretty(sapServiceNotificationAck), 'TRUE', 'updateServiceNotificationAcknowledgement', 'Case');

        ut_Log.updateNotificationSAPLogStatus(sapServiceNotificationAck.SFDC_ID, sapServiceNotificationAck.DISTRIBUTION_STATUS, JSON.serializePretty(sapServiceNotificationAck), JSON.serializePretty(response));

        return response;
	}

	global class SAPServiceNotification
	{
		webservice String RESULT_ACTION;
		webservice String SAP_IDOC_NUMBER;
		webservice String WM_TRANSACTION_NUMBER;
		webservice String SAP_NOTIFICATION_NUMBER;
		webservice String SAP_NOTIFICATION_TYPE;
		webservice String SUBJECT;
		webservice String DESCRIPTION;
		webservice String MILESTONE_STATUS;
		webservice String STATUS;
		webservice String SAP_WORK_ORDER_REFERENCE;		
		webservice List<SAPServiceNotificationAccount> accounts;
		webservice String SAP_CONTACT;
		webservice String NAME_CONTACT_PERSON;
		webservice String CONTACT_STREET;
		webservice String CONTACT_ZIP_CODE;
		webservice String CONTACT_CITY;
		webservice String CONTACT_COUNTRY;
		webservice String CONTACT_TELEPHONE;
		webservice String REPORTED_BY;
		webservice String NOTIFIED_DATE;
		webservice String NOTIFIED_TIME;
		webservice String CUSTOMER_PO;
		webservice String PO_DATE;
		webservice String SERVICE_CONTRACT_DOC;
		webservice String SERVICE_CONTRACT_ITEM;
		webservice String SAP_EQUIPMENT_NUMBER;
		webservice String SAP_EQUIPMENT_DESCRIPTION;
		webservice String SERIAL_LOT_NUMBER;
		webservice String MATERIAL;
		webservice String MATERIAL_DESC;
		webservice String PRIORITY;
		webservice String EFFECT;
		webservice String PREFERRED_START_DATE;
		webservice String PREFERRED_START_TIME;
		webservice String PREFERRED_END_DATE;
		webservice String PREFERRED_END_TIME;
		webservice String BUSINESS_UNIT;
		webservice String CASE_OWNER;
		webservice List<SAPServiceNotificationItem> items;
		webservice String CODING_GROUP;
		webservice String CODING_CODE;
		webservice String SHORT_TEXT;
		webservice String LONG_TEXT;
		webservice String GCH_NUMBER;
		webservice String REFERENCE_NUMBER;
		webservice String SERVICE_KEY_NOTE;
		webservice String COMPLAINT;
		webservice String PRODUCT_RECEIVED_DATE;
		webservice String PRIOR_REPAIR_DATE;
		webservice String ZR_NOTIFICATION_NUMBER_LINKED_TO_Q3;
		webservice String COUNTRY_KEY;
		webservice String QUESTION_1;
		webservice String QUESTION_2;
		webservice String QUESTION_3;
		webservice String COMMENT;
		webservice String REPORTED_SERIAL_NUMBER;
		webservice String MALFUNCTION_START;
		webservice String START_MALFUNCTION_TIME;
		webservice String MALFUNCTION_END;
		webservice String MALFUNCTION_END_TIME;
		webservice String BREAKDOWN_INDICATOR;
		webservice String BREAKDOWN_DURATION;
		webservice String BREAKDOWN_UNIT;
		webservice String WORK_CENTER;
		webservice String SALES_ORG;
		webservice String SALES_ORG_TEXT;
		webservice String DISTR_CHANNEL;
		webservice String DISTR_CHANNEL_TEXT;
		webservice String DIVISION;
		webservice String DIVISION_TEXT;
		webservice String COMPANY_CODE;
		webservice String COMPANY_CODE_TEXT;
		webservice String COMPANY_CODE_CITY_COUNTRY;
		webservice String SERMAINT_CONTRACT;
		webservice String CONTRACT_START;
		webservice String CONTRACT_END;

		webservice String SFDC_ERR_STACKTRACE; 
		webservice String SAP_ID;
		webservice String CASE_ID;
		webservice String SFDC_ID;//For CGEN3 compatibility
		webservice String NAME;
		webservice String DISTRIBUTION_STATUS;
		webservice String DIST_ERROR_DETAILS;
		webservice DateTime SAP_CREATED_DATE;
		webservice DateTime SAP_LASTMODIFIED_DATE;
		webservice DateTime CREATED_DATE;
		webservice DateTime LASTMODIFIED_DATE;
		webservice String SUBJECT_CODING;
		webservice String SUBJECT_CODE_GROUP;
		webservice String TSU_RECALL_INDICATOR;
		webservice String TSU_RECALL_NUMBER;
		webservice String SEND_COMPLAINT_REPORT;	
		webservice String CURRENT_LOCATION;

        webservice List<SAPServiceNotificationEquipStatus> equipstatus;
        webservice List<SAPServiceNotificationActHeader> activities;
					
	}

	global class SAPServiceNotificationItem{
		webservice String ITEM_NUMBER;
		webservice String CODE_GROUP;
		webservice String PART_OF_OBJECT;
		webservice String OBJECT_PART_TEXT;
		webservice String CODE_GROUP_PROBLEM;
		webservice String PROBLEM_OR_DAMAGE_CODE;
		webservice String PROBLEM_DESC;
		webservice String DAMAGE_TEXT;
		webservice String DAMAGE_LONG_TEXT;
		webservice List<SAPServiceNotificationItemCause> causes;
		webservice List<SAPServiceNotificationActItem> activities;
        webservice List<SAPServiceNotificationChar> characteristics;		
	}
	
	global class SAPServiceNotificationChar {
        webservice String CHAR_NAME;
        webservice String CHAR_VALUE;
        webservice String CHAR_DATE;
    }

    global class SAPServiceNotificationEquipStatus {
        webservice String EQUI_STATUS;
        webservice String EQUI_INACTIVE;
        webservice String EQUI_UPD;
    }

    global class SAPServiceNotificationActHeader {
        webservice String ACTIVITY_HDR_NUMBER;
        webservice String ACTIVITY_HDR_GROUP;
        webservice String ACTIVITY_HDR_CODE;
    }
     
    global class SAPServiceNotificationActItem {
        webservice String ACTIVITY_ITM_NUMBER;
        webservice String ACTIVITY_ITM_GROUP;
        webservice String ACTIVITY_ITM_CODE;
        webservice String ACTIVITY_ITM_DESC;
        webservice String ACTIVITY_ITM_TEXT;
        webservice String ACTIVITY_ITM_LONG_TEXT;
        webservice String ACTIVITY_ITM_CREATED_BY;
        webservice String ACTIVITY_ITM_CREATED_ON;        
    }
	
	
	global class SAPServiceNotificationItemCause {
		webservice String CAUSE_NUMBER;
		webservice String CAUSE_GROUP;
		webservice String CAUSE_CODE;
		webservice String CAUSE_TEXT;
		webservice String CAUSE_LONG_TEXT;
		webservice String CAUSE_SHORT_TEXT;
	}

	global class SAPServiceNotificationAccount{
		webservice String ACCOUNT_NUMBER;
		webservice String ACCOUNT_TYPE;
		webservice String ACCOUNT_NAME;
		webservice String ACCOUNT_STREET;
		webservice String ACCOUNT_ZIP_CODE;
		webservice String ACCOUNT_CITY;
		webservice String ACCOUNT_COUNTRY;
		webservice String ACCOUNT_TELEPHONE;	
	}


	/*
	 * Description      : Structure for ServiceNotification Acknowledgement to update x-ref Ids
	 */     
	global class SAPServiceNotificationAck{
		webservice String MESSAGE;
		webservice String SAP_ID;
		webservice String SFDC_ID; 
		webservice String DISTRIBUTION_STATUS;
		webservice List<SAPServiceNotificationItemAck> DIST_ERROR_DETAILS;
	}

	global class SAPServiceNotificationItemAck{
		webservice String ERROR_MESSAGE;
		webservice String ERROR_NUMBER;
		webservice String ERROR_ID;
		webservice String ERROR_TYPE;
	}

}