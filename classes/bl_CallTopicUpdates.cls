/*      Class Name: bl_CallTopicUpdates
 *      Test Class Name: 
 *      Created Date : 23/04/2013
 *      Description : This class is called by trigger 'tr_UpdateSubjectsOnCallRecord'
                      'tr_CallTopicBU','tr_UpdateProductsOnCallRecord' and used: 
                      1.To update 'Call_Topic_Subjects_Concatendated__c' on Call Topic
                      2.To update 'Call_Topic_Products_Concatendated__c' on Call Topic
                      3.To update 'Call_Topic_Business_Units_Concatendated__c' on Call Topic
 *      Author = Manish Kumar Srivastava
 */
public class bl_CallTopicUpdates{
    public static void UpdateSubjectsConcatenated(List<Call_Topic_Subject__c> lst_triggerCTS){
        map<id,string> mapSubjectidWithName=new map<id,string>();
        map<id,set<id>> mapCTidWithSubjectid=new map<id,set<id>>();
        set<id> setCTIds=new set<id>();
        for(Call_Topic_Subject__c cts:lst_triggerCTS){
            if(cts.Call_Topic__c!=null){
                setCTids.add(cts.Call_Topic__c);  
            } 
        }
        if(setCTids.size()>0){
            List<Call_Topic_Subject__c> lstCTS=[
                                                select id,Call_Topic__c,Subject__c,Subject__r.Name 
                                                from Call_Topic_Subject__c 
                                                where Call_Topic__c in :setCTIds order by Subject__r.Name
                                                ];
            for(Call_Topic_Subject__c cts:lstCTS){
                mapSubjectidWithName.put(cts.Subject__c,cts.Subject__r.Name);
                if(mapCTidWithSubjectid.get(cts.Call_Topic__c)==null){
                    mapCTidWithSubjectid.put(cts.Call_Topic__c,new set<id>());    
                }
                mapCTidWithSubjectid.get(cts.Call_Topic__c).add(cts.Subject__c);
            }
            List<Call_Topics__c> lstCT=new List<Call_Topics__c>();
            string strSubName='';
            for(id ctid:mapCTidWithSubjectid.keyset()){
                Call_Topics__c CT = new Call_Topics__c(id=ctid);
                strSubName='';
                for(id  subid:mapCTidWithSubjectid.get(ctid)){
                    if(strSubName==null || strSubName==''){
                        strSubName=mapSubjectidWithName.get(subid);
                    }else{
                        strSubName=strSubName+';'+mapSubjectidWithName.get(subid);
                    }
                }
                CT.Call_Topic_Subjects_Concatendated__c=strSubName;
                lstCT.add(CT);
            }
            if(lstCT.size()>0){
                update lstCT;  
            }
        }
    }
    public static void UpdateProductsConcatenated(List<Call_Topic_Products__c> lst_triggerCTP){
        map<id,string> mapProductidWithName=new map<id,string>();
        map<id,set<id>> mapCTidWithProductsid=new map<id,set<id>>();
        set<id> setCTIds=new set<id>();
        for(Call_Topic_Products__c ctp:lst_triggerCTP){
            if(ctp.Call_Topic__c!=null){
                setCTids.add(ctp.Call_Topic__c);  
            } 
        }
        if(setCTids.size()>0){
            List<Call_Topic_Products__c> lstCTP=[
                            Select c.Product__r.Name, c.Product__c, c.Id,
                            c.Call_Topic__c From Call_Topic_Products__c c
                            where c.Call_Topic__c in: setCTIds order by c.Product__r.Name
                            ];
            for(Call_Topic_Products__c ctp:lstCTP){
                mapProductidWithName.put(ctp.Product__c,ctp.Product__r.Name);
                if( mapCTidWithProductsid.get(ctp.Call_Topic__c)==null){
                    mapCTidWithProductsid.put(ctp.Call_Topic__c,new set<id>());    
                }
                mapCTidWithProductsid.get(ctp.Call_Topic__c).add(ctp.Product__c);
            }
            List<Call_Topics__c> lstCT=new List<Call_Topics__c>();
            string strPrdName='';
            for(id ctid:mapCTidWithProductsid.keyset()){
                Call_Topics__c CT = new Call_Topics__c(id=ctid);
                strPrdName='';
                for(id  Prdid:mapCTidWithProductsid.get(ctid)){
                    if(strPrdName==null || strPrdName==''){
                        strPrdName=mapProductidWithName.get(Prdid);
                    }else{
                        strPrdName=strPrdName+';'+mapProductidWithName.get(Prdid);
                    }
                }
                CT.Call_Topic_Products_Concatendated__c=strPrdName;
                lstCT.add(CT);
            }
            if(lstCT.size()>0){
                update lstCT;  
            }
        }
    }
    public static void UpdateBUsConcatenated(List<Call_Topic_Business_Unit__c> lst_triggerCTB){
        map<id,string> mapBUidWithName=new map<id,string>();
        map<id,set<id>> mapCTidWithBUsid=new map<id,set<id>>();
        set<id> setCTIds=new set<id>();
        for(Call_Topic_Business_Unit__c ctb:lst_triggerCTB){
            if(ctb.Call_Topic__c!=null){
                setCTids.add(ctb.Call_Topic__c);  
            } 
        }
        if(setCTids.size()>0){
            List<Call_Topic_Business_Unit__c> lstCTB=[
                            Select c.Business_Unit__r.Name, c.Business_Unit__c, c.Id,
                            c.Call_Topic__c From Call_Topic_Business_Unit__c c
                            where c.Call_Topic__c in: setCTIds order by c.Business_Unit__r.Name
                            ];
            for(Call_Topic_Business_Unit__c ctb:lstCTB){
                mapBUidWithName.put(ctb.Business_Unit__c,ctb.Business_Unit__r.Name);
                if( mapCTidWithBUsid.get(ctb.Call_Topic__c)==null){
                    mapCTidWithBUsid.put(ctb.Call_Topic__c,new set<id>());    
                }
                mapCTidWithBUsid.get(ctb.Call_Topic__c).add(ctb.Business_Unit__c);
            }
            List<Call_Topics__c> lstCT=new List<Call_Topics__c>();
            string strBUName='';
            for(id ctid:mapCTidWithBUsid.keyset()){
                strBUName='';
                Call_Topics__c CT = new Call_Topics__c(id=ctid);
                for(id  BUid:mapCTidWithBUsid.get(ctid)){
                    if(strBUName==null || strBUName==''){
                        strBUName=mapBUidWithName.get(BUid);
                    }else{
                        strBUName=strBUName+';'+mapBUidWithName.get(BUid);
                    }
                }
                CT.Call_Topic_Business_Units_Concatendated__c=strBUName;
                lstCT.add(CT);
            }
            if(lstCT.size()>0){
                update lstCT;  
            }
        }
    }
}