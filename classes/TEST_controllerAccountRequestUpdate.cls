/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TEST_controllerAccountRequestUpdate {

    static testMethod void myUnitTest() {
                
        Account acc = new Account();
        acc.Name = 'Test Coverage Account 1 ' ;
        acc.BillingStreet = 'Test';
        acc.BillingCity  = 'Test';
        acc.BillingState   = 'Test';
        acc.BillingPostalCode    = '1234';
        acc.BillingCountry = 'Test';
        acc.Phone = '12345';
        acc.Fax = '12345';
        acc.Website = 'www.abc.com';
        acc.Account_Address_Line_1__c = 'Address1';                    
        acc.Account_Address_Line_2__c = 'Address2';                    
        acc.Account_Address_Line_3__c = 'Address3';                    
        acc.Account_Postal_Code__c = 'Address4';                    
        acc.Account_Province__c = 'Address5';                    
        acc.Account_Country_vs__c = 'NETHERLANDS';                    
        acc.Account_City__c = 'Address7';
        acc.recordtypeid = FinalConstants.sapAccountRecordTypeId;        
        insert acc;       
        
        Test.startTest();
        
        /* Creation of an instance of the controller to be able to call     */  
        ApexPages.StandardController sct = new ApexPages.StandardController(acc); //set up the standardcontroller      
        controllerAccountRequestUpdate  controller = new controllerAccountRequestUpdate(sct);     
       
        System.assertEquals(false, controller.canCloseWindow);
        
        // Missing Reason field so error 
       	controller.send();       	
       	System.assertEquals(false, controller.canCloseWindow);
                        
        controller.accountRequest.Account_Update_Reason__c= 'Request Update' ; 
        
        controller.Send();
        
        controller.send();       	
       	System.assertEquals(true, controller.canCloseWindow);
        
        List<Account_Update_Request__c> updateRequest = [Select Id from Account_Update_Request__c where Account__c = :acc.Id];
        System.assert(updateRequest.size() == 1);
    }
}