/*  
    Test Class Name - Test_Triggers 
    Description  â€“  The written class will cover trgTherapyBeforeDelete,trgProductGroupBeforeDelete 
                    trgGrowthDriverBeforeDelete,trgSBUAccStdBeforeInsert
    Author - Shweta Singh(WIPRO) 
    Created Date  - 09/06/2011 
*/
    
    @isTest
    Private class Test_triggers 
    {    
        static testMethod void myUnitTest() 
        {
            //Insert Company
             DIB_Country__c dibCntry=new DIB_Country__c();
             dibCntry.name='testCountry';
             dibCntry.Country_ISO_Code__c='TS';
             insert dibCntry;
            

             Company__c apCmpny = new Company__c();
             apCmpny.name='TestMedtronic';
             apCmpny.CurrencyIsoCode = 'EUR';
             apCmpny.Current_day_in_Q1__c=56;
             apCmpny.Current_day_in_Q2__c=34; 
             apCmpny.Current_day_in_Q3__c=5; 
             apCmpny.Current_day_in_Q4__c= 0;   
             apCmpny.Current_day_in_year__c =200;
             apCmpny.Days_in_Q1__c=56;  
             apCmpny.Days_in_Q2__c=34;
             apCmpny.Days_in_Q3__c=13;
             apCmpny.Days_in_Q4__c=22;
             apCmpny.Days_in_year__c =250;
             apCmpny.Company_Code_Text__c = 'T35';
             insert apCmpny;
            //Insert Business Unit
            Business_Unit__c bu =  new Business_Unit__c();
            bu.Company__c =apCmpny.id;
            bu.name='BUMedtronic';
            bu.Company__c = apCmpny.Id; 
            insert bu;
            
             //Insert SBU
            Sub_Business_Units__c sbu1 = new Sub_Business_Units__c();
            sbu1.name='SBUMedtronic1';
            sbu1.Business_Unit__c=bu.id;
            sbu1.Account_Plan_Default__c='Revenue';
            sbu1.Account_Flag__c = 'AF_Solutions__c';
            insert sbu1;
            
             //Insert SBU
            Sub_Business_Units__c sbu2 = new Sub_Business_Units__c();
            sbu2.name='SBUMedtronic2';
            sbu2.Business_Unit__c=bu.id;
            sbu2.Account_Plan_Default__c='Revenue';
            sbu2.Account_Flag__c = 'AF_Solutions__c';
            insert sbu2;

             //Insert SBU
            Sub_Business_Units__c sbu3 = new Sub_Business_Units__c();
            sbu3.name='SBUMedtronic3';
            sbu3.Business_Unit__c=bu.id;
            sbu3.Account_Plan_Default__c='Revenue';
            sbu3.Account_Flag__c = 'AF_Solutions__c';
            insert sbu3;

           //Insert Therapy Group
            Therapy_Group__c tg = new Therapy_Group__c();
            tg.Name='Therapy Group';
            tg.Sub_Business_Unit__c = sbu1.Id;
            tg.Company__c = apCmpny.Id;             
            insert tg;
           
            //Insert Therapy
            Therapy__c tp = new Therapy__c();
            tp.Name='Therapy';
            tp.Therapy_Group__c = tg.id;
            tp.Business_Unit__c=bu.id;
            tp.Sub_Business_Unit__c=sbu1.id;
            tp.Therapy_Name_Hidden__c = 'Therapy'; 
            insert tp;
            
            Therapy__c tp1 = new Therapy__c();
            tp1.Name='Therapy1';
            tp1.Therapy_Group__c = tg.id;
            tp1.Business_Unit__c=bu.id;
            tp1.Sub_Business_Unit__c=sbu1.id;
            tp1.Therapy_Name_Hidden__c ='Therapy1'; 
            insert tp1;
            
            //Insert Product Group
            Product_Group__c pg = new Product_Group__c();
            pg.Name = 'Product Group';
            pg.Therapy_ID__c = tp.id;
            insert pg;
            
            Product_Group__c pg1 = new Product_Group__c();
            pg1.Name = 'Product Group 1';
            pg1.Therapy_ID__c = tp1.id;
            insert pg1;

            Product_Group__c pg2 = new Product_Group__c();
            pg2.Name = 'Product Group 2';
            pg2.Therapy_ID__c = tp1.id;
            insert pg2;

            //Insert Account
            Account a = new Account() ;
            a.Name = 'Test Account Name ';  
            a.RecordTypeId = FinalConstants.sapAccountRecordTypeId ;
            a.Account_Country__c='BELGIUM';
            insert a;

            Account a1 = new Account() ;
            a1.Name = 'Test Account Name1';  
            a1.RecordTypeId = FinalConstants.sapAccountRecordTypeId ;
            a1.Account_Country__c='BELGIUM';
            insert a1;

            Account a2 = new Account() ;
            a2.Name = 'Test Account Name2';  
            a2.Account_Country__c='BELGIUM';
            insert a2;

            DIB_Department__c  dibDept = new DIB_Department__c();
            dibDept.Account__c = a.id;
            dibDept.Active__c = true;
            insert dibDept;
                    
            contact  con=  new contact();
            con.firstname='fn';
            con.lastname='ln';
            con.accountid=a.id;
            con.MailingCountry='BELGIUM';
	        con.Contact_Department__c = 'Diabetes Adult';
	        con.Contact_Primary_Specialty__c = 'ENT';
	        con.Affiliation_To_Account__c = 'Employee';
	        con.Contact_Job_Title__c = 'Manager';
	        con.Contact_Gender__c ='Male';
            
            insert con;

            //test trgTherapyBeforeDelete--starts
            delete pg;
            delete tp;
            
            try
            {
                delete tp1;
            }
            catch(exception e)
            {
            
            } 
            //test trgTherapyBeforeDelete--ends


            //test trgProductGroupBeforeDelete --starts
             delete pg1;
            //delete aper;


            
            delete a2;
            
        
            dibDept.is_merged__c = true;
            update dibDept;


        }
     }