@isTest
private with sharing class Test_ctrlExt_AccountDetailsMobile {
	
	private static testmethod void testController(){
		
		generateBUInformation();
		
		Profile saProfile = [Select Id from Profile where Name = 'System Administrator' LIMIT 1];
		
		User testUser = new User();
		testUser.FirstName = 'Unit Test';
		testUser.LastName = 'User';
		testUser.ProfileId = saProfile.Id;
		testUser.CommunityNickname = 'uniTUser';
		testUser.Alias_unique__c = 'uniTUser123456';
		testUser.Username = 'unit.test.user@medtronic.com.unittest'; 
		testUser.Email = 'unit.test.user@medtronic.com';
		testUser.Alias = 'uniTUser';
		testUser.TimeZoneSidKey = 'Europe/Amsterdam'; 
		testUser.LocaleSidKey = 'en_US';
		testUser.EmailEncodingKey =  'ISO-8859-1';
		testUser.LanguageLocaleKey = 'en_US';
		testUser.CostCenter__c = '123456';
		testUser.Company_Code_Text__c = 'TST';		
		insert testUser;
		 
		Account acc = new Account();
    	acc.RecordTypeId = [select Id from RecordType where DeveloperName = 'CAN_Buying_Group' and sObjectType = 'Account' and isActive = true].Id;
    	acc.Name = 'UnitTest Account Name';
    	acc.Type = 'Buying Group';
    	acc.Account_Country_vs__c = 'CANADA';
    	
    	insert acc;
    	    	    	
    	User_Business_Unit__c userSBU = new User_Business_Unit__c();
    	userSBU.Primary__c = true;
    	userSBU.Sub_Business_Unit__c = [Select Id from Sub_Business_Units__c LIMIT 1].Id;
    	userSBU.User__c = testUser.Id;
    	
    	insert userSBU;
		
		System.runAs(testUser){
		
			ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(acc);
			
			ctrlExt_AccountDetailsMobile controller = new ctrlExt_AccountDetailsMobile(sc);
			
			System.assert(controller.availableBUs.size()==2);
			System.assert(controller.selectedView == 'BUMedtronic Account Performance');
		}		
	}
	
	public static void generateBUInformation(){
		
		Company__c cmpny = new Company__c();		
        cmpny.name='Test Region';
        cmpny.CurrencyIsoCode = 'EUR';
        cmpny.Current_day_in_Q1__c=56;
        cmpny.Current_day_in_Q2__c=34; 
        cmpny.Current_day_in_Q3__c=5; 
        cmpny.Current_day_in_Q4__c= 0;   
        cmpny.Current_day_in_year__c =200;
        cmpny.Days_in_Q1__c=56;  
        cmpny.Days_in_Q2__c=34;
        cmpny.Days_in_Q3__c=13;
        cmpny.Days_in_Q4__c=22;
        cmpny.Days_in_year__c =250;
        cmpny.Company_Code_Text__c = 'TST';
		insert cmpny;
		
		Business_Unit__c bu =  new Business_Unit__c();
        bu.Company__c =cmpny.id;
        bu.name='BUMedtronic Account Performance';
        bu.Account_Plan_Activities__c=true;        
        insert bu;
        
        Sub_Business_Units__c sbu = new Sub_Business_Units__c();
        sbu.name='SBUMedtronic1 Account Performance1236';
        sbu.Business_Unit__c=bu.id;
        sbu.Account_Plan_Default__c='Revenue';
        sbu.Account_Flag__c = 'AF_Solutions__c';
		insert sbu;
		
		DIB_Country__c country = new DIB_Country__c();
		country.Company__c = cmpny.Id;
		country.Name = 'Unit Test Country';
		country.Country_ISO_Code__c = 'ZZ';
		country.CurrencyIsoCode = 'EUR';
		insert country;
	}

}