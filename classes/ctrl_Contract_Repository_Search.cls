public with sharing class ctrl_Contract_Repository_Search {
	
	@AuraEnabled
    public static List<ContractRepository> searchContracts(String searchText, String accountId, String contractType, Date startFromDate, Date startToDate, Date endFromDate, Date endToDate, List<String> mpgCodes){
    	
    	String contractFields = 'Id, Name, Contract_nr__c, Account__r.Name, Primary_Contract_Type__c, MPG_Code__c, Additional_Contract_Types__c, Original_Valid_From_Date__c, Original_Valid_To_Date__c'; 
    	
    	List<String> whereClauses = new List<String>();
    	
    	if(accountId != null && accountId != '') whereClauses.add('Account__c = \'' + accountId + '\'');
    	if(contractType != null && contractType != '') whereClauses.add('(Primary_Contract_Type__c = \'' + contractType + '\' OR Additional_Contract_Types__c INCLUDES (\'' + contractType + '\') )');
    	if(startFromDate != null) whereClauses.add('Original_Valid_From_Date__c >= :startFromDate');
    	if(startToDate != null) whereClauses.add('Original_Valid_From_Date__c <= :startToDate');
    	if(endFromDate != null) whereClauses.add('Original_Valid_To_Date__c >= :endFromDate');
    	if(endToDate != null) whereClauses.add('Original_Valid_To_Date__c <= :endToDate');
    	if(mpgCodes != null && mpgCodes.size() > 0) whereClauses.add('MPG_Code__c INCLUDES (\'' + String.join(mpgCodes, ';') + '\')');
    	
    	List<Contract_Repository__c> results;
		    	
    	if(searchText != null && searchText != ''){
    		
    		if(searchText.endsWith('*') == false) searchText += '*';
    		
    		String searchString = 'FIND :searchText IN ALL FIELDS RETURNING Contract_Repository__c (' + contractFields + ( whereClauses.size() > 0 ? ' WHERE ' + String.join(whereClauses, ' AND ') : '')  + ') LIMIT 201';
    		System.debug(searchString);
    		results = Search.query(searchString)[0];
    		
    	}else{
    		
    		String queryString = 'Select ' + contractFields + ' FROM Contract_Repository__c ' + ( whereClauses.size() > 0 ? ' WHERE ' + String.join(whereClauses, ' AND ') : '') + ' ORDER BY Contract_nr__c LIMIT 201';
    		System.debug(queryString);
    		results = Database.query(queryString);    		
    	}
    	
    	List<ContractRepository> contracts = new List<ContractRepository>();
    	
    	for(Contract_Repository__c result : results){
    		
    		ContractRepository contract = new ContractRepository(); 
    		contract.id = '/' + result.Id;
    		contract.contractId = result.Name;    		
    		contract.customerContractId = result.Contract_nr__c;
    		contract.account = result.Account__r.Name;
    		
    		List<String> contractTypes = new List<String>{result.Primary_Contract_Type__c};
    		if(result.Additional_Contract_Types__c != null) contractTypes.addAll(result.Additional_Contract_Types__c.split(';'));    		
    		
    		contract.contractType =  String.join(contractTypes, '\n');    		
    		contract.mpgCodes =  result.MPG_Code__c;    		
    		contract.validFrom = result.Original_Valid_From_Date__c;
    		contract.validTo = result.Original_Valid_To_Date__c;
    		
    		contracts.add(contract);
    	}
    	
    	return contracts;    	
    }
    
    @AuraEnabled
    public static Map<String, List<PicklistValue>> getPicklistValues(){
    	
    	String sessionId = UserInfo.getSessionId();
        Id contractSchedulingRT = Schema.SObjectType.Contract_Repository__c.getRecordTypeInfosByDeveloperName().get('Interfaced_Contracts').getRecordTypeId();
        
        Http http = new Http();
        Map<String, List<PicklistValue>> result = new Map<String, List<PicklistValue>>();
        
        String instanceURL = Support_Portal_URL__c.getInstance().Instance_URL__c;
        
        HttpRequest req = new HttpRequest();    	    	
		req.setEndpoint(instanceURL + '/services/data/v46.0/ui-api/object-info/Contract_Repository__c/picklist-values/' + contractSchedulingRT);
		req.setMethod('GET');
		req.setHeader('Authorization', 'Bearer ' + sessionId);
		req.setTimeout(120000);
					
	    HttpResponse res = http.send(req);
	    
	    PicklistValuesResponse response = (PicklistValuesResponse) JSON.deserialize(res.getBody(), PicklistValuesResponse.class);
               	
		List<PicklistValue> contractTypeValues = new List<PicklistValue>();
		contractTypeValues.add(new PicklistValue('', ''));
		contractTypeValues.addAll(response.picklistFieldValues.get('Primary_Contract_Type__c').values);
	    result.put('Contract_Type', contractTypeValues);
	    
	    List<PicklistValue> mpgCodeValues = response.picklistFieldValues.get('MPG_Code__c').values;
	    result.put('MPG_Code', mpgCodeValues);
    	
    	return result;
    }
    
    public class PicklistValuesResponse{
    	
    	public Map<String, FieldPicklistValues> picklistFieldValues {get; set;}
    }
    
    public class FieldPicklistValues{
    	
    	public List<PicklistValue> values {get; set;}
    }
    
    public class PicklistValue{
    	
    	@AuraEnabled public String label {get; set;}
    	@AuraEnabled public String value {get; set;}
    	
    	public PicklistValue(String inputValue, String inputLabel){
    		
    		label = inputLabel;
    		value = inputValue;
    	}
    }  
    
    public class ContractRepository{
    	
    	@AuraEnabled public String id { get; set; }
    	@AuraEnabled public String contractId { get; set; }
    	@AuraEnabled public String customerContractId { get; set; }
    	@AuraEnabled public String account { get; set; }
    	@AuraEnabled public String contractType { get; set; }
    	@AuraEnabled public String mpgCodes { get; set; }
    	@AuraEnabled public Date validFrom { get; set; }
    	@AuraEnabled public Date validTo { get; set; }
    }    
}