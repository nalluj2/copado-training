/*
  *  Class Name    : CaseLink
  *  VF Page Name  : CaseLink
  *  Description   : This class include the logic to attach cases and wokr order to the case being viewed                
  *  Created Date  : 28/8/2013
  *  Author        : Wipro Tech. 
*/
public class CaseLink{
    public  List<Case> caseList{get;set;}
    public Id currentRecordId;
    public id currentCaseSystemId;
    public id currentCaseParentId;
    public id recordID{get;set;}    
     
    public static Boolean updateFlag = false; 
    public CaseLink(ApexPages.StandardController controller) {        
        currentRecordId=ApexPages.currentPage().getParameters().get('id');      
        if(currentRecordId!=null){          
            Case currentCaseDeatil=[Select ParentId,AssetId from case where id=:currentRecordId];
            if(currentCaseDeatil.ParentId!=null){
                currentCaseParentId=currentCaseDeatil.ParentId;
            }
            if(currentCaseDeatil.AssetId!=null){
                currentCaseSystemId=currentCaseDeatil.AssetId;
            }
        }                   
    }  
    //wrapper class to bind checkbox with Case and work order record 
    public class wrpCaseAndWorkOrder{
        public boolean selected{get;set;}
        public Case objCase{get;set;}
        public workorder__c objWO{get;set;}
        public String caseDesc{get;set;}
        public string caseCloseDate{get;set;}
        public string woCompletedDate{get;set;}
        
        public wrpCaseAndWorkOrder(Case wrpCase,String strdescription,String strCloseDate){
           objCase=wrpCase;
           selected=false;
           caseDesc=strdescription; 
           caseCloseDate=strCloseDate;  
                
        }
        public wrpCaseAndWorkOrder(workorder__c wrpWO,String strWoCompletedDate){
            objWO=wrpWO;
            selected=false;
            woCompletedDate=strWoCompletedDate;
            
        }
    }     
    public List<wrpCaseAndWorkOrder> CaseAndWoList;
    List<wrpCaseAndWorkOrder> selectedReordList; 
    //get the list of cases and work orders related to Case System
    public List<wrpCaseAndWorkOrder> getCasesAndWOs(){          
        CaseAndWoList=new  List<wrpCaseAndWorkOrder>();
        if(currentCaseSystemId!=NULL){
            for(Case c:[SELECT Id,ClosedDate, ParentId, CaseNumber, Status, Subject,  Description, RecordType.Name, AccountId, AssetId, owner.name FROM Case WHERE RecordType.DeveloperName IN('New_Case','Closed_Case') AND LastModifiedDate >= LAST_N_DAYS:90 And AssetId=:currentCaseSystemId and id!=:currentRecordId and id!=:currentCaseParentId and parentid!=:currentRecordId order By CaseNumber ]){
                String desSubstring=null;
                //if descrition size is more than 150 diplay 150 char only
                if(c.Description.length()>150){
                    desSubstring=c.Description.substring(0,149);
                }
                else{
                    desSubstring=c.Description;
                }               
                string caseCloseDate;
                //changing date fromat to "mmm dd, yyyy" by using standard "FORMAT" method of DateTime
                if(c.ClosedDate!=null)
                    caseCloseDate = c.ClosedDate.format('MMM dd, yyyy');            
                CaseAndWoList.add(new wrpCaseAndWorkOrder(c,desSubstring,caseCloseDate));
           }
           for(workorder__c wo:[SELECT id, Date_Completed__c,Name,Case__c,Status__c,RecordType.Name, owner.name, Asset__c FROM workorder__c WHERE LastModifiedDate >= LAST_N_DAYS:90 And (case__c = null OR Case__c!=:currentRecordId) And Asset__c=:currentCaseSystemId  Order By Name]){
                string woCompletedDate;
                //changing date fromat to "mmm dd, yyyy" by using standard "new Instance" method of Date
                if(wo.Date_Completed__c!=null)                  
                    woCompletedDate=datetime.newInstance(wo.Date_Completed__c, time.newInstance(0,0,0,0)).format('MMM dd,yyyy');
                CaseAndWoList.add(new wrpCaseAndWorkOrder(wo,woCompletedDate));
           }          
        } 
       return CaseAndWoList;       
    }        
   
    //attache selected wokr orders and Case to the Case being viewed
    public pagereference AttacheToCase(){
        List<workorder__c> listOfWOtoUpdate=new List<workorder__c>();
        List<Case> listOfCaseToUpdate=new List<Case>();
        selectedReordList=new List<wrpCaseAndWorkOrder>(); 
        boolean showSuccessMessage=false;               
        IF(CaseAndWoList.size()>0){         
            for(wrpCaseAndWorkOrder objCsandWo:CaseAndWoList){
                if(objCsandWo.selected==true && objCsandWo.objCase!=null){
                    objCsandWo.objCase.ParentId=currentRecordId;
                    listOfCaseToUpdate.add(objCsandWo.objCase); 
                    selectedReordList.add(objCsandWo);
                }  
                else if(objCsandWo.selected==true && objCsandWo.objWO!=null) {
                    objCsandWo.objWO.Case__c=currentRecordId;
                    listOfWOtoUpdate.add(objCsandWo.objWO); 
                    selectedReordList.add(objCsandWo);
                }               
            }  
            updateFlag=true;                       
            if(listOfCaseToUpdate.size()>0)         
                update listOfCaseToUpdate;
            if(listOfWOtoUpdate.size()>0)
                update listOfWOtoUpdate; 
        }
        //Show Error Message if no recor is selected
        if(CaseAndWoList.size()>0 && listOfWOtoUpdate.size()==0 && listOfCaseToUpdate.size()==0){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select a Record To Attach');
            ApexPages.addMessage(myMsg);
            return null;             
        } 
        //show Error message if a case recor is not saved
        if(CaseAndWoList.size()==0 && currentRecordId==null){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please create a case record first.');
            ApexPages.addMessage(myMsg); 
            return null; 
        }  
        //Show Message when record is successfully attahced to case
        else if(CaseAndWoList.size()>0 && (listOfWOtoUpdate.size()>0 || listOfCaseToUpdate.size()>0)){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,'Selected Record Attached to Case. Please Refresh the page to see the attached record on the Case.');
            ApexPages.addMessage(myMsg); 
            return null;
        }
        //show error message when there is no enough reocrd to attach
        if(CaseAndWoList.size()==0 && currentRecordId!=null){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,'There are no enough Cases or Workorders to attach');
            ApexPages.addMessage(myMsg); 
            return null;
        }        
        return null;
        
    }    
}