public without sharing class SynchronizationService_MRIScanner implements SynchronizationService_Object{

	//Set with the external Ids of the records already processed in this transaction. Used to avoid generating multiple notifications for the same change
	private Set<String> processedRecords = new Set<String>();
	
	//This method returns the External Ids of the records that have relevant modifications for the target Org. This includes created records or 
	//modified records where synchronized fields have changed. 
	public Set<String> hasChangeForNotification(List<SObject> records, Map<Id, SObject> oldRecords){
		
		Set<String> result = new Set<String>();
				
		//For MRI_Scanner__c we only process after insert and using the SFDC Record Id as external identifier		
		for (SObject rawObject : records){
			
			MRI_Scanner__c record = (MRI_Scanner__c)rawObject;						

			if(processedRecords.contains(record.External_Id__c)) continue;

            //On Insert we always send notification
            if (oldRecords == null){
                
                result.add(record.External_Id__c);
                
            }else{
             	// If Update we only send notification when relevant fields are modified   
                if (bl_SynchronizationService_Utils.hasChanged(record, oldRecords.get(record.Id), syncFields ) == true ){
					result.add(record.External_Id__c);
					processedRecords.add(record.External_Id__c);
                }
            }

		}
		
		return result;
	}
	
	//This method collects and returns the complete information for a record. This includes the records itself as well as any related record that needs
	//to exist in the target Org too. The returned format is JSON serialized string.
	public String generatePayload(String externalId){

		MRIScannerSyncPayload payload = new MRIScannerSyncPayload();
		
		String query = 	'SELECT ' + String.join( syncFields, ',');
		query += ', Account__r.SAP_Id__c, Account__r.AccountNumber, Account__r.RecordType.DeveloperName';
		query += ' FROM MRI_Scanner__c WHERE External_Id__c = :externalId LIMIT 2';
		List<MRI_Scanner__c> lstMRIScanner = Database.query(query);

		//If no record or more than 1 is found, we return an empty response
		if (lstMRIScanner.size() == 1){

			MRI_Scanner__c oMRIScanner = lstMRIScanner[0];	
			bl_SynchronizationService_Utils.prepareForJSON(oMRIScanner, syncFields);		
			oMRIScanner.Id = null;
			payload.mriScanner = oMRIScanner;

            Set<Id> accountIds = new Set<Id>();

            if (oMRIScanner.Account__c != null){
                
                accountIds.add(oMRIScanner.Account__c);               
                
                if ( (oMRIScanner.Account__r.RecordType.DeveloperName == 'SAP_Account') || (oMRIScanner.Account__r.RecordType.DeveloperName == 'Distributor') ){
                	payload.accountId = oMRIScanner.Account__r.SAP_Id__c; 
                }else{
                	payload.accountId = oMRIScanner.Account__r.AccountNumber;
            	}

				oMRIScanner.Account__c = null;
				oMRIScanner.Account__r = null;							
            }

            //Done in a separate query because we cannot clear the value from the CreatedDate field before sending it to the target
            MRI_Scanner__c oMRIScannerDetails = [SELECT CreatedDate, CreatedBy.Name, LastModifiedDate, LastModifiedBy.Name FROM MRI_Scanner__c WHERE External_Id__c = :externalId];
            
            payload.CreatedDate = oMRIScannerDetails.CreatedDate;          
            payload.CreatedBy = oMRIScannerDetails.CreatedBy.Name;                 
            payload.LastModifiedDate = oMRIScannerDetails.LastModifiedDate;            
            payload.LastModifiedBy = oMRIScannerDetails.LastModifiedBy.Name;           

			payload.generateAccountPayload(accountIds, null);

		}

		return JSON.serialize(payload);
	}
	
	//Implementation needed only in Target Org
	public String processPayload(String inputPayload){						
		return null;
	}
	
	// Model classes for the requests and responses. To be used by the JSON serialize/deserialize
	private class MRIScannerSyncPayload extends SynchronizationService_Payload{

		public MRI_Scanner__c mriScanner { get; set; }

		public String accountId { get; set; }

		public Datetime createdDate {get; set;}
		public String createdBy {get; set;}
		public Datetime lastModifiedDate {get; set;}
		public String lastModifiedBy {get; set;}

	}
	
	private List<String> syncFields = new List<String>{

		'External_Id__c'
		,'Name'
		,'Account__c'
		,'MRI_Contact__c'
		,'MRI_IP_Address__c'
		,'MRI_Location__c'
		,'MRI_Make__c'
		,'MRI_Model__c'
		,'MRI_Strength_T__c'
		,'MRI_SW_Version__c'
		,'MRI_Tech__c'
		,'Primary__c'
		,'Connected_Via__c'
		,'Auth_Key_Expiration_Date__c'
		,'File_Sharing_Protocol__c'
		,'MRI_Status__c'
		,'Port_Distance_from_Console__c'
		,'Port_Location__c'
		,'Auth_Key_Start_Date__c'
		,'VL_Console_TYpe__c'
		,'Auth_Key_Warn_Date__c'

	};
}