@isTest
public class TestDeleteInfoClass {
	
	static testMethod void testContacttrigger() {  		
  		Account account = new Account(Name = 'XxXxXxX');
  		insert account;

  		Contact contact = new Contact(LastName = 'XxXxXxX33', FirstName = 'test',
  		AccountId = account.Id,
  		Contact_Department__c = 'Diabetes Adult', 
     	Contact_Primary_Specialty__c = 'ENT',
     	Affiliation_To_Account__c = 'Employee',
     	Primary_Job_Title_vs__c = 'Manager',
     	Contact_Gender__c = 'Male'
  		);
  		insert contact;
  
  		Contact newContact = [SELECT Id FROM Contact WHERE LastName = 'XxXxXxX33'];
  		if (newContact != null) {
   			delete newContact;
   			undelete newContact;
 		}	
	}
	
	static testMethod void testAccounttrigger() {
  		
  		Account account = new Account(Name = 'XxXxXxX');
  		insert account;
  
  		Account newAccount = [SELECT Id FROM Account WHERE Name = 'XxXxXxX'];  		
  		if (newAccount != null) {			
   			newAccount.Name = 'piet';
  			update newAccount;
   
   			delete newAccount;
   			undelete newAccount; 
 		}	
	}
	
	static testMethod void testAccounttriggerMergre() {
  		
  		Account account = new Account(Name = 'XxXxXxX');
  		insert account;
  		
  		Account account2 = new Account(Name = 'XxXxXxX2');
  		insert account2;
  
  		Account newAccount = [SELECT Id FROM Account WHERE Name = 'XxXxXxX'];  		
  		Account newAccount2 = [SELECT Id FROM Account WHERE Name = 'XxXxXxX2'];
  		if (newAccount != null &&  newAccount2 != null) {
   			merge newAccount newAccount2 ;
   			undelete newAccount2;
 		}	
	}
	
	static testMethod void testContacttriggerMergre() {

  		Account account = new Account(Name = 'XxXxXxX');
  		insert account;
  		
  		Contact contact = new Contact(LastName = 'XxXxXxX88', FirstName = 'test',
  		AccountId = account.Id,  		
  		Contact_Department__c = 'Diabetes Adult', 
     	Contact_Primary_Specialty__c = 'ENT',
     	Affiliation_To_Account__c = 'Employee',
     	Primary_Job_Title_vs__c = 'Manager',
     	Contact_Gender__c = 'Male'  		
  		);
  		insert contact;
  		
  		Contact contact2 = new Contact(LastName = 'XxXxXxX2', FirstName = 'test',
  		AccountId = account.Id,  		
  		Contact_Department__c = 'Diabetes Adult', 
     	Contact_Primary_Specialty__c = 'ENT',
     	Affiliation_To_Account__c = 'Employee',
     	Primary_Job_Title_vs__c = 'Manager',
     	Contact_Gender__c = 'Male'  		
  		);
  		insert contact2;
  
  		Contact newContact = [SELECT Id FROM Contact WHERE LastName = 'XxXxXxX88'];
  		Contact newContact2 = [SELECT Id FROM Contact WHERE LastName = 'XxXxXxX2'];  
  		if (newContact != null && newContact2 != null ) {  			
      			merge newContact newContact2;   
      			undelete newContact2;  
 		}	  		  		  		  		
	}
	
}