@isTest
private class Test_bl_Opportunity_Trigger_Tender {
    
    private static testmethod void testUpdatePrevOpportunity(){
    	
    	RecordType tenderRT = [Select Name, Id from RecordType where sObjectType = 'Opportunity' AND DeveloperName = 'Business_Critical_Tender'];
    	
        User oUser = [SELECT Id FROM User WHERE IsActive = true AND ProfileId = :UserInfo.getProfileId() AND UserRoleId = :UserInfo.getUserRoleId() AND Id = :UserInfo.getUserId() LIMIT 1];

    	Account acc = new Account();        
        acc.Name = 'Test Germany';        
        acc.Account_Country_vs__c = 'GERMANY';
        acc.RecordTypeId = [Select Id from RecordType where SObjectType = 'Account' AND DeveloperName = 'SAP_Account'].Id;        
        acc.Account_Active__c = true;
        acc.SAP_ID__c = '1111111111';
        //acc.SAP_Channel__c = '30';
        insert acc;
        
        Opportunity tenderOpp = new Opportunity();
		tenderOpp.RecordTypeId = tenderRT.Id;
		tenderOpp.AccountId = acc.Id;
		tenderOpp.Name = 'Test Tender Opportunity';
        tenderOpp.CloseDate = Date.today().addDays(30);
        tenderOpp.StageName = 'Closed Won';
		tenderOpp.Reason_Won__c = 'We are the best!';
		tenderOpp.Contract_Number__c = '123456789';
		tenderOpp.Current_Extended_To_Date__c = Date.today().addYears(1);
		tenderOpp.Actual_Last_Possible_Valid_To_Date__c = Date.today().addMonths(13);
		tenderOpp.Actual_Result_Announcement_Date__c = Date.today().addMonths(6);
		tenderOpp.Result_According_to_Strategy__c = 'Won';
		tenderOpp.Stand_Still_Period_End_Date__c = Date.today().addMonths(2);
		tenderOpp.Contract_Start_Date__c = Date.today().addYears(1); 
		tenderOpp.Contract_End_Date__c = Date.today().addYears(2);
		tenderOpp.Opportunity_Country_vs__c = 'NETHERLANDS;BELGIUM';				
        tenderOpp.Business_Unit_Group__c = 'CVG; Diabetes';
        tenderOpp.Business_Unit_msp__c = 'Vascular; Diabetes';
        tenderOpp.Sub_Business_Unit__c = 'Coro + PV; Diabetes Core';
		tenderOpp.Tender_Announcement_Date__c = Date.today();
        tenderOpp.IHS_Tender_Number__c = '100';
        tenderOpp.Tender_Submission_Date_Time__c = Datetime.now();
        tenderOpp.Question_Deadline_Date__c = Date.today();
        tenderOpp.Tender_Expiration_Date__c = Date.today();
        tenderOpp.Expected_Contract_Start_Date__c = Date.today();
        tenderOpp.Expected_Contract_End_Date__c = Date.today();
        tenderOpp.Expected_Last_Possible_Valid_To_Date__c = Date.today();
        tenderOpp.Type_of_Tender__c = 'Negotiated Tender';
		insert tenderOpp;
		
		Opportunity_Lot__c oppLot = new Opportunity_Lot__c();
		oppLot.Opportunity__c = tenderOpp.Id;
		oppLot.Status__c = 'Won';
		oppLot.Name = 'Unit Test Lot';
		oppLot.Current_Potential__c = 2000;
		oppLot.Estimated_Revenue__c = 1000;
		oppLot.Award_Type__c = 'Multi Award';
		oppLot.Lot_Owner__c = UserInfo.getUserId();
		oppLot.Price_Weight__c = 25;
		oppLot.Quality_Weight__c = 25;
		oppLot.Other_Weight__c = 50;
		insert oppLot;
		
		List<Opportunity_Account__c> oppAccounts = [Select Id, Account__c from Opportunity_Account__c where Opportunity__c = :tenderOpp.Id];
		
		System.assert(oppAccounts.size() == 1);
		System.assert(oppAccounts[0].Account__c == acc.Id);
		
		Test.startTest();
		
		Opportunity nextOpp = new Opportunity();
		nextOpp.RecordTypeId = tenderRT.Id;
		nextOpp.AccountId = acc.Id;
		nextOpp.Name = 'Test Tender Follow-up Opportunity';
        nextOpp.CloseDate = Date.today();
        nextOpp.StageName = 'Prospecting/Lead';		
		insert nextOpp;
		
		nextOpp.Previous_Opportunity__c = tenderOpp.Id;
		update nextOpp;
    	
    	nextOpp = [Select Current_Contract_Number__c, Next_Tender_Expected_Announcement_Date__c, Last_Poss_Valid_to_Date_of_Last_Contract__c, 
    					Expected_Effective_Date__c, CloseDate, Current_Potential_Last_Tender__c, Estimated_Revenue_Won_Last_Tender__c, Opportunity_Country_vs__c, 
    					Business_Unit_Group__c, Business_Unit_msp__c, Sub_Business_Unit__c from Opportunity where Id = :nextOpp.Id];
    	
    	System.assertEquals(tenderOpp.Contract_Number__c, nextOpp.Current_Contract_Number__c);
    	System.assertEquals(tenderOpp.Current_Extended_To_Date__c - 150, nextOpp.Next_Tender_Expected_Announcement_Date__c);
    	System.assertEquals(tenderOpp.Current_Extended_To_Date__c + 1, nextOpp.Expected_Effective_Date__c);
    	System.assertEquals(tenderOpp.Current_Extended_To_Date__c + 1, nextOpp.CloseDate);
    	System.assertEquals(tenderOpp.Actual_Last_Possible_Valid_To_Date__c + 1, nextOpp.Last_Poss_Valid_to_Date_of_Last_Contract__c);
        Decimal decCurrentPotentialLastTender = nextOpp.Current_Potential_Last_Tender__c;
        System.assertEquals(2000, decCurrentPotentialLastTender.round().intValue());
        Decimal decEstimatedRevenueWonLastTender = nextOpp.Estimated_Revenue_Won_Last_Tender__c;
        System.assertEquals(1000, decEstimatedRevenueWonLastTender.round().intValue());     
        System.assert(nextOpp.Opportunity_Country_vs__c.contains('BELGIUM'));
        System.assert(nextOpp.Opportunity_Country_vs__c.contains('NETHERLANDS'));
        System.assert(nextOpp.Business_Unit_Group__c.contains('CVG'));
        System.assert(nextOpp.Business_Unit_Group__c.contains('Diabetes'));
        System.assert(nextOpp.Business_Unit_msp__c.contains('Vascular'));
        System.assert(nextOpp.Business_Unit_msp__c.contains('Diabetes'));
        System.assert(nextOpp.Sub_Business_Unit__c.contains('Coro + PV'));
        System.assert(nextOpp.Sub_Business_Unit__c.contains('Diabetes Core'));
    	
    	tenderOpp.Contract_Number__c = '987654321';
    	tenderOpp.Current_Extended_To_Date__c = Date.today().addYears(2);
    	tenderOpp.Actual_Last_Possible_Valid_To_Date__c = Date.today().addMonths(15);
        tenderOpp.Opportunity_Country_vs__c = 'NETHERLANDS;SPAIN';     
        tenderOpp.Business_Unit_Group__c = 'Diabetes';
        tenderOpp.Business_Unit_msp__c = 'Diabetes';
        tenderOpp.Sub_Business_Unit__c = 'Diabetes Core';

        tenderOpp.Type = 'National Tender';
        tenderOpp.Type_of_Tender__c = 'Negotiated Tender';
        tenderOpp.Indicator__c = 'Rumoured Extension';
        tenderOpp.Business_Owner__c = oUser.Id;
    	update tenderOpp;
    	
    	nextOpp = 
            [
                SELECT 
                    Current_Contract_Number__c, Next_Tender_Expected_Announcement_Date__c, Last_Poss_Valid_to_Date_of_Last_Contract__c
                    , Expected_Effective_Date__c, CloseDate, Opportunity_Country_vs__c
                    , Business_Unit_Group__c, Business_Unit_msp__c, Sub_Business_Unit__c
                    , Type, Type_of_Tender__c, Indicator__c, Business_Owner__c
                FROM 
                    Opportunity
                WHERE
                    Id = :nextOpp.Id
            ];
    	
    	System.assertEquals(tenderOpp.Contract_Number__c, nextOpp.Current_Contract_Number__c);
    	System.assertEquals(tenderOpp.Current_Extended_To_Date__c - 150, nextOpp.Next_Tender_Expected_Announcement_Date__c);
    	System.assertEquals(tenderOpp.Current_Extended_To_Date__c + 1, nextOpp.Expected_Effective_Date__c);
    	System.assertEquals(tenderOpp.Current_Extended_To_Date__c + 1, nextOpp.CloseDate);
    	System.assertEquals(tenderOpp.Actual_Last_Possible_Valid_To_Date__c + 1, nextOpp.Last_Poss_Valid_to_Date_of_Last_Contract__c);    	    	
    	System.assert(nextOpp.Opportunity_Country_vs__c.contains('SPAIN'));
    	System.assert(nextOpp.Opportunity_Country_vs__c.contains('NETHERLANDS'));
    	System.assertEquals(tenderOpp.Business_Unit_Group__c, nextOpp.Business_Unit_Group__c);
    	System.assertEquals(tenderOpp.Business_Unit_msp__c, nextOpp.Business_Unit_msp__c);
        System.assertEquals(tenderOpp.Sub_Business_Unit__c, nextOpp.Sub_Business_Unit__c);

        System.assertEquals(tenderOpp.Type, nextOpp.Type);
        System.assertEquals(tenderOpp.Type_of_Tender__c, nextOpp.Type_of_Tender__c);
        System.assertEquals(tenderOpp.Indicator__c, nextOpp.Indicator__c);
        System.assertEquals(tenderOpp.Business_Owner__c, nextOpp.Business_Owner__c);
    	
    	oppLot.Current_Potential__c = 3000;
		oppLot.Estimated_Revenue__c = 2000;
		update oppLot;
		
		nextOpp = [Select Current_Potential_Last_Tender__c, Estimated_Revenue_Won_Last_Tender__c from Opportunity where Id = :nextOpp.Id];
    	    	
        decCurrentPotentialLastTender = nextOpp.Current_Potential_Last_Tender__c;
        System.assertEquals(3000, decCurrentPotentialLastTender.round().intValue());
        decEstimatedRevenueWonLastTender = nextOpp.Estimated_Revenue_Won_Last_Tender__c;
        System.assertEquals(2000, decEstimatedRevenueWonLastTender.round().intValue());     
    	    	
    	Opportunity tenderOpp2 = new Opportunity();
		tenderOpp2.RecordTypeId = tenderRT.Id;
		tenderOpp2.AccountId = acc.Id;
		tenderOpp2.Name = 'Test Tender Opportunity 2';
        tenderOpp2.CloseDate = Date.today().addDays(60);
        tenderOpp2.StageName = 'Prospecting/Lead';	
        tenderOpp2.Previous_Opportunity__c = tenderOpp.Id;	
		insert tenderOpp2;
		
		tenderOpp2 = [Select Id, Business_Unit_Group__c, Business_Unit_msp__c, Sub_Business_Unit__c from Opportunity where Id = :tenderOpp2.Id];
		
        System.assert(tenderOpp2.Business_Unit_Group__c == 'Diabetes');
        System.assert(tenderOpp2.Business_Unit_msp__c == 'Diabetes');
        System.assert(tenderOpp2.Sub_Business_Unit__c == 'Diabetes Core');
				
		nextOpp.Previous_Opportunity__c = tenderOpp2.Id;
		update nextOpp;
		
		nextOpp = [Select Current_Contract_Number__c, Next_Tender_Expected_Announcement_Date__c, Last_Poss_Valid_to_Date_of_Last_Contract__c, 
    					Expected_Effective_Date__c, CloseDate, Current_Potential_Last_Tender__c, Estimated_Revenue_Won_Last_Tender__c, Opportunity_Country_vs__c, 
    					Business_Unit_Group__c, Business_Unit_msp__c, Sub_Business_Unit__c from Opportunity where Id = :nextOpp.Id];
    	
    	System.assertEquals(null, nextOpp.Current_Contract_Number__c);
    	System.assertEquals(null, nextOpp.Next_Tender_Expected_Announcement_Date__c);
    	System.assertEquals(null, nextOpp.Expected_Effective_Date__c);
    	System.assertEquals(tenderOpp.Current_Extended_To_Date__c + 1, nextOpp.CloseDate);
    	System.assertEquals(null, nextOpp.Last_Poss_Valid_to_Date_of_Last_Contract__c);
    	System.assertEquals(0, nextOpp.Current_Potential_Last_Tender__c);
    	System.assertEquals(0, nextOpp.Estimated_Revenue_Won_Last_Tender__c);    	
    	System.assertEquals(null, nextOpp.Opportunity_Country_vs__c);
    	System.assertEquals(tenderOpp.Business_Unit_Group__c, nextOpp.Business_Unit_Group__c);
    	System.assertEquals(tenderOpp.Business_Unit_msp__c, nextOpp.Business_Unit_msp__c);
    	System.assertEquals(tenderOpp.Sub_Business_Unit__c, nextOpp.Sub_Business_Unit__c);
    }


    @IsTest private static void testUpdatePrevOpportunity_DealVIDNonTender(){
    	
    	RecordType tenderRT = clsUtil.getRecordTypeByDevName('Opportunity', 'Deal_VID_Non_tender');
    	
        User oUser = [SELECT Id FROM User WHERE IsActive = true AND ProfileId = :UserInfo.getProfileId() AND UserRoleId = :UserInfo.getUserRoleId() AND Id = :UserInfo.getUserId() LIMIT 1];

		clsTestData_Account.createAccount_SAPAccount();        
		clsTestData_Opportunity.iRecord_Opportunity = 4;
		clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'Deal_VID_Non_tender').Id;
		Opportunity oOpportunity = clsTestData_Opportunity.createOpportunity(false)[0];
	        oOpportunity.Name = 'TEST Opportunity';
	        oOpportunity.CloseDate = Date.today().addDays(30);
	        oOpportunity.StageName = 'Prospecting/Lead';
			oOpportunity.Contract_Number__c = '123456789';
			oOpportunity.Current_Extended_To_Date__c = Date.today().addYears(1);
			oOpportunity.Alt_Payment_Terms_2__c = 'M006 - Due 120 Days after End of Month';
			oOpportunity.Contract_End_Date__c = Date.today().addYears(2);
		insert oOpportunity;

		Opportunity oOpportunity2 = clsTestData_Opportunity.createOpportunity(false)[1];
	        oOpportunity2.Name = 'TEST Opportunity 2';
	        oOpportunity2.CloseDate = Date.today().addDays(30);
	        oOpportunity2.StageName = 'Prospecting/Lead';
			oOpportunity2.Contract_Number__c = '123456789';
			oOpportunity2.Current_Extended_To_Date__c = Date.today().addYears(1);
			oOpportunity2.Alt_Payment_Terms_2__c = 'M006 - Due 120 Days after End of Month';
			oOpportunity2.Contract_End_Date__c = null;
		insert oOpportunity2;

		Test.startTest();
		
		Opportunity oOpportunity_Next = clsTestData_Opportunity.createOpportunity(false)[2];
	        oOpportunity_Next.Name = 'TEST Opportunity - Next';
	        oOpportunity_Next.CloseDate = Date.today().addDays(30);
	        oOpportunity_Next.StageName = 'Prospecting/Lead';
			oOpportunity_Next.Contract_Number__c = '123456789';
			oOpportunity_Next.Current_Extended_To_Date__c = Date.today().addYears(1);
			oOpportunity_Next.Alt_Payment_Terms_2__c = 'M006 - Due 120 Days after End of Month';
			oOpportunity_Next.Contract_End_Date__c = Date.today().addYears(2);
		insert oOpportunity_Next;

			oOpportunity_Next.Previous_Opportunity__c = oOpportunity.Id;
		update oOpportunity_Next;

    	oOpportunity_Next = 
			[
				SELECT 
					Next_Tender_Expected_Announcement_Date__c, Expected_Effective_Date__c, CloseDate, Contract_End_Date__c
				FROM 
					Opportunity
				WHERE 
					Id = :oOpportunity_Next.Id
			];
    	
    	System.assertEquals(oOpportunity_Next.Next_Tender_Expected_Announcement_Date__c, oOpportunity.Contract_End_Date__c.addMonths(-6));
    	System.assertEquals(oOpportunity_Next.Expected_Effective_Date__c, oOpportunity.Contract_End_Date__c.addDays(1));
    	System.assertEquals(oOpportunity_Next.CloseDate, oOpportunity.Contract_End_Date__c.addDays(1));



		Opportunity oOpportunity_Next2 = clsTestData_Opportunity.createOpportunity(false)[3];
	        oOpportunity_Next2.Name = 'TEST Opportunity - Next 2';
			oOpportunity_Next2.CloseDate = Date.today().addDays(30);
	        oOpportunity_Next2.StageName = 'Prospecting/Lead';
			oOpportunity_Next2.Contract_Number__c = '123456789';
			oOpportunity_Next2.Current_Extended_To_Date__c = Date.today().addYears(1);
			oOpportunity_Next2.Alt_Payment_Terms_2__c = 'M006 - Due 120 Days after End of Month';
			oOpportunity_Next2.Contract_End_Date__c = null;
		insert oOpportunity_Next2;

			oOpportunity_Next2.Previous_Opportunity__c = oOpportunity2.Id;
		update oOpportunity_Next2;

		oOpportunity_Next2 = 
			[
				SELECT 
					Next_Tender_Expected_Announcement_Date__c, Expected_Effective_Date__c, CloseDate, Contract_End_Date__c
				FROM 
					Opportunity
				WHERE 
					Id = :oOpportunity_Next2.Id
			];
    	
    	System.assertEquals(oOpportunity_Next2.Next_Tender_Expected_Announcement_Date__c, null);
    	System.assertEquals(oOpportunity_Next2.Expected_Effective_Date__c, null);

		Test.stopTest();

    }
	    
    
    private static testmethod void testCopyCountryISOCodes(){
    	
    	RecordType tenderRT = [Select Name, Id from RecordType where sObjectType = 'Opportunity' AND DeveloperName = 'Business_Critical_Tender'];
    	
    	Account acc = new Account();        
        acc.Name = 'Test Germany';        
        acc.Account_Country_vs__c = 'GERMANY';
        acc.RecordTypeId = [Select Id from RecordType where SObjectType = 'Account' AND DeveloperName = 'SAP_Account'].Id;        
        acc.Account_Active__c = true;
        acc.SAP_ID__c = '1111111111';
        //acc.SAP_Channel__c = '30';
        insert acc;
        
        Dib_Country__c country1 = new Dib_Country__c();
        country1.Name = 'Netherlands';
        country1.Country_ISO_code__c = 'NL';
        
        Dib_Country__c country2 = new Dib_Country__c();
        country2.Name = 'Belgium';
        country2.Country_ISO_code__c = 'BE';
        
        insert new List<Dib_Country__c>{country1, country2};
        
        Test.startTest();
        
        Opportunity tenderOpp = new Opportunity();
		tenderOpp.RecordTypeId = tenderRT.Id;
		tenderOpp.AccountId = acc.Id;
		tenderOpp.Name = 'Test Tender Opportunity';
        tenderOpp.CloseDate = Date.today().addDays(30);
        tenderOpp.StageName = 'Prospecting/Lead';
		tenderOpp.Contract_Number__c = '123456789';
		tenderOpp.Current_Extended_To_Date__c = Date.today().addYears(1);
		tenderOpp.Opportunity_Country_vs__c = 'Netherlands;Belgium';
		insert tenderOpp;
		
		tenderOpp = [Select Opportunity_Country_Text__c from Opportunity where Id = :tenderOpp.Id];
		
		System.assert(tenderOpp.Opportunity_Country_Text__c.contains('BE'));
		System.assert(tenderOpp.Opportunity_Country_Text__c.contains('NL'));
    }
    
    private static testmethod void testDeepCloneTender(){
    	
    	RecordType tenderRT = [Select Name, Id from RecordType where sObjectType = 'Opportunity' AND DeveloperName = 'Business_Critical_Tender'];
    	
    	Account acc = new Account();        
        acc.Name = 'Test Germany';        
        acc.Account_Country_vs__c = 'GERMANY';
        acc.RecordTypeId = [Select Id from RecordType where SObjectType = 'Account' AND DeveloperName = 'SAP_Account'].Id;        
        acc.Account_Active__c = true;
        acc.SAP_ID__c = '1111111111';
        //acc.SAP_Channel__c = '30';
        insert acc;
        
        Contact cnt = new Contact();
        cnt.LastName = 'Test_triggerAffiliationBeforeUpsert cont 1 ';
        cnt.FirstName = 'Test';
        cnt.AccountId = acc.Id ; 
        cnt.Contact_Department__c = 'Diabetes Adult'; 
        cnt.Contact_Primary_Specialty__c = 'ENT';
        cnt.Affiliation_To_Account__c = 'Employee';
        cnt.Primary_Job_Title_vs__c = 'Manager';
        cnt.Contact_Gender__c = 'Male';
        insert cnt;
        
        Company__c cmpny = new Company__c();		
        cmpny.name='Europe';
        cmpny.CurrencyIsoCode = 'EUR';
        cmpny.Current_day_in_Q1__c=56;
        cmpny.Current_day_in_Q2__c=34; 
        cmpny.Current_day_in_Q3__c=5; 
        cmpny.Current_day_in_Q4__c= 0;   
        cmpny.Current_day_in_year__c =200;
        cmpny.Days_in_Q1__c=56;  
        cmpny.Days_in_Q2__c=34;
        cmpny.Days_in_Q3__c=13;
        cmpny.Days_in_Q4__c=22;
        cmpny.Days_in_year__c =250;
        cmpny.Company_Code_Text__c = 'T35';
		insert cmpny;
		
		Business_Unit_Group__c bug =  new Business_Unit_Group__c();
        bug.Master_Data__c =cmpny.id;
        bug.name='RTG';      
        insert bug;
		
		Business_Unit__c bu =  new Business_Unit__c();
        bu.Company__c =cmpny.id;
        bu.name='RTG BU';
        bu.Business_Unit_Group__c = bug.Id;
        bu.Account_Plan_Activities__c=true;        
        insert bu;
        
        Sub_Business_Units__c sbu = new Sub_Business_Units__c();
        sbu.name='RTG sBU';
        sbu.Business_Unit__c=bu.id;
		insert sbu;
                    	
    	User currentUser = new User(Id = UserInfo.getUserId());
		currentUser.Company_Code_text__c = 'T35';
		update currentUser;
		
		Test.startTest();
		
		Opportunity tenderOpp = new Opportunity();
		tenderOpp.RecordTypeId = tenderRT.Id;
		tenderOpp.AccountId = acc.Id;
		tenderOpp.Name = 'Test Tender Opportunity';
        tenderOpp.CloseDate = Date.today().addDays(30);
        tenderOpp.StageName = 'Prospecting/Lead';
		tenderOpp.Contract_Number__c = '123456789';
		tenderOpp.Current_Extended_To_Date__c = Date.today().addYears(1);
		insert tenderOpp;
		
		Opportunity_Lot__c oppLot = new Opportunity_Lot__c();
		oppLot.Opportunity__c = tenderOpp.Id;
		oppLot.Name = 'Unit Test Lot';
		oppLot.Sub_Business_Unit__c = sbu.Id;
		oppLot.Award_Type__c = 'Multi Award';
		oppLot.Lot_Owner__c = UserInfo.getUserId();
		oppLot.Price_Weight__c = 25;
		oppLot.Quality_Weight__c = 25;
		oppLot.Other_Weight__c = 50;
		insert oppLot;
		
		OpportunityTeamMember oppTeamMember = new OpportunityTeamMember();
		oppTeamMember.UserId = UserInfo.getUserId();
		oppTeamMember.TeamMemberRole = 'Account Manager';		
		oppTeamMember.OpportunityId = tenderOpp.Id;
		insert oppTeamMember;
		
		Stakeholder_Mapping__c stakeholder = new Stakeholder_Mapping__c();
		stakeholder.Opportunity__c = tenderOpp.Id;
		stakeholder.Contact__c = cnt.Id;
		insert stakeholder;
		
		Note oppNote = new Note();
		oppNote.ParentId = tenderOpp.Id;
		oppNote.Body = 'Unit Test Note';
		oppNote.Title = 'Unit Test Note';
		insert oppNote;
		
		Attachment oppAttachment = new Attachment();
		oppAttachment.ParentId = tenderOpp.Id;
		oppAttachment.Name = 'Unit Test Attachment';
		oppAttachment.Description = 'Unit Test Attachment';
		oppAttachment.Body = Blob.valueOf('Unit Test Attachment');
		insert oppAttachment;
		
		Opportunity newOpp = new Opportunity();
		newOpp.RecordTypeId = tenderRT.Id;
		newOpp.AccountId = acc.Id;
		newOpp.Name = 'Test Tender Opportunity';
        newOpp.CloseDate = Date.today().addDays(30);
        newOpp.StageName = 'Prospecting/Lead';
		newOpp.Contract_Number__c = '123456789';
		newOpp.Current_Extended_To_Date__c = Date.today().addYears(1);
		newOpp.Cloned_From__c = tenderOpp.Id;
		insert newOpp;
		
		newOpp = [Select Id, (Select Id from Opportunity_Lots__r), (Select Id from OpportunityTeamMembers), 
    		(Select Id from Opportunity_Accounts__r), (Select Id from Stakeholder_Mapping__r), 
    		(Select Body, Title, IsPrivate from Notes), 
    		(Select Id from Attachments) 
    		from Opportunity where Id = :newOpp.Id];
    		
    	System.assert(newOpp.Opportunity_Lots__r.size() == 1);
    	System.assert(newOpp.OpportunityTeamMembers.size() == 1);
    	System.assert(newOpp.Opportunity_Accounts__r.size() == 1);
    	System.assert(newOpp.Stakeholder_Mapping__r.size() == 1);
    	System.assert(newOpp.Notes.size() == 1);
    	System.assert(newOpp.Attachments.size() == 1);
    }

    private static testmethod void copyExpectedEffectiveDate_To_CloseDate(){
    	
    	Id idRecordType_Tender = clsUtil.getRecordTypeByDevName('Opportunity', 'Business_Critical_Tender').Id;
    	
    	Account oAccount = new Account();        
			oAccount.Name = 'Test Germany';        
			oAccount.Account_Country_vs__c = 'GERMANY';
			oAccount.RecordTypeId = clsUtil.getRecordTypeByDevName('Account', 'SAP_Account').Id;        
			oAccount.Account_Active__c = true;
			oAccount.SAP_ID__c = '1111111111';
			//oAccount.SAP_Channel__c = '30';
		insert oAccount;
        
        Dib_Country__c oDIBCountry1 = new Dib_Country__c();
			oDIBCountry1.Name = 'Netherlands';
			oDIBCountry1.Country_ISO_code__c = 'NL';
        Dib_Country__c oDIBCountry2 = new Dib_Country__c();
			oDIBCountry2.Name = 'Belgium';
			oDIBCountry2.Country_ISO_code__c = 'BE';
        insert new List<Dib_Country__c>{oDIBCountry1, oDIBCountry2};
        
        Test.startTest();
        
        Opportunity oOpportunityTender = new Opportunity();
			oOpportunityTender.RecordTypeId = idRecordType_Tender;
			oOpportunityTender.AccountId = oAccount.Id;
			oOpportunityTender.Name = 'Test Tender Opportunity';
			oOpportunityTender.CloseDate = Date.today().addDays(30);
			oOpportunityTender.StageName = 'Prospecting/Lead';
			oOpportunityTender.Contract_Number__c = '123456789';
			oOpportunityTender.Current_Extended_To_Date__c = Date.today().addYears(1);
			oOpportunityTender.Opportunity_Country_vs__c = 'Netherlands;Belgium';
			oOpportunityTender.Expected_Effective_Date__c = Date.today().addDays(15);
		insert oOpportunityTender;
		
		oOpportunityTender = [SELECT Id, Expected_Effective_Date__c, CloseDate FROM Opportunity WHERE Id = :oOpportunityTender.Id];
		System.assertEquals(oOpportunityTender.CloseDate, oOpportunityTender.Expected_Effective_Date__c, Date.today().addDays(15));


			oOpportunityTender.Expected_Effective_Date__c = Date.today().addDays(30);
		update oOpportunityTender;

		oOpportunityTender = [SELECT Id, Expected_Effective_Date__c, CloseDate FROM Opportunity WHERE Id = :oOpportunityTender.Id];
		System.assertEquals(oOpportunityTender.CloseDate, oOpportunityTender.Expected_Effective_Date__c, Date.today().addDays(30));


			oOpportunityTender.Expected_Effective_Date__c = null;
		update oOpportunityTender;

				oOpportunityTender = [SELECT Id, Expected_Effective_Date__c, CloseDate FROM Opportunity WHERE Id = :oOpportunityTender.Id];
		System.assertEquals(oOpportunityTender.CloseDate, Date.today().addDays(30));
		System.assertEquals(oOpportunityTender.Expected_Effective_Date__c, null);

        Test.stopTest();

    }

}