@RestResource(urlMapping='/CalendarEventService/*')
global with sharing class ws_EventService {
    
    @HttpPost
	global static String doPost() {
 		
 		RestRequest req = RestContext.request;
			
		System.debug('post called '+req);
		System.debug('post requestBody '+req.requestBody);
			
		String body = req.requestBody.toString();
		IFEventRequest eventRequest = (IFEventRequest)System.Json.deserialize(body, IFEventRequest.class);
			
		System.debug('post requestBody '+eventRequest);
			
		Event event = eventRequest.event;

		IFEventResponse resp = new IFEventResponse();

		//We catch all exception to assure that 
		try{
				
			resp.event = bl_Activity.saveEvent(event);
			resp.id = eventRequest.id;
	
			resp.success = true;
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
		}
 
		bl_Mobilesync.storeRequest(UserInfo.getUserId(), 'EventUpsert', req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
  			
		return System.JSON.serialize(resp);		
	}

	@HttpDelete
	global static String doDelete(){
		
		RestRequest req = RestContext.request;
			
		System.debug('post called '+req);
		System.debug('post requestBody '+req.requestBody);
			
		String body = req.requestBody.toString();
		IFEventDeleteRequest eventDeleteRequest = (IFEventDeleteRequest)System.Json.deserialize(body, IFEventDeleteRequest.class);
			
		System.debug('post requestBody '+eventDeleteRequest);
			
		Event event = eventDeleteRequest.event;
		
		IFEventDeleteResponse resp = new IFEventDeleteResponse();
		
		List<Event> eventsToDelete = [select Id, Mobile_Id__c from Event where Mobile_ID__c = :event.Mobile_ID__c];
		
		try{
			
			if (eventsToDelete !=null && eventsToDelete.size()>0 ){	
				
				Event eventToDelete = eventsToDelete.get(0);
				delete eventToDelete;
				resp.event = eventToDelete;
				resp.id = eventDeleteRequest.id;
				resp.success = true;
				
			}else{
				
				resp.event = event;
				resp.id = eventDeleteRequest.id;
				resp.message='event not found';
				resp.success = true;
			}
			
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
		}
			
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'EventDelete',req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
			
		return System.JSON.serialize(resp);
	}


	global class IFEventRequest{
		
		public Event event {get;set;}
		public String id{get;set;}
	}
		
	global class IFEventResponse{
		
		public Event event {get;set;}
		public String id{get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}
	
	global class IFEventDeleteRequest{
		
		public Event event {get;set;}
		public String id{get;set;}
	}
		
	global class IFEventDeleteResponse{
		
		public Event event {get;set;}
		public String id{get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}
}