public class ba_AccountSoldToLocationUpdate implements Database.Batchable<SObject>
{
	public Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator(
            [
            	SELECT	Id,Name,
                		SAP_Account_Name__c,
                		SAP_Account_Type__c,
                		SAP_Id__c,
                		SAP_Customer_Group__c,
                		Account_Country_vs__c,
                		Account_City__c,
                		Account_Province__c,
                		Account_Address_Line_1__c,
                		Account_Postal_Code__c
                FROM	Account
                WHERE	SAP_Account_Type__c = 'Sold-to party'    
            ]
        );
    }
    
    public void execute(Database.BatchableContext bc, List<SObject> scope)
    {
        bl_Account.createSMAXLocations((List<Account>)scope);
    }
    
    public void finish(Database.BatchableContext bc)
    {
        
    }
}