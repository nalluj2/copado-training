@isTest
public class testComment_Logic{
   
   	static testmethod void  test(){

        clsTestData_MasterData.tCompanyCode = 'EUR';
        clsTestData_MasterData.tBusinessUnitName = 'Cranial Spinal';
        clsTestData_MasterData.createBusinessUnit(true);
   
   		Account acc1=new Account();
            acc1.SAP_ID__c = '00112233';
            acc1.AccountNumber='as';
            acc1.Name='MNav House Account';
            acc1.Account_Country_vs__c='USA';
            //acc1.FSE__c = u1.id;
            //acc1.Cass_Rel__c = u1.id;
            acc1.CurrencyIsoCode='USD';
            acc1.Phone='24107954';
        insert acc1; 
        
        Contact objContact=new Contact();
            objContact.LastName='Saha';
            objContact.FirstName = 'TEST';
            objContact.AccountId = acc1.id;
            objContact.Contact_Department__c = 'Pediatric';
            objContact.Contact_Primary_Specialty__c = 'Neurology';
            objContact.Affiliation_To_Account__c = 'Employee';
            objContact.Primary_Job_Title_vs__c = 'Nurse';
            objContact.Contact_Gender__c = 'Female';
        insert objContact;
   
        Asset sys=new Asset();
            sys.Name='Niha';
            sys.Serial_Nr__c='XXXXX';
            sys.recordtypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('O-Arm').getRecordTypeId(); 
            sys.Asset_Product_Type__c='O-Arm 1000';
            sys.AccountId=acc1.Id;
            sys.Ownership_Status__c='EOL';	
		insert sys;
                
        Product2 pro=new Product2();
            pro.RecordTypeId = clsUtil.getRecordTypeByDevName('Product2', 'SAP_Product').Id;
            pro.Name='abc';
            pro.Tracking__c = TRUE ;
            pro.Maintenance_Frequency__c= 12;
            pro.Business_Unit_ID__c = clsTestData_MasterData.oMain_BusinessUnit.Id;
        insert pro;
        
        //Exlude execution of OMA triggers
    	clsUtil.bDoNotExecute = true;
        
        Case objcase=new Case();
            objcase.RecordTypeId = clsUtil.getRecordTypeByDevName('Case', 'New_Case').Id;     
        insert objcase;
        
        bl_CalculateBusinessHoursAges.blnInsertCheck = true;
        
        Test.startTest();
        
        Complaint__c objcomplaint1=new Complaint__c();
            objcomplaint1.Status__c='Open';
            objcomplaint1.Account_Name__c=acc1.id;
            objcomplaint1.Asset__c=sys.id;
            //objcomplaint.OwnerId = grp.Id;                  
            objcomplaint1.Medtronic_Aware_Date__c = system.today();
            objcomplaint1.Formal_Investigation_Required__c='No';
            objcomplaint1.Formal_Investigation_Justification__c='abc';
            objcomplaint1.Methods__c='abc';
            objcomplaint1.Results__c='abcc';
            objcomplaint1.Case__c = objcase.id;
        insert objcomplaint1;
        
        Case objcase1=new Case();
            objcase1.Subject='abc';
            objcase1.Description='abc';
            objcase1.Date_Complaint_Became_Known__c=system.today();
            objcase1.ContactId=objContact.id;
            objcase1.AccountId=acc1.id;
            objcase1.Event_Happen_During_Surgery__c= 'No';
            objcase1.Complaint_Case__c = 'Yes';
            objcase1.Complaint__c=objcomplaint1.Id;        
            objcase1.Case_Aborted__c= 'No';
            objcase1.Was_Medtronic_Imaging_Aborted__c= 'No';
            objcase1.Was_Navigation_Aborted__c= 'No';
            objcase1.Serious_Injury__c= 'No';
            objcase1.RecordTypeId = clsUtil.getRecordTypeByDevName('Case', 'New_Case').Id;     
        insert objcase1;
              
       	List<Workorder_Sparepart__c> objCasePartList = new List<Workorder_Sparepart__c>();
       	Workorder_Sparepart__c objCPart= new Workorder_Sparepart__c();
            objCPart.recordtypeId = Schema.SObjectType.Workorder_Sparepart__c.getRecordTypeInfosByName().get('Hardware').getRecordTypeId();
            objCPart.Owner__c = objContact.id;
            objCPart.Order_Part__c = pro.id;
            objCPart.Order_Status__c = 'Pending';
            objCPart.RI_Part__c = pro.id;
            objCPart.Return_Item_Status__c = 'Pending Return';
            objCPart.case__c = objcase1.id; 
            objCPart.Able_to_Duplicate_the_Issue__c = 'Yes';
            objCPart.Failure_Mode__c = 'Accuracy';
            objCPart.BAI_OOBF__c = true;
            objCPart.Findings_and_Conclusions__c = 'abc';
            objCPart.Root_Cause__c = '  3D Model';
            objCPart.Sub_Root_Cause__c = '3D Model'; 
            objCPart.Root_Cause_Other__c = 'abc'; 
            objCPart.Evaluation_Notes__c = 'TestDescription';
            objCPart.Lot_Number__c = '111111111';
            objCPart.Complaint__c=objcomplaint1.id;
            objCasePartList.add(objCPart);
        insert objCasePartList;
  
		objCPart.Complaint__c = null;        
		Database.SaveResult CP =  Database.update(objCPart);
       
        List<Case_Comment__c> objCommentList = new List<Case_Comment__c>();
        Case_Comment__c objCaseComment=new Case_Comment__c();
            objCaseComment.Comment_Subject__c='Test';
            objCaseComment.Comment_Subject__c='Test123';
            objCaseComment.Case_Part__c = objCPart.id;
            objCaseComment.Complaint__c = objcomplaint1.id;
            //objCaseComment.Work_Order__c =WO.id;
            objCaseComment.Case__c = objcase1.id;
            objCommentList.add(objCaseComment);
        insert objCommentList;
   
        //Comment_Logic.CreateCommentFromCP(objCasePartList); Automatically executed when added the Case Part to the Complaint
        Comment_Logic.UpdateComplaintNumber(objCommentList);
        
        Comment_Logic.firstRun = true;
        
        objCPart.BAI_OOBF__c = false;
        objCPart.Lot_Number__c = '123456789';
        objCPart.Findings_and_Conclusions__c = 'abcd';
        update objCPart;
     }  
}