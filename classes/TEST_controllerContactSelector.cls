/**
 * Creation Date :  20090219
 * Description :    Test coverage for controller controllerContactSelector
 * Author :         ABSI - BC
 */

@isTest
private class TEST_controllerContactSelector {

    private static Map<String, String> dataMap = new Map<String, String>();

    private static String tBusinessUnitName = '';

    private static User oUser_Admin;
    private static User oUser;

    private static void createTestData(){

        oUser_Admin = clsTestData_User.createUser_SystemAdministratorMDT('tstadm1', true);
        oUser = clsTestData_User.createUser('tstusr1', true);

        System.runAs(oUser_Admin){

            Id id_RecordType_Implant = clsUtil.getRecordTypeByDevName('Implant__c', new List<String>(bl_Implant.SET_AFSRECORDTYPE)[0]).Id;
            Id id_RecordType_Account = clsUtil.getRecordTypeByDevName('Account', 'Competitor').Id;

            // Create test sObjects 
            List<Account> testAccounts = new List<Account>();
            Account acc1 = new Account();
                acc1.Name = 'TEST_triggerImplantBeforeInsert TestAccount1' ;
                acc1.recordtypeid = id_RecordType_Account; 
            testAccounts.add(acc1);

            Account acc2 = new Account();
                acc2.Name = 'TEST_triggerImplantBeforeInsert TestAccount2' ; 
            testAccounts.add(acc2);
            insert testAccounts;

            
            List<Contact> testContacts = new List<Contact>();
            Contact cont1 = new Contact();
                cont1.LastName = 'TEST_triggerImplantBeforeInsert TestCont1' ;  
                cont1.FirstName = 'Test Contact 1';  
                cont1.AccountId = acc1.Id ;
                cont1.Contact_Active__c = true  ;
                cont1.Integrate_with_MMX__c = true;
                cont1.Contact_Department__c = 'Diabetes Adult'; 
                cont1.Contact_Primary_Specialty__c = 'ENT';
                cont1.Affiliation_To_Account__c = 'Employee';
                cont1.Primary_Job_Title_vs__c = 'Manager';
                cont1.Contact_Gender__c = 'Male'; 
            testContacts.add(cont1);
            
            Contact cont2 = new Contact();
                cont2.LastName = 'TEST_triggerImplantBeforeInsert TestCont2';  
                cont2.FirstName = 'Test Contact 2';  
                cont2.AccountId = acc1.Id ;
                cont2.Contact_Active__c = true;
                cont2.Integrate_with_MMX__c = true;
                cont2.Contact_Department__c = 'Diabetes Adult'; 
                cont2.Contact_Primary_Specialty__c = 'ENT';
                cont2.Affiliation_To_Account__c = 'Employee';
                cont2.Primary_Job_Title_vs__c = 'Manager';
                cont2.Contact_Gender__c = 'Male';         
            testContacts.add(cont2);
            insert testContacts;
            
                    
            Affiliation__c aff = new Affiliation__c();
                aff.Affiliation_To_Contact__c = testContacts.get(0).Id;
                aff.Affiliation_To_Account__c = testAccounts.get(0).Id;
                aff.Affiliation_From_Contact__c = testContacts.get(1).Id;
                aff.Affiliation_From_Account__c = testAccounts.get(1).Id;
                aff.Affiliation_Type__c = 'Referring';
                aff.RecordTypeId = FinalConstants.recordTypeIdC2A;
            insert aff;
            
            Affiliation_Details__c affDetails = new Affiliation_Details__c();
                affDetails.Affiliation_Details_Affiliation__c = aff.Id;
                affDetails.Affiliation_Details_Start_Date__c = Date.newInstance(Date.today().year(), 1, 1);
                affDetails.Affiliation_Details_End_Date__c = Date.newInstance(Date.today().year(), 12, 31);
            insert affDetails;
            string DATE_OF_SURGERY='';         
            dataMap.put('contactToId',testContacts.get(0).Id);
            dataMap.put('contactFromId',testContacts.get(1).Id);
            dataMap.put('accountToId',testAccounts.get(0).Id);
            dataMap.put('accountFromId',testAccounts.get(1).Id);
            dataMap.put('retURL', '/');
            dataMap.put('action', 'Implant');
            dataMap.put(DATE_OF_SURGERY,'2011-08-04 14:55');

            // Create Master Data
            clsTestData_MasterData.tCompanyCode = 'EUR';
            clsTestData_MasterData.createCompany();
            clsTestData_MasterData.tBusinessUnitGroupName = 'Restorative';
            clsTestData_MasterData.createBusinessUnitGroup();
            if (!String.isBlank(tBusinessUnitName)){
                clsTestData_MasterData.tBusinessUnitName = tBusinessUnitName;
            }
            clsTestData_MasterData.createBusinessUnit();
            clsTestData_MasterData.createSubBusinessUnit();

            // Create Therapy
            clsTestData_Therapy.createTherapyGroup();
            clsTestData_Therapy.createTherapy(false);
            for (Therapy__c oTherapy : clsTestData_Therapy.lstTherapy){
                oTherapy.Single_Etiology__c = true;
            }
            insert clsTestData_Therapy.lstTherapy;

            // Create Product
            clsTestData_Product.createProductGroup(true);
            clsTestData_Product.idRecordType_Product = clsUtil.getRecordTypeByDevName('Product2', 'MDT_Marketing_Product').Id;
            clsTestData_Product.iRecord_Product = 3;
            clsTestData_Product.createProductData_NoBU(false);

            Integer iCounter = 0;
            for (Product2 oProduct : clsTestData_Product.lstProduct){
                oProduct.Consumable_Bool__c = true;
                oProduct.IsActive = true;
                oProduct.RecordTypeId = clsTestData_Product.idRecordType_Product;
                oProduct.Business_Unit_ID__c = clsTestData_MasterData.oMain_BusinessUnit.Id;
                oProduct.Product_Group__c = clsTestData_Product.oMain_ProductGroup.Id;
                if (iCounter != 1){
                    oProduct.Manufacturer_Account_ID__c = acc1.id;
                }
                iCounter++;
            }
            insert clsTestData_Product.lstProduct;

            // Create Implant
            Implant__c oImplant = new Implant__c();
                oImplant.Business_Unit__c = clsTestData_MasterData.oMain_BusinessUnit.Id;
                oImplant.Business_Unit_Text__c = clsTestData_MasterData.oMain_Company.Name + ' - ' + clsTestData_MasterData.oMain_BusinessUnit.Name;
                oImplant.Implant_Implanting_Contact__c = testContacts.get(1).Id;
                oImplant.Implant_Implanting_Account__c = testAccounts.get(0).Id;
                oImplant.Implant_Date_Of_Surgery__c = System.today() - 1;
                oImplant.Therapy__c = clsTestData_Therapy.oMain_Therapy.Id;
                oImplant.Implant_Therapy__c = clsTestData_Therapy.oMain_Therapy.Id;
                oImplant.Attended_Implant_Text__c = 'UnAttended';
                oImplant.Duration_Nr__c = 10;
                oImplant.Procedure_Text__c='Replacement';
                oImplant.Manufacturer_ID__c = acc1.id;//'0012000000kvvDU';  // 001P000000R0kKd // Staging Medtronic Competitor Account Id
                oImplant.Implant_Manufacturer_ID__c=acc1.id;        
            insert oImplant ;            

            dataMap.put('URL','/apex/implantSelectorPage?core.apexpages.devmode.url=1&retURL=%2Fhome%2Fhome.jsp&save_new=1&sfdc.override=1');
            dataMap.put('DATE_OF_SURGERY','2011-08-04 14:55');        

            // Create Implant Option / Implant Option Therapy
            Implant_Option__c oImplantOption = new Implant_Option__c();
                oImplantOption.Type_Text__c = 'Cause';
            insert oImplantOption;
            
            Implant_Option_Therapy__c oImplantOptionTherapy = new Implant_Option_Therapy__c();
                oImplantOptionTherapy.Implant_Option_ID__c = oImplantOption.id;
                oImplantOptionTherapy.Therapy_ID__c = clsTestData_Therapy.oMain_Therapy.Id;
            insert oImplantOptionTherapy;
            
            Implant_Detail_Junction__c oImplantDetailJunction = new Implant_Detail_Junction__c();
                oImplantDetailJunction.Implant_ID__c = oImplant.Id;
                oImplantDetailJunction.Implant_Option_ID__c = oImplantOption.id;       
            insert oImplantDetailJunction;


            Therapy_Product__c oTherapyProduct1 = new Therapy_Product__c();
                oTherapyProduct1.Therapy__c = clsTestData_Therapy.oMain_Therapy.Id;
                oTherapyProduct1.Product__c = clsTestData_Product.lstProduct[1].Id;
            insert oTherapyProduct1;


            Therapy_Product__c oTherapyProduct2 = new Therapy_Product__c();
                oTherapyProduct2.Therapy__c = clsTestData_Therapy.oMain_Therapy.Id;
                oTherapyProduct2.Product__c = clsTestData_Product.lstProduct[2].Id;
            Insert oTherapyProduct2;

            Asset oAsset = new Asset();
                oAsset.Name = 'Test Asset';
                oAsset.AccountId = acc1.id;
                oAsset.Quantity = 10; 
                oAsset.Product2Id = clsTestData_Product.lstProduct[1].Id; 
                oAsset.Serial_Nr__c = clsUtil.getNewGUID();
            insert oAsset;
            
            dataMap.put('URL','/apex/implantSelectorPage?core.apexpages.devmode.url=1&retURL=%2Fhome%2Fhome.jsp&save_new=1&sfdc.override=1&id='+oImplant.Id);
            dataMap.put('id', oImplant.Id);
            dataMap.put('retURL',null);
            dataMap.put('DATE_OF_SURGERY','2011-08-04 14:55');

        }
       
    }


    @isTest static void test_controllerContactSelector_Multi_UserBU() {

        tBusinessUnitName = '';
        createTestData();

        System.runAs(oUser_Admin){

            clsTestData_MasterData.createUserBusinessUnit();

            List<User_Business_Unit__c> lstUserBU = [SELECT Business_Unit_Id__c, Business_Unit_text__c FROM User_Business_Unit__c WHERE User__c= :UserInfo.getUserId() ORDER BY Business_Unit_text__c];
            System.debug('**BC** MULTI - lstUserBU (' + lstUserBU.size() + ') : ' + lstUserBU);

            // Load Test Data
            String DATE_OF_SURGERY = '';
            List<Account> lstAccount = [SELECT Id FROM Account];
            List<Contact> lstContact = [SELECT Id FROM Contact];
            Implant__c oImplant = [SELECT Id, RecordTypeId, Implant_Therapy__c  FROM Implant__c LIMIT 1];
            Product2 oProduct = [SELECT Id FROM Product2 LIMIT 1];

            dataMap.put('contactToId', lstContact[0].Id);
            dataMap.put('contactFromId', lstContact[1].Id);
            dataMap.put('accountToId', lstAccount[0].Id);
            dataMap.put('accountFromId', lstAccount[1].Id);
            dataMap.put('retURL', '/');
            dataMap.put('action', 'Implant');
            dataMap.put('dateOfSurgery','2011-08-04 14:55');

            dataMap.put('URL','/apex/implantSelectorPage?core.apexpages.devmode.url=1&retURL=%2Fhome%2Fhome.jsp&save_new=1&sfdc.override=1');
            dataMap.put('dateOfSurgery','2011-08-04 14:55');        

            dataMap.put('URL','/apex/implantSelectorPage?core.apexpages.devmode.url=1&retURL=%2Fhome%2Fhome.jsp&save_new=1&sfdc.override=1&id='+oImplant.Id);
            dataMap.put('id', oImplant.Id);
            dataMap.put('retURL',null);
            dataMap.put('dateOfSurgery','2011-08-04 14:55');
            dataMap.put('implContact', lstContact[0].Id);
            dataMap.put('implAcc', lstAccount[0].Id);

            Test.startTest();

            testController(oImplant, true, oProduct);

            Test.stopTest();

        }

    }


    @isTest static void test_controllerContactSelector_Single_UserBU() {

        tBusinessUnitName = 'Cranial Spinal';
        createTestData();

        System.runAs(oUser_Admin){

            clsTestData_MasterData.createUserBusinessUnit();

            List<User_Business_Unit__c> lstUserBU = [SELECT Business_Unit_Id__c, Business_Unit_text__c FROM User_Business_Unit__c WHERE User__c= :UserInfo.getUserId() ORDER BY Business_Unit_text__c];
            System.debug('**BC** SINGLE - lstUserBU (' + lstUserBU.size() + ') : ' + lstUserBU);


            // Load Test Data
            String DATE_OF_SURGERY = '';
            List<Account> lstAccount = [SELECT Id FROM Account];
            List<Contact> lstContact = [SELECT Id FROM Contact];
            Implant__c oImplant = [SELECT Id, RecordTypeId, Implant_Therapy__c  FROM Implant__c LIMIT 1];
            Product2 oProduct = [SELECT Id FROM Product2 LIMIT 1];

            dataMap.put('contactToId', lstContact[0].Id);
            dataMap.put('contactFromId', lstContact[1].Id);
            dataMap.put('accountToId', lstAccount[0].Id);
            dataMap.put('accountFromId', lstAccount[1].Id);
            dataMap.put('retURL', '/');
            dataMap.put('action', 'Implant');
            dataMap.put('dateOfSurgery','2011-08-04 14:55');

            dataMap.put('URL','/apex/implantSelectorPage?core.apexpages.devmode.url=1&retURL=%2Fhome%2Fhome.jsp&save_new=1&sfdc.override=1');
            dataMap.put('dateOfSurgery','2011-08-04 14:55');        

            dataMap.put('URL','/apex/implantSelectorPage?core.apexpages.devmode.url=1&retURL=%2Fhome%2Fhome.jsp&save_new=1&sfdc.override=1&id='+oImplant.Id);
            dataMap.put('id', oImplant.Id);
            dataMap.put('retURL',null);
            dataMap.put('dateOfSurgery','2011-08-04 14:55');
            dataMap.put('implContact', lstContact[0].Id);
            dataMap.put('implAcc', lstAccount[0].Id);

            Test.startTest();

            testController(oImplant, true, oProduct);

            Test.stopTest();

        }

    }

    @isTest static void test_controllerContactSelector_No_UserBU() {

        createTestData();

        System.runAs(oUser_Admin){

            List<User_Business_Unit__c> lstUserBU = [SELECT Business_Unit_Id__c, Business_Unit_text__c FROM User_Business_Unit__c WHERE User__c= :UserInfo.getUserId() ORDER BY Business_Unit_text__c];
            System.debug('**BC** NO - lstUserBU (' + lstUserBU.size() + ') : ' + lstUserBU);

            // Load Test Data
            String DATE_OF_SURGERY = '';
            List<Account> lstAccount = [SELECT Id FROM Account];
            List<Contact> lstContact = [SELECT Id FROM Contact];
            Implant__c oImplant = [SELECT Id, RecordTypeId, Implant_Therapy__c  FROM Implant__c LIMIT 1];
            Product2 oProduct = [SELECT Id FROM Product2 LIMIT 1];

            dataMap.put('contactToId', lstContact[0].Id);
            dataMap.put('contactFromId', lstContact[1].Id);
            dataMap.put('accountToId', lstAccount[0].Id);
            dataMap.put('accountFromId', lstAccount[1].Id);
            dataMap.put('retURL', '/');
            dataMap.put('action', 'Implant');
            dataMap.put('dateOfSurgery','2011-08-04 14:55');

            dataMap.put('URL','/apex/implantSelectorPage?core.apexpages.devmode.url=1&retURL=%2Fhome%2Fhome.jsp&save_new=1&sfdc.override=1');
            dataMap.put('dateOfSurgery','2011-08-04 14:55');        

            dataMap.put('URL','/apex/implantSelectorPage?core.apexpages.devmode.url=1&retURL=%2Fhome%2Fhome.jsp&save_new=1&sfdc.override=1&id='+oImplant.Id);
            dataMap.put('id', oImplant.Id);
            dataMap.put('retURL',null);
            dataMap.put('dateOfSurgery','2011-08-04 14:55');
            dataMap.put('implContact', lstContact[0].Id);
            dataMap.put('implAcc', lstAccount[0].Id);

            Test.startTest();

            testController(oImplant, true, oProduct);

            Test.stopTest();

        }

    }

    @isTest static void test_controllerContactSelector2() {

        createTestData();

        System.runAs(oUser_Admin){

            clsTestData_MasterData.createUserBusinessUnit();

            List<User_Business_Unit__c> lstUserBU = [SELECT Business_Unit_Id__c, Business_Unit_text__c FROM User_Business_Unit__c WHERE User__c= :UserInfo.getUserId() ORDER BY Business_Unit_text__c];
            System.debug('**BC** TEST 2 - lstUserBU (' + lstUserBU.size() + ') : ' + lstUserBU);


            // Load Test Data
            String DATE_OF_SURGERY = '';
            List<Account> lstAccount = [SELECT Id FROM Account];
            List<Contact> lstContact = [SELECT Id FROM Contact];
            Implant__c oImplant = [SELECT Id, RecordTypeId, Implant_Therapy__c  FROM Implant__c LIMIT 1];
            Product2 oProduct = [SELECT Id FROM Product2 LIMIT 1];

            dataMap.put('contactToId', lstContact[0].Id);
            dataMap.put('contactFromId', lstContact[1].Id);
            dataMap.put('accountToId', lstAccount[0].Id);
            dataMap.put('accountFromId', lstAccount[1].Id);
            dataMap.put('retURL', '/');
            dataMap.put('action', 'Implant');
            dataMap.put(DATE_OF_SURGERY,'2011-08-04 14:55');

            dataMap.put('URL','/apex/implantSelectorPage?core.apexpages.devmode.url=1&retURL=%2Fhome%2Fhome.jsp&save_new=1&sfdc.override=1');
            dataMap.put('DATE_OF_SURGERY','2011-08-04 14:55');        

            dataMap.put('URL','/apex/implantSelectorPage?core.apexpages.devmode.url=1&retURL=%2Fhome%2Fhome.jsp&save_new=1&sfdc.override=1&id='+oImplant.Id);
            dataMap.put('id', oImplant.Id);
            dataMap.put('DATE_OF_SURGERY','2011-08-04 14:55');

            Test.startTest();

                oImplant.RecordTypeId    = clsUtil.getRecordTypeByDevName('Implant__c', new List<String>(bl_Implant.SET_AFSRECORDTYPE)[0]).Id;
            update oImplant;
            testController(oImplant, true, oProduct);

            Test.stopTest();

        }
        
    }


    private static void testController(Implant__c Implant, Boolean isUpdate, Product2 pro){

        System.runAs(oUser_Admin){

            List<User_Business_Unit__c> lstUserBU = [SELECT Business_Unit_Id__c, Business_Unit_text__c FROM User_Business_Unit__c WHERE User__c= :UserInfo.getUserId() ORDER BY Business_Unit_text__c];
            System.debug('**BC** TEST - lstUserBU (' + lstUserBU.size() + ') : ' + lstUserBU);


            Boolean bTest = false;                                  //-BC - 201401 - Added
            DateTime dtTest;
            List<SelectOption> lstSO = new list<SelectOption>();    //-BC - 201401 - Added
            
            Account acc1 = new Account();
            acc1.Name = 'TEST_triggerImplantBeforeInsert TestAccount1' ; 
            insert acc1;
            
            Sobject myAccount = new Account();
            ApexPages.currentPage().getParameters().put('DATE_OF_SURGERY','2011-08-04 14:55');
            ApexPages.StandardController newSct = new ApexPages.Standardcontroller(myAccount); //set up the standardcontroller
            controllerContactSelector newController = new controllerContactSelector();
            newController = new controllerContactSelector(newSct);
            newController.setDataMap(dataMap); 
            newController.dataMap.put('retUrl', 'retUrl');
            newController.dataMap.put('recordType', Implant.RecordTypeId);  //-BC - 201401 - Added
            newController.action = 'Implant' ;
            newController.getAMTitle(); 
            newController.setAMTitle('New Implant'); 
            newController.init();
            bTest = newController.bIsAFS;           //-BC - 201401 - Added
            lstSO = newController.getTherapies();   //-BC - 201401 - Added
            dtTest = newController.dStartDate;
            newController.getLeadPositionOptions();

            newController.getImplantProductsPickList(implant.Implant_Therapy__c,'impProduct');  //-BC - 20140618 - Added
            newController.getImplantProductsPickList(implant.Implant_Therapy__c,'repProduct');  //-BC - 20140618 - Added
            newController.getImplantProductsPickList(null,'repProduct');                        //-BC - 20140618 - Added

            newController.next();
            newController.getAccountContactAffiliations(acc1.Id);
            newController.RemoveProducts();
            newController.inupdatemode=false;
            newController.getAddProductsLists(); 
            implant.Implant_Referring_Contact__c=null;
            newController.refreshRefferingFromAccountNames();
            implant.Implant_Referring_Account__c=null;
            newController.refreshFromAccountNames();
            implant.Procedure_Text__c='New Implant';
            newController.ReplacedProductActive();  
       
            controllerContactSelector.stringToDate('07-03-2012');
            if (isUpdate )
                newController.setUpdateMode('Implant');
            newController.refreshAction();
            newController.action = 'Implant' ;
            newController.action = null ;
           // newController.init();
           //newController.next();        
            newController.refreshFromAccountNames();
            newController.refreshToAccountNames();
            newController.getAffFromAccountNames();
            newController.getAffToAccountNames();
            newController.getImplant();
            newController.getAccLabel();
            newController.getConLabel();
            newController.getTherapies();
            newController.refreshAction();
            newController.cancel();

            ApexPages.currentPage().getParameters().put('id',Implant.Id);   //-BC - 201401 - Added
            newController.redirectToContactSelector();
            
            newController.RedirectToDifferntPage();
            
            //newController.next();
            newController.getAffFromContactNames();
            implant.Procedure_Text__c='New Implant';       
            newController.ReplacedProductActive();
            newController.DurationActive();
            newController.getPatientDetail();
            newController.getQuantities();
            newController.getReplacedProducts();
            //newController.getImplantProducts();
            newController.getPatientDetails();
            newController.addProduct();
            newController.Quantity = '20';
            newController.ImplantProduct = pro.id;
            newController.addProduct();

            //-BC - 201501 - CR-6835 - START
            newController.etiologyChange();
            newController.therapyChange();
            newController.getAFSMarketingProducts();
            newController.getImplantProducts();
            //-BC - 201501 - CR-6835 - STOP

            newController.removeProductUsed();
            newController.removeProductUsed();
            newController.getvisibleWaitTime();
            newController.getvisibleTravelTime();
            newController.getvisibleStartTime();    //-BC - 201401 - CR-2611 - Added

            
            //newController.next();

        }
        
    }
    
}