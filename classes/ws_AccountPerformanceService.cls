/*
 *      Description : This class exposes methods to create / update / delete a Account Performance 
 *
 *      Author = Rudy De Coninck
 */
@RestResource(urlMapping='/AccountPerformanceService/*')
global with sharing class ws_AccountPerformanceService {
	@HttpPost
	global static String doPost() {
 		RestRequest req = RestContext.request;
			
		System.debug('post called '+req);
		System.debug('post requestBody '+req.requestBody);
			
		String body = req.requestBody.toString();
		IFAccountPerformanceRequest accountPerformanceRequest = (IFAccountPerformanceRequest)System.Json.deserialize(body, IFAccountPerformanceRequest.class);
			
		System.debug('post requestBody '+accountPerformanceRequest);
			
		Account_Performance__c accountPerformance = accountPerformanceRequest.accountPerformance;
		
		IFAccountPerformanceResponse resp = new IFAccountPerformanceResponse();
		
		//Only way to implement this change at this moment without making changes to the mobile app
		boolean compNullified = false;
		if(body!=null && body.contains('"Primary_Competitor__c": null')){
			compNullified=true;
		}
		
		//We catch all exception to assure that 
		try{
			
			bl_AccountPerformanceLoad.saveAccountPerformanceChangeMobile(accountPerformance, compNullified);
			resp.accountPerformance = accountPerformance;
			resp.id = accountPerformanceRequest.id;
	
			resp.success = true;
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
		}
 
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'AccountPerformance' ,req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
  			
		return System.JSON.serialize(resp);		
	}

	
	global class IFAccountPerformanceRequest{
		public Account_Performance__c accountPerformance {get;set;}
		public String id{get;set;}
	}
	
	global class IFAccountPerformanceResponse{
		public Account_Performance__c accountPerformance {get;set;}
		public String id{get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}
	
	
}