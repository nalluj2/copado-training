@isTest public class clsTestData_AccountPlan {
	
	public static Id idRecordType_AccountPlan2;
    public static Account_Plan_2__c oMain_AccountPlan2;
	public static List<Account_Plan_2__c> lstAccountPlan2 = new List<Account_Plan_2__c>();
    public static Date dStartDate_AccountPlan2 = Date.today().addDays(-60);
    public static Date dEndDate_AccountPlan2 = Date.today().addDays(60);


    //---------------------------------------------------------------------------------------------------
    // Create Account Plan 2 Data
    //---------------------------------------------------------------------------------------------------
    public static List<Account_Plan_2__c> createAccountPlan2(){
        return createAccountPlan2(true);
    }
    public static List<Account_Plan_2__c> createAccountPlan2(Boolean bInsert){

        if (oMain_AccountPlan2 == null){

            if (clsTestData_MasterData.oMain_Company == null) clsTestData_MasterData.createCompany();
            if (clsTestData_MasterData.oMain_BusinessUnitGroup == null) clsTestData_MasterData.createBusinessUnitGroup();
            if (clsTestData_MasterData.oMain_BusinessUnit == null) clsTestData_MasterData.createBusinessUnit();
            if (clsTestData_MasterData.oMain_SubBusinessUnit == null) clsTestData_MasterData.createSubBusinessUnit();
            if (clsTestData_Account.oMain_Account == null) clsTestData_Account.createAccount();

            lstAccountPlan2 = new List<Account_Plan_2__c>();
            for (Account oAccount : clsTestData_Account.lstAccount){
                for (Id idBusinessUnitGroup : clsTestData_MasterData.mapBusinessUnitGroupId_BusinessUnits.keyset()){
                    for (Business_Unit__c oBusinessUnit : clsTestData_MasterData.mapBusinessUnitGroupId_BusinessUnits.get(idBusinessUnitGroup)){
                        if (clsTestData_MasterData.mapBusinessUnitId_SubBusinessUnits.containsKey(oBusinessUnit.Id)){
                            for (Sub_Business_Units__c oSubBusinessUnit : clsTestData_MasterData.mapBusinessUnitId_SubBusinessUnits.get(oBusinessUnit.Id)){
                                Account_Plan_2__c oAccountPlan2 = new Account_Plan_2__c();
                                    oAccountPlan2.Account__c = oAccount.Id;
                                    oAccountPlan2.Account_Plan_Level__c = 'Sub Business Unit';
                                    oAccountPlan2.Business_Unit__c = oBusinessUnit.Id;
                                    oAccountPlan2.Sub_Business_Unit__c = oSubBusinessUnit.Id;
                                    oAccountPlan2.Business_Unit_Group__c = idBusinessUnitGroup;                                    
                                    oAccountPlan2.Start_Date__c = dStartDate_AccountPlan2;
                                    oAccountPlan2.End_Date__c = dEndDate_AccountPlan2;
                                    if (idRecordType_AccountPlan2 != null){
                                        oAccountPlan2.RecordTypeId = idRecordType_AccountPlan2; 
                                    }
                                lstAccountPlan2.add(oAccountPlan2);
                            }
                        }
                    }
                }
            }
            if (bInsert) insert lstAccountPlan2;
            oMain_AccountPlan2 = lstAccountPlan2[0];
        }

        return lstAccountPlan2;
    }
    //---------------------------------------------------------------------------------------------------

}