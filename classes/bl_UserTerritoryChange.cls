public with sharing class bl_UserTerritoryChange {

	
	public static void processUserTerritoryChanges(List<User_Territory_Change__c> userTerritoryChanges){
		
		List<Id> adds = new List<Id>();
		List<Id> removes = new List<Id>();
		
		for (User_Territory_Change__c utc : userTerritoryChanges){
			
			if (utc.Operation__c!=null && utc.Operation__c.equals('Add')){
				adds.add(utc.Id);	
			}
			if (utc.Operation__c!=null && utc.Operation__c.equals('Remove')){
				removes.add(utc.Id);	
			}
			
		}
		
		if (adds.size()>0){
			doSoapCallAdd(adds);
			System.debug('Adds '+adds  );
		}
		
		if (removes.size()>0){
			doSoapCallRemove(removes);
			System.debug('Removes '+removes);
		}
	}
	
	@future	
	public static void doSoapCallAdd(List<Id> userTerritoryChangeIds){
		
		List<User_Territory_Change__c> userTerritoryChanges = [Select User__c, Territory__c from User_Territory_Change__c where Id IN :userTerritoryChangeIds];
		
		List<UserTerritory2Association> userTerritories = new List<UserTerritory2Association>();
		
		for(User_Territory_Change__c userTerritoryChange : userTerritoryChanges){
	
			UserTerritory2Association userTerritory = new UserTerritory2Association();
			userTerritory.UserId = userTerritoryChange.User__c ;
			userTerritory.Territory2Id =  userTerritoryChange.Territory__c ;
			
			userTerritories.add(userTerritory);
		}
		
		insert userTerritories;
	}
	
	@future	
	public static void doSoapCallRemove(List<Id> userTerritoryChangeIds){
       
       List<User_Territory_Change__c> userTerritoryChanges = [Select User_Territory_ID__c from User_Territory_Change__c where Id IN :userTerritoryChangeIds];
       
       Set<Id> userTerritoryAssociationIds = new Set<Id>();
       
       for(User_Territory_Change__c userTerritoryChange : userTerritoryChanges){
           	
			userTerritoryAssociationIds.add(userTerritoryChange.User_Territory_ID__c);
       }
       		
       	delete [Select Id from UserTerritory2Association where Id IN :userTerritoryAssociationIds];
       
	}
	
	
	public static List<User_Territory_Change__c> linkRelationshipDataToTerritoryChange(List<User_Territory_Change__c> userTerritoryChanges){
		
		Set<Id> utcToRemove = new Set<Id>();
		Set<Id> userIds = new Set<Id>();
		Set<Id> terrIds = new Set<Id>();
		
		Set<String> userNames = new Set<String>();
		Set<String> territoryNames = new Set<String>();
		Set<String> userSapIDs = new Set<String>();
		Set<String> terrExternalKeys = new Set<String>();
		
		for (User_Territory_Change__c utc :userTerritoryChanges){
			userNames.add(utc.User_name__c);
			territoryNames.add(utc.Territory_Name__c);
			if (null!=utc.Territory_External_Key__c){
				terrExternalKeys.add(utc.Territory_External_Key__c);
			}
			if (null!=utc.User_SAP_ID__c){
				userSapIDs.add(utc.User_SAP_ID__c);
			}
		}
		
		System.debug('userNames : '+userNames);
		System.debug('territoryNames : '+territoryNames);
		System.debug('user sap ids  : '+userSapIDs);
		System.debug('terrExternalKeys : '+terrExternalKeys);
		
		List<Territory2> territories = [select Id, name from Territory2 where name in :territoryNames];
		List<User> users = [select Id, name from User where name in :userNames];
		
		List<Territory2> territoriesExtKey = [select Id, name, External_Key__c from Territory2 where external_key__c in :terrExternalKeys];
		List<User> usersSAP = [select Id, name, sap_id__c from User where sap_id__c in :userSapIDs];
		
		Map<String,User> userMap = new Map<String,User>();
		Map<String,Territory2> territoryMap = new Map<String,Territory2>();
		Map<String,User> userSAPmap = new Map<String,User>();
		Map<String,Territory2> territoryExtKeyMap = new Map<String,Territory2>();
		
		for (Territory2 t : territories){
			territoryMap.put(t.name, t);
		}		

		for (User u : users){
			userMap.put(u.name, u);
		}	
		
		for (Territory2 t : territoriesExtKey){
			territoryExtKeyMap.put(t.external_key__c, t);
		}		

		for (User u : usersSAP){
			userSAPMap.put(u.sap_id__c, u);
		}	

		for (User_Territory_Change__c utc : userTerritoryChanges){
			User u = null;
			if(utc.User_SAP_ID__c != null){
				u = userSAPMap.get(utc.User_SAP_ID__c);
				if (u == null ){
					utc.addError('User SAP ID not found');
				}else{
					utc.User__c = u.id;
				} 
			}else{
				if (utc.User_name__c!=null){
					u = userMap.get(utc.User_name__c);
					if (u == null ){
						utc.addError('User name not found');
					}else{
						utc.User__c = u.id;
					} 
				}
			}
			
			Territory2 t = null;
			if (utc.Territory_External_Key__c!=null){
				t = territoryExtKeyMap.get(utc.Territory_External_Key__c);
				if (t == null){
					utc.addError('Territory external key not found');
				}else{
					utc.Territory__c = t.id;
				}
			}else{
				if (utc.Territory_Name__c!=null){
					t = territoryMap.get(utc.Territory_Name__c);
					if (t == null){
						utc.addError('Territory name not found');
					}else{
						utc.Territory__c = t.id;
					}
				}
			}
			
			//Link userTerritory if remove
			if (utc.operation__c!= null && utc.operation__c.compareTo('Remove')==0 && utc.Territory__c!=null && utc.User__c!=null){
				utcToRemove.add(utc.id);
				userIds.add(utc.User__c);
				terrIds.add(utc.Territory__c);
			}
		}
		
		System.debug('Existing terrIds'+terrIds);
		
		
		List<UserTerritory2Association> existingUserTerritories = [select Id, UserId, Territory2Id from UserTerritory2Association where Territory2Id in :terrIds and UserId in :userIds];
		Map<String,String> mapUT = new Map<String,String>();
		for (UserTerritory2Association ut : existingUserTerritories){
			mapUt.put(''+ut.UserId+ut.Territory2Id,ut.Id);
		}
		
		
		for (User_Territory_Change__c utc : userTerritoryChanges){
			if (utc.operation__c!= null && utc.operation__c.compareTo('Remove')==0 && utc.Territory__c!=null && utc.User__c!=null){
				String ut = mapUT.get(utc.User__c+utc.Territory__c);
				utc.User_Territory_ID__c = ut;
			}
		}
		
		
		return userTerritoryChanges;
	}

}