@isTest
private class TEST_ws_FileUpload {
	
	@isTest static void testUploadAttachment() {

		//----------------------------------------------------------------------------------------------------------------
		// Create Test Data
		//----------------------------------------------------------------------------------------------------------------

		Account oAccount = clsTestData_Account.createAccount()[0];

        String tFileName = 'testfile.txt';
		Blob oBlob = Blob.valueOf('presents a dummy file');

		//----------------------------------------------------------------------------------------------------------------
        
		Test.startTest();

		RestRequest oRequest = new RestRequest();
			oRequest.params.put('FileName' , tFileName);
			oRequest.params.put('ParentId', oAccount.Id);	
			oRequest.requestBody = oBlob;
		RestContext.request = oRequest;

		String tResponse = ws_FileUpload.uploadAttachment();

		Test.stopTest();
		

    }
	
}