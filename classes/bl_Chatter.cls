/*
 *      Created Date : 14-02-2013
 *      Description : This class is used to centralize the logic to make followers for a record.
 *      Author = Manish Kumar Srivastava
 *		------------------------------------------------------------------------------------------
 *      Modified Date 	: 17-02-2014
 *      Author 			: Bart Caelen
 *      Description 	: Added the method addChatterFeed_Text
 */
public class bl_Chatter{

    public static void CreateFollower(map<id,set<id>> mapRecordIdWithUserIds){
        EntitySubscription follower;
        List<EntitySubscription> lstEntitySubs=new List<EntitySubscription>();
        if(mapRecordIdWithUserIds.keyset().size()>0){
            for(id recid:mapRecordIdWithUserIds.keyset()){
                if(mapRecordIdWithUserIds.get(recid).size()>0){
                    for(id userid:mapRecordIdWithUserIds.get(recid)){
                        follower=new EntitySubscription(parentid=recid,SubscriberId=userid);    
                        lstEntitySubs.add(follower);
                    }
                }
            }
            if(lstEntitySubs.size()>0){
            	insert lstEntitySubs;
            }
        }
    }

	//- BC - 20140217 - Added the logic to create Chatter Feeds for all kind of objects - START
	//	- mapOldData 				--> map with the old data (from trigger.oldMap)
	//	- mapNewData 				--> map with the new data (from trigger.newMap)
	//	- tAction					--> INSERT / UPDATE / DELETE /UNDELETE
	//	- tParentIDFieldName		--> Name of the field that indicates the object where the chatter feed will be created. This can be a parent field (lookup / master-detail) but also just the ID field if there is no parent.
	//	- setFieldNamesToMonitor	--> Set of fieldnames that will be compared if it's an UPDATE action - for each field in this set that is changed (oldData vs newData) a new Chatter Feeed will be created
	//	- tFieldForURLLabel			--> In the chatter feed there will be a link to the record (expect when the action is DELETE). This parameters defines the value that will be shown as the label of the link.  Normally you will use "Name" but this field doesn't exist on all sObjects (eg. Task, Note, ...)
	public static void createChatterFeed(map<id, sObject> mapOldData, map<id, sObject> mapNewData, string tAction, string tParentIDFieldName, set<string> setFieldNamesToMonitor, string tFieldForURLLabel){
		
		list<bl_Chatter.w_ChatterFeed> lstWChatterFeed = new list<bl_Chatter.w_ChatterFeed>();
		
		if (tAction.toUpperCase() == 'INSERT'){

			for (sObject oSObject : mapNewData.values()){
				bl_Chatter.w_ChatterFeed oWChatterFeed = new bl_Chatter.w_ChatterFeed();
					oWChatterFeed.tAction 	= tAction;
					oWChatterFeed.idParent 	= id.valueOf(string.valueOf(oSObject.get(tParentIDFieldName)));
					oWChatterFeed.idRecord 	= id.valueOf(string.valueOf(oSObject.get('id')));
					oWChatterFeed.tName 	= string.valueOf(oSObject.get(tFieldForURLLabel));					
				lstWChatterFeed.add(oWChatterFeed);
			}
			
		}else if (tAction.toUpperCase() == 'UNDELETE'){

			for (sObject oSObject : mapNewData.values()){
				bl_Chatter.w_ChatterFeed oWChatterFeed = new bl_Chatter.w_ChatterFeed();
					oWChatterFeed.tAction 	= tAction;
					oWChatterFeed.idParent 	= id.valueOf(string.valueOf(oSObject.get(tParentIDFieldName)));
					oWChatterFeed.idRecord 	= id.valueOf(string.valueOf(oSObject.get('id')));
					oWChatterFeed.tName 	= string.valueOf(oSObject.get(tFieldForURLLabel));					
				lstWChatterFeed.add(oWChatterFeed);
			}

		}else if (tAction.toUpperCase() == 'DELETE'){
			clsUtil.debug('tAction is delete');
			clsUtil.debug('mapOldData.values() : ' + mapOldData.values());
			for (sObject oSObject : mapOldData.values()){
				bl_Chatter.w_ChatterFeed oWChatterFeed = new bl_Chatter.w_ChatterFeed();
					oWChatterFeed.tAction 	= 'DELETE';
					oWChatterFeed.idParent 	= id.valueOf(string.valueOf(oSObject.get(tParentIDFieldName)));
					oWChatterFeed.idRecord 	= id.valueOf(string.valueOf(oSObject.get('id')));
					oWChatterFeed.tName 	= string.valueOf(oSObject.get(tFieldForURLLabel));
				clsUtil.debug('oWChatterFeed : ' + oWChatterFeed);
				lstWChatterFeed.add(oWChatterFeed);
			}


		}else if (tAction.toUpperCase() == 'UPDATE'){

			for (sObject oSObject : mapNewData.values()){
				for (string tFieldName : setFieldNamesToMonitor){
					// Verify if predefined fields are changed
					object oObj_Old = mapOldData.get(string.valueOf(oSObject.get('Id'))).get(tFieldName);
					object oObj_New = oSObject.get(tFieldName);
					if (oObj_Old != oObj_New){

						bl_Chatter.w_ChatterFeed oWChatterFeed = new bl_Chatter.w_ChatterFeed();
							oWChatterFeed.tAction 		= 'UPDATE';
							oWChatterFeed.idParent 		= id.valueOf(string.valueOf(oSObject.get(tParentIDFieldName)));
							oWChatterFeed.idRecord 		= id.valueOf(string.valueOf(oSObject.get('id')));
							oWChatterFeed.tName 		= string.valueOf(oSObject.get(tFieldForURLLabel));
							oWChatterFeed.tFieldName	= tFieldName;
							oWChatterFeed.oObj_Old 		= oObj_Old; 					
							oWChatterFeed.oObj_New 		= oObj_New;
						lstWChatterFeed.add(oWChatterFeed);

					}
				}
			}
		}

		if (lstWChatterFeed.size() > 0){
			createChatterFeed(lstWChatterFeed);
		}		
		
	}

	public static void createChatterFeed(list<bl_Chatter.w_ChatterFeed> lstWChatterFeed){

		list<FeedItem> lstFeedItem_Insert = new list<FeedItem>();
		
		map<string, Schema.SObjectField> mapField;

		for (bl_Chatter.w_ChatterFeed oChatterFeed : lstWChatterFeed){
			string tPrefix = string.valueOf(oChatterFeed.idRecord).subString(0, 3);
			string tSObjectName = clsUtil.getSObjectNameFromIDPrefix(tPrefix);
			string tSObject_Label = clsUtil.getSObjectLabel(tSObjectName);
			
			FeedItem oFeedItem = new FeedItem();
			oFeedItem.ParentId = oChatterFeed.idParent;

			if ( (oChatterFeed.idRecord != null) && (clsUtil.isNull(oChatterFeed.tName, '') != '') ){
				oFeedItem.LinkUrl = '/' + oChatterFeed.idRecord;
				oFeedItem.Title = oChatterFeed.tName;
			}
				
			if (oChatterFeed.tAction == 'INSERT'){

				oFeedItem.Body = 'Created a new ' + tSObject_Label + '.';

			}else if (oChatterFeed.tAction == 'UPDATE'){
				mapField = clsUtil.getFieldsForSobject(tSObjectName);
				string tFieldLabel = mapField.get(oChatterFeed.tFieldName).getDescribe().getLabel();

				oFeedItem.Body = 'The ' + tSObject_Label + ' is changed.';
				if ( (mapField.get(oChatterFeed.tFieldName).getDescribe().getType() == schema.Displaytype.Date) || (mapField.get(oChatterFeed.tFieldName).getDescribe().getType() == schema.Displaytype.DateTime) ){
					oFeedItem.Body += ' The field "' + tFieldLabel + '" is changed from "';
					if (oChatterFeed.oObj_Old != null){
						oFeedItem.Body += date.valueOf(clsUtil.isNull(oChatterFeed.oObj_Old, '')).format();
					}
					oFeedItem.Body += '" to "';
					if (oChatterFeed.oObj_New != null){
						oFeedItem.Body += date.valueOf(clsUtil.isNull(oChatterFeed.oObj_New, '')).format();
					}
					oFeedItem.Body += '"';
				}else{
					oFeedItem.Body += ' The field "' + tFieldLabel + '" is changed from "' + clsUtil.isNull(oChatterFeed.oObj_Old, '') + '" to "' + clsUtil.isNull(oChatterFeed.oObj_New,'') + '"';
				}
				
			}else if (oChatterFeed.tAction == 'DELETE'){
				clsUtil.debug('oChatterFeed.tAction = DELETE');

				oFeedItem.Body = 'Deleted the ' + tSObject_Label + ' with the name  "' + oChatterFeed.tName + '".';

				clsUtil.debug('oFeedItem.Body : ' + oFeedItem.Body);
			
			}else if (oChatterFeed.tAction == 'UNDELETE'){

				oFeedItem.Body = 'Undeleted the ' + tSObject_Label + '.';

			}
			lstFeedItem_Insert.add(oFeedItem);
		}
		
		clsUtil.debug('lstFeedItem_Insert : ' + lstFeedItem_Insert);
		
		if (lstFeedItem_Insert.size() > 0){
			insert lstFeedItem_Insert;
		}
	}


	public class w_ChatterFeed {
		
		public w_ChatterFeed(){
			this.idParent 	= null;
			this.idRecord 	= null;
			this.tName		= '';
			this.tAction 	= '';
			this.tFieldName = '';
			this.oObj_New 	= null;
			this.oObj_Old 	= null;
		}
		
		public id idParent			{ get;set; }
		public id idRecord			{ get;set; }
		public string tName			{ get;set; }
		public string tAction		{ get;set; }
		public object oObj_Old		{ get;set; }
		public object oObj_New		{ get;set; }
		public string tFieldName	{ get;set; }
	
	}

	//- BC - 20140217 - Added the logic to create Chatter Feeds for all kind of objects - STOP


	// Create a Chatter Post related to a record (= idParent) with @Mention (= idUser) and a certain text (= tMessageBody)
	public static ConnectApi.FeedItemInput createFeedItemInput(Id idParent, Id idUser, String tMessageBody){

		ConnectApi.FeedItemInput oFeedItemInput = new ConnectApi.FeedItemInput();
		ConnectApi.MentionSegmentInput oMentionSegmentInput = new ConnectApi.MentionSegmentInput();
		ConnectApi.MessageBodyInput oMessageBodyInput = new ConnectApi.MessageBodyInput();
		ConnectApi.TextSegmentInput oTextSegmentInput = new ConnectApi.TextSegmentInput();

		oMessageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

		oMentionSegmentInput.id = idUser;
		oMessageBodyInput.messageSegments.add(oMentionSegmentInput);

		oTextSegmentInput.text = tMessageBody;
		oMessageBodyInput.messageSegments.add(oTextSegmentInput);

		oFeedItemInput.body = oMessageBodyInput;
		oFeedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
		oFeedItemInput.subjectId = idParent;

		return oFeedItemInput;

	}

}