/*
 *   Controller name: ctrlExt_ManageUserSBUs
 *   Page Name : page_TransferDiabetesSegmentation
 *   Author : Dheeraj
 *   Description: This class is used to include logic for add/edit/delete user SBU records
*/
public class ctrlExt_ManageUserSBUs {
    public boolean boolHasUserSBUs{get;set;}
    public boolean boolHasAvailBUs{get;set;}
    public boolean boolHasAvailSBUs{get;set;}
    
    public list<clWrapSelBUs> lstWrapSelBUs{get;set;}
    public list<clWrapSelBUs> lstWrapSelSBUs{get;set;}
    
    public list<clWrapSelBUs> lstWrapAvlBUs{get;set;}
    public list<clWrapSelBUs> lstWrapAvlSBUs{get;set;}    
   
    public id userId{get;set;}    
    public boolean shomessage{get;set;}
    public boolean buValue{get;set;}
    public integer countSlectedBU=0;   
   
    
    public List<User_Business_Unit__c> lstUserBUs=new List<User_Business_Unit__c>();
    
    public class clWrapSelBUs {
        public Business_Unit__c BU {get; set;}
        public Sub_Business_Units__c SBU {get; set;}
        public Boolean boolSelected {get; set;}
        public boolean boolPrimarySelected{get;set;}
        
        public clWrapSelBUs(Business_Unit__c BUData, boolean boolsel) {
            BU = BUData;
            boolSelected = boolsel;
        }        
        public clWrapSelBUs(Sub_Business_Units__c SBUData, boolean boolsel,boolean boolsel1) {
            SBU = SBUData;
            boolSelected = boolsel;           
            boolPrimarySelected=boolsel1;            
        }
    }   
    
    
    public ctrlExt_ManageUserSBUs(ApexPages.StandardController controller){
        // Get list of BUs
        
        lstWrapSelBUs=new List<clWrapSelBUs>();
        lstWrapSelSBUs=new List<clWrapSelBUs>();
        
        //map<id,string> mapUserCompany = SharedMethods.getUserCompany();
        
        boolHasUserSBUs = false; 
        buValue=false;      
        map<id,string> mapUserBUs = new map<id,string>();
        map<id,string> mapUserSBUs = new map<id,string>();
        map<id,boolean> mapUserSBUPrimary=new map<id,boolean>();
        string UserCompany;
        
        userId = ApexPages.currentPage().getParameters().get('id'); 
        if(userId!=null){
            UserCompany=[select id,Company_Code_Text__c from User where id=:userId].Company_Code_Text__c;
        }
       
        lstUserBUs = [Select Primary__c,Business_Unit_Id__c,Sub_Business_Unit__c,Sub_Business_Unit_name__c,Business_Unit_text__c From User_Business_Unit__c where User__c=:userId order by Business_Unit_text__c];
        for (User_Business_Unit__c UserBU:lstUserBUs) {
            mapUserBUs.put(UserBU.Business_Unit_Id__c,UserBU.Business_Unit_text__c);
            mapUserSBUs.put(UserBU.Sub_Business_Unit__c,UserBU.Sub_Business_Unit_name__c); 
            mapUserSBUPrimary.put(UserBU.Sub_Business_Unit__c,UserBU.Primary__c);
        }
        set<id> setUserBUs = mapUserBUs.keyset();
        set<id> setUserSBUs=mapUserSBUs.keyset();    
       
        
        // Get Available BUs
        boolHasAvailBUs=false;        
        list<Business_Unit__c> lstAvailBUs = [select id, Name,Company__r.Company_Code_Text__c from Business_Unit__c where Company__r.Company_Code_Text__c =:UserCompany order by Name];
        if(lstAvailBUs.size()>0){
            boolHasAvailBUs=true;   
            lstWrapAvlBUs = new List<clWrapSelBUs>(); 
            for(Business_Unit__c BU:lstAvailBUs){                
                if(setUserBUs.contains(BU.id)==true){                    
                    lstWrapAvlBUs.add(new clWrapSelBUs(BU,true));                    
                } else {                    
                    lstWrapAvlBUs.add(new clWrapSelBUs(BU,false));                  
                }
            }                         
        }       

        // Get Available SBUs
        list<Sub_Business_Units__c> lstAvailSBUs = [select id, Name,Business_Unit__r.name,Business_Unit__r.Company__c from Sub_Business_Units__c where Business_Unit__r.Company__r.Company_Code_Text__c =:UserCompany order by Business_Unit__r.Name,Name];
            if(lstAvailSBUs.size()>0){
                boolHasAvailSBUs=true;             
                lstWrapAvlSBUs=new List<clWrapSelBUs>();          
                for(Sub_Business_Units__c SBU:lstAvailSBUs){                
                    if(setUserSBUs.contains(SBU.id)==true){ 
                        if(mapUserSBUPrimary.get(SBU.id)==true){                            
                            lstWrapAvlSBUs.add(new clWrapSelBUs(SBU,true,true));
                        }
                        else if(mapUserSBUPrimary.get(SBU.id)==false){                           
                            lstWrapAvlSBUs.add(new clWrapSelBUs(SBU,true,false));
                        }
                    }                     
                    else {                    
                        lstWrapAvlSBUs.add(new clWrapSelBUs(SBU,false,false));                  
                    }
                }               
            }        
        
        }
        Map<id,id> BuSbumap=new map<id,id>();
       
        public void selectSBU(){
            id idSelectedBU = null;
            if(ApexPages.currentPage().getParameters().get('p')!=null)
                idSelectedBU  = ApexPages.currentPage().getParameters().get('p');                
            for(integer i=0;i<lstWrapAvlBUs.size();i++){                
                if(idSelectedBU != null){
                    if(idSelectedBU == lstWrapAvlBUs[i].BU.ID){
                        buValue = lstWrapAvlBUs[i].boolselected;     
                    }
                }
            }                   
            if(lstWrapAvlBUs.size()>0){
                for(integer j=0;j<lstWrapAvlSBUs.size();j++){                                               
                    if(lstWrapAvlSBUs[j].sbu.Business_Unit__c==idSelectedBU){                                                    
                        if(buValue==true){                            
                            lstWrapAvlSBUs[j].boolselected=true;                                
                        }                              
                        else if(buValue == false){
                            lstWrapAvlSBUs[j].boolselected=false;
                            lstWrapAvlSBUs[j].boolPrimarySelected=false;
                        }
                    }      
                }                
            }           
       } 


        public pagereference save(){
            shomessage=false;
            string strUSBU='';
            lstWrapSelSBUs.clear();
            for(integer i=0;i<lstWrapAvlSBUs.size();i++){
                if(lstWrapAvlSBUs[i].boolselected==true)
                {
                    lstWrapSelSBUs.add(lstWrapAvlSBUs[i]);
                }
                if(lstWrapAvlSBUs[i].boolselected==false && lstWrapAvlSBUs[i].boolPrimarySelected==true){
                    shomessage=true;
                }                                    
            }
            if(shomessage==true){                    
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'To make a SBU primary please select that SBU first');
                    ApexPages.addMessage(myMsg);                                       
                    return null;
            }
            integer count=0;
            for(integer i=0;i<lstWrapSelSBUs.size();i++){
                if(lstWrapSelSBUs[i].boolPrimarySelected==true){
                    count=1;
                } 
            }
            if(count==0 && lstWrapSelSBUs.size()>0){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select a primary SBU');
                ApexPages.addMessage(myMsg);                                       
                return null;
            }
            for(integer i=0;i<lstWrapAvlBUs.size();i++){
                if(lstWrapAvlBUs[i].boolSelected==true){
                    lstWrapSelBUs.add(lstWrapAvlBUs[i]);
                    countSlectedBU=countSlectedBU+1;
                }                
            }             
            if(lstUserBUs.size()>0){                                     
                delete lstUserBUs;                    
            }
            List<User_Business_Unit__c> lstUsbu=new List<User_Business_Unit__c>();                
            User u=new User();   

            Set<String> setBusinessUnitName = new Set<String>();   //-BC - 20150211 - CR-3238 - Added                        
            for(integer i=0;i<lstWrapAvlSBUs.size();i++){               
                
                //logic to insert selected user SBU
                if(lstWrapAvlSBUs[i].boolSelected==true){
                    User_Business_Unit__c uSbu=new User_Business_Unit__c();
                    uSbu.Sub_Business_Unit__c=lstWrapAvlSBUs[i].SBU.id;
                    uSbu.User__c=userId;
                    uSbu.Primary__c=lstWrapAvlSBUs[i].boolPrimarySelected;                
                    lstUsbu.add(uSbu);                    
                    
                    //logic to update user Business unit and Sub business unit field on user
                    u.id=uSbu.User__c;                        
                    if(strUSBU==''){
                        u.User_Sub_Bus__c=lstWrapAvlSBUs[i].SBU.name;                        
                    }  
                    else{                  
                        u.User_Sub_Bus__c=strUSBU+','+lstWrapAvlSBUs[i].SBU.name; 
                    }
                    strUSBU= u.User_Sub_Bus__c;  
                    if(lstWrapAvlSBUs[i].boolPrimarySelected==true && lstWrapAvlBUs.size()!=countSlectedBU){                            
                        u.User_Business_Unit_vs__c=lstWrapAvlSBUs[i].SBU.Business_Unit__r.name;
                    }
                    else if(lstWrapAvlBUs.size()==countSlectedBU){                            
                        u.User_Business_Unit_vs__c='All';
                    }
                    if(lstWrapAvlSBUs[i].boolPrimarySelected==true){
                    	u.Primary_sBU__c =lstWrapAvlSBUs[i].SBU.name;
                    }

                    setBusinessUnitName.add(lstWrapAvlSBUs[i].SBU.Business_Unit__r.Name);   //-BC - 20150211 - CR-3238 - Added
                }                              
            }
            //-BC - 20150211 - CR-3238 - Added - START
            if (setBusinessUnitName.size() > 0){
                String tBusinessUnitName_Final = '';
                for (String tBusinessUnitName : setBusinessUnitName){
                    tBusinessUnitName_Final += tBusinessUnitName + ',';
                }
                tBusinessUnitName_Final = tBusinessUnitName_Final.left(tBusinessUnitName_Final.length()-1);
                u.Business_Unit_Name__c = tBusinessUnitName_Final;
            }else{
                u.Business_Unit_Name__c = '';                
            }
            //-BC - 20150211 - CR-3238 - Added - STOP

            insert lstUsbu;                
            if(u.id!=null){
                update u;
            }
            else if(u.id==null){
              u.id= ApexPages.currentPage().getParameters().get('id');
              u.User_Sub_Bus__c=null;
              update u; 
            }
            Pagereference pageref=new PageReference('/'+userId+'?noredirect=1');  
            return pageref;                    
        } 
        public pagereference Cancel(){
            Pagereference pageref=new PageReference('/'+userId+'?noredirect=1');            
            return pageref;
        }
    }