/** * This class contains unit tests for TMA Apex classes.
 *  Created : 20112402 
 *  Description : Test Coverage for TMA Apex Classes. 
 *  Classes Covered : controllerTerritoryAssignEmployeest, controllerTerritoryAddPage, controllerTerritoryEditPage, controllerTerritoryDelete 
 *  Author : MDT / JaKe
 */
@isTest(SeeAllData = true)
private class TEST_TMA1 {
    static testMethod void testcontrollerTerritoryAssignEmployees(){
    PageReference pageRef = Page.TerritoryAssignEmployees;
    Test.setCurrentPage(pageRef);
    User u, u2;

    // Get Company
    Company__c comp = [select id,name from Company__c where name='Europe' limit 1];                            
    
    Territory2 terr1 = new Territory2();
    Territory2 terr2 = new Territory2();
    Territory2 terr3 = new Territory2();
    Territory2 terr4 = new Territory2();
    List<Territory2> terrList=new List<Territory2>();
           
    User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
    System.runAs ( thisUser ) {
	
	Id MDT_Terr_Model = [Select Id from Territory2Model where DeveloperName = 'Medtronic'].Id;
	
// create Territories
    terr1.Name = 'Territory1';
    terr1.DeveloperName = 'Territory1';
    terr1.Business_Unit__c = 'CRHF';
    terr1.Territory2TypeId = bl_Territory.territoryTypeMap.get('Region');
    terr1.Country_UID__c='NL';
    terr1.Short_Description__c ='test1 description';
    terr1.Therapy_Groups_Text__c='CardioInsight';
    terr1.Company__c = string.valueOf(comp.id).substring(0,15);
    terr1.Territory2ModelId = MDT_Terr_Model; 
    terrList.add(terr1);
    
    terr2.Name = 'Territory2';
    terr2.DeveloperName = 'Territory2';
    terr2.Business_Unit__c = 'CRHF';
    terr2.Territory2TypeId = bl_Territory.territoryTypeMap.get('Country');
    terr2.Country_UID__c='NL';
    terr2.Short_Description__c ='test2 description';
    terr2.Therapy_Groups_Text__c='CardioInsight';
    terr2.Company__c = string.valueOf(comp.id).substring(0,15);
    terr2.Territory2ModelId = MDT_Terr_Model; 
    terrList.add(terr2);
    
    terr3.Name = 'Territory3';
    terr3.DeveloperName = 'Territory3';
    terr3.Business_Unit__c = 'CRHF';
    terr3.Territory2TypeId = bl_Territory.territoryTypeMap.get('District');
    terr3.Country_UID__c='NL';
    terr3.ParentTerritory2Id= terr1.id;
    terr3.Short_Description__c ='test2 description';
    terr3.Therapy_Groups_Text__c='CardioInsight';
    terr3.Company__c = string.valueOf(comp.id).substring(0,15);
    terr3.Territory2ModelId = MDT_Terr_Model; 
    terrList.add(terr3);
            
    insert terrList;

    }

    Profile p = [select id from profile where name='System Administrator'];
    string t = [select Territory_UID2__c from Territory2 where id =:terr1.id].Territory_UID2__c;
       // UserRole r = [Select id from userrole where name='System Administrator'];
        u = new User(alias = 'standt1', email='standarduser@testorg.com', 
            emailencodingkey='UTF-8', lastname='AssignEmployees_Testing', 
            languagelocalekey='en_US', 
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles',            
            username='AssignEmployees_standarduser1@testorg.com',
            Alias_unique__c='standarduser_Alias_unique__c',
            Territory_UID__c=t,
            Company_Code_Text__c='EUR',
            Region_vs__c = 'Europe',
            Sub_Region__c = 'NWE',
            Country_vs__c = 'NETHERLANDS');

    System.runAs(u) {
        
    //  Create countries            
        //DIB_Country__c country1 = new DIB_Country__c(Name='Belgium',Country_ISO_Code__c='BE');
        //DIB_Country__c country2 = new DIB_Country__c(Name='Netherlands',Country_ISO_Code__c='NL');
        //DIB_Country__c[] countries = new DIB_Country__c[]{country1, country2};
        //insert countries;
        //Id roleId = [select Id from Roles where name='System Administrator'].Id;
        Id profId = [select Id from Profile where name='System Administrator'].Id;
        
    // Create some users
        User Utest1 = new User();
        Utest1.LastName='Janssen';
        Utest1.Alias='jansw1';
        Utest1.Email='wim.janssen@janssen.nl';
        Utest1.Username='wim.janssen@janssen.nl';
        Utest1.CommunityNickname='jansw1';
        //Utest1.UserRoleId =roleId;
        Utest1.ProfileId=profId;
        Utest1.User_Business_Unit_vs__c='CRHF';
        Utest1.EmailEncodingKey='UTF-8';
        Utest1.LanguageLocaleKey='en_US';
        Utest1.LocaleSidKey='en_US';
        Utest1.TimeZoneSidKey='Europe/Amsterdam';        
        Utest1.Job_Title_vs__c='Sales Rep';
        Utest1.Country_vs__c= 'NETHERLANDS';
        Utest1.Region_vs__c = 'Europe';
        Utest1.Sub_Region__c = 'NWE';
        Utest1.Territory_UID__c=terr1.Territory_UID2__c;
        Utest1.Alias_unique__c='standarduser1_Alias_unique__c';
        Utest1.Company_Code_Text__c = 'EUR';
        insert Utest1;
        
        User Utest2 = new User();
        Utest2.LastName='Smit';
        Utest2.Alias='smith1';
        Utest2.Email='hans.smit@smit.nl';
        Utest2.Username='hans.smit@smit.nl';
        Utest2.CommunityNickname='smith1';
        //Utest2.UserRoleId =roleId;
        Utest2.ProfileId=profId;
        Utest2.User_Business_Unit_vs__c='CRHF';
        Utest2.EmailEncodingKey='UTF-8';
        Utest2.LanguageLocaleKey='en_US';
        Utest2.LocaleSidKey='en_US';
        Utest2.TimeZoneSidKey='Europe/Amsterdam';
        Utest2.Job_Title_vs__c='Sales Rep';
        Utest2.Country_vs__c= 'NETHERLANDS';
        Utest2.Region_vs__c = 'Europe';
        Utest2.Sub_Region__c = 'NWE';
        Utest2.Territory_UID__c=terr1.Territory_UID2__c;
        Utest2.Alias_unique__c='standarduser2_Alias_unique__c';
        Utest2.Company_Code_Text__c = 'EUR';        
        insert Utest2;
    
    
    //  Create one configuration record 
        Territory_Level_Configuration__c tlc = new Territory_Level_Configuration__c();
        tlc.Job_Title_vs__c='Sales Rep';
        tlc.Name='Country8';
        tlc.Territory_Level__c=8;
        tlc.Territory_Abbreviation__c='CN';
        tlc.Message_No_Accounts__c='No Accounts';
        tlc.Message_No_Child_Territories__c='No Child Territories';
        tlc.Message_No_Employees__c='No Employees';
        tlc.Company__c  = comp.id;        
        insert tlc;
        
    
    
    // coverage with no Territory Id in the URL
        ApexPages.StandardController sct1 = new ApexPages.StandardController(terr1); //set up the standardcontroller      
        controllerTerritoryAssignEmployees  controller1 = new controllerTerritoryAssignEmployees(sct1);
    
    
        controller1.SelectEmployee.add(Utest1);
        controller1.SelectedEmployee();
        controller1.AvailableEmployees1(); 
        controller1.RemoveSelectedEmployee();
    
    //   
        ApexPages.currentPage().getParameters().put('TerId', terr1.Id);
        ApexPages.StandardController sct = new ApexPages.StandardController(terr1); //set up the standardcontroller      
    
        controllerTerritoryAssignEmployees  controller = new controllerTerritoryAssignEmployees(sct);
    
    
    
        controller.AssignedButtonPressed = false;
        controller.RemoveFilterPressed = true;
        controller.AvailableEmployees1(); 
    
        controller.getActiveEmployees();
        controller.getAssignEmployees();
        controller.getAvailableEmployees();
        controller.getCountries();
        controller.getCountry();
        controller.getCrumbs();
        controller.GetEmplRecSize();
        controller.getHasNextEmp();
        controller.getHasPreviousEmp();
        controller.getSelectEmployee();
        controller.getTDetail();
        controller.getTest1();
        controller.getTotalPagesEmp();
    
        controller.firstEmp();
        controller.previousEmp();
        controller.nextEmp();
        controller.lastEmp();
        controller.setCountry('NL');
        controller.setActiveEmployees(controller.getActiveEmployees());
        controller.SelectedEmployee();
    
        controller.CancelEmplAssign();
    
        controller.AssignEmployees();
        controller.EmplName='test';
        controller.EmplJobtitle='test';
        controller.EmplCountry='test';
        controller.EmplBusUnit='test';
        controller.AvailableEmployees1();
        controller.RemoveEmployeeFilter();
        
        ApexPages.currentPage().getParameters().put('TerId', terr3.Id);
        ApexPages.StandardController sct2 = new ApexPages.StandardController(terr1); //set up the standardcontroller      
    
        controllerTerritoryAssignEmployees  controller2 = new controllerTerritoryAssignEmployees(sct2);        
     
        controller1.SelectEmployee.add(Utest1);
        controller1.SelectedEmployee();
        controller2.AvailableEmployees1();
        controller2.SelectedEmplAll();
    
    
        controller2.SelEmplId=u.Id;
        controller2.SelectedEmployee();  
        controller2.SelectedEmployee();
        controller2.EmplRecords=null;
        controller2.getEmplRecSize();
            
    }
    

    
    }

    static testMethod void Test_controllerTerritoryAddPage(){
    	
    	Id MDT_Terr_Model = [Select Id from Territory2Model where DeveloperName = 'Medtronic'].Id;
    	
	    PageReference pageRef = Page.TerritoryAddPage;
	    Test.setCurrentPage(pageRef);
	    User u;
	
	    // Get Company
	    Company__c comp = [select id,name from Company__c where name='Europe' limit 1];                                
	    Territory2 terr1 = new Territory2();
	    Territory2 terr2 = new Territory2(); 
	    User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
	    System.runAs ( thisUser ) {
	
	    terr1.Name = 'Territory1';
	    terr1.DeveloperName = 'Territory1';
	    terr1.Short_Description__c = 'Territory1';
	    terr1.Business_Unit__c = 'CRHF';
	    terr1.Territory2TypeId = bl_Territory.territoryTypeMap.get('Region');
	    terr1.Country_UID__c='C1';
	    terr1.Company__c = string.valueOf(comp.id).substring(0,15);
	    terr1.Territory2ModelId = MDT_Terr_Model; 
	    insert terr1;
	    
	    terr2.Name = 'Territory2';
	    terr2.DeveloperName = 'Territory2';
	    terr2.Short_Description__c = 'Territory2';
	    terr2.Business_Unit__c = 'CRHF';    
	    terr2.Country_UID__c='C1';
	    terr2.ParentTerritory2Id= terr1.id;
	    terr2.Territory2TypeId = bl_Territory.territoryTypeMap.get('Territory');
	    terr2.Therapy_Groups_Text__c='CardioInsight';
	    terr2.Company__c = string.valueOf(comp.id).substring(0,15);    
	    terr2.Territory2ModelId = MDT_Terr_Model; 
	    insert terr2;
    
	    Profile p = [select id from profile where name='System Administrator'];
	    string t = [select Territory_UID2__c from Territory2 where id =:terr1.id].Territory_UID2__c;
	       // UserRole r = [Select id from userrole where name='System Administrator'];
	        u = new User(alias = 'standt1', email='standarduser@testorg.com', 
	            emailencodingkey='UTF-8', lastname='AddPage_Testing', 
	            languagelocalekey='en_US', 
	            localesidkey='en_US', profileid = p.Id,
	            timezonesidkey='America/Los_Angeles', 
	            CountryOR__c='Country1',
	            Country= 'Country1',
	            username='AddPage_standarduser1@testorg.com',
	            Alias_unique__c='standarduser_Alias_unique__c',
	            Territory_UID__c=t,
	            Company_Code_Text__c = 'EUR');
	
	    }
        
        
    	System.runAs(u) {
        
            
            ApexPages.StandardController sct = new ApexPages.StandardController(terr1); //set up the standardcontroller      
    
            controllerTerritoryAddPage  controller = new controllerTerritoryAddPage(sct);
            controller.getNewTerritory();
            controller.getTDetail();
        
            ApexPages.currentPage().getParameters().put('Id', terr2.Id);
            ApexPages.StandardController sct1 = new ApexPages.StandardController(terr2); //set up the standardcontroller      
            controllerTerritoryAddPage  controller1 = new controllerTerritoryAddPage(sct1);
            controller1.getNewTerritory();
            controller1.getTDetail();
            controller1.getCrumbs();
            controller1.getTerrGroups();

            controller1.getTerrGroups();
            terr2.Territory2TypeId = bl_Territory.territoryTypeMap.get('District');
            update terr2;
            //controller1.Change();
            
            terr1.Territory2TypeId = bl_Territory.territoryTypeMap.get('District');
            terr1.Country_UID__c='D1';
            update terr1;
            terr2.Territory2TypeId = bl_Territory.territoryTypeMap.get('District');
            terr2.Country_UID__c='D2';            
            update terr2;

            ApexPages.currentPage().getParameters().put('Id', terr2.Id);
            ApexPages.StandardController sct2 = new ApexPages.StandardController(terr2); //set up the standardcontroller      
            controllerTerritoryAddPage  controller2 = new controllerTerritoryAddPage(sct2);
            controller2.getNewTerritory().Short_Description__c = 'Unit Test New Territory 2';
            controller2.Change();

            controller2.ChangeButtonPressed=true;

            controller2.getTerrGroups();            
            ApexPages.currentPage().getParameters().put('Id', terr2.Id);
            ApexPages.StandardController sct3 = new ApexPages.StandardController(terr1); //set up the standardcontroller      
            controllerTerritoryAddPage  controller3 = new controllerTerritoryAddPage(sct3);
            
            terr2.Therapy_Groups_Text__c='CardioInsight';
            update terr2;

            controller3.getTerrGroups();            
		}
	}
// End Test_controllerTerritoryAddPage


    static testMethod void Test_controllerTerritoryEditPage(){
    
    
	    PageReference pageRef = Page.TerritoryEditPage;
	    Test.setCurrentPage(pageRef);
	    User u;
	    
	    // Get Company
	    Company__c comp = [select id,name from Company__c where name='Europe' limit 1];
	                                    
	    DIB_Country__c country1 = new DIB_Country__c(Name='Country1',Country_ISO_Code__c='C1');
	    DIB_Country__c country2 = new DIB_Country__c(Name='Country2',Country_ISO_Code__c='C2');
	    DIB_Country__c[] countries = new DIB_Country__c[]{country1, country2};
	    insert countries;
	        
	    Territory2 terr0 = new Territory2();
	    Territory2 terr1 = new Territory2();
	    Territory2 terr2 = new Territory2();
	    Territory2 terr3 = new Territory2();         
	    
	    User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
	    System.runAs ( thisUser ) {
	
		Id MDT_Terr_Model = [Select Id from Territory2Model where DeveloperName = 'Medtronic'].Id;
		
	    terr0.Name = 'Territory0';
	    terr0.DeveloperName = 'Territory0';
	    terr0.Business_Unit__c = 'CRHF';
	    terr0.Territory2TypeId = bl_Territory.territoryTypeMap.get('District');
	    terr0.Country_UID__c='C1';
	    terr0.Short_Description__c ='test1 description';
	    terr0.Therapy_Groups_Text__c='CardioInsight';
	    terr0.Company__c = string.valueOf(comp.id).substring(0,15);   
	    terr0.Territory2ModelId = MDT_Terr_Model;      
	    insert terr0;
	
	    terr1.Name = 'Territory1';
	    terr1.DeveloperName = 'Territory1';
	    terr1.Business_Unit__c = 'CRHF';
	    terr1.Territory2TypeId = bl_Territory.territoryTypeMap.get('District');
	    terr1.Country_UID__c='C1';
	    terr1.Short_Description__c ='test1 description';
	    terr1.Therapy_Groups_Text__c='CardioInsight';
	    terr1.ParentTerritory2Id= terr0.id;
	    terr1.Company__c = string.valueOf(comp.id).substring(0,15);   
	    terr1.Territory2ModelId = MDT_Terr_Model;             
	    insert terr1;
	    
	    terr2.Name = 'Territory2';
	    terr2.DeveloperName = 'Territory2';
	    terr2.Business_Unit__c = 'CRHF';
	    terr2.Territory2TypeId = bl_Territory.territoryTypeMap.get('Region');
	    terr2.Country_UID__c='C1';
	    terr2.ParentTerritory2Id= terr1.id;
	    terr2.Short_Description__c ='test2 description';
	    terr2.Therapy_Groups_Text__c='CardioInsight';
	    terr2.Company__c = string.valueOf(comp.id).substring(0,15);     
	    terr2.Territory2ModelId = MDT_Terr_Model;                       
	    insert terr2;
	    
	    terr3.Name = 'Territory3';
	    terr3.DeveloperName = 'Territory3';
	    terr3.Business_Unit__c = 'CRHF';
	    terr3.Territory2TypeId = bl_Territory.territoryTypeMap.get('Territory');
	    terr3.Country_UID__c='C1';
	    terr3.ParentTerritory2Id= terr0.id;
	    terr3.Short_Description__c ='test2 description';
	    terr3.Therapy_Groups_Text__c='CardioInsight';
	    terr3.Company__c = string.valueOf(comp.id).substring(0,15);       
	    terr3.Territory2ModelId = MDT_Terr_Model;                     
	    insert terr3;
	    
	    Profile p = [select id from profile where name='System Administrator'];
	    string t = [select Territory_UID2__c from Territory2 where id =:terr1.id].Territory_UID2__c;
	       // UserRole r = [Select id from userrole where name='System Administrator'];
	        u = new User(alias = 'standt1', email='standarduser@testorg.com', 
	            emailencodingkey='UTF-8', lastname='EditPage_Testing', 
	            languagelocalekey='en_US', 
	            localesidkey='en_US', profileid = p.Id,
	            timezonesidkey='America/Los_Angeles', 
	            CountryOR__c='Country1',
	            Country= 'Country1',
	            username='EditPage_standarduser1@testorg.com',
	            Alias_unique__c='standarduser_Alias_unique__c',
	            Territory_UID__c=t,
	            Company_Code_Text__c = 'EUR');
	
	    }
        
        
    	System.runAs(u) {
        
            
            ApexPages.StandardController sct = new ApexPages.StandardController(terr1); //set up the standardcontroller      
    
            controllerTerritoryEditPage  controller = new controllerTerritoryEditPage(sct);
            controller.getCrumbs();
            controller.getTDetail();
    
            ApexPages.currentPage().getParameters().put('Id', terr2.Id);
            ApexPages.StandardController sct1 = new ApexPages.StandardController(terr1); //set up the standardcontroller      
            controllerTerritoryEditPage  controller1 = new controllerTerritoryEditPage(sct1);

            controller1.getTDetail();
            controller1.getCrumbs();
            controller1.getTerrGroups();

            controller1.getTerrGroups();
            controller1.selectedDistrict='Territory0';
            controller1.Change();
            
            ApexPages.currentPage().getParameters().put('Id', terr2.Id);
            ApexPages.StandardController sct2 = new ApexPages.StandardController(terr1); //set up the standardcontroller      
            controllerTerritoryEditPage  controller2 = new controllerTerritoryEditPage(sct2);



            controller2.ChangeButtonPressed=true;            

            terr2.Territory2TypeId = bl_Territory.territoryTypeMap.get('Territory');
            terr2.Therapy_Groups_Text__c='CardioInsight';
            update terr2;
            controller2.getDistricts();
    
		}
	}
// End Test TerritoryEditPage

    static testMethod void Test_controllerTerritoryDelete(){
	    
	    PageReference pageRef = Page.confirmPage;
	    Test.setCurrentPage(pageRef);
	    User u;
	
	    // Get Company
	    Company__c comp = [select id,name from Company__c where name='Europe' limit 1];                                
	    DIB_Country__c country1 = new DIB_Country__c(Name='Country1',Country_ISO_Code__c='C1');
	    DIB_Country__c country2 = new DIB_Country__c(Name='Country2',Country_ISO_Code__c='C2');
	    DIB_Country__c[] countries = new DIB_Country__c[]{country1, country2};
	    insert countries;
	        
	    Territory2 terr1 = new Territory2();
	    Territory2 terr2 = new Territory2(); 
	    Territory2 terr3 = new Territory2();       
	    User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
	    
	    Id MDT_Terr_Model = [Select Id from Territory2Model where DeveloperName = 'Medtronic'].Id;
	    
	    System.runAs ( thisUser ) {
	
	    terr1.Name = 'Territory1';
	    terr1.DeveloperName = 'Territory1';
	    terr1.Business_Unit__c = 'CRHF';
	    terr1.Territory2TypeId = bl_Territory.territoryTypeMap.get('District');
	    terr1.Country_UID__c='C1';
	    terr1.Short_Description__c ='test description';
	    terr1.Company__c = string.valueOf(comp.id).substring(0,15);    
	    terr1.Territory2ModelId = MDT_Terr_Model;   
	    insert terr1;
	    
	    terr2.Name = 'Territory2';
	    terr2.DeveloperName = 'Territory2';
	    terr2.Business_Unit__c = 'CRHF';
	    terr2.Territory2TypeId = bl_Territory.territoryTypeMap.get('Country');
	    terr2.Country_UID__c='C1';
	    terr2.ParentTerritory2Id= terr1.id;
	    terr2.Short_Description__c ='test description';
	    terr2.Company__c = string.valueOf(comp.id).substring(0,15);    
	    terr2.Territory2ModelId = MDT_Terr_Model;   
	    insert terr2;
	    
	    terr3.Name = 'Territory3';
	    terr3.DeveloperName = 'Territory3';
	    terr3.Business_Unit__c = 'CRHF';
	    terr3.Territory2TypeId = bl_Territory.territoryTypeMap.get('Territory');
	    terr3.Country_UID__c='C1';
	    terr3.ParentTerritory2Id= terr1.id;
	    terr3.Short_Description__c ='test2 description';
	    terr3.Therapy_Groups_Text__c='CardioInsight';
	    terr3.Company__c = string.valueOf(comp.id).substring(0,15);       
	    terr3.Territory2ModelId = MDT_Terr_Model;    
	    insert terr3;
	
	
	    Profile p = [select id from profile where name='System Administrator'];
	    string t = [select Territory_UID2__c from Territory2 where id =:terr1.id].Territory_UID2__c;
	       // UserRole r = [Select id from userrole where name='System Administrator'];
	        u = new User(alias = 'standt1', email='standarduser@testorg.com', 
	            emailencodingkey='UTF-8', lastname='TerritoryDelete_Testing', 
	            languagelocalekey='en_US', 
	            localesidkey='en_US', profileid = p.Id,
	            timezonesidkey='America/Los_Angeles', 
	            CountryOR__c='Country1',
	            Country= 'Country1',
	            username='TerritoryDelete_standarduser1@testorg.com',
	            Alias_unique__c='TerritoryDelete_standarduser_Alias_unique__c',
	            Territory_UID__c=t, Company_Code_Text__c='EUR');
	
	    }
        
        
   	 	System.runAs(u) {
        
            
            ApexPages.StandardController sct = new ApexPages.StandardController(terr2); //set up the standardcontroller      
    
            controllerTerritoryDelete  controller = new controllerTerritoryDelete(sct);
            controller.TerritoryDelete();
            controller.getWarntext();
            controller.Cancel();
            controller.DeleteTerritory();
    
            ApexPages.currentPage().getParameters().put('Id', terr1.Id);
            ApexPages.StandardController sct1 = new ApexPages.StandardController(terr1); //set up the standardcontroller      
            controllerTerritoryDelete  controller1 = new controllerTerritoryDelete(sct1);
            controller.TerritoryDelete();

		}
	}
}