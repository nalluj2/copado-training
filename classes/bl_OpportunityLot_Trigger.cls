//--------------------------------------------------------------------------------------------------------------------------------
//  Author      :   Bart Caelen
//  Date        :   10/08/2018
//  Description :   APEX TEST Class for the APEX Trigger tr_OpportunityLot and APEX Class bl_OpportunityLot_Trigger
//  Change Log  :    
//--------------------------------------------------------------------------------------------------------------------------------
public class bl_OpportunityLot_Trigger{
	
	//----------------------------------------------------------------------------------------------------------------------------
	// Calculate the "Roll-up Summary Fields" on Opportunity based on the related Opportunity Lot Records
	//----------------------------------------------------------------------------------------------------------------------------
	public static void calculateRLSFieldOnOpportunity(List<Opportunity_Lot__c> lstTriggerNew, Map<Id, Opportunity_Lot__c> mapTriggerOld){

		if (bl_Trigger_Deactivation.isTriggerDeactivated('opplot_calculateRLSFieldOnOpportunity')) return;

		Set<Id> setID_RecordType = new Set<Id>{clsUtil.getRecordTypeByDevName('Opportunity', 'Business_Critical_Tender').Id};

		// Mapping between the Opportunity Lof Field and the Opportunity Field
		Map<String, String> mapFieldName = new Map<String, String>{
			'Estimated_Revenue__c'=>'Estimated_Revenue_Won_Text__c'
			, 'Current_Potential__c'=>'Current_Potential_Text__c'
			, 'Full_Potential__c'=>'Full_Potential_Text__c'
		};

		// Get the RecordType of the related Opportunity
		Set<Id> setID_Opportunity_All = new Set<Id>();
		for (Opportunity_Lot__c oOpportunityLot : lstTriggerNew){

			setID_Opportunity_All.add(oOpportunityLot.Opportunity__c);

		}
		Map<Id, Opportunity> mapOpportunity = new Map<Id, Opportunity>([SELECT Id, RecordTypeId, CurrencyIsoCode FROM Opportunity WHERE Id = :setID_Opportunity_All]);

		// Validate if the RLS Fields on Opportunity need to be recalculated
		//	Needed when a RLS Field is populated on an INSERT, DELETE or UNDELETE
		//	Needed when a RLS Field is changed on an UPDATE
		Set<Id> setID_Opportunity = new Set<Id>();
		for (Opportunity_Lot__c oOpportunityLot : lstTriggerNew){
		
			if (!setID_RecordType.contains(mapOpportunity.get(oOpportunityLot.Opportunity__c).RecordTypeId)) continue;

			for (String tFieldName : mapFieldName.keySet()){
				
				if (mapTriggerOld != null){

					// UPDATE
					if ( oOpportunityLot.get(tFieldName) != mapTriggerOld.get(oOpportunityLot.Id).get(tFieldName) ){
						setID_Opportunity.add(oOpportunityLot.Opportunity__c);	
						break; // no need to check the other RLS fields - they will all be recalculated
					}

				}else{
					
					// INSERT - DELETE - UNDELETE
					if (oOpportunityLot.get(tFieldName) != null){
						setID_Opportunity.add(oOpportunityLot.Opportunity__c);	
						break; // no need to check the other RLS fields - they will all be recalculated
					}
					
				}

			}
			
		}

		if (setID_Opportunity.size() == 0) return;

		// Execute the Aggregated SOQL for the collected Opportunities - SUM of currency fields are always calculated in the default CurrencyISOCode of the org
		String tSOQL = 'SELECT Opportunity__c';
		for (String tFieldName : mapFieldName.keySet()){
			tSOQL += ', SUM(' + tFieldName + ') ' + tFieldName;
		}
		tSOQL += ' FROM Opportunity_Lot__c';
		tSOQL += ' WHERE Opportunity__c in :setID_Opportunity';
		tSOQL += ' GROUP BY Opportunity__c';
		List<AggregateResult> lstAggregateResult = Database.query(tSOQL);

		List<Opportunity> lstOpportunity_Update = new List<Opportunity>();
		Set<Id> setID_Opportunity_Processed = new Set<Id>();
		for (AggregateResult oAggregateResult : lstAggregateResult){
			
			Opportunity oOpportunity = new Opportunity(Id = String.valueOf(oAggregateResult.get('Opportunity__c')));

			for (String tFieldName : mapFieldName.keySet()){
				// Convert the calculated value into the correct CurrencyISOCode
				Decimal decValue = (Decimal)oAggregateResult.get(tFieldName);
				if(decValue != null){
					
					CurrencyType oCurrencyType = bl_Currency.getCurrencyType(mapOpportunity.get(oOpportunity.Id).CurrencyIsoCode);
					decValue = decValue * oCurrencyType.ConversionRate;
				}
				
				oOpportunity.put(mapFieldName.get(tFieldName), decValue);
			}

			lstOpportunity_Update.add(oOpportunity);
			setID_Opportunity_Processed.add(oOpportunity.Id);
		
		}

		// Set the RLS fields to 0 if there are no Opportunity Lot Records anymore
		for (Id id_Opportunity : setID_Opportunity){
		
			if (setID_Opportunity_Processed.contains(id_Opportunity)) continue; // Record Already Processed

			Opportunity oOpportunity = new Opportunity(Id = id_Opportunity);
			for (String tFieldName : mapFieldName.keySet()){
				oOpportunity.put(mapFieldName.get(tFieldName), 0);
			}

			lstOpportunity_Update.add(oOpportunity);

		}

		if (lstOpportunity_Update.size() > 0) update lstOpportunity_Update;
	
	}
	//----------------------------------------------------------------------------------------------------------------------------

}
//--------------------------------------------------------------------------------------------------------------------------------