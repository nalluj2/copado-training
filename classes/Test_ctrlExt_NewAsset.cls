/*  Test Class Name - Test_ctrlExt_NewAsset
    Description  –  The class is written to cover 'ctrlExt_NewAsset'.
    Author - Rudy De Coninck
    Created Date  - 29/08/2013 
    Update by Peng Han in 22/05/2014  
*/   
                    
@isTest
public with sharing class Test_ctrlExt_NewAsset {

	static testMethod void assetPageSelection() {
		
        User testUser = clsTestData_User.createUser('test.user.001@medtronic.com.test', 'tu001', 'NETHERLANDS', clsUtil.getUserProfileId('EUR Field Force RTG'), clsUtil.getUserRoleId('EUR xBU'), true);

        System.runAs(testUser){
			
			Id mktProdRTId = clsUtil.getRecordTypeByDevName('Product2', 'MDT_Marketing_Product').Id;
			
			Product2 prd = new Product2(Name = 'Unit Test Product', RecordTypeId = mktProdRTId);
            insert prd;
			
			Asset a = new Asset();
			a.name = 'test asset';			
			a.serial_nr__c = '123456';
			
			Account acc = new Account();
			acc.name='test account new asset';
			insert acc;
			
			Department_Info__c di = new Department_Info__c();
			di.Account__c = acc.id;
			insert di;
			
			ApexPages.StandardController sct = new ApexPages.StandardController(a);
	        ctlrExt_NewAsset controller = new ctlrExt_NewAsset(sct);
	        controller.redirectToUser();
	        controller.getProducts();
	        controller.asset.name='test';
	        controller.dept = di;
	        controller.selectedProductName = prd.id;
	        controller.saveAsset();
	        controller.saveAndNewAsset();
        }
	}
	
	static testMethod void assetEdit() {
		
        User testUser = clsTestData_User.createUser('test.user.001@medtronic.com.test', 'tu001', 'NETHERLANDS', clsUtil.getUserProfileId('EUR Field Force RTG'), clsUtil.getUserRoleId('EUR xBU'), true);
        
        System.runAs(testUser){
			
			Id mktProdRTId = clsUtil.getRecordTypeByDevName('Product2', 'MDT_Marketing_Product').Id;
			
			Product2 prd = new Product2(Name = 'Unit Test Product', RecordTypeId = mktProdRTId);
            insert prd;
									
			Account acc = new Account();
			acc.name='test account new asset';
			insert acc;
			
			Department_Info__c di = new Department_Info__c();
			di.Account__c = acc.id;
			insert di;
			
			Asset a = new Asset();
			a.name = 'test asset';			
			a.serial_nr__c = '123456';
			a.AccountId = acc.Id;
			a.Product2Id = prd.Id;
			a.Department_Info__c = di.Id;
			insert a;
			
			ApexPages.currentPage().getParameters().put('id', a.Id);
			ApexPages.StandardController sct = new ApexPages.StandardController(a);
	        ctlrExt_NewAsset controller = new ctlrExt_NewAsset(sct);
	        controller.redirectToUser();	        
        }
	}
}