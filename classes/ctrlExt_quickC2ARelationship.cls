public with sharing class ctrlExt_quickC2ARelationship {
	
	public Affiliation__c relationship {get; set;}
	
	public List<SelectOption> jobTitleOptions {get; set;}
	public List<SelectOption> departmentOptions {get; set;}	
	public List<SelectOption> buOptions {get; set;}
	public List<SelectOption> therapyOptions {get; set;}
		
	public Boolean isBUMandatory {get; set;}
	public Boolean isTherapyVisible {get; set;}	
		
	public Boolean isSaveError {get; set;}
	
	public ctrlExt_quickC2ARelationship(ApexPages.StandardController sc){
		
		User currentUser = [Select Id, Name, CountryOR__c from User where Id =: UserInfo.getUserId()];
				
		relationship = new Affiliation__c();
		
		Id inputId = sc.getId();
		
		if(inputId.getSObjectType().getDescribe().getName() == 'Contact'){
			relationship.Affiliation_From_Contact__c = inputId;
		}else{
			relationship.Affiliation_To_Account__c = inputId;
		}
				
		relationship.Affiliation_Start_Date__c = Date.today();
		
		RecordType rt = [Select Id from RecordType where SObjectType = 'Affiliation__c' AND DeveloperName = 'C2A'];
		relationship.RecordTypeId = rt.Id;	
		
		loadJobTitles();
		loadDepartments();	
				
		isBUMandatory = !bl_Relationship.isBusinessUnitNotMandatory(currentUser.CountryOR__c);	
		isTherapyVisible = !bl_Relationship.hideTherapy(currentUser.CountryOR__c);
	}
	
	public void onRelTypeChange(){
		
		if(relationship.Affiliation_Type__c == 'Referring with details'){
			loadBUs();
			if(Schema.sObjectType.Affiliation__c.fields.Therapy__c.isCreateable() && isTherapyVisible) onBUChange();
		}else{
			relationship.Business_Unit__c = null;
			relationship.Therapy__c = null;
		}		
	}
	
	public void onBUChange(){
		
		relationship.Therapy__c = null;
		loadTherapies();
	}
	
	private void loadBUs() {
        
        buOptions = new List<SelectOption>();   
        relationship.Business_Unit__c = null;
        
        // Check User BU
        Map<Id, String> userBUs = SharedMethods.getUserBusinessUnits();
        
        buOptions.add(new SelectOption('','--None--'));  
    
        if(userBUs.size()>0){
                        
            for(Id BuId : userBUs.keySet()){
                buOptions.add(new SelectOption(BuId,UserBUs.get(BuId)));                
            }
            
            Id IdUserPrimaryBU = SharedMethods.getUserPrimaryBU(); 
            
            if(IdUserPrimaryBU!=null){
            	relationship.Business_Unit__c = IdUserPrimaryBU;  
            }                       
        }
        
        SelectOptionSorter.doSort(BUOptions, SelectOptionSorter.FieldToSort.Label);   
    }
    
    private void loadTherapies() {
               
        therapyOptions = SharedMethods.getTherapies(relationship.Business_Unit__c);
        
        // If only one therapy, set it by default ! 
        if (therapyOptions.size() == 2){
            relationship.Therapy__c = therapyOptions[1].getValue() ; 
        }           
    }
    
    private void loadJobTitles(){

		jobTitleOptions = new List<SelectOption>();

       	Map<Id, String> userCompany = SharedMethods.getUserCompany();
    	Set<Id> companyId = UserCompany.keyset();

        List<Company_Related_Setup__c> lstJobTitles = [select Value_Text__c from Company_Related_Setup__c where  
            Object_Name_Text__c='Affiliation__c' and Field_Name_Text__c='Affiliation_Position_In_Account__c' and  Active_Checkbox__c=:true 
            and Master_Data_MDRel__c IN:companyId order by SeqOrder_Number__c];
        
        jobTitleOptions.add(new SelectOption('','--None--'));
        
        for (Company_Related_Setup__c JobTitle:lstJobTitles) {
            jobTitleOptions.add(new SelectOption(JobTitle.Value_Text__c,JobTitle.Value_Text__c)); 
        }                     
    }

    private void loadDepartments(){
	
		departmentOptions = new List<SelectOption>();
       	
       	Map<Id, String> userCompany = SharedMethods.getUserCompany();
       	Set<Id> companyId = userCompany.keyset();

        List<Company_Related_Setup__c> lstDepartment = [select Value_Text__c from Company_Related_Setup__c where  
            Object_Name_Text__c='Affiliation__c' and Field_Name_Text__c='Department__c' and  Active_Checkbox__c=:true 
            and Master_Data_MDRel__c in:CompanyId order by SeqOrder_Number__c];
        
        departmentOptions.add(new SelectOption('','--None--'));
        
        for (Company_Related_Setup__c Department:lstDepartment) {
            departmentOptions.add(new SelectOption(Department.Value_Text__c,Department.Value_Text__c)); 
        }       
    } 
    
    public void save(){
		
		isSaveError = false;
		
		if(relationship.Affiliation_Start_Date__c == null){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Start Date is mandatory'));
			isSaveError = true;
		}
		
		if(relationship.Affiliation_Type__c == null){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Relationship Type is mandatory'));
			isSaveError = true;
		}
		
		if(relationship.Affiliation_From_Contact__c == null || String.valueOf(relationship.Affiliation_From_Contact__c) == ''){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'From Contact is mandatory'));
			isSaveError = true;
		}
			
		if(relationship.Affiliation_To_Account__c == null || String.valueOf(relationship.Affiliation_To_Account__c) == ''){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'To/Parent Account is mandatory'));
			isSaveError = true;
		}
		
		if(relationship.Affiliation_Type__c == 'Referring with details' && isBUMandatory == true && (relationship.Business_Unit__c == null || String.valueOf(relationship.Business_Unit__c) == '')){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Business Unit is mandatory'));
			isSaveError = true;
		}
		
		if(relationship.Affiliation_Type__c == 'Referring with details' && Schema.sObjectType.Affiliation__c.fields.Therapy__c.isCreateable() && isTherapyVisible == true && (relationship.Therapy__c == null || String.valueOf(relationship.Therapy__c) == '')){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Therapy is mandatory'));
			isSaveError = true;
		}	
		
		if(isSaveError == false){                        
            
            String tSOQL = 'SELECT id FROM Affiliation__c WHERE Affiliation_From_Contact__c = \'' + relationship.Affiliation_From_Contact__c + '\'';
            tSOQL += ' and Affiliation_To_Account__c = \'' + relationship.Affiliation_To_Account__c + '\'';
            tSOQL += ' and RecordTypeId = \'' + relationship.RecordTypeId + '\'';
            
            if (relationship.Affiliation_Type__c == 'Referring with details' && Schema.sObjectType.Affiliation__c.fields.Therapy__c.isCreateable()  && isTherapyVisible == true){
                tSOQL += ' and Therapy__c = \'' + relationship.Therapy__c + '\'';
            }
            
            tSOQL += ' LIMIT 1';    
            
            List<Affiliation__c> lstAffiliation = Database.Query(tSOQL);
            
            if (lstAffiliation.size()>0){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, Label.This_Relationship_is_already_existing));
                isSaveError = true;                
            }
		}
		
		if(isSaveError) return;
		
		try{
		
			insert relationship;
			
		}catch(Exception e){
			ApexPages.addMessages(e);
			isSaveError = true;
		}
	}
}