public class bl_Procedural_Solutions_Kit {
        
    public static void setCountry(List<Procedural_Solutions_Kit__c> triggerNew){
    	
    	Set<Id> accountIds = new Set<Id>();
    	
    	for(Procedural_Solutions_Kit__c box : triggerNew){
    		
    		if(box.Country__c == null && box.Account__c != null) accountIds.add(box.Account__c);    			
    	}
    	
    	if(accountIds.size() > 0){
    		
    		Map<Id, Account> accounts = new Map<Id, Account>([Select Id, Account_Country_vs__c from Account where Id IN :accountIds]);
    		
    		for(Procedural_Solutions_Kit__c box : triggerNew){
    		
    			if(box.Country__c == null && box.Account__c != null){
    				
    				Account acc = accounts.get(box.Account__c);
    				box.Country__c = acc.Account_Country_vs__c.toUpperCase();
    			}    			
    		}
    	}
    }
    
    public static void checkVersionOf(List<Procedural_Solutions_Kit__c> triggerNew){
    	
    	Set<Id> originalBoxIds = new Set<Id>();
    	
    	for(Procedural_Solutions_Kit__c box : triggerNew){
    		
    		if(box.Version_of__c != null) originalBoxIds.add(box.Version_of__c);    			
    	}
    	
    	if(originalBoxIds.size() > 0){
    		
    		Map<Id, Procedural_Solutions_Kit__c> originals = new Map<Id, Procedural_Solutions_Kit__c>([Select Id, Version_of__c from Procedural_Solutions_Kit__c where Id IN :originalBoxIds]);
    		
    		for(Procedural_Solutions_Kit__c box : triggerNew){
    		
    			if(box.Version_of__c != null){
    				
    				Procedural_Solutions_Kit__c original = originals.get(box.Version_of__c);
    				
    				if(original.Version_of__c != null){
    					
    					box.Version_of__c = original.Version_of__c;
    				}
    			}    			
    		}
    	}
    }
    
    
    /*
    public static void generateConfigurationHash(List<Procedural_Solutions_Kit__c> triggerNew){
    	
    	List<Procedural_Solutions_Kit__c> submittedBoxes = new List<Procedural_Solutions_Kit__c>();
    	
    	for(Procedural_Solutions_Kit__c box : triggerNew){
    		
    		if(box.Status__c != 'Open') submittedBoxes.add(box);
    	}
    	
    	if(submittedBoxes.size() > 0){
    		
    		Map<Id, Procedural_Solutions_Kit__c> boxProducts = new Map<Id, Procedural_Solutions_Kit__c>([Select Id, Country__c, (Select Id, Product__c, Quantity__c from Procedural_Solutions_Kit_Products__r ORDER BY Product__c ASC) from Procedural_Solutions_Kit__c where Id IN :submittedBoxes]);
    		
    		for(Procedural_Solutions_Kit__c box : submittedBoxes){
    			
    			Procedural_Solutions_Kit__c boxConfig = boxProducts.get(box.Id);
    			
    			if(boxConfig != null && boxConfig.Procedural_Solutions_Kit_Products__r.size() > 0){
    				
    				List<String> config = new List<String>();
    				
    				for(Procedural_Solutions_Kit_Product__c boxProd : boxConfig.Procedural_Solutions_Kit_Products__r){
    					
    					config.add(boxProd.Product__c + ':' + boxProd.Quantity__c);
    				}
    				
    				String globalConfigString = String.join(config, ';');
    				String configString = box.Country__c + '-' + globalConfigString;
    				
    				box.Configuration_Hash_Global__c = EncodingUtil.convertToHex(Crypto.generateDigest('SHA-256', Blob.valueOf(globalConfigString)));
    				box.Configuration_Hash__c = EncodingUtil.convertToHex(Crypto.generateDigest('SHA-256', Blob.valueOf(configString)));
    				
    			}else{
    				
    				box.Configuration_Hash_Global__c = null;
    				box.Configuration_Hash__c = null;
    			}
    		}
    	}
    }
    
    public static void calculateSameConfigBoxes(List<Procedural_Solutions_Kit__c> triggerNew, Map<Id, Procedural_Solutions_Kit__c> oldMap){
    	
    	List<Procedural_Solutions_Kit__c> submittedBoxes = new List<Procedural_Solutions_Kit__c>();
    	Map<String, Integer> submittedConfigurations = new Map<String, Integer>();
    	
    	for(Procedural_Solutions_Kit__c box : triggerNew){
    		
    		// We calculate only in the moment when a Box goes from Open to any of the subsequent submitted statuses
    		if(box.Status__c != 'Open' && oldMap.get(box.Id).Status__c == 'Open' && box.Configuration_Hash__c != null){
    			
    			submittedBoxes.add(box);
    			
    			submittedConfigurations.put(box.Configuration_Hash__c, 0);    			
    		}
    	}
    	
    	if(submittedBoxes.size() > 0){
    		
    		for(AggregateResult existingConfigs : [Select count(Id) sum, Configuration_Hash__c from Procedural_Solutions_Kit__c 
    													where Configuration_Hash__c IN :submittedConfigurations.keySet() AND Id NOT IN :submittedBoxes GROUP BY Configuration_Hash__c]){
    			
    			String hash = String.valueOf(existingConfigs.get('Configuration_Hash__c'));
    			
    			submittedConfigurations.put(hash, Integer.valueOf(existingConfigs.get('sum')));
    		}
    		
    		for(Procedural_Solutions_Kit__c box : submittedBoxes){
    			
    			Integer prevSubmittedConfig = submittedConfigurations.get(box.Configuration_Hash__c);
    			box.Boxes_with_Same_Config__c = prevSubmittedConfig;
    		}
    	}
    }
    */
    
    public static void setCalculatedName(List<Procedural_Solutions_Kit__c> triggerNew){
    	
    	Set<Id> originals = new Set<Id>();
    	
    	for(Procedural_Solutions_Kit__c box : triggerNew){
    		
    		if(box.Version_of__c != null) originals.add(box.Version_of__c);
    	}
    	
    	Map<Id, Procedural_Solutions_Kit__c> originalMap = new Map<Id, Procedural_Solutions_Kit__c>();
    	Map<Id, Integer> originalsLastVersion = new Map<Id, Integer>();
    	
    	if(originals.size() > 0){
    		
    		for(Procedural_Solutions_Kit__c original : [Select Id, Name, (Select Id, Name from Versions__r where Id NOT IN :triggerNew) from Procedural_Solutions_Kit__c where Id IN :originals]){
    			
    			Integer lastVersion = 1;
    			
    			for(Procedural_Solutions_Kit__c version : original.Versions__r){
    				
    				Integer versionNumber = Integer.valueOf(version.Name.subStringAfter('V'));
    				if(versionNumber > lastVersion) lastVersion = versionNumber;
    			}
    			
    			originalsLastVersion.put(original.Id, lastVersion);
    			originalMap.put(original.Id, original);
    		}    		
    	}
    	
    	List<Procedural_Solutions_Kit__c> toUpdate = new List<Procedural_Solutions_Kit__c>();
    	
    	for(Procedural_Solutions_Kit__c box : triggerNew){
    		
    		Procedural_Solutions_Kit__c boxCopy = new Procedural_Solutions_Kit__c(Id = box.Id);
    		toUpdate.add(boxCopy);
    		
    		if(box.Version_of__c != null){
    			
    			Procedural_Solutions_Kit__c original = originalMap.get(box.Version_of__c);
    			
    			String originalName = original.Name;
    			if(originalName.contains('V')) originalName = originalName.subStringBefore('V');
    			
    			Integer lastVersion = originalsLastVersion.get(box.Version_of__c);
    			
    			boxCopy.Name__c = originalName + 'V' + ++lastVersion; 
    			
    			originalsLastVersion.put(original.Id, lastVersion);
    			
    		}else{
    			
    			boxCopy.Name__c = box.Name_Number_Generator__c + 'V1'; 
    		}
    		
    		boxCopy.Name = boxCopy.Name__c;
    	}
    	
    	update toUpdate;
    }
    
    
    public static void sendSubmissionEmails(List<Procedural_Solutions_Kit__c> triggerNew, Map<Id, Procedural_Solutions_Kit__c> oldMap){
    	
    	List<Id> requestedBoxes = new List<Id>();
    	
    	for(Procedural_Solutions_Kit__c box : triggerNew){
    		
    		if(box.Status__c == 'Approval Pending' && (oldMap == null || oldMap.get(box.Id).Status__c != 'Approval Pending')){
    			
    			requestedBoxes.add(box.Id);
    		}    		
    	}
    	
    	if(requestedBoxes.size() > 0){
    		
    		sendSubmissionEmailsFuture(requestedBoxes);
    	}    	 
    }
   	
   	private static List<String> PS_EMEA_Emails {
   		
   		get{
   			if(PS_EMEA_Emails == null){
   				
   				PS_EMEA_Emails = new List<String>();
				for(Procedural_Solutions_Kit_Email__c PS_Email_setting : Procedural_Solutions_Kit_Email__c.getall().values()) PS_EMEA_Emails.add(PS_Email_setting.email__c);
   			}
   			
   			return PS_EMEA_Emails;
   		}
   		
   		set;
   	}
   	
   	private static Set<Id> alreadyProcessed = new Set<Id>();
    
    public static void sendReOpenEmails(List<Procedural_Solutions_Kit__c> triggerNew, Map<Id, Procedural_Solutions_Kit__c> oldMap){
    	
    	List<Procedural_Solutions_Kit__c> reopenedBoxes = new List<Procedural_Solutions_Kit__c>();
    	
    	for(Procedural_Solutions_Kit__c box : triggerNew){
    		
    		if(alreadyProcessed.add(box.Id)){
    		
    			if(box.Status__c == 'Open' && oldMap.get(box.Id).Status__c == 'Approval Pending' && box.Rejection_Reason__c == null){
    			
    				reopenedBoxes.add(box);
    			}    		
    		}
    	}
    	
    	if(reopenedBoxes.size() > 0){
    			
			List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
    		
    		for(Procedural_Solutions_Kit__c box : reopenedBoxes){
    			
    			Messaging.SingleEmailMessage PSEMEA_email = new Messaging.SingleEmailMessage();    			    			
				PSEMEA_email.setSubject('Submission for Box ' + box.Name + (box.Customer_Reference__c != null ? ' / ' + box.Customer_Reference__c : ''));
	    		PSEMEA_email.setHtmlBody('Submission for Box ' + box.Name + (box.Customer_Reference__c != null ? ' / ' + box.Customer_Reference__c : '') + ' has been recalled.');
	    		PSEMEA_email.setToAddresses(PS_EMEA_Emails); 
	    		PSEMEA_email.setSaveAsActivity(false);	    		   			
	    		emails.add(PSEMEA_email);	
    		}
    		
    		Messaging.sendEmail(emails);
    	} 
    }
    
    public static void sendRejectionEmails(List<Procedural_Solutions_Kit__c> triggerNew, Map<Id, Procedural_Solutions_Kit__c> oldMap){
    	
    	List<Procedural_Solutions_Kit__c> rejectedBoxes = new List<Procedural_Solutions_Kit__c>();
    	
    	for(Procedural_Solutions_Kit__c box : triggerNew){
    		
    		if(box.Status__c == 'Open' && oldMap.get(box.Id).Status__c == 'Approval Pending' && box.Rejection_Reason__c != null){
    			
    			rejectedBoxes.add(box);
    		}
    	}
    	
    	if(rejectedBoxes.size() > 0){
    			
			List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
			List<Procedural_Solutions_Kit__c> toUpdate = new List<Procedural_Solutions_Kit__c>();
    		
    		for(Procedural_Solutions_Kit__c box : rejectedBoxes){
    			
    			Messaging.SingleEmailMessage PSEMEA_email = new Messaging.SingleEmailMessage();    			    			
				PSEMEA_email.setSubject('Your Box ' + box.Name + ' has been rejected');
	    		
	    		String body = 'Dear,<br/><br/>';
	    		body += 'Your Box with ID \'' + box.Name + '\' and description \'' + box.Box_Description__c + '\' has been rejected for the following reason:<br/><br/>';  
	    		body += box.Rejection_Reason__c + ' <br/><br/>'; 
				body += 'Please take the needed actions and re-submit your box for its revision.<br/><br/>Regards.';
	    		
	    		PSEMEA_email.setHtmlBody(body);
	    		PSEMEA_email.setTargetObjectId(box.ownerId); 
	    		PSEMEA_email.setSaveAsActivity(false);	    		   			
	    		emails.add(PSEMEA_email);	
	    			    		
	    		Procedural_Solutions_Kit__c boxCopy = new Procedural_Solutions_Kit__c();
	    		boxCopy.Id = box.Id;
	    		boxCopy.Rejection_Reason__c = null;
	    			    		
	    		toUpdate.add(boxCopy);
    		}
    		
    		Messaging.sendEmail(emails);
    		update toUpdate;
    	} 
    }
    
    public static void blockReOpenAfterApproved(List<Procedural_Solutions_Kit__c> triggerNew, Map<Id, Procedural_Solutions_Kit__c> oldMap){
    	
    	if(ProfileMedtronic.isSystemAdministratorProfile(UserInfo.getProfileId()) == false && PermissionSetMedtronic.hasPermissionSet('Box_Builder_Admin') == false){
    	
	    	for(Procedural_Solutions_Kit__c box : triggerNew){
	    		
	    		Procedural_Solutions_Kit__c oldBox = oldMap.get(box.Id);
	    		
	    		if(box.Status__c == 'Open' && (oldBox.Status__c == 'Approved' || oldBox.Status__c == 'Available') ){
	    			
	    			box.addError('You cannot re-Open this Box because it has been already approved');
	    		}
	    	}    	
    	}
    }
    
    @Future
    private static void sendSubmissionEmailsFuture(List<Id> boxIds){
    	    		    		
		List<Procedural_Solutions_Kit__c> boxes = [	Select Id, Name, 
														Box_Description__c, 
														Customer_Reference__c, 
														No_Of_Boxes__c,
														Boxes_with_Same_Config__c, 
														New_Customer_or_Kit__c,
														Sales_Rep_Job_Title__c, 
														Sales_Rep_Manager__c, 
														Country__c,
														Procedure__c,
														Include_IFU_with_Box__c,
														Account__c, Account__r.Name, Account__r.SAP_ID__c, Account__r.SAP_Search_Term1__c, Account__r.SAP_Search_Term2__c, Account__r.SAP_Account_Type__c,
														Contact__r.Name, Contact__r.Email, Contact__r.Primary_Job_Title_vs__c, 
														Opportunity__r.Name, 
														OwnerId, Owner.Name,
														
														(Select Id, Quantity__c, Proposed_Price__c, Total_Price__c, CurrencyIsoCode, Product__r.CFN_Code_Text__c, Product__r.Name from Procedural_Solutions_Kit_Products__r) 
													from Procedural_Solutions_Kit__c where Id IN :boxIds];
		
		List<String> PS_EMEA_Emails = new List<String>();
		for(Procedural_Solutions_Kit_Email__c PS_Email_setting : Procedural_Solutions_Kit_Email__c.getall().values()) PS_EMEA_Emails.add(PS_Email_setting.email__c);	
				
		List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
		List<Note> notes = new List<Note>();
        List<Attachment> attachments = new List<Attachment>();
        
        Set<Id> masterShipTos = new Set<Id>();
        for(Procedural_Solutions_Kit__c box : boxes){
        	
        	if(box.Account__r != null && box.Account__r.SAP_Account_Type__c == 'Ship to Party' && box.Account__r.SAP_Search_Term1__c == 'MASTER' && box.Account__r.SAP_Search_Term2__c == 'SHIP TO'){
        		
        		masterShipTos.add(box.Account__c);
        	}	        
        }
        
        Map<Id, Account> accountMap = new Map<Id, Account>();
        
        if(masterShipTos.size() > 0){
        
        	accountMap = new Map<Id, Account>([Select Id, (Select Affiliation_To_Account__r.Name, Affiliation_To_Account__r.SAP_ID__c from Affiliations__r where RecordType.DeveloperName = 'A2A' AND Affiliation_Type__c = 'Master Ship To/Sold To' AND Affiliation_Start_Date__c <= TODAY AND (Affiliation_End_Date__c = null OR Affiliation_End_Date__c >= TODAY )) from Account where Id IN :masterShipTos]);
        }
		
		for(Procedural_Solutions_Kit__c box : boxes){
			
			String mainBody = generateEmailBody(box);
			String attachment = generateAttachmentBody(box, accountMap);
			Messaging.EmailFileAttachment emailAttachment = new Messaging.EmailFileAttachment();
			emailAttachment.setFileName('report.csv');
			emailAttachment.setBody(Blob.valueOf(attachment));			
						        	               												
    		Messaging.SingleEmailMessage PSEMEA_email = new Messaging.SingleEmailMessage();    			    			
			PSEMEA_email.setSubject('Submission for Box ' + box.Name + (box.Customer_Reference__c != null ? ' / ' + box.Customer_Reference__c : ''));
    		PSEMEA_email.setHtmlBody(mainBody + generateApprovalSection(box));
    		PSEMEA_email.setToAddresses(PS_EMEA_Emails); 
    		PSEMEA_email.setSaveAsActivity(false);
    		PSEMEA_email.setFileAttachments(new Messaging.EmailFileAttachment[] {emailAttachment});   			
    		emails.add(PSEMEA_email);
    			
    		Messaging.SingleEmailMessage owner_Email = new Messaging.SingleEmailMessage();    			    			
			owner_Email.setSubject('Submission confirmation for Box ' + box.Name + (box.Customer_Reference__c != null ? ' / ' + box.Customer_Reference__c : ''));
    		owner_Email.setHtmlBody(mainBody + '</body></html>');
    		owner_Email.setTargetObjectId(box.OwnerId); 
    		owner_Email.setSaveAsActivity(false);   	
    		//owner_Email.setFileAttachments(new Messaging.EmailFileAttachment[] {emailAttachment});		
    		emails.add(owner_Email);
    		
    		Note n = new Note();
	        n.parentId = box.Opportunity__c;
	        n.title = box.Name;
	        n.body = attachment;
	        notes.add(n);
	
	        Attachment attach = new Attachment();    
	        attach.Name = 'report.csv';
	        attach.body = Blob.valueOf(attachment);
	        attach.parentId = box.Opportunity__c;
	        attach.ContentType = 'text/csv';
	        attachments.add(attach);
		}
		
		if(emails.size() > 0) Messaging.sendEmail(emails);    	
		if(notes.size() > 0) Database.insert(notes, false);
        if(attachments.size() > 0) Database.insert(attachments, false);
    }
        
    private static String generateEmailBody(Procedural_Solutions_Kit__c box){
    	
    	String body = '<html><style type="text/css">body { font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; }.table { width: 100%; max-width: 100%;}table {background-color: transparent;}table {border-spacing: 0;border-collapse: collapse;}.table>thead>tr>th {vertical-align: bottom;border-bottom: 2px solid #ddd;}.table>tbody>tr>td,.table>tbody>tr>th,.table>tfoot>tr>td,.table>tfoot>tr>th,.table>thead>tr>td,.table>thead>tr>th { padding: 8px;line-height: 1.42857143;vertical-align: top;border-top: 1px solid #ddd;}th { text-align: left;} </style>';
		body += '<body>';        
        body += '<table class="table" border="0">';
        body += '<tr>';
        body += '<th class="label" style="width: 40%">Box Id</th>';
        body += '<td class="value">' + (box.Country__c != null ? box.Country__c : '') + '/' + box.Name.replace('-', '') + '</td>';
        body += '</tr><tr>';
        body += '<th class="label">Box Desc</th>';
        body += '<td class="value">' + (box.Box_Description__c != null ? box.Box_Description__c : '') + '</td>';
        body += '</tr><tr>';
        body += '<th class="label">Procedure</th>';
        body += '<td class="value">' + (box.Procedure__c != null ? box.Procedure__c : '') + '</td>';
        body += '</tr><tr>';
        body += '<th class="label">Account</th>';
        body += '<td class="value">' + (box.Account__r != null ? box.Account__r.Name : '') + '</td>';
        body += '</tr><tr>';
        body += '<th class="label">Contact</th>';
        body += '<td class="value">' + (box.Contact__r != null ? box.Contact__r.Name : '') + '</td>';
        body += '</tr><tr>';
        body += '<th class="label">Email</th>';
        body += '<td class="value">' + (box.Contact__r != null && box.Contact__r.Email != null ? box.Contact__r.Email : '') + '</td>';
        body += '</tr><tr>';
        body += '<th class="label">Opportunity</th>';
        body += '<td class="value">' + (box.Opportunity__r != null ? box.Opportunity__r.Name : '') + '</td>';
        body += '</tr><tr>';
        body += '<th class="label">Sales Rep</th>';
        body += '<td class="value">' + box.Owner.Name + '</td>';
        body += '</tr><tr>';
        body += '<th class="label">Date</th>';
        body += '<td class="value">' + Datetime.now().format('MMMM d\',\'  yyyy') + '</td>';
        body += '</tr><tr>';
        body += '<th class="label">Number of Boxes</th>';
        body += '<td class="value">' + (box.No_Of_Boxes__c != null ? String.valueOf(box.No_Of_Boxes__c) : '') + '</td>';
        body += '</tr><tr>';       
        body += '<th class="label">Customer Reference</th>';
        body += '<td class="value">' + (box.Customer_Reference__c != null ? box.Customer_Reference__c : '' ) + '</td>';
        body += '</tr>';
        body += '</table>';    
        body += '<br/><br/>';
        
        body += '<table class="table" border="0">';
        body += '<tr>';
        body += '<th>Prouct No</th>';
        body += '<th>Description</th>';
        body += '<th style="text-align:center">Proposed Price</th>';
        body += '<th style="text-align:center">Quantity No</th>';
        body += '<th style="text-align:center">Total Price</th>';
        body += '</tr>';
        
        String boxCurrency;
        Decimal totalPrice = 0;
        
        for(Procedural_Solutions_Kit_Product__c product : box.Procedural_Solutions_Kit_Products__r){

			body += '<tr>';
			
        	body += '<td class="value">' + product.Product__r.CFN_Code_Text__c + '</td>';
        	body += '<td class="value">' + product.Product__r.Name + '</td>';
        	body += '<td class="value" style="text-align:center">' + (product.Proposed_Price__c != null ? product.CurrencyIsoCode + ' ' + product.Proposed_Price__c : '') + '</td>';        	
			body += '<td class="value" style="text-align:center">' + product.Quantity__c + '</td>';
			body += '<td class="value" style="text-align:center">' + (product.Proposed_Price__c != null ? product.CurrencyIsoCode + ' ' + product.Total_Price__c : '') + '</td>';
        	
        	body += '</tr>';
        	
        	boxCurrency = product.CurrencyIsoCode;
        	if(product.Total_Price__c != null) totalPrice += product.Total_Price__c;
        }
        
        body += '<tr><td/><td/><td/><td/><td/></tr>';
        
        body += '<tr>';
			
    	body += '<td class="value"></td>';
    	body += '<td class="value"></td>';
    	body += '<td class="value"></td>';        	
		body += '<td class="value" style="font-weight : bold;border-top-style:solid;border-top-width:1px;text-align:center">Total</td>';
		body += '<td class="value" style="font-weight : bold;border-top-style:solid;border-top-width:1px;text-align:center">' + boxCurrency + ' ' + totalPrice + '</td>';
        	
    	body += '</tr>';
        
		body += '</table>';
		
    	return body;	
    }
    
    private static String generateApprovalSection(Procedural_Solutions_Kit__c box){
    	    	
		String emailService;
				
		if(Test.isRunningTest() == false){
			
			EmailServicesAddress emailServicesAddress = [SELECT Id, LocalPart, EmailDomainName FROM EmailServicesAddress WHERE Function.FunctionName = 'Procedural Solutions Kits'];		
			emailService = emailServicesAddress.LocalPart + '@' + emailServicesAddress.EmailDomainName;
			
		}else{
			
			emailService = 'dummyTest@medtronic.com';
		}
    	
    	String body = '<br/><br/>';
    	
    	body += '<p>Use the following links to either Approver or Reject this Box configuration:</p>';

		body += '<br/><br/>';
				    
		body += '<table class="table">';
		body += '<tr><td align="center" style="width:40%">';
		body += '</td><td align="center" style="white-space: nowrap">';
		body += '<a href="mailto:' + emailService + '?subject=Approve/' + box.Name + '">Approve</a>';
		body += '</td><td align="center" style="width:20%">';
		body += '</td><td align="center" style="white-space: nowrap">';
		body += '<a href="mailto:' + emailService + '?subject=Reject/' + box.Name + '&body=Add a Rejection reason below between the markers (do not delete the markers below) %0D%0A%0D%0A<==========%0D%0A %0D%0A==========>">Reject</a>';
		body += '</td><td align="center" style="width:40%">';
		body += '</td></tr>';
		body += '</table>';
		   
		body += '</body></html>';
    	
    	
    	return body;	
    }
        
    private static String generateAttachmentBody(Procedural_Solutions_Kit__c box, Map<Id, Account> accountMap){ 
      
      	String attachment = '"Account ID","Opportunity","Date","Sales Rep","Sales Rep Title","Sales Rep RSM","Country","Hospital","Contact","Contact Title","Product No","Description","Qty","Box ID","Box Description","No of Boxes","Previous Boxes with same configuration","New Customer or Kit","Include IFU with Box","Customer Reference","Procedure","Proposed Price Per EA","Currency"\n';
      	Datetime now = Datetime.now();
      	
      	Account boxAccount = box.Account__r;      	
      	if(boxAccount != null && accountMap.containsKey(boxAccount.Id) && accountMap.get(boxAccount.Id).Affiliations__r.size() > 0) boxAccount = accountMap.get(boxAccount.Id).Affiliations__r[0].Affiliation_To_Account__r;
      
    	for(Procedural_Solutions_Kit_Product__c boxProd : box.Procedural_Solutions_Kit_Products__r){
        
	        attachment += '"'+ (boxAccount != null ? boxAccount.SAP_ID__c : '');
	        attachment += '","' + (box.Opportunity__r != null ? box.Opportunity__r.Name : '');
	        attachment += '",' + now.format('dd/MM/yyyy');
	        attachment += ',"' + box.Owner.Name;
	        attachment += '","' + box.Sales_Rep_Job_Title__c;
	        attachment += '","' + box.Sales_Rep_Manager__c;
	        attachment += '","' + box.Country__c;
	        attachment += '","' + (boxAccount != null ? boxAccount.Name : '');
	        attachment += '","' + (box.Contact__r != null ? box.Contact__r.Name : '');
	        attachment += '","' + (box.Contact__r != null ? box.Contact__r.Primary_Job_Title_vs__c : '');
	        attachment += '","' + (boxProd.Product__r != null ? boxProd.Product__r.CFN_Code_Text__c : '');
	        attachment += '","' + (boxProd.Product__r != null ? boxProd.Product__r.Name : '');
	        attachment += '",' + boxProd.Quantity__c;	       
	        attachment += ',"' + box.Name;      
	        attachment += '","' + escapeText(box.Box_Description__c);
	        attachment += '",' + box.No_Of_Boxes__c;
	        attachment += ',' + box.Boxes_with_Same_Config__c;
	        attachment += ',"' + box.New_Customer_or_Kit__c;
	        attachment += '","' + (box.Include_IFU_with_Box__c != null ? box.Include_IFU_with_Box__c : '');
	        attachment += '","' + (box.Customer_Reference__c != null ? escapeText(box.Customer_Reference__c) : box.Name);
            attachment += '","' + box.Procedure__c;
            attachment += '",' + boxProd.Proposed_Price__c;
	        attachment += ',"' + boxProd.CurrencyIsoCode;
        	attachment += '"\n';
      	}
      
    	return attachment;
    }
    
    private static String escapeText(String inputText){
    	
    	if(inputText == null) return null;
    	
    	return inputText.replaceAll('[^a-zA-Z0-9 ]+', ' ');
    }
}