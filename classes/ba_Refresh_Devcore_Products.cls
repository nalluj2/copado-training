global class ba_Refresh_Devcore_Products implements Database.Batchable<sObject>, Database.Stateful{
    
    @TestVisible
	private Map<Id, Id> dataMap;
        
    global Database.QueryLocator start(Database.BatchableContext ctx){
    	
    	dataMap = new Map<Id, Id>();
    	
    	if(Test.isRunningTest() == false){
    		
    		for(SObject acc : Database.query('Select Id, External_Id__c from Account where RecordType.DeveloperName = \'Competitor\'')) dataMap.put(String.valueOf(acc.get('External_Id__c')), acc.Id);    	
    		for(SObject bu : Database.query('Select Id, External_Id__c from Business_Unit__c')) dataMap.put(String.valueOf(bu.get('External_Id__c')), bu.Id);    	
    		for(SObject prodGroup : Database.query('Select Id, External_Id__c from Product_Group__c')) dataMap.put(String.valueOf(prodGroup.get('External_Id__c')), prodGroup.Id);
    	}   
    	 	
    	String query = 'SELECT Business_Unit_ID__c, Product_Group__c, Manufacturer_Account_ID__c FROM Product2';
    	
    	return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext ctx, List<Product2> records){
    	
    	for(Product2 prod : records){
    		
    		if(prod.Business_Unit_ID__c != null) prod.Business_Unit_ID__c = dataMap.get(prod.Business_Unit_ID__c);
    		if(prod.Product_Group__c != null) prod.Product_Group__c = dataMap.get(prod.Product_Group__c);
    		if(prod.Manufacturer_Account_ID__c != null) prod.Manufacturer_Account_ID__c = dataMap.get(prod.Manufacturer_Account_ID__c);    		
    	}
    	
    	Database.update(records, false);
    }
    
    global void finish(Database.BatchableContext BC){
    	
    }
}