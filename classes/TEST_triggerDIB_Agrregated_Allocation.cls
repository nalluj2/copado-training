/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TEST_triggerDIB_Agrregated_Allocation {

    static testMethod void myUnitTest() {
        // Create a few Aggregated Allocation lines :
        Account acc = new account(name ='TestCoverage'); 
        insert acc ; 
        
        DIB_Fiscal_Period__c ff = new DIB_Fiscal_Period__c() ; 
       // ff.
         
        List<DIB_Aggregated_Allocation__c> aa = new List<DIB_Aggregated_Allocation__c>{} ; 
        for (Integer i=0; i < 60 ; i++){
			DIB_Aggregated_Allocation__c myAggregated_Allocation = new DIB_Aggregated_Allocation__c();
			myAggregated_Allocation.Sales_Force_Account__c = acc.Id ;
			myAggregated_Allocation.Fiscal_Month__c = '11';
  			myAggregated_Allocation.Fiscal_Year__c = String.valueOf(2010 + i);
			aa.add(myAggregated_Allocation) ; 
        }
        insert aa ; 
        
        for (DIB_Aggregated_Allocation__c a  : aa){
        	a.Fiscal_Month__c = '10'; 
        }
        update aa ; 
    }
}