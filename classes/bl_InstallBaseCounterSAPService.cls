public with sharing class bl_InstallBaseCounterSAPService {
	
	public static Installed_Product_Measures__c getProductMeasuresDetails(Id counterId){
		
		List<Installed_Product_Measures__c> counter = [Select 	Id,																
																New_Recorded_Timestamp__c, 
																New_Recorded_Value__c, 
																SAP_Measure_Point_ID__c, 
																Description__c, 
																Read_By__c 
																from Installed_Product_Measures__c where Id = :counterId];
		
		if(counter.isEmpty()) throw new ws_Exception('No Installed Product Measure was found for Id: ' + counterId);  	
    	if(counter.size() > 1) throw new ws_Exception('Multiple Installed Product Measures were found for Id: ' + counterId);
		
		return counter[0];
	}
	
	public static ws_InstallBaseCounterSAPService.SAPInstallBaseCounter mapMeasurementToCounter(Installed_Product_Measures__c prodMeasure){
		
		ws_InstallBaseCounterSAPService.SAPInstallBaseCounter counter = new ws_InstallBaseCounterSAPService.SAPInstallBaseCounter();		
		String newRecordedDateTime = datetimeToString(prodMeasure.New_Recorded_Timestamp__c);
		counter.NEW_RECORDED_DATE = newRecordedDateTime.subString(0, 10);
		counter.NEW_RECORDED_TIME = newRecordedDateTime.subString(10, 18);
		counter.NEW_RECORDED_VALUE = prodMeasure.New_Recorded_Value__c;
		counter.COUNTER_ID = prodMeasure.SAP_Measure_Point_ID__c;
		counter.DESCRIPTION = prodMeasure.Description__c;		
		counter.READ_BY = prodMeasure.Read_By__c;
		
		return counter;
	}    
	
	// Convert DateTime into String in UTC format and timezone
    private static String datetimeToString(DateTime inputDatetime){
    	
    	if(inputDatetime == null) return null;
    	
    	return inputDatetime.formatGMT('yyyy-MM-ddHH:mm:ss');
    }
}