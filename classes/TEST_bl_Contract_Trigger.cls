@isTest
private class TEST_bl_Contract_Trigger{
    
    @IsTest(SeeAllData=true) // ConnectApi methods are not supported in data siloed tests - use SeeAllData=true
    static void test_createChatterPost(){

        List<EntitySubscription> lstEntitySubscription_Existing;
        List<FeedItem> lstFeedItem;
        Integer iEntitySubscription = 0;
        Integer iFeedItem = 0;

        //---------------------------------------
        // CREATE TEST DATA
        //---------------------------------------
        // Create Test User
        User oUser_SystemAdmin = clsTestData_User.createUser_SystemAdministratorMDT('tstAdm1', false);
            oUser_SystemAdmin.User_Business_Unit_vs__c = 'Cranial Spinal';
            oUser_SystemAdmin.Primary_sBU__c = 'Spine & Biologics';
        insert oUser_SystemAdmin;

        // Create Opportunity
        System.runAs(oUser_SystemAdmin){
            clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'Service_Contract_Opportunity').Id;
            clsTestData_Opportunity.createOpportunity();

            // Create Contract
            clsTestData_Contract.iRecord_Contract = 1;
            clsTestData_Contract.idRecordType_Contract = clsUtil.getRecordTypeByDevName('Contract', 'RTG_Service_Contract').Id;
            clsTestData_Contract.createContract(false);
            clsTestData_Contract.oMain_Contract.Opportunity__c = clsTestData_Opportunity.oMain_Opportunity.Id;
            insert clsTestData_Contract.oMain_Contract;

        }
        lstEntitySubscription_Existing = [SELECT ParentId, SubscriberId FROM EntitySubscription WHERE ParentId = :clsTestData_Contract.oMain_Contract.Id ORDER BY ParentId];
        iEntitySubscription = lstEntitySubscription_Existing.size();
        lstFeedItem = [SELECT Id, Body FROM FeedItem WHERE ParentId = :clsTestData_Contract.oMain_Contract.Id ORDER BY Id DESC];
		System.debug('**BC** lstFeedItem 0 : ' + lstFeedItem.size());
        iFeedItem = lstFeedItem.size();
        //---------------------------------------


        //---------------------------------------
        // TEST LOGIC
        //---------------------------------------
        Test.startTest();

            System.runAs(oUser_SystemAdmin){

                    clsTestData_Contract.oMain_Contract.SAP_Contract_Number__c = '6SAPCN00000000';
                update clsTestData_Contract.oMain_Contract;

                lstEntitySubscription_Existing = [SELECT ParentId, SubscriberId FROM EntitySubscription WHERE ParentId = :clsTestData_Contract.oMain_Contract.Id ORDER BY ParentId];
                System.assertEquals(iEntitySubscription + 1, lstEntitySubscription_Existing.size());

                lstFeedItem = [SELECT Id, Body FROM FeedItem WHERE ParentId = :clsTestData_Contract.oMain_Contract.Id ORDER BY Id DESC];
				System.debug('**BC** lstFeedItem 1 : ' + lstFeedItem.size());
                System.assertEquals(lstFeedItem.size(), iFeedItem + 1);


                    clsTestData_Contract.oMain_Contract.SAP_Contract_Approved__c = 'Yes';
                update clsTestData_Contract.oMain_Contract;

                lstFeedItem = [SELECT Id, Body FROM FeedItem WHERE ParentId = :clsTestData_Contract.oMain_Contract.Id ORDER BY Id DESC];
				System.debug('**BC** lstFeedItem 2 : ' + lstFeedItem.size());
                System.assertEquals(lstFeedItem.size(), iFeedItem + 2);


                    clsTestData_Contract.oMain_Contract.SAP_Contract_Approved__c = 'No';
                    clsTestData_Contract.oMain_Contract.SAP_Contract_Comments__c = 'Additional Comments';
                update clsTestData_Contract.oMain_Contract;

            }

        Test.stopTest();
        //---------------------------------------


        //---------------------------------------
        //---------------------------------------
        lstFeedItem = [SELECT Id, Body FROM FeedItem WHERE ParentId = :clsTestData_Contract.oMain_Contract.Id ORDER BY Id DESC];
		System.debug('**BC** lstFeedItem 3 : ' + lstFeedItem.size());
        System.assertEquals(lstFeedItem.size(), iFeedItem + 3);

        System.assert(lstFeedItem[0].Body.contains('is disapproved'));
        System.assert(lstFeedItem[1].Body.contains('is approved'));
        System.assert(lstFeedItem[2].Body.contains('Please check the SAP Contract '));
        //---------------------------------------

    }
        
}