@isTest
private class TEST_ctrlExt_AccountAffiliationView {
	
	@isTest static void createTestData() {

		//-----------------------------------------------------
		// CREATE TEST DATA
		//-----------------------------------------------------
		String tCountry = 'BELGIUM';

		clsTestData_MasterData.createCompany();
        List<Business_Unit__c> lstBusinessUnit = clsTestData_MasterData.createBusinessUnit(false);
        for (Business_Unit__c oBU : lstBusinessUnit){
            oBU.Contact_Flag__c = 'Cranial_and_Spinal__c';
        }
        insert lstBusinessUnit;
        List<Sub_Business_Units__c> lstSubBusinessUnit = clsTestData_MasterData.createSubBusinessUnit(false);
        for (Sub_Business_Units__c oSBU : lstSubBusinessUnit){
            oSBU.Contact_Flag__c = 'Brain_Modulation__c';
        }
        insert lstSubBusinessUnit;
		clsTestData_MasterData.createCompanyRelatedSetup_Department();

    	clsTestData_MasterData.tCountryCode = 'BE';
    	clsTestData_MasterData.createCountry();

		clsTestData_MasterData.createSubBusinessUnit();

		clsTestData_Affiliation.tAccountTypeCountry_ISOCode = 'BE';
		clsTestData_Affiliation.createAccountTypeCountry();

		clsTestData_Account.iRecord_Account = 10;
		clsTestData_Account.tCountry_Account = tCountry;
		clsTestData_Account.createAccount();

        clsTestData_Contact.iRecord_Contact = 10;
        List<Contact> lstContact = clsTestData_Contact.createContact(false);
        for (Contact oContact : lstContact){
            oContact.Cranial_and_Spinal__c = true;
            oContact.Brain_Modulation__c = true;
        }
        insert lstContact;

		clsTestData_Affiliation.iRecord_Affiliation_C2A = 10;
		clsTestData_Affiliation.createAffiliation_C2A();

		clsTestData_Affiliation.iRecord_Affiliation_A2A = 10;
		clsTestData_Affiliation.createAffiliation_A2A();
		//-----------------------------------------------------

	}
	
	@isTest static void test_ctrlExt_AccountAffiliationView() {

		String tTest = '';
		Integer iTest = 0;
		Id idTest;
		Date dTest;
		Boolean bTest = false;
		List<SelectOption> lstSO_Test = new List<SelectOption>();
		List<SObject> lstSObject_Test = new List<SObject>();
		List<String> lstString_Test = new List<String>();


		// CREATE TEST DATA
		createTestData();

		// TESTING
		Test.startTest();

        ApexPages.StandardController oCTRL = new ApexPages.StandardController(clsTestData_Account.oMain_Account);
        ctrlExt_AccountAffiliationView oCTRLEXT = new ctrlExt_AccountAffiliationView(oCTRL);

        oCTRLEXT.initialize();


        // TEST GETTERS & SETTERS - GENERAL
		idTest = oCTRLEXT.id_AssignedAffiliation;
		lstSO_Test = oCTRLEXT.lstPageSize;

		oCTRLEXT.bSaveFilter = false;
		bTest = oCTRLEXT.bSaveFilter;
		oCTRLEXT.bSaveFilter = true;
		bTest = oCTRLEXT.bSaveFilter;
		
        oCTRLEXT.bShowFilter = false;
        bTest = oCTRLEXT.bShowFilter;
        tTest = oCTRLEXT.tShowHideFilterLabel;
        oCTRLEXT.showHideFilter();
        bTest = oCTRLEXT.bShowFilter;
        tTest = oCTRLEXT.tShowHideFilterLabel;

        
        // TEST GETTERS & SETTERS - C2A
        oCTRLEXT.tSortField_C2A = '';
        tTest = oCTRLEXT.tSortOrder_C2A;
        tTest = oCTRLEXT.tPrevSortField_C2A;
        tTest = oCTRLEXT.tSortOrder_C2A;

        oCTRLEXT.tSortField_C2A = 'Affiliation_From_Contact__r.lastname';
        oCTRLEXT.sortData_C2A();
        tTest = oCTRLEXT.tSortOrder_C2A;
        oCTRLEXT.sortData_C2A();
        tTest = oCTRLEXT.tSortOrder_C2A;
        oCTRLEXT.sortData_C2A();
        tTest = oCTRLEXT.tSortOrder_C2A;
        tTest = oCTRLEXT.tWarning_C2A;

        lstSObject_Test = oCTRLEXT.lstRecord_C2A;
        bTest = oCTRLEXT.hasNext_C2A;
        bTest = oCTRLEXT.hasPrevious_C2A;
        iTest = oCTRLEXT.pageNumber_C2A;
        iTest = oCTRLEXT.totalPages_C2A;
        iTest = oCTRLEXT.pageSize_C2A;
        iTest = oCTRLEXT.resultSize_C2A;

        oCTRLEXT.pageSize_C2A = 1;
        oCTRLEXT.pageNumber_C2A = 1;

        oCTRLEXT.first_C2A();
        oCTRLEXT.next_C2A();
        oCTRLEXT.last_C2A();
        oCTRLEXT.previous_C2A();


        // TEST GETTERS & SETTERS - A2A
        oCTRLEXT.tSortField_A2A = '';
        tTest = oCTRLEXT.tSortOrder_A2A;
        tTest = oCTRLEXT.tPrevSortField_A2A;
        tTest = oCTRLEXT.tSortOrder_A2A;

        oCTRLEXT.tSortField_A2A = 'Affiliation_To_Account__r.Type';
        oCTRLEXT.sortData_A2A();
        tTest = oCTRLEXT.tSortOrder_A2A;
        oCTRLEXT.sortData_A2A();
        tTest = oCTRLEXT.tSortOrder_A2A;
        oCTRLEXT.sortData_A2A();
        tTest = oCTRLEXT.tSortOrder_A2A;
        tTest = oCTRLEXT.tWarning_A2A;

        lstSObject_Test = oCTRLEXT.lstRecord_A2A;
        bTest = oCTRLEXT.hasNext_A2A;
        bTest = oCTRLEXT.hasPrevious_A2A;
        iTest = oCTRLEXT.pageNumber_A2A;
        iTest = oCTRLEXT.totalPages_A2A;
        iTest = oCTRLEXT.pageSize_A2A;
        iTest = oCTRLEXT.resultSize_A2A;

        oCTRLEXT.pageSize_A2A = 1;
        oCTRLEXT.pageNumber_A2A = 1;

        oCTRLEXT.first_A2A();
        oCTRLEXT.next_A2A();
        oCTRLEXT.last_A2A();
        oCTRLEXT.previous_A2A();  


        // TEST GETTERS & SETTERS - FILTER
        oCTRLEXT.oAffiliation_Filter.Affiliation_End_Date__c = Date.today();
        dTest = oCTRLEXT.oAffiliation_Filter.Affiliation_End_Date__c;

        lstString_Test = oCTRLEXT.lstPrimarySpeciality_Selected;
        lstSO_Test = oCTRLEXT.lstSO_PrimarySpeciality;
        List<String> lstPrimarySpeciality_Selected = new List<String>();
        for (SelectOption oSO : lstSO_Test){
        	lstPrimarySpeciality_Selected.add(oSO.getValue());
        }

        tTest = oCTRLEXT.tSelected_BusinessUnit;
        lstSO_Test = oCTRLEXT.lstSO_BusinessUnit;

        tTest = oCTRLEXT.tSelected_SubBusinessUnit;
        lstSO_Test = oCTRLEXT.lstSO_SubBusinessUnit;

        tTest = oCTRLEXT.tSelected_Department;
        lstSO_Test = oCTRLEXT.lstSO_Department;

        tTest = oCTRLEXT.tSelected_ActiveInactive;
        lstSO_Test = oCTRLEXT.lstSO_ActiveInactive;

        bTest = oCTRLEXT.bRenderAccountHierarchyButton;
        

        // TEST ACTION - C2A
        oCTRLEXT.id_AssignedAffiliation = clsTestData_Affiliation.lstAffiliation_C2A[1].Id;
        oCTRLEXT.viewAffiliation_C2A();

        oCTRLEXT.createAffiliation_C2A();

        oCTRLEXT.id_AssignedAffiliation = clsTestData_Affiliation.lstAffiliation_C2A[0].Id;	// Primary Affiliation
		oCTRLEXT.editAffiliation_C2A();
        oCTRLEXT.id_AssignedAffiliation = clsTestData_Affiliation.lstAffiliation_C2A[1].Id;
		oCTRLEXT.editAffiliation_C2A();

        oCTRLEXT.id_AssignedAffiliation = clsTestData_Affiliation.lstAffiliation_C2A[0].Id;	// Primary Affiliation
		oCTRLEXT.deleteAffiliation_C2A();
        
        clsUtil.hasException = true;
        oCTRLEXT.id_AssignedAffiliation = clsTestData_Affiliation.lstAffiliation_C2A[1].Id;
		try{ oCTRLEXT.deleteAffiliation_C2A(); }catch(Exception oEX){}
        clsUtil.hasException = false;
		oCTRLEXT.deleteAffiliation_C2A();


		oCTRLEXT.createContact();

		oCTRLEXT.tSelected_BusinessUnit = clsTestData_MasterData.oMain_BusinessUnit.Id;
		oCTRLEXT.tSelected_SubBusinessUnit = clsTestData_MasterData.oMain_SubBusinessUnit.Id;
		oCTRLEXT.tSelected_Department = 'Department 1';
		oCTRLEXT.tSelected_ActiveInactive = 'ACTIVE';
		oCTRLEXT.lstPrimarySpeciality_Selected = lstPrimarySpeciality_Selected;
		oCTRLEXT.applyFilter();
		oCTRLEXT.saveFilter();
		oCTRLEXT.loadFilter();


        // TEST ACTION - A2A
        oCTRLEXT.id_AssignedAffiliation = clsTestData_Affiliation.lstAffiliation_C2A[1].Id;
        oCTRLEXT.viewAffiliation_A2A();

        oCTRLEXT.createAffiliation_A2A();

        oCTRLEXT.id_AssignedAffiliation = clsTestData_Affiliation.lstAffiliation_C2A[0].Id;	// Primary Affiliation
		oCTRLEXT.editAffiliation_A2A();
        oCTRLEXT.id_AssignedAffiliation = clsTestData_Affiliation.lstAffiliation_C2A[1].Id;
		oCTRLEXT.editAffiliation_A2A();

        oCTRLEXT.id_AssignedAffiliation = clsTestData_Affiliation.lstAffiliation_C2A[0].Id;	// Primary Affiliation
		oCTRLEXT.deleteAffiliation_A2A();
        
        clsUtil.hasException = true;
        oCTRLEXT.id_AssignedAffiliation = clsTestData_Affiliation.lstAffiliation_C2A[1].Id;
		try{ oCTRLEXT.deleteAffiliation_A2A(); }catch(Exception oEX){}
        clsUtil.hasException = false;
		oCTRLEXT.deleteAffiliation_A2A();

    }

}