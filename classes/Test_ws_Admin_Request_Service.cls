@isTest
private class Test_ws_Admin_Request_Service {
    
    private static testmethod void testRemoveAssignments(){
    	
    	User testUser = new User(
			alias = 'unitTest'
			, email='test@medtronic.com'
			, emailencodingkey='UTF-8'
			, lastname='Testing'
			, languagelocalekey='en_US'
			, localesidkey='en_US'
			, profileid = UserInfo.getProfileId()
			, timezonesidkey='America/Los_Angeles'
			, username='test@medtronic.com.unitTest'
			, Alias_unique__c = 'uTestU'
			, Company_Code_Text__c = 'EUR'
			, CostCenter__c = '000000'
		);
		
		insert testUser;
		
	 	PermissionSet perm = [Select ID from PermissionSet where Name = 'Org_Configuration'];
	 	
	 	PermissionSetAssignment userPerm = new PermissionSetAssignment();
	 	userPerm.AssigneeId = testUser.Id;
	 	userPerm.PermissionSetId = perm.Id;
	 	insert userPerm;
	 	
	 	Test.startTest();
	 	
	 	ws_Admin_Request_Service.Request request = new ws_Admin_Request_Service.Request();
	 	request.UserId = testUser.Id;
	 	request.permissionSets = new List<String>{'Org_Configuration'};
	 	
	 	RestRequest req = new RestRequest();	 	
	 	req.requestbody = Blob.valueOf(JSON.serialize(request));
	 	
	 	RestContext.request = req; 
    	
    	ws_Admin_Request_Service.Response response = ws_Admin_Request_Service.doPost();
    	
    	System.assert(response.isSuccess == true);
    	
    	List<PermissionSetAssignment> assignments = [Select Id from PermissionSetAssignment where AssigneeId = :testUser.Id AND PermissionSet.Name = 'Org_Configuration'];
    	
    	System.assert(assignments.size() == 0);
    }
    
    private static testmethod void testRemoveAssignments_Error(){
    	    		 	
	 	Test.startTest();
	 	
	 	ws_Admin_Request_Service.Request request = new ws_Admin_Request_Service.Request();
	 	request.UserId = '0010E00000BMiK2';// ID of an Account
	 	request.permissionSets = new List<String>{'Org_Configuration'};
	 	
	 	RestRequest req = new RestRequest();	 	
	 	req.requestbody = Blob.valueOf(JSON.serialize(request));
	 	
	 	RestContext.request = req; 
    	
    	ws_Admin_Request_Service.Response response = ws_Admin_Request_Service.doPost();
    	
    	System.assert(response.isSuccess == false);
    	System.assert(response.errorMessage.startsWith('Context User'));    	
    }    
}