@isTest
private class Test_ctrl_Schedule_Activity {
    
    private static testmethod void testTeamDayCalendar(){
    	
    	createTestData();
    	
    	Test.startTest();
    	
    	Case openCase = [Select Id from Case where Status = 'Open'];
    	
    	ctrl_Schedule_Activity.CalendarView calendar = ctrl_Schedule_Activity.getTeamAvailability(openCase.Id, 1000, false);    	    	
    	System.assert(calendar.members.size() > 0);
    	
    	Implant_Scheduling_Team_Member__c member = [Select Member__c, Team__c from Implant_Scheduling_Team_Member__c LIMIT 1];
    	ctrl_Calendar_Event.assignCase(openCase.Id, member.Member__c);
    }
    
    private static void createTestData(){
		
		Account acc = new Account();
		acc.Name = 'Test Account';
		acc.Account_Country_vs__c = 'NETHERLANDS';		
		insert acc;
		
		Implant_Scheduling_Team__c team = [Select Id from Implant_Scheduling_Team__c];
		
		Id caseActivitySchedulingRT = [Select Id from RecordType where SObjectType = 'Case' AND DeveloperName = 'Implant_Scheduling'].Id;
		
		List<Case> caseList = new List<Case>();
				
		Case openCase = new Case();		
		openCase.AccountId = acc.Id;			
		openCase.Start_of_Procedure__c = DateTime.newInstance(Date.today(), Time.newInstance(22, 0, 0, 0)); 
		openCase.Procedure_Duration_Implants__c = '04:00';
		openCase.RecordTypeId = caseActivitySchedulingRT;
		openCase.Activity_Scheduling_Team__c = team.Id;
		openCase.Type = 'Implant Support Request';
		openCase.Status = 'Open';			
		caseList.add(openCase);
				
		List<Implant_Scheduling_Team_Member__c> members = [Select Member__c, Team__c from Implant_Scheduling_Team_Member__c];
				
		for(Implant_Scheduling_Team_Member__c member : members){
								
			Case assignedCaseSingle = new Case();		
			assignedCaseSingle.AccountId = acc.Id;			
			assignedCaseSingle.Start_of_Procedure__c = DateTime.newInstance(Date.today(), Time.newInstance(10, 0, 0, 0)); 
			assignedCaseSingle.Procedure_Duration_Implants__c = '03:00';
			assignedCaseSingle.RecordTypeId = caseActivitySchedulingRT;
			assignedCaseSingle.Activity_Scheduling_Team__c = team.Id;
			assignedCaseSingle.Assigned_To__c = member.Member__c;
			assignedCaseSingle.Type = 'Implant Support Request';
			assignedCaseSingle.Status = 'Assigned';
			caseList.add(assignedCaseSingle);
			
			Case assignedCaseMulti = new Case();		
			assignedCaseMulti.AccountId = acc.Id;			
			assignedCaseMulti.Start_of_Procedure__c = DateTime.newInstance(Date.today(), Time.newInstance(21, 0, 0, 0)); 
			assignedCaseMulti.Procedure_Duration_Implants__c = '04:00';
			assignedCaseMulti.RecordTypeId = caseActivitySchedulingRT;
			assignedCaseMulti.Activity_Scheduling_Team__c = team.Id;
			assignedCaseMulti.Assigned_To__c = member.Member__c;
			assignedCaseMulti.Type = 'Service Request';
			assignedCaseMulti.Status = 'Assigned';
				
			caseList.add(assignedCaseMulti);			
		}
				
		bl_Case_Trigger.runningSchedulingApp = true;
		
		insert caseList;
		
		Id eventActivitySchedulingRT = [Select Id from RecordType where SObjectType = 'Event' AND DeveloperName = 'Activity_Scheduling'].Id;
		List<Event> personalEvents = new List<Event>();
		
		for(Implant_Scheduling_Team_Member__c member : members){
								
			Event personalEventSingle = new Event();
			personalEventSingle.Subject = 'Test Subject Single';	
			personalEventSingle.StartDateTime = DateTime.newInstance(Date.today(), Time.newInstance(12, 0, 0, 0)); 
			personalEventSingle.EndDateTime = personalEventSingle.StartDateTime.addHours(5);
			personalEventSingle.RecordTypeId = eventActivitySchedulingRT;
			personalEventSingle.OwnerId = member.Member__c;
			personalEvents.add(personalEventSingle);
			
			Event personalEventMulti = new Event();
			personalEventMulti.Subject = 'Test Subject Multi';	
			personalEventMulti.StartDateTime = DateTime.newInstance(Date.today(), Time.newInstance(12, 0, 0, 0));
			personalEventMulti.EndDateTime = DateTime.newInstance(Date.today().addDays(1), Time.newInstance(10, 0, 0, 0));
			personalEventMulti.RecordTypeId = eventActivitySchedulingRT;
			personalEventMulti.OwnerId = member.Member__c;			
			personalEvents.add(personalEventMulti);			
		}
				
		insert personalEvents;
	}
	
	@TestSetup
	private static void setData() {
		
		Mobile_App__c app = new Mobile_App__c();
		app.Name = 'Activity Scheduling Tool';
		app.Active__c = true;
		app.Unique_Key__c = 'ACT_SCHDL';
		insert app;
		
		List<Implant_Scheduling_Team__c> teams = new List<Implant_Scheduling_Team__c>();
		
		Id activitySchedulingTeamRT = [Select Id from RecordType where SObjectType = 'Implant_Scheduling_Team__c' AND DeveloperName = 'Activity_Scheduling_Team'].Id;
		
		Implant_Scheduling_Team__c team = new Implant_Scheduling_Team__c();			
		team.Name = 'Test Team 1';
		team.Team_Color__c = 'rgb(99, 99, 99)';
		team.Work_Day_End__c = '18';
		team.Work_Day_Start__c = '8';
		team.Working_Days__c = '1:5';			
		team.RecordTypeId = activitySchedulingTeamRT;
		insert team;
		
		List<User> otherUsers = [Select Id from User where Profile.Name LIKE 'EUR Field Force CVG%' AND isActive = true LIMIT 10];
		
		List<Implant_Scheduling_Team_Member__c> members = new List<Implant_Scheduling_Team_Member__c>();
		
		Integer i = 0;
		
		for(User otherUser : otherUsers){
			
			Implant_Scheduling_Team_Member__c member = new Implant_Scheduling_Team_Member__c();
			member.Team__c = team.Id;
			member.Member__c = otherUser.Id;				
			
			members.add(member);
			i++;
		}
		
		insert members;	
	} 
}