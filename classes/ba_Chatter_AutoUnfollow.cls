//---------------------------------------------------------------------------------------------------------------------------------------------------
//  Author		:    Bart Caelen
//  Date 		:    20180201
//  Description	:    CR-14975 - This batch/scheduled Class will unfollow records
//---------------------------------------------------------------------------------------------------------------------------------------------------
global class ba_Chatter_AutoUnfollow implements Database.Batchable<sObject>, Schedulable, Database.AllowsCallouts, Database.Stateful {
	
	global String tObjectType;
	global Integer iBatchSize = 200;
	global String tSOQL = '';


	// SCHEDULE Settings
	global void execute(SchedulableContext sc){ 


		// Set BatchSize
		ba_Chatter_AutoUnfollow oBatch = new ba_Chatter_AutoUnfollow();
			oBatch.tObjectType = 'User';
		Database.executebatch(oBatch, iBatchSize);


	}


	// BATCH Settings
	global Database.QueryLocator start(Database.BatchableContext BC) {


		Boolean bRunningInASandbox = clsUtil.bRunningInASandbox();

		if (String.isBlank(tSOQL)){

			tSOQL = 'SELECT Id FROM ' + tObjectType + ' WHERE AutoUnfollow__c = true';

		}

		if (Test.isRunningTest()){
			tSOQL += ' LIMIT ' + String.valueOf(iBatchSize);
		}

		return Database.getQueryLocator(tSOQL);


	}


   	global void execute(Database.BatchableContext BC, List<sObject> lstData) {


		if (!Schema.getGlobalDescribe().get(tObjectType).getDescribe().isFeedEnabled()) return;

   		// Put the Id's of the processing records in a Set
   		Set<Id> setID_ProcessingData = new Set<Id>();
   		for (sObject oData : lstData){
   			setID_ProcessingData.add(String.valueOf(oData.get('Id')));
   		}


   		// Get the EntitySubscription records (followers) related to the processing Id's
		List<EntitySubscription> lstEntitySubscription =
			[
				SELECT Id
				FROM EntitySubscription
				WHERE parentId IN :setID_ProcessingData
			];

   		// Delete the EntitySubscription records (followers)
   		delete lstEntitySubscription;


	}
	

	global void finish(Database.BatchableContext BC) {


		// Start the Batch job for the next type (object) if not running a Test
		ba_Chatter_AutoUnfollow oBatch = new ba_Chatter_AutoUnfollow();

		if (tObjectType == 'User'){

			oBatch.tObjectType = 'Contact';

		}else if (tObjectType == 'Contact'){

			oBatch.tObjectType = 'Opportunity';

		}else if (tObjectType == 'Opportunity'){

			oBatch.tObjectType = 'Change_Request__c';

		}else if (tObjectType == 'Change_Request__c'){

			oBatch.tObjectType = 'Lead';

		}else if (tObjectType == 'Lead'){

			oBatch.tObjectType = 'Task';

		}else if (tObjectType == 'Task'){

			oBatch.tObjectType = 'Event';

		}else if (tObjectType == 'Event'){

//			oBatch.tObjectType = 'Account_Extended_Profile__c';
//
//		}else if (tObjectType == 'Account_Extended_Profile__c'){

		}

		if ( !Test.isRunningTest() && !String.isBlank(oBatch.tObjectType) ) Database.executebatch(oBatch, iBatchSize);
		

	}
	
}
//---------------------------------------------------------------------------------------------------------------------------------------------------