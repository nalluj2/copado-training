//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 24-11-2015
//  Description      : Business Logic class that contains the logic to send date to SAP
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public class bl_NotificationSAP {

    //----------------------------------------------------------------------------------------
    // CONSTANTS
    //----------------------------------------------------------------------------------------
    public static final String DIRECTION = 'Outbound';

    public static final String SCHEDULED_NOTIFICATION_NAME = 'SAP NOTIFICATION RETRY FOR '; // We will add the SFDC ID dynamically when the scheduling is executed
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
    // SEND DATA TO WEBMETHODS / SAP
    // These methods are called by APEX Triggers
    //----------------------------------------------------------------------------------------
    // Collect the data and parameters that will be send to WebMethods / SAP
    // Case Data
    public static boolean sendNotificationToSAP_Case(List<Case> lstData, String tAction){

        NotificationSAP oNotificationSAP = new NotificationSAP();
            oNotificationSAP.tDirection = bl_NotificationSAP.DIRECTION;
            oNotificationSAP.lstData = lstData;
    
            // Set Salesforce.com Related Settings
            oNotificationSAP.tSFDC_Object = 'Case';
            oNotificationSAP.tSFDC_FieldName_Id = 'Id';
            oNotificationSAP.tSFDC_FieldName_SAPID = 'SVMX_SAP_Notification_Number__c';

            // Set WebMethods Related Settings
            oNotificationSAP.tWM_Object = 'ServiceNotification';
            oNotificationSAP.tWM_Operation = tAction;
            oNotificationSAP.tWM_Process = 'ServiceNotificationUpdate';

            oNotificationSAP.tWebServiceName = 'Case_NotificationSAP';

        return sendNotificationToSAP(oNotificationSAP);

    }
    // SVMXC__Service_Order__c Data
    public static boolean sendNotificationToSAP_ServiceOrder(List<SVMXC__Service_Order__c> lstData, String tAction){

        NotificationSAP oNotificationSAP = new NotificationSAP();
            oNotificationSAP.tDirection = bl_NotificationSAP.DIRECTION;
            oNotificationSAP.lstData = lstData;
    
            // Set Salesforce.com Related Settings
            oNotificationSAP.tSFDC_Object = 'SVMXC__Service_Order__c';
            oNotificationSAP.tSFDC_FieldName_Id = 'Id';
            oNotificationSAP.tSFDC_FieldName_SAPID = 'SVMX_SAP_Service_Order_No__c';

            // Set WebMethods Related Settings
            oNotificationSAP.tWM_Object = 'ServiceOrder';
            oNotificationSAP.tWM_Operation = tAction;
            oNotificationSAP.tWM_Process = 'ServiceOrderUpdate';

            oNotificationSAP.tWebServiceName = 'ServiceOrder_NotificationSAP';


        return sendNotificationToSAP(oNotificationSAP);
    
    }
    public static boolean sendNotificationToSAP_ServiceOrder_Completed(List<SVMXC__Service_Order__c> lstData, String tAction){

        NotificationSAP oNotificationSAP = new NotificationSAP();
            oNotificationSAP.tDirection = bl_NotificationSAP.DIRECTION;
            oNotificationSAP.lstData = lstData;
    
            // Set Salesforce.com Related Settings
            oNotificationSAP.tSFDC_Object = 'SVMXC__Service_Order__c';
            oNotificationSAP.tSFDC_FieldName_Id = 'Id';
            oNotificationSAP.tSFDC_FieldName_SAPID = 'SVMX_SAP_Service_Order_No__c';

            // Set WebMethods Related Settings
            oNotificationSAP.tWM_Object = 'ServiceOrder';
            oNotificationSAP.tWM_Operation = tAction;
            oNotificationSAP.tWM_Process = 'ServiceOrderComplete';

            oNotificationSAP.tWebServiceName = 'ServiceOrderComplete_NotificationSAP';


        return sendNotificationToSAP(oNotificationSAP);
    
    }
    // Attachment Data
    public static boolean sendNotificationToSAP_Attachment(List<Attachment> lstData, String tAction){

        NotificationSAP oNotificationSAP = new NotificationSAP();
            oNotificationSAP.tDirection = bl_NotificationSAP.DIRECTION;
            oNotificationSAP.lstData = lstData;
    
            // Set Salesforce.com Related Settings
            oNotificationSAP.tSFDC_Object = 'Attachment';
            oNotificationSAP.tSFDC_FieldName_Id = 'Id';
            oNotificationSAP.tSFDC_FieldName_SAPID = '';

            // Set WebMethods Related Settings
            oNotificationSAP.tWM_Object = '';
            oNotificationSAP.tWM_Operation = '';
            oNotificationSAP.tWM_Process = '';

            oNotificationSAP.tWebServiceName = 'Attachment_NotificationSAP';

        return sendNotificationToSAP(oNotificationSAP);

    }
    // Install Base Data
    public static boolean sendNotificationToSAP_InstalledProduct(List<SVMXC__Installed_Product__c> lstData, String tAction){

        NotificationSAP oNotificationSAP = new NotificationSAP();
            oNotificationSAP.tDirection = bl_NotificationSAP.DIRECTION;
            oNotificationSAP.lstData = lstData;
    
            // Set Salesforce.com Related Settings
            oNotificationSAP.tSFDC_Object = 'SVMXC__Installed_Product__c';
            oNotificationSAP.tSFDC_FieldName_Id = 'Id';
            oNotificationSAP.tSFDC_FieldName_SAPID = 'SVMX_SAP_Equipment_ID__c';

            // Set WebMethods Related Settings
            oNotificationSAP.tWM_Object = '';
            oNotificationSAP.tWM_Operation = '';
            oNotificationSAP.tWM_Process = '';

            oNotificationSAP.tWebServiceName = 'InstalledProduct_NotificationSAP';

        return sendNotificationToSAP(oNotificationSAP);

    }
    
    // Install Base Counter Data
    public static boolean sendNotificationToSAP_InstalledProductCounter(List<Installed_Product_Measures__c> lstData, String tAction){

        NotificationSAP oNotificationSAP = new NotificationSAP();
            oNotificationSAP.tDirection = bl_NotificationSAP.DIRECTION;
            oNotificationSAP.lstData = lstData;
    
            // Set Salesforce.com Related Settings
            oNotificationSAP.tSFDC_Object = 'Installed_Product_Measures__c';
            oNotificationSAP.tSFDC_FieldName_Id = 'Id';
            oNotificationSAP.tSFDC_FieldName_SAPID = 'SAP_Measure_Point_ID__c';

            // Set WebMethods Related Settings
            oNotificationSAP.tWM_Object = '';
            oNotificationSAP.tWM_Operation = '';
            oNotificationSAP.tWM_Process = '';

            oNotificationSAP.tWebServiceName = 'InstProductCounter_NotificationSAP'; //TODO

        return sendNotificationToSAP(oNotificationSAP);

    }

    // Perform the actual sendDataToSAP
    public static boolean sendNotificationToSAP(NotificationSAP oNotificationSAP){

        Boolean bResult = true;

        try{
            
            clsUtil.bubbleException1();

            // Get the ID's of the processing records
            if (oNotificationSAP.setId_Record.size() == 0){
                Set<Id> setId_Record = new Set<Id>();
                for (sObject oData : oNotificationSAP.lstData){
                    setId_Record.add(String.valueOf(oData.get(oNotificationSAP.tSFDC_FieldName_Id)));
                }
                oNotificationSAP.setId_Record = setId_Record;
            }

            List<NotificationSAPLog__c> lstNotificationSAPLog = new List<NotificationSAPLog__c>();
            if ( (oNotificationSAP.oNotificationSAPLog == null) || (oNotificationSAP.oNotificationSAPLog.Id == null) ){
                // Create a NotificationSAPLog__c record for each processing record
                oNotificationSAP.oWebServiceSetting = loadWebServiceSetting(oNotificationSAP.tWebserviceName);
                oNotificationSAP.tStatus = 'NEW';
                lstNotificationSAPLog = ut_Log.createNotificationSAPLog(oNotificationSAP);
            }else{
                lstNotificationSAPLog.add(oNotificationSAP.oNotificationSAPLog);                    
            }
			
			// Change by Jesus for processing multiple retries at the same time. We need to check the Status of the new Notification individually.
            //if (oNotificationSAP.tStatus == 'NEW'){

                Map<Id, NotificationSAPLog__c> mapRecordId_NotificationSAPLog = new Map<Id, NotificationSAPLog__c>();
                for (NotificationSAPLog__c oNotificationSAPLog : lstNotificationSAPLog){
                    
                    if(oNotificationSAPLog.Status__c == 'NEW') mapRecordId_NotificationSAPLog.put(oNotificationSAPLog.Record_ID__c, oNotificationSAPLog);
                }
				
				if(mapRecordId_NotificationSAPLog.size() > 0){
					
	                // Call the Batch 
	                ba_NotificationSAP oBatch = new ba_NotificationSAP();
	                    oBatch.oNotificationSAP = oNotificationSAP;
	                    oBatch.mapRecordId_NotificationSAPLog = mapRecordId_NotificationSAPLog;
	                Database.executebatch(oBatch, 1);   // Batch Size needs to be 1 because we need to call the WebMethods Web Service record by record
				}
            //}

        }catch(Exception oEX){
            clsUtil.debug('Error occured in bl_NotificationSAP.sendNotificationToSAP on line ' + oEX.getLineNumber() + ' : ' + oEX.getMessage());
            bResult = false;
        }

        return bResult;

    }
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
    // Get the SOQL query to select the data that needs processing based on previous collect data
    //----------------------------------------------------------------------------------------
    public static String getSOQLToProcessData(NotificationSAP oNotificationSAP){

        String tSOQL = '';

        // Build the Query to select all data that needs processing
        tSOQL = 'SELECT ' + clsUtil.isNull(oNotificationSAP.tSFDC_FieldName_Id, 'Id');

        if (clsUtil.isNull(oNotificationSAP.tSFDC_FieldName_SAPID, '') != ''){
            tSOQL += ', ' + oNotificationSAP.tSFDC_FieldName_SAPID;
        }

        tSOQL += ' FROM ' + oNotificationSAP.tSFDC_Object;

        String tWhere = '';
        if (oNotificationSAP.setId_Record.size() > 0){
            String tIDs = '';
            for (Id id_Record : oNotificationSAP.setId_Record){
                tIDs += '\'' + id_Record + '\',';
            }
            tIDs = tIDs.left(tIDs.length() - 1);
            
            tWhere = ' WHERE ' + oNotificationSAP.tSFDC_FieldName_Id + ' in (' + tIDs + ')';
        }else{
            // Make sure no data is selected if no ID's are passed as a parameter
            tWhere = ' WHERE ' + oNotificationSAP.tSFDC_FieldName_Id + ' = null';
        }

        tSOQL += tWhere;

        return tSOQL;

    }
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
    //  Process the data with a PENDING status based on a NotificationSAPLog__c record
    //----------------------------------------------------------------------------------------
    public static boolean processPendingCallout(NotificationSAPLog__c oNotificationSAPLog){

        NotificationSAP oNotificationSAP = new NotificationSAP(oNotificationSAPLog);

        // Get the data that needs to be processed
        String tSOQL = getSOQLToProcessData(oNotificationSAP);
        oNotificationSAP.lstData = Database.Query(tSOQL);

        // Perform the Callout
        return sendNotificationToSAP(oNotificationSAP);

    }
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
    // CALL WEBSERVICE ON WEBMETHODS / SAP
    //----------------------------------------------------------------------------------------
    // Process a list of records
    public static boolean callWebMethod(NotificationSAP oNotificationSAP){
        return callWebMethod(oNotificationSAP, null);
    }
    
    public static boolean callWebMethod(NotificationSAP oNotificationSAP, Map<Id, NotificationSAPLog__c> mapRecordId_NotificationSAPLog){

        Boolean bResult = true;
        
        try{

            clsUtil.bubbleException2();

            // Loop through the list of data and perform a callWebMethod with a single record
            for (sObject oData : oNotificationSAP.lstData){

                if (mapRecordId_NotificationSAPLog != null){
                    NotificationSAPLog__c oNotificationSAPLog = mapRecordId_NotificationSAPLog.get(Id.valueOf(String.valueOf(oData.get(oNotificationSAP.tSFDC_FieldName_Id))));
                    oNotificationSAP = new bl_NotificationSAP.NotificationSAP(oNotificationSAPLog);
                }

                bResult = callWebMethod(oData, oNotificationSAP);
            }

        }catch(Exception oEX){
            clsUtil.debug('Error occured in bl_NotificationSAP.callWebMethod (multi) on line ' + oEX.getLineNumber() + ' : ' + oEX.getMessage());
            bResult = false;
        }

        return bResult;
    }

    public static boolean callWebMethod(sObject oData, NotificationSAP oNotificationSAP){
        Boolean bResult = true;

        try{

            clsUtil.bubbleException3();

            oNotificationSAP.tValue_Id = String.valueOf(clsUtil.isNull(oData.get(oNotificationSAP.tSFDC_FieldName_Id), ''));
            oNotificationSAP.tValue_SAPID = '';
            if (clsUtil.isNull(oNotificationSAP.tSFDC_FieldName_SAPID, '') != ''){
                oNotificationSAP.tValue_SAPID = String.valueOf(clsUtil.isNull(oData.get(oNotificationSAP.tSFDC_FieldName_SAPID), ''));
            }

            // Get the Settings for the Web-Service and the actual Salesforce.com Org
            oNotificationSAP.oWebServiceSetting = loadWebServiceSetting(oNotificationSAP.tWebserviceName);
            if (oNotificationSAP.oWebServiceSetting == null){
                oNotificationSAP.tStatus = 'ERROR';
                oNotificationSAP.tErrorDescription = 'Error in bl_NotificationSAP.callWebMethod - No Web Service Settings found in the Custom Setting "WebServiceSetting__c" for this Salesforce.com Org';
                ut_Log.upsertNotificationSAPLog(oNotificationSAP);
                return false;
            }

            // Build the Request XML
            oNotificationSAP.tRequestBody = tBuildRequestBody(oNotificationSAP);

            // Call the Web-Service
            bResult = bPerformCallout(oNotificationSAP);


        }catch(Exception oEX){
            clsUtil.debug('Error occured in bl_NotificationSAP.callWebMethod (single) on line ' + oEX.getLineNumber() + ' : ' + oEX.getMessage());
            bResult = false;
        }

        return bResult;
    }
    //----------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------
    // BUILD THE REQUEST XML FOR THE CALL-OUT
    // WebMethods only accepts 1 record per call
    //----------------------------------------------------------------------------------------
    private static String tBuildRequestBody(NotificationSAP oNotificationSAP){

        WebServiceSetting__c oWebServiceSetting = oNotificationSAP.oWebServiceSetting;

        String tXMLNS = oWebServiceSetting.XMLNS__c;
        String tWebServiceName = oWebServiceSetting.WebServiceName__c;
        String tDataName = oWebServiceSetting.DataName__c;

        String tRequestBody = '';

        if (oNotificationSAP.tSFDC_Object == 'SVMXC__Installed_Product__c'){

            tRequestBody += '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:' + tXMLNS + '>';
                tRequestBody += '<soapenv:Header/>';
                tRequestBody += '<soapenv:Body>';
                    tRequestBody += '<' + tWebServiceName + '>';
                        tRequestBody += '<' + tDataName + '>';
                            tRequestBody += '<INSTALL_PRODUCT_ID>' + String.escapeSingleQuotes(clsUtil.isNull(oNotificationSAP.tValue_SAPID, '')) + '</INSTALL_PRODUCT_ID>';
                            tRequestBody += '<SMAX_Instance_Id>' + oWebServiceSetting.Instance_Id__c + '</SMAX_Instance_Id>';
                        tRequestBody += '</' + tDataName + '>';
                    tRequestBody += '</' + tWebServiceName + '>';
                tRequestBody += '</soapenv:Body>';
            tRequestBody += '</soapenv:Envelope>';

        }else if (oNotificationSAP.tSFDC_Object == 'Installed_Product_Measures__c'){

            tRequestBody += '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:' + tXMLNS + '>';
                tRequestBody += '<soapenv:Header/>';
                tRequestBody += '<soapenv:Body>';
                    tRequestBody += '<' + tWebServiceName + '>';
                        tRequestBody += '<' + tDataName + '>';
                            tRequestBody += '<measurementDocID>' + String.escapeSingleQuotes(clsUtil.isNull(oNotificationSAP.tValue_Id, '')) + '</measurementDocID>';
                            tRequestBody += '<smaxInstanceID>' + oWebServiceSetting.Instance_Id__c + '</smaxInstanceID>';
                        tRequestBody += '</' + tDataName + '>';
                    tRequestBody += '</' + tWebServiceName + '>';
                tRequestBody += '</soapenv:Body>';
            tRequestBody += '</soapenv:Envelope>';

        }else if (oNotificationSAP.tSFDC_Object == 'Attachment'){

            tRequestBody += '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:' + tXMLNS + '>';
                tRequestBody += '<soapenv:Header/>';
                tRequestBody += '<soapenv:Body>';
                    tRequestBody += '<' + tWebServiceName + '>';
                        tRequestBody += '<' + tDataName + '>';
                            tRequestBody += '<AttachementId>' + String.escapeSingleQuotes(clsUtil.isNull(oNotificationSAP.tValue_Id, '')) + '</AttachementId>';
                            tRequestBody += '<SMAX_Instance_Id>' + oWebServiceSetting.Instance_Id__c + '</SMAX_Instance_Id>';
                        tRequestBody += '</' + tDataName + '>';
                    tRequestBody += '</' + tWebServiceName + '>';
                tRequestBody += '</soapenv:Body>';
            tRequestBody += '</soapenv:Envelope>';

        }else{

            tRequestBody += '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:' + tXMLNS + '>';
                tRequestBody += '<soapenv:Header/>';
                tRequestBody += '<soapenv:Body>';
                    tRequestBody += '<' + tWebServiceName + '>';
                        tRequestBody += '<' + tDataName + '>';
                            tRequestBody += '<Object>' + String.escapeSingleQuotes(oNotificationSAP.tWM_Object) + '</Object>';
                            tRequestBody += '<Operation>' + String.escapeSingleQuotes(oNotificationSAP.tWM_Operation) + '</Operation>';
                            tRequestBody += '<Process>' + String.escapeSingleQuotes(oNotificationSAP.tWM_Process) + '</Process>';
                            tRequestBody += '<SFDC_ID>' + String.escapeSingleQuotes(clsUtil.isNull(oNotificationSAP.tValue_Id, '')) + '</SFDC_ID>';
                            tRequestBody += '<SAP_ID>' + String.escapeSingleQuotes(clsUtil.isNull(oNotificationSAP.tValue_SAPID, '')) + '</SAP_ID>';
                            tRequestBody += '<SMAX_Instance_Id>' + oWebServiceSetting.Instance_Id__c + '</SMAX_Instance_Id>';
                        tRequestBody += '</' + tDataName + '>';
                    tRequestBody += '</' + tWebServiceName + '>';
                tRequestBody += '</soapenv:Body>';
            tRequestBody += '</soapenv:Envelope>';
        }

        return tRequestBody;

    }
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
    // WEBSERVICE CALLOUT METHODS
    //----------------------------------------------------------------------------------------
    private static boolean bPerformCallout(NotificationSAP oNotificationSAP){

        Boolean bResult = true;

        Boolean bError = false;
        oNotificationSAP.tStatus = '';
        oNotificationSAP.tErrorDescription = '';

        List<NotificationResponseFromSAP> lstNotificationResponseFromSAP = new List<NotificationResponseFromSAP>();

        // Make the WebService Call
        try{

            clsUtil.bubbleException4();

            oNotificationSAP.tAuthorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(Blob.valueOf(oNotificationSAP.oWebServiceSetting.Username__c + ':' + oNotificationSAP.oWebServiceSetting.Password__c));
            
            // Authentication
            Http oHTTP = new Http();

            // Prepare the request
            HttpRequest oHTTPRequest= new HttpRequest();
                oHTTPRequest.setMethod('POST');
                oHTTPRequest.setHeader('Content-Type', 'text/xml;charset=UTF-8');
                oHTTPRequest.setEndpoint(oNotificationSAP.oWebServiceSetting.EndPointURL__c);
                oHTTPRequest.setTimeout(oNotificationSAP.oWebServiceSetting.TimeOut__c.intValue());
                oHTTPRequest.setHeader('Authorization', oNotificationSAP.tAuthorizationHeader);
                oHTTPRequest.setBody(oNotificationSAP.tRequestBody);

            // Send the request
            HttpResponse oHTTPResponse = oHTTP.send(oHTTPRequest);

            // Get the response of the Callout and process it
            oNotificationSAP.tResponseBody = oHTTPResponse.getBody();

            // Process the Response
            lstNotificationResponseFromSAP = processResponse(oNotificationSAP.tResponseBody, oHTTPResponse.getStatusCode());


        }catch(Exception oEX){
            clsUtil.debug('Error occured in bl_NotificationSAP.bPerformCallout on line ' + oEX.getLineNumber() + ' : ' + oEX.getMessage());
            bError = true;
            oNotificationSAP.tStatus = 'FAILED';
            oNotificationSAP.tErrorDescription = oEX.getStackTraceString().replace('\n',' / ') + ' - ' + oEX.getMessage();
            bResult = false;
        }

        // Create a log record
        if (bError == false){
            if (lstNotificationResponseFromSAP.size() == 1){
                NotificationResponseFromSAP oNotificationResponseFromSAP = lstNotificationResponseFromSAP[0];
                if (oNotificationResponseFromSAP.tIsSuccess.toUpperCase() == 'TRUE'){
                    oNotificationSAP.tStatus = 'NEW';
                    oNotificationSAP.tErrorDescription = '';
                }else if (oNotificationResponseFromSAP.tIsSuccess.toUpperCase() == 'FALSE'){
                    oNotificationSAP.tStatus = 'FAILED';
                    oNotificationSAP.tErrorDescription = oNotificationResponseFromSAP.tMSG;
                }else{
                	oNotificationSAP.tStatus = 'FAILED';
                    oNotificationSAP.tErrorDescription = 'No response was received from WebMethods';
                }
            }else{
                oNotificationSAP.tStatus = 'FAILED';
                oNotificationSAP.tErrorDescription = 'Multiple responses received from Web Service';
            }
        }

        ut_Log.upsertNotificationSAPLog(oNotificationSAP);
        
        String responseSuccess;
        
        if(lstNotificationResponseFromSAP.size() == 1) responseSuccess = lstNotificationResponseFromSAP[0].tIsSuccess;
        else responseSuccess = 'FALSE';
        
        ut_Log.logOutboundMessage(oNotificationSAP.tValue_Id, oNotificationSAP.tValue_SAPId, JSON.serializePretty(oNotificationSAP.tRequestBody), JSON.serializePretty(lstNotificationResponseFromSAP), responseSuccess, 'sendNotificationSAP', oNotificationSAP.tSFDC_Object);

        return bResult;        
    }
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
    // WEBSERVICE CALLOUT METHODS - PROCESS THE RESPONSE
    //----------------------------------------------------------------------------------------
    private static List<NotificationResponseFromSAP> processResponse(String tResponse, Integer iStatusCode){

        List<NotificationResponseFromSAP> lstNotificationResponseFromSAP = new List<NotificationResponseFromSAP>();

        Dom.Document oDomDoc = new Dom.Document();
        oDomDoc.load(tResponse);
        Dom.XMLNode oDomXMLRoot = oDomDoc.getRootElement();

        if (iStatusCode == 200){
            // SUCCESS
            for (Dom.XMLNode oDomXMLnode_Body : oDomXMLRoot.getChildren()) {
                for (Dom.XMLNode oDomXMLnode_Response : oDomXMLnode_Body.getChildren()) {
                    for (Dom.XMLNode oDomXMLnode_Result : oDomXMLnode_Response.getChildren()) {
                        String tIsSuccess = oDomXMLnode_Result.getChildElement('isSuccess', null).getText();
                        String tMSG = oDomXMLnode_Result.getChildElement('msg', null).getText();
                        lstNotificationResponseFromSAP.add(new NotificationResponseFromSAP(tIsSuccess, tMSG));                    
                    }
                }
            }

        }else{
            // FAILURE
            String tMSG = '';
            for (Dom.XMLNode oDomXMLnode_Body : oDomXMLRoot.getChildren()) {
                
                String tName = clsUtil.isNull(oDomXMLnode_Body.getName(), '');
                String tValue = clsUtil.isNull(oDomXMLnode_Body.getText(), '');

                if (tName != ''){
                    if (tMSG != '') tMSG += ' -- ';
                    tMSG += tName + ' : ' + tValue;
                }

            }

            lstNotificationResponseFromSAP.add(new NotificationResponseFromSAP('FALSE', tMSG));                    

        }

        return lstNotificationResponseFromSAP;

    }
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
    // LOAD THE SETTINGS RELATED TO THE WEBMETHODS WEBSERVICES
    //----------------------------------------------------------------------------------------
    public static WebServiceSetting__c loadWebServiceSetting(String tWebServiceName){

        WebServiceSetting__c oWSS;

        String tOrgId = Userinfo.getOrganizationId().left(15);

        Map<String, WebServiceSetting__c> mapWebServiceSetting = WebServiceSetting__c.getAll();
        if (mapWebServiceSetting.containsKey(tWebserviceName)){
            if (mapWebServiceSetting.get(tWebserviceName).Org_Id__c == tOrgId){
                oWSS = mapWebServiceSetting.get(tWebserviceName);
            }
        }

        return oWSS;

    }    
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
    // WRAPPER - NOTIFICATIONSAP
    //----------------------------------------------------------------------------------------
    public class NotificationSAP {

        public NotificationSAP(){

            initialize();

        }

        public NotificationSAP(NotificationSAPLog__c aoNotificationSAPLog){

            initialize();

            tWM_Object = aoNotificationSAPLog.WM_Object_Name__c;
            tWM_Operation = aoNotificationSAPLog.WM_Operation__c;
            tWM_Process = aoNotificationSAPLog.WM_Process__c;
            
            tWebServiceName = aoNotificationSAPLog.WebServiceName__c;

            tSFDC_Object = aoNotificationSAPLog.SFDC_Object_Name__c;
            tSFDC_FieldName_Id = aoNotificationSAPLog.SFDC_Fieldname_ID__c;
            tSFDC_FieldName_SAPID = aoNotificationSAPLog.SFDC_Fieldname_SAPID__c;
            setId_Record = new Set<Id>();
            tSFDC_RecordID = aoNotificationSAPLog.Record_ID__c;

            oNotificationSAPLog = aoNotificationSAPLog;

            oWebServiceSetting = null;
            tAuthorizationHeader = aoNotificationSAPLog.Authorization_Header__c;
            tValue_Id = aoNotificationSAPLog.Record_ID__c;
            tValue_SAPID = aoNotificationSAPLog.Record_SAPID__c;
            tRequestBody = aoNotificationSAPLog.Request__c;
            tResponseBody = aoNotificationSAPLog.Response__c;

            tStatus = aoNotificationSAPLog.Status__c;
            tErrorDescription = aoNotificationSAPLog.Error_Description__c;
            tDirection = aoNotificationSAPLog.Direction__c;

            setId_Record.add(tValue_Id);

        }

        public List<sObject> lstData;

        public String tWM_Object;
        public String tWM_Operation;
        public String tWM_Process;

        public String tWebServiceName;

        public String tSFDC_Object;
        public String tSFDC_FieldName_Id;
        public String tSFDC_FieldName_SAPID;

        public Set<Id> setId_Record;
        public String tSFDC_RecordID;

        public NotificationSAPLog__c oNotificationSAPLog;

        public WebServiceSetting__c oWebServiceSetting;
        public String tAuthorizationHeader;
        public String tValue_Id;
        public String tValue_SAPId;
        public String tRequestBody;
        public String tResponseBody;

        public String tStatus;
        public String tErrorDescription;
        public String tDirection;

        public String tCalloutFailureEmailAddress;
        public Integer iCalloutMaxRetries;
        public Integer iCalloutRetryMinutes;

        private void initialize(){

            lstData = new List<sObject>();
            
            tWM_Object = '';
            tWM_Operation = '';
            tWM_Process = '';
            
            tWebServiceName = '';

            tSFDC_Object = '';
            tSFDC_FieldName_Id = '';
            tSFDC_FieldName_SAPID = '';
            setId_Record = new Set<Id>();
            tSFDC_RecordID = '';

            oNotificationSAPLog = new NotificationSAPLog__c();

            oWebServiceSetting = null;
            tAuthorizationHeader = '';
            tValue_Id = '';
            tValue_SAPID = '';
            tRequestBody = '';
            tResponseBody = '';

            tStatus = '';
            tErrorDescription = '';
            tDirection = '';

            tCalloutFailureEmailAddress = '';
            iCalloutMaxRetries = 0;
            iCalloutRetryMinutes = 60;
        }



    }
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
    // WRAPPER - WEBSERVICE RESPONSE
    //----------------------------------------------------------------------------------------
    public class NotificationResponseFromSAP {
        public NotificationResponseFromSAP(String tIsSuccess, String tMSG){
            this.tIsSuccess  = tIsSuccess;
            this.tMSG = tMSG;
        }

        public String tIsSuccess { get;set; }
        public String tMSG { get;set; }
    }
    //----------------------------------------------------------------------------------------

}
//--------------------------------------------------------------------------------------------------------------------