@RestResource(urlMapping='/Lookup')
global class TwoRing_PerformLookupController {
    @HttpPost
    global static List<SObject> performLookup(TwoRing_QueryObject query) {
		  return TwoRing_Repository.performLookup(query);
    }
}