public with sharing class ctrl_TimeReportingKronosView {
	
	public List<String> kronosKeys {get; set;}
	public List<String> reportDays {get; set;}
	public Map<String, Integer> timeReportMap {get; set;}
	
	public Time_Reporting__c contextUserHelper {get; set;}
	public User contextUser {get; set;}
	
	public Date startViewWeek {get; set;}
	public Date endViewWeek {get; set;}
	
	public Integer weekTotal {get; set;}
	
	public ctrl_TimeReportingKronosView(){
		
		//Context User init
		String contextUserId = ApexPages.currentPage().getParameters().get('userId');		
		if(contextUserId == null) contextUserId = UserInfo.getUserId();
		
		contextUser = [Select Id, Name from User where Id =:contextUserId];
		
		contextUserHelper = new Time_Reporting__c();
		contextUserHelper.OwnerId = contextUser.Id;
		
		//Week days init
		String refDay = ApexPages.currentPage().getParameters().get('refDay');
		
		if(refDay!=null){
			startViewWeek = Date.parse(refDay).toStartOfWeek();			
		}else{				
			startViewWeek = Date.today().toStartOfWeek();
		}
		
		endViewWeek = startViewWeek.addDays(6);
		
		//Table init
		loadTable();				
	}
	
	private void loadTable(){
											
		timeReportMap = new Map<String, Integer>();
		kronosKeys = new List<String>();
		reportDays = new List<String>();
		
		weekTotal = 0;
		
		for(Integer i=0; i<7; i++){
			reportDays.add(formatDate(startViewWeek.addDays(i)));
		}
		
		Set<String> keySet = new Set<String>();
				
		for(Time_Reporting__c report : [Select Date__c, Hours__c, K_Category__c,
					K_Project__c, Reporting_Activity__r.Name  from Time_Reporting__c where OwnerId=:contextUser.Id
					AND hours__c >0 AND Date__c >=:startViewWeek AND Date__c <=:endViewWeek]){
			
			String key =  report.K_Category__c + ' / ' +report.K_Project__c+' - '+report.Reporting_Activity__r.Name;
						
			if(keySet.add(key)){
				
				kronosKeys.add(key);
				
				for(Integer i=0; i<7; i++){
					
					Date selDay = startViewWeek.addDays(i);
									
					timeReportMap.put(key+'-'+formatDate(selDay), 0);
				}	
				//Key Total init
				timeReportMap.put(key, 0);
			}
						
			String dayKey = formatDate(report.Date__c);			
			
			Integer dayCount = timeReportMap.get(key+'-'+dayKey);						
			timeReportMap.put( key+'-'+dayKey, Integer.valueOf(dayCount + report.hours__c));
			
			//Key total
			Integer keyCount = timeReportMap.get(key);
			timeReportMap.put( key, Integer.valueOf(keyCount + report.hours__c));
									
			//Week total
			weekTotal += Integer.valueOf(report.hours__c);
		}	
		
		kronosKeys.sort();
	}
	
	private String formatDate(Date d){
		
		return getDayName(d)+' '+d.day()+'/'+d.month();
	}
	
	private String getDayName(Date day){
		
		Datetime myDate = Datetime.newInstance(day.year(), day.month(), day.day());  
		return myDate.format('EEE');  
	}
	
	public void nextWeek(){
		
		startViewWeek = startViewWeek.addDays(7);
		endViewWeek = endViewWeek.addDays(7);
								
		loadTable();		
	}
	
	public void prevWeek(){
		
		startViewWeek = startViewWeek.addDays(-7);
		endViewWeek = endViewWeek.addDays(-7);
		
		loadTable();		
	}
	
	public void changeContextUser(){
		
		if(contextUserHelper.OwnerId==null) contextUserHelper.OwnerId = contextUser.Id;		
		contextUser = [Select Id, Name from User where Id =:contextUserHelper.OwnerId];
		
		loadTable();		
	}
}