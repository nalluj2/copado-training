global without sharing class ctrl_SMAX_Interface_Monitor {
    
    transient public List<FailedRecord> reportItems {get; private set;}
    public Integer totalItems {get; private set;}
    
    public List<SelectOption> objectTypes {get; private set;}
    public String selectedObjectType {get; set;}
    
    public List<SelectOption> filterOptions {get; private set;}
    public String selectedFilterOption {get; set;}
    public String filterId {get; set;}
    
    private final Integer MAX_ITEMS = 150;
    
    public ctrl_SMAX_Interface_Monitor(){
    	
    	objectTypes = new List<SelectOption>();
    	objectTypes.add(new SelectOption('Attachment', 'Attachment'));
    	objectTypes.add(new SelectOption('SVMXC__Installed_Product__c', 'Installed Product'));
    	objectTypes.add(new SelectOption('Installed_Product_Measures__c', 'Installed Product Measure'));
    	objectTypes.add(new SelectOption('Case', 'Service Notification'));
    	objectTypes.add(new SelectOption('SVMXC__Service_Order__c', 'Service Order'));
    	
    	filterOptions = new List<SelectOption>();
    	filterOptions.add(new SelectOption('All', 'All Failures'));
    	filterOptions.add(new SelectOption('External_ID__c', 'SAP Id'));
    	filterOptions.add(new SelectOption('Internal_ID__c', 'SFDC Id'));
    	
    	Map<String, String> inputParams = ApexPages.currentPage().getParameters();
    	
    	String objectType = inputParams.get('objectType');
    	String filterOption = inputParams.get('filterOption');
    	String inputId = inputParams.get('filterId');
    	
    	if(objectType != null && filterOption != null && inputId != null){
    		
    		selectedObjectType = objectType;
    		selectedFilterOption = filterOption;
    		filterId = inputId;
    		
    		loadReport();
    		
    	}else{
    		
    		selectedObjectType = 'SVMXC__Service_Order__c';   	
    		selectedFilterOption = 'All';
    	}    	 	
    }
    
    public void loadReport(){
    	
    	String query = 'Select Id, Internal_ID__c, Last_Outbound_Notification__r.Outbound_Message_Request__c, Last_Outbound_Notification__r.Error_Description__c, (Select Id, Name, Status__c, Record_ID__c, WM_Process__c, Retries__c, LastModifiedDate from NotificationSAPLogs__r ORDER BY CreatedDate DESC, Id DESC) from Notification_Grouping__c where Scope__c = :selectedObjectType ';
    	
    	if(selectedFilterOption == 'All') query += ' AND Outbound_Status__c = \'Failure\' AND Archived__c = false '; 
    	else query += ' AND ' + selectedFilterOption + ' = \'' + filterId + '\'' ;
		
		query += ' LIMIT 150 ';
		
		Map<Id, Notification_Grouping__c> notificationMap = new Map<Id, Notification_Grouping__c>();
		
		for(Notification_Grouping__c grouping : Database.query(query)){
			
			notificationMap.put(grouping.Internal_ID__c, grouping);
		}
				
		reportItems = new List<FailedRecord>();
		
		if(selectedObjectType == 'SVMXC__Service_Order__c'){
						
			for(SVMXC__Service_Order__c sOrder : [Select Id, Name, SVMX_SAP_Service_Order_No__c, SVMXC__Order_Status__c, CreatedDate, LastModifiedDate from SVMXC__Service_Order__c where Id IN :notificationMap.keySet() ORDER BY CreatedDate]){
			    
			    NotificationSAPLog__c lastNotification = notificationMap.get(sOrder.Id).Last_Outbound_Notification__r;
				List<NotificationSAPLog__c> notifications = notificationMap.get(sOrder.Id).NotificationSAPLogs__r;
			    			    			        
			    FailedRecord reportItem = new FailedRecord();
			    reportItem.SAP_Id = sOrder.SVMX_SAP_Service_Order_No__c;
			    reportItem.SFDC_Id = sOrder.Id;
			    reportItem.Name = sOrder.Name;
			    reportItem.Status = sOrder.SVMXC__Order_Status__c;
			    reportItem.CreatedDate = sOrder.CreatedDate.format();
			    reportItem.LastmodifiedDate = sOrder.LastModifiedDate.format();
			    
			    reportItem.LastNotification = lastNotification;     
			    reportItem.Notifications = notifications;
			        
			    reportItems.add(reportItem);			        
			} 
			       	
		}else if(selectedObjectType == 'Case'){
			
			for(Case sNotification : [Select Id, CaseNumber, SVMX_SAP_Notification_Number__c, Status, CreatedDate, LastModifiedDate from Case where Id IN :notificationMap.keySet() ORDER BY CreatedDate]){
    			
    			NotificationSAPLog__c lastNotification = notificationMap.get(sNotification.Id).Last_Outbound_Notification__r;
				List<NotificationSAPLog__c> notifications = notificationMap.get(sNotification.Id).NotificationSAPLogs__r;
			        
		        FailedRecord reportItem = new FailedRecord();
		        reportItem.SAP_Id = sNotification.SVMX_SAP_Notification_Number__c;
		        reportItem.SFDC_Id = sNotification.Id;
		        reportItem.Name = sNotification.CaseNumber;
		        reportItem.Status = sNotification.Status;
		        reportItem.CreatedDate = sNotification.CreatedDate.format();
		        reportItem.LastmodifiedDate = sNotification.LastModifiedDate.format();
		        
		        reportItem.LastNotification = lastNotification;  
		        reportItem.Notifications = notifications;
		        
		        reportItems.add(reportItem);		    			           
			}   
			
		}else if(selectedObjectType == 'Attachment'){
			
			for(Attachment attach : [Select Id, Name, CreatedDate, ParentId, Parent.Name, LastModifiedDate from Attachment where Id IN :notificationMap.keySet() ORDER BY CreatedDate]){
    			
    			NotificationSAPLog__c lastNotification = notificationMap.get(attach.Id).Last_Outbound_Notification__r;
				List<NotificationSAPLog__c> notifications = notificationMap.get(attach.Id).NotificationSAPLogs__r;
			        
		        FailedRecord reportItem = new FailedRecord();
		        reportItem.SAP_Id = '-';
		        reportItem.SFDC_Id = attach.Id;
		        reportItem.Name = attach.Name;
		        reportItem.Status = '-';
		        reportItem.RelatedRecord = attach.ParentId;
		        reportItem.RelatedRecordName = attach.Parent.Name;
		        reportItem.CreatedDate = attach.CreatedDate.format();
		        reportItem.LastmodifiedDate = attach.LastModifiedDate.format();
		        
		        reportItem.LastNotification = lastNotification;  
		        reportItem.Notifications = notifications;
		        
		        reportItems.add(reportItem);			           
			} 
			
		}else if(selectedObjectType == 'SVMXC__Installed_Product__c'){
			
			for(SVMXC__Installed_Product__c instProd : [Select Id, Name, SVMX_SAP_Equipment_ID__c, CreatedDate, LastModifiedDate from SVMXC__Installed_Product__c where Id IN :notificationMap.keySet() ORDER BY CreatedDate]){
    			
    			NotificationSAPLog__c lastNotification = notificationMap.get(instProd.Id).Last_Outbound_Notification__r;
				List<NotificationSAPLog__c> notifications = notificationMap.get(instProd.Id).NotificationSAPLogs__r;
			        
		        FailedRecord reportItem = new FailedRecord();
		        reportItem.SAP_Id = instProd.SVMX_SAP_Equipment_ID__c;
		        reportItem.SFDC_Id = instProd.Id;
		        reportItem.Name = instProd.Name;
		        reportItem.Status = '-';
		        reportItem.CreatedDate = instProd.CreatedDate.format();
		        reportItem.LastmodifiedDate = instProd.LastModifiedDate.format();
		        
		        reportItem.LastNotification = lastNotification;  
		        reportItem.Notifications = notifications;
		        
		        reportItems.add(reportItem);			    
			} 
			
		}else if(selectedObjectType == 'Installed_Product_Measures__c'){
			
			for(Installed_Product_Measures__c prodMeasure : [Select Id, Name, SAP_Measure_Point_ID__c, Installed_Product__c, Installed_Product__r.Name,  CreatedDate, LastModifiedDate from Installed_Product_Measures__c where Id IN :notificationMap.keySet() ORDER BY CreatedDate]){
    
    			NotificationSAPLog__c lastNotification = notificationMap.get(prodMeasure.Id).Last_Outbound_Notification__r;
				List<NotificationSAPLog__c> notifications = notificationMap.get(prodMeasure.Id).NotificationSAPLogs__r;
			         
		        FailedRecord reportItem = new FailedRecord();
		        reportItem.SAP_Id = prodMeasure.SAP_Measure_Point_ID__c;
		        reportItem.SFDC_Id = prodMeasure.Id;
		        reportItem.Name = prodMeasure.Name;
		        reportItem.Status = '-';
		        reportItem.RelatedRecord = prodMeasure.Installed_Product__c;
		        reportItem.RelatedRecordName = prodMeasure.Installed_Product__r.Name;
		        reportItem.CreatedDate = prodMeasure.CreatedDate.format();
		        reportItem.LastmodifiedDate = prodMeasure.LastModifiedDate.format();
		        
		        reportItem.LastNotification = lastNotification;  
		        reportItem.Notifications = notifications;
		        
		        reportItems.add(reportItem);			    
			} 			
		}
					
		totalItems = reportItems.size();
		
		if(totalItems == MAX_ITEMS){
			
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Maximum number of items to display reached'));
		}
    }
            
    @RemoteAction
	global static String retryItem(String lastNotifId){
    	    	
    	NotificationSAPLog__c lastNotification = [Select Status__c, Record_ID__c, WebServiceName__c from NotificationSAPLog__c where Id = :lastNotifId];
    	
    	if(lastNotification.WebServiceName__c == 'ServiceOrder_NotificationSAP' || lastNotification.WebServiceName__c == 'ServiceOrderComplete_NotificationSAP'){
    		
    		SVMXC__Service_Order__c sOrder = [SELECT Id, SVMX_SAP_Service_Order_No__c from SVMXC__Service_Order__c where Id = :lastNotification.Record_ID__c];
    		
    		if(lastNotification.WebServiceName__c == 'ServiceOrder_NotificationSAP'){
    			    			 
				bl_NotificationSAP.sendNotificationToSAP_ServiceOrder(new List<SVMXC__Service_Order__c>{sOrder}, 'UPDATE');				
    			
    		}else{
    			
    			bl_NotificationSAP.sendNotificationToSAP_ServiceOrder_Completed(new List<SVMXC__Service_Order__c>{sOrder}, 'UPDATE');
    		}
    		    		
    	}else if(lastNotification.WebServiceName__c == 'Case_NotificationSAP'){
    		
    		Case sNotification = [SELECT Id, SVMX_SAP_Notification_Number__c from Case where Id = :lastNotification.Record_ID__c];
    		
    		bl_NotificationSAP.sendNotificationToSAP_Case(new List<Case>{sNotification}, 'UPDATE');
    		
    	}else if(lastNotification.WebServiceName__c == 'InstalledProduct_NotificationSAP'){
    		
    		SVMXC__Installed_Product__c instProduct = [SELECT Id, SVMX_SAP_Equipment_ID__c from SVMXC__Installed_Product__c where Id = :lastNotification.Record_ID__c];
    		
    		bl_NotificationSAP.sendNotificationToSAP_InstalledProduct(new List<SVMXC__Installed_Product__c>{instProduct}, 'UPDATE');
    		
    	}else if(lastNotification.WebServiceName__c == 'InstProductCounter_NotificationSAP'){
    		
    		Installed_Product_Measures__c prodMeasure = [SELECT Id, SAP_Measure_Point_ID__c from Installed_Product_Measures__c where Id = :lastNotification.Record_ID__c];
    		
    		bl_NotificationSAP.sendNotificationToSAP_InstalledProductCounter(new List<Installed_Product_Measures__c>{prodMeasure}, 'UPDATE');
    		
    	}else if(lastNotification.WebServiceName__c == 'Attachment_NotificationSAP'){
    	
    		Attachment att = [SELECT Id from Attachment where Id = :lastNotification.Record_ID__c];
    		
    		bl_NotificationSAP.sendNotificationToSAP_Attachment(new List<Attachment>{att}, 'UPDATE');
    		
    	}else{
    		    		
    		return 'Error : Could not identify a valid process to retry';
    	}
    	
    	return 'Success';  	
    }
    
    public class FailedRecord{
    
    	public String SAP_Id  {get; set;}
    	public String SFDC_Id  {get; set;}
    	public String Name  {get; set;}
    	public String Status  {get; set;}
    	public String RelatedRecord {get; set;}
    	public String RelatedRecordName {get; set;}
    	public String CreatedDate  {get; set;}
    	public String LastmodifiedDate  {get; set;}
    	
    	public NotificationSAPLog__c LastNotification  {get; set;} 
    	public List<NotificationSAPLog__c> Notifications  {get; set;}    
    }
}