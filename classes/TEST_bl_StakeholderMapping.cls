/**
 * Created by dijkea2 on 27-1-2017.
 * Change Log       : PMT-22787: MITG EMEA User Setup - Changed record type from MITG_Emerging_Markets_Opportunity_Stakeholder to MITG_EMEA_Opportunity_Stakeholder
 */

@IsTest
private class TEST_bl_StakeholderMapping {

    @testSetup
    static void setup() {
        clsTestData_Opportunity.iRecord_StakeholderMapping = 2;
		clsTestData_Account.createAccount(false);
			clsTestData_Account.oMain_Account.isSales_Force_Account__c = false;
		insert clsTestData_Account.oMain_Account;
        clsTestData_Opportunity.createStakeholderMapping();
    }

    static testMethod void test_populateMobileId_true() {

        List<Stakeholder_Mapping__c> stakeholderMappingList;

        stakeholderMappingList = [SELECT Id, Mobile_ID__c FROM Stakeholder_Mapping__c];
        System.assertEquals(2, stakeholderMappingList.size() );

        for(Stakeholder_Mapping__c sm : stakeholderMappingList){
            sm.Mobile_ID__c = null;
        }
        update stakeholderMappingList;

        stakeholderMappingList = [
            SELECT Id, Mobile_ID__c
            FROM Stakeholder_Mapping__c
        ];
        System.assertNotEquals(null, stakeholderMappingList[0].Mobile_ID__c );
    }

    static testMethod void test_populateMobileId_false() {

		clsTestData_Account.createAccount(false);
			clsTestData_Account.oMain_Account.isSales_Force_Account__c = false;
		insert clsTestData_Account.oMain_Account;
        Stakeholder_Mapping__c sm = clsTestData_Opportunity.createStakeholderMapping(false)[0];

        //PMT-22787: MITG EMEA User Setup - Changed record type from MITG_Emerging_Markets_Opportunity_Stakeholder to MITG_EMEA_Opportunity_Stakeholder
        //sm.RecordTypeId =  RecordTypeMedtronic.getRecordTypeByDevName('Stakeholder_Mapping__c', 'MITG_Emerging_Markets_Opportunity_Stakeholder').Id;
        sm.RecordTypeId =  RecordTypeMedtronic.getRecordTypeByDevName('Stakeholder_Mapping__c', 'MITG_EMEA_Opportunity_Stakeholder').Id;
        sm.Mobile_ID__c = null;

        insert sm;

        System.assertEquals(null, sm.Mobile_ID__c);
    }



}