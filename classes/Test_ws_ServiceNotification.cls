//  Author           : Tom Ansley
//  Created Date     : 02-29-2016
//  Description      : APEX Test Class to test the logic in ws_ServiceNotificationSAPService
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
@isTest
private class Test_ws_ServiceNotification {
	
	@testSetup
    static void createTestData() {
        
        // Create Custom Setting - WebService Setting Data
        clsTestData.createCustomSettingData_WebServiceSetting(true);

		clsTestData.createCustomSettingData_WebServiceMappings(true);
		
		clsTestData.createCustomSettingData_SupportedTypes(true);
		
        clsTestData.createAccountData(false);
        
        Account acct = clsTestData.oMain_Account;
        acct.SAP_Id__c = '0001928641';
        insert acct;
        
        // Create Installed Product Data
        clsTestData.createSVMXCInstalledProductData(false);
        SVMXC__Installed_Product__c product = clsTestData.oMain_SVMXCInstalledProduct;
        product.SVMX_SAP_Equipment_ID__c = '000000000000006568';
        insert product;
        
        SVMX_Damage_Cause_Code__c codeDamage = new SVMX_Damage_Cause_Code__c();
        codeDamage.SVMX_Cause_Code_Group__c = 'DSR00005';
        codeDamage.SVMX_Damage_Code__c = '299';
        codeDamage.SVMX_Code_Type__c = 'Damage';
        
        SVMX_Damage_Cause_Code__c codeCause = new SVMX_Damage_Cause_Code__c();
        codeCause.SVMX_Cause_Code_Group__c = 'DSR00005';
        codeCause.SVMX_Code__c = '300';
        codeCause.SVMX_Code_Type__c = 'Cause';
        
        insert new List<SVMX_Damage_Cause_Code__c>{codeDamage, codeCause};
    }
    
    @isTest
    static void testInsertServiceNotificationWS()
    {
    	ws_ServiceNotificationSAPService.SAPServiceNotification notification = new ws_ServiceNotificationSAPService.SAPServiceNotification();
    	
		notification.SAP_NOTIFICATION_NUMBER   = '000300500871';
		notification.SAP_NOTIFICATION_TYPE     = 'ZC';
        notification.SAP_EQUIPMENT_DESCRIPTION = 'Launcher, 6F, JL4.0 (MPRWM) - Test';
        notification.QUESTION_1                = 'Y';
        notification.QUESTION_2                = 'N';
        notification.QUESTION_3                = 'Y';
        notification.MATERIAL                  = '006739785322222';
        notification.SAP_EQUIPMENT_NUMBER      = '000000000000006568';
        notification.SERIAL_LOT_NUMBER         = '00000000036078768';
        notification.PREFERRED_START_DATE      = '20140112';
        notification.PREFERRED_START_TIME      = '010101';
        notification.BREAKDOWN_INDICATOR       = 'TRUE';
        notification.PO_DATE                   = '20140112';
    	
    	//ACCOUNTS
    	notification.accounts = new List<ws_ServiceNotificationSAPService.SAPServiceNotificationAccount>();    	
    	
    	ws_ServiceNotificationSAPService.SAPServiceNotificationAccount acct = new ws_ServiceNotificationSAPService.SAPServiceNotificationAccount();
    	acct.ACCOUNT_NUMBER = '0001928641';
    	acct.ACCOUNT_CITY = 'Heerlen';
        acct.ACCOUNT_COUNTRY = 'NL';
        acct.ACCOUNT_NAME = 'Atrium Medisch Centrum Heerlen';
        acct.ACCOUNT_STREET = 'Postbus 4446';
        acct.ACCOUNT_TELEPHONE = '(909)123-1234';
        acct.ACCOUNT_TYPE = 'SP';
        acct.ACCOUNT_ZIP_CODE = '6401 CX';
    	
    	notification.accounts.add(acct);
    	
    	//ITEMS
    	notification.items = new List<ws_ServiceNotificationSAPService.SAPServiceNotificationItem>();

    	ws_ServiceNotificationSAPService.SAPServiceNotificationItem item = new ws_ServiceNotificationSAPService.SAPServiceNotificationItem();
    	item.CODE_GROUP_PROBLEM = 'DSR00005';
    	item.PROBLEM_OR_DAMAGE_CODE = '299';

		//ITEM CAUSES
		item.causes = new List<ws_ServiceNotificationSAPService.SAPServiceNotificationItemCause>();
		
		ws_ServiceNotificationSAPService.SAPServiceNotificationItemCause cause = new ws_ServiceNotificationSAPService.SAPServiceNotificationItemCause();
		cause.CAUSE_GROUP = 'DSR00005';
		cause.CAUSE_CODE = '300';
		cause.CAUSE_TEXT = 'Cause Text';
		cause.CAUSE_LONG_TEXT = 'Long Text';

		notification.items.add(item);
    	
    	
        // Load User Data - SAP Interface
		User oUser = [SELECT Id, Name, isActive FROM User WHERE (Profile.Name = 'SAP Interface') AND IsActive = true LIMIT 1];

        Test.startTest();

        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new TEST_bl_NotificationSAP_Mock());  

        System.runAs(oUser){
        	
        	//create notification
    		ws_ServiceNotificationSAPService.SAPServiceNotification response = ws_ServiceNotificationSAPService.upsertServiceNotification(notification);
    		
    		System.assertEquals('TRUE', response.DISTRIBUTION_STATUS, response.DIST_ERROR_DETAILS);
    		
    		List<Case> createdCase = [Select Id from Case where SVMX_SAP_Notification_Number__c = '000300500871'];
    		System.assert(createdCase.size() == 1);
    		
    		//update invalid notification
    		acct.ACCOUNT_NUMBER = '12345'; //An invalid number;
    		response.accounts = new List<ws_ServiceNotificationSAPService.SAPServiceNotificationAccount>();    	
   			response.accounts.add(acct);
    		
    		response = ws_ServiceNotificationSAPService.upsertServiceNotification(response);
    		
    		System.assert(response.DISTRIBUTION_STATUS == 'FALSE');
    		
        	//update notification
        	acct.ACCOUNT_NUMBER = '0001928641';
    		response = ws_ServiceNotificationSAPService.upsertServiceNotification(notification);
    		
    		System.assertEquals('TRUE', response.DISTRIBUTION_STATUS, response.DIST_ERROR_DETAILS);
    		
        }
    	
    	Test.stopTest();
    	
    }
    
    @isTest
    static void testGetServiceNotificationWS()
    {
    	ws_ServiceNotificationSAPService.SAPServiceNotification notification = new ws_ServiceNotificationSAPService.SAPServiceNotification();
    	
		notification.SAP_NOTIFICATION_NUMBER   = '000300500871';
		notification.SAP_NOTIFICATION_TYPE     = 'ZC';
        notification.SAP_EQUIPMENT_DESCRIPTION = 'Launcher, 6F, JL4.0 (MPRWM) - Test';
        notification.QUESTION_1                = 'Y';
        notification.QUESTION_2                = 'N';
        notification.QUESTION_3                = 'Y';
        notification.MATERIAL                  = '006739785322222';
        notification.SAP_EQUIPMENT_NUMBER      = '000000000000006568';
        notification.SERIAL_LOT_NUMBER         = '00000000036078768';
    	
    	notification.accounts = new List<ws_ServiceNotificationSAPService.SAPServiceNotificationAccount>();    	
    	
    	ws_ServiceNotificationSAPService.SAPServiceNotificationAccount acct = new ws_ServiceNotificationSAPService.SAPServiceNotificationAccount();
    	acct.ACCOUNT_NUMBER = '0001928641';
    	acct.ACCOUNT_CITY = 'Heerlen';
        acct.ACCOUNT_COUNTRY = 'NL';
        acct.ACCOUNT_NAME = 'Atrium Medisch Centrum Heerlen';
        acct.ACCOUNT_STREET = 'Postbus 4446';
        acct.ACCOUNT_TELEPHONE = '(909)123-1234';
        acct.ACCOUNT_TYPE = 'SP';
        acct.ACCOUNT_ZIP_CODE = '6401 CX';
    	
    	notification.accounts.add(acct);
    	
        // Load User Data - SAP Interface
		User oUser = [SELECT Id, Name, isActive FROM User WHERE (Profile.Name = 'SAP Interface') AND IsActive = true LIMIT 1];

        Test.startTest();

        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new TEST_bl_NotificationSAP_Mock());  

        System.runAs(oUser){
    		
    		//create notification
    		ws_ServiceNotificationSAPService.SAPServiceNotification response = ws_ServiceNotificationSAPService.upsertServiceNotification(notification);
    		
    		//get notification
    		Case cse = [SELECT Id FROM Case Order By CreatedDate LIMIT 1];
    		
    		//get notification update
    		response = ws_ServiceNotificationSAPService.getServiceNotificationUpdate(cse.Id);
    		
    		//get invalid notification update
    		response = ws_ServiceNotificationSAPService.getServiceNotificationUpdate('Invalid Id');
    		
 		   	System.debug('OUTPUT - ' + response);
        }
    	
    	Test.stopTest();
    	
    }

    @isTest
    static void testUpdateServiceNotificationAckWS()
    {
    	ws_ServiceNotificationSAPService.SAPServiceNotification notification = new ws_ServiceNotificationSAPService.SAPServiceNotification();
    	
		notification.SAP_NOTIFICATION_NUMBER   = '000300500871';
		notification.SAP_NOTIFICATION_TYPE     = 'ZC';
        notification.SAP_EQUIPMENT_DESCRIPTION = 'Launcher, 6F, JL4.0 (MPRWM) - Test';
        notification.QUESTION_1                = 'Y';
        notification.QUESTION_2                = 'N';
        notification.QUESTION_3                = 'Y';
        notification.MATERIAL                  = '006739785322222';
        notification.SAP_EQUIPMENT_NUMBER      = '000000000000006568';
        notification.SERIAL_LOT_NUMBER         = '00000000036078768';
    	
    	notification.accounts = new List<ws_ServiceNotificationSAPService.SAPServiceNotificationAccount>();    	
    	
    	ws_ServiceNotificationSAPService.SAPServiceNotificationAccount acct = new ws_ServiceNotificationSAPService.SAPServiceNotificationAccount();
    	acct.ACCOUNT_NUMBER = '0001928641';
    	acct.ACCOUNT_CITY = 'Heerlen';
        acct.ACCOUNT_COUNTRY = 'NL';
        acct.ACCOUNT_NAME = 'Atrium Medisch Centrum Heerlen';
        acct.ACCOUNT_STREET = 'Postbus 4446';
        acct.ACCOUNT_TELEPHONE = '(909)123-1234';
        acct.ACCOUNT_TYPE = 'SP';
        acct.ACCOUNT_ZIP_CODE = '6401 CX';
    	
    	notification.accounts.add(acct);
    	
        // Load User Data - SAP Interface
		User oUser = [SELECT Id, Name, isActive FROM User WHERE (Profile.Name = 'SAP Interface') AND IsActive = true LIMIT 1];

        Test.startTest();

        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new TEST_bl_NotificationSAP_Mock());  

        System.runAs(oUser){
    		
    		//create notification
    		ws_ServiceNotificationSAPService.SAPServiceNotification response = ws_ServiceNotificationSAPService.upsertServiceNotification(notification);
    		
    		//get notification
    		Case cse = [SELECT Id FROM Case Order By CreatedDate LIMIT 1];
    		
    		ws_ServiceNotificationSAPService.SAPServiceNotificationAck ack = new ws_ServiceNotificationSAPService.SAPServiceNotificationAck();
    		ack.SAP_ID = '000300500871';
    		ack.SFDC_ID = cse.Id;
    		ack.DISTRIBUTION_STATUS = 'TRUE';
    		
    		ack = ws_ServiceNotificationSAPService.updateServiceNotificationAcknowledgement(ack);
 		   	System.debug('OUTPUT - ' + ack);
        }
    	
    	Test.stopTest();
    	
    }
    
    @isTest
    static void testInsertServiceNotificationNotSupportedWS()
    {
    	ws_ServiceNotificationSAPService.SAPServiceNotification notification = new ws_ServiceNotificationSAPService.SAPServiceNotification();
    	
		notification.SAP_NOTIFICATION_NUMBER   = '000300500871';
		notification.SAP_NOTIFICATION_TYPE     = 'ZA';
        notification.SAP_EQUIPMENT_DESCRIPTION = 'Launcher, 6F, JL4.0 (MPRWM) - Test';
        notification.QUESTION_1                = 'Y';
        notification.QUESTION_2                = 'N';
        notification.QUESTION_3                = 'Y';
        notification.MATERIAL                  = '006739785322222';
        notification.SAP_EQUIPMENT_NUMBER      = '000000000000006568';
        notification.SERIAL_LOT_NUMBER         = '00000000036078768';
        notification.PREFERRED_START_DATE      = '20140112';
        notification.PREFERRED_START_TIME      = '010101';
        notification.BREAKDOWN_INDICATOR       = 'TRUE';
        notification.PO_DATE                   = '20140112';
    	
    	//ACCOUNTS
    	notification.accounts = new List<ws_ServiceNotificationSAPService.SAPServiceNotificationAccount>();    	
    	
    	ws_ServiceNotificationSAPService.SAPServiceNotificationAccount acct = new ws_ServiceNotificationSAPService.SAPServiceNotificationAccount();
    	acct.ACCOUNT_NUMBER = '0001928641';
    	acct.ACCOUNT_CITY = 'Heerlen';
        acct.ACCOUNT_COUNTRY = 'NL';
        acct.ACCOUNT_NAME = 'Atrium Medisch Centrum Heerlen';
        acct.ACCOUNT_STREET = 'Postbus 4446';
        acct.ACCOUNT_TELEPHONE = '(909)123-1234';
        acct.ACCOUNT_TYPE = 'SP';
        acct.ACCOUNT_ZIP_CODE = '6401 CX';
    	
    	notification.accounts.add(acct);
    	
    	//ITEMS
    	notification.items = new List<ws_ServiceNotificationSAPService.SAPServiceNotificationItem>();

    	ws_ServiceNotificationSAPService.SAPServiceNotificationItem item = new ws_ServiceNotificationSAPService.SAPServiceNotificationItem();
    	item.CODE_GROUP_PROBLEM = 'DSR00005';
    	item.PROBLEM_OR_DAMAGE_CODE = '299';

		//ITEM CAUSES
		item.causes = new List<ws_ServiceNotificationSAPService.SAPServiceNotificationItemCause>();
		
		ws_ServiceNotificationSAPService.SAPServiceNotificationItemCause cause = new ws_ServiceNotificationSAPService.SAPServiceNotificationItemCause();
		cause.CAUSE_GROUP = 'DSR00005';
		cause.CAUSE_CODE = '300';
		cause.CAUSE_TEXT = 'Cause Text';
		cause.CAUSE_LONG_TEXT = 'Long Text';

		notification.items.add(item);
    	
    	
        // Load User Data - SAP Interface
		User oUser = [SELECT Id, Name, isActive FROM User WHERE (Profile.Name = 'SAP Interface') AND IsActive = true LIMIT 1];

        Test.startTest();

        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new TEST_bl_NotificationSAP_Mock());  

        System.runAs(oUser){
        	
        	//create notification
    		ws_ServiceNotificationSAPService.SAPServiceNotification response = ws_ServiceNotificationSAPService.upsertServiceNotification(notification);
    		
    		System.assert(response.DISTRIBUTION_STATUS == 'TRUE');
    		System.assert(response.DIST_ERROR_DETAILS == 'Notification Type not supported. Action skipped.');
    		
    		List<Case> createdCase = [Select Id from Case where SVMX_SAP_Notification_Number__c = '000300500871'];
    		System.assert(createdCase.size() == 0);
        }
    	
    	Test.stopTest();
    	
    }

}