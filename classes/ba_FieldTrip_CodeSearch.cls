/**
 * Created by dijkea2 on 20-9-2016.
 */

global with sharing class ba_FieldTrip_CodeSearch implements Database.Batchable<SObject>, Database.Stateful {

    private Integer countRecords = 0;

    private List<String> batchList;
	private Set<String> searchSet;	
	private Map<String, FieldAnalyse> fieldAnalyseMap;	        
    private String sObjectName;
        
    private String objectName;
    private String objectField;
    
    global ba_FieldTrip_CodeSearch(List<String> batchList, Set<String> searchFields, Map<String, FieldAnalyse> outputMap, String objectName) {
        
        this.batchList = batchList;
        this.searchSet = searchFields;
        this.sObjectName = objectName;
        
        if(outputMap != null) fieldAnalyseMap = outputMap;       
        else{

        	fieldAnalyseMap = new Map<String, FieldAnalyse>();
	        
	        for(String field : searchSet){
	
	 	       fieldAnalyseMap.put(field.toLowerCase(), new FieldAnalyse(field.toLowerCase()));
	        }
        }
    }
        
    global Database.QueryLocator start(Database.BatchableContext ctx){
        
        String name;
        
        if(batchList.size() > 0){
            
            name = batchList[0];
            batchList.remove(0);
        }
		
		String query;
		
        if(name == 'ApexClass'){
            
            query = 'SELECT Id,Name,Body,LastModifiedBy.Name,LastModifiedDate From ApexClass WHERE NamespacePrefix = \'\'';
            objectField = 'Body';
            objectName = 'ApexClass';
            
        } else if (name == 'ApexPage'){
            
            query = 'SELECT Id,Name,Markup,LastModifiedBy.Name,LastModifiedDate From ApexPage WHERE NamespacePrefix = \'\'';
            objectField = 'Markup';
            objectName = 'ApexPage';
            
        } else if(name == 'ApexTrigger'){
            
            query = 'SELECT Id,Name,Body,LastModifiedBy.Name,LastModifiedDate From ApexTrigger WHERE NamespacePrefix = \'\'';
            objectField = 'Body';
            objectName = 'ApexTrigger';
            
        } else if (name == 'ApexComponent'){
            
            query = 'SELECT Id,Name,Markup,LastModifiedBy.Name,LastModifiedDate From ApexComponent WHERE NamespacePrefix = \'\'';
            objectField = 'Markup';
            objectName = 'ApexComponent';
        }
        
        if(Test.isRunningTest() == true) query += ' LIMIT 200';
        
        return Database.getQueryLocator(query);
    }


    global void execute(Database.BatchableContext ctx, List<SObject> resultList){
     
        for(Sobject  s : resultList){
                
        	//Counting the records
            countRecords++;
                                
            String componentBody = ((String)s.get(objectField)).toLowerCase();
                
            for(String field : searchSet){
                                
                if(componentBody.indexOf(field.toLowerCase()) != -1){
            	
            	    FieldAnalyse fieldAnalyse = fieldAnalyseMap.get(field.toLowerCase());

                    CodeSearch codeSearch = new CodeSearch( s.Id, (String) s.get('Name'));
                    codeSearch.type = objectName;
                    codeSearch.cnt = componentBody.countMatches(field.toLowerCase());                
                   	codeSearch.last_modified_date = (Datetime) s.get('LastModifiedDate');
                   	
                    //Add to List
                   	fieldAnalyse.code_searchs.add(codeSearch);
                }
            }
        }        
    }
   
    global void finish(Database.BatchableContext BC){

        if(batchList.size() > 0){

            ba_FieldTrip_CodeSearch handler = new ba_FieldTrip_CodeSearch(batchList, searchSet, fieldAnalyseMap, sObjectName);            
            Database.executeBatch(handler, 200);

        } else {

        	String msg = getMessage(BC);

            Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
            Blob csvBlob = Blob.valueOf( getAttachmentBody() );
            String suffix = Datetime.now().format('yyyyMMdd_HH:mm:ss');
            string csvname = 'Fieldtrip_'+ sObjectName + '_' + suffix + '.csv';
            csvAttc.setFileName(csvname);
            csvAttc.setBody(csvBlob);

            Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
            String[] toAddresses = new list<string> {UserInfo.getUserEmail()};

            email.setSubject('Fieldtrip' + objectName);
            email.setToAddresses( toAddresses );
            email.setPlainTextBody(msg);
            email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
            Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
        }
    }

    /**
     * Helpers
     *
     */
    public String getAttachmentBody(){

        String              body;
        List<String>        lineValueList;
        List<String>        lineList = new List<String>();
        Map<String,String>  fieldMapping = new Map<String,String>();

        fieldMapping.put('Field Name','');
        fieldMapping.put('Type','');
        fieldMapping.put('Name','cnt');
        fieldMapping.put('Count','cnt');
        fieldMapping.put('Last modified date','last_modified_date');        
        fieldMapping.put('Id','id');

        String header = String.join(new List<String>(fieldMapping.keySet()), ',');
        lineList.add(header);

        for(String key : fieldAnalyseMap.keySet()){
            
            lineValueList = new List<String>();
            
            //Collumn I
            lineValueList.add(key);

            lineList.add(String.join(lineValueList, ','));

            for(CodeSearch cs : fieldAnalyseMap.get(key).code_searchs){

                lineValueList = new List<String>();
                lineValueList.add('');
                lineValueList.add(cs.type);
                lineValueList.add(cs.name);
                lineValueList.add(String.valueOf(cs.cnt));
                lineValueList.add(String.valueOf(cs.last_modified_date));                
                lineValueList.add(cs.id);

                lineList.add(String.join(lineValueList, ','));
            }
        }

        body = String.join(lineList, '\n');

        return body;
    }

    public String getMessage(Database.BatchableContext BC){

        List<String> msgList = new List<String>();

        AsyncApexJob a =[
            SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Id,CompletedDate
            FROM AsyncApexJob
            WHERE Id = :BC.getJobId()
        ];
        
        msgList.add('Hi ' + UserInfo.getFirstName() + ', ');
        msgList.add('');

        msgList.add('Fieldtrip Report.');
        msgList.add('');

        msgList.add('We did a field analyse  for: '+ objectName);
        msgList.add('Field trip is executed and has status: '+a.Status);
        msgList.add('The batch apex job processed '+a.TotalJobItems + ' batches with '+a.NumberOfErrors + ' failures.');

        msgList.add('The Batch executed the following:');
        msgList.add('');
        msgList.add('Object Name: '+ objectName);
        msgList.add('Number of Fields: '+ searchSet.size());
        msgList.add('Number of Records: '+ countRecords);

        msgList.add('In the attachment you will find a full report.');
        msgList.add('');
        msgList.add('');
        msgList.add('Kind regards,');
        msgList.add('The Fieldtrip Team.');

        return String.join(msgList, '\n');
    }


    /**
     * Inner Classes
     *
     */

    global class FieldAnalyse {
        
        public String name;
        public List<CodeSearch> code_searchs;

        public FieldAnalyse(String n){
            this.name = n;
            this.code_searchs = new List<CodeSearch>();
        }
    }

    global class CodeSearch {
        
        public String id;
        public String name;
        public String type;
        public Integer cnt;
        public Sobject last_modified_by;
        public Datetime last_modified_date;

        public CodeSearch(){}

        public CodeSearch (String id, String name){
            this.id = id;
            this.name = name;
        }
    }
}