//---------------------------------------------------------------------------------------------------------------------------------------------------
//  Author      :    Bart Caelen
//  Date        :    04/05/2018
//  Description :    Re-Written test logic based on Re-Written Batch APEX "ba_Application_Usage_Detail"
//---------------------------------------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_ba_Application_Usage_Detail {
	
	private static DateTime dtLastApplicationLogin_Old = Datetime.newInstance(2018, 4, 1, 8, 0, 0);

    @testSetup static void createTestData() {

		User oUser_Admin = clsTestData_User.createUser_SystemAdministratorMDT('tstadm1', true);

		User oUser = [SELECT Id, Last_Application_Login__c FROM User WHERE Id = :UserInfo.getUserId()];
			oUser.Last_Application_Login__c = dtLastApplicationLogin_Old;
		update oUser;

		System.runAs(oUser_Admin){

	    	List<Application_Usage_Detail__c> lstApplicationUsageDetail = new List<Application_Usage_Detail__c>();
			Application_Usage_Detail__c oApplicationUsageDetail = new Application_Usage_Detail__c();			
				oApplicationUsageDetail.Action_Date__c = Date.today();
				oApplicationUsageDetail.Action_DateTime__c = Datetime.now();
				oApplicationUsageDetail.Action_Type__c = 'Read';
				oApplicationUsageDetail.Capability__c = 'login';
				oApplicationUsageDetail.External_Record_Id__c = '0Yaw000006DmAkVCAV';
				oApplicationUsageDetail.Internal_Record_Id__c = '0Yaw000006DmAkVCAV';
				oApplicationUsageDetail.Object__c = 'Account_Plan_Objective__c';
				oApplicationUsageDetail.Process_Area__c = 'Account Planning';
				oApplicationUsageDetail.Source__c = 'SFDC';
				oApplicationUsageDetail.User_Id__c = UserInfo.getUserId();    	
			lstApplicationUsageDetail.add(oApplicationUsageDetail);

			insert lstApplicationUsageDetail;

		}

    }


    @isTest static void test_Scheduling() {


    	User oUser_Admin = [SELECT Id FROM User WHERE Alias = 'tstadm1' LIMIT 1];

        Test.startTest();

        //---------------------------------------
        // TEST SCHEDULING
        //---------------------------------------
        System.runAs(oUser_Admin){

	        String tCRON_EXP = '0 0 0 3 9 ? 2099';

	        ba_Application_Usage_Detail oBatch = new ba_Application_Usage_Detail();
	        
	        string tJobId = System.schedule('ba_Application_Usage_Detail_TEST', tCRON_EXP, oBatch);

	        // Get the information from the CronTrigger API object
	        CronTrigger oCronTrigger = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :tJobId];

	        // Verify the expressions are the same
	        System.assertEquals(tCRON_EXP, oCronTrigger.CronExpression);

	        // Verify the job has not run
	        System.assertEquals(0, oCronTrigger.TimesTriggered);

	        // Verify the next time the job will run
	        System.assertEquals('2099-09-03 00:00:00', String.valueOf(oCronTrigger.NextFireTime));

	    }
        //---------------------------------------

        Test.stopTest();

    }

	
	@isTest static void test_ba_Application_Usage_Detail() {

    	User oUser_Admin = [SELECT Id FROM User WHERE Alias = 'tstadm1' LIMIT 1];
        
        System.runAs(oUser_Admin){
		
			User oUser = [SELECT Id, Last_Application_Login__c, (SELECT Action_DateTime__c FROM Application_Usage_Details__r ORDER BY Action_DateTime__c DESC LIMIT 1) FROM User WHERE Id = :UserInfo.getUserId()];
			System.assertNotEquals(oUser.Last_Application_Login__c, oUser.Application_Usage_Details__r[0].Action_DateTime__c);
		
		}
		
		Test.startTest();


        System.runAs(oUser_Admin){

	        ba_Application_Usage_Detail oBatch = new ba_Application_Usage_Detail();
	        	oBatch.iBatchSize = 1;
	        Database.executebatch(oBatch, 1);

	    }

		Test.stopTest();


        System.runAs(oUser_Admin){
		
			User oUser = [SELECT Id, Last_Application_Login__c, (SELECT Action_DateTime__c FROM Application_Usage_Details__r ORDER BY Action_DateTime__c DESC LIMIT 1) FROM User WHERE Id = :UserInfo.getUserId()];
			System.assertEquals(oUser.Last_Application_Login__c, oUser.Application_Usage_Details__r[0].Action_DateTime__c);
		
		}
		

	}
	
}
//---------------------------------------------------------------------------------------------------------------------------------------------------