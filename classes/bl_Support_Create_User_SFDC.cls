//--------------------------------------------------------------------------------------------------------------------
//  Author           : 
//  Created Date     : 
//  Description      : APEX Class - Business Logic for Create SFDC User in the Support Portal
//  Change Log       : 
//		- 20/03/2019 - Bart Caelen - Replaced MEACAT by CEMA
//--------------------------------------------------------------------------------------------------------------------
public without sharing class bl_Support_Create_User_SFDC {
    
    private ctrl_Support_Requests.SessionData session;
    private Map<String, String> mapBUName_BUGName;
    private Map<Id, Company__c> mapCompany;

    public List<UserBU> userBusinessUnits {get; set;}
    public Map<Id, UserBU> userBUById {get; set;}
    public Map<Id, UserSBU> userSBUById {get; set;} 
   
    public String searchAlias {get; set;}
                
    public Boolean selectAll {get; set;}
    public String selectedItem {get; set;}   
       
    public List<SelectOption> regionOptions {get; set;} 
    public List<SelectOption> countryOptions {get; set;} 

    public Boolean sapIdMandatory {get; set;}

	// Mobile App        
	public List<String> lstSelectedMobileApp { get; set; }
	public List<SelectOption> lstSO_MobileApp { 
		get{
			return loadMobileApp();
		} 
		set; 
	}

    private Map<String, Company__c> mapCountryCompany = new Map<String, Company__c>();
    
    public bl_Support_Create_User_SFDC( ctrl_Support_Requests.SessionData sData){
        
        this.session = sData; 

        mapCompany = new Map<Id, Company__c>([SELECT Id, Name FROM Company__c ORDER BY Name]);


    }
    
    public PageReference requestCreateUser(){
        
        session.resetRequest();
        
        session.request.status__c='New';
        session.request.application__c='Salesforce.com';
        session.request.ownerId = session.internalUserId;
        session.request.request_Type__c = 'Create User';
                
        searchAlias = '';
                
        session.comment = null;
        session.comments = null;
        
        sapIdMandatory = false;
        
        return Page.Support_CreateUser;         
    }
    
    public PageReference initializeInputRequestId()
    {
        initializeBUInformation();    
                
        if(session.uInfo.email == session.request.Manager_Email__c) session.isAppApprover = true;
        else session.isAppApprover = false;
                                                
        return Page.Support_CreateUser;    
    }
    
    @TestVisible private void loadRegionOptions(){
        
        if(regionOptions != null) return;
        
        regionOptions = new List<SelectOption>();
        
        regionOptions.add(new SelectOption('', '--None--'));
        
        for(Company__c region : [Select Id, Name from Company__c ORDER BY Name]){
            
            regionOptions.add(new SelectOption(region.Id, region.Name));            
        }       
    }

	@TestVisible private List<SelectOption> loadMobileApp(){

		List<String> lstMobileAppName = new List<String>();

		String tBusinessUnitGroup = '';

		if (mapBUName_BUGName != null && mapBUName_BUGName.containsKey(session.request.User_Business_Unit_vs__c)){
			tBusinessUnitGroup = mapBUName_BUGName.get(session.request.User_Business_Unit_vs__c);
		}

		String tSOQL = 'SELECT Id, Name FROM Mobile_App__c WHERE Active__c = true';
		if (!String.isBlank(tBusinessUnitGroup)) tSOQL += ' AND (Business_Unit_Group__c = null OR Business_Unit_Group__c = :tBusinessUnitGroup)';

		List<Mobile_App__c> lstMobileApp = Database.query(tSOQL);

		lstSelectedMobileApp = new List<String>{'--None--'};
		List<SelectOption> lstSO = new List<SelectOption>();
		lstSO.add(new SelectOption('--None--', '--None--'));
		for (Mobile_App__c oMobileApp : lstMobileApp){
			lstSO.add(new SelectOption(oMobileApp.Name, oMobileApp.Name));
		}

		return lstSO;

	}

    public void geographicalRegionChanged(){
        session.request.Geographical_Subregion__c = null;
        session.request.Geographical_Country_vs__c = null;
        initializeBUInformation();
        clearCompanyCodeData();
    }

    public void geographicalSubRegionChanged(){
        session.request.Geographical_Country_vs__c = null;
        initializeBUInformation();
        clearCompanyCodeData();
    }

    public void userBusinessUnitChanged(){

		mapBUName_BUGName = new Map<String, String>();
		if (session.request.Region__c != null){
            
			List<Sub_Business_Units__c> lstSBU = 
				[
					SELECT
						Id, Name, Business_Unit__r.Business_Unit_Group__r.Name, Business_Unit__c, Business_Unit__r.Name 
					FROM
						Sub_Business_Units__c
					WHERE
						Business_Unit__r.Company__c =: session.request.Region__c
					ORDER BY
						Business_Unit__r.Business_Unit_Group__r.Name, Business_Unit__r.Name,Name ASC
				];

			for (Sub_Business_Units__c oSBU : lstSBU){

				mapBUName_BUGName.put(oSBU.Business_Unit__r.Name, oSBU.Business_Unit__r.Business_Unit_Group__r.Name);
				
			}
			
		}

    }

    public void clearCompanyCodeData(){
        session.request.Region__c = null;
        session.request.Company_Code__c = null;
        session.request.User_Business_Unit_vs__c = null;  
        session.request.Jobtitle_vs__c = null;  
    }
    
    public void refreshCompanyCodeData(){

        if (session.request.Geographical_Country_vs__c == null){
            clearCompanyCodeData();
            return;
        }

        Company__c oCompany = getRequestCompany();
        if (oCompany == null){
            clearCompanyCodeData();
        }else{
            session.request.Region__c = oCompany.Id;
            session.request.Company_Code__c = oCompany.Company_Code_Text__c;
        }


    }
    public void countrySelectionChanged(){

        refreshCompanyCodeData();

        checkSapIdMandatory();

        initializeBUInformation();

	}

    private Company__c getRequestCompany(){

        Company__c oCompany = null;

        if (!mapCountryCompany.containsKey(session.request.Geographical_Country_vs__c)){
            List<DIB_Country__c> lstDIBCountry = [SELECT Id, Name, Company__r.Id, Company__r.Company_Code_Text__c FROM DIB_Country__c WHERE Name = :session.request.Geographical_Country_vs__c];
            if (lstDIBCountry.size() > 0){
                mapCountryCompany.put(lstDIBCountry[0].Name, lstDIBCountry[0].Company__r);
            }
        }
        oCompany = mapCountryCompany.get(session.request.Geographical_Country_vs__c);

        return oCompany;

    }

    private Id getRequestCompanyId(){

        Id id_Result = null;

        List<DIB_Country__c> lstDIBCountry = [SELECT Id, Name, Company__r.Id FROM DIB_Country__c WHERE Name = :session.request.Geographical_Country_vs__c];
        if (lstDIBCountry.size() > 0){
            id_Result = lstDIBCountry[0].Company__r.Id;
        }

        return id_Result;

    }

    
    public void searchByAlias(){
        
        Pattern alphaNumeric = Pattern.compile('^[A-Za-z0-9]+$');
        Matcher matcher = alphaNumeric.matcher(searchAlias);
        
        if(!matcher.matches()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Only alphanumeric characters are allowed'));
            return;
        }
        
        ws_adUserService.Service1Soap userService = new ws_adUserService.Service1Soap();
        userService.timeout_x=120000;
        
        ws_adUserService.userinfo searchResult = userService.getUserInfo(searchAlias);          
            
        if(searchResult.firstName==null && searchResult.lastName==null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'User not found'));
        }
        
        fillUserInfo(searchResult);     
    }
    
    private void fillUserInfo(ws_adUserService.userinfo inputInfo){
        
        session.request.Alias__c= searchAlias.toLowerCase();
        session.request.Firstname__c=inputInfo.firstName;
        session.request.Lastname__c=inputInfo.lastName;
        session.request.Email__c=inputInfo.email;
        session.request.Peoplesoft_ID__c=inputInfo.peopleSoft;
        session.request.Manager__c=inputInfo.managerName;
        session.request.Manager_Email__c = inputInfo.managerEmail;
        session.request.Cost_Center__c = inputInfo.costcenter;
        session.request.Federation_ID__c= session.request.Alias__c;
    }   
    
    public void loadSameAsUserInfo(){
        
        if(session.request.Same_as_user__c!=null){
            
            User sameAs = 
                [
                    SELECT 
                        Id, Job_Title_vs__c, Company_Code_Text__c, CountryOR__c
                        , Region_vs__c, Sub_Region__c, Country_vs__c, User_Business_Unit_vs__c
                    FROM
                        User
                    WHERE 
                        Id = :session.request.Same_as_user__c
                ];
            
            //-BC - 20151028 - CR-8006 - START
            session.request.Geographical_Region_vs__c = sameAs.Region_vs__c;
            session.request.Geographical_SubRegion__c = sameAs.Sub_Region__c;
            session.request.Geographical_Country_vs__c = sameAs.Country_vs__c;
            session.request.Company_Code__c = sameAs.Company_Code_Text__c;
            session.request.User_Business_Unit_vs__c = sameAs.User_Business_Unit_vs__c;
            session.request.Jobtitle_vs__c = sameAs.Job_Title_vs__c;
            initializeBUInformation();

            if (session.request.Region__c == null){
                session.request.Region__c = getRequestCompanyId();
            }
            
            List<DIB_Country__c> country = [Select Id, Company__r.Id, Company__r.Company_Code_Text__c from DIB_Country__c where Name = :sameAs.Country_vs__c AND Company__c = :session.request.Region__c];
            if (country.size()>0){
                session.request.Country__c = country[0].Id;
                session.request.Company_Code__c = country[0].Company__r.Company_Code_Text__c;
            }

            for (User_Business_Unit__c userBU : [Select Sub_Business_Unit__c, Sub_Business_Unit__r.Name, Sub_Business_Unit__r.Business_Unit__c, Primary__c from User_Business_Unit__c where User__c =: sameAs.Id]){

                UserBU reqBU = userBUById.get(userBU.Sub_Business_Unit__r.Business_Unit__c);
                reqBU.selected = true;
                
                UserSBU reqSBU = userSBUById.get(userBU.Sub_Business_Unit__c);
                reqSBU.selected=true;
                reqSBU.primary = userBU.Primary__c;

                if (userBU.Primary__c){
                    session.request.Primary_sBU__c = userBU.Sub_Business_Unit__r.Name;
                }             
            }
            
            checkSapIdMandatory();

        }
    }
    
    public void resubmit(){
        
        if(Test.isRunningTest()==false) bl_CreateUserRequest.notifyUserRequestApprovers(new List<Create_User_Request__c>{session.request});      
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request has been re-Submitted'));
    }
    
    /*
        Create User Business Unit information
    */
    
    @TestVisible private void initializeBUInformation(){
        
        userBusinessUnits = new List<UserBU>();
        userBUById = new Map<Id, UserBU>();
        userSBUById = new Map<Id, UserSBU>();
        
        if (session.request.Region__c == null){
            session.request.Region__c = getRequestCompanyId();
        }
        
        if(session.request.Region__c!=null){
            
            List<Sub_Business_Units__c> lstSBU = 
                [
                    SELECT
                        Id, Name, Business_Unit__c, Business_Unit__r.Name 
                    FROM
                        Sub_Business_Units__c
                    WHERE
                        Business_Unit__r.Company__c =: session.request.Region__c
                    ORDER BY
                        Business_Unit__r.Name,Name ASC
                ];

            for (Sub_Business_Units__c sbu:lstSBU){
                
                UserBU userBU = userBUById.get(sbu.Business_Unit__c);
                
                if(userBU==null){
                    userBU = new UserBU();
                    userBU.id = sbu.Business_Unit__c;
                    userBU.name = sbu.Business_Unit__r.Name;
                    userBU.selected = false;
                    userBU.sbus = new List<UserSBU>();              
                    userBUById.put(sbu.Business_Unit__c, userBU);
                    userBusinessUnits.add(userBU);
                }
                
                UserSBU userSBU = new UserSBU();
                userSBU.id = sbu.Id;
                userSBU.name = sbu.Name;
                userSBU.parentBUId = sbu.Business_Unit__c;
                userSBU.selected = false;
                userSBU.primary = false;
                
                userBU.sbus.add(userSBU);
                userSBUById.put(sbu.Id, userSBU);
            }
        }
        
        if (session.request.Id != null){
            for (User_Request_BU__c requestBU : [Select Sub_Business_Unit__c, Sub_Business_Unit__r.Business_Unit__c, Primary__c 
                                                    from User_Request_BU__c where User_Request__c=:session.request.Id]){
                
                UserSBU sbu = userSBUById.get(requestBU.Sub_Business_Unit__c);
                sbu.selected=true;
                sbu.primary = requestBU.Primary__c;
                
                UserBU bu = userBUBYId.get(requestBU.Sub_Business_Unit__r.Business_Unit__c);
                bu.selected=true;
            }   
        }   
    }
    
    public void selectAllBU(){
        
        for(UserBU bu: userBUById.values()){
            bu.selected = selectAll;
        }
        
        for(UserSBU sbu: userSBUById.values()){
            sbu.selected = selectAll;
            if(selectAll==false) sbu.primary = false;
        }
        
        checkSapIdMandatory();  
    }
    
    public void selectBU(){
        
        UserBU bu = userBUById.get(selectedItem);
        
        for(UserSBU sbu : bu.sbus){
            
            sbu.selected = bu.selected;
            if(bu.selected==false) sbu.primary = false;         
        }
        
        checkSapIdMandatory();  
        
    }
    
    public void selectSBU(){
        
        UserSBU sbu = userSBUById.get(selectedItem);
        UserBU bu = userBUById.get(sbu.parentBUId);
        
        if(sbu.selected==true){         
            bu.selected=true;
        }else{
            Boolean selected=false;
            for(UserSBU childSBU:bu.sbus){
                if(childSBU.selected==true){
                    selected = true;
                    break;
                }
            }
            bu.selected=selected;
        }
        
        checkSapIdMandatory();  
    }
    
    public void makePrimary(){
        
        UserSBU sbu = userSBUById.get(selectedItem);
        
        if(sbu.primary==true){
            
            if(sbu.selected==false){
                sbu.selected=true;
                selectSBU();
            }

            for(UserSBU otherSbu : userSBUById.values()){
                if(otherSbu.id!=selectedItem){
                    otherSbu.primary = false;                   
                }   
            }

            session.request.Primary_sBU__c = sbu.Name;

            UserBU oUserBU = userBUById.get(sbu.parentBUId);

            if (session.request.User_Business_Unit_vs__c != oUserBU.Name){
                session.request.User_Business_Unit_vs__c = oUserBU.Name;
            }

        }   
        
        checkSapIdMandatory();      
    }
    
    public void checkSapIdMandatory(){
        
        sapIdMandatory = bValidateSAPIdMandatory();
    }

    @TestVisible private Boolean bValidateSAPIdMandatory(){

        Boolean bResult = false;

        //- BC - 20160119 - CR-10544 - START
        if ( (session.request.Application__c == 'Salesforce.com') && (session.request.License_Type__c == 'Full Salesforce') && (userBUById != null) && (clsUtil.isNull(session.request.Jobtitle_vs__c, '') != '') ){        

			// Get Business Units based on the Business Unit Group for which this logic should be applied
            Set<String> setBU_DIB = clsUtil.getBusinessUnitName('Europe', 'Diabetes');
            Set<String> setBU_CVG = clsUtil.getBusinessUnitName('Europe', 'CVG');
            Set<String> setBU_RTG = clsUtil.getBusinessUnitName('Europe', 'Restorative');
            Set<String> setBU_MITG = clsUtil.getBusinessUnitName('Europe', 'MITG');

            Set<String> setJobTitle_DIB = new Set<String>{'sales rep'};
            Set<String> setJobTitle_CVG = new Set<String>{'sales rep', 'district manager', 'technical consultant', 'connect me'};
            Set<String> setJobTitle_RTG = new Set<String>{'sales rep', 'district manager', 'technical consultant', 'connect me'};
            Set<String> setJobTitle_MITG = new Set<String>{'sales rep', 'district manager', 'technical consultant', 'connect me'};

			String tGeographicalRegion = session.request.Geographical_Region_vs__c;

            for (UserBU oUserBU : userBUById.values()){
            
                if (oUserBU.selected == true){

                    if (setBU_DIB.contains(oUserBU.Name.toLowerCase())){

                        bResult = setJobTitle_DIB.contains(session.request.Jobtitle_vs__c.toLowerCase());

                    }else if (setBU_CVG.contains(oUserBU.Name.toLowerCase())){

                        if ( (tGeographicalRegion != null) && ( (tGeographicalRegion == 'Europe') || (tGeographicalRegion == 'CEMA') ) && (session.request.Geographical_Country_vs__c.toUpperCase() != 'CANADA') ){
                            bResult = setJobTitle_CVG.contains(session.request.Jobtitle_vs__c.toLowerCase());
                        }

                    }else if (setBU_RTG.contains(oUserBU.Name.toLowerCase())){

                        if ( (tGeographicalRegion != null) && ( (tGeographicalRegion == 'Europe') || (tGeographicalRegion == 'CEMA') ) && (session.request.Geographical_Country_vs__c.toUpperCase() != 'CANADA') ){
                            bResult = setJobTitle_RTG.contains(session.request.Jobtitle_vs__c.toLowerCase());
                        }

                    }else if (setBU_MITG.contains(oUserBU.Name.toLowerCase())){

                        if ( (tGeographicalRegion != null) && ( (tGeographicalRegion == 'Europe') || (tGeographicalRegion == 'CEMA') ) && (session.request.Geographical_Country_vs__c.toUpperCase() != 'CANADA') ){
                            bResult = setJobTitle_MITG.contains(session.request.Jobtitle_vs__c.toLowerCase());
                        }

                    }

                }

                if (bResult) break;
            }       

        }         

        return bResult;

    }

    // Validate SAP ID Length based on Job Title but only for EUROPE and MEA Excluding CANADA
    @TestVisible private Boolean bValidateSAPIdLength(Boolean bGenerateError){
        
        Boolean bResult = true;

        Map<String, Integer> mapJobTitle_Length = new Map<String, Integer>{'sales rep' => 10, 'district manager' => 10, 'technical consultant' => 10};

        if (!bValidateSAPIdMandatory()){
            return bResult;
        }

        // If the SAP Id is populated we need to validate the length
        if (session.request.User_SAP_Id__c != null){

			String tGeographicalRegion = session.request.Geographical_Region_vs__c;

            if ( (tGeographicalRegion != null) && ( (tGeographicalRegion == 'Europe') || (tGeographicalRegion == 'CEMA') ) && (session.request.Geographical_Country_vs__c.toUpperCase() != 'CANADA') ){

                if (mapJobTitle_Length.containsKey(session.request.Jobtitle_vs__c.toLowerCase())){

                    if (session.request.User_SAP_Id__c.length() != mapJobTitle_Length.get(session.request.Jobtitle_vs__c.toLowerCase())){
                        bResult = false;
                        if (bGenerateError){
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'SAP Id should contain ' + mapJobTitle_Length.get(session.request.Jobtitle_vs__c.toLowerCase()) + ' characters'));
                        }
                    }
                }
            }

        }

        return bResult;

    }  

    private class UserBU{
        
        public Id id {get; set;}
        public String name {get; set;}
        public Boolean selected {get; set;}
        
        public List<UserSBU> sbus {get; set;}
    } 
    
    private class UserSBU{
        
        public Id id {get; set;}
        public String name {get; set;}
        public Id parentBUId {get; set;}
        public Boolean selected {get; set;}
        public Boolean primary {get; set;}
    }
    
    public void createNewUserrequest(){
        
        if(session.request.Email__c == null){            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Invalid User'));
            return; 
        }
        
        if (session.request.Application__c == 'Salesforce.com' && session.request.License_Type__c == null){          
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'License Type is required'));
            return; 
        }
        

        if (session.request.Application__c == 'Salesforce.com' && session.request.Geographical_Region_vs__c == null){          
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Geographical Region is required'));
            return; 
        }
        if (session.request.Application__c == 'Salesforce.com' && session.request.Geographical_Subregion__c == null){          
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Geographical Subregion is required'));
            return; 
        }
        if (session.request.Application__c == 'Salesforce.com' && session.request.Geographical_Country_vs__c == null){          
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Geographical Country is required'));
            return; 
        }

        if (session.request.Application__c == 'Salesforce.com' && session.request.License_Type__c == 'Full Salesforce' && session.request.Company_Code__c == null){          
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Company Code is required'));
            return; 
        }

        if (session.request.Application__c == 'Salesforce.com' && session.request.License_Type__c == 'Full Salesforce' && session.request.User_Business_Unit_vs__c == null){          
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'User Business Unit is required'));
            return; 
        }

        if (session.request.Application__c == 'Salesforce.com' && session.request.License_Type__c == 'Full Salesforce' && session.request.Jobtitle_vs__c == null){            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Job Title is required'));
            return; 
        }

        if (sapIdMandatory && session.request.User_SAP_Id__c == null){            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'SAP Id is required'));
            return; 
        }

        if (bValidateSAPIdLength(true) == false){
            return;
        }
                
        SavePoint sp = Database.setSavepoint(); 
        
        try{

            if (session.request.Region__c == null){
                session.request.Region__c = getRequestCompanyId();
            }
            
            session.request.Requestor_Email__c = session.uInfo.email;
            session.request.Requestor_Name__c = session.uInfo.firstName + ' ' + session.uInfo.lastName;
            
            if(session.request.Comment__c!=null && session.request.Comment__c!=''){
                
                session.comment = new User_Request_Comment__c();
                session.comment.Comment__c = session.request.Comment__c;         
                
                session.request.Comment__c = null;
            }

			if (lstSelectedMobileApp != null && lstSelectedMobileApp.size() > 0){
				List<String> lstSelectedMobileApp_Tmp = new List<String>();
				for (String tSelectedMobileApp : lstSelectedMobileApp){
					if (tSelectedMobileApp != '--None--') lstSelectedMobileApp_Tmp.add(tSelectedMobileApp);
				}
				if (lstSelectedMobileApp_Tmp.size() > 0){
					session.request.Mobile_Apps__c = String.join(lstSelectedMobileApp_Tmp, ';');
				}
			}

            insert session.request;

            if(userSBUById!=null){
                
                List<User_Request_BU__c> requestBUs = new List<User_Request_BU__c>();
                Boolean primaryExists = false;
                
                for(UserSBU sbu: userSBUById.values()){
                                
                    if(sbu.selected == true){
                        
                        User_Request_BU__c requestBU = new User_Request_BU__c();
                            requestBU.User_Request__c = session.request.Id;
                            requestBU.Sub_Business_Unit__c = sbu.Id;
                            requestBU.Primary__c = sbu.Primary;
                        
                        if(sbu.Primary==true) primaryExists = true;
                        
                        requestBUs.add(requestBU);
                    }
                }
                
                if(primaryExists == false && session.request.License_Type__c == 'Full Salesforce') throw new ValidationException('You need to select a Sub Business Unit as primary');
                
                if(!requestBUs.isEmpty()) insert requestBUs;
            }
                        
            if(session.comment!=null){
                
                session.comment.User_Request__c = session.request.Id;                
                session.comment.From__c = session.uInfo.firstName + ' ' + session.uInfo.lastName;   
                session.comment.From_Email__c = session.uInfo.email;            
                insert session.comment;
                                
                session.getRequestComments();
            }   
            
            session.comment = new User_Request_Comment__c();        

        }catch(Exception e){
            
            Database.rollback(sp);
            session.request.Id=null;
            ApexPages.addMessages(e);
            if(Test.isRunningTest()) throw e;
            return;
        }
        
        session.request = 
            [
                SELECT 
                    Id, Name, FirstName__c, LastName__c, Assignee__c, Email__c,Peoplesoft_ID__c,Manager__c, Manager_Email__c, Status__c
                    , Cost_Center__c, Region__c,Region__r.Name, Country__c,Country__r.Name, License_Type__c, Comment__c
                    , District_Manager__c,District_Manager__r.Name, Same_as_user__c,Same_as_user__r.Name, Alias__c, Application__c, Request_Type__c
                    , profile__c, Requestor_Email__c, User_SAP_Id__c, Federation_ID__c
                    , Geographical_Region_vs__c, Geographical_SubRegion__c, Geographical_Country_vs__c, User_Business_Unit_vs__c, Company_Code__c, Primary_sBU__c, Jobtitle_vs__c
					, Mobile_Apps__c
                FROM 
                    Create_User_Request__c
                WHERE
                    Id =:session.request.Id
            ];
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request '+session.request.Name+' created successfully.'));       
    }
}