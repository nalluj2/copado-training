public without sharing class SynchronizationService_Comment implements SynchronizationService_Object{
	
	//Set with the external Ids of the records already processed in this transaction. Used to avoid generating multiple notifications for the same change
	private Set<String> processedRecords = new Set<String>();
	
	//This method returns the External Ids of the records that have relevant modifications for the target Org. This includes created records or 
	//modified records where synchronized fields have changed. 
	public Set<String> hasChangeForNotification(List<SObject> records, Map<Id, SObject> oldRecords){
		
		Set<String> result = new Set<String>();
				
		for(SObject rawObject : records){
			
			Case_Comment__c record = (Case_Comment__c) rawObject;
			
			if(processedRecords.contains(record.External_Id__c)) continue;
			
			//All fields for Comments are relevant for the synchronization			
			result.add(record.External_Id__c);
			processedRecords.add(record.External_Id__c);			
		}
		
		return result;
	}
	
	//This method collects and returns the complete information for a record. This includes the records itself as well as any related record that needs
	//to exist in the target Org too. The returned format is JSON serialized string.
	public String generatePayload(String externalId){
		
		CommentSyncPayload payload = new CommentSyncPayload();
		
		String query = 	'SELECT ' + String.join( syncFields, ',') +
						',Case__r.External_Id__c, Workorder__r.External_Id__c, Complaint__r.External_Id__c, Parent_Complaint__r.External_Id__c, Case_Part__r.External_Id__c ' +  										
  						'FROM Case_Comment__c where External_Id__c = :externalId LIMIT 2';		
		
		List<Case_Comment__c> comments = Database.query(query);
		
		//If no record or more than 1 is found, we return an empty response
		if(comments.size() == 1){
			
			Case_Comment__c comment = comments[0];	
			bl_SynchronizationService_Utils.prepareForJSON(comment, syncFields);		
			comment.Id = null;//The local SFDC Id is not relevant in the target Org
			
			payload.Comment = comment;	
			
			if(comment.Case__r != null){
				payload.CaseId = comment.Case__r.External_Id__c;				
				comment.Case__r = null;//If we send this reference to the target Org it may cause problems in the upsert dml
				comment.Case__c = null;//The local SFDC Id is not relevant in the target Org
			}
			
			if(comment.Complaint__r != null){
				payload.ComplaintId = comment.Complaint__r.External_Id__c;				
				comment.Complaint__r = null;//If we send this reference to the target Org it may cause problems in the upsert dml
				comment.Complaint__c = null;//The local SFDC Id is not relevant in the target Org
			}
						
			if(comment.Workorder__r != null){
				payload.WorkorderId = comment.Workorder__r.External_Id__c;				
				comment.Workorder__r = null;//If we send this reference to the target Org it may cause problems in the upsert dml
				comment.Workorder__c = null;//The local SFDC Id is not relevant in the target Org
			}
			
			if(comment.Case_Part__r != null){
				payload.SparepartId = comment.Case_Part__r.External_Id__c;				
				comment.Case_Part__r = null;//If we send this reference to the target Org it may cause problems in the upsert dml
				comment.Case_Part__c = null;//The local SFDC Id is not relevant in the target Org
			}	
			
			//Done in a separate query because we cannot clear the value from the CreatedDate field before sending it to the target
			Case_Comment__c commentDetails = [Select Name, CreatedDate, CreatedBy.Name, LastModifiedDate, LastModifiedBy.Name from Case_Comment__c where External_Id__c = :externalId];
			payload.commentName = commentDetails.Name;			
			payload.CreatedDate = commentDetails.CreatedDate;			
			payload.CreatedBy = commentDetails.CreatedBy.Name;					
			payload.LastModifiedDate = commentDetails.LastModifiedDate;			
			payload.LastModifiedBy = commentDetails.LastModifiedBy.Name;						
		}
		
		return JSON.serialize(payload);
	}
	
	//Implementation needed only in Target Org
	public String processPayload(String inputPayload){		
		return null;		
	}
	
	//Model classes for the requests and responses. To be used by the JSON serialize/deserialize
	private class CommentSyncPayload{
		
		public Case_Comment__c comment {get; set;}
		public String commentName {get; set;}
		public String complaintId {get; set;}
		public String workorderId {get; set;}
		public String caseId {get; set;}
		public String sparepartId {get; set;}	
		public Datetime createdDate {get; set;}
		public String createdBy {get; set;}
		public Datetime lastModifiedDate {get; set;}
		public String lastModifiedBy {get; set;}	
	}
	
	private static List<String> syncFields = new List<String>
	{
		'Case_Part__c',
		'Case__c',
		'Comment_Subject__c',
		'Comment__c',
		'Complaint__c',
		'External_Id__c',
		'Parent_Complaint__c',
		'Workorder__c'
	};
}