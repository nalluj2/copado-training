public without sharing class bl_CreateUserRequest {
	
    public static final String tNONE = '--NONE--';

	private static BusinessHours euBH {
										get{
											if(euBH == null) euBH = [Select Id from BusinessHours where Name = 'EUR Central'];
											return euBH;
										} 
										
										set;
									  }	
	
	public static void enrichUserCreateRequests(List<Create_User_Request__c> createUserRequestList){
		
		Set<ID> sameAsUserIdsInrequest = new Set<ID>();
				
		for (Create_User_Request__c cur : createUserRequestList){
			if(cur.same_as_user__c!=null && cur.License_Type__c == 'Full Salesforce'){
				sameAsUserIdsInrequest.add(cur.same_as_user__c);
			}
			
			if(cur.License_Type__c == 'Chatter Free') cur.Profile__c = 'Chatter Free User';		
		}
								
		Map<Id,User> mapSameAsUsers;
		
		if(sameAsUserIdsInrequest.size()>0) mapSameAsUsers = new Map<Id,User>([SELECT Id, UserRole.name,Profile.name FROM User where id IN :sameAsUserIdsInrequest]);
		else mapSameAsUsers = new Map<Id, User>();
				
		for (Create_User_Request__c cur : createUserRequestList){
			
			if(cur.same_as_user__c!=null && cur.License_Type__c == 'Full Salesforce'){
				
				User sameAsUser = mapSameAsUsers.get(cur.same_as_user__c);
			
				cur.Role_of_same_as_user__c = sameAsUser.UserRole.name;
				cur.Profile_of_same_as_user__c = sameAsUser.Profile.name;
			}else{
				cur.Role_of_same_as_user__c = null;
				cur.Profile_of_same_as_user__c = null;
			}			 
		}		
	}
	
	public static void notifyUserRequestApprovers(List<Create_User_Request__c> requests){
		
		EmailTemplate approverTemplate = [select Id from EmailTemplate where DeveloperName='Create_User_Request_Approver'];
		Map<String, Support_application__c> appConfig = Support_application__c.getAll();
		
		Map<String, Contact> contactsByEmail = new Map<String, Contact>();		                      
        Map<String, List<Messaging.Singleemailmessage>> mailsByEmail = new Map<String, List<Messaging.Singleemailmessage>>();
        
        RecordType mdtRT = [Select Id from RecordType where SobjectType = 'Contact' and DeveloperName = 'MDT_Employee'];
        	
		String tTimeStamp = clsUtil.getTimeStamp();
		Integer iCounter = 0;
        for(Create_User_Request__c request : requests){
        	
        	List<String> approverEmails;
        	
        	if(request.Application__c == 'Salesforce.com'){
        		approverEmails = new List<String>{request.Manager_Email__c};
        	}else{
        		approverEmails = appConfig.get(request.Application__c).Emails_of_approvers__c.split(';');
        	}
        	
        	for(String approverEmail : approverEmails){
        		
				iCounter++;
        		Contact dummy = contactsByEmail.get(approverEmail);
        		
        		if(dummy==null){
        			Contact dummyContact = new Contact();
					dummyContact.Email = approverEmail;
					dummyContact.FirstName = 'dummy';
					dummyContact.LastName = 'contact';
					dummyContact.Alias_unique__c = tTimeStamp + String.valueOf(iCounter);
					dummyContact.RecordTypeId = mdtRT.Id;
					
					contactsByEmail.put(approverEmail, dummyContact);
        		}
        		
        		List<Messaging.Singleemailmessage> contactEmails = mailsByEmail.get(approverEmail);
        		
        		if(contactEmails==null){
        			contactEmails = new List<Messaging.Singleemailmessage>();
        			mailsByEmail.put(approverEmail, contactEmails);
        		}
        		
	    		Messaging.Singleemailmessage mail = new Messaging.Singleemailmessage();       
		        mail.setTemplateId(approverTemplate.Id);           
		        mail.setUseSignature(false);
		        mail.setSaveAsActivity(false);
		        mail.setWhatId(request.Id);
		        
		        contactEmails.add(mail);
        	}
        }		
        
        insert contactsByEmail.values();
        
        List<Messaging.Singleemailmessage> allMails = new List<Messaging.Singleemailmessage>();
        
        for(Contact contact : contactsByEmail.values()){
        	        	        	
        	for(Messaging.Singleemailmessage contactEmail : mailsByEmail.get(contact.Email)){
        		
        		contactEmail.setTargetObjectId(contact.Id);        		
        		allMails.add(contactEmail);        			
        	}       	
        }
        
        // Send the email
        Messaging.sendEmail(allMails);
        
        Triggers_Soft_Deactivation.AffiliationBeforeDelete = true;
        delete contactsByEmail.values();                         
        Triggers_Soft_Deactivation.AffiliationBeforeDelete = false;
	}	
	
	public static void notifyDataLoadApprovers(Create_User_Request__c dataLoad){
        
    	List<Contact> contacts = getApproverContacts(dataLoad.Data_Load_Approvers__c);
            
    	EmailTemplate approverTemplate = [select Id from EmailTemplate where DeveloperName='Data_Load_Request_Approver'];
                       
        List<Messaging.Singleemailmessage> mails = new List<Messaging.Singleemailmessage>();
        
        for(Contact contact : contacts){    
        	     
            Messaging.Singleemailmessage mail = new Messaging.Singleemailmessage();
           
            mail.setTargetObjectId(contact.Id);
            mail.setTemplateId(approverTemplate.Id);           
            mail.setUseSignature(false);
            mail.setSaveAsActivity(false);
            mail.setWhatId(dataLoad.Id);
            
            mails.add(mail);              
        }
        
        // Send the email
		if (clsUtil.bEmailDeliverabilityEnabled()) Messaging.sendEmail(mails);
        
        Triggers_Soft_Deactivation.AffiliationBeforeDelete = true;
        delete contacts;                         
        Triggers_Soft_Deactivation.AffiliationBeforeDelete = false;
  	}
  
	private static List<Contact> getApproverContacts(String approvers){
	    
		RecordType mdtRT = [Select Id from RecordType where SobjectType = 'Contact' and DeveloperName = 'MDT_Employee'];
	       
	    List<String> approverEmails = approvers.split(';');
	    
	    List<Contact> dummyContacts = new List<Contact>();
	    
		String tTimeStamp = clsUtil.getTimeStamp();
		Integer iCounter = 0;
	    for(String approverEmail : approverEmails){
	      
			iCounter++;

	    	Contact dummyContact = new Contact();
			dummyContact.Email = approverEmail;
			dummyContact.FirstName = 'dummy';
			dummyContact.LastName = 'contact';
			dummyContact.Alias_unique__c = tTimeStamp + String.valueOf(iCounter);
			dummyContact.RecordTypeId = mdtRT.Id;
			  
			dummyContacts.add(dummyContact);
	    }
	    
	    insert dummyContacts;
	        
	    return dummyContacts;
	}
	
	public static void setResponseTimeBusinessHours(Create_User_Request__c req){
													
		Long responseTimeInMilis = BusinessHours.diff(euBH.Id, req.CreatedDate, req.Assigned_Date__c);
			
		req.Response_Time__c = Integer.valueOf( ((Decimal)responseTimeInMilis).divide(3600000, 0 , System.RoundingMode.HALF_UP));		
	}
	
	public static void setCompletionTimeBusinessHours(Create_User_Request__c req){
						
		Long completionTimeInMilis = BusinessHours.diff(euBH.Id, req.CreatedDate, req.Completed_Date__c);
			
		req.Completion_Time__c = Integer.valueOf( ((Decimal)completionTimeInMilis).divide(3600000, 0 , System.RoundingMode.HALF_UP));		
	}


    public static Map<Id, User> loadAdminUsers(String tAdminGroupName){

        Map<Id, User> mapUser = new Map<Id, User>();

        List<GroupMember> lstGroupMember_Admin = [SELECT UserOrGroupId FROM GroupMember WHERE Group.DeveloperName = :tAdminGroupName];
        Set<Id> setID_UserAdmin = new Set<Id>();
        Set<Id> setID_GroupAdmin = new Set<Id>();

        // Get the UserId's and the GroupId's of the GroupMembers
        for (GroupMember oGroupMember_Admin : lstGroupMember_Admin){

            if (String.valueOf(oGroupMember_Admin.UserOrGroupId).startsWith('005')){	// is User

                setID_UserAdmin.add(oGroupMember_Admin.UserOrGroupId);

            }else if (String.valueOf(oGroupMember_Admin.UserOrGroupId).startsWith('00G')){	// is Group

                setID_GroupAdmin.add(oGroupMember_Admin.UserOrGroupId);

            }

        }

        // Get the UserId's of the GroupMembers of the collected Groups
        if (setID_GroupAdmin.size() > 0){
            List<GroupMember> lstGroupMember_Admin2 = [SELECT UserOrGroupId FROM GroupMember WHERE GroupId = :setID_GroupAdmin];

            for (GroupMember oGroupMember_Admin : lstGroupMember_Admin2){

                if (String.valueOf(oGroupMember_Admin.UserOrGroupId).startsWith('005')){	// is User

                    setID_UserAdmin.add(oGroupMember_Admin.UserOrGroupId);

                }

            }

        }

        // Load the Active User Id and Name of the collected UserAdmins and populated the AdminUser Picklist
        if (setID_UserAdmin.size() > 0){

            mapUser = new Map<Id, User>([SELECT Id, Name FROM User WHERE Id = :setID_UserAdmin AND IsActive = TRUE ORDER BY Name]);

        }

        return mapUser;

    }	

}