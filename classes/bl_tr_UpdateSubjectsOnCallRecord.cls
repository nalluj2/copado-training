/**
 *      Created Date : 20130222
 *      Description : This class is used as a business logic class for tr_UpdateSubjectsOnCallRecord trigger on call topic subjects object.
 * 
 *      Author = Kaushal
 */
public class bl_tr_UpdateSubjectsOnCallRecord{

    public static void UpdateSubjectsConcatenated(set<id> CTSIds){
    
        map<id,string> mapSubjectidWithName=new map<id,string>();
        map<id,set<id>> mapCRidWithSubjectid=new map<id,set<id>>();
        
        List<Call_Topic_Subject__c> lstCTS=[
                                            select id,Call_Topic__c,Call_Topic__r.Call_Records__c,Subject__c,Subject__r.Name 
                                            from Call_Topic_Subject__c 
                                            where id in :CTSIds
                                            ];
        set<id> setTempid=new set<id>();                                    
        for(Call_Topic_Subject__c cts:lstCTS){
            mapSubjectidWithName.put(cts.Subject__c,cts.Subject__r.Name);
            setTempid=mapCRidWithSubjectid.get(cts.Call_Topic__r.Call_Records__c);
            if(setTempid==null){
                setTempid=new set<id>();
                setTempid.add(cts.Subject__c);
                mapCRidWithSubjectid.put(cts.Call_Topic__r.Call_Records__c,setTempid);
            }else{
                setTempid.add(cts.Subject__c);
                mapCRidWithSubjectid.put(cts.Call_Topic__r.Call_Records__c,setTempid);
            }
        }
        
        List<Call_Records__c> lstCR=new List<Call_Records__c>();
        string strSubName='';
        for(id crid:mapCRidWithSubjectid.keyset()){
            Call_Records__c CR = new Call_Records__c(id=crid);
            for(id  subid:mapCRidWithSubjectid.get(crid)){
                if(strSubName==null || strSubName==''){
                    strSubName=mapSubjectidWithName.get(subid);
                }else{
                    strSubName=strSubName+';'+mapSubjectidWithName.get(subid);
                }
            }
            
            CR.Subjects_Concatenated__c=strSubName;
            lstCR.add(CR);
        }
        if(lstCR.size()>0){
            update lstCR;  
        }
    
    }

}