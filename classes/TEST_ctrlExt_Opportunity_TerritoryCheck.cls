@isTest
private class TEST_ctrlExt_Opportunity_TerritoryCheck {

    private static List<User> lstUser_WithMultipleTerritories;
    private static List<User> lstUser;
    private static Boolean bUpdateCompanyData = true;
    private static List<Opportunity> lstOpportunity;
    private static List<Opportunity> lstOpportunity2;
    private static Territory2 oTerritory;

    @isTest static void createTestData() {

        Id idProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1].Id;
        Id idRole = [SELECT Id FROM UserRole WHERE DeveloperName = 'SystemAdministrator' LIMIT 1].Id;

        // Create Test Users
        lstUser = new List<User>();
        lstUser.add(clsTestData_User.createUser_SystemAdministrator('tadm001', false));
        lstUser.add(clsTestData_User.createUser_SystemAdministrator('tadm002', false));
        lstUser.add(clsTestData_User.createUser_SystemAdministrator('tadm003', false));
        insert lstUser;

        // Get Territory, UserTerritory and Users with multiple Territories
        List<Territory2> lstTerritory = 
            [
                SELECT 
                    Id, Name, DeveloperName, Company__c, CurrencyIsoCode, OpportunityAccessLevel 
                FROM 
                    Territory2
                WHERE 
                    CurrencyIsoCode  = 'EUR'    
                    AND OpportunityAccessLevel = 'Edit'
            ];
        oTerritory = lstTerritory[0];


        List<UserTerritory2Association> lstUserTerritory =
            [
                SELECT
                    Id, IsActive, Territory2Id, UserId 
                FROM 
                    UserTerritory2Association
                WHERE
                    IsActive = true
                    AND Territory2Id = :lstTerritory
                ORDER BY
                    UserId, Territory2Id
            ]; 
        Map<Id, Set<Id>> mapUserID_TerritoryID = new Map<Id, Set<Id>>();
        for (UserTerritory2Association oUserTerritory : lstUserTerritory){
            Set<Id> setID_Territory = new Set<Id>();
            if (mapUserID_TerritoryID.containsKey(oUserTerritory.UserId)){
                setID_Territory = mapUserID_TerritoryID.get(oUserTerritory.UserId);
            }
            setID_Territory.add(oUserTerritory.Territory2Id);
            mapUserID_TerritoryID.put(oUserTerritory.UserId, setID_Territory);
        }
        Set<Id> setID_UserWithMultipleTerritories = new Set<Id>();
        for (Id id_User : mapUserID_TerritoryID.keySet()){
            Set<Id> setID_Territory = mapUserID_TerritoryID.get(id_User);
            if (setID_Territory.size() > 1){
                setID_UserWithMultipleTerritories.add(id_User);
            }
        }

        lstUser_WithMultipleTerritories = 
            [
                SELECT Id
                FROM User
                WHERE Id = :setID_UserWithMultipleTerritories
            ];


        // Create Data
        Account oAccount;
        System.runAs(lstUser[0]){
            clsTestData.createCompanyData();
                if (bUpdateCompanyData){
                    for (Company__c oCompany : clsTestData.lstCompany){
                        oCompany.Ignore_Account_Territory__c = true; 
                        oCompany.Ignore_User_Territory__c = true;
                    }
                    update clsTestData.lstCompany;
                }
            clsTestData.createAccountData();
            oAccount = clsTestData.oMain_Account;
        }

        Id idRecordType_Opportunity_EURMITG = clsUtil.getRecordTypeByDevName('Opportunity', 'EUR_MITG_Standard_Opportunity').Id;
        Id idRecordType_Opportunity_MEAMITG = clsUtil.getRecordTypeByDevName('Opportunity', 'MEA_MITG_Standard_Opportunity').Id;
        Id idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'Non_Capital_Opportunity').Id;

        System.runAs(lstUser[1]){
            lstOpportunity = new List<Opportunity>();
            Opportunity oOpportunity_EUR1 = new Opportunity(AccountId = oAccount.Id, name = 'Test oppty MITG EUR 1', CloseDate = System.Today(), StageName = 'Closed Won Hurray!', DoNotByPassTerritorySelection__c = true, Initiative__c = 'Hernia', RecordTypeId = idRecordType_Opportunity_EURMITG);
            lstOpportunity.add(oOpportunity_EUR1);
            Opportunity oOpportunity_EUR2 = new Opportunity(AccountId = oAccount.Id, name = 'Test oppty MITG EUR 2', CloseDate = System.Today(), StageName = 'Closed Won Hurray!', DoNotByPassTerritorySelection__c = false, Initiative__c = 'Hernia', RecordTypeId = idRecordType_Opportunity_EURMITG);
            lstOpportunity.add(oOpportunity_EUR2);
            Opportunity oOpportunity_MEA1 = new Opportunity(AccountId = oAccount.Id, name = 'Test oppty MITG MEA 1', CloseDate = System.Today(), StageName = 'Closed Won Hurray!', DoNotByPassTerritorySelection__c = true, RecordTypeId = idRecordType_Opportunity_MEAMITG);
            lstOpportunity.add(oOpportunity_MEA1);
            Opportunity oOpportunity_MEA2 = new Opportunity(AccountId = oAccount.Id, name = 'Test oppty MITG MEA 2', CloseDate = System.Today(), StageName = 'Closed Won Hurray!', DoNotByPassTerritorySelection__c = false, RecordTypeId = idRecordType_Opportunity_MEAMITG);
            lstOpportunity.add(oOpportunity_MEA2);
            Opportunity oOpportunity_1 = new Opportunity(AccountId = oAccount.Id, name = 'Test oppty 1', CloseDate = System.Today(), StageName = 'Closed Won Hurray!', DoNotByPassTerritorySelection__c = true, RecordTypeId = idRecordType_Opportunity);
            lstOpportunity.add(oOpportunity_1);
            Opportunity oOpportunity_2 = new Opportunity(AccountId = oAccount.Id, name = 'Test oppty 2', CloseDate = System.Today(), StageName = 'Closed Won Hurray!', DoNotByPassTerritorySelection__c = false, RecordTypeId = idRecordType_Opportunity);
            lstOpportunity.add(oOpportunity_2);
            insert lstOpportunity;
        }

        System.runAs(lstUser_WithMultipleTerritories[0]){
            lstOpportunity2 = new List<Opportunity>();
            Opportunity oOpportunity_EUR1 = new Opportunity(AccountId = oAccount.Id, name = 'Test oppty MITG EUR 1', CloseDate = System.Today(), StageName = 'Closed Won Hurray!', DoNotByPassTerritorySelection__c = true, Initiative__c = 'Hernia', RecordTypeId = idRecordType_Opportunity_EURMITG);
            lstOpportunity.add(oOpportunity_EUR1);
            Opportunity oOpportunity_EUR2 = new Opportunity(AccountId = oAccount.Id, name = 'Test oppty MITG EUR 2', CloseDate = System.Today(), StageName = 'Closed Won Hurray!', DoNotByPassTerritorySelection__c = false, Initiative__c = 'Hernia', RecordTypeId = idRecordType_Opportunity_EURMITG);
            lstOpportunity.add(oOpportunity_EUR2);
            Opportunity oOpportunity_MEA1 = new Opportunity(AccountId = oAccount.Id, name = 'Test oppty MITG MEA 1', CloseDate = System.Today(), StageName = 'Closed Won Hurray!', DoNotByPassTerritorySelection__c = true, RecordTypeId = idRecordType_Opportunity_MEAMITG);
            lstOpportunity.add(oOpportunity_MEA1);
            Opportunity oOpportunity_MEA2 = new Opportunity(AccountId = oAccount.Id, name = 'Test oppty MITG MEA 2', CloseDate = System.Today(), StageName = 'Closed Won Hurray!', DoNotByPassTerritorySelection__c = false, RecordTypeId = idRecordType_Opportunity_MEAMITG);
            lstOpportunity.add(oOpportunity_MEA2);
            Opportunity oOpportunity_1 = new Opportunity(AccountId = oAccount.Id, name = 'Test oppty 1', CloseDate = System.Today(), StageName = 'Closed Won Hurray!', DoNotByPassTerritorySelection__c = true, RecordTypeId = idRecordType_Opportunity);
            lstOpportunity.add(oOpportunity_1);
            Opportunity oOpportunity_2 = new Opportunity(AccountId = oAccount.Id, name = 'Test oppty 2', CloseDate = System.Today(), StageName = 'Closed Won Hurray!', DoNotByPassTerritorySelection__c = false, RecordTypeId = idRecordType_Opportunity);
            lstOpportunity.add(oOpportunity_2);
            insert lstOpportunity2;
        }

        lstOpportunity = [SELECT Id, Name, LastmodifiedDate, CreatedDate, AccountId, OwnerId, Territory2Id, DoNotByPassTerritorySelection__c, Owner.Company_Code_Text__c FROM Opportunity WHERE Id = :lstOpportunity];
        lstOpportunity2 = [SELECT Id, Name, LastmodifiedDate, CreatedDate, AccountId, OwnerId, Territory2Id, DoNotByPassTerritorySelection__c, Owner.Company_Code_Text__c FROM Opportunity WHERE Id = :lstOpportunity2];

    }
    
    @isTest static void test_ctrlExt_Opportunity_TerritoryCheck_1_1() {

        // Create Data
        bUpdateCompanyData = false;
        createTestData();

        Test.startTest();

        System.runAs(lstUser[1]){

            for (Opportunity oOpportunity : lstOpportunity){
                testControllerExtension1(oOpportunity);
            }

        }

        Test.stopTest();

    }

    @isTest static void test_ctrlExt_Opportunity_TerritoryCheck_1_2() {

        // Create Data
        bUpdateCompanyData = false;
        createTestData();

        Test.startTest();

        System.runAs(lstUser[1]){

            for (Opportunity oOpportunity : lstOpportunity){
                testControllerExtension2(oOpportunity);
            }

        }

        Test.stopTest();

    }


    @isTest static void test_ctrlExt_Opportunity_TerritoryCheck_2_1() {

        // Create Data
        bUpdateCompanyData = true;
        createTestData();

        System.runAs(lstUser[1]){

            for (Opportunity oOpportunity : lstOpportunity){
                oOpportunity.OwnerId = lstUser[2].Id;
            }
            update lstOpportunity;

        }
        
        Test.startTest();

        System.runAs(lstUser[2]){

            for (Opportunity oOpportunity : lstOpportunity){
                testControllerExtension1(oOpportunity);
            }

        }
    
        Test.stopTest();

    }

	    @isTest static void test_ctrlExt_Opportunity_TerritoryCheck_2_2() {

        // Create Data
        bUpdateCompanyData = true;
        createTestData();

        System.runAs(lstUser[1]){

            for (Opportunity oOpportunity : lstOpportunity){
                oOpportunity.OwnerId = lstUser[2].Id;
            }
            update lstOpportunity;

        }
        
        Test.startTest();

        System.runAs(lstUser[2]){

            for (Opportunity oOpportunity : lstOpportunity){
                testControllerExtension2(oOpportunity);
            }

        }
    
        Test.stopTest();

    }
    @isTest static void test_ctrlExt_Opportunity_TerritoryCheck_3_1() {

        // Create Data
        bUpdateCompanyData = true;
        createTestData();

        User oUser = lstUser_WithMultipleTerritories[1];

        System.runAs(lstUser[1]){

            for (Opportunity oOpportunity : lstOpportunity2){
                oOpportunity.OwnerId = lstUser_WithMultipleTerritories[1].Id;
            }
            update lstOpportunity2;

        }

        Test.startTest();

        System.runAs(lstUser_WithMultipleTerritories[1]){

            for (Opportunity oOpportunity : lstOpportunity2){
                testControllerExtension1(oOpportunity);
            }

        }
    
        Test.stopTest();
        
    }    

    @isTest static void test_ctrlExt_Opportunity_TerritoryCheck_3_2() {

        // Create Data
        bUpdateCompanyData = true;
        createTestData();

        User oUser = lstUser_WithMultipleTerritories[1];

        System.runAs(lstUser[1]){

            for (Opportunity oOpportunity : lstOpportunity2){
                oOpportunity.OwnerId = lstUser_WithMultipleTerritories[1].Id;
            }
            update lstOpportunity2;

        }

        Test.startTest();

        System.runAs(lstUser_WithMultipleTerritories[1]){

            for (Opportunity oOpportunity : lstOpportunity2){
                testControllerExtension2(oOpportunity);
            }

        }
    
        Test.stopTest();
        
    }    

    private static void testControllerExtension1(Opportunity oOpportunity){

        PageReference oPageRef = Page.Opportunity_TerritoryCheck;
        Test.setCurrentPageReference(oPageRef);

        ApexPages.StandardController oStdCtrl = new ApexPages.standardController(oOpportunity);
        ctrlExt_Opportunity_TerritoryCheck oCtrlExt = new ctrlExt_Opportunity_TerritoryCheck(oStdCtrl);
        
        PageReference init = oCtrlExt.init();
        
        PageReference saveTerritory = oCtrlExt.saveTerritory();
        oCtrlExt.selectedTerrId = oTerritory.Id;
        saveTerritory = oCtrlExt.saveTerritory();
        
        PageReference getNoOverrideURL = oCtrlExt.getNoOverrideURL();

        List<Territory2> lstTerritory = oCtrlExt.lstTerritory;

    }

    private static void testControllerExtension2(Opportunity oOpportunity){

        PageReference oPageRef = Page.Opportunity_TerritoryCheck;
            oPageRef.getParameters().put('displayTrueTeamAccess', '1');          
        Test.setCurrentPageReference(oPageRef);

        ApexPages.StandardController oStdCtrl = new ApexPages.standardController(oOpportunity);
        ctrlExt_Opportunity_TerritoryCheck oCtrlExt = new ctrlExt_Opportunity_TerritoryCheck(oStdCtrl);
        
        PageReference init = oCtrlExt.init();

        PageReference saveTerritory = oCtrlExt.saveTerritory();
        oCtrlExt.selectedTerrId = oTerritory.Id;
        saveTerritory = oCtrlExt.saveTerritory();

        PageReference getNoOverrideURL = oCtrlExt.getNoOverrideURL();

        List<Territory2> lstTerritory = oCtrlExt.lstTerritory;

    }    
        
}