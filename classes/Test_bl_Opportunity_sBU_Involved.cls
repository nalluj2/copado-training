@isTest
private class Test_bl_Opportunity_sBU_Involved {
	
	private static testmethod void testEMEAIHS(){
    	
    	Account acc = new Account();
    	acc.Name = 'IHS Account';
    	acc.Account_Country_vs__c = 'Netherlands';
    	insert acc;
    	
        Id EMEA_IHS_rt = clsUtil.getRecordTypeByDevName('Opportunity', 'EMEA_IHS_Managed_Services').Id;
    	
    	Opportunity opp_EMEA = new Opportunity();
        opp_EMEA.RecordTypeId = EMEA_IHS_rt;
        opp_EMEA.Name = 'TEST Opportunity';
        opp_EMEA.AccountId = acc.Id;
        opp_EMEA.CloseDate = Date.today().addMonths(6);
        opp_EMEA.StageName = '0 - Tracking';
        opp_EMEA.Deal_Category__c = 'New Customer';
        opp_EMEA.Origin_Text__c = 'NTG Program';
        opp_EMEA.Type = 'Standard Sale';
        opp_EMEA.Amount = 50000;
    	
    	insert new List<Opportunity>{opp_EMEA};
    	    	
    	Opportunity_BU_Involved__c EMEA_CVG = new Opportunity_BU_Involved__c();
    	EMEA_CVG.Opportunity__c = opp_EMEA.Id;
    	EMEA_CVG.BU_Involved__c = 'CVG';    	
    	EMEA_CVG.BU_Percentage__c = 40;
    	
    	Opportunity_BU_Involved__c EMEA_MITG = new Opportunity_BU_Involved__c();
    	EMEA_MITG.Opportunity__c = opp_EMEA.Id;
    	EMEA_MITG.BU_Involved__c = 'MITG';    	
    	EMEA_MITG.BU_Percentage__c = 40;
    	
    	Opportunity_BU_Involved__c EMEA_RTG = new Opportunity_BU_Involved__c();
    	EMEA_RTG.Opportunity__c = opp_EMEA.Id;
    	EMEA_RTG.BU_Involved__c = 'RTG';    	
    	EMEA_RTG.BU_Percentage__c = 20;
    	
    	insert new List<Opportunity_BU_Involved__c>{EMEA_CVG, EMEA_MITG, EMEA_RTG};
    	    	
    	Opportunity_SBU_Involved__c EMEA_CVG_Vascular = new Opportunity_SBU_Involved__c();
    	EMEA_CVG_Vascular.Opportunity_BU_Involved__c = EMEA_CVG.Id;
    	EMEA_CVG_Vascular.Name = 'Coro + PV';
    	EMEA_CVG_Vascular.Percentage__c = 20;
    	
    	Opportunity_SBU_Involved__c EMEA_CVG_CRHF = new Opportunity_SBU_Involved__c();
    	EMEA_CVG_CRHF.Opportunity_BU_Involved__c = EMEA_CVG.Id;
    	EMEA_CVG_CRHF.Name = 'CRHF';   	
    	EMEA_CVG_CRHF.Percentage__c = 20;
    	
    	Opportunity_SBU_Involved__c EMEA_MITG_SI = new Opportunity_SBU_Involved__c();
    	EMEA_MITG_SI.Opportunity_BU_Involved__c = EMEA_MITG.Id;
    	EMEA_MITG_SI.Name = 'Surgical Innovations';   	
    	EMEA_MITG_SI.Percentage__c = 40;
    	
    	Opportunity_SBU_Involved__c EMEA_RTG_ENT = new Opportunity_SBU_Involved__c();
    	EMEA_RTG_ENT.Opportunity_BU_Involved__c = EMEA_RTG.Id;
    	EMEA_RTG_ENT.Name = 'Cranial Spinal';   	
    	EMEA_RTG_ENT.Percentage__c = 20;
    	
    	Test.startTest();	
    	
    	insert new List<Opportunity_SBU_Involved__c>{EMEA_CVG_Vascular, EMEA_CVG_CRHF, EMEA_MITG_SI, EMEA_RTG_ENT};
    	    	
    	EMEA_CVG = [Select SBUs_Involved__c from Opportunity_BU_Involved__c where Id = :EMEA_CVG.Id];
		System.debug('**BC** EMEA_CVG.SBUs_Involved__c : ' + EMEA_CVG.SBUs_Involved__c);
    	System.assert(EMEA_CVG.SBUs_Involved__c == 'Coro + PV - 20%;CRHF - 20%');
    	
    	EMEA_MITG = [Select SBUs_Involved__c from Opportunity_BU_Involved__c where Id = :EMEA_MITG.Id];
    	System.assert(EMEA_MITG.SBUs_Involved__c == 'Surgical Innovations - 40%');
    	
    	EMEA_RTG = [Select SBUs_Involved__c from Opportunity_BU_Involved__c where Id = :EMEA_RTG.Id];
    	System.assert(EMEA_RTG.SBUs_Involved__c == 'Cranial Spinal - 20%');
    	
    	Boolean isError = false;
    	
    	try{
    		
    		EMEA_CVG_CRHF.Percentage__c = null;
    		update EMEA_CVG_CRHF;
    		
    	}catch(Exception e){
    		
    		isError = true;
    		System.assert(e.getMessage().contains('If at least one of the Sub-Business Units has a percentage value, all the other Sub-Business Units of the same Business Unit must have a percentage value too'));
    	}
    	
    	System.assert(isError == true);
    	
    	isError = false;
    	
    	try{
    		
    		EMEA_CVG_CRHF.Percentage__c = 30;
    		update EMEA_CVG_CRHF;
    		
    	}catch(Exception e){
    		
    		isError = true;
    		System.assert(e.getMessage().contains('The sum of the Sub-Business Units percentages must be equal to the Business Unit percentage'));
    	}
    	
    	System.assert(isError == true);
    	
    	bl_Opportunity_sBU_Involved.skipValidation = true;
    	
    	delete EMEA_MITG_SI;
    	
    	bl_Opportunity_sBU_Involved.skipValidation = false;
    	
    	undelete EMEA_MITG_SI;
    	 
    }
    
    private static testmethod void testEMEALeadIHS(){
    	
    	Account acc = new Account();
    	acc.Name = 'IHS Account';
    	acc.Account_Country_vs__c = 'Netherlands';
    	insert acc;
    	
    	Id EMEA_IHS_rt = clsUtil.getRecordTypeByDevName('Opportunity', 'EMEA_IHS_Lead').Id;
    	
    	Opportunity opp_EMEA= new Opportunity();
        opp_EMEA.RecordTypeId = EMEA_IHS_rt;
        opp_EMEA.Name = 'TEST Opportunity Lead';
        opp_EMEA.AccountId = acc.Id;
        opp_EMEA.CloseDate = Date.today().addMonths(6);
        opp_EMEA.StageName = '0 - Lead';        
        opp_EMEA.Type = 'Standard Sale';
        opp_EMEA.Amount = 100000;
                
        insert new List<Opportunity>{opp_EMEA};
                   	    	
    	Opportunity_BU_Involved__c EMEA_MITG = new Opportunity_BU_Involved__c();
    	EMEA_MITG.Opportunity__c = opp_EMEA.Id;
    	EMEA_MITG.BU_Involved__c = 'MITG';
    	   	
    	Opportunity_BU_Involved__c EMEA_RTG = new Opportunity_BU_Involved__c();
		EMEA_RTG.Opportunity__c = opp_EMEA.Id;
    	EMEA_RTG.BU_Involved__c = 'RTG';
    	   	
    	insert new List<Opportunity_BU_Involved__c>{EMEA_MITG, EMEA_RTG};
    	    	    	
    	Opportunity_SBU_Involved__c EMEA_MITG_SI = new Opportunity_SBU_Involved__c();
    	EMEA_MITG_SI.Opportunity_BU_Involved__c = EMEA_MITG.Id;
    	EMEA_MITG_SI.Name = 'Surgical Innovations';   	
    	   	
    	Opportunity_SBU_Involved__c EMEA_RTG_SI = new Opportunity_SBU_Involved__c();
    	EMEA_RTG_SI.Opportunity_BU_Involved__c = EMEA_RTG.Id;
    	EMEA_RTG_SI.Name = 'Surgical Innovations';   	

    	Test.startTest();	
    	
    	insert new List<Opportunity_SBU_Involved__c>{EMEA_MITG_SI};
    	    	
    	EMEA_MITG = [Select SBUs_Involved__c from Opportunity_BU_Involved__c where Id = :EMEA_MITG.Id];
    	System.assert(EMEA_MITG.SBUs_Involved__c == 'Surgical Innovations');
    	
    	EMEA_RTG = [Select SBUs_Involved__c from Opportunity_BU_Involved__c where Id = :EMEA_RTG.Id];
    	System.assert(EMEA_RTG.SBUs_Involved__c == null);


    	Boolean isError = false;
    	
    	try{
    		
			insert EMEA_RTG_SI;
    		
    	}catch(Exception e){
    		
    		isError = true;
    		System.assert(e.getMessage().contains('duplicate value'));
    	}
    	
    	System.assert(isError == true);
  
    }        
}