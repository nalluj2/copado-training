public without sharing class bl_UMD_Input_Complete_Trigger {
    
    public static void setUniqueKey(List<UMD_Input_Complete__c> triggerNew, Map<Id, UMD_Input_Complete__c> oldMap){
    	
    	Set<String> sbus = new Set<String>();
    	List<UMD_Input_Complete__c> toPopulate = new List<UMD_Input_Complete__c>();
    	
    	for(UMD_Input_Complete__c inputComplete : triggerNew){
    		
    		inputComplete.Unique_Key__c = inputComplete.Unique_Key_Formula__c;
    		
    		if(inputComplete.Sub_Business_Unit__c != null && (inputComplete.Business_Unit__c == null || inputComplete.Business_Unit_Group__c == null)){
    			
    			sbus.add(inputComplete.Sub_Business_Unit__c);
    			toPopulate.add(inputComplete);
    		}
    	}
    	
    	if(toPopulate.size() > 0){
	    		    	
	    	Map<String, Sub_Business_Units__c> sbuMap = new Map<String, Sub_Business_Units__c>();	    	
	    	for(Sub_Business_Units__c sbu : [Select Name, Business_Unit__r.Name, Business_Unit__r.Business_Unit_Group__r.Name from Sub_Business_Units__c where Name IN :sbus AND Business_Unit__r.Company__r.Name = 'Europe']){
	    		
	    		sbuMap.put(sbu.Name, sbu);
	    	}
	    	
	    	for(UMD_Input_Complete__c inputComplete : toPopulate){
	    		
	    		Sub_Business_Units__c sbu = sbuMap.get(inputComplete.Sub_Business_Unit__c);
	    		
	    		if(inputComplete.Business_Unit__c == null) inputComplete.Business_Unit__c = sbu.Business_Unit__r.Name;
	    		if(inputComplete.Business_Unit_Group__c == null) inputComplete.Business_Unit_Group__c = sbu.Business_Unit__r.Business_Unit_Group__r.Name;	    		    		
	    	}
    	}
    }
    
    public static void matchSegmentation(List<UMD_Input_Complete__c> triggerNew, Map<Id, UMD_Input_Complete__c> oldMap){
    	
    	Set<String> keys = new Set<String>();
    	List<UMD_Input_Complete__c> toPopulate = new List<UMD_Input_Complete__c>();
    	
    	for(UMD_Input_Complete__c inputComplete : triggerNew){
    		
    		if(oldMap == null || inputComplete.Unique_Key__c != oldMap.get(inputComplete.Id).Unique_Key__c){    				
    			
    			List<String> inputKey = inputComplete.Unique_Key__c.split(':'); 
    			
    			Integer inputFiscalYear = Integer.valueOf(inputKey[2].split('FY')[1]);    			
    			String key = inputKey[0] + ':' + inputKey[1] + ':FY' + (inputFiscalYear - 1);
    			
    			keys.add(key);
    			toPopulate.add(inputComplete);
    		}	
    	}
    	
    	if(toPopulate.size() > 0){
	    	
	    	Map<String, Id> customerSegmentationMap = new Map<String, Id>();
	    	for(Customer_Segmentation__c customerSegmentation : [Select Id, Unique_Key__c from Customer_Segmentation__c where Unique_Key__c IN :keys]) customerSegmentationMap.put(customerSegmentation.Unique_Key__c, customerSegmentation.Id);
	    		    	
	    	for(UMD_Input_Complete__c inputComplete : toPopulate){
	    		
	    		List<String> inputKey = inputComplete.Unique_Key__c.split(':'); 
    			
    			Integer inputFiscalYear = Integer.valueOf(inputKey[2].split('FY')[1]);    			
    			String key = inputKey[0] + ':' + inputKey[1] + ':FY' + (inputFiscalYear - 1);
	    		
	    		inputComplete.Current_Segmentation__c = customerSegmentationMap.get(key);	    		
	    	}
    	}
    }
    
    public static void manageSharing(List<UMD_Input_Complete__c> triggerNew, Map<Id, UMD_Input_Complete__c> oldMap){
    	
    	List<UMD_Input_Complete__c> toCalculate = new List<UMD_Input_Complete__c>();
    	
    	for(UMD_Input_Complete__c inputComplete : triggerNew){
    		
    		if(oldMap == null || inputComplete.Assigned_To__c != oldMap.get(inputComplete.Id).Assigned_To__c || inputComplete.Approver_1__c != oldMap.get(inputComplete.Id).Approver_1__c || inputComplete.Approver_2__c != oldMap.get(inputComplete.Id).Approver_2__c ){
    			
    			toCalculate.add(inputComplete);
    		}
    	}
    	
    	if(toCalculate.size() > 0){
    		
    		Map<Id, UMD_Input_Complete__c> inputCompleteMap = new Map<Id, UMD_Input_Complete__c>([Select Id, (Select Id, UserOrGroupId from Shares where RowCause = 'Assigned_Approver__c') from UMD_Input_Complete__c where Id IN: toCalculate]);
    		
    		List<UMD_Input_Complete__Share> toInsert = new List<UMD_Input_Complete__Share>();
    		List<UMD_Input_Complete__Share> toDelete = new List<UMD_Input_Complete__Share>();
    		
    		for(UMD_Input_Complete__c inputComplete : toCalculate){
    			    			    			
    			Map<Id, UMD_Input_Complete__Share> existingUserShares = new Map<Id, UMD_Input_Complete__Share>();
    			
    			for(UMD_Input_Complete__Share share : inputCompleteMap.get(inputComplete.Id).Shares){
    				
    				existingUserShares.put(share.UserOrGroupId, share);
    			}
    			    			
    			Set<Id> userIds = new Set<Id>();
    			
    			if(inputComplete.Assigned_to__c != null) userIds.add(inputComplete.Assigned_to__c);
    			if(inputComplete.Approver_1__c != null) userIds.add(inputComplete.Approver_1__c);
    			if(inputComplete.Approver_2__c != null) userIds.add(inputComplete.Approver_2__c);
    			
    			for(Id userId : userIds){
    				
    				if(existingUserShares.containsKey(userId) == false){
    					
    					UMD_Input_Complete__Share share = new UMD_Input_Complete__Share();
    					share.ParentID = inputComplete.Id;
    					share.RowCause = Schema.UMD_Input_Complete__Share.RowCause.Assigned_Approver__c;
    					share.UserOrGroupId = userId;
    					share.AccessLevel = 'Edit';
    					toInsert.add(share);
    				}
    			}	
    			
    			for(Id userId : existingUserShares.keySet()){
    				
    				if(userIds.contains(userId) == false) toDelete.add(existingUserShares.get(userId));
    			}
    		}
    		
    		if(toInsert.size() > 0) insert toInsert;
    		if(toDelete.size() > 0) delete toDelete;    		
    	}
    }
}