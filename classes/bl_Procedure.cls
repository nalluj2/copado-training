public class bl_Procedure {
    
    public static Account_Procedure__c saveProcedure(Account_Procedure__c procedure){
		
		if (null != procedure){
					
			upsert procedure Mobile_ID__c;
		}
		
		return procedure;		
	}
}