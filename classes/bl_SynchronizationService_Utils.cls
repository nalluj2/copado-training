public class bl_SynchronizationService_Utils {
	
	public static Boolean isSyncProcess = false;
			
	//For Test execution only
	public static HttpCalloutMock calloutMock;
	
	public static Boolean isSyncEnabled 
	{
		get{
			if(isSyncEnabled == null){ 
		
				List<Synchronization_Service_Setting__c> settings = [Select Sync_Enabled__c from Synchronization_Service_Setting__c LIMIT 1];   		
   			
   				if(settings.size() == 0) isSyncEnabled = false;
   				else isSyncEnabled = settings[0].Sync_Enabled__c;
			}
		
   			return isSyncEnabled;	
		}
		
		set;
	}
	
	public static Object executeService(String serviceName, String restMethod, String requestBody, System.Type responseType, SessionInfo sessionInfo){
		
		//getTargetOrgSession();
		
		//Create the HTTP request to be sent to the target Org  	
     	HttpRequest req = new HttpRequest();    	    	
	    req.setEndpoint(sessionInfo.targetURL + serviceName);
	    req.setMethod(restMethod);
	    req.setHeader('Authorization', 'Bearer ' + sessionInfo.sessionId);
	    req.setHeader('Content-Type', 'application/json'); 	   
	    req.setTimeout(120000); 
	    
	    if(requestBody != null) req.setBody(requestBody);
	    
    	Http http = new Http();
        HttpResponse res;
        
        if(Test.isRunningTest() == false ) res = http.send(req); 
        else res = calloutMock.respond(req);
    	     	
     	if(res.getStatusCode() < 200 || res.getStatusCode() >= 300){
     		
     		throw new SyncService_Exception('Error during Rest call: ' + res.getStatus() + ' ('+ res.getStatusCode() + '). Response received : ' + res.getBody());        	
     	}
     	     	
     	if(responseType != null) return JSON.deserialize(res.getBody(), responseType);
		
		return null;		
	}
	
	public static Blob getAttachmentBody(String attachmentId, SessionInfo sessionInfo){
		
		//getTargetOrgSession();
		
		//Create the HTTP request to be sent to the target Org  	
     	HttpRequest req = new HttpRequest();    	    	
	    req.setEndpoint(sessionInfo.targetURL + '/services/data/v34.0/sobjects/Attachment/' + attachmentId + '/body');
	    req.setMethod('GET');
	    req.setHeader('Authorization', 'Bearer ' + sessionInfo.sessionId);
	    req.setHeader('Content-Type', 'application/json'); 	   
	    req.setTimeout(120000); 
    
    	Http http = new Http();
        HttpResponse res = http.send(req);
    	     	
     	if(res.getStatusCode() < 200 || res.getStatusCode() >= 300){
     		
     		throw new SyncService_Exception('Error during Rest call: ' + res.getStatus() + ' ('+ res.getStatusCode() + '). Response received : ' + res.getBody());        	
     	}
 						
		return res.getBodyAsBlob();	
	}
	
	public static List<SObject> executeQuery(String query, Map<String, String> fieldMapping, SessionInfo sessionInfo){
		
		//getTargetOrgSession();
		
		//Create the HTTP request to be sent to the target Org  	
	    HttpRequest req = new HttpRequest();    	    	
		req.setEndpoint(sessionInfo.targetURL + '/services/data/v30.0/query/?q=' + EncodingUtil.URLEncode(query, 'UTF-8'));
		req.setMethod('GET');
		req.setHeader('Authorization', 'Bearer ' + sessionInfo.sessionId);
		req.setHeader('Content-Type', 'application/json');
		req.setTimeout(120000);
		 		    
	    Http http = new Http();
	    HttpResponse res = http.send(req);
	    	     	
	    if(res.getStatusCode() < 200 || res.getStatusCode() >= 300){
	     		
	    	throw new SyncService_Exception('Error during Rest call: ' + res.getStatus() + ' ('+ res.getStatusCode() + '). Response received : ' + res.getBody());	             	
	    }
	    
	    String resBody =  res.getBody();
	    
	    if(fieldMapping != null) resBody = translatePayload (resBody, fieldMapping);
	     	     	     	
	    //If we receive a response successully, we collect the Records returned and we create/update them locally
	    QueryResponse response = (QueryResponse) JSON.deserialize(resBody, QueryResponse.class);
	     	
	    List<SObject> records = new List<SObject>();     	
	    records.addAll(response.records);
	     	
	    //Theoretically SFDC may return the records in batches if the size of the response is too big. Therefore we need to check if that is the case and handle this case.
     	if(response.done == false){
	       	     			
	    	addAllRecords(response.nextRecordsUrl, records, sessionInfo);
     	}
     	
	    return records;
	}
	
	private static void addAllRecords(String nextRecordsUrl , List<SObject> records, SessionInfo sessionInfo){
   		
   		//Create the HTTP request to be sent to the target Org  	
     	HttpRequest req = new HttpRequest();    	    	
	    req.setEndpoint(sessionInfo.targetURL + nextRecordsUrl);
	    req.setMethod('GET');
	    req.setHeader('Authorization', 'Bearer ' + sessionInfo.sessionId);
	    req.setHeader('Content-Type', 'application/json');
	    req.setTimeout(120000);
	    	     
    	Http http = new Http();
        HttpResponse res = http.send(req);
    	     	
     	if(res.getStatusCode() < 200 || res.getStatusCode() >= 300){
     		
     		throw new SyncService_Exception('Error during Rest call: ' + res.getStatus() + ' ('+ res.getStatusCode() + '). Response received : ' + res.getBody());        	
     	}
     	     	     	
     	//If we receive a response successully, we collect the Records returned and we create/update them locally
     	QueryResponse response = (QueryResponse) JSON.deserialize( res.getBody(), QueryResponse.class);
     	
     	records.addAll(response.records);
     	
     	//Recurrent call until the query has finished
     	if(response.done == false) addAllRecords(response.nextRecordsUrl, records, sessionInfo);
   	}
	
	public static void notifySyncErrors(List<String> errors){
		
		//The list of users to be notified is stored in a Public Group
   		List<GroupMember> notifyUsers = [Select UserOrGroupId from GroupMember where Group.Name = 'Synchronization Service Admins'];
   			
   		if(notifyUsers.size()>0){
   				
   			List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();
   				
   			String emailBody = '';
   				
   			for(String errorMessage : errors){
   				emailBody += errorMessage + ' <br/><br/> ';
   			}
   				
   			for(GroupMember userMember : notifyUsers){
   				
	   			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
				mail.setTargetObjectId(userMember.UserOrGroupId);
				mail.setSaveAsActivity(false);
				mail.setSubject('There have been errors in the Synchronization Service process');
				mail.setHTMLBody('Error details : <br/><br/> '+ emailBody);
					
				messages.add(mail);
   			}
				
			Messaging.sendEmail(messages);   				
   		}   	
	}
	
	public static SessionInfo getTargetOrgSession(){
		
		//if(targetURL != null && sessionId != null) return;
		
		List<Synchronization_Service_Setting__c> settings = [Select username__c, password__c, client_id__c, client_secret__c, is_sandbox__c from Synchronization_Service_Setting__c LIMIT 1];
   		
   		if(settings.size() == 0) throw new SyncService_Exception('No sync properties found');
   		
   		Synchronization_Service_Setting__c setting = settings[0];
   		   		   		
   		HttpRequest req = new HttpRequest();
    	    	
    	if(setting.Is_sandbox__c == true) req.setEndpoint('https://test.salesforce.com/services/oauth2/token');
    	else req.setEndpoint('https://login.salesforce.com/services/oauth2/token');
    	
	    req.setMethod('POST');
	    String body  = 'grant_type=password';
	    body += '&client_id=' + setting.client_id__c;
	    body += '&client_secret=' + setting.client_secret__c;
	    body += '&username=' + EncodingUtil.URLEncode( setting.username__c, 'UTF-8');
	    body += '&password=' + EncodingUtil.URLEncode( setting.password__c, 'UTF-8');	
	    req.setBody(body);
	       		
    	Http http = new Http();
        HttpResponse res = http.send(req);
    	   		
   		if(res.getStatusCode() < 200 || res.getStatusCode() >= 300){
     		   
        	throw new SyncService_Exception('Error during login call: ' + res.getStatus() + ' ('+ res.getStatusCode() + '). Response received : ' + res.getBody());
     	}
   		
   		LoginResponse response = (LoginResponse) JSON.deserialize(res.getBody(), LoginResponse.class);
   		   		
   		SessionInfo sessionInfo = new SessionInfo();
   		sessionInfo.targetURL = response.instance_url;
   		sessionInfo.sessionId = response.access_token;
   		
   		return sessionInfo;
	}
	
	//Returns true if any of the listed fields has a different value than the previous record version
	public static Boolean hasChanged(SObject newRecord, SObject oldRecord, List<String> syncFields){
		
		for(String syncField : syncFields){
			
			if(newRecord.get(syncField) != oldRecord.get(syncField) ) return true;
		}	
		
		return false;
	}
	
	//Used to translate field and object names from the JSON serialized String. Used for the cases when the API names do not match between orgs.
	public static String translatePayload(String payload, Map<String, String> fieldMapping){
		
		for(String key : fieldMapping.keySet()){
			
			payload = payload.replace('"' + key + '"', '"' + fieldMapping.get(key) + '"');
		}
		
		return payload;
	}
	
	//To force the JSON serialized object to include the null values we need to explicitely set the null value after querying the record 
	public static void prepareForJSON(SObject record, List<String> syncFields){
		
		for(String field : syncFields){				
			if(record.get(field) == null) record.put(field, null);
		}		
	}
		
	public class QueryResponse{
		public List<SObject> records {get;set;}
		public Integer totalSize {get;set;}
		public Boolean done {get;set;}
		public String nextRecordsUrl {get; set;}
	}
	
	private class LoginResponse{		
		public String id {get; set;}
		public String issued_at {get; set;}
		public String instance_url {get; set;}
		public String signature {get; set;}
		public String access_token {get; set;}
		public String token_type {get; set;}		
	}
	
	public class SessionInfo{
		
		public String targetURL;
		public String sessionId;
	}
	
	public class SyncService_Exception extends Exception{
		
	}
}