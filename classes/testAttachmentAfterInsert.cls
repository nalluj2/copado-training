@isTest
Public Class testAttachmentAfterInsert
{

    static testmethod void testAttachments()
    {   
    
        Test.starttest();
        
        Account acc=new Account();
        acc.AccountNumber='as';
        acc.Name='abc';
        acc.Account_Country_vs__c='USA';
        acc.CurrencyIsoCode='USD';
        acc.Phone='24107954';
        insert acc;
        
        Asset sys=new Asset();
        sys.Name='Niha';
        sys.Asset_Product_Type__c='Planning Station';
        sys.AccountId=acc.id;  
        sys.Serial_Nr__c = '0123445';         
        sys.Ownership_Status__c='EOL';  
        sys.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Navigation').getRecordTypeId();     
        insert sys;
        
//Anantha Added 11/6
        Product2 pro=new Product2();
        pro.Name='abc Test';
        pro.Tracking__c = TRUE ;
        pro.Maintenance_Frequency__c= 12;
       insert pro;
//End of Anantha Added

	    Complaint__c objcomplaint=new Complaint__c();
	    objcomplaint.Status__c='Open';
	    objcomplaint.Account_Name__c=acc.id;
	    objcomplaint.Asset__c=sys.id;                    
	    objcomplaint.Medtronic_Aware_Date__c = system.today();
	    objcomplaint.Formal_Investigation_Required__c='Yes';
	    objcomplaint.Date_FIR_Set_to_Yes__c = date.today();
	    objcomplaint.Formal_Investigation_Justification__c='abc';
	    objcomplaint.Methods__c='abc';
	    objcomplaint.Results__c='abcc';
	    objcomplaint.Formal_Investigation_Reference__c='asd';
	    //objcomplaint.Case__c=cse.id;
	    insert objcomplaint;
	    
	    Case cse=new Case();
	    cse.AccountId = acc.Id;
	    cse.AssetId = sys.Id;
	    cse.Complaint__c = objcomplaint.Id;
	    cse.Date_Complaint_Became_Known__c = Date.Today();
	    cse.Complaint_Case__c = 'Yes';
        cse.RecordTypeId = [Select Id from RecordType where SObjectType = 'Case' AND DeveloperName = 'New_Case'].Id;
        insert cse;
        
        Workorder__c WO =new Workorder__c();            
        WO.Account__c=acc.id;
        WO.Asset__c=sys.id;
        WO.CurrencyIsoCode='EUR';
        WO.Bio_Med_FSE_Visit__c='No';
        WO.Covered_Under__c='T&M';
        WO.Complaint__c=objcomplaint.id;
        WO.Date_Completed__c=system.today();
        WO.Status__c='In Process';
        WO.RecordTypeId = [Select Id from RecordType where SObjectType = 'Workorder__c' AND DeveloperName = 'Training'].Id;
        insert WO;
//Anantha Added 11/6        
        
        Workorder_Sparepart__c CP = new Workorder_Sparepart__c();
        CP.Account__c=acc.id;
        CP.Asset__c=sys.id;
        //CP.CurrencyIsoCode='USD';
        CP.Case__c=cse.id;
        CP.Complaint__c=objcomplaint.id;
        CP.Order_Part__c=pro.id;
        CP.Order_Status__c = 'Pending';
        CP.Return_Item_Status__c = 'Pending Return';
        CP.RI_Part__c =pro.id;
        CP.RecordTypeId = [Select Id from RecordType where SObjectType = 'Workorder_Sparepart__c' AND DeveloperName = 'Hardware'].Id;        
        insert CP;
//End of Anantha Added
        
        List<Attachment> objAttachment=new List<Attachment>();
        
        Attachment attach=new Attachment();     
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=WO.id;
        objAttachment.add(attach);
        
        Attachment attach1=new Attachment();     
        attach1.Name='Unit Test Attachment';
        Blob bodyBlob1=Blob.valueOf('Unit Test Attachment Body');
        attach1.body=bodyBlob1;
        attach1.parentId=cse.id;
        objAttachment.add(attach1);
 
 //Anantha Added 11/6        
        Attachment attachCP=new Attachment();     
        attachCP.Name='Unit Test Attachment';
        Blob bodyBlob2=Blob.valueOf('Unit Test Attachment Body');
        attachCP.body=bodyBlob2;
        attachCP.parentId=CP.id;       
        objAttachment.add(attachCP);
        
        
        insert objAttachment;
        //attach.parentId=WO.id;
        //update attach;
        Test.stoptest();
        
        List<Sync_Notification__c> notifications = [Select Record_Id__c from Sync_Notification__c where Record_Object_Type__c = 'Attachment'];
        
        System.assert(notifications.size() == 3);
        
        WO = [Select Has_Attachment__c from Workorder__c where id = :WO.Id];
        
        System.assert(WO.Has_Attachment__c == true);
        
        cse = [Select Has_Attachment__c from Case where id = :cse.Id];
        
        System.assert(cse.Has_Attachment__c == true);
    }
}