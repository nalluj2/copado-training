@isTest
private class Test_ctrlExt_Tender_Opportunity_BU_Info {
    
    private static testmethod void testUpdateOpportunityBUInfo(){

    	String tBUG_Name = 'Restorative';
    	String tBU_Name = 'Cranial Spinal';
    	String tSBU_Name = 'Brain Modulation';
	    	
    	RecordType tenderRT = [Select Name, Id from RecordType where sObjectType = 'Opportunity' AND DeveloperName = 'Business_Critical_Tender'];
    	
    	Account acc = new Account();        
        acc.Name = 'Test Germany';        
        acc.Account_Country_vs__c = 'GERMANY';
        acc.RecordTypeId = [Select Id from RecordType where SObjectType = 'Account' AND DeveloperName = 'SAP_Account'].Id;        
        acc.Account_Active__c = true;
        acc.SAP_ID__c = '1111111111';
        //acc.SAP_Channel__c = '30';
        insert acc;
        
        Opportunity tenderOpp = new Opportunity();
		tenderOpp.RecordTypeId = tenderRT.Id;
		tenderOpp.AccountId = acc.Id;
		tenderOpp.Name = 'Test Tender Opportunity';
        tenderOpp.CloseDate = Date.today().addDays(30);
        tenderOpp.StageName = 'Prospecting/Lead';		
		insert tenderOpp;
		
		Company__c cmpny = new Company__c();		
        cmpny.name='Europe';
        cmpny.CurrencyIsoCode = 'EUR';
        cmpny.Current_day_in_Q1__c=56;
        cmpny.Current_day_in_Q2__c=34; 
        cmpny.Current_day_in_Q3__c=5; 
        cmpny.Current_day_in_Q4__c= 0;   
        cmpny.Current_day_in_year__c =200;
        cmpny.Days_in_Q1__c=56;  
        cmpny.Days_in_Q2__c=34;
        cmpny.Days_in_Q3__c=13;
        cmpny.Days_in_Q4__c=22;
        cmpny.Days_in_year__c =250;
        cmpny.Company_Code_Text__c = 'T35';
		insert cmpny;
		
		Business_Unit_Group__c bug =  new Business_Unit_Group__c();
	        bug.Master_Data__c =cmpny.id;
    	    bug.name = tBUG_Name;      
        insert bug;
		
		Business_Unit__c bu =  new Business_Unit__c();
	        bu.Company__c =cmpny.id;
	        bu.name = tBU_Name;
	        bu.Business_Unit_Group__c = bug.Id;
	        bu.Account_Plan_Activities__c=true;        
        insert bu;
        
        Sub_Business_Units__c sbu = new Sub_Business_Units__c();
	        sbu.name = tSBU_Name;
	        sbu.Business_Unit__c=bu.id;
		insert sbu;
		
		User currentUser = new User(Id = UserInfo.getUserId());
		currentUser.Company_Code_text__c = 'T35';
		update currentUser;
		
		Test.startTest();
						
		ctrlExt_Tender_Opportunity_BU_Info controller = new ctrlExt_Tender_Opportunity_BU_Info(new ApexPages.StandardController(tenderOpp));
		
		System.assert(controller.bugs.size() == 1);
				
		controller.bugs[0].businessUnits[0].subBusinessUnits[0].selected = true;
		
		controller.sbuSelected();
		
		System.assert(controller.bugs[0].selected == true);
		System.assert(controller.bugs[0].businessUnits[0].selected == true);
		
		controller.save();
		
		tenderOpp = [Select Business_Unit_Group__c, Business_Unit_msp__c, Sub_Business_Unit__c from Opportunity where Id = :tenderOpp.Id];
		
		System.assert(tenderOpp.Business_Unit_Group__c == tBUG_Name);
		System.assert(tenderOpp.Business_Unit_msp__c == tBU_Name);
		System.assert(tenderOpp.Sub_Business_Unit__c == tSBU_Name);
		
		controller = new ctrlExt_Tender_Opportunity_BU_Info(new ApexPages.StandardController(tenderOpp));
		
		System.assert(controller.bugs[0].selected == true);
		System.assert(controller.bugs[0].businessUnits[0].selected == true);
		System.assert(controller.bugs[0].businessUnits[0].subBusinessUnits[0].selected == true);
		
		controller.bugs[0].businessUnits[0].subBusinessUnits[0].selected = false;
		
		controller.sbuSelected();
		
		System.assert(controller.bugs[0].selected == false);
		System.assert(controller.bugs[0].businessUnits[0].selected == false);
		
		controller.save();
		
		tenderOpp = [Select Business_Unit_Group__c, Business_Unit_msp__c, Sub_Business_Unit__c from Opportunity where Id = :tenderOpp.Id];
		
		System.assert(tenderOpp.Business_Unit_Group__c == null);
		System.assert(tenderOpp.Business_Unit_msp__c == null);
		System.assert(tenderOpp.Sub_Business_Unit__c == null);
    }
    
    
}