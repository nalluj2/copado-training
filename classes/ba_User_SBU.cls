//------------------------------------------------------------------------------------------------
// Verify that the BU Related data on User matches the UserSBU data.  
// If not Update the fields on User.
//------------------------------------------------------------------------------------------------
global class ba_User_SBU implements Database.Batchable<sObject>, Database.Stateful {

	global String tSOQL = 'SELECT ID, Name, Company_Code_Text__c, User_Business_Unit_vs__c, User_Sub_Bus__c, Primary_sBU__c, Business_Unit_Name__c, ProfileId, License_Name__c FROM User WHERE License_Name__c in (\'Salesforce\') AND Company_Code_Text__c in (\'EUR\')';

	global Map<String, List<Business_Unit__c>> mapCompanyCode_BusinessUnit;
	global List<String> lstUserID_OK;
	global List<String> lstUserID_Error;
	global List<String> lstUserID_NoUserSBU;
	global List<String> lstUserID_InvalidCompanyCode;

	global String tErrorLog;

	// BATCH Settings
	global Database.QueryLocator start(Database.BatchableContext BC) {

		lstUserID_OK = new List<String>();
		lstUserID_NoUserSBU = new List<String>();
		lstUserID_Error = new List<String>();
		lstUserID_InvalidCompanyCode = new List<String>();

		List<Business_Unit__c> lstBU = [SELECT Id, Name, Company__r.Company_Code_Text__c FROM Business_Unit__c ORDER BY Company__r.Company_Code_Text__c, Name];

		mapCompanyCode_BusinessUnit = new Map<String, List<Business_Unit__c>>();
		for (Business_Unit__c oBU : lstBU){

			List<Business_Unit__c> lstBU_Tmp = new List<Business_Unit__c>();
			if (mapCompanyCode_BusinessUnit.containsKey(oBU.Company__r.Company_Code_Text__c)){
				lstBU_Tmp = mapCompanyCode_BusinessUnit.get(oBU.Company__r.Company_Code_Text__c);
			}
			lstBU_Tmp.add(oBU);
			mapCompanyCode_BusinessUnit.put(oBU.Company__r.Company_Code_Text__c, lstBU_Tmp);
		}

		return Database.getQueryLocator(tSOQL);
	}

   	global void execute(Database.BatchableContext BC, List<User> lstUser) {

   		List<User> lstUser_Update = new List<User>();

   		Map<Id, List<User_Business_Unit__c>> mapUserID_UserBU = new Map<Id, List<User_Business_Unit__c>>();

   		List<User_Business_Unit__c> lstUserBU_All = 
   			[
				SELECT ID, User__c, Business_Unit_Id__c, Business_Unit_text__c, Sub_Business_Unit__c, Sub_Business_Unit__r.Name, Primary__c
				FROM User_Business_Unit__c
				WHERE User__c = :lstUser
				ORDER BY User__c, Business_Unit_text__c, Sub_Business_Unit__r.Name	   			
			];

		for (User_Business_Unit__c oUserBU : lstUserBU_All){

			List<User_Business_Unit__c> lstUserBU_Tmp = new List<User_Business_Unit__c>();
			if (mapUserID_UserBU.containsKey(oUserBU.User__c)){
				lstUserBU_Tmp = mapUserID_UserBU.get(oUserBU.User__c);
			}
			lstUserBU_Tmp.add(oUserBU);
			mapUserID_UserBU.put(oUserBU.User__c, lstUserBU_Tmp);
		}


   		for (User oUser : lstUser){

   			System.debug('**BC** oUser ' + oUser.Name + ' : ' + oUser.Id);
   			System.debug('**BC** oUser.Company_Code_Text__c : ' + oUser.Company_Code_Text__c);
   			System.debug('**BC** mapCompanyCode_BusinessUnit.containsKey(oUser.Company_Code_Text__c) : ' + mapCompanyCode_BusinessUnit.containsKey(oUser.Company_Code_Text__c));
   			System.debug('**BC** mapCompanyCode_BusinessUnit : ' + mapCompanyCode_BusinessUnit);

   			List<Business_Unit__c> lstBU = mapCompanyCode_BusinessUnit.get(oUser.Company_Code_Text__c);
   			if (!mapCompanyCode_BusinessUnit.containsKey(oUser.Company_Code_Text__c)){
				String tError = oUser.Name + ' (' + oUser.Id + ') - INVALID COMPANY CODE ' + oUser.Company_Code_Text__c;
				lstUserID_InvalidCompanyCode.add(tError);
   				continue; // User has no User SBU
   			}
   			if (!mapUserID_UserBU.containsKey(oUser.Id)){
				String tError = oUser.Name + ' (' + oUser.Id + ')';
				lstUserID_NoUserSBU.add(tError);
   				continue; // User has no User SBU
   			}
			List<User_Business_Unit__c> lstUserBU = mapUserID_UserBU.get(oUser.Id);

			Boolean bSelectedALL = false;
			if (lstBU.size() == lstUserBU.size()) bSelectedALL = true;

			System.debug('**BC** lstUserBU (' + lstUserBU.size() + ') : ' + lstUserBU);

			String tUserBusinessUnit = '';
			String tUserSubBusinessUnit = '';
			String tUserPrimarySBU = '';
			String tUserBusinessUnitName = '';

			for (User_Business_Unit__c oUserBU : lstUserBU){

				System.debug('**BC** oUserBU : ' + oUserBU);
				if (oUserBU.Primary__c == true){
					tUserBusinessUnit = oUserBU.Business_Unit_text__c;
					tUserPrimarySBU = oUserBU.Sub_Business_Unit__r.Name;
				}
				if (!String.isBlank(tUserSubBusinessUnit)) tUserSubBusinessUnit += ',';
				tUserSubBusinessUnit += oUserBU.Sub_Business_Unit__r.Name;

				if (!tUserBusinessUnitName.contains(oUserBU.Business_Unit_text__c)){
					if (!String.isBlank(tUserBusinessUnitName)) tUserBusinessUnitName += ',';
					tUserBusinessUnitName += oUserBU.Business_Unit_text__c;
				}

			}

			System.debug('**BC** oUser.User_Business_Unit_vs__c -- tUserBusinessUnit : ' + oUser.User_Business_Unit_vs__c + ' -- ' + tUserBusinessUnit);
			System.debug('**BC** oUser.User_Sub_Bus__c -- tUserSubBusinessUnit : ' + oUser.User_Sub_Bus__c + ' -- ' + tUserSubBusinessUnit);
			System.debug('**BC** oUser.Primary_sBU__c -- tUserPrimarySBU : ' + oUser.Primary_sBU__c + ' -- ' + tUserPrimarySBU);

			Boolean bUserUpdated = false;
			if (oUser.User_Business_Unit_vs__c != tUserBusinessUnit){
				System.debug('**BC** DIFF 1');
				System.debug('**BC** oUser.User_Business_Unit_vs__c : ' + oUser.User_Business_Unit_vs__c);
				System.debug('**BC** tUserBusinessUnit : ' + tUserBusinessUnit);
				oUser.User_Business_Unit_vs__c = tUserBusinessUnit;
				bUserUpdated = true;
			}
			if (oUser.User_Sub_Bus__c != tUserSubBusinessUnit){
				System.debug('**BC** DIFF 2');
				System.debug('**BC** oUser.User_Sub_Bus__c : ' + oUser.User_Sub_Bus__c);
				System.debug('**BC** tUserBusinessUnit : ' + tUserSubBusinessUnit);
				oUser.User_Sub_Bus__c = tUserSubBusinessUnit;
				bUserUpdated = true;
			}
			if (oUser.Primary_sBU__c != tUserPrimarySBU){
				System.debug('**BC** DIFF 3');
				System.debug('**BC** oUser.Primary_sBU__c : ' + oUser.Primary_sBU__c);
				System.debug('**BC** tUserBusinessUnit : ' + tUserPrimarySBU);
				oUser.Primary_sBU__c = tUserPrimarySBU;
				bUserUpdated = true;
			}
			if (oUser.Business_Unit_Name__c != tUserBusinessUnitName){
				System.debug('**BC** DIFF 4');
				System.debug('**BC** oUser.Business_Unit_Name__c : ' + oUser.Business_Unit_Name__c);
				System.debug('**BC** tUserBusinessUnitName : ' + tUserBusinessUnitName);
				oUser.Business_Unit_Name__c = tUserBusinessUnitName;
				bUserUpdated = true;
			}

			if (bUserUpdated){
				lstUser_Update.add(oUser);
			}

		}

		System.debug('**BC** lstUser_Update (' + lstUser_Update.size() + ') : ' + lstUser_Update);

		if (lstUser_Update.size() > 0){

			List<Database.SaveResult> lstSaveResult_Update = Database.update(lstUser_Update, false);

			Integer iCounter = 0;
			for (Database.SaveResult oSaveResult : lstSaveResult_Update){
				
				User oUser = lstUser_Update[iCounter];
				
				if (!oSaveResult.isSuccess()){
					
					String tError = oUser.Name + ' (' + oUser.Id + ') : ';
					for(Database.Error oError : oSaveResult.getErrors()){
						tError += oError.getMessage()+'; ';
					}

					lstUserID_Error.add(tError);
				}else{
					String tError = oUser.Name + ' (' + oUser.Id + ')';
					lstUserID_OK.add(tError);
				}
				iCounter++;
			}
//			update lstUser_Update;
		}

	}
	
	global void finish(Database.BatchableContext BC) {

		String tResult = '<BR />';
		tResult += '<HR />';
		tResult += ' ERRORS ';
		tResult += '<HR />';
		tResult += '<BR />';
		tResult += '<BR />';
		tResult += String.join(lstUserID_Error, '<BR />');
		tResult += '<BR />';
		tResult += '<HR />';
		tResult += ' NO USER BUSINESS UNITES DEFINED ';
		tResult += '<HR />';
		tResult += '<BR />';
		tResult += '<BR />';
		tResult += String.join(lstUserID_NoUserSBU, '<BR />');
		tResult += '<BR />';
		tResult += '<HR />';
		tResult += ' INVALID COMPANY CODE ';
		tResult += '<HR />';
		tResult += '<BR />';
		tResult += '<BR />';
		tResult += String.join(lstUserID_InvalidCompanyCode, '<BR />');
		tResult += '<BR />';
		tResult += '<HR />';
		tResult += ' USER UPDATES OK ';
		tResult += '<HR />';
		tResult += '<BR />';
		tResult += '<BR />';
		tResult += String.join(lstUserID_OK, '<BR />');


		String tEmailBody = tResult;
		Messaging.Singleemailmessage oEmail = new Messaging.Singleemailmessage();
			oEmail.setHTMLBody(tEmailBody);
			oEmail.setTargetObjectId(UserInfo.getUserId());
			oEmail.setSubject('ba_User_SBU');
			oEmail.setSaveAsActivity(false);
		Messaging.sendEmail(new Messaging.Singleemailmessage[] { oEmail});

	}
	
}