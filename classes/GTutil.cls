Global class GTutil{
    public static boolean chkInsertImplantedProduct = true;
    public static boolean chkUpdateImplantedProduct = true;
    public static boolean chkInsertImplantAfterAll = true;
    public static boolean chkUpdateImplantAfterAll = true;
    public static boolean chkInsertSubjectDiscusedAfterAll = true; 
    public static boolean chkUpdateSubjectDiscusedAfterAll = true; 
    public static boolean glbOppUpdate=true;//added by manish
    public static boolean chkUpdUser=false;//used in trigger on user.
    public static boolean chkPcodeRealignment = true;
    public static boolean boolAcountCallTopic = true;
    public static boolean boolContactCallTopic = true;    
    public static boolean boolCallRecord = true;
    public static boolean boolOppProSchedule = true;
    public static boolean boolCaseQAFlag = true;
    public static boolean boolCaseOMASearchKey = true;
    public static boolean netPromotorScore = true;
	public static boolean relationshipDetailBefore = true;
    public static boolean ContactVisitReportAfterAll = true;
    public static boolean ImplantBeforeInsertUpdate = true;
	public static boolean trgConcatImplainDetail = true;
    
}