public class bl_SVMXC_Product_Stock_Trigger {
    
    public static void populateProductInformation(List<SVMXC__Product_Stock__c> productStocks){
    	
    	Set<Id> productIds = new Set<Id>();
    	
    	for(SVMXC__Product_Stock__c prodStock : productStocks){
    		
    		if(prodStock.SVMXC__Product__c != null) productIds.add(prodStock.SVMXC__Product__c);
    	}   	
    	
    	Map<Id, Product2> products = new Map<Id, Product2>([Select Id, SVMX_Returnable_Part__c, ProductCode from Product2 where Id IN :productIds]);
    	
    	for(SVMXC__Product_Stock__c prodStock : productStocks){
    		
    		Product2 product = products.get(prodStock.SVMXC__Product__c);
    		
    		if(product != null){
    			
    			prodStock.SVMX_Returnable_Part__c = product.SVMX_Returnable_Part__c;
    			prodStock.SVMX_Part_Number__c = product.ProductCode;
    			
    		}else{
    			
    			prodStock.SVMX_Returnable_Part__c = false;
    			prodStock.SVMX_Part_Number__c = null;
    		}
    	} 
    }
}