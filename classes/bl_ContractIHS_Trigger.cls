//--------------------------------------------------------------------------------------------------------------------------------
//  Author      :   Bart Caelen
//  Date        :   26/09/2017
//  Description :   APEX Class for the APEX Trigger tr_ContractIHS
//  Change Log  :    
//--------------------------------------------------------------------------------------------------------------------------------
public with sharing class bl_ContractIHS_Trigger {

	//----------------------------------------------------------------------------------------------------------------------------
	// Clone related records of the cloned Contract_IHS__c record.
	//	1.0 - clone related Contract_Equipment_Detail_IHS__c records
	//----------------------------------------------------------------------------------------------------------------------------
	public static void cloneRelatedData(List<Contract_IHS__c> lstTriggerNew){

		Set<Id> setID_CloneFrom = new Set<Id>();

		for (Contract_IHS__c oContractIHS : lstTriggerNew){

			if (oContractIHS.Cloned_From__c != null) setID_CloneFrom.add(oContractIHS.Cloned_From__c);

		}

		if (setID_CloneFrom.size() > 0){

			Map<Id, Contract_IHS__c> mapCloneFromContractIHS = new Map<Id, Contract_IHS__c>(
				[
					SELECT 
						Id 
						, (
							SELECT Contract_Recordtype__c, Equipment_IHS__c, Equipment_Model__c, Equipment_SerialNumber__c, Equipment_Value__c
							FROM Contract_Equipment_Details_IHS__r
						) 
					FROM 
						Contract_IHS__c 
					WHERE 
						Id IN :setID_CloneFrom
				]
			);

			List<Contract_Equipment_Detail_IHS__c> lstContractEquipmentDetailIHS_Insert = new List<Contract_Equipment_Detail_IHS__c>();
			for (Contract_IHS__c oContractIHS : lstTriggerNew){

				if (oContractIHS.Cloned_From__c != null){
					
					Contract_IHS__c oCloneFrom = mapCloneFromContractIHS.get(oContractIHS.Cloned_From__c);
					
					if ( (oCloneFrom.Contract_Equipment_Details_IHS__r != null) && (oCloneFrom.Contract_Equipment_Details_IHS__r.size() > 0) ){
						
						for (Contract_Equipment_Detail_IHS__c oContractEquipmentDetailIHS : oCloneFrom.Contract_Equipment_Details_IHS__r){
							
							Contract_Equipment_Detail_IHS__c oContractEquipmentDetailIHS_Cloned = oContractEquipmentDetailIHS.clone(false);
							oContractEquipmentDetailIHS_Cloned.Contract_IHS__c = oContractIHS.Id;
							
							lstContractEquipmentDetailIHS_Insert.add(oContractEquipmentDetailIHS_Cloned);

						}
					}
				}			

			}


			if (lstContractEquipmentDetailIHS_Insert.size() > 0){

				insert lstContractEquipmentDetailIHS_Insert;

			}

		}

	}
	//----------------------------------------------------------------------------------------------------------------------------

}
//--------------------------------------------------------------------------------------------------------------------------------