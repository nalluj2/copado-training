public without sharing class ctrl_SMAX_Interface_Report {
    
    transient public List<FailedRecord> reportItems {get; private set;}
    public String headers {get; set;}
    public String selectedObjectType {get; set;}
    
    public ctrl_SMAX_Interface_Report(){
    	
    	reportItems = new List<FailedRecord>();
    	
    	Map<String, String> inputParams = ApexPages.currentPage().getParameters();
    	
    	selectedObjectType = inputParams.get('objectType');
    	
    	if(selectedObjectType != null){
    	    	    	
	    	String query = ' ';
    	
	    	query += '  '; 
	    										
			Map<Id, Notification_Grouping__c> notificationMap = new Map<Id, Notification_Grouping__c>();
			
			for(Notification_Grouping__c grouping : [Select Id, Internal_ID__c, 
														Last_Outbound_Notification__c, Last_Outbound_Notification__r.Status__c, Last_Outbound_Notification__r.WM_Process__c, Last_Outbound_Notification__r.Retries__c, Last_Outbound_Notification__r.Outbound_Message_Request__c, Last_Outbound_Notification__r.Error_Description__c, Last_Outbound_Notification__r.CreatedDate, Last_Outbound_Notification__r.LastModifiedDate 
														from Notification_Grouping__c where Scope__c = :selectedObjectType AND Outbound_Status__c = 'Failure' AND Archived__c = false]){
				
				notificationMap.put(grouping.Internal_ID__c, grouping);
			}
			
			if(selectedObjectType == 'SVMXC__Service_Order__c'){
				
				headers = '"Name","SAP Id","SFDC Id","Status","Last Modified Date","Country","FSE Name","Last Notification Process","Last Notification Status","Last Notification Created Date","Last Notification Last Modified Date","Last Notification Retries","Last Notification Error"';
							
				for(SVMXC__Service_Order__c sOrder : [Select Id, Name, Account_Country__c, SVMXC__Group_Member__r.Name, SVMX_SAP_Service_Order_No__c, SVMXC__Order_Status__c, CreatedDate, LastModifiedDate from SVMXC__Service_Order__c where Id IN :notificationMap.keySet() ORDER BY CreatedDate]){
				    									    
				    NotificationSAPLog__c lastNotification = notificationMap.get(sOrder.Id).Last_Outbound_Notification__r;
				    				    				        
			        FailedRecord reportItem = new FailedRecord();
			        reportItem.SAP_Id = sOrder.SVMX_SAP_Service_Order_No__c;
			        reportItem.SFDC_Id = sOrder.Id;
			        reportItem.Name = sOrder.Name;
			        reportItem.Status = sOrder.SVMXC__Order_Status__c;
			        reportItem.lastNotProcess = lastNotification.WM_Process__c;
			        reportItem.lastNotStatus = lastNotification.Status__c;
			        if(lastNotification.Retries__c != null) reportItem.lastNotRetries = lastNotification.Retries__c.setScale(0).format();
			        else reportItem.lastNotRetries = '0';
			        reportItem.modified = sOrder.LastModifiedDate.format();
			        reportItem.FSEName = sOrder.SVMXC__Group_Member__r.Name;
			        reportItem.lastNotCreated = lastNotification.CreatedDate.format();
			        reportItem.lastNotModified = lastNotification.LastModifiedDate.format();
			        reportItem.Country = sOrder.Account_Country__c;
			        
			        if(lastNotification.Outbound_Message_Request__c != null) reportItem.lastNotError = extractErrorMessage(lastNotification.Outbound_Message_Request__c);
			        else reportItem.lastNotError = lastNotification.Error_Description__c;
			        
			        reportItems.add(reportItem);			        
				} 
				       	
			}else if(selectedObjectType == 'Case'){
				
				headers = '"Name","SAP Id","SFDC Id","Status","Last Modified Date","Country","FSE Name","Last Notification Process","Last Notification Status","Last Notification Created Date","Last Notification Last Modified Date","Last Notification Retries","Last Notification Error"';
				
				for(Case sNotification : [Select Id, CaseNumber, Account_Country__c, SVMX_SAP_Notification_Number__c, Status, CreatedDate, LastModifiedDate, (Select Id, SVMXC__Group_Member__r.Name from SVMXC__Service_Order__r) from Case where Id IN :notificationMap.keySet() ORDER BY CreatedDate]){
	    				    
				    NotificationSAPLog__c lastNotification = notificationMap.get(sNotification.Id).Last_Outbound_Notification__r;
				        
			        FailedRecord reportItem = new FailedRecord();
			        reportItem.SAP_Id = sNotification.SVMX_SAP_Notification_Number__c;
			        reportItem.SFDC_Id = sNotification.Id;
			        reportItem.Name = sNotification.CaseNumber;
			        reportItem.Status = sNotification.Status;
			        reportItem.modified = sNotification.LastModifiedDate.format();
			        reportItem.lastNotProcess = lastNotification.WM_Process__c;
			        reportItem.lastNotStatus = lastNotification.Status__c;
			        if(sNotification.SVMXC__Service_Order__r != null && sNotification.SVMXC__Service_Order__r.size() > 0) reportItem.FSEName = sNotification.SVMXC__Service_Order__r[0].SVMXC__Group_Member__r.Name;				        
			        if(lastNotification.Retries__c != null) reportItem.lastNotRetries = lastNotification.Retries__c.setScale(0).format();
			        else reportItem.lastNotRetries = '0';
			        reportItem.lastNotCreated = lastNotification.CreatedDate.format();
			        reportItem.lastNotModified = lastNotification.LastModifiedDate.format();
			        reportItem.Country = sNotification.Account_Country__c;
			        
			        if(lastNotification.Outbound_Message_Request__c != null) reportItem.lastNotError = extractErrorMessage(lastNotification.Outbound_Message_Request__c);
			        else reportItem.lastNotError = lastNotification.Error_Description__c;
			        
			        reportItems.add(reportItem);				          
				}   
				
			}else if(selectedObjectType == 'Attachment'){
				
				headers = '"Name","SAP Id","SFDC Id","Service Order/Notification Id","Status","Last Modified Date","Country","FSE Name","Last Notification Process","Last Notification Status","Last Notification Created Date","Last Notification Last Modified Date","Last Notification Retries","Last Notification Error"';
				
				List<Attachment> attachments = [Select Id, ParentId, Name, CreatedDate, LastModifiedDate, CreatedBy.Name from Attachment where Id IN :notificationMap.keySet() ORDER BY CreatedDate];
				
				Set<Id> caseIds = new Set<Id>();
				Set<Id> woIds = new Set<Id>();
				
				for(Attachment attach : attachments){
											    	
			    	if(string.valueof(attach.ParentId.getSObjectType()) == 'Case') caseIds.add(attach.ParentId);			    	
			    	else if(string.valueof(attach.ParentId.getSObjectType()) == 'SVMXC__Service_Order__c') woIds.add(attach.ParentId);			    		
				}
				
				Map<Id, String> parentServiceId = new Map<Id, String>();
				
				if(caseIds.size() > 0){
					
					for(Case serviceNotification : [Select Id, SVMX_SAP_Notification_Number__c from Case where Id = :caseIds]){
						
						parentServiceId.put(serviceNotification.Id, serviceNotification.SVMX_SAP_Notification_Number__c);
					}				
				}
				
				if(woIds.size() > 0){
					
					for(SVMXC__Service_Order__c serviceOrder : [Select Id, SVMX_SAP_Service_Order_No__c from SVMXC__Service_Order__c where Id = :woIds]){
						
						parentServiceId.put(serviceOrder.Id, serviceOrder.SVMX_SAP_Service_Order_No__c);
					}				
				}
								
				for(Attachment attach : attachments){
	    
				    NotificationSAPLog__c lastNotification = notificationMap.get(attach.Id).Last_Outbound_Notification__r;
				        
			        FailedRecord reportItem = new FailedRecord();
			        reportItem.SAP_Id = '-';
			        reportItem.SFDC_Id = attach.Id;
			        reportItem.Name = attach.Name;
			        reportItem.Status = '-';
			        reportItem.FSEName = attach.CreatedBy.Name;
			        reportItem.modified = attach.LastModifiedDate.format();
			        reportItem.lastNotProcess = lastNotification.WM_Process__c;
			        reportItem.lastNotStatus = lastNotification.Status__c;
			        if(lastNotification.Retries__c != null) reportItem.lastNotRetries = lastNotification.Retries__c.setScale(0).format();
			        else reportItem.lastNotRetries = '0';
			        reportItem.lastNotCreated = lastNotification.CreatedDate.format();
			        reportItem.lastNotModified = lastNotification.LastModifiedDate.format();
			        reportItem.parentId = parentServiceId.get(attach.ParentId);
			        
			        if(lastNotification.Outbound_Message_Request__c != null) reportItem.lastNotError = extractErrorMessage(lastNotification.Outbound_Message_Request__c);
			        else reportItem.lastNotError = lastNotification.Error_Description__c;
			        
			        reportItems.add(reportItem);		       
				} 
				
			}else if(selectedObjectType == 'SVMXC__Installed_Product__c'){
				
				headers = '"Name","SAP Id","SFDC Id","Status","Last Modified Date","Country","FSE Name","Last Notification Process","Last Notification Status","Last Notification Created Date","Last Notification Last Modified Date","Last Notification Retries","Last Notification Error"';
				
				for(SVMXC__Installed_Product__c instProd : [Select Id, Name, Account_Country__c, SVMX_SAP_Equipment_ID__c, CreatedDate, LastModifiedDate from SVMXC__Installed_Product__c where Id IN :notificationMap.keySet() ORDER BY CreatedDate]){
	    
				    NotificationSAPLog__c lastNotification = notificationMap.get(instProd.Id).Last_Outbound_Notification__r;
				      
			        FailedRecord reportItem = new FailedRecord();
			        reportItem.SAP_Id = instProd.SVMX_SAP_Equipment_ID__c;
			        reportItem.SFDC_Id = instProd.Id;
			        reportItem.Name = instProd.Name;
			        reportItem.Status = '-';
			        reportItem.modified = instProd.LastModifiedDate.format();
			        reportItem.lastNotProcess = lastNotification.WM_Process__c;
			        reportItem.lastNotStatus = lastNotification.Status__c;
			        if(lastNotification.Retries__c != null) reportItem.lastNotRetries = lastNotification.Retries__c.setScale(0).format();
			        else reportItem.lastNotRetries = '0';
			        reportItem.lastNotCreated = lastNotification.CreatedDate.format();
			        reportItem.lastNotModified = lastNotification.LastModifiedDate.format();
			        reportItem.Country = instProd.Account_Country__c;
			        
			        if(lastNotification.Outbound_Message_Request__c != null) reportItem.lastNotError = extractErrorMessage(lastNotification.Outbound_Message_Request__c);
			        else reportItem.lastNotError = lastNotification.Error_Description__c;
			        
			        reportItems.add(reportItem);				        
				} 
				
			}else if(selectedObjectType == 'Installed_Product_Measures__c'){
				
				headers = '"Name","SAP Id","SFDC Id","Installed Product Id","Equipment Number Id","Status","Last Modified Date","Country","FSE Name","Last Notification Process","Last Notification Status","Last Notification Created Date","Last Notification Last Modified Date","Last Notification Retries","Last Notification Error"';
				
				for(Installed_Product_Measures__c prodMeasure : [Select Id, Name, Read_By__c, Installed_Product__r.Name, Installed_Product__r.SVMX_SAP_Equipment_ID__c, Installed_Product__r.Account_Country__c, SAP_Measure_Point_ID__c, CreatedDate, LastModifiedDate from Installed_Product_Measures__c where Id IN :notificationMap.keySet() ORDER BY CreatedDate]){
	    
				    NotificationSAPLog__c lastNotification = notificationMap.get(prodMeasure.Id).Last_Outbound_Notification__r;
				       
			        FailedRecord reportItem = new FailedRecord();
			        reportItem.SAP_Id = prodMeasure.SAP_Measure_Point_ID__c;
			        reportItem.SFDC_Id = prodMeasure.Id;
			        reportItem.Name = prodMeasure.Name;
			        reportItem.FSEName = prodMeasure.Read_By__c;
			        reportItem.Status = '-';
			        reportItem.modified = prodMeasure.LastModifiedDate.format();
			        reportItem.lastNotProcess = lastNotification.WM_Process__c;
			        reportItem.lastNotStatus = lastNotification.Status__c;
			        if(lastNotification.Retries__c != null) reportItem.lastNotRetries = lastNotification.Retries__c.setScale(0).format();
			        else reportItem.lastNotRetries = '0';
			        reportItem.lastNotCreated = lastNotification.CreatedDate.format();
			        reportItem.lastNotModified = lastNotification.LastModifiedDate.format();
			        reportItem.Country = prodMeasure.Installed_Product__r.Account_Country__c;
			        reportItem.ParentId = prodMeasure.Installed_Product__r.Name;
			        reportItem.SecondaryParentId = prodMeasure.Installed_Product__r.SVMX_SAP_Equipment_ID__c;
			        
			        if(lastNotification.Outbound_Message_Request__c != null) reportItem.lastNotError = extractErrorMessage(lastNotification.Outbound_Message_Request__c);
			        else reportItem.lastNotError = lastNotification.Error_Description__c;
			        
			        reportItems.add(reportItem);				        
				}				
			}		
    	}
    }
    
    private String extractErrorMessage(String response){
    	
    	String errorMessage = '';
    	
    	if(response != null){
    		
    		try{
    			
    			AckMessage message = (AckMessage) JSON.deserialize(response, AckMessage.class);
    			
    			if(message.DISTRIBUTION_STATUS == 'FALSE'){
    				
    				Set<String> errors = new Set<String>();
    				
    				for(SAPErrorMessage error : message.DIST_ERROR_DETAILS){
    					
    					if((error.ERROR_TYPE == 'E' || error.ERROR_TYPE == null ) && error.ERROR_MESSAGE != 'Error during processing of BAPI methods' && error.ERROR_MESSAGE != 'Error  during processing of BAPI methods'){
    						
    						errors.add(error.ERROR_MESSAGE);
    					}
    				}
    				
    				errorMessage = String.join(new List<String>(errors), ';');
    			}
    		
    		}catch(Exception e){
    			
    			errorMessage = 'Exception occur while getting the error message';
    		}
    	}
    	
    	return errorMessage;
    }
    
    public class FailedRecord{
    
    	public String Name  {get; set;}
    	public String SAP_Id  {get; set;}
    	public String SFDC_Id  {get; set;}    	
    	public String Status  {get; set;}
    	public String Modified  {get; set;}
    	public String FSEName  {get; set;}
    	public String lastNotProcess  {get; set;}
    	public String lastNotStatus  {get; set;}
    	public String lastNotRetries  {get; set;}    	
    	public String lastNotError  {get; set;}    
    	public String lastNotCreated  {get; set;}    
    	public String lastNotModified  {get; set;}    	 
    	public String country {get; set;}
    	public String parentId {get; set;}
    	public String secondaryParentId {get; set;}  	   
    }
    
    public class AckMessage {
                   
        public String DISTRIBUTION_STATUS {get; set;}
        public List<SAPErrorMessage> DIST_ERROR_DETAILS {get; set;}        
    }
    
    public class SAPErrorMessage{
    	
    	public String ERROR_TYPE {get; set;}
		public String ERROR_MESSAGE {get; set;}
    }
}