@isTest
private class Test_Release_Deployment_Component {
	
	@testSetup
	private static void setCustomSettings(){
		
		User currentUser = new User(Id = UserInfo.getUserId());
		
		System.runAs(currentUser){
				
			RDC_Settings__c apexClass = new RDC_Settings__c(name='ApexClass', Object_API_Required__c=false, Field_API_Required__c=true, Validate_Item_API_Name__c=true, Uses_Folder__c=false, Allow_Manual__c=false);
			RDC_Settings__c customField = new RDC_Settings__c(name='CustomField', Object_API_Required__c=true, Field_API_Required__c=true, Validate_Item_API_Name__c=true, Uses_Folder__c=false, Allow_Manual__c=true);
			RDC_Settings__c customObject = new RDC_Settings__c(name='CustomObject', Object_API_Required__c=true, Field_API_Required__c=false, Validate_Item_API_Name__c=false, Uses_Folder__c=false, Allow_Manual__c=true);			
			RDC_Settings__c emailTemplate = new RDC_Settings__c(name='EmailTemplate', Object_API_Required__c=false, Field_API_Required__c=true, Validate_Item_API_Name__c=true, Uses_Folder__c=true, Allow_Manual__c=true);
			RDC_Settings__c workflowRule = new RDC_Settings__c(name='WorkflowRule', Object_API_Required__c=true, Field_API_Required__c=true, Validate_Item_API_Name__c=false, Uses_Folder__c=false, Allow_Manual__c=true);
							
			insert new List<RDC_Settings__c>{apexClass, customField, customObject, emailTemplate, workflowRule};
		}	
		
		Change_Request__c cr = new Change_Request__c();        
        cr.Request_Date__c = Date.today();
        cr.Change_Request__c = 'test';
        cr.Requestor_Lookup__c = Userinfo.getUserId();
        insert(cr);
                
        Work_Item__c wi = new Work_Item__c();
        wi.Change_Request__c = cr.Id;        
        wi.Due_Date__c = Date.today();        
        insert(wi);	
	}	
	
	private static Work_Item__c workItem {
		
		get{
		
			if(workItem == null) workItem = [Select Id from Work_Item__c];
			
			return workItem;
		}
		
		set;
	}
	
	private static testmethod void testApexClass(){
			
		Release_Deployment_Component__c rdc = new Release_Deployment_Component__c();
		rdc.Work_Item__c = workItem.Id;		
        rdc.Type__c = 'Development Change';
        rdc.Meta_data_type__c = 'ApexClass'; 
        rdc.API_Field_name__c = 'Any Class';
        rdc.Item_Name__c = 'Any Class';
        rdc.Action_type__c = 'Created';
        rdc.Deletion_Order__c = null;
        rdc.Deployment_Type_to_Staging__c = 'Manual';               
        rdc.Responsible_for_Deployment__c = UserInfo.getUserId();
        
        Boolean failed = false;
        
        try{
        	
        	insert rdc;
        	
        }catch(Exception e){
        	
        	System.assert(e.getMessage().contains('ApexClass cannot be defined as Manual deployment type'));
        	failed = true;
        }
        
        System.assert(failed == true);
        failed = false;
        
        rdc.Deployment_Type_to_Staging__c = 'Eclipse';  
        
        try{
        	        	     
        	insert rdc;
        
        }catch(Exception e){
        	
        	System.assert(e.getMessage().contains('Incorrect Item API Name format'));
        	failed = true;
        } 
		
		System.assert(failed == true);
		
        rdc.API_Field_name__c = 'Any_Class';     
        insert rdc; 
		
		rdc = [Select Id, Responsible_for_Deployment__c from Release_Deployment_Component__c where Id = :rdc.Id];
		System.assert(rdc.Responsible_for_Deployment__c == null);
	}
	
	private static testmethod void testCustomField(){
			
		Release_Deployment_Component__c rdc = new Release_Deployment_Component__c();
		rdc.Work_Item__c = workItem.Id;		
        rdc.Type__c = 'Data Model Change';
        rdc.Meta_data_type__c = 'CustomField'; 
        rdc.API_Field_name__c = 'FieldAPIName';
        rdc.Item_Name__c = 'Field Label';        
        rdc.Action_type__c = 'Created';
        rdc.Deletion_Order__c = null;
        rdc.Deployment_Type_to_Staging__c = 'Eclipse';
        
        Boolean failed = false;
        
        try{
        	
        	insert rdc;
        	
        }catch(Exception e){
        	
        	System.assert(e.getMessage().contains('requires the Object API Name to be specified'));
        	failed = true;
        }
        
        System.assert(failed == true);
        failed = false;
        
        try{
        	
        	rdc.Object__c = 'Account';        
        	insert rdc;
        
        }catch(Exception e){
        	
        	System.assert(e.getMessage().contains('Standard Fields must be set as Manual deployment type'));
        	failed = true;
        } 
		
		System.assert(failed == true);
                
        rdc.API_Field_name__c = 'FieldAPIName__c';
        insert rdc;
	}
	
	private static testmethod void testCustomObject(){
			
		Release_Deployment_Component__c rdc = new Release_Deployment_Component__c();
		rdc.Work_Item__c = workItem.Id;		
        rdc.Type__c = 'Data Model Change';
        rdc.Meta_data_type__c = 'CustomObject'; 
        rdc.Object__c = 'Opportunity Product';        
        rdc.Action_type__c = 'Updated';
        rdc.Deletion_Order__c = null;
        rdc.Deployment_Type_to_Staging__c = 'Eclipse';
        
        Boolean failed = false;
        
        try{
        	
        	insert rdc;
        	
        }catch(Exception e){
        	
        	System.assert(e.getMessage().contains('Incorrect Object API Name format'));
        	failed = true;
        }
        
        System.assert(failed == true);
        
        rdc.Object__c = 'OpportunityProduct';
        insert rdc;
	}
	
	private static testmethod void testEmailTemplate(){
			
		Release_Deployment_Component__c rdc = new Release_Deployment_Component__c();
		rdc.Work_Item__c = workItem.Id;		
        rdc.Type__c = 'Configuration Change';
        rdc.Meta_data_type__c = 'EmailTemplate'; 
        rdc.Object__c = 'Account';
        rdc.API_Field_name__c = 'EmailTemplate';                
        rdc.Action_type__c = 'Created';
        rdc.Deletion_Order__c = null;
        rdc.Deployment_Type_to_Staging__c = 'Eclipse';
        
        Boolean failed = false;
        
        try{
        	
        	insert rdc;
        	
        }catch(Exception e){
        	
        	System.assert(e.getMessage().contains('requires the Item API Name and Label to be specified'));
        	failed = true;
        }
        
        System.assert(failed == true);
        failed = false;
        
        rdc.Item_Name__c = 'Email Template Label';
        
        try{
        	
        	insert rdc;
        	
        }catch(Exception e){
        	
        	System.assert(e.getMessage().contains('requires the API Name to be specified in the following format: folder_name/item_API_name'));
        	failed = true;
        }
        
        System.assert(failed == true);
        failed = false;
        
        rdc.API_Field_name__c = 'Folder/EmailTemplate'; 
        insert rdc;
	}
	
	private static testmethod void testWorkflowRule(){
			
		Release_Deployment_Component__c rdc = new Release_Deployment_Component__c();
		rdc.Work_Item__c = workItem.Id;		
        rdc.Type__c = 'Configuration Change';
        rdc.Meta_data_type__c = 'WorkflowRule'; 
        rdc.Object__c = 'Opportunity Product';
        rdc.API_Field_name__c = 'Workflow Rule Name';  
        rdc.Item_Name__c = 'Workflow Rule Label';              
        rdc.Action_type__c = 'Created';
        rdc.Deletion_Order__c = null;
        rdc.Deployment_Type_to_Staging__c = 'Eclipse';
        
        Boolean failed = false;
        
        try{
        	
        	insert rdc;
        	
        }catch(Exception e){
        	
        	System.assert(e.getMessage().contains('Incorrect Object API Name format'));
        	failed = true;
        }
        
        System.assert(failed == true);
        
        rdc.Object__c = 'OpportunityProduct';        
        insert rdc;
	}
	
	private static testmethod void testDataManipulation(){
			
		Release_Deployment_Component__c rdc = new Release_Deployment_Component__c();
		rdc.Work_Item__c = workItem.Id;		
        rdc.Type__c = 'Data Manipulation';                         
        rdc.Action_type__c = 'Updated';
        rdc.Deletion_Order__c = null;
        rdc.Deployment_Type_to_Staging__c = 'Eclipse';
        
        Boolean failed = false;
        
        try{
        	
        	insert rdc;
        	
        }catch(Exception e){
        	
        	System.assert(e.getMessage().contains('Data Manipulations must be set as Manual deployment type'));
        	failed = true;
        }
        
        System.assert(failed == true);
        failed = false;
        
        rdc.Deployment_Type_to_Staging__c = 'Manual';
        
        try{
        	
        	insert rdc;
        	
        }catch(Exception e){
        	
        	System.assert(e.getMessage().contains('Data Manipulations must specify the Object API Name.'));
        	failed = true;
        }
        
        System.assert(failed == true);
        failed = false;
        
        rdc.Object__c = 'OpportunityProduct';      
                
        try{
        	
        	insert rdc;
        	
        }catch(Exception e){
        	
        	System.assert(e.getMessage().contains('A Responsible for Deployment is required for Manual deployment types'));
        	failed = true;
        }
        
        System.assert(failed == true);
        failed = false;
        
        rdc.Responsible_for_Deployment__c = UserInfo.getUserId();  
        insert rdc;
	}
	
	private static testmethod void testProfile(){
			
		Release_Deployment_Component__c rdc = new Release_Deployment_Component__c();
		rdc.Work_Item__c = workItem.Id;		
        rdc.Type__c = 'Configuration Change';
        rdc.Meta_data_type__c = 'Profile';        
        rdc.API_Field_name__c = 'EUR Field Force CVG';  
        rdc.Item_Name__c = 'EUR Field Force CVG';              
        rdc.Action_type__c = 'Created';
        rdc.Deletion_Order__c = null;
        rdc.Deployment_Type_to_Staging__c = 'Eclipse';
        
        Boolean failed = false;
        
        try{
        	
        	insert rdc;
        	
        }catch(Exception e){
        	
        	System.assert(e.getMessage().contains('must be set as Manual deployment type'));
        	failed = true;
        }
        
        System.assert(failed == true);
        failed = false;
        
        rdc.Deployment_Type_to_Staging__c = 'Manual';
        rdc.Responsible_for_Deployment__c = UserInfo.getUserId();        	
        insert rdc;
	}
	
	private static testmethod void testDelete(){
			
		Release_Deployment_Component__c rdc = new Release_Deployment_Component__c();
		rdc.Work_Item__c = workItem.Id;		
        rdc.Type__c = 'Data Model Change';
        rdc.Meta_data_type__c = 'CustomField'; 
        rdc.Object__c = 'Account';
        rdc.API_Field_name__c = 'FieldAPIName__c';
        rdc.Item_Name__c = 'Field Label';
        rdc.Action_type__c = 'Deleted';
        rdc.Deletion_Order__c = null;
        rdc.Deployment_Type_to_Staging__c = 'Eclipse';
        
        Boolean failed = false;
        
        try{
        	
        	insert rdc;
        	
        }catch(Exception e){
        	
        	System.assert(e.getMessage().contains('When deleting through Eclipse deployment, please specify if it should be deleted before or after the deployment'));
        	failed = true;
        }
        
        System.assert(failed == true);
                
        rdc.Deletion_Order__c = 'After Deployment';                	
        insert rdc;
	}
	
	private static testmethod void testAPINameChange(){
			
		Release_Deployment_Component__c rdc = new Release_Deployment_Component__c();
		rdc.Work_Item__c = workItem.Id;		
        rdc.Type__c = 'Data Model Change';
        rdc.Meta_data_type__c = 'CustomField'; 
        rdc.Object__c = 'Account';
        rdc.API_Field_name__c = 'FieldAPIName__c';
        rdc.Item_Name__c = 'Field Label';
        rdc.Action_type__c = 'Updated';
        rdc.Deletion_Order__c = null;
        rdc.API_Field_name_Change__c = true;
        rdc.Deployment_Type_to_Staging__c = 'Eclipse';
        
        Boolean failed = false;
        
        try{
        	
        	insert rdc;
        	
        }catch(Exception e){
        	
        	System.assert(e.getMessage().contains('When the API Name has changed it must be set as Manual deployment type'));
        	failed = true;
        }
        
        System.assert(failed == true);
        failed = false;
        
        rdc.Deployment_Type_to_Staging__c = 'Manual';
        rdc.Responsible_for_Deployment__c = UserInfo.getUserId(); 
        
        try{
        	
        	insert rdc;
        	
        }catch(Exception e){
        	
        	System.assert(e.getMessage().contains('When the API Name has changed you must provide the Old Label and Old API Name values'));
        	failed = true;
        }
        
        System.assert(failed == true);
        failed = false;
        
        rdc.Old_API_Name__c = 'Old API Name';
        rdc.Old_Label__c = 'Old Label'; 
        
         try{
        	
        	insert rdc;
        	
        }catch(Exception e){
        	
        	System.assert(e.getMessage().contains('Incorrect Old API Name format'));
        	failed = true;
        }
        
        System.assert(failed == true);
                
        rdc.Old_API_Name__c = 'Old_API_Name__c';                	
        insert rdc;
	}
    
}