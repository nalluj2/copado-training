public class ctrl_UploadAttachment {

	private id idParent;

	//------------------------------------------------------------------
	// CONSTRUCTOR
	//------------------------------------------------------------------
	public ctrl_UploadAttachment() {

 		Map<String, String> mapURLParameters = ApexPages.currentPage().getParameters();

 		idParent = mapURLParameters.get('id');

 		bShowHeader = true;
 		bShowSidebar = true;

 		try{ bShowHeader = Boolean.valueOf(mapURLParameters.get('showheader')); }catch(Exception oEX){}
 		try{ bShowSidebar = Boolean.valueOf(mapURLParameters.get('showsidebar')); }catch(Exception oEX){}

 		tTabStyle = idParent.getSobjectType().getDescribe().getLabel();
 		tObjectLabel = tTabStyle;
		
	}
	//------------------------------------------------------------------


	//------------------------------------------------------------------
	// GETTERS & SETTERS
	//------------------------------------------------------------------
	public Boolean bShowHeader { get; private set; }
	public Boolean bShowSidebar { get; private set; }
	public String tTabStyle { get; private set; }
	public String tObjectLabel { get; private set;}
	//------------------------------------------------------------------


	//------------------------------------------------------------------
	// ACTIONS
	//------------------------------------------------------------------
	public PageReference backToParent(){

		PageReference oPageReference = new PageReference('/' + idParent);
			oPageReference.setRedirect(true);
		return oPageReference;

	}
	//------------------------------------------------------------------

}