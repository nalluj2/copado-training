/****************************************************************************************
 * Name    : SETCase_TriggerHandler 
 * Author  : Bill Shan
 * Date    : 14/12/2015 
 * Purpose : Contains all the logic coming from Account trigger
 * Dependencies: Create task automatically when a SET Case is create, so that the task reporting
 * 				will include the SET Case activities.
 *
 * ========================
 * = MODIFICATION HISTORY =
 * ========================
 * DATE        AUTHOR               CHANGE
 * ----        ------               ------
 *****************************************************************************************/
public class SETCase_TriggerHandler {
    
    private static Id SETTaskRTId{
    	
    	get{
    		
    		if(SETTaskRTId == null) SETTaskRTId = [Select Id from RecordType Where sObjectType = 'Task' and DeveloperName = 'SET_Activity' limit 1].Id;
    		
    		return SETTaskRTId;
    	}
    	set;
    }
        
    public static void createTask(List<Cost_Analysis_Case__c> triggerNew){
        
        if (bl_Trigger_Deactivation.isTriggerDeactivated('SETCase_createTask')) return;
                
        List<Task> setTasks = new List<Task>();
        List<Cost_Analysis_Case__c> toUpdate = new List<Cost_Analysis_Case__c>();
       
        for(Cost_Analysis_Case__c setCase : triggerNew){
        	
            if(setCase.Task_Created__c == false && setCase.Account__c != null && setCase.Start_Date__c != null && setCase.Status__c.startsWith('Closed ')){
            	
                Task SetTask = new Task();
                SetTask.OwnerId = setCase.OwnerId;
                SetTask.Status = 'Completed';
                SetTask.Priority = 'Low';
                SetTask.IsReminderSet = false;
                SetTask.ActivityDate = setCase.Start_Date__c;   
                SetTask.RecordTypeId = SETTaskRTId; 
                SetTask.WhatId = setCase.Opportunity__c != null ? setCase.Opportunity__c : setCase.Account__c;
                SetTask.Activity_Type__c = 'Scrub/Clin. Eval.';
                
                if(setCase.Contact__c != null) SetTask.WhoId = setCase.Contact__c;
                
                SetTask.Subject = 'SET Activity for SET Case ' + setCase.Name;
                SetTask.Description = 'This is the SET Activity for SET Case ' + setCase.Name + '\n' + 
                                      'The details of the SET Case can be found at ' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + setCase.Id;
                
                setTasks.add(SetTask);
                
                Cost_Analysis_Case__c caseToUpdate = new Cost_Analysis_Case__c();
                caseToUpdate.id = setCase.Id;
                caseToUpdate.Task_Created__c = true;
                toUpdate.add(caseToUpdate);
            }
        }
        
        if(setTasks.size() > 0) insert setTasks;
        if(toUpdate.size() > 0) update toUpdate;
    }
}