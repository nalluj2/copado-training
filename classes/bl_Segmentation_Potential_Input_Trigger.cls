public without sharing class bl_Segmentation_Potential_Input_Trigger {
	
	public static void setUniqueKey(List<Segmentation_Potential_Input__c> triggerNew, Map<Id, Segmentation_Potential_Input__c> oldMap){
    	
    	Set<Id> procedureIds = new Set<Id>();
    	List<Segmentation_Potential_Input__c> toPopulate = new List<Segmentation_Potential_Input__c>();
    	
    	for(Segmentation_Potential_Input__c potInput : triggerNew){
    		
    		potInput.Unique_Key__c = potInput.Unique_Key_Formula__c;
    		
    		if(potInput.Sub_Business_Unit__c == null || potInput.Business_Unit__c == null || potInput.Business_Unit_Group__c == null){
    			
    			procedureIds.add(potInput.Segmentation_Potential_Procedure__c);
    			toPopulate.add(potInput);
    		}    		
    	}
    	
    	if(toPopulate.size() > 0){
	    		    	
	    	Map<Id, Segmentation_Potential_Procedure__c> procedureMap = new Map<Id, Segmentation_Potential_Procedure__c>([Select Id, Sub_Business_Unit__c from Segmentation_Potential_Procedure__c where Id IN :procedureIds]);
	    	
	    	Map<String, Sub_Business_Units__c> sbuMap = new Map<String, Sub_Business_Units__c>();
	    	for(Sub_Business_Units__c sbu : [Select Name, Business_Unit__r.Name, Business_Unit__r.Business_Unit_Group__r.Name from Sub_Business_Units__c where Business_Unit__r.Company__r.Name = 'Europe']){
	    	
	    		sbuMap.put(sbu.Name, sbu);
	    	}
	    	
	    	for(Segmentation_Potential_Input__c potInput : toPopulate){
	    			    		
	    		Segmentation_Potential_Procedure__c procedure = procedureMap.get(potInput.Segmentation_Potential_Procedure__c);
	    		Sub_Business_Units__c sbu = sbuMap.get(procedure.Sub_Business_Unit__c);
	    			
	    		if(potInput.Sub_Business_Unit__c == null) potInput.Sub_Business_Unit__c = sbu.Name;
	    		if(potInput.Business_Unit__c == null) potInput.Business_Unit__c = sbu.Business_Unit__r.Name;
	    		if(potInput.Business_Unit_Group__c == null) potInput.Business_Unit_Group__c = sbu.Business_Unit__r.Business_Unit_Group__r.Name;	    		    		
	    	}
    	}
    	
    	for(Segmentation_Potential_Input__c potInput : triggerNew){
    		    		
    		String fiscalYear = potInput.Unique_Key__c.split(':')[2];	    		
	    	    		
    		potInput.Segmentation_Key__c = potInput.Account__c + ':' + potInput.Sub_Business_Unit__c + ':' + fiscalYear;
    	}
    } 
    
    public static void matchSegmentation(List<Segmentation_Potential_Input__c> triggerNew, Map<Id, Segmentation_Potential_Input__c> oldMap){
    	
    	Set<String> keys = new Set<String>();
    	List<Segmentation_Potential_Input__c> toPopulate = new List<Segmentation_Potential_Input__c>();
    	
    	for(Segmentation_Potential_Input__c potInput : triggerNew){
    		
    		if(oldMap == null || potInput.Segmentation_Key__c != oldMap.get(potInput.Id).Segmentation_Key__c){
    				
    			List<String> inputKey = potInput.Segmentation_Key__c.split(':'); 
    			
    			Integer inputFiscalYear = Integer.valueOf(inputKey[2].split('FY')[1]);
    			
    			String key = inputKey[0] + ':' + inputKey[1] + ':FY' + (inputFiscalYear - 1);
    			
    			keys.add(key);
    			toPopulate.add(potInput);
			}	
    	}
    	
    	if(toPopulate.size() > 0){
	    	
	    	Map<String, Id> customerSegmentationMap = new Map<String, Id>();
	    	for(Customer_Segmentation__c customerSegmentation : [Select Id, Unique_Key__c from Customer_Segmentation__c where Unique_Key__c IN :keys]) customerSegmentationMap.put(customerSegmentation.Unique_Key__c, customerSegmentation.Id);
	    		    	
	    	for(Segmentation_Potential_Input__c potInput : toPopulate){
	    		
	    		List<String> inputKey = potInput.Segmentation_Key__c.split(':'); 
    			
    			Integer inputFiscalYear = Integer.valueOf(inputKey[2].split('FY')[1]);    			
    			String key = inputKey[0] + ':' + inputKey[1] + ':FY' + (inputFiscalYear - 1);
	    		
	    		potInput.Current_Segmentation__c = customerSegmentationMap.get(key);		
	    	}
    	}
    }  
    
    public static void manageSharing(List<Segmentation_Potential_Input__c> triggerNew, Map<Id, Segmentation_Potential_Input__c> oldMap){
    	
    	List<Segmentation_Potential_Input__c> toCalculate = new List<Segmentation_Potential_Input__c>();
    	
    	for(Segmentation_Potential_Input__c potInput : triggerNew){
    		
    		if(oldMap == null || potInput.Assigned_To__c != oldMap.get(potInput.Id).Assigned_To__c || potInput.Approver_1__c != oldMap.get(potInput.Id).Approver_1__c || potInput.Approver_2__c != oldMap.get(potInput.Id).Approver_2__c ){
    			
    			toCalculate.add(potInput);
    		}
    	}
    	
    	if(toCalculate.size() > 0){
    		
    		Map<Id, Segmentation_Potential_Input__c> potInputMap = new Map<Id, Segmentation_Potential_Input__c>([Select Id, (Select Id, UserOrGroupId from Shares where RowCause = 'Assigned_Approver__c') from Segmentation_Potential_Input__c where Id IN: toCalculate]);
    		
    		List<Segmentation_Potential_Input__Share> toInsert = new List<Segmentation_Potential_Input__Share>();
    		List<Segmentation_Potential_Input__Share> toDelete = new List<Segmentation_Potential_Input__Share>();
    		
    		for(Segmentation_Potential_Input__c potInput : toCalculate){
    			    			    			
    			Map<Id, Segmentation_Potential_Input__Share> existingUserShares = new Map<Id, Segmentation_Potential_Input__Share>();
    			
    			for(Segmentation_Potential_Input__Share share : potInputMap.get(potInput.Id).Shares){
    				
    				existingUserShares.put(share.UserOrGroupId, share);
    			}
    			    			
    			Set<Id> userIds = new Set<Id>();
    			
    			if(potInput.Assigned_to__c != null) userIds.add(potInput.Assigned_to__c);
    			if(potInput.Approver_1__c != null) userIds.add(potInput.Approver_1__c);
    			if(potInput.Approver_2__c != null) userIds.add(potInput.Approver_2__c);
    			
    			for(Id userId : userIds){
    				
    				if(existingUserShares.containsKey(userId) == false){
    					
    					Segmentation_Potential_Input__Share share = new Segmentation_Potential_Input__Share();
    					share.ParentID = potInput.Id;
    					share.RowCause = Schema.Segmentation_Potential_Input__Share.RowCause.Assigned_Approver__c;
    					share.UserOrGroupId = userId;
    					share.AccessLevel = 'Edit';
    					toInsert.add(share);
    				}
    			}	
    			
    			for(Id userId : existingUserShares.keySet()){
    				
    				if(userIds.contains(userId) == false) toDelete.add(existingUserShares.get(userId));
    			}
    		}
    		
    		if(toInsert.size() > 0) insert toInsert;
    		if(toDelete.size() > 0) delete toDelete;    		
    	}
    }  
}