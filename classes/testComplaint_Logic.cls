@isTest
 Public Class testComplaint_Logic{
   static testmethod void  test(){
        
        Account acc=new Account();
        acc.AccountNumber='as';
        acc.Name='abc';
        acc.Account_Country_vs__c='USA';
        acc.CurrencyIsoCode='USD';
        acc.Phone='24107954';
        insert acc; 

        Contact objContact=new Contact();
        objContact.LastName='Saha';
        objContact.FirstName = 'TEST';
        objContact.AccountId = acc.id;
        objContact.Contact_Department__c = 'Pediatric';
        objContact.Contact_Primary_Specialty__c = 'Neurology';
        objContact.Affiliation_To_Account__c = 'Employee';
        objContact.Primary_Job_Title_vs__c = 'Nurse';
        objContact.Contact_Gender__c = 'Female';
        insert objContact;
        
        Asset sys=new Asset();
        sys.Name='Niha';
        sys.Serial_Nr__c='XXXXX';
        sys.recordtypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('O-Arm').getRecordTypeId(); 
        sys.Asset_Product_Type__c='O-Arm 1000';
        sys.AccountId=acc.Id;
        sys.Ownership_Status__c='EOL';	
		insert sys;
        
        //Exlude execution of OMA triggers
    	clsUtil.bDoNotExecute = true;
        
        List<Case> CaseList= new  List<Case>();
        Case objcase1=new Case();
        objcase1.RecordTypeId = clsUtil.getRecordTypeByDevName('Case', 'New_Case').Id;  
        insert objcase1;
        
        bl_CalculateBusinessHoursAges.blnInsertCheck = true;
        
        Test.startTest();
                
        List<Complaint__c> complaintlist = new List<Complaint__c>();
        List<Id>ComId=new List<Id>();
                
        Complaint__c c=new Complaint__c();
        c.Account_Name__c = acc.id;
        c.Status__c = 'Open';
        c.Medtronic_Aware_Date__c = system.today();
        c.Formal_Investigation_Required__c = 'No';
        c.Case__c=objcase1.id;
        c.Asset__c = sys.id;
        insert c;
        
                       
        c.Contact_Name__c = objContact.Id;
        c.Results__c = 'TEST';
        c.Conclusions__c = 'TEST';
        c.Methods__c = 'TEST';
        c.status__c = 'Ready for Closure Review';
        c.RFR_Code__c = 'test';
        c.Brief_Description__c='TEST';
        c.Event_Summary__c='TEST';
        c.Formal_Investigation_Required__c='Complete';
        c.Date_FIR_Set_to_Yes__c = date.today();
        c.Formal_Investigation_Reference__c='TEST';
        c.Formal_Investigation_Justification__c='TEST';
        c.Was_device_used_in_diagnosis__c ='Yes';
        c.Was_device_used_in_treatment__c='Yes';
		c.Did_device_meet_specifications__c='Yes';
		c.Relationship_to_device__c='Test';
		c.Event_Summary__c='test';
		c.Brief_Description__c='test';
		c.Conclusions__c='test';
		c.Methods__c = 'test';
		c.Results__c='test';
		c.Investigation_summary_sent_to_complaint__c='Yes';
        
        update c;
       
        c.Contact_Name__c = objContact.Id;
        c.Results__c = 'TEST';
        c.Conclusions__c = 'TEST';
        c.Methods__c = 'TEST';
        c.status__c = 'Opened in Error';
        c.RFR_Code__c = 'test';
        c.Brief_Description__c='TEST';
        c.Event_Summary__c='TEST';
        c.Formal_Investigation_Required__c='Yes';
        c.Date_FIR_Set_to_Yes__c = date.today();
        c.Formal_Investigation_Reference__c='TEST';
        c.Formal_Investigation_Justification__c='TEST';
        c.Was_device_used_in_diagnosis__c ='Yes';
        c.Was_device_used_in_treatment__c='Yes';
		c.Did_device_meet_specifications__c='Yes';
		c.Relationship_to_device__c='Test';
		c.Event_Summary__c='test';
		c.Brief_Description__c='test';
		c.Conclusions__c='test';
		c.Methods__c = 'test';
		c.Results__c='test';
		c.Investigation_summary_sent_to_complaint__c='Yes';
        update c;      
                      
        Test.stopTest(); 
   }
 } //end c