@isTest private class TEST_ws_SharedMethods {
	
	@isTest static void test_method_one() {
		//------------------------------------------------
		// CREATE TEST DATA
		//------------------------------------------------
		clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'CAN_Diabetes_Standard_Opportunity').Id;
		clsTestData_Opportunity.iRecord_Opportunity = 10;
		clsTestData_Opportunity.createOpportunity(false);

		Map<Integer, String> mapSAPData = new Map<Integer, String>();
		mapSAPData.put(0, 'New,Open,Open Reason 0');
		mapSAPData.put(1, 'Preparation,Open,Open Reason 1');
		mapSAPData.put(2, 'Trial,Open,Open Reason 2');
		mapSAPData.put(3, 'Approval Pending,Open,Open Reason 3');
		mapSAPData.put(4, 'Approved,Open,Open Reason 4');
		mapSAPData.put(5, 'Order Entry,Open,Open Reason 5');
		mapSAPData.put(6, ',Closed / Won,Closed Won Reason');
		mapSAPData.put(7, ',Closed / Lost,Closed Lost Reason');
		mapSAPData.put(8, ',Closed / Cancelled,Closed Cancelled Reason');
		mapSAPData.put(9, ',Hold,Hold Reason');

		Integer i = 0;
		for (Opportunity oOpportunity : clsTestData_Opportunity.lstOpportunity){

			String tData = mapSAPData.get(i);
			List<String> lstData =  tData.split(',');

			oOpportunity.SAP_Phase__c = lstData[0];
			oOpportunity.SAP_Status__c = lstData[1];
			oOpportunity.SAP_Reason__c = lstData[2];

			i++;

		}

		insert clsTestData_Opportunity.lstOpportunity;
		//------------------------------------------------

		for (Opportunity oOpp : clsTestData.lstOpportunity){
			clsUtil.debug('oOpp.Id : ' + oOpp.Id);
			clsUtil.debug('oOpp.SAP_Phase__c : ' + oOpp.SAP_Phase__c);
			clsUtil.debug('oOpp.SAP_Status__c : ' + oOpp.SAP_Status__c);
			clsUtil.debug('oOpp.SAP_Reason__c : ' + oOpp.SAP_Reason__c);
		}


		//------------------------------------------------
		// PERFORM TESTING
		//------------------------------------------------
		ws_SharedMethods.getOpportunityDetails(clsTestData_Opportunity.oMain_Opportunity.Id);
		ws_SharedMethods.getOpportunityLineItemDetails(clsTestData_Opportunity.oMain_Opportunity.Id);

		ws_SharedMethods.getSAPInterfaceProfileName(ut_StaticValues.INTERFACE_NAME_SAP_OPPORTUNITY);
		ws_SharedMethods.getOutboundWebServiceEndpointAndTimeout(ut_StaticValues.INTERFACE_NAME_SAP_OPPORTUNITY);

		for (Opportunity oOpp : clsTestData_Opportunity.lstOpportunity){
			ws_SharedMethods.mapOpportunitySalesStageSAPToSFDC(oOpp);
			ws_SharedMethods.mapOpportunitySalesStageSFDCToSAP(oOpp);
		}

		ws_SharedMethods.processInboundMessage('SAP Opportunity');
		ws_SharedMethods.processInboundMessage('SAP Opportunity', false);
		ws_SharedMethods.processInboundMessage('SAP Opportunity', true);

		ws_SharedMethods.sendOutboundMessage(ut_StaticValues.INTERFACE_NAME_SAP_OPPORTUNITY);
		ws_SharedMethods.sendOutboundMessage(ut_StaticValues.INTERFACE_NAME_SAP_OPPORTUNITY, false);
		ws_SharedMethods.sendOutboundMessage(ut_StaticValues.INTERFACE_NAME_SAP_OPPORTUNITY, true);
		//------------------------------------------------

	}
	
	@isTest static void test_mapOpportunitySalesStageSAPToSFDC() {

		//------------------------------------------------
		// Create Test Data
		//------------------------------------------------
		clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'CAN_DIB_OPPORTUNITY').Id;
		clsTestData_Opportunity.createOpportunity(true);
		//------------------------------------------------


		//------------------------------------------------
		// Perform Test
		//------------------------------------------------
		Test.startTest();

		// Opportunity Status = OPEN
		clsTestData_Opportunity.oMain_Opportunity.SAP_Status__c = 'Open';

		clsTestData_Opportunity.oMain_Opportunity.SAP_Phase__c = 'New';
		clsTestData_Opportunity.oMain_Opportunity.SAP_Reason__c = 'Other New';
		ws_SharedMethods.mapOpportunitySalesStageSAPToSFDC(clsTestData_Opportunity.oMain_Opportunity);
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.StageName, 'New');
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.Reason_Open__c, 'Other New');

		clsTestData_Opportunity.oMain_Opportunity.SAP_Phase__c = 'Preparation';
		clsTestData_Opportunity.oMain_Opportunity.SAP_Reason__c = 'Other Preparation';
		ws_SharedMethods.mapOpportunitySalesStageSAPToSFDC(clsTestData_Opportunity.oMain_Opportunity);
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.StageName, 'Preparation');
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.Reason_Open__c, 'Other Preparation');

		clsTestData_Opportunity.oMain_Opportunity.SAP_Phase__c = 'Approval Pending';
		clsTestData_Opportunity.oMain_Opportunity.SAP_Reason__c = 'Other Approval Pending';
		ws_SharedMethods.mapOpportunitySalesStageSAPToSFDC(clsTestData_Opportunity.oMain_Opportunity);
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.StageName, 'Approval Pending');
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.Reason_Open__c, 'Other Approval Pending');

		clsTestData_Opportunity.oMain_Opportunity.SAP_Phase__c = 'Approved';
		clsTestData_Opportunity.oMain_Opportunity.SAP_Reason__c = 'Other Approved';
		ws_SharedMethods.mapOpportunitySalesStageSAPToSFDC(clsTestData_Opportunity.oMain_Opportunity);
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.StageName, 'Approved');
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.Reason_Open__c, 'Other Approved');

		clsTestData_Opportunity.oMain_Opportunity.SAP_Phase__c = 'Trial';
		clsTestData_Opportunity.oMain_Opportunity.SAP_Reason__c = 'Other Trial';
		ws_SharedMethods.mapOpportunitySalesStageSAPToSFDC(clsTestData_Opportunity.oMain_Opportunity);
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.StageName, 'Trial');
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.Reason_Open__c, 'Other Trial');

		clsTestData_Opportunity.oMain_Opportunity.SAP_Phase__c = 'Order Entry';
		clsTestData_Opportunity.oMain_Opportunity.SAP_Reason__c = 'Other Order Entry';
		ws_SharedMethods.mapOpportunitySalesStageSAPToSFDC(clsTestData_Opportunity.oMain_Opportunity);
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.StageName, 'Order Entry');
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.Reason_Open__c, 'Other Order Entry');

		// Opportunity Status = HOLD
		clsTestData_Opportunity.oMain_Opportunity.SAP_Status__c = 'Hold';
		clsTestData_Opportunity.oMain_Opportunity.SAP_Phase__c = 'New';
		clsTestData_Opportunity.oMain_Opportunity.SAP_Reason__c = 'Other Hold';
		ws_SharedMethods.mapOpportunitySalesStageSAPToSFDC(clsTestData_Opportunity.oMain_Opportunity);
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.StageName, 'Hold');
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.Reason_Hold__c, 'Other Hold');

		// Opportunity Status = CLOSED / WON
		clsTestData_Opportunity.oMain_Opportunity.SAP_Status__c = 'Closed / Won';
		clsTestData_Opportunity.oMain_Opportunity.SAP_Phase__c = 'New';
		clsTestData_Opportunity.oMain_Opportunity.SAP_Reason__c = 'Other Closed / Won';
		ws_SharedMethods.mapOpportunitySalesStageSAPToSFDC(clsTestData_Opportunity.oMain_Opportunity);
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.StageName, 'Closed Won');
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.Reason_Won__c, 'Other Closed / Won');

		// Opportunity Status = CLOSED / LOST
		clsTestData_Opportunity.oMain_Opportunity.SAP_Status__c = 'Closed / Lost';
		clsTestData_Opportunity.oMain_Opportunity.SAP_Phase__c = 'New';
		clsTestData_Opportunity.oMain_Opportunity.SAP_Reason__c = 'Other Closed / Lost';
		ws_SharedMethods.mapOpportunitySalesStageSAPToSFDC(clsTestData_Opportunity.oMain_Opportunity);
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.StageName, 'Closed Lost');
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.Reason_Lost__c, 'Other Closed / Lost');

		// Opportunity Status = CLOSED / CANCELLED
		clsTestData_Opportunity.oMain_Opportunity.SAP_Status__c = 'Closed / Cancelled';
		clsTestData_Opportunity.oMain_Opportunity.SAP_Phase__c = 'New';
		clsTestData_Opportunity.oMain_Opportunity.SAP_Reason__c = 'Other Closed / Cancelled';
		ws_SharedMethods.mapOpportunitySalesStageSAPToSFDC(clsTestData_Opportunity.oMain_Opportunity);
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.StageName, 'Closed Cancelled');
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.Reason_Cancelled__c, 'Other Closed / Cancelled');

			clsTestData_Opportunity.oMain_Opportunity.Reason_Lost__c = null;
			clsTestData_Opportunity.oMain_Opportunity.Reason_Won__c = null;
			clsTestData_Opportunity.oMain_Opportunity.Reason_Open__c = null;
			clsTestData_Opportunity.oMain_Opportunity.Reason_Hold__c = null;
		update clsTestData_Opportunity.oMain_Opportunity;

		Test.stopTest();
		//------------------------------------------------


		//------------------------------------------------
		// Validation
		//------------------------------------------------
		List<Opportunity> lstOpportunity = [SELECT Id, StageName, Reason_Cancelled__c, SAP_Status__c, SAP_Phase__c, SAP_Reason__c FROM Opportunity WHERE Id = :clsTestData_Opportunity.oMain_Opportunity.Id];

		System.assertEquals(lstOpportunity.size(), 1);
		System.assertEquals(lstOpportunity[0].StageName, 'Closed Cancelled');
		System.assertEquals(lstOpportunity[0].SAP_Status__c, 'Closed / Cancelled');
		System.assertEquals(lstOpportunity[0].Reason_Cancelled__c, lstOpportunity[0].SAP_Reason__c, 'Other Closed / Cancelled');
			//------------------------------------------------

	}


	@isTest static void test_mapOpportunitySalesStageSFDCToSAP() {

		//------------------------------------------------
		// Create Test Data
		//------------------------------------------------
		clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'CAN_DIB_OPPORTUNITY').Id;
		clsTestData_Opportunity.createOpportunity(false);
		//------------------------------------------------


		//------------------------------------------------
		// Perform Test
		//------------------------------------------------
		Test.startTest();

		// Opportunity StageName = NEW
		clsTestData_Opportunity.oMain_Opportunity.StageName = 'New';
		clsTestData_Opportunity.oMain_Opportunity.Reason_Open__c = 'Other New';
		ws_SharedMethods.mapOpportunitySalesStageSFDCToSAP(clsTestData_Opportunity.oMain_Opportunity);
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.SAP_Status__c, 'Open');
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.SAP_Phase__c, 'New');
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.SAP_Reason__c, 'Other New');

		// Opportunity StageName = PREPARATION
		clsTestData_Opportunity.oMain_Opportunity.StageName = 'Preparation';
		clsTestData_Opportunity.oMain_Opportunity.Reason_Open__c = 'Other Preparation';
		ws_SharedMethods.mapOpportunitySalesStageSFDCToSAP(clsTestData_Opportunity.oMain_Opportunity);
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.SAP_Status__c, 'Open');
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.SAP_Phase__c, 'Preparation');
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.SAP_Reason__c, 'Other Preparation');

		// Opportunity StageName = APPROVAL PENDING
		clsTestData_Opportunity.oMain_Opportunity.StageName = 'Approval Pending';
		clsTestData_Opportunity.oMain_Opportunity.Reason_Open__c = 'Other Approval Pending';
		ws_SharedMethods.mapOpportunitySalesStageSFDCToSAP(clsTestData_Opportunity.oMain_Opportunity);
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.SAP_Status__c, 'Open');
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.SAP_Phase__c, 'Approval Pending');
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.SAP_Reason__c, 'Other Approval Pending');

		// Opportunity StageName = APPROVED
		clsTestData_Opportunity.oMain_Opportunity.StageName = 'Approved';
		clsTestData_Opportunity.oMain_Opportunity.Reason_Open__c = 'Other Approved';
		ws_SharedMethods.mapOpportunitySalesStageSFDCToSAP(clsTestData_Opportunity.oMain_Opportunity);
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.SAP_Status__c, 'Open');
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.SAP_Phase__c, 'Approved');
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.SAP_Reason__c, 'Other Approved');

		// Opportunity StageName = TRIAL
		clsTestData_Opportunity.oMain_Opportunity.StageName = 'Trial';
		clsTestData_Opportunity.oMain_Opportunity.Reason_Open__c = 'Other Trial';
		ws_SharedMethods.mapOpportunitySalesStageSFDCToSAP(clsTestData_Opportunity.oMain_Opportunity);
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.SAP_Status__c, 'Open');
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.SAP_Phase__c, 'Trial');
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.SAP_Reason__c, 'Other Trial');

		// Opportunity StageName = ORDER ENTRY
		clsTestData_Opportunity.oMain_Opportunity.StageName = 'Order Entry';
		clsTestData_Opportunity.oMain_Opportunity.Reason_Open__c = 'Other Order Entry';
		ws_SharedMethods.mapOpportunitySalesStageSFDCToSAP(clsTestData_Opportunity.oMain_Opportunity);
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.SAP_Status__c, 'Open');
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.SAP_Phase__c, 'Order Entry');
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.SAP_Reason__c, 'Other Order Entry');


		// Opportunity StageName = HOLD
		clsTestData_Opportunity.oMain_Opportunity.StageName = 'Hold';
		clsTestData_Opportunity.oMain_Opportunity.Reason_Hold__c = 'Other Hold';
		ws_SharedMethods.mapOpportunitySalesStageSFDCToSAP(clsTestData_Opportunity.oMain_Opportunity);
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.SAP_Status__c, 'Hold');
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.SAP_Reason__c, 'Other Hold');

		// Opportunity StageName = CLOSED / WON
		clsTestData_Opportunity.oMain_Opportunity.StageName = 'Closed Won';
		clsTestData_Opportunity.oMain_Opportunity.Reason_Won__c = 'Other Closed Won';
		ws_SharedMethods.mapOpportunitySalesStageSFDCToSAP(clsTestData_Opportunity.oMain_Opportunity);
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.SAP_Status__c, 'Closed / Won');
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.SAP_Reason__c, 'Other Closed Won');

		// Opportunity StageName = CLOSED / LOST
		clsTestData_Opportunity.oMain_Opportunity.StageName = 'Closed Lost';
		clsTestData_Opportunity.oMain_Opportunity.Reason_Lost__c = 'Other Closed Lost';
		ws_SharedMethods.mapOpportunitySalesStageSFDCToSAP(clsTestData_Opportunity.oMain_Opportunity);
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.SAP_Status__c, 'Closed / Lost');
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.SAP_Reason__c, 'Other Closed Lost');

		// Opportunity StageName = CLOSED / CANCELLED
		clsTestData_Opportunity.oMain_Opportunity.StageName = 'Closed Cancelled';
		clsTestData_Opportunity.oMain_Opportunity.Reason_Cancelled__c = 'Other Closed Cancelled';
		ws_SharedMethods.mapOpportunitySalesStageSFDCToSAP(clsTestData_Opportunity.oMain_Opportunity);
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.SAP_Status__c, 'Closed / Cancelled');
		System.assertEquals(clsTestData_Opportunity.oMain_Opportunity.SAP_Reason__c, 'Other Closed Cancelled');


			clsTestData_Opportunity.oMain_Opportunity.Reason_Cancelled__c = null;
			clsTestData_Opportunity.oMain_Opportunity.Reason_Lost__c = null;
			clsTestData_Opportunity.oMain_Opportunity.Reason_Won__c = null;
			clsTestData_Opportunity.oMain_Opportunity.Reason_Hold__c = null;
			clsTestData_Opportunity.oMain_Opportunity.StageName = 'New';
			clsTestData_Opportunity.oMain_Opportunity.Reason_Open__c = 'Other';
		insert clsTestData_Opportunity.oMain_Opportunity;

		Test.stopTest();
		//------------------------------------------------


		//------------------------------------------------
		// Validation
		//------------------------------------------------
		List<Opportunity> lstOpportunity = [SELECT Id, StageName, Reason_Open__c, SAP_Status__c, SAP_Phase__c, SAP_Reason__c FROM Opportunity WHERE Id = :clsTestData_Opportunity.oMain_Opportunity.Id];

		System.assertEquals(lstOpportunity.size(), 1);
		System.assertEquals(lstOpportunity[0].StageName, lstOpportunity[0].SAP_Phase__c, 'New');
		System.assertEquals(lstOpportunity[0].Reason_Open__c, lstOpportunity[0].SAP_Reason__c, 'Other');
		System.assertEquals(lstOpportunity[0].SAP_Status__c, 'Open');
		//------------------------------------------------

	}
	
}