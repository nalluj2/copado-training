@isTest
private class Test_bl_XmlGenerator {

    static testMethod void getXmlToDownloadShouldReturnTheXml() {
                
       List<ReleaseDeploymentComponentWrapper> rcdw = createWrapperData();
       
       string xmlResult = bl_XmlGenerator.generatePackageXml(rcdw);
       
       string resultToExpect ='<?xml version="1.0" encoding="UTF-8"?>'+
							  '<Package xmlns="http://soap.sforce.com/2006/04/metadata">'+
    							'<types>'+
        							'<members>ctrl_Project</members>'+
        							'<name>ApexClass</name>'+
    							'</types>'+
    							'<types>'+
        							'<members>Account_Plan_2__c.Business_Unit_Group__c</members>'+
        							'<members>Account_Plan_2__c.Country__c</members>'+
        							'<members>Account_Plan_2__c.Logistics_Influence__c</members>'+
        							'<name>CustomField</name>'+
    							'</types>'+
    							'<types>'+
        							'<members>contract_manager</members>'+
        							'<name>PermissionSet</name>'+
    							'</types>'+
    							'<types>'+
        							'<members>Account_Plan_2__c.EUR_NEURO_BU</members>'+
        							'<members>Account_Plan_2__c.EUR_SPINE_sBU</members>'+
        							'<name>RecordType</name>'+
    							'</types>'+
    							'<version>28.0</version>'+
								'</Package>';
       
       
       //System.assertEquals(xmlResult,resultToExpect);
    }
    
    private static List<ReleaseDeploymentComponentWrapper> createWrapperData(){
    	List<ReleaseDeploymentComponentWrapper> rcdw = new List<ReleaseDeploymentComponentWrapper>();
       
       Release_Deployment_Component__c rdc	= new Release_Deployment_Component__c();
       rdc.Meta_data_type__c = 'ApexClass';
       rdc.API_Field_name__c ='ctrl_Project';
       
       ReleaseDeploymentComponentWrapper item = new ReleaseDeploymentComponentWrapper(rdc);
       rcdw.add(item);
       
       rdc	= new Release_Deployment_Component__c();
       rdc.Meta_data_type__c = 'CustomField';
       rdc.API_Field_name__c ='Account_Plan_2__c.Business_Unit_Group__c';
       
       item = new ReleaseDeploymentComponentWrapper(rdc);
       rcdw.add(item);
       
       rdc	= new Release_Deployment_Component__c();
       rdc.Meta_data_type__c = 'CustomField';
       rdc.API_Field_name__c ='Account_Plan_2__c.Country__c';
       
       item = new ReleaseDeploymentComponentWrapper(rdc);
       rcdw.add(item);
       
       rdc	= new Release_Deployment_Component__c();
       rdc.Meta_data_type__c = 'CustomField';
       rdc.API_Field_name__c ='Account_Plan_2__c.Logistics_Influence__c';
       
       item = new ReleaseDeploymentComponentWrapper(rdc);
       rcdw.add(item);
       
       rdc	= new Release_Deployment_Component__c();
       rdc.Meta_data_type__c = 'PermissionSet';
       rdc.API_Field_name__c ='contract_manager';
       
       item = new ReleaseDeploymentComponentWrapper(rdc);
       rcdw.add(item);
       
       rdc	= new Release_Deployment_Component__c();
       rdc.Meta_data_type__c = 'RecordType';
       rdc.API_Field_name__c ='Account_Plan_2__c.EUR_NEURO_BU';
       
       item = new ReleaseDeploymentComponentWrapper(rdc);
       rcdw.add(item);
       
       rdc	= new Release_Deployment_Component__c();
       rdc.Meta_data_type__c = 'RecordType';
       rdc.API_Field_name__c ='Account_Plan_2__c.EUR_SPINE_sBU';
       
       item = new ReleaseDeploymentComponentWrapper(rdc);
       rcdw.add(item);
       
       return rcdw;
    	
    }
}