/**
 * @description       : 
 * @author            : tom.h.ansley@medtronic.com
 * @last modified on  : 10-29-2020
 * @last modified by  : tom.h.ansley@medtronic.com
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   10-28-2020   tom.h.ansley@medtronic.com   Initial Version
**/
@isTest
public with sharing class Test_ctrl_SMAX_STO_Create {

    static testMethod void testSTOCreateUI() 
    {
    	    	        
        ctrl_SMAX_STO_Create controller = new ctrl_SMAX_STO_Create(new ApexPages.StandardController(new SVMXC__Parts_Request__c()));        
        controller.order.Delivery_Type__c = '2DAY';
        controller.deliveryTypeChanged();
        
        controller.order.Receiving_Address_Type__c = 'I will provide Address';
        controller.receivingAddressChanged();
        controller.order.Name1__c = 'Testy Testerson';
        controller.order.Street__c = '12 Main St';
        controller.order.Postal_Code__c = '12345';
        controller.order.City__c  = 'Antwerp';
        controller.order.StateRegion__c  = 'Antwerp';        
        controller.order.Country__c = 'BELGIUM';
        
        controller.save();
		System.assert(controller.pageError == '');
        
        controller.submit(); //FAIL NO LINE ITEMS
		System.assert(controller.pageError != '');
        
        controller.addLineItem();
        
        controller.lineItems[0].Requested_Qty__c = 1;
        controller.lineItems[0].Unit_of_Measure__c = 'EA';

        controller.save();
		System.assert(controller.pageError == '');
		
        controller.submit(); //FAIL AGAIN (NO LINE ITEM PRODUCT)
		System.assert(controller.pageError != '');
        
        controller.lineItems[0].SVMXC__Product__c = [SELECT Id FROM Product2 LIMIT 1].Id;

        controller.save();
		System.assert(controller.pageError == '');
		
        controller.addLineItem();

        controller.lineItemId = 1;
        controller.removeLineItem();

        controller.save();
        System.assert(controller.pageError == '');

		Test.startTest();    	
        Test.setMock(HttpCalloutMock.class, new Test_STO_Service_Mock());
        
        controller.submit(); //SUCCESS!!
        System.assert(controller.pageError == '');
        
        Test.stopTest();
		
		SVMXC__Parts_Request__c order = [Select Id, SAP_STO_Number__c from SVMXC__Parts_Request__c where Id = :controller.order.Id];
		System.assertEquals('9999999999', order.SAP_STO_Number__c);
    }
    
    static testMethod void testSTOUpdateUI() 
    {
    	
    	SVMXC__Parts_Request__c order = new SVMXC__Parts_Request__c();
    	order.Delivery_Type__c = '2DAY';
    	order.Receiving_Address_Type__c = 'I will provide Address';
    	order.Name1__c = 'Testy Testerson';    	
    	order.Street__c = '12 Main St';
    	order.Postal_Code__c = '12345';
    	order.StateRegion__c = 'Antwerp';
    	order.City__c  = 'Antwerp'; 
    	order.Country__c = 'BELGIUM';    	
    	insert order;
    	
    	SVMXC__Parts_Request_Line__c orderLine = new SVMXC__Parts_Request_Line__c();
    	orderLine.SVMXC__Parts_Request__c = order.Id;
    	orderLine.SVMXC__Product__c = [Select Id from Product2 LIMIT 1].Id;
    	orderLine.Requested_Qty__c = 1;
    	orderLine.Unit_of_Measure__c = 'EA';
    	insert orderLine;
    	        
        ctrl_SMAX_STO_Create controller = new ctrl_SMAX_STO_Create(new ApexPages.StandardController(order));        
                
        controller.lineItems[0].Requested_Qty__c = 2;
        
        controller.save();        
        System.assert(controller.pageError == '');

		Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Test_STO_Service_Mock());
        
        controller.submit();        
        System.assertEquals('', controller.pageError);
        
        Test.stopTest();
    }
    
    static testMethod void testSTOCloneUI() 
    {
    	
    	SVMXC__Parts_Request__c order = new SVMXC__Parts_Request__c();
    	order.Delivery_Type__c = '2DAY';
    	order.Receiving_Address_Type__c = 'I will provide Address';
    	order.Name1__c = 'Testy Testerson';    	
    	order.Street__c = '12 Main St';
    	order.Postal_Code__c = '12345';
    	order.StateRegion__c = 'Antwerp';
    	order.City__c  = 'Antwerp'; 
    	order.Country__c = 'BELGIUM';    	
    	insert order;
    	
    	SVMXC__Parts_Request_Line__c orderLine = new SVMXC__Parts_Request_Line__c();
    	orderLine.SVMXC__Parts_Request__c = order.Id;
    	orderLine.SVMXC__Product__c = [Select Id from Product2 LIMIT 1].Id;
    	orderLine.Requested_Qty__c = 1;
    	orderLine.Unit_of_Measure__c = 'EA';
    	insert orderLine;
    	
    	String originalOrderId = order.Id;
    	        
    	Pagereference STOpage = Page.SMAX_STO_Create;
    	STOpage.getParameters().put('clone', '1');
    	Test.setCurrentPageReference(STOPage);
        ctrl_SMAX_STO_Create controller = new ctrl_SMAX_STO_Create(new ApexPages.StandardController(order));        
                
        controller.countryChanged();
        
        controller.order.Receiving_Address_Type__c = 'My Address';
        controller.receivingAddressChanged();
        
        controller.order.Delivery_Type__c = 'OVERNIGHT';
        controller.deliveryTypeChanged();
        System.assert(controller.pageError != '');
        
        controller.submit(); //FAIL (NO COMMENTS ADDED)
		System.assert(controller.pageError != '');
		
        controller.order.Part_Request_Comments__c = 'Overnight comments';
        
        controller.save();        
        System.assert(controller.pageError == '');

		Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Test_STO_Service_Mock());
        
        controller.submit();        
        System.assertEquals('', controller.pageError);
        
        Test.stopTest();
        
        System.assertNotEquals(originalOrderId, controller.order.Id);
    }
    
    static testMethod void testSTODelete() 
    {
    	
    	SVMXC__Parts_Request__c order = new SVMXC__Parts_Request__c();
    	order.Delivery_Type__c = '2DAY';
    	order.Receiving_Address_Type__c = 'I will provide Address';
    	order.Name1__c = 'Testy Testerson';    	
    	order.Street__c = '12 Main St';
    	order.Postal_Code__c = '12345';
    	order.StateRegion__c = 'Antwerp';
    	order.City__c  = 'Antwerp'; 
    	order.Country__c = 'BELGIUM';    	
    	insert order;
    	    	        
        ctrl_SMAX_STO_Create controller = new ctrl_SMAX_STO_Create(new ApexPages.StandardController(order));        
              
        controller.deleteOrder();        
    }


    //----------------------------------------------------------------------------------------
    // CREATE TEST DATA
    //----------------------------------------------------------------------------------------
    @testSetup static void createTestData() {

        // Create Custom Setting Data
        clsTestData.createCustomSettingData(true);

        clsTestData.createProductData();

        clsTestData.createSVMXCServiceGroupMemberData(true);
        
        clsTestData.createCountryData();
        
        clsTestData.createSAPRegionData();
    }
    //----------------------------------------------------------------------------------------
   
}