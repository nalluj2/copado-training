/*
 *      Description : This class exposes methods to create / update / delete a CVG Project Opportunity 
 *
 *      Author = Rudy De Coninck
 */
@RestResource(urlMapping='/CVGProjectOpportunityService/*')
global class ws_CVGProjectOpportunityService{
	
	@HttpPost
	global static String doPost() {
 		RestRequest req = RestContext.request;
			
		System.debug('post called '+req);
		System.debug('post requestBody '+req.requestBody.toString());
			
		String body = req.requestBody.toString();
		IFCVGProjectOpportunityRequest CVGProjectOpportunityRequest = (IFCVGProjectOpportunityRequest)System.Json.deserialize(body, IFCVGProjectOpportunityRequest.class);
			
		System.debug('post requestBody '+CVGProjectOpportunityRequest);
		ICVGProjectOpportunityResponse resp = new ICVGProjectOpportunityResponse();	
		try{
				
			Opportunity opp = CVGProjectOpportunityRequest.opportunity;
			
			resp.opportunity = bl_AccountPlanning.saveCVGProjectOpportunity(opp);
			resp.id = CVGProjectOpportunityRequest.id;
	
			resp.success = true;
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
		}
 
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'CVGProjectOpportunity' ,req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
  			
		return System.JSON.serialize(resp);		
	}

	@HttpDelete
	global static String doDelete(){
		RestRequest req = RestContext.request;
			
		System.debug('post called '+req);
		System.debug('post requestBody '+req.requestBody);
			
		String body = req.requestBody.toString();
		IFCVGProjectOpportunityDeleteResponse resp = new IFCVGProjectOpportunityDeleteResponse();
		
		IFCVGProjectOpportunityDeleteRequest CVGProjectOpportunityDeleteRequest = (IFCVGProjectOpportunityDeleteRequest)System.Json.deserialize(body, IFCVGProjectOpportunityDeleteRequest.class);
			
		System.debug('post requestBody '+CVGProjectOpportunityDeleteRequest);
			
		Opportunity opp =CVGProjectOpportunityDeleteRequest.opportunity;
		
		List<Opportunity>oppToDelete;
		if(opp==null || opp.mobile_Id__c==null){
			oppToDelete = new List<Opportunity>();
		}else{
			oppToDelete = [select id, mobile_id__c from Opportunity where Mobile_ID__c = :opp.Mobile_ID__c];
		}
		
		try{
			
			if (oppToDelete !=null && oppToDelete.size()>0 ){	
				
				delete oppToDelete;
				resp.opportunity = oppToDelete.get(0);
				resp.id = CVGProjectOpportunityDeleteRequest.id;
				resp.success = true;
			}else{
				resp.opportunity = opp;
				resp.id = CVGProjectOpportunityDeleteRequest.id;
				resp.message='Opportunity not found';
				resp.success = true;
			}
			
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
		}
  			
			
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'CVGProjectOpportunityDelete',req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
		
		return System.JSON.serialize(resp);
	}
	
	global class IFCVGProjectOpportunityRequest{
		public String id {get;set;}
		public Opportunity opportunity {get;set;}
	}
	
	global class IFCVGProjectOpportunityDeleteRequest{
		public String id{get;set;}
		public Opportunity opportunity {get;set;}
	}
		
	global class IFCVGProjectOpportunityDeleteResponse{
		public String id{get;set;}
		public Opportunity opportunity {get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}

	global class ICVGProjectOpportunityResponse{
		public String id {get;set;}
		public Opportunity opportunity {get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}
	

}