@isTest
private class Test_ba_create_technician_adhoc_orders {
    
    private static testmethod void createTechnicianWorkOders(){
    	
    	List<User> users = [Select Id from User where Profile.Name = 'IT Business Analyst' AND isActive = true LIMIT 2];
    	System.assert(users.size() == 2);
    	
    	createTechnicianData(users);
    	
    	SVMXC__Service_Group_Members__c technician = [SELECT Id, SVMXC__Inventory_Location__c, SVMXC__Salesforce_User__c, SVMXC__Service_Group__c, SVMX_SAP_Employee_Id__c FROM SVMXC__Service_Group_Members__c LIMIT 1];
    	
    	SVMXC__Service_Order__c adhoc = new SVMXC__Service_Order__c();
		adhoc.SVMXC__Order_Status__c = 'On Site';
		adhoc.SVMXC__Purpose_of_Visit__c = 'Placeholder';
		adhoc.SVMXC__Group_Member__c = technician.Id;
		adhoc.SVMXC__Service_Group__c = technician.SVMXC__Service_Group__c;
		adhoc.SVMX_FSE_Stock_Location__c = technician.SVMXC__Inventory_Location__c;
		adhoc.SVMX_SAP_Employee_Id__c = technician.SVMX_SAP_Employee_Id__c;
		adhoc.OwnerId = technician.SVMXC__Salesforce_User__c;
		
		SVMXC__Service_Order__c nonAdhoc = new SVMXC__Service_Order__c();
		nonAdhoc.SVMXC__Order_Status__c = 'On Site';
		nonAdhoc.SVMXC__Purpose_of_Visit__c = 'Repair';
		nonAdhoc.SVMXC__Group_Member__c = technician.Id;
		nonAdhoc.SVMXC__Service_Group__c = technician.SVMXC__Service_Group__c;
		nonAdhoc.SVMX_FSE_Stock_Location__c = technician.SVMXC__Inventory_Location__c;
		nonAdhoc.SVMX_SAP_Employee_Id__c = technician.SVMX_SAP_Employee_Id__c;
		nonAdhoc.OwnerId = technician.SVMXC__Salesforce_User__c;
		    	    	    	
    	insert new List<SVMXC__Service_Order__c>{adhoc, nonAdhoc};    	    	
    	    	    	
    	Test.startTest();
    	
    	Database.executeBatch( new ba_create_technician_adhoc_orders());

    	Test.stopTest();
    	
    	List<SVMXC__Service_Order__c> adhocWOs = [Select Id, SVMXC__Order_Status__c, SVMXC__Purpose_of_Visit__c, SVMXC__Group_Member__c, OwnerId from SVMXC__Service_Order__c];
    	
    	System.assert(adhocWOs.size() == 21);   
    	
    	for(SVMXC__Service_Order__c adhocWO : adhocWOs){
    		
    		if(adhocWO.Id == nonAdhoc.Id) continue;
    		
    		System.assert(adhocWO.SVMXC__Order_Status__c == 'On Site');
    		System.assert(adhocWO.SVMXC__Purpose_of_Visit__c == 'Placeholder');
    		System.assert(adhocWO.SVMXC__Group_Member__c != null);
    		System.assert(adhocWO.OwnerId != UserInfo.getUserId());
    	}	
    }
    
    private static testmethod void createTechnicianWorkOders_20(){
    	
    	List<User> users = [Select Id from User where Profile.Name = 'IT Business Analyst' AND isActive = true LIMIT 2];
    	System.assert(users.size() == 2);
    	
    	createTechnicianData(users);
    	
    	SVMXC__Service_Group_Members__c technician = [SELECT Id, SVMXC__Inventory_Location__c, SVMXC__Salesforce_User__c, SVMXC__Service_Group__c, SVMX_SAP_Employee_Id__c FROM SVMXC__Service_Group_Members__c LIMIT 1];
    	
    	SVMXC__Service_Order__c adhoc = new SVMXC__Service_Order__c();
		adhoc.SVMXC__Order_Status__c = 'On Site';
		adhoc.SVMXC__Purpose_of_Visit__c = 'Placeholder';
		adhoc.SVMXC__Group_Member__c = technician.Id;
		adhoc.SVMXC__Service_Group__c = technician.SVMXC__Service_Group__c;
		adhoc.SVMX_FSE_Stock_Location__c = technician.SVMXC__Inventory_Location__c;
		adhoc.SVMX_SAP_Employee_Id__c = technician.SVMX_SAP_Employee_Id__c;
		adhoc.OwnerId = technician.SVMXC__Salesforce_User__c;
		
		SVMXC__Service_Order__c nonAdhoc = new SVMXC__Service_Order__c();
		nonAdhoc.SVMXC__Order_Status__c = 'On Site';
		nonAdhoc.SVMXC__Purpose_of_Visit__c = 'Repair';
		nonAdhoc.SVMXC__Group_Member__c = technician.Id;
		nonAdhoc.SVMXC__Service_Group__c = technician.SVMXC__Service_Group__c;
		nonAdhoc.SVMX_FSE_Stock_Location__c = technician.SVMXC__Inventory_Location__c;
		nonAdhoc.SVMX_SAP_Employee_Id__c = technician.SVMX_SAP_Employee_Id__c;
		nonAdhoc.OwnerId = technician.SVMXC__Salesforce_User__c;
		    	    	    	
    	insert new List<SVMXC__Service_Order__c>{adhoc, nonAdhoc};    	    	
    	    	    	
    	Test.startTest();
    	
    	ba_create_technician_adhoc_orders batch = new ba_create_technician_adhoc_orders();
    	batch.noAdhocs = 20;
    	Database.executeBatch(batch);

    	Test.stopTest();
    	
    	List<SVMXC__Service_Order__c> adhocWOs = [Select Id, SVMXC__Order_Status__c, SVMXC__Purpose_of_Visit__c, SVMXC__Group_Member__c, OwnerId from SVMXC__Service_Order__c];
    	
    	System.assert(adhocWOs.size() == 41);   
    	
    	for(SVMXC__Service_Order__c adhocWO : adhocWOs){
    		
    		if(adhocWO.Id == nonAdhoc.Id) continue;
    		
    		System.assert(adhocWO.SVMXC__Order_Status__c == 'On Site');
    		System.assert(adhocWO.SVMXC__Purpose_of_Visit__c == 'Placeholder');
    		System.assert(adhocWO.SVMXC__Group_Member__c != null);
    		System.assert(adhocWO.OwnerId != UserInfo.getUserId());
    	}	
    }
    
    private static void createTechnicianData(List<User> users){			
		
		SVMXC__Service_Group__c techGroup = new SVMXC__Service_Group__c();
    	techGroup.SVMXC__Active__c = true; 
    	techGroup.SVMXC__Country__c = 'Netherlands, The';
    	techGroup.SVMXC__Description__c = 'Test Technician Group';    	
    	insert techGroup;
    	
    	SVMXC__Service_Group_Members__c technician = new SVMXC__Service_Group_Members__c();
    	technician.SVMXC__Active__c = true;
    	technician.SVMXC__Country__c = 'Netherlands, The';
    	technician.SVMXC__Salesforce_User__c = users[0].Id;
    	technician.SVMX_SAP_Employee_Id__c = '55555';
    	technician.SVMXC__Service_Group__c = techGroup.Id; 
    	
    	SVMXC__Service_Group_Members__c techMate = new SVMXC__Service_Group_Members__c();
    	techMate.SVMXC__Active__c = true;
    	techMate.SVMXC__Country__c = 'Netherlands, The';
    	techMate.SVMXC__Salesforce_User__c = users[1].Id;
    	techMate.SVMX_SAP_Employee_Id__c = '66666';
    	techMate.SVMXC__Service_Group__c = techGroup.Id;    	
    	
    	insert new List<SVMXC__Service_Group_Members__c>{technician, techMate};
    	    	    	
    	SVMXC__Site__c techLocation = new SVMXC__Site__c();
    	techLocation.SVMXC__Country__c = 'Netherlands, The';
    	techLocation.SVMX_SAP_Location_ID__c = 'YYYYY';
    	techLocation.SVMXC__Service_Engineer__c = UserInfo.getUserId();
    	techLocation.SVMX_Technician__c = technician.Id;
    	techLocation.SVMXC__Stocking_Location__c = true;
    	techLocation.SVMXC__IsStaging_Location__c = true;
    	techLocation.SVMXC__IsRepair_Location__c = true;
    	techLocation.SVMXC__IsReceiving_Location__c = true;
    	techLocation.SVMXC__IsGood_Stock__c = true;
    	
    	SVMXC__Site__c techMateLocation = new SVMXC__Site__c();
    	techMateLocation.SVMXC__Country__c = 'Netherlands, The';
    	techMateLocation.SVMX_SAP_Location_ID__c = 'ZZZZZ';
    	techMateLocation.SVMXC__Service_Engineer__c = UserInfo.getUserId();
    	techMateLocation.SVMX_Technician__c = techMate.Id;
    	techMateLocation.SVMXC__Stocking_Location__c = true;
    	techMateLocation.SVMXC__IsStaging_Location__c = true;
    	techMateLocation.SVMXC__IsRepair_Location__c = true;
    	techMateLocation.SVMXC__IsReceiving_Location__c = true;
    	techMateLocation.SVMXC__IsGood_Stock__c = true;
    	
    	insert new List<SVMXC__Site__c>{techLocation, techMateLocation};
    	    	    	
    	technician.SVMXC__Inventory_Location__c = techLocation.Id;
    	techMate.SVMXC__Inventory_Location__c = techMateLocation.Id;
    	update new List<SVMXC__Service_Group_Members__c>{technician, techMate};
	}    
}