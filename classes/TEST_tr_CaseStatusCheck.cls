@isTest
    private class TEST_tr_CaseStatusCheck
    {
        
        static testMethod void myUnitTest() 
        {
            RecordType RT =[
                        Select Id,DeveloperName 
                        from RecordType 
                        where DeveloperName=:'OMA_Spine_Scientific_Exchange_Product' limit 1
                        ];
            Account a = new Account() ; 
            a.Name = 'TestAccountOMA'; 
            a.SAP_ID__c = '04556665'; 
            a.RecordTypeId = FinalConstants.sapAccountRecordTypeId ; 
            insert a;
             
            contact con=new contact(accountid=a.id,LastName='last');
			con.FirstName = 'test';
            con.Phone = '0714820303'; 
            con.Email ='test1@contact.com';
            con.Contact_Department__c = 'Diabetes Adult';
            con.Contact_Primary_Specialty__c = 'ENT';
            con.Affiliation_To_Account__c = 'Employee';
            con.Primary_Job_Title_vs__c = 'Manager';
            con.Contact_Gender__c ='Male';  
            insert con; 
            test.startTest();

                // This prevents the execution of the Trigger trgUpdateBUCaseRecorTypeField 
                // The current Test Class will Execute the Trigger tr_CaseStatusCheck which will perform many updates on Cases which will fire the trgUpdateBUCaseRecorTypeField  and so we run into a Too-Many-SOQL-QUery Error
                clsUtil.bDoNotExecute = true;   

                bl_CalculateBusinessHoursAges.blnUpdateCheck=false;
                Case cs=new Case(Status='New',accountid=a.id,contactid=con.id,Origin='Patient',Therapy_Picklist__c='testClassTh');
                insert cs;
                
                bl_CalculateBusinessHoursAges.blnUpdateCheck=true;
                cs.status='Closed';
                cs.Type='On Label';
                cs.OMA_Products__c='INFUSE, OMF';
                cs.RecordTypeId=RT.id;
                cs.Last_Status_Change__c=system.today();
                update cs;
                
                cs.status='New';
                //cs.Last_Status_Change__c=system.today();
                update cs;

                bl_tr_CaseTriggerActions.checkAfterUpdate=false;
                bl_CalculateBusinessHoursAges.blnUpdateCheck=false;
                cs.status = 'Acknowledged';
                cs.Last_Status_Change__c=system.today();
                update cs;

                bl_tr_CaseTriggerActions.checkAfterUpdate=false;
                bl_CalculateBusinessHoursAges.blnUpdateCheck=false;
                
                cs.Last_Status_Change__c=system.today();
                cs.status='Closed';
                update cs;
            
            test.stopTest(); 
                
                cs.status = 'New';
                cs.Last_Status_Change__c=system.today();
                update cs;
                
                bl_tr_CaseTriggerActions.checkAfterUpdate=false;
                bl_CalculateBusinessHoursAges.blnUpdateCheck=false;
                
                cs.Last_Status_Change__c=system.today();
                cs.status='Question Confirmed';
                
                update cs;

        }
    }