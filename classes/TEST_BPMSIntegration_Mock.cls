global class TEST_BPMSIntegration_Mock implements HttpCalloutMock{

    global HTTPResponse respond(HTTPRequest oHTTPRequest){

		String tBody = '<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body><ser-root:provisionOnboardingResponse xmlns:ser-root="http://integration.medtronic.com/learning/onboarding/1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
		tBody += '<ns2:provisionOnboardingResponse xmlns:ns2="http://integration.medtronic.com/learning/onboarding/provisionOnboardingResponse/1/">';
		tBody += '<correlationId>997FEEB2-6134-4448-94D4-E9CCA461B4C6</correlationId>';
		tBody += '<returnCode>200</returnCode>';
		tBody += '<returnMessage>Successfully initiated Onboarding Process</returnMessage>';
		tBody += '</ns2:provisionOnboardingResponse>';
		tBody += '</ser-root:provisionOnboardingResponse></soapenv:Body></soapenv:Envelope>';

        HttpResponse oHTTPResponse = new HttpResponse();
			oHTTPResponse.setHeader('Content-Type', 'application/json');       
			oHTTPResponse.setBody(tBody);
			oHTTPResponse.setStatusCode(200);
        return oHTTPResponse;

	}

}