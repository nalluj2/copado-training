@isTest 
private class Test_DailyUnitTestRun {
	
	private static testmethod void testRunUnitTests(){
						
		Test.startTest();
		
		System.runAs(new User(Id = UserInfo.getUserId())){
		
			Unit_Test_Sandbox__c sandbox = new Unit_Test_Sandbox__c();
			sandbox.name = 'DEVCORE';
			sandbox.Email_Address__c = 'Test@medtronic.com';
			insert sandbox;
		}
			
		sc_Run_All_Internal_Tests scheduledClass = new sc_Run_All_Internal_Tests();			
		scheduledClass.execute(null);
			
		Test.setMock(HttpCalloutMock.class, new Test_DailyUnitTestRun_Mock());
		
		Test.stopTest();
	}
	
	private static testmethod void testCheckUnitTestResults(){
								
		Test.startTest();
		
		System.runAs(new User(Id = UserInfo.getUserId())){
			
			Unit_Test_Sandbox__c sandbox = new Unit_Test_Sandbox__c();
			sandbox.name = 'DEVCORE';
			sandbox.Email_Address__c = 'Test@medtronic.com';
			insert sandbox;
		}
		
		sc_Check_UnitTest_Run_Results scheduledClass = new sc_Check_UnitTest_Run_Results();
		
		ApexTestRunResult lastRun = new ApexTestRunResult();				
		bl_Check_UnitTest_Run_Results.lastRun = new List<ApexTestRunResult>{lastRun};		
		
		ApexTestResult testResultPassed = new ApexTestResult();		
		testResultPassed.MethodName = 'TestMethod';
		testResultPassed.ApexClass = new ApexClass(Name = 'Test_UnitTest');		
		testResultPassed.Outcome = 'Pass';	
		
		ApexTestResult testResultFailed = new ApexTestResult();		
		testResultFailed.MethodName = 'TestMethod';
		testResultFailed.ApexClass = new ApexClass(Name = 'Test_UnitTest');	
		testResultFailed.Message = 'Method failed.';
		testResultFailed.StackTrace = 'In first line.';
		testResultFailed.Outcome = 'Fail';	
				
		bl_Check_UnitTest_Run_Results.testResults = new List<ApexTestResult>{testResultPassed, testResultFailed};	
		
		scheduledClass.execute(null);
			
		Test.setMock(HttpCalloutMock.class, new Test_DailyUnitTestRun_Mock());
		
		Test.stopTest();
	}
}