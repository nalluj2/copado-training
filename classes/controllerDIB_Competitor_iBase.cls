public with sharing class controllerDIB_Competitor_iBase {
/**
 * Creation Date: 	20090922
 * Description: 	controllerDIB_Competitor_iBase 
 * 
 * Author: 	ABSI - Bob Ceuppens & Miguel Coimbra
 */	
	
	private DIB_Invoice_Line__c[] invoiceLinesThisMonth = new DIB_Invoice_Line__c[]{} ; 
	private List<DIB_Competitor_iBase__c> DIB_Comp_iBase;
	public List<DIB_Class_Competitor_iBase> DIB_Comp_Class_iBases {get;set;}

	
	public Account [] competitors {set;get;}
	
	public Integer PAGE_SIZE {get{return 25;} set;} 
	
	private DIB_Competitor_iBase__c[] storedDIB_Comp_iBases = new DIB_Competitor_iBase__c[]{}; 
	// To show or not the DataGrid of all the Line Items. 
	private Boolean canShowDG = false ; 
	
	// Period 
	public String periodChosen ; 	
	public String getPeriodChosen()	{							return this.periodChosen ;		}	
	public void setPeriodChosen(String s )	{					this.periodChosen = s;			}

	//Territories
	public String territoryFilter{set; get;}
	//Segments
	public String departmentSegment {set; get;}
	//Product Filter
	public String productFilter{set; get;}
	public String selectedProductFilter = null;
	
	public String searchCriteria {set; get;}
	
	
	// Period 2
	public String periodChosen2 ; 	
	public String getPeriodChosen2()	{							
		String s = this.periodChosen ;
		String result = DIB_AllocationSharedMethods.getFiscalMonthName(s.substring(0,2)) + ' / ' +  s.substring(3,7) ;  	
		return result ; 			
	}	
	
	// get the substring of the period wich will be the first 2 characters 
	public String getSelectedMonth(){
		String pickedPeriod = getPeriodChosen();    	
		return pickedPeriod.substring(0, 2);	
	}
		
	// get the substring of year which will be the last 4 characters. 
	public String getSelectedYear(){
		String pickedPeriod = getPeriodChosen();    	
		return pickedPeriod.substring(3, 7); 	
	}

	private DIB_Department__c [] DIBDepartments = new DIB_Department__c[]{};
		
	public List<DIB_Competitor_iBase__c> getDIB_Comp_iBase() {					return DIB_Comp_iBase;								}
	
	public void setDIB_Comp_iBase(List<DIB_Competitor_iBase__c> DIB_Comp_iBaseIn) {	this.DIB_Comp_iBase = DIB_Comp_iBaseIn;		}
	
	
	private void setcanShowDG(Boolean b){										this.canShowDG = b;									}
	public Boolean getcanShowDG(){												return this.canShowDG; 								}
	
	// Page Iterator 
	public Integer pageNr {get;set;}
	
	public Integer NrOfItems{get;set;}
	
	public Integer totalPages{get;set;}
	
	//SubmitResult
	public String submitResult{get;set;}	

	
	
	/**
	 *	Controller  
	 */
	 
	public ApexPages.StandardSetController con {	
		get {
			con.setPageSize(PAGE_SIZE);
			return con; 
		}
		set;
	}
	
	
	
	/*
	 * Method called the first time the VF page is open. Action tag. 
	 */
	 
	public void Init(){
		// Clear submitResult
		submitResult = '';
		getPeriodItems();
		loadCompetitors();
		setcanShowDG(false);
		displayCompetitors();
		setcanShowDG(false);
	}	
	
	public void loadCompetitors(){
		if('Pumps'.equals(productFilter)){
			this.competitors = [Select a.Pumps__c, a.Name, a.Id, a.CGMs__c From Account a where a.RecordTypeId = :FinalConstants.ACCOUNT_COMPETITOR_RECORDTYPE_ID and a.Pumps__c = true and a.Account_Active__c = true order by a.Name];
		}else if('CGMS'.equals(productFilter)){
			this.competitors = [Select a.Pumps__c, a.Name, a.Id, a.CGMs__c From Account a where a.RecordTypeId = :FinalConstants.ACCOUNT_COMPETITOR_RECORDTYPE_ID and a.CGMs__c = true and a.Account_Active__c = true order by a.Name];
		}else{
			this.competitors = [Select a.Pumps__c, a.Name, a.Id, a.CGMs__c From Account a where a.RecordTypeId = :FinalConstants.ACCOUNT_COMPETITOR_RECORDTYPE_ID and (a.CGMs__c = true or a.Pumps__c = true) and a.Account_Active__c = true order by a.Name];
		}
		selectedProductFilter = productFilter;
		System.debug('####################### competitor size: ' + this.competitors.size() + '#######################');
	}
	
	/*
	 * Controller 
	 */
	 	
	public void manageCon(){		
		
		DIB_Comp_Class_iBases = new List<DIB_Class_Competitor_iBase>(); 	
		
		// Account Filter : 
		String currentUserSAPId = DIB_AllocationSharedMethods.getCurrentUserSAPId(System.Userinfo.getUserId());
		
		String searchCriteriaEntered = '%' + searchCriteria + '%';
		//Segment Filter to soql
		String departmentSegmentEntered = '%';
	    String dummyDepartmentSegments = null;  
		if(this.departmentSegment != null && this.departmentSegment != ''){
			departmentSegmentEntered = '%' + departmentSegment + '%'; 
			dummyDepartmentSegments = 'Unknown';
		}
		
		
		/*if (accountFilter == 'MyAccounts') {
			//if (dibILs[i].Sold_To__r.DIB_SAP_Sales_Rep_Id__c != currentUserSAPId) 
			this.Segments = [SELECT d.Department__c, d.Account__r.isSales_Force_Account__c, d.Account__c 
												FROM DIB_Department__c d
												WHERE d.Account__r.isSales_Force_Account__c = true	
												AND d.Account__r.DIB_SAP_Sales_Rep_Id__c =: currentUserSAPId
												AND d.Account__r.DIB_SAP_Sales_Rep_Id__c != null
												AND d.Account__r.Name like :searchCriteriaEntered
												limit 1000];
												
		}else{	// All Accounts
			this.Segments = [SELECT d.Department__c, d.Account__r.isSales_Force_Account__c, d.Account__c 
									FROM DIB_Department__c d
									WHERE d.Account__r.isSales_Force_Account__c = true	
									AND d.Account__r.Name like :searchCriteriaEntered		 									
									limit 1000];			
		}*/

		//Get filtered accounts for terrotory. (Return null if the filter is not used)
    	Set <Id> filteredAccountIds = DIB_AllocationSharedMethods.getFilteredAccounts(this.territoryFilter);
		if(filteredAccountIds == null){
			con = new ApexPages.StandardSetController(Database.getQueryLocator([Select d.Id, 
							d.Department_ID__r.Name, d.Department_ID__c, d.Department_Master_Name__c,
							d.Account__c, d.Account__r.Id, d.Account__r.isSales_Force_Account__c, d.Account__r.Name  
							From DIB_Department__c d 
							WHERE d.Account__r.isSales_Force_Account__c = true 
							AND d.Account__r.Name like :searchCriteriaEntered
							AND d.Account__r.Account_Active__c = true
							AND d.Active__c = true
							AND (d.DiB_Segment__c like :departmentSegmentEntered
								OR d.DiB_Segment__c = :dummyDepartmentSegments
								)
							order by d.Account__r.Name, d.Account__c, d.Department_ID__r.Name]));			
		} else {
			con = new ApexPages.StandardSetController(Database.getQueryLocator([Select d.Id, 
							d.Department_ID__r.Name, d.Department_ID__c, d.Department_Master_Name__c,
							d.Account__c, d.Account__r.Id, d.Account__r.isSales_Force_Account__c, d.Account__r.Name  
							From DIB_Department__c d 
							WHERE d.Account__r.isSales_Force_Account__c = true 
							AND d.Account__r.Account_Active__c = true 
							AND d.Account__c in: filteredAccountIds 
							AND d.Account__r.Name like :searchCriteriaEntered
							AND d.Active__c = true
							AND (d.DiB_Segment__c like :departmentSegmentEntered
								OR d.DiB_Segment__c = :dummyDepartmentSegments
								)
							order by d.Account__r.Name, d.Account__c, d.Department_ID__r.Name]));			
		}
		

		
		// If no results, do not show the main table. 
		if (con.getResultSize() == 0){		 			
			setcanShowDG(false);			
		}else{
			setcanShowDG(true);
		}
						
		this.NrOfItems = con.getResultSize()	;
		
		// Just to be able to know how many pages are available : 
		con.last(); 
		this.totalPages = con.getPageNumber();
		// ***  
					
	}
	

	
	/**
	 *	Section : Period : 
	 */
	 
	 public List<SelectOption> getPeriodItems() {	 	
		if (periodChosen == null) {
		 	String paramChosenValue = Apexpages.currentPage().getParameters().get(FinalConstants.PARAM_DEFAULT_CHOSEN_PERIOD);
		 	if(paramChosenValue != null && paramChosenValue != ''){
		 		this.periodChosen = paramChosenValue;
		 		System.debug('#################### Default Period: ' + this.periodChosen + ', ' + DIB_AllocationSharedMethods.getCurrentPeriod());
		 	}else{	 	
				this.periodChosen = DIB_AllocationSharedMethods.getCurrentPeriod();
		 	}
		}
		return DIB_AllocationSharedMethods.getPeriodLines() ;  
	}
	
	// Show or Hide the button Submit Depeding on the selected Fiscal Period 
	private Boolean showSubmitButton; 
	public Boolean getshowSubmitButton(){					return this.showSubmitButton ; 	}
	public void setshowSubmitButton(Boolean b){				this.showSubmitButton = b;	 	}	
	
	public void checkShowHideSubmitButton(){
		System.debug('######################## showHideSubmitButton call method : ' ) ; 
		setshowSubmitButton(DIB_Manage_Task_Status.showHideSubmitButton(getSelectedMonth(), getSelectedYear())) ; 
	}
	
	/**
	 *	Section : Main Table Functionnalities  
	 */
	
	private void loadComp_iBase(){		
		this.DIBDepartments = (List<DIB_Department__c>) con.getRecords();		
	 	// Create a matrix of new Competitors iBase data	 	
	 	drawComp_iBase();
	}
		
	private void drawComp_iBase(){

		// Clear submitResult
		submitResult = '';
		
		//Get the chosen period and extract the selected Month & the selected Year : 
		String selMonth = getSelectedMonth() ; 
		String selYear = getSelectedYear();
		// get the chosen Department ; 		
		
		system.debug('################### drawComp_iBase_month : ' +  selMonth + '############## sel Year:  ' +  selYear) ; 
		
		storedDIB_Comp_iBases = [Select d.Current_IB__c,Competitor_Name__c, d.Competitor_Account_ID__r.Id,d.Competitor_Account_ID__r.Name,d.Competitor_Account_ID__c,
										d.Sales_Force_Account__c, d.Sales_Force_Account__r.Name, d.Id, d.Fiscal_year__c, d.Fiscal_Month__c,
										d.IB_Change__c, d.Account_Segmentation__c, d.SYSTEM_FIELD_Month_IB__c,d.System_Field_Comp_Name__c
												FROM DIB_Competitor_iBase__c d 
												WHERE d.Fiscal_Month__c  =: selMonth 
												AND d.Fiscal_Year__c =: selYear 
												AND d.Account_Segmentation__c in: this.DIBDepartments
												AND d.Type__c = :this.selectedProductFilter 
												order by  Sales_Force_Account__c, Competitor_Name__c];
										
		//Get the stored Competitors iBase data
		//	1 step : Create an empty matrix with all the values. 
												
		for (DIB_Department__c  DIBDepartment:DIBDepartments) {
			Account currentAccount = DIBDepartment.Account__r;
			System.debug(' ################## ' + 'controllerDIB_Competitor_iBase loadComp_iBase currentAccount: ' + currentAccount + ' ################');
			new List<DIB_Competitor_iBase__c>();
			DIB_Class_Competitor_iBase curDIB_Class_Competitor_iBase = new DIB_Class_Competitor_iBase(currentAccount);			
			curDIB_Class_Competitor_iBase.dibDepartment = DIBDepartment;
			List<DIB_Competitor_iBase__c> tempDIB_Comp_iBase = curDIB_Class_Competitor_iBase.comp_iBase;
			
			for (Account currentCompetitor:this.competitors) {
				System.debug(' ################## ' + 'controllerDIB_Competitor_iBase loadComp_iBase currentCompetitor: ' + currentCompetitor + ' ################');
				// create a "dummy" Competitor Line ... as a default one. 
				DIB_Competitor_iBase__c currentComp_iBase = new DIB_Competitor_iBase__c();
				currentComp_iBase.Competitor_Account_ID__c = currentCompetitor.Id;
				currentComp_iBase.Sales_Force_Account__c = currentAccount.Id;
				currentComp_iBase.IB_Change__c = 0;
				currentComp_iBase.Fiscal_Month__c = selMonth ; 
				currentComp_iBase.Fiscal_year__c = selYear ; 
				currentComp_iBase.Current_IB__c = null ; 
				currentComp_iBase.SYSTEM_FIELD_Month_IB__c = null ;
				currentComp_iBase.Type__c = this.selectedProductFilter;
				// Used in the Roll Up summary fields. 
				currentComp_iBase.System_Field_Comp_Name__c = currentCompetitor.Name ; 
				// Master Detail RelationShip
				/*for (DIB_Department__c seg : this.Segments){
					if (seg.Account__c == currentAccount.Id){
						currentComp_iBase.Account_Segmentation__c = seg.Id ; 		
					}
				}*/
				currentComp_iBase.Account_Segmentation__c = DIBDepartment.Id;
				//currentComp_iBase.Department__c = DIBDepartment.Department_ID__r.Name;		
				System.debug(' ################## ' + 'controllerDIB_Competitor_iBase drawComp_iBase_ currentComp_iBase: ' + currentComp_iBase + ' ################');
				 
				tempDIB_Comp_iBase.add(currentComp_iBase);
			}
			
			DIB_Comp_Class_iBases.add(curDIB_Class_Competitor_iBase);			
		}
	
	
		System.debug(' ################## ' + 'controllerDIB_Competitor_iBase loadComp_iBase DIB_Comp_Class_iBases.size(): ' + DIB_Comp_Class_iBases.size() + ' ################');
		
		// 2 step : check if the different Matches of the matrix already exist or not. 
		
		for (DIB_Competitor_iBase__c storedComp_iBase:storedDIB_Comp_iBases) {
			DIB_Class_Competitor_iBase tempDIB_Class_Comp_iBase;
			for (Integer i=0;i< DIB_Comp_Class_iBases.size();i++) {
				tempDIB_Class_Comp_iBase = 	DIB_Comp_Class_iBases.get(i);
				if (tempDIB_Class_Comp_iBase.dibDepartment.Id == storedComp_iBase.Account_Segmentation__c) {
					System.debug(' ################## ' + 'controllerDIB_Competitor_iBase match_Sales_Force_Account__c: ' +  storedComp_iBase.Sales_Force_Account__c + ' ################');
					 List<DIB_Competitor_iBase__c> tempComp_iBases = tempDIB_Class_Comp_iBase.comp_iBase;
					 DIB_Competitor_iBase__c curDIB_Comp_iBase;
					 for (Integer j=0;j< tempComp_iBases.size();j++) {
					 	curDIB_Comp_iBase = tempComp_iBases.get(j);
					 	if (curDIB_Comp_iBase.Competitor_Account_ID__c == storedComp_iBase.Competitor_Account_ID__c) {
					 		System.debug(' ################## ' + 'controllerDIB_Competitor_iBase match_Competitor__c: ' + curDIB_Comp_iBase.Competitor_Account_ID__c + 'storedComp_iBase: ' + storedComp_iBase +  ' ################');
					 		curDIB_Comp_iBase=storedComp_iBase;
					 		curDIB_Comp_iBase.IB_Change__c=storedComp_iBase.IB_Change__c;
					 		curDIB_Comp_iBase.SYSTEM_FIELD_Month_IB__c = null ;
							System.debug(' ################## ' + 'controllerDIB_Competitor_iBase curDIB_Comp_iBase: ' + 	 +  ' ################');
					 	}
					 	tempComp_iBases.set(j,curDIB_Comp_iBase);
					 }
					 System.debug(' ################## ' + 'controllerDIB_Competitor_iBase tempComp_iBases: ' + tempComp_iBases +  ' ################');
					 tempDIB_Class_Comp_iBase.comp_iBase = tempComp_iBases;
				}
			}
		}
	
		// Calculate Total :
		calculateTotalCompetitors(); 
		
	}
	
	
	/**
	 *	Change Request : Calculate Current IB up to the selected month : 
	 *
	 */
	 
	private void calculateTotalCompetitors(){
		String selectedMonth = getSelectedMonth(); 
		String selectedYear = getSelectedYear(); 	
										
		for (DIB_Class_Competitor_iBase iBaseComps: this.DIB_Comp_Class_iBases) {
			
			Account a = iBaseComps.SFAccount ;
			DIB_Department__c d = iBaseComps.dibDepartment;
			
			String selectedYearMonth = selectedYear + selectedMonth;
			System.debug(' ################## ' + 'controllerDIB_Competitor_iBase selectedYearMonth ' + selectedYearMonth + ' ################');
			
			List<DIB_Competitor_iBase__c> lineComps= iBaseComps.comp_iBase;
			
			List<DIB_Competitor_iBase__c> existingComps = [Select d.Current_IB__c,d.SYSTEM_FIELD_Comp_Name__c, d.Competitor_Account_ID__c,d.Competitor_Name__c, d.Sales_Force_Account__c, 
										d.Id, d.Fiscal_year__c, d.Fiscal_Month__c, d.IB_Change__c, d.Account_Segmentation__c
										FROM DIB_Competitor_iBase__c d 
										WHERE d.Account_Segmentation__c = :d.Id										
										AND d.Fiscal_Year_Month__c <=: selectedYearMonth
										AND d.Type__c = :this.selectedProductFilter 
										];
			List<DIB_Department__c  > beginDepartmentValues = [Select d.Id, d.Department_ID__r.Name,
										d.Begin_MDT_Install_Base_Pump__c, d.Begin_MDT_Install_Base_CGMS__c,   
										
										d.Begin_Roche__c, d.Begin_Roche_IB_Pumps_FM__c ,d.Begin_Roche_IB_Pumps_FY__c,
										d.Begin_Roche_IB_CGMS__c, d.Begin_Roche_IB_CGMS_FM__c, d.Begin_Roche_IB_CGMS_FY__c,
										
										d.Begin_Animas_Install_Base_Pumps__c,d.Begin_Animas_IB_Pumps_FM__c,d.Begin_Animas_IB_Pumps_FY__c,
										
										d.Begin_Other_Install_Base_Pumps__c, d.Begin_Other_IB_Pumps_FM__c, d.Begin_Other_IB_Pumps_FY__c,
										d.Begin_Other_IB_CGMS__c, d.Begin_Other_IB_CGMS_FM__c, d.Begin_Other_IB_CGMS_FY__c, 
										
										d.Begin_Patch_Pump_Installed_Base__c, d.Begin_Patch_Pump_FM__c, d.Begin_Patch_Pump_FY__c,
										
										d.Begin_Abott_IB_CGMS__c, d.Begin_Abott_IB_CGMS_FM__c, d.Begin_Abott_IB_CGMS_FY__c,
										
										d.Begin_Dexcom_IB_CGMS__c, d.Begin_Dexcom_IB_CGMS_FM__c, d.Begin_Dexcom_IB_CGMS_FY__c
										
										From DIB_Department__c d
										where d.Id =:d.Id];
																				
			if (beginDepartmentValues.size() == 0) {
				continue;
			}
			System.debug(' ################## ' + 'controllerDIB_Competitor_iBase existingComps.size ' + existingComps.size() + ' lineComps.size ' + lineComps.size() + ' ################');
			
			for (DIB_Competitor_iBase__c currComp : lineComps ){
				//if comp.IB_Change__c > 0 )
				Double total = 0 ; 
				for (DIB_Competitor_iBase__c othComp: existingComps){
					if ( currComp.Competitor_Account_ID__c == othComp.Competitor_Account_ID__c){						 
							if (othComp.IB_Change__c != null) {						
								total += othComp.IB_Change__c ;
								System.debug(' ################## ' + 'controllerDIB_Competitor_iBase othComp ' + othComp + ' total: ' + total + ' ################');
							}
					}						
				}
				if (currComp.SYSTEM_FIELD_Comp_Name__c != null) {
					System.debug(' ################## ' + 'controllerDIB_Competitor_iBase calculateTotalCompetitors_currComp ' + currComp +  ' ################');

					if (currComp.SYSTEM_FIELD_Comp_Name__c.equals('Roche')) {
						for (DIB_Department__c myBeginDepartmentValue : beginDepartmentValues) {
							if (currComp.Account_Segmentation__c == myBeginDepartmentValue.Id) {
								total = calculateTotal(total, 
									myBeginDepartmentValue.Begin_Roche__c, myBeginDepartmentValue.Begin_Roche_IB_Pumps_FM__c ,myBeginDepartmentValue.Begin_Roche_IB_Pumps_FY__c,
									myBeginDepartmentValue.Begin_Roche_IB_CGMS__c, myBeginDepartmentValue.Begin_Roche_IB_CGMS_FM__c, myBeginDepartmentValue.Begin_Roche_IB_CGMS_FY__c);	
								System.debug(' ################## ' + 'controllerDIB_Competitor_iBase beginDepartmentValues.get(0) ' + beginDepartmentValues.get(0) + ' total: ' + total + ' ################');
							}
						}
					}
					if (currComp.SYSTEM_FIELD_Comp_Name__c.equals('Animas')) {
						for (DIB_Department__c myBeginDepartmentValue : beginDepartmentValues) {	
							if (currComp.Account_Segmentation__c == myBeginDepartmentValue.Id) {
								total = calculateTotal(total,
										myBeginDepartmentValue.Begin_Animas_Install_Base_Pumps__c,myBeginDepartmentValue.Begin_Animas_IB_Pumps_FM__c,myBeginDepartmentValue.Begin_Animas_IB_Pumps_FY__c);					
								System.debug(' ################## ' + 'controllerDIB_Competitor_iBase beginDepartmentValues.get(0) ' + beginDepartmentValues.get(0) + ' total: ' + total + ' ################');
							}
						}
					}
					if (currComp.SYSTEM_FIELD_Comp_Name__c.equals('Other')) {
						for (DIB_Department__c myBeginDepartmentValue : beginDepartmentValues) {
							if (currComp.Account_Segmentation__c == myBeginDepartmentValue.Id) {
								total = calculateTotal(total,
										myBeginDepartmentValue.Begin_Other_Install_Base_Pumps__c,myBeginDepartmentValue.Begin_Other_IB_Pumps_FM__c,myBeginDepartmentValue.Begin_Other_IB_Pumps_FY__c,
										myBeginDepartmentValue.Begin_Other_IB_CGMS__c,myBeginDepartmentValue.Begin_Other_IB_CGMS_FM__c,myBeginDepartmentValue.Begin_Other_IB_CGMS_FY__c);	
				
								System.debug(' ################## ' + 'controllerDIB_Competitor_iBase beginDepartmentValues.get(0) ' + beginDepartmentValues.get(0) + ' total: ' + total + ' ################');
							}
						}
					}				
					if (currComp.SYSTEM_FIELD_Comp_Name__c.equals('Patch')) {
						for (DIB_Department__c myBeginDepartmentValue : beginDepartmentValues) {
							if (currComp.Account_Segmentation__c == myBeginDepartmentValue.Id) {
								total = calculateTotal(total,
										myBeginDepartmentValue.Begin_Patch_Pump_Installed_Base__c, myBeginDepartmentValue.Begin_Patch_Pump_FM__c, myBeginDepartmentValue.Begin_Patch_Pump_FY__c);
								System.debug(' ################## ' + 'controllerDIB_Competitor_iBase beginDepartmentValues.get(0) ' + beginDepartmentValues.get(0) + ' total: ' + total + ' ################');
							}
						}
					}

					if (currComp.SYSTEM_FIELD_Comp_Name__c.equals('Dexcom')) {
						for (DIB_Department__c myBeginDepartmentValue : beginDepartmentValues) {
							if (currComp.Account_Segmentation__c == myBeginDepartmentValue.Id) {
								total = calculateTotal(total,
								myBeginDepartmentValue.Begin_Dexcom_IB_CGMS__c, myBeginDepartmentValue.Begin_Dexcom_IB_CGMS_FM__c, myBeginDepartmentValue.Begin_Dexcom_IB_CGMS_FY__c);
								System.debug(' ################## ' + 'controllerDIB_Competitor_iBase beginDepartmentValues.get(0) ' + beginDepartmentValues.get(0) + ' total: ' + total + ' ################');
							}
						}
					}
					if (currComp.SYSTEM_FIELD_Comp_Name__c.equals('Abott')) {
						for (DIB_Department__c myBeginDepartmentValue : beginDepartmentValues) {
							if (currComp.Account_Segmentation__c == myBeginDepartmentValue.Id) {
								total = calculateTotal(total,
									myBeginDepartmentValue.Begin_Abott_IB_CGMS__c, myBeginDepartmentValue.Begin_Abott_IB_CGMS_FM__c, myBeginDepartmentValue.Begin_Abott_IB_CGMS_FY__c);
								System.debug(' ################## ' + 'controllerDIB_Competitor_iBase beginDepartmentValues.get(0) ' + beginDepartmentValues.get(0) + ' total: ' + total + ' ################');
							}
						}
					}										
				}
				System.debug('############## Calculated Total : ' + currComp.Current_IB__c + ', ' + total);
				currComp.Current_IB__c = total ;
			}
			
		}
	}
	/**
	* Calculate the start value
	*/
	private Double calculateTotal(Double total, Double startCountPumps, String startMonthPumps, String startYearPumps, 
									Double startCountCgms, String startMonthCgms, String startYearCgms){
		if('Pumps'.equals(productFilter)){
			return calculateTotal(total, startCountPumps, startMonthPumps, startYearPumps);
		}else{
			return calculateTotal(total, startCountCgms, startMonthCgms, startYearCgms);
		}
	}
	/**
	* Calculate the start value
	*/	
	private Double calculateTotal(Double total, Double startCount, String startMonth, String startYear){
		if(startCount != null && startMonth != null && startYear != null){
			String selectedYearMonth = getSelectedYear() + getSelectedMonth();
			String startYearMonth = startYear + startMonth;
			if(startYearMonth <= selectedYearMonth){
				total += startCount;
			}
		}
		return total;
	}
	
	/**
	 *  Section : Pagination ( First / Previous / Next / Last)
	 */ 
	// indicates whether there are more records after the current page set.
	public Boolean hasNext {
		get {
			return con.getHasNext();
		}
		set;
	}
 
	// indicates whether there are more records before the current page set.
	public Boolean hasPrevious {
		get {
			return this.con.getHasPrevious();
		}
		set;
	}

	// returns the page number of the current page set
	public Integer pageNumber {
		get {
			return this.con.getPageNumber();
		}
		set;
	}
 
	// PickList Selection of the Page
	
	public String SelectedPage{get;set;}	
	
	public List<SelectOption> pages {
		get{
			List<SelectOption> options = new List<SelectOption>();
			for (Integer i=1 ; i <= this.totalPages ; i++){
		 	 	options.add(new SelectOption(''+i,'' + i));
			}
 	 		return options;
		}
		set;
  	}
  	
  	public void changePage(){
  		manageCon();
 		pageNr = Integer.valueOf(this.selectedPage) ; 
 		con.setPageNumber(pageNr); 	
 		loadComp_iBase(); 
  	}
 	
	// returns the first page of records
 	public void first() {
 		manageCon();
 		this.con.first();
 		pageNr = 1 ; 
 		SelectedPage = String.valueOf(pageNr) ; 
 		loadComp_iBase();
 	}
 
 	// returns the last page of records
 	public void last() {
 		manageCon();
 		this.con.last();
 		pageNr = con.getPageNumber();
 		SelectedPage = String.valueOf(pageNr) ;
 		loadComp_iBase();
 	}
 
 	// returns the previous page of records
 	public void previous() {
 		manageCon();
 		pageNr-- ; 
 		con.setPageNumber(pageNr); 	
 		SelectedPage = String.valueOf(pageNr) ;	
 		loadComp_iBase(); 
 	}
 
 	// returns the next page of records
 	public void next() {
 		System.debug('################ next !!!!! button appuyé');
 		manageCon();
 		pageNr++ ; 
 		con.setPageNumber(pageNr); 	
 		SelectedPage = String.valueOf(pageNr) ;	
 		loadComp_iBase();
 	}
	
	/**
	 *	Section : Buttons  & Save Methods : 
	 */
	
	public void save(){
		// Clear submitResult
		submitResult = '';
		
		// Get all the items : 
		List<DIB_Competitor_iBase__c> all_Competitors_To_Upsert = new List<DIB_Competitor_iBase__c>{} ;
		
		// Manage the different Lines If they can be updated or not. And if can,
		// - Incremeent or decrement the current Install Base. 
		
		for (DIB_Class_Competitor_iBase tempDIB_Class_Comp_iBase:DIB_Comp_Class_iBases) {
			List<DIB_Competitor_iBase__c> lineComps= tempDIB_Class_Comp_iBase.comp_iBase;
			for (DIB_Competitor_iBase__c comp : lineComps ){
				// only upsert when the value is not empty 
				System.debug('########################### save DIB_Competitor_iBase__c_comp: ' + comp);
				if (comp.SYSTEM_FIELD_Month_IB__c != null){
					// ** new MC : 201002
					if ((comp.SYSTEM_FIELD_Month_IB__c + comp.Current_IB__c) < 0){
						comp.SYSTEM_FIELD_Month_IB__c.addError('IB cannot be negative.');
						// TO DO --> to be confirmed (MC)
						// Get all the Competitor Installed Base besides this one ( if it exists ) 
						// for the same account and the same Competitor 
						//  and if the Summe of all of them is bellow Zero : drop error.
					// **  
					} else {
						comp.IB_Change__c += comp.SYSTEM_FIELD_Month_IB__c ; 					
						comp.SYSTEM_FIELD_Month_IB__c = null ;
						comp.Current_IB__c = null ;
						all_Competitors_To_Upsert.add(comp);
					}
				}			
			}
		}
		try{
			System.debug('########################### Total Competitors to update : ' + all_Competitors_To_Upsert.size());
			upsert all_Competitors_To_Upsert ; 
			// Calcualte total : 
			calculateTotalCompetitors();
		}catch(DmlException e){
			System.debug('########################### Total Competitors to update error : ' + e);
			ApexPages.addMessages(e) ; 
		} 	
	}
	
	 // Submit button : Submit current Month & Year and all accounts for the current user : 
    public void SubmitcurrentPeriod(){
    	if (DIB_Manage_Task_Status.submitCompetitor(this.getSelectedMonth(), this.getSelectedYear())) {
    		submitResult = FinalConstants.DiB_submitResultSuccess;
    	}
    	else {
    		submitResult = FinalConstants.DiB_submitResultFailure;
    	}
    }
	
	public PageReference displayCompetitors()	{
		System.Debug('############ displayCompetitors') ;
		loadCompetitors();
		// Called each time The user clicks in the dysplay button : 
		first();		
		checkShowHideSubmitButton();
		return null;
	}	
	
   /**
   *  ############################# Section : Territory Filter : ##################################################### 
   */
	public List<SelectOption> getTerritoryFilterOptions() {
    	return DIB_AllocationSharedMethods.getTerritoryFilterOptions();
  	}

   /**
   *  ############################# Section : Segment Filter : ##################################################### 
   */
	public List<SelectOption> getSegmentFilterOptions() {
    	return DIB_AllocationSharedMethods.getSegmentFilterOptions();
  	}	
   /**
   *  ############################# Section : Pumps / CGMS : ##################################################### 
   */
	public List<SelectOption> getProductFilterOptions() {
		List<SelectOption> rtnList = new List<SelectOption>();
		//rtnList.add(new SelectOption('','Pumps / CGMS'));
		rtnList.add(new SelectOption('Pumps','Pumps'));
		rtnList.add(new SelectOption('CGMS','CGMS'));
    	return rtnList;
  	}		

}