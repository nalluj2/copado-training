public without sharing class ctrl_Person_Calendar {
	
	private static List<String> weekDays = new List<String>{'MON','TUE','WED','THU','FRI', 'SAT', 'SUN'};
	private static List<String> months = new List<String>{'January','February','March','April','May','June','July','August','September','October','November','December'};
	
	@AuraEnabled
    public static CalendarView getWeek(Date sampleDate, Id userId, Decimal height) {
        
        if(height < 450) height = 450;
        
        Integer userFirstDay; 
        Integer userStartHour; 
        Integer userEndHour;         
        Integer userLastDay;
        
        for(Implant_Scheduling_Team_Member__c userTeam : [Select Team__r.Work_Day_Start__c, Team__r.Work_Day_End__c, Team__r.Working_Days__c from Implant_Scheduling_Team_Member__c where Member__c = :userId]){
        	        	
        	Integer teamStartTime = Integer.valueOf(userTeam.Team__r.Work_Day_Start__c);
        	Integer teamEndTime = Integer.valueOf(userTeam.Team__r.Work_Day_End__c);
        	
        	if(userStartHour == null || teamStartTime < userStartHour) userStartHour = teamStartTime;
        	if(userEndHour == null || teamEndTime > userEndHour) userEndHour = teamEndTime;
        	        	
        	Integer teamFirstWeekDay = Integer.valueOf(userTeam.Team__r.Working_Days__c.split(':')[0]); 	
        	        	
        	if(teamFirstWeekDay == 7 || userFirstDay == null) userFirstDay = teamFirstWeekDay;
        	
        	Integer teamNumberWeekDays = Integer.valueOf(userTeam.Team__r.Working_Days__c.split(':')[1]);
        	Integer teamLastDay = teamFirstWeekDay == 7 ? teamNumberWeekDays - 1 : teamNumberWeekDays;
        	
        	if(userLastDay == null || teamLastDay > userLastDay) userLastDay = teamLastDay;        	
        }
        
        if(userFirstDay == null) userFirstDay = 1; 
        if(userStartHour == null) userStartHour = 8; 
        if(userEndHour == null) userEndHour = 18;         
        if(userLastDay == null) userLastDay = 5;
        
        if(sampleDate == null) sampleDate = Date.today();
                
        CalendarView result = new CalendarView();
        
        Time startTime = Time.newInstance(0, 0, 0, 0);
     	Time endTime = Time.newInstance(23, 59, 59, 0);
     	
     	DateTime startDate = DateTime.newInstance(sampleDate, startTime);
     	
     	Integer sampleDayOfWeek = Integer.valueOf(startDate.format('u'));        
                
        if(userFirstDay != sampleDayOfWeek){        
        	
        	if(userFirstDay == 7) startDate = startDate.addDays(-(sampleDayOfWeek));
        	else startDate = startDate.addDays(-(sampleDayOfWeek - 1));        	
        }        
     	     	
     	DateTime endDate = DateTime.newInstance(startDate.date().addDays(6), endTime);
        
        List<Event> eventList = [Select Subject, WhatId, Id, StartDateTime, EndDateTime, IsAllDayEvent from Event where OwnerId = :userId AND StartDateTime < :endDate AND EndDateTime >= :startDate AND isRecurrence = false ORDER BY StartDateTime];

        System.debug(eventList);        	
     	
     	Integer userMinWeekDays = userFirstDay == 7 ? (userLastDay + 1) : userLastDay;
     	if(userMinWeekDays > 7) userMinWeekDays = 7;
     	
     	Integer earliestStartHour = userStartHour;
        Integer latestEndHour = userEndHour;
        Date latestDay = startDate.addDays(userMinWeekDays - 1).date();
                     	     		
     	Set<Id> caseIds = new Set<Id>();
     	
     	for(Event event : eventList){
        	
        	if(event.WhatId != null && String.valueOf(event.whatId).startsWith('500')) caseIds.add(event.WhatId);        		
        }
        
        Map<Id, Case> cases = new Map<Id, Case>();
     	
     	if(caseIds.size() > 0){
     		
     		cases = new Map<Id, Case>([Select Id, Subject, Type, Procedure__c, AccountId, Account.Name, Product_Group__r.Name, Account.BillingCity, Activity_Type_Picklist__c, Activity_Scheduling_Team__r.Name, Activity_Scheduling_Team__r.Team_Color__c from Case where Id IN :caseIds]);
     	}
     	        
        for(Event event : eventList){
        	
        	if(event.WhatId != null && String.valueOf(event.whatId).startsWith('500') && cases.get(event.WhatId).Type != 'Meeting'){
        		
	        	Integer eventStart;
	        	Integer eventEnd;
	        	
	        	Integer eventDays = event.StartDateTime.date().daysBetween(event.EndDateTime.date());
	        	
	        	if(eventDays == 0){
	        	
		        	eventStart = event.StartDateTime.hour();
		        	eventEnd = event.EndDateTime.minute() > 0 ? event.EndDateTime.hour() + 1 : event.EndDateTime.hour();
		        	
	        	}else{
	        		
	        		if(event.StartDateTime.date() < endDate.date()) eventStart = 0;
	        		else eventStart = event.StartDateTime.hour();
	        			        		
	        		if(event.EndDateTime.date() > startDate.date()) eventEnd = 24;	        			
	        		else eventEnd = event.EndDateTime.minute() > 0 ? event.EndDateTime.hour() + 1 : event.EndDateTime.hour();
	        	}	
	        	
	        	if(eventStart < earliestStartHour) earliestStartHour = eventStart;
	        	if(eventEnd > latestEndHour) latestEndHour = eventEnd;	
	        	
	        	for(Integer i = 0; i <= eventDays; i++){
	        			        		
	        		DateTime day = event.StartDateTime.addDays(i);
	        		
	        		if(day >= startDate && day <= endDate){
	        				        			
	        			if(day.Date() > latestDay) latestDay = day.Date();        
	        		}
	        	}
        	}
        }
     	     	
        List<Hour> hours = new List<Hour>();
        for(Integer i = earliestStartHour; i < latestEndHour; i++){
        	
        	Hour hour = new Hour();
        	hour.hour = i;
                	
        	if(i < 8 || i >= 18) hour.isOWH = true;
        	else hour.isOWH = false;
        	
        	hours.add(hour);
        }  	
                
        Decimal pixelsPerMinute = height / ((latestEndHour - earliestStartHour) * 60);
        Integer pixelsPerHour = Integer.valueOf(pixelsPerMinute * 60);                
        result.hourHeight = pixelsPerHour;
        result.hours = hours;
        height = (latestEndHour - earliestStartHour) * pixelsPerHour;
        
        startTime = Time.newInstance(earliestStartHour, 0, 0, 0);
        if(latestEndHour == 24) endTime = Time.newInstance(23, 59, 59, 0);
        else endTime = Time.newInstance(latestEndHour, 0, 0, 0);
        
        DateTime sampleDateTime = DateTime.newInstance(startDate.date(), Time.newInstance(0,0,0,0));
                
        result.firstDay = startDate.date();
                
        Date todayDate = Date.today(); 
                
        List<Day> days = new List<Day>();
        
        Integer numberOfDays = startDate.date().daysBetween(latestDay) + 1;
        
        for(Integer i = 0; i < numberOfDays; i++){
        	
        	Day weekDay = new Day();
        	weekDay.dayKey = sampleDateTime.date().format();
        	
        	Integer dayOfWeek = Integer.valueOf(sampleDateTime.format('u'));
        	weekDay.dayName = weekDays[dayOfWeek - 1];
        	weekDay.day = sampleDateTime.day();
        	weekDay.month = sampleDateTime.month();
        	weekDay.year = sampleDateTime.year();
        	weekDay.isToday = sampleDateTime.date() == todayDate;
        	
        	if(sampleDateTime.Date() > startDate.addDays(userMinWeekDays - 1).Date()) weekDay.isOWH = true;
        	else weekDay.isOWH = false;
        	        	
        	days.add(weekDay);
        	
        	sampleDateTime = sampleDateTime.addDays(1);        	
        }     	
     	
     	result.days = days;
     	
     	Day firstDay = days[0];
     	Day lastDay = days[days.size() - 1];
     	
     	if(firstDay.month == lastDay.month){
     		
     		result.headerLabel = firstDay.day + ' - ' + lastDay.day + ' ' + months.get(firstDay.month - 1)  + ' ' + firstDay.year;
     		
     	}else if(firstDay.year == lastDay.year){
     		
     		result.headerLabel = firstDay.day + ' ' + months.get(firstDay.month - 1) + ' - ' + lastDay.day + ' ' + months.get(lastDay.month - 1)  + ' ' + firstDay.year;
     		
     	}else{
     		
     		result.headerLabel = firstDay.day + ' ' + months.get(firstDay.month - 1) + ' ' + firstDay.year + ' - ' + lastDay.day + ' ' + months.get(lastDay.month - 1) + ' ' + lastDay.year;
     	}
     	
     	Map<String, List<CalendarItem>> events = new Map<String, List<CalendarItem>>();
     	for(String dayName : weekDays){
     		events.put(dayName, new List<CalendarItem>());
     	}
     	
     	for(Event viewEvent : eventList){
     		
     		Case eventCase;
     		if(viewEvent.WhatId != null && String.valueOf(viewEvent.whatId).startsWith('500')) eventCase = cases.get(viewEvent.WhatId);
     		
     		if(viewEvent.IsAllDayEvent == true){
	     			
     			viewEvent.StartDateTime = DateTime.newInstance(viewEvent.StartDateTime.date(), Time.newInstance(0, 0, 0, 0));
 				viewEvent.EndDateTime = DateTime.newInstance(viewEvent.EndDateTime.date().addDays(1), Time.newInstance(0, 0, 0, 0));
     		}
     		
     		Integer eventDays = viewEvent.StartDateTime.date().daysBetween(viewEvent.EndDateTime.date());
     		
     		if(eventDays == 0){
	     		
	     		Integer dayOfWeek = Integer.valueOf(viewEvent.StartDateTime.format('u'));    	
	     		String dayName = weekDays[dayOfWeek - 1];
	     		
	     		List<CalendarItem> dayEvents = events.get(dayName);     		
	     		
	     		CalendarItem calItem = new CalendarItem();
	     		calItem.id = viewEvent.Id;	     		
	     		calItem.width = 96;	     		
	     			     		
	     		if(eventCase == null){
	     			
	     			calItem.subject = viewEvent.subject;
	     			calItem.teamName = 'Time-off';
	     			calItem.teamColor = 'rgb(' + ctrl_Activity_Scheduling_App.timeOffColor + ')';
	     			calItem.color = 'rgba(' + ctrl_Activity_Scheduling_App.timeOffColor + ', 0.8)';
	     			calItem.caseType = 'event';
	     				     			
	     		}
	     		else{
	     			
	     			calItem.caseId = viewEvent.WhatId;
	     			calItem.teamName = eventCase.Activity_Scheduling_Team__r.Name;
 					calItem.teamColor = eventCase.Activity_Scheduling_Team__r.Team_Color__c;
 					calItem.subject =  (eventCase.Type == 'Meeting' ? eventCase.Subject : eventCase.Account.Name);
 					calItem.accountId = eventCase.AccountId;
 					calItem.location = (eventCase.AccountId != null ? eventCase.Account.BillingCity : null);
 					     					
 					if(eventCase.Type == 'Implant Support Request'){
 						
 						calItem.color = 'rgba(' + ctrl_Activity_Scheduling_App.implantColor + ', 0.8)';
 						calItem.caseType = 'implant';
 						calItem.subject += ' / ' + eventCase.Procedure__c + ' / ' + eventCase.Product_Group__r.Name; 	
 						calItem.procedure = eventCase.Procedure__c;
 						     							
 					}else if(eventCase.Type == 'Service Request'){
 						
 						calItem.color = 'rgba(' + ctrl_Activity_Scheduling_App.serviceColor + ', 0.8)';
 						calItem.caseType = 'service';     		
 						calItem.subject += ' / ' + eventCase.Activity_Type_Picklist__c;
 						calItem.activityType = eventCase.Activity_Type_Picklist__c;
 											
 					}else{
 						
 						calItem.color = 'rgba(' + ctrl_Activity_Scheduling_App.meetingColor + ', 0.8)';
 						calItem.caseType = 'meeting';
 					}
	     		}
	     		
	     		calItem.timeframe = viewEvent.StartDateTime.format('H:mm') + ' - ' + viewEvent.EndDateTime.format('H:mm');
	     		
	     		Time start;
     			if(viewEvent.StartDateTime.time() < startTime) start = startTime;
     			else start = viewEvent.StartDateTime.Time();
	     		
	     		Decimal top = ((start.hour() - startTime.hour()) * pixelsPerHour) + ((start.minute() - startTime.minute()) * pixelsPerMinute);
     			calItem.top = Integer.valueOf(top);
	     			     		
	     		Time endT;
     			if(viewEvent.EndDateTime.time() > endTime) endT = endTime;
     			else endT = viewEvent.EndDateTime.Time();
	     		
	     		Decimal eventHeight = ((endT.hour() - start.hour()) * pixelsPerHour) + ((endT.minute() - start.minute()) * pixelsPerMinute) - 1;
     			calItem.height = Integer.valueOf(eventHeight);	     		
	     		     		
	     		calItem.zIndex = (dayEvents.size() + 1);
	     		
	     		if(calItem.height <= 0 ) continue;
	     		
	     		dayEvents.add(calItem);
	     		
     		}else{
     			
     			for(Integer i = 0; i <= eventDays; i++){
     				
     				CalendarItem calItem = new CalendarItem();
 					calItem.id = viewEvent.Id;     				
     				calItem.width = 96;
     				calItem.caseId = viewEvent.WhatId;
     				     				
     				if(eventCase == null){
	     			
		     			calItem.subject = viewEvent.subject;
		     			calItem.teamName = 'Time-off';
		     			calItem.teamColor = 'rgb(' + ctrl_Activity_Scheduling_App.timeOffColor + ')';
	     				calItem.color = 'rgba(' + ctrl_Activity_Scheduling_App.timeOffColor + ', 0.8)';
	     				calItem.caseType = 'event';
	     						     			
		     		}
		     		else{
		     			
		     			calItem.teamName = eventCase.Activity_Scheduling_Team__r.Name;
	 					calItem.teamColor = eventCase.Activity_Scheduling_Team__r.Team_Color__c;
	 					calItem.subject =  (eventCase.Type == 'Meeting' ? eventCase.Subject : eventCase.Account.Name);
	 					calItem.accountId = eventCase.AccountId;
	 					calItem.location = (eventCase.AccountId != null ? eventCase.Account.BillingCity : null);
	 					     					
	 					if(eventCase.Type == 'Implant Support Request'){
	 						
	 						calItem.color = 'rgba(' + ctrl_Activity_Scheduling_App.implantColor + ', 0.8)';
	 						calItem.caseType = 'implant';
	 						calItem.subject += ' / ' + eventCase.Procedure__c + ' / ' + eventCase.Product_Group__r.Name; 	
	 						calItem.procedure = eventCase.Procedure__c;
	 						     							
	 					}else if(eventCase.Type == 'Service Request'){
	 						
	 						calItem.color = 'rgba(' + ctrl_Activity_Scheduling_App.serviceColor + ', 0.8)';
	 						calItem.caseType = 'service';     		
	 						calItem.subject += ' / ' + eventCase.Activity_Type_Picklist__c;	
	 						calItem.activityType = eventCase.Activity_Type_Picklist__c;
	 										
	 					}else{
 					
 							calItem.color = 'rgba(' + ctrl_Activity_Scheduling_App.meetingColor + ', 0.8)';
 							calItem.caseType = 'meeting';
 						} 
		     		}
		     		
		     		calItem.timeframe = getTimeFrame(viewEvent.StartDateTime, viewEvent.EndDateTime);
     				     				     				    
					Integer dayOfWeek;
					Time start;
					Time endT;
					
     				if(i == 0){
     					
     					if(viewEvent.StartDateTime.date() < startDate.date()) continue;
     					
     					dayOfWeek = Integer.valueOf(viewEvent.StartDateTime.format('u'));
     					
		     			if(viewEvent.StartDateTime.time() < startTime) start = startTime;
		     			else start = viewEvent.StartDateTime.Time();
			     			     		
			     		endT = endTime;		     			
			     		
     				}else if(i == eventDays){
     					
     					if(viewEvent.EndDateTime.date() > endDate.date()) continue;
     					
     					dayOfWeek = Integer.valueOf(viewEvent.EndDateTime.format('u'));
     					
	     				start = startTime;
		     			
		     			if(viewEvent.EndDateTime.time() > endTime) endT = endTime;
		     			else endT = viewEvent.EndDateTime.Time();
			     		     					
     				}else{
     					
     					if(viewEvent.StartDateTime.date().addDays(i) < startDate.date() || viewEvent.StartDateTime.date().addDays(i) > endDate.date()) continue;
     					
     					dayOfWeek = Integer.valueOf(viewEvent.StartDateTime.addDays(i).format('u'));
     					
     					start = startTime;
     					endT = endTime;
     				}  
     				
     				String dayName = weekDays[dayOfWeek - 1];     						     		
     				List<CalendarItem> dayEvents = events.get(dayName);  
     				
     				Decimal top = ((start.hour() - startTime.hour()) * pixelsPerHour) + ((start.minute() - startTime.minute()) * pixelsPerMinute);
	     			calItem.top = Integer.valueOf(top);				
	     			
	     			Decimal eventHeight = ((endT.hour() - start.hour()) * pixelsPerHour) + ((endT.minute() - start.minute()) * pixelsPerMinute) - 1;
	     			calItem.height = Integer.valueOf(eventHeight);	
	     			
	     			calItem.zIndex = (dayEvents.size() + 1);     				
     				
     				if(calItem.height <= 0 ) continue;
     					     				
     				dayEvents.add(calItem);
     			}
     		}
     	}
     	
     	for(List<CalendarItem> dayItems : events.values()){
     		
     		if(dayItems.size() == 0) continue;
     		
     		if(hasOverlaps(dayItems)){
     		     		
	     		for(Integer i = 0; i < height; i++){
	     		
	     			List<CalendarItem> pixelItems = getOverlaps(i, dayItems);
	     		
	     			Integer maxOverlaps = pixelItems.size();
	     			
	     			for(CalendarItem item : pixelItems){
	     				
	     				if(item.maxOverlap == null || item.maxOverlap < maxOverlaps){
	     					
	     					item.maxOverlap = maxOverlaps;
	     				}
	     			}     		
	     		} 
	     		
	     		for(Integer i = 0; i < height; i++){
     		
	     			List<CalendarItem> pixelItems = getOverlaps(i, dayItems);
	     			
	     			Integer minLeft = 0;
	     			
	     			for(CalendarItem item : pixelItems){
	     				
	     				if(item.left == null){
	     					
	     					item.width = 96/item.maxOverlap;
	     					item.left = minLeft;
	     					
	     					minLeft = item.left + item.width;
	     					
	     				}else{
	     					     					     						
	     					if(minLeft == item.left){
	     							
	     						minLeft += item.width;
	     					}     					
	     				}
	     			}     		
     			}
	     		    		
     		}else{
     			
     			for(CalendarItem item : dayItems){
     				
     				item.left = 0;     			
     			}     			
     		}
     	}
     	     	
     	result.events = events;
     	
     	return result;   
    }
    
    private static String getTimeFrame(DateTime startDT, DateTime endDT){
    	
    	if(endDT.Year() != startDT.Year()){
    		
    		return startDT.format('dd MMM yyyy H:mm') + ' - ' + endDT.format('dd MMM yyyy H:mm');
    		
    	}else if(endDT.Month() != startDT.Month() || endDT.Day() != startDT.Day()){
    		
    		return startDT.format('dd MMM H:mm') + ' - ' + endDT.format('dd MMM H:mm');
    		
    	}else{
    		
    		return startDT.format('H:mm') + ' - ' + endDT.format('H:mm');
    	}
    }
    
    private static Boolean hasOverlaps(List<CalendarItem> items){
    	
    	for(Integer i = 0; i < items.size(); i++){
    		
    		CalendarItem item = items[i];
    		
    		for(Integer j = (i + 1); j < items.size(); j++){
    				
				CalendarItem otherItem = items[j];
    				
				if(otherItem.top <= (item.top + item.height) && (otherItem.top + otherItem.height) >= item.top) return true;
    		}	    		
    	}
    	
    	return false;
    }
    
    private static List<CalendarItem> getOverlaps(Integer pixel, List<CalendarItem> items){
    	
    	List<CalendarItem> pixelItems = new List<CalendarItem>();
    	
    	for(CalendarItem item : items){
    		
    		if(pixel >= item.top && pixel <= (item.top + item.height)){
    			
    			pixelItems.add(item);
    		}    		
    	}
    	
    	return pixelItems;
    }
        
    public class CalendarView {
    	
    	@AuraEnabled public String headerLabel {get; set;}
    	@AuraEnabled public Date firstDay {get; set;}
    	@AuraEnabled public Integer hourHeight {get; set;}
    	@AuraEnabled public List<Hour> hours {get; set;}
    	@AuraEnabled public List<Day> days {get; set;}
    	@AuraEnabled public Map<String, List<CalendarItem>> events {get; set;}
    }    
    
    public class Day {
    	
    	@AuraEnabled public String dayKey { get; set; }
    	@AuraEnabled public String dayName { get; set; }
    	@AuraEnabled public Integer day { get; set; }
    	@AuraEnabled public Integer month { get; set; }
    	@AuraEnabled public Integer year { get; set; }
    	@AuraEnabled public Boolean isToday { get; set; }
    	@AuraEnabled public Boolean isOWH { get; set; }
    }
    
    public class Hour {
    	
    	@AuraEnabled public Integer hour { get; set; }
    	@AuraEnabled public Boolean isOWH { get; set; }
    }
    
    public class CalendarItem {
    	
    	@AuraEnabled public String id { get; set; }
    	@AuraEnabled public String subject { get; set; }
    	@AuraEnabled public String timeframe { get; set; }
    	@AuraEnabled public String location { get; set; }
    	@AuraEnabled public Integer width { get; set; }
    	@AuraEnabled public Integer top { get; set; }
    	@AuraEnabled public Integer left { get; set; }
    	@AuraEnabled public Integer height { get; set; }
    	@AuraEnabled public Integer zIndex { get; set; }
    	@AuraEnabled public String color { get; set; }
    	@AuraEnabled public String teamName { get; set; }
    	@AuraEnabled public String teamColor { get; set; }
    	@AuraEnabled public Integer maxOverlap { get; set; }
    	@AuraEnabled public String caseId { get; set; }
    	@AuraEnabled public String caseType { get; set; }
    	@AuraEnabled public String accountId { get; set; }
    	@AuraEnabled public String procedure { get; set; }
    	@AuraEnabled public String activityType { get; set; }
    }
    
}