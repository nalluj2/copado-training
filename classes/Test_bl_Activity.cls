/*
 *      Created Date    : 20140811
 *      Author          : Bart Caelen
 *      Description     : TEST Class for the APEX Class bl_Activity
 */
@isTest private class Test_bl_Activity {
	

    private static List<Account_Plan_2__c> lstAccountPlan = new List<Account_Plan_2__c>();
    private static List<Account_Plan_Objective__c> lstAccountPlanObjective = new List<Account_Plan_Objective__c>();
    private static List<Task> lstTask   = new List<Task>();
    private static Task oTask1 = new Task();
    private static Task oTask2 = new Task();

    private static void createTestData_AccountPlan(){

        //---------------------------------------------------
        // CREATE TEST DATA
        //---------------------------------------------------
        clsTestData.createSubBusinessUnitData();
        clsTestData.createAccountData();
        
        RecordType eurDibSbu = [Select Id from RecordType where sObjectType = 'Account_Plan_2__c' AND DeveloperName = 'EUR_DIB_SBU'];

        lstAccountPlan = new List<Account_Plan_2__c>();
        Account_Plan_2__c oAccountPlan_1 = new Account_Plan_2__c();
            oAccountPlan_1.Account__c = clsTestData.oMain_Account.Id;
            oAccountPlan_1.Account_Plan_Level__c = 'Sub Business Unit';
            oAccountPlan_1.Sub_Business_Unit__c = [Select Id from Sub_Business_Units__c where Name = 'Brain Modulation' AND Business_Unit__r.Company__r.Name = 'Europe'].Id;
            oAccountPlan_1.Start_Date__c = Date.today();            
            oAccountPlan_1.RecordTypeId = eurDibSbu.Id;
        lstAccountPlan.add(oAccountPlan_1);

        Account_Plan_2__c oAccountPlan_2 = new Account_Plan_2__c();
            oAccountPlan_2.Account__c = clsTestData.oMain_Account.Id;
            oAccountPlan_2.Account_Plan_Level__c = 'Sub Business Unit';
            oAccountPlan_2.Sub_Business_Unit__c = [Select Id from Sub_Business_Units__c where Name = 'Coro + PV' AND Business_Unit__r.Company__r.Name = 'Europe'].Id;
            oAccountPlan_2.Start_Date__c = Date.today();            
            oAccountPlan_2.RecordTypeId = eurDibSbu.Id;
        lstAccountPlan.add(oAccountPlan_2);
        insert lstAccountPlan;

        lstAccountPlan = [SELECT Id FROM Account_Plan_2__c];

        lstTask = new List<Task>();
        
        clsTestData.iRecord_Task    = 5;

        clsTestData.oMain_Task      = null;
        clsTestData.idTask_What     = oAccountPlan_1.Id;
        clsTestData.tTask_Status    = 'New';
        clsTestData.createTaskData(false);
        lstTask.addAll(clsTestData.lstTask);
        oTask1 = clsTestData.oMain_Task;

        clsTestData.oMain_Task      = null;
        clsTestData.idTask_What     = oAccountPlan_1.Id;
        clsTestData.tTask_Status    = 'Completed';
        clsTestData.createTaskData(false);
        lstTask.addAll(clsTestData.lstTask);

        clsTestData.oMain_Task      = null;
        clsTestData.idTask_What     = oAccountPlan_2.Id;
        clsTestData.tTask_Status    = 'New';
        clsTestData.createTaskData(false);
        lstTask.addAll(clsTestData.lstTask);
        oTask2 = clsTestData.oMain_Task;

        clsTestData.oMain_Task      = null;
        clsTestData.idTask_What     = oAccountPlan_2.Id;
        clsTestData.tTask_Status    = 'Completed';
        clsTestData.createTaskData(false);
        lstTask.addAll(clsTestData.lstTask);

        insert lstTask;
        //---------------------------------------------------
    }

    private static void createTestData_AccountPlanObjective(){

        //---------------------------------------------------
        // CREATE TEST DATA
        //---------------------------------------------------
    	createTestData_AccountPlan();

        lstAccountPlanObjective = new List<Account_Plan_Objective__c>();
        for (Account_Plan_2__c oAccountPlan : lstAccountPlan){
            for (Integer i=0; i<1; i++){
                Account_Plan_Objective__c oAccountPlanObjective = new Account_Plan_Objective__c();
                    oAccountPlanObjective.Account_Plan__c = oAccountPlan.Id;
                    oAccountPlanObjective.Status__c = 'Opportunity';
                    oAccountPlanObjective.Target_Date__c = date.today().addDays(10);
                    oAccountPlanObjective.Description__c = 'TEST Account Plan Objective ' + i;
                lstAccountPlanObjective.add(oAccountPlanObjective);
            }
        }
        insert lstAccountPlanObjective;

        lstTask = new List<Task>();

        clsTestData.iRecord_Task    = 5;

        clsTestData.oMain_Task      = null;
        clsTestData.idTask_What     = lstAccountPlanObjective[0].Id;
        clsTestData.tTask_Status    = 'New';
        clsTestData.createTaskData(false);
        lstTask.addAll(clsTestData.lstTask);
        oTask1 = clsTestData.oMain_Task;

        clsTestData.oMain_Task      = null;
        clsTestData.idTask_What     = lstAccountPlanObjective[0].Id;
        clsTestData.tTask_Status    = 'Completed';
        clsTestData.createTaskData(false);
        lstTask.addAll(clsTestData.lstTask);

        clsTestData.oMain_Task      = null;
        clsTestData.idTask_What     = lstAccountPlanObjective[1].Id;
        clsTestData.tTask_Status    = 'New';
        clsTestData.createTaskData(false);
        lstTask.addAll(clsTestData.lstTask);
        oTask2 = clsTestData.oMain_Task;

        clsTestData.oMain_Task      = null;
        clsTestData.idTask_What     = lstAccountPlanObjective[1].Id;
        clsTestData.tTask_Status    = 'Completed';
        clsTestData.createTaskData(false);
        lstTask.addAll(clsTestData.lstTask);

        insert lstTask;        
        //---------------------------------------------------

    }

	@isTest static void test_bl_Activity_AccountPlan() {

		//---------------------------------------------------
		// CREATE TEST DATA
		//---------------------------------------------------
		createTestData_AccountPlan();
		//---------------------------------------------------

		lstTask = [SELECT Id, WhatID FROM Task WHERE WhatId = :lstAccountPlan];
		Map<Id, sObject> mapSObject_Old = new Map<Id, sObject>();
			mapSObject_Old.put(oTask1.Id, oTask2);

		//---------------------------------------------------

		Test.startTest();

		bl_Activity.updateParentWithRecordCountOfChildActivities(lstTask, mapSObject_Old);

		try{
			clsUtil.hasException = true;
			bl_Activity.updateParentWithRecordCountOfChildActivities(lstTask);
		}catch(Exception oEX){
		}

		Test.stopTest();

	}

	@isTest static void test_bl_Activity_AccountPlanObjective() {

		//---------------------------------------------------
		// CREATE TEST DATA
		//---------------------------------------------------
		createTestData_AccountPlanObjective();
		//---------------------------------------------------

		lstTask = [SELECT Id, WhatID FROM Task WHERE WhatId = :lstAccountPlanObjective];
		Map<Id, sObject> mapSObject_Old = new Map<Id, sObject>();
			mapSObject_Old.put(oTask1.Id, oTask2);

		//---------------------------------------------------

		Test.startTest();

		bl_Activity.updateParentWithRecordCountOfChildActivities(lstTask, mapSObject_Old);

		try{
			clsUtil.hasException = true;
			bl_Activity.updateParentWithRecordCountOfChildActivities(lstTask);
		}catch(Exception oEX){
		}

		Test.stopTest();

	}	

	/* New implementation is covered in Test_tr_Event_ImplantSupport
	@isTest static void test_implantScheduling_notification() {
		
		Account acc = new Account();
		acc.Name = 'Test Account';
		acc.Account_Country_vs__c = 'NETHERLANDS';
		insert acc;
		
		Case implCase = new Case();
		implCase.AccountId = acc.Id;		
		implCase.RecordTypeId = [Select Id from RecordType where SObjectType = 'Case' AND DeveloperName = 'Implant_Scheduling'].Id;			
		insert implCase;
			
		Event evnt = new Event();
		evnt.OwnerId = UserInfo.getUserId();
		evnt.WhatId = implCase.Id;			
		evnt.Type = 'Implant Support';
		evnt.StartDateTime = DateTime.now().addHours(4);
		evnt.EndDateTime = DateTime.now().addHours(7);			
		evnt.Subject = 'Implant support';
		evnt.Description = 'Test description';				
		insert evnt;	
	}
*/

    @isTest static void text_bl_Activity_ValidateActiveAccount(){

        User oUser_System = clsTestData_User.createUser_SystemAdministratorMDT('tadm01', true);
        User oUser = clsTestData_User.createUser('test.user.001@medtronic.com.test', 'tu001', 'NETHERLANDS', clsUtil.getUserProfileId('EUR Field Force RTG'), clsUtil.getUserRoleId('EUR xBU'), true);

        // Create Test Data
        Account oAccount_Active;
        Account oAccount_Disabled;
        System.runAs(oUser_System){
            List<Account> lstAccount = new List<Account>();
            oAccount_Active = new Account();
                oAccount_Active.Name = 'Test Account Active';
                oAccount_Active.Account_Country_vs__c = 'NETHERLANDS';
                oAccount_Active.SAP_Search_Term1__c = '';
            lstAccount.add(oAccount_Active);
            oAccount_Disabled = new Account();
                oAccount_Disabled.Name = 'Test Account Disabled';
                oAccount_Disabled.Account_Country_vs__c = 'NETHERLANDS';
                oAccount_Disabled.SAP_Search_Term1__c = 'DISABLED';
            lstAccount.add(oAccount_Disabled);
            insert lstAccount;
        }

        // Execute logic
        Test.startTest();

        Boolean bError = false;
        String tError = '';

        // INSERT TASK LINKED TO DISABLED ACCOUNT
        System.runAs(oUser){
            Task oTask_Disabled = new Task();
                oTask_Disabled.WhatId = oAccount_Disabled.Id;
                oTask_Disabled.Subject = 'TEST Task Disabeld';

            bError = false;
            tError = '';
            try{
                insert oTask_Disabled;
            }catch(Exception oErr){
                bError = true;
                tError = oErr.getMessage();
            }
            System.assertEquals(bError, true);
            System.assert(tError.contains('linked to a disabled Account'));
        }

        System.runAs(oUser_System){
            Task oTask_Disabled = new Task();
                oTask_Disabled.WhatId = oAccount_Disabled.Id;
                oTask_Disabled.Subject = 'TEST Task Disabeld';

            bError = false;
            tError = '';
            try{
                insert oTask_Disabled;
            }catch(Exception oErr){
                bError = true;
                tError = oErr.getMessage();
            }
            System.assertEquals(bError, false);
            System.assert(String.isBlank(tError));
        }


        // INSERT TASK LINKED TO ACTIVE ACCOUNT
        System.runAs(oUser){
            Account oAccount = new Account();
                oAccount.Name = 'Test Account';
                oAccount.Account_Country_vs__c = 'NETHERLANDS';
                oAccount.SAP_Search_Term1__c = '';
            insert oAccount;
            Task oTask_Active = new Task();
                oTask_Active.WhatId = oAccount.Id;
                oTask_Active.Subject = 'TEST Task Active';

            bError = false;
            tError = '';
            try{
                insert oTask_Active;
            }catch(Exception oErr){
                bError = true;
                tError = oErr.getMessage();
            }
            System.assertEquals(bError, false);
            System.assert(String.isBlank(tError));
        }


        // INSERT EVENT LINKED TO DISABLED ACCOUNT
        System.runAs(oUser){
            Event oEvent_Disabled = new Event();
                oEvent_Disabled.WhatId = oAccount_Disabled.Id;
                oEvent_Disabled.Type = 'Other';
                oEvent_Disabled.DurationInMinutes = 120;
                oEvent_Disabled.StartDateTime = DateTime.Now();
                oEvent_Disabled.EndDateTime = DateTime.Now().addHours(2);

            bError = false;
            tError = '';
            try{
                insert oEvent_Disabled;
            }catch(Exception oErr){
                bError = true;
                tError = oErr.getMessage();
            }
            System.assertEquals(bError, true);
            System.assert(tError.contains('linked to a disabled Account'));
        }

        System.runAs(oUser_System){
            Event oEvent_Disabled = new Event();
                oEvent_Disabled.WhatId = oAccount_Disabled.Id;
                oEvent_Disabled.Type = 'Other';
                oEvent_Disabled.DurationInMinutes = 120;
                oEvent_Disabled.StartDateTime = DateTime.Now();
                oEvent_Disabled.EndDateTime = DateTime.Now().addHours(2);

            bError = false;
            tError = '';
            try{
                insert oEvent_Disabled;
            }catch(Exception oErr){
                bError = true;
                tError = oErr.getMessage();
            }
            System.assertEquals(bError, false);
            System.assert(String.isBlank(tError));
        }


        // INSERT EVENT LINKED TO ACTIVE ACCOUNT
        System.runAs(oUser){
            Account oAccount = new Account();
                oAccount.Name = 'Test Account';
                oAccount.Account_Country_vs__c = 'NETHERLANDS';
                oAccount.SAP_Search_Term1__c = '';
            insert oAccount;
            Event oEvent_Active = new Event();
                oEvent_Active.WhatId = oAccount.Id;
                oEvent_Active.Type = 'Other';
                oEvent_Active.DurationInMinutes = 120;
                oEvent_Active.StartDateTime = DateTime.Now();
                oEvent_Active.EndDateTime = DateTime.Now().addHours(2);

            bError = false;
            tError = '';
            try{
                insert oEvent_Active;
            }catch(Exception oErr){
                bError = true;
                tError = oErr.getMessage();
            }
            System.assertEquals(bError, false);
            System.assert(String.isBlank(tError));
        }

        Test.stopTest();

    }

}