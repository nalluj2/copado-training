@isTest
private class Test_ba_CVGOpportunityReminder {
    
    private static testmethod void sendReminder(){
    	
		User oUser_Admin = clsTestData_User.createUser_SystemAdministratorMDT('tstadm1', false);
		User oUser = clsTestData_User.createUser('tstsusr1', clsUtil.getUserProfileId('EUR Field Force CVG'), clsUtil.getUserRoleId('UK FF (CVG)'), false);
		List<User> lstUser = new List<User>();
			lstUser.add(oUser_Admin);
			lstUser.add(oUser);
		insert lstUser;


		System.runAs(oUser_Admin){

    		Account acc = new Account();
    			acc.Name = 'Test Account';
    			acc.SAP_Id__c = '0123456789';
    		insert acc;

		
    	   
//    		User currentUser = new User(Id = UserInfo.getUserId());
//    		User crdmUser = [Select Id from User where Profile.Name = 'EUR Field Force CVG' AND isActive = true LIMIT 1];
    	    	
    		Opportunity opp = new Opportunity();
			opp.Name = 'Test opp';
			opp.AccountId = acc.Id;		
			opp.Sub_Business_Unit_Id__c = [Select Id from Sub_Business_Units__c].Id;
			opp.RecordTypeId = [Select Id from RecordType where SObjectType = 'Opportunity' AND DeveloperName = 'CVG_Project'].Id;
			opp.StageNAme = 'Closed won';
			opp.CloseDate = Date.today().addDays(30);
			opp.Project_End_Date__c = Date.today().addDays(30);
			opp.OwnerId = UserInfo.getUserId();
		
			Opportunity opp_inactiveOwner = new Opportunity();
			opp_inactiveOwner.Name = 'Test opp Inactive Owner';
			opp_inactiveOwner.AccountId = acc.Id;		
			opp_inactiveOwner.Sub_Business_Unit_Id__c = [Select Id from Sub_Business_Units__c].Id;
			opp_inactiveOwner.RecordTypeId = opp.RecordTypeId;
			opp_inactiveOwner.StageNAme = 'Closed won';
			opp_inactiveOwner.CloseDate = Date.today().addDays(30);
			opp_inactiveOwner.Project_End_Date__c = Date.today().addDays(30);
			opp_inactiveOwner.OwnerId = oUser.Id;
		
			insert new List<Opportunity>{opp, opp_inactiveOwner};
		
//		System.runAs(currentUser){
			
				oUser.isActive = false;
				oUser.User_Status__c = 'Left Company';
			update oUser;
		}
						
		Test.startTest();
				
		System.runAs(oUser_Admin){

			Database.executeBatch(new ba_CVGOpportunityReminder(), 100);

		}
		
		Test.stopTest();		
    }
    
    @testSetup
	private static void createData() {
	
		Company__c cmpny = new Company__c();		
        cmpny.name='Europe';
        cmpny.CurrencyIsoCode = 'EUR';
        cmpny.Current_day_in_Q1__c=56;
        cmpny.Current_day_in_Q2__c=34; 
        cmpny.Current_day_in_Q3__c=5; 
        cmpny.Current_day_in_Q4__c= 0;   
        cmpny.Current_day_in_year__c =200;
        cmpny.Days_in_Q1__c=56;  
        cmpny.Days_in_Q2__c=34;
        cmpny.Days_in_Q3__c=13;
        cmpny.Days_in_Q4__c=22;
        cmpny.Days_in_year__c =250;
        cmpny.Company_Code_Text__c = 'T35';
		insert cmpny;
		
		Business_Unit_Group__c bug =  new Business_Unit_Group__c();
        bug.Master_Data__c =cmpny.id;
        bug.name='CVG';      
        insert bug;
		
		Business_Unit__c bu =  new Business_Unit__c();
        bu.Company__c =cmpny.id;
        bu.name='CRHF';
        bu.Business_Unit_Group__c = bug.Id;
        bu.Account_Plan_Activities__c=true;        
        insert bu;
        
        Sub_Business_Units__c sbu = new Sub_Business_Units__c();
        sbu.name='Implantables & Diagnostic';
        sbu.Business_Unit__c=bu.id;
		insert sbu;	
		
		User currentUser = new User(Id = UserInfo.getUserId());
		currentUSer.Company_Code_Text__c = 'T35';
		update currentUser;	
		
		Email_Addresses__c emailSettings = Email_Addresses__c.getInstance('CVG Opportunity reminder');
		
		if(emailSettings == null){
			emailSettings = new Email_Addresses__c();
			emailSettings.name = 'CVG Opportunity reminder';
			emailSettings.Email__c = 'fake.email@medtronic.com';
			insert emailSettings;
		}
	}    
}