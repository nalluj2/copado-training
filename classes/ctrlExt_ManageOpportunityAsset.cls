//------------------------------------------------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 18-09-2019
//  Description      : APEX Controller Extension for VF Page addOpportunityAsset
//  Change Log       : 
//------------------------------------------------------------------------------------------------------------------------------------------------------------
public with sharing class ctrlExt_ManageOpportunityAsset {

	//--------------------------------------------------------------------------------------------------------------------------------------------------------
	// Private variables
	//--------------------------------------------------------------------------------------------------------------------------------------------------------
	private Map<Id, Asset> mapAsset;
	private List<Opportunity_Asset__c> lstAssetOpportunity_Insert = new List<Opportunity_Asset__c>();
	private List<Opportunity_Asset__c> lstAssetOpportunity_Delete = new List<Opportunity_Asset__c>();

    private Opportunity oOpportunity { get; set; }
	//--------------------------------------------------------------------------------------------------------------------------------------------------------


	//--------------------------------------------------------------------------------------------------------------------------------------------------------
	// Constructor
	//--------------------------------------------------------------------------------------------------------------------------------------------------------
    public ctrlExt_ManageOpportunityAsset(ApexPages.StandardController stdCtrl){
    	
		bRenderPage = true;

    	Id id_Opportunity = stdCtrl.getId();

    	if (id_Opportunity == null){
    	
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'No context Opportunity found'));
			bRenderPage = false;
    		return;

    	}

    	oOpportunity = [SELECT Id, AccountId, Account.Name, Account.SAP_ID__c FROM Opportunity WHERE Id = :id_Opportunity];

		initialize();

	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------------


	//--------------------------------------------------------------------------------------------------------------------------------------------------------
	// Private Methods
	//--------------------------------------------------------------------------------------------------------------------------------------------------------
	private void initialize(){

		lstAssetOpportunity_Insert = new List<Opportunity_Asset__c>();
		lstAssetOpportunity_Delete = new List<Opportunity_Asset__c>();

		bShowConfirm = false;

    	lstSO_BU = new List<SelectOption>();
    	lstSO_BU.add(new SelectOption('', '-- All --'));
    	for (AggregateResult oAR : [SELECT Product2.Business_Unit_ID__r.Name BusinessUnit FROM Asset WHERE AccountId = :oOpportunity.AccountId AND Product2.Business_Unit_ID__r.Business_Unit_Group__r.Name = 'Restorative' AND Product2.Business_Unit_ID__r.Name != null GROUP BY Product2.Business_Unit_ID__r.Name]){
    		
    		String tBU = (String)oAR.get('BusinessUnit');
    		lstSO_BU.add(new SelectOption(tBU, tBU));

    	}
    	lstSO_BU.sort();
    	
    	lstSO_ProductGroup = new List<SelectOption>();
    	lstSO_ProductGroup.add(new SelectOption('', '-- All --'));
    	for (AggregateResult oAR : [SELECT Product2.Product_Group__r.Name ProductGroup FROM Asset WHERE AccountId = :oOpportunity.AccountId AND Product2.Business_Unit_ID__r.Business_Unit_Group__r.Name = 'Restorative' AND Product2.Product_Group__r.Name != null GROUP BY Product2.Product_Group__r.Name]){
    		
    		String tProductGroup = (String)oAR.get('ProductGroup');
    		lstSO_ProductGroup.add(new SelectOption(tProductGroup, tProductGroup));

    	}
    	lstSO_ProductGroup.sort();

    	lstSO_Status = new List<SelectOption>();
    	for (AggregateResult oAR : [SELECT Status FROM Asset WHERE AccountId = :oOpportunity.AccountId AND Product2.Business_Unit_ID__r.Business_Unit_Group__r.Name = 'Restorative' AND Status != null GROUP BY Status]){
    		
    		String tStatus = (String)oAR.get('Status');
    		lstSO_Status.add(new SelectOption(tStatus, tStatus));

    	}
    	lstSO_Status.sort();
    	
    	lstFilter_Status = new List<String>();


	}


	private Map<String, String> getDIENCodeMapping(){
	
		Map<String, String> mapDIENCodeMapping = new Map<String, String>();

		for (Mapping_Asset_ServiceLevel_DIENCode__c oMapping : [SELECT CFN_Code__c, Service_Level__c, DIEN_Code__c FROM Mapping_Asset_ServiceLevel_DIENCode__c WHERE CFN_Code__c != null]){

			String tKey = oMapping.CFN_Code__c + '$$' + oMapping.Service_Level__c;
			mapDIENCodeMapping.put(tKey, oMapping.DIEN_Code__c);

		}

		return mapDIENCodeMapping;

	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------------


	//--------------------------------------------------------------------------------------------------------------------------------------------------------
	// Getters & Setters
	//--------------------------------------------------------------------------------------------------------------------------------------------------------
	public Boolean bRenderPage { get; private set; }

    public List<wrData> lstWRData { get; set;}
	public Boolean bEnableButton_ADD_DEL { 
		get{
			return (lstAssetOpportunity_Insert.size() > 0 || lstAssetOpportunity_Delete.size() > 0);
		}
	}
	public Boolean bShowConfirm { get; private set; }
    
    public Boolean bBulkSelected_ADD { get; set; }
    public Boolean bBulkSelected_DEL { get; set; }
    public Opportunity_Asset__c oBulkServiceLevel { get; set; }
    
    public String tFilter_BU { get; set; }
    public List<SelectOption> lstSO_BU { get; set; }
    
    public String tFilter_ProductGroup { get; set; }
    public List<SelectOption> lstSO_ProductGroup { get; set; }
    
    public List<String> lstFilter_Status { get; set; }
    public List<SelectOption> lstSO_Status { get; set; }

	public Integer iNum_ADD { 

		get{
			return lstAssetOpportunity_Insert.size();
		}

	}

	public Integer iNum_DEL { 

		get{
			return lstAssetOpportunity_Delete.size();
		}

	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------------
    

	//--------------------------------------------------------------------------------------------------------------------------------------------------------
	// Actions
	//--------------------------------------------------------------------------------------------------------------------------------------------------------
	public void loadAssetData(){
    	
    	Map<Id, Opportunity_Asset__c> mapAssetAddedToOpportunity = new Map<Id, Opportunity_Asset__c>();
    	
    	for (Opportunity_Asset__c oAssetOpportunity : [SELECT Id, Asset__c, Service_Level__c FROM Opportunity_Asset__c WHERE Opportunity__c = :oOpportunity.Id]){
    		
    		mapAssetAddedToOpportunity.put(oAssetOpportunity.Asset__c, oAssetOpportunity);

    	}
    	
    	lstWRData = new List<wrData>();
		lstAssetOpportunity_Insert = new List<Opportunity_Asset__c>();
		lstAssetOpportunity_Delete = new List<Opportunity_Asset__c>();

    	
    	String tSOQL = 'SELECT Id, Name, Serial_Nr__c, CFN_Text__c, Status, Product2Id, Product2.Business_Unit_ID__c, Product2.Product_Group__c';
		tSOQL += ' FROM Asset';
		tSOQL += ' WHERE AccountId = \'' + oOpportunity.AccountId + '\' AND Product2.Business_Unit_ID__r.Business_Unit_Group__r.Name = \'Restorative\'';
    	
    	if (String.isNotBlank(tFilter_BU)) tSOQL += ' AND Product2.Business_Unit_ID__r.Name = \'' + tFilter_BU + '\'';
    	if (String.isNotBlank(tFilter_ProductGroup)) tSOQL += ' AND Product2.Product_Group__r.Name = \'' + tFilter_ProductGroup + '\'';
    	if (lstFilter_Status.size() > 0) tSOQL += ' AND Status IN (\'' + String.join(lstFilter_Status, '\',\'') + '\')';
    	    	
    	tSOQL += ' ORDER BY Serial_Nr__c, CFN_Text__c, Name';
    	
		mapAsset = new Map<Id, Asset>();

    	for (Asset oAsset : Database.query(tSOQL)){
    		
			mapAsset.put(oAsset.Id, oAsset);

    		wrData oWRData= new wrData();
    		oWRData.oAsset = oAsset;
    		
    		if (mapAssetAddedToOpportunity.containsKey(oAsset.Id)){
    			
    			oWRData.bSelected_ADD = true;
    			oWRData.oAssetOpportunity = mapAssetAddedToOpportunity.get(oAsset.Id);
    			
    		}else{
    			
    			Opportunity_Asset__c oAssetOpportunity = new Opportunity_Asset__c();
    				oAssetOpportunity.Opportunity__c = oOpportunity.Id;
    				oAssetOpportunity.Asset__c = oAsset.Id;
    			oWRData.oAssetOpportunity = oAssetOpportunity;

    		}
    		
    		lstWRData.add(oWRData);
    	}
    	
    	bBulkSelected_ADD = false;
		bBulkSelected_DEL = false;
    	oBulkServiceLevel = new Opportunity_Asset__c();

    }


    public void selectionAddChanged(){

    	lstAssetOpportunity_Insert = new List<Opportunity_Asset__c>();
    	for (wrData oWRData : lstWRData){
    			
    		if (oWRData.bSelected_ADD == true && oWRData.oAssetOpportunity.Id == null){
				lstAssetOpportunity_Insert.add(oWRData.oAssetOpportunity);
			}

    	}
		
	}    

    public void bulkSelectedAddChange(){
    	
    	lstAssetOpportunity_Insert = new List<Opportunity_Asset__c>();

    	for (wrData oWRData : lstWRData){
    		
    		if (oWRData.oAssetOpportunity.Id == null){
				oWRData.bSelected_ADD = bBulkSelected_ADD;
				if (bBulkSelected_ADD){
					lstAssetOpportunity_Insert.add(oWRData.oAssetOpportunity);
				}
			}

    	}    	

    }


    public void selectionDelChanged(){

    	lstAssetOpportunity_Delete = new List<Opportunity_Asset__c>();
    	for (wrData oWRData : lstWRData){
    			
			if (oWRData.oAssetOpportunity.Id != null && oWRData.bSelected_DEL){
				lstAssetOpportunity_Delete.add(oWRData.oAssetOpportunity);
			}

    	}
		
	}

    public void bulkSelectedDelChange(){
    	
    	lstAssetOpportunity_Delete = new List<Opportunity_Asset__c>();

    	for (wrData oWRData : lstWRData){
    		
    		if (oWRData.oAssetOpportunity.Id != null){
				oWRData.bSelected_DEL = bBulkSelected_DEL;	
				if (bBulkSelected_DEL){
					lstAssetOpportunity_Delete.add(oWRData.oAssetOpportunity);
				}
			}

    	}    	

    }


    public void bulkServiceLevelChange(){
    	
    	for (wrData oWRData : lstWRData){
    		
    		if (oWRData.oAssetOpportunity.Id == null) oWRData.oAssetOpportunity.Service_Level__c = oBulkServiceLevel.Service_Level__c;	

    	}    	

    }


    public void filterBUChange(){
    	
    	lstSO_ProductGroup = new List<SelectOption>();
    	lstSO_ProductGroup.add(new SelectOption('', '-- All --'));
    	
    	String tSOQL = 'SELECT Product2.Product_Group__r.Name ProductGroup FROM Asset WHERE AccountId = \'' + oOpportunity.AccountId + '\' AND Product2.Business_Unit_ID__r.Business_Unit_Group__r.Name = \'Restorative\' AND Product2.Product_Group__r.Name != null';
    	if (String.isNotBlank(tFilter_BU)) tSOQL += ' AND Product2.Business_Unit_ID__r.Name = \'' + tFilter_BU + '\' ';
    	tSOQL += ' GROUP BY Product2.Product_Group__r.Name';
    	
    	for (AggregateResult oAR : Database.query(tSOQL)){
    		
    		String tProductGroup = (String)oAR.get('ProductGroup');
    		lstSO_ProductGroup.add(new SelectOption(tProductGroup, tProductGroup));

    	}
    	
    	lstSO_ProductGroup.sort();
    	
    	tFilter_ProductGroup = null;
    	
    	filterProductGroupChange();

    }

    
    public void filterProductGroupChange(){
    	
    	lstSO_Status = new List<SelectOption>();
    	
    	String tSOQL = 'SELECT Status FROM Asset WHERE AccountId = \'' + oOpportunity.AccountId + '\' AND Product2.Business_Unit_ID__r.Business_Unit_Group__r.Name = \'Restorative\' AND Status != null';
    	
    	if (String.isNotBlank(tFilter_BU)) tSOQL += ' AND Product2.Business_Unit_ID__r.Name = \'' + tFilter_BU + '\'';
    	if (String.isNotBlank(tFilter_ProductGroup)) tSOQL += ' AND Product2.Product_Group__r.Name = \'' + tFilter_ProductGroup + '\'';
    	
    	tSOQL += ' GROUP BY Status';

		for (AggregateResult oAR : Database.query(tSOQL)){
    		
    		String tStatus = (String)oAR.get('Status');
    		lstSO_Status.add(new SelectOption(tStatus, tStatus));

    	}
    	    	
    	lstSO_Status.sort();
    	
    	lstFilter_Status.clear();

    }


    public PageReference addDelAsset(){
    	
    	try{
    		
			if (Test.isRunningTest()) clsUtil.bubbleException('ctrlExt_AddOpportunityAsset.addDelAsset');
			Boolean bLoadData = false;

    		if (lstAssetOpportunity_Insert.size() > 0){

				Map<String, String> mapDIENCode = getDIENCodeMapping();
				for (Opportunity_Asset__c oAssetOpportunity : lstAssetOpportunity_Insert){

					if (mapAsset.containsKey(oAssetOpportunity.Asset__c)){

						String tKey = mapAsset.get(oAssetOpportunity.Asset__c).CFN_Text__c + '$$' + oAssetOpportunity.Service_Level__c;
						if (mapDIENCode.containsKey(tKey)) oAssetOpportunity.DIEN_Code__c = mapDIENCode.get(tKey);

					}

				}

				insert lstAssetOpportunity_Insert;
				bLoadData = true;

			}

			if (lstAssetOpportunity_Delete.size() > 0){

				delete lstAssetOpportunity_Delete;
				bLoadData = true;

			}

			if (bLoadData) loadAssetData();

    		
    	}catch(Exception e){
    		
    		ApexPages.addMessages(e);
    		return null;	
    	}
    	    	
    	return null;

    }
	//--------------------------------------------------------------------------------------------------------------------------------------------------------


	//--------------------------------------------------------------------------------------------------------------------------------------------------------
	// Wrapper Class
	//--------------------------------------------------------------------------------------------------------------------------------------------------------
    public class wrData{
    	
    	public Boolean bSelected_ADD { get; set; }
    	public Boolean bSelected_DEL { get; set; }
    	public Asset oAsset { get; set; }
    	public Opportunity_Asset__c oAssetOpportunity { get; set; }
    }
	//--------------------------------------------------------------------------------------------------------------------------------------------------------

}
//------------------------------------------------------------------------------------------------------------------------------------------------------------