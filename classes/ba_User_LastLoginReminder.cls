//------------------------------------------------------------------------------------------------------------------------------
//  Author      : Bart Caelen
//  Date        : 20150326
//  Description : CR-7212
//                  Batch APEX to inform users about their last login in SFDC
//------------------------------------------------------------------------------------------------------------------------------
//  Author      : Bart Caelen
//  Date        : 20150408
//  Description : CR-8360
//                  Batch APEX to inform users about their last login in SFDC using a Custom Setting to be more generic and flexible
//------------------------------------------------------------------------------------------------------------------------------
global class ba_User_LastLoginReminder implements Database.Batchable<sObject>, Database.Stateful, Schedulable {

    global Integer iBatchSize = 50;
    global List<User_Last_Login_Reminder__c> lstUserLastLoginReminder;
    global Integer iCounter = 0;
    global User_Last_Login_Reminder__c oUserLastLoginReminder;


    // SCHEDULE Settings
    global void execute(SchedulableContext sc){
        // Get Settings from Custom Setting
        List<User_Last_Login_Reminder__c> lstULLR = User_Last_Login_Reminder__c.getAll().values();
        if (lstULLR.size() > 0){
            // Execute the batch apex for the first Custom Setting record
            ba_User_LastLoginReminder oBatch = new ba_User_LastLoginReminder();
                oBatch.lstUserLastLoginReminder = lstULLR;
                oBatch.iCounter = 0;
            Database.executebatch(oBatch, iBatchSize);         
        }
    } 

    // CONSTRUCTORS
    global ba_User_LastLoginReminder(){
    }

    // BATCH Settings
    global Database.QueryLocator start(Database.BatchableContext BC) {

        oUserLastLoginReminder = lstUserLastLoginReminder[iCounter];
        clsUtil.debug('Processing ' + oUserLastLoginReminder.Name);

        Set<String> setUserLicence_Included = new Set<String>();
            setUserLicence_Included.add('Salesforce');

        if (oUserLastLoginReminder != null){
            Set<String> setCountryName = new Set<String>();
            if (oUserLastLoginReminder.Country_Name__c != null){
                setCountryName.addAll(oUserLastLoginReminder.Country_Name__c.split(';'));
            }

            Set<String> setProfileID = new Set<String>();
            if (oUserLastLoginReminder.Profile_ID__c != null){
                setProfileID.addAll(oUserLastLoginReminder.Profile_ID__c.split(';'));
            }

            String tSOQL = 
                'SELECT Id, Last_Application_Login__c, Country, Manager.Email'
                + ' FROM User'
                + ' WHERE IsActive = TRUE'
                + ' AND Last_Application_Login__c != null'
                + ' AND License_Name__c = :setUserLicence_Included';
            if (setCountryName.size() > 0){
                tSOQL += ' AND Country in :setCountryName';
            }                
            if (setProfileID.size() > 0){
                tSOQL += ' AND ProfileId in :setProfileID';
            }
            if (Test.isRunningTest()){
                tSOQL += ' LIMIT ' + String.valueOf(iBatchSize);
            }

            return Database.getQueryLocator(tSOQL);

        }else{
            return null;
        }
    }

    global void execute(Database.BatchableContext BC, List<User> lstUser) {

        if ( (lstUser != null) && (lstUser.size() > 0) && (oUserLastLoginReminder != null) ){
            bl_User.processUserLastLoginReminder(lstUser, oUserLastLoginReminder);
        }

    }
    
    global void finish(Database.BatchableContext BC) {

        iCounter++;        
        if (iCounter < lstUserLastLoginReminder.size()){
            ba_User_LastLoginReminder oBatch = new ba_User_LastLoginReminder();
                oBatch.lstUserLastLoginReminder = lstUserLastLoginReminder;
                oBatch.iCounter = iCounter; 
            if (!Test.isRunningTest()){
                Database.executebatch(oBatch, iBatchSize);         
            }
        }
    }

}
//------------------------------------------------------------------------------------------------------------------------------