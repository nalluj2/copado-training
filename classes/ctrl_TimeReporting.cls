public with sharing class ctrl_TimeReporting {
	
	public List<String> activityKeys {get; set;}
	public List<String> reportDays {get; set;}
	public Map<String, Time_Reporting__c> timeReportMap {get; set;}
	
	public Time_Reporting__c inputReport {get; set;}
	public String selectedReportId {get; set;}
	
	public List<CalendarWeek> calWeeks {get; set;}
	public List<String> weekDays {get; set;}
	
	public Date startViewWeek {get; set;}
	public Date endViewWeek {get; set;}
	public String selectedDay {get; set;}
	
	public List<SelectOption> months {get; set;}
	public String selectedMonth {get; set;}
	public List<SelectOption> years {get; set;}
	public String selectedYear {get; set;}
	
	public Time_Reporting__c contextUserHelper {get; set;}
	public User contextUser {get; set;}
			
	private Date startMonth {get; set;}
	private Date endMonth {get; set;}
	
	public Boolean isManager {get; set;}
	public Boolean kronosInputRequired {get; set;}
	public List<SelectOption> activityOptions {get; set;}
	
	public ctrl_TimeReporting(){
				
		loadCalOptions();	
		loadActivityOptions();
				
		contextUserHelper = new Time_Reporting__c();
		contextUserHelper.OwnerId = UserInfo.getUserId();
		contextUser = [Select Id, Name from User where Id =:UserInfo.getUserId()];
		
		setNewInputReport();
		
		loadTable();
		
		kronosInputRequired = false;
		isManager = true;
	}
	
	private void loadTable(){
											
		timeReportMap = new Map<String, Time_Reporting__c>();
		activityKeys = new List<String>();
		reportDays = new List<String>();
		
		for(Integer i=0; i<7; i++){
			reportDays.add(formatDate(startViewWeek.addDays(i)));
		}
		
		Set<String> keySet = new Set<String>();
		
		for(Time_Reporting__c report : [Select Id, OwnerId, Date__c, Hours__c, Project__c,
					Project__r.Project_Name__c, Change_Request__c, Change_Request__r.Change_Request__c, Release__c, 
					Release__r.Name, Reporting_Activity__c, Kronos_Category__c,
					Kronos_Project__c  from Time_Reporting__c where OwnerId=:contextUser.Id
					AND Date__c >=:startViewWeek AND Date__c <=:endViewWeek ORDER BY Date__c, CreatedDate ASC]){
			
			String key =  getReportKey(report);
			
			if(!keySet.contains(key)){
				
				activityKeys.add(key);
				keySet.add(key);
				
				for(Integer i=0; i<7; i++){
					
					Date selDay = startViewWeek.addDays(i);
					
					Time_Reporting__c templateReport = report.clone(false);
					templateReport.hours__c=null;
					templateReport.Date__c=selDay;
					
					timeReportMap.put(key+'-'+formatDate(selDay), templateReport);
				}				
			}
			
			timeReportMap.put(key+'-'+formatDate(report.Date__c), report);
		}
		
		loadCalendarData();	
	}
	
	private String getReportKey(Time_Reporting__c report){
		
		String key = report.Reporting_Activity__c;
		
		if(report.Release__c!=null) key+= '/'+report.Release__c;
		else if(report.Project__c!=null) key+= '/'+report.Project__c;
		else if(report.Change_Request__c!=null) key+= '/'+report.Change_Request__c;
				
		return key; 
	}
	
	private String formatDate(Date d){
		
		return getDayName(d)+' '+d.day()+'/'+d.month();
	}
	
	public void changeContextUser(){
		
		if(contextUserHelper.OwnerId==null) contextUserHelper.OwnerId = contextUser.Id;		
		contextUser = [Select Id, Name from User where Id =:contextUserHelper.OwnerId];
		
		loadTable();
		setNewInputReport();		
	}
	
	public void changeMonth(){
		
		Date firstDayOfMonth = Date.newInstance(Integer.valueOf(selectedYear), Integer.valueOf(selectedMonth), 1);
		
		startViewWeek = firstDayOfMonth.toStartOfWeek();
		endViewWeek = startViewWeek.addDays(6);
		
		loadTable();
		if(inputReport.Id!=null) setNewInputReport();
	}
	
	public void nextWeek(){
		
		startViewWeek = startViewWeek.addDays(7);
		endViewWeek = endViewWeek.addDays(7);
		
		checkNewWeekInCalendar();
						
		loadTable();
		if(inputReport.Id!=null) setNewInputReport();
	}
	
	public void prevWeek(){
		
		startViewWeek = startViewWeek.addDays(-7);
		endViewWeek = endViewWeek.addDays(-7);
		
		checkNewWeekInCalendar();
		
		loadTable();
		if(inputReport.Id!=null) setNewInputReport();
	}
	
	public PageReference toKronosView(){
		
		PageReference pr = new PageReference('/apex/Time_Reporting_Kronos_View');
		pr.getParameters().put('refDay', startViewWeek.format());
		pr.getParameters().put('userId', contextUser.Id);
		pr.setRedirect(true);
		
		return pr;
	}
	
	private void checkNewWeekInCalendar(){
		
		Integer selMonth = Integer.valueOf(selectedMonth);		
		if(startViewWeek.month()!= selMonth && endViewWeek.month()!=selMonth){
			
			selectedMonth = String.valueOf(startViewWeek.month());
			selectedYear = String.valueOf(startViewWeek.year());			
		}		
	}
	
	public void goToWeek(){
		
		Date goToDay = Date.parse(selectedDay);
		
		startViewWeek = goToDay.toStartOfWeek();
		endViewWeek = startViewWeek.addDays(6);
		
		loadTable();
		if(inputReport.Id!=null) setNewInputReport();
	}
	
	public void deleteReport(){
													
		List<Time_Reporting__c> toDelete = new List<Time_Reporting__c>();
		List<String> toDeleteKeys = new List<String>();	
		
		for(Integer i=0; i<7; i++){
			
			Date selDay = startViewWeek.addDays(i);			
			String key = selectedReportId+'-'+formatDate(selDay);
			toDeleteKeys.add(key);							
				
			Time_Reporting__c toDel = timeReportMap.get(key);
			if(toDel.Id!=null) toDelete.add(toDel);
		}			
		
		if(toDelete.size()>0){
			try{ 			
				delete toDelete;
			}catch(Exception e){
				ApexPages.addMessages(e); return;
			}
		}
			
		Integer keyIndex;
		Integer i=0;
		for(String key:activityKeys){
			
			if(key==selectedReportId){
				keyIndex = i;
				break;
			}
			
			i++;
		}
			
		if(keyIndex!=null) activityKeys.remove(keyIndex);
			
		for(String key : toDeleteKeys){
			
			timeReportMap.remove(key);
		}
		
		loadCalendarData();	
	}
	
	
	public void setNewInputReport(){
		
		inputReport	= new Time_Reporting__c();
		inputReport.Date__c = Date.today();
		inputReport.OwnerId = contextUser.Id;
		contextUserHelper.Date__c = null;
		
		kronosInputRequired = false;
	}
	
	public void updateReports(){
		
		try{
			
			List<Time_Reporting__c> toUpsert = new List<Time_Reporting__c>();
			List<Time_Reporting__c> toDelete = new List<Time_Reporting__c>();
			
			for(Time_Reporting__c report : timeReportMap.values()){
				
				if(report.hours__c!=null && report.hours__c>0){
					toUpsert.add(report);
				}else if(report.Id!=null){
					toDelete.add(report);
				}
			}		
			
			if(toDelete.size()>0) delete toDelete;
			if(toUpsert.size()>0) upsert toUpsert;
			
		}catch(Exception e){
			ApexPages.addMessages(e); return;
		}
		
		loadTable();
	}
	
	public void revertChanges(){
		
		loadTable();		
	}
	
	public void cloneWeek(){
		
		Map<String, Time_Reporting__c> prevActivities = new Map<String, Time_Reporting__c>();
		
		for(String key : activityKeys){
			
			Time_Reporting__c repTemplate = timeReportMap.get(key+'-'+reportDays[0]);
			repTemplate.hours__c = null;
			repTemplate.Date__c = null;
			prevActivities.put(key, repTemplate);
		}
		
		nextWeek();
		
		for(String key: prevActivities.keySet()){
			
			if(timeReportMap.get(key+'-'+reportDays[0])==null){
				
				activityKeys.add(key);
				
				Time_Reporting__c temp = prevActivities.get(key);		
			
				for(Integer i=0; i<7; i++){
					
					Date selDay = startViewWeek.addDays(i);
					
					Time_Reporting__c templateReport = temp.clone();			
					templateReport.Date__c=selDay;
					
					timeReportMap.put(key+'-'+formatDate(selDay), templateReport);
				}		
			}			
		}
	}
	
	public void addNewReport(){
		
		ensureKronosDataConsistency(inputReport);
		
		Boolean validRecord = true;
		Savepoint sp = Database.setSavepoint();
		
		try{
			Time_Reporting__c testReport = inputReport.clone();
			testReport.Date__c = startViewWeek;
			testReport.hours__c = 1;
			
			insert testReport;
		}catch(Exception e){
			ApexPages.addMessages(e);
			validRecord = false;
		}
		
		Database.rollback(sp);
		
		if(validRecord == false) return;
		
		String key =  getReportKey(inputReport);
		
		Set<String> keySet = new Set<String>(activityKeys);
		if(keySet.contains(key)){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, 'Activity already exists for current week')); return;
		}
		
		activityKeys.add(key);		
		
		if(inputReport.Change_Request__c!= null){
			Change_Request__c cr = [Select Change_Request__c from Change_Request__c where Id=:inputReport.Change_Request__c];
			inputReport.Change_Request__r = cr;
		}
		
		if(inputReport.Release__c != null){
			Project__c rel = [Select Name from Project__c where Id=:inputReport.Release__c];
			inputReport.Release__r = rel;
		}			
		
		if(inputReport.Project__c != null){
			Project_Management__c proj = [Select Project_Name__c from Project_Management__c where Id=:inputReport.Project__c];
			inputReport.Project__r = proj;
		}
			
		for(Integer i=0; i<7; i++){
			
			Date selDay = startViewWeek.addDays(i);
			
			Time_Reporting__c templateReport = inputReport.clone();			
			templateReport.Date__c=selDay;
			
			timeReportMap.put(key+'-'+formatDate(selDay), templateReport);
		}	
			
		setNewInputReport();		
	}
	
	private void ensureKronosDataConsistency(Time_Reporting__c report){
		
		if(kronosInputRequired){
			report.Project__c = null;
			report.Release__c = null;
			report.Change_request__c = null;
		}
	}
	
	public void onActivityChange(){
		
		kronosInputRequired = false;
		
		if(inputReport.Reporting_Activity__c!=null && String.valueOf(inputReport.Reporting_Activity__c)!=''){
			Reporting_Activity__c act = [Select Requires_manual_input__c from Reporting_Activity__c where Id=:inputReport.Reporting_Activity__c];		
			kronosInputRequired = act.Requires_manual_input__c;
		}
	}
	
	private void loadCalendarData(){
		
		startMonth = Date.newInstance(Integer.valueOf(selectedYear), Integer.valueOf(selectedMonth), 1);
		endMonth = startMonth.addMonths(1).addDays(-1);
		
		Date refMonday = Date.newInstance(1900, 1, 1);
		Date today = Date.today();
		
		Map<Integer, CalendarDay> daysInCalendar = new Map<Integer, CalendarDay>();
		calWeeks =  new List<CalendarWeek>();
		weekDays = new List<String>();
							
		Date firstDay = startMonth.toStartOfWeek();
		Date lastDay = endMonth.toStartOfWeek().addDays(6) ;
		
		Integer numberOfWeeks = (firstDay.daysBetween(lastDay)+1)/7;
		
		for(Integer i=0; i<7; i++){
			Date selDay = firstDay.addDays(i);
			weekDays.add(getDayName(selDay));
		}
		
		for(Integer i=0; i<numberOfWeeks; i++){
			
			CalendarWeek week = new CalendarWeek();
			
			for(Integer j=0; j<7; j++){
				
				Date selDay = firstDay.addDays((7*i)+j);
				
				CalendarDay day = new CalendarDay(selDay.day(), selDay.format());
				week.days.add(day);
				
				Integer weekDayNumber = Math.mod(refMonday.daysBetween(selDay), 7);
				if(weekDayNumber>4) day.isWeekend = true;
				
				if(selDay>=startMonth && selDay<=endMonth) day.inCurrentMonth = true;								
				if(selDay == today) day.isToday = true;
				if(selDay>=startViewWeek && selDay<=endViewWeek) day.isViewWeek = true;
				
				daysInCalendar.put(selDay.dayOfYear(), day);
			}
			
			calWeeks.add(week);
		}
		
		for(AggregateResult daySummary : [Select Date__c, sum(hours__c) totalHours from Time_reporting__c 
											where OwnerId=:contextUser.Id AND Date__c>=:firstDay 
											AND Date__c <=:lastDay GROUP BY Date__c ORDER BY Date__c ASC]){
			
			Date workingDay = (Date) daySummary.get('Date__c');
			CalendarDay calDay = daysInCalendar.get(workingDay.dayOfYear());
			calDay.hours =  Integer.valueOf(daySummary.get('totalHours'));											 	
		}		
	}
	
	private String getDayName(Date day){
		
		Datetime myDate = Datetime.newInstance(day.year(), day.month(), day.day());  
		return myDate.format('EEE');  
	}
	
	private void loadCalOptions(){
		
		months = new List<SelectOption>();
		months.add(new SelectOption('1', 'Jan'));
		months.add(new SelectOption('2', 'Feb'));
		months.add(new SelectOption('3', 'Mar'));
		months.add(new SelectOption('4', 'Apr'));
		months.add(new SelectOption('5', 'May'));
		months.add(new SelectOption('6', 'Jun'));
		months.add(new SelectOption('7', 'Jul'));
		months.add(new SelectOption('8', 'Aug'));
		months.add(new SelectOption('9', 'Sep'));
		months.add(new SelectOption('10', 'Oct'));
		months.add(new SelectOption('11', 'Nov'));
		months.add(new SelectOption('12', 'Dec'));
		
		Date tod = Date.today(); 
		Integer currYear = tod.year();
		
		years = new List<SelectOption>();
		years.add(new SelectOption(String.valueOf(currYear - 1), String.valueOf(currYear - 1) ));
		years.add(new SelectOption(String.valueOf(currYear), String.valueOf(currYear)));
		years.add(new SelectOption(String.valueOf(currYear + 1), String.valueOf(currYear + 1 )));
		
		selectedMonth = String.valueOf(tod.month());
		selectedYear = String.valueOf(tod.year());	
		
		startViewWeek = tod.toStartOfWeek();
		endViewWeek = startViewWeek.addDays(6);	
	}
	
	private void loadActivityOptions(){
		
		activityOptions = new List<SelectOption>();
		activityOptions.add(new SelectOption('', '--None--'));
		
		for(Reporting_activity__c act:[Select Id, Name from Reporting_Activity__c ORDER BY Name]){
			
			activityOptions.add(new SelectOption(act.Id, act.Name));			
		}		
	}
	
	private class CalendarWeek{
		
		public List<CalendarDay> days {get; set;}
		
		public CalendarWeek(){
			days = new List<CalendarDay>();
		}
	}
	
	private class CalendarDay{
		
		public String dateString {get; set;}
		public Integer dayNumber {get; set;}
		public Integer hours {get; set;}
		public Boolean isWeekend {get; set;}
		public Boolean isViewWeek {get; set;}
		public Boolean inCurrentMonth {get; set;}	
		public Boolean isToday {get; set;}	
		
		public CalendarDay(Integer dnumber, String formattedDate){
			
			this.dayNumber = dnumber;			
			this.hours = 0;
			this.isWeekend = false;
			this.isToday = false;
			this.isViewWeek = false;
			this.inCurrentMonth = false;
			this.dateString = formattedDate;
		}
		
		public String getStyle(){
			
			if(hours<8 && isweekend==false) return 'color:red';
			if(hours>8 || (hours>0 && isweekend)) return 'color:blue';
			
			return 'color:black';			
		}
		
		public String getDayStyle(){
			
			if(istoday) return 'background-color: lightGreen;';			
			if(isweekend) return 'background-color: lightYellow;';
			if(isViewWeek) return 'background-color: lightBlue;';
			
			return '';
		}
	}
}