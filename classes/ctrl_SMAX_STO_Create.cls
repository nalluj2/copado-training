/**
 * @description       : Controller for handling STO create UI logic.
 * @author            : tom.h.ansley@medtronic.com
 * @group             : 
 * @last modified on  : 12-14-2020
 * @last modified by  : tom.h.ansley@medtronic.com
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   09-18-2020   tom.h.ansley@medtronic.com   Initial Version
**/
public with sharing class ctrl_SMAX_STO_Create {
    
    public SVMXC__Parts_Request__c order {get; set;}    
    public String pageError {get; set;}
    public List<SVMXC__Parts_Request_Line__c> lineItems {get; set;}           
    public List<SelectOption> countries {get; set;}  
    public List<SelectOption> regions {get; set;}    
    private String retURL {get; set;} 
        
    public ctrl_SMAX_STO_Create(ApexPages.StandardController controller) {
        
        retURL = ApexPages.currentPage().getParameters().get('retURL');
        
        order = (SVMXC__Parts_Request__c) controller.getRecord();
        
        if(order.Id != null){
        	
        	lineItems = [SELECT Id,  SVMXC__Product__c, Requested_Qty__c, Unit_of_Measure__c FROM SVMXC__Parts_Request_Line__c WHERE SVMXC__Parts_Request__c = :order.Id];

			if(order.Country__c != null) getCountryRegions();
			        	        				
        }else{
        	
        	lineItems = new List<SVMXC__Parts_Request_Line__c>();
        }
                
        pageError = '';
               
        countries = new List<SelectOption>();
                
        for (DIB_Country__c country : [SELECT Name from DIB_Country__c ORDER BY Name])
        {
            countries.add(new SelectOption(country.Name.toUpperCase(), country.Name.toUpperCase()));
        }        
        
        String clone = ApexPages.currentPage().getParameters().get('clone');
        
        if(clone == '1'){
        	
        	SVMXC__Parts_Request__c cloned = order.clone();
        	order.clear();
        	order.Delivery_Type__c = cloned.Delivery_Type__c;
        	order.SM_Catalyst_Work_Order__c = cloned.SM_Catalyst_Work_Order__c;
        	order.Part_Request_Comments__c = cloned.Part_Request_Comments__c;
        	order.Delivery_Instructions__c = cloned.Delivery_Instructions__c;
        	order.Receiving_Address_Type__c = cloned.Receiving_Address_Type__c;
        	order.Ship_To_Customer__c = cloned.Ship_To_Customer__c;
        	order.Name1__c = cloned.Name1__c;
        	order.Name2__c = cloned.Name2__c;
        	order.Street__c = cloned.Street__c;
        	order.City__c = cloned.City__c;
        	order.Postal_Code__c = cloned.Postal_Code__c;
        	order.StateRegion__c = cloned.StateRegion__c;
        	order.Country__c = cloned.Country__c;
        	System.debug(order);
        	System.debug(cloned);
        	
        	for(SVMXC__Parts_Request_Line__c lineItem : lineItems){
        		
        		lineItem.Id = null;
        		lineItem.SVMXC__Parts_Request__c = null;
        	}   
        	
        	System.debug(lineItems);     	
        }
    }    
        
    public void setWorkOrderStatusToAwaitingParts(){
                
        update new SVMXC__Service_Order__c(Id = order.SM_Catalyst_Work_Order__c, SVMXC__Order_Status__c='Awaiting Parts');
	}

    public void deliveryTypeChanged()
    {

        if (order.Delivery_Type__c == 'OVERNIGHT')
        {
            pageError = 'If delivery type is \'Next day before 12\' a justification and approval must be provided in the part request comments field.';
            System.debug(LoggingLevel.ERROR, 'PAGE ERROR - ' + pageError);
            
        }else{
        	
        	pageError = '';
        }
    }
    
    public void receivingAddressChanged(){

        if (order.Receiving_Address_Type__c == 'I will provide Address' && order.Country__c == null)
        {
			User currentUser = [Select Id, Country_vs__c from User where Id = :UserInfo.getUserId()];	
        	order.Country__c = currentUser.Country_vs__c;  
        	
        	if(order.Country__c == null) order.Country__c = countries[0].getValue(); 
        	
        	getCountryRegions();            
        }        
    }
    
    public void countryChanged(){
		
		order.StateRegion__c = null;
		
		getCountryRegions();
    }
    
    private void getCountryRegions(){
    	
        String countryCode = clsUtil.getCountryISOCodeByName(order.Country__c); 
        
        regions = new List<SelectOption>();
        
        List<SAP_Region_Code__c> countryRegions = [Select Label__c, Name, Region_Code__c from SAP_Region_Code__c where Country_Code__c = :countryCode Order By Label__c];
        
        if(countryRegions.size() == 1){
        	
        	regions.add(new SelectOption(countryRegions[0].Name, countryRegions[0].Label__c));
        	
        }else{
        	
        	regions.add(new SelectOption('', '-- Select --'));
        	
	        for(SAP_Region_Code__c countryRegion : countryRegions){
	        	
	        	if(countryRegion.Region_Code__c != countryCode){
	        		regions.add(new SelectOption(countryRegion.Name, countryRegion.Label__c));
	        	}
	        }  
        }
    }

    public void addLineItem(){
    	
        SVMXC__Parts_Request_Line__c lineItem = new SVMXC__Parts_Request_Line__c();        
        lineItem.Unit_of_Measure__c = 'EA';

        lineItems.add(lineItem);        
    }

	public Integer lineItemId {get; set;}

    public void removeLineItem(){
    	        
        lineItems.remove(lineItemId);
    }

    public PageReference deleteOrder(){
    	
        delete order;
		
		if(retURL != null) return new PageReference(retURL);
		
		String keyPrefix = SVMXC__Parts_Request__c.SObjectType.getDescribe().getKeyPrefix();
		
        return new PageReference('/' + keyPrefix + '/o');
    }
    
    public PageReference save()
    {
        pageError = '';
        
        try {
            
            if (order.Receiving_Address_Type__c != 'I will provide Address')
            {                
                order.Name1__c = null;
                order.Name2__c = null;
                order.Street__c = null;
                order.City__c = null;
                order.Postal_Code__c = null;
                order.StateRegion__c = null;   
                order.Country__c = null;               
            }
            
            if (order.Receiving_Address_Type__c != 'Customers Address')
            {                
                order.Ship_To_Customer__c = null;                        
            }

            upsert order;
            
            for(SVMXC__Parts_Request_Line__c lineItem : lineItems){
            	
            	if(lineItem.Id == null) lineItem.SVMXC__Parts_Request__c = order.Id;
            }
                        
            upsert lineItems;            
        
        } catch (Exception e) {
        
            pageError = 'There was an error saving the parts request - ' + e.getMessage() + ' - ' + e.getStackTraceString();
            System.debug(LoggingLevel.ERROR, 'PAGE ERROR - ' + pageError);
        }

        return null;
    }
    

    /*
     * Method which gets called when the user tries to create the STO.
     */
    public PageReference submit()
    {

        if(pageError != '') return null;

        if (order.Delivery_Type__c == 'OVERNIGHT' && clsUtil.isBlank(order.Part_Request_Comments__c))
        {
            pageError = 'If delivery type is overnight a justification and approval must be provided in the part request comments field.';            
            return null;
        }

        if (order.Receiving_Address_Type__c == 'Customers Address' && clsUtil.isBlank(order.Ship_To_Customer__c))
        {
            pageError = 'If receiving address type is "Customers Address" the Ship-to Customer must be provided.';            
            return null;

        } else if (order.Receiving_Address_Type__c == 'I will provide Address')
        {
            if (clsUtil.isBlank(order.Name1__c) 
                || clsUtil.isBlank(order.Street__c) 
                || clsUtil.isBlank(order.City__c) 
                || clsUtil.isBlank(order.Postal_Code__c)
                || clsUtil.isBlank(order.StateRegion__c)
                || clsUtil.isBlank(order.Country__c))
            {
                pageError = 'If receiving address type is "I will provide Address" the name, street, city, postal code, region and country must be provided.';                
                return null;
            }
        }
		
		if(lineItems.size() == 0){
			
			pageError = 'At least one line item must be added.';                
            return null;
		}
		
        for (SVMXC__Parts_Request_Line__c lineItem: lineItems)
        {
            if (clsUtil.isBlank(lineItem.SVMXC__Product__c) 
                || clsUtil.isBlank(lineItem.Requested_Qty__c) 
                || clsUtil.isBlank(lineItem.Unit_of_Measure__c))
            {
                pageError = 'All line items must have product, requested quantity and unit of measure provided.';                
                return null;
            }
        }

        try {

            SVMXC__Parts_Request__c response = bl_STOSAPService.createCalloutSTOCreate(bl_STOSAPService.getPartsRequest(order.Id));

            if (response != null)
            {
                if (response.SVMXC__Status__c == 'Open')
                {
                	
                	try{
                		
                		if(order.SM_Catalyst_Work_Order__c != null) setWorkOrderStatusToAwaitingParts();
                		
                	}catch(Exception e){
                		
                		System.debug('Error updating the related Work Order status to \'Awaiting Parts\': ' + e.getMessage() + '. ' + e.getStackTraceString());
                	}
                    
                    return new PageReference('/' + order.Id);

                } else {
                    pageError = response.SVMXC__Additional_Information__c;
                }
            }

        } catch (Exception e) {
            
            pageError = e.getMessage() + '&nbsp;&nbsp;' + e.getStackTraceString();
            System.debug(LoggingLevel.ERROR, 'PAGE ERROR - ' + pageError);
            
            order.SVMXC__Status__c = 'Error in Processing';
            order.SAP_Last_Sync_Date_Time__c     = System.now();
            order.SAP_Last_Sync_Error_Message__c = pageError;
            order.SAP_Last_Sync_Message_Type__c  = 'Create';
            order.SAP_Last_Sync_Status__c        = 'Failure';
            
            update order;
        }

        return null;
    } 
}