public with sharing class SVMX_SiteWorkOrderCreation 
{
    private SVMXC__Service_Order__c workOrder;

    // Collection of current (selected or unselected) lines 
	private Map<Id, Boolean> oldLinesSelection = new Map<Id, Boolean>();

    /*============================================================================
        C.O.N.S.T.R.U.C.T.O.R
    =============================================================================*/       
    public SVMX_SiteWorkOrderCreation(ApexPages.StandardController controller) 
    {
        this.workOrder = (SVMXC__Service_Order__c) controller.getRecord();
        String query = 'SELECT Id, SVMX_PS_Is_Site_Visit__c, SVMX_PS_Number_of_Child_WO__c';
        for(Schema.FieldSetMember f : this.getWOFieldsToMap())
            query += ', ' + f.getFieldPath();
        query += ' FROM SVMXC__Service_Order__c ';
        query += ' WHERE Id=\'' + this.workOrder.Id + '\'';
        this.workOrder = Database.query(query);
    }  

    public List<Schema.FieldSetMember> getWOFields() {
        return SObjectType.SVMXC__Service_Order__c.FieldSets.SVMX_PS_CreationSiteVisit.getFields();
    }

    public List<Schema.FieldSetMember> getWOFieldsToMap() {
        return SObjectType.SVMXC__Service_Order__c.FieldSets.SVMX_PS_Work_Order_to_Site_Visit_Mapping.getFields();
    }

    public Boolean globalSelected { get; set; } { globalSelected = true; } //select all checkbox 

    // define the column name to order by (default: Name)   
        public String sortBy {
        get {
            if(sortBy == null)
                sortBy = 'SVMX_Parent_Work_Order__c';
            return sortBy;
        }
        set;
    }

    // define the order type to be applied (default ASC)
    public String sortType {
        get {
            if(sortType == null)
                sortType = 'ASC';
            return sortType;
        }
        set;
    }

    // apply filter or sort conditions and rerender the search result
    public PageReference onClickSortBy() {
        // Save current selection
        for(WorkOrder wo : relatedWorkOrders)
            oldLinesSelection.put(wo.WorkOrder.Id, wo.isSelected);
            relatedWorkOrders = null;
        return null;
    }

    public List<WorkOrder> relatedWorkOrders 
    {
        get 
        {
            if(relatedWorkOrders == null)
            {
                relatedWorkOrders = new List<WorkOrder>();
                String query = 'SELECT Id, SVMX_Parent_Work_Order__c';
                for(Schema.FieldSetMember f : this.getWOFields())
                    query += ', ' + f.getFieldPath();
                query += ' FROM SVMXC__Service_Order__c ';
                query += ' WHERE (SVMXC__Order_Status__c=\'Open\' OR SVMXC__Order_Status__c=\'Ready to Plan\')';
                query += ' AND SVMXC__Company__c=\'' + this.workOrder.SVMXC__Company__c + '\'';
                query += ' AND OwnerId=\'' + UserInfo.getUserId() + '\'';
                query += ' AND SVMX_PS_Is_Site_Visit__c=false';
                if(!workOrder.SVMX_PS_Is_Site_Visit__c)
                    query += ' AND SVMX_Parent_Work_Order__c=null';
                else
                    query += ' AND (SVMX_Parent_Work_Order__c=null OR SVMX_Parent_Work_Order__c=\''+workOrder.Id+'\')';
                query += ' ORDER BY ' + sortBy + ' ' + sortType;
                if(sortBy == 'SVMX_Parent_Work_Order__c')
                    query += ' NULLS LAST';
                query += ' LIMIT 50';
                for(SVMXC__Service_Order__c wo : Database.query(query)) {
                    WorkOrder woObject = new WorkOrder(wo);
                    if(oldLinesSelection.containsKey(wo.Id))
                        woObject.isSelected = oldLinesSelection.get(wo.Id);
                    else if(workOrder.SVMX_PS_Is_Site_Visit__c && wo.SVMX_Parent_Work_Order__c == null) {
                        woObject.isSelected = false;
                        globalSelected = false;
                    }
                    relatedWorkOrders.add(woObject);
                }
            }
            return relatedWorkOrders;
        }
        private set;
    }

    public class WorkOrder
    {
        public Boolean isSelected                   { get; set; }
        public Boolean isAlreadyLinked              { get; set; }
        public SVMXC__Service_Order__c workOrder    { get; set; }

        public WorkOrder(SVMXC__Service_Order__c workOrder)
        {
            this.workOrder = workOrder;
            isSelected = true;
            isAlreadyLinked = this.workOrder.SVMX_Parent_Work_Order__c != null;
        }
    }

   // Update records and go back to work order detail page
   public PageReference applyChangesAndClose() 
   {
        SVMXC__Service_Order__c parentWorkOrder = new SVMXC__Service_Order__c();
        List<SVMXC__Service_Order__c> selectedWOs = new List<SVMXC__Service_Order__c>();
        List<SVMXC__Service_Order__c> unselectedWOs = new List<SVMXC__Service_Order__c>();
        List<SVMXC__Service_Order__c> wosToUpdate = new List<SVMXC__Service_Order__c>();
 
        try {
            if(workOrder.SVMX_PS_Is_Site_Visit__c) {
                // Update mode only
                parentWorkOrder = workOrder;
            } else {
                // Creation mode only
                for(Schema.FieldSetMember woField : this.getWOFieldsToMap())
                    parentWorkOrder.put(woField.getFieldPath(), workOrder.get(woField.getFieldPath()));
                parentWorkOrder.SVMX_PS_Is_Site_Visit__c = true;
            }

            if(relatedWorkOrders.size() > 0)
            {
                Integer nbOfChildWorkOrders = 0;
                for(WorkOrder wo : relatedWorkOrders)
                {
                    if(wo.isSelected) {
                        nbOfChildWorkOrders++;
                        // Make sure only new WO are added for update
                        if(wo.WorkOrder.SVMX_Parent_Work_Order__c == null) {
                            selectedWOs.add(wo.WorkOrder);
                        }
                    } else if(wo.WorkOrder.SVMX_Parent_Work_Order__c != null) {
                        // This wo was unselected
                        unselectedWOs.add(wo.WorkOrder);
                    }
                }
                if(nbOfChildWorkOrders > 1) {
                    if(!workOrder.SVMX_PS_Is_Site_Visit__c)
                        insert parentWorkOrder;
                    for(SVMXC__Service_Order__c selectedWO : selectedWOs) {
                        selectedWO.SVMX_Parent_Work_Order__c = parentWorkOrder.Id;
                        selectedWO.SVMXC__Group_Member__c = parentWorkOrder.SVMXC__Group_Member__c;
                        wosToUpdate.add(selectedWO);
                    }
                    for(SVMXC__Service_Order__c unselectedWO : unselectedWOs) {
                        // Set Parent WO to blank 
                        unselectedWO.SVMX_Parent_Work_Order__c = null;
                        wosToUpdate.add(unselectedWO);
                    }
                    if(wosToUpdate.size() > 0)
                        update wosToUpdate;
                    if(nbOfChildWorkOrders != parentWorkOrder.SVMX_PS_Number_of_Child_WO__c) {
                        parentWorkOrder.SVMX_PS_Number_of_Child_WO__c = nbOfChildWorkOrders;
                        update parentWorkOrder;
                    }
                }
                else {
                    ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Error, 'Please select at least two lines');
                    ApexPages.addMessage(errorMsg);
                    return null;         
                }
            }
            else
            {
                ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Error, 'There are no related work orders');
                ApexPages.addMessage(errorMsg);
                return null; 
            }
        } catch(Exception ex) {
            ApexPages.addMessages(ex);
            return null;
        }   
        return new ApexPages.StandardController(parentWorkOrder).view();
    }   
}