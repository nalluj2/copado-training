//---------------------------------------------------------------------------------------------------------------------------------
//	Author 			: Bart Caelen
//  Created Date 	: 22-11-2017
//  Description 	: APEX TEST Class for the APEX Trigger tr_CampaignMember and APEX Class bl_CampaignMember_Trigger
//  Change Log 		: 
//---------------------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_bl_CampaignMember_Trigger {

	@isTest static void test_updateContact_LMS_Insert() {

		//-------------------------------------------------------
		// Create Test Data
		//-------------------------------------------------------
		clsTestData_MasterData.createCountry();

		Id id_RecordType_Campaign = clsUtil.getRecordTypeByDevName('Campaign', 'LEARNING').Id;

		clsTestData_CustomSetting.mapCampaignMemberStatus_Responded = new Map<String, Boolean>{'Start enrollment'=>true};
		clsTestData_CustomSetting.createCampaignMemberStatus(true, 'RecordTypeID', id_RecordType_Campaign);


		clsTestData_Campaign.idRecordType_Campaign_LMS = id_RecordType_Campaign;
		List<Campaign> lstCampaign = clsTestData_Campaign.createCampaignLMS(false);
			lstCampaign[0].Campaign_Language__c = 'Dutch';
			lstCampaign[0].LMS_Division__c = 'Division';
			lstCampaign[0].IsActive = true;
		insert lstCampaign;
		lstCampaign = [SELECT Id, RecordTypeID, Campaign_Country_vs__c, LMS_Language_ISO__c, LMS_Division__c FROM Campaign WHERE Id = :lstCampaign];
		System.assertEquals(lstCampaign.size(), 1);
		System.assertEquals(lstCampaign[0].RecordTypeID, id_RecordType_Campaign);

		clsTestData_Contact.createMDTEmployee();

		clsTestData_Contact.iRecord_Contact = 2;
		List<Contact> lstContact = clsTestData_Contact.createContact(false);
			lstContact[0].MailingCountry = 'Belgium';
			lstContact[0].LMS_Key_Customer_Contact__c = clsTestData_Contact.oMain_MDTEmployee.Id;
			lstContact[1].MailingCountry = 'Netherlands';
			lstContact[1].LMS_Key_Customer_Contact__c = clsTestData_Contact.oMain_MDTEmployee.Id;
		insert lstContact;

		clsTestData_Campaign.iRecord_CampaignMember = 2;
		List<CampaignMember> lstCampaignMember = clsTestData_Campaign.createCampaignMember(lstCampaign[0].Id, false);


		lstContact = [SELECT Id, Email, MailingCountry, LMS_IsActive__c, LMS_Language__c, LMS_Country__c, LMS_Division__c, LMS_Userid__c FROM Contact WHERE Id = :clsTestData_Contact.lstContact];
		System.assertEquals(lstContact.size(), 2);
		for (Contact oContact : lstContact){
			System.assertEquals(oContact.LMS_isActive__c, false);
			System.assertEquals(oContact.LMS_Division__c, null);
		}
		//-------------------------------------------------------


		//-------------------------------------------------------
		// Test Logic
		//-------------------------------------------------------
		Test.startTest();

		for (CampaignMember oCampaignMember : lstCampaignMember){
			oCampaignMember.Status = 'Start enrollment';
		}
		insert lstCampaignMember;
		
		//-------------------------------------------------------
		// Verify Result
		//-------------------------------------------------------
		lstContact = [SELECT Id, Email, MailingCountry, LMS_IsActive__c, LMS_Language__c, LMS_Country__c, LMS_Division__c, LMS_Userid__c FROM Contact WHERE Id = :clsTestData_Contact.lstContact];
		System.assertEquals(lstContact.size(), 2);
		for (Contact oContact : lstContact){

			System.assertEquals(oContact.LMS_isActive__c, false);
			System.assertEquals(oContact.LMS_Language__c, lstCampaign[0].LMS_Language_ISO__c);
			if (oContact.MailingCountry == 'Belgium'){
				System.assertEquals(oContact.LMS_Country__c, 'BE');
			}else if (oContact.MailingCountry == 'Netherlands'){
				System.assertEquals(oContact.LMS_Country__c, 'NL');
			}
			System.assertEquals(oContact.LMS_Division__c, lstCampaign[0].LMS_Division__c);			
		}
		//-------------------------------------------------------
		
		List<Contact_ED_Sync__c> syncRecords = [Select Id, Outcome__c from Contact_ED_Sync__c];
		System.assertEquals(syncRecords.size(), 2);
		
		Test.setMock(HttpCalloutMock.class, new Test_ED_Provisioning_Mock());
		
		Test.stopTest();
		
		syncRecords = [Select Id, Outcome__c from Contact_ED_Sync__c];
		System.assertEquals(syncRecords.size(), 2);
		
		for(Contact_ED_Sync__c syncRecord : syncRecords){
			
			System.assertEquals(syncRecord.Outcome__c, 'Success');
		}
		
		lstContact = [SELECT Id, Email, MailingCountry, LMS_IsActive__c, LMS_Language__c, LMS_Country__c, LMS_Division__c, LMS_Userid__c FROM Contact WHERE Id = :clsTestData_Contact.lstContact];
		System.assertEquals(lstContact.size(), 2);
		for (Contact oContact : lstContact){

			System.assertEquals(oContact.LMS_isActive__c, true);						
			System.assert(oContact.LMS_Userid__c != null);			
		}
	}
	
	@isTest static void test_updateContact_LMS_Insert_No_Division() {

		//-------------------------------------------------------
		// Create Test Data
		//-------------------------------------------------------
		clsTestData_MasterData.createCountry();

		Id id_RecordType_Campaign = clsUtil.getRecordTypeByDevName('Campaign', 'LEARNING').Id;

		clsTestData_CustomSetting.mapCampaignMemberStatus_Responded = new Map<String, Boolean>{'Start enrollment'=>true};
		clsTestData_CustomSetting.createCampaignMemberStatus(true, 'RecordTypeID', id_RecordType_Campaign);


		clsTestData_Campaign.idRecordType_Campaign_LMS = id_RecordType_Campaign;
		List<Campaign> lstCampaign = clsTestData_Campaign.createCampaignLMS(false);
			lstCampaign[0].Campaign_Language__c = 'Dutch';
			lstCampaign[0].LMS_ED_Group__c = 'TestGroup1';
			lstCampaign[0].IsActive = true;
		insert lstCampaign;
		lstCampaign = [SELECT Id, RecordTypeID, Campaign_Country_vs__c, LMS_Language_ISO__c, LMS_Division__c FROM Campaign WHERE Id = :lstCampaign];
		System.assertEquals(lstCampaign.size(), 1);
		System.assertEquals(lstCampaign[0].RecordTypeID, id_RecordType_Campaign);

		clsTestData_Contact.createMDTEmployee();

		clsTestData_Contact.iRecord_Contact = 2;
		List<Contact> lstContact = clsTestData_Contact.createContact(false);
			lstContact[0].MailingCountry = 'Belgium';
			lstContact[0].LMS_Key_Customer_Contact__c = clsTestData_Contact.oMain_MDTEmployee.Id;
			lstContact[1].MailingCountry = 'Netherlands';
			lstContact[1].LMS_Key_Customer_Contact__c = clsTestData_Contact.oMain_MDTEmployee.Id;
		insert lstContact;

		clsTestData_Campaign.iRecord_CampaignMember = 2;
		List<CampaignMember> lstCampaignMember = clsTestData_Campaign.createCampaignMember(lstCampaign[0].Id, false);


		lstContact = [SELECT Id, Email, MailingCountry, LMS_IsActive__c, LMS_Language__c, LMS_Country__c, LMS_Division__c, LMS_Userid__c FROM Contact WHERE Id = :clsTestData_Contact.lstContact];
		System.assertEquals(lstContact.size(), 2);
		for (Contact oContact : lstContact){
			System.assertEquals(oContact.LMS_isActive__c, false);
			System.assertEquals(oContact.LMS_Division__c, null);
		}
		//-------------------------------------------------------


		//-------------------------------------------------------
		// Test Logic
		//-------------------------------------------------------
		Test.startTest();

		for (CampaignMember oCampaignMember : lstCampaignMember){
			oCampaignMember.Status = 'Start enrollment';
		}
		insert lstCampaignMember;
		
		//-------------------------------------------------------
		// Verify Result
		//-------------------------------------------------------
		lstContact = [SELECT Id, Email, MailingCountry, LMS_IsActive__c, LMS_Language__c, LMS_Country__c, LMS_Division__c, LMS_Userid__c FROM Contact WHERE Id = :clsTestData_Contact.lstContact];
		System.assertEquals(lstContact.size(), 2);
		for (Contact oContact : lstContact){

			System.assertEquals(oContact.LMS_isActive__c, false);
			System.assertEquals(oContact.LMS_Language__c, null);			
			System.assertEquals(oContact.LMS_Division__c, null);			
		}
		//-------------------------------------------------------
		
		List<Contact_ED_Sync__c> syncRecords = [Select Id, Outcome__c from Contact_ED_Sync__c];
		System.assertEquals(syncRecords.size(), 2);
		
		Test.setMock(HttpCalloutMock.class, new Test_ED_Provisioning_Mock());
		
		Test.stopTest();
		
		syncRecords = [Select Id, Outcome__c from Contact_ED_Sync__c];
		System.assertEquals(syncRecords.size(), 2);
		
		for(Contact_ED_Sync__c syncRecord : syncRecords){
			
			System.assertEquals(syncRecord.Outcome__c, 'Success');
		}
		
		lstContact = [SELECT Id, Email, MailingCountry, LMS_IsActive__c, LMS_Language__c, LMS_Country__c, LMS_Division__c, LMS_Userid__c FROM Contact WHERE Id = :clsTestData_Contact.lstContact];
		System.assertEquals(lstContact.size(), 2);
		for (Contact oContact : lstContact){

			System.assertEquals(oContact.LMS_isActive__c, false);						
			System.assert(oContact.LMS_Userid__c != null);			
		}
	}

	@isTest static void test_updateContact_LMS_Update() {

		//-------------------------------------------------------
		// Create Test Data
		//-------------------------------------------------------
		clsTestData_MasterData.createCountry();

		Id id_RecordType_Campaign = clsUtil.getRecordTypeByDevName('Campaign', 'LEARNING').Id;

		clsTestData_CustomSetting.mapCampaignMemberStatus_Responded = new Map<String, Boolean>{'Start enrollment'=>true};
		clsTestData_CustomSetting.createCampaignMemberStatus(true, 'RecordTypeID', id_RecordType_Campaign);


		clsTestData_Campaign.idRecordType_Campaign_LMS = id_RecordType_Campaign;
		List<Campaign> lstCampaign = clsTestData_Campaign.createCampaignLMS(false);
			lstCampaign[0].Campaign_Language__c = 'Dutch';
			lstCampaign[0].Campaign_Country_vs__c = 'BELGIUM';
			lstCampaign[0].LMS_Division__c = 'Division';
			lstCampaign[0].IsActive = true;
		insert lstCampaign;
		lstCampaign = [SELECT Id, RecordTypeID, Campaign_Country_vs__c, LMS_Language_ISO__c, LMS_Division__c FROM Campaign WHERE Id = :lstCampaign];
		System.assertEquals(lstCampaign.size(), 1);
		System.assertEquals(lstCampaign[0].RecordTypeID, id_RecordType_Campaign);

		clsTestData_Contact.createMDTEmployee();

		clsTestData_Contact.iRecord_Contact = 2;
		List<Contact> lstContact = clsTestData_Contact.createContact(false);
			lstContact[0].MailingCountry = 'Belgium';
			lstContact[0].LMS_Key_Customer_Contact__c = clsTestData_Contact.oMain_MDTEmployee.Id;
			lstContact[1].MailingCountry = 'Netherlands';
			lstContact[1].LMS_Key_Customer_Contact__c = clsTestData_Contact.oMain_MDTEmployee.Id;
		insert lstContact;


		clsTestData_Campaign.iRecord_CampaignMember = 2;
		List<CampaignMember> lstCampaignMember = clsTestData_Campaign.createCampaignMember(lstCampaign[0].Id, true);

		lstContact = [SELECT Id, Email, MailingCountry, LMS_IsActive__c, LMS_Language__c, LMS_Country__c, LMS_Division__c, LMS_Userid__c FROM Contact WHERE Id = :clsTestData_Contact.lstContact];
		System.assertEquals(lstContact.size(), 2);
		for (Contact oContact : lstContact){
			System.assertEquals(oContact.LMS_isActive__c, false);
		}
		//-------------------------------------------------------


		//-------------------------------------------------------
		// Test Logic
		//-------------------------------------------------------
		Test.startTest();

		for (CampaignMember oCampaignMember : lstCampaignMember){
			oCampaignMember.Status = 'Start enrollment';
		}
		update lstCampaignMember;
		
		//-------------------------------------------------------
		// Verify Result
		//-------------------------------------------------------
		lstContact = [SELECT Id, Email, MailingCountry, LMS_IsActive__c, LMS_Language__c, LMS_Country__c, LMS_Division__c, LMS_Userid__c FROM Contact WHERE Id = :clsTestData_Contact.lstContact];
		System.assertEquals(lstContact.size(), 2);
		for (Contact oContact : lstContact){

			System.assertEquals(oContact.LMS_isActive__c, false);
			System.assertEquals(oContact.LMS_Language__c, lstCampaign[0].LMS_Language_ISO__c);
			System.assertEquals(oContact.LMS_Country__c, 'BE');
			System.assertEquals(oContact.LMS_Division__c, lstCampaign[0].LMS_Division__c);
		}
		
		List<Contact_ED_Sync__c> syncRecords = [Select Id, Outcome__c from Contact_ED_Sync__c];
		System.assertEquals(syncRecords.size(), 2);
		//-------------------------------------------------------
		
		Test.setMock(HttpCalloutMock.class, new Test_ED_Provisioning_Mock());
		
		Test.stopTest();
		
		syncRecords = [Select Id, Outcome__c from Contact_ED_Sync__c];
		System.assertEquals(syncRecords.size(), 2);
		
		for(Contact_ED_Sync__c syncRecord : syncRecords){
			
			System.assertEquals(syncRecord.Outcome__c, 'Success');
		}
		
		lstContact = [SELECT Id, Email, MailingCountry, LMS_IsActive__c, LMS_Language__c, LMS_Country__c, LMS_Division__c, LMS_Userid__c FROM Contact WHERE Id = :clsTestData_Contact.lstContact];
		System.assertEquals(lstContact.size(), 2);
		for (Contact oContact : lstContact){

			System.assertEquals(oContact.LMS_isActive__c, true);						
			System.assert(oContact.LMS_Userid__c != null);			
		}
	}


	@isTest static void test_BPMSIntegration() {

		//-------------------------------------------------------
		// Create Test Data
		//-------------------------------------------------------
		clsTestData_MasterData.createCountry();
		BPMS_API_Settings__c oBPMSAPISettings = [SELECT Id, Active__c FROM BPMS_API_Settings__c][0];
			oBPMSAPISettings.Active__c = true;
		update oBPMSAPISettings;

		Id id_RecordType_Campaign = clsUtil.getRecordTypeByDevName('Campaign', 'LEARNING').Id;

		clsTestData_CustomSetting.mapCampaignMemberStatus_Responded = new Map<String, Boolean>{'Start enrollment'=>true};
		clsTestData_CustomSetting.createCampaignMemberStatus(true, 'RecordTypeID', id_RecordType_Campaign);


		clsTestData_Campaign.idRecordType_Campaign_LMS = id_RecordType_Campaign;
		List<Campaign> lstCampaign = clsTestData_Campaign.createCampaignLMS(false);
			lstCampaign[0].Campaign_Language__c = 'Dutch';
			lstCampaign[0].LMS_Division__c = 'Division';
			lstCampaign[0].IsActive = true;
		insert lstCampaign;
		lstCampaign = [SELECT Id, RecordTypeID, Campaign_Country_vs__c, LMS_Language_ISO__c, LMS_Division__c FROM Campaign WHERE Id = :lstCampaign];
		System.assertEquals(lstCampaign.size(), 1);
		System.assertEquals(lstCampaign[0].RecordTypeID, id_RecordType_Campaign);

		clsTestData_Contact.createMDTEmployee();

		clsTestData_Contact.iRecord_Contact = 2;
		List<Contact> lstContact = clsTestData_Contact.createContact(false);
			lstContact[0].MailingCountry = 'Belgium';
			lstContact[0].LMS_Key_Customer_Contact__c = clsTestData_Contact.oMain_MDTEmployee.Id;
			lstContact[1].MailingCountry = 'Netherlands';
			lstContact[1].LMS_Key_Customer_Contact__c = clsTestData_Contact.oMain_MDTEmployee.Id;
		insert lstContact;

		clsTestData_Campaign.iRecord_CampaignMember = 2;
		List<CampaignMember> lstCampaignMember = clsTestData_Campaign.createCampaignMember(lstCampaign[0].Id, false);

		lstContact = [SELECT Id, Email, MailingCountry, LMS_IsActive__c, LMS_Language__c, LMS_Country__c, LMS_Division__c, LMS_Userid__c FROM Contact WHERE Id = :clsTestData_Contact.lstContact];
		System.assertEquals(lstContact.size(), 2);
		for (Contact oContact : lstContact){
			System.assertEquals(oContact.LMS_isActive__c, false);
			System.assertEquals(oContact.LMS_Division__c, null);
		}
		//-------------------------------------------------------


		//-------------------------------------------------------
		// Test Logic
		//-------------------------------------------------------
		Test.startTest();

		Test.setMock(HttpCalloutMock.class, new TEST_BPMSIntegration_Mock());

		for (CampaignMember oCampaignMember : lstCampaignMember){
			oCampaignMember.Status = 'Start enrollment';
		}
		insert lstCampaignMember;
		
		//-------------------------------------------------------
		// Verify Result
		//-------------------------------------------------------
		lstContact = [SELECT Id, Email, MailingCountry, LMS_IsActive__c, LMS_Language__c, LMS_Country__c, LMS_Division__c, LMS_Userid__c FROM Contact WHERE Id = :clsTestData_Contact.lstContact];
		System.assertEquals(lstContact.size(), 2);
		for (Contact oContact : lstContact){

			System.assertEquals(oContact.LMS_isActive__c, false);
			System.assertEquals(oContact.LMS_Language__c, lstCampaign[0].LMS_Language_ISO__c);
			if (oContact.MailingCountry == 'Belgium'){
				System.assertEquals(oContact.LMS_Country__c, 'BE');
			}else if (oContact.MailingCountry == 'Netherlands'){
				System.assertEquals(oContact.LMS_Country__c, 'NL');
			}
			System.assertEquals(oContact.LMS_Division__c, lstCampaign[0].LMS_Division__c);			
		}
		//-------------------------------------------------------
		
		List<Contact_ED_Sync__c> syncRecords = [Select Id, Outcome__c from Contact_ED_Sync__c];
		System.assertEquals(syncRecords.size(), 2);
		
		Test.stopTest();
		
		syncRecords = [Select Id, Outcome__c from Contact_ED_Sync__c];
		System.assertEquals(syncRecords.size(), 2);
		
		for(Contact_ED_Sync__c syncRecord : syncRecords){
			
			System.assertEquals(syncRecord.Outcome__c, 'Success');
		}
		
		lstContact = [SELECT Id, Email, MailingCountry, LMS_IsActive__c, LMS_Language__c, LMS_Country__c, LMS_Division__c, LMS_Userid__c FROM Contact WHERE Id = :clsTestData_Contact.lstContact];
		System.assertEquals(lstContact.size(), 2);
		for (Contact oContact : lstContact){

			System.assertEquals(oContact.LMS_isActive__c, false);	// This will be updated by the BPMS Integration
			System.assert(oContact.LMS_Userid__c == null);			// This will be updated by the BPMS Integration
		}
	}
	
		
	@TestSetup
	private static void generateCustomSettings(){
		
		ED_API_Settings__c apiSettings = new ED_API_Settings__c();
			apiSettings.API_Key__c = '987654321';
			apiSettings.Token_Authentication__c = '123546789';
			apiSettings.Org_Id__c = UserInfo.getOrganizationId();
			apiSettings.Default_Password__c = 'Medtronic123';
			apiSettings.Target_Server_URL__c  = 'https://fakeURL.com';		
		insert apiSettings; 

		CSOD_API_Settings__c oCSODAPISettings = new CSOD_API_Settings__c();
			oCSODAPISettings.Token_Authentication__c = '123546789';
			oCSODAPISettings.Org_Id__c = UserInfo.getOrganizationId();
			oCSODAPISettings.Target_Server_URL__c  = 'https://fakeURL.com';		
			oCSODAPISettings.Timeout__c = 60000;
			oCSODAPISettings.Maximum_Records__c = 20;
			oCSODAPISettings.Target_server_URL_PATCH__c = 'https://fakeURL.com';
			oCSODAPISettings.Token_Authentication_PATCH__c = '123546789';
		insert oCSODAPISettings;

		BPMS_API_Settings__c oBPMSAPISettings= new BPMS_API_Settings__c();
			oBPMSAPISettings.Token_Authentication__c = '123546789';
			oBPMSAPISettings.Org_Id__c = UserInfo.getOrganizationId();
			oBPMSAPISettings.Target_Server_URL__c  = 'https://fakeURL.com';		
			oBPMSAPISettings.Timeout__c = 60000;
			oBPMSAPISettings.Maximum_Records__c = 50;
		insert oBPMSAPISettings;

	}
}
//---------------------------------------------------------------------------------------------------------------------------------