public class bl_CFN_Mapping_Asset {
	
	public static void populateUniqueKey(List<CFN_Mapping_Asset__c> triggerNew){
		
		Set<Id> productIds = new Set<Id>();
		
		for(CFN_Mapping_Asset__c mapping : triggerNew){
			
			if(mapping.Product__c != null) productIds.add(mapping.Product__c);			
		}
		
		if(productIds.size() > 0){
			
			Map<Id, Product2> productMap = new Map<Id, Product2>([Select Id, Region_vs__c from Product2 where Id IN :productIds]);
			
			for(CFN_Mapping_Asset__c mapping : triggerNew){
			
				if(mapping.Name != null && mapping.Product__c != null){
					
					Product2 prod = productMap.get(mapping.Product__c);
					mapping.Unique_Key__c = mapping.Name + ':' + prod.Region_vs__c;
					
				}else{
					
					mapping.Unique_Key__c = null;
				}			
			} 
		}
	}
	/*
	public static void calculateAssetInterfacedFlag(List<CFN_Mapping_Asset__c> records, Map<Id, CFN_Mapping_Asset__c> oldMap){
		
		Set<Id> mktProducts = new Set<Id>();
		
		for(CFN_Mapping_Asset__c mapping : records){
			
			//Insert, Delete and Undelete
			if(oldMap == null){
				
				if(mapping.Product__c != null) mktProducts.add(mapping.Product__c);
			
			//Update (possible re-parenting)	
			}else{
				
				CFN_Mapping_Asset__c oldMapping = oldMap.get(mapping.Id);
				
				if(mapping.Product__c != oldMapping.Product__c){
					
					if(mapping.Product__c != null) mktProducts.add(mapping.Product__c);
					if(oldMapping.Product__c != null) mktProducts.add(oldMapping.Product__c);
				}	
			}
		}
		
		if(mktProducts.size() > 0) calculateProductAssetInterfacedFlag(mktProducts);		
	}
	
	public static void calculateProductAssetInterfacedFlag(Set<Id> productIds){
		
		List<Product2> products = [Select Id, Linked_Assets_Interfaced__c, (Select Id from CFN_Mapping_Assets__r LIMIT 1) from Product2 where Id IN :productIds FOR UPDATE];
		
		for(Product2 prod : products){
			
			if(prod.CFN_Mapping_Assets__r.size() > 0) prod.Linked_Assets_Interfaced__c = true;
			else prod.Linked_Assets_Interfaced__c = false;
		}
		
		update products;
	}
	*/
    
}