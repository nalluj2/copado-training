@isTest private class Test_ba_CallRecord {

    @isTest static void test_ba_CallRecord() {
		//- CREATE TEST DATA
		// Account
        Account oAccount = new Account();
        	oAccount.Name = 'TEST Account'; 
        	oAccount.Account_Country_vs__c = 'BELGIUM';
        insert oAccount;               
        
        // Contact
        Contact oContact = new Contact();
        	oContact.LastName = 'TEST Contact';  
        	oContact.FirstName = 'TEST Contact';  
	        oContact.AccountId = oAccount.Id;  
	        oContact.Phone = '001222333'; 
	        oContact.Email =' test@medtronic.com';
	        oContact.Contact_Department__c = 'Diabetes Adult'; 
	        oContact.Contact_Primary_Specialty__c = 'ENT';
	        oContact.Affiliation_To_Account__c = 'Employee';
	        oContact.Primary_Job_Title_vs__c = 'Manager';
	        oContact.Contact_Gender__c = 'Male';
        insert oContact;  

         // MASTER (Company)
         Company__c oCompany = new Company__c();
	        oCompany.Name='Test Company name';
    	    oCompany.Company_Code_Text__c = 'T46';
         insert oCompany;

		// Call Activity Type        
        Call_Activity_Type__c oCallActivityType = new Call_Activity_Type__c();
	        oCallActivityType.name = 'test subject';
	        oCallActivityType.Company_ID__c = oCompany.id;
        insert oCallActivityType; 
        
		// Call Record        
        Call_Records__c oCallRecord = new Call_Records__c(); 
	        oCallRecord.Call_Channel__c = '1:1 visit'; 
	        oCallRecord.Call_Date__c = system.today()-1; 
        insert oCallRecord;    
		
		// Contact Visit Report
        Contact_Visit_Report__c oContactVisitReport = new Contact_Visit_Report__c (); 
	        oContactVisitReport.Call_Records__c = oCallRecord.Id; 
	        oContactVisitReport.Attending_Contact__c = oContact.id; 
	        oContactVisitReport.Attending_Affiliated_Account__c = oAccount.Id; 
        insert oContactVisitReport;
        


		list<Call_Records__c> lstCR = [SELECT ID, Process_Batch__c, Countries__c, Related_Accounts__c, Number_of_Related_Accounts__c, Call_Contact_Concatenated__c FROM Call_Records__c WHERE ID = :oCallRecord.Id];
		system.assert(lstCR.size()>0);
		system.assertEquals(lstCR[0].Process_Batch__c, true);
		system.assertEquals(lstCR[0].Related_Accounts__c, null);
		system.assertEquals(lstCR[0].Countries__c, 'BELGIUM');
		system.assertEquals(lstCR[0].Number_of_Related_Accounts__c, null);
		system.assertEquals(lstCR[0].Call_Contact_Concatenated__c, null);

		test.startTest();

		//---------------------------------------
		// TEST SCHEDULING
		//---------------------------------------
		string tCRON_EXP = '0 0 0 3 9 ? 2099';
		
		string tJobId = System.schedule('ba_CallRecord_TEST', tCRON_EXP, new ba_CallRecord());

		// Get the information from the CronTrigger API object
		CronTrigger oCronTrigger = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :tJobId];

		// Verify the expressions are the same
		System.assertEquals(tCRON_EXP, oCronTrigger.CronExpression);

		// Verify the job has not run
		System.assertEquals(0, oCronTrigger.TimesTriggered);

		// Verify the next time the job will run
		System.assertEquals('2099-09-03 00:00:00', String.valueOf(oCronTrigger.NextFireTime));
		//---------------------------------------

		//---------------------------------------
		// TEST BATCH
		//---------------------------------------
    	ba_CallRecord oBatch = new ba_CallRecord();
        Database.executebatch(oBatch,100);        	
		//---------------------------------------

		test.stopTest();

		lstCR = [SELECT ID, Process_Batch__c, Countries__c, Related_Accounts__c, Number_of_Related_Accounts__c, Call_Contact_Concatenated__c FROM Call_Records__c WHERE ID = :oCallRecord.Id];
		system.assert(lstCR.size()>0);
		system.assertEquals(lstCR[0].Process_Batch__c, false);
		system.assertNotEquals(lstCR[0].Countries__c, null);
		system.assertNotEquals(lstCR[0].Related_Accounts__c, null);
		system.assertNotEquals(lstCR[0].Number_of_Related_Accounts__c, null);
		system.assertNotEquals(lstCR[0].Call_Contact_Concatenated__c, null);

    }
}