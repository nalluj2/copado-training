@isTest
private class TEST_bl_SupportRequest {
	
	@isTest static void test_bl_SupportRequest() {

        List<Support_Request_Parameter__c> lstSRP = new List<Support_Request_Parameter__c>();
        Support_Request_Parameter__c oSRP = new Support_Request_Parameter__c();
            oSRP.Active__c = true;
            oSRP.Key__c = '01511000000Amg8AAC';
            oSRP.Sequence__c = 1;
            oSRP.Type__c = 'Cvent - Document';
            oSRP.Value__c = 'Cvent Test Document';
        lstSRP.add(oSRP);
        insert lstSRP;
		
		ws_adUserServiceMockImpl mockup = new ws_adUserServiceMockImpl();		
		Test.setMock(WebServiceMock.class, mockup);
		
		Test.startTest();

        bl_SupportRequest.loadSupportRequestParameter();
        bl_SupportRequest.loadSupportRequestParameter('Cvent - Document');
        bl_SupportRequest.loadSupportRequestDetailData('');
        bl_SupportRequest.loadSupportRequestOverviewData('');
        bl_SupportRequest.searchByAlias('caeleb1');

        Test.stopTest();

	}
	
}