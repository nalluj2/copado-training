public with sharing class ctrl_Objective_Task_Subject_Selection {
	
	public List<SelectOption> objectiveSubjects {get; set;}
	public String selectedSubject {get; set;}
	private Account_Plan_Objective__c objective {get; set;}	
	private String retURL {get; set;}
	
	public ctrl_Objective_Task_Subject_Selection(){
		
		Map<String, String> inputParams = ApexPages.currentPage().getparameters();
		retURL = inputParams.get('retURL');
						
		try{
			
			Id objectiveId = inputParams.get('objId');
				
			List<Account_Plan_Objective__c> objectiveList = [Select Id, Business_Objective__c from Account_Plan_Objective__c where Id = :objectiveId];
			
			if(objectiveList.size() == 0) throw new IllegalArgumentException('Invalid or null Account Plant Objective Id');
			
			objective = objectiveList[0];
				
		}catch(Exception e){
			
			ApexPages.addMessages(e);
			return;
		}
		
		objectiveSubjects = new List<SelectOption>();		
		
		for(Objective_Subject__c subject : [Select Name from Objective_Subject__c where Business_Objective__c = :objective.Business_Objective__c ORDER BY Name]){
			
			objectiveSubjects.add(new SelectOption(subject.Name, subject.Name));
		}
				
		objectiveSubjects.add(new SelectOption('Other', 'Other (please, specify in comments)'));
	}
	
	public PageReference cancel(){
		
		PageReference pr = new PageReference(retURL);		
		pr.setRedirect(true);
		return pr;
	}
	
	public PageReference createTask(){
		
		PageReference pr = new PageReference('/00T/e');
		pr.getParameters().put('what_id', objective.Id);
		pr.getParameters().put('tsk5', selectedSubject);
		pr.getParameters().put('retURL', retURL);
		pr.setRedirect(true);
		return pr;
	}
}