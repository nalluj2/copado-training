@RestResource(urlMapping='/ActivitySchedulingService/*')
global without sharing class ws_Activity_Scheduling {
    
    @HttpGet
    global static void doGet() {
 		
		String query = RestContext.request.params.get('query');
		
		try{
			
			List<SObject> queryResult = Database.query(query);
			
			String responseBody = JSON.serialize(queryResult);
			
			//responseBody = responseBody.replaceAll('"attributes":\\{"type":"(.*?)","url":"\\/services\\/data\\/(.*?)"\\},','');			
			
			RestContext.response.responseBody = Blob.valueOf(responseBody);
			
		}catch (Exception e){
						
			RestResponse res = RestContext.response;
			res.statusCode = 400;
			
			SalesforceError err = new SalesforceError();
			err.errorCode = 'BAD_REQUEST';
			err.message =  e.getMessage();
			
			RestContext.response.responseBody = Blob.valueOf(JSON.serialize(err));
		}		 				
	}
	
	global class SalesforceError {
    	
    	public String errorCode;
    	public String message;    	
	}
}