public class bl_Contract_xBU {
    
    public static void createHistoryTracking(List<Contract_xBU__c> lstTriggerNew, Map<Id, Contract_xBU__c> mapTriggerOld){

        if (bl_Trigger_Deactivation.isTriggerDeactivated('contractxBU_HistoryTracking')) return;

		List<History_Tracking__c> lstHistoryTracking = new List<History_Tracking__c>();

		// Load Related Data
		Map<Id, String> mapRelatedData = new Map<Id, String>();
		Map<String, HistoryTracking_ContractxBU__c> mapHistoryTracking = HistoryTracking_ContractxBU__c.getAll();
		for (String tKey : mapHistoryTracking.keySet()){

			HistoryTracking_ContractxBU__c oHistoryTracking = mapHistoryTracking.get(tKey);
			String tReferenceTo = oHistoryTracking.ReferenceTo__c;
			String tReferenceToField = oHistoryTracking.ReferenceToField__c;

			if ( (!String.isBlank(tReferenceTo)) && (!String.isBlank(tReferenceToField)) ){

				mapRelatedData.putAll(bl_HistoryTracking.loadRelatedData(tReferenceTo, tReferenceToField, lstTriggerNew, mapTriggerOld, String.valueOf(oHistoryTracking.get('Name'))));
			}

		}

		Set<String> setFieldName_HistoryTracking = HistoryTracking_ContractxBU__c.getAll().keySet();
		if (setFieldName_HistoryTracking.size() == 0) return;

		for (Contract_xBU__c oContractxBU : lstTriggerNew){

			Contract_xBU__c oContractxBU_Old;
			if (mapTriggerOld != null) oContractxBU_Old = mapTriggerOld.get(oContractxBU.Id);

			lstHistoryTracking.addAll(bl_HistoryTracking.createHistoryTracking('Contract_xBU__c', 'Contract_xBU__c', oContractxBU_Old, oContractxBU, setFieldName_HistoryTracking, mapRelatedData, false));
		}

		if (lstHistoryTracking.size() > 0) insert lstHistoryTracking;	
	}
	
	public static void setContractNumber(List<Contract_xBU__c> triggerNew){
						
		String suffix = '-' + String.valueOf(Date.today().year()).substring(2,4);
		
		String searchSuffix = '%' + suffix;
		List<Contract_xBU__c> lastExistingContract = [Select Internal_Contract_Number__c from Contract_xBU__c where Contract_Number__c LIKE :searchSuffix ORDER BY Internal_Contract_Number__c DESC LIMIT 1];
		
		Integer currentNumber = 0;
		if(lastExistingContract.size() > 0) currentNumber = Integer.valueOf(lastExistingContract[0].Internal_Contract_Number__c);
		
		for(Contract_xBU__c contract : triggerNew){
			
			if(contract.Internal_Contract_Number__c == null){
			
				contract.Internal_Contract_Number__c = ++currentNumber;							
				contract.Contract_Number__c = String.valueOf(contract.Internal_Contract_Number__c) + suffix;
			}			
		}
	}


	//------------------------------------------------------------------------------------------------------------------------------------------------------
	// When the Old_Contract__c on a Contract_XBU__c is populated, we update the Contract_XBU__c record that has been selected in the Old_Contract__c field:
	//	- update the New_Contract__c field with the Contract_XBU__c which has been updated by the user
	//	- set Reminder_Stop__c to true to stop implemented workflows
	// When the Old_Contract__c on the updated Contract_XBU__c already contained a value or the Old_Contract__c is cleared, we update the Contract_XBU__c record that was previous selected in this field:
	//	- clear the New_Contract__c field
	//	- set Reminder_Stop__c to false
	//------------------------------------------------------------------------------------------------------------------------------------------------------
	public static void updateOldContract(List<Contract_xBU__c> lstTriggerNew, Map<Id, Contract_xBU__c> mapTriggerOld){

        if (bl_Trigger_Deactivation.isTriggerDeactivated('contractxBU_updateOldContract')) return;

		List<Contract_xBU__c> lstContract_Processing = new List<Contract_xBU__c>();
		Set<Id> setID_ContractOld_Clear = new Set<Id>();
		for (Contract_xBU__c oContract : lstTriggerNew){

			if (mapTriggerOld == null){			
				
				if (oContract.Old_Contract__c != null){

					lstContract_Processing.add(oContract);

				}

			}else{

				if (oContract.Old_Contract__c != mapTriggerOld.get(oContract.Id).Old_Contract__c){
					
					// Old_Contract__c is changed
					System.debug('**BC** oContract : ' + oContract);
					System.debug('**BC** oContract.Old_Contract__c : ' + oContract.Old_Contract__c);
					System.debug('**BC** mapTriggerOld.get(oContract.Id).Old_Contract__c : ' + mapTriggerOld.get(oContract.Id).Old_Contract__c);
				
					if (mapTriggerOld.get(oContract.Id).Old_Contract__c == null){

						// Old_Contract__c is populated - process the new value
						lstContract_Processing.add(oContract); 

					}else{

						if (oContract.Old_Contract__c == null){

							// Old_Contract__c is cleared - we need to clear New_Contract__c on the previous Old_Contract__c record
							setID_ContractOld_Clear.add(mapTriggerOld.get(oContract.Id).Old_Contract__c);

						}else{

							// Old_Contract__c is changed - we need to clear New_Contract__c on the previous Old_Contract__c record and process the changed Contract
							setID_ContractOld_Clear.add(mapTriggerOld.get(oContract.Id).Old_Contract__c);
							lstContract_Processing.add(oContract); 

						}


					}

				}

			}

		}

		List<Contract_xBU__c> lstContractOld_Update = new List<Contract_xBU__c>();
		if (lstContract_Processing.size() > 0){

			for (Contract_xBU__c oContract : lstContract_Processing){

				lstContractOld_Update.add(new Contract_xBU__c(Id = oContract.Old_Contract__c, New_Contract__c = oContract.Id, Reminder_Stop__c = true));

			}

		}

		if (setID_ContractOld_Clear.size() > 0){

			for (Id id_Contract : setID_ContractOld_Clear){

				lstContractOld_Update.add(new Contract_xBU__c(Id = id_Contract, New_Contract__c = null, Reminder_Stop__c = false));

			}

		}

		if (lstContractOld_Update.size() > 0) update lstContractOld_Update;

	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------

}