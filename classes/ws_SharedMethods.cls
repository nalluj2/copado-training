/*
 * Description      : Class for shared methods related to interfacing
 * Author           : Patrick Brinksma
 * Created Date     : 09-08-2013
 */
public class ws_SharedMethods {

	public static Map<String, Map<String, String>> sapIntegrationMappings = null;
	

	/*
	 * Description      : Method to retrieve a field mapping for the SAP integration. The object and field identify the list of mappings
	 *                    and the fromValue inside the list identifies the value being mapped from. Note that the mappings have been made
	 *                    bi-directional, so a user can ask for either value in the mapping and the other value will be returned.
	 *                    For example if the mapping was - ValueTo : ValueFrom. If 'ValueTo' was passed in 'ValueFrom' would be returned.
	 *                    If 'ValueFrom' was passed in 'ValueTo' would be returned.
	 * Author           : Tom Ansley
	 * Created Date     : 12-29-2015
	 */
	public static String getSAPMapping(String obj, String field, String fromValue)
	{
		
		String strValue = null;
		
		//if the mappings haven't been processed yet then get all mappings and put them
		//into the following format - KEY   - API_OBJECT_NAME + API_FIELD_NAME
		//                            VALUE - (FROM, TO)
		if (ws_SharedMethods.sapIntegrationMappings == null)
		{
			sapIntegrationMappings = new Map<String, Map<String, String>>();
			
			//for all mappings in custom settings
			for (SAP_Interface_Mappings__c mapping : SAP_Interface_Mappings__c.getAll().values())
			{
				
				//get the name of the list of fields by concatenating the object and field together.
				String name = mapping.API_Object_Name__c + mapping.API_Field_Name__c;
				
				Map<String, String> objMappings = sapIntegrationMappings.get(name);
				
				if (objMappings == null)
				{
					objMappings = new Map<String, String>();
					sapIntegrationMappings.put(name, objMappings);
				}
					
				objMappings.put(mapping.From__c, mapping.To__c);
				//objMappings.put(mapping.To__c, mapping.From__c);
			}

		}
		
		//use the given object and field to get that fields mappings
		Map<String, String> objMappings = sapIntegrationMappings.get(obj + field);
		
		if (objMappings != null)
		{
			strValue = objMappings.get(fromValue);
			
			//if (strValue == null) strValue = '';
		}
		
		return strValue;
	}
	
	public static Set<String> getSupportedTypes(String objName)
	{
		
		Set<String> supportedTypes = new Set<String>();
		
		for(SMAX_Supported_Type__c supportedType : [Select Type__c from SMAX_Supported_Type__c where Object__c = :objName]){
			
			supportedTypes.add(supportedType.Type__c);
		}
		
		return supportedTypes;
	}

	/*
	 * Description      : Method to retrieve endpoint and timeout from Custom Setting WebServiceEndPoints__c
	 * Author           : Patrick Brinksma
	 * Created Date     : 10-08-2013
	 */
	public static Map<String, String> getOutboundWebServiceEndpointAndTimeout(String interfaceName){
		// Get all entries for Web Service Endpoints
		Map<String, WebServiceEndPoints__c> mapOfNameToEndpoint = WebServiceEndPoints__c.getAll();
		// 15 character Org Id is used
		String orgId = Userinfo.getOrganizationId().left(15);
		Map<String, String> mapOfValues = new Map<String, String>();
		// Set return if interface name and org id are identical
		for (WebServiceEndPoints__c thisWs : mapOfNameToEndpoint.values()){
			if (thisWs.Interface_Name__c == interfaceName && thisWs.Org_Id__c == orgId){
				mapOfValues.put('Endpoint', thisWs.Endpoint__c); 
				mapOfValues.put('Timeout', String.valueof(thisWs.Timeout__c.intValue()));
			}
		}
		return mapOfValues;
	}

	/*
	 * Description      : Method to validate if inbound message is allowed to be processed
	 * Author           : Patrick Brinksma
	 * Created Date     : 09-08-2013
	 */
	public static Boolean processInboundMessage(String msgName){
		return processInboundMessage(msgName, true);
	}

 	public static Boolean processInboundMessage(String msgName, Boolean validateAll){
		// First validate if the message needs to be processed
		Boolean processMsg = true;
		// Custom setting can hold an All instance, which determines for every SAP interface
		Map<String, SAP_Interface_Settings__c> mapOfCSToValues = SAP_Interface_Settings__c.getAll();
		if (mapOfCSToValues.get('All') <> null){
			if (mapOfCSToValues.get('All').Disable_SAP_to_SFDC__c == true){
				processMsg = false;
			} 
		}
		// Custom setting can hold a specific instance for this interface, which determines for every SAP interface
		if (mapOfCSToValues.get(msgName) <> null){
			if (mapOfCSToValues.get(msgName).Disable_SAP_to_SFDC__c == true){
				processMsg = false;
			} 
		} 		
		return processMsg;
 	}
 	
	/*
	 * Description      : Method to validate if inbound message is allowed to be processed
	 * Author           : Patrick Brinksma
	 * Created Date     : 09-08-2013
	 */
	public static Boolean sendOutboundMessage(String msgName){
		return sendOutboundMessage(msgName, true);
	}

 	public static Boolean sendOutboundMessage(String msgName, Boolean validateAll){
		// First validate if the interface needs to be send
		Boolean sendMsg = true;
		// Custom setting can hold an All instance, which determines for every SAP interface
		Map<String, SAP_Interface_Settings__c> mapOfCSToValues = SAP_Interface_Settings__c.getAll();
		if (mapOfCSToValues.get('All') <> null){
			if (mapOfCSToValues.get('All').Disable_SFDC_to_SAP__c == true){
				sendMsg = false;
			} 
		}
		// Custom setting can hold a specific instance for this interface, which determines for every SAP interface
		if (mapOfCSToValues.get(msgName) <> null){
			if (mapOfCSToValues.get(msgName).Disable_SFDC_to_SAP__c == true){
				sendMsg = false;
			} 
		} 		
		return sendMsg;
 	} 	

	/*
	 * Description      : Method to return Profile Name under which the inbound SAP interface runs
	 * Author           : Patrick Brinksma
	 * Created Date     : 11-08-2013
	 */ 
	public static String getSAPInterfaceProfileName(String interfaceName){
		SAP_Interface_Settings__c settings = SAP_Interface_Settings__c.getInstance(interfaceName);
		if (null!=settings){
			return settings.Interface_User_Profile_Name__c;
		}else{
			return '';
		}
	}

	/*
	 * Description      : Method to query for Opportunity details given an Id
	 * Author           : Patrick Brinksma
	 * Created Date     : 09-08-2013
	 */ 
	public static List<Opportunity> getOpportunityDetails(Id opptyId){
		return [select 
                    Account.SAP_ID__c,
                    Account.Account_Country_vs__c, 
                    Physician_Account_ID__r.SAP_ID__c,
                    Physician_Account_ID__r.Account_Country_vs__c,  
                    Automatic_Creation_Bool__c, 
                    CloseDate, 
                    CreatedDate, 
                    CurrencyIsoCode, 
                    Department_Text__c, 
                    Description, 
                    Dist_Err_Details__c,
                    Distribution_Lock__c, 
                    Distribution_Status_Text__c,
                    Id, 
                    LastModifiedDate,
                    Name, 
                    Name_2_Text__c, 
                    Origin_Text__c, 
                    OwnerId,
                    Owner.SAP_ID__c, 
                    Primary_Health_Insurer__c,
                    Primary_Health_Insurer__r.SAP_Id__c, 
                    Requested_Delivery_Date__c, 
                    SAP_Created_Date__c, 
                    SAP_ID_Text__c,
                    SAP_Last_Modified_Date__c, 
                    SAP_Order_GUID__c,
                    SAP_Phase__c,
                    SAP_Reason__c,
                    SAP_Status__c,
                    SAP_Ship_To_ID__c, 
                    SAP_Ship_To_ID__r.SAP_ID__c,
                    SAP_Sold_To_ID__c,
                    SAP_Sold_To_ID__r.SAP_ID__c,
                    Training_Date__c,
                    Trial_End_Date__c, 
                    Trial_Start_Date__c, 
                    Type
                from Opportunity 
                where Id = :opptyId];
	}

	/*
	 * Description      : Method to query for OpportunityLineItem details given an Id
	 * Author           : Patrick Brinksma
	 * Created Date     : 09-08-2013
	 */ 	
	public static List<OpportunityLineItem> getOpportunityLineItemDetails(Id opptyId){
		return [select 
					CreatedDate, 
					Id, 
					LastModifiedDate,
					PricebookEntry.Product2.Unit_Of_Measure_Text__c, 
					PricebookEntry.ProductCode, 
					Quantity, 
					SAP_Created_Date__c, 
					SAP_Item_Category_Text__c, 
					SAP_Item_ID_Text__c,
					SAP_Item_GUID__c,
					SAP_Item_SerialNo_Text__c,
					SAP_Last_Modified_Date__c, 
					SAP_Warranty_Start_Date__c, 
					Status__c, 
					UnitPrice,
					Warranty_End_Date__c
                from OpportunityLineItem 
                where OpportunityId = :opptyId];
	}
	
	/*
	 * Description      : Method to map Sales Stage and SAP values from SAP to SFDC
	 * Author           : Patrick Brinksma
	 * Created Date     : 11-08-2013
	 *--------------------------------------------------------------------------------
	 * Description      : Added the Stage/Phase value "Trial"
	 * Modifier         : Bart Caelen
	 * Created Date     : 20140225
	 *--------------------------------------------------------------------------------
	 * Description      : CR-4494 - Applied new mapping logic
	 * Modifier         : Bart Caelen
	 * Created Date     : 20140422
	 */ 	
	public static void mapOpportunitySalesStageSAPToSFDC(Opportunity thisOppty){
		//---------------------------------------------------
		// SAP to SFDC
		//---------------------------------------------------

		// Clear the REASON values
		thisOppty.Reason_Cancelled__c = '';
		thisOppty.Reason_Hold__c = '';
		thisOppty.Reason_Lost__c = '';
		thisOppty.Reason_Open__c = '';
		thisOppty.Reason_Won__c = '';
		
		// Map Status to StageName (if SAP Status is Open get it from Phase)
		String sapStatus = thisOppty.SAP_Status__c;
		String sapPhase = thisOppty.SAP_Phase__c;
		if (null != sapStatus){

			if (sapStatus.equalsIgnoreCase('Open')){
				if (null != sapPhase){
					if (sapPhase.equalsIgnoreCase('New')){
						thisOppty.StageName = 'New';
					} else if (sapPhase.equalsIgnoreCase('Preparation')){
						thisOppty.StageName = 'Preparation';	
					} else if (sapPhase.equalsIgnoreCase('Approval Pending')){
						thisOppty.StageName = 'Approval Pending';	
					} else if (sapPhase.equalsIgnoreCase('Approved')){
						thisOppty.StageName = 'Approved';	
					} else if (sapPhase.equalsIgnoreCase('Trial')){
						thisOppty.StageName = 'Trial';	
					} else if (sapPhase.equalsIgnoreCase('Order Entry')){
						thisOppty.StageName = 'Order Entry';	
					} else if (sapPhase.equalsIgnoreCase('Trial Approved')){
						thisOppty.StageName = 'Trial Approved';	
					} else if (sapPhase.equalsIgnoreCase('Trial Appr. Pending')){
						thisOppty.StageName = 'Trial Approval Pending';	
					}
				}
				thisOppty.Reason_Open__c = thisOppty.SAP_Reason__c;
			} else if (sapStatus.equalsIgnoreCase('Hold')){
				thisOppty.StageName = 'Hold';
				thisOppty.Reason_Hold__c = thisOppty.SAP_Reason__c;
			} else if (sapStatus.equalsIgnoreCase('Closed / Won')) {
				thisOppty.StageName = 'Closed Won';
				thisOppty.Reason_Won__c = thisOppty.SAP_Reason__c;
			} else if (sapStatus.equalsIgnoreCase('Closed / Lost')) {
				thisOppty.StageName = 'Closed Lost';
				thisOppty.Reason_Lost__c = thisOppty.SAP_Reason__c;
			} else if (sapStatus.equalsIgnoreCase('Closed / Cancelled')) {
				thisOppty.StageName = 'Closed Cancelled';
				thisOppty.Reason_Cancelled__c = thisOppty.SAP_Reason__c;
			}
		}
		
		System.debug('*** thisOppty.StageName: ' + thisOppty.StageName);	
	}

	/*
	 * Description      : Method to map Sales Stage and SAP values from SFDC to SAP
	 * Author           : Patrick Brinksma
	 * Created Date     : 11-08-2013
	 *--------------------------------------------------------------------------------
	 * Description      : Added the Stage/Phase value "Trial"
	 * Modifier         : Bart Caelen
	 * Created Date     : 20140225
	 *--------------------------------------------------------------------------------
	 * Description      : CR-4494 - Applied new mapping logic
	 * Modifier         : Bart Caelen
	 * Created Date     : 20140422
	 */ 	
	public static void mapOpportunitySalesStageSFDCToSAP(Opportunity thisOppty){
		//---------------------------------------------------
		// SFDC to SAP
		//---------------------------------------------------

		// Clear the SAP REASON value
		thisOppty.SAP_Reason__c = '';

		String tStageName = thisOppty.StageName;	
		if (tStageName != null){
			if (
				tStageName.equalsIgnoreCase('New') 
				|| tStageName.equalsIgnoreCase('Preparation') 
				|| tStageName.equalsIgnoreCase('Approval Pending') 
				|| tStageName.equalsIgnoreCase('Approved') 
				|| tStageName.equalsIgnoreCase('Trial') 
				|| tStageName.equalsIgnoreCase('Order Entry')
				|| tStageName.equalsIgnoreCase('Trial Approved')
			){
				thisOppty.SAP_Status__c = 'Open';
				thisOppty.SAP_Phase__c = tStageName;
				thisOppty.SAP_Reason__c = thisOppty.Reason_Open__c;
				
			} else if (tStageName.equalsIgnoreCase('Trial Approval Pending')){
				
				thisOppty.SAP_Status__c = 'Open';
				thisOppty.SAP_Phase__c = 'Trial Appr. Pending';
				thisOppty.SAP_Reason__c = thisOppty.Reason_Open__c;
					
			} else if (tStageName.equalsIgnoreCase('Hold')){
				thisOppty.SAP_Status__c = 'Hold';
				thisOppty.SAP_Reason__c = thisOppty.Reason_Hold__c;
			} else if (tStageName.equalsIgnoreCase('Closed Won')){
				thisOppty.SAP_Status__c = 'Closed / Won';
				thisOppty.SAP_Reason__c = thisOppty.Reason_Won__c;
			} else if (tStageName.equalsIgnoreCase('Closed Lost')){
				thisOppty.SAP_Status__c = 'Closed / Lost';
				thisOppty.SAP_Reason__c = thisOppty.Reason_Lost__c;
			} else if (tStageName.equalsIgnoreCase('Closed Cancelled')){
				thisOppty.SAP_Status__c = 'Closed / Cancelled';
				thisOppty.SAP_Reason__c = thisOppty.Reason_Cancelled__c;
			}
		}		
	}
}