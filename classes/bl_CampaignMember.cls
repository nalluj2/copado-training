/*
 *      Description : This class centralizes business logic related to CampaignMember
 *
 *      Author = Christophe Saenen
 */
 
public with sharing class bl_CampaignMember {

	public static void deleteFollowUpTask(Map<CampaignMember, List<string>> deleteMap) {
		system.debug('### deleteFollowUpTask=');
		string soql = 'Select Id From Task Where subject = \'Perform training follow-up action\' and ';
		
		for(CampaignMember cmVal : deleteMap.keyset()) {
			list<string> valLst = deleteMap.get(cmVal);
//			if(!string.isBlank(valLst[1])) 
			soql += '(WhoId = \'' + valLst[0] + '\' and WhatId = \'' + valLst[1] + '\') Or '; // whatid is empty for leads
		}
		
		soql = soql.substring(0, soql.length()-3);
		system.debug('### soql=' + soql);
		
		List<Task> taskLst = Database.query(soql);
		
		system.debug('### taskLst.size()=' + taskLst.size());
		
		delete taskLst;
	}

	public static void createFollowUpTask(List<CampaignMember> triggerNew) {
       	String tCampaignType = 'Training & Education';
        String tTaskSubject = 'Perform training follow-up action';

        // Get the Campaign data for the processing Campaign Members
        Set<Id> setId_Campaign = new Set<Id>();
        for (CampaignMember oCampaignMember : triggerNew){
            setId_Campaign.add(oCampaignMember.CampaignId);
        }
        // Only select the Campaigns that are needed in a Map
        //  - CEE Campaign (RecordType)
        //  - Automatically_create_follow_up_tasks__c = true
        //  - Type = 'Training and Education'
        // CR-9923 Christophe Saenen - removed recordtype filter
        Map<Id, Campaign> mapCampaign = new Map<Id, Campaign>(
            [   SELECT Id, EndDate, Type 
                FROM Campaign 
                WHERE 
                    Id = :setId_Campaign 
                    AND Automatically_create_follow_up_tasks__c = true
                    AND Type = :tCampaignType 
//                    AND (RecordType.Name like 'CEE%' OR RecordType.Name like 'MEA%')
            ]);
            
        // Create Follow Up Tasks
        List<Task> lstTask = new List<Task>();
        for (CampaignMember oCampaignMember : triggerNew){
            // Validate if we need to create a Task for the Campaign Member if the related Campaign is found in the previously created map
            if (mapCampaign.containsKey(oCampaignMember.CampaignId)){
                Campaign oCampaign = mapCampaign.get(oCampaignMember.CampaignId);
    
                Date dActivityDate = oCampaign.EndDate.addDays(21);
                if (dActivityDate < Date.today()){
                    dActivityDate = Date.today();
                }
                    
                Task oTask = new Task();
                 oTask.ActivityDate = dActivityDate;
                 oTask.ReminderDateTime = null;
                 oTask.Subject = tTaskSubject;
                 oTask.OwnerId = oCampaignMember.CreatedById;
                 // CR-9923 Christophe Saenen - WhatId ipv Campaign__c
                 if(oCampaignMember.ContactId!=null) oTask.WhatId  = oCampaign.Id;
                 oTask.WhoId = clsUtil.isNull(oCampaignMember.ContactId, oCampaignMember.LeadId);
                 lstTask.add(oTask);
            }
        }

        if (lstTask.size() > 0){            
            Database.DMLOptions oDMLOptions = new Database.DMLOptions(); 
            oDMLOptions.EmailHeader.TriggerUserEmail = TRUE;    // Send out notification email
            oDMLOptions.OptAllOrNone = FALSE;   // Don't block the creation of an Campaign Member if the Task couldn't be generated

            List<Database.SaveResult> lstSaveResult = Database.Insert(lstTask, oDMLOptions);
            for (Database.SaveResult oSaveResult : lstSaveResult){
                if (!oSaveResult.isSuccess()){
                    // Operation failed - get all errors and log error
                    for (Database.Error oError : oSaveResult.getErrors()){
                        clsUtil.debug('The following error has occurred while creating a Follow Up Task in tr_CampaignMember_CreateFollowUpTask : ' + oError.getStatusCode() + ' - ' + oError.getMessage());
                    }
                }
            }
        }
	}
    
}