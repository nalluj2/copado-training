@RestResource(urlMapping='/ServiceOrderService/*')
global class ws_ServiceOrderService {

	@HttpPost
  	global static ServiceOrderResponse upsertServiceOrder() {
  		
  		RestRequest req = RestContext.request;
			
		String body = req.requestBody.toString();
    	ServiceOrderRequest request = (ServiceOrderRequest)System.Json.deserialize(body, ServiceOrderRequest.class);
  		
  		ServiceOrderResponse resp = new ServiceOrderResponse();
  		
  		clsUtil.debug('INIT - req : ' + req);
  		clsUtil.debug('INIT - req.requestBody : ' + req.requestBody);
  		clsUtil.debug('INIT - body : ' + body);
  		clsUtil.debug('INIT - request : ' + request);
  		clsUtil.debug('INIT - resp : ' + resp);

  		Savepoint sp = Database.setSavePoint();
  		
  		try{
  		
  			// Get the Business Unit based on the provided Therapy - CR-12286
			List<Therapy__c> lstTherapy = [SELECT Business_Unit__c FROM Therapy__c WHERE Id = :request.therapyId LIMIT 1];

	  		// Create/Update Call Record record
	  		Call_Records__c callRecord = new Call_Records__c();
		  		callRecord.Mobile_Id__c = request.serviceOrderId;
		  		callRecord.Start_Time__c = request.startDateTime;
		  		callRecord.Call_Date__c = request.startDateTime.date();  		
		  		callRecord.Call_Travel_Time_Minutes__c = request.travelTime;
		  		callRecord.Wait_Time__c = request.waitingTime;
		  		callRecord.Comments__c = request.notes;
		  		callRecord.OwnerId = request.userId;
		  		callRecord.RecordTypeId = RecordTypeMedtronic.CallRecord('Business_Unit').Id;
		  		callRecord.No_Show__c = request.noShow;
		  		callRecord.Net_Promoter_Score__c = request.netPromoterScore;
		  		callRecord.Scheduled_Activity__c = request.sourceCaseId;
		  		if (lstTherapy.size() > 0){ // CR-12286
			  		callRecord.Business_Unit__c = lstTherapy[0].Business_Unit__c;
		  		}
	  		upsert callRecord Mobile_Id__c;
	  		

	  		//resp.callRecord = callRecord;
	  		resp.id =callRecord.id;
	  		
	  		// Create/Update Call Contact record
	  		Contact_Visit_Report__c attendedContact = new Contact_Visit_Report__c();
	  		attendedContact.Call_Records__c = callRecord.Id;
	  		
	  		List<Contact_Visit_Report__c> existingContacts = [Select Id from Contact_Visit_Report__c where Call_Records__r.Mobile_Id__c = :request.serviceOrderId];
	  		if(existingContacts.size() > 0){
	  			attendedContact.Id = existingContacts[0].Id;
	  		}
	  		
	  		attendedContact.Attending_Contact__c = request.contactId;
	  		attendedContact.Attending_Affiliated_Account__c = request.accountId;
	  		
	  		upsert attendedContact;
	  		
	  		List<Contact_Visit_Report__c> attContacts = new List<Contact_Visit_Report__c>();
	  		attContacts.add(attendedContact);
	  		//resp.attendedContacts = attContacts;
	  		
			// Create/Update Call Topic record
			Call_Topics__c callTopic = new Call_Topics__c();
			callTopic.Call_Records__c = callRecord.Id;
	  		
	  		List<Call_Topics__c> existingTopics = [Select Id from Call_Topics__c where Call_Records__r.Mobile_Id__c = :request.serviceOrderId];
	  		if(existingTopics.size() > 0){
				//delete all existing and recreate in next steps  			
	  			bl_CallManagement.deleteCallTopicCascade(existingTopics);
	  		}
	  		if (null!=request.serviceType && request.serviceType.length()>0){
	  			callTopic.Call_Activity_Type__c = request.serviceType;
	  		}
	  		callTopic.Call_Topic_Duration__c = request.duration;
	  		callTopic.Call_Topic_Therapy__c = request.therapyId;
	  		  		
	  		System.debug('CallTopic to upsert: '+callTopic);
	  		upsert CallTopic;
	  		
	  		List<ServiceOrderProduct> soProducts = request.serviceOrderProducts;
	  		
	  		List<Call_Topic_Products__c> ctProducts = new List<Call_Topic_Products__c>();
	  		if (soProducts.size()>0){
	  			
	  			for (ServiceOrderProduct sop : soProducts){
		  			Call_Topic_Products__c ctProduct =  new Call_Topic_Products__c();
			  			ctProduct.Call_Topic__c = callTopic.Id;
			  			ctProduct.Product__c = sop.productId;
		  			ctProducts.add(ctProduct);
	  			}
	  		}
	  		
	  		if (ctProducts.size()>0){
	  			upsert ctProducts;
	  		}
	  		
	  		List<String> productIds = new List<String>();
	  		
	  		for (Call_Topic_Products__c ctp : ctProducts){
	  			productIds.add(ctp.id);
	  		}
	  		
	  		resp.productIds = productIds;
	  		
	  		/* To be completed once everything has been analyzed and designed properly */
	  		//-BC - 20150831 - CR-8648 - START
	  		// Create/Update WorkOrders records
	  		RecordType oRT_Workorder_ServiceOrder = clsUtil.getRecordTypeByDevName('Workorder__c', 'EUR_Service_Order');
	  		WorkOrder__c oWorkOrder = new WorkOrder__c();
	  			oWorkOrder.RecordTypeId = oRT_Workorder_ServiceOrder.Id;
		  		oWorkOrder.Account__c = request.accountId;
		  		oWorkOrder.Status__c = 'CLOSED';
		  		oWorkOrder.Date_Closed__c = request.startDateTime.date();
		  		oWorkOrder.Travel_Hours__c = Decimal.valueOf(clsUtil.isIntegerNull(request.travelTime, 0)) / 60;
		  		oWorkOrder.Wait_Hours__c = Decimal.valueOf(clsUtil.isIntegerNull(request.waitingTime, 0)) / 60;
		  		oWorkOrder.Working_Hours__c = Decimal.valueOf(clsUtil.isIntegerNull(request.duration, 0)) / 60;
		  		oWorkOrder.Resolution_Notes__c = request.notes;
		  		oWorkOrder.Service_Order_Id__c = request.serviceorderid;
		  		oWorkOrder.No_Show__c = request.noShow;
		  		oWorkOrder.TSEngineer__c = request.userId;
		  		oWorkOrder.Call_Record__c = callRecord.id;
		  		oWorkOrder.Product_Group__c = request.productGroupId;
		  		oWorkOrder.Support_Type__c = request.supportType;
	  		upsert oWorkOrder;

		  		callRecord.WorkOrder__c = oWorkOrder.Id;
			update callRecord;
	  		
	  		// Create/Update WorkOrderItems records
	  		RecordType oRT_WorkorderSparepart_ServiceOrder = clsUtil.getRecordTypeByDevName('Workorder_Sparepart__c', 'EUR_Service_Order');
	  		List<Workorder_Sparepart__c> lstWorkorderSparepart = new List<Workorder_Sparepart__c>();
	  		for(ServiceOrderProduct oServiceOrderProduct : request.serviceOrderProducts){
	  			Workorder_Sparepart__c oWorkorderSparepart = new Workorder_Sparepart__c();
		  			oWorkorderSparepart.RecordTypeId = oRT_WorkorderSparepart_ServiceOrder.Id;
		  			oWorkorderSparepart.Workorder__c = oWorkOrder.Id;
		  			oWorkorderSparepart.Sparepart_Installed__c	= oServiceOrderProduct.productId;
		  			oWorkorderSparepart.Installed_Quantity__c	= oServiceOrderProduct.quantity;
		  			oWorkorderSparepart.Remarks__c 	= oServiceOrderProduct.comments;
		  			oWorkorderSparepart.Installed_Serial_Number__c = oServiceOrderProduct.serialNumber;
		  			oWorkorderSparepart.Comment_Type_Description__c = oServiceOrderProduct.commentTypeDescription;
	  			lstWorkorderSparepart.add(oWorkorderSparepart);
	  		}
	  		upsert lstWorkorderSparepart;
	  		//-BC - 20150831 - CR-8648 - STOP
	  		
	  		resp.success = true;
	  		
  		}catch(Exception e){
  			
  			System.debug('Error on line ' + e.getLineNumber() + ' : ' + e);
  			Database.rollback(sp);
  			
  			resp.success = false;
  			resp.message = e.getMessage();
  		}
  		  		  	
  		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'ServiceOrder' ,req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
  			
		return resp;    		
  	}
  	
	global class ServiceOrderRequest{
		
		global String serviceOrderId {get; set;}
		
		global String accountId {get; set;}
		global String contactId {get; set;} 
		global String serviceType {get; set;}
		global String supportType {get; set;}
		global String therapyId {get; set;}
		global String productGroupId {get; set;}
		
		global DateTime startDateTime {get; set;}		
		global Integer duration {get; set;}
		global Integer travelTime {get; set;}
		global Integer waitingTime {get; set;}
		global Boolean noShow {get;set;}
		global Integer netPromoterScore {get; set;}
		
		global String notes {get; set;}		
		global String patientHospitalId {get; set;}
		global String poNumber {get; set;}
				
		global String userId {get; set;}
		global String sourceCaseId {get; set;}
		
		global List<ServiceOrderProduct> serviceOrderProducts {get; set;}	
	}
	
	global class ServiceOrderProduct{
		
		global String productId {get; set;}
		global Integer quantity {get; set;}
		global String commentTypeDescription { get;set; }
		
		global String serialNumber {get; set;}
		global String comments {get; set;}
	}
	
	global class ServiceOrderResponse{
		
		public List<String> productIds {get;set;}
		public String id{get;set;}
		public Boolean success {get; set;}
		public String message {get; set;}
	}
}