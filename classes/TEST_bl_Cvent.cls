//--------------------------------------------------------------------------------------------------------------------
//  Description : This TEST APEX Class is used to test the logic in bl_Cvent
//  Version     : 1.0
//  Author      : Bart Caelen
//  Date        : 20150508
//--------------------------------------------------------------------------------------------------------------------
@isTest
private class TEST_bl_Cvent {

    static Integer iBatchSize = 100;

    private static User oUser;

    private static List<CampaignMemberStatus__c> lstCampaignMemberStatus;

    @isTest static void createTestData() {

        oUser = clsTestData_User.createUser_SystemAdministrator('tadm000', true);

        System.runAs(oUser){

            Id id_RecordType_Campaign_Cvent = clsUtil.getRecordTypeByDevName('Campaign', 'CVent_EMEA').Id;
            Id id_RecordType_Campaign_EuropexBU = clsUtil.getRecordTypeByDevName('Campaign', 'Europe_xBU').Id;

            List<Profile> lstProfile = [SELECT Id, Name FROM Profile ORDER BY Name];
            Map<String, String> mapProfileId = new Map<String, String>();
            for (Profile oProfile : lstProfile){
                mapProfileId.put(oProfile.Name.toUpperCase(), String.valueOf(oProfile.Id).left(15));
            }
            SystemAdministratorProfileId__c oSystemAdminstratorProfileId = new SystemAdministratorProfileId__c();
                oSystemAdminstratorProfileId.Name = 'Default';
                oSystemAdminstratorProfileId.CVent__c = mapProfileId.get('CVENT EMEA');
                oSystemAdminstratorProfileId.SystemAdministratorMDT__c = mapProfileId.get('SYSTEM ADMINISTRATOR MDT');
                oSystemAdminstratorProfileId.SystemAdministrator__c = mapProfileId.get('SYSTEM ADMINISTRATOR');
                oSystemAdminstratorProfileId.ITBusinessAnalyst__c = mapProfileId.get('IT BUSINESS ANALYST');
                oSystemAdminstratorProfileId.MMX_Interface__c = mapProfileId.get('MMX INTERFACE');
                oSystemAdminstratorProfileId.SAPInterface__c = mapProfileId.get('SAP INTERFACE');
            insert oSystemAdminstratorProfileId;

            clsTestData_CustomSetting.mapCampaignMemberStatus_Responded = new Map<String, Boolean>{'Attended'=>true, 'Registered'=>true, 'No Show.'=>false, 'CancelledWrong'=>true};
            lstCampaignMemberStatus = clsTestData_CustomSetting.createCampaignMemberStatus(true, 'RecordtypeId', id_RecordType_Campaign_Cvent);

            // Create Account Data
            clsTestData.iRecord_Account = 1;
            clsTestData.createAccountData();

            // Create Contact Data
            clsTestData.iRecord_Contact = 10;
            clsTestData.createContactData();

            // Create Campaign Data
    //        clsTestData.createCampaignData(true, id_RecordType_Campaign_EuropexBU);
            bl_Cvent.t_Test_Event_Id = '4E6F80BA-F3D0-48C4-A97B-0B144D6B73B4';
            clsTestData.createCampaignData(false, id_RecordType_Campaign_Cvent);
            clsTestData.oMain_Campaign.Cvent_Event_Id__c = bl_Cvent.t_Test_Event_Id;
            insert clsTestData.oMain_Campaign;

            bl_Cvent.t_Test_Campaign_Id = clsTestData.oMain_Campaign.Id;

            // Create CampaignMember Data
            clsTestData.iRecord_CampaignMember = 10;
            clsTestData.createCampaignMemberData(true);

            CampaignMember oCampaignMember = [SELECT Id, ContactId, Contact.Email FROM CampaignMember WHERE Id = : clsTestData.oMain_CampaignMember.Id];
            bl_Cvent.t_Test_CampaignMember_Email = oCampaignMember.Contact.Email;
            bl_Cvent.t_Test_Contact_Id = oCampaignMember.ContactId;

            // Update the Campaign RecordType to CVent
            // We can't create CampaignMembers in SFDC for CVent Campaigns (validation rule)
                clsTestData.oMain_Campaign.RecordtypeId = id_RecordType_Campaign_Cvent;
                clsTestData.oMain_Campaign.Cvent_Event_Id__c = bl_Cvent.t_Test_Event_Id;
            update clsTestData.oMain_Campaign;

        }

    }

    @isTest static void test_processCampaign() {

        // Create Test Data
        createTestData();

        // Load Test Data
        Id id_RecordType_Campaign_Cvent = clsUtil.getRecordTypeByDevName('Campaign', 'CVent_EMEA').Id;

        List<String> lstCampaignId = new List<String>();
        List<Campaign> lstCampaign = 
            [
                SELECT Id, Cvent_Event_Id__c, Status
                FROM Campaign
                WHERE RecordtypeId = :id_RecordType_Campaign_Cvent
                AND Status != 'Completed'
            ];
        for (Campaign oCampaign : lstCampaign){
            lstCampaignId.add(oCampaign.Id);
        }


        Test.startTest();

        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new TEST_bl_Cvent_Mock());  

        // Process Collected Campaigns
        System.runAs(oUser){
            bl_Cvent.processCampaign(lstCampaign, 'CVENT_EMEA');
        }

        Test.stopTest();


    }
    
    @isTest static void test_processCampaignMember() {

        // Create Test Data
        createTestData();

        // Load Test Data
        Id id_RecordType_Campaign_Cvent = clsUtil.getRecordTypeByDevName('Campaign', 'CVent_EMEA').Id;

        List<String> lstCampaignMemberId = new List<String>();
        List<CampaignMember> lstCampaignMember = 
            [
                SELECT Id, ContactId, Contact.Email, LeadId, Lead.Email, Registration_Type__c, CampaignId, Campaign.Cvent_Event_Id__c, Campaign.Status, Campaign.StartDate, Campaign.EndDate, Status 
                FROM CampaignMember
                WHERE Campaign.RecordtypeId = :id_RecordType_Campaign_Cvent
                AND Campaign.Status != 'Completed'
                ORDER BY CampaignId
            ];
        for (CampaignMember oCampaignMember : lstCampaignMember){
            lstCampaignMemberId.add(oCampaignMember.Id);
        }


        Test.startTest();
        
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new TEST_bl_Cvent_Mock());  

        System.runAs(oUser){
            // Process Collected CampaignMembers
            bl_Cvent.processCampaignMember(lstCampaignMember, 'CVENT_EMEA');
        }

        Test.stopTest();
    }

    @isTest static void test_processCampaignMember_Id() {

        // Create Test Data
        createTestData();

        // Load Test Data

        Id id_RecordType_Campaign_Cvent = clsUtil.getRecordTypeByDevName('Campaign', 'CVent_EMEA').Id;

        List<String> lstCampaignMemberId = new List<String>();
        List<CampaignMember> lstCampaignMember = 
            [
                SELECT Id, ContactId, Contact.Email, LeadId, Lead.Email, Registration_Type__c, CampaignId, Campaign.Cvent_Event_Id__c, Campaign.Status, Campaign.StartDate, Campaign.EndDate, Status
                FROM CampaignMember
                WHERE Campaign.RecordtypeId = :id_RecordType_Campaign_Cvent
                AND Campaign.Status != 'Completed'
                ORDER BY CampaignId
            ];
        for (CampaignMember oCampaignMember : lstCampaignMember){
            lstCampaignMemberId.add(oCampaignMember.Id);
        }


        Test.startTest();
        
        // Set mock callout class 
        bl_Cvent.t_Test_CampaignMember_Email = 'I_DONT_EXIST@MEDTRONIC.COM';
        Test.setMock(HttpCalloutMock.class, new TEST_bl_Cvent_Mock());  

        System.runAs(oUser){
            // Process Collected CampaignMembers
            bl_Cvent.processCampaignMember(lstCampaignMember, 'CVENT_EMEA');
        }

        Test.stopTest();
    }    

    @isTest static void test_processUpdateCventEvent() {

        // Create Test Data
        createTestData();

        // Load Test Data
        Id id_RecordType_Campaign_Cvent = clsUtil.getRecordTypeByDevName('Campaign', 'CVent_EMEA').Id; 

        List<String> lstCampaignId = new List<String>();
        List<Campaign> lstCampaign =  
            [
                SELECT Id, Cvent_Event_Id__c, Status, HCP_Seats__c, Staff_Seats__c, Total_Delegate__c, Total_MDT_Staff__c, CVent_BusinessUnit__c, CVent_Division__c, CVent_TherapyProductGroup__c
                FROM Campaign
                WHERE RecordtypeId = :id_RecordType_Campaign_Cvent
                AND Status != 'Completed'
            ];
        for (Campaign oCampaign : lstCampaign){
            lstCampaignId.add(oCampaign.Id);
        }

        Test.startTest();

        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new TEST_bl_Cvent_Mock());  

        // Process Collected Campaigns
        System.runAs(oUser){
            bl_Cvent.updateCventEvent(lstCampaign, 'CVENT_EMEA');
        }

        Test.stopTest();
    }


    @isTest static void test_updateCventContact_NotAvailableForRegistration() {

        // Create Test Data
        createTestData();

        Test.startTest();

        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new TEST_bl_Cvent_Mock());  

        List<String> lstCventContactId = new List<String>{'5379193b-3662-4752-8187-451eb080c53c','5379193b-3662-4752-8187-451eb080c53d','5379193b-3662-4752-8187-451eb080c53e'};

        // Process Collected Campaigns
        System.runAs(oUser){
            bl_Cvent.updateCventContact_NotAvailableForRegistration(lstCventContactId, 'CVENT_EMEA');
        }

        Test.stopTest();
    }  


    @isTest static void test_tGetCampaignMemberStatus(){

        // Create Test Data
        createTestData();

        List<bl_Cvent.Cvent_Registration> lstCventRegistration = new List<bl_Cvent.Cvent_Registration>();
        // Results in New CampaignMemberStatus "Attended"
        bl_Cvent.Cvent_Registration oCventRegistration1 = new bl_Cvent.Cvent_Registration();
            oCventRegistration1.Status = 'ACCEPTED';
            oCventRegistration1.Participant = true;
        lstCventRegistration.add(oCventRegistration1);
        // Results in New CampaignMemberStatus "Registered" (if Campaign End Date < today)
        // Results in New CampaignMemberStatus "No Show." (if Campaign End Date > today)
        bl_Cvent.Cvent_Registration oCventRegistration2 = new bl_Cvent.Cvent_Registration();
            oCventRegistration2.Status = 'ACCEPTED';
            oCventRegistration2.Participant = false;
        lstCventRegistration.add(oCventRegistration2);
        // Results in New CampaignMemberStatus "Cancelled."
        bl_Cvent.Cvent_Registration oCventRegistration3 = new bl_Cvent.Cvent_Registration();
            oCventRegistration3.Status = 'CANCELLED';
            oCventRegistration3.Participant = false;
        lstCventRegistration.add(oCventRegistration3);

        List<Id> lstID_Campaign = new List<Id>();
            lstID_Campaign.add(clsTestData.oMain_Campaign.Id);
        bl_Cvent.mapCampaignId_AvailableCampaignMemberStatus = bl_Cvent.getCampaignId_CampaignMemberStatus(lstID_Campaign);

        System.runAs(oUser){

                clsTestData.oMain_Campaign.StartDate = Date.today().addDays(-10);
                clsTestData.oMain_Campaign.EndDate = Date.today().addDays(5);
            update clsTestData.oMain_Campaign;
        }

        List<CampaignMember> lstCampaignMember = 
            [
                SELECT Id, ContactId, Contact.Email, LeadId, Lead.Email, Registration_Type__c, CampaignId, Campaign.Cvent_Event_Id__c, Campaign.Status, Campaign.StartDate, Campaign.EndDate, Status
                FROM CampaignMember
                WHERE CampaignId = :clsTestData.oMain_Campaign.Id
            ];

        Test.startTest();

        String tCampaignMemberStatus_New = '';
        for (CampaignMember oCampaignMember : lstCampaignMember){
            tCampaignMemberStatus_New = bl_Cvent.tGetCampaignMemberStatus(oCventRegistration1, oCampaignMember);
            System.assertEquals(tCampaignMemberStatus_New, 'Attended');
            tCampaignMemberStatus_New = bl_Cvent.tGetCampaignMemberStatus(oCventRegistration2, oCampaignMember);
            System.assertEquals(tCampaignMemberStatus_New, 'Registered');
            tCampaignMemberStatus_New = bl_Cvent.tGetCampaignMemberStatus(oCventRegistration3, oCampaignMember);
            System.assertNotEquals(tCampaignMemberStatus_New, 'Cancelled.');
            System.assertEquals(tCampaignMemberStatus_New, oCampaignMember.Status);
        }


        System.runAs(oUser){

                clsTestData.oMain_Campaign.StartDate = Date.today().addDays(-10);
                clsTestData.oMain_Campaign.EndDate = Date.today().addDays(-5);
            update clsTestData.oMain_Campaign;

        }

        lstCampaignMember = 
            [
                SELECT Id, ContactId, Contact.Email, LeadId, Lead.Email, Registration_Type__c, CampaignId, Campaign.Cvent_Event_Id__c, Campaign.Status, Campaign.StartDate, Campaign.EndDate, Status
                FROM CampaignMember
                WHERE CampaignId = :clsTestData.oMain_Campaign.Id
            ];

        for (CampaignMember oCampaignMember : lstCampaignMember){
            tCampaignMemberStatus_New = bl_Cvent.tGetCampaignMemberStatus(oCventRegistration1, oCampaignMember);
            System.assertEquals(tCampaignMemberStatus_New, 'Attended');
            tCampaignMemberStatus_New = bl_Cvent.tGetCampaignMemberStatus(oCventRegistration2, oCampaignMember);
            System.assertEquals(tCampaignMemberStatus_New, 'No Show.');
            tCampaignMemberStatus_New = bl_Cvent.tGetCampaignMemberStatus(oCventRegistration3, oCampaignMember);
            System.assertNotEquals(tCampaignMemberStatus_New, 'Cancelled.');
            System.assertEquals(tCampaignMemberStatus_New, oCampaignMember.Status);
        }

        Test.stopTest();


    }

}
//--------------------------------------------------------------------------------------------------------------------