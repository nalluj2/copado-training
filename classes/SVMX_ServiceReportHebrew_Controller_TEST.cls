@istest
public class SVMX_ServiceReportHebrew_Controller_TEST {
    @istest
    public static void testPage1(){
        // Create Custom Setting Data
        SVMX_PS_QR_Code_per_Country__c qrCS = new SVMX_PS_QR_Code_per_Country__c ();
        qrCS.Name = 'Israel';
        qrCS.DocumentId__c = '12345';
        qrCS.URL__c = 'https://www.medtronic.com';
        insert qrCS;
        
        // Create Account Data
        clsTestData.iRecord_Account = 1;
        clsTestData.tCountry_Account = 'United Kingdom';
        clsTestData.createAccountData(false);
        clsTestData.oMain_Account.SAP_Id__c = 'SAPA001';
        clsTestData.oMain_Account.Account_Address_Line_1__c = 'Street Line 1';
        clsTestData.oMain_Account.Account_Address_Line_2__c = 'Street Line 2';
        clsTestData.oMain_Account.Account_City__c = 'London';
        clsTestData.oMain_Account.Account_Province__c = 'London';
        insert clsTestData.oMain_Account;
        
        //Create Site
        clsTestData.createSVMXCSite(false);
        clsTestData.oMain_SVMXCSite.Name = 'Test Site';
        clsTestData.oMain_SVMXCSite.SVMXC__Street__c = 'Churchstreet';
        clsTestData.oMain_SVMXCSite.SVMXC__City__c = 'Test City';
        clsTestData.oMain_SVMXCSite.SVMXC__State__c = 'Test State';
        insert clsTestData.oMain_SVMXCSite;

        
        // Create parent SVMXC__Service_Order__c Data
        clsTestData.createSVMXCServiceOrderData(false);
        clsTestData.oMain_SVMXCServiceOrder.SVMX_SAP_Service_Order_No__c = 'T12345T';
        clsTestData.oMain_SVMXCServiceOrder.SVMX_Send_Back_to_Service_Center__c = true;
        clsTestData.oMain_SVMXCServiceOrder.SVMX_PS_Service_Report_To_Share__c = true;
        clsTestData.oMain_SVMXCServiceOrder.SVMX_PS_Is_Site_Visit__c = true;
        clsTestData.oMain_SVMXCServiceOrder.SVMX_Debrief_Complete__c = true;
        clsTestData.oMain_SVMXCServiceOrder.SVMX_PS_Service_Report_To_Share__c = false;
        clsTestData.oMain_SVMXCServiceOrder.SVMXC__Site__c = clsTestData.oMain_SVMXCSite.Id;
        clsTestData.oMain_SVMXCServiceOrder.SVMXC__Company__c = clsTestData.oMain_Account.Id;
        clsTestData.oMain_SVMXCServiceOrder.SVMXC__Completed_Date_Time__c = Date.today().addDays(10);
        clsTestData.oMain_SVMXCServiceOrder.SVMXC__Country__c = 'Israel';
        insert clsTestData.oMain_SVMXCServiceOrder;
        
        SVMXC__Service_Order__c wo = [SELECT id, SVMXC__Country__c, SVMXC__Site__r.Name, SVMXC__Site__r.SVMXC__Street__c,SVMXC__Site__r.SVMXC__City__c,SVMXC__Site__r.SVMXC__State__c, SVMX_PS_Language__c,
           SVMXC__Company__r.Name,SVMXC__Company__r.Account_Address_Line_1__c,SVMXC__Company__r.Account_Address_Line_2__c,SVMXC__Company__r.Account_City__c,SVMXC__Company__r.Account_Province__c  FROM SVMXC__Service_Order__c];
        
        Test.StartTest();
            PageReference testPage = Page.SVMX_PS_Service_Report_Standalone_Hebrew;
            testPage.getParameters().put('id', String.valueOf(clsTestData.oMain_SVMXCServiceOrder.Id));
            Test.setCurrentPage(testPage);
            ApexPages.StandardController sc = new  ApexPages.StandardController(wo); 
            SVMX_PS_Service_Report_Hebrew_Controller  testPageController = new SVMX_PS_Service_Report_Hebrew_Controller(sc);
            system.assertNotEquals(null,testPageController.getDocID());
            system.assertNotEquals(null,testPageController.getDocURL());
            
        Test.StopTest();
    }
}