global class ba_Opportunity_BUG_BU_SBU implements Database.Batchable<sObject>, Database.Stateful {
	
	global String tSOQL = 'SELECT Id, Name, Business_Unit_Group__c, Business_Unit_msp__c, Sub_Business_Unit__c, Therapies__c FROM Opportunity WHERE RecordType.DeveloperName in (\'Capital_Opportunity\', \'Non_Capital_Opportunity\', \'Service_Contract_Opportunity\', \'Strategic_Account_Management\') AND IsClosed = true';
	global List<String> lstOpportunityID_Error;
	global List<String> lstOpportunityID_OK;
	
	global Database.QueryLocator start(Database.BatchableContext context) {


		lstOpportunityID_OK = new List<String>();
		lstOpportunityID_Error = new List<String>();

		return Database.getQueryLocator(tSOQL);

	}

   	global void execute(Database.BatchableContext context, List<Opportunity> lstOpportunity) {

        // Get the OpportunityLineItes that are related the collect Opportunities that need processing
        List<OpportunityLineItem> lstOLI = 
            [
                SELECT 
                    Id, OpportunityId
                    , PriceBookEntry.Product2.Business_Unit_Group__c, PriceBookEntry.Product2.Business_Unit_ID__c, PriceBookEntry.Product2.Sub_Business_Unit__c
                    , PricebookEntry.Product2.Product_Group__r.Therapy_ID__r.Name
                    , Products_Therapies__c
                FROM 
                    OpportunityLineItem
                WHERE 
                    OpportunityId = :lstOpportunity
            ];
        System.debug('** setOpportunity_BUG_BU_SBU - lstOLI (' + lstOLI.size() + ') : ' + lstOLI); 


        // Get a Map of Opportunity ID and the related OpportunityLineItems
        // Collect also the Business Units ID and Business Unit Group ID of the Products on the OpportunityLineItems
        Set<Id> setID_BUG = new Set<Id>();
        Set<Id> setID_BU = new Set<Id>();
        Map<Id, List<OpportunityLineItem>> mapOpportunityId_OLI = new Map<Id, List<OpportunityLineItem>>();
        for (OpportunityLineItem oOLI : lstOLI){

            List<OpportunityLineItem> lstOLI_Tmp = new List<OpportunityLineItem>();
            if (mapOpportunityId_OLI.containsKey(oOLI.OpportunityId)){
                lstOLI_Tmp = mapOpportunityId_OLI.get(oOLI.OpportunityId);
            }
            lstOLI_Tmp.add(oOLI);
            mapOpportunityId_OLI.put(oOLI.OpportunityId, lstOLI_Tmp);

            if (oOLI.PriceBookEntry.Product2.Business_Unit_ID__c != null){
                setID_BU.add(oOLI.PriceBookEntry.Product2.Business_Unit_ID__c);
            }

			if (oOLI.PricebookEntry.Product2.Business_Unit_Group__c != null){
                setID_BUG.add(oOLI.PriceBookEntry.Product2.Business_Unit_Group__c);
			}


        }
        System.debug('** ba_Opportunity_BUG_BU_SBU - mapOpportunityId_OLI (' + mapOpportunityId_OLI.size() + ') : ' + mapOpportunityId_OLI); 
        System.debug('** ba_Opportunity_BUG_BU_SBU - setID_BU (' + setID_BU.size() + ') : ' + setID_BU); 
        System.debug('** ba_Opportunity_BUG_BU_SBU - setID_BUG (' + setID_BUG.size() + ') : ' + setID_BUG); 


        // Get the Business Unit Group, Business Unit and Sub Business Unit based on the collected Sub Business Unit ID's
        Map<Id, Business_Unit__c> mapBU = new Map<Id, Business_Unit__c>(
            [
                SELECT 
                    Id, Name
                    , Business_Unit_Group__c, Business_Unit_Group__r.Name
                FROM 
                    Business_Unit__c
                WHERE 
                    Id = :setID_BU
            ]
        ); 
        System.debug('** ba_Opportunity_BUG_BU_SBU - mapBU (' + mapBU.size() + ') : ' + mapBU); 


        // Get the Business Unit Group, Business Unit and Sub Business Unit based on the collected Sub Business Unit ID's
        Map<Id, Business_Unit_Group__c> mapBUG = new Map<Id, Business_Unit_Group__c>(
            [
                SELECT 
                    Id, Name
                FROM 
                    Business_Unit_Group__c
                WHERE 
                    Id = :setID_BUG
            ]
        ); 
        System.debug('** ba_Opportunity_BUG_BU_SBU - mapBUG (' + mapBUG.size() + ') : ' + mapBUG); 


        // Loop through the processing Opportunities and for each related OpportunityLineItem get the name of the Business Unit Group, Business Unit and Sub Business Unit
        //  and concatenate each one in a field on Opportunity
        List<Opportunity> lstOpportunity_Update = new List<Opportunity>();
        for (Opportunity oOpportunity : lstOpportunity){

            if (mapOpportunityId_OLI.containsKey(oOpportunity.Id)){

                // Opportunity has OpportunityLineItems - populate fields on Opportunity

                Set<String> setBUG = new Set<String>();
                Set<String> setBU = new Set<String>();
                Set<String> setSBU = new Set<String>();
                Set<String> setTherapy = new Set<String>();

                for (OpportunityLineItem oOLI : mapOpportunityId_OLI.get(oOpportunity.Id)){

					if (oOLI.PriceBookEntry.Product2.Business_Unit_ID__c != null){

						Business_Unit__c oBU = mapBU.get(oOLI.PriceBookEntry.Product2.Business_Unit_ID__c);
						System.debug('** ba_Opportunity_BUG_BU_SBU - oBU : ' + oBU); 

						String tBUG = '';
						String tBU = '';
						String tSBU = '';
						String tTherapy = '';
						try{ tBUG = oBU.Business_Unit_Group__r.Name; }catch(Exception oEX){}
						try{ tBU = oBU.Name; }catch(Exception oEX){}
						try{ tSBU = oOLI.PriceBookEntry.Product2.Sub_Business_Unit__c; }catch(Exception oEX){}
						try{ tTherapy = oOLI.PricebookEntry.Product2.Product_Group__r.Therapy_ID__r.Name; }catch(Exception oEX){}

						System.debug('** ba_Opportunity_BUG_BU_SBU - tBUG : ' + tBUG); 
						System.debug('** ba_Opportunity_BUG_BU_SBU - tBU : ' + tBU); 
						System.debug('** ba_Opportunity_BUG_BU_SBU - tSBU : ' + tSBU); 
						System.debug('** ba_Opportunity_BUG_BU_SBU - tTherapy : ' + tTherapy); 

						if (!String.isBlank(tBUG)) setBUG.add(tBUG);
						if (!String.isBlank(tBU)) setBU.add(tBU);
						if (!String.isBlank(tSBU)) setSBU.add(tSBU);
						if (!String.isBlank(tTherapy)) setTherapy.add(tTherapy);
					
					}else if (oOLI.PriceBookEntry.Product2.Business_Unit_Group__c != null){

						Business_Unit_Group__c oBUG = mapBUG.get(oOLI.PriceBookEntry.Product2.Business_Unit_Group__c);
						System.debug('** ba_Opportunity_BUG_BU_SBU - oBUG : ' + oBUG); 

						String tBUG = '';
						try{ tBUG = oBUG.Name; }catch(Exception oEX){}

						System.debug('** ba_Opportunity_BUG_BU_SBU - tBUG : ' + tBUG); 

						if (!String.isBlank(tBUG)) setBUG.add(tBUG);

					}

                }
                System.debug('** ba_Opportunity_BUG_BU_SBU - setBUG : ' + setBUG); 
                System.debug('** ba_Opportunity_BUG_BU_SBU - setBU : ' + setBU); 
                System.debug('** ba_Opportunity_BUG_BU_SBU - setSBU : ' + setSBU); 
                System.debug('** ba_Opportunity_BUG_BU_SBU - setTherapy : ' + setTherapy); 

                List<String> lstBUG = new List<String>();
                lstBUG.addAll(setBUG);
                String tBUG = String.join(lstBUG, ';');

                List<String> lstBU = new List<String>();
                lstBU.addAll(setBU);
                String tBU = String.join(lstBU, ';');

                List<String> lstSBU = new List<String>();
                lstSBU.addAll(setSBU);
                String tSBU = String.join(lstSBU, ';');

                List<String> lstTherapy = new List<String>();
                lstTherapy.addAll(setTherapy);
                String tTherapy = String.join(lstTherapy, ';');

                System.debug('** ba_Opportunity_BUG_BU_SBU - tBUG 2 : ' + tBUG); 
                System.debug('** ba_Opportunity_BUG_BU_SBU - tBU 2 : ' + tBU); 
                System.debug('** ba_Opportunity_BUG_BU_SBU - tSBU 2 : ' + tSBU); 
                System.debug('** ba_Opportunity_BUG_BU_SBU - tTherapy 2 : ' + tTherapy); 

                if ( (oOpportunity.Business_Unit_Group__c != tBUG) || (oOpportunity.Business_Unit_msp__c != tBU) || (oOpportunity.Sub_Business_Unit__c != tSBU) || (oOpportunity.Therapies__c != tTherapy) ){
					
						oOpportunity.Business_Unit_Group__c = tBUG;
						oOpportunity.Business_Unit_msp__c = tBU;
						oOpportunity.Sub_Business_Unit__c = tSBU;
						oOpportunity.Therapies__c = tTherapy;
					lstOpportunity_Update.add(oOpportunity);

				}

            }else{
/*
                // Opportunity has NO OpportunityLineItems - clear fields on Opportunity
                if (
					(oOpportunity.Business_Unit_Group__c != null)
					|| (oOpportunity.Business_Unit_msp__c != null)
					|| (oOpportunity.Sub_Business_Unit__c != null)
					|| (oOpportunity.Therapies__c != null)
				){
					
						oOpportunity.Business_Unit_Group__c = null;
						oOpportunity.Business_Unit_msp__c = null;
						oOpportunity.Sub_Business_Unit__c = null;
						oOpportunity.Therapies__c = null;
					lstOpportunity_Update.add(oOpportunity);

				}
*/
            }

        }

        System.debug('** ba_Opportunity_BUG_BU_SBU - lstOpportunity_Update (' + lstOpportunity_Update.size() + ') : ' + lstOpportunity_Update); 
        if (lstOpportunity_Update.size() > 0){

            List<Database.SaveResult> lstSaveResult_Update = Database.update(lstOpportunity_Update, false);
            System.debug('** ba_Opportunity_BUG_BU_SBU - lstSaveResult_Update (' + lstSaveResult_Update.size() + ') : ' + lstSaveResult_Update); 

            Integer iCounter = 0;
            for (Database.SaveResult oSaveResult : lstSaveResult_Update){
                
                if (!oSaveResult.isSuccess()){                                    
                    String tError = '';
                    for (Database.Error oError : oSaveResult.getErrors()){
                        tError += 'Field : ' + oError.getFields() + ' - ' + oError.getMessage() + '; ';
                    }
					lstOpportunityID_Error.add('Error while saving Opportunity "' + lstOpportunity_Update[iCounter].Name + '" (' + lstOpportunity_Update[iCounter].Id + ') : ' + tError);
                }else{

					lstOpportunityID_OK.add('Update OK for "' + lstOpportunity_Update[iCounter].Name + '" (' + lstOpportunity_Update[iCounter].Id + ')');

				}
                iCounter++;
            }

        }

	
	}
	
	global void finish(Database.BatchableContext BC) {

		String tResult = '<BR />';
		tResult += '<HR />';
		tResult += ' ERRORS ';
		tResult += '<HR />';
		tResult += '<BR />';
		tResult += '<BR />';
		tResult += String.join(lstOpportunityID_Error, '<BR />');
		tResult += '<BR />';
		tResult += '<HR />';
		tResult += ' OK ';
		tResult += '<HR />';
		tResult += '<BR />';
		tResult += '<BR />';
		tResult += String.join(lstOpportunityID_OK, '<BR />');

		String tEmailBody = tResult;

		Messaging.Singleemailmessage oEmail = new Messaging.Singleemailmessage();
			oEmail.setHTMLBody(tEmailBody);
			oEmail.setTargetObjectId(UserInfo.getUserId());
			oEmail.setSubject('ba_Opportunity_BUG_BU_SBU');
			oEmail.setSaveAsActivity(false);
		Messaging.sendEmail(new Messaging.Singleemailmessage[] { oEmail});

	}
	
}