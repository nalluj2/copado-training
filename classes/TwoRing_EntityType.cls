global class TwoRing_EntityType {
	public static String ACCOUNT = 'account';
	public static String PERSONACCOUNT = 'personAccount';
	public static String CONTACT = 'contact';
	public static String LEAD = 'lead';
}