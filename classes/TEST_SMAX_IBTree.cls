/**
 * @description       : Test Class for IBTree related Controllers (ctrl_SVMXC_IBTree_WO + ctrl_SVMXC_IBTree_IB + ctrl_SVMXC_IBTree)
 * @author            : bart.caelen@medtronic.com
 * @last modified on  : 15/10/2020
 * @last modified by  : bart.caelen@medtronic.com
 * Modifications Log 
 * Ver		Date			Author							Modification
 * 1.0		15/10/2020		bart.caelen@medtronic.com		Initial Version
**/
@isTest private class TEST_SMAX_IBTree{

    //----------------------------------------------------------------------------------------
    // CREATE TEST DATA
    //----------------------------------------------------------------------------------------
    @testSetup static void createTestData(){

        Account oAccount_SAP = new Account();
			oAccount_SAP.Name = 'Test Account';
			oAccount_SAP.SAP_Id__c = '11111'; // This will set the record type to SAP Account
			oAccount_SAP.Account_Country__c = 'CHINA';
			oAccount_SAP.RecordTypeId = clsUtil.getRecordTypeByDevName('Account', 'SAP_Account').Id;
        insert oAccount_SAP;
        

        SVMXC__Site__c oLocation = new SVMXC__Site__c();
			oLocation.SVMX_SAP_Location_ID__c = oAccount_SAP.SAP_Id__c;
			oLocation.SVMXC__Account__c = oAccount_SAP.Id;
			oLocation.Name = 'Test Account oLocation';
			oLocation.SVMXC__Location_Type__c = 'Customer';  
			oLocation.SVMXC__Zip__c = oAccount_SAP.Account_Postal_Code__c;                 
			oLocation.SVMXC__State__c = oAccount_SAP.Account_Province__c;
			oLocation.SVMXC__Country__c = oAccount_SAP.Account_Country__c;
			oLocation.SVMXC__City__c = oAccount_SAP.Account_City__c;               
			oLocation.SVMXC__Street__c = oAccount_SAP.ShippingStreet;
        insert oLocation;

        List<SVMXC__Site__c> lstLocation = [SELECT Id FROM SVMXC__Site__c WHERE SVMXC__Account__c = :oAccount_SAP.Id];
        System.assert(lstLocation.size() == 1);
        

        Product2 oProduct_Installed = new Product2();
			oProduct_Installed.IsActive = true; 
			oProduct_Installed.Name = 'Test Installed Product';
			oProduct_Installed.RecordTypeId = clsUtil.getRecordTypeByDevName('Product2', 'SAP_Product').Id;
			oProduct_Installed.ProductCode = '12345';
			oProduct_Installed.CFN_Code_Text__c = '12345';
        
        Product2 oProduct_Sparepart = new Product2();
			oProduct_Sparepart.IsActive = true; 
			oProduct_Sparepart.Name = 'Test Sparepart';
			oProduct_Sparepart.RecordTypeId = clsUtil.getRecordTypeByDevName('Product2', 'SAP_Product').Id;
			oProduct_Sparepart.ProductCode = '54321';
			oProduct_Sparepart.CFN_Code_Text__c = '54321';

        Product2 oProduct_Software = new Product2();
			oProduct_Software.IsActive = true; 
			oProduct_Software.Name = 'Test Software';
			oProduct_Software.RecordTypeId = clsUtil.getRecordTypeByDevName('Product2', 'SAP_Product').Id;
			oProduct_Software.ProductCode = '10884521722569';
			oProduct_Software.CFN_Code_Text__c = '10884521722569';
        insert new List<Product2>{oProduct_Installed, oProduct_Sparepart, oProduct_Software};   
		
		List<Product2> lstProduct = [SELECT Id, ProductCode FROM Product2 WHERE RecordTypeId = :clsUtil.getRecordTypeByDevName('Product2', 'SAP_Product').Id AND ProductCode != null];
		System.assertEquals(lstProduct.size(), 3);


        Product_Software__c oProductSoftware = new Product_Software__c();
			oProductSoftware.Product__c = oProduct_Software.Id;
			oProductSoftware.Software_Version__c = 'V.1.0.1';
			oProductSoftware.Status__c = 'AL Created';
			oProductSoftware.SAP_Version__c = 'AD';
        insert oProductSoftware;


        SVMXC__Service_Group__c oServiceGroup = new SVMXC__Service_Group__c();
			oServiceGroup.SVMXC__Active__c = true; 
			oServiceGroup.SVMXC__Country__c = 'Netherlands, The';
			oServiceGroup.SVMXC__Description__c = 'Test Technician Group';      
        insert oServiceGroup;

        
        SVMXC__Service_Group_Members__c oServiceGroupMember = new SVMXC__Service_Group_Members__c();
			oServiceGroupMember.SVMXC__Active__c = true;
			oServiceGroupMember.SVMXC__Country__c = 'Netherlands, The';
			oServiceGroupMember.SVMXC__Salesforce_User__c = UserInfo.getUserId();
			oServiceGroupMember.SVMX_SAP_Employee_Id__c = '55555';
			oServiceGroupMember.SVMXC__Service_Group__c = oServiceGroup.Id;      
        insert oServiceGroupMember;
        

        SVMXC__Site__c oSite_Group = new SVMXC__Site__c();
			oSite_Group.SVMXC__Country__c = 'Netherlands, The';
			oSite_Group.SVMX_SAP_Location_ID__c = 'XXXXX';
			oSite_Group.SVMX_Service_Team__c = oServiceGroup.Id;
			oSite_Group.SVMXC__Stocking_Location__c = true;
			oSite_Group.SVMXC__IsStaging_Location__c = true;
			oSite_Group.SVMXC__IsRepair_Location__c = true;
			oSite_Group.SVMXC__IsReceiving_Location__c = true;
			oSite_Group.SVMXC__IsGood_Stock__c = true;
			oSite_Group.SAP_Plant__c = '1235';
                
        SVMXC__Site__c oSite_Tech = new SVMXC__Site__c();
			oSite_Tech.SVMXC__Country__c = 'Netherlands, The';
			oSite_Tech.SVMX_SAP_Location_ID__c = 'YYYYY';
			oSite_Tech.SVMXC__Service_Engineer__c = UserInfo.getUserId();
			oSite_Tech.SVMX_Technician__c = oServiceGroupMember.Id;
			oSite_Tech.SVMXC__Stocking_Location__c = true;
			oSite_Tech.SVMXC__IsStaging_Location__c = true;
			oSite_Tech.SVMXC__IsRepair_Location__c = true;
			oSite_Tech.SVMXC__IsReceiving_Location__c = true;
			oSite_Tech.SVMXC__IsGood_Stock__c = true;
			oSite_Tech.SAP_Plant__c = '1234';
        insert new List<SVMXC__Site__c>{oSite_Group, oSite_Tech};
        

	        oServiceGroup.SVMX_Service_Team_Location__c = oSite_Group.Id;
        update oServiceGroup;
        
			oServiceGroupMember.SVMXC__Inventory_Location__c = oSite_Tech.Id;
        update oServiceGroupMember;
        

        SVMXC__Installed_Product__c oInstalledProduct1 = new SVMXC__Installed_Product__c();
			oInstalledProduct1.SVMXC__Company__c = oAccount_SAP.Id;
			oInstalledProduct1.SVMXC__Country__c = 'Japan';
			oInstalledProduct1.SVMXC__Product__c = oProduct_Installed.Id;
			oInstalledProduct1.SVMX_SAP_Equipment_ID__c = '99999';
			oInstalledProduct1.SVMXC__Serial_Lot_Number__c = '99999';
			oInstalledProduct1.IB_Component_Type__c = 'Repairable Equipment';
		insert oInstalledProduct1;

        SVMXC__Installed_Product__c oInstalledProduct2 = new SVMXC__Installed_Product__c();
			oInstalledProduct2.SVMXC__Company__c = oAccount_SAP.Id;
			oInstalledProduct2.SVMXC__Country__c = 'Japan';
			oInstalledProduct2.SVMXC__Product__c = oProduct_Installed.Id;
			oInstalledProduct2.SVMX_SAP_Equipment_ID__c = '88888';
			oInstalledProduct2.SVMXC__Serial_Lot_Number__c = '88888';
			oInstalledProduct2.IB_Component_Type__c = 'Repairable Equipment';
			oInstalledProduct2.SVMXC__Parent__c = oInstalledProduct1.Id;
		insert oInstalledProduct2;

        SVMXC__Installed_Product__c oInstalledProduct3 = new SVMXC__Installed_Product__c();
			oInstalledProduct3.SVMXC__Company__c = oAccount_SAP.Id;
			oInstalledProduct3.SVMXC__Country__c = 'Japan';
			oInstalledProduct3.SVMXC__Product__c = oProduct_Installed.Id;
			oInstalledProduct3.SVMX_SAP_Equipment_ID__c = '77777';
			oInstalledProduct3.SVMXC__Serial_Lot_Number__c = '77777';
			oInstalledProduct3.IB_Component_Type__c = 'Repairable Equipment';
			oInstalledProduct3.SVMXC__Parent__c = oInstalledProduct2.Id;
		insert oInstalledProduct3;
        

        SVMXC__Product_Stock__c oProductStock = new SVMXC__Product_Stock__c();
			oProductStock.SVMX_Batch_Number__c = '';
			oProductStock.SVMXC__Location__c = oSite_Tech.Id;
			oProductStock.SVMXC__Product__c = oProduct_Sparepart.Id;
			oProductStock.SVMXC__Quantity2__c = 4;
	        oProductStock.SVMX_SAP_Plant__c = '22222';
        insert oProductStock;
        

        SVMX_Damage_Cause_Code__c oDamageCode1 = new SVMX_Damage_Cause_Code__c();
			oDamageCode1.SVMX_Code_Type__c = 'Damage';
			oDamageCode1.SVMX_Damage_Code__c = 'DMG1';
			oDamageCode1.SVMX_Damage_Code_Text__c = 'Damage Code 1';     
			oDamageCode1.SVMX_Cause_Code_Group__c = 'DMG GRP 1';
        
        SVMX_Damage_Cause_Code__c oDamageCode2 = new SVMX_Damage_Cause_Code__c();
			oDamageCode2.SVMX_Code_Type__c = 'Damage';
			oDamageCode2.SVMX_Damage_Code__c = 'DMG2';
			oDamageCode2.SVMX_Damage_Code_Text__c = 'Damage Code 2';     
			oDamageCode2.SVMX_Cause_Code_Group__c = 'DMG GRP 2';
        
        SVMX_Damage_Cause_Code__c oCauseCode1 = new SVMX_Damage_Cause_Code__c();
			oCauseCode1.SVMX_Code_Type__c = 'Cause';
			oCauseCode1.SVMX_Code__c = 'CAS1';
			oCauseCode1.SVMX_Cause_Code_Text__c = 'Cause Code 1';        
			oCauseCode1.SVMX_Cause_Code_Group__c = 'CAS GRP 1';
        
        SVMX_Damage_Cause_Code__c oCauseCode2 = new SVMX_Damage_Cause_Code__c();
			oCauseCode2.SVMX_Code_Type__c = 'Cause';
			oCauseCode2.SVMX_Code__c = 'CAS2';
			oCauseCode2.SVMX_Cause_Code_Text__c = 'Cause Code 2';        
			oCauseCode2.SVMX_Cause_Code_Group__c = 'CAS GRP 2';
        insert new List<SVMX_Damage_Cause_Code__c>{oDamageCode1, oDamageCode2, oCauseCode1, oCauseCode2};

        
    }
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
    // TEST DATA APEX Controllers
	//	- ctrl_SVMXC_IBTree_WO
	//	- ctrl_SVMXC_IBTree_IB
	//	- ctrl_SVMXC_IBTree
    //----------------------------------------------------------------------------------------
    private static testmethod void testRunInstallBaseController(){
        

		List<SVMXC__Installed_Product__c> lstInstalledProduct = [SELECT Id FROM SVMXC__Installed_Product__c ORDER BY SVMX_SAP_Equipment_ID__c DESC];


        Case oCase = new Case();
			oCase.AccountId = [Select Id from Account].Id;
			oCase.RecordTypeId = clsUtil.getRecordTypeByDevName('Case', 'Field_Service').Id;
			oCase.Description = 'Test SAP Case';
			oCase.Subject = 'Test SAP Case';
			oCase.Status = 'New';
			oCase.Actual_Breakdown_Date_Time__c = Datetime.now().addDays(-2);
			oCase.Customer_Down__c = true;
			oCase.SVMX_SAP_Notification_Type__c = '  ZC';
			oCase.SVMX_SAP_Notification_Number__c = '66666';
			oCase.Serial_Number__c = '99999';
        insert oCase;
        

        SVMXC__Service_Order__c oServiceOrder = new SVMXC__Service_Order__c();
			oServiceOrder.SVMXC__Company__c = oCase.AccountId;
			oServiceOrder.SVMXC__Case__c = oCase.Id;
			oServiceOrder.SVMXC__Component__c = lstInstalledProduct[0].Id;
			oServiceOrder.SVMXC__Country__c = 'Netherlands, The';
			oServiceOrder.SVMX_Order_Category__c = 'ZRS2';
			oServiceOrder.SVMXC__Order_Status__c = 'Scheduled';
			oServiceOrder.SVMX_Planned_Job_Duration__c = 6;
			oServiceOrder.SVMXC_Preferred_Start_Date__c = Date.today().addDays(2);
			oServiceOrder.SVMXC__Priority__c = 'High';
			oServiceOrder.SVMXC__Product__c = [SELECT Id FROM Product2 WHERE CFN_Code_Text__c = '12345'].Id;
			oServiceOrder.SVMX_SAP_Customer__c = '11111';
			oServiceOrder.SVMX_SAP_Employee_Id__c = '55555';
			oServiceOrder.SVMX_SAP_Notification_Number__c = '66666';
			oServiceOrder.SVMX_SAP_Equipment_Number__c = '99999';
			oServiceOrder.SVMX_SAP_Service_Order_No__c = '88888';
			oServiceOrder.SVMXC__Scheduled_Date_Time__c = Datetime.now().addDays(3);
			oServiceOrder.SVMX_Serial_Number__c = '99999';
			oServiceOrder.SVMXC__Service_Group__c = [SELECT Id FROM SVMXC__Service_Group__c].Id;
			oServiceOrder.SVMX_Service_Team_Location__c = [SELECT Id FROM SVMXC__Site__c WHERE SVMX_SAP_Location_ID__c = 'XXXXX'].Id;
			oServiceOrder.SVMXC__Group_Member__c = [SELECT Id FROM SVMXC__Service_Group_Members__c].Id;
			oServiceOrder.SVMX_FSE_Stock_Location__c = [SELECT Id FROM SVMXC__Site__c WHERE SVMX_SAP_Location_ID__c = 'YYYYY'].Id;
		insert oServiceOrder;
        

        List<SVMXC_Work_Order_Operation__c> lstWorkOrderOperation = [Select Id from SVMXC_Work_Order_Operation__c where SVMXC_Work_Order__c = :oServiceOrder.Id];
        System.assert(lstWorkOrderOperation.size () == 1);

        
        SVMXC__Service_Order_Line__c oServiceOrderLine = new SVMXC__Service_Order_Line__c();
			oServiceOrderLine.SVMXC__Product__c = [SELECT Id FROM Product2 WHERE CFN_Code_Text__c = '54321'].Id;
			oServiceOrderLine.SVMXC__Actual_Quantity2__c = 1;
			oServiceOrderLine.SVMXC__Service_Order__c = oServiceOrder.Id;
			oServiceOrderLine.SVMXC__Line_Type__c = 'Parts';
			oServiceOrderLine.SVMX_Product_Stock__c = [SELECT Id FROM SVMXC__Product_Stock__c].Id;
			oServiceOrderLine.RecordTypeId = clsUtil.getRecordTypeByDevName('SVMXC__Service_Order_Line__c', 'UsageConsumption').Id;
        insert oServiceOrderLine; 
        
        oServiceOrderLine = [SELECT Id, SVMX_Work_Order_Operation__c FROM SVMXC__Service_Order_Line__c WHERE Id = :oServiceOrderLine.Id];
        System.assert(oServiceOrderLine.SVMX_Work_Order_Operation__c == lstWorkOrderOperation[0].Id);


        Test.startTest();
        
            ApexPages.StandardController oStandardController_ServiceOrder = new ApexPages.StandardController(oServiceOrder);
            ctrl_SVMXC_IBTree_WO oController_ServiceOrder = new ctrl_SVMXC_IBTree_WO(oStandardController_ServiceOrder);

            ApexPages.StandardController oStandardController_InstalledProduct = new ApexPages.StandardController(lstInstalledProduct[0]);
            ctrl_SVMXC_IBTree_IB oController_InstalledProduct = new ctrl_SVMXC_IBTree_IB(oStandardController_InstalledProduct);

            ctrl_SVMXC_IBTree oController_IBTree1 = new ctrl_SVMXC_IBTree();
				oController_IBTree1.thisIBId = lstInstalledProduct[2].Id;
			
            ctrl_SVMXC_IBTree oController_IBTree2 = new ctrl_SVMXC_IBTree();
				oController_IBTree2.topIBId = lstInstalledProduct[0].Id;
				oController_IBTree2.hierarchyTree.getFlattenedTree();
				oController_IBTree2.hierarchyTree.topInstalledProduct.getSpacerStr();
					
        Test.stopTest();
    }
    //----------------------------------------------------------------------------------------

}