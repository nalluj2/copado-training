//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 20-01-2016
//  Description      : APEX Test Class to test the logic in tr_Attachment
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_tr_Attachment {

    //----------------------------------------------------------------------------------------
    // CREATE TEST DATA
    //----------------------------------------------------------------------------------------
    @testSetup static void createTestData() {

        // Create Custom Setting Data
        clsTestData.createCustomSettingData(true);

        // Create Attachment Data
        clsTestData.createSVMXCServiceOrderData(true);

        // Create Attachment Data
        clsTestData.idAttachment_Parent = clsTestData.oMain_SVMXCServiceOrder.Id;
        clsTestData.createAttachmentData();

    }
    //----------------------------------------------------------------------------------------

	
    //----------------------------------------------------------------------------------------
    // Basic Testing
    //----------------------------------------------------------------------------------------
	@isTest static void test_tr_Attachment() {
	
		// Select Data
		List<Attachment> lstData = [SELECT Id, Name FROM Attachment];
		Attachment oData = lstData[0];
		
		// Update Data
		update oData;

		// Delete Data
		delete oData;

		// Undelete Data
		Undelete oData;

	}
    //----------------------------------------------------------------------------------------
	
}
//--------------------------------------------------------------------------------------------------------------------