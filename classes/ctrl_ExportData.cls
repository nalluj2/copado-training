//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 07-11-2017
//  Description      : APEX Controller Class for the VisualForce Page exportData
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public class ctrl_ExportData {

	//------------------------------------------------------------------------------
	// PRIVATE VARIABLES
	//------------------------------------------------------------------------------
	private String tExportSep = '\t';	// Tab Delimted
	private Id idData;
	private String tExportType;
	//------------------------------------------------------------------------------


	//------------------------------------------------------------------------------
	// CONSTRUCTOR
	//------------------------------------------------------------------------------
	public ctrl_ExportData(){
		
		idData = ApexPages.currentPage().getParameters().get('id');
		tExportType = ApexPages.currentPage().getParameters().get('exportType');

		tDataExport = '';

		if (String.isBlank(idData) || String.isBlank(tExportType)) return;

		if (tExportType == clsUtil.getRecordTypeByDevName('Opportunity', 'Business_Critical_Tender').Id){
			createDataExport_OpportunityBusinessCriticalTender();
		}else if (tExportType == clsUtil.getRecordTypeByDevName('Opportunity', 'Deal_VID_Non_tender').Id){
			createDataExport_OpportunityDealVIDNonTender();
		}


	}
	//------------------------------------------------------------------------------


	//------------------------------------------------------------------------------
	// GETTERS & SETTERS
	//------------------------------------------------------------------------------
	public String tDataExport { get; private set; }
	public String tFileName { get; private set; }
	public String tContentType { get; private set; }
	//------------------------------------------------------------------------------


	//------------------------------------------------------------------------------
	// PRIVATE METHODS
	//------------------------------------------------------------------------------
	private string createDataExport_OpportunityDealVIDNonTender(){

		tDataExport = '';

		try{

			if (Test.isRunningTest()) clsUtil.bubbleException('ctrl_ExportData.createDataExport_OpportunityDealVIDNonTender');

			// Set settings for the Data Export File
			tExportSep = '\t';
			tContentType = 'text/tab-separated-values';

			// Set the name of the file
			String tFileName_Field = 'Name';
			tFileName = '';

	        // Setup the fieldmapping - these field will be extracted in this sequence
	        List<String> fieldNames = new List<String>();
			fieldNames.add('Opportunity_Number__c');
			fieldNames.add('AccountGroup');
			fieldNames.add('Owner.Alias_unique__c');
			fieldNames.add('CustomerId');
			fieldNames.add('Expected_Contract_Start_Date__c');
			fieldNames.add('Expected_Contract_End_Date__c');
			fieldNames.add('OrderId');
			fieldNames.add('FirstOfLoad');
			fieldNames.add('LastOfLoad');

			
			List<String> fieldLabels = new List<String>();			
			fieldLabels.add('#SFDCOpportunityNumber');
			fieldLabels.add('AccountGroup');
			fieldLabels.add('OnBehalfOfId');
			fieldLabels.add('CustomerId');
			fieldLabels.add('ValidFrom');
			fieldLabels.add('ValidTo');
			fieldLabels.add('OrderId');
			fieldLabels.add('FirstOfLoad');
			fieldLabels.add('LastOfLoad');			

						
	        // Put all fields in a set to prevent duplicates in the SOQL
			Set<String> setFieldName = new Set<String>();
			setFieldName.add('Name');
			setFieldName.add('Opportunity_Number__c');
			setFieldName.add('Owner.Alias_unique__c');
			setFieldName.add('Account.SAP_ID__c');
			setFieldName.add('Account.Buying_Group_Number__c');
			setFieldName.add('Expected_Contract_Start_Date__c');
			setFieldName.add('Expected_Contract_End_Date__c');
			setFieldName.add(tFileName_Field);

	        // Select the data which will be used to create the Export File
	        String tSOQL = 'SELECT Id';
	        for (String tFieldName : setFieldName){
	        	tSOQL += ',' + tFieldName;
	        }
	        tSOQL += ' FROM Opportunity';
	        tSOQL += ' WHERE Id = :idData';
	        tSOQL += ' ORDER BY Id';
	        List<Opportunity> lstData = Database.query(tSOQL);


			// Create Header Row
			List<String> lstDataExport = new List<String>();
			lstDataExport.add(String.join(fieldLabels, tExportSep));

			// Creata Data Row(s)
			Integer iCounter = 0;
			for (Opportunity oOpportunity: lstData){

				List<String> lstData_Row = new List<String>();

				for (String tFieldName : fieldNames){

					String tData = '';

					if (tFieldName == 'FirstOfLoad'){

						tData = 'false';
						if (iCounter == 0){
							tData = 'true';
						}

					}else if (tFieldName == 'LastOfLoad'){

						tData = 'false';
						if (iCounter == lstData.size() - 1){
							tData = 'true';
						}

					}else if ( (tFieldName == 'AccountGroup') || (tFieldName == 'OrderId') ){

						tData = '';

						if (tFieldName == 'OrderId'){
							tData = String.valueOf(iCounter + 1);
						}

					}else if (tFieldName == 'CustomerId'){

						tData = oOpportunity.Account.SAP_ID__c;
						if (!String.isBlank(oOpportunity.Account.Buying_Group_Number__c)){
							tData = oOpportunity.Account.Buying_Group_Number__c;
						}

					}else{

						try{ 

							List<String> lstFieldPart = tFieldName.split('\\.');
							sObject oSObject = oOpportunity;
							for (Integer i = 0; i < lstFieldPart.size() - 1; i++){
								if(oSObject != null) oSObject = oSObject.getSObject(lstFieldPart[i]);
							}
							if(oSObject != null) tData = String.valueOf(oSObject.get(lstFieldPart[lstFieldPart.size()-1])).escapeCsv();

							if ( (tFieldName == 'Expected_Contract_Start_Date__c') || (tFieldName == 'Expected_Contract_End_Date__c') ){
								// Format Date Value - MM/DD/YYYY
								Date dValue = Date.valueOf(oSObject.get(lstFieldPart[lstFieldPart.size()-1]));
								if (dValue != null){
									tData = dValue.format();
								}

							}


						}catch(Exception oEX){ 
							System.debug('**BC** Error related to field "' + tFieldName + '"" on line ' + oEX.getLineNumber() + ' : ' + oEX.getMessage());
						}

					}

					System.debug('**BC** tFieldName : ' + tFieldName);
					System.debug('**BC** tData : ' + tData);

					lstData_Row.add(tData);
					
				}

				iCounter++;

	        	lstDataExport.add(String.join(lstData_Row, tExportSep));

	        	// Set the filename
	        	if (String.isBlank(tFileName)){

					try{ 

						List<String> lstFieldPart = tFileName_Field.split('\\.');
						sObject oSObject = oOpportunity;
						for (Integer i = 0; i < lstFieldPart.size() - 1; i++){
							oSObject = oSObject.getSObject(lstFieldPart[i]);
						}
						String tFileName_Field_Value = String.valueOf(oSObject.get(lstFieldPart[lstFieldPart.size()-1])).escapeCsv();

						tFileName = tFileName_Field_Value.replaceAll('[^a-zA-Z0-9]', '_') + '.csv';

					}catch(Exception oEX){
						System.debug('**BC** Error while setting filename on line ' + oEX.getLineNumber() + ' : ' + oEX.getMessage());
						tFileName = 'extract_' + idData + '.csv';
					}

	        	}

			}

			System.debug('**BC** lstDataExport (' + lstDataExport.size() + ') : ' + lstDataExport);

			tDataExport = String.join(lstDataExport, '\n');

		}catch(Exception oEX){
			tDataExport = 'Error while exporting ' + tExportType + ' for record "' + idData + '" : ' + oEX.getMessage();
		}

		return tDataExport;
			
	}
	
	private String createDataExport_OpportunityBusinessCriticalTender(){

		tDataExport = '';

		try{

			if (Test.isRunningTest()) clsUtil.bubbleException('ctrl_ExportData.createDataExport_OpportunityBusinessCriticalTender');

			// Set settings for the Data Export File
			tExportSep = '\t';
			tContentType = 'text/tab-separated-values';

			// Set the name of the file
			String tFileName_Field = 'Opportunity__r.Name';
			tFileName = '';

	        // Setup the fieldmapping - these field will be extracted in this sequence
	        List<String> fieldNames = new List<String>();
			fieldNames.add('Opportunity__r.Opportunity_Number__c');
			fieldNames.add('AccountGroup');
			fieldNames.add('Opportunity_Opportunity_Lot_Number__c');
			fieldNames.add('Award_Type__c');
			fieldNames.add('Opportunity__r.Alt_Payment_terms_2__c');
			fieldNames.add('Opportunity__r.Name');
			fieldNames.add('Name');
			fieldNames.add('Lot_Owner__r.Alias_unique__c');
			fieldNames.add('CustomerId');
			fieldNames.add('Account__r.SAP_Sales_Org__c');
			fieldNames.add('Account__r.SAP_Sales_Office__c');
			fieldNames.add('Opportunity__r.IHS_Tender_Number__c');
			fieldNames.add('Opportunity__r.IHS_Tender_Number__c');
			fieldNames.add('Opportunity__r.Expected_Contract_Start_Date__c');
			fieldNames.add('Opportunity__r.Expected_Contract_End_Date__c');
			fieldNames.add('Opportunity__r.Tender_Submission_Date_Time__c');
			fieldNames.add('OrderId');
			fieldNames.add('FirstOfLoad');
			fieldNames.add('LastOfLoad');
			
			List<String> fieldLabels = new List<String>();			
			fieldLabels.add('#SFDCOpportunityNumber');
			fieldLabels.add('AccountGroup');
			fieldLabels.add('SFDCLOTNumber');
			fieldLabels.add('AwardType');
			fieldLabels.add('AltPaymentTerms');
			fieldLabels.add('TenderName');
			fieldLabels.add('LotName');
			fieldLabels.add('OnBehalfOfId');
			fieldLabels.add('CustomerId');
			fieldLabels.add('SalesOrg');
			fieldLabels.add('SalesOffice');
			fieldLabels.add('AwardedContractId');
			fieldLabels.add('SAPReferenceID');
			fieldLabels.add('ValidFrom');
			fieldLabels.add('ValidTo');
			fieldLabels.add('SubmissionDate');
			fieldLabels.add('OrderId');
			fieldLabels.add('FirstOfLoad');
			fieldLabels.add('LastOfLoad');			
						
	        // Put all fields in a set to prevent duplicates in the SOQL
			Set<String> setFieldName = new Set<String>();
			setFieldName.add('Name');
			setFieldName.add('Award_Type__c');
			setFieldName.add('Opportunity_Opportunity_Lot_Number__c');
			setFieldName.add('Opportunity__r.Id');
			setFieldName.add('Opportunity__r.Name');
			setFieldName.add('Opportunity__r.Opportunity_Number__c');
			setFieldName.add('Lot_Owner__r.Alias_unique__c');
			setFieldName.add('Account__r.SAP_ID__c');
			setFieldName.add('Account__r.Buying_Group_Number__c');
			setFieldName.add('Account__r.SAP_Sales_Org__c');
			setFieldName.add('Account__r.SAP_Sales_Office__c');
			setFieldName.add('Opportunity__r.IHS_Tender_Number__c');
			setFieldName.add('Opportunity__r.Expected_Contract_Start_Date__c');
			setFieldName.add('Opportunity__r.Expected_Contract_End_Date__c');
			setFieldName.add('Opportunity__r.Tender_Submission_Date_Time__c');
			setFieldName.add('Opportunity__r.Alt_Payment_terms_2__c');
			setFieldName.add(tFileName_Field);

	        // Select the data which will be used to create the Export File
	        String tSOQL = 'SELECT Id';
	        for (String tFieldName : setFieldName){
	        	tSOQL += ',' + tFieldName;
	        }
	        tSOQL += ' FROM Opportunity_Lot__c';
	        tSOQL += ' WHERE Opportunity__c = :idData AND Reason_for_No_Offer__c = null';
	        tSOQL += ' ORDER BY Id';
	        List<Opportunity_Lot__c> lstData = Database.query(tSOQL);


			// Create Header Row
			List<String> lstDataExport = new List<String>();
			lstDataExport.add(String.join(fieldLabels, tExportSep));

			// Creata Data Row(s)
			Integer iCounter = 0;
			for (Opportunity_Lot__c oOpportunityLot : lstData){

				List<String> lstData_Row = new List<String>();

				for (String tFieldName : fieldNames){

					String tData = '';

					if (tFieldName == 'FirstOfLoad'){

						tData = 'false';
						if (iCounter == 0){
							tData = 'true';
						}

					}else if (tFieldName == 'LastOfLoad'){

						tData = 'false';
						if (iCounter == lstData.size() - 1){
							tData = 'true';
						}

					}else if (tFieldName == 'CustomerId'){

						tData = oOpportunityLot.Account__r.SAP_ID__c;
						if (!String.isBlank(oOpportunityLot.Account__r.Buying_Group_Number__c)){
							tData = oOpportunityLot.Account__r.Buying_Group_Number__c;
						}

					}else if ( (tFieldName == 'AccountGroup') || (tFieldName == 'OrderId') ){

						tData = '';

						if (tFieldName == 'OrderId'){
							tData = String.valueOf(iCounter + 1);
						}

					}else{

						try{ 

							List<String> lstFieldPart = tFieldName.split('\\.');
							sObject oSObject = oOpportunityLot;
							for (Integer i = 0; i < lstFieldPart.size() - 1; i++){
								if(oSObject != null) oSObject = oSObject.getSObject(lstFieldPart[i]);
							}
							if(oSObject != null) tData = String.valueOf(oSObject.get(lstFieldPart[lstFieldPart.size()-1])).escapeCsv();

							if ( (tFieldName == 'Opportunity__r.Expected_Contract_Start_Date__c') || (tFieldName == 'Opportunity__r.Expected_Contract_End_Date__c') ){
								// Format Date Value - MM/DD/YYYY
								Date dValue = Date.valueOf(oSObject.get(lstFieldPart[lstFieldPart.size()-1]));
								if (dValue != null){
									tData = dValue.format();
//									tData = clsUtil.tFormatDate(dValue, 'MDY', '/').escapeCsv();
								}

							}else if ( (tFieldName == 'Opportunity__r.Tender_Submission_Date_Time__c') ){
								// Format Date Value - MM/DD/YYYY
								Datetime dtValue = Datetime.valueOf(oSObject.get(lstFieldPart[lstFieldPart.size()-1]));
								if (dtValue != null){
									tData = dtValue.format();
//									tData = clsUtil.tFormatDate(dtValue.date(), 'MDY', '/').escapeCsv();
								}

							}else if ( (tFieldName == 'Opportunity__r.Alt_Payment_terms_2__c') ){
																
								if(tData != null) tData = tData.split(' - ')[0].trim();								
							}


						}catch(Exception oEX){ 
							System.debug('**BC** Error related to field "' + tFieldName + '"" on line ' + oEX.getLineNumber() + ' : ' + oEX.getMessage());
						}

					}

					System.debug('**BC** tFieldName : ' + tFieldName);
					System.debug('**BC** tData : ' + tData);

					lstData_Row.add(tData);
					
				}

				iCounter++;

	        	lstDataExport.add(String.join(lstData_Row, tExportSep));

	        	// Set the filename
	        	if (String.isBlank(tFileName)){

					try{ 

						List<String> lstFieldPart = tFileName_Field.split('\\.');
						sObject oSObject = oOpportunityLot;
						for (Integer i = 0; i < lstFieldPart.size() - 1; i++){
							oSObject = oSObject.getSObject(lstFieldPart[i]);
						}
						String tFileName_Field_Value = String.valueOf(oSObject.get(lstFieldPart[lstFieldPart.size()-1])).escapeCsv();

						tFileName = tFileName_Field_Value.replaceAll('[^a-zA-Z0-9]', '_') + '.csv';

					}catch(Exception oEX){
						System.debug('**BC** Error while setting filename on line ' + oEX.getLineNumber() + ' : ' + oEX.getMessage());
						tFileName = 'extract_' + idData + '.csv';
					}

	        	}

			}

			System.debug('**BC** lstDataExport (' + lstDataExport.size() + ') : ' + lstDataExport);

			tDataExport = String.join(lstDataExport, '\n');

		}catch(Exception oEX){
			tDataExport = 'Error while exporting ' + tExportType + ' for record "' + idData + '" : ' + oEX.getMessage();
		}

		return tDataExport;

	}
	//------------------------------------------------------------------------------


}
//--------------------------------------------------------------------------------------------------------------------