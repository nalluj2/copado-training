@isTest
private class Test_ba_SynchronizationService_Outbound {
	
	private static testmethod void outboundNotification(){
		
		Synchronization_Service_Setting__c syncSettings = new Synchronization_Service_Setting__c();
		syncSettings.username__c = 'test@medtronic.com';
		syncSettings.password__c = 'password';
		syncSettings.client_id__c = '123456789';
		syncSettings.client_secret__c = 'XXXXX';
		syncSettings.is_sandbox__c = true;
		syncSettings.Sync_Enabled__c = true;
		insert syncSettings;	
		
		Sync_Notification__c outboundNotification = new Sync_Notification__c();
		outboundNotification.Record_Object_Type__c = 'Case';
		outboundNotification.Record_Id__c = 'Record_Id';
		outboundNotification.Status__c = 'New';
		outboundNotification.Type__c = 'Outbound';
		outboundNotification.Notification_Id__c = 'Outbound_Notification_Id';				
		insert outboundNotification;
		
		Test.startTest();
					
		ba_SynchronizationService_Outbound batch = new ba_SynchronizationService_Outbound();
		Database.executeBatch(batch);
		
		CalloutMock mockImpl = new CalloutMock();
		
		Test.setMock(HttpCalloutMock.class, mockImpl);
		bl_SynchronizationService_Utils.calloutMock = mockImpl;	
		
		Test.stopTest();
	}
	
	private static testmethod void outboundNotification_err(){
		
		Synchronization_Service_Setting__c syncSettings = new Synchronization_Service_Setting__c();
		syncSettings.username__c = 'test@medtronic.com';
		syncSettings.password__c = 'password';
		syncSettings.client_id__c = '123456789';
		syncSettings.client_secret__c = 'XXXXX';
		syncSettings.is_sandbox__c = true;
		syncSettings.Sync_Enabled__c = true;
		insert syncSettings;	
		
		Sync_Notification__c outboundNotification = new Sync_Notification__c();
		outboundNotification.Record_Object_Type__c = 'Case';
		outboundNotification.Record_Id__c = 'Record_Id';
		outboundNotification.Status__c = 'New';
		outboundNotification.Type__c = 'Outbound';
		outboundNotification.Notification_Id__c = 'Outbound_Notification_Id';				
		insert outboundNotification;
		
		Test.startTest();
					
		ba_SynchronizationService_Outbound batch = new ba_SynchronizationService_Outbound();
		Database.executeBatch(batch);
		
		CalloutMock_err mockImpl = new CalloutMock_err();
		
		Test.setMock(HttpCalloutMock.class, mockImpl);
		bl_SynchronizationService_Utils.calloutMock = mockImpl;	
		
		Test.stopTest();
	}
	
	public class CalloutMock implements HttpCalloutMock {

        public HTTPResponse respond(HTTPRequest req) {
            
            HttpResponse resp = new HttpResponse();
            resp.setStatusCode(200);
            resp.setStatus('Complete');
            
            String respBody; 
            
            if(req.getEndpoint().endsWith('services/oauth2/token')){
            	respBody =  '{' +
					            '"id" : "1",' +
					            '"issued_at" : "1",' +
					            '"instance_url" : "1",' +
					            '"signature" : "1",' +
					            '"access_token" : "1",' +
					            '"token_type" : "1"' +
				            '}';
				            
            }else{
            	
            	respBody =  req.getBody();            	          	
            }
            
            resp.setBody(respBody);
            
            return resp;
        }
	}
	
	public class CalloutMock_err implements HttpCalloutMock {

        public HTTPResponse respond(HTTPRequest req) {
            
            HttpResponse resp = new HttpResponse();
                       
            if(req.getEndpoint().endsWith('services/oauth2/token')){
            	
            	resp.setStatusCode(200);
            	resp.setStatus('Complete');
            	
            	String respBody =  '{' +
					            '"id" : "1",' +
					            '"issued_at" : "1",' +
					            '"instance_url" : "1",' +
					            '"signature" : "1",' +
					            '"access_token" : "1",' +
					            '"token_type" : "1"' +
				            '}';
				resp.setBody(respBody);
				            
            }else{
            	
            	resp.setStatusCode(400);
            	resp.setStatus('Complete');		
            }
                        
            return resp;
        }
	}
}