@isTest
private class TEST_ut_StaticValues {
	
	@isTest static void createTestData() {
		
        clsTestData_MasterData.tCompanyCode = 'EUR';
//        clsTestData_MasterData.tBusinessUnitName = 'Surgical Technologies';
        clsTestData_MasterData.createBusinessUnit(true);

	}
	
	@isTest static void test_ut_StaticValues(){

		createTestData();

		String tTest = '';
		Set<String> setTest = new Set<String>();
		Set<Id> setTest_ID = new Set<Id>();

		tTest = ut_StaticValues.INTERFACE_NAME_SAP_OPPORTUNITY;

		tTest = ut_StaticValues.INTERFACE_NAME_SAP_OPPORTUNITY;

		tTest = ut_StaticValues.RTDEVNAME_AFF_A2A;
	
		tTest = ut_StaticValues.RTDEVNAME_CAN_DIB_OPPORTUNITY;
		//tTest = ut_StaticValues.RTDEVNAME_EUR_DIB_OPPORTUNITY;
	
		tTest = ut_StaticValues.RTDEVNAME_SAP_PATIENT;
	
		tTest = ut_StaticValues.RTDEVNAME_CAN_DIB_LEAD;
		tTest = ut_StaticValues.RTDEVNAME_CAN_DIB_LEAD_CONVERSION;
	
		tTest = ut_StaticValues.COUNTRY_ASSIGNMENT_BUCKET;
	
		tTest = ut_StaticValues.COMPANY_EUROPE;
	
		tTest = ut_StaticValues.BUG_CVG;
	
		tTest = ut_StaticValues.BU_CRHF;

		tTest = ut_StaticValues.APLEVEL_BUG;
		tTest = ut_StaticValues.APLEVEL_BU;
		tTest = ut_StaticValues.APLEVEL_SBU;
		
		tTest = ut_StaticValues.ACCOUNT_TEAM_MEMBER_ROLE_TEAM_LEADER;
		
		tTest = ut_StaticValues.INSURER_TYPE_PRIMARY;
		tTest = ut_StaticValues.INSURER_TYPE_SECONDARY;
		tTest = ut_StaticValues.INSURER_TYPE_ADDITIONAL;
	
		tTest = ut_StaticValues.ACCNT_TYPE_PER_BU;
		tTest = ut_StaticValues.ACCNT_TYPE_PER_SBU;

		setTest_ID = ut_StaticValues.setOfOpportunityIdAlreadySent;
		setTest = ut_StaticValues.setOfTriggersExecuted;

		tTest = ut_StaticValues.getUserProfileName();

		Set<String> setBusinessUnitName = new Set<String>{'Neurovascular'};
		Set<String> setCompanyName = new Set<String>{'Europe'};
		Map<String, Id> mapTest = ut_StaticValues.getBusinessUnitIdByName(setBusinessUnitName, setCompanyName);

		Set<String> setBusinessUnitGroupName = new Set<String>{'Restorative'};
		mapTest = ut_StaticValues.getBusinessUnitGroupIdByName(setBusinessUnitGroupName, setCompanyName);


	}
	
}