public without sharing class ctrl_LayoutPreference{
       
    public PageReference initialize(){
    	
    	String params = CryptoUtils.decryptText(ApexPages.currentPage().getParameters().get('params'));
    	
    	String[] inputParams = params.split('&');
    	
        String profileId = inputParams[0];
        String userId = inputParams[1];
        
        //If any is null we finish here
        if(userId == null || profileId == null) return null;
            
        User user = [select Id, ProfileId, Profile.Name from User where Id = :userId];
        if(user.Profile.Name.endsWith(' - Mobile') == false) user.Previous_Profile_Id__c = user.ProfileId;
        user.ProfileId = profileId;
        
        update user;
               
        return null;
    }   
}