@isTest public class clsTestData_CustomSetting {

	public static Boolean bCustomSettingDataCreated = false;

	public static WebServiceSetting__c oMain_CustomSetting_WebserviceSetting;
	public static List<WebServiceSetting__c> lstCustomSetting_WebServiceSetting = new List<WebServiceSetting__c>();

	public static SAP_Interface_Mappings__c oMain_CustomSetting_WebServiceMappings;
	public static List<SAP_Interface_Mappings__c> lstCustomSetting_WebServiceMappings = new List<SAP_Interface_Mappings__c>();

	public static SystemAdministratorProfileId__c oSystemAdminstratorProfileId;
	public static List<SystemAdministratorProfileId__c> lstSystemAdminstratorProfileId = new List<SystemAdministratorProfileId__c>();

	public static SMAX_Email_Settings__c oMain_CustomSetting_SMAXEmailSettings;
	public static List<SMAX_Email_Settings__c> lstCustomSetting_SMAXEmailSettings = new List<SMAX_Email_Settings__c>();

	public static Opportunity_Trigger_Settings__c oMain_OpportunityTriggerSettings;
	public static List<Opportunity_Trigger_Settings__c> lstOpportunityTriggerSettings = new List<Opportunity_Trigger_Settings__c>();

    public static Opportunity_European_Record_Types__c oMain_OpportunityEuropeanRecordTypes;
    public static List<Opportunity_European_Record_Types__c> lstOpportunityEuropeanRecordTypes = new List<Opportunity_European_Record_Types__c>();

    public static Email2SFDC_PublicTask__c oMain_Email2SFDC_PublicTask;
    public static String tEmail2SFDC_PublicTask_SetupOwnerId = '00e20000000mwLWAAY';
    public static Boolean bEmail2SFDC_PublicTask_PublicTask = true;
    public static Boolean bEmail2SFDC_PublicTask_EmailToSalesforceOnly = false;
    public static List<Email2SFDC_PublicTask__c> lstEmail2SFDC_PublicTask = new List<Email2SFDC_PublicTask__c>();

    public static Map<String, Boolean> mapCampaignMemberStatus_Responded = new Map<String, Boolean>{'Invited'=>false, 'Confirmed'=>true, 'No Show'=>false};
    public static List<CampaignMemberStatus__c> lstCampaignMemberStatus = new List<CampaignMemberStatus__c>();
    public static CampaignMemberStatus__c oMain_CampaignMemberStatus;

    public static Set<String> setFieldName_HistoryTracking_AccountExtendedProfile = new Set<String>{'Section_Owner_SI_AST_Driven__c','Commencement_Date__c','General_Surgery_procedures__c','Gynecologic_procedures__c','Head_Neck_procedures__c','Hemorrhoids_procedures__c'};
    public static HistoryTracking_AccountExtendedProfile__c oMain_HistoryTracking_AccountExtendedProfile;
    public static List<HistoryTracking_AccountExtendedProfile__c> lstHistoryTracking_AccountExtendedProfile = new List<HistoryTracking_AccountExtendedProfile__c>();

	public static Set<String> setFieldName_HistoryTracking_ContractRepository = new Set<String>{'Account__c','Primary_Contract_Type__c','Comments__c','VDV_Deal_ID__c','VDV_Deal_Type__c','Delivery_Terms__c'};
    public static HistoryTracking_ContractRepository__c oMain_HistoryTracking_ContractRepository;
    public static List<HistoryTracking_ContractRepository__c> lstHistoryTracking_ContractRepository = new List<HistoryTracking_ContractRepository__c>();


    //---------------------------------------------------------------------------------------------------
    // Custom Setting Data
    //---------------------------------------------------------------------------------------------------
    public static void createCustomSettingData(){
    	createCustomSettingData(true);
    }
    public static void createCustomSettingData(Boolean bInsert){

        if (bCustomSettingDataCreated == false){

            createCustomSettingData_WebServiceSetting(bInsert);
            createCustomSettingData_WebServiceMappings(bInsert);
            createCustomSettingData_SystemAdministratorProfileId(bInsert);
            createCustomSettingData_SMAXEmailSettings(bInsert);
            createCustomSetting_OpportunityTriggerSettings(bInsert);
            createOpportunityEuropeanRecordTypes(bInsert);
            createEmail2SFDC_PublicTask(bInsert);
            createCampaignMemberStatus(bInsert);
            createHistoryTracking_AccountExtendedProfile(bInsert);
            createHistoryTracking_ContractRepository(bInsert);

            bCustomSettingDataCreated = true;

        }

    }
    

    public static List<WebServiceSetting__c> createCustomSettingData_WebServiceSetting(){
    	return createCustomSettingData_WebServiceSetting(true);
    }
    public static List<WebServiceSetting__c> createCustomSettingData_WebServiceSetting(Boolean bInsert){

        if (oMain_CustomSetting_WebserviceSetting == null){

            Set<String> setSFDCObjectName = new Set<String>();
                setSFDCObjectName.add('Case_NotificationSAP');
                setSFDCObjectName.add('ServiceOrder_NotificationSAP');
                setSFDCObjectName.add('ServiceOrderComplete_NotificationSAP');
                setSFDCObjectName.add('Attachment_NotificationSAP');
                setSFDCObjectName.add('InstalledProduct_NotificationSAP');

            // Creata Custom Setting Data
            lstCustomSetting_WebServiceSetting = new List<WebServiceSetting__c>();
            for (String tSFDCObjectName : setSFDCObjectName){
                WebServiceSetting__c oSetting = new WebServiceSetting__c();
                    oSetting.Name = tSFDCObjectName;
                    oSetting.EndpointUrl__c = 'http://144.15.228.14:33500/ws/MdtAny_SfdcInt.SRServiceOrder.webservice.providers.ws:processServiceOrder_WSDL';
                    oSetting.XMLNS__c = 'mdt="http://msplwa107-pr.wby1-dev.medtronic.com/MdtAny_SfdcInt.SRServiceOrder.webservice.providers.ws:processServiceOrder_WSDL"';
                    oSetting.WebServiceName__c = 'mdt:webservicename';
                    oSetting.DataName__c = 'WebServiceName';
                    oSetting.Timeout__c = 60000;
                    oSetting.Username__c = 'username';
                    oSetting.Password__c = 'password';
                    oSetting.Callout_Failure_Email_Address__c = 'callout.failure@medtronic.com';
                    oSetting.Callout_Max_Retries__c = 5;
                    oSetting.Callout_Retry_Minutes__c = '60';
                    oSetting.Org_Id__c = Userinfo.getOrganizationId().left(15);
                    oSetting.Instance_Id__c = 'XXX';
                lstCustomSetting_WebServiceSetting.add(oSetting);
            }

            if (bInsert) insert lstCustomSetting_WebServiceSetting;
            oMain_CustomSetting_WebServiceSetting = lstCustomSetting_WebServiceSetting[0];

        }

        return lstCustomSetting_WebServiceSetting;

    }


    public static List<SAP_Interface_Mappings__c> createCustomSettingData_WebServiceMappings(){
    	return createCustomSettingData_WebServiceMappings(true);
    }
    public static List<SAP_Interface_Mappings__c> createCustomSettingData_WebServiceMappings(Boolean bInsert){

        if (oMain_CustomSetting_WebServiceMappings == null){

            lstCustomSetting_WebServiceMappings = new List<SAP_Interface_Mappings__c>();
            
            SAP_Interface_Mappings__c oSetting = new SAP_Interface_Mappings__c();
	            oSetting.Name = '1';
	            oSetting.API_Field_Name__c  = 'GCH_Question_1__c';
	            oSetting.API_Object_Name__c = 'Case';
	            oSetting.From__c            = 'Yes';
	            oSetting.To__c              = 'Y';
            lstCustomSetting_WebServiceMappings.add(oSetting);
            
            oSetting = new SAP_Interface_Mappings__c();
	            oSetting.Name = '2';
	            oSetting.API_Field_Name__c  = 'GCH_Question_1__c';
	            oSetting.API_Object_Name__c = 'Case';
	            oSetting.From__c            = 'No';
	            oSetting.To__c              = 'N';
            lstCustomSetting_WebServiceMappings.add(oSetting);
            
            if (bInsert) insert lstCustomSetting_WebServiceMappings;

            oMain_CustomSetting_WebServiceMappings = lstCustomSetting_WebServiceMappings[0];

        }

        return lstCustomSetting_WebServiceMappings;

    }


    public static List<SystemAdministratorProfileId__c> createCustomSettingData_SystemAdministratorProfileId(){
    	return createCustomSettingData_SystemAdministratorProfileId(true);
    }
    public static List<SystemAdministratorProfileId__c> createCustomSettingData_SystemAdministratorProfileId(Boolean bInsert){

    	if (oSystemAdminstratorProfileId == null){

	        List<Profile> lstProfile = [SELECT Id, Name FROM Profile ORDER BY Name];
	        Map<String, String> mapProfileId = new Map<String, String>();
	        for (Profile oProfile : lstProfile){
	            mapProfileId.put(oProfile.Name.toUpperCase(), String.valueOf(oProfile.Id).left(15));
	        }

	        lstSystemAdminstratorProfileId = new List<SystemAdministratorProfileId__c>();
	        
	        oSystemAdminstratorProfileId = new SystemAdministratorProfileId__c();
	            oSystemAdminstratorProfileId.Name = 'Default';
	            oSystemAdminstratorProfileId.CVent__c = mapProfileId.get('CVENT');
	            oSystemAdminstratorProfileId.SystemAdministratorMDT__c = mapProfileId.get('SYSTEM ADMINISTRATOR MDT');
	            oSystemAdminstratorProfileId.SystemAdministrator__c = mapProfileId.get('SYSTEM ADMINISTRATOR');
	            oSystemAdminstratorProfileId.ITBusinessAnalyst__c = mapProfileId.get('IT BUSINESS ANALYST');
	            oSystemAdminstratorProfileId.MMX_Interface__c = mapProfileId.get('MMX INTERFACE');
	            oSystemAdminstratorProfileId.SAPInterface__c = mapProfileId.get('SAP INTERFACE');
	        lstSystemAdminstratorProfileId.add(oSystemAdminstratorProfileId);

	        if (bInsert) insert lstSystemAdminstratorProfileId;

	    }

	    return lstSystemAdminstratorProfileId;

    }


    public static List<SMAX_Email_Settings__c> createCustomSettingData_SMAXEmailSettings(){
    	return createCustomSettingData_SMAXEmailSettings(true);
    }
    public static List<SMAX_Email_Settings__c> createCustomSettingData_SMAXEmailSettings(Boolean bInsert){

        if (oMain_CustomSetting_SMAXEmailSettings == null){

            lstCustomSetting_SMAXEmailSettings = new List<SMAX_Email_Settings__c>();

            Set<String> setData = new Set<String>();
                setData.add('AD_HOC');
                //setData.add('PART_REQUEST');
                setData.add('SERVICECENTER');
                setData.add('SPAREPARTS');

            for (String tData : setData){
                SMAX_Email_Settings__c oSetting = new SMAX_Email_Settings__c();
                    oSetting.Name = tData;
                    oSetting.Subject__c  = 'Subject ' + tData;
                    oSetting.TO__c = 'info.to@medtronic.test';
                    //oSetting.CC__c = 'info.cc@medtronic.test';
                    //oSetting.BCC__c = 'info.bcc@medtronic.test';
                lstCustomSetting_SMAXEmailSettings.add(oSetting);
            }

            if (bInsert) insert lstCustomSetting_SMAXEmailSettings;

            oMain_CustomSetting_SMAXEmailSettings = lstCustomSetting_SMAXEmailSettings[0];

        }

        return lstCustomSetting_SMAXEmailSettings;
    }


    public static List<Opportunity_Trigger_Settings__c> createCustomSetting_OpportunityTriggerSettings(){
    	return createCustomSetting_OpportunityTriggerSettings(true);
    }
    public static List<Opportunity_Trigger_Settings__c> createCustomSetting_OpportunityTriggerSettings(Boolean bInsert){

    	if (oMain_OpportunityTriggerSettings == null){

    		lstOpportunityTriggerSettings = new List<Opportunity_Trigger_Settings__c>();

            Opportunity_Trigger_Settings__c oSetting = new Opportunity_Trigger_Settings__c();
                oSetting.Name = 'Default';
                oSetting.Create_Insurance_Verification_Records_F__c = false;
            lstOpportunityTriggerSettings.add(oSetting);

            if (bInsert) insert lstOpportunityTriggerSettings;

            oMain_OpportunityTriggerSettings = lstOpportunityTriggerSettings[0];
    	}

    	return lstOpportunityTriggerSettings;

    }


    public static List<Opportunity_European_Record_Types__c> createOpportunityEuropeanRecordTypes(){
        return createOpportunityEuropeanRecordTypes(true);
    }
    public static List<Opportunity_European_Record_Types__c> createOpportunityEuropeanRecordTypes(Boolean bInsert){

        if (oMain_OpportunityEuropeanRecordTypes == null){

            lstOpportunityEuropeanRecordTypes = new List<Opportunity_European_Record_Types__c>();

            String tData_All = '2;Cardio_Vascular_Tender,3;CRDM_Service_Contract,5;CRDM_Tender,6;Diabetes_Standard_Opportunity,7;Diabetes_Tender,8;Non_Capital_Opportunity,9;Neuromodulation_Tender,12;Spine_Biologics_Tender,13;Service_Contract_Opportunity,15;Surgical_Technologies_Tender,16;CAN_Cardio_Vascular_Tender,17;CAN_Cardio_Vascular_Standard_Opportunity,18;CAN_CRDM_Standard_Opportunity,19;CAN_CRDM_Tender,20;CAN_Diabetes_Standard_Opportunity,21;CAN_Diabetes_Tender,22;CAN_DIB_OPPORTUNITY,23;CAN_Neuromodulation_Standard_Opportunity,24;CAN_Neuromodulation_Tender,25;CAN_Spine_Biologics_Standard_Opportunity,26;CAN_Spine_Biologics_Tender,27;CAN_Surgical_Technologies_Standard_Opportunity,28;CAN_Surgical_Technologies_Tender,29;CVG_Project,30;CAN_DIB_OPPORTUNITY,31;Capital_Opportunity,32;CAN_Surgical_Technologies_Capital_Opportunity,33;MEA_CRDM_Tender,34;MEA_Cardio_Vascular_Tender,35;MEA_Diabetes_Tender,36;MEA_Neuromodulation_Tender,37;MEA_Spine_Biologics_Tender,38;MEA_Surgical_Technologies_Tender,39;Business_Critical_Tender';
            List<String> lstData = tData_All.split(',');

            for (String tData : lstData){
                List<String> lstData_Field = tData.split(';');
                Opportunity_European_Record_Types__c oSetting = new Opportunity_European_Record_Types__c();
                    oSetting.Name = lstData_Field[0];
                    oSetting.European_Record_Types__c = lstData_Field[1];
                lstOpportunityEuropeanRecordTypes.add(oSetting);
            }

            if (bInsert) insert lstOpportunityEuropeanRecordTypes;

            oMain_OpportunityEuropeanRecordTypes = lstOpportunityEuropeanRecordTypes[0];
        }

        return lstOpportunityEuropeanRecordTypes;

    }    


    public static List<Email2SFDC_PublicTask__c> createEmail2SFDC_PublicTask(){
        return createEmail2SFDC_PublicTask(true);
    }
    public static List<Email2SFDC_PublicTask__c> createEmail2SFDC_PublicTask(Boolean bInsert){

        if (oMain_Email2SFDC_PublicTask == null){

            lstEmail2SFDC_PublicTask = new List<Email2SFDC_PublicTask__c>();

            tEmail2SFDC_PublicTask_SetupOwnerId = clsUtil.isNull(tEmail2SFDC_PublicTask_SetupOwnerId, '00e20000000mwLWAAY');

            List<String> lstSetupOwnerId = tEmail2SFDC_PublicTask_SetupOwnerId.split(';');

            for (String tSetupOwnerId : lstSetupOwnerId){

                Email2SFDC_PublicTask__c oEmail2SFDC_PublicTask = new Email2SFDC_PublicTask__c();

                    oEmail2SFDC_PublicTask.Public_Task__c = bEmail2SFDC_PublicTask_PublicTask;
                    oEmail2SFDC_PublicTask.Email_To_Salesforce_Only__c = bEmail2SFDC_PublicTask_EmailToSalesforceOnly;
                    oEmail2SFDC_PublicTask.SetupOwnerId = tSetupOwnerId;

                lstEmail2SFDC_PublicTask.add(oEmail2SFDC_PublicTask);

            }

            if (bInsert) insert lstEmail2SFDC_PublicTask;

            oMain_Email2SFDC_PublicTask = lstEmail2SFDC_PublicTask[0];
        }

        return lstEmail2SFDC_PublicTask;

    }


    public static List<CampaignMemberStatus__c> createCampaignMemberStatus(){
        return createCampaignMemberStatus(true);
    }
    public static List<CampaignMemberStatus__c> createCampaignMemberStatus(Boolean bInsert){
        return createCampaignMemberStatus(bInsert, 'RecordTypeId', '012w0000000QBfRAAW');
    }
    public static List<CampaignMemberStatus__c> createCampaignMemberStatus(Boolean bInsert, String tFilterField, String tFilterValue){

        if (oMain_CampaignMemberStatus == null){

            lstCampaignMemberStatus = new List<CampaignMemberStatus__c>();

            Integer iCounter = 1;
            for (String tCampaignMemberStatus : mapCampaignMemberStatus_Responded.keySet()){

                CampaignMemberStatus__c oCMS = new CampaignMemberStatus__c();
                    oCMS.Name = 'CMS' + iCounter;
                    oCMS.Member_Status_Label__c = tCampaignMemberStatus;
                    if (iCounter > 1){
                        oCMS.IsDefault__c = false;
                    }else{
                        oCMS.IsDefault__c = true;
                    }
                    oCMS.HasResponded__c = mapCampaignMemberStatus_Responded.get(tCampaignMemberStatus);
                    oCMS.SortOrder__c = 10 + iCounter;
                    oCMS.Filter_Field__c = tFilterField;
                    oCMS.Filter_Value_Text__c = tFilterValue;

                lstCampaignMemberStatus.add(oCMS);

                iCounter++;
            }

            if (bInsert) insert lstCampaignMemberStatus;

            oMain_CampaignMemberStatus = lstCampaignMemberStatus[0];
        }

        return lstCampaignMemberStatus;
    }


    public static List<HistoryTracking_AccountExtendedProfile__c> createHistoryTracking_AccountExtendedProfile(){
        return createHistoryTracking_AccountExtendedProfile(true);
    }
    public static List<HistoryTracking_AccountExtendedProfile__c> createHistoryTracking_AccountExtendedProfile(Boolean bInsert){

        if (oMain_HistoryTracking_AccountExtendedProfile == null){

            for (String tFieldName : setFieldName_HistoryTracking_AccountExtendedProfile){

                HistoryTracking_AccountExtendedProfile__c oHistoryTracking_AccountExtendedProfile = new HistoryTracking_AccountExtendedProfile__c();
                    oHistoryTracking_AccountExtendedProfile.Name = tFieldName;
                    if (tFieldName == 'Section_Owner_SI_AST_Driven__c'){
                        // Reference Field
                        oHistoryTracking_AccountExtendedProfile.ReferenceTo__c = 'User';
                        oHistoryTracking_AccountExtendedProfile.ReferenceToField__c = 'Name';
                    }
                lstHistoryTracking_AccountExtendedProfile.add(oHistoryTracking_AccountExtendedProfile);

            }

            if (bInsert) insert lstHistoryTracking_AccountExtendedProfile;

            oMain_HistoryTracking_AccountExtendedProfile = lstHistoryTracking_AccountExtendedProfile[0];
        }
        return lstHistoryTracking_AccountExtendedProfile;
    }


    public static List<HistoryTracking_ContractRepository__c> createHistoryTracking_ContractRepository(){
        return createHistoryTracking_ContractRepository(true);
    }
    public static List<HistoryTracking_ContractRepository__c> createHistoryTracking_ContractRepository(Boolean bInsert){

        if (oMain_HistoryTracking_ContractRepository == null){

			lstHistoryTracking_ContractRepository = new List<HistoryTracking_ContractRepository__c>();
			Integer iCounter = 0;
            for (String tFieldName : setFieldName_HistoryTracking_ContractRepository){

                HistoryTracking_ContractRepository__c oHistoryTracking_ContractRepsitory = new HistoryTracking_ContractRepository__c();
					oHistoryTracking_ContractRepsitory.Name = String.valueOf(iCounter);
                    oHistoryTracking_ContractRepsitory.Field_Name__c = tFieldName;
                    if (tFieldName == 'Account__c'){
                        // Reference Field
                        oHistoryTracking_ContractRepsitory.Reference_To__c = 'Account';
                        oHistoryTracking_ContractRepsitory.Reference_To_Field__c = 'Name';
                    }
                lstHistoryTracking_ContractRepository.add(oHistoryTracking_ContractRepsitory);

				iCounter++;

            }

            if (bInsert) insert lstHistoryTracking_ContractRepository;

            oMain_HistoryTracking_ContractRepository = lstHistoryTracking_ContractRepository[0];
        }
        return lstHistoryTracking_ContractRepository;
    }
    //---------------------------------------------------------------------------------------------------
	
}