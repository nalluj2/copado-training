/**
 *      Created Date : 20120906
 *      Description : This class is use to see the relation between Territory and Account
 * 
 *      Author = Rudy De Coninck
 */


public with sharing class bl_Territory {

	//This static variable is used to store Account/Territory information to be used in before and after trigger
	private static Map<Id, Map<String, AccountTerritoryShare>> accountTerritoryShares;
	
	//All Business Units that belong to MITG group (for all regions)
    private static Set<Id> mitgBUs {
    	get{
    		
    		if(mitgBUs == null){
    			
    			mitgBUs = new Set<Id>();
        
        		for(Business_Unit__c bu : [Select Id from Business_Unit__c where Business_Unit_Group__r.Name = 'MITG']){
            		mitgBUs.add(bu.Id);
        		}
    		}
    		
        	return mitgBUs;
    	}
    	
    	set;
    }
    
    public static Map<String, Id> territoryTypeMap{
    	
    	get{
    		
    		if(territoryTypeMap == null){
    			
    			territoryTypeMap = new Map<String, Id>();
    			
    			for(Territory2Type terrType : [Select Id, DeveloperName from Territory2Type]){
    				
    				territoryTypeMap.put(terrType.DeveloperName, terrType.Id);
    			}
    		}
    		
    		return territoryTypeMap;
    	}
    	
    	set;
    }
	
	public static Boolean checkAccountUnderTerritory(List<ID> territories, ID accountID){
		
		System.debug('Territories '+territories+' for accountId: '+accountId);
		
		List<ObjectTerritory2Association> accountShares = [select Id from ObjectTerritory2Association 	where ObjectId = :accountId and Territory2Id IN :territories LIMIT 1];
		
		if(accountShares.size() > 0) return true;
		
		return false;
	}
				
	public static List<Territory2> getTerritoriesForAccount(Id accountId){ 
		
		List<Territory2> territories = [Select Id, Name, Territory2Type.DeveloperName, Territory_UID2__c , Business_Unit__c, Therapy_Groups_Text__c
			from Territory2 where Id IN (select Territory2Id from ObjectTerritory2Association where ObjectId = :accountId) order by Name];
		
		return territories;
		
	}
	
	//Populate the static variable with Account and Territory information for the Account Territory Changes in scope
	public static void getTerritoryForAccounts(Set<Id> accountIds){
		
		//Build final map with Territory information per Account		
		accountTerritoryShares = new Map<Id, Map<String, AccountTerritoryShare>>();		
		
		//Initialize the map
		for(Id accId : accountIds){
			
			accountTerritoryShares.put(accId, new Map<String, AccountTerritoryShare>());			
		}			
		
		for (ObjectTerritory2Association accShare : [select ObjectId, AssociationCause, Territory2.Id, Territory2.Name, Territory2.Business_Unit__c, Territory2.Company__c, Territory2.Therapy_Groups_Text__c, Territory2.Territory2Type.DeveloperName, Territory2.Territory_UID2__c from ObjectTerritory2Association where ObjectId IN :accountIds]){
						
			Map<String, AccountTerritoryShare> accountTerritories = accountTerritoryShares.get(accShare.ObjectId);
			
			AccountTerritoryShare accountTerritoryShare = new AccountTerritoryShare();
			
			accountTerritoryShare.territory = accShare.Territory2;
			accountTerritoryShare.accShare = accShare;
									
			//Order therapy group values and format them so it can be used for String comparation										
			String key = buildTerritoryKey(accShare.Territory2.Business_Unit__c, accShare.Territory2.Company__c , accShare.Territory2.Therapy_Groups_Text__c);
			
			accountTerritories.put(key, accountTerritoryShare);					
		}		
	}
	
	private static String buildTerritoryKey(String buName, String company, String therapyGroups){
		
		if(therapyGroups == null) therapyGroups = '';
						
		List<String> therapyGroupList= therapyGroups.replaceAll('\\s+', '').split(';');
		therapyGroupList.sort();
								
		return buName.toUpperCase() + ':' + company.substring(0, 15) + ':' + String.join(therapyGroupList , ';');		
	}
	
	/*
	* Return a list of account ids for the supplied territories
	*/
	public static Set<ID> calculateAccountsBelowTerritory(List<ID> territories){
		
		Set<Id> resultAccountIds= new Set<Id>();
		
		for (ObjectTerritory2Association accountShare : [select ObjectId from ObjectTerritory2Association where Territory2Id IN: territories]){
			resultAccountIds.add(accountShare.ObjectId);
		}
		
		return resultAccountIds;
	}
			
	/*
	* All children territories until territoryType = stop at child level, ALL to get all levels
	*/
	public static List<Territory2> calculateChildrenForTerritory(ID territoryId, String territoryType){
		
		List<Territory2> foundTerritories = new List<Territory2>();
		
		List<Territory2> regions = [select Id, Name, Territory2Type.DeveloperName, Territory_UID2__c, ParentTerritory2Id from Territory2 where Id = :territoryId];

		for (Territory2 region : regions){

			List<Territory2> regionId = new List<Territory2>();
			regionId.add(region);
			System.debug('Processing region '+region.name);			
			
			List<Territory2> children=regionId;
			Territory2 firstOneFound= new Territory2();
			while (firstOneFound.Territory2Type == null || firstOneFound.Territory2Type.DeveloperName.compareTo('Territory') != 0){
				children = getChildren(children);
				System.debug('Found children '+children);
				if (children.size()>0){
					firstOneFound = children.get(0);
					if (territoryType!=null && (territoryType.compareTo(firstOneFound.Territory2Type.DeveloperName)==0 || territoryType.compareTo('ALL')==0)){
						foundTerritories.addAll(children);
					}

				}else{
					break;
				}
			}
		
		}
		return foundTerritories;
		
	}
	
	
	public static List<Territory2> getChildren(List<Territory2> territories){
	
		List<Id> territoryIds = new List<Id>();
		for (Territory2 territory : territories){
			if (territory.Territory2Type.developerName.compareTo('Region')!=0){
				territoryIds.add(territory.Id);
			}else{
				territoryIds.add(territory.Id);
			}
		}
		
		List<Territory2> children = [select Id, Name, ParentTerritory2Id, Territory2Type.DeveloperName, Territory_UID2__c from Territory2 where ParentTerritory2Id in : territoryIds];
		
		return children;		
	}


	public static void linkRelationshipDataToAccountTerritoryChange(List<Account_Territory_Change__c> accountTerritoryChanges){
		
		Set<String> buNames = new Set<String>();
		Set<String> territoryNames = new Set<String>();
		Set<String> companyNames = new Set<String>();
		Set<String> territoryIds = new Set<String>();
		Set<Id> accountIds = new Set<Id>();
		
		for (Account_Territory_Change__c atc : accountTerritoryChanges){
			
			//Account
			accountIds.add(atc.Account__c);
			
			//Old Territory						
			if(atc.Old_Territory_Id__c != null && atc.Old_Territory_Id__c != '') territoryIds.add(atc.Old_Territory_Id__c);
			
			//Business Unit Name							
			if(atc.Business_Unit_Name__c != null && atc.Business_Unit_Name__c != '') buNames.add(atc.Business_Unit_Name__c);
				
			//New Territory Id/Name
			if(atc.New_Territory_ID__c != null && atc.New_Territory_ID__c != '') territoryIds.add(atc.New_Territory_ID__c);					
			else if(atc.New_Territory_Name__c != null && atc.New_Territory_Name__c != '') territoryNames.add(atc.New_Territory_Name__c);
			
			//Company				
			companyNames.add(atc.Company_Name__c);								
		}
		
		//Company Map		
		Map<String, Company__c> companyNameMap = new Map<String, Company__c>();		
		if(companyNames.size() > 0) for(Company__c company : [select Id, Name from Company__c where Name in : companyNames]) companyNameMap.put(company.Name,company);			
				
		//Business Unit Map
		Map<String,Business_Unit__c> businessUnitNameMap = new Map<String, Business_Unit__c>();		
		if(buNames.size() > 0) for(Business_Unit__c bu : [select Id, Name, Company__c from Business_Unit__c where Name in : buNames]) businessUnitNameMap.put(bu.name+bu.Company__c,bu);
		
		//Territory by Name Map				
		Map<String,Territory2> territoryNameMap = new Map<String,Territory2>();		
		if(territoryNames.size() > 0) for(Territory2 te : [select Id, Name, Territory_UID2__c, Territory2Type.DeveloperName, Therapy_Groups_Text__c from Territory2 where Name in : territoryNames]) territoryNameMap.put(te.Name,te);
			
		//Territory by Id Map
		Map<String,Territory2> territoryIdMap = new Map<String,Territory2>();		
		if(territoryIds.size() > 0) for(Territory2 te : [select Id, Name, Territory_UID2__c, Territory2Type.DeveloperName, Therapy_Groups_Text__c from Territory2 where Id in : territoryIds]) territoryIdMap.put(String.valueOf(te.Id).substring(0,15),te);
		
		//Account Map
		Map<Id, Account> accountsMap = new Map<Id, Account>([select Id, Account_Country_vs__c from account where id = : accountIds]);
		
		//Country Map
		Map<String, DIB_Country__c> countryMap = new Map<String, DIB_Country__c>();		
		for(DIB_Country__c country : [select Id, company__c, t_country__c from DIB_Country__c]) countryMap.put(country.t_country__c, country);
		
		//List of records that passed the first validation
		List<Account_Territory_Change__c> validationPass = new List<Account_Territory_Change__c>();
		//List of Ids for the Accounts related to the records that passed the validation. Used later to load data needed for the second validation step
		Set<Id> validatedAccountIds = new Set<Id>();
		
		//Validate input Ids/Names		
		for (Account_Territory_Change__c atc : accountTerritoryChanges){
			
			//For Delete operations Old Territory Id is mandatory
			if(atc.Operation__c == 'DELETE'){
				
				Territory2 oldTerritoryFound = territoryIdMap.get(atc.Old_Territory_ID__c.substring(0,15));// We use the Id 15char version
				
				if(oldTerritoryFound == null){
					
					atc.addError('Old territory not found, please check Id');
					continue;
				}
					
				atc.Old_Territory_UID__c=oldTerritoryFound.Territory_UID2__c;
				atc.Old_Territory_Name__c=oldTerritoryFound.Name;
				atc.Old_Territory_ID__c = oldTerritoryFound.Id;					
				
				//We do not add this records to the second validation list as it is not relevant for Delete operation
				
			}else{
			
				//Link Company
				Company__c companyFound = companyNameMap.get(atc.Company_Name__c);
				
				if(companyFound == null){					
					atc.addError('Company not found, please check spelling');
					continue;
				}
					
				atc.Company__c = companyFound.id;
				
				//Link Business unit
				Business_Unit__c buFound = businessUnitNameMap.get(atc.Business_Unit_Name__c+companyFound.id);
				
				if(buFound == null){
					atc.addError('Business unit not found, please check spelling');				
					continue;
				}
					
				atc.Business_Unit__c = buFound.id;
							
				//Link New Territory
				Territory2 newTerritoryFound;			
				
				if(atc.New_Territory_ID__c != null) newTerritoryFound = territoryIdMap.get(atc.New_Territory_ID__c.substring(0,15));				
				else newTerritoryFound = territoryNameMap.get(atc.New_Territory_Name__c);
											 
				if (newTerritoryFound == null){					
					atc.addError('New territory not found, please check spelling or id');
					continue;
				}
					
				if (newTerritoryFound.Territory2Type.DeveloperName != 'Territory'){					
					
					atc.addError('New territory is not a lowest level Territory Type');
					continue;										
				}
						
				atc.New_Territory_UID__c = newTerritoryFound.Territory_UID2__c;
				atc.New_Territory_ID__c = newTerritoryFound.id;
				atc.Therapy_Groups__c = newTerritoryFound.Therapy_Groups_Text__c;
				atc.New_Territory_Name__c = newTerritoryFound.Name;
						
				//Check that account country is part of Company						 
				Account acc = accountsMap.get(atc.Account__c);							
				DIB_Country__c country = countryMap.get(acc.Account_Country_vs__c);
							
				if(country != null && country.company__c != atc.Company__c){									
					atc.addError('Account is not part of the company of the target territory ');
					continue;
				}
				
				//If we arrive here it means that the first validation step is successful for this record
				validationPass.add(atc);
				validatedAccountIds.add(atc.Account__c);
			}		
		}
		
		//Method that populates the map with Account / Territory information
		getTerritoryForAccounts(validatedAccountIds);
						
		//Second validation step. Check existing Territories and Therapy Group combinations
		for (Account_Territory_Change__c atc : validationPass){
			
			//Only for assignments with Therapy Groups assigned. Exclude MITG assignments from the validation
            if (atc.Therapy_Groups__c != null && mitgBUs.contains(atc.Business_Unit__c) == false){
				
				Map<String, AccountTerritoryShare> accountTerritories = accountTerritoryShares.get(atc.Account__c);
				
				String key = buildTerritoryKey(atc.Business_Unit_Name__c, atc.Company__c, atc.Therapy_Groups__c);
				
				AccountTerritoryShare existingTerritory = accountTerritories.get(key);
												
				Territory2 oldTerr;
				
				if (existingTerritory!=null){					
					
					Territory2 exstTer = existingTerritory.Territory;
					atc.Old_Territory_UID__c=exstTer.Territory_UID2__c;
					atc.Old_Territory_Name__c=exstTer.name;
					atc.Old_Territory_ID__c = exstTer.id;
					
				}else{
					atc.Old_Territory_Name__c='None';
				}
								
				Set<String> therapyGroupsAlreadyCovered = new Set<String>();
								
				for (AccountTerritoryShare terrShare : accountTerritories.values()){
													
					Territory2 terr = terrShare.Territory;
															
					if (oldTerr!=null && oldTerr.id==terr.id){
						//do nothing as this territory will be replaced;
					}else{

						if(terr.Therapy_Groups_Text__c != null) therapyGroupsAlreadyCovered.addAll(terr.Therapy_Groups_Text__c.split(';'));
					}					
				}
				
				//If any of the new Therapy Groups is already covered by other Territories we add an error
				List<String> newTerrThGroups = atc.Therapy_Groups__c.split(';');
				Boolean error = false;
				for (String thGroup : newTerrThGroups){
					
					if (therapyGroupsAlreadyCovered.contains(thGroup)){
						atc.addError('There is already an existing assignment for this account and therapy group ' + thGroup + '( AccountId : ' + atc.Account__c + ' )');
						error = true;
						break;
					}
				}
				
				//If validation was passed we add thew new Territory to the map to avoid duplicates among the new records
				if(error == false){
					
					Territory2 newTerritory = new Territory2(Id = atc.New_Territory_ID__c, Therapy_Groups_Text__c = atc.Therapy_Groups__c);
					AccountTerritoryShare accTerrShare = new AccountTerritoryShare();
					accTerrShare.Territory = newTerritory;
					
					//Add to the Map with a key that will not return this territory when looking for old territories later on
					accountTerritories.put(atc.Account__c + ':' + atc.New_Territory_ID__c, accTerrShare);
				}					
			}
		}
		
		System.debug('Updated terr changes '+accountTerritoryChanges);		
	}
	
	public static void migrateAccountsToNewTerritory(List<Account_Territory_Change__c> accountTerritoryChanges){
		
		Map<Id, ObjectTerritory2Association> accountSharesToDelete = new Map<Id, ObjectTerritory2Association>();//Use a Map to avoid possible duplicate AccShare in the list
		List<ObjectTerritory2Association> accountSharesToCreate = new List<ObjectTerritory2Association>();
		
		Set<Id> territoryIds = new Set<Id>();
		
		for(Account_Territory_Change__c atc : accountTerritoryChanges){
			territoryIds.add(atc.New_Territory_ID__c);	
		}
						
		for (Account_Territory_Change__c atc : accountTerritoryChanges){
		
			if (atc.Account__c!=null && atc.Business_Unit__c!=null && atc.New_Territory_UID__c!=null && atc.Company__c!=null && atc.Therapy_Groups__c != null){
				
				if(mitgBUs.contains(atc.Business_Unit__c) == false){
				
					Map<String, AccountTerritoryShare> accountTerritories = accountTerritoryShares.get(atc.Account__c);
					
					String key = buildTerritoryKey(atc.Business_Unit_Name__c, atc.Company__c, atc.Therapy_Groups__c);
					
					AccountTerritoryShare existingTerritory = accountTerritories.get(key);
					
					System.debug('AccountshareTerritory '+existingTerritory);
									
					//Always remove existing link (for all operations)
					//Delete existing link
					if (existingTerritory!=null){
										
						ObjectTerritory2Association accountShare = existingTerritory.accShare;
						if (accountShare.AssociationCause.equals('Territory2Manual')){
							accountSharesToDelete.put(accountShare.Id, accountShare);
						}					
					}
				}

				//Create new links only when update				
				if(atc.Operation__c==null || atc.Operation__c.equals('UPDATE') || atc.Operation__c.equals('CREATE')){
					
					ObjectTerritory2Association newAccountShare = new ObjectTerritory2Association();
					newAccountShare.objectId = atc.Account__c;					
					newAccountShare.Territory2Id = atc.New_Territory_ID__c;
					newAccountShare.AssociationCause ='Territory2Manual';
					accountSharesToCreate.add(newAccountShare);				
				}	
			}
		}
		
		System.debug('AccountShares to delete '+accountSharesToDelete);
		if(accountSharesToDelete.size() > 0) delete accountSharesToDelete.values();
		System.debug('AccountShares to create '+accountSharesToCreate);
		if(accountSharesToCreate.size() > 0) insert accountSharesToCreate;		
	}
	
	public static void deleteAccountsFromTerritory(List<Account_Territory_Change__c> accountTerritoryChanges){
		
		Set<Id> territoryIds = new Set<Id>();
		Set<Id> accountIds = new Set<Id>();
		for (Account_Territory_Change__c atc : accountTerritoryChanges){
			territoryIds.add(atc.Old_Territory_ID__c);
			accountIds.add(atc.Account__c);
		}
		
		Map<String, ObjectTerritory2Association> accSharesMap = new Map<String, ObjectTerritory2Association>();
		
		for(ObjectTerritory2Association accShare : [Select Id, objectId, Territory2Id from ObjectTerritory2Association where objectId IN :accountIds AND Territory2Id IN :territoryIds AND AssociationCause = 'Territory2Manual']){
			
			accSharesMap.put(accShare.objectId + ':' + accShare.Territory2Id, accShare);	
		}
		
		System.debug('Shares map -->' + accSharesMap);
		
		List<ObjectTerritory2Association> accSharesToDelete = new List<ObjectTerritory2Association>();
		
		for(Account_Territory_Change__c accTerChange : accountTerritoryChanges){
			
			String key = accTerChange.Account__c + ':' + accTerChange.Old_Territory_ID__c;
			
			ObjectTerritory2Association accShare = accSharesMap.get(key);
			
			if(accShare != null) accSharesToDelete.add(accShare);			
		}
		
		if(accSharesToDelete.size() > 0) delete accSharesToDelete;
	}


	public static Map<Id, Set<Id>> loadTerritory_Account(Set<Id> setID_Account){
		
		Map<Id, Set<Id>> mapAccountID_TerritoryID = new Map<Id, Set<Id>>();

		// Load the Territories of the Accounts				
		for (ObjectTerritory2Association oAccountShare : [SELECT ObjectId, Territory2Id FROM ObjectTerritory2Association WHERE ObjectId = :setID_Account]){
			
			Set<Id> setID_Territory = mapAccountID_TerritoryID.get(oAccountShare.ObjectId);
			
			if (setID_Territory == null){
				
				setID_Territory = new Set<Id>();
				mapAccountID_TerritoryID.put(oAccountShare.ObjectId, setID_Territory);
			}
			
			setID_Territory.add(oAccountShare.Territory2Id);
			
		}
		
		return mapAccountID_TerritoryID;	
	}

	public static Map<Id, Set<Id>> loadTerritory_User(Set<Id> setID_User){
		
		Map<Id, Set<Id>> mapUserId_TerritoryID = new Map<Id, Set<Id>>();

		// Load the Territories of the Users
		for (UserTerritory2Association oUserTerritory : [SELECT Territory2Id, UserId FROM UserTerritory2Association WHERE UserId = :setID_User AND isActive = True]){
			Set<Id> setID_Territory = new Set<Id>();
			if (mapUserId_TerritoryID.containsKey(oUserTerritory.UserId)){
				setID_Territory = mapUserId_TerritoryID.get(oUserTerritory.UserId);
			}
			setID_Territory.add(oUserTerritory.Territory2Id);
			mapUserId_TerritoryID.put(oUserTerritory.UserId, setID_Territory);
		}

		return mapUserId_TerritoryID;

	}
	
	public class AccountTerritoryShare{
		
		public Territory2 territory;
		public ObjectTerritory2Association accShare;
	}
}