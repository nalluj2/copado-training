//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 03-05-2019
//  Description      : APEX Class - Business Logic for tr_Goal
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public class bl_Goal_Trigger{

	//----------------------------------------------------------------------------------------------------------------
	// Pupulate the picklist Business Unit Group on Goal with the Business Unit Group Name of the selected Sub Business Unit
	//----------------------------------------------------------------------------------------------------------------
	public static void populateBusinessUnitGroup(List<Goal__c> lstTriggerNew, Map<Id, Goal__c> mapTriggerOld){

        if (bl_Trigger_Deactivation.isTriggerDeactivated('goal_populateBusinessUnitGroup')) return;

		Set<Id> setID_SBU = new Set<Id>();
		for (Goal__c oGoal : lstTriggerNew) setID_SBU.add(oGoal.Sub_Business_Unit__c);

		Map<Id, Sub_Business_Units__c> mapSBU = new Map<Id, Sub_Business_Units__c>([SELECT Id, Business_Unit__r.Business_Unit_Group__r.Name FROM Sub_Business_Units__c WHERE Id = :setID_SBU]);

		for (Goal__c oGoal : lstTriggerNew){

			Sub_Business_Units__c oSBU = mapSBU.get(oGoal.Sub_Business_Unit__c);

			if (mapTriggerOld != null){

				if (oGoal.Business_Unit_Group__c != oSBU.Business_Unit__r.Business_Unit_Group__r.Name) oGoal.Business_Unit_Group__c = oSBU.Business_Unit__r.Business_Unit_Group__r.Name;

			}else{

				oGoal.Business_Unit_Group__c = oSBU.Business_Unit__r.Business_Unit_Group__r.Name;

			}
		
		}
	}
	//----------------------------------------------------------------------------------------------------------------
	
}
//--------------------------------------------------------------------------------------------------------------------