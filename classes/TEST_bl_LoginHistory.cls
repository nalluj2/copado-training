//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 09-01-2018
//  Description      : APEX Test Class for bl_LoginHistory
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_bl_LoginHistory {
	
    @isTest static void test_GetSource_Application() {

        Test.startTest();

        ba_UserLoginHistory oBatch = new ba_UserLoginHistory();

        wr_LoginHistory oLoginHistory = new wr_LoginHistory('Success', 'Application', 'Browser');
        String tSource = bl_LoginHistory.tGetSource(oLoginHistory);
        System.assert(!String.isBlank(tSource));

        Test.stopTest();

    }


    @isTest static void test_GetSource_PartnerPortal() {

        Test.startTest();

        ba_UserLoginHistory oBatch = new ba_UserLoginHistory();

        wr_LoginHistory oLoginHistory = new wr_LoginHistory('Success', 'Partner Portal', 'Browser');
        String tSource = bl_LoginHistory.tGetSource(oLoginHistory);
        System.assert(!String.isBlank(tSource));

        Test.stopTest();

    }


    @isTest static void test_GetSource_SSO() {

        Test.startTest();

        ba_UserLoginHistory oBatch = new ba_UserLoginHistory();

        wr_LoginHistory oLoginHistory = new wr_LoginHistory('Success', 'SAML Sfdc Initiated SSO', 'Browser');
        String tSource = bl_LoginHistory.tGetSource(oLoginHistory);
        System.assert(!String.isBlank(tSource));

        Test.stopTest();

    }    


    @isTest static void test_GetSource_ChatterCommunitiesExternalUser() {

        Test.startTest();

        ba_UserLoginHistory oBatch = new ba_UserLoginHistory();

        wr_LoginHistory oLoginHistory = new wr_LoginHistory('Success', 'Chatter Communities External User', 'Browser');
		String tSource = bl_LoginHistory.tGetSource(oLoginHistory);
        System.assert(!String.isBlank(tSource));

        Test.stopTest();

    }    


    @isTest static void test_GetSource_RemoteAccess() {

        Test.startTest();

        ba_UserLoginHistory oBatch = new ba_UserLoginHistory();

        wr_LoginHistory oLoginHistory = new wr_LoginHistory('Success', 'Remote Access 2.0', 'Browser');
        String tSource = bl_LoginHistory.tGetSource(oLoginHistory);
        System.assert(!String.isBlank(tSource));

        oLoginHistory = new wr_LoginHistory('Success', 'Remote Access 2.0', 'Salesforce Partner Community');
        tSource = bl_LoginHistory.tGetSource(oLoginHistory);
        System.assert(!String.isBlank(tSource));

        oLoginHistory = new wr_LoginHistory('Success', 'Remote Access 2.0', 'Salesforce for Android');
        tSource = bl_LoginHistory.tGetSource(oLoginHistory);
        System.assert(!String.isBlank(tSource));

        oLoginHistory = new wr_LoginHistory('Success', 'Remote Access 2.0', 'Salesforce for iOS');
        tSource = bl_LoginHistory.tGetSource(oLoginHistory);
        System.assert(!String.isBlank(tSource));

        oLoginHistory = new wr_LoginHistory('Success', 'Remote Access 2.0', 'Salesforce1');
        tSource = bl_LoginHistory.tGetSource(oLoginHistory);
        System.assert(!String.isBlank(tSource));

        oLoginHistory = new wr_LoginHistory('Success', 'Remote Access 2.0', 'ServiceMax');
        tSource = bl_LoginHistory.tGetSource(oLoginHistory);
        System.assert(!String.isBlank(tSource));

        oLoginHistory = new wr_LoginHistory('Success', 'Remote Access 2.0', 'ServiceMax Mobile');
        tSource = bl_LoginHistory.tGetSource(oLoginHistory);
        System.assert(!String.isBlank(tSource));

        oLoginHistory = new wr_LoginHistory('Success', 'Remote Access 2.0', 'Salesforce Marketing Cloud');
        tSource = bl_LoginHistory.tGetSource(oLoginHistory);
        System.assert(!String.isBlank(tSource));

        Test.stopTest();

    }    
   	

}
//--------------------------------------------------------------------------------------------------------------------