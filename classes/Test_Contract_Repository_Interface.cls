@isTest
global class Test_Contract_Repository_Interface {
    
    private static testmethod void testInterfaceCreateFile(){
        
        Contract_Repository_Interface_Settings__c settings = new Contract_Repository_Interface_Settings__c();
        settings.Documentum_Key__c = 'abcdefghijklmnop';
        settings.Documentum_Server_URL__c = 'https://medtronic.com';
        settings.End_Point_URL_Server__c = 'https://medtronic.com';
        settings.Org_Id__c = UserInfo.getOrganizationId().left(15);
        settings.Password__c = 'testuser';
        settings.Username__c = 'testpassword';      
        insert settings;
        
        Account testAcc = new Account();
        testAcc.Name = 'Test Account';
        testAcc.Account_Country_vs__c = 'BELGIUM';
        insert testAcc;
        
        Contract_Repository__c contract = new Contract_Repository__c();
        contract.Account__c = testAcc.Id;
        contract.Primary_Contract_Type__c = 'Rebate agreement';
        contract.Rebate_Type__c = 'Straight Unit';
        //contract.Payment_Options__c = 'Credit Note';
        contract.put('Payment_Options__c','Credit Note');
        contract.Pre_Payment_Terms__c = 'Final Payment';
        contract.Payment_Period__c = 'Monthly';     
        contract.Original_Valid_From_Date__c = Date.today();
        contract.Original_Valid_To_Date__c = Date.today().addYears(1);
        insert contract;
                
        Contract_Repository_Document__c doc = new Contract_Repository_Document__c();        
        doc.Contract_Repository__c = contract.Id;
        doc.Name = 'Document';
        doc.Document_Type__c = '2. Contract';               
        insert doc;
        
        Contract_Repository_Document_Version__c docVersion1 = new Contract_Repository_Document_Version__c();
        docVersion1.Document__c = doc.Id;
        docVersion1.File_Name__c = 'File_name v1';
        docVersion1.Version__c = 1;
        docVersion1.File_Type__c = 'csv';
        docVersion1.Size__c = 1024;
        insert docVersion1;
                
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CreateDocumentMock());
        
        bl_Attachment_Trigger.unitTestRunInterface = true;
        
        Attachment docFile = new Attachment();
        docFile.ParentId = docVersion1.Id;
        docFile.Name = 'test.pdf';
        docFile.Body = Blob.valueOf('TEST BODY');
        insert docFile;
        
        Test.stopTest();
        
        ws_Contract_Repository_Interface.UploadDocumentResponse ackInput = new ws_Contract_Repository_Interface.UploadDocumentResponse();
        ackInput.attachmentId = docFile.Id;
        ackInput.documentumId = 'TestId';
        ackInput.isSuccess = true;
        
        ws_Contract_Repository_Interface.documentUploadConfirmation(ackInput);
        
        List<Attachment> att = [Select Id from Attachment where Id = :docFile.Id];
        System.assert(att.size() == 0);
        
        docVersion1 = [Select Id, Documentum_Id__c from Contract_Repository_Document_Version__c where Id = :docVersion1.Id];
        System.assert(docVersion1.Documentum_Id__c == 'TestId');
    }
    
    private static testmethod void testInterfaceCreateFile_Batch(){
        
        Contract_Repository_Interface_Settings__c settings = new Contract_Repository_Interface_Settings__c();
        settings.Documentum_Key__c = 'abcdefghijklmnop';
        settings.Documentum_Server_URL__c = 'https://medtronic.com';
        settings.End_Point_URL_Server__c = 'https://medtronic.com';
        settings.Org_Id__c = UserInfo.getOrganizationId().left(15);
        settings.Password__c = 'testuser';
        settings.Username__c = 'testpassword';      
        insert settings;
        
        Account testAcc = new Account();
        testAcc.Name = 'Test Account';
        testAcc.Account_Country_vs__c = 'BELGIUM';
        insert testAcc;
        
        Contract_Repository__c contract = new Contract_Repository__c();
        contract.Account__c = testAcc.Id;
        contract.Primary_Contract_Type__c = 'Rebate agreement';
        contract.Rebate_Type__c = 'Straight Unit';
        //contract.Payment_Options__c = 'Credit Note';
        contract.put('Payment_Options__c','Credit Note');
        contract.Pre_Payment_Terms__c = 'Final Payment';
        contract.Payment_Period__c = 'Monthly';     
        contract.Original_Valid_From_Date__c = Date.today();
        contract.Original_Valid_To_Date__c = Date.today().addYears(1);
        insert contract;
                
        Contract_Repository_Document__c doc1 = new Contract_Repository_Document__c();       
        doc1.Contract_Repository__c = contract.Id;
        doc1.Name = 'Documen 1t';
        doc1.Document_Type__c = '2. Contract';              
        insert doc1;
        
        Contract_Repository_Document_Version__c doc1Version1 = new Contract_Repository_Document_Version__c();
        doc1Version1.Document__c = doc1.Id;
        doc1Version1.File_Name__c = 'File_name v1';
        doc1Version1.Version__c = 1;
        doc1Version1.File_Type__c = 'csv';
        doc1Version1.Size__c = 1024;        
        insert doc1Version1;
        
        Attachment doc1File = new Attachment();
        doc1File.ParentId = doc1Version1.Id;
        doc1File.Name = 'test.pdf';
        doc1File.Body = Blob.valueOf('TEST BODY');                
        insert doc1File;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CreateDocumentMock());
        
        ba_Contract_Repository_Inteface batch = new ba_Contract_Repository_Inteface();
        batch.execute(null);
        
        Test.stopTest();        
    }
    
    private static testmethod void testInterfaceCreateFile_Error(){
        
        Contract_Repository_Interface_Settings__c settings = new Contract_Repository_Interface_Settings__c();
        settings.Documentum_Key__c = 'abcdefghijklmnop';
        settings.Documentum_Server_URL__c = 'https://medtronic.com';
        settings.End_Point_URL_Server__c = 'https://medtronic.com';
        settings.Org_Id__c = UserInfo.getOrganizationId().left(15);
        settings.Password__c = 'testuser';
        settings.Username__c = 'testpassword';      
        insert settings;
        
        Account testAcc = new Account();
        testAcc.Name = 'Test Account';
        testAcc.Account_Country_vs__c = 'BELGIUM';
        insert testAcc;
        
        Contract_Repository__c contract = new Contract_Repository__c();
        contract.Account__c = testAcc.Id;
        contract.Primary_Contract_Type__c = 'Rebate agreement';
        contract.Rebate_Type__c = 'Straight Unit';
        //contract.Payment_Options__c = 'Credit Note';
        contract.put('Payment_Options__c','Credit Note');
        contract.Pre_Payment_Terms__c = 'Final Payment';
        contract.Payment_Period__c = 'Monthly';     
        contract.Original_Valid_From_Date__c = Date.today();
        contract.Original_Valid_To_Date__c = Date.today().addYears(1);
        insert contract;
                
        Contract_Repository_Document__c doc = new Contract_Repository_Document__c();        
        doc.Contract_Repository__c = contract.Id;
        doc.Name = 'Document';
        doc.Document_Type__c = '2. Contract';               
        insert doc;
        
        Contract_Repository_Document_Version__c docVersion1 = new Contract_Repository_Document_Version__c();
        docVersion1.Document__c = doc.Id;
        docVersion1.File_Name__c = 'File_name v1';
        docVersion1.Version__c = 1;
        docVersion1.File_Type__c = 'csv';
        docVersion1.Size__c = 1024;
        insert docVersion1;
                
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CreateDocumentMock());
        
        bl_Attachment_Trigger.unitTestRunInterface = true;
        
        Attachment docFile = new Attachment();
        docFile.ParentId = docVersion1.Id;
        docFile.Name = 'test.pdf';
        docFile.Body = Blob.valueOf('TEST BODY');
        insert docFile;
        
        Test.stopTest();
        
        ws_Contract_Repository_Interface.UploadDocumentResponse ackInput = new ws_Contract_Repository_Interface.UploadDocumentResponse();
        ackInput.attachmentId = docFile.Id;
        ackInput.errorMessage = 'Test Error';
        ackInput.isSuccess = false;
        
        ws_Contract_Repository_Interface.documentUploadConfirmation(ackInput);
        
        List<Attachment> att = [Select Id from Attachment where Id = :docFile.Id];
        System.assert(att.size() == 1);
        
        docVersion1 = [Select Id, Documentum_Id__c, Interface_Error__c from Contract_Repository_Document_Version__c where Id = :docVersion1.Id];
        System.assert(docVersion1.Documentum_Id__c == null);
        System.assert(docVersion1.Interface_Error__c == 'Test Error');
    }
    
    private static testmethod void testInterfaceDeleteVersion(){
        
        Contract_Repository_Interface_Settings__c settings = new Contract_Repository_Interface_Settings__c();
        settings.Documentum_Key__c = 'abcdefghijklmnop';
        settings.Documentum_Server_URL__c = 'https://medtronic.com';
        settings.End_Point_URL_Server__c = 'https://medtronic.com';
        settings.Org_Id__c = UserInfo.getOrganizationId().left(15);
        settings.Password__c = 'testuser';
        settings.Username__c = 'testpassword';      
        insert settings;
        
        Account testAcc = new Account();
        testAcc.Name = 'Test Account';
        testAcc.Account_Country_vs__c = 'BELGIUM';
        insert testAcc;
        
        Contract_Repository__c contract = new Contract_Repository__c();
        contract.Account__c = testAcc.Id;
        contract.Primary_Contract_Type__c = 'Rebate agreement';
        contract.Rebate_Type__c = 'Straight Unit';
        //contract.Payment_Options__c = 'Credit Note';
        contract.put('Payment_Options__c','Credit Note');
        contract.Pre_Payment_Terms__c = 'Final Payment';
        contract.Payment_Period__c = 'Monthly';     
        contract.Original_Valid_From_Date__c = Date.today();
        contract.Original_Valid_To_Date__c = Date.today().addYears(1);
        insert contract;
                
        Contract_Repository_Document__c doc = new Contract_Repository_Document__c();        
        doc.Contract_Repository__c = contract.Id;
        doc.Name = 'Document';
        doc.Document_Type__c = '2. Contract';               
        insert doc;
        
        Contract_Repository_Document_Version__c docVersion1 = new Contract_Repository_Document_Version__c();
        docVersion1.Document__c = doc.Id;
        docVersion1.File_Name__c = 'File_name v1';
        docVersion1.Version__c = 1;
        docVersion1.File_Type__c = 'csv';
        docVersion1.Size__c = 1024;
        docVersion1.Documentum_Id__c = 'docId';
        insert docVersion1;
                
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new DeleteVersionMock(false));
        
        bl_Contract_Repository_Document_Version.unitTestRunInterface = true;
        
        docVersion1.Marked_for_Deletion__c = true;
        update docVersion1;
        
        Test.stopTest();
        
        docVersion1 = [Select Id, Is_Deleted__c from Contract_Repository_Document_Version__c where Id = :docVersion1.Id];
        System.assert(docVersion1.Is_Deleted__c == true);
    }
    
    private static testmethod void testInterfaceDeleteVersion_Batch(){
        
        Contract_Repository_Interface_Settings__c settings = new Contract_Repository_Interface_Settings__c();
        settings.Documentum_Key__c = 'abcdefghijklmnop';
        settings.Documentum_Server_URL__c = 'https://medtronic.com';
        settings.End_Point_URL_Server__c = 'https://medtronic.com';
        settings.Org_Id__c = UserInfo.getOrganizationId().left(15);
        settings.Password__c = 'testuser';
        settings.Username__c = 'testpassword';      
        insert settings;
        
        Account testAcc = new Account();
        testAcc.Name = 'Test Account';
        testAcc.Account_Country_vs__c = 'BELGIUM';
        insert testAcc;
        
        Contract_Repository__c contract = new Contract_Repository__c();
        contract.Account__c = testAcc.Id;
        contract.Primary_Contract_Type__c = 'Rebate agreement';
        contract.Rebate_Type__c = 'Straight Unit';
        //contract.Payment_Options__c = 'Credit Note';
        contract.put('Payment_Options__c','Credit Note');
        contract.Pre_Payment_Terms__c = 'Final Payment';
        contract.Payment_Period__c = 'Monthly';     
        contract.Original_Valid_From_Date__c = Date.today();
        contract.Original_Valid_To_Date__c = Date.today().addYears(1);
        insert contract;
                
        Contract_Repository_Document__c doc = new Contract_Repository_Document__c();        
        doc.Contract_Repository__c = contract.Id;
        doc.Name = 'Document';
        doc.Document_Type__c = '2. Contract';               
        insert doc;
        
        Contract_Repository_Document_Version__c docVersion1 = new Contract_Repository_Document_Version__c();
        docVersion1.Document__c = doc.Id;
        docVersion1.File_Name__c = 'File_name v1';
        docVersion1.Version__c = 1;
        docVersion1.File_Type__c = 'csv';
        docVersion1.Size__c = 1024;
        docVersion1.Documentum_Id__c = 'docId';
        docVersion1.Marked_for_Deletion__c = true;
        insert docVersion1;
                
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new DeleteVersionMock(false));
        
        ba_Contract_Repository_Document_Version batch = new ba_Contract_Repository_Document_Version();
        batch.execute(null);
        
        Test.stopTest();
        
        docVersion1 = [Select Id, Is_Deleted__c from Contract_Repository_Document_Version__c where Id = :docVersion1.Id];
        System.assert(docVersion1.Is_Deleted__c == true);
    }
    
    private static testmethod void testInterfaceDeleteVersion_Error(){
        
        Contract_Repository_Interface_Settings__c settings = new Contract_Repository_Interface_Settings__c();
        settings.Documentum_Key__c = 'abcdefghijklmnop';
        settings.Documentum_Server_URL__c = 'https://medtronic.com';
        settings.End_Point_URL_Server__c = 'https://medtronic.com';
        settings.Org_Id__c = UserInfo.getOrganizationId().left(15);
        settings.Password__c = 'testuser';
        settings.Username__c = 'testpassword';      
        insert settings;
        
        Account testAcc = new Account();
        testAcc.Name = 'Test Account';
        testAcc.Account_Country_vs__c = 'BELGIUM';
        insert testAcc;
        
        Contract_Repository__c contract = new Contract_Repository__c();
        contract.Account__c = testAcc.Id;
        contract.Primary_Contract_Type__c = 'Rebate agreement';
        contract.Rebate_Type__c = 'Straight Unit';
        //contract.Payment_Options__c = 'Credit Note';
        contract.put('Payment_Options__c','Credit Note');
        contract.Pre_Payment_Terms__c = 'Final Payment';
        contract.Payment_Period__c = 'Monthly';     
        contract.Original_Valid_From_Date__c = Date.today();
        contract.Original_Valid_To_Date__c = Date.today().addYears(1);
        insert contract;
                
        Contract_Repository_Document__c doc = new Contract_Repository_Document__c();        
        doc.Contract_Repository__c = contract.Id;
        doc.Name = 'Document';
        doc.Document_Type__c = '2. Contract';               
        insert doc;
        
        Contract_Repository_Document_Version__c docVersion1 = new Contract_Repository_Document_Version__c();
        docVersion1.Document__c = doc.Id;
        docVersion1.File_Name__c = 'File_name v1';
        docVersion1.Version__c = 1;
        docVersion1.File_Type__c = 'csv';
        docVersion1.Size__c = 1024;
        docVersion1.Documentum_Id__c = 'docId';
        insert docVersion1;
                
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new DeleteVersionMock(true));
        
        bl_Contract_Repository_Document_Version.unitTestRunInterface = true;
        
        docVersion1.Marked_for_Deletion__c = true;
        update docVersion1;
        
        Test.stopTest();
        
        docVersion1 = [Select Id, Is_Deleted__c, Interface_Error__c from Contract_Repository_Document_Version__c where Id = :docVersion1.Id];
        System.assert(docVersion1.Is_Deleted__c == false);
        System.assert(docVersion1.Interface_Error__c == 'Error Message');
    }
    
    
    global class CreateDocumentMock implements HttpCalloutMock {
        
        global HTTPResponse respond(HTTPRequest req) {
            
            HTTPResponse resp = new HTTPResponse();
            resp.setStatusCode(200);
            
            return resp;
        }
    }
    
    global class DeleteVersionMock implements HttpCalloutMock {
        
        private Boolean triggerError = false;
        
        
        public DeleteVersionMock(Boolean withError){
            
            this.triggerError = withError;  
        }       
        
        global HTTPResponse respond(HTTPRequest req) {
            
            HTTPResponse resp = new HTTPResponse();
            resp.setStatusCode(200);
            
            String responseBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">';
            responseBody += '<soapenv:Body>';
            responseBody += '<ser-root:deleteContractsResponse xmlns:ser-root="http://msplwa115-pr.wby1-dev.medtronic.com/MdtSalesforce_Contracts.pub.providers.ws:deleteContracts_WSDL" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
            responseBody += '<deleteContractsOutput>';
            responseBody += '<documentumId>documentumId</documentumId>';
            
            if(triggerError == false){
                
                responseBody += '<isSuccess>True</isSuccess>';
                responseBody += '<msg>successfully invoked documentum API</msg>';
                
            }else{
                
                responseBody += '<isSuccess>False</isSuccess>';
                responseBody += '<msg>Error Message</msg>';
            }
            
            responseBody += '</deleteContractsOutput>';
            responseBody += '</ser-root:deleteContractsResponse>';
            responseBody += '</soapenv:Body>';
            responseBody += '</soapenv:Envelope>';
            
            resp.setBody(responseBody);
            
            return resp;
        }
    }
}