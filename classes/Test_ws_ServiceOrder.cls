@IsTest(seeAllData=true)
public with sharing class Test_ws_ServiceOrder {

	public static testmethod void testServiceOrder(){
		
		RestRequest req = new RestRequest();  
    	RestResponse res = new RestResponse();
    	
		String id=GuidUtil.NewGuid();


        List<Therapy__c> lstTherapy = [SELECT Id, Business_Unit__c FROM Therapy__c WHERE Active__c = true AND Company_Text__c = 'Europe' AND Business_Unit__c != null LIMIT 1];

        //-BC - 20150901 - Added logic to create Account and Contact test data that are relared to each other - START
    	//Create test data
        clsTestData.iRecord_Account = 1;
        clsTestData.createAccountData(false);
        clsTestData.oMain_Account.Account_Country_vs__c = 'GERMANY';
        insert clsTestData.lstAccount;
        Account aAccount = clsTestData.lstAccount[0];

        clsTestData.iRecord_Contact = 1;
        clsTestData.createContactData();
        Contact aContact = clsTestData.oMain_Contact;

//    	List<Account> accounts = [select Id from account where Account_Country_vs__c = 'GERMANY' limit 1];
//    	Account aAccount = accounts[0];
    	
//    	List<Contact> contacts = [select Id from contact limit 1];
//    	Contact aContact = contacts[0];
        //-BC - 20150901 - Added logic to create Account and Contact test data that are relared to each other - STOP

    	List<User> users = [select Id from user where country = 'GERMANY' and isActive= true and userType = 'Standard' limit 1];
    	User aUser = users[0];
    	
    	List<Product2> ps = [select Id from product2 where RecordTypeID = :RecordTypeMedtronic.Product('SAP_Product').id limit 1];
    	Product2 aProduct = ps[0];
    	
    	List<ws_ServiceOrderService.ServiceOrderProduct> serviceOrderProducts = new List<ws_ServiceOrderService.ServiceOrderProduct>();
    	ws_ServiceOrderService.ServiceOrderProduct p = new ws_ServiceOrderService.ServiceOrderProduct();
    	p.productId = aProduct.id;
    	
    	serviceOrderProducts.add(p);
    	
    	Call_Activity_Type__c cat = ([select Id from Call_Activity_Type__c where Company_ID__r.name = 'Europe' and name='Implant Support'])[0];
    	String serviceType =cat.Id;
    	
    	DateTime startDateTime = DateTime.now();
    	Integer duration = 2;
    	Integer travelTime = 2;
    	String notes ='Notes';
    	String poNumber = 'po123';
    	String patientHospitalId = 'cccc 1111';
    	String supportType = 'In Person'; 
    	
    	JSONGenerator gen = JSON.createGenerator(true);
    	
		gen.writeStartObject();
		
        gen.writeStringField('serviceOrderId', id);
	    gen.writeObjectField('accountId',aAccount.id);
	    gen.writeObjectField('contactId',aContact.id);
	    gen.writeObjectField('serviceType',serviceType);
	    gen.writeObjectField('supportType',supportType);
	    gen.writeObjectField('startDateTime',startDateTime);
	    gen.writeObjectField('duration',duration);
	    gen.writeObjectField('travelTime', travelTime);
	    gen.writeObjectField('waitingTime', travelTime);
	    gen.writeObjectField('noShow', false);
	    gen.writeObjectField('notes', notes);
	    gen.writeObjectField('patientHospitalId', patientHospitalId);
	    gen.writeObjectField('poNumber', poNumber);
	    gen.writeObjectField('userId', aUser.id);
	    gen.writeObjectField('serviceOrderProducts',serviceOrderProducts);
        gen.writeObjectField('therapyId', lstTherapy[0].Id);
	    
	   
        // Get the JSON string.
        String pretty = gen.getAsString();	    	
		System.debug('test '+pretty);

        req.requestbody = Blob.valueOf(pretty);
    	
     	req.requestURI = '/services/apexrest/ServiceOrderService';  
	    req.httpMethod = 'POST';
    	RestContext.request = req;
    	RestContext.response = res;
		

		ws_ServiceOrderService.ServiceOrderResponse response = ws_ServiceOrderService.upsertServiceOrder();
		System.debug('test '+response);
		
        List<Call_Records__c> lstCallRecord = [SELECT Id, Business_Unit__c FROM Call_Records__c WHERE Mobile_Id__c = :id];
        System.assertEquals(lstCallRecord.size(), 1);
        System.assertEquals(lstCallRecord[0].Business_Unit__c, lstTherapy[0].Business_Unit__c);
        
        List<WorkOrder__c> lstWorkorder = [SELECT Id, RecordType.DeveloperName, Service_Order_Id__c, Support_Type__c FROM WorkOrder__c WHERE Call_Record__c = :lstCallRecord[0].id];
        System.assertEquals(lstWorkorder.size(), 1);
        System.assertEquals(lstWorkorder[0].RecordType.DeveloperName, 'EUR_Service_Order');
        System.assertEquals(lstWorkorder[0].Service_Order_Id__c, id);
        System.assertEquals(lstWorkorder[0].Support_Type__c, 'In Person');
        
	}

}