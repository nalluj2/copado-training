/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TEST_controllerDIB_Competitor_iBase_2 {

    static testMethod void myUnitTest() {
    	
    	System.debug('################## BEGIN Test Coverage : TEST_controllerDIB_Competitor_iBase ########################## ') ;
    	//testAsAdmin(); 
		testAsRepUser() ; 
		System.debug('################## END Test Coverage : TEST_controllerDIB_Competitor_iBase ########################## ') ;	
        
    }
    
    
    private static void testAsRepUser(){
    	
    	Id prof1 = [select id from profile where name like '%DiB Sales Entry%' limit 1].Id;
        User u2 = new User(alias = 'repuser', email='usertest@absi.be', emailencodingkey='UTF-8', lastname='Testing', 
        		languagelocalekey='en_US',localesidkey='en_US', profileid = prof1, timezonesidkey='America/Los_Angeles', 
        		username='usertest@absi.be', Alias_unique__c='usertest1234',Company_Code_Text__c='EUR');        
        
        System.runAs(u2){
        	String month = '02' ; 
        	String year = '2020' ; 
        	
        	Account competitor = new Account() ; 
	    	competitor.name = 'ANIMAs' ;
	    	competitor.CGMs__c = true;
	    	competitor.Pumps__c = true;
	    	competitor.RecordTypeId = FinalConstants.ACCOUNT_COMPETITOR_RECORDTYPE_ID;
	    	insert competitor ; 
	    	   
			// Sales Force Acounnts 
			List<Account> sfaAccounts 		= new List<Account>{} ;
			
			// create Sales Force Accounts : 
			for (Integer i = 0; i < 1 ; i++){////20
				Account acc1 = new Account();
				acc1.Name = 'Test Coverage Sales Force Account ' + i ;
				acc1.isSales_Force_Account__c = true ; 
				sfaAccounts.add(acc1) ;	
			}
			
			insert sfaAccounts ; 
			
			// Create Departments 
			List <DIB_Department__c> departments = new List<DIB_Department__c>{} ; 
			
			for (Account a : sfaAccounts){
				DIB_Department__c d = new  DIB_Department__c() ; 
			 	d.Account__c = a.Id ;
			 	departments.add(d); 
			}
			insert departments  ; 
	    	   	    
	    	List<DIB_Competitor_iBase__c> compIBs = new List<DIB_Competitor_iBase__c>{} ; 
	    	for (Integer i = 0 ; i < 1 ; i++){////15
	    		DIB_Competitor_iBase__c comp = new DIB_Competitor_iBase__c() ; 
	    		comp.Current_IB__c = 10 ;   	    		
	    		comp.Competitor_Account_ID__c = competitor.Id ; 
	    		comp.Sales_Force_Account__c = sfaAccounts[i].Id ;    		 
	    		comp.Fiscal_year__c = year ; 
	    		comp.Fiscal_Month__c = month ;
				comp.IB_Change__c = 23 ;    
				for (DIB_Department__c d : departments){
					if (d.Account__c == 	comp.Sales_Force_Account__c ){
						comp.Account_Segmentation__c = d.Id; 
					}
				}
				if (i == 10){
				}
				comp.Type__c = 'Pumps';
				compIBs.add(comp) ; 
	    	}
	    	insert compIBs ; 
	    	   	    
	    	   	    	     	 	
	        //ApexPages.Standardcontroller sct = new ApexPages.Standardcontroller(competitor);
			controllerDIB_Competitor_iBase  controller = new controllerDIB_Competitor_iBase();
			
		    PageReference pPageReference = Page.DIB_Competitor_iBase;
        	Test.setCurrentPage(pPageReference);
			
			controller.Init();				 
			controller.searchCriteria = 'Test Coverage Sales Force';
			controller.setPeriodChosen(month+'-' + year);  
			controller.getPeriodChosen2();
			
			controller.next(); 
			
	
			controller.last();	
			controller.save() ;			
			
			//controller.setAccountFilter('My Accounts') ; 
			controller.next();
			//controller.setSelectedAccountType('Adult/Pediatric'); 
			controller.previous(); 	

			
			/* controller.SubmitcurrentPeriod(); is having a problem with the number of rows retrieved.
				Instead of calling the  SubmitcurrentPeriod() method directly, it will be done by calling the Class DIB_Manage_Task_Status. 
			*/					
			 
			
        }
    	
    }
}