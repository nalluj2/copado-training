@isTest public class clsTestData_Campaign {
	
	public static Integer iRecord_Campaign = 1;
    public static List<Campaign> lstCampaign = new List<Campaign>();
    public static Id idRecordType_Campaign = clsUtil.getRecordTypeByDevName('Campaign', 'Diabetes_Campaign').Id;
    public static Campaign oMain_Campaign;

	public static Integer iRecord_Campaign_CVent = 1;
    public static List<Campaign> lstCampaign_Cvent = new List<Campaign>();
    public static Id idRecordType_Campaign_Cvent = clsUtil.getRecordTypeByDevName('Campaign', 'CVent').Id;
    public static Campaign oMain_Campaign_Cvent;

    public static Integer iRecord_CampaignMember = 1;
    public static List<CampaignMember> lstCampaignMember = new List<CampaignMember>();
    public static Id idRecordType_CampaignMember = clsUtil.getRecordTypeByDevName('CampaignMember', 'Standard_Campaign_Member_Recordtype').Id;
    public static CampaignMember oMain_CampaignMember;

    public static Integer iRecord_TargetAccount = 1;
    public static List<Target_Account__c> lstTargetAccount = new List<Target_Account__c>();
    public static Target_Account__c oMain_TargetAccount;

	public static Integer iRecord_Campaign_LMS = 1;
    public static List<Campaign> lstCampaign_LMS = new List<Campaign>();
    public static Id idRecordType_Campaign_LMS = clsUtil.getRecordTypeByDevName('Campaign', 'LEARNING').Id;
    public static Campaign oMain_Campaign_LMS;

    public static String tBusinessUnitName = '';
    public static Map<Id, String> mapCampaignRecordType_BusinessUnitName = new Map<Id, String>();

	public static List<Campaign> createCampaign(){
		return createCampaign(true);
	}
	public static List<Campaign> createCampaign(Boolean bInsert){

		if (oMain_Campaign == null){

            lstCampaign = new List<Campaign>();

            for (Integer i=0; i<iRecord_Campaign; i++){
	            Campaign oCampaign = new Campaign();
	                oCampaign.Name = 'TEST CAMPAIGN';
	                oCampaign.Type = 'Training & Education';
                    oCampaign.RecordTypeID = idRecordType_Campaign;
                    if (String.isBlank(tBusinessUnitName)){
                    	populateCampaignData();
                    	if (mapCampaignRecordType_BusinessUnitName.containsKey(idRecordType_Campaign)){
                    		oCampaign.Business_Unit_Picklist__c = mapCampaignRecordType_BusinessUnitName.get(idRecordType_Campaign);
                    	}
                    }
	            lstCampaign.add(oCampaign);
	        }

            if (bInsert) insert lstCampaign;
		}

        oMain_Campaign = lstCampaign[0];			

		return lstCampaign;
	}


	public static List<Campaign> createCampaignCvent(){
		return createCampaignCvent(true);
	}
	public static List<Campaign> createCampaignCvent(Boolean bInsert){

		if (oMain_Campaign_Cvent == null){

            lstCampaign_Cvent = new List<Campaign>();

            for (Integer i=0; i<iRecord_Campaign_CVent; i++){
	            Campaign oCampaign = new Campaign();
	                oCampaign.Name = 'TEST CAMPAIGN';
	                oCampaign.Type = 'Training & Education';
                    oCampaign.RecordTypeID = idRecordType_Campaign_Cvent;
                    if (String.isBlank(tBusinessUnitName)){
                    	populateCampaignData();
                    	if (mapCampaignRecordType_BusinessUnitName.containsKey(idRecordType_Campaign_Cvent)){
                    		oCampaign.Business_Unit_Picklist__c = mapCampaignRecordType_BusinessUnitName.get(idRecordType_Campaign_Cvent);
                    	}
                    }
	            lstCampaign_Cvent.add(oCampaign);
	        }

            if (bInsert) insert lstCampaign_Cvent;
		}

        oMain_Campaign_Cvent = lstCampaign_Cvent[0];			

		return lstCampaign_Cvent;

	}
	

	public static List<CampaignMember> createCampaignMember(){
		return createCampaignMember(true);
	}
	public static List<CampaignMember> createCampaignMember(Boolean bInsert){
		return createCampaignMember(oMain_Campaign.Id, bInsert);
	}
	public static List<CampaignMember> createCampaignMember(Id idCampaign, Boolean bInsert){

		if (oMain_CampaignMember == null){

            if (iRecord_CampaignMember != clsTestData_Contact.lstContact.size()){
                clsTestData_Contact.oMain_Contact = null;
                clsTestData_Contact.iRecord_Contact = iRecord_CampaignMember;
                clsTestData_Contact.createContact();
            }

            lstCampaignMember = new List<CampaignMember>();

            for (Integer i=0; i<iRecord_CampaignMember; i++){
                CampaignMember oCampaignMember = new CampaignMember();
                    oCampaignMember.ContactId = clsTestData_Contact.lstContact[i].Id;
                    oCampaignMember.Status = 'Sent';
                    oCampaignMember.CampaignId = idCampaign;
                    oCampaignMember.Campaign_Member_Type__c = 'Participant';
	            lstCampaignMember.add(oCampaignMember);
	        }

            if (bInsert) insert lstCampaignMember;
		}

        oMain_CampaignMember = lstCampaignMember[0];			

		return lstCampaignMember;

	}


	public static List<Target_Account__c> createTargetAccount(){
		return createTargetAccount(true);
	}
	public static List<Target_Account__c> createTargetAccount(Boolean bInsert){

		if (oMain_TargetAccount == null){

            if (oMain_Campaign == null) createCampaign();

            lstTargetAccount = new List<Target_Account__c>();

            for (Integer i=0; i<iRecord_TargetAccount; i++){
                Target_Account__c oTargetAccount = new Target_Account__c();
                    oTargetAccount.Campaign__c = oMain_Campaign.Id;
                    oTargetAccount.Status__c = 'New';
	            lstTargetAccount.add(oTargetAccount);
	        }

            if (bInsert) insert lstTargetAccount;
		}

        oMain_TargetAccount = lstTargetAccount[0];			

		return lstTargetAccount;

	}


	public static List<Campaign> createCampaignLMS(){
		return createCampaignLMS(true);
	}
	public static List<Campaign> createCampaignLMS(Boolean bInsert){

		if (oMain_Campaign_LMS == null){

            lstCampaign_LMS = new List<Campaign>();

            for (Integer i = 0; i < iRecord_Campaign_LMS; i++){
	            Campaign oCampaign = new Campaign();
	                oCampaign.Name = 'TEST CAMPAIGN LMS';
	                oCampaign.Type = 'Training & Education';
                    oCampaign.RecordTypeID = idRecordType_Campaign_LMS;
	            lstCampaign_LMS.add(oCampaign);
	        }

            if (bInsert) insert lstCampaign_LMS;
		}

        oMain_Campaign_LMS = lstCampaign_LMS[0];			

		return lstCampaign_LMS;
	}


	private static void populateCampaignData(){

		if (mapCampaignRecordType_BusinessUnitName.size() == 0){

			mapCampaignRecordType_BusinessUnitName.put(clsUtil.getRecordTypeByDevName('Campaign', 'CVG_Campaign').Id, 'CRHF');
			mapCampaignRecordType_BusinessUnitName.put(clsUtil.getRecordTypeByDevName('Campaign', 'RTG_Campaign').Id, 'Cranial Spinal');
			mapCampaignRecordType_BusinessUnitName.put(clsUtil.getRecordTypeByDevName('Campaign', 'Diabetes_Campaign').Id, 'Diabetes');

		}
	}

}