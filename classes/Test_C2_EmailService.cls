@isTest
private class Test_C2_EmailService {
    
    @testSetup
    static void createTestData(){
    	
    	User seller = new User();
        seller.ProfileId = UserInfo.getProfileId();
        seller.Username = 'mdt1test2@mdt1test.com';
        seller.Country_vs__c = 'NETHERLANDS';
        seller.Country = 'Netherlands';
        seller.Alias = 'utUser';
        seller.Email = 'c2testfrom@covidien.com';
        seller.EmailEncodingKey = 'UTF-8';
        seller.Firstname = 'C2';
        seller.Lastname = 'Test';
        seller.LanguageLocaleKey = 'en_US';
        seller.LocaleSidKey = 'en_US';
        seller.TimeZoneSidKey = 'Europe/Amsterdam';
        seller.Region_vs__c = 'Europe';
	  	seller.User_Business_Unit_vs__c='Cranial Spinal';
	    seller.Alias_unique__c = 'utUser';   	
	    seller.Sub_Region__c = 'NWE';    
        seller.CurrencyISOCode = 'CHF';        
        insert seller;
        
        Account acc = new Account();
        acc.AccountNumber = '0000000';
		acc.Name = 'Syn Test Account';
		acc.Phone = '+123456789';
		acc.BillingCity = 'Minneapolis';
		acc.BillingCountry = 'UNITED STATES';
		acc.BillingState = 'Minnesota';
		acc.BillingStreet = 'street';
		acc.BillingPostalCode = '123456';
		acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non-SAP Account').getRecordTypeId();	
		
		Account nlAcc = new Account();
        nlAcc.AccountNumber = '0000000';
		nlAcc.Name = 'MDT Employee Netherlands';
		nlAcc.Account_Country_vs__c = 'NETHERLANDS';
		nlAcc.Phone = '+123456789';
		nlAcc.BillingCity = 'Heerlen';
		nlAcc.BillingCountry = 'Netherlands';
		nlAcc.BillingState = 'Limburg';
		nlAcc.BillingStreet = 'street';
		nlAcc.BillingPostalCode = '123456';
		nlAcc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('MDT Account').getRecordTypeId();
		
		Account eocAcc = new Account();
        eocAcc.AccountNumber = '0000000';
		eocAcc.Name = 'MDT Employee EOC';
		eocAcc.Account_Country_vs__c = 'NETHERLANDS';
		eocAcc.Phone = '+123456789';
		eocAcc.BillingCity = 'Heerlen';
		eocAcc.BillingCountry = 'Netherlands';
		eocAcc.BillingState = 'Limburg';
		eocAcc.BillingStreet = 'street';
		eocAcc.BillingPostalCode = '123456';
		eocAcc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('MDT Account').getRecordTypeId();
		
		insert new List<Account>{acc, nlAcc, eocAcc};
		
		Contact cnt = new Contact();
		cnt.FirstName = 'Test';
		cnt.LastName = 'Contact';
		cnt.Phone = '+123456789';		
		cnt.Email = 'c2testto@covidien.com';
		cnt.MobilePhone = '+123456789';		
		cnt.MailingCity = 'Minneapolis';
		cnt.MailingCountry = 'UNITED STATES';
		cnt.MailingState = 'Minnesota';
		cnt.MailingStreet = 'street';
		cnt.MailingPostalCode = '123456';		
		cnt.Contact_Department__c = 'Cardiology';
		cnt.Contact_Primary_Specialty__c = 'Cardiovascular'; 
		cnt.Affiliation_To_Account__c = 'Employee'; 
		cnt.Primary_Job_Title_vs__c = 'Surgeon'; 
		cnt.Contact_Gender__c = 'Male'; 
		cnt.AccountId = acc.Id;
		cnt.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Generic Contact').getRecordTypeId();
		
		Contact cnt1 = new Contact();
		cnt1.FirstName = 'Test';
		cnt1.LastName = 'Contact 1';
		cnt1.Phone = '+123456789';		
		cnt1.Email = 'c2testcc@covidien.com';
		cnt1.MobilePhone = '+123456789';		
		cnt1.MailingCity = 'Minneapolis';
		cnt1.MailingCountry = 'UNITED STATES';
		cnt1.MailingState = 'Minnesota';
		cnt1.MailingStreet = 'street';
		cnt1.MailingPostalCode = '123456';		
		cnt1.Contact_Department__c = 'Cardiology';
		cnt1.Contact_Primary_Specialty__c = 'Cardiovascular'; 
		cnt1.Affiliation_To_Account__c = 'Employee'; 
		cnt1.Primary_Job_Title_vs__c = 'Surgeon'; 
		cnt1.Contact_Gender__c = 'Male'; 
		cnt1.AccountId = acc.Id;
		cnt1.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Generic Contact').getRecordTypeId();
		
		Contact cnt2 = new Contact();
		cnt2.FirstName = 'Test';
		cnt2.LastName = 'Contact 2';
		cnt2.Phone = '+123456789';		
		cnt2.Alternate_Email__c = 'cttest@covidien.com';
		cnt2.MobilePhone = '+123456789';		
		cnt2.MailingCity = 'Minneapolis';
		cnt2.MailingCountry = 'UNITED STATES';
		cnt2.MailingState = 'Minnesota';
		cnt2.MailingStreet = 'street';
		cnt2.MailingPostalCode = '123456';		
		cnt2.Contact_Department__c = 'Cardiology';
		cnt2.Contact_Primary_Specialty__c = 'Cardiovascular'; 
		cnt2.Affiliation_To_Account__c = 'Employee'; 
		cnt2.Primary_Job_Title_vs__c = 'Surgeon'; 
		cnt2.Contact_Gender__c = 'Male'; 
		cnt2.AccountId = acc.Id;
		cnt2.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Generic Contact').getRecordTypeId();
                
        insert new List<Contact>{cnt, cnt1, cnt2};
   	
    } 
    
    static testMethod void testC2EmailService() {
             
        User testUser = [Select Id from User where email = 'c2testfrom@covidien.com'];
        
        System.runAs(testUser){
        
	        // Create a new email, envelope object and Attachment
	        Messaging.InboundEmail email = new Messaging.InboundEmail();
	        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
	        
	        env.fromAddress = 'Unit Test';
	        email.fromAddress = 'c2testfrom@covidien.com';
	        email.toAddresses = new List<String>{'c2testto@covidien.com'};
	        email.ccAddresses = new List<String>{'c2testcc@covidien.com','Test Contact 2<cttest@covidien.com>'};
	        	        	                       	        
	        email.subject = 'test';
	        email.plainTextBody = 'test';
	   
	        // call the class and test it with the data in the testMethod
	        Gizmo_InboundEmailHandler emailServiceObj = new Gizmo_InboundEmailHandler();
	        
	        Messaging.InboundEmailResult res = emailServiceObj.handleInboundEmail(email, env ); 
	        
	        System.assert(res.success == true);
	        
	        List<Task> tasks = [Select Id, WhoId, WhatId, OwnerId, IsReminderSet, Subject, Description from Task];
	        
	        System.assert(tasks.size() == 1);
	        
	        Task contentSharing = tasks[0];
	        
	        System.assert(contentSharing.WhatId == [Select Id from Account where RecordType.DeveloperName = 'NON_SAP_Account'].Id);
	        System.assert(contentSharing.OwnerId == testUser.Id);
	        System.assert(contentSharing.IsReminderSet == false);
	        System.assert(contentSharing.Subject == 'test');
	        System.assert(contentSharing.Description == 'test');
	        
	        List<TaskRelation> relations = [Select Id from TaskRelation where TaskId = :contentSharing.Id AND IsWhat = false];
	        
	        System.assertEquals(3, relations.size());	        
        }		
    }
    
    static testMethod void testC2EmailService_MissingParts() {
             
        User testUser = [Select Id from User where email = 'c2testfrom@covidien.com'];
        
        System.runAs(testUser){
        
	        // Create a new email, envelope object and Attachment
	        Messaging.InboundEmail email = new Messaging.InboundEmail();
	        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
	        
	        env.fromAddress = 'Unit Test';
	        email.fromAddress = 'wrongfromemail@covidien.com';
	        email.toAddresses = new List<String>{'wrongtoemail@covidien.com'};
	        	        
	        // call the class and test it with the data in the testMethod
	        Gizmo_InboundEmailHandler emailServiceObj = new Gizmo_InboundEmailHandler();
	        
	        Messaging.InboundEmailResult res = emailServiceObj.handleInboundEmail(email, env ); 
	        
	        System.assert(res.success == true);
	        
	        List<Task> tasks = [Select Id, WhoId, WhatId, OwnerId, IsReminderSet, Subject, Description from Task];
	        
	        System.assert(tasks.size() == 1);
	        
	        Task contentSharing = tasks[0];
	        
	        System.assert(contentSharing.WhatId == [Select Id from Account where RecordType.DeveloperName = 'MDT_Account' AND Name = 'MDT Employee Netherlands'].Id);
	        System.assert(contentSharing.OwnerId == testUser.Id);
	        System.assert(contentSharing.IsReminderSet == true);
	        System.assert(contentSharing.Subject == 'Content Sharing');
	        System.assert(contentSharing.Description.contains('Content Sharing'));
	        System.assert(contentSharing.Description.contains('wrongfromemail@covidien.com'));
	        
	        List<TaskRelation> relations = [Select Id from TaskRelation where TaskId = :contentSharing.Id AND IsWhat = false];
	        
	        System.assertEquals(0, relations.size());	        
        }		
    }   
}