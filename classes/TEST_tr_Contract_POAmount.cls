/*
 *      Description : This is the Test Class for the APEX Trigger tr_Contract_POAmount
 *      Version     : 1.0
 *      Author      : Bart Caelen
 *      Date        : 20140623
*/
@isTest private class TEST_tr_Contract_POAmount {
	
	@isTest static void test_tr_Contract_POAmount() {
	    //---------------------------------------
        // Create Test Data
        //---------------------------------------
		RecordType oRT_Contract = RecordTypeMedtronic.getRecordTypeByDevName('Contract', 'RTG_Service_Contract');
		clsTestData.createContractData(true, oRT_Contract.Id);
		clsTestData.iRecord_AssetContract = 5;
		clsTestData.createAssetContractData(true);
        //---------------------------------------

        Test.startTest();

	    //---------------------------------------
        // TEST 1
        //---------------------------------------
        List<Contract> lstContract = [SELECT Id, PO_Amount_derived_from_linked_assets__c, PO_Amount__c FROM Contract WHERE ID = :clsTestData.oMain_Contract.Id];
        system.assertEquals(lstContract[0].PO_Amount_derived_from_linked_assets__c, false);
        system.assertEquals(lstContract[0].PO_Amount__c, 0);
	    //---------------------------------------

	    //---------------------------------------
        // TEST 2
        //---------------------------------------
        clsTestData.oMain_Contract.PO_Amount_derived_from_linked_assets__c = true;
        update clsTestData.oMain_Contract;

        lstContract = [SELECT Id, PO_Amount_derived_from_linked_assets__c, PO_Amount__c FROM Contract WHERE ID = :clsTestData.oMain_Contract.Id];
        system.assertEquals(lstContract[0].PO_Amount_derived_from_linked_assets__c, true);
        system.assertEquals(lstContract[0].PO_Amount__c, 500);
	    //---------------------------------------


	    //---------------------------------------
        // TEST 3
        //---------------------------------------
        clsTestData.oMain_Contract.PO_Amount__c = 1200;
        update clsTestData.oMain_Contract;

        lstContract = [SELECT Id, PO_Amount_derived_from_linked_assets__c, PO_Amount__c FROM Contract WHERE ID = :clsTestData.oMain_Contract.Id];
        system.assertEquals(lstContract[0].PO_Amount_derived_from_linked_assets__c, true);
        system.assertEquals(lstContract[0].PO_Amount__c, 500);
	    //---------------------------------------


	    //---------------------------------------
        // TEST 4
        //---------------------------------------
        clsTestData.oMain_Contract.PO_Amount_derived_from_linked_assets__c = false;
        clsTestData.oMain_Contract.PO_Amount__c = 1200;
        update clsTestData.oMain_Contract;

        lstContract = [SELECT Id, PO_Amount_derived_from_linked_assets__c, PO_Amount__c FROM Contract WHERE ID = :clsTestData.oMain_Contract.Id];
        system.assertEquals(lstContract[0].PO_Amount_derived_from_linked_assets__c, false);
        system.assertEquals(lstContract[0].PO_Amount__c, 1200);
	    //---------------------------------------


	    //---------------------------------------
        // TEST 5
        //---------------------------------------
		clsTestData.oMain_Contract = null;
		clsTestData.createContractData(false, oRT_Contract.Id);

		clsTestData.oMain_Contract.PO_Amount_derived_from_linked_assets__c = true;
		clsTestData.oMain_Contract.PO_Amount__c = 500;
		insert clsTestData.oMain_Contract;

        lstContract = [SELECT Id, PO_Amount_derived_from_linked_assets__c, PO_Amount__c FROM Contract WHERE ID = :clsTestData.oMain_Contract.Id];
        system.assertEquals(lstContract[0].PO_Amount_derived_from_linked_assets__c, true);
        system.assertEquals(lstContract[0].PO_Amount__c, 0);
        //---------------------------------------

        Test.stopTest();

	}
	
}