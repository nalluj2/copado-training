public with sharing class ctrl_Picklist_Lookup_Popup {

	public List<SObject> records {get;set;}
		
	public String componentId {get; set;}
	public String objectType {get; set;}
	public List<String> fieldNames {get;set;}
	private String fieldString;
	private String filter;
	
	public String sortColumn {get; set;}
    public String sortDir {get; set;}
    public String selectedColumn {get; set;}	
		
	//Controller used by ChatterLookupPopup
	public ctrl_Picklist_Lookup_Popup(){
					
		String inputParams = ApexPages.currentPage().getParameters().get('p');
		
		if(inputParams!=null){
			
			String params = CryptoUtils.decryptText(inputParams); 
			
			for(String valuePair : params.split('&')){
				
				String[] pair = valuePair.split('=');
				
				if(pair.size() == 2){
					if(pair[0] == 'compId') componentId = pair[1];
					else if(pair[0] == 'sobject') objectType = pair[1];	
					else if(pair[0] == 'filter') filter = EncodingUtil.urlDecode(pair[1], 'UTF-8' );
					else if(pair[0] == 'fields'){
						fieldString = pair[1];
						fieldNames = fieldString.split(',');
					}
				}				 
			}
		}	
		
		sortColumn = 'Name';
		sortDir = 'ASC';
		
		getRecords();			
	}
				
	//perform search and return error or results.
	public void getRecords(){
				
		String query = 'Select Id,'+ fieldString + ' FROM ' + objectType + ' ' + filter + ' ORDER BY '+ sortColumn + ' ' + sortDir;
				
		records = Database.query(query);						
	}	
	
	public void sortTable(){
        
        if(selectedColumn == sortColumn){
            
            if(sortDir == 'ASC') sortDir = 'DESC';
            else sortDir = 'ASC';
            
        }else{
        	
            sortDir = 'ASC';
        }
        
        sortColumn = selectedColumn;
                      
        getRecords();
    }   
}