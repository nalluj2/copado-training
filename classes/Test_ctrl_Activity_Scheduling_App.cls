@isTest
private class Test_ctrl_Activity_Scheduling_App {
	
	private static testmethod void testGetTeams(){
		
		List<ctrl_Activity_Scheduling_App.Team> teamOptions = ctrl_Activity_Scheduling_App.getTeams();		
		System.assert(teamOptions.size() == 4);
	}
	
	private static testmethod void testGetOpenRequestsWeek(){
		
		createTestCases();
		
		Integer openRequests = ctrl_Activity_Scheduling_App.getOpenRequestsWeek();		
		System.assert(openRequests == 6);
	}
	
	private static testmethod void testGetPicklistValues(){
		
		Test.startTest();
		
		Test.setMock(HttpCalloutMock.class, new mock_Interface());
		
		Map<String, List<ctrl_Activity_Scheduling_App.PicklistValue>> picklistValueMap = ctrl_Activity_Scheduling_App.getPicklistValues();		
		
		System.assert(picklistValueMap.get('Activity_Scheduling_Team__c').size() == 3);
		System.assert(picklistValueMap.get('Assigned_To__c').size() > 1);
		System.assert(picklistValueMap.get('Procedure__c').size() == 10);
	}
	
	private static void createTestCases(){
		
		Account acc = new Account();
		acc.Name = 'Test Account';
		acc.Account_Country_vs__c = 'NETHERLANDS';		
		insert acc;
		
		List<Implant_Scheduling_Team__c> teams = [Select Id from Implant_Scheduling_Team__c];
		
		Id caseActivitySchedulingRT = [Select Id from RecordType where SObjectType = 'Case' AND DeveloperName = 'Implant_Scheduling'].Id;
		
		List<Case> caseList = new List<Case>();
		
		for(Integer i = 0; i < 6; i++){
		
			Case openCase = new Case();		
			openCase.AccountId = acc.Id;			
			openCase.Start_of_Procedure__c = DateTime.newInstance(Date.today().addDays(1), Time.newInstance(10 + i, 0, 0, 0)); 
			openCase.Procedure_Duration_Implants__c = '0' + i + ':00';
			openCase.RecordTypeId = caseActivitySchedulingRT;
			openCase.Activity_Scheduling_Team__c = teams.get(Math.mod(i, 2)).Id;
			openCase.Status = 'Open';	
			
			caseList.add(openCase);
		}
		
		List<Implant_Scheduling_Team_Member__c> members = [Select Member__c, Team__c from Implant_Scheduling_Team_Member__c];
		
		for(Integer i = 0; i < members.size(); i++){
			
			Implant_Scheduling_Team_Member__c member = members.get(i);
			
			Case assignedCase = new Case();		
			assignedCase.AccountId = acc.Id;			
			assignedCase.Start_of_Procedure__c = DateTime.newInstance(Date.today().addDays(-i), Time.newInstance(10, 0, 0, 0)); 
			assignedCase.Procedure_Duration_Implants__c = '0' + i + ':00';
			assignedCase.RecordTypeId = caseActivitySchedulingRT;
			assignedCase.Activity_Scheduling_Team__c = member.Team__c;
			assignedCase.Assigned_To__c = member.Member__c;
			assignedCase.Status = 'Assigned';	
			
			caseList.add(assignedCase);
		}
		
		
		bl_Case_Trigger.runningSchedulingApp = true;
		
		insert caseList;
	}
	
	@TestSetup
	private static void setData() {
		
		Mobile_App__c app = new Mobile_App__c();
		app.Name = 'Activity Scheduling Tool';
		app.Active__c = true;
		app.Unique_Key__c = 'ACT_SCHDL';
		insert app;
		
		List<Implant_Scheduling_Team__c> teams = new List<Implant_Scheduling_Team__c>();
		
		Id activitySchedulingTeamRT = [Select Id from RecordType where SObjectType = 'Implant_Scheduling_Team__c' AND DeveloperName = 'Activity_Scheduling_Team'].Id;
		
		Implant_Scheduling_Team__c team1 = new Implant_Scheduling_Team__c();			
		team1.Name = 'Test Team 1';
		team1.Team_Color__c = 'rgb(99, 99, 99)';
		team1.Work_Day_End__c = '18';
		team1.Work_Day_Start__c = '8';
		team1.Working_Days__c = '1:5';			
		team1.RecordTypeId = activitySchedulingTeamRT;			
				
		Implant_Scheduling_Team__c team2 = new Implant_Scheduling_Team__c();			
		team2.Name = 'Test Team 2';
		team2.Team_Color__c = 'rgb(99, 99, 99)';
		team2.Work_Day_End__c = '18';
		team2.Work_Day_Start__c = '8';
		team2.Working_Days__c = '1:5';			
		team2.RecordTypeId = activitySchedulingTeamRT;			
		
		insert new List<Implant_Scheduling_Team__c>{team1, team2};
				
		List<User> otherUsers = [Select Id from User where Profile.Name LIKE 'EUR Field Force CVG%' AND isActive = true LIMIT 10];
		
		List<Implant_Scheduling_Team_Member__c> members = new List<Implant_Scheduling_Team_Member__c>();
		
		Integer i = 0;
		
		for(User otherUser : otherUsers){
			
			Implant_Scheduling_Team_Member__c member = new Implant_Scheduling_Team_Member__c();
			member.Team__c = Math.mod(i, 2) == 0 ? team1.Id : team2.Id;
			member.Member__c = otherUser.Id;				
			
			members.add(member);
			i++;
		}
		
		insert members;		
	}  
	
	private class mock_Interface implements HttpCalloutMock {

        public HTTPResponse respond(HTTPRequest req) {
            
            HttpResponse httpResponse = new HttpResponse();
	        httpResponse.setHeader('Content-Type', 'application/json');
	        httpResponse.setStatusCode(200);
		        
		    List<String> fieldNames = new List<String>{'Procedure_Duration_Implants__c', 'Activity_Type_Picklist__c', 'Procedure__c', 'Origin', 'Priority', 'Venue__c', 'Experience_of_the_Implanter__c'};    
		        
		    ctrl_Activity_Scheduling_App.PicklistValuesResponse response = new ctrl_Activity_Scheduling_App.PicklistValuesResponse();		        
		    response.picklistFieldValues = new Map<String, ctrl_Activity_Scheduling_App.FieldPicklistValues>(); 
		    
		    for(String fieldName : fieldNames){
		    	
		    	ctrl_Activity_Scheduling_App.FieldPicklistValues fieldPicklists = new ctrl_Activity_Scheduling_App.FieldPicklistValues();		    	
		    	fieldPicklists.values = new List<ctrl_Activity_Scheduling_App.PicklistValue>(); 
		    
			    for(Integer i = 0; i < 10; i++){
			    	
			    	ctrl_Activity_Scheduling_App.PicklistValue pickValue = new ctrl_Activity_Scheduling_App.PicklistValue('picklistValue' + i, 'Picklist Label ' + i);		    	
			    	fieldPicklists.values.add(pickValue);
			    }
			    
			    response.picklistFieldValues.put(fieldName, fieldPicklists);
		    }
		        
	        httpResponse.setBody(JSON.serialize(response));
	        
            return httpResponse;
        }
	}		
}