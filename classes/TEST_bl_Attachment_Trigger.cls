//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 22-01-2016
//  Description      : APEX Test Class to test the logic in bl_Attachment_Trigger
//  Change Log       : PMT-22787: MITG EMEA User Setup
//--------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_bl_Attachment_Trigger {
 
    //----------------------------------------------------------------------------------------
    // CREATE TEST DATA
    //----------------------------------------------------------------------------------------
    @testSetup static void createTestData() {

        // Create Custom Setting Data
        clsTestData.createCustomSettingData(true);

        // Create Service Order Data
        clsTestData.createSVMXCServiceOrderData(true);
    }
    //----------------------------------------------------------------------------------------

	
    //----------------------------------------------------------------------------------------
    // Test related to notificationSAP
    //----------------------------------------------------------------------------------------
	@isTest static void test_notificationSAP_Insert() {
		
		// Select Data
		SVMXC__Service_Order__c sOrder = [SELECT Id FROM SVMXC__Service_Order__c];
		
		Test.startTest();
		
		Integer emailbefore = Limits.getEmailInvocations();
				
		Attachment oAttachment = new Attachment();
        oAttachment.ParentId= sOrder.Id;
        oAttachment.Name = 'TESTAttachmentData.pdf';
        oAttachment.Body = Blob.valueOf('TEST BODY');
        insert oAttachment;
		
		NotificationSAPLog__c notification = [Select Record_ID__c, Status__c from NotificationSAPLog__c where Record_ID__c = :oAttachment.Id];
		System.assert(notification.Status__c == 'NEW');
		
		System.assertEquals(emailbefore, Limits.getEmailInvocations());
		
		Test.stopTest();

	}
	
	@isTest static void test_notificationSAP_Rename() {
		
		// Select Data
		SVMXC__Service_Order__c sOrder = [SELECT Id FROM SVMXC__Service_Order__c];
		
		Test.startTest();
		
		Integer emailbefore = Limits.getEmailInvocations();
				
		Attachment oAttachment = new Attachment();
        oAttachment.ParentId= sOrder.Id;
        oAttachment.Name = 'TEST.Attachment.Data.pdf';
        oAttachment.Body = Blob.valueOf('TEST BODY WITH DOT');
        insert oAttachment;
		
		oAttachment = [Select Name from Attachment where Id = :oAttachment.Id];
		System.assert(oAttachment.Name == 'TEST_Attachment_Data.pdf');
	
		NotificationSAPLog__c notification = [Select Record_ID__c, Status__c from NotificationSAPLog__c where Record_ID__c = :oAttachment.Id];
		System.assert(notification.Status__c == 'NEW');
		
		System.assertEquals(emailbefore, Limits.getEmailInvocations());
		
		Test.stopTest();

	}
	
	@isTest static void test_notificationSAP_NotSupportedExtension() {
		
		// Select Data
		SVMXC__Service_Order__c sOrder = [SELECT Id FROM SVMXC__Service_Order__c];
		
		Test.startTest();
		
		Integer emailbefore = Limits.getEmailInvocations();
				
		Attachment oAttachment = new Attachment();
        oAttachment.ParentId= sOrder.Id;
        oAttachment.Name = 'TESTAttachmentData.log';
        oAttachment.Body = Blob.valueOf('TEST BODY');
        insert oAttachment;
		
		List<NotificationSAPLog__c> notifications = [Select Record_ID__c, Status__c from NotificationSAPLog__c where Record_ID__c = :oAttachment.Id];
		System.assert(notifications.size() == 0);
		
		System.assert((Limits.getEmailInvocations() - emailbefore) == 1);
		
		Test.stopTest();

	}
	
	@isTest static void test_notificationSAP_NotSupportedExtension_Failure() {
		
		// Select Data
		SVMXC__Service_Order__c sOrder = [SELECT Id FROM SVMXC__Service_Order__c];
		
		sOrder.SVMXC__Service_Group__c = null;
		update sOrder;
		
		Integer emailbefore = Limits.getEmailInvocations();
						
		Test.startTest();
						
		Attachment oAttachment = new Attachment();
        oAttachment.ParentId= sOrder.Id;
        oAttachment.Name = 'TESTAttachmentData.log';
        oAttachment.Body = Blob.valueOf('TEST BODY');
        insert oAttachment;
		
		List<NotificationSAPLog__c> notifications = [Select Record_ID__c, Status__c from NotificationSAPLog__c where Record_ID__c = :oAttachment.Id];
		System.assert(notifications.size() == 0);
		
		System.assert((Limits.getEmailInvocations() - emailbefore) == 1);
		
		Test.stopTest();

	}
    //----------------------------------------------------------------------------------------


	//----------------------------------------------------------------------------------------
    // Test related to MITG Attachments
    //----------------------------------------------------------------------------------------
	@isTest static void test_Attachment_MITG(){

		// CREATE TEST DATA
		User oUser_Admin = clsTestData_User.createUser_SystemAdministratorMDT('tstadm01', true);
		User oUser_MITG = clsTestData_User.createUser('tstmitg1', false);
			
        	//PMT-22787
        	//New profile EMEA Field Force MITG will replace profiles: EUR Field Force MITG and MEA Field Force MITG
        	//oUser_MITG.ProfileId = clsUtil.getUserProfileId('EUR Field Force MITG');
			oUser_MITG.ProfileId = clsUtil.getUserProfileId('EMEA Field Force MITG');
		insert oUser_MITG;

		System.runAs(oUser_MITG){
		
			clsTestData_Account.createAccount(false);
				clsTestData_Account.oMain_Account.Name = 'TEST Account MITG';
				clsTestData_Account.oMain_Account.isSales_Force_Account__c = false;
			insert clsTestData_Account.oMain_Account;

			
			
			//PMT-22787
        	//New record type for opportunity EMEA MITG Standard Opportunity will replace record types: EUR MITG Standard Opportunity and MEA MITG Standard Opportunity
        	//clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'EUR_MITG_Standard_Opportunity').Id;
            clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'EMEA_MITG_Standard_Opportunity').Id;
			clsTestData_Opportunity.createOpportunity();
		
		}

		System.runAs(oUser_Admin){

			List<Attachment> lstAttachment = new List<Attachment>();
				Attachment oAttachment_Account = new Attachment();
					oAttachment_Account.ParentId = clsTestData_Account.oMain_Account.Id;
					oAttachment_Account.Name = 'TEST Attachment';
					oAttachment_Account.Body = Blob.valueOf('TEST BODY');
				lstAttachment.add(oAttachment_Account);
				Attachment oAttachment_Opportunity = new Attachment();
					oAttachment_Opportunity.ParentId = clsTestData_Opportunity.oMain_Opportunity.Id;
					oAttachment_Opportunity.Name = 'TEST Attachment';
					oAttachment_Opportunity.Body = Blob.valueOf('TEST BODY');
				lstAttachment.add(oAttachment_Opportunity);
			insert lstAttachment;

		}

		System.runAs(oUser_Admin){

			List<Account> lstAccount = [SELECT Id, Linked_Attachments__c FROM Account WHERE Name = 'TEST Account MITG'];
			System.assertEquals(lstAccount.size(), 1);
			System.assertEquals(lstAccount[0].Linked_Attachments__c, false);

			List<Opportunity> lstOpportunity = [SELECT Id, Linked_Attachments__c FROM Opportunity];
			System.assertEquals(lstOpportunity.size(), 1);
			System.assertEquals(lstOpportunity[0].Linked_Attachments__c, false);

		}

		// TEST LOGIC
		Test.startTest();

		System.runAs(oUser_MITG){

			List<Attachment> lstAttachment = new List<Attachment>();
				Attachment oAttachment_Account = new Attachment();
					oAttachment_Account.ParentId = clsTestData_Account.oMain_Account.Id;
					oAttachment_Account.Name = 'TEST Attachment MITG';
					oAttachment_Account.Body = Blob.valueOf('TEST BODY');
				lstAttachment.add(oAttachment_Account);
				Attachment oAttachment_Opportunity = new Attachment();
					oAttachment_Opportunity.ParentId = clsTestData_Opportunity.oMain_Opportunity.Id;
					oAttachment_Opportunity.Name = 'TEST Attachment MITG';
					oAttachment_Opportunity.Body = Blob.valueOf('TEST BODY');
				lstAttachment.add(oAttachment_Opportunity);
			insert lstAttachment;

			List<Account> lstAccount = [SELECT Id, Linked_Attachments__c FROM Account WHERE Name = 'TEST Account MITG'];
			System.assertEquals(lstAccount[0].Linked_Attachments__c, true);

			List<Opportunity> lstOpportunity = [SELECT Id, Linked_Attachments__c FROM Opportunity];
			System.assertEquals(lstOpportunity[0].Linked_Attachments__c, true);


			delete lstAttachment;

			lstAccount = [SELECT Id, Linked_Attachments__c FROM Account WHERE Name = 'TEST Account MITG'];
			System.assertEquals(lstAccount[0].Linked_Attachments__c, false);

			lstOpportunity = [SELECT Id, Linked_Attachments__c FROM Opportunity];
			System.assertEquals(lstOpportunity[0].Linked_Attachments__c, false);


			undelete lstAttachment;

			lstAccount = [SELECT Id, Linked_Attachments__c FROM Account WHERE Name = 'TEST Account MITG'];
			System.assertEquals(lstAccount[0].Linked_Attachments__c, true);

			lstOpportunity = [SELECT Id, Linked_Attachments__c FROM Opportunity];
			System.assertEquals(lstOpportunity[0].Linked_Attachments__c, true);
		}

		Test.stopTest();

		// VALIDATE RESULT
	}

    //----------------------------------------------------------------------------------------
}
//--------------------------------------------------------------------------------------------------------------------