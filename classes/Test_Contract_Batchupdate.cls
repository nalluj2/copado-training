@isTest 
private class Test_Contract_Batchupdate {
	
	static testMethod void myUnitTest() {
        
        Account acc=new Account();
        acc.AccountNumber='as';
        acc.Name='MNav House Account';
        acc.AccountNumber = '0001234567';
        acc.Account_Country_vs__c='USA';        
        acc.CurrencyIsoCode='USD';
        acc.Phone='24107954';
        insert acc; 
        
        Asset sys = new Asset();
        sys.Serial_Nr__c = 'XXXX';
        sys.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Navigation').getRecordTypeId();
        sys.Sold_as_Refurbished__c=true;
        sys.Name='Hyd';
        sys.Asset_Product_Type__c='Fusion';
        sys.Delivery_Date__c=System.today();
        sys.AccountId=Acc.Id;
        sys.Ownership_Status__c='FPU';
        sys.Contract_End_Date__c=system.Today();
        sys.AxiEM_Included__c=false;        
        insert sys;
        
        Contract sContract = new Contract();        
        //sContract.of_FSE_Visits_Used__c =  50;
        //sContract.of_Surgery_Coverages_Used__c = 50;        
        sContract.StartDate = date.today().addDays(-30);
        sContract.ContractTerm = 15;
        //sContract.Anniversary__c = date.today();
        //sContract.Additional_FSE_Visits_Purchased__c = 50;
        //sContract.Additional_Surgery_Coverages_Purchased__c = 50;
        sContract.name = 'Test Contract';
        sContract.AccountId = acc.Id;
        insert sContract;
        
        AssetContract__c ent = new AssetContract__c();
        ent.of_FSE_Visits__c = 20;
        ent.of_Surgery_Coverages__c = 20;
        ent.Contract__c = sContract.id;
        ent.Asset__c = sys.id;
        insert ent;
        
        sContract.Anniversary__c = Date.Today();   
        sContract.entitlement_anniversary_check__c = false;
        update sContract;
                
        Test.startTest();
        
        sContract = [Select Anniversary__c from Contract where Id = :sContract.Id];
        System.assertEquals(Date.Today(), sContract.Anniversary__c);
         
        Database.executeBatch(new Contract_Batchupdate(),1);
         
    	Test.StopTest();       
	}
}