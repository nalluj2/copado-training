/*
 *      Description : This class exposes methods to create / update / delete a Account Plan Objective
 *
 *      Author = Rudy De Coninck
 */

@RestResource(urlMapping='/AccountPlanObjectiveService/*')
global with sharing class ws_AccountPlanObjectiveService {
	@HttpPost
	global static String doPost() {
 		RestRequest req = RestContext.request;
			
		System.debug('post called '+req);
		System.debug('post requestBody '+req.requestBody);
			
		String body = req.requestBody.toString();
		IFAccountPlanObjectiveRequest accountPlanObjectiveRequest = (IFAccountPlanObjectiveRequest)System.Json.deserialize(body, IFAccountPlanObjectiveRequest.class);
			
		System.debug('post requestBody '+accountPlanObjectiveRequest);
			
		Account_Plan_Objective__c accountPlanObjective = accountPlanObjectiveRequest.accountPlanObjective;

		IFAccountPlanObjectiveResponse resp = new IFAccountPlanObjectiveResponse();

		//We catch all exception to assure that 
		try{
				
			resp.accountPlanObjective = bl_AccountPlanning.saveAccountPlanObjective(accountPlanObjective);
			resp.id = accountPlanObjectiveRequest.id;
	
			resp.success = true;
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
		}
 
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'AccountPlanObjective' ,req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
  			
		return System.JSON.serialize(resp);		
	}

	@HttpDelete
	global static String doDelete(){
		RestRequest req = RestContext.request;
			
		System.debug('post called '+req);
		System.debug('post requestBody '+req.requestBody);
			
		String body = req.requestBody.toString();
		IFAccountPlanObjectiveDeleteRequest accountPlanObjectiveDeleteRequest = (IFAccountPlanObjectiveDeleteRequest)System.Json.deserialize(body, IFAccountPlanObjectiveDeleteRequest.class);
			
		System.debug('post requestBody '+accountPlanObjectiveDeleteRequest);
			
		Account_Plan_Objective__c accountPlanObjective = accountPlanObjectiveDeleteRequest.accountPlanObjective;
		IFAccountPlanObjectiveDeleteResponse resp = new IFAccountPlanObjectiveDeleteResponse();
		
		List<Account_Plan_Objective__c>accountPlanObjectivesToDelete = [select id, mobile_id__c from Account_Plan_Objective__c where Mobile_ID__c = :accountPlanObjective.Mobile_ID__c];
		
		try{
			
			if (accountPlanObjectivesToDelete !=null && accountPlanObjectivesToDelete.size()>0 ){	
				
				Account_Plan_Objective__c accountPlanObjectiveToDelete = accountPlanObjectivesToDelete.get(0);
				delete accountPlanObjectiveToDelete;
				resp.accountPlanObjective = accountPlanObjectiveToDelete;
				resp.id = accountPlanObjectiveDeleteRequest.id;
				resp.success = true;
			}else{
				resp.accountPlanObjective = accountPlanObjective;
				resp.id = accountPlanObjectiveDeleteRequest.id;
				resp.message='Account Plan Objective not found';
				resp.success = true;
			}
			
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
		}
		
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'AccountPlanObjectiveDelete',req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
			
		return System.JSON.serialize(resp);
	}
	
	global class IFAccountPlanObjectiveRequest{
		public Account_Plan_Objective__c accountPlanObjective {get;set;}
		public String id{get;set;}
	}
	
	global class IFAccountPlanObjectiveDeleteRequest{
		public Account_Plan_Objective__c accountPlanObjective {get;set;}
		public String id{get;set;}
	}
		
	global class IFAccountPlanObjectiveDeleteResponse{
		public Account_Plan_Objective__c accountPlanObjective {get;set;}
		public String id{get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}

	global class IFAccountPlanObjectiveResponse{
		public Account_Plan_Objective__c accountPlanObjective {get;set;}
		public String id{get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}
}