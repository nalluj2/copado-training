/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 *
 * Creation Date:   20090305
 * Description:     Test Coverage of trigger : ContactAfterUndelete !
 *              
 * Author:          ABSI - MC
 */
@isTest
private class TEST_triggerContactAfterUndelete {

    static testMethod void coverTriggerContactAfterUndelete() {
        System.debug(' ################## ' + 'BEGIN TEST_triggerContactAfterUndelete' + ' ################');
        
        DIB_Country__c nl = [SELECT Id,  BU_Not_Mandatory_on_Relationship__c, Hide_Therapy_on_Relationship__c FROM DIB_Country__c where Country_ISO_Code__c = 'NL'];
        nl.BU_Not_Mandatory_on_Relationship__c = true;
        nl.Hide_Therapy_on_Relationship__c = true;
        update nl;
        
        List<Account> accList = new List<Account>();
        Account acc1 = new Account();
        acc1.Name = 'TEST_triggerContactAfterUndelete Test Coverage Account 1 ' ;
        acc1.Account_Country_vs__c = 'NETHERLANDS';      
        accList.add(acc1);

        Account acc2 = new Account();
        acc2.Name = 'TEST_triggerContactAfterUndelete Test Coverage Account 2 ' ; 
        acc2.Account_Country_vs__c = 'NETHERLANDS';
        accList.add(acc2);
        
        insert accList;
        
        List<Contact> cntList = new List<Contact>();
        
        Contact cont1 = new Contact();
        cont1.LastName = 'TEST_triggerContactAfterUndelete Test Coverage' ;  
		cont1.FirstName = 'test';
        cont1.AccountId = acc1.Id ; 
	    cont1.Contact_Department__c = 'Diabetes Adult'; 
	    cont1.Contact_Primary_Specialty__c = 'ENT';
	    cont1.Affiliation_To_Account__c = 'Employee';
	    cont1.Primary_Job_Title_vs__c = 'Manager';
	    cont1.Contact_Gender__c = 'Male'; 
        cntList.add(cont1);       
             
        Contact cont2 = new Contact();
        cont2.LastName = 'TEST_triggerContactAfterUndelete Test Coverage' ;  
		cont2.FirstName = 'test';
        cont2.AccountId = acc2.Id ;
	    cont2.Contact_Department__c = 'Diabetes Adult'; 
	    cont2.Contact_Primary_Specialty__c = 'ENT';
	    cont2.Affiliation_To_Account__c = 'Employee';
	    cont2.Primary_Job_Title_vs__c = 'Manager';
	    cont2.Contact_Gender__c = 'Male'; 
        cntList.add(cont2); 
        
        insert cntList;   
         
        List<Affiliation__c> affList = new List<Affiliation__c>();             
        // The addError function will fire if there is any related object to Contact, so let's create one : (Per ex.: a new affiliation)
        Affiliation__c aff1 = new Affiliation__c();
        aff1.RecordTypeId = FinalConstants.recordTypeIdC2C;
        aff1.Affiliation_From_Contact__c = cont1.Id;
        aff1.Affiliation_To_Contact__c = cont2.Id ;
        aff1.Affiliation_From_Account__c = acc1.Id ;
        aff1.Affiliation_To_Account__c = acc2.Id ;
        affList.add(aff1) ;
        
        Affiliation__c aff2 = new Affiliation__c();
        aff2.RecordTypeId = FinalConstants.recordTypeIdC2C;
        aff2.Affiliation_From_Contact__c = cont2.Id ;
        aff2.Affiliation_From_Account__c = acc2.Id ; 
        aff2.Affiliation_To_Contact__c = cont1.Id ;
        aff2.Affiliation_To_Account__c = acc1.Id ; 
        affList.add(aff2) ;
        
        insert affList;
        
        List<Affiliation__c> lstAff = [Select i.id from Affiliation__c i where i.Affiliation_From_Contact__c =:cont1.Id or i.Affiliation_To_Contact__c =: cont1.Id];       
        delete lstAff;
        
        Test.startTest();
        
       // delete cont1 ; 
        
        //undelete cont1 ;
        
        Test.stopTest();
        
        /*  
         * Once undeleted, the affiliations still need to be linked to the Contact, if it is then this means they are undeleted as well !
         */
         
        system.assertEquals(aff1.Affiliation_To_Contact__c, cont2.Id);
        system.assertEquals(aff2.Affiliation_From_Contact__c, cont2.Id);         
        
        System.debug(' ################## ' + 'END TEST_triggerContactAfterUndelete' + ' ################');
             
    }
}