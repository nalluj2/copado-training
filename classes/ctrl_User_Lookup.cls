public with sharing class ctrl_User_Lookup {
	
	public List<SObject> allCFUsers {get;set;}
	
	public String searchString {get;set;}
	public Boolean hasResults {get;set;}
	
	public String componentId {get; set;}
	private Boolean activeFlag;
	
	//Controller used by ChatterLookupPopup
	public ctrl_User_Lookup(){
		searchString = '';
		hasResults = false;		
		
		componentId = ApexPages.currentPage().getParameters().get('compId');
		
		String inputActiveFlag = ApexPages.currentPage().getParameters().get('active');
		
		if(inputActiveFlag!=null && inputActiveFlag!='') activeFlag = Boolean.valueOf(inputActiveFlag);
		else activeFlag = true;
	}
				
	//perform search and return error or results.
	public void search(){
		
		if(searchString.length()<2){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'search term must be at least two characters'));
			return;
		}
		
		String escapedInput = String.escapeSingleQuotes(searchString);
		
		if(escapedInput.endsWith('*')==false) escapedInput+='*';
		
		String searchquery='FIND \''+escapedInput+'\'IN NAME FIELDS RETURNING User(id WHERE isActive = '+activeFlag+')'; 
		List<List<SObject>> searchList = search.query(searchquery);
		
		List<SObject> users = searchList[0];
		
		if(users.isEmpty()){
			hasResults = false;
			allCFUsers = users;
		}else{
		
			allCFUsers = [Select Id, Name, Email, Alias, UserRole.Name from User where Id IN:users ORDER BY Name ASC LIMIT 100];
			hasResults = true;
		}		
	}
	
	//reset search, display all users
	public void clearResults(){
		searchString = '';
		hasResults = false;		
	}
			
}