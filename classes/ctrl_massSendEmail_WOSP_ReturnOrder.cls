//------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 02-10-2019
//  Description      : APEX Controller for VF Page massSendEmail_WOSP_ReturnOrder
//  Change Log       : 
//------------------------------------------------------------------------------------------------------------
public class ctrl_massSendEmail_WOSP_ReturnOrder {


    //--------------------------------------------------------------------------------------------------------
    // PRIVATE VARIABLES
    //--------------------------------------------------------------------------------------------------------
    private String tPara_RecordId;                  // Id of the processing Record [Case, ...)
    private String tPara_InitialTO;                 // User ID's [separated by $] that will be added to the Selected TO Box by default
    private String tPara_InitialCC;                 // User ID's [separated by $] that will be added to the Selected CC Box by default
    private String tPara_InitialBCC;                // User ID's [separated by $] that will be added to the Selected BCC Box by default
    private String tPara_OrgWideEmailAddressId;     // OrgWideEmailAddress ID's separated by $
    private String tPara_AdditionalEmailTO;         // Email Addresses [separated by $] that will be added to the TO of the email that will be send
    private String tPara_AdditionalEmailCC;         // Email Addresses [separated by $] that will be added to the CC of the email that will be send
    private String tPara_AdditionalEmailBCC;        // Email Addresses [separated by $] that will be added to the BCC of the email that will be send
	private String tEmailSandboxSufix;				// The sufix that will be added to email addresses when on a sandbox

    private List<String> lstOrgWideEmailAddressId;  // Available OrgWideEmailAddress ID's
    private Map<Id, OrgWideEmailAddress> mapOrgWideEmailAddress = new Map<Id, OrgWideEmailAddress>();
    @TestVisible private Map<String, List<String>> mapCountry_EmailAddresses = new Map<String, List<String>>();
    @TestVisible private Map<String, List<String>> mapEmailAddresses_Country = new Map<String, List<String>>();
    @TestVisible private Map<String, List<String>> mapCountry_EmailAddresses_IS = new Map<String, List<String>>();

    private Case oCase;
    //--------------------------------------------------------------------------------------------------------


    //--------------------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //--------------------------------------------------------------------------------------------------------
    public ctrl_massSendEmail_WOSP_ReturnOrder() {

        // Read URL Parameters
        Map<String, String> mapURLParameter = ApexPages.currentPage().getParameters();
			System.debug('**BC** mapURLParameter : ' + mapURLParameter);
            tPara_RecordId = mapURLParameter.get('id');
            tPara_InitialTO = mapURLParameter.get('initialto');
            tPara_InitialCC = mapURLParameter.get('initialcc');
            tPara_InitialBCC = mapURLParameter.get('initialbcc');
            tPara_OrgWideEmailAddressId = mapURLParameter.get('orgwideemailid');
			tPara_AdditionalEmailTO = mapURLParameter.get('additionalemailto');
			tPara_AdditionalEmailCC = mapURLParameter.get('additionalemailcc');
			tPara_AdditionalEmailBCC = mapURLParameter.get('additionalemailbcc');

        // Initialize Data
        initialize();
        
    }
    //--------------------------------------------------------------------------------------------------------


    //--------------------------------------------------------------------------------------------------------
    // PRIVATE METHODS
    //--------------------------------------------------------------------------------------------------------
    private void initialize(){

        tEmailSandboxSufix = '';
        if (clsUtil.bRunningInASandbox()) tEmailSandboxSufix = '.test';

		lstwrWOSP = new List<wrWOSP>();

        // Load Data - CASE
        oCase =
            [
                SELECT 
                    Id, CaseNumber
					, ShippingStreet__c, ShippingPostalCode__c, ShippingCity__c, ShippingState__c, ShippingCountry__c, ShippingDepartment__c
					, Account.Id, Account.Name, Account.SAP_Id__c
					, Account.Account_Address_Line_1__c, Account.Account_Address_Line_2__c, Account.Account_Address_Line_3__c, Account.Account_Postal_Code__c, Account.Account_City__c, Account.Account_Province__c, Account.Account_Country_vs__c
					, Account.BillingStreet, Account.BillingPostalCode, Account.BillingCity, Account.BillingState, Account.BillingCountry
					, Contact.Name, Contact.Phone, Contact.Email
					, Owner.Name, Owner.Phone, Owner.Email
					, Asset.Customer_Warranty_End_Date__c, Asset.hasActiveContract__c, Asset.Service_Level__c
                FROM 
                    Case 
                WHERE 
                    Id = :tPara_RecordId
            ];

		owrCase = new wrCase(oCase);


        // Load Data - WorkOrder SparePart (WOSP)
        String tSOQL = 'SELECT Id, Name, SAP_Return__c, No_Return_Justification__c, Lot_Number__c, Return_Item_Status__c, Order_Quantity__c, Special_Notes__c, CurrencyIsoCode';
        tSOQL += ', Order_Part__c, Order_Part__r.Id, Order_Part__r.Name, Order_Part__r.ProductCode, Order_Part__r.CFN_Code_Text__c, Order_Part__r.Description, Order_Part_List_Price__c, Order_Part_List_Price_Manual__c';
		tSOQL += ', RI_Part__c, RI_Part__r.Id, RI_Part__r.Name, RI_Part_Quantity__c, RI_Part__r.ProductCode, RI_Part__r.CFN_Code_Text__c, RI_Part__r.Description';
		tSOQL += ', Case__r.CaseNumber';
        tSOQL += ' FROM Workorder_Sparepart__c';
        tSOQL += ' WHERE Case__c = \'' + oCase.Id + '\'';

        List<Workorder_Sparepart__c> lstWOSP = Database.query(tSOQL);

		for (Workorder_Sparepart__c oWOSP : lstWOSP){
			lstwrWOSP.add(new wrWOSP(oWOSP));
		}

        // Initialize variables
        lstId_CC = new List<String>();
        lstId_BCC = new List<String>();
        lstId_Initial_TO = new List<String>();
        lstId_Initial_CC = new List<String>();
        lstId_Initial_BCC = new List<String>();
        lstOrgWideEmailAddressId = new List<String>();


        // Set the Initial values for the TO, CC and BCC box
        if (!String.isBlank(tPara_InitialTO)){
            List<String> lstValue = tPara_InitialTO.split('\\$');
            lstId_Initial_TO.addAll(lstValue);
        }
        if (!String.isBlank(tPara_InitialCC)){
            List<String> lstValue = tPara_InitialCC.split('\\$');
            lstId_Initial_CC.addAll(lstValue);
        }
        if (!String.isBlank(tPara_InitialBCC)){
            List<String> lstValue = tPara_InitialBCC.split('\\$');
            lstId_Initial_BCC.addAll(lstValue);
        }
        if (!String.isBlank(tPara_OrgWideEmailAddressId)){
            List<String> lstValue = tPara_OrgWideEmailAddressId.split('\\$');
            lstOrgWideEmailAddressId.addAll(lstValue);
        }
		tAdditionalEmail_TO = '';
        if (!String.isBlank(tPara_AdditionalEmailTO)){
            List<String> lstValue = tPara_AdditionalEmailTO.split('\\$');
			for (String tEmail : lstValue){
				if (!String.isBlank(tAdditionalEmail_TO)) tAdditionalEmail_TO += ',';
				tAdditionalEmail_TO += tEmail + tEmailSandboxSufix;
			}
        }
		tAdditionalEmail_CC = '';
        if (!String.isBlank(tPara_AdditionalEmailCC)){
            List<String> lstValue = tPara_AdditionalEmailCC.split('\\$');
			for (String tEmail : lstValue){
				if (!String.isBlank(tAdditionalEmail_CC)) tAdditionalEmail_CC += ',';
				tAdditionalEmail_CC += tEmail + tEmailSandboxSufix;
			}
        }
		tAdditionalEmail_BCC = '';
        if (!String.isBlank(tPara_AdditionalEmailBCC)){
            List<String> lstValue = tPara_AdditionalEmailBCC.split('\\$');
			for (String tEmail : lstValue){
				if (!String.isBlank(tAdditionalEmail_BCC)) tAdditionalEmail_BCC += ',';
				tAdditionalEmail_BCC += tEmail + tEmailSandboxSufix;
			}
        }

        // Load OrgWideEmailAddress
        loadOrgWideEmailAddress();

        // Load Country Email Address
        loadCountryEmailAddresses();

        // Get the TO Email Addresses
        tCountryEmail = '';
        lstCountryEmail_Selected = new List<String>();
        iCountryEmail_Size = 1;
        bCountryEmail_Multiple = false;
        lstSO_CountryEmail = populateCountryEmail();

    }


    private void loadCountryEmailAddresses(){

        mapCountry_EmailAddresses = new Map<String, List<String>>();
        mapEmailAddresses_Country = new Map<String, List<String>>();

        List<DIB_Country__c> lstCountry = [SELECT Name, Email_Service_Repair__c FROM DIB_Country__c WHERE Email_Service_Repair__c != null];
        for (DIB_Country__c oCountry : lstCountry){
			
			//Service & Repair
			if(oCountry.Email_Service_Repair__c != null && oCountry.Email_Service_Repair__c != ''){
	            
	            List<String> lstEmail_Tmp = oCountry.Email_Service_Repair__c.split(';');
	            
	            List<String> lstEmail = new List<String>();
	            for (String tEmail : lstEmail_Tmp){
	                
	                tEmail += tEmailSandboxSufix;
	                lstEmail.add(tEmail);
	                
	                List<String> emailCountries = mapEmailAddresses_Country.get(tEmail);
	                if(emailCountries == null){
	                	emailCountries = new List<String>();
	                	mapEmailAddresses_Country.put(tEmail, emailCountries);
	                }
	                emailCountries.add(oCountry.Name.toUpperCase());
	            }
	            
	            mapCountry_EmailAddresses.put(oCountry.Name.toUpperCase(), lstEmail);
			}
           
        }

    }


    @TestVisible private List<SelectOption> populateCountryEmail(){

        List<SelectOption> lstSO_Tmp = new List<SelectOption>();
        String tShippingCountry = clsUtil.isNull(oCase.ShippingCountry__c, oCase.Account.Account_Country_vs__c).toUpperCase();

        lstCountryEmail_Selected = new List<String>();

        if ( (tShippingCountry == '') || (!mapCountry_EmailAddresses.containsKey(tShippingCountry)) ){

            lstSO_Tmp.add(new SelectOption('', ' -- NONE -- '));
    
            for (String tCountry : mapCountry_EmailAddresses.keySet()){

                List<String> lstEmail = mapCountry_EmailAddresses.get(tCountry);

                for (String tEmail : lstEmail){
                    lstSO_Tmp.add(new SelectOption(tEmail, tEmail + ' (' + tCountry + ')'));
                }
            }

            bCountryEmail_Multiple = false;

        }else{

            List<String> lstEmail = mapCountry_EmailAddresses.get(tShippingCountry);

            for (String tEmail : lstEmail){
                lstSO_Tmp.add(new SelectOption(tEmail, tEmail + ' (' + tShippingCountry + ')'));
                lstCountryEmail_Selected.add(tEmail); 
            }

            iCountryEmail_Size = lstCountryEmail_Selected.size();
            if (iCountryEmail_Size == 0) iCountryEmail_Size = 1;

            bCountryEmail_Multiple = false;
            if (iCountryEmail_Size > 1) bCountryEmail_Multiple = true;

        }

        return lstSO_Tmp;

    }

    
    private void loadOrgWideEmailAddress(){

        lstSO_OrgWideEmailAddress = new List<SelectOption>();

        lstSO_OrgWideEmailAddress.add(new SelectOption(UserInfo.getUserId(), '"' + UserInfo.getFirstName() + ' ' + UserInfo.getLastName() + '" <' + UserInfo.getUserEmail() + '>'));

        if (lstOrgWideEmailAddressId.size() > 0){
            mapOrgWideEmailAddress = new Map<Id, OrgWideEmailAddress>([SELECT Id, DisplayName, Address FROM OrgWideEmailAddress WHERE Id = :lstOrgWideEmailAddressId ORDER BY DisplayName]);

            for (OrgWideEmailAddress oOrgWideEmailAddress : mapOrgWideEmailAddress.values()){
                lstSO_OrgWideEmailAddress.add(new SelectOption(oOrgWideEmailAddress.Id, '"' + oOrgWideEmailAddress.DisplayName + '" <' + oOrgWideEmailAddress.Address + '>'));
            }

        }

        id_EmailFrom = UserInfo.getUserId();

        bShowFROM = false;
        if (lstSO_OrgWideEmailAddress.size() > 1){
            bShowFROM = true;
        }

    }


    @TestVisible private String tEmailBody(List<wrWOSP> lstWRWOSP_Processing){

        String tBody = '';

        String tBaseURL = URL.getSalesforceBaseUrl().toExternalForm();

		// Collect the SAP_Return__c Numbers
		Set<String> setSAPReturn = new Set<String>();
        for (wrWOSP owrWOSP : lstWRWOSP_Processing){
			if (owrWOSP.oWOSP.SAP_Return__c != null) setSAPReturn.add(owrWOSP.oWOSP.SAP_Return__c);
		}
		List<String> lstSAPReturn = new List<String>();
		lstSAPReturn.addAll(setSAPReturn);


        tBody += '<p>';
			tBody += 'Please update/ complete Return order number " ' + owrCase.tReturnOrderNumber + '"';
			tBody += ' and arrange a pick-up for Parent Case <a href="' + tBaseURL + '/' + oCase.Id + '">' + oCase.CaseNumber + '</a>';
			tBody += ' Account <a href="' + tBaseURL + '/' + oCase.Account.Id + '">' + oCase.Account.Name + '</a>';
//            tBody += 'Please create a return order for ';
//            tBody += '<a href="' + tBaseURL + '/' + oCase.Account.Id + '">' + oCase.Account.Name + '</a>';
//            tBody += ' located in ' + clsUtil.isNull(oCase.Account.BillingCity, '') + ', ' + clsUtil.isNull(oCase.Account.BillingState, '') + ' ' + clsUtil.isNull(oCase.Account.BillingCountry, '');
//            tBody += ' related to Parent Case <a href="' + tBaseURL + '/' + oCase.Id + '">' + oCase.CaseNumber + '</a>';
        tBody += '</p>';

        tBody += '<p>';
            tBody += '<table border="0">';
                tBody += '<tr>';
                    tBody += '<td>';
                        tBody += 'SAP Account Number: ';
                    tBody += '</td>';
                    tBody += '<td>';
                        tBody += clsUtil.isNull(oCase.Account.SAP_Id__c, '');
                    tBody += '</td>';
                tBody += '</tr>';
                tBody += '<tr>';
                    tBody += '<td>';
                        tBody += 'SAP Return #: ';
                    tBody += '</td>';
                    tBody += '<td>';
                        tBody += '';
						if (lstSAPReturn.size() > 0){
							tBody += String.join(lstSAPReturn,', ');
						}
                    tBody += '</td>';
                tBody += '</tr>';
            tBody += '</table>';
        tBody += '</p>';


        tBody += '<p>';
            tBody += '<b><u>Case Owner:</u></b>';
            tBody += '<br />';
			if (!String.isBlank(oCase.Owner.Name)){
	            tBody += oCase.Owner.Name;
			}
            tBody += '<br />';
			if (!String.isBlank(oCase.Owner.Phone)){
				tBody += oCase.Owner.Phone;
				tBody += '<br />';
			}
			if (!String.isBlank(oCase.Owner.Email)){
				tBody += oCase.Owner.Email;
				tBody += '<br />';
			}
        tBody += '</p>';


		tBody += '<p>';

            tBody += '<b><u>Pick-up Information:</u></b>';
            tBody += '<table>';
                tBody += '<tr>';
                    tBody += '<td style="width:20px;">&nbsp;</td>';
                    tBody += '<td>';

                        tBody += '<p>';
							tBody += '<b><u>Pick-up Address:</u></b>';
                            tBody += '<br />';
                            String tPickupAddress = clsUtil.isNull(owrCase.tPickupAddress, '');
                            if (!String.isBlank(tPickupAddress)){
                                tPickupAddress = tPickupAddress.replaceAll('\r\n', '\n');
                                List<String> lstPickupAddressData = tPickupAddress.split('\n');
                                for (String tPickupAddressLine : lstPickupAddressData){
                                    tBody += tPickupAddressLine;
                                    tBody += '<br />';
                                }
                            }
                        tBody += '</p>';

                        tBody += '<p>';
			                tBody += '<b><u>Pick-up Department:</u></b>';
							tBody += '<br />';
							tBody += clsUtil.isNull(owrCase.tPickupDepartment, '');
                        tBody += '</p>';

                        tBody += '<p>';
			                tBody += '<b><u>Pick-up Contact Name:</u></b>';
							tBody += '<br />';
							tBody += clsUtil.isNull(owrCase.tPickupContactName, '');
                        tBody += '</p>';

                        tBody += '<p>';
			                tBody += '<b><u>Pick-up Contact Number:</u></b>';
							tBody += '<br />';
			                tBody += clsUtil.isNull(owrCase.tPickupContactNumber, '');
                        tBody += '</p>';

                    tBody += '</td>';
                tBody += '</tr>';
			tBody += '</table>';

        tBody += '</p>';


		tBody += '<p>';

            tBody += '<b><u>Additional Information:</u></b>';
            tBody += '<table>';
                tBody += '<tr>';
                    tBody += '<td style="width:20px;">&nbsp;</td>';
                    tBody += '<td>';

                        tBody += '<p>';
                           String tAdditionalInformation = clsUtil.isNull(owrCase.tAdditionalInformation, '');
                            if (!String.isBlank(tAdditionalInformation)){
                                tAdditionalInformation = tAdditionalInformation.replaceAll('\r\n', '\n');
                                List<String> lstAdditionalInformationData = tAdditionalInformation.split('\n');
                                for (String tAdditionalInformationLine : lstAdditionalInformationData){
                                    tBody += tAdditionalInformationLine;
                                    tBody += '<br />';
                                }
                            }
							tBody += '<br />';
                        tBody += '</p>';

                    tBody += '</td>';
                tBody += '</tr>';
			tBody += '</table>';

        tBody += '</p>';

        tBody += '<p><hr /></p>';

        Integer iCounter = 1;
        for (wrWOSP owrWOSP : lstWRWOSP_Processing){

            tBody += '<p>';
                tBody += '<b><u>Part Information ' + iCounter + ' :</u></b>';
                tBody += '<br />';
                tBody += '<a href="' + tBaseURL + '/' + owrWOSP.oWOSP.Id + '">' + owrWOSP.oWOSP.Name + '</a>';
                tBody += '<br />';
                tBody += 'Part UPN : ' + clsUtil.isNull(owrWOSP.oWOSP.RI_Part__r.ProductCode, '');
                tBody += '<br />';
                tBody += 'Part CFN : ' + clsUtil.isNull(owrWOSP.oWOSP.RI_Part__r.CFN_Code_Text__c, '');
                tBody += '<br />';
                tBody += 'Part Name : ' + clsUtil.isNull(owrWOSP.oWOSP.RI_Part__r.Name, '');
                tBody += '<br />';
                tBody += '<br />';
				tBody += clsUtil.tGetFieldLabel('Workorder_Sparepart__c', 'RI_Part_Quantity__c') + ' : ' + clsUtil.isNull(owrWOSP.oWOSP.RI_Part_Quantity__c, '');
				tBody += '<br />';
				tBody += clsUtil.tGetFieldLabel('Workorder_Sparepart__c', 'Lot_Number__c') + ' : ' + clsUtil.isNull(owrWOSP.oWOSP.Lot_Number__c, '');
				tBody += '<br />';
				tBody += clsUtil.tGetFieldLabel('Workorder_Sparepart__c', 'SAP_Return__c') + ' : ' + clsUtil.isNull(owrWOSP.oWOSP.SAP_Return__c, '');
				tBody += '<br />';
				tBody += clsUtil.tGetFieldLabel('Workorder_Sparepart__c', 'Return_Item_Status__c') + ' : ' + clsUtil.isNull(owrWOSP.oWOSP.Return_Item_Status__c, '');
                tBody += '<br />';
				tBody += 'Additional Information : ' + clsUtil.isNull(owrWOSP.tAdditionalInformation, '');
				tBody += '<br />';
            tBody += '</p>';

            tBody += '<p><hr /></p>';

            iCounter++;

        }

        return tBody;

    }


	@TestVisible private String tGetDateTimeStamp(){

		String tDateTimeStamp = '';

		Datetime dtNow = Datetime.now();
		Integer iOffset = UserInfo.getTimezone().getOffset(dtNow);
		Datetime dtLocal = dtNow.addSeconds(iOffset/1000);

		String tYear = String.valueOf(dtLocal.yearGmt());
		String tMonth = String.valueOf(dtLocal.monthGmt());
		String tDay = String.valueOf(dtLocal.dayGmt());
		String tHour = String.valueOf(dtLocal.hourGmt());
		String tMinute = String.valueOf(dtLocal.minuteGmt());
		String tSecond = String.valueOf(dtLocal.secondGmt());

		if (tMonth.length() == 1) tMonth = '0' + tMonth;
		if (tDay.length() == 1) tDay = '0' + tDay;
		if (tHour.length() == 1) tHour = '0' + tHour;
		if (tMinute.length() == 1) tMinute = '0' + tMinute;
		if (tSecond.length() == 1) tSecond = '0' + tSecond;

		tDateTimeStamp = tYear + tMonth + tDay + '-' + tHour + tMinute + tSecond;

		return tDateTimeStamp;
	}
    //--------------------------------------------------------------------------------------------------------


    //--------------------------------------------------------------------------------------------------------
    // GETTERS & SETTERS
    //--------------------------------------------------------------------------------------------------------
	public wrCase owrCase { get; set; }                                         // Contains the data that is used on the Visual Force page [Case Data, WOSP Data, ...]
    public List<wrWOSP> lstwrWOSP { get; set; }                                 // Contains the WOSP records in a Wrapper to be able to know which one are selected or not
    public List<SelectOption> lstSO_OrgWideEmailAddress { get; private set; }   // Contains the available OrgWideEmailAddresses
    public Id id_EmailFrom { get; set; }                                        // Contains the selected From Email Id [OrgWideEmailAddress or Current User Id]
    public Boolean bShowFROM { get; private set; }

    public List<String> lstId_CC { get; set; }                                  // Contains the selected CC ID's
    public List<String> lstId_BCC { get; set; }                                 // Contains the selected BCC ID's
    public List<String> lstId_Initial_TO { get; set; }                          // Contains the Initial TO ID's on the load of the Page
    public List<String> lstId_Initial_CC { get; set; }                          // Contains the Initial CC ID's on the load of the Page
    public List<String> lstId_Initial_BCC { get; set; }                         // Contains the Initial BCC ID's on the load of the Page

    public String tAdditionalEmail_TO { get; set; }								// Contains the Additional TO Email Addresses
    public String tAdditionalEmail_CC { get; set; }								// Contains the Additional CC Email Addresses
    public String tAdditionalEmail_BCC { get; set; }							// Contains the Additional BCC Email Addresses

    public String tCountryEmail { get; set; }                                   // Contains the selected TO Email Address
    public Boolean bCountryEmail_Multiple { get; private set; }
    public Integer iCountryEmail_Size { get; set; }
    public List<String> lstCountryEmail_Selected { get; set; }                  // Contains the selected TO Email Addresses
    public List<SelectOption> lstSO_CountryEmail { get; private set; }          // Contains the available TO Email Addresses based on the Shipping Country    
    //--------------------------------------------------------------------------------------------------------



    //--------------------------------------------------------------------------------------------------------
    // ACTIONS
    //--------------------------------------------------------------------------------------------------------
    public PageReference processWOSP(){

        PageReference oPageRef;

        try{

            if (Test.isRunningTest()) clsUtil.bubbleException('processWOSP_EXCEPTION');

            Boolean bMissingRequiredFields = false;

            //-----------------------------------------------------------------------------
            // Validate required data
            //-----------------------------------------------------------------------------
            // Verify if required fields are populated
			if (String.isBlank(owrCase.tPickupAddress)){
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please provide a "Pick-up Address"'));
				bMissingRequiredFields = true;
			}

			if (String.isBlank(owrCase.tPickupDepartment)){
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please provide a "Pick-up Department"'));
				bMissingRequiredFields = true;
			}

			if (String.isBlank(owrCase.tPickupContactName)){
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please provide a "Pick-up Contact Name"'));
				bMissingRequiredFields = true;
			}


            // Verify that 1 or more WOSP records are selected
            Id id_WOSP;
            List<wrWOSP> lstWRWOSP_Processing = new List<wrWOSP>();
            for (wrWOSP owrWOSP : lstwrWOSP){
                if (owrWOSP.bSelected){
                    lstWRWOSP_Processing.add(owrWOSP);
                }
            }
            if (lstWRWOSP_Processing.size() == 0){
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please select 1 or more WOSP records to process'));
				bMissingRequiredFields = true;
            }

			if (bMissingRequiredFields) return oPageRef;


            // Verify if required fields are populated on WOSP
			for (wrWOSP oWRWOSP : lstWRWOSP_Processing){
			
				oWRWOSP.lstErrorMessage = new List<String>();
				oWRWOSP.bDisplay_ReturnItemStatusError = false;

				if (oWRWOSP.bSelected){

					if (oWRWOSP.oWOSP.RI_Part_Quantity__c == null){
						oWRWOSP.lstErrorMessage.add('Please provide a "' + clsUtil.tGetFieldLabel('Workorder_Sparepart__c', 'RI_Part_Quantity__c') + '"');
						bMissingRequiredFields = true;
					}

					if (oWRWOSP.oWOSP.Lot_Number__c == null){
						oWRWOSP.lstErrorMessage.add('Please provide a "' + clsUtil.tGetFieldLabel('Workorder_Sparepart__c', 'Lot_Number__c') + '"');
						bMissingRequiredFields = true;
					}

					if (oWRWOSP.oWOSP.SAP_Return__c == null){
						oWRWOSP.lstErrorMessage.add('Please provide a "' + clsUtil.tGetFieldLabel('Workorder_Sparepart__c', 'SAP_Return__c') + '"');
						bMissingRequiredFields = true;
					}

					if (oWRWOSP.oWOSP.Return_Item_Status__c == null){
						oWRWOSP.lstErrorMessage.add('Please provide a "' + clsUtil.tGetFieldLabel('Workorder_Sparepart__c', 'Return_Item_Status__c') + '"');
						bMissingRequiredFields = true;
					}else{
						Workorder_Sparepart__c oWOSP_Tmp = [SELECT Return_Item_Status__c FROM Workorder_Sparepart__c WHERE Id = :oWRWOSP.oWOSP.Id];

						if (oWRWOSP.tReturnItemStatus != oWOSP_Tmp.Return_Item_Status__c){
							oWRWOSP.lstErrorMessage.add('Please verify the "' + clsUtil.tGetFieldLabel('Workorder_Sparepart__c', 'Return_Item_Status__c') + '" on the WOSP');
							oWRWOSP.bDisplay_ReturnItemStatusError = true;
							bMissingRequiredFields = true;
						}
					}

				}

			}
            if (bMissingRequiredFields) return oPageRef;


            // Verify that an Email TO is selected
            if ( (String.isBlank(tCountryEmail)) && (lstCountryEmail_Selected.size() == 0) ){
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please select a TO email address'));
                return oPageRef;
            }
            //-----------------------------------------------------------------------------


            //-----------------------------------------------------------------------------
            // Send Email(s)
            //-----------------------------------------------------------------------------
            List<String> lstEmail_TO = new List<String>();
            List<String> lstEmail_CC = new List<String>();
            List<String> lstEmail_BCC = new List<String>();
            Map<Id, Contact> mapContact = new Map<Id, Contact>([SELECT Id, Email FROM Contact WHERE Id = :lstId_CC]);

            Id id_User_TO;
            if (bCountryEmail_Multiple){
                lstEmail_TO.addAll(lstCountryEmail_Selected);
            }else{
                lstEmail_TO.add(tCountryEmail);
            }

            for (String tID : lstId_CC){
                if (mapContact.containsKey(tID)){
                    lstEmail_CC.add(mapContact.get(tID).Email);
                }
            }
            lstEmail_BCC.add(UserInfo.getUserEmail());
            

			if (!String.isBlank(tAdditionalEmail_TO)) lstEmail_TO.addAll(tAdditionalEmail_TO.split(','));
			if (!String.isBlank(tAdditionalEmail_CC)) lstEmail_CC.addAll(tAdditionalEmail_CC.split(','));
			if (!String.isBlank(tAdditionalEmail_BCC)) lstEmail_BCC.addAll(tAdditionalEmail_BCC.split(','));
            
            List<Messaging.SingleEmailMessage> lstEmail = new List<Messaging.SingleEmailMessage>();

            List<EmailMessage> lstEmailMessage_Insert = new List<EmailMessage>();

            String tEmailBody_HTML = tEmailBody(lstWRWOSP_Processing);
            String tEmailBody_Text = tEmailBody_HTML.stripHtmlTags();
            String tEmailSubject = oCase.CaseNumber + ' - RTG Spare Part Return Request for ' + oCase.Account.Name + ' (' + tGetDateTimeStamp() + ')';
            Messaging.SingleEmailMessage oEmail = new Messaging.SingleEmailMessage();
                oEmail.setSubject(tEmailSubject);
                oEmail.setHtmlBody(tEmailBody_HTML);
                oEmail.setPlainTextBody(tEmailBody_Text);
                oEmail.setTargetObjectId(Userinfo.getUserId());
                oEmail.setTreatTargetObjectAsRecipient(false);
                if (lstEmail_TO.size() > 0) oEmail.setTOAddresses(lstEmail_TO);
                if (lstEmail_CC.size() > 0) oEmail.setCCAddresses(lstEmail_CC);
                if (lstEmail_BCC.size() > 0) oEMail.setBCCAddresses(lstEmail_BCC);
                oEmail.setSaveAsActivity(false);
                if ( (id_EmailFrom != null) && (id_EmailFrom != UserInfo.getUserId()) ){
                    oEmail.setOrgWideEmailAddressId(id_EmailFrom);
                }
            lstEmail.add(oEmail);

            EmailMessage oEmailMessage = new EmailMessage();
                oEmailMessage.Status = '3';
                oEmailMessage.FromAddress = Userinfo.getUserEmail();
                oEmailMessage.FromName = Userinfo.getUserName();
                oEmailMessage.ToAddress = oEmail.toAddresses[0];
                oEmailMessage.Subject = oEmail.Subject;
                oEmailMessage.TextBody = oEmail.PlainTextBody;
                oEmailMessage.HtmlBody = oEmail.HtmlBody;
                oEmailMessage.ParentId = oCase.Id;
            lstEmailMessage_Insert.add(oEmailMessage);

            List<Messaging.SendEmailResult> lstSendEmailResult = Messaging.sendEmail(lstEmail, false);

            // Clear the status of all wrWOSP records
            for (wrWOSP owrWOSP : lstwrWOSP){
                owrWOSP.tStatus = '';
                owrWOSP.tStatusImage = '/s.gif';
            }

            Boolean bSuccess = true;

            String tStatus = '';
            String tStatusImage = '';
            if (lstSendEmailResult[0].isSuccess()){
                tStatus = 'Success';
                tStatusImage = '/img/msg_icons/confirm16.png';
            }else{
                tStatus = lstSendEmailResult[0].getErrors()[0].getMessage();
                tStatusImage = '/img/msg_icons/error16.png';
                bSuccess = false;
            }

            for (wrWOSP owrWOSP : lstWRWOSP_Processing){
                owrWOSP.tStatus = tStatus;
                owrWOSP.tStatusImage = tStatusImage;
            }

            if (bSuccess){
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'The selected records are process successfully.'));
                if (lstEmailMessage_Insert.size() > 0 ) insert lstEmailMessage_Insert;
            }else{
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING, 'The selected records are processed with errors.  Please verify the status in the WOSP overview.'));
            }
            //-----------------------------------------------------------------------------


        }catch(Exception oEX){
            System.debug('Error in processWOSP on line ' + oEX.getLineNumber() + ' : ' + oEX.getMessage());
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Error in processWOSP on line ' + oEX.getLineNumber() + ' : ' + oEX.getMessage()));
        }

        return oPageRef;

    }


    public PageReference backToRecord(){

        PageReference oPageRef = new PageReference('/'+ tPara_RecordId);
            oPageRef.setRedirect(true);
        return oPageRef;

    }
    //--------------------------------------------------------------------------------------------------------


    //--------------------------------------------------------------------------------------------------------
    // HELPER CLASS
    //--------------------------------------------------------------------------------------------------------
    public class wrCase{

        public Case oCase { get;set; }

        public String tTitle { get; set; }
        public String tName { get; set; }
        public String tBackTo { get; set; }

		public String tPickupAddress { get; set; }
		public String tPickupDepartment { get; set; }
		public String tPickupContactName { get; set; }
		public String tPickupContactNumber { get; set; }
		public String tReturnOrderNumber { get; set; }

		public Integer iPickupAddressLines { get; private set; }

		public String tAdditionalInformation { get; set; }

		public String tAdditionalInformation_LotNumber { get; private set; }

		public wrCase(Case aoCase){

            oCase = aoCase;
			tTitle = 'Case';
            tName = aoCase.CaseNumber;
            tBackTo = 'Back to Case';

			tReturnOrderNumber = '';

			iPickupAddressLines = 3;

			String tAddressLine1 = clsUtil.isNull(aoCase.Account.Account_Address_Line_1__c, '');
			String tAddressLine2 = clsUtil.isNull(aoCase.Account.Account_Address_Line_2__c, '');
			String tAddressLine3 = clsUtil.isNull(aoCase.Account.Account_Address_Line_3__c, '');
			String tAddressLine4 = clsUtil.isNull(aoCase.Account.Account_Postal_Code__c, '') + ' ' + clsUtil.isNull(aoCase.Account.Account_City__c, '');
			String tAddressLine5 = clsUtil.isNull(aoCase.Account.Account_Province__c, '');
			String tAddressLine6 = clsUtil.isNull(aoCase.Account.Account_Country_vs__c, '');

			tAddressLine1 = tAddressLine1.trim();
			tAddressLine2 = tAddressLine2.trim();
			tAddressLine3 = tAddressLine3.trim();
			tAddressLine4 = tAddressLine4.trim();
			tAddressLine5 = tAddressLine5.trim();
			tAddressLine6 = tAddressLine6.trim();

			tPickupAddress = '';
			Integer iCounter = 0;
			if (!String.isBlank(tAddressLine1)) { tPickupAddress += tAddressLine1 + ' \r\n'; iCounter++; }
			if (!String.isBlank(tAddressLine2)) { tPickupAddress += tAddressLine2 + ' \r\n'; iCounter++; }
			if (!String.isBlank(tAddressLine3)) { tPickupAddress += tAddressLine3 + ' \r\n'; iCounter++; }
			if (!String.isBlank(tAddressLine4)) { tPickupAddress += tAddressLine4 + ' \r\n'; iCounter++; }
			if (!String.isBlank(tAddressLine5)) { tPickupAddress += tAddressLine5 + ' \r\n'; iCounter++; }
			if (!String.isBlank(tAddressLine6)) { tPickupAddress += tAddressLine6; iCounter++; }
			if (iCounter > 3) iPickupAddressLines = iCounter - 1;

			tPickupDepartment = aoCase.ShippingDepartment__c;
			tPickupContactName = '';
			tPickupContactNumber = '';

			tAdditionalInformation = '';

			tAdditionalInformation_LotNumber = 'If there is no lot/batch information of the defective part available, please use the serial number of the system that has been repaired (e.g. for O-arm C1234)';

		}

	}


    public class wrWOSP{

        public Workorder_Sparepart__c oWOSP { get; set; }
        public Product2 oReturnPart { get; set; }
		
		public Boolean bSelected { get; set; }
        public String tStatus { get; set; }
        public String tStatusImage { get; set; }

		public String tReturnItemStatus { get; set; }
		public List<SelectOption> lstSO_ReturnItemStatus { get; set; }
		public String tReturnItemStatus_Error { get;set; }
		public String tAdditionalInformation { get; set; }

		public List<String> lstErrorMessage { get; set; }
		public Boolean bDisplay_Error { 
			get{
				Boolean bValue = false;
				if (lstErrorMessage != null && lstErrorMessage.size() > 0) bValue = true;
				return bValue;
			}
		}

		public Boolean bDisplay_WOSP { 
			get{
				Boolean bValue = false;
				if (bSelected) bValue = true;
				return bValue; 
			}
		}

		public Boolean bDisplay_ReturnItemStatusError { get; set; }

        public wrWOSP(Workorder_Sparepart__c aoWorkorderSparepart){

			lstErrorMessage = new List<String>();
            bSelected = false; 
            oWOSP = aoWorkorderSparepart;
            tStatus = '';
            tStatusImage = '/s.gif';
            if (aoWorkorderSparepart.Order_Part_List_Price_Manual__c == null) aoWorkorderSparepart.Order_Part_List_Price_Manual__c = aoWorkorderSparepart.Order_Part_List_Price__c;

			oReturnPart = aoWorkorderSparepart.RI_Part__r;

			tReturnItemStatus = 'Ready for pick-up';
			lstSO_ReturnItemStatus = new List<SelectOption>();
				lstSO_ReturnItemStatus.add(new SelectOption('Returned Unused', 'Returned Unused'));
				lstSO_ReturnItemStatus.add(new SelectOption('Ready for pick-up', 'Ready for pick-up'));

			tReturnItemStatus_Error = 'The <b>Return Item Status</b> on the WOSP is different.  Please update it first on the WOSP : ';

			tAdditionalInformation = '';

			bDisplay_ReturnItemStatusError = false;
        }

    }

	//--------------------------------------------------------------------------------------------------------


}
//------------------------------------------------------------------------------------------------------------