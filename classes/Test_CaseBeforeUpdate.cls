@isTest
private class Test_CaseBeforeUpdate {
    
    private static Id getRecordTypeId(String devName, String objType){
        return [select Id from RecordType where DeveloperName = :devName and sObjectType = :objType and isActive = true].Id;
    }
    
    static testMethod void shouldUpdateContactData(){
                
        Account accountToInsert = new Account();
        accountToInsert.Name = 'testAccount';
        accountToInsert.NameLocal='test';
        accountToInsert.RecordTypeId = RecordTypeMedtronic.Account('Pending_Account').Id;
        insert(accountToInsert);        
        
        List<Account> accounts = new List<Account>();
        accounts.add(accountToInsert);
                               
        Contact contactToInsert = Test_TestDataUtil.createAccountContacts('', 1, accounts)[0];
        contactToInsert.FirstName = 'test';
		contactToInsert.Phone = '123';
        contactToInsert.Email = 'test@test.com';
        contactToInsert.Fax = '456';
        contactToInsert.MobilePhone = '789';
        contactToInsert.MailingPostalCode = '147';
        contactToInsert.MailingState = 'state';
        contactToInsert.MailingCity = 'city';
        contactToInsert.MailingStreet = 'street';
        contactToInsert.Account_ID__c = accountToInsert.Id;  
        insert(contactToInsert);
        
        Case caseToInsert = new Case();
        caseToInsert.RecordTypeId = RecordTypeMedtronic.Case('OMA').Id;
        caseToInsert.ContactId = contactToInsert.Id;
        caseToInsert.Requester_Type__c = 'HCP';
        caseToInsert.Priority = 'Low';
        caseToInsert.Origin ='HCP Email';
        caseToInsert.Status='New';
        caseToInsert.Subject='test';
        caseToInsert.Therapy_Picklist__c = 'TDD Other';
        caseToInsert.Type='On Label';
        caseToInsert.Case_Classification__c ='Other';
        insert(caseToInsert);
        
        caseToInsert.Status = 'Closed';
        caseToInsert.Response__c = 'test';
        caseToInsert.Response_Method__c ='Fax';
        caseToInsert.Quality_Check__c = 'N/A';
        update(caseToInsert);
        
        Case caseToAssert = [select c.Contact_Fax__c,
                                    c.Contact_Mailing_Zip__c
                             from Case c
                             where c.Id =: caseToInsert.Id
                             limit 1];
        
        System.assertEquals(contactToInsert.Fax,caseToAssert.Contact_Fax__c);
        System.assertEquals(contactToInsert.MailingPostalCode,caseToAssert.Contact_Mailing_Zip__c);
    }

    @isTest private static void test_Case_ST_Service(){

        List<Account> lstAccount = new List<Account>();
        Account oAccount = new Account();
            oAccount.Name = 'testAccount';
            oAccount.NameLocal='test';
            oAccount.RecordTypeId = clsUtil.getRecordTypeByDevName('Account', 'SAP_Account').Id;
            oAccount.SAP_Id__c = '123456';
        lstAccount.add(oAccount);
        insert lstAccount;        
        
        List<Contact> lstContact = new List<Contact>();
        Contact oContact = new Contact();
            oContact.RecordTypeId = clsUtil.getRecordTypeByDevName('Contact', 'Generic_Contact').Id;
            oContact.LastName = 'Test Contact 1';
			oContact.FirstName = 'test';
            oContact.Contact_Gender__c = 'Male';
            oContact.Contact_Primary_Specialty__c = 'Physiology';
            oContact.Contact_Department__c = 'Physiotherapy';
            oContact.AccountId = oAccount.Id;
            oContact.Affiliation_To_Account__c = 'Employee';
            oContact.Primary_Job_Title_vs__c = 'Physician';
            oContact.MailingCountry = oAccount.Account_Country_vs__c;
            oContact.Phone = '123';
            oContact.Email = 'test@test.com';
            oContact.Fax = '456';
            oContact.MobilePhone = '789';
            oContact.MailingPostalCode = '147';
            oContact.MailingState = 'state';
            oContact.MailingCity = 'city';
            oContact.MailingStreet = 'street';
            oContact.Account_ID__c = oAccount.Id;  
        lstContact.add(oContact);
        insert lstContact;
        
        Case oCase = new Case();
            oCase.RecordTypeId = clsUtil.getRecordTypeByDevName('Case', 'New_Case').Id;
            oCase.ContactId = oContact.Id;
            oCase.Requester_Type__c = 'HCP';
            oCase.Priority = 'Low';
            oCase.Origin ='HCP Email';
            oCase.Status='New';
            oCase.Subject='test';
            oCase.Therapy_Picklist__c = 'TDD Other';
            oCase.Type='On Label';
            oCase.Case_Classification__c ='Other';
        insert oCase;
    }
}