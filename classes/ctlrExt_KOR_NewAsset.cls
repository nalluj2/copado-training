/**
*   VF page         : page_KOR_NewAsset
*   Controller name : ctlrExt_KOR_NewAsset
*   Description     : this class is used as an extension in vf page "page_KOR_NewAsset" to create new asset
*   Author          : Peng Han
*   Date            : 05/03/2014
*/
public class ctlrExt_KOR_NewAsset {
    public string currentRecordID;
    public Asset asset{get;set;}
    public string selectedProductName{get;set;}
    public string departmentname{get;set;} 
    Asset assetRecord;
    public Department_Info__c dept{get;set;} 
    
    public ctlrExt_KOR_NewAsset(ApexPages.StandardController controller) {
        currentRecordID=ApexPages.currentPage().getParameters().get('id'); 
        this.asset = (Asset)controller.getRecord();  
        dept=new Department_Info__c(); 
        if(currentRecordID==null){
           List<Department_Info__c> depts =[select Name,Department__c,Account__c from Department_Info__c where id=:asset.Department_Info__c];
           if (depts!=null && depts.size()>0){
                dept=depts.get(0);
           }
           asset.Quantity=1;
           List<selectOption> lstProductOptions = getProducts();
           if (lstProductOptions != null && lstProductOptions.size() > 0) {
                asset.name = lstProductOptions[0].getLabel();
           }
        }
        else{
            assetRecord=[select Department_Info__c,product2Id from asset where id=:currentRecordID];
            if(assetRecord.Department_Info__c!=null){
                dept=[select Name,Department__c,Account__c from Department_Info__c where id=:assetRecord.Department_Info__c];
            }
            if(assetRecord.product2Id!=null){
                selectedProductName=assetRecord.product2Id;
            }
        }
        departmentname=dept.Department__c;
    }
    
    //get the list of product to diplay in the product picklist.....
    public List<selectOption> getProducts(){
        List<SelectOption> lstProductOptions=new List<selectOption>();
        set<id> recordTypeIds=new set<id>();
        recordTypeIds.add(RecordTypeMedtronic.product('MDT_Marketing_Product').id);
        recordTypeIds.add(RecordTypeMedtronic.product('Competitor_Marketing_Product').id);
        
        Set<Id> buIds = SharedMethods.getUserBusinessUnits().keySet();
        List<Product2> lstProducts=[
            Select Id,Name,Business_Unit_ID__c from Product2 where Consumable_Bool__c=:true and IsActive=:true 
            and RecordTypeId in:recordTypeIds and Business_Unit_ID__c=:buIds and Product_Group_Name__c<>'CR' order BY name];
        for(Product2 prod:lstProducts){
            lstProductOptions.add(new selectOption(prod.id,prod.name));
        }
        return lstProductOptions;
    }
    
    public pageReference saveAsset() {
        return saveRecord(false);
    }
    
    public pageReference saveAndNewAsset() {
        return saveRecord(true);
    }
    
    //autopopulate asset name with selected product name 
    public void updateAssetName(){ 
        string productName=[select name from Product2 where id=:selectedProductName].name;
        asset.Name=productName;       
    }
    
    // insert or update asset
    public pageReference saveRecord(boolean isSaveAndNew) {        
        asset.Accountid=dept.Account__c;
        asset.Department_Info__c=dept.id;
        asset.product2Id=selectedProductName;
        if (selectedProductName != null) {
            updateAssetName();
        }
        upsert asset;
        Pagereference pageRef;
        if (isSaveAndNew) {
            pageRef = new PageReference('/apex/page_KOR_NewAsset?CF00N20000003LFdr_lkid=' + dept.id + '&CF00N20000003LFdr=' + dept.Name + '&sfdc.override=1&retURL=%2F' + dept.id);
            pageRef.getParameters().put('nooverride','1');
            pageRef.setRedirect(true);
        } else {
            pageRef = new Pagereference('/'+asset.id); 
        }
        return pageRef;
    }

}