/*
 *      Description : This is the Test Class for the APEX Controller Extension ctrlExt_iPlan2AssetList
 *      Version     : 1.0
 *      Author      : Bart Caelen
 *      Date        : 201403
*/
@isTest private class Test_ctrlExt_iPlan2AssetList {

    @isTest static void test_ctrlExt_iPlan2AssetList_ALL() {
		Integer iCounter = 0;
		Boolean bTest = false;
        String tTest = '';
		
	    //---------------------------------------
        // Create Test Data
        //---------------------------------------
        clsTestData.createCompanyData();
        clsTestData.createBusinessUnitData();
        clsTestData.createSubBusinessUnitData();
        clsTestData.createCountryData();
        clsTestData.tCountry_Account = 'BELGIUM';
        clsTestData.createAccountData();
        clsTestData.createBURelatedListSetupData_Asset();
        clsTestData.createProductData();
		clsTestData.createAccountPlan2Data();

		// Asset Support Mail            
        Asset_Support_Mail__c oAssetSupportMail = new Asset_Support_Mail__c();
            oAssetSupportMail.name = clsTestData.tCountry_Account;
            oAssetSupportMail.Email__c = 'info@medtronic.com';
        insert oAssetSupportMail;
	    //---------------------------------------


	    //---------------------------------------
	    // GET TEST USER
	    //---------------------------------------
        Profile oProfile = [SELECT ID FROM Profile WHERE Name = 'System Administrator MDT' LIMIT 1];

        User oUser = new User(
            profileid = oProfile.Id            
        	, alias = 'test1'
        	, email = 'test1sfdc@testorg.medtronic.com'             
            , emailencodingkey = 'UTF-8'
            , lastname = 'test1'             
            , languagelocalekey = 'en_US'             
            , localesidkey = 'en_US'
            , timezonesidkey = 'America/Los_Angeles'             
            , CountryOR__c = clsTestData.tCountry_Account          
            , Country = clsTestData.tCountry_Account
            , username = 'test1sfdc@testorg.medtronic.com'
            , Alias_unique__c = 'test1_alias'
            , Company_Code_Text__c = 'Eur'
            , User_Business_Unit_vs__c = 'CRHF'
            , Job_Title_vs__c = 'Technical Consultant'
        );  
        insert oUser; 
        //---------------------------------------

	    //---------------------------------------
	    // Perform Testing
	    //---------------------------------------
        System.runAs(oUser){

            clsTestData.oMain_AccountPlan2 = 
                [
                    SELECT
                        Id, Account__c, Account__r.Name, Account__r.Account_Country_vs__c
                        , Sub_Business_Unit__c, Business_Unit__c, Business_Unit_Group__c
                    FROM
                        Account_Plan_2__c
                    WHERE 
                        Id = :clsTestData.oMain_AccountPlan2.Id
                ];

            PageReference oPage = new PageReference('/iPlan2AssetList?Id=' + clsTestData.oMain_AccountPlan2.Id);
            ApexPages.StandardController oSTDCTRL = new ApexPages.StandardController(clsTestData.oMain_AccountPlan2);
	        ctrlExt_iPlan2AssetList oCTRL = new ctrlExt_iPlan2AssetList(oSTDCTRL);

			oCTRL.createNewData();        	
        }
	    //---------------------------------------

    }
}