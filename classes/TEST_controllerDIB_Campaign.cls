/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TEST_controllerDIB_Campaign {
    
    private datetime currentDateTime; 
    private Date currentDate;
    private Date startDate;
    private Date endDate;
    private controllerDIB_Campaign myControllerDIB_Campaign;    
    private DIB_Campaign_Program__c campaignProgram;
    
    static testMethod void myUnitTest() {
        System.debug(' ################## ' + 'BEGIN TEST_controllerDIB_Campaign' + ' ################');
        Id myProfile = [select id from profile where name like '%DiB Sales Entry%' limit 1].Id;
        //User myUser = [Select id from User where profileId =:myProfile and IsActive=true limit 1];
        User myUser = new User(alias = 'repuser', email='usertest@absi.be', emailencodingkey='UTF-8', lastname='Testing', 
                languagelocalekey='en_US',localesidkey='en_US', profileid = myProfile, timezonesidkey='America/Los_Angeles', 
                username='usertest@absi.be');        
                
        //System.runAs(myUser) {
            System.debug('Current User: ' + UserInfo.getUserName());
            System.debug('Current Profile: ' + UserInfo.getProfileId());
            TEST_controllerDIB_Campaign myTestController = new TEST_controllerDIB_Campaign();
            myTestController.init();
            myTestController.createRecords();
            myTestController.doTests();
        //}
        
        System.debug(' ################## ' + 'END TEST_controllerDIB_Campaign' + ' ################');
    }
    
    public void init() {
        System.debug(' ################## ' + 'TEST_controllerDIB_Campaign_init' + ' ################');
        datetime currentDateTime = datetime.now();
        currentDate = currentDateTime.date();
        startDate = currentDateTime.date().addYears(-2);
        endDate = currentDateTime.date().addYears(+2);      
    }
    
    public void doTests() {
    
        List<DIB_Campaign__c> myDIB_Campaigns = new List<DIB_Campaign__c>{} ;
        List<DIB_Class_Campaign> myDIB_Class_Campaign = new List<DIB_Class_Campaign>();
        List<DIB_Campaign_Account__c> myDIB_Campaign_Account = new List<DIB_Campaign_Account__c>();
        
        DIB_Campaign__c myDIB_Campaign = new DIB_Campaign__c();
        
        String periodChosen = '12-2010';
        String accountType = 'Adult';

        pagereference pg=new pagereference('/apex/DIB_Campaign'); 
        test.setCurrentpage(pg);
                
        myControllerDIB_Campaign = new controllerDIB_Campaign();
        myControllerDIB_Campaign.Init();
        
       
        // Getters and setters
        
        myControllerDIB_Campaign.checkShowHideSubmitButton();
        myControllerDIB_Campaign.setPeriodChosen(periodChosen);
        String myTestStr = myControllerDIB_Campaign.periodChosen2; 
        //myControllerDIB_Campaign.getAccountTypes();
        //myControllerDIB_Campaign.getSelectedAccountType();
        //myControllerDIB_Campaign.setSelectedAccountType(accountType);
        myControllerDIB_Campaign.getcanShowDG();
        myControllerDIB_Campaign.getStartPeriod();
        myControllerDIB_Campaign.setStartPeriod(startDate);
        myControllerDIB_Campaign.getEndPeriod();
        myControllerDIB_Campaign.setEndPeriod(endDate);
        myControllerDIB_Campaign.getshowSubmitButton();
        myControllerDIB_Campaign.getDIB_Campaigns();
        myControllerDIB_Campaign.setDIB_Campaigns(myDIB_Campaigns);
        myControllerDIB_Campaign.getDIB_Campaign_Class();
        myControllerDIB_Campaign.setDIB_Campaign_Class(myDIB_Class_Campaign);
        myControllerDIB_Campaign.setmyExisting_DIB_Campaigns_Accounts(myDIB_Campaign_Account);
        myControllerDIB_Campaign.getMyExisting_DIB_Campaigns_Accounts();
        Integer myTestInt = myControllerDIB_Campaign.PAGE_SIZE;
        myControllerDIB_Campaign.getSelectedProgram();
        myControllerDIB_Campaign.setSelectedProgram('');
        myControllerDIB_Campaign.getsearchCriteria();
        myControllerDIB_Campaign.setsearchCriteria('');
        //myControllerDIB_Campaign.getAccountFilter();
        //myControllerDIB_Campaign.setAccountFilter('');
        myControllerDIB_Campaign.getshowError1();
        myControllerDIB_Campaign.setshowError1(true);
        myControllerDIB_Campaign.geterrorMessage1();
        myControllerDIB_Campaign.geterrorMessage2();
        
        
        myControllerDIB_Campaign.loadCampaigns();
        periodChosen = '10-0020'; 
        myControllerDIB_Campaign.setPeriodChosen(periodChosen);
        myControllerDIB_Campaign.selectedPrograms = new String[]{campaignProgram.Id};
        myControllerDIB_Campaign.loadCampaigns();  
        
        periodChosen = '12-2010'; 
        myControllerDIB_Campaign.setPeriodChosen(periodChosen);     
        this.simulateEditCampiagns();
        
        myControllerDIB_Campaign.save();
        
        myControllerDIB_Campaign.SubmitcurrentPeriod();
        myControllerDIB_Campaign.getFiscalFormat(1);
        myControllerDIB_Campaign.getFiscalFormat(11);
        
        myControllerDIB_Campaign.displayCampaigns();

        myControllerDIB_Campaign.getEmptySelectOption();        
        myControllerDIB_Campaign.getProgramNames();
        myControllerDIB_Campaign.getTerritoryFilterOptions();
        myControllerDIB_Campaign.getSegmentFilterOptions();
        
        myControllerDIB_Campaign.first();
        myControllerDIB_Campaign.last();
        myControllerDIB_Campaign.previous();
        myControllerDIB_Campaign.next();
        myControllerDIB_Campaign.changePage();
        
        Boolean TestValueB =  myControllerDIB_Campaign.hasPrevious;
        TestValueB =  myControllerDIB_Campaign.hasNext;
        Integer TestValueI =  myControllerDIB_Campaign.pageNumber;
        List<SelectOption> TestValueLS =  myControllerDIB_Campaign.pages;
      }
      
      public void simulateEditCampiagns() {
            List<DIB_Class_Campaign> myDIB_Campaign_Class;
            myDIB_Campaign_Class = myControllerDIB_Campaign.getDIB_Campaign_Class();
            Integer i = 1;
            for(DIB_Class_Campaign tempDIB_Class_Campaign:myDIB_Campaign_Class) {
                List<DIB_Campaign_Account__c> campAccToUpdate = new List<DIB_Campaign_Account__c>{}  ; 
                List<DIB_Campaign_Account__c> camp_account = tempDIB_Class_Campaign.getCamp_account();
                System.debug(' ################## ' + 'controllerDIB_Campaign_save_camp_account size: ' + camp_account.size() + ' ################');
                for (DIB_Campaign_Account__c cacc : camp_account){
                    if (math.mod(i,2)==0) {
                        cacc.isMember__c=true;  
                    }
                    else {
                        cacc.isMember__c=false;
                        cacc.SYSTEM_FIELD_End_Date__c = null;
                    }
                    i++;
                }
        }
        myControllerDIB_Campaign.setDIB_Campaign_Class(myDIB_Campaign_Class);
      }
      
       public void createRecords() {
        
            String accountType = 'Adult';
            
            // Sold To Accounts 
            List<Account> soldToAccounts    = new List<Account>{} ;
            
            // Sales Force Accounts 
            List<Account> sfaAccounts       = new List<Account>{} ;
            
            // Create a few Sold to Accounts ; 
            for (Integer i = 0; i < 10 ; i++){
                Account acc1 = new Account();
                acc1.Name = 'TEST_controllerDIB_Campaign Aatest Coverage Sold to or ship To Account 111' + i ;
                acc1.Account_Active__c = true;
                soldToAccounts.add(acc1) ;
            }
            insert soldToAccounts ;
            
            // create Sales Force Accounts : 
            for (Integer i = 0; i < 10 ; i++){
                Account acc1 = new Account();
                acc1.Name = 'TEST_controllerDIB_Campaign Aatest Coverage Sales Force Account 222' + i ;
                acc1.isSales_Force_Account__c = true ; 
                acc1.Account_Active__c = true;
                sfaAccounts.add(acc1) ;
            }
            insert sfaAccounts ;

            Department_Master__c department = new Department_Master__c();
            department.Name = 'Adult';
            insert department;
            
            List <DIB_Department__c> departments = new List<DIB_Department__c>{} ; 
            //Department
            for (Integer i = 0; i < 10 ; i++){          
                DIB_Department__c dep1 = new DIB_Department__c();
                dep1.Name = 'Test Coverage Department ' + i ;
                dep1.Account__c = sfaAccounts[i].Id;
                dep1.Active__c = true;
                departments.add(dep1);              
            }
            insert departments;
            
            campaignProgram = new DIB_Campaign_Program__c();
            campaignProgram.name = 'Campain Pro';
            insert campaignProgram;
            
            DIB_Global_Campaign__c globalCampaign = new DIB_Global_Campaign__c();
            globalCampaign.Name = 'Global Campain';
            globalCampaign.DIB_Campaign_Program__c = campaignProgram.id;
            insert globalCampaign;
            
            List<DIB_Campaign__c> myCampaigns = new List<DIB_Campaign__c>();
            DIB_Campaign__c myCampaign;
            
            myCampaign = new DIB_Campaign__c();
            myCampaign.Name = 'Aatest_Campaign1';
            myCampaign.Start_Fiscal_Month__c = '02';
            myCampaign.Start_Fiscal_Year__c = '2009';
            myCampaign.End_Fiscal_Month__c = '03';
            myCampaign.End_Fiscal_Year__c = '2050';
            myCampaign.Global_Campaign__c = globalCampaign.Id;
            myCampaign.isGlobal__c = true;
            myCampaigns.add(myCampaign);
            
            myCampaign = new DIB_Campaign__c();
            myCampaign.Name = 'Aatest_Campaign2';
            myCampaign.Start_Fiscal_Month__c = '02';
            myCampaign.Start_Fiscal_Year__c = '2009';
            myCampaign.End_Fiscal_Month__c = '03';
            myCampaign.End_Fiscal_Year__c = '2050';
            myCampaign.Global_Campaign__c = globalCampaign.Id;
            myCampaign.isGlobal__c = true;
            myCampaigns.add(myCampaign);

            myCampaign = new DIB_Campaign__c();
            myCampaign.Name = 'Aatest_Campaign2';
            myCampaign.Start_Fiscal_Month__c = '03';
            myCampaign.Start_Fiscal_Year__c = '2014';
            myCampaign.End_Fiscal_Month__c = '08';
            myCampaign.End_Fiscal_Year__c = '2024';
            myCampaign.Global_Campaign__c = globalCampaign.Id;
            myCampaign.isGlobal__c = true;
            myCampaigns.add(myCampaign);
                
            insert myCampaigns;
            
            /*DIB_Campaign__c [] ttest = [select Start_Date_FF__c,End_Date_FF__c from DIB_Campaign__c where id in :myCampaigns];
            for(DIB_Campaign__c tttest:ttest){
                System.debug(' ################## Test Values: Start Date : ' + tttest.Start_Date_FF__c + ' End date : ' + tttest.End_Date_FF__c);
            }*/
            System.debug(' ################## ' + 'TEST_controllerDIB_Campaign_createRecords myCampaigns: ' + myCampaigns + ' ################');
            
            List<DIB_Campaign_Account__c> myCampaignAccounts = new List<DIB_Campaign_Account__c>();
            DIB_Campaign_Account__c myCampaignAccount;
            
            myCampaignAccount = new DIB_Campaign_Account__c();
            myCampaignAccount.Account__c = sfaAccounts.get(0).Id;
            myCampaignAccount.Campaign__c = myCampaigns.get(0).Id;
            //myCampaignAccount.Department__c = accountType;
            myCampaignAccount.SYSTEM_FIELD_Start_Date__c = startDate;
            myCampaignAccounts.add(myCampaignAccount);

            myCampaignAccount = new DIB_Campaign_Account__c();
            myCampaignAccount.Account__c = sfaAccounts.get(0).Id;
            myCampaignAccount.Campaign__c = myCampaigns.get(1).Id;
            myCampaignAccount.Department_ID__c = departments.get(0).Id;
            myCampaignAccount.Start_Fiscal_Month__c = '01';
            myCampaignAccount.Start_Fiscal_Year__c = '2009';
            myCampaignAccount.End_Fiscal_Month__c = '01';
            myCampaignAccount.End_Fiscal_Year__c = '2020';
            //myCampaignAccount.Department__c = accountType;
            myCampaignAccount.SYSTEM_FIELD_Start_Date__c = startDate;
            myCampaignAccounts.add(myCampaignAccount);
            
            myCampaignAccount = new DIB_Campaign_Account__c();
            myCampaignAccount.Account__c = sfaAccounts.get(0).Id;
            myCampaignAccount.Campaign__c = myCampaigns.get(1).Id;
            myCampaignAccount.Department_ID__c = departments.get(0).Id;
            myCampaignAccount.Start_Fiscal_Month__c = '01';
            myCampaignAccount.Start_Fiscal_Year__c = '2019';
            myCampaignAccount.End_Fiscal_Month__c = '01';
            myCampaignAccount.End_Fiscal_Year__c = '2020';
            //myCampaignAccount.Department__c = accountType;
            myCampaignAccount.SYSTEM_FIELD_Start_Date__c = startDate.addYears(5);
            myCampaignAccounts.add(myCampaignAccount);
            
            insert myCampaignAccounts;
            
            System.debug(' ################## ' + 'TEST_controllerDIB_Campaign_createRecords myCampaignAccounts: ' + myCampaignAccounts + ' ################');
       }
}