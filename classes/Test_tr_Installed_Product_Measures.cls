@isTest
private class Test_tr_Installed_Product_Measures {

	private static testmethod void testPopulateReadBy(){
		
		User currentUser = [Select Id, Alias from User where Id = :UserInfo.getUserId()];
		
		SVMXC__Installed_Product__c instProd = new SVMXC__Installed_Product__c();
    	insert instProd;
    	
    	Installed_Product_Measures__c prodMeasure = new Installed_Product_Measures__c();
    	prodMeasure.Installed_Product__c = instProd.Id;
    	prodMeasure.Active__c = true;
    	prodMeasure.Description__c = 'Unit Test Measurement Value';
    	prodMeasure.Last_Recorded_Timestamp__c = Datetime.now().addDays(-15);
    	prodMeasure.Last_Recorded_Value_Num__c = 20;
    	//prodMeasure.New_Recorded_Value_Num__c = 35;
    	prodMeasure.SAP_Measure_Point_ID__c = '11111111';
    	prodMeasure.Type__c = 'Gauge Counter'; 
		prodMeasure.Unit_of_Measurement__c = 'EA';
		insert prodMeasure;
		
		//Insert, then no populate
		prodMeasure = [Select Id, Read_By__c from Installed_Product_Measures__c where Id = :prodMeasure.Id];
		
		System.assert(prodMeasure.Read_By__c == null);
		
		//No change of value, then no populate
		prodMeasure.New_Recorded_Value_Num__c = null;
		update prodMeasure;
				
		prodMeasure = [Select Id, Read_By__c from Installed_Product_Measures__c where Id = :prodMeasure.Id];
		
		System.assert(prodMeasure.Read_By__c == null);
		
		//Change of value, then populate
		prodMeasure.New_Recorded_Value_Num__c = 45;
		update prodMeasure;
				
		prodMeasure = [Select Id, Read_By__c from Installed_Product_Measures__c where Id = :prodMeasure.Id];
		
		System.assert(prodMeasure.Read_By__c == currentUser.alias);
	}


	private static testmethod void testsetAccountFromIP(){
		
		User currentUser = [Select Id, Alias from User where Id = :UserInfo.getUserId()];
		
		clsTestData_Account.iRecord_Account = 2;
		List<Account> lstAccount = clsTestData_Account.createAccount();

		SVMXC__Installed_Product__c oInstalledProduct = new SVMXC__Installed_Product__c();
			oInstalledProduct.SVMXC__Company__c = lstAccount[0].Id;
    	insert oInstalledProduct;
    	
    	Installed_Product_Measures__c oInstalledProductMeasure = new Installed_Product_Measures__c();
    		oInstalledProductMeasure.Installed_Product__c = oInstalledProduct.Id;
    		oInstalledProductMeasure.Active__c = true;
    		oInstalledProductMeasure.Description__c = 'Unit Test Measurement Value';
    		oInstalledProductMeasure.Last_Recorded_Timestamp__c = Datetime.now().addDays(-15);
    		oInstalledProductMeasure.Last_Recorded_Value_Num__c = 20;
    		oInstalledProductMeasure.New_Recorded_Value_Num__c = 35;
    		oInstalledProductMeasure.SAP_Measure_Point_ID__c = '11111111';
    		oInstalledProductMeasure.Type__c = 'Gauge Counter'; 
			oInstalledProductMeasure.Unit_of_Measurement__c = 'EA';
		
		Test.startTest();
		
			insert oInstalledProductMeasure;
	
			oInstalledProductMeasure = [SELECT Id, Account_from_IP__c FROM Installed_Product_Measures__c WHERE Id = :oInstalledProductMeasure.Id];
			System.assert(oInstalledProductMeasure.Account_from_IP__c == lstAccount[0].Id);

			// If you update the Installed_Product_Measures__c, the Account_from_IP__c will be populated with the SVMXC__Company__c on SVMXC__Installed_Product__c
			oInstalledProductMeasure.Account_from_IP__c = lstAccount[1].Id;
			update oInstalledProductMeasure;

		Test.stopTest();
		
		oInstalledProductMeasure = [SELECT Id, Account_from_IP__c FROM Installed_Product_Measures__c WHERE Id = :oInstalledProductMeasure.Id];
		System.assert(oInstalledProductMeasure.Account_from_IP__c == lstAccount[0].Id);

	}
	
	private static testmethod void testSendNotificationSAP(){
		
		WebServiceSetting__c counterSettings = new WebServiceSetting__c();
        counterSettings.Name = 'InstProductCounter_NotificationSAP';
        counterSettings.EndpointUrl__c = 'http://144.15.228.14:33500/ws/MdtAny_SfdcInt.SRServiceOrder.webservice.providers.ws:processServiceOrder_WSDL';
        counterSettings.XMLNS__c = 'mdt="http://msplwa107-pr.wby1-dev.medtronic.com/MdtAny_SfdcInt.SRServiceOrder.webservice.providers.ws:processServiceOrder_WSDL"';
        counterSettings.WebServiceName__c = 'mdt:webservicename';
        counterSettings.DataName__c = 'WebServiceName';
        counterSettings.Timeout__c = 60000;
        counterSettings.Username__c = 'username';
        counterSettings.Password__c = 'password';
        counterSettings.Callout_Failure_Email_Address__c = 'callout.failure@medtronic.com';
        counterSettings.Callout_Max_Retries__c = 5;
        counterSettings.Callout_Retry_Minutes__c = '60';
        counterSettings.Org_Id__c = Userinfo.getOrganizationId().left(15);
        counterSettings.Instance_Id__c = 'XXX';	
        insert counterSettings;	
		
		SVMXC__Installed_Product__c instProd = new SVMXC__Installed_Product__c();
    	insert instProd;
    	
    	Installed_Product_Measures__c prodMeasure = new Installed_Product_Measures__c();
    	prodMeasure.Installed_Product__c = instProd.Id;
    	prodMeasure.Active__c = true;
    	prodMeasure.Description__c = 'Unit Test Measurement Value';
    	prodMeasure.Last_Recorded_Timestamp__c = Datetime.now().addDays(-15);
    	prodMeasure.Last_Recorded_Value_Num__c = 20;    	
    	prodMeasure.SAP_Measure_Point_ID__c = '11111111';
    	prodMeasure.Type__c = 'Gauge Counter'; 
		prodMeasure.Unit_of_Measurement__c = 'EA';
		insert prodMeasure;
		
		List<NotificationSAPLog__c> notifications = [Select Id from NotificationSAPLog__c where Record_ID__c = :prodMeasure.Id];
		
		System.assert(notifications.size() == 0);
		
		Test.startTest();

        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new TEST_bl_NotificationSAP_Mock());  
		
		prodMeasure.New_Recorded_Value_Num__c = 45;
		update prodMeasure;
		
		notifications = [Select Id, Retries__c from NotificationSAPLog__c where Record_ID__c = :prodMeasure.Id];
		
		System.assert(notifications.size() == 1);
		System.assert(notifications[0].Retries__c == -1);
		
		Test.stopTest();	
		
		notifications = [Select Id, Retries__c from NotificationSAPLog__c where Record_ID__c = :prodMeasure.Id];
		
		System.assert(notifications.size() == 1);
		System.assert(notifications[0].Retries__c == 0);
	}
}