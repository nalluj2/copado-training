public with sharing class ctrl_CVG_Task_Template {
 	
 	public Task task {get; private set;}
    public Opportunity opp {get; private set;}
    public String contacts {get; private set;}
    
    public String urlString {
    		
    		get{
    			Support_Portal_URL__c settings = Support_Portal_URL__c.getInstance();
    			return settings.Instance_URL__c;	
    		} 
    		
    		private set;
    }
        
    public String tskId {
    	
    	get;
    	 
    	set{
    		if(value != null && value != ''){
    			
    			task = [Select Id, Subject, Description, WhatId, CreatedBy.Name, ActivityDate, Priority, (Select Relation.Name from TaskRelations where IsWhat = false) from Task where Id = :value];
    			
    			contacts = '';
    			
    			if(task.TaskRelations.size() > 0){
    				
    				List<String> contactNames = new List<String>();
    				
    				for(TaskRelation taskRel : task.TaskRelations){
    					
    					contactNames.add(taskRel.Relation.Name);
    				}
    				
    				contacts = String.join(contactNames, '; ');
    			}
    			   			
    			if(task.WhatId != null){
    				
    				opp = [Select Id, Name, Account.Name, Account_Plan__r.Name from Opportunity where Id = :task.WhatId];
    			}
    		}	    		
    	}
    }        
}