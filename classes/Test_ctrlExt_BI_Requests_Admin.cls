@isTest
private class Test_ctrlExt_BI_Requests_Admin {

    private static User currentUser = [Select Id, Email from User where Id=:UserInfo.getUserId()];
    
    private static testMethod void sfdcDispatcher(){
                       
        Create_User_Request__c userRequest = createRequest('Generic Service');
        
        ApexPages.StandardController sc = new ApexPages.StandardController(userRequest);
        ctrlExt_Support_Request_Dispatcher controller = new ctrlExt_Support_Request_Dispatcher(sc);
        
        controller.redirect();

    }

    private static testMethod void completeRequest(){
    	
    	System.runAs(currentUser){
            assignUserToAdminGroup();
        }	
        
        Create_User_Request__c userRequest = createRequest('Generic Service');
       
        userRequest.Created_User__c = UserInfo.getUserId();
        
        insert userRequest;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(userRequest);
        ctrlExt_BI_Requests_Admin controller = new ctrlExt_BI_Requests_Admin(sc);
        
        controller.assignToMe();
	        userRequest.Sub_Status__c = 'Solved (Permanently)';
    	    userRequest.Time_Spent__c = 100;

        Boolean bTest = controller.bIsUpdatable;
        bTest = controller.bIsAssigned;

        controller.completeRequest();

        System.assert(userRequest.Status__c == 'Completed');

        bTest = controller.bIsUpdatable;
        bTest = controller.bIsAssigned;

    }	

    private static testMethod void saveRequest(){
    	
    	System.runAs(currentUser){
            assignUserToAdminGroup();
        }	
        
        Create_User_Request__c userRequest = createRequest('Generic Service');
       
        userRequest.Created_User__c = UserInfo.getUserId();
        
        insert userRequest;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(userRequest);
        ctrlExt_BI_Requests_Admin controller = new ctrlExt_BI_Requests_Admin(sc);
        
        controller.assignToMe();
	        userRequest.Sub_Status__c = 'Solved (Permanently)';
    	    userRequest.Time_Spent__c = 100;
        controller.saveRequest();

    }

    private static testMethod void assignToOtherUser(){

        System.runAs(currentUser){
            assignUserToAdminGroup();
        }
                        
        Create_User_Request__c userRequest = createRequest('Generic Service');

        userRequest.Created_User__c = UserInfo.getUserId();
        insert userRequest;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(userRequest);
        ctrlExt_BI_Requests_Admin controller = new ctrlExt_BI_Requests_Admin(sc);

        System.assert(controller.isAdminUser == true);

        Id id_AdminUser;
        for (Id id_User : mapUser_Admin.keySet()){
            if (id_User != UserInfo.getUserId()){
                id_AdminUser = id_User;
                break;
            }
        }
        if (id_AdminUser != null){
            controller.tAdminUserId = id_AdminUser;
            controller.saveRequest();
        }

    }    

    private static testMethod void saveRequest_Error(){
    	
    	System.runAs(currentUser){
            assignUserToAdminGroup();
        }	
        
        Create_User_Request__c userRequest = createRequest('Generic Service');
       
        userRequest.Created_User__c = UserInfo.getUserId();

        insert userRequest;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(userRequest);
        ctrlExt_BI_Requests_Admin controller = new ctrlExt_BI_Requests_Admin(sc);
        
        controller.assignToMe();
	        userRequest.Sub_Status__c = 'Solved (Permanently)';
    	    userRequest.Time_Spent__c = 999999999;	//--> error

    	Boolean bError = false;
        try{
        	controller.saveRequest();
    	}catch(Exception oEX){
			bError = true;
    	}

    	System.assertEquals(bError, true);

    }	

    private static testMethod void rejectRequest(){
    	       
        Create_User_Request__c userRequest = createRequest('Generic Service');
        	userRequest.Description_Long__c = 'descr';                
        insert userRequest;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(userRequest);
        ctrlExt_BI_Requests_Admin controller = new ctrlExt_BI_Requests_Admin(sc);
                
        controller.reject();
        
        System.assert(userRequest.Status__c == 'Rejected');
    }


    private static Create_User_Request__c createRequest(String requestType){
        
        Create_User_Request__c userRequest = new Create_User_Request__c();          
	        userRequest.Status__c = 'New';
	        userRequest.Application__c = 'BI';
	        userRequest.Request_Type__c = requestType;          
	        userRequest.Requestor_Email__c = currentUser.Email;     
        return userRequest;

    }


    private static Map<Id, User> mapUser_Admin = new Map<Id, User>();
    private static void assignUserToAdminGroup(){
        
        List<GroupMember> memberships = [Select UserOrGroupId from GroupMember where Group.DeveloperName = 'Support_Portal_BI' AND UserOrGroupId = :UserInfo.getUserId()];
        
        if(memberships.isEmpty()){
        
            Group adminGroup = [Select Id from Group where DeveloperName = 'Support_Portal_BI'];
            
            GroupMember membership = new GroupMember();
	            membership.GroupId = adminGroup.Id;
    	        membership.UserOrGroupId = UserInfo.getUserId();
            insert membership;
        }      

        mapUser_Admin = bl_CreateUserRequest.loadAdminUsers('Support_Portal_BI');

    }

}