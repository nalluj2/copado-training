public with sharing class ctrl_massSendEmail_WOSP_RequestPQ {

	//--------------------------------------------------------------------------------------------------------
    // PRIVATE VARIABLES
    //--------------------------------------------------------------------------------------------------------
    private String tPara_RecordId;                  // Id of the processing Record [Case, ...)    
    private String tPara_InitialTO;                 // User ID's [separated by $] that will be added to the Selected TO Box by default
    private String tPara_InitialCC;                 // User ID's [separated by $] that will be added to the Selected CC Box by default
    private String tPara_InitialBCC;                // User ID's [separated by $] that will be added to the Selected BCC Box by default
    private String tPara_OrgWideEmailAddressId;     // OrgWideEmailAddress ID's separated by $
    private String tPara_AdditionalEmailTO;         // Email Addresses [separated by $] that will be added to the TO of the email that will be send
    private String tPara_AdditionalEmailCC;         // Email Addresses [separated by $] that will be added to the CC of the email that will be send
    private String tPara_AdditionalEmailBCC;        // Email Addresses [separated by $] that will be added to the BCC of the email that will be send
	private String tEmailSandboxSufix;				// The sufix that will be added to email addresses when on a sandbox

    private List<String> lstOrgWideEmailAddressId;  // Available OrgWideEmailAddress ID's
    private Map<Id, OrgWideEmailAddress> mapOrgWideEmailAddress = new Map<Id, OrgWideEmailAddress>();
    @TestVisible private Map<String, List<String>> mapCountry_EmailAddresses = new Map<String, List<String>>();
    @TestVisible private Map<String, List<String>> mapEmailAddresses_Country = new Map<String, List<String>>();
    @TestVisible private Map<String, List<String>> mapCountry_EmailAddresses_IS = new Map<String, List<String>>();

    private Case oCase;
    //--------------------------------------------------------------------------------------------------------


    //--------------------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    //--------------------------------------------------------------------------------------------------------
    public ctrl_massSendEmail_WOSP_RequestPQ() {

        // Read URL Parameters
        Map<String, String> mapURLParameter = ApexPages.currentPage().getParameters();
            tPara_RecordId = mapURLParameter.get('id');            
			tPara_AdditionalEmailTO = mapURLParameter.get('additionalemailto');			

        // Initialize Data
        initialize();
        
    }
    //--------------------------------------------------------------------------------------------------------


    //--------------------------------------------------------------------------------------------------------
    // PRIVATE METHODS
    //--------------------------------------------------------------------------------------------------------
    private void initialize(){

	    tEmailSandboxSufix = '';
        if (clsUtil.bRunningInASandbox()) tEmailSandboxSufix = '.test';

        // Load Data
        oCase =
            [
                SELECT 
                    Id, CaseNumber, Additional_Notes_for_Customer_Care__c, Asset.hasActiveContract__c, Asset.Customer_Warranty_End_Date__c, Asset.Service_Level__c
                    , OwnerId, Owner.Name, Owner.Phone, Owner.Email
                    , Account.Id, Account.Name, Account.SAP_Id__c, Account.BillingStreet, Account.BillingPostalCode, Account.BillingCity, Account.BillingState, Account.BillingCountry
                FROM 
                    Case 
                WHERE 
                    Id = :tPara_RecordId
            ];

        owrData = new wrData();
            owrData.oCase = new Case();
            owrData.tTitle = 'Case';
            owrData.tName = oCase.CaseNumber;
            owrData.tBackTo = 'Back to Case';
            owrData.oCase.Additional_Notes_for_Customer_Care__c = '';            
            if (oCase.Asset.hasActiveContract__c){
                owrData.tAssetHasActiveContract = 'Yes';
            }else{
                owrData.tAssetHasActiveContract = 'No';
            }

            owrData.oCase.Asset = oCase.Asset;
            owrData.oCase.Account = oCase.Account;

        // Initialize variables
        lstId_CC = new List<String>();
        lstId_BCC = new List<String>();
        lstId_Initial_TO = new List<String>();
        lstId_Initial_CC = new List<String>();
        lstId_Initial_BCC = new List<String>();
        lstOrgWideEmailAddressId = new List<String>();


        // Set the Initial values for the TO, CC and BCC box     
		tAdditionalEmail_TO = '';
        if (!String.isBlank(tPara_AdditionalEmailTO)){
            List<String> lstValue = tPara_AdditionalEmailTO.split('\\$');
			for (String tEmail : lstValue){
				if (!String.isBlank(tAdditionalEmail_TO)) tAdditionalEmail_TO += ',';
				tAdditionalEmail_TO += tEmail + tEmailSandboxSufix;
			}
        }
		                            
        loadData_WOSP();
        
        lstwrServiceProducts = new List<wrServiceProduct>();
        
        // Load OrgWideEmailAddress
        loadOrgWideEmailAddress();

        // Load Country Email Address
        loadCountryEmailAddresses();

        // Get the TO Email Addresses
        tCountryEmail = '';
        lstCountryEmail_Selected = new List<String>();
        iCountryEmail_Size = 1;
        bCountryEmail_Multiple = false;
        lstSO_CountryEmail = populateCountryEmail();
        populateCountryEmail_IS();
    }


    private void loadCountryEmailAddresses(){

        mapCountry_EmailAddresses = new Map<String, List<String>>();
        mapEmailAddresses_Country = new Map<String, List<String>>();

        List<DIB_Country__c> lstCountry = [SELECT Name, Email_Service_Repair__c, Email_Inside_Sales__c FROM DIB_Country__c WHERE Email_Service_Repair__c != null OR Email_Inside_Sales__c != null];
        for (DIB_Country__c oCountry : lstCountry){
			
			//Service & Repair
			if(oCountry.Email_Service_Repair__c != null && oCountry.Email_Service_Repair__c != ''){
	            
	            List<String> lstEmail_Tmp = oCountry.Email_Service_Repair__c.split(';');
	            
	            List<String> lstEmail = new List<String>();
	            for (String tEmail : lstEmail_Tmp){
	                
	                tEmail += tEmailSandboxSufix;
	                lstEmail.add(tEmail);
	                
	                List<String> emailCountries = mapEmailAddresses_Country.get(tEmail);
	                if(emailCountries == null){
	                	emailCountries = new List<String>();
	                	mapEmailAddresses_Country.put(tEmail, emailCountries);
	                }
	                emailCountries.add(oCountry.Name.toUpperCase());
	            }
	            
	            mapCountry_EmailAddresses.put(oCountry.Name.toUpperCase(), lstEmail);
			}
            
            //Inside Sales
            if(oCountry.Email_Inside_Sales__c != null && oCountry.Email_Inside_Sales__c != ''){
	            
	            List<String> lstEmail_IS_Tmp = oCountry.Email_Inside_Sales__c.split(';');
	            List<String> lstEmail_IS = new List<String>();
	            for (String tEmail : lstEmail_IS_Tmp){
	                tEmail += tEmailSandboxSufix;
	                lstEmail_IS.add(tEmail);
	            }
	            
	            mapCountry_EmailAddresses_IS.put(oCountry.Name.toUpperCase(), lstEmail_IS);
            }
        }
    }


    @TestVisible private List<SelectOption> populateCountryEmail(){

        List<SelectOption> lstSO_Tmp = new List<SelectOption>();
        String tBillingCountry = clsUtil.isNull(owrData.oCase.Account.BillingCountry, '').toUpperCase();

        lstCountryEmail_Selected = new List<String>();

        if ( (tBillingCountry == '') || (!mapCountry_EmailAddresses.containsKey(tBillingCountry)) ){

            lstSO_Tmp.add(new SelectOption('', ' -- NONE -- '));
    
            for (String tCountry : mapCountry_EmailAddresses.keySet()){

                List<String> lstEmail = mapCountry_EmailAddresses.get(tCountry);

                for (String tEmail : lstEmail){
                    lstSO_Tmp.add(new SelectOption(tEmail, tEmail + ' (' + tCountry + ')'));
                }
            }

            bCountryEmail_Multiple = false;

        }else{

            List<String> lstEmail = mapCountry_EmailAddresses.get(tBillingCountry);

            for (String tEmail : lstEmail){
                lstSO_Tmp.add(new SelectOption(tEmail, tEmail + ' (' + tBillingCountry + ')'));
                lstCountryEmail_Selected.add(tEmail); 
            }

            iCountryEmail_Size = lstCountryEmail_Selected.size();
            if (iCountryEmail_Size == 0) iCountryEmail_Size = 1;

            bCountryEmail_Multiple = false;
            if (iCountryEmail_Size > 1) bCountryEmail_Multiple = true;

        }

        return lstSO_Tmp;

    }
    
    public void populateCountryEmail_IS(){
        
        String tBillingCountry = clsUtil.isNull(owrData.oCase.Account.BillingCountry, '').toUpperCase();
		
		System.debug('Selected Country: ' + tCountryEmail);
		
		if(tBillingCountry == '' && tCountryEmail != null && tCountryEmail != ''){
			
			List<String> emailCountries = mapEmailAddresses_Country.get(tCountryEmail);
						
			tBillingCountry = emailCountries[0];
		}
			
        lstCountryEmailIS_Selected = new List<String>();
		System.debug('tBillingCountry: ' + tBillingCountry);
        if(tBillingCountry != '' && mapCountry_EmailAddresses_IS.containsKey(tBillingCountry)){
           
            List<String> lstEmail = mapCountry_EmailAddresses_IS.get(tBillingCountry);

            for (String tEmail : lstEmail){
                
                lstCountryEmailIS_Selected.add(tEmail); 
            }
        }
        
        System.debug('lstCountryEmailIS_Selected: ' + String.join(lstCountryEmailIS_Selected, ','));
    }


    private void loadData_WOSP(){

        lstId_BCC.add(UserInfo.getUserId());

        lstwrWOSP = new List<wrWOSP>();

        String tSOQL = '';

        List<Workorder_Sparepart__c> lstWorkorderSparepart = [SELECT Id, Name, Order_Quantity__c, Special_Notes__c, Order_Part__c, Order_Part__r.Id, Order_Part__r.Name, Order_Part__r.ProductCode, 
        														Order_Part__r.CFN_Code_Text__c, Order_Part__r.Description, Order_Part_List_Price__c, Order_Part_List_Price_Manual__c, CurrencyIsoCode
        														FROM Workorder_Sparepart__c
        														WHERE Case__c = :oCase.Id];

        for (Workorder_Sparepart__c oWorkorderSparepart : lstWorkorderSparepart){
            lstwrWOSP.add(new wrWOSP(oWorkorderSparepart));
        }        
    }


    private void loadOrgWideEmailAddress(){

        lstSO_OrgWideEmailAddress = new List<SelectOption>();

//        lstSO_OrgWideEmailAddress.add(new SelectOption('', ' -- NONE -- '));
        lstSO_OrgWideEmailAddress.add(new SelectOption(UserInfo.getUserId(), '"' + UserInfo.getFirstName() + ' ' + UserInfo.getLastName() + '" <' + UserInfo.getUserEmail() + '>'));

        if (lstOrgWideEmailAddressId.size() > 0){
            mapOrgWideEmailAddress = new Map<Id, OrgWideEmailAddress>([SELECT Id, DisplayName, Address FROM OrgWideEmailAddress WHERE Id = :lstOrgWideEmailAddressId ORDER BY DisplayName]);

            for (OrgWideEmailAddress oOrgWideEmailAddress : mapOrgWideEmailAddress.values()){
                lstSO_OrgWideEmailAddress.add(new SelectOption(oOrgWideEmailAddress.Id, '"' + oOrgWideEmailAddress.DisplayName + '" <' + oOrgWideEmailAddress.Address + '>'));
            }

        }

        id_EmailFrom = UserInfo.getUserId();
    }


    private Boolean saveRecord(){

        Boolean bDoUpdate = false;
       
        if (oCase.Additional_Notes_for_Customer_Care__c != owrData.oCase.Additional_Notes_for_Customer_Care__c){
            oCase.Additional_Notes_for_Customer_Care__c = owrData.oCase.Additional_Notes_for_Customer_Care__c;
            bDoUpdate = true;
        }

        System.debug('**BC** save - bDoUpdate : ' + bDoUpdate);
        if (bDoUpdate) update oCase;  
		
		List<Workorder_Sparepart__c> selectedWOSP = new List<Workorder_Sparepart__c>();
		
		for(wrWOSP owrWOSP : lstwrWOSP){
			
			if (owrWOSP.bSelected){
                selectedWOSP.add(owrWOSP.oWOSP);
            }
		}
				
		if(selectedWOSP.isEmpty() == false) update selectedWOSP;
		
        return bDoUpdate;      

    }

    @TestVisible private String tEmailBody(List<wrWOSP> lstWRWOSP_Processing){

        String tBody = '';

        String tBaseURL = URL.getSalesforceBaseUrl().toExternalForm();

        tBody += '<p>';
            tBody += 'Please create a PQ for ';
            tBody += '<a href="' + tBaseURL + '/' + oCase.Account.Id + '">' + oCase.Account.Name + '</a>';
            tBody += ' located in ' + clsUtil.isNull(oCase.Account.BillingCity, '') + ', ' + clsUtil.isNull(oCase.Account.BillingState, '') + ' ' + clsUtil.isNull(oCase.Account.BillingCountry, '');
            tBody += ' related to Parent Case <a href="' + tBaseURL + '/' + oCase.Id + '">' + oCase.CaseNumber + '</a>';
        tBody += '</p>';

        tBody += '<p>';
            tBody += '<table border="0">';
                tBody += '<tr>';
                    tBody += '<td>';
                        tBody += '<b>SAP Account Number: </b>';
                    tBody += '</td>';
                    tBody += '<td>';
                        tBody += clsUtil.isNull(oCase.Account.SAP_Id__c, '');
                    tBody += '</td>';
                tBody += '</tr>';
            tBody += '</table>';
        tBody += '</p>';


        tBody += '<p>';
            tBody += '<b><u>Case Owner:</u></b>';
            tBody += '<br />';
            tBody += clsUtil.isNull(oCase.Owner.Name, '');
            tBody += '<br />';
            tBody += clsUtil.isNull(oCase.Owner.Phone, '');
            tBody += '<br />';
            tBody += clsUtil.isNull(oCase.Owner.Email, '');
            tBody += '<br />';
        tBody += '</p>';
                
        tBody += '<p>';
            tBody += '<b><u>Shared Part Information:</u></b>';

            tBody += '<table>';
                tBody += '<tr>';
                    tBody += '<td style="width:20px;">&nbsp;</td>';
                    tBody += '<td>';

                        tBody += '<p>';
                            tBody += '<b><u>Customer Warranty End Date:</u></b>';
                            tBody += '<br />';
                            if (clsUtil.isNull(oCase.Asset.Customer_Warranty_End_Date__c, '') != ''){
                                tBody += oCase.Asset.Customer_Warranty_End_Date__c.format();
                            }
                        tBody += '</p>';


                        tBody += '<p>';
                            tBody += '<b><u>Asset Active Contract?:</u></b>';
                            tBody += '<br />';
                            if (oCase.Asset.hasActiveContract__c){
                                tBody += 'Yes';
                            }else{
                                tBody += 'No';
                            }
                        tBody += '</p>';

                        tBody += '<p>';
                            tBody += '<b><u>Asset Service Level:</u></b>';
                            tBody += '<br />';
                            tBody += clsUtil.isNull(oCase.Asset.Service_Level__c, '');
                        tBody += '</p>';
                        
                        tBody += '<p>';
                            tBody += '<b><u>Additional Notes for Customer Care:</u></b>';
                            tBody += '<br />';
                            String tAdditionalNotes = clsUtil.isNull(oCase.Additional_Notes_for_Customer_Care__c, '');
                            if (!String.isBlank(tAdditionalNotes)){
                                tAdditionalNotes = tAdditionalNotes.replaceAll('\r\n', '\n');
                                List<String> lstAdditionalNotesData = tAdditionalNotes.split('\n');
                                for (String tAdditionalNotesDataLine : lstAdditionalNotesData){
                                    tBody += tAdditionalNotesDataLine;
                                    tBody += '<br />';
                                }
                            }
                            tBody += '<br />';
                        tBody += '</p>';
                        
                    tBody += '</td>';
                tBody += '</tr>';
            tBody += '</table>';

        tBody += '</p>';


        tBody += '<p><hr /></p>';
		
		//Spareparts
        Integer iCounter = 1;
        for (wrWOSP owrWOSP : lstWRWOSP_Processing){

            tBody += '<p>';
                tBody += '<b><u>Part Information ' + iCounter + ':</u></b>';
                tBody += '<br />';
                tBody += '<a href="' + tBaseURL + '/' + owrWOSP.oWOSP.Id + '">' + owrWOSP.oWOSP.Name + '</a>';
                tBody += '<br />';
                tBody += 'Part UPN: ' + clsUtil.isNull(owrWOSP.oWOSP.Order_Part__r.ProductCode, '');
                tBody += '<br />';
                tBody += 'Part CFN: ' + clsUtil.isNull(owrWOSP.oWOSP.Order_Part__r.CFN_Code_Text__c, '');
                tBody += '<br />';
                tBody += 'Part Quantity: ' + clsUtil.isNull(owrWOSP.oWOSP.Order_Quantity__c, '');
                tBody += '<br />';
                tBody += 'Part Name: ' + clsUtil.isNull(owrWOSP.oWOSP.Order_Part__r.Name, '');
                tBody += '<br />';
                tBody += 'Part Description: ' + clsUtil.isNull(owrWOSP.oWOSP.Order_Part__r.Description, '');
                tBody += '<br />';
                tBody += 'Selling Price: ' + clsUtil.isNull(owrWOSP.oWOSP.CurrencyIsoCode, '') + ' ' + clsUtil.isNull(owrWOSP.oWOSP.Order_Part_List_Price_Manual__c, '');
                tBody += '<br />';
            tBody += '</p>';

            tBody += '<p>';
                tBody += '<b><u>Parts Return Notes:</u></b>';
                tBody += '<br />';                
                if (!String.isBlank(owrWOSP.oWOSP.Special_Notes__c)){
                	tBody += clsUtil.isNull(owrWOSP.oWOSP.Special_Notes__c, '');
                	tBody += '<br />';
                }
                tBody += '<br />';
            tBody += '</p>';

            tBody += '<p><hr /></p>';

            iCounter++;

        }
        
        //Service Products
        if(lstwrServiceProducts.size() > 0){
	        
	        Set<Id> serviceProductIds = new Set<Id>();
	        
	        for(wrServiceProduct owrService : lstwrServiceProducts){
				
				if(String.isEmpty(owrService.tProductId) == false) serviceProductIds.add(owrService.tProductId);
			}
			
			Map<Id, Product2> productMap = new Map<Id, Product2>([Select Id, Name, CFN_Code_Text__c, ProductCode, Description from Product2 where Id IN :serviceProductIds]);
	        
	        iCounter = 1;
	        for(wrServiceProduct owrService : lstwrServiceProducts){
	
				if(String.isEmpty(owrService.tProductId) == false){
	
					Product2 serviceProduct = productMap.get(owrService.tProductId);
					
		            tBody += '<p>';
		                tBody += '<b><u>Service Information ' + iCounter + ':</u></b>';	                
		                tBody += '<br />';
		                tBody += 'Service CFN: ' + clsUtil.isNull(serviceProduct.CFN_Code_Text__c, '');
		                tBody += '<br />';	                
		                tBody += 'Service Name: ' + clsUtil.isNull(serviceProduct.Name, '');
		                tBody += '<br />';
		                tBody += 'Service Description: ' + clsUtil.isNull(serviceProduct.Description, '');
		                tBody += '<br /><br />';	                	               
		            tBody += '</p>';
		            	
		            tBody += '<p><hr /></p>';
		
		            iCounter++;
				}
	        }
        }		

        return tBody;

    }

	@TestVisible private String tGetDateTimeStamp(){

		String tDateTimeStamp = '';

		Datetime dtNow = Datetime.now();
		Integer iOffset = UserInfo.getTimezone().getOffset(dtNow);
		Datetime dtLocal = dtNow.addSeconds(iOffset/1000);

		String tYear = String.valueOf(dtLocal.yearGmt());
		String tMonth = String.valueOf(dtLocal.monthGmt());
		String tDay = String.valueOf(dtLocal.dayGmt());
		String tHour = String.valueOf(dtLocal.hourGmt());
		String tMinute = String.valueOf(dtLocal.minuteGmt());
		String tSecond = String.valueOf(dtLocal.secondGmt());

		if (tMonth.length() == 1) tMonth = '0' + tMonth;
		if (tDay.length() == 1) tDay = '0' + tDay;
		if (tHour.length() == 1) tHour = '0' + tHour;
		if (tMinute.length() == 1) tMinute = '0' + tMinute;
		if (tSecond.length() == 1) tSecond = '0' + tSecond;

		tDateTimeStamp = tYear + tMonth + tDay + '-' + tHour + tMinute + tSecond;

		return tDateTimeStamp;
	}
    //--------------------------------------------------------------------------------------------------------


    //--------------------------------------------------------------------------------------------------------
    // GETTERS & SETTERS
    //--------------------------------------------------------------------------------------------------------
    public wrData owrData { get; set; }                                         // Contains the data that is used on the Visual Force page [Name, Shipping Address, ...]
    public List<wrWOSP> lstwrWOSP { get; set; }                                 // Contains the WOSP records in a Wrapper to be able to know which one are selected or not
    public List<wrServiceProduct> lstwrServiceProducts { get; set; }            // Contains the Service Product records in a Wrapper    
    public List<SelectOption> lstSO_OrgWideEmailAddress { get; private set; }   // Contains the available OrgWideEmailAddresses
    public Id id_EmailFrom { get; set; }                                        // Contains the selected From Email Id [OrgWideEmailAddress or Current User Id]    
    
    public List<String> lstId_CC { get; set; }                                  // Contains the selected CC ID's
    public List<String> lstId_BCC { get; set; }                                 // Contains the selected BCC ID's
    public List<String> lstId_Initial_TO { get; set; }                          // Contains the Initial TO ID's on the load of the Page
    public List<String> lstId_Initial_CC { get; set; }                          // Contains the Initial CC ID's on the load of the Page
    public List<String> lstId_Initial_BCC { get; set; }                         // Contains the Initial BCC ID's on the load of the Page

    public String tAdditionalEmail_TO { get; set; }								// Contains the Additional TO Email Addresses
    public String tAdditionalEmail_CC { get; set; }								// Contains the Additional CC Email Addresses
    public String tAdditionalEmail_BCC { get; set; }							// Contains the Additional BCC Email Addresses

    public String tCountryEmail { get; set; }                                   // Contains the selected TO Email Address
    public Boolean bCountryEmail_Multiple { get; private set; }
    public Integer iCountryEmail_Size { get; set; }
    public List<String> lstCountryEmail_Selected { get; set; }                  // Contains the selected TO Email Addresses
    public List<String> lstCountryEmailIS_Selected { get; set; }				// Contains the Inside Sales CC Email Addresses
    public List<SelectOption> lstSO_CountryEmail { get; private set; }          // Contains the available TO Email Addresses based on the Shipping Country    
    //--------------------------------------------------------------------------------------------------------


    //--------------------------------------------------------------------------------------------------------
    // ACTIONS
    //--------------------------------------------------------------------------------------------------------    
    public PageReference saveData(){

        PageReference oPageRef;

        try{

            if (Test.isRunningTest()) clsUtil.bubbleException('saveData_EXCEPTION');

            Boolean bIsUpdated = saveRecord();

            // Get the TO Email Addresses
            if (bIsUpdated){
            	
            	lstSO_CountryEmail = populateCountryEmail();
            	populateCountryEmail_IS();
            }


        }catch(Exception oEX){
            System.debug('Error in saveData on line ' + oEX.getLineNumber() + ' : ' + oEX.getMessage());
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, oEX.getMessage()));
        }

        return oPageRef;

    }


    public PageReference processWOSP(){

        PageReference oPageRef;

        try{

            if (Test.isRunningTest()) clsUtil.bubbleException('processWOSP_EXCEPTION');

            //-----------------------------------------------------------------------------
            // Validate required data
            //-----------------------------------------------------------------------------            

            // Verify that 1 or more WOSP records are selected
            Id id_WOSP;
            List<wrWOSP> lstWRWOSP_Processing = new List<wrWOSP>();
            for (wrWOSP owrWOSP : lstwrWOSP){
                if (owrWOSP.bSelected){
                    lstWRWOSP_Processing.add(owrWOSP);
                }
            }
            if (lstWRWOSP_Processing.size() == 0){
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please select 1 or more WOSP records to process'));
                return oPageRef;
            }

            // Verify that an Email TO is selected
            if ( (String.isBlank(tCountryEmail)) && (lstCountryEmail_Selected.size() == 0) ){
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please select a TO email address'));
                return oPageRef;
            }
            //-----------------------------------------------------------------------------


            //-----------------------------------------------------------------------------
            // Save Shipping Address Data
            //-----------------------------------------------------------------------------
            saveRecord();
            //-----------------------------------------------------------------------------


            //-----------------------------------------------------------------------------
            // Send Email(s)
            //-----------------------------------------------------------------------------
            List<String> lstEmail_TO = new List<String>();
            List<String> lstEmail_CC = new List<String>();
            List<String> lstEmail_BCC = new List<String>();
            Map<Id, Contact> mapContact = new Map<Id, Contact>([SELECT Id, Email FROM Contact WHERE Id = :lstId_CC]);

            Id id_User_TO;
            if (bCountryEmail_Multiple){
                lstEmail_TO.addAll(lstCountryEmail_Selected);
            }else{
                lstEmail_TO.add(tCountryEmail);
            }
            
            if(lstCountryEmailIS_Selected.size() > 0) lstEmail_CC.addAll(lstCountryEmailIS_Selected);
            
            for (String tID : lstId_CC){
                if (mapContact.containsKey(tID)){
                    lstEmail_CC.add(mapContact.get(tID).Email);
                }
            }
            lstEmail_BCC.add(UserInfo.getUserEmail());

			if (!String.isBlank(tAdditionalEmail_TO)) lstEmail_TO.addAll(tAdditionalEmail_TO.split(','));
			if (!String.isBlank(tAdditionalEmail_CC)) lstEmail_CC.addAll(tAdditionalEmail_CC.split(','));
			if (!String.isBlank(tAdditionalEmail_BCC)) lstEmail_BCC.addAll(tAdditionalEmail_BCC.split(','));
            
            List<Messaging.SingleEmailMessage> lstEmail = new List<Messaging.SingleEmailMessage>();

            List<EmailMessage> lstEmailMessage_Insert = new List<EmailMessage>();
            
            String tEmailBody_HTML = tEmailBody(lstWRWOSP_Processing);
            String tEmailBody_Text = tEmailBody_HTML.stripHtmlTags();
            String tEmailSubject = 'Please create a PQ for ' + oCase.CaseNumber + ' - MNAV Spare Part Order for ' + oCase.Account.Name + ' (' + tGetDateTimeStamp() + ')';
            Messaging.SingleEmailMessage oEmail = new Messaging.SingleEmailMessage();
                oEmail.setSubject(tEmailSubject);
                oEmail.setHtmlBody(tEmailBody_HTML);
                oEmail.setPlainTextBody(tEmailBody_Text);
                oEmail.setTargetObjectId(Userinfo.getUserId());
                oEmail.setTreatTargetObjectAsRecipient(false);
                if (lstEmail_TO.size() > 0) oEmail.setTOAddresses(lstEmail_TO);
                if (lstEmail_CC.size() > 0) oEmail.setCCAddresses(lstEmail_CC);
                if (lstEmail_BCC.size() > 0) oEMail.setBCCAddresses(lstEmail_BCC);
                oEmail.setSaveAsActivity(false);
                if ( (id_EmailFrom != null) && (id_EmailFrom != UserInfo.getUserId()) ){
                    oEmail.setOrgWideEmailAddressId(id_EmailFrom);
                }
            lstEmail.add(oEmail);
			
            EmailMessage oEmailMessage = new EmailMessage();
                oEmailMessage.Status = '3';
                oEmailMessage.FromAddress = Userinfo.getUserEmail();
                oEmailMessage.FromName = Userinfo.getUserName();
                oEmailMessage.ToAddress = oEmail.toAddresses[0];
                oEmailMessage.Subject = oEmail.Subject;
                oEmailMessage.TextBody = oEmail.PlainTextBody;
                oEmailMessage.HtmlBody = oEmail.HtmlBody;
                oEmailMessage.ParentId = oCase.Id;
            lstEmailMessage_Insert.add(oEmailMessage);
            
            List<Messaging.SendEmailResult> lstSendEmailResult = Messaging.sendEmail(lstEmail, false);

            // Clear the status of all wrWOSP records
            for (wrWOSP owrWOSP : lstwrWOSP){
                owrWOSP.tStatus = '';
                owrWOSP.tStatusImage = '/s.gif';
            }

            Boolean bSuccess = true;
            
            String tStatus = '';
            String tStatusImage = '';
            if (lstSendEmailResult[0].isSuccess()){
                tStatus = 'Success';
                tStatusImage = '/img/msg_icons/confirm16.png';
            }else{
                tStatus = lstSendEmailResult[0].getErrors()[0].getMessage();
                tStatusImage = '/img/msg_icons/error16.png';
                bSuccess = false;
            }

            for (wrWOSP owrWOSP : lstWRWOSP_Processing){
                owrWOSP.tStatus = tStatus;
                owrWOSP.tStatusImage = tStatusImage;
            }           

            if (bSuccess){
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'The selected records are process successfully.'));
                if (lstEmailMessage_Insert.size() > 0 ) insert lstEmailMessage_Insert;
            }else{
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING, 'The selected records are processed with errors.  Please verify the status in the WOSP overview.'));
            }
            //-----------------------------------------------------------------------------

        }catch(Exception oEX){
            System.debug('Error in processWOSP on line ' + oEX.getLineNumber() + ' : ' + oEX.getMessage());
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, oEX.getMessage()));
        }

        return oPageRef;

    }
    
    public void addServiceProduct(){
    	
    	lstwrServiceProducts.add(new wrServiceProduct());
    }
    
    public Integer rowToRemove {get; set;}
    
    public void deleteServiceProduct(){
    	
    	lstwrServiceProducts.remove(rowToRemove);
    }


    public PageReference backToRecord(){

        PageReference oPageRef = new PageReference('/'+ tPara_RecordId);
            oPageRef.setRedirect(true);
        return oPageRef;

    }

    //--------------------------------------------------------------------------------------------------------


    //--------------------------------------------------------------------------------------------------------
    // HELPER CLASS
    //--------------------------------------------------------------------------------------------------------
    public class wrData{

        public String tTitle { get; set; }
        public String tName { get; set; }
        public String tBackTo { get; set; }
        public String tAssetHasActiveContract { get; set; }
        public Case oCase { get;set; }

        public wrData(){

            tTitle = '';
            tName = '';
            tBackTo = '';
            tAssetHasActiveContract = '';

        }

    }

    public class wrWOSP{

        public Workorder_Sparepart__c oWOSP { get; set; }
        public Boolean bSelected { get; set; }
        public String tStatus { get; set; }
        public String tStatusImage { get; set; }

        public wrWOSP(Workorder_Sparepart__c oWorkorderSparepart){
            bSelected = false; 
            oWOSP = oWorkorderSparepart;
            tStatus = '';
            tStatusImage = '/s.gif';
            if(oWorkorderSparepart.Order_Part_List_Price_Manual__c == null) oWorkorderSparepart.Order_Part_List_Price_Manual__c = oWorkorderSparepart.Order_Part_List_Price__c;
        }

    }
    
    public class wrServiceProduct{

        public Id tProductId { get; set; }
        public String tProductName { get; set; }
        public Decimal dRecommendedPrice { get; set; }
        public String tCurrency{ get; set; }
        public Decimal dPrice { get; set; }        
    }
    //--------------------------------------------------------------------------------------------------------    
}