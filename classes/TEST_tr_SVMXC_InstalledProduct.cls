//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 20-01-2016
//  Description      : APEX Test Class to test the logic in tr_SVMXC_InstalledProduct
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_tr_SVMXC_InstalledProduct {

    //----------------------------------------------------------------------------------------
    // CREATE TEST DATA
    //----------------------------------------------------------------------------------------
    @testSetup static void createTestData() {

        // Create Custom Setting Data
        clsTestData.createCustomSettingData(true);

        // Create SVMXC__Installed_Product__c Data
        clsTestData.createSVMXCInstalledProductData(true);

    }
    //----------------------------------------------------------------------------------------

	
    //----------------------------------------------------------------------------------------
    // Basic Testing
    //----------------------------------------------------------------------------------------
	@isTest static void test_tr_SVMXC_InstalledProduct() {
	
		// Select Data
		List<SVMXC__Installed_Product__c> lstData = [SELECT Id, SVMXC__Status__c FROM SVMXC__Installed_Product__c];
		SVMXC__Installed_Product__c oData = lstData[0];
		
		// Update Data
		update oData;

		// Delete Data
		delete oData;

		// Undelete Data
		Undelete oData;

	}
    //----------------------------------------------------------------------------------------
	
}
//--------------------------------------------------------------------------------------------------------------------