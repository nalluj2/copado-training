@isTest
private class TEST_TherapyProductBeforeUpsert
 {
    static TestMethod void triggerTherapyProductBeforeUpsert()
  {   

 //Insert Company
     Company__c apCmpny = new Company__c();
     apCmpny.name='TestMedtronic';
     apCmpny.CurrencyIsoCode = 'EUR';
     apCmpny.Current_day_in_Q1__c=56;
     apCmpny.Current_day_in_Q2__c=34; 
     apCmpny.Current_day_in_Q3__c=5; 
     apCmpny.Current_day_in_Q4__c= 0;   
     apCmpny.Current_day_in_year__c =200;
     apCmpny.Days_in_Q1__c=56;  
     apCmpny.Days_in_Q2__c=34;
     apCmpny.Days_in_Q3__c=13;
     apCmpny.Days_in_Q4__c=22;
     apCmpny.Days_in_year__c =250;
     apCmpny.Company_Code_Text__c = 'T15';
     insert apCmpny;

    Business_Unit__c bu1 =  new Business_Unit__c();
    bu1.Company__c =apCmpny.id;
    bu1.name='BU1Medtronic';
    insert bu1;

     //Insert SBU
    Sub_Business_Units__c sbu1 = new Sub_Business_Units__c();
    sbu1.name='SBUMedtronic1';
    sbu1.Business_Unit__c=bu1.id;
    sbu1.Account_Plan_Default__c='Revenue';
    sbu1.Account_Flag__c = 'AF_Solutions__c';
    insert sbu1;

    /*
    product2 pr =  new product2();
    pr.RecordTypeId = RecordTypeMedtronic.Product('MDT_Marketing_Product').id;
    pr.name='testName';
    insert pr;

    //Insert Therapy Group
    Therapy_Group__c tg = new Therapy_Group__c();
    tg.Name='Therapy Group';
    tg.Sub_Business_Unit__c = sbu1.Id;
    tg.Company__c = apCmpny.id;
    insert tg;

    Therapy__c th = new Therapy__c();
    th.name='test ## th';
	th.Business_Unit__c = bu1.id;
	th.Sub_Business_Unit__c = sbu1.id;
	th.Therapy_Group__c= tg.id;
	th.Therapy_Name_Hidden__c='test ## th';
    insert th;
    
   Therapy_Product__c tp = new Therapy_Product__c();
    tp.Product__c=pr.id;
    tp.therapy__c=th.id;
        
   insert tp;
  
   tp.Therapy_Product_Unique__c = string.valueOf(tp.Product__c) + string.valueOf(tp.Therapy__c);
    
    update tp;
//    TherapyProduct[0].Therapy_Products_Unique__c = string.valueOf(TherapyProduct[0].Product__c) + string.valueOf(TherapyProduct[0].Therapy__c)
	*/
     }
}