/**
 * Creation Date: 	20090905
 * Description:		This class is used to put the matrix of Campaigns - Accounts on screen
 *
 * Author: 	ABSI - BC
 */
public with sharing class DIB_Class_Campaign {

			private Account SFAccount = new Account();
			private List<DIB_Campaign_Account__c> camp_account = new List<DIB_Campaign_Account__c>();
			public DIB_Department__c dibDepartment {set;get;} 
			
			public Account getSFAccount() {
				return SFAccount;
			}
				
			public  List<DIB_Campaign_Account__c> getCamp_account() {
				return camp_account;
			}
			
			public void setSFAccount(Account SFAccountIn) {
				SFAccount = SFAccountIn;
				SFAccount.System_Account__c = SFAccountIn.Id;
			}
			
			public  void setCamp_account(List<DIB_Campaign_Account__c> camp_accountIn) {
				camp_account = camp_accountIn;
			}
}