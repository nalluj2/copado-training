@isTest public class clsTestData_CallRecord {
	
    public static Integer iRecord_CallRecord = 1;
    public static Id idRecordType_CallRecord = clsUtil.getRecordTypeByDevName('Call_Records__c', 'xBU').Id;
    public static List<Call_Records__c> lstCallRecord = new List<Call_Records__c>();
    public static Call_Records__c oMain_CallRecord;

    public static Integer iRecord_ContactVisitReport = 1;
    public static List<Contact_Visit_Report__c> lstContactVisitReport = new List<Contact_Visit_Report__c>();
    public static Contact_Visit_Report__c oMain_ContactVisitReport;

    public static Integer iRecord_ContactCallTopicCount = 1;
    public static List<Contact_Call_Topic_Count__c> lstContactCallTopicCount = new List<Contact_Call_Topic_Count__c>();
    public static Contact_Call_Topic_Count__c oMain_ContactCallTopicCount;


    //---------------------------------------------------------------------------------------------------
    // Create Call Record Data
    //---------------------------------------------------------------------------------------------------
    public static List<Call_Records__c> createCallRecord(){
        return createCallRecord(true);
    }
    public static List<Call_Records__c> createCallRecord(Boolean bInsert){

        if (oMain_CallRecord == null){

            lstCallRecord = new List<Call_Records__c>();

            for (Integer i=0; i<iRecord_CallRecord; i++){

                Call_Records__c oCallRecord = new Call_Records__c(); 
                    oCallRecord.RecordTypeID = idRecordType_CallRecord;
                    oCallRecord.Call_Channel__c = '1:1 visit'; 
                    oCallRecord.Call_Date__c = Date.today().addDays(-i); 
                lstCallRecord.add(oCallRecord);    

            }

            if (bInsert) insert lstCallRecord;

            oMain_CallRecord = lstCallRecord[0];
        }

        return lstCallRecord;

    }
    //---------------------------------------------------------------------------------------------------


    //---------------------------------------------------------------------------------------------------
    // Create Contact Visit Report Data
    //---------------------------------------------------------------------------------------------------
    public static List<Contact_Visit_Report__c> createContactVisitReport(){
    	return createContactVisitReport(true);
    }
    public static List<Contact_Visit_Report__c> createContactVisitReport(Boolean bInsert){

        if (oMain_ContactVisitReport == null){

            if (clsTestData_Account.oMain_Account == null) clsTestData_Account.createAccount();
            if (clsTestData_Contact.oMain_Contact == null) clsTestData_Contact.createContact();
            if (oMain_CallRecord == null) createCallRecord();

            lstContactVisitReport = new List<Contact_Visit_Report__c>();

            for (Integer i=0; i<iRecord_ContactVisitReport; i++){
	            Contact_Visit_Report__c oContactVisitReport = new Contact_Visit_Report__c();
	                oContactVisitReport.Call_Records__c = oMain_CallRecord.Id; 
	                oContactVisitReport.Attending_Contact__c = clsTestData_Contact.oMain_Contact.Id; 
	                oContactVisitReport.Attending_Affiliated_Account__c = clsTestData_Contact.oMain_Contact.AccountId; 
	            lstContactVisitReport.add(oContactVisitReport);
	        }
            
            if (bInsert) insert lstContactVisitReport;

            oMain_ContactVisitReport = lstContactVisitReport[0];
        }

        return lstContactVisitReport;

    }
    //---------------------------------------------------------------------------------------------------


    //---------------------------------------------------------------------------------------------------
    // Create Contact Visit Report Data
    //---------------------------------------------------------------------------------------------------
    public static List<Contact_Call_Topic_Count__c> createContactCallTopicCount(){
    	return createContactCallTopicCount(true);
    }
    public static List<Contact_Call_Topic_Count__c> createContactCallTopicCount(Boolean bInsert){

        if (oMain_ContactCallTopicCount == null){

            if (clsTestData_Contact.oMain_Contact == null) clsTestData_Contact.createContact();
            if (oMain_CallRecord == null) createCallRecord();

            lstContactCallTopicCount = new List<Contact_Call_Topic_Count__c>();

            for (Integer i=0; i<iRecord_ContactCallTopicCount; i++){
	            Contact_Call_Topic_Count__c oContactCallTopicCount = new Contact_Call_Topic_Count__c();
	                oContactCallTopicCount.Call_Recording__c = oMain_CallRecord.Id; 
	                oContactCallTopicCount.Contact__c = clsTestData_Contact.oMain_Contact.Id; 
	                oContactCallTopicCount.active__c = true;
	            lstContactCallTopicCount.add(oContactCallTopicCount);
	        }
            
            if (bInsert) insert lstContactCallTopicCount;

            oMain_ContactCallTopicCount = lstContactCallTopicCount[0];
        }

        return lstContactCallTopicCount;

    }
    //---------------------------------------------------------------------------------------------------

}