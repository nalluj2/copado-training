@isTest 
private class TEST_ws_OpportunityAccountService{

	@testSetup static void createTestData(){
		
		// Create Account Data
		clsTestData_Account.iRecord_Account = 5;
		clsTestData_Account.createAccount();

		// Create Opportunity Data
		clsTestData_Opportunity.createOpportunity();

	}


	@isTest private static void createOpportunityAccount(){

		//------------------------------------------
		// Create / Select Test Data
		//------------------------------------------
		Opportunity oOpportunity = [SELECT Id, AccountId FROM Opportunity][0];
		List<Account> lstAccount = [SELECT Id FROM Account WHERE Id != :oOpportunity.AccountId];

		Opportunity_Account__c oOpportunityAccount = new Opportunity_Account__c();
			oOpportunityAccount.Mobile_ID__c = GuidUtil.NewGuid();
			oOpportunityAccount.Opportunity__c = oOpportunity.Id;
			oOpportunityAccount.Account__c = lstAccount[0].Id;
		//------------------------------------------


		//------------------------------------------
		// Execute Logic
		//------------------------------------------
		ws_OpportunityAccountService.IFOpportunityAccountResponse oResponse;

		Test.startTest();

    		RestRequest oRestRequest = new RestRequest();
        
			oRestRequest.requestURI = '/services/apexrest/OpportunityAccountService';
			oRestRequest.httpMethod = 'POST';

			ws_OpportunityAccountService.IFOpportunityAccountRequest oRequest = new ws_OpportunityAccountService.IFOpportunityAccountRequest();
			oRequest.opportunityAccount = oOpportunityAccount;
        
			oRestRequest.requestbody = Blob.valueOf(JSON.serializePretty(oRequest));
        
			RestContext.request = oRestRequest;
			RestContext.response = new RestResponse();
    	
    		String tResponse = ws_OpportunityAccountService.doPost(); 
			System.debug('** Response : ' + tResponse);
		
			oResponse = (ws_OpportunityAccountService.IFOpportunityAccountResponse) JSON.deserialize(tResponse, ws_OpportunityAccountService.IFOpportunityAccountResponse.class);
		
		Test.stopTest();
		//------------------------------------------


		//------------------------------------------
		// Validate Result
		//------------------------------------------
		System.assertEquals(oResponse.success, true);
		System.assertNotEquals(oResponse.opportunityAccount.id, null);
		
		oOpportunityAccount = [SELECT Id, Opportunity__c, Account__c FROM Opportunity_Account__c WHERE Id = :oResponse.opportunityAccount.Id];
		System.assertEquals(oResponse.opportunityAccount.Account__c, lstAccount[0].Id);
		System.assertEquals(oResponse.opportunityAccount.Opportunity__c, oOpportunity.Id);
		System.assert(oResponse.opportunityAccount.Id != null);
		//------------------------------------------

	}

/* 
	NOT IMPLEMENTED AT THE MOMENT

	@isTest private static void updateOpportunityAccount(){

		//------------------------------------------
		// Create / Select Test Data
		//------------------------------------------
		Opportunity oOpportunity = [SELECT Id, AccountId FROM Opportunity][0];
		List<Account> lstAccount = [SELECT Id FROM Account WHERE Id != :oOpportunity.AccountId];

		Opportunity_Account__c oOpportunityAccount = new Opportunity_Account__c();
			oOpportunityAccount.Opportunity__c = oOpportunity.Id;
			oOpportunityAccount.Account__c = lstAccount[0].Id;
		insert oOpportunityAccount;

		oOpportunityAccount = [SELECT Id, Mobile_ID__c, Opportunity__c, Account__c FROM Opportunity_Account__c WHERE Id = :oOpportunityAccount.Id][0];
		String tMobileId_Old = oOpportunityAccount.Mobile_ID__c;
		String tMobileId_New = GuidUtil.NewGuid();
		oOpportunityAccount.Mobile_ID__c = tMobileId_New;
		//------------------------------------------


		//------------------------------------------
		// Execute Logic
		//------------------------------------------
		ws_OpportunityAccountService.IFOpportunityAccountResponse oResponse;

		Test.startTest();

    		RestRequest oRestRequest = new RestRequest();
        
			oRestRequest.requestURI = '/services/apexrest/OpportunityAccountService';
			oRestRequest.httpMethod = 'POST';

			ws_OpportunityAccountService.IFOpportunityAccountRequest oRequest = new ws_OpportunityAccountService.IFOpportunityAccountRequest();
			oRequest.opportunityAccount = oOpportunityAccount;
        
			oRestRequest.requestbody = Blob.valueOf(JSON.serializePretty(oRequest));
        
			RestContext.request = oRestRequest;
			RestContext.response = new RestResponse();
    	
    		String tResponse = ws_OpportunityAccountService.doPost(); 
			System.debug('** Response : ' + tResponse);
		
			oResponse = (ws_OpportunityAccountService.IFOpportunityAccountResponse) JSON.deserialize(tResponse, ws_OpportunityAccountService.IFOpportunityAccountResponse.class);
		
		Test.stopTest();
		//------------------------------------------


		//------------------------------------------
		// Validate Result
		//------------------------------------------
		System.debug('**BC** oResponse.message : ' + oResponse.message);
		System.assertEquals(oResponse.success, true);
		System.assertNotEquals(oResponse.opportunityAccount.id, null);
		
		List<Opportunity_Account__c> lstOpportunityAccount = [SELECT Id, Mobile_ID__c, Opportunity__c, Account__c FROM Opportunity_Account__c WHERE (Mobile_ID__c = :tMobileId_Old OR Mobile_ID__c = :tMobileId_New)];
		System.assertEquals(lstOpportunityAccount.size(), 1);
		System.assertEquals(lstOpportunityAccount[0].Mobile_ID__c, tMobileId_New);
		System.assertEquals(oResponse.opportunityAccount.Account__c, lstAccount[0].Id);
		System.assertEquals(oResponse.opportunityAccount.Opportunity__c, oOpportunity.Id);
		System.assertEquals(oResponse.opportunityAccount.Mobile_ID__c, tMobileId_New);
		//------------------------------------------

	}
*/

	@isTest private static void deleteOpportunityAccount(){

		//------------------------------------------
		// Create / Select Test Data
		//------------------------------------------
		Opportunity oOpportunity = [SELECT Id, AccountId FROM Opportunity][0];
		List<Account> lstAccount = [SELECT Id FROM Account WHERE Id != :oOpportunity.AccountId];

		Opportunity_Account__c oOpportunityAccount = new Opportunity_Account__c();
			oOpportunityAccount.Opportunity__c = oOpportunity.Id;
			oOpportunityAccount.Account__c = lstAccount[1].Id;
		insert oOpportunityAccount;

		oOpportunityAccount = [SELECT Id, Mobile_ID__c, Opportunity__c, Account__c FROM Opportunity_Account__c WHERE Id = :oOpportunityAccount.Id][0];
		String tMobileId = oOpportunityAccount.Mobile_ID__c;
		//------------------------------------------


		//------------------------------------------
		// Execute Logic
		//------------------------------------------
		ws_OpportunityAccountService.IFOpportunityAccountDeleteResponse oResponse;

		Test.startTest();

    		RestRequest oRestRequest = new RestRequest();
        
			oRestRequest.requestURI = '/services/apexrest/OpportunityAccountService';
			oRestRequest.httpMethod = 'DELETE';

			ws_OpportunityAccountService.IFOpportunityAccountDeleteRequest oRequest = new ws_OpportunityAccountService.IFOpportunityAccountDeleteRequest();
			oRequest.opportunityAccount = oOpportunityAccount;
        
			oRestRequest.requestbody = Blob.valueOf(JSON.serializePretty(oRequest));
        
			RestContext.request = oRestRequest;
			RestContext.response = new RestResponse();
    	
    		String tResponse = ws_OpportunityAccountService.doDelete(); 
			System.debug('** Response : ' + tResponse);
		
			oResponse = (ws_OpportunityAccountService.IFOpportunityAccountDeleteResponse) JSON.deserialize(tResponse, ws_OpportunityAccountService.IFOpportunityAccountDeleteResponse.class);
		
		Test.stopTest();
		//------------------------------------------


		//------------------------------------------
		// Validate Result
		//------------------------------------------
		System.assertEquals(oResponse.success, true);
		
		List<Opportunity_Account__c> lstOpportunityAccount = [SELECT Id, Mobile_ID__c, Opportunity__c, Account__c FROM Opportunity_Account__c WHERE Mobile_ID__c = :tMobileId];
		System.assertEquals(lstOpportunityAccount.size(), 0);
		//------------------------------------------
	}
}