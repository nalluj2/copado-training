global with sharing class ba_IHS_Opportunity_TARP_Reminder implements Schedulable, Database.Batchable<sObject> {
    
    global void execute(SchedulableContext ctx){        
                               
        Database.executeBatch(new ba_IHS_Opportunity_TARP_Reminder(), 200);    
    } 
    
    global Database.QueryLocator start(Database.BatchableContext ctx){    
                
		Set<String> setOpportunityRecordTypeDeveloperName = new Set<String>{'EMEA_IHS_Managed_Services', 'EMEA_IHS_Consulting_Opportunity'};
        Date targetDate = Date.today().addMonths(18);
        
        String query = 'Select Id, Contract_Owner__c from Opportunity where RecordType.DeveloperName IN :setOpportunityRecordTypeDeveloperName';
        query += ' AND StageName LIKE \'%Closed Won%\' AND Contract_End_Date__c >= TODAY AND Contract_End_Date__c <= :targetDate AND TARP_in_place_date__c = null AND Contract_Owner__c != null';
        
        return Database.getQueryLocator(query);

    }

    global void execute(Database.BatchableContext ctx, List<Opportunity> records){
    	    	    	
    	List<Task> tarpReminders = new List<Task>();
    	
    	List<Opportunity> oppWithTARPTasks = [Select Id, Contract_Owner__c, Contract_End_Date__c, OwnerId, (Select Id from Tasks where Subject = 'Arrange a TARP with the Account Project Board') from Opportunity where Id IN :records];

    	for (Opportunity opp : oppWithTARPTasks){
    		
    		if(opp.Tasks.size() == 0){
    			
    			Task tarpReminder = new Task();
    			tarpReminder.ActivityDate = opp.Contract_End_Date__c;
    			tarpReminder.ReminderDateTime = opp.Contract_End_Date__c.addMonths(-17); 
    			tarpReminder.OwnerId = opp.Contract_Owner__c; 
    			tarpReminder.WhatId = opp.Id;
    			tarpReminder.Status = 'Not Started';
    			//tarpReminder.RecordTypeId = null;
    			tarpReminder.Subject = 'Arrange a TARP with the Account Project Board';
    			
    			tarpReminders.add(tarpReminder);
    		
			}
    	
		}
    	
    	if (tarpReminders.size() >0){
    		
    		Database.DMLOptions DMLOptions = new Database.DMLOptions();
            DMLOptions.EmailHeader.triggerUserEmail = true;
        
            Database.insert(tarpReminders, DMLOptions);    		
    	
		}
    
	}
    
    global void finish(Database.BatchableContext ctx){ 
	
	}

}