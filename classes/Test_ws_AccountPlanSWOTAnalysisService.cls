@isTest
private class Test_ws_AccountPlanSWOTAnalysisService {
    
    private static testmethod void testCreateSWOTAnalysis(){
    	
    	Account_Plan_2__c accPlan = [Select Id from Account_Plan_2__c];
		
		Test.startTest();
		
		Account_Plan_SWOT_Analysis__c swot = new Account_Plan_SWOT_Analysis__c();
		swot.Account_Plan__c = accPlan.Id;
		swot.Department__c = 'Adult Type 1';
		swot.Mobile_Id__c = 'UnitTestSWOT';
    	
    	ws_AccountPlanSWOTAnalysisService.IFAccountPlanSWOTRequest request = new ws_AccountPlanSWOTAnalysisService.IFAccountPlanSWOTRequest();
	 	request.accountPlanSWOT = swot;
	 	request.id = 'testId';
	 	
	 	RestRequest req = new RestRequest();	 	
	 	req.requestbody = Blob.valueOf(JSON.serialize(request));
	 	
	 	RestContext.request = req; 
    	
    	String res = ws_AccountPlanSWOTAnalysisService.doPost();
    	
    	ws_AccountPlanSWOTAnalysisService.IFAccountPlanSWOTResponse response = (ws_AccountPlanSWOTAnalysisService.IFAccountPlanSWOTResponse) JSON.deserialize(res, ws_AccountPlanSWOTAnalysisService.IFAccountPlanSWOTResponse.class);
    	
    	System.assert(response.success == true);
    	
    	swot = [Select Id from Account_Plan_SWOT_Analysis__c where Mobile_Id__c = 'UnitTestSWOT'];
    }
    
    private static testmethod void testUpdateSWOTAnalysis(){
    	
    	Account_Plan_2__c accPlan = [Select Id from Account_Plan_2__c];
		
		Account_Plan_SWOT_Analysis__c swot = new Account_Plan_SWOT_Analysis__c();
		swot.Account_Plan__c = accPlan.Id;
		swot.Department__c = 'Adult Type 1';
		swot.Mobile_Id__c = 'UnitTestSWOT';
		insert swot;
		
		Test.startTest();
		
    	swot = swot.clone();
    	swot.Department__c = 'Pediatric';
    	
    	ws_AccountPlanSWOTAnalysisService.IFAccountPlanSWOTRequest request = new ws_AccountPlanSWOTAnalysisService.IFAccountPlanSWOTRequest();
	 	request.accountPlanSWOT = swot;
	 	request.id = 'testId';
	 	
	 	RestRequest req = new RestRequest();	 	
	 	req.requestbody = Blob.valueOf(JSON.serialize(request));
	 	
	 	RestContext.request = req; 
    	
    	String res = ws_AccountPlanSWOTAnalysisService.doPost();
    	
    	ws_AccountPlanSWOTAnalysisService.IFAccountPlanSWOTResponse response = (ws_AccountPlanSWOTAnalysisService.IFAccountPlanSWOTResponse) JSON.deserialize(res, ws_AccountPlanSWOTAnalysisService.IFAccountPlanSWOTResponse.class);
    	
    	System.assert(response.success == true);
    	
    	swot = [Select Id, Department__c from Account_Plan_SWOT_Analysis__c where Mobile_Id__c = 'UnitTestSWOT'];
    	
    	System.assert(swot.Department__c == 'Pediatric');
    }
    
    private static testmethod void testDeleteSWOTAnalysis(){
    	
    	Account_Plan_2__c accPlan = [Select Id from Account_Plan_2__c];
		
		Account_Plan_SWOT_Analysis__c swot = new Account_Plan_SWOT_Analysis__c();
		swot.Account_Plan__c = accPlan.Id;
		swot.Department__c = 'Adult Type 1';
		swot.Mobile_Id__c = 'UnitTestSWOT';
		insert swot;
		
		Test.startTest();
		
    	swot = swot.clone();
    	    	
    	ws_AccountPlanSWOTAnalysisService.IFAccountPlanSWOTDeleteRequest request = new ws_AccountPlanSWOTAnalysisService.IFAccountPlanSWOTDeleteRequest();
	 	request.accountPlanSWOT = swot;
	 	request.id = 'testId';
	 	
	 	RestRequest req = new RestRequest();	 	
	 	req.requestbody = Blob.valueOf(JSON.serialize(request));
	 	
	 	RestContext.request = req; 
    	
    	String res = ws_AccountPlanSWOTAnalysisService.doDelete();
    	
    	ws_AccountPlanSWOTAnalysisService.IFAccountPlanSWOTDeleteResponse response = (ws_AccountPlanSWOTAnalysisService.IFAccountPlanSWOTDeleteResponse) JSON.deserialize(res, ws_AccountPlanSWOTAnalysisService.IFAccountPlanSWOTDeleteResponse.class);
    	
    	System.assert(response.success == true);
    	
    	List<Account_Plan_SWOT_Analysis__c> swots = [Select Id, Department__c from Account_Plan_SWOT_Analysis__c where Mobile_Id__c = 'UnitTestSWOT'];
    	
    	System.assert(swots.size() == 0);
    }
    
    private static testmethod void testCreateSWOTAnalysis_Error(){
    	
    	Account_Plan_2__c accPlan = [Select Id from Account_Plan_2__c];
		
		Test.startTest();
		
		Account_Plan_SWOT_Analysis__c swot = new Account_Plan_SWOT_Analysis__c();
		swot.Account_Plan__c = accPlan.Id;		
		swot.Mobile_Id__c = 'UnitTestSWOT';
    	
    	ws_AccountPlanSWOTAnalysisService.IFAccountPlanSWOTRequest request = new ws_AccountPlanSWOTAnalysisService.IFAccountPlanSWOTRequest();
	 	request.accountPlanSWOT = swot;
	 	request.id = 'testId';
	 	
	 	RestRequest req = new RestRequest();	 	
	 	req.requestbody = Blob.valueOf(JSON.serialize(request));
	 	
	 	RestContext.request = req; 
    	
    	String res = ws_AccountPlanSWOTAnalysisService.doPost();
    	
    	ws_AccountPlanSWOTAnalysisService.IFAccountPlanSWOTResponse response = (ws_AccountPlanSWOTAnalysisService.IFAccountPlanSWOTResponse) JSON.deserialize(res, ws_AccountPlanSWOTAnalysisService.IFAccountPlanSWOTResponse.class);
    	
    	System.assert(response.success == false);
    	
    	List<Account_Plan_SWOT_Analysis__c> swots = [Select Id from Account_Plan_SWOT_Analysis__c where Mobile_Id__c = 'UnitTestSWOT'];
    	System.assert(swots.size() == 0);
    }
    
    private static testmethod void testDeleteSWOTAnalysis_NotFound(){
    			
		Test.startTest();
		
		Account_Plan_SWOT_Analysis__c swot = new Account_Plan_SWOT_Analysis__c();				
		swot.Mobile_Id__c = 'UnitTestSWOT';
		    	
    	ws_AccountPlanSWOTAnalysisService.IFAccountPlanSWOTDeleteRequest request = new ws_AccountPlanSWOTAnalysisService.IFAccountPlanSWOTDeleteRequest();
	 	request.accountPlanSWOT = swot;
	 	request.id = 'testId';
	 	
	 	RestRequest req = new RestRequest();	 	
	 	req.requestbody = Blob.valueOf(JSON.serialize(request));
	 	
	 	RestContext.request = req; 
    	
    	String res = ws_AccountPlanSWOTAnalysisService.doDelete();
    	
    	ws_AccountPlanSWOTAnalysisService.IFAccountPlanSWOTDeleteResponse response = (ws_AccountPlanSWOTAnalysisService.IFAccountPlanSWOTDeleteResponse) JSON.deserialize(res, ws_AccountPlanSWOTAnalysisService.IFAccountPlanSWOTDeleteResponse.class);
    	
    	System.assert(response.success == true);
		System.assert(response.message == 'Account Plan SWOT Analysis not found');
    }
    
    @TestSetup
    private static void setData() {
	
		Company__c cmpny = new Company__c();		
        cmpny.name='Europe';
        cmpny.CurrencyIsoCode = 'EUR';
        cmpny.Current_day_in_Q1__c=56;
        cmpny.Current_day_in_Q2__c=34; 
        cmpny.Current_day_in_Q3__c=5; 
        cmpny.Current_day_in_Q4__c= 0;   
        cmpny.Current_day_in_year__c =200;
        cmpny.Days_in_Q1__c=56;  
        cmpny.Days_in_Q2__c=34;
        cmpny.Days_in_Q3__c=13;
        cmpny.Days_in_Q4__c=22;
        cmpny.Days_in_year__c =250;
        cmpny.Company_Code_Text__c = 'T35';
		insert cmpny;
		        
        Business_Unit_Group__c bug_DiB =  new Business_Unit_Group__c();
        bug_DiB.Master_Data__c =cmpny.id;
        bug_DiB.name='Diabetes';              
        insert bug_DiB;
		       
        Business_Unit__c bu_DiB =  new Business_Unit__c();
        bu_DiB.Company__c =cmpny.id;
        bu_DiB.name='Diabetes';
        bu_DiB.Business_Unit_Group__c = bug_DiB.Id;             
        insert bu_DiB;
                        
        Sub_Business_Units__c sbu_DiB = new Sub_Business_Units__c();
        sbu_DiB.name='Diabetes Core';
        sbu_DiB.Business_Unit__c=bu_DiB.id;		
		insert sbu_DiB;
		
		DIB_Country__c country = new DIB_Country__c();
		country.Company__c = cmpny.Id;
		country.Name = 'Unit Test Country';
		country.Country_ISO_Code__c = 'ZZ';
		country.CurrencyIsoCode = 'EUR';
		insert country;
								
		Account testAccount = new Account();
		testAccount.Name = 'RTG Test Account';
		testAccount.Account_Country_vs__c = 'BELGIUM';
		insert testAccount;			
		
		Account_Plan_2__c accPlan = new Account_Plan_2__c();
		accPlan.Account__c = testAccount.Id;
		accPlan.Account_Plan_Level__c = 'Sub Business Unit';
		accPlan.Business_Unit_Group__c = bug_DiB.Id;
		accPlan.Business_Unit__c = bu_DiB.Id;
		accPlan.Sub_Business_Unit__c = sbu_DiB.Id;
		accPlan.RecordTypeId = [Select Id from RecordType where SObjectType = 'Account_Plan_2__c' AND DeveloperName = 'EUR_DIB_sBU'].Id;
		insert accPlan;	
		
		Department_Master__c adult1 = new Department_Master__c( Name = 'Adult Type 1');
		Department_Master__c pediatric = new Department_Master__c( Name = 'Pediatric');
		insert new List<Department_Master__c>{adult1, pediatric};
		
		DIB_Department__c accAdult1 = new DIB_Department__c();
		accAdult1.Account__c = testAccount.Id;
		accAdult1.Department_ID__c = adult1.Id;
		
		DIB_Department__c accPediatric = new DIB_Department__c();
		accPediatric.Account__c = testAccount.Id;
		accPediatric.Department_ID__c = pediatric.Id;
		
		insert new List<DIB_Department__c>{accAdult1, accPediatric};
	}
}