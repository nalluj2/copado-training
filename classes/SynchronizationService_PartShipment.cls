public without sharing class SynchronizationService_PartShipment implements SynchronizationService_Object{
	
	//Set with the external Ids of the records already processed in this transaction. Used to avoid generating multiple notifications for the same change
	private Set<String> processedRecords = new Set<String>();
		
	//This method returns the External Ids of the records that have relevant modifications for the target Org. This includes created records or 
	//modified records where synchronized fields have changed. 
	public Set<String> hasChangeForNotification(List<SObject> records, Map<Id, SObject> oldRecords){
		
		Set<String> result = new Set<String>();
				
		for(SObject rawObject : records){
			
			Parts_Shipment__c record = (Parts_Shipment__c) rawObject;
			
			if(processedRecords.contains(record.External_Id__c)) continue;
			
			//On Insert we always send notification
			if(oldRecords == null){
				result.add(record.External_Id__c);
				processedRecords.add(record.External_Id__c);
			}//If Update we only send notification when relevant fields are modified
			else{
				
				if( bl_SynchronizationService_Utils.hasChanged( record, oldRecords.get(record.Id), syncFields ) == true ){
					
					result.add(record.External_Id__c);
					processedRecords.add(record.External_Id__c);
				}
			}
		}
		
		return result;
	}
	
	//This method collects and returns the complete information for a record. This includes the records itself as well as any related record that needs
	//to exist in the target Org too. The returned format is JSON serialized string.
	public String generatePayload(String externalId){
		
		PartShipmentSyncPayload payload = new PartShipmentSyncPayload();
		
		String query = 	'SELECT ' + String.join( syncFields, ',') + 
						',Account__r.SAP_Id__c, Account__r.AccountNumber, Account__r.RecordType.DeveloperName, Asset__r.Serial_Nr__c' + 
						',Case__r.External_Id__c, Complaint__r.External_Id__c, Workorder__r.External_Id__c ' +
						'FROM Parts_Shipment__c WHERE External_Id__c = :externalId LIMIT 2';
						
		List<Parts_Shipment__c> partShipments = Database.query(query);
				
		//If no record or more than 1 is found, we return an empty response
		if(partShipments.size() == 1){
			
			Parts_Shipment__c partShipment = partShipments[0];	
			bl_SynchronizationService_Utils.prepareForJSON(partShipment, syncFields);		
			partShipment.Id = null;
			
			payload.PartShipment = partShipment;
			
			Set<Id> accountIds = new Set<Id>();
			
			//Account		
			if(partShipment.Account__r != null){
				
				accountIds.add(partShipment.Account__c);
				
				if ( (partShipment.Account__r.RecordType.DeveloperName == 'SAP_Account') || (partShipment.Account__r.RecordType.DeveloperName == 'Distributor') ){
					payload.AccountId = partShipment.Account__r.SAP_Id__c;	
				}else{
					payload.AccountId = partShipment.Account__r.AccountNumber;	
				}
				partShipment.Account__c = null;
				partShipment.Account__r = null;							
			}
			
			//Asset
			if(partShipment.Asset__r != null){
				
				payload.assetId = partShipment.Asset__r.Serial_Nr__c;
				partShipment.Asset__r = null;
				partShipment.Asset__c = null;
			}			
			
			//Case
			if(partShipment.Case__r != null){
				
				payload.caseId = partShipment.Case__r.External_Id__c;
				partShipment.Case__r = null;
				partShipment.Case__c = null;
			}
			
			//Complaint
			if(partShipment.Complaint__r != null){
				
				payload.complaintId = partShipment.Complaint__r.External_Id__C;
				partShipment.Complaint__r = null;
				partShipment.Complaint__c = null;
			}
			
			//Work Order
			if(partShipment.Workorder__r != null){
				
				payload.workorderId = partShipment.Workorder__r.External_Id__c;
				partShipment.Workorder__r = null;
				partShipment.Workorder__c = null;
			}
			
			//Done in a separate query because we cannot clear the value from the CreatedDate field before sending it to the target
			Parts_Shipment__c partDetails = [Select Name, CreatedDate, CreatedBy.Name, LastModifiedDate, LastModifiedBy.Name from Parts_Shipment__c where External_Id__c = :externalId];
			
			payload.ShipmentName = partDetails.Name;			
			payload.CreatedDate = partDetails.CreatedDate;			
			payload.CreatedBy = partDetails.CreatedBy.Name;					
			payload.LastModifiedDate = partDetails.LastModifiedDate;			
			payload.LastModifiedBy = partDetails.LastModifiedBy.Name;					

			payload.generateAccountPayload(accountIds, null);
		}
		
		return JSON.serialize(payload);
	}
	
	//Implementation needed only in Target Org
	public String processPayload(String inputPayload){						
		return null;
	}
	
	//Model classes for the requests and responses. To be used by the JSON serialize/deserialize
	private class PartShipmentSyncPayload extends SynchronizationService_Payload{
		
		public Parts_Shipment__c partShipment {get; set;}
		public String shipmentName {get; set;}		
		public String accountId {get; set;}		
		public String assetId {get; set;}	
		public String caseId {get; set;}	
		public String complaintId {get; set;}
		public String workorderId {get; set;}
		public Datetime createdDate {get; set;}
		public String createdBy {get; set;}
		public Datetime lastModifiedDate {get; set;}
		public String lastModifiedBy {get; set;}
	}	
	
	private static List<String> syncFields = new List<String>
	{
		'Account__c',
		'Asset__c',
		'Case__c',
		'Complaint__c',
		'External_Id__c',
		'PO_Number__c',
		'Shipping_Attention_To__c',
		'Shipping_City__c',
		'Shipping_Comments__c',
		'Shipping_Company__c',
		'Shipping_Country__c',
		'Shipping_Phone__c',
		'Shipping_State__c',
		'Shipping_Street__c',
		'Shipping_Zip_Code__c',
		'Ship_Via__c',
		'Status__c',
		'Tracking_Number__c',
		'Workorder__c'								
	};
}