//--------------------------------------------------------------------------------------------------------------------------------
//  Author      :   Bart Caelen
//  Date        :   20160524
//  Description :   APEX TEST Class for the APEX Trigger tr_Opportunity and APEX Class bl_Opportunity_Trigger
//  Change Log  :    
//--------------------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_bl_TimeReporting_Trigger {

	private static List<Change_Request__c> lstChangeRequest = new List<Change_Request__c>();
	private static List<Change_Request__c> lstChangeRequest_NoProject = new List<Change_Request__c>();

	private static List<Time_Reporting__c> lstTimeReporting = new List<Time_Reporting__c>();
	private static List<Time_Reporting__c> lstTimeReporting_Project = new List<Time_Reporting__c>();
	private static List<Time_Reporting__c> lstTimeReporting_Release = new List<Time_Reporting__c>();
	private static List<Time_Reporting__c> lstTimeReporting_ChangeRequest = new List<Time_Reporting__c>();

    //----------------------------------------------------------------------------------------
    // CREATE TEST DATA
    //----------------------------------------------------------------------------------------
	private static void createTestData(Boolean bInsert) {

		lstTimeReporting = new List<Time_Reporting__c>();

		clsTestData_ProjectManagement.iRecord_TimeReporting_Release = 1;
		clsTestData_ProjectManagement.iHours_TimeReporting_Release = 2;
		lstTimeReporting_Release.addAll(clsTestData_ProjectManagement.createTimeReporting_Release(false));

		clsTestData_ProjectManagement.iRecord_TimeReporting_ProjectManagement = 1;
		clsTestData_ProjectManagement.iHours_TimeReporting_ProjectManagement = 2;
		lstTimeReporting_Project.addAll(clsTestData_ProjectManagement.createTimeReporting_ProjectManagement(false));

		clsTestData_ProjectManagement.iRecord_TimeReporting_ChangeRequest = 1;
		clsTestData_ProjectManagement.iHours_TimeReporting_ChangeRequest = 2;
		lstTimeReporting_ChangeRequest.addAll(clsTestData_ProjectManagement.createTimeReporting_ChangeRequest(false));

		lstTimeReporting.addAll(lstTimeReporting_Project);
		lstTimeReporting.addAll(lstTimeReporting_Release);
		lstTimeReporting.addAll(lstTimeReporting_ChangeRequest);

		if (bInsert)insert lstTimeReporting;

    }
    //----------------------------------------------------------------------------------------

	
	@isTest static void test_bl_TimeReporting_Trigger_INSERT() {

		// Create Test Data
		createTestData(false);


		// Perform Logic
		Test.startTest();

		insert lstTimeReporting;

		Test.stopTest();


		// Validate Logic

	}
	
	@isTest static void test_bl_TimeReporting_Trigger_UPDATE_Hours() {

		// Create Test Data
		createTestData(true);

		// Perform Logic
		Test.startTest();

		for (Time_Reporting__c oTimeReporting : lstTimeReporting){
			oTimeReporting.hours__c = 4;
		}

		update lstTimeReporting;

		Test.stopTest();


		// Validate Logic

	}

	@isTest static void test_bl_TimeReporting_Trigger_UPDATE_ChangeRequest() {

		// Create Test Data
		createTestData(true);
		clsTestData_ProjectManagement.oMain_ChangeRequest = null;
		clsTestData_ProjectManagement.iRecord_TimeReporting_ChangeRequest = 1;
		List<Change_Request__c> lstCR = clsTestData_ProjectManagement.createChangeRequest(true, true);

		// Perform Logic
		Test.startTest();

		for (Time_Reporting__c oTimeReporting : lstTimeReporting_ChangeRequest){
			oTimeReporting.Change_Request__c = lstCR[0].id;
		}

		update lstTimeReporting;

		Test.stopTest();


		// Validate Logic

	}

	@isTest static void test_bl_TimeReporting_Trigger_UPDATE_Release() {

		// Create Test Data
		createTestData(true);
		clsTestData_ProjectManagement.oMain_Release = null;
		clsTestData_ProjectManagement.iRecord_TimeReporting_Release = 1;
		List<Project__c> lstRelease = clsTestData_ProjectManagement.createRelease(true);

		// Perform Logic
		Test.startTest();

		for (Time_Reporting__c oTimeReporting : lstTimeReporting_Release){
			oTimeReporting.Release__c = lstRelease[0].id;
		}

		update lstTimeReporting;

		Test.stopTest();


		// Validate Logic

	}


	@isTest static void test_bl_TimeReporting_Trigger_UPDATE_Project() {

		// Create Test Data
		createTestData(true);
		clsTestData_ProjectManagement.oMain_ProjectManagement = null;
		clsTestData_ProjectManagement.iRecord_TimeReporting_ProjectManagement = 1;
		List<Project_Management__c> lstProjectManagement = clsTestData_ProjectManagement.createProjectManagement(true);

		// Perform Logic
		Test.startTest();

		for (Time_Reporting__c oTimeReporting : lstTimeReporting_Project){
			oTimeReporting.Project__c = lstProjectManagement[0].id;
			oTimeReporting.Change_Request__c = null;
			oTimeReporting.Release__c = null;
		}

		update lstTimeReporting;

		Test.stopTest();


		// Validate Logic

	}

	@isTest static void test_bl_TimeReporting_Trigger_DELETE() {

		// Create Test Data
		createTestData(true);

		// Perform Logic
		Test.startTest();

		delete lstTimeReporting;

		Test.stopTest();


		// Validate Logic
	}
	

	@isTest static void test_bl_TimeReporting_Trigger_UNDELETE() {

		// Create Test Data
		createTestData(true);
		delete lstTimeReporting;

		// Perform Logic
		Test.startTest();

		undelete lstTimeReporting;

		Test.stopTest();


		// Validate Logic
	}

}