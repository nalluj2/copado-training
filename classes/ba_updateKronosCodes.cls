global without sharing class ba_updateKronosCodes 
						implements Database.Batchable<SObject>{
	
	public List<Id> projectIds = new List<Id>();
	public List<Id> changeRequestIds = new List<Id>();
	public List<Id> releaseIds = new List<Id>();
	
	global Database.QueryLocator start(Database.BatchableContext BC){
		
		String query = 'Select Id, K_Category__c, Kronos_Category__c, K_Project__c, Kronos_Project__c from Time_Reporting__c WHERE ';
		
		if(changeRequestIds.size()>0){
			
			query += 'Change_Request__c IN :changeRequestIds ';
		}
		
		if(releaseIds.size()>0){
			
			if(changeRequestIds.size()>0) query += ' OR ';
			query += 'Release__c IN :releaseIds ';
		}
		
		if(projectIds.size()>0){
			
			if(changeRequestIds.size()>0 || releaseIds.size()>0 ) query += ' OR ';
			query += 'Project__c IN :projectIds ';
		}
		
		return Database.getQueryLocator( query );
	}
	
	global void execute(Database.BatchableContext BC, List<SObject> scope){
		
		List<Time_Reporting__c> timeReports = (List<Time_Reporting__c>) scope;
		
		for(Time_Reporting__c timeReport : timeReports){
			
			timeReport.K_Category__c = timeReport.Kronos_Category__c;
			timeReport.K_Project__c = timeReport.Kronos_Project__c;
		}
		
		update timeReports;
	}
	
	global void finish(Database.BatchableContext BC){
		
	}
}