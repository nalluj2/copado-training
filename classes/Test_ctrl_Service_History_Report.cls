@isTest
private class Test_ctrl_Service_History_Report {

    @testSetup
    private static void createTestData(){
        
        User currentUser = [Select Id from User where Id = :UserInfo.getUserId()];
        
        System.runAs(currentUser){
            
            LW_Configuration__c lwConfig = LW_Configuration__c.getInstance();
            lwConfig.Account_Id__c = '1111';        
            lwConfig.Geography_Id__c = '2222';
            lwConfig.Product_Id__c = '3333';
            lwConfig.Segment_Id__c = '4444';
            lwConfig.URL__c = 'https://boreports.com?App=launch';
            upsert lwConfig;
                        
            BO_Report_Definition__c dbs = new BO_Report_Definition__c();
            dbs.Name = 'Brain Modulation';
            dbs.Fix_Parameters__c = 'lsSpsAsset%20Name=*ALL&lsSpsContract%20Name=*ALL&noDetailsPanel=true&mode=Report';
            dbs.Report_Family__c = 'Service History Report';
            dbs.Report_Id__c = 'ASndYjDPXQVKqm_pm.Nvwhs';
            
            BO_Report_Definition__c neurosurgery = new BO_Report_Definition__c();
            neurosurgery.Name = 'Neurovascular';
            neurosurgery.Fix_Parameters__c = 'lsSpsAsset%20Name=*ALL&lsSpsContract%20Name=*ALL&noDetailsPanel=true&mode=Report';
            neurosurgery.Report_Family__c = 'Service History Report';
            neurosurgery.Report_Id__c = 'ASndYjDPXQVKqm_pm.Nvwhs';
                
            insert new List<BO_Report_Definition__c>{dbs, neurosurgery};
            
            BO_Report_Param_Definition__c neurosurgeryParam = new BO_Report_Param_Definition__c();
            neurosurgeryParam.Name = 'Test'; 
            neurosurgeryParam.Label__c = 'Test';
            neurosurgeryParam.Optional__c = false;
            neurosurgeryParam.Sequence__c = 1;
            neurosurgeryParam.BO_Report_Definition__c = neurosurgery.Id;
                        
            insert neurosurgeryParam;
        }
        
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        Company__c eur = new Company__c();      
        eur.name='Europe';
        eur.CurrencyIsoCode = 'EUR';
        eur.Current_day_in_Q1__c=56;
        eur.Current_day_in_Q2__c=34; 
        eur.Current_day_in_Q3__c=5; 
        eur.Current_day_in_Q4__c= 0;   
        eur.Current_day_in_year__c =200;
        eur.Days_in_Q1__c=56;  
        eur.Days_in_Q2__c=34;
        eur.Days_in_Q3__c=13;
        eur.Days_in_Q4__c=22;
        eur.Days_in_year__c =250;
        eur.Company_Code_Text__c = 'T35';
        insert eur;
        
        Business_Unit_Group__c rtg = new Business_Unit_Group__c();
        rtg.Name = 'Restorative';
        rtg.Master_Data__c = eur.Id;
        insert rtg;
        
        Business_Unit__c oBU1 =  new Business_Unit__c();
        oBU1.Company__c = eur.id;
        oBU1.Name = 'Neurovascular';
        oBU1.Business_Unit_Group__c = rtg.Id;
        
        Business_Unit__c oBU2 =  new Business_Unit__c();
        oBU2.Company__c = eur.id;
        oBU2.Name = 'Cranial Spinal';
        oBU2.Business_Unit_Group__c = rtg.Id;
                             
        insert new List<Business_Unit__c>{oBU1, oBU2};
        
        Sub_Business_Units__c oSBU1 = new Sub_Business_Units__c();
        oSBU1.Name = 'Neurovascular';
        oSBU1.Business_Unit__c = oBU1.id;
        
        Sub_Business_Units__c oSBU2 = new Sub_Business_Units__c();
        oSBU2.Name = 'Brain Modulation';
        oSBU2.Business_Unit__c = oBU2.id;
        
        Sub_Business_Units__c oSBU3 = new Sub_Business_Units__c();
        oSBU3.Name = 'Spine & Biologics';
        oSBU3.Business_Unit__c = oBU2.id;
                
        insert new List<Sub_Business_Units__c>{oSBU1, oSBU2, oSBU3};
    }    
    
    private static testmethod void testPromptSelection(){
    	
    	List<User_Business_Unit__c> userSBUs = new List<User_Business_Unit__c>();
    	
    	for(Sub_Business_Units__c sbu : [Select Id from Sub_Business_Units__c where Name IN ('Neurovascular', 'Brain Modulation')]){
    		
    		User_Business_Unit__c userSBU = new User_Business_Unit__c();
    		userSBU.User__c = UserInfo.getUserId();
    		userSBU.Sub_Business_Unit__c = sbu.Id;
    		
    		userSBUs.add(userSBU);
    	} 
    	System.debug('**BC** userSBUs (' + userSBUs.size() + ') : ' + userSBUs);
        List<Sub_Business_Units__c> lstSBU = [SELECT Id, Name FROM Sub_Business_Units__c];
        System.debug('**BC** lstSBU (' + lstSBU.size() + ') : ' + lstSBU);
    	userSBUs[0].Primary__c = true;
    	
    	insert userSBUs;
    	
    	Test.startTest();
    	
    	Account acc = [Select Id from Account];
    	
    	ctrl_Service_History_Report con = new ctrl_Service_History_Report(new ApexPages.StandardController(acc));
    	    
    	System.assert(con.reports.size() == 2);    
    	
    	con.selectedReport = [Select Id from BO_Report_Definition__c where Name = 'Neurovascular'].Id;
    	
    	con.reportSelected();
    	
    	System.assert(con.promptParams.size() == 1);
    	
    	PageReference pr = con.toReport();
    	
    	System.assert(pr == null);
    	
    	con.promptParams[0].value = 'test value';
    	
    	pr = con.toReport();
    	
    	System.assert(pr != null);
    		   	
    }
    
    private static testmethod void testSingleOption(){
    	
    	List<User_Business_Unit__c> userSBUs = new List<User_Business_Unit__c>();
    	
    	for(Sub_Business_Units__c sbu : [Select Id from Sub_Business_Units__c where Name IN ('Neurovascular', 'Brain Modulation')]){
    		
    		User_Business_Unit__c userSBU = new User_Business_Unit__c();
    		userSBU.User__c = UserInfo.getUserId();
    		userSBU.Sub_Business_Unit__c = sbu.Id;
    		
    		userSBUs.add(userSBU);
    	} 
    	
    	userSBUs[0].Primary__c = true;
    	
    	insert userSBUs;
    	
    	delete [Select Id from BO_Report_Definition__c where Name = 'Neurovascular'];
    	
    	Test.startTest();
    	
    	Account acc = [Select Id from Account];
    	    	
    	ctrl_Service_History_Report con = new ctrl_Service_History_Report(new ApexPages.StandardController(acc));
    	         
    	System.assert(con.reports.size() == 1);    
    	   	
    	PageReference pr = con.autoSelectSingleOption();
    	
    	System.assert(pr != null);
    		   	
    }
            
    private static testmethod void testNoReportAvailable(){
    	
    	Account acc = [Select Id from Account];
    	
    	ctrl_Service_History_Report con = new ctrl_Service_History_Report(new ApexPages.StandardController(acc));
    	    	
    	System.assert(con.reports.size() == 0);
    	System.assert(ApexPages.getMessages().size() == 1); 
    	System.assert(ApexPages.getMessages()[0].getSummary() == 'There is no report available for your business unit configuration');    
    }
    
    private static testmethod void testNoAccount(){
    	    	    	
    	ctrl_Service_History_Report con = new ctrl_Service_History_Report(new ApexPages.StandardController(new Account()));
    	  	
    	System.assert(con.reports.size() == 0);
    	System.assert(ApexPages.getMessages().size() == 1); 
    	System.assert(ApexPages.getMessages()[0].getSummary() == 'No context Account found');    
    }
}