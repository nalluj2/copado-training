@isTest
private class Test_Support_Requests_CVent {

	
	private static testmethod void changeRequest(){
						
		Test.startTest();
		
		ctrl_Support_Requests controller = new ctrl_Support_Requests();			
		System.assert(controller.userLoggedIn==true);
									
		controller.helper_cventRequests.requestCVentChangeRequest();
								
		System.assert(controller.session.request.Priority__c == 'Medium');
										
		controller.session.request.Area_CR__c = 'Test';
		controller.session.request.Description_Long__c = 'blabla';
		controller.session.request.CR_Type__c = 'Problem';
		controller.session.request.Priority__c = 'High';
				
		controller.helper_cventRequests.createCVentRequest();
		
		System.assert(controller.session.request.Name != null);		
	}
	
	private static testmethod void serviceRequest(){
						
		Test.startTest();
		
		ctrl_Support_Requests controller = new ctrl_Support_Requests();			
		System.assert(controller.userLoggedIn==true);
									
		controller.helper_cventRequests.requestCVentServiceRequest();
		System.assert(controller.session.request.request_Type__c == 'Generic Service');
								
		controller.session.request.Area_SR__c = 'Test Area';
		controller.session.request.Event_Name__c = 'Test Event Name';
		controller.session.request.Description_Long__c = 'Test Description';
						
		controller.helper_cventRequests.createCVentRequest();

		System.assert(controller.session.request.Name != null);		
	}
	
	private static testmethod void changeRequestError(){
						
		Test.startTest();
		
		ctrl_Support_Requests controller = new ctrl_Support_Requests();			
		
		controller.helper_cventRequests.requestCVentChangeRequest();													
		controller.session.request.Description_Long__c = 'blabla';
		
		controller.helper_cventRequests.createCVentRequest();
				
		System.assert(controller.session.request.Name == null);
	}
	
	private static testmethod void serviceRequestError(){
						
		Test.startTest();
		
		ctrl_Support_Requests controller = new ctrl_Support_Requests();			
									
		controller.helper_cventRequests.requestCVentServiceRequest();
								
		// TEST 1		
		controller.helper_cventRequests.createCVentRequest();
		System.assert(controller.session.request.Name == null);
		
		// TEST 2
		controller.session.request.Area_SR__c = 'Test Area';
		controller.helper_cventRequests.createCVentRequest();
		System.assert(controller.session.request.Name == null);

		// TEST 3
		controller.session.request.Description_Long__c = 'Test Description';
		controller.session.request.Application__c = null;

		Boolean isError = false;
		
		try{
			controller.helper_cventRequests.createCVentRequest();
		}catch(Exception e){
			isError = true;
		}
		
		System.assert(isError == true);	
	}

	public static void generateBUInformation(){
		
		Company__c cmpny = new Company__c();		
        cmpny.name='Europe';
        cmpny.CurrencyIsoCode = 'EUR';
        cmpny.Current_day_in_Q1__c=56;
        cmpny.Current_day_in_Q2__c=34; 
        cmpny.Current_day_in_Q3__c=5; 
        cmpny.Current_day_in_Q4__c= 0;   
        cmpny.Current_day_in_year__c =200;
        cmpny.Days_in_Q1__c=56;  
        cmpny.Days_in_Q2__c=34;
        cmpny.Days_in_Q3__c=13;
        cmpny.Days_in_Q4__c=22;
        cmpny.Days_in_year__c =250;
        cmpny.Company_Code_Text__c = 'T35';
		insert cmpny;
		
		Business_Unit__c bu =  new Business_Unit__c();
        bu.Company__c =cmpny.id;
        bu.name='BUMedtronic Account Performance';
        bu.Account_Plan_Activities__c=true;        
        insert bu;
	}
}