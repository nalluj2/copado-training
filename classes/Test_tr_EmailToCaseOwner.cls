@isTest(seealldata=false)
private class Test_tr_EmailToCaseOwner {
        
	static testMethod void myUnitTest() {

			Id id_RecordType_Case = clsUtil.getRecordTypeByDevName('Case', 'OMA_Spine_Scientific_Exchange_Product').Id;
			
			User oUser_System1 = clsTestData_User.createUser_SystemAdministrator('tstadm1', false);
			User oUser_System2 = clsTestData_User.createUser_SystemAdministrator('tstadm2', false);
			User oUser = clsTestData_User.createUser('tst001', clsUtil.getUserProfileId('US OMA Spine MedInfo'), null, false);
				oUser.Region_vs__c = 'AMERICAS';
				oUser.Sub_Region__c = 'USA';
				oUser.Country_vs__c = 'USA';
			insert new List<User>{oUser, oUser_System1, oUser_System2};
			
			Task oTask;
			Contact oContact;
			System.runAs(oUser_System1){

				List<Account> lstacc=new List<Account>();
				Account a = new Account() ; 
					a.Name = 'TestAccountOMA'; 
					a.SAP_ID__c = '04556665'; 
					a.RecordTypeId = FinalConstants.sapAccountRecordTypeId ;
				lstacc.add(a);
				Account a1 = new Account() ; 
					a1.Name = 'TestAccountOMA1'; 
					a1.SAP_ID__c = '04556250'; 
					a1.RecordTypeId = FinalConstants.sapAccountRecordTypeId ;
				lstacc.add(a1);
				insert lstacc;

				oContact = new contact(accountid=a.id,LastName='last');
					oContact.FirstName = 'TEST';
					oContact.Phone = '0714820303'; 
					oContact.Email ='test1@contact.com';
					oContact.Contact_Department__c = 'Diabetes Adult';
					oContact.Contact_Primary_Specialty__c = 'ENT';
					oContact.Affiliation_To_Account__c = 'Employee';
					oContact.Primary_Job_Title_vs__c = 'Manager';
					oContact.Contact_Gender__c ='Male';  
				insert oContact;

				system.debug('++++++++1');

				Case cs=new Case(Status='New',ContactId=oContact.Id,Origin='Patient',Therapy_Picklist__c='testClassTh', RecordTypeId = id_RecordType_Case);
				insert cs;
				
				oTask = new task();
					oTask.whatid = cs.id;
					oTask.Status = 'In Progress';
					oTask.Priority = 'Normal';
					oTask.Subject = 'Not Specified';
					oTask.OwnerId = oUser.id;
				insert oTask;

			}

			System.runAs(oUser_System2){

					oTask.Status='Completed';
				update oTask;
			
				Case cs1=new Case(Status='New',ContactId=oContact.Id,Origin='Patient',Therapy_Picklist__c='testClassTh',   Therapy_Use__c='abd',Suggested_Keywords__c='test', RecordTypeId = id_RecordType_Case);
				insert cs1;
				Case cs2=new Case(Status='New',ContactId=oContact.Id,Origin='Patient',Therapy_Use__c='abd',Suggested_Keywords__c='test', RecordTypeId = id_RecordType_Case);
				insert cs2;
				Case cs3=new Case(Status='New',ContactId=oContact.Id,Origin='Patient',Therapy_Use__c='abd', RecordTypeId = id_RecordType_Case);
				insert cs3;

			}

	}    

}