public class SVMX_PS_Service_Report_Hebrew_Controller {
    public SVMXC__Service_Order__c wo;
    public String siteName {get;set;}
    public String companyName{get;set;}
    public String siteStreet{get;set;}
    public String companyStreet1{get;set;}
    public String companyStreet2{get;set;}
    public String siteCity{get;set;}
    public String companyCity{get;set;}
    public String siteState{get;set;}
    public String companyProvice{get;set;}
    public String country = '';
    public String language = '';
    public String genericLangCode = 'en_US';
    public String genericQRCode = 'generic';
    
    public SVMX_PS_Service_Report_Hebrew_Controller(ApexPages.StandardController stdController){
        this.wo = (SVMXC__Service_Order__c) stdController.getRecord();
        country = wo.SVMXC__Country__c;
        if(country == 'Israel'){
            if(wo.SVMXC__Site__r.Name != null)
                siteName = wo.SVMXC__Site__r.Name.reverse();
            if(wo.SVMXC__Company__r.Name != null)
                CompanyName = wo.SVMXC__Company__r.Name.reverse();
            if(wo.SVMXC__Site__r.SVMXC__Street__c != null)
                siteStreet = wo.SVMXC__Site__r.SVMXC__Street__c.reverse();
            if(wo.SVMXC__Company__r.Account_Address_Line_1__c != null)
                companyStreet1 = wo.SVMXC__Company__r.Account_Address_Line_1__c.reverse();
            if(wo.SVMXC__Company__r.Account_Address_Line_2__c != null)
                companyStreet2 = wo.SVMXC__Company__r.Account_Address_Line_2__c.reverse();
            if(wo.SVMXC__Site__r.SVMXC__City__c != null)
                siteCity = wo.SVMXC__Site__r.SVMXC__City__c.reverse();
            if(wo.SVMXC__Company__r.Account_City__c != null)
                companyCity = wo.SVMXC__Company__r.Account_City__c.reverse();
            if(wo.SVMXC__Site__r.SVMXC__State__c != null)
                siteState = wo.SVMXC__Site__r.SVMXC__State__c.reverse();
            if(wo.SVMXC__Company__r.Account_Province__c != null)
                companyProvice = wo.SVMXC__Company__r.Account_Province__c.reverse();
        }
       
    }
    
    public String getDocID(){
        language = wo.SVMX_PS_Language__c;
        Map<String, String> mcs = new Map<String, String>();
        for(SVMX_PS_QR_Code_per_Country__c cs:SVMX_PS_QR_Code_per_Country__c.getAll().values()){
            mcs.put(cs.name.toLowerCase(),cs.documentId__c);
        }
        if(Country != null){
            if(mcs.containsKey(Country.toLowerCase())){
                return mcs.get(Country.toLowerCase());
            }
            else{
                if(language == genericLangCode){
                    return mcs.get(genericQRCode);
                }
                return null;
            }
        }
        else {
            return null;
        }
                
    }
    
    public String getDocURL(){
        language = wo.SVMX_PS_Language__c;
        Map<String, String> mcs = new Map<String, String>();
        for(SVMX_PS_QR_Code_per_Country__c cs:SVMX_PS_QR_Code_per_Country__c.getAll().values()){
            mcs.put(cs.name.toLowerCase(),cs.URL__c);
        }
        if(Country != null){
            if(mcs.containsKey(Country.toLowerCase())){
                return mcs.get(Country.toLowerCase());
            }
            else{
                if(language == genericLangCode){
                    return mcs.get(genericQRCode);
                }
                return null;
            }
        }
        else {
            return null;
        }
                
    }
    
}