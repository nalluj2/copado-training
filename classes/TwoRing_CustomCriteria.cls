global class TwoRing_CustomCriteria {
   global List<string> fields;
   global boolean isTextField;
   global List<List<String>> values;
}