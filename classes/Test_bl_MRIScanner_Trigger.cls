//--------------------------------------------------------------------------------------------------------------------------------
//  Author      :   Bart Caelen
//  Date        :   25/10/2016
//  Description :   APEX TEST Class for the APEX Trigger tr_MRIScanner and APEX Class bl_MRIScanner_Trigger
//  Change Log  :    
//--------------------------------------------------------------------------------------------------------------------------------
@isTest private class Test_bl_MRIScanner_Trigger {
	
	@isTest static void test_SynchronizationService_createNotificationIfChanged_Insert() {
		
		//--------------------------------------------------------
		// Create Test Data
		//--------------------------------------------------------
		// Create Setting Data
		Synchronization_Service_Setting__c oSynchronizationServiceSetting = new Synchronization_Service_Setting__c();
			oSynchronizationServiceSetting.username__c = 'test@medtronic.com';
			oSynchronizationServiceSetting.password__c = 'password';
			oSynchronizationServiceSetting.client_id__c = '123456789';
			oSynchronizationServiceSetting.client_secret__c = 'XXXXX';
			oSynchronizationServiceSetting.is_sandbox__c = true;
			oSynchronizationServiceSetting.Sync_Enabled__c = true;
		insert oSynchronizationServiceSetting;	

		// Create Account Data
		clsTestData_Account.createAccount();

		// Create MRI Scanner Data
		MRI_Scanner__c oMRIScanner = new MRI_Scanner__c();
			oMRIScanner.Account__c = clsTestData_Account.oMain_Account.Id;
			oMRIScanner.MRI_Contact__c = 'MRI Contact';
			oMRIScanner.MRI_IP_Address__c = '100.100.100.100';
			oMRIScanner.MRI_Location__c = 'MRI Location';
			oMRIScanner.MRI_Make__c = 'Siemens';
			oMRIScanner.MRI_Model__c = 'Avanto';
			oMRIScanner.MRI_Strength_T__c = '3.0T';
			oMRIScanner.MRI_SW_Version__c = 'MRI Version';
			oMRIScanner.MRI_Tech__c = 'MRI Tech';
			oMRIScanner.Primary__c = true;
		//--------------------------------------------------------


		//--------------------------------------------------------
		// Execute Logic
		//--------------------------------------------------------
		Test.startTest();
		
		Test_bl_SynchronizationService_Utils.CalloutMock mockImpl = new Test_bl_SynchronizationService_Utils.CalloutMock();
		
		Test.setMock(HttpCalloutMock.class, mockImpl);
		bl_SynchronizationService_Utils.calloutMock = mockImpl;

		insert oMRIScanner;

		Test.stopTest();		
		//--------------------------------------------------------


		//--------------------------------------------------------
		// Validation
		//--------------------------------------------------------
		oMRIScanner = [SELECT External_ID__c FROM MRI_Scanner__c WHERE Id = :oMRIScanner.Id];
		System.assert(oMRIScanner.External_ID__c != null);
		//--------------------------------------------------------

	}

	@isTest static void test_SynchronizationService_createNotificationIfChanged_Update() {
		
		//--------------------------------------------------------
		// Create Test Data
		//--------------------------------------------------------
		// Create Setting Data
		Synchronization_Service_Setting__c oSynchronizationServiceSetting = new Synchronization_Service_Setting__c();
			oSynchronizationServiceSetting.username__c = 'test@medtronic.com';
			oSynchronizationServiceSetting.password__c = 'password';
			oSynchronizationServiceSetting.client_id__c = '123456789';
			oSynchronizationServiceSetting.client_secret__c = 'XXXXX';
			oSynchronizationServiceSetting.is_sandbox__c = true;
			oSynchronizationServiceSetting.Sync_Enabled__c = true;
		insert oSynchronizationServiceSetting;	

		// Create Account Data
		clsTestData_Account.createAccount();

		// Create MRI Scanner Data
		MRI_Scanner__c oMRIScanner = new MRI_Scanner__c();
			oMRIScanner.Account__c = clsTestData_Account.oMain_Account.Id;
			oMRIScanner.MRI_Contact__c = 'MRI Contact';
			oMRIScanner.MRI_IP_Address__c = '100.100.100.100';
			oMRIScanner.MRI_Location__c = 'MRI Location';
			oMRIScanner.MRI_Make__c = 'Siemens';
			oMRIScanner.MRI_Model__c = 'Avanto';
			oMRIScanner.MRI_Strength_T__c = '3.0T';
			oMRIScanner.MRI_SW_Version__c = 'MRI Version';
			oMRIScanner.MRI_Tech__c = 'MRI Tech';
			oMRIScanner.Primary__c = true;
		insert oMRIScanner;
		//--------------------------------------------------------


		//--------------------------------------------------------
		// Execute Logic
		//--------------------------------------------------------
		Test.startTest();
		
		Test_bl_SynchronizationService_Utils.CalloutMock mockImpl = new Test_bl_SynchronizationService_Utils.CalloutMock();
		
		Test.setMock(HttpCalloutMock.class, mockImpl);
		bl_SynchronizationService_Utils.calloutMock = mockImpl;

			oMRIScanner.MRI_Location__c = 'MRI Location 2';
		update oMRIScanner;

		Test.stopTest();		
		//--------------------------------------------------------


		//--------------------------------------------------------
		// Validation
		//--------------------------------------------------------
		oMRIScanner = [SELECT External_ID__c FROM MRI_Scanner__c WHERE Id = :oMRIScanner.Id];
		System.assert(oMRIScanner.External_ID__c != null);
		//--------------------------------------------------------

	}	
	
}
//--------------------------------------------------------------------------------------------------------------------------------