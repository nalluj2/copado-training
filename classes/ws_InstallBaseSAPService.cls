/*
 * Description      : Interface class to send/receive case data from SFDC to Install Base in SAP and back 
 * Author           : Tom Ansley
 * Created Date     : 12-09-2015
 * Change Log       : 
 */
global class ws_InstallBaseSAPService {

	public static final Integer TRIM_ERROR_MSG_LENGTH = 132; 

	webservice static SAPInstallBaseResponse getInstallBaseUpdate(SAPInstallBaseRequest request){
		
		//map the request to an installed product SFDC custom object
		SVMXC__Installed_Product__c prod = bl_InstallBaseSAPService.mapRequestToInstalledProduct(request);

		//map the installed product SFDC custom object to a response.
		SAPInstallBaseResponse response = bl_InstallBaseSAPService.mapInstalledProductToResponse(prod);

		ut_Log.logOutboundMessage(prod.Id, request.INSTALL_PRODUCT_ID, JSON.serializePretty(request), JSON.serializePretty(response), '', 'getInstallBaseUpdate', 'SVMXC__Installed_Product__c');
		
		return response;

	}

	webservice static SAPInstallBaseAck updateInstallBaseAcknowledgement(SAPInstallBaseAck sapInstallBaseAck){

    	SVMXC__Installed_Product__c installedProduct; 
    	   	
    	SAPInstallBaseAck response = new SAPInstallBaseAck();

    	Savepoint sp = Database.setSavepoint();
    	
    	String tNotificationSAPLogStatus = 'FALSE';
	    try{
    	
            clsUtil.debug('updateInstallBaseAcknowledgement - sapInstallBaseAck : ' + sapInstallBaseAck);        

	    	if (sapInstallBaseAck.DISTRIBUTION_STATUS == 'TRUE'){
	    		
                // SUCCESS
				installedProduct = [SELECT Id, SVMX_SAP_Equipment_ID__c FROM SVMXC__Installed_Product__c WHERE SVMX_SAP_Equipment_ID__c = :sapInstallBaseAck.INSTALL_PRODUCT_ID];
				tNotificationSAPLogStatus = 'TRUE';

	    	}else{

                // ERROR OCCURED
				tNotificationSAPLogStatus = 'FALSE';
            }
	    	
	    	response.DISTRIBUTION_STATUS = 'TRUE';

    	}catch (Exception ex){

			tNotificationSAPLogStatus = 'FALSE';

            Database.rollback(sp);
            
            // Construct error message
            String errMsg = (ex.getMessage()).left(TRIM_ERROR_MSG_LENGTH);
            
            //TODO: Update the Sync Notification record with the error occurred. Special case to take into account for the lock mechanism
                 	        	
        	response.DISTRIBUTION_STATUS = 'FALSE';
        	SAPInstallBaseItemAck msg = new SAPInstallBaseItemAck();
        	msg.ERROR_MESSAGE = errMsg;
        	response.DIST_ERROR_DETAILS = new List<SAPInstallBaseItemAck>{msg};     	  
        }

    	response.INSTALL_PRODUCT_ID = sapInstallBaseAck.INSTALL_PRODUCT_ID;
    	
    	String tInstalledProductId = '';
    	if (installedProduct != null){
    		tInstalledProductId = installedProduct.Id;
    	}
		ut_Log.logOutboundMessage(tInstalledProductId, sapInstallBaseAck.INSTALL_PRODUCT_ID, JSON.serializePretty(sapInstallBaseAck), JSON.serializePretty(response), '', 'updateInstallBaseAcknowledgement', 'SVMXC__Installed_Product__c');

        // Update the Notification SAP Log record with the Acknowledgement response
        ut_Log.updateNotificationSAPLogStatus(tInstalledProductId, tNotificationSAPLogStatus, JSON.serializePretty(sapInstallBaseAck), JSON.serializePretty(response));
            	
    	return response;
	}	
    
	global class SAPInstallBaseRequest {
		webservice String INSTALL_PRODUCT_ID;
	}
	
	global class SAPInstallBaseResponse {
		webservice String EQUIPMENT_NUMBER; 
		webservice String USER_STATUS;      
		webservice String INSTALL_CLASS;    
		webservice String CHARACTERISTIC;
		webservice String INVENTORY;
		webservice String OPTIONAL_SOFTWARE;
		webservice String EQUIPMENT_ROOM;
		webservice String LAST_DISINFECTION_DATE;
		webservice String LAST_PM_DATE;
	}
     
	global class SAPInstallBaseAck {
		webservice String MESSAGE;
		webservice String DISTRIBUTION_STATUS;
		webservice String INSTALL_PRODUCT_ID;
		webservice List<SAPInstallBaseItemAck> DIST_ERROR_DETAILS;
	}
	
	global class SAPInstallBaseItemAck {
		webservice String ERROR_MESSAGE;
		webservice String ERROR_NUMBER;
		webservice String ERROR_ID;
		webservice String ERROR_TYPE;
	}
	
}