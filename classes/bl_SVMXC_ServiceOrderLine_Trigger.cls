//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 05-01-2016
//  Description      : APEX Class - Business Logic for tr_SVMXC_ServiceOrderLine
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public class bl_SVMXC_ServiceOrderLine_Trigger {
	
	// When a Work Detail (Time Confirmation or Goods Movement) is created we need to assign the corresponding Operation for the Work Order Technician.
	// This process is done transparently to the User as it is not part of SMAX functionality but required for SAP.
	public static void populateOperationAndTechnician(List<SVMXC__Service_Order_Line__c> triggerNew){
		
		Set<Id> orderIds = new Set<Id>();
		
		for(SVMXC__Service_Order_Line__c orderLine : triggerNew){
			
			if(orderLine.SVMXC__Group_Member__c == null || orderLine.SVMX_Work_Order_Operation__c == null) orderIds.add(orderLine.SVMXC__Service_Order__c);			
		}
				
		if(orderIds.size() > 0){
			
			// Retrieve related Work Order information including Operation data
			Map<Id, SVMXC__Service_Order__c> ordersMap = new Map<Id, SVMXC__Service_Order__c>([Select Id, SVMXC__Group_Member__c, 
																								(Select Id from Work_Order_Operations__r ORDER BY CreatedDate ASC LIMIT 1) 
																							from SVMXC__Service_Order__c where Id IN :orderIds]);
				
			// For each Work Detail we link them to the corresponding Operation and Technician 
			for(SVMXC__Service_Order_Line__c orderLine : triggerNew){
				
				SVMXC__Service_Order__c order = ordersMap.get(orderLine.SVMXC__Service_Order__c);
				
				// Populate the technician that is assigned currently to the Order
				if(orderLine.SVMXC__Group_Member__c == null){
				
					orderLine.SVMXC__Group_Member__c = order.SVMXC__Group_Member__c;
				}
				
				if(orderLine.SVMX_Work_Order_Operation__c == null){
					
					SVMXC_Work_Order_Operation__c operation;					
					if(order.Work_Order_Operations__r.size() > 0) operation = order.Work_Order_Operations__r[0];
					
					// An Operation is mandatory in order to send the data to SAP. In a normal situation there will always be an Operation as they are
					// created automatically when a Work Order is created or the Technician is changed.
					if(operation != null){
						
						orderLine.SVMX_Work_Order_Operation__c = operation.Id;
						
					}else{
						
						orderLine.addError('No Operation defined for parent Work Order');
					}		
				}
			}
		}
	}

	//------------------------------------------------------------------------------------------------
	// SEND EMAILS RELATED TO SERVICE ORDER
	// 	Send email to xxx when a new Part Request is created
	//------------------------------------------------------------------------------------------------	
	
	/*	REPLACED BY NEW INTERFACE LOGIC THAT CREATES THE PART REQUESTS DIRECTLY IN SAP
	
	public static void sendEmail(List<SVMXC__Service_Order_Line__c> triggerNew, String tAction){

		List<Messaging.SingleEmailMessage> lstEmail_Send = new List<Messaging.SingleEmailMessage>();

		if (tAction == 'INSERT'){

			RecordType oRT = clsUtil.getRecordTypeByDevName('SVMXC__Service_Order_Line__c', 'RequestReceipt');
			if (oRT != null){

		    	List<SVMXC__Service_Order_Line__c> lstServiceOrderLine = 
		    		[
		    			SELECT
		    				Id
		    				, SVMXC__Date_Requested__c, SVMXC__Requested_Location__c, SVMXC__Requested_Quantity2__c
		    				, SVMXC__Product__r.Name, SVMXC__Product__r.ProductCode, SVMXC__Product__r.CFN_Code_Text__c
		    				, SVMXC__Service_Order__r.Account_Country__c, SVMXC__Service_Order__r.SVMX_SAP_Service_Order_No__c, SVMXC__Service_Order__r.SVMXC__Service_Group__c
		    				, SVMXC__Service_Order__r.SVMX_FSE_Stock_Location__r.SAP_Plant__c, SVMXC__Service_Order__r.SVMX_FSE_Stock_Location__r.SVMX_SAP_Location_ID__c
		    			FROM
		    				SVMXC__Service_Order_Line__c
		    			WHERE
		    				Id = :triggerNew
		    				AND RecordTypeId = :oRT.Id
	    			];

	    		if (lstServiceOrderLine.size() > 0){
	    			lstEmail_Send.addAll(sendEmail_PartRequest(lstServiceOrderLine));
	    		}
			}	
		}

	    // SEND THE EMAILS
	    if (lstEmail_Send.size() > 0){
			try{
				
				Messaging.sendEmail(lstEmail_Send);   				

			}catch(Exception oEX){}

	    }

	}

	private static List<Messaging.SingleEmailMessage> sendEmail_PartRequest(List<SVMXC__Service_Order_Line__c> lstServiceOrderLine){

		List<Messaging.SingleEmailMessage> lstEmail_Send = new List<Messaging.SingleEmailMessage>();

		try{
			if (lstServiceOrderLine.size() > 0){

				SMAX_Email_Settings__c oEmailSettings = SMAX_Email_Settings__c.getInstance('PART_REQUEST');
				List<String> defaultTO = oEmailSettings.TO__c.split(',');
				//List<String> lstCC = new List<String>();
	            //if (clsUtil.isNull(oEmailSettings.CC__c, '') != ''){
	            //	lstCC = clsUtil.isNull(oEmailSettings.CC__c, '').split(',');
	            //}
	            //List<String> lstBCC = new List<String>();
	            //if (clsUtil.isNull(oEmailSettings.BCC__c, '') != ''){
	            //	lstBCC = clsUtil.isNull(oEmailSettings.BCC__c, '').split(',');
	            //}
			            
				if (oEmailSettings != null){
					for (SVMXC__Service_Order_Line__c oServiceOrderLine : lstServiceOrderLine){

						String tEmail_Subject = oServiceOrderLine.SVMXC__Service_Order__r.Account_Country__c + ' : ' + oEmailSettings.Subject__c;			            
			            
			            SVMXC__Service_Group__c teamSettings = bl_SVMXC_ServiceOrder_Trigger.serviceTeams.get(oServiceOrderLine.SVMXC__Service_Order__r.SVMXC__Service_Group__c);
						
						List<String> toAddresses;
						
						//If the Service Order has a team and the team has email addresses defined we use them. Otherwise we use the default addresses
						if(teamSettings != null && teamSettings.SVMX_Service_Team_emails__c != null) toAddresses = teamSettings.SVMX_Service_Team_emails__c.split(',');						 	
						else toAddresses = defaultTO;	

						String tEmail_Body_HTML = '';
							tEmail_Body_HTML += 'Please arrange to have the below part/s ordered (purchase requisition) for:';
							tEmail_Body_HTML += '<br /><br />';
							tEmail_Body_HTML += '<ul>';
								tEmail_Body_HTML += '<li>SAP SO number : ' + oServiceOrderLine.SVMXC__Service_Order__r.SVMX_SAP_Service_Order_No__c + '</li>';
								tEmail_Body_HTML += '<li>Required delivery date : ' + oServiceOrderLine.SVMXC__Date_Requested__c + '</li>';
								tEmail_Body_HTML += '<li>Plant number : ' + oServiceOrderLine.SVMXC__Service_Order__r.SVMX_FSE_Stock_Location__r.SAP_Plant__c + '</li>';
								tEmail_Body_HTML += '<li>FSE Txxx location : ' + oServiceOrderLine.SVMXC__Service_Order__r.SVMX_FSE_Stock_Location__r.SVMX_SAP_Location_ID__c + '</li>';
								tEmail_Body_HTML += '<li>UPN/CFN : ' + oServiceOrderLine.SVMXC__Product__r.Name + '</li>';
									tEmail_Body_HTML += '<ul>';
										tEmail_Body_HTML += '<li>UPN : ' + oServiceOrderLine.SVMXC__Product__r.ProductCode + '</li>';
										tEmail_Body_HTML += '<li>CFN : ' + oServiceOrderLine.SVMXC__Product__r.CFN_Code_Text__c + '</li>';
									tEmail_Body_HTML += '</ul>';
								tEmail_Body_HTML += '<li>Quantity for each UPN/CFN : ' + oServiceOrderLine.SVMXC__Requested_Quantity2__c + '</li>';
							tEmail_Body_HTML += '</ul>';
							tEmail_Body_HTML += '<br /><br />';

						String tEmail_Body_Text = tEmail_Body_HTML.replace('<br />', '\n').replace('<ul>', '').replace('</ul>', '').replace('<li>', ' * ').replace('</li>', '\n');

						Messaging.SingleEmailMessage oEmail = new Messaging.SingleEmailMessage();
							oEmail.setToAddresses(toAddresses); 
							//if (lstCC.size() > 0) oEmail.setCCAddresses(lstCC);
							//if (lstBCC.size() > 0) oEmail.setBCCAddresses(lstBCC);
							oEmail.setSubject(tEmail_Subject);
							oEmail.setPlainTextBody(tEmail_Body_Text);
							oEmail.setHTMLBody(tEmail_Body_HTML);
						lstEmail_Send.add(oEmail);

				    }
				}else{
					clsUtil.debug('Error in bl_SVMXC_ServiceOrderLine_Trigger.sendEmail_PartRequest - no email setting "PART_REQUEST" defined in "SMAX_Email_Settings__c".');
				}
			}
	    }catch(Exception oEX){
	    	lstEmail_Send = new List<Messaging.SingleEmailMessage>();
	    }

		return lstEmail_Send;

	}
	*/
	//------------------------------------------------------------------------------------------------


	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// Prevent updates on restricted fields - when a restricted field which is not empty, is update it will be reverted to the old value
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public static void preventUpdateOnRestrictedFields(List<SVMXC__Service_Order_Line__c> triggerNew, Map<Id, SVMXC__Service_Order_Line__c> triggerOldMap, String tAction){

		if (tAction == 'UPDATE'){

			// Do not execute for System Administrator - they are allowed to update Restricted Fields
			if (!ProfileMedtronic.isSystemAdministratorProfile(UserInfo.getProfileId())){

				Set<String> setRestrictedFieldName = new Set<String>();
					setRestrictedFieldName.add('SVMXC_Component_SAP_Id__c');
					setRestrictedFieldName.add('SAP_Id__c');

				for (SVMXC__Service_Order_Line__c oData : triggerNew){
					SVMXC__Service_Order_Line__c oData_Old = triggerOldMap.get(oData.Id);

					// If a restricted field is changed while the field was not empty, revert back to the old value
					for (String tFieldName : setRestrictedFieldName){
						if ( 
							(clsUtil.isNull(oData_Old.get(tFieldName), '') != '') 
							&& (oData.get(tFieldName) != oData_Old.get(tFieldName))
						){
							oData.put(tFieldName, oData_Old.get(tFieldName));
						}
					}
				}
			}

		}

	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	public static void defaultQtyOnTimeRegistration(List<SVMXC__Service_Order_Line__c> triggerNew){
		
		Id usage_RTId = RecordTypeMedtronic.getRecordTypeByDevName('SVMXC__Service_Order_Line__c', 'UsageConsumption').Id;
		
		for(SVMXC__Service_Order_Line__c sOrderLine : triggerNew){
			
			if(sOrderLine.RecordTypeId == usage_RTId && (sOrderLine.SVMXC__Line_Type__c == 'Travel' || sOrderLine.SVMXC__Line_Type__c == 'Labor') 
				&& sOrderLine.SVMXC__Actual_Quantity2__c == null){
				
				sOrderLine.SVMXC__Actual_Quantity2__c = 0;
			}			
		}
	}
}
//--------------------------------------------------------------------------------------------------------------------