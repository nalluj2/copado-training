//---------------------------------------------------------------------------------------------------------------------------------
//	Author 			: Bart Caelen
//  Created Date 	: 30-04-2018
//	Description 	: This Class contains the Business Logic that is used on Campaign
//  Change Log 		: 
//---------------------------------------------------------------------------------------------------------------------------------
public class bl_Campaign {

	public static final Set<String> setCampaignStatus_Closed = new Set<String>{'Aborted', 'Completed', 'Cancelled'};

	private static Map<String, Id> mapBusinessUnit_BusinessUnitID;


	//-----------------------------------------------------------------------------------------------------------------------------
	// Update the Business Unit Lookup on Campaign based on the Business Unit Picklist value
	//-----------------------------------------------------------------------------------------------------------------------------
	public static List<Campaign> updateBusinessUnit(List<Campaign> lstCampaign, Boolean bDoUpdate){

		List<Campaign> lstCampaign_Update = new List<Campaign>();

		// We only use the EUR Business Unit becasue we don't have a region defined on Campaign at the moment
		if (mapBusinessUnit_BusinessUnitID == null){
			mapBusinessUnit_BusinessUnitID = new Map<String, Id>();
			List<Business_Unit__c> lstBusinessUnit = [SELECT Id, Name FROM Business_Unit__c WHERE Company__r.Company_Code_Text__c  = 'EUR'];
			for (Business_Unit__c oBusinessUnit : lstBusinessUnit){
				mapBusinessUnit_BusinessUnitID.put(oBusinessUnit.Name, oBusinessUnit.Id);
			}
		}


		for (Campaign oCampaign : lstCampaign){

			if (oCampaign.Business_Unit_Picklist__c != null){

				if (mapBusinessUnit_BusinessUnitID.containsKey(oCampaign.Business_Unit_Picklist__c)){

					Id id_BusinessUnit = mapBusinessUnit_BusinessUnitID.get(oCampaign.Business_Unit_Picklist__c);
					if (oCampaign.Business_Unit__c != id_BusinessUnit){
						oCampaign.Business_Unit__c = mapBusinessUnit_BusinessUnitID.get(oCampaign.Business_Unit_Picklist__c);
						lstCampaign_Update.add(oCampaign);
					}
	
				}else{
	
					oCampaign.Business_Unit__c = null;
					lstCampaign_Update.add(oCampaign);

				}

			}else{

				oCampaign.Business_Unit__c = null;
					lstCampaign_Update.add(oCampaign);

			}

		}

		if (bDoUpdate && lstCampaign_Update.size() > 0){
			update lstCampaign_Update;
		}

		return lstCampaign_Update;

	}
	//-----------------------------------------------------------------------------------------------------------------------------


	//-----------------------------------------------------------------------------------------------------------------------------
	// "Create" a new event linked to a Campaign and Public Calendar
	//-----------------------------------------------------------------------------------------------------------------------------
	public static Event createEvent(Campaign oCampaign, Id id_Calendar, Event oEvent){

		Boolean bIsChanged = false;

		if (oEvent == null){

			// Validate that the required field to create a new Event are available - if not, the event will not be created
			if ( oCampaign.Send_Date__c == null ){
				return null;
			}

			oEvent = new Event();

		}

		if (oEvent.WhatId != oCampaign.Id){
			oEvent.WhatId = oCampaign.Id;
			bIsChanged = true;
		}

		if (oEvent.OwnerId != id_Calendar){
			oEvent.OwnerId = id_Calendar;
			bIsChanged = true;
		}

		if (oEvent.Subject != oCampaign.Name){
			oEvent.Subject = oCampaign.Name;
			bIsChanged = true;
		}
		
		if (oEvent.Description != oCampaign.Description){
			oEvent.Description = oCampaign.Description;
			bIsChanged = true;
		}

		if (oCampaign.Send_Date__c != null){

			if (oEvent.StartDateTime != oCampaign.Send_Date__c){
				oEvent.StartDateTime = oCampaign.Send_Date__c;
				oEvent.EndDateTime = oCampaign.Send_Date__c;
				bIsChanged = true;
			}

//		}else{
//
//			if (oCampaign.StartDate != null){
//
//				Datetime dtStart = Datetime.newInstance(oCampaign.StartDate.year(), oCampaign.StartDate.month(), oCampaign.StartDate.day());
//				if (oEvent.StartDateTime != dtStart){
//					oEvent.StartDateTime = dtStart;
//					bIsChanged = true;
//				}
//
//			}
//
//			if (oCampaign.EndDate != null){
//
//				Datetime dtEnd = Datetime.newInstance(oCampaign.EndDate.year(), oCampaign.EndDate.month(), oCampaign.EndDate.day());
//				if (oEvent.EndDateTime != dtEnd){
//					oEvent.EndDateTime = dtEnd;
//					bIsChanged = true;
//				}
//
//			}
			
		}

		if (bIsChanged){
			return oEvent;
		}else{
			// Nothing Changed 
			return null;
		}

	}
	//-----------------------------------------------------------------------------------------------------------------------------


	//-----------------------------------------------------------------------------------------------------------------------------
	// Read the settings in Campaign_Calendar__c to determine if the processing Campaign should be linnked to Public Calendar.
	//	This method returns the Public Calendar Id of there is one defined for this type of Campaign.
	//-----------------------------------------------------------------------------------------------------------------------------
	private static Map<String, List<Campaign_Calendar__c>> mapCampaignRecordType_CampaignCalendars;
	public static Id getCalendarIdForCampaign(Campaign oCampaign){

		Id id_Calendar;

		if (mapCampaignRecordType_CampaignCalendars == null){

			Map<String, Campaign_Calendar__c> mapCampaignCalendar = Campaign_Calendar__c.getAll();


			mapCampaignRecordType_CampaignCalendars = new Map<String, List<Campaign_Calendar__c>>();
			for (Campaign_Calendar__c oCampaignCalendar : mapCampaignCalendar.values()){

				List<Campaign_Calendar__c> lstCampaignCalendar_Tmp = new List<Campaign_Calendar__c>();
				if (mapCampaignRecordType_CampaignCalendars.containsKey(oCampaignCalendar.Campaign_RecordType_Name__c)){
					lstCampaignCalendar_Tmp = mapCampaignRecordType_CampaignCalendars.get(oCampaignCalendar.Campaign_RecordType_Name__c);
				}
				lstCampaignCalendar_Tmp.add(oCampaignCalendar);
				mapCampaignRecordType_CampaignCalendars.put(oCampaignCalendar.Campaign_RecordType_Name__c, lstCampaignCalendar_Tmp);
			}

		}

		RecordType oRecordType = clsUtil.getRecordTypeById(oCampaign.RecordTypeId);
		if (oRecordType != null){

			if (mapCampaignRecordType_CampaignCalendars.containsKey(oRecordType.DeveloperName)){

				List<Campaign_Calendar__c> lstCampaignCalendar = mapCampaignRecordType_CampaignCalendars.get(oRecordType.DeveloperName);
				for (Campaign_Calendar__c oCampaignCalendar : lstCampaignCalendar){

					if (
						(oCampaign.Type == oCampaignCalendar.Campaign_Type__c)
						&& (oCampaign.Sub_Type__c == oCampaignCalendar.Campaign_Sub_Type__c)
					){
						id_Calendar = oCampaignCalendar.Calendar_Id__c;
						break;
					}

				}

			}

		}

		return id_Calendar;

	}
	//-----------------------------------------------------------------------------------------------------------------------------


}
//---------------------------------------------------------------------------------------------------------------------------------