global class Test_ED_Provisioning_Mock implements HttpCalloutMock{
    
    public Boolean loginError {get; set;}
    public Boolean findUser {get; set;}
    public Boolean findWithGroups {get; set;}
    public Boolean createUserError {get; set;}    
    
    public Test_ED_Provisioning_Mock(){
    	
    	this.loginError = false;
    	this.findUser = true;
    	this.findWithGroups = true;
    	this.createUserError = false;    	
    }
    	
    global HTTPResponse respond(HTTPRequest req) {
		
		String enpoint = req.getEndpoint();
		
		String body = '';
		Integer statusCode = 200;
		
		//Login
		if(enpoint.contains('oauth2/token/bearer')){
			
			bl_ContactEDProvisioning.SessionResponse session = new bl_ContactEDProvisioning.SessionResponse();
			
			if(loginError == false){
				
				session.access_token = 'UnitTestToken';
					
			}else{
				
				session.errorCode = 'ERR';
				session.errorType = 'ERROR';
				session.errorDescription = 'Unit Test Error';				
			}
			
			body = JSON.serializePretty(session);
			
		//Search User	
		}else if(enpoint.contains('wsviewexternaluserprofilesearch')){
			
			bl_ContactEDProvisioning.UserResponse userResp = new bl_ContactEDProvisioning.UserResponse();
			
			if(findUser == false){
				
				userResp.message = 'User not found';
				userResp.Email = 'fake.email@medtronic.com';
				
				findUser = true;//We don't find it first, but then after create we should find it;
				
			}else{
				
				bl_ContactEDProvisioning.ExternalUser userDetails = new bl_ContactEDProvisioning.ExternalUser();
				userDetails.firstname = 'Test';
		        userDetails.lastname = 'User';
		        userDetails.email = 'fake.email@medtronic.com';          
		        userDetails.country = 'Netherlands';      
		        userDetails.languageCode = 'en_uk';
        		userDetails.defaultPassword = 'Medtronic123';
        		userDetails.directoryKey = 'unitTestKey';
				
				userResp.externalUser = userDetails; 
				
				if(findWithGroups == true){
					
					userResp.groups = new List<String>{'TestGroup1'};	
				}				
			}
			
			body = JSON.serializePretty(userResp);
			
		//Create User	
		}else if(enpoint.contains('wscreateuserdirkeyuid')){
			
			bl_ContactEDProvisioning.UserResponse userResp = new bl_ContactEDProvisioning.UserResponse();
			
			if(createUserError == true){
				
				userResp.message = 'Cannot create new User';				
				
			}else{
			
				bl_ContactEDProvisioning.ExternalUser userDetails = new bl_ContactEDProvisioning.ExternalUser();
				userDetails.firstname = 'Test';
		        userDetails.lastname = 'User';
		        userDetails.email = 'fake.email@medtronic.com';          
		        userDetails.country = 'Netherlands';      
		        userDetails.languageCode = 'en_uk';
	    		userDetails.defaultPassword = 'Medtronic123';
	    		userDetails.directoryKey = 'unitTestKey';
				
				userResp.externalUser = userDetails; 
			}
			
			body = JSON.serializePretty(userResp);
			
		//Update User	
		}else if(enpoint.contains('wsmodifyexternaluserprofiles')){
			
			bl_ContactEDProvisioning.UserResponse userResp = new bl_ContactEDProvisioning.UserResponse();
					
			bl_ContactEDProvisioning.ExternalUser userDetails = new bl_ContactEDProvisioning.ExternalUser();
			userDetails.firstname = 'Test';
	        userDetails.lastname = 'User';
	        userDetails.email = 'fake.email@medtronic.com';          
	        userDetails.country = 'Netherlands';      
	        userDetails.languageCode = 'en_uk';
    		userDetails.defaultPassword = 'Medtronic123';
    		userDetails.directoryKey = 'unitTestKey';
			
			userResp.externalUser = userDetails;			
								
			body = JSON.serializePretty(userResp);	
		}
						
        HttpResponse resp = new HttpResponse();
        resp.setHeader('Content-Type', 'application/json');       
        resp.setBody(body);
        resp.setStatusCode(statusCode);
                
        return resp;
    }
    
}