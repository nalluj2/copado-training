//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 09-03-2016
//  Description      : APEX Class - Business Logic for tr_SVMXC_WorkOrderOperation
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public class bl_SVMXC_WorkOrderOperation_Trigger {

	//------------------------------------------------------------------------------------------------
	// Public variables coming from the trigger
	//------------------------------------------------------------------------------------------------
	public static List<SVMXC_Work_Order_Operation__c> lstTriggerNew = new List<SVMXC_Work_Order_Operation__c>();
	public static Map<Id, SVMXC_Work_Order_Operation__c> mapTriggerNew = new Map<Id, SVMXC_Work_Order_Operation__c>();
	public static Map<Id, SVMXC_Work_Order_Operation__c> mapTriggerOld = new Map<Id, SVMXC_Work_Order_Operation__c>();
	//------------------------------------------------------------------------------------------------

	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// Prevent updates on restricted fields - when a restricted field which is not empty, is update it will be reverted to the old value
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public static void preventUpdateOnRestrictedFields(String tAction){

		if (tAction == 'UPDATE'){

			clsUtil.debug('ProfileMedtronic.isSystemAdministratorProfile(UserInfo.getProfileId()) : ' + ProfileMedtronic.isSystemAdministratorProfile(UserInfo.getProfileId()));

			// Do not execute for System Administrator - they are allowed to update Restricted Fields
			if (!ProfileMedtronic.isSystemAdministratorProfile(UserInfo.getProfileId())){

				Set<String> setRestrictedFieldName = new Set<String>();
					setRestrictedFieldName.add('SVMXC_SAP_Id__c');

				for (SVMXC_Work_Order_Operation__c oData : lstTriggerNew){
					SVMXC_Work_Order_Operation__c oData_Old = mapTriggerOld.get(oData.Id);

					// If a restricted field is changed while the field was not empty, revert back to the old value
					for (String tFieldName : setRestrictedFieldName){
						if ( 
							(clsUtil.isNull(oData_Old.get(tFieldName), '') != '') 
							&& (oData.get(tFieldName) != oData_Old.get(tFieldName))
						){
							oData.put(tFieldName, oData_Old.get(tFieldName));
						}
					}
				}

			}

		}

	}
	//----------------------------------------------------------------------------------------------------------------
}