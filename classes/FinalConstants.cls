/**
 * Creation Date:   20090218
 * Description:     FinalConstants helper class to store all Static Final Constants (like hardcoded SFDC Ids)
 *              
 * Author:  ABSI - BC
 */
 
public class FinalConstants {

 // AFFILIATIONS
 public static final String contactToFieldId = 'CF00NS0000000WZX8_lkid';
 public static final String contactFromFieldId = 'CF00NS0000000WZX7_lkid';
 
 public static final String accountToFieldId = 'CF00N20000001ZgWf_lkid';
 public static final String accountFromFieldId = 'CF00N20000001ZXNP_lkid';
 
 public static final String affiliationTypeFieldLabel = 'affType';
 
 public static final String affiliationPrecribingType = 'Prescribing' ; 
 
 // IMPLANTS    
 public static final String refContactFieldId = 'CF00N20000001bGKK_lkid';
 public static final String implContactFieldId = 'CF00N20000001bGJv_lkid';
 public static final String refAccountFieldId = 'CF00N20000001bGKt_lkid';
 public static final String implAccountFieldId = 'CF00N20000001bGK0_lkid';
 public static final String implTherapyFieldId = '00N20000001bGJb';
 public static final String implBUFieldId = '00N20000001bGIZ';
 
  // ACTIVITIES
 public static final String activityBUFieldId = '00N20000001oe8o';
 public static final String activityTherapyFieldId = '00N20000001oe8f';
 
  // CASES
 public static final String caseBUFieldId = '00N20000001oeAu';
 public static final String caseTherapyFieldId = '00N20000001oeAz';

 // CONTACT
 public static final String contactFieldId = 'CF00N20000001bO0M_lkid';

 //PREFIXES
 public static final String implantPrefix = 'a0N';
 public static final String eventPrefix = '00U';
 public static final String taskPrefix = '00T';
 public static final String casePrefix = '500';
 public static final String visitReportPrefix = 'a0Q';
 public static final String accountPrefix = '001';
 public static final String contactPrefix = '003';
 
 //Production : 
 public static final String recordTypeIdC2C = '012200000004z6h';
 public static final String recordTypeIdA2A = '012200000004z6c';
 public static final String recordTypeIdC2A = '012200000004z6S';
 
 public static final String C2CAffiliationPage = 'createC2CAffiliationPage';
 public static final String C2AAffiliationPage = 'createC2AAffiliationPage';
 public static final String A2AAffiliationPage = 'createA2AAffiliationPage';
   
 
 //Profile Ids
 public static final String SystemAdminProfileId = '00e20000000mwLW';
 public static final String CustSystemAdminProfileId = '00e20000000nGvn';
 public static final String DataStewardProfileId = '00e20000000nGvi';
 public static final String StandardUserProfileId = '00e20000000nIqQ';
 public static final String SAPInterfaceProfileId = '00e20000001MaTG';
 
 // DIABETES
 //USer Id
 public static final String RELATIONALJUNCTIONUSERID = '00520000001LbVDAA0' ; 
 
 // Patient account Sandbox 
 //public static final Id patientAccountId = '001R000000L4r6TIAR' ;
 
 // Patient Account Production 
 public static final Id patientAccountId = '0012000000RxC0zAAF' ;
 
 public static final Id sapAccountRecordTypeId = '0122000000050EE' ;
 
 public static final String IL_TYPE_PUMPS = 'Pumps';
 public static final String IL_TYPE_CGMS = 'CGMS';
 
 public static final String unidentified = 'Unidentified' ; 
 
 public static final String NO_ROWS_SELECTED_ERROR = 'Please, select at least one row.' ;
 public static final String NO_VALUE_ENTERED = 'You must enter a value.' ;
 public static final String INVALID_ACCOUNT_SELECTED = 'Selected account is not active or not a salesforce account.' ;
 public static final String NO_DEP_FOR_SFA = 'This department is not available for the selected Sales Force Account.'; 
 
 public static final String DEP_NO_ACTIVE = 'This department is not active for the selected Sales Force Account.';
 public static final String NO_SFDC_ACCOUNT = 'Selected Account is not a Sales Force Account.'; 
 
 public static final String SFACCOUNT_ERROR_MSG_1 = 'Enter a search criteria in order to limit the number of Accounts.' ;   
 public static final String SFACCOUNT_ERROR_MSG_2 = 'The result of your request contains too many Accounts. Please enter a filter on the left side.' ;
 
 public static final String GREEN_FLAG = 'https://emea.salesforce.com/servlet/servlet.ImageServer?id=01520000000r0WB&oid=00D20000000IYpi&lastMod=1262169442000';
 public static final String ORANGE_FLAG = 'https://emea.salesforce.com/servlet/servlet.ImageServer?id=01520000000r0WG&oid=00D20000000IYpi&lastMod=1262169442000';
 public static final String RED_FLAG = 'https://emea.salesforce.com/servlet/servlet.ImageServer?id=01520000000r0WF&oid=00D20000000IYpi&lastMod=1262169442000';
 
 public static final String DiB_submitResultSuccess = 'You have successfully submitted. You can still make changes if desired.';
 
 public static final String DiB_submitResultFailure = 'There was an error submitting your results. Please contact the data steward';
   
 public static final String N0_MULTIPLE_DEPARTMENTS_ERROR_MSG = 'A Sales Force Account can only have one account segmentation with the same department.' ;
 
 public static final String ACCOUNT_COMPETITOR_RECORDTYPE_ID = '01220000000YYku';
 
 //Bu and SBU
    /*public static final Id Surgical = 'a0VT0000000dNtKMAU'; 
    public static final Id Diabetes = 'a0VT0000000dNtPMAU'; 
    public static final Id SpineBio = 'a0VT0000000dNtFMAU'; 
    public static final Id CRDM     = 'a0VT0000000dNtAMAU';  
    public static final Id Cardio   = 'a0VT0000000dNtBMAU'; 
    
     
    public static final Id AFS       = 'a0hT00000008EDXIA2';  
    public static final Id Kyphon    = 'a0hT00000008EDrIAM'; 
    public static final Id CoreSpine = 'a0hT00000008EDwIAM';  
    public static final Id CoreCRDM  = 'a0hT00000008EDhIAM';  
    public static final Id SQDM      = 'a0hT00000008EDcIAM';*/
    
    
 public static final String NO_EDIT_DELETE_PERMISSION = 'Insufficient Privileges';
 
 public static final String PARAM_DEFAULT_CHOSEN_PERIOD = 'chosenperiod';
 
 public static final String [] SECURITY_MYSALES_TAB_01 = new String[]{'00e20000001MaTFAA0','00e20000001QHtCAAW'};
    
}