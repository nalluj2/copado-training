@isTest
private class Test_SynchronizationService_PartShipment {
	
	private static testMethod void syncPartShipment(){
		
		Synchronization_Service_Setting__c syncSettings = new Synchronization_Service_Setting__c();
		syncSettings.username__c = 'test@medtronic.com';
		syncSettings.password__c = 'password';
		syncSettings.client_id__c = '123456789';
		syncSettings.client_secret__c = 'XXXXX';
		syncSettings.is_sandbox__c = true;
		syncSettings.sync_Enabled__c = true;
		insert syncSettings;
		
		//Products
		Product2 testProduct = new Product2();
		testProduct.Name = 'PoleStar';
		testProduct.ProductCode = '000999';
		testProduct.isActive = true;
					
		insert testProduct;
		
		//Account
		Account acc = new Account();	
		acc.AccountNumber = '0000000';
		acc.Name = 'Syn Test Account';
		acc.Phone = '+123456789';
		acc.BillingCity = 'Minneapolis';
		acc.BillingCountry = 'UNITED STATES';
		acc.BillingState = 'Minnesota';
		acc.BillingStreet = 'street';
		acc.BillingPostalCode = '123456';
		acc.ST_NAV_Non_SAP_Account__c = true;
		acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non-SAP Account').getRecordTypeId();	
		insert acc;
		
		//Asset
		Asset asset = new Asset();
		asset.AccountId = acc.Id;
		asset.Product2Id = testProduct.Id;
		asset.Asset_Product_Type__c = 'PoleStar N-10';
		asset.Ownership_Status__c = 'Purchased';
		asset.Name = '123456789';
		asset.Serial_Nr__c = '123456789';
		asset.Status = 'Installed';
		asset.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('PoleStar').getRecordTypeId();
		insert asset;
				
		//Complaint				
		Complaint__c comp = new Complaint__c();
		comp.Account_Name__c = acc.Id;
		comp.Asset__c = asset.Id;
		comp.Status__c = 'Open';
		comp.External_Id__c = 'Test_Complaint_Id';
		insert comp;
		
		//Case
		Case newCase = new Case();
		newCase.AccountId = acc.Id;
		newCase.AssetId = asset.Id;
		newCase.RecordTypeId = clsUtil.getRecordTypeByDevName('Case', 'New_Case').Id;
		newCase.External_Id__c = 'Test_Case_Id';				
		insert newCase;
	
		//Workorder
		Workorder__c workOrder = new Workorder__c();
		workOrder.Account__c = acc.Id;
		workOrder.Asset__c = asset.Id;
		workOrder.Status__c = 'In Process';
		workOrder.External_Id__c = 'Test_Work_Order_Id';
		workOrder.RecordTypeId = Schema.SObjectType.Workorder__c.getRecordTypeInfosByName().get('PoleStar System Checkout').getRecordTypeId();
		insert workOrder;
		
		Test.startTest();
		
		Test_bl_SynchronizationService_Utils.CalloutMock mockImpl = new Test_bl_SynchronizationService_Utils.CalloutMock();
		
		Test.setMock(HttpCalloutMock.class, mockImpl);
		bl_SynchronizationService_Utils.calloutMock = mockImpl;
				
		Parts_Shipment__c partShipment = new Parts_Shipment__c();
		partShipment.External_Id__c = 'Test_Parts_Shipment_Id';
		partShipment.Ship_Via__c = 'Ground';		
		partShipment.Account__c = acc.Id;
		partShipment.Asset__c = asset.Id;
		partShipment.Case__c = newCase.Id;	
		partShipment.Complaint__c = comp.Id;	
		partShipment.Workorder__c = workOrder.Id;		
		insert partShipment;
							
		SynchronizationService_PartShipment partShipmentService = new SynchronizationService_PartShipment();
		String payload = partShipmentService.generatePayload('Test_Parts_Shipment_Id');
		
		System.assert(payload != null);
		
		partShipmentService.processPayload(null);
			
	}
}