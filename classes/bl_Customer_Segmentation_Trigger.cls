public without sharing class bl_Customer_Segmentation_Trigger {
    
    public static void setUniqueKey(List<Customer_Segmentation__c> triggerNew){
    	
    	for(Customer_Segmentation__c segmentation : triggerNew){
    	
	    	segmentation.Unique_Key__c = segmentation.Account__c ;
	    	
	    	if(segmentation.Sub_Business_Unit__c != null) segmentation.Unique_Key__c += ':' + segmentation.Sub_Business_Unit__c;
	    	else if(segmentation.Business_Unit_Group__c != null) segmentation.Unique_Key__c += ':' + segmentation.Business_Unit_Group__c;
	    	
	    	segmentation.Unique_Key__c += ':' + segmentation.Fiscal_Year__c;
	    }
    }
    
    public static void matchInputs(List<Customer_Segmentation__c> triggerNew, Map<Id, Customer_Segmentation__c> oldMap){
    	
    	Map<String, Id> keysMap = new Map<String, Id>();
    	
    	for(Customer_Segmentation__c segmentation : triggerNew){
    		
    		if(oldMap == null || segmentation.Unique_Key__c != oldMap.get(segmentation.Id).Unique_Key__c){    				
    			
    			List<String> segmentationKey = segmentation.Unique_Key__c.split(':'); 
    			
    			Integer segmentationFiscalYear = Integer.valueOf(segmentationKey[2].split('FY')[1]);    			
    			String key = segmentationKey[0] + ':' + segmentationKey[1] + ':FY' + (segmentationFiscalYear + 1);
    			
    			keysMap.put(key, segmentation.Id);    			
    		}	
    	}
    	
    	if(keysMap.size() > 0){
    		
    		//Potential Inputs
	    	List<Segmentation_Potential_Input__c> oldPotInputs = [Select Id from Segmentation_Potential_Input__c where Current_Segmentation__c IN :keysMap.values()];
	    	
	    	if(oldPotInputs.size() > 0){
	    		
	    		for(Segmentation_Potential_Input__c potInput : oldPotInputs) potInput.Current_Segmentation__c = null;	    		
	    		update oldPotInputs;
	    	}
	    	
	    	List<Segmentation_Potential_Input__c> potInputs = [Select Id, Segmentation_Key__c from Segmentation_Potential_Input__c where Segmentation_Key__c IN :keysMap.keySet()];
	    	
	    	if(potInputs.size() > 0){
	    		
	    		for(Segmentation_Potential_Input__c potInput : potInputs) potInput.Current_Segmentation__c = keysMap.get(potInput.Segmentation_Key__c);	    		
	    		update potInputs;
	    	}
	    	
	    	// Input Completes
	    	List<UMD_Input_Complete__c> oldInputCompletes = [Select Id from UMD_Input_Complete__c where Current_Segmentation__c IN :keysMap.values()];
	    	
	    	if(oldInputCompletes.size() > 0){
	    		
	    		for(UMD_Input_Complete__c inputComplete : oldInputCompletes) inputComplete.Current_Segmentation__c = null;	    		
	    		update oldInputCompletes;
	    	} 
	    	
	    	List<UMD_Input_Complete__c> inputCompletes = [Select Id, Unique_Key__c from UMD_Input_Complete__c where Unique_Key__c IN :keysMap.keySet()];
	    	
	    	if(inputCompletes.size() > 0){
	    		
	    		for(UMD_Input_Complete__c inputComplete : inputCompletes) inputComplete.Current_Segmentation__c = keysMap.get(inputComplete.Unique_Key__c);	    		
	    		update inputCompletes;
	    	}
    	}
    }    	
}