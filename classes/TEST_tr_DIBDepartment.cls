//------------------------------------------------------------------------------------------------------------
//	Author		:	Bart Caelen
//	Created		:	2014-10-22
//	Description	:   APEX TEST CLASS for the Trigger on DIB_Department__c
//	CR 			:	CR-5831
//------------------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_tr_DIBDepartment {
	
	@isTest static void test_tr_DIBDepartment(){
		//--------------------------------------------------------
		// CREATE TEST DATA
		//--------------------------------------------------------
		// Create Master Data
		clsTestData.createBusinessUnitData();
		clsTestData.createSubBusinessUnitData();

		// Create Account Data
		clsTestData.idRecordType_Account = RecordTypeMedtronic.Account('SAP_Account').Id;
		clsTestData.createAccountData(false);
		clsTestData.oMain_Account.isSales_Force_Account__c = false;
		insert clsTestData.oMain_Account;
		//--------------------------------------------------------

		//--------------------------------------------------------
		// PERFOM TESTING
		//--------------------------------------------------------
		Test.startTest();

		clsTestData.oMain_Account = [SELECT Id, isSales_Force_Account__c FROM Account WHERE Id = :clsTestData.oMain_Account.Id];
		system.assertEquals(clsTestData.oMain_Account.isSales_Force_Account__c, false);

		// Create DIB_Department__c Data
		clsTestData.createDepartmentMasterData();
		clsTestData.createDIBDepartmentData();

		Test.stopTest();

		clsTestData.oMain_Account = [SELECT Id, isSales_Force_Account__c FROM Account WHERE Id = :clsTestData.oMain_Account.Id];
		system.assertEquals(clsTestData.oMain_Account.isSales_Force_Account__c, true);
		//--------------------------------------------------------

	}

}
//------------------------------------------------------------------------------------------------------------