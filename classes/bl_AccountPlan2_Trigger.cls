//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 09-08-2018
//  Description      : APEX Class - Business Logic for tr_AccountPlan2 and bl_AccountPlan2_Trigger
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public class bl_AccountPlan2_Trigger{


	//----------------------------------------------------------------------------------------------------------------
	// Update a Last Modified Date/Time field on Account Plan 2 if one of the related fields are updated
	//----------------------------------------------------------------------------------------------------------------
	public static void updateLastModifiedDateFields(List<Account_Plan_2__c> lstTriggerNew, Map<Id, Account_Plan_2__c> mapTriggerOld){
	
		for (Account_Plan_2__c oAccountPlan2 : lstTriggerNew){
			
			Account_Plan_2__c oAccountPlan2_Old = mapTriggerOld.get(oAccountPlan2.Id);

			if ( 
				(oAccountPlan2.Political__c != oAccountPlan2_Old.Political__c) 
				|| (oAccountPlan2.Economic__c != oAccountPlan2_Old.Economic__c) 
				|| (oAccountPlan2.Legal__c != oAccountPlan2_Old.Legal__c) 
				|| (oAccountPlan2.Environmental__c != oAccountPlan2_Old.Environmental__c) 
				|| (oAccountPlan2.Technological__c != oAccountPlan2_Old.Technological__c) 
				|| (oAccountPlan2.Social__c != oAccountPlan2_Old.Social__c) 
			){

				oAccountPlan2.PESTEL_Last_Modified_Date__c = Datetime.now();
					
			}

			if ( 
				(oAccountPlan2.Other_Strengths__c != oAccountPlan2_Old.Other_Strengths__c) 
				|| (oAccountPlan2.Other_Opportunities__c != oAccountPlan2_Old.Other_Opportunities__c) 
				|| (oAccountPlan2.Other_Weaknesses__c != oAccountPlan2_Old.Other_Weaknesses__c) 
				|| (oAccountPlan2.Other_Threats__c != oAccountPlan2_Old.Other_Threats__c) 
				|| (oAccountPlan2.Strengths__c != oAccountPlan2_Old.Strengths__c) 
				|| (oAccountPlan2.Opportunities__c != oAccountPlan2_Old.Opportunities__c) 
				|| (oAccountPlan2.Weaknesses__c != oAccountPlan2_Old.Weaknesses__c) 
				|| (oAccountPlan2.Threats__c != oAccountPlan2_Old.Threats__c) 
				|| (oAccountPlan2.Needs__c != oAccountPlan2_Old.Needs__c)
			){

				oAccountPlan2.SWOT_Last_Modified_Date__c = Datetime.now();
					
			}
				
			
			if ( 
				(oAccountPlan2.Medtronic_Strengths__c != oAccountPlan2_Old.Medtronic_Strengths__c) 
				|| (oAccountPlan2.Medtronic_Opportunities__c != oAccountPlan2_Old.Medtronic_Opportunities__c) 
				|| (oAccountPlan2.Medtronic_Weaknesses__c != oAccountPlan2_Old.Medtronic_Weaknesses__c) 
				|| (oAccountPlan2.Medtronic_Threats__c != oAccountPlan2_Old.Medtronic_Threats__c) 
				|| (oAccountPlan2.Medtronic_Potential_Synergies__c != oAccountPlan2_Old.Medtronic_Potential_Synergies__c) 
				
			){

				oAccountPlan2.Medtronic_Last_Modified_Date__c = Datetime.now();
					
			}
				

			if ( 
				(oAccountPlan2.Them__c != oAccountPlan2_Old.Them__c) 
				|| (oAccountPlan2.Us__c != oAccountPlan2_Old.Us__c) 
				|| (oAccountPlan2.Fit__c != oAccountPlan2_Old.Fit__c) 
				|| (oAccountPlan2.Proof__c != oAccountPlan2_Old.Proof__c) 
				|| (oAccountPlan2.Medtronic_Potential_Synergies__c != oAccountPlan2_Old.Medtronic_Potential_Synergies__c) 
				
			){

				oAccountPlan2.Care_Abouts_Last_Modified_Date__c = Datetime.now();
					
			}

		}
			
	}
	//----------------------------------------------------------------------------------------------------------------

}
//--------------------------------------------------------------------------------------------------------------------