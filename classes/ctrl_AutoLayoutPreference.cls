public without sharing class ctrl_AutoLayoutPreference {
    
    public Boolean offerSwitch {get; set;}
    public String sf1ChatterGroupId {get; set;}
    private Id targetProfileId;
    private Boolean toMobile;
           
    public ctrl_AutoLayoutPreference(){
            
        User currentUser = [select ProfileId, Profile.name, Previous_Profile_Id__c from User where Id = :UserInfo.getuserId()];
        
        offerSwitch = false;
        toMobile = false;
        
        if(isSF1() == true){
        
            if( currentUser.Profile.Name.endsWith(' - Mobile') == false ){
                
                List<Profile_Mapping__c> mapping = [Select Target_Profile__c from Profile_Mapping__c where Source_Profile__c=:currentUser.Profile.Name];
                
                if(mapping.size()>0){
                    
                    List<Profile> targetProfile = [Select Id, Name from Profile where Name =: mapping[0].Target_Profile__c];
                    
                    if(targetProfile.size()>0){
                        targetProfileId = targetProfile[0].Id;
                        toMobile = true;
                        offerSwitch = true;
                    }
                }
            }
        }else{
        
            if( currentUser.Profile.Name.endsWith(' - Mobile') == true && currentUser.Previous_Profile_Id__c !=null){
                
                targetProfileId = currentUser.Previous_Profile_Id__c;                
                offerSwitch = true;
            }
        }
        
        String groupName = 'Salesforce1 Open Pilot';
        if(Test.isRunningTest() == true) groupName = 'Salesforce1 - UnitTest';
        
        List<CollaborationGroup> chatterGroups = [Select Id from CollaborationGroup where Name =:groupName  AND CollaborationType = 'Public' LIMIT 1]; 
        
        if(chatterGroups.size()>0){
        	
        	sf1ChatterGroupId = chatterGroups[0].Id;
        }
       
    }    
    
    public void switchProfile(){
        
        String remoteURL = SiteUrls__c.getInstance('Support').Url__c+'SwitchLayoutPreference?';
        String params = targetProfileId+'&'+UserInfo.getUserId();
        remoteURL+='params='+EncodingUtil.urlEncode(CryptoUtils.encryptText(params), 'UTF-8');
    
        HttpRequest httpRequest = new Httprequest();
        httpRequest.setMethod('GET');
        httprequest.setEndpoint(remoteURL);
        
        if(Test.isRunningTest() == false) new Http().send(httpRequest);
    
        User currentUser = [select Id, ProfileId, Profile.Name from User where Id = :UserInfo.getuserId()];
        
        if(currentUser.profileId != targetProfileId){
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Something went wrong'));        	
        }else{            
            offerSwitch = false;
        }
        
        //Usage detail
		Application_Usage_Detail__c usage = new Application_Usage_Detail__c();
		usage.Process_Area__c = 'SFDC Foundation';
		usage.Source__c = 'SF1';
		if(toMobile) usage.Action_Type__c = 'Switch to Mobile Profile';
		else usage.Action_Type__c = 'Switch to Full Profile';
		usage.Capability__c = 'Profile Switch';		
		usage.Action_Date__c = Date.today();
		usage.Action_DateTime__c = DateTime.now();		
		usage.User_Id__c = UserInfo.getUserId();
		
		insert usage;
    }

    public Boolean isSF1(){
        if(ApexPages.currentPage().getParameters().get('isdtp') == 'p1' ||
            (ApexPages.currentPage().getParameters().get('retURL') != null && ApexPages.currentPage().getParameters().get('retURL').contains('projectone') )
        ){
            return true;
        }else{
            return false;
        }
    }
}