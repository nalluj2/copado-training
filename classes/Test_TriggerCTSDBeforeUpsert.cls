/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_TriggerCTSDBeforeUpsert{

    static testMethod void SubjectTest() 
    {

    //Insert Company
    Company__c cmpny = new Company__c();
    cmpny.name='TestMedtronic';
    cmpny.CurrencyIsoCode = 'EUR';
    cmpny.Current_day_in_Q1__c=56;
    cmpny.Current_day_in_Q2__c=34; 
    cmpny.Current_day_in_Q3__c=5; 
    cmpny.Current_day_in_Q4__c= 0;   
    cmpny.Current_day_in_year__c =200;
    cmpny.Days_in_Q1__c=56;  
    cmpny.Days_in_Q2__c=34;
    cmpny.Days_in_Q3__c=13;
    cmpny.Days_in_Q4__c=22;
    cmpny.Days_in_year__c =250;
    cmpny.Company_Code_Text__c = 'T13';
    insert cmpny;
          
   //create Business Unit :
    Business_Unit__c BU = new Business_Unit__c();
      BU.Name = 'Test CRHF'; 
    BU.abbreviated_name__c='test_ab1';
    BU.Company__c = cmpny.id; 
    insert BU;
   
   //create Call Activity Type :
    Call_Activity_Type__c CAT = new Call_Activity_Type__c();
    CAT.Name = 'Testing';
    CAT.Company_ID__c = cmpny.id;
    insert CAT;
    
    //create Call Category :
    Call_Category__c CC = new Call_Category__c();
    CC.Name = 'EducationTest';
    CC.Company_ID__c = cmpny.id;
    insert CC;
   
     // create Call Topic Setting : 
     Call_Topic_Settings__c CTS = new Call_Topic_Settings__c();
		 CTS.Business_Unit__c= BU.Id;
		 CTS.Call_Activity_Type__c = CAT.Id;
		 CTS.Call_Category__c = CC.Id;
		 CTS.Region_vs__c = 'Europe';
     insert CTS;
    
    // create Subject : 
     Subject__c subjects = new Subject__c();
     subjects.Name = 'Diabetes';
     subjects.Company_ID__c = cmpny.id;
     insert Subjects;
    
     // Create Call Topic Setting Details 
     Call_Topic_Setting_Details__c CTSD = new  Call_Topic_Setting_Details__c() ; 
     CTSD.Call_Topic_Setting__c = CTS.id;
     CTSD.Subject__c = subjects .Id;
     insert CTSD;

    update CTSD;
    

     // try to insert a Subject that exists already. 
     Call_Topic_Setting_Details__c CTSD2 = new  Call_Topic_Setting_Details__c();
     CTSD2.Call_Topic_setting__c = CTS.Id;
     CTSD2.Subject__c = subjects .Id;
     try{
       insert CTSD2 ; 
        }
     catch(DMLException e){
        System.assert( e.getMessage().contains('This Subject is already present in the Existing Call Topic Setting'), 
                e.getMessage() );
     }
     
   }
 }