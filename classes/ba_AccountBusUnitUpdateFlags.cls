global class ba_AccountBusUnitUpdateFlags implements Schedulable, Database.Batchable<SObject>, Database.Stateful {
    
    private Integer totalProcessedAccounts = 0;
    
    // Schedulable
    global void execute(SchedulableContext ctx){
        Database.executeBatch(new ba_AccountBusUnitUpdateFlags(), 2000);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'select Id from Account where RecordType.DeveloperName = \'Buying_Entity\'';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Account> listAccountBatch) {
        Set<Id> setAccountId = new Set<Id>();
        
        for (Account recAccount : listAccountBatch){
            totalProcessedAccounts++;
            setAccountId.add(recAccount.Id);
        }
        bl_Relationship.updateA2A_BU_Flag(setAccountId);
    }

    global void finish(Database.BatchableContext BC) {
        String emailBody = '';
        
        AsyncApexJob aaj = [SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID =: BC.getJobId() ];
				
		emailBody += 'Total Processed Accounts: ' + totalProcessedAccounts + ' <br/>';
		emailBody += 'Total Number of Errors: ' + aaj.NumberOfErrors + ' <br/><br/>';
								
		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
		email.setTargetObjectId(USerInfo.getUserId());
		email.setSaveAsActivity(false);
		email.setSubject('Account Business Unit Flags batch results ' + Date.today().format());
		email.setHTMLBody(emailBody);
				
		Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{email});
    }

}