/*
 *      Created Date 	: 17-02-2014
 *      Author 			: Bart Caelen
 *      Description 	: TEST Class for the APEX TRIGGER tr_AccountPlanStakeholder_ChatFeed
 */
@isTest private class TEST_tr_Task_ChatterFeed {

    @isTest static void test_tr_Task_ChatterFeed() {
    	
		//------------------------------------------------
		// CREATE TEST DATA
		//------------------------------------------------
//		clsTestData.createTaskData();		// TEST Insert
		List<Task> lstTask = clsTestData.createTaskData(false);
		clsTestData_AccountPlan.createAccountPlan2();
		for (Task oTask : lstTask){
			oTask.WhatId = clsTestData_AccountPlan.oMain_AccountPlan2.Id;
		}
		insert lstTask;
		//------------------------------------------------
		
		// TEST Update
		update clsTestData.oMain_Task;

		// TEST Delete
		delete clsTestData.oMain_Task;
		
		// TEST Undelete
		undelete clsTestData.oMain_Task;
		
    }
}