@isTest
private class Test_MobileAppAssignment {
    
    static testMethod void createMobileAppAssignment() {
        Mobile_app__c m = new Mobile_app__c();
        m.Name = 'Call Recording';
        m.Unique_Key__c = 'UT-CR';
        insert m;
        
        Mobile_app_assignment__c ma = new Mobile_app_assignment__c();
        ma.Mobile_App__c = m.id;
        ma.Active__c = TRUE;
        ma.User__c = UserInfo.getUserId();
        insert ma;
    }
    
    static testMethod void addtoCallRecording() {
        generateUser('TstUsr', 2, false);
        
        List<User> userList = [select id, 
                                      name, 
                                      email,
                                      username 
                                 from user WHERE UserType = 'Standard' 
                                limit 2];
                                
//      User u1 = Test_MobileAppAssignment.createUserData(clsUtil.getTimeStamp() + '@test.medtronic.com', 'BELGIUM', [select id from Profile where name = 'System Administrator'].Id, [select id from UserRole where developername = 'SystemAdministrator'].Id);

        Mobile_app__c m1 = new Mobile_app__c();
        m1.Name = 'Call Recording';
        m1.Active__c = True; 
        m1.Unique_Key__c = 'UT-CR';

        Mobile_app__c m2 = new Mobile_app__c();
        m2.Name = 'APDR';
        m2.Active__c = True; 
        m2.Unique_Key__c = 'UT-APDR';
        
        insert new List<Mobile_app__c>{m1, m2};

// ==================   initial situation           

        Mobile_app_assignment__c ma0 = new Mobile_app_assignment__c();
            ma0.Mobile_App__c = m2.id;
            ma0.Active__c = True;
            ma0.User__c = userList[0].Id;
        insert ma0;         
        
        List<Mobile_App_Assignment__c> maa0List = 
            [
                SELECT Id,User__c, User__r.Username
                FROM Mobile_app_assignment__c
                WHERE 
                    Mobile_app__r.Name = :m1.Name 
                    AND User__r.Username = :userList[0].Username
            ];


        // check if the user was added to the 'Call recording' app      
        System.assertEquals(maa0List.size(), 1);
        System.assertEquals(maa0List[0].User__r.Username, userList[0].Username);
        
// ==================   insert app assignment record

        String tError1 = '';    
        try { delete ma0; }catch(Exception oEX){ tError1 = oEX.getMessage(); }
        if (!String.isBlank(tError1)) System.assert(tError1.contains('You can\'t delete a Mobile App Assignement - pleae de-activate the Mobile App Assignment'));
 
        Mobile_app_assignment__c ma1 = new Mobile_app_assignment__c();
            ma1.Mobile_App__c = m2.id;
            ma1.Active__c = True;
            ma1.User__c = userList[0].Id;

        String tError2 = '';    
        try{ insert ma1; }catch(Exception oEX){ tError2 = oEX.getMessage();}
        if (!String.isBlank(tError2)) System.assert(tError2.contains('duplicate value found'));
         
        //System.assert(tError2.contains('This user can only be assigned to one of the following Apps'));
        
        list<Mobile_app_assignment__c> maa1List = [Select Id,
                                                          User__c,
                                                          User__r.Username
                                                     From Mobile_app_assignment__c
                                                    Where Mobile_app__r.Name = :m1.Name
                                                      And User__r.Username = :userList[0].Username];

        // check if the user wasn't added a second time to the 'Call recording' app         
        System.assertEquals(maa1List.size(), 1);
        System.assertEquals(maa1List[0].User__r.Username, userList[0].Username);
        
// ==================   update app assignment record    
        ma0.Active__c = False;
        update ma0;                 
        
        list<Mobile_app_assignment__c> maa2List = [Select Id,
                                                          User__c,
                                                          User__r.Username
                                                     From Mobile_app_assignment__c
                                                    Where Mobile_app__r.Name = :m1.Name
                                                      And User__r.Username = :userList[0].Username];

        // check if the user wasn't added a second time to the 'Call recording' app         
        System.assertEquals(maa2List.size(), 1);
        System.assertEquals(maa2List[0].User__r.Username, userList[0].Username);
    
        Test.startTest();
        
// ==================   update app assignment record user   
        ma0.User__c = userList[1].Id;
        update ma0;         
        
        list<Mobile_app_assignment__c> maa3List = [Select Id,
                                                          User__c,
                                                          User__r.Username
                                                     From Mobile_app_assignment__c
                                                    Where Mobile_app__r.Name = :m1.Name
                                                      And User__r.Username = :userList[1].Username];

        // check if the second user was added to the 'Call recording' app       
        System.assertEquals(maa3List.size(), 1);
        System.assertEquals(maa3List[0].User__r.Username, userList[1].Username);
        
    }
    
    private static testmethod void testAPDRCrossCheck(){
        
        Mobile_app__c crApp = new Mobile_app__c();
        crApp.Name = 'Call Recording';
        crApp.Active__c = True; 
        crApp.Unique_Key__c = 'UT-CR';
                
        Mobile_app__c cvgApp = new Mobile_app__c();
        cvgApp.Name = 'APDR';
        cvgApp.Active__c = True;
        cvgApp.Unique_Key__c = 'UT-APDR'; 
        
        Mobile_app__c rtgApp = new Mobile_app__c();
        rtgApp.Name = 'APDR for RTG';
        rtgApp.Active__c = True;
        rtgApp.App_Incompatibility_Metrics__c = 'APDR';
        rtgApp.Unique_Key__c = 'UT-APDRTRG'; 

        Mobile_app__c dibApp = new Mobile_app__c();
        dibApp.Name = 'DIB';
        dibApp.Active__c = True;
        dibApp.Unique_Key__c = 'UT-DIB'; 
        
        insert new List<Mobile_app__c>{crApp, cvgApp, rtgApp, dibApp};
        
        User testUser = new User(
            ProfileId = UserInfo.getProfileId(),
            Username = 'unittest@medtronic.com.unittest',
            Alias = 'unitt',
            Email='unittestuser@medtronic.com',
            EmailEncodingKey='UTF-8',
            Firstname='UnitTest',
            Lastname='User',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago',
            DefaultCurrencyIsoCode = 'EUR',
            Alias_Unique__c = 'unitTest',
            Company_Code_Text__c = 'EUR',
            Region_vs__c = 'Europe',
            Sub_Region__c = 'NWE',
            Country_vs__c = 'NETHERLANDS',
            User_Business_Unit_vs__c = 'All');
        insert testUser;
        
        Test.startTest();
        
        Mobile_app_assignment__c cvgAssign = new Mobile_app_assignment__c();
        cvgAssign.User__c = testUser.Id;
        cvgAssign.Mobile_app__c = cvgApp.Id;
        cvgAssign.Active__c = true;
        insert cvgAssign;
        
        Mobile_app_assignment__c rtgAssign = new Mobile_app_assignment__c();
        rtgAssign.User__c = testUser.Id;
        rtgAssign.Mobile_app__c = rtgApp.Id;
        rtgAssign.Active__c = false;
        insert rtgAssign;

        Mobile_app_assignment__c dibAssign = new Mobile_app_assignment__c();
        dibAssign.User__c = testUser.Id;
        dibAssign.Mobile_app__c = dibApp.Id;
        dibAssign.Active__c = false;
        insert dibAssign;
        
        Boolean isError = false;
        
        try{
            
            rtgAssign.Active__c = true;
            update rtgAssign;
            
            
        }catch(DMLException e){
            
            isError = true;
            
            //System.assert(e.getMessage().contains('This user can only be assigned to one of the following Apps:'));
            System.assert(e.getMessage().contains('This user can\'t be assigned to'));
        }   
        
        System.assert(isError == true); 
        
        cvgAssign.Active__c = false;
        rtgAssign.Active__c = true;
        update new List<Mobile_app_assignment__c>{cvgAssign, rtgAssign};                
    }    
       
    public static void generateUser(string loginName, integer nbrOfUsers, boolean isOSSAdmin) {
        List<User> usrLst = new List<User>();
        Profile p = [SELECT Id FROM Profile WHERE Name='EUR Account Data Steward'];
        for(integer i = 0; i < nbrOfUsers; i++) {
            User u = new User (
                Lastname = 'lastname' + i,
                Alias = loginName + i,
                LanguageLocaleKey = 'en_US',
                Email = loginName + i + '@medtronic.com',
                Username = loginName + i + '@medtronic.com.unittest',
                CommunityNickname = 'Nickame' + i,
                ProfileId = p.Id,
                TimeZoneSidKey = 'Europe/Brussels',
                LocaleSidKey = 'en_US',
                EmailEncodingKey =  'UTF-8',            
                Company_Code_Text__c = 'EUR',
                User_Business_Unit_vs__c = 'All',
                User_Sub_Bus__c = 'Aortic,Structural Heart,EST,HST,Coro + PV,Renal Denervation',
                Job_Title_vs__c = 'Country Manager',
                CostCenter__c = 'test' + i,
                Alias_unique__c = loginName + i,
                User_Status__c = 'Current',
                Region_vs__c = 'Europe',
                Sub_Region__c = 'NWE',
                Country_vs__c = 'NETHERLANDS',
                OneStopShop_Admin__c = isOSSAdmin
            );
            usrLst.add(u);
        }
        
        Test_MobileAppAssignment.ins(usrLst);
        
//      return usrLst;
    }
            
    public static Database.Saveresult[] ins (Sobject[] objs) {
        Database.Saveresult[] res;
         if (Test.isRunningTest()) {
              System.runAs(new User(Id = Userinfo.getUserId())) {
              res = database.insert(objs);
              }
            } else {
            res = database.insert(objs);
            }  
        return res;
    }
    
     @testSetup static void testDataSetup (){
        Mobile_app__c crApp = new Mobile_app__c();
        crApp.Name = 'Call Recording';
        crApp.Active__c = True; 
        crApp.Unique_Key__c = 'CRDup';
                
        Mobile_app__c cvgApp = new Mobile_app__c();
        cvgApp.Name = 'APDR';
        cvgApp.Active__c = True;
        cvgApp.App_Incompatibility_Metrics__c = 'APDR for RTG;DIB';
        cvgApp.Unique_Key__c = 'APDRDup'; 
        
        Mobile_app__c rtgApp = new Mobile_app__c();
        rtgApp.Name = 'APDR for RTG';
        rtgApp.Active__c = True;
        rtgApp.App_Incompatibility_Metrics__c = 'APDR;DIB';
        rtgApp.Unique_Key__c = 'APDRTRGDup'; 

        Mobile_app__c dibApp = new Mobile_app__c();
        dibApp.Name = 'DIB';
        dibApp.Active__c = True;
        dibApp.App_Incompatibility_Metrics__c = 'APDR;APDR for RTG';
        dibApp.Unique_Key__c = 'DIBDup';
        
        Test.startTest();
        insert new List<Mobile_app__c>{crApp, cvgApp, rtgApp, dibApp};
        
        User testUser = new User(
            ProfileId = UserInfo.getProfileId(),
            Username = 'inCompTest@medtronic.com.unittest',
            Alias = 'inComp',
            Email='incomptest@medtronic.com',
            EmailEncodingKey='UTF-8',
            Firstname='UnitTest inComp',
            Lastname='Usert',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago',
            DefaultCurrencyIsoCode = 'EUR',
            Alias_Unique__c = 'incomp',
            Company_Code_Text__c = 'EUR',
            Region_vs__c = 'Europe',
            Sub_Region__c = 'NWE',
            Country_vs__c = 'NETHERLANDS',
            User_Business_Unit_vs__c = 'All');
            
            User testUser2 = new User(
            ProfileId = UserInfo.getProfileId(),
            Username = 'inComp2@medtronic.com.unittest',
            Alias = 'inComp2',
            Email='incomp2@medtronic.com',
            EmailEncodingKey='UTF-8',
            Firstname='UnitTest2 inComp',
            Lastname='Usert2',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago',
            DefaultCurrencyIsoCode = 'EUR',
            Alias_Unique__c = 'incom2',
            Company_Code_Text__c = 'EUR',
            Region_vs__c = 'Europe',
            Sub_Region__c = 'NWE',
            Country_vs__c = 'NETHERLANDS',
            User_Business_Unit_vs__c = 'All');
        insert New List<User> {testUser,testUser2};
        
        Test.stopTest();
    
    }
    
    Public Static testMethod void test_AppIncompatabilty (){
        Map<String, Id> appNameIdMap = New Map<String, Id>();
        for(Mobile_app__c app: [SELECT Id,Name FROM Mobile_app__c]){
            appNameIdMap.put(app.Name,app.Id);
        }
        List<User> testUser = [SELECT Id FROM User WHERE UserType = 'Standard'];
        
        Mobile_app_assignment__c adpr = new Mobile_app_assignment__c();
        adpr.User__c = testUser[0].Id;
        adpr.Mobile_app__c = appNameIdMap.get('APDR');
        adpr.Active__c = true;
        
        Mobile_app_assignment__c rtg= new Mobile_app_assignment__c();
        rtg.User__c = testUser[0].Id;
        rtg.Mobile_app__c = appNameIdMap.get('APDR for RTG');
        rtg.Active__c = true;
        
        Mobile_app_assignment__c dib= new Mobile_app_assignment__c();
        rtg.User__c = testUser[0].Id;
        rtg.Mobile_app__c = appNameIdMap.get('DIB');
        rtg.Active__c = true;
        Test.startTest();
        try{
            insert New List<Mobile_app_assignment__c>{dib,rtg,adpr};
        }
        Catch(exception exp){
            
        }
        System.assert([SELECT Id FROM Mobile_app_assignment__c].size()==0);
        
        adpr = new Mobile_app_assignment__c();
        adpr.User__c = testUser[0].Id;
        adpr.Mobile_app__c = appNameIdMap.get('APDR');
        adpr.Active__c = true;

        Insert adpr;
        //ADPR + Default call recording
        
        rtg= new Mobile_app_assignment__c();
        rtg.User__c = testUser[0].Id;
        rtg.Mobile_app__c = appNameIdMap.get('APDR for RTG');
        rtg.Active__c = true;
        
        dib= new Mobile_app_assignment__c();
        rtg.User__c = testUser[0].Id;
        rtg.Mobile_app__c = appNameIdMap.get('DIB');
        rtg.Active__c = true;
        try{
            insert New List<Mobile_app_assignment__c>{dib,rtg};
        }
        Catch(exception exp){
            
        }
        System.assert([SELECT Id FROM Mobile_app_assignment__c].size()==2);
        Test.stopTest();
    }
    
    Public Static testMethod void test_AppIncompwithDiffUseers (){
        Map<String, Id> appNameIdMap = New Map<String, Id>();
        for(Mobile_app__c app: [SELECT Id,Name FROM Mobile_app__c]){
            appNameIdMap.put(app.Name,app.Id);
        }
        List<User> testUser = [SELECT Id FROM User WHERE UserType = 'Standard'];
        
        Mobile_app_assignment__c adpr = new Mobile_app_assignment__c();
        adpr.User__c = testUser[0].Id;
        adpr.Mobile_app__c = appNameIdMap.get('APDR');
        adpr.Active__c = true;
        
        Mobile_app_assignment__c rtg= new Mobile_app_assignment__c();
        rtg.User__c = testUser[1].Id;
        rtg.Mobile_app__c = appNameIdMap.get('APDR for RTG');
        rtg.Active__c = true;
        

        Test.startTest();

        insert New List<Mobile_app_assignment__c>{rtg,adpr};
        //Including default call recording    
        System.assert([SELECT Id FROM Mobile_app_assignment__c].size()==4);
        
       
    }
        
}