/*
 *      Description : This class exposes methods to create / update / delete a Account Plan 2
 *
 *      Author = Rudy De Coninck
 */
@RestResource(urlMapping='/AccountPlan2Service/*')
global with sharing class ws_AccountPlan2Service {

	@HttpPost
	global static String doPost() {
 		RestRequest req = RestContext.request;
			
		System.debug('post called '+req);
		System.debug('post requestBody '+req.requestBody);
			
		String body = req.requestBody.toString();
		IFAccountPlan2Request accountPlanRequest = (IFAccountPlan2Request)System.Json.deserialize(body, IFAccountPlan2Request.class);
			
		System.debug('post requestBody '+accountPlanRequest);
			
		Account_Plan_2__c accountPlan = accountPlanRequest.accountPlan;

		IFAccountPlan2Response resp = new IFAccountPlan2Response();

		//We catch all exception to assure that 
		try{
				
			resp.accountPlan = bl_AccountPlanning.saveAccountPlan2(accountPlan);
			resp.id = accountPlanRequest.id;
	
			resp.success = true;
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
		}
 
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'AccountPlan2' ,req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
  			
		return System.JSON.serialize(resp);		
	}

	@HttpDelete
	global static String doDelete(){
		RestRequest req = RestContext.request;
			
		System.debug('post called '+req);
		System.debug('post requestBody '+req.requestBody);
			
		String body = req.requestBody.toString();
		IFAccountPlan2DeleteRequest accountPlanDeleteRequest = (IFAccountPlan2DeleteRequest)System.Json.deserialize(body, IFAccountPlan2DeleteRequest.class);
			
		System.debug('post requestBody '+accountPlanDeleteRequest);
			
		Account_Plan_2__c accountPlan = accountPlanDeleteRequest.accountPlan;
		IFAccountPlan2DeleteResponse resp = new IFAccountPlan2DeleteResponse();
		
		List<Account_Plan_2__c>accountPlansToDelete = [select id, mobile_id__c from Account_Plan_2__c where Mobile_ID__c = :accountPlan.Mobile_ID__c];
		
		try{
			
			if (accountPlansToDelete !=null && accountPlansToDelete.size()>0 ){	
				
				Account_Plan_2__c accountPlanToDelete = accountPlansToDelete.get(0);
				delete accountPlanToDelete;
				resp.accountPlan = accountPlanToDelete;
				resp.id = accountPlanDeleteRequest.id;
				resp.success = true;
			}else{
				resp.accountPlan = accountPlan;
				resp.id = accountPlanDeleteRequest.id;
				resp.message='AccountPlan not found';
				resp.success = true;
			}
			
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
		}
			
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'AccountPlan2Delete',req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
			
		return System.JSON.serialize(resp);
	}


	global class IFAccountPlan2Request{
		public Account_Plan_2__c accountPlan {get;set;}
		public String id{get;set;}
	}
	
	global class IFAccountPlan2DeleteRequest{
		public Account_Plan_2__c accountPlan {get;set;}
		public String id{get;set;}
	}
		
	global class IFAccountPlan2DeleteResponse{
		public Account_Plan_2__c accountPlan {get;set;}
		public String id{get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}

	global class IFAccountPlan2Response{
		public Account_Plan_2__c accountPlan {get;set;}
		public String id{get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}

}