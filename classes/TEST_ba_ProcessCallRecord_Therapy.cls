@isTest 
private class TEST_ba_ProcessCallRecord_Therapy {

	@isTest
	private static void test_ba_ProcessCallRecord_Therapy() {

		//----------------------------------------------------
		// Create Test Data
		//----------------------------------------------------
		clsTestData_MasterData.createSubBusinessUnit();
		clsTestData_CallRecord.iRecord_CallRecord = 10;
		clsTestData_CallRecord.createCallRecord();
		clsTestData_Contact.createContact();

        Call_Activity_Type__c oCallActivityType = new Call_Activity_Type__c();
	        oCallActivityType.name = 'test C-A-T 1';
	        oCallActivityType.Company_ID__c = clsTestData_MasterData.oMain_Company.Id;
        insert oCallActivityType;

        // Call_Category__c
		Call_Category__c oCallCategory = new Call_Category__c();
			oCallCategory.Company_ID__c = clsTestData_MasterData.oMain_Company.Id;
			oCallCategory.Name = 'test C-C 1';		
		insert oCallCategory;
	
		List<Therapy__c> lstTherapy = clsTestData_Therapy.createTherapy();
		List<Call_Topics__c> lstCallTopic = new List<Call_Topics__c>();
		List<Contact_Visit_Report__c> lstCallContact = new List<Contact_Visit_Report__c>();
		
		for (Call_Records__c oCallRecord : clsTestData_CallRecord.lstCallRecord){
					
			Call_Topics__c oCallTopic = new Call_Topics__c();
				oCallTopic.Call_Records__c = oCallRecord.Id;
				oCallTopic.Call_Activity_Type__c = oCallActivityType.Id;
				oCallTopic.Call_Category__c = oCallCategory.Id;
				oCallTopic.Call_Topic_Therapy__c = lstTherapy[0].Id;
			lstCallTopic.add(oCallTopic);
			
			Contact_Visit_Report__c callContact = new Contact_Visit_Report__c();
				callContact.Call_Records__c = oCallRecord.Id;
				callContact.Attending_Contact__c = clsTestData_Contact.oMain_Contact.Id;
				callContact.Attending_Affiliated_Account__c = clsTestData_Contact.oMain_Contact.AccountId;
			lstCallContact.add(callContact);	

		}
		insert lstCallTopic;
		insert lstCallContact;

		for (Call_Records__c oCallRecord : clsTestData_CallRecord.lstCallRecord){
			oCallRecord.Therapy__c = '';
		}
		update clsTestData_CallRecord.lstCallRecord;

		List<Call_Records__c> lstCallRecord = [SELECT Id, Therapy__c FROM Call_Records__c];
		for (Call_Records__c oCallRecord : lstCallRecord){
			System.assert(String.isBlank(oCallRecord.Therapy__c));
		}
		
		lstCallContact = [SELECT Id, Therapy__c FROM Contact_Visit_Report__c];
		for (Contact_Visit_Report__c oCallContact : lstCallContact){
			System.assert(String.isBlank(oCallContact.Therapy__c));
		}
		//----------------------------------------------------


		//----------------------------------------------------
		// Execute Logic
		//----------------------------------------------------
		Test.startTest();

			ba_ProcessCallRecord_Therapy oBatch = new ba_ProcessCallRecord_Therapy();
				oBatch.tSOQL = 'SELECT Id, Therapy__c FROM Call_Records__c';
				oBatch.tAdditionalEmail = 'bart.caelen@medtronic.com';
			Database.executeBatch(oBatch, 200);

		Test.stopTest();
		//----------------------------------------------------
		

		//----------------------------------------------------
		// Validate Result
		//----------------------------------------------------
		lstCallRecord = [SELECT Id, Therapy__c FROM Call_Records__c];
		for (Call_Records__c oCallRecord : lstCallRecord){
			System.assert(!String.isBlank(oCallRecord.Therapy__c));
		}
		
		lstCallContact = [SELECT Id, Therapy__c FROM Contact_Visit_Report__c];
		for (Contact_Visit_Report__c oCallContact : lstCallContact){
			System.assert(!String.isBlank(oCallContact.Therapy__c));
		}
		//----------------------------------------------------

	}
}