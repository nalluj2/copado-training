//---------------------------------------------------------------------------------------------------------------------------------------------------
//  Author		:    Bart Caelen
//  Date 		:    24/10/2016
//  Description	:    This class centralizes business logic related to Stakeholder Mapping
//---------------------------------------------------------------------------------------------------------------------------------------------------
public class bl_StakeholderMapping {

	public static Stakeholder_Mapping__c saveStakeholderMapping(Stakeholder_Mapping__c oStakeholderMapping){
		
		if (oStakeholderMapping != null){
			upsert oStakeholderMapping Mobile_ID__c;
		}
		
		return oStakeholderMapping;
	}

}
//---------------------------------------------------------------------------------------------------------------------------------------------------