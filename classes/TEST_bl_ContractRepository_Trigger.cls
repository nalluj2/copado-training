//---------------------------------------------------------------------------------------------------------------------------------
//  Author          : Bart Caelen
//  Created Date    : 16/07/2020
//  Description     : APEX TEST Class for bl_ContractRepository_Trigger
//  Change Log      : 
//---------------------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_bl_ContractRepository_Trigger {

    private static Contract_Repository__c oContract_Repository;
    private static User oUser_System = [SELECT Id, Manager_Email__c FROM User WHERE Alias = 'system'];

    //----------------------------------------------------------------------------------------------------------------
    // TEST DATA
    //----------------------------------------------------------------------------------------------------------------
    @isTest static void createTestData() {
        
        // Create Custom Setting Data
        clsTestData_CustomSetting.createHistoryTracking_ContractRepository();

        // Create Account Data
        clsTestData_Account.iRecord_Account_SAPAccount = 2;
        clsTestData_Account.createAccount_SAPAccount(false);
        // Set additional Account fields that will be used to update the Contract Repository record on creation
            clsTestData_Account.lstAccount_SAPAccount[0].SAP_Delivery_Priority_Id__c = '35';
            clsTestData_Account.lstAccount_SAPAccount[0].SAP_Shipping_Conditions_Id__c = 'Y2';
            clsTestData_Account.lstAccount_SAPAccount[0].SAP_Payment_Terms__c = 'Net 365 Days';
            clsTestData_Account.lstAccount_SAPAccount[0].Account_Country_vs__c = 'BELGIUM';

            clsTestData_Account.lstAccount_SAPAccount[1].SAP_Delivery_Priority_Id__c = '30';
            clsTestData_Account.lstAccount_SAPAccount[1].SAP_Shipping_Conditions_Id__c = 'Y1';
            clsTestData_Account.lstAccount_SAPAccount[1].SAP_Payment_Terms__c = 'Net 240 Days';
            clsTestData_Account.lstAccount_SAPAccount[1].Account_Country_vs__c = 'FRANCE';
        insert clsTestData_Account.lstAccount_SAPAccount;

        // Create Account Extended Profile
        oContract_Repository = new Contract_Repository__c();
            oContract_Repository.Account__c = clsTestData_Account.lstAccount_SAPAccount[0].Id;
            oContract_Repository.Primary_Contract_Type__c = 'CareLink';
            oContract_Repository.Comments__c = 'A1';
            oContract_Repository.VDV_Deal_ID__c = 'B1';
            oContract_Repository.VDV_Deal_Type__c = 'Standard';
            oContract_Repository.Payment_Terms__c = 'Net 3 Days';

    }
    //----------------------------------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------------------------------
    // HISTORY TRACKING
    //----------------------------------------------------------------------------------------------------------------
    @isTest static void test_createHistoryTracking_INSERT() {

        // Create Test Data
        createTestData();
        List<History_Tracking__c> lstHistoryTracking = [SELECT Id FROM History_Tracking__c WHERE Contract_Repository__c != null];
        System.assertEquals(lstHistoryTracking.size(), 0);

        // Test/Execute Logic
        Test.startTest();

            System.runAs(oUser_System){

                insert oContract_Repository;

            }

        Test.stopTest();

        // Validate Result
        lstHistoryTracking = [SELECT Id, FieldName__c, FieldLabel__c, OldValue__c, NewValue__c FROM History_Tracking__c WHERE Contract_Repository__c != null];
        System.assertEquals(lstHistoryTracking.size(), 1);
        System.assertEquals(lstHistoryTracking[0].FieldLabel__c, 'Created');

    }

    @isTest static void test_createHistoryTracking_UPDATE() {

        // Create Test Data
        createTestData();
        insert oContract_Repository;

        List<History_Tracking__c> lstHistoryTracking = [SELECT Id FROM History_Tracking__c WHERE Contract_Repository__c != null];
        System.assertEquals(lstHistoryTracking.size(), 1);

        // Test/Execute Logic
        Test.startTest();

            System.runAs(oUser_System){

                    oContract_Repository.Account__c  = clsTestData_Account.lstAccount_SAPAccount[1].Id;
                    oContract_Repository.Comments__c = 'A2';
                    oContract_Repository.VDV_Deal_ID__c = 'B2';
                    oContract_Repository.VDV_Deal_Type__c = 'Tender';
                    oContract_Repository.Delivery_Terms__c = '48 Hours';
                update oContract_Repository;

            }

        Test.stopTest();

        // Validate Result
        lstHistoryTracking = [SELECT Id, FieldName__c, FieldLabel__c, OldValue__c, NewValue__c FROM History_Tracking__c WHERE Contract_Repository__c != null];
        System.assertEquals(lstHistoryTracking.size(), 6);
        Integer iCounter = 0;
        for (History_Tracking__c oHistoryTracking : lstHistoryTracking){

            if (oHistoryTracking.FieldLabel__c == 'Created'){
                iCounter++;
            }else if (oHistoryTracking.FieldName__c == 'Account__c'){
                System.assertEquals(oHistoryTracking.OldValue__c, clsTestData_Account.lstAccount_SAPAccount[0].Name);
                System.assertEquals(oHistoryTracking.NewValue__c, clsTestData_Account.lstAccount_SAPAccount[1].Name);
                iCounter++;
            }else if (oHistoryTracking.FieldName__c == 'Comments__c'){
                System.assertEquals(oHistoryTracking.OldValue__c, 'A1');
                System.assertEquals(oHistoryTracking.NewValue__c, 'A2');
                iCounter++;
            }else if (oHistoryTracking.FieldName__c == 'VDV_Deal_ID__c'){
                System.assertEquals(oHistoryTracking.OldValue__c, 'B1');
                System.assertEquals(oHistoryTracking.NewValue__c, 'B2');
                iCounter++;
            }else if (oHistoryTracking.FieldName__c == 'VDV_Deal_Type__c'){
                System.assertEquals(oHistoryTracking.OldValue__c, 'Standard');
                System.assertEquals(oHistoryTracking.NewValue__c, 'Tender');
                iCounter++;
            }else if (oHistoryTracking.FieldName__c == 'Delivery_Terms__c'){
                System.assertEquals(oHistoryTracking.OldValue__c, '24 Hours');
                System.assertEquals(oHistoryTracking.NewValue__c, '48 Hours');
                iCounter++;
            }
        }
        System.assertEquals(iCounter, 6);

    }
    //----------------------------------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------------------------------
    // POPULATE WITH ACCOUNT DATA
    //----------------------------------------------------------------------------------------------------------------
    @isTest static void test_populateWithAccountData_INSERT() {

        // Create Test Data
        createTestData();


        // Test/Execute Logic
        Test.startTest();

            System.runAs(oUser_System){

                insert oContract_Repository;

            }

        Test.stopTest();


        // Validate Result
        oContract_Repository = [SELECT Id, Account__c, Payment_Terms__c, Delivery_Terms__c, Account_Country__c FROM Contract_Repository__c WHERE Id = :oContract_Repository.Id];
        Account oAccount = [SELECT Id, SAP_Payment_Terms__c, Delivery_Terms__c, Account_Country_vs__c FROM Account WHERE Id = :oContract_Repository.Account__c];
        System.assertEquals(oContract_Repository.Delivery_Terms__c, oAccount.Delivery_Terms__c);
        System.assertEquals(oContract_Repository.Account_Country__c, oAccount.Account_Country_vs__c);
        System.assertEquals(oContract_Repository.Payment_Terms__c,'Net 3 Days');// oAccount.SAP_Payment_Terms__c);This value is entered on creation of contract repository record

    }

    @isTest static void test_populateWithAccountData_UPDATE() {

        // Create Test Data
        createTestData();
        insert oContract_Repository;


        // Test/Execute Logic
        Test.startTest();

            System.runAs(oUser_System){

                oContract_Repository.Account__c = clsTestData_Account.lstAccount_SAPAccount[1].Id;
                update oContract_Repository;

            }

        Test.stopTest();


        // Validate Result
        oContract_Repository = [SELECT Id, Account__c, Payment_Terms__c, Delivery_Terms__c, Account_Country__c FROM Contract_Repository__c WHERE Id = :oContract_Repository.Id];
        Account oAccount = [SELECT Id, SAP_Payment_Terms__c, Delivery_Terms__c, Account_Country_vs__c FROM Account WHERE Id = :oContract_Repository.Account__c];
        Account oAccountNew = [SELECT Id, SAP_Payment_Terms__c, Delivery_Terms__c, Account_Country_vs__c FROM Account WHERE Id = :clsTestData_Account.lstAccount_SAPAccount[0].Id];
        
        System.assertEquals(oContract_Repository.Account__c, clsTestData_Account.lstAccount_SAPAccount[1].Id);
        System.assertEquals(oContract_Repository.Delivery_Terms__c, oAccountNew.Delivery_Terms__c);
        System.assertEquals(oContract_Repository.Account_Country__c, oAccount.Account_Country_vs__c);
        System.assertEquals(oContract_Repository.Payment_Terms__c, 'Net 3 Days');//This value is entered on creation of contract repository record
        
        oContract_Repository.Payment_Terms__c = '';
        oContract_Repository.Delivery_Terms__c = '';
        update oContract_Repository;
        
        Contract_Repository__c oContract_RepositoryNew = [SELECT Id, Account__c, Payment_Terms__c, Delivery_Terms__c, Account_Country__c FROM Contract_Repository__c WHERE Id = :oContract_Repository.Id];
        System.assertEquals(oContract_RepositoryNew.Delivery_Terms__c, oAccount.Delivery_Terms__c);
        System.assertEquals(oContract_RepositoryNew.Account_Country__c, oAccount.Account_Country_vs__c);
        System.assertEquals(oContract_RepositoryNew.Payment_Terms__c, oAccount.SAP_Payment_Terms__c);
        

    }
    //----------------------------------------------------------------------------------------------------------------
        
    private static testMethod void testOwnerTeamMemberCreation(){
                    
        Account testAcc = new Account();
        testAcc.Name = 'Test Account';
        testAcc.Account_Country_vs__c = 'BELGIUM';
        insert testAcc;
                
        Test.startTest();
        
        Contract_Repository__c contract = new Contract_Repository__c();
        contract.Account__c = testAcc.Id;
        contract.Primary_Contract_Type__c = 'Rebate agreement';
        contract.Rebate_Type__c = 'Straight Unit';
        contract.MPG_Code__c = 'A1;A2;A3';
        //contract.Payment_Options__c = 'Credit Note';
        contract.put('Payment_Options__c','Credit Note');
        contract.Pre_Payment_Terms__c = 'Final Payment';
        contract.Payment_Period__c = 'Monthly';     
        contract.Original_Valid_From_Date__c = Date.today();
        contract.Original_Valid_To_Date__c = Date.today().addYears(1);      
        insert contract;
        
        List<Contract_Repository_Team__c> contractTeam = [Select Id, Team_Member__c, Access_Level__c, Contract_Repository_Owner__c from Contract_Repository_Team__c where Contract_Repository__c = :contract.Id];
        
        System.assert(contractTeam.size() == 1);
        System.assert(contractTeam[0].Team_Member__c == UserInfo.getUserId());
        System.assert(contractTeam[0].Access_Level__c == 'Read/Write');
        System.assert(contractTeam[0].Contract_Repository_Owner__c = true); 
        
        
    }
    
    private static testMethod void testMainAccountCreation(){
                    
        Account testAcc1 = new Account();
        testAcc1.Name = 'Test Account 1';
        testAcc1.Account_Country_vs__c = 'BELGIUM';
        
        Account testAcc2 = new Account();
        testAcc2.Name = 'Test Account 2';
        testAcc2.Account_Country_vs__c = 'BELGIUM';
        
        insert new List<Account>{testAcc1, testAcc2};
                
        Test.startTest();
        
        Contract_Repository__c contract = new Contract_Repository__c();
        contract.Account__c = testAcc1.Id;
        contract.Primary_Contract_Type__c = 'Rebate agreement';
        contract.Rebate_Type__c = 'Straight Unit';
        contract.MPG_Code__c = 'A1;A2;A3';
        //contract.Payment_Options__c = 'Credit Note';
        contract.put('Payment_Options__c','Credit Note');
        contract.Pre_Payment_Terms__c = 'Final Payment';
        contract.Payment_Period__c = 'Monthly';     
        contract.Original_Valid_From_Date__c = Date.today();
        contract.Original_Valid_To_Date__c = Date.today().addYears(1);      
        insert contract;
        
        List<Contract_Repository_Account__c> contractAcc = [Select Id, Account__c from Contract_Repository_Account__c where Contract_Repository__c = :contract.Id];
        
        System.assert(contractAcc.size() == 1);
        System.assert(contractAcc[0].Account__c == testAcc1.Id);
        
        contract.Account__c = testAcc2.Id;
        update contract;
        
        contractAcc = [Select Id, Account__c from Contract_Repository_Account__c where Contract_Repository__c = :contract.Id];
        
        System.assert(contractAcc.size() == 1);
        System.assert(contractAcc[0].Account__c == testAcc2.Id);    
    }
      //Test method to check the email reminder date fields in Contract repository are correctly populated or not
    @isTest static void test_populateEmailReminderDates() {

        // Create Master Data
        clsTestData_MasterData.tCountryCode = 'BE';
        clsTestData_MasterData.createCountry();

        // Create Contract Repository Setting Data
        Contract_Repository_Setting__c oContractRepositorySetting = new Contract_Repository_Setting__c();
            oContractRepositorySetting.Country__c = clsTestData_MasterData.oMain_Country.Id;
            oContractRepositorySetting.Contract_Type__c = 'CareLink';
            oContractRepositorySetting.Email_Template_Name_1__c = 'Contract_xBU_End_Date_30';
            oContractRepositorySetting.Email_Template_Name_2__c = 'Contract_xBU_End_Date_60';
            oContractRepositorySetting.Email_Template_Name_3__c = 'Contract_xBU_End_Date_90';
            oContractRepositorySetting.BCC_Email_Address__c = 'test@medtronic.com.invalid';
            oContractRepositorySetting.Default_Access_Level__c = 'Read/Write';
            oContractRepositorySetting.Interface_Active__c = 'No';
            oContractRepositorySetting.Sales_Office__c = null;
            oContractRepositorySetting.X1st_Email_Reminder_of_Days__c = 30;
            oContractRepositorySetting.X2nd_Email_Reminder_of_Days__c = 20;
            oContractRepositorySetting.X3rd_Email_Reminder_of_Days__c = 10;
        insert oContractRepositorySetting;

        // Create Test Data
        createTestData();

        // Test/Execute Logic
        Test.startTest();
        oContract_Repository.Original_Valid_To_Date__c = System.today().addYears(1);
        insert oContract_Repository;

        // Validate Result
        oContract_Repository = [SELECT Id,Email_Reminder_Date_1__c,Email_Reminder_Date_2__c,Email_Reminder_Date_3__c,Original_Valid_To_Date__c FROM Contract_Repository__c WHERE Id = :oContract_Repository.Id];
        
        System.assertEquals(oContract_Repository.Email_Reminder_Date_1__c, oContract_Repository.Original_Valid_To_Date__c.addDays(-30));
        System.assertEquals(oContract_Repository.Email_Reminder_Date_2__c, oContract_Repository.Original_Valid_To_Date__c.addDays(-20));
        System.assertEquals(oContract_Repository.Email_Reminder_Date_3__c, oContract_Repository.Original_Valid_To_Date__c.addDays(-10));
        
        oContract_Repository.Last_Prolonged_to__c= System.today().addYears(2);
        update oContract_Repository;
        
        oContract_Repository = [SELECT Id,Email_Reminder_Date_1__c,Email_Reminder_Date_2__c,Email_Reminder_Date_3__c,Original_Valid_To_Date__c,Last_Prolonged_to__c FROM Contract_Repository__c WHERE Id = :oContract_Repository.Id];
        System.assertEquals(oContract_Repository.Email_Reminder_Date_1__c, oContract_Repository.Last_Prolonged_to__c.addDays(-30));
        System.assertEquals(oContract_Repository.Email_Reminder_Date_2__c, oContract_Repository.Last_Prolonged_to__c.addDays(-20));
        System.assertEquals(oContract_Repository.Email_Reminder_Date_3__c, oContract_Repository.Last_Prolonged_to__c.addDays(-10));
        
        Test.stopTest();
    }     
}
//---------------------------------------------------------------------------------------------------------------------------------