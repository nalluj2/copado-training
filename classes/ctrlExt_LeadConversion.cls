//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 11-04-2016
//  Description      : APEX Controller Extension for VF Page LeadConversion (	)
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public class ctrlExt_LeadConversion {

    //------------------------------------------------------------------------
    // Private Variables
    //------------------------------------------------------------------------
    private transient Savepoint oSavePoint;
    private static String tCountryISOCode = '';
//    private static String tOpportunityCurrencyISOCode = '';
    private static String tFieldSetAccount;
    private static String tFieldSetContact;
    private static String tFieldSetOpportunity;

    private static String tRecordType_Account;
    private static String tRecordType_Contact;
    private static String tRecordType_Opportunity;
    private static String tRecordType_StakeholderMapping;
	private static Boolean bUseMEA_MITG_Standaard_Opportunity = false;

    private static String tLeadConvertStatus = 'Qualified';
    private static String tStakeholderMapping_Role = 'Purchase'; //-BC - 20170807 - CR-14200 - Updated from 'Decision Maker'
    //------------------------------------------------------------------------
    

    //------------------------------------------------------------------------
    // Constructor
    //------------------------------------------------------------------------
    public ctrlExt_LeadConversion(ApexPages.StandardController stdController) {

        initialize(stdController);

    }
    //------------------------------------------------------------------------


    //------------------------------------------------------------------------
    // Getters & Setters
    //------------------------------------------------------------------------
    public Boolean bShowPage { get;set; }

    public Lead oLead { get;set; }
    public Account oAccount { get;set; }
    public Contact oContact { get;set; }
    public Opportunity oOpportunity { get;set; }
    public Boolean bAddOpportunity { get;set; }
    public List<Product> lstProduct { get;set; }
    public Boolean bNewAccountAllowed { get;set; }
    public Boolean bNewContactAllowed { get;set; }
    public Boolean bNewOpportunityAllowed { get;set; }

    public Id idOpportunityProductGroup { 
        get;
        set{
            idOpportunityProductGroup = value;
            oOpportunity.Pricebook2Id = idOpportunityProductGroup;
        } 
    }
    public String tErrorMessage { get;set; }
    public List<Schema.FieldSetMember> lstFieldSet_Account { get;set; }
    public List<Schema.FieldSetMember> lstFieldSet_Contact { get;set; }
    public List<Schema.FieldSetMember> lstFieldSet_Opportunity { get;set; }

    public List<SelectOption> lstOpportunityProductGroup { 
        get{
            List<SelectOption> lstSO = new List<SelectOption>();
            
            List<Pricebook2> lstPriceBook = 
                [
                    SELECT Id, Name, CurrencyISOCode 
                    FROM Pricebook2 
                    WHERE Id = :getPricebookId()
                    ORDER BY Name
                ];

            lstSO.add(new SelectOption('NULL', '--None--'));
            for (Pricebook2 oPricebook : lstPriceBook){
                lstSO.add(new SelectOption(oPricebook.Id, oPricebook.Name + ' (' + tOpportunityCurrencyISOCode + ')'));
            }
            return lstSO;
        }
        private set; 
    }   

    public String tOpportunityCurrencyISOCode { get; private set; } 
    //------------------------------------------------------------------------


    //------------------------------------------------------------------------
    // Private methods
    //------------------------------------------------------------------------
    private void initialize(ApexPages.StandardController stdController){

        bShowPage = true;

        bNewAccountAllowed = false;
        Schema.DescribeSObjectResult oDescribeAccount = Account.sObjectType.getDescribe();
        bNewAccountAllowed = oDescribeAccount.isCreateable();

        bNewContactAllowed = false;
        Schema.DescribeSObjectResult oDescribeContact = Contact.sObjectType.getDescribe();
        bNewContactAllowed = oDescribeContact.isCreateable();

        bNewOpportunityAllowed = false;
        Schema.DescribeSObjectResult oDescribeOpportunity = Opportunity.sObjectType.getDescribe();
        bNewOpportunityAllowed = oDescribeOpportunity.isCreateable();

        bAddOpportunity = false;

        if (!Test.isRunningTest()){
            
            stdController.addFields(new List<String>{
                'Name', 'OwnerId', 'RecordType.Name', 'FirstName', 'LastName', 'Salutation_Text__c', 'Phone', 'Email', 'Title', 'CurrencyIsoCode', 'Description'
                , 'Company', 'Street', 'City', 'State', 'PostalCode', 'Country'
                , 'Lead_Convert_Account__c', 'Lead_Convert_Contact__c', 'Lead_Convert_Opportunity__c', 'Department_Picklist__c', 'Connected_As__c'
                , 'Opportunity_Product_Group__c', 'Lead_Comments__c', 'Email_Opt_In__c', 'Phone_Opt_In__c', 'Education_Opt_In__c', 'Verbal_Marketing_Consent_Action__c', 'Preference_Center__c'
            });
        } 

        try{
            oLead = (Lead)stdController.getRecord();
        }catch(Exception oEX){}

        if (oLead == null){
            bShowPage = false;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'There is no lead defined.'));
            return;
        }

        setGeneralSettings(oLead);

        lstFieldSet_Account = new List<Schema.FieldSetMember>();
        lstFieldSet_Contact = new List<Schema.FieldSetMember>();
        lstFieldSet_Opportunity = new List<Schema.FieldSetMember>();
        try{ lstFieldSet_Account = SObjectType.Account.FieldSets.getMap().get(tFieldSetAccount).getFields(); }catch(Exception oEX){clsUtil.debug('Error retieving FieldSet on Account : ' + oEX.getMessage());}
        try{ lstFieldSet_Contact = SObjectType.Contact.FieldSets.getMap().get(tFieldSetContact).getFields(); }catch(Exception oEX){clsUtil.debug('Error retieving FieldSet on Contact : ' + oEX.getMessage());}
        try{ lstFieldSet_Opportunity = SObjectType.Opportunity.FieldSets.getMap().get(tFieldSetOpportunity).getFields(); }catch(Exception oEX){clsUtil.debug('Error retieving FieldSet on Opportunity : ' + oEX.getMessage());}

        // Get Account, Contact and Opportunity Object
        oAccount = (Account)Account.sObjectType.newSObject(clsUtil.getRecordTypeByDevName('Account', tRecordType_Account).Id, true);
        oContact = (Contact)Contact.sObjectType.newSObject(clsUtil.getRecordTypeByDevName('Contact', tRecordType_Contact).Id, true);
        oOpportunity = (Opportunity)Opportunity.sObjectType.newSObject(clsUtil.getRecordTypeByDevName('Opportunity', tRecordType_Opportunity).Id, true);
        
        // Populate default values - Account
        if (bNewAccountAllowed){
            oAccount.Name = oLead.Company;
            oAccount.Phone = oLead.Phone;
            oAccount.BillingStreet = oLead.Street;
            oAccount.BillingCity = oLead.City;
            oAccount.BillingState = oLead.State;
            oAccount.BillingPostalCode = oLead.PostalCode;
            oAccount.BillingCountry = oLead.Country;
        }
        
        // Populate default values - Contact
        if (bNewContactAllowed){
            oContact.FirstName = oLead.FirstName;
            oContact.LastName = oLead.LastName;
            oContact.Salutation_Text__c = oLead.Salutation_Text__c;
            oContact.Phone = oLead.Phone;
            oContact.Email = oLead.Email;
            oContact.Contact_Department__c = oLead.Department_Picklist__c;
            oContact.Affiliation_To_Account__c = oLead.Connected_As__c;
            oContact.Title = oLead.Title;
			// GDPR related fields
			oContact.Email_Opt_In__c = oLead.Email_Opt_In__c;
			oContact.Phone_Opt_In__c = oLead.Phone_Opt_In__c;
			oContact.Education_Opt_In__c = oLead.Education_Opt_In__c;
			oContact.Verbal_Marketing_Consent_Action__c = oLead.Verbal_Marketing_Consent_Action__c;
			oContact.Preference_Center__c = oLead.Preference_Center__c;
        }

        // Populate default values - Opportunity
        DIB_Country__c oCountry = clsUtil.getCountryByISOCode(tCountryISOCode);
        tOpportunityCurrencyISOCode = '';
        if (oCountry != null){
            tOpportunityCurrencyISOCode = oCountry.CurrencyISOCode;
            oOpportunity.CurrencyISOCode = tOpportunityCurrencyISOCode;
			bUseMEA_MITG_Standaard_Opportunity = oCountry.Use_MEA_MITG_Standaard_Opportunity_RT__c;
        }
//        oOpportunity.CurrencyISOCode = oLead.CurrencyISOCode;

        System.debug('**BC** initialize - tCountryISOCode : ' + tCountryISOCode);
        System.debug('**BC** initialize - oCountry : ' + oCountry);
        System.debug('**BC** initialize - tOpportunityCurrencyISOCode : ' + tOpportunityCurrencyISOCode);

        String tOpportunityProductGroup = clsUtil.isNull(oLead.Opportunity_Product_Group__c, '--None--');
        idOpportunityProductGroup = null;
        if (clsUtil.getMapPricebookName_PricebookId().containsKey(tOpportunityProductGroup)){
            idOpportunityProductGroup = clsUtil.getMapPricebookName_PricebookId().get(tOpportunityProductGroup);
        }

    }

    // This function set some general variables that are also needed in the Remote Action
    @TestVisible private static void setGeneralSettings(Lead oLead){

        if (oLead.Country != null){
            tCountryISOCode = oLead.Country.toUpperCase();
	        DIB_Country__c oCountry = clsUtil.getCountryByISOCode(tCountryISOCode);
			if (oCountry != null){
				bUseMEA_MITG_Standaard_Opportunity = oCountry.Use_MEA_MITG_Standaard_Opportunity_RT__c;
			}

        }


        System.debug('**BC** setGeneralSettings - oLead.Country.toUpperCase() : ' + oLead.Country.toUpperCase());
        System.debug('**BC** setGeneralSettings - tCountryISOCode : ' + tCountryISOCode);

        // FIELDSETS
        tFieldSetAccount = 'LeadConversion_EUR_NewAccount';
        tFieldSetContact = 'LeadConversion_EUR_NewContact';
        tFieldSetOpportunity = 'LeadConversion_EUR_NewOpportunity';

        // ACCOUNT RECORDTYPE
        tRecordType_Account = 'NON_SAP_Account';

        // CONTACT RECORDTYPE
        tRecordType_Contact = 'Generic_Contact';
        
        //This is a temp custom setting, which can be removed after the record type EMEA_MITG_Standard_Opportunity has been activated for Medtronic. 
        EMEA_MITG_Opportunites__c recEMEAMITGOpportunites = [select Id, MITG_EMEA_Available__c from EMEA_MITG_Opportunites__c where Name = 'EMEA MITG Opportunities' ];
        
        tRecordType_Opportunity = 'EUR_MITG_Standard_Opportunity';
		// STAKEHOLDER MAPPING RECORDTYPE
		tRecordType_StakeholderMapping = 'MITG_European_Opportunity_Stakeholder';
 
        if (recEMEAMITGOpportunites.MITG_EMEA_Available__c == false){
			// OPPORTUNITY RECORDTYPE
			if (bUseMEA_MITG_Standaard_Opportunity) tRecordType_Opportunity = 'MEA_MITG_Standard_Opportunity';
        }
        else{
            tRecordType_Opportunity = 'EMEA_MITG_Standard_Opportunity';
            tRecordType_StakeholderMapping = 'MITG_EMEA_Opportunity_Stakeholder';
        }

    }

    private Set<Id> setID_Pricebook = new Set<Id>();
    private Set<Id> getPricebookId(){

        if (setID_Pricebook.size() > 0){
            return setID_Pricebook;
        }
        
        setID_Pricebook = new Set<Id>();

        String tOpportunityRecordTypeId = clsUtil.getRecordTypeByDevName('Opportunity', tRecordType_Opportunity).Id;
        List<Price_Book_Management__c> lstPricebookManagement = 
            [
                SELECT 
                    Price_Book_ID__c
                FROM 
                    Price_Book_Management__c
                WHERE 
                    (
                        (Opportunity_Record_Type_ID__c = :tOpportunityRecordTypeId.left(15))
                        OR
                        (Opportunity_Record_Type_ID__c = :tOpportunityRecordTypeId.left(18))
                    )

            ];
        if (lstPricebookManagement.size() > 0){
            for (Price_Book_Management__c oPricebookManagement : lstPricebookManagement){
                setID_Pricebook.add(oPricebookManagement.Price_Book_ID__c);
            }
        }

        return setID_Pricebook;
    }

    private static void setSBUFieldsonContact(Contact aoContact){

        // Load SBU Fields on Contact related to MITG and set those fields to TRUE on the new Contact
        List<Sub_Business_Units__c> lstSBU_ContactField_MITG = 
            [
                SELECT 
                    Id, Contact_Flag__c 
                FROM 
                    Sub_Business_Units__c 
                WHERE 
                    Contact_Flag__c != null
                    AND Business_Unit__r.Business_Unit_Group__r.Name = 'MITG'
                ORDER BY 
                    Contact_Flag__c
            ];
        Set<String> setFieldContactSBU = new Set<String>();
        for (Sub_Business_Units__c oSBU : lstSBU_ContactField_MITG){
            setFieldContactSBU.add(oSBU.Contact_Flag__c);
        }

        for (String tField : setFieldContactSBU){
            aoContact.put(tField, TRUE);
        }

    }    
    //------------------------------------------------------------------------


    //------------------------------------------------------------------------
    // Public Methods
    //------------------------------------------------------------------------
    public void fetchProducts(){

        lstProduct = new List<Product>();
        if (idOpportunityProductGroup != null){

            String tSOQL = 'SELECT Id, Name, UnitPrice, CurrencyIsoCode';
            tSOQL += ' FROM PricebookEntry';
            tSOQL += ' WHERE IsDeleted = false AND isActive = true';
            tSOQL += ' AND Pricebook2Id = \'' + idOpportunityProductGroup + '\'';
            tSOQL += ' AND CurrencyIsoCode = \'' + oOpportunity.CurrencyIsoCode + '\'';
            tSOQL += ' ORDER BY Name';
            tSOQL += ' LIMIT 800';

            System.debug('**BC** fetchProducts - oOpportunity.CurrencyIsoCode : ' + oOpportunity.CurrencyIsoCode);
            System.debug('**BC** fetchProducts - tSOQL : ' + tSOQL);

            List<PricebookEntry> lstPricebookEntry = Database.query(tSOQL);

            for (PricebookEntry oPricebookEntry : lstPricebookEntry){
                Product oProduct = new Product(oPricebookEntry);
                lstProduct.add(oProduct);
            }

        }
    }
    //------------------------------------------------------------------------


    //------------------------------------------------------------------------
    // Remote Action
    //------------------------------------------------------------------------
    @RemoteAction
    public static Response tryConvert(String tJSON_Lead, String tJSON_Account, String tJSON_Contact, String tJSON_Opportunity, String tJSON_Product, Boolean bAddOpportunity, String tCurrencyISOCode){

        Response oResponse = new Response();

        System.debug('**BC** tryConvert - tCurrencyISOCode : ' + tCurrencyISOCode);

        Savepoint oSavePoint;
        try{

            oSavePoint = Database.setSavepoint();
            clsUtil.bubbleException();

            Lead oLead = (Lead)JSON.deserialize(tJSON_Lead, Lead.class);
            Account oAccount = (Account)JSON.deserialize(tJSON_Account, Account.class);
            Contact oContact = (Contact)JSON.deserialize(tJSON_Contact, Contact.class);
            Opportunity oOpportunity = (Opportunity)JSON.deserialize(tJSON_Opportunity, Opportunity.class);
            List<Product> lstProduct = (List<Product>)JSON.deserialize(tJSON_Product, List<Product>.class);

            setGeneralSettings(oLead);

            oAccount.RecordTypeId = clsUtil.getRecordTypeByDevName('Account', tRecordType_Account).Id;
            oAccount.Created_by_Lead_Conversion__c = true;

            oContact.RecordTypeId = clsUtil.getRecordTypeByDevName('Contact', tRecordType_Contact).Id;
            oContact.Created_by_Lead_Conversion__c = true;

            List<Schema.FieldSetMember> lstFieldSet_Opportunity = new List<Schema.FieldSetMember>();
            if (bAddOpportunity){

                oOpportunity.RecordTypeId = clsUtil.getRecordTypeByDevName('Opportunity', tRecordType_Opportunity).Id;

                // Get Opportunity Field Set
                System.debug('**BC** tryConvert - tFieldSetOpportunity : ' + tFieldSetOpportunity);
                System.debug('**BC** tryConvert - SObjectType.Opportunity.FieldSets.getMap() : ' + SObjectType.Opportunity.FieldSets.getMap());
                lstFieldSet_Opportunity = SObjectType.Opportunity.FieldSets.getMap().get(tFieldSetOpportunity).getFields();
            }

            Database.LeadConvert oLeadConvert = new database.LeadConvert();
            oLeadConvert.setLeadId(oLead.Id);

            // Account Data
            if (!String.isBlank(oLead.Lead_Convert_Account__c)){
                // Use Existing Account
                oLeadConvert.setAccountId(oLead.Lead_Convert_Account__c);
            }else{
                // Create New Account
                insert oAccount;
                oLeadConvert.setAccountId(oAccount.Id); 
            }

            // Contact Data
            if (!String.isBlank(oLead.Lead_Convert_Contact__c)){
                // Use Existing Contact
                oLeadConvert.setContactId(oLead.Lead_Convert_Contact__c);
            }else{
                // Create New Contact
                if (!String.isBlank(oLead.Lead_Convert_Account__c)){
                    oContact.AccountId = olead.Lead_Convert_Account__c;
                }else{
                    oContact.AccountId = oAccount.Id;
                }
                setSBUFieldsonContact(oContact);
                insert oContact;
                oLeadConvert.setContactId(oContact.Id); 
            }

            Boolean bCreateOpportunity = false;
            if (bAddOpportunity && (oLead.Lead_Convert_Opportunity__c == null)){
                bCreateOpportunity = true;
                oLeadConvert.setOpportunityName(oOpportunity.Name);                
            }
            oLeadConvert.setDoNotCreateOpportunity(!bCreateOpportunity);

            oLeadConvert.setOverwriteLeadSource(false);
            oLeadConvert.setSendNotificationEmail(false);
            oLeadConvert.setConvertedStatus(tLeadConvertStatus);

            List<Note> lstNote = [SELECT Id, ParentId, Title, IsPrivate, Body, OwnerId FROM Note WHERE ParentId = :oLead.Id];
            List<Attachment> lstAttachment = [SELECT Id, ParentId, Name, IsPrivate, ContentType, BodyLength, Body, OwnerId, Description FROM Attachment WHERE ParentId = :oLead.Id];
            
            // Convert the Lead
            Database.LeadConvertResult oLeadConvertResult = Database.convertLead(oLeadConvert);

            // Opportunity Data
            Opportunity oOpportunity_Update; 
            if (bAddOpportunity){
                if (bCreateOpportunity){

                    // DO NOT EXECUTE TRIGGER LOGIC THAT IS NOT NEEDED TO PREVENT ERROR 'TOO MANY SOQL QUERIES'
                    clsUtil.setID_DoNotExecuteFor.add(oLeadConvertResult.getOpportunityId());

                    oOpportunity_Update = new Opportunity(Id=oLeadConvertResult.getOpportunityId());
                    for (Schema.FieldSetMember oFieldSetMember : lstFieldSet_Opportunity){
                        // Don't update the Opportunity Name becaus this is populated with a value including the Account Name
                        if (oFieldSetMember.FieldPath == 'Name') continue;
                        oOpportunity_Update.put(oFieldSetMember.FieldPath, oOpportunity.get(oFieldSetMember.FieldPath));
                    }

                        oOpportunity_Update.Description = oLead.Description;
                        oOpportunity_Update.Lead_Comments__c = oLead.Lead_Comments__c;
                        oOpportunity_Update.RecordTypeId = oOpportunity.RecordTypeID;
                        oOpportunity_Update.Created_by_Lead_Conversion__c = true;
                        oOpportunity_Update.CurrencyISOCode = tCurrencyISOCode;
                        if (lstProduct.size() > 0) oOpportunity_Update.Pricebook2Id = oOpportunity.Pricebook2Id;
//                        if (oOpportunity.Pricebook2Id != null) oOpportunity_Update.Pricebook2Id = oOpportunity.Pricebook2Id;
                    update oOpportunity_Update;

                    // Create Stakeholder_Mapping__c record 
                    //  Previous Opportunity Contact Role was automatically created during the standard Lead Conversion
                    //  and this was updated to set isPrimary to true but this field doesn't exist on Stakeholder Mapping
                    Stakeholder_Mapping__c oStakeHolderMapping = new Stakeholder_Mapping__c();
                        oStakeHolderMapping.Opportunity__c = oLeadConvertResult.getOpportunityId();
                        oStakeHolderMapping.Contact__c = oLeadConvertResult.getContactId();
                        oStakeHolderMapping.Role__c = tStakeholderMapping_Role;
                        oStakeHolderMapping.RecordTypeId = clsUtil.getRecordTypeByDevName('Stakeholder_Mapping__c', tRecordType_StakeholderMapping).Id;
                    insert oStakeHolderMapping;

                    // Add OpportunityLineItem Records     
                    List<OpportunityLineItem> lstOLI = new List<OpportunityLineItem>();           
                    for (Product oProduct : lstProduct){
                        if (oProduct.Quantity > 0 && oProduct.UnitPrice >= 0){
                            OpportunityLineItem oOLI = new OpportunityLineItem();
                                oOLI.OpportunityId = oLeadConvertResult.getOpportunityId();
                                oOLI.PricebookEntryId = oProduct.Id;
                                oOLI.Quantity = oProduct.Quantity;
                                oOLI.UnitPrice = oProduct.UnitPrice;
                                oOLI.Description = oProduct.Description;
                            lstOLI.add(oOLI);   
                        }
                    }

                    if (lstOLI.size() > 0 ) insert lstOLI;

                }else if (!String.isBlank(oLead.Lead_Convert_Opportunity__c)){

                    oOpportunity_Update = new Opportunity(id=oLead.Lead_Convert_Opportunity__c);
                        oOpportunity_Update.CampaignId = oLead.Campaign_Most_Recent__c;
                        oOpportunity_Update.Description += '\n' + oLead.Description;
                        oOpportunity_Update.Lead_Comments__c += '\n' + oLead.Lead_Comments__c;
                    update oOpportunity_Update;
                 
                    // Validate if there is already a Stakeholder_Mapping__c record - if not, create it
                    List<Stakeholder_Mapping__c> lstStakeholderMapping = 
                        [
                            SELECT
                                Id, Opportunity__c, Contact__c, Role__c
                            FROM
                                Stakeholder_Mapping__c
                            WHERE
                                Opportunity__c = :oLead.Lead_Convert_Opportunity__c
                                AND Contact__c = :oLeadConvertResult.getContactId()
                            LIMIT 1
                        ];
                    if (lstStakeholderMapping.size() == 0){
                        // Create a new Stakeholder Mapping record
                        Stakeholder_Mapping__c oStakeHolderMapping = new Stakeholder_Mapping__c();
                            oStakeHolderMapping.Opportunity__c = oLead.Lead_Convert_Opportunity__c;
                            oStakeHolderMapping.Contact__c = oLeadConvertResult.getContactId();
                            oStakeHolderMapping.Role__c = tStakeholderMapping_Role;
                            oStakeHolderMapping.RecordTypeId = clsUtil.getRecordTypeByDevName('Stakeholder_Mapping__c', tRecordType_StakeholderMapping).Id;
                        insert oStakeHolderMapping;
                    }else{
                        // Update the existing Stakeholder Mapping record
                        Stakeholder_Mapping__c oStakeHolderMapping = lstStakeholderMapping[0];
                            oStakeHolderMapping.Role__c = tStakeholderMapping_Role;
                        update oStakeHolderMapping;
                    }
                   
                }

                // Copy Notes from the Lead to the new Opportunity - Contact is done automatically
                List<Note> lstNote_Opportunity = new List<Note>();
                for (Note oNote : lstNote){
                    Note oNote_Opportunity = oNote.clone(false,true,false,false);
                    if (bCreateOpportunity){
                        oNote_Opportunity.ParentId = oLeadConvertResult.getOpportunityId();
                    }else if (!String.isBlank(oLead.Lead_Convert_Opportunity__c)){
                        oNote_Opportunity.ParentId = oLead.Lead_Convert_Opportunity__c;
                    }
                    lstNote_Opportunity.add(oNote_Opportunity);
                }
                insert lstNote_Opportunity;    

                // Copy Attachments from the Lead to the new Opportunity - Contact is done automatically
                List<Attachment> lstAttachment_Opportunity = new List<Attachment>();
                for (Attachment oAttachment : lstAttachment)
                {
                    Attachment oAttachment_Opportunity = oAttachment.clone(false,true,false,false);
                    if (bCreateOpportunity){
                        oAttachment_Opportunity.ParentId = oLeadConvertResult.getOpportunityId();
                    }else if (!String.isBlank(oLead.Lead_Convert_Opportunity__c)){
                        oAttachment_Opportunity.ParentId = oLead.Lead_Convert_Opportunity__c;
                    }
                    lstAttachment_Opportunity.add(oAttachment_Opportunity);
                }
                insert lstAttachment_Opportunity; 

            }

            String tID = '';
            if (oOpportunity_Update != null){
                tID = oOpportunity_Update.Id;
            }else{
                tID = oLeadConvertResult.getContactId();
            }
            if (UserInfo.getUserType() != 'Standard'){
                tID = 'partner/' + tID;
            }

            oResponse.forwardTo = '/' + tID; 
            oResponse.success = true;

        }catch(Exception oEX){
            Database.rollback(oSavePoint);
            oResponse.success = false;
            oResponse.message = 'Sorry, there was an error. The lead was not converted. Please notify support. \n' + oEX.getMessage() + ' Line #: ' + oEX.getLineNumber();
            oResponse.forwardTo = null;
        }

        clsUtil.setID_DoNotExecuteFor = new Set<Id>();

        return oResponse;
    }
    //------------------------------------------------------------------------


    //------------------------------------------------------------------------
    // Helper Class
    //------------------------------------------------------------------------
    public class Product{
        public String Id { get;set; }
        public String Name { get;set; }
        public Integer Quantity { get;set; }
        public Decimal UnitPrice { get;set; }
        public String Description { get;set; }
        public String CurrencyISOCode { get;set; }
        
        public Product(PricebookEntry oPricebookEntry){
            Id = oPricebookEntry.Id;
            Name = oPricebookEntry.Name + ' (' + oPricebookEntry.CurrencyIsoCode + ')';
            Quantity = 0;
            UnitPrice = oPricebookEntry.UnitPrice;
            Description = '';
            CurrencyISOCode = oPricebookEntry.CurrencyIsoCode;
        }
    }
    
    public class Response{
        public String message {get;set;}
        public String forwardTo {get;set;}
        public Boolean success {get;set;}
    }
    //------------------------------------------------------------------------

}
//--------------------------------------------------------------------------------------------------------------------