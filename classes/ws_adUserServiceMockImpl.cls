@isTest
global with sharing class ws_adUserServiceMockImpl implements WebServiceMock{
	
	public Boolean forceLoginFail = false;
	
	global void doInvoke(Object stub, Object request, Map<String, Object> response,
           String endpoint, String soapAction, String requestName, String responseNS,
           String responseName, String responseType) {
		
		if(responseName == 'valUserResponse'){
			
			ws_adUserService.valUserResponse_element responseElement = new ws_adUserService.valUserResponse_element();
			
			if(forceLoginFail) responseElement.valUserResult = 'Error';
			else responseElement.valUserResult = 'OK';
			
			response.put('response_x', responseElement);
		}
		
		if(responseName == 'getUserInfoResponse' ){
			
			ws_adUserService.getUserInfoResponse_element responseElement = new ws_adUserService.getUserInfoResponse_element(); 
			responseElement.getUserInfoResult = new ws_adUserService.userinfo();
			responseElement.getUserInfoResult.firstName = 'test';
			responseElement.getUserInfoResult.lastName = 'name';
			responseElement.getUserInfoResult.email = 'mock@medtronic.com';
			responseElement.getUserInfoResult.costcenter = '123456'; 
			responseElement.getUserInfoResult.managerName = 'mock manager';
			responseElement.getUserInfoResult.managerEmail = 'manager.mock@medtronic.com';
			responseElement.getUserInfoResult.peopleSoft = '123456';
			
			response.put('response_x', responseElement);
		}         
   }
}