/******************************************************************************
 * Filename: Contract_Logic.cls
 * Author:   Etherios Inc. for Medtronic Corporation
 * Purpose:  This is a common controller for the Contract object. 
 ****************************************************************************** 
 * Copyright 2012 Etherios Inc. All rights reserved.
 * Customer confidential. Do not distribute.
 ****************************************************************************** 
 * History
 * ----------------------------------------------------------------------------
 * 2012-05-10   Patrick Stephens             Created
 * 2012-08-03   Charles Howard               Removed references to the System.Service_Contract__c field
 *
 ******************************************************************************/

global class Contract_Logic {

/* Not needed in EUR side
public static void deleteEntitlements(List<Contract> contracts){
  List<Entitlement> delEntitlements;
  boolean toDeleteEnt = false;
  boolean toDeleteWo = false;
    Try{
          delEntitlements = [SELECT Id FROM Entitlement WHERE ContractId in :contracts];
          toDeleteEnt = true;
      } catch (exception e){
          //nothing to delete
          toDeleteEnt = false;
      }
  if(toDeleteEnt) {delete delEntitlements;}
} // end deleteEntitlements
*/

//Commented by praveen reddy..same functionality has implemented in entitlement and systems trigger
/*public static void contractExpireDate(Set<Id> SC)
{
    //Get list of systems attached to updated service contracts
    List<Entitlements_and_Systems__c> referencedSystems=[SELECT ID, system__r.Id 
                                            FROM Entitlements_and_Systems__c 
                                            WHERE Service_Contract__r.id in :SC 
                                            and Service_Contract__r.Type__c <> 'Surgical Support Pack'];
    
    Set<id> referencedSystemIDs=new set<id>{};
    
    for (Entitlements_and_Systems__c ES : referencedSystems)
    {
        //Collect system__r.id from referencedSystems
        referencedSystemIDs.add(ES.system__r.id);
    }
    
    List<Entitlements_and_Systems__c> referencedEntitlements=[SELECT Id, EndDate__c, service_contract__r.id, service_contract__r.enddate, system__r.id, system__r.contract_expire_date__c 
                                                            FROM Entitlements_and_Systems__c
                                                            WHERE system__r.id in :referencedSystemIDs and service_contract__r.Type__c not in ('SSP', 'Surgical Support Pack')];
    
    Map<id,System__c> systemMap=new map<id,System__c>{};
    
    for (Entitlements_and_Systems__c ES : referencedEntitlements)
    {
        //Define map to collect system records
        if (systemMap.get(ES.system__r.id) != null)
        {
            System__c tempSystem=systemMap.get(ES.system__r.id);
            if (tempSystem.contract_expire_date__c == null || tempSystem.contract_expire_date__c < ES.EndDate__c)
            {
                tempSystem.contract_expire_date__c=ES.EndDate__c;
                systemMap.put(tempSystem.id,tempSystem);
            }
        }
        else 
        {
            system__c tempSystem=new system__c(id=ES.system__r.id,contract_expire_date__c=ES.EndDate__c);
            systemMap.put(tempSystem.id,tempSystem);
        }
    }

    database.update(systemMap.values());
}*/



//Written by Archana as part of SFDC Changes - Re-engineering Interim Project Phase 1
public static void LoadContractData(List<Contract> ContractList)
{
	List<Contract> ContractListUpdate = new List<Contract>();
	System.debug('LoadContractData ContractList --->'+ContractList);
    for(Contract s: ContractList)
    {
    	System.debug('LoadContractData Contract --->'+s);
            Date TempAnniversary;
            /*Hotel nights is out of scope
            if(s.Total_of_Hotel_Nights_Purchased__c != null)
            {
                if(s.of_Hotel_Nights_Used__c == null)
                    s.of_Hotel_Nights_Used__c = 0;
                 s.Hotel_Nights_Expired__c = s.Total_of_Hotel_Nights_Purchased__c - s.of_Hotel_Nights_Used__c;
                 
            }*/
            if(s.Total_of_FSE_Visits_Purchased__c != null)  
            { 
                if(s.of_FSE_Visits_Used__c == null)
                    s.of_FSE_Visits_Used__c = 0;
                s.FSE_Visits_Expired__c=s.Total_of_FSE_Visits_Purchased__c- s.of_FSE_Visits_Used__c;
                
            }
            if(s.Total_of_Surgery_Coverages_Purchased__c != null)   
            {
                if(s.of_Surgery_Coverages_Used__c == null)
                    s.of_Surgery_Coverages_Used__c = 0;
                s.Surgery_Coverages_Expired__c = s.Total_of_Surgery_Coverages_Purchased__c - s.of_Surgery_Coverages_Used__c;
                
            }
            System.debug('TempAnniversary 1 s.EndDate --->'+s.EndDate);
            System.debug('TempAnniversary 1 s.Anniversary__c --->'+s.Anniversary__c);
            System.debug('TempAnniversary 1 s.EndDat123e --->'+s.Anniversary__c.addmonths(12));
            If(s.EndDate!= null && s.Anniversary__c != null && s.EndDate < s.Anniversary__c.addmonths(12))
            {
                TempAnniversary = s.EndDate;
                System.debug('TempAnniversary 1 ');
            }
            else if(s.Anniversary__c != null)
            {
                TempAnniversary = s.Anniversary__c.addmonths(12);
                System.debug('TempAnniversary 2 ');
            }
            
                 
            if(tempAnniversary!=s.Anniversary__c)
              
               {
               
               System.debug('********************* ' +s.id+'***' +TempAnniversary);
               
               
               if(s.Anniversary__c != null && TempAnniversary != null)
               System.debug('*****'+s.Anniversary__c.daysBetween(TempAnniversary));
               
                  Decimal RefillEntitlements_SurgeryCoverage = 0;
                  Decimal RefillEntitlements_FSEvisits = 0; 
                  //Decimal RefillEntitlements_hotelnights = 0; Hotel nights is out of scope
                
               /* Hotel nights is out of scope
               If(s.Anniversary__c != null && TempAnniversary != null){
               	decimal d1=0;
               	decimal InitialHotelNights=0;
               	if(s.Initial_Hotel_Nights__c!=0)
               	{
               
                d1= (s.Anniversary__c.daysBetween(TempAnniversary))/365* (s.Initial_Hotel_Nights__c);
               	}
               	else
               	{
               		List<Entitlements_and_Systems__c> entitlements=[SELECT Id, of_Hotel_Nights__c FROM Entitlements_and_Systems__c WHERE service_contract__c =:s.Id];
               		System.debug('entitlements 123 ----->'+entitlements);
               		for(Entitlements_and_Systems__c ent : entitlements)
               		{
               			InitialHotelNights = InitialHotelNights+ent.of_Hotel_Nights__c;
               		}
               		System.debug('entitlements InitialHotelNights 123 ----->'+InitialHotelNights);
               		 d1= (s.Anniversary__c.daysBetween(TempAnniversary))/365* (InitialHotelNights);
               		
               	}
                RefillEntitlements_hotelnights = d1.round();  
                }
                */
                
                IF(s.Initial_FSE_Visits__c!=0 && s.Anniversary__c != null && TempAnniversary != null){
                 
                decimal d2= (s.Anniversary__c.daysBetween(TempAnniversary))/365* (s.Initial_FSE_Visits__c);
               
                RefillEntitlements_FSEvisits = d2.round(); 
                }
                
                IF(s.Initial_Surgery_Coverages__c!=0 && s.Anniversary__c != null && TempAnniversary != null){
                         
                decimal d3= (s.Anniversary__c.daysBetween(TempAnniversary))/365* (s.Initial_Surgery_Coverages__c);
               
                RefillEntitlements_SurgeryCoverage = d3.round(); 
                }
                //System.debug('s.Additional_Hotel_Nights_Purchased__c : '+s.Additional_Hotel_Nights_Purchased__c);
                 System.debug('s.Additional_FSE_Visits_Purchased__c: '+s.Additional_FSE_Visits_Purchased__c);
                  System.debug('s.Additional_Surgery_Coverages_Purchased__c : '+s.Additional_Surgery_Coverages_Purchased__c);
                
                /*if(s.Additional_Hotel_Nights_Purchased__c == null)
                    s.Additional_Hotel_Nights_Purchased__c = 0;
                s.Additional_Hotel_Nights_Purchased__c = s.Additional_Hotel_Nights_Purchased__c + RefillEntitlements_hotelnights ;
                */
                
                if(s.Additional_FSE_Visits_Purchased__c == null)
                    s.Additional_FSE_Visits_Purchased__c = 0;
                s.Additional_FSE_Visits_Purchased__c = s.Additional_FSE_Visits_Purchased__c+ RefillEntitlements_FSEvisits ;
                
                if(s.Additional_Surgery_Coverages_Purchased__c == null)
                    s.Additional_Surgery_Coverages_Purchased__c = 0;
                s.Additional_Surgery_Coverages_Purchased__c = s.Additional_Surgery_Coverages_Purchased__c+ RefillEntitlements_SurgeryCoverage ;

                  //System.debug('s.Additional_Hotel_Nights_Purchased__c : '+s.Additional_Hotel_Nights_Purchased__c);
                  System.debug('s.Additional_FSE_Visits_Purchased__c: '+s.Additional_FSE_Visits_Purchased__c);
                  System.debug('s.Additional_Surgery_Coverages_Purchased__c : '+s.Additional_Surgery_Coverages_Purchased__c);
                  
                  //System.debug('RefillEntitlements_hotelnights : '+RefillEntitlements_hotelnights);
                  System.debug(' RefillEntitlements_FSEvisits : '+ RefillEntitlements_FSEvisits);
                  System.debug('RefillEntitlements_SurgeryCoverage : '+RefillEntitlements_SurgeryCoverage);
            }
         //    ContractListUpdate.add(s);
            // s.put(Field,Value); 
      }  
      //  update ContractListUpdate;
}


//This class is to keep contract name consistent since it is just a text field and people are used to an autonumber
//It will write over any text in the Name field with the Contract Number and keep them equal
//Deployed as part of SFDC "re-engineering" interim project, Phase 1
public static void setContractName (Map<id, Contract> svcContracts)
{
    List<Contract> scRecords = [select Id, name, ContractNumber, Contract_Type__c, EndDate
                           FROM Contract where Id in :svcContracts.keySet()];
    
    //List of service contracts that need to be updated
    List<Contract> scToUpdate = new List<Contract>{};
    
    for (Contract sc : scRecords)
    {
        //Check if the contract name does not match the contract number
        if (sc.name != sc.ContractNumber)
        {
            String num = sc.ContractNumber;
            //Set contract name = contract number
            sc.Name = sc.ContractNumber;
            
            //Add changed record to the list to be updated
            scToUpdate.add(sc);
        }
        if(sc.Contract_Type__c == 'Surgical Support Pack')
        {
           //Date tempDate = sc.StartDate; //create a new variable tempDate ans assign it to the record's Start Date
          // tempDate = tempDate.addMonths(sc.Term); //add the months from contract term to the tempDate
          // sc.EndDate = tempDate.addDays(-1);//decrease the day for the End Date by 1 and store the new value
           sc.Anniversary__c = sc.EndDate;
        }
    }
    //Update records
    update scToUpdate;
}

	private static Id serviceContractRT{
		get{
			
			if(serviceContractRT == null){
				
				serviceContractRT = [Select Id from RecordType where SObjectType = 'Contract' AND DeveloperName = 'RTG_Service_Contract'].Id;
			}
			
			return serviceContractRT;
		} 
		
		set;
	}
	
	
	public static void cloneContractConnections(List<Contract> newContracts){
		
		Set<Id> cloneFromIds = new Set<Id>();
		Set<Id> cloneRTGServiceContractIds = new Set<Id>();
		
		for(Contract newContract : newContracts){
			
			if(newContract.Clone_From__c != null){
				
				cloneFromIds.add(newContract.Clone_From__c);
				if(newContract.RecordTypeId == serviceContractRT) cloneRTGServiceContractIds.add(newContract.Clone_From__c);
			}			
		}
		
		if(cloneFromIds.size() > 0){
		
			Map<Id, Contract> cloneFromContracts = new Map<Id, Contract>([Select Id, 
																		(Select Asset__c, Contract__c, Entitlements__c, Service_Level__c, CurrencyIsoCode, of_Surgery_Coverages__c, of_FSE_Visits__c from AssetsContracts__r),
																		(Select Id from Attachments where IsPrivate = false) 
																		from Contract where Id IN :cloneFromIds]);
																		
			Map<Id, Attachment> cloneFromAttachmentsMap = new Map<Id, Attachment>();			
			if(cloneRTGServiceContractIds.size() > 0) cloneFromAttachmentsMap = new Map<Id, Attachment>([Select Body, ContentType, Description, IsPrivate, Name from Attachment where ParentId IN :cloneRTGServiceContractIds AND IsPrivate = false]);
				
			List<AssetContract__c> clonedConnections = new List<AssetContract__c>();
			List<Attachment> clonedAttachments = new List<Attachment>();
			
			for(Contract newContract : newContracts){
				
				if(newContract.Clone_From__c != null){
					
					Contract cloneFrom = cloneFromContracts.get(newContract.Clone_From__c);
					
					if(cloneFrom.AssetsContracts__r != null && cloneFrom.AssetsContracts__r.size() > 0){
						
						for(AssetContract__c connection : cloneFrom.AssetsContracts__r){
							
							AssetContract__c clonedConnection = connection.clone(false);
							clonedConnection.Contract__c = newContract.Id;
							
							clonedConnections.add(clonedConnection);
						}
					}
					
					if(newContract.RecordTypeId == serviceContractRT){
						
						for(Attachment contractAttachment : cloneFrom.Attachments){
    					
	    					Attachment fullAttachment = cloneFromAttachmentsMap.get(contractAttachment.Id);
	    					Attachment clonedAttachment = fullAttachment.clone();
	    					clonedAttachment.ParentId = newContract.Id;
	    					
	    					clonedAttachments.add(clonedAttachment);
	    				}
					}
				}			
			}
					
			if(clonedConnections.size() > 0) insert clonedConnections;
			if(clonedAttachments.size() > 0) insert clonedAttachments;
		}
	}
}