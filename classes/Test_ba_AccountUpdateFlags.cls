@isTest
private class Test_ba_AccountUpdateFlags {
		
	private static testmethod void test_updateAccountFlagsBatch(){
		
		// Master Data
		Company__c europe = new Company__c();		
	        europe.name='Europe';
	        europe.CurrencyIsoCode = 'EUR';
	        europe.Current_day_in_Q1__c=56;
	        europe.Current_day_in_Q2__c=34; 
	        europe.Current_day_in_Q3__c=5; 
	        europe.Current_day_in_Q4__c= 0;   
	        europe.Current_day_in_year__c =200;
	        europe.Days_in_Q1__c=56;  
	        europe.Days_in_Q2__c=34;
	        europe.Days_in_Q3__c=13;
	        europe.Days_in_Q4__c=22;
	        europe.Days_in_year__c =250;
	        europe.Company_Code_Text__c = 'T35';
			insert europe;
		
		// Business Unit Group
		Business_Unit_Group__c rtg =  new Business_Unit_Group__c();
	        rtg.Master_Data__c = europe.id;
	        rtg.name = 'RTG';      
        insert rtg;
		
		// Business Units
		Business_Unit__c neuro =  new Business_Unit__c();
	        neuro.Company__c = europe.id;
	        neuro.Business_Unit_Group__c = rtg.Id;
	        neuro.name = 'Neuro';
	        neuro.Account_Flag__c = 'Vascular__c';

        Business_Unit__c st =  new Business_Unit__c();
	        st.Company__c = europe.id;
	        st.Business_Unit_Group__c = rtg.Id;
	        st.name = 'Cranial Spinal';
	        st.Account_Flag__c = 'Cranial_and_spinal__c';

        insert new List<Business_Unit__c>{neuro, st};
        
        // Sub-Business Units
        Sub_Business_Units__c dbs = new Sub_Business_Units__c();
	        dbs.name = 'DBS';
	        dbs.Business_Unit__c = neuro.id;
	        dbs.Account_Flag__c = 'Brain_Modulation__c';

        Sub_Business_Units__c pumps = new Sub_Business_Units__c();
	        pumps.name = 'Pumps';
	        pumps.Business_Unit__c = neuro.id;
	        pumps.Account_Flag__c = 'EUR_Neuro_Pumps__c';

        Sub_Business_Units__c ent = new Sub_Business_Units__c();
	        ent.name = 'ENT';
	        ent.Business_Unit__c = st.id;
	        ent.Account_Flag__c = 'ENT_NT__c';
		insert new List<Sub_Business_Units__c>{dbs, pumps, ent};
		
		// Therapy Groups
		Therapy_Group__c dbsTG = new Therapy_Group__c();
			dbsTG.Name = 'CardioInsight';
			dbsTG.Sub_Business_Unit__c = dbs.Id;
			dbsTG.Company__c = europe.Id;

		Therapy_Group__c pumpsTG = new Therapy_Group__c();
			pumpsTG.Name = 'Conventional EP';
			pumpsTG.Sub_Business_Unit__c = pumps.Id;
			pumpsTG.Company__c = europe.Id;

		Therapy_Group__c entDrills = new Therapy_Group__c();
			entDrills.Name = 'Cryo';
			entDrills.Sub_Business_Unit__c = ent.Id;
			entDrills.Company__c = europe.Id;
		
		insert new List<Therapy_Group__c>{dbsTG, pumpsTG, entDrills};
		
		User currentUser = new User(Id = UserInfo.getUserId());
		
		Id MDT_TerritoryModelId = [Select Id from Territory2Model where DeveloperName = 'Medtronic'].Id;
		
		// Territory
		Territory2 neuroTer = new Territory2();
			neuroTer.Name = 'Neuro Territory';
			neuroTer.DeveloperName = 'Unit_Test_Neuro_Territory';
			neuroTer.Therapy_Groups_Text__c = 'CardioInsight; Conventional EP';
			neuroTer.Business_Unit__c = 'Neuromodulation';
			neuroTer.Company__c = String.valueOf(europe.Id).substring(0, 15);
			neuroTer.Territory2TypeId = bl_Territory.territoryTypeMap.get('Territory');
			neuroTer.Short_Description__c = 'Unit Test Neuromodulation Territory';
			neuroTer.Territory2ModelId = MDT_TerritoryModelId;
		
		Territory2 stTer = new Territory2();
			stTer.Name = 'CS Territory';
			stTer.DeveloperName = 'Unit_Test_CS_Territory';
			stTer.Therapy_Groups_Text__c = 'Cryo';
			stTer.Business_Unit__c = 'Cranial Spinal';
			stTer.Company__c = String.valueOf(europe.Id).substring(0, 15);
			stTer.Territory2TypeId = bl_Territory.territoryTypeMap.get('Territory');
			stTer.Short_Description__c = 'Unit Test Cranial Spinal Territory';
			stTer.Territory2ModelId = MDT_TerritoryModelId;
		
		System.runAs(currentUser){
			
			 insert new List<Territory2>{neuroTer, stTer};

		}
						
		// Account
		Account acc1 = new Account();
			acc1.Name = 'Unit Test Account 1';
			acc1.SAP_Id__c = '123456789';
		
		Account acc2 = new Account();
			acc2.Name = 'Unit Test Account 2';
			acc2.SAP_Id__c = '987654321';
			acc2.EUR_Neuro_Pumps__c = true;
			acc2.ENT_NT__c = true;
			acc2.Vascular__c = true;
			acc2.Cranial_and_Spinal__c = true;
		
		insert new List<Account>{acc1, acc2};
		

		// Account Share
		ObjectTerritory2Association acc1NeuroTer = new ObjectTerritory2Association();
			acc1NeuroTer.ObjectId = acc1.Id;
			acc1NeuroTer.Territory2Id = neuroTer.Id;
			acc1NeuroTer.AssociationCause ='Territory2Manual';
		
		ObjectTerritory2Association acc1STTer = new ObjectTerritory2Association();
			acc1STTer.ObjectId = acc1.Id;
			acc1STTer.Territory2Id = stTer.Id;
			acc1STTer.AssociationCause ='Territory2Manual';
		
		ObjectTerritory2Association acc2STTer = new ObjectTerritory2Association();
			acc2STTer.ObjectId = acc2.Id;
			acc2STTer.Territory2Id = stTer.Id;
			acc2STTer.AssociationCause ='Territory2Manual';
		
		insert new List<ObjectTerritory2Association>{acc1NeuroTer, acc1STTer, acc2STTer};
		

		// EXECUTE LOGIC
		Test.startTest();
			
			ba_AccountUpdateFlags batch = new ba_AccountUpdateFlags();
			Database.executeBatch(batch);
		
		Test.stopTest();
		
		
		// VALIDATE RESULT
		acc1 = [Select Id, Vascular__c, Cranial_and_Spinal__c, Brain_Modulation__c, EUR_Neuro_Pumps__c, ENT_NT__c from Account where Id = :acc1.Id];
		System.assert(acc1.Vascular__c == true); 
		System.assert(acc1.Cranial_and_Spinal__c == true);
		System.assert(acc1.Brain_Modulation__c == true);
		System.assert(acc1.EUR_Neuro_Pumps__c == true);
		System.assert(acc1.ENT_NT__c == true);
		
		acc2 = [Select Id, Vascular__c, Cranial_and_Spinal__c, Brain_Modulation__c, EUR_Neuro_Pumps__c, ENT_NT__c from Account where Id = :acc2.Id];
		System.assert(acc2.Vascular__c == false); 
		System.assert(acc2.Cranial_and_Spinal__c == true);
		System.assert(acc2.Brain_Modulation__c == false);
		System.assert(acc2.EUR_Neuro_Pumps__c == false);
		System.assert(acc2.ENT_NT__c == true);
		
	}    
}