//  Change Log       : PMT-22787: MITG EMEA User Setup - Changed profile EUR Field Force MITG to EMEA Field Force MITG
@isTest
private class Test_Cost_Analysis_Case {
    
    
    private static testmethod void testCreateSETCaseInSFDC(){
    	
        //PMT-22787: MITG EMEA User Setup - Changed profile EUR Field Force MITG to EMEA Field Force MITG
    	User mitgUser = [Select Id from User where Profile.Name in ('EUR Field Force MITG', 'EMEA Field Force MITG') AND IsActive = true AND Id NOT IN (Select AssigneeId from PermissionSetASsignment where PermissionSet.Name = 'SET_Admin') Limit 1];
        //User mitgUser = [Select Id from User where Profile.Name = 'EMEA Field Force MITG' AND IsActive = true AND Id NOT IN (Select AssigneeId from PermissionSetASsignment where PermissionSet.Name = 'SET_Admin') Limit 1];
    	
    	Cost_Analysis_Case__c setCase = new Cost_Analysis_Case__c();
		
		Boolean isError = false;
		    	
    	System.runAs(mitgUser){
	    	
	    	try{
	    		
	    		insert setCase;
	    		
	    	}catch(Exception e){
	    		
	    		isError = true;
	    		System.assert(e.getMessage().contains('This record can only be managed in Crossworld or by designated administrators'));			
	    	}
    	}
    	
    	System.assert(isError == true);
    	
    	insert setCase;
    	
    	isError = false;
    	
	    Cost_Analysis_Related_Product__c setCaseProduct = new Cost_Analysis_Related_Product__c();
	    setCaseProduct.Cost_Analysis_Case__c = setCase.Id;
	    
	    System.runAs(mitgUser){
	    	
	    	try{
	    			    	
	    		insert setCaseProduct;
	    		
	    	}catch(Exception e){
	    		
	    		isError = true;
	    		System.assert(e.getMessage().contains('This record can only be managed in Crossworld or by designated administrators'));	
	    	}
	    }
		
		System.assert(isError == true);
		
		Cost_Analysis_Question__c setCaseQuestion = new Cost_Analysis_Question__c();
		insert setCaseQuestion;
			    	
	    Cost_Analysis_Case_Answer__c setCaseAnswer = new Cost_Analysis_Case_Answer__c();
	    setCaseAnswer.Cost_Analysis_Case__c = setCase.Id;
	    setCaseAnswer.Cost_Analysis_Question__c = setCaseQuestion.Id;
	    
	    isError = false;
	    
	    System.runAs(mitgUser){
	    	
	    	try{
	    		
	    		insert setCaseAnswer;
	    		
	    	}catch(Exception e){
	    		
	    		isError = true;
	    		System.assert(e.getMessage().contains('This record can only be managed in Crossworld or by designated administrators'));	
	    	}
    	}
    	
    	System.assert(isError == true);
    }    
    
    private static testmethod void testCreateSETCaseInSFDC_SystemAdmin(){
    	   	
    	Test.startTest();
    	
    	Cost_Analysis_Case__c setCase = new Cost_Analysis_Case__c();
    	insert setCase;
    	
    	Cost_Analysis_Related_Product__c setCaseProduct = new Cost_Analysis_Related_Product__c();
    	setCaseProduct.Cost_Analysis_Case__c = setCase.Id;
    	insert setCaseProduct;

		Cost_Analysis_Question__c setCaseQuestion = new Cost_Analysis_Question__c();
		insert setCaseQuestion;
		    	
    	Cost_Analysis_Case_Answer__c setCaseAnswer = new Cost_Analysis_Case_Answer__c();
    	setCaseAnswer.Cost_Analysis_Case__c = setCase.Id;
    	setCaseAnswer.Cost_Analysis_Question__c = setCaseQuestion.Id;
    	insert setCaseAnswer;
    }
    
    private static testmethod void testCreateSETCaseInSFDC_SETAdmin(){
    	
    	User mitgAdmin;
    	
    	List<PermissionSetAssignment> boxBuilderAdmins = [Select AssigneeId from PermissionSetASsignment where PermissionSet.Name = 'SET_Admin' AND Assignee.IsActive = true];
    	
    	if(boxBuilderAdmins.size() > 0){
    		
    		mitgAdmin = new User(Id = boxBuilderAdmins[0].AssigneeId);
    		
    	}else{
    		//PMT-22787: MITG EMEA User Setup - Changed profile EUR Field Force MITG to EMEA Field Force MITG
    		//mitgAdmin = [Select Id from User where Profile.Name = 'EUR Field Force MITG' AND IsActive = true Limit 1];
            mitgAdmin = [Select Id from User where Profile.Name = 'EMEA Field Force MITG' AND IsActive = true Limit 1];
    		
    		PermissionSetASsignment assignment = new PermissionSetASsignment();
    		assignment.AssigneeId = mitgAdmin.Id;
    		assignment.PermissionSetId = [Select Id from PermissionSet where name = 'SET_Admin'].Id;
    		insert assignment;	
    	}
    	    	   	
    	Test.startTest();
    	
    	System.runAs(mitgAdmin){
    	
	    	Cost_Analysis_Case__c setCase = new Cost_Analysis_Case__c();
	    	insert setCase;
	    	
	    	Cost_Analysis_Related_Product__c setCaseProduct = new Cost_Analysis_Related_Product__c();
	    	setCaseProduct.Cost_Analysis_Case__c = setCase.Id;
	    	insert setCaseProduct;
	
			Cost_Analysis_Question__c setCaseQuestion = new Cost_Analysis_Question__c();
			insert setCaseQuestion;
			    	
	    	Cost_Analysis_Case_Answer__c setCaseAnswer = new Cost_Analysis_Case_Answer__c();
	    	setCaseAnswer.Cost_Analysis_Case__c = setCase.Id;
	    	setCaseAnswer.Cost_Analysis_Question__c = setCaseQuestion.Id;
	    	insert setCaseAnswer;
	    	
	    	update setCaseAnswer;
	    	update setCaseProduct;
	    	update setCase;
	    	
	    	delete setCaseAnswer;
	    	delete setCaseProduct;
	    	delete setCase;
    	}
    }
    
     private static testmethod void testCreateTask_SETCase(){
    	   	
    	Test.startTest();
    	
    	Account acc = new Account();
    	acc.Name = 'SET Test Account';
    	insert acc;
    	
    	Cost_Analysis_Case__c setCase = new Cost_Analysis_Case__c();
    	setCase.Account__c = acc.Id;
    	setCase.Start_Date__c = Date.today();
    	setCase.Status__c = 'Closed without HCP approval';
    	insert setCase;
    	
    	setCase = [Select Id, Task_Created__c from Cost_Analysis_Case__c where Id = :setCase.Id];
    	
    	System.assert(setCase.Task_Created__c == true);
    	
    	List<Task> tsk = [Select Id, Subject from Task where WhatId = :acc.Id];
    	System.assert(tsk.size() == 1);
    	System.assert(tsk[0].Subject.startsWith('SET Activity for SET Case '));
    }
    
    @TestSetup
    private static void setupSettings(){
    	
    	SystemAdministratorProfileId__c ids = SystemAdministratorProfileId__c.getInstance('Default');
    	
    	if(ids == null){
    		ids = new SystemAdministratorProfileId__c();
    		ids.Name = 'Default';
    	}
    	
    	if(ids.SystemAdministrator__c == null){
    		
    		ids.SystemAdministrator__c = String.valueOf([Select Id from Profile where Name = 'System Administrator'].Id).substring(0,15);
    		upsert ids;
    	}
    }
}