//------------------------------------------------------------------------------------------------------------------------------------------------------------
//  Author           :	Bart van Steenbergen
//  Created Date     :	10-12-2019
//  Description      :	APEX Batch for retrieving all contacts related to accounts which are in scope for deletion.
//  					Per Contact record all possible relations with the Contact Object will be queried
//  					The results will be stored in a csv file and send to the user who initiated the batch execution.
//  					The CSV files size should be below 6Mb.
//  					
//  					The batch can be started via the Anonymous window and ba_ContactsInScopeForDeletion.startBatch();
//  Change Log       : 
//------------------------------------------------------------------------------------------------------------------------------------------------------------
global class ba_ContactsInScopeForDeletion implements Database.Batchable<SObject>, Database.Stateful {
    
    global List<Id> listContactId;
    global String sCSV = '';
    global Integer iRecCounter = 0;
    
    global static void startBatch(){
        
        ba_ContactsInScopeForDeletion ba = new ba_ContactsInScopeForDeletion();
        
        if(Test.isRunningTest() == false){
            Id batchId = Database.executeBatch(ba, 1);
        }
        else{
            Id batchId = Database.executeBatch(ba);
        }
        
    }
    
    private void RetrieveContactRelationships(){
        
        Schema.DescribeSObjectResult R = Contact.SObjectType.getDescribe();
		List<Schema.ChildRelationship> C = R.getChildRelationships();
		List<String> listChildRelationship = new List<String>();
        
        sCSV = sCSV + '\'#\',';
        sCSV = sCSV + '\'ContactId\',';
		sCSV = sCSV + '\'ContactName\',';
		sCSV = sCSV + '\'AccountId\',';
        sCSV = sCSV + '\'AccountName\',';
        sCSV = sCSV + '\'AccountRecordType\',';
        sCSV = sCSV + '\'AccountCreated\',';
        sCSV = sCSV + '\'AccountCreatedBy\',';
        sCSV = sCSV + '\'Account_Country_vs__c \',';
        
        for (Schema.ChildRelationship rec : C){
    		if (rec.getRelationshipName() != null){
                if (rec.getRelationshipName() == 'OutgoingEmail') continue;
                if (rec.getRelationshipName() == 'OutgoingEmailRelations') continue;
                if (rec.getRelationshipName() == 'OpenActivities') continue;
				if (rec.getRelationshipName() == 'OpportunityContactRoles') continue;
        		listChildRelationship.add(rec.getRelationshipName());
                sCSV = sCSV + '\'' + rec.getRelationshipName() + '\',';
    		}
		}
        
        sCSV = sCSV.removeEnd(',');
        sCSV = sCSV + '\n';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        RetrieveContactRelationships();
        String query = 'select Id, Name, AccountId, Account.Name, Account.RecordType.Name, Account.CreatedDate, Account.CreatedBy.Name, Account.Account_Country_vs__c from Contact where Account.In_Scope_for_Deletion__c = \'Yes\' order by CreatedDate';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Contact> listContactBatch) {
        
        Schema.DescribeSObjectResult R = Contact.SObjectType.getDescribe();
		List<Schema.ChildRelationship> C = R.getChildRelationships();
		List<String> listChildRelationship = new List<String>();
        
        for (Schema.ChildRelationship rec : C){
    		if (rec.getRelationshipName() != null){
                if (rec.getRelationshipName() == 'OutgoingEmail') continue;
                if (rec.getRelationshipName() == 'OutgoingEmailRelations') continue;
                if (rec.getRelationshipName() == 'OpenActivities') continue;
				if (rec.getRelationshipName() == 'OpportunityContactRoles') continue;
        		listChildRelationship.add(rec.getRelationshipName());       
    		}
		}
        
        Integer iCounter = 0;
        
        for (contact recContactBatch : listContactBatch){
            
            iRecCounter++;
            
			String sQuery = 'select Id ';
            List<String> listRelationshipName = new List<String>();
            List<String> listRelationshipNameTotal = new List<String>();
            
            sCSV = sCSV + '\'' + iRecCounter + '\',';
            sCSV = sCSV + '\'' + recContactBatch.Id + '\',';
            sCSV = sCSV + '\'' + recContactBatch.Name + '\',';
            sCSV = sCSV + '\'' + recContactBatch.AccountId + '\',';
            sCSV = sCSV + '\'' + recContactBatch.Account.Name + '\',';
            sCSV = sCSV + '\'' + recContactBatch.Account.RecordType.Name + '\',';
            sCSV = sCSV + '\'' + recContactBatch.Account.CreatedDate  + '\',';
            sCSV = sCSV + '\'' + recContactBatch.Account.CreatedBy.Name  + '\',';
            sCSV = sCSV + '\'' + recContactBatch.Account.Account_Country_vs__c   + '\',';
            
            for (String sChildRelationship : listChildRelationship){
                
                listRelationshipNameTotal.add(sChildRelationship);
                
                if (sChildRelationship == 'OutgoingEmail') continue;
                if (sChildRelationship == 'OutgoingEmailRelations') continue;
                if (sChildRelationship == 'OpenActivities') continue;
                if (sChildRelationship == 'OpportunityContactRoles') continue;
                
                if ((iCounter < 19) && (listChildRelationship != listRelationshipNameTotal)){
                    listRelationshipName.add(sChildRelationship);
                    sQuery = sQuery + ', (select Id from ' + sChildRelationship + ') ';
                    iCounter++;
                }
                else{
                    listRelationshipName.add(sChildRelationship);
                    
                    sQuery = sQuery + ', (select Id from ' + sChildRelationship + ') ' + 'from Contact where Id = \'' + recContactBatch.Id + '\'';
                    iCounter = 0;
                    List<Contact> listContact = Database.query(sQuery);
                    
                    for (Contact recContact : listContact){

                        String sContactJSON = JSON.serialize(recContact);
                        
                        for (String sRelationshipName : listRelationshipName){
                            if (sContactJSON.contains(sRelationshipName)){
                                String sSubStringContactJSON = sContactJSON.substringAfter(sRelationshipName + '":{"totalSize":');
                                sSubStringContactJSON = sSubStringContactJSON.substringBefore(',');
                                sCSV = sCSV + '\'' +sSubStringContactJSON + '\',';
                            }
                            else{
                                sCSV = sCSV + '\'0\',';
                            }
                    	}
                    }
                    
                    sQuery = 'select Id ';
                    listRelationshipName = new List<String>();
                }
        	}
            
            sCSV = sCSV.removeEnd(',');
			sCSV = sCSV + '\n';
        }
    }
    
    global void finish(Database.BatchableContext BC) {
		Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
		csvAttc.setFileName('Contact.csv');
		csvAttc.setBody(Blob.valueOf(sCSV));
        
        User recUser = [select Id, email from user where Id = : UserInfo.getUserId()];
        
		Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
		email.setSubject('CSV Contacts and Relationships for deletion');
		email.setToAddresses( new list<string> {recUser.email} );
		email.setPlainTextBody('Contact CSV ');
		email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
		Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
    }
}