//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 10-04-2017
//  Description      : APEX Controller Extensions for the VisualForce page Opportunity_UploadProduct which allows 
//                      the user to create Opportunity Products (Opportunity Line Items) based on a text file that they upload.
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public with sharing class ctrlExt_Opportunity_UploadProduct {

    //------------------------------------------------------------------------
    // PRIVATE VARIABLES
    //------------------------------------------------------------------------
    private final Opportunity oOpportunity;

    private String tDelimiter = ';';
    @TestVisible private Map<String, FieldMapping> mapFieldMapping;
    private Set<String> setSkipFieldMapping;
    private String tProductIdentifier = '';
    private String tCurrencyField = '';
    //------------------------------------------------------------------------


    //------------------------------------------------------------------------
    // CONSTRUCTOR
    //------------------------------------------------------------------------
    public ctrlExt_Opportunity_UploadProduct(ApexPages.StandardController stdController) {

        try{
            bRenderPage = false;
            Id id_Opportunity = ApexPages.currentPage().getParameters().get('id');

            if (id_Opportunity != null){

                oOpportunity = [SELECT Id, Name, Pricebook2Id, CurrencyIsoCode, Type FROM Opportunity WHERE Id = :id_Opportunity];
                bRenderPage = true; 

            }

            if (oOpportunity == null){

                ApexPages.Message oErrorMsg = new ApexPages.Message(ApexPages.severity.ERROR, label.No_Opportunity_Provided);
                ApexPages.addMessage(oErrorMsg);

            }

            lstDataRecord = new List<DataRecord>();
            lstDataRecord_Error = new List<DataRecord>();
            lstDataRecord_Skipped = new List<DataRecord>();
            bFileProcessed = false;

            //--------------------------------------------------------------------
            // Field Mapping Settings
            //--------------------------------------------------------------------
            mapFieldMapping = new Map<String, FieldMapping>();
            FieldMapping oFieldMapping1 = new FieldMapping();
                oFieldMapping1.tFieldNameFile = 'Revenue'; // 'Sales Price'
                oFieldMapping1.tFieldNameSFDC = 'UnitPrice';
                oFieldMapping1.tFieldType = 'Decimal';
            mapFieldMapping.put('Revenue', oFieldMapping1);
            FieldMapping oFieldMapping2 = new FieldMapping();
                oFieldMapping2.tFieldNameFile = 'BB Code'; // 'Identifier'
                oFieldMapping2.tFieldNameSFDC = 'Identifier__c';
                oFieldMapping2.tFieldType = 'Text';
                oFieldMapping2.bProductIdentifier = true;
            mapFieldMapping.put('BB Code', oFieldMapping2);
            FieldMapping oFieldMapping3 = new FieldMapping();
                oFieldMapping3.tFieldNameFile = 'Currency';
                oFieldMapping3.tFieldNameSFDC = 'CurrencyIsoCode';
                oFieldMapping3.tFieldType = 'Text';
                oFieldMapping3.bCurrencyField = true;
            mapFieldMapping.put('Currency', oFieldMapping3);

            setSkipFieldMapping = new Set<String>();
                setSkipFieldMapping.add('UnitPrice');

            tProductIdentifier = 'Identifier__c';
            tCurrencyField = 'CurrencyIsoCode';

            tPageTitle = oOpportunity.Name;
            tPageSubTitle = 'Upload Opportunity Product (IHS)';
            //--------------------------------------------------------------------

        }catch (Exception oEX){

            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Error on line ' + oEX.getLineNumber() + ' : ' + oEX.getMessage()));

        }

    }
    //------------------------------------------------------------------------


    //------------------------------------------------------------------------
    // GETTER & SETTER
    //------------------------------------------------------------------------
    public String tPageTitle { get; private set; }
    public String tPageSubTitle { get; private set; }

    public Boolean bRenderPage { get; private set; }
    public String tFileName { get; set; }
    public Blob oFile { get; set; }

    public Boolean bFileProcessed { get; private set; }

    public List<DataRecord> lstDataRecord { get; private set; }
    public Boolean bHasValidRecords {

        get{
            return lstDataRecord.size() > 0;
        }

    }

    public List<DataRecord> lstDataRecord_Error { get; private set; }
    public Boolean bHasInvalidRecords {

        get{
            return lstDataRecord_Error.size() > 0;
        }

    }

    public List<DataRecord> lstDataRecord_Skipped { get; private set; }
    public Boolean bHasSkippedRecords {

        get{
            return lstDataRecord_Skipped.size() > 0;
        }

    }
    //------------------------------------------------------------------------


    //------------------------------------------------------------------------
    // ACTIONS    
    //------------------------------------------------------------------------
    public void processFile(){

        Map<Integer, DataRecord> mapRow_DataRecord = new Map<Integer, DataRecord>();
        Map<Integer, DataRecord> mapRow_DataRecord_Error = new Map<Integer, DataRecord>();
        Map<Integer, DataRecord> mapRow_DataRecord_Skipped = new Map<Integer, DataRecord>();

        bFileProcessed = false;

        //------------------------------------------------------
        // Validate that we have a file to process
        //------------------------------------------------------
        if (oFile == null){

            ApexPages.Message oErrorMsg = new ApexPages.Message(ApexPages.severity.ERROR, label.Please_choose_a_valid_file);
            ApexPages.addMessage(oErrorMsg);
            return;

        }
        //------------------------------------------------------


        //------------------------------------------------------
        // Validate the format and data in the file
        //------------------------------------------------------
        String tFileData;

        try{

            clsUtil.bubbleException();
            tFileData = oFile.toString();

        }catch (Exception oEX){

            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, label.An_error_has_occured_Please_check_the_file_template_or_try_again_later));
            return;

        }

        // Split the data by CRLF to get a list of Data Lines
        List<String> lstFileDataLine = tFileData.split('\n');
        for (String tFileDataLine : lstFileDataLine){

            tFileDataLine = tFileDataLine.trim();   // Removes also \n (CRLF) at the end of line

        }

        // Validate that there is a Header Line with the correct number of fields and that there is at least 1 data line
        if ( (lstFileDataLine.size() < 2) || (lstFileDataLine.size() == 0) || (lstFileDataLine[0].split(tDelimiter).size() != mapFieldMapping.size()) ){

            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, label.Please_check_data_in_file_for_correct_format));
            return;

        }
        //------------------------------------------------------


        //------------------------------------------------------
        // Process the Data File - Header Line
        //------------------------------------------------------
        List<DataField> lstDataField_Header = new List<DataField>();
        String tFileDataLine_Header = lstFileDataLine[0];
        List<String> lstDataField_HeaderValue = tFileDataLine_Header.split(tDelimiter);

        for (String tDataField_Header : lstDataField_HeaderValue){

            tDataField_Header = tDataField_Header.trim();   // Removes also \n (CRLF) at the end of line

            DataField oDataField_Header = new DataField();
            if (mapFieldMapping.containsKey(tDataField_Header)){
                oDataField_Header.tName = mapFieldMapping.get(tDataField_Header).tFieldNameSFDC;
                oDataField_Header.tLabel = tDataField_Header;
                lstDataField_Header.add(oDataField_Header);
            }

        }
        //------------------------------------------------------


        //------------------------------------------------------
        // Process the Data File - Validate Data Line(s)
        //------------------------------------------------------
        lstDataRecord = new List<DataRecord>();
        lstDataRecord_Error = new List<DataRecord>();

        Set<String> setProduct_Identifier = new Set<String>();

        Integer iCounter_Row = 0;
        for (String tFileDataLine : lstFileDataLine){

            if (iCounter_Row == 0){
                iCounter_Row++;
                continue; // Skip the header line
            } 

            DataRecord oDataRecord = new DataRecord();
            oDataRecord.iRow = iCounter_Row;

            List<DataField> lstDataField = new List<DataField>();
            Integer iCounter_Field = 0;
            List<String> lstDataField_Value = tFileDataLine.split(tDelimiter);

            String tError = '';
            if (lstDataField_Value.size() <> mapFieldMapping.size()){

                // Number of columns are not correct
                tError = 'Found ' + lstDataField_Value.size() + ' columns while we expect ' + mapFieldMapping.size() + ' columns';

            }
            
            DataField oDataField_Row = new DataField();
                oDataField_Row.tLabel = 'Row';
                oDataField_Row.tName = '';
                oDataField_Row.tValue = String.valueOf(iCounter_Row);
            lstDataField.add(oDataField_Row);

            oDataRecord.tError = tError;
            DataField oDataField_Error = new DataField();
                oDataField_Error.tLabel = 'Error';
                oDataField_Error.tValue = tError;
            lstDataField.add(oDataField_Error);


            Integer iFieldSkipRecord = 0;
            for (String tDataFieldValue : lstDataField_Value){

                tDataFieldValue = tDataFieldValue.trim();   // Removes also \n (CRLF) at the end of line

                if (!String.isBlank(tError)){
                    
                    // Add Fields for error record
                    DataField oDataField = new DataField();
                        oDataField.tLabel = 'Data';
                        oDataField.tName = 'Data';
                        oDataField.tValue = tDataFieldValue;
                    lstDataField.add(oDataField);
                
                }else{

                    DataField oDataField = new DataField();
                        oDataField.tLabel = lstDataField_Header[iCounter_Field].tLabel;
                        oDataField.tName = lstDataField_Header[iCounter_Field].tName;
                        oDataField.tValue = tDataFieldValue;
                    lstDataField.add(oDataField);

                    if (lstDataField_Header[iCounter_Field].tName == clsUtil.isNull(tProductIdentifier, '')){
                        if (!String.isBlank(tDataFieldValue)){
                            setProduct_Identifier.add(tDataFieldValue);
                        }
                        oDataRecord.tIdentifier = '' + tDataFieldValue;

                    }

                    if (lstDataField_Header[iCounter_Field].tName == clsUtil.isNull(tCurrencyField, '')){
                        if (tDataFieldValue != oOpportunity.CurrencyIsoCode){
                            tError = 'Product Currency ("' + tDataFieldValue + '") does not match the Opportunity Currency ("' + oOpportunity.CurrencyIsoCode + '")';
                            oDataField_Error.tValue = tError;
                            oDataRecord.tError = tError;
                        }
                    }

                    // Validate if fields defined as Field Skip Records are both 0 or empty
                    if (setSkipFieldMapping.contains(lstDataField_Header[iCounter_Field].tName)){
                        Decimal decValue = 0;
                        if (clsUtil.isBlank(tDataFieldValue)){
                            decValue = 0;
                        }else{
                            try{ decValue = Decimal.valueOf(tDataFieldValue); }catch(Exception oEX){ }
                        }

                        if (decValue == 0){
                            iFieldSkipRecord++;
                        }

                        if (iFieldSkipRecord == setSkipFieldMapping.size()){
                            tError = 'Skipped because of 0 / null / "" values';
                            oDataField_Error.tValue = tError;
                            oDataRecord.tError = tError;
                        }

                    }

                    iCounter_Field++;

                }

            }

            oDataRecord.lstDataField = lstDataField;
            if (iFieldSkipRecord == setSkipFieldMapping.size()){

                lstDataRecord_Skipped.add(oDataRecord);
                mapRow_DataRecord_Skipped.put(iCounter_Row, oDataRecord);

            }else if (!String.isBlank(tError)){

                lstDataRecord_Error.add(oDataRecord);
                mapRow_DataRecord_Error.put(iCounter_Row, oDataRecord);

            }else{

                lstDataRecord.add(oDataRecord);                        
                mapRow_DataRecord.put(iCounter_Row, oDataRecord);

            }

            iCounter_Row++;

        }
        //------------------------------------------------------



        //------------------------------------------------------
        // Load Product Data
        //------------------------------------------------------
        Map<String, PricebookEntry> mapProductIdentifier_PricebookEntry = new Map<String, PricebookEntry>(); 
        if (setProduct_Identifier.size() > 0){
            String tSOQL = 'SELECT Id, UnitPrice, Product2.' + tProductIdentifier;
            tSOQL += ' FROM PricebookEntry';
            tSOQL += ' WHERE IsActive = true';
                tSOQL += ' AND CurrencyIsoCode = \'' + oOpportunity.CurrencyIsoCode + '\'';
                tSOQL += ' AND Pricebook2Id = \'' + oOpportunity.Pricebook2Id + '\'';
                tSOQL += ' AND Product2.' + tProductIdentifier + ' = :setProduct_Identifier';

            List<PricebookEntry> lstPricebookEntry = Database.query(tSOQL);    
            for (PricebookEntry oPricebookEntry : lstPricebookEntry){

                String tProductIdentifier_Value = (String)oPricebookEntry.getSObject('Product2').get(tProductIdentifier);
                if (!mapProductIdentifier_PricebookEntry.containskey(tProductIdentifier_Value) ){
                    mapProductIdentifier_PricebookEntry.put(tProductIdentifier_Value, oPricebookEntry);
                }

            }
        }
        //------------------------------------------------------


        //------------------------------------------------------
        // Process the validated Data File - Create Opportunity Line Items
        //------------------------------------------------------
        List<OpportunityLineItem> lstOLI = new List<OpportunityLineItem>();
        for (DataRecord oDataRecord : lstDataRecord){

            OpportunityLineItem oOLI = new OpportunityLineItem();

            if (!mapProductIdentifier_PricebookEntry.containsKey(oDataRecord.tIdentifier)){

                // Product not found
                oDataRecord.tError = 'No product found with identifier "' + oDataRecord.tIdentifier + '"';
                oDataRecord.lstDataField[1].tValue = oDataRecord.tError;
                mapRow_DataRecord_Error.put(oDataRecord.iRow, oDataRecord);
                mapRow_DataRecord.remove(oDataRecord.iRow);

            }else{

                oOLI.OpportunityId = oOpportunity.Id;
                oOLI.Quantity = 1;

                oDataRecord.lstDataField.remove(1); // Remove the error field
                for (DataField oDataField : oDataRecord.lstDataField){

                    if (!String.isBlank(oDataField.tName)){

                        if (oDataField.tName == clsUtil.isNull(tCurrencyField, '')){

                            continue;   // Nothing todo - Currency is only used to validate the data quality and will not be used to update data

                        }else if (oDataField.tName == clsUtil.isNull(tProductIdentifier, '')){

                            oOLI.put('PricebookEntryId', mapProductIdentifier_PricebookEntry.get(oDataField.tValue).Id);

                        }else{

                            if (mapFieldMapping.get(oDataField.tLabel).tFieldType == 'Decimal'){
                                oOLI.put(oDataField.tName, Decimal.valueOf(oDataField.tValue));
                            }else if (mapFieldMapping.get(oDataField.tLabel).tFieldType == 'Date'){
                                oOLI.put(oDataField.tName, Date.valueOf(oDataField.tValue));
                            }else{
                                oOLI.put(oDataField.tName, oDataField.tValue);
                            }
                        }
                    }
                }

              lstOLI.add(oOLI);

            }

        }

        if (lstOLI.size() > 0){

            try{
                
                insert lstOLI;
            
            }catch(Exception oEX){

                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Error while creating the Opportunity Product records ' + oEX.getLineNumber() + ' : ' + oEX.getMessage()));

            }

        }
        //------------------------------------------------------



        //------------------------------------------------------
        // Sort Maps
        //------------------------------------------------------
        if (mapRow_DataRecord.size() > 0){
            Set<Integer> setRow = mapRow_DataRecord.keySet();
            List<Integer> lstRow = new List<Integer>();
            lstRow.addAll(setRow);
            lstRow.sort();
            Map<Integer, DataRecord> mapRow_DataRecord_Tmp = new Map<Integer, DataRecord>();
            for (Integer iRow : lstRow){
                mapRow_DataRecord_Tmp.put(iRow, mapRow_DataRecord.get(iRow));
            }
            mapRow_DataRecord = mapRow_DataRecord_Tmp;
        }

        if (mapRow_DataRecord_Error.size() > 0){
            Set<Integer> setRow = mapRow_DataRecord_Error.keySet();
            List<Integer> lstRow = new List<Integer>();
            lstRow.addAll(setRow);
            lstRow.sort();
            Map<Integer, DataRecord> mapRow_DataRecord_Tmp = new Map<Integer, DataRecord>();
            for (Integer iRow : lstRow){
                mapRow_DataRecord_Tmp.put(iRow, mapRow_DataRecord_Error.get(iRow));
            }
            mapRow_DataRecord_Error = mapRow_DataRecord_Tmp;
        }

        if (mapRow_DataRecord_Skipped.size() > 0){
            Set<Integer> setRow = mapRow_DataRecord_Skipped.keySet();
            List<Integer> lstRow = new List<Integer>();
            lstRow.addAll(setRow);
            lstRow.sort();
            Map<Integer, DataRecord> mapRow_DataRecord_Tmp = new Map<Integer, DataRecord>();
            for (Integer iRow : lstRow){
                mapRow_DataRecord_Tmp.put(iRow, mapRow_DataRecord_Skipped.get(iRow));
            }
            mapRow_DataRecord_Skipped = mapRow_DataRecord_Tmp;
        }
        //------------------------------------------------------


        //------------------------------------------------------
        // Prepare lists to display the results
        //------------------------------------------------------
        lstDataRecord = mapRow_DataRecord.values();
        lstDataRecord_Error = mapRow_DataRecord_Error.values();
        lstDataRecord_Skipped = mapRow_DataRecord_Skipped.values();
        //------------------------------------------------------

        bFileProcessed = true;

    }


    public PageReference backToOpportunity(){

        return new PageReference('/' + oOpportunity.Id);

    }    
    //------------------------------------------------------------------------



    //------------------------------------------------------------------------
    // HELPER CLASS
    //------------------------------------------------------------------------
    public class DataRecord {

        public Integer iRow { get; set; }
        public List<DataField> lstDataField { get; set; }
        public String tError { get; set; }
        public String tIdentifier { get; set; }

        public DataRecord(){

            iRow = 0;
            lstDataField = new List<DataField>();
            tError = '';
            tIdentifier = '';

        }

    }

    public class DataField {

        public String tName { get; set; }
        public String tLabel { get; set; }
        public String tValue { get; set; }

        public DataField(){

            tName = '';
            tLabel = '';
            tValue = '';

        }

    }
 
    public class FieldMapping{

        public String tFieldNameFile { get; set; }
        public String tFieldNameSFDC { get; set; }
        public String tFieldType { get; set; }
        public Boolean bProductIdentifier { get; set; }
        public Boolean bCurrencyField { get; set; }

        public FieldMapping(){

            tFieldNameFile = '';
            tFieldNameSFDC = '';
            tFieldType = '';
            bProductIdentifier = false;
            bCurrencyField = false;

        }

    }   
    //------------------------------------------------------------------------


}
//--------------------------------------------------------------------------------------------------------------------