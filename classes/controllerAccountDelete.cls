/**
 * Creation Date: 	20090223
 * Description: 	controllerAccountDelete class that overrides the standard delete functionality
 *				
 * Author: 	Medtronic - Jake
 *
 * Change log: Rudy De Coninck Refactored to use configuration definition of lookup
 */

public class controllerAccountDelete {
	
	private final Account delAcc;
	public static string warnText; 
	public ID returnAcc;
	
	public controllerAccountDelete(ApexPages.StandardController accController) {
		this.delAcc = (Account)accController.getRecord();
	}
	
	// Show the message on the VF page why the Account can't be deleted.
	public string getWarntext(){
		return warntext;
	} 
	
	// Back method will go back to the to deleted Account, because it is not possible to delete
	public PageReference Back(){
		system.debug('return id = ' + returnAcc);
		PageReference p = new PageReference('/'+ returnAcc);		
		p.setRedirect(true);
		return p;
	
	}
	
	private String delId ; 
	public String getdelId(){			return this.delId ;	}
	public void setdelId(String s){		this.delId = s ;	}
	
	public PageReference AccountDelete2(){
		if (ApexPages.currentPage().getParameters().get('id') != null){
			delId = ApexPages.currentPage().getParameters().get('id');
		}else if (ApexPages.currentPage().getParameters().get('delID') != null){
			delId = ApexPages.currentPage().getParameters().get('delID');
		}
       	Account delAccount=[select id, RecordTypeId, name ,SAP_ID__c from Account where id = :delId];
       	
       	// *** Update 24/11/2009 
       	if (delAccount.SAP_ID__c != null || delAccount.RecordTypeId == FinalConstants.sapAccountRecordTypeId){
        	ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'You can\'t delete SAP Accounts.');
        	returnAcc= delAccount.id;
        	ApexPages.addMessage(errorMsg);
        	return null ;  
        }  
       	
		//Only for non sap accounts
		if (delAccount.SAP_ID__c == null)
        {
			// if System Admin DO NOT TRIGGER !!		
			if (!ProfileMedtronic.isSystemAdministratorProfile(UserInfo.getProfileId())) { 
				
				Schema.DescribeSObjectResult desAcc = Account.SObjectType.getDescribe();		// Describe the Account object to go through the different child objects
				List<Schema.ChildRelationship> childRT = desAcc.getChildRelationships(); 		// childRT = All the childobjects of account
				
					Integer countChildren = 0  ;
					String message ='';
					
					for (Schema.ChildRelationship a : childRT){							// Loop into the different related items 
							SObjectField cFieldName 	= a.getField();					
							SObjectType	 cObjectType 	= a.getChildSObject();
							String 		 cRelationName	= a.getRelationshipName(); 
							String ChildObjectLabel = a.getChildSObject().getDescribe().getLabel();
							system.debug('cObjectType : ' + cObjectType);
							system.debug('cFieldName : ' + cFieldName);
							system.debug('cRelationName : ' + cRelationName);
							system.debug('a.isCascadeDelete : ' + a.isCascadeDelete());
							system.debug('a.isRestrictedDelete : ' + a.isRestrictedDelete());
						if (a.isCascadeDelete()  == false){ // isCascade = Returns true if the child object is deleted when the parent object is deleted, false otherwise.
							 
							// /!\ Some Objects doesn't allow query statement such as :
							//		- Object : 'ProcessInstanceHistory' || field : 'TargetObjectId' || Realtion Name : 'ProcessSteps' ) 					
							if (a.isRestrictedDelete() && cRelationName != null && cRelationName != 'ProcessSteps' ){
								String qry = 'Select Id, ' + cFieldName + ' from ' + cObjectType + ' where ' + cFieldName + ' = \'' + delAccount.Id + '\'' ;
								System.debug(qry);		
								SObject[] s = Database.query(qry);
								if (s.size() != 0){
									countChildren++ ;	
									message+=' '+ChildObjectLabel+' ('+s.size()+'),';	
								}
							}
						}
					}
					if (countChildren > 0){
						if (message.length()>1){
							message= message.substring(0,message.length()-1);
						}
				    	warnText='Account '+delAccount.name+' cannot be deleted as related records exist that reference this account: '+message;	
				    	returnAcc = delAccount.Id;	 
				    	 			        
					} else{     
						
						delete delAccount;
				    	warnText='DELETED Account';	
						PageReference p = new PageReference('/001/o');		
						p.setRedirect(true);
						return p;
					}		 		
					
			} else { 

				//warnText ='Only System Administrator can Mass Delete Accounts ! ';		 

				Schema.DescribeSObjectResult desAcc = Account.SObjectType.getDescribe();		// Describe the Account object to go through the different child objects
				List<Schema.ChildRelationship> childRT = desAcc.getChildRelationships(); 		// childRT = All the childobjects of account
				
					for (Schema.ChildRelationship a : childRT){
						System.debug(''+a.getChildSObject()+'.'+a.getField()+' ('+a.getRelationshipName()+') '+a.isCascadeDelete()+' '+a.isRestrictedDelete());
						
					}
				
					for (Schema.ChildRelationship a : childRT){						
						
						// Loop into the different related items 
						//Delete all items where the lookup to account is set as "What to do if the lookup record is deleted?" = "Don't allow deletion of the lookup record that's part of a lookup relationship."
						if (a.isCascadeDelete()  == false && a.isRestrictedDelete()){			// isCascade = Returns true if the child object is deleted when the parent object is deleted, false otherwise.
							SObjectField cFieldName 	= a.getField();					
							SObjectType	 cObjectType 	= a.getChildSObject();
							String 		 cRelationName	= a.getRelationshipName();  
							system.debug('cObjectType : ' + cObjectType);
							system.debug('cFieldName : ' + cFieldName);
							system.debug('cRelationName : ' + cRelationName);				
							// /!\ Some Objects doesn't allow query statement such as :
							//		- Object : 'ProcessInstanceHistory' || field : 'TargetObjectId' || Relation Name : 'ProcessSteps' ) 					
							if (cRelationName != null && cRelationName != 'ProcessSteps'){
								String qry = 'Select ' + cFieldName + ' from ' + cObjectType + ' where ' + cFieldName + ' = \'' + delAccount.Id + '\'' ;
								System.debug(qry);		
								SObject[] s = Database.query(qry);
								if (s.size() != 0){
									delete s;		
								}
							}
						}
					}
					try{
						delete delAccount;
					}catch(DMLException ex){
						ApexPages.addMessages(ex);
												
					}
					PageReference p = new PageReference('/001/o');		
					p.setRedirect(true);
					return p;

			}	
        }
        else
        {
        warnText ='It is not allowed to delete this SAP Account (SAP number : ' + delAccount.SAP_ID__c + ')';
        }

		returnAcc= delAccount.id;
		return null;
	
		
	}	
		
}