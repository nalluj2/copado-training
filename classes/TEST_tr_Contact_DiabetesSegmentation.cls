//------------------------------------------------------------------------------------------------------------------------------------------------------------
// Author       : Bart Caelen
// Date         : 20150616
// Description  : CR-6861
//                  Test Class for tr_Contact_DiabetesSegmentation
// Version      :  1.0
//------------------------------------------------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_tr_Contact_DiabetesSegmentation {
	
    @testSetup static void createTestData(){
        //--------------------------------------------------------
        // CREATE TEST DATA
        //--------------------------------------------------------
        // Create Master Data
        clsTestData.createBusinessUnitData();
        clsTestData.createSubBusinessUnitData();

        // Create Account / Contact Data - 1
        clsTestData.idRecordType_Account = RecordTypeMedtronic.Account('SAP_Account').Id;
        clsTestData.createAccountData();
        clsTestData.iRecord_Contact = 4;
        clsTestData.createContactData();

        // Create DIB_Department__c Data
        clsTestData.createDepartmentMasterData();
        clsTestData.iRecord_DIBDepartment = 2;
        clsTestData.createDIBDepartmentData();

        /* 	CR-15511
        List<Contact> lstContact = [SELECT Id FROM Contact limit 2];
        for (Contact oContact : lstContact){
            oContact.Diabetes_Segmentation__c = clsTestData.oMain_DIBDepartment.Id;
        }
        update lstContact;
        */

        // Create Account Data - 2
        clsTestData.oMain_Account = null;
        clsTestData.idRecordType_Account = RecordTypeMedtronic.Account('SAP_Account').Id;
        clsTestData.createAccountData(false);
        Account oAccount2 = clsTestData.oMain_Account;
            oAccount2.Name = 'TEST ACCOUNT NEW 1';
        insert oAccount2;
        clsTestData.iRecord_DIBDepartment = 2;
        clsTestData.oMain_DIBDepartment = null;
        clsTestData.createDIBDepartmentData();

        // Create Account Data - 3
        clsTestData.oMain_Account = null;
        clsTestData.idRecordType_Account = RecordTypeMedtronic.Account('SAP_Account').Id;
        clsTestData.createAccountData(false);
        Account oAccount3 = clsTestData.oMain_Account;
            oAccount3.Name = 'TEST ACCOUNT NEW 2';
        insert oAccount3;
        //--------------------------------------------------------
	}

    @isTest static void test_RelinkContactToAccountWithSameDepartment(){

        //--------------------------------------------------------
        // Load Test Data
        //--------------------------------------------------------
        List<Account> lstAccount = [SELECT Id, Name FROM Account];
        List<Contact> lstContact = [SELECT Id, Name FROM Contact];

        Account oAccount_New_1 = new Account();
        Account oAccount_New_2 = new Account();
        for (Account oAccount : lstAccount){
            if (oAccount.Name == 'TEST ACCOUNT NEW 1'){
                oAccount_New_1 = oAccount;
            }else if (oAccount.Name == 'TEST ACCOUNT NEW 2'){
                oAccount_New_2 = oAccount;
            }
        }

        List<DIB_Department__c> lstDepartment = [SELECT Id, Name, Account__c FROM DIB_Department__c];
        Set<Id> setID_Department_1 = new Set<Id>();
        Set<Id> setID_Department_2 = new Set<Id>();
        for (DIB_Department__c oDepartment : lstDepartment){
            if (oDepartment.Account__c == oAccount_New_1.Id){
                setID_Department_1.add(oDepartment.Id);
            }else if (oDepartment.Account__c == oAccount_New_2.Id){
                setID_Department_2.add(oDepartment.Id);
            }
        }
        //--------------------------------------------------------


        //--------------------------------------------------------
        // Perform Testing
        //--------------------------------------------------------
        Test.startTest();

        for (Contact oContact : lstContact){
            oContact.AccountId = oAccount_New_1.Id;
        }

        update lstContact;

        Test.stopTest();
		
		/* CR-15511
        lstContact = [SELECT Id, Name, Diabetes_Segmentation__c FROM Contact where Diabetes_Segmentation__c != null];
        for (Contact oContact : lstContact){
            System.assert(setID_Department_1.contains(oContact.Diabetes_Segmentation__c));
        }
        */
        //--------------------------------------------------------

    }

    @isTest static void test_RelinkContactToAccountWithoutSameDepartment(){

        //--------------------------------------------------------
        // Load Test Data
        //--------------------------------------------------------
        List<Account> lstAccount = [SELECT Id, Name FROM Account];
        List<Contact> lstContact = [SELECT Id, Name FROM Contact];
        Account oAccount_New_1 = new Account();
        Account oAccount_New_2 = new Account();
        for (Account oAccount : lstAccount){
            if (oAccount.Name == 'TEST ACCOUNT NEW 1'){
                oAccount_New_1 = oAccount;
            }else if (oAccount.Name == 'TEST ACCOUNT NEW 2'){
                oAccount_New_2 = oAccount;
            }
        }
        //--------------------------------------------------------


        //--------------------------------------------------------
        // Perform Testing
        //--------------------------------------------------------
        Test.startTest();

        for (Contact oContact : lstContact){
            oContact.AccountId = oAccount_New_2.Id;
        }

        update lstContact;
		
		Test.stopTest();
		
		/* CR-15511
        for (Contact oContact : lstContact){
            oContact.Diabetes_Segmentation__c = null;
        }

        lstContact = [SELECT Id, Name, Diabetes_Segmentation__c FROM Contact];
        for (Contact oContact : lstContact){
            System.assertEquals(oContact.Diabetes_Segmentation__c, null);
        }
        */
        //--------------------------------------------------------

    }
	
}
//------------------------------------------------------------------------------------------------------------------------------------------------------------