//--------------------------------------------------------------------------------------------------------------------------------
//  Author      :   Bart Caelen
//  Date        :   14/06/2017
//  Description :   Controller Extension for the VF Page "SendEmailToCaseContacts".  This page will redirect the user to Standard
//                      SFDC EmailAuthor page to send emails to all Contacts that are defined in the Case Contact Roles.
//  Change Log  :    
//--------------------------------------------------------------------------------------------------------------------------------
public with sharing class ctrlExt_SendEmailToCaseContacts {

    private String tRType = '003';
    private String tURLParameter = '';

    //----------------------------------------------------------------
    // CONSTRUCTOR
    //----------------------------------------------------------------
    public ctrlExt_SendEmailToCaseContacts(ApexPages.StandardController stdController) {

        bError = false;

        try{

            clsUtil.bubbleException1();

            if (!Test.isRunningTest()){
                stdController.addFields(new List<String>{'CaseNumber'});
            }
            oCase = (Case)stdController.getRecord();

            // This contains all provided parameters that we also need to pass to the EmailAuthor page
            tURLParameter = ApexPages.currentPage().getUrl().substringAfter('&');

        }catch(Exception oEX){
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, oEX.getMessage()));
            bError = true;
        }
    }
    //----------------------------------------------------------------


    //----------------------------------------------------------------
    // GETTER & SETTERS
    //----------------------------------------------------------------
    public Case oCase { get; private set; }
    public Boolean bError { get; private set; }
    //----------------------------------------------------------------


    //----------------------------------------------------------------
    // PRIVATE METHODS
    //----------------------------------------------------------------
    @TestVisible private String tBuildURL(){

        String tURL = '';
        try{

            clsUtil.bubbleException2();

            String tEmailAddress = '';

            List<Case_Contact_Roles__c> lstCaseContactRoles = [SELECT Id, Contact__r.Email FROM Case_Contact_Roles__c WHERE Case__c = :oCase.Id ORDER BY Contact__r.Email];

            Set<String> setEmail = new Set<String>();
            for (Case_Contact_Roles__c oCaseContactRoles : lstCaseContactRoles){
                setEmail.add(oCaseContactRoles.Contact__r.Email);
            }
            for (String tEmail : setEmail) tEmailAddress += (tEmailAddress==''?'':'; ') + tEmail;

            tURL = '/_ui/core/email/author/EmailAuthor?p3_lkid=' + oCase.Id;
            if (!String.isBlank(tEmailAddress)) tURL += '&p24=' + tEmailAddress;
            if (!String.isBlank(tURLParameter)) tURL += '&' + tURLParameter;
            if (!tURLParameter.containsIgnoreCase('retURL')) tURL += '&retURL=%2F' + oCase.Id;


        }catch(Exception oEX){
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, oEX.getMessage()));
            bError = true;
            tURL = '';
        }

        return tURL;

    }
    //----------------------------------------------------------------


    //----------------------------------------------------------------
    // ACTION METHODS
    //----------------------------------------------------------------
    public PageReference backToCase(){

        PageReference oPageRef = new PageReference('/' + oCase.Id);
            oPageRef.setRedirect(true);
        return oPageRef;

    }

    public PageReference redirectPage(){

        if (bError) return null;

        String tURL = tBuildURL();
        if (bError) return null;

        PageReference oPageRef = new PageReference(tBuildURL());
            oPageRef.setRedirect(true);
        return oPageRef;

    }    
    //----------------------------------------------------------------

}
//--------------------------------------------------------------------------------------------------------------------------------