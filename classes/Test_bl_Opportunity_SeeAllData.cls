/*
 * Author           :       Bart Caelen
 * Date             :       20141218
 * Description      :       CR-5245 - Test Class for bl_Opportunity with SeeAllData=true to test the logic followOpportunityBusinessLeader and setBusinessUnit
 *                          When the test class fails, check the picklist values that are defined in the field "Company__c" of the Territory Object.
 */
@isTest(SeeAllData=true) 
private class Test_bl_Opportunity_SeeAllData {

        
    @isTest static void test_followOpportunityBusinessLeader() {
        //------------------------------------------------
        // Load User Data
        //------------------------------------------------
        List<User> lstUser_Admin = [SELECT Id, Name FROM User WHERE (Profile.Name = 'System Administrator MDT' OR Profile.Name = 'System Administrator') AND IsActive = true AND License_Name__c = 'Salesforce'];

        Map<Id, User> mapUser = new Map<Id, User>([SELECT Id, Job_Title_vs__c FROM User WHERE Job_Title_vs__c in ('Country BU Manager', 'District Manager') AND IsActive = true AND Id != :lstUser_Admin AND License_Name__c = 'Salesforce' ORDER BY Job_Title_vs__c]);
        System.assertNotEquals(mapUser.size(), 0);

        Map<Id, User> mapUser_ALL = new Map<Id, User>([SELECT Id, Job_Title_vs__c FROM User WHERE IsActive = true and UserType = 'Standard' and Id != :lstUser_Admin AND License_Name__c = 'Salesforce']);
        System.assertNotEquals(mapUser_ALL.size(), 0);
        //------------------------------------------------

        
        //------------------------------------------------
        // Load Opportunity Recordtype Data
        //------------------------------------------------
        clsUtil.loadRecordTypeMap();
        Map<String, RecordType> mapRecordType = clsUtil.mapObjectName_RecordTypeName_RecordType.get('Opportunity');
        Id idRecordType_Opportunity_1 = mapRecordType.values()[0].Id;
        Id idRecordType_Opportunity_2 = mapRecordType.values()[1].Id;

        Set<Id> setID_RecordType = new Set<Id>();
        setID_RecordType.add(idRecordType_Opportunity_1);
        setID_RecordType.add(idRecordType_Opportunity_2);
        String tOpportunity_RecordTypeIds = idRecordType_Opportunity_1 + ',' + idRecordType_Opportunity_2;

        List<Company__c> lstCompany = [SELECT Id, Name FROM Company__c WHERE Name = 'Europe'];
        String tCompanyID_18 = lstCompany[0].Id;
        String tCompanyID_15 = tCompanyID_18.left(15);
        Set<String> setCompanyId = new Set<String>{tCompanyID_18, tCompanyID_15};
        //------------------------------------------------


        //------------------------------------------------
        // Load Territory and UserTerritory Data
        //------------------------------------------------
        Map<Id, Territory2> mapTerritory = new Map<Id, Territory2>([SELECT Id, Name, DeveloperName, Company__c, Business_Unit__c, Country_UID__c, ParentTerritory2Id, Territory2Type.DeveloperName FROM Territory2 WHERE Company__c = :setCompanyId AND Business_Unit__c = 'Neurovascular']);
        Set<Id> setID_Territory = new Set<Id>();
        for (Territory2 oTerritory : mapTerritory.values()){
            setID_Territory.add(oTerritory.Id);
        }
        Map<Id, List<User>> mapTerritoryID_Users = new Map<Id, List<User>>();
        List<UserTerritory2Association> lstUserTerritory = [SELECT Id, IsActive, Territory2Id, UserId FROM UserTerritory2Association WHERE Territory2Id = :setID_Territory ORDER BY Territory2Id];
        for (UserTerritory2Association oUserTerritroy : lstUserTerritory){
            List<User> lstUser = new List<User>();
            if (mapTerritoryID_Users.containsKey(oUserTerritroy.Territory2Id)){
                lstUser = mapTerritoryID_Users.get(oUserTerritroy.Territory2Id);
            }
            Id idUser = (Id)oUserTerritroy.UserId;
            lstUser.add(mapUser.get(idUser));
            mapTerritoryID_Users.put(oUserTerritroy.Territory2Id, lstUser);
        }

        Territory2 oTerritory_Territory;
        Territory2 oTerritory_District;
        Territory2 oTerritory_Salesforce;
        Territory2 oTerritory_BusinessUnit;
        Integer iCounterUser_DistrictManager = 0;
        Integer iCounterUser_BusinessUnit = 0;
        for (Territory2 oTerritory : mapTerritory.values()){
            if (oTerritory.Territory2Type.DeveloperName == 'Territory'){
                if (mapTerritoryID_Users.containsKey(oTerritory.Id)){
                    if (mapTerritoryID_Users.get(oTerritory.Id).size() > 0){

                        iCounterUser_DistrictManager = 0;
                        iCounterUser_BusinessUnit = 0;

                        oTerritory_Territory = oTerritory;
                        oTerritory_District = mapTerritory.get(oTerritory_Territory.ParentTerritory2Id);
                        oTerritory_Salesforce = mapTerritory.get(oTerritory_District.ParentTerritory2Id);
                        oTerritory_BusinessUnit = mapTerritory.get(oTerritory_Salesforce.ParentTerritory2Id);

                        System.assertNotEquals(oTerritory_Territory, null);
                        System.assertNotEquals(oTerritory_District, null);
                        System.assertNotEquals(oTerritory_Salesforce, null);
                        System.assertNotEquals(oTerritory_BusinessUnit, null);

                        try{
                            if (mapTerritoryID_Users.containsKey(oTerritory_District.Id)){
                                if (mapTerritoryID_Users.get(oTerritory_District.Id).size() > 0){
                                    List<User> lstUser_District = mapTerritoryID_Users.get(oTerritory_District.Id);
                                    for(User oUser : lstUser_District){
                                        if (oUser.Job_Title_vs__c == 'District Manager'){
                                            iCounterUser_DistrictManager++;
                                        }
                                    }
                                }
                            }
                            if (iCounterUser_DistrictManager == 0) continue;

                            if (mapTerritoryID_Users.containsKey(oTerritory_BusinessUnit.Id)){
                                if (mapTerritoryID_Users.get(oTerritory_BusinessUnit.Id).size() > 0){
                                    List<User> lstUser_BusinessUnit = mapTerritoryID_Users.get(oTerritory_BusinessUnit.Id);
                                    for(User oUser : lstUser_BusinessUnit){
                                        if (oUser.Job_Title_vs__c == 'Country BU Manager'){
                                            iCounterUser_BusinessUnit++;
                                        }
                                    }
                                }
                            }
                            if (iCounterUser_BusinessUnit == 0) continue;

                            if ( (iCounterUser_DistrictManager > 0) && (iCounterUser_BusinessUnit > 0) ){
                                break;
                            }
                        }catch(Exception oEX){}
                    }
                }
            }
        }

        System.assertNotEquals(mapTerritory.size(), 0);
        System.assertNotEquals(lstUserTerritory.size(), 0);
        //------------------------------------------------


        //------------------------------------------------
        // Load and Update Business Unit and Sub Business Unit  Data
        //------------------------------------------------
        Set<Id> setID_BU = new Set<Id>();
        Map<Id, Map<String, Id>> mapCompanyID_BU_Name_Id = new Map<Id, Map<String, Id>>();
        List<Sub_Business_Units__c> lstSBU = new List<Sub_Business_Units__c>();
        List<Business_Unit__c> lstBU = 
            [
                SELECT 
                    Id, Name, Company__c, Opp_RT_followed_by_Business_Owner__c, VP__c 
                    , (SELECT Id, Name, Segment_Leader__c FROM Sub_Business_Units__r)
                FROM Business_Unit__c
            ];
        for (Business_Unit__c oBU : lstBU){
            oBU.Opp_RT_followed_by_Business_Owner__c = tOpportunity_RecordTypeIds;
            oBU.VP__c = mapUser_ALL.values()[0].Id;
            oBU.Opportunity_Amount_Threshold__c = 50000;
            for (Sub_Business_Units__c oSBU : oBU.Sub_Business_Units__r){
                oSBU.Segment_Leader__c = mapUser_ALL.values()[1].Id;
                oSBU.Segment_Leader__c = mapUser_ALL.values()[2].Id;
                oSBU.Opportunity_Amount_Threshold__c = 30000;
                lstSBU.add(oSBU);
            }
            setID_BU.add(oBU.Id);

            Id idCompany = (Id)oBU.Company__c;
            Map<String, Id> mapBU_Name_ID = new Map<String, Id>();
            if (mapCompanyID_BU_Name_Id.containsKey(idCompany)){
                mapBU_Name_ID = mapCompanyID_BU_Name_Id.get(idCompany);
            }
            mapBU_Name_ID.put(oBU.Name, oBU.Id);
            mapCompanyID_BU_Name_Id.put(idCompany, mapBU_Name_ID);
        }
        update lstBU;
        update lstSBU;
        //------------------------------------------------


        //------------------------------------------------
        // Load Opportunity Test Data
        //------------------------------------------------
        Id idCompany = (Id)oTerritory_Territory.Company__c;

        System.debug('**BC** idCompany : ' + idCompany);
        System.debug('**BC** oTerritory_Territory.Id : ' + oTerritory_Territory.Id);
        System.debug('**BC** oTerritory_Territory.Business_Unit__c : ' + oTerritory_Territory.Business_Unit__c);
        System.debug('**BC** mapCompanyID_BU_Name_Id.get(idCompany) : ' + mapCompanyID_BU_Name_Id.get(idCompany));
        System.debug('**BC** mapCompanyID_BU_Name_Id  : ' + mapCompanyID_BU_Name_Id);
        System.debug('**BC** mapCompanyID_BU_Name_Id.values()  : ' + mapCompanyID_BU_Name_Id.values());

        List<Opportunity> lstOpportunity = 
            [
                SELECT 
                    Id, Amount, CurrencyIsoCode, isClosed, Territory2Id, RecordTypeId, RecordType.DeveloperName, Business_Unit__c, Business_Unit__r.Name, Country__c
                    , (
                        SELECT Id, PriceBookEntry.Pricebook2Id, PriceBookEntry.Product2.Product_Group__r.Therapy_Id__r.Sub_Business_Unit__c, PriceBookEntry.Product2.Product_Group__r.Therapy_Id__r.Business_Unit__c 
                        FROM OpportunityLineItems
                    )
                FROM 
                    Opportunity
                WHERE
                    Business_Unit__c = :mapCompanyID_BU_Name_Id.get(idCompany).get(oTerritory_Territory.Business_Unit__c)
                    and RecordTypeId in :setID_RecordType
                    and Amount > 50000
                    and isClosed = false
                LIMIT 10        
            ];

        List<EntitySubscription> lstEntitySubscription = 
            [
                SELECT Id, ParentId, SubscriberId, CreatedById
                FROM EntitySubscription
                WHERE ParentId = :lstOpportunity
            ];
        delete lstEntitySubscription;


        lstEntitySubscription = new List<EntitySubscription>();
        for (Opportunity oOpportunity : lstOpportunity){
            EntitySubscription oEntitySubscription1 = new EntitySubscription();
                oEntitySubscription1.ParentId = oOpportunity.Id;
                oEntitySubscription1.SubscriberId = mapUser_ALL.values()[8].Id;
            lstEntitySubscription.add(oEntitySubscription1);

            EntitySubscription oEntitySubscription2 = new EntitySubscription();
                oEntitySubscription2.ParentId = oOpportunity.Id;
                oEntitySubscription2.SubscriberId = mapUser_ALL.values()[9].Id;
            lstEntitySubscription.add(oEntitySubscription2);

        }
        insert lstEntitySubscription;
        //------------------------------------------------


        //------------------------------------------------
        // Execute Logic to test
        //------------------------------------------------
        Test.startTest();
/*
            // This Code results in an error 
            //  DEPENDENCY_EXISTS, Cannot complete this operation. This user is a target of a workflow field update
            System.runAs(lstUser_Admin[1]){
                // De-Activate used Users and execute process - this should not result in an error
                List<User> lstUser_Update = new List<User>();
                    mapUser_ALL.values()[0].IsActive = false;
                    mapUser_ALL.values()[0].User_Status__c = 'Left Company';
                    mapUser_ALL.values()[1].IsActive = false;
                    mapUser_ALL.values()[1].User_Status__c = 'Left Company';
                    mapUser_ALL.values()[2].IsActive = false;
                    mapUser_ALL.values()[2].User_Status__c = 'Left Company';
                lstUser_Update.add(mapUser_ALL.values()[0]);
                lstUser_Update.add(mapUser_ALL.values()[1]);
                lstUser_Update.add(mapUser_ALL.values()[2]);
                update lstUser_Update;
            }
            System.runAs(lstUser_Admin[0]){
                Boolean bError = false;
                try{
                    bl_Opportunity.followOpportunityBusinessLeader(lstOpportunity);
                }catch(Exception oEx){
                    bError = true;
                }
                System.assertEquals(bError, false);
            }
            System.runAs(lstUser_Admin[1]){
                // Re-Activate used Users and execute process
                List<User> lstUser_Update = new List<User>();
                    mapUser_ALL.values()[0].IsActive = true;
                    mapUser_ALL.values()[0].User_Status__c = '';
                    mapUser_ALL.values()[1].IsActive = true;
                    mapUser_ALL.values()[1].User_Status__c = '';
                    mapUser_ALL.values()[2].IsActive = true;
                    mapUser_ALL.values()[2].User_Status__c = '';
                lstUser_Update.add(mapUser_ALL.values()[0]);
                lstUser_Update.add(mapUser_ALL.values()[1]);
                lstUser_Update.add(mapUser_ALL.values()[2]);
                update lstUser_Update;
            }
*/
            System.runAs(lstUser_Admin[0]){
                bl_Opportunity.followOpportunityBusinessLeader(lstOpportunity);
            }


            System.runAs(lstUser_Admin[0]){
                for (Opportunity oOpportunity : lstOpportunity){
                    oOpportunity.StageName = 'Closed Won';
                    oOpportunity.Reason_Won__c = 'We are the best!';
                }
                update lstOpportunity;
//              Database.update(lstOpportunity, false);

                List<Opportunity> lstOpportunity_Closed = 
                    [
                        SELECT 
                            Id, Amount, CurrencyIsoCode, isClosed, Territory2Id, RecordTypeId, RecordType.DeveloperName, Business_Unit__c, Business_Unit__r.Name, Country__c
                            , (
                                SELECT Id, PriceBookEntry.Pricebook2Id, PriceBookEntry.Product2.Product_Group__r.Therapy_Id__r.Sub_Business_Unit__c, PriceBookEntry.Product2.Product_Group__r.Therapy_Id__r.Business_Unit__c 
                                FROM OpportunityLineItems
                            )
                        FROM 
                            Opportunity
                        WHERE
                            Id = :lstOpportunity
                    ];


                bl_Opportunity.followOpportunityBusinessLeader(lstOpportunity_Closed);
            }
        
        Test.stopTest();
        //------------------------------------------------
    }

    @isTest static void followOpportunityBusinessLeader_Service() {
        //------------------------------------------------
        // Load User Data
        //------------------------------------------------
        List<User> lstUser_Admin = [SELECT Id, Name FROM User WHERE (Profile.Name = 'System Administrator MDT' OR Profile.Name = 'System Administrator') AND IsActive = true AND License_Name__c = 'Salesforce'];

        Map<Id, User> mapUser_ALL = new Map<Id, User>([SELECT Id, Job_Title_vs__c FROM User WHERE IsActive = true and UserType = 'Standard' AND Id != :lstUser_Admin AND License_Name__c = 'Salesforce']);
        System.assertNotEquals(mapUser_ALL.size(), 0);
        //------------------------------------------------

        
        //------------------------------------------------
        // Load Opportunity Recordtype Data	
        //------------------------------------------------
        clsUtil.loadRecordTypeMap();
        Map<String, RecordType> mapRecordType = clsUtil.mapObjectName_RecordTypeName_RecordType.get('Opportunity');
        Id idRecordType_Opportunity_Service = clsUtil.getRecordTypeByDevName('Opportunity', 'Service_Contract_Opportunity').Id;

        Set<Id> setID_RecordType = new Set<Id>();
        setID_RecordType.add(idRecordType_Opportunity_Service);
        String tOpportunity_RecordTypeIds = idRecordType_Opportunity_Service;
        //------------------------------------------------


        //------------------------------------------------
        // Load and Update Business Unit and Sub Business Unit  Data
        //------------------------------------------------
        Set<Id> setID_BU = new Set<Id>();
        Map<Id, Map<String, Id>> mapCompanyID_BU_Name_Id = new Map<Id, Map<String, Id>>();
        List<Sub_Business_Units__c> lstSBU = new List<Sub_Business_Units__c>();
        List<Business_Unit__c> lstBU = 
            [
                SELECT 
                    Id, Name, Company__c, Opp_ServiceRT_followed_by_Business_Owner__c, VP__c 
                    , (SELECT Id, Name, Segment_Leader__c FROM Sub_Business_Units__r)
                FROM Business_Unit__c
            ];
        for (Business_Unit__c oBU : lstBU){
            oBU.Opp_ServiceRT_followed_by_Business_Owner__c = tOpportunity_RecordTypeIds;
            oBU.VP__c = mapUser_ALL.values()[0].Id;
            oBU.Opportunity_Amount_Threshold__c = 50000;
            for (Sub_Business_Units__c oSBU : oBU.Sub_Business_Units__r){
                oSBU.Segment_Leader__c = mapUser_ALL.values()[1].Id;
                oSBU.Segment_Leader__c = mapUser_ALL.values()[2].Id;
                oSBU.Opportunity_Amount_Threshold__c = 30000;
                lstSBU.add(oSBU);
            }
            setID_BU.add(oBU.Id);

            Id idCompany = (Id)oBU.Company__c;
            Map<String, Id> mapBU_Name_ID = new Map<String, Id>();
            if (mapCompanyID_BU_Name_Id.containsKey(idCompany)){
                mapBU_Name_ID = mapCompanyID_BU_Name_Id.get(idCompany);
            }
            mapBU_Name_ID.put(oBU.Name, oBU.Id);
            mapCompanyID_BU_Name_Id.put(idCompany, mapBU_Name_ID);
        }
        update lstBU;
        update lstSBU;
        //------------------------------------------------


        //------------------------------------------------
        // Load Opportunity Test Data
        //------------------------------------------------
        List<Opportunity> lstOpportunity = 
            [
                SELECT 
                    Id, Amount, CurrencyIsoCode, isClosed, Territory2Id, RecordTypeId, RecordType.DeveloperName, Business_Unit__c, Business_Unit__r.Name, Country__c
                    , (
                        SELECT Id, PriceBookEntry.Pricebook2Id, PriceBookEntry.Product2.Product_Group__r.Therapy_Id__r.Sub_Business_Unit__c, PriceBookEntry.Product2.Product_Group__r.Therapy_Id__r.Business_Unit__c 
                        FROM OpportunityLineItems
                    )
                FROM 
                    Opportunity
                WHERE
                    RecordTypeId in :setID_RecordType
                    and Amount > 30000
                    and isClosed = false
                LIMIT 10        
            ];
        //------------------------------------------------

        if (lstOpportunity.size() > 0){
            System.assertNotEquals(lstOpportunity.size(), 0);

            List<EntitySubscription> lstEntitySubscription = 
                [
                    SELECT Id, ParentId, SubscriberId, CreatedById
                    FROM EntitySubscription
                    WHERE ParentId = :lstOpportunity
                ];
            delete lstEntitySubscription;


            lstEntitySubscription = new List<EntitySubscription>();
            Set<String> setCountryName = new Set<String>();
            for (Opportunity oOpportunity : lstOpportunity){
                if (oOpportunity.Country__c != null){
                    setCountryName.add(oOpportunity.Country__c);
                }

                EntitySubscription oEntitySubscription1 = new EntitySubscription();
                    oEntitySubscription1.ParentId = oOpportunity.Id;
                    oEntitySubscription1.SubscriberId = mapUser_ALL.values()[8].Id;
                lstEntitySubscription.add(oEntitySubscription1);

                EntitySubscription oEntitySubscription2 = new EntitySubscription();
                    oEntitySubscription2.ParentId = oOpportunity.Id;
                    oEntitySubscription2.SubscriberId = mapUser_ALL.values()[9].Id;
                lstEntitySubscription.add(oEntitySubscription2);

            }
            insert lstEntitySubscription;


            //------------------------------------------------
            // Load and Update Country Data
            //------------------------------------------------
            List<DIB_Country__c> lstCountry = 
                [
                    SELECT
                        Id, Name, Opportunity_Amount_Threshold__c, Service_Marketing_Manager__c, Service_Segment_Leader__c, Local_Service_Manager__c
                    FROM 
                        DIB_Country__c
                    WHERE
                        Name = :setCountryName
                ];
            for (DIB_Country__c oCountry : lstCountry){
                oCountry.Opportunity_Amount_Threshold__c = 30000;
                oCountry.Service_Marketing_Manager__c = mapUser_ALL.values()[3].Id;
                oCountry.Service_Segment_Leader__c = mapUser_ALL.values()[4].Id;
                oCountry.Local_Service_Manager__c = mapUser_ALL.values()[5].Id;
            }
            update lstCountry;
            //------------------------------------------------


            //------------------------------------------------
            // Execute Logic to test
            //------------------------------------------------
            Test.startTest();
/*
                System.runAs(lstUser_Admin[0]){
                    bl_Opportunity.followOpportunityBusinessLeader(lstOpportunity);
                }

                // This Code results in an error 
                //  DEPENDENCY_EXISTS, Cannot complete this operation. This user is a target of a workflow field update
                System.runAs(lstUser_Admin[1]){
                    // De-Activate used Users and execute process - this should not result in an error
                    List<User> lstUser_Update = new List<User>();
                        mapUser_ALL.values()[0].IsActive = false;
                        mapUser_ALL.values()[0].User_Status__c = 'Left Company';
                        mapUser_ALL.values()[1].IsActive = false;
                        mapUser_ALL.values()[1].User_Status__c = 'Left Company';
                        mapUser_ALL.values()[2].IsActive = false;
                        mapUser_ALL.values()[2].User_Status__c = 'Left Company';
                    lstUser_Update.add(mapUser_ALL.values()[0]);
                    lstUser_Update.add(mapUser_ALL.values()[1]);
                    lstUser_Update.add(mapUser_ALL.values()[2]);
                    update lstUser_Update;
                }
                System.runAs(lstUser_Admin[0]){
                    Boolean bError = false;
                    try{
                        bl_Opportunity.followOpportunityBusinessLeader(lstOpportunity);
                    }catch(Exception oEx){
                        bError = true;
                    }
                    System.assertEquals(bError, false);
                }
                System.runAs(lstUser_Admin[1]){
                    // Re-Activate used Users and execute process
                    List<User> lstUser_Update = new List<User>();
                        mapUser_ALL.values()[0].IsActive = true;
                        mapUser_ALL.values()[0].User_Status__c = '';
                        mapUser_ALL.values()[1].IsActive = true;
                        mapUser_ALL.values()[1].User_Status__c = '';
                        mapUser_ALL.values()[2].IsActive = true;
                        mapUser_ALL.values()[2].User_Status__c = '';
                    lstUser_Update.add(mapUser_ALL.values()[0]);
                    lstUser_Update.add(mapUser_ALL.values()[1]);
                    lstUser_Update.add(mapUser_ALL.values()[2]);
                    update lstUser_Update;
                }
*/
                System.runAs(lstUser_Admin[0]){
                    bl_Opportunity.followOpportunityBusinessLeader_Service(lstOpportunity);

                    for (Opportunity oOpportunity : lstOpportunity){
                        oOpportunity.StageName = 'Closed Won';
                        oOpportunity.Reason_Won__c = 'We are the best!';
                    }
                    Database.update(lstOpportunity, false);

                    lstOpportunity = 
                        [
                            SELECT 
                                Id, Amount, CurrencyIsoCode, isClosed, Territory2Id, RecordTypeId, RecordType.DeveloperName, Business_Unit__c, Business_Unit__r.Name, Country__c
                                , (
                                    SELECT Id, PriceBookEntry.Pricebook2Id, PriceBookEntry.Product2.Product_Group__r.Therapy_Id__r.Sub_Business_Unit__c, PriceBookEntry.Product2.Product_Group__r.Therapy_Id__r.Business_Unit__c 
                                    FROM OpportunityLineItems
                                )
                            FROM 
                                Opportunity
                            WHERE
                                Id = :lstOpportunity
                        ];

                    bl_Opportunity.followOpportunityBusinessLeader_Service(lstOpportunity);
                }

            Test.stopTest();
            //------------------------------------------------
        }
    }   
    //--------------------------------------------------------------------------------------------------------------------------------------

}