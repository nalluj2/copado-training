//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 20-01-2016
//  Description      : APEX Test Class to test the logic in tr_Case and bl_Case_Trigger
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_tr_Case {
	
	private static User currentUser = new User(Id = UserInfo.getUserId());
	
    //----------------------------------------------------------------------------------------
    // CREATE TEST DATA
    //----------------------------------------------------------------------------------------
    @testSetup static void createTestData() {
				
		System.runAs(currentUser){

	        // Create Custom Setting Data
	        clsTestData.createCustomSettingData(true);
	
	        // Create Account Data Data
	        clsTestData.tCountry_Account = 'United Kingdom';
	        clsTestData.iRecord_Account = 1;
	        clsTestData.createAccountData(true);
	
	        // Create Case Data
	        clsTestData.createCaseData(false);
	        Integer iCounter = 1;
	        for (Case oCase : clsTestData.lstCase){
	        	oCase.SVMX_SAP_Notification_Number__c = 'SAP' + String.valueOf(iCounter);
	        	iCounter++;
	        }
	        insert clsTestData.lstCase;
		}

    }
    //----------------------------------------------------------------------------------------

	
    //----------------------------------------------------------------------------------------
    // Basic Testing
    //----------------------------------------------------------------------------------------
	@isTest static void test_tr_Case() {
	
		// Select Data
		List<Case> lstData = [SELECT Id, CaseNumber, Status FROM Case];
		Case oData = lstData[0];
		
		// Update Data
		update oData;

		// Delete Data
		delete oData;

		// Undelete Data
		Undelete oData;

	}
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
    // Test related to notificationSAP
    //----------------------------------------------------------------------------------------
	@isTest static void test_notificationSAP_Update() {

		// Select Data
		List<Case> lstData = [SELECT Id, CaseNumber, Status FROM Case];
		Case oData = lstData[0];

		// UPDATE
		Test.startTest();

			oData.Status = 'Completed';
		update oData;

		Test.stopTest();

	}
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
    // Test Prevent Update on Restricted Fields
    //----------------------------------------------------------------------------------------
	@isTest static void test_preventUpdateOnRestrictedFields_System() {

		// Select Data
		List<Case> lstData = [SELECT Id, SVMX_SAP_Notification_Number__c FROM Case WHERE SVMX_SAP_Notification_Number__c != null];
		Case oData = lstData[0];

		String tSAPNotificationNumber = oData.SVMX_SAP_Notification_Number__c;
		System.assertNotEquals(clsUtil.isNull(tSAPNotificationNumber, ''), 'T1E1S1T1');

		// UPDATE
		Test.startTest();

			oData.SVMX_SAP_Notification_Number__c = 'T1E1S1T1';
		update oData;

		Test.stopTest();

		// Validate that the update has been performed
		lstData = [SELECT Id, SVMX_SAP_Notification_Number__c FROM Case WHERE Id = :oData.Id];
		oData = lstData[0];
		System.assertEquals(oData.SVMX_SAP_Notification_Number__c, 'T1E1S1T1');
		System.assertNotEquals(oData.SVMX_SAP_Notification_Number__c, tSAPNotificationNumber);

	}

	@isTest static void test_preventUpdateOnRestrictedFields_NotSystem() {

		// Get the User Profile and Role
		Profile oProfile = [SELECT Id FROM Profile WHERE Name = 'EUR Field Service Engineer' LIMIT 1];
		UserRole oUserRole = [SELECT Id FROM UserRole WHERE DeveloperName = 'UKxBU' LIMIT 1];

		// Select an Active User based on the collected Profile and Role
		//	If there is not Active User available, create one
		List<User> lstUser = [SELECT Id FROM User WHERE IsActive = true AND ProfileId = :oProfile.Id AND UserRoleId = :oUserRole.Id LIMIT 1];
		User oUser;
		if (lstUser.size() == 1){
			oUser = lstUser[0];
		}else{
			
			System.runAs(currentUser){
				
				clsTestData.createUserData(clsUtil.getTimeStamp() + '@test.medtronic.com', 'UNITED KINGDOM', oProfile.Id, oUserRole.Id, true);
				oUser = clsTestData.oMain_User;
			}
		}

		Test.startTest();
		
		List<Case> lstData = [SELECT Id, SVMX_SAP_Notification_Number__c FROM Case WHERE SVMX_SAP_Notification_Number__c != null];
		Case oData = lstData[0];
		oData.OwnerId = oUser.Id;
		update oData;
		
		String tSAPNotificationNumber = '';

		// UPDATE		
			System.runAs(oUser){

				tSAPNotificationNumber = oData.SVMX_SAP_Notification_Number__c;

				System.assertNotEquals(clsUtil.isNull(oData.SVMX_SAP_Notification_Number__c, ''), 'T1E1S1T1');

					oData.SVMX_SAP_Notification_Number__c = 'T1E1S1T1';
				update oData;
			}

		Test.stopTest();

		// Validate that the update has NOT been performed		
		oData = [SELECT Id, SVMX_SAP_Notification_Number__c FROM Case WHERE Id = :oData.Id];
		System.assertEquals(clsUtil.isNull(oData.SVMX_SAP_Notification_Number__c, ''), tSAPNotificationNumber);

	}
    //----------------------------------------------------------------------------------------	
    
    private static testmethod void testCloneNeuroOMACase(){
    	
    	Account testAcc = new Account();
    	testAcc.Name = 'Test Account';
    	testAcc.Account_Country_vs__c = 'USA';
    	testAcc.RecordTypeId = [Select Id from RecordType where SObjectType = 'Account' AND DeveloperName = 'NON_SAP_Account' ].Id;
    	insert testAcc;
    	
    	Contact testCnt1 = new Contact();
    	testCnt1.FirstName = 'Test';
    	testCnt1.LastName = 'Contact 1';
    	testCnt1.AccountId = testAcc.Id;
    	testCnt1.RecordTypeId = [Select Id from RecordType where SObjectType = 'Contact' AND DeveloperName = 'Generic_Contact'].Id;
    	testCnt1.Contact_Department__c = 'Cardiology';
    	testCnt1.Contact_Primary_Specialty__c = 'Cardiology';
    	testCnt1.Affiliation_To_Account__c = 'Employee';
    	testCnt1.Primary_Job_Title_vs__c = 'Head Nurse';
    	testCnt1.Contact_Gender__c = 'Male';
    	
    	Contact testCnt2 = new Contact();
    	testCnt2.FirstName = 'Test';
    	testCnt2.LastName = 'Contact 2';
    	testCnt2.AccountId = testAcc.Id;
    	testCnt2.RecordTypeId = testCnt1.RecordTypeId;
    	testCnt2.Contact_Department__c = 'Cardiology';
    	testCnt2.Contact_Primary_Specialty__c = 'Cardiology';
    	testCnt2.Affiliation_To_Account__c = 'Employee';
    	testCnt2.Primary_Job_Title_vs__c = 'Head Nurse';
    	testCnt2.Contact_Gender__c = 'Female';
    	
    	insert new List<Contact>{testCnt1, testCnt2};
    	
    	Case omaCase = new Case();
    	omaCase.RecordTypeId = [Select Id from RecordType where SObjectType = 'Case' AND DeveloperName = 'OMA'].Id;
    	omaCase.AccountId = testAcc.Id;
    	omaCase.ContactId = testCnt1.Id;    	
    	omaCase.Status = 'New';
    	insert omaCase;
    	
    	Case_Contact_Roles__c contactRole1 = new Case_Contact_Roles__c();
    	contactRole1.Case__c = omaCase.Id;
    	contactRole1.Contact__c = testCnt1.Id;
    	contactRole1.Role__c = 'Field Employee';
    	contactRole1.RecordTypeId = [Select Id from RecordType where SObjectType = 'Case_Contact_Roles__c' AND DeveloperName = 'OMA_Case_Contact'].Id;
    	
    	Case_Contact_Roles__c contactRole2 = new Case_Contact_Roles__c();
    	contactRole2.Case__c = omaCase.Id;
    	contactRole2.Contact__c = testCnt2.Id;
    	contactRole2.Role__c = 'Field Employee';
    	contactRole2.RecordTypeId = contactRole1.RecordTypeId;
    	
    	insert new List<Case_Contact_Roles__c>{contactRole1, contactRole2};
    	
    	Attachment caseAttachment = new Attachment();
		caseAttachment.ParentId = omaCase.Id;
		caseAttachment.Name = 'Unit Test Attachment';
		caseAttachment.Description = 'Unit Test Attachment';
		caseAttachment.Body = Blob.valueOf('Unit Test Attachment');
		insert caseAttachment;
		
		Test.startTest();
		
		Case omaCaseClone = new Case();
    	omaCaseClone.RecordTypeId = omaCase.RecordTypeId;
    	omaCaseClone.AccountId = testAcc.Id;
    	omaCaseClone.ContactId = testCnt1.Id;    	
    	omaCaseClone.Status = 'New';
    	omaCaseClone.Cloned_From__c = omaCase.Id;
    	insert omaCaseClone;
    	
    	List<Case_Contact_Roles__c> clonedContactRoles = [Select Id from Case_Contact_Roles__c where Case__c = :omaCaseClone.Id];
    	System.assert(clonedContactRoles.size() == 2);
    	
    	List<Attachment> clonedAttachments = [Select Id from Attachment where ParentId = :omaCaseClone.Id];
    	System.assert(clonedAttachments.size() == 1);
    } 
    
    private static testmethod void testCloneSpineOMACase(){
    	
    	Account testAcc = new Account();
    	testAcc.Name = 'Test Account';
    	testAcc.Account_Country_vs__c = 'USA';
    	testAcc.RecordTypeId = [Select Id from RecordType where SObjectType = 'Account' AND DeveloperName = 'NON_SAP_Account' ].Id;
    	insert testAcc;
    	
    	Contact testCnt1 = new Contact();
    	testCnt1.FirstName = 'Test';
    	testCnt1.LastName = 'Contact 1';
    	testCnt1.AccountId = testAcc.Id;
    	testCnt1.RecordTypeId = [Select Id from RecordType where SObjectType = 'Contact' AND DeveloperName = 'Generic_Contact'].Id;
    	testCnt1.Contact_Department__c = 'Cardiology';
    	testCnt1.Contact_Primary_Specialty__c = 'Cardiology';
    	testCnt1.Affiliation_To_Account__c = 'Employee';
    	testCnt1.Primary_Job_Title_vs__c = 'Head Nurse';
    	testCnt1.Contact_Gender__c = 'Male';
    	
    	Contact testCnt2 = new Contact();
    	testCnt2.FirstName = 'Test';
    	testCnt2.LastName = 'Contact 2';
    	testCnt2.AccountId = testAcc.Id;
    	testCnt2.RecordTypeId = testCnt1.RecordTypeId;
    	testCnt2.Contact_Department__c = 'Cardiology';
    	testCnt2.Contact_Primary_Specialty__c = 'Cardiology';
    	testCnt2.Affiliation_To_Account__c = 'Employee';
    	testCnt2.Primary_Job_Title_vs__c = 'Head Nurse';
    	testCnt2.Contact_Gender__c = 'Female';
    	
    	insert new List<Contact>{testCnt1, testCnt2};
    	
    	Case omaCase = new Case();
    	omaCase.RecordTypeId = [Select Id from RecordType where SObjectType = 'Case' AND DeveloperName = 'OMA_Spine_Clinical_Support_Observation'].Id;
    	omaCase.AccountId = testAcc.Id;
    	omaCase.ContactId = testCnt1.Id;    	
    	omaCase.Status = 'New';
    	insert omaCase;
    	
    	Case_Contact_Roles__c contactRole1 = new Case_Contact_Roles__c();
    	contactRole1.Case__c = omaCase.Id;
    	contactRole1.Contact__c = testCnt1.Id;
    	contactRole1.Role__c = 'Field Employee';
    	contactRole1.RecordTypeId = [Select Id from RecordType where SObjectType = 'Case_Contact_Roles__c' AND DeveloperName = 'OMA_Case_Contact'].Id;
    	
    	Case_Contact_Roles__c contactRole2 = new Case_Contact_Roles__c();
    	contactRole2.Case__c = omaCase.Id;
    	contactRole2.Contact__c = testCnt2.Id;
    	contactRole2.Role__c = 'Field Employee';
    	contactRole2.RecordTypeId = contactRole1.RecordTypeId;
    	
    	insert new List<Case_Contact_Roles__c>{contactRole1, contactRole2};
    	
    	Attachment caseAttachment = new Attachment();
		caseAttachment.ParentId = omaCase.Id;
		caseAttachment.Name = 'Unit Test Attachment';
		caseAttachment.Description = 'Unit Test Attachment';
		caseAttachment.Body = Blob.valueOf('Unit Test Attachment');
		insert caseAttachment;
		
		Test.startTest();
		
		Case omaCaseClone = new Case();
    	omaCaseClone.RecordTypeId = omaCase.RecordTypeId;
    	omaCaseClone.AccountId = testAcc.Id;
    	omaCaseClone.ContactId = testCnt1.Id;    	
    	omaCaseClone.Status = 'New';
    	omaCaseClone.Cloned_From__c = omaCase.Id;
    	insert omaCaseClone;
    	
    	List<Case_Contact_Roles__c> clonedContactRoles = [Select Id from Case_Contact_Roles__c where Case__c = :omaCaseClone.Id];
    	System.assert(clonedContactRoles.size() == 0);
    	
    	List<Attachment> clonedAttachments = [Select Id from Attachment where ParentId = :omaCaseClone.Id];
    	System.assert(clonedAttachments.size() == 0);
    } 

	@isTest static void test_setCaseComplexity() {

		// Create Test Data
		clsTestData.createAccountData(false);
			clsTestData.oMain_Account.isSales_Force_Account__c = false;
		insert clsTestData.oMain_Account;

		clsTestData.iRecord_Case = 5;
		clsTestData.createCaseData(false);
		for (Case oCase : clsTestData.lstCase){
			oCase.Case_Complexity__c = '2';
			oCase.RecordTypeId = clsUtil.getRecordTypeByDevName('Case', 'OMA_Neoro_MedInfo').Id;
		}

		// Test Logic
		Test.startTest();

			Case oCase;

			// TEST INSERT
			insert clsTestData.lstCase[0];

			oCase = [SELECT Case_Complexity__c FROM Case WHERE Id = :clsTestData.lstCase[0].Id];
			System.assertEquals(oCase.Case_Complexity__c, '2');


				clsTestData.lstCase[1].Repeat_Case__c = true;
				clsTestData.lstCase[1].Standard_Response__c = false;
				clsTestData.lstCase[1].Internal_Data__c = false;
			insert clsTestData.lstCase[1];

			oCase = [SELECT Case_Complexity__c FROM Case WHERE Id = :clsTestData.lstCase[1].Id];
			System.assertEquals(oCase.Case_Complexity__c, '1');


				clsTestData.lstCase[2].Repeat_Case__c = false;
				clsTestData.lstCase[2].Standard_Response__c = true;
				clsTestData.lstCase[2].Internal_Data__c = false;
			insert clsTestData.lstCase[2];

			oCase = [SELECT Case_Complexity__c FROM Case WHERE Id = :clsTestData.lstCase[2].Id];
			System.assertEquals(oCase.Case_Complexity__c, '1');


				clsTestData.lstCase[3].Repeat_Case__c = true;
				clsTestData.lstCase[3].Standard_Response__c = true;
				clsTestData.lstCase[3].Internal_Data__c = false;
			insert clsTestData.lstCase[3];

			oCase = [SELECT Case_Complexity__c FROM Case WHERE Id = :clsTestData.lstCase[3].Id];
			System.assertEquals(oCase.Case_Complexity__c, '1');


				clsTestData.lstCase[4].Repeat_Case__c = false;
				clsTestData.lstCase[4].Standard_Response__c = false;
				clsTestData.lstCase[4].Internal_Data__c = true;
			insert clsTestData.lstCase[4];

			oCase = [SELECT Case_Complexity__c FROM Case WHERE Id = :clsTestData.lstCase[4].Id];
			System.assertEquals(oCase.Case_Complexity__c, '3');


			// TEST UPDATE
			oCase = [SELECT Case_Complexity__c FROM Case WHERE Id = :clsTestData.lstCase[0].Id];


				oCase.Repeat_Case__c = true;
				oCase.Standard_Response__c = false;
				oCase.Internal_Data__c = false;
			update oCase;

			oCase = [SELECT Case_Complexity__c FROM Case WHERE Id = :clsTestData.lstCase[0].Id];
			System.assertEquals(oCase.Case_Complexity__c, '1');



				oCase.Repeat_Case__c = false;
				oCase.Standard_Response__c = false;
				oCase.Internal_Data__c = true;
			update oCase;

			oCase = [SELECT Case_Complexity__c FROM Case WHERE Id = :clsTestData.lstCase[0].Id];
			System.assertEquals(oCase.Case_Complexity__c, '3');



				oCase.Repeat_Case__c = false;
				oCase.Standard_Response__c = true;
				oCase.Internal_Data__c = false;
			update oCase;

			oCase = [SELECT Case_Complexity__c FROM Case WHERE Id = :clsTestData.lstCase[0].Id];
			System.assertEquals(oCase.Case_Complexity__c, '1');



				oCase.Repeat_Case__c = false;
				oCase.Standard_Response__c = false;
				oCase.Internal_Data__c = false;
			update oCase;

			oCase = [SELECT Case_Complexity__c FROM Case WHERE Id = :clsTestData.lstCase[0].Id];
			System.assertEquals(oCase.Case_Complexity__c, '2');



				oCase.Repeat_Case__c = true;
				oCase.Standard_Response__c = true;
				oCase.Internal_Data__c = false;
			update oCase;

			oCase = [SELECT Case_Complexity__c FROM Case WHERE Id = :clsTestData.lstCase[0].Id];
			System.assertEquals(oCase.Case_Complexity__c, '1');

		Test.stopTest();


	}

}
//--------------------------------------------------------------------------------------------------------------------