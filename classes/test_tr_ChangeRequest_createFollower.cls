@isTest
public class test_tr_ChangeRequest_createFollower{
    private static testMethod void unitTest(){
        
        Company__c cmpny = new Company__c();
         cmpny.name='TestMedtronic';
         cmpny.CurrencyIsoCode = 'EUR';
         cmpny.Current_day_in_Q1__c=56;
         cmpny.Current_day_in_Q2__c=34; 
         cmpny.Current_day_in_Q3__c=5; 
         cmpny.Current_day_in_Q4__c= 0;   
         cmpny.Current_day_in_year__c =200;
         cmpny.Days_in_Q1__c=57;  
         cmpny.Days_in_Q2__c=35;
         cmpny.Days_in_Q3__c=13;
         cmpny.Days_in_Q4__c=22;
         cmpny.Days_in_year__c =250;
         cmpny.Company_Code_Text__c = 'T33';
         insert cmpny;

        User u1=[select id,name from User where Profile.name='System Administrator' and isActive=:True limit 1];
        
        User u2=[select id,name from User where Profile.name='System Administrator MDT' and isActive=:True limit 1];
        
        Change_Request__c CR=new Change_Request__c();
        CR.Change_Request__c='Test CR';
        CR.Origin__c='Support';
        CR.Request_Date__c=System.today();
        CR.Requestor_Lookup__c=u1.id;
        CR.Company__c=cmpny.id;
        insert CR;
        
        CR.Requestor_Lookup__c=u2.id;
        update CR;
        
    }

}