@isTest
private class Test_SynchronizationService_Comment {
	
	private static testMethod void syncComment(){
		
		Synchronization_Service_Setting__c syncSettings = new Synchronization_Service_Setting__c();
		syncSettings.username__c = 'test@medtronic.com';
		syncSettings.password__c = 'password';
		syncSettings.client_id__c = '123456789';
		syncSettings.client_secret__c = 'XXXXX';
		syncSettings.is_sandbox__c = true;
		syncSettings.sync_Enabled__c = true;
		insert syncSettings;
		
		//Products
		Product2 testProduct = new Product2();
		testProduct.Name = 'PoleStar';
		testProduct.ProductCode = '000999';
		testProduct.isActive = true;
					
		insert testProduct;
		
		//Account
		Account acc = new Account();	
		acc.AccountNumber = '0000000';
		acc.Name = 'Syn Test Account';
		acc.Phone = '+123456789';
		acc.BillingCity = 'Minneapolis';
		acc.BillingCountry = 'UNITED STATES';
		acc.BillingState = 'Minnesota';
		acc.BillingStreet = 'street';
		acc.BillingPostalCode = '123456';
		acc.ST_NAV_Non_SAP_Account__c = true; //-BC - 20161101 - Added
		acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non-SAP Account').getRecordTypeId();	
		insert acc;
		
		//Contact
		Contact cnt = new Contact();
		cnt.FirstName = 'Test';
		cnt.LastName = 'Contact';
		cnt.Phone = '+123456789';		
		cnt.Email = 'test.contact@gmail.com';
		cnt.MobilePhone = '+123456789';		
		cnt.MailingCity = 'Minneapolis';
		cnt.MailingCountry = 'UNITED STATES';
		cnt.MailingState = 'Minnesota';
		cnt.MailingStreet = 'street';
		cnt.MailingPostalCode = '123456';
		cnt.External_Id__c = 'Test_Contact_Id';
		cnt.Contact_Department__c = 'Cardiology';
		cnt.Contact_Primary_Specialty__c = 'Cardiovascular'; 
		cnt.Affiliation_To_Account__c = 'Employee'; 
		cnt.Primary_Job_Title_vs__c = 'Surgeon'; 
		cnt.Contact_Gender__c = 'Male'; 
		cnt.AccountId = acc.Id;
		cnt.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Generic Contact').getRecordTypeId();				
		insert cnt;
		
		//Asset
		Asset asset = new Asset();
		asset.AccountId = acc.Id;
		asset.Product2Id = testProduct.Id;
		asset.Asset_Product_Type__c = 'PoleStar N-10';
		asset.Ownership_Status__c = 'Purchased';
		asset.Name = '123456789';
		asset.Serial_Nr__c = '123456789';
		asset.Status = 'Installed';
		asset.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('PoleStar').getRecordTypeId();
		insert asset;
				
		//Complaint
		Complaint__c comp = new Complaint__c();
		comp.Account_Name__c = acc.Id;
		comp.Asset__c = asset.Id;
		comp.Status__c = 'Open';
		comp.External_Id__c = 'Test_Complaint_Id';
		insert comp;
		
		//Case
		Case newCase = new Case();
		newCase.AccountId = acc.Id;
		newCase.ContactId = cnt.Id;
		newCase.AssetId = asset.Id;
		newCase.RecordTypeId = clsUtil.getRecordTypeByDevName('Case', 'New_Case').Id;
		newCase.External_Id__c = 'Test_Case_Id';					
		insert newCase;
		
		//Workorder
		Workorder__c workOrder = new Workorder__c();
		workOrder.Account__c = acc.Id;
		workOrder.Asset__c = asset.Id;
		workOrder.Status__c = 'In Process';
		workOrder.External_Id__c = 'Test_Work_Order_Id';
		workOrder.RecordTypeId = Schema.SObjectType.Workorder__c.getRecordTypeInfosByName().get('PoleStar System Checkout').getRecordTypeId();
		insert workOrder;
		
		//Case Part
		Workorder_Sparepart__c sparePart = new Workorder_Sparepart__c();
		sparePart.Case__c = newCase.Id;
		sparePart.Account__c = acc.Id;
		sparePart.Asset__c = asset.Id;
		sparePart.External_Id__c = 'Test_Case_Part_Id';		
		sparePart.RecordTypeId = Schema.SObjectType.Workorder_Sparepart__c.getRecordTypeInfosByName().get('Hardware').getRecordTypeId();
		insert sparePart;	
				
		Test.startTest();
		
		Test_bl_SynchronizationService_Utils.CalloutMock mockImpl = new Test_bl_SynchronizationService_Utils.CalloutMock();
		
		Test.setMock(HttpCalloutMock.class, mockImpl);
		bl_SynchronizationService_Utils.calloutMock = mockImpl;
						
		//Comment
		Case_Comment__c comment = new Case_Comment__c();
		comment.External_Id__c = 'Test_Comment_Id';
		comment.Comment__c = 'Unit test comment';
		comment.Comment_Subject__c = 'Test Comment Subject';
		comment.Case__c = newCase.Id;
		comment.Complaint__c = comp.Id;
		comment.Workorder__c = workOrder.Id;
		comment.Case_Part__c = sparepart.Id;		
		insert comment;	
							
		SynchronizationService_Comment commentService = new SynchronizationService_Comment();
		commentService.generatePayload('Test_Comment_Id');
		
		commentService.processPayload(null);			
	}
}