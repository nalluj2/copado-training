/*
 *      Description : This class is the Controller Extension for the VF Page Case_GenerateCalendarEvent - the standard controller is Case.
 *          Based on the provided Case we build a Redirect URL to pass different parameters from the Case to the Task will be pre-filled with these parameters.
 *          We need to do this with a VF Page and not by a Custom Button on Case, because we want to pass the Date and Time to the Task page.  
 *              On Case this is stored in 1 field, but on the Task page this is visible in 2 fields 
 *          The Case will also be set to Closed.
 *      Version = 1.0
 *      Author  = Bart Caelen
 *      Date    = 2014-06-19
*/
public class ctrlExt_Case_GenerateCalendarEvent {

    //------------------------------------------------------------------------
    // Private Variables
    //------------------------------------------------------------------------
    private final Case oCase;
    private String tStartDateTimeValue  = '';
    private String tStartDateValue      = '';
    private String tStartTimeValue      = '';
    private String tEndDateTimeValue    = '';
    private String tEndDateValue        = '';
    private String tEndTimeValue        = '';
    //------------------------------------------------------------------------

    //-----------------------------------------------------------------------
    // Constructor
    //------------------------------------------------------------------------
    public ctrlExt_Case_GenerateCalendarEvent(ApexPages.StandardController stdController) {
        
        this.oCase              = (Case)stdController.getRecord();

        Datetime dtStart        = oCase.Start_of_Procedure__c;
        Datetime dtEnd          = oCase.End_of_Procedure__c;

        // Convert the DateTime to the local time zone 
        //  and returns the converted date as a formatted string using the locale of the context user
        if ( clsUtil.isNull(dtStart, '') != '' )    tStartDateTimeValue = dtStart.format();
        if ( clsUtil.isNull(dtEnd, '') != '' )      tEndDateTimeValue   = dtEnd.format();

        // Split the converted DateTime value to get the Date and Time value in 2 variables
        List<String> lstPart    = tStartDateTimeValue.split(' ', 2);
        if (lstPart.size()>=1)  tStartDateValue = lstPart[0];
        if (lstPart.size()>=2)  tStartTimeValue = lstPart[1];
        lstPart                 = tEndDateTimeValue.split(' ', 2);
        if (lstPart.size()>=1)  tEndDateValue   = lstPart[0];
        if (lstPart.size()>=2)  tEndTimeValue   = lstPart[1];

    }
    //------------------------------------------------------------------------

    //------------------------------------------------------------------------
    // Redirect Logic
    //  Executed on load of the page by using the ACTION parameter 
    //------------------------------------------------------------------------
    public PageReference redirect(){

        // Close the Case (if is not yet closed)
        if (oCase.Status != 'Closed'){
            try{
                oCase.Status = 'Closed';
                update oCase;   
            }catch(Exception oEX){
            }
        }
		
		PageReference oPage = new PageReference('/00U/e');
		oPage.setRedirect(true);
		
		Map<String, String> params = oPage.getParameters();
		
        // Build the URL which will be used to redirect to a new pre-filled Task page
        params.put('who_id',clsUtil.isNull(oCase.Contactid, ''));
        params.put('what_id',clsUtil.isNull(oCase.Id, ''));
        params.put('retURL',clsUtil.isNull(oCase.Id, ''));
        params.put('StartDateTime',tStartDateValue);
        params.put('StartDateTime_time', tStartTimeValue);
        params.put('EndDateTime',tEndDateValue);
        params.put('EndDateTime_time',tEndTimeValue);
        params.put('IsReminderSet', '0');
        params.put('evt5', clsUtil.isNull(oCase.Subject, '').left(250));
        params.put('evt6',clsUtil.isNull(oCase.Description, '').left(1200));
        params.put('evt10','Implant Support');
        params.put('evt12',clsUtil.isNull(oCase.Account.Name, '').left(255));
        
        return oPage;        

    }
    //------------------------------------------------------------------------
}