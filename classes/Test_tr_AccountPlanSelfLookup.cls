/*  
    Test Class Name - Test_tr_AccountPlanSelfLookup 
    Description  –  The written class will cover AccountPlanSelfLookupTrigger.
                    
                    
    Author - Kaushal Singh    
    Created Date  - 29/11/2012 
*/

//seeAllData set to true to access Master Data
@isTest(seeAllData=true)
private class Test_tr_AccountPlanSelfLookup{
    static testMethod void testAccountPlan() {

        Company__c c = [select Id, Name, Company_Code_Text__c from Company__c where Name = 'Europe'];
        user u;
        User thisUser = [ select Id from User where Id = :userinfo.getuserid() ];    
        System.runAs ( thisUser ) {
        Profile p = [select id from profile where name='System Administrator'];    
        u = new User(alias = 'standt1', email='standarduser@testorg.com',             
                        emailencodingkey='UTF-8', lastname='TerritoryManagement_Testing',             
                        languagelocalekey='en_US',             
                        localesidkey='en_US', profileid = p.Id,            
                        timezonesidkey='America/Los_Angeles',             
                        CountryOR__c='Netherlands',          
                        Country= 'Netherlands',            
                        username='AccountPlanSelfLookup@testorg.com',
                        Alias_unique__c='standarduser_Alias_unique__c',Company_Code_Text__c='T45');  
                        insert u;    
        }
       	system.runas(u){ 
           
	        Business_Unit_Group__c BUG = [select Id, Name, Master_Data__c from Business_Unit_Group__c where Name = 'CVG' and Master_Data__c = :c.Id];
	        Business_Unit__c BU = [select Id from Business_Unit__c where Name = 'CRHF' and Business_Unit_Group__c = :BUG.Id];
	        List<Sub_Business_Units__c> ListOfSBU = [select Id from Sub_Business_Units__c where (Name = 'Implantables & Diagnostic' or Name = 'AF Solutions' or Name = 'AF Solutions (tmp)') and Business_Unit__c = :BU.Id];
	        
	        Account a = new Account(); 
	        a.Name = 'Test Acc Plan'; 
	        insert a;
	        
	        Test.startTest();
	        
	        Account_Plan_2__c ap = new Account_Plan_2__c();
	        ap.Account__c=a.id;
	        ap.Account_Plan_Level__c='Business Unit Group';
	        ap.Business_Unit_Group__c=BUG.id;
	        insert ap;
 
 	        Account_Plan_2__c ap2 = new Account_Plan_2__c();
	        ap2.Account__c=a.id;
	        ap2.Account_Plan_Level__c='Sub Business Unit';
	        ap2.Sub_Business_Unit__c = ListOfSBU[0].id;
	        ap2.Business_Unit__c = BU.Id;
	        ap2.Business_Unit_Group__c=BUG.id;
	        insert ap2;
	        
 	        Account_Plan_2__c ap3 = new Account_Plan_2__c();
	        ap3.Account__c=a.id;
	        ap3.Account_Plan_Level__c='Sub Business Unit';
	        ap3.Sub_Business_Unit__c = ListOfSBU[1].id;
	        ap3.Business_Unit__c = BU.Id;
	        ap3.Business_Unit_Group__c=BUG.id;
	        insert ap3;	        
	        
	        Test.stopTest();
        }  
    }   
}