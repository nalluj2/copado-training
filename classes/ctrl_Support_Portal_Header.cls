public class ctrl_Support_Portal_Header {

    // Constructor
    public ctrl_Support_Portal_Header() {
        bl_SupportRequest.loadSupportRequestParameter();
        lstSupportRequestParameter_CventDoc = new List<Support_Request_Parameter__c>();
        lstSupportRequestParameter_CventLink = new List<Support_Request_Parameter__c>();
        lstSupportRequestParameter_BIDoc = new List<Support_Request_Parameter__c>();
        lstSupportRequestParameter_BILink = new List<Support_Request_Parameter__c>();
        lstSupportRequestParameter_BOTDoc = new List<Support_Request_Parameter__c>();
        lstSupportRequestParameter_BOTLink = new List<Support_Request_Parameter__c>();

        if (bl_SupportRequest.mapSupportRequestParameterType_SupportRequestParameter.size()>0){
            if (bl_SupportRequest.mapSupportRequestParameterType_SupportRequestParameter.containsKey('BOT - Document')){
                lstSupportRequestParameter_BOTDoc = bl_SupportRequest.mapSupportRequestParameterType_SupportRequestParameter.get('BOT - Document');
            }
            if (bl_SupportRequest.mapSupportRequestParameterType_SupportRequestParameter.containsKey('BOT - Link')){
                lstSupportRequestParameter_BOTLink = bl_SupportRequest.mapSupportRequestParameterType_SupportRequestParameter.get('BOT - Link');
            }

            if (bl_SupportRequest.mapSupportRequestParameterType_SupportRequestParameter.containsKey('Cvent - Document')){
                lstSupportRequestParameter_CventDoc = bl_SupportRequest.mapSupportRequestParameterType_SupportRequestParameter.get('Cvent - Document');
            }
            if (bl_SupportRequest.mapSupportRequestParameterType_SupportRequestParameter.containsKey('Cvent - Link')){
                lstSupportRequestParameter_CventLink = bl_SupportRequest.mapSupportRequestParameterType_SupportRequestParameter.get('Cvent - Link');
            }

            if (bl_SupportRequest.mapSupportRequestParameterType_SupportRequestParameter.containsKey('BI - Document')){
                lstSupportRequestParameter_BIDoc = bl_SupportRequest.mapSupportRequestParameterType_SupportRequestParameter.get('BI - Document');
            }
            if (bl_SupportRequest.mapSupportRequestParameterType_SupportRequestParameter.containsKey('BI - Link')){
                lstSupportRequestParameter_BILink = bl_SupportRequest.mapSupportRequestParameterType_SupportRequestParameter.get('BI - Link');
            }
        }
    }

    // Getters & Setters
    public List<Support_Request_Parameter__c> lstSupportRequestParameter_CventDoc { get; set; }
    public List<Support_Request_Parameter__c> lstSupportRequestParameter_CventLink { get; set; }
    public List<Support_Request_Parameter__c> lstSupportRequestParameter_BIDoc { get; set; }
    public List<Support_Request_Parameter__c> lstSupportRequestParameter_BILink { get; set; }
    public List<Support_Request_Parameter__c> lstSupportRequestParameter_BOTDoc { get; set; }
    public List<Support_Request_Parameter__c> lstSupportRequestParameter_BOTLink { get; set; }

    public Boolean bHasCventLink { 
        get{
            Boolean bResult = false;
            if (lstSupportRequestParameter_CventLink.size() > 0){
                bResult = true;
            }
            return bResult;
        }
    }

    public Boolean bHasBILink { 
        get{
            Boolean bResult = false;
            if (lstSupportRequestParameter_BILink.size() > 0){
                bResult = true;
            }
            return bResult;
        }
    }

    public Boolean bHasBOTLink { 
        get{
            Boolean bResult = false;
            if (lstSupportRequestParameter_BOTLink.size() > 0){
                bResult = true;
            }
            return bResult;
        }
    }
    
}