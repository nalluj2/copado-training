@IsTest
public class TEST_ba_AccountsInScopeForDeletion {
    
    static testMethod void testMethod1(){
        List<Account> listAccount= new List<Account>();
        List<Contact> listContact = new List<Contact>();
        
        for (Integer i=0; i<1;i++){
			Account recAccount = new Account();
			recAccount.Name ='Name' + i;
			recAccount.In_Scope_for_Deletion__c = 'Yes';
            listAccount.add(recAccount);
        }

		insert listAccount;
        
        for (Account recAccount : listAccount){
            Contact recContact = new Contact();
        	recContact.LastName = 'Contact 1 ';
        	recContact.FirstName = 'UnitTest';
        	recContact.AccountId = recAccount.Id; 
        	recContact.Contact_Department__c = 'Diabetes Adult'; 
        	recContact.Contact_Primary_Specialty__c = 'ENT';
        	recContact.Affiliation_To_Account__c = 'Employee';
        	recContact.Primary_Job_Title_vs__c = 'Manager';
        	recContact.Contact_Gender__c = 'Male';       
        	recContact.MailingCountry = 'Netherlands';
            
            listContact.add(recContact);
        }
        
        insert listContact;
        
        Test.startTest();
		ba_AccountsInScopeForDeletion a = new ba_AccountsInScopeForDeletion();
	        ba_AccountsInScopeForDeletion.startBatch();
        Test.stopTest();
    }
}