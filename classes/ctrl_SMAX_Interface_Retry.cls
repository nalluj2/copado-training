public without sharing class ctrl_SMAX_Interface_Retry {
    
    public String objectType {get; set;}
    public String recordIds {get; set;}
    
    private List<Id> idList {get; set;}
    public boolean retryInProcess {get; set;}
    public List<RetryItem> retryStatusList {get; set;}
    
    public ctrl_SMAX_Interface_Retry(){
    	
    	retryInProcess = false;
    }
    
    public void retryRecords(){
    	    	    		
    	idList = new List<Id>();
    	
    	for(String id : recordIds.split('\n')){
    		
    		String cleanId = id.trim();
    		
    		if(cleanId != '' && cleanId != null) idList.add(cleanId);
    	}
    	
    	if(idList.size() > 80){
    		
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'A maximum of 80 records can be retried per request.'));
    		return;
    	} 	
    	
    	Map<Id, NotificationSAPLog__c> recordLastNotification = new Map<Id, NotificationSAPLog__c>();
		
		Set<Id> retryIds = new Set<Id>();
		List<NotificationSAPLog__c> toUpdate = new List<NotificationSAPLog__c>();
		
		for(Notification_Grouping__c grouping : [Select Last_Outbound_Notification__c, Last_Outbound_Notification__r.Name, Last_Outbound_Notification__r.Record_Id__c,Last_Outbound_Notification__r.Status__c, Last_Outbound_Notification__r.Retry_DateTime__c, Last_Outbound_Notification__r.WebServiceName__c from Notification_Grouping__c where Scope__c = :objectType AND Internal_Id__c IN :idList]){
	        	        
		    NotificationSAPLog__c notification = grouping.Last_Outbound_Notification__r;
		    	
		    recordLastNotification.put(notification.Record_Id__c, notification);
		    	
	    	if(notification.Status__c == 'COMPLETED'){
		    		
	    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Record with Id "' + notification.Record_Id__c + '" has been completed already.'));
		    		
	    	}else if(notification.Status__c == 'RETRY' && notification.Retry_DateTime__c != null && notification.Retry_DateTime__c > Datetime.now()){
		    		
	    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Record with Id "' + notification.Record_Id__c + '" is already in the retry queue.'));
		    		
		    }else{
		    		
		    	retryIds.add(notification.Record_Id__c);
		    		
		    	if(notification.Status__c != 'FAILED' && notification.Status__c != 'ERROR' && notification.Status__c != 'SKIPPED'){
		    			
		    		notification.Status__c = 'FAILED';
		    		toUpdate.add(notification);
		    	}
		    }
		}
		
		if(toUpdate.size() > 0) update toUpdate;
		
		if(retryIds.size() > 0){
					
			if(objectType == 'SVMXC__Service_Order__c'){
				    		
	    		List<SVMXC__Service_Order__c> orderUpdate = new List<SVMXC__Service_Order__c>();
	    		List<SVMXC__Service_Order__c> orderComplete = new List<SVMXC__Service_Order__c>();
	    		
	    		for(SVMXC__Service_Order__c sOrder : [SELECT Id, SVMX_SAP_Service_Order_No__c from SVMXC__Service_Order__c where Id = :retryIds]){
	    			
	    			NotificationSAPLog__c lastNotification = recordLastNotification.get(sOrder.Id);
	    				    			
	    			if(lastNotification.WebServiceName__c == 'ServiceOrder_NotificationSAP') orderUpdate.add(sOrder);
	    			else orderComplete.add(sOrder);
	    		}  		
	    		   		
	    		if(orderUpdate.size() > 0) bl_NotificationSAP.sendNotificationToSAP_ServiceOrder(orderUpdate, 'UPDATE');	    			    			
	    		if(orderComplete.size() > 0) bl_NotificationSAP.sendNotificationToSAP_ServiceOrder_Completed(orderComplete, 'UPDATE');	    		
	    		    		
	    	}else if(objectType == 'Case'){
	    		
	    		List<Case> sNotifications = [SELECT Id, SVMX_SAP_Notification_Number__c from Case where Id = :retryIds];
	    		
	    		bl_NotificationSAP.sendNotificationToSAP_Case(sNotifications, 'UPDATE');
	    		
	    	}else if(objectType == 'SVMXC__Installed_Product__c'){
	    		
	    		List<SVMXC__Installed_Product__c> instProducts = [SELECT Id, SVMX_SAP_Equipment_ID__c from SVMXC__Installed_Product__c where Id = :retryIds];
	    		
	    		bl_NotificationSAP.sendNotificationToSAP_InstalledProduct(instProducts, 'UPDATE');
	    		
	    	}else if(objectType == 'Installed_Product_Measures__c'){
	    		
	    		List<Installed_Product_Measures__c> prodMeasures = [SELECT Id, SAP_Measure_Point_ID__c from Installed_Product_Measures__c where Id = :retryIds];
	    		
	    		bl_NotificationSAP.sendNotificationToSAP_InstalledProductCounter(prodMeasures, 'UPDATE');
	    		
	    	}else if(objectType == 'Attachment'){
	    	
	    		List<Attachment> attachments = [SELECT Id from Attachment where Id = :retryIds];
	    		
	    		bl_NotificationSAP.sendNotificationToSAP_Attachment(attachments, 'UPDATE');    		
	    	}			
		}
		
		retryInProcess = true;
		
		refreshStatus();
    }
    
    public void refreshStatus(){
    	
    	String nameField = objectType == 'Case' ? 'CaseNumber' : 'Name';
    	    	    	
    	Map<Id, SObject> recordMap = new Map<Id, SObject>(Database.query('Select Id, ' + nameField + ' from ' + objectType + ' where Id IN :idList'));
    	
    	Map<Id, NotificationSAPLog__c> recordLastNotification = new Map<Id, NotificationSAPLog__c>();
    	
    	for(Notification_Grouping__c grouping : [Select Last_Outbound_Notification__c, Last_Outbound_Notification__r.Name, Last_Outbound_Notification__r.Status__c, Last_Outbound_Notification__r.Record_ID__c, Last_Outbound_Notification__r.Retry_DateTime__c, Last_Outbound_Notification__r.Outbound_Message_Request__c, Last_Outbound_Notification__r.Error_Description__c, Last_Outbound_Notification__r.LastModifiedDate from Notification_Grouping__c where Scope__c = :objectType AND Internal_ID__c IN :recordMap.keySet()]){
	        
	        NotificationSAPLog__c notification = grouping.Last_Outbound_Notification__r;		    	
		    recordLastNotification.put(notification.Record_Id__c, notification);
		}
		
		Boolean allDone = true;
		retryStatusList = new List<RetryItem>();
		
		for(Id recordId : idList){
			
			SObject record = recordMap.get(recordId);
			NotificationSAPLog__c lastNotification = recordLastNotification.get(recordId);
			
			RetryItem listItem = new RetryItem();
			listItem.id = recordId;
			
			if(record != null) listItem.name = (String) record.get(nameField);
			else listItem.name = 'Record not found for object ' + objectType + ' and Id ' + recordId;
			
			listItem.notification = lastNotification;
			
			if(lastNotification != null) listItem.message = parseResponse(lastNotification.Outbound_Message_Request__c, lastNotification.Error_Description__c);
			
			retryStatusList.add(listItem);
			
			if(lastNotification != null && lastNotification.Status__c == 'NEW') allDone = false;
		}
    	
    	if(allDone){
    		
    		retryInProcess = false;
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm, 'All records have been retried successfully.'));
    	}
    }
    
    private String parseResponse(String response, String errorDescription){
    	
    	String errorMessage = '';
    	
    	if(response != null){
    		
    		try{
    			
    			AckMessage message = (AckMessage) JSON.deserialize(response, AckMessage.class);
    			
    			if(message.DISTRIBUTION_STATUS == 'FALSE'){
    				
    				Set<String> errors = new Set<String>();
    				
    				for(SAPErrorMessage error : message.DIST_ERROR_DETAILS){
    					
    					if((error.ERROR_TYPE == 'E' || error.ERROR_TYPE == null) && error.ERROR_MESSAGE != 'Error during processing of BAPI methods' && error.ERROR_MESSAGE != 'Error  during processing of BAPI methods'){
    						
    						errors.add(error.ERROR_MESSAGE);
    					}
    				}
    				
    				errorMessage = String.join(new List<String>(errors), ';');
    			}
    		
    		}catch(Exception e){
    			
    			errorMessage = 'Exception occur while getting the error message';
    		}
    	}
    	
    	if(errorMessage == '' && errorDescription != null) errorMessage = errorDescription;
    	
    	return errorMessage;
    }
    
    public class RetryItem {
    	
    	public Id id {get; set;}
    	public String name {get; set;}
    	public NotificationSAPLog__c notification {get; set;}    
    	public String message {get; set;}	
    }
    
    public class AckMessage {
                   
        public String DISTRIBUTION_STATUS {get; set;}
        public List<SAPErrorMessage> DIST_ERROR_DETAILS {get; set;}        
    }
    
    public class SAPErrorMessage{
    	
    	public String ERROR_TYPE {get; set;}
		public String ERROR_MESSAGE {get; set;}
    }
}