global class sc_Run_All_Internal_Tests implements Schedulable{
	
    global void execute(SchedulableContext ctx){        
                        
        List<String> toAddresses = new List<String>();
                
        for(Unit_Test_Sandbox__c sandbox : Unit_Test_Sandbox__c.getall().values()){
        	
        	toAddresses.add(sandbox.Email_Address__c);        	
        }
         
        if(toAddresses.size() > 0){        
	        
	        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
	        email.setToAddresses(toAddresses);
	        email.setSubject('Run All Internal Unit Tests');
	        email.setPlainTextBody('Run All Internal Unit Tests');	        
	        
	        if(Test.isRunningTest() == false) Messaging.sendEmail(new Messaging.SingleEmailMessage[]{email});
        }
        
        //Run the Unit Tests locally
        bl_Run_All_Internal_Tests.runAllInternalTests();    
    }
}