@isTest
private class Test_trgFillManufacturer {

    static testMethod void myUnitTest() {

	     //Insert Company
	     Company__c cmpny = new Company__c();
	     cmpny.name='TestMedtronic';
	     cmpny.CurrencyIsoCode = 'EUR';
	     cmpny.Current_day_in_Q1__c=56;
	     cmpny.Current_day_in_Q2__c=34; 
	     cmpny.Current_day_in_Q3__c=5; 
	     cmpny.Current_day_in_Q4__c= 0;   
	     cmpny.Current_day_in_year__c =200;
	     cmpny.Days_in_Q1__c=56;  
	     cmpny.Days_in_Q2__c=34;
	     cmpny.Days_in_Q3__c=13;
	     cmpny.Days_in_Q4__c=22;
	     cmpny.Days_in_year__c =250;
	     cmpny.Company_Code_Text__c = 'T34';
	     insert cmpny;
     
		Business_Unit__c BU = new Business_Unit__c();
		Bu.name='BU1';
		Bu.Company__c = cmpny.Id;
		insert BU;

		Id rt = [Select Id From RecordType where SobjectType='Product2' and IsActive=true and Name=:'MDT Marketing Product' limit 1].id;
		 
		Product2 p = new Product2();
		p.RecordTypeId = rt ; //'012P00000008ZVv';
		p.Name = 'Product Name';
		p.Business_Unit_ID__c = BU.id;
		p.Consumable_Bool__c=true;
		insert p;		        
    }
}