/*
 *      Created Date : 17-02-2014
 *      Author : Bart Caelen
 *      Description : TEST Class for the APEX Class clsUtil
 */
@isTest private class Test_clsUtil {

    private static Boolean bHasException = false;
    private static List<Account> lstAccount;
    private static String tAccountName;
    private static List<Contact> lstContact;

    @isTest static void createTestData(){

        lstAccount = new List<Account>();
        for (integer i=0; i<51; i++){
            Account oAccount = new Account();
                oAccount.Name = 'ZTEST Account ' + i;
            lstAccount.add(oAccount);
        }
        insert lstAccount;
        tAccountName = lstAccount[0].Name;

        lstContact = new List<Contact>();
        for (Integer iAccount = 0; iAccount < 10; iAccount++){
            for (integer i=0; i < 5; i++){
                Contact oContact = new Contact();
                    oContact.AccountId = lstAccount[iAccount].Id;
                    oContact.LastName = 'TEST Contact ' + i;  
					oContact.FirstName = 'Test';
                    oContact.MailingCountry = 'BELGIUM';
                    oContact.Contact_Department__c = 'Diabetes Adult'; 
                    oContact.Contact_Primary_Specialty__c = 'ENT';
                    oContact.Affiliation_To_Account__c = 'Employee';
                    oContact.Primary_Job_Title_vs__c = 'Manager';
                    oContact.Contact_Gender__c = 'Male';                 
                lstContact.add(oContact);
            }
        }
        insert lstContact;
                
        lstContact = [SELECT ID, Lastname, Account.Name FROM Contact];

		clsTestData_MasterData.createBusinessUnit();
		clsTestData_MasterData.createCountry();

        clsTestData.createDIBFiscalPeriodData();

    }

    @isTest static void test_clsUtil() {
        
        //---------------------------------------
        // Set Exception
        //---------------------------------------
        bHasException = false;    
        test_clsUtil_All();
            
    }

    @isTest static void test_clsUtil_Exception() {
        
        //---------------------------------------
        // Set Exception
        //---------------------------------------
        try{
        	        	        	        	
            bHasException = true;
                        
            test_clsUtil_All();
            
        }catch(Exception e){
        }
            
    }

    private static void test_clsUtil_All() {
		
		User testUser = [Select Id from User where Profile.Name = 'IT Business Analyst' AND isActive = true AND ManagerId != null AND Manager.isActive = true AND Manager.Profile.Name = 'IT Business Analyst' LIMIT 1];
             
        System.runAs(testUser){ 
		
	        //---------------------------------------
	        // Create Test Data
	        //---------------------------------------
	        createTestData();
	        //---------------------------------------
	
	
	        //---------------------------------------
	        // Variable Declarations
	        //---------------------------------------
	        String tTest;
	        Date dTest;
	        DateTime dtTest;
	        Decimal decTest;
	        Double dblTest;
	        Integer iTest;
	        Boolean bTest = false;
	        Set<String> setFieldName = new Set<String>();
	        RecordType oRecordType;
	        Id id_Test;        
	        //---------------------------------------
	    
	        clsUtil.hasException = bHasException;        
	
	        //---------------------------------------
	        // IsBlank Functions
	        //---------------------------------------
	        try{ bTest = clsUtil.isBlank(tTest); }catch(Exception oEX){}
	        try{ bTest = clsUtil.isBlank(tTest); }catch(Exception oEX){}
	
	        try{ bTest = clsUtil.isBlank(id_Test); }catch(Exception oEX){}
//	        try{ id_Test = clsUtil.getUserProfileId('System Administrator'); }catch(Exception oEX){}
	        try{ bTest = clsUtil.isBlank(id_Test); }catch(Exception oEX){}

	        try{ bTest = clsUtil.isBlank(dTest); }catch(Exception oEX){}
	        try{ bTest = clsUtil.isBlank(dtTest); }catch(Exception oEX){}
	        try{ bTest = clsUtil.isBlank(oRecordType); }catch(Exception oEX){}
	        try{ bTest = clsUtil.isBlank(dblTest); }catch(Exception oEX){}
	        try{ bTest = clsUtil.isBlank(decTest); }catch(Exception oEX){}
	        //---------------------------------------
	
	
	        //---------------------------------------
	        // IsNull Functions
	        //---------------------------------------
	        tTest = '';
	        try{ clsUtil.isNull(tTest, '.'); }catch(Exception oEX){}
	        tTest = 'TEST';
	        try{ clsUtil.isNull(tTest, '.'); }catch(Exception oEX){}
	
	        try{ clsUtil.isNull(dTest, '.'); }catch(Exception oEX){}
	        dTest = Date.today();
	        try{ clsUtil.isNull(dTest, '.'); }catch(Exception oEX){}
	
	        try{ clsUtil.isNull(decTest, '.'); }catch(Exception oEX){}
	        decTest = 1;
	        try{ clsUtil.isNull(decTest, '.'); }catch(Exception oEX){}
	
	        try{ clsUtil.isNull(iTest, '.'); }catch(Exception oEX){}
	        iTest = 1;
	        try{ clsUtil.isNull(iTest, '.'); }catch(Exception oEX){}
	
	        try{ clsUtil.isDecimalNull(decTest, 10); }catch(Exception oEX){}
	        decTest = null;
	        try{ clsUtil.isDecimalNull(decTest, 10); }catch(Exception oEX){}
	
	        try{ clsUtil.isBooleanNull(bTest, true); }catch(Exception oEX){}
	        bTest = null;
	        try{ clsUtil.isBooleanNull(bTest, false); }catch(Exception oEX){}
	
	        try{ dTest = clsUtil.isDateNull(dTest, Date.today()); }catch(Exception oEX){}
	        dTest = Date.today();
	        try{ dTest = clsUtil.isDateNull(dTest, null); }catch(Exception oEX){}
	        try{ dTest = clsUtil.isDateNull(null, Date.today()); }catch(Exception oEX){}
	
	        try{ iTest = clsUtil.isIntegerNull(null, 1); }catch(Exception oEX){}
	        iTest = 1;
	        try{ iTest = clsUtil.isIntegerNull(iTest, 2); }catch(Exception oEX){}
	
	        Object oObj;
	        try{ tTest = clsUtil.isNull(oObj, 'TEST'); }catch(Exception oEX){}
	        oObj = new Account();
	        try{ tTest = clsUtil.isNull(oObj, 'TEST'); }catch(Exception oEX){}
	        //---------------------------------------
	
	
	        //---------------------------------------
	        // Sorting List sObject
	        //---------------------------------------
	        try{ clsUtil.sortList(lstAccount, 'Name', 'ASC'); }catch(Exception oEX){}
	        try{ clsUtil.sortList(lstAccount, 'Name', 'DESC'); }catch(Exception oEX){}
	        //---------------------------------------
	
	
	        //---------------------------------------
	        // Salesforce.com ID Converter
	        //---------------------------------------
	        String tSFDCID = '';
	        try{ tSFDCID = clsUtil.convertSFDCID('azerty'); }catch(Exception oEX){}
	        if (!clsUtil.hasException) System.assertEquals('azerty', tSFDCID);
	        try{ tSFDCID = clsUtil.convertSFDCID('0013000000K7WW2'); }catch(Exception oEX){}
	        if (!clsUtil.hasException) System.assertEquals('0013000000K7WW2AAN', tSFDCID);

			Schema.sObjectType oSObjectType_Contact = clsUtil.getSObjectType('Contact');
			Boolean bIsValidId = false;
			try{ bIsValidId = clsUtil.bIsValidId('azerty', oSObjectType_Contact); }catch(Exception oEX){}
			if (!clsUtil.hasException) System.assertEquals(bIsValidId, false);

			bIsValidId= false;
			try{ bIsValidId = clsUtil.bIsValidId('0033000000K7WW2', oSObjectType_Contact); }catch(Exception oEX){}
			if (!clsUtil.hasException) System.assertEquals(bIsValidId, true);
	        //---------------------------------------
	 
	        //---------------------------------------
	        // Get Selectoptions from Picklist Value
	        //---------------------------------------
	        try{ clsUtil.getPicklistValues('Account', 'type'); }catch(Exception oEX){}
	        //---------------------------------------
	
	        //---------------------------------------
			// Get Field Dependencies
	        //---------------------------------------
			List<String> lstValue = new List<String>();
			clsUtil.getFieldDependencies('User', 'User_Business_Unit_vs__c', 'Job_Title_vs__c');

			lstValue = new List<String>();
			lstValue = clsUtil.getDependentValues('User', 'User_Business_Unit_vs__c', 'Job_Title_vs__c', 'All'); 
			System.assert(lstValue.size() > 0);

			lstValue = new List<String>();
			lstValue = clsUtil.getUserJobTitle('All');
			System.assert(lstValue.size() > 0);

			lstValue = new List<String>();
			lstValue = clsUtil.getUserCountry('NWE');
			System.assert(lstValue.size() > 0);
	        //---------------------------------------

	        //---------------------------------------
	        // Debugging
	        //---------------------------------------
	        try{ clsUtil.debug('DEBUG TEST STATEMENT'); }catch(Exception oEX){}
	        //---------------------------------------
	
	        
	        //---------------------------------------
	        // Get Picklist Values From Records
	        //---------------------------------------
	        try{ clsUtil.getPicklistValuesFromRecords('Account', 'Id', 'Name'); }catch(Exception oEX){}
	        try{ clsUtil.getPicklistValuesFromRecords(lstAccount, 'Account', 'Name'); }catch(Exception oEX){}
	        try{ clsUtil.getPicklistValuesFromRecords('Account', 'Id', 'Name', 'Name', tAccountName); }catch(Exception oEX){}
	        try{ clsUtil.getPicklistValuesFromRecords('Contact', 'Account.Id', 'Account.Lastname', 'STRING', 'Account.Id', lstAccount[0].Id, null, lstContact); }catch(Exception oEX){}
	        try{ clsUtil.getPicklistValuesFromRecords('Contact', 'Account.Id', 'Account.Lastname', 'Account.isDeleted', false); }catch(Exception oEX){}
	        //---------------------------------------
	
	
	        //---------------------------------------
	        // Sort list of SelectOptions
	        //---------------------------------------
	        List<SelectOption> lstSO = new List<SelectOption>();
	            lstSO.Add(new SelectOption('1', 'D'));
	            lstSO.Add(new SelectOption('4', 'C'));
	            lstSO.Add(new SelectOption('2', 'B'));
	            lstSO.Add(new SelectOption('3', 'A'));
	
	        try{ lstSO = clsUtil.sortListSelectOptionByLabel(lstSO, 'ASC'); }catch(Exception oEX){}
	        try{ lstSO = clsUtil.sortListSelectOptionByLabel(lstSO, 'DESC'); }catch(Exception oEX){}
	        try{ lstSO = clsUtil.sortListSelectOptionByValue(lstSO, 'ASC'); }catch(Exception oEX){}
	        try{ lstSO = clsUtil.sortListSelectOptionByValue(lstSO, 'DESC'); }catch(Exception oEX){}
	        //---------------------------------------
	        
	        
	        //---------------------------------------
	        // TEST Describe
	        //---------------------------------------
	        try{ clsUtil.mapFieldNameDescribe('Account'); }catch(Exception oEX){}
	        setFieldName = new Set<String>();
	            setFieldName.add('Id');
	            setFieldName.add('Name');
	        try{ clsUtil.mapFieldNameDescribe('Account', setFieldName); }catch(Exception oEX){}
	        //---------------------------------------
	
	        
	        //---------------------------------------
	        // TEST VF Error
	        //---------------------------------------
	        try{ clsUtil.createVFError('TEST ERROR DESCRIPTION'); }catch(Exception oEX){}
	        try{
	                lstAccount[0].put('FieldThatDoesNotExists','test');
	        }catch(Exception oERR){
	            try{ clsUtil.createVFError(oERR, 'TEST'); }catch(Exception oEX){}
	            try{ clsUtil.createVFError(oERR, 'TEST', 'DESCRIPTION'); }catch(Exception oEX){}
	        }
	        //---------------------------------------
	
	        
	        //---------------------------------------
	        // TEST Describe Functions
	        //---------------------------------------
	        try{ tTest = clsUtil.getSObjectNameFromIDPrefix('001'); }catch(Exception oEX){}
	        try{ tTest = clsUtil.getPrefixFromSObjectName('Account'); }catch(Exception oEX){}
	        try{ Map<String, Schema.SObjectField> mapField = clsUtil.getFieldsForSobject('Account'); }catch(Exception oEX){}
			try{ Map<String, Schema.DescribeFieldResult> mapFieldDescribe = clsUtil.getFieldDescribeForSobject('Account'); }catch(Exception oEX){}
	        try{ setFieldName = clsUtil.getUpdateableFields('Account'); }catch(Exception oEX){}
	        try{ tTest = clsUtil.getSObjectLabel('Contact'); }catch(Exception oEX){}
	        try{ setFieldName = clsUtil.getAccessibleFields('Account'); }catch(Exception oEX){}
	        try{ tTest = clsUtil.tGetFieldLabel('Account', 'Name'); }catch(Exception oEX){}
	        //---------------------------------------
	
	
	        //---------------------------------------
	        // SOQL Function
	        //---------------------------------------
	        try{ tTest = clsUtil.tGetSOQLForSObject('Account'); }catch(Exception oEX){}
	        //---------------------------------------
	
	
	        //---------------------------------------
	        // TEST Business Unit Functions
	        //---------------------------------------
	        try{ clsUtil.getBusinessUnit('Europe', 'Neurovascular'); }catch(Exception oEX){}
	        try{ clsUtil.getBusinessUnitName('Europe', 'Restorative'); }catch(Exception oEX){}
			//---------------------------------------
	
	
	        //---------------------------------------
	        // TEST Date Formatting Functions
	        //---------------------------------------
	        try{ tTest = clsUtil.tFormatDate_DDMMYYYY(Date.today(), '-'); }catch(Exception oEX){}
	        try{ tTest = clsUtil.tFormatDate_YYYYMMDD(Date.today(), '-'); }catch(Exception oEX){}
	        try{ tTest = clsUtil.tFormatDate(Date.today(), 'DMY', '-'); }catch(Exception oEX){}
	        try{ tTest = clsUtil.tFormatDate(Date.today(), 'MDY', '-'); }catch(Exception oEX){}
	        try{ tTest = clsUtil.tFormatDate(Date.today(), 'YMD', '-'); }catch(Exception oEX){}
	        try{ tTest = clsUtil.tFormatDate(Date.today(), 'YYY', '-'); }catch(Exception oEX){}
	        //---------------------------------------
	
	
	        //---------------------------------------
	        // TEST GUID Functions
	        //---------------------------------------
	        try{ tTest = clsUtil.getNewGUID(); }catch(Exception oEX){}
	        //---------------------------------------
	
	
	        //---------------------------------------
	        // TEST String Functions
	        //---------------------------------------
	        try{ tTest = clsUtil.getCharAtIndex('AZERTY', 3); }catch(Exception oEX){}
	        //---------------------------------------
	
			Test.startTest();
	        //---------------------------------------
	        // TEST countRelatedRecords Function
	        //---------------------------------------
	        Set<String> setAccountID = new Set<String>();
	            for (Account oAccount : lstAccount){
	                setAccountID.add(oAccount.Id);
	            }
	        Set<String> setChildObjectName = new Set<String>();
	            setChildObjectName.add('Task');
	            setChildObjectName.add('Event');
	        Map<String, Integer> mapResult;
	        try{ mapResult = clsUtil.countRelatedRecords('Account', 'Id', setAccountID, setChildObjectName, 'WhatId', ''); }catch(Exception oEX){}
	
	        setChildObjectName = new Set<String>();
	            setChildObjectName.add('Contact');
	        try{ mapResult = clsUtil.countRelatedRecords('Account', 'Id', setAccountID, setChildObjectName, 'AccountId', ''); }catch(Exception oEX){}
	        //---------------------------------------
	
	
	        //---------------------------------------
	        // TEST updateParentWithRecordCountOfChild Function
	        //---------------------------------------
	        try{ clsUtil.updateParentWithRecordCountOfChild('Account', 'Id', 'NumberOfEmployees', setAccountID, setChildObjectName, 'AccountId', true); }catch(Exception oEX){}
	        //---------------------------------------
	
	
	        //---------------------------------------
	        // TEST convertCurrency Function
	        //---------------------------------------
	        try{ decTest = clsUtil.convertCurrency('USD', 'EUR', 100); }catch(Exception oEX){}
	        //---------------------------------------
	
	
	        //---------------------------------------
	        // TEST getRecordTypeByDevName Function
	        //---------------------------------------
	        try{ oRecordType = clsUtil.getRecordTypeByDevName('Account', 'MDT_Account'); }catch(Exception oEX){}
	        try{ oRecordType = clsUtil.getRecordTypeById(oRecordType.Id); }catch(Exception oEX){}
	        //---------------------------------------
	
	
	        //---------------------------------------
	        // TEST getTimeStamp
	        //---------------------------------------
	        try{ tTest = clsUtil.getTimeStamp(); }catch(Exception oEX){}
	        //---------------------------------------
	
	
	        //---------------------------------------
	        // TEST getParentTerritory
	        //---------------------------------------
	        Map<Id, Territory2> mapTerritory = new Map<Id, Territory2>(
	            [
	                SELECT 
	                    Id, Name
	                    , ParentTerritory2Id
	                    , Business_Unit__c, Company__c
	                FROM 
	                    Territory2
	            ]
	        );
	        Id idTerritory_Base;
	        for (Territory2 oTerritory : mapTerritory.values()){
	            if (oTerritory.ParentTerritory2Id != null){
	                idTerritory_Base = oTerritory.Id;
	                break;
	            }
	        }
	        if (idTerritory_Base != null){
	            try{ clsUtil.getParentTerritory(mapTerritory, idTerritory_Base, 1); }catch(Exception oEX){}
	        }
	        //---------------------------------------
	
	
	        //---------------------------------------
	        // TEST splitDataList
	        //---------------------------------------
	        List<String> lstDataList = new List<String>();
	        for (Integer iCounter = 0 ; iCounter < 1000 ; iCounter++){
	            String tData = 'DATA ' + String.valueOf(iCounter);
	            lstDataList.add(tData);
	        }
	        Map<Integer, List<String>> mapData = new Map<Integer, List<String>>();
	        try{ mapData = clsUtil.splitDataList(lstDataList, 200); }catch(Exception oEX){}
	        if (!clsUtil.hasException){
	            System.assertEquals(mapData.size(), 5);
	            for (Integer iCounter : mapData.keySet()){
	                System.assertEquals(mapData.get(iCounter).size(), 200);
	            }
	        }
	        //---------------------------------------
	
	        try{ sObject oSObject = clsUtil.createSObject('Account'); }catch(Exception oEX){}
	
	        tTest = null;
	        try{ bTest = clsUtil.checkNullOrBlank(tTest); }catch(Exception oEX){}
	        tTest = '';
	        try{ bTest = clsUtil.checkNullOrBlank(tTest); }catch(Exception oEX){}
	        tTest = 'test';
	        try{ bTest = clsUtil.checkNullOrBlank(tTest); }catch(Exception oEX){}
	
	        try{ bTest = clsUtil.bRunningInASandbox(); }catch(Exception oEX){}
	
	
	        //---------------------------------------
	        // TEST User related functions
	        //---------------------------------------
	        try{ id_Test = clsUtil.getUserProfileId('System Administrator'); }catch(Exception oEX){}
	        try{ id_Test = clsUtil.getUserRoleId('System Administrator'); }catch(Exception oEX){}
	        //---------------------------------------
	
	
	        //---------------------------------------
	        // TEST Email related functions 
	        //---------------------------------------
	        List<String> lstTO = new List<String>{'test.to@medtronic.com'};
	        List<String> lstCC = new List<String>{'test.cc@medtronic.com'};
	        List<String> lstBCC = new List<String>{'test.bcc@medtronic.com'};
	        try{ bTest = clsUtil.bSendEmail(lstTO, 'Subject', 'Body Plain', 'Body HTML'); }catch(Exception oEX){}
	        try{ bTest = clsUtil.bSendEmail('test.reply@medtronic.com', 'Sender Display Name', lstTO, lstCC, lstBCC, 'Subject', 'Body Plain', 'Body HTML'); }catch(Exception oEX){}
			try{ bTest = clsUtil.bEmailDeliverabilityEnabled(); }catch(Exception oEX){}
	        //---------------------------------------
	 
	
	        //---------------------------------------
	        // TEST Country related functions
	        //---------------------------------------
	        try{ clsUtil.getRegionByCountryName('Belgium'); }catch(Exception oEX){}
	        try{ clsUtil.getRegionByCountryISOCode('BE'); }catch(Exception oEX){}
	        try{ clsUtil.getCountryByISOCode('BE'); }catch(Exception oEX){}
	        try{ clsUtil.getCountryISOCodeByName('Belgium'); }catch(Exception oEX){}
			try{ clsUtil.getCountryISOCode3ByIsoCode('BE'); }catch(Exception oEX){}
	        //---------------------------------------
	
	
	        //---------------------------------------
	        // TEST Pricebook related functions
	        //---------------------------------------
	        Map<Id, Pricebook2> mapPricebook = clsUtil.getPricebookData();
	        Map<String, Id> mapPricebookName_PricebookId = clsUtil.getMapPricebookName_PricebookId();
	        //---------------------------------------
	
	
	        //---------------------------------------
	        // Process Approval Process
	        //---------------------------------------
	        List<Leave_Application__c> lstLeaveApplication = new List<Leave_Application__c>();
	        Leave_Application__c oLeaveApplication1 = new Leave_Application__c();
	            oLeaveApplication1.Employee__c = UserInfo.getUserId();
	            oLeaveApplication1.Leave_Type__c = 'Holiday';
	            oLeaveApplication1.Status__c = 'New';
	            oLeaveApplication1.Start_Date__c = Date.today().addDays(30);
	            oLeaveApplication1.End_Date__c = Date.today().addDays(35);
	            oLeaveApplication1.Description__c = 'DESCRIPTION 1';
	        lstLeaveApplication.add(oLeaveApplication1);
	        Leave_Application__c oLeaveApplication2 = new Leave_Application__c();
	            oLeaveApplication2.Employee__c = UserInfo.getUserId();
	            oLeaveApplication2.Leave_Type__c = 'Holiday';
	            oLeaveApplication2.Status__c = 'New';
	            oLeaveApplication2.Start_Date__c = Date.today().addDays(60);
	            oLeaveApplication2.End_Date__c = Date.today().addDays(65);
	            oLeaveApplication2.Description__c = 'DESCRIPTION 2';
	        lstLeaveApplication.add(oLeaveApplication2);
	        insert lstLeaveApplication;
	
	        // Create an approval request for the Leave Application - APPROVE
	        Approval.ProcessSubmitRequest oApprovalRequest_Approve = new Approval.ProcessSubmitRequest();
	            oApprovalRequest_Approve.setComments('Submitting request for approval - APPROVE.');
	            oApprovalRequest_Approve.setObjectId(lstLeaveApplication[0].Id);
	            // Submit on behalf of a specific submitter
	            oApprovalRequest_Approve.setSubmitterId(UserInfo.getUserId());
	
	            // Submit the record to specific process and skip the criteria evaluation
	            oApprovalRequest_Approve.setProcessDefinitionNameOrId('Leave_Application');
	            oApprovalRequest_Approve.setSkipEntryCriteria(true);
	
	        // Submit the approval request for the account
	        Approval.ProcessResult oApprovalResult_Approve = Approval.process(oApprovalRequest_Approve);
	
	        // Verify the result
	        System.assert(oApprovalResult_Approve.isSuccess());
	
	        // Create an approval request for the Opportunity - REJECT
	        Approval.ProcessSubmitRequest oApprovalRequest_Reject = new Approval.ProcessSubmitRequest();
	            oApprovalRequest_Reject.setComments('Submitting request for approval - REJECT.');
	            oApprovalRequest_Reject.setObjectId(lstLeaveApplication[1].Id);
	            // Submit on behalf of a specific submitter
	            oApprovalRequest_Reject.setSubmitterId(UserInfo.getUserId());
	
	            // Submit the record to specific process and skip the criteria evaluation
	            oApprovalRequest_Reject.setProcessDefinitionNameOrId('Leave_Application');
	            oApprovalRequest_Reject.setSkipEntryCriteria(true);
	
	        // Submit the approval request for the account
	        Approval.ProcessResult oApprovalResult_Reject = Approval.process(oApprovalRequest_Reject);
	
	        // Verify the result
	        System.assert(oApprovalResult_Reject.isSuccess());
	
	        Set<Id> setID_Approve = new Set<Id>();
	        setID_Approve.add(oLeaveApplication1.Id);
	
	        Set<Id> setID_Reject = new Set<Id>();
	        setID_Reject.add(oLeaveApplication2.Id);
	
	        clsUtil.processApproval(setID_Approve, 'APPROVE');
	        clsUtil.processApproval(setID_Reject, 'REJECT');
	        //---------------------------------------


	        //---------------------------------------
	        // Logic related to DIB_Fiscal_Year__c
	        //---------------------------------------
	        Set<Date> setDate = new Set<Date>();
	        Date dDate1 = Date.today().addDays(0);
	        Date dDate2 = Date.today().addDays(60);
	        Date dDate3 = Date.today().addDays(250);
	        setDate.add(dDate1);
	        setDate.add(dDate2);
	        setDate.add(dDate3);
	        Map<Date, String> mapDate_FiscalPerdiodLabel = clsUtil.getFiscalPeriodLabel(setDate);
	        //---------------------------------------
	
	
	        //---------------------------------------
	        // Exception Handling Functions
	        //---------------------------------------
	        Boolean bException = false;
	        try{
	        	clsUtil.hasException = false;
	            clsUtil.bubbleException();
	        	clsUtil.hasException = true;
	            clsUtil.bubbleException();
	        }catch(Exception oEX){
	        	bException = true;
	        }
	        System.assert(bException);
        	bException = false;
	        try{
	            clsUtil.hasException1 = false;
	            clsUtil.bubbleException1();
	            clsUtil.hasException1 = true;
	            clsUtil.bubbleException1();
	        }catch(Exception oEX){
	        	bException = true;
	        }
	        System.assert(bException);
        	bException = false;
	        try{
	            clsUtil.hasException2 = false;
	            clsUtil.bubbleException2();
	            clsUtil.hasException2 = true;
	            clsUtil.bubbleException2();
	        }catch(Exception oEX){
	        	bException = true;
	        }
	        System.assert(bException);
        	bException = false;
	        try{
	            clsUtil.hasException3 = false;
	            clsUtil.bubbleException3();
	            clsUtil.hasException3 = true;
	            clsUtil.bubbleException3();
	        }catch(Exception oEX){
	        	bException = true;
	        }
	        System.assert(bException);
        	bException = false;
	        try{
	            clsUtil.hasException4 = false;
	            clsUtil.bubbleException4();
	            clsUtil.hasException4 = true;
	            clsUtil.bubbleException4();
	        }catch(Exception oEX){
	        	bException = true;
	        }
	        System.assert(bException);
        	bException = false;
	        try{
	            clsUtil.hasException5 = false;
	            clsUtil.bubbleException5();
	            clsUtil.hasException5 = true;
	            clsUtil.bubbleException5();
	        }catch(Exception oEX){
	        	bException = true;
	        }
	        System.assert(bException);
        	bException = false;
	        try{
	        	clsUtil.tExceptionName = 'TEST_EXCEPTION_1';
	        	clsUtil.bubbleException('TEST_EXCEPTION_1');
	        }catch(Exception oEX){
	        	bException = true;
	        }
	        System.assert(bException);
	    	bException = false;

	        clsUtil.clearAllHasExceptions();
	        //---------------------------------------
        } 
    }

    @isTest static void test_getTerritoryUserForAccount(){

		//-----------------------------------------------------------------------------------------------------------
		// Create Test Data
		//-----------------------------------------------------------------------------------------------------------
		// Create Test Data - Account
		clsTestData_Account.tCountry_NonSAPPersonAccount = 'GERMANY';
		clsTestData_Account.createNonSAPPersonAccount();

		// Create Test Data - Account Share + OpportunityTeamMember
		Map<Id, Set<Id>> mapTerritoryID_UserIDs = new Map<Id, Set<Id>>();
		Set<Id> setID_User = new Set<Id>();

		List<UserTerritory2Association> lstUserTerritory = 
			[
				SELECT UserId, Territory2Id 
				FROM UserTerritory2Association 
				WHERE 
					isActive = true 
					AND Territory2.Territory2Type.DeveloperName = 'Territory' 
					AND Territory2.Country_UID__c = 'DE' 
					
			];
					
		for (UserTerritory2Association oUserTerritory : lstUserTerritory){
				
			Set<Id> setID_TerritoryUser = new Set<Id>();
			if (mapTerritoryID_UserIDs.containsKey(oUserTerritory.Territory2Id)){
				setID_TerritoryUser = mapTerritoryID_UserIDs.get(oUserTerritory.Territory2Id);
			}
			setID_TerritoryUser.add(oUserTerritory.UserId);
			mapTerritoryID_UserIDs.put(oUserTerritory.Territory2Id, setID_TerritoryUser);
			setID_User.add(oUserTerritory.UserId);

		}
					
		List<ObjectTerritory2Association> lstAccountShare = new List<ObjectTerritory2Association>();
		for (Id id_Territory : mapTerritoryID_UserIDs.keySet()){
				
			ObjectTerritory2Association oAccountShare = new ObjectTerritory2Association();
				oAccountShare.ObjectId = clsTestData_Account.oMain_NonSAPPersonAccount.Id;
				oAccountShare.Territory2Id = id_Territory;
				oAccountShare.AssociationCause ='Territory2Manual';
			lstAccountShare.add(oAccountShare);

		}
		insert lstAccountShare;
		System.assert(lstAccountShare.size() > 0);
		//-----------------------------------------------------------------------------------------------------------


		//-----------------------------------------------------------------------------------------------------------
		// Execute Test
		//-----------------------------------------------------------------------------------------------------------
		Test.startTest();

		Map<Id, Set<Id>> mapAccountID_UserIDs = clsUtil.getTerritoryUserForAccount(new Set<Id>{clsTestData_Account.oMain_NonSAPPersonAccount.Id});

		Test.stopTest();
		//-----------------------------------------------------------------------------------------------------------


		//-----------------------------------------------------------------------------------------------------------
        // Validate Test Result
		//-----------------------------------------------------------------------------------------------------------
        List<User> lstUser = [SELECT Id FROM User WHERE Id = :setID_User AND IsActive = true];
        System.assert(mapAccountID_UserIDs.get(clsTestData_Account.oMain_NonSAPPersonAccount.Id).size() > 0);
		System.assertEquals(mapAccountID_UserIDs.get(clsTestData_Account.oMain_NonSAPPersonAccount.Id).size(), lstUser.size());
		//-----------------------------------------------------------------------------------------------------------

    }


    @isTest static void test_getDIENCode(){

		//-----------------------------------------------------------------------------------------------------------
		// Create Test Data
		//-----------------------------------------------------------------------------------------------------------
		// Create Test Data - Mapping_Asset_ServiceLevel_DIENCode__c
		Mapping_Asset_ServiceLevel_DIENCode__c oMappingDIENCode = new Mapping_Asset_ServiceLevel_DIENCode__c();
			oMappingDIENCode.CFN_Code__c = 'ABCDEF';
			oMappingDIENCode.Service_Level__c = '123456';
			oMappingDIENCode.DIEN_Code__c = 'DIENCODE';
		insert oMappingDIENCode;
		//-----------------------------------------------------------------------------------------------------------


		//-----------------------------------------------------------------------------------------------------------
		// Execute Test
		//-----------------------------------------------------------------------------------------------------------
		Test.startTest();

			String tDIENCode = clsUtil.getDIENCode('', '');
			System.assert(String.isBlank(tDIENCode));

			tDIENCode = clsUtil.getDIENCode('ABCDEF', '');
			System.assert(String.isBlank(tDIENCode));

			tDIENCode = clsUtil.getDIENCode('', '123456');
			System.assert(String.isBlank(tDIENCode));

			tDIENCode = clsUtil.getDIENCode('ABCDEF', '123456');
			System.assert(tDIENCode == 'DIENCODE');

			tDIENCode = clsUtil.getDIENCode('ABCDEFG', '123456');
			System.assert(String.isBlank(tDIENCode));

		Test.stopTest();
		//-----------------------------------------------------------------------------------------------------------

	}

}