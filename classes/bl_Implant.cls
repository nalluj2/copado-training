/*
 *      Description : This class centralizes business logic related to Implants (Implant__c)
 *		Version	= 1.0
 *      Author 	= Bart Caelen
 *		Date	= 06/01/2014
*/
public with sharing class bl_Implant {
	
	// This set contains the RecordType Developer Names that will use th AFS/CDMR logic
	public static final set<string> SET_AFSRECORDTYPE = new set<string>{'AFS_Cryo_AF','AFS_Focal_Cryo','AFS_Phased_RF'};
	// This map contains the link between the AFS RecordType Developer Name and the corresponding default Therapy
	public static final map<string, string> MAP_AFSRECORDTYPE_THERAPY = new map<string, string>{'AFS_Cryo_AF'=>'Cryo AF','AFS_Focal_Cryo'=>'Focal Cryo','AFS_Phased_RF'=>'Phased RF'};
	// This map contains the link between the Therapy Name and his ID and will be loaded when the loadTherapyData is executed
	private static map<string, string> mapTherapyName_Id = new map<string, string>(); 
    // This set contains the RTG Business Units 
    public static final Set<String> setBU_RTG = new Set<String>{'Neurovascular','Cranial Spinal','Pain','Pelvic Health','RTG IHS','ENT'};
	
	// This method returns TRUE if the AFS/CDMR logic is applied
	public static boolean bIsAFS(string tRecordTypeId){
		boolean bResult = false;
        string tID = convertSFDCID(tRecordTypeID);
    	for (string tRecordTypeName : SET_AFSRECORDTYPE){
        	if (tID == RecordTypeMedtronic.getRecordTypeByDevName('Implant__c', tRecordTypeName).Id){
        		bResult = true;
        		break;
        	}
    	}
    	return bResult;
	}
	
	// This method returns the Therapy ID of the Therapy record which is defined as the default for the provided Recordtype
	public static string tTherapyForRecordTypeId(string tRecordTypeId){
        string tID = convertSFDCID(tRecordTypeID);
		string tRecordTypeName = RecordTypeMedtronic.getRecordTypeById(tID).DeveloperName;
		string tTherapyName = MAP_AFSRECORDTYPE_THERAPY.get(tRecordTypeName);
		string tTherapyID = mapTherapyName_Id.get(tTherapyName);
    	return tTherapyID;
	}
	
	// This method will try to convert the provided string into an 18 character long ID and returns the 18 character ID as a string
    public static string convertSFDCID(string tID){
        string tReturn  = tID;
        try{
            ID idSFDC   = tID;
            // If we get here, it's a valid ID and the idSFDC field is now an 18 character one
            tReturn     = idSFDC;
        } catch (System.StringException e){
            // Passed parameter is not a valid Salesforce.com ID
            tReturn     = tID;
        }
        return tReturn;
    }
    
	// This method loads the Therapy Data for the provided Business Unit and populates the map mapTherapyName_Id for further usage and returns the selected Therapies as a list of SelectOptions
  	public static list<SelectOption> loadTherapyData(id idBusinessUnit){

		//- Load the Therapy Select Options
        List<SelectOption> lstSelectOption = SharedMethods.getTherapies(idBusinessUnit);

        //- Add the collected Therapies into a map for easier searching
        //- This can not be done in an easy way in the SharedMethods.getTherapies(BU) method where the data is SOQL-ed, 
        // because the SharedMethods class is scheduled (or a method is used in a scheduled class) and therefor it can not be updated unless we un-schedule this class or the classes that use methods from it
        mapTherapyName_Id = new map<string, string>();
        for (SelectOption oOption : lstSelectOption){
        	mapTherapyName_Id.put(oOption.getLabel(), oOption.getValue());
        }
        
        return lstSelectOption;
 	}

  	public static list<SelectOption> loadProductGroupData(id idTherapy) {

		system.debug('### idTherapy=' + idTherapy);
		
		//- Load the Therapy Select Options
        List<SelectOption> lstSelectOption = SharedMethods.getProductGroups(idTherapy);

        mapTherapyName_Id = new map<string, string>();
        for (SelectOption oOption : lstSelectOption){
        	mapTherapyName_Id.put(oOption.getLabel(), oOption.getValue());
        }
        
        return lstSelectOption;
 	}
 
	
}