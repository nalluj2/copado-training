public with sharing class ctrlExt_Support_Request_Dispatcher {
	
	Create_User_Request__c request;
	
	public ctrlExt_Support_Request_Dispatcher(ApexPages.standardController sc){
				
		request = (Create_User_Request__c) sc.getRecord();
	}
	
	public PageReference redirect(){
		
		PageReference pr;
		
		if(request.Application__c == 'Salesforce.com'){
			
			pr = Page.Support_CreateUser_Admin;
			pr.getParameters().put('id' , request.Id);
						
		}else if(request.Application__c == 'CVent'){
			
			pr = Page.Support_CVent_Requests_Admin;
			pr.getParameters().put('id' , request.Id);			

		}else if(request.Application__c == 'BI'){
			
			pr = Page.Support_BI_Requests_Admin;
			pr.getParameters().put('id' , request.Id);			

		}else if(request.Application__c == 'Crossworld'){
			
			pr = Page.Support_XW_Request_Admin;
			pr.getParameters().put('id' , request.Id);			

		}else if(request.Application__c == 'BOT'){
			
			pr = Page.Support_BOT_Requests_Admin;
			pr.getParameters().put('id' , request.Id);			

		}
				
		return pr;
	}
}