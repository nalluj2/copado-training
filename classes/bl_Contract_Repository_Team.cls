public class bl_Contract_Repository_Team {
    
    public static void setUniqueKey(List<Contract_Repository_Team__c> triggerNew){
    	
    	for(Contract_Repository_Team__c teamMember : triggerNew){
    		
    		teamMember.Unique_Key__c = teamMember.Contract_Repository__c + ':' + teamMember.Team_Member__c;
    	}
    }
    
    public static void manageSharing(List<Contract_Repository_Team__c> triggerNew){
    	
    	Set<Id> repositoryIds = new Set<Id>();
    	System.debug('triggerNew: ' + triggerNew);
    	for(Contract_Repository_Team__c teamMember : triggerNew){
    		
    		repositoryIds.add(teamMember.Contract_Repository__c);    		
    	}
    	System.debug('repositoryIds: ' + repositoryIds);
    	Map<String, Contract_Repository__Share> existingShares = new Map<String, Contract_Repository__Share>();
    	
    	for(Contract_Repository__Share repoShare : [Select ParentId, UserOrGroupId, AccessLevel, Id from Contract_Repository__Share where parentId IN :repositoryIds AND RowCause = 'Contract_Team_Member__c']){
    		
    		existingShares.put(repoShare.ParentId + ':' + repoShare.UserOrGroupId, repoShare);
    	}
    	
    	List<Contract_Repository__Share> toDelete = new List<Contract_Repository__Share>();
    	List<Contract_Repository__Share> toUpsert = new List<Contract_Repository__Share>();
    	
    	for(Contract_Repository__c repo : [Select Id, (Select Team_Member__c, Access_Level__c from Contract_Repository_Teams__r) from Contract_Repository__c where Id IN :repositoryIds]){
    		
    		System.debug('Team size: ' + repo.Contract_Repository_Teams__r.size());
    		
    		for(Contract_Repository_Team__c teamMember : repo.Contract_Repository_Teams__r){
    			
    			Contract_Repository__Share share = existingShares.remove(repo.Id + ':' + teamMember.Team_Member__c);
    			if(teamMember.Access_Level__c == 'None'){
    				
    				if(share != null) toDelete.add(share);
    				
    			}//CR-28989
                else if(teamMember.Access_Level__c == 'Read/Write'){
    				
    				if(share == null){
    					
    					share = new Contract_Repository__Share();
    					share.ParentId = repo.Id;
    					share.UserOrGroupId = teamMember.Team_Member__c;
    					share.RowCause = Schema.Contract_Repository__Share.RowCause.Contract_Team_Member__c;
    					share.AccessLevel = 'Edit';
    					toUpsert.add(share);
    					
    				}else if(share.AccessLevel != 'Edit'){
    					
    					share.AccessLevel = 'Edit';
    					toUpsert.add(share);
    				}	
    			}
                else if(teamMember.Access_Level__c.startsWith('Read Only')){
    				if(share == null){
    					share = new Contract_Repository__Share();
    					share.ParentId = repo.Id;
    					share.UserOrGroupId = teamMember.Team_Member__c;
    					share.RowCause = Schema.Contract_Repository__Share.RowCause.Contract_Team_Member__c;
    					share.AccessLevel = 'Read';
    					toUpsert.add(share);
    					
    				}else if(share.AccessLevel != 'Read'){
    					
    					share.AccessLevel = 'Read';
    					toUpsert.add(share);
    				}
    				
    			}
    		}
    	}
    	System.debug('Existing Map size: ' + existingShares.size());
    	if(existingShares.size() > 0) toDelete.addAll(existingShares.values());
    	
    	if(toDelete.size() > 0) delete toDelete;
    	if(toUpsert.size() > 0) upsert toUpsert;
    }
    
    public static Boolean isAutoProcessRunning = false;
    
    public static void protectAutoMembers(List<Contract_Repository_Team__c> triggerNew, Map<Id, Contract_Repository_Team__c> oldMap){
    	
    	for(Contract_Repository_Team__c contractTeam : triggerNew){
    		    
    		Contract_Repository_Team__c oldContract = oldMap != null ? oldMap.get(contractTeam.Id) : null;    		    		
    		if(
    			(oldContract != null && oldContract.Auto_Aligned_with_Territory__c == true && (contractTeam.Team_Member__c != oldContract.Team_Member__c || contractTeam.Auto_Aligned_with_Territory__c == false)) || //Update auto aligned
    			(oldContract != null && oldContract.Contract_Repository_Owner__c == true && (contractTeam.Team_Member__c != oldContract.Team_Member__c || contractTeam.Access_Level__c != oldContract.Access_Level__c ||  contractTeam.Contract_Repository_Owner__c == false)) || // Update Owner
    			(oldContract == null && (contractTeam.Auto_Aligned_with_Territory__c == true || contractTeam.Contract_Repository_Owner__c == true)) //DELETE
    		){
    			
    			if(isAutoProcessRunning == false) contractTeam.addError('Automatically added Team members cannot be modified or deleted.');    			
    		}
    	}
    }
}