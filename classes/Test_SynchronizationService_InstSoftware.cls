@isTest
private class Test_SynchronizationService_InstSoftware {
	
	private static testMethod void syncInstSoftware_Inbound(){
		
		Synchronization_Service_Setting__c syncSettings = new Synchronization_Service_Setting__c();
		syncSettings.username__c = 'test@medtronic.com';
		syncSettings.password__c = 'password';
		syncSettings.client_id__c = '123456789';
		syncSettings.client_secret__c = 'XXXXX';
		syncSettings.is_sandbox__c = true;
		syncSettings.sync_Enabled__c = true;
		insert syncSettings;
		
		//Products
		Product2 testProduct = new Product2();
		testProduct.Name = 'PoleStar';
		testProduct.ProductCode = '000999';
		testProduct.isActive = true;
					
		insert testProduct;
		
		//Account
		Account acc = new Account();	
		acc.AccountNumber = '0000000';
		acc.Name = 'Syn Test Account';
		acc.Phone = '+123456789';
		acc.BillingCity = 'Minneapolis';
		acc.BillingCountry = 'UNITED STATES';
		acc.BillingState = 'Minnesota';
		acc.BillingStreet = 'street';
		acc.BillingPostalCode = '123456';
		acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non-SAP Account').getRecordTypeId();	
		insert acc;
				
		//Asset
		Asset asset = new Asset();
		asset.AccountId = acc.Id;
		asset.Product2Id = testProduct.Id;
		asset.Asset_Product_Type__c = 'PoleStar N-10';
		asset.Ownership_Status__c = 'Purchased';
		asset.Name = '123456789';
		asset.Serial_Nr__c = '123456789';
		asset.Status = 'Installed';
		asset.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('PoleStar').getRecordTypeId();
		insert asset;
		
		//Software
		Software__c soft = new Software__c();
		soft.Name = 'Test Software';
		soft.Active__c = true;
		soft.External_Id__c = 'Test_Software_Id';
		soft.Software_Name__c = 'Test Software';
		soft.Version__c = '1.0';
		soft.Asset_Product_Type__c = 'PoleStar N-10';
		insert soft;
		
		Test.startTest();
		
		Test_bl_SynchronizationService_Utils.CalloutMock mockImpl = new Test_bl_SynchronizationService_Utils.CalloutMock();
		
		Test.setMock(HttpCalloutMock.class, mockImpl);
		bl_SynchronizationService_Utils.calloutMock = mockImpl;
					
		SynchronizationService_InstSoftware.InstalledSoftwareSyncPayload request = new SynchronizationService_InstSoftware.InstalledSoftwareSyncPayload();
						
		//Installed software
		Installed_Software__c iSoftware = new Installed_Software__c();		
		iSoftware.External_Id__c = 'Test_Installed_Software_Id';
		iSoftware.Purchased__c = true;
		iSoftware.Purchase_Date__c = Date.Today();
		iSoftware.Install_Date__c = Date.Today();
		request.instSoftware = iSoftware;
				
		//Other
		request.assetId = '123456789';
		request.softwareId = 'Test_Software_Id';
		request.createdDate = DateTime.now();
		request.createdBy = 'Test User';
		
		String payload = JSON.serialize(request);
		
		bl_SynchronizationService_Target.processRecordPayload(payload, 'Installed_Software__c');
		
		List<Installed_Software__c> instSoftwares = [Select Id from Installed_Software__c where External_Id__c = 'Test_Installed_Software_Id'];
						
		System.assert(instSoftwares.size() == 1);	
	}
	
	private static testMethod void syncInstSoftware_Outbound(){
        
        //Products
        Product2 testProduct = new Product2();
        testProduct.Name = 'O-Arm';
        testProduct.ProductCode = '000999';
        testProduct.isActive = true;                    
        insert testProduct;
                        
        //Account
		Account acc = new Account();	
		acc.AccountNumber = '0000000';
		acc.Name = 'Syn Test Account';
		acc.Phone = '+123456789';
		acc.BillingCity = 'Minneapolis';
		acc.BillingCountry = 'UNITED STATES';
		acc.BillingState = 'Minnesota';
		acc.BillingStreet = 'street';
		acc.BillingPostalCode = '123456';
		acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non-SAP Account').getRecordTypeId();	
		insert acc;
                
       //Asset
		Asset asset = new Asset();
		asset.AccountId = acc.Id;
		asset.Product2Id = testProduct.Id;
		asset.Asset_Product_Type__c = 'O-Arm 1000';
		asset.Ownership_Status__c = 'Purchased';
		asset.Name = '123456789';
		asset.Serial_Nr__c = '123456789';
		asset.Status = 'Installed';
		asset.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('O-Arm').getRecordTypeId();
		insert asset;
        
        //Software
		Software__c soft = new Software__c();
		soft.Name = 'Test Software';
		soft.Active__c = true;
		soft.External_Id__c = 'Test_Software_Id';
		soft.Software_Name__c = 'Test Software';
		soft.Version__c = '1.0';
		soft.Asset_Product_Type__c = 'O-Arm 1000';
		insert soft;
        
        Test.startTest();
        
        Installed_Software__c iSoftware = new Installed_Software__c();
        iSoftware.Asset__c = asset.Id;
        iSoftware.Software__c = soft.Id;
        iSoftware.External_Id__c = 'Test_Installed_Software_Id';        
        iSoftware.Purchased__c = true;
        iSoftware.Purchase_Date__c = Date.Today();
        iSoftware.Install_Date__c = Date.Today();
        insert iSoftware;
            
        List<Sync_Notification__c> notifications = [Select Id from Sync_Notification__c where Status__c = 'New' AND Type__c = 'Outbound' AND Record_Object_Type__c = 'Installed_Software__c'];
        
        System.assert(notifications.size() == 1);
        
        SynchronizationService_InstSoftware instSoftService = new SynchronizationService_InstSoftware();
        
        String payload = instSoftService.generatePayload('Test_Installed_Software_Id');
        
        System.assert(payload != null);                
    }
	
	private static testMethod void syncInstSoftware_err(){
		
		SynchronizationService_InstSoftware.InstalledSoftwareSyncPayload request = new SynchronizationService_InstSoftware.InstalledSoftwareSyncPayload();
		
		//Installed software
		Installed_Software__c iSoftware = new Installed_Software__c();		
		iSoftware.External_Id__c = 'Test_Installed_Software_Id';
		iSoftware.Purchased__c = true;
		iSoftware.Purchase_Date__c = Date.Today();
		iSoftware.Install_Date__c = Date.Today();
		request.instSoftware = iSoftware;
				
		//Other				
		request.createdDate = DateTime.now();
		request.createdBy = 'Test User';
						
		//Asset error
		request.assetId = 'X';
				
		String result = bl_SynchronizationService_Target.processRecordPayload( JSON.serialize(request), 'Installed_Software__c');
			
		System.assert(result == 'Asset not in INTL');			
	}
}