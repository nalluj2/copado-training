/*
 *      Created Date 	: 17-02-2014
 *      Author 			: Bart Caelen
 *      Description 	: TEST Class for the APEX TRIGGER tr_Attachment_ChatterFeed
 */
@isTest private class TEST_tr_Attachment_ChatterFeed {

    @isTest static void test_tr_Note_ChatterFeed_AccPlan() {
    	
		// Master Data
		Company__c europe = new Company__c();
		europe.Name = 'Europe';
		europe.Company_Code_Text__c = 'TST';
		insert europe;
		
		// Business Unit Groups
		Business_Unit_Group__c cvgBUG = new Business_Unit_Group__c();
		cvgBUG.Master_Data__c = europe.id;
        cvgBUG.name='CVG'; 
				       
        insert cvgBUG;
        
        Account acc = new Account();
		acc.Name = 'Unit Test Account';
		acc.SAP_Id__c = '123456789';
		acc.Account_Country_vs__c = 'Belgium';
		insert acc;
        
		Account_Plan_2__c accPlan = new Account_Plan_2__c();
		accPlan.Account__c = acc.Id;
		accPlan.Account_Plan_Level__c = 'Business Unit Group';
		accPlan.Business_Unit_Group__c = cvgBUG.Id;
		insert accPlan;
		
		Test.startTest();
		
		Attachment testAttach = new Attachment();
		testAttach.ParentId = accPlan.Id;
		testAttach.Name = 'Unit Test Note';
		testAttach.Body = Blob.valueOf('Unit Test Note');
		insert testAttach;
		
		Test.stopTest();
		
		List<FeedItem> accPlanPosts = [Select Id from FeedItem where ParentId = :accPlan.Id];
		
		System.assert(accPlanPosts.size() == 1);
    }
    
    @isTest static void test_tr_Note_ChatterFeed_MITGOpp() {
    			        
        Account acc = new Account();
		acc.Name = 'Unit Test Account';
		acc.SAP_Id__c = '123456789';
		acc.Account_Country_vs__c = 'Belgium';
		insert acc;
        
		Opportunity mitgOpp = new Opportunity();
		mitgOpp.AccountId = acc.Id;
		mitgOpp.RecordTypeId = [Select Id from RecordType where SObjectType = 'Opportunity' AND DeveloperName = 'EUR_MITG_Standard_Opportunity'].Id;
		mitgOpp.Name = 'Unit Test Opp';
		mitgOpp.CloseDate = Date.today().addMonths(2);
		mitgOpp.StageName = 'Identify';
		insert mitgOpp;
		
		Test.startTest();
		
		Attachment testAttach = new Attachment();
		testAttach.ParentId = mitgOpp.Id;
		testAttach.Name = 'Unit Test Note';
		testAttach.Body = Blob.valueOf('Unit Test Note');
		insert testAttach;
		
		Test.stopTest();
		
		List<FeedItem> oppPosts = [Select Id from FeedItem where ParentId = :mitgOpp.Id];
		
		System.assert(oppPosts.size() == 1);
    }
    
    @isTest static void test_tr_Note_ChatterFeed() {
    			        
        Account acc = new Account();
		acc.Name = 'Unit Test Account';
		acc.SAP_Id__c = '123456789';
		acc.Account_Country_vs__c = 'Belgium';
		insert acc;
        		
		Test.startTest();
		
		Attachment testAttach = new Attachment();
		testAttach.ParentId = acc.Id;
		testAttach.Name = 'Unit Test Note';
		testAttach.Body = Blob.valueOf('Unit Test Note');
		insert testAttach;
		
		Test.stopTest();
		
		List<FeedItem> accPosts = [Select Id from FeedItem where ParentId = :acc.Id];
		
		System.assert(accPosts.size() == 0);
    }
}