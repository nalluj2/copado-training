@isTest
private class Test_CVG_Email_Templates {
	
	@testSetup
	private static void createData() {
	
		Company__c cmpny = new Company__c();		
        cmpny.name='Europe';
        cmpny.CurrencyIsoCode = 'EUR';
        cmpny.Current_day_in_Q1__c=56;
        cmpny.Current_day_in_Q2__c=34; 
        cmpny.Current_day_in_Q3__c=5; 
        cmpny.Current_day_in_Q4__c= 0;   
        cmpny.Current_day_in_year__c =200;
        cmpny.Days_in_Q1__c=56;  
        cmpny.Days_in_Q2__c=34;
        cmpny.Days_in_Q3__c=13;
        cmpny.Days_in_Q4__c=22;
        cmpny.Days_in_year__c =250;
        cmpny.Company_Code_Text__c = 'T35';
		insert cmpny;
		
		Business_Unit_Group__c bug =  new Business_Unit_Group__c();
        bug.Master_Data__c =cmpny.id;
        bug.name='RTG';      
        insert bug;
		
		Business_Unit__c bu =  new Business_Unit__c();
        bu.Company__c =cmpny.id;
        bu.name='RTG BU';
        bu.Business_Unit_Group__c = bug.Id;
        bu.Account_Plan_Activities__c=true;        
        insert bu;
        
        Sub_Business_Units__c sbu = new Sub_Business_Units__c();
        sbu.name='RTG sBU';
        sbu.Business_Unit__c=bu.id;
		insert sbu;
		
		DIB_Country__c country = new DIB_Country__c();
		country.Company__c = cmpny.Id;
		country.Name = 'Unit Test Country';
		country.Country_ISO_Code__c = 'ZZ';
		country.CurrencyIsoCode = 'EUR';
		insert country;
		
		Growth_Driver__c sbuDriver = new Growth_Driver__c();		
		sbuDriver.Growth_Driver__c = 'test SBU';
		sbuDriver.Description__c = 'test explanation';		
		sbuDriver.Active__c = true;
		sbuDriver.Sub_Business_Unit__c = sbu.Id;
		insert sbuDriver;
		
		Product2 prod = new Product2();
		prod.name = 'test Product';
		prod.isActive=true;		
        insert prod;
        
        PricebookEntry pbe=new PricebookEntry();
        pbe.Pricebook2Id = Test.getStandardPricebookId();
        pbe.Product2Id=prod.id;
        pbe.UseStandardPrice=false;
        pbe.unitPrice=100;
        pbe.isActive=true;
        pbe.CurrencyIsoCode='EUR';
        insert pbe;
        
        User currentUser = new User(Id = UserInfo.getUserId());
		currentUSer.Company_Code_Text__c = 'T35';
		update currentUser;
								
		Account testAccount = new Account();
		testAccount.Name = 'RTG Test Account';
		insert testAccount;
		
		Contact cnt = new Contact();    	
    	cnt.AccountId = testAccount.Id;
    	cnt.FirstName = 'Test';
		cnt.LastName = 'Contact';
		cnt.Phone = '+123456789';		
		cnt.Email = 'test.contact@gmail.com';
		cnt.MobilePhone = '+123456789';		
		cnt.MailingCity = 'Minneapolis';
		cnt.MailingCountry = 'UNITTEST';
		cnt.MailingState = 'Minnesota';
		cnt.MailingStreet = 'street';
		cnt.MailingPostalCode = '123456';		
		cnt.Contact_Department__c = 'Cardiology';
		cnt.Contact_Primary_Specialty__c = 'Cardiovascular'; 
		cnt.Affiliation_To_Account__c = 'Employee'; 
		cnt.Primary_Job_Title_vs__c = 'Surgeon'; 
		cnt.Contact_Gender__c = 'Male';		
		cnt.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Generic Contact').getRecordTypeId();	
    	insert cnt;
		
		Account_Plan_2__c accPlan = new Account_Plan_2__c();
		accPlan.Account__c = testAccount.Id;
		accPlan.Account_Plan_Level__c = 'Business Unit Group';
		accPlan.Business_Unit_Group__c = bug.Id;
		insert accPlan;
		
		Opportunity opp = new Opportunity();
		opp.Name = 'Test opp';
		opp.AccountId = testAccount.Id;
		opp.Account_Plan__c = accPlan.Id;
		opp.Sub_Business_Unit_Id__c = sbu.Id;
		opp.RecordTypeId = [Select Id from RecordType where SObjectType = 'Opportunity' AND DeveloperName = 'CVG_Project'].Id;
		opp.StageNAme = 'Identify';
		opp.CloseDate = Date.today();
		insert opp;
		
		Opportunity_Growth_Driver__c oppGrowthDriver = new Opportunity_Growth_Driver__c();
		oppGrowthDriver.Opportunity__c = opp.Id;
		oppGrowthDriver.Growth_Driver_Id__c = sbuDriver.Id;
		insert oppGrowthDriver;
		
		OpportunityLineItem oppProd = new OpportunityLineItem();
		oppProd.OpportunityId = opp.Id;
		oppProd.PriceBookEntryId = pbe.Id;
		oppProd.Quantity = 1;
		oppProd.TotalPrice = 1000;
		insert oppProd;
		
		Task tsk = new Task();
		tsk.WhatId = opp.Id;
		tsk.Subject = 'Test Task';
		tsk.WhoId = cnt.Id;
		tsk.ActivityDate = Date.today();
		tsk.Priority = 'Medium';
		tsk.Description = 'Test taks description';
		insert tsk;
		
		
	}    
	
	private static testmethod void testOpportunityTemplate(){
		
		Opportunity opp = [Select Id from Opportunity];
		
		Test.startTesT();
		
		ctrl_CVG_Opportunity_Template controller = new ctrl_CVG_Opportunity_Template();
		controller.OppId = opp.Id;
		
		System.assert(controller.opp != null);
		System.assert(controller.oppGrowths != null && controller.oppGrowths.size() == 1);
		System.assert(controller.oppProd != null);	
	}
	
	private static testmethod void testTaskTemplate(){
		
		Task tsk = [Select Id from Task];
		
		Test.startTesT();
		
		ctrl_CVG_Task_Template controller = new ctrl_CVG_Task_Template();
		controller.TskId = tsk.Id;
		
		System.assert(controller.task != null);
		System.assert(controller.opp != null);
					
	}
}