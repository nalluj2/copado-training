@isTest
private class Test_tr_StakeholderEmployeeRelationship {
    
    private static testmethod void testCalculateRelationshipWithMedtronicIHS(){
    	
		clsTestData_Account.createAccount();
		clsTestData_Contact.createContact();

		clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'EMEA_IHS_Managed_Services').Id;
		clsTestData_Opportunity.iRecord_Opportunity = 1;
		List<Opportunity> lstOpportunity1 = clsTestData_Opportunity.createOpportunity();

		System.assert(lstOpportunity1.size() == 1);
		
		clsTestData_Opportunity.idRecordType_StakeholderMapping = clsUtil.getRecordTypeByDevName('Stakeholder_Mapping__c', 'EMEA_IHS_Stakeholder_Mapping').Id;
		clsTestData_Opportunity.iRecord_StakeholderMapping = 1;
		List<Stakeholder_Mapping__c> lstStakeholderMapping1 = clsTestData_Opportunity.createStakeholderMapping();
		
		Stakeholder_Mapping__c stakeholder = [SELECT Id, Relationship_with_Medtronic_IHS__c FROM Stakeholder_Mapping__c WHERE Opportunity__c = :lstOpportunity1[0].Id];		
		System.assert(stakeholder.Relationship_with_Medtronic_IHS__c == null);		
		
		List<Stakeholder_Employee_Relationship__c> lstEmployeeRelationship = [SELECT Id FROM Stakeholder_Employee_Relationship__c];
		System.assert(lstEmployeeRelationship.size() == 0);

		Test.startTest();
		
		Stakeholder_Employee_Relationship__c stakeholderEmployee1 = new Stakeholder_Employee_Relationship__c();
		stakeholderEmployee1.MDT_Contact__c = stakeholder.Id;
		stakeholderEmployee1.Relationship_Quality__c = '3';
		stakeholderEmployee1.Employee__c = 'Employee 1';
		
		Stakeholder_Employee_Relationship__c stakeholderEmployee2 = new Stakeholder_Employee_Relationship__c();
		stakeholderEmployee2.MDT_Contact__c = stakeholder.Id;
		stakeholderEmployee2.Relationship_Quality__c = '5';
		stakeholderEmployee2.Employee__c = 'Employee 2';
				
		insert new List<Stakeholder_Employee_Relationship__c>{stakeholderEmployee1, stakeholderEmployee2};
		
		stakeholder = [SELECT Id, Relationship_with_Medtronic_IHS__c FROM Stakeholder_Mapping__c WHERE Id = :stakeholder.Id];
		System.assert(stakeholder.Relationship_with_Medtronic_IHS__c == '4');
		
		
		stakeholderEmployee1.Relationship_Quality__c = '5';
		update stakeholderEmployee1;
		
		stakeholder = [SELECT Id, Relationship_with_Medtronic_IHS__c FROM Stakeholder_Mapping__c WHERE Id = :stakeholder.Id];
		System.assert(stakeholder.Relationship_with_Medtronic_IHS__c == '5');
		
		Stakeholder_Employee_Relationship__c stakeholderEmployee3 = new Stakeholder_Employee_Relationship__c();
		stakeholderEmployee3.MDT_Contact__c = stakeholder.Id;
		stakeholderEmployee3.Relationship_Quality__c = '1';
		stakeholderEmployee3.Employee__c = 'Employee 3';
		insert stakeholderEmployee3;
		
		stakeholder = [SELECT Id, Relationship_with_Medtronic_IHS__c FROM Stakeholder_Mapping__c WHERE Id = :stakeholder.Id];
		System.assert(stakeholder.Relationship_with_Medtronic_IHS__c == '4');
		
		delete new List<Stakeholder_Employee_Relationship__c>{stakeholderEmployee1, stakeholderEmployee2, stakeholderEmployee3};	
		
		stakeholder = [SELECT Id, Relationship_with_Medtronic_IHS__c FROM Stakeholder_Mapping__c WHERE Id = :stakeholder.Id];
		System.assert(stakeholder.Relationship_with_Medtronic_IHS__c == null);
		
		undelete stakeholderEmployee3;
		
		stakeholder = [SELECT Id, Relationship_with_Medtronic_IHS__c FROM Stakeholder_Mapping__c WHERE Id = :stakeholder.Id];
		System.assert(stakeholder.Relationship_with_Medtronic_IHS__c == '1');
    }
}