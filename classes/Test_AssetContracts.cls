@isTest
private class Test_AssetContracts {

    static testMethod void myUnitTest() {

        Account a = new Account() ; 
            a.Name = 'Test account'; 
            a.SAP_ID__c = '045566651' ; 
            a.Account_Country_vs__c = 'ITALY';
            a.RecordTypeId = FinalConstants.sapAccountRecordTypeId;
        insert a; 

        clsTestData_Account.oMain_Account = a;
        Asset ass = clsTestData_Asset.createAsset(false)[0];
            ass.Name = 'test';
            ass.AccountId = a.Id;
            ass.Asset_Type_Picklist__c = 'Programmer';   
            ass.Price = 1;
        insert ass;
    	    	
    	Contract c = new Contract();
        	c.AccountId = a.Id;
        	c.Agreement_level_Picklist__c = 'test';
        	c.StartDate = Date.Today() -1;
        	c.ContractTerm = 6;
    	insert c;

        AssetContract__c ac = new AssetContract__c();
            ac.Asset__c = ass.Id;
            ac.Contract__c = c.Id;
        insert ac;
                        
        c.Status = 'Activated';
		update c;

		Test.startTest();

    	Contract c2 = new Contract();
        	c2.AccountId = a.Id;
        	c2.Agreement_level_Picklist__c = 'test';
        	c2.StartDate = Date.Today() -2;
        	c2.ContractTerm = 6;
    	insert c2;

        AssetContract__c ac2 = new AssetContract__c();
            ac2.Asset__c = ass.Id;
            ac2.Contract__c = c.Id;
        insert ac2;

            c2.Status = 'Activated';
		update c2;
		
            c.StartDate = Date.today() - 365;
		update c;

            c2.StartDate = Date.today() - 365;
		update c2;
			
		delete c;
		delete c2;

        Test.stopTest();

    }
}