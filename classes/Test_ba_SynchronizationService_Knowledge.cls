@isTest
private class Test_ba_SynchronizationService_Knowledge {
	
	private static testMethod void synchKnowledgeChanges(){
		
		User knowledgeUser = [Select Id from User where UserPermissionsKnowledgeUser = true AND isActive = true LIMIT 1];
		
		System.runAs(knowledgeUser){
		
			Synchronization_Service_Setting__c syncSettings = new Synchronization_Service_Setting__c();
			syncSettings.username__c = 'test@medtronic.com';
			syncSettings.password__c = 'password';
			syncSettings.client_id__c = '123456789';
			syncSettings.client_secret__c = 'XXXXX';
			syncSettings.is_sandbox__c = true;
			syncSettings.Sync_Enabled__c = true;
			insert syncSettings;
			
			Test.startTest();
			
			//Invoked thorugh the web service created for that			
			ws_SynchronizationService_Knowledge.doGet();
					
			CalloutMock mockImpl = new CalloutMock();
			
			Test.setMock(HttpCalloutMock.class, mockImpl);
			bl_SynchronizationService_Utils.calloutMock = mockImpl;	
			
			Test.stopTest();
		}	
	}
	
	private static testMethod void synchKnowledgeDelete(){
		

		User knowledgeUser = clsTestData_User.createUser_SystemAdministratorMDT('tstadm1', false);
			knowledgeUser.UserPermissionsKnowledgeUser = true;
		insert knowledgeUser;
		
		System.runAs(knowledgeUser){
		
			Synchronization_Service_Setting__c syncSettings = new Synchronization_Service_Setting__c();
			syncSettings.username__c = 'test@medtronic.com';
			syncSettings.password__c = 'password';
			syncSettings.client_id__c = '123456789';
			syncSettings.client_secret__c = 'XXXXX';
			syncSettings.is_sandbox__c = true;
			syncSettings.Sync_Enabled__c = true;
			insert syncSettings;
			
			Text_Article__kav article = new Text_Article__kav();
			article.Article_Text__c = 'UnitTest Knowledge Article Version'; 
			article.Intranet_URL__c = 'Test';
			article.Language = 'en_US';
			article.Title = 'UnitTest Knowledge Article Version';
			article.Summary = 'Test';
			article.UrlName = 'Test';						
			insert article;
			
			article = [Select Id, KnowledgeArticleId from Text_Article__kav where Language = 'en_US' AND PublishStatus = 'Draft' AND Id = :article.Id];
					
			KbManagement.PublishingService.publishArticle( article.KnowledgeArticleId, true);
			
			Sync_Record_Id_Mapping__c idMapping = new Sync_Record_Id_Mapping__c();
			idMapping.Name = article.KnowledgeArticleId;
			idMapping.External_Id__c = '00a000000000001AAA';
			idMapping.Object_Type__c = 'KnowledgeArticle';
			insert idMapping;
			
			Test.startTest();
			
			ba_SynchronizationService_Knowledge batch = new ba_SynchronizationService_Knowledge();
			Database.executeBatch(batch);
			
			CalloutMock_del mockImpl = new CalloutMock_del();
			
			Test.setMock(HttpCalloutMock.class, mockImpl);
			bl_SynchronizationService_Utils.calloutMock = mockImpl;	
			
			Test.stopTest();
		}
	}
	
	private static testMethod void synchKnowledgeUpdate(){
		
		User knowledgeUser = clsTestData_User.createUser_SystemAdministratorMDT('tstadm1', false);
			knowledgeUser.UserPermissionsKnowledgeUser = true;
		insert knowledgeUser;
		
		System.runAs(knowledgeUser){
			
			Synchronization_Service_Setting__c syncSettings = new Synchronization_Service_Setting__c();
			syncSettings.username__c = 'test@medtronic.com';
			syncSettings.password__c = 'password';
			syncSettings.client_id__c = '123456789';
			syncSettings.client_secret__c = 'XXXXX';
			syncSettings.is_sandbox__c = true;
			syncSettings.Sync_Enabled__c = true;
			insert syncSettings;
			
			Text_Article__kav article = new Text_Article__kav();
			article.Article_Text__c = 'UnitTest Knowledge Article Version'; 
			article.Intranet_URL__c = 'Test';
			article.Language = 'en_US';
			article.Title = 'UnitTest Knowledge Article Version';
			article.Summary = 'Test';
			article.UrlName = 'Test';						
			insert article;
			
			article = [Select Id, KnowledgeArticleId from Text_Article__kav where Language = 'en_US' AND PublishStatus = 'Draft' AND Id = :article.Id];
					
			KbManagement.PublishingService.publishArticle( article.KnowledgeArticleId, true);
			
			Sync_Record_Id_Mapping__c idMapping = new Sync_Record_Id_Mapping__c();
			idMapping.Name = article.KnowledgeArticleId;
			idMapping.External_Id__c = '00a000000000001AAA';
			idMapping.Object_Type__c = 'KnowledgeArticle';
			insert idMapping;
			
			Test.startTest();
			
			ba_SynchronizationService_Knowledge batch = new ba_SynchronizationService_Knowledge();
			Database.executeBatch(batch);
			
			CalloutMock_updt mockImpl = new CalloutMock_updt();
			
			Test.setMock(HttpCalloutMock.class, mockImpl);
			bl_SynchronizationService_Utils.calloutMock = mockImpl;	
			
			Test.stopTest();
		}
	}
	
	public class CalloutMock implements HttpCalloutMock {

        public HTTPResponse respond(HTTPRequest req) {
            
            HttpResponse resp = new HttpResponse();
            resp.setStatusCode(200);
           	resp.setStatus('Complete');
            	           
            if(req.getEndpoint().endsWith('services/oauth2/token')){
            	
            	String respBody =  '{' +
					            '"id" : "1",' +
					            '"issued_at" : "1",' +
					            '"instance_url" : "1",' +
					            '"signature" : "1",' +
					            '"access_token" : "1",' +
					            '"token_type" : "1"' +
				            '}';
				resp.setBody(respBody);
				            
            }if(req.getEndpoint().contains('Sync_Record_Change__c')){
            	
				String respBody = 
        		'{' +
        			'"totalSize": 1,' +
					'"done": true,' +
					'"records": [' +
        				'{' +
	        				'"attributes": {' +
	            				'"type": "Sync_Record_Change__c",' +
	            				'"url": "/services/data/v32.0/sobjects/Sync_Record_Change__c/"' +
	        				'},' +        
	        				'"Record_Id__c": "00a000000000001AAA",' +
	        				'"Secondary_Record_Id__c": "0av000000000001AAA",' +
	        				'"Action__c": "Update",' +
	        				'"Status__c": "New",' +
	        				'"Record_Object_Type__c": "Text_Article__kav"' +	        					        				
        				'},' + 
        				'{' +
	        				'"attributes": {' +
	            				'"type": "Sync_Record_Change__c",' +
	            				'"url": "/services/data/v32.0/sobjects/Sync_Record_Change__c/"' +
	        				'},' +        
	        				'"Record_Id__c": "00a000000000002AAA",' +
	        				'"Secondary_Record_Id__c": "0av000000000002AAA",' +
	        				'"Action__c": "Update",' +
	        				'"Status__c": "New",' +
	        				'"Record_Object_Type__c": "Index_Page__kav"' +	        					        				
        				'},' + 
        				'{' +
	        				'"attributes": {' +
	            				'"type": "Sync_Record_Change__c",' +
	            				'"url": "/services/data/v32.0/sobjects/Sync_Record_Change__c/"' +
	        				'},' +        
	        				'"Record_Id__c": "00a000000000003AAA",' +
	        				'"Secondary_Record_Id__c": "0av000000000003AAA",' +
	        				'"Action__c": "Update",' +
	        				'"Status__c": "New",' +
	        				'"Record_Object_Type__c": "Long_Text__kav"' +	        					        				
        				'},' + 
        				'{' +
	        				'"attributes": {' +
	            				'"type": "Sync_Record_Change__c",' +
	            				'"url": "/services/data/v32.0/sobjects/Sync_Record_Change__c/"' +
	        				'},' +        
	        				'"Record_Id__c": "00a000000000004AAA",' +
	        				'"Secondary_Record_Id__c": "0av000000000004AAA",' +
	        				'"Action__c": "Update",' +
	        				'"Status__c": "New",' +
	        				'"Record_Object_Type__c": "Procedure__kav"' +	        					        				
        				'},' + 
        				'{' +
	        				'"attributes": {' +
	            				'"type": "Sync_Record_Change__c",' +
	            				'"url": "/services/data/v32.0/sobjects/Sync_Record_Change__c/"' +
	        				'},' +        
	        				'"Record_Id__c": "00a000000000005AAA",' +
	        				'"Secondary_Record_Id__c": "0av000000000005AAA",' +
	        				'"Action__c": "Update",' +
	        				'"Status__c": "New",' +
	        				'"Record_Object_Type__c": "Table_of_Contents__kav"' +	        					        				
        				'}' +               
        			']' +
        		'}';
        		
        		resp.setBody(respBody);		
            	
            }else{
            	
            	bl_SynchronizationService_Utils.QueryResponse queryResp = new bl_SynchronizationService_Utils.QueryResponse();            	
				queryResp.totalSize = 1;
				queryResp.done = true;
				queryResp.nextRecordsUrl = null;
            	
            	String respBody = 
            		'{' +
            			'"totalSize": 1,' +
						'"done": true,' +
						'"records": [';
            	
            	if(req.getEndpoint().contains('Text_Article__kav')){
            			            	
            		respBody +=	'{' +
		        				'"attributes": {' +
		            				'"type": "Text_Article__kav",' +
		            				'"url": "/services/data/v32.0/sobjects/Text_Article__kav/"' +
		        				'},' +   
		        				'"Id": "0av000000000001AAA",' +     
		        				'"Article_Text__c": "UnitTest Knowledge Article Version",' +
		        				'"Intranet_URL__c": "Test",' +
		        				'"Language": "en_US",' +
		        				'"Title": "UnitTest Knowledge Text Article Version",' +
		        				'"Summary": "Test",' +
		        				'"UrlName": "Unit-Test-Text-Article",' +
		        				'"DataCategorySelections": {' +
									'"totalSize": 1,' +
									'"done": true,' +
									'"records": [' +
    									'{' +
                    						'"attributes": {' +
                        						'"type": "Text_Article__DataCategorySelection",' +
                        						'"url": "/services/data/v32.0/sobjects/Text_Article__DataCategorySelection/"' +
                    						'},' +                    
                    						'"DataCategoryName": "All",' +
                    						'"DataCategoryGroupName": "Imaging"' +                   					 		
                						'}' +               
            						']' +
            					'}' +
            				'}';
            				
            	}else if(req.getEndpoint().contains('Index_Page__kav')){
            		    
            		respBody +=	'{' +
		        				'"attributes": {' +
		            				'"type": "Index_Page__kav",' +
		            				'"url": "/services/data/v32.0/sobjects/Index_Page__kav/"' +
		        				'},' +   
		        				'"Id": "0av000000000002AAA",' +     
		        				'"Category__c": "TestCategory",' +
		        				'"Language": "en_US",' +
		        				'"Title": "UnitTest Knowledge Index Page Article Version",' +
		        				'"Summary": "Test",' +
		        				'"UrlName": "Unit-Test-Index-Page",' +
		        				'"DataCategorySelections": {' +
									'"totalSize": 1,' +
									'"done": true,' +
									'"records": [' +
    									'{' +
                    						'"attributes": {' +
                        						'"type": "Index_Page__DataCategorySelection",' +
                        						'"url": "/services/data/v32.0/sobjects/Index_Page__DataCategorySelection/"' +
                    						'},' +                    
                    						'"DataCategoryName": "All",' +
                    						'"DataCategoryGroupName": "Imaging"' +                   					 		
                						'}' +               
            						']' +
            					'}' +
            				'}';
            				
            	}else if(req.getEndpoint().contains('Long_Text__kav')){
            		        
            		respBody +=	'{' +
		        				'"attributes": {' +
		            				'"type": "Long_Text__kav",' +
		            				'"url": "/services/data/v32.0/sobjects/Long_Text__kav/"' +
		        				'},' +   
		        				'"Id": "0av000000000003AAA",' +     
		        				'"Article_Text__c": "Test Text",' +
		        				'"Article_Text_Continued__c": "Test Text Continuation",' +
		        				'"Intranet_URL__c": "Test-Intranet-URL",' +
		        				'"Language": "en_US",' +
		        				'"Title": "UnitTest Knowledge Long Text Article Version",' +
		        				'"Summary": "Test",' +
		        				'"UrlName": "Unit-Test-Long-Text",' +
		        				'"DataCategorySelections": {' +
									'"totalSize": 1,' +
									'"done": true,' +
									'"records": [' +
    									'{' +
                    						'"attributes": {' +
                        						'"type": "Long_Text__DataCategorySelection",' +
                        						'"url": "/services/data/v32.0/sobjects/Long_Text__DataCategorySelection/"' +
                    						'},' +                    
                    						'"DataCategoryName": "All",' +
                    						'"DataCategoryGroupName": "Imaging"' +                   					 		
                						'}' +               
            						']' +
            					'}' +
            				'}';
            	}else if(req.getEndpoint().contains('Procedure__kav')){
            		        
            		respBody +=	'{' +
		        				'"attributes": {' +
		            				'"type": "Procedure__kav",' +
		            				'"url": "/services/data/v32.0/sobjects/Procedure__kav/"' +
		        				'},' +   
		        				'"Id": "0av000000000004AAA",' +     
		        				'"Instructions__c": "Test Instructions",' +
		        				'"Option_1_Description__c": "Option 1 Description",' +
		        				'"Option_1_Title__c": "Option 1 Title",' +
		        				'"Language": "en_US",' +
		        				'"Title": "UnitTest Knowledge Procedure Article Version",' +
		        				'"Summary": "Test",' +
		        				'"UrlName": "Unit-Test-Procedure",' +
		        				'"DataCategorySelections": {' +
									'"totalSize": 1,' +
									'"done": true,' +
									'"records": [' +
    									'{' +
                    						'"attributes": {' +
                        						'"type": "Procedure__DataCategorySelection",' +
                        						'"url": "/services/data/v32.0/sobjects/Procedure__DataCategorySelection/"' +
                    						'},' +                    
                    						'"DataCategoryName": "All",' +
                    						'"DataCategoryGroupName": "Imaging"' +                   					 		
                						'}' +               
            						']' +
            					'}' +
            				'}';
            	}else if(req.getEndpoint().contains('Table_of_Contents__kav')){
            		        
            		respBody +=	'{' +
		        				'"attributes": {' +
		            				'"type": "Table_of_Contents__kav",' +
		            				'"url": "/services/data/v32.0/sobjects/Table_of_Contents__kav/"' +
		        				'},' +   
		        				'"Id": "0av000000000005AAA",' +	        				
		        				'"Language": "en_US",' +
		        				'"Title": "UnitTest Knowledge Table of Contents Article Version",' +
		        				'"Summary": "Test",' +
		        				'"UrlName": "Unit-Test-TOC",' +
		        				'"DataCategorySelections": {' +
									'"totalSize": 1,' +
									'"done": true,' +
									'"records": [' +
    									'{' +
                    						'"attributes": {' +
                        						'"type": "Table_of_Contents__DataCategorySelection",' +
                        						'"url": "/services/data/v32.0/sobjects/Table_of_Contents__DataCategorySelection/"' +
                    						'},' +                    
                    						'"DataCategoryName": "All",' +
                    						'"DataCategoryGroupName": "Imaging"' +                   					 		
                						'}' +               
            						']' +
            					'}' +
            				'}';
            	}                
            	
            	respBody +=	']' +
            				'}';
            		
            	resp.setBody(respBody);
        	}             	            							
                        
            return resp;
        }
	} 
	
	public class CalloutMock_del implements HttpCalloutMock {

        public HTTPResponse respond(HTTPRequest req) {
            
            HttpResponse resp = new HttpResponse();
            resp.setStatusCode(200);
           	resp.setStatus('Complete');
            	           
            if(req.getEndpoint().endsWith('services/oauth2/token')){
            	
            	String respBody =  '{' +
					            '"id" : "1",' +
					            '"issued_at" : "1",' +
					            '"instance_url" : "1",' +
					            '"signature" : "1",' +
					            '"access_token" : "1",' +
					            '"token_type" : "1"' +
				            '}';
				resp.setBody(respBody);
				            
            }if(req.getEndpoint().contains('Sync_Record_Change__c')){
            	
				String respBody = 
        		'{' +
        			'"totalSize": 1,' +
					'"done": true,' +
					'"records": [' +
        				'{' +
	        				'"attributes": {' +
	            				'"type": "Sync_Record_Change__c",' +
	            				'"url": "/services/data/v32.0/sobjects/Sync_Record_Change__c/"' +
	        				'},' +        
	        				'"Record_Id__c": "00a000000000001AAA",' +
	        				'"Secondary_Record_Id__c": null,' +
	        				'"Action__c": "Delete",' +
	        				'"Status__c": "New",' +
	        				'"Record_Object_Type__c": "Text_Article__kav"' +	        					        				
        				'}' +            
        			']' +
        		'}';
        		
        		resp.setBody(respBody);		
            	
            } 							
                        
            return resp;
        }
	} 
	
	public class CalloutMock_updt implements HttpCalloutMock {

        public HTTPResponse respond(HTTPRequest req) {
            
            HttpResponse resp = new HttpResponse();
            resp.setStatusCode(200);
           	resp.setStatus('Complete');
            	           
            if(req.getEndpoint().endsWith('services/oauth2/token')){
            	
            	String respBody =  '{' +
					            '"id" : "1",' +
					            '"issued_at" : "1",' +
					            '"instance_url" : "1",' +
					            '"signature" : "1",' +
					            '"access_token" : "1",' +
					            '"token_type" : "1"' +
				            '}';
				resp.setBody(respBody);
				            
            }if(req.getEndpoint().contains('Sync_Record_Change__c')){
            	
				String respBody = 
        		'{' +
        			'"totalSize": 1,' +
					'"done": true,' +
					'"records": [' +
        				'{' +
	        				'"attributes": {' +
	            				'"type": "Sync_Record_Change__c",' +
	            				'"url": "/services/data/v32.0/sobjects/Sync_Record_Change__c/"' +
	        				'},' +        
	        				'"Record_Id__c": "00a000000000001AAA",' +
	        				'"Secondary_Record_Id__c": "0av000000000001AAA",' +
	        				'"Action__c": "Update",' +
	        				'"Status__c": "New",' +
	        				'"Record_Object_Type__c": "Text_Article__kav"' +	        					        				
        				'}' +            
        			']' +
        		'}';
        		
        		resp.setBody(respBody);	            	
            }else{
            	
            	bl_SynchronizationService_Utils.QueryResponse queryResp = new bl_SynchronizationService_Utils.QueryResponse();            	
				queryResp.totalSize = 1;
				queryResp.done = true;
				queryResp.nextRecordsUrl = null;
            	
            	String respBody = 
            		'{' +
            			'"totalSize": 1,' +
						'"done": true,' +
						'"records": [' +
							'{' +
		        				'"attributes": {' +
		            				'"type": "Text_Article__kav",' +
		            				'"url": "/services/data/v32.0/sobjects/Text_Article__kav/"' +
		        				'},' +   
		        				'"Id": "0av000000000001AAA",' +     
		        				'"Article_Text__c": "UnitTest Knowledge Article Version",' +
		        				'"Intranet_URL__c": "Test",' +
		        				'"Language": "en_US",' +
		        				'"Title": "UnitTest Knowledge Text Article Version",' +
		        				'"Summary": "Test",' +
		        				'"UrlName": "Test",' +
		        				'"DataCategorySelections": {' +
									'"totalSize": 1,' +
									'"done": true,' +
									'"records": [' +
    									'{' +
                    						'"attributes": {' +
                        						'"type": "Text_Article__DataCategorySelection",' +
                        						'"url": "/services/data/v32.0/sobjects/Text_Article__DataCategorySelection/"' +
                    						'},' +                    
                    						'"DataCategoryName": "All",' +
                    						'"DataCategoryGroupName": "Imaging"' +                   					 		
                						'}' +               
            						']' +
            					'}' +
            				'}' +
            			']' +
            		'}'; 
            		
            		resp.setBody(respBody);           				
            	}							
                        
            return resp;
        }
	} 
}