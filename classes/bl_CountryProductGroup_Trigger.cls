public with sharing class bl_CountryProductGroup_Trigger {

	//---------------------------------------------------------------------------------------------------
	// Populate External ID field (CountryProductGroup__c) on Country_ProductGroup__c
	//---------------------------------------------------------------------------------------------------
	public static void populateExternalId(List<Country_ProductGroup__c> lstTriggerNew, Map<Id, Country_ProductGroup__c> mapTriggerOld){

        if (bl_Trigger_Deactivation.isTriggerDeactivated('CountryProductGroup_PopulateExternalId')) return;

		for (Country_ProductGroup__c oCountryProductGroup : lstTriggerNew){

			if (mapTriggerOld == null){

				// INSERT LOGIC - populate CountryProductGroup__c if it's empty on creation
				if (String.isBlank(oCountryProductGroup.CountryProductGroup__c)){

					if ( 
						(!String.isBlank(oCountryProductGroup.Product_Group__c))
						&& 
						(!String.isBlank(oCountryProductGroup.Country__c)) 
					){

						oCountryProductGroup.CountryProductGroup__c = oCountryProductGroup.Country__c + '-' + oCountryProductGroup.Product_Group__c;

					}

				}

			}else{

				// UPDATE LOGIC - re-populate CountryProductGroup__c if one of the members is changed or if CountryProductGroup__c is still emtpy
				Country_ProductGroup__c oCountryProductGroup_Old = mapTriggerOld.get(oCountryProductGroup.Id);

				if (
					(String.isBlank(oCountryProductGroup.CountryProductGroup__c))
					||
					(oCountryProductGroup.Product_Group__c != oCountryProductGroup_Old.Product_Group__c)
					||
					(oCountryProductGroup.Country__c != oCountryProductGroup_Old.Country__c)
				){

					oCountryProductGroup.CountryProductGroup__c = oCountryProductGroup.Country__c + '-' + oCountryProductGroup.Product_Group__c;

				}

			}

		}

	}
	//---------------------------------------------------------------------------------------------------

}