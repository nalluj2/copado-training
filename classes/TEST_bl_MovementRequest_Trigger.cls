//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 03-05-2016
//  Description      : APEX TEST Class - Test APEX for :
//                      - tr_MovementRequest (APEX Trigger)
//                      - bl_MovementRequest_Trigger (APEX Class)
//  Change Log       : PMT-22787: MITG EMEA User Setup - Changed profile from EUR Field Force MITG to EMEA Field Force MITG
//--------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_bl_MovementRequest_Trigger {

    private static Boolean bCreateData = true;
    private static List<Movement_Request__c> lstMovementRequest;
    private static List<Demo_Product__c> lstDemoProduct;
    private static List<User> lstUser_MITG;

    @isTest static void createTestData() {

        clsTestData.iRecord_Account = 4;
        clsTestData.createAccountData();

        clsTestData.iRecord_Contact = 2;
        clsTestData.createContactData();

        clsTestData.iRecord_Opportunity = 2;
        clsTestData.createOpportunityData();

        clsTestData.createProductData();

        // Create Demo_Product__c data
        lstDemoProduct = new List<Demo_Product__c>();
        for (Product2 oProduct : clsTestData.lstProduct){
            Demo_Product__c oDemoProduct = new Demo_Product__c();
                oDemoProduct.Asset_Name__c = oProduct.Id;
                oDemoProduct.Physically_Inspected__c = true;
                oDemoProduct.Current_Location__c = 'Hospital';
            lstDemoProduct.add(oDemoProduct);
        }
        insert lstDemoProduct;

        // Create Movement_Request__c data
        lstMovementRequest = new List<Movement_Request__c>();
        for (Demo_Product__c oDemoProduct : lstDemoProduct){
            Movement_Request__c oMovementRequest = new Movement_Request__c();
                oMovementRequest.Demo_Product_Name__c = oDemoProduct.Id;
                oMovementRequest.Hospital__c = clsTestData.lstAccount[0].Id;
                oMovementRequest.Collect_product_at_Hospital__c = clsTestData.lstAccount[1].Id;
                oMovementRequest.Contact_Name__c = clsTestData.lstContact[0].Id;
                oMovementRequest.Approval_Status__c = 'Approved';
                oMovementRequest.Start_Date__c = Date.today().addDays(-10);
                oMovementRequest.End_Date__c = Date.today().addDays(10);
                oMovementRequest.Start_Date_and_Time__c = Datetime.now().addDays(-10);
                oMovementRequest.End_Date_and_Time__c = Datetime.now().addDays(10);
                oMovementRequest.Reloan__c = true;
            lstMovementRequest.add(oMovementRequest);
        }

        if (bCreateData){
            insert lstMovementRequest;
        }
    }

    @isTest static void createTestDataWithProductManager() {

		//PMT-22787: MITG EMEA User Setup - Changed profile from EUR Field Force MITG to EMEA Field Force MITG
        //List<Profile> lstProfile = [SELECT Id, Name FROM Profile WHERE Name in ('EUR Field Force MITG') LIMIT 1];
        List<Profile> lstProfile = [SELECT Id, Name FROM Profile WHERE Name in ('EMEA Field Force MITG') LIMIT 1];

        Profile oProfile_SystemAdmin = lstProfile[0];
        
        User currentUser = new User(Id = UserInfo.getUserId());
        
        System.runAs(currentUser){
            
            User mitgUser = new User (
                Lastname = 'MITG User',
                Alias = 'mitgu',
                LanguageLocaleKey = 'en_US',
                Email = 'mitg.test.user@medtronic.com',
                Username = 'mitg.test.user@medtronic.com',
                CommunityNickname = 'mitguser',
                ProfileId = oProfile_SystemAdmin.Id,
                TimeZoneSidKey = 'Europe/Brussels',
                LocaleSidKey = 'en_US',
                EmailEncodingKey =  'UTF-8',            
                Company_Code_Text__c = 'EUR',
				User_Business_Unit_vs__c = 'Surgical Innovations',
                Job_Title_vs__c = clsUtil.getUserJobTitle('Surgical Innovations')[0],
                CostCenter__c = '123456',
                Alias_unique__c = 'mitgu',
                User_Status__c = 'Current',
                Region_vs__c = 'Europe',
                Sub_Region__c = 'NWE'
            );
            
            insert mitgUser;
            
            lstUser_MITG = new List<User>{mitgUser};
        }
            
        clsTestData.iRecord_Account = 4;
        clsTestData.createAccountData();

        clsTestData.iRecord_Contact = 2;
        clsTestData.createContactData();

        clsTestData.iRecord_Opportunity = 2;
        clsTestData.createOpportunityData();

        List<Product2> lstProduct = new List<Product2>();
        for (Integer iCounter = 0; iCounter < 5; iCounter++){
            Product2 oProduct = new Product2();
                oProduct.Name = 'TEST PRODUCT ' + String.valueOf(iCounter);
            lstProduct.add(oProduct);
        }
        insert lstProduct;

        // Create Demo_Product__c data
        lstDemoProduct = new List<Demo_Product__c>();
        for (Product2 oProduct : lstProduct){
            Demo_Product__c oDemoProduct = new Demo_Product__c();
                oDemoProduct.Asset_Name__c = oProduct.Id;
                oDemoProduct.Physically_Inspected__c = true;
                oDemoProduct.Current_Location__c = 'Hospital';
                oDemoProduct.OwnerId = lstUser_MITG[0].Id;
            lstDemoProduct.add(oDemoProduct);
        }
        System.runAs(lstUser_MITG[0]){
            insert lstDemoProduct;  
        }

        // Create Movement_Request__c data
        lstMovementRequest = new List<Movement_Request__c>();
        for (Demo_Product__c oDemoProduct : lstDemoProduct){
            oDemoProduct.OwnerId = lstUser_MITG[0].Id;
            Movement_Request__c oMovementRequest = new Movement_Request__c();
                oMovementRequest.Demo_Product_Name__c = oDemoProduct.Id;
                oMovementRequest.Opportunity__c = clsTestData.oMain_Opportunity.Id;
                oMovementRequest.Approval_Status__c = 'Pending';            
            lstMovementRequest.add(oMovementRequest);
        }
        update lstDemoProduct;

        if (bCreateData){
            insert lstMovementRequest;
        }
    }   

    @isTest static void test_validateProductLoanToOtherHospital_Insert1() {
        
        bCreateData = false;
        createTestData();

        Test.startTest();

        insert lstMovementRequest;

        Test.stopTest();
    
    }

    @isTest static void test_validateProductLoanToOtherHospital_Insert2() {
        
        Boolean bError = false;
        createTestData();

        Test.startTest();

        Movement_Request__c oMovementRequest = new Movement_Request__c();
            oMovementRequest.Demo_Product_Name__c = lstMovementRequest[0].Demo_Product_Name__c;
            oMovementRequest.Hospital__c = lstMovementRequest[0].Hospital__c;
            oMovementRequest.Collect_product_at_Hospital__c = lstMovementRequest[0].Collect_product_at_Hospital__c;
            oMovementRequest.Contact_Name__c = lstMovementRequest[0].Contact_Name__c;
            oMovementRequest.Approval_Status__c = lstMovementRequest[0].Approval_Status__c;
            oMovementRequest.Start_Date__c = lstMovementRequest[0].End_Date__c.addDays(-10);
            oMovementRequest.End_Date__c = oMovementRequest.Start_Date__c.addDays(60);
        try{
            // Start Date (New) < End Date (Existing)
            insert oMovementRequest;
        }catch(Exception oEX){
            bError = true;
            System.assert(oEX.getMessage().contains('Demo Product is currently being loaned at another place'));
        }

        System.assertEquals(bError, true);


        Test.stopTest();
    
    }   

    @isTest static void test_validateProductLoanToOtherHospital_Insert3() {
        
        Boolean bError = false;
        createTestData();

        lstMovementRequest = 
            [
                SELECT 
                    Id, Demo_Product_Name__c, Hospital__c, Collect_product_at_Hospital__c, Contact_Name__c, Approval_Status__c
                    , Start_Date__c, End_Date__c, Start_Date_and_Time__c, End_Date_and_Time__c
                FROM 
                    Movement_Request__c
            ];

        Test.startTest();

        Movement_Request__c oMovementRequest = new Movement_Request__c();
            oMovementRequest.Demo_Product_Name__c = lstMovementRequest[0].Demo_Product_Name__c;
            oMovementRequest.Hospital__c = clsTestData.lstAccount[2].Id;
            oMovementRequest.Collect_product_at_Hospital__c = clsTestData.lstAccount[3].Id;
            oMovementRequest.Contact_Name__c = clsTestData.lstContact[1].Id;
            oMovementRequest.Approval_Status__c = 'Approved';
            oMovementRequest.Start_Date__c = lstMovementRequest[0].End_Date__c.addDays(10);
            oMovementRequest.End_Date__c = oMovementRequest.Start_Date__c.addDays(60);
            oMovementRequest.Start_Date_and_Time__c = lstMovementRequest[0].End_Date_and_Time__c.addDays(-10);
            oMovementRequest.End_Date_and_Time__c = lstMovementRequest[0].Start_Date_and_Time__c.addDays(10);

            System.assert(oMovementRequest.Start_Date__c > lstMovementRequest[0].End_Date__c);
            System.assert(oMovementRequest.Start_Date_and_Time__c < lstMovementRequest[0].End_Date_and_Time__c);
            System.assert(oMovementRequest.End_Date_and_Time__c > lstMovementRequest[0].Start_Date_and_Time__c);

        try{
            // End Date (New) < Start Data (Existing)
            // Start Date Time (New) < End Date Time (Existing) && End Date Time (New) > Start Date Time (Existing)
            insert oMovementRequest;
        }catch(Exception oEX){
            bError = true;
            System.assert(oEX.getMessage().contains('Demo Product is currently being loaned at another place'));
        }

        System.assertEquals(bError, true);


        Test.stopTest();
    
    }   

    @isTest static void test_validateProductLoanToOtherHospital_Insert4() {
        
        Boolean bError = false;
        createTestData();

        Test.startTest();

        Movement_Request__c oMovementRequest = new Movement_Request__c();
            oMovementRequest.Demo_Product_Name__c = lstMovementRequest[0].Demo_Product_Name__c;
            oMovementRequest.Hospital__c = lstMovementRequest[0].Hospital__c;
            oMovementRequest.Collect_product_at_Hospital__c = lstMovementRequest[0].Collect_product_at_Hospital__c;
            oMovementRequest.Contact_Name__c = lstMovementRequest[0].Contact_Name__c;
            oMovementRequest.Approval_Status__c = lstMovementRequest[0].Approval_Status__c;
            oMovementRequest.Start_Date__c = lstMovementRequest[0].End_Date__c.addDays(10);
            oMovementRequest.End_Date__c = oMovementRequest.Start_Date__c.addDays(10);
            oMovementRequest.Start_Date_and_Time__c = lstMovementRequest[0].End_Date_and_Time__c.addDays(0);
            oMovementRequest.End_Date_and_Time__c = lstMovementRequest[0].Start_Date_and_Time__c.addDays(0);
            oMovementRequest.Reloan__c = true;

            System.assert(oMovementRequest.Start_Date__c >= lstMovementRequest[0].End_Date__c);
            System.assert(oMovementRequest.Start_Date_and_Time__c >= lstMovementRequest[0].End_Date_and_Time__c);
            System.assert(oMovementRequest.End_Date_and_Time__c <= lstMovementRequest[0].Start_Date_and_Time__c);
            System.assert(oMovementRequest.Start_Date__c.addDays(-30) < lstMovementRequest[0].End_Date__c);
        try{
            // Start Date (New) < End Date (Existing) + 30
            insert oMovementRequest;
        }catch(Exception oEX){
            bError = true;
            System.assert(oEX.getMessage().contains('Demo Product cannot be loaned more than one time'));
        }

        System.assertEquals(bError, true);

        Test.stopTest();
    
    }   

    @isTest static void test_addProductManagerToOpportunityTeam() {
        
        Boolean bError = false;

        bCreateData = false;
        createTestDataWithProductManager();

        List<OpportunityTeamMember> lstOpportunityTeamMember = [SELECT Id FROM OpportunityTeamMember WHERE OpportunityId =:clsTestData.oMain_Opportunity.Id];

        Integer iOpportuntyTeamMember1 = lstOpportunityTeamMember.size();
        System.debug('**BC** lstOpportunityTeamMember.size() 1 : ' + lstOpportunityTeamMember.size());

        Test.startTest();

            insert lstMovementRequest;

        Test.stopTest();

        lstOpportunityTeamMember = [SELECT Id FROM OpportunityTeamMember WHERE OpportunityId =:clsTestData.oMain_Opportunity.Id];

        Integer iOpportuntyTeamMember2 = lstOpportunityTeamMember.size();
        System.debug('**BC** lstOpportunityTeamMember.size() 2 : ' + lstOpportunityTeamMember.size());
        System.assertEquals(iOpportuntyTeamMember2, iOpportuntyTeamMember1 + 1);

    }

}
//--------------------------------------------------------------------------------------------------------------------