/**
 *      Created Date : 20121113
 *      Description : This class is used to archive remove Account Call Topic Count records older than 1 year
 * 
 *      Authors = Dheeraj Gupta, Rudy De Coninck
 */
global class ba_AccountCallTopicArchive implements Schedulable, Database.Batchable<SObject> {
	
        global void execute(SchedulableContext ctx){        
            // Start Batch Apex job        
            Database.executeBatch(new ba_AccountCallTopicArchive(),200);    
        } 

        global Database.QueryLocator start(Database.BatchableContext ctx){    
			String query='Select a.ID,a.active__c,Visit_Date__c From Account_Call_Topic_Count__c a';
            return Database.getQueryLocator(query);      
        } 

        global void execute(Database.BatchableContext ctx, List<SObject> records){  

            // Select all Account_Call_Topic_Count__c records which are older then 365 days
		 	Date currentdate = system.Today();                  
			currentdate = date.valueOf(currentdate - 365);         	

           Account_Call_Topic_Count__c ACTCount;
           list<Account_Call_Topic_Count__c> lstACTCounts = new list<Account_Call_Topic_Count__c>(); 
           // Get Query records one by one
           for(SObject record : records){     
	            ACTCount =(Account_Call_Topic_Count__c) record; 
           		System.Debug('ACTCount - ' + ACTCount.Id + ' Visit Date - ' + ACTCount.Visit_Date__c);                     
	            if(ACTCount.Visit_Date__c < currentdate){
					// Creating records so & make flag as false
	    	        Account_Call_Topic_Count__c ACTC = new  Account_Call_Topic_Count__c(id=ACTCount.Id);
	    	        lstACTCounts.add(ACTC);
	            } 
           }
           
           // Updating the list if its size is > 0
           if(lstACTCounts.size()>0){
           		delete lstACTCounts;
           }
        	
        }	

        global void finish(Database.BatchableContext ctx){
        	    
        }            
	
}