@isTest
private class Test_ba_CVG_Agent_Team_Member {
        
    private static testmethod void runBatch(){
    	
    	Contact portalContact;
    	
    	System.runAs(new User(Id = UserInfo.getUserId())){
    		
    		clsTestData_MasterData.createSubBusinessUnit();
    		
    		Account portalAccount = new Account();
			portalAccount.Name = 'Test Portal Account';						
			insert portalAccount;
			
			//Create contact
			portalContact = new Contact();
			portalContact.FirstName = 'Test';
			portalContact.Lastname = 'Portal User';
			portalContact.AccountId = portalAccount.Id;
			portalContact.RecordTypeId = [Select Id from RecordType where SObjectType = 'Contact' AND DeveloperName = 'MDT_Agent_Distributor'].Id;
			insert portalContact;
    	}
    	
    	Id agentProfileId = [Select Id from Profile where Name = 'EUR Field Agent CVG'].Id;
    	
    	User CVGAgent = clsTestData_User.createUser('cvg.agent@medtronic.com.unittest', 'utAlias', 'Europe', 'Italy', 'ITALY', agentProfileId, null, false);
    	CVGAgent.ContactId = portalContact.Id;
    	insert CVGAgent;
    	
    	Sub_Business_Units__c pvEmbo = [Select Id, Company_Id__c from Sub_Business_Units__c where name = 'Coro + PV'];
    	
    	Therapy_Group__c perifVascular = new Therapy_Group__c();
    	perifVascular.Name = 'Peripheral Vascular';
    	perifVascular.Sub_Business_Unit__c = pvEmbo.Id;
    	perifVascular.Company__c = pvEmbo.Company_Id__c;    	
    	insert perifVascular;
    	
    	Account acc = new Account();
    	acc.Name = 'Test Account';
    	acc.Account_Country_vs__c = 'BELGIUM';
    	acc.SAP_Id__c = '123456789';
    	insert acc;
    	
    	Account_Team_Member__c ATM = new Account_Team_Member__c();
    	ATM.Account__c = acc.Id;
    	ATM.User__c = CVGAgent.Id;
    	ATM.Therapy_Group__c = perifVascular.Id;
    	ATM.Primary__c = true;
    	insert ATM;
    	
    	Account_Plan_2__c accPlan = new Account_Plan_2__c();
    	accPlan.Account__c = acc.Id;
		accPlan.Account_Plan_Level__c = 'Sub Business Unit';
		accPlan.Sub_Business_Unit__c = pvEmbo.Id;
		insert accPlan;
    	
    	Opportunity opp = new Opportunity();
    	opp.Name = 'Test Opportunity';
		opp.AccountId = acc.Id;
		opp.RecordTypeId = [Select Id from RecordType where SObjectType = 'Opportunity' AND DeveloperName = 'CVG_Project'].Id;
		opp.StageName = 'Identify';
		opp.CloseDate = Date.Today().addMonths(2);
		opp.Sub_Business_Unit_Id__c = pvEmbo.Id;
		insert opp;
    	
    	Test.startTest();
    	
    	Database.executeBatch(new ba_CVG_Agent_Team_Member(), 50);
    	
    	Test.stopTest();
    	
    	List<Account_Plan_Team_Member__c> APTM = [Select Id, Auto_Created__c from Account_Plan_Team_Member__c where Account_Plan__c = :accPlan.Id AND Team_Member__c = :CVGAgent.Id];
    	System.assert(APTM.size() == 1);
    	System.assert(APTM[0].Auto_Created__c == true);
    	
    	List<OpportunityTeamMember> OTM = [Select Id, Auto_Created__c from OpportunityTeamMember where OpportunityId = :opp.Id AND UserId = :CVGAgent.Id];
    	System.assert(OTM.size() == 1);
    	System.assert(OTM[0].Auto_Created__c == true);
    }
}