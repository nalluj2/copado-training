//---------------------------------------------------------------------------------------------------------------------------------
//	Author 			: Bart Caelen
//  Created Date 	: 30-04-2018
//  Description 	: APEX Class - Business Logic for the APEX Trigger tr_Campaign
//  Change Log 		: 
//---------------------------------------------------------------------------------------------------------------------------------
public class bl_Campaign_Trigger {

	//-----------------------------------------------------------------------------------------------------------------------------
	// Update the Business Unit Lookup on Campaign based on the Business Unit Picklist value
	//-----------------------------------------------------------------------------------------------------------------------------
	public static void updateBusinessUnit(List<Campaign> lstTriggerNew, Map<Id, Campaign> mapTriggerOld){

        if (bl_Trigger_Deactivation.isTriggerDeactivated('camp_updateBusinessUnit')) return;

		Set<Id> setID_RecordType_Campaign = new Set<Id>{
			clsUtil.getRecordTypeByDevName('Campaign', 'Europe_xBU').Id
			, clsUtil.getRecordTypeByDevName('Campaign', 'CVG_Campaign').Id
			, clsUtil.getRecordTypeByDevName('Campaign', 'RTG_Campaign').Id
			, clsUtil.getRecordTypeByDevName('Campaign', 'Diabetes_Campaign').Id
		};

		List<Campaign> lstCampaign_Process = new List<Campaign>();
		for (Campaign oCampaign : lstTriggerNew){

			if (setID_RecordType_Campaign.contains(oCampaign.RecordTypeId)){

				if (mapTriggerOld != null){

					// Fields not changed - continue to next record
					if (
						(oCampaign.Business_Unit_Picklist__c == mapTriggerOld.get(oCampaign.Id).Business_Unit_Picklist__c)
						&& (oCampaign.Business_Unit__c == mapTriggerOld.get(oCampaign.Id).Business_Unit__c)
					){
						continue;
					}

				}

				lstCampaign_Process.add(oCampaign);

			}

		}

		if (lstCampaign_Process.size() > 0){

			bl_Campaign.updateBusinessUnit(lstCampaign_Process, false);

		}

	}
	//-----------------------------------------------------------------------------------------------------------------------------


	//-----------------------------------------------------------------------------------------------------------------------------
	// Process new created Campaigns and create an Event linked to a Public Calendar based on the Setting Data in Campaign_Calendar__mdt.
	//-----------------------------------------------------------------------------------------------------------------------------
	public static void createCalendarEvent(List<Campaign> lstTriggerNew){

        if (bl_Trigger_Deactivation.isTriggerDeactivated('camp_createCalendarEvent')) return;

        List<Event> lstEvent_Insert = new List<Event>();

		for (Campaign oCampaign : lstTriggerNew){

			Id id_Calendar = bl_Campaign.getCalendarIdForCampaign(oCampaign);

			if (id_Calendar != null){

				Event oEvent = bl_Campaign.createEvent(oCampaign, id_Calendar, null);
				if (oEvent != null) lstEvent_Insert.add(oEvent);

			}

		}

		if (lstEvent_Insert.size() > 0) insert lstEvent_Insert;

	}
	//-----------------------------------------------------------------------------------------------------------------------------


	//-----------------------------------------------------------------------------------------------------------------------------
	// Processed updated Campaigns and create, update or delete an Event linked to a Public Calendar based on the Setting Data in Campaign_Calendar__mdt.
	//	This Event can be created if the Campaign was not matching the settings in Campaign_Calendar__mdt before.
	//	This Event can be deleted if the Campaign was matching the settings in Campaign_Calendar__mdt but not anymore.
	// 	This Event can be updated if the Campaign was matching the settings in Campaign_Calendar__mdt and is still matching the settings in Campaign_Calendar__mdt.
	//-----------------------------------------------------------------------------------------------------------------------------
	public static void updateCalendarEvent(Map<Id, Campaign> mapTriggerNew, Map<Id, Campaign> mapTriggerOld){

        if (bl_Trigger_Deactivation.isTriggerDeactivated('camp_updateCalendarEvent')) return;

        Map<Id, Id> mapCampaignId_CalendarId_Delete = new Map<Id, Id>();
        Map<Id, Id> mapCampaignId_CalendarId_Insert = new Map<Id, Id>();
        Map<Id, Map<Id, Id>> mapCampaignId_CalendarId_Update = new Map<Id, Map<Id, Id>>();

		for (Campaign oCampaign : mapTriggerNew.values()){

			Campaign oCampaign_Old = mapTriggerOld.get(oCampaign.Id);

			Id id_Calendar = bl_Campaign.getCalendarIdForCampaign(oCampaign);
			Id id_Calendar_Old = bl_Campaign.getCalendarIdForCampaign(oCampaign_Old);

			if (id_Calendar == null){

				if (id_Calendar_Old != null){

					// Delete Event because the Campaign doesn't need to be linked to an Event anymore
					mapCampaignId_CalendarId_Delete.put(oCampaign.Id, id_Calendar_Old);

				}

			}else{

				if (id_Calendar_Old == null){

					// Create Event because the Campaign needs to be linked to an Event while it wasn't linked before
					mapCampaignId_CalendarId_Insert.put(oCampaign.Id, id_Calendar);

				}else{

					// Update Event
					Map<Id, Id> mapCalendarId = new Map<Id, Id>();
					mapCalendarId.put(id_Calendar_Old, id_Calendar);
					mapCampaignId_CalendarId_Update.put(oCampaign.Id, mapCalendarId);

				}


			}

		}

		if (mapCampaignId_CalendarId_Delete.size() > 0){

			List<Event> lstEvent_Delete = [SELECT Id, WhatId, OwnerId FROM Event WHERE WhatId = :mapCampaignId_CalendarId_Delete.keySet() AND OwnerId = :mapCampaignId_CalendarId_Delete.values()];
			
			if (lstEvent_Delete.size() > 0) delete lstEvent_Delete;

		}	
		
		if (mapCampaignId_CalendarId_Insert.size() > 0){

			List<Event> lstEvent_Insert = new List<Event>();
			for (Id id_Campaign : mapCampaignId_CalendarId_Insert.keySet()){
		
				Campaign oCampaign = mapTriggerNew.get(id_Campaign);
				Event oEvent = bl_Campaign.createEvent(oCampaign, mapCampaignId_CalendarId_Insert.get(id_Campaign), null);
				if (oEvent != null) lstEvent_Insert.add(oEvent);
		
			}

			if (lstEvent_Insert.size() > 0) insert lstEvent_Insert;

		}	

		if (mapCampaignId_CalendarId_Update.size() > 0){

			List<Map<Id, Id>> lstCalendarId = mapCampaignId_CalendarId_Update.values();
			Set<Id> setCalendarId_Old = new set<Id>();
			for (Map<Id, Id> mapCalendarId_Old_New : lstCalendarId){
    			setCalendarId_Old.addAll(mapCalendarId_Old_New.keySet());
			}	

			List<Event> lstEvent = [SELECT Id, Subject, Description, WhatId, OwnerId, StartDateTime, EndDateTime FROM Event WHERE WhatId = :mapCampaignId_CalendarId_Update.keySet() AND OwnerId = :setCalendarId_Old];
			List<Event> lstEvent_Update = new List<Event>();
			for (Event oEvent : lstEvent){

				Map<Id, Id> mapCalendarId = mapCampaignId_CalendarId_Update.get(oEvent.WhatId);
				Id id_Calendar = mapCalendarId.values()[0];

				Campaign oCampaign = mapTriggerNew.get(oEvent.WhatId);
				Event oEvent_Updated = bl_Campaign.createEvent(oCampaign, id_Calendar, oEvent);
				if (oEvent_Updated != null) lstEvent_Update.add(oEvent_Updated);

			}

			if (lstEvent_Update.size() > 0) update lstEvent_Update;

		}

	}
	//-----------------------------------------------------------------------------------------------------------------------------


	//-----------------------------------------------------------------------------------------------------------------------------
	// Prevent the creation of duplicate Campaign SBU records - the combination of SBU, Country and Fiscal Year needs to be unique
	//-----------------------------------------------------------------------------------------------------------------------------
	public static void preventDuplicateCampaign_SBU(List<Campaign> lstTriggerNew, Map<Id, Campaign> mapTriggerOld){

        if (bl_Trigger_Deactivation.isTriggerDeactivated('camp_preventDuplicateCampaign_SBU')) return;

		Set<Id> setID_RecordType = new Set<Id>{clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_SBU').Id};

		// Loop through the processing Campaigns and collect SBU, Country and Fiscal Year so that we can search for other existing records which have 
		//	the same values in these fields
		Set<Id> setID_Campaign = new Set<Id>();
		Set<String> setSBU = new Set<String>();
		Set<String> setCountry = new Set<String>();
		Set<String> setFiscalYear = new Set<String>();
		for (Campaign oCampaign : lstTriggerNew){

			if (setID_RecordType.contains(oCampaign.RecordTypeId)){

				if (mapTriggerOld != null){
				
					Campaign oCampaign_Old = mapTriggerOld.get(oCampaign.Id);

					if (
						(oCampaign.sBU_vs__c != oCampaign_Old.sBU_vs__c)
						|| (oCampaign.Campaign_Country_vs__c != oCampaign_Old.Campaign_Country_vs__c)
						|| (oCampaign.Fiscal_Year__c != oCampaign_Old.Fiscal_Year__c)
					){

						if (!String.isBlank(oCampaign.Id)) setID_Campaign.add(oCampaign.Id);
						if (!String.isBlank(oCampaign.sBU_vs__c)) setSBU.add(oCampaign.sBU_vs__c);
						if (!String.isBlank(oCampaign.Campaign_Country_vs__c)) setCountry.add(oCampaign.Campaign_Country_vs__c);
						if (!String.isBlank(oCampaign.Fiscal_Year__c)) setFiscalYear.add(oCampaign.Fiscal_Year__c);

					}
				
				}else{

					if (!String.isBlank(oCampaign.Id)) setID_Campaign.add(oCampaign.Id);
					if (!String.isBlank(oCampaign.sBU_vs__c)) setSBU.add(oCampaign.sBU_vs__c);
					if (!String.isBlank(oCampaign.Campaign_Country_vs__c)) setCountry.add(oCampaign.Campaign_Country_vs__c);
					if (!String.isBlank(oCampaign.Fiscal_Year__c)) setFiscalYear.add(oCampaign.Fiscal_Year__c);

				}

			}

		}

		if (setSBU.size() == 0 && setCountry.size() == 0 && setFiscalYear.size() == 0)  return;

		// Select existing Campaigns based on the collected SBU, Country and Fiscal Year but exclude the processing Campaigns
		List<Campaign> lstCampaign = 
			[
				SELECT 
					Id, Name, sBU_vs__c, Campaign_Country_vs__c, Fiscal_Year__c
				FROM 
					Campaign 
				WHERE 
					Id != :setID_Campaign
					AND sBU_vs__c = :setSBU
					AND Campaign_Country_vs__c = :setCountry
					AND Fiscal_Year__c = :setFiscalYear
			];

		// Create a map with the unique key combination (SBU, Country and Fiscal) and the Campaign
		Map<String, Campaign> mapKey_Campaign = new Map<String, Campaign>();
		for (Campaign oCampaign : lstCampaign){
			mapKey_Campaign.put(oCampaign.sBU_vs__c + oCampaign.Campaign_Country_vs__c + oCampaign.Fiscal_Year__c, oCampaign);
		}


		// Loop through the processing Campaign records and if the unique key combination (SBU, Country and Fiscal) exists in the mapKey_Campaign, 
		//	we need to throw an error to prevent duplciate records from being created
		for (Campaign oCampaign : lstTriggerNew){

			if (setID_RecordType.contains(oCampaign.RecordTypeId)){
				
				String tKey = oCampaign.sBU_vs__c + oCampaign.Campaign_Country_vs__c + oCampaign.Fiscal_Year__c;
				if (mapKey_Campaign.containsKey(tKey)){
				
					// Throw Error
					Campaign oCampaign_Existing = mapKey_Campaign.get(tKey);
					oCampaign.addError('There is already a Campaign SBU with the same sBU, Country and Fiscal Year : <a href="' + Url.getSalesforceBaseUrl().toExternalForm() + '/' + oCampaign_Existing.Id + '">' + oCampaign_Existing.Name +'</a>', false);

				}

			}

		}

	}
	//-----------------------------------------------------------------------------------------------------------------------------


	//----------------------------------------------------------------------------------------------------------------
	// Populate the picklist Business Unit Group on Campaign with the Business Unit Group Name of the selected Sub Business Unit (for Campaign_SBU Campaigns)
	// Populate the picklist Business Unit Group on Campaign with the Business Unit Group of the Parent Campaign (for Campaign_Goal, Campaign_Strategy, Campaign_Tactic Campaigns)
	// Populate the picklist Country (Campaign_Country_vs__c) on Campaign with the Country (Campaign_Country_vs__c) of the Parent Campaign (for Campaign_Goal, Campaign_Strategy, Campaign_Tactic Campaigns)
	// Populate the picklist Sub Business Unit (sBU_vs__c) on Campaign with the Sub Business Unit (sBU_vs__c) of the Parent Campaign (for Campaign_Goal, Campaign_Strategy, Campaign_Tactic Campaigns)
	//----------------------------------------------------------------------------------------------------------------
	public static void populateFields(List<Campaign> lstTriggerNew, Map<Id, Campaign> mapTriggerOld){

        if (bl_Trigger_Deactivation.isTriggerDeactivated('camp_populateFields')) return;

		Set<Id> setID_RecordType_SBU = new Set<Id>{clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_SBU').Id};
		Set<Id> setID_RecordType_CopyFromParent = new Set<Id>{clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_Goal').Id, clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_Strategy').Id, clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_Tactic').Id};

		Set<String> setSBU = new Set<String>();
		Set<Id> setID_Parent = new Set<Id>();
		for (Campaign oCampaign : lstTriggerNew){

			if (setID_RecordType_SBU.contains(oCampaign.RecordTypeId)){

				setSBU.add(oCampaign.sBU_vs__c);

			}else if (setID_RecordType_CopyFromParent.contains(oCampaign.RecordTypeId)){

				setID_Parent.add(oCampaign.ParentId);

			}

		}

		if (setSBU.size() > 0){

			Map<String, Sub_Business_Units__c> mapSBU = new Map<String, Sub_Business_Units__c>();
			for (Sub_Business_Units__c oSBU : [SELECT Id, Name, Business_Unit__r.Business_Unit_Group__r.Name FROM Sub_Business_Units__c WHERE Company_Code__c = 'EUR' AND Name = :setSBU]){
				mapSBU.put(oSBU.Name, oSBU);
			}

			for (Campaign oCampaign : lstTriggerNew){

				if (setID_RecordType_SBU.contains(oCampaign.RecordTypeId)){

					if (mapSBU.containsKey(oCampaign.sBU_vs__c)){

						Sub_Business_Units__c oSBU = mapSBU.get(oCampaign.sBU_vs__c);

						if (mapTriggerOld != null){

							if (oCampaign.Business_Unit_Group__c != oSBU.Business_Unit__r.Business_Unit_Group__r.Name) oCampaign.Business_Unit_Group__c = oSBU.Business_Unit__r.Business_Unit_Group__r.Name;

						}else{

							oCampaign.Business_Unit_Group__c = oSBU.Business_Unit__r.Business_Unit_Group__r.Name;

						}

					}

				}
		
			}

		}

		if(setID_Parent.size() > 0){

			Map<Id, Campaign> mapCampaignParent = new Map<Id, Campaign>([SELECT Id, Business_Unit_Group__c, Campaign_Country_vs__c, sBU_vs__c FROM Campaign WHERE Id = :setID_Parent]);

			for (Campaign oCampaign : lstTriggerNew){

				if (setID_RecordType_CopyFromParent.contains(oCampaign.RecordTypeId)){

					if (mapCampaignParent.containsKey(oCampaign.ParentId)){
					
						Campaign oCampaign_Parent = mapCampaignParent.get(oCampaign.ParentId);

						if (mapTriggerOld != null){

							if (oCampaign.Business_Unit_Group__c != oCampaign_Parent.Business_Unit_Group__c) oCampaign.Business_Unit_Group__c = oCampaign_Parent.Business_Unit_Group__c;
							if (oCampaign.Campaign_Country_vs__c != oCampaign_Parent.Campaign_Country_vs__c) oCampaign.Campaign_Country_vs__c = oCampaign_Parent.Campaign_Country_vs__c;
							if (oCampaign.sBU_vs__c != oCampaign_Parent.sBU_vs__c) oCampaign.sBU_vs__c = oCampaign_Parent.sBU_vs__c;

						}else{

							oCampaign.Business_Unit_Group__c = oCampaign_Parent.Business_Unit_Group__c;
							oCampaign.Campaign_Country_vs__c = oCampaign_Parent.Campaign_Country_vs__c;
							oCampaign.sBU_vs__c = oCampaign_Parent.sBU_vs__c;

						}

					}
		
				}

			}

		}

	}
	//----------------------------------------------------------------------------------------------------------------


	//----------------------------------------------------------------------------------------------------------------
	// Validation Rules related to Campaigns
	//----------------------------------------------------------------------------------------------------------------
	public static void validateCampaign(List<Campaign> lstTriggerNew, Map<Id, Campaign> mapTriggerOld){

		if (bl_Trigger_Deactivation.isTriggerDeactivated('camp_validateCampaign')) return;

		Id id_RecordType_Campaign_Goal = clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_Goal').Id;
		Id id_RecordType_Campaign_Strategy = clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_Strategy').Id;
		Id id_RecordType_Campaign_Tactic = clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_Tactic').Id;

		List<Campaign> lstCampaign_Goal = new List<Campaign>();
		List<Campaign> lstCampaign_Strategy = new List<Campaign>();
		List<Campaign> lstCampaign_Tactic = new List<Campaign>();
		for (Campaign oCampaign : lstTriggerNew){

			if (oCampaign.RecordTypeId == id_RecordType_Campaign_Goal){

				lstCampaign_Goal.add(oCampaign);

			}else if (oCampaign.RecordTypeId == id_RecordType_Campaign_Strategy){
			
				lstCampaign_Strategy.add(oCampaign);

			}else if (oCampaign.RecordTypeId == id_RecordType_Campaign_Tactic){
				
				lstCampaign_Tactic.add(oCampaign);
			
			}

		}


		if (lstCampaign_Goal.size() > 0) validateCampaign_Goal(lstCampaign_Goal, mapTriggerOld);
		if (lstCampaign_Strategy.size() > 0) validateCampaign_Strategy(lstCampaign_Strategy, mapTriggerOld);
		if (lstCampaign_Tactic.size() > 0) validateCampaign_Tactic(lstCampaign_Tactic, mapTriggerOld);
		
	}
	//----------------------------------------------------------------------------------------------------------------


	//----------------------------------------------------------------------------------------------------------------
	// Validation Rules related to Goal Campaigns
	//----------------------------------------------------------------------------------------------------------------
	public static void validateCampaign_Goal(List<Campaign> lstTriggerNew, Map<Id, Campaign> mapTriggerOld){

		if (bl_Trigger_Deactivation.isTriggerDeactivated('camp_validateCampaign_Goal')) return;

		Set<Id> setID_RecordType = new Set<Id>{clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_Goal').Id};


		Set<Id> setID_ParentCampaign = new Set<Id>();
		for (Campaign oCampaignGoal : lstTriggerNew){
		
			if (setID_RecordType.contains(oCampaignGoal.RecordTypeId)){

				if ( (oCampaignGoal.ParentId != null) && (oCampaignGoal.Therapy_vs__c != null) ) setID_ParentCampaign.add(oCampaignGoal.ParentId);

			}

		}
		
		if (setID_ParentCampaign.size() == 0) return;

		// Get the Parent Campaign with the Sub Business Unit
		Map<Id, Campaign> mapParentCampaign = new Map<Id, Campaign>([SELECT Id, sBU_vs__c FROM Campaign WHERE Id = :setID_ParentCampaign AND sBU_vs__c != null]);

		// Get the SBU of the Parent Campaigns
		Set<String> setSBU = new Set<String>();
		for (Campaign oCampaignParent : mapParentCampaign.values()) setSBU.add(oCampaignParent.sBU_vs__c);

		// Get the Therapies of the collected SBU's
		List<Therapy__c> lstTherapy = [SELECT Id, Name, Sub_Business_Unit__r.Name FROM Therapy__c WHERE Sub_Business_Unit__r.Name = :setSBU AND Sub_Business_Unit__r.Company_Code__c = 'EUR' ORDER BY Sub_Business_Unit__c];
		Map<String, Set<String>> mapSBU_Therapy = new Map<String, Set<String>>();
		for (Therapy__c oTherapy : lstTherapy){

			Set<String> setTherapy_Tmp = new Set<String>();
			if (mapSBU_Therapy.containsKey(oTherapy.Sub_Business_Unit__r.Name)) setTherapy_Tmp = mapSBU_Therapy.get(oTherapy.Sub_Business_Unit__r.Name);
			setTherapy_Tmp.add(oTherapy.Name);
			mapSBU_Therapy.put(oTherapy.Sub_Business_Unit__r.Name, setTherapy_Tmp);

		}

		for (Campaign oCampaignGoal : lstTriggerNew){
		
			if (setID_RecordType.contains(oCampaignGoal.RecordTypeId)){

				if ( (oCampaignGoal.ParentId != null) && (oCampaignGoal.Therapy_vs__c != null) ){
					
					// Get the Parent Campaign of the processing Campaign
					Campaign oCampaignParent = mapParentCampaign.get(oCampaignGoal.ParentId);

					// Get the Therapies related to the SBU of the Parent Campaign
					Set<String> setTherapy_SBU = mapSBU_Therapy.get(oCampaignParent.sBU_vs__c);

					// Verify that the selected Therapies on the Goal Campaign are all included in the Therapies of the SBU of the Parent Campaign - if not throw an error
					Set<String> setTherapy_CampaignGoal = new Set<String>();
					setTherapy_CampaignGoal.addAll(oCampaignGoal.Therapy_vs__c.split(';'));
					if (!setTherapy_SBU.containsAll(setTherapy_CampaignGoal)){

						String tError_Therapy = '';
						for (String tTherapy_Tmp : setTherapy_CampaignGoal){

							if (!setTherapy_SBU.contains(tTherapy_Tmp)) tError_Therapy += tTherapy_Tmp + ',';

						}
						tError_Therapy = tError_Therapy.left(tError_Therapy.length() - 1);
						oCampaignGoal.Therapy_vs__c.addError('You can only select Therapies that are linked to the Sub Business Unit of the Parent Campaign.  The following Therapies are not allowed : ' + tError_Therapy);

					}
				
				}

			}

		}
		
	}
	//----------------------------------------------------------------------------------------------------------------


	//----------------------------------------------------------------------------------------------------------------
	// Validation Rules related to Strategy Campaigns
	//----------------------------------------------------------------------------------------------------------------
	public static void validateCampaign_Strategy(List<Campaign> lstTriggerNew, Map<Id, Campaign> mapTriggerOld){

		if (bl_Trigger_Deactivation.isTriggerDeactivated('camp_validateCampaign_Strategy')) return;

		Set<Id> setID_RecordType = new Set<Id>{clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_Strategy').Id};


		Set<Id> setID_ParentCampaign_Therapy = new Set<Id>();
		Set<Id> setID_ParentCampaign_Strategy = new Set<Id>();
		for (Campaign oCampaignStrategy: lstTriggerNew){
		
			if (setID_RecordType.contains(oCampaignStrategy.RecordTypeId)){

				if (oCampaignStrategy.ParentId != null){
					
					if (oCampaignStrategy.Therapy_vs__c != null) setID_ParentCampaign_Therapy.add(oCampaignStrategy.ParentId);
					if (oCampaignStrategy.Strategy__c != null) setID_ParentCampaign_Strategy.add(oCampaignStrategy.ParentId);

				}

			}

		}

		// Validation on selected Therapy
		if (setID_ParentCampaign_Therapy.size() > 0){

			// Get the Parent Campaign with the selected Therapies
			Map<Id, Campaign> mapParentCampaign = new Map<Id, Campaign>([SELECT Id, Therapy_vs__c FROM Campaign WHERE Id = :setID_ParentCampaign_Therapy]);

			// Verify that the selected Therapies on the processing Therapy Campaign are also selected on the Parent Campaign (Goal Campaign)	
			for (Campaign oCampaignStrategy: lstTriggerNew){
		
				if (setID_RecordType.contains(oCampaignStrategy.RecordTypeId)){

					if ( (oCampaignStrategy.ParentId != null) && (oCampaignStrategy.Therapy_vs__c != null) ){

						// Get the Parent Campaign of the processing Campaign
						Campaign oCampaignParent = mapParentCampaign.get(oCampaignStrategy.ParentId);

						// Get the selected Therapies for the processing Therapy Canpaign and the Parent Campaign
						Set<String> setTherapy_Parent = new Set<String>();
						setTherapy_Parent.addAll(oCampaignParent.Therapy_vs__c.split(';'));
						Set<String> setTherapy_Strategy = new Set<String>();
						setTherapy_Strategy.addAll(oCampaignStrategy.Therapy_vs__c.split(';'));

						// If the processing Therapy Campaign contains a Therapy that is not on defined on the Parent Campaign, we need to throw an error
						if (!setTherapy_Parent.containsAll(setTherapy_Strategy)){

							String tError_Therapy = '';
							for (String tTherapy_Tmp : setTherapy_Strategy){

								if (!setTherapy_Parent.contains(tTherapy_Tmp)) tError_Therapy += tTherapy_Tmp + ',';

							}
							tError_Therapy = tError_Therapy.left(tError_Therapy.length() - 1);
							oCampaignStrategy.Therapy_vs__c.addError('You can only select Therapies that are selected on the Parent Campaign.  The following Therapies are not allowed : ' + tError_Therapy);
					
						}

					}

				}

			}

		}


		// Validation on selected Strategy
		if (setID_ParentCampaign_Strategy.size() > 0){

			// Get the Goal ID's of the Parent Goal Campaign
			Map<Id, Campaign> mapParentCampaign = new Map<Id, Campaign>([SELECT Id, Goal__c FROM Campaign WHERE Id = :setID_ParentCampaign_Strategy AND Goal__c != null]);
			Set<Id> setID_Goal = new Set<Id>();
			for (Campaign oCampaign : mapParentCampaign.values()){
				setID_Goal.add(oCampaign.Goal__c);
			}
	
			// Get the Strategy ID's of the collected Goal records
			List<Strategy__c> lstStrategy = [SELECT Id, Goal__c FROM Strategy__c WHERE Goal__c = :setID_Goal AND Active__c = true ORDER By Goal__c];

			// Get a Map of Goal ID and the related Strategy records
			Map<Id, Set<Id>> mapGoalId_StrategyId = new Map<Id, Set<Id>>();
			for (Strategy__c oStrategy : lstStrategy){

				Set<Id> setID_Strategy = new Set<Id>();
				if (mapGoalId_StrategyId.containsKey(oStrategy.Goal__c)) setID_Strategy = mapGoalId_StrategyId.get(oStrategy.Goal__c);
				setID_Strategy.add(oStrategy.Id);
				mapGoalId_StrategyId.put(oStrategy.Goal__c, setID_Strategy);

			}
			
			// Get a map of the Parent Goal Campaign with the related Strategy records of the Goal on that Campaign
			Map<Id, Set<Id>> mapParentCampaignId_StrategyId = new Map<Id, Set<Id>>();
			for (Campaign oCampaign : mapParentCampaign.values()){

				Set<Id> setID_Strategy = new Set<Id>();
 				if (mapParentCampaignId_StrategyId.containsKey(oCampaign.Id)) setID_Strategy = mapParentCampaignId_StrategyId.get(oCampaign.Id);
				
				if (mapGoalId_StrategyId.containsKey(oCampaign.Goal__c)){
					setID_Strategy.addAll(mapGoalId_StrategyId.get(oCampaign.Goal__c));
				}
				mapParentCampaignId_StrategyId.put(oCampaign.Id, setID_Strategy);

			}


			// Verify that the selected Therapies on the processing Therapy Campaign are also selected on the Parent Campaign (Goal Campaign)	
			for (Campaign oCampaignStrategy: lstTriggerNew){
		
				if (setID_RecordType.contains(oCampaignStrategy.RecordTypeId)){

					if ( (oCampaignStrategy.ParentId != null) && (oCampaignStrategy.Strategy__c != null) ){

						Set<Id> setID_Strategy_Parent = mapParentCampaignId_StrategyId.get(oCampaignStrategy.ParentId);

						if (!setID_Strategy_Parent.contains(oCampaignStrategy.Strategy__c)){

							oCampaignStrategy.Strategy__c.addError('You can only select an active Strategy which is linked to the Goal of the Parent Campaign.');

						}

					}

				}

			}

		}

	}


	//----------------------------------------------------------------------------------------------------------------
	// Validation Rules related to Tactic Campaigns
	//----------------------------------------------------------------------------------------------------------------
	public static void validateCampaign_Tactic(List<Campaign> lstTriggerNew, Map<Id, Campaign> mapTriggerOld){

		if (bl_Trigger_Deactivation.isTriggerDeactivated('camp_validateCampaign_Tactic')) return;

		Set<Id> setID_RecordType = new Set<Id>{clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_Tactic').Id};


		Set<Id> setID_ParentCampaign_Therapy = new Set<Id>();
		Set<Id> setID_ParentCampaign_Tactic = new Set<Id>();
		for (Campaign oCampaignTactic: lstTriggerNew){
		
			if (setID_RecordType.contains(oCampaignTactic.RecordTypeId)){

				if (oCampaignTactic.ParentId != null){
					
					if (oCampaignTactic.Therapy_vs__c != null) setID_ParentCampaign_Therapy.add(oCampaignTactic.ParentId);
					if (oCampaignTactic.Tactic__c != null) setID_ParentCampaign_Tactic.add(oCampaignTactic.ParentId);

				}

			}

		}

		// Validation on selected Therapy
		if (setID_ParentCampaign_Therapy.size() > 0){

			// Get the Parent Campaign with the selected Therapies
			Map<Id, Campaign> mapParentCampaign = new Map<Id, Campaign>([SELECT Id, Therapy_vs__c FROM Campaign WHERE Id = :setID_ParentCampaign_Therapy]);

			// Verify that the selected Therapies on the processing Tactic Campaign are also selected on the Parent Campaign (Strategy Campaign)	
			for (Campaign oCampaignTactic: lstTriggerNew){
		
				if (setID_RecordType.contains(oCampaignTactic.RecordTypeId)){

					if ( (oCampaignTactic.ParentId != null) && (oCampaignTactic.Therapy_vs__c != null) ){

						// Get the Parent Campaign of the processing Campaign
						Campaign oCampaignParent = mapParentCampaign.get(oCampaignTactic.ParentId);

						// Get the selected Therapies for the processing Therapy Canpaign and the Parent Campaign
						Set<String> setTherapy_Parent = new Set<String>();
						if (oCampaignParent.Therapy_vs__c != null) setTherapy_Parent.addAll(oCampaignParent.Therapy_vs__c.split(';'));
						Set<String> setTherapy_Tactic = new Set<String>();
						if (oCampaignTactic.Therapy_vs__c != null) setTherapy_Tactic.addAll(oCampaignTactic.Therapy_vs__c.split(';'));

						// If the processing Therapy Campaign contains a Therapy that is not on defined on the Parent Campaign, we need to throw an error
						if (!setTherapy_Parent.containsAll(setTherapy_Tactic)){

							String tError_Therapy = '';
							for (String tTherapy_Tmp : setTherapy_Tactic){

								if (!setTherapy_Parent.contains(tTherapy_Tmp)) tError_Therapy += tTherapy_Tmp + ',';

							}
							tError_Therapy = tError_Therapy.left(tError_Therapy.length() - 1);
							oCampaignTactic.Therapy_vs__c.addError('You can only select Therapies that are selected on the Parent Campaign.  The following Therapies are not allowed : ' + tError_Therapy);
					
						}

					}

				}

			}

		}


		// Validation on selected Strategy
		if (setID_ParentCampaign_Tactic.size() > 0){

			// Get the Strategy ID's of the Parent Strategy Campaign
			Map<Id, Campaign> mapParentCampaign = new Map<Id, Campaign>([SELECT Id, Strategy__c FROM Campaign WHERE Id = :setID_ParentCampaign_Tactic AND Strategy__c != null]);
			Set<Id> setID_Strategy = new Set<Id>();
			for (Campaign oCampaign : mapParentCampaign.values()){
				setID_Strategy.add(oCampaign.Strategy__c);
			}
	
			// Get the Tactic ID's of the collected Strategy records
			List<Tactic__c> lstTactic = [SELECT Id, Strategy__c FROM Tactic__c WHERE Strategy__c = :setID_Strategy AND Active__c = true ORDER BY Strategy__c];

			// Get a Map of Strategy ID and the related Tactic ID's
			Map<Id, Set<Id>> mapStrategyId_TacticId = new Map<Id, Set<Id>>();
			for (Tactic__c oTactic : lstTactic){

				Set<Id> setID_Tactic = new Set<Id>();
				if (mapStrategyId_TacticId.containsKey(oTactic.Strategy__c)) setID_Tactic = mapStrategyId_TacticId.get(oTactic.Strategy__c);
				setID_Tactic.add(oTactic.Id);
				mapStrategyId_TacticId.put(oTactic.Strategy__c, setID_Tactic);

			}
			
			// Get a map of the Parent Strategy Campaign Id with the related Tactic records of the Strategy on that Campaign
			Map<Id, Set<Id>> mapParentCampaignId_TacticId = new Map<Id, Set<Id>>();
			for (Campaign oCampaign : mapParentCampaign.values()){

				Set<Id> setID_Tactic = new Set<Id>();
 				if (mapParentCampaignId_TacticId.containsKey(oCampaign.Id)) setID_Tactic = mapParentCampaignId_TacticId.get(oCampaign.Id);
				
				if (mapStrategyId_TacticId.containsKey(oCampaign.Strategy__c)){
					setID_Tactic.addAll(mapStrategyId_TacticId.get(oCampaign.Strategy__c));
				}
				mapParentCampaignId_TacticId.put(oCampaign.Id, setID_Tactic);

			}


			// Verify that the selected Tactics on the processing Tactic Campaign are also selected on the Parent Campaign (Strategy Campaign)	
			for (Campaign oCampaignTactic: lstTriggerNew){
		
				if (setID_RecordType.contains(oCampaignTactic.RecordTypeId)){

					if ( (oCampaignTactic.ParentId != null) && (oCampaignTactic.Tactic__c != null) ){

						Set<Id> setID_Tactic_Parent = mapParentCampaignId_TacticId.get(oCampaignTactic.ParentId);

						if (!setID_Tactic_Parent.contains(oCampaignTactic.Tactic__c)){

							oCampaignTactic.Tactic__c.addError('You can only select an active Tactic which is linked to the Strategy of the Parent Campaign.');

						}

					}

				}

			}

		}

	}


	//----------------------------------------------------------------------------------------------------------------
	// Set Campaign to Active / InActive based on the Status of the Campaign
	//----------------------------------------------------------------------------------------------------------------
	public static void setCampaignActiveStatus(List<Campaign> lstTriggerNew, Map<Id, Campaign> mapTriggerOld){

		if (bl_Trigger_Deactivation.isTriggerDeactivated('camp_validateCampaign_Tactic')) return;
		
		Set<Id> setID_RecordType = new Set<Id>{clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_SBU').Id, clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_Goal').Id, clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_Strategy').Id, clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_Tactic').Id};
		Set<String> setCampaignStatus_InActive = new Set<String>{'Aborted', 'Completed'};


		for (Campaign oCampaign : lstTriggerNew){

			if (setID_RecordType.contains(oCampaign.RecordTypeId)){

				if (setCampaignStatus_InActive.contains(oCampaign.Status)){	// The Campaign status is an Inactive status - set the Campaign to InActive

					if (oCampaign.IsActive == true) oCampaign.IsActive = false;
				
				}else{ 	// The Campaign status is an Active status - set the Campaign to Active

					if (oCampaign.IsActive == false) oCampaign.IsActive  = true;

				}

			}

		}

	}


}
//---------------------------------------------------------------------------------------------------------------------------------