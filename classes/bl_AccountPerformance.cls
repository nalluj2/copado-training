public class bl_AccountPerformance {
    
    public static User userData
                  { 
                    get
                    {
                      if(userData == null){
                        userData = [Select User_Business_Unit_vs__c, Job_Title_vs__c, Company_Code_Text__c, CountryOR__c  from User where id=:UserInfo.getUserId()];
                      }
                      return userData;
                    }
    
                    set;
                  }
    
    public static list<Account_Performance_Rule__c> getUserAccess(){
        id idCurrentUserId = UserInfo.getUserId();
        //string strBUs = '';
        string strQuery='';
        // Get User Business Unit & see if Useer has All BUs
        User usr=[select User_Business_Unit_vs__c, Job_Title_vs__c, Company_Code_Text__c  from User where id=:idCurrentUserId limit 1];
        list<Account_Performance_Rule__c> lstSecurityMessage = new list<Account_Performance_Rule__c>();

        list<CS_ACC_PER_xBU_Job_Title__c> lstCSACCPERxBU = [select Job_Title__c from CS_ACC_PER_xBU_Job_Title__c where Job_Title__c=:usr.Job_Title_vs__c];
        if(lstCSACCPERxBU.size()>0){
            lstSecurityMessage = [select Business_Unit__c,Business_Unit__r.name, Business_Unit__r.Company__c, Highest_level__c, Show_Account_for__c, View_Restriction__c, Security_Restriction_Message__c,Business_Unit__r.Business_Unit_Group__c 
            from Account_Performance_Rule__c 
            where Job_Title_vs__c=:usr.Job_Title_vs__c and company__r.Company_Code_Text__c=: usr.Company_Code_Text__c 
            order by Rule_Rank__c];         
        } else {
            set<id> setUserBUs = SharedMethods.getUserBusinessUnits().keyset();
            System.debug('User '+usr.Job_Title_vs__c+' UserSubIds'+setUserBUs+',companycode '+usr.Company_Code_Text__c+' ' );
            lstSecurityMessage = [select Business_Unit__c,Business_Unit__r.name, Business_Unit__r.Company__c, Highest_level__c, Show_Account_for__c, View_Restriction__c, Security_Restriction_Message__c,Business_Unit__r.Business_Unit_Group__c 
            from Account_Performance_Rule__c 
            where Job_Title_vs__c=:usr.Job_Title_vs__c and company__r.Company_Code_Text__c=: usr.Company_Code_Text__c 
            and Business_Unit__c=: setUserBUs order by Rule_Rank__c];
        }
        return lstSecurityMessage;
    }
    
    public static List<SelectOption> getShowDataPer(string strHighestLevel){
        List<SelectOption> optionList = new List<SelectOption>();
        if(strHighestLevel!=null){
            if(strHighestLevel=='Business Unit'){
              optionList.add(new SelectOption('Business Unit','Business Unit'));
              optionList.add(new SelectOption('Sub Business Unit','Sub Business Unit'));
              optionList.add(new SelectOption('Therapy','Therapy'));
              optionList.add(new SelectOption('Product Group','Product Group'));
            } else if(strHighestLevel=='Sub Business Unit') {
              optionList.add(new SelectOption('Sub Business Unit','Sub Business Unit'));
              optionList.add(new SelectOption('Therapy','Therapy'));
              optionList.add(new SelectOption('Product Group','Product Group'));
            } else if(strHighestLevel=='Therapy'){
              optionList.add(new SelectOption('Therapy','Therapy'));
              optionList.add(new SelectOption('Product Group','Product Group'));                                
            } else if(strHighestLevel=='Product Group'){
              optionList.add(new SelectOption('Product Group','Product Group'));                                                
            }
        }
        return optionList;  
    }

    public static String getuserCountry(){
        
        if(userData.CountryOR__c!=null && userData.CountryOR__c!=''){
            return userData.CountryOR__c;    
        } else {
            return null;
        }
    }

    public static string getAccountCountry(id accID){
        Account acc = [select Account_Country__c from Account where id=:accID limit 1];
        if(acc.Account_Country__c!=null && acc.Account_Country__c!=''){
            return acc.Account_Country__c;  
        } else {
            return null;
        }
    }
    
   
        
     public static map<id,set<id>> getBUCompany(){
        set<id> setBUIds = new set<id>();
        map<id,set<id>> mapCompanyBU  = new Map<Id, set<id>>();
        list<Business_Unit__c> lstBusiness = [SELECT Company__c, id From Business_Unit__c order by Company__c];
        id idCompanyName;
        for(Business_Unit__c BU:lstBusiness){
            if(idCompanyName!=BU.Company__c){
                setBUIds = new set<id>();   
            }
            setBUIds.add(BU.id);
            mapCompanyBU.put(BU.Company__c,setBUIds);
            idCompanyName=BU.Company__c;
        }   
        //System.Debug('>>>!!! mapCompanyBU - ' + mapCompanyBU + ' KeySet - ' + mapCompanyBU.get('a0wR0000002rvtLIAQ'));
        return mapCompanyBU;
     }

     public static map<id,set<id>> getBUGroup(){
        set<id> setBUIds = new set<id>();
        map<id,set<id>> mapGroupBU  = new Map<Id, set<id>>();
        list<Business_Unit__c> lstBusiness = [SELECT Business_Unit_Group__c, id From Business_Unit__c order by Business_Unit_Group__c];
        id idBUGroup;
        for(Business_Unit__c BU:lstBusiness){
            if(idBUGroup!=BU.Business_Unit_Group__c){
                setBUIds = new set<id>();   
            }
            setBUIds.add(BU.id);
            mapGroupBU.put(BU.Business_Unit_Group__c,setBUIds);
            idBUGroup=BU.Business_Unit_Group__c;
        }   
        //System.Debug('>>>!!! mapCompanyBU - ' + mapCompanyBU + ' KeySet - ' + mapCompanyBU.get('a0wR0000002rvtLIAQ'));
        return mapGroupBU;
     }

     public static map<id,set<id>> getSBUCompany(){
        set<id> setSBUIds = new set<id>();
        map<id,set<id>> mapCompanySBU  = new Map<Id, set<id>>();
        list<Sub_Business_Units__c> lstSubBusiness = [SELECT Business_Unit__r.Company__c, id From Sub_Business_Units__c order by Business_Unit__r.Company__c];
        id idCompanyName;
        for(Sub_Business_Units__c SBU:lstSubBusiness){
            if(idCompanyName!=SBU.Business_Unit__r.Company__c){
                setSBUIds = new set<id>();  
            }
            setSBUIds.add(SBU.id);
            mapCompanySBU.put(SBU.Business_Unit__r.Company__c,setSBUIds);
            idCompanyName=SBU.Business_Unit__r.Company__c;
        }   
        return mapCompanySBU;
     }

     public static map<id,set<id>> getSBUGroup(){
        set<id> setSBUIds = new set<id>();
        map<id,set<id>> mapGroupSBU  = new Map<Id, set<id>>();
        list<Sub_Business_Units__c> lstSubBusiness = [SELECT Business_Unit__r.Business_Unit_Group__c, id From Sub_Business_Units__c order by Business_Unit__r.Business_Unit_Group__c];
        id idSBUGroup;
        for(Sub_Business_Units__c SBU:lstSubBusiness){
            if(idSBUGroup!=SBU.Business_Unit__r.Business_Unit_Group__c){
                setSBUIds = new set<id>();  
            }
            setSBUIds.add(SBU.id);
            mapGroupSBU.put(SBU.Business_Unit__r.Business_Unit_Group__c,setSBUIds);
            idSBUGroup=SBU.Business_Unit__r.Business_Unit_Group__c;
        }   
        //System.Debug('>>>!!! mapCompanyBU - ' + mapCompanyBU + ' KeySet - ' + mapCompanyBU.get('a0wR0000002rvtLIAQ'));
        return mapGroupSBU;
     }

     public static map<id,set<id>> getSBUBUs(){
        set<id> setSBUIds = new set<id>();
        map<id,set<id>> mapSBUBU  = new Map<Id, set<id>>();
        list<Sub_Business_Units__c> lstSubBU = [SELECT Business_Unit__c, id From Sub_Business_Units__c order by Business_Unit__c];
        id idBU;
        for(Sub_Business_Units__c SBU:lstSubBU){
            if(idBU!=SBU.Business_Unit__c){
                setSBUIds = new set<id>();  
            }
            setSBUIds.add(SBU.id);
            mapSBUBU.put(SBU.Business_Unit__c,setSBUIds);
            idBU=SBU.Business_Unit__c;
        }   
        //System.Debug('>>>!!! mapCompanyBU - ' + mapCompanyBU + ' KeySet - ' + mapCompanyBU.get('a0wR0000002rvtLIAQ'));
        return mapSBUBU;
     }

    public static set<id> getUserSUB(){
        set<id> setUserSubs = new set<id>();
        list<User_Business_Unit__c> lstSBUs = [select Sub_Business_Unit__c from User_Business_Unit__c where User__c=:UserInfo.getUserId()];                     
        if(lstSBUs.size()>0){
            for(User_Business_Unit__c SBU:lstSBUs){
                setUserSubs.add(SBU.Sub_Business_Unit__c);      
            }
        }
        return setUserSubs;
    }

     public static map<id,set<id>> getTherapyCompany(){
        set<id> setTherapyIds = new set<id>();
        map<id,set<id>> mapCompanyTherapy  = new Map<Id, set<id>>();
        list<Therapy__c> lstTherapies = [select Id,Company_Id__c from Therapy__c order by Company_Id__c];
        id idCompanyName;
        for(Therapy__c TH:lstTherapies){
            if(idCompanyName!=TH.Company_Id__c){
                setTherapyIds = new set<id>();  
            }
            setTherapyIds.add(TH.id);
            mapCompanyTherapy.put(TH.Company_Id__c,setTherapyIds);
            idCompanyName=TH.Company_Id__c;
        }   
        return mapCompanyTherapy;
     }

     public static map<id,set<id>> getTherapyGroup(){
        set<id> setTherapyIds = new set<id>();
        map<id,set<id>> mapGroupTherapy  = new Map<Id, set<id>>();
        list<Therapy__c> lstTherapies = [select ID,Business_Unit__r.Business_Unit_Group__c from Therapy__c order by Business_Unit__r.Business_Unit_Group__c];
        id idGroupName;
        for(Therapy__c TH:lstTherapies){
            if(idGroupName!=TH.Business_Unit__r.Business_Unit_Group__c){
                setTherapyIds = new set<id>();  
            }
            setTherapyIds.add(TH.id);
            mapGroupTherapy.put(TH.Business_Unit__r.Business_Unit_Group__c,setTherapyIds);
            idGroupName=TH.Business_Unit__r.Business_Unit_Group__c;
        }   
        return mapGroupTherapy;
     }

     public static map<id,set<id>> getTherapyBU(){
        set<id> setTherapyIds = new set<id>();
        map<id,set<id>> mapBUTherapy  = new Map<Id, set<id>>();
        list<Therapy__c> lstTherapies = [select ID,Business_Unit__c from Therapy__c order by Business_Unit__c];
        id idBUName;
        for(Therapy__c TH:lstTherapies){
            if(idBUName!=TH.Business_Unit__c){
                setTherapyIds = new set<id>();  
            }
            setTherapyIds.add(TH.id);
            mapBUTherapy.put(TH.Business_Unit__c,setTherapyIds);
            idBUName=TH.Business_Unit__c;
        }   
        return mapBUTherapy;
     }

     public static set<id> getTherapyUserSBUs(set<id> lstUserSBU){
        set<id> setTherapyIds = new set<id>();
        list<Therapy__c> lstTherapies = [select ID from Therapy__c where Sub_Business_Unit__c in: lstUserSBU];
        for(Therapy__c TH:lstTherapies){
            setTherapyIds.add(TH.id);
        }   
        return setTherapyIds;
     }
    
    public static map<id,set<id>> getPGCompany(){
        set<id> setPGIds = new set<id>();
        map<id,set<id>> mapCompanyPG = new Map<Id, set<id>>();
        list<Product_Group__c> lstPG = [select Id,Therapy_ID__r.Business_Unit__r.Company__c from Product_Group__c order by Therapy_ID__r.Business_Unit__r.Company__c];
        id idCompanyName;
        for(Product_Group__c PG:lstPG){
            if(idCompanyName!=PG.Therapy_ID__r.Business_Unit__r.Company__c){
                setPGIds = new set<id>();   
            }
            setPGIds.add(PG.id);
            mapCompanyPG.put(PG.Therapy_ID__r.Business_Unit__r.Company__c,setPGIds);
            idCompanyName=PG.Therapy_ID__r.Business_Unit__r.Company__c;
        }   
        return mapCompanyPG;
     }

    public static map<id,set<id>> getPGGroup(){
        set<id> setPGIds = new set<id>();
        map<id,set<id>> mapGroupPG = new Map<Id, set<id>>();
        list<Product_Group__c> lstPG = [select ID, Therapy_ID__r.Business_Unit__r.Business_Unit_Group__c from Product_Group__c order by Therapy_ID__r.Business_Unit__r.Business_Unit_Group__c];
        id idGroupName;
        for(Product_Group__c PG:lstPG){
            if(idGroupName!=PG.Therapy_ID__r.Business_Unit__r.Business_Unit_Group__c){
                setPGIds = new set<id>();   
            }
            setPGIds.add(PG.id);
            mapGroupPG.put(PG.Therapy_ID__r.Business_Unit__r.Business_Unit_Group__c,setPGIds);
            idGroupName=PG.Therapy_ID__r.Business_Unit__r.Business_Unit_Group__c;
        }   
        return mapGroupPG;
     }
     
    public static set<id> getPGSBUs(set<id> setUserSBUs){
        set<id> setPGIds = new set<id>();
        list<Product_Group__c> lstPG = [select ID from Product_Group__c where Therapy_ID__r.Sub_Business_Unit__c in: setUserSBUs];
        for(Product_Group__c PG:lstPG){
            setPGIds.add(PG.id);
        }   
        return setPGIds;
     }

    
    public static iPlanResultWrapper doesIplanExist(Account_Plan_2__c accPlan){
        if(accPlan.Business_Unit__c != null){
            return doesIplanExistForBU(accPlan);
        }
        
        if(accPlan.Business_Unit_Group__c != null){
            return doesIplanExistForBUGroup(accPlan);
        }
        
        if(accPlan.Sub_Business_Unit__c != null){
            return doesIplanExistForSBU(accPlan);
        }
        
        return new iPlanResultWrapper();
    }
    
    private static iPlanResultWrapper doesIplanExistForBU(Account_Plan_2__c accPlan){
        List<Account_Plan_2__c> accPlanFromDb = [select apl.Business_Unit__r.name, apl.Name, apl.Mobile_ID__c,
                                                        apl.Owner.Name
                                                 from Account_Plan_2__c apl
                                                 where apl.Account__c =: accPlan.Account__c
                                                 and apl.Business_Unit__c =: accPlan.Business_Unit__c and apl.Account_Plan_Level__c = 'Business Unit'];
        if(accPlanFromDb.size() > 0){
            string message = 'There is an existing iPlan for ' +accPlanFromDb[0].Business_Unit__r.Name +'.|| Please contact '+accPlanFromDb[0].owner.name+' to add you to the accountplan team members.';       
            return returnIplanExists(accPlanFromDb,accPlan,message);
        }
        
        iPlanResultWrapper result = new iPlanResultWrapper();
        result.isFound = false;
        result.errorMessage = 'There is no iPlan found.';
        
        return result;
    }
    
    private static iPlanResultWrapper doesIplanExistForBUGroup(Account_Plan_2__c accPlan){
        List<Account_Plan_2__c> accPlanFromDb = [select apl.Business_Unit__c, apl.Name, apl.Mobile_ID__c,
                                                        apl.Business_Unit_Group__r.name,apl.Owner.Name
                                                 from Account_Plan_2__c apl
                                                 where apl.Account__c =: accPlan.Account__c
                                                 and apl.Business_Unit_Group__c =: accPlan.Business_Unit_Group__c and apl.Account_Plan_Level__c = 'Business Unit Group'];
        if(accPlanFromDb.size() > 0){ System.debug('accPLanFromDb '+accPlanFromDb);
            string message = 'There is an existing iPlan for ' +accPlanFromDb[0].Business_Unit_Group__r.Name +'.|| Please contact '+accPlanFromDb[0].owner.name+' to add you to the accountplan team members.';     
            return returnIplanExists(accPlanFromDb,accPlan,message);
        }
        
        iPlanResultWrapper result = new iPlanResultWrapper();
        result.isFound = false;
        result.errorMessage = 'There is no iPlan found.';
        
        return result;
    }
    
    private static iPlanResultWrapper doesIplanExistForSBU(Account_Plan_2__c accPlan){
        List<Account_Plan_2__c> accPlanFromDb = [select apl.Business_Unit__c, apl.Name, apl.Mobile_ID__c,
                                                        apl.Sub_Business_Unit__r.name,apl.Owner.Name
                                                 from Account_Plan_2__c apl
                                                 where apl.Account__c =: accPlan.Account__c
                                                 and apl.Sub_Business_Unit__c =: accPlan.Sub_Business_Unit__c and apl.Account_Plan_Level__c = 'Sub Business Unit'];
        if(accPlanFromDb.size() > 0){   
            string message = 'There is an existing iPlan for ' +accPlanFromDb[0].Sub_Business_Unit__r.Name +'.|| Please synchronize.';      
            return returnIplanExists(accPlanFromDb,accPlan,message);
        }
        
        iPlanResultWrapper result = new iPlanResultWrapper();
        result.isFound = false;
        result.errorMessage = 'There is no iPlan found.';
        return result;
    }
    
    private static iPlanResultWrapper returnIplanExists(List<Account_Plan_2__c> accPlanFromDb,Account_Plan_2__c accPlan,string message){
            string errorMessage = message;
            iPlanResultWrapper result = new iPlanResultWrapper();
            result.errorMessage = errorMessage;
            result.iPlan = accPlanFromDb[0];
            result.isFound = true;
            return result;
    }
    
    
    public static boolean enrichAccountPerformance= false;              
}