//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 04-12-2015
//  Description      : Batch APEX to perform the callouts to WebMethods record by record.  Each record will be a job.
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
global class ba_NotificationSAP implements Database.Batchable<sObject>, Database.AllowsCallouts {
	
    //----------------------------------------------------------------------------------------
	// GENERAL PARAMETERS
    //----------------------------------------------------------------------------------------
	global String tSOQL = '';
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
	// PARAMETERS NEEDED TO CALL WEBSERVICE ON WEBMETHODS / SAP + CREATE LOGGING + ....
    //----------------------------------------------------------------------------------------
    global bl_NotificationSAP.NotificationSAP oNotificationSAP;
    global Map<Id, NotificationSAPLog__c> mapRecordId_NotificationSAPLog;
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
	// CONSTRUCTOR
    //----------------------------------------------------------------------------------------
	global ba_NotificationSAP() {
		
	}
    //----------------------------------------------------------------------------------------
	

    //----------------------------------------------------------------------------------------
    // START - Create SOQL Query
    //----------------------------------------------------------------------------------------
	global Database.QueryLocator start(Database.BatchableContext BC) {

		
		if (clsUtil.isNull(tSOQL, '') == ''){

			tSOQL = bl_NotificationSAP.getSOQLToProcessData(oNotificationSAP);

		}
		if (Test.isRunningTest()){
			tSOQL += ' LIMIT 1';
		}

		return Database.getQueryLocator(tSOQL);

	}
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
    // EXECUTE - Process the selected data in batches
    //----------------------------------------------------------------------------------------
   	global void execute(Database.BatchableContext BC, List<sObject> lstData) {
		
		oNotificationSAP.lstData = lstData;

		oNotificationSAP.oNotificationSAPLog = mapRecordId_NotificationSAPLog.get(lstData[0].Id);

   		Boolean bResult = bl_NotificationSAP.callWebMethod(oNotificationSAP, mapRecordId_NotificationSAPLog);

	}
    //----------------------------------------------------------------------------------------

	
    //----------------------------------------------------------------------------------------
    // FINISH - Perform logic after all batch jobs are completed
    //----------------------------------------------------------------------------------------
	global void finish(Database.BatchableContext BC) {
		
	}
    //----------------------------------------------------------------------------------------
	
}
//--------------------------------------------------------------------------------------------------------------------