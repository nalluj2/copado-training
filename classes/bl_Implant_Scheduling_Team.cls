public without sharing class bl_Implant_Scheduling_Team {
	
	public static Boolean skipDuringTest = false;
	
	public static void setUniqueKey(List<Implant_Scheduling_Team__c> triggerNew){
				
		for(Implant_Scheduling_Team__c team : triggerNew){
		
			team.Unique_key__c = team.Name;
    	}
	}
	
	public static void createPublicGroup(List<Implant_Scheduling_Team__c> triggerNew){
			
		Map<String, Group> groups = new Map<String, Group>();
		Map<String, Group> agentGroups = new Map<String, Group>();	
				
		for(Implant_Scheduling_Team__c team : triggerNew){
		
			Group teamGroup = new Group();
			teamGroup.Name = team.Name;
			teamGroup.DoesIncludeBosses = false;
			
			groups.put(team.Name, teamGroup);
			
			Group agentGroup = new Group();
			agentGroup.Name = team.Name + ' - Agents';
			agentGroup.DoesIncludeBosses = false;
			
			agentGroups.put(team.Name, agentGroup);
    	}
    	
    	List<Group> allGroups = new List<Group>();
    	allGroups.addAll(groups.values());
    	allGroups.addAll(agentGroups.values());
    	
    	insert allGroups;
    	    	
    	for(Implant_Scheduling_Team__c team : triggerNew){
		
			Group teamGroup = groups.get(team.Name);
			Group agentGroup = agentGroups.get(team.Name);
			team.Team_Group__c = teamGroup.Id;
			team.Team_Group_Externals__c = agentGroup.Id;
    	}
    	
	}
	
	public static void shareTeamWithGroup(List<Implant_Scheduling_Team__c> triggerNew){
				
    	List<Implant_Scheduling_Team__Share> shares = new List<Implant_Scheduling_Team__Share>();
    	
    	for(Implant_Scheduling_Team__c team : triggerNew){
			
			Implant_Scheduling_Team__Share share = new Implant_Scheduling_Team__Share();
			share.ParentId  = team.Id;
			share.UserOrGroupId = team.Team_Group__c;
			share.AccessLevel = 'Read';
			share.RowCause = Schema.Implant_Scheduling_Team__Share.RowCause.Team_Member__c;
			
			shares.add(share);
			
			Implant_Scheduling_Team__Share shareAgents = new Implant_Scheduling_Team__Share();
			shareAgents.ParentId  = team.Id;
			shareAgents.UserOrGroupId = team.Team_Group_Externals__c;
			shareAgents.AccessLevel = 'Read';
			shareAgents.RowCause = Schema.Implant_Scheduling_Team__Share.RowCause.Team_Member__c;
			
			shares.add(shareAgents);			
    	}
    	
    	insert shares;
	}
	
	public static void addAdminForOwner(List<Implant_Scheduling_Team__c> triggerNew){
		
		List<Implant_Scheduling_Team_Admin__c> admins = new List<Implant_Scheduling_Team_Admin__c>();
    	
    	for(Implant_Scheduling_Team__c team : triggerNew){
			
			Implant_Scheduling_Team_Admin__c admin = new Implant_Scheduling_Team_Admin__c();
			admin.Team__c  = team.Id;
			admin.Admin__c = team.OwnerId;
			admin.Owner__c = true;
						
			admins.add(admin);
    	}
    	
    	insert admins;
	}
	
	public static void updateAdminForOwner(List<Implant_Scheduling_Team__c> triggerNew, Map<Id, Implant_Scheduling_Team__c> mapOld){
		
		List<Implant_Scheduling_Team_Admin__c> newAdmins = new List<Implant_Scheduling_Team_Admin__c>();
		Set<Id> updatedTeams = new Set<Id>();
    	
    	for(Implant_Scheduling_Team__c team : triggerNew){
			
			Implant_Scheduling_Team__c oldTeam = mapOld.get(team.Id);
			
			if(team.OwnerId != oldTeam.OwnerId){
				
				Implant_Scheduling_Team_Admin__c admin = new Implant_Scheduling_Team_Admin__c();
				admin.Team__c  = team.Id;
				admin.Admin__c = team.OwnerId;
				admin.Owner__c = true;
							
				newAdmins.add(admin);		
				
				updatedTeams.add(team.Id);		
			}
    	}
    	
    	delete [Select Id from Implant_Scheduling_Team_Admin__c where Team__c IN :updatedTeams AND Owner__c = true];
    	
    	insert newAdmins;
	}
		
    public static void updatePublicGroup(List<Implant_Scheduling_Team__c> triggerNew, Map<Id, Implant_Scheduling_Team__c> mapOld){
			
		List<Group> groups = new List<Group>();	
				
		for(Implant_Scheduling_Team__c team : triggerNew){
		
			Implant_Scheduling_Team__c oldTeam = mapOld.get(team.Id);
			
			if(team.Name != oldTeam.Name){
				
				Group teamGroup = new Group();
				teamGroup.Id = team.Team_Group__c;
				teamGroup.Name = team.Name;
			
				groups.add(teamGroup);
				
				Group agentGroup = new Group();
				agentGroup.Id = team.Team_Group_Externals__c;
				agentGroup.Name = team.Name + ' - Agents';
			
				groups.add(agentGroup);
			}
    	}
    	
    	if(groups.size() > 0) update groups;    	
	}
	
	@future
	public static void addTeamMembers(Set<Id> teamMemberIds){
		
		List<Implant_Scheduling_Team_Member__c> teamMembers = [Select Team__r.Team_Group__c, Team__r.Team_Group_Externals__c, Member__c, Member__r.UserType from Implant_Scheduling_Team_Member__c where Id IN :teamMemberIds];
		
		Set<Id> userIds = new Set<Id>();
		Set<Id> agentIds = new Set<Id>();
		Set<String> userGroups = new Set<String>();
		Set<String> agentGroups = new Set<String>();
				
		for(Implant_Scheduling_Team_Member__c teamMember : teamMembers){
			
			if(teamMember.Member__r.UserType == 'PowerPartner'){
				
				agentIds.add(teamMember.Member__c);
				agentGroups.add(teamMember.Member__c + ':' + teamMember.Team__r.Team_Group_Externals__c);
				
			}else{
			
				userIds.add(teamMember.Member__c);
				userGroups.add(teamMember.Member__c + ':' + teamMember.Team__r.Team_Group__c);
			}
		}
		
		if(userIds.size() > 0) addUserToGroup(userGroups, userIds);				
		if(agentIds.size() > 0) addUserToGroup(agentGroups, agentIds);
			
		Set<Id> allUserIds = new Set<Id>();
		allUserIds.addAll(userIds);
		allUserIds.addAll(agentIds);
		
		addPermissionSet(allUserIds);
	}
	
	@future
	public static void addTeamAdmin(Set<Id> teamAdminIds){
		
		List<Implant_Scheduling_Team_Admin__c> teamAdmins = [Select Team__r.Team_Group__c, Admin__c from Implant_Scheduling_Team_Admin__c where Id IN :teamAdminIds];
		
		Set<Id> userIds = new Set<Id>();
		Set<String> userGroups = new Set<String>();
				
		for(Implant_Scheduling_Team_Admin__c teamAdmin : teamAdmins){
			
			userIds.add(teamAdmin.Admin__c);
			userGroups.add(teamAdmin.Admin__c + ':' + teamAdmin.Team__r.Team_Group__c);
		}
				
		addUserToGroup(userGroups, userIds);
		
		addPermissionSet(userIds);
	}
	
	private static void addUserToGroup(Set<String> userGroups, Set<Id> userIds){
			
		Map<Id, Set<Id>> groupsPerUser = new Map<Id, Set<Id>>();
		
		for(GroupMember userGroup : [Select GroupId, UserOrGroupId from GroupMember where UserOrGroupId IN :userIds]){
			
			Set<Id> groupIds = groupsPerUser.get(userGroup.UserOrGroupId);
			
			if(groupIds == null){
				
				groupIds = new Set<Id>();
				groupsPerUser.put(userGroup.UserOrGroupId, groupIds);
			}
												
			groupIds.add(userGroup.groupId);
		}
		
		List<GroupMember> groupMembers = new List<GroupMember>();
		
		for(String userGroup : userGroups){
						
			Id userId = userGroup.split(':')[0]; 
			Id groupId = userGroup.split(':')[1];
			
			if(groupsPerUser.get(userId) == null || groupsPerUser.get(userId).contains(groupId) == false){
										
				GroupMember member = new GroupMember();
				member.GroupId = groupId;
				member.UserOrGroupId = userId;
				
				groupMembers.add(member);
			}			
		}	
		
		if(groupMembers.size()> 0 ) insert groupMembers;		
	}
				
	private static void addPermissionSet(Set<Id> userIds){
		
		PermissionSet ps = [Select Id from PermissionSet where Name = 'Implant_Scheduling_with_Teams'];
		
		Set<Id> usersWithPS = new Set<Id>();		
		for(PermissionSetAssignment psAssignment : [Select Id, AssigneeId from PermissionSetAssignment where PermissionSetId = :ps.Id AND AssigneeId IN :userIds]){
			
			usersWithPS.add(psAssignment.AssigneeId);
		}
		
		List<PermissionSetAssignment> toInsert = new List<PermissionSetAssignment>();
		
		for(Id userId : userIds){
			
			if(usersWithPS.contains(userId) == false){
				
				PermissionSetAssignment psAssignment = new PermissionSetAssignment();
				psAssignment.PermissionSetId = ps.Id;
				psAssignment.AssigneeId = userId;
				
				toInsert.add(psAssignment);
			}
		}
		
		if(toInsert.size() > 0) insert toInsert;		
	}
	
	@future 
	public static void deleteTeamMembers(Set<Id> teamMembers){
				
		Set<Id> userIds = new Set<Id>();
		Set<Id> agentIds = new Set<Id>();		
		Set<String> userGroups = new Set<String>();				
		Set<String> agentGroups = new Set<String>();
			
		for(Implant_Scheduling_Team_Member__c teamMember : [Select Team__r.Team_Group__c, Team__r.Team_Group_Externals__c, Member__c, Member__r.UserType from Implant_Scheduling_Team_Member__c where Id IN :teamMembers ALL ROWS]){
			
			if(teamMember.Member__r.UserType == 'PowerPartner'){
				
				agentIds.add(teamMember.Member__c);
				agentGroups.add(teamMember.Member__c + ':' + teamMember.Team__r.Team_Group_Externals__c);
				
			}else{
			
				userIds.add(teamMember.Member__c);
				userGroups.add(teamMember.Member__c + ':' + teamMember.Team__r.Team_Group__c);
			}			
		}
			
		if(userIds.size() > 0) removeUsersFromGroup(userGroups, userIds);
		if(agentIds.size() > 0) removeUsersFromGroup(agentGroups, agentIds);
		
		Set<Id> allUserIds = new Set<Id>();
		allUserIds.addAll(userIds);
		allUserIds.addAll(agentIds);
		
		removePermissionSet(allUserIds);
	}
	
	@future 
	public static void deleteTeamAdmins(Set<Id> teamAdmins){
				
		Set<Id> userIds = new Set<Id>();		
		Set<String> userGroups = new Set<String>();
			
		for(Implant_Scheduling_Team_Admin__c teamAdmin : [Select Team__r.Team_Group__c, Admin__c from Implant_Scheduling_Team_Admin__c where Id IN :teamAdmins ALL ROWS]){
			
			userIds.add(teamAdmin.Admin__c);				
			userGroups.add(teamAdmin.Admin__c + ':' + teamAdmin.Team__r.Team_Group__c);
		}
			
		removeUsersFromGroup(userGroups, userIds);
		
		removePermissionSet(userIds);
	}
	
	private static void removeUsersFromGroup(Set<String> userGroups, Set<Id> userIds){
		
		for(Implant_Scheduling_Team_Admin__c teamAdmin : [Select Team__r.Team_Group__c, Admin__c from Implant_Scheduling_Team_Admin__c where Admin__c IN :userIds]) userGroups.remove(teamAdmin.Admin__c + ':' + teamAdmin.Team__r.Team_Group__c);		
		for(Implant_Scheduling_Team_Member__c teamMember : [Select Team__r.Team_Group__c, Member__c from Implant_Scheduling_Team_Member__c where Member__c IN :userIds]) userGroups.remove(teamMember.Member__c + ':' + teamMember.Team__r.Team_Group__c);
		
		if(userGroups.size() == 0) return;
		
		List<GroupMember> toDelete = new List<GroupMember>();
			
		for(GroupMember groupMember : [Select Id, GroupId, UserOrGroupId from GroupMember where UserOrGroupId IN : userIds]){
				
			String key = groupMember.UserOrGroupId + ':' + groupMember.GroupId;
				
			if(userGroups.contains(key)) toDelete.add(groupMember);
				
		}
			
		if(toDelete.size() > 0) delete toDelete;		
	}
	
	private static void removePermissionSet(Set<Id> userIds){
			
		for(Implant_Scheduling_Team_Admin__c teamAdmin : [Select Admin__c from Implant_Scheduling_Team_Admin__c where Admin__c IN :userIds]) userIds.remove(teamAdmin.Admin__c);		
		for(Implant_Scheduling_Team_Member__c teamMember : [Select Member__c from Implant_Scheduling_Team_Member__c where Member__c IN :userIds]) userIds.remove(teamMember.Member__c);
		
		if(userIds.size() == 0) return;
				
		List<PermissionSetAssignment> toDelete = [Select Id from PermissionSetAssignment where PermissionSet.Name = 'Implant_Scheduling_with_Teams' AND AssigneeId IN :userIds];
		
		if(toDelete.size() > 0) delete toDelete;		
	}
	
	public static void addMobileApp(Set<Id> userIds){
		
		Mobile_App__c mobileApp = [Select Id from Mobile_App__c where Unique_Key__c = 'ACT_SCHDL'];
		
		Map<Id, Mobile_App_Assignment__c> appAssignments = new Map<Id, Mobile_App_Assignment__c>();		
		
		for(Mobile_App_Assignment__c appAssignment : [Select Id, Active__c, User__c from Mobile_App_Assignment__c where Mobile_App__c = :mobileApp.Id AND User__c IN :userIds]){
			
			appAssignments.put(appAssignment.User__c, appAssignment);
		}
		
		List<Mobile_App_Assignment__c> toUpsert = new List<Mobile_App_Assignment__c>();
		
		for(Id userId : userIds){
			
			Mobile_App_Assignment__c appAssignment = appAssignments.get(userId);
			
			if(appAssignment == null){
				
				appAssignment = new Mobile_App_Assignment__c();
				appAssignment.Mobile_App__c = mobileApp.Id;
				appAssignment.User__c = userId;
				appAssignment.Active__c = true;
				
				toUpsert.add(appAssignment);
				
			}else if(appAssignment.Active__c == false){
				
				appAssignment.Active__c = true;
				
				toUpsert.add(appAssignment);
			}
		}
		
		if(toUpsert.size() > 0) upsert toUpsert;	
	}
	
	public static void removeMobileApp(Set<Id> userIds){
		
		for(Implant_Scheduling_Team_Admin__c teamAdmin : [Select Admin__c from Implant_Scheduling_Team_Admin__c where Admin__c IN :userIds]) userIds.remove(teamAdmin.Admin__c);		
		for(Implant_Scheduling_Team_Member__c teamMember : [Select Member__c from Implant_Scheduling_Team_Member__c where Member__c IN :userIds]) userIds.remove(teamMember.Member__c);
		
		if(userIds.size() == 0) return;
		
		List<Mobile_App_Assignment__c> appAssignments = [Select Id, Active__c, User__c from Mobile_App_Assignment__c where Mobile_App__r.Unique_Key__c = 'ACT_SCHDL' AND User__c IN :userIds];
		
		for(Mobile_App_Assignment__c appAssignment : appAssignments){
			
			appAssignment.Active__c = false;
		}
		
		update appAssignments;
	}
	
	public static void blockDeleteAssignedMembers(List<Implant_Scheduling_Team_Member__c> members){
		
		Set<Id> teamIds = new Set<Id>();
		Set<Id> userIds = new Set<Id>();
		Map<String, Implant_Scheduling_Team_Member__c> deletionKeys = new Map<String, Implant_Scheduling_Team_Member__c>();
		
		for(Implant_Scheduling_Team_Member__c member : members){
			
			teamIds.add(member.Team__c);
			userIds.add(member.Member__c);
			deletionKeys.put(member.Team__c + ':' + member.Member__c, member);
		}
		
		DateTime now = DateTime.now();
		
		for(Case futureAssignedCase : [Select Id, Activity_Scheduling_Team__c, Assigned_To__c FROM Case where RecordType.DeveloperName = 'Implant_Scheduling' AND Activity_Scheduling_Team__c IN :teamIds AND Assigned_To__c IN :userIds AND Start_of_Procedure__c > :now]){
			
			String caseKey = futureAssignedCase.Activity_Scheduling_Team__c + ':' + futureAssignedCase.Assigned_To__c;
			
			Implant_Scheduling_Team_Member__c toDeleteMember = deletionKeys.get(caseKey);
			
			if(toDeleteMember != null) toDeleteMember.addError('There are future Requests assigned to this Member. Please re-assigned these Requests first and try again.');
		}
	}
}