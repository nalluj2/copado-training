public with sharing class ControllerManageMembers {
    
    Public Id campaignId{get;set;}
    
    Public List<SelectOption> options {get;set;}
    
    Public String selected{get;set;}
    
    String qryString;
    
    public Boolean PopUp{get;set;}
    
    Public String FieldName1{ get; set; }
    
    Public String FieldName2{ get; set; }
    
    Public String FieldName3{ get; set; } 
    
    Public String FieldName4{ get; set; }
    
    Public String FieldName5{ get; set; }
    
    Public String OutName1{ get; set; }
    
    Public String OutName2{ get; set; }
    
    Public String OutName3{ get; set; }
    
    Public String OutName4{ get; set; }
    
    Public String OutName5{ get; set; }
    
    Public String Value1{ get; set; }
    
    Public String Value2{ get; set; } 
    
    Public String Value3{ get; set; }
    
    Public String Value4{ get; set; }
    
    Public String Value5{ get; set; }
    
    Public string selectedTab{get;set;}
    
    Public String business;
    
    public Boolean AsgnInvitationCaller{get;set;}
    
    Public Integer size{get;set;}
    
    public CampaignMember Cmp{get;set;}
    
    Map<Id,Id>AccandTargetIdMap=new Map<Id,id>();
    
    Public List<CampaignMember> setconcmp{get;set;}
    
    List<wrapperClass>lstSelectedContact;
    
    Public Integer noOfRecordsTarget{get; set;}
    
    List<wrapClass>lstSelectedCampaignMember;
    
    public boolean errorstatus{get;set;}
   
    public boolean boolCCChamp{get;set;}
    
    private Integer pageNumber;
   
    private Integer pageSize;
   
    public List<wrapClass> lstwrpr{get;set;}
    
    private Integer totalPageNumber;

    List<CampaignMember> lstcmpmbr=new List<CampaignMember>();
    
    List<CampaignMember> Deletelstcmpmbr=new List<CampaignMember>();
    
    List<CampaignMember> Acceptlstcmpmbr=new List<CampaignMember>();
    
    List<CampaignMember> lstcmp=new List<CampaignMember>();
    
    Campaign lstcmpgn{get;set;}
    
    Set<id> Setaccid=new Set<id>();
    
    Public Set<id> SetCntctId=new Set<id>();
    
    public String accIds;
    
    public String contactIds;
    
    public string RecordTypeName;
    
    public ControllerManageMembers(ApexPages.StandardController controller) {
   
        campaignId=ApexPages.currentPage().getParameters().get('aid');
        selectedTab='name1';
        PopUp = False;
        pageNumber = 0;
        totalPageNumber = 0;
        pageSize =100;
        
        AsgnInvitationCaller=false;
        errorstatus=false;
        Cmp = new CampaignMember();
        
        List<Target_Account__c> trgtlst=[
                                         Select id, name,Account__c 
                                         from Target_Account__c 
                                         where Campaign__c=:campaignId and Status__c='Approved'
                                        ]; 
       
        for(Target_Account__c ta:trgtlst){
            Setaccid.add(ta.Account__c);
            AccandTargetIdMap.put(ta.Account__c,ta.id);
        }
            
        lstcmpgn=new Campaign();
        lstcmpgn=[
                  Select id,Business_Unit__c,Business_Unit__r.Name,Business_Unit__r.Contact_Flag__c,name,status 
                  from Campaign 
                  where id=:campaignId
                 ];
        
        setconcmp=[
                   Select id,Invitation_Caller_Formula__c ,Invitation_Caller__c,Invitation_Caller__r.name,CC_Status__c,Contact.Primary_Job_Title_vs__c, 
                   Contact.Contact_Primary_Specialty__c,Contact.name,Target_Account__r.Account__r.Name,lastmodifiedDate,NPS_Value__c,Feedback_Caller_Formula__c 
                   from CampaignMember 
                   where CampaignID =:campaignId 
                  ];
    
        for(id acc:SetAccId){
            if(accIds!='' && accIds!=null){
                accIds = accIds + '\'' + string.valueOf(acc) + '\', ';
            }else{
                accIds = '\'' + string.valueOf(acc) + '\', ';
            }
        }
        if(accIds !='' && accIds !=null){
            accIds  ='(' + accIds.substring(0,accIds.lastIndexOf(','))+')'  ;      
        } 
        
        getlstSelectedCampaignMember();        
        BindData(1);    
        
        if(lstcmpgn.Status == 'Invitation Calls Completed' || lstcmpgn.Status=='Survey Completed' || lstcmpgn.Status=='Feedback Calls Completed'){
            AsgnInvitationCaller=true;
        }
    
        Schema.DescribeFieldResult CamMemAccess = Schema.sObjectType.CampaignMember.fields.Invitation_Caller__c;
        
        if(CamMemAccess.isCreateable()){
            boolCCChamp=true;  
        }else{
            boolCCChamp=false;             
         }
    }
    public ApexPages.StandardSetController setConContact{get{
          
    if(setConContact== null) {  
        contactIds='';
        
        size=100; 
        string OffsetNum;
        if(lstcmpgn.Business_Unit__c !=null){
            set<id> setContactid=new set<id>();
            business=lstcmpgn.Business_Unit__r.Contact_Flag__c;
            business='Affiliation_From_Contact__r.'+business;
            RecordTypeName='\''+FinalConstants.recordTypeIdC2A+'\'';
            if(accIds!=null){
            string  strAffiliation='Select id,Affiliation_From_Contact__c from Affiliation__c where Affiliation_Active__c=true and Affiliation_To_Account__c in '+accIds+' and recordtypeId='+RecordTypeName+' and '+business+'=true  ' ;
            
            
            
            for(Affiliation__c af:Database.Query(strAffiliation)){
                if(af.Affiliation_From_Contact__c!=null){
                    setContactid.add(af.Affiliation_From_Contact__c);
                }
            }  
            }
                
            if(setContactid != null){
                for(id cntct:setContactid){
                    if(contactIds!='' && contactIds!=null){
                        contactIds = contactIds + '\'' + string.valueOf(cntct) + '\', ';
                    }else{
                        contactIds = '\'' + string.valueOf(cntct) + '\', ';
                    }
                }  
                if(contactIds !='' && contactIds !=null){
                    contactIds  ='(' + contactIds.substring(0,contactIds.lastIndexOf(','))+')'  ;      
                } 
            }
            if(contactIds!=null ){
               
                string str='Select id,name,Email,Account_Name__c,Primary_Job_Title_vs__c,Contact_Primary_Specialty__c,Contact_Active__c,Account.id from Contact where id in '+contactIds+' and Contact_Active__c=true ';
                setConContact=new ApexPages.StandardSetController(Database.getQueryLocator(str)); 
                setConContact.setPageSize(size);
                noOfRecordsTarget= setConContact.getResultSize();
            }
                          
        }else{
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Business Unit is not selected on Campaign.');
            ApexPages.addMessage(msg);
            return null;
        }

    }
        return setConContact;
        
    }set;}
   
    public List<wrapperClass>getlstSelectedContact(){
        if(setConContact!=null){
            List<Contact> Contacts= (List<Contact>)setConContact.getRecords();
            lstSelectedContact=new List<wrapperClass>();
            for(Contact tc:Contacts){
            
                wrapperClass obj=new wrapperClass(false,tc);
                lstSelectedContact.add(obj);
                
            } 
        }
        return lstSelectedContact;
    }
    
    public void setlstSelectedContact(List<wrapperClass>lst){
        this.lstSelectedContact=lst;
    }
    
    public class wrapperClass{
        
        public boolean bn{get;set;}
        public Contact Cntct{get;set;}
        public wrapperClass(boolean bn, Contact Cntct){
            this.bn=bn;
            this.Cntct=Cntct;           
        }
    }
    
    public pageReference FilterAccount(){
        return null;
    } 
    
    public List<wrapClass>getlstSelectedCampaignMember(){  
       
        lstSelectedCampaignMember=new List<wrapClass>();
        lstSelectedCampaignMember.clear();
        for(CampaignMember cm:setconcmp){
            wrapClass objt=new wrapClass(false,cm);
            lstSelectedCampaignMember.add(objt);
        }
        return lstSelectedCampaignMember;
    }
    
    public void setlstSelectedCampaignMember(List<wrapClass>lst){
        this.lstSelectedCampaignMember=lst;
    }
   
    public class wrapClass{
        public boolean bln{get;set;}
        public CampaignMember Cmbr{get;set;}
        public wrapClass(boolean bn, CampaignMember Cmbr){
            this.bln=bln;
            this.Cmbr=Cmbr;           
        }
    }  
    
    public pageReference refresh() {
        setConContact = null;
        getlstSelectedContact();
        return null;
    }          
                  
    public List<SelectOption> getcolumnNamesNew() {
        
        List<SelectOption> optionsFieldName = new List<SelectOption>();   
            
        optionsFieldName.add(new SelectOption('','--None--'));
        optionsFieldName.Add(new SelectOption('Name','Name'));
        optionsFieldName.Add(new SelectOption('Account_Name__c','Account Name'));
        optionsFieldName.Add(new SelectOption('Primary_Job_Title_vs__c','Primary Job Title'));
        optionsFieldName.Add(new SelectOption('Contact_Primary_Specialty__c','Primary Specialty'));
       
       
        return optionsFieldName ; 
    }

    public List<SelectOption> getOperatorNames() {
        
        List<SelectOption> optionsFieldName = new List<SelectOption>();       
        optionsFieldName .add(new SelectOption('','--None--'));
        optionsFieldName .add(new SelectOption('e','equals'));
        optionsFieldName .add(new SelectOption('n','not equal to'));
        optionsFieldName .add(new SelectOption('s','starts with'));
        optionsFieldName .add(new SelectOption('c','contains'));   
        //optionsFieldName .add(new SelectOption('k','does not contain'));      
        optionsFieldName .add(new SelectOption('l','less than'));
        optionsFieldName .add(new SelectOption('g','greater than'));
        optionsFieldName .add(new SelectOption('m','less or equal'));
        optionsFieldName .add(new SelectOption('h','greater or equal'));              
        return optionsFieldName ;        
    }  
           
    public pageReference filterContact(){
        String strOpr;
        if (FieldName1!= Null && OutName1!=Null ){
            strOpr=getOprater(OutName1,value1,FieldName1);   
            if(strOpr=='Error'){
                    // String strTemp= ErrorMsg(fieldName1);                               
                return null;
            }
            qryString = 'Select id,name,Email,Account_Name__c,Primary_Job_Title_vs__c,Contact_Primary_Specialty__c,Account.id,Contact_Active__c from Contact where id in '+contactIds+' and Contact_Active__c =true and ' + strOpr ;                
                if (FieldName2!= Null && OutName2!=Null ){   
                    strOpr=getOprater(OutName2,value2,FieldName2);
                    if(strOpr=='Error'){
                        //String strTemp= ErrorMsg(fieldName2);                               
                        return null;
                    }
                    qryString = qryString + ' and ' + strOpr;
                    
                    if (FieldName3!= Null && OutName3!=Null ){   
                        strOpr=getOprater(OutName3,value3,FieldName3);
                        if(strOpr=='Error'){
                            //String strTemp= ErrorMsg(fieldName3);                               
                            return null;
                        }
                        qryString = qryString + ' and ' + strOpr;
                            if (FieldName4!= Null && OutName4!=Null ){   
                                strOpr=getOprater(OutName4,value4,FieldName4);
                                if(strOpr=='Error'){
                                    //String strTemp= ErrorMsg(fieldName4);                               
                                    return null;
                                }
                                qryString = qryString + ' and ' + strOpr;
                                if (FieldName5!= Null && OutName5!=Null){   
                                    strOpr=getOprater(OutName5,value5,FieldName5);
                                    if(strOpr=='Error'){
                                        //tring strTemp= ErrorMsg(fieldName5);                               
                                        return null;
                                    }
                                    qryString = qryString + ' and ' + strOpr;
                                }
                            }
                    }       
                }
                
            }else{
                refresh();
                return null;
            }
            setConContact=new ApexPages.StandardSetController(Database.getQueryLocator(qryString));                          
            setConContact.setPageSize(size);
            noOfRecordsTarget = setConContact.getResultSize();
            return null;
    }
    
    private string getOprater(String OutName,String Value,String FieldName){
           
        String fieldtype;
        if (FieldName!='Name'){
            Map<String, Schema.sObjectField> M = Schema.SObjectType.Contact.fields.getMap();
            schema.DescribeFieldResult F = m.get(FieldName).getDescribe();          
            fieldtype = F.getType().name();
        }
        else
            fieldtype='STRING';
        
        string OptrName;
        //NUMBER Percent
        
            if (OutName=='e'){
                OptrName=FieldName + ' = \'' + Value  + '\'';       
            }       
            else if (OutName=='n'){
                OptrName=FieldName + ' != \'' + Value + '\'';    
            }
            else if (OutName=='s'){
                OptrName=FieldName + ' like \'' + Value + '%\'';     
            }
            else if (OutName=='c'){
                OptrName=FieldName + ' like \'%' + Value+ '%\'';    
            }           
            else if (OutName=='l'){
                OptrName='Error';   
            }
            else if (OutName=='g'){
                OptrName='Error';   
            }
            else if (OutName=='m'){
                OptrName='Error';   
            }
            else if (OutName=='h'){
                OptrName='Error';   
            }
        
        return  OptrName;
    }
    
    public List<SelectOption> getcolumnNamesNewCmpgn() {
        
        List<SelectOption> optionsFieldName = new List<SelectOption>();   
           
        optionsFieldName.add(new SelectOption('','--None--'));
        optionsFieldName.Add(new SelectOption('NPS_Value__c','NPS Value'));
        optionsFieldName.Add(new SelectOption('Feedback_Caller_Formula__c','Feedback Caller'));
        optionsFieldName.Add(new SelectOption('Invitation_Caller_Formula__c','Invitation Caller'));
        optionsFieldName.Add(new SelectOption('CC_Status__c','Status'));
       
        return optionsFieldName ; 
    }
    
    public pageReference filterContactExisting(){
        
        String strOpr;
        if (FieldName1!= Null && OutName1!=Null ){
                strOpr=getOprater1(OutName1,value1,FieldName1);   
            if(strOpr=='Error'){
                    // String strTemp= ErrorMsg(fieldName1);                               
                return null;
            }
            qryString = 'Select Invitation_Caller__c,Invitation_Caller__r.name,Target_Account__r.Account__r.Name,Contact.Name,Invitation_Caller_Formula__c,Feedback_Caller_Formula__c,CC_Status__c,Contact.Primary_Job_Title_vs__c,Contact.Contact_Primary_Specialty__c,NPS_Value__c from CampaignMember where CampaignID =:campaignId and ' + strOpr ;
                if (FieldName2!= Null && OutName2!=Null ){   
                    strOpr=getOprater1(OutName2,value2,FieldName2);
                    if(strOpr=='Error'){
                        //String strTemp= ErrorMsg(fieldName2);                               
                        return null;
                    }
                    qryString = qryString + ' and ' + strOpr;
                    
                    if (FieldName3!= Null && OutName3!=Null ){   
                        strOpr=getOprater1(OutName3,value3,FieldName3);
                        if(strOpr=='Error'){
                            //String strTemp= ErrorMsg(fieldName3);                               
                            return null;
                        }
                        qryString = qryString + ' and ' + strOpr;
                        if (FieldName4!= Null && OutName4!=Null ){   
                            strOpr=getOprater1(OutName4,value4,FieldName4);
                            if(strOpr=='Error'){
                                //String strTemp= ErrorMsg(fieldName4);                               
                                return null;
                            }
                            qryString = qryString + ' and ' + strOpr;
                            if (FieldName5!= Null && OutName5!=Null){   
                                strOpr=getOprater1(OutName5,value5,FieldName5);
                                if(strOpr=='Error'){
                                    //tring strTemp= ErrorMsg(fieldName5);                               
                                    return null;
                                }
                                qryString = qryString + ' and ' + strOpr;
                            }
                        }
                    }       
                }
                
            }else{
                setconcmp=[Select id,Invitation_Caller_Formula__c ,Invitation_Caller__c,Invitation_Caller__r.name,CC_Status__c,Contact.Primary_Job_Title_vs__c,Contact.Contact_Primary_Specialty__c,Contact.name,Target_Account__r.Account__r.Name,lastmodifiedDate,NPS_Value__c,Feedback_Caller_Formula__c from CampaignMember where CampaignID =:campaignId];
                return null;
            }
            
            setconcmp=Database.query(qryString); 
            getlstSelectedCampaignMember();
            BindData(pageNumber);
            return null;
    }
       
       private string getOprater1(String OutName,String Value,String FieldName){
          
        String fieldtype;
        if (FieldName!='Name'){
            Map<String, Schema.sObjectField> M = Schema.SObjectType.CampaignMember.fields.getMap();
            schema.DescribeFieldResult F = m.get(FieldName).getDescribe();          
            fieldtype = F.getType().name();
        }
        else
        fieldtype='STRING';
        
        string OptrName;
        //NUMBER Percent
        
            if (OutName=='e'){
                OptrName=FieldName + ' = \'' + Value  + '\'';       
            }       
            else if (OutName=='n'){
                OptrName=FieldName + ' != \'' + Value + '\'';    
            }
            else if (OutName=='s'){
                OptrName=FieldName + ' like \'' + Value + '%\'';     
            }
            else if (OutName=='c'){
                OptrName=FieldName + ' like \'%' + Value+ '%\'';    
            }           
            else if (OutName=='l'){
                OptrName='Error';   
            }
            else if (OutName=='g'){
                OptrName='Error';   
            }
            else if (OutName=='m'){
                OptrName='Error';   
            }
            else if (OutName=='h'){
                OptrName='Error';   
            }
       
        return  OptrName;
    }
    
    //////
    public Pagereference AddContactswithStatusPendingApproval(){
        if(lstcmpgn.Status=='Accounts Approved' || lstcmpgn.Status=='Contacts Approved' || lstcmpgn.Status=='Invitation Callers Assigned'){ 
            lstcmpmbr.clear();
            Map<Id,Id>mapOfCampaignmbrIdandCampaignmbrId=new Map<id,id>();
            List<CampaignMember> lstcmpgnmbr=[
                                              Select id,ContactID 
                                              from CampaignMember 
                                              where CampaignID=:campaignId
                                             ];
       
            for(CampaignMember cmpmbr:lstcmpgnmbr){
                mapOfCampaignmbrIdandCampaignmbrId.put(cmpmbr.ContactID,cmpmbr.ContactID);
            }
            for(wrapperClass obj:lstSelectedContact){
       
                if(obj.bn==true && mapOfCampaignmbrIdandCampaignmbrId.get(obj.cntct.id)==null){
                    CampaignMember campobj=new CampaignMember(Target_Account__c=AccandTargetIdMap.get(obj.cntct.Account.id),CampaignID=campaignId,CC_Status__c='Pending Approval',ContactID=obj.cntct.id,   Invitation_Caller__c=userinfo.getuserid());
                    lstcmpmbr.add(campobj);
                }
            }
            if(lstcmpmbr.size()>0){
                Insert lstcmpmbr;
                setconcmp= [
                            Select id,Invitation_Caller__c,Invitation_Caller__r.name,Invitation_Caller_Formula__c ,CC_Status__c,Contact.Primary_Job_Title_vs__c,
                            Contact.Contact_Primary_Specialty__c,Contact.name,Target_Account__r.Account__r.Name,lastmodifiedDate,NPS_Value__c,Feedback_Caller_Formula__c 
                            from CampaignMember 
                            where CampaignID =:campaignId
                           ];               
                getlstSelectedCampaignMember();
                BindData(pageNumber);
                selectedTab='name2';
            }
        }   
        
        else{
            errorstatus=true;
        }
            
       Return null;
    }
    /////
    public Integer getPageNumber()
    {
        return pageNumber;
    }
    
    public Integer getPageSize()
    {
        return pageSize;
    }
    
    private void BindData(Integer newPageIndex)
    {
        try
        {
            
            lstwrpr=new List<wrapClass>();      
                Transient Integer counter = 0;
                Transient Integer min = 0;
                Transient Integer max = 0;
            if (newPageIndex > pageNumber)
            {
                min = pageNumber * pageSize;
                max = newPageIndex * pageSize;
            }
            else{
                max = newPageIndex * pageSize;
                min = max - pageSize;

            }
        for(wrapClass c : lstSelectedCampaignMember)
        {
            counter++;
            if (counter > min && counter <= max)
                lstwrpr.add(c);
        }
        pageNumber = newPageIndex;
        
        }
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }
    }
    
    public Boolean getPreviousButtonEnabled()
    {
        return !(pageNumber > 1);
    }
    
    public Boolean getNextButtonDisabled()
    {
        if (lstwrpr == null) 
        {
            return true;
        }
        else
        {
            return ((pageNumber * pageSize) >= lstSelectedCampaignMember.size());
        }
    }
    
    public PageReference nextBtnClick() {
        
        getlstSelectedCampaignMember(); 
        BindData(pageNumber + 1);
           
        return null;
    }

    public PageReference previousBtnClick() {
        
        getlstSelectedCampaignMember(); 
        BindData(pageNumber - 1);
        return null;
    
    }
    
    public Integer getTotalPageNumber()
    {
        if (totalPageNumber == 0 && lstSelectedCampaignMember !=null)
        {
            getlstSelectedCampaignMember(); 
            totalPageNumber = lstSelectedCampaignMember.size() / pageSize;
            Integer mod = lstSelectedCampaignMember.size() - (totalPageNumber * pageSize);
            if (mod > 0)
                totalPageNumber++;
        }
        
    return totalPageNumber;
    }
    
    /////
    public Pagereference Remove(){
        
        Deletelstcmpmbr.Clear();
        
        for(wrapClass obj:lstwrpr){
            if(obj.bln==true){
                Deletelstcmpmbr.add(obj.Cmbr);
            }
        }
       
        if(Deletelstcmpmbr.size()>0){
            delete Deletelstcmpmbr;
        
            setconcmp=[
                       Select id,Invitation_Caller_Formula__c ,Invitation_Caller__c,Invitation_Caller__r.name,CC_Status__c,Contact.Primary_Job_Title_vs__c,
                       Contact.Contact_Primary_Specialty__c,Contact.name,Target_Account__r.Account__r.Name,lastmodifiedDate,Feedback_Caller_Formula__c,NPS_Value__c 
                       from CampaignMember 
                       where CampaignID =:campaignId
                      ];
            getlstSelectedCampaignMember();
        
            BindData(pageNumber);
        }
    return null;
    }
    
    public Pagereference Approved(){
        Acceptlstcmpmbr.Clear();
        for(wrapClass obj:lstwrpr){
            if(obj.bln==true){
                obj.Cmbr.CC_Status__c='Approved';
                Acceptlstcmpmbr.add(obj.Cmbr);
            }
        }
        if(Acceptlstcmpmbr.size()>0){
            Update Acceptlstcmpmbr;
            setconcmp=[
                       Select id,Invitation_Caller_Formula__c ,Invitation_Caller__c,Invitation_Caller__r.name,CC_Status__c,Contact.Primary_Job_Title_vs__c,
                       Contact.Contact_Primary_Specialty__c,Contact.name,Target_Account__r.Account__r.Name,lastmodifiedDate,Feedback_Caller_Formula__c,NPS_Value__c 
                       from CampaignMember 
                       where CampaignID =:campaignId
                      ];
            getlstSelectedCampaignMember();
            BindData(pageNumber);
        }
        return null;
    }
    
    public void UserWindow(){
        PopUp = True;
    }
    
    public void CancelWindow(){
        PopUp = False;
    }
    
    public pagereference OkWindow(){
        PopUp = False;
        List<CampaignMember> MemberLstToUpdate = new List<CampaignMember>(); 
        for(wrapclass Wr:lstwrpr){
            if(Wr.bln==True){
                Wr.Cmbr.Invitation_Caller__c = Cmp.Invitation_Caller__c;
                MemberLstToUpdate.add(Wr.Cmbr);
            }
        }
        if(MemberLstToUpdate.size()>0){
            update MemberLstToUpdate;
            setconcmp=[
                       Select id,Invitation_Caller_Formula__c ,Invitation_Caller__c,Invitation_Caller__r.name,CC_Status__c,Contact.Primary_Job_Title_vs__c,
                       Contact.Contact_Primary_Specialty__c,Contact.name,Target_Account__r.Account__r.Name,lastmodifiedDate,Feedback_Caller_Formula__c,NPS_Value__c 
                       from CampaignMember 
                       where CampaignID =:campaignId
                      ];
            getlstSelectedCampaignMember();
            BindData(pageNumber);
        }
        
        return null;
    }
    
    public pagereference clearfilters(){
        fieldName1='';
        fieldName2='';
        fieldName3='';
        fieldName4='';
        fieldName5='';
        OutName1='';
        OutName2='';
        OutName3='';
        OutName4='';
        OutName5='';
        value1=null;
        value2=null;
        value3=null;
        value4=null;
        value5=null;
        return null;
    }
       
}