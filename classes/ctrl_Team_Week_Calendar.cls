public without sharing class ctrl_Team_Week_Calendar {
	
	private static List<String> weekDays = new List<String>{'MON','TUE','WED','THU','FRI', 'SAT', 'SUN'};
	private static List<String> months = new List<String>{'January','February','March','April','May','June','July','August','September','October','November','December'};
	
	@AuraEnabled
    public static CalendarView getWeek(String teamId, Date sampleDate, Decimal width) {
        
        if(width < 900) width = 900;
        
        CalendarView result = new CalendarView();
        
        Implant_Scheduling_Team__c selectedTeam = [Select Work_Day_Start__c, Work_Day_End__c, Working_Days__c, (Select Member__c, Member__r.Name from Team_Members__r ORDER BY Member__r.Name) from Implant_Scheduling_Team__c where Id = :teamId];
        
        List<TeamMember> members = new List<TeamMember>();
     	Map<String, TeamMember> memberMap = new Map<String, TeamMember>();
     	 
     	TeamMember unassignedRequests = new TeamMember('unassigned', 'Unassigned Requests');     	
     	unassignedRequests.height = 45;	
     	members.add(unassignedRequests);
     	memberMap.put('unassigned', unassignedRequests);     	
     	     	
     	Integer counter = 0;
     	
     	for(Implant_Scheduling_Team_Member__c teamMember : selectedTeam.Team_Members__r){
     		
     		TeamMember member = new TeamMember(teamMember.Member__c, teamMember.Member__r.Name);  	
     		member.className = Math.mod(counter++, 2) == 0 ? 'row-even' : 'row-odd';
     		member.height = 45;	
     		members.add(member);
     		memberMap.put(teamMember.Member__c, member);
     	}
     	     	     	
     	result.members = members;
        
        if(sampleDate == null) sampleDate = Date.today();
                                
        DateTime startDate = DateTime.newInstance(sampleDate, Time.newInstance(0, 0, 0, 0));
     	
     	Integer sampleDayOfWeek = Integer.valueOf(startDate.format('u'));
     	
     	Integer teamFirstDay = Integer.valueOf(selectedTeam.Working_Days__c.split(':')[0]);
                
        if(teamFirstDay != sampleDayOfWeek){        
        	
        	if(teamFirstDay == 7) startDate = startDate.addDays(-(sampleDayOfWeek));
        	else startDate = startDate.addDays(-(sampleDayOfWeek - 1));        	
        }
        
        DateTime endDate = DateTime.newInstance(startDate.Date().addDays(6), Time.newInstance(23, 59, 59, 0));
        
        System.debug(memberMap.keySet());
        System.debug(startDate);
        System.debug(endDate);
        
        List<Event> eventList = [Select Subject, WhatId, Id, StartDateTime, IsAllDayEvent, EndDateTime, OwnerId, Type from Event where OwnerId IN :memberMap.keySet() AND StartDateTime < :endDate AND EndDateTime >= :startDate AND isRecurrence = false ORDER BY StartDateTime];
        List<Case> caseList = [Select Subject, Id, Status, AccountId, Account.Name, Account.BillingCity, Product_Group__r.Name, Start_of_Procedure__c, End_of_Procedure__c, Type, Activity_Type_Picklist__c, Procedure__c, Activity_Scheduling_Team__r.Name, Activity_Scheduling_Team__r.Team_Color__c from Case where Activity_Scheduling_Team__c = :teamId AND Status IN ('Open', 'Cancelled', 'Rejected') AND Is_Recurring__c = false AND Start_of_Procedure__c < :endDate AND End_of_Procedure__c > :startDate ORDER BY Start_of_Procedure__c];
        
        System.debug(eventList);
        System.debug(caseList);
        
        Integer teamStartHour = Integer.valueOf(selectedTeam.Work_Day_Start__c);
        Integer teamEndHour = Integer.valueOf(selectedTeam.Work_Day_End__c);        
        Integer teamMinWeekDays = Integer.valueOf(selectedTeam.Working_Days__c.split(':')[1]);
        
        Integer earliestStartHour = teamStartHour;
        Integer latestEndHour = teamEndHour;
        Date latestDay = startDate.addDays(teamMinWeekDays - 1).Date();
              
        for(Case viewCase : caseList){
     		
     		if(viewCase.Status == 'Open' && viewCase.Type != 'Meeting'){
	        	
	        	Integer caseStart;
		        Integer caseEnd;
		        	
	        	Integer caseDays = viewCase.Start_of_Procedure__c.date().daysBetween(viewCase.End_of_Procedure__c.date());
	        	
	        	if(caseDays == 0){
	        	
		        	caseStart = viewCase.Start_of_Procedure__c.hour();
		        	caseEnd = viewCase.End_of_Procedure__c.minute() > 0 ? viewCase.End_of_Procedure__c.hour() + 1 : viewCase.End_of_Procedure__c.hour();
		        			        	
	        	}else{
	        		
	        		if(viewCase.Start_of_Procedure__c.date() < endDate.date()) caseStart = 0;
	        		else caseStart = viewCase.Start_of_Procedure__c.hour();
	        			        		
	        		if(viewCase.End_of_Procedure__c.date() > startDate.date()) caseEnd = 24;	        			
	        		else caseEnd = viewCase.End_of_Procedure__c.minute() > 0 ? viewCase.End_of_Procedure__c.hour() + 1 : viewCase.End_of_Procedure__c.hour();
	        	}	
	        	
	        	if(caseStart < earliestStartHour) earliestStartHour = caseStart;
	        	if(caseEnd > latestEndHour) latestEndHour = caseEnd;
	        	
	        	for(Integer i = 0; i <= caseDays; i++){
	        			        		
	        		DateTime day = viewCase.Start_of_Procedure__c.addDays(i);
	        		
	        		if(day >= startDate && day <= endDate){
	        				        			
	        			if(day.Date() > latestDay) latestDay = day.Date();        
	        		}
	        	}
     		}     		   	
        }
        	
     	Set<Id> caseIds = new Set<Id>();
        
        for(Event viewEvent : eventList){
     		
     		if(viewEvent.WhatId != null && String.valueOf(viewEvent.whatId).startsWith('500')){
     			
     			caseIds.add(viewEvent.WhatId);
     			
     			if(viewEvent.Type == 'Meeting') continue;//If meeting type, then we ignore it in this logic
        
        		Integer eventStart;
	        	Integer eventEnd;
	        	
	        	Integer eventDays = viewEvent.StartDateTime.date().daysBetween(viewEvent.EndDateTime.date());
	        	
	        	if(eventDays == 0){
	        	
		        	eventStart = viewEvent.StartDateTime.hour();
		        	eventEnd = viewEvent.EndDateTime.minute() > 0 ? viewEvent.EndDateTime.hour() + 1 : viewEvent.EndDateTime.hour();		        	
		        			        	
	        	}else{
	        		
	        		if(viewEvent.StartDateTime.date() < endDate.date()) eventStart = 0;
	        		else eventStart = viewEvent.StartDateTime.hour();
	        			        		
	        		if(viewEvent.EndDateTime.date() > startDate.date()) eventEnd = 24;	        			
	        		else eventEnd = viewEvent.EndDateTime.minute() > 0 ? viewEvent.EndDateTime.hour() + 1 : viewEvent.EndDateTime.hour();
	        	}	
	        	
	        	if(eventStart < earliestStartHour) earliestStartHour = eventStart;
	        	if(eventEnd > latestEndHour) latestEndHour = eventEnd;
	        	
	        	for(Integer i = 0; i <= eventDays; i++){
	        			        		
	        		DateTime day = viewEvent.StartDateTime.addDays(i);
	        		
	        		if(day >= startDate && day <= endDate){
	        			
	        			 if(day.Date() > latestDay) latestDay = day.Date();    
	        		}
	        	}
     		}
        }     
        
        List<Hour> hours = new List<Hour>();
        for(Integer i = earliestStartHour; i < latestEndHour; i++){
        	
        	Hour hour = new Hour();
        	hour.hour = i;
        	
        	if(Math.mod(i - earliestStartHour, 2) == 0){
        		
        		hour.isMarker = true;
        		if(latestEndHour - i >= 2) hour.markerSpan = 2;
        		else hour.markerSpan = latestEndHour - i;
        	}
        	
        	if(i < teamStartHour || i >= teamEndHour) hour.isOWH = true;
        	else hour.isOWH = false;
        	
        	hours.add(hour);
        }
        
        Integer numberOfDays = startDate.date().daysBetween(latestDay) + 1;
        
        Decimal pixelsPerMinute = width / ((latestEndHour - earliestStartHour) * 60 * numberOfDays);        
        Integer pixelsPerHour = Integer.valueOf(pixelsPerMinute * 60);
        width = (latestEndHour - earliestStartHour) * pixelsPerHour * numberOfDays;
        pixelsPerMinute = Decimal.valueOf(pixelsPerHour) / 60;
       
        startDate = DateTime.newInstance(startDate.Date(), Time.newInstance(earliestStartHour, 0, 0, 0));
        
        result.firstDay = startDate.date();
        result.hourWidth = pixelsPerHour;
        result.hours = hours;
        
        Date todayDate = Date.today(); 
                
        List<Day> days = new List<Day>();
        
        for(Integer i = 0; i < numberOfDays; i++){
        	        	
        	DateTime sampleDateTime = startDate.addDays(i);
        	
        	Day weekDay = new Day();        	
        	
        	Integer dayOfWeek = Integer.valueOf(sampleDateTime.format('u'));
        	weekDay.dayName = weekDays[dayOfWeek - 1];
        	weekDay.day = sampleDateTime.day();
        	weekDay.month = sampleDateTime.month();
        	weekDay.year = sampleDateTime.year();
        	weekDay.isToday = sampleDateTime.date() == todayDate;
        	
        	if(sampleDateTime.Date() > startDate.addDays(teamMinWeekDays - 1).Date()) weekDay.isOWH = true;
        	else weekDay.isOWH = false;
        	        	
        	days.add(weekDay);        	        	
        }
     	
     	result.days = days;
                
        Day firstDay = days[0];
     	Day lastDay = days[days.size() - 1];
     	
     	if(firstDay.month == lastDay.month){
     		
     		result.headerLabel = firstDay.day + ' - ' + lastDay.day + ' ' + months.get(firstDay.month - 1)  + ' ' + firstDay.year;
     		
     	}else if(firstDay.year == lastDay.year){
     		
     		result.headerLabel = firstDay.day + ' ' + months.get(firstDay.month - 1) + ' - ' + lastDay.day + ' ' + months.get(lastDay.month - 1)  + ' ' + firstDay.year;
     		
     	}else{
     		
     		result.headerLabel = firstDay.day + ' ' + months.get(firstDay.month - 1) + ' ' + firstDay.year + ' - ' + lastDay.day + ' ' + months.get(lastDay.month - 1) + ' ' + lastDay.year;
     	}
     	    	
     	Map<String, List<CalendarItem>> events = new Map<String, List<CalendarItem>>();
     		
     	for(TeamMember member : members){
     			
     		events.put(member.id, new List<CalendarItem>());
     	}
     	     	
     	Time startTime = Time.newInstance(earliestStartHour, 0, 0, 0);
     	Time endTime;
     	if(latestEndHour == 24) endTime = Time.newInstance(23, 59, 59, 0);
     	else endTime = Time.newInstance(latestEndHour, 0, 0, 0);
     	
     	endDate = DateTime.newInstance(latestDay, endTime);
     	
     	Map<Id, Case> cases = new Map<Id, Case>();
     	
     	if(caseIds.size() > 0){
     		
     		cases = new Map<Id, Case>([Select Id, Type, Subject, Procedure__c, Activity_Type_Picklist__c, AccountId, Account.Name, Account.BillingCity, Product_Group__r.Name, Activity_Scheduling_Team__r.Name, Activity_Scheduling_Team__r.Team_Color__c from Case where Id IN :caseIds]);
     	}
     	
     	for(Event viewEvent : eventList){
     		
     		Case eventCase;
     		
     		if(viewEvent.WhatId != null && String.valueOf(viewEvent.whatId).startsWith('500')) eventCase = cases.get(viewEvent.WhatId);
     			     		
	     	List<CalendarItem> dayMemberEvents = events.get(viewEvent.OwnerId);   
	     		     		
		    CalendarItem calItem = new CalendarItem();
	     	calItem.id = viewEvent.Id;	     	
	     		     		
	     	if(eventCase == null){
	     			
	     		calItem.subject = viewEvent.subject;	
	     		calItem.teamName = 'Time-off';
 				calItem.teamColor = 'rgb(' + ctrl_Activity_Scheduling_App.timeOffColor + ')';
	     		calItem.color = 'rgba(' + ctrl_Activity_Scheduling_App.timeOffColor + ', 0.8)';
	     		calItem.caseType = 'event';
	     		
	     		if(viewEvent.IsAllDayEvent == true){
	     			
	     			viewEvent.StartDateTime = DateTime.newInstance(viewEvent.StartDateTime.date(), Time.newInstance(0, 0, 0, 0));
	 				viewEvent.EndDateTime = DateTime.newInstance(viewEvent.EndDateTime.date().addDays(1), Time.newInstance(0, 0, 0, 0));
	     		}
	     			
	     	}else{
	     		
	     		calItem.caseId = viewEvent.WhatId;	
 				calItem.subject = (eventCase.Type == 'Meeting' ? eventCase.Subject : eventCase.Account.Name);
 				calItem.teamName = eventCase.Activity_Scheduling_Team__r.Name;
 				calItem.teamColor = eventCase.Activity_Scheduling_Team__r.Team_Color__c;
 				calItem.accountId = eventCase.AccountId;
 				calItem.location = (eventCase.AccountId != null ? eventCase.Account.BillingCity : null); 				
 					     					
 				if(eventCase.Type == 'Implant Support Request'){
 						
 					calItem.color = 'rgba(' + ctrl_Activity_Scheduling_App.implantColor + ', 0.8)';
 					calItem.caseType = 'implant';     	
 					calItem.subject += ' / ' + eventCase.Procedure__c + ' / ' + eventCase.Product_Group__r.Name; 					 	
 					calItem.procedure = eventCase.Procedure__c;					
 							
 				}else if(eventCase.Type == 'Service Request'){
 						
 					calItem.color = 'rgba(' + ctrl_Activity_Scheduling_App.serviceColor + ', 0.8)';
 					calItem.caseType = 'service';     					
 					calItem.subject += ' / ' + eventCase.Activity_Type_Picklist__c; 					 		
 					calItem.activityType = eventCase.Activity_Type_Picklist__c;
 						
 				}else{
 					
 					calItem.color = 'rgba(' + ctrl_Activity_Scheduling_App.meetingColor + ', 0.8)';
 					calItem.caseType = 'meeting';
 				}
	     	}
	     	
	     	calItem.timeframe = getTimeFrame(viewEvent.StartDateTime, viewEvent.EndDateTime);
     		
	     	DateTime start;
	     	if(viewEvent.StartDateTime < startDate) start = startDate;
	     	else if(viewEvent.StartDateTime.time() < startDate.time()) start = DateTime.newInstance(viewEvent.StartDateTime.date(), startDate.time());
	     	else start = viewEvent.StartDateTime;
     		     		
	     	Decimal left = (startDate.date().daysBetween(start.date()) * (hours.size() * pixelsPerHour + 1)) + ((start.hour() - startTime.hour()) * pixelsPerHour) + (start.minute() * pixelsPerMinute);
	     	calItem.left = Integer.valueOf(left);
	     		
	     	DateTime endT;
	     	if(viewEvent.EndDateTime > endDate) endT = endDate;
	     	else if(viewEvent.EndDateTime.time() > endDate.time()) endT = DateTime.newInstance(viewEvent.EndDateTime.date(), endDate.time());
	     	else if(viewEvent.EndDateTime.time() < startDate.time()) endT = DateTime.newInstance(viewEvent.EndDateTime.date(), startDate.time());
	     	else endT = viewEvent.EndDateTime;
	     		     		     		     			
	     	Decimal eventwidth = (start.date().daysBetween(endT.date()) * (hours.size() * pixelsPerHour + 1)) + ((endT.hour() - start.hour()) * pixelsPerHour) + ((endT.minute() - start.minute()) * pixelsPerMinute); 			
	     	calItem.width = Integer.valueOf(eventwidth) - 1;     		
	     		
	     	calItem.height = 44;     		
	     	calItem.zIndex = (dayMemberEvents.size() + 1);
	     		
	     	if(calItem.width <= 0 ) continue;
	     		
	     	dayMemberEvents.add(calItem);     		
     	}
     	
     	Boolean hasUnassigned = false;
     	
     	for(Case viewCase : caseList){
     		
     		if(viewCase.Status == 'Open' && viewCase.Type != 'Meeting') hasUnassigned = true;
     		
	     	List<CalendarItem> dayMemberEvents = events.get('unassigned');
	     		     		
		    CalendarItem calItem = new CalendarItem();
	     	calItem.id = viewCase.Id;
	     	calItem.caseId = viewCase.Id;
	     	calItem.subject = (viewCase.Type == 'Meeting' ? viewCase.Subject : viewCase.Account.Name);
	     	calItem.timeframe = getTimeFrame(viewCase.Start_of_Procedure__c, viewCase.End_of_Procedure__c);
	     	calItem.accountId = viewCase.AccountId;
	     	calItem.location = (viewCase.AccountId != null ? viewCase.Account.BillingCity : null);
	     	 			
 			if(viewCase.Status == 'Open'){
     		
     			calItem.teamName = viewCase.Activity_Scheduling_Team__r.Name;
	 			calItem.teamColor = viewCase.Activity_Scheduling_Team__r.Team_Color__c;
	 				
	     	}else{
	     			
	     		calItem.subject = '[' + viewCase.Status + '] ' + calItem.subject;
	     		calItem.teamName = viewCase.Status;
	 			calItem.teamColor = 'rgb(' + ctrl_Activity_Scheduling_App.cancelRejectedColor + ')';
	     	}
	 					     					
 			if(viewCase.Type == 'Implant Support Request'){
 					
 				if(viewCase.Status == 'Open') calItem.color = 'rgba(' + ctrl_Activity_Scheduling_App.implantColor + ', 0.8)';
 				else  calItem.color = 'rgba(' + ctrl_Activity_Scheduling_App.cancelRejectedColor + ', 0.8)';
 				calItem.caseType = 'implant';     	
 				calItem.subject += ' / ' + viewCase.Procedure__c + ' / ' + viewCase.Product_Group__r.Name; 				 	
 				calItem.procedure = viewCase.Procedure__c;					
 						
 			}else if(viewCase.Type == 'Service Request'){
 					
 				if(viewCase.Status == 'Open') calItem.color = 'rgba(' + ctrl_Activity_Scheduling_App.serviceColor + ', 0.8)';
 				else calItem.color = 'rgba(' + ctrl_Activity_Scheduling_App.cancelRejectedColor + ', 0.8)';
 				calItem.caseType = 'service';     					
 				calItem.subject += ' / ' + viewCase.Activity_Type_Picklist__c; 				
 				calItem.activityType = viewCase.Activity_Type_Picklist__c;
 					
 			}else{
 				
 				calItem.color = 'rgba(' + ctrl_Activity_Scheduling_App.meetingColor + ', 0.8)'; 				
 				calItem.caseType = 'meeting';				
 			}
 			
	     	DateTime start;
	     	if(viewCase.Start_of_Procedure__c < startDate) start = startDate;
	     	else if(viewCase.Start_of_Procedure__c.time() < startDate.time()) start = DateTime.newInstance(viewCase.Start_of_Procedure__c.date(), startDate.time());
	     	else start = viewCase.Start_of_Procedure__c;
	     		     		
	     	Decimal left = (startDate.date().daysBetween(start.date()) * (hours.size() * pixelsPerHour + 1)) + ((start.hour() - startTime.hour()) * pixelsPerHour) + (start.minute() * pixelsPerMinute);
	     	calItem.left = Integer.valueOf(left);
	     		
	     	DateTime endT;
	     	if(viewCase.End_of_Procedure__c > endDate) endT = endDate;
	     	else if(viewCase.End_of_Procedure__c.time() > endDate.time()) endT = DateTime.newInstance(viewCase.End_of_Procedure__c.date(), endDate.time());
	     	else if(viewCase.End_of_Procedure__c.time() < startDate.time()) endT = DateTime.newInstance(viewCase.End_of_Procedure__c.date(), startDate.time());
	     	else endT = viewCase.End_of_Procedure__c;
	     		     		     		     			
	     	Decimal eventwidth = (start.date().daysBetween(endT.date()) * (hours.size() * pixelsPerHour + 1)) + ((endT.hour() - start.hour()) * pixelsPerHour) + ((endT.minute() - start.minute()) * pixelsPerMinute); 			
	     	calItem.width = Integer.valueOf(eventwidth) - 1;     		
	     		
	     	calItem.height = 44;     		
	     	calItem.zIndex = (dayMemberEvents.size() + 1);
	     			     		
	     	if(calItem.width <= 0 ) continue;   					     					
				
	     	dayMemberEvents.add(calItem);
     	}
     	
     	if(hasUnassigned) memberMap.get('unassigned').className = 'unassigned-pending';
     	else memberMap.get('unassigned').className = 'unassigned-clear';
     	
     	for(String dayMember : events.keySet()){
     		
     		List<CalendarItem> dayMemberItems = events.get(dayMember);
     		TeamMember member = memberMap.get(dayMember);
     		
     		if(dayMemberItems.size() == 0) continue;
     		
     		if(hasOverlaps(dayMemberItems)){
     		
	     		Integer maxOverlaps = 0;
	     		
	     		for(Integer i = 0; i < width; i++){
	     		
	     			List<CalendarItem> pixelItems = getOverlaps(i, dayMemberItems);
	     		
	     			if(pixelItems.size() > maxOverlaps) maxOverlaps = pixelItems.size();     			     		
	     		}
	     		
	     		Integer dayHeight = (maxOverlaps * 45) + 5;	     		
	     		if(dayHeight > member.height) member.height = dayHeight;
	     		
	     		for(Integer i = 0; i < width; i++){
     		
	     			List<CalendarItem> pixelItems = getOverlaps(i, dayMemberItems);
	     			
	     			Set<Integer> usedSlots = new Set<Integer>();
	     			
	     			for(CalendarItem item : pixelItems){
	     				
	     				if(item.top == null){
	     					
	     					Integer top = 0;
	     					if(dayMember != 'unassigned') top = 1;
	     					
	     					while(true){
	     						
	     						if(usedSlots.contains(top) == false){
	     							
	     							item.top = top;
	     							usedSlots.add(top);
	     							break;
	     						}
	     						
	     						top += 45;
	     					}
	     					
	     				}else{
	     					     					     						
	     					usedSlots.add(item.top);    					
	     				}
	     			}     		
	     		}     	
	     		
     		}else{
     			     				     		
	     		if(member.height < 50) member.height = 50;
     			
     			for(CalendarItem item : dayMemberItems){
     				
     				if(dayMember != 'unassigned') item.top = 1;
     				else item.top = 0;
     			}
     		}
     	}
     	 	
     	result.events = events;
     	
     	return result;   
    }
    
    private static String getTimeFrame(DateTime startDT, DateTime endDT){
    	
    	if(endDT.Year() != startDT.Year()){
    		
    		return startDT.format('dd MMM yyyy H:mm') + ' - ' + endDT.format('dd MMM yyyy H:mm');
    		
    	}else if(endDT.Month() != startDT.Month() || endDT.Day() != startDT.Day()){
    		
    		return startDT.format('dd MMM H:mm') + ' - ' + endDT.format('dd MMM H:mm');
    		
    	}else{
    		
    		return startDT.format('H:mm') + ' - ' + endDT.format('H:mm');
    	}
    }
    
    private static Boolean hasOverlaps(List<CalendarItem> items){
    	
    	for(Integer i = 0; i < items.size(); i++){
    		
    		CalendarItem item = items[i];
    		
    		for(Integer j = (i + 1); j < items.size(); j++){
    				
				CalendarItem otherItem = items[j];
    				
				if(otherItem.left <= (item.left + item.width) && (otherItem.left + otherItem.width) >= item.left) return true;
    		}	    		
    	}
    	
    	return false;
    }
    
    private static List<CalendarItem> getOverlaps(Integer pixel, List<CalendarItem> items){
    	
    	List<CalendarItem> pixelItems = new List<CalendarItem>();
    	
    	for(CalendarItem item : items){
    		
    		if(pixel >= item.left && pixel <= (item.left + item.width)){
    			
    			pixelItems.add(item);
    		}    		
    	}
    	
    	return pixelItems;
    }
        
    public class CalendarView {
    	
    	@AuraEnabled public String headerLabel {get; set;}
    	@AuraEnabled public Date firstDay {get; set;}
    	@AuraEnabled public Integer hourWidth {get; set;}  
    	@AuraEnabled public List<Hour> hours {get; set;}  
    	@AuraEnabled public List<Day> days {get; set;}	
    	@AuraEnabled public List<TeamMember> members {get; set;}
    	@AuraEnabled public Map<String, List<CalendarItem>> events {get; set;}
    }
    
    
    public class TeamMember {
    	
    	@AuraEnabled public String id { get; set; }
    	@AuraEnabled public String name { get; set; }
    	@AuraEnabled public String className { get; set; }
    	@AuraEnabled public Integer height { get; set; }
    	
    	public TeamMember(String inputId, String inputName){
    		
    		id = inputId;
    		name = inputName;    		
    	}
    }
    
    public class Day {
    	    	
    	@AuraEnabled public String dayName { get; set; }
    	@AuraEnabled public Integer day { get; set; }
    	@AuraEnabled public Integer month { get; set; }
    	@AuraEnabled public Integer year { get; set; }
    	@AuraEnabled public Boolean isToday { get; set; }
    	@AuraEnabled public Boolean isOWH { get; set; }
    }
    
    public class Hour {
    	
    	@AuraEnabled public Integer hour { get; set; }   	
    	@AuraEnabled public Boolean isMarker { get; set; }
    	@AuraEnabled public Integer markerSpan { get; set; }
    	@AuraEnabled public Boolean isOWH { get; set; }
    }
    
    public class CalendarItem {
    	
    	@AuraEnabled public String id { get; set; }
    	@AuraEnabled public String subject { get; set; }
    	@AuraEnabled public String timeframe { get; set; }
    	@AuraEnabled public String location { get; set; }
    	@AuraEnabled public Integer width { get; set; }
    	@AuraEnabled public Integer top { get; set; }
    	@AuraEnabled public Integer left { get; set; }
    	@AuraEnabled public Integer height { get; set; }
    	@AuraEnabled public Integer zIndex { get; set; }
    	@AuraEnabled public String color { get; set; }
    	@AuraEnabled public String teamName { get; set; }
    	@AuraEnabled public String teamColor { get; set; }
    	@AuraEnabled public Integer maxOverlap { get; set; }
    	@AuraEnabled public String caseId { get; set; }
    	@AuraEnabled public String caseType { get; set; }
    	@AuraEnabled public String accountId { get; set; }
    	@AuraEnabled public String procedure { get; set; }
    	@AuraEnabled public String activityType { get; set; }
    }    
}