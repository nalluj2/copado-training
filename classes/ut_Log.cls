public with sharing class ut_Log {

    public enum EMAIL_TYPE {MAX_RETRIES, INFO}
    public static final List<String> lstErrorStatus_NotificationSAPLog = new List<String>{ 'ERROR', 'FAILED' };

    public static void logOutboundMessage(String internalId, String externalId, String request, String response, String status, String operation, String obj){
        
        Outbound_Message__c outboundMsg = new Outbound_Message__c();
        outboundMsg.Request__c = request;
        outboundMsg.Response__c = response;
        outboundMsg.Internal_ID__c = internalId;
        outboundMsg.External_ID__c = externalId;
        outboundMsg.Operation__c = operation;
        outboundMsg.User__c = UserInfo.getUserId();
        outboundMsg.Object__c = obj;
        outboundMsg.Status__c = status;
        insert outboundMsg;
    }


    //----------------------------------------------------------------------------------------
    // Create / Update a NotificationSAPLog record for logging purposes
    //----------------------------------------------------------------------------------------
    // - Status : NEW, COMPLETED, FAILED, SKIPPED, RETRY
    public static List<NotificationSAPLog__c> createNotificationSAPLog(bl_NotificationSAP.NotificationSAP oNotificationSAP){

        List<NotificationSAPLog__c> lstNotificationSAPLog_Insert = new List<NotificationSAPLog__c>();

        try{

            clsUtil.bubbleException();

            List<NotificationSAPLog__c> lstNotificationSAPLog_Update = new List<NotificationSAPLog__c>();

			if (oNotificationSAP.oWebServiceSetting == null){
                // Get the Web Service Setting data from the Custom Setting
                WebServiceSetting__c oWebServiceSetting = bl_NotificationSAP.loadWebServiceSetting(oNotificationSAP.tWebServiceName);
                oNotificationSAP.oWebServiceSetting = oWebServiceSetting;
            }

			Set<String> recordIDs = new Set<String>();
			
            for (sObject oData : oNotificationSAP.lstData){

                String tValue_Id;
                
                if (clsUtil.isNull(oNotificationSAP.tSFDC_FieldName_Id, '') != ''){
                    recordIDs.add(String.valueOf(clsUtil.isNull(oData.get(oNotificationSAP.tSFDC_FieldName_Id), '')));
                }
            }
            
            Map<String, List<NotificationSAPLog__c>> recordSAPNotificationLogs = getNotificationSAPLogs(recordIDs, oNotificationSAP.tWebServiceName);
            
            for (sObject oData : oNotificationSAP.lstData){

                String tValue_Id;
                String tValue_SAPId;

                if (clsUtil.isNull(oNotificationSAP.tSFDC_FieldName_Id, '') != ''){
                    tValue_Id = String.valueOf(clsUtil.isNull(oData.get(oNotificationSAP.tSFDC_FieldName_Id), ''));
                }
                
                if (clsUtil.isNull(oNotificationSAP.tSFDC_FieldName_SAPID, '') != ''){
                    tValue_SAPId = String.valueOf(clsUtil.isNull(oData.get(oNotificationSAP.tSFDC_FieldName_SAPID), ''));
                }

                // Set helper fields
                oNotificationSAP.tValue_Id = tValue_Id;
                oNotificationSAP.tValue_SAPId = tValue_SAPId;
             
                NotificationSAPLog__c oNotificationSAPLog = new NotificationSAPLog__c();

                    // Validate the Notification SAP and set the correct status based on existingv Notification SAP Log records
                    lstNotificationSAPLog_Update.addAll(validateNotificationSAP(oNotificationSAP, recordSAPNotificationLogs.get(tValue_Id)));  // This will update existing NotificationSAPLog__c records which have a status "RETRY" to a status "SKIPPED"

                    oNotificationSAPLog.Retries__c = -1;

                    oNotificationSAPLog.SFDC_Object_Name__c = oNotificationSAP.tSFDC_Object;
                    oNotificationSAPLog.SFDC_Fieldname_ID__c = oNotificationSAP.tSFDC_FieldName_Id;
                    oNotificationSAPLog.SFDC_Fieldname_SAPID__c = oNotificationSAP.tSFDC_FieldName_SAPId;
                    oNotificationSAPLog.Record_ID__c = oNotificationSAP.tValue_Id;
                    oNotificationSAPLog.Record_SAPID__c = oNotificationSAP.tValue_SAPId;
                    oNotificationSAPLog.EndpointUrl__c = oNotificationSAP.oWebServiceSetting.EndpointUrl__c;
                    oNotificationSAPLog.Authorization_Header__c = oNotificationSAP.tAuthorizationHeader;
                    oNotificationSAPLog.Request__c = clsUtil.isNull(oNotificationSAP.tRequestBody, '').left(131072);
                    oNotificationSAPLog.Response__c = clsUtil.isNull(oNotificationSAP.tResponseBody, '').left(131072);
                    oNotificationSAPLog.Status__c = oNotificationSAP.tStatus;
                    oNotificationSAPLog.Direction__c = oNotificationSAP.tDirection;
                    oNotificationSAPLog.Error_Description__c = oNotificationSAP.tErrorDescription;
                    oNotificationSAPLog.User__c = UserInfo.getUserId();
                    oNotificationSAPLog.WM_Object_Name__c = oNotificationSAP.tWM_Object;
                    oNotificationSAPLog.WM_Operation__c = oNotificationSAP.tWM_Operation;
                    oNotificationSAPLog.WM_Process__c = oNotificationSAP.tWM_Process;
                    oNotificationSAPLog.WebServiceName__c = oNotificationSAP.tWebServiceName;

                    lstNotificationSAPLog_Insert.add(oNotificationSAPLog);  

            }

            if (lstNotificationSAPLog_Update.size() > 0){
                update lstNotificationSAPLog_Update;
            }

            if (lstNotificationSAPLog_Insert.size() > 0){
                insert lstNotificationSAPLog_Insert;
            }


        }catch(Exception oEX){
            clsUtil.debug('Error occured in ut_Log.createNotificationSAPLog on line ' + oEX.getLineNumber() + ' : ' + oEX.getMessage());
        }

        return lstNotificationSAPLog_Insert;

    }

    public static void upsertNotificationSAPLog(bl_NotificationSAP.NotificationSAP oNotificationSAP){

        try{
            
            clsUtil.bubbleException();

            List<NotificationSAPLog__c> lstNotificationSAPLog_Upsert = new List<NotificationSAPLog__c>();

            // Create new / Update existing NotificationSAPLog record
            NotificationSAPLog__c oNotificationSAPLog_Upsert = new NotificationSAPLog__c();

                if ( (oNotificationSAP.oNotificationSAPLog != null) && (oNotificationSAP.oNotificationSAPLog.Id != null) ){
                    oNotificationSAPLog_Upsert.Id = oNotificationSAP.oNotificationSAPLog.Id;
                    oNotificationSAPLog_Upsert.Retries__c = clsUtil.isDecimalNull(oNotificationSAP.oNotificationSAPLog.Retries__c, 0) + 1;
                    oNotificationSAPLog_Upsert.Retry_Cron_Job_Id__c = oNotificationSAP.oNotificationSAPLog.Retry_Cron_Job_Id__c;
                }

                oNotificationSAPLog_Upsert.SFDC_Object_Name__c = oNotificationSAP.tSFDC_Object;
                oNotificationSAPLog_Upsert.SFDC_Fieldname_ID__c = oNotificationSAP.tSFDC_FieldName_Id;
                oNotificationSAPLog_Upsert.SFDC_Fieldname_SAPID__c = oNotificationSAP.tSFDC_FieldName_SAPId;
                oNotificationSAPLog_Upsert.Record_ID__c = oNotificationSAP.tValue_Id;
                oNotificationSAPLog_Upsert.Record_SAPID__c = oNotificationSAP.tValue_SAPId;
                oNotificationSAPLog_Upsert.EndpointUrl__c = oNotificationSAP.oWebServiceSetting.EndpointUrl__c;
                oNotificationSAPLog_Upsert.Authorization_Header__c = oNotificationSAP.tAuthorizationHeader;
                oNotificationSAPLog_Upsert.Request__c = clsUtil.isNull(oNotificationSAP.tRequestBody, '').left(131072);
                oNotificationSAPLog_Upsert.Response__c = clsUtil.isNull(oNotificationSAP.tResponseBody, '').left(131072);
                oNotificationSAPLog_Upsert.Status__c = oNotificationSAP.tStatus;
                oNotificationSAPLog_Upsert.Direction__c = oNotificationSAP.tDirection;
                oNotificationSAPLog_Upsert.Error_Description__c = oNotificationSAP.tErrorDescription;
                oNotificationSAPLog_Upsert.User__c = UserInfo.getUserId();
                oNotificationSAPLog_Upsert.WM_Object_Name__c = oNotificationSAP.tWM_Object;
                oNotificationSAPLog_Upsert.WM_Operation__c = oNotificationSAP.tWM_Operation;
                oNotificationSAPLog_Upsert.WM_Process__c = oNotificationSAP.tWM_Process;
                oNotificationSAPLog_Upsert.WebServiceName__c = oNotificationSAP.tWebServiceName;

            // In case of an update (retry), select all other failed callout log records for the same Salesforce.com Record ID and update/delete these records
            if ( (oNotificationSAP.oNotificationSAPLog != null) && (oNotificationSAP.oNotificationSAPLog.Id != null) ){
/*                
                String tWHERE = ' WHERE';
                    tWHERE += ' Id != \'' + oNotificationSAP.oNotificationSAPLog.Id + '\'';
                    tWHERE += ' AND Record_ID__c = \'' + oNotificationSAP.tValue_Id + '\'';
                    tWHERE += ' AND WebServiceName__c = \'' + oNotificationSAP.tWebServiceName + '\'';
                    tWHERE += ' AND Status__c in :lstErrorStatus_NotificationSAPLog';
                List<NotificationSAPLog__c> lstNotificationSAPLog_Delete = loadNotificationSAPLog(tWhere, '');                    

                for (NotificationSAPLog__c oNotificationSAPLog_Delete : lstNotificationSAPLog_Delete){
                    oNotificationSAPLog_Delete.Status__c = 'SKIPPED';
                    lstNotificationSAPLog_Upsert.add(oNotificationSAPLog_Delete);
                }
*/
            }
            // Get the Web Service Setting data from the Custom Setting
            WebServiceSetting__c oWebServiceSetting = bl_NotificationSAP.loadWebServiceSetting(oNotificationSAPLog_Upsert.WebServiceName__c);

            // Schedule a retry if in case of a failure
            if (
                (oNotificationSAPLog_Upsert.Status__c.toUpperCase() == 'FAILED')
            ){
                if (oNotificationSAPLog_Upsert.Retries__c < oWebServiceSetting.Callout_Max_Retries__c){
                    // Update the status to RETRY and schedule the Retry
                    oNotificationSAPLog_Upsert.Status__c = 'RETRY';
                }else if (oNotificationSAPLog_Upsert.Retries__c > oWebServiceSetting.Callout_Max_Retries__c){
                    oNotificationSAPLog_Upsert.Retries__c = oWebServiceSetting.Callout_Max_Retries__c;
                }

                // Update the NotificationSAPLog Record
                lstNotificationSAPLog_Upsert.add(oNotificationSAPLog_Upsert);
                upsert lstNotificationSAPLog_Upsert; 

                String tWHERE = 'Id = \'' + oNotificationSAPLog_Upsert.Id + '\'';
                lstNotificationSAPLog_Upsert = ut_Log.loadNotificationSAPLog(tWhere, '');

                oNotificationSAPLog_Upsert = lstNotificationSAPLog_Upsert[0];

                // Schedule the Retry
                scheduleRetryNotificationSAP(oNotificationSAPLog_Upsert);

            }else{
 
                lstNotificationSAPLog_Upsert.add(oNotificationSAPLog_Upsert);
                upsert lstNotificationSAPLog_Upsert; 

            }

        }catch(Exception oEX){
            clsUtil.debug('Error occured in ut_Log.upsertNotificationSAPLog on line ' + oEX.getLineNumber() + ' : ' + oEX.getMessage());
        }

    }
    
    private static Map<String, List<NotificationSAPLog__c>> getNotificationSAPLogs(Set<String> recordIds, String tWebServiceName){
    	
    	Map<String, List<NotificationSAPLog__c>> result = new Map<String, List<NotificationSAPLog__c>>();
    	
    	// Verify if we have 'NEW', 'PENDING" or 'RETRY' Notifications to SAP for the processing data and set the Status accordingly   
        for(NotificationSAPLog__c notificationSAPLog : [SELECT 
										                    ID, Name, Status__c, Record_ID__c, WebServiceName__c, Retry_Cron_Job_Name__c, Retry_Cron_Job_Id__c
										                FROM
										                    NotificationSAPLog__c
										                WHERE
										                    WebServiceName__c = :tWebServiceName
										                    AND Record_ID__c IN :recordIds //oNotificationSAP.tValue_Id
										                    AND Status__c in ('NEW', 'PENDING', 'RETRY')
										                ORDER BY 
										                    Status__c DESC
										            ]){

			List<NotificationSAPLog__c> recordNotifications = result.get(notificationSAPLog.Record_ID__c);
			
			if(recordNotifications == null){
				
				recordNotifications = new List<NotificationSAPLog__c>();
				result.put(notificationSAPLog.Record_ID__c, recordNotifications);
			}

			recordNotifications.add(notificationSAPLog);						            	
        }
    	
		return result;    	
    }

    private static List<NotificationSAPLog__c> validateNotificationSAP(bl_NotificationSAP.NotificationSAP oNotificationSAP, List<NotificationSAPLog__c> lstNotificationSAPLog){

        List<NotificationSAPLog__c> lstNotificationSAPLog_Update = new List<NotificationSAPLog__c>();
        
        if (lstNotificationSAPLog == null || lstNotificationSAPLog.size() == 0){
    
            // There are no other notifications found which have a status NEW, PENDING or RETRY --> process a new notification
            oNotificationSAP.tStatus = 'NEW';
    
        }else{

            for (NotificationSAPLog__c oNotificationSAPLog : lstNotificationSAPLog){

                if (oNotificationSAPLog.Status__c == 'PENDING'){

                    // There is already a notification PENDING which will be executed in the future so we can skip the new notification
                    oNotificationSAP.tStatus = 'SKIPPED';
                    break;

                }else if (oNotificationSAPLog.Status__c == 'NEW'){

                    // There is already a notification with a status NEW this means there is already a notification processing and we need to set
                    //  the new Notification to PENDING
                    oNotificationSAP.tStatus = 'PENDING';
                    break;

                }else if (oNotificationSAPLog.Status__c == 'RETRY'){

                    // There is already a notification with a status RETRY so a notification has been scheduled for retry
                    //  We need to cancel the scheduled notification and process the new notification
                    if (!clsUtil.isBlank(oNotificationSAPLog.Retry_Cron_Job_Id__c)){
                        // Abort previous scheduled jobs
                        List<CronTrigger> lstCronTrigger = [SELECT Id FROM CronTrigger WHERE Id = :oNotificationSAPLog.Retry_Cron_Job_Id__c];
                        for (CronTrigger oCronTrigger : lstCronTrigger){
                            System.abortJob(oCronTrigger.Id);
                        }
                    }

                    // Change the status from RETRY to SKIPPED because we don't need to perform the RETRY anymore because we execute the NEW
                    oNotificationSAPLog.Status__c = 'SKIPPED';
                    lstNotificationSAPLog_Update.add(oNotificationSAPLog);

                    oNotificationSAP.tStatus = 'NEW';

                }
            }
        }

        return lstNotificationSAPLog_Update;

     }
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
    // Update the status of NotificationSAPLog records
    //----------------------------------------------------------------------------------------
    public static void updateNotificationSAPLogStatus(String tRecordId, String tStatus_New){
        updateNotificationSAPLogStatus(tRecordId, tStatus_New, '', '');
    }

    public static void updateNotificationSAPLogStatus(String tRecordId, String tStatus_New, String tOutboundMessageRequest, String tOutboundMessageResponse){

        try{

            clsUtil.bubbleException();

            if (clsUtil.isNull(tRecordId, '') != ''){
                List<NotificationSAPLog__c> lstNotificationSAPLog = getNotificationSAPLog(tRecordId, 'NEW');


                NotificationSAPLog__c oNotificationSAPLog;                
                if (lstNotificationSAPLog.size() > 0){
                    oNotificationSAPLog = lstNotificationSAPLog[0];

                    // Get the Web Service Setting data from the Custom Setting
                    WebServiceSetting__c oWebServiceSetting = bl_NotificationSAP.loadWebServiceSetting(oNotificationSAPLog.WebServiceName__c);

                    oNotificationSAPLog.Outbound_Message_Request__c = tOutboundMessageRequest;
                    oNotificationSAPLog.Outbound_Message_Response__c = tOutboundMessageResponse;

                    if (tStatus_New == 'TRUE'){
                        
                        oNotificationSAPLog.Status__c = 'COMPLETED';

                    }else if (tStatus_New == 'FALSE'){
                                                                        
                        if (isErrorForRetry(tOutboundMessageRequest) && oNotificationSAPLog.Retries__c < oWebServiceSetting.Callout_Max_Retries__c){

                            oNotificationSAPLog.Status__c = 'RETRY';

                        }else{

                            oNotificationSAPLog.Status__c = 'FAILED';
                        }
        
                    }else{

                        oNotificationSAPLog.Status__c = tStatus_New;

                    }

                    update oNotificationSAPLog;

                    if (oNotificationSAPLog.Status__c == 'RETRY'){

                        // Schedule the RETRY
                        scheduleRetryNotificationSAP(oNotificationSAPLog);

                    }else if ( (oNotificationSAPLog.Status__c == 'COMPLETED') || (oNotificationSAPLog.Status__c == 'FAILED') ){

                        if ( (oNotificationSAPLog.Status__c == 'FAILED') && (oNotificationSAPLog.Retries__c >= oWebServiceSetting.Callout_Max_Retries__c) ){
                            // Send an email to inform that also the retries have failed
                            sendEmail(EMAIL_TYPE.MAX_RETRIES, oNotificationSAPLog);
                        }

                        // When the NotificationSAPLog status becomes COMPLETED, we need to process the PENDING record (if there is any)
                        List<NotificationSAPLog__c> lstNotificationSAPLog_Pending = ut_Log.getNotificationSAPLog(tRecordId, 'PENDING');

                        if (lstNotificationSAPLog_Pending.size() > 0){
                            NotificationSAPLog__c oNotificationSAPLog_Pending = lstNotificationSAPLog_Pending[0];
                                oNotificationSAPLog_Pending.Status__c = 'NEW';
                            update oNotificationSAPLog_Pending;

                            bl_NotificationSAP.processPendingCallout(oNotificationSAPLog_Pending);
                        }

                    }

                }
            }

        }catch(Exception oEX){
            clsUtil.debug('Error occured in ut_Log.updateNotificationSAPLog on line ' + oEX.getLineNumber() + ' : ' + oEX.getMessage());
        }

    }
    //----------------------------------------------------------------------------------------
	
	private static Boolean isErrorForRetry(String responseMessage){
				
    	if(responseMessage == null || responseMessage == '') return true;
    		
    	try{
    			
    		AckMessage message = (AckMessage) JSON.deserialize(responseMessage, AckMessage.class);
    			    			    				
    		for(SAPErrorMessage error : message.DIST_ERROR_DETAILS){
    				
    			if(error.ERROR_TYPE == null || (error.ERROR_TYPE == 'E' && isFunctionalError(error.ERROR_MESSAGE) == false)) return true;   			
    		}
    		
    	}catch(Exception e){
    			
    		// We couldn't extract the message to we retry it.
    		System.debug('There was an error parsing the response: ' + e.getMessage());
    		return true;
    	}
    	    	
    	return false;
	}
	
	private static List<Interface_functional_error__c> functionalErrors{
		
		get{
			
			if(functionalErrors == null){
				
				Map<String, Interface_functional_error__c> functionalErrorsMap = Interface_functional_error__c.getall();
				
				if(functionalErrorsMap != null) functionalErrors = functionalErrorsMap.values();
				else functionalErrors = new List<Interface_functional_error__c>();
			}
			
			return functionalErrors;
		}
		
		set;
	}
	
	private static Boolean isFunctionalError(String errorMessage){
		
		if(errorMessage == null) return false;
		
		for(Interface_functional_error__c functionalError : functionalErrors){
			
			if(functionalError.Equals_To__c != null && functionalError.Equals_To__c != errorMessage) continue;
			if(functionalError.Starts_With__c != null && errorMessage.startsWith(functionalError.Starts_With__c) == false) continue;
			if(functionalError.Contains__c != null && errorMessage.contains(functionalError.Contains__c) == false) continue;
			if(functionalError.Ends_With__c != null && errorMessage.endsWith(functionalError.Ends_With__c) == false) continue;
			
			return true;
		}
				
		return false;
	}
	
	public class AckMessage {
               
        public List<SAPErrorMessage> DIST_ERROR_DETAILS {get; set;}        
    }
    
    public class SAPErrorMessage{
    	
    	public String ERROR_TYPE {get; set;}
		public String ERROR_MESSAGE {get; set;}
    }
	

    //----------------------------------------------------------------------------------------
    // Schedule retry of send notification
    //----------------------------------------------------------------------------------------
    public static void scheduleRetryNotificationSAP(NotificationSAPLog__c oNotificationSAPLog){
        
        try{

            clsUtil.bubbleException();

            // Get the Web Service Setting data from the Custom Setting
            WebServiceSetting__c oWebServiceSetting = bl_NotificationSAP.loadWebServiceSetting(oNotificationSAPLog.WebServiceName__c);

            clsUtil.debug('ut_Log.scheduleRetryNotificationSAP - oNotificationSAPLog.Retries__c : ' + oNotificationSAPLog.Retries__c);
            clsUtil.debug('ut_Log.scheduleRetryNotificationSAP - oWebServiceSetting.Callout_Max_Retries__c : ' + oWebServiceSetting.Callout_Max_Retries__c);
            if (oNotificationSAPLog.Retries__c >= oWebServiceSetting.Callout_Max_Retries__c){

                // Send an email to inform that also the retries have failed
                sendEmail(EMAIL_TYPE.MAX_RETRIES, oNotificationSAPLog);
    
            }else if (oNotificationSAPLog.Retries__c < oWebServiceSetting.Callout_Max_Retries__c){

                if (oWebServiceSetting.Callout_Max_Retries__c > 0){
                    
                    // Get the retry minutes
                    List<String> lstRetryMinute = clsUtil.isNull(oWebServiceSetting.Callout_Retry_Minutes__c, '').split(',');
                    Integer iMinutesRetry = 60; // Default Value
                    if (lstRetryMinute.size() > 0){
                        if (oNotificationSAPLog.Retries__c < lstRetryMinute.size()){
                            try{ iMinutesRetry = Integer.valueOf(lstRetryMinute[oNotificationSAPLog.Retries__c.intValue()]); }catch(Exception oEX){}
                        }
                    }

                    // Schedule a Retry in case of a failure 
                    sc_NotificationSAP_Retry.scheduleAPEX(oNotificationSAPLog, 0, 0, iMinutesRetry, 0);
                }
            }

        }catch(Exception oEX){
            clsUtil.debug('Error occured in ut_Log.scheduleRetryNotificationSAP on line ' + oEX.getLineNumber() + ' : ' + oEX.getMessage());
        }

    }
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
    // Get the NotificationSAPLog records based on the SFDC Record Id and Status
    //----------------------------------------------------------------------------------------
    public static List<NotificationSAPLog__c> getNotificationSAPLog(String tRecordId, String tStatus){

        List<NotificationSAPLog__c> lstNotificationSAPLog = new List<NotificationSAPLog__c>();

        try{

            clsUtil.bubbleException();
            
            String tWHERE = '(Record_ID__c = \'' + tRecordId + '\' OR Record_SAPID__c = \'' + tRecordId + '\') AND Status__c = \'' + tStatus + '\'';
            if(tStatus == 'NEW') tWHERE += ' AND Retries__c != -1';
            
            lstNotificationSAPLog = ut_Log.loadNotificationSAPLog(tWhere, '');

        }catch(Exception oEX){
            clsUtil.debug('Error occured in ut_Log.getNotificationSAPLog on line ' + oEX.getLineNumber() + ' : ' + oEX.getMessage());
        }

        return lstNotificationSAPLog;

    }
    //----------------------------------------------------------------------------------------

    
    //----------------------------------------------------------------------------------------
    // Send Error email 
    //----------------------------------------------------------------------------------------
    @TestVisible private static void sendEmail(EMAIL_TYPE oEmailType, NotificationSAPLog__c oNotificationSAPLog){

        clsUtil.debug('ut_Log.sendEmail - oEmailType : ' + oEmailType);        
        clsUtil.debug('ut_Log.sendEmail - oNotificationSAPLog : ' + oNotificationSAPLog);        
        if (oEmailType == EMAIL_TYPE.MAX_RETRIES){

            // Get the Web Service Setting data from the Custom Setting
            WebServiceSetting__c oWebServiceSetting = bl_NotificationSAP.loadWebServiceSetting(oNotificationSAPLog.WebServiceName__c);

            if (clsUtil.isNull(oWebServiceSetting.Callout_Failure_Email_Address__c, '') != ''){
                List<String> lstTOAddress = oWebServiceSetting.Callout_Failure_Email_Address__c.split(',');
                String tSubject = 'Notification to SAP "' + oNotificationSAPLog.Name + '" failed after ' + clsUtil.isDecimalNull(oWebServiceSetting.Callout_Max_Retries__c, 0).intValue() + ' retries.';
                String tBody_Plain = 'Notification to SAP "' + oNotificationSAPLog.Name + '" failed after ' + clsUtil.isDecimalNull(oWebServiceSetting.Callout_Max_Retries__c, 0).intValue() + ' retries.';
                String tBody_HTML = 'Notification to SAP "' + oNotificationSAPLog.Name + '" failed after ' + clsUtil.isDecimalNull(oWebServiceSetting.Callout_Max_Retries__c, 0).intValue() + ' retries.';

                clsUtil.bSendEmail(lstTOAddress, tSubject, tBody_Plain, tBody_HTML);
            }else{
                clsUtil.debug('ut_Log.sendEmail - No email address specified.');            
            }

        }

    }
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
    // LOAD THE NOTIFICATIONSAPLOG RECORDS BASED ON THE PROVIDED WHERE AND ORDER BY
    //----------------------------------------------------------------------------------------
    public static String tSOQL_NotificationSAPLog(){

        String tSOQL = 'SELECT';
            tSOQL += ' Id, Name';
            tSOQL += ' , SFDC_Object_Name__c, SFDC_Fieldname_ID__c, SFDC_Fieldname_SAPID__c, Record_ID__c, Record_SAPID__c';
            tSOQL += ' , EndpointUrl__c, Authorization_Header__c, Request__c, Response__c, Status__c, Direction__c, Error_Description__c, User__c';
            tSOQL += ' , WM_Object_Name__c, WM_Operation__c, WM_Process__c';
            tSOQL += ' , Retries__c, Retry_Cron_Expression__c, Retry_DateTime__c, Retry_Cron_Job_Name__c, Retry_Cron_Job_Id__c, Send_Date__c, WebServiceName__c';
            tSOQL += ' , Outbound_Message_Request__c, Outbound_Message_Response__c';
        tSOQL += ' FROM NotificationSAPLog__c';

        return tSOQL;

    }
    public static List<NotificationSAPLog__c> loadNotificationSAPLog(String tWhere, String tOrderBy){
        
        List<NotificationSAPLog__c> lstNotificaionSAPLog = new List<NotificationSAPLog__c>();
        try{
            clsUtil.bubbleException();

            String tSOQL = tSOQL_NotificationSAPLog();
            if (clsUtil.isNull(tWhere, '') != ''){
                if (!tWhere.toUpperCase().contains('WHERE')){
                    tSOQL += ' WHERE';
                }
                tSOQL += ' ' + tWhere;
            }
            if (clsUtil.isNull(tOrderBy, '') != ''){
                if (!tOrderBy.toUpperCase().contains('ORDER BY')){
                    tSOQL += ' ORDER BY';
                }
                tSOQL += ' ' + tOrderBy;
            }

            lstNotificaionSAPLog = database.query(tSOQL);

        }catch(Exception oEX){
            clsUtil.debug('Error occured in bl_NotificationSAP.loadNotificationSAPLog on line ' + oEX.getLineNumber() + ' : ' + oEX.getMessage());
        }

        return lstNotificaionSAPLog;

    }
    //----------------------------------------------------------------------------------------


}