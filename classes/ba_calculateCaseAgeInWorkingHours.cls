/*
* This batch gets all Open Cases with OMA Record Type and calculates their Age
* in working days. To be executed by a schedulable class every day.
* Created on 17/06/2014 by Jesus
*/
global without sharing class ba_calculateCaseAgeInWorkingHours 
								implements Database.Batchable<SObject>, Database.Stateful{
	
	global List<String> errors = new List<String>();
	
	global Database.QueryLocator start(Database.BatchableContext BC){
      
      	Set<Id> omaRecordTypes = bl_CaseAgeCalculation.omaRecordTypes;
      
      	String query = 'Select Id from Case where RecordTypeId IN:omaRecordTypes AND isClosed = false';
            
    	return Database.getQueryLocator(query);
   	}

	global void execute(Database.BatchableContext BC, List<SObject> scope){
						
		try{
		
			List<Case> caseRecords = (List<Case>) scope;
			
			//for(Case cs : caseRecords){
			//	cs.Working_Days_Age__c = bl_CaseAgeCalculation.getOpenCaseAgeInWorkingDays(cs); 
			//}
			
			//Trigger a dummy update to launch the calculation logic. The AllOrNone flag is set to false, so even if a Case cannot be updated
			//the rest will stil be updated successfully.			
			for(Database.SaveResult res : Database.update(caseRecords, false)){
				
				if(res.isSuccess() == false){
					
					//If there was an error, get the message and recordId and notify the Admins afterwards.
					for(Database.Error err : res.getErrors()){
						
						errors.add('Error or record with Id: '+res.getId()+'. '+err.getMessage());						
					}	
				}			
			}
			
		}catch(Exception e){
									
			errors.add(e.getMessage()+' / '+e.getStacktraceString());
			
			throw e;
		}
	}

   	global void finish(Database.BatchableContext BC){
   		   		  		
   		if(errors.size()>0){
   			
   			List<GroupMember> notifyUsers = [Select UserOrGroupId from GroupMember where Group.Name = 'Admins'];
   			
   			if(notifyUsers.size()>0){
   				
   				List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();
   				
   				String errorText = '';
   				
   				for(String errorMessage : errors){
   					errorText += errorMessage + ' /---/ ';
   				}
   				
   				for(GroupMember userMember : notifyUsers){
   				
	   				Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
					mail.setTargetObjectId(userMember.UserOrGroupId);
					mail.setSaveAsActivity(false);
					mail.setSubject('There have been errors in the Case Age calcualtion scheduled process' );
					mail.setPlainTextBody('Error details : '+ errorText);
					
					messages.add(mail);
   				}
				
				Messaging.sendEmail(messages);   				
   			}   				
   		}
   	}
	
}