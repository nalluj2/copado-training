@isTest
private class Test_ctrl_Contract_Repository_Search {
    
    private static testmethod void testSearchController(){
        
        Account testAcc1 = new Account();
        testAcc1.Name = 'Test Account 1';
        testAcc1.Account_Country_vs__c = 'BELGIUM';
                
        Account testAcc2 = new Account();
        testAcc2.Name = 'Test Account 2';
        testAcc2.Account_Country_vs__c = 'BELGIUM';
        
        insert new List<Account>{testAcc1, testAcc2};
            
        Contract_Repository__c contract = new Contract_Repository__c();
        contract.Account__c = testAcc1.Id;
        contract.Primary_Contract_Type__c = 'Rebate agreement';
        contract.Rebate_Type__c = 'Straight Unit';
        contract.MPG_Code__c = 'A1;A2;A3';
        //contract.Payment_Options__c = 'Credit Note';
        contract.put('Payment_Options__c','Credit Note');
        contract.Pre_Payment_Terms__c = 'Final Payment';
        contract.Payment_Period__c = 'Monthly';     
        contract.Original_Valid_From_Date__c = Date.today();
        contract.Original_Valid_To_Date__c = Date.today().addYears(1);      
        insert contract;
        
        Test.startTest();
        
        //All Contracts
        List<ctrl_Contract_Repository_Search.ContractRepository> response = ctrl_Contract_Repository_Search.searchContracts(null, null, null, null, null, null, null, null);
        System.assert(response.size() == 1);
        
        //Contract for Account 1
        response = ctrl_Contract_Repository_Search.searchContracts(null, testAcc1.Id, null, null, null, null, null, null);
        System.assert(response.size() == 1);
        
        //Contract for Account 2
        response = ctrl_Contract_Repository_Search.searchContracts(null, testAcc2.Id, null, null, null, null, null, null);
        System.assert(response.size() == 0);
        
        //Rebate agreement contracts
        response = ctrl_Contract_Repository_Search.searchContracts(null, null, 'Rebate agreement', null, null, null, null, null);
        System.assert(response.size() == 1);
        
        //Capital Equipment contracts
        response = ctrl_Contract_Repository_Search.searchContracts(null, null, 'Capital Equipment', null, null, null, null, null);
        System.assert(response.size() == 0);
        
        Id [] fixedSearchResults= new Id[1];
        fixedSearchResults[0] = contract.Id;
        Test.setFixedSearchResults(fixedSearchResults);
        
        //Search contracts
        response = ctrl_Contract_Repository_Search.searchContracts('Test', null, null, null, null, null, null, null);
        System.assert(response.size() == 1);            
    }
    
    private static testmethod void testGetPicklistValues(){
        
        Test.setMock(HttpCalloutMock.class, new mock_Interface());
        
        ctrl_Contract_Repository_Search.getPicklistValues();
    }
    
    private class mock_Interface implements HttpCalloutMock {

        public HTTPResponse respond(HTTPRequest req) {
            
            HttpResponse httpResponse = new HttpResponse();
            httpResponse.setHeader('Content-Type', 'application/json');
            httpResponse.setStatusCode(200);
                
            ctrl_Contract_Repository_Search.PicklistValuesResponse response = new ctrl_Contract_Repository_Search.PicklistValuesResponse();             
            response.picklistFieldValues = new Map<String, ctrl_Contract_Repository_Search.FieldPicklistValues>(); 
                        
            ctrl_Contract_Repository_Search.FieldPicklistValues fieldPicklists = new ctrl_Contract_Repository_Search.FieldPicklistValues();             
            fieldPicklists.values = new List<ctrl_Contract_Repository_Search.PicklistValue>(); 
            
            for(Integer i = 0; i < 10; i++){
                    
                ctrl_Contract_Repository_Search.PicklistValue pickValue = new ctrl_Contract_Repository_Search.PicklistValue('picklistValue' + i, 'Picklist Label ' + i);                
                fieldPicklists.values.add(pickValue);
            }
                
            response.picklistFieldValues.put('Primary_Contract_Type__c', fieldPicklists);
            response.picklistFieldValues.put('MPG_Code__c', fieldPicklists);
                
            httpResponse.setBody(JSON.serialize(response));
            
            return httpResponse;
        }
    }   
    
}