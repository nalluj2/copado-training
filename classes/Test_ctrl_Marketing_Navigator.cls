@isTest
private class Test_ctrl_Marketing_Navigator {
    
    private static testmethod void testRedirectSalesNavigator(){
    	    	    	
    	SICA_Endpoints__c endpointMarketing = new SICA_Endpoints__c(Name = 'TableauMarketing', URL__c = 'https://tableau.marketing.test.com');
    	    	
    	insert endpointMarketing;
    	    	    	    	
    	PageReference contextPage = Page.Marketing_Navigator;
		
		Test.setCurrentPage(contextPage);
		
		Test.StartTest();
		
		ctrl_Marketing_Navigator controller = new ctrl_Marketing_Navigator();
		
		PageReference result = controller.openMarketingNavigator();		
		System.assert(result.getUrl() == 'https://tableau.marketing.test.com');		
    }
}