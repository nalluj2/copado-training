@RestResource(urlMapping='/ProceduralSolutionKitProductService/*')
global with sharing class ws_ProceduralSolutionKitProductService {
    
    @HttpPost
	global static String doPost() {
 		
 		RestRequest req = RestContext.request;
			
		System.debug('post called '+ req);
		System.debug('post requestBody '+ req.requestBody.toString());
			
		String body = req.requestBody.toString();
		ProceduralSolutionKitProductRequest proceduralSolutionKitProductRequest = (ProceduralSolutionKitProductRequest) System.Json.deserialize(body, ProceduralSolutionKitProductRequest.class);
			
		System.debug('post requestBody ' + proceduralSolutionKitProductRequest);
			
		Procedural_Solutions_Kit_Product__c proceduralSolutionKitProduct = ProceduralSolutionKitProductRequest.ProceduralSolutionKitProduct;

		ProceduralSolutionKitProductResponse resp = new ProceduralSolutionKitProductResponse();

		//We catch all exception to assure that 
		try{
				
			resp.ProceduralSolutionKitProduct = bl_ProceduralSolutionKit.saveProceduralSolutionKitProduct(proceduralSolutionKitProduct);
			resp.id = ProceduralSolutionKitProductRequest.id;
	
			resp.success = true;
			
		}catch (Exception e){
			
			resp.message = e.getMessage();
			resp.success = false;
			RestResponse res = RestContext.response;
			res.statusCode = 400;
		}
 
		bl_Mobilesync.storeRequest(UserInfo.getUserId(), 'ProceduralSolutionKitProduct', req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
  			
		return System.JSON.serialize(resp);		
	}

	@HttpDelete
	global static String doDelete(){
		RestRequest req = RestContext.request;
			
		System.debug('post called ' + req);
		System.debug('post requestBody ' + req.requestBody.toString());
			
		String body = req.requestBody.toString();
		ProceduralSolutionKitProductDeleteRequest proceduralSolutionKitProductDeleteRequest = (ProceduralSolutionKitProductDeleteRequest) System.Json.deserialize(body, ProceduralSolutionKitProductDeleteRequest.class);
			
		System.debug('post requestBody ' + proceduralSolutionKitProductDeleteRequest);
			
		Procedural_Solutions_Kit_Product__c proceduralSolutionKitProduct = ProceduralSolutionKitProductDeleteRequest.ProceduralSolutionKitProduct;
		
		ProceduralSolutionKitProductDeleteResponse resp = new ProceduralSolutionKitProductDeleteResponse();
		
		List<Procedural_Solutions_Kit_Product__c> proceduralSolutionKitProductsToDelete = [select id, mobile_id__c from Procedural_Solutions_Kit_Product__c where Mobile_ID__c = :proceduralSolutionKitProduct.Mobile_ID__c];
		
		try{
			
			if (proceduralSolutionKitProductsToDelete != null && proceduralSolutionKitProductsToDelete.size() > 0){	
				
				Procedural_Solutions_Kit_Product__c proceduralSolutionKitProductToDelete = proceduralSolutionKitProductsToDelete.get(0);
				bl_ProceduralSolutionKit.deleteProceduralSolutionKitProduct(proceduralSolutionKitProductToDelete);
				resp.ProceduralSolutionKitProduct = proceduralSolutionKitProductToDelete;
				resp.id = proceduralSolutionKitProductDeleteRequest.id;
				resp.success = true;
				
			}else{
				
				resp.ProceduralSolutionKitProduct = proceduralSolutionKitProduct;
				resp.id = proceduralSolutionKitProductDeleteRequest.id;
				resp.message='ProceduralSolutionKitProduct not found';
				resp.success = true;
			}
			
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
			RestResponse res = RestContext.response;
			res.statusCode = 400;
		}
			
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'ProceduralSolutionKitProductDelete', req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
			
		return System.JSON.serialize(resp);
	}

	global class ProceduralSolutionKitProductRequest{
		public Procedural_Solutions_Kit_Product__c ProceduralSolutionKitProduct {get;set;}
		public String id{get;set;}
	}
	
	global class ProceduralSolutionKitProductDeleteRequest{
		public Procedural_Solutions_Kit_Product__c ProceduralSolutionKitProduct {get;set;}
		public String id{get;set;}
	}
		
	global class ProceduralSolutionKitProductDeleteResponse{
		public Procedural_Solutions_Kit_Product__c ProceduralSolutionKitProduct {get;set;}
		public String id{get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}

	global class ProceduralSolutionKitProductResponse{
		public Procedural_Solutions_Kit_Product__c ProceduralSolutionKitProduct {get;set;}
		public String id{get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}
}