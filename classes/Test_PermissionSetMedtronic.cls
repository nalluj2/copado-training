@isTest
private without sharing class Test_PermissionSetMedtronic {
    
    private static testmethod void testUserPermissions(){
    	
    	PermissionSetAssignment userPermission = [Select AssigneeId, PermissionSet.Name from PermissionSetASsignment where Assignee.IsActive = true LIMIT 1];
    	
    	User testUser = new User(Id = userPermission.AssigneeId);
    	
    	System.runAs(testUser){
    		
    		System.assert(PermissionSetMEdtronic.hasPermissionSet(userPermission.PermissionSet.Name) == true);
    		
    		System.assert(PermissionSetMEdtronic.hasPermissionSet('Non_Existing_Permission_Set') == false);
    	}
    }
}