/*
 *      Description : This is the Test Class for the APEX Controller Extension ctrlExt_AccountAssetList
 *      Version     : 1.0
 *      Author      : Bart Caelen
 *      Date        : 201403
*/
@isTest private class Test_ctrlExt_AccountAssetList {

    @isTest static void test_ctrlExt_AccountAssetList_ALL() {
		Integer iCounter = 0;
		Boolean bTest = false;
        String tTest = '';
		
	    //---------------------------------------
        // Create Test Data
        //---------------------------------------
        clsTestData.createCompanyData();
        clsTestData.createBusinessUnitData();
        clsTestData.createSubBusinessUnitData();
        clsTestData.createCountryData();
        clsTestData.tCountry_Account = 'BELGIUM';
        clsTestData.createAccountData();
        clsTestData.createContactData();
        clsTestData.createBURelatedListSetupData_Asset();
        clsTestData.createProductData();		
	    //---------------------------------------


	    //---------------------------------------
	    // GET TEST USER
	    //---------------------------------------
        Profile oProfile = [SELECT ID FROM Profile WHERE Name = 'System Administrator MDT' LIMIT 1];
        Profile oProfile_PowerPartner = [SELECT ID FROM Profile WHERE Name = 'CAN DiB RAC Agent' LIMIT 1];    

        User oUser = new User(
            profileid = oProfile.Id            
        	, alias = 'test1'
        	, email = 'test1sfdc@testorg.medtronic.com'             
            , emailencodingkey = 'UTF-8'
            , lastname = 'test1'             
            , languagelocalekey = 'en_US'             
            , localesidkey = 'en_US'
            , timezonesidkey = 'America/Los_Angeles'             
            , CountryOR__c = clsTestData.tCountry_Account         
            , Country = clsTestData.tCountry_Account
            , username = 'test1sfdc@testorg.medtronic.com'
            , Alias_unique__c = 'test1_alias'
            , Company_Code_Text__c = 'Eur'
            , User_Business_Unit_vs__c = 'CRHF'
            , Job_Title_vs__c = 'Technical Consultant'
        );  
        insert oUser; 

        User oUser_PowerPartner = new User(
            profileid = oProfile_PowerPartner.Id            
        	, alias = 'test2'
        	, email = 'test2sfdc@testorg.medtronic.com'             
            , emailencodingkey = 'UTF-8'
            , lastname = 'test2'             
            , languagelocalekey = 'en_US'             
            , localesidkey = 'en_US'
            , timezonesidkey = 'America/Los_Angeles'             
            , CountryOR__c = clsTestData.tCountry_Account          
            , Country = clsTestData.tCountry_Account    
            , username = 'test2sfdc@testorg.medtronic.com'
            , Alias_unique__c = 'test2_alias'
            , Company_Code_Text__c = 'Eur'
            , User_Business_Unit_vs__c = 'CRHF'
            , Job_Title_vs__c = 'Technical Consultant'
            , ContactID  = clsTestData.oMain_Contact.Id
        );  
        insert oUser_PowerPartner; 
	    //---------------------------------------

	    //---------------------------------------
	    // Perform Testing
	    //---------------------------------------
        System.runAs(oUser){

            clsTestData.oMain_Account = 
                [
                    SELECT
                        Id, Name, Account_Country_vs__c
                    FROM
                        Account
                    WHERE 
                        Id = :clsTestData.oMain_Account.Id
                ];

            PageReference oPage = new PageReference('/AccountAssetList?Id=' + clsTestData.oMain_Account.Id);
	        ApexPages.StandardController oSTDCTRL = new ApexPages.StandardController(clsTestData.oMain_Account);
	        ctrlExt_AccountAssetList oCTRL = new ctrlExt_AccountAssetList(oSTDCTRL);
	        
            oCTRL.createNewData();

        }

		System.runAs(oUser_PowerPartner){

	        ApexPages.StandardController oSTDCTRL = new ApexPages.StandardController(clsTestData.oMain_Account);
	        ctrlExt_AccountAssetList oCTRL = new ctrlExt_AccountAssetList(oSTDCTRL);
	        
        }

	    //---------------------------------------

    }
}