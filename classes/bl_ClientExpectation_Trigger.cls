//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 13-02-2019
//  Description      : APEX Class - Business Logic for tr_ClientExpectation
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public class bl_ClientExpectation_Trigger  {


    //--------------------------------------------------------------------------------------------------------------------
	// Populate the field Expectation_Ready_Date__c on Client_Expectation__c if it's empty or if 
	//	Expectation_Source_Date__c and/or Expectation_Realization_days__c is changed.
	//	This logic is only implemented for IHS Managed Service Opportunities or IHS Consulting Opportunities
    //--------------------------------------------------------------------------------------------------------------------
	public static void IHS_SetExpectationReadyDate(List<Client_Expectation__c> lstTriggerNew, Map<Id, Client_Expectation__c> mapTriggerOld){

        if (bl_Trigger_Deactivation.isTriggerDeactivated('clientExp_IHS_SetExpectationReadyDate')) return;

        Set<Id> setID_RecordType_Opportunity = new Set<Id>{clsUtil.getRecordTypeByDevName('Opportunity', 'EMEA_IHS_Managed_Services').Id, clsUtil.getRecordTypeByDevName('Opportunity', 'EMEA_IHS_Consulting_Opportunity').Id}; 

		Set<Id> setID_Opportunity = new Set<Id>();
		for (Client_Expectation__c oClientExpectation : lstTriggerNew){
			setID_Opportunity.add(oClientExpectation.Opportunity__c);
		}

		// Get the Opportunity Records that are in scope based on the Record Type
		Map<Id, Opportunity> mapOpportunity = new Map<Id, Opportunity>([SELECT Id FROM Opportunity WHERE Id in :setID_Opportunity AND RecordTypeId in :setID_RecordType_Opportunity]);

		for (Client_Expectation__c oClientExpectation : lstTriggerNew){

			if (!mapOpportunity.containsKey(oClientExpectation.Opportunity__c)) continue;	// Only process Client_Expectation__c records that are related to Opportunity records that are in scope

			// Populate the Expectation_Ready_Date__c if it's empty or if Expectation_Source_Date__c and/or Expectation_Realization_days__c is changed
			Boolean bSetExpectationReadyDate = false;
			if (mapTriggerOld == null){

				if (oClientExpectation.Expectation_Ready_Date__c == null) bSetExpectationReadyDate = true;
			
			}else{

				Client_Expectation__c oClientExpectation_OLD = mapTriggerOld.get(oClientExpectation.Id);

				if (
					(oClientExpectation.Expectation_Source_Date__c != oClientExpectation_OLD.Expectation_Source_Date__c)
					|| (oClientExpectation.Expectation_Realization_days__c != oClientExpectation_OLD.Expectation_Realization_days__c)
					|| (oClientExpectation.Expectation_Ready_Date__c == null)
				){
					bSetExpectationReadyDate = true;
				}

			}

			if (bSetExpectationReadyDate){
			
				Date dExpectationReadyDate = null;
				
				if (
					(oClientExpectation.Expectation_Source_Date__c != null)
					&& (oClientExpectation.Expectation_Realization_days__c != null)
				){

					dExpectationReadyDate = oClientExpectation.Expectation_Source_Date__c.addDays(Integer.valueOf(oClientExpectation.Expectation_Realization_days__c)); 

				}
						
				oClientExpectation.Expectation_Ready_Date__c = dExpectationReadyDate;

			}

		}

	}
    //--------------------------------------------------------------------------------------------------------------------

}
//--------------------------------------------------------------------------------------------------------------------