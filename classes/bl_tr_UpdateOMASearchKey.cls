/*
 *      Created Date : 30 -May-2013
 *      Description : This is the class is called by trigger trgUpdateBUCaseRecorTypeField to 
                      update the OMA Search Key field.
 *
 *      Author = Kaushal Singh
 *      
 */
public class bl_tr_UpdateOMASearchKey{

    public static void OMASearckKey(List<Case> cases){
    	
        if(GTutil.boolCaseOMASearchKey){
            GTutil.boolCaseOMASearchKey=false;
             
            Set<Id> SetCaseId = new Set<Id>(); 
                        
            Map<Id,Schema.RecordTypeInfo> caseRecordTypesById = Schema.SObjectType.Case.getRecordTypeInfosById();
            
            for(Case inputCase : cases){
            	
            	if(caseRecordTypesById.get(inputCase.RecordTypeId).getDeveloperName().startsWith('OMA')) setCaseId.add(inputCase.Id);
            }
            
            if(setCaseId.size() > 0){
            	            
	            List<Case> lstToUpdateCase=new List<Case>();
	           
	            List<Case> lstOfCase=new List<Case>();
	            lstOfCase=[
	                       Select Id,ContactId,Therapy_Picklist__c,OMA_Search_Key__c,Therapy_Use__c,Suggested_Keywords__c,Contact.Name,Contact.Email,Contact.Phone 
	                       from Case where id in:SetCaseId];
	            //Avoid executing this code when the Case is not of type OMA          
	            if(lstOfCase.isEmpty()) return;
	            	            
	            for(Case c:lstOfCase){
	            	
	            	Contact caseContact = c.Contact;
	            	
	            	if(caseContact != null){
	            	
	                    if(c.Therapy_Picklist__c==null){
	                        c.Therapy_Picklist__c='';    
	                    }
	                    if(c.Therapy_Use__c==null){
	                        c.Therapy_Use__c='';    
	                    }
	                    if(c.Suggested_Keywords__c==null){
	                        c.Suggested_Keywords__c='';    
	                    }
	                    
	                    c.OMA_Search_Key__c=c.Therapy_Picklist__c+';'+' '+ c.Therapy_Use__c+';'+' '+ c.Suggested_Keywords__c+';'+' '+caseContact.Name+';'+' '+caseContact.Email+';'+' '+caseContact.Phone;
	                    
	                    lstToUpdateCase.add(c);
	                }	                    
	            }
	            
	            if(lstToUpdateCase.size()>0){
	                update lstToUpdateCase;    
	            } 
            }
        }    
    }
}