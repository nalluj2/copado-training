/*
 * Description      : Interface class to send/receive Opportunities to/from SAP
 * Author           : Rudy Coninck
 * Created Date     : 03-07-2013
 * Change Log       : 16-07-2013 - Patrick Brinksma - Continue development
 */
global class ws_OpportunitySAPService {

    // BU to lookup Opportunity Record Type in Price Book Mgmt
    public static final String RECORD_TYPE_BU = 'Diabetes';
    public static final Integer TRIM_ERROR_MSG_LENGTH = 132;
    // Price book id to be used to create Opportunity Products
    public static Id priceBookId;
    
    /*
     * Description      : WS Method to Create / Update Opportunity
     * Author           : Rudy Coninck
     * Created Date     : 03-07-2013
     */
    webservice static SAPOpportunity upsertOpportunity(SAPOpportunity sapOpportunityToUpsert){
        // Structure for return as response
        SAPOpportunity sapOpp;
        // SAP Opportunity Id
        String sapOppId;
        System.debug('*** sapOpp upsertOpportunity: ' + sapOpportunityToUpsert);
        String status;
        // Wrap in try catch to construct error message in case of Exception
        try{
            // Validate if message is allowed to be processed, if not throw custom exception
            if (!ws_SharedMethods.processInboundMessage('SAP Opportunity')){
                throw new ws_Exception('SAP Inbound Messages are currently not processed for this interface!');             
            }
            // Escape single quotes as this value is used in SOQL query and externally determined
            sapOppId = String.escapeSingleQuotes(sapOpportunityToUpsert.SAP_ID);
            // Create Opportunity sObject for upsert based on input
            Opportunity opp = mapSapOpportunityToOpportunity(sapOpportunityToUpsert);
            // Upsert Opportunity based on SAP ID
            System.debug('*** Opp before upsert: ' + opp);
            upsert opp SAP_ID_Text__c;          
            // Create Opportunity Line Item sObjects for upsert based on input
            List<OpportunityLineItem> oppLineItems = mapSAPOpportunityItemsToOpportunityLineItems(opp, sapOpportunityToUpsert.OPPORTUNITY_ITEMS);
            // Upsert Opportunity based on SAP ITEM ID
            upsert oppLineItems SAP_Item_GUID__c;
            // Retrieve Data again to get joined values
            List<Opportunity> listOfOppty = ws_SharedMethods.getOpportunityDetails(opp.Id);
            oppLineItems = ws_SharedMethods.getOpportunityLineItemDetails(opp.Id);
            // reconvert to sap opp for reply with sfdc ids
            sapOpp = mapOpportunityToSAPOpportunity(listOfOppty[0], oppLineItems);
            status='TRUE';            
        } catch (Exception ex){
            // Construct error message 
            String errMsg = (ex.getMessage()).left(TRIM_ERROR_MSG_LENGTH);
            // Full stack trace
            String errTrace = ex.getMessage() + ' :Stack Trace: ' +  ex.getStackTraceString();
            // Return original Object including status and err message
            sapOpportunityToUpsert.SFDC_ERR_STACKTRACE = errTrace;
            sapOpportunityToUpsert.DISTRIBUTION_STATUS = 'Error in process';
            sapOpportunityToUpsert.DIST_ERROR_DETAILS = errMsg;
            sapOpp = sapOpportunityToUpsert;
            status='FALSE';
        }   


        bl_Opportunity.logOutboundMessage(null, sapOpportunityToUpsert.SAP_ID, sapOpportunityToUpsert.toString(), sapOpp.toString(),status,'upsertOpportunity');

        // Debug return object        
        System.debug('*** sapOpp: ' + sapOpp);
        
        return sapOpp;
    }

    /*
     * Description      : WS Method to Update Opportunity acknowledgement to update x-ref ids or if error occurred in SAP, update details
     * Author           : Patrick Brinksma
     * Created Date     : 13-08-2013
     */
    webservice static SAPOpportunityAck updateOpportunityAcknowledgement(SAPOpportunityAck sapOpportunityAckToUpdate){
        // SAP Opportunity Id
        Id opptyId;  
        String status;  
        // Wrap in try catch to construct error message in case of Exception
        try{
            
            System.debug('SAPOpportunityAck '+sapOpportunityAckToUpdate);
            // Validate if message is allowed to be processed, if not throw custom exception
            if (!ws_SharedMethods.processInboundMessage('SAP Opportunity')){
                throw new ws_Exception('SAP Inbound Messages are currently not processed for this interface!');             
            }
            opptyId = sapOpportunityAckToUpdate.OPPORTUNITYID;
            if (opptyId == null){
                throw new ws_Exception('Opportunity Id is required!');
            }
            // Escape single quotes as this value is used in SOQL query and externally determined
            opptyId = String.escapeSingleQuotes(sapOpportunityAckToUpdate.OPPORTUNITYID);                       
            // Retrieve Opportunity
            List<Opportunity> listOfOppty = ws_SharedMethods.getOpportunityDetails(opptyId);
            if (listOfOppty.isEmpty()){
                throw new ws_Exception('Opportunity with Id \'' + opptyId + '\' could not be found!');
            }   
            // Update Opportunity SAP ID fields
            Opportunity thisOppty = listOfOppty[0];
            thisOppty.SAP_ID_Text__c = sapOpportunityAckToUpdate.SAP_ID;
            thisOppty.SAP_Order_GUID__c = sapOpportunityAckToUpdate.SAP_ORDER_GUID;
            // Set Distribution Lock to false, notifications on change may be sent out
            thisOppty.Distribution_Lock__c = false;
            // Distribution details 
            thisOppty.Distribution_Status_Text__c = sapOpportunityAckToUpdate.DISTRIBUTION_STATUS;
            if (sapOpportunityAckToUpdate.DIST_ERROR_DETAILS <> null){
                thisOppty.Dist_Err_Details__c = sapOpportunityAckToUpdate.DIST_ERROR_DETAILS.left(255);
                status='FALSE';
            }else{
                //- BC - 20140625 - CR-2859 - Clear the Error field if there is no error - START
                thisOppty.Dist_Err_Details__c = '';
                //- BC - 20140625 - CR-2859 - Clear the Error field if there is no error - STOP
                status='TRUE';
            }            
            // Update opportunity
            update thisOppty;
            // Retrieve Opportunity Line Item
            List<OpportunityLineItem> listOfOpptyLine = ws_SharedMethods.getOpportunityLineItemDetails(opptyId);
            Map<Id, OpportunityLineItem> mapOfIdToOpptyLine = new Map<Id, OpportunityLineItem>();
            for (OpportunityLineItem thisOpptyLine : listOfOpptyLine){
                mapOfIdToOpptyLine.put(thisOpptyLine.Id, thisOpptyLine);
            }      
            
            if(sapOpportunityAckToUpdate.OPPORTUNITY_ITEMS != null){                      
                // Loop through line items and update SAP x-ref id fields
                for (SAPOpportunityItemAck thisSapOppItemAck : sapOpportunityAckToUpdate.OPPORTUNITY_ITEMS){
                    if (mapOfIdToOpptyLine.get(thisSapOppItemAck.OPPORTUNITYPRODUCTID) != null){
                        mapOfIdToOpptyLine.get(thisSapOppItemAck.OPPORTUNITYPRODUCTID).SAP_Item_GUID__c = thisSapOppItemAck.SAP_ITEM_GUID;
                        mapOfIdToOpptyLine.get(thisSapOppItemAck.OPPORTUNITYPRODUCTID).SAP_Item_ID_Text__c = thisSapOppItemAck.SAP_ITEM_ID;
                    }
                }
            }
            // update line items
            if (!mapOfIdToOpptyLine.isEmpty()){
                update mapOfIdToOpptyLine.values();
            }
        } catch (Exception ex){
            // Construct error message
            String errMsg = (ex.getMessage()).left(TRIM_ERROR_MSG_LENGTH);
            // Full stack trace
            String errTrace = ex.getMessage() + ' :Stack Trace: ' +  ex.getStackTraceString();
            // Return original Object including status and err message
            sapOpportunityAckToUpdate.SFDC_ERR_STACKTRACE = errTrace;
            sapOpportunityAckToUpdate.DISTRIBUTION_STATUS = 'Error in process';
            sapOpportunityAckToUpdate.DIST_ERROR_DETAILS = errMsg;
            status='FALSE';         
        }  
        bl_Opportunity.logOutboundMessage(sapOpportunityAckToUpdate.OPPORTUNITYID, sapOpportunityAckToUpdate.SAP_ID, sapOpportunityAckToUpdate.toString(), sapOpportunityAckToUpdate.toString(),status,'updateOpportunityAcknowledgement');
        
        return sapOpportunityAckToUpdate;       
    }
    
    /*
     * Description      : WS Method to Retrieve Opportunity
     * Author           : Rudy Coninck
     * Created Date     : 03-07-2013
     */ 
    webservice static SAPOpportunity getOpportunity(String opportunityId){
        SAPOpportunity sapOpp;
        // Validate Oppty Line Item attributes to sent back
        String status;
        try{
            if (opportunityId == null || opportunityId.trim() == ''){
                throw new ws_Exception('Opportunity Id is required!');
            }
            // Escape quotes as it is an externally received value
            opportunityId = String.escapeSingleQuotes(opportunityId);
            // Query for Opportunity
            List<Opportunity> listOfOpp = ws_SharedMethods.getOpportunityDetails(opportunityId);
            // If opportunity is not found, throw custom exception
            if (listOfOpp.isEmpty()){
                throw new ws_Exception('Opportunity with Id ' + opportunityId + ' could not be found!');
            }
            // Query for Opportunity Line Items
            List<OpportunityLineItem> oppLineItems = ws_SharedMethods.getOpportunityLineItemDetails(opportunityId);
            // Map response message
            sapOpp = mapOpportunityToSAPOpportunity(listOfOpp[0], oppLineItems);
            status='TRUE';
        } catch (Exception ex){
            // Construct error message
            String errMsg = (ex.getMessage()).left(TRIM_ERROR_MSG_LENGTH);
            String errTrace = ex.getMessage() + ' :Stack Trace: ' +  ex.getStackTraceString();
            // As the error is thrown, need to construct a new Opportunity to populate the response message
            sapOpp = mapOpportunityToSAPOpportunity(new Opportunity(
                                                                    Dist_Err_Details__c = errMsg, 
                                                                    Distribution_Status_Text__c = 'Error in process'
                                                                    ), 
                                                                    new List<OpportunityLineItem>(), true, errTrace);
           status='FALSE';  
        } 
              
        
        bl_Opportunity.logOutboundMessage(opportunityId, null, 'Get Opportunity '+opportunityId, sapOpp.toString(),status,'getOpportunity');
        
        
        // Debug return object        
        System.debug('Sap Opp '+sapOpp);
        
        return sapOpp;
    }
    
    /*
     * Description      : Map SFDC Oppty to SAP Message Oppty for reply message
     * Author           : Rudy Coninck
     * Created Date     : 03-07-2013
     */     
    public static SAPOpportunity mapOpportunityToSAPOpportunity(Opportunity opportunity, List<OpportunityLineItem> opportunityLineItems){
        return mapOpportunityToSAPOpportunity(opportunity, opportunityLineItems, false, null);
    }
    // Method includes properties for error handling
    public static SAPOpportunity mapOpportunityToSAPOpportunity(Opportunity opportunity, List<OpportunityLineItem> opportunityLineItems, Boolean bShowError, String sfdcStackTrace){
        // Response Object
        SAPOpportunity sapOpp = new SAPOpportunity();
        
        // Populate fields of response object
        sapOpp.OPPORTUNITYID = opportunity.id;
        sapOpp.SAP_ID = opportunity.SAP_ID_Text__c;
        sapOpp.SAP_ORDER_GUID = opportunity.SAP_Order_GUID__c;
        sapOpp.NAME = opportunity.name;
        sapOpp.NAME2 = opportunity.Name_2_Text__c;
        sapOpp.SAP_ACCOUNTID = opportunity.account.SAP_ID__c;
        
        sapOpp.DISTRIBUTION_STATUS = 'Successfully processed';
        // In case of an error thrown in this class, add details to response
        if (bShowError){
            sapOpp.DISTRIBUTION_STATUS = 'Error in process';
            sapOpp.DIST_ERROR_DETAILS = opportunity.Dist_Err_Details__c;
            sapOpp.SFDC_ERR_STACKTRACE = sfdcStackTrace;
        }else{
            //- BC - 20140625 - CR-2859 - Clear the Error fields if there is no error - START
            sapOpp.DIST_ERROR_DETAILS = '';
            sapOpp.SFDC_ERR_STACKTRACE = '';
            //- BC - 20140625 - CR-2859 - Clear the Error fields if there is no error - STOP
        } 
        // Validate if this is required in response!
        sapOpp.DISTRIBUTION_LOCK = opportunity.Distribution_Lock__c;
        sapOpp.SAP_CREATED_DATE = opportunity.SAP_Created_Date__c;
        sapOpp.SAP_LASTMODIFIED_DATE = opportunity.SAP_Last_Modified_Date__c;
        sapOpp.CREATED_DATE = opportunity.CreatedDate;
        sapOpp.LASTMODIFIED_DATE = opportunity.LastModifiedDate;
        sapOpp.CLOSEDATE = opportunity.CloseDate;
        sapOpp.SAP_PHASE = opportunity.SAP_Phase__c;
        sapOpp.SAP_STATUS = opportunity.SAP_Status__c;
        sapOpp.SAP_REASON = opportunity.SAP_Reason__c;
        sapOpp.TYPE = opportunity.Type;
        sapOpp.ORIGIN = opportunity.Origin_Text__c;
        sapOpp.DEPARTMENT = opportunity.Department_Text__c;
        sapOpp.CURRENCYISOCODE = opportunity.CurrencyIsoCode;
        sapOpp.DESCRIPTION = opportunity.Description;
        sapOpp.TRAINING_DATE = opportunity.Training_Date__c;
        sapOpp.TRIAL_START_DATE = opportunity.Trial_Start_Date__c;
        sapOpp.TRIAL_END_DATE = opportunity.Trial_End_Date__c;
        sapOpp.REQUESTED_DELIVERY_DATE = opportunity.Requested_Delivery_Date__c;
        sapOpp.SAP_SHIP_TO_ID = opportunity.SAP_Ship_To_ID__r.SAP_ID__c;
        sapOpp.SAP_SOLD_TO_ID = opportunity.SAP_Sold_To_ID__r.SAP_ID__c;
        sapOpp.SAP_SALESREP = opportunity.Owner.SAP_ID__c;
        sapOpp.SAP_PHYSICIAN = opportunity.Physician_Account_ID__r.SAP_ID__c;
        sapOpp.SAP_HEALTH_INSURANCE = opportunity.Primary_Health_Insurer__r.SAP_Id__c;
        // Patrck Brinksma - 03 Sep 2013 - Added per request
        sapOpp.COUNTRY = opportunity.Account.Account_Country_vs__c;
        
        // Map the Line Items to reply message  
        List<SAPOpportunityItem> sapOpportunityLineItems = new List<SAPOpportunityItem>();
        for (OpportunityLineItem lineItem : opportunityLineItems ){
            sapOpportunityLineItems.add(mapOpportunityLineItemToSAPOpportunityItem(lineItem));
        }
        // Add lines to structure
        sapOpp.OPPORTUNITY_ITEMS = sapOpportunityLineItems;
        
        return sapOpp;
        
    }

    /*
     * Description      : Map SFDC Oppty Line Item to SAP Message Oppty Line Item for reply message
     * Author           : Rudy Coninck
     * Created Date     : 03-07-2013
     */     
    public static SAPOpportunityItem mapOpportunityLineItemToSAPOpportunityItem(OpportunityLineItem oppLineItem){
        SAPOpportunityItem sapOppItem = new SAPOpportunityItem();
        
        sapOppItem.SAP_ITEM_ID = oppLineItem.SAP_Item_ID_Text__c;
        sapOppItem.SAP_ITEM_GUID = oppLineItem.SAP_Item_GUID__c;
        sapOppItem.SAP_ITEM_SERIALNO = oppLineItem.SAP_Item_SerialNo_Text__c;
        sapOppItem.OPPORTUNITYPRODUCTID = oppLineItem.Id;
        sapOppItem.PRODUCTCODE = oppLineItem.PricebookEntry.ProductCode;
        sapOppItem.QUANTITY = String.valueof(oppLineItem.Quantity);
        sapOppItem.UNIT_OF_MEASURE = oppLineItem.PricebookEntry.Product2.Unit_Of_Measure_Text__c;
        sapOppItem.SAP_ITEM_CATEGORY = oppLineItem.SAP_Item_Category_Text__c;
        sapOppItem.STATUS = oppLineItem.Status__c;
        sapOppItem.SAP_ITEM_SERIALNO = oppLineItem.SAP_Item_SerialNo_Text__c;
        sapOppItem.SAP_WARRANTY_START_DATE = oppLineItem.SAP_Warranty_Start_Date__c;
        sapOppItem.SAP_WARRANTY_END_DATE = oppLineItem.Warranty_End_Date__c;
        sapOppItem.SALES_PRICE = String.valueof(oppLineItem.UnitPrice);
        sapOppItem.SAP_CREATED_DATE = oppLineItem.SAP_Created_Date__c;
        sapOppItem.SAP_LASTMODIFIED_DATE = oppLineItem.SAP_Last_Modified_Date__c;
        sapOppItem.CREATEDDATE = oppLineItem.CreatedDate;
        sapOppItem.LASTMODIFIED_DATE = oppLineItem.LastModifiedDate;        
        
        return sapOppItem;
    }
    
    //TODO null checks, conversions, lookup, error handling
    
    public static Opportunity mapSapOpportunityToOpportunity(SAPOpportunity sapOpp){
        System.debug('*** sap physician passed: ' + sapOpp);
        /*
        if(sapOpp.SAP_PHYSICIAN==null){
            sapOpp.SAP_PHYSICIAN='null';
            System.debug('*** sap physician set to null ');
        }   */    
       
        // Opportunity to return
        Opportunity opportunity = new Opportunity();
        // Id for the correct recordtype
        Id oppRTId;
        // Set for all Accounts to be retrieved by SAP ID
        Set<String> setOfAccntSAPId = new Set<String>{};
        if (sapOpp.SAP_ACCOUNTID!=null && sapOpp.SAP_ACCOUNTID.length()>0){
            setOfAccntSAPId.add(sapOpp.SAP_ACCOUNTID);
        }
        if (sapOpp.SAP_SHIP_TO_ID!=null && sapOpp.SAP_SHIP_TO_ID.length()>0){
            setOfAccntSAPId.add(sapOpp.SAP_SHIP_TO_ID);
        }
        if (sapOpp.SAP_SOLD_TO_ID!=null && sapOpp.SAP_SOLD_TO_ID.length()>0){
            setOfAccntSAPId.add(sapOpp.SAP_SOLD_TO_ID);
        }
        if (sapOpp.SAP_HEALTH_INSURANCE!=null && sapOpp.SAP_HEALTH_INSURANCE.length()>0){
            setOfAccntSAPId.add(sapOpp.SAP_HEALTH_INSURANCE);
        }
        if (sapOpp.SAP_PHYSICIAN!=null && sapOpp.SAP_PHYSICIAN.length()>0){
            setOfAccntSAPId.add(sapOpp.SAP_PHYSICIAN);
        }

        //Set<String> setOfAccntSAPId = new Set<String>{sapOpp.SAP_ACCOUNTID, sapOpp.SAP_SHIP_TO_ID, sapOpp.SAP_SOLD_TO_ID, sapOpp.SAP_HEALTH_INSURANCE, sapOpp.SAP_PHYSICIAN};
        // Get all accounts, including ship to and sold to
        System.debug('Set of acc sap ids '+setOfAccntSAPId);
        List<Account> listOfAccount = [select Id, SAP_ID__c, Account_Country_vs__c, Account_Postal_Code__c from Account where SAP_ID__c in :setOfAccntSAPId];
        // Put them into a map
        Map<String, Account> mapOfSAPIdToAccnt = new Map<String, Account>();
        for (Account thisAccount : listOfAccount){
            if (thisAccount.SAP_ID__c!=null){
                mapOfSAPIdToAccnt.put(thisAccount.SAP_ID__c, thisAccount);
            }
        }
        // Validate if the Opportunity Account exists, otherwise throw error
        if (mapOfSAPIdToAccnt.get(sapOpp.SAP_ACCOUNTID) != null){
            Account oppAccnt = mapOfSAPIdToAccnt.get(sapOpp.SAP_ACCOUNTID); //fldkfldkfldkkfd
            //Implement exception countries that are also handled by CAN - CR-7835, domTomCountries
            String country = oppAccnt.Account_Country_vs__c;
            String postalCode = oppAccnt.Account_Postal_Code__c;
            boolean matchDomTomCountry = false;
            if (postalCode!=null){
                if (postalCode.startsWith('971') || postalCode.startsWith('972') || postalCode.startsWith('973') 
                    || postalCode.startsWith('974') || postalCode.startsWith('975') )
                {
                    matchDomTomCountry=true; 
                }
            }
            
            if (country.equals('FRANCE') && matchDomTomCountry){
                //Override so that this is handled as CANADA 
                country='CANADA'; 
            }
            
            //Hardcoded country to Netherlands in case is a EUR Opportunitiy, because all countries are sharing the same record type and pricebook.
            //The Netherlands is set as the Blue Print for Europe that's why it is used.
            if(country != 'CANADA') country = 'NETHERLANDS';
            
            System.debug('Account: '+oppAccnt+'Country: '+country+' domTomCountry:'+matchDomTomCountry);
            // Lookup Record type from Price Book Mgmt using a Like for case insensitive search
            List<Price_Book_Management__c> listOfPBMgmt = [select Price_Book_ID__c, Opportunity_Record_Type_ID__c from Price_Book_Management__c where Business_Unit__r.Name = :RECORD_TYPE_BU and Country__r.Name LIKE :country];
            if (!listOfPBMgmt.isEmpty()){
                oppRTId = listOfPBMgmt[0].Opportunity_Record_Type_ID__c;
                priceBookId = listOfPBMgmt[0].Price_Book_ID__c;
                System.debug('*** priceBookId: ' + priceBookId);
            } else {
                // Set error as RecordType could not be found
                throw new ws_Exception('Opportunity RecordType for Country ' + country + ' could not be found!');
            }
        } else {
            // Set error, as Account should be available
            throw new ws_Exception('Account with SAP ID ' + sapOpp.SAP_ACCOUNTID + ' could not be found!');
        }

        // Set opportunity fields
        
        // Set to success, error handling will change it accordingly.
        opportunity.Distribution_Status_Text__c = ' ';
        opportunity.Distribution_Lock__c = false;
        
        opportunity.RecordTypeId = oppRTId;
        opportunity.SAP_ID_Text__c = sapOpp.SAP_ID;
        opportunity.SAP_Order_GUID__c = sapOpp.SAP_ORDER_GUID;
        opportunity.name = sapOpp.NAME;
        opportunity.Name_2_Text__c = sapOpp.NAME2;
        opportunity.accountId = mapOfSAPIdToAccnt.get(sapOpp.SAP_ACCOUNTID).Id;
        
        // As according to mapping document, Automatic Creation is true if value in interface is 'OTC-BATCH' (username batch process SAP)
        if (sapOpp.AUTOMATIC_CREATION.toUpperCase() == 'OTC-BATCH'){
            opportunity.Automatic_Creation_Bool__c = true;
        }
                    
        opportunity.SAP_Created_Date__c = sapOpp.SAP_CREATED_DATE;
        opportunity.SAP_Last_Modified_Date__c = sapOpp.SAP_LASTMODIFIED_DATE;
        opportunity.CloseDate = sapOpp.CLOSEDATE;
        opportunity.sap_phase__c = sapOpp.SAP_PHASE;
        opportunity.sap_status__c = sapOpp.SAP_STATUS;
        opportunity.sap_reason__c = sapOpp.SAP_REASON;
        
        //CR-6844. Hardcoded translation for a specific case. 
        if(sapOpp.TYPE == 'Pump - From Comp.') opportunity.Type = 'Pump - From Competitor';
        else opportunity.Type = sapOpp.TYPE;
        
        opportunity.Origin_Text__c = sapOpp.ORIGIN;
        opportunity.Department_Text__c = sapOpp.DEPARTMENT;
        opportunity.CurrencyIsoCode = sapOpp.CURRENCYISOCODE;
        opportunity.Description = sapOpp.DESCRIPTION;
        opportunity.Training_Date__c = sapOpp.TRAINING_DATE;
        opportunity.Trial_Start_Date__c = sapOpp.TRIAL_START_DATE;
        opportunity.Trial_End_Date__c = sapOpp.TRIAL_END_DATE;
        opportunity.Requested_Delivery_Date__c = sapOpp.REQUESTED_DELIVERY_DATE;

        // Populate Accounts using the map
        if (mapOfSAPIdToAccnt.get(sapOpp.SAP_SHIP_TO_ID) != null){
            opportunity.SAP_Ship_To_ID__c = mapOfSAPIdToAccnt.get(sapOpp.SAP_SHIP_TO_ID).Id;
        }else{
            System.debug('SAP ship to account'+sapOpp.SAP_SHIP_TO_ID+' not found in map '+mapOfSAPIdToAccnt);
        }
        if (mapOfSAPIdToAccnt.get(sapOpp.SAP_SOLD_TO_ID) != null){
            opportunity.SAP_Sold_To_ID__c = mapOfSAPIdToAccnt.get(sapOpp.SAP_SOLD_TO_ID).Id;
        }else{
            System.debug('SAP sold to account'+sapOpp.SAP_SOLD_TO_ID+' not found '+mapOfSAPIdToAccnt);
        }
        if (mapOfSAPIdToAccnt.get(sapOpp.SAP_PHYSICIAN) != null){
            opportunity.Physician_Account_ID__c = mapOfSAPIdToAccnt.get(sapOpp.SAP_PHYSICIAN).Id;
        }else{
            System.debug('SAP physician'+sapOpp.SAP_PHYSICIAN+' not found '+mapOfSAPIdToAccnt);
        }
        if (mapOfSAPIdToAccnt.get(sapOpp.SAP_HEALTH_INSURANCE) != null){
            opportunity.Primary_Health_Insurer__c = mapOfSAPIdToAccnt.get(sapOpp.SAP_HEALTH_INSURANCE).Id;
        }else{
            System.debug('SAP health insurance'+sapOpp.SAP_HEALTH_INSURANCE+' not found '+mapOfSAPIdToAccnt);
        }

        // Populate the owner using the SAP ID of the Sales Rep
        List<User> listOfUser = [select Id from User where SAP_ID__c = :sapOpp.SAP_SALESREP];
        if (listOfUser.size() == 1){
            opportunity.OwnerId = listOfUser[0].Id;
        }
        
        // Map Sales Stage
        ws_SharedMethods.mapOpportunitySalesStageSAPToSFDC(opportunity);
        
        return opportunity;
        
    }
    
    public static List<OpportunityLineItem> mapSAPOpportunityItemsToOpportunityLineItems(Opportunity opp, List<SAPOpportunityItem> sapOpportunityItems){
        List<OpportunityLineItem> oppLineItems = new List<OpportunityLineItem>();
        
        // Map Of Sap Item Guid to Product Code
        Map<String, String> mapItemGuidToProductCode = new Map<String, String>();
        // Set of SAP Product Codes to lookup Pricebook Entries
        Set<String> setOfSAPProdCode = new Set<String>();
        // Set of SAP Item Guids that are being updated
        Set<String> setOfSAPGuidForUpdate = new Set<String>();
        // Map of Product Code to Sales Price for creating Pricebook Entries
        Map<String, Decimal> mapOfProductCodeToPrice = new Map<String, Decimal>();
        
        // Populate both sets and map
        for (SAPOpportunityItem sapOppItem : sapOpportunityItems){
            setOfSAPProdCode.add(sapOppItem.PRODUCTCODE);
            mapItemGuidToProductCode.put(sapOppItem.SAP_ITEM_GUID, sapOppItem.PRODUCTCODE);
            mapOfProductCodeToPrice.put(sapOppItem.PRODUCTCODE, Decimal.valueOf(sapOppItem.SALES_PRICE));
        }
        
        
        // First determine which line items already exist
        Map<Id, OpportunityLineItem> mapOfIdToItems = new Map<Id, OpportunityLineItem>([select Id, SAP_Item_GUID__c from OpportunityLineItem where OpportunityId = :opp.Id and SAP_Item_GUID__c in :mapItemGuidToProductCode.keySet()]);
        // Loop through result set and remove existing Product Codes, and populate set to indicate which line items are updates
        for (OpportunityLineItem thisItem : mapOfIdToItems.values()){
            setOfSAPProdCode.remove(thisItem.SAP_Item_GUID__c);
            setOfSAPGuidForUpdate.add(thisItem.SAP_Item_GUID__c);
        }
        if (!setOfSAPProdCode.isEmpty()){
            List<PricebookEntry> listOfPBEntry = [SELECT Id, ProductCode FROM PricebookEntry where Pricebook2Id = :priceBookId and ProductCode in :setOfSAPProdCode];
            Map<String, Id> mapOfProdCodeToPBEntry = new Map<String, Id>();
            for (PricebookEntry pbEntry : listOfPBEntry){
                mapOfProdCodeToPBEntry.put(pbEntry.ProductCode, pbEntry.Id);
            }
            // Build set of Product Codes for which we don't have a Pricebook Entry
            Set<String> setOfMissingPBEntry = new Set<String>();
            for (String prodCode : setOfSAPProdCode){
                if (mapOfProdCodeToPBEntry.get(prodCode) == null){
                    setOfMissingPBEntry.add(prodCode);
                }
            } 
            if (!setOfMissingPBEntry.isEmpty()){
                // Look up products to create price book entries
                List<Product2> listOfProd = [select Id, ProductCode from Product2 where ProductCode in :setOfMissingPBEntry];
                List<PricebookEntry> listOfNewPBEntryStd = new List<PricebookEntry>();
                List<PricebookEntry> listOfNewPBEntry = new List<PricebookEntry>();
                
                // For every product code create a pricebook entry, first in Standard Pricebook
                Pricebook2 pbStandard = [select id from Pricebook2 where isStandard=true];
                for (Product2 thisProd : listOfProd){
                    listOfNewPBEntryStd.add(new PricebookEntry(Pricebook2Id = pbStandard.Id,
                                                        CurrencyIsoCode = opp.CurrencyIsoCode,
                                                        Product2Id = thisProd.Id,
                                                        UnitPrice = mapOfProductCodeToPrice.get(thisProd.ProductCode),
                                                        isActive = true));              
                    listOfNewPBEntry.add(new PricebookEntry(Pricebook2Id = priceBookId, 
                                                            CurrencyIsoCode = opp.CurrencyIsoCode, 
                                                            Product2Id = thisProd.Id,
                                                            isActive = true,
                                                            UnitPrice = mapOfProductCodeToPrice.get(thisProd.ProductCode)));
                }
                
                // Insert missing Price Book Entries
                if (!listOfNewPBEntry.isEmpty()){
                    insert listOfNewPBEntryStd;
                    insert listOfNewPBEntry;
                    // Re-query for additional fields
                    listOfNewPBEntry = [select Id, ProductCode from PricebookEntry where Id in :listOfNewPBEntry];
                }
                // Add inserted to the map
                for (PricebookEntry pbEntry : listOfNewPBEntry){
                    mapOfProdCodeToPBEntry.put(pbEntry.ProductCode, pbEntry.Id);
                }               
            }
            // Now we can create the Opportunity Line Items
            for (SAPOpportunityItem sapOppItem : sapOpportunityItems){
                OpportunityLineItem oppLineItem = new OpportunityLineItem();
                oppLineItem.SAP_Item_ID_Text__c = sapOppItem.SAP_ITEM_ID;
                oppLineItem.SAP_Item_GUID__c = sapOppItem.SAP_ITEM_GUID;
                oppLineItem.SAP_Created_Date__c = sapOppItem.SAP_CREATED_DATE;
                oppLineItem.SAP_Last_Modified_Date__c = sapOppItem.SAP_LASTMODIFIED_DATE;
                oppLineItem.SAP_Item_SerialNo_Text__c = sapOppItem.SAP_ITEM_SERIALNO;
                oppLineItem.OpportunityId = opp.id;
                // Only set the PriceBookEntryId in case of an insert, otherwise exception will be generated
                if (!setOfSAPGuidForUpdate.contains(oppLineItem.SAP_Item_GUID__c)){         
                    oppLineItem.PricebookEntryId = mapOfProdCodeToPBEntry.get(sapOppItem.PRODUCTCODE);
                }
                oppLineItem.Quantity = Decimal.valueOf(sapOppItem.QUANTITY);
                oppLineItem.UnitPrice = Decimal.valueOf(sapOppItem.SALES_PRICE);
                oppLineItem.SAP_Item_Category_Text__c = sapOppItem.SAP_ITEM_CATEGORY;
                oppLineItem.SAP_Warranty_Start_Date__c = sapOppItem.SAP_WARRANTY_START_DATE;
                oppLineItem.Warranty_End_Date__c = sapOppItem.SAP_WARRANTY_END_DATE;
                oppLineItem.Status__c = sapOppItem.STATUS;
                oppLineItems.add(oppLineItem);
            }
        }
        System.debug('OppLineItems : '+oppLineItems);
        return oppLineItems;
    }
    
    /*
     * Description      : Structure for Opportunity
     */     
    global class SAPOpportunity{
        webservice String SFDC_ERR_STACKTRACE;
        webservice String SAP_ID;
        webservice String SAP_ORDER_GUID;
        webservice String NAME;
        webservice String NAME2;
        webservice String SAP_ACCOUNTID;
        webservice String AUTOMATIC_CREATION;
        webservice String OPPORTUNITYID;
        webservice String DISTRIBUTION_STATUS;
        webservice String DIST_ERROR_DETAILS;
        webservice boolean DISTRIBUTION_LOCK;
        webservice DateTime SAP_CREATED_DATE;
        webservice DateTime SAP_LASTMODIFIED_DATE;
        webservice DateTime CREATED_DATE;
        webservice DateTime LASTMODIFIED_DATE;
        webservice Date CLOSEDATE;
        webservice String SAP_PHASE;
        webservice String SAP_STATUS;
        webservice String SAP_REASON;
        webservice String TYPE;
        webservice String ORIGIN;
        webservice String COUNTRY;
        webservice String DEPARTMENT;
        webservice String CURRENCYISOCODE;
        webservice String DESCRIPTION;
        webservice Date TRAINING_DATE;
        webservice Date TRIAL_START_DATE;
        webservice Date TRIAL_END_DATE;
        webservice Date REQUESTED_DELIVERY_DATE;
        webservice String SAP_SHIP_TO_ID;
        webservice String SAP_SOLD_TO_ID;
        webservice String SAP_SALESREP;
        webservice String SAP_PHYSICIAN;
        webservice String SAP_HEALTH_INSURANCE;
        webservice List<SAPOpportunityItem> OPPORTUNITY_ITEMS;
    }
    
    /*
     * Description      : Structure for Opportunity Line Item
     */ 
    global class SAPOpportunityItem{
        webservice String SAP_ITEM_ID;
        webservice String SAP_ITEM_GUID;
        webservice String OPPORTUNITYPRODUCTID;
        webservice String PRODUCTCODE;
        webservice String QUANTITY;
        webservice String UNIT_OF_MEASURE;
        webservice String SAP_ITEM_CATEGORY;
        webservice String STATUS;
        webservice String SAP_ITEM_SERIALNO;
        webservice Date SAP_WARRANTY_START_DATE;
        webservice Date SAP_WARRANTY_END_DATE;
        webservice String SALES_PRICE;
        webservice DateTime SAP_CREATED_DATE;
        webservice DateTime SAP_LASTMODIFIED_DATE;
        webservice DateTime CREATEDDATE;
        webservice DateTime LASTMODIFIED_DATE;
    }
    
    /*
     * Description      : Structure for Opportunity Acknowledgement to update x-ref Ids
     */     
    global class SAPOpportunityAck{
        webservice String SFDC_ERR_STACKTRACE;
        webservice String SAP_ID;
        webservice String SAP_ORDER_GUID;
        webservice String OPPORTUNITYID;
        webservice String DISTRIBUTION_STATUS;
        webservice String DIST_ERROR_DETAILS;
        webservice List<SAPOpportunityItemAck> OPPORTUNITY_ITEMS;
    }
    
    /*
     * Description      : Structure for Opportunity Line Item Acknowledgement to update x-ref Ids
     */ 
    global class SAPOpportunityItemAck{
        webservice String SAP_ITEM_ID;
        webservice String SAP_ITEM_GUID;
        webservice String OPPORTUNITYPRODUCTID;
    }
    
}