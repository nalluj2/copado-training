/*
 *      Description : This class exposes methods to create a Task record
 *
 *      Author = Rudy De Coninck
 */
@RestResource(urlMapping='/TaskService/*')
global class ws_Task {


	@HttpPost
	global static String doPost() {
		
		RestRequest req = RestContext.request;
			
		System.debug('post called '+req);
		System.debug('post requestBody '+req.requestBody);
			
		String body = req.requestBody.toString();
		IFTaskRequest taskRequest = (IFTaskRequest)System.Json.deserialize(body, IFTaskRequest.class);
			
		System.debug('post requestBody '+taskRequest);
			
		Task task = taskRequest.task;
		List<Id> contactIds = taskRequest.contactIds;

		IFTaskResponse resp = new IFTaskResponse();
		
		Savepoint sp = Database.setSavePoint();
		
		//We catch all exception to assure that 
		try{
				
			resp.task = bl_Task.saveTask(task, contactIds);
			resp.id = taskRequest.id;
	
			resp.success = true;
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
			
			Database.rollback(sp);
		}
 
		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'Task' ,req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
  			
		return System.JSON.serialize(resp);	 			
	}

	@HttpDelete
	global static String doDelete(){
		RestRequest req = RestContext.request;
			
		System.debug('post called '+req);
		System.debug('post requestBody '+req.requestBody);
			
		String body = req.requestBody.toString();
		IFTaskDeleteRequest taskDeleteRequest = (IFTaskDeleteRequest)System.Json.deserialize(body, IFTaskDeleteRequest.class);
			
		System.debug('post requestBody '+taskDeleteRequest);
			
		Task task = taskDeleteRequest.task;
		IFTaskDeleteResponse resp = new IFTaskDeleteResponse();
		
		List<Task>tasksToDelete = [select id, mobile_id__c from Task where Mobile_ID__c = :task.Mobile_ID__c];
		
		try{
			
			if (tasksToDelete !=null && tasksToDelete.size()>0 ){	
				
				Task taskToDelete = tasksToDelete.get(0);
				delete taskToDelete;
				resp.task = taskToDelete;
				resp.id = taskDeleteRequest.id;
				resp.success = true;
			}else{
				resp.task = task;
				resp.id = taskDeleteRequest.id;
				resp.message='Task not found';
				resp.success = true;
			}
			
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
		}

		bl_Mobilesync.storeRequest(UserInfo.getUserId(),'TaskDelete',req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
			
		return System.JSON.serialize(resp);
	}
	
	global class IFTaskRequest{
		public Task task {get;set;}		
		public List<Id> contactIds {get;set;}
		public String id {get;set;}
	}
		
	global class IFTaskResponse{
		public Task task {get;set;}
		public String id {get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}
	
	global class IFTaskDeleteRequest{
		public Task task {get;set;}
		public String id {get;set;}
	}
		
	global class IFTaskDeleteResponse{
		public Task task {get;set;}
		public String id {get;set;}
		public boolean success {get;set;}
		public String message {get;set;} 
	}
}