@isTest
private class TestBatchClassesUpdateFlags {

    static testMethod void testBatchClassClearAccountSBUFlagsATM() {
        Account acc3 = new Account();
        acc3.Name = 'Account Profiling Test Coverage Account 3 ' ; 
        acc3.Aortic__c=true;
        acc3.Diabetes__c=true;
        acc3.HST__c=true;
        acc3.Structural_Heart__c=true;
        acc3.ECT__c=true;
        acc3.Core_Neuro__c=true;
        acc3.Pelvic_Health__c=true;
        acc3.Coro_PV__c=true;
        acc3.Neuromodulation__c=true;
        acc3.AF_Solutions__c=true; 
        acc3.Implantables_Diagnostic__c =true;
        acc3.Interventional__c =true;
        acc3.Navigation__c =true;
        acc3.ENT_NT__c =true;
        acc3.Vascular__c = true; 
        insert acc3;  

     Test.startTest();                    
        Account acc2 = new Account();
        acc2.Name = 'Account Profiling Test Coverage Account 2 ' ; 
        acc2.Aortic__c =true;
        acc2.Diabetes__c=true;
        acc2.HST__c=true;
        acc2.Structural_Heart__c=true;
        acc2.ECT__c=true;
        acc2.Core_Neuro__c=true;
        acc2.Pelvic_Health__c=true;
        acc2.Coro_PV__c=true;
        acc2.Neuromodulation__c=true;
        acc2.AF_Solutions__c=true; 
        acc2.Implantables_Diagnostic__c =true;
        acc2.Interventional__c =true;
        acc2.Navigation__c =true;
        acc2.ENT_NT__c =true;
        acc2.Vascular__c = true; 
        insert acc2;  

        Account acc4 = new Account();
        acc4.Name = 'Account Profiling Test Coverage Account 4 ' ; 
        acc4.Aortic__c=false;
        acc4.Diabetes__c=false;
        acc4.HST__c=false;
        acc4.Structural_Heart__c=false;
        acc4.ECT__c=false;
        acc4.Core_Neuro__c=false;
        acc4.Pelvic_Health__c=false;
        acc4.Coro_PV__c=false;
        acc4.Neuromodulation__c=true;
        acc4.AF_Solutions__c=false; 
        acc4.Implantables_Diagnostic__c =false;
        acc4.Interventional__c =false;
        acc4.Navigation__c =false;
        acc4.ENT_NT__c =false;
        acc4.Vascular__c = false; 
        insert acc4; 
        Company__c c = new Company__c(); 
            C.Name='Test Company name';
            C.Company_Code_Text__c = 'T46';
            Insert C;
            
            
            Business_Unit__c BU = new Business_Unit__c();
            BU.Name='Test Bu Name';
            BU.Company__c = C.Id;
            Insert BU;  
            Sub_Business_Units__c SBU = new Sub_Business_Units__c();
            SBU.Name='Test Bu Name';
            SBU.Business_Unit__c = BU.Id;
            Insert SBU;  
         
        Test.stopTest();              
    } 
    @isTest(SeeAllData=true)
    static  void testBatchClassUpdateAccountFlags() { 

        // Get BU records        
        List<ID> BUList=new List<ID>();        
        ID BU;        
        string BUNAme;        
        AggregateResult[]  BUs = [Select ID,Name from Business_Unit__c group by ID,Name having Name<>null];                        
        for (AggregateResult ac : BUs){             
            BU=(ID)ac.get('ID');            
            BUNAme=string.valueOf(ac.get('Name'));                               
            BUList.add(BU);        
       }

        // Get SBU records        
        List<ID> SBUList=new List<ID>();                
        ID SBU;        
        string SBUNAme;        
        AggregateResult[]  SBUs = [Select ID,Name from Sub_Business_Units__c group by ID,Name having Name<>null];                        
        for (AggregateResult ac : SBUs){             
            SBU=(ID)ac.get('ID');            
            SBUNAme=string.valueOf(ac.get('Name'));                               
            SBUList.add(SBU);                        
 
        }         

          //Insert Company
         Company__c cmpny = new Company__c();
         cmpny.name='TestMedtronic';
         cmpny.CurrencyIsoCode = 'EUR';
         cmpny.Current_day_in_Q1__c=56;
         cmpny.Current_day_in_Q2__c=34; 
         cmpny.Current_day_in_Q3__c=5; 
         cmpny.Current_day_in_Q4__c= 0;   
         cmpny.Current_day_in_year__c =200;
         cmpny.Days_in_Q1__c=56;  
         cmpny.Days_in_Q2__c=34;
         cmpny.Days_in_Q3__c=13;
         cmpny.Days_in_Q4__c=22;
         cmpny.Days_in_year__c =250;
         cmpny.Company_Code_Text__c = 'T24';
         insert cmpny;
 
        Therapy_Group__c TG=new Therapy_Group__c();
            TG.Name='Test Therapy Group';
            TG.Sub_Business_Unit__c =SBUList.get(0);
            TG.Company__c = cmpny.Id;
        insert TG;    
        
        Therapy__c th = new Therapy__c();
            th.Name = 'Test Coverage th';
            th.Business_unit__c=BUList.get(0);
            th.Sub_Business_Unit__c=SBUList.get(0);
            th.Therapy_Group__c=TG.ID;
            th.Therapy_Name_Hidden__c = 'Test Coverage th';
        insert th;
 
     
        Territory2 Terr1=[select id,name,Business_Unit__c,Territory2Type.DeveloperName,Therapy_Groups_Text__c from Territory2 where Territory2Type.DeveloperName = 'Territory' limit 1];

        Account acc1 = new Account();
        acc1.Name = 'Account Profiling Test Coverage Account 1 ' ; 
        insert acc1 ;               
        
        Account acc3 = new Account();
        acc3.Name = 'Account Profiling Test Coverage Account 3 ' ; 
        acc3.Aortic__c=true;
        acc3.Diabetes__c=true;
        acc3.HST__c=true;
        acc3.Structural_Heart__c=true;
        acc3.ECT__c=true;
        acc3.Core_Neuro__c=true;
        acc3.Pelvic_Health__c=true;
        acc3.Coro_PV__c=true;
        acc3.Neuromodulation__c=true;
        acc3.AF_Solutions__c=true; 
        acc3.Implantables_Diagnostic__c =true;
        acc3.Interventional__c =true;
        acc3.Navigation__c =true;
        acc3.ENT_NT__c =true;
        acc3.Vascular__c = true; 
        insert acc3;  
                    
        Test.startTest();     
          
        Account acc2 = new Account();
        acc2.Name = 'Account Profiling Test Coverage Account 2 ' ; 
        acc2.Aortic__c=true;
        acc2.Diabetes__c=true;
        acc2.HST__c=true;
        acc2.Structural_Heart__c=true;
        acc2.ECT__c=true;
        acc2.Core_Neuro__c=true;
        acc2.Pelvic_Health__c=true;
        acc2.Coro_PV__c=true;
        acc2.Neuromodulation__c=true;
        acc2.AF_Solutions__c=true; 
        acc2.Implantables_Diagnostic__c =true;
        acc2.Interventional__c =true;
        acc2.Navigation__c =true;
        acc2.ENT_NT__c =true;
        acc2.Vascular__c = true; 
        insert acc2;  

        Account acc4 = new Account();
        acc4.Name = 'Account Profiling Test Coverage Account 4 ' ; 
        acc4.Aortic__c=false;
        acc4.Diabetes__c=false;
        acc4.HST__c=false;
        acc4.Structural_Heart__c=false;
        acc4.ECT__c=false;
        acc4.Core_Neuro__c=false;
        acc4.Pelvic_Health__c=false;
        acc4.Coro_PV__c=false;
        acc4.Neuromodulation__c=false;
        acc4.AF_Solutions__c=false; 
        acc4.Implantables_Diagnostic__c =false;
        acc4.Interventional__c =false;
        acc4.Navigation__c =false;
        acc4.ENT_NT__c =false;
        acc4.Vascular__c = false; 
        insert acc4;  
                
        ObjectTerritory2Association AccSh2=new ObjectTerritory2Association();
        AccSh2.ObjectId = acc4.ID;
        AccSh2.Territory2Id = terr1.Id;
        AccSh2.AssociationCause ='Territory2Manual';
        insert AccSh2;
        
        Test.stopTest();              
    }   
}