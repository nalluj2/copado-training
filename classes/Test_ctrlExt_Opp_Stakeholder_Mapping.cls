@isTest 
private class Test_ctrlExt_Opp_Stakeholder_Mapping {
	
	private static id oppRTG_BUG_Id;
	private static id shGeneric_Id;
	
	private static testMethod void redirectGSM(){
		
		setData();
		
		system.assertnotequals(Test_ctrlExt_Opp_Stakeholder_Mapping.oppRTG_BUG_Id, null);
		system.assertnotequals(Test_ctrlExt_Opp_Stakeholder_Mapping.shGeneric_Id, null);
		
		clsTestData.createOpportunityData(true);
		
		system.debug('### clsTestData.oMain_Opportunity.Id = ' + clsTestData.oMain_Opportunity.Id);
		system.debug('### clsTestData.oMain_Opportunity.RecordTypeId = ' + clsTestData.oMain_Opportunity.RecordTypeId);
			
		Test.startTest();
		
		Stakeholder_Mapping__c stakeholder = new Stakeholder_Mapping__c();
		stakeholder.Opportunity__c = clsTestData.oMain_Opportunity.Id;

		system.debug('### stakeholder.Opportunity__c = ' + stakeholder.Opportunity__c);
		
		ApexPages.StandardController sc = new ApexPages.StandardController(stakeholder);
		ctrlExt_Opportunity_Stakeholder_Mapping controller = new ctrlExt_Opportunity_Stakeholder_Mapping(sc);
		
		PageReference pr = controller.redirectWithRecordType();
		
		System.assert(pr.getParameters().get('RecordType') == String.valueOf(Test_ctrlExt_Opp_Stakeholder_Mapping.shGeneric_Id));
	}
	
	private static void setData() {
	
		Company__c cmpny = new Company__c();		
        cmpny.name='Europe';
        cmpny.CurrencyIsoCode = 'EUR';
        cmpny.Current_day_in_Q1__c=56;
        cmpny.Current_day_in_Q2__c=34; 
        cmpny.Current_day_in_Q3__c=5; 
        cmpny.Current_day_in_Q4__c= 0;   
        cmpny.Current_day_in_year__c =200;
        cmpny.Days_in_Q1__c=56;  
        cmpny.Days_in_Q2__c=34;
        cmpny.Days_in_Q3__c=13;
        cmpny.Days_in_Q4__c=22;
        cmpny.Days_in_year__c =250;
        cmpny.Company_Code_Text__c = 'T35';
		insert cmpny;
		
		Business_Unit_Group__c bug =  new Business_Unit_Group__c();
        bug.Master_Data__c =cmpny.id;
        bug.name='RTG';      
        insert bug;
		
		Business_Unit__c bu =  new Business_Unit__c();
        bu.Company__c =cmpny.id;
        bu.name='RTG BU';
        bu.Business_Unit_Group__c = bug.Id;
        bu.Account_Plan_Activities__c=true;        
        insert bu;
        
        Sub_Business_Units__c sbu = new Sub_Business_Units__c();
        sbu.name='RTG sBU';
        sbu.Business_Unit__c=bu.id;
		insert sbu;		
						
		Account testAccount = new Account();
		testAccount.Name = 'RTG Test Account';
		insert testAccount;

		Test_ctrlExt_Opp_Stakeholder_Mapping.oppRTG_BUG_Id = clsTestData.idRecordType_Opportunity;
		Test_ctrlExt_Opp_Stakeholder_Mapping.shGeneric_Id = RecordTypeMedtronic.getRecordTypeByDevName('Stakeholder_Mapping__c', 'General_Stakeholder_Mapping').Id;
		
		Pricebook2 customPB = new Pricebook2(Name='Standard Price Book', isActive=true);
		insert customPB;		

		system.debug('### clsTestData.idRecordType_Opportunity = ' + clsTestData.idRecordType_Opportunity);
		system.debug('### customPB.Id = ' + customPB.Id);
		system.debug('### Test_ctrlExt_Opp_Stakeholder_Mapping.shGeneric_Id = ' + Test_ctrlExt_Opp_Stakeholder_Mapping.shGeneric_Id);
		
		Price_Book_Management__c pbm = new Price_Book_Management__c();
		pbm.Region__c = cmpny.Id;
		pbm.Opportunity_Record_Type_ID__c = clsTestData.idRecordType_Opportunity;
		pbm.Price_Book_ID__c = customPB.Id;
		pbm.Stakeholder_Mapping_Id__c = Test_ctrlExt_Opp_Stakeholder_Mapping.shGeneric_Id;
		insert pbm;
		
		system.debug('### pbm.Id = ' + pbm.Id);		
				
	}
}