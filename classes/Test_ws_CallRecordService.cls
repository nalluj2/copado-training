@IsTest(seeAllData=true)
public with sharing class Test_ws_CallRecordService { 

	public static testmethod void testCallRecordBU(){
		Call_Records__c cr = ws_callRecordServiceRequestFactory.createBUCallRecord();
		ws_callRecordServiceRequestFactory.deleteRecord(cr.mobile_id__c); 
	}
	
	
	/*
	public static testmethod void testDoGet(){
		
		RestRequest req = new RestRequest(); 
    	RestResponse res = new RestResponse();
    	
    	List<Contact_Visit_Report__c> attendedContacts = new List<Contact_Visit_Report__c>();
		String id=GuidUtil.NewGuid();
		List<Call_Topics__c> callTopics = new List<Call_Topics__c>();
		List<Call_Topic_Subject__c> callTopicSubjects  = new List<Call_Topic_Subject__c>();
		List<Call_Topic_Products__c> callTopicProducts  = new List<Call_Topic_Products__c>();
		List<Call_Topic_Business_Unit__c> callTopicBusinessUnit  = new List<Call_Topic_Business_Unit__c>();

		Company__c company = new Company__c(name='EUR');
		company.Company_Code_Text__c ='TST';
		insert company;
		
		Business_Unit__c bu = new Business_Unit__c(name='test bu');
		bu.Company__c = company.id;
		insert bu;
	
		Call_Records__c callRecordMobileId = new Call_Records__c();
	
		Call_Records__c callRecord = new Call_Records__c();
		callRecord.Business_Unit__c = bu.id;
		callRecord.Mobile_ID__c = GuidUtil.NewGuid();
		
		callRecordMobileId.Mobile_ID__c = callRecord.Mobile_ID__c;
		
		Call_Activity_Type__c callActType = new Call_Activity_Type__c();
		callActType.Company_ID__c = company.id;
		insert callActType;

        Call_Topics__c ct1 = new Call_topics__c();
        ct1.Call_Records__r = callRecordMobileId;
        ct1.Call_Activity_Type__c = callActType.id;
        ct1.Mobile_ID__c = GuidUtil.NewGuid();
        
        Call_Topics__c ctMobileID = new Call_topics__c();
        ctMobileID.Mobile_ID__c = ct1.Mobile_ID__c;

        Call_Topic_Subject__c subject1 = new Call_Topic_Subject__c();
        subject1.Call_Topic__r = ctMobileID;
        subject1.Mobile_ID__c = GuidUtil.NewGuid();
		callTopicSubjects.add(subject1);

		Product2 pr = new Product2(name='test pr1');
		insert pr;
		
		
		Call_Topic_Products__c product1 = new Call_Topic_Products__c();
		product1.Mobile_ID__c = GUIDUtil.NewGuid();
		product1.Product__c = pr.id;

		callTopicProducts.add(product1);
    	
    	callTopics.add(ct1);
    	
    	Account a = new Account(name='test account');
    	insert a;
    	
    	Contact c = new Contact(firstname='test', lastname='tester');
    	c.accountId = a.id;
    	c.Contact_Department__c='test';
    	c.Contact_Primary_Specialty__c='test';
    	c.Affiliation_To_Account__c = a.id;
    	c.Primary_Job_Title_vs__c ='test';
    	c.Contact_Gender__c ='Male';
    	insert c;
    	
    	
    	Contact_Visit_Report__c cvr1 = new Contact_Visit_Report__c();
    	cvr1.Attending_Affiliated_Account__c = a.id;
    	cvr1.Attending_Contact__c = c.id;
    	cvr1.Mobile_ID__c = GuidUtil.NewGuid();
        cvr1.Call_Records__r = callRecordMobileId;
		attendedContacts.add(cvr1);    	
    	
    	
    	JSONGenerator gen = JSON.createGenerator(true);
    	
		gen.writeStartObject();
        gen.writeStringField('id', '12346');
	    gen.writeObjectField('callTopicSubjects',callTopicSubjects);
	    gen.writeObjectField('callTopics',callTopics);
	    //gen.writeObjectField('callTopicProducts',callTopicProducts);
	    gen.writeObjectField('callTopicBusinessUnit',callTopicBusinessUnit);
	    gen.writeObjectField('attendedContacts',attendedContacts);
	    gen.writeObjectField('callRecord', callRecord);
	   
        
        // Get the JSON string.
        String pretty = gen.getAsString();	    	
		System.debug('test '+pretty);

        req.requestbody = Blob.valueOf(pretty);
    	
     	req.requestURI = '/services/apexrest/CallRecordService';  
	    req.httpMethod = 'POST';
    	RestContext.request = req;
    	RestContext.response = res;
		
		//Update of created record
		String test = ws_CallRecordService.doPost();
		System.debug('test '+test);
		
	}*/


}