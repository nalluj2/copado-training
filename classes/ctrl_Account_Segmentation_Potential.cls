public with sharing class ctrl_Account_Segmentation_Potential {
    
    @AuraEnabled
    public static List<PotentialProcedureInput> getUserPotentialInputs(String role) {
    	
    	try{
    	
	    	List<PotentialProcedureInput> result = new List<PotentialProcedureInput>();
	    	
	    	List<UMD_Purpose__c> potentialPurpose = [Select Id, Fiscal_Year_vs__c from UMD_Purpose__c where Unique_Qualification_Key__c = 'Potential'];
			
			if(potentialPurpose.size() == 1){
	    		    		    		    		
	    		Map<Id, Segmentation_Potential_Procedure__c> procedureMap = new Map<Id, Segmentation_Potential_Procedure__c>(
	    		[Select Id, (Select Id, Competitor_picklist__c, Competitor_Product_picklist__c from Segmentation_Potential_Proc_Competitors__r where Active__c = true ORDER BY Competitor_picklist__c) from Segmentation_Potential_Procedure__c]
	    		);
	    		
		    	Set<String> prevKeys = new Set<String>();	    	
		    	
		    	String query = 'Select Id, Account__c, Account__r.Name, Account__r.BillingCity , Account__r.SAP_Id__c, Last_12_mth_Sales__c, Complete__c, Approved__c, Approved_2__c, Assigned_To__c, Assigned_To__r.Name, Approver_1__c, Approver_1__r.Name, Approver_2__c, Approver_2__r.Name';
		    	query += ', CurrencyIsoCode, Potential__c, Potential_Medtronic__c, Current_Segmentation__r.Size_Segment__c, CreatedBy.Name, CreatedDate';	    	
		    	query += ', Segmentation_Potential_Procedure__c, Segmentation_Potential_Procedure__r.Name, Segmentation_Potential_Procedure__r.Registration_Unit__c, Segmentation_Potential_Procedure__r.Sub_Business_Unit__c';    	
		    	query += ', (SELECT Field, CreatedBy.Name, CreatedDate FROM Histories ORDER BY CreatedDate DESC)';
				query += ' from Segmentation_Potential_Input__c where UMD_Purpose__c = \'' + potentialPurpose[0].Id + '\' ';
				
				String compQuery = 'Select Id, Potential__c, CurrencyIsoCode, Segmentation_Potential_Input__c, Segmentation_Potential_Proc_Competitor__c,  CreatedBy.Name, CreatedDate, (SELECT Field, CreatedBy.Name, CreatedDate FROM Histories ORDER BY CreatedDate DESC) from Segmentation_Potential_Input_Competitor__c where Segmentation_Potential_Input__r.UMD_Purpose__c = \'' + potentialPurpose[0].Id + '\' ';
				
				if(role == 'Input'){
					
					query +=  'AND Assigned_To__c = \'' + UserInfo.getUserId() + '\'';
					compQuery +=  'AND Segmentation_Potential_Input__r.Assigned_To__c = \'' + UserInfo.getUserId() + '\'';
					
				}else{
					
					query +=  'AND (Approver_1__c = \'' + UserInfo.getUserId() + '\' OR Approver_2__c = \'' + UserInfo.getUserId() + '\')';
					compQuery +=  'AND (Segmentation_Potential_Input__r.Approver_1__c = \'' + UserInfo.getUserId() + '\' OR Segmentation_Potential_Input__r.Approver_2__c = \'' + UserInfo.getUserId() + '\')';
				}
							
				compQuery += ' ORDER BY Segmentation_Potential_Proc_Competitor__r.Competitor_picklist__c';
				
				Map<Id, List<Segmentation_Potential_Input_Competitor__c>> inputCompetitorMap = new Map<Id, List<Segmentation_Potential_Input_Competitor__c>>();
				
				for(Segmentation_Potential_Input_Competitor__c inputCompetitor : Database.query(compQuery)){
					
					List<Segmentation_Potential_Input_Competitor__c> inputCompetitors = inputCompetitorMap.get(inputCompetitor.Segmentation_Potential_Input__c);
					
					if(inputCompetitors == null){
						
						inputCompetitors = new List<Segmentation_Potential_Input_Competitor__c>();
						inputCompetitorMap.put(inputCompetitor.Segmentation_Potential_Input__c, inputCompetitors);
					}
					
					inputCompetitors.add(inputCompetitor);
				}
		    	
		    	String prevFiscalYear = 'FY' + (Integer.valueOf(potentialPurpose[0].Fiscal_Year_VS__c.split('FY')[1]) - 1);
		    	
		    	for(Segmentation_Potential_Input__c potentialInput : Database.query(query)){
		    		   			
	    			prevKeys.add(potentialInput.Account__c + ':' + potentialInput.Segmentation_Potential_Procedure__c + ':' + prevFiscalYear);
		    		
		    		PotentialProcedureInput procedureInput = new PotentialProcedureInput();
		    		procedureInput.record = potentialInput;	    		
		    		procedureInput.account = new PotentialAccount(potentialInput.Account__r);
		    		if(potentialInput.Current_Segmentation__r != null) procedureInput.account.accountSegment = potentialInput.Current_Segmentation__r.Size_Segment__c;
	    			if(procedureInput.account.accountSegment == null || procedureInput.account.accountSegment == 'Not Segmented') procedureInput.account.accountSegment = '-';
		    		
		    		if(potentialInput.Potential__c != null) procedureInput.potentialTS = getTimeStamp(potentialInput, 'Potential__c');
		    		if(potentialInput.Potential_Medtronic__c != null) procedureInput.potentialMDTTS = getTimeStamp(potentialInput, 'Potential_Medtronic__c');
		    		
		    		if(potentialInput.Complete__c == true) procedureInput.completedTS = getTimeStamp(potentialInput, 'Complete__c');
		    		else procedureInput.completedTS = potentialInput.Assigned_To__r.Name;
		    		if(potentialInput.Approved__c == true) procedureInput.approved1TS = getTimeStamp(potentialInput, 'Approved__c');
		    		else if(potentialInput.Approver_1__c != null) procedureInput.approved1TS = potentialInput.Approver_1__r.Name;
		    		if(potentialInput.Approved_2__c == true) procedureInput.approved2TS = getTimeStamp(potentialInput, 'Approved_2__c');
		    		else if(potentialInput.Approver_2__c != null)procedureInput.approved2TS = potentialInput.Approver_2__r.Name;
		    			    		
		    		Map<Id, Segmentation_Potential_Input_Competitor__c> existingCompetitors = new Map<Id, Segmentation_Potential_Input_Competitor__c>();
		    		
		    		List<Segmentation_Potential_Input_Competitor__c> inputCompetitors = inputCompetitorMap.get(potentialInput.Id);
		    		if(inputCompetitors != null){
			    		
			    		for(Segmentation_Potential_Input_Competitor__c	inputCompetitor : inputCompetitors){
			    			
			    			existingCompetitors.put(inputCompetitor.Segmentation_Potential_Proc_Competitor__c, inputCompetitor);
			    		}
		    		}
		    		
		    		procedureInput.competitors = new List<PotentialProcedureCompetitorInput>();
		    		
		    		for(Segmentation_Potential_Proc_Competitor__c procedureCompetitor : procedureMap.get(potentialInput.Segmentation_Potential_Procedure__c).Segmentation_Potential_Proc_Competitors__r){
		    			
		    			PotentialProcedureCompetitorInput competitorInput = new PotentialProcedureCompetitorInput();
		    			
		    			Segmentation_Potential_Input_Competitor__c existingInput = existingCompetitors.get(procedureCompetitor.Id);
		    			
		    			if(existingInput == null){
		    				
		    				existingInput = new Segmentation_Potential_Input_Competitor__c();
		    				existingInput.Segmentation_Potential_Proc_Competitor__c = procedureCompetitor.Id;
		    				existingInput.Segmentation_Potential_Input__c = potentialInput.Id;
		    				existingInput.CurrencyIsoCode = potentialInput.CurrencyIsoCode;
		    			
		    			}else{
		    				
		    				competitorInput.potentialTS = getTimeStamp(existingInput, 'Potential__c');
		    			}
		    			
		    			existingInput.Segmentation_Potential_Proc_Competitor__r = procedureCompetitor;
		    			
		    			competitorInput.record = existingInput;	    			
		    			procedureInput.competitors.add(competitorInput);
		    		}	    		
		    		
		    		result.add(procedureInput);	    		
		    	}
		    		    		    		    	
		    	//Previous FY Data    	
		    	Map<String, Segmentation_Potential_Input__c> prevProcedureInputs = new Map<String, Segmentation_Potential_Input__c>();
			   	Map<String, Segmentation_Potential_Input_Competitor__c> prevProcedureCompInputs = new Map<String, Segmentation_Potential_Input_Competitor__c>();
		    		
		    	String prevQuery = 'Select Id, Account__c, Segmentation_Potential_Procedure__c, CurrencyIsoCode, Potential__c, Potential_Medtronic__c'; 
				prevQuery += ', (Select Id, Potential__c, CurrencyIsoCode, Segmentation_Potential_Proc_Competitor__c from Segmentation_Potential_Input_Competitors__r)';
				prevQuery += ' from Segmentation_Potential_Input__c where Unique_Key__c IN :keys';
		    		    	
		    	for(SObject record : bl_Without_Sharing_Service.queryByKeys(prevQuery, prevKeys)){
		    		
		    		Segmentation_Potential_Input__c potentialInput = (Segmentation_Potential_Input__c) record;
		    		
		    		prevProcedureInputs.put(potentialInput.Account__c + ':' + potentialInput.Segmentation_Potential_Procedure__c, potentialInput);
		    		
		    		for(Segmentation_Potential_Input_Competitor__c potentialCompInput : potentialInput.Segmentation_Potential_Input_Competitors__r){
		    			
		    			prevProcedureCompInputs.put(potentialInput.Account__c + ':' + potentialInput.Segmentation_Potential_Procedure__c + ':' + potentialCompInput.Segmentation_Potential_Proc_Competitor__c, potentialCompInput);	
		    		}
				}
		    	
		    	for(PotentialProcedureInput procedurePotential : result){
	    			
	    			//Prev values
	    			String key = procedurePotential.account.accountId + ':' + procedurePotential.record.Segmentation_Potential_Procedure__c;		    			
	    			procedurePotential.prevRecord = prevProcedureInputs.get(key);
	    			
	    			for(PotentialProcedureCompetitorInput compInput : procedurePotential.competitors){
	    				
	    				String compKey = procedurePotential.account.accountId + ':' + procedurePotential.record.Segmentation_Potential_Procedure__c + ':' + compInput.record.Segmentation_Potential_Proc_Competitor__c;		    			
	    				compInput.prevRecord = prevProcedureCompInputs.get(compKey);
	    			}
				}
			}
			
			return result;
		
		}catch(Exception e){
    		
    		System.debug(e.getMessage() + ' | ' + e.getStackTraceString());
    		AuraHandledException auraEx = new AuraHandledException(e.getMessage()); 
    		if(test.isRunningTest()) auraEx.setMessage(e.getMessage());
    		throw auraEx;
    	}
    }
    
    public static String getTimeStamp(Segmentation_Potential_Input__c potentialInput, String fieldName){
    	
    	String timeStamp;
    	
    	for(Segmentation_Potential_Input__History fieldHistory : potentialInput.Histories){
    		
    		if(fieldHistory.Field == fieldName){
    			
    			timeStamp = fieldHistory.CreatedBy.Name + ', ' + fieldHistory.CreatedDate.format('d MMM yyyy HH:mm');
    			break;
    		}
    	}
    	
    	if(timeStamp == null) timeStamp = potentialInput.CreatedBy.Name + ', ' + potentialInput.CreatedDate.format('d MMM yyyy HH:mm');
    	
    	return timeStamp;
    }
    
    public static String getTimeStamp(Segmentation_Potential_Input_Competitor__c inputCompetitor, String fieldName){
    	
    	String timeStamp;
    	
    	for(Segmentation_Potential_Input_Competitor__History fieldHistory : inputCompetitor.Histories){
    		
    		if(fieldHistory.Field == fieldName){
    			
    			timeStamp = fieldHistory.CreatedBy.Name + ', ' + fieldHistory.CreatedDate.format('d MMM yyyy HH:mm');
    			break;
    		}
    	}
    	
    	if(timeStamp == null) timeStamp = inputCompetitor.CreatedBy.Name + ', ' + inputCompetitor.CreatedDate.format('d MMM yyyy HH:mm');
    	
    	return timeStamp;
    }
    
    @AuraEnabled
    public static List<PotentialProcedureInput> savePotentials(List<PotentialProcedureInput> potentials) {
    	
    	bl_Customer_Segmentation.fromApp = true;
    	
    	System.SavePoint sp = Database.setSavepoint();
    	
    	try{
    		
    		List<Segmentation_Potential_Input__c> accountPotentialInputs = new List<Segmentation_Potential_Input__c>();
    		List<Segmentation_Potential_Input_Competitor__c> compInputs_Upsert = new List<Segmentation_Potential_Input_Competitor__c>();
    		List<Segmentation_Potential_Input_Competitor__c> compInputs_Delete = new List<Segmentation_Potential_Input_Competitor__c>();
    		
			for(PotentialProcedureInput procedurePotential : potentials){
			
    			if(procedurePotential.changed == true){
    				
    				accountPotentialInputs.add(procedurePotential.record);    				
    			}
    			
    			for(PotentialProcedureCompetitorInput compInput : procedurePotential.competitors){
    				
    				if(compInput.changed == true){
    					
    					if(compInput.record.Potential__c != null) compInputs_Upsert.add(compInput.record);
    					else if(compInput.record.Id != null){
    						
    						Segmentation_Potential_Input_Competitor__c toDelete = compInput.record;
    						compInputs_Delete.add(toDelete);
    						
    						compInput.record = new Segmentation_Potential_Input_Competitor__c();
							compInput.record.Segmentation_Potential_Proc_Competitor__c = toDelete.Segmentation_Potential_Proc_Competitor__c;
							compInput.record.Segmentation_Potential_Input__c = toDelete.Segmentation_Potential_Input__c;
							compInput.potentialTS = null; 
    					}
    				}
    			}
			}
			    		
    		if(accountPotentialInputs.size() > 0){
    			
    			Map<Id, Segmentation_Potential_Input__c> updatedMap = new Map<Id, Segmentation_Potential_Input__c>(
    				[Select Id, Complete__c, Approved__c, Approved_2__c, Assigned_To__r.Name, Approver_1__r.Name, Approver_2__r.Name, Potential__c, Potential_Medtronic__c				
					from Segmentation_Potential_Input__c where Id = :accountPotentialInputs]
    				);
    			
    			update accountPotentialInputs;
    			
    			String ts = UserInfo.getFirstName() + ' ' + UserInfo.getLastName() + ', ' + DateTime.now().format('d MMM yyyy HH:mm');
    			
    			for(PotentialProcedureInput procedurePotential : potentials){
			
	    			if(procedurePotential.changed == true){
	    					    					    				
	    				Segmentation_Potential_Input__c potentialInput = updatedMap.get(procedurePotential.record.Id);
	    				
						if(procedurePotential.record.Potential__c != null && procedurePotential.record.Potential__c != potentialInput.Potential__c) procedurePotential.potentialTS = ts;
						else if(procedurePotential.record.Potential__c == null) procedurePotential.potentialTS = null;
						
			    		if(procedurePotential.record.Potential_Medtronic__c != null && procedurePotential.record.Potential_Medtronic__c != potentialInput.Potential_Medtronic__c) procedurePotential.potentialMDTTS = ts;
			    		else if(procedurePotential.record.Potential_Medtronic__c == null) procedurePotential.potentialMDTTS = null;
			    		
			    		if(procedurePotential.record.Complete__c == true && procedurePotential.record.Complete__c != potentialInput.Complete__c) procedurePotential.completedTS = ts;
			    		else if(procedurePotential.record.Complete__c == false) procedurePotential.completedTS = potentialInput.Assigned_To__r.Name;
			    		
			    		if(potentialInput.Approved__c == true && procedurePotential.record.Approved__c != potentialInput.Approved__c) procedurePotential.approved1TS = ts;
			    		else if(potentialInput.Approved__c == false && potentialInput.Approver_1__c != null) procedurePotential.approved1TS = potentialInput.Approver_1__r.Name;
			    		
			    		if(potentialInput.Approved_2__c == true && procedurePotential.record.Approved_2__c != potentialInput.Approved_2__c) procedurePotential.approved2TS = ts;
			    		else if(potentialInput.Approved_2__c == false && potentialInput.Approver_2__c != null)procedurePotential.approved2TS = potentialInput.Approver_2__r.Name;			    		
	    			}
    			}
    		}
    		
    		if(compInputs_Upsert.size() > 0){
    			
    			Map<Id, Segmentation_Potential_Input_Competitor__c> updatedMap = new Map<Id, Segmentation_Potential_Input_Competitor__c>(
    				[Select Id, Potential__c from Segmentation_Potential_Input_Competitor__c where Id = :compInputs_Upsert]
    				);
    				
    			upsert compInputs_Upsert;
    			
    			String ts = UserInfo.getFirstName() + ' ' + UserInfo.getLastName() + ', ' + DateTime.now().format('d MMM yyyy HH:mm');
    			
    			for(PotentialProcedureInput procedurePotential : potentials){
				    			
    				for(PotentialProcedureCompetitorInput compInput : procedurePotential.competitors){
    					
    					if(compInput.changed == true){
    						
    						Segmentation_Potential_Input_Competitor__c inputCompetitor = updatedMap.get(compInput.record.Id);
    						
    						if(compInput.record.Potential__c != null && (inputCompetitor == null || compInput.record.Potential__c != inputCompetitor.Potential__c)) compInput.potentialTS = ts;
    						else if(compInput.record.Potential__c == null) compInput.potentialTS = null;    					
    					}    					
    				}    			
    			}
    		}
    			
    		if(compInputs_Delete.size() > 0) delete compInputs_Delete;
    		
    		
    		
    		
    		return potentials;
    		
    	}catch(Exception e){
    		
    		System.debug(e.getMessage() + ' | ' + e.getStackTraceString());
    		Database.rollback(sp);
    		AuraHandledException auraEx = new AuraHandledException(e.getMessage()); 
    		if(test.isRunningTest()) auraEx.setMessage(e.getMessage());
    		throw auraEx;
    	}    	
    }
         
    public class PotentialProcedureInput{
    	    	
    	@AuraEnabled public boolean changed {get; set;}    	
    	@AuraEnabled public PotentialAccount account {get; set;}
    	@AuraEnabled public Segmentation_Potential_Input__c record {get; set;}
    	@AuraEnabled public List<PotentialProcedureCompetitorInput> competitors {get; set;}
    	@AuraEnabled public Segmentation_Potential_Input__c prevRecord {get; set;}
    	@AuraEnabled public Account_Segmentation_Validation__c currentSegmentation {get; set;}
    	@AuraEnabled public String completedTS {get; set;}
    	@AuraEnabled public String approved1TS {get; set;}
    	@AuraEnabled public String approved2TS {get; set;}
    	@AuraEnabled public String potentialTS {get; set;}
    	@AuraEnabled public String potentialMDTTS {get; set;}    	    	    	
    }
    
    public class PotentialAccount{
    	
    	@AuraEnabled public String accountName {get; set;}
    	@AuraEnabled public String accountId {get; set;}
    	@AuraEnabled public String accountSAPId {get; set;}
    	@AuraEnabled public String accountCity {get; set;}
    	@AuraEnabled public String accountSegment {get; set;}   
    	
    	public PotentialAccount(){}
    	
    	public PotentialAccount(Account acc){
    		
    		accountName = acc.Name;
			accountId = acc.Id;
			accountCity = acc.BillingCity;
			accountSAPId = acc.SAP_Id__c;			    		
    	} 	
    }
    
    public class PotentialProcedureCompetitorInput{
    	
    	@AuraEnabled public Segmentation_Potential_Input_Competitor__c record {get; set;}
    	@AuraEnabled public Segmentation_Potential_Input_Competitor__c prevRecord {get; set;}
    	@AuraEnabled public boolean changed {get; set;}
    	@AuraEnabled public String potentialTS {get; set;}    	
    }
}