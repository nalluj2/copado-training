@isTest
private class Test_ctrlExt_Opportunity_Lot_Edit {
        
    private static Sub_Business_Units__c sbu;
    private static Opportunity tenderOpp;
    private static RecordType tenderRT = [Select Name, Id from RecordType where sObjectType = 'Opportunity' AND DeveloperName = 'Business_Critical_Tender'];
    
    private static testmethod void testAddOpportunityLots(){
    	
    	createTestData();
    			
		User currentUser = new User(Id = UserInfo.getUserId());
		currentUser.Company_Code_text__c = 'T35';
		update currentUser;
		
		Test.startTest();
		
		Opportunity_Lot__c oppLot = new Opportunity_Lot__c();
			oppLot.Opportunity__c = tenderOpp.Id;
			oppLot.Price_Weight__c = 25;
			oppLot.Quality_Weight__c = 25;
			oppLot.Other_Weight__c = 50;

		
		ctrlExt_Opportunity_Lot_Edit controller = new ctrlExt_Opportunity_Lot_Edit(new ApexPages.StandardController(oppLot));
		
		System.assert(controller.oppLots.size() == 0);
		System.assert(controller.oppSBUs.size() == 1);
		System.assert(controller.oppAccounts.size() == 1);
		
		controller.addNewLot();
		
		controller.editOppLot.lot.Name = 'Unit Test Lot';
		controller.editOppLot.lot.Lot_Owner__c = UserInfo.getUserId();
		controller.onSBUChange();
		
		controller.applyEdit();
		
		System.assert(controller.editOppLot == null);
		System.assert(controller.oppLots.size() == 1);
		
		controller.oppLots[0].lot.Award_Type__c = 'Hunting License';
		controller.oppLots[0].lot.Price_Weight__c = 10;
		controller.oppLots[0].lot.Quality_Weight__c = 10;
		controller.oppLots[0].lot.Other_Weight__c = 80;
		
		PageReference result = controller.save();
						
		System.assert(result != null);
		
		List<Opportunity_Lot__c> oppLots = [Select Id from Opportunity_Lot__c where Opportunity__c = :tenderOpp.Id];
				
		System.assert(oppLots.size() == 1);		
	}
    
    private static testmethod void testEditOpportunityLot(){
    	
    	createTestData();
    					
		User currentUser = new User(Id = UserInfo.getUserId());
		currentUser.Company_Code_text__c = 'T35';
		update currentUser;
		
		Test.startTest();
		
		Opportunity_Lot__c oppLot = new Opportunity_Lot__c();
			oppLot.Opportunity__c = tenderOpp.Id;
			oppLot.Name = 'Unit Test Lot';
			oppLot.Sub_Business_Unit__c = sbu.Id;
			oppLot.Award_Type__c = 'Hunting License';
			oppLot.Lot_Owner__c = UserInfo.getUserId();
			oppLot.Price_Weight__c = 25;
			oppLot.Quality_Weight__c = 25;
			oppLot.Other_Weight__c = 50;
		insert oppLot;
								
		ctrlExt_Opportunity_Lot_Edit controller = new ctrlExt_Opportunity_Lot_Edit(new ApexPages.StandardController(oppLot));
						
		System.assert(controller.oppLots.size() == 1);
		
		controller.selectedLot = oppLot.Id;
		controller.editLot();
		
		System.asserT(controller.editOppLot != null);
		
		controller.editOppLot.lot.Name = 'Unit Test Lot Changed';
		controller.applyEdit();
		
		PageReference result = controller.save();
		
		System.assert(result != null);
						
		List<Opportunity_Lot__c> lots = [Select Id, Name from Opportunity_Lot__c where Opportunity__c = :TenderOpp.Id];
		
		System.assert(lots.size() == 1);
		System.assert(lots[0].Name == 'Unit Test Lot Changed');	
    }
    
    private static testmethod void testInlineCloneOpportunityLot(){
    	
    	createTestData();
    					
		User currentUser = new User(Id = UserInfo.getUserId());
		currentUser.Company_Code_text__c = 'T35';
		update currentUser;
		
		Test.startTest();
		
		Opportunity_Lot__c oppLot = new Opportunity_Lot__c();
			oppLot.Opportunity__c = tenderOpp.Id;
			oppLot.Name = 'Unit Test Lot';
			oppLot.Sub_Business_Unit__c = sbu.Id;
			oppLot.Award_Type__c = 'Hunting License';
			oppLot.Lot_Owner__c = UserInfo.getUserId();
			oppLot.Price_Weight__c = 25;
			oppLot.Quality_Weight__c = 25;
			oppLot.Other_Weight__c = 50;
		insert oppLot;
								
		ctrlExt_Opportunity_Lot_Edit controller = new ctrlExt_Opportunity_Lot_Edit(new ApexPages.StandardController(oppLot));
						
		System.assert(controller.oppLots.size() == 1);
		
		controller.selectedLot = oppLot.Id;
		controller.cloneLot();
		
		System.assert(controller.editOppLot != null);
		System.assert(controller.selectedLot != String.valueOf(oppLot.Id));
		System.assert(controller.oppLots.size() == 2);
		
		controller.editOppLot.lot.Name = 'Unit Test Lot Cloned';
		controller.applyEdit();
		
		PageReference result = controller.save();
		
		System.assert(result != null);
						
		List<Opportunity_Lot__c> lots = [Select Id, Name from Opportunity_Lot__c where Opportunity__c = :TenderOpp.Id];
		
		System.assert(lots.size() == 2);			
    }
    
    private static testmethod void testCloneOpportunityLot(){
    	
    	createTestData();
    					
		User currentUser = new User(Id = UserInfo.getUserId());
		currentUser.Company_Code_text__c = 'T35';
		update currentUser;
		
		Test.startTest();
		
		Opportunity_Lot__c oppLot = new Opportunity_Lot__c();
			oppLot.Opportunity__c = tenderOpp.Id;
			oppLot.Name = 'Unit Test Lot';
			oppLot.Sub_Business_Unit__c = sbu.Id;
			oppLot.Award_Type__c = 'Hunting License';
			oppLot.Lot_Owner__c = UserInfo.getUserId();
			oppLot.Price_Weight__c = 25;
			oppLot.Quality_Weight__c = 25;
			oppLot.Other_Weight__c = 50;
		insert oppLot;
		
		ApexPages.currentPage().getParameters().put('clone', '1');				
		ctrlExt_Opportunity_Lot_Edit controller = new ctrlExt_Opportunity_Lot_Edit(new ApexPages.StandardController(oppLot));
						
		System.assert(controller.oppLots.size() == 2);
		System.assert(controller.selectedLot != null);
		System.assert(controller.editOppLot != null);
						
		controller.editOppLot.lot.Name = 'Unit Test Lot cloned';
		
		PageReference result = controller.save();
						
		System.assert(result != null);
		
		List<Opportunity_Lot__c> lots = [Select Id from Opportunity_Lot__c where Opportunity__c = :tenderOpp.Id];
		
		System.assert(lots.size() == 2);
    }
    
    private static testmethod void testDeleteOpportunityLot(){
    	
    	createTestData();
    					
		User currentUser = new User(Id = UserInfo.getUserId());
		currentUser.Company_Code_text__c = 'T35';
		update currentUser;
		
		Test.startTest();
		
		Opportunity_Lot__c oppLot = new Opportunity_Lot__c();
			oppLot.Opportunity__c = tenderOpp.Id;
			oppLot.Name = 'Unit Test Lot';
			oppLot.Sub_Business_Unit__c = sbu.Id;
			oppLot.Award_Type__c = 'Hunting License';
			oppLot.Lot_Owner__c = UserInfo.getUserId();
			oppLot.Price_Weight__c = 25;
			oppLot.Quality_Weight__c = 25;
			oppLot.Other_Weight__c = 50;
		insert oppLot;
								
		ctrlExt_Opportunity_Lot_Edit controller = new ctrlExt_Opportunity_Lot_Edit(new ApexPages.StandardController(oppLot));
						
		System.assert(controller.oppLots.size() == 1);		
		System.assert(controller.editOppLot != null);
		
		controller.cancelEdit();
		
		System.assert(controller.editOppLot == null);
		
		controller.selectedLot = oppLot.Id;
		controller.deleteLot();
		
		PageReference result = controller.save();
						
		System.assert(result != null);
		
		List<Opportunity_Lot__c> lots = [Select Id from Opportunity_Lot__c where Opportunity__c = :tenderOpp.Id];
		
		System.assert(lots.size() == 0);
    }
    
    private static void createTestData(){
    	
    	Account acc = new Account();        
			acc.Name = 'Test Germany';        
			acc.Account_Country_vs__c = 'GERMANY';
			acc.RecordTypeId = [Select Id from RecordType where SObjectType = 'Account' AND DeveloperName = 'SAP_Account'].Id;        
			acc.Account_Active__c = true;
			acc.SAP_ID__c = '1111111111';
			//acc.SAP_Channel__c = '30';
        insert acc;
        
        Company__c cmpny = new Company__c();		
			cmpny.name='Europe';
			cmpny.CurrencyIsoCode = 'EUR';
			cmpny.Current_day_in_Q1__c=56;
			cmpny.Current_day_in_Q2__c=34; 
			cmpny.Current_day_in_Q3__c=5; 
			cmpny.Current_day_in_Q4__c= 0;   
			cmpny.Current_day_in_year__c =200;
			cmpny.Days_in_Q1__c=56;  
			cmpny.Days_in_Q2__c=34;
			cmpny.Days_in_Q3__c=13;
			cmpny.Days_in_Q4__c=22;
			cmpny.Days_in_year__c =250;
			cmpny.Company_Code_Text__c = 'T35';
		insert cmpny;
		
		Business_Unit_Group__c bug =  new Business_Unit_Group__c();
			bug.Master_Data__c =cmpny.id;
			bug.name='Restorative';      
        insert bug;
		
		Business_Unit__c bu =  new Business_Unit__c();
			bu.Company__c =cmpny.id;
			bu.name='Neurovascular';
			bu.Business_Unit_Group__c = bug.Id;
			bu.Account_Plan_Activities__c=true;        
        insert bu;
        
			sbu = new Sub_Business_Units__c();
			sbu.name='Neurovascular';
			sbu.Business_Unit__c=bu.id;
		insert sbu;
		
		tenderOpp = new Opportunity();
			tenderOpp.RecordTypeId = tenderRT.Id;
			tenderOpp.AccountId = acc.Id;
			tenderOpp.Name = 'Test Tender Opportunity';
			tenderOpp.CloseDate = Date.today().addDays(30);
			tenderOpp.StageName = 'Prospecting/Lead';	
			tenderOpp.Sub_Business_Unit__c = 'Neurovascular';	
		insert tenderOpp;		    	
    }
}