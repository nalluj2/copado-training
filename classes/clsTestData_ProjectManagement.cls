@isTest public class clsTestData_ProjectManagement {

	public static Integer iRecord_ProjectManagement = 1;
	public static List<Project_Management__c> lstProjectManagement;
	public static Project_Management__c oMain_ProjectManagement;

	public static Integer iRecord_Release = 1;
	public static List<Project__c> lstRelease;
	public static Project__c oMain_Release;

	public static Integer iRecord_ChangeRequest = 1;
	public static List<Change_Request__c> lstChangeRequest;
	public static Change_Request__c oMain_ChangeRequest;

	public static Integer iRecord_ReportingActivity = 1;
	public static List<Reporting_Activity__c> lstReportingActivity;
	public static Reporting_Activity__c oMain_ReportingActivity;
	
	public static Integer iRecord_RegistrationCode = 1;
	public static List<Time_Registration_Code__c> lstRegistrationCode;
	public static Time_Registration_Code__c oMain_RegistrationCode;

	public static Integer iRecord_TimeReporting_Release = 1;
	public static Integer iHours_TimeReporting_Release = 2;
	public static List<Time_Reporting__c> lstTimeReporting_Release;
	public static Time_Reporting__c oMain_TimeReporting_Release;

	public static Integer iRecord_TimeReporting_ProjectManagement = 1;
	public static Integer iHours_TimeReporting_ProjectManagement = 2;
	public static List<Time_Reporting__c> lstTimeReporting_ProjectManagement;
	public static Time_Reporting__c oMain_TimeReporting_ProjectManagement;

	public static Integer iRecord_TimeReporting_ChangeRequest = 1;
	public static Integer iHours_TimeReporting_ChangeRequest = 2;
	public static List<Time_Reporting__c> lstTimeReporting_ChangeRequest;
	public static Time_Reporting__c oMain_TimeReporting_ChangeRequest;



	//---------------------------------------------------------------------------------------------------
    // Create Project Management Data
    //---------------------------------------------------------------------------------------------------
    public static List<Project_Management__c> createProjectManagement() {

    	return createProjectManagement(true);

    }

    public static List<Project_Management__c> createProjectManagement(Boolean bInsert){

    	if (oMain_ProjectManagement == null){
			
			if (oMain_RegistrationCode == null) createRegistrationCode();
			
	    	lstProjectManagement = new List<Project_Management__c>();
	    	for (Integer i = 0; i < iRecord_ProjectManagement; i++){

				Project_Management__c oProjectManagement = new Project_Management__c();
					oProjectManagement.Project_Name__c='Test Project';
					oProjectManagement.Requestor__c = UserInfo.getUserId();
					oProjectManagement.Region__c = 'Western Europe and Canada';
					oProjectManagement.Project_Description__c = 'Unit Test Description';
					oProjectManagement.Business_Rationale__c = 'Increase Revenue';
					oProjectManagement.Background_Information__c = 'Unit Test information';
					oProjectManagement.Problem_Statement__c = 'No Problem';
					//oProjectManagement.Kronos_Category__c = 'catCode';
					//oProjectManagement.Kronos_Project__c = 'projCode';
					oProjectManagement.Time_Registration_Project__c = oMain_RegistrationCode.Id;
				lstProjectManagement.add(oProjectManagement);

	    	}

	    	if (lstProjectManagement.size() > 0){
		    	if (bInsert) insert lstProjectManagement;
		    	oMain_ProjectManagement = lstProjectManagement[0];
		    }

	    }

    	return lstProjectManagement;
    
    }
    //---------------------------------------------------------------------------------------------------


	//---------------------------------------------------------------------------------------------------
    // Create Project / Release Data
    //---------------------------------------------------------------------------------------------------
    public static List<Project__c> createRelease() {

    	return createRelease(true);

    }

    public static List<Project__c> createRelease(Boolean bInsert){

    	if (oMain_Release == null){
			
			if (oMain_RegistrationCode == null) createRegistrationCode();
			
    		lstRelease = new List<Project__c>();
    		for (Integer i = 0; i < iRecord_Release;  i++){
				Project__c oRelease = new Project__c();
					oRelease.Name = 'Test Release ' + String.valueOf(i);
					oRelease.Start_Date__c = Date.today();
					oRelease.Project_Type__c = 'Minor Release';
					oRelease.Status__c = 'Scheduled';
					oRelease.Project_Manager__c = UserInfo.getUserId();
					oRelease.End_Date__c = Date.today().addDays(10);
					//oRelease.Kronos_Category__c = 'catCode';
					//oRelease.Kronos_Project__c = 'projCode';
					oRelease.Time_Registration_Project__c = oMain_RegistrationCode.Id;
					oRelease.Total_Budget__c = 1000;
				lstRelease.add(oRelease);
			}

			if (lstRelease.size() > 0){
				if (bInsert) insert lstRelease;
				oMain_Release = lstRelease[0];
			}
    	}

    	return lstRelease;
    }
	//---------------------------------------------------------------------------------------------------


	//---------------------------------------------------------------------------------------------------
    // Create Change Request Data
    //---------------------------------------------------------------------------------------------------
    public static List<Change_Request__c> createChangeRequest() {

    	return createChangeRequest(true, false);

    }

    public static List<Change_Request__c> createChangeRequest(Boolean bInsert, Boolean bRelatedToProjectManagment){

    	if (oMain_ChangeRequest == null){

	    	if (bRelatedToProjectManagment){
	    		if (oMain_ProjectManagement == null) createProjectManagement();
	    	}
	    	
	    	if (oMain_RegistrationCode == null) createRegistrationCode();

	    	lstChangeRequest = new List<Change_Request__c>();
	    	for (Integer i = 0; i < iRecord_ChangeRequest; i++){

	    		Change_Request__c oChangeRequest= new Change_Request__c();
					oChangeRequest.Change_Request__c = 'Unit test sample CR ' + String.valueOf(i);
					oChangeRequest.Details__c = 'Unit Test details';
					oChangeRequest.Status__c = 'New';
					oChangeRequest.Origin__c = 'Support Portal';			
					oChangeRequest.Request_Date__c = Date.today();
					oChangeRequest.Priority__c = 'Medium';		
					oChangeRequest.Requestor_Lookup__c = UserInfo.getUserId();
					//oChangeRequest.Kronos_Category__c = 'catCode';
					//oChangeRequest.Kronos_Project__c = 'projCode';
					oChangeRequest.Time_Registration_Project__c = oMain_RegistrationCode.Id;
					if (bRelatedToProjectManagment){
						oChangeRequest.Project_Management__c = oMain_ProjectManagement.Id;
					}
	    		lstChangeRequest.add(oChangeRequest);
	    	}

	    	if (lstChangeRequest.size() > 0){
		    	if (bInsert) insert lstChangeRequest;
		    	oMain_ChangeRequest = lstChangeRequest[0];
	    	}

	    }

	    return lstChangeRequest;

	}	
    //---------------------------------------------------------------------------------------------------


	//---------------------------------------------------------------------------------------------------
    // Create Reporting Activity Data (needed on Time Reporting)
    //---------------------------------------------------------------------------------------------------
    public static List<Reporting_Activity__c> createReportingActivity(){

    	return createReportingActivity(true);

    }

    public static List<Reporting_Activity__c> createReportingActivity(Boolean bInsert){

    	if (oMain_ReportingActivity == null){

    		lstReportingActivity = new List<Reporting_Activity__c>();
    		for (Integer i = 0; i < iRecord_ReportingActivity; i++){

				Reporting_Activity__c oReportActivity = new Reporting_Activity__c();
					oReportActivity.Name = 'Test Activity';
					oReportActivity.Requires_Manual_Input__c = false;
					oReportActivity.chargeable__c = true;
				lstReportingActivity.add(oReportActivity);
			}
			
			if (lstReportingActivity.size() > 0){
				if (bInsert) insert lstReportingActivity;
				oMain_ReportingActivity = lstReportingActivity[0];
			}
		}

		return lstReportingActivity;

	}
	//---------------------------------------------------------------------------------------------------
	
	//---------------------------------------------------------------------------------------------------
    // Create Registration Code Data (needed on Time Reporting)
    //---------------------------------------------------------------------------------------------------
    public static List<Time_Registration_Code__c> createRegistrationCode(){

    	return createRegistrationcode(true);

    }

    public static List<Time_Registration_Code__c> createRegistrationCode(Boolean bInsert){

    	if (oMain_RegistrationCode == null){

    		lstRegistrationCode = new List<Time_Registration_Code__c>();
    		for (Integer i = 0; i < iRecord_RegistrationCode; i++){

				Time_Registration_Code__c oRegistrationCode = new Time_Registration_Code__c();
					oRegistrationCode.Name = 'Test Project ' + i;					
					oRegistrationCode.Time_Registration_Category__c = 'Test Category ' + i;
					oRegistrationCode.Active__c = true;
					oRegistrationCode.Business_Unit__c = 'Test BU';
					oRegistrationCode.Time_Registration_ID__c = 'TRC' + i;
					
				lstRegistrationCode.add(oRegistrationCode);
			}
			
			if (lstRegistrationCode.size() > 0){
				if (bInsert) insert lstRegistrationCode;
				oMain_RegistrationCode = lstRegistrationCode[0];
			}
		}

		return lstRegistrationCode;

	}
	//---------------------------------------------------------------------------------------------------


	//---------------------------------------------------------------------------------------------------
    // Create Time Reporting - Related to Release Data
    //---------------------------------------------------------------------------------------------------
    public static List<Time_Reporting__c> createTimeReporting_Release(){

    	return createTimeReporting_Release(true);

    }

    public static List<Time_Reporting__c> createTimeReporting_Release(Boolean bInsert){

    	if (oMain_TimeReporting_Release == null){

    		if (oMain_Release == null) createRelease();
    		if (oMain_ReportingActivity == null) createReportingActivity();

    		lstTimeReporting_Release = new List<Time_Reporting__c>();
    		for (Integer i = 0; i < iRecord_TimeReporting_Release; i++){

				Time_Reporting__c oTimeReporting = new Time_Reporting__c();
					oTimeReporting.Reporting_Activity__c = oMain_ReportingActivity.Id;
					oTimeReporting.Release__c = oMain_Release.Id;
					oTimeReporting.Hours__c = iHours_TimeReporting_Release;
					oTimeReporting.OwnerId = UserInfo.getUserId();
					oTimeReporting.Date__c = Date.today();		
				lstTimeReporting_Release.add(oTimeReporting);
			}

			if (lstTimeReporting_Release.size() > 0){
				if (bInsert) insert lstTimeReporting_Release;
				oMain_TimeReporting_Release = lstTimeReporting_Release[0];
			}

    	}

    	return lstTimeReporting_Release;

    }
	//---------------------------------------------------------------------------------------------------


	//---------------------------------------------------------------------------------------------------
    // Create Time Reporting - Related to Project Management Data
    //---------------------------------------------------------------------------------------------------
    public static List<Time_Reporting__c> createTimeReporting_ProjectManagement(){

    	return createTimeReporting_ProjectManagement(true);

    }

    public static List<Time_Reporting__c> createTimeReporting_ProjectManagement(Boolean bInsert){

    	if (oMain_TimeReporting_ProjectManagement == null){

    		if (oMain_ProjectManagement == null) createProjectManagement();
    		if (oMain_ReportingActivity == null) createReportingActivity();

    		lstTimeReporting_ProjectManagement = new List<Time_Reporting__c>();
    		for (Integer i = 0; i < iRecord_TimeReporting_ProjectManagement; i++){

				Time_Reporting__c oTimeReporting = new Time_Reporting__c();
					oTimeReporting.Reporting_Activity__c = oMain_ReportingActivity.Id;
					oTimeReporting.Project__c = oMain_ProjectManagement.Id;
					oTimeReporting.Hours__c = iHours_TimeReporting_ProjectManagement;
					oTimeReporting.OwnerId = UserInfo.getUserId();
					oTimeReporting.Date__c = Date.today();		
				lstTimeReporting_ProjectManagement.add(oTimeReporting);
			}

			if (lstTimeReporting_ProjectManagement.size() > 0){
				if (bInsert) insert lstTimeReporting_ProjectManagement;
				oMain_TimeReporting_ProjectManagement = lstTimeReporting_ProjectManagement[0];
			}

    	}

    	return lstTimeReporting_ProjectManagement;

    }
	//---------------------------------------------------------------------------------------------------


	//---------------------------------------------------------------------------------------------------
    // Create Time Reporting - Related to Change Request Data
    //---------------------------------------------------------------------------------------------------
    public static List<Time_Reporting__c> createTimeReporting_ChangeRequest(){

    	return createTimeReporting_ChangeRequest(true);

    }

    public static List<Time_Reporting__c> createTimeReporting_ChangeRequest(Boolean bInsert){

    	if (oMain_TimeReporting_ChangeRequest == null){

    		if (oMain_ChangeRequest == null) createChangeRequest();
    		if (oMain_ReportingActivity == null) createReportingActivity();

    		lstTimeReporting_ChangeRequest = new List<Time_Reporting__c>();
    		for (Integer i = 0; i < iRecord_TimeReporting_ChangeRequest; i++){

				Time_Reporting__c oTimeReporting = new Time_Reporting__c();
					oTimeReporting.Reporting_Activity__c = oMain_ReportingActivity.Id;
					oTimeReporting.Change_Request__c = oMain_ChangeRequest.Id;
					oTimeReporting.Hours__c = iHours_TimeReporting_ChangeRequest;
					oTimeReporting.OwnerId = UserInfo.getUserId();
					oTimeReporting.Date__c = Date.today();		
				lstTimeReporting_ChangeRequest.add(oTimeReporting);
			}

			if (lstTimeReporting_ChangeRequest.size() > 0){
				if (bInsert) insert lstTimeReporting_ChangeRequest;
				oMain_TimeReporting_ChangeRequest = lstTimeReporting_ChangeRequest[0];
			}

    	}

    	return lstTimeReporting_ChangeRequest;

    }
    //---------------------------------------------------------------------------------------------------

}