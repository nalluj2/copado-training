/**
 * Creation Date :  20090305
 * Description :    Test coverage for controller controllerAffiliationSelector
 * Author :         ABSI - BC
 */

@isTest
private class TEST_controllerAffiliationSelector {


    private Map<String, String> dataMap = new Map<String, String>();
    
    private void testControllerAffiliationSelector() {
        
        DIB_Country__c nl = [SELECT Id,  BU_Not_Mandatory_on_Relationship__c, Hide_Therapy_on_Relationship__c FROM DIB_Country__c where Country_ISO_Code__c = 'NL'];
        nl.BU_Not_Mandatory_on_Relationship__c = true;
        nl.Hide_Therapy_on_Relationship__c = true;
        update nl;
        
        // Create test sObjects 
        Account acc1 = new Account();
        acc1.Name = 'TEST_controllerAffiliationSelector Test Coverage Account 1 ' ; 
        acc1.Account_Country_vs__c = 'NETHERLANDS';
        Test.startTest();
        insert acc1 ; 
        
        Account acc2 = new Account();
        acc2.Name = 'TEST_controllerAffiliationSelector Test Coverage Account 2 ' ; 
        acc2.Account_Country_vs__c = 'NETHERLANDS';
        insert acc2;
        
        Contact cont1 = new Contact();
        cont1.LastName = 'TEST_controllerAffiliationSelector TestCont1' ;  
        cont1.FirstName = 'Test Contact 1' ;  
        cont1.AccountId = acc1.Id ;
        cont1.Contact_Inactive_Reason__c = 'InactiveReason'; 
        cont1.Contact_Active__c = false  ;  
	    cont1.Contact_Department__c = 'Diabetes Adult'; 
	    cont1.Contact_Primary_Specialty__c = 'ENT';
	    cont1.Affiliation_To_Account__c = 'Employee';
	    cont1.Primary_Job_Title_vs__c = 'Manager';
	    cont1.Contact_Gender__c = 'Male'; 
        insert cont1;
        
        Contact cont2 = new Contact();
        cont2.LastName = 'TEST_controllerAffiliationSelector TestCont2';  
        cont2.FirstName = 'Test Contact 2' ;  
        cont2.AccountId = acc1.Id ;
        cont2.Contact_Inactive_Reason__c = 'InactiveReason';
        cont2.Contact_Active__c = false;         
	    cont2.Contact_Department__c = 'Diabetes Adult'; 
	    cont2.Contact_Primary_Specialty__c = 'ENT';
	    cont2.Affiliation_To_Account__c = 'Employee';
	    cont2.Primary_Job_Title_vs__c = 'Manager';
	    cont2.Contact_Gender__c = 'Male'; 
        insert cont2;       
        
        List<Contact> testContacts = new List<Contact>();
        testContacts.add(cont1);
        testContacts.add(cont2);
        
        // Create test Affiliation
        
        Affiliation__c insertAff=new Affiliation__c();
        insertAff.Affiliation_From_Contact__c = testContacts.get(0).Id;
        insertAff.Affiliation_From_Account__c = acc1.Id ; 
        insertAff.Affiliation_To_Contact__c = testContacts.get(1).Id;
        insertAff.Affiliation_To_Account__c = acc2.Id ; 
        insertAff.RecordTypeId = FinalConstants.recordTypeIdC2C;
        insert insertAff;
        
        // Prepare testAffiliation for redirect with different recordTypeIds
        String URL = '/apex/affiliationSelectorPage?Id=' + insertAff.Id;
        dataMap.put('URL',URL);
        dataMap.put('recordTypeId',FinalConstants.recordTypeIdC2C);
        this.testAffiliation(insertAff);

        insertAff.RecordTypeId = FinalConstants.recordTypeIdC2A;
        dataMap.put('recordTypeId',FinalConstants.recordTypeIdC2A);
        this.testAffiliation(insertAff);

        insertAff.RecordTypeId = FinalConstants.recordTypeIdA2A;
        dataMap.put('recordTypeId',FinalConstants.recordTypeIdA2A);
        this.testAffiliation(insertAff);
        
        Test.stopTest();
        System.debug(' ################## ' + 'END TEST_controllerAffiliationSelector' + ' ################');
        
    }
    
    private void testAffiliation(Affiliation__c curAff) {
        ApexPages.StandardController newSct = new ApexPages.StandardController(curAff); //set up the standardcontroller     
        controllerAffiliationSelector newController = new controllerAffiliationSelector(newSct);
        newController.setDataMap(dataMap);
        newController.redirectToAffiliationCreatePages();
    }
    
    static testMethod void myUnitTest() {

        System.debug(' ################## ' + 'BEGIN TEST_controllerAffiliationSelector' + ' ################');
        
        TEST_controllerAffiliationSelector myTest = new TEST_controllerAffiliationSelector();
        
        myTest.testControllerAffiliationSelector();
        
        System.debug(' ################## ' + 'END TEST_controllerAffiliationSelector' + ' ################');
        
    }
}