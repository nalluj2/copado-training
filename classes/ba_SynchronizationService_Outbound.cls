/*	Integration Service related class
	
	This batch takes care to send the notifications generated in source to the target Org. On each execution it takes in scope all the notifications
	with status New (not sent yet) so each notification will be re-tried after any possible error in previous processing. Please notice that only 
	notificatons of type Outbound are processed (to avoid conflicts when the same Org can be target and source of notifications). 
*/
global class ba_SynchronizationService_Outbound implements Database.Batchable<SObject>, Database.Stateful, Database.AllowsCallouts {
	
	//List of errors occured due to connection problems
	private List<String> errors = new List<String>();	
	
	private bl_SynchronizationService_Utils.SessionInfo sessionInfo;
	
	global List<Sync_Notification__c> start(Database.BatchableContext BC){
      	
      	sessionInfo = bl_SynchronizationService_Utils.getTargetOrgSession();
      	      	      	
      	List<Sync_Notification__c> notifications = new List<Sync_Notification__c>();      	      	
      	
      	//If the service is disabled we do not process any notification
      	if(bl_SynchronizationService_Utils.isSyncEnabled == true){
      	
	      	//Notifications about Complaint creations go first. Otherwise in the target Org we will get updates on Cases, Comments or Work Orders to link to Complaints that doesn't exist yet.      	      	
	      	List<Sync_Notification__c> complaintNotifications = [Select Id, Status__c, Type__c, Notification_Id__c, Record_Id__c, Record_Object_Type__c, Process_Time__c from Sync_Notification__c where Status__c = 'New' AND Type__c = 'Outbound' AND Record_Object_Type__c = 'Complaint__c' ORDER BY CreatedDate ASC ];
	      	List<Sync_Notification__c> otherNotifications = [Select Id, Status__c, Type__c, Notification_Id__c, Record_Id__c, Record_Object_Type__c, Process_Time__c from Sync_Notification__c where Status__c = 'New' AND Type__c = 'Outbound' AND Record_Object_Type__c != 'Complaint__c' ORDER BY CreatedDate ASC ];
	      	
	      	if(complaintNotifications.size() > 0) notifications.addAll(complaintNotifications);
	      	if(otherNotifications.size() > 0) notifications.addAll(otherNotifications);
      	}
      	
      	return notifications;
   	}
   	
   	global void execute(Database.BatchableContext BC, List<Sync_Notification__c> notifications){
        
        //The body of the request is a list with the notifications
	    SynchronizationServiceRequest syncRequest = new SynchronizationServiceRequest();
	    syncRequest.notifications = notifications;
	    
	    SynchronizationServiceResponse response;
	    
	    try{
	    	
	    	try{
	    		
	    		response = (SynchronizationServiceResponse) bl_SynchronizationService_Utils.executeService('/services/apexrest/SynchronizationServiceTarget', 'POST', JSON.serialize(syncRequest), SynchronizationServiceResponse.class, sessionInfo);
	    		
	    	}catch(Exception ex) {
	    			
    			if(ex.getMessage().contains('INVALID_SESSION_ID')){
    				
    				sessionInfo = bl_SynchronizationService_Utils.getTargetOrgSession();
    				
    				response = (SynchronizationServiceResponse) bl_SynchronizationService_Utils.executeService('/services/apexrest/SynchronizationServiceTarget', 'POST', JSON.serialize(syncRequest), SynchronizationServiceResponse.class, sessionInfo);
    				
    			}else throw ex;
    		}	
	    	
	    }catch(Exception e){
	    	
	    	errors.add(e.getMessage() + ' - ' + e.getStacktraceString());
	    	
	    	return;
	    }
     	
     	//If we receive a response successully, we collect the Notifications returned (the ones that succeeded) and we update their status to Processed
     	DateTime now = DateTime.now();
     
     	for(Sync_Notification__c notification : response.notifications){
     	
     		notification.Status__c = 'Processed';
     		notification.Process_Time__c = now; 
     	}
     
    	update response.notifications;     
    }
 
 	//If we go any error during the execution, we inform the 'watchers' about it
   	global void finish(Database.BatchableContext BC){
   		
   		if(errors.size() > 0){
   			
   			bl_SynchronizationService_Utils.notifySyncErrors(errors);   			  	
   		}   		
   	}
   	   	
   	//Model classes for the requests and responses. To be used by the JSON serialize/deserialize
   	public class SynchronizationServiceRequest{
		public List<Sync_Notification__c> notifications {get;set;}
	}
	
	public class SynchronizationServiceResponse{
		public List<Sync_Notification__c> notifications {get;set;}
	}		
}