//---------------------------------------------------------------------------------------------------------------------------------------------------
//  Author		:    Bart Caelen
//  Date 		:    20141217
//  Description	:    CR-5245 + CR-7731 - This batch/scheduled Class will collect the Opportunities that need to be process to Auto Follow 
//---------------------------------------------------------------------------------------------------------------------------------------------------
global class ba_Opportunity_BusinessLeaderFollow implements Database.Batchable<sObject>, Schedulable {

	global Boolean bProcessService = false;

	// SCHEDULE Settings
	global void execute(SchedulableContext sc){
		ba_Opportunity_BusinessLeaderFollow oBatch = new ba_Opportunity_BusinessLeaderFollow();
		oBatch.bProcessService = false;
		Database.executebatch(oBatch, 200);         

		ba_Opportunity_BusinessLeaderFollow oBatch_Service = new ba_Opportunity_BusinessLeaderFollow();
		oBatch_Service.bProcessService = true;
		Database.executebatch(oBatch_Service, 200);         

	} 

	// BATCH Settings
	global Database.QueryLocator start(Database.BatchableContext BC) {

		// Only process Opportunities that are linked to a Business Unit which have the Opp_RT_followed_by_Business_Owner__c != null
		String tSOQL = 'SELECT Id, Amount, CurrencyIsoCode, isClosed, Territory2Id, RecordTypeId, RecordType.DeveloperName, Business_Unit__c, Business_Unit__r.Name, Country__c';
		tSOQL += ', (SELECT Id, PriceBookEntry.Pricebook2Id, PriceBookEntry.Product2.Product_Group__r.Therapy_Id__r.Sub_Business_Unit__c, PriceBookEntry.Product2.Product_Group__r.Therapy_Id__r.Business_Unit__c FROM OpportunityLineItems)';
		tSOQL += ' FROM Opportunity';
		if (bProcessService){
			tSOQL += ' WHERE Business_Unit__r.Opp_ServiceRT_followed_by_Business_Owner__c != null';
		}else{
			tSOQL += ' WHERE Business_Unit__r.Opp_RT_followed_by_Business_Owner__c != null';
		}

		return Database.getQueryLocator(tSOQL);

	}

	global void execute(Database.BatchableContext BC, List<Opportunity> lstOpportunity) {

		if (bProcessService){
			bl_Opportunity.followOpportunityBusinessLeader(lstOpportunity);
		}else{
			bl_Opportunity.followOpportunityBusinessLeader_Service(lstOpportunity);
		}
		
	}
		
	global void finish(Database.BatchableContext BC) {}

}
//---------------------------------------------------------------------------------------------------------------------------------------------------