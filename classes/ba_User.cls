/*
 *      Created Date : 20140313
 *      Description : Batch to process Users 
 * 
 *      Author = Rudy De Coninck
 */
global class ba_User implements Schedulable,Database.Batchable<sObject> { 

    public String tQuery = 'SELECT Id, Last_Application_Login__c, Last_Application_Login_Status__c, CreatedDate FROM User';
    public Integer iBatchSize = 5;
    
    // Default Constructor
    global ba_User(){
        if (Test.isRunningTest()){
            iBatchSize = 1;
            tQuery += ' LIMIT 1';
        }
    }
    global ba_User(String atQuery, Integer aiBatchSize){
        tQuery = atQuery;
        iBatchSize = aiBatchSize;
    }
   
    global void execute(SchedulableContext ctx){        
        // Start Batch Apex job        
        Database.executeBatch(new ba_User(), iBatchSize);    
    } 
        

    global Database.QueryLocator start(Database.BatchableContext ctx){    
        return Database.getQueryLocator(tQuery);         
    }

    global void execute(Database.BatchableContext ctx, List<SObject> records){
        
        List<User> recordsToProcess = (List<User>)records;

		List<User> usersToUpdate = bl_User.updateUserWithLastApplicationLoginStatus(recordsToProcess);
        
        if(usersToUpdate.size()>0){
        	update usersToUpdate;
        }
        
    }

    global void finish(Database.BatchableContext ctx){
    }  	

}