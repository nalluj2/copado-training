public class bl_Segmentation {
    
    public static void updateAccountSegment(List<Segmentation__c> triggerNew){
    	
    	Set<Id> therapyIds =  new Set<Id>();
 	
	 	for (Segmentation__c segm : triggerNew){
	 	
	 		if (segm.End__c == null) therapyIds.add(segm.Therapy__c);
	 	}
	 	
	 	if(therapyIds.size() == 0) return;
	 	
	 	Map<Id, String> mapSegmentationFields = new Map<Id, String>();
	 	
	 	for(Therapy__c therapy : [SELECT Id, Therapy_Name_Hidden__c FROM Therapy__c WHERE Id IN :therapyIds]){
	 		
	 		String fieldName = therapy.Therapy_Name_Hidden__c.replace(' ','_').replace('-','_').replace('/','_');
	 		
	 		mapSegmentationFields.put(therapy.Id, fieldName + '_Segment__c');
	 	}
	 	
	 	Set<String> setFieldName_Account = accFieldsMap.keySet();
	 	for (String tFieldName : setFieldName_Account){
	 		tFieldName = tFieldName.toLowerCase();
	 	}

	 	Map<Id, Account> accountsToUpdate = new Map<Id, Account>();
	 	for (Segmentation__c segm : triggerNew){
	 		
	 		if (segm.End__c == null){
	 			
	 			String fieldName = mapSegmentationFields.get(segm.Therapy__c); 
	 			
	 			if (fieldName != null && setFieldName_Account.contains(fieldName.toLowerCase())){	//-BC - 20170831 - Added setFieldName_Account.contains(fieldName.toLowerCase())

		 			Account accountToUpdate = accountsToUpdate.get(segm.Account__c);
		 			
		 			if(accountToUpdate == null){
		 			
		 				accountToUpdate = new Account(Id = segm.Account__c);
		 				accountsToUpdate.put(accountToUpdate.id,accountToUpdate);
		 			}
		 			
		 			accountToUpdate.put(fieldName, segm.Segment__c);

	 			}
	 		}
	 	}
	 	
	 	if(accountsToUpdate.size() > 0) update accountsToUpdate.values();    	
    }   
    
    private static Map<String, Schema.SObjectField> accFieldsMap {
    	
    	get{
    		
    		if(accFieldsMap == null) accFieldsMap = Schema.SObjectType.Account.fields.getMap();
    		
    		return accFieldsMap;
    	}
    	
    	set;
    }
    
    public static void clearAccountSegmentValue(List<Segmentation__c> triggerOld){
    	    	
 		Set<Id> therapyIds =  new Set<Id>();
 	
	 	for (Segmentation__c segm : triggerOld){
	 	
	 		// Only when the Segmentation deleted is the active one (End Date is null) 
	 		if(segm.End__c == null) therapyIds.add(segm.Therapy__c);
	 	}
	 	
	 	if(therapyIds.size() == 0) return;
	 	
	 	Map<Id, String> mapSegmentationFields = new Map<Id, String>();
	 	
	 	for(Therapy__c therapy : [SELECT Id, Therapy_Name_Hidden__c FROM Therapy__c WHERE Id IN :therapyIds]){
	 		
	 		String fieldName = therapy.Therapy_Name_Hidden__c.replace(' ','_').replace('-','_').replace('/','_');
	 		
	 		mapSegmentationFields.put(therapy.Id, fieldName + '_Segment__c');
	 	}

	 	Set<String> setFieldName_Account = accFieldsMap.keySet();
	 	for (String tFieldName : setFieldName_Account){
	 		tFieldName = tFieldName.toLowerCase();
	 	}
	 	
	 	Map<Id, Account> accountsToUpdate = new Map<Id, Account>();
     	for (Segmentation__c segm : triggerOld){
	 		
	 		if (segm.End__c == null){
	 			
	 			String fieldName = mapSegmentationFields.get(segm.Therapy__c); 
	 			
	 			if (fieldName != null && setFieldName_Account.contains(fieldName.toLowerCase())){	//-BC - 20170831 - Added setFieldName_Account.contains(fieldName.toLowerCase())
	 				
		 			Account accountToUpdate = accountsToUpdate.get(segm.Account__c);
		 			
		 			if(accountToUpdate == null){
		 			
		 				accountToUpdate = new Account(Id = segm.Account__c);
		 				accountsToUpdate.put(accountToUpdate.id, accountToUpdate);
		 			}
		 			
		 			accountToUpdate.put(fieldName, null);
	 			}
	 		}
	 	}
	 	
	 	if(accountsToUpdate.size() > 0) update accountsToUpdate.values();
    }
    
    public static void endPreviousSegmentation(List<Segmentation__c> triggerNew){
    	
    	Map<String, Segmentation__c> mapNewSegmentations =  new Map<String, Segmentation__c>();
    	
	    Set<Id> accountIds = new Set<Id>();
	    Set<Id> therapyIds = new Set<Id>();
	    
	    for(Segmentation__c segNew : triggerNew){
	    	
	    	if(segNew.End__c == null){
	    		
	    		String key = segNew.Account__c + '' + segNew.Therapy__c;
	    		mapNewSegmentations.put(key, segNew);
	    	
	    		accountIds.add(segNew.Account__c);
	    		therapyIds.add(segNew.Therapy__c);
	    	}
	    }
	    
	    if(accountIds.size() == 0) return;
	    
	    //Get existing segmentation without end date
	    List<Segmentation__c> segmentationOld = [select Account__c, Therapy__c, End__c, Start__c, UniqueKey_Ac_Ther__c, Id from Segmentation__c 
	    	where Account__c IN :accountIds and Therapy__c IN :therapyIds and End__c = null AND Id NOT IN :triggerNew];
	  
	  	Map<String,Segmentation__c> mapOldSegmentations =  new Map<String,Segmentation__c>();
	  	
	  	for(Segmentation__c segOld : segmentationOld){
	  		
	  		String key = segOld.Account__c + '' + segOld.Therapy__c;
	    	mapOldSegmentations.put(key, segOld);
	  	} 
	  		      
		List<Segmentation__c> segmentUpdate = new List<Segmentation__c>();
		
		for(Segmentation__c segNew : triggerNew){
			
			if(segNew.End__c == null){
			
				String key = segNew.Account__c + '' + segNew.Therapy__c;      
			    
			    Segmentation__c segOld = mapOldSegmentations.get(key);
		        
		        if(segOld != null){
		        	
		        	// If the start date of the existing Segmentation is earlier than the start date of the new Segmentation, we update the existing one to end it
		        	if(segNew.Start__c > segOld.Start__c){
		          		
		          		Date dt = segNew.start__c - 1;
		            	segOld.End__c = dt;
		            	segmentUpdate.add(segOld);
		        	}	        	        	
		        } 
			}
	    }
	    
		if(segmentUpdate.size() > 0) update segmentUpdate;		  	
    }
    
    public static void validateSegmentOverlap(List<Segmentation__c> triggerNew){
    	
    	Set<Id> accountIds = new Set<Id>();
    	Set<Id> therapyIds = new Set<Id>();
    	
    	for(Segmentation__c segm : triggerNew){
    		
    		accountIds.add(segm.Account__c);
    		therapyIds.add(segm.Therapy__c);
    	}
    	
    	Map<String, List<Segmentation__c>> accountTherapySegmentationMap = new Map<String, List<Segmentation__c>>();  
    	
    	for(Segmentation__c segm :[Select Id, Name, Account__c, Therapy__c, Start__c, End__c from Segmentation__c where Account__c IN :accountIds AND Therapy__c IN :therapyIds]){
    		
    		String key = segm.Account__c + '' + segm.Therapy__c;
    		
    		List<Segmentation__c> accountTherapySegmentations = accountTherapySegmentationMap.get(key);
    		
    		if(accountTherapySegmentations == null){
    			
    			accountTherapySegmentations = new List<Segmentation__c>();
    			accountTherapySegmentationMap.put(key, accountTherapySegmentations);
    		}
    		
    		accountTherapySegmentations.add(segm);
    	}
    	
    	for(Segmentation__c segm : triggerNew){
    		
    		String key = segm.Account__c + '' + segm.Therapy__c;
    		
    		List<Segmentation__c> accountTherapySegmentations = accountTherapySegmentationMap.get(key);
    		
    		Segmentation__c duplicate = segmentOverlapCheck(segm, accountTherapySegmentations);
    		
    		if(duplicate != null) segm.addError('This Segmentation overlaps with the record \'' + duplicate.Name + '\' (' + duplicate.Id + ')');
    	}    	
    }
    
    private static Segmentation__c segmentOverlapCheck(Segmentation__c segm, List<Segmentation__c> otherSegms){
    	
    	DateRange range = new DateRange(segm.Start__c, segm.End__c);
    	
    	for(Segmentation__c otherSegm : otherSegms){
    		
    		if(segm.Id == otherSegm.Id) continue;
    		
    		DateRange otherRange = new DateRange(otherSegm.Start__c, otherSegm.End__c);
    		
    		if(range.overlaps(otherRange)) return otherSegm;
    	}
    	
    	return null;
    }
}