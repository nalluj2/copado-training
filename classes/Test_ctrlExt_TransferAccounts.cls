//--------------------------------------------------------------------------------------------------------------------
//  Description : This TEST APEX Class is used to test the logic in ctrlExt_TransferAccounts
//  Version     : 1.0
//  Author      : Bart Caelen
//  Date        : 08/09/2016
//--------------------------------------------------------------------------------------------------------------------
@isTest private class Test_ctrlExt_TransferAccounts {
	
	private static Affiliation__c oAffiliation_TransferTo;

    @isTest static void createTestData() {

    	//---------------------------------------------------------------------------
    	// Create Test Data
    	//---------------------------------------------------------------------------
    	// Create Affiliation C2A Data
    	clsTestData_Affiliation.iRecord_Affiliation_C2A = 10;
    	clsTestData_Affiliation.createAffiliation_C2A();
    	
    	// Create new Account Data (this Account will be used to transfer the Affiliations to)
    	clsTestData_Account.oMain_Account = null;
    	clsTestData_Account.iRecord_Account = 1;
    	clsTestData_Account.createAccount();
    	oAffiliation_TransferTo = clsTestData_Affiliation.createAffiliation_C2A(clsTestData_Account.oMain_Account, clsTestData_Contact.lstContact[5], false, true)[0];
    	clsTestData_Affiliation.createAffiliation_C2A(clsTestData_Account.oMain_Account, clsTestData_Contact.lstContact[6], true, true);
    	//---------------------------------------------------------------------------

    }

	@isTest static void test_ctrlExt_TransferAccounts() {

		// Create Test Data
		createTestData();

		// Load Test Data
		List<Affiliation__c> lstAffiliation = 
			[
				SELECT
					Id, Name, Affiliation_To_Account__c, Contact_Name__c, Department__c, Contact_Primary_Specialty__c
					, Affiliation_From_Contact__c, Affiliation_From_Contact__r.Contact_Active__c, Affiliation_From_Contact__r.firstname, Affiliation_From_Contact__r.lastname
					, Affiliation_Primary__c,Affiliation_Active__c, Affiliation_Start_Date__c, Affiliation_End_Date__c, Affiliation_Type__c,Affiliation_Position_In_Account__c 
				FROM 
					Affiliation__c
			];

		List<Account> lstAccount = [SELECT Id FROM Account WHERE Id = :lstAffiliation[0].Affiliation_To_Account__c];

		Test.startTest();

    	PageReference oPageRef = Page.page_TransferContacts;
        Test.setCurrentPage(oPageRef);

        System.currentPageReference().getParameters().put('Id', lstAccount[0].Id);
        ctrlExt_TransferAccounts oCtrlExt = new ctrlExt_TransferAccounts(new ApexPages.StandardController(lstAccount[0]));

        oCtrlExt.newAccount = oAffiliation_TransferTo;

		List<ctrlExt_TransferAccounts.wrpContacts> lstwrpContacts = oCtrlExt.getRelationships();
		for (ctrlExt_TransferAccounts.wrpContacts owrpContact : lstwrpContacts){
			owrpContact.selected = true;
		}
        oCtrlExt.transferSelectedContacts();

		Test.stopTest();

	}
	
}
//--------------------------------------------------------------------------------------------------------------------