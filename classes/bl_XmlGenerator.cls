public with sharing class bl_XmlGenerator {
	
	private static string baseXml ='<?xml version="1.0" encoding="UTF-8"?>\n' +
							 '<Package xmlns="http://soap.sforce.com/2006/04/metadata">\n'+
							 '<types>\n';
	private static string membersStart = '<members>\n';
	private static string membersEnd = '\n</members>\n';
	private static string nameStart = '<name>\n';
	private static string nameEnd = '\n</name>\n';
	private static string typesStart = '<types>\n';
	private static string typesEnd = '</types>\n';
	private static string xmlEnd ='<version>28.0</version>\n' +
								  '</Package>';
	
	public static string generatePackageXml(List<ReleaseDeploymentComponentWrapper> rdcw){
		string xmlToReturn = baseXml;
		
		string packageType = '';
		for(ReleaseDeploymentComponentWrapper rdcwr : rdcw){
			if(packageType == '' || packageType == null){
				packageType = rdcwr.rdc.Meta_data_type__c;
			}
			
			if(packageType == '' || packageType == null){
				continue;
			}
			
			if(rdcwr.rdc.API_Field_name__c != null && rdcwr.rdc.Meta_data_type__c.equals(packageType)){
				xmlToReturn += membersStart;
				xmlToReturn += rdcwr.rdc.API_Field_name__c;
				xmlToReturn += membersEnd;	 
			}
			else{
				xmlToReturn += nameStart;
				xmlToReturn += packageType;
				xmlToReturn += nameEnd;
				xmlToReturn += typesEnd;
				xmlToReturn += typesStart;
				
				packageType = rdcwr.rdc.Meta_data_type__c;	
				if(rdcwr.rdc.Meta_data_type__c.equals(packageType)&& rdcwr.rdc.API_Field_name__c != null){
					xmlToReturn += membersStart;
					xmlToReturn += rdcwr.rdc.API_Field_name__c;
					xmlToReturn += membersEnd;	 
				}		
			}
		}
		
		if(xmlToReturn.endsWith(membersEnd)){
			xmlToReturn += nameStart;
			xmlToReturn += packageType;
			xmlToReturn += nameEnd;
			xmlToReturn += typesEnd;
		}
		
		xmlToReturn += xmlEnd;
		return xmlToReturn;
	}
}