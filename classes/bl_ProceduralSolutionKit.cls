public class bl_ProceduralSolutionKit {
    
    private static Boolean fromCrossworld = false;
    
    public static Boolean isDMLAllowed(List<SObject> triggerNew){
    	
    	if(fromCrossworld == false && ProfileMedtronic.isSystemAdministratorProfile(UserInfo.getProfileId()) == false && PermissionSetMedtronic.hasPermissionSet('Box_Builder_Admin') == false){
    		
    		for(SObject record : triggerNew){
    			
    			record.addError('This record can only be managed in Crossworld or by designated administrators');	
    		}    	
    		
    		return false;		
    	}    	
    	
    	return true;
    }
    
    public static Procedural_Solutions_Kit__c saveProceduralSolutionKit(Procedural_Solutions_Kit__c proceduralSolutionsKit){
		
		fromCrossworld = true;
				
		upsert proceduralSolutionsKit Mobile_ID__c;
		
		fromCrossworld = false;
		
		return [Select Id, Name, Mobile_ID__c from Procedural_Solutions_Kit__c where Id = :proceduralSolutionsKit.Id][0];		
	}
	
	public static void deleteProceduralSolutionKit(Procedural_Solutions_Kit__c proceduralSolutionsKit){
		
		fromCrossworld = true;
				
		delete proceduralSolutionsKit;
		
		fromCrossworld = false;		
	}
	
	public static Procedural_Solutions_Kit_Product__c saveProceduralSolutionKitProduct(Procedural_Solutions_Kit_Product__c proceduralSolutionsKitProduct){
		
		fromCrossworld = true;
				
		upsert proceduralSolutionsKitProduct Mobile_ID__c;
		
		fromCrossworld = false;
		
		return proceduralSolutionsKitProduct;		
	}
	
	public static void deleteProceduralSolutionKitProduct(Procedural_Solutions_Kit_Product__c proceduralSolutionsKitProduct){
		
		fromCrossworld = true;
				
		delete proceduralSolutionsKitProduct;
		
		fromCrossworld = false;		
	}
	
	
	public static Procedural_Solutions_Kit_Share__c saveProceduralSolutionKitShare(Procedural_Solutions_Kit_Share__c proceduralSolutionsKitShare){
		
		fromCrossworld = true;
				
		upsert proceduralSolutionsKitShare Mobile_ID__c;
		
		fromCrossworld = false;
		
		return proceduralSolutionsKitShare;		
	}
	
	public static void deleteProceduralSolutionKitShare(Procedural_Solutions_Kit_Share__c proceduralSolutionsKitShare){
		
		fromCrossworld = true;
				
		delete proceduralSolutionsKitShare;
		
		fromCrossworld = false;		
	}
	
}