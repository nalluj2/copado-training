/*
 *   Controller name: ctrlExt_ManageUserSBUs
 *   Page Name : page_ManageUserSBUs
 *   Author : Priti Jiaswal
 *   Description: This class include logic to transfer DIB from one account to other
 *                account and call merge function to merge DIB. 
                 
*/

public class ctrlExt_TransferDiabetesSegmentation{
    
    // Variables    
    List<DIB_Department__c> existingDIB = new List<DIB_Department__c>();
    List<DIB_Invoice_Line__c> invDIB = new List<DIB_Invoice_Line__c>();
    List<DIB_Aggregated_Allocation__c> aggregateDIB = new List<DIB_Aggregated_Allocation__c>();
    List<DIB_Competitor_iBase__c> comptDIB = new List<DIB_Competitor_iBase__c>();
    List<DIB_Campaign_Account__c> campACCDIB = new List<DIB_Campaign_Account__c>();

    // Public Variables    
    private id previousAcountId;

    // Getter / Setter
    id currentRecordID { get;set; }
    public DIB_Department__c currentDibDeatil { get;set; }
    
    // Constructor      
    public ctrlExt_TransferDiabetesSegmentation(ApexPages.StandardController controller){
        
        currentRecordID = ApexPages.currentPage().getParameters().get('id');
        
        currentDibDeatil = 
            [
                SELECT
                    Id, Account__c, Account__r.Name,Account__r.IsPersonAccount, To_Account__c
                    , Is_Merged__c, Department_ID__r.Name
                FROM
                    DIB_Department__c
                WHERE 
                    Id = :currentRecordID
            ];
        
        previousAcountId = currentDibDeatil.Account__c;
    }

    // Actions
    public Pagereference transferDiabetes(){ 
             
        if (currentDibDeatil.To_Account__c == currentDibDeatil.Account__c){

            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Diabetes Segmentation cannot be transfered to the same account that it is currently linked to');
            ApexPages.addMessage(myMsg);                                       
            return null;

        }else{

            existingDIB = 
                [
                    SELECT 
                        Id, Name, Account__c, Department_ID__r.Name 
                    FROM 
                        DIB_Department__c
                    WHERE 
                        Account__c=:currentDibDeatil.To_Account__c
                ];            

            for (DIB_Department__c dib : existingDIB){
                if (currentDibDeatil.Department_ID__r.name == dib.Department_ID__r.name){
                    currentDibDeatil.Is_Merged__c = true;
                }
            }
            currentDibDeatil.IsTransfer__c = true;
            currentDibDeatil.Account__c = currentDibDeatil.To_Account__c;
            currentDibDeatil.To_Account__c = null;            
            update currentDibDeatil;             

            
            invDIB = 
                [
                    SELECT
                        Department_ID__c, Sales_Force_Account__c
                    FROM 
                        DIB_Invoice_Line__c
                    WHERE
                        Department_ID__c = :currentRecordID
                ];
            for (DIB_Invoice_Line__c inv : invDIB){
                inv.Sales_Force_Account__c = currentDibDeatil.Account__c;
            }
            update invDIB;


            aggregateDIB = 
                [
                    SELECT
                        DIB_Department_ID__c, Sales_Force_Account__c 
                    FROM
                        DIB_Aggregated_Allocation__c
                    WHERE
                        DIB_Department_ID__c = :currentRecordID
                ];
            for (DIB_Aggregated_Allocation__c agg : aggregateDIB){
                agg.Sales_Force_Account__c = currentDibDeatil.Account__c;
            }
            update aggregateDIB;


            comptDIB =
                [
                    SELECT 
                        Account_Segmentation__c, Sales_Force_Account__c
                    FROM
                        DIB_Competitor_iBase__c 
                    WHERE
                        Account_Segmentation__c = :currentRecordID
                ];
            for (DIB_Competitor_iBase__c comp : comptDIB){
                comp.Sales_Force_Account__c = currentDibDeatil.Account__c;
            }
            update comptDIB;            


            campACCDIB = 
                [
                    SELECT 
                        Account__c,Department_ID__c 
                    FROM
                        DIB_Campaign_Account__c 
                    WHERE
                        Department_ID__c = :currentRecordID
                ];
            for (DIB_Campaign_Account__c campAcc : campACCDIB){
                campAcc.Account__c = currentDibDeatil.Account__c;
            }
            update campACCDIB;

			/*CR-15511 Get rid of Contact Diabetes_Segmentation__c field
            //-BC - 20150616 - CR-6861 - START
            //------------------------------------------------------------------------------------------------------------------------------------------------
            // Get all Contacts which are related to the Diabetes Segmentation (Department) OR that are related to the OLD Account
            // These Contacts need to be updated:
            //  - the primary Account of the Contacts related to the Diabetes Segmentation should be updated to the new Account of the Diabates Segmentation (Department)
            //  - the Diabetes Segmentation of the Contacts related to the OLD Account will be updated to the new Diabetes Segmentation
            //  - the Address of the Contact will be updated to the Address of the new Account (Existing logic in tr_Contact_updateAddress)
            //------------------------------------------------------------------------------------------------------------------------------------------------
            List<Contact> lstContact = 
                [
                    SELECT
                        Id, Name, AccountId, Diabetes_Segmentation__c
                    FROM
                        Contact
                    WHERE
                        Diabetes_Segmentation__c = :currentRecordID
                        OR 
                        AccountId = :previousAcountId
                ];

            for (Contact oContact : lstContact){
                oContact.AccountId = currentDibDeatil.Account__c;
                oContact.Diabetes_Segmentation__c = currentDibDeatil.Id;
            }
            update lstContact;            
            //------------------------------------------------------------------------------------------------------------------------------------------------
            //-BC - 20150616 - CR-6861 - STOP
			*/
            
            bl_Account_Trigger.mergeAccountDepartments(new Set<Id>{currentDibDeatil.Account__c});            
            Pagereference pageref = new pagereference('/' + currentDibDeatil.Account__c);
            return pageref;
        }
    }
}