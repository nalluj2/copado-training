/*
*	Contains all business logic for product
*	Author: Rudy De Coninck
*	Creation date: 29-8-2013	
*
*/

public class bl_Product2 {
	
	private static Set<Id> alreadyValidated = new Set<Id>();
		
	private static Id marketingProductRTid{
		
		get{
			if(marketingProductRTid == null) marketingProductRTid = [Select Id from RecordType where SObjectType = 'Product2' AND DeveloperName = 'MDT_Marketing_Product'].Id;
			
			return marketingProductRTid;
		}
	}
	
	public static void checkMarketingProductHierarchy(List<Product2> productList, Map<Id, Product2> oldMap){
		
		if(bl_Trigger_Deactivation.isTriggerDeactivated('checkMrktProductHierarchy')) return;
		
		List<Product2> prodScope = new List<Product2>();
		Set<Id> buScope = new Set<Id>();
		
		for(Product2 prod : productList){
			
			//Not validated yet, Marketing Product and has a link to a parent Product
			if(prod.recordTypeId == marketingProductRTid && prod.Parent_Product__c != null && alreadyValidated.add(prod.Id) == true){
			
				// Insert or Undelete -> Change in the structure and therefore we need to validate
				// Update -> Only needs validation if the parent Product value has changed
				if(oldMap == null || (prod.Parent_Product__c != oldMap.get(prod.Id).Parent_Product__c)){
														
					prodScope.add(prod);
					buScope.add(prod.Business_Unit_ID__c);			
				}
			}
		}
		
		if(prodScope.size() >0){
			
			Map<Id, Map<Id, Product2>> productHierarchyByBU = new Map<Id, Map<Id, Product2>>();
			
			for(Product2 mrktProduct : [Select Id, Business_Unit_ID__c, (Select Id from Products__r) from Product2 where Business_Unit_ID__c IN : buScope AND RecordTypeId = :marketingProductRTid]){
				
				Map<Id, Product2> buProductHierarchy = productHierarchyByBU.get(mrktProduct.Business_Unit_ID__c);
				
				if(buProductHierarchy == null){
					
					buProductHierarchy = new Map<Id, Product2>();
					productHierarchyByBU.put(mrktProduct.Business_Unit_ID__c, buProductHierarchy);
				}
				
				buProductHierarchy.put(mrktProduct.Id, mrktProduct);
			}
			
			for(Product2 prod : prodScope){
				
				Map<Id, Product2> buProductHierarchy = productHierarchyByBU.get(prod.Business_Unit_ID__c);
				
				if(checkParentInHierarchy(prod, prod.Parent_Product__c, buProductHierarchy) == true){
					
					prod.addError('A loop has been detected in the Product Hierarchy. Please choose a Parent Product that does not belong to the Product hierarchy.');
				}
			}
		}
	}
	
	private static Boolean checkParentInHierarchy(Product2 prod, Id findInHierarchy, Map<Id, Product2> productHierarchy){
		
		Product2 prodHierarchy = productHierarchy.get(prod.Id);
		
		if(prodHierarchy != null){
			
			for(Product2 childProd : prodHierarchy.Products__r){
				
				if(childProd.Id == findInHierarchy) return true;
				else if(checkParentInHierarchy(childProd, findInHierarchy, productHierarchy) == true) return true;
			}						
		}
		
		return false;
	}
	/*
	public static void validateNotKitable(List<Product2> productList, Map<Id, Product2> oldMap){
		
		if(bl_Trigger_Deactivation.isTriggerDeactivated('validateNotKitable')) return;
		
		Map<Id, Product2> unflagged = new Map<Id, Product2>();
		
		for(Product2 prod : productList){
			
			if(prod.KitableIndex__c != 'Kitable' && oldMap.get(prod.Id).KitableIndex__c == 'Kitable'){
				
				unflagged.put(prod.Id, prod);
			}			
		}
		
		if(unflagged.size() > 0){
			
			List<Product2> prodWithTemplates = [Select Id, (Select Id from Procedural_Solutions_Kit_Template_Prod__r LIMIT 1) from Product2 where Id IN :unflagged.keySet()];
			
			for(Product2 prod : prodWithTemplates){
				
				if(prod.Procedural_Solutions_Kit_Template_Prod__r.size() > 0){
					
					Product2 trigProd = unflagged.get(prod.Id);
					trigProd.addError('This Product is being used in Pre-configured Boxes. Please remove its usage before marking it as not Kitable.');
				}
			}
		}
	}
	*/
	
	public static void informBoxOwnersOnProdRejection(List<Product2> triggerNew, Map<Id, Product2> oldMap){
    	
    	Map<Id, Product2> rejected = new Map<Id, Product2>();
    	    	    	    	
    	for(Product2 prod : triggerNew){
    		
    		if(prod.KitableIndex__c == 'Request Rejected' && oldMap.get(prod.Id).KitableIndex__c != 'Request Rejected'){
    		
    			rejected.put(prod.Id, prod);    			    			
    		}    		
    	}
    	
    	if(rejected.size() > 0){
    		
    		List<Messaging.SingleEmailMessage> notifications = new List<Messaging.SingleEmailMessage>();
    		
    		for(Procedural_Solutions_Kit_Product__c boxProd : [Select Procedural_Solutions_Kit__r.Name, Procedural_Solutions_Kit__r.Box_Description__c, Procedural_Solutions_Kit__r.Country__c, Procedural_Solutions_Kit__r.OwnerId, Procedural_Solutions_Kit__r.Owner.Name, Product__c, Product__r.Name from Procedural_Solutions_Kit_Product__c 
    																where Procedural_Solutions_Kit__r.Status__c = 'Open' AND Product__c IN :rejected.keySet()]){
    																	
    			Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
    			email.setTargetObjectId(boxProd.Procedural_Solutions_Kit__r.OwnerId);
    			email.setSaveAsActivity(false);    				    				
    			email.setSubject('Box Builder: Product \'' + boxProd.Product__r.Name + '\' in your Box \'' + boxProd.Procedural_Solutions_Kit__r.Name + '\' has been rejected');
    				
    			String body = 'Dear ' + boxProd.Procedural_Solutions_Kit__r.Owner.Name + ',<br/><br/>';
    			body += 'The product \'' + boxProd.Product__r.Name + '\' has been rejected and cannot be used in the configuration of your Box \'' + boxProd.Procedural_Solutions_Kit__r.Name + '\' (' + boxProd.Procedural_Solutions_Kit__r.Box_Description__c + ').<br/><br/>';
    			body += 'Reason for rejection: ' + rejected.get(boxProd.Product__c).Rejected_Reason__c;    				
    				
    			email.setHTMLBody(body);
    				
    			notifications.add(email);    			
    		}    		
    		
    		if(notifications.size() > 0) Messaging.sendEmail(notifications);    		
    	}
    }
	
	/*
	 *	CR-12836.
	 *	Alwin van Dijken | 7dots
	 */
	/* Replaced by the Product availability functionality
	public static void setKitable(List<Product2> productList, Map<Id, Product2> oldMap){

		for(Product2 item : productList){
			if(item.Kitable__c != oldMap.get(item.Id).get('Kitable__c') || item.KitableIndex__c == '' ){
				if(item.Kitable__c){
					item.KitableIndex__c = 'isKitable';
				} else {
					item.KitableIndex__c = 'isNotKitable';
				}
			}
		}

	}

	public static void setKitable(List<Product2> productList){
		for(Product2 item : productList){
			if(item.Kitable__c){
				item.KitableIndex__c = 'isKitable';
			} else {
				item.KitableIndex__c = 'isNotKitable';
			}
		}

	}
	*/
}