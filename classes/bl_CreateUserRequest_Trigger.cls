//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 28-03-2017
//  Description      : APEX Class - Business Logic for tr_CreateUserRequest
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public class bl_CreateUserRequest_Trigger {

	//----------------------------------------------------------------------------------------------------------------
	// Populate the field Last_Status_Change__c with the current date if the Status__c of the Support Request is changed.
	// Populate the field Last_Sub_Status_Change__c with the current date if the Sub_Status__c of the Support Request is changed.
	//----------------------------------------------------------------------------------------------------------------
	public static void processStatusChange(List<Create_User_Request__c> lstTriggerNew, Map<Id, Create_User_Request__c> mapTriggerOld){

		for (Create_User_Request__c oCreateUserRequest : lstTriggerNew){

			Create_User_Request__c oCreateUserRequest_Old = mapTriggerOld.get(oCreateUserRequest.Id);

			if (oCreateUserRequest.Status__c != oCreateUserRequest_Old.Status__c) oCreateUserRequest.Last_Status_Change__c = DateTime.now();
			if (oCreateUserRequest.Sub_Status__c != oCreateUserRequest_Old.Sub_Status__c) oCreateUserRequest.Last_Sub_Status_Change__c = DateTime.now();

		}

	}
	//----------------------------------------------------------------------------------------------------------------

}
//--------------------------------------------------------------------------------------------------------------------