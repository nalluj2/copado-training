/**
 * Creation Date :  20090305
 * Description :    Test Coverage for trigger AffiliationBeforeDelete
 * Author :         ABSI - BC
 */

@isTest
private class TEST_triggerAffiliationBeforeDelete {

    static testMethod void myUnitTest() {
		        
        Boolean isError = false;
        
//        User regularUser = [Select Id from User where Profile.NAme = 'EUR Field Force Neuromodulation' AND isActive = true AND Country = 'NETHERLANDS' LIMIT 1];
        User oUser_System = clsTestData_User.createUser_SystemAdministratorMDT('tadm01', true);
        User regularUser = clsTestData_User.createUser('test.user.001@medtronic.com.test', 'tu001', 'NETHERLANDS', clsUtil.getUserProfileId('EUR Field Force RTG'), clsUtil.getUserRoleId('EUR xBU'), true);
        
        Affiliation__c aff;
        
        System.runAs(regularUser){
        
        	// Account        
	        Account acc1 = new Account();
	        acc1.Name = 'UnitTest Account 1' ; 
	        acc1.SAP_ID__c = '0123456789';        
	        insert acc1;
	
	        // Contact        
	        Contact cnt1 = new Contact();
	        cnt1.LastName = 'Contact 1 ';
	        cnt1.FirstName = 'UnitTest';
	        cnt1.AccountId = acc1.Id ; 
	        cnt1.Contact_Department__c = 'Diabetes Adult'; 
	        cnt1.Contact_Primary_Specialty__c = 'ENT';
	        cnt1.Affiliation_To_Account__c = 'Employee';
	        cnt1.Primary_Job_Title_vs__c = 'Manager';
	        cnt1.Contact_Gender__c = 'Male';
	        insert cnt1;   
	
	    	aff = [select Id from Affiliation__c where Affiliation_From_Contact__c = :cnt1.Id];
	               
	        try{
	        	
	            delete aff;
	        
	        }catch (Exception e) {
	            
	            isError = true;
	            System.assert(e.getMessage().contains(Label.You_can_only_change_primary_relationships_via_the_contact_detail_page));
	        }
        
        }
        
        System.assert(isError == true);
        
        System.runAs(oUser_System){

	        delete aff;
	        aff = [select Id, isDeleted from Affiliation__c where Id = :aff.Id ALL ROWS];
	        System.assert(aff.isDeleted == true);

	    }
	    
    }
}