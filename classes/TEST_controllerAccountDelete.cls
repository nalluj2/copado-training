/**
 * Creation Date :  20090227
 * Description :    Test Coverage of the controller 'Account Delete' (Delete button is overwritten).
 *                  (To facilitate the development of robust, error-free code, Apex Code requires the creation and execution of unit tests.
 *                  Unit tests are comprised of test methods and classes that verify whether a particular piece of code is working properly.)
 * Author :         Medtronic - Jake
 */

@isTest
private class TEST_controllerAccountDelete {

    static testMethod void myUnitTest() {
            System.debug(' ################## ' + 'BEGIN TEST_controllerAccountDelete' + ' ################');  
            //AccountBeforeDeleteAsNormalUser();
            AccountBeforeDeleteAsNormalUserSap();           
            AccountBeforeDeleteAsAdminNoAffilions();
            //AccountBeforeDeleteAsAdminWithAffilions();
            System.debug(' ################## ' + 'END TEST_controllerAccountDelete' + ' ################');    
    }


    private static void AccountBeforeDeleteAsNormalUser(){      
        // Before start test coverage, it's requested to preparate all the needed data with DML statements : 
        // 2 different profiles linkes to 2 different users to test the trigger with both kind of users ! 
        //Id p2 = [select id from profile where name='Kyphon Front Office' limit 1].Id; 
        Id p2 = clsUtil.getUserProfileId('EUR Field Force RTG');
//        Id p2 = [select id from profile where name='EUR Field Force Spine & Biologics' limit 1].Id;
        User u2 = new User(alias = 'DsUser', email='dsusertest@absi.be', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                localesidkey='en_US', profileid = p2, timezonesidkey='America/Los_Angeles', username='dsusertest@absi.be', Alias_unique__c='usertestD1234',Company_Code_Text__c='EUR');
        //User U2 = new User() ; 
        //U2 = [Select id from User where profileId =:p2 and IsActive=true limit 1];
        
         //<!> startTest method to set the context of the following apex methods as separate from the previous data preparation or DML statements.  

         
         System.runAs(u2) {
             // The System Administrator is always able to delete all records ! But not the others, let's why this code is running as a normal user ! 
             System.debug('Current User: ' + UserInfo.getUserName());
             System.debug('Current Profile: ' + UserInfo.getProfileId());    
             
             Account acc1 = new Account();
             acc1.Name = 'TEST controllerAccountDelete Test Coverage Account 1 ' ; 
             insert acc1 ;
            
             Contact cont1 = new Contact();
             cont1.LastName = 'TEST_controllerAccountDelete Test Coverage' ;  
             cont1.AccountId = acc1.Id ;
             cont1.Contact_Department__c = 'Diabetes Adult'; 
             cont1.Contact_Primary_Specialty__c = 'ENT';
             cont1.Affiliation_To_Account__c = 'Employee';
             cont1.Primary_Job_Title_vs__c = 'Manager';
             cont1.Contact_Gender__c = 'Male'; 
             insert cont1 ;     
             
             Affiliation__c aff1 = new Affiliation__c();
             aff1.Affiliation_To_Contact__c = cont1.Id ;
             insert aff1 ;       
             
             // Delete Account  : 
             // Set a current page
             PageReference pageRef = new PageReference('/' + acc1.id);
             pageRef.getParameters().put('id',acc1.id);
             Test.setCurrentPage(pageRef);
             Account acct = new Account(); 
             ApexPages.StandardController sct = new ApexPages.StandardController(acct); //set up the standardcontroller     
             controllerAccountDelete controller = new controllerAccountDelete(sct);
            
             controller.setdelId(acc1.Id);
             controller.AccountDelete2();
             controller.getWarntext();
             controller.back();
             
          }
    }     
 


    /**
    *
    * Test to delete an Account with an SAP number.
    *
    */  

        private static void AccountBeforeDeleteAsNormalUserSap(){ 
        Id p3 = clsUtil.getUserProfileId('EUR Field Force RTG');
//        Id p3 = [select id from profile where name='EUR Field Force Spine & Biologics' limit 1].Id;
        User u3 = new User(alias = 'JkUser', email='Jkusertest@absi.be', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                    localesidkey='en_US', profileid = p3, timezonesidkey='America/Los_Angeles', username='jkusertest@absi.be', Alias_unique__c='usertestA1234',Company_Code_Text__c='EUR');
        //User U3 = [Select id from User where profileid =:p3 and IsActive=true limit 1];
        
         System.runAs(u3) {
             // The System Administrator is always able to delete all records ! But not the others, let's why this code is running as a normal user ! 
             System.debug('Current User: ' + UserInfo.getUserName());
             System.debug('Current Profile: ' + UserInfo.getProfileId());    
             
             Account acc1 = new Account();
             acc1.Name = 'TEST_controllerAccountDelete Test Coverage Account 111 ' ; 
             acc1.SAP_ID__c = '12345';
             insert acc1 ;
             

             Account acc2 = new Account();
             acc2.Name = 'TEST_controllerAccountDelete Test Coverage Account 11 ' ; 
             acc2.SAP_ID__c = '6789';
             insert acc2 ;
            
             Contact cont1 = new Contact();
             cont1.LastName = 'TEST_controllerAccountDelete Test Coverage123 ' ;  
             cont1.FirstName = 'Test Contact' ;  
             cont1.AccountId = acc1.Id ; 
             cont1.Contact_Department__c = 'Diabetes Adult'; 
             cont1.Contact_Primary_Specialty__c = 'ENT';
             cont1.Affiliation_To_Account__c = 'Employee';
             cont1.Primary_Job_Title_vs__c = 'Manager';
             cont1.Contact_Gender__c = 'Male';              
             insert cont1 ;     
             
             // The addError function will fire if there is any related object to Contact, so let's create one : (Per ex.: a new affiliation)
             Affiliation__c aff1 = new Affiliation__c();
             aff1.Affiliation_To_Contact__c = cont1.Id ;
             aff1.Affiliation_From_Account__c = acc2.Id ; 
             //insert aff1 ;       
             // Delete Account  : 
            // Set a current page
            PageReference pageRef = new PageReference('/' + acc1.id);
            pageRef.getParameters().put('id',acc1.id);
            Test.setCurrentPage(pageRef);
            Account acct = new Account(); 
            ApexPages.StandardController sct = new ApexPages.StandardController(acct); //set up the standardcontroller      
            controllerAccountDelete controller = new controllerAccountDelete(sct);

            controller.AccountDelete2();
            controller.getWarntext();
            controller.back();
         }
         }

    /**
    *
    * Test to delete an Account without affiliations.
    *
    */  
    private static void AccountBeforeDeleteAsAdminNoAffilions(){
        Id p4 = clsUtil.getUserProfileId('System Administrator');
//        Id p4 = [select id from profile where name='System Administrator' limit 1 ].Id; 
        User u4 = new User(alias = 'JnDsUser', email='jnusertest@absi.be', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                    localesidkey='en_US', profileid = p4, timezonesidkey='America/Los_Angeles', username='jnusertest@absi.be', Alias_unique__c='usertestB1234',Company_Code_Text__c='EUR');
         //User U4 = [Select id from User where profileid =:p4 and IsActive= true limit 1];
         
         System.runAs(u4) {
             // The System Administrator is always able to delete all records ! But not the others, let's why this code is running as a normal user ! 
             System.debug('Current User: ' + UserInfo.getUserName());
             System.debug('Current Profile: ' + UserInfo.getProfileId());    
             
             Account acc4 = new Account();
             acc4.Name = 'TEST_controllerAccountDelete Test Coverage Account 411 ' ; 
             insert acc4 ;
            
             
             // Delete Account  : 
            // Set a current page
            PageReference pageRef4 = new PageReference('/' + acc4.id);
            pageRef4.getParameters().put('id',acc4.id);
            Test.setCurrentPage(pageRef4);
            Account acct4 = new Account(); 
            ApexPages.StandardController sct4 = new ApexPages.StandardController(acct4); //set up the standardcontroller        
            controllerAccountDelete controller = new controllerAccountDelete(sct4);
            //controller.AccountDelete2();
            //controller.getWarntext();
            //controller.back();
         }
    }

    /**
     * Description : Make the test running as a System Administrator  !!! 
     *
    */
        private static void AccountBeforeDeleteAsAdminWithAffilions(){
        Id p1 = clsUtil.getUserProfileId('System Administrator');
//        Id p1 = [select id from profile where name='System Administrator' limit 1 ].Id;
        User u1 = new User(alias = 'admin', email='testadminUser@absi.be', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                    localesidkey='en_US', profileid = p1, timezonesidkey='America/Los_Angeles', username='testadminUser@absi.be', Alias_unique__c='usertestC1234',Company_Code_Text__c='EUR');
        //User U1 = [Select id from User where profileid =:p1 and IsActive= true limit 1];
        
        
         System.runAs(u1) {
             // The System Administrator is always able to delete all records ! But not the others, let's why this code is running as a normal user ! 
             // Test Mass Delete :     

             List <Account> accMass = new List<Account>();           
             for (Integer i=0; i < 7 ; i++){
                Account acc2 = new Account();
                acc2.Name = 'TEST_controllerAccountDelete Test Coverage Account 2345' + i ; 
                accMass.add(acc2) ;
             }  
             insert accMass;
             
             List <Contact> conMass = new List<Contact>();
             for (Account accs : [Select Id ,Name from Account where Name like 'Test Coverage Account%']){  
                Contact tempcont = new Contact();
                tempcont.LastName = 'Mass delcoverage ' + accs.Name.substring(18) ;
                tempcont.AccountId = accs.Id ; 
                conMass.add(tempcont) ;
             }
             insert conMass;
             
             List <Account> accDel = [Select Id from Account where Name like 'Test Coverage Account%'];
             List<Database.DeleteResult> DR_Dels ;
//           test.starttest();
             // Delete Account  : 
            // Set a current page
            PageReference pageRef = new PageReference('/' + accMass[0].id);
            pageRef.getParameters().put('id',accMass[0].id);
            Test.setCurrentPage(pageRef);
            Account acct = new Account(); 
            ApexPages.StandardController sct = new ApexPages.StandardController(acct); //set up the standardcontroller      
            controllerAccountDelete controller = new controllerAccountDelete(sct);

            //controller.AccountDelete2();
            controller.getWarntext();
            controller.back();
 
    }
    }

    
}