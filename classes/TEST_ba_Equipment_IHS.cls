/**
 * Created by dijkea2 on 26-4-2017.
 */

@isTest
private class TEST_ba_Equipment_IHS {

    static Integer iBatchSize = 100;
    public static Id idRecordType_Case_IHS = clsUtil.getRecordTypeByDevName('Case', 'IHS_Temporary_Solution').Id;
    public static Id idCaseQueueOther = [select Id, Name from Group where Name = 'IHS Equipment Mgmt Queue' AND Type = 'Queue' LIMIT 1].Id;
    public static Id idCaseQueueItaly = [select Id, Name from Group where Name = 'IHS Equipment Mgmt Italy Queue' AND Type = 'Queue' LIMIT 1].Id;

    @testSetup
    static void setup() {

        Id idRecordType_Supplier = clsUtil.getRecordTypeByDevName('Account', 'Supplier').Id;
        Id idRecordType_Account_SAPAccount = clsUtil.getRecordTypeByDevName('Account', 'SAP_Account').Id;
        
        clsTestData.iRecord_Account = 1;
        clsTestData.createAccountData();
        System.Debug('clsTestData -> lstAccount: ' + clsTestData.lstAccount);

        //Account
        List<Account> lstAccount = clsTestData_Account.createAccount(false);
        lstAccount[0].RecordTypeId = idRecordType_Account_SAPAccount;
        lstAccount[0].Account_Country_vs__c = 'BELGIUM';
        insert lstAccount;
        System.Debug('lstAccount: ' + lstAccount);

        //Supplier
        clsTestData_Account.oMain_Account = null;
        List<Account> lstSupplier = clsTestData_Account.createAccount(false);
        lstSupplier[0].RecordTypeId = idRecordType_Supplier;
        lstSupplier[0].Type = 'Service Supplier';
        insert lstSupplier;
        System.Debug('lstSupplier: ' + lstSupplier);

        //Contract
        /*
        Id idRecordType_HospitalContract = clsUtil.getRecordTypeByDevName('Contract_IHS__c', 'Hospital_Contract').Id;
        Id idRecordType_SupplierContract = clsUtil.getRecordTypeByDevName('Contract_IHS__c', 'Supplier_Contract').Id;
        List<Contract_IHS__c> lstContract = new List<Contract_IHS__c>();
        Contract_IHS__c oContract;
		*/

        List<Equipment_IHS__c> lstEquipment = new List<Equipment_IHS__c>();
        
        for(Account oAccount : lstAccount){
			
			/*
            lstContract.add( new Contract_IHS__c(
                    RecordTypeId = idRecordType_SupplierContract,
                    Start_Date__c = System.today(),
                    End_Date__c = System.today(),
                    Contract_Type__c = 'Maintenance', // Maintenance | Electrical Safety | Maintenance incl Electrical Safety
                    Active__c = true
            ));
			*/
			
            for (Integer i = 0; i < 5; i++){

                Equipment_IHS__c oEquipment = new Equipment_IHS__c();
                oEquipment.Account__c = oAccount.Id;
                oEquipment.Supplier__c = lstSupplier[0].Id;
                oEquipment.Serial_Number__c = 'SER_' + i;
                oEquipment.Next_PM_Date__c = System.today() + (i * 7);
                oEquipment.Next_EST_Date__c = System.today() + (i * 7);
                if(i == 0) oEquipment.Active__c = false;
                else oEquipment.Active__c = true;

                lstEquipment.add(oEquipment);
            }
        }
        
        System.Debug('Equipment size: ' + lstEquipment.size());
        System.Debug(lstEquipment);

        insert lstEquipment;
    }

    @isTest
    static void test_ba_Equipment_IHS_Scheduling() {
        
        Test.startTest();
        //---------------------------------------
        // TEST SCHEDULING
        //---------------------------------------
        string tCRON_EXP = '0 0 0 3 9 ? 2099';

        string tJobId = System.schedule('ba_Equipment_IHS_Test', tCRON_EXP, new ba_Equipment_IHS());

        // Get the information from the CronTrigger API object
        CronTrigger oCronTrigger = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :tJobId];

        // Verify the expressions are the same
        System.assertEquals(tCRON_EXP, oCronTrigger.CronExpression);

        // Verify the job has not run
        System.assertEquals(0, oCronTrigger.TimesTriggered);

        // Verify the next time the job will run
        System.assertEquals('2099-09-03 00:00:00', String.valueOf(oCronTrigger.NextFireTime));
        //---------------------------------------

        Test.stopTest();
    }

    @isTest
    static void test_ba_Equipment_IHS() {
        Test.startTest();

        //---------------------------------------
        // TEST BATCH
        //---------------------------------------
        ba_Equipment_IHS oBatch = new ba_Equipment_IHS();
        
        oBatch.notification_per_email = true;

        Database.executeBatch(oBatch, iBatchSize);
        //---------------------------------------

        Test.stopTest();
        
        List<Case> lstCase = [Select Id from Case where RecordType.DeveloperName = 'IHS_Temporary_Solution' AND Failure_Type__c IN ('Preventive Maintenance', 'Electrical Safety Inspection')];
        
        System.assert(lstCase.size() == 6);
    }

    @isTest
    static void test_ba_Equipment_IHS_withCases() {
        // Setup
        List<Equipment_IHS__c> lstEquipment = [SELECT
                Id, Name, Account__c, Supplier_Contract__c, Account__r.Account_Country_vs__c, Next_PM_Date__c
        FROM Equipment_IHS__c];
        
        List<Case> lstCase = new List<Case>();

        for(Equipment_IHS__c oEquipment : lstEquipment){
            
            Case oCase = new Case();
            oCase.RecordTypeId = idRecordType_Case_IHS;
            oCase.AccountId = oEquipment.Account__c;
            oCase.Equipment_IHS__c = oEquipment.Id;
            oCase.Status = 'New';           
            oCase.Origin = 'Internal';
            oCase.Failure_Type__c = 'Preventive Maintenance';            
            oCase.OwnerId = idCaseQueueOther;
            oCase.IHS_Eqmt_Next_PM_Date__c = oEquipment.Next_PM_Date__c;
            
            lstCase.add(oCase);
        }
        
        insert lstCase;
        
        //---------------------------------------

        Test.startTest();

        //---------------------------------------
        // TEST BATCH
        //---------------------------------------
        ba_Equipment_IHS oBatch = new ba_Equipment_IHS();
        
        Database.executeBatch(oBatch, iBatchSize);
        //---------------------------------------

        Test.stopTest();
        
        lstCase = [Select Id from Case where RecordType.DeveloperName = 'IHS_Temporary_Solution' AND Failure_Type__c IN ('Preventive Maintenance', 'Electrical Safety Inspection')];
        
        System.assert(lstCase.size() == 8);
    }

}