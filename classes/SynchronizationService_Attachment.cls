public without sharing class SynchronizationService_Attachment implements SynchronizationService_Object{
	
	//This method returns the External Ids of the records that have relevant modifications for the target Org. This includes created records or 
	//modified records where synchronized fields have changed. 
	public Set<String> hasChangeForNotification(List<SObject> records, Map<Id, SObject> oldRecords){
		
		Set<String> result = new Set<String>();
				
		//For attachments we only process after insert and using the SFDC Record Id as external identifier		
		for(SObject rawObject : records){
			
			Attachment record = (Attachment) rawObject;						
			result.add(record.Id);		
		}
		
		return result;
	}
	
	//This method collects and returns the complete information for a record. This includes the records itself as well as any related record that needs
	//to exist in the target Org too. The returned format is JSON serialized string.
	public String generatePayload(String externalId){
		
		AttachmentSyncPayload payload = new AttachmentSyncPayload();
		
		String query = 	'SELECT ' + String.join( syncFields, ',') + ' FROM Attachment WHERE Id = :externalId LIMIT 2';
						
		List<Attachment> attachments = Database.query(query);
				
		//If no record or more than 1 is found, we return an empty response
		if(attachments.size() == 1){
			
			Attachment attachment = attachments[0];	
									
			payload.Name = attachment.Name;
			payload.Description = attachment.Description;
			payload.AttachmentId = attachment.Id;			
			payload.parentType = String.valueOf(attachment.ParentId.getSObjectType());
										
			if(payload.parentType == 'Case'){
				
				Case parentCase = [Select External_Id__c from Case where Id = :attachment.ParentId];								
				
				payload.ParentId = parentCase.External_Id__c;
			}
						
			if(payload.parentType == 'Workorder__c'){
				
				Workorder__c parentWO = [Select External_Id__c from Workorder__c where Id = :attachment.ParentId];								
				
				payload.ParentId = parentWO.External_Id__c;
			}
			
			if(payload.parentType == 'Workorder_Sparepart__c'){
				
				Workorder_Sparepart__c parentSparepart = [Select External_Id__c from Workorder_Sparepart__c where Id = :attachment.ParentId];								
				
				payload.ParentId = parentSparepart.External_Id__c;
			}			
		}
		
		return JSON.serialize(payload);
	}
	
	//Implementation needed only in Target Org
	public String processPayload(String inputPayload){						
		return null;
	}
	
	//Model classes for the requests and responses. To be used by the JSON serialize/deserialize
	private class AttachmentSyncPayload extends SynchronizationService_Payload{
		
		public String name {get; set;}
		public String description {get; set;}
		public String attachmentId {get; set;}
		public String parentId {get; set;}
		public String parentType {get; set;}			
	}
	
	private List<String> syncFields = new List<String>{
		
		'Description',
		'Name',
		'ParentId'
	};
}