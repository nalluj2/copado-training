/*
 *      Created Date 	: 14-05-2014
 *      Author 			: Bart Caelen
 *      Description 	: TEST Class for the APEX TRIGGER tr_Task_FollowTask
 */
@isTest private class Test_tr_Task_FollowTask {
	
	@isTest static void test_tr_Task_FollowTask() {

		Id id_RecordType_Task = clsUtil.getRecordTypeByDevName('Task', 'Generic_Task').Id;

		//------------------------------------------------
		// CREATE TEST DATA
		//------------------------------------------------
		clsTestData.createCountryData();

		DIB_Country__c oCountry = clsTestData.mapCode_Country.get('BE');
		system.assertEquals(oCountry.Follow_Task__c, false);
			oCountry.Follow_Task__c = true;
		update oCountry;
		system.assertEquals(oCountry.Follow_Task__c, true);

		clsTestData.oMain_User = null;
		clsTestData.createUserData('test_tr_task_followtask@test.medtronic.com', 'BELGIUM', UserInfo.getProfileId(), null);
		system.assertEquals(clsTestData.oMain_User.Username,'test_tr_task_followtask@test.medtronic.com');

		Id idUser = UserInfo.getUserId();

		Id idtask_Main;

		Test.startTest();

		//------------------------------------------------------------------------------------------------
		// TEST 1 - INSERT - Assigned user will be the same as Owner of the task
		//------------------------------------------------------------------------------------------------
		System.runAs(clsTestData.oMain_User){
			clsTestData.id_RecordType_Task = id_RecordType_Task;
			clsTestData.createTaskData();

			List<EntitySubscription> lstEntitySubscription = 
				[
					SELECT ID 
					FROM EntitySubscription 
					WHERE ParentId = :clsTestData.oMain_Task.Id
				];

			system.assertEquals(lstEntitySubscription.size(), 0);
			idtask_Main = clsTestData.oMain_Task.Id;
		}
		//------------------------------------------------------------------------------------------------

		//------------------------------------------------------------------------------------------------
		// TEST 2 - INSERT - Assigned user will  be different from the Owner of the task
		//------------------------------------------------------------------------------------------------
		System.runAs(clsTestData.oMain_User){
			clsTestData.id_RecordType_Task = id_RecordType_Task;
			clsTestData.idTask_Owner = idUser;
			clsTestData.oMain_Task = null;
			clsTestData.createTaskData();

			List<EntitySubscription> lstEntitySubscription = 
				[
					SELECT ID 
					FROM EntitySubscription 
					WHERE ParentId = :clsTestData.oMain_Task.Id
				];

			system.assertEquals(lstEntitySubscription.size(), 1);
		}
		//------------------------------------------------------------------------------------------------

		//------------------------------------------------------------------------------------------------
		// TEST 3 - Another user follows the task - on completion of the task this user should still follow the task
		//------------------------------------------------------------------------------------------------
		// Subscribe also to the Task record
		EntitySubscription oEntitySubscription = new EntitySubscription();
			oEntitySubscription.ParentId = clsTestData.oMain_Task.Id;
			oEntitySubscription.SubscriberId = UserInfo.getUserId();
		Insert oEntitySubscription;

		List<EntitySubscription> lstEntitySubscription = 
			[
				SELECT ID 
				FROM EntitySubscription 
				WHERE ParentId = :clsTestData.oMain_Task.Id
			];

		system.assertEquals(lstEntitySubscription.size(), 2);

		// Test Update - set task to completed and remove the Creator Subscriber only
		clsTestData.oMain_Task.Status = 'Completed';
		update clsTestData.oMain_Task;

		lstEntitySubscription = 
			[
				SELECT ID 
				FROM EntitySubscription 
				WHERE ParentId = :clsTestData.oMain_Task.Id
			];

		system.assertEquals(lstEntitySubscription.size(), 1);
		//------------------------------------------------------------------------------------------------

		Test.stopTest();
		//------------------------------------------------
	}
	
}