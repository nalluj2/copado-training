@isTest
private class Test_SynchronizationService_Asset {
	
	private static testMethod void syncAsset(){
		
		Synchronization_Service_Setting__c syncSettings = new Synchronization_Service_Setting__c();
		syncSettings.username__c = 'test@medtronic.com';
		syncSettings.password__c = 'password';
		syncSettings.client_id__c = '123456789';
		syncSettings.client_secret__c = 'XXXXX';
		syncSettings.is_sandbox__c = true;
		syncSettings.sync_Enabled__c = true;
		insert syncSettings;
		
		//Products
		Product2 testProduct = new Product2();
		testProduct.Name = 'PoleStar';
		testProduct.ProductCode = '000999';
		testProduct.isActive = true;
					
		insert testProduct;
		
		//Account
		Account acc = new Account();	
		acc.AccountNumber = '0000000';
		acc.Name = 'Syn Test Account';
		acc.Phone = '+123456789';
		acc.BillingCity = 'Minneapolis';
		acc.BillingCountry = 'UNITED STATES';
		acc.BillingState = 'Minnesota';
		acc.BillingStreet = 'street';
		acc.BillingPostalCode = '123456';
		acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non-SAP Account').getRecordTypeId();	
		insert acc;
		
		//Contact
		Contact cnt = new Contact();
		cnt.FirstName = 'Test';
		cnt.LastName = 'Contact';
		cnt.Phone = '+123456789';		
		cnt.Email = 'test.contact@gmail.com';
		cnt.MobilePhone = '+123456789';		
		cnt.MailingCity = 'Minneapolis';
		cnt.MailingCountry = 'UNITED STATES';
		cnt.MailingState = 'Minnesota';
		cnt.MailingStreet = 'street';
		cnt.MailingPostalCode = '123456';
		cnt.External_Id__c = 'Test_Contact_Id';
		cnt.Contact_Department__c = 'Cardiology';
		cnt.Contact_Primary_Specialty__c = 'Cardiovascular'; 
		cnt.Affiliation_To_Account__c = 'Employee'; 
		cnt.Primary_Job_Title_vs__c = 'Surgeon'; 
		cnt.Contact_Gender__c = 'Male'; 
		cnt.AccountId = acc.Id;
		cnt.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Generic Contact').getRecordTypeId();				
		insert cnt;
		
		Test.startTest();
		
		Test_bl_SynchronizationService_Utils.CalloutMock mockImpl = new Test_bl_SynchronizationService_Utils.CalloutMock();
		
		Test.setMock(HttpCalloutMock.class, mockImpl);
		bl_SynchronizationService_Utils.calloutMock = mockImpl;
		
		//Asset
		Asset asset = new Asset();
		asset.AccountId = acc.Id;
		asset.Product2Id = testProduct.Id;
		asset.Asset_Product_Type__c = 'PoleStar N-10';
		asset.Ownership_Status__c = 'Purchased';
		asset.Name = '123456789';
		asset.Serial_Nr__c = '123456789';
		asset.Hospital_Point_Of_Contact__c = cnt.Id;
		asset.Status = 'Installed';		
		asset.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('PoleStar').getRecordTypeId();
		insert asset;
		
		asset.Last_PM_Completed_By__c = UserInfo.getUserId();
		asset.Storage_Area__c = 'Test Storage Area';
		update asset;		
								
		List<Sync_Notification__c> notifications = [Select Id from Sync_Notification__c where Status__c = 'New' AND Type__c = 'Outbound' AND Record_Object_Type__c = 'Asset'];
		
		System.assert(notifications.size() == 1);
		
		SynchronizationService_Asset assetService = new SynchronizationService_Asset();
		
		String payload = assetService.generatePayload('123456789');
		
		Test.stopTest();
		
		System.assert(payload != null);
	} 
	
	private static testMethod void syncAssetInbound(){
		
		Synchronization_Service_Setting__c syncSettings = new Synchronization_Service_Setting__c();
		syncSettings.username__c = 'test@medtronic.com';
		syncSettings.password__c = 'password';
		syncSettings.client_id__c = '123456789';
		syncSettings.client_secret__c = 'XXXXX';
		syncSettings.is_sandbox__c = true;
		syncSettings.sync_Enabled__c = true;
		insert syncSettings;
		
		//Products
		Product2 testProduct = new Product2();
		testProduct.Name = 'PoleStar';
		testProduct.ProductCode = '000999';
		testProduct.isActive = true;					
		insert testProduct;
		
		//Account
		Account acc = new Account();	
		acc.AccountNumber = '0000000';
		acc.SAP_ID__c = '0000000';
		acc.Name = 'Syn Test Account';
		acc.Phone = '+123456789';
		acc.BillingCity = 'Minneapolis';
		acc.BillingCountry = 'UNITED STATES';
		acc.BillingState = 'Minnesota';
		acc.BillingStreet = 'street';
		acc.BillingPostalCode = '123456';
		acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non-SAP Account').getRecordTypeId();	
		insert acc;
						
		//Asset
		Asset asset = new Asset();
		asset.AccountId = acc.Id;
		asset.Product2Id = testProduct.Id;
		asset.Asset_Product_Type__c = 'PoleStar N-10';
		asset.Ownership_Status__c = 'Purchased';
		asset.Name = '123456789';
		asset.Serial_Nr__c = '123456789';
		asset.Status = 'Installed';
		asset.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('PoleStar').getRecordTypeId();
		insert asset;
		
		Test.startTest();
		
		Test_bl_SynchronizationService_Utils.CalloutMock mockImpl = new Test_bl_SynchronizationService_Utils.CalloutMock();
		
		Test.setMock(HttpCalloutMock.class, mockImpl);
		bl_SynchronizationService_Utils.calloutMock = mockImpl;
		
		SynchronizationService_Asset.AssetSyncPayload request = new SynchronizationService_Asset.AssetSyncPayload();
						
		//Asset
		Asset sys = new Asset();
		sys.Serial_Nr__c = '123456789';
		sys.Computer_Type__c = 'Rollingstone';
		sys.IP_Address__c = '192.168.1.1';
		sys.MAC_Address__c = '0000-0000-0000-0000';
		request.systemAsset = sys;
				
		request.lastModifiedDate = DateTime.now();
		request.lastModifiedBy = 'Test User';
		
		String payload = JSON.serialize(request);
		
		bl_SynchronizationService_Target.processRecordPayload(payload, 'System__c');
		
		List<Asset> assets = [Select Id, IP_Address__c from Asset where Serial_Nr__c = '123456789'];
						
		System.assert(assets.size() == 1);
		System.assert(assets[0].IP_Address__c == '192.168.1.1');					
	} 	
	
	private static testMethod void syncAssetInboundNoAssetFound(){
		
		SynchronizationService_Asset.AssetSyncPayload request = new SynchronizationService_Asset.AssetSyncPayload();
						
		//Asset
		Asset sys = new Asset();
		sys.Serial_Nr__c = '123456789';
		sys.Computer_Type__c = 'Rollingstone';
		sys.IP_Address__c = '192.168.1.1';
		sys.MAC_Address__c = '0000-0000-0000-0000';
		request.systemAsset = sys;
					
		String response = bl_SynchronizationService_Target.processRecordPayload( JSON.serialize(request), 'System__c');
		
		System.assert(response == 'Asset not in INTL');
	}
}