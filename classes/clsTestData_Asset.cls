@isTest public  class clsTestData_Asset {

	public static Integer iRecord_Asset = 1;
    public static Asset oMain_Asset;
    public static List<Asset> lstAsset = new List<Asset>();

    public static Integer iRecord_AssetContract = 1;
    public static AssetContract__c oMain_AssetContract;
    public static List<AssetContract__c> lstAssetContract = new List<AssetContract__c>();


    //---------------------------------------------------------------------------------------------------
    // Create Asset Data
    //---------------------------------------------------------------------------------------------------
    public static List<Asset> createAsset(){
    	return createAsset(true);
    }
    public static List<Asset> createAsset(Boolean bInsert){

        if (oMain_Asset == null){

            if (clsTestData_Product.oMain_Product == null){
            	clsTestData_Product.iRecord_Product = iRecord_Asset;
            	clsTestData_Product.createProduct();
            }
            if (clsTestData_Account.oMain_Account == null) clsTestData_Account.createAccount(true);

            lstAsset = new List<Asset>();

            Integer iCounter = 0;
            for (Product2 oProduct : clsTestData_Product.lstProduct){
                Asset oAsset = new Asset();
                    oAsset.Product2Id = oProduct.Id; 
                    oAsset.Quantity = 10; 
                    oAsset.Name = 'TEST Asset ' + iCounter;
                    oAsset.AccountId = clsTestData_Account.oMain_Account.Id;
                    oAsset.Asset_Type_Picklist__c = String.valueOf(iCounter);
                    oAsset.Serial_Nr__c = clsUtil.getNewGUID() + '_' + String.valueOf(iCounter);

                lstAsset.add(oAsset);
                iCounter++;
            }
            if (bInsert) insert lstAsset; 

            oMain_Asset = lstAsset[0];           
        }

        return lstAsset;
    } 	
    //---------------------------------------------------------------------------------------------------


    //---------------------------------------------------------------------------------------------------
    // Create Asset Contract Data
    //---------------------------------------------------------------------------------------------------
    public static List<AssetContract__c> createAssetContract(){
        return createAssetContract(true);
    }
    public static List<AssetContract__c> createAssetContract(Boolean bInsert){

        if (oMain_AssetContract == null){

            if (oMain_Asset == null){
                iRecord_Asset = iRecord_AssetContract;
                createAsset();
            }else if (lstAsset.size() < iRecord_AssetContract){
                oMain_Asset = null;
                iRecord_Asset = iRecord_AssetContract - lstAsset.size();
                createAsset();
            }

            if (clsTestData_Contract.oMain_Contract == null){
                clsTestData_Contract.iRecord_Contract = iRecord_AssetContract;
                clsTestData_Contract.createContract();
            }else if (clsTestData_Contract.lstContract.size() < iRecord_AssetContract){
                clsTestData_Contract.oMain_Contract = null;
                clsTestData_Contract.iRecord_Contract = iRecord_AssetContract - clsTestData_Contract.lstContract.size();
                clsTestData_Contract.createContract();
            }

            lstAssetContract = new List<AssetContract__c>();
            for (Integer i=0; i<iRecord_AssetContract; i++){
                AssetContract__c oAssetContract = new AssetContract__c();
                    oAssetContract.Asset__c = lstAsset[i].Id;
                    oAssetContract.Contract__c = clsTestData_Contract.lstContract[i].Id;
                lstAssetContract.add(oAssetContract);
            }
            if (bInsert) insert lstAssetContract; 

            oMain_AssetContract = lstAssetContract[0];           
        }

        return lstAssetContract;

    } 
    //---------------------------------------------------------------------------------------------------

}