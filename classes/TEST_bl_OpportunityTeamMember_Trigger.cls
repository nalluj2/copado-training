//--------------------------------------------------------------------------------------------------------------------------------
//  Author      :   Bart Caelen
//  Date        :   29/05/2017
//  Description :   APEX TEST Class for the APEX Trigger tr_OpportunityTeamMember and APEX Class bl_OpportunityTeamMember_Trigger
//  Change Log  :    
//--------------------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_bl_OpportunityTeamMember_Trigger {
	
	private static Id id_Opportunity;
	private static List<OpportunityTeamMember> lstOpportunityTeamMember;
	private static Boolean bInsertOpportunityTeamMember = true;
	private static Integer iRecord_OpportunityTeamMember = 2;
	private static String tRecordTypeDevName = 'EMEA_IHS_Managed_Services';

	@isTest static void createTestData(){

		clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', tRecordTypeDevName).Id;
		clsTestData_Opportunity.createOpportunity(true);
		id_Opportunity = clsTestData_Opportunity.oMain_Opportunity.Id;

		clsTestData_Opportunity.iRecord_OpportunityTeamMember = iRecord_OpportunityTeamMember;
		lstOpportunityTeamMember = clsTestData_Opportunity.createOpportunityTeamMember(bInsertOpportunityTeamMember);

	}

	@isTest static void test_preventDeleteOppOwner(){

		//----------------------------------------------------
		// Create Test Data
		//----------------------------------------------------
		tRecordTypeDevName = 'EUR_MITG_Standard_Opportunity';
		iRecord_OpportunityTeamMember = 2;
		bInsertOpportunityTeamMember = true;
		createTestData();

		Opportunity oOpportunity = [SELECT Id, OwnerId, (SELECT Id, UserId, Is_Opportunity_Owner__c FROM OpportunityTeamMembers) FROM Opportunity WHERE Id = :id_Opportunity];
		System.assertEquals(oOpportunity.OpportunityTeamMembers.size(), 3);
		//----------------------------------------------------


		//----------------------------------------------------
		// Perform Tests
		//----------------------------------------------------
		Test.startTest();

		for (OpportunityTeamMember oOpportunityTeamMember : oOpportunity.OpportunityTeamMembers){

			String tError = '';
			Boolean bIsOpportunityOwner = oOpportunityTeamMember.Is_Opportunity_Owner__c;

			try{

				delete oOpportunityTeamMember;

			}catch(Exception oEX){

				tError = oEX.getMessage();
			}

			if (bIsOpportunityOwner){
				System.assert(tError.contains('You cannot delete this Opportunity Team Member because it\'s also the Owner of the Opportunity.'));
			}else{
				System.assert(String.isBlank(tError));
			}

		}

		Test.stopTest();
		//----------------------------------------------------


		//----------------------------------------------------
		// Validate Test Result
		//----------------------------------------------------
		oOpportunity = [SELECT Id, OwnerId, (SELECT Id, UserId, Is_Opportunity_Owner__c FROM OpportunityTeamMembers) FROM Opportunity WHERE Id = :id_Opportunity];
		System.assertEquals(oOpportunity.OpportunityTeamMembers.size(), 1);
		//----------------------------------------------------

	}
}
//--------------------------------------------------------------------------------------------------------------------------------