/**
 * The created class wil cover the campaignAfterInsertUpdate trigger
 * Creation Date :  20120328
 * Description :    Test coverage for Trigger campaignAfterInsertUpdate
 * 
 * Author :         Shweta/WIpro
 */
 
@isTest
private class TEST_campaignAfterInsertUpdate
{
    static testMethod void myUnitTest() 
    {

     Company__c cmpny = new Company__c();
     cmpny.name='Europe';
     cmpny.CurrencyIsoCode = 'EUR';
     cmpny.Current_day_in_Q1__c=56;
     cmpny.Current_day_in_Q2__c=34; 
     cmpny.Current_day_in_Q3__c=5; 
     cmpny.Current_day_in_Q4__c= 0;   
     cmpny.Current_day_in_year__c =200;
     cmpny.Days_in_Q1__c=56;  
     cmpny.Days_in_Q2__c=34;
     cmpny.Days_in_Q3__c=13;
     cmpny.Days_in_Q4__c=22;
     cmpny.Days_in_year__c =250;
     cmpny.Company_Code_Text__c = 'T33';
     insert cmpny;

     //Insert Business Unit
     Business_Unit__c bu =  new Business_Unit__c();
     bu.Company__c =cmpny.id;
     bu.name='CRHF';
     bu.Account_Plan_Activities__c=true;
     insert bu;
   
    //Insert SBU
    Sub_Business_Units__c sbu = new Sub_Business_Units__c();
    sbu.name='AF Solutions';
    sbu.Business_Unit__c=bu.id;
    sbu.Account_Plan_Default__c='Revenue';
    sbu.Account_Flag__c = 'AF_Solutions__c';
    insert sbu;


    campaign cmp = new campaign();
    cmp.Business_Unit__c = bu.id;
    cmp.name='Campaign1';
    cmp.sBU_vs__c = 'AF Solutions';
    cmp.Business_Unit_Picklist__c = 'CRHF';
    cmp.recordtypeid=clsUtil.getRecordTypeByDevName('Campaign', 'CVG_Campaign').Id;
    insert cmp;


    }

 }