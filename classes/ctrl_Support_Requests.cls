public without sharing class ctrl_Support_Requests {
    
    public class SessionData{
        
        public String username {get; set;}
        public String password {get; set;}  
        public Id internalUserId {get; private set;}
        public ws_adUserService.userinfo uInfo {get; private set;}
        public Set<String> managedApplications {get; set;}
        public Id id_Company { get;set; }
        
        public Boolean changeRequestAllowed {get; set;}
        public Boolean isAppApprover {get; set;}
           
        public Create_User_Request__c request {get; set;}
                
        public Id inputRequestId {get; set;}
        public String attachmentId {get; set;}
        
        // Request Comments 
        public User_Request_Comment__c comment {get; set;}
        public List<User_Request_Comment__c> comments {get; set;}
              
        // Request Requests 
        public Attachment inputFile {get; set;}
        public Attachment inputFile2 {get; set;}
        public Attachment inputFile3 {get; set;}
        public List<Attachment> readOnlyAttachments {get; set;}
        
        
        public void resetRequest()
        {
            request = new Create_User_Request__c();
            request.OwnerId = internalUserId;
        }
        
        public void getRequestComments()
        {
            comments = [Select Id, Comment__c, From__c, From_Email__c, CreatedDate from User_Request_Comment__c where User_Request__c=:request.Id ORDER BY CreatedDate DESC];        
        }
        
        public Boolean saveCommentAndAttachment(){
        
            SavePoint sp = Database.setSavepoint();
            
            try{
                
                if (Test.isRunningTest()) clsUtil.bubbleException('ctrl_Support_Requests.saveCommentAndAttachment_EXCEPTION');

                if(comment!=null && comment.Comment__c!=null && comment.Comment__c!='')
                {                                
                    comment.User_Request__c = request.Id;                
                    comment.From__c = uInfo.firstName + ' ' + uInfo.lastName;   
                    comment.From_Email__c = uInfo.email;            
                    insert comment;                                
                }
            
                if(inputFile!=null && inputFile.Name!=null)
                {
                    inputFile.ParentId = request.Id;
                    inputFile.IsPrivate = false;        
                    insert inputFile;
                    
                    inputFile.Body = null;
                }
                
                if(comment!=null && comment.Comment__c!=null && comment.Comment__c!='')
                {                
                    comment = new User_Request_Comment__c();
                    getRequestComments();       
                }
                
                if(inputFile!=null && inputFile.Name!=null) readOnlyAttachments.add(inputFile);
            
            }catch(Exception e)
            {
                ApexPages.addMessages(e);
                Database.rollback(sp);
            
                if(Test.isRunningTest()) throw e;           
            
                return false;
            
            }finally
            {
                inputFile = new Attachment();           
            }
            
            return true;    
        }  
        
        public void saveAttachments(Id requestId){
            
            if(inputFile.Name!=null){
                inputFile.ParentId = requestId;
                inputFile.IsPrivate = false;        
                insert inputFile;
                
                inputFile.Body = null;
            }
            
            if(inputFile2.Name!=null){
                inputFile2.ParentId = requestId;
                inputFile2.IsPrivate = false;       
                insert inputFile2;
                
                inputFile2.Body = null;
            }
            
            if(inputFile3.Name!=null){
                inputFile3.ParentId = requestId;
                inputFile3.IsPrivate = false;       
                insert inputFile3;
                
                inputFile3.Body = null;
            }
            
            if(inputFile.Name!=null) readOnlyAttachments.add(inputFile);
            if(inputFile2.Name!=null) readOnlyAttachments.add(inputFile2);
            if(inputFile3.Name!=null) readOnlyAttachments.add(inputFile3);
        }
        
        public void clearAttachments(){
        
            inputFile = new Attachment(); 
            inputFile2 = new Attachment();
            inputFile3 = new Attachment();    
        } 
        
        public void deleteAttachment(){
        
            if(attachmentId!=null){
                
                Database.delete((Id) attachmentId, true);            
                readOnlyAttachments = [Select Id, Name, CreatedDate from Attachment where ParentId=:request.Id ORDER BY CreatedDate ASC];
            }       
        } 
    }
    
    public SessionData session {get; set;}
    public Boolean userLoggedIn {get; set;}
    
    public String selectedItem {get; set;}
        
    //Request Tables
    public List<Request> requests {get; set;}
    public List<Request> pendingRequests {get; set;}
    public Date fromDate {get; set;}
    public Date toDate {get; set;}
    
    //HELPERS            
    public bl_Support_Create_User_SFDC helper_sfdcNewUser {get; set;}
    public bl_Support_Manage_User_SFDC helper_sfdcManageUSer {get; set;}
    public bl_Support_Requests_SFDC helper_sfdcRequests {get; set;}
    public bl_Support_CVENT_Requests helper_cventRequests {get; set;}
    public bl_SupportRequest_BI helper_Request_BI { get;set; }
    public bl_SupportRequest_XW helper_Request_XW { get;set; }
    public bl_SupportRequest_BOT helper_Request_BOT { get;set; }
            
    public ctrl_Support_Requests(){
              
        session = new SessionData();
                
        //Helper classes to hold specific variables and codes for each applicaton / request type        
        helper_sfdcNewUser = new bl_Support_Create_User_SFDC(session);
        helper_sfdcManageUser = new bl_Support_Manage_User_SFDC(session);
        helper_sfdcRequests = new bl_Support_Requests_SFDC(session);
        helper_cventRequests = new bl_Support_CVENT_Requests(session);
        helper_Request_BI = new bl_SupportRequest_BI(session);
        helper_Request_XW = new bl_SupportRequest_XW(session);
        helper_Request_BOT = new bl_SupportRequest_BOT(session);
         
        session.inputRequestId = ApexPages.currentPage().getParameters().get('requestId');
                                
        if(UserInfo.getUserType() == 'Guest')
        {
            userLoggedIn=false;
            
        }else
        {   
            loginInternalUser();
   
            if(session.inputRequestId!=null)
            {                
                loadInputRequest();
                
            }else
            {                
                loadOpenRequests(); 
                loadPendingRequests();
            }       
        }               
    }
        
    private void loginInternalUser(){
        
        User currentUser = [Select Alias, Email, CostCenter__c, Peoplesoft_Id__c, Manager.Email, Manager.Name from User where Id=:UserInfo.getUserId()];
        session.uInfo = new ws_adUserService.userinfo();
        session.uInfo.email = currentUser.Email;
        session.uInfo.firstName = UserInfo.getFirstName();
        session.uInfo.lastName = UserInfo.getLastName();
        session.uInfo.costcenter = currentUser.CostCenter__c;
        session.uInfo.peopleSoft = currentUser.Peoplesoft_ID__c;
        session.username = currentUser.alias;
        session.internalUserId = UserInfo.getUserId();
        
        if(currentUser.Manager!=null){
            session.uInfo.managerEmail = currentUser.Manager.Email;
            session.uInfo.managerName = currentUser.Manager.Name;   
        }
        
        getManagedApplicationsByUser();     
        getSecurityValues();
        
        userLoggedIn=true;
    }
    
    public PageReference doLogin(){
            
        ws_adUserService.Service1Soap userService = new ws_adUserService.Service1Soap();
        userService.timeout_x=120000;
        String encodedPassword = EncodingUtil.base64Encode(Blob.valueOf(session.password));
                        
        String response;
        
        try{
            response = userService.valUser(session.username, encodedPassword);
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Service Unavailable: '+e.getMessage()));
            return null;
        } 
        
        if(response != 'OK'){
            
            userLoggedIn = false;
            String errorDesc='Login failed. '+response+': ';
            
            if(response=='EX E003') errorDesc+='Unknown user name or bad password.';
            else if(response=='EX E017') errorDesc+='Invalid account.';
            else if(response=='EX E011') errorDesc+='Account disabled.';
            else if(response=='EX E016') errorDesc+='Account lock out.';
            else if(response=='EX E000') errorDesc+='Service unavailable.';
            else errorDesc+='Unknown error.';
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, errorDesc));
            return null;
        }
        
        userLoggedIn = true;
        
        session.uInfo = userService.getUserInfo(session.username);
        
        //Try to match the logged in User to a internal SFDC User
        List<User> internalUser = [Select Id from User where isActive = true AND Alias =: session.username AND UserType != 'CSNOnly' ORDER BY CreatedDate ASC LIMIT 1];
        
        if(internalUser.size()>0)
        {    
            session.internalUserId = internalUser[0].Id;
            getSecurityValues();
            
        }else
        {
            //If no user is found we user the Portal Guest User
            session.internalUserId = UserInfo.getUserId();
            session.changeRequestAllowed = false;
        }
        
        getManagedApplicationsByUser();
        
        if(session.inputRequestId==null)
        {
            loadOpenRequests();
            loadPendingRequests();
            return Page.Support_Requests;
        }
                
        return loadInputRequest();                          
    }
    
    private void getSecurityValues(){
        
        List<Mobile_App_Assignment__c> assignments = [Select Id from Mobile_App_Assignment__c where Mobile_App__r.Name = 'Advanced Support Portal User' AND User__c =: session.internalUserId AND Active__c = true];
        
        if(assignments.size()>0){
            session.changeRequestAllowed = true;
        }else{          
            session.changeRequestAllowed = false;
        }
    }
    
    private void getManagedApplicationsByUser(){
        
        session.managedApplications = new Set<String>();
        
        Map<String, Support_application__c> config = Support_application__c.getAll();
        
        for(String app : config.keySet()){
            
            Support_Application__c appConfig = config.get(app);
            
            Set<String> appEmails = new Set<String>(appConfig.Emails_of_approvers__c.split(';'));
            if(appEmails.contains(session.uInfo.Email)) session.managedApplications.add(app);           
        }
    }
    
    private PageReference loadInputRequest(){
        
        Create_User_Request__c oSupportRequest = bl_SupportRequest.loadSupportRequestDetailData(session.inputRequestId);     
        if (oSupportRequest == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Invalid Request Id'));
            return null;
        }
        session.request = oSupportRequest;
        session.getRequestComments();
        session.comment = new User_Request_Comment__c();
            
        session.readOnlyAttachments = oSupportRequest.attachments;
        session.inputFile = new Attachment();
        
        if (session.request.Application__c == 'Salesforce.com'){   

            if (session.request.Request_Type__c == 'Create User') return helper_sfdcNewUser.initializeInputRequestId();               
            else if (session.request.Request_Type__c == 'Data Load') return helper_sfdcRequests.initializeInputRequestId();
            else if (session.request.Request_Type__c == 'Change User Setup') return Page.Support_Change_User_Setup;
            else if (session.request.Request_Type__c == 'Reset Password') return Page.Support_Reset_Password;   
            else if (session.request.Request_Type__c == 'Enable Mobile App')    return Page.Support_Enable_App;
            else if (session.request.Request_Type__c == 'De-activate User') return Page.Support_Deactivate_User;
            else if (session.request.Request_Type__c == 'Re-activate User') return Page.Support_Reactivate_User;
            else if (session.request.Request_Type__c == 'Bug') return Page.Support_Request_Bug;          
            else if (session.request.Request_Type__c == 'Generic Service') return Page.Support_Other_Request;
            else if (session.request.Request_Type__c == 'Change Request') return Page.Support_Change_Request;             
        
        }else if (session.request.Application__c == 'CVent'){
        
            if (session.request.Request_Type__c == 'Change Request') return Page.Support_CVent_Change_Request;
            else if (session.request.Request_Type__c == 'Generic Service') return Page.Support_CVent_Generic_Service;

        }else if (session.request.Application__c == 'BOT'){

            if (session.request.Request_Type__c == 'Create User') return Page.Support_BOT_CreateUser;
            else if (session.request.Request_Type__c == 'Data Extract') return Page.Support_BOT_DataExtract;
            else if (session.request.Request_Type__c == 'Generic Service') return Page.Support_BOT_GeneralRequest;
        
        }else if(session.request.Application__c == helper_Request_BI.tApplicationName){
        
            if (session.request.Request_Type__c == 'Create User') return Page.Support_BI_CreateUser;
            else if (session.request.Request_Type__c == 'Change User Setup') return Page.Support_BI_ChangeUserSetup;
            else if (session.request.Request_Type__c == 'De-activate User') return Page.Support_BI_DeactivateUser;
            else if (session.request.Request_Type__c == 'Re-activate User') return Page.Support_BI_ReactivateUser;
            else if (session.request.Request_Type__c == 'Generic Service') return Page.Support_BI_GenericService;
        
        }else if (session.request.Application__c == 'Crossworld'){
        
            if (session.request.Request_Type__c == 'Bug') return Page.Support_XW_Bug;      
            else if (session.request.Request_Type__c == 'Enable Mobile App')    return Page.Support_XW_Enable_App;      
            else if (session.request.Request_Type__c == 'Change Request') return Page.Support_XW_Change_Request;
            else if (session.request.Request_Type__c == 'Generic Service') return Page.Support_XW_Generic_Service;
        
        }
        
        return null;    
    }
            
    public PageReference goBack(){
        
        loadOpenRequests();
        loadPendingRequests();
        return Page.Support_Requests;       
    }
                   
    public void updateRequest(){
        
        if(session.saveCommentAndAttachment()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request updated successfully.'));
        }       
    }

    public void approve(){
        
        String prevStatus = session.request.Status__c;
        
        try{

            if (Test.isRunningTest()) clsUtil.bubbleException('ctrl_Support_Requests.approve_EXCEPTION');

            session.request.status__c='Approved';
            session.request.Approved_Rejected_By__c = session.uInfo.firstName+' '+session.uInfo.lastName;
            update session.request;
            
            session.saveCommentAndAttachment();
            
        }catch(Exception e){
            
            ApexPages.addMessages(e);
            session.request.status__c=prevStatus;
            
            if(Test.isRunningTest()) throw e;
            
            return;
        }
                
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request Approved successfully.'));      
    }
        
    public void reject(){
        
        String prevStatus = session.request.Status__c;
        
        try{

            if (Test.isRunningTest()) clsUtil.bubbleException('ctrl_Support_Requests.reject_EXCEPTION');

            session.request.status__c='Rejected';
            session.request.Approved_Rejected_By__c = session.uInfo.firstName+' '+session.uInfo.lastName;
            update session.request;
            
            session.saveCommentAndAttachment();
            
        }catch(Exception e)
        {    
            ApexPages.addMessages(e);
            session.request.status__c=prevStatus;
            
            if(Test.isRunningTest()) throw e;
            
            return;
        }
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.info, 'Request Rejected successfully.'));     
    }
    
    public void cancel(){
        
        String prevStatus = session.request.Status__c;
        
        try{                       

            if (Test.isRunningTest()) clsUtil.bubbleException('ctrl_Support_Requests.cancel_EXCEPTION');

            session.request.status__c='Cancelled';
            update session.request;
            
            session.saveCommentAndAttachment();
            
        }catch(Exception e){
            ApexPages.addMessages(e);
            session.request.status__c=prevStatus;
            if(Test.isRunningTest()) throw e;
            return;
        }
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.info, 'Request Cancelled successfully.'));        
    }
    
    /*
        Display Request Table
    */
    
    public String openReqSortColumn {get; set;}
    public String openReqSortDir {get; set;}
    public String selectedOpenReqColumn {get; set;}
    
    public String pendingReqSortColumn {get; set;}
    public String pendingReqSortDir {get; set;}
    public String selectedPendingReqColumn {get; set;}
    
    public static String sortField = 'CreatedDate';
    public static String sortDir = 'ASC'; 
    
    public void sortOpenRequests(){
        
        if(selectedOpenReqColumn == openReqSortColumn){
            
            if(openReqSortDir == 'ASC') openReqSortDir = 'DES';
            else openReqSortDir = 'ASC';
            
        }else{
            openReqSortDir = 'ASC';
        }
        
        openReqSortColumn = selectedOpenReqColumn;
        
        sortField = openReqSortColumn;
        sortDir = openReqSortDir;
        
        requests.sort();
    }   
    
    public void sortPendingRequests(){
        
        if(selectedPendingReqColumn == pendingReqSortColumn){
            
            if(pendingReqSortDir == 'ASC') pendingReqSortDir = 'DES';
            else pendingReqSortDir = 'ASC';
            
        }else{
            pendingReqSortDir = 'ASC';
        }
        
        pendingReqSortColumn = selectedPendingReqColumn;
        
        sortField = pendingReqSortColumn;
        sortDir = pendingReqSortDir;
        
        pendingRequests.sort();
    }   
    
    public PageReference seeAllRequests(){
                        
        toDate = Date.today();
        fromDate = toDate.addMonths(-1);
        
        loadAllRequests();
        
        return Page.Support_My_Requests;
    }
    
    public PageReference viewRequest(){
        
        session.inputRequestId = selectedItem;      
        return loadInputRequest();
    }
    
    public void loadAllRequests(){
        
        if(openReqSortColumn == null){
            openReqSortColumn = 'CreatedDate';
            openReqSortDir = 'ASC';
        }
        
        requests = new List<Request>();
        
        DateTime fromDatetime;
        if(fromDate!=null) fromDateTime = DateTime.newInstance(fromDate, Time.newInstance(0, 0, 0, 0));
        
        DateTime toDatetime;
        if(toDate!=null) toDateTime = DateTime.newInstance(toDate,  Time.newInstance(23, 59, 59, 0));

        String tWhere = 'Requestor_Email__c = \'' + session.uInfo.email + '\'';
        tWhere += ' AND CreatedDate >= ' +  fromDatetime.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        tWhere += ' AND CreatedDate <= ' + toDatetime.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        List<Create_User_Request__c> lstUserRequest = bl_SupportRequest.loadSupportRequestOverviewData(tWhere);

        requests = getRequest(lstUserRequest);

        sortField = openReqSortColumn;
        sortDir = openReqSortDir;       
        requests.sort();
    }
    
    @TestVisible private void loadOpenRequests(){
        
        if(openReqSortColumn == null){
            openReqSortColumn = 'CreatedDate';
            openReqSortDir = 'ASC';
        }
        
        requests = new List<Request>();

        String tWhere = 'Requestor_Email__c = \'' + session.uInfo.email + '\'';
        tWhere += ' AND Status__c IN (\'New\',\'Submitted for Approval\',\'Approved\',\'Scheduled\',\'In Progress\')';
        List<Create_User_Request__c> lstUserRequest = bl_SupportRequest.loadSupportRequestOverviewData(tWhere);

        requests = getRequest(lstUserRequest);

        sortField = openReqSortColumn;
        sortDir = openReqSortDir;
        requests.sort();
    }
    
    @TestVisible private void loadPendingRequests(){
        
        if(pendingReqSortColumn == null){
            pendingReqSortColumn = 'CreatedDate';
            pendingReqSortDir = 'ASC';
        }
        
        pendingRequests = new List<Request>();
        
        List<Create_User_Request__c> pendingRequestsTemp = new List<Create_User_Request__c>();

        List<Create_User_Request__c> managerApprover = [Select Id from Create_User_Request__c 
                                                            where Status__c ='Submitted for Approval' AND Manager_Email__c =:session.uInfo.email AND Application__c='Salesforce.com']; 

        if(managerApprover.size()>0) pendingRequestsTemp.addAll(managerApprover);
        
        if(session.managedApplications.size()>0){
        
            List<Create_User_Request__c> appApprover = [Select Id from Create_User_Request__c 
                                                            where Status__c ='Submitted for Approval' AND Application__c IN: session.managedApplications];
            
            if(appApprover.size()>0) pendingRequestsTemp.addAll(appApprover);                                               
        }
        
        if(session.managedApplications.contains('Data Load Request')){
            
            String userEmail = '%' + session.uInfo.email + '%';
            
            List<Create_User_Request__c> dataLoadApprover = [Select Id from Create_User_Request__c 
                                                            where Status__c ='Submitted for Approval' AND Request_Type__c = 'Data Load' AND Data_Load_Approvers__c LIKE: userEmail ];
            
            if(dataLoadApprover.size()>0) pendingRequestsTemp.addAll(dataLoadApprover);     
        }                       
        
        if(pendingRequestsTemp.size()>0){
                      
            String tWhere_ID = '';
            for (Create_User_Request__c oUserRequest_Pending : pendingRequestsTemp){
                tWhere_ID += '\'' + oUserRequest_Pending.Id + '\',';
            }
            tWhere_ID = tWhere_ID.left(tWhere_ID.length()-1);

            String tWhere = 'Id in (' + tWhere_ID + ')';
            List<Create_User_Request__c> lstUserRequest = bl_SupportRequest.loadSupportRequestOverviewData(tWhere);

            pendingRequests = getRequest(lstUserRequest);
        }
        
        sortField = pendingReqSortColumn;
        sortDir = pendingReqSortDir;
        
        pendingRequests.sort();
    }

    @TestVisible private List<Request> getRequest(List<Create_User_Request__c> lstUserRequest){
        List<Request> lstReuest = new List<Request>();

        for(Create_User_Request__c userRequest : lstUserRequest){

            Request req = new Request(userRequest.CreatedDate, userRequest.Completed_Date__c);
            req.Id = userRequest.Id;
            req.Name = userRequest.Name ;
            req.status = userRequest.Status__c ;
            req.reqType = userRequest.Request_Type__c ;
            req.Application = userRequest.Application__c == 'BI' ? 'Data Services' : userRequest.Application__c; //Dirty workaround to avoid having to change the picklist value from BI to Data Services

            if (userRequest.Application__c == 'Salesforce.com'){

                if(userRequest.Request_Type__c == 'Data Load' ) req.Details = userRequest.Data_Type__c;
                else if(userRequest.Request_Type__c == 'Generic Service' ) req.Details = userRequest.Service_Request_Type__c;
                else if(userRequest.Request_Type__c == 'Change Request' ) req.Details = userRequest.CR_Type__c + ' - ' +userRequest.Priority__c;
                else if(userRequest.Request_Type__c == 'Create User' ) req.Details = userRequest.Firstname__c + ' ' + userRequest.Lastname__c;
                else if(userRequest.Request_Type__c == 'Enable Mobile App' ) req.Details = userRequest.Created_User__r.Name + ' - ' + userRequest.Mobile_App__c;
                else if(userRequest.Request_Type__c == 'De-activate User' ){ 
                    
                    if( userRequest.Created_User__c != null) req.Details = userRequest.Created_User__r.Name;
                    else req.Details = userRequest.Firstname__c + ' ' + userRequest.Lastname__c;
                    
                    req.Details += ' - ' + userRequest.Date_of_deactivation__c.format('YYYY-MMM-dd HH:mm z');
                }               
                else req.Details = userRequest.Created_User__r!=null? userRequest.Created_User__r.Name : '';

            }else if (userRequest.Application__c == 'CVent'){

                if(userRequest.Request_Type__c == 'Change Request' ) req.Details = userRequest.Area_CR__c + ' - ' + userRequest.Priority__c;
                else if(userRequest.Request_Type__c == 'Generic Service' ) req.Details = userRequest.Area_SR__c;
                else req.Details = userRequest.Created_User__r!=null? userRequest.Created_User__r.Name : '';

            }else if (userRequest.Application__c == 'BI'){

                if(userRequest.Request_Type__c == 'Create User' ) req.Details = userRequest.Firstname__c + ' ' + userRequest.Lastname__c;
                else if(userRequest.Request_Type__c == 'Change User Setup' ) req.Details = userRequest.Firstname__c + ' ' + userRequest.Lastname__c;
                else if(userRequest.Request_Type__c == 'Generic Service' ) req.Details = userRequest.Area_SR__c;
                else if(userRequest.Request_Type__c == 'Re-activate User' ) req.Details = userRequest.Firstname__c + ' ' + userRequest.Lastname__c;
                else if(userRequest.Request_Type__c == 'De-activate User' ){ 
                    
                    if( userRequest.Created_User__c != null) req.Details = userRequest.Created_User__r.Name;
                    else req.Details = userRequest.Firstname__c + ' ' + userRequest.Lastname__c;
                    
                    req.Details += ' - ' + userRequest.Date_of_deactivation__c.format('YYYY-MMM-dd HH:mm z');
                }               
                else req.Details = userRequest.Created_User__r!=null? userRequest.Created_User__r.Name : '';

            }else{

                if(userRequest.Request_Type__c == 'Data Load' ) req.Details = userRequest.Data_Type__c;
                else if(userRequest.Request_Type__c == 'Generic Service' ) req.Details = userRequest.Service_Request_Type__c;
                else if(userRequest.Request_Type__c == 'Change Request' ) req.Details = userRequest.CR_Type__c + ' - ' +userRequest.Priority__c;
                else if(userRequest.Request_Type__c == 'Create User' ) req.Details = userRequest.Firstname__c + ' ' + userRequest.Lastname__c;
                else if(userRequest.Request_Type__c == 'Enable Mobile App' ) req.Details = userRequest.Created_User__r.Name + ' - ' + userRequest.Mobile_App__c;
                else if(userRequest.Request_Type__c == 'Re-activate User' ) req.Details = userRequest.Firstname__c + ' ' + userRequest.Lastname__c;
                else if(userRequest.Request_Type__c == 'Change User Setup' ) req.Details = userRequest.Firstname__c + ' ' + userRequest.Lastname__c;
                else if(userRequest.Request_Type__c == 'De-activate User' ){ 
                    
                    if( userRequest.Created_User__c != null) req.Details = userRequest.Created_User__r.Name;
                    else req.Details = userRequest.Firstname__c + ' ' + userRequest.Lastname__c;
                    
                    req.Details += ' - ' + userRequest.Date_of_deactivation__c.format('YYYY-MMM-dd HH:mm z');
                }               
                else req.Details = userRequest.Created_User__r!=null? userRequest.Created_User__r.Name : '';
            }
                                    
                        
            lstReuest.add(req);      
        }    

        return lstReuest;    
    }
            
    public class Request implements Comparable{
        
        public Id id {get; set;}
        public String name {get; set;}
        public String status {get; set;}
        public String reqType {get; set;}
        public String details {get; set;}
        public String requestor {get; set;}
        public String application {get; set;}
        private DateTime createdDate;
        private DateTime completedDate; 
                        
        public Request(DateTime created, DateTime completed){
            createdDate = created;
            completedDate = completed;
        }
        
        public String getCreatedDate(){
            
            if(UserInfo.getUserType() == 'Guest'){
                return createdDate.format('YYYY-MMM-dd HH:mm z') ;
            }
            
            return createdDate.format();
        }
        
        public String getCompletedDate(){
            
            if(completedDate == null) return '';
            
            if(UserInfo.getUserType() == 'Guest'){
                return completedDate.format('YYYY-MMM-dd HH:mm z') ;
            }
            
            return completedDate.format();
        }
        
        public Integer compareTo(Object compareTo) {
            
            Request other = (Request) compareTo;
            sortField = ctrl_Support_Requests.sortField;
            sortDir = ctrl_Support_Requests.sortDir;
            
            if(sortField == 'CreatedDate'){
                            
                if(this.createdDate > other.createdDate) {
                    if(sortDir == 'ASC') return 1;
                    else return -1;
                }
                                
                if(this.createdDate < other.createdDate) {
                    if(sortDir == 'ASC') return -1;
                    else return 1;
                }             
                
            }else if(sortField == 'CompletedDate'){
                
                if(this.completedDate != null || other.completedDate != null){
                                                                
                    if((this.completedDate != null && other.completedDate == null) || (this.completedDate > other.completedDate)) {
                        if(sortDir == 'ASC') return 1;
                        else return -1;
                    }
                                    
                    if((this.completedDate == null && other.completedDate != null) || (this.completedDate < other.completedDate)) {
                        if(sortDir == 'ASC') return -1;
                        else return 1;
                    }
                }
                                
            }else if(sortField == 'RequestType'){
                            
                if(this.reqType > other.reqType) {
                    if(sortDir == 'ASC') return 1;
                    else return -1;
                }
                                
                if(this.reqType < other.reqType) {
                    if(sortDir == 'ASC') return -1;
                    else return 1;
                }               
            }else if(sortField == 'Status'){
                            
                if(this.status > other.status) {
                    if(sortDir == 'ASC') return 1;
                    else return -1;
                }
                                
                if(this.status < other.status) {
                    if(sortDir == 'ASC') return -1;
                    else return 1;
                }               
            }else if(sortField == 'RequestId'){
                            
                if(this.name > other.name) {
                    if(sortDir == 'ASC') return 1;
                    else return -1;
                }
                                
                if(this.name < other.name) {
                    if(sortDir == 'ASC') return -1;
                    else return 1;
                }               
            }else if(sortField == 'Requestor'){
                            
                if(this.requestor > other.requestor) {
                    if(sortDir == 'ASC') return 1;
                    else return -1;
                }
                                
                if(this.requestor < other.requestor) {
                    if(sortDir == 'ASC') return -1;
                    else return 1;
                }               
            }else if(sortField == 'Application'){
                            
                if(this.application > other.application) {
                    if(sortDir == 'ASC') return 1;
                    else return -1;
                }
                                
                if(this.application < other.application) {
                    if(sortDir == 'ASC') return -1;
                    else return 1;
                }               
            }

            if(this.createdDate > other.createdDate) return 1;
            if(this.createdDate < other.createdDate) return -1;
            
            return 0;
        }   
    }

}