//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 26-10-2018
//  Description      : APEX Class - Business Logic for tr_Contract
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public class bl_Contract_Trigger {
	
	//----------------------------------------------------------------------------------------------------------------
	// Logic for RTG_Service_Contracts
	//	- add owner as chatter follower when the SAP Contract Number is populated / changed
	//	- create chatter post when one of the following fields are changed:
	//		- SAP Contract Number
	//		- SAP Contract Approval (different post for Yes and No)
	//----------------------------------------------------------------------------------------------------------------
	public static void createChatterPost(List<Contract> lstTriggerNew, Map<Id, Contract> mapTriggerOld){

	    if (bl_Trigger_Deactivation.isTriggerDeactivated('Contract_createChatterPost')) return;
	
		Set<Id> setID_RecordType = new Set<Id>{clsUtil.getRecordTypeByDevName('Contract', 'RTG_Service_Contract').Id};

		List<EntitySubscription> lstEntitySubscription = new List<EntitySubscription>();
		List<ConnectApi.BatchInput> lstBatchInput = new List<ConnectApi.BatchInput>();
		List<FeedItem> lstFeedItem = new List<FeedItem>();

		
		// Get the Followers of the Contracts in case of an Update
		Map<Id, Set<Id>> mapContractId_UserId = new Map<Id, Set<Id>>();
		if (mapTriggerOld != null){

			Set<Id> setID_Contract = new Set<Id>();

			for (Contract oContract : lstTriggerNew){

				setID_Contract.add(oContract.Id);

			}

			List<EntitySubscription> lstEntitySubscription_Existing = [SELECT ParentId, SubscriberId FROM EntitySubscription WHERE ParentId = :setID_Contract ORDER BY ParentId LIMIT 1000];
			for (EntitySubscription oEntitySubscription : lstEntitySubscription_Existing){

				Set<Id> setID_User = new Set<Id>();
				if (mapContractId_UserId.containsKey(oEntitySubscription.ParentId)){
					setID_User = mapContractId_UserId.get(oEntitySubscription.ParentId);
				}
				setID_User.add(oEntitySubscription.SubscriberId);
				mapContractId_UserId.put(oEntitySubscription.ParentId, setID_User);
			}

		}

		for (Contract oContract : lstTriggerNew){

			if (!setID_RecordType.contains(oContract.RecordTypeId)) continue;

			Contract oContract_Old = mapTriggerOld.get(oContract.Id);

			// Logic when SAP_Contract_Number__c is populated / changed
			if (
				(oContract.SAP_Contract_Number__c != oContract_Old.SAP_Contract_Number__c)
				&& 	(oContract.SAP_Contract_Number__c != null)
			){

				// Add the owner of the Contract as a follower of the record (= Inside Sales)
                if (mapContractId_UserId.containsKey(oContract.Id)){
	                if (!mapContractId_UserId.get(oContract.Id).contains(oContract.OwnerId)){
	                	lstEntitySubscription.add(new EntitySubscription(ParentId = oContract.Id, SubscriberId = oContract.OwnerId)); 
	                }
                }else{
	            	lstEntitySubscription.add(new EntitySubscription(ParentId = oContract.Id, SubscriberId = oContract.OwnerId)); 
                }


                // Create a Chatter Post that indicats that the SAP Contract Number is populated / Changed
                //	add the Contract Owner in the @Mention of the Chatter Post
                ConnectApi.FeedItemInput oFeedItemInput = bl_Chatter.createFeedItemInput(oContract.Id, oContract.OwnerId, '\nPlease check the SAP Contract "' + oContract.SAP_Contract_Number__c + '".');
				ConnectApi.BatchInput oBatchInput = new ConnectApi.BatchInput(oFeedItemInput);
				lstBatchInput.add(oBatchInput);

			}


			// Logic when SAP_Contract_Approved__c is populated / changed
			if (
				(oContract.SAP_Contract_Approved__c != oContract_Old.SAP_Contract_Approved__c)
			){

				if (oContract.SAP_Contract_Approved__c == 'Yes'){

	                // Create a Chatter Post that indicats that the SAP Contract Number is Approved
	    			FeedItem oFeedItem = new FeedItem();
						oFeedItem.ParentId = oContract.Id;
						oFeedItem.LinkUrl = '/' + oContract.Id;
						oFeedItem.Title = oContract.Name;
						oFeedItem.Body = 'The SAP Contract "' + oContract.SAP_Contract_Number__c + '" is approved and can be activated.';
					lstFeedItem.add(oFeedItem);

				}else if (oContract.SAP_Contract_Approved__c == 'No'){

	                // Create a Chatter Post that indicats that the SAP Contract Number is Disapproved
	    			FeedItem oFeedItem = new FeedItem();
						oFeedItem.ParentId = oContract.Id;
						oFeedItem.LinkUrl = '/' + oContract.Id;
						oFeedItem.Title = oContract.Name;
						oFeedItem.Body = 'The SAP Contract "' + oContract.SAP_Contract_Number__c + '" is disapproved.\n\n' + oContract.SAP_Contract_Comments__c;
					lstFeedItem.add(oFeedItem);

				}

			}

		}

		if (lstEntitySubscription.size() > 0) insert lstEntitySubscription;
		if (lstBatchInput.size() > 0) ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), lstBatchInput);
		if (lstFeedItem.size() > 0) insert lstFeedItem; 

	}

}
//--------------------------------------------------------------------------------------------------------------------