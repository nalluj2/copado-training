//  Change Log       : PMT-22787: MITG EMEA User Setup - Changed profile from EUR Field Force MITG to EMEA Field Force MITG
@isTest
private class Test_ws_ProceduralSolutionsKitService {
    
    private static testmethod void testProceduralSolutionKit(){
    	//PMT-22787: MITG EMEA User Setup - Changed profile from EUR Field Force MITG to EMEA Field Force MITG
    	User mitgUser = [Select Id from User where Profile.Name in ('EUR Field Force MITG', 'EMEA Field Force MITG') AND IsActive = true AND Id NOT IN (Select AssigneeId from PermissionSetASsignment where PermissionSet.Name = 'Box_Builder_Admin') Limit 1];
        //User mitgUser = [Select Id from User where Profile.Name = 'EMEA Field Force MITG' AND IsActive = true AND Id NOT IN (Select AssigneeId from PermissionSetASsignment where PermissionSet.Name = 'Box_Builder_Admin') Limit 1];
    	
    	System.runAs(mitgUser){
	    	
	    	//Create
	    	Procedural_Solutions_Kit__c box = new Procedural_Solutions_Kit__c();
	    	box.Mobile_ID__c = 'mobileId';
	    	
	    	ws_ProceduralSolutionKitService.ProceduralSolutionKitRequest request = new ws_ProceduralSolutionKitService.ProceduralSolutionKitRequest();
		 	request.ProceduralSolutionKit = box;
		 	request.id = 'testId';
		 		 	
	    	RestRequest req = new RestRequest();	 	
		 	req.requestbody = Blob.valueOf(JSON.serialize(request));
		 	
		 	RestContext.request = req;
		 	
		 	String resp = ws_ProceduralSolutionKitService.doPost();
		 	
		 	ws_ProceduralSolutionKitService.ProceduralSolutionKitResponse response = (ws_ProceduralSolutionKitService.ProceduralSolutionKitResponse) JSON.deserialize(resp, ws_ProceduralSolutionKitService.ProceduralSolutionKitResponse.class);
		 	
		 	System.assert(response.success == true);
		 	
		 	box = [Select Id, Mobile_Id__c from Procedural_Solutions_Kit__c where Mobile_Id__c = 'mobileId'];
			
			//Delete		 	
		 	ws_ProceduralSolutionKitService.ProceduralSolutionKitDeleteRequest requestDel = new ws_ProceduralSolutionKitService.ProceduralSolutionKitDeleteRequest();
	 		requestDel.ProceduralSolutionKit = box;
	 		requestDel.id = 'testId';
		 	
		 	RestRequest reqDel = new RestRequest();	 	
		 	reqDel.requestbody = Blob.valueOf(JSON.serialize(requestDel));
		 	
		 	RestContext.request = reqDel;
		 	
		 	resp = ws_ProceduralSolutionKitService.doDelete();
		 	
		 	ws_ProceduralSolutionKitService.ProceduralSolutionKitDeleteResponse responseDel = (ws_ProceduralSolutionKitService.ProceduralSolutionKitDeleteResponse) JSON.deserialize(resp, ws_ProceduralSolutionKitService.ProceduralSolutionKitDeleteResponse.class);
		 	
		 	System.assert(responseDel.success == true);
		 	
		 	List<Procedural_Solutions_Kit__c> boxes = [Select Id from Procedural_Solutions_Kit__c where Mobile_Id__c = 'mobileId'];
		 	
		 	System.assert(boxes.size() == 0);
		 	
		 	//Delete (record not found)
		 	resp = ws_ProceduralSolutionKitService.doDelete();
		 	
		 	responseDel = (ws_ProceduralSolutionKitService.ProceduralSolutionKitDeleteResponse) JSON.deserialize(resp, ws_ProceduralSolutionKitService.ProceduralSolutionKitDeleteResponse.class);
		 	
		 	System.assert(responseDel.success == true);
		 	System.assert(responseDel.message == 'ProceduralSolutionKit not found');
    	}
    }
    
    private static testmethod void testProceduralSolutionKitProduct(){
    	//PMT-22787: MITG EMEA User Setup - Changed profile from EUR Field Force MITG to EMEA Field Force MITG
    	User mitgUser = [Select Id from User where Profile.Name in ('EUR Field Force MITG', 'EMEA Field Force MITG') AND IsActive = true AND Id NOT IN (Select AssigneeId from PermissionSetASsignment where PermissionSet.Name = 'Box_Builder_Admin') Limit 1];
        //User mitgUser = [Select Id from User where Profile.Name = 'EMEA Field Force MITG' AND IsActive = true AND Id NOT IN (Select AssigneeId from PermissionSetASsignment where PermissionSet.Name = 'Box_Builder_Admin') Limit 1];
    	
    	Procedural_Solutions_Kit__c box = new Procedural_Solutions_Kit__c();
	    box.Mobile_ID__c = 'mobileId';
	    box.OwnerId = mitgUser.Id;
	    insert box;
    	
    	System.runAs(mitgUser){
	    	
	    	//Create
	    	Procedural_Solutions_Kit_Product__c boxProduct = new Procedural_Solutions_Kit_Product__c();
	    	boxProduct.Procedural_Solutions_Kit__c = box.Id;
	    	boxProduct.Mobile_ID__c = 'mobileId';
	    	
	    	ws_ProceduralSolutionKitProductService.ProceduralSolutionKitProductRequest request = new ws_ProceduralSolutionKitProductService.ProceduralSolutionKitProductRequest();
		 	request.ProceduralSolutionKitProduct = boxProduct;
		 	request.id = 'testId';
		 		 	
	    	RestRequest req = new RestRequest();	 	
		 	req.requestbody = Blob.valueOf(JSON.serialize(request));
		 	
		 	RestContext.request = req;
		 	
		 	String resp = ws_ProceduralSolutionKitProductService.doPost();
		 	
		 	ws_ProceduralSolutionKitProductService.ProceduralSolutionKitProductResponse response = (ws_ProceduralSolutionKitProductService.ProceduralSolutionKitProductResponse) JSON.deserialize(resp, ws_ProceduralSolutionKitProductService.ProceduralSolutionKitProductResponse.class);
		 	
		 	System.assert(response.success == true);
		 	
		 	boxProduct = [Select Id, Mobile_Id__c from Procedural_Solutions_Kit_Product__c where Mobile_Id__c = 'mobileId'];
			
			//Delete		 	
		 	ws_ProceduralSolutionKitProductService.ProceduralSolutionKitProductDeleteRequest requestDel = new ws_ProceduralSolutionKitProductService.ProceduralSolutionKitProductDeleteRequest();
	 		requestDel.ProceduralSolutionKitProduct = boxProduct;
	 		requestDel.id = 'testId';
		 	
		 	RestRequest reqDel = new RestRequest();	 	
		 	reqDel.requestbody = Blob.valueOf(JSON.serialize(requestDel));
		 	
		 	RestContext.request = reqDel;
		 	
		 	resp = ws_ProceduralSolutionKitProductService.doDelete();
		 	
		 	ws_ProceduralSolutionKitProductService.ProceduralSolutionKitProductDeleteResponse responseDel = (ws_ProceduralSolutionKitProductService.ProceduralSolutionKitProductDeleteResponse) JSON.deserialize(resp, ws_ProceduralSolutionKitProductService.ProceduralSolutionKitProductDeleteResponse.class);
		 	
		 	System.assert(responseDel.success == true);
		 	
		 	List<Procedural_Solutions_Kit_Product__c> boxProducts = [Select Id from Procedural_Solutions_Kit_Product__c where Mobile_Id__c = 'mobileId'];
		 	
		 	System.assert(boxProducts.size() == 0);
		 	
		 	//Delete (record not found)
		 	resp = ws_ProceduralSolutionKitProductService.doDelete();
		 	
		 	responseDel = (ws_ProceduralSolutionKitProductService.ProceduralSolutionKitProductDeleteResponse) JSON.deserialize(resp, ws_ProceduralSolutionKitProductService.ProceduralSolutionKitProductDeleteResponse.class);
		 	
		 	System.assert(responseDel.success == true);
		 	System.assert(responseDel.message == 'ProceduralSolutionKitProduct not found');
    	}
    }
    
    private static testmethod void testProceduralSolutionKitShare(){
    	//PMT-22787: MITG EMEA User Setup - Changed profile from EUR Field Force MITG to EMEA Field Force MITG
    	User mitgUser = [Select Id from User where Profile.Name in ('EUR Field Force MITG', 'EMEA Field Force MITG') AND IsActive = true AND Id NOT IN (Select AssigneeId from PermissionSetASsignment where PermissionSet.Name = 'Box_Builder_Admin') Limit 1];
        //User mitgUser = [Select Id from User where Profile.Name = 'EMEA Field Force MITG' AND IsActive = true AND Id NOT IN (Select AssigneeId from PermissionSetASsignment where PermissionSet.Name = 'Box_Builder_Admin') Limit 1];
    	
    	Procedural_Solutions_Kit__c box = new Procedural_Solutions_Kit__c();
	    box.Mobile_ID__c = 'mobileId';
	    box.OwnerId = mitgUser.Id;
	    insert box;
	    
	    Id currentUserId = UserInfo.getUserId();
    	
    	System.runAs(mitgUser){
	    	
	    	//Create
	    	Procedural_Solutions_Kit_Share__c boxShare = new Procedural_Solutions_Kit_Share__c();
	    	boxShare.Procedural_Solutions_Kit__c = box.Id;
	    	boxShare.Mobile_ID__c = 'mobileId';
	    	boxShare.User__c = currentUserId;
	    	
	    	ws_ProceduralSolutionKitShareService.ProceduralSolutionKitShareRequest request = new ws_ProceduralSolutionKitShareService.ProceduralSolutionKitShareRequest();
		 	request.ProceduralSolutionKitShare = boxShare;
		 	request.id = 'testId';
		 		 	
	    	RestRequest req = new RestRequest();	 	
		 	req.requestbody = Blob.valueOf(JSON.serialize(request));
		 	
		 	RestContext.request = req;
		 	
		 	String resp = ws_ProceduralSolutionKitShareService.doPost();
		 	
		 	ws_ProceduralSolutionKitShareService.ProceduralSolutionKitShareResponse response = (ws_ProceduralSolutionKitShareService.ProceduralSolutionKitShareResponse) JSON.deserialize(resp, ws_ProceduralSolutionKitShareService.ProceduralSolutionKitShareResponse.class);
		 	
		 	System.assert(response.success == true);
		 	
		 	boxShare = [Select Id, Mobile_Id__c from Procedural_Solutions_Kit_Share__c where Mobile_Id__c = 'mobileId'];
		 	
		 	List<Procedural_Solutions_Kit__share> shares = [Select Id from Procedural_Solutions_Kit__share where ParentId = :box.Id AND UserOrGroupId = :currentUserId AND RowCause = :Schema.Procedural_Solutions_Kit__Share.RowCause.manual];
		 	System.assert(shares.size() == 1);
			
			//Delete		 	
		 	ws_ProceduralSolutionKitShareService.ProceduralSolutionKitShareDeleteRequest requestDel = new ws_ProceduralSolutionKitShareService.ProceduralSolutionKitShareDeleteRequest();
	 		requestDel.ProceduralSolutionKitShare = boxShare;
	 		requestDel.id = 'testId';
		 	
		 	RestRequest reqDel = new RestRequest();	 	
		 	reqDel.requestbody = Blob.valueOf(JSON.serialize(requestDel));
		 	
		 	RestContext.request = reqDel;
		 	
		 	resp = ws_ProceduralSolutionKitShareService.doDelete();
		 	
		 	ws_ProceduralSolutionKitShareService.ProceduralSolutionKitShareDeleteResponse responseDel = (ws_ProceduralSolutionKitShareService.ProceduralSolutionKitShareDeleteResponse) JSON.deserialize(resp, ws_ProceduralSolutionKitShareService.ProceduralSolutionKitShareDeleteResponse.class);
		 	
		 	System.assert(responseDel.success == true);
		 	
		 	shares = [Select Id from Procedural_Solutions_Kit__share where ParentId = :box.Id AND UserOrGroupId = :currentUserId AND RowCause = :Schema.Procedural_Solutions_Kit__Share.RowCause.manual];
		 	System.assert(shares.size() == 0);
		 	
		 	List<Procedural_Solutions_Kit_Share__c> boxes = [Select Id from Procedural_Solutions_Kit_Share__c where Mobile_Id__c = 'mobileId'];		 	
		 	System.assert(boxes.size() == 0);
		 	
		 	//Delete (record not found)
		 	resp = ws_ProceduralSolutionKitShareService.doDelete();
		 	
		 	responseDel = (ws_ProceduralSolutionKitShareService.ProceduralSolutionKitShareDeleteResponse) JSON.deserialize(resp, ws_ProceduralSolutionKitShareService.ProceduralSolutionKitShareDeleteResponse.class);
		 	
		 	System.assert(responseDel.success == true);
		 	System.assert(responseDel.message == 'ProceduralSolutionKitShare not found');
    	}
    }
    
    @TestSetup
    private static void setupSettings(){
    	
    	SystemAdministratorProfileId__c ids = SystemAdministratorProfileId__c.getInstance('Default');
    	
    	if(ids == null){
    		ids = new SystemAdministratorProfileId__c();
    		ids.Name = 'Default';
    	}
    	
    	if(ids.SystemAdministrator__c == null){
    		
    		ids.SystemAdministrator__c = String.valueOf([Select Id from Profile where Name = 'System Administrator'].Id).substring(0,15);
    		upsert ids;
    	}
    	
    	MITG_App_Procedure__c mitgProcedure = new MITG_App_Procedure__c();
    	mitgProcedure.Name = 'Test Procedure';
    	mitgProcedure.App__c = 'Procedural Solutions Builder';
    	mitgProcedure.Region__c = 'EUR';
    	insert mitgProcedure;
    }
}