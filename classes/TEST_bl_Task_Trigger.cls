@isTest private class TEST_bl_Task_Trigger {
	
	private static User oUser_SystemAdministrator1;
	private static User oUser_SystemAdministrator2;

	private static User oUser_SystemAdministratorMDT1;
	private static User oUser_SystemAdministratorMDT2;

	@isTest private static void createTestData_User_SystemAdministrator() {
		
		List<User> lstUser = new List<User>();
		lstUser.add(clsTestData_User.createUser_SystemAdministrator('tadm001', false));
		lstUser.add(clsTestData_User.createUser_SystemAdministrator('tadm002', false));

		oUser_SystemAdministrator1 = lstUser[0];
		oUser_SystemAdministrator2 = lstUser[1];

	}

	@isTest private static void createTestData_User_SystemAdministratorMDT() {
		

		List<User> lstUser = new List<User>();
		lstUser.add(clsTestData_User.createUser_SystemAdministratorMDT('tadm001', false));
		lstUser.add(clsTestData_User.createUser_SystemAdministratorMDT('tadm002', false));

		oUser_SystemAdministratorMDT1 = lstUser[0];
		oUser_SystemAdministratorMDT2 = lstUser[1];

	}

	@isTest private static void createTestData_Email2SFDC_PublicTask() {
		
		// Create Custom Setting Data
		clsTestData_CustomSetting.oMain_Email2SFDC_PublicTask = null;
		clsTestData_CustomSetting.bEmail2SFDC_PublicTask_PublicTask = true;
		clsTestData_CustomSetting.bEmail2SFDC_PublicTask_EmailToSalesforceOnly = true;
		clsTestData_CustomSetting.tEmail2SFDC_PublicTask_SetupOwnerId = clsUtil.getUserProfileId('System Administrator');
		clsTestData_CustomSetting.createEmail2SFDC_PublicTask();

		clsTestData_CustomSetting.oMain_Email2SFDC_PublicTask = null;
		clsTestData_CustomSetting.bEmail2SFDC_PublicTask_PublicTask = true;
		clsTestData_CustomSetting.bEmail2SFDC_PublicTask_EmailToSalesforceOnly = false;
		clsTestData_CustomSetting.tEmail2SFDC_PublicTask_SetupOwnerId = clsUtil.getUserProfileId('System Administrator MDT');
		clsTestData_CustomSetting.createEmail2SFDC_PublicTask();

	}	

	@isTest private static void test_setTaskAsPublic_Public1() {

		// Create Test Data
		createTestData_User_SystemAdministrator();
		System.runAs(oUser_SystemAdministrator1){
			createTestData_Email2SFDC_PublicTask();
		}


		// Execute Logic
		Test.startTest();

		Task oTask;

		System.runAs(oUser_SystemAdministrator2){

			oTask = new Task();

				oTask.Subject = 'TASK Subject';
				oTask.Description = 'Additional To: emailtosalesforce@1tkkaoxzajcncd7vk2n8s4ilqmsc1zqin7hg4cuq214aconbek.5e-8pteuay.cs84.le.sandbox.salesforce.com .....';
				oTask.ActivityDate = Date.today();
				oTask.Status = 'Not Started';
				oTask.Priority = 'Normal';

			insert oTask;

		}

		Test.stopTest();


		// Validate Result
		oTask = [SELECT IsVisibleInSelfService FROM Task WHERE Id = :oTask.Id];
		System.assertEquals(oTask.IsVisibleInSelfService, true);


	}

	@isTest private static void test_setTaskAsPublic_Public2() {

		// Create Test Data
		createTestData_User_SystemAdministratorMDT();
		System.runAs(oUser_SystemAdministratorMDT1){
			createTestData_Email2SFDC_PublicTask();
		}


		// Execute Logic
		Test.startTest();

		Task oTask;

		System.runAs(oUser_SystemAdministratorMDT2){

			oTask = new Task();

				oTask.Subject = 'TASK Subject';
				oTask.Description = 'TASK Description';
				oTask.ActivityDate = Date.today();
				oTask.Status = 'Not Started';
				oTask.Priority = 'Normal';

			insert oTask;

		}

		Test.stopTest();


		// Validate Result
		oTask = [SELECT IsVisibleInSelfService FROM Task WHERE Id = :oTask.Id];
		System.assertEquals(oTask.IsVisibleInSelfService, true);


	}	

	@isTest private static void test_setTaskAsPublic_NotPublic() {

		// Create Test Data
		createTestData_User_SystemAdministrator();


		// Execute Logic
		Test.startTest();

		Task oTask;

		System.runAs(oUser_SystemAdministrator2){

			oTask = new Task();

				oTask.Subject = 'TASK Subject';
				oTask.Description = 'Additional To: emailtosalesforce@1tkkaoxzajcncd7vk2n8s4ilqmsc1zqin7hg4cuq214aconbek.5e-8pteuay.cs84.le.sandbox.salesforce.com .....';
				oTask.ActivityDate = Date.today();
				oTask.Status = 'Not Started';
				oTask.Priority = 'Normal';

			insert oTask;

		}

		Test.stopTest();


		// Validate Result
		oTask = [SELECT IsVisibleInSelfService FROM Task WHERE Id = :oTask.Id];
		System.assertEquals(oTask.IsVisibleInSelfService, false);


	}	
	
	@isTest(SeeAllData=true)	
	private static void testAccountPlanObjectiveMandatory(){
		
		Account acc = new Account();
		acc.Name = 'Unit Test Account';
		acc.SAP_Id__c = '9123456789';
		acc.Account_Country_vs__c = 'Belgium';
				
		Account acc_CAN = new Account();
		acc_CAN.Name = 'Unit Test Account Canada';
		acc_CAN.SAP_Id__c = '9987654321';
		acc_CAN.Account_Country_vs__c = 'Canada';
		
		insert new List<Account>{acc, acc_CAN};
			
		Account_Plan_2__c DIB_AccPlan = new Account_Plan_2__c();
		DIB_AccPlan.Account__c = acc.Id;
		DIB_AccPlan.Account_Plan_Level__c = 'Sub Business Unit';
		DIB_AccPlan.Business_Unit_Group__c = [Select Id from Business_Unit_Group__c where Name = 'Diabetes' AND Master_Data__r.Company_Code_Text__c = 'EUR'].Id;
		DIB_AccPlan.Business_Unit__c = [Select Id from Business_Unit__c where Name = 'Diabetes' AND Business_Unit_Group__c = :DIB_AccPlan.Business_Unit_Group__c].Id;		
		DIB_AccPlan.Sub_Business_Unit__c = [Select Id from Sub_Business_Units__c where Name = 'Diabetes Core' AND Business_Unit__c = :DIB_AccPlan.Business_Unit__c].Id;		
		
		Account_Plan_2__c NON_DIB_AccPlan = new Account_Plan_2__c();
		NON_DIB_AccPlan.Account__c = acc.Id;
		NON_DIB_AccPlan.Account_Plan_Level__c = 'Sub Business Unit';
		NON_DIB_AccPlan.Business_Unit_Group__c = [Select Id from Business_Unit_Group__c where Name = 'CVG' AND Master_Data__r.Company_Code_Text__c = 'EUR'].Id;
		NON_DIB_AccPlan.Business_Unit__c = [Select Id from Business_Unit__c where Name = 'CRHF' AND Business_Unit_Group__c = :NON_DIB_AccPlan.Business_Unit_Group__c].Id;		
		NON_DIB_AccPlan.Sub_Business_Unit__c = [Select Id from Sub_Business_Units__c where Name = 'Implantables & Diagnostic' AND Business_Unit__c = :NON_DIB_AccPlan.Business_Unit__c].Id;
		
		Account_Plan_2__c CAN_DIB_AccPlan = new Account_Plan_2__c();
		CAN_DIB_AccPlan.Account__c = acc_CAN.Id;
		CAN_DIB_AccPlan.Account_Plan_Level__c = 'Sub Business Unit';
		CAN_DIB_AccPlan.Business_Unit_Group__c = [Select Id from Business_Unit_Group__c where Name = 'Diabetes' AND Master_Data__r.Company_Code_Text__c = 'EUR'].Id;
		CAN_DIB_AccPlan.Business_Unit__c = [Select Id from Business_Unit__c where Name = 'Diabetes' AND Business_Unit_Group__c = :CAN_DIB_AccPlan.Business_Unit_Group__c].Id;		
		CAN_DIB_AccPlan.Sub_Business_Unit__c = [Select Id from Sub_Business_Units__c where Name = 'Diabetes Core' AND Business_Unit__c = :CAN_DIB_AccPlan.Business_Unit__c].Id;
		
		insert new List<Account_Plan_2__c>{DIB_AccPlan, NON_DIB_AccPlan, CAN_DIB_AccPlan};
		
		Business_Objective__c DIB_Objective = new Business_Objective__c();
		DIB_Objective.Name = 'Unit Test';
		DIB_Objective.Explanation__c = 'Unit Test';
		DIB_Objective.Active__c = true;
		DIB_Objective.Sub_Business_Unit__c = DIB_AccPlan.Sub_Business_Unit__c;
		DIB_Objective.Type__c = 'Qualitative';
		DIB_Objective.Target_Date__c = 'FY ' + Date.today().year();
		insert DIB_Objective;
		
		Account_Plan_Objective__c objective = new Account_Plan_Objective__c();
		objective.Account_Plan__c = DIB_AccPlan.Id;
		objective.Business_Objective__c = DIB_Objective.Id;
		objective.Description__c = 'Test description';
		insert objective;
		
		Test.startTest();
		
		Task DIB_Task = new Task();
		DIB_Task.WhatId = DIB_AccPlan.Id;
		DIB_Task.Subject = 'Test DIB Task';
		DIB_Task.Status = 'Not Started';
		DIB_Task.Priority = 'Normal';
		
		Boolean isError = false;
		try{
			
			insert DIB_Task;
			
		}catch(Exception e){
			
			isError = true;
			System.assert(e.getMessage().contains('Account Plan Objective is mandatory when the task is connected to a Diabetes Account Plan'));
		}
		
		System.assert(isError == true);
				
		DIB_Task.Account_Plan_Objective__c = objective.Id;
		insert DIB_Task;
		
		Task NON_DIB_Task = new Task();
		NON_DIB_Task.WhatId = NON_DIB_AccPlan.Id;
		NON_DIB_Task.Subject = 'Test DIB Task';
		NON_DIB_Task.Status = 'Not Started';
		NON_DIB_Task.Priority = 'Normal';
		
		insert NON_DIB_Task;
		
		Task CAN_DIB_Task = new Task();
		CAN_DIB_Task.WhatId = CAN_DIB_AccPlan.Id;
		CAN_DIB_Task.Subject = 'Test DIB Task';
		CAN_DIB_Task.Status = 'Not Started';
		CAN_DIB_Task.Priority = 'Normal';
		
		insert CAN_DIB_Task;	
	} 


	@isTest private static void test_CVG_MET_PopulateAccount(){


		//------------------------------------------
		// Create Test Data
		//------------------------------------------
		clsTestData_Account.iRecord_Account = 2;
		clsTestData_Account.idRecordType_Account = clsUtil.getRecordTypeByDevName('Account', 'NON_SAP_Account').Id;
		List<Account> lstAccount = clsTestData_Account.createAccount(true);

		clsTestData_Opportunity.iRecord_Opportunity = 2;
		clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'CVG_Project').Id;
		List<Opportunity> lstOpportunity = clsTestData_Opportunity.createOpportunity(false);
		lstOpportunity[0].AccountId = lstAccount[0].Id;
		lstOpportunity[1].AccountId = lstAccount[1].Id;
		insert lstOpportunity;
		//------------------------------------------


		//------------------------------------------
		// Test Logic
		//------------------------------------------
		Test.startTest();

			Task oTask = new Task();
				oTask.RecordTypeId = clsUtil.getRecordTypeByDevName('Task', 'Marketing_Execution_Task').Id;
				oTask.Subject = 'TASK Subject';
				oTask.Description = 'Description';
				oTask.ActivityDate = Date.today();
				oTask.Status = 'Not Started';
				oTask.Priority = 'Normal';
				oTask.WhatId = lstOpportunity[0].Id;
			insert oTask;

			oTask = [SELECT Id, Account__c FROM Task WHERE Id = :oTask.Id];
			System.assertEquals(oTask.Account__c, lstOpportunity[0].AccountId, lstAccount[0].Id);


				oTask.Account__c = lstAccount[1].Id;
			update oTask;
						
			oTask = [SELECT Id, Account__c FROM Task WHERE Id = :oTask.Id];
			System.assertEquals(oTask.Account__c, lstOpportunity[0].AccountId, lstAccount[0].Id);


				oTask.WhatId = lstOpportunity[1].Id;
			update oTask;

			oTask = [SELECT Id, Account__c FROM Task WHERE Id = :oTask.Id];
			System.assertEquals(oTask.Account__c, lstOpportunity[1].AccountId, lstAccount[1].Id);


				oTask.WhatId = null;
			update oTask;

			oTask = [SELECT Id, Account__c FROM Task WHERE Id = :oTask.Id];
			System.assertEquals(oTask.Account__c, null);

		Test.stopTest();
		//------------------------------------------
		

		//------------------------------------------
		// Validate Test Result
		//------------------------------------------

		//------------------------------------------


	}


	@isTest private static void test_CVG_MET_PopulateShortDescription(){


		//------------------------------------------
		// Create Test Data
		//------------------------------------------
		clsTestData_Account.iRecord_Account = 2;
		clsTestData_Account.idRecordType_Account = clsUtil.getRecordTypeByDevName('Account', 'NON_SAP_Account').Id;
		List<Account> lstAccount = clsTestData_Account.createAccount(true);

		clsTestData_Opportunity.iRecord_Opportunity = 2;
		clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'CVG_Project').Id;
		List<Opportunity> lstOpportunity = clsTestData_Opportunity.createOpportunity(false);
		lstOpportunity[0].AccountId = lstAccount[0].Id;
		lstOpportunity[1].AccountId = lstAccount[1].Id;
		insert lstOpportunity;
		//------------------------------------------


		//------------------------------------------
		// Test Logic
		//------------------------------------------
		Test.startTest();

			Task oTask = new Task();
				oTask.RecordTypeId = clsUtil.getRecordTypeByDevName('Task', 'Marketing_Execution_Task').Id;
				oTask.Subject = 'TASK Subject';
				oTask.Description = 'Description0015Description0015Description0015Description0015Description0015Description0015Description0015Description0015Description0015Description0015Description0015Description0015Description0015Description0015Description0015Description0015Description0015Description0015Description0015Description0015';
				oTask.ActivityDate = Date.today();
				oTask.Status = 'Not Started';
				oTask.Priority = 'Normal';
				oTask.WhatId = lstOpportunity[0].Id;
			insert oTask;

			oTask = [SELECT Id, Description, Short_Description__c FROM Task WHERE Id = :oTask.Id];
			System.assertEquals(oTask.Short_Description__c, oTask.Description.left(255), 'Description0015Description0015Description0015Description0015Description0015Description0015Description0015Description0015Description0015Description0015Description0015Description0015Description0015Description0015Description0015Description0015Description0015');


				oTask.Description = 'Short Description';
			update oTask;
						
			oTask = [SELECT Id, Description, Short_Description__c FROM Task WHERE Id = :oTask.Id];
			System.assertEquals(oTask.Short_Description__c, oTask.Description.left(255), 'Short Description');

		Test.stopTest();
		//------------------------------------------
		

		//------------------------------------------
		// Validate Test Result
		//------------------------------------------

		//------------------------------------------


	}


	@isTest private static void test_CVG_MET_SendNotificationEmail(){


		//------------------------------------------
		// Create Test Data
		//------------------------------------------
		User oUser_Admin1 = clsTestData_User.createUser_SystemAdministratorMDT('tstadm1', false);
		User oUser_Marketeer = clsTestData_User.createUser_SystemAdministratorMDT('tstadm2', false);
		User oUser_Admin2 = clsTestData_User.createUser_SystemAdministratorMDT('tstadm3', false);
		List<User> lstUser = new List<User>{oUser_Admin1, oUser_Admin2, oUser_Marketeer};
		insert lstUser;

		List<Opportunity> lstOpportunity;
		System.runAs(oUser_Admin1){
			PermissionSet oPermissionSet = [SELECT Id FROM PermissionSet WHERE Name = 'CVG_Marketing_Strategy'];
			PermissionSetAssignment oPermissionSetAssignment = new PermissionSetAssignment();
				oPermissionSetAssignment.AssigneeId = oUser_Marketeer.Id;
				oPermissionSetAssignment.PermissionSetId = oPermissionSet.Id;
			insert oPermissionSetAssignment ;

			clsTestData_Account.iRecord_Account = 2;
			clsTestData_Account.idRecordType_Account = clsUtil.getRecordTypeByDevName('Account', 'NON_SAP_Account').Id;
			List<Account> lstAccount = clsTestData_Account.createAccount(true);

			clsTestData_Opportunity.iRecord_Opportunity = 2;
			clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'CVG_Project').Id;
			lstOpportunity = clsTestData_Opportunity.createOpportunity(false);
				lstOpportunity[0].AccountId = lstAccount[0].Id;
				lstOpportunity[1].AccountId = lstAccount[1].Id;
			insert lstOpportunity;

		}
		//------------------------------------------


		//------------------------------------------
		// Test Logic
		//------------------------------------------
		System.debug('**BC** Limits.getEmailInvocations() 1 : ' + Limits.getEmailInvocations()); 
		Test.startTest();

			System.runAs(oUser_Marketeer){

				Task oTask1 = new Task();
					oTask1.RecordTypeId = clsUtil.getRecordTypeByDevName('Task', 'Marketing_Execution_Task').Id;
					oTask1.Subject = 'TASK Subject 1';
					oTask1.Description = 'Description 1';
					oTask1.ActivityDate = Date.today();
					oTask1.Status = 'Not Started';
					oTask1.Priority = 'Normal';
					oTask1.WhatId = lstOpportunity[0].Id;
					oTask1.Created_in_Mobile_CVG__c = false;
				insert oTask1;
				oTask1 = [SELECT Id, OwnerId FROM Task WHERE Id = :oTask1.Id];
				System.assertEquals(oTask1.OwnerId, oUser_Marketeer.Id);


					oTask1.OwnerId = oUser_Admin2.Id;
				update oTask1;
				oTask1 = [SELECT Id, OwnerId FROM Task WHERE Id = :oTask1.Id];
				System.assertEquals(oTask1.OwnerId, oUser_Admin2.Id);


				Task oTask2 = new Task();
					oTask2.RecordTypeId = clsUtil.getRecordTypeByDevName('Task', 'Marketing_Execution_Task').Id;
					oTask2.Subject = 'TASK Subject 1';
					oTask2.Description = 'Description 1';
					oTask2.ActivityDate = Date.today();
					oTask2.Status = 'Not Started';
					oTask2.Priority = 'Normal';
					oTask2.WhatId = lstOpportunity[0].Id;
					oTask2.OwnerId = oUser_Admin2.Id;
					oTask2.Created_in_Mobile_CVG__c = false;
				insert oTask2;
				oTask2 = [SELECT Id, OwnerId FROM Task WHERE Id = :oTask2.Id];
				System.assertEquals(oTask2.OwnerId, oUser_Admin2.Id);

			
			}

		Test.stopTest();
		//------------------------------------------
		

		//------------------------------------------
		// Validate Test Result
		//------------------------------------------

		//------------------------------------------

	}


	@isTest private static void test_CVG_MET_UpdateCampaignMember(){


		//------------------------------------------
		// Create Test Data
		//------------------------------------------
		User oUser_Admin1 = clsTestData_User.createUser_SystemAdministratorMDT('tstadm1', false);
		User oUser_Marketeer = clsTestData_User.createUser_SystemAdministratorMDT('tstadm2', false);
		User oUser_Admin2 = clsTestData_User.createUser_SystemAdministratorMDT('tstadm3', false);
		List<User> lstUser = new List<User>{oUser_Admin1, oUser_Admin2, oUser_Marketeer};
		insert lstUser;

		List<Opportunity> lstOpportunity;
		List<Account> lstAccount;
		List<Contact> lstContact;
		List<Campaign> lstCampaign;
		Campaign oCampaign_Tactic1;
		Campaign oCampaign_Tactic2;
		List<CampaignMember> lstCampaignMember;
		List<Campaign_Account__c> lstCampaignAccount;

		Map<String, String> mapTaskStatus_Status = new Map<String, String>();
			mapTaskStatus_Status.put('Completed', 'Completed');
			mapTaskStatus_Status.put('In Progress', 'In Progress');
			mapTaskStatus_Status.put('Waiting on someone else', 'In Progress');
			mapTaskStatus_Status.put('Deferred', 'Aborted');

		List<String> lstStatus = new List<String>();
		lstStatus.addAll(mapTaskStatus_Status.keySet());

		System.runAs(oUser_Admin1){

			// Custom Setting Data
			List<CampaignMemberStatus__c> lstCampaignMemberStatus = new List<CampaignMemberStatus__c>();
            CampaignMemberStatus__c oCMS1 = new CampaignMemberStatus__c();
                oCMS1.Name = 'CVG Planned';
                oCMS1.Member_Status_Label__c = 'Planned';
                oCMS1.IsDefault__c = false;
                oCMS1.HasResponded__c = false;
                oCMS1.SortOrder__c = 3;
                oCMS1.Filter_Field__c = 'RecordTypeId';
                oCMS1.Filter_Value_Text__c = '0121r000000xGUdAAM';
            lstCampaignMemberStatus.add(oCMS1);
            CampaignMemberStatus__c oCMS2 = new CampaignMemberStatus__c();
                oCMS2.Name = 'CVG Completed';
                oCMS2.Member_Status_Label__c = 'Completed';
                oCMS2.IsDefault__c = false;
                oCMS2.HasResponded__c = false;
                oCMS2.SortOrder__c = 5;
                oCMS2.Filter_Field__c = 'RecordTypeId';
                oCMS2.Filter_Value_Text__c = '0121r000000xGUdAAM';
            lstCampaignMemberStatus.add(oCMS2);
            CampaignMemberStatus__c oCMS3 = new CampaignMemberStatus__c();
                oCMS3.Name = 'CVG Aborted';
                oCMS3.Member_Status_Label__c = 'Aborted';
                oCMS3.IsDefault__c = false;
                oCMS3.HasResponded__c = false;
                oCMS3.SortOrder__c = 6;
                oCMS3.Filter_Field__c = 'RecordTypeId';
                oCMS3.Filter_Value_Text__c = '0121r000000xGUdAAM';
            lstCampaignMemberStatus.add(oCMS3);
            CampaignMemberStatus__c oCMS4 = new CampaignMemberStatus__c();
                oCMS4.Name = 'CVG In Progress';
                oCMS4.Member_Status_Label__c = 'In Progress	';
                oCMS4.IsDefault__c = false;
                oCMS4.HasResponded__c = false;
                oCMS4.SortOrder__c = 4;
                oCMS4.Filter_Field__c = 'RecordTypeId';
                oCMS4.Filter_Value_Text__c = '0121r000000xGUdAAM';
            lstCampaignMemberStatus.add(oCMS4);
			insert lstCampaignMemberStatus;


			// Assign user to permission set to gain access to the Task RecordType "Marketing_Execution_Task"
			PermissionSet oPermissionSet = [SELECT Id FROM PermissionSet WHERE Name = 'CVG_Marketing_Strategy'];
			PermissionSetAssignment oPermissionSetAssignment = new PermissionSetAssignment();
				oPermissionSetAssignment.AssigneeId = oUser_Marketeer.Id;
				oPermissionSetAssignment.PermissionSetId = oPermissionSet.Id;
			insert oPermissionSetAssignment ;


			// Create Account Data
			clsTestData_Account.iRecord_Account = 5;
			clsTestData_Account.idRecordType_Account = clsUtil.getRecordTypeByDevName('Account', 'NON_SAP_Account').Id;
			lstAccount = clsTestData_Account.createAccount(true);


			// Create Contact Data
			clsTestData_Contact.iRecord_Contact = 5;
			lstContact = clsTestData_Contact.createContact(false);
				lstContact[0].AccountId = lstAccount[0].Id;
				lstContact[1].AccountId = lstAccount[0].Id;
				lstContact[2].AccountId = lstAccount[1].Id;
				lstContact[3].AccountId = lstAccount[1].Id;
				lstContact[4].AccountId = lstAccount[1].Id;
			insert lstContact;


			// Create Opportunity Data
			clsTestData_Opportunity.iRecord_Opportunity = 5;
			clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'CVG_Project').Id;
			lstOpportunity = clsTestData_Opportunity.createOpportunity(false);
				lstOpportunity[0].AccountId = lstAccount[0].Id;
				lstOpportunity[1].AccountId = lstAccount[1].Id;
				lstOpportunity[2].AccountId = lstAccount[2].Id;
				lstOpportunity[3].AccountId = lstAccount[3].Id;
				lstOpportunity[4].AccountId = lstAccount[4].Id;
			insert lstOpportunity;


			// Create Tactic Campaign Data
			lstCampaign = new List<Campaign>();
			oCampaign_Tactic1 = new Campaign();
				oCampaign_Tactic1.RecordTypeId = clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_Tactic').Id;
				oCampaign_Tactic1.Name = 'TEST Campaign Tactic 1';
			lstCampaign.add(oCampaign_Tactic1);
			oCampaign_Tactic2 = new Campaign();
				oCampaign_Tactic2.RecordTypeId = clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_Tactic').Id;
				oCampaign_Tactic2.Name = 'TEST Campaign Tactic 2';
			lstCampaign.add(oCampaign_Tactic2);
			insert lstCampaign;


			// Create Campaign Member Data
			lstCampaignMember = new List<CampaignMember>();
			CampaignMember oCampaignMember1 = new CampaignMember();
				oCampaignMember1.CampaignId = oCampaign_Tactic1.Id;
				oCampaignMember1.ContactId = lstContact[0].Id;
				oCampaignMember1.Status = 'Planned';
			lstCampaignMember.add(oCampaignMember1);
			CampaignMember oCampaignMember2 = new CampaignMember();
				oCampaignMember2.CampaignId = oCampaign_Tactic1.Id;
				oCampaignMember2.ContactId = lstContact[1].Id;
				oCampaignMember2.Status = 'Planned';
			lstCampaignMember.add(oCampaignMember2);
			CampaignMember oCampaignMember3 = new CampaignMember();
				oCampaignMember3.CampaignId = oCampaign_Tactic1.Id;
				oCampaignMember3.ContactId = lstContact[2].Id;
				oCampaignMember3.Status = 'Planned';
			lstCampaignMember.add(oCampaignMember3);
			CampaignMember oCampaignMember4 = new CampaignMember();
				oCampaignMember4.CampaignId = oCampaign_Tactic1.Id;
				oCampaignMember4.ContactId = lstContact[3].Id;
				oCampaignMember4.Status = 'Planned';
			lstCampaignMember.add(oCampaignMember4);
			CampaignMember oCampaignMember5 = new CampaignMember();
				oCampaignMember5.CampaignId = oCampaign_Tactic1.Id;
				oCampaignMember5.ContactId = lstContact[4].Id;
				oCampaignMember5.Status = 'Planned';
			lstCampaignMember.add(oCampaignMember5);
			insert lstCampaignMember;


			// Create Campaign Account Data
			lstCampaignAccount = new List<Campaign_Account__c>();
			Campaign_Account__c oCampaignAccount1 = new Campaign_Account__c();
				oCampaignAccount1.Campaign__c = oCampaign_Tactic2.Id;
				oCampaignAccount1.Account__c = lstAccount[0].Id;
				oCampaignAccount1.Status__c = 'Planned';
			lstCampaignAccount.add(oCampaignAccount1);
			Campaign_Account__c oCampaignAccount2 = new Campaign_Account__c();
				oCampaignAccount2.Campaign__c = oCampaign_Tactic2.Id;
				oCampaignAccount2.Account__c = lstAccount[1].Id;
				oCampaignAccount2.Status__c = 'Planned';
			lstCampaignAccount.add(oCampaignAccount2);
			Campaign_Account__c oCampaignAccount3 = new Campaign_Account__c();
				oCampaignAccount3.Campaign__c = oCampaign_Tactic2.Id;
				oCampaignAccount3.Account__c = lstAccount[2].Id;
				oCampaignAccount3.Status__c = 'Planned';
			lstCampaignAccount.add(oCampaignAccount3);
			Campaign_Account__c oCampaignAccount4 = new Campaign_Account__c();
				oCampaignAccount4.Campaign__c = oCampaign_Tactic2.Id;
				oCampaignAccount4.Account__c = lstAccount[3].Id;
				oCampaignAccount4.Status__c = 'Planned';
			lstCampaignAccount.add(oCampaignAccount4);
			Campaign_Account__c oCampaignAccount5 = new Campaign_Account__c();
				oCampaignAccount5.Campaign__c = oCampaign_Tactic2.Id;
				oCampaignAccount5.Account__c = lstAccount[4].Id;
				oCampaignAccount5.Status__c = 'Planned';
			lstCampaignAccount.add(oCampaignAccount5);
			insert lstCampaignAccount;

		}


		List<Task> lstTask_CampaignMember;
		List<Task> lstTask_CampaignAccount;

		System.runAs(oUser_Marketeer){

			lstTask_CampaignMember = new List<Task>();
			Task oTask_CM_1_1 = new Task();
				oTask_CM_1_1.RecordTypeId = clsUtil.getRecordTypeByDevName('Task', 'Marketing_Execution_Task').Id;
				oTask_CM_1_1.Subject = 'TASK Subject CM 1 - 1';
				oTask_CM_1_1.Description = 'Description CM 1 - 1';
				oTask_CM_1_1.ActivityDate = Date.today();
				oTask_CM_1_1.Status = 'Not Started';
				oTask_CM_1_1.Priority = 'Normal';
				oTask_CM_1_1.Campaign__c = oCampaign_Tactic1.Id;
				oTask_CM_1_1.WhoId = lstContact[0].Id;
			lstTask_CampaignMember.add(oTask_CM_1_1);
			Task oTask_CM_1_2 = new Task();
				oTask_CM_1_2.RecordTypeId = clsUtil.getRecordTypeByDevName('Task', 'Marketing_Execution_Task').Id;
				oTask_CM_1_2.Subject = 'TASK Subject CM 1 - 2';
				oTask_CM_1_2.Description = 'Description CM 1 - 2';
				oTask_CM_1_2.ActivityDate = Date.today();
				oTask_CM_1_2.Status = 'Not Started';
				oTask_CM_1_2.Priority = 'Normal';
				oTask_CM_1_2.Campaign__c = oCampaign_Tactic1.Id;
				oTask_CM_1_2.WhoId = lstContact[0].Id;
			lstTask_CampaignMember.add(oTask_CM_1_2);
			Task oTask_CM_2 = new Task();
				oTask_CM_2.RecordTypeId = clsUtil.getRecordTypeByDevName('Task', 'Marketing_Execution_Task').Id;
				oTask_CM_2.Subject = 'TASK Subject CM 2';
				oTask_CM_2.Description = 'Description CM 2';
				oTask_CM_2.ActivityDate = Date.today();
				oTask_CM_2.Status = 'Not Started';
				oTask_CM_2.Priority = 'Normal';
				oTask_CM_2.Campaign__c = oCampaign_Tactic1.Id;
				oTask_CM_2.WhoId = lstContact[1].Id;
			lstTask_CampaignMember.add(oTask_CM_2);
			Task oTask_CM_3 = new Task();
				oTask_CM_3.RecordTypeId = clsUtil.getRecordTypeByDevName('Task', 'Marketing_Execution_Task').Id;
				oTask_CM_3.Subject = 'TASK Subject CM 2';
				oTask_CM_3.Description = 'Description CM 2';
				oTask_CM_3.ActivityDate = Date.today();
				oTask_CM_3.Status = 'Not Started';
				oTask_CM_3.Priority = 'Normal';
				oTask_CM_3.Campaign__c = oCampaign_Tactic1.Id;
				oTask_CM_3.WhoId = lstContact[2].Id;
			lstTask_CampaignMember.add(oTask_CM_3);
			Task oTask_CM_4 = new Task();
				oTask_CM_4.RecordTypeId = clsUtil.getRecordTypeByDevName('Task', 'Marketing_Execution_Task').Id;
				oTask_CM_4.Subject = 'TASK Subject CM 4';
				oTask_CM_4.Description = 'Description CM 4';
				oTask_CM_4.ActivityDate = Date.today();
				oTask_CM_4.Status = 'Not Started';
				oTask_CM_4.Priority = 'Normal';
				oTask_CM_4.Campaign__c = oCampaign_Tactic1.Id;
				oTask_CM_4.WhoId = lstContact[3].Id;
			lstTask_CampaignMember.add(oTask_CM_4);
			Task oTask_CM_5 = new Task();
				oTask_CM_5.RecordTypeId = clsUtil.getRecordTypeByDevName('Task', 'Marketing_Execution_Task').Id;
				oTask_CM_5.Subject = 'TASK Subject CM 5';
				oTask_CM_5.Description = 'Description CM 5';
				oTask_CM_5.ActivityDate = Date.today();
				oTask_CM_5.Status = 'Not Started';
				oTask_CM_5.Priority = 'Normal';
				oTask_CM_5.Campaign__c = oCampaign_Tactic1.Id;
				oTask_CM_5.WhoId = lstContact[4].Id;
			lstTask_CampaignMember.add(oTask_CM_5);
			insert lstTask_CampaignMember;

			lstTask_CampaignAccount = new List<Task>();
			Task oTask_CA_1_1 = new Task();
				oTask_CA_1_1.RecordTypeId = clsUtil.getRecordTypeByDevName('Task', 'Marketing_Execution_Task').Id;
				oTask_CA_1_1.Subject = 'TASK Subject CM 1 - 1';
				oTask_CA_1_1.Description = 'Description CM 1 - 1';
				oTask_CA_1_1.ActivityDate = Date.today();
				oTask_CA_1_1.Status = 'Not Started';
				oTask_CA_1_1.Priority = 'Normal';
				oTask_CA_1_1.Campaign__c = oCampaign_Tactic2.Id;
				oTask_CA_1_1.Account__c = lstOpportunity[0].AccountId;
				oTask_CA_1_1.WhatId = lstOpportunity[0].Id;
			lstTask_CampaignAccount.add(oTask_CA_1_1);
			Task oTask_CA_1_2 = new Task();
				oTask_CA_1_2.RecordTypeId = clsUtil.getRecordTypeByDevName('Task', 'Marketing_Execution_Task').Id;
				oTask_CA_1_2.Subject = 'TASK Subject CM 1 - 2';
				oTask_CA_1_2.Description = 'Description CM 1 - 2';
				oTask_CA_1_2.ActivityDate = Date.today();
				oTask_CA_1_2.Status = 'Not Started';
				oTask_CA_1_2.Priority = 'Normal';
				oTask_CA_1_2.Campaign__c = oCampaign_Tactic2.Id;
				oTask_CA_1_2.Account__c = lstOpportunity[0].AccountId;
				oTask_CA_1_2.WhatId = lstOpportunity[0].Id;
			lstTask_CampaignAccount.add(oTask_CA_1_2);
			Task oTask_CA_2 = new Task();
				oTask_CA_2.RecordTypeId = clsUtil.getRecordTypeByDevName('Task', 'Marketing_Execution_Task').Id;
				oTask_CA_2.Subject = 'TASK Subject CA 2';
				oTask_CA_2.Description = 'Description CA 2';
				oTask_CA_2.ActivityDate = Date.today();
				oTask_CA_2.Status = 'Not Started';
				oTask_CA_2.Priority = 'Normal';
				oTask_CA_2.Campaign__c = oCampaign_Tactic2.Id;
				oTask_CA_2.Account__c = lstOpportunity[1].AccountId;
				oTask_CA_2.WhatId = lstOpportunity[1].Id;
			lstTask_CampaignAccount.add(oTask_CA_2);
			Task oTask_CA_3 = new Task();
				oTask_CA_3.RecordTypeId = clsUtil.getRecordTypeByDevName('Task', 'Marketing_Execution_Task').Id;
				oTask_CA_3.Subject = 'TASK Subject CA 3';
				oTask_CA_3.Description = 'Description CA 3';
				oTask_CA_3.ActivityDate = Date.today();
				oTask_CA_3.Status = 'Not Started';
				oTask_CA_3.Priority = 'Normal';
				oTask_CA_3.Campaign__c = oCampaign_Tactic2.Id;
				oTask_CA_3.Account__c = lstOpportunity[2].AccountId;
				oTask_CA_3.WhatId = lstOpportunity[2].Id;
			lstTask_CampaignAccount.add(oTask_CA_3);
			Task oTask_CA_4 = new Task();
				oTask_CA_4.RecordTypeId = clsUtil.getRecordTypeByDevName('Task', 'Marketing_Execution_Task').Id;
				oTask_CA_4.Subject = 'TASK Subject CA 4';
				oTask_CA_4.Description = 'Description CA 4';
				oTask_CA_4.ActivityDate = Date.today();
				oTask_CA_4.Status = 'Not Started';
				oTask_CA_4.Priority = 'Normal';
				oTask_CA_4.Campaign__c = oCampaign_Tactic2.Id;
				oTask_CA_4.Account__c = lstOpportunity[3].AccountId;
				oTask_CA_4.WhatId = lstOpportunity[3].Id;
			lstTask_CampaignAccount.add(oTask_CA_4);
			Task oTask_CA_5 = new Task();
				oTask_CA_5.RecordTypeId = clsUtil.getRecordTypeByDevName('Task', 'Marketing_Execution_Task').Id;
				oTask_CA_5.Subject = 'TASK Subject CA 5';
				oTask_CA_5.Description = 'Description CA 5';
				oTask_CA_5.ActivityDate = Date.today();
				oTask_CA_5.Status = 'Not Started';
				oTask_CA_5.Priority = 'Normal';
				oTask_CA_5.Campaign__c = oCampaign_Tactic2.Id;
				oTask_CA_5.Account__c = lstOpportunity[4].AccountId;
				oTask_CA_5.WhatId = lstOpportunity[4].Id;
			lstTask_CampaignAccount.add(oTask_CA_5);
			insert lstTask_CampaignAccount;

		}
		//------------------------------------------


		//------------------------------------------
		// Test Logic
		//------------------------------------------
		Test.startTest();

			System.runAs(oUser_Marketeer){

				List<Task> lstTask_Update1 = new List<Task>();

				lstTask_CampaignMember[0].Status = lstStatus[0];
				lstTask_Update1.add(lstTask_CampaignMember[0]);
			
				lstTask_CampaignMember[2].Status = lstStatus[1];
				lstTask_Update1.add(lstTask_CampaignMember[2]);

				lstTask_CampaignMember[3].Status = lstStatus[2];
				lstTask_Update1.add(lstTask_CampaignMember[3]);
			
				lstTask_CampaignMember[4].Status = lstStatus[3];
				lstTask_Update1.add(lstTask_CampaignMember[4]);

				lstTask_CampaignAccount[0].Status = lstStatus[0];
				lstTask_Update1.add(lstTask_CampaignAccount[0]);

				lstTask_CampaignAccount[2].Status = lstStatus[1];
				lstTask_Update1.add(lstTask_CampaignAccount[2]);

				lstTask_CampaignAccount[3].Status = lstStatus[2];
				lstTask_Update1.add(lstTask_CampaignAccount[3]);

				lstTask_CampaignAccount[4].Status = lstStatus[3];
				lstTask_Update1.add(lstTask_CampaignAccount[4]);

				update lstTask_Update1;
			

				List<Task> lstTask_Update2 = new List<Task>();

				lstTask_CampaignMember[1].Status = lstStatus[0];
				lstTask_Update2.add(lstTask_CampaignMember[1]);

				lstTask_CampaignAccount[1].Status = lstStatus[0];
				lstTask_Update2.add(lstTask_CampaignAccount[1]);

				update lstTask_Update2;

			}



		Test.stopTest();
		//------------------------------------------
		

		//------------------------------------------
		// Validate Test Result
		//------------------------------------------
		lstCampaignMember = [SELECT Id, ContactId, Status FROM CampaignMember];
		System.assert(lstCampaignMember.size() == 5);
		Integer iNumCheck_CM = 0;

		for (CampaignMember oCampaignMember : lstCampaignMember){

			if (oCampaignMember.ContactId == lstContact[0].Id){

				System.assertEquals(oCampaignMember.Status, mapTaskStatus_Status.get(lstStatus[0]));
				iNumCheck_CM++;

			}else if (oCampaignMember.ContactId == lstContact[1].Id){

				System.assertEquals(oCampaignMember.Status, mapTaskStatus_Status.get(lstStatus[1]));
				iNumCheck_CM++;

			}else if (oCampaignMember.ContactId == lstContact[2].Id){

				System.assertEquals(oCampaignMember.Status, mapTaskStatus_Status.get(lstStatus[2]));
				iNumCheck_CM++;

			}else if (oCampaignMember.ContactId == lstContact[3].Id){

				System.assertEquals(oCampaignMember.Status, mapTaskStatus_Status.get(lstStatus[3]));
				iNumCheck_CM++;

			}else if (oCampaignMember.ContactId == lstContact[4].Id){

				System.assertEquals(oCampaignMember.Status, 'Planned');
				iNumCheck_CM++;

			}
			
		}
		System.assertEquals(iNumCheck_CM, 5);


		lstCampaignAccount = [SELECT Id, Account__c, Status__c FROM Campaign_Account__c];
		System.assert(lstCampaignAccount.size() == 5);
		Integer iNumCheck_CA = 0;

		for (Campaign_Account__c oCampaignAccount : lstCampaignAccount){
				
			if (oCampaignAccount.Account__c == lstAccount[0].Id){

				System.assertEquals(oCampaignAccount.Status__c, mapTaskStatus_Status.get(lstStatus[0]));
				iNumCheck_CA++;

			}else if (oCampaignAccount.Account__c == lstAccount[1].Id){

				System.assertEquals(oCampaignAccount.Status__c, mapTaskStatus_Status.get(lstStatus[1]));
				iNumCheck_CA++;

			}else if (oCampaignAccount.Account__c == lstAccount[2].Id){

				System.assertEquals(oCampaignAccount.Status__c, mapTaskStatus_Status.get(lstStatus[2]));
				iNumCheck_CA++;

			}else if (oCampaignAccount.Account__c == lstAccount[3].Id){

				System.assertEquals(oCampaignAccount.Status__c, mapTaskStatus_Status.get(lstStatus[3]));
				iNumCheck_CA++;

			}else if (oCampaignAccount.Account__c == lstAccount[4].Id){

				System.assertEquals(oCampaignAccount.Status__c, 'Planned');
				iNumCheck_CA++;

			}
			
		}
		System.assertEquals(iNumCheck_CA, 5);
		//------------------------------------------

	}

}