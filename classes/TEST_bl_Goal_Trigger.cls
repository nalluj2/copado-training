//--------------------------------------------------------------------------------------------------------------------------------
//  Author      :   Bart Caelen
//  Date        :   03-05-2019
//  Description :   APEX TEST Class for the APEX Trigger tr_Goal and APEX Class bl_Goal_Trigger
//  Change Log  :    
//--------------------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_bl_Goal_Trigger {

	@isTest private static void test_populateBusinessUnitGroup() {

		// Create Master Data - Company - BUG - BU - SBU
		clsTestData_MasterData.createSubBusinessUnit();

		// Load Test Data
		List<Sub_Business_Units__c> lstSBU = [SELECT Id, Business_Unit__r.Business_Unit_Group__r.Name FROM Sub_Business_Units__c];

		// Test Logic
		Test.startTest();

			Goal__c oGoal = new Goal__c();
				oGoal.Name = 'TESTGOAL01';
				oGoal.Sub_Business_Unit__c = lstSBU[0].Id;
				oGoal.Active__c = true;
			insert oGoal;

			oGoal = [SELECT Id, Business_Unit_Group__c FROM Goal__c WHERE Id = :oGoal.Id];
			System.assertEquals(oGoal.Business_Unit_Group__c, lstSBU[0].Business_Unit__r.Business_Unit_Group__r.Name);

			Sub_Business_Units__c oSBU_New;
			for (Sub_Business_Units__c oSBU : lstSBU){
				
				if (oSBU.Business_Unit__r.Business_Unit_Group__r.Name != oGoal.Business_Unit_Group__c){
					oSBU_New = oSBU;
					break;
				}
			}

				oGoal.Sub_Business_Unit__c = oSBU_New.Id;
			update oGoal;

			oGoal = [SELECT Id, Business_Unit_Group__c FROM Goal__c WHERE Id = :oGoal.Id];
			System.assertEquals(oGoal.Business_Unit_Group__c, oSBU_New.Business_Unit__r.Business_Unit_Group__r.Name);

		Test.stopTest();

	}

}
//--------------------------------------------------------------------------------------------------------------------------------