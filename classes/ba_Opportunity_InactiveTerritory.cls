//---------------------------------------------------------------------------------------------------------------------------------------------------
//  Author		:    Bart Caelen
//  Date 		:    21/02/2018
//  Description	:    CR-14529
//						This Batch APEX will process all OPEN CVG Opportunities that are linked to an inactive Territory (Territory with no Active UserTerritory)
//						and it will update the owner of the Opportunity with the user which is linked to a Territory of the related Account and for which the
//						Territory of the Account is linked to at least 1 Therapy Group of the inactive Territory of the Opportunity.
//---------------------------------------------------------------------------------------------------------------------------------------------------
global class ba_Opportunity_InactiveTerritory implements Database.Batchable<sObject>, Schedulable, Database.AllowsCallouts, Database.Stateful {

	//------------------------------------------------------------------------
	// Batch APEX Variables
	//------------------------------------------------------------------------
	global Integer iBatchSize = 200;
	global String tAdditionalWhere = '';
	//------------------------------------------------------------------------

	//------------------------------------------------------------------------
	// Debug / Error Handling Variables
	//------------------------------------------------------------------------
	global Integer iUpdated_Opportunity = 0;
	global Integer iUpdated_OpportunityLineItem = 0;
	global Set<Id> setID_Opportunity_UpdateSuccess = new Set<Id>(); 
	global Map<Id, String> mapOpportunityId_UpdateFailed = new Map<Id, String>();
	global Set<Id> setID_OpportunityLineItem_UpdateSuccess = new Set<Id>(); 
	global Map<Id, String> mapOpportunityLineItemId_UpdateFailed = new Map<Id, String>();
	//------------------------------------------------------------------------


	//------------------------------------------------------------------------
	// SCHEDULE Settings
	//------------------------------------------------------------------------
	global void execute(SchedulableContext sc){ 

		// Set BatchSize
		ba_Opportunity_InactiveTerritory oBatch = new ba_Opportunity_InactiveTerritory();
		Database.executebatch(oBatch, iBatchSize);

	}
	//------------------------------------------------------------------------


	//------------------------------------------------------------------------
	// BATCH Settings
	//------------------------------------------------------------------------
	global Database.QueryLocator start(Database.BatchableContext BC) {

		// Process all OPEN CVG Opportunities based on the CVG RecordTypes
		String tSOQL = ' SELECT Id, Territory2Id, AccountId, Account.Name';
		tSOQL += ' FROM Opportunity';
		tSOQL += ' WHERE Territory2Id != null ';
		tSOQL += ' AND IsClosed = false';
		tSOQL += ' AND RecordType.DeveloperName in (\'CRDM_Service_Contract\', \'CVG_Project\')';
		tSOQL += tAdditionalWhere;
		tSOQL += ' ORDER BY Territory2Id';

		return Database.getQueryLocator(tSOQL);

	}

   	global void execute(Database.BatchableContext BC, List<Opportunity> lstOpportunity) {

   		// Get the Active Territory Id's of the processing Opportunities
		Set<Id> setID_Territory_Active = getActiveTerritoryIdForOpportunity(lstOpportunity);

		// Only process the Opportunities that are linked to an In-Active Territory (which has no active UserTerritories)
   		Set<Id> setID_Account = new Set<Id>();
   		Set<Id> setID_Territory_InActive = new Set<Id>();
   		for (Opportunity oOpportunity : lstOpportunity){

   			if (!setID_Territory_Active.contains(oOpportunity.Territory2Id)){

   				setID_Account.add(oOpportunity.AccountId);
   				setID_Territory_InActive.add(oOpportunity.Territory2Id);

   			}

   		}


		if (setID_Account.size() > 0){

	   		// For the processing Opportunities (which are related to an InActive Territory), get a related Active Account Territory based on a match on Therapy Groups
	   		Map<Id, Id> mapOpportunityID_ActiveAccountTerritoryId = getActiveAccountTerritoryIdForOpportunity(lstOpportunity, setID_Territory_InActive, setID_Account);

	   		if (mapOpportunityID_ActiveAccountTerritoryId.size() > 0){

		   		//---------------------------------------------------------------------------
		   		// Get the Active UserTerritory records of the collected Active Territories
		   		//---------------------------------------------------------------------------
				List<UserTerritory2Association> lstUserTerritory = 
					[
						SELECT 
							Id, UserId, Territory2Id, IsActive
						FROM 
							UserTerritory2Association
						WHERE 
							Territory2Id = :mapOpportunityID_ActiveAccountTerritoryId.values()
							AND IsActive = true
							AND User.isActive = true
					];

				Map<Id, Id> mapActiveTerritoryId_ActiveUserId = new Map<Id, Id>();
				for (UserTerritory2Association oUserTerritory : lstUserTerritory){

					if (!mapActiveTerritoryId_ActiveUserId.containsKey(oUserTerritory.Territory2Id)){
						mapActiveTerritoryId_ActiveUserId.put(oUserTerritory.Territory2Id, oUserTerritory.UserId);
					}
				}
		   		//---------------------------------------------------------------------------


		   		//---------------------------------------------------------------------------
		   		// Update Opportunities - update OwnerId and TerritoryId
		   		//---------------------------------------------------------------------------
				List<Opportunity> lstOpportunity_Update = new List<Opportunity>();
				for (Id id_Opportunity : mapOpportunityID_ActiveAccountTerritoryId.keySet()){

					Id id_ActiveAccountTerritory;
					Id id_ActiveUser;
					if (mapOpportunityID_ActiveAccountTerritoryId.containsKey(id_Opportunity)){
						id_ActiveAccountTerritory = mapOpportunityID_ActiveAccountTerritoryId.get(id_Opportunity);
						if (mapActiveTerritoryId_ActiveUserId.containsKey(id_ActiveAccountTerritory)){
							id_ActiveUser = mapActiveTerritoryId_ActiveUserId.get(id_ActiveAccountTerritory);
						}
					}
					
					if ( (id_ActiveAccountTerritory != null) && (id_ActiveUser != null)){

						Opportunity oOpportunity_Update = new Opportunity();
							oOpportunity_Update.Id = id_Opportunity;
							oOpportunity_Update.Territory2Id = id_ActiveAccountTerritory;
							oOpportunity_Update.OwnerId = id_ActiveUser;
						lstOpportunity_Update.add(oOpportunity_Update);

					}

				}

				if (lstOpportunity_Update.size() > 0){
					List<Database.SaveResult> lstSaveResult = Database.update(lstOpportunity_Update, false);

					for (Database.SaveResult oSaveResult : lstSaveResult) {
	
						if (oSaveResult.isSuccess()) {
	
							System.debug('** Successfully updated the owner of the Opportunity (' + oSaveResult.getId() + ')');
							setID_Opportunity_UpdateSuccess.add(oSaveResult.getId());

						}else{
	
							for (Database.Error oError : oSaveResult.getErrors()){
								String tError = 'The following error has occurred while updating the owner of the Opportunity (' + oSaveResult.getId() + ') : ';
								tError += oError.getStatusCode() + ': ' + oError.getMessage();
								tError += ' - Opportunity fields that affected this error: ' + oError.getFields();
								mapOpportunityId_UpdateFailed.put(oSaveResult.getId(), tError);
							}
	
						}
					}					
				}
		   		//---------------------------------------------------------------------------


		   		//---------------------------------------------------------------------------
		   		// Update OpportunityLineItems - update Who__c (=Opportunity.OnwerId)
		   		//---------------------------------------------------------------------------
				List<OpportunityLineItem> lstOpportunityLineItem = 
					[
						SELECT Id, Who__c, Opportunity.OwnerId
						FROM OpportunityLineItem
						WHERE OpportunityId = :lstOpportunity_Update
					];

				List<OpportunityLineItem> lstOpportunityLineItem_Update = new List<OpportunityLineItem>();
				for (OpportunityLineItem oOpportunityLineItem : lstOpportunityLineItem){

					if (oOpportunityLineItem.Who__c != oOpportunityLineItem.Opportunity.OwnerId){

						oOpportunityLineItem.Who__c = oOpportunityLineItem.Opportunity.OwnerId;
						lstOpportunityLineItem_Update.add(oOpportunityLineItem);

					}

				}

				if (lstOpportunityLineItem_Update.size() > 0){

					List<Database.SaveResult> lstSaveResult = Database.update(lstOpportunityLineItem_Update, false);

					for (Database.SaveResult oSaveResult : lstSaveResult) {
	
						if (oSaveResult.isSuccess()) {
	
							System.debug('** Successfully updated the Who of the OpportunityLineItem (' + oSaveResult.getId() + ')');
							setID_OpportunityLineItem_UpdateSuccess.add(oSaveResult.getId());
	
						}else{
	
							for (Database.Error oError : oSaveResult.getErrors()){
								String tError = 'The following error has occurred while updating the Who of the OpportunityLineItem (' + oSaveResult.getId() + ') : ';
								tError += oError.getStatusCode() + ': ' + oError.getMessage();
								tError += ' - OpportunityLineItem fields that affected this error: ' + oError.getFields();
								mapOpportunityLineItemId_UpdateFailed.put(oSaveResult.getId(), tError);
							}
	
						}
					}					

				}
		   		//---------------------------------------------------------------------------
	   		}

	   	}

	}


	global void finish(Database.BatchableContext BC) {

		System.debug('** ' + setID_Opportunity_UpdateSuccess.size() + ' Opportunity Updated Successfully : ' + setID_Opportunity_UpdateSuccess);
		System.debug('** ' + mapOpportunityId_UpdateFailed.size() + ' Opportunity Updates Failed : ' + mapOpportunityId_UpdateFailed);

		System.debug('** ' + setID_OpportunityLineItem_UpdateSuccess.size() + ' OpportunityLineItems Updated Successfully : ' + setID_OpportunityLineItem_UpdateSuccess);
		System.debug('** ' + mapOpportunityLineItemId_UpdateFailed.size() + ' OpportunityLineItem Updates Failed : ' + mapOpportunityLineItemId_UpdateFailed);

	}
	//------------------------------------------------------------------------


	//------------------------------------------------------------------------
	// Private methods
	//------------------------------------------------------------------------
	// Get all Active Territory Id's for the processing Opportunities
	private Set<Id> getActiveTerritoryIdForOpportunity(List<Opportunity> lstOpportunity){

   		// Get the Territory Id of the processing Opportunities
   		Set<Id> setID_Territory = new Set<Id>();
   		for (Opportunity oOpportunity : lstOpportunity){

   			setID_Territory.add(oOpportunity.Territory2Id);

   		}

   		// Get the Active UserTerritory records of the collected Territories to identify the Active Territories (and later on also the In-Active Territories)
		List<UserTerritory2Association> lstUserTerritory = 
			[
				SELECT 
					Territory2Id
				FROM 
					UserTerritory2Association
				WHERE 
					Territory2Id = :setID_Territory
					AND IsActive = true
					AND User.IsActive = true
			];

		Set<Id> setID_Territory_Active = new Set<Id>();
		for (UserTerritory2Association oUserTerritory : lstUserTerritory){

			setID_Territory_Active.add(oUserTerritory.Territory2Id);

		}
   		System.debug('** getActiveTerritoryIdForOpportunity - setID_Territory_Active (' + setID_Territory_Active.size() + ') : ' + setID_Territory_Active);

		return setID_Territory_Active;

	}

	
	// Get all Active Account Territory Id's for the processing Opportunities
	private Map<Id, Id> getActiveAccountTerritoryIdForOpportunity(List<Opportunity> lstOpportunity, Set<Id> setID_Territory_InActive, Set<Id> setID_Account){

		Map<Id, Id> mapOpportunityID_ActiveAccountTerritoryId = new Map<Id, Id>();

   		// Get the Therapy Groups of the In-Active Territories
   		Map<Id, Territory2> mapTerritory_InActive = new Map<Id, Territory2>(
   			[
   				SELECT Id, Therapy_Groups_Text__c
   				FROM Territory2
   				WHERE Id = :setID_Territory_InActive
   			]);

   		// Get the Active Territories of the processing Accounts
		Map<Id, Set<Territory2>> mapAccountID_ActiveTerritory = getActiveTerritoryForAccountId(setID_Account);

   		// Loop the processing Opportunities and only process the Opportunities that are linked to an In-Active Territory
   		for (Opportunity oOpportunity : lstOpportunity){

   			if (mapTerritory_InActive.containsKey(oOpportunity.Territory2Id)){

   				// Get the Therapy Groups of the In-Active Territory of the processing Opportunity
   				String tTherapyGroup_InactiveTerritory = clsUtil.isNull(mapTerritory_InActive.get(oOpportunity.Territory2Id).Therapy_Groups_Text__c, '');
   				List<String> lstTherapyGroup_InactiveTerritory = tTherapyGroup_InactiveTerritory.split(';');

		   		// Get the Active Territories of the Account related to the Processing Opportunity
   				Set<Territory2> setTerritory_Active = new Set<Territory2>();
   				if (mapAccountID_ActiveTerritory.containsKey(oOpportunity.AccountId)){
   					setTerritory_Active = mapAccountID_ActiveTerritory.get(oOpportunity.AccountId);
   				}

		   		// Loop through the Active Territories of the Account related to the processing Opportunities and search for a match between
		   		//	the Therapy Group of these Territories and the Therapy Group of the Inactive Territory of the processing Opportunity.
   				Boolean bFound = false;
   				for (Territory2 oTerritory : setTerritory_Active){

	   				String tTherapyGroup_ActiveTerritory = clsUtil.isNull(oTerritory.Therapy_Groups_Text__c, '');
	   				Set<String> setTherapyGroup_ActiveTerritory = new Set<String>();
	   				setTherapyGroup_ActiveTerritory.addAll(tTherapyGroup_ActiveTerritory.split(';'));

   					for (String tTherapyGroup : lstTherapyGroup_InactiveTerritory){

   						if (setTherapyGroup_ActiveTerritory.contains(tTherapyGroup)){

   							// This map contains the Id of the processing Opportunity and one of the matching Active Territories of the Account related to the processing Opportunity
   							mapOpportunityID_ActiveAccountTerritoryId.put(oOpportunity.Id, oTerritory.Id);	
   							bFound = true;
   							break;
   						}
   					
   					}

   					if (bFound) break;
   				}

   			}

   		}

   		return mapOpportunityID_ActiveAccountTerritoryId;

	}


	// Get all Active Territory Id's for the processing Account Id's
	private Map<Id, Set<Territory2>> getActiveTerritoryForAccountId(Set<Id> setID_Account){
						
		// Get the Territories for the prcoessing Accounts
		List<ObjectTerritory2Association> lstAcccountShare = 
			[SELECT ObjectId, Territory2Id, Territory2.Id, Territory2.Therapy_Groups_Text__c FROM ObjectTerritory2Association WHERE ObjectId = :setID_Account];
		
		Map<Id, List<Territory2>> allAccount_Territories = new Map<Id, List<Territory2>>();	
		Set<Id> territoryIds = new Set<Id>();		
		for(ObjectTerritory2Association accShare : lstAcccountShare){
			
			territoryIds.add(accShare.Territory2Id);
			
			List<Territory2> accTerritories = allAccount_Territories.get(accShare.ObjectId);
			
			if(accTerritories == null){
				
				accTerritories = new List<Territory2>();
				allAccount_Territories.put(accShare.ObjectId, accTerritories);
			}
			
			accTerritories.add(accShare.Territory2);
		}	
				
		Set<Id> setID_Territory_Active = new Set<Id>();		
		for (UserTerritory2Association oUserTerritory : [SELECT Territory2Id FROM UserTerritory2Association WHERE Territory2Id = :territoryIds AND IsActive = true AND User.IsActive = true]){
			
			setID_Territory_Active.add(oUserTerritory.Territory2Id);						
		}
		
		Map<Id, Set<Territory2>> mapAccountID_ActiveTerritory = new Map<Id, Set<Territory2>>();
		
		for (Id id_Account : setID_Account){
			
			//This Map contains all Active Territories of the Account
			Set<Territory2> setTerritory = new Set<Territory2>();
			mapAccountID_ActiveTerritory.put(id_Account, setTerritory);
				
			if (allAccount_Territories.containsKey(id_Account)){
								
				for (Territory2 accTerritory : allAccount_Territories.get(id_Account)){
					
					if (setID_Territory_Active.contains(accTerritory.Id)) setTerritory.add(accTerritory);		
					
				}
			}
		}

		return mapAccountID_ActiveTerritory;

	}
	//------------------------------------------------------------------------

}
//---------------------------------------------------------------------------------------------------------------------------------------------------