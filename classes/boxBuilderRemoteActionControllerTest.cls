@isTest
private class boxBuilderRemoteActionControllerTest{

    static testmethod void testSendSubmitEmail(){
    	
		User oUser_SystemAdmin = clsTestData_User.createUser_SystemAdministratorMDT('tstadm01', false);
			oUser_SystemAdmin.ManagerId = UserInfo.getUserId();
		insert oUser_SystemAdmin;
			    	
		System.runAs(oUser_SystemAdmin){

			Opportunity testOpp = clsTestData_Opportunity.createOpportunity()[0];

			Procedural_Solutions_Kit_Email__c oProceduralSolutionsKitEmail = new Procedural_Solutions_Kit_Email__c();
				oProceduralSolutionsKitEmail.Name = 'Test Medtronic Email';
				oProceduralSolutionsKitEmail.email__c = 'test@medtronic.com.test';
			insert oProceduralSolutionsKitEmail;
        
			Procedural_Solutions_Kit__c box = new Procedural_Solutions_Kit__c();
			box.Opportunity__c = testOpp.Id;
			box.Box_Description__c = 'Unit Test Box';
			box.Box__c = true; 
			box.Status__c = 'Open';
			box.Mobile_Id__c = 'unit_test_id';
			bl_ProceduralSolutionKit.saveProceduralSolutionKit(box);
        
			Integer emailbefore = Limits.getEmailInvocations();


			Test.startTest();
        
       			box.Status__c = 'Approval Pending';
				bl_ProceduralSolutionKit.saveProceduralSolutionKit(box); 
        
			Test.stopTest();

			// This Assert can't be executed because the Limits.getEmailInvocations() will always be 0 because of the Test.stopTest but we need the Test.stopTest because saveProceduralSolutionKit will send the email @future.
			// But the notes and attachments are also created in the same @future logic - we test if they are sucessfully created.
//			System.assertNotEquals(emailbefore,Limits.getEmailInvocations(),'email not sent');
        
			List<Note> notes = [Select Id from Note where parentId = :testOpp.Id];
			System.assert(notes.size() == 1);
        
			List<Attachment> attachments = [Select Id from Attachment where parentId = :testOpp.Id];
			System.assert(attachments.size() == 1);
		}

    }       
   /* 
    static testmethod void testSendShareEmail(){
    	
        Opportunity testOpp = new Opportunity(Name = 'TestOpp', StageName='Test', CloseDate = Date.today());
        insert testOpp;
        
        Account acc = new Account();
        acc.Name = 'Unit test account';
        insert acc;
        
        Contact cnt = new Contact();
        cnt.FirstName = 'Unit';
        cnt.LastName = 'Test';
        cnt.AccountId = acc.Id;
        cnt.Contact_Department__c = 'Oncology';		
		cnt.Contact_Primary_Specialty__c = 'Oncology'; 
		cnt.Affiliation_To_Account__c = 'Employee'; 
		cnt.Primary_Job_Title_vs__c = 'Director'; 
		cnt.Contact_Gender__c = 'male'; 
		cnt.email = 'unit.test@medtronic.com';
        insert cnt;
        
        Procedural_Solutions_Kit__c box = new Procedural_Solutions_Kit__c();
        box.Opportunity__c = testOpp.Id;
        box.Box_Description__c = 'Unit Test Box';
        box.Box__c = true; 
        insert box;
        
        Test.startTest();
        
        Integer emailbefore = Limits.getEmailInvocations();
       	
       	Map<String, String> params = new Map<String, String>();
        
        params.put('target_object_id', cnt.Id);
        params.put('what_id', box.Id);
        params.put('template_id', [Select Id from EmailTemplate where Name = 'share_template'].Id);
               
        boxBuilderRemoteActionController.sendShareEmail(params);

        System.assertNotEquals(emailbefore,Limits.getEmailInvocations(),'email not sent');
        Test.stopTest();
    }    
    
    private static testmethod void testRequestKitable(){
    	
    	Product2 prod = new Product2();
    	prod.Name = 'UT Product';
    	prod.KitableIndex__c = 'Not Approved';
    	insert prod;
    	
    	boxBuilderRemoteActionController.requestProductKitable(prod.Id);
    	
    	prod = [Select Id, KitableIndex__c from Product2 where Id = :prod.Id];
    	
    	System.assert(prod.KitableIndex__c == 'Approval Pending');
    }   
    
    private static testmethod void testMatchConfiguration(){
    	
    	Account acc = new Account();
        acc.Name = 'Unit test account';
        acc.Account_Country_vs__c = 'NETHERLANDS';
        insert acc;
    	    	    	
    	Opportunity testOpp = new Opportunity();
    	testOpp.Name = 'TestOpp';
    	testOpp.StageName='Identify';
    	testOpp.CloseDate = Date.today();
    	testOpp.AccountId = acc.Id;
        insert testOpp;
        
        Product2 prod = new Product2();
    	prod.Name = 'UT Product';
    	prod.KitableIndex__c = 'Not Approved';
    	insert prod;
                      
        Procedural_Solutions_Kit__c box = new Procedural_Solutions_Kit__c();
        box.Opportunity__c = testOpp.Id;
        box.Box_Description__c = 'Unit Test Box';
        box.Box__c = true; 
        box.Procedure__c = 'Bariatric';
        box.Country__c = 'NETHERLANDS';
        box.Status__c = 'Open';
        insert box;
        
        Procedural_Solutions_Kit_Product__c boxProd = new Procedural_Solutions_Kit_Product__c();
        boxProd.Procedural_Solutions_Kit__c = box.Id;
        boxProd.Product__c = prod.Id;
        boxProd.Quantity__c = 1;
        insert boxProd;
        
        box.Status__c = 'Submitted';
        update box;
        
        Test.startTest();
        
        Procedural_Solutions_Kit__c matchingBox = new Procedural_Solutions_Kit__c();
        matchingBox.Opportunity__c = testOpp.Id;
        matchingBox.Box_Description__c = 'Unit Test Matching Box';
        matchingBox.Box__c = true;        
        matchingBox.Country__c = 'NETHERLANDS';
        matchingBox.Status__c = 'Open';
        insert matchingBox;
        
        Procedural_Solutions_Kit_Product__c matchingBoxProd = new Procedural_Solutions_Kit_Product__c();
        matchingBoxProd.Procedural_Solutions_Kit__c = matchingBox.Id;
        matchingBoxProd.Product__c = prod.Id;
        matchingBoxProd.Quantity__c = 1;
        insert matchingBoxProd;
        
        String result = boxBuilderRemoteActionController.matchingConfigurations(box.Id, 'NETHERLANDS');
        
        System.assert(result.startsWith('There are 1 (1 in your country)'));
    	
    }
    */
}