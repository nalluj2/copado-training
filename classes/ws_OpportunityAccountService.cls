//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 19/11/2019
//  Description      : APEX Class - This class exposes methods to create / update / delete a Opportunity Account
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
@RestResource(urlMapping='/OpportunityAccountService/*')
global with sharing class ws_OpportunityAccountService {

	@HttpPost
	global static String doPost() {

 		RestRequest req = RestContext.request;
			
		System.debug('** ws_OpportunityAccountService - doPost - req : ' + req);
		System.debug('** ws_OpportunityAccountService - doPost - req.requestBody : ' + req.requestBody);
			
		String body = req.requestBody.toString();
		IFOpportunityAccountRequest opportunityAccountRequest = (IFOpportunityAccountRequest)System.Json.deserialize(body, IFOpportunityAccountRequest.class);
			
		System.debug('** ws_OpportunityAccountService - doPost - opportunityAccountRequest : ' + opportunityAccountRequest);
			
		Opportunity_Account__c opportunityAccount = opportunityAccountRequest.opportunityAccount;

		IFOpportunityAccountResponse resp = new IFOpportunityAccountResponse();

		//We catch all exception to assure that 
		try{
				
			resp.opportunityAccount = bl_Opportunity_Account.saveOpportunityAccount(opportunityAccount);
			resp.id = opportunityAccountRequest.id;
			resp.success = true;

		}catch (Exception e){
			
			resp.message = e.getMessage();
			resp.success = false;

		}
 
		bl_Mobilesync.storeRequest(UserInfo.getUserId(), 'OpportunityAccount', req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
  			
		return System.JSON.serialize(resp);

	}

	@HttpDelete
	global static String doDelete(){

		RestRequest req = RestContext.request;
			
		System.debug('** ws_OpportunityAccountService - doDelete - req : ' + req);
		System.debug('** ws_OpportunityAccountService - doDelete - req.requestBody : ' + req.requestBody);
			
		String body = req.requestBody.toString();
		IFOpportunityAccountDeleteRequest opportunityAccountDeleteRequest = (IFOpportunityAccountDeleteRequest)System.Json.deserialize(body, IFOpportunityAccountDeleteRequest.class);
			
		System.debug('** ws_OpportunityAccountService - doDelete - opportunityAccountDeleteRequest : ' + opportunityAccountDeleteRequest);
			
		Opportunity_Account__c opportunityAccount = opportunityAccountDeleteRequest.opportunityAccount;
		IFOpportunityAccountDeleteResponse resp = new IFOpportunityAccountDeleteResponse();
		
		List<Opportunity_Account__c> lstOpportunityAccount_Delete = [SELECT id, Mobile_ID__c FROM Opportunity_Account__c WHERE Mobile_ID__c = :opportunityAccount.Mobile_ID__c];
		
		try{
			
			if (lstOpportunityAccount_Delete !=null && lstOpportunityAccount_Delete.size()>0 ){	
				
				Opportunity_Account__c oOpportunityAccount_Delete = lstOpportunityAccount_Delete.get(0);
				delete oOpportunityAccount_Delete;
				resp.opportunityAccount = oOpportunityAccount_Delete;
				resp.id = opportunityAccountDeleteRequest.id;
				resp.success = true;
			}else{
				resp.opportunityAccount = opportunityAccount;
				resp.id = opportunityAccountDeleteRequest.id;
				resp.message='Opportunity Account not found';
				resp.success = true;
			}
			
		}catch (Exception e){
			
			resp.message=e.getMessage();
			resp.success=false;
		}

		bl_Mobilesync.storeRequest(UserInfo.getUserId(), 'OpportunityAccountDelete', req.requestBody.toString(), System.JSON.serializePretty(resp), resp.success);
			
		return System.JSON.serialize(resp);		

	}
	
	
	global class IFOpportunityAccountRequest{
		public Opportunity_Account__c opportunityAccount { get;set; }
		public String id { get;set; }
	}
	
	global class IFOpportunityAccountDeleteRequest{
		public Opportunity_Account__c opportunityAccount { get;set; }
		public String id { get;set; }
	}
		
	global class IFOpportunityAccountDeleteResponse{
		public Opportunity_Account__c opportunityAccount { get;set; }
		public String id { get;set; }
		public Boolean success { get;set; }
		public String message { get;set; }
	}

	global class IFOpportunityAccountResponse{
		public Opportunity_Account__c opportunityAccount { get;set; }
		public String id { get;set; }
		public Boolean success { get;set; }
		public String message { get;set; }
	}


}
//--------------------------------------------------------------------------------------------------------------------