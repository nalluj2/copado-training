//---------------------------------------------------------------------------------------------------------------------------------
//	Author 			: Bart Caelen
//  Created Date 	: 15-02-2019
//  Description 	: APEX TEST Class for bl_ContactCSODIntegration
//  Change Log 		: 
//---------------------------------------------------------------------------------------------------------------------------------
@isTest  private class TEST_bl_ContactCSODIntegration {


	@isTest
	private static void doCSODIntegration_EmployeeInCSOD() {

		// Load Test Data
		Contact oContact = loadContact();
		Campaign oCampaign = loadCampaign();

		Contact_ED_Sync__c oSync = new Contact_ED_Sync__c();
			oSync.Contact__c = oContact.Id;
			oSync.Campaign__c = oCampaign.Id;
		insert oSync;

		oSync = [SELECT Contact__c, Campaign__c, Campaign__r.Curriculum_ID__c, Outcome__c, Outcome_Details__c FROM Contact_ED_Sync__c];
		Contact oContact_Result;


		// Execute Test
		Test.startTest();

			TEST_CSODIntegration_Mock oMock = new TEST_CSODIntegration_Mock();
				oMock.bEmployeeInCSOD = true;
				oMock.bError = false;
	
			Test.setMock(HttpCalloutMock.class, oMock);

			oContact_Result = bl_ContactCSODIntegration.doCSODIntegration(oContact, oSync);

		Test.stopTest();


		// Validate Test Result
	
	}

	@isTest
	private static void doCSODIntegration_EmployeeNotInCSOD() {

		// Load Test Data
		Contact oContact = loadContact();
		Campaign oCampaign = loadCampaign();

		Contact_ED_Sync__c oSync = new Contact_ED_Sync__c();
			oSync.Contact__c = oContact.Id;
			oSync.Campaign__c = oCampaign.Id;
		insert oSync;

		oSync = [SELECT Contact__c, Campaign__c, Campaign__r.Curriculum_ID__c, Outcome__c, Outcome_Details__c FROM Contact_ED_Sync__c];
		Contact oContact_Result;


		// Execute Test
		Test.startTest();

			TEST_CSODIntegration_Mock oMock = new TEST_CSODIntegration_Mock();
				oMock.bEmployeeInCSOD = false;
				oMock.bError = false;
	
			Test.setMock(HttpCalloutMock.class, oMock);

			oContact_Result = bl_ContactCSODIntegration.doCSODIntegration(oContact, oSync);

		Test.stopTest();


		// Validate Test Result
	
	}

	@isTest
	private static void getEmployee() {

		// Load Test Data
		Contact oContact = loadContact();
		bl_ContactCSODIntegration.employeeResults oResponse;


		// Execute Test
		Test.startTest();

			TEST_CSODIntegration_Mock oMock = new TEST_CSODIntegration_Mock();
				oMock.bError = false;
	
			Test.setMock(HttpCalloutMock.class, oMock);

			oResponse = bl_ContactCSODIntegration.getEmployee(oContact.LMS_Userid__c);

		Test.stopTest();


		// Validate Test Result
		System.assert(!String.isBlank(oResponse.externalUserId));

	}

	@isTest
	private static void createEmployee() {
	
		// Load Test Data
		Contact oContact = loadContact();
		bl_ContactCSODIntegration.createEmployeeResult oResponse;


		// Execute Test
		Test.startTest();

			TEST_CSODIntegration_Mock oMock = new TEST_CSODIntegration_Mock();
				oMock.bError = false;
	
			Test.setMock(HttpCalloutMock.class, oMock);

			oResponse = bl_ContactCSODIntegration.createEmployee(oContact);

		Test.stopTest();


		// Validate Test Result
		System.assert(!String.isBlank(oResponse.externalUserId));
		System.assert(!String.isBlank(oResponse.timestamp));

	}

	@isTest
	private static void createEmployeeManager() {
	
		// Load Test Data
		Contact oContact = loadContact();
		bl_ContactCSODIntegration.createEmployeeResult oResponse;


		// Execute Test
		Test.startTest();

			TEST_CSODIntegration_Mock oMock = new TEST_CSODIntegration_Mock();
				oMock.bError = false;
	
			Test.setMock(HttpCalloutMock.class, oMock);

			oResponse = bl_ContactCSODIntegration.createEmployeeManager(oContact);

		Test.stopTest();


		// Validate Test Result
		System.assert(!String.isBlank(oResponse.externalUserId));
		System.assert(!String.isBlank(oResponse.timestamp));

	}

	@isTest
	private static void updateEmployee() {
	
		// Load Test Data
		Contact oContact = loadContact();
		bl_ContactCSODIntegration.updateEmployeeResult oResponse;


		// Execute Test
		Test.startTest();

			TEST_CSODIntegration_Mock oMock = new TEST_CSODIntegration_Mock();
				oMock.bError = false;
	
			Test.setMock(HttpCalloutMock.class, oMock);

			oResponse = bl_ContactCSODIntegration.updateEmployee(oContact, new bl_ContactCSODIntegration.employeeResults());

		Test.stopTest();


		// Validate Test Result
		System.assert(!String.isBlank(oResponse.externalUserId));

	}

	@isTest
	private static void createTrainingAssignment() {
	
		// Load Test Data
		Contact oContact = loadContact();
		Campaign oCampaign = loadCampaign();

		bl_ContactCSODIntegration.trainingAssignmentResult oResponse;


		// Execute Test
		Test.startTest();

			TEST_CSODIntegration_Mock oMock = new TEST_CSODIntegration_Mock();
				oMock.bError = false;
	
			Test.setMock(HttpCalloutMock.class, oMock);

			oResponse = bl_ContactCSODIntegration.createTrainingAssignment(oCampaign.Curriculum_ID__c, oContact.LMS_Userid__c);

		Test.stopTest();


		// Validate Test Result
		System.assert(!String.isBlank(oResponse.status));
		System.assert(!String.isBlank(oResponse.Reason));

	}

	@isTest
	private static void bEmployeeUpdateNeeded() {

		// Load Test Data
		Contact oContact = loadContact();

		Boolean bUpdateNeeded = false;


		// Execute Test
		Test.startTest();

			TEST_CSODIntegration_Mock oMock = new TEST_CSODIntegration_Mock();
				oMock.bError = false;
	
			Test.setMock(HttpCalloutMock.class, oMock);

			bl_ContactCSODIntegration.employeeResults oEmployeeResponse1 = bl_ContactCSODIntegration.getEmployee(oContact.LMS_Userid__c);
			bl_ContactCSODIntegration.EmployeeRequest oEmployeeRequest2 = new bl_ContactCSODIntegration.EmployeeRequest();//bl_ContactCSODIntegration.createEmployeeRequest(oContact);
					oEmployeeRequest2.userIdentifier = oEmployeeResponse1.userIdentifier;
					oEmployeeRequest2.userName = oEmployeeResponse1.userName;
					oEmployeeRequest2.firstName = oEmployeeResponse1.firstName;
					oEmployeeRequest2.lastName = oEmployeeResponse1.lastName;
					oEmployeeRequest2.prefix = oEmployeeResponse1.prefix;
					oEmployeeRequest2.primaryEmail = oEmployeeResponse1.primaryEmail;
					oEmployeeRequest2.countryName = oEmployeeResponse1.countryName;
					oEmployeeRequest2.isActive = oEmployeeResponse1.isActive;
					oEmployeeRequest2.displayLanguage = oEmployeeResponse1.displayLanguage;
					oEmployeeRequest2.divisionId = oEmployeeResponse1.divisionId;
					oEmployeeRequest2.hcpNumber = oEmployeeResponse1.hcpNumber;
					oEmployeeRequest2.learningManagerUserId = oEmployeeResponse1.learningManagerUserId;
					oEmployeeRequest2.learningManagerFirstName = oEmployeeResponse1.learningManagerFirstName;
					oEmployeeRequest2.learningManagerLastName = oEmployeeResponse1.learningManagerLastName;
					oEmployeeRequest2.learningManagerEmail = oEmployeeResponse1.learningManagerEmail;
					oEmployeeRequest2.learningManagerPhone = oEmployeeResponse1.learningManagerPhone;
					oEmployeeRequest2.learningManagerPhone = 'TEST225566'; 

			bUpdateNeeded = bl_ContactCSODIntegration.bEmployeeUpdateNeeded(oEmployeeResponse1, oEmployeeRequest2);

		Test.stopTest();


		// Validate Test Result
		System.assert(bUpdateNeeded);
	
	}

	@isTest
	private static void oAPISettings() {

		// Load Test Data
		Contact oContact = loadContact();
		Campaign oCampaign = loadCampaign();


		// Execute Test
		Test.startTest();

			CSOD_API_Settings__c oAPISettings = bl_ContactCSODIntegration.oAPISettings;

		Test.stopTest();


		// Validate Test Result
		System.assert(oAPISettings != null);
	
	}


	private static Contact loadContact(){
	
		Contact oContact = 
			[
				SELECT 
					Id, FirstName, LastName, LMS_Country__c, LMS_Language__c, LMS_Division__c, LMS_IsActive__c, Email, LMS_Userid__c, Title, MailingCountry, Identification_Number__c, LMS_Activation_Date__c
					, LMS_Key_Customer_Contact__r.Alias_unique__c, LMS_Key_Customer_Contact__r.FirstName, LMS_Key_Customer_Contact__r.LastName, LMS_Key_Customer_Contact__r.Title, LMS_Key_Customer_Contact__r.Email, LMS_Key_Customer_Contact__r.Phone, LMS_Key_Customer_Contact__r.Account.Account_Country_vs__c
					, (Select Id, Status, Campaign.LMS_ED_Group__c from CampaignMembers where Status IN ('Start enrollment', 'Enrolled') AND Campaign.RecordType.DeveloperName = 'LEARNING' AND Campaign.LMS_ED_Group__c != null)
				FROM 
					Contact
				LIMIT 1
			];

		return oContact;
	}

	private static Campaign loadCampaign(){
		
		Campaign oCampaign =
			[
				SELECT Id, Curriculum_ID__c
				FROM Campaign
				LIMIT 1
			];

		return oCampaign;
	
	}



	@TestSetup
	private static void generateData(){
						
		// Create Master Data
		List<DIB_Country__c> lstCountry = clsTestData_MasterData.createCountry();
		System.debug('**BC** lstCountry (' + lstCountry.size() + ') : ' + lstCountry);

		
		// Create Custom Setting Data
		clsTestData_CustomSetting.mapCampaignMemberStatus_Responded = new Map<String, Boolean>{'Start enrollment'=>true};
		clsTestData_CustomSetting.createCampaignMemberStatus(true, 'RecordTypeID', clsUtil.getRecordTypeByDevName('Campaign', 'LEARNING').Id);

		CSOD_API_Settings__c oCSODAPISettings = new CSOD_API_Settings__c();
			oCSODAPISettings.Token_Authentication__c = '123546789';
			oCSODAPISettings.Org_Id__c = UserInfo.getOrganizationId();
			oCSODAPISettings.Target_Server_URL__c = 'https://fakeURL.com';		
			oCSODAPISettings.Timeout__c = 60000;
			oCSODAPISettings.Maximum_Records__c = 50;
			oCSODAPISettings.Target_server_URL_PATCH__c = 'https://fakeURL.com';
			oCSODAPISettings.Token_Authentication_PATCH__c = '123546789';
			oCSODAPISettings.Active__c = true;
		insert oCSODAPISettings;

        ED_API_Settings__c oEDAPISettings = new ED_API_Settings__c();
			oEDAPISettings.API_Key__c = '987654321';
			oEDAPISettings.Token_Authentication__c = '123546789';
			oEDAPISettings.Org_Id__c = UserInfo.getOrganizationId();
			oEDAPISettings.Default_Password__c = 'Medtronic123';
			oEDAPISettings.Target_Server_URL__c  = 'https://fakeURL.com';		
		insert oEDAPISettings; 


		// Create Account Data
		Account oAccount = new Account();
			oAccount.Name = 'Test Account';
			oAccount.Account_Country_vs__c = 'NETHERLANDS';
		insert oAccount;
		

		// Create Contact Data
		Contact oContact_MDTEmployee = clsTestData_Contact.createMDTEmployee(false)[0];
			oContact_MDTEmployee.Contact_Active__c = true;
		insert oContact_MDTEmployee;

		Contact oContact = new Contact();
			oContact.AccountId = oAccount.Id;
			oContact.FirstName = 'Unit';
			oContact.LastName = 'Test';
			oContact.Email = 'unit.test@medtronic.com';
			oContact.Contact_Department__c = 'Diabetes Adult'; 
			oContact.Contact_Primary_Specialty__c = 'ENT';
			oContact.Affiliation_To_Account__c = 'Employee';
			oContact.Primary_Job_Title_vs__c = 'Manager';
			oContact.Contact_Gender__c = 'Male';
			oContact.LMS_Country__c = 'NL';
			oContact.LMS_Language__c = 'nl-NL';
			oContact.LMS_Key_Customer_Contact__c = oContact_MDTEmployee.Id;
			oContact.RecordTypeId = clsUtil.getRecordTypeByDevName('Contact', 'Generic_Contact').Id;
		insert oContact;
		

		// Create Campaign + Campaign Member Data
		Campaign oCampaign = new Campaign();		
			oCampaign.Name = 'TEST CAMPAIGN LMS';
			oCampaign.Type = 'Training & Education';
			oCampaign.RecordTypeID = clsUtil.getRecordTypeByDevName('Campaign', 'LEARNING').Id;
			oCampaign.Curriculum_ID__c = 'TEST Curriculum ID';
			oCampaign.IsActive = true;
        insert oCampaign;
                    
		CampaignMember oCampaignMember = new CampaignMember();
			oCampaignMember.ContactId = oContact.Id;
			oCampaignMember.Status = 'Start enrollment';
			oCampaignMember.CampaignId = oCampaign.Id;        
        insert oCampaignMember;
        
	}    
	
}
//---------------------------------------------------------------------------------------------------------------------------------