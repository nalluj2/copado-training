//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 04-12-2015
//  Description      : APEX Test Class to test the logic in bl_NotificationSAP and ba_NotificationSAP
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_bl_NotificationSAP {

    //----------------------------------------------------------------------------------------
    // CREATE TEST DATA
    //----------------------------------------------------------------------------------------
    @testSetup static void createTestData() {

        Test.startTest();

        User oUser = clsTestData_User.createUser_SystemAdministrator('tadm000', true);

        System.runAs(oUser){

            // Create Custom Setting - WebService Setting Data
            clsTestData.createCustomSettingData_WebServiceSetting(true);

            // Create Case Data
            clsTestData.createCaseData();

            // Create Attachment Data
            clsTestData.idAttachment_Parent = clsTestData.oMain_Case.Id;
            clsTestData.createAttachmentData();

            // Create Service Order Data
            clsTestData.createSVMXCServiceOrderData(true);

            // Create Service Order Line Data
            clsTestData.createSVMXCServiceOrderLineData(true);


            // Create Installed Product Data
            clsTestData.createSVMXCInstalledProductData(true);

        }

        Test.stopTest();

    }
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
    // TEST EXCEPTION HANDLING
    //----------------------------------------------------------------------------------------
    @isTest static void test_bl_NotificationSAP_ErrorHandling() {
        
        // Load Test Data
        List<Case> lstData = [SELECT Id, SVMX_SAP_Notification_Number__c FROM Case];

        // Load User Data
        User oUser = [SELECT Id FROM User WHERE Alias_Unique__c = 'tadm000' LIMIT 1];

        Test.startTest();

            // Set mock callout class 
            Test.setMock(HttpCalloutMock.class, new TEST_bl_NotificationSAP_Mock());  

            // Process Collected Campaigns
            System.runAs(oUser){
                clsUtil.clearAllHasExceptions();
                clsUtil.hasException1 = true;
                try{
                    bl_NotificationSAP.sendNotificationToSAP_Case(lstData, '');
                }catch(Exception oEX){
                    clsUtil.debug('test_bl_NotificationSAP_ErrorHandling - hasException1 : ' + oEX.getMessage());
                }

                clsUtil.clearAllHasExceptions();
                clsUtil.hasException2 = true;
                try{
                    bl_NotificationSAP.sendNotificationToSAP_Case(lstData, '');
                }catch(Exception oEX){
                    clsUtil.debug('test_bl_NotificationSAP_ErrorHandling - hasException2 : ' + oEX.getMessage());
                }

                clsUtil.clearAllHasExceptions();
                clsUtil.hasException3 = true;
                try{
                    bl_NotificationSAP.sendNotificationToSAP_Case(lstData, '');
                }catch(Exception oEX){
                    clsUtil.debug('test_bl_NotificationSAP_ErrorHandling - hasException3 : ' + oEX.getMessage());
                }

                clsUtil.clearAllHasExceptions();
                clsUtil.hasException4 = true;
                try{
                    bl_NotificationSAP.sendNotificationToSAP_Case(lstData, '');
                }catch(Exception oEX){
                    clsUtil.debug('test_bl_NotificationSAP_ErrorHandling - hasException4 : ' + oEX.getMessage());
                }

                clsUtil.clearAllHasExceptions();
                clsUtil.hasException2 = true;
                try{
                    bl_NotificationSAP.callWebMethod(new bl_NotificationSAP.NotificationSAP());
                }catch(Exception oEX){
                    clsUtil.debug('test_bl_NotificationSAP_ErrorHandling - hasException2 in callWebMethod : ' + oEX.getMessage());
                }

            }

        Test.stopTest();
        
    }
    //----------------------------------------------------------------------------------------

	
    //----------------------------------------------------------------------------------------
    // TEST CASE RECORDS
    //----------------------------------------------------------------------------------------
	@isTest static void test_bl_NotificationSAP_Case_Success() {
		
        // Load Test Data
        List<Case> lstData = [SELECT Id, SVMX_SAP_Notification_Number__c FROM Case LIMIT 1];

        // Load User Data
        User oUser = [SELECT Id FROM User WHERE Alias_Unique__c = 'tadm000' LIMIT 1];


        Test.startTest();

            // Set mock callout class 
            Test.setMock(HttpCalloutMock.class, new TEST_bl_NotificationSAP_Mock());  

            // Process Collected Campaigns
            System.runAs(oUser){
                bl_NotificationSAP.sendNotificationToSAP_Case(lstData, '');
            }

        Test.stopTest();
        
	}
	
    @isTest static void test_bl_NotificationSAP_Case_Failure() {
        
        // Load Test Data
        List<Case> lstData = [SELECT Id, SVMX_SAP_Notification_Number__c FROM Case LIMIT 1];

        // Load User Data
        User oUser = [SELECT Id FROM User WHERE Alias_Unique__c = 'tadm000' LIMIT 1];


        Test.startTest();

            // Set mock callout class 
            Test.setMock(HttpCalloutMock.class, new TEST_bl_NotificationSAP_Mock(500, 'ERROR', new Map<String, String>(), 'FALSE', 'Error occured while processing record'));  

            // Process Collected Campaigns
            System.runAs(oUser){
                bl_NotificationSAP.sendNotificationToSAP_Case(lstData, '');
            }

        Test.stopTest();
        
    }

    @isTest static void test_bl_NotificationSAP_Case_Failure_Bad_Response() {
        
        // Load Test Data
        List<Case> lstData = [SELECT Id, SVMX_SAP_Notification_Number__c FROM Case LIMIT 1];

        // Load User Data
        User oUser = [SELECT Id FROM User WHERE Alias_Unique__c = 'tadm000' LIMIT 1];


        Test.startTest();

            // Set mock callout class 
            Test.setMock(HttpCalloutMock.class, new TEST_bl_NotificationSAP_Mock(200, 'ERROR_BAD_RESPONSE', new Map<String, String>(), 'true', ''));  

            // Process Collected Campaigns
            System.runAs(oUser){
                bl_NotificationSAP.sendNotificationToSAP_Case(lstData, '');
            }

        Test.stopTest();
        
    }    
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
    // TEST SVMXC__Service_Order__c RECORDS
    //----------------------------------------------------------------------------------------
    @isTest static void test_bl_NotificationSAP_SVMXCServiceOrder_Success() {
        
        // Load Test Data
        List<SVMXC__Service_Order__c> lstData = [SELECT Id, SVMX_SAP_Service_Order_No__c FROM SVMXC__Service_Order__c LIMIT 1];

        // Load User Data
        User oUser = [SELECT Id FROM User WHERE Alias_Unique__c = 'tadm000' LIMIT 1];


        Test.startTest();

            // Set mock callout class 
            Test.setMock(HttpCalloutMock.class, new TEST_bl_NotificationSAP_Mock());  

            // Process Collected Campaigns
            System.runAs(oUser){
                bl_NotificationSAP.sendNotificationToSAP_ServiceOrder(lstData, '');
            }

        Test.stopTest();
        
    }
    
    @isTest static void test_bl_NotificationSAP_SVMXCServiceOrder_Failure() {
        
        // Load Test Data
        List<SVMXC__Service_Order__c> lstData = [SELECT Id, SVMX_SAP_Service_Order_No__c FROM SVMXC__Service_Order__c LIMIT 1];

        // Load User Data
        User oUser = [SELECT Id FROM User WHERE Alias_Unique__c = 'tadm000' LIMIT 1];


        Test.startTest();

            // Set mock callout class 
            Test.setMock(HttpCalloutMock.class, new TEST_bl_NotificationSAP_Mock(500, 'NOK', new Map<String, String>(), 'FALSE', 'Error occured while processing record'));  

            // Process Collected Campaigns
            System.runAs(oUser){
                bl_NotificationSAP.sendNotificationToSAP_ServiceOrder(lstData, '');
            }

        Test.stopTest();
        
    }
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
    // TEST SVMXC__Service_Order__c COMPLETED RECORDS
    //----------------------------------------------------------------------------------------
    @isTest static void test_bl_NotificationSAP_SVMXCServiceOrder_Completed_Success() {
        
        // Load Test Data
        List<SVMXC__Service_Order__c> lstData = [SELECT Id, SVMX_SAP_Service_Order_No__c FROM SVMXC__Service_Order__c LIMIT 1];

        // Load User Data
        User oUser = [SELECT Id FROM User WHERE Alias_Unique__c = 'tadm000' LIMIT 1];


        Test.startTest();

            // Set mock callout class 
            Test.setMock(HttpCalloutMock.class, new TEST_bl_NotificationSAP_Mock());  

            // Process Collected Campaigns
            System.runAs(oUser){
                bl_NotificationSAP.sendNotificationToSAP_ServiceOrder_Completed(lstData, '');
            }

        Test.stopTest();
        
    }
    
    @isTest static void test_bl_NotificationSAP_SVMXCServiceOrder_Completed_Failure() {
        
        // Load Test Data
        List<SVMXC__Service_Order__c> lstData = [SELECT Id, SVMX_SAP_Service_Order_No__c FROM SVMXC__Service_Order__c LIMIT 1];

        // Load User Data
        User oUser = [SELECT Id FROM User WHERE Alias_Unique__c = 'tadm000' LIMIT 1];


        Test.startTest();

            // Set mock callout class 
            Test.setMock(HttpCalloutMock.class, new TEST_bl_NotificationSAP_Mock(500, 'NOK', new Map<String, String>(), 'FALSE', 'Error occured while processing record'));  

            // Process Collected Campaigns
            System.runAs(oUser){
                bl_NotificationSAP.sendNotificationToSAP_ServiceOrder_Completed(lstData, '');
            }

        Test.stopTest();
        
    }
    //----------------------------------------------------------------------------------------

	
    //----------------------------------------------------------------------------------------
    // TEST Attachment RECORDS
    //----------------------------------------------------------------------------------------
    @isTest static void test_bl_NotificationSAP_Attachment_Success() {
        
        // Load Test Data
        List<Attachment> lstData = [SELECT Id FROM Attachment LIMIT 1];

        // Load User Data
        User oUser = [SELECT Id FROM User WHERE Alias_Unique__c = 'tadm000' LIMIT 1];


        Test.startTest();

            // Set mock callout class 
            Test.setMock(HttpCalloutMock.class, new TEST_bl_NotificationSAP_Mock());  

            // Process Collected Campaigns
            System.runAs(oUser){
                bl_NotificationSAP.sendNotificationToSAP_Attachment(lstData, '');
            }

        Test.stopTest();
        
    }
    
    @isTest static void test_bl_NotificationSAP_Attachment_Failure() {
        
        // Load Test Data
        List<Attachment> lstData = [SELECT Id FROM Attachment LIMIT 1];

        // Load User Data
        User oUser = [SELECT Id FROM User WHERE Alias_Unique__c = 'tadm000' LIMIT 1];


        Test.startTest();

            // Set mock callout class 
            Test.setMock(HttpCalloutMock.class, new TEST_bl_NotificationSAP_Mock(500, 'NOK', new Map<String, String>(), 'FALSE', 'Error occured while processing record'));  

            // Process Collected Campaigns
            System.runAs(oUser){
                bl_NotificationSAP.sendNotificationToSAP_Attachment(lstData, '');
            }

        Test.stopTest();
        
    }
    //----------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------
    // TEST Installed Product RECORDS
    //----------------------------------------------------------------------------------------
    @isTest static void test_bl_NotificationSAP_InstalledProduct_Success() {
        
        // Load Test Data
        List<SVMXC__Installed_Product__c> lstData = [SELECT Id FROM SVMXC__Installed_Product__c LIMIT 1];

        // Load User Data
        User oUser = [SELECT Id FROM User WHERE Alias_Unique__c = 'tadm000' LIMIT 1];


        Test.startTest();

            // Set mock callout class 
            Test.setMock(HttpCalloutMock.class, new TEST_bl_NotificationSAP_Mock());  

            // Process Collected Campaigns
            System.runAs(oUser){
                bl_NotificationSAP.sendNotificationToSAP_InstalledProduct(lstData, '');
            }

        Test.stopTest();
        
    }
    
    @isTest static void test_bl_NotificationSAP_InstalledProduct_Failure() {
        
        // Load Test Data
        List<SVMXC__Installed_Product__c> lstData = [SELECT Id FROM SVMXC__Installed_Product__c LIMIT 1];

        // Load User Data
        User oUser = [SELECT Id FROM User WHERE Alias_Unique__c = 'tadm000' LIMIT 1];


        Test.startTest();

            // Set mock callout class 
            Test.setMock(HttpCalloutMock.class, new TEST_bl_NotificationSAP_Mock(500, 'NOK', new Map<String, String>(), 'FALSE', 'Error occured while processing record'));  

            // Process Collected Campaigns
            System.runAs(oUser){
                bl_NotificationSAP.sendNotificationToSAP_InstalledProduct(lstData, '');
            }

        Test.stopTest();
        
    }
    //----------------------------------------------------------------------------------------
}
//------------------------------------------------------------------------------------------------------------------------