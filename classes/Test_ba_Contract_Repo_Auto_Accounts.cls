@isTest
private class Test_ba_Contract_Repo_Auto_Accounts {
    
    @TestSetup
    private static void createContract(){

        List<Account> listAccount = new List<Account>();
        Id devRecordTypeId = clsUtil.GetRecordTypeByDevName('Account','Buying_Entity').Id;
        
        //Create new Account with type Buying Group
        listAccount.add(new Account(name='Test Buying Group Belgium DS', RecordTypeId=devRecordTypeId, Type='Buying Group', Account_Country_vs__c='BELGIUM'));
        //Create new Account with type Group Purchasing Organization
        listAccount.add(new Account(name='Test GPO Belgium DS', RecordTypeId=devRecordTypeId, Type='Group Purchasing Organization', Account_Country_vs__c='BELGIUM'));
        //Create new Account with type Group Purchasing Organization
        listAccount.add(new Account(name='Test Sub GPO Belgium DS', RecordTypeId=devRecordTypeId, Type='Group Purchasing Organization', Account_Country_vs__c='BELGIUM'));
        
        Id RecordTypeIdSAPAccount = clsUtil.GetRecordTypeByDevName('Account','SAP_Account').Id;
       
        //Create new Account with type Group Purchasing Organization
        listAccount.add(new Account(name='UZ Leuven', RecordTypeId=RecordTypeIdSAPAccount, Type='Public Hospital', Account_Country_vs__c='BELGIUM',SAP_Account_Type__c='Sold-to party'));
        listAccount.add(new Account(name='Test UZ Leuven', RecordTypeId=RecordTypeIdSAPAccount, Type='Public Hospital', Account_Country_vs__c='BELGIUM',SAP_Account_Type__c='Sold-to party'));
            
        insert listAccount;
        
        List<Affiliation__c> listAffiliation = new List<Affiliation__c>();
        
        //Create Relationship with start date in the past and no end date
        //Account BU flags Vascular and Diabetes will be set by batch
        Affiliation__c curAff = new Affiliation__c(Affiliation_From_Account__c = [select Id, Name from Account where Name = 'Test GPO Belgium DS'].Id);
        curAff.Affiliation_Type__c = 'SubGPO/GPO';
        curAff.Affiliation_To_Account__c = [select Id from Account where Name = 'Test Buying Group Belgium DS'].Id;
        curAff.Business_Unit_Group_vs__c = 'CVG; Diabetes';
        curAff.Business_Unit_vs__c = 'Diabetes; Vascular';
        curAff.Sub_Business_Unit_vs__c = 'Coro + PV; Diabetes Core; HCC';
        curAff.Affiliation_Start_Date__c = System.Today().addYears(-1);
        curAff.Affiliation_End_Date__c = System.Today().addYears(1);
        
        listAffiliation.add(curAff);
        
        curAff = new Affiliation__c(Affiliation_From_Account__c = [select Id, Name from Account where Name = 'Test Sub GPO Belgium DS'].Id);
        curAff.Affiliation_Type__c = 'SubGPO/GPO';
        curAff.Affiliation_To_Account__c = [select Id from Account where Name = 'Test Buying Group Belgium DS'].Id;
        curAff.Business_Unit_Group_vs__c = 'CVG; Diabetes';
        curAff.Business_Unit_vs__c = 'Diabetes; Vascular';
        curAff.Sub_Business_Unit_vs__c = 'Coro + PV; Diabetes Core; HCC';
        curAff.Affiliation_Start_Date__c = System.Today().addYears(-1);
        curAff.Affiliation_End_Date__c = System.Today().addYears(1);
        
        listAffiliation.add(curAff);
        
        //Create Relationship with start date and end date in the past
        //No Account BU flags will be set by batch
        curAff = new Affiliation__c(Affiliation_From_Account__c = [select Id, Name from Account where Name = 'UZ Leuven'].Id);
        curAff.Affiliation_Type__c = 'SubGPO/GPO';
        curAff.Affiliation_To_Account__c = [select Id from Account where Name = 'Test GPO Belgium DS'].Id;
        curAff.Business_Unit_Group_vs__c = 'Restorative';
        curAff.Business_Unit_vs__c = 'Pain';
        curAff.Sub_Business_Unit_vs__c = 'Pain Stim; TDD';
        curAff.Affiliation_Start_Date__c = System.Today().addYears(-1);
        curAff.Affiliation_End_Date__c = System.Today().addYears(1);
        
        listAffiliation.add(curAff);
        
        //Create Relationship with start date in the future and no end date
        //No Account BU flags will be set by batch
        curAff = new Affiliation__c(Affiliation_From_Account__c = [select Id, Name from Account where Name = 'Test UZ Leuven'].Id);
        curAff.Affiliation_Type__c = 'SubGPO/GPO';
        curAff.Affiliation_To_Account__c = [select Id from Account where Name = 'Test Buying Group Belgium DS'].Id;
        curAff.Business_Unit_Group_vs__c = 'MITG';
        curAff.Business_Unit_vs__c = 'Robotics';
        curAff.Sub_Business_Unit_vs__c = 'Robotics';
        curAff.Affiliation_Start_Date__c = System.Today().addYears(-2);
        curAff.Affiliation_End_Date__c = System.Today().addYears(-1);
        
        listAffiliation.add(curAff);
        
        insert listAffiliation;
        
        Contract_Repository__c contract = new Contract_Repository__c();
        contract.Account__c = [select Id from Account where Name = 'Test Buying Group Belgium DS'].Id;
        contract.Contract_nr__c = '11111111';
        contract.Primary_Contract_Type__c = 'Rebate agreement';
        contract.Rebate_Type__c = 'Straight Unit';
        contract.MPG_Code__c = 'A1';
        contract.Payment_Options__c = 'Credit Note';
        contract.Pre_Payment_Terms__c = 'Final Payment';
        contract.Payment_Period__c = 'Monthly';     
        contract.Original_Valid_From_Date__c = Date.today();
        contract.Original_Valid_To_Date__c = Date.today().addYears(1);      
        insert contract;
    } 
     
    @isTest static void testConRepoAccountCreation(){
    
        Test.startTest();
            ba_Contract_Repository_Auto_Accounts accCreation = new ba_Contract_Repository_Auto_Accounts(true);
            ID batchprocessid = Database.executeBatch(accCreation);
        Test.stopTest();
        system.assertEquals([SELECT Id FROM Contract_Repository_Account__c].size(), 4);
        

    }
    @isTest static void testAccCreationwithNonBE(){
                 
        Test.startTest();
        clsTestData_CustomSetting.createHistoryTracking_ContractRepository(true);
        Contract_Repository__c conRepo = New Contract_Repository__c (Id=[SELECT Id FROM Contract_Repository__c].Id,Account__c =[select Id, Name from Account where Name = 'Test UZ Leuven'].Id);
        Update conRepo;
        system.debug('......'+[SELECT Id FROM History_Tracking__c]);
        ba_Contract_Repository_Auto_Accounts accupdate = new ba_Contract_Repository_Auto_Accounts(true);
        ID batchprocessid1 = Database.executeBatch(accupdate);
        Test.stopTest();
        system.assertEquals([SELECT Id FROM Contract_Repository_Account__c].size(), 1);
    }
    
   
}