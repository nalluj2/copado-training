//--------------------------------------------------------------------------------------------------------------------
//  Description : This TEST APEX Class is used to test the logic in ctrl_User_Selector
//  Version     : 1.0
//  Author      : Bart Caelen
//  Date        : 08/09/2016
//--------------------------------------------------------------------------------------------------------------------
@isTest private class Test_ctrl_User_Selector {
	
    @testSetup static void createTestData() {

    	//---------------------------------------------------------------------------
    	// Create Test Data
    	//---------------------------------------------------------------------------
    	// Create One Stop Shop Data
    	clsTestData_OSS.createAllData();
    	//---------------------------------------------------------------------------

    }


	@isTest static void test_ctrl_User_Selector() {

		OSS_User_Override__c oOSSUserOverride = [SELECT Id, Name, OSS_Content__c, User_Operator__c, User__c FROM OSS_User_Override__c LIMIT 1];

		Test.startTest();

        PageReference oPageRef = new PageReference('/User_Selector');
		Test.setCurrentPage(oPageRef);
        ApexPages.StandardController oSTDCTRL = new ApexPages.StandardController(oOSSUserOverride);
        ctrl_User_Selector oCTRL = new ctrl_User_Selector(oSTDCTRL);

        	oPageRef.getParameters().put('id', oOSSUserOverride.Id);
        oSTDCTRL = new ApexPages.StandardController(oOSSUserOverride);
        oCTRL = new ctrl_User_Selector(oSTDCTRL);
        
        oOSSUserOverride = oCTRL.cur;
        oCTRL.save();
        oCTRL.cancel();

        Test.stopTest();

	}

}
//--------------------------------------------------------------------------------------------------------------------