@isTest
private class TestBuSelect {

    static testMethod void NoBusinessUnit() {
        User u;
        PageReference pageRef = Page.NoBusinessUnitPage;  
        Test.setCurrentPage(pageRef);

        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];    
        System.runAs ( thisUser ) {

            Profile p = [select id from profile where name='System Administrator'];    
            u = new User(alias = 'standt1', email='standarduser@testorg.com',             
                        emailencodingkey='UTF-8', lastname='TerritoryManagement_Testing',             
                        languagelocalekey='en_US',             
                        localesidkey='en_US', profileid = p.Id,            
                        timezonesidkey='America/Los_Angeles',             
                        CountryOR__c='Country1',          
                        Country= 'Country1',            
                        username='TestBuSelect@testorg.com',
                        Alias_unique__c='standarduser_Alias_unique__c',Company_Code_Text__c='EUR');            
        }   
         
        System.runAs(u) {
            
            // Create Test Data
            Company__c c = new Company__c(); 
            C.Name='Test Company name';
            C.Company_Code_Text__c = 'T46';
            Insert C;
            
            
            Business_Unit__c BU = new Business_Unit__c();
            BU.Name='Test Bu Name';
            BU.Company__c = C.Id;
            Insert BU;
            
            Sub_Business_Units__c SBU = new Sub_Business_Units__c();
            SBU.Name='Test Bu Name';
            SBU.Business_Unit__c = BU.Id;
            Insert SBU;         
            
            User_Business_Unit__c USBU = new User_Business_Unit__c();
            ApexPages.StandardController sct = new ApexPages.StandardController(USBU);
            ControllerNoBusinessUnitPage controller = new ControllerNoBusinessUnitPage(sct);
            controller.getCompanies();
            controller.getBUSBUList();
            controller.Company = c.Id;
            controller.getBUSBUList();
            controller.cancelButton();
            controller.Company = null;
            String[] strBUSBU1=new String[]{};
            controller.BUSBU=strBUSBU1;
            controller.Submit();
            String[] strBUSBU=new String[]{SBU.Name};
            controller.BUSBU=strBUSBU;
            controller.Company = c.Id;
            controller.Submit();
            
        }     
    }

    static testMethod void SelectOneBU() {
        User u;
        PageReference pageRef = Page.SelectOneBU;  
        Test.setCurrentPage(pageRef);

        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];    
        System.runAs ( thisUser ) {

            Profile p = [select id from profile where name='System Administrator'];    
            u = new User(alias = 'standt1', email='standarduser@testorg.com',             
                        emailencodingkey='UTF-8', lastname='TerritoryManagement_Testing',             
                        languagelocalekey='en_US',             
                        localesidkey='en_US', profileid = p.Id,            
                        timezonesidkey='America/Los_Angeles',             
                        CountryOR__c='Country1',          
                        Country= 'Country1',            
                        username='TestBuSelect1@testorg.com',
                        Alias_unique__c='standarduser_Alias_unique__c',Company_Code_Text__c='EUR');            
        }   
         
        System.runAs(u) {
            
            // Create Test Data
            Company__c c = new Company__c(); 
            C.Name='Test Company name';
            C.Company_Code_Text__c = 'T47';            
            Insert C;
            
            
            Business_Unit__c BU = new Business_Unit__c();
            BU.Name='Cranial Spinal';
            BU.Company__c = C.Id;
            Insert BU;

            Business_Unit__c BU1 = new Business_Unit__c();
            BU1.Name='Test2 Bu Name';
            BU1.Company__c = C.Id;
            Insert BU1;
            
            Sub_Business_Units__c SBU = new Sub_Business_Units__c();
            SBU.Name='Test Bu Name2';
            SBU.Business_Unit__c = BU.Id;
            Insert SBU;         

            Sub_Business_Units__c SBU1 = new Sub_Business_Units__c();
            SBU1.Name='Test Bu Name3';
            SBU1.Business_Unit__c = BU1.Id;
            Insert SBU1;
            
            User_Business_Unit__c USBU1 = new User_Business_Unit__c();
            USBU1.Sub_Business_Unit__c = SBU.Id;
            USBU1.User__c = u.Id;
            USBU1.Primary__c = true;
            Insert USBU1;

            Therapy_Group__c oTherapyGroup = new Therapy_Group__c();
                oTherapyGroup.Name = 'Test Therapy Group';
                oTherapyGroup.Company__c = c.Id;
                oTherapyGroup.Sub_Business_Unit__c = SBU.Id;
            insert oTherapyGroup;
            Therapy__c oTherapy = new Therapy__c();
                oTherapy.Name = 'Test Therapy';
                oTherapy.Therapy_Group__c = oTherapyGroup.Id;
                oTherapy.Business_Unit__c = BU.Id;
                oTherapy.Sub_Business_Unit__c = SBU.Id;
                oTherapy.Therapy_Name_Hidden__c = 'TestTherapy';
            insert oTherapy;
                                  
            
            User_Business_Unit__c USBU = new User_Business_Unit__c();
            ApexPages.currentPage().getParameters().put('mainPage', 'visitreport');
            ApexPages.currentPage().getParameters().put('eventId', 'eventId');
            ApexPages.currentPage().getParameters().put('IMPLANTING_CONTACT', 'IMPLANTING_CONTACT');
            ApexPages.currentPage().getParameters().put('IMPLANTING_ACCOUNT', 'IMPLANTING_ACCOUNT');
            ApexPages.currentPage().getParameters().put('VISIT_REPT_TYPE', 'VISIT_REPT_TYPE');
            ApexPages.currentPage().getParameters().put('conId', 'conId');
            ApexPages.currentPage().getParameters().put('action', 'Implant');
            ApexPages.currentPage().getParameters().put('AccountId', 'AccountId');
            
            ApexPages.StandardController sct = new ApexPages.StandardController(USBU);
            ControllerSelectOneBU controller = new ControllerSelectOneBU(sct);
            List<SelectOption> lstSO_BU = controller.lstSO_BU;
            List<SelectOption> lstSO_Therapy = controller.lstSO_Therapy;
            controller.cancelButton();
            controller.RedirectToNoBU();
            controller.BU=null;
            controller.continueSave();
            controller.BU = BU.Id;
            controller.continueSave();
            controller.tTherapyID = oTherapy.Id;
            controller.continueSave();
            
        }       
    }

    
}