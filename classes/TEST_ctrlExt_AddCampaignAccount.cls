//------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 23-05-2019
//  Description      : TEST APEX for APEX Controller ctrlExt_AddCampaignAccount
//  Change Log       : 
//------------------------------------------------------------------------------------------------------------
@isTest private class TEST_ctrlExt_AddCampaignAccount{

	@isTest private static void test_ctrlExt_AddCampaignAccount() {

	    //---------------------------------------
        // Create Test Data
        //---------------------------------------
		clsTestData_Account.iRecord_Account_SAPAccount = 50;
		clsTestData_Account.createAccount_SAPAccount();

		clsTestData_Campaign.idRecordType_Campaign = clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_Tactic').Id;
		clsTestData_Campaign.createCampaign();
        //---------------------------------------

		//---------------------------------------
	    // Perform Testing
	    //---------------------------------------
		Test.startTest();

            PageReference oPage = new PageReference('/addCammpaignAccount?Id=' + clsTestData_Campaign.oMain_Campaign.Id);
	        ApexPages.StandardController oSTDCTRL = new ApexPages.StandardController(clsTestData_Campaign.oMain_Campaign);
	        ctrlExt_AddCampaignAccount oCTRL = new ctrlExt_AddCampaignAccount(oSTDCTRL);

			Campaign oCampaign = oCTRL.oCampaign;

			List<Campaign_Account__c> lstCampaignAccount = [SELECT Id, Account__c FROM Campaign_Account__c WHERE Campaign__c = :oCampaign.Id];
			System.assertEquals(lstCampaignAccount.size(), 0);


			Boolean bTest = oCTRL.bHasNext;
	        bTest = oCTRL.bHasPrevious;
	        bTest = oCTRL.bMainSelected;

			Integer iTest = oCTRL.iPageNumber;
			iTest = oCTRL.iTotalPages;
			iTest = oCTRL.iSelectedRecords;

			Id idTest = oCTRL.idAccount_Delete;
			idTest = oCTRL.idAccount_Delete;

			List<SelectOption> lstSO_Test = oCTRL.lstAccountExistingView;

			String tTest = oCTRL.tAccountFilterId;
			tTest = oCTRL.tSortDir;
			tTest = oCTRL.tSortField;


			// Get all available Accounts
			List<ctrlExt_AddCampaignAccount.wrAccount> lstWRAccount = oCTRL.lstWRAccount;
			System.debug('**BC** lstWRAccount (' + lstWRAccount.size() + ') : ' + lstWRAccount);

			// Set the Selected checkbox to true for all available Accounts
			for (ctrlExt_AddCampaignAccount.wrAccount oWRAccount : lstWRAccount) oWRAccount.bSelected = true;
			oCTRL.updateSearchItemsMap();
			System.assertEquals(oCTRL.iSelectedRecords, lstWRAccount.size());
			lstWRAccount[0].bSelected = false;
			oCTRL.updateSearchItemsMap();
			System.assertEquals(oCTRL.iSelectedRecords, lstWRAccount.size()-1);
			lstWRAccount[0].bSelected = true;
			oCTRL.updateSearchItemsMap();
			System.assertEquals(oCTRL.iSelectedRecords, lstWRAccount.size());

			// Add the selected Accounts to the Selected List
			oCTRL.selectAccount();

			// Verify that all available Accounts are in the Selected List
			List<ctrlExt_AddCampaignAccount.wrAccount> lstWRAccount_Selected = oCTRL.lstWRAccount_Selected;
			System.assertEquals(lstWRAccount_Selected.size(), lstWRAccount.size());

			// Remove 2 Accounts from the Selected List
			oCTRL.idAccount_Delete = clsTestData_Account.lstAccount_SAPAccount[0].Id;
			oCTRL.deleteSelectedAccount();
			oCTRL.idAccount_Delete = clsTestData_Account.lstAccount_SAPAccount[1].Id;
			oCTRL.deleteSelectedAccount();

			// Verify that all available Accounts -2 are in the Selected List
			lstWRAccount_Selected = oCTRL.lstWRAccount_Selected;
			System.assertEquals(lstWRAccount_Selected.size(), lstWRAccount.size() - 2);

			// Try to add the Selected Accounts to the Campaign without setting the Status - verify that an error is added to the Accounts in the Selected list
			oCTRL.addAccountToCampaign();
			lstWRAccount_Selected = oCTRL.lstWRAccount_Selected;
			for (ctrlExt_AddCampaignAccount.wrAccount oWRAccount : lstWRAccount_Selected) System.assert(oWRAccount.tResult.contains('Please select a status'));

			// Set the Status on the Selected List and try to add the Selected Accounts to the Campaign - verify that all accounts are added
			oCTRL.oCampaignAccountStatus.Status__c = 'Planned';
			oCTRL.applyStatusToAll();
			oCTRL.addAccountToCampaign();
			lstCampaignAccount = [SELECT Id, Account__c FROM Campaign_Account__c WHERE Campaign__c = :oCampaign.Id];
			System.assertEquals(lstCampaignAccount.size(), lstWRAccount_Selected.size());

			// Try to add the Selected list again to the Campaign - verify that each selected account will get an error - no duplicate Accounts are added to the Campaign
			oCTRL.addAccountToCampaign();
			lstWRAccount_Selected = oCTRL.lstWRAccount_Selected;
			for (ctrlExt_AddCampaignAccount.wrAccount oWRAccount : lstWRAccount_Selected) System.assert(oWRAccount.tResult.contains('This Account is already linked to the Campaign'));
			lstCampaignAccount = [SELECT Id, Account__c FROM Campaign_Account__c WHERE Campaign__c = :oCampaign.Id];
			System.assertEquals(lstCampaignAccount.size(), lstWRAccount_Selected.size());


			ApexPages.StandardSetController oStandardSetController = oCTRL.stdSetController_Account;
			System.debug('**BC** oStandardSetController.getRecords() : ' + oStandardSetController.getRecords());


			oCTRL.resetFilter();
			oCTRL.addAccountToCampaign();
			oCTRL.deleteSelectedAccount();
			oCTRL.deleteSelectedAccount_All();
			oCTRL.firstPage();
			oCTRL.lastPage();
			oCTRL.nextPage();
			oCTRL.previousPage();
			oCTRL.resetFilter();
			oCTRL.selectAccount();
			oCTRL.updateSearchItemsMap();

		Test.stopTest();
	    //---------------------------------------

	}
}
//------------------------------------------------------------------------------------------------------------