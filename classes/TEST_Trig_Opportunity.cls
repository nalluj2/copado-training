@isTest
private class TEST_Trig_Opportunity{

	@isTest static void test_Trig_Opportunity() {

		//----------------------------------------------------------------------------------------------------
		// Create Test Data
		//----------------------------------------------------------------------------------------------------
		User oUser_SAP = clsTestData_User.createUser('sap.test@medtronic.com.test', 'tsap001', 'BELGIUM', clsUtil.getUserProfileId('SAP Interface'), clsUtil.getUserRoleId('EUR xBU'), false);
		User oUser_SystemAdministrator = clsTestData_User.createUser_SystemAdministrator('tadm000', false);
		List<User> lstUser = new List<User>{ oUser_SAP, oUser_SystemAdministrator};
		insert lstUser;

		List<Opportunity> lstOpportunityCANDIB;

		System.runAs(oUser_SystemAdministrator){
			clsTestData_Opportunity.iRecord_Opportunity_CANDIB = 5;
			lstOpportunityCANDIB = clsTestData_Opportunity.createOpportunity_CANDIB(false);
			for (Opportunity oOpportunity : lstOpportunityCANDIB){
				oOpportunity.StageName = 'Closed Won';
			}
		}
		//----------------------------------------------------------------------------------------------------


		//----------------------------------------------------------------------------------------------------
		// Test Logic
		//----------------------------------------------------------------------------------------------------
		Test.startTest();

		System.runAs(oUser_SAP){
			insert lstOpportunityCANDIB;
		}
		
		Test.stopTest();
		//----------------------------------------------------------------------------------------------------


		//----------------------------------------------------------------------------------------------------
		// Validate Result
		//----------------------------------------------------------------------------------------------------

		//----------------------------------------------------------------------------------------------------

	}
	
}