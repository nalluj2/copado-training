//---------------------------------------------------------------------------------------------------------------------------------
//	Author 			: Bart Caelen
//  Created Date 	: 08-05-2018
//  Description 	: APEX TEST Class for tr_Implant and bl_Implant_Trigger
//  Change Log 		: 
//---------------------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_bl_Implant_Trigger {
	
    private static Boolean bTestDataCreated = false;
    private static List<Implant__c> lstImplant = new List<Implant__c>();
    private static Business_Unit__c oBU_Main;

    @isTest static void createTestData_CRHF() {

        //---------------------------------------------------------
        // CREATE TEST DATA
        //---------------------------------------------------------
        // Company Data
        clsTestData.createCompanyData();
        clsTestData.createBusinessUnitData();
        clsTestData.createSubBusinessUnitData();
        clsTestData.createTherapyGroupData();

        for (Business_Unit__c oBU : clsTestData.mapBusinessUnit.values()){
            if ( (oBU.name == 'CRHF') && (oBU.Company__c == clsTestData.oMain_Company.Id) ){
                oBU_Main = oBU;
                break;
            }
        }
        clsTestData.oMain_BusinessUnit = oBU_Main;
        clsTestData.oMain_SubBusinessUnit = clsTestData.mapBusinessUnit_SubBusinessUnits.get(oBU_Main.Id)[0];
        clsTestData.createTherapyData();

        // Account Data
        clsTestData.iRecord_Account = 1;
        clsTestData.createAccountData();
        
        // Contact Data
        clsTestData.iRecord_Contact = 5;
        clsTestData.createContactData();

        // Call Activity Type Data
        clsTestData.iRecord_CallActivityType = 5;
        clsTestData.createCallActivityTypeData();

        // Call Category Data
        clsTestData.iRecord_CallCategory = 5;
        clsTestData.createCallCategoryData();

        // Product_Group__c Test Data
        Product_Group__c oProductGroup = new Product_Group__c();
            oProductGroup.Name = 'TEST Product Group';
            oProductGroup.Active__c = true;
            oProductGroup.Therapy_ID__c = clsTestData.oMain_Therapy.Id;
            oProductGroup.Editable_in__c = 'Units';
            oProductGroup.Product_Group_Relevant__c = true;
        insert oProductGroup;

        // Custom Setting Data - Europe_Call_Record__c
        User oUser_SystemAdministrator = [SELECT Id, Name, isActive FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true AND Id != :UserInfo.getUserID() LIMIT 1];
        System.runAs(oUser_SystemAdministrator){
            Europe_Call_Record__c oEuropeCallRecord = Europe_Call_Record__c.getOrgDefaults();
            if (oEuropeCallRecord == null){
                oEuropeCallRecord = new Europe_Call_Record__c();
            }
                oEuropeCallRecord.Implant_Support__c = clsTestData.lstCallActivityType[0].Id;
                oEuropeCallRecord.Case_Support__c = clsTestData.lstCallActivityType[1].Id;
                oEuropeCallRecord.Product_Demonstration__c = clsTestData.lstCallActivityType[2].Id;
                oEuropeCallRecord.Promotion__c = clsTestData.lstCallCategory[0].Id;
                oEuropeCallRecord.Support__c = clsTestData.lstCallCategory[1].Id;
                oEuropeCallRecord.SetupOwnerId = UserInfo.getOrganizationId();
            upsert oEuropeCallRecord;
        }        
        //---------------------------------------------------------

        bTestDataCreated = true;

    }

    @isTest static void createTestData_ST() {

        //---------------------------------------------------------
        // CREATE TEST DATA
        //---------------------------------------------------------
        // Company Data
        clsTestData.createCompanyData();
        clsTestData.createBusinessUnitData();
        clsTestData.createSubBusinessUnitData();
        clsTestData.createTherapyGroupData();

        for (Business_Unit__c oBU : clsTestData.mapBusinessUnit.values()){
            if ( (oBU.Name == 'Neurovascular') && (oBU.Company__c == clsTestData.oMain_Company.Id) ){
                oBU_Main = oBU;
                break;
            }
        }
        clsTestData.oMain_BusinessUnit = oBU_Main;
        clsTestData.oMain_SubBusinessUnit = clsTestData.mapBusinessUnit_SubBusinessUnits.get(oBU_Main.Id)[0];
        clsTestData.createTherapyData();

        // Account Data
        clsTestData.iRecord_Account = 1;
        clsTestData.createAccountData();
        
        // Contact Data
        clsTestData.iRecord_Contact = 5;
        clsTestData.createContactData();

        // Call Activity Type Data
        clsTestData.iRecord_CallActivityType = 5;
        clsTestData.createCallActivityTypeData();

        // Call Category Data
        clsTestData.iRecord_CallCategory = 5;
        clsTestData.createCallCategoryData();

        // Custom Setting Data - Europe_Call_Record__c
        User oUser_SystemAdministrator = [SELECT Id, Name, isActive FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true AND Id != :UserInfo.getUserID() LIMIT 1];
        System.runAs(oUser_SystemAdministrator){
            Europe_Call_Record__c oEuropeCallRecord = Europe_Call_Record__c.getOrgDefaults();
            if (oEuropeCallRecord == null){
                oEuropeCallRecord = new Europe_Call_Record__c();
            }
                oEuropeCallRecord.Implant_Support__c = clsTestData.lstCallActivityType[0].Id;
                oEuropeCallRecord.Case_Support__c = clsTestData.lstCallActivityType[1].Id;
                oEuropeCallRecord.Product_Demonstration__c = clsTestData.lstCallActivityType[2].Id;
                oEuropeCallRecord.Promotion__c = clsTestData.lstCallCategory[0].Id;
                oEuropeCallRecord.Support__c = clsTestData.lstCallCategory[1].Id;
                oEuropeCallRecord.SetupOwnerId = UserInfo.getOrganizationId();
            upsert oEuropeCallRecord;
        }        
        //---------------------------------------------------------

        bTestDataCreated = true;

    }

    @isTest static void createTestData_Implant() {

        //---------------------------------------------------------
        // CREATE TEST DATA
        //---------------------------------------------------------
        if (bTestDataCreated == false){
            createTestData_CRHF();
        }

        List<Product_Group__c> lstProductGroup = [SELECT Id FROM Product_Group__c];

        // Implant Test Data
        lstImplant = new List<Implant__c>();

        Implant__c oImplant_AFS = new Implant__c();
            oImplant_AFS.RecordTypeId = clsUtil.getRecordTypeByDevName('Implant__c', 'AFS_Cryo_AF').Id;
            oImplant_AFS.Implant_Implanting_Account__c = clsTestData.lstAccount[0].Id;
            oImplant_AFS.Implant_Implanting_Contact__c = clsTestData.lstContact[0].Id;
            oImplant_AFS.Implant_Referring_Account__c = clsTestData.lstAccount[0].Id;
            oImplant_AFS.Implant_Referring_Contact__c = clsTestData.lstContact[1].Id;
            oImplant_AFS.Implant_Therapy__c = clsTestData.oMain_Therapy.Id;
            oImplant_AFS.Therapy__c = clsTestData.oMain_Therapy.Id;
            oImplant_AFS.Attended_Implant_Text__c = 'Attended';        
            oImplant_AFS.Duration_Nr__c = 10;
            oImplant_AFS.Procedure_time_in_min__c = 10;        
            if (lstProductGroup.size() > 0){
                oImplant_AFS.Product_Group__c = lstProductGroup[0].Id;
            }
        lstImplant.add(oImplant_AFS); 

        Implant__c oImplant_EUR = new Implant__c();
            oImplant_EUR.RecordTypeId = clsUtil.getRecordTypeByDevName('Implant__c', 'EUR_Implant').Id;
            oImplant_EUR.Implant_Implanting_Account__c = clsTestData.lstAccount[0].Id;
            oImplant_EUR.Implant_Implanting_Contact__c = clsTestData.lstContact[0].Id;
            oImplant_EUR.Implant_Referring_Account__c = clsTestData.lstAccount[0].Id;
            oImplant_EUR.Implant_Referring_Contact__c = clsTestData.lstContact[1].Id;
            oImplant_EUR.Implant_Therapy__c = clsTestData.oMain_Therapy.Id;
            oImplant_EUR.Therapy__c = clsTestData.oMain_Therapy.Id;
            oImplant_EUR.Attended_Implant_Text__c = 'Attended';        
            oImplant_EUR.Duration_Nr__c = 10;
            oImplant_EUR.Procedure_time_in_min__c = 10;        
            if (lstProductGroup.size() > 0){
                oImplant_EUR.Product_Group__c = lstProductGroup[0].Id;
            }
        lstImplant.add(oImplant_EUR); 


        Implant__c oImplant_EUR_ST = new Implant__c();
            oImplant_EUR_ST.RecordTypeId = clsUtil.getRecordTypeByDevName('Implant__c', 'EUR_Implant').Id;
            oImplant_EUR_ST.Implant_Implanting_Account__c = clsTestData.lstAccount[0].Id;
            oImplant_EUR_ST.Implant_Implanting_Contact__c = clsTestData.lstContact[0].Id;
            oImplant_EUR_ST.Implant_Referring_Account__c = clsTestData.lstAccount[0].Id;
            oImplant_EUR_ST.Implant_Referring_Contact__c = clsTestData.lstContact[1].Id;
            oImplant_EUR_ST.Implant_Therapy__c = clsTestData.oMain_Therapy.Id;
            oImplant_EUR_ST.Therapy__c = clsTestData.oMain_Therapy.Id;
            oImplant_EUR_ST.Attended_Implant_Text__c = 'Attended';        
            oImplant_EUR_ST.Duration_Nr__c = 10;
            oImplant_EUR_ST.Procedure_time_in_min__c = 10;        
            if (lstProductGroup.size() > 0){
                oImplant_EUR_ST.Product_Group__c = lstProductGroup[0].Id;
            }
        lstImplant.add(oImplant_EUR_ST); 

        GTutil.chkInsertImplantAfterAll = true;
        insert lstImplant;  
        GTutil.chkInsertImplantAfterAll = true;        
        //---------------------------------------------------------

    }

    @isTest static void test_ImplantAfterAll_INSERT_1() {

        Test.startTest();

        //---------------------------------------------------------
        // CREATE TEST DATA
        //---------------------------------------------------------
        createTestData_CRHF();
        //---------------------------------------------------------

        //---------------------------------------------------------
        // CREATE IMPLANT DATA
        //---------------------------------------------------------
        createTestData_Implant();
        //---------------------------------------------------------

        Test.stopTest();

    }

    @isTest static void test_ImplantAfterAll_INSERT_2() {

        Test.startTest();

        //---------------------------------------------------------
        // CREATE TEST DATA
        //---------------------------------------------------------
        createTestData_ST();
        //---------------------------------------------------------

        //---------------------------------------------------------
        // CREATE IMPLANT DATA
        //---------------------------------------------------------
        createTestData_Implant();
        //---------------------------------------------------------

        Test.stopTest();

    }    

    @isTest static void test_ImplantAfterAll_UPDATE_1() {

        //---------------------------------------------------------
        // CREATE TEST DATA
        //---------------------------------------------------------
        createTestData_CRHF();
        GTutil.chkInsertImplantAfterAll = true;
        GTutil.chkUpdateImplantAfterAll = true;
        createTestData_Implant();
        GTutil.chkInsertImplantAfterAll = true;
        GTutil.chkUpdateImplantAfterAll = true;


        lstImplant = Database.query(clsUtil.tGetSOQLForSObject('Implant__c'));
        List<Call_Records__c> lstCallRecord = Database.query(clsUtil.tGetSOQLForSObject('Call_Records__c'));

        System.assertNotEquals(lstImplant.size(), 0);
        System.assertNotEquals(lstCallRecord.size(), 0);
        //---------------------------------------------------------

        Test.startTest();

        //---------------------------------------------------------
        // UPDATE IMPLANT DATA
        //---------------------------------------------------------
        System.assertNotEquals(lstImplant.size(), 0);
        List<Implant__c> lstImplant_Update = new List<Implant__c>();
        for (Implant__c oImplant : lstImplant){
            System.assertNotEquals(oImplant.Call_Record_ID__c, null);
            System.assertEquals(oImplant.Attended_Implant_Text__c, 'Attended');

                oImplant.Implant_Referring_Contact__c = clsTestData.lstContact[4].Id;
                oImplant.Implant_Date_Of_Surgery__c = Date.today().addDays(-10);
                oImplant.Duration_Nr__c = 50;
                oImplant.Procedure_time_in_min__c = 50;
            lstImplant_Update.add(oImplant);
        }
        GTutil.chkUpdateImplantAfterAll = true;
        update lstImplant_Update;
        //---------------------------------------------------------

        Test.stopTest();

    }

    @isTest static void test_ImplantAfterAll_UPDATE_2() {

        //---------------------------------------------------------
        // CREATE TEST DATA
        //---------------------------------------------------------
        createTestData_ST();
        GTutil.chkInsertImplantAfterAll = true;
        GTutil.chkUpdateImplantAfterAll = true;
        createTestData_Implant();
        GTutil.chkInsertImplantAfterAll = true;
        GTutil.chkUpdateImplantAfterAll = true;

        lstImplant = Database.query(clsUtil.tGetSOQLForSObject('Implant__c'));
        List<Call_Records__c> lstCallRecord = Database.query(clsUtil.tGetSOQLForSObject('Call_Records__c'));

        System.assertNotEquals(lstImplant.size(), 0);
        System.assertNotEquals(lstCallRecord.size(), 0);
        //---------------------------------------------------------

        Test.startTest();

        //---------------------------------------------------------
        // UPDATE IMPLANT DATA
        //---------------------------------------------------------
        System.assertNotEquals(lstImplant.size(), 0);
        List<Implant__c> lstImplant_Update = new List<Implant__c>();
        for (Implant__c oImplant : lstImplant){
            System.assertNotEquals(oImplant.Call_Record_ID__c, null);
            System.assertEquals(oImplant.Attended_Implant_Text__c, 'Attended');

                oImplant.Implant_Referring_Contact__c = clsTestData.lstContact[4].Id;
                oImplant.Implant_Date_Of_Surgery__c = Date.today().addDays(-5);
                oImplant.Duration_Nr__c = 50;
                oImplant.Procedure_time_in_min__c = 50;
            lstImplant_Update.add(oImplant);
        }
        GTutil.chkUpdateImplantAfterAll = true;
        update lstImplant_Update;
        //---------------------------------------------------------

        Test.stopTest();

    }    

    @isTest static void test_ImplantAfterAll_DELETE_1() {

        //---------------------------------------------------------
        // CREATE TEST DATA
        //---------------------------------------------------------
        createTestData_CRHF();
        createTestData_Implant();
        //---------------------------------------------------------

        Test.startTest();

        //---------------------------------------------------------
        // DELETE IMPLANT DATA
        //---------------------------------------------------------
        System.assertNotEquals(lstImplant.size(), 0);
        delete lstImplant;
        //---------------------------------------------------------

        Test.stopTest();

    }  

    @isTest static void test_ImplantAfterAll_DELETE_2() {

        //---------------------------------------------------------
        // CREATE TEST DATA
        //---------------------------------------------------------
        createTestData_ST();
        createTestData_Implant();
        //---------------------------------------------------------

        Test.startTest();

        //---------------------------------------------------------
        // DELETE IMPLANT DATA
        //---------------------------------------------------------
        System.assertNotEquals(lstImplant.size(), 0);
        delete lstImplant;
        //---------------------------------------------------------

        Test.stopTest();

    }   
    
}
//---------------------------------------------------------------------------------------------------------------------------------