//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 02-04-2019
//  Description      : TEST APEX for the Batch APEX ba_Review_Field_Usage
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_ba_Review_Field_Usage {

	@isTest
	private static void test_ba_Review_Field_Usage() {

		// Create Test Data
		clsTestData_Account.createAccount();

		// Test Logic
		Test.startTest();

		ba_Review_Field_Usage oBatch = new ba_Review_Field_Usage();
			oBatch.tSObjectName = 'Account';
		Database.executeBatch(oBatch , 1);

		Test.stopTest();

	}

}
//--------------------------------------------------------------------------------------------------------------------