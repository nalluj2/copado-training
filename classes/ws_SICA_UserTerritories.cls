@RestResource(urlMapping='/SICA_UserTerritories/*')
global with sharing class ws_SICA_UserTerritories {
    
    @HttpGet
	global static void doGet() {
 		
 		List<Id> userTerritories = new List<Id>();
					
		for(UserTerritory2Association userTerritory : [SELECT Territory2Id FROM UserTerritory2Association WHERE UserId = :UserInfo.getUserId() AND isActive = True]){
			
			userTerritories.add(userTerritory.Territory2Id);
		}	
		
		if(userTerritories.size() > 0) getTerritoryDescendants(userTerritories, userTerritories);
			
		RestContext.response.responseBody = Blob.valueOf(JSON.serialize(userTerritories));
	}
	
	private static void getTerritoryDescendants(List<Id> parentTerritories, List<Id> allTerritories){
		
		List<Id> childTerrs = new List<Id>();
		
		for(Territory2 childTerr : [Select Id from Territory2 where ParentTerritory2Id IN :parentTerritories]){
			
			childTerrs.add(childTerr.Id);
			allTerritories.add(childTerr.Id);
		}
		
		if(childTerrs.size() > 0) getTerritoryDescendants(childTerrs, allTerritories);
	}
}