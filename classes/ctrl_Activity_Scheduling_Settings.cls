global with sharing class ctrl_Activity_Scheduling_Settings {
	
	@AuraEnabled
    public static Activity_Scheduling_User_Settings__c getUserSettings(){
    	
    	List<Activity_Scheduling_User_Settings__c> settings = [Select Country__c, Post_Code__c from Activity_Scheduling_User_Settings__c where OwnerId = :UserInfo.getUserId()];
    	
    	if(settings.size() == 0){
    		
    		Activity_Scheduling_User_Settings__c newSettings = new Activity_Scheduling_User_Settings__c();
    		newSettings.Country__c = [Select Country_vs__c from User where Id = :UserInfo.getUserId()].Country_vs__c;
    		
    		settings.add(newSettings);    		
    	}
    	
    	return settings[0];
    }    
    
    @AuraEnabled
    public static List<String> getCountryPicklistValues() {
    	
    	String sessionId = UserInfo.getSessionId();
    	System.debug(Schema.SObjectType.Activity_Scheduling_User_Settings__c.getRecordTypeInfosByDeveloperName());
        Id masterRT = Schema.SObjectType.Activity_Scheduling_User_Settings__c.getRecordTypeInfosByDeveloperName().values()[0].getRecordTypeId();
        
        Http http = new Http();
                
        String instanceURL = Support_Portal_URL__c.getInstance().Instance_URL__c;
        
        HttpRequest req = new HttpRequest();    	    	
		req.setEndpoint(instanceURL + '/services/data/v46.0/ui-api/object-info/Activity_Scheduling_User_Settings__c/picklist-values/' + masterRT + '/Country__c');
		req.setMethod('GET');
		req.setHeader('Authorization', 'Bearer ' + sessionId);
		req.setTimeout(120000);
					
	    HttpResponse res = http.send(req);
	    System.debug(res.getBody());
	    System.debug(res.getBody().unescapeHtml4());
	    FieldPicklistValues response = (FieldPicklistValues) JSON.deserialize(res.getBody().unescapeHtml4(), FieldPicklistValues.class);
                	
		List<PicklistValue> pickValues = response.values;        	
		
		List<String> result = new List<String>();
		    	
		for(PicklistValue pickValue : pickValues){
			
			result.add(pickValue.value); 
		}
                
        return result;
    }
    
    @AuraEnabled
    public static void saveUserSettings(Activity_Scheduling_User_Settings__c settings){
    	
    	try{
    		
    		settings.OwnerId = UserInfo.getUserId();
    		settings.Unique_Key__c = UserInfo.getUserId();
    		
    		upsert settings Unique_Key__c;
    		
    	}catch(Exception e){

    		String errorMessage;
    		if(e.getMessage().contains('Exception:')) errorMessage = e.getMessage().split('Exception:')[1].split('Class.')[0];
    		else errorMessage = e.getMessage();
    		AuraHandledException auraEx = new AuraHandledException(errorMessage); 
    		if(test.isRunningTest()) auraEx.setMessage(e.getMessage());
    		throw auraEx;
    	}    
    }    
   
    public class FieldPicklistValues{
    	
    	public List<PicklistValue> values {get; set;}
    }
    
    public class PicklistValue{
    	
    	@AuraEnabled public String label {get; set;}
    	@AuraEnabled public String value {get; set;}
    	
    	public PicklistValue(String inputValue, String inputLabel){
    		
    		label = inputLabel;
    		value = inputValue;
    	}
    }
}