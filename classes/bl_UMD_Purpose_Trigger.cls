public with sharing class bl_UMD_Purpose_Trigger {
	
	public static void setUniqueKeys(List<UMD_Purpose__c> triggerNew){
		
		for(UMD_Purpose__c purpose : triggerNew){
		
			purpose.Unique_Key__c = purpose.Segmentation_Qualification__c + ':' + purpose.Fiscal_Year_VS__c;
			
			if(purpose.Status__c == 'Open for input') purpose.Unique_Qualification_Key__c = purpose.Segmentation_Qualification__c; 
			else purpose.Unique_Qualification_Key__c = null;
		}   
	} 
	
	public static void blockUpdateFY(List<UMD_Purpose__c> triggerNew, Map<Id, UMD_Purpose__c> oldMap){
		
		if (bl_Trigger_Deactivation.isTriggerDeactivated('UMD_Purpose_change_FY')) return;
		
		List<UMD_Purpose__c> modified = new List<UMD_Purpose__c>();
		
		for(UMD_Purpose__c purpose : triggerNew){
			
			if(purpose.Fiscal_Year_vs__c != oldMap.get(purpose.Id).Fiscal_Year_vs__c || purpose.Segmentation_Qualification__c != oldMap.get(purpose.Id).Segmentation_Qualification__c) modified.add(purpose);
		}
		
		if(modified.size() > 0){
			
			Map<Id, UMD_Purpose__c> purposeMap = new Map<Id, UMD_Purpose__c>([Select Id, (Select Id from Segmentation_Potential_Input__r LIMIT 1), (Select Id from UMD_Input_Complete__r LIMIT 1) from UMD_Purpose__c where Id IN :modified]);
			
			for(UMD_Purpose__c purpose : modified){
				
				UMD_Purpose__c db_purpose = purposeMap.get(purpose.Id);
				
				if(db_purpose.Segmentation_Potential_Input__r.size() > 0 || db_purpose.UMD_Input_Complete__r.size() > 0) purpose.addError('This purpose is in use by Segmentation input records and therefore the Fiscal Year and Qualification cannot be changed. Get in contact with Salesforce IT if you need to change this value.');
			}
		}
	}
	
	public static void cloneWithQuestions(List<UMD_Purpose__c> triggerNew){
		
		Set<Id> clonedFrom = new Set<Id>();
		
		for(UMD_Purpose__c purpose : triggerNew){
			
			if(purpose.isClone()) clonedFrom.add(purpose.getCloneSourceId());
		}
		
		if(clonedFrom.size() > 0){
			
			Map<Id, UMD_Purpose__c> clonedFromMap = new Map<Id, UMD_Purpose__c>([Select Id, (Select UMD_Question__c, Sequence__c, Business_Unit_Group__c, Sub_Business_Unit__c from UMD_Purpose_Question__r) from UMD_Purpose__c where Id IN :clonedFrom]);
			
			List<UMD_Purpose_Question__c> toInsert = new List<UMD_Purpose_Question__c>();
			
			for(UMD_Purpose__c purpose : triggerNew){
			
				if(purpose.isClone()){
					
					for(UMD_Purpose_Question__c original : clonedFromMap.get(purpose.getCloneSourceId()).UMD_Purpose_Question__r){
						
						UMD_Purpose_Question__c clone = original.clone();
						clone.UMD_Purpose__c = purpose.Id;
						
						toInsert.add(clone);		
					}					
				}
			}
			
			if(toInsert.size() > 0) insert toInsert;	
		}
	}   
}