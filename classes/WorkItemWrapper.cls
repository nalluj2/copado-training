public with sharing class WorkItemWrapper{
        public Work_Item__c WorkItem {get;set;}
        public Boolean IsChecked{get;set;}
        
        public WorkItemWrapper(Work_Item__c wi,Boolean boolValue){
            WorkItem = wi;
            IsChecked = boolValue;
        }
    }