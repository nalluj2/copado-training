/**
 *      Created Date : 20130213
 *      Description : This class contains the business logic for deleting the territories of the deleting account which are in the
                      same salesforce node in case of account merge.
 * 
 *      Author = Kaushal
 */
public class bl_TerritoryMerge{

    public static Set<Id> oldShareIds;
    
    private static Set<Id> processedAccounts = new Set<Id>();
        
    public static void afterAccountUpdateAccShareDeleteLogic(Id accountId){
    	    	
    	if(processedAccounts.add(accountId) == true){
    		    			      	       
	        List<ObjectTerritory2Association> mergedAccShares = new List<ObjectTerritory2Association>();// List of Account Shares that have been transferred from looser to winner
	        List<ObjectTerritory2Association> existingAccShares = new List<ObjectTerritory2Association>();// List of Account Shares that the winner had before the merge
	        System.debug('Old Shares : ' + oldShareIds);       
	        //Divide Account Shares into originals or merged
	        
	        Long now = DateTime.now().getTime();
	        
	        for(ObjectTerritory2Association accShare : [Select Id, Territory2.Id, LastModifiedDate, LastModifiedById, Territory2.ParentTerritory2Id from ObjectTerritory2Association where ObjectId = :accountId AND Territory2.Territory2Type.DeveloperName = 'Territory']){
	            
	            System.debug('After Merge Share : ' + accShare);
	            	            
	        	if(oldShareIds.contains(accShare.Territory2Id) && ((now - accShare.LastModifiedDate.getTime()) < 30000) && accShare.LastModifiedById == UserInfo.getUserId()) mergedAccShares.add(accShare);    
	            else existingAccShares.add(accShare);
	        }
	        
	        //If the winner Account or both looser Accounts didn't have any Territory assigned then there is no need to merge
	        if(mergedAccShares.size() == 0 ||  existingAccShares.size() == 0) return;
	        	        	        
		    Map<Id, Territory2> mapMergedTer = calculateTerritory(mergedAccShares);
		    Map<Id, Territory2> mapExistingTer = calculateTerritory(existingAccShares);
		        
		    Set<Id> mergedSFIds = calculateSFTerritory(mapMergedTer);
		    Set<Id> existingSFIds = calculateSFTerritory(mapExistingTer);
		    
		    Set<Id> matchingIds = new Set<Id>();
		       
		    for(Id existingSFId : existingSFIds){
		        	
		        if(mergedSFIds.contains(existingSFId)) matchingIds.add(existingSFId);		        
		    }
	        
	        //If there are no common grandparent territories there is no action needed.
		    if(matchingIds.size() == 0) return;    
		    
            Set<Id> districtIds = new Set<Id>();
            for(Territory2 ter : [Select id from Territory2 where ParentTerritory2id IN :matchingIds AND Territory2Type.DeveloperName = 'District']){
            	
            	districtIds.add(ter.Id);
            }
                                    
            Set<Id> duplicateTerrIds = new Set<Id>();            
            for(Territory2 terr : [Select Id from Territory2 where ParentTerritory2Id IN :districtIds and Id IN :mapMergedTer.keySet()]){
            	
            	duplicateTerrIds.add(terr.Id);
            }
               
                       
            List<ObjectTerritory2Association> accSharesToDelete = new List<ObjectTerritory2Association>();
            
            for(ObjectTerritory2Association accShare : mergedAccShares){
            	
            	if(duplicateTerrIds.contains(accShare.Territory2Id)) accSharesToDelete.add(accShare);
            }
                        
            if(accSharesToDelete.size() > 0) delete accSharesToDelete;            	            
    	}    	  
    }
    
   
    // This method is used to get the SF territory for the territory at lowest level i.e. territory type='Territory'.
    public static Set<Id> calculateSFTerritory(Map<Id, Territory2> territories){
        
        Set<Id> districtIds = new Set<Id>();
        
		for(Territory2 ter : territories.values()){
			
			districtIds.add(ter.ParentTerritory2Id);
		}
        
        Set<Id> sfIds = new Set<Id>();              
        
        for(Territory2 ter : [Select Id, ParentTerritory2id from Territory2 where Id IN :districtIds AND Territory2Type.DeveloperName = 'District']){
        	
        	sfIds.add(ter.ParentTerritory2Id);
        }       
        
        return sfIds;
    }
    
    // This method is used to get the territory of an Account Share.
    public static Map<Id, Territory2> calculateTerritory(List<ObjectTerritory2Association> accShares){
        
        Map<Id, Territory2> mapTerritory = new Map<Id, Territory2>();
                
        for(ObjectTerritory2Association accShare : accShares){
        	
        	mapTerritory.put(accShare.Territory2.Id, accShare.Territory2);
        }
                          
        return mapTerritory;
    } 
}