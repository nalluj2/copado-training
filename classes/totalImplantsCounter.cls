/**
 *      Created Date : 20090518
 *      Description : Class called after an implant is saved, deleted, undeleted or created 
 *          If Implant is saved
 *          If (Implanting contact <> "") And (referring contact  <> "")
 *             If affiliation exist between implanting an referring contact in calendar year of date of surgery
 *                                Actual no. of implants = count(query:table"implants" where implanting contact = implcon and referring contact = refcon and start date equal or > affstartdate)
 *              If affiliation does not exist
 *                               Create affiliation + create aff detail in calendar year corresponding to dtae of surgery year (current functionality)
 * 
 *      Author = Miguel Coimbra / ABSI 
 *
 */


public class totalImplantsCounter {
    // THIS CLASS IS NOT BULKYFIED : THE NR OF IMPLANTS IS LIMITED!!!
    public static void countImplants(Implant__c[] implants) {
        for (Implant__c implant:implants){
            /*######################## AFFILIATION  ###############################*/
            if (implant.Implant_Referring_Contact__c == null || implant.Implant_Implanting_Contact__c == null){
                // only if it's an implant with a referring and implanting Contact ! 
                return ; 
            }
            // select a list of Affiliation that corresponds to the Implant
            //modified by priti to include therapy implement change related to  CR-2350
            system.debug('implant therapy'+implant.Therapy__c);
            Affiliation__c[] affTab = [SELECT Id, Therapy__c, Affiliation_From_Contact__c,Affiliation_To_Contact__c,
                                Affiliation_From_Account__c,Affiliation_To_Account__c
                                FROM Affiliation__c 
                                where Affiliation_Type__c = 'Referring' and RecordTypeId =: FinalConstants.recordTypeIdC2C
                                and Affiliation_From_Contact__c =: implant.Implant_Referring_Contact__c
                                and Affiliation_To_Contact__c =: implant.Implant_Implanting_Contact__c
                                and Affiliation_From_Account__c =: implant.Implant_Referring_Account__c
                                and Affiliation_To_Account__c =: implant.Implant_Implanting_Account__c 
                                and Therapy__c=:implant.Therapy__c limit 1];              
            system.debug('affiliation'+affTab);
            Affiliation__c aff ;
            // If there is no Affiliation -> create an Affiliation
            if (affTab.size() == 0) {
                aff = totalImplantsCounter.createAffiliation(implant);
                return;
            }else{
                // Otherwize assign it to the existing ! 
                aff = affTab[0];
            }
            /*######################## AFFILIATION DETAILS #######################*/
            Affiliation_Details__c[] aff_detailstab = new Affiliation_Details__c[]{};
            aff_detailstab = [SELECT Id, No_of_Implants__c,Actual_No_Of_Tests__c, Affiliation_Details_Start_Date__c, Affiliation_Details_End_Date__c,Fiscal_Year__c,
                              Affiliation_Details_Affiliation__r.Therapy__c FROM Affiliation_Details__c 
                                where Affiliation_Details_Affiliation__c =: aff.Id 
                                and Affiliation_Details_Start_Date__c <=: implant.Implant_Date_Of_Surgery__c 
                                and Affiliation_Details_End_Date__c >=: implant.Implant_Date_Of_Surgery__c limit 1];
                                // To do where Date between from and to 
            
            // If there is no Affiliation_Details for this Affiliation -> create one
            Affiliation_Details__c aff_details ;            
            if (aff_detailstab.size() == 0) {   
                aff_details = totalImplantsCounter.createAffiliationDetails(aff, implant.Implant_Date_Of_Surgery__c,implant.Procedure_Text__c);
                return;
            }else{
                aff_details = aff_detailstab[0];
            }
            
            /*######################## IMPLANTS COUNTER #######################*/       
            // count how many Implants there are for this combination Contact / Account
            /*Integer nrImplants = [SELECT count() FROM Implant__c 
                        where Implant_Referring_Contact__c =: implant.Implant_Referring_Contact__c
                        and Implant_Implanting_Contact__c =: implant.Implant_Implanting_Contact__c
                        and Implant_Referring_Account__c =: implant.Implant_Referring_Account__c
                        and Implant_Implanting_Account__c =: implant.Implant_Implanting_Account__c
                        and Implant_Date_Of_Surgery__c >=:aff_details.Affiliation_Details_Start_Date__c 
                        and Implant_Date_Of_Surgery__c <=:aff_details.Affiliation_Details_End_Date__c ];
                    
            aff_details.No_of_Implants__c  = nrImplants ; */
            //added by priti to update the actual implant and actual test on relationship detail...
            //modified by priti to include therapy implement change related to  CR-2350
            Integer nrActualImplants = [SELECT count() FROM Implant__c 
                        where Implant_Referring_Contact__c =: implant.Implant_Referring_Contact__c
                        and Implant_Implanting_Contact__c =: implant.Implant_Implanting_Contact__c
                        and Implant_Referring_Account__c =: implant.Implant_Referring_Account__c
                        and Implant_Implanting_Account__c =: implant.Implant_Implanting_Account__c
                        and Implant_Date_Of_Surgery__c >=:aff_details.Affiliation_Details_Start_Date__c 
                        and Implant_Date_Of_Surgery__c <=:aff_details.Affiliation_Details_End_Date__c 
                        and Procedure_Text__c=:'New Implant'
                        and Therapy__c=:aff_details.Affiliation_Details_Affiliation__r.Therapy__c];
            Integer nrActualTest = [SELECT count() FROM Implant__c 
                            where Implant_Referring_Contact__c =: implant.Implant_Referring_Contact__c
                            and Implant_Implanting_Contact__c =: implant.Implant_Implanting_Contact__c
                            and Implant_Referring_Account__c =: implant.Implant_Referring_Account__c
                            and Implant_Implanting_Account__c =: implant.Implant_Implanting_Account__c
                            and Implant_Date_Of_Surgery__c >=:aff_details.Affiliation_Details_Start_Date__c 
                            and Implant_Date_Of_Surgery__c <=:aff_details.Affiliation_Details_End_Date__c 
                            and Procedure_Text__c=:'Test'
                            and Therapy__c=:aff_details.Affiliation_Details_Affiliation__r.Therapy__c];
            aff_details.No_of_Implants__c  = nrActualImplants;
            aff_details.Actual_No_Of_Tests__c  = nrActualTest;
            update aff_details;
        }
        
    }
    // Create Affiliation 
    public static Affiliation__c createAffiliation(Implant__c implant) {
        try{
            Affiliation__c newAff = new Affiliation__c();
            newAff.Affiliation_Start_Date__c = Date.today();
            newAff.Affiliation_From_Contact__c = implant.Implant_Referring_Contact__c;
            newAff.Affiliation_To_Contact__c = implant.Implant_Implanting_Contact__c;
            newAff.Affiliation_From_Account__c = implant.Implant_Referring_Account__c;
            newAff.Affiliation_To_Account__c = implant.Implant_Implanting_Account__c;
            newAff.Affiliation_Type__c = 'Referring';
            newAff.Therapy__c = implant.Therapy__c ;
//            newAff.Business_Unit__c =SharedMethods.getBUlookup();
            //added by shweta:  Work plan item:R4.1 020(Apr 5' 2012 Release)
            newAff.Business_Unit__c=implant.Business_Unit__c;
            newAff.RecordTypeId = FinalConstants.recordTypeIdC2C;
            insert newAff;
            return newAff;
        }catch(DmlException ex){
            // ApexPages.addMessages(ex);
            System.debug('############### Error ( totalImplantsCounter.93)### while creating new affiliation :' + ex ) ; 
            return null; 
        }       
    }
    // Create Affiliation Details 
    public static Affiliation_Details__c createAffiliationDetails(Affiliation__c aff,Date dateOfSurgery,String procedure) {
        try{
            Affiliation_Details__c affDetails = new Affiliation_Details__c();
            affDetails.Affiliation_Details_Affiliation__c = aff.Id;
            if (dateOfSurgery != null){ 
                //modified by priti to update the affiliation detail start date and end date with current FY start date and End Date
                FiscalYearSettings fiscalYear=[SELECT Name,StartDate,EndDate FROM FiscalYearSettings where StartDate<=:dateOfSurgery and EndDate>=:dateOfSurgery];              
                affDetails.Fiscal_Year__c=fiscalYear.Name;
                affDetails.Affiliation_Details_Start_Date__c = fiscalYear.StartDate;                
                affDetails.Affiliation_Details_End_Date__c = fiscalYear.EndDate;                
            }
            //affDetails.No_of_Implants__c = 1;
            if(procedure=='New Implant'){
                affDetails.No_of_Implants__c = 1;
            }
            else if(procedure=='Test'){
                affDetails.Actual_No_Of_Tests__c  = 1;
            }
            insert affDetails;
            return affDetails;
        }catch(DmlException ex){
            //ApexPages.addMessages(ex);
            System.debug('############### Error ( totalImplantsCounter.111)### while creating affiliation details  :' + ex) ;
            return null;
        }       
    }
}