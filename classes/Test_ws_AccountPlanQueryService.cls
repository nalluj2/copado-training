@isTest(SeeAllData=true)
private class Test_ws_AccountPlanQueryService {

    static testMethod void serviceShouldReturnExistingIplan() {
    	
    	Profile prof = [select p.Id
    				    from Profile p
    				    where p.Name =: 'EUR Field Force CVG'];
    	
    	User userToAdd = new User();
    	userToAdd.ProfileId = prof.Id;
    	userToAdd.Alias_unique__c = 'alias1';
    	userTOAdd.Alias ='test1';
    	userToAdd.FirstName ='test1';
    	userToAdd.LastName ='test1';
    	userToAdd.Username ='test2@medtronic.test.com';
    	userToAdd.CommunityNickname ='test1';
    	userToAdd.Email ='tes2t@test.com';
 		userToAdd.EmailEncodingKey = 'ISO-8859-1';
 		userToAdd.LanguageLocaleKey = 'en_US';
 		userToAdd.LocaleSidKey = 'en_US';
 		userToAdd.TimeZoneSidKey = 'Europe/Amsterdam';
    	insert(userToAdd);    	
    	    	 	
    	List<Sub_Business_Units__c> sbus = [SELECT Id, Business_Unit__c, Business_Unit__r.Company__c, Business_Unit__r.Business_Unit_Group__c FROM Sub_Business_Units__c  WHERE Business_Unit__r.name = 'CRHF' and Business_Unit__r.Company__r.Name='Europe'];
    	Sub_Business_Units__c sbu = sbus.get(0);
    	
    	User_Business_Unit__c userBu = new User_Business_Unit__c();
    	userBu.User__c = userToAdd.Id;
    	userBu.Sub_Business_Unit__c = sbu.id;
    	insert(userBu);    	  	
    	
    	Account_Plan_Level__c apl = new Account_Plan_Level__c();
    	apl.Business_Unit__c = sbu.Business_Unit__c;
    	apl.Company__c = sbu.Business_Unit__r.Company__c;
    	apl.Account_Plan_Level__c = 'Business Unit Group';
    	apl.Account_Plan_Record_Type__c	= RecordTypeMedtronic.Iplan2('EUR_CVG_BUG').Id;
    	insert(apl);
    	    	 	
   		System.RunAs(userToAdd) { 
    	Account accountToAdd = new Account();
    	accountToAdd.Name = 'testAccount';
    	accountToAdd.RecordTypeId = RecordTypeMedtronic.Account('SAP_Account').id;
    	insert(accountToAdd); 
    	 		
    	Account_Plan_2__c iplan2 = new Account_Plan_2__c();  	
    	iplan2.Account_Plan_Level__c = 'Business Unit Group';
    	iplan2.Business_Unit__c = sbu.Business_Unit__c;
    	iplan2.Business_Unit_Group__c = sbu.Business_Unit__r.Business_Unit_Group__c;
    	iplan2.Account__c = accountToAdd.Id;
    	iplan2.RecordTypeId = RecordTypeMedtronic.Iplan2('EUR_CVG_BUG').Id;
	    insert(iplan2);
    	
    	
	    	
	    	ws_AccountPlanQueryService.AccountPerformanceRequest performanceRequest = new ws_AccountPlanQueryService.AccountPerformanceRequest();
	    	performanceRequest.accountPlan = iplan2;
	    	String JsonMsg=JSON.serialize(performanceRequest);
	    	
	    	
			RestRequest req = new RestRequest(); 
			RestResponse res = new RestResponse();
	
			req.requestURI = '/services/apexrest/AccountPlanQueryService';  
			req.httpMethod = 'POST';
			req.requestBody = Blob.valueOf(JsonMsg);
			RestContext.request = req;
			RestContext.response = res;
			
			string result =  ws_AccountPlanQueryService.doesIplanExist();
			ws_AccountPlanQueryService.AccountPerformanceResponse response =(ws_AccountPlanQueryService.AccountPerformanceResponse) JSON.deserialize(result, ws_AccountPlanQueryService.AccountPerformanceResponse.class);
			
			//System.assertEquals('There is an existing iPlan for Cardio Vascular. Please contact test1 test1 to add you to the accountplan team members.', response.message);
    	}
    }
}