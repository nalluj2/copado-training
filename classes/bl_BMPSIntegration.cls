//---------------------------------------------------------------------------------------------------------------------------------
//	Author 			: Bart Caelen
//  Created Date 	: 05-12-2019
//  Description 	: APEX Class for used for the CSOD (CornerStone On Demand) integration
//  Change Log 		: CR-27450 BPMS Integration
//---------------------------------------------------------------------------------------------------------------------------------
public class bl_BMPSIntegration  {

	@future(callout = true)
	public static void initiateOnboarding(Id id_SyncData){

		Contact_ED_Sync__c oSyncData = [SELECT Id, Contact__c, Campaign__c, Campaign__r.Curriculum_ID__c, Campaign_Member_Id__c FROM Contact_ED_Sync__c WHERE Id = :id_SyncData];

		oSyncData = initiateOnboarding(oSyncData);

		update oSyncData;

	}

	@TestVisible private static Contact_ED_Sync__c initiateOnboarding(Contact_ED_Sync__c oSyncData){
		
		String tBody = '';
		tBody += '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://integration.medtronic.com/learning/onboarding/1/" xmlns:ns1="http://integration.medtronic.com/learning/onboarding/provisionOnboardingRequest/1/">';
		   tBody += '<soapenv:Header/>';
		   tBody += '<soapenv:Body>';
			  tBody += '<ns:provisionOnboardingRequest>';
				 tBody += '<ns1:provisionOnboardingRequest>';
					tBody += '<messageIdentifier>' + oSyncData.Id + '</messageIdentifier>';
					tBody += '<consumerIdentifier>IMPACT</consumerIdentifier>';
					tBody += '<userIdentifier>' + oSyncData.Contact__c + '</userIdentifier>';
					tBody += '<campaignMemberIdentifier>' + oSyncData.Campaign_Member_Id__c + '</campaignMemberIdentifier>';
					tBody += '<campaignIdentifier>' + oSyncData.Campaign__c + '</campaignIdentifier>';
					tBody += '<curriculumIdentifier>' + oSyncData.Campaign__r.Curriculum_ID__c + '</curriculumIdentifier>';
				 tBody += '</ns1:provisionOnboardingRequest>';
			  tBody += '</ns:provisionOnboardingRequest>';
		   tBody += '</soapenv:Body>';
		tBody += '</soapenv:Envelope>';

		Http oHTTP = new Http();
		HttpRequest oHTTPRequest = new HttpRequest();
			oHTTPRequest.setEndpoint(oAPISettings.Target_server_URL__c);
			oHTTPRequest.setMethod('POST');
			oHTTPRequest.setHeader('Authorization', oAPISettings.Token_Authentication__c);
			oHTTPRequest.setHeader('Content-Length', '0');
			oHTTPRequest.setHeader('Content-Type', 'text/xml');
			oHTTPRequest.setTimeout(Integer.valueOf(oAPISettings.Timeout__c));
			oHTTPRequest.setBody(tBody);
		HttpResponse oHTTPResponse = oHTTP.send(oHTTPRequest);

		if (oHTTPResponse.getStatusCode() != 200){
			oSyncData.Outcome__c = 'Failure';
			oSyncData.Outcome_Details__c = 'Error while initiating the onboarding process: ' + oHTTPResponse.getStatus() + ' (' + oHTTPResponse.getStatusCode() + ')\n\n';
		}else{
			oSyncData.Outcome__c = 'Success';
			oSyncData.Outcome_Details__c = '';
		}
		oSyncData.Outcome_Details__c += 'REQUEST\n' + String.valueOf(oHTTPRequest.getBody()) + '\n\nRESPONSE\n' + String.valueOf(oHTTPResponse.getBody());

    	return oSyncData;

	}
	// CR-27450 BPMS Integration - STOP


	//---------------------------------------------------------------------------------------------------------------------------
	// Retrieve the API Settings based on the OrganizationId of the User
	//---------------------------------------------------------------------------------------------------------------------------
 	@TestVisible public static BPMS_API_Settings__c oAPISettings{
 		
 		get{
 			
 			if (oAPISettings == null){
 				
 				BPMS_API_Settings__c oSettings = BPMS_API_Settings__c.getInstance();
 				
 				if (oSettings.Org_Id__c != UserInfo.getOrganizationId()){
					// Don't throw an error because at the moment (09/12/2019) the old logic is used if no new settings are found.
					// throw new ws_Exception('Not API Settings available for this Salesforce Environment (' + UserInfo.getOrganizationId() + ') in BPMS API Settings (BPMS_API_Settings__c), please contact your System Administrator.');
					oSettings = null;

				}
 				
 				oAPISettings = oSettings;

 			}
 			
 			return oAPISettings;

 		}
		
		set; 		

 	}
	//---------------------------------------------------------------------------------------------------------------------------

}