//---------------------------------------------------------------------------------------------------------------------------------------------------
//  Author		:   Bart Caelen
//  Date 		:   16/10/2018
//  Description	:	This batch/scheduled Class will get create, update and delete the Call_Recording_Overview__c records based on
//						Account_Call_Topic_Count__c and Contact_Call_Topic_Count__c data
//					Executed Steps:
//						1. Collect Account ID's which are used in Account_Call_Topic_Count__c records
//						2. For each collected Account ID, process all related Account_Call_Topic_Count__c records and create / update Call_Recording_Overview__c based on these records
//						3. Delete the Call_Recording_Overview__c records for the Account ID's that are not processed in step 2
//						4. Collect Contact ID's which are used in Contact_Call_Topic_Count__c records
//						5. For each collected Contact ID, process all related Contact_Call_Topic_Count__c records and create / update Call_Recording_Overview__c based on these records
//						6. Delete the Call_Recording_Overview__c records for the Contact  ID's that are not processed in step 5
//---------------------------------------------------------------------------------------------------------------------------------------------------
global class ba_CallRecordingOverview implements Schedulable, Database.Batchable<SObject>, Database.Stateful {
	
	global String tParentSObjectType = 'ACCOUNT';
	global String tAction = 'COLLECTID';
	global Integer iBatchSize_CollectId = 2000;
	global Integer iBatchSize_DeleteData = 2000;
	global Integer iBatchSize_ProcessData = 10;

	global Set<Id> setID_Collected;
	private String tFieldName_Collected;

	// SCHEDULE Settings
    global void execute(SchedulableContext ctx){        
        
		ba_CallRecordingOverview oBatch = new ba_CallRecordingOverview();
		Database.executebatch(oBatch, 2000);

    } 


	// BATCH Settings
	global Database.QueryLocator start(Database.BatchableContext context) {

		String tSOQL = '';

		if (tParentSObjectType == 'ACCOUNT'){

			if (tAction == 'COLLECTID'){

				setID_Collected = new Set<Id>();

				tSOQL = 'SELECT Account__c FROM Account_Call_Topic_Count__c WHERE Account__c != null';
				tFieldName_Collected = 'Account__c';
		
			}else if (tAction == 'DELETEDATA'){

				// Delete Call_Recording_Overview__c records which are not used anymore - Account Related
				tSOQL = 'SELECT Id FROM Call_Recording_Overview__c WHERE (Account__c != null AND Account__c not in :setID_Collected) OR (Account__c = null AND Contact__c = null)';

			}else if (tAction == 'PROCESSDATA'){

				tSOQL = 'SELECT Id FROM Account WHERE Id = :setID_Collected';

			}

		}else if (tParentSObjectType == 'CONTACT'){
		
			if (tAction == 'COLLECTID'){

				setID_Collected = new Set<Id>();

				tSOQL = 'SELECT Contact__c FROM Contact_Call_Topic_Count__c WHERE Contact__c != null';
				tFieldName_Collected = 'Contact__c';

			}else if (tAction == 'DELETEDATA'){

				// Delete Call_Recording_Overview__c records which are not used anymore - Contact Related
				tSOQL = 'SELECT Id FROM Call_Recording_Overview__c WHERE (Contact__c != null AND Contact__c not in :setID_Collected) OR (Account__c = null AND Contact__c = null)';

			}else if (tAction == 'PROCESSDATA'){

				tSOQL = 'SELECT Id FROM Contact WHERE Id = :setID_Collected';

			}

		}

		return Database.getQueryLocator(tSOQL);

	}

   	global void execute(Database.BatchableContext context, List<sObject> lstData) {


		if (tAction == 'COLLECTID'){

			for (sObject oData : lstData) setID_Collected.add(Id.valueOf(String.valueOf(oData.get(tFieldName_Collected))));
		
		}else if (tAction == 'DELETEDATA'){

			if (lstData.size() > 0) delete lstData;

		}else if (tAction == 'PROCESSDATA'){

			Set<Id> setID_Data = new Set<Id>();
			for (SObject oData : lstData) setID_Data.add(Id.valueOf(String.valueOf(oData.get('Id'))));
			if (setID_Data.size() > 0) processData(tParentSObjectType, setID_Data);

		}else if (tAction == 'CLEANUPDATA'){

			if (lstData.size() > 0) delete lstData;
		
		}
	
	}
	
	global void finish(Database.BatchableContext context) {

		if (tAction == 'COLLECTID'){

			System.debug('** FINISH COLLECTID - ' + tParentSObjectType + ' - setID_Collected (' + setID_Collected.size() + ')');

			ba_CallRecordingOverview oBatch = new ba_CallRecordingOverview();
				oBatch.tParentSObjectType = tParentSObjectType;
				oBatch.tAction = 'DELETEDATA';
				oBatch.setID_Collected = setID_Collected;
				oBatch.iBatchSize_CollectId = iBatchSize_CollectId;
				oBatch.iBatchSize_DeleteData = iBatchSize_DeleteData;
				oBatch.iBatchSize_ProcessData = iBatchSize_ProcessData;
			if (!Test.isRunningTest()) Database.executebatch(oBatch, iBatchSize_DeleteData);

		}else if (tAction == 'DELETEDATA'){

			System.debug('** FINISH DELETEDATA - ' + tParentSObjectType);

			ba_CallRecordingOverview oBatch = new ba_CallRecordingOverview();
				oBatch.tParentSObjectType = tParentSObjectType;
				oBatch.tAction = 'PROCESSDATA';
				oBatch.setID_Collected = setID_Collected;
				oBatch.iBatchSize_CollectId = iBatchSize_CollectId;
				oBatch.iBatchSize_DeleteData = iBatchSize_DeleteData;
				oBatch.iBatchSize_ProcessData = iBatchSize_ProcessData;
			if (!Test.isRunningTest()) Database.executebatch(oBatch, iBatchSize_ProcessData);
			

		}else if (tAction == 'PROCESSDATA'){

			System.debug('** FINISH PROCESSDATA - ' + tParentSObjectType);

			if (tParentSObjectType == 'ACCOUNT'){

				// Start process for Contact
				ba_CallRecordingOverview oBatch = new ba_CallRecordingOverview();
					oBatch.tParentSObjectType = 'CONTACT';
					oBatch.tAction = 'COLLECTID';
				if (!Test.isRunningTest()) Database.executebatch(oBatch, iBatchSize_CollectId);
				
			}else if (tParentSObjectType == 'CONTACT'){

				// No actions needed

			}

		}
		
	}

	private void processData(String tParentSObjectType, Set<Id> setID_Data){

		// Get Number of Call Records for each Data - BU combination
		Map<String, Double> mapDataBU_NumCallRecord = getNumCallRecord(tParentSObjectType, setID_Data, 'Business_Unit__c');
		// Get Last Call Date for each Data - BU combination
		Map<String, Date> mapDataBU_LastCallDate = getLastCallDate(tParentSObjectType, setID_Data, 'Business_Unit__c');
		// Get all Data - BU Key's
		Set<String> setDataBU = new Set<String>();
			setDataBU.addAll(mapDataBU_NumCallRecord.keySet());
			setDataBU.addAll(mapDataBU_LastCallDate.keySet());


		// Get Number of Call Records for each Data - SBU combination
		Map<String, Double> mapDataSBU_NumCallRecord = getNumCallRecord(tParentSObjectType, setID_Data, 'Sub_Business_Unit__c');
		// Get Last Call Date for each Data - SBU combination
		Map<String, Date> mapDataSBU_LastCallDate = getLastCallDate(tParentSObjectType, setID_Data, 'Sub_Business_Unit__c');
		// Get all Data - SBU Key's
		Set<String> setDataSBU = new Set<String>();
			setDataSBU.addAll(mapDataSBU_NumCallRecord.keySet());
			setDataSBU.addAll(mapDataSBU_LastCallDate.keySet());


		// List of Call_Recording_Overview__c which will be UPSERTED
		List<Call_Recording_Overview__c> lstCallRecordingOverview = new List<Call_Recording_Overview__c>();


		// Loop through the collected Data - Business Unit Key's and create/update Call_Recording_Overview__c records
		for (String tKey : setDataBU){
			
			Id id_Data = tKey.left(18);
			Id id_BusinessUnit = tKey.right(18);

			Call_Recording_Overview__c oCallRecordingOverview = new Call_Recording_Overview__c();
				oCallRecordingOverview.Unique_Key__c = tKey;
				if (tParentSObjectType == 'ACCOUNT'){
					oCallRecordingOverview.Account__c = id_Data;
				}else if (tParentSObjectType == 'CONTACT'){
					oCallRecordingOverview.Contact__c = id_Data;
				}
				oCallRecordingOverview.Business_Unit__c = id_BusinessUnit;
				if (mapDataBU_LastCallDate.containsKey(tKey)){
					oCallRecordingOverview.Last_Call_Date__c = mapDataBU_LastCallDate.get(tKey);
				}else{
					oCallRecordingOverview.Last_Call_Date__c = null;
				}
				if (mapDataBU_NumCallRecord.containsKey(tKey)){
					oCallRecordingOverview.Number_Of_Calls__c = mapDataBU_NumCallRecord.get(tKey);
				}else{
					oCallRecordingOverview.Number_Of_Calls__c = null;
				}
			lstCallRecordingOverview.add(oCallRecordingOverview);

		}


		// Loop through the collected Data - Sub Business Unit Key's and create/update Call_Recording_Overview__c records
		for (String tKey : setDataSBU){
			
			Id id_Data = tKey.left(18);
			Id id_SubBusinessUnit = tKey.right(18);

			Call_Recording_Overview__c oCallRecordingOverview = new Call_Recording_Overview__c();
				oCallRecordingOverview.Unique_Key__c = tKey;
				if (tParentSObjectType == 'ACCOUNT'){
					oCallRecordingOverview.Account__c = id_Data;
				}else if (tParentSObjectType == 'CONTACT'){
					oCallRecordingOverview.Contact__c = id_Data;
				}
				oCallRecordingOverview.Sub_Business_Unit__c = id_SubBusinessUnit;
				if (mapDataSBU_LastCallDate.containsKey(tKey)){
					oCallRecordingOverview.Last_Call_Date__c = mapDataSBU_LastCallDate.get(tKey);
				}else{
					oCallRecordingOverview.Last_Call_Date__c = null;
				}
				if (mapDataSBU_NumCallRecord.containsKey(tKey)){
					oCallRecordingOverview.Number_Of_Calls__c = mapDataSBU_NumCallRecord.get(tKey);
				}else{
					oCallRecordingOverview.Number_Of_Calls__c = null;
				}
			lstCallRecordingOverview.add(oCallRecordingOverview);

		}

		// Upsert Call_Recording_Overview__c based on the Unique_Key__c field to create new records and update existing records
		upsert lstCallRecordingOverview Unique_Key__c;
	
		// Delete the Call Records for the processed Accounts / Contacts that haven't been created or updated - they are out of scope
		List<Call_Recording_Overview__c> lstCallRecordingOverview_Delete;
		if (tParentSObjectType == 'ACCOUNT'){

			lstCallRecordingOverview_Delete = [SELECT Id FROM Call_Recording_Overview__c WHERE Account__c = :setID_Data AND Id != :lstCallRecordingOverview];

		}else if (tParentSObjectType == 'CONTACT'){

			lstCallRecordingOverview_Delete = [SELECT Id FROM Call_Recording_Overview__c WHERE Contact__c = :setID_Data AND Id != :lstCallRecordingOverview];

		}

		delete lstCallRecordingOverview_Delete;

	}


	// Get Number of Call Records for each Data - BU / SBU combination
	private Map<String, Double> getNumCallRecord(String tParentSObjectType, Set<Id> setID_Parent, String tFieldGroupBy){

		Map<String, Double> mapData = new Map<String, Double>();
		
		String tSObject = '';
		String tFieldParent = '';

		if (tParentSObjectType == 'ACCOUNT'){
			
			tSObject = 'Account_Call_Topic_Count__c';
			tFieldParent = 'Account__c';

		}else if (tParentSObjectType == 'CONTACT'){

			tSObject = 'Contact_Call_Topic_Count__c';
			tFieldParent = 'Contact__c';

		}

		String tSOQL = 'SELECT ' + tFieldParent + ', ' + tFieldGroupBy + ', COUNT_Distinct(Call_Recording__c) countCallRecording';
		tSOQL += ' FROM ' + tSObject;
		tSOQL += ' WHERE active__c = true AND ' + tFieldParent + ' in :setID_Parent AND ' + tFieldGroupBy + ' <> null';
		tSOQL += ' GROUP BY ' + tFieldParent + ', ' + tFieldGroupBy;

		List<AggregateResult> lstAggResult = Database.query(tSOQL);

        for (AggregateResult oAggResult : lstAggResult){

			String tKey = '' + (ID)oAggResult.get(tFieldParent) + (ID)oAggResult.get(tFieldGroupBy);
			mapData.put(tKey, Double.valueOf(oAggResult.get('countCallRecording')));

		}                    

		return mapData ;

	}

	// Get Last Call Date of Call Records for each Data - BU / SBU combination
	private Map<String, Date> getLastCallDate(String tParentSObjectType, Set<Id> setID_Parent, String tFieldGroupBy){

		Map<String, Date> mapData = new Map<String, Date>();

		String tSObject = '';
		String tFieldParent = '';
		String tFieldCallDate = '';

		if (tParentSObjectType == 'ACCOUNT'){
			
			tSObject = 'Account_Call_Topic_Count__c';
			tFieldParent = 'Account__c';
			tFieldCallDate = 'Visit_Date__c';

		}else if (tParentSObjectType == 'CONTACT'){

			tSObject = 'Contact_Call_Topic_Count__c';
			tFieldParent = 'Contact__c';
			tFieldCallDate = 'Call_Date__c';

		}

		String tSOQL = 'SELECT ' + tFieldParent + ', ' + tFieldGroupBy + ', Max(' + tFieldCallDate + ') lastCallDate';
		tSOQL += ' FROM ' + tSObject;
		tSOQL += ' WHERE ' + tFieldParent + ' in :setID_Parent AND ' + tFieldGroupBy + ' <> null';
		tSOQL += ' GROUP BY ' + tFieldParent + ', ' + tFieldGroupBy;

		List<AggregateResult> lstAggResult = Database.query(tSOQL);

        for (AggregateResult oAggResult : lstAggResult){

			String tKey = '' + (ID)oAggResult.get(tFieldParent) + (ID)oAggResult.get(tFieldGroupBy);
			mapData.put(tKey, Date.valueOf(oAggResult.get('lastCallDate')));

		}                    

		return mapData ;

	}

}
//---------------------------------------------------------------------------------------------------------------------------------------------------