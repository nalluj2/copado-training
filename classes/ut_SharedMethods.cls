/*
 * Description		: Utility class to support shared methods	 
 * Author        	: Patrick Brinksma
 * Created Date    	: 26-09-2013
 */
public class ut_SharedMethods {

    // Patrick Brinksma - 26 Sep 2013 - Copied from SharedMethods.getUserBusinessUnits and adjusted for SubBusinessUnits
    public static map<id,string> getUserSubBusinessUnits(){
        try {       
            Id userId = UserInfo.getUserId();
            
            map<id,string> mapUserSBUs = new map<id,string>();
            List<User_Business_Unit__c> lstUserSBUs = [Select Sub_Business_Unit__c, Sub_Business_Unit_name__c From User_Business_Unit__c where User__c=:userId order by Sub_Business_Unit_name__c];
            for (User_Business_Unit__c UserSBU:lstUserSBUs) {
                mapUserSBUs.put(UserSBU.Sub_Business_Unit__c,UserSBU.Sub_Business_Unit_name__c); 
            }
            return mapUserSBUs;    
        } catch (DmlException ex){
              ApexPages.addMessages(ex);
              return null;
        }               
    }
    
    public static List<string> getUserBuIds(){
	 	try {       
            Id userId = UserInfo.getUserId();
            
            List<string> mapUserSBUs = new List<string>();
            List<User_Business_Unit__c> lstUserSBUs = [Select Business_Unit_id__c, Sub_Business_Unit_name__c From User_Business_Unit__c where User__c=:userId order by Sub_Business_Unit_name__c];
            System.debug('listUserSBUs '+lstUserSBUs +' userId '+userId);
            
            for (User_Business_Unit__c UserSBU:lstUserSBUs) {
                mapUserSBUs.add(UserSBU.Business_Unit_id__c); 
            }
            return mapUserSBUs;    
        } catch (DmlException ex){
              ApexPages.addMessages(ex);
              return null;
        }    
    }

}