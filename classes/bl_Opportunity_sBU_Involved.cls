public class bl_Opportunity_sBU_Involved {
    
    public static Boolean skipValidation = false;
    
    public static void setUniqueKey(List<Opportunity_sBU_Involved__c> triggerNew){
    	
    	Set<Id> oppBUinvolvedIds = new Set<Id>();
    	
    	for(Opportunity_sBU_Involved__c sBU : triggerNew) oppBUinvolvedIds.add(sBU.Opportunity_BU_Involved__c);
    	
    	Map<Id, Opportunity_BU_Involved__c> oppBUInvolved = new Map<Id, Opportunity_BU_Involved__c>([Select Id, Opportunity__c from Opportunity_BU_Involved__c where Id IN :oppBUInvolvedIds]);
    	
    	for(Opportunity_sBU_Involved__c sBU : triggerNew){
    		
    		sBU.Unique_Key__c = oppBUInvolved.get(sBU.Opportunity_BU_Involved__c).Opportunity__c + ':' + sBU.Name;
    	}
    }
    
    public static void validateTotalsAndCalculateSummary(List<Opportunity_sBU_Involved__c> triggerNew){
    	
    	Set<Id> buInvolvedIds = new Set<Id>();
    	
    	for(Opportunity_sBU_Involved__c sBU : triggerNew) buInvolvedIds.add(sBU.Opportunity_BU_Involved__c);	
    	
    	List<Opportunity_BU_Involved__c> buInvolved = [Select Id, Name, BU_Percentage__c, (Select Id, Name, Percentage__c from Opportunity_sBU_s_Involved__r order by Name ASC) from Opportunity_BU_Involved__c where Id IN :buInvolvedIds];
    	
    	for(Opportunity_BU_Involved__c bu : buInvolved){
    		
    		List<String> sbuSummary = new List<String>();
    		
    		Boolean hasPercentage = false;
    		Decimal total = 0;
    		
    		for(Opportunity_sBU_Involved__c sbu : bu.Opportunity_sBU_s_Involved__r){
    			    			    			
    			if(sbu.Percentage__c != null){
    				
    				hasPercentage = true;
    				total += sbu.Percentage__c;
    				sbuSummary.add(sbu.Name + ' - ' + sbu.Percentage__c + '%');    				
    				
    			}else{
    				
    				sbuSummary.add(sbu.Name);
    			}
    		}
    		
    		if(hasPercentage == true && skipValidation == false){
    			
    			for(Opportunity_sBU_Involved__c sbu : bu.Opportunity_sBU_s_Involved__r){
    				
    				if(sbu.Percentage__c == null) throw new ValidationException(bu.Name + ' : ' + 'If at least one of the Sub-Business Units has a percentage value, all the other Sub-Business Units of the same Business Unit must have a percentage value too');
    			}
    			
    			if(total != bu.BU_Percentage__c) throw new ValidationException(bu.Name + ' : ' + 'The sum of the Sub-Business Units percentages must be equal to the Business Unit percentage');
    		}
    		
    		bu.sBUs_Involved__c = String.join(sbuSummary, ';');
    	}
    	
    	update buInvolved;
    }
}