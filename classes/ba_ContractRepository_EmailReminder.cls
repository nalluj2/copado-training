//---------------------------------------------------------------------------------------------------------------------------------------------------
//  Author      :   Bart Caelen
//  Date        :   22/07/2020
//  Description :   This Batch/Scheduled APEX Class will process Contract_Repository__c records and send Email Reminders based on the Reminder Date fields
//                      provided on the Contract_Repository__c record.
//
//                  * Only Active Contracts need to be processed.
//                  * The emails need to be send to the users mentioned in the Contract Repository Team
//                      * on the 1st reminder date (Email_Reminder_Date_1__c) the email should be send all Team Members where Email_Reminder__c (multiselect) contains 1
//                      * on the 2nd reminder date (Email_Reminder_Date_2__c) the email should be send all Team Members where Email_Reminder__c (multiselect) contains 2
//                      * on the 3nd reminder date (Email_Reminder_Date_3__c) the email should be send all Team Members where Email_Reminder__c (multiselect) contains 3
//
//                  * A Country specific email address will be added in BCC which is stored in the Contract_Repository_Setting__c related to the Country
//                  * The email will use a specific Email Template for each reminder (1,2 and 3) which is stored in the Contract_Repository_Setting__c related to the Country 
//                  * Sending the reminder emails will also stop when new contract is created and linked to the existing processing contract
//                  * When a Contract Repository is linked to a Contract Repository Bundle, the emails will be send only 1 time for each Contract Repository
//
//---------------------------------------------------------------------------------------------------------------------------------------------------
global class ba_ContractRepository_EmailReminder implements Schedulable, Database.Batchable<SObject>, Database.Stateful {

    global String tFolder_EmailTemplate = 'Contract_Email_Templates'; // DeveloperName of the Folder where the Contract Repository Email Templates are stored
    global Set<String> setStatusContract_Active = new Set<String>{'In Progress','Sent to customer','Sent to sales rep','Signed by Customer','Active'};

    global Map<String, Id> mapEmailTemplateName_Id;

    global Map<String, Contract_Repository_Setting__c> mapCountry_ContractRepositorySetting;

    global Set<Id> setID_ContractRepositoryBundle_EmailSend_1;
    global Set<Id> setID_ContractRepositoryBundle_EmailSend_2;
    global Set<Id> setID_ContractRepositoryBundle_EmailSend_3;

    global String tSendDisplayEmail = 'Contract Repository Email Reminder';

    private List<String> lstError;


    // SCHEDULE Settings
    global void execute(SchedulableContext ctx){        
        
        ba_ContractRepository_EmailReminder oBatch = new ba_ContractRepository_EmailReminder();
        Database.executebatch(oBatch, 200);

    } 


    // BATCH Settings
    global Database.QueryLocator start(Database.BatchableContext context) {

        lstError = new List<String>();

        Set<String> setEmailTemplateName = new Set<String>();
            setID_ContractRepositoryBundle_EmailSend_1 = new Set<Id>();
            setID_ContractRepositoryBundle_EmailSend_2 = new Set<Id>();
            setID_ContractRepositoryBundle_EmailSend_3 = new Set<Id>();

        // Get the Contract_Repository_Setting__c records for each Country
        mapCountry_ContractRepositorySetting = new Map<String, Contract_Repository_Setting__c>();
        List<Contract_Repository_Setting__c> lstContractRepositorySetting = [SELECT Country__r.Name, Email_Template_Name_1__c, Email_Template_Name_2__c, Email_Template_Name_3__c, BCC_Email_Address__c,Contract_Type__c FROM Contract_Repository_Setting__c];
        for (Contract_Repository_Setting__c oContractRepositorySetting : lstContractRepositorySetting){

            mapCountry_ContractRepositorySetting.put(oContractRepositorySetting.Country__r.Name.toUpperCase()+':'+oContractRepositorySetting.Contract_Type__c, oContractRepositorySetting);
            
            if (oContractRepositorySetting.Email_Template_Name_1__c != null) setEmailTemplateName.add(oContractRepositorySetting.Email_Template_Name_1__c);
            if (oContractRepositorySetting.Email_Template_Name_2__c != null) setEmailTemplateName.add(oContractRepositorySetting.Email_Template_Name_2__c);
            if (oContractRepositorySetting.Email_Template_Name_3__c != null) setEmailTemplateName.add(oContractRepositorySetting.Email_Template_Name_3__c);

        }

        // Get all the Email Templates related to the Contract Repository logic
        mapEmailTemplateName_Id = new Map<String, Id>();
        List<EmailTemplate> lstEmailTemplate = [SELECT Id, Name, DeveloperName FROM EmailTemplate WHERE Folder.DeveloperName = :tFolder_EmailTemplate AND DeveloperName in :setEmailTemplateName];
        for (EmailTemplate oEmailTemplate : lstEmailTemplate){
            
            mapEmailTemplateName_Id.put(oEmailTemplate.DeveloperName, oEmailTemplate.Id);

        }

        Date dDateToday = Date.today();
        return Database.getQueryLocator('SELECT Id, Account__r.Account_Country_vs__c, Email_Reminder_Date_1__c, Email_Reminder_Date_2__c, Email_Reminder_Date_3__c, Contract_Repository_Bundle__c,Primary_Contract_Type__c, (SELECT Id FROM Contract_Repository__r), (SELECT Team_Member__c, Email__c, Email_Reminder__c FROM Contract_Repository_Teams__r WHERE Email_Reminder__c != null) FROM Contract_Repository__c WHERE Contract_Status__c = :setStatusContract_Active AND ( Email_Reminder_Date_1__c = :dDateToday OR Email_Reminder_Date_2__c = :dDateToday OR Email_Reminder_Date_3__c = :dDateToday )');

    }


    global void execute(Database.BatchableContext context, List<Contract_Repository__c> lstContractRepository) {

        List<Messaging.SingleEmailMessage> lstEmail = new List<Messaging.SingleEmailMessage>();

        for (Contract_Repository__c oContractRepository : lstContractRepository){

            if (oContractRepository.Contract_Repository__r.size() > 0) continue;    // We don't need to process Contract_Repository__c which are indicated as a Previous_Contract__c on a new Contract_Repository__c record

            // Get the Contract_Repository_Setting__c for the Country related to the Account of the Contract_Repository__c record
            Contract_Repository_Setting__c oContractRepositorySetting;
            if (mapCountry_ContractRepositorySetting.containsKey(oContractRepository.Account__r.Account_Country_vs__c+':'+oContractRepository.Primary_Contract_Type__c)){

                oContractRepositorySetting = mapCountry_ContractRepositorySetting.get(oContractRepository.Account__r.Account_Country_vs__c+':'+oContractRepository.Primary_Contract_Type__c);
            
            }

            // Verify that a Contract_Repository_Setting__c is available
            if (oContractRepositorySetting == null){

                System.debug('Error in ba_ContractRepository_EmailReminder - no Contract Repository Setting record found for the Country and Contract Type "' + oContractRepository.Account__r.Account_Country_vs__c+':'+oContractRepository.Primary_Contract_Type__c + '" on the Contract Repository record "' + oContractRepository.Id + '"');
                lstError.add('Error while processing Contract Repository record "' + oContractRepository.Id + '" : no Contract Repository Setting record found for the Country and Contract Type "' + oContractRepository.Account__r.Account_Country_vs__c+':'+oContractRepository.Primary_Contract_Type__c + '"');
                continue;

            }


            if (oContractRepository.Email_Reminder_Date_1__c == Date.today()){

                if (!setID_ContractRepositoryBundle_EmailSend_1.contains(oContractRepository.Contract_Repository_Bundle__c)){
    
                    Id id_EmailTemplate;

                    // Get the Email Template Id based on the Email Template Name
                    if (mapEmailTemplateName_Id.containsKey(oContractRepositorySetting.Email_Template_Name_1__c)){

                        id_EmailTemplate = mapEmailTemplateName_Id.get(oContractRepositorySetting.Email_Template_Name_1__c);

                    }else{

                        System.debug('Error in ba_ContractRepository_EmailReminder - no Email Template Name 1 "' + oContractRepositorySetting.Email_Template_Name_1__c + '" found in the folder "' + tFolder_EmailTemplate + '" for the Country "' + oContractRepository.Account__r.Account_Country_vs__c + '" on the Contract Repository record "' + oContractRepository.Id + '"');
                        lstError.add('Error while processing Contract Repository record "' + oContractRepository.Id + '" : no Email Template Name 1 "' + oContractRepositorySetting.Email_Template_Name_1__c + '" found in the folder "' + tFolder_EmailTemplate + '" for the Country "' + oContractRepository.Account__r.Account_Country_vs__c + '"');

                    }

                    if (id_EmailTemplate != null){
                
                        // Send an Email to all Contract Repository Team Members where Email_Reminder__c contains 1 
                        for (Contract_Repository_Team__c oContractRepositoryTeam : oContractRepository.Contract_Repository_Teams__r){
                
                            if (oContractRepositoryTeam.Email_Reminder__c.contains('1')){
                        
                                Messaging.SingleEmailMessage oEmail = createEmail(id_EmailTemplate, oContractRepositoryTeam.Team_Member__c, oContractRepository.Id, oContractRepositorySetting.BCC_Email_Address__c);
                                lstEmail.add(oEmail);

                            }
                
                        }

                        if (oContractRepository.Contract_Repository_Bundle__c != null) setID_ContractRepositoryBundle_EmailSend_1.add(oContractRepository.Contract_Repository_Bundle__c);

                    }

                }

            }

            
            if (oContractRepository.Email_Reminder_Date_2__c == Date.today()){

                if (!setID_ContractRepositoryBundle_EmailSend_2.contains(oContractRepository.Contract_Repository_Bundle__c)){

                    Id id_EmailTemplate;

                    // Get the Email Template Id based on the Email Template Name
                    if (mapEmailTemplateName_Id.containsKey(oContractRepositorySetting.Email_Template_Name_2__c)){

                        id_EmailTemplate = mapEmailTemplateName_Id.get(oContractRepositorySetting.Email_Template_Name_2__c);

                    }else{

                        System.debug('Error in ba_ContractRepository_EmailReminder - no Email Template Name 2 "' + oContractRepositorySetting.Email_Template_Name_1__c + '" found in the folder "' + tFolder_EmailTemplate + '" for the Country "' + oContractRepository.Account__r.Account_Country_vs__c + '" on the Contract Repository record "' + oContractRepository.Id + '"');
                        lstError.add('Error while processing Contract Repository record "' + oContractRepository.Id + '" : no Email Template Name 2 "' + oContractRepositorySetting.Email_Template_Name_1__c + '" found in the folder "' + tFolder_EmailTemplate + '" for the Country "' + oContractRepository.Account__r.Account_Country_vs__c + '"');

                    }

                    if (id_EmailTemplate != null){

                        // Send an Email to all Contract Repository Team Members where Email_Reminder__c contains 2
                        for (Contract_Repository_Team__c oContractRepositoryTeam : oContractRepository.Contract_Repository_Teams__r){
                
                            if (oContractRepositoryTeam.Email_Reminder__c.contains('2')){
                    
                                Messaging.SingleEmailMessage oEmail = createEmail(id_EmailTemplate, oContractRepositoryTeam.Team_Member__c, oContractRepository.Id, oContractRepositorySetting.BCC_Email_Address__c);
                                lstEmail.add(oEmail);

                            }
                
                        }

                        if (oContractRepository.Contract_Repository_Bundle__c != null) setID_ContractRepositoryBundle_EmailSend_2.add(oContractRepository.Contract_Repository_Bundle__c);

                    }

                }

            }

        
            if (oContractRepository.Email_Reminder_Date_3__c == Date.today()){

                if (!setID_ContractRepositoryBundle_EmailSend_3.contains(oContractRepository.Contract_Repository_Bundle__c)){
                
                    Id id_EmailTemplate;

                    // Get the Email Template Id based on the Email Template Name
                    if (mapEmailTemplateName_Id.containsKey(oContractRepositorySetting.Email_Template_Name_3__c)){

                        id_EmailTemplate = mapEmailTemplateName_Id.get(oContractRepositorySetting.Email_Template_Name_3__c);

                    }else{

                        System.debug('Error in ba_ContractRepository_EmailReminder - no Email Template Name 3 "' + oContractRepositorySetting.Email_Template_Name_1__c + '" found in the folder "' + tFolder_EmailTemplate + '" for the Country "' + oContractRepository.Account__r.Account_Country_vs__c + '" on the Contract Repository record "' + oContractRepository.Id + '"');
                        lstError.add('Error while processing Contract Repository record "' + oContractRepository.Id + '" : no Email Template Name 3 "' + oContractRepositorySetting.Email_Template_Name_1__c + '" found in the folder "' + tFolder_EmailTemplate + '" for the Country "' + oContractRepository.Account__r.Account_Country_vs__c + '"');

                    }

                    if (id_EmailTemplate != null){

                        // Send an Email to all Contract Repository Team Members where Email_Reminder__c contains 3 
                        for (Contract_Repository_Team__c oContractRepositoryTeam : oContractRepository.Contract_Repository_Teams__r){
                
                            if (oContractRepositoryTeam.Email_Reminder__c.contains('3')){
                    
                                Messaging.SingleEmailMessage oEmail = createEmail(id_EmailTemplate, oContractRepositoryTeam.Team_Member__c, oContractRepository.Id, oContractRepositorySetting.BCC_Email_Address__c);
                                lstEmail.add(oEmail);

                            }
                
                        }

                        if (oContractRepository.Contract_Repository_Bundle__c != null) setID_ContractRepositoryBundle_EmailSend_3.add(oContractRepository.Contract_Repository_Bundle__c);

                    }

                }

            }

        }


        if (lstEmail.size() > 0){

            List<Messaging.SendEmailResult> lstSendEmailResult = Messaging.sendEmail(lstEmail, false);

            for (Messaging.SendEmailResult oSendEmailResult : lstSendEmailResult){

                if (!oSendEmailResult.isSuccess()){

                    for (Messaging.SendEmailError oSendEmailError : oSendEmailResult.getErrors()){
                        System.debug('**BC** Send Email Failure : ' + oSendEmailError.getMessage());
                        lstError.add('Error while sending the emails : ' + oSendEmailError.getMessage());
                    }


                }

            }

        }
    
    }
    

    global void finish(Database.BatchableContext context) {

        if (Test.isRunningTest() && lstError.size() == 0) lstError.add('dummy test error');
        
        if (lstError.size() > 0){

            String tEmailBody = 'Error while processing ba_ContractRepository_EmailReminder : <br /><br />';
            
            for (String tError : lstError){
                tEmailBody += tError + '<br />';
            }

            // Send Error Email
            Messaging.SingleEmailMessage oEmail_Error = new Messaging.SingleEmailMessage();
                oEmail_Error.setTargetObjectId(UserInfo.getUserId());
                oEmail_Error.setSubject('ba_ContractRepository_EmailReminder errors');
                oEmail_Error.setHtmlBody(tEmailBody);
                oEmail_Error.setUseSignature(false);
                oEmail_Error.setSaveAsActivity(false);

            List<Messaging.SendEmailResult> lstSendEmailResult = Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{oEmail_Error}, false);

        }

    }


    private Messaging.SingleEmailMessage createEmail(Id id_EmailTemplate, Id id_User, Id id_ContractRepository, String tBCC_Email){

        Messaging.SingleEmailMessage oEmail = Messaging.renderStoredEmailTemplate(id_EmailTemplate, id_User, id_ContractRepository);

        String tEmailSubject = oEmail.getSubject();
        String tEmailPlainTextBody = oEmail.getPlainTextBody();

        oEmail.setTargetObjectId(id_User);
        oEmail.setTemplateId(id_EmailTemplate);
        oEmail.setSubject(tEmailSubject);
        if (!String.isBlank(tBCC_Email)) oEmail.setBccAddresses(New List<String>{tBCC_Email});
        oEmail.setPlainTextBody(tEmailPlainTextBody);
        oEmail.setUseSignature(false);
        oEmail.setSaveAsActivity(false);
        oEmail.setSenderDisplayName(tSendDisplayEmail);

        return oEmail;  

    }

}
//---------------------------------------------------------------------------------------------------------------------------------------------------