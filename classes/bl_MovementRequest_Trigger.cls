//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 03-05-2016
//  Description      : APEX Class - Business Logic for tr_MovementRequest
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public with sharing class bl_MovementRequest_Trigger {

    //--------------------------------------------------------------------------------------------------------------------
    // Public variables coming from the trigger
    //--------------------------------------------------------------------------------------------------------------------
    public static List<Movement_Request__c> lstTriggerNew = new List<Movement_Request__c>();
    public static Map<Id, Movement_Request__c> mapTriggerNew = new Map<Id, Movement_Request__c>();
    public static Map<Id, Movement_Request__c> mapTriggerOld = new Map<Id, Movement_Request__c>();
    //--------------------------------------------------------------------------------------------------------------------


    //--------------------------------------------------------------------------------------------------------------------
    // Validate if a Demo Productis loaned to another Hospital and prevent another loan
    //--------------------------------------------------------------------------------------------------------------------
    public static void validateProductLoanToOtherHospital(){

        // Check if same demo product is being loaned to another hospital
        Map<String, Date> mapDemoProductName_StartDate = new Map<String, Date>();
        Map<String, DateTime> mapDemoProductName_StartDateTime = new Map<String, DateTime>();
        Map<String, DateTime> mapDemoProductName_EndDateTime = new Map<String, DateTime>();
        Map<String, String> mapDemoProductName_HospitalName = new Map<String, String>();
        
        for (Movement_Request__c oMovementRequest : lstTriggerNew){
            mapDemoProductName_StartDate.put(oMovementRequest.Demo_Product_Name__c, oMovementRequest.Start_Date__c);
            mapDemoProductName_StartDateTime.put(oMovementRequest.Demo_Product_Name__c, oMovementRequest.Start_Date_and_Time__c);
            mapDemoProductName_EndDateTime.put(oMovementRequest.Demo_Product_Name__c, oMovementRequest.End_Date_and_Time__c);
            mapDemoProductName_HospitalName.put(oMovementRequest.Demo_Product_Name__c, oMovementRequest.Hospital__c);
        }
            
        List<Movement_Request__c> lstMovementRequest = 
        	[
        		SELECT
        			Id, Name, Hospital__c, Demo_Product_Name__c, Reloan__c
        			, Start_Date_and_Time__c, End_Date_and_Time__c, End_Date__c
				FROM 
					Movement_Request__c 
				WHERE 
					(
						Demo_Product_Name__c in :mapDemoProductName_StartDate.keySet()
						AND Approval_Status__c = 'Approved'
					) 
					OR Demo_Product_Name__c in :mapDemoProductName_StartDateTime.keySet()
			];
        
        for (Movement_Request__c oMovementRequest_New : lstTriggerNew){
            
            for (Movement_Request__c oMovementRequest : lstMovementRequest){

                if (
					(oMovementRequest.End_Date__c > mapDemoProductName_StartDate.get(oMovementRequest.Demo_Product_Name__c)) 
					|| 
					(
						(oMovementRequest.End_Date_and_Time__c > mapDemoProductName_StartDateTime.get(oMovementRequest.Demo_Product_Name__c))
						&& 
                		(oMovementRequest.Start_Date_and_Time__c < mapDemoProductName_EndDateTime.get(oMovementRequest.Demo_Product_Name__c))
                	)
                ){
                    oMovementRequest_New.addError('Demo Product is currently being loaned at another place');
                    return;
                }
                
                if (
                	(oMovementRequest.Hospital__c == mapDemoProductName_HospitalName.get(oMovementRequest.Demo_Product_Name__c))
                	&& 
                	(oMovementRequest.Reloan__c == true)
                	&& 
                    (oMovementRequest.End_Date__c.addDays(30) > mapDemoProductName_StartDate.get(oMovementRequest.Demo_Product_Name__c))
                ){
                    oMovementRequest_New.addError('Demo Product cannot be loaned more than one time');
                    return;
                }
            }
        }
	}
    //--------------------------------------------------------------------------------------------------------------------

	
    //--------------------------------------------------------------------------------------------------------------------
    // When a Movement Request is created we need to add the Owner of the Demo Product to the Opportunity Team
    //--------------------------------------------------------------------------------------------------------------------
    public static void addProductManagerToOpportunityTeam(){

        // Get a Map of all Demo Products and the related Opportunities
        Set<Id> setID_Opportunity = new Set<Id>();
        Set<Id> setID_DemoProduct = new Set<Id>();
        Map<Id, Set<Id>> mapDemoProductId_OpportunityId = new Map<Id, Set<Id>>();
        for (Movement_Request__c oMovementRequest : lstTriggerNew){
            if ( (oMovementRequest.Opportunity__c != null) && (oMovementRequest.Demo_Product_Name__c != null) ){
                setID_Opportunity.add(oMovementRequest.Opportunity__c);
                setID_DemoProduct.add(oMovementRequest.Demo_Product_Name__c);
            }
        }

        clsUtil.debug('setID_Opportunity (' + setID_Opportunity.size() + ') : ' + setID_Opportunity);
        clsUtil.debug('setID_DemoProduct (' + setID_DemoProduct.size() + ') : ' + setID_DemoProduct);

        // Get the Active Owners of the collected Demo Products
        Map<Id, Demo_Product__c> mapDemoProductId_OwnerId = new Map<Id, Demo_Product__c>();
        if (setID_DemoProduct.size() > 0){
            mapDemoProductId_OwnerId = new Map<Id, Demo_Product__c>(            
                [
                    SELECT 
                        Id, OwnerId, Owner.IsActive
                    FROM 
                        Demo_Product__c
                    WHERE
                        Id = :setID_DemoProduct
                ]);
        }
        clsUtil.debug('mapDemoProductId_OwnerId (' + mapDemoProductId_OwnerId.size() + ') : ' + mapDemoProductId_OwnerId);


        // Get all Existing OpportunityTeamMembers
        Map<Id, Set<Id>> mapOpportunityId_OpportunityTeamMemberUserId = new Map<Id, Set<Id>>();
        if (setID_Opportunity.size() > 0){
            List<OpportunityTeamMember> lstOpportunityTeamMember = 
                [
                    SELECT 
                        Id, OpportunityId, UserId 
                    FROM
                        OpportunityTeamMember
                    WHERE 
                        OpportunityId = :setID_Opportunity
                    ORDER BY 
                        OpportunityId
                ];
            for (OpportunityTeamMember oOpportunityTeamMember : lstOpportunityTeamMember){
                Set<Id> setID_OpportunityTeamMemberUserId = new Set<Id>();
                if (mapOpportunityId_OpportunityTeamMemberUserId.containsKey(oOpportunityTeamMember.OpportunityId)){
                    setID_OpportunityTeamMemberUserId = mapOpportunityId_OpportunityTeamMemberUserId.get(oOpportunityTeamMember.OpportunityId);
                }
                mapOpportunityId_OpportunityTeamMemberUserId.put(oOpportunityTeamMember.OpportunityId, setID_OpportunityTeamMemberUserId);
            }
        }
        clsUtil.debug('mapOpportunityId_OpportunityTeamMemberUserId (' + mapOpportunityId_OpportunityTeamMemberUserId.size() + ') : ' + mapOpportunityId_OpportunityTeamMemberUserId);


        List<OpportunityTeamMember> lstOpportunityTeamMember_Insert = new List<OpportunityTeamMember>();
        for (Movement_Request__c oMovementRequest : lstTriggerNew){

            if ( (oMovementRequest.Opportunity__c != null) && (oMovementRequest.Demo_Product_Name__c != null) ){

                if (mapDemoProductId_OwnerId.containsKey(oMovementRequest.Demo_Product_Name__c)){

                    Id id_DemoProductOwner = mapDemoProductId_OwnerId.get(oMovementRequest.Demo_Product_Name__c).OwnerId;

                    if (mapOpportunityId_OpportunityTeamMemberUserId.containsKey(oMovementRequest.Opportunity__c)){

                        Set<Id> setId_OpportunityTeamMember = mapOpportunityId_OpportunityTeamMemberUserId.get(oMovementRequest.Opportunity__c);

                        if (setId_OpportunityTeamMember.contains(id_DemoProductOwner)){
                            continue;
                        }

                    }

                    OpportunityTeamMember oOpportunityTeamMember = new OpportunityTeamMember();
                        oOpportunityTeamMember.OpportunityId = oMovementRequest.Opportunity__c;
                        oOpportunityTeamMember.UserId = id_DemoProductOwner;
                    lstOpportunityTeamMember_Insert.add(oOpportunityTeamMember);

                }
            }
        }

        clsUtil.debug('lstOpportunityTeamMember_Insert (' + lstOpportunityTeamMember_Insert.size() + ') : ' + lstOpportunityTeamMember_Insert);

        if (lstOpportunityTeamMember_Insert.size() > 0){
            insert lstOpportunityTeamMember_Insert;
        }

    }
    //--------------------------------------------------------------------------------------------------------------------
}
//--------------------------------------------------------------------------------------------------------------------