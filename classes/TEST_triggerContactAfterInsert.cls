/**
 * Creation Date :  20090218
 * Description :    Test Coverage of the trigger 'ContactAfterInsert' (for the Contact Object).
 *                  (To facilitate the development of robust, error-free code, Apex Code requires the creation and execution of unit tests.
 *                  Unit tests are comprised of test methods and classes that verify whether a particular piece of code is working properly.)
 * Author :         ABSI - MC
 */
@isTest
private class TEST_triggerContactAfterInsert {
    static TestMethod void triggerContactAfterInsert(){
        System.debug(' ################## ' + 'BEGIN TEST_triggerContactAfterInsert' + ' ################');
        Account acc1 = new Account();
        acc1.Name = 'TEST_triggerContactAfterInsert Test Coverage Account 1'; 
        acc1.SAP_Account_Type__c='Sold-to party'; 
        
        Test.startTest();
        insert acc1 ;
         
        Contact cont1 = new Contact();
        cont1.LastName = 'TEST_triggerContactAfterInsert Test Coverage' ;  
		cont1.FirstName = 'test';
        cont1.AccountId = acc1.Id ;
        cont1.Contact_Active__c = true  ; 
        cont1.Affiliation_To_Account__c = 'Employee';
	    cont1.Contact_Department__c = 'Diabetes Adult'; 
	    cont1.Contact_Primary_Specialty__c = 'ENT';	    
	    cont1.Primary_Job_Title_vs__c = 'Manager';
	    cont1.Contact_Gender__c = 'Male'; 
        insert cont1 ;
        Test.stopTest();
                
        List <Affiliation__c> affs = [Select Id, Affiliation_From_Contact__c, Affiliation_To_Account__c, Affiliation_Primary__c, Affiliation_Start_Date__c from Affiliation__c where Affiliation_From_Contact__c =:cont1.Id limit 1];
        System.assertEquals(affs.size(), 1);                            // It must have only one affiliation (the one created by the Apex trigger)! 
        System.assertEquals(affs[0].Affiliation_From_Contact__c,cont1.Id); 
        System.assertEquals(affs[0].Affiliation_To_Account__c,cont1.AccountId); 
        System.assertNotEquals(affs[0].Affiliation_Start_Date__c,null); 
        
        System.debug(' ################## ' + 'END TEST_triggerContactAfterInsert' + ' ################');
            
    }
}