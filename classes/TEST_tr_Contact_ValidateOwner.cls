//------------------------------------------------------------------------------------------------------------
//  Author      : Bart Caelen
//  Date        : 2015-09-01
//  Description : CR-9426
//                  This is the test class for the APEX Trigger tr_Contact_ValidateOwner
//  Version     : 1.0
//------------------------------------------------------------------------------------------------------------
@isTest
private class TEST_tr_Contact_ValidateOwner {
  
    private static String tProfile_MEA = 'MEA Field Force CVG';
    private static String tProfile_EUR = 'EUR Field Force CVG';
    private static User oUser_MEA;
    private static User oUser_EUR;
    private static Contact oContact_EUR;
    private static Contact oContact_MEA;
    private static Account oAccount_EUR;
    private static Account oAccount_MEA;

    @testSetup static void createTestData() {

    List<User> lstUser = new List<User>();
    User oUser_Admin = clsTestData_User.createUser_SystemAdministratorMDT('tstadm1', false);
    lstUser.add(oUser_Admin);

    oUser_MEA = clsTestData_User.createUser('tstusr1', clsUtil.getUserProfileId(tProfile_MEA), clsUtil.getUserRoleId('Turkey DM (CVG)'), false);
      oUser_MEA.Region_vs__c = 'CEMA';
      oUser_MEA.Sub_Region__c = 'MEACAT';
      oUser_MEA.Country_vs__c = 'BENIN';
    lstUser.add(oUser_MEA);

    oUser_EUR = clsTestData_User.createUser('tstusr2', clsUtil.getUserProfileId(tProfile_EUR), clsUtil.getUserRoleId('Germany (CVG)'), false);
      oUser_EUR.Region_vs__c = 'Europe';
      oUser_EUR.Sub_Region__c = 'France';
      oUser_EUR.Country_vs__c = 'France';
    lstUser.add(oUser_EUR);

    insert lstUser;

    System.runAs(oUser_Admin){

//      oUser_MEA = [SELECT Id FROM User WHERE Profile.Name = :tProfile_MEA AND IsActive = true AND (NOT UserRole.DeveloperName IN ('EMEA_xBU', 'EMEA_Data_Steward', 'SystemAdministrator'))  LIMIT 1];
//      oUser_EUR = [SELECT Id FROM User WHERE Profile.Name = :tProfile_EUR AND IsActive = true AND (NOT UserRole.DeveloperName IN ('EMEA_xBU', 'EMEA_Data_Steward', 'SystemAdministrator'))  LIMIT 1];
      RecordType oRecordType_Account = clsUtil.getRecordTypeByDevName('Account', 'MDT_Account');
      RecordType oRecordType_Contact = clsUtil.getRecordTypeByDevName('Contact', 'MDT_Employee');

      // CREATE MEA DATA
        clsTestData.iRecord_Account = 1;
        clsTestData.idRecordType_Account = oRecordType_Account.Id;
        clsTestData.createAccountData(false);
        clsTestData.oMain_Account.OwnerId = oUser_MEA.Id;
      insert clsTestData.oMain_Account;

        clsTestData.iRecord_Contact = 2;
        clsTestData.idRecordType_Contact = oRecordType_Contact.Id;
        clsTestData.createContactData(false);
        for (Contact oContact : clsTestData.lstContact){
          oContact.OwnerId = oUser_MEA.Id; 
          oContact.Primary_Job_Title_vs__c = 'Country Manager';  
          oContact.Alias_unique__c = 'Con1'+System.now(); 
        }

      insert clsTestData.lstContact;

      system.assertEquals(clsTestData.oMain_Contact.OwnerId, oUser_MEA.Id);
      system.assertEquals(clsTestData.oMain_Account.OwnerId, oUser_MEA.Id);

      clsTestData.oMain_Account = null;
      clsTestData.lstAccount = new List<Account>();
      clsTestData.oMain_Contact = null;
      clsTestData.lstContact = new List<Contact>();

      // CREATE EUR DATA
        clsTestData.iRecord_Account = 1;
        clsTestData.idRecordType_Account = oRecordType_Account.Id;
        clsTestData.createAccountData(false);
        clsTestData.oMain_Account.OwnerId = oUser_EUR.Id;
      insert clsTestData.oMain_Account;

        clsTestData.iRecord_Contact = 2;
        clsTestData.idRecordType_Contact = oRecordType_Contact.Id;
        clsTestData.createContactData(false);
        for (Contact oContact : clsTestData.lstContact){
          oContact.OwnerId = oUser_EUR.Id;  
          oContact.Primary_Job_Title_vs__c = 'Country Manager';
          oContact.Alias_unique__c = 'Con1'+System.now().addDays(2);   
        }
      insert clsTestData.lstContact;

      system.assertEquals(clsTestData.oMain_Contact.OwnerId, oUser_EUR.Id);
      system.assertEquals(clsTestData.oMain_Account.OwnerId, oUser_EUR.Id);

    }

    }

    @isTest static void loadTestData() {

        Map<Id, Contact> mapContact = new Map<Id, Contact>([SELECT Id, OwnerId, Owner.Profile.Name, Account.Id, Account.OwnerId FROM Contact]);

        System.assertEquals(mapContact.size(), 4);
        Set<Id> setID_User = new Set<Id>();
        for (Contact oContact : mapContact.values()){
            if (oContact.Owner.Profile.Name == tProfile_EUR){
                oUser_EUR = oContact.Owner;
                oContact_EUR = oContact;
                oAccount_EUR = oContact.Account;
            }else if (oContact.Owner.Profile.Name == tProfile_MEA){
                oUser_MEA = oContact.Owner;
                oContact_MEA = oContact;
                oAccount_MEA = oContact.Account;
            }
        }
        System.assertNotEquals(oAccount_EUR, null);
        System.assertNotEquals(oAccount_MEA, null);
        System.assertNotEquals(oContact_EUR, null);
        System.assertNotEquals(oContact_MEA, null);

        System.assertEquals(oContact_EUR.OwnerId, oUser_EUR.Id);
        System.assertEquals(oAccount_EUR.OwnerId, oUser_EUR.Id);
        System.assertEquals(oContact_MEA.OwnerId, oUser_MEA.Id);
        System.assertEquals(oAccount_MEA.OwnerId, oUser_MEA.Id);

        System.assertNotEquals(oContact_EUR.OwnerId, oAccount_MEA.OwnerId);

    }

  @isTest static void test_tr_Contact_ValidateOwner_Single() {

        loadTestData();

        List<Id> lstID_Account = new List<Id>();
        lstID_Account.add(oAccount_MEA.Id);
        List<UserRecordAccess> lstUAR = [SELECT RecordId, HasReadAccess FROM UserRecordAccess WHERE UserId = :oUser_EUR.Id AND RecordId = :lstID_Account];
        System.assertEquals(lstUAR.size(), 1);
        for (UserRecordAccess oUAR : lstUAR){
            System.assertEquals(oUAR.HasReadAccess, false);
        }

        Test.startTest();

            oContact_EUR.AccountId = oAccount_MEA.Id;
        update oContact_EUR;

        Test.stopTest();

        oContact_EUR = [SELECT Id, AccountId, OwnerId FROM Contact WHERE Id = :oContact_EUR.Id];

        System.assertEquals(oContact_EUR.AccountId, oAccount_MEA.Id);
        System.assertEquals(oContact_EUR.OwnerId, oAccount_MEA.OwnerId);

  }

    @isTest static void test_tr_Contact_ValidateOwner_Multiple() {

        loadTestData();

        List<Id> lstID_Account = new List<Id>();
        lstID_Account.add(oAccount_MEA.Id);
        List<UserRecordAccess> lstUAR = [SELECT RecordId, HasReadAccess FROM UserRecordAccess WHERE UserId = :oUser_EUR.Id AND RecordId = :lstID_Account];
        System.assertEquals(lstUAR.size(), 1);
        for (UserRecordAccess oUAR : lstUAR){
            System.assertEquals(oUAR.HasReadAccess, false);
        }

        Test.startTest();

        List<Contact> lstContact = [SELECT Id, AccountId, OwnerId FROM Contact];
        for (Contact oContact : lstContact){
            oContact.AccountId = oAccount_EUR.Id;
        }
        update lstContact;

        Test.stopTest();

        lstContact = [SELECT Id, AccountId, OwnerId FROM Contact WHERE OwnerId = :oAccount_EUR.OwnerId];
        System.assertEquals(lstContact.size(), 4);

    }
  
}
//------------------------------------------------------------------------------------------------------------