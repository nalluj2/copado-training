@isTest
private class Test_SynchronizationService_Complaint {
	
	private static testMethod void syncComplaint(){
		
		Synchronization_Service_Setting__c syncSettings = new Synchronization_Service_Setting__c();
		syncSettings.username__c = 'test@medtronic.com';
		syncSettings.password__c = 'password';
		syncSettings.client_id__c = '123456789';
		syncSettings.client_secret__c = 'XXXXX';
		syncSettings.is_sandbox__c = true;
		syncSettings.sync_Enabled__c = true;
		insert syncSettings;
		
		//Products
		Product2 testProduct = new Product2();
		testProduct.Name = 'PoleStar';
		testProduct.ProductCode = '000999';
		testProduct.isActive = true;
					
		insert testProduct;
		
		//Account
		Account acc = new Account();	
		acc.AccountNumber = '0000000';
		acc.Name = 'Syn Test Account';
		acc.Phone = '+123456789';
		acc.BillingCity = 'Minneapolis';
		acc.BillingCountry = 'UNITED STATES';
		acc.BillingState = 'Minnesota';
		acc.BillingStreet = 'street';
		acc.BillingPostalCode = '123456';
		acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non-SAP Account').getRecordTypeId();	
		insert acc;
		
		//Contact
		Contact cnt = new Contact();
		cnt.FirstName = 'Test';
		cnt.LastName = 'Contact';
		cnt.Phone = '+123456789';		
		cnt.Email = 'test.contact@gmail.com';
		cnt.MobilePhone = '+123456789';		
		cnt.MailingCity = 'Minneapolis';
		cnt.MailingCountry = 'UNITED STATES';
		cnt.MailingState = 'Minnesota';
		cnt.MailingStreet = 'street';
		cnt.MailingPostalCode = '123456';
		cnt.External_Id__c = 'Test_Contact_Id';
		cnt.Contact_Department__c = 'Cardiology';
		cnt.Contact_Primary_Specialty__c = 'Cardiovascular'; 
		cnt.Affiliation_To_Account__c = 'Employee'; 
		cnt.Primary_Job_Title_vs__c = 'Surgeon'; 
		cnt.Contact_Gender__c = 'Male'; 
		cnt.AccountId = acc.Id;
		cnt.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Generic Contact').getRecordTypeId();				
		insert cnt;
		
		//Asset
		Asset asset = new Asset();
		asset.AccountId = acc.Id;
		asset.Product2Id = testProduct.Id;
		asset.Asset_Product_Type__c = 'PoleStar N-10';
		asset.Ownership_Status__c = 'Purchased';
		asset.Name = '123456789';
		asset.Serial_Nr__c = '123456789';
		asset.Status = 'Installed';
		asset.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('PoleStar').getRecordTypeId();
		insert asset;
		
		Test.startTest();
		
		Test_bl_SynchronizationService_Utils.CalloutMock mockImpl = new Test_bl_SynchronizationService_Utils.CalloutMock();
		
		Test.setMock(HttpCalloutMock.class, mockImpl);
		bl_SynchronizationService_Utils.calloutMock = mockImpl;
				
		//Complaint
		Complaint__c comp = new Complaint__c();
		comp.Account_Name__c = acc.Id;
		comp.Asset__c = asset.Id;
		comp.Status__c = 'Open';
		comp.Contact_Name__c = cnt.Id;
		comp.Initial_Reporter__c = cnt.Id;
		comp.Surgeon1__c = cnt.Id;
		comp.External_Id__c = 'Test_Complaint_Id';
		insert comp;
						
		List<Sync_Notification__c> notifications = [Select Id from Sync_Notification__c where Status__c = 'New' AND Type__c = 'Outbound' AND Record_Object_Type__c = 'Complaint__c'];
		
		System.assert(notifications.size() == 1);
		
		SynchronizationService_Complaint complaintService = new SynchronizationService_Complaint();
		
		String payload = complaintService.generatePayload( 'Test_Complaint_Id');
		
		System.assert(payload != null);
	}

	private static testMethod void syncComplaintInbound(){
		
		Synchronization_Service_Setting__c syncSettings = new Synchronization_Service_Setting__c();
		syncSettings.username__c = 'test@medtronic.com';
		syncSettings.password__c = 'password';
		syncSettings.client_id__c = '123456789';
		syncSettings.client_secret__c = 'XXXXX';
		syncSettings.is_sandbox__c = true;
		syncSettings.sync_Enabled__c = true;
		insert syncSettings;
		
		//Products
		Product2 testProduct = new Product2();
		testProduct.Name = 'PoleStar';
		testProduct.ProductCode = '000999';
		testProduct.isActive = true;					
		insert testProduct;
		
		//Account
		Account acc = new Account();	
		acc.AccountNumber = '0000000';
		acc.SAP_ID__c = '0000000';
		acc.Name = 'Syn Test Account';
		acc.Phone = '+123456789';
		acc.BillingCity = 'Minneapolis';
		acc.BillingCountry = 'UNITED STATES';
		acc.BillingState = 'Minnesota';
		acc.BillingStreet = 'street';
		acc.BillingPostalCode = '123456';
		acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non-SAP Account').getRecordTypeId();	
		insert acc;
		
		//Contact
		Contact cnt = new Contact();
		cnt.FirstName = 'Test';
		cnt.LastName = 'Contact';
		cnt.Phone = '+123456789';		
		cnt.Email = 'test.contact@gmail.com';
		cnt.MobilePhone = '+123456789';		
		cnt.MailingCity = 'Minneapolis';
		cnt.MailingCountry = 'UNITED STATES';
		cnt.MailingState = 'Minnesota';
		cnt.MailingStreet = 'street';
		cnt.MailingPostalCode = '123456';
		cnt.External_Id__c = 'Test_Contact_Id';
		cnt.Contact_Department__c = 'Cardiology';
		cnt.Contact_Primary_Specialty__c = 'Cardiovascular'; 
		cnt.Affiliation_To_Account__c = 'Employee'; 
		cnt.Primary_Job_Title_vs__c = 'Surgeon'; 
		cnt.Contact_Gender__c = 'Male'; 
		cnt.AccountId = acc.Id;
		cnt.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Generic Contact').getRecordTypeId();				
		insert cnt;
		
		//Asset
		Asset asset = new Asset();
		asset.AccountId = acc.Id;
		asset.Product2Id = testProduct.Id;
		asset.Asset_Product_Type__c = 'PoleStar N-10';
		asset.Ownership_Status__c = 'Purchased';
		asset.Name = '123456789';
		asset.Serial_Nr__c = '123456789';
		asset.Status = 'Installed';
		asset.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('PoleStar').getRecordTypeId();
		insert asset;
		
		Test.startTest();
		
		Test_bl_SynchronizationService_Utils.CalloutMock mockImpl = new Test_bl_SynchronizationService_Utils.CalloutMock();
		
		Test.setMock(HttpCalloutMock.class, mockImpl);
		bl_SynchronizationService_Utils.calloutMock = mockImpl;
		
		SynchronizationService_Complaint.ComplaintSyncPayload request = new SynchronizationService_Complaint.ComplaintSyncPayload();
						
		//Complaint
		Complaint__c complaint = new Complaint__c();
		complaint.External_Id__c = 'Test_Complaint_Id';
		complaint.Status__c = 'In Process';
		request.complaint = complaint;
				
		//Other
		request.accountId = '0000000';		
		request.complainantId = 'Test_Contact_Id';
		request.initialReporterId = 'Test_Contact_Id';
		request.surgeonId = 'Test_Contact_Id';
		request.assetId = '123456789';				
		request.recordType = 'Complaint_Open';
		request.createdDate = DateTime.now();
		request.createdBy = 'Test User';
		
		String payload = JSON.serialize(request);
		
		bl_SynchronizationService_Target.processRecordPayload(payload, 'Complaint__c');
		
		List<Complaint__c> complaints = [Select Id from Complaint__c where External_Id__c = 'Test_Complaint_Id'];
						
		System.assert(complaints.size() == 1);					
	} 	
	
	private static testMethod void syncComplaintInbound_err(){
		
		SynchronizationService_Complaint.ComplaintSyncPayload request = new SynchronizationService_Complaint.ComplaintSyncPayload();
		
		Complaint__c complaint = new Complaint__c();
		complaint.External_Id__c = 'Test_Complaint_Id';
		complaint.Status__c = 'In Process';
		request.complaint = complaint;
				
		//Other				
		request.createdDate = DateTime.now();
		request.createdBy = 'Test User';
				
		Boolean isError = false;
		
		//Account error
		request.accountId = 'X';
		
		try{
			
			bl_SynchronizationService_Target.processRecordPayload( JSON.serialize(request), 'Complaint__c');
			
		}catch(Exception e){
			isError = true;
		}
		
		System.assert(isError == true);
			
		//Contact error
		isError = false;		
		request.accountId = null;
		request.complainantId = 'X';
		
		try{
			
			bl_SynchronizationService_Target.processRecordPayload( JSON.serialize(request), 'Complaint__c');
			
		}catch(Exception e){
			isError = true;
		}
		
		System.assert(isError == true);
		
		//Asset error
		isError = false;		
		request.complainantId = null;
		request.assetId = 'X';
		
		try{
			
			bl_SynchronizationService_Target.processRecordPayload( JSON.serialize(request), 'Complaint__c');
			
		}catch(Exception e){
			isError = true;
		}
		
		System.assert(isError == true);
	}

}