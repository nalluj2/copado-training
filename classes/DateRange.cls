/*
Description: This class is used to implement logic to work with dateranges
Author - Rudy De Coninck
Created Date  - 9/10/2014
*/
global class DateRange {


	private Long min;
    private Long max;
    
    
    global DateRange(Date startDate, Date endDate){
        if(startDate == null){
            throw new IllegalArgumentException('illegal argument: null date');
        }
        
        min = DateTime.newInstance(startDate, Time.newInstance(0, 0, 0, 0)).getTime();
        if (endDate !=null){
        	max = DateTime.newInstance(endDate, Time.newInstance(0, 0, 0, 0)).getTime();
        }else{
        	//Open end time 
        	max = DateTime.newInstance(9999,12,1,0,0,0).getTime();
        }
    }
    
    global Long max(){ return max; }
    global Long min(){ return min;}
    global String toAString(){ return '[' + min + ',' + max + ']'; }
    global Boolean overlaps(DateRange value){ return min <= value.max() && value.min() <= max; }
    

}