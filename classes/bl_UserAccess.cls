global without sharing class bl_UserAccess {
	
	public static Map<Id, Boolean> hasEditAccess(List<SObject> records){
		
		List<Id> recordIds = new List<Id>();
		
		for(SObject obj : records){
			recordIds.add(obj.Id);
		}
		
		return hasEditAccess(recordIds);
	}
	
	
	//Max input size is 200. This is a restriction of SFDC on the table UserRecordAccess
	public static Map<Id, Boolean> hasEditAccess(List<Id> recordIds){
		
		Map<Id, Boolean> result = new Map<Id, Boolean>();
		System.debug(recordIds);
		for(UserRecordAccess userAccess : [Select recordId, hasEditAccess from UserRecordAccess where recordId IN: recordIds AND UserId=:UserInfo.getUserId()]){
			
			result.put(userAccess.RecordId, userAccess.HasEditAccess);
			
		}
		
		return result;
	}
	
	webservice static Boolean isBusinessUnit(String buName){
		
		List<User_Business_Unit__c> userSBUs = [Select Id from User_Business_Unit__c where User__c = :UserInfo.getUserId() and Business_Unit_Text__c = :buName LIMIT 1];
		
		if(userSBUs.size() > 0) return true;
		
		return false;
	}
	
}