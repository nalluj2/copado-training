public with sharing class ctrl_Marketing_Navigator {

	public PageReference openMarketingNavigator(){
		
		Map<String, SICA_Endpoints__c> endpoints = SICA_Endpoints__c.getall();
		
		String url = endpoints.get('TableauMarketing').URL__c;
						
		PageReference retURL = new PageReference(url);
      	retURL.setRedirect(true);
      	return retURL;
	}        
}