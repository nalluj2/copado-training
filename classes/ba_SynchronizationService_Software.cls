global class ba_SynchronizationService_Software implements Database.Batchable<String>, Database.Stateful, Database.AllowsCallouts, Schedulable{

	static private Integer BATCH_SIZE = 1000;
	
	//GetUpdated timestamps 
	private String syncStartDatetime;
	private String syncEndDatetime;
	
	//Mechanism to detect limit errors and notify admins
	private Integer totalBatches;
	private Integer processedBatches;
	
	private bl_SynchronizationService_Utils.SessionInfo sessionInfo;
	
	//List of errors occured
	private List<String> errors = new List<String>();	
	
	//Schedule execute to launch the batch process
	global void execute(SchedulableContext SC) {
    	
    	if(bl_SynchronizationService_Utils.isSyncEnabled == true){
			
			ba_SynchronizationService_Software batch = new ba_SynchronizationService_Software();
			Database.executeBatch(batch, BATCH_SIZE);	 
    	}
   	}
			
	global List<String> start(Database.BatchableContext BC){
      	
      	sessionInfo = bl_SynchronizationService_Utils.getTargetOrgSession();
      	
      	totalBatches = 0;
      	processedBatches = 0;
      	
      	List<Synchronization_Service_Setting__c> syncSettings = [Select Last_Sync_Software__c from Synchronization_Service_Setting__c LIMIT 1];
      	
      	if(syncSettings.size() > 0 && syncSettings[0].Last_Sync_Software__c != null){
      		
      		syncStartDatetime = syncSettings[0].Last_Sync_Software__c;
      		
      	}else{
      		//Use a initial start date
      		//The getUpdated method limits the max start date to 30 days before the current date
      		syncStartDatetime = DateTime.now().addDays(-30).addHours(1).formatGmt('yyyy-MM-dd\'T\'HH:mm:ss') + '+00:00';
      	}
      		  	      	
      	String startDateTime = EncodingUtil.URLEncode(syncStartDatetime, 'UTF-8');
      	String endDateTime = EncodingUtil.URLEncode(DateTime.now().formatGmt('yyyy-MM-dd\'T\'HH:mm:ss') + '+00:00', 'UTF-8');
      	      	
	    String serviceURL =  '/services/data/v30.0/sobjects/Software__c/updated/?start=' + startDateTime + '&end=' + endDateTime;
	    	          	
      	GetUpdatedResponse response;
	    
    	try {
    		
    		response = (GetUpdatedResponse) bl_SynchronizationService_Utils.executeService(serviceURL, 'GET', null, GetUpdatedResponse.class, sessionInfo);
    		
    	}catch(Exception e) {
        	
        	errors.add('Error during Rest call: ' + e.getMessage() + ' - ' + e.getStacktraceString());
        	
        	//If we get an error during the call, we stop here. The last updated datetime will not be updated, so the next time this batch triggers it will retry from the same point
        	return new List<String>();
    	}
     		
		//We get in the response the actual last datetime covered in the response. This needs to be the value used as Start Datetime in the next sync 
		syncEndDatetime = response.latestDateCovered;
		     	     	     	
      	if(response.ids != null){
      		
      		Decimal div = ((Decimal)response.ids.size()) / BATCH_SIZE;
			totalBatches = (Integer)div.round(System.RoundingMode.UP);
      		
      		return response.ids;
      	}
      	
      	//If we do not get any id in the response, the batch doesn't need to run.
      	return new List<String>();
   	}
   	
   	global void execute(Database.BatchableContext BC, List<String> updatedIds){
        
        //Get all Software fields
        String query = 'SELECT ' + String.join( syncFields, ',') + ' FROM Software__c where Id IN (\'' + String.join(updatedIds, '\',\'') + '\')';        
		
		List<Software__c> records;
					                
    	try {
    		
    		try{
    			
    			records = new List<Software__c>( (List<Software__c>) bl_SynchronizationService_Utils.executeQuery(query, fieldMapping, sessionInfo));//Created a new instance because just casting was creating a DML exception in the Upsert operation
    		
    		}catch(Exception ex) {
	    			
    			if(ex.getMessage().contains('INVALID_SESSION_ID')){
    				
    				sessionInfo = bl_SynchronizationService_Utils.getTargetOrgSession();
    				
    				records = new List<Software__c>( (List<Software__c>) bl_SynchronizationService_Utils.executeQuery(query, fieldMapping, sessionInfo));//Created a new instance because just casting was creating a DML exception in the Upsert operation
    				
    			}else throw ex;
    		}	
    		
    	} catch(Exception e) {
        	
        	errors.add('Error during Rest call: ' + e.getMessage() + ' - ' + e.getStacktraceString());
        	
        	//If we get an error during the call, we stop here. The last updated datetime will not be updated, so the next time this batch triggers it will retry from the same point
        	return;
    	}
     	     	     	
     	//Sample implementation untill we get the details of the External Id on Products for both Orgs
     	Set<String> productNames = new Set<String>();
     	
     	for(Software__c updatedSoftware : records){
     		
     		updatedSoftware.Id = null;//We cannot use the Id from the other Org
     		
     		productNames.add(updatedSoftware.Software_Product__r.Name);     		
     	}
     	
     	Map<String, Id> productIdMap = new Map<String, Id>();
     	
     	for(Product2 softProduct : [Select Id, Name from Product2 where Name IN :productNames]){
     		
     		productIdMap.put(softProduct.Name, softProduct.Id);
     	}
     	
     	for(Software__c updatedSoftware : records){
     		
     		Id productId = productIdMap.get(updatedSoftware.Software_Product__r.Name);
     		     		     		
     		updatedSoftware.Software_Product__c = productId;     			
     		    		
     		updatedSoftware.Software_Product__r = null;//To avoid problems with the upsert and external Ids
     	}
	    
	    //We allow partial success. In case one of the records fails we allow the rest to succeed but the Last Sync Date will not be updated, so the successful records will be updated multiple times.
	    List<Database.UpsertResult> upsertResult = Database.upsert(records,  Software__c.Fields.External_Id__c, false);	 
	    
	    for(Integer i = 0; i < records.size(); i++){
			
			Database.UpsertResult result = upsertResult[i];
			System.debug(result);
			if(!result.isSuccess()) errors.add('Error updating Software record (' + records[i].Name + '). Error message : ' + result.getErrors()[0].getMessage());
		}   
		
		processedBatches++ ;//Batch processed successfully without any limit error
   	}
   	   	   	
   	//If we go any error during the execution, we inform the 'watchers' about it
   	global void finish(Database.BatchableContext BC){
   		
   		if(processedBatches != totalBatches && errors.size() == 0){
   			
   			errors.add('An unhandle exception has occurred in at least one of the batches. Check the batch execution for more information. Batch Id = '+ bc.getJobId());
   		}
   		
   		if(errors.size() > 0){
   			
   			bl_SynchronizationService_Utils.notifySyncErrors(errors);
   			
   		}else{
   			
   			//Sync was successful or there were no changes. We can update the last sync date.
   			List<Synchronization_Service_Setting__c> settings = [Select Id, Last_Sync_Software__c from Synchronization_Service_Setting__c];
   			
   			Synchronization_Service_Setting__c setting = settings[0];   		
   			setting.Last_Sync_Software__c = syncEndDatetime.substring(0, syncEndDatetime.length() - 9) + '+00:00'; //The format returned does not match exactly with the one required as input
   			
   			update setting;
   		}   		
   	}
   	   	   	
   	public class GetUpdatedResponse{
		public List<String> ids {get;set;}
		public String latestDateCovered {get;set;}
	}
	
	private static List<String> syncFields = new List<String>
	{
		'Name', 
		'Accessory__c',
		'Active__c',
		'Computer_Type__c',
		'External_Id__c',
		'FCA__c',
		'PMM_Notes__c',
		'Revision__c',
		'Software_Name__c',
		'Software_Product__r.Name',
		'System_Type__c',
		'Version__c',

    'SIU_Software_Name__c',

    'Available_for_Cases_Part__c'		
	};
	
	private static Map<String, String> fieldMapping = new Map<String, String>		
	{
		'System_Type__c' => 'Asset_Product_Type__c'			
	};

}