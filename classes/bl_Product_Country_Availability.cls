public class bl_Product_Country_Availability {
    
    public static void populateUniqueKey(List<Product_Country_Availability__c> triggerNew){
    	
    	for(Product_Country_Availability__c prodCountry : triggerNew){
    	
    		prodCountry.Unique_Key__c = prodCountry.Product__c + ':' + prodCountry.Country__c;
    	}
    }
    
    private static Map<Id, DiB_Country__c> countryMap {
    	
    	get{
    		
    		if(countryMap == null) countryMap = new Map<Id, DiB_Country__c>([Select Id, Name from DiB_Country__c where Box_Builder_Enabled__c = true]);
    		    		
    		return countryMap;
    	}
    	
    	set;
    }
            
    public static void informBoxOwnersOnProdApproval(List<Product_Country_Availability__c> triggerNew, Map<Id, Product_Country_Availability__c> oldMap){
    	    	
    	Set<Id> productIds = new Set<Id>();
    	Set<String> countries = new Set<String>();
    	
    	Set<String> approved = new Set<String>();
    	
    	for(Product_Country_Availability__c prodCountry : triggerNew){
    		
    		if(prodCountry.Status__c == 'Available' && (oldMap == null || oldMap.get(prodCountry.Id).Status__c != 'Available')){
    		
    			productIds.add(prodCountry.Product__c);
    			
    			DiB_Country__c country = countryMap.get(prodCountry.Country__c);
    			countries.add(country.Name);
    			
    			approved.add(prodCountry.Product__c + ':' + country.Name.toUpperCase());    			
    		}    		
    	}
    	
    	if(approved.size() > 0){
    		
    		List<Messaging.SingleEmailMessage> notifications = new List<Messaging.SingleEmailMessage>();
    		
    		for(Procedural_Solutions_Kit_Product__c boxProd : [Select Procedural_Solutions_Kit__r.Name, Procedural_Solutions_Kit__r.Box_Description__c, Procedural_Solutions_Kit__r.Country__c, Procedural_Solutions_Kit__r.OwnerId, Procedural_Solutions_Kit__r.Owner.Name, Product__c, Product__r.Name from Procedural_Solutions_Kit_Product__c 
    																where Procedural_Solutions_Kit__r.Status__c = 'Open' AND Product__c IN :productIds AND Procedural_Solutions_Kit__r.Country__c IN :countries]){
    			
    			String key = boxProd.Product__c + ':' + boxProd.Procedural_Solutions_Kit__r.Country__c.toUpperCase();
    			
    			if(approved.contains(key)){
    				
    				Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
    				email.setTargetObjectId(boxProd.Procedural_Solutions_Kit__r.OwnerId);
    				email.setSaveAsActivity(false);    				    				
    				email.setSubject('Box Builder: Product \'' + boxProd.Product__r.Name + '\' in your Box \'' + boxProd.Procedural_Solutions_Kit__r.Name + '\' has been approved for its usage');
    				
    				String body = 'Dear ' + boxProd.Procedural_Solutions_Kit__r.Owner.Name + ',<br/><br/>';
    				body += 'The product \'' + boxProd.Product__r.Name + '\' has been approved in ' + boxProd.Procedural_Solutions_Kit__r.Country__c + '. Please review the configuration of your Box \'' + boxProd.Procedural_Solutions_Kit__r.Name + '\' (' + boxProd.Procedural_Solutions_Kit__r.Box_Description__c + ').';    				
    				
    				email.setHTMLBody(body);
    				
    				notifications.add(email);
    			}
    		}    		
    		
    		if(notifications.size() > 0) Messaging.sendEmail(notifications);    		
    	}
    	
    }
    
    public static void informBoxOwnersOnProdRejection(List<Product_Country_Availability__c> triggerNew, Map<Id, Product_Country_Availability__c> oldMap){
    	
    	Set<Id> productIds = new Set<Id>();
    	Set<String> countries = new Set<String>();
    	
    	Map<String, Product_Country_Availability__c> rejected = new Map<String, Product_Country_Availability__c>();
    	
    	for(Product_Country_Availability__c prodCountry : triggerNew){
    		
    		if(prodCountry.Status__c == 'Request Rejected' && (oldMap == null || oldMap.get(prodCountry.Id).Status__c != 'Request Rejected')){
    		
    			productIds.add(prodCountry.Product__c);
    			
    			DiB_Country__c country = countryMap.get(prodCountry.Country__c);
    			countries.add(country.Name);
    			
    			rejected.put(prodCountry.Product__c + ':' + country.Name.toUpperCase(), prodCountry);    			
    		}    		
    	}
    	
    	if(rejected.size() > 0){
    		
    		List<Messaging.SingleEmailMessage> notifications = new List<Messaging.SingleEmailMessage>();
    		
    		for(Procedural_Solutions_Kit_Product__c boxProd : [Select Procedural_Solutions_Kit__r.Name, Procedural_Solutions_Kit__r.Box_Description__c, Procedural_Solutions_Kit__r.Country__c, Procedural_Solutions_Kit__r.OwnerId, Procedural_Solutions_Kit__r.Owner.Name, Product__c, Product__r.Name from Procedural_Solutions_Kit_Product__c 
    																where Procedural_Solutions_Kit__r.Status__c = 'Open' AND Product__c IN :productIds AND Procedural_Solutions_Kit__r.Country__c IN :countries]){
    			
    			String key = boxProd.Product__c + ':' + boxProd.Procedural_Solutions_Kit__r.Country__c.toUpperCase();
    			
    			if(rejected.containsKey(key)){
    				
    				Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
    				email.setTargetObjectId(boxProd.Procedural_Solutions_Kit__r.OwnerId);
    				email.setSaveAsActivity(false);    				    				
    				email.setSubject('Box Builder: Product \'' + boxProd.Product__r.Name + '\' in your Box \'' + boxProd.Procedural_Solutions_Kit__r.Name + '\' has been rejected');
    				
    				String body = 'Dear ' + boxProd.Procedural_Solutions_Kit__r.Owner.Name + ',<br/><br/>';
    				body += 'The product \'' + boxProd.Product__r.Name + '\' has been rejected in ' + boxProd.Procedural_Solutions_Kit__r.Country__c + ' and cannot be used in the configuration of your Box \'' + boxProd.Procedural_Solutions_Kit__r.Name + '\' (' + boxProd.Procedural_Solutions_Kit__r.Box_Description__c + ').<br/><br/>';
    				body += 'Reason for rejection: ' + rejected.get(key).Rejected_Reason__c;    				
    				
    				email.setHTMLBody(body);
    				
    				notifications.add(email);
    			}
    		}    		
    		
    		if(notifications.size() > 0) Messaging.sendEmail(notifications);    		
    	}
    }
    
    private static Id spainCountryId {
    	
    	get{
    		
    		if(spainCountryId == null){
    			List<DiB_Country__c> spain = [Select Id from DiB_Country__c where Name = 'Spain'];
    			if(spain.size() == 1) spainCountryId = spain[0].Id;
    		}
    		
    		return spainCountryId;
    	}
    	set;
    }
    
    private static Id canaryIslandsCountryId {
    	
    	get{
    		
    		if(canaryIslandsCountryId == null){ 
    			List<DiB_Country__c> canaryIslands = [Select Id from DiB_Country__c where Name = 'Canary Islands'];
    			if(canaryIslands.size() == 1) canaryIslandsCountryId = canaryIslands[0].Id;
    		}
    		
    		return canaryIslandsCountryId;
    	}
    	set;
    }
    
    public static void alignSpainWithCanaryIslands(List<Product_Country_Availability__c> triggerNew){
    	
    	Set<Id> productsForSpain = new Set<Id>();
    	List<Product_Country_Availability__c> spanishAvailabilities = new List<Product_Country_Availability__c>();
    	    	
    	for(Product_Country_Availability__c prodCountry : triggerNew){
    		
    		if(prodCountry.Country__c == spainCountryId){
    			productsForSpain.add(prodCountry.Product__c);
    			spanishAvailabilities.add(prodCountry);
    		}    		
    	}
    	
    	if(productsForSpain.size() > 0){
    		
    		Map<Id, Product_Country_Availability__c> canarianAvailabilities = new Map<Id, Product_Country_Availability__c>();
    		
    		for(Product_Country_Availability__c ciAvailability : [Select Status__c, Rejected_Reason__c, Unique_Key__c, Product__c from Product_Country_Availability__c where Product__c IN :productsForSpain AND Country__r.Name = 'Canary Islands']){
				
				canarianAvailabilities.put(ciAvailability.Product__c, ciAvailability);    			
    		}
    		
    		List<Product_Country_Availability__c> forUpsert = new List<Product_Country_Availability__c>();
    		
    		for(Product_Country_Availability__c spanishAvailability : spanishAvailabilities){
    			
    			Product_Country_Availability__c canarianAvailability = canarianAvailabilities.get(spanishAvailability.Product__c);
    			
    			if(canarianAvailability == null){
    				
    				canarianAvailability = new Product_Country_Availability__c();
    				canarianAvailability.Product__c = spanishAvailability.Product__c;
    				canarianAvailability.Country__c = canaryIslandsCountryId;
    				canarianAvailability.Status__c = spanishAvailability.Status__c;
    				canarianAvailability.Rejected_Reason__c = spanishAvailability.Rejected_Reason__c;
    				canarianAvailability.Unique_Key__c = canarianAvailability.Product__c + ':' + canarianAvailability.Country__c;
    				
    				forUpsert.add(canarianAvailability);
    				
    			}else{
    				
    				if(canarianAvailability.Status__c != spanishAvailability.Status__c || canarianAvailability.Rejected_Reason__c != spanishAvailability.Rejected_Reason__c){
    					
    					canarianAvailability.Status__c = spanishAvailability.Status__c;
    					canarianAvailability.Rejected_Reason__c = spanishAvailability.Rejected_Reason__c;
    					
    					forUpsert.add(canarianAvailability);
    				}
    			}
    		}
    		
    		if(forUpsert.size() > 0) upsert forUpsert Unique_Key__c; 
    	}
    }
}