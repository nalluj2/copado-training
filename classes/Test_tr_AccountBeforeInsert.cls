/*
 *      Author 		: Bart Caelen
 *		Date		: 2014-07-22
 *      Description : 
 *			TEST Class for tr_AccountBeforeInsert to cover the code
 *		Version		:  1.0
*/
@isTest private class Test_tr_AccountBeforeInsert {
	
	@isTest static void test_tr_AccountBeforeInsert_SAPAccount() {

		//----------------------------------------
		// CREATE TEST DATA
		//----------------------------------------
		User oUser = [SELECT Id FROM User WHERE IsActive = true and Lastname = 'SAP Interface' LIMIT 1];
    	//----------------------------------------


		Test.startTest();


		//----------------------------------------
		// TEST 1 - Validate data update by SAP-INTEGRATOR
		//----------------------------------------
		System.runAs(oUser){
			clsTestData.idRecordType_Account = RecordTypeMedtronic.getRecordTypeByDevName('Account', 'SAP_Account').Id;
			clsTestData.iRecord_Account = 1;
			clsTestData.createAccountData(false);

			clsTestData.oMain_Account.SAP_ID__c = 'SAP001';
			insert clsTestData.oMain_Account;

			clsTestData.oMain_Account = [SELECT Id, Phone, SAP_Phone__c, Fax, SAP_Fax__c, Account_Email__c, SAP_Email__c, Account_Province__c, SAP_Region__c FROM Account WHERE Id = :clsTestData.oMain_Account.Id];

			System.assertEquals(clsTestData.oMain_Account.Phone, clsTestData.oMain_Account.SAP_Phone__c);
			System.assertEquals(clsTestData.oMain_Account.Fax, clsTestData.oMain_Account.SAP_Fax__c);
			System.assertEquals(clsTestData.oMain_Account.Account_Email__c, clsTestData.oMain_Account.SAP_Email__c);
			System.assertEquals(clsTestData.oMain_Account.Account_Province__c, clsTestData.oMain_Account.SAP_Region__c);

			clsTestData.oMain_Account.SAP_Phone__c = '001';
			clsTestData.oMain_Account.SAP_Fax__c = '002';
			clsTestData.oMain_Account.SAP_Email__c = 'test@test.test';
			clsTestData.oMain_Account.SAP_Region__c = '003';

			System.assertNotEquals(clsTestData.oMain_Account.Phone, clsTestData.oMain_Account.SAP_Phone__c);
			System.assertNotEquals(clsTestData.oMain_Account.Fax, clsTestData.oMain_Account.SAP_Fax__c);
			System.assertNotEquals(clsTestData.oMain_Account.Account_Email__c, clsTestData.oMain_Account.SAP_Email__c);
			System.assertNotEquals(clsTestData.oMain_Account.Account_Province__c, clsTestData.oMain_Account.SAP_Region__c);

			update clsTestData.oMain_Account;

			clsTestData.oMain_Account = [SELECT Id, Phone, SAP_Phone__c, Fax, SAP_Fax__c, Account_Email__c, SAP_Email__c, Account_Province__c, SAP_Region__c FROM Account WHERE Id = :clsTestData.oMain_Account.Id];

			System.assertEquals(clsTestData.oMain_Account.Phone, clsTestData.oMain_Account.SAP_Phone__c);
			System.assertEquals(clsTestData.oMain_Account.Fax, clsTestData.oMain_Account.SAP_Fax__c);
			System.assertEquals(clsTestData.oMain_Account.Account_Email__c, clsTestData.oMain_Account.SAP_Email__c);
			System.assertEquals(clsTestData.oMain_Account.Account_Province__c, clsTestData.oMain_Account.SAP_Region__c);
		}
		//----------------------------------------

		//----------------------------------------
		// TEST 2 - Data is not updated if an NON-SAP-INTEGRATOR performs the update
		//----------------------------------------
		clsTestData.oMain_Account = [SELECT Id, Phone, SAP_Phone__c, Fax, SAP_Fax__c, Account_Email__c, SAP_Email__c, Account_Province__c, SAP_Region__c FROM Account WHERE Id = :clsTestData.oMain_Account.Id];

		clsTestData.oMain_Account.SAP_Phone__c = '011';
		clsTestData.oMain_Account.SAP_Fax__c = '012';
		clsTestData.oMain_Account.SAP_Email__c = 'test1@test1.test1';
		clsTestData.oMain_Account.SAP_Region__c = '013';

		System.assertNotEquals(clsTestData.oMain_Account.Phone, clsTestData.oMain_Account.SAP_Phone__c);
		System.assertNotEquals(clsTestData.oMain_Account.Fax, clsTestData.oMain_Account.SAP_Fax__c);
		System.assertNotEquals(clsTestData.oMain_Account.Account_Email__c, clsTestData.oMain_Account.SAP_Email__c);
		System.assertNotEquals(clsTestData.oMain_Account.Account_Province__c, clsTestData.oMain_Account.SAP_Region__c);

		update clsTestData.oMain_Account;

		clsTestData.oMain_Account = [SELECT Id, Phone, SAP_Phone__c, Fax, SAP_Fax__c, Account_Email__c, SAP_Email__c, Account_Province__c, SAP_Region__c FROM Account WHERE Id = :clsTestData.oMain_Account.Id];

		System.assertNotEquals(clsTestData.oMain_Account.Phone, clsTestData.oMain_Account.SAP_Phone__c);
		System.assertNotEquals(clsTestData.oMain_Account.Fax, clsTestData.oMain_Account.SAP_Fax__c);
		System.assertNotEquals(clsTestData.oMain_Account.Account_Email__c, clsTestData.oMain_Account.SAP_Email__c);
		System.assertNotEquals(clsTestData.oMain_Account.Account_Province__c, clsTestData.oMain_Account.SAP_Region__c);
		//----------------------------------------



		Test.stopTest();
	}
	

	@isTest static void test_tr_AccountBeforeInsert_PersonAccount() {

		//----------------------------------------
		// CREATE TEST DATA
		//----------------------------------------
		User oUser = [SELECT Id FROM User WHERE IsActive = true and Lastname = 'SAP Interface' LIMIT 1];
    	//----------------------------------------

		Test.startTest();

		System.runAs(oUser){
			clsTestData.createPersonAccountData(false);
			clsTestData.oMain_PersonAccount.SAP_ID__c = 'TESTSAPID';
			clsTestData.oMain_PersonAccount.SAP_Channel__c = '30';
			clsTestData.oMain_PersonAccount.SAP_Auth_Group__c = 'ZPAU';
			insert clsTestData.oMain_PersonAccount;
			update clsTestData.oMain_PersonAccount;
		}

		Test.stopTest();
	}
	
}