public without sharing class bl_Support_Requests_SFDC {

	private ctrl_Support_Requests.SessionData session;
	
	public bl_Support_Requests_SFDC( ctrl_Support_Requests.SessionData sData){
		
		this.session = sData;		
	}
	
	public PageReference initializeInputRequestId()
	{
		if(session.request.Data_Load_Approvers__c != null && session.request.Data_Load_Approvers__c.contains(session.uInfo.Email)) session.isAppApprover = true;              
        else session.isAppApprover = false;
                                    
        return Page.Support_Data_Load_Request;
	}
	
	public PageReference requestBug(){  
        
        newRequest('Bug');              
        return Page.Support_Request_Bug;            
    }
    
    public PageReference requestChangeRequest(){    
                                    
        newRequest('Change Request');                    
        return Page.Support_Change_Request;         
    }
    
    public PageReference requestServiceRequest(){
        
        newRequest('Generic Service'); 
        return Page.Support_Other_Request;
    }
    
    public PageReference requestDataLoad(){
        
        newRequest('Data Load');
        return Page.Support_Data_Load_Request;
    }
    
    public void createServiceRequest(){
        
        if(session.request.Description_Long__c == null || session.request.Description_Long__c==''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'A Description is required'));
            return; 
        }
        
        if(session.request.Service_Request_Type__c == null || session.request.Service_Request_Type__c==''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Service Request Type is required'));
            return; 
        }
        
        Create_User_Request__c toInsertRequest = session.request.clone(false);

        SavePoint sp = Database.setSavepoint();
        
        try{
                                
            toInsertRequest.Requestor_Name__c = session.uInfo.firstName+' '+session.uInfo.lastName;
            toInsertRequest.Requestor_Email__c = session.uInfo.email;                       
            insert toInsertRequest;
            
            session.saveAttachments(toInsertRequest.Id);
                        
            session.request.Id = toInsertRequest.Id;
            
            session.comment = new User_Request_Comment__c();
                        
        } catch (Exception e) {
            
            ApexPages.addMessages(e);
            Database.rollback(sp);
            
            if(Test.isRunningTest()) throw e;
            
            return ;        
        }finally{           
            session.clearAttachments();           
        }
        
        session.request = [Select Id, Name, Description_Long__c, Assignee__c, Status__c, Service_Request_Type__c from Create_User_Request__c where Id=:session.request.Id];
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request '+session.request.Name+' created successfully.'));
    }
    
    public void createBugRequest(){
        
        if(session.request.Description_Long__c == null || session.request.Description_Long__c==''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'A Description is required'));
            return; 
        }
                
        Create_User_Request__c toInsertRequest = session.request.clone(false);

        SavePoint sp = Database.setSavepoint();
        
        try{
                                
            toInsertRequest.Requestor_Name__c = session.uInfo.firstName+' '+session.uInfo.lastName;
            toInsertRequest.Requestor_Email__c = session.uInfo.email;                     
            insert toInsertRequest;
            
            session.saveAttachments(toInsertRequest.Id);
                        
            session.request.Id = toInsertRequest.Id;
            
            session.comment = new User_Request_Comment__c();
                        
        } catch (Exception e) {
                                    
            ApexPages.addMessages(e);
            Database.rollback(sp);
            
            if(Test.isRunningTest()) throw e;
            
            return ;
        }finally{
            session.clearAttachments();
        }
        
        session.request = [Select Id, Name, Description_Long__c, Assignee__c, Status__c from Create_User_Request__c where Id=:session.request.Id];
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request '+session.request.Name+' created successfully.'));
    }
    
    public void createChangeRequest(){
        
        if(session.request.Description_Long__c == null || session.request.Description_Long__c==''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'A Description is required'));
            return; 
        }else if(session.request.Priority__c == null || session.request.Priority__c==''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Priority is required'));
            return; 
        }else if(session.request.Business_Rationale__c == null || session.request.Business_Rationale__c==''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Business Rationale is required'));
            return; 
        }else if(session.request.CR_Type__c == null || session.request.CR_Type__c==''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Change Request Type is required'));
            return; 
        }       
                
        Create_User_Request__c toInsertRequest = session.request.clone(false);

        SavePoint sp = Database.setSavepoint();
        
        try{
                                
            toInsertRequest.Requestor_Name__c = session.uInfo.firstName+' '+session.uInfo.lastName;
            toInsertRequest.Requestor_Email__c = session.uInfo.email;
            insert toInsertRequest;
            
            session.saveAttachments(toInsertRequest.Id);
                        
            session.request.Id = toInsertRequest.Id;
            
            session.comment = new User_Request_Comment__c();
                        
        } catch (Exception e) {
                                    
            ApexPages.addMessages(e);
            Database.rollback(sp);
            
            if(Test.isRunningTest()) throw e;
            
            return ;
        }finally{
            session.clearAttachments(); 
        }
        
        session.request = [Select Id, Name, Assignee__c, CR_Type__c, Description_Long__c, Status__c, Priority__c, Business_Rationale__c from Create_User_Request__c where Id=:session.request.Id];
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request '+session.request.Name+' created successfully.'));
    }
    
    public void createDataLoadRequest(){
                        
        if(session.request.Description_Long__c == null || session.request.Description_Long__c==''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'A Description is required'));
            return; 
        }
        
        if(session.request.Data_Type__c == null || session.request.Data_Type__c==''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Data Administration Type is required'));
            return; 
        }
                
        Create_User_Request__c toInsertRequest = session.request.clone(true);

        SavePoint sp = Database.setSavepoint();
        
        try{
            
            if(session.request.Id==null){                            
                toInsertRequest.Requestor_Name__c = session.uInfo.firstName+' '+session.uInfo.lastName;
                toInsertRequest.Requestor_Email__c = session.uInfo.email;
                toInsertRequest.Request_Type__c = 'Data Load';              
                insert toInsertRequest;
            }
            
            session.saveAttachments(toInsertRequest.Id);
                        
            session.request.Id = toInsertRequest.Id;
            
            session.comment = new User_Request_Comment__c();
                        
        } catch (Exception e) {
                        
            ApexPages.addMessages(e);
            Database.rollback(sp);
            if(Test.isRunningTest()) throw e;
            return ;
        }finally{
            
            session.clearAttachments();
        }
        
        session.request = [Select Id, Name, Data_Type__c, Assignee__c, Description_Long__c, Status__c, Requestor_Email__c, Data_Load_Result__c from Create_User_Request__c where Id=:session.request.Id];
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request '+session.request.Name+' created successfully.'));
    }
    
    public void reSubmitDataLoad(){
        
        String prevStatus = session.request.Status__c;
        
        try{
            
            session.request.Status__c='Submitted for Approval';
            update session.request;
            
            bl_CreateUserRequest.notifyDataLoadApprovers(session.request);
            
            session.saveCommentAndAttachment();
            
        }catch(Exception e){
            ApexPages.addMessages(e);
            session.request.Status__c=prevStatus;
            if(Test.isRunningTest()) throw e;
            return;
        }
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, 'Request Submitted successfully.'));     
    }
    
    private void newRequest(String requestType)
    {
    	session.resetRequest();
        session.request.Status__c = 'New';
        session.request.Application__c = 'Salesforce.com';
        session.request.Request_Type__c = requestType;
        
        session.clearAttachments();
        
        session.readOnlyAttachments = new List<Attachment>();
        session.comments = null;
    }
}