@isTest public class clsTestData_TenderCalendar {
	
	public static Integer iRecord_TenderCalendar2 = 1;
	public static Tender_Calendar_2__c oMain_TenderCalendar2;
	public static List<Tender_Calendar_2__c> lstTenderCalendar2 = new List<Tender_Calendar_2__c>();
	public static Date dTenderCalendar2_StartDate = Date.today().addDays(-30);
	public static Date dTenderCalendar2_EndDate = Date.today().addDays(30);


	//---------------------------------------------------------------------------------------------------
    // Create Tender Calendar 2 Data
    //---------------------------------------------------------------------------------------------------
	public static List<Tender_Calendar_2__c> createTenderCalendar2(){
		return createTenderCalendar2(true);
	}
    public static List<Tender_Calendar_2__c> createTenderCalendar2(Boolean bInsert){

        if (oMain_TenderCalendar2 == null){

        	if (clsTestData_AccountPlan.oMain_AccountPlan2 == null) clsTestData_AccountPlan.createAccountPlan2();
        	if (clsTestData_Product.oMain_ProductGroup == null) clsTestData_Product.createProductGroup();

        	lstTenderCalendar2 = new List<Tender_Calendar_2__c>();

        	for (Integer i=0; i<iRecord_TenderCalendar2; i++){
        		Tender_Calendar_2__c oTenderCalendar2 = new Tender_Calendar_2__c();
					oTenderCalendar2.Account_Plan_ID__c = clsTestData_AccountPlan.oMain_AccountPlan2.id;
					oTenderCalendar2.Awarded_Units__c = 20;
					oTenderCalendar2.Price_Info__c = 12;
					oTenderCalendar2.Total_Units__c = 100;
					oTenderCalendar2.Start_Date__c = dTenderCalendar2_StartDate;
					oTenderCalendar2.End_Date__c = dTenderCalendar2_EndDate;
					oTenderCalendar2.Product_Group_ID__c = clsTestData_Product.oMain_ProductGroup.Id;
				lstTenderCalendar2.Add(oTenderCalendar2);
        	}

        	if (bInsert) insert lstTenderCalendar2;
        	oMain_TenderCalendar2 = lstTenderCalendar2[0];
        }

        return lstTenderCalendar2;
    }
    //---------------------------------------------------------------------------------------------------
}