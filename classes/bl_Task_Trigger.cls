//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 20-03-2017
//  Description      : APEX Class - Business Logic for tr_Task
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public with sharing class bl_Task_Trigger {


	public static void setTaskAsPublic(List<Task> lstTriggerNew){

		if (
			(!Schema.sObjectType.Task.Fields.Description.isAccessible()) 
			|| (!Schema.sObjectType.Task.Fields.IsVisibleInSelfService.isUpdateable()) 
		){
			return;
		}

		// Verify if Task contains 'emailtosalesforce@' in the Description 
		for (Task oTask : lstTriggerNew){

			// Verify if the processing user (= CreatedBy of the task record) has Public_Task__c marked as TRUE in the Custom Setting 
			Email2SFDC_PublicTask__c oEmail2SFDC_PublicTask = Email2SFDC_PublicTask__c.getInstance(UserInfo.getUserId());

			Boolean bSetPublic = false;
			if (oEmail2SFDC_PublicTask != null){

				if (oEmail2SFDC_PublicTask.Public_Task__c){

					if (oEmail2SFDC_PublicTask.Email_To_Salesforce_Only__c){

						if ( 
							(!String.isBlank(oTask.Description))
							&& (oTask.Description.contains('emailtosalesforce@')) 
						){
							bSetPublic = true;
						}

					}else{

						bSetPublic = true;

					}

					if (bSetPublic){ 

						if (oTask.IsVisibleInSelfService == false){

							oTask.IsVisibleInSelfService = true;

						}

					}

				}

			}

		}

	}
	
	public static void DIBAccountPlanObjective_Mandatory(List<Task> triggerNew){
		
		if(bl_Trigger_Deactivation.isTriggerDeactivated('tsk_AccountPlanObjective_mandatory')) return;
		
		List<Task> toProcess = new List<Task>();
		Set<Id> accPlanIds = new Set<Id>();
		
		for(Task tsk : triggerNew){
			
			if(tsk.WhatId != null && String.valueOf(tsk.WhatId.getSObjectType()) == 'Account_Plan_2__c' && tsk.Account_Plan_Objective__c == null){
				
				accPlanIds.add(tsk.whatId);
				toProcess.add(tsk);
			}			
		}
		
		//Nothing to check
		if(toProcess.isEmpty()) return;
		
		Map<Id, Account_Plan_2__c> DIB_AccPlanMap = new Map<Id, Account_Plan_2__c>([Select Id from Account_Plan_2__c where Id IN :accPlanIds AND Account__r.Account_Country_vs__c != 'CANADA' AND Business_Unit_Group__r.Name = 'Diabetes' AND Business_Unit_Group__r.Master_Data__r.Company_Code_Text__c IN ('EUR')]);
		
		for(Task tsk : toProcess){
			
			if(DIB_AccPlanMap.containsKey(tsk.WhatId)) tsk.addError('Account Plan Objective is mandatory when the task is connected to a Diabetes Account Plan');
		}
	}


	//----------------------------------------------------------------------------------------------------------------
	// Set the Account__c field on Task to macth the Account on the linked Opportunity (WhatId)
	//----------------------------------------------------------------------------------------------------------------
	public static void CVG_MET_PopulateAccount(List<Task> lstTriggerNew, Map<Id, Task> mapTriggerOld){

		if (bl_Trigger_Deactivation.isTriggerDeactivated('tsk_CVG_MET_PopulateAccount')) return;

		Set<Id> setID_RecordType = new Set<Id>{clsUtil.getRecordTypeByDevName('Task', 'Marketing_Execution_Task').Id};
	
		// Collect the Opportunity ID's of the processing Marketing Execution Tasks
		List<Task> lstTask_Processing = new List<Task>();
		Set<Id> setID_Opportunity = new Set<Id>();
		for (Task oTask : lstTriggerNew){

			if (setID_RecordType.contains(oTask.RecordTypeId)){
			
				if (clsUtil.isNull(oTask.WhatId, '').startsWith('006')){

					if (mapTriggerOld == null){

						setID_Opportunity.add(oTask.WhatId);
						lstTask_Processing.add(oTask);
				
					}else{
				
						if ( (mapTriggerOld.get(oTask.Id).WhatId != oTask.WhatId) || (mapTriggerOld.get(oTask.Id).Account__c != oTask.Account__c) ){
					
							setID_Opportunity.add(oTask.WhatId);				
							lstTask_Processing.add(oTask);

						}

					}

				}else{

					if (oTask.Account__c != null) oTask.Account__c = null;
					
				}

			}
			
		}
		
		if (setID_Opportunity.size() == 0) return;


		// Get the Account Id's of the colected Opportunity Id's
		Map<Id, Opportunity> mapOpportunity = new Map<Id, Opportunity>([SELECT Id, AccountId FROM Opportunity WHERE Id = :setID_Opportunity]);

		// Set the Account__c on the processing Task to match the value of the Account on the related Opportunity
		for (Task oTask : lstTask_Processing){

			Opportunity oOpportunity = mapOpportunity.get(oTask.WhatId);
			if (oTask.Account__c != oOpportunity.AccountId) oTask.Account__c = oOpportunity.AccountId;
			
		}

	}
	//----------------------------------------------------------------------------------------------------------------


	//----------------------------------------------------------------------------------------------------------------
	// Send an Email notification when a Marketeer User (Custom Permission) creates a task for another user
	//----------------------------------------------------------------------------------------------------------------
	public static void CVG_MET_SendNotificationEmail(List<Task> lstTriggerNew, Map<Id, Task> mapTriggerOld){

		if (bl_Trigger_Deactivation.isTriggerDeactivated('tsk_CVG_MET_SendNotificationEmail')) return;

		// Only execute this logic when 1 task is created or updated to prevent bulk notifications to be send out.
		if (lstTriggerNew.size() > 1) return;

		// Verify that the executing user is a Marketeer
		Boolean bIsMarketeer = FeatureManagement.checkPermission('CVG_Marketeer');
		if (!bIsMarketeer) return;

		Set<Id> setID_RecordType = new Set<Id>{clsUtil.getRecordTypeByDevName('Task', 'Marketing_Execution_Task').Id};

		EmailTemplate oEmaiTemplate = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'Task_Notification_template_CVG'];
		List<Messaging.SingleEmailMessage> lstEmail = new List<Messaging.SingleEmailMessage>();

		// Collect the CVG Tasks that are created by a Marketeer and assigned to amother user
		List<Id> lstID_Task_Processing = new List<Id>();
		for (Task oTask : lstTriggerNew){

			if ( 
				(setID_RecordType.contains(oTask.RecordTypeId)) 
				&& (oTask.OwnerId != UserInfo.getUserId())
				&& (oTask.Created_in_Mobile_CVG__c == false)	// There is already logic that will process the Tasks that are created in Mobile
			){


	    		if (mapTriggerOld == null){

					lstID_Task_Processing.add(oTask.Id);

				}else if (oTask.OwnerId != mapTriggerOld.get(oTask.Id).OwnerId){

					lstID_Task_Processing.add(oTask.Id);	    		

				}

			}

		}

		if (lstID_Task_Processing.size() > 0){
			bl_Task.sendCVGEmail(lstID_Task_Processing);
		}

	}
	//----------------------------------------------------------------------------------------------------------------


	//----------------------------------------------------------------------------------------------------------------
	// Populate the Short Description of the Task with the 255 first characters of the Description
	//----------------------------------------------------------------------------------------------------------------
	public static void CVG_MET_PopulateShortDescription(List<Task> lstTriggerNew, Map<Id, Task> mapTriggerOld){

		if (bl_Trigger_Deactivation.isTriggerDeactivated('tsk_CVG_MET_PopulateShortDescription')) return;

		Set<Id> setID_RecordType = new Set<Id>{clsUtil.getRecordTypeByDevName('Task', 'Marketing_Execution_Task').Id};

		for (Task oTask : lstTriggerNew){
	
			if (setID_RecordType.contains(oTask.RecordTypeId)){ 

				if (mapTriggerOld == null){

					oTask.Short_Description__c = clsUtil.isNull(oTask.Description, '').left(255);

				}else if(oTask.Description != mapTriggerOld.get(oTask.Id).Description){

					oTask.Short_Description__c = clsUtil.isNull(oTask.Description, '').left(255);

				}
			
			}

		}

	}
	//----------------------------------------------------------------------------------------------------------------



	//----------------------------------------------------------------------------------------------------------------
	// When a Task is linked to a Tactic Campaign (Task field = Campaign__c) and the Task is updated to a certain status 
	//	we need to update the related Campaign Member (Contact) and / or the related Campaign Account (Account) to a certain status.
	//
	//	Task Status					Campaign Member Status			Campaign Account Status
	//
	//	Completed					Completed						Completed
	//	In Progress					In Progress						In Progress
	//	Waiting on someone else		In Progress						In Progress
	//	Deferred					Aborted							Aborted
	//
	//	Completed is only used when all tasks (RT = Marketing_Execution_Task) related to the same Campaign Member / Campaign Account are marked as completed
	//----------------------------------------------------------------------------------------------------------------
	public static void CVG_MET_UpdateCampaignMember(List<Task> lstTriggerNew, Map<Id, Task> mapTriggerOld){

		if (bl_Trigger_Deactivation.isTriggerDeactivated('tsk_CVG_UpdateCampaignMember')) return;

		Set<Id> setID_RecordType = new Set<Id>{clsUtil.getRecordTypeByDevName('Task', 'Marketing_Execution_Task').Id};
		
		Map<Id, Map<Id, String>> mapCampaignId_ContactId_Status = new Map<Id, Map<Id, String>>();
		Map<Id, Map<Id, String>> mapCampaignId_AccountId_Status = new Map<Id, Map<Id, String>>();

		Set<Id> setID_Campaign_All = new Set<Id>();
		Set<Id> setID_Account_All = new Set<Id>();
		Set<Id> setID_Contact_All = new Set<Id>();

		Map<String, String> mapTaskStatus_Status = new Map<String, String>();
			mapTaskStatus_Status.put('Completed', 'Completed');
			mapTaskStatus_Status.put('In Progress', 'In Progress');
			mapTaskStatus_Status.put('Waiting on someone else', 'In Progress');
			mapTaskStatus_Status.put('Deferred', 'Aborted');

		Set<String> setInProgress = new Set<String>();
			setInProgress.add('Waiting on someone else');
			setInProgress.add('In Progress');
			setInProgress.add('Completed');


		// Collect all Tactic Campign Id's			
		for (Task oTask : lstTriggerNew){

			if (!setID_RecordType.contains(oTask.RecordTypeId) || oTask.Campaign__c == null) continue;
			setID_Campaign_All.add(oTask.Campaign__c);

		}
	
		// Collect all Task for each Campaign Id
		List<Task> lstTask_CampaignTactic = [SELECT Id, WhoId, Account__c, Campaign__c, Status FROM Task WHERE RecordTypeId = :setID_RecordType AND Campaign__c = :setID_Campaign_All AND Campaign__r.RecordTypeId = :clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_Tactic').Id ORDER BY Campaign__c];
		Map<Id, List<Task>> mapCampaignId_Tasks = new Map<Id, List<Task>>();
		for (Task oTask_CampaignTactic : lstTask_CampaignTactic){
			List<Task> lstTask_Tmp = new List<Task>();
			if (mapCampaignId_Tasks.containsKey(oTask_CampaignTactic.Campaign__c)) lstTask_Tmp = mapCampaignId_Tasks.get(oTask_CampaignTactic.Campaign__c);
			lstTask_Tmp.add(oTask_CampaignTactic);
			mapCampaignId_Tasks.put(oTask_CampaignTactic.Campaign__c, lstTask_Tmp);
		}

		for (Task oTask : lstTriggerNew){

			Id idCampaign = oTask.Campaign__c;
			Id idAccount = oTask.Account__c;
			Id idContact;
			String tId = oTask.WhoId;
			if (!String.isBlank(tId) && tId.startsWith('003')) idContact = oTask.WhoId;

			// Only process the task if the Task RecordType is "Marketing_Execution_Task" and the Campaign is populated with a Tactic Campaign
			if ( !setID_RecordType.contains(oTask.RecordTypeId) || oTask.Campaign__c == null || !mapCampaignId_Tasks.containsKey(idCampaign) ) continue;

			Task oTask_Old = mapTriggerOld.get(oTask.Id);
			if ( (oTask_Old.Status != oTask.Status) && (mapTaskStatus_Status.containsKey(oTask.Status)) ){

				String tStatus = mapTaskStatus_Status.get(oTask.Status);	// This is the default status for the Campaign Account when not all Tasks are completed or none of the tasks is "In Progress"

				if (idAccount != null){

					List<Task> lstTask_Campaign = mapCampaignId_Tasks.get(idCampaign);

					if (oTask.Status == 'Completed'){

						// Verify if all Tasks related to this Account / Campaign Tactic are Completed or not
						Boolean bAllTasksCompleted = true;

						for (Task oTask_Campaign : lstTask_Campaign){

							if (oTask_Campaign.Account__c == idAccount){
								
								if ( (oTask_Campaign.Status != 'Completed') && (oTask_Campaign.Status != 'Deferred') ){
									bAllTasksCompleted = false;
									break;
								}
							
							}

						}

						if (!bAllTasksCompleted){

							// Not All Tasks are completed but at least 1 is completed ==> Campaign Account is updated to the status "In Progress"
							tStatus = 'In Progress';

						}else{

							// All Tasks are completed ==> Campaign Account is updated to the status "Completed"
							tStatus = 'Completed';

						}
					
					}else{

						// Not all Tasks are completed - verify if 1 or more tasks are "In Progress"
						// If all other Tasks are completed and this Task is updated to "Deferred" we should ignore the deferred status.
						Boolean bInProgress = false;
						Boolean bAllTasksCompleted = true;
						Boolean bHasCompletedTask = false;

						for (Task oTask_Campaign : lstTask_Campaign){

							if (oTask_Campaign.Account__c == idAccount){
								
								if (oTask_Campaign.Status == 'Completed') bHasCompletedTask = true;

								if ( (oTask_Campaign.Status != 'Completed') && (oTask_Campaign.Status != 'Deferred') ){
									bAllTasksCompleted = false;
								}
								if (setInProgress.contains(oTask_Campaign.Status)){
									bInProgress = true;
									break;
								}
							
							}

						}

						if (bHasCompletedTask && bAllTasksCompleted){

							tStatus = 'Completed';

						}else if (bInProgress){

							// At least 1 task is "In Progress" ==> Campaign Account is updated to the status "In Progress"
							tStatus = 'In Progress';

						}
					
					}

					Map<Id, String> mapAccountId_Status = new Map<Id, String>();
					if (mapCampaignId_AccountId_Status.containsKey(idCampaign)) mapAccountId_Status = mapCampaignId_AccountId_Status.get(idCampaign);
					mapAccountId_Status.put(idAccount, tStatus);
					setID_Account_All.add(idAccount);
					mapCampaignId_AccountId_Status.put(idCampaign, mapAccountId_Status);

				}


				if (idContact != null){

					List<Task> lstTask_Campaign = mapCampaignId_Tasks.get(idCampaign);
					
					if (oTask.Status == 'Completed'){

						// Verify if all Tasks related to this Contact / Campaign Tactic are Completed or not
						Boolean bAllTasksCompleted = true;

						for (Task oTask_Campaign : lstTask_Campaign){

							if (oTask_Campaign.WhoId == idContact){
								
								if ( (oTask_Campaign.Status != 'Completed') && (oTask_Campaign.Status != 'Deferred') ){
									bAllTasksCompleted = false;
									break;
								}
							
							}

						}

						if (!bAllTasksCompleted){

							// Not All Tasks are completed but at least 1 is completed ==> Campaign Member is updated to the status "In Progress"
							tStatus = 'In Progress';

						}else{

							// All Tasks are completed ==> Campaign Member is updated to the status "Completed"
							tStatus = 'Completed';

						}
					
					}else{

						// Not all Tasks are completed - verify if 1 or more tasks are "In Progress"
						// If all other Tasks are completed and this Task is updated to "Deferred" we should ignore the deferred status.
						Boolean bInProgress = false;
						Boolean bAllTasksCompleted = true;
						Boolean bHasCompletedTask = false;

						for (Task oTask_Campaign : lstTask_Campaign){

							if (oTask_Campaign.WhoId == idContact){
								
								if (oTask_Campaign.Status == 'Completed') bHasCompletedTask = true;

								if ( (oTask_Campaign.Status != 'Completed') && (oTask_Campaign.Status != 'Deferred') ){
									bAllTasksCompleted = false;
								}

								if (setInProgress.contains(oTask_Campaign.Status)){
									bInProgress = true;
									break;
								}
							
							}

						}


						if (bHasCompletedTask && bAllTasksCompleted){

							tStatus = 'Completed';

						}else if (bInProgress){

							// At least 1 task is "In Progress" ==> Campaign Account is updated to the status "In Progress"
							tStatus = 'In Progress';

						}
					
					}

					Map<Id, String> mapContactId_Status = new Map<Id, String>();
					if (mapCampaignId_ContactId_Status.containsKey(idCampaign)) mapContactId_Status = mapCampaignId_ContactId_Status.get(idCampaign);
					mapContactId_Status.put(idContact, tStatus);
					setID_Contact_All.add(idContact);
					mapCampaignId_ContactId_Status.put(idCampaign, mapContactId_Status);

				}

			}

		}

		List<Campaign_Account__c> lstCampaignAccount_Update = new List<Campaign_Account__c>();
		if (mapCampaignId_AccountId_Status.size() > 0){
			
			List<Campaign_Account__c> lstCampaignAccount = [SELECT Id, Campaign__c, Account__c, Status__c FROM Campaign_Account__c WHERE Campaign__c = :mapCampaignId_AccountId_Status.keySet() AND Account__c in :setID_Account_All];

			for (Campaign_Account__c oCampaignAccount : lstCampaignAccount){

				Map<Id, String> mapAccountId_Status = mapCampaignId_AccountId_Status.get(oCampaignAccount.Campaign__c);

				if (mapAccountId_Status.containsKey(oCampaignAccount.Account__c)){

					oCampaignAccount.Status__c = mapAccountId_Status.get(oCampaignAccount.Account__c);
					lstCampaignAccount_Update.add(oCampaignAccount);

				}

			}

		}
		if (lstCampaignAccount_Update.size() > 0) Database.update(lstCampaignAccount_Update, false);


		List<CampaignMember> lstCampaignMember_Update = new List<CampaignMember>();
		if (mapCampaignId_ContactId_Status.size() > 0){
			
			List<CampaignMember> lstCampaignMember = [SELECT Id, CampaignId, ContactId, Status FROM CampaignMember WHERE CampaignId = :mapCampaignId_ContactId_Status.keySet() AND ContactId = :setID_Contact_All];

			for (CampaignMember oCampaignMember : lstCampaignMember){

				Map<Id, String> mapContactId_Status = mapCampaignId_ContactId_Status.get(oCampaignMember.CampaignId);

				if (mapContactId_Status.containsKey(oCampaignMember.ContactId)){

					oCampaignMember.Status = mapContactId_Status.get(oCampaignMember.ContactId);
					lstCampaignMember_Update.add(oCampaignMember);

				}

			}

		}
		if (lstCampaignMember_Update.size() > 0) Database.update(lstCampaignMember_Update, false);

	}
	//----------------------------------------------------------------------------------------------------------------
}
//--------------------------------------------------------------------------------------------------------------------