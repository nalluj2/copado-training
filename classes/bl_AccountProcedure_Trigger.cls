//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 11-09-2017
//  Description      : APEX Class - Business Logic for tr_AccountProcedure
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public with sharing class bl_AccountProcedure_Trigger {

	//----------------------------------------------------------------------------------------------------------------
	// Check for duplicate EMEA_MITG_Procedure records based on the Account__c and Procedure__c combination
	//----------------------------------------------------------------------------------------------------------------
	public static void preventDuplicate(List<Account_Procedure__c> lstTriggerNew){

        if (bl_Trigger_Deactivation.isTriggerDeactivated('AccProc_preventDuplicate')) return;

		Set<Id> setID_RecordType = new Set<Id>();
		setID_RecordType.add(clsUtil.getRecordTypeByDevName('Account_Procedure__c', 'EMEA_MITG_Procedure').Id);

		Set<Id> setID_Account = new Set<Id>();
		Set<String> setProcedure = new Set<String>();
		Set<Id> setID_AccountProcedure = new Set<Id>();

		for (Account_Procedure__c oAccountProcedure : lstTriggerNew){

			if (setID_RecordType.contains(oAccountProcedure.RecordTypeId)){

				String tUniqueKey = oAccountProcedure.Account__c + '_' + oAccountProcedure.Procedure__c;
				setID_Account.add(oAccountProcedure.Account__c);
				setProcedure.add(oAccountProcedure.Procedure__c);
				if (oAccountProcedure.Id != null) setID_AccountProcedure.add(oAccountProcedure.Id);

			}

		}

		Map<String, Account_Procedure__c> mapKey_AccountProcedure = new Map<String, Account_Procedure__c>();
		Set<String> setExistingKey = new Set<String>();
		for (Account_Procedure__c oAccountProcedure : [SELECT Id, Name, Account__c, Procedure__c FROM Account_Procedure__c WHERE Id != :setID_AccountProcedure AND Account__c = :setID_Account AND Procedure__c = :setProcedure]){
			String tUniqueKey = oAccountProcedure.Account__c + '_' + oAccountProcedure.Procedure__c;
			mapKey_AccountProcedure.put(tUniqueKey, oAccountProcedure);
		}

		for (Account_Procedure__c oAccountProcedure : lstTriggerNew){

			if (setID_RecordType.contains(oAccountProcedure.RecordTypeId)){

				String tUniqueKey = oAccountProcedure.Account__c + '_' + oAccountProcedure.Procedure__c;

				if (mapKey_AccountProcedure.containsKey(tUniqueKey)){
					String tURL = '<a style="text-decoration: none;" href="/' + mapKey_AccountProcedure.get(tUniqueKey).Id + '">' + mapKey_AccountProcedure.get(tUniqueKey).Name + '</a>';
					oAccountProcedure.addError('The selected Procedure already exists for this Account : ' + tURL, false);

				}

			}

		}

	}
	//----------------------------------------------------------------------------------------------------------------

}
//--------------------------------------------------------------------------------------------------------------------