/*
Helper class to encrypt and decrypt url parameters. 
In this way Users will not be able to play around with them and try to hack the application 
*/
public with sharing class CryptoUtils {
	
	private final static String encodedKey = 'YdieEYh4JVu7VBQw0uD1zA==';
		
	public static String encryptText(String text){
		
		Blob key = EncodingUtil.base64Decode(encodedKey);		
		return EncodingUtil.base64Encode(Crypto.encryptWithManagedIV('AES128', key, Blob.valueOf(text)));
	}
	
	public static String decryptText(String text){
		
		Blob key = EncodingUtil.base64Decode(encodedKey);
		return Crypto.decryptWithManagedIV('AES128', key, EncodingUtil.base64Decode(text)).toString();
	}
}