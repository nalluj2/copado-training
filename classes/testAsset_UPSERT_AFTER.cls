@isTest
public class testAsset_UPSERT_AFTER {

    private static Id id_Account;
    private static Id id_Product;
    @isTest static void createTestData(){

        //------------------------------------------------
        // Create Test Data
        //------------------------------------------------
        Synchronization_Service_Setting__c syncSettings = new Synchronization_Service_Setting__c();
            syncSettings.username__c = 'test@medtronic.com';
            syncSettings.password__c = 'password';
            syncSettings.client_id__c = '123456789';
            syncSettings.client_secret__c = 'XXXXX';
            syncSettings.is_sandbox__c = true;
            syncSettings.sync_Enabled__c = true;
        insert syncSettings;
        
        //Products
        Product2 testProduct = new Product2();
            testProduct.Name = 'PoleStar';
            testProduct.ProductCode = '000999';
            testProduct.isActive = true;
        insert testProduct;
        id_Product = testProduct.Id;
        
        //Account
        Account acc = new Account();    
            acc.AccountNumber = '0000000';
            acc.Name = 'Syn Test Account';
            acc.Phone = '+123456789';
            acc.BillingCity = 'Minneapolis';
            acc.BillingCountry = 'UNITED STATES';
            acc.BillingState = 'Minnesota';
            acc.BillingStreet = 'street';
            acc.BillingPostalCode = '123456';
            acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non-SAP Account').getRecordTypeId();  
            acc.SAP_Id__c = 'SAP001';
        insert acc;
        id_Account = acc.Id;
        
        //Contact
        Contact cnt = new Contact();
            cnt.FirstName = 'Test';
            cnt.LastName = 'Contact';
            cnt.Phone = '+123456789';       
            cnt.Email = 'test.contact@gmail.com';
            cnt.MobilePhone = '+123456789';     
            cnt.MailingCity = 'Minneapolis';
            cnt.MailingCountry = 'UNITED STATES';
            cnt.MailingState = 'Minnesota';
            cnt.MailingStreet = 'street';
            cnt.MailingPostalCode = '123456';
            cnt.External_Id__c = 'Test_Contact_Id';
            cnt.Contact_Department__c = 'Cardiology';
            cnt.Contact_Primary_Specialty__c = 'Cardiovascular'; 
            cnt.Affiliation_To_Account__c = 'Employee'; 
            cnt.Primary_Job_Title_vs__c = 'Surgeon'; 
            cnt.Contact_Gender__c = 'Male'; 
            cnt.AccountId = acc.Id;
            cnt.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Generic Contact').getRecordTypeId();              
        insert cnt;
        //------------------------------------------------

    }

    @isTest static void test_createNotificationIfChanged_Insert(){

        //------------------------------------------------
        // Create Test Data
        //------------------------------------------------
        createTestData();

        //Asset
        Asset asset = new Asset();
            asset.AccountId = id_Account;
            asset.Product2Id = id_Product;
            asset.Asset_Product_Type__c = 'PoleStar N-10';
            asset.Ownership_Status__c = 'Purchased';
            asset.Name = '123456789';
            asset.Serial_Nr__c = '123456789';
            asset.Status = 'Installed';
            asset.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('PoleStar').getRecordTypeId();
        //------------------------------------------------

        //------------------------------------------------
        // Perform Test
        //------------------------------------------------
        Test.startTest();
        
        Test_bl_SynchronizationService_Utils.CalloutMock mockImpl = new Test_bl_SynchronizationService_Utils.CalloutMock();
		
		Test.setMock(HttpCalloutMock.class, mockImpl);
		bl_SynchronizationService_Utils.calloutMock = mockImpl;

        insert asset;

        Test.stopTest();
        //------------------------------------------------

        //------------------------------------------------
        // Validate Test
        //------------------------------------------------
        List<Sync_Notification__c> notifications = [Select Id from Sync_Notification__c where Status__c = 'New' AND Type__c = 'Outbound' AND Record_Object_Type__c = 'Asset'];
        System.assert(notifications.size() == 1);
        //------------------------------------------------
    }


    @isTest static void test_createNotificationIfChanged_Update(){

        //------------------------------------------------
        // Create Test Data
        //------------------------------------------------
        createTestData();

        //Asset
        Asset asset = new Asset();
            asset.AccountId = id_Account;
            asset.Product2Id = id_Product;
            asset.Asset_Product_Type__c = 'PoleStar N-10';
            asset.Ownership_Status__c = 'Purchased';
            asset.Name = '123456789';
            asset.Serial_Nr__c = '123456789';
            asset.Status = 'Installed';
            asset.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('PoleStar').getRecordTypeId();
        insert asset;
        //------------------------------------------------

        //------------------------------------------------
        // Perform Test
        //------------------------------------------------
        Test.startTest();
        
        Test_bl_SynchronizationService_Utils.CalloutMock mockImpl = new Test_bl_SynchronizationService_Utils.CalloutMock();
		
		Test.setMock(HttpCalloutMock.class, mockImpl);
		bl_SynchronizationService_Utils.calloutMock = mockImpl;

        update asset;

        Test.stopTest();
        //------------------------------------------------

        //------------------------------------------------
        // Validate Test
        //------------------------------------------------
        List<Sync_Notification__c> notifications = [Select Id from Sync_Notification__c where Status__c = 'New' AND Type__c = 'Outbound' AND Record_Object_Type__c = 'Asset'];
        System.assert(notifications.size() == 1);
        //------------------------------------------------
    }

}