//---------------------------------------------------------------------------------------------------------------------------------
//	Author 			: Bart Caelen
//  Created Date 	: 15-05-2018
//  Description 	: APEX Test Class for bl_Campaign, bl_Campaign_Trigger
//  Change Log 		: 
//---------------------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_bl_Campaign {
	
	private static List<String> lstPrimarySpeciality = new List<String>{'Administration','Anesthesiology','Audiologist','Bariatrics','Biomedics'};
	
	@testSetup static void createTestData() {

		User oUser = [SELECT Id FROM User WHERE isActive = true AND Profile.Name = 'IT Business Analyst' AND Id != :UserInfo.getUserId() LIMIT 1];

		// Create Custom Setting Data
		List<Campaign_Calendar__c> lstCampaignCalendar = new List<Campaign_Calendar__c>();
		Campaign_Calendar__c oCampaignCalendar1 = new Campaign_Calendar__c();
			oCampaignCalendar1.Name = 'TEST_CAMPAIGN_CALENDAR_1';
			oCampaignCalendar1.Calendar_Id__c = UserInfo.getUserId();
			oCampaignCalendar1.Campaign_RecordType_Name__c = 'Europe_xBU';
			oCampaignCalendar1.Campaign_Type__c = 'Training & Education';
			oCampaignCalendar1.Campaign_Sub_Type__c = 'Workshop';
		lstCampaignCalendar.add(oCampaignCalendar1);
		Campaign_Calendar__c oCampaignCalendar2 = new Campaign_Calendar__c();
			oCampaignCalendar2.Name = 'TEST_CAMPAIGN_CALENDAR_2';
			oCampaignCalendar2.Calendar_Id__c = oUser.Id;
			oCampaignCalendar2.Campaign_RecordType_Name__c = 'Europe_xBU';
			oCampaignCalendar2.Campaign_Type__c = 'Training & Education';
			oCampaignCalendar2.Campaign_Sub_Type__c = 'Local';
		lstCampaignCalendar.add(oCampaignCalendar2);
		insert lstCampaignCalendar;

		// Create Business Unit Data
		clsTestData_MasterData.tCompanyCode = 'EUR';
		clsTestData_MasterData.createBusinessUnit();
		clsTestData_MasterData.createSubBusinessUnit();
		clsTestData_Therapy.createTherapyGroup();

		List<Therapy_Group__c> lstTherapyGroup = [SELECT Id, Name, Sub_Business_Unit__c, Sub_Business_Unit__r.Business_Unit__c, Sub_Business_Unit__r.Name FROM Therapy_Group__c WHERE Sub_Business_Unit__r.Name in ('ENT', 'Coro + PV') ORDER BY Sub_Business_Unit__r.Name];

		List<Therapy__c> lstTherapy = new List<Therapy__c>();		
        Therapy__c oTherapy1 = new Therapy__c();
            oTherapy1.Name = 'Other Interventional Coronary';
            oTherapy1.Business_Unit__c = lstTherapyGroup[0].Sub_Business_Unit__r.Business_Unit__c;
            oTherapy1.Sub_Business_Unit__c = lstTherapyGroup[0].Sub_Business_Unit__c;
            oTherapy1.Therapy_Group__c = lstTherapyGroup[0].Id;
            oTherapy1.Therapy_Name_Hidden__c = 'Other Interventional Coronary';
        lstTherapy.add(oTherapy1);
        Therapy__c oTherapy2 = new Therapy__c();
            oTherapy2.Name = 'Hearing Restoration';
            oTherapy2.Business_Unit__c = lstTherapyGroup[1].Sub_Business_Unit__r.Business_Unit__c;
            oTherapy2.Sub_Business_Unit__c = lstTherapyGroup[1].Sub_Business_Unit__c;
            oTherapy2.Therapy_Group__c = lstTherapyGroup[1].Id;
            oTherapy2.Therapy_Name_Hidden__c = 'Hearing Restoration';
        lstTherapy.add(oTherapy2);
		insert lstTherapy;

		// Create Contact Data
		clsTestData_Contact.iRecord_Contact = 5;
		List<Contact> lstContact = clsTestData_Contact.createContact(false);
		Integer iCounter = 0;
		for (Contact oContact : lstContact){
			oContact.Contact_Primary_Specialty__c = lstPrimarySpeciality[iCounter];
			iCounter++;
		}
		insert lstContact;

		// Create Campaign & Campaign Member Test Data
		clsTestData_Campaign.idRecordType_Campaign = clsUtil.getRecordTypeByDevName('Campaign', 'Europe_xBU').Id;
		clsTestData_Campaign.createCampaign(false);
			clsTestData_Campaign.oMain_Campaign.StartDate = Date.today();
			clsTestData_Campaign.oMain_Campaign.EndDate = Date.today().addDays(1);
			clsTestData_Campaign.oMain_Campaign.Send_Date__c = Datetime.now().addDays(1);
			clsTestData_Campaign.oMain_Campaign.Type = 'Training & Education';
			clsTestData_Campaign.oMain_Campaign.Sub_Type__c = 'Workshop';
			clsTestData_Campaign.oMain_Campaign.Description = 'Description goes here';
		insert clsTestData_Campaign.oMain_Campaign;

		clsTestData_Campaign.iRecord_CampaignMember = 5;
		clsTestData_Campaign.createCampaignMember();

	}


	@isTest static void test_updateBusinessUnit() {


		//-------------------------------------------------------
		// Create / Select Test Data
		//-------------------------------------------------------
		String tBusinessUnitName = 'Neurovascular';
		List<Business_Unit__c> lstBusinessUnit = [SELECT Id FROM Business_Unit__c WHERE Name = :tBusinessUnitName AND Company__r.Company_Code_Text__c = 'EUR'];

		List<Campaign> lstCampaign = 
			[
				SELECT 
					Id, Business_Unit__c, Business_Unit_Picklist__c
				FROM
					Campaign
			];
		System.assertEquals(lstCampaign.size(), 1);
		System.assert(lstCampaign[0].Business_Unit_Picklist__c == null);
		System.assert(lstCampaign[0].Business_Unit__c == null);
		//-------------------------------------------------------


		//-------------------------------------------------------
		// Test Logic
		//-------------------------------------------------------
		Test.startTest();

				lstCampaign[0].Business_Unit_Picklist__c = tBusinessUnitName;
			update lstCampaign[0];

		Test.stopTest();
		//-------------------------------------------------------


		//-------------------------------------------------------
		// Verify Result
		//-------------------------------------------------------
		lstCampaign = 
			[
				SELECT 
					Id, Business_Unit__c, Business_Unit_Picklist__c
				FROM
					Campaign
			];
		System.assertEquals(lstCampaign.size(), 1);
		System.assertEquals(lstCampaign[0].Business_Unit_Picklist__c, tBusinessUnitName);
		System.assertEquals(lstCampaign[0].Business_Unit__c, lstBusinessUnit[0].Id);
		//-------------------------------------------------------

	}


	@isTest static void test_createEvent() {

		//-------------------------------------------------------
		// Create / Select Test Data
		//-------------------------------------------------------
		List<Campaign> lstCampaign = 
			[
				SELECT 
					Id, Name, Description, StartDate, EndDate, Send_Date__c
				FROM
					Campaign
			];
		//-------------------------------------------------------


		//-------------------------------------------------------
		// Test Logic
		//-------------------------------------------------------
		Test.startTest();

			bl_Campaign.createEvent(lstCampaign[0], UserInfo.getUserId(), null);

		Test.stopTest();
		//-------------------------------------------------------


		//-------------------------------------------------------
		// Verify Result
		//-------------------------------------------------------
		//-------------------------------------------------------

	}


	@isTest static void test_getCalendarIdForCampaign() {


		//-------------------------------------------------------
		// Create / Select Test Data
		//-------------------------------------------------------
		List<Campaign> lstCampaign = 
			[
				SELECT 
					Id, RecordTypeId, Type, Sub_Type__c
				FROM
					Campaign
			];
		//-------------------------------------------------------


		//-------------------------------------------------------
		// Test Logic
		//-------------------------------------------------------
		Test.startTest();

			bl_Campaign.getCalendarIdForCampaign(lstCampaign[0]);

		Test.stopTest();
		//-------------------------------------------------------


		//-------------------------------------------------------
		// Verify Result
		//-------------------------------------------------------
		//-------------------------------------------------------

	}


	@isTest static void test_createCalendarEvent(){

		//-------------------------------------------------------
		// Create / Select Test Data
		//-------------------------------------------------------
		List<Campaign> lstCampaign = 
			[
				SELECT 
					Id, RecordTypeId, Type, Sub_Type__c, Send_Date__c
				FROM
					Campaign
			];
		//-------------------------------------------------------


		//-------------------------------------------------------
		// Test Logic
		//-------------------------------------------------------
		Test.startTest();

			List<Event> lstEvent = [SELECT Id, WhatId FROM Event WHERE WhatId = :lstCampaign[0].Id];
			System.assertEquals(lstEvent.size(), 1);

		Test.stopTest();
		//-------------------------------------------------------


		//-------------------------------------------------------
		// Verify Result
		//-------------------------------------------------------
		//-------------------------------------------------------

	}


	@isTest static void test_updateCalendarEvent(){

		//-------------------------------------------------------
		// Create / Select Test Data
		//-------------------------------------------------------
		List<Campaign> lstCampaign = 
			[
				SELECT 
					Id, StartDate, EndDate, Send_Date__c, RecordTypeId, Type, Sub_Type__c
				FROM
					Campaign
			];
		//-------------------------------------------------------


		//-------------------------------------------------------
		// Test Logic
		//-------------------------------------------------------
		Test.startTest();

				lstCampaign[0].StartDate = Date.today().addDays(30);
				lstCampaign[0].EndDate = Date.today().addDays(31);
			update lstCampaign[0];


				lstCampaign[0].Sub_Type__c = 'National';
			update lstCampaign[0];

				lstCampaign[0].Sub_Type__c = 'Local';
			update lstCampaign[0];

		Test.stopTest();
		//-------------------------------------------------------


		//-------------------------------------------------------
		// Verify Result
		//-------------------------------------------------------
		List<Event> lstEvent = [SELECT Id, WhatId, StartDateTime, EndDateTime FROM Event WHERE WhatId = :lstCampaign[0].Id];
		System.assertEquals(lstEvent.size(), 1);
		//-------------------------------------------------------

	}


	@isTest static void test_preventDuplicateCampaign_SBU(){

		//-------------------------------------------------------
		// Create Test Data
		//-------------------------------------------------------
		Campaign oCampaign1 = new Campaign();
			oCampaign1.RecordTypeId = clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_SBU').Id;
			oCampaign1.Name = 'TEST001';
			oCampaign1.Fiscal_Year__c = 'FY2021';
			oCampaign1.Campaign_Country_vs__c = 'BELGIUM';
			oCampaign1.sBU_vs__c = 'Coro + PV';
			oCampaign1.Status = 'Planned';
		insert oCampaign1;
		//-------------------------------------------------------


		//-------------------------------------------------------
		// Test Logic
		//-------------------------------------------------------
		Test.startTest();

			// INSERT - DUPLCIATE
			String tError = '';
			Campaign oCampaign2 = new Campaign();
				oCampaign2.RecordTypeId = clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_SBU').Id;
				oCampaign2.Name = 'TEST002';
				oCampaign2.Status = oCampaign1.Status;
				oCampaign2.Fiscal_Year__c = oCampaign1.Fiscal_Year__c;
				oCampaign2.Campaign_Country_vs__c = oCampaign1.Campaign_Country_vs__c;
				oCampaign2.sBU_vs__c = oCampaign1.sBU_vs__c;
			try{
				insert oCampaign2;
			}catch(Exception oEX){
				tError = oEX.getMessage();
			}
			System.assert(!String.isBlank(tError));
			System.assert(tError.contains('TEST001'));


			// INSERT - NO DUPLCIATE
			tError = '';
			oCampaign2 = new Campaign();
				oCampaign2.RecordTypeId = clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_SBU').Id;
				oCampaign2.Name = 'TEST002';
				oCampaign2.Status = oCampaign1.Status;
				oCampaign2.Fiscal_Year__c = 'FY2023';
				oCampaign2.Campaign_Country_vs__c = oCampaign1.Campaign_Country_vs__c;
				oCampaign2.sBU_vs__c = oCampaign1.sBU_vs__c;
			try{
				insert oCampaign2;
			}catch(Exception oEX){
				tError = oEX.getMessage();
			}
			System.assert(String.isBlank(tError));


			// UPDATE - DUPLCIATE
			tError = '';
			oCampaign2.Fiscal_Year__c = 'FY2021';
			try{
				update oCampaign2;
			}catch(Exception oEX){
				tError = oEX.getMessage();
			}
			System.assert(!String.isBlank(tError));
			System.assert(tError.contains('TEST001'));


			// UPDATE - NO DUPLCIATE
			tError = '';
			oCampaign2.Fiscal_Year__c = 'FY2022';
			try{
				update oCampaign2;
			}catch(Exception oEX){
				tError = oEX.getMessage();
			}
			System.assert(String.isBlank(tError));

		Test.stopTest();
		//-------------------------------------------------------


		//-------------------------------------------------------
		// Verify Result
		//-------------------------------------------------------

		oCampaign2 = [SELECT Id, Business_Unit_Group__c FROM Campaign WHERE Id = :oCampaign2.Id];
		System.assertEquals(oCampaign2.Business_Unit_Group__c, 'CVG');

		//-------------------------------------------------------

	}


	@isTest static void test_populateFields(){

		//-------------------------------------------------------
		// Create Test Data
		//-------------------------------------------------------
		Sub_Business_Units__c oSBU = [SELECT Id FROM Sub_Business_Units__c WHERE Name = 'Coro + PV'];
		System.assert(oSBU != null);
		Goal__c oGoal = new Goal__c();
			oGoal.Active__c = true;
			oGoal.Sub_Business_Unit__c = oSBU.Id;
		insert oGoal;

		Campaign oCampaign_SBU = new Campaign();
			oCampaign_SBU.RecordTypeId = clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_SBU').Id;
			oCampaign_SBU.Name = 'TEST Campaign SBU';
			oCampaign_SBU.Fiscal_Year__c = 'FY2021';
			oCampaign_SBU.Campaign_Country_vs__c = 'BELGIUM';
			oCampaign_SBU.sBU_vs__c = 'Coro + PV';
			oCampaign_SBU.Status = 'Planned';
		insert oCampaign_SBU;
		//-------------------------------------------------------


		//-------------------------------------------------------
		// Test Logic
		//-------------------------------------------------------
		Test.startTest();

			Campaign oCampaign_Goal = new Campaign();
				oCampaign_Goal.RecordTypeId = clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_Goal').Id;
				oCampaign_Goal.ParentId = oCampaign_SBU.Id;
				oCampaign_Goal.Name = 'TEST Campaign Goal';
				oCampaign_Goal.Goal__c = oGoal.Id;
			insert oCampaign_Goal;

			Campaign oCampaign_Strategy = new Campaign();
				oCampaign_Strategy.RecordTypeId = clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_Strategy').Id;
				oCampaign_Strategy.ParentId = oCampaign_Goal.Id;
				oCampaign_Strategy.Name = 'TEST Campaign Strategy';
			insert oCampaign_Strategy;

			Campaign oCampaign_Tactic = new Campaign();
				oCampaign_Tactic.RecordTypeId = clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_Tactic').Id;
				oCampaign_Tactic.ParentId = oCampaign_Strategy.Id;
				oCampaign_Tactic.Name = 'TEST Campaign Tactic';
			insert oCampaign_Tactic;

		Test.stopTest();
		//-------------------------------------------------------


		//-------------------------------------------------------
		// Verify Result
		//-------------------------------------------------------
		List<Id> lstID_Campaign = new List<Id>{oCampaign_SBU.Id, oCampaign_Goal.Id, oCampaign_Strategy.Id, oCampaign_Tactic.Id};
		List<Campaign> lstCampaign = [SELECT Id, Name, Business_Unit_Group__c, Campaign_Country_vs__c, sBU_vs__c FROM Campaign WHERE Id = :lstID_Campaign];
		for (Campaign oCampaign : lstCampaign){

			System.assertEquals(oCampaign.Business_Unit_Group__c, 'CVG');
			System.assertEquals(oCampaign.Campaign_Country_vs__c, 'BELGIUM');
			System.assertEquals(oCampaign.sBU_vs__c, 'Coro + PV');

		}
		//-------------------------------------------------------

	}


	@isTest static void test_validateCampaign_Goal(){

		//-------------------------------------------------------
		// Create Test Data
		//-------------------------------------------------------
		List<Therapy__c> lstTherapy = [SELECT Id, Name, Sub_Business_Unit__c, Sub_Business_Unit__r.Name FROM Therapy__c WHERE Name = 'Other Interventional Coronary' LIMIT 1];
		String tSBU = lstTherapy[0].Sub_Business_Unit__r.Name;
		System.debug('**BC** tSBU  ' + tSBU );

		Goal__c oGoal = new Goal__c();
			oGoal.Active__c = true;
			oGoal.Sub_Business_Unit__c = lstTherapy[0].Sub_Business_Unit__c;
		insert oGoal;

		Campaign oCampaign_SBU = new Campaign();
			oCampaign_SBU.RecordTypeId = clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_SBU').Id;
			oCampaign_SBU.Name = 'TEST Campaign SBU';
			oCampaign_SBU.Fiscal_Year__c = 'FY2021';
			oCampaign_SBU.Campaign_Country_vs__c = 'BELGIUM';
			oCampaign_SBU.sBU_vs__c = tSBU;
			oCampaign_SBU.Status = 'Planned';
		insert oCampaign_SBU;
		//-------------------------------------------------------


		//-------------------------------------------------------
		// Test Logic
		//-------------------------------------------------------
		String tError = '';

		Test.startTest();

			Campaign oCampaign_Goal = new Campaign();
				oCampaign_Goal.RecordTypeId = clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_Goal').Id;
				oCampaign_Goal.ParentId = oCampaign_SBU.Id;
				oCampaign_Goal.Name = 'TEST Campaign Goal';
				oCampaign_Goal.Goal__c = oGoal.Id;
				oCampaign_Goal.sBU_vs__c = lstTherapy[0].Sub_Business_Unit__r.Name;
				oCampaign_Goal.Therapy_vs__c = lstTherapy[0].Name;
			try{
				insert oCampaign_Goal;
			}catch(Exception oEX){
				tError = oEX.getMessage();
				System.debug('**BC** oEX.getMessage() : ' + oEX.getMessage());
			}
			System.assert(String.isBlank(tError));

			tError = '';
			oCampaign_Goal.Therapy_vs__c = 'Hearing Restoration';
			try{
				update oCampaign_Goal;
			}catch(Exception oEX){
				tError = oEX.getMessage();
			}
			System.assert(!String.isBlank(tError));
			System.assert(tError.contains('Hearing Restoration'));

		Test.stopTest();
		//-------------------------------------------------------


		//-------------------------------------------------------
		// Verify Result
		//-------------------------------------------------------

		//-------------------------------------------------------
	}


	@isTest static void test_validateCampaign_Strategy(){

		//-------------------------------------------------------
		// Create Test Data
		//-------------------------------------------------------
		List<Therapy__c> lstTherapy = [SELECT Id, Name, Sub_Business_Unit__c, Sub_Business_Unit__r.Name FROM Therapy__c WHERE Name = 'Other Interventional Coronary' LIMIT 1];
		String tSBU = lstTherapy[0].Sub_Business_Unit__r.Name;
		System.debug('**BC** tSBU  ' + tSBU );
		List<Sub_Business_Units__c> lstSBU_Other = [SELECT Id, Name FROM Sub_Business_Units__c WHERE Id != :lstTherapy[0].Sub_Business_Unit__c];

		List<Goal__c> lstGoal = new List<Goal__c>();
		Goal__c oGoal = new Goal__c();
			oGoal.Active__c = true;
			oGoal.Sub_Business_Unit__c = lstTherapy[0].Sub_Business_Unit__c;
		lstGoal.add(oGoal);
		Goal__c oGoal_Other = new Goal__c();
			oGoal_Other.Active__c = true;
			oGoal_Other.Sub_Business_Unit__c = lstSBU_Other[0].Id;
		lstGoal.add(oGoal_Other);
		insert lstGoal;

		List<Strategy__c> lstStrategy = new List<Strategy__c>();
		Strategy__c oStrategy1 = new Strategy__c();
			oStrategy1.Name = 'TESTSTRAT1';
			oStrategy1.Goal__c = oGoal.Id;
			oStrategy1.Active__c = true;
		lstStrategy.add(oStrategy1);
		Strategy__c oStrategy2 = new Strategy__c();
			oStrategy2.Name = 'TESTSTRAT2';
			oStrategy2.Goal__c = oGoal.Id;
			oStrategy2.Active__c = false;
		lstStrategy.add(oStrategy2);
		Strategy__c oStrategy3 = new Strategy__c();
			oStrategy3.Name = 'TESTSTRAT3';
			oStrategy3.Goal__c = oGoal_Other.Id;
			oStrategy3.Active__c = true;
		lstStrategy.add(oStrategy3);
		insert lstStrategy;


		Campaign oCampaign_SBU = new Campaign();
			oCampaign_SBU.RecordTypeId = clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_SBU').Id;
			oCampaign_SBU.Name = 'TEST Campaign SBU';
			oCampaign_SBU.Fiscal_Year__c = 'FY2021';
			oCampaign_SBU.Campaign_Country_vs__c = 'BELGIUM';
			oCampaign_SBU.sBU_vs__c = tSBU;
			oCampaign_SBU.Status = 'Planned';
		insert oCampaign_SBU;

		Campaign oCampaign_Goal = new Campaign();
			oCampaign_Goal.RecordTypeId = clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_Goal').Id;
			oCampaign_Goal.ParentId = oCampaign_SBU.Id;
			oCampaign_Goal.Name = 'TEST Campaign Goal';
			oCampaign_Goal.Goal__c = oGoal.Id;
			oCampaign_Goal.Therapy_vs__c = lstTherapy[0].Name;
			oCampaign_Goal.sBU_vs__c = lstTherapy[0].Sub_Business_Unit__r.Name;
		insert oCampaign_Goal;
		//-------------------------------------------------------


		//-------------------------------------------------------
		// Test Logic
		//-------------------------------------------------------
		String tError = '';

		Test.startTest();

			Campaign oCampaign_Strategy = new Campaign();
				oCampaign_Strategy.RecordTypeId = clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_Strategy').Id;
				oCampaign_Strategy.ParentId = oCampaign_Goal.Id;
				oCampaign_Strategy.Name = 'TEST Campaign Strategy';
				oCampaign_Strategy.Therapy_vs__c = lstTherapy[0].Name;
				oCampaign_Strategy.sBU_vs__c = lstTherapy[0].Sub_Business_Unit__r.Name;
				oCampaign_Strategy.Strategy__c = oStrategy1.Id;
			try{
				insert oCampaign_Strategy;
			}catch(Exception oEX){
				tError = oEX.getMessage();
			}
			System.assert(String.isBlank(tError));


			tError = '';
			oCampaign_Strategy.Therapy_vs__c = 'Hearing Restoration';
			try{
				update oCampaign_Strategy;
			}catch(Exception oEX){
				tError = oEX.getMessage();
			}
			System.assert(!String.isBlank(tError));
			System.assert(tError.contains('Hearing Restoration'));


			tError = '';
			oCampaign_Strategy.Therapy_vs__c = lstTherapy[0].Name;
			oCampaign_Strategy.sBU_vs__c = lstTherapy[0].Sub_Business_Unit__r.Name;
			oCampaign_Strategy.Strategy__c = oStrategy2.Id;
			try{
				update oCampaign_Strategy;
			}catch(Exception oEX){
				tError = oEX.getMessage();
			}
			System.assert(!String.isBlank(tError));
			System.assert(tError.contains('You can only select an active Strategy which is linked to the Goal of the Parent Campaign'));


			tError = '';
			oCampaign_Strategy.Strategy__c = oStrategy3.Id;
			try{
				update oCampaign_Strategy;
			}catch(Exception oEX){
				tError = oEX.getMessage();
			}
			System.assert(!String.isBlank(tError));
			System.assert(tError.contains('You can only select an active Strategy which is linked to the Goal of the Parent Campaign'));

		Test.stopTest();
		//-------------------------------------------------------


		//-------------------------------------------------------
		// Verify Result
		//-------------------------------------------------------

		//-------------------------------------------------------
	}


	@isTest static void test_validateCampaign_Tactic(){

		//-------------------------------------------------------
		// Create Test Data
		//-------------------------------------------------------
		List<Therapy__c> lstTherapy = [SELECT Id, Name, Sub_Business_Unit__c, Sub_Business_Unit__r.Name FROM Therapy__c WHERE Name = 'Other Interventional Coronary' LIMIT 1];
		String tSBU = lstTherapy[0].Sub_Business_Unit__r.Name;
		System.debug('**BC** tSBU  ' + tSBU );
		List<Sub_Business_Units__c> lstSBU_Other = [SELECT Id, Name FROM Sub_Business_Units__c WHERE Id != :lstTherapy[0].Sub_Business_Unit__c];

		Goal__c oGoal = new Goal__c();
			oGoal.Active__c = true;
			oGoal.Sub_Business_Unit__c = lstTherapy[0].Sub_Business_Unit__c;
		insert oGoal;

		List<Strategy__c> lstStrategy = new List<Strategy__c>();
		Strategy__c oStrategy = new Strategy__c();
			oStrategy.Name = 'TESTSTRAT1';
			oStrategy.Goal__c = oGoal.Id;
			oStrategy.Active__c = true;
		lstStrategy.add(oStrategy);
		Strategy__c oStrategy_Other = new Strategy__c();
			oStrategy_Other.Name = 'TESTSTRAT2';
			oStrategy_Other.Goal__c = oGoal.Id;
			oStrategy_Other.Active__c = true;
		lstStrategy.add(oStrategy_Other);
		insert lstStrategy;

		List<Tactic__c> lstTactic = new List<Tactic__c>();
		Tactic__c oTactic_Active = new Tactic__c();
			oTactic_Active.Name = 'TESTTACT1';
			oTactic_Active.Strategy__c = oStrategy.Id;
			oTactic_Active.Active__c = true;
		lstTactic.add(oTactic_Active);
		Tactic__c oTactic_InActive = new Tactic__c();
			oTactic_InActive.Name = 'TESTTACT2';
			oTactic_InActive.Strategy__c = oStrategy.Id;
			oTactic_InActive.Active__c = false;
		lstTactic.add(oTactic_InActive);
		Tactic__c oTactic_Other = new Tactic__c();
			oTactic_Other.Name = 'TESTTACT3';
			oTactic_Other.Strategy__c = oStrategy_Other.Id;
			oTactic_Other.Active__c = true;
		lstTactic.add(oTactic_Other);
		insert lstTactic;


		Campaign oCampaign_SBU = new Campaign();
			oCampaign_SBU.RecordTypeId = clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_SBU').Id;
			oCampaign_SBU.Name = 'TEST Campaign SBU';
			oCampaign_SBU.Fiscal_Year__c = 'FY2021';
			oCampaign_SBU.Campaign_Country_vs__c = 'BELGIUM';
			oCampaign_SBU.sBU_vs__c = tSBU;
			oCampaign_SBU.Status = 'Planned';
		insert oCampaign_SBU;

		Campaign oCampaign_Goal = new Campaign();
			oCampaign_Goal.RecordTypeId = clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_Goal').Id;
			oCampaign_Goal.ParentId = oCampaign_SBU.Id;
			oCampaign_Goal.Name = 'TEST Campaign Goal';
			oCampaign_Goal.Goal__c = oGoal.Id;
			oCampaign_Goal.Therapy_vs__c = lstTherapy[0].Name;
			oCampaign_Goal.sBU_vs__c = lstTherapy[0].Sub_Business_Unit__r.Name;
		insert oCampaign_Goal;

		Campaign oCampaign_Strategy = new Campaign();
			oCampaign_Strategy.RecordTypeId = clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_Strategy').Id;
			oCampaign_Strategy.ParentId = oCampaign_Goal.Id;
			oCampaign_Strategy.Name = 'TEST Campaign Strategy';
			oCampaign_Strategy.Therapy_vs__c = lstTherapy[0].Name;
			oCampaign_Strategy.sBU_vs__c = lstTherapy[0].Sub_Business_Unit__r.Name;
			oCampaign_Strategy.Strategy__c = oStrategy.Id;
		insert oCampaign_Strategy;
		//-------------------------------------------------------


		//-------------------------------------------------------
		// Test Logic
		//-------------------------------------------------------
		String tError = '';

		Test.startTest();

			Campaign oCampaign_Tactic = new Campaign();
				oCampaign_Tactic.RecordTypeId = clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_Tactic').Id;
				oCampaign_Tactic.ParentId = oCampaign_Strategy.Id;
				oCampaign_Tactic.Name = 'TEST Campaign Tactic';
				oCampaign_Tactic.Therapy_vs__c = lstTherapy[0].Name;
				oCampaign_Tactic.sBU_vs__c = lstTherapy[0].Sub_Business_Unit__r.Name;
				oCampaign_Tactic.Tactic__c = oTactic_Active.Id;
			try{
				insert oCampaign_Tactic;
			}catch(Exception oEX){
				tError = oEX.getMessage();
			}
			System.assert(String.isBlank(tError));


			tError = '';
			oCampaign_Tactic.Therapy_vs__c = 'Hearing Restoration';
			try{
				update oCampaign_Tactic;
			}catch(Exception oEX){
				tError = oEX.getMessage();
			}
			System.assert(!String.isBlank(tError));
			System.assert(tError.contains('Hearing Restoration'));


			tError = '';
			oCampaign_Tactic.Therapy_vs__c = lstTherapy[0].Name;
			oCampaign_Tactic.sBU_vs__c = lstTherapy[0].Sub_Business_Unit__r.Name;
			oCampaign_Tactic.Tactic__c = oTactic_InActive.Id;
			try{
				update oCampaign_Tactic;
			}catch(Exception oEX){
				tError = oEX.getMessage();
			}
			System.assert(!String.isBlank(tError));
			System.assert(tError.contains('You can only select an active Tactic which is linked to the Strategy of the Parent Campaign'));


			tError = '';
			oCampaign_Tactic.Tactic__c = oTactic_Other.Id;
			try{
				update oCampaign_Tactic;
			}catch(Exception oEX){
				tError = oEX.getMessage();
			}
			System.assert(!String.isBlank(tError));
			System.assert(tError.contains('You can only select an active Tactic which is linked to the Strategy of the Parent Campaign'));


			tError = '';
			oCampaign_Tactic.Tactic__c = oTactic_Active.Id;
			try{
				update oCampaign_Tactic;
			}catch(Exception oEX){
				tError = oEX.getMessage();
			}
			System.assert(String.isBlank(tError));


		Test.stopTest();
		//-------------------------------------------------------


		//-------------------------------------------------------
		// Verify Result
		//-------------------------------------------------------

		//-------------------------------------------------------
	}


	@isTest static void test_setCampaignActiveStatus(){

		//-------------------------------------------------------
		// Create Test Data
		//-------------------------------------------------------
		Sub_Business_Units__c oSBU = [SELECT Id FROM Sub_Business_Units__c WHERE Name = 'Coro + PV'];
		System.assert(oSBU != null);
		Goal__c oGoal = new Goal__c();
			oGoal.Active__c = true;
			oGoal.Sub_Business_Unit__c = oSBU.Id;
		insert oGoal;


		List<Campaign> lstCampaign = new List<Campaign>();
		Campaign oCampaign_SBU = new Campaign();
			oCampaign_SBU.RecordTypeId = clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_SBU').Id;
			oCampaign_SBU.Name = 'TEST Campaign SBU';
			oCampaign_SBU.Fiscal_Year__c = 'FY2021';
			oCampaign_SBU.Campaign_Country_vs__c = 'BELGIUM';
			oCampaign_SBU.sBU_vs__c = 'Coro + PV';
			oCampaign_SBU.Status = 'Planned';
		lstCampaign.add(oCampaign_SBU);
		insert oCampaign_SBU;
		Campaign oCampaign_Goal = new Campaign();
			oCampaign_Goal.RecordTypeId = clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_Goal').Id;
			oCampaign_Goal.ParentId = oCampaign_SBU.Id;
			oCampaign_Goal.Name = 'TEST Campaign Goal';
			oCampaign_Goal.Goal__c = oGoal.Id;
			oCampaign_Goal.Status = 'Planned';
		lstCampaign.add(oCampaign_Goal);
		insert oCampaign_Goal;
		Campaign oCampaign_Strategy = new Campaign();
			oCampaign_Strategy.RecordTypeId = clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_Strategy').Id;
			oCampaign_Strategy.ParentId = oCampaign_Goal.Id;
			oCampaign_Strategy.Name = 'TEST Campaign Strategy';
			oCampaign_Strategy.Status = 'Planned';
		lstCampaign.add(oCampaign_Strategy);
		insert oCampaign_Strategy;
		Campaign oCampaign_Tactic = new Campaign();
			oCampaign_Tactic.RecordTypeId = clsUtil.getRecordTypeByDevName('Campaign', 'Campaign_Tactic').Id;
			oCampaign_Tactic.ParentId = oCampaign_Strategy.Id;
			oCampaign_Tactic.Name = 'TEST Campaign Tactic';
			oCampaign_Tactic.Status = 'Planned';
		lstCampaign.add(oCampaign_Tactic);
		insert oCampaign_Tactic;
		//-------------------------------------------------------


		//-------------------------------------------------------
		// Test Logic
		//-------------------------------------------------------
		Test.startTest();

			lstCampaign = [SELECT Id, IsActive FROM Campaign WHERE Id = :lstCampaign];
			System.assertEquals(lstCampaign.size(), 4);
			for (Campaign oCampaign : lstCampaign){
				System.assertEquals(oCampaign.IsActive, true);
				oCampaign.Status = 'Completed';
			}

			update lstCampaign;

		Test.stopTest();
		//-------------------------------------------------------


		//-------------------------------------------------------
		// Verify Result
		//-------------------------------------------------------
		lstCampaign = [SELECT Id, IsActive FROM Campaign WHERE Id = :lstCampaign];
		for (Campaign oCampaign : lstCampaign){
			System.assertEquals(oCampaign.IsActive, false);
		}
		//-------------------------------------------------------
		
	}	

}
//---------------------------------------------------------------------------------------------------------------------------------