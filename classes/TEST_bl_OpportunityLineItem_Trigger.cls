//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 24-05-2016
//  Description      : APEX TEST Class - Test APEX for :
//						- tr_OpportunityLineItem (APEX Trigger)
//					 	- bl_bl_OpportunityLineItem_Trigger (APEX Class)
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_bl_OpportunityLineItem_Trigger {

    //----------------------------------------------------------------------------------------
    // CREATE TEST DATA
    //----------------------------------------------------------------------------------------
	@isTest private static void createTestData(){

		clsTestData_MasterData.tCompanyCode = 'EUR';
		clsTestData_MasterData.createCompany();

		clsTestData_MasterData.createBusinessUnit();

		clsTestData_Account.createAccount();

		clsTestData_Product.iRecord_Product = 5;
		clsTestData_Product.createProduct(false);
		for (Product2 oProduct : clsTestData_Product.lstProduct){
			oProduct.CanUseRevenueSchedule = true;
		}
		insert clsTestData_Product.lstProduct;

		clsTestData_Opportunity.iRecord_Pricebook = 1;
		clsTestData_Opportunity.createPricebook(false);
		clsTestData_Opportunity.lstPricebook[0].Name = 'TEST_PRICEBOOK';
		insert clsTestData_Opportunity.lstPricebook;

		clsTestData_Opportunity.createPricebookEntry();


		clsTestData_Opportunity.createOpportunity(false);
			clsTestData_Opportunity.oMain_Opportunity.CurrencyISOCode = 'EUR';
			clsTestData_Opportunity.oMain_Opportunity.Pricebook2Id = clsTestData_Opportunity.oMain_Pricebook.Id;
			clsTestData_Opportunity.oMain_Opportunity.Stagename = 'Develop';
		insert clsTestData_Opportunity.oMain_Opportunity;

	}
    //----------------------------------------------------------------------------------------
	
	@isTest static void test_setPriceBasedOnAllLost_Insert() {
		
		createTestData();

		Opportunity oOpportunity = [SELECT Id, Name, Pricebook2Id FROM Opportunity WHERE Id = :clsTestData_Opportunity.oMain_Opportunity.Id];
			oOpportunity.Pricebook2Id = clsTestData_Opportunity.oMain_Pricebook.Id;
		update oOpportunity;

		List<PricebookEntry> lstPricebookEntry = [SELECT Id, IsActive, Name, Pricebook2Id, Pricebook2.Name, Product2Id, ProductCode, UnitPrice, UseStandardPrice FROM PricebookEntry WHERE Pricebook2.Name = 'TEST_PRICEBOOK'];

		// Create Opportunity Line Items
		List<OpportunityLineItem> lstOLI = new List<OpportunityLineItem>();
		for (PricebookEntry oPricebookEntry : lstPricebookEntry){
			clsUtil.debug('oPricebookEntry.Pricebook2Id : ' + oPricebookEntry.Pricebook2Id);
			if (oPricebookEntry.Pricebook2Id == oOpportunity.Pricebook2Id){
				OpportunityLineItem oOLI = new OpportunityLineItem();
			        oOLI.PricebookEntryId = oPricebookEntry.Id;
			        oOLI.Opportunityid = oOpportunity.Id;
			        oOLI.Quantity = 100;
			        oOLI.UnitPrice = 100;
			        oOLI.All_Lost__c = true;
				lstOLI.add(oOLI);
			}
		}


		Test.startTest();

		insert lstOLI;	

		Test.stopTest();

		lstOLI = [SELECT Id, All_Lost__c, UnitPrice, Original_Price__c FROM OpportunityLineItem WHERE Id = :lstOLI];

		for (OpportunityLineItem oOLI : lstOLI){
			System.assertEquals(oOLI.All_Lost__c, true);
			System.assertEquals(oOLI.UnitPrice, 0);
			System.assertEquals(oOLI.Original_Price__c, 100);
		}

	}

	@isTest static void test_setPriceBasedOnAllLost_Update_1() {
		
		createTestData();

		Opportunity oOpportunity = [SELECT Id, Name, Pricebook2Id FROM Opportunity WHERE Id = :clsTestData_Opportunity.oMain_Opportunity.Id];
			oOpportunity.Pricebook2Id = clsTestData_Opportunity.oMain_Pricebook.Id;
		update oOpportunity;

		// Create Opportunity Line Items
		List<PricebookEntry> lstPricebookEntry = clsTestData_Opportunity.lstPricebookEntry;

		List<OpportunityLineItem> lstOLI = new List<OpportunityLineItem>();
		
		for (PricebookEntry oPricebookEntry : lstPricebookEntry){
			OpportunityLineItem oOLI = new OpportunityLineItem();
		        oOLI.PricebookEntryId = oPricebookEntry.Id;
		        oOLI.Opportunityid = oOpportunity.Id;
		        oOLI.Quantity = 100;
		        oOLI.UnitPrice = 100;
		        oOLI.All_Lost__c = false;
			lstOLI.add(oOLI);
		}
		insert lstOLI;	


		Test.startTest();

		for (OpportunityLineItem oOLI : lstOLI){
			oOLI.All_Lost__c = true;
		}
		update lstOLI;

		lstOLI = 
			[
				SELECT Id, All_Lost__c, PricebookEntryId, OpportunityId, Quantity, UnitPrice, Original_Price__c 
				FROM OpportunityLineItem 
				WHERE OpportunityId = :oOpportunity.Id
			];

		for (OpportunityLineItem oOLI : lstOLI){
			System.assertEquals(oOLI.All_Lost__c, true);
			System.assertEquals(oOLI.UnitPrice, 0);
			System.assertEquals(oOLI.Original_Price__c, 100);
			
			oOLI.All_Lost__c = false;
		}

		update lstOLI;

		Test.stopTest();

		lstOLI = 
			[
				SELECT Id, All_Lost__c, PricebookEntryId, OpportunityId, Quantity, UnitPrice, Original_Price__c 
				FROM OpportunityLineItem 
				WHERE OpportunityId = :oOpportunity.Id
			];

		for (OpportunityLineItem oOLI : lstOLI){
			System.assertEquals(oOLI.All_Lost__c, false);
			System.assertEquals(oOLI.UnitPrice, oOLI.Original_Price__c);
		}


	}	

	@isTest static void test_deleteOpportunityLineItemSchedule() {
		
		createTestData();

		Opportunity oOpportunity = [SELECT Id, Name, Pricebook2Id FROM Opportunity WHERE Id = :clsTestData_Opportunity.oMain_Opportunity.Id];
			oOpportunity.Pricebook2Id = clsTestData_Opportunity.oMain_Pricebook.Id;
		update oOpportunity;

		// Create Opportunity Line Items
		List<PricebookEntry> lstPricebookEntry = clsTestData_Opportunity.lstPricebookEntry;

		List<OpportunityLineItem> lstOLI = new List<OpportunityLineItem>();
		
		for (PricebookEntry oPricebookEntry : lstPricebookEntry){
			OpportunityLineItem oOLI = new OpportunityLineItem();
		        oOLI.PricebookEntryId = oPricebookEntry.Id;
		        oOLI.Opportunityid = oOpportunity.Id;
		        oOLI.Quantity = 100;
		        oOLI.UnitPrice = 100;
		        oOLI.All_Lost__c = false;
			lstOLI.add(oOLI);
		}
		insert lstOLI;	

		List<OpportunityLineItemSchedule> lstOLIS = new List<OpportunityLineItemSchedule>();
		for (OpportunityLineItem oOLI : lstOLI){
			OpportunityLineItemSchedule oOLIS = new OpportunityLineItemSchedule();
				oOLIS.OpportunityLineItemId = oOLI.Id;
//				oOLIS.Quantity = oOLI.Quantity;
				oOLIS.Revenue = oOLI.UnitPrice;
				oOLIS.ScheduleDate = Date.today().addDays(10);
				oOLIS.Type = 'Revenue';
			lstOLIS.add(oOLIS);
		}

		insert lstOLIS;

		lstOLIS = [SELECT Id FROM OpportunityLineItemSchedule WHERE OpportunityLineItemId in :lstOLI];
		System.assertNotEquals(lstOLIS.size(), 0);

		Test.startTest();

		for (OpportunityLineItem oOLI : lstOLI){
			oOLI.All_Lost__c = true;
		}

		update lstOLI;

		Test.stopTest();

		lstOLIS = [SELECT Id FROM OpportunityLineItemSchedule WHERE OpportunityLineItemId in :lstOLI];
		System.assertEquals(lstOLIS.size(), 0);

	}		


	@isTest static void test_updateOpportunityField_RLS_IHS_Insert(){


		//------------------------------------------------
		// Create Test Data
		//------------------------------------------------
		// Create Master Data
		clsTestData_MasterData.tCompanyCode = 'EUR';
		clsTestData_MasterData.createCompany();
		// Create DIB_Fiscal_Period__c data
		clsTestData_System.createDIBFiscalPeriod();
		// Create Account Data
		clsTestData_Account.createAccount();
		// Create Opportunity Data
		clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'EMEA_IHS_Managed_Services').Id;
		clsTestData_Opportunity.createOpportunity();
		// Create Product Data
		clsTestData_Product.iRecord_Product = 5;
		clsTestData_Product.createProductData_NoBU(false);
			clsTestData_Product.lstProduct[0].Name = 'R1 CV Products';
			clsTestData_Product.lstProduct[1].Name = 'R2 NV Products';
			clsTestData_Product.lstProduct[2].Name = 'R3 SI Products';
			clsTestData_Product.lstProduct[3].Name = 'R5 Third Party Products';
			clsTestData_Product.lstProduct[4].Name = 'R9 Diabetes Products Incremental';
		insert clsTestData_Product.lstProduct;
		//------------------------------------------------

		List<Opportunity> lstOpportunity = [SELECT Id, Amount, IHS_Service_Amount__c FROM Opportunity];
		for (Opportunity oOpportunity : lstOpportunity){

			System.assertEquals(clsUtil.isDecimalNull(oOpportunity.Amount, 0), 0);

		}

		//------------------------------------------------
		// Perform Test
		//------------------------------------------------
		Test.startTest();

		// Create Opportunity Line Item Data
		clsTestData_Opportunity.iRecord_OpportunityLineItem = 5;
		clsTestData_Opportunity.createOpportunityLineItem(true);
		for (OpportunityLineItem oOLI : clsTestData_Opportunity.lstOpportunityLineItem){
			oOLI.Quantity = 5;
			oOLI.UnitPrice = 200;
		}
		update clsTestData_Opportunity.lstOpportunityLineItem;

		Test.stopTest();
		//------------------------------------------------


		//------------------------------------------------
		// Validate Test Result
		//------------------------------------------------
		List<OpportunityLineItem> lstOLI = 
			[
				SELECT Id, Quantity, UnitPrice, TotalPrice, Opportunity.Amount, Opportunity.IHS_Service_Amount__c
				FROM OpportunityLineItem 
				WHERE OpportunityId = :clsTestData_Opportunity.oMain_Opportunity.Id
			];

		System.assertEquals(lstOLI.size(), 5);
		for (OpportunityLineItem oOLI : lstOLI){

			System.assertEquals(oOLI.Opportunity.Amount, 5000);
			System.assertEquals(oOLI.Opportunity.IHS_Service_Amount__c, 1000);

			System.assertEquals(oOLI.Quantity, 5);
			System.assertEquals(oOLI.UnitPrice, 200);

		}
		//------------------------------------------------

	}


	@isTest static void test_updateOpportunityField_RLS_IHS_Delete(){

		//------------------------------------------------
		// Create Test Data
		//------------------------------------------------
		// Create Master Data
		clsTestData_MasterData.tCompanyCode = 'EUR';
		clsTestData_MasterData.createCompany();
		// Create DIB_Fiscal_Period__c data
		clsTestData_System.createDIBFiscalPeriod();
		// Create Account Data
		clsTestData_Account.createAccount();
		// Create Opportunity Data
		clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'EMEA_IHS_Managed_Services').Id;
		clsTestData_Opportunity.createOpportunity();
		// Create Product Data
		clsTestData_Product.iRecord_Product = 5;
		clsTestData_Product.createProductData_NoBU(false);
			clsTestData_Product.lstProduct[0].Name = 'R1 CV Products';
			clsTestData_Product.lstProduct[1].Name = 'R2 NV Products';
			clsTestData_Product.lstProduct[2].Name = 'R3 SI Products';
			clsTestData_Product.lstProduct[3].Name = 'R5 Third Party Products';
			clsTestData_Product.lstProduct[4].Name = 'R9 Diabetes Products Incremental';
		insert clsTestData_Product.lstProduct;
		// Create Opportunity Line Item Data
		clsTestData_Opportunity.iRecord_OpportunityLineItem = 5;
		clsTestData_Opportunity.createOpportunityLineItem(true);
		for (OpportunityLineItem oOLI : clsTestData_Opportunity.lstOpportunityLineItem){
			oOLI.Quantity = 5;
			oOLI.UnitPrice = 200;
		}
		update clsTestData_Opportunity.lstOpportunityLineItem;
		//------------------------------------------------


		List<OpportunityLineItem> lstOLI = 
			[
				SELECT Id, Product2.Name, Quantity, Opportunity.Amount, Opportunity.IHS_Service_Amount__c
				FROM OpportunityLineItem 
				WHERE OpportunityId = :clsTestData_Opportunity.oMain_Opportunity.Id
			];

		for (OpportunityLineItem oOLI : lstOLI){

			System.assertEquals(oOLI.Opportunity.Amount, 5000);
			System.assertEquals(oOLI.Opportunity.IHS_Service_Amount__c, 1000);

		}


		//------------------------------------------------
		//  Test
		//------------------------------------------------
		Test.startTest();

		List<OpportunityLineItem> lstOLI_Delete = new List<OpportunityLineItem>();
		List<OpportunityLineItem> lstOLI_Delete_1 = new List<OpportunityLineItem>();
		List<OpportunityLineItem> lstOLI_Delete_2 = new List<OpportunityLineItem>();
		for (OpportunityLineItem oOLI : lstOLI){

			if ( (oOLI.Product2.Name == 'R1 CV Products') || (oOLI.Product2.Name == 'R2 NV Products') || (oOLI.Product2.Name == 'R3 SI Products') || (oOLI.Product2.Name == 'R9 Diabetes Products Incremental') ){
				if (lstOLI_Delete_1.size() == 0){
					lstOLI_Delete_1.add(oOLI);
				}
			}else{
				if (lstOLI_Delete_2.size() == 0){
					lstOLI_Delete_2.add(oOLI);
				}
			}

		}
		System.assertEquals(lstOLI_Delete_1.size(), lstOLI_Delete_2.size(), 1);
		lstOLI_Delete.addAll(lstOLI_Delete_1);
		lstOLI_Delete.addAll(lstOLI_Delete_2);
		delete lstOLI_Delete;

		Test.stopTest();
		//------------------------------------------------


		//------------------------------------------------
		// Validate Test Result
		//------------------------------------------------
		lstOLI = 
			[
				SELECT Id, Quantity, UnitPrice, Opportunity.Amount, Opportunity.IHS_Service_Amount__c
				FROM OpportunityLineItem 
				WHERE OpportunityId = :clsTestData_Opportunity.oMain_Opportunity.Id
			];

		System.assertEquals(lstOLI.size(), 3);
		for (OpportunityLineItem oOLI : lstOLI){

			System.assertEquals(oOLI.Opportunity.Amount, 3000);
			System.assertEquals(oOLI.Opportunity.IHS_Service_Amount__c, 0);

			System.assertEquals(oOLI.Quantity, 5);
			System.assertEquals(oOLI.UnitPrice, 200);

		}
		//------------------------------------------------

	}	


	@isTest static void test_updateOpportunityField_RLS_IHS_Update(){

		//------------------------------------------------
		// Create Test Data
		//------------------------------------------------
		// Create Master Data
		clsTestData_MasterData.tCompanyCode = 'EUR';
		clsTestData_MasterData.createCompany();
		// Create DIB_Fiscal_Period__c data
		clsTestData_System.createDIBFiscalPeriod();
		// Create Account Data
		clsTestData_Account.createAccount();
		// Create Opportunity Data
		clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'EMEA_IHS_Managed_Services').Id;
		clsTestData_Opportunity.createOpportunity();
		// Create Product Data
		clsTestData_Product.iRecord_Product = 5;
		clsTestData_Product.createProductData_NoBU(false);
			clsTestData_Product.lstProduct[0].Name = 'R1 CV Products';
			clsTestData_Product.lstProduct[1].Name = 'R2 NV Products';
			clsTestData_Product.lstProduct[2].Name = 'R3 SI Products';
			clsTestData_Product.lstProduct[4].Name = 'R9 Diabetes Products Incremental';
			clsTestData_Product.lstProduct[3].Name = 'R5 Third Party Products';
		insert clsTestData_Product.lstProduct;
		// Create Opportunity Line Item Data
		clsTestData_Opportunity.iRecord_OpportunityLineItem = 5;
		clsTestData_Opportunity.createOpportunityLineItem(true);
		for (OpportunityLineItem oOLI : clsTestData_Opportunity.lstOpportunityLineItem){
			oOLI.Quantity = 5;
			oOLI.UnitPrice = 200;
		}
		update clsTestData_Opportunity.lstOpportunityLineItem;
		//------------------------------------------------

		List<OpportunityLineItem> lstOLI = 
			[
				SELECT Id, Product2.Name, Quantity, Opportunity.Amount, Opportunity.IHS_Service_Amount__c
				FROM OpportunityLineItem 
				WHERE OpportunityId = :clsTestData_Opportunity.oMain_Opportunity.Id
			];

		for (OpportunityLineItem oOLI : lstOLI){

			System.assertEquals(oOLI.Opportunity.Amount, 5000);
			System.assertEquals(oOLI.Opportunity.IHS_Service_Amount__c, 1000);

		}


		//------------------------------------------------
		// Perform Test
		//------------------------------------------------
		Test.startTest();

		List<OpportunityLineItem> lstOLI_Update = new List<OpportunityLineItem>();
		List<OpportunityLineItem> lstOLI_Update_1 = new List<OpportunityLineItem>();
		List<OpportunityLineItem> lstOLI_Update_2 = new List<OpportunityLineItem>();
		for (OpportunityLineItem oOLI : lstOLI){

			if ( (oOLI.Product2.Name == 'R1 CV Products') || (oOLI.Product2.Name == 'R2 NV Products') || (oOLI.Product2.Name == 'R3 SI Products') || (oOLI.Product2.Name == 'R9 Diabetes Products Incremental') ){
				if (lstOLI_Update_1.size() == 0){
					lstOLI_Update_1.add(oOLI);
				}
			}else{
				if (lstOLI_Update_2.size() == 0){
					lstOLI_Update_2.add(oOLI);
				}
			}

		}

		System.assertEquals(lstOLI_Update_1.size(), 1);
		System.assertEquals(lstOLI_Update_2.size(), 1);
		lstOLI_Update.addAll(lstOLI_Update_1);
		lstOLI_Update.addAll(lstOLI_Update_2);
		update lstOLI_Update;

		Test.stopTest();
		//------------------------------------------------


		//------------------------------------------------
		// Validate Test Result
		//------------------------------------------------
		lstOLI = 
			[
				SELECT Id, Product2.Name, Quantity, Opportunity.Amount, Opportunity.IHS_Service_Amount__c
				FROM OpportunityLineItem 
				WHERE OpportunityId = :clsTestData_Opportunity.oMain_Opportunity.Id
			];

		System.assertEquals(lstOLI.size(), 5);
		Integer iCounter = 0;
		for (OpportunityLineItem oOLI : lstOLI){

			System.assertEquals(oOLI.Quantity, 5);

			System.assertEquals(oOLI.Opportunity.Amount, 5000);
			System.assertEquals(oOLI.Opportunity.IHS_Service_Amount__c, 1000);

			iCounter++;

		}
		//------------------------------------------------

	}	


	@isTest static void test_setOpportunity_BUG_BU_SBU_INSERT(){


		//------------------------------------------------
		// Create Test Data
		//------------------------------------------------
		// Create Master Data
		clsTestData_MasterData.tCompanyCode = 'EUR';
		clsTestData_MasterData.createCompany();
		clsTestData_MasterData.tBusinessUnitGroupName = 'Restorative';
		clsTestData_MasterData.createBusinessUnit();
		clsTestData_MasterData.createSubBusinessUnit();

		// Create DIB_Fiscal_Period__c data
//		clsTestData_System.createDIBFiscalPeriod();
		// Create Account Data
		clsTestData_Account.createAccount();
		// Create Opportunity Data
		clsTestData_Opportunity.idRecordType_Opportunity = clsUtil.getRecordTypeByDevName('Opportunity', 'Capital_Opportunity').Id;
		clsTestData_Opportunity.createOpportunity();
		// Create Product Data
		clsTestData_Product.createProductGroup();
		clsTestData_Product.iRecord_Product = 2;
		clsTestData_Product.createProduct(false);
			clsTestData_Product.lstProduct[0].Product_Group__c = clsTestData_Product.oMain_ProductGroup.Id;
			clsTestData_Product.lstProduct[1].Business_Unit_ID__c = null;
			clsTestData_Product.lstProduct[1].Business_Unit_Group__c = clsTestData_MasterData.oMain_BusinessUnitGroup.Id;
		insert clsTestData_Product.lstProduct;

		List<Product2> lstProduct = [SELECT Id, Business_Unit_Group__c, Business_Unit_ID__c, Product_Group__r.Therapy_ID__r.Name, Sub_Business_Unit__c FROM Product2];
		System.debug('**BC** lstProduct (' + lstProduct.size() + ') : ' + lstProduct);

		// Create PricebookEntry
		clsTestData_Opportunity.createPricebookEntry(true);

		//------------------------------------------------

		List<Opportunity> lstOpportunity = [SELECT Id, Business_Unit_Group__c, Business_Unit_msp__c, Sub_Business_Unit__c, Therapies__c FROM Opportunity];
		for (Opportunity oOpportunity : lstOpportunity){
			System.assert(String.isBlank(oOpportunity.Business_Unit_Group__c));
			System.assert(String.isBlank(oOpportunity.Business_Unit_msp__c));
			System.assert(String.isBlank(oOpportunity.Sub_Business_Unit__c));
			System.assert(String.isBlank(oOpportunity.Therapies__c));
		}

		//------------------------------------------------
		// Perform Test
		//------------------------------------------------
		Test.startTest();

		// Create Opportunity Line Item Data
		clsTestData_Opportunity.iRecord_OpportunityLineItem = 2;
		clsTestData_Opportunity.createOpportunityLineItem(true);

		Test.stopTest();
		//------------------------------------------------


		//------------------------------------------------
		// Validate Test Result
		//------------------------------------------------
		lstOpportunity = [SELECT Id, Business_Unit_Group__c, Business_Unit_msp__c, Sub_Business_Unit__c, Therapies__c FROM Opportunity];

		System.assertEquals(lstOpportunity.size(), 1);
		for (Opportunity oOpportunity : lstOpportunity){

			System.assert(!String.isBlank(oOpportunity.Business_Unit_Group__c));
			System.assert(!String.isBlank(oOpportunity.Business_Unit_msp__c));
			System.assert(!String.isBlank(oOpportunity.Sub_Business_Unit__c));
			System.assert(!String.isBlank(oOpportunity.Therapies__c));

		}
		//------------------------------------------------

	}
    

}
//--------------------------------------------------------------------------------------------------------------------