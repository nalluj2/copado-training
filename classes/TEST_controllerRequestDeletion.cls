/**
 * Creation Date :  20090213
 * Description :    Test Coverage of the controller 'Request Deletion' (Button in Contact Object).
 *                  (To facilitate the development of robust, error-free code, Apex Code requires the creation and execution of unit tests.
 *                  Unit tests are comprised of test methods and classes that verify whether a particular piece of code is working properly.)
 * Author :         ABSI - MC
 */
@isTest
private class TEST_controllerRequestDeletion {

    static TestMethod void testController_Delete(){
        
        Account oAccount = new Account();
        oAccount.Name = 'Test Account' ; 
        insert oAccount;       
                
        Contact oContact = new Contact();
            oContact.FirstName = 'Test';
            oContact.LastName = 'Contact' ;  
            oContact.AccountId = oAccount.Id ;             
            oContact.Contact_Department__c = 'Diabetes Adult'; 
            oContact.Contact_Primary_Specialty__c = 'ENT';
            oContact.Affiliation_To_Account__c = 'Employee';
            oContact.Primary_Job_Title_vs__c = 'Manager';
            oContact.Contact_Gender__c = 'Male';                   
        insert oContact;      
        
        Test.startTest();
                       
        ControllerRequestDeletion oControllerRequestDeletion = new ControllerRequestDeletion(new ApexPages.StandardController(oContact));
                
        oControllerRequestDeletion.deletionRequest.Contact_Deletion_Action__c = 'Delete';
                
        oControllerRequestDeletion.Send();
        
        System.assert(oControllerRequestDeletion.canCloseWindow == false);
                
        oControllerRequestDeletion.deletionRequest.Contact_Deletion_Reason__c = 'Request deletion'; 
        
        oControllerRequestDeletion.Send();
		
		System.assert(oControllerRequestDeletion.canCloseWindow == true);
                 
        List<Contact_Deletion_Request__c> deletionRequests = [Select Id from Contact_Deletion_Request__c where Contact__c = :oContact.Id];       
        
        System.assert(deletionRequests.size() == 1);
    } 

    static TestMethod void testController_Merge(){
        
        Account oAccount1 = new Account();
            oAccount1.Name = 'Test Account 1' ;
		
		Account oAccount2 = new Account();
            oAccount2.Name = 'Test Account 2' ;
                         
        insert new List<Account>{oAccount1, oAccount2};       
                
        Contact oContact1 = new Contact();
            oContact1.FirstName = 'Test';
            oContact1.LastName = 'Contact 1' ;  
            oContact1.AccountId = oAccount1.Id ;             
            oContact1.Contact_Department__c = 'Diabetes Adult'; 
            oContact1.Contact_Primary_Specialty__c = 'ENT';
            oContact1.Affiliation_To_Account__c = 'Employee';
            oContact1.Primary_Job_Title_vs__c = 'Manager';
            oContact1.Contact_Gender__c = 'Male';  
               
        Contact oContact2 = new Contact();
        	oContact2.FirstName = 'Test';
            oContact2.LastName = 'Contact 2' ;  
            oContact2.AccountId = oAccount2.Id ;             
            oContact2.Contact_Department__c = 'Diabetes Adult'; 
            oContact2.Contact_Primary_Specialty__c = 'ENT';
            oContact2.Affiliation_To_Account__c = 'Employee';
            oContact2.Primary_Job_Title_vs__c = 'Manager';
            oContact2.Contact_Gender__c = 'Male';              
            
        insert new List<Contact>{oContact1, oContact2};      
		
		Test.startTest();
		                         
        ControllerRequestDeletion oControllerRequestDeletion = new ControllerRequestDeletion(new ApexPages.StandardController(oContact1));
                       
        oControllerRequestDeletion.deletionRequest.Contact_Deletion_Action__c = 'Merge';
        
        oControllerRequestDeletion.send();        
        System.assert(oControllerRequestDeletion.canCloseWindow == false);
    
        oControllerRequestDeletion.deletionRequest.Contact_to_Merge_with__c = oContact2.Id;
        oControllerRequestDeletion.contactMergeWithChanged();
        
        System.assert(oControllerRequestDeletion.accountToMergeWith.Id == oAccount2.Id);
        
        oControllerRequestDeletion.send();        
        System.assert(oControllerRequestDeletion.canCloseWindow == false);
        
        List<SelectOption> lstSO = oControllerRequestDeletion.getAccountNames();
		System.assert(lstSO.size() == 2);
		
        oControllerRequestDeletion.deletionRequest.Prim_Acc_name_of_Contact_to_merge_with__c = lstSO[0].getValue();
                
        oControllerRequestDeletion.send();        
        System.assert(oControllerRequestDeletion.canCloseWindow == true);
        
        List<Contact_Deletion_Request__c> deletionRequests = [Select Id from Contact_Deletion_Request__c where Contact__c = :oContact1.Id];       
        
        System.assert(deletionRequests.size() == 1);
   } 
}