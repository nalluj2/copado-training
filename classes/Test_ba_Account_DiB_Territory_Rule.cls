/*
* Due to Salesforce limitations on AccountHistory object for UnitTests a proper test execution cannot be implemented. Therefore only line coverage test is implemented.
*/
@isTest
private class Test_ba_Account_DiB_Territory_Rule {
	
	private static testmethod void testBatch(){
		
		Account acc = new Account(Name = 'Unit Test Account');
		insert acc;
								        	
        Test.startTest();        
        
        CalloutMock mockImpl = new CalloutMock(false);		
		Test.setMock(HttpCalloutMock.class, mockImpl);
        
        ba_Account_DiB_Territory_Rule_Trigger batch = new ba_Account_DiB_Territory_Rule_Trigger();
        
        List<AccountHistory> recordList = new List<AccountHistory>{new AccountHistory(AccountId = acc.Id)};
        
        batch.execute(null, recordList);
        	
        Database.executeBatch(batch, 200);
        	
        Test.stopTest();        
	}
	
	private static testmethod void testBatch_Error(){
		
		User oUser_SystemAdmin = clsTestData_User.createUser_SystemAdministrator('tadm001', false);
		Account acc;
		Group adminsGroup;

		System.runAs(oUser_SystemAdmin){		
			acc = new Account(Name = 'Unit Test Account');
			insert acc;
		
			adminsGroup = [Select Id, (Select Id from GroupMembers) from Group where Name = 'Synchronization Service Admins'];

		}

		if (adminsGroup.GroupMembers.size() == 0){
			
			GroupMember adminMember = new GroupMember();
			adminMember.UserOrGroupId = oUser_SystemAdmin.iD;
			adminMember.GroupId = adminsGroup.Id;
			insert adminMember;

		}

								        	
        Test.startTest();        
        
		System.runAs(oUser_SystemAdmin){		
			CalloutMock mockImpl = new CalloutMock(true);		
			Test.setMock(HttpCalloutMock.class, mockImpl);
        
			ba_Account_DiB_Territory_Rule_Trigger batch = new ba_Account_DiB_Territory_Rule_Trigger();
        
			List<AccountHistory> recordList = new List<AccountHistory>{new AccountHistory(AccountId = acc.Id)};
        
			batch.execute(null, recordList);
        
			System.assert(batch.errors.size() == 1);
        
			batch.finish(null);

		}

	}
	
	public class CalloutMock implements HttpCalloutMock {
		
		private Boolean returnError;
		
		public CalloutMock(Boolean retError){
			
			returnError = retError;
		}
		
        public HTTPResponse respond(HTTPRequest req) {
            
            HttpResponse resp = new HttpResponse();
            resp.setStatusCode(200);
            resp.setStatus('Complete');
            
            String respBody; 
                        	
        	if(returnError == false){
        		
        		respBody =  '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns="urn:enterprise.soap.sforce.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' +
							   '<soapenv:Body>' +
							      '<updateResponse>' +
							         '<result>' +
							            '<id>recordId</id>' +
							            '<success>true</success>' +
							         '</result>' +
							      '</updateResponse>' +
							   '</soapenv:Body>' +
							'</soapenv:Envelope>';
        	}else{
        		
        		respBody =  '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns="urn:enterprise.soap.sforce.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' +
							   '<soapenv:Body>' +
							      '<updateResponse>' +
							         '<result>' +
							            '<id>recordId</id>' +
							            '<success>false</success>' +
							            '<errors>' +								               
							               '<message>Unit Test forced error</message>' +
							               '<statusCode>ERROR_CODE</statusCode>' +
							            '</errors>' +
							         '</result>' +
							      '</updateResponse>' +
							   '</soapenv:Body>' +
							'</soapenv:Envelope>';
        	}
            
            resp.setBody(respBody);
            
            return resp;
        }
	}   
}