@isTest
private class TEST_ctrl_Support_Requests {
	
	private static String tVisualForcePage;

	private static List<Create_User_Request__c> lstUserRequest = new List<Create_User_Request__c>();

	private static User oUser_SystemAdministrator;

	private static void createTestUser(){

		List<User> lstUser = new List<User>();
		oUser_SystemAdministrator = clsTestData_User.createUser_SystemAdministrator('tadm001', true);

	}

	private static void createTestData_Settings(){

		List<Support_application__c> lstSupportApplication = new List<Support_application__c>();
		Support_application__c oSupportApplication1 = new Support_application__c();
			oSupportApplication1.Name = 'Support Application Name';
			oSupportApplication1.Emails_of_approvers__c = 'mock@medtronic.com' ;
		lstSupportApplication.add(oSupportApplication1);

		Support_application__c oSupportApplication2 = new Support_application__c();
			oSupportApplication2.Name = 'Data Load Request';
			oSupportApplication2.Emails_of_approvers__c = 'mock@medtronic.com' ;
		lstSupportApplication.add(oSupportApplication2);
		insert lstSupportApplication;

	}

	private static void createTestData_UserRequest_Pending(String tApplication, String tType, Boolean bInsert){

        // Create Pending Request
        List<Create_User_Request__c> lstUserRequest_Pending = new List<Create_User_Request__c>();
        Create_User_Request__c oUserRequest_Pending1 = new Create_User_Request__c();
	        oUserRequest_Pending1.Firstname__c = 'test';
	        oUserRequest_Pending1.Lastname__c = 'user';
	        oUserRequest_Pending1.Email__c = 'test.user@medtronic.com';
	        oUserRequest_Pending1.Status__c ='Submitted for Approval';
	        oUserRequest_Pending1.Application__c = tApplication;
	        oUserRequest_Pending1.Request_Type__c = tType;
	        oUserRequest_Pending1.Requestor_Email__c = UserInfo.getUserEmail();
	        oUserRequest_Pending1.Manager_Email__c = UserInfo.getUserEmail();
        lstUserRequest_Pending.add(oUserRequest_Pending1);

        Create_User_Request__c oUserRequest_Pending2 = new Create_User_Request__c();
	        oUserRequest_Pending2.Firstname__c = 'test';
	        oUserRequest_Pending2.Lastname__c = 'user';
	        oUserRequest_Pending2.Email__c = 'test.user@medtronic.com';
	        oUserRequest_Pending1.Status__c ='Submitted for Approval';
	        oUserRequest_Pending2.Application__c = 'Data Load Request';
	        oUserRequest_Pending2.Request_Type__c = tType;
	        oUserRequest_Pending2.Requestor_Email__c = UserInfo.getUserEmail();
	        oUserRequest_Pending2.Manager_Email__c = UserInfo.getUserEmail();
        lstUserRequest_Pending.add(oUserRequest_Pending2);

        if (bInsert){
        	insert lstUserRequest_Pending;
        }		

	}

	private static Create_User_Request__c createTestData_UserRequest(String tApplication, String tType, Boolean bInsert){

        Create_User_Request__c oUserRequest = new Create_User_Request__c();          
	        oUserRequest.Firstname__c = 'test';
	        oUserRequest.Lastname__c = 'user';
	        oUserRequest.Email__c = 'test.user@medtronic.com';
	        oUserRequest.Status__c = 'New';
	        oUserRequest.Application__c = tApplication;
	        oUserRequest.Request_Type__c = tType;
	        oUserRequest.Requestor_Email__c = UserInfo.getUserEmail();  

		if (tType == 'Generic Service'){
	        oUserRequest.Service_Request_Type__c = 'Other';  
		}else if (tType == 'Change Request'){
			oUserRequest.CR_Type__c = 'Other';
			oUserRequest.Priority__c = 'Medium';
		}else if (tType == 'Data Load'){
			oUserRequest.Data_Type__c = 'Accounts';
		}else if (tType == 'Create User'){
	        oUserRequest.Created_User__c = UserInfo.getUserId(); 
			oUserRequest.Company_Code__c = 'EUR';
			oUserRequest.User_Business_Unit_vs__c = 'CRHF';
	        oUserRequest.Jobtitle_vs__c = 'BU Back Office';
		}else if (tType == 'Enable Mobile App'){
		}else if (tType == 'Change User Setup'){
		}else if (tType == 'Re-activate User'){
		}else if (tType == 'De-activate User'){
			oUserRequest.Date_of_deactivation__c = Datetime.now();
		}

		if (tApplication == 'Salesforce.com'){

			tVisualForcePage = 'Support_Other_Request';

		}else if (tApplication == 'BOT'){

			tVisualForcePage = 'Support_BOT_GeneralRequest';

			if (tType == 'Generic Service'){
		        oUserRequest.Description_Long__c = 'TEST';   
			}

		}else if (tApplication == 'CVent'){

			tVisualForcePage = 'Support_CVent_Generic_Service';

		}else if (tApplication == 'BI'){

			tVisualForcePage = 'Support_BI_GenericService';

		}else if (tApplication == 'Crossworld'){

			tVisualForcePage = 'Support_XW_Generic_Service';

		}

		if (bInsert){
	        insert oUserRequest;
		}

		return oUserRequest;

	}


	@isTest static void test_Salesforce(){

		if (oUser_SystemAdministrator == null) createTestUser();

		Create_User_Request__c oUserRequest;

		System.runAs(oUser_SystemAdministrator){

			createTestData_Settings();

			createTestData_UserRequest_Pending('Salesforce.com', 'Data Load Request', true);
			
			oUserRequest = createTestData_UserRequest('Salesforce.com', 'Generic Service', true);

			lstUserRequest = new List<Create_User_Request__c>();
				lstUserRequest.add(createTestData_UserRequest('Salesforce.com', 'Generic Service', false));
				lstUserRequest.add(createTestData_UserRequest('Salesforce.com', 'Data Load', false));
				lstUserRequest.add(createTestData_UserRequest('Salesforce.com', 'Change Request', false));
				lstUserRequest.add(createTestData_UserRequest('Salesforce.com', 'Create User', false));
				lstUserRequest.add(createTestData_UserRequest('Salesforce.com', 'Enable Mobile App', false));
				lstUserRequest.add(createTestData_UserRequest('Salesforce.com', 'Re-activate User', false));
				lstUserRequest.add(createTestData_UserRequest('Salesforce.com', 'De-activate User', false));
				lstUserRequest.add(createTestData_UserRequest('Salesforce.com', 'Change User Setup', false));
			insert lstUserRequest;

		}

		Test.startTest();

		System.runAs(oUser_SystemAdministrator){

			testctrl_Support_Requests(oUserRequest);

		}

		Test.stopTest();

	}

	@isTest static void test_CVent(){

		if (oUser_SystemAdministrator == null) createTestUser();

		Create_User_Request__c oUserRequest;

		System.runAs(oUser_SystemAdministrator){

			createTestData_Settings();

			createTestData_UserRequest_Pending('CVent.com', 'Data Load Request', true);

			oUserRequest = createTestData_UserRequest('CVent', 'Generic Service', true);

			lstUserRequest = new List<Create_User_Request__c>();
				lstUserRequest.add(createTestData_UserRequest('CVent', 'Generic Service', false));
//				lstUserRequest.add(createTestData_UserRequest('CVent', 'Data Load', false));
				lstUserRequest.add(createTestData_UserRequest('CVent', 'Change Request', false));
//				lstUserRequest.add(createTestData_UserRequest('CVent', 'Create User', false));
//				lstUserRequest.add(createTestData_UserRequest('CVent', 'Enable Mobile App', false));
//				lstUserRequest.add(createTestData_UserRequest('CVent', 'Re-activate User', false));
//				lstUserRequest.add(createTestData_UserRequest('CVent', 'De-activate User', false));
//				lstUserRequest.add(createTestData_UserRequest('CVent', 'Change User Setup', false));
			insert lstUserRequest;

		}

		Test.startTest();

		System.runAs(oUser_SystemAdministrator){

			testctrl_Support_Requests(oUserRequest);

		}

		Test.stopTest();

	}


	@isTest static void test_BOT(){

		if (oUser_SystemAdministrator == null) createTestUser();

		Create_User_Request__c oUserRequest;

		System.runAs(oUser_SystemAdministrator){

			createTestData_Settings();

			createTestData_UserRequest_Pending('BOT', 'Data Load Request', true);

			oUserRequest = createTestData_UserRequest('BOT', 'Generic Service', true);

			lstUserRequest = new List<Create_User_Request__c>();
				lstUserRequest.add(createTestData_UserRequest('BOT', 'Generic Service', false));
				lstUserRequest.add(createTestData_UserRequest('BOT', 'Data Load', false));
				lstUserRequest.add(createTestData_UserRequest('BOT', 'Change Request', false));
				lstUserRequest.add(createTestData_UserRequest('BOT', 'Create User', false));
				lstUserRequest.add(createTestData_UserRequest('BOT', 'Enable Mobile App', false));
				lstUserRequest.add(createTestData_UserRequest('BOT', 'Re-activate User', false));
				lstUserRequest.add(createTestData_UserRequest('BOT', 'De-activate User', false));
				lstUserRequest.add(createTestData_UserRequest('BOT', 'Change User Setup', false));
			insert lstUserRequest;

		}

		Test.startTest();

		System.runAs(oUser_SystemAdministrator){

			testctrl_Support_Requests(oUserRequest);

		}

		Test.stopTest();

	}


	@isTest static void test_BI(){

		if (oUser_SystemAdministrator == null) createTestUser();

		Create_User_Request__c oUserRequest;

		System.runAs(oUser_SystemAdministrator){

			createTestData_Settings();

			createTestData_UserRequest_Pending('BI', 'Data Load Request', true);

			oUserRequest = createTestData_UserRequest('BI', 'Generic Service', true);

			lstUserRequest = new List<Create_User_Request__c>();
				lstUserRequest.add(createTestData_UserRequest('BI', 'Generic Service', false));
				lstUserRequest.add(createTestData_UserRequest('BI', 'Data Load', false));
				lstUserRequest.add(createTestData_UserRequest('BI', 'Change Request', false));
				lstUserRequest.add(createTestData_UserRequest('BI', 'Create User', false));
				lstUserRequest.add(createTestData_UserRequest('BI', 'Enable Mobile App', false));
				lstUserRequest.add(createTestData_UserRequest('BI', 'Re-activate User', false));
				lstUserRequest.add(createTestData_UserRequest('BI', 'De-activate User', false));
				lstUserRequest.add(createTestData_UserRequest('BI', 'Change User Setup', false));
			insert lstUserRequest;

		}

		Test.startTest();

		System.runAs(oUser_SystemAdministrator){

			testctrl_Support_Requests(oUserRequest);

		}

		Test.stopTest();

	}

	@isTest static void test_Crossworld(){

		if (oUser_SystemAdministrator == null) createTestUser();

		Create_User_Request__c oUserRequest;

		System.runAs(oUser_SystemAdministrator){

			createTestData_Settings();

			createTestData_UserRequest_Pending('Crossworld', 'Data Load Request', true);

			oUserRequest = createTestData_UserRequest('Crossworld', 'Generic Service', true);

			lstUserRequest = new List<Create_User_Request__c>();
				lstUserRequest.add(createTestData_UserRequest('Crossworld', 'Generic Service', false));
				lstUserRequest.add(createTestData_UserRequest('Crossworld', 'Data Load', false));
				lstUserRequest.add(createTestData_UserRequest('Crossworld', 'Change Request', false));
				lstUserRequest.add(createTestData_UserRequest('Crossworld', 'Create User', false));
				lstUserRequest.add(createTestData_UserRequest('Crossworld', 'Enable Mobile App', false));
				lstUserRequest.add(createTestData_UserRequest('Crossworld', 'Re-activate User', false));
				lstUserRequest.add(createTestData_UserRequest('Crossworld', 'De-activate User', false));
				lstUserRequest.add(createTestData_UserRequest('Crossworld', 'Change User Setup', false));
			insert lstUserRequest;

		}

		Test.startTest();

		System.runAs(oUser_SystemAdministrator){

			testctrl_Support_Requests(oUserRequest);

		}

		Test.stopTest();

	}


	private static void testctrl_Support_Requests(Create_User_Request__c oUserRequest) {

		Id idRequest;
		Id idTest;
		String tTest;
		Boolean bTest;
		Date dTest;
		Integer iTest;

        PageReference oPageReference = new PageReference('/apex/' + tVisualForcePage + '?requestId=' + oUserRequest.Id);
        Test.setCurrentPage(oPageReference);

		ctrl_Support_Requests oCTRL = new ctrl_Support_Requests();
        oCTRL.session.username = 'unittest';
        oCTRL.session.password = 'testpassword';

		ws_adUserServiceMockImpl mockup = new ws_adUserServiceMockImpl();
		mockup.forceLoginFail = true;		
		Test.setMock(WebServiceMock.class, mockup);
        oCTRL.doLogin();
        System.assert(oCTRL.userLoggedIn==false);           

		mockup = new ws_adUserServiceMockImpl();
		Test.setMock(WebServiceMock.class, mockup);
        oCTRL.doLogin();
        System.assert(oCTRL.userLoggedIn==true);           

	    oCTRL.loadPendingRequests();
	    oCTRL.loadOpenRequests();

		//------------------------------------------------------------------------------------
		//------------------------------------------------------------------------------------
    	bTest = oCTRL.userLoggedIn;
    	tTest = oCTRL.selectedItem;
        
		List<ctrl_Support_Requests.Request> lstRequest = new List<ctrl_Support_Requests.Request>();
		List<ctrl_Support_Requests.Request> lstRequest_Pending = new List<ctrl_Support_Requests.Request>();
		//oCTRL.requests;
    
    	// HELPERS            
    	bl_Support_Create_User_SFDC oHelper_SFDC1 = oCTRL.helper_sfdcNewUser;
	    bl_Support_Manage_User_SFDC oHelper_SFDC2 = oCTRL.helper_sfdcManageUSer;
	    bl_Support_Requests_SFDC oHelper_SFDC3 = oCTRL.helper_sfdcRequests;
	    bl_Support_CVENT_Requests oHelper_CVent = oCTRL.helper_cventRequests;
	    bl_SupportRequest_BI oHelper_BI = oCTRL.helper_Request_BI;
	    bl_SupportRequest_XW oHelper_XW = oCTRL.helper_Request_XW;
	    bl_SupportRequest_BOT oHelper_BOT = oCTRL.helper_Request_BOT;

	    oCTRL.fromDate = Date.today().addDays(-10);
	    oCTRL.toDate = Date.today().addDays(+10);
	    oCTRL.loadAllRequests();

		lstRequest = oCTRL.requests;
		lstRequest_Pending = oCTRL.pendingRequests;

		dTest = oCTRL.fromDate;
		dTest = oCTRL.toDate;

	    tTest = oCTRL.openReqSortColumn;
	    tTest = oCTRL.openReqSortDir;
	    tTest = oCTRL.selectedOpenReqColumn;
	    tTest = oCTRL.pendingReqSortColumn;
	    tTest = oCTRL.pendingReqSortDir;
	    tTest = oCTRL.selectedPendingReqColumn;

	    oCTRL.sortOpenRequests();
	    oCTRL.sortPendingRequests();

	    oCTRL.seeAllRequests();

	    oCTRL.selectedItem = oUserRequest.Id;
	    oCTRL.viewRequest();

		//------------------------------------------------------------------------------------


		//------------------------------------------------------------------------------------
		// Session Related Logic
		//------------------------------------------------------------------------------------
        oCTRL.session.request = oUserRequest;

		tTest = oCTRL.session.username;
		tTest = oCTRL.session.password;
		ws_adUserService.userinfo oUserInfo = oCTRL.session.uInfo;

        Set<String> mapTest = oCTRL.session.managedApplications;
        idTest = oCTRL.session.id_Company;
        
        bTest = oCTRL.session.changeRequestAllowed;
        bTest = oCTRL.session.isAppApprover;
          
        Create_User_Request__c oCreateUserRequest = oCTRL.session.request;
                
        idRequest = oCTRL.session.inputRequestId;
        tTest = oCTRL.session.attachmentId;
        
        // Request Comments 
        User_Request_Comment__c oUserRequestComment = oCTRL.session.comment;
        	oUserRequestComment = new User_Request_Comment__c();
        	oUserRequestComment.Comment__c = 'Test Comment';
		oCTRL.session.comment = oUserRequestComment;
        List<User_Request_Comment__c> lstUserRequestComment = oCTRL.session.comments;
              
        // Request Requests 
        oCTRL.session.inputFile = new Attachment();
        	oCTRL.session.inputFile.Name = 'TEST File 1';
        	oCTRL.session.inputFile.Body = Blob.valueOf('TEST BODY 1');
        oCTRL.session.inputFile2 = new Attachment();
        	oCTRL.session.inputFile2.Name = 'TEST File 2';
        	oCTRL.session.inputFile2.Body = Blob.valueOf('TEST BODY 2');
        oCTRL.session.inputFile3 = new Attachment();
        	oCTRL.session.inputFile3.Name = 'TEST File 3';
        	oCTRL.session.inputFile3.Body = Blob.valueOf('TEST BODY 3');
        Attachment oAttachment = oCTRL.session.inputFile;
        oAttachment = oCTRL.session.inputFile2;
        oAttachment = oCTRL.session.inputFile3;
        oCTRL.session.readOnlyAttachments = new List<Attachment>();
        List<Attachment> lstAttachment = oCTRL.session.readOnlyAttachments;

        oCTRL.session.resetRequest();
        oCTRL.session.request = oUserRequest;
        oCTRL.session.getRequestComments();
        oCTRL.getRequest(lstUserRequest);

        oCTRL.session.inputFile = new Attachment();
        	oCTRL.session.inputFile.Name = 'TEST File 1';
        	oCTRL.session.inputFile.Body = Blob.valueOf('TEST BODY 1');
        try{
			clsUtil.tExceptionName = 'ctrl_Support_Requests.saveCommentAndAttachment_EXCEPTION';
    	    oCTRL.session.saveCommentAndAttachment();
	    }catch(Exception oEX){}
		clsUtil.tExceptionName = '';
	    oCTRL.session.saveCommentAndAttachment();

        oCTRL.session.inputFile = new Attachment();
        	oCTRL.session.inputFile.Name = 'TEST File 1';
        	oCTRL.session.inputFile.Body = Blob.valueOf('TEST BODY 1');
        oCTRL.session.saveAttachments(oUserRequest.Id);
        oCTRL.session.clearAttachments();
        oCTRL.session.attachmentId = oAttachment.Id;
        oCTRL.session.deleteAttachment();
		//------------------------------------------------------------------------------------


		//------------------------------------------------------------------------------------
		// Request Related Logic
		//------------------------------------------------------------------------------------
		ctrl_Support_Requests.Request oRequest = new ctrl_Support_Requests.Request(Datetime.now().addDays(-10), Datetime.now());
		ctrl_Support_Requests.Request oRequest2 = new ctrl_Support_Requests.Request(Datetime.now().addDays(-20), Datetime.now().addDays(-5));
		tTest = oRequest.getCreatedDate();
		tTest = oRequest.getCompletedDate();

		oCTRL.selectedOpenReqColumn='CreatedDate';
		oCTRL.sortOpenRequests();
		iTest = oRequest.compareTo(oRequest2);

		oCTRL.selectedOpenReqColumn='CompletedDate';
		oCTRL.sortOpenRequests();
		iTest = oRequest.compareTo(oRequest2);

		oCTRL.selectedOpenReqColumn='RequestType';
		oCTRL.sortOpenRequests();
		iTest = oRequest.compareTo(oRequest2);

		oCTRL.selectedOpenReqColumn='Status';
		oCTRL.sortOpenRequests();
		iTest = oRequest.compareTo(oRequest2);

		oCTRL.selectedOpenReqColumn='RequestId';
		oCTRL.sortOpenRequests();
		iTest = oRequest.compareTo(oRequest2);

		oCTRL.selectedOpenReqColumn='Requestor';
		oCTRL.sortOpenRequests();
		iTest = oRequest.compareTo(oRequest2);

		oCTRL.selectedOpenReqColumn='Application';
		oCTRL.sortOpenRequests();
		iTest = oRequest.compareTo(oRequest2);
		//------------------------------------------------------------------------------------


		//------------------------------------------------------------------------------------
	    // ACTIONS
		//------------------------------------------------------------------------------------
	    oCTRL.goBack();
	    oCTRL.updateRequest();

        try{
			clsUtil.tExceptionName = 'ctrl_Support_Requests.approve_EXCEPTION';
    	    oCTRL.approve();
	    }catch(Exception oEX){}
		clsUtil.tExceptionName = '';
		oCTRL.approve();		
	    
        try{
			clsUtil.tExceptionName = 'ctrl_Support_Requests.reject_EXCEPTION';
    	    oCTRL.reject();
	    }catch(Exception oEX){}
		clsUtil.tExceptionName = '';
	    oCTRL.reject();

      	try{
			clsUtil.tExceptionName = 'ctrl_Support_Requests.cancel_EXCEPTION';
		    oCTRL.cancel();
	    }catch(Exception oEX){}
		clsUtil.tExceptionName = '';
//	    oCTRL.cancel();
		//------------------------------------------------------------------------------------

	}
	
}