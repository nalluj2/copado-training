//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 29/05/2017
//  Description      : APEX Class - Business Logic for tr_OpportunityTeamMember
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
public with sharing class bl_OpportunityTeamMember_Trigger {


	//----------------------------------------------------------------------------------------------------------------
	// Prevent the deletion if an Opportunity Team Member that is also the owner of the Opportunity
	//----------------------------------------------------------------------------------------------------------------
	public static void preventDeleteOppOwner(List<OpportunityTeamMember> lstTriggerOld){

	    if (bl_Trigger_Deactivation.isTriggerDeactivated('OppTeamMember_preventDeleteOppOwner')) return;

        // Define the Opportunity RecordTypes for which this logic should be executed
        Set<Id> setID_RecordType = bl_Opportunity.getOpportunityRecordTypeID('OPPOWNER_OPPTEAM');

	    Set<Id> setID_Opportunity = new Set<Id>();
	    for (OpportunityTeamMember oOpportunityTeamMember : lstTriggerOld){
	    	setID_Opportunity.add(oOpportunityTeamMember.OpportunityId);
	    }


	    Map<Id, Opportunity> mapOpportunity = new Map<Id, Opportunity>([SELECT Id, OwnerId FROM Opportunity WHERE Id = :setID_Opportunity AND RecordTypeId = :setID_RecordType]);


	    for (OpportunityTeamMember oOpportunityTeamMember : lstTriggerOld){

	    	if (mapOpportunity.containsKey(oOpportunityTeamMember.OpportunityId)){

	    		if (oOpportunityTeamMember.UserId == mapOpportunity.get(oOpportunityTeamMember.OpportunityId).OwnerId){

	    			oOpportunityTeamMember.addError('You cannot delete this Opportunity Team Member because it\'s also the Owner of the Opportunity.');

	    		}

	    	}

	    }

	}
	//----------------------------------------------------------------------------------------------------------------
	
	
	public static void giveEditAccess(List<OpportunityTeamMember> triggerNew){
	
		Set<Id> oppIds = new Set<Id>();
		Set<Id> userIds = new Set<Id>();
		
		for(OpportunityTeamMember OTM : triggerNeW){
			
			if(OTM.Auto_Created__c == true){
				
				oppIds.add(OTM.OpportunityId);
				userIds.add(OTM.UserId);
			}
		}
	
		Map<String, OpportunityShare> mapOppShare = new Map<String, OpportunityShare>();
		
		for(OpportunityShare oppShareRecord : [select Id, OpportunityAccessLevel, OpportunityId, UserOrGroupId from OpportunityShare where OpportunityId IN: oppIds AND UserOrGroupId IN :userIds AND RowCause = 'Team']){
			
			mapOppShare.put(oppShareRecord.OpportunityId + ':' + oppShareRecord.UserOrGroupId, oppShareRecord);			
		}
 		
 		List<OpportunityShare> toUpdate = new List<OpportunityShare>();
 		
		for(OpportunityTeamMember OTM : triggerNeW){
			
			if(OTM.Auto_Created__c == true){
				
				OpportunityShare oppShareRecord = mapOppShare.get(OTM.OpportunityId + ':' + OTM.UserId);				
				oppShareRecord.OpportunityAccessLevel = 'Edit';
				toUpdate.add(oppShareRecord);
			}
		}
		
		if(toUpdate.size() > 0) update toUpdate;
	}    
}
//--------------------------------------------------------------------------------------------------------------------