//--------------------------------------------------------------------------------------------------------------------------------
//  Author      :   Bart Caelen
//  Date        :   20160630
//  Description :   APEX TEST Class for the APEX Trigger tr_TrainingCourse and APEX Class bl_TrainingCourse_Trigger
//  Change Log  :    
//--------------------------------------------------------------------------------------------------------------------------------
@isTest private class TEST_bl_TrainingCourse_Trigger {
	
	private static Training_Course__c oTrainingCourse;
	private static List<Training_Nomination__c> lstTrainingNomination;
	private static Map<String, Integer> mapStatus_Counter;
	private static Boolean bCreateTaskForNomination = false;
	private static List<Task> lstTask = new List<Task>();

	@isTest static void createTestData() {

		// Create Contact Data
		clsTestData.iRecord_Contact = 6;
		clsTestData.createContactData();

		// Create Training Course Data
		oTrainingCourse = new Training_Course__c();
			oTrainingCourse.Course_Title__c = 'TEST TRAINING COURSE 001';
			oTrainingCourse.Start_Date__c = Date.today().addDays(20);
			oTrainingCourse.Last_Date_to_Register__c = Date.today().addDays(10);
			oTrainingCourse.Status__c = 'Open for Nomination';
			oTrainingCourse.of_Seats_Available__c = 3;
		insert oTrainingCourse;

		// Create Training Nomination Data
		lstTrainingNomination = new List<Training_Nomination__c>();
		for (Integer i = 0; i < 6; i++){
			Training_Nomination__c oTrainingNomination = new Training_Nomination__c();
				oTrainingNomination.Course__c = oTrainingCourse.Id;
				oTrainingNomination.Contact_Name__c = clsTestData.lstContact[i].Id;
				oTrainingNomination.Business_Unit_vs__c = 'Surgical Innovations';
				oTrainingNomination.Role__c = 'Trainee';
				if ( (i == 0) || (i == 1) ){
					oTrainingNomination.Status__c = 'Approved';
				}else if ( i == 2 ){
					oTrainingNomination.Status__c = 'Evaluation';
				}else{
					oTrainingNomination.Status__c = 'Nominated';
				}
			lstTrainingNomination.add(oTrainingNomination);	
		}

		insert lstTrainingNomination;

		System.assertEquals(lstTrainingNomination.size(), 6);

		mapStatus_Counter = new Map<String, Integer>();
		lstTask = new List<Task>();
		for (Training_Nomination__c oTrainingNomination : lstTrainingNomination){
			Integer iCounter = 0;
			if (mapStatus_Counter.containsKey(oTrainingNomination.Status__c)){
				iCounter = mapStatus_Counter.get(oTrainingNomination.Status__c);
			}
			iCounter++;
			mapStatus_Counter.put(oTrainingNomination.Status__c, iCounter);

			if (bCreateTaskForNomination){
				Task oTask = new Task();
					oTask.WhatId = oTrainingNomination.Id;
					oTask.Subject = 'Test Task';
					oTask.Status = 'Not Started';
				lstTask.add(oTask);
			}
		}

		if (bCreateTaskForNomination){
			insert lstTask;
		}
        
        EMEA_MITG_Opportunites__c recEMEAMITG = EMEA_MITG_Opportunites__c.getValues('EMEA MITG Opportunities');
        
        if (recEMEAMITG == null){
			//Temp solution, due to fact that EMEA MITG will be used later in production.
        	recEMEAMITG = new EMEA_MITG_Opportunites__c();
        	recEMEAMITG.Name = 'EMEA MITG Opportunities';
        	recEMEAMITG.MITG_EMEA_Available__c = true;
        	insert recEMEAMITG;
        }
        


		System.assertEquals(mapStatus_Counter.get('Approved'), 2);
		System.assertEquals(mapStatus_Counter.get('Evaluation'), 1);
		System.assertEquals(mapStatus_Counter.get('Nominated'), 3);
		System.assertEquals(mapStatus_Counter.get('Approved - No Seats'), null);		
		System.assertEquals(mapStatus_Counter.get('Cancelled'), null);		
		System.assertEquals(mapStatus_Counter.get('Follow Up'), null);

	}

	@isTest static void test_updateTrainingNomination_Url() {
		
		// Create / Load Test Data
		createTestData();

		Test.startTest();

			oTrainingCourse.Final_Follow_Up_Survey__c = 'http://www.finalfollowupsurvey.com';
			oTrainingCourse.Initial_Follow_Up_Survey__c = 'http://www.initialfollowupservey.com'; 
			oTrainingCourse.Logistics_Survey__c = 'http://www.logisticssurvey.com';
		update oTrainingCourse;

		Test.stopTest();

		lstTrainingNomination = 
			[
				SELECT 
					Id, Status__c, Final_Follow_Up_Survey__c, Initial_Follow_Up_Survey__c, Logistics_Survey__c 
					, Course__r.Final_Follow_Up_Survey__c, Course__r.Initial_Follow_Up_Survey__c, Course__r.Logistics_Survey__c 
				FROM 
					Training_Nomination__c 
				WHERE 
					Course__c = :oTrainingCourse.Id
				];

		for (Training_Nomination__c oTrainingNomination : lstTrainingNomination){
			if (oTrainingNomination.Status__c == 'Approved'){
				System.assertEquals(oTrainingNomination.Final_Follow_Up_Survey__c, oTrainingNomination.Course__r.Final_Follow_Up_Survey__c);
				System.assertEquals(oTrainingNomination.Initial_Follow_Up_Survey__c, oTrainingNomination.Course__r.Initial_Follow_Up_Survey__c);
				System.assertEquals(oTrainingNomination.Logistics_Survey__c, oTrainingNomination.Course__r.Logistics_Survey__c);
			}else{
				System.assertEquals(clsUtil.isNull(oTrainingNomination.Final_Follow_Up_Survey__c, ''), '');
				System.assertEquals(clsUtil.isNull(oTrainingNomination.Initial_Follow_Up_Survey__c, ''), '');
				System.assertEquals(clsUtil.isNull(oTrainingNomination.Logistics_Survey__c, ''), '');
			}
		}

	}


	@isTest static void test_updateTrainingNomination_Status_NominationClosed() {

		// Create / Load Test Data
		createTestData();
	
		Test.startTest();

			oTrainingCourse.Status__c = 'Nomination Closed';
		update oTrainingCourse;

		Test.stopTest();

		lstTrainingNomination = 
			[
				SELECT 
					Id, Status__c
				FROM 
					Training_Nomination__c 
				WHERE 
					Course__c = :oTrainingCourse.Id
				];

		mapStatus_Counter = new Map<String, Integer>();
		for (Training_Nomination__c oTrainingNomination : lstTrainingNomination){
			Integer iCounter = 0;
			if (mapStatus_Counter.containsKey(oTrainingNomination.Status__c)){
				iCounter = mapStatus_Counter.get(oTrainingNomination.Status__c);
			}
			iCounter++;
			mapStatus_Counter.put(oTrainingNomination.Status__c, iCounter);
		}

		System.assertEquals(mapStatus_Counter.get('Approved'), 2);
		System.assertEquals(mapStatus_Counter.get('Evaluation'), null);
		System.assertEquals(mapStatus_Counter.get('Nominated'), 3);
		System.assertEquals(mapStatus_Counter.get('Not Approved - No Seats'), 1);		
		System.assertEquals(mapStatus_Counter.get('Cancelled'), null);		
		System.assertEquals(mapStatus_Counter.get('Follow Up'), null);

	}
	
	@isTest static void test_updateTrainingNomination_Status_Cancelled() {

		// Create / Load Test Data
		createTestData();

		Test.startTest();

			oTrainingCourse.Status__c = 'Cancelled';
		update oTrainingCourse;

		Test.stopTest();

		lstTrainingNomination = 
			[
				SELECT 
					Id, Status__c
				FROM 
					Training_Nomination__c 
				WHERE 
					Course__c = :oTrainingCourse.Id
				];

		mapStatus_Counter = new Map<String, Integer>();
		for (Training_Nomination__c oTrainingNomination : lstTrainingNomination){
			Integer iCounter = 0;
			if (mapStatus_Counter.containsKey(oTrainingNomination.Status__c)){
				iCounter = mapStatus_Counter.get(oTrainingNomination.Status__c);
			}
			iCounter++;
			mapStatus_Counter.put(oTrainingNomination.Status__c, iCounter);
		}

		System.assertEquals(mapStatus_Counter.get('Approved'), null);
		System.assertEquals(mapStatus_Counter.get('Evaluation'), null);
		System.assertEquals(mapStatus_Counter.get('Nominated'), null);
		System.assertEquals(mapStatus_Counter.get('Approved - No Seats'), null);
		System.assertEquals(mapStatus_Counter.get('Cancelled'), 6);
		System.assertEquals(mapStatus_Counter.get('Follow Up'), null);

	}

	@isTest static void test_updateTrainingNomination_Status_Completed() {

		// Create / Load Test Data
		createTestData();

		Test.startTest();

			oTrainingCourse.Status__c = 'Completed';
		update oTrainingCourse;

		Test.stopTest();

		lstTrainingNomination = 
			[
				SELECT 
					Id, Status__c
				FROM 
					Training_Nomination__c 
				WHERE 
					Course__c = :oTrainingCourse.Id
				];

		mapStatus_Counter = new Map<String, Integer>();
		for (Training_Nomination__c oTrainingNomination : lstTrainingNomination){
			Integer iCounter = 0;
			if (mapStatus_Counter.containsKey(oTrainingNomination.Status__c)){
				iCounter = mapStatus_Counter.get(oTrainingNomination.Status__c);
			}
			iCounter++;
			mapStatus_Counter.put(oTrainingNomination.Status__c, iCounter);
		}

		System.assertEquals(mapStatus_Counter.get('Approved'), null);
		System.assertEquals(mapStatus_Counter.get('Evaluation'), 1);
		System.assertEquals(mapStatus_Counter.get('Nominated'), 3);
		System.assertEquals(mapStatus_Counter.get('Approved - No Seats'), null);
		System.assertEquals(mapStatus_Counter.get('Cancelled'), null);
		System.assertEquals(mapStatus_Counter.get('Follow Up'), 2);


	}	

	@isTest(seeAllData=true) static void test_validateTrainingCourseClosure() {

		// Create / Load Test Data
		bCreateTaskForNomination = true;
		createTestData();
		System.assert(lstTask.size() > 0);


		Boolean bException = false;
		Test.startTest();

			oTrainingCourse.Status__c = 'Nomination Closed';
		try{
			update oTrainingCourse;
		}catch(Exception oEX){
			System.assert(oEX.getMessage().contains(Label.TrainingCourseOpenTaskError));
			bException = true;
		}
		System.assertEquals(bException, true);

		Test.stopTest();

	}

	@isTest static void test_updateTrainingNomination_PACEApprover() {

		// Create / Load Test Data
		createTestData();
        
		Test.startTest();

			oTrainingCourse.PACE_Approver__c = UserInfo.getUserId();
		update oTrainingCourse;

		Test.stopTest();

		System.assertEquals(oTrainingCourse.PACE_Approver__c, UserInfo.getUserId());

		lstTrainingNomination = 
			[
				SELECT 
					Id, PACE_Approver__c
				FROM 
					Training_Nomination__c
				WHERE 
					Course__c =: oTrainingCourse.Id
			];

		for (Training_Nomination__c oTrainingNomination : lstTrainingNomination){

			System.assertEquals(oTrainingNomination.PACE_Approver__c, oTrainingCourse.PACE_Approver__c);

		}

	}
}
//--------------------------------------------------------------------------------------------------------------------------------