@isTest
private class Test_ctrlExt_quickCallRecord_addAction {
	
	private static testmethod void addCallRecordBUAction(){
						
		Test_CallRecord_Utils.initializeCallRecordRelatedData();
		
		RecordType buRT = [Select Id from RecordType where SobjectType = 'Call_Records__c' AND DeveloperName = 'Business_Unit'];
		Business_Unit__c bu = [Select Id, Name from Business_Unit__c LIMIT 1];
		
		Call_Records__c callRecord = new Call_Records__c();
		callRecord.Business_Unit__c = bu.Id;
		callRecord.Call_Date__c = Date.today();
		callRecord.RecordTypeId = buRT.Id;
		insert callRecord;
		
		ApexPages.standardController sc = new ApexPages.StandardController( callRecord );
		
		ctrlExt_quickCallRecord_addAction controller = new ctrlExt_quickCallRecord_addAction(sc);
		
		System.assert(controller.buName == bu.Name);
		
		System.assert(controller.activityTypes.size()==2);		
		controller.callTopic.Call_Activity_Type__c = controller.activityTypes[1].getValue();				
		controller.activityTypeChanged();
		
		System.assert(controller.subjects.size()==1);
		controller.selectedSubjects = new List<String>{controller.subjects[0].getValue()};
		
		System.assert(controller.therapies.size()==2);		
		controller.calltopic.Call_Topic_Therapy__c = controller.therapies[1].getValue();
		controller.therapyChanged();
		
		System.assert(controller.products.size()==1);
		controller.selectedProducts = new List<String>{controller.products[0].getValue()};

		controller.callTopic.Call_Topic_Duration__c = 50;
		controller.callTopic.Call_Topic_Lobbying__c  = 'Selling';

		controller.save();

		System.assert(controller.isSaveError == false);
	}
	
	private static testmethod void addCallRecordxBUAction(){
		
		RecordType xbuRT = [Select Id from RecordType where SobjectType = 'Call_Records__c' AND DeveloperName = 'xBU'];
						
		Test_CallRecord_Utils.initializeXBUCallRecordRelatedData(xbuRT);
						
		Call_Records__c callRecord = new Call_Records__c();		
		callRecord.Call_Date__c = Date.today();
		callRecord.RecordTypeId = xbuRT.Id;
		insert callRecord;
		
		ApexPages.standardController sc = new ApexPages.StandardController( callRecord );
		
		ctrlExt_quickCallRecord_addAction controller = new ctrlExt_quickCallRecord_addAction(sc);
		
		System.assert(controller.buName == null);
		
		System.assert(controller.activityTypes.size()==2);		
		controller.callTopic.Call_Activity_Type__c = controller.activityTypes[1].getValue();				
		controller.activityTypeChanged();
		
		System.assert(controller.subjects.size()==1);
		controller.selectedSubjects = new List<String>{controller.subjects[0].getValue()};
		
		System.assert(controller.bus.size()==1);		
		controller.selectedBUs = new List<String>{controller.bus[0].getValue()};

		controller.callTopic.Call_Topic_Duration__c = 50;
		controller.callTopic.Call_Topic_Lobbying__c  = 'Selling';

		controller.save();

		System.assert(controller.isSaveError == false);
	} 
	
	private static testmethod void testValidation(){
		
		Test_CallRecord_Utils.initializeCallRecordRelatedData();
		
		RecordType buRT = [Select Id from RecordType where SobjectType = 'Call_Records__c' AND DeveloperName = 'Business_Unit'];
		
		Call_Records__c callRecord = new Call_Records__c();
		callRecord.Business_Unit__c = [Select Id from Business_Unit__c LIMIT 1].Id;
		callRecord.Call_Date__c = Date.today();
		callRecord.RecordTypeId = buRT.Id;
		insert callRecord;
		
		ApexPages.standardController sc = new ApexPages.StandardController( callRecord );
		
		ctrlExt_quickCallRecord_addAction controller = new ctrlExt_quickCallRecord_addAction(sc);
		
		controller.productMandatory = true;
		controller.subjectMandatory = true;
		controller.durationMandatory = true;
		controller.lobbyingMandatory = true;
		
		controller.callTopic.Call_Topic_Duration__c = null;
		controller.selectedProducts = new List<String>();
		controller.selectedSubjects = new List<String>();


				
		controller.save();
		
		System.assert(ApexPages.getMessages().size()>0);
	}
}