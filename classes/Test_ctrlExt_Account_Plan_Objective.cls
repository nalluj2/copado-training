@isTest
private class Test_ctrlExt_Account_Plan_Objective {

	private static testMethod void redirectBUGRTG(){
		
		setData();
		
		Id accPlanRTG_BUG_Id = RecordTypeMedtronic.getRecordTypeByDevName('Account_Plan_2__c', 'EUR_RTG_BUG').Id;
		Id objectiveRTG_Id = RecordTypeMedtronic.getRecordTypeByDevName('Account_Plan_Objective__c', 'RTG').Id;
		
		Account_Plan_Level__c bugLevel = new Account_Plan_Level__c();
			bugLevel.Account_Plan_Level__c = 'Business Unit Group';
			bugLevel.Company__c = [Select Id from Company__c].Id;
			bugLevel.Business_Unit__c = [Select Id from Business_Unit__c].Id;
			bugLevel.Account_Plan_Record_Type__c = String.valueOf(accPlanRTG_BUG_Id).left(15);
			bugLevel.Account_Plan_Objective_Record_Type__c = String.valueOf(objectiveRTG_Id).left(15);		
		insert bugLevel;
		
		Account_Plan_2__c accPlan = new Account_Plan_2__c();
			accPlan.Account__c = [Select Id from Account].Id;
			accPlan.Account_Plan_Level__c = 'Business Unit Group';
			accPlan.Business_Unit_Group__c = [Select Id from Business_Unit_Group__c].Id;
			accPlan.RecordTypeId = accPlanRTG_BUG_Id;
		insert accPlan;
		
		Test.startTest();
		
		Account_Plan_Objective__c objective = new Account_Plan_Objective__c();
			objective.Account_Plan__c = accPlan.Id;
		
		ApexPages.StandardController sc = new ApexPages.StandardController(objective);
		ctrlExt_Account_Plan_Objective controller = new ctrlExt_Account_Plan_Objective(sc);
		
		PageReference pr = controller.redirectWithRecordType();
		
		System.assert(pr.getParameters().get('RecordType') == null);
	
	}
	
	private static testMethod void redirectSBUGeneral(){
		
		setData();
		
		Id accPlanCVG_sBU_Id = RecordTypeMedtronic.getRecordTypeByDevName('Account_Plan_2__c', 'EUR_CVG_sBU').Id;
		Id objectiveRTG_Id = RecordTypeMedtronic.getRecordTypeByDevName('Account_Plan_Objective__c', 'General').Id;
				
		Account_Plan_Level__c sbuLevel = new Account_Plan_Level__c();
			sbuLevel.Account_Plan_Level__c = 'Sub Business Unit';
			sbuLevel.Company__c = [Select Id from Company__c].Id;
			sbuLevel.Business_Unit__c = [Select Id from Business_Unit__c].Id;
			sbuLevel.Account_Plan_Record_Type__c = String.valueOf(accPlanCVG_sBU_Id).left(15);				
		insert sbuLevel;
		
		Account_Plan_2__c accPlan = new Account_Plan_2__c();
			accPlan.Account__c = [Select Id from Account].Id;
			accPlan.Account_Plan_Level__c = 'Sub Business Unit';
			accPlan.Business_Unit_Group__c = [Select Id from Business_Unit_Group__c].Id;
			accPlan.Business_Unit__c = sbuLevel.Business_Unit__c;
			accPlan.Sub_Business_Unit__c = [Select Id from Sub_Business_Units__c].Id;
			accPlan.RecordTypeId = accPlanCVG_sBU_Id;
		insert accPlan;
		
		Test.startTest();
		
		Account_Plan_Objective__c objective = new Account_Plan_Objective__c();
			objective.Account_Plan__c = accPlan.Id;
		
		ApexPages.StandardController sc = new ApexPages.StandardController(objective);
		ctrlExt_Account_Plan_Objective controller = new ctrlExt_Account_Plan_Objective(sc);
		
		PageReference pr = controller.redirectWithRecordType();
		
		System.assert(pr.getParameters().get('RecordType') == objectiveRTG_Id);

	}
	
	private static testMethod void redirect_Edit_BUGRTG(){
		
		setData();
		
		Id accPlanRTG_BUG_Id = RecordTypeMedtronic.getRecordTypeByDevName('Account_Plan_2__c', 'EUR_RTG_BUG').Id;
		Id objectiveRTG_Id = RecordTypeMedtronic.getRecordTypeByDevName('Account_Plan_Objective__c', 'RTG').Id;
				
		Account_Plan_2__c accPlan = new Account_Plan_2__c();
			accPlan.Account__c = [Select Id from Account].Id;
			accPlan.Account_Plan_Level__c = 'Business Unit Group';
			accPlan.Business_Unit_Group__c = [Select Id from Business_Unit_Group__c].Id;
			accPlan.RecordTypeId = accPlanRTG_BUG_Id;
		insert accPlan;
		
		Test.startTest();
		
		Account_Plan_Objective__c objective = new Account_Plan_Objective__c();
			objective.Account_Plan__c = accPlan.Id;
			objective.Description__c = 'test description';
			objective.Target_Date__c = Date.today();
			objective.RecordTypeId = objectiveRTG_Id;
		insert objective;
		
		ApexPages.StandardController sc = new ApexPages.StandardController(objective);
		ctrlExt_Account_Plan_Objective controller = new ctrlExt_Account_Plan_Objective(sc);
		
		PageReference pr = controller.redirectWithRecordType();
		
		System.assert(pr.getParameters().get('RecordType') == null);
	
	}
	
	private static testMethod void redirect_Edit_SBUGeneral(){
		
		setData();
		
		Id accPlanCVG_sBU_Id = RecordTypeMedtronic.getRecordTypeByDevName('Account_Plan_2__c', 'EUR_CVG_sBU').Id;
		Id objectiveGeneral_Id = RecordTypeMedtronic.getRecordTypeByDevName('Account_Plan_Objective__c', 'General').Id;
						
		Account_Plan_2__c accPlan = new Account_Plan_2__c();
			accPlan.Account__c = [Select Id from Account].Id;
			accPlan.Account_Plan_Level__c = 'Sub Business Unit';
			accPlan.Business_Unit_Group__c = [Select Id from Business_Unit_Group__c].Id;
			accPlan.Business_Unit__c = [Select Id from Business_Unit__c].Id;
			accPlan.Sub_Business_Unit__c = [Select Id from Sub_Business_Units__c].Id;
			accPlan.RecordTypeId = accPlanCVG_sBU_Id;
		insert accPlan;
		
		Test.startTest();
		
		Account_Plan_Objective__c objective = new Account_Plan_Objective__c();
			objective.Account_Plan__c = accPlan.Id;
			objective.Account_Plan__c = accPlan.Id;
			objective.Description__c = 'test description';
			objective.Target_Date__c = Date.today();
			objective.RecordTypeId = objectiveGeneral_Id;
		insert objective;
		
		ApexPages.StandardController sc = new ApexPages.StandardController(objective);
		ctrlExt_Account_Plan_Objective controller = new ctrlExt_Account_Plan_Objective(sc);
		
		PageReference pr = controller.redirectWithRecordType();
		
		System.assert(pr.getParameters().get('nooverride') == '1');

	}
	
	private static void setData() {
	
		Company__c cmpny = new Company__c();		
			cmpny.Name = 'Europe';
			cmpny.CurrencyIsoCode = 'EUR';
			cmpny.Current_day_in_Q1__c = 56;
			cmpny.Current_day_in_Q2__c = 34; 
			cmpny.Current_day_in_Q3__c = 5; 
			cmpny.Current_day_in_Q4__c = 0;   
			cmpny.Current_day_in_year__c = 200;
			cmpny.Days_in_Q1__c  =56;  
			cmpny.Days_in_Q2__c = 34;
			cmpny.Days_in_Q3__c = 13;
			cmpny.Days_in_Q4__c = 22;
			cmpny.Days_in_year__c = 250;
			cmpny.Company_Code_Text__c = 'T35';
		insert cmpny;
		
		Business_Unit_Group__c bug =  new Business_Unit_Group__c();
	        bug.Master_Data__c = cmpny.id;
		    bug.Name = 'RTG';      
        insert bug;
		
		Business_Unit__c bu =  new Business_Unit__c();
			bu.Company__c = cmpny.id;
			bu.name = 'RTG BU';
			bu.Business_Unit_Group__c = bug.Id;
			bu.Account_Plan_Activities__c = true;        
        insert bu;
        
        Sub_Business_Units__c sbu = new Sub_Business_Units__c();
			sbu.Name = 'RTG sBU';
			sbu.Business_Unit__c = bu.id;
		insert sbu;		
						
		Account testAccount = new Account();
			testAccount.Name = 'RTG Test Account';
		insert testAccount;		
	
	}

}