@isTest
private class Test_ctrlExt_QuickAccount_TeamMember {
	
	private static testmethod void addTeamMember(){
		
		//Insert Account        
        Account acct = new Account();
            acct.Name = 'Test_triggerAffiliationBeforeUpsert Test Coverage Account 1 ' ; 
            acct.SAP_ID__c = '0004567898';
            acct.Type = 'Public Hospital';
            acct.Account_Country_vs__c = 'NETHERLANDS';      
        insert acct ;
			
		ctrlExt_QuickAccount_TeamMember controller = new ctrlExt_QuickAccount_TeamMember (new ApexPages.Standardcontroller(acct));
		Account_Team_Member__c teamMember = controller.teamMember;
		teamMember.User__c = UserInfo.getUserId();
		teamMember.Role__c = 'Core Team';
		
		controller.save();
		
		System.assert(teamMember.Id != null);
		
	}		
}