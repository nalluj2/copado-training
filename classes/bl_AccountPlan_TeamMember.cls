public class bl_AccountPlan_TeamMember{
	
	public static void validateTeamMemberIsOwner(List<Account_Plan_Team_Member__c> triggerNew){
		
		// Validate if TeamMember is Account Plan Owner
    	Set<Id> setOfAccntPlanId = new Set<Id>();
    	
    	Map<Id, Account_Plan_Team_Member__c> mapOfIdToAccntPlanTM = new Map<Id, Account_Plan_Team_Member__c>();
    	 
    	for (Account_Plan_Team_Member__c accntPlanTM : triggerNew){
    		// Only validate if flag is NOT set
    		if (accntPlanTM.Account_Plan_Owner__c == false){
	    		setOfAccntPlanId.add(accntPlanTM.Account_Plan__c);
	    		mapOfIdToAccntPlanTM.put(accntPlanTM.Id, accntPlanTM);
    		}
    	}
    	
    	if (!setOfAccntPlanId.isEmpty()){
    		// Retrieve Account Plans
    		List<Account_Plan_2__c> listOfAccntPlan = [select Id, OwnerId from Account_Plan_2__c where Id in :setOfAccntPlanId];
    		// Loop through Account Plan list and Team Member map to validate if owner account plan is account plan team member
    		// Set flag if true
    		for (Account_Plan_2__c thisAccntPlan : listOfAccntPlan){
    			
    			for (Id accntPlanTMId : mapOfIdToAccntPlanTM.keySet()){
    				// If Account Team Member is Owner of the Account Plan, set flag
    				if (mapOfIdToAccntPlanTM.get(accntPlanTMId).Team_Member__c == thisAccntPlan.OwnerId){
    					mapOfIdToAccntPlanTM.get(accntPlanTMId).Account_Plan_Owner__c = true;
    				}
    			}
    		}
    	}		
	}
	
	public static void manageAccountPlanSharing(List<Account_Plan_Team_Member__c> triggerNew, Map<Id, Account_Plan_Team_Member__c> triggerOldMap){
				
		Map<Id, Set<Id>> mapAccPlanidWithUsers = new Map<Id, Set<Id>>();
		Map<Id, Set<Id>> mapOldAccPlanidWithUsers=new Map<Id, Set<Id>>();		
                
        for(Account_Plan_Team_Member__c APTM : triggerNew){
        	
        	// Patrick Brinksma - 22/07/2013 Prevent Sharing to be added for Account Plan Owner
        	if (!APTM.Account_Plan_Owner__c){ 
	            
	            Set<Id> setTempId = mapAccPlanidWithUsers.get(APTM.Account_Plan__c);
	            
	            if(setTempId == null){
	            	
	            	setTempId = new Set<Id>();
	            	mapAccPlanidWithUsers.put(APTM.Account_Plan__c, setTempId);	            	
	            }
	            	  
	            setTempId.add(APTM.Team_Member__c);
	            
	            // Update
        		if(triggerOldMap != null && APTM.Team_Member__c != triggerOldMap.get(APTM.id).Team_Member__c){
        				
    				Set<Id> setOldTempId = mapOldAccPlanidWithUsers.get(APTM.Account_Plan__c);
	                
	                if(setOldTempId == null){
		            
		                setOldTempId = new Set<Id>();
		                mapOldAccPlanidWithUsers.put(APTM.Account_Plan__c, setOldTempId);
	                
	                }  
	                
	                setOldTempId.add(triggerOldMap.get(APTM.id).Team_Member__c);	                       			
        		}
        	}
        }
        
        bl_AccountPlanSharingToTeamMembers.AccountPlanSharingToAddedTeamMembers(mapAccPlanidWithUsers);
        bl_AccountPlanSharingToTeamMembers.AccountPlanDeleteSharingForRemovedTeamMembers(mapOldAccPlanidWithUsers);         
	}
	
	public static void manageAccountPlanSharingDelete(List<Account_Plan_Team_Member__c> triggerOld){
		
		Map<Id, Set<Id>> mapAccPlanidWithUsers = new Map<Id, Set<Id>>();
        
        for(Account_Plan_Team_Member__c APTM : triggerOld){
        	
            Set<Id> setTempId = mapAccPlanidWithUsers.get(APTM.Account_Plan__c);
	            
            if(setTempId == null){
	            	
            	setTempId = new Set<Id>();
            	mapAccPlanidWithUsers.put(APTM.Account_Plan__c, setTempId);	            	
            }
	            	  
	        setTempId.add(APTM.Team_Member__c);
        }
        
        bl_AccountPlanSharingToTeamMembers.AccountPlanDeleteSharingForRemovedTeamMembers(mapAccPlanidWithUsers); 
	}
	
	public static void validateUniqueStrategicAccountManager(List<Account_Plan_Team_Member__c> triggerNew){
		
		Set<Id> accountPlanIds = new Set<Id>();
		
		for(Account_Plan_Team_Member__c accPlanTM : triggerNew){
			
			accountPlanIds.add(accPlanTM.Account_Plan__c);
		}
		
		Set<Id> multipleSAMAccPlanIds = new Set<Id>();
		
		for(Account_Plan_2__c accPlan : [Select Id, (Select Id from Account_Plan_Teams__r where Role__c = 'Strategic Account Manager') from Account_Plan_2__c where Id IN :accountPlanIds AND RecordType.DeveloperName IN ('EUR_CVG_BUG', 'EUR_CVG_sBU', 'MEA_CVG_BUG', 'MEA_CVG_sBU', 'RUS_CVG_BUG', 'RUS_CVG_sBU')]){
			
			if(accPlan.Account_Plan_Teams__r.size() > 1) multipleSAMAccPlanIds.add(accPlan.Id);
		}
		
		if(multipleSAMAccPlanIds.size() > 0){
			
			for(Account_Plan_Team_Member__c accPlanTM : triggerNew){
			
				if(multipleSAMAccPlanIds.contains(accPlanTM.Account_Plan__c)) accPlanTM.addError('Only one Team Member with role \'Strategic Account Manager\' can be created per Account Plan');
			}			
		}
	}


	//----------------------------------------------------------------------------------------------------------------------------------------
	// Sync the Account Plan Team Member(s) with the Opportunity Team Members
	//----------------------------------------------------------------------------------------------------------------------------------------
	public static void syncAPTM_OTM(List<Account_Plan_Team_Member__c> lstTriggerNew, Map<Id, Account_Plan_Team_Member__c> mapTriggerOld, Boolean bIsDelete){

        if (bl_Trigger_Deactivation.isTriggerDeactivated('accountPlanTeamMember_syncAPTM_OTM')) return;

        Set<Id> setID_RecordType = new Set<Id>{clsUtil.getRecordTypeByDevName('Account_Plan_2__c', 'SAM_Account_Plan').Id};


		// Collect the Account Plan Id's for the processing Account Plan Team Members
		Set<Id> setID_AccountPlan = new Set<Id>();
		for (Account_Plan_Team_Member__c oAccountPlanTeamMember : lstTriggerNew) setID_AccountPlan.add(oAccountPlanTeamMember.Account_Plan__c);


		// Collect the Opportunities linked to the collected Account Plan records for the Account Plans that need to be processed (based on RecordType)
		Map<Id, Account_Plan_2__c> mapAccountPlan = new Map<Id, Account_Plan_2__c>([SELECT Id, (SELECT Id FROM Opportunities__r) FROM Account_Plan_2__c WHERE Id = :setID_AccountPlan AND RecordTypeId = :setID_RecordType]);
		List<Opportunity> lstOpportunity_ALL = [SELECT Id, Name, Account_Plan__c FROM Opportunity LIMIT 100];


		Set<Id> setID_Opportunity_All = new Set<Id>();
		Map<Id, Set<Id>> mapAccountPlanId_OpportunityId = new Map<Id, Set<Id>>();
		for (Account_Plan_2__c oAccountPlan : mapAccountPlan.values()){

			Set<Id> setID_Opportunity_Tmp = new Set<Id>();
			if (mapAccountPlanId_OpportunityId.containsKey(oAccountPlan.Id)) setID_Opportunity_Tmp = mapAccountPlanId_OpportunityId.get(oAccountPlan.Id);

			for (Opportunity oOpportunity : oAccountPlan.Opportunities__r) setID_Opportunity_Tmp.add(oOpportunity.Id);

			mapAccountPlanId_OpportunityId.put(oAccountPlan.Id, setID_Opportunity_Tmp);
			setID_Opportunity_All.addAll(setID_Opportunity_Tmp);

		}

		if (setID_Opportunity_All.size() == 0) return;

		Map<Id, Opportunity> mapOpportunity = new Map<Id, Opportunity>([SELECT Id, Account_Plan__c, (SELECT Id, Name, OpportunityAccessLevel, TeamMemberRole, Title, UserId FROM OpportunityTeamMembers) FROM Opportunity WHERE Id = :setID_Opportunity_All ORDER BY Account_Plan__c]);

		// Get the Opportunity Team Member User Id with the corresponding Opportunity Team Member records in a map
		Map<Id, Map<Id, List<OpportunityTeamMember>>> mapOpportunityId_UserId_OpportunityTeamMember = new Map<Id, Map<Id, List<OpportunityTeamMember>>>();
		for (Opportunity oOpportunity : mapOpportunity.values()){

			Map<Id, List<OpportunityTeamMember>> mapUserID_OpportunityTeamMember = new Map<Id, List<OpportunityTeamMember>>();
			if (mapOpportunityId_UserId_OpportunityTeamMember.containsKey(oOpportunity.Id)) mapUserID_OpportunityTeamMember = mapOpportunityId_UserId_OpportunityTeamMember.get(oOpportunity.Id);

			for (OpportunityTeamMember oOpportunityTeamMember : oOpportunity.OpportunityTeamMembers){

				List<OpportunityTeamMember> lstOpportunityTeamMember = new List<OpportunityTeamMember>();
				if (mapUserID_OpportunityTeamMember.containsKey(oOpportunityTeamMember.UserId)) lstOpportunityTeamMember = mapUserID_OpportunityTeamMember.get(oOpportunityTeamMember.UserId);
				lstOpportunityTeamMember.add(oOpportunityTeamMember);
				mapUserID_OpportunityTeamMember.put(oOpportunityTeamMember.UserId, lstOpportunityTeamMember);

			} 
			mapOpportunityId_UserId_OpportunityTeamMember.put(oOpportunity.Id, mapUserID_OpportunityTeamMember);

		}

		// Collect the ID's that are needed to create the Account Plan Team Member records for CREATE and DELETE (UPDATE wil result in DELETE and CREATE)
		Map<Id, Set<Id>> mapAccountPlanID_UserId_Insert = new Map<Id, Set<Id>>();
		Map<Id, Set<Id>> mapAccountPlanID_UserId_Delete = new Map<Id, Set<Id>>();

		// Loop through the processing Account Plan Team Members and determine if the Account Plan Team Member is CREATED, UPDATED or DELETED
		for (Account_Plan_Team_Member__c oAccountPlanTeamMember : lstTriggerNew){

			if (!mapAccountPlan.containsKey(oAccountPlanTeamMember.Account_Plan__c)) continue;

			if (bIsDelete){

				// DELETE - Account Plan Team Member is deleted - delete related Opportunity Team Members
				Set<Id> setID_User = new Set<Id>();
				if (mapAccountPlanID_UserId_Delete.containsKey(oAccountPlanTeamMember.Account_Plan__c)) setID_User = mapAccountPlanID_UserId_Delete.get(oAccountPlanTeamMember.Account_Plan__c);
				setID_User.add(oAccountPlanTeamMember.Team_Member__c);
				mapAccountPlanID_UserId_Delete.put(oAccountPlanTeamMember.Account_Plan__c, setID_User);

			}else{

				if (mapTriggerOld == null){

					// INSERT - Account Plan Team Member is created - create related Opportunity Team Members
					Set<Id> setID_User = new Set<Id>();
					if (mapAccountPlanID_UserId_Insert.containsKey(oAccountPlanTeamMember.Account_Plan__c)) setID_User = mapAccountPlanID_UserId_Insert.get(oAccountPlanTeamMember.Account_Plan__c);
					setID_User.add(oAccountPlanTeamMember.Team_Member__c);
					mapAccountPlanID_UserId_Insert.put(oAccountPlanTeamMember.Account_Plan__c, setID_User);

				}else{

					Account_Plan_Team_Member__c oAccountPlanTeamMember_OLD = mapTriggerOld.get(oAccountPlanTeamMember.Id);
					// UPDATE - Account Plan Team Member is updated - update related Opportunity Team Members
					// Verify if Team Member is changed --> delete old and create new Opportunity Team Member
					if (oAccountPlanTeamMember.Team_Member__c != oAccountPlanTeamMember_OLD.Team_Member__c){
						
						Set<Id> setID_User_Insert = new Set<Id>();
						if (mapAccountPlanID_UserId_Insert.containsKey(oAccountPlanTeamMember.Account_Plan__c)) setID_User_Insert = mapAccountPlanID_UserId_Insert.get(oAccountPlanTeamMember.Account_Plan__c);
						setID_User_Insert.add(oAccountPlanTeamMember.Team_Member__c);
						mapAccountPlanID_UserId_Insert.put(oAccountPlanTeamMember.Account_Plan__c, setID_User_Insert);

						Set<Id> setID_User_Delete = new Set<Id>();
						if (mapAccountPlanID_UserId_Delete.containsKey(oAccountPlanTeamMember_OLD.Account_Plan__c)) setID_User_Delete = mapAccountPlanID_UserId_Delete.get(oAccountPlanTeamMember_OLD.Account_Plan__c);
						setID_User_Delete.add(oAccountPlanTeamMember_OLD.Team_Member__c);
						mapAccountPlanID_UserId_Delete.put(oAccountPlanTeamMember_OLD.Account_Plan__c, setID_User_Delete);

					}

				}

			}

		}


		// Process the CREATED Account Plan Team Members
		if (mapAccountPlanID_UserId_Insert.size() > 0){

			List<OpportunityTeamMember> lstOpportunityTeamMember_Insert = new List<OpportunityTeamMember>();

			for (Id id_AccountPlan : mapAccountPlanID_UserId_Insert.keySet()){

				Set<Id> setID_User = mapAccountPlanID_UserId_Insert.get(id_AccountPlan);

				if (mapAccountPlanId_OpportunityId.containsKey(id_AccountPlan)){

					Set<Id> setID_Opportunity = mapAccountPlanId_OpportunityId.get(id_AccountPlan);

					for (Id id_Opportunity : setID_Opportunity){

						if (mapOpportunityId_UserId_OpportunityTeamMember.containsKey(id_Opportunity)){

							Map<Id, List<OpportunityTeamMember>> mapUserID_OpportunityTeamMember = mapOpportunityId_UserId_OpportunityTeamMember.get(id_Opportunity);

							for (Id id_User : setID_User){
						
								if (!mapUserID_OpportunityTeamMember.containsKey(id_User)){
						
									// Create new Opportunity Team Member
									OpportunityTeamMember oOpportunityTeamMember = new OpportunityTeamMember();
										oOpportunityTeamMember.OpportunityId = id_Opportunity;
										oOpportunityTeamMember.UserId = id_User;
										oOpportunityTeamMember.TeamMemberRole = 'Account Manager';
										oOpportunityTeamMember.OpportunityAccessLevel = 'Edit';
									lstOpportunityTeamMember_Insert.add(oOpportunityTeamMember);

								}

							}

						}

					}

				}

			}

			if (lstOpportunityTeamMember_Insert.size() > 0) insert lstOpportunityTeamMember_Insert;

		}			


		// Process the DELETED Account Plan Team Members
		if (mapAccountPlanID_UserId_Delete.size() > 0){

			List<OpportunityTeamMember> lstOpportunityTeamMember_Delete = new List<OpportunityTeamMember>();

			for (Id id_AccountPlan : mapAccountPlanID_UserId_Delete.keySet()){

				Set<Id> setID_User = mapAccountPlanID_UserId_Delete.get(id_AccountPlan);

				if (mapAccountPlanId_OpportunityId.containsKey(id_AccountPlan)){

					Set<Id> setID_Opportunity = mapAccountPlanId_OpportunityId.get(id_AccountPlan);

					for (Id id_Opportunity : setID_Opportunity){

						if (mapOpportunityId_UserId_OpportunityTeamMember.containsKey(id_Opportunity)){

							Map<Id, List<OpportunityTeamMember>> mapUserID_OpportunityTeamMember = mapOpportunityId_UserId_OpportunityTeamMember.get(id_Opportunity);

							for (Id id_User : setID_User){
						
								if (mapUserID_OpportunityTeamMember.containsKey(id_User)){
						
									// Delete existing Opportunity Team Members
									lstOpportunityTeamMember_Delete.addAll(mapUserID_OpportunityTeamMember.get(id_User));
								
								}

							}

						}

					}

				}

			}

			if (lstOpportunityTeamMember_Delete.size() > 0) delete lstOpportunityTeamMember_Delete;

		}


	}
	//----------------------------------------------------------------------------------------------------------------------------------------



}