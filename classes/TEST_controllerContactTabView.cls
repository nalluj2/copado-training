/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TEST_controllerContactTabView {

    static testMethod void myUnitTest() {
    	System.debug(' ################## ' + 'BEGIN TEST_controllerContactTabView' + ' ################');
	    Account acc1 = new Account();
	    acc1.name ='account test' ;
	    acc1.SAP_ID__c =  'TESTSAPID6'; 
	    insert acc1 ; 
	    
	    Contact cont1 = new Contact();
		cont1.LastName = 'TestCont1' ;  
		cont1.FirstName = 'Test Contact  1';
		cont1.AccountId = acc1.Id ;
		cont1.Contact_Active__c = false  ;
		cont1.Contact_Inactive_Reason__c = 'Inactive Reason is required to all inactve Contacts'; 
	    cont1.Contact_Department__c = 'Diabetes Adult'; 
	    cont1.Contact_Primary_Specialty__c = 'ENT';
	    cont1.Affiliation_To_Account__c = 'Employee';
	    cont1.Primary_Job_Title_vs__c = 'Manager';
	    cont1.Contact_Gender__c = 'Male'; 		
		insert cont1;
	    
        ApexPages.StandardController contactstdController = new ApexPages.StandardController(cont1); //set up the standardcontroller    	
		controllerContactTabView controller = new controllerContactTabView(contactstdController);
		controller.goToContactTabView();
		System.debug(' ################## ' + 'END TEST_controllerContactTabView' + ' ################');
    }
}