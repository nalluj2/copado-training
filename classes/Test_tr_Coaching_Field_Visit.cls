//  Change Log       : PMT-22787: MITG EMEA User Setup - Added record type MITG EMEA Field Visit
@isTest
private class Test_tr_Coaching_Field_Visit {
    
    static testMethod void testClosureWithOpenTasks() {
	
        //RecordType fvrt = [select id from RecordType where DeveloperName = 'MITG_MEA_Field_Visit'];
        //Recordtype taskrt = [select id from RecordType where DeveloperName = 'MITG_Europe_Task'];
        
		//Profile pId = [select id from Profile where Name = 'MEA Field Force MITG'];
        
        
        RecordType fvrt = [select id from RecordType where DeveloperName = 'MITG_EMEA_Field_Visit'];
        Recordtype taskrt = [select id from RecordType where DeveloperName = 'MITG_EMEA_Task'];
        
		Profile pId = [select id from Profile where Name = 'EMEA Field Force MITG'];
        
        User manager = new User();
        manager.firstName = 'Manager';
        manager.LastName = 'Test';
        manager.Alias = 'MTest';
        manager.Email = 'manager.test@medtronic.com';
        manager.Username = 'manager.test@medtronic.com';
        manager.CommunityNickname = 'manager.test';
        manager.Alias_unique__c = 'testmngr';
        manager.ProfileID= pId.Id; 
        manager.Region_vs__c = 'Europe';
        manager.Company_Code_text__c = 'EUR';
        manager.User_Business_Unit_vs__c  = 'Surgical Innovations';                
        manager.TimeZoneSidKey ='Asia/Riyadh';
        manager.LocaleSidKey = 'en_US';
        manager.EmailEncodingKey ='UTF-8';
        manager.LanguageLocaleKey ='en_US';
                
        User rep = new User();
        rep.firstName = 'Rep';
        rep.LastName = 'Test';
        rep.Alias = 'RTest';
        rep.Email = 'Rep.test@medtronic.com';
        rep.Username = 'Rep.test@medtronic.com';
        rep.CommunityNickname = 'Rep.test';
        rep.Alias_unique__c = 'testrep';
        rep.ProfileID= pId.Id; 
        rep.Region_vs__c = 'Europe';
        rep.Company_Code_text__c = 'EUR';
        rep.User_Business_Unit_vs__c  = 'Surgical Innovations';                
        rep.TimeZoneSidKey ='Asia/Riyadh';
        rep.LocaleSidKey = 'en_US';
        rep.EmailEncodingKey = 'UTF-8';
        rep.LanguageLocaleKey = 'en_US';
        
        insert new List<User>{manager, rep};
        
		Test.startTest();
		
        Coaching_Field_Visit__c f = new Coaching_Field_Visit__c();
        f.recordtypeId = fvrt.Id  ;
        f.Manager_Mentor__c = manager.Id;
        f.Sales_Rep__c = rep.Id;
        f.field_visit_status__c = 'Planned';
        insert f;
        
        Task t = new Task();
        t.recordtypeId = taskrt.Id;
        t.ownerId = rep.Id;
        t.subject = 'Call';
        t.status = 'In Progress';
        t.priority = 'Normal';        
        t.WhatId = f.Id;
        insert t;
		
		System.runAs(manager)
        {		
			try {
        		
				f.field_visit_status__c = 'Closed';
				update f;
				
            }catch (Exception e)
			{
				System.Assert(e.getMessage().contains('Cannot close Coaching/Field visit record since there are open tasks'));
			}
        }
        
    }
    
    static testMethod void testCreateShares() {
                
        //ID eurMITGProfileID = [Select id from Profile where Name = 'EUR Field Force MITG' Limit 1].id;
        ID eurMITGProfileID = [Select id from Profile where Name = 'EMEA Field Force MITG' Limit 1].id;
        
        User manager = new User();
        manager.firstName = 'Manager';
        manager.LastName = 'Test';
        manager.Alias = 'MTest';
        manager.Email = 'manager.test@medtronic.com';
        manager.Username = 'manager.test@medtronic.com';
        manager.CommunityNickname = 'manager.test';
        manager.Alias_unique__c = 'testmngr';
        manager.ProfileID= eurMITGProfileID; 
        manager.Region_vs__c = 'Europe';
        manager.Company_Code_text__c = 'EUR';
        manager.User_Business_Unit_vs__c  = 'Surgical Innovations';                
        manager.TimeZoneSidKey ='Europe/Amsterdam';
        manager.LocaleSidKey = 'en_US';
        manager.EmailEncodingKey ='UTF-8';
        manager.LanguageLocaleKey ='en_US';
                
        User rep = new User();
        rep.firstName = 'Rep';
        rep.LastName = 'Test';
        rep.Alias = 'RTest';
        rep.Email = 'Rep.test@medtronic.com';
        rep.Username = 'Rep.test@medtronic.com';
        rep.CommunityNickname = 'Rep.test';
        rep.Alias_unique__c = 'testrep';
        rep.ProfileID= eurMITGProfileID; 
        rep.Region_vs__c = 'Europe';
        rep.Company_Code_text__c = 'EUR';
        rep.User_Business_Unit_vs__c  = 'Surgical Innovations';                
        rep.TimeZoneSidKey ='Europe/Amsterdam';
        rep.LocaleSidKey = 'en_US';
        rep.EmailEncodingKey = 'UTF-8';
        rep.LanguageLocaleKey = 'en_US';
        
        insert new List<User>{manager, rep};
		
        Test.startTest(); 
        
        //RecordType rtc=[Select Id from RecordType where DeveloperName='MITG_EUR_Field_Visit' and SobjectType='Coaching_Field_Visit__c' Limit 1];
        RecordType rtc=[Select Id from RecordType where DeveloperName='MITG_EMEA_Field_Visit' and SobjectType='Coaching_Field_Visit__c' Limit 1];
        //Create portalusercontac
		
        Coaching_Field_Visit__c FieldVisit = new Coaching_Field_Visit__c(         
        RecordtypeId = rtc.id,
        Manager_Mentor__c = manager.Id,
        Sales_Rep__c = rep.Id);          
        
        insert fieldVisit;   
          
        List<Coaching_Field_Visit__Share> managerShare = [SELECT Id, UserOrGroupId FROM Coaching_Field_Visit__Share 
															WHERE ParentId = :fieldVisit.Id AND RowCause = 'Manager_Mentor_Access__c'];
       
        System.assert(managerShare.size() == 1);
		System.assert(managerShare[0].UserOrGroupId == manager.Id);
		
		List<Coaching_Field_Visit__Share> salesRepShare = [SELECT Id, UserOrGroupId FROM Coaching_Field_Visit__Share 
															WHERE ParentId = :fieldVisit.Id AND RowCause = 'Sales_Rep_Access__c'];
       
        System.assert(salesRepShare.size() == 1);
		System.assert(salesRepShare[0].UserOrGroupId == rep.Id);
		
		fieldVisit.Manager_Mentor__c = rep.Id;
		fieldVisit.Sales_Rep__c = manager.Id;
		update fieldVisit;
		
		managerShare = [SELECT Id, UserOrGroupId FROM Coaching_Field_Visit__Share 
															WHERE ParentId = :fieldVisit.Id AND RowCause = 'Manager_Mentor_Access__c'];
       
        System.assert(managerShare.size() == 1);
		System.assert(managerShare[0].UserOrGroupId == rep.Id);
		
		salesRepShare = [SELECT Id, UserOrGroupId FROM Coaching_Field_Visit__Share 
															WHERE ParentId = :fieldVisit.Id AND RowCause = 'Sales_Rep_Access__c'];
       
        System.assert(salesRepShare.size() == 1);
		System.assert(salesRepShare[0].UserOrGroupId == manager.Id);
        
        Test.stopTest();
    }
}