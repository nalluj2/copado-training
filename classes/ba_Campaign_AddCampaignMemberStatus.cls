/*
//--------------------------------------------------------------------------------------------------------------------
//  Author      : Christophe Saenen
//  Version     : 1.0
//  Date        : 201600302
//  Description : CR-10656 creation of task from trigger tr_CampaignMember_CreateFollowUpTask to batch ba_Campaign_AddCampaignMemberStatus
//--------------------------------------------------------------------------------------------------------------------
*/

 global with sharing class ba_Campaign_AddCampaignMemberStatus implements Schedulable, Database.Batchable<sObject> {
	public string query;
	public string totals;
	public string taskStr;

	global void execute(SchedulableContext sc){
    	ba_Campaign_AddCampaignMemberStatus oBatch = new ba_Campaign_AddCampaignMemberStatus();
        Database.executebatch(oBatch, 200);        	
    } 
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		this.query =  'Select Id,';
 		this.query += '	   FollowUpTask_Created__c,'; 
  		this.query += '	   CampaignId,';
		this.query += '	   CreatedById,';
		this.query += '	   ContactId,';
		this.query += '	   LeadId';
		this.query += ' From CampaignMember';
		this.query += ' Where Campaign.Type = \'Training & Education\'';
		this.query += '   And Campaign.Automatically_create_follow_up_tasks__c = true';
		this.query += '   And Campaign.Status not in (\'Completed\', \'Aborted\', \'Cancelled\')';
		this.query += '   And Campaign_Member_Type__c = \'Participant\'';
		this.query += '   And FollowUpTask_Created__c = false';
		
		return Database.getQueryLocator(query);
	}
    
	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		List<CampaignMember> cmemList = (List<CampaignMember>) scope;
system.debug('### cmemList.size() = ' + cmemList.size());		
/*		
		Map<Id, CampaignMember> cmemMap = new Map<Id, CampaignMember>([Select Id,
																			  FollowUpTask_Created__c,
																			  CampaignId,
																			  CreatedById,
																			  ContactId,
																			  LeadId
																	     From CampaignMember
																	    Where Campaign.Type = 'Training & Education'
																	      And Campaign.Automatically_create_follow_up_tasks__c = true
																	      And Campaign.Status not in ('Completed', 'Aborted', 'Cancelled')
																	      And Campaign_Member_Type__c = 'Participant'
																	      And FollowUpTask_Created__c = false]);
*/																	      
		bl_CampaignMember.createFollowUpTask(cmemList);		
		
		List<CampaignMember> cupdList = new List<CampaignMember>();
		this.taskStr = '';
		For(CampaignMember cUpd : cmemList)	{
			cUpd.FollowUpTask_Created__c = true;
			cupdList.add(cUpd);
			taskStr += cUpd.id + '<br />';
		}
system.debug('### cupdList.size() = ' + cupdList.size());		
		this.totals = cupdList.size() + ' tasks created';
		update cupdList;																				      
	}		
	
	global void finish(Database.BatchableContext BC) {
			String emailBody = 'Dear ' + USerInfo.getFirstName() + ' ' + UserInfo.getLastName() + ',<br/><br/>The following tasks where created successfully:<br/><br/>' + this.taskStr;
			Messaging.Singleemailmessage mail = new Messaging.Singleemailmessage();
			mail.setHtmlBody(emailBody);
			mail.setTargetObjectId(UserInfo.getUserId());
			mail.setSubject(this.totals + ' successfully');
			mail.setSaveAsActivity(false);
			
			Messaging.sendEmail(new Messaging.Singleemailmessage[] { mail });	 	
	}
    
}