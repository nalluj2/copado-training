global without sharing class ctrl_Schedule_Activity {
	
	private static List<String> weekDays = new List<String>{'MON','TUE','WED','THU','FRI', 'SAT', 'SUN'};
	private static List<String> months = new List<String>{'January','February','March','April','May','June','July','August','September','October','November','December'};
	
	@AuraEnabled
    public static CalendarView getTeamAvailability(String activityId, Decimal width, Boolean includeTravelTime) {
        
        if(width < 900) width = 900;
        
        Case selectedActivity = [Select Id, Type, AccountId, Account.Name, Account.BillingCity, Procedure__c, Activity_Type_Picklist__c, Experience_of_the_Implanter__c, Product_Group__r.Name, Product_Group__r.Therapy_ID__c, Subject, Description, Start_of_Procedure__c, Procedure_Duration_Implants__c, End_of_Procedure__c, Activity_Scheduling_Team__c, Activity_Scheduling_Team__r.Name,
        (Select Attendee__c from Activity_Scheduling_Attendees__r) from Case where Id = :activityId];
        
        CalendarView result = new CalendarView();
        result.activity = selectedActivity;
        
        Implant_Scheduling_Team__c selectedTeam = [Select Work_Day_Start__c, Work_Day_End__c, Working_Days__c, (Select Member__c, Member__r.Name from Team_Members__r ORDER BY Member__r.Name) from Implant_Scheduling_Team__c where Id = :selectedActivity.Activity_Scheduling_Team__c];
        
        List<TeamMember> members = new List<TeamMember>();
     	Map<String, TeamMember> memberMap = new Map<String, TeamMember>();
     	
     	Integer counter = 0;
     	
     	for(Implant_Scheduling_Team_Member__c teamMember : selectedTeam.Team_Members__r){
     		
     		TeamMember member = new TeamMember(teamMember.Member__c, teamMember.Member__r.Name);  	
     		member.className = Math.mod(counter++, 2) == 0 ? 'row-even' : 'row-odd';	
     		member.height = 45;
     		members.add(member);
     		memberMap.put(teamMember.Member__c, member);
     	}
     	
     	result.members = members;
     	
     	Map<Id, String> memberAddress = new Map<Id, String>();
     	
     	for(Activity_Scheduling_User_Settings__c userSettings : [Select Country__c, Post_Code__c, OwnerId from Activity_Scheduling_User_Settings__c where OwnerId IN :memberMap.keySet()]){
     		
     		memberAddress.put(userSettings.OwnerId, userSettings.Post_Code__c + ' ' + userSettings.Country__c);
     	}
     	
        Date startDate = selectedActivity.Start_of_Procedure__c.date();
        if(selectedActivity.Start_of_Procedure__c.hour() <= 8) startDate = startDate.addDays(-1);        
        
        Date endDate = selectedActivity.End_of_Procedure__c.date();
        if(selectedActivity.End_of_Procedure__c.hour() >= 20) endDate = endDate.addDays(1);
        
        DateTime startDateTime = DateTime.newInstance(startDate, Time.newInstance(0, 0, 0, 0));
     	DateTime endDateTime = DateTime.newInstance(endDate, Time.newInstance(23, 59, 59, 0));
     	
        List<Event> eventList = [Select Subject, WhatId, Id, StartDateTime, EndDateTime, IsAllDayEvent, OwnerId from Event where OwnerId IN :memberMap.keySet() AND StartDateTime < :endDateTime AND EndDateTime >= :startDateTime AND isRecurrence = false AND WhatId != :activityId ORDER BY StartDateTime];
        
        Integer teamStartHour = Integer.valueOf(selectedTeam.Work_Day_Start__c);
        Integer teamEndHour = Integer.valueOf(selectedTeam.Work_Day_End__c);
        
        Integer firstDay = Integer.valueOf(selectedTeam.Working_Days__c.split(':')[0]);
        Integer numberWorkingDays = Integer.valueOf(selectedTeam.Working_Days__c.split(':')[1]);
        
        Set<Integer> teamWorkingDays = new Set<Integer>();
        
        for(Integer i = 0; i < numberWorkingDays; i++){
        	
        	if(firstDay > 7) teamWorkingDays.add(firstDay - 7);
        	else teamWorkingDays.add(firstDay);
        	
        	firstDay ++;
        } 
        
        System.debug('teamWorkingDays: ' + teamWorkingDays);
        
        Integer earliestStartHour = teamStartHour;
        Integer latestEndHour = teamEndHour;
                     	     		
     	Set<Id> caseIds = new Set<Id>();
     	
     	for(Event event : eventList){
        	
        	if(event.WhatId != null && String.valueOf(event.whatId).startsWith('500')) caseIds.add(event.WhatId);
    	}
     	
     	Map<Id, Case> cases = new Map<Id, Case>();
     	
     	if(caseIds.size() > 0){
     		
     		cases = new Map<Id, Case>([Select Id, Type, Subject,  Account.Name, Account.BillingCity, Procedure__c, Product_Group__r.Name, Activity_Type_Picklist__c, Activity_Scheduling_Team__r.Name, Activity_Scheduling_Team__r.Team_Color__c from Case where Id IN :caseIds]);
     	}
     	
     	Map<Id, Event> memberLastPrevEvent = new Map<Id, Event>();
     	Map<Id, Event> memberFirstNexEvent = new Map<Id, Event>();
     	        
        for(Event event : eventList){
        	
        	if(event.WhatId != null && String.valueOf(event.whatId).startsWith('500') && cases.get(event.WhatId).Type != 'Meeting'){
        		
	        	Integer eventStart;
	        	Integer eventEnd;
	        	
	        	Integer eventDays = event.StartDateTime.date().daysBetween(event.EndDateTime.date());
	        	
	        	if(eventDays == 0){
	        	
		        	eventStart = event.StartDateTime.hour();
		        	eventEnd = event.EndDateTime.minute() > 0 ? event.EndDateTime.hour() + 1 : event.EndDateTime.hour();
		        	
	        	}else{
	        		
	        		if(event.StartDateTime.date() < endDateTime.date()) eventStart = 0;
	        		else eventStart = event.StartDateTime.hour();
	        			        		
	        		if(event.EndDateTime.date() > startDateTime.date()) eventEnd = 24;	        			
	        		else eventEnd = event.EndDateTime.minute() > 0 ? event.EndDateTime.hour() + 1 : event.EndDateTime.hour();
	        	}	
	        	
	        	if(eventStart < earliestStartHour) earliestStartHour = eventStart;
	        	if(eventEnd > latestEndHour) latestEndHour = eventEnd;
	        	
	        	if(event.EndDateTime <= selectedActivity.Start_of_Procedure__c){
	        		
	        		Event prevEvent = memberLastPrevEvent.get(event.OwnerId);
	        		
	        		if(prevEvent == null || prevEvent.EndDateTime < event.EndDateTime) memberLastPrevEvent.put(event.OwnerId, event);
	        	}	
	        	
	        	if(event.StartDateTime > selectedActivity.End_of_Procedure__c){
	        		
	        		Event nexEvent = memberFirstNexEvent.get(event.OwnerId);
	        		
	        		if(nexEvent == null || nexEvent.StartDateTime > event.StartDateTime) memberFirstNexEvent.put(event.OwnerId, event);
	        	}	
        	}
        }
        
        //Get travel times        
        Map<Id, Integer> goTimeMap = new Map<Id, Integer>();
     	Map<Id, Integer> returnTimeMap = new Map<Id, Integer>();
     	
     	if(includeTravelTime == true){
     	
	     	for(TeamMember member : members){
	     		
	     		Integer goTimeMins = Integer.valueOf((Math.random() * 60).round()) + 15;
	     		Integer returnTimeMins = Integer.valueOf((Math.random() * 60).round()) + 15;
	     		
	     		goTimeMap.put(member.Id, goTimeMins);
	     		returnTimeMap.put(member.Id, returnTimeMins);	
	 		}    		
     	}    
            	
    	Integer caseDays = selectedActivity.Start_of_Procedure__c.date().daysBetween(selectedActivity.End_of_Procedure__c.date());
    	
    	for(TeamMember member : members){
    		
    		Integer caseStart;
	    	Integer caseEnd;
    		
    		Integer goTimeMins = 0;
     		Integer returnTimeMins = 0;
     		
     		if(includeTravelTime == true){
     			
     			goTimeMins = goTimeMap.get(member.Id);
	     		returnTimeMins = returnTimeMap.get(member.Id);     			
     		}
    		
	    	if(caseDays == 0){
	    	
	        	caseStart = selectedActivity.Start_of_Procedure__c.addMinutes(-goTimeMins).hour();
	        	caseEnd = selectedActivity.End_of_Procedure__c.addMinutes(returnTimeMins).minute() > 0 ? selectedActivity.End_of_Procedure__c.addMinutes(returnTimeMins).hour() + 1 : selectedActivity.End_of_Procedure__c.addMinutes(returnTimeMins).hour();		        	
	        			        	
	    	}else{
	    		
	    		if(selectedActivity.Start_of_Procedure__c.date() < endDateTime.date()) caseStart = 0;
	    		else caseStart = selectedActivity.Start_of_Procedure__c.addMinutes(-goTimeMins).hour();
	    			        		
	    		if(selectedActivity.End_of_Procedure__c.date() > startDateTime.date()) caseEnd = 24;	        			
	    		else caseEnd = selectedActivity.End_of_Procedure__c.addMinutes(returnTimeMins).minute() > 0 ? selectedActivity.End_of_Procedure__c.addMinutes(returnTimeMins).hour() + 1 : selectedActivity.End_of_Procedure__c.addMinutes(returnTimeMins).hour();
	    	}	
	    	
	    	if(caseStart < earliestStartHour) earliestStartHour = caseStart;
	    	if(caseEnd > latestEndHour) latestEndHour = caseEnd;    	
    	}
    	        
        Time startTime = Time.newInstance(earliestStartHour, 0, 0, 0);
     	Time endTime;
     	if(latestEndHour == 24) endTime = Time.newInstance(23, 59, 59, 0);
     	else endTime = Time.newInstance(latestEndHour, 0, 0, 0); 
        
        startDateTime = DateTime.newInstance(startDate, startTime);     	
     	endDateTime = DateTime.newInstance(endDate, endTime);
                
        List<Hour> hours = new List<Hour>();
        for(Integer i = earliestStartHour; i < latestEndHour; i++){
        	
        	Hour hour = new Hour();
        	hour.hour = i;
                	
        	if(i < teamStartHour || i >= teamEndHour) hour.isOWH = true;
        	else hour.isOWH = false;
        	
        	hours.add(hour);
        } 
        
        Integer numberDays = startDateTime.date().daysBetween(endDateTime.date()) + 1;
                                
        Decimal pixelsPerMinute = width / ((latestEndHour - earliestStartHour) * 60 * numberDays);        
        Integer pixelsPerHour = Integer.valueOf(pixelsPerMinute * 60);
        width = (latestEndHour - earliestStartHour) * pixelsPerHour * numberDays;
        pixelsPerMinute = Decimal.valueOf(pixelsPerHour) / 60;
        
        result.hourWidth = pixelsPerHour;
        result.hours = hours;        
        
        List<Day> days = new List<Day>();
        
        DateTime sampleDate = DateTime.newInstance(startDateTime.date(), Time.newInstance(0,0,0,0));
                
        for(Integer i = 0; i < numberDays; i++){
        	
        	Day weekDay = new Day();
        	weekDay.dayKey = sampleDate.format();
        	
        	Integer dayOfWeek = Integer.valueOf(sampleDate.format('u'));
        	weekDay.dayName = weekDays[dayOfWeek - 1];
        	weekDay.day = sampleDate.day();
        	weekDay.month = months[sampleDate.month() - 1];
        	weekDay.year = sampleDate.year();
        	
        	if(teamWorkingDays.contains(dayOfWeek) == false) weekDay.isOWH = true;
        	else weekDay.isOWH = false;
        	       	
        	days.add(weekDay);
        	
        	sampleDate = sampleDate.addDays(1);        	
        }
        
        result.days = days;
         
     	Map<String, List<CalendarItem>> events = new Map<String, List<CalendarItem>>();
     		
     	for(TeamMember member : members){
     	
     		events.put(member.id, new List<CalendarItem>());
     	}
     	    		
     	for(Event viewEvent : eventList){
     		
     		Case eventCase;     		     		
     		if(viewEvent.WhatId != null && String.valueOf(viewEvent.whatId).startsWith('500')) eventCase = cases.get(viewEvent.WhatId);
     		    			
	     	List<CalendarItem> dayMemberEvents = events.get(viewEvent.OwnerId);   
	     		     		
		    CalendarItem calItem = new CalendarItem();
	     	calItem.id = viewEvent.Id;	     	
	     		     		
	     	if(eventCase == null){
	     			
	     		calItem.subject = viewEvent.subject;
	     		calItem.teamColor = 'rgb(' + ctrl_Activity_Scheduling_App.timeOffColor + ')';
	     		calItem.color = 'rgba(' + ctrl_Activity_Scheduling_App.timeOffColor + ', 0.8)';
	     		calItem.teamName = 'Time-off';
	     		
	     		if(viewEvent.IsAllDayEvent == true){
	     			
	     			viewEvent.StartDateTime = DateTime.newInstance(viewEvent.StartDateTime.date(), Time.newInstance(0, 0, 0, 0));
	 				viewEvent.EndDateTime = DateTime.newInstance(viewEvent.EndDateTime.date().addDays(1), Time.newInstance(0, 0, 0, 0));
	     		}
	     			
	     	}else{
	     		
	     		calItem.caseId = viewEvent.WhatId;	
 				calItem.teamName = eventCase.Activity_Scheduling_Team__r.Name;
 				calItem.teamColor = eventCase.Activity_Scheduling_Team__r.Team_Color__c;
 				calItem.subject = (eventCase.Type == 'Meeting' ? eventCase.Subject : eventCase.Account.Name);
 				calItem.location = (eventCase.AccountId != null ? eventCase.Account.BillingCity : null);
 					     					
 				if(eventCase.Type == 'Implant Support Request'){
 						
 					calItem.color = 'rgba(' + ctrl_Activity_Scheduling_App.implantColor + ', 0.8)';
 					calItem.caseType = 'implant';     	
 					calItem.subject += ' / ' + eventCase.Procedure__c + ' / ' + eventCase.Product_Group__r.Name; 	
 						
 				}else if(eventCase.Type == 'Service Request'){
 						
 					calItem.color = 'rgba(' + ctrl_Activity_Scheduling_App.serviceColor + ', 0.8)';
 					calItem.caseType = 'service';     			
 					calItem.subject += ' / ' + eventCase.Activity_Type_Picklist__c;
 										
 				}else{
 					
 					calItem.color = 'rgba(' + ctrl_Activity_Scheduling_App.meetingColor + ', 0.8)';
 					calItem.caseType = 'meeting';
 				}
	     	}
	     	
	     	calItem.timeframe = getTimeFrame(viewEvent.StartDateTime, viewEvent.EndDateTime);
     		
	     	DateTime start;
	     	if(viewEvent.StartDateTime < startDateTime) start = startDateTime;
	     	else if(viewEvent.StartDateTime.time() < startDateTime.time()) start = DateTime.newInstance(viewEvent.StartDateTime.date(), startDateTime.time());
	     	else start = viewEvent.StartDateTime;
     		     		
	     	Decimal left = (startDateTime.date().daysBetween(start.date()) * (hours.size() * pixelsPerHour + 1)) + ((start.hour() - startTime.hour()) * pixelsPerHour) + (start.minute() * pixelsPerMinute);
	     	calItem.left = Integer.valueOf(left);
	     		
	     	DateTime endT;
	     	if(viewEvent.EndDateTime > endDateTime) endT = endDateTime;
	     	else if(viewEvent.EndDateTime.time() > endDateTime.time()) endT = DateTime.newInstance(viewEvent.EndDateTime.date(), endDateTime.time());
	     	else if(viewEvent.EndDateTime.time() < startDateTime.time()) endT = DateTime.newInstance(viewEvent.EndDateTime.date(), startDateTime.time());
	     	else endT = viewEvent.EndDateTime;
	     		     		     		     			
	     	Decimal eventwidth = (start.date().daysBetween(endT.date()) * (hours.size() * pixelsPerHour + 1)) + ((endT.hour() - start.hour()) * pixelsPerHour) + ((endT.minute() - start.minute()) * pixelsPerMinute); 			
	     	calItem.width = Integer.valueOf(eventwidth)  - 1;     		
	     		
	     	calItem.height = 44;     		
	     	calItem.zIndex = (dayMemberEvents.size() + 1);
	     		
	     	if(calItem.width <= 0 ) continue;
	     		
	     	dayMemberEvents.add(calItem); 
     	}
     	
     	String timeFrame = getTimeFrame(selectedActivity.Start_of_Procedure__c, selectedActivity.End_of_Procedure__c);
     	
     	Map<Id, Integer> prevImplantsMap = new Map<Id, Integer>();
     	
     	if(selectedActivity.Type == 'Implant Support Request'){
	     	
	     	for(AggregateResult userImplants : [Select User_ID__c, count(Id) Total FROM Implant__c Where User_ID__c = :memberMap.keySet() AND Implant_Implanting_Account__c = :selectedActivity.AccountId
			    							And Therapy__c = :selectedActivity.Product_Group__r.Therapy_ID__c And Implant_Date_Of_Surgery__c >= LAST_N_MONTHS:6 AND Attended_Implant_Text__c = :'Attended' Group By User_ID__c]){
	    		
		    	
		    	prevImplantsMap.put((Id)userImplants.get('User_ID__c'), (Integer)userImplants.get('Total'));		    	
			}
     	}
     	
     	Set<Id> attendees = new Set<Id>();
     	for(Activity_Scheduling_Attendee__c attendee : selectedActivity.Activity_Scheduling_Attendees__r) attendees.add(attendee.Attendee__c);
     	
 		for(TeamMember member : members){
     		
     		List<CalendarItem> dayMemberEvents = events.get(member.id);
     		
     		CalendarItem calItem = new CalendarItem();
     		calItem.id = selectedActivity.Id;
     		calItem.caseId = selectedActivity.Id;
     		calItem.subject = member.name;
     		calItem.timeframe = timeFrame;
     		calItem.teamName = selectedActivity.Activity_Scheduling_Team__r.Name;	  
     		calItem.memberId = member.id;
     		calItem.assignmentPlaceholder = true;
     		
     		Integer goTimeMins = 0;
 			Integer returnTimeMins = 0;
 		
 			if(includeTravelTime == true){
 			
     			goTimeMins = goTimeMap.get(member.Id);	     			
     			returnTimeMins = returnTimeMap.get(member.Id);	
     		}	     		
     		
     		DateTime start;
		 	if(selectedActivity.Start_of_Procedure__c.addMinutes(-goTimeMins) < startDateTime) start = startDateTime;
		 	else start = selectedActivity.Start_of_Procedure__c.addMinutes(-goTimeMins);     		     		
		 	
		 	Decimal left = (startDateTime.date().daysBetween(start.date()) * (hours.size() * pixelsPerHour + 1)) + ((start.hour() - startTime.hour()) * pixelsPerHour) + (start.minute() * pixelsPerMinute);
		 			 			 		
		 	DateTime endT;
		 	if(selectedActivity.End_of_Procedure__c.addMinutes(returnTimeMins) > endDateTime) endT = endDateTime;
		 	else endT = selectedActivity.End_of_Procedure__c.addMinutes(returnTimeMins);     
		 			     		     		     			
		 	Decimal eventwidth = (start.date().daysBetween(endT.date()) * hours.size() * pixelsPerHour) + ((endT.hour() - start.hour()) * pixelsPerHour) + ((endT.minute() - start.minute()) * pixelsPerMinute) - 1;
     		
     		if(includeTravelTime == true){
     		
				DateTime eventDateTime = selectedActivity.Start_of_Procedure__c;
 				calItem.goTimeWidth = Integer.valueOf((start.date().daysBetween(eventDateTime.date()) * hours.size() * pixelsPerHour) + ((eventDateTime.hour() - start.hour()) * pixelsPerHour) + ((eventDateTime.minute() - start.minute()) * pixelsPerMinute) - 1);
 				calItem.goTime = timeToString(goTimeMins);	     		
	     		
	     		eventDateTime = selectedActivity.End_of_Procedure__c;
	 			calItem.returnTimeWidth = Integer.valueOf((eventDateTime.date().daysBetween(endT.date()) * hours.size() * pixelsPerHour) + ((endT.hour() - eventDateTime.hour()) * pixelsPerHour) + ((endT.minute() - eventDateTime.minute()) * pixelsPerMinute) - 1);
	 			
	 			calItem.returnTime = timeToString(returnTimeMins);	     		     		
     		}
     		
     		calItem.left = Integer.valueOf(left);    		 			
     		calItem.width = Integer.valueOf(eventwidth);    		
     		calItem.height = 44;     		
     		calItem.zIndex = (dayMemberEvents.size() + 1);			     		
     			     		
     		if(attendees.contains(member.id)){
     			
     			calItem.color = 'rgba(200, 200, 200, 0.75)';
     			calItem.isAttendee = true;
     			
     		}else{
     				     				     		
     			calItem.color = 'rgba(255, 248, 107, 0.75)';
     			calItem.isAttendee = false;
     		}
     		
     		if(selectedActivity.Type == 'Implant Support Request' && attendees.contains(member.id) == false){
     			
     			calItem.prevImplants = prevImplantsMap.get(member.id);
     			if(calItem.prevImplants == null) calItem.prevImplants = 0;
     		}
     				     			
     		dayMemberEvents.add(calItem);
 		}
 		
     	 	
     	for(String dayMember : events.keySet()){
     		
     		List<CalendarItem> dayMemberItems = events.get(dayMember);
     		dayMemberItems.sort();
     		TeamMember member = memberMap.get(dayMember);
     		
     		if(dayMemberItems.size() == 0) continue;
     		
     		if(hasOverlaps(dayMemberItems)){
	     		
	     		Integer maxOverlaps = 0;
	     		
	     		for(Integer i = 0; i < width; i++){
	     		
	     			List<CalendarItem> pixelItems = getOverlaps(i, dayMemberItems);
	     		
	     			if(pixelItems.size() > maxOverlaps) maxOverlaps = pixelItems.size();     			     		
	     		}
	     		
	     		Integer dayHeight = (maxOverlaps * 45);	     		
	     		if(dayHeight > member.height) member.height = dayHeight;
	     		
	     		for(Integer i = 0; i < width; i++){
     		
	     			List<CalendarItem> pixelItems = getOverlaps(i, dayMemberItems);
	     			
	     			Set<Integer> usedSlots = new Set<Integer>();
	     			
	     			for(CalendarItem item : pixelItems){
	     				
	     				if(item.top == null){
	     					
	     					Integer top = 0;
	     					if(dayMember != members[0].id) top = 1;
	     					
	     					while(true){
	     						
	     						if(usedSlots.contains(top) == false){
	     							
	     							item.top = top;
	     							usedSlots.add(top);
	     							break;
	     						}
	     						
	     						top += 45;
	     					}
	     					
	     				}else{
	     					     					     						
	     					usedSlots.add(item.top);    					
	     				}
	     				
	     				if(pixelItems.size() > 1 && item.assignmentPlaceholder == true){
	     				
	     					item.hasOverlap = true;
	     					item.color = 'rgba(255, 156, 69, 0.75)';
	     				}
	     			}     		
	     		}     		
	     		
     		}else{
     			     			
     			for(CalendarItem item : dayMemberItems){
     				
     				if(dayMember != members[0].id) item.top = 1;
     				else item.top = 0;
     			}
     		}
     	}
     	
     	result.events = events;
     	
     	return result;   
    }
    
    private static String getTimeFrame(DateTime startDT, DateTime endDT){
    	
    	if(endDT.Year() != startDT.Year()){
    		
    		return startDT.format('dd MMM yyyy H:mm') + ' - ' + endDT.format('dd MMM yyyy H:mm');
    		
    	}else if(endDT.Month() != startDT.Month() || endDT.Day() != startDT.Day()){
    		
    		return startDT.format('dd MMM H:mm') + ' - ' + endDT.format('dd MMM H:mm');
    		
    	}else{
    		
    		return startDT.format('H:mm') + ' - ' + endDT.format('H:mm');
    	}
    }
    
    private static String timeToString(Integer timeMins){
    	
    	String hours = String.valueOf(timeMins / 60);
    	if(hours.length() == 1) hours = '0'+hours;
    	
    	String mins = String.valueOf(Math.mod(timeMins, 60));
    	if(mins.length() == 1) mins = '0'+mins;
    	
    	return hours + ':' + mins;
    }
    
    private static Boolean hasOverlaps(List<CalendarItem> items){
    	
    	for(Integer i = 0; i < items.size(); i++){
    		
    		CalendarItem item = items[i];
    		
    		for(Integer j = (i + 1); j < items.size(); j++){
    				
				CalendarItem otherItem = items[j];
    				
				if(otherItem.left <= (item.left + item.width) && (otherItem.left + otherItem.width) >= item.left) return true;
    		}	    		
    	}
    	
    	return false;
    }
    
    private static List<CalendarItem> getOverlaps(Integer pixel, List<CalendarItem> items){
    	
    	List<CalendarItem> pixelItems = new List<CalendarItem>();
    	
    	for(CalendarItem item : items){
    		
    		if(pixel >= item.left && pixel <= (item.left + item.width)){
    			
    			pixelItems.add(item);
    		}    		
    	}
    	
    	return pixelItems;
    }
        
    public class CalendarView {
    	
    	@AuraEnabled public Case activity {get; set;}
    	@AuraEnabled public Date selectedDay {get; set;}
    	@AuraEnabled public Integer hourWidth {get; set;}
    	@AuraEnabled public List<Hour> hours {get; set;}
    	@AuraEnabled public List<Day> days {get; set;}	    	
    	@AuraEnabled public List<TeamMember> members {get; set;}
    	@AuraEnabled public Map<String, List<CalendarItem>> events {get; set;}
    }
    
    
    public class TeamMember {
    	
    	@AuraEnabled public String id { get; set; }
    	@AuraEnabled public String name { get; set; }
    	@AuraEnabled public String className { get; set; }
    	@AuraEnabled public Integer height { get; set; }
    	
    	public TeamMember(String inputId, String inputName){
    		
    		id = inputId;
    		name = inputName;    		
    	}
    }
    
    public class Day {
    	
    	@AuraEnabled public String dayKey { get; set; }
    	@AuraEnabled public String dayName { get; set; }
    	@AuraEnabled public Integer day { get; set; }
    	@AuraEnabled public String month { get; set; }
    	@AuraEnabled public Integer year { get; set; }
    	@AuraEnabled public Boolean isOWH { get; set; }
    }
    
    public class Hour {
    	
    	@AuraEnabled public Integer hour { get; set; }
    	@AuraEnabled public Boolean isOWH { get; set; }
    }
    
    global class CalendarItem implements Comparable{
    	
    	@AuraEnabled public String id { get; set; }
    	@AuraEnabled public String subject { get; set; }
    	@AuraEnabled public String timeframe { get; set; }
    	@AuraEnabled public String location { get; set; }
    	@AuraEnabled public Integer width { get; set; }
    	@AuraEnabled public Integer top { get; set; }
    	@AuraEnabled public Integer left { get; set; }
    	@AuraEnabled public Integer height { get; set; }
    	@AuraEnabled public Integer zIndex { get; set; }
    	@AuraEnabled public Integer goTimeWidth { get; set; }
    	@AuraEnabled public String goTime { get; set; }
    	@AuraEnabled public Integer returnTimeWidth { get; set; }
    	@AuraEnabled public String returnTime { get; set; }
    	@AuraEnabled public String color { get; set; }
    	@AuraEnabled public String teamName { get; set; }
    	@AuraEnabled public String teamColor { get; set; }
    	@AuraEnabled public Integer maxOverlap { get; set; }
    	@AuraEnabled public String caseId { get; set; }
    	@AuraEnabled public String caseType { get; set; }
    	@AuraEnabled public Boolean assignmentPlaceholder { get; set; }
    	@AuraEnabled public Boolean isAttendee { get; set; }
    	@AuraEnabled public Boolean hasOverlap { get; set; }
    	@AuraEnabled public Integer prevImplants { get; set; }
    	@AuraEnabled public String memberId { get; set; }
    	
    	global Integer compareTo(Object compareTo) {
			
			CalendarItem other = (CalendarItem) compareTo;
			
			if (left == other.left) return 0;
        	if (left > other.left) return 1;
        	return -1;
		}    	
    }        
}