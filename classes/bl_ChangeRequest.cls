/*
 * 		Author 		:	Bart Caelen
 *		Date		: 	2014-02
 * 		Description	:	This APEX Class contains the Business Logic for processing Change_Request__c records
 * 		version		: 	1.0
 */
public with sharing class bl_ChangeRequest {
	
	//------------------------------------------------------------------------------------------------------------
	// PROCESS DATA - Called from the trigger
	//------------------------------------------------------------------------------------------------------------
	public static void processData(list<Change_Request__c> lstChangeRequest, set<id> setID_ChangeRequest, set<id> setID_ProcessArea, string tType){

		// Check if there are other PLANNED CR's withing the same Process Area
		map<id, Process_Area__c> mapProcessArea = new map<id, Process_Area__c>
			( 
				[
					SELECT
						ID
						, (
							SELECT ID, Project__c, Change_Request__c, Status__c, Change_Weight__c 
							FROM Change_Requests__r 
							WHERE 
								ID not in :setID_ChangeRequest
								AND Status__c in  ('Planned', 'Completed')
						)
						, (
							SELECT ID, Type__c, Test_User__c, Parameters__c, Process_Area__r.OwnerId
							FROM Tests__r
						)
					FROM
						Process_Area__c
					WHERE 
						ID = :setID_ProcessArea
				]
			);
			
		if ( (tType == 'PLANNED_TO_NOT_COMPLETED') || (tType == 'DELETE') ){
			processData_Delete(lstChangeRequest, mapProcessArea);
		}else if ( (tType.toUpperCase() == 'ADD_MINOR') || (tType.toUpperCase() == 'ADD_MAJOR') ){
			processData_Insert(lstChangeRequest, mapProcessArea, tType);
		}
	}
	//------------------------------------------------------------------------------------------------------------

	
	//------------------------------------------------------------------------------------------------------------
	// PROCESS DELETE
	//------------------------------------------------------------------------------------------------------------
	private static void processData_Delete(list<Change_Request__c> lstChangeRequest, map<id, Process_Area__c> mapProcessArea){

		set<id> setReleaseID_DeleteReleaseTest = new set<id>();
		map<id, list<Test__c>> mapReleaseID_Tests = new map<id, list<Test__c>>();
		
		set<id> setID_Release = new set<id>();
		for (Change_Request__c oChangeRequest : lstChangeRequest){
			setID_Release.add(oChangeRequest.Project__c);
		}
			
		for (Change_Request__c oChangeRequest : lstChangeRequest){

			if (oChangeRequest.Project__c != null){	// -BC - 20150513 - Added to make sure we only delete release test records if there is an release

				// Get the Process Area of the Change Request that needs processing				
				Process_Area__c oProcessArea = mapProcessArea.get(oChangeRequest.Process_Area__c);

				integer iCounter_CR_Major = 0;
				integer iCounter_CR_Minor = 0;
				for (Change_Request__c oCR : oProcessArea.Change_Requests__r){
					if ( setID_Release.contains(oCR.Project__c) ){
						if (oCR.Change_Weight__c == 'Major'){
							iCounter_CR_Major++;
						}else if (oCR.Change_Weight__c == 'Minor'){
							iCounter_CR_Minor++;
						}
					}
				}

				// Collect the data that will be deleted	
				if ( (iCounter_CR_Major == 0) && (iCounter_CR_Minor == 0) ){

					// Delete all Release Tests for this Release
					mapReleaseID_Tests.put(oChangeRequest.Project__c, oProcessArea.Tests__r);

				}else if ( (iCounter_CR_Major == 0) && (iCounter_CR_Minor != 0) ){

					// Delete the Optional Release Test
					list<Test__c> lstTest = new list<Test__c>();
					for (Test__c oTest : oProcessArea.Tests__r){
						if (oTest.Type__c == 'Optional'){
							lstTest.add(oTest);
						}
					}
						
					if (lstTest.size()>0){
						mapReleaseID_Tests.put(oChangeRequest.Project__c, lstTest);
					}
				}
			}
		}

		if (mapReleaseID_Tests.size() > 0){
			// Create the SOQL to select all records that need to be deleted
			// This is a dynamic SOQL bcause we need to select Release_Test__c records filtered on 
			//	one or more Release which can have one ore more Tests that will result in a list of records that we need to delete
			String tSOQL_SELECT = 'SELECT ID FROM Release_Test__c';
			String tSOQL_WHERE = '';
			for (Id idRelease : mapReleaseID_Tests.keySet()){
				String tTest_WHERE = '';
				for (Test__c oTest : mapReleaseID_Tests.get(idRelease)){
					if (tTest_WHERE != ''){
						tTest_WHERE += ' OR ';
					}
					tTest_WHERE += ' ( Test__r.Id = \'' + oTest.Id + '\' )';
				}

				if (tSOQL_WHERE != ''){
					tSOQL_WHERE += ' OR ';
				}
				tSOQL_WHERE += ' (';
					tSOQL_WHERE += ' Release__c = \'' + idRelease + '\'';
					if (tTest_WHERE != ''){
						tSOQL_WHERE += ' AND ( ' + tTest_WHERE + ' ) ';
					}
				tSOQL_WHERE += ')';
			}
			

			tSOQL_WHERE = ' WHERE ' + tSOQL_Where;
			
			clsUtil.debug('processData_Delete - tSOQL_SELECT + tSOQL_WHERE : ' + tSOQL_SELECT + tSOQL_WHERE);
			list<Release_Test__c> lstReleaseTest_Delete = database.query(tSOQL_SELECT + tSOQL_WHERE);


			if (lstReleaseTest_Delete.size() > 0){
				// DELETE the Release_Test_Step__c and Release_Test__c records
				deleteData_ReleaseTest_ReleaseTestStep(lstReleaseTest_Delete);			
			}
		}
	}
	//------------------------------------------------------------------------------------------------------------


	//------------------------------------------------------------------------------------------------------------
	// PROCESS INSERT
	//------------------------------------------------------------------------------------------------------------
	private static void processData_Insert(list<Change_Request__c> lstChangeRequest, map<id, Process_Area__c> mapProcessArea, string tType){

		map<id, list<Test__c>> mapReleaseID_Tests = new map<id, list<Test__c>>();
		
		for (Change_Request__c oChangeRequest : lstChangeRequest){
			Process_Area__c oProcessArea = mapProcessArea.get(oChangeRequest.Process_Area__c);

			list<Test__c> lstTest = new list<Test__c>();
			for (Test__c oTest : oProcessArea.Tests__r){
				if (tType.toUpperCase() == 'ADD_MINOR'){
					// Only create the Mandatory Tests
					if (oTest.Type__c == 'Mandatory'){
						lstTest.add(oTest);
					}
				}else if (tType.toUpperCase() == 'ADD_MAJOR'){
					// Create the Mandatory & Optional Tests
					lstTest.add(oTest);
				}
			}

			if (lstTest.size()>0){
				mapReleaseID_Tests.put(oChangeRequest.Project__c, lstTest);
			}
		}
			
		// INSERT the Release_Test__c and Release_Test_Step__c data
		insertData_ReleaseTest_ReleaseTestStep(mapReleaseID_Tests, mapProcessArea);
	}
	//------------------------------------------------------------------------------------------------------------


	//------------------------------------------------------------------------------------------------------------
	// PROCESS RELEASE TEST STEPS
	//------------------------------------------------------------------------------------------------------------
	private static void processData_ReleaseTestStep(list<Release_Test__c> lstReleaseTest, string tType){

		if (tType == 'ADD'){

			set<id> setID_Test = new set<id>();
			map<id, id> mapTestID_ReleaseTestID = new map<id, id>();
			for (Release_Test__c oReleaseTest : lstReleaseTest){
				mapTestID_ReleaseTestID.put(oReleaseTest.Test__c, oReleaseTest.id);
			}
			
			list<Test__c> lstTest = 
				[
					SELECT 
						ID
						, (SELECT ID, Name, Description__c, Expected_Result__c, Step__c FROM Test_Steps__r)
					FROM Test__c
					WHERE ID = :mapTestID_ReleaseTestID.keySet()
				];
			
			
			list<Release_Test_Step__c> lstReleaseTestStep = new list<Release_Test_Step__c>(); 
			for (Test__c oTest : lstTest){
				for (Test_Step__c oTestStep : oTest.Test_Steps__r){
					Release_Test_Step__c oRTS = new Release_Test_Step__c();
						oRTS.Name = oTestStep.Name;
						oRTS.Release_Test__c = mapTestID_ReleaseTestID.get(oTest.Id);
						oRTS.Description__c = oTestStep.Description__c;
						oRTS.Expected_Results__c = oTestStep.Expected_Result__c;
						oRTS.Step__c = oTestStep.Step__c;
					lstReleaseTestStep.add(oRTS);
				}
			}
			
			if (lstReleaseTestStep.size() > 0){
				insert lstReleaseTestStep;
			}
				
		}else if (tType == 'DELETE'){
			
			list<Release_Test_Step__c> lstReleaseStepTest = 
				[
					SELECT ID
					FROM Release_Test_Step__c
					WHERE Release_Test__c = :lstReleaseTest
				];
				
			if (lstReleaseStepTest.size() > 0){
				delete lstReleaseStepTest;
			}

		}
	}
	
	private static void deleteData_ReleaseTest_ReleaseTestStep(list<Release_Test__c> lstReleaseTest_Delete){
		
		// Create a savepoint
		Savepoint oSavePoint = Database.setSavepoint();
		try{
			if (lstReleaseTest_Delete.size() > 0){
				// First Delete the Release Test Step Records
				processData_ReleaseTestStep(lstReleaseTest_Delete, 'DELETE');
				// Delete the Release Test Records
				delete lstReleaseTest_Delete;
			}
		}catch(Exception oERR){
			database.rollback(oSavePoint);
			throw new clsUtil.cException('An error occured on line ' + oERR.getLineNumber() + ' : ' + oERR.getMessage());
		}
		
	}

	
	private static void insertData_ReleaseTest_ReleaseTestStep(map<id, list<Test__c>> mapReleaseID_Tests, map<id, Process_Area__c> mapProcessArea){

		// DO NOT INSERT THE RELEASE TEST IF THERE IS ALREADY A RECORD WITH THE SAME Release__c / Test__c COMBINATION
		set<string> setReleaseIDAndTestID_Existing = getExistingReleaseTest_TestID_And_ReleaseID(mapProcessArea);

		list<Release_Test__c> lstReleaseTest_Insert = new list<Release_Test__c>();
		for (id idRelease : mapReleaseID_Tests.keySet()){
			for (Test__c oTest : mapReleaseID_Tests.get(idRelease)){
				string tKey = clsUtil.convertSFDCID(idRelease) + '_' + clsUtil.convertSFDCID(oTest.Id);
				if (!setReleaseIDAndTestID_Existing.contains(tKey)){
					Release_Test__c oReleaseTest = new Release_Test__c();
						oReleaseTest.Release__c = idRelease;
						oReleaseTest.Test__c = oTest.Id;
						oReleaseTest.Test_User__c = oTest.Test_User__c;
						oReleaseTest.Parameters__c = oTest.Parameters__c;
						oReleaseTest.Tester__c = oTest.Process_Area__r.OwnerId;
					lstReleaseTest_Insert.add(oReleaseTest);
					setReleaseIDAndTestID_Existing.add(clsUtil.convertSFDCID(oReleaseTest.Release__c) + '_' + clsUtil.convertSFDCID(oReleaseTest.Test__c));
				}
			}
		}

		// Create a savepoint
		Savepoint oSavePoint = Database.setSavepoint();
		try{
			if (lstReleaseTest_Insert.size() > 0 ){
				// ADD the Release Test records
				insert lstReleaseTest_Insert;
								
				// ADD the Release Test Step records		
				processData_ReleaseTestStep(lstReleaseTest_Insert, 'ADD');
			}
		}catch(Exception oERR){
			database.rollback(oSavePoint);
			throw new clsUtil.cException('An error occured on line ' + oERR.getLineNumber() + ' : ' + oERR.getMessage());
		}		
	}
	//------------------------------------------------------------------------------------------------------------



	private static set<string> getExistingReleaseTest_TestID_And_ReleaseID(map<id, Process_Area__c> mapProcessArea){

		set<id> setID_Test = new set<id>();
		set<id> setID_Release = new set<id>();		
		for (id idProcessArea : mapProcessArea.keyset()){
			for (Test__c oTest : mapProcessArea.get(idProcessArea).Tests__r){
				setID_Test.add(oTest.Id);
			}
			for (Change_Request__c oChangeRequest : mapProcessArea.get(idProcessArea).Change_Requests__r){
				setID_Release.add(oChangeRequest.Project__c);
			}
		}
		
		list<Release_Test__c> lstReleaseTest = 
			[
				SELECT
					ID, Test__c, Release__c
				FROM
					Release_Test__c
				WHERE 
					Test__c = :setID_Test
					AND
					Release__c = :setID_Release
			];
		
		set<string> setReleaseIDAndTestID_Existing = new set<string>();
		for (Release_Test__c oReleaseTest : lstReleaseTest){
			setReleaseIDAndTestID_Existing.add(clsUtil.convertSFDCID(oReleaseTest.Release__c) + '_' + clsUtil.convertSFDCID(oReleaseTest.Test__c));
		}
		
		return setReleaseIDAndTestID_Existing;
	}	


	//----------------------------------------------------------------------------------------------------------------
	// Calculate the Total Cost on Change Request Bundle which is the sum of the Total Cost of each relate Change Request.
	//	The Total Cost of the relate Change Request will be converted to the Currency of the Change Request Bundle.
	//----------------------------------------------------------------------------------------------------------------
	public static List<Change_Request_Bundle__c> calculateTotalCostOnBundle(Set<Id> setID_ChangeRequestBundle, Boolean bDoUpdate){
	
		Set<String> setStatus_Excluded = new Set<String>{'Rejected', 'Deferred', 'Closed - duplicate', 'Closed - out of scope', 'Closed - resolved', 'Closed - workaround', 'On Hold', 'Closed'};

		List<Change_Request_Bundle__c> lstChangeRequestBundle_Update = new List<Change_Request_Bundle__c>();
	
		if (setID_ChangeRequestBundle.size() > 0){

			// Get all Change Requests of the collected Change Request Bundles and calculate the total cost per Change Request Bundle
			List<Change_Request__c> lstChangeRequest_Sum = 
				[
					SELECT Id, Change_Request_Bundle__c, Change_Request_Bundle__r.CurrencyIsoCode, Total_Cost__c, CurrencyIsoCode 
					FROM Change_Request__c 
					WHERE 
						Change_Request_Bundle__c in :setID_ChangeRequestBundle
						AND Status__c not in :setStatus_Excluded
						AND Total_Cost__c != null AND Total_Cost__c != 0
					ORDER BY Change_Request_Bundle__c
				];

			Map<Id, Decimal> mapChangeRequestBundle_TotalCost = new Map<Id, Decimal>();
			for (Change_Request__c oChangeRequest : lstChangeRequest_Sum){
		
				Decimal decTotalCost = 0;
				if (mapChangeRequestBundle_TotalCost.containsKey(oChangeRequest.Change_Request_Bundle__c)){
					decTotalCost = mapChangeRequestBundle_TotalCost.get(oChangeRequest.Change_Request_Bundle__c);
				}
				decTotalCost += clsUtil.convertCurrency(oChangeRequest.CurrencyIsoCode, oChangeRequest.Change_Request_Bundle__r.CurrencyIsoCode, oChangeRequest.Total_Cost__c);
				mapChangeRequestBundle_TotalCost.put(oChangeRequest.Change_Request_Bundle__c, decTotalCost);

			}
			for (Id id_ChangeRequestBundle : setID_ChangeRequestBundle){

				Change_Request_Bundle__c oChangeRequestBundle = new Change_Request_Bundle__c(Id = id_ChangeRequestBundle);	
					oChangeRequestBundle.Total_Cost__c = 0;
					if (mapChangeRequestBundle_TotalCost.containsKey(id_ChangeRequestBundle)){
						oChangeRequestBundle.Total_Cost__c = mapChangeRequestBundle_TotalCost.get(id_ChangeRequestBundle);
					}
				lstChangeRequestBundle_Update.add(oChangeRequestBundle);
		
			}
			if (bDoUpdate && lstChangeRequestBundle_Update.size() > 0) update lstChangeRequestBundle_Update;

		}

		return lstChangeRequestBundle_Update;

	}
	//----------------------------------------------------------------------------------------------------------------

}