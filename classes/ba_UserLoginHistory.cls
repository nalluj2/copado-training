/*
 *      Created Date : 20140313
 *      Description : Batch to process Login History 
 * 
 *      Author = Rudy De Coninck
 */

global class ba_UserLoginHistory implements Schedulable, Database.Batchable<SObject>{

    public String tQuery = 'SELECT ApiType,ApiVersion,Application,Browser,ClientVersion,Id,LoginTime,LoginType,LoginUrl,Platform,SourceIp,Status,UserId FROM LoginHistory where LoginTime >= LAST_WEEK';
    public Integer iBatchSize = 200;

    //default constructor
    global ba_UserLoginHistory(){
        if (Test.isRunningTest()){
//            iBatchSize = 1;
            tQuery += ' LIMIT ' + iBatchSize;
        }
    }
    
    global ba_UserLoginHistory(String atQuery, Integer aiBatchSize){
        tQuery = atQuery;
        iBatchSize = aiBatchSize;
    }   
   
   
    global void execute(SchedulableContext ctx){        
        // Start Batch Apex job        
        Database.executeBatch(new ba_UserLoginHistory(), iBatchSize);    
    } 
        

    global Database.QueryLocator start(Database.BatchableContext ctx){    
        return Database.getQueryLocator(tQuery);         
    }

    global void execute(Database.BatchableContext ctx, List<SObject> records){
        
        List<LoginHistory> recordsToProcess = (List<LoginHistory>)records;
        List<Application_Usage_Detail__c> appUsageDetails = new List<Application_Usage_Detail__c>();
        for (LoginHistory lh : recordsToProcess){
        	
        	// We only want to store some logins and do filtering here as filtering cannot be done in SOQL
        	
        	boolean include = false;
            wr_LoginHistory oLoginHistory = new wr_LoginHistory(lh.Status, lh.LoginType, lh.Application);
            String tSource = bl_LoginHistory.tGetSource(oLoginHistory);
            if (!String.isBlank(tSource)) include = true;

            if ( (include == false) && (Test.isRunningTest()) ){
                include = true;
                tSource = 'SFDC';
            }

        	if (include){
	        	Application_Usage_Detail__c detail = new Application_Usage_Detail__c(); 
	        	detail.Action_DateTime__c = lh.LoginTime;
	        	Date actionDate = date.newinstance(lh.LoginTime.year(), lh.LoginTime.month(), lh.LoginTime.day());
	        	detail.Action_Date__c = actionDate;
	        	detail.Action_Type__c = 'Read';
	        	detail.Capability__c = 'Login';
	        	detail.External_Record_Id__c = lh.id;
	        	detail.Internal_Record_Id__c = lh.id; 
	        	detail.Object__c = 'Login';
	        	detail.Process_Area__c='SFDC Foundation';
                detail.Source__c = tSource;
	        	detail.User_Id__c = lh.UserId;
	        	appUsageDetails.add(detail);
	       	}
        }
        upsert appUsageDetails External_Record_Id__c;
        
    }

    global void finish(Database.BatchableContext ctx){

    }


}