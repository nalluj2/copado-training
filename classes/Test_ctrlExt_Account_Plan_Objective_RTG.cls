@isTest
private class Test_ctrlExt_Account_Plan_Objective_RTG {
	
	private static testmethod void BUG_AccountPlan_Objective(){
		
		setData();
		
		Account_Plan_2__c accPlan = new Account_Plan_2__c();
		accPlan.Account__c = [Select Id from Account].Id;
		accPlan.Account_Plan_Level__c = 'Business Unit Group';
		accPlan.Business_Unit_Group__c = [Select Id from Business_Unit_Group__c where name = 'RTG'].Id;
		accPlan.RecordTypeId = [Select Id from RecordType where SObjectType = 'Account_Plan_2__c' AND DeveloperName = 'EUR_RTG_BUG'].Id;
		insert accPlan;
		
		Test.startTest();
		
		Account_Plan_Objective__c objective = new Account_Plan_Objective__c();
		objective.Account_Plan__c = accPlan.Id;
		
		ApexPages.StandardController sc = new ApexPages.StandardController(objective);
		ctrlExt_Account_Plan_Objective_Edit_RTG controller = new ctrlExt_Account_Plan_Objective_Edit_RTG(sc);
		
		System.assert(objective.RecordTypeId == [Select Id from RecordType where SObjectType = 'Account_Plan_Objective__c' AND DeveloperName = 'RTG'].Id);
		System.assert(controller.isDiabetesDepartment == false);
		System.assert(ApexPages.getMessages().isEmpty());
		
		objective.Business_Objective__c = Database.query( 'Select Id from Business_Objective__c '+controller.businessObjectiveFilter)[0].Id;
		controller.businessObjectiveSelected();
		
		System.assert(controller.businessObjective != null);
				
		controller.targetDateToCurrentFY();
		
		objective.Description__c = 'Test BUG Account Plan Objective';
		
		controller.save();
		
		System.assert(ApexPages.getMessages().isEmpty());
		
		sc = new ApexPages.StandardController(objective);
		controller = new ctrlExt_Account_Plan_Objective_Edit_RTG(sc);
		
		System.assert(controller.businessObjective != null);
		
		controller.cancel();
	} 

	private static testmethod void BU_AccountPlan_Objective(){
		
		setData();
		
		Account_Plan_2__c accPlan = new Account_Plan_2__c();
		accPlan.Account__c = [Select Id from Account].Id;
		accPlan.Account_Plan_Level__c = 'Business Unit';
		accPlan.Business_Unit__c = [Select Id from Business_Unit__c where name = 'RTG BU'].Id;
		accPlan.RecordTypeId = [Select Id from RecordType where SObjectType = 'Account_Plan_2__c' AND DeveloperName = 'EUR_RTG_BU'].Id;
		insert accPlan;
		
		Test.startTest();
		
		Account_Plan_Objective__c objective = new Account_Plan_Objective__c();
		objective.Account_Plan__c = accPlan.Id;
		
		ApexPages.StandardController sc = new ApexPages.StandardController(objective);
		ctrlExt_Account_Plan_Objective_Edit_RTG controller = new ctrlExt_Account_Plan_Objective_Edit_RTG(sc);
		
		System.assert(objective.RecordTypeId == [Select Id from RecordType where SObjectType = 'Account_Plan_Objective__c' AND DeveloperName = 'RTG'].Id);
		System.assert(controller.isDiabetesDepartment == false);
		System.assert(ApexPages.getMessages().isEmpty());
		
		objective.Business_Objective__c = Database.query( 'Select Id from Business_Objective__c '+controller.businessObjectiveFilter)[0].Id;
		controller.businessObjectiveSelected();
		
		System.assert(controller.businessObjective != null);
				
		controller.targetDateToCurrentFY();
		
		objective.Description__c = 'Test BU Account Plan Objective';
		
		controller.save();
		
		System.assert(ApexPages.getMessages().isEmpty());
		
		sc = new ApexPages.StandardController(objective);
		controller = new ctrlExt_Account_Plan_Objective_Edit_RTG(sc);
		
		System.assert(controller.businessObjective != null);
		
		controller.cancel();
	} 
		
	private static testmethod void sBU_AccountPlan_Objective(){
		
		setData();
		
		Account_Plan_2__c accPlan = new Account_Plan_2__c();
		accPlan.Account__c = [Select Id from Account].Id;
		accPlan.Account_Plan_Level__c = 'Sub Business Unit';
		accPlan.Sub_Business_Unit__c = [Select Id from Sub_Business_Units__c where name = 'RTG sBU'].Id;
		accPlan.RecordTypeId = [Select Id from RecordType where SObjectType = 'Account_Plan_2__c' AND DeveloperName = 'EUR_RTG_sBU'].Id;
		insert accPlan;
		
		Test.startTest();
		
		Account_Plan_Objective__c objective = new Account_Plan_Objective__c();
		objective.Account_Plan__c = accPlan.Id;
		
		ApexPages.StandardController sc = new ApexPages.StandardController(objective);
		ctrlExt_Account_Plan_Objective_Edit_RTG controller = new ctrlExt_Account_Plan_Objective_Edit_RTG(sc);
		
		System.assert(objective.RecordTypeId == [Select Id from RecordType where SObjectType = 'Account_Plan_Objective__c' AND DeveloperName = 'RTG'].Id);
		System.assert(controller.isDiabetesDepartment == false);
		System.assert(ApexPages.getMessages().isEmpty());
		
		objective.Business_Objective__c = Database.query( 'Select Id from Business_Objective__c '+controller.businessObjectiveFilter)[0].Id;
		controller.businessObjectiveSelected();
		
		System.assert(controller.businessObjective != null);
		
		objective.Target__c = 1000;
		objective.Description__c = 'Test sBU Account Plan Objective';
		
		controller.save();
		
		System.assert(ApexPages.getMessages().isEmpty());
	}
	
	private static testmethod void sBU_AccountPlan_Objective_DiB(){
		
		setData();
		
		Account_Plan_2__c accPlan = new Account_Plan_2__c();
		accPlan.Account__c = [Select Id from Account].Id;
		accPlan.Account_Plan_Level__c = 'Sub Business Unit';
		accPlan.Sub_Business_Unit__c = [Select Id from Sub_Business_Units__c where Name = 'Diabetes Core'].Id;
		accPlan.RecordTypeId = [Select Id from RecordType where SObjectType = 'Account_Plan_2__c' AND DeveloperName = 'EUR_DiB_sBU'].Id;
		insert accPlan;
		
		Test.startTest();
		
		Account_Plan_Objective__c objective = new Account_Plan_Objective__c();
		objective.Account_Plan__c = accPlan.Id;
		
		ApexPages.StandardController sc = new ApexPages.StandardController(objective);
		ctrlExt_Account_Plan_Objective_Edit_RTG controller = new ctrlExt_Account_Plan_Objective_Edit_RTG(sc);
		
		System.assert(objective.RecordTypeId == [Select Id from RecordType where SObjectType = 'Account_Plan_Objective__c' AND DeveloperName = 'RTG'].Id);
		System.assert(controller.isDiabetesDepartment == true);
		System.assert(ApexPages.getMessages().isEmpty());
		
		objective.Business_Objective__c = Database.query( 'Select Id from Business_Objective__c '+controller.businessObjectiveFilter)[0].Id;
		controller.businessObjectiveSelected();
		
		System.assert(controller.businessObjective != null);
		
		objective.Description__c = 'Test sBU Account Plan Objective';
		objective.Department__c = 'Adult Type 1';
		
		controller.save();
		
		System.assert(ApexPages.getMessages().isEmpty());
		
		objective = [Select Account_Department__c from Account_Plan_Objective__c where Id = :objective.Id];
		System.assert(objective.Account_Department__c != null);
	} 
	
	private static testmethod void AccountPlan_Objective_Errors(){
		
		setData();
		
		Account_Plan_2__c accPlan = new Account_Plan_2__c();
		accPlan.Account__c = [Select Id from Account].Id;
		accPlan.Account_Plan_Level__c = 'Sub Business Unit';
		accPlan.Sub_Business_Unit__c = [Select Id from Sub_Business_Units__c where name = 'RTG sBU'].Id;
		insert accPlan;
		
		Test.startTest();
		
		Account_Plan_Objective__c objective = new Account_Plan_Objective__c();
		objective.Account_Plan__c = accPlan.Id;
		
		ApexPages.StandardController sc = new ApexPages.StandardController(objective);
		ctrlExt_Account_Plan_Objective_Edit_RTG controller = new ctrlExt_Account_Plan_Objective_Edit_RTG(sc);
			
		System.assert(controller.save() == null);
		
		objective.Business_Objective__c = Database.query( 'Select Id from Business_Objective__c '+controller.businessObjectiveFilter)[0].Id;
		controller.businessObjectiveSelected();
		
		System.assert(controller.businessObjective != null);
		
		System.assert(controller.save() == null);
		
		objective.Target__c = 1000;
		objective.Description__c = 'Test sBU Account Plan Objective';
				
		System.assert(controller.save() != null);
		
		Account_Plan_Objective__c dupe = new Account_Plan_Objective__c();
		dupe.Account_Plan__c = accPlan.Id;
		
		ApexPages.StandardController dupeSc = new ApexPages.StandardController(dupe);
		ctrlExt_Account_Plan_Objective_Edit_RTG dupeController = new ctrlExt_Account_Plan_Objective_Edit_RTG(dupeSc);
		
		dupe.Business_Objective__c = objective.Business_Objective__c;		
		dupeController.businessObjectiveSelected();
		
		dupe.Target_Date__c = Date.today().addDays(7);
		dupe.Description__c = 'Test sBU Account Plan Objective';
		
		System.assert(dupeController.save() == null);
	}	
	
	private static testmethod void AccountPlan_Objective_Errors_Dib(){
		
		setData();
		
		Account_Plan_2__c accPlan = new Account_Plan_2__c();
		accPlan.Account__c = [Select Id from Account].Id;
		accPlan.Account_Plan_Level__c = 'Sub Business Unit';
		accPlan.Sub_Business_Unit__c = [Select Id from Sub_Business_Units__c where Name = 'Diabetes Core'].Id;
		accPlan.RecordTypeId = [Select Id from RecordType where SObjectType = 'Account_Plan_2__c' AND DeveloperName = 'EUR_DiB_sBU'].Id;
		insert accPlan;
		
		Test.startTest();
		
		Account_Plan_Objective__c objective = new Account_Plan_Objective__c();
		objective.Account_Plan__c = accPlan.Id;
		
		ApexPages.StandardController sc = new ApexPages.StandardController(objective);
		ctrlExt_Account_Plan_Objective_Edit_RTG controller = new ctrlExt_Account_Plan_Objective_Edit_RTG(sc);
			
		System.assert(controller.save() == null);
		
		objective.Business_Objective__c = Database.query( 'Select Id from Business_Objective__c '+controller.businessObjectiveFilter)[0].Id;
		controller.businessObjectiveSelected();
		
		System.assert(controller.businessObjective != null);
		
		System.assert(controller.save() == null);
		
		objective.Target_Date__c = Date.today().addDays(7);
		objective.Description__c = 'Test sBU Account Plan Objective';
		objective.Department__c = 'Adult Type 2';
				
		System.assert(controller.save() == null);
		
		DIB_Department__c accAdultPed = new DIB_Department__c();
		accAdultPed.Account__c = accPlan.Account__c;
		accAdultPed.Department_ID__c = [Select Id from Department_Master__c where Name = 'Adult/Pediatric'].Id;
		insert accAdultPed;
		
		System.assert(controller.save() != null);
		
		Account_Plan_Objective__c nodupe = new Account_Plan_Objective__c();
		nodupe.Account_Plan__c = accPlan.Id;
		
		ApexPages.StandardController dupeSc = new ApexPages.StandardController(nodupe);
		ctrlExt_Account_Plan_Objective_Edit_RTG dupeController = new ctrlExt_Account_Plan_Objective_Edit_RTG(dupeSc);
		
		nodupe.Business_Objective__c = objective.Business_Objective__c;		
		dupeController.businessObjectiveSelected();
				
		nodupe.Description__c = 'Test sBU Account Plan Objective';
		nodupe.Department__c = 'Adult Type 1';
		
		System.assert(dupeController.save() != null);
	}	
	
	private static void setData() {
	
		Company__c cmpny = new Company__c();		
        cmpny.name='Europe';
        cmpny.CurrencyIsoCode = 'EUR';
        cmpny.Current_day_in_Q1__c=56;
        cmpny.Current_day_in_Q2__c=34; 
        cmpny.Current_day_in_Q3__c=5; 
        cmpny.Current_day_in_Q4__c= 0;   
        cmpny.Current_day_in_year__c =200;
        cmpny.Days_in_Q1__c=56;  
        cmpny.Days_in_Q2__c=34;
        cmpny.Days_in_Q3__c=13;
        cmpny.Days_in_Q4__c=22;
        cmpny.Days_in_year__c =250;
        cmpny.Company_Code_Text__c = 'T35';
		insert cmpny;
		
		Business_Unit_Group__c bug =  new Business_Unit_Group__c();
        bug.Master_Data__c =cmpny.id;
        bug.name='RTG';
        
        Business_Unit_Group__c bug_DiB =  new Business_Unit_Group__c();
        bug_DiB.Master_Data__c =cmpny.id;
        bug_DiB.name='Diabetes';              
        insert new List<Business_Unit_Group__c>{bug, bug_DiB};
		
		Business_Unit__c bu =  new Business_Unit__c();
        bu.Company__c =cmpny.id;
        bu.name='RTG BU';
        bu.Business_Unit_Group__c = bug.Id;
        bu.Account_Plan_Activities__c=true;  
        
        Business_Unit__c bu_DiB =  new Business_Unit__c();
        bu_DiB.Company__c =cmpny.id;
        bu_DiB.name='Diabetes';
        bu_DiB.Business_Unit_Group__c = bug_DiB.Id;          
              
        insert new List<Business_Unit__c>{bu, bu_DiB};
        
        Sub_Business_Units__c sbu = new Sub_Business_Units__c();
        sbu.name='RTG sBU';
        sbu.Business_Unit__c=bu.id;
        
        Sub_Business_Units__c sbu_DiB = new Sub_Business_Units__c();
        sbu_DiB.name='Diabetes Core';
        sbu_DiB.Business_Unit__c=bu_DiB.id;
		
		insert new List<Sub_Business_Units__c>{sbu, sbu_DiB};
		
		DIB_Country__c country = new DIB_Country__c();
		country.Company__c = cmpny.Id;
		country.Name = 'Unit Test Country';
		country.Country_ISO_Code__c = 'ZZ';
		country.CurrencyIsoCode = 'EUR';
		insert country;
		
		Business_Objective__c sbuObjective = new Business_Objective__c();
		sbuObjective.Name = 'SBU Objective';
		sbuObjective.Explanation__c = 'test explanation';
		sbuObjective.Type__c = 'Quantitative';
		sbuObjective.Active__c = true;
		sbuObjective.Sub_Business_Unit__c = sbu.Id;
		
		Business_Objective__c sbuObjective_DiB = new Business_Objective__c();
		sbuObjective_DiB.Name = 'SBU Objective Diabetes';
		sbuObjective_DiB.Explanation__c = 'test explanation';
		sbuObjective_DiB.Type__c = 'Qualitative';
		sbuObjective_DiB.Active__c = true;
		sbuObjective_DiB.Sub_Business_Unit__c = sbu_DiB.Id;
		insert new List<Business_Objective__c>{sbuObjective, sbuObjective_DiB};
		
		Business_Objective__c buObjective = new Business_Objective__c();
		buObjective.Name = 'BU Objective';
		buObjective.Explanation__c = 'test explanation';
		buObjective.Type__c = 'Qualitative';
		buObjective.Active__c = true;
		buObjective.Business_Unit__c = bu.Id;
		insert buObjective;

		Business_Objective__c bugObjective = new Business_Objective__c();
		bugObjective.Name = 'BUG Objective';
		bugObjective.Explanation__c = 'test explanation';
		bugObjective.Type__c = 'Qualitative';
		bugObjective.Active__c = true;
		bugObjective.Business_Unit_Group__c = bug.Id;
		insert bugObjective;
				
		Account testAccount = new Account();
		testAccount.Name = 'RTG Test Account';
		testAccount.Account_Country_vs__c = 'BELGIUM';
		insert testAccount;
		
		DIB_Fiscal_Period__c period = new DIB_Fiscal_Period__c();
		period.Start_Date__c = Date.Today().addDays(-7);
		period.End_date__c = Date.Today().addDays(7);
		period.Fiscal_Month__c = '12';
		period.Fiscal_Year__c = String.valueOf(Date.Today().year());
		period.Quarter__c = 'Q4';
		insert period;
		
		Department_Master__c adultPediatric = new Department_Master__c(Name = 'Adult/Pediatric');
		Department_Master__c adultT1 = new Department_Master__c(Name = 'Adult Type 1');
		Department_Master__c adultT2 = new Department_Master__c(Name = 'Adult Type 2');
		Department_Master__c pediatric = new Department_Master__c(Name = 'Pediatric');
		
		insert new List<Department_Master__c>{adultPediatric, adultT1, adultT2, pediatric};
		
		DIB_Department__c accAdultT1 = new DIB_Department__c();
		accAdultT1.Account__c = testAccount.Id;
		accAdultT1.Department_ID__c = adultT1.Id;
		insert accAdultT1;
	}
}