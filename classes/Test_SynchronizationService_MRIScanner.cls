//--------------------------------------------------------------------------------------------------------------------------------
//  Author      :   Bart Caelen
//  Date        :   25/10/2016
//  Description :   APEX TEST Class for the APEX Class SynchronizationService_MRIScanner
//  Change Log  :    
//--------------------------------------------------------------------------------------------------------------------------------
@isTest private class Test_SynchronizationService_MRIScanner {
	
	@isTest static void test_generatePayload() {
		
		//--------------------------------------------------------
		// Create Test Data
		//--------------------------------------------------------
		// Create Setting Data
		Synchronization_Service_Setting__c oSynchronizationServiceSetting = new Synchronization_Service_Setting__c();
			oSynchronizationServiceSetting.username__c = 'test@medtronic.com';
			oSynchronizationServiceSetting.password__c = 'password';
			oSynchronizationServiceSetting.client_id__c = '123456789';
			oSynchronizationServiceSetting.client_secret__c = 'XXXXX';
			oSynchronizationServiceSetting.is_sandbox__c = true;
			oSynchronizationServiceSetting.Sync_Enabled__c = true;
		insert oSynchronizationServiceSetting;	

		// Create Account Data
		Account oAccount = clsTestData_Account.createAccount()[0];
		clsTestData_Account.oMain_Account = null;
		Account oAccount_SAP = clsTestData_Account.createAccount_SAPAccount()[0];

		// Create MRI Scanner Data
		List<MRI_Scanner__c> lstMRIScanner = new List<MRI_Scanner__c>();
		MRI_Scanner__c oMRIScanner1 = new MRI_Scanner__c();
			oMRIScanner1.Name = 'TEST MRI Scanner 1';
			oMRIScanner1.Account__c = oAccount.Id;
			oMRIScanner1.MRI_Contact__c = 'MRI Contact';
			oMRIScanner1.MRI_IP_Address__c = '100.100.100.100';
			oMRIScanner1.MRI_Location__c = 'MRI Location';
			oMRIScanner1.MRI_Make__c = 'Siemens';
			oMRIScanner1.MRI_Model__c = 'Avanto';
			oMRIScanner1.MRI_Strength_T__c = '3.0T';
			oMRIScanner1.MRI_SW_Version__c = 'MRI Version';
			oMRIScanner1.MRI_Tech__c = 'MRI Tech';
			oMRIScanner1.Primary__c = true;
		lstMRIScanner.add(oMRIScanner1);
		MRI_Scanner__c oMRIScanner2 = new MRI_Scanner__c();
			oMRIScanner2.Name = 'TEST MRI Scanner 2';
			oMRIScanner2.Account__c = oAccount_SAP.Id;
			oMRIScanner2.MRI_Contact__c = 'MRI Contact';
			oMRIScanner2.MRI_IP_Address__c = '100.100.100.100';
			oMRIScanner2.MRI_Location__c = 'MRI Location';
			oMRIScanner2.MRI_Make__c = 'Siemens';
			oMRIScanner2.MRI_Model__c = 'Avanto';
			oMRIScanner2.MRI_Strength_T__c = '3.0T';
			oMRIScanner2.MRI_SW_Version__c = 'MRI Version';
			oMRIScanner2.MRI_Tech__c = 'MRI Tech';
			oMRIScanner2.Primary__c = true;
		lstMRIScanner.add(oMRIScanner2);
		insert lstMRIScanner;

		lstMRIScanner = [SELECT Id, External_ID__c, Name, Account__c, MRI_Contact__c, MRI_IP_Address__c, MRI_Location__c, MRI_Make__c, MRI_Model__c, MRI_Strength_T__c, MRI_SW_Version__c, Primary__c FROM MRI_Scanner__c WHERE ID = :lstMRIScanner];
		//--------------------------------------------------------


		//--------------------------------------------------------
		// Execute Logic
		//--------------------------------------------------------
		Test.startTest();
		
		Test_bl_SynchronizationService_Utils.CalloutMock mockImpl = new Test_bl_SynchronizationService_Utils.CalloutMock();
		
		Test.setMock(HttpCalloutMock.class, mockImpl);
		bl_SynchronizationService_Utils.calloutMock = mockImpl;

			lstMRIScanner[0].MRI_Location__c = 'MRI Location 2';
		update lstMRIScanner[0];

			lstMRIScanner[1].MRI_Location__c = 'MRI Location 2';
		update lstMRIScanner[1];

		SynchronizationService_MRIScanner oSynchronizationServiceMRIScanner = new SynchronizationService_MRIScanner();
		String tPayload1 = oSynchronizationServiceMRIScanner.generatePayload(lstMRIScanner[0].External_ID__c);
		String tPayload2 = oSynchronizationServiceMRIScanner.generatePayload(lstMRIScanner[1].External_ID__c);

		Test.stopTest();		
		//--------------------------------------------------------


		//--------------------------------------------------------
		// Validation
		//--------------------------------------------------------
		System.assert(tPayload1 != null);
		System.assert(tPayload2 != null);
		//--------------------------------------------------------
	}

	@isTest static void test_processPayload() {
		
		//--------------------------------------------------------
		// Create Test Data
		//--------------------------------------------------------
		//--------------------------------------------------------


		//--------------------------------------------------------
		// Execute Logic
		//--------------------------------------------------------
		Test.startTest();

		SynchronizationService_MRIScanner oSynchronizationServiceMRIScanner = new SynchronizationService_MRIScanner();
		String tResult = oSynchronizationServiceMRIScanner.processPayload('');
		
		Test.stopTest();		
		//--------------------------------------------------------


		//--------------------------------------------------------
		// Validation
		//--------------------------------------------------------
		// Logic is not implemented so the result should be NULL
		System.assert(tResult == null);
		//--------------------------------------------------------
	}	
		
}
//--------------------------------------------------------------------------------------------------------------------------------