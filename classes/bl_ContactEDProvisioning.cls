/*
	This class is tested also in TEST_bl_CampaignMember_Trigger
*/
public with sharing class bl_ContactEDProvisioning {
 	
 	private static String sessionToken{
 		
 		get{
 			
 			if(sessionToken == null) acquireSessionToken();
		
			return sessionToken;
 		} 		
 	}
 	
 	private static ED_API_Settings__c apiSettings{
 		
 		get{
 			
 			if(apiSettings == null){
 				
 				ED_API_Settings__c settings = ED_API_Settings__c.getInstance();
				Id id_OrgId = settings.Org_Id__c; // Making sure we use the 18 character version of the ID in the Custom Setting

 				if(id_OrgId != UserInfo.getOrganizationId()) throw new ws_Exception('No API Settings available (ED_API_Settings__c), please contact your System Administrator.');
 				
 				apiSettings = settings;
 			}
 			
 			return apiSettings;
 		} 		
 	}

	public static Contact doEDUserProvision(Contact cnt, Contact_ED_Sync__c sync){
													
		UserResponse userResp = searchByEmail(cnt.Email);

		System.debug('**BC** doEDUserProvision - userResp : ' + userResp);
		
		Set<String> userGroups = new Set<String>();        	
    	for(CampaignMember member : cnt.CampaignMembers){
			if (!String.isBlank(member.Campaign.LMS_ED_Group__c)) userGroups.add(member.Campaign.LMS_ED_Group__c.trim().toUpperCase());
		}
		
		System.debug('User Groups: ' + userGroups);
		
		//It doesn't exist in ED
    	if(userResp.externalUser == null){
			
			sync.Outcome_Details__c = 'User not found.';
			
			//If there is at least a Group to be added or a LMS Division we create the user
			System.debug('**BC** doEDUserProvision - before createExternalUser - userGroups (' + userGroups.size() + ') : ' + userGroups);
			System.debug('**BC** doEDUserProvision - before createExternalUser - cnt.LMS_Division__c : ' + cnt.LMS_Division__c);
			if(userGroups.size() > 0 || cnt.LMS_Division__c != null){        						
			
				createExternalUser(cnt, userGroups);
				System.debug('**BC** doEDUserProvision - after createExternalUser - cnt : ' + cnt);
				System.debug('**BC** doEDUserProvision - after createExternalUser - userGroups : ' + userGroups);
																
				sync.Outcome_Details__c += '\nUser provisioned.';
				
				// We need to retrieve the External Directory Id
				userResp = searchByEmail(cnt.Email);

				if (userResp.externalUser == null){
					throw new ws_Exception('Error seaching by email - created user not found (' + cnt.Email + ') - please try again');
				}
				System.debug('**BC** doEDUserProvision - after createExternalUser - searchByEmail - userResp.externalUser : ' + userResp.externalUser);

			
			}else{
					
				sync.Outcome_Details__c += '\nUser does not require provisioning.';
			}
		
		//It exists		
		}else{
			
			sync.Outcome_Details__c = 'User found.';
			
			Set<String> addGroups = new Set<String>();
			Set<String> removeGroups = new Set<String>();
											
			if(userResp.groups != null){
				for(String userGroup : userResp.groups){
					// Only remove User Groups that are related to LMS and for which the name ends with  "_HCP_LMS"
					if  (userGroup.trim().toUpperCase().endsWith('_HCP_LMS')) removeGroups.add(userGroup.trim().toUpperCase());
				}
			}
							
			for(String userGroup : userGroups){
				
				//It was not in the list
				if(removeGroups.remove(userGroup) == false) addGroups.add(userGroup);
			}
			
			//If there is any group to add or remove					
			if(addGroups.size() > 0 || removeGroups.size() > 0){
				
				updateExternalUser(cnt, addGroups, removeGroups);
				
				sync.Outcome_Details__c += '\nUser modified:';
				if(addGroups.size() > 0) sync.Outcome_Details__c += '\n-Added Groups: ' + String.join(new List<String>(addGroups), ',');
				if(removeGroups.size() > 0) sync.Outcome_Details__c += '\n-Removed Groups: ' + String.join(new List<String>(removeGroups), ',');
				
			}else{
				
				sync.Outcome_Details__c += '\nUser not modified.';
			}							
		}

		System.debug('**BC** doEDUserProvision - userResp.externalUser : ' + userResp.externalUser);

		if(userResp.externalUser != null){
			
			sync.Outcome_Details__c += '\nUser Id: ' + userResp.externalUser.directoryKey;
			if(userGroups.size() > 0) sync.Outcome_Details__c += '\nUser Groups: ' + String.join(new List<String>(userGroups), ',');
			
			Boolean updateContact = false;
			
			if(cnt.LMS_IsActive__c == false && cnt.LMS_Division__c != null){
				
				cnt.LMS_IsActive__c = true;
				sync.Outcome_Details__c += '\nContact LMS Activated.';
				updateContact = true;
			}
			System.debug('**BC** doEDUserProvision - cnt.LMS_IsActive__c : ' + cnt.LMS_IsActive__c);
			System.debug('**BC** doEDUserProvision - cnt.LMS_Division__c : ' + cnt.LMS_Division__c);
			
			System.debug('**BC** doEDUserProvision - cnt.LMS_UserId__c : ' + cnt.LMS_UserId__c);
			System.debug('**BC** doEDUserProvision - userResp.externalUser.directoryKey : ' + userResp.externalUser.directoryKey);
			if(cnt.LMS_UserId__c != userResp.externalUser.directoryKey){
											
				cnt.LMS_Userid__c = userResp.externalUser.directoryKey;
				updateContact = true;
				sync.Outcome_Details__c += '\nContact User Id set to "' + userResp.externalUser.directoryKey + '"';
			}
			
			if(updateContact == true) return cnt;
		}		
		
		return null;
	}   
		
	private static UserResponse searchByEmail(String email){
		
		if(email == null) throw new ws_Exception('Error seaching by email: No email address available.');
		System.debug('**BC** apiSettings.Target_Server_URL__c : ' + apiSettings.Target_Server_URL__c);
		Http http = new Http();
		HttpRequest request = new HttpRequest();
		request.setEndpoint(apiSettings.Target_Server_URL__c + '/provision/v1/service/wsviewexternaluserprofilesearch?email=' + email);
		request.setMethod('GET');
		request.setHeader('X-MDT-API-KEY', apiSettings.API_Key__c);
		request.setHeader('Authorization', 'Bearer ' + sessionToken);
		request.setHeader('Accept', ' */*');
		request.setTimeout(60000);
		HttpResponse response = http.send(request);

		System.debug('**BC** searchByEmail - email : ' + email);
		System.debug('**BC** searchByEmail - request 1 : ' + request.toString());
		System.debug('**BC** searchByEmail - request 2 : ' + String.valueOf(request.getBody()));
		System.debug('**BC** searchByEmail - response 1 : ' + response.toString());
		System.debug('**BC** searchByEmail - response 2 : ' + String.valueOf(response.getBody()));
		
		if(response.getStatusCode() != 200) throw new ws_Exception('Error seaching by email: ' + response.getStatus() + ' (' + response.getStatusCode() + ')');
		System.debug(''+response.getBody());
    	UserResponse userResp = (UserResponse) JSON.deserialize(response.getBody(), UserResponse.class);
		System.debug(''+userResp);
    	if(userResp.externalUser == null && userResp.email == null) throw new ws_Exception('Error seaching by email: ' + userResp.message);
    	
		return userResp;
	}
	
	private static void createExternalUser(Contact cnt, Set<String> userGroups){
		
		Http http = new Http();
		HttpRequest request = new HttpRequest();
		request.setEndpoint(apiSettings.Target_Server_URL__c + '/provision/v1/service/wscreateuserdirkeyuid');
		request.setMethod('POST');
		request.setHeader('X-MDT-API-KEY', apiSettings.API_Key__c);
		request.setHeader('Authorization', 'Bearer ' + sessionToken);
		request.setHeader('Accept', ' */*');
		request.setHeader('Content-Type', 'application/json');
		request.setTimeout(60000);
		
		ExternalUser extUser = new ExternalUser();
		extUser.firstname = cnt.FirstName;
        extUser.lastname = cnt.LastName;
        extUser.email = cnt.Email;          
        extUser.country = bl_Contact_LearningManagement_Provision.countryMapping.get(cnt.LMS_Country__c);      
        extUser.languageCode = cnt.LMS_Language__c;
        extUser.defaultPassword = apiSettings.Default_Password__c;
                                
        UserResponse userResp = new UserResponse();
        userResp.externalUser = extUser;
        userResp.groups = new List<String>(userGroups);
                
        request.setBody(JSON.serialize(userResp));   
		
		HttpResponse response = http.send(request);

		System.debug('**BC** createExternalUser - request 1 : ' + request.toString());
		System.debug('**BC** createExternalUser - request 2 : ' + String.valueOf(request.getBody()));
		System.debug('**BC** createExternalUser - response 1 : ' + response.toString());
		System.debug('**BC** createExternalUser - response 2 : ' + String.valueOf(response.getBody()));
		
		if(response.getStatusCode() >= 400) throw new ws_Exception('Error creating user: ' + response.getStatus() + ' (' + response.getStatusCode() + ')');  	
	}
	
	private static void updateExternalUser(Contact cnt, Set<String> addGroups, Set<String> removeGroups){
		
		Http http = new Http();
		HttpRequest request = new HttpRequest();
		request.setEndpoint(apiSettings.Target_Server_URL__c + '/provision/v1/service/wsmodifyexternaluserprofiles');
		request.setMethod('POST');
		request.setHeader('X-MDT-API-KEY', apiSettings.API_Key__c);
		request.setHeader('Authorization', 'Bearer ' + sessionToken);
		request.setHeader('Accept', ' */*');
		request.setHeader('Content-Type', 'application/json');
		request.setTimeout(60000);
		
		ExternalUser extUser = new ExternalUser();		
        extUser.firstname = cnt.FirstName;
        extUser.lastname = cnt.LastName;
        extUser.email = cnt.Email;          
        extUser.country = bl_Contact_LearningManagement_Provision.countryMapping.get(cnt.LMS_Country__c);      
        extUser.languageCode = cnt.LMS_Language__c;        
                                        
        UserResponse userResp = new UserResponse();
        userResp.externalUser = extUser;             	
        userResp.groups = new List<String>(addGroups);
        userResp.removeGroups = new List<String>(removeGroups);
        
        request.setBody(JSON.serialize(userResp));   
		
		HttpResponse response = http.send(request);

		System.debug('**BC** updateExternalUser - request 1 : ' + request.toString());
		System.debug('**BC** updateExternalUser - request 2 : ' + String.valueOf(request.getBody()));
		System.debug('**BC** updateExternalUser - response 1 : ' + response.toString());
		System.debug('**BC** updateExternalUser - response 2 : ' + String.valueOf(response.getBody()));
		
		if(response.getStatusCode() >= 400) throw new ws_Exception('Error updating user: ' + response.getStatus() + ' (' + response.getStatusCode() + ')');
		System.debug(''+response.getBody());
    	userResp = (UserResponse) JSON.deserialize(response.getBody(), UserResponse.class);
		System.debug(''+userResp);		
		if(userResp.externalUser == null) throw new ws_Exception('Error updating user: ' + userResp.message);		
	}
	
	private static void acquireSessionToken(){
		
		Http http = new Http();
		HttpRequest request = new HttpRequest();
		request.setEndpoint(apiSettings.Target_Server_URL__c + '/apiplatform/v1/oauth2/token/bearer?grant_type=client_credentials');
		request.setMethod('POST');
		request.setHeader('X-MDT-API-KEY', apiSettings.API_Key__c);
		request.setHeader('Authorization', apiSettings.Token_Authentication__c);
		request.setHeader('Content-Length', '0');
		request.setTimeout(60000);
		HttpResponse response = http.send(request);
		
		System.debug('**BC** acquireSessionToken - request 1 : ' + request.toString());
		System.debug('**BC** acquireSessionToken - request 2 : ' + String.valueOf(request.getBody()));
		System.debug('**BC** acquireSessionToken - response 1 : ' + response.toString());
		System.debug('**BC** acquireSessionToken - response 2 : ' + String.valueOf(response.getBody()));

		if(response.getStatusCode() != 200) throw new ws_Exception('Error acquiring session token: ' + response.getStatus() + ' (' + response.getStatusCode() + ')');

    	SessionResponse session = (SessionResponse) JSON.deserialize(response.getBody(), SessionResponse.class);
    	
    	if(session.access_token == null)  throw new ws_Exception('Error acquiring session token: ' + session.errorType + ' (' + session.errorCode + '). ' + session.errorDescription);
    	
    	sessionToken = session.access_token;
	}
	
	public class SessionResponse{
    	
    	public String access_token {get; set;}
	    public String errorCode {get; set;}
	    public String errorType {get; set;}
	    public String errorDescription {get; set;}    
	}
	
	public class UserResponse{
    	
    	public ExternalUser externalUser {get; set;}
    	public List<String> groups {get; set;}
    	public List<String> removeGroups {get; set;}       
	    	    
	    public String Message {get; set;}
	    public String Email {get; set;}
	}
	
	public class ExternalUser{
        
        public String firstname {get; set;}
        public String lastname {get; set;}
        public String email {get; set;}
        public String country {get; set;}        
        public String languageCode {get; set;}
        public String defaultPassword {get; set;}
        public String lmsDivision {get; set;}
        public String lmsLearningMgrUserId {get; set;}        
        public String directoryKey {get; set;}
    }	
}