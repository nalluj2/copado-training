public with sharing class iPlanResultWrapper {
	public Account_Plan_2__c iPlan {get;set;}
	public string errorMessage{get;set;}
	public boolean isFound {get;set;}
}