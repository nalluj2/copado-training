public with sharing class AssetContractNewExtension 
{
    private final AssetContract__c esRecord;
    
    // Constructor
    public AssetContractNewExtension(ApexPages.StandardController stdController)
    {
        // Get the current Entitlement System record from the standard controller 
        esRecord = (AssetContract__c) stdController.getRecord();
        
        // Check to see if the Service Contract field is filled in
        if(esRecord.Contract__c != null)
        {
            // If it is then query the related Service Contract for Start and End Date
            Contract relatedContract = [select Id, StartDate, EndDate from Contract where Id = :esRecord.Contract__c limit 1];
        
            if(relatedContract != null)
            {
                // Fill Start and End date into the Entitlement System record
                esRecord.StartDate__c = relatedContract.StartDate;
                esRecord.EndDate__c = relatedContract.EndDate;
            }           
        }
        
        // Auto-select the three desired coverage options for the Entitlement and System record 
        esRecord.Entitlements__c = '100% Sys. Parts, 20% Disc. for NS Parts; 100% Coverage for Labor; 100% Coverage for Software Upgrade';
    }  
}