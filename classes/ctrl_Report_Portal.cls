/*
 *      One Stop Shop - Report Portal
 *      @Authors Christophe Saenen, Jesus Lozano, Bart Caelen
 */

public without sharing class ctrl_Report_Portal {
            
    public SessionData session {get; set;}
    public Boolean userLoggedIn {get; set;}
    public Boolean isGuestUser {get; set;}
    public User currentUser {get;set;}
    
    public CVG_Portal_Config__c CVGConfig {get; set;}
        
    private Set<String> setAdminProfile = new Set<String>{'System Administrator', 'System Administrator MDT', 'IT Business Analyst', 'IT Business Analyst (Read-only)', 'IT Business Analyst (RM) FORCE.COM', 'IT Business Analyst - Mobile Team'};
	
	private String portalContextBUG {get; set;}
	
    private Set<String> setSBU { get;set; }
    private Set<String> setBU { get;set; }
    private Set<String> setBUG { get;set; }
        
    public String selectedSection {get; set;}           
    public string launchLinkId {get;set;}        
    public transient String errorMessage {get; private set;}
                    
    public ctrl_Report_Portal() {
        
        session = new SessionData();
        
        if(UserInfo.getUserType() == 'Guest') isGuestUser = true;
        else isGuestUser = false;
        
        String url = ApexPages.currentPage().getUrl().substringBefore('?');
        
        System.debug('URL : ' + url);
        
        if(url.equalsIgnoreCase('/apex/report_portal')){
        	
        	CVGConfig = CVG_Portal_Config__c.getValues('CVG Config');        	
        	portalContextBUG = 'CVG';
        	
        }else if(url.equalsIgnoreCase('/apex/report_portal_rtg')){
        	
        	CVGConfig = CVG_Portal_Config__c.getValues('RTG Config');
        	portalContextBUG = 'RTG';
        	
        }else if(url.equalsIgnoreCase('/apex/report_portal_mitg')){
        	
        	CVGConfig = CVG_Portal_Config__c.getValues('MITG Config');
        	portalContextBUG = 'MITG';
        }
                
        
        if(isGuestUser == true){
        	
        	userLoggedIn=false;
        	
        	Map<String, String> inputParams = ApexPages.currentPage().getParameters();
        	
        	if(inputParams != null && inputParams.get('user') != null && inputParams.get('token') != null){
        		
        		loginWithSSO(inputParams);        		
        	}        	
        }
        else loginInternalUser();
    }
    
    private void loginWithSSO(Map<String, String> inputParams){
    	
    	String userAlias;
    	
    	try{
    		
    		userAlias = CryptoUtils.decryptText(inputParams.get('user'));
    		String token = CryptoUtils.decryptText(inputParams.get('token'));
    		
    		Long tokenTime = Long.valueOf(token);
    		
    		//If the difference between the time passed in the URL and now is more than 10 seconds then it is invalid
    		if(DateTime.now().getTime() - tokenTime > 10000){
    			errorMessage = 'Access denied. Access token has expired';    		
    			return;
    		}
    		
    	}catch(Exception e){
    		    		
    		return;
    	}
    	
    	currentUser = [
                SELECT
                    FederationIdentifier 
                    , FirstName
                    , LastName
                    , Email 
                    , CostCenter__c
                    , Peoplesoft_Id__c
                    , Manager.Email
                    , Manager.Name
                    , OneStopShop_Admin__c
                    , Company_Code_Text__c
                    , Job_Title_vs__c
                    , Region_vs__c
                    , Sub_Region__c
                    , Country_vs__c
                    , Profile.Name
                FROM 
                    User 
                WHERE 
                    FederationIdentifier = :userAlias
            ];
    	
    	loadUserBusinessUnitData(currentUser.Id);
    	
    	if(setBUG.contains(portalContextBUG)) {
                        
            session.uInfo = new ws_adUserService.userinfo();
            session.uInfo.email = currentUser.Email;
            session.uInfo.firstName = currentUser.FirstName;
            session.uInfo.lastName = currentUser.LastName;
            session.uInfo.costcenter = currentUser.CostCenter__c;
            session.uInfo.peopleSoft = currentUser.Peoplesoft_ID__c;
            session.username = currentUser.FederationIdentifier;
            session.internalUserId = currentUser.Id;

            if(currentUser.Manager!=null){
                session.uInfo.managerEmail = currentUser.Manager.Email;
                session.uInfo.managerName = currentUser.Manager.Name;   
            }
            
            userLoggedIn = true;            
        }
        else {
            userLoggedIn = false;
            errorMessage = 'Access denied. This Portal is only for ' + portalContextBUG + ' users';    
        }
    }
    
    private void loginInternalUser() {
        
        currentUser = 
            [
                SELECT
                    FederationIdentifier 
                    , Email 
                    , CostCenter__c
                    , Peoplesoft_Id__c
                    , Manager.Email
                    , Manager.Name
                    , OneStopShop_Admin__c
                    , Company_Code_Text__c
                    , Job_Title_vs__c
                    , Region_vs__c
                    , Sub_Region__c
                    , Country_vs__c
                    , Profile.Name
                FROM 
                    User 
                WHERE 
                    Id = :UserInfo.getUserId()
            ];
        
        loadUserBusinessUnitData(UserInfo.getUserId());
        
        // Only oss admins may login as an internal user                     
        if (currentUser.OneStopShop_Admin__c == true && setBUG.contains(portalContextBUG)) {
                        
            session.uInfo = new ws_adUserService.userinfo();
            session.uInfo.email = currentUser.Email;
            session.uInfo.firstName = UserInfo.getFirstName();
            session.uInfo.lastName = UserInfo.getLastName();
            session.uInfo.costcenter = currentUser.CostCenter__c;
            session.uInfo.peopleSoft = currentUser.Peoplesoft_ID__c;
            session.username = currentUser.FederationIdentifier;
            session.internalUserId = UserInfo.getUserId();

            if(currentUser.Manager!=null){
                session.uInfo.managerEmail = currentUser.Manager.Email;
                session.uInfo.managerName = currentUser.Manager.Name;   
            }
            
            userLoggedIn = true;
        }
        else {
            userLoggedIn = false;
            errorMessage = 'Access denied. Please contact your administrator';   
        }
    }
    
    // Load User_Business_Unit__c data and put all BU's (Name) and SBU's (Name) in Sets which will be used to select the correct OSS data
    private void loadUserBusinessUnitData(Id id_User){

        List<User_Business_Unit__c> lstUserBU =
            [
                SELECT 
                    Business_Unit_text__c
                    , Sub_Business_Unit__r.Name
                    , Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__r.Abbreviated_Name__c
                FROM 
                     User_Business_Unit__c
                WHERE 
                    User__c = :id_User
                ORDER BY 
                    Business_Unit_text__c, Sub_Business_Unit__r.Name                            
            ];

        setBU = new Set<String>();
        setSBU = new Set<String>();
        setBUG = new Set<String>(); 
        for (User_Business_Unit__c oUserBU : lstUserBU){
            setBU.add(oUserBU.Business_Unit_text__c);
            setSBU.add(oUserBU.Sub_Business_Unit__r.Name);
            setBUG.add(oUserBU.Sub_Business_Unit__r.Business_Unit__r.Business_Unit_Group__r.Abbreviated_Name__c);
        }
    }

    public void doLogin(){
            
        ws_adUserService.Service1Soap userService = new ws_adUserService.Service1Soap();
        userService.timeout_x=120000;
        String encodedPassword = EncodingUtil.base64Encode(Blob.valueOf(session.password));
                        
        String response;
        
        try{            
            response = userService.valUser(session.username, encodedPassword);
        }catch(Exception e){
            errorMessage = 'Service Unavailable: '+e.getMessage();
            return;
        } 
        
        if(response != 'OK'){
            
            userLoggedIn = false;
            String errorDesc='Login failed. '+response+': ';
            
            if(response=='EX E003') errorDesc+='Unknown user name or bad password';
            else if(response=='EX E017') errorDesc+='Invalid account';
            else if(response=='EX E011') errorDesc+='Account disabled';
            else if(response=='EX E016') errorDesc+='Account lock out';
            else if(response=='EX E000') errorDesc+='Service unavailable';
            else errorDesc+='Unknown error';
            
            errorMessage =  errorDesc;
        } else {
            
            session.uInfo = userService.getUserInfo(session.username);
            
            //Try to match the logged in User to an internal SFDC User
            List<User> internalUser = 
                [
                    SELECT 
                        Company_Code_Text__c
                        , Job_Title_vs__c
                        , Region_vs__c
                        , Sub_Region__c
                        , Country_vs__c
                        , FederationIdentifier
                        , Profile.Name
                    FROM 
                        User 
                    WHERE 
                        FederationIdentifier = :session.username 
                    ORDER BY 
                        CreatedDate ASC 
                    LIMIT 1
                ];

            //If an internal user doesn't exist, no access!
            if(internalUser.size() > 0) {
                session.internalUserId = internalUser[0].Id;
                currentUser = internalUser[0];

                loadUserBusinessUnitData(session.internalUserId);
				
				if(setBUG.contains(portalContextBUG)){
					
					userLoggedIn = true;
					recordLoginAction();
					
				}else{
					
					userLoggedIn = false;               
                	errorMessage = 'Access denied. This Portal is only for ' + portalContextBUG + ' users';
				}

            } else {
                userLoggedIn = false;               
                errorMessage = 'Access denied. Please contact your administrator';
            }
        }                               
    }

    //private Map<Id, SectionWrapper> mapSectionData;
    public Map<Id, SectionWrapper> sectionMap { 
        get{
        	
        	if(sectionMap == null || Test.isRunningTest() == true) sectionMap = getSectionMapData();
        	
            return sectionMap;
        }
        private set; 
    }

    private Map<Id, SectionWrapper> getSectionMapData(){
								
        Map<Id, SectionWrapper> mapSectionData = new Map<Id, SectionWrapper>();

        // Dynamic SOQL can't use the public properties in a class as a dynamic variable - therefor we need to create local variables
        String tUser_CompanyCodeText = currentUser.Company_Code_Text__c;
        String tUser_JobTitle = currentUser.Job_Title_vs__c;
        String tUser_Region = currentUser.Region_vs__c;
        String tUser_SubRegion = currentUser.Sub_Region__c;
        String tUser_Country = currentUser.Country_vs__c;
        String tUser_Id = currentuser.Id;

        String tSOQL = 'SELECT';
            tSOQL += ' Id, Active__c, Description__c, Sequence__c, Link__c, Target__c, Text__c, Title__c, Sub_Group__c, Button_Color__c'; 
            tSOQL += ', Section_Name__c, Section_Name__r.Id, Section_Name__r.Description__c, Section_Name__r.Sequence__c, Section_Name__r.Section_Label__c, Section_Name__r.Logo__c ';
            tSOQL += ', (SELECT Id, User__c, User_Operator__c FROM OSS_User_Override__r WHERE User__c = :tUser_Id)';  //- Bart Caelen - 20161128 -Added filter on user
            tSOQL += ', (';
                tSOQL += 'SELECT';
                    tSOQL += ' Id, Operator__c'; 
                tSOQL += ' FROM'; 
                    tSOQL += ' OSS_Permissions__r';
                tSOQL += ' WHERE';
                    tSOQL += ' Active__c = true';
                    tSOQL += ' AND Company_Code__c = :tUser_CompanyCodeText';
                    tSOQL += ' AND ( Job_Title_vs__c includes (:tUser_JobTitle) or Job_Title_vs__c = null )';
                    tSOQL += ' AND ( Region__c = \'' + tUser_Region + '\' OR Region__c = null )';
                    tSOQL += ' AND ( Sub_Region__c = \'' + tUser_SubRegion + '\' OR Sub_Region__c = null )';
                    tSOQL += ' AND ( Country_vs__c includes (\'' + tUser_Country + '\') OR Country_vs__c = null )';
                    tSOQL += ' AND ( Business_Unit_vs__c = :setBU OR Business_Unit_vs__c = null )';
                    tSOQL += ' AND ( Sub_Business_Unit__r.Name = :setSBU OR Sub_Business_Unit__c = null )';
        tSOQL += ' )'; 
        tSOQL += ' FROM';
            tSOQL += ' OSS_Content__c WHERE Section_Name__r.One_Stop_Shop__r.Name = \'' + portalContextBUG +'\' ';
        
        if (!setAdminProfile.contains(currentUser.Profile.Name)){
            tSOQL += ' AND ';
                tSOQL += ' Section_Name__r.Active__c = true';
                tSOQL += ' AND Active__c = true';
        }

        tSOQL += ' ORDER BY Section_Name__r.Sequence__c, Sub_Group__c, Sequence__c, Name';

        List<OSS_Content__c> lstOSSContent = Database.query(tSOQL);

        Set<Id> setID_OSSContent_Allowed = new Set<Id>();

        Map<Id, OSS_Section__c> mapOSSSection = new Map<Id, OSS_Section__c>();
        Map<Id, List<OSS_Content__c>> mapOSSSectionId_OSSContent = new Map<Id, List<OSS_Content__c>>();
        Map<String, List<OSS_Content__c>> mapOperator_OSSContent_Permission = new Map<String, List<OSS_Content__c>>();
        Map<String, List<OSS_Content__c>> mapOperator_OSSContent_UserOverride = new Map<String, List<OSS_Content__c>>();

        for (OSS_Content__c oOSSContent : lstOSSContent){

            mapOSSSection.put(oOSSContent.Section_Name__r.Id, oOSSContent.Section_Name__r);

            // Populate map which contains the Section and the related Content
            List<OSS_Content__c> lstOSSContent_Tmp1 = new List<OSS_Content__c>();
            if (mapOSSSectionId_OSSContent.containsKey(oOSSContent.Section_Name__r.Id)){
                lstOSSContent_Tmp1 = mapOSSSectionId_OSSContent.get(oOSSContent.Section_Name__r.Id);
            }
            lstOSSContent_Tmp1.add(oOSSContent);
            mapOSSSectionId_OSSContent.put(oOSSContent.Section_Name__r.Id, lstOSSContent_Tmp1);


            // Populate map which contains the Operator and the related Content based on the Permissions
            for (OSS_Permission__c oOSSPermission : oOSSContent.OSS_Permissions__r){

                List<OSS_Content__c> lstOSSContent_Tmp2 = new List<OSS_Content__c>();
                if (mapOperator_OSSContent_Permission.containsKey(oOSSPermission.Operator__c)){
                    lstOSSContent_Tmp2 = mapOperator_OSSContent_Permission.get(oOSSPermission.Operator__c);
                }
                lstOSSContent_Tmp2.add(oOSSContent);
                mapOperator_OSSContent_Permission.put(oOSSPermission.Operator__c, lstOSSContent_Tmp2);

            }

            // Populate map which contains the Operator and the related Content based on the User Overwrite
            for (OSS_User_Override__c oOSSUserOverride : oOSSContent.OSS_User_Override__r){

                List<OSS_Content__c> lstOSSContent_Tmp3 = new List<OSS_Content__c>();
                if (mapOperator_OSSContent_UserOverride.containsKey(oOSSUserOverride.User_Operator__c)){
                    lstOSSContent_Tmp3 = mapOperator_OSSContent_UserOverride.get(oOSSUserOverride.User_Operator__c);
                }
                lstOSSContent_Tmp3.add(oOSSContent);
                mapOperator_OSSContent_UserOverride.put(oOSSUserOverride.User_Operator__c, lstOSSContent_Tmp3);

            }


            if (setAdminProfile.contains(currentUser.Profile.Name)){
                setID_OSSContent_Allowed.add(oOSSContent.Id);
            }

        }

        // Include / Exclude access to content for the user based on the Permissions and User Override Data
        if (!setAdminProfile.contains(currentUser.Profile.Name)){
            
            if (mapOperator_OSSContent_Permission.containsKey('Include')){
                for (OSS_Content__c oOSSContent : mapOperator_OSSContent_Permission.get('Include')){
                    setID_OSSContent_Allowed.add(oOSSContent.Id);
                }
            }

            if (mapOperator_OSSContent_Permission.containsKey('Exclude')){
                for (OSS_Content__c oOSSContent : mapOperator_OSSContent_Permission.get('Exclude')){
                    if (setID_OSSContent_Allowed.contains(oOSSContent.Id)){
                        setID_OSSContent_Allowed.remove(oOSSContent.Id);
                    }
                }
            }

            if (mapOperator_OSSContent_UserOverride.containsKey('Include')){
                for (OSS_Content__c oOSSContent : mapOperator_OSSContent_UserOverride.get('Include')){
                    setID_OSSContent_Allowed.add(oOSSContent.Id);
                }
            }

            if (mapOperator_OSSContent_UserOverride.containsKey('Exclude')){
                for (OSS_Content__c oOSSContent : mapOperator_OSSContent_UserOverride.get('Exclude')){
                    if (setID_OSSContent_Allowed.contains(oOSSContent.Id)){
                        setID_OSSContent_Allowed.remove(oOSSContent.Id);
                    }
                }
            }

        }
				
        // Populate the SectionMap which is used on the VF Page to display the Sections and the Links (Content)
        for (Id id_OSSSection : mapOSSSectionId_OSSContent.keySet()){

            OSS_Section__c oOSSSection = mapOSSSection.get(id_OSSSection);

            List<ContentWrapper> lstContentWrapper = new List<ContentWrapper>();
            Integer iCounter = 0;
            for (OSS_Content__c oOSSContent : mapOSSSectionId_OSSContent.get(id_OSSSection)){

                if (setID_OSSContent_Allowed.contains(oOSSContent.Id)){
                    ContentWrapper oContentWrapper = new ContentWrapper();
                        oContentWrapper.content = oOSSContent;                        
                        oContentWrapper.color = contentColors[Math.mod(iCounter , 6)];
                    lstContentWrapper.add(oContentWrapper);

                    iCounter++;
                }

            }
            
            if(lstContentWrapper.size() > 0){
            	
            	SectionWrapper oSectionWrapper = new SectionWrapper(oOSSSection, lstContentWrapper);
            	mapSectionData.put(oOSSSection.Id, oSectionWrapper);
            }
        }

        if(mapSectionData.size() > 0){
        	List<SectionWrapper> sectionList = mapSectionData.values();
        	sectionList.sort();
        	selectedSection = sectionList[0].section.Id;
        }

        return mapSectionData;
    }

    private List<String> contentColors = new List<String>{'rgb(91,127,149)','rgb(0,75,135)','rgb(0,133,202)','rgb(0,169,224)', 'rgb(113,197,232)', 'rgb(185,217,235)' };
       
    public List<SectionWrapper> sections {
        
        get {
            
            List<SectionWrapper> sectionList = sectionMap.Values();  
            sectionList.sort();
            
            return sectionList;
        } 
        
        private set;
    }   
    
    public void onLoadAction(){
    	
    	if(userLoggedIn) recordLoginAction();
    }
    
    private void recordLoginAction(){
    	
    	Application_Usage_Detail__c aud = new Application_Usage_Detail__c (
                                            Action_Date__c = Date.Today(),
                                            Action_DateTime__c = Datetime.Now(),
                                            Action_Type__c = 'Read',
                                            Capability__c = 'Login',                            
                                            Process_Area__c = 'OSS',
                                            Source__c = 'OneStopShop ' + portalContextBUG,
                                            User_Id__c = currentuser.Id                                     
        );
        
        try { insert aud;}
        catch (exception e) {}
    	
    }
       
    // register a button click in the app usage detail object for reporting & dashboarding purpose
    public void processLinkButton() {
        
        Application_Usage_Detail__c aud = new Application_Usage_Detail__c (
                                            Action_Date__c = Date.Today(),
                                            Action_DateTime__c = Datetime.Now(),
                                            Action_Type__c = 'Read',
                                            Internal_Record_Id__c = this.launchLinkId,
                                            Object__c = 'OSS_Content__c',
                                            Source__c = 'OneStopShop ' + portalContextBUG,
                                            User_Id__c = currentuser.Id                                     
        );
        
        try { insert aud;}
        catch (exception e) {}
    }
    
    // wrapper class which contains sections + grouped links     
    public class SectionWrapper implements Comparable{
        
        public OSS_Section__c section {get;set;}
        
        public List<ContentWrapper> linkLst {get;set;}
        
        public SectionWrapper(OSS_Section__c s, List<ContentWrapper> lst) {
            this.section = s;
            this.linkLst = lst;
        }
        
        public Integer compareTo(Object compareTo) {

            SectionWrapper compareToSection = (SectionWrapper)compareTo;
    
            if (section.Sequence__c == compareToSection.section.Sequence__c) return 0;
    
            if (section.Sequence__c > compareToSection.section.Sequence__c) return 1;
    
            return -1;
        }
        
    }
    
    public class ContentWrapper{
        
        public OSS_Content__c content {get; set;}
        public String color {get; set;}
    }
    
    public class SessionData {       
        public String username {get; set;}
        public String password {get; set;}  
        public Id internalUserId {get; private set;}
        public ws_adUserService.userinfo uInfo {get; private set;}        
    }           
}