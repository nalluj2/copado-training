@isTest
private class Test_ctrl_Acitivity_Scheduling_Activity {
    
    private static testmethod void testUpdateCaseActivity(){
    	
    	createTestData();
    	
    	Test.startTest();
    	
    	Case assignedCase = [Select Id from Case where Status = 'Assigned'];
    	
    	ctrl_Activity_Scheduling_Activity.CaseActivity caseRequest = ctrl_Activity_Scheduling_Activity.getCase(assignedCase.Id);
    	System.assert(caseRequest.canEdit == true);
    	System.assert(caseRequest.caseRecord.Type == 'Implant Support Request');
    	System.assert(caseRequest.caseRecord.Assigned_To__c == UserInfo.getUserId());
    	
    	Case caseRecord = caseRequest.caseRecord;
    	caseRecord.Subject = 'Test Subject';
    	caseRecord.Date_Received_Date__c = Date.today();
    	caseRecord.Procedure__c = 'New Implant';
    	caseRecord.Product_Group__c = [Select Id from Product_Group__c].Id;
    	
    	ctrl_Activity_Scheduling_Activity.saveCase(caseRecord, caseRecord.Activity_Scheduling_Attendees__r);
    	assignedCase = [Select Id, Subject from Case where Id = :assignedCase.Id];
    	System.assert(assignedCase.Subject == 'Test Subject');
    }
    
    private static testmethod void testRejectCancelCaseActivity(){
    	
    	createTestData();
    	
    	Test.startTest();
    	
    	Case assignedCase = [Select Id from Case where Status = 'Assigned'];
    	
    	ctrl_Activity_Scheduling_Activity.CaseActivity caseRequest = ctrl_Activity_Scheduling_Activity.getCase(assignedCase.Id);
    	    	
    	Case caseRecord = caseRequest.caseRecord;
    	caseRecord.Subject = 'Test Subject';
    	caseRecord.Date_Received_Date__c = Date.today();
    	caseRecord.Procedure__c = 'New Implant';
    	caseRecord.Product_Group__c = [Select Id from Product_Group__c].Id;
    	caseRecord.Status = 'Cancelled';
    	    	
    	Boolean hasError = false;
    	
    	try{
    	
    		ctrl_Activity_Scheduling_Activity.saveCase(caseRecord, caseRecord.Activity_Scheduling_Attendees__r);
    		
    	}catch(Exception e){
    		
    		hasError = true;
    		System.assert(e.getMessage().contains('Reason for Cancellation is mandatory'));
    	}
    	
    	System.assert(hasError == true);
    	
    	caseRecord.Reason_for_Rejection_Cancellation__c = 'Test cancellation reason';
    	
    	ctrl_Activity_Scheduling_Activity.saveCase(caseRecord, new List<Activity_Scheduling_Attendee__c>());
    	
    	assignedCase = [Select Id, Status, Assigned_To__c, Assignee__c from Case where Id = :assignedCase.Id];
    	System.assert(assignedCase.Status == 'Cancelled');
    	System.assert(assignedCase.Assigned_To__c == null);
    	System.assert(assignedCase.Assignee__c == null);
    }
    
    private static testmethod void testRejectAssignmentCaseActivity(){
    	
    	createTestData();
    	
    	Test.startTest();
    	
    	Case assignedCase = [Select Id from Case where Status = 'Assigned'];
    	
    	ctrl_Activity_Scheduling_Activity.CaseActivity caseRequest = ctrl_Activity_Scheduling_Activity.getCase(assignedCase.Id);
    	    	
    	Case caseRecord = caseRequest.caseRecord;
    	caseRecord.Subject = 'Test Subject';
    	caseRecord.Date_Received_Date__c = Date.today();
    	caseRecord.Procedure__c = 'New Implant';
    	caseRecord.Product_Group__c = [Select Id from Product_Group__c].Id;
    	caseRecord.Assignee__c = [Select Id from User where Profile.Name = 'EUR Field Force CVG' AND isActive = true LIMIT 1].Id;
    	   	
    	ctrl_Activity_Scheduling_Activity.saveCase(caseRecord, caseRecord.Activity_Scheduling_Attendees__r);
    	
    	caseRecord.Assigned_To__c = null;
    	caseRecord.Status = 'Open';
    	
    	Boolean hasError = false;
    	
    	try{
    	
    		ctrl_Activity_Scheduling_Activity.saveCase(caseRecord, new List<Activity_Scheduling_Attendee__c>());
    		
    	}catch(Exception e){
    		
    		hasError = true;
    		System.assert(e.getMessage().contains('You need to provide a reason for the rejection of a Request assigned to you.'));
    	}
    	
    	System.assert(hasError == true);
    	
    	caseRecord.Reason_for_Reject_Cancel_Assigned_to__c = 'Test rejection reason';
    	
    	ctrl_Activity_Scheduling_Activity.saveCase(caseRecord, new List<Activity_Scheduling_Attendee__c>());
    	
    	assignedCase = [Select Id, Status, Assigned_To__c, Assignee__c from Case where Id = :assignedCase.Id];
    	System.assert(assignedCase.Status == 'Open');
    	System.assert(assignedCase.Assigned_To__c == null);
    	System.assert(assignedCase.Assignee__c != null);
    }
    
    private static testmethod void testUpdateEventActivity(){
    	
    	createTestData();
    	
    	Test.startTest();
    	
    	Event userEvent = [Select Id from Event where OwnerId = :UserInfo.getUserId() AND WhatId = null];
    	
    	ctrl_Activity_Scheduling_Activity.EventActivity personalEvent = ctrl_Activity_Scheduling_Activity.getEvent(userEvent.Id);
    	System.assert(personalEvent.canEdit == true);
    	
    	Event eventRecord = personalEvent.eventRecord;
    	eventRecord.Subject = 'Modified Subject';
    	
    	ctrl_Activity_Scheduling_Activity.saveEvent(eventRecord);
    	userEvent = [Select Id, Subject from Event where Id = :userEvent.Id];
    	System.assert(userEvent.Subject == 'Modified Subject');    	
    }
    
    private static testmethod void testCreateCaseActivity(){
    	
    	createTestData();
    	
    	Test.startTest();
    	
    	Account acc = [Select Id from Account];
    	Implant_Scheduling_Team__c team = [Select Id from Implant_Scheduling_Team__c];		
		Id caseActivitySchedulingRT = [Select Id from RecordType where SObjectType = 'Case' AND DeveloperName = 'Implant_Scheduling'].Id;
				
    	Case caseRecord = new Case();
    	caseRecord.AccountId = acc.Id;			
		caseRecord.Start_of_Procedure__c = DateTime.newInstance(Date.today(), Time.newInstance(14, 0, 0, 0)); 
		caseRecord.Procedure_Duration_Implants__c = '02:00';
		caseRecord.RecordTypeId = caseActivitySchedulingRT;
		caseRecord.Activity_Scheduling_Team__c = team.Id;
		caseRecord.Type = 'Implant Support Request';
		caseRecord.Status = 'Open';
    	caseRecord.Subject = 'Test Subject';
    	caseRecord.Date_Received_Date__c = Date.today();
    	caseRecord.Procedure__c = 'Replacement';
    	caseRecord.Product_Group__c = [Select Id from Product_Group__c].Id;
    	
    	ctrl_Activity_Scheduling_Activity.saveCase(caseRecord, new List<Activity_Scheduling_Attendee__c>());
    	caseRecord = [Select Id, Subject from Case where Id = :caseRecord.Id];   
    	    	
    	ctrl_Activity_Scheduling_Activity.removeCase(caseRecord);
    	List<Case> caseList = [Select Id, Subject from Case where Id = :caseRecord.Id];  
    	System.assert(caseList.size() == 0);
    }
    
    private static testmethod void testCreateRecurringCaseActivity(){
    	
    	createTestData();
    	
    	Test.startTest();
    	
    	Account acc = [Select Id from Account];
    	Implant_Scheduling_Team__c team = [Select Id from Implant_Scheduling_Team__c];		
		Id caseActivitySchedulingRT = [Select Id from RecordType where SObjectType = 'Case' AND DeveloperName = 'Implant_Scheduling'].Id;
				
    	Case caseRecord = new Case();
    	caseRecord.AccountId = acc.Id;			
		caseRecord.Start_of_Procedure__c = DateTime.newInstance(Date.today().toStartOfWeek(), Time.newInstance(14, 0, 0, 0)); 
		caseRecord.Procedure_Duration_Implants__c = '02:00';
		caseRecord.RecordTypeId = caseActivitySchedulingRT;
		caseRecord.Activity_Scheduling_Team__c = team.Id;
		caseRecord.Type = 'Implant Support Request';
		caseRecord.Status = 'Open';
    	caseRecord.Subject = 'Test Subject';
    	caseRecord.Date_Received_Date__c = Date.today();
    	caseRecord.Procedure__c = 'Replacement';
    	caseRecord.Product_Group__c = [Select Id from Product_Group__c].Id;
    	caseRecord.Is_Recurring__c = true;
    	caseRecord.Recurring_End_Date__c = Date.today().toStartOfWeek().addDays(27);
    	caseRecord.Recurring_N_Weeks__c = '2';
    	caseRecord.Recurring_Week_Days__c = '1;2;3;4;5';
    	
    	ctrl_Activity_Scheduling_Activity.saveCase(caseRecord, new List<Activity_Scheduling_Attendee__c>());
    	caseRecord = [Select Id, Subject from Case where Id = :caseRecord.Id];   
    	
    	List<Case> caseList = [Select Id from Case where ParentId = :caseRecord.Id];  
    	System.assert(caseList.size() == 10);
    }
    
    private static testmethod void testCreateCaseActivity_Validations(){
    	
    	createTestData();
    	
    	Test.startTest();
    	
    	Account acc = [Select Id from Account];
    	Implant_Scheduling_Team__c team = [Select Id from Implant_Scheduling_Team__c];		
		Id caseActivitySchedulingRT = [Select Id from RecordType where SObjectType = 'Case' AND DeveloperName = 'Implant_Scheduling'].Id;
				
    	Case caseRecord = new Case();
    	caseRecord.AccountId = acc.Id;			
		caseRecord.Start_of_Procedure__c = DateTime.newInstance(Date.today(), Time.newInstance(14, 0, 0, 0)); 
		caseRecord.Procedure_Duration_Implants__c = '02:00';
		caseRecord.RecordTypeId = caseActivitySchedulingRT;
		caseRecord.Activity_Scheduling_Team__c = team.Id;
		caseRecord.Type = 'Implant Support Request';
		caseRecord.Status = 'Open';
    	caseRecord.Subject = 'Test Subject';
    	
    	Boolean hasError = false;
    	
    	try{
    	
    		ctrl_Activity_Scheduling_Activity.saveCase(caseRecord, new List<Activity_Scheduling_Attendee__c>());
    		
    	}catch(Exception e){
    		
    		hasError = true;
    		System.assert(e.getMessage().contains('Field Request Date is mandatory'));
    	}
    	
    	System.assert(hasError == true);
    	
    	caseRecord.Date_Received_Date__c = Date.today();
    	
    	hasError = false;
    	
    	try{
    	
    		ctrl_Activity_Scheduling_Activity.saveCase(caseRecord, new List<Activity_Scheduling_Attendee__c>());
    		
    	}catch(Exception e){
    		
    		hasError = true;
    		System.assert(e.getMessage().contains('Field Procedure is mandatory'));
    	}
    	
    	System.assert(hasError == true);
    	
    	caseRecord.Procedure__c = 'Replacement';
    	
    	hasError = false;
    	
    	try{
    	
    		ctrl_Activity_Scheduling_Activity.saveCase(caseRecord, new List<Activity_Scheduling_Attendee__c>());
    		
    	}catch(Exception e){
    		
    		hasError = true;
    		System.assert(e.getMessage().contains('Field Product Group is mandatory'));
    	}
    	
    	System.assert(hasError == true);
    	    	
    	caseRecord.Product_Group__c = [Select Id from Product_Group__c].Id;
    	caseRecord.Type = 'Service Request';
    	
    	hasError = false;
    	
    	try{
    	
    		ctrl_Activity_Scheduling_Activity.saveCase(caseRecord, new List<Activity_Scheduling_Attendee__c>());
    		
    	}catch(Exception e){
    		
    		hasError = true;
    		System.assert(e.getMessage().contains('Field Activity Type is mandatory'));
    	}
    	
    	System.assert(hasError == true);
    }
    
    private static testmethod void testCreateEventActivity(){
    	
    	Test.startTest();
    	
    	Id eventActivitySchedulingRT = [Select Id from RecordType where SObjectType = 'Event' AND DeveloperName = 'Activity_Scheduling'].Id;
    	
    	Event eventRecord = new Event();
    	eventRecord.Subject = 'Event Subject';
    	eventRecord.StartDateTime = DateTime.newInstance(Date.today(), Time.newInstance(8, 0, 0, 0)); 
		eventRecord.EndDateTime = DateTime.newInstance(Date.today(), Time.newInstance(14, 0, 0, 0));
		eventRecord.RecordTypeId = eventActivitySchedulingRT;
    	
    	ctrl_Activity_Scheduling_Activity.saveEvent(eventRecord);
    	eventRecord = [Select Id, Subject from Event where Id = :eventRecord.Id];    	
    	
    	ctrl_Activity_Scheduling_Activity.removeEvent(eventRecord);
    	
    	List<Event> eventList = [Select Id, Subject from Event where Id = :eventRecord.Id];
    	System.assert(eventList.size() == 0);
    }
    
    private static testmethod void testCreateEventActivity_Validations(){
    	
    	Test.startTest();
    	
    	Id eventActivitySchedulingRT = [Select Id from RecordType where SObjectType = 'Event' AND DeveloperName = 'Activity_Scheduling'].Id;
    	
    	Event eventRecord = new Event();    	
    	eventRecord.StartDateTime = DateTime.newInstance(Date.today(), Time.newInstance(14, 0, 0, 0)); 
		eventRecord.EndDateTime = DateTime.newInstance(Date.today(), Time.newInstance(8, 0, 0, 0));
		eventRecord.RecordTypeId = eventActivitySchedulingRT;
    	
    	Boolean hasError = false;
    	
    	try{
    		
    		ctrl_Activity_Scheduling_Activity.saveEvent(eventRecord);
    		
    	}catch(Exception e){
    		
    		hasError = true;
    		System.assert(e.getMessage().contains('Field Subject is mandatory'));
    	}
    	
    	System.assert(hasError == true);
    	
    	eventRecord.Subject = 'Event Subject';
    	
    	hasError = false;
    	
    	try{
    		
    		ctrl_Activity_Scheduling_Activity.saveEvent(eventRecord);
    		
    	}catch(Exception e){
    		
    		hasError = true;
    		System.assert(e.getMessage().contains('End cannot be equal of before Start'));
    	}
    	
    	System.assert(hasError == true);
    }
        
    private static testmethod void testGetTeamMembers(){
    	    	
    	Test.startTest();
    	
    	Implant_Scheduling_Team__c team = [Select Id from Implant_Scheduling_Team__c];
    	List<User> results = ctrl_Activity_Scheduling_Activity.getTeamUsers(team.Id);
    	System.assert(results.size() == 1);
    }
    
    private static testmethod void testGetContacts(){
    	
    	createTestData();
    	
    	Test.startTest();
    	
    	Account acc = [Select Id from Account];
    	
    	List<Contact> results = ctrl_Activity_Scheduling_Activity.getContactsForAccount(acc.Id);
    	System.assert(results.size() == 1);    	
    }
    
    private static testmethod void testProductGroups(){
    	
    	createTestData();
    	
    	Test.startTest();
    	
    	Sub_Business_Units__c AFSolutions = [Select Id from Sub_Business_Units__c where Name = 'AF Solutions'];
    	
    	List<Product_Group__c> results = ctrl_Activity_Scheduling_Activity.getSBUProductGroups(AFSolutions.Id);
    	System.assert(results.size() == 1);    	
    }
    
    private static void createTestData(){
		
		clsTestData_MasterData.createSubBusinessUnit();
		
		Sub_Business_Units__c AFSolutions = [Select Id, Business_Unit__c, Business_Unit__r.Business_Unit_Group__c, Business_Unit__r.Company__c from Sub_Business_Units__c where Name = 'AF Solutions'];
		
		Therapy_Group__c phasedRF = new Therapy_Group__c();
        phasedRF.Name = 'Phased RF';
        phasedRF.Sub_Business_Unit__c = AFSolutions.Id;
        phasedRF.Company__c = AFSolutions.Business_Unit__r.Company__c;                        
        insert phasedRF;
                        
        Therapy__c phasedRFTherapy = new Therapy__c();
        phasedRFTherapy.Name = 'Phased RF';
        phasedRFTherapy.Business_Unit__c = AFSolutions.Business_Unit__c;
        phasedRFTherapy.Sub_Business_Unit__c = AFSolutions.id;
        phasedRFTherapy.Therapy_Group__c = phasedRF.id;  
        phasedRFTherapy.Therapy_Name_Hidden__c = 'Phased RF';             
        insert phasedRFTherapy;
		
		Product_Group__c productGroup = new Product_Group__c();
        productGroup.Name = 'Hardware pRF';
        productGroup.Therapy_ID__c = phasedRFTherapy.Id;
        insert productGroup;
		
		Account acc = new Account();
		acc.Name = 'Test Account';
		acc.Account_Country_vs__c = 'NETHERLANDS';		
		insert acc;
		
		Contact contact = new Contact();
        contact.LastName = 'Contact' ;  
        contact.FirstName = 'Test';
        contact.AccountId = acc.Id ;  
        contact.Phone = '009569699'; 
        contact.Email ='test@coverage.com';
        contact.Contact_Department__c = 'Diabetes Adult'; 
        contact.Contact_Primary_Specialty__c = 'ENT';
        contact.Affiliation_To_Account__c = 'Employee';
        contact.Primary_Job_Title_vs__c = 'Manager';
        contact.Contact_Gender__c = 'Male';
        contact.Primary_Job_Title_vs__c = 'Administrator';
        insert contact ;  
		
		Implant_Scheduling_Team__c team = [Select Id from Implant_Scheduling_Team__c];
		
		Id caseActivitySchedulingRT = [Select Id from RecordType where SObjectType = 'Case' AND DeveloperName = 'Implant_Scheduling'].Id;
										
		Case assignedCase = new Case();		
		assignedCase.AccountId = acc.Id;			
		assignedCase.Start_of_Procedure__c = DateTime.newInstance(Date.today(), Time.newInstance(10, 0, 0, 0)); 
		assignedCase.Procedure_Duration_Implants__c = '03:00';
		assignedCase.RecordTypeId = caseActivitySchedulingRT;
		assignedCase.Activity_Scheduling_Team__c = team.Id;
		assignedCase.Assigned_To__c = UserInfo.getUserId();
		assignedCase.Type = 'Implant Support Request';
		assignedCase.Status = 'Assigned';
						
		bl_Case_Trigger.runningSchedulingApp = true;
		
		insert assignedCase;
		
		Activity_Scheduling_Attendee__c attendee = new Activity_Scheduling_Attendee__c();
		attendee.Case__c = assignedCase.Id;
		attendee.Attendee__c = [Select Id from User where Profile.Name = 'EUR Field Force CVG' AND isActive = true LIMIT 1].Id;
		
		insert attendee;
		
		Id eventActivitySchedulingRT = [Select Id from RecordType where SObjectType = 'Event' AND DeveloperName = 'Activity_Scheduling'].Id;
								
		Event personalEvent = new Event();
		personalEvent.Subject = 'Test Subject';	
		personalEvent.StartDateTime = DateTime.newInstance(Date.today(), Time.newInstance(12, 0, 0, 0)); 
		personalEvent.EndDateTime = personalEvent.StartDateTime.addHours(5);
		personalEvent.RecordTypeId = eventActivitySchedulingRT;
		personalEvent.OwnerId = UserInfo.getUserId();
		
		insert personalEvent;
	}
	
	@TestSetup
	private static void setData() {
		
		Mobile_App__c app = new Mobile_App__c();
		app.Name = 'Activity Scheduling Tool';
		app.Active__c = true;
		app.Unique_Key__c = 'ACT_SCHDL';
		insert app;
		
		List<Implant_Scheduling_Team__c> teams = new List<Implant_Scheduling_Team__c>();
		
		Id activitySchedulingTeamRT = [Select Id from RecordType where SObjectType = 'Implant_Scheduling_Team__c' AND DeveloperName = 'Activity_Scheduling_Team'].Id;
		
		Implant_Scheduling_Team__c team = new Implant_Scheduling_Team__c();			
		team.Name = 'Test Team 1';
		team.Team_Color__c = 'rgb(99, 99, 99)';
		team.Work_Day_End__c = '18';
		team.Work_Day_Start__c = '8';
		team.Working_Days__c = '1:5';			
		team.RecordTypeId = activitySchedulingTeamRT;
		insert team;
			
		Implant_Scheduling_Team_Member__c member = new Implant_Scheduling_Team_Member__c();
		member.Team__c = team.Id;
		member.Member__c = UserInfo.getUserId();				
		
		insert member;	
	} 
}