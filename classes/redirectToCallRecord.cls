/**
 *  Creation Date : 28/05/2009
 *  Description : Redirect Buttons to pages for Visit Report Like Edit button and New Button ! 
 *  Author : ABSI / Miguel Coimbra
 *
 */

public with sharing class redirectToCallRecord {
    public Call_Records__c cr ; 
    
    public redirectToCallRecord (ApexPages.StandardController controller ) {
        this.cr = (Call_Records__c) controller.getRecord();
    }
    
    public PageReference GoToEditPage(){
        try{
            Savepoint sp = Database.setSavepoint(); 
            update cr;
            Database.rollback(sp); 
        } catch(DmlException ex){
          ApexPages.addMessages(ex);
          return null;
        }  
        cr=[select id,recordtypeid from Call_Records__c where id=:cr.id];
                system.debug('####RTName'+cr.recordtypeid);
                Schema.DescribeSObjectResult d = Schema.SObjectType.Call_Records__c;
                Map<id,Schema.RecordTypeInfo> rtMapById = d.getRecordTypeInfosById();
                // system.debug('crRecTypeName'+rtMapById.get(URLParams.get('RecordType')).name);
                //system.debug('###RTName'+rtMapById.get(cr.recordtypeid).name);
               
                if((cr.recordtypeid!=null) && (rtMapById.get(cr.recordtypeid).name=='xBU' ||rtMapById.get(cr.recordtypeid).name=='Marketing'))
                {
                    string crRecTypeid=(string)cr.recordtypeid;
                    PageReference pageRef = new PageReference('/apex/SAMvisitReportPage?retURL=%2F'+cr.Id+ '&id='+ cr.Id +'&sfdc.override=1'+'&rt='+crrectypeid.substring(0,15));
                    pageRef.setRedirect(true);
                    return pageRef;
                }
                else{ 
                PageReference pageRef;
                    if(cr.Recordtypeid!=null)
                    {    
                    string crRecTypeid=(string)cr.recordtypeid;           
                     pageRef = new PageReference('/apex/visitReportPage?retURL=%2F'+cr.Id+ '&id='+ cr.Id +'&sfdc.override=1'+'&RecordType='+crrectypeid.substring(0,15));
                    }
                    else
                    {
                     pageRef = new PageReference('/apex/visitReportPage?retURL=%2F'+cr.Id+ '&id='+ cr.Id +'&sfdc.override=1');
                   
                    }
                    pageRef.setRedirect(true);
                    return pageRef;
                }
    }
    
}