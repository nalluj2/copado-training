/**
 * 
 */
@isTest
private class Test_dao_CollaborationGroup {
    private static final String COLLABOGROUPNAME = 'LIONS';
    private static final String COLLABOGROUPNAME2 = 'TIGERS';

    static testMethod void myUnitTest() {
        //generating test object 
        List<CollaborationGroup> cglist = new List<CollaborationGroup>(); 
        CollaborationGroup cg = new CollaborationGroup(CollaborationType = 'public',Name=COLLABOGROUPNAME);
        cglist.add(cg);
        cg = new CollaborationGroup(CollaborationType = 'private',Name=COLLABOGROUPNAME2);
        cglist.add(cg);
        insert cglist;
        
        cgList.clear();
        List<String> nameList = new List<String>(); 
        nameList.add(COLLABOGROUPNAME);
        nameList.add(COLLABOGROUPNAME2);
        //test
        cglist = dao_CollaborationGroup.getCollaborationGroupByNames(nameList);
        
        System.assertEquals(2,cglist.size());
    }
}