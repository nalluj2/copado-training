@RestResource(urlMapping='/UpdateEntity')
global class TwoRing_UpdateEntityController {
	@HttpPost
	global static String updateEntity(TwoRing_CreateOrUpdateEntityObject entityObject) {
		return TwoRing_Repository.updateEntity(entityObject);
	}
}