/*
 *      Description : This is the APEX Class that holds Business Logic for Activities (shared for Taks & Event)
 *      Version     : 1.0
 *      Author      : Bart Caelen
 *      Date        : 20140811
*/
public  class bl_Activity {
	
	
	public static boolean sendNotificationTOMMX=false;
	
	//------------------------------------------------------------------------------------------------
	// This method is used in a trigger on Task and Event and it will update the parent object 
	//	with a value that represents the number of related Activities (Open and History)
	//------------------------------------------------------------------------------------------------
	public static List<sObject> updateParentWithRecordCountOfChildActivities(List<sObject> lstSObject) {
		return updateParentWithRecordCountOfChildActivities(lstSObject, new Map<Id, sObject>());
	}

	public static List<sObject> updateParentWithRecordCountOfChildActivities(List<sObject> lstSObject, Map<Id, sObject> mapSObject_Old) {

		List<sObject> lstResult = new List<sObject>();
		try{
			clsUtil.bubbleException();

			Set<String> setChildObjectName = new Set<String>();
				setChildObjectName.add('Task');
				setChildObjectName.add('Event');
			
			//------------------------------------------------------------------------------
			// Parameters for Account_Plan_2__c related logic
			//------------------------------------------------------------------------------
			String tParentSObjectName = 'Account_Plan_2__c';
			String tChildParentField = 'WhatID';
			String tParentFieldToUpdate = 'Number_of_Activities__c';
	        String tSObjectPrefix = clsUtil.getPrefixFromSObjectName(tParentSObjectName);
	        String tParentFilter = '';
	        Set<String> setParentID = getParentIds(lstSObject, mapSObject_Old, tSObjectPrefix, tChildParentField);

            if (setParentID.size() > 0){
                Triggers_Soft_Deactivation.tr_AccountPlan2_AfterInsertUpdate = true;
                bl_AccountPlanning.escapeDupValidation = true;
                lstResult = clsUtil.updateParentWithRecordCountOfChild(tParentSObjectName, 'Id', tParentFieldToUpdate, setParentID, setChildObjectName, tChildParentField, true, tParentFilter);
                Triggers_Soft_Deactivation.tr_AccountPlan2_AfterInsertUpdate = false;
                bl_AccountPlanning.escapeDupValidation = false;
            }
			//------------------------------------------------------------------------------



			//------------------------------------------------------------------------------
			// Parameters for Account_Plan_Objective__c related logic - RTG
			//------------------------------------------------------------------------------
			tParentSObjectName = 'Account_Plan_Objective__c';
			tChildParentField = 'WhatID';
			tParentFieldToUpdate = 'Number_of_Activities__c';
	        tSObjectPrefix = clsUtil.getPrefixFromSObjectName(tParentSObjectName);
			tParentFilter = '';
/*		
			tParentFilter = '(Account_Plan_2__r.IdRecordTypeId in (\'' 
				+ clsUtil.getRecordTypeByDevName('Account_Plan_2__c', 'EUR_RTG_sBU').Id
				+ '\',\'' + 
				+ clsUtil.getRecordTypeByDevName('Account_Plan_2__c', 'EUR_RTG_BUG').Id
				+ '\')';
*/
	        setParentID = getParentIds(lstSObject, mapSObject_Old, tSObjectPrefix, tChildParentField);

            if (setParentID.size() > 0){
                lstResult = clsUtil.updateParentWithRecordCountOfChild(tParentSObjectName, 'Id', tParentFieldToUpdate, setParentID, setChildObjectName, tChildParentField, true, tParentFilter);
            }
			//------------------------------------------------------------------------------


		}catch(Exception oEX){
			lstResult = new List<sObject>();
		}
		return lstResult;
	}

	private static Set<String> getParentIds(List<sObject> lstSObject, Map<Id, sObject> mapSObject_Old, String tSObjectPrefix, String tChildParentField) {
	    
	    Set<String> setParentID = new Set<String>();

	    if (lstSObject.size() > 0){
	        for (sObject oSObject : lstSObject){
	        	if (mapSObject_Old != null && mapSObject_Old.size()>0){
	            	sObject oSObject_Old = mapSObject_Old.get(String.valueOf(oSObject.get('Id')));
	            	
	            	if (oSObject.get(tChildParentField) != oSObject_Old.get(tChildParentField)){
		                if (oSObject_Old.get(tChildParentField) != null){
		                    if (String.valueOf(oSObject_Old.get(tChildParentField)).startsWith(tSObjectPrefix)){
		                        setParentID.add(String.valueOf(oSObject_Old.get(tChildParentField)));
		                    }
		                }
	            	}
	        	}
	            if (oSObject.get(tChildParentField) != null){
	                if (String.valueOf(oSObject.get(tChildParentField)).startsWith(tSObjectPrefix)){
	                    setParentID.add(String.valueOf(oSObject.get(tChildParentField)));
	                }
	            }
	        }		
		}

		return setParentID;
	}
	//------------------------------------------------------------------------------------------------

/*
	
	public static void changeNotification(List<Event> eventsToProcess, String operation){
		
		//Check if interface is activated
		Boolean sendSFDCToMMX = ws_SharedMethods.sendOutboundMessage('MMX');
		System.debug('Sending to mmx, interface status: '+sendSFDCToMMX);
		List<Id> eventsforNotification = new List<Id>();
		if (sendSFDCToMMX && !sendNotificationTOMMX && !system.isFuture()){
			for (Event e : eventsToProcess){
				
				//check if whatId
				if (e.WhatId != null && String.valueOf(e.WhatId).startsWith('500') && e.Type=='Implant Support' && e.isChild == false){
					eventsforNotification.add(e.id);	
					System.debug('Adding event to send to mmx: '+e);
				}else{
					System.debug('Events not added to send to mmx as not Implant support or case attached or assigned to secondary attendee: '+e);
				}
				 
				
			}
		}
		
		if (eventsforNotification.size()>0){
			System.debug('Sending to mmx: '+eventsforNotification);
			sendNotification(eventsforNotification,operation);
			sendNotificationTOMMX = true;
		}
		
	}
	
	
	@future(callout=true)
	public static void sendNotification(List<Id> eventIds, String operation){
		
		// Counter for looping, Governor limit allows 10 callouts
		Integer maxLoops = 0;
		// List of Event that have been sent
		List<Event> listOfEventsToUpdate = new List<Event>();
		for (ID thisEventId: eventIds){
			String action = operation;
			
			Organization currOrg = [Select IsSandbox from Organization limit 1];
			
			String environment;
			//hostname production environment has eu in it currently eu3.salesforce.com
			if (currOrg.isSandbox == true){
				environment = 'https://test.salesforce.com';
			}else{		 		
		 		environment = 'https://login.salesforce.com';
			}
			
			String jobid = 'ImplantSchedule';
			String requestvalue = thisEventid;
			
			ws_ChangeNotificationToMMX.Service1Soap port = new ws_ChangeNotificationToMMX.Service1Soap();
			
			// Retrieve endpoint and timeout
			Map<String, String> mapOfValues = ws_SharedMethods.getOutboundWebServiceEndpointAndTimeout('MMX');
 			if ( (mapOfValues!=null) && (mapOfValues.size()>0) ){
				port.timeout_x = Integer.valueof(mapOfValues.get('Timeout'));
				port.endpoint_x = mapOfValues.get('Endpoint');				
			}
			
			Outbound_Message__c outboundMessage = new Outbound_Message__c();
			String request = '';
			request+=' Objectname: Implant';
			outboundMessage.Object__c =  'Implant';
			request+=' Operation: '+operation; 

			request+=' User '+UserInfo.getFirstName() + ' '+UserInfo.getLastName()+' ('+UserInfo.getUserName()+')';
			outboundMessage.Request__c = request;
			outboundMessage.Operation__c = 'EventNotification';

			outboundMessage.User__c = UserInfo.getUserId();
			Event thisEvent = new Event(Id=thisEventId);
			try{
				String wsResponse = port.setImplantScheduling(jobid, requestvalue, environment, action);

				System.debug('*** wsResponse: ' + wsResponse);
				
				outboundMessage.Response__c = wsResponse;
				
				// Validate response
				if (wsResponse.startsWith('OK') && operation!='Delete'){
					thisEvent.Distribution_Status__c = 'SUCCESS';
					thisEvent.Distribution_Error_Details__c = '';
					listOfEventsToUpdate.add(thisEvent);
				}
			
			}catch(Exception e){
				outboundMessage.Response__c = e.getMessage();
				outboundMessage.Status__c = 'false';
				thisEvent.Distribution_Status__c = 'SUCCESS';
				thisEvent.Distribution_Error_Details__c = e.getMessage();
			}
			insert outboundMessage;
			
			// For governor limits, only
			maxLoops++;
			if (maxLoops >= 10) continue;		
		}		
		// Update event (Distribution status)
		if (!listOfEventsToUpdate.isEmpty()){
			
			bl_Case_Trigger.runningSchedulingApp = true;
			
			update listOfEventsToUpdate;
		}
	}
*/	
	public static Event saveEvent(Event event){
		
		if (null != event) upsert event Mobile_ID__c;
				
		return event;		
	} 	



    public static void validateActiveAccount(List<sObject> lstTriggerNew, String tType){

		if (bl_Trigger_Deactivation.isTriggerDeactivated('activity_validateActiveAccount')) return;

		// Do not excecute for System Administrator, IT Business Anallist and Interfase users
		if ( 
			(ProfileMedtronic.isSystemAdministratorProfile(UserInfo.getProfileId())) 
			|| (ProfileMedtronic.isBusinessAnalystProfile(UserInfo.getProfileId())) 
			|| (ProfileMedtronic.isSAPInterface(UserInfo.getProfileId())) 
			|| (ProfileMedtronic.isMMXInterface(UserInfo.getProfileId())) 
		){
 			return;
		}

    	Set<Id> setID_Account = new Set<Id>();

		for (sObject oSObject : lstTriggerNew){

			if (oSObject.get('WhatId') != null){

				String tWhatID = String.valueOf(oSObject.get('WhatId'));
				if (tWhatID.startsWith('001')){
					setID_Account.add(tWhatID);
				}

			}

		}        

		Map<Id, Account> mapAccount = new Map<Id, Account>([SELECT Id FROM Account WHERE Id = :setID_Account AND SAP_Search_Term1__c = 'DISABLED']);

		for (sObject oSObject : lstTriggerNew){

			if (oSObject.get('WhatId') != null){

				String tWhatID = String.valueOf(oSObject.get('WhatId'));
				if (tWhatID.startsWith('001')){

					if (mapAccount.containsKey(tWhatID)){
						//  Account is disabled - show error
						if (tType == 'Event'){
	
							oSObject.addError('You can\'t create an Event linked to a disabled Account');
	
						}else if (tType == 'Task'){

							oSObject.addError('You can\'t create a Task linked to a disabled Account');

						}
					}

				}

			}

		}

    }

}