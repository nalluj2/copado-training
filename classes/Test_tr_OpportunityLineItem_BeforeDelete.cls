/*
 * Work Item Nr     : WI-336
 * Description      : WI-336 - Test Class for tr_OpportunityLineItem_BeforeDelete        
 * Author           : Patrick Brinksma
 * Created Date     : 03-09-2013
 */
 @isTest(seeAllData=true)
private class Test_tr_OpportunityLineItem_BeforeDelete {

    static testMethod void testOpportunityLineItemDelete() {
        // Set Mock WS response
        Test.setMock(WebServiceMock.class, new Test_ws_SAPOpportunityNotificationMockup());

        // Validate if Test User has a User Business Unit set to primary 
        List<User_Business_Unit__c> listOfSBUser = [select Id, Primary__c from User_Business_Unit__c where User__c = :Userinfo.getUserId()];
        if (listOfSBUser.isEmpty()){
            Id subUnitId = [select Id from Sub_Business_Units__c where name = 'Diabetes Core' and Business_Unit__r.Company__r.Company_Code_Text__c = 'EUR'].Id;
            User_Business_Unit__c sbUser = new User_Business_Unit__c();
            sbUser.User__c = Userinfo.getUserId();
            sbUser.Sub_Business_Unit__c = subUnitId;
            sbUser.Primary__c = true;
            insert sbUser;
        } else {
            // Validate if there is a primary already
            Boolean setPrimary = true;
            for (User_Business_Unit__c userBU : listOfSBUser){
                if (userBU.Primary__c == true){
                    setPrimary = false;
                    continue;
                }
            }
            if (setPrimary){
                listOfSBUser[0].Primary__c = true;
                update listOfSBUser;
            }
        }
        
        // Create test person account
        String randVal = Test_TestDataUtil.createRandVal();
        List<Account> listOfAccount = Test_TestDataUtil.createSAPPersonAccounts(randVal, 1);
        insert listOfAccount;
        // Create test Opportunity
        List<String> listOfRecordTypeDevName = new List<String>{'CAN_DIB_OPPORTUNITY'};
        List<Opportunity> listOfOppty = Test_TestDataUtil.createOpportunities(randVal, listOfAccount, listOfRecordTypeDevName, 1);
        insert listOfOppty;
        // Add lineitem
        Opportunity thisOppty = [select Id, Pricebook2Id from Opportunity where Id = :listOfOppty[0].Id];

        //-BC - 20150629 - Added to be sure that the Test Class succeeds - START
        List<Pricebook2> lstPricebook = [SELECT ID FROM Pricebook2 WHERE Name = 'CAN DiB Pricebook'];
        // Create Pricebook and entry for product 
        List<PricebookEntry> listOfPBEntry = Test_TestDataUtil.createPricebookEntries(lstPricebook[0].Id, 1);
        insert listOfPBEntry;

        // Create Pricebook and entry for product 
//      List<PricebookEntry> listOfPBEntry = Test_TestDataUtil.createPricebookEntries(thisOppty.Pricebook2Id, 1);
//      insert listOfPBEntry;
        //-BC - 20150629 - Added to be sure that the Test Class succeeds - STOP

        // Add product line
        OpportunityLineItem opptyLine1 = new OpportunityLineItem(OpportunityId = listOfOppty[0].Id,
                                                                PricebookEntryId = listOfPBEntry[0].Id,
                                                                Quantity = 1.0,
                                                                TotalPrice = 2500.0,
                                                                SAP_Item_ID_Text__c = 'SAPITEMIDX');   
        Test.startTest();
        
        insert opptyLine1;
        
        try{
            delete opptyLine1;
        } 
        catch (System.Dmlexception dmlEx){
            System.assert(dmlEx.getMessage().contains(Label.OpportunityLineItemDeleteError));
        }
        
        Test.stopTest();        
    }
}