/*
 *      Created Date : 20121001
 *      Description : This class is a utility class to use the recordtypes in other classes
 *
 *      Author = Rudy De Coninck
 *		Update: Patrick Brinksma - 13/08/2013 - Added allRecordTypes so this class can be used throughout the code
 *		Update: Bart Caelen - 06/01/2014 - Addedd mapObjectRecordTypeNamesRecordType so this class can be used to retreive a recordtype using 2 parameters: sObjectName & RecordType Developername
 */
 public class RecordTypeMedtronic {

	static Map<String,RecordType> accountRecordTypes;
	static Set<Id> personAccountRecordTypes;
	static Map<String, RecordType> allRecordTypes;
	static map<String, map<String, RecordType>> mapObjectRecordTypeNamesRecordType;		//-BC - 201401 - Added to store the objects with their Record Type Developer Names and the corresponding RecordType
	static map<String, RecordType> mapRecordTypeIDRecordType;							//-BC - 201401 - Added to store the recordtType ID and the corresponding RecordType
	static Map<String,RecordType> productRecordTypes;
	static Map<String,RecordType> opportunityRecordTypes;
	static Map<String,RecordType> caseRecordTypes;
	static Map<String,RecordType> iplan2RecordTypes;
	static Map<String,RecordType> campaignRecordTypes;
	static Map<String,RecordType> contactRecordTypes;
	static Map<String,RecordType> relationshipRecordTypes;
	static Map<String,RecordType> callRecordRecordTypes;
	static Map<String,RecordType> eventRecordTypes;

	static {
		accountRecordTypes = new Map<String,RecordType>{};
		allRecordTypes = new Map<String, RecordType>();
		mapObjectRecordTypeNamesRecordType = new map<String, Map<String, RecordType>>();	//-BC - 201401 - Added to store the objects with their Record Type Developer Names and the corresponding RecordType
		mapRecordTypeIDRecordType = new map<string, RecordType>();							//-BC - 201401 - Added to store the recordtType ID and the corresponding RecordType
		personAccountRecordTypes = new Set<ID>();
		productRecordTypes = new Map<String,RecordType>{};
		opportunityRecordTypes = new Map<String,RecordType>{};
		caseRecordTypes = new Map<String,RecordType>{};
		iplan2RecordTypes = new Map<String,RecordType>{};
		campaignRecordTypes = new Map<String,RecordType>{};
		contactRecordTypes = new Map<String,RecordType>{};
		relationshipRecordTypes = new Map<String,RecordType>{};
		callRecordRecordTypes = new Map<String,RecordType>{};
		eventRecordTypes = new Map<String,RecordType>{};
		init();		
	}
	
	public static void init(){

		//-BC - 201401 - Store the SOQL in a map so we can used that also to retrieve the recordtype based on the ID without perfoming the SOQL again - START		
//		List<RecordType> rtypes = [Select DeveloperName, Id, IsPersonType, sObjectType From RecordType  
//	                  where isActive=true];
//		//Create a map between the Record Type Name and Id for easy retrieval
//		for(RecordType rt: rtypes){
		
		mapRecordTypeIDRecordType = new map<string, RecordType>([Select DeveloperName, Id, name, IsPersonType, sObjectType From RecordType where isActive=true]);	                   
	     
	     //Create a map between the Record Type Name and Id for easy retrieval
	     for(RecordType rt: mapRecordTypeIDRecordType.values()){
		//-BC - 201401 - Store the SOQL in a map so we can used that also to retrieve the recordtype based on the ID without perfoming the SOQL again - STOP		

	     	allRecordTypes.put(rt.DeveloperName, rt);
	     	
	     	//-BC - 201401 - Added to store the objects with their Record Type Developer Names and the corresponding RecordType - START
	     	map<String, RecordType> mapRecordTypeNamesRecordType	= new map<String, RecordType>();
	     	if (mapObjectRecordTypeNamesRecordType.containsKey(rt.sObjectType)){
	     		mapRecordTypeNamesRecordType = mapObjectRecordTypeNamesRecordType.get(rt.sObjectType);
	     	}
	     	mapRecordTypeNamesRecordType.put(rt.DeveloperName, rt);
	     	mapObjectRecordTypeNamesRecordType.put(rt.sObjectType, mapRecordTypeNamesRecordType);
	     	//-BC - 201401 - Added to store the objects with their Record Type Developer Names and the corresponding RecordType - STOP
	     	
	        if (rt.sObjectType.compareTo('Account')==0){
	        	accountRecordTypes.put(rt.DeveloperName,rt);
	        	if (rt.IsPersonType){
	        		personAccountRecordTypes.add(rt.id);
	        	}
	        }
	        
	        if (rt.sObjectType.compareTo('Product2')==0){
	        	productRecordTypes.put(rt.DeveloperName,rt);
	        }
	        
	        if (rt.sObjectType.compareTo('Opportunity')==0){
	        	opportunityRecordTypes.put(rt.DeveloperName,rt);
	        }
	        
	        if (rt.sObjectType.compareTo('Case')==0){
	        	caseRecordTypes.put(rt.DeveloperName,rt);
	        }
	        
	        if (rt.sObjectType.compareTo('Account_Plan_2__c')==0){
	        	iplan2RecordTypes.put(rt.DeveloperName,rt);
	        }
	        
	        if (rt.sObjectType.compareTo('Campaign')==0){
	        	campaignRecordTypes.put(rt.DeveloperName,rt);
	        }
	        
	        if (rt.sObjectType.compareTo('Contact')==0){
	        	contactRecordTypes.put(rt.DeveloperName,rt);
	        }

	        if (rt.sObjectType.compareTo('Affiliation__c')==0){
	        	relationshipRecordTypes.put(rt.DeveloperName,rt);
	        }
	        
	        if (rt.sObjectType.compareTo('Call_Records__c')==0){
	        	callRecordRecordTypes.put(rt.DeveloperName,rt);
	        }
	        if (rt.sObjectType.compareTo('Event')==0){
	        	eventRecordTypes.put(rt.DeveloperName,rt);
	        }
	        
	     }
	}
	
	public static RecordType Campaign(string name){
		return campaignRecordTypes.get(name);
	}
	
	public static RecordType Iplan2(String name){
		return iplan2RecordTypes.get(name);
	}
	
	public static RecordType Account(String name){
		return accountRecordTypes.get(name);
	}
	
	public static RecordType Case(string name){
		return caseRecordTypes.get(name);
	}
	
	public static RecordType Opportunity(String name){
		return opportunityRecordTypes.get(name);
	}
	
	public static RecordType Product(String name){
		return productRecordTypes.get(name);
	}
	
	public static RecordType Contact(String name){
		return contactRecordTypes.get(name);
	}

	public static RecordType Relationship(String name){
		return relationshipRecordTypes.get(name);
	}

	public static RecordType CallRecord(String name){
		return callRecordRecordTypes.get(name);
	}
	
	public static RecordType Event(String name){
		return eventRecordTypes.get(name);
	}

	public static RecordType getRecordTypeByDevName(String devName){
		return allRecordTypes.get(devName);
	}

 	//-BC - 201401 - Added to retrieve the RecordType for a certain sObject and Record Type Developer Name or the RecordType ID - START
	public static RecordType getRecordTypeByDevName(String tObjectName, String tDevName){
		return mapObjectRecordTypeNamesRecordType.get(tObjectName).get(tDevName);
	}
	public static RecordType getRecordTypeById(String tRecordTypeId){
		return mapRecordTypeIDRecordType.get(tRecordTypeId);
	}
 	//-BC - 201401 - Added to retrieve the RecordType for a certain sObject and Record Type Developer Name or the RecordType ID - STOP
	
	public static boolean isPersonAccountRecordtype(ID id){
		return personAccountRecordTypes.contains(id);
	}
	
	public static testmethod void getSapAccountRecordType(){
		System.assert(RecordTypeMedtronic.Account('SAP_Account')!=null);
	}


}