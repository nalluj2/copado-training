//--------------------------------------------------------------------------------------------------------------------
//  Description : This TEST APEX Class is used to test the logic in ba_UserLoginHistory
//  Version     : 1.0
//  Author      : Bart Caelen
//  Date        : 07/09/2016
//--------------------------------------------------------------------------------------------------------------------
@isTest (SeeAllData=true)
private class Test_ba_UserLoginHistory {
	
    static Integer iBatchSize = 200;
    static String tSOQL = 'SELECT Id, ApiType, ApiVersion, Application, Browser, ClientVersion, LoginTime, LoginType, LoginUrl, Platform, SourceIp, Status, UserId FROM LoginHistory';

    @isTest static void test_ba_UserLoginHistory_Scheduling() {

        Test.startTest();

        //---------------------------------------
        // TEST SCHEDULING
        //---------------------------------------
        String tCRON_EXP = '0 0 0 3 9 ? 2099';
        
        String tJobId = System.schedule('ba_UserLoginHistory_TEST', tCRON_EXP, new ba_UserLoginHistory());

        // Get the information from the CronTrigger API object
        CronTrigger oCronTrigger = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :tJobId];

        // Verify the expressions are the same
        System.assertEquals(tCRON_EXP, oCronTrigger.CronExpression);

        // Verify the job has not run
        System.assertEquals(0, oCronTrigger.TimesTriggered);

        // Verify the next time the job will run
        System.assertEquals('2099-09-03 00:00:00', String.valueOf(oCronTrigger.NextFireTime));
        //---------------------------------------

        Test.stopTest();
    
    }

    @isTest static void test_ba_UserLoginHistory() {

        Test.startTest();

        //---------------------------------------
        // TEST BATCH
        //---------------------------------------

		String tQuery = 'SELECT ApiType,ApiVersion,Application,Browser,ClientVersion,Id,LoginTime,LoginType,LoginUrl,Platform,SourceIp,Status,UserId FROM LoginHistory where LoginTime >= LAST_WEEK LIMIT ' + iBatchSize;
    	ba_UserLoginHistory oBatch = new ba_UserLoginHistory(tQuery, iBatchSize);
        Database.executebatch(oBatch, iBatchSize);        	
        //---------------------------------------

        Test.stopTest();
    }   

}
//--------------------------------------------------------------------------------------------------------------------