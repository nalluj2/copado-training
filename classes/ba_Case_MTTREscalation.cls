global class ba_Case_MTTREscalation implements Schedulable,Database.Batchable<sObject>, Database.Stateful {

	private Set<Id> setIDRecordType = new Set<Id>{clsUtil.getRecordTypeByDevName('Case', 'New_Case').Id};
	private Set<String> setExcludedStatus_WO = new Set<String>{'Completed','Cancelled','Closed'};
	
	global String tSOQL = '';

	//------------------------------------------------------------------------------------------------
	// SCHEDULE Settings
	//------------------------------------------------------------------------------------------------
	global void execute(SchedulableContext sc){ 

		ba_Case_MTTREscalation oBatch = new ba_Case_MTTREscalation();
		Database.executebatch(oBatch, 200);

	}
	//------------------------------------------------------------------------------------------------


	//------------------------------------------------------------------------------------------------
	// BATCH Settings
	//------------------------------------------------------------------------------------------------
	global Database.QueryLocator start(Database.BatchableContext BC) {

		if (String.isBlank(tSOQL)){
			tSOQL = '';
			tSOQL = 'SELECT Id';
			tSOQL += ' FROM Case';
			tSOQL += ' WHERE RecordtypeId = :setIDRecordType';
			tSOQL += ' AND Complaint_Case__c = \'Yes\'';
			if (Test.isRunningTest()){
				tSOQL += ' LIMIT ' + String.valueOf(200);
			}
		}

		return Database.getQueryLocator(tSOQL);

	}

	global void execute(Database.BatchableContext BC, List<Case> lstCase) {

		Case_Logic.processMTTREscalation(lstCase, setExcludedStatus_WO);

	}
		
	global void finish(Database.BatchableContext BC) {


	}
	//------------------------------------------------------------------------------------------------

}