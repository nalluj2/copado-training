//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//	Created Date    : 09/10/2018
//  Author          : Bart Caelen
//  Description     : Controller Extension Class for the VisualForce Page iPlan2_ActivityOverview which displays 2 overviews of related Activities:
//		1. Open Activity
//			- contains all Open Activities (Task & Event) which are related to the Account_Plan_Objective__c which is related to the Account_Plan_2__c
//		2. Activity History
//			- contains all Closed/Completed Activities (Task & Event) which are related to the Account_Plan_Objective__c which is related to the Account_Plan_2__c
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
public class ctrlExt_iPlan2_ActivityOverview {

    //------------------------------------------------------------------------------------
    // Private Variables
    //------------------------------------------------------------------------------------
    private Account_Plan_2__c oIPlan2;
    private String tQuery_OpenActivity = '';
    private String tQuery_ActivityHistory = '';
    private List<OpenActivity> lstOpenActivity = new List<OpenActivity>();
    private List<ActivityHistory> lstActivityHistory = new List<ActivityHistory>();
    //------------------------------------------------------------------------------------


    //------------------------------------------------------------------------------------
    // Constructor
    //------------------------------------------------------------------------------------
    public ctrlExt_iPlan2_ActivityOverview(ApexPages.StandardController stdController) {
        this.oIPlan2    = (Account_Plan_2__c)stdController.getRecord();
        tURL_Redirect   = stdController.view().getUrl();

        initialize();

        loadData();
    }
    //------------------------------------------------------------------------------------


    //------------------------------------------------------------------------------------
    // GETTER / SETTER
    //------------------------------------------------------------------------------------
    public Boolean bShowButtons                     { get;private set; }
    public List<wr_Activity> lstWROpenActivity      { get;private set; }
    public List<wr_Activity> lstWRActivityHistory   { get;private set; }
    public String tURL_Redirect                     { get;private set; }
    public String tURL_NewTask                      { get;private set; }
    public String tURL_NewEvent                     { get;private set; }
    public String tURL_LogACall                     { get;private set; }
    public String tURL_MailMerge                    { get;private set; }
    public String tURL_SendAnEmail                  { get;private set; }
    public String tURL_ViewAll                      { get;private set; }
    public String tURL_General                      { get;private set; }
    //------------------------------------------------------------------------------------


    //------------------------------------------------------------------------------------
    // Private functions
    //------------------------------------------------------------------------------------
    private void initialize(){

        this.bShowButtons           = false;
        
        this.lstOpenActivity        = new List<OpenActivity>();
        this.lstActivityHistory     = new List<ActivityHistory>();
        this.lstWROpenActivity      = new List<wr_Activity>();
        this.lstWRActivityHistory   = new List<wr_Activity>();

        this.tURL_NewTask           = '/00T/e?who_id=' + UserInfo.getUserId() + '&what_id=' + oIPlan2.Id + '&retURL=%2F' + oIPlan2.Id;
        this.tURL_NewEvent          = '/00U/e?who_id=' + UserInfo.getUserId() + '&what_id=' + oIPlan2.Id + '&retURL=%2F' + oIPlan2.Id;
        this.tURL_LogACall          = '/00T/e?title=Call&who_id=' + UserInfo.getUserId() + '&what_id=' + oIPlan2.Id + '&followup=1&tsk5=Call&retURL=%2F' + oIPlan2.Id;
        this.tURL_MailMerge         = '/mail/mmchoose.jsp?id=' + oIPlan2.Id + '&1=' + oIPlan2.Name + '&retURL=%2F' + oIPlan2.Id;
        this.tURL_SendAnEmail       = '/_ui/core/email/author/EmailAuthor?p2_lkid=' + oIPlan2.Account__c + '&rtype=003&p3_lkid=' + oIPlan2.Id + '&retURL=%2F' + oIPlan2.Id;
        this.tURL_ViewAll           = '/ui/core/activity/ViewAllActivityHistoryPage?retURL=%2F' + oIPlan2.Id + '&id=' + oIPlan2.Id;
        this.tURL_General           = '/';

        tQuery_OpenActivity         = 'SELECT';
        tQuery_OpenActivity         += ' Id, Subject, WhoId, Who.Name, WhatID, What.Name, isTask, ActivityDate, Status, Priority, OwnerID, Owner.Name, LastModifiedDate, StartDateTime, EndDateTime, Campaign__c, Campaign__r.Name';
        tQuery_OpenActivity         += ' FROM OpenActivities';
        tQuery_OpenActivity         += ' ORDER BY ActivityDate ASC NULLS LAST, LastModifiedDate DESC';
        tQuery_OpenActivity         += ' LIMIT 499';

        tQuery_ActivityHistory      = 'SELECT';
        tQuery_ActivityHistory      += ' Id, Subject, WhoId, Who.Name, WhatID, What.Name, isTask, ActivityDate, Status, Priority, OwnerID, Owner.Name, LastModifiedDate, StartDateTime, EndDateTime, Campaign__c, Campaign__r.Name';
        tQuery_ActivityHistory      += ' FROM ActivityHistories';
        tQuery_ActivityHistory      += ' ORDER BY ActivityDate ASC NULLS LAST, LastModifiedDate DESC';
        tQuery_ActivityHistory      += ' LIMIT 499';

    }

    private void loadData(){

        Map<Id, OpenActivity> mapID_OpenActivity = new Map<Id, OpenActivity>();
        Map<Id, ActivityHistory> mapID_ActivityHistory = new Map<Id, ActivityHistory>();
        Map<Id, Account_Plan_Objective__c> mapID_AccountPlanObjective = new Map<Id, Account_Plan_Objective__c>();

        // SELECT Account_Plan_Objective__c WHITH THE RELATED DATA (ActivityHistory & OpenActivity)
        String tQuery = 'SELECT';
        tQuery += ' ID, Description__c';
        tQuery += ', (';
        tQuery += tQuery_ActivityHistory;
        tQuery += ')';
        tQuery += ', (';
        tQuery += tQuery_OpenActivity;
        tQuery += ')';
        tQuery += ' FROM Account_Plan_Objective__c';
        tQuery += ' WHERE Account_Plan__c = \'' + oIPlan2.Id + '\'';

        List<Account_Plan_Objective__c> lstAccountPlanObjective = Database.query(tQuery);

        for (Account_Plan_Objective__c oAccountPlanObjective : lstAccountPlanObjective){
            mapID_OpenActivity.putAll(oAccountPlanObjective.OpenActivities);
            mapID_ActivityHistory.putAll(oAccountPlanObjective.ActivityHistories);
            mapID_AccountPlanObjective.put(oAccountPlanObjective.Id, oAccountPlanObjective);

            lstOpenActivity.addAll(oAccountPlanObjective.OpenActivities);
            lstActivityHistory.addAll(oAccountPlanObjective.ActivityHistories);
        }


        // Sort the Collected Lists of Data
        lstOpenActivity = clsUtil.sortList(lstOpenActivity, 'ActivityDate', 'ASC');
        lstActivityHistory = clsUtil.sortList(lstActivityHistory, 'LastModifiedDate', 'ASC');

        string tPrefix_AccountPlanObjective = clsUtil.getPrefixFromSObjectName('Account_Plan_Objective__c');

        lstWROpenActivity = new List<wr_Activity>();
        for (OpenActivity oAct : lstOpenActivity){
            wr_Activity oWR_Act = new wr_Activity();
                if (String.valueOf(oAct.WhatID).startsWith(tPrefix_AccountPlanObjective)){
                    oWR_Act.tType           = 'Objective';
                }
                oWR_Act.oOpenActivity   = oAct;
                oWR_Act.tLink_Parent    = '/' + oAct.WhatId + '?retURL=%2F' + oIPlan2.Id; 
                oWR_Act.tLink_Detail    = '/' + oAct.Id + '?retURL=%2F' + oIPlan2.Id; 
                oWR_Act.tLink_Edit      = '/' + oAct.Id + '/e?retURL=%2F' + oIPlan2.Id; 
                oWR_Act.tLink_Close     = '/' + oAct.Id + '/e?close=1&retURL=%2F' + oIPlan2.Id; 

                if (mapID_AccountPlanObjective.containsKey(oAct.WhatId)){
                    oWR_Act.tObjective  = mapID_AccountPlanObjective.get(oAct.WhatId).Description__c;
                }
            lstWROpenActivity.add(oWR_Act);
        }

        lstWRActivityHistory = new List<wr_Activity>();
        for (ActivityHistory oAct : lstActivityHistory){
            wr_Activity oWR_Act = new wr_Activity();
                if (String.valueOf(oAct.WhatID).startsWith(tPrefix_AccountPlanObjective)){
                    oWR_Act.tType           = 'Objective';
                }
                oWR_Act.oActivityHistory    = oAct;
                oWR_Act.tLink_Parent        = '/' + oAct.WhatId + '?retURL=%2F' + oIPlan2.Id; 
                oWR_Act.tLink_Detail        = '/' + oAct.Id + '?retURL=%2F' + oIPlan2.Id; 
                oWR_Act.tLink_Edit          = '/' + oAct.Id + '/e?retURL=%2F' + oIPlan2.Id; 

                if (mapID_AccountPlanObjective.containsKey(oAct.WhatId)){
                    oWR_Act.tObjective  = mapID_AccountPlanObjective.get(oAct.WhatId).Description__c;
                }
            lstWRActivityHistory.add(oWR_Act);
        }
    }
    //------------------------------------------------------------------------------------


    //------------------------------------------------------------------------------------
    // Wrapper Class
    //------------------------------------------------------------------------------------
    public class wr_Activity{

        public wr_Activity(){
            this.tObjective         = '';
            this.tType              = '';
            this.tLink_Parent       = '';
            this.tLink_Detail       = '';
            this.tLink_Edit         = '';
            this.tLink_Close        = '';
            this.oOpenActivity      = new OpenActivity();
            this.oActivityHistory   = new ActivityHistory();
        }

        public String tObjective                { get;set; }
        public String tType                     { get;set; }
        public String tLink_Parent              { get;set; }
        public String tLink_Detail              { get;set; }
        public String tLink_Edit                { get;set; }
        public String tLink_Close               { get;set; }
        public OpenActivity oOpenActivity       { get;set; }
        public ActivityHistory oActivityHistory { get;set; }
    }
    //------------------------------------------------------------------------------------
}