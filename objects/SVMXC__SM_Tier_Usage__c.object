<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <customHelpPage>SVMXC__SVMX_LaunchHelp</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Consumption of tiered pricing at work detail level</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>SVMXC__SM_Calculated_By__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>User that calculated the tiered price.</description>
        <externalId>false</externalId>
        <inlineHelpText>User that calculated the tiered price.</inlineHelpText>
        <label>Calculated By</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Tier_Usages</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SVMXC__SM_Calculated_On__c</fullName>
        <deprecated>false</deprecated>
        <description>Date/time when this tier usage line was initiated.</description>
        <externalId>false</externalId>
        <inlineHelpText>Date/time when this tier usage line was initiated.</inlineHelpText>
        <label>Calculated On</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>SVMXC__SM_Committed_By__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>User that confirmed the tiered price.</description>
        <externalId>false</externalId>
        <inlineHelpText>User that confirmed the tiered price.</inlineHelpText>
        <label>Committed By</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Tier_Usages1</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SVMXC__SM_Committed_On__c</fullName>
        <deprecated>false</deprecated>
        <description>Date/time when this tier usage line was confirmed.</description>
        <externalId>false</externalId>
        <inlineHelpText>Date/time when this tier usage line was confirmed.</inlineHelpText>
        <label>Committed On</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>SVMXC__SM_Is_Committed__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Flag indicates if the Tier Usage record is confirmed.</description>
        <externalId>false</externalId>
        <inlineHelpText>Flag indicates if the Tier Usage record is confirmed.</inlineHelpText>
        <label>Is Committed</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>SVMXC__SM_Line_Price__c</fullName>
        <deprecated>false</deprecated>
        <description>Total price for this tier usage line</description>
        <externalId>false</externalId>
        <inlineHelpText>Total price for this tier usage line</inlineHelpText>
        <label>Line Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>3</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>SVMXC__SM_Part__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Name of the product used. Is a lookup to an existing salesforce product record</description>
        <externalId>false</externalId>
        <inlineHelpText>Name of the product used. Is a lookup to an existing salesforce product record</inlineHelpText>
        <label>Part</label>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>Tier Usages</relationshipLabel>
        <relationshipName>Tier_Usages</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SVMXC__SM_Price__c</fullName>
        <deprecated>false</deprecated>
        <description>Price per unit in this tier.</description>
        <externalId>false</externalId>
        <inlineHelpText>Price per unit in this tier.</inlineHelpText>
        <label>Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>3</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>SVMXC__SM_Quantity__c</fullName>
        <deprecated>false</deprecated>
        <description>Number of units consumed from the given Tier.</description>
        <externalId>false</externalId>
        <inlineHelpText>Number of units consumed from the given Tier.</inlineHelpText>
        <label>Quantity</label>
        <precision>18</precision>
        <required>false</required>
        <scale>5</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SVMXC__SM_Tiered_Pricing__c</fullName>
        <deprecated>false</deprecated>
        <description>Tiered Pricing for which this usage is applicable.</description>
        <externalId>false</externalId>
        <inlineHelpText>Tiered Pricing for which this usage is applicable.</inlineHelpText>
        <label>Tiered Pricing</label>
        <referenceTo>SVMXC__SM_Tiered_Pricing__c</referenceTo>
        <relationshipLabel>Tier Usages</relationshipLabel>
        <relationshipName>Tier_Usages</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>SVMXC__SM_Work_Detail__c</fullName>
        <deprecated>false</deprecated>
        <description>Work Detail for which this usage is applicable.</description>
        <externalId>false</externalId>
        <inlineHelpText>Work Detail for which this usage is applicable.</inlineHelpText>
        <label>Work Detail</label>
        <referenceTo>SVMXC__Service_Order_Line__c</referenceTo>
        <relationshipLabel>Tier Usages</relationshipLabel>
        <relationshipName>Tier_Usages</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <label>Tier Usage</label>
    <nameField>
        <displayFormat>{00000000}</displayFormat>
        <label>Record Number</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Tier Usages</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
