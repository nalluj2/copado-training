<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Created for BI
Used to store month records for each country (master - detail). 
In period record, attrition per month and year, and per product type can be defined.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Attrition_CGMS__c</fullName>
        <description>Used to define the attrition of CGMS in the time period defined on the record</description>
        <externalId>false</externalId>
        <inlineHelpText>Used to define the attrition of CGMS in the time period defined on the record</inlineHelpText>
        <label>Attrition CGMS</label>
        <precision>4</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Attrition_Pumps__c</fullName>
        <description>Used to define the attrition of pumps in the time period defined on the record</description>
        <externalId>false</externalId>
        <inlineHelpText>Used to define the attrition of pumps in the time period defined on the record</inlineHelpText>
        <label>Attrition Pumps</label>
        <precision>4</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Country_Period_Fiscal_Month__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Define the attrition period (must be a unique record, if time period already exist, change the data in that record). 01 is May, since 01 is the first fiscal month.</inlineHelpText>
        <label>Fiscal Month</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>01</fullName>
                    <default>false</default>
                    <label>01</label>
                </value>
                <value>
                    <fullName>02</fullName>
                    <default>false</default>
                    <label>02</label>
                </value>
                <value>
                    <fullName>03</fullName>
                    <default>false</default>
                    <label>03</label>
                </value>
                <value>
                    <fullName>04</fullName>
                    <default>false</default>
                    <label>04</label>
                </value>
                <value>
                    <fullName>05</fullName>
                    <default>false</default>
                    <label>05</label>
                </value>
                <value>
                    <fullName>06</fullName>
                    <default>false</default>
                    <label>06</label>
                </value>
                <value>
                    <fullName>07</fullName>
                    <default>false</default>
                    <label>07</label>
                </value>
                <value>
                    <fullName>08</fullName>
                    <default>false</default>
                    <label>08</label>
                </value>
                <value>
                    <fullName>09</fullName>
                    <default>false</default>
                    <label>09</label>
                </value>
                <value>
                    <fullName>10</fullName>
                    <default>false</default>
                    <label>10</label>
                </value>
                <value>
                    <fullName>11</fullName>
                    <default>false</default>
                    <label>11</label>
                </value>
                <value>
                    <fullName>12</fullName>
                    <default>false</default>
                    <label>12</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Country_Period_Fiscal_Year__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Define the attrition period (must be a unique record, if time period already exist, change the data in that record).</inlineHelpText>
        <label>Fiscal Year</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>2008</fullName>
                    <default>false</default>
                    <label>2008</label>
                </value>
                <value>
                    <fullName>2009</fullName>
                    <default>false</default>
                    <label>2009</label>
                </value>
                <value>
                    <fullName>2010</fullName>
                    <default>false</default>
                    <label>2010</label>
                </value>
                <value>
                    <fullName>2011</fullName>
                    <default>false</default>
                    <label>2011</label>
                </value>
                <value>
                    <fullName>2012</fullName>
                    <default>false</default>
                    <label>2012</label>
                </value>
                <value>
                    <fullName>2013</fullName>
                    <default>false</default>
                    <label>2013</label>
                </value>
                <value>
                    <fullName>2014</fullName>
                    <default>false</default>
                    <label>2014</label>
                </value>
                <value>
                    <fullName>2015</fullName>
                    <default>false</default>
                    <label>2015</label>
                </value>
                <value>
                    <fullName>2016</fullName>
                    <default>false</default>
                    <label>2016</label>
                </value>
                <value>
                    <fullName>2017</fullName>
                    <default>false</default>
                    <label>2017</label>
                </value>
                <value>
                    <fullName>2018</fullName>
                    <default>false</default>
                    <label>2018</label>
                </value>
                <value>
                    <fullName>2019</fullName>
                    <default>false</default>
                    <label>2019</label>
                </value>
                <value>
                    <fullName>2020</fullName>
                    <default>false</default>
                    <label>2020</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>DIB_Country__c</fullName>
        <description>Used to link the Country Period to the applicable country via a master-detail relationship</description>
        <externalId>false</externalId>
        <inlineHelpText>Used to link the Country Period to the applicable country via a master-detail relationship</inlineHelpText>
        <label>DIB Country</label>
        <referenceTo>DIB_Country__c</referenceTo>
        <relationshipLabel>Country Attrition</relationshipLabel>
        <relationshipName>DIB_Country_Periods</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Unique_Check__c</fullName>
        <caseSensitive>true</caseSensitive>
        <description>Field populated by field update based on country ID, month and year.</description>
        <externalId>false</externalId>
        <label>Unique Check</label>
        <length>25</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <label>Country Period</label>
    <nameField>
        <displayFormat>CP-{0}</displayFormat>
        <label>Country Period Nr</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Country Periods</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Mandatory_field_check</fullName>
        <active>true</active>
        <errorConditionFormula>OR(
TEXT(Attrition_CGMS__c) = NULL,
Attrition_CGMS__c  &lt; 0.0,
Attrition_CGMS__c  &gt; 1,
TEXT(Attrition_Pumps__c)  = NULL,
Attrition_Pumps__c  &lt; 0.0,
Attrition_Pumps__c  &gt; 1,
DIB_Country__c = NULL,
LEN(TEXT(Country_Period_Fiscal_Month__c)) &lt;&gt; 2,
LEN(TEXT(Country_Period_Fiscal_Year__c)) &lt;&gt; 4,
TEXT(Country_Period_Fiscal_Month__c) = NULL,
TEXT(Country_Period_Fiscal_Year__c) = NULL,
VALUE(TEXT(Country_Period_Fiscal_Month__c)) &gt; 12,
VALUE(TEXT(Country_Period_Fiscal_Month__c)) &lt; 1,
VALUE(TEXT(Country_Period_Fiscal_Year__c)) &lt; 2000,
VALUE(TEXT(Country_Period_Fiscal_Year__c)) &gt; 2030
)</errorConditionFormula>
        <errorMessage>Invalid values or missing mandatory fields</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
