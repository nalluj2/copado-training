<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>To store account delete and merge information. Also possible to load records for roll-up* purposes. DATA CURRENTLY NOT USED (AUG2011)

Records are created directly when delete or merge is done by accountafterdelete trigger (also when mass deleting via API).

1 When account deleted, create delete info record and fill fields according to logic described in table below. 
•	Deleted Account SFDC ID
•	Deleted Account SAP ID (normally empty, since SAP accounts can not yet be deleted, though logic needs to be in place).
•	Delete Account SF ID
•	Deleted Account Record Type (copy record type name as text)
•	Type

2 When two or three accounts merged, create delete info record(s) and fill fields according to logic described in table below.
•	Deleted Account SFDC ID
•	Deleted Account SAP ID
•	Delete Account SF ID
•	Deleted Account Record Type
•	Surviving Account SFDC ID
•	Surviving Account SAP ID
•	Surviving Account SF ID
•	Surviving Account Record Type
•	Type</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>Account_Creator__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Account Creator</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Account_Delete_Info</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Deleted_Account_Country__c</fullName>
        <externalId>false</externalId>
        <label>Deleted Account Country</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Deleted_Account_Name__c</fullName>
        <externalId>false</externalId>
        <label>Deleted Account Name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Deleted_Account_Record_Type__c</fullName>
        <description>When account deleted (single delete or via merge) record type is saved as text in this field 

Values:
SAP Account
Non-SAP Account
Pending Account

When existing account record type name is changed, these values will not be changed for previously created delete info records.</description>
        <externalId>false</externalId>
        <label>Deleted Account Record Type</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Deleted_Account_SAP_ID__c</fullName>
        <description>When account deleted (single delete or via merge) the SAP ID is stored in this field (is null for non-SAP)

NOTE: SAP accounts cannot be deleted therefore this field is redundant until the rule is changed.</description>
        <externalId>false</externalId>
        <label>Deleted Account SAP ID</label>
        <length>10</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Deleted_Account_SFDC_ID__c</fullName>
        <description>When account deleted (single delete or via merge) the SFDC 18 char ID is stored in this field.</description>
        <externalId>false</externalId>
        <label>Deleted Account SFDC ID</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Deleted_SF_ID__c</fullName>
        <description>Autonumber unique field of the account</description>
        <externalId>false</externalId>
        <label>Deleted SF ID</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Surviving_Account_Record_Type__c</fullName>
        <description>When account survived (via merge) the record type is stored as text in this field.

Values:
SAP Account
Non-SAP Account
Pending Account

When existing account record type name is changed, these values will not be changed for previously created delete info records.</description>
        <externalId>false</externalId>
        <label>Surviving Account Record Type</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Surviving_Account_SAP_ID__c</fullName>
        <description>Only for master accounts, surviving a merge, the SFDC ID is stored. Using this value, BI can determine where historic sales of the deleted SAP ID should be matched to e.g.
(is null for non-SAP)</description>
        <externalId>false</externalId>
        <label>Surviving Account SAP ID</label>
        <length>10</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Surviving_Account_SFDC_ID__c</fullName>
        <description>Only for master accounts, surviving a merge, the SFDC ID is stored. Using this value, BI can determine where historic sales of the deleted SFDC ID should be matched to e.g.</description>
        <externalId>false</externalId>
        <label>Surviving Account SFDC ID</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Surviving_SF_ID__c</fullName>
        <description>Autonumber unique field of the account</description>
        <externalId>false</externalId>
        <label>Surviving SF ID</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Type__c</fullName>
        <description>Values: 
•	Deleted (when account is deleted via delete button or api call)
•	Merged (when account is deleted via merge)
•	Virtual (when no physical merge or delete has been done in the system, but the record is created by manual entry or load). 

*SAP active accounts can never be deleted. 
Inactive SAP and Non-SAP deletions can only be done by system admins. 
Pending accounts can be deleted by Data Stewards</description>
        <externalId>false</externalId>
        <label>Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Delete</fullName>
                    <default>false</default>
                    <label>Delete</label>
                </value>
                <value>
                    <fullName>Merge</fullName>
                    <default>false</default>
                    <label>Merge</label>
                </value>
                <value>
                    <fullName>Virtual</fullName>
                    <default>false</default>
                    <label>Virtual</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Account Delete Info</label>
    <nameField>
        <displayFormat>AC-{00000000}</displayFormat>
        <label>Account Delete Nr</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Account Delete Info</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
