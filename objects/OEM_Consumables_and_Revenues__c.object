<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object contains the Consumables and Revenue data for the OEM Extended Profile</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Main_Supplier_as_of_Today__c</fullName>
        <externalId>false</externalId>
        <label>Main Supplier as of Today</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Contractual Dealer</fullName>
                    <default>false</default>
                    <label>Contractual Dealer</label>
                </value>
                <value>
                    <fullName>Generics</fullName>
                    <default>false</default>
                    <label>Generics</label>
                </value>
                <value>
                    <fullName>Masimo</fullName>
                    <default>false</default>
                    <label>Masimo</label>
                </value>
                <value>
                    <fullName>Medtronic</fullName>
                    <default>false</default>
                    <label>Medtronic</label>
                </value>
                <value>
                    <fullName>OEM Manufacturer</fullName>
                    <default>false</default>
                    <label>OEM Manufacturer</label>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                    <label>Other</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Manufacturer__c</fullName>
        <description>Manufacturer for which the potential revenue is recorded</description>
        <externalId>false</externalId>
        <label>Manufacturer</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Draeger</fullName>
                    <default>false</default>
                    <label>Draeger</label>
                </value>
                <value>
                    <fullName>Fukuda</fullName>
                    <default>false</default>
                    <label>Fukuda</label>
                </value>
                <value>
                    <fullName>GE Healthcare</fullName>
                    <default>false</default>
                    <label>GE Healthcare</label>
                </value>
                <value>
                    <fullName>Mindray</fullName>
                    <default>false</default>
                    <label>Mindray</label>
                </value>
                <value>
                    <fullName>Nihon Kohden</fullName>
                    <default>false</default>
                    <label>Nihon Kohden</label>
                </value>
                <value>
                    <fullName>Philips</fullName>
                    <default>false</default>
                    <label>Philips</label>
                </value>
                <value>
                    <fullName>Physio Control</fullName>
                    <default>false</default>
                    <label>Physio Control</label>
                </value>
                <value>
                    <fullName>Siemens Healthcare GmbH</fullName>
                    <default>false</default>
                    <label>Siemens Healthcare GmbH</label>
                </value>
                <value>
                    <fullName>Spacelabs Healthcare</fullName>
                    <default>false</default>
                    <label>Spacelabs Healthcare</label>
                </value>
                <value>
                    <fullName>Welch Allyn</fullName>
                    <default>false</default>
                    <label>Welch Allyn</label>
                </value>
                <value>
                    <fullName>Zoll Medical Group</fullName>
                    <default>false</default>
                    <label>Zoll Medical Group</label>
                </value>
                <value>
                    <fullName>None</fullName>
                    <default>false</default>
                    <label>None</label>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                    <label>Other</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>OEM_Extended_Profile__c</fullName>
        <externalId>false</externalId>
        <label>OEM Extended Profile</label>
        <referenceTo>MITG_Competitor_iBase__c</referenceTo>
        <relationshipLabel>OEM Consumables and Revenues</relationshipLabel>
        <relationshipName>OEM_Consumables_and_Revenues</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>true</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Product__c</fullName>
        <externalId>false</externalId>
        <label>Product</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>INVOS</fullName>
                    <default>false</default>
                    <label>INVOS</label>
                </value>
                <value>
                    <fullName>GE Entropy</fullName>
                    <default>false</default>
                    <label>GE Entropy</label>
                </value>
                <value>
                    <fullName>GE TruSat Sp02</fullName>
                    <default>false</default>
                    <label>GE TruSat Sp02</label>
                </value>
                <value>
                    <fullName>BIS</fullName>
                    <default>false</default>
                    <label>BIS</label>
                </value>
                <value>
                    <fullName>Masimo Sp02</fullName>
                    <default>false</default>
                    <label>Masimo Sp02</label>
                </value>
                <value>
                    <fullName>Mc GRATH</fullName>
                    <default>false</default>
                    <label>Mc GRATH</label>
                </value>
                <value>
                    <fullName>MICROSTREAM</fullName>
                    <default>false</default>
                    <label>MICROSTREAM</label>
                </value>
                <value>
                    <fullName>NELLCOR</fullName>
                    <default>false</default>
                    <label>NELLCOR</label>
                </value>
                <value>
                    <fullName>OTHER</fullName>
                    <default>false</default>
                    <label>OTHER</label>
                </value>
                <value>
                    <fullName>Philips Fast Sp02</fullName>
                    <default>false</default>
                    <label>Philips Fast Sp02</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>X12_Month_Potential_Revenue__c</fullName>
        <externalId>false</externalId>
        <label>12 Month Potential Revenue</label>
        <precision>8</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <label>OEM Consumables and Revenues</label>
    <nameField>
        <displayFormat>OEM{0000}</displayFormat>
        <label>OEM Consumables and Revenues Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>OEM Consumables and Revenues</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
