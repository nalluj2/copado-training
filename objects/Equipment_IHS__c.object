<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>The Equipment (IHS) object holds the Equipments (similar to Assets) for IHS. These are in general purchased from Suppliers (so not Medtronic products) and located in a Hospital.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>true</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>Account__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>Lookup filtered by Record Type ‘SAP Account&apos;.
Hospital where the Equipment is located.</description>
        <externalId>false</externalId>
        <inlineHelpText>Lookup filtered by Record Type ‘SAP Account&apos;.
Hospital where the Equipment is located.</inlineHelpText>
        <label>Account</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Account.RecordTypeId</field>
                <operation>equals</operation>
                <value>SAP Account, Non-SAP Account</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Equipments (IHS) (Account)</relationshipLabel>
        <relationshipName>Equipments_IHS1</relationshipName>
        <required>true</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Active__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Indicates if this Equipment is in use or not.</description>
        <externalId>false</externalId>
        <inlineHelpText>Indicates if this Equipment is in use or not.</inlineHelpText>
        <label>Active</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Cathlab__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Cathlab where the Equipment is located</description>
        <externalId>false</externalId>
        <inlineHelpText>Cathlab where the Equipment is located</inlineHelpText>
        <label>Cathlab(IHS)</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Cathlab_IHS__c.Hospital__c</field>
                <operation>equals</operation>
                <valueField>$Source.Account__c</valueField>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Cathlab_IHS__c</referenceTo>
        <relationshipLabel>Equipments (IHS)</relationshipLabel>
        <relationshipName>Equipments_IHS</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Country_from_Account__c</fullName>
        <description>Country from the Account of the equipment, used in listviews to select by country</description>
        <externalId>false</externalId>
        <formula>TEXT( Account__r.Account_Country_vs__c )</formula>
        <label>Country from Account</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <externalId>false</externalId>
        <label>Description</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>EST_Schedule__c</fullName>
        <description>This field defines with which intervals the Electrical Safety Tests need to be performed for this equipment</description>
        <externalId>false</externalId>
        <inlineHelpText>This field defines with which intervals the Electrical Safety Tests need to be performed for this equipment</inlineHelpText>
        <label>EST Schedule</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Annual</fullName>
                    <default>false</default>
                    <label>Annual</label>
                </value>
                <value>
                    <fullName>Semi Annual</fullName>
                    <default>false</default>
                    <label>Semi Annual</label>
                </value>
                <value>
                    <fullName>Quarterly</fullName>
                    <default>false</default>
                    <label>Quarterly</label>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                    <label>Other</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>End_of_Service_Date__c</fullName>
        <description>Date at which the Equipment goes out of service</description>
        <externalId>false</externalId>
        <label>End of Service Date</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>End_of_Service__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Indicates if the Equipment is at end of service</description>
        <externalId>false</externalId>
        <label>End of Service</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>End_of_Warranty_Date__c</fullName>
        <description>End of warranty date for the Equipment</description>
        <externalId>false</externalId>
        <label>End of Warranty Date</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Equipment_IHS_Name__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Search for the name to be used for this Equipment</inlineHelpText>
        <label>Equipment (IHS) Name</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Equipment_IHS_Name__c.Active__c</field>
                <operation>equals</operation>
                <value>True</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Equipment_IHS_Name__c</referenceTo>
        <relationshipLabel>Equipments (IHS)</relationshipLabel>
        <relationshipName>Equipments_IHS</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Hospital_Contract__c</fullName>
        <description>Contract with the hospital under which the Equipment is covered.</description>
        <externalId>false</externalId>
        <inlineHelpText>Contract with the hospital under which the Equipment is covered.</inlineHelpText>
        <label>Hospital Contract</label>
        <length>50</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Install_Date__c</fullName>
        <externalId>false</externalId>
        <label>Install Date</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Last_EST_Date__c</fullName>
        <description>Last Electrical Safety Test Date</description>
        <externalId>false</externalId>
        <inlineHelpText>Last time a Electrical Safety Test was done</inlineHelpText>
        <label>Last EST Date</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Last_PM_Date__c</fullName>
        <externalId>false</externalId>
        <label>Last PM Date</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Local_Tag__c</fullName>
        <externalId>false</externalId>
        <label>Local Tag #</label>
        <length>30</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Location__c</fullName>
        <description>Location in the Hospital where the Equipment is located.</description>
        <externalId>false</externalId>
        <inlineHelpText>Location in the Hospital where the Equipment is located.</inlineHelpText>
        <label>Location</label>
        <length>200</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Manufacturer__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>The manufacturer of the Equipment</description>
        <externalId>false</externalId>
        <label>Manufacturer</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Account.RecordTypeId</field>
                <operation>equals</operation>
                <value>Supplier</value>
            </filterItems>
            <filterItems>
                <field>Account.Type</field>
                <operation>equals</operation>
                <value>Manufacturer</value>
            </filterItems>
            <infoMessage>This is the Manufacturer of the Equipment</infoMessage>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Equipments (IHS) (Account)</relationshipLabel>
        <relationshipName>Equipments_IHS2</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Mobile__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Is the Equipment mobile or fixed?</description>
        <externalId>false</externalId>
        <inlineHelpText>Is the Equipment mobile or fixed?</inlineHelpText>
        <label>Mobile?</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Model__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Lookup filtered by Record Type ‘MDT Marketing Product’.</description>
        <externalId>false</externalId>
        <inlineHelpText>Lookup filtered by Record Type ‘MDT Marketing Product’.</inlineHelpText>
        <label>Model</label>
        <lookupFilter>
            <active>true</active>
            <errorMessage>The Product Record Type must be: &apos;MDT Marketing Product&apos;.</errorMessage>
            <filterItems>
                <field>Product2.RecordType.DeveloperName</field>
                <operation>equals</operation>
                <value>MDT_Marketing_Product, Competitor_Marketing_Product</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>Equipments (IHS)</relationshipLabel>
        <relationshipName>Equipments_IHS</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Next_EST_Date__c</fullName>
        <description>Next Electrical Safety Test Date</description>
        <externalId>false</externalId>
        <inlineHelpText>Next time the Electrical Safety Test must be done</inlineHelpText>
        <label>Next EST Date</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Next_PM_Date__c</fullName>
        <externalId>false</externalId>
        <label>Next PM Date</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Ownership__c</fullName>
        <externalId>false</externalId>
        <label>Ownership</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Medtronic</fullName>
                    <default>true</default>
                    <label>Medtronic</label>
                </value>
                <value>
                    <fullName>Hospital</fullName>
                    <default>false</default>
                    <label>Hospital</label>
                </value>
                <value>
                    <fullName>Supplier</fullName>
                    <default>false</default>
                    <label>Supplier</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>PM_Schedule__c</fullName>
        <externalId>false</externalId>
        <label>PM Schedule</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Annual</fullName>
                    <default>false</default>
                    <label>Annual</label>
                </value>
                <value>
                    <fullName>Semi Annual</fullName>
                    <default>false</default>
                    <label>Semi Annual</label>
                </value>
                <value>
                    <fullName>Quarterly</fullName>
                    <default>false</default>
                    <label>Quarterly</label>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                    <label>Other</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Purchase_Order_Date__c</fullName>
        <description>The date of the PO that was used by Medtronic to acquire this Equipment</description>
        <externalId>false</externalId>
        <inlineHelpText>The date of the PO that was used by Medtronic to acquire this Equipment</inlineHelpText>
        <label>Purchase Order Date</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Purchase_Order_Number__c</fullName>
        <description>The PO Number that was used by Medtronic to acquire this equipment</description>
        <externalId>false</externalId>
        <inlineHelpText>The PO Number that was used by Medtronic to acquire this equipment</inlineHelpText>
        <label>Purchase Order Number</label>
        <length>50</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Purchase_Order_Value__c</fullName>
        <description>The value of the PO that was used by Medtronic to acquire this Equipment</description>
        <externalId>false</externalId>
        <inlineHelpText>The value of the PO that was used by Medtronic to acquire this Equipment</inlineHelpText>
        <label>Purchase Order Value</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Related_Equipment_IHS__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Link to another IHS Equipment.</description>
        <externalId>false</externalId>
        <inlineHelpText>Link to another IHS Equipment.</inlineHelpText>
        <label>Parent Equipment (IHS)</label>
        <referenceTo>Equipment_IHS__c</referenceTo>
        <relationshipLabel>Related Equipments (IHS)</relationshipLabel>
        <relationshipName>Related_Equipments_IHS</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Serial_Number__c</fullName>
        <description>Serial Number identifies the Equipment.</description>
        <externalId>false</externalId>
        <inlineHelpText>Serial Number identifies the Equipment.</inlineHelpText>
        <label>Serial Number</label>
        <length>80</length>
        <required>true</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Software_Version__c</fullName>
        <externalId>false</externalId>
        <label>Software Version</label>
        <length>40</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Supplier_Contract__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Lookup filtered by the Supplier of this record.</description>
        <externalId>false</externalId>
        <inlineHelpText>Lookup filtered by the Supplier of this record.</inlineHelpText>
        <label>Supplier Contract</label>
        <lookupFilter>
            <active>true</active>
            <errorMessage>The Supplier Contract must be related to this Equipment Supplier.</errorMessage>
            <filterItems>
                <field>Contract_IHS__c.Supplier_Name__r.Id</field>
                <operation>equals</operation>
                <valueField>$Source.Supplier__c</valueField>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Contract_IHS__c</referenceTo>
        <relationshipLabel>Equipments (IHS)</relationshipLabel>
        <relationshipName>Equipments_IHS</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Supplier__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>Lookup filtered by Record Type ‘Supplier’.
Supplier from which the Equipment was purchased.</description>
        <externalId>false</externalId>
        <inlineHelpText>Lookup filtered by Record Type ‘Supplier’.
Supplier from which the Equipment was purchased.</inlineHelpText>
        <label>Supplier</label>
        <lookupFilter>
            <active>true</active>
            <errorMessage>The Account Record Type must be: &apos;Supplier&apos;.</errorMessage>
            <filterItems>
                <field>Account.RecordType.DeveloperName</field>
                <operation>equals</operation>
                <value>Supplier</value>
            </filterItems>
            <filterItems>
                <field>Account.Type</field>
                <operation>equals</operation>
                <value>Service Supplier</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Equipments (IHS)</relationshipLabel>
        <relationshipName>Equipments_IHS</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>System_ID__c</fullName>
        <externalId>false</externalId>
        <label>System ID</label>
        <length>50</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Equipment (IHS)</label>
    <listViews>
        <fullName>Active_Equipment_Italy</fullName>
        <columns>NAME</columns>
        <columns>Local_Tag__c</columns>
        <columns>Description__c</columns>
        <columns>Manufacturer__c</columns>
        <columns>Model__c</columns>
        <columns>Serial_Number__c</columns>
        <columns>Install_Date__c</columns>
        <columns>Account__c</columns>
        <columns>Supplier__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Active__c</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <filters>
            <field>Country_from_Account__c</field>
            <operation>equals</operation>
            <value>ITALY</value>
        </filters>
        <label>Active Equipment Italy</label>
        <language>en_US</language>
        <sharedTo>
            <group>IHS_Equipment_Management_Italy_Users</group>
        </sharedTo>
    </listViews>
    <listViews>
        <fullName>Active_Equipments_by_Account</fullName>
        <columns>NAME</columns>
        <columns>Serial_Number__c</columns>
        <columns>Local_Tag__c</columns>
        <columns>System_ID__c</columns>
        <columns>Software_Version__c</columns>
        <columns>Account__c</columns>
        <columns>Install_Date__c</columns>
        <columns>Last_PM_Date__c</columns>
        <columns>Next_PM_Date__c</columns>
        <columns>PM_Schedule__c</columns>
        <columns>Supplier__c</columns>
        <columns>Model__c</columns>
        <columns>Supplier_Contract__c</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Active__c</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <filters>
            <field>Account__c</field>
            <operation>contains</operation>
            <value>Virgen del</value>
        </filters>
        <label>Active Equipments by Account</label>
        <language>en_US</language>
        <sharedTo>
            <roleAndSubordinatesInternal>EUR_xBU</roleAndSubordinatesInternal>
        </sharedTo>
    </listViews>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Account__c</columns>
        <columns>Country_from_Account__c</columns>
        <columns>Active__c</columns>
        <columns>Install_Date__c</columns>
        <columns>Last_PM_Date__c</columns>
        <columns>Local_Tag__c</columns>
        <columns>Location__c</columns>
        <columns>Manufacturer__c</columns>
        <columns>Mobile__c</columns>
        <columns>Model__c</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <label>All Equipment</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Equipments_Spain</fullName>
        <columns>NAME</columns>
        <columns>Account__c</columns>
        <columns>Country_from_Account__c</columns>
        <columns>Serial_Number__c</columns>
        <columns>Active__c</columns>
        <columns>Install_Date__c</columns>
        <columns>Manufacturer__c</columns>
        <columns>Supplier__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Country_from_Account__c</field>
            <operation>equals</operation>
            <value>Spain</value>
        </filters>
        <label>Equipments Spain</label>
        <language>en_US</language>
        <sharedTo>
            <roleAndSubordinatesInternal>SpainxBU</roleAndSubordinatesInternal>
        </sharedTo>
    </listViews>
    <nameField>
        <displayFormat>EIHS-{000000}</displayFormat>
        <label>Equipment (IHS) Number</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Equipments (IHS)</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Active__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Model__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Software_Version__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Account__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Location__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Last_PM_Date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Next_PM_Date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Serial_Number__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Supplier__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Serial_Number__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Account__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Model__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Active__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Description__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Manufacturer__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Supplier__c</lookupDialogsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>Product_Mandatory</fullName>
        <active>true</active>
        <description>The Product lookup field must have a value.</description>
        <errorConditionFormula>ISNULL(Model__c)</errorConditionFormula>
        <errorMessage>Please select a Product.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
