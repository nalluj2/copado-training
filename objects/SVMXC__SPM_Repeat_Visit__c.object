<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <customHelpPage>SVMXC__SVMX_LaunchHelp</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Holds the repeat visit status of the Work-orders that got into the system</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>SVMXC__Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Lookup to Account</description>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>SPM - Repeat Visits</relationshipLabel>
        <relationshipName>SPM_Repeat_Visits_del1</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SVMXC__Calculation_Method__c</fullName>
        <deprecated>false</deprecated>
        <description>This field is used to capture calculation method.</description>
        <externalId>false</externalId>
        <inlineHelpText>This field is used to capture calculation method.</inlineHelpText>
        <label>Calculation Method</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SVMXC__Case__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Lookup to Case</description>
        <externalId>false</externalId>
        <inlineHelpText>Lookup to Case</inlineHelpText>
        <label>Case</label>
        <referenceTo>Case</referenceTo>
        <relationshipLabel>SPM - Repeat Visits</relationshipLabel>
        <relationshipName>SPM_Repeat_Visits</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SVMXC__Configuration_Name__c</fullName>
        <deprecated>false</deprecated>
        <description>This field is used to capture configuration name.</description>
        <externalId>false</externalId>
        <inlineHelpText>This field is used to capture configuration name.</inlineHelpText>
        <label>Configuration Name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SVMXC__First_Incident_On__c</fullName>
        <deprecated>false</deprecated>
        <description>Holds the Work Order Creation date</description>
        <externalId>false</externalId>
        <label>First Incident On</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>SVMXC__Installed_Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Lookup to Installed Product</description>
        <externalId>false</externalId>
        <label>Installed Product</label>
        <referenceTo>SVMXC__Installed_Product__c</referenceTo>
        <relationshipLabel>SPM - Repeat Visits</relationshipLabel>
        <relationshipName>SPM_Repeat_Visit</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SVMXC__Is_Repeat_Visit__c</fullName>
        <deprecated>false</deprecated>
        <description>Holds the repeat visit status based on the value present in Repeat Visit Within (Days) field.</description>
        <externalId>false</externalId>
        <formula>!ISNULL( SVMXC__Subsequent_Incident_On__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Repeat Visit?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>SVMXC__Primary_Work_Order__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Lookup to Primary Work Order</description>
        <externalId>false</externalId>
        <inlineHelpText>Lookup to Primary Work Order</inlineHelpText>
        <label>Primary Work Order</label>
        <referenceTo>SVMXC__Service_Order__c</referenceTo>
        <relationshipLabel>SPM - Repeat Visits (Primary Work Order)</relationshipLabel>
        <relationshipName>SPM_Repeat_Visits</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SVMXC__Product_Warranty__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Holds the reference to Product Warranty Object</description>
        <externalId>false</externalId>
        <label>Product Warranty</label>
        <referenceTo>SVMXC__Warranty__c</referenceTo>
        <relationshipLabel>SPM - Repeat Visits</relationshipLabel>
        <relationshipName>SPM_Repeat_Visit</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SVMXC__Repeat_Visit_Within__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>SVMXC__Subsequent_Incident_On__c - SVMXC__First_Incident_On__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Repeat Visit Within (Days)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SVMXC__Service_Contract__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Holds the reference to Service Contract Object</description>
        <externalId>false</externalId>
        <label>Service Contract</label>
        <referenceTo>SVMXC__Service_Contract__c</referenceTo>
        <relationshipLabel>SPM - Repeat Visits</relationshipLabel>
        <relationshipName>SPM_Repeat_Visit</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SVMXC__Service_Manager__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Holds the reference to User Object</description>
        <externalId>false</externalId>
        <label>Service Manager</label>
        <referenceTo>User</referenceTo>
        <relationshipName>SPM_Repeat_Visit</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SVMXC__Service_Request__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Lookup to Service Request</description>
        <externalId>false</externalId>
        <inlineHelpText>Lookup to Service Request</inlineHelpText>
        <label>Service Request</label>
        <referenceTo>SVMXC__Service_Request__c</referenceTo>
        <relationshipLabel>SPM - Repeat Visits</relationshipLabel>
        <relationshipName>SPM_Repeat_Visits</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SVMXC__Service_Team__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Holds the reference to Service Team Object</description>
        <externalId>false</externalId>
        <label>Service Team</label>
        <referenceTo>SVMXC__Service_Group__c</referenceTo>
        <relationshipLabel>SPM - Repeat Visits</relationshipLabel>
        <relationshipName>SPM_Repeat_Visit</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SVMXC__Subsequent_Incident_On__c</fullName>
        <deprecated>false</deprecated>
        <description>Holds the Subsequent Work Order Creation date</description>
        <externalId>false</externalId>
        <label>Followup Incident On</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>SVMXC__Subsequent_Work_Order__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Holds the Subsequent Work Order Reference</description>
        <externalId>false</externalId>
        <label>Followup Work Order</label>
        <referenceTo>SVMXC__Service_Order__c</referenceTo>
        <relationshipLabel>SPM - Repeat Visits (Subsequent Work Order)</relationshipLabel>
        <relationshipName>SPM_Repeat_Visit</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SVMXC__Technician__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Holds the reference to Technician Object</description>
        <externalId>false</externalId>
        <label>Technician</label>
        <referenceTo>SVMXC__Service_Group_Members__c</referenceTo>
        <relationshipLabel>SPM - Repeat Visits</relationshipLabel>
        <relationshipName>SPM_Repeat_Visit</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SVMXC__Work_Order__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Holds the Work Order reference</description>
        <externalId>false</externalId>
        <label>Work Order</label>
        <referenceTo>SVMXC__Service_Order__c</referenceTo>
        <relationshipLabel>SPM - Repeat Visits (Work Order)</relationshipLabel>
        <relationshipName>SPM_Repeat_Visit1</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>SPM - Repeat Visit</label>
    <nameField>
        <displayFormat>{0000000000}</displayFormat>
        <label>Record Number</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>SPM - Repeat Visits</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
