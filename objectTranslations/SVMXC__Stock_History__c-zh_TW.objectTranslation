<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>庫存歷史記錄</value>
    </caseValues>
    <fields>
        <help><!-- Change Type Indicates if the type of change in product quantity was “Increase” or “Decrease”. --></help>
        <label><!-- Change Type --></label>
        <name>SVMXC__Change_Type__c</name>
        <picklistValues>
            <masterLabel>Decrease</masterLabel>
            <translation>減少</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Increase</masterLabel>
            <translation>增加</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Set</masterLabel>
            <translation>設定</translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Name of the salesforce user that has initiated original transaction which resulted in stock change. --></help>
        <label><!-- Changed By --></label>
        <name>SVMXC__Changed_By__c</name>
        <relationshipLabel><!-- Stock History (Changed By) --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Price of the product from the default pricebook at the time of transaction --></help>
        <label><!-- Cost --></label>
        <name>SVMXC__Cost__c</name>
    </fields>
    <fields>
        <help><!-- Currency of the product price at the time of the transaction, which resulted in stock change. --></help>
        <label><!-- Currency --></label>
        <name>SVMXC__Currency__c</name>
    </fields>
    <fields>
        <help><!-- Date/time when the change in product stock was made. --></help>
        <label><!-- Date Changed --></label>
        <name>SVMXC__Date_Changed__c</name>
    </fields>
    <fields>
        <help><!-- From Product Stock --></help>
        <label><!-- From Product Stock --></label>
        <name>SVMXC__From_Product_Stock__c</name>
        <relationshipLabel><!-- Serial History (From Product Stock) --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Inventory account number for this location. Used for costing/accounting purposes. --></help>
        <label><!-- Inventory Account --></label>
        <name>SVMXC__Inventory_Account__c</name>
    </fields>
    <fields>
        <label><!-- Inventory Process --></label>
        <name>SVMXC__Inventory_Process__c</name>
        <relationshipLabel><!-- Stock History --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Flag indicates if transaction is for/ by a Partner or not. --></help>
        <label><!-- IsPartnerRecord --></label>
        <name>SVMXC__IsPartnerRecord__c</name>
    </fields>
    <fields>
        <help><!-- Flag indicates if transaction is for/ by a Partner or not. --></help>
        <label><!-- Is Partner --></label>
        <name>SVMXC__IsPartner__c</name>
    </fields>
    <fields>
        <help><!-- Current physical location of the product. This is a lookup to an existing site record. --></help>
        <label><!-- Location --></label>
        <name>SVMXC__Location__c</name>
        <relationshipLabel><!-- Stock History --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Flag indicates if this transaction resulted in negative stock or not. --></help>
        <label><!-- Negative Stock --></label>
        <name>SVMXC__Negative_Stock__c</name>
    </fields>
    <fields>
        <help><!-- User notified of negative stock. Reference to an existing Salesforce user record. --></help>
        <label><!-- Notified User --></label>
        <name>SVMXC__Notified_User__c</name>
        <relationshipLabel><!-- Stock History (Notified User) --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Lookup to Partner Account. --></help>
        <label><!-- Partner Account --></label>
        <name>SVMXC__Partner_Account__c</name>
        <relationshipLabel><!-- Stock History --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Lookup to Partner Contact. --></help>
        <label><!-- Partner Contact --></label>
        <name>SVMXC__Partner_Contact__c</name>
        <relationshipLabel><!-- Stock History --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Reference to the Parts Request line. --></help>
        <label><!-- Parts Request Line --></label>
        <name>SVMXC__Parts_Request_Line__c</name>
        <relationshipLabel><!-- Stock History --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Reference to the Parts Request. --></help>
        <label><!-- Parts Request --></label>
        <name>SVMXC__Parts_Request__c</name>
        <relationshipLabel><!-- Stock History --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Select Product Serial --></help>
        <label><!-- Product Serial --></label>
        <name>SVMXC__Product_Serial__c</name>
        <relationshipLabel><!-- Serial History --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Product Stock --></label>
        <name>SVMXC__Product_Stock__c</name>
        <relationshipLabel><!-- Stock History (Product Stock) --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Name of the product. This is a lookup to an existing salesforce product record. --></help>
        <label><!-- Product --></label>
        <name>SVMXC__Product__c</name>
        <relationshipLabel><!-- Stock History --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Stock quantity in the given product+location+status after the transaction. --></help>
        <label><!-- Qty after change --></label>
        <name>SVMXC__Quantity_after_change2__c</name>
    </fields>
    <fields>
        <help><!-- Stock quantity in the given product+location+status before the transaction. --></help>
        <label><!-- Qty before change --></label>
        <name>SVMXC__Quantity_before_change2__c</name>
    </fields>
    <fields>
        <help><!-- Reference to the RMA/Shipment Line. --></help>
        <label><!-- RMA Line --></label>
        <name>SVMXC__RMA_Line__c</name>
        <relationshipLabel><!-- Stock History (RMA Line) --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Reference to the RMA/Shipment Order. --></help>
        <label><!-- RMA --></label>
        <name>SVMXC__RMA__c</name>
        <relationshipLabel><!-- Stock History (RMA) --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Identification number assigned to a specific quantity or lot of materials from a single manufacturer. --></help>
        <label><!-- Batch/Lot --></label>
        <name>SVMXC__SM_Batch_Lot2__c</name>
        <relationshipLabel><!-- Stock History --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Batch/Lot --></label>
        <name>SVMXC__SM_Batch_Lot__c</name>
        <relationshipLabel><!-- Stock History --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Reference to the service order line. --></help>
        <label><!-- Work Details --></label>
        <name>SVMXC__Service_Order_Line__c</name>
        <relationshipLabel><!-- Stock History --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Reference to the Service Order. --></help>
        <label><!-- Work Order --></label>
        <name>SVMXC__Service_Order__c</name>
        <relationshipLabel><!-- Stock History --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Reference to the RMA/Shipment Line. --></help>
        <label><!-- Shipment Line --></label>
        <name>SVMXC__Shipment_Line__c</name>
        <relationshipLabel><!-- Stock History (Shipment Line) --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Reference to the RMA/Shipment Order. --></help>
        <label><!-- Shipment --></label>
        <name>SVMXC__Shipment__c</name>
        <relationshipLabel><!-- Stock History (Shipment) --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Current status of product. --></help>
        <label><!-- Status --></label>
        <name>SVMXC__Status__c</name>
        <picklistValues>
            <masterLabel>Available</masterLabel>
            <translation>可用</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Consumed</masterLabel>
            <translation>已消耗</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>In Transit</masterLabel>
            <translation>運送途中</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Returned</masterLabel>
            <translation>已退貨</translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Reference to the Stock Adjustment. --></help>
        <label><!-- Stock Adjustment --></label>
        <name>SVMXC__Stock_Adjustment__c</name>
        <relationshipLabel><!-- Stock History --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Reference to the Stock Transfer line. --></help>
        <label><!-- Stock Transfer Line --></label>
        <name>SVMXC__Stock_Transfer_Line__c</name>
        <relationshipLabel><!-- Stock History --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Reference to the Stock Transfer. --></help>
        <label><!-- Stock Transfer --></label>
        <name>SVMXC__Stock_Transfer__c</name>
        <relationshipLabel><!-- Stock History --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Value of the stock quantity at the time of transaction --></help>
        <label><!-- Stock Value --></label>
        <name>SVMXC__Stock_Value__c</name>
    </fields>
    <fields>
        <label><!-- To Product Stock --></label>
        <name>SVMXC__To_Product_Stock__c</name>
        <relationshipLabel><!-- Serial History (To Product Stock) --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Product quantity in the source transaction. --></help>
        <label><!-- Transaction Qty --></label>
        <name>SVMXC__Transaction_Quantity2__c</name>
    </fields>
    <fields>
        <help><!-- Status should be be assigned based on various original transactions which resulted in stock change. --></help>
        <label><!-- Transaction Type --></label>
        <name>SVMXC__Transaction_Type__c</name>
        <picklistValues>
            <masterLabel>Parts Receipt</masterLabel>
            <translation>零件收取</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>RMA Receipt</masterLabel>
            <translation>RMA 收取</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>SVO Receipt</masterLabel>
            <translation>SVO 收取</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>SVO Usage</masterLabel>
            <translation>SVO 使用</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Shipment</masterLabel>
            <translation>出貨</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Stock Adjustment</masterLabel>
            <translation>庫存調整</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Stock Transfer</masterLabel>
            <translation>庫存移轉</translation>
        </picklistValues>
    </fields>
    <nameFieldLabel>名稱</nameFieldLabel>
    <recordTypes>
        <description><!-- Serial History --></description>
        <label><!-- Serial History --></label>
        <name>SVMXC__Serial_History</name>
    </recordTypes>
    <recordTypes>
        <description><!-- Stock History --></description>
        <label><!-- Stock History --></label>
        <name>SVMXC__Stock_History</name>
    </recordTypes>
</CustomObjectTranslation>
