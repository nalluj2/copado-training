<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Condiciones de la garantía</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Condiciones de la garantía</value>
    </caseValues>
    <fields>
        <help><!-- Flag indicates if all products involved in the repair will be covered by this warranty. This flag does not drive any functionality in ServiceMax and is for informational purposes only --></help>
        <label><!-- All Products Covered(DO NOT USE) --></label>
        <name>SVMXC__All_Products_Covered__c</name>
    </fields>
    <fields>
        <help><!-- Flag indicates if all services performed will be covered by this warranty. This flag does not drive any functionality in ServiceMax and is for informational purposes only --></help>
        <label><!-- All Services Covered(DO NOT USE) --></label>
        <name>SVMXC__All_Services_Covered__c</name>
    </fields>
    <fields>
        <help><!-- Specify the date field name from Installed Product object to be considered in warranty creation. --></help>
        <label><!-- Coverage Effective From --></label>
        <name>SVMXC__Coverage_Effective_From2__c</name>
    </fields>
    <fields>
        <help><!-- Specify the date field name from Installed Product object to be considered in warranty creation. --></help>
        <label><!-- Coverage Effective From(DO NOT USE) --></label>
        <name>SVMXC__Coverage_Effective_From__c</name>
        <picklistValues>
            <masterLabel>Entry Date</masterLabel>
            <translation>Fecha de entrada</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Install Date</masterLabel>
            <translation>Fecha de instalación</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Ship Date</masterLabel>
            <translation>Fecha de envío</translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- A number indicating the duration of coverage for expenses --></help>
        <label><!-- Duration of Expenses Coverage --></label>
        <name>SVMXC__Duration_of_Expenses__c</name>
    </fields>
    <fields>
        <help><!-- A number indicating the duration of coverage for parts --></help>
        <label><!-- Duration of Material Coverage --></label>
        <name>SVMXC__Duration_of_Material_Coverage__c</name>
    </fields>
    <fields>
        <help><!-- A number indicating the duration of coverage for labor. --></help>
        <label><!-- Duration of Labor Coverage --></label>
        <name>SVMXC__Duration_of_Time_Coverage__c</name>
    </fields>
    <fields>
        <label><!-- Exchange Type --></label>
        <name>SVMXC__Exchange_Type__c</name>
        <picklistValues>
            <masterLabel>Advance Exchange</masterLabel>
            <translation>Intercambio avanzado</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Return Exchange</masterLabel>
            <translation>Intercambio de devolución</translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Define any exceptions not covered by the standard coverage of the template --></help>
        <label><!-- Exclusions --></label>
        <name>SVMXC__Exclusions__c</name>
    </fields>
    <fields>
        <help><!-- A number between 1 and 100 indicating the percentage of coverage on expenses --></help>
        <label><!-- Expenses % Covered --></label>
        <name>SVMXC__Expenses_Covered__c</name>
    </fields>
    <fields>
        <help><!-- Flag indicates if an invoice is always raised at the end of service delivery. This flag does not drive any functionality in ServiceMax and is for informational purposes only --></help>
        <label><!-- Invoice Required --></label>
        <name>SVMXC__Invoice_Required__c</name>
    </fields>
    <fields>
        <help><!-- Flag indicates if customer is given a temporary replacement product when the covered product is under repair. This flag does not drive any functionality in ServiceMax and is for informational purposes only --></help>
        <label><!-- Loaner Provided --></label>
        <name>SVMXC__Loaner_Provided__c</name>
    </fields>
    <fields>
        <help><!-- A number between 1 and 100 indicating the percentage of coverage on parts --></help>
        <label><!-- Material % Covered --></label>
        <name>SVMXC__Material_Covered__c</name>
    </fields>
    <fields>
        <help><!-- Flag indicates if customer is eligible for onsite repair when entitled by this warranty. This flag does not drive any functionality in ServiceMax and is for informational purposes only --></help>
        <label><!-- Onsite Repair --></label>
        <name>SVMXC__Onsite_Repair__c</name>
    </fields>
    <fields>
        <help><!-- Flag indicates if an RMA is always required or an RMA is implied when this warranty is used to entitle a customer. This flag does not drive any functionality in ServiceMax and is for informational purposes only --></help>
        <label><!-- RMA Required --></label>
        <name>SVMXC__RMA_Required__c</name>
    </fields>
    <fields>
        <help><!-- Flag indicates if customer shipments are covered by the warranty. This flag does not drive any functionality in ServiceMax and is for informational purposes only --></help>
        <label><!-- Shipment Allowed --></label>
        <name>SVMXC__Shipment_Allowed__c</name>
    </fields>
    <fields>
        <help><!-- Indicates if this template is used for warranty calculation or used as a reference in service contracts. When creating warranty for installed products, only templates with type Warranty are considered --></help>
        <label><!-- Template Type --></label>
        <name>SVMXC__Template_Type__c</name>
        <picklistValues>
            <masterLabel>Service Contract</masterLabel>
            <translation>Contrato de servicios</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Warranty</masterLabel>
            <translation>Garantía</translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- A number between 1 and 100 indicating the percentage of coverage on labor --></help>
        <label><!-- Labor % Covered --></label>
        <name>SVMXC__Time_Covered__c</name>
    </fields>
    <fields>
        <help><!-- Flag indicates if this warranty is transferable to other products under special circumstances. This flag does not drive any functionality in ServiceMax and is for informational purposes only --></help>
        <label><!-- Transferable --></label>
        <name>SVMXC__Transferable__c</name>
    </fields>
    <fields>
        <help><!-- Combined with duration of coverage, this qualifies the expenses coverage duration in days, weeks, months or years --></help>
        <label><!-- Unit of Time for Expenses Coverage --></label>
        <name>SVMXC__Unit_of_Time_Expenses__c</name>
        <picklistValues>
            <masterLabel>Days</masterLabel>
            <translation>Días</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Months</masterLabel>
            <translation>Meses</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Weeks</masterLabel>
            <translation>Semanas</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Years</masterLabel>
            <translation>Años</translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Combined with duration of coverage, this qualifies the material coverage duration in days, weeks, months or years --></help>
        <label><!-- Unit of Time for Material Coverage --></label>
        <name>SVMXC__Unit_of_Time_Material__c</name>
        <picklistValues>
            <masterLabel>Days</masterLabel>
            <translation>Días</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Months</masterLabel>
            <translation>Meses</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Weeks</masterLabel>
            <translation>Semanas</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Years</masterLabel>
            <translation>Años</translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Combined with duration of coverage, this qualifies the labor coverage duration in days, weeks, months or years --></help>
        <label><!-- Unit of Time for Labor Coverage --></label>
        <name>SVMXC__Unit_of_Time__c</name>
        <picklistValues>
            <masterLabel>Days</masterLabel>
            <translation>Días</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Months</masterLabel>
            <translation>Meses</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Weeks</masterLabel>
            <translation>Semanas</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Years</masterLabel>
            <translation>Años</translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Flag indicates if the customer is authorized to walk into a service center for service. This flag does not drive any functionality in ServiceMax and is for informational purposes only --></help>
        <label><!-- Walk-in Allowed --></label>
        <name>SVMXC__Walk_in_Allowed__c</name>
    </fields>
    <gender>Feminine</gender>
    <nameFieldLabel>Nombre de plantilla</nameFieldLabel>
    <validationRules>
        <errorMessage><!-- Please specify the duration and unit of time of Expenses coverage. --></errorMessage>
        <name>SVMXC__Expenses_Duration_UoT_Check</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Please specify the duration and unit of time of Labor coverage. --></errorMessage>
        <name>SVMXC__Labor_Duration_UoT_Check</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Please specify the duration and unit of time of Material coverage. --></errorMessage>
        <name>SVMXC__Material_Duration_UoT_Check</name>
    </validationRules>
    <webLinks>
        <label><!-- Coverage_By_Counters --></label>
        <name>SVMXC__Coverage_By_Counters</name>
    </webLinks>
    <webLinks>
        <label><!-- Warranty_Definition --></label>
        <name>SVMXC__Warranty_Definition</name>
    </webLinks>
</CustomObjectTranslation>
