<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Registro do trabalho ServiceMax</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Registro do trabalho ServiceMax</value>
    </caseValues>
    <fields>
        <help><!-- For sync time log records: CPU time for executing Apex code, and any processes that are called from the code like package code and workflows. This excludes the time spent in DB for SOQL &amp; DML. It is in seconds if Type is MASTER_*; milliseconds otherwise. --></help>
        <label><!-- Apex CPU Time --></label>
        <name>SVMXC__Apex_CPU_Time__c</name>
    </fields>
    <fields>
        <help><!-- For sync time log records, this field stores the status of the sync request: InProgress - Indicates receipt of sync request by the server; Completed / Failed - Indicates end of processing of sync request by the server. --></help>
        <label><!-- Call Status --></label>
        <name>SVMXC__Call_Status__c</name>
        <picklistValues>
            <masterLabel>Completed</masterLabel>
            <translation><!-- Completed --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Failed</masterLabel>
            <translation><!-- Failed --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>InProgress</masterLabel>
            <translation><!-- InProgress --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- For sync time log records, this field stores the date time at which the processing of sync response from server was completed by the client app. --></help>
        <label><!-- Client Response Processing Timestamp --></label>
        <name>SVMXC__Client_Response_Processing_Timestamp__c</name>
    </fields>
    <fields>
        <help><!-- For sync time log records, this field stores the date time at which sync response from server was received by the client app. --></help>
        <label><!-- Client Response Receive Timestamp --></label>
        <name>SVMXC__Client_Response_Receive_Timestamp__c</name>
    </fields>
    <fields>
        <help><!-- For sync time log records, this field stores the version number of mobile client application that initiated the sync request. --></help>
        <label><!-- Client Version --></label>
        <name>SVMXC__Client_Version__c</name>
    </fields>
    <fields>
        <help><!-- For master records (Type = MASTER_*), server processing time in seconds: Response Returned Timestamp - Request Received Timestamp. For other sync time log records, it identifies the sync request purpose (e.g.:  VALIDATE_PROFILE, MOBILE_DEVICE_TAGS). --></help>
        <label><!-- Event Name --></label>
        <name>SVMXC__Event_Name__c</name>
    </fields>
    <fields>
        <help><!-- If Type = MASTER_*, network latency in seconds: (Request Received Timestamp - Log Timestamp) + (Client Response Receive Timestamp - Response Returned Timestamp). For other sync time log records, it identifies the sync request type (SYNC / META_SYNC). --></help>
        <label><!-- Event Type --></label>
        <name>SVMXC__Event_Type__c</name>
    </fields>
    <fields>
        <help><!-- For master records (Type = MASTER_*):  Full Name of the mobile client user who initiated the sync request from the client app. For other sync time log records: User&apos;s Salesforce Record Id. --></help>
        <label><!-- Group Id --></label>
        <name>SVMXC__Group_Id__c</name>
    </fields>
    <fields>
        <help><!-- For sync time log records, it indicates the type of sync operation initiated by the mobile user (INITIAL SYNC, INCREMENTAL DATA SYNC, CONFIG SYNC, SFM SEARCH). For logs pushed by client app, it is the log category (Application Level/Performance Level). --></help>
        <label><!-- Log Category --></label>
        <name>SVMXC__Log_Category__c</name>
        <picklistValues>
            <masterLabel>Application Level</masterLabel>
            <translation><!-- Application Level --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>CONFIG SYNC</masterLabel>
            <translation><!-- CONFIG SYNC --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>GET PRICE SYNC</masterLabel>
            <translation><!-- GET PRICE SYNC --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>INCREMENTAL DATA SYNC</masterLabel>
            <translation><!-- INCREMENTAL DATA SYNC --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>INITIAL SYNC</masterLabel>
            <translation><!-- INITIAL SYNC --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Performance Level</masterLabel>
            <translation><!-- Performance Level --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>SFM SEARCH</masterLabel>
            <translation><!-- SFM SEARCH --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- For sync time log records, it is the time taken by the client app to process sync response: Client Response Processing Timestamp - Client Response Receive Timestamp.  It is in seconds if Type is MASTER_*; milliseconds otherwise. --></help>
        <label><!-- Log Context --></label>
        <name>SVMXC__Log_Context__c</name>
    </fields>
    <fields>
        <help><!-- For sync time log records, it is the date and time at which sync request  was initiated by the client app. --></help>
        <label><!-- Log Timestamp --></label>
        <name>SVMXC__Log_Timestamp__c</name>
    </fields>
    <fields>
        <help><!-- For master records (Type = MASTER_*), it is the time taken in seconds for DB operations in server: Server Processing Time - Code Execution Time.  For logs pushed by client app, it is the the level (1/2/3) of log message (3 logged when level is verbose). --></help>
        <label><!-- Log level --></label>
        <name>SVMXC__Log_level__c</name>
    </fields>
    <fields>
        <help><!-- For master records (Type = MASTER_*), time taken in seconds for a sync request (e.g. INITIAL SYNC, CONFIG SYNC): Client Response Processing Timestamp of last WS call - Log Timestamp of first WS call. For logs pushed by client app, this is the log message. --></help>
        <label><!-- Message --></label>
        <name>SVMXC__Message__c</name>
    </fields>
    <fields>
        <help><!-- For sync time log records, it is the unique request id for a specific sync call (such as INITIAL SYNC, INCREMENTAL DATA SYNC, or CONFIG SYNC) made by a client app.
For logs pushed by client app, it indicates the operation executed by the client code. --></help>
        <label><!-- Operation --></label>
        <name>SVMXC__Operation__c</name>
    </fields>
    <fields>
        <help><!-- If Type = MASTER_*:  Profile Name of the ServiceMax Group Profile of mobile user. For other sync time log records: Salesforce Record Id of the ServiceMax Group Profile of mobile user. For logs pushed by client app: Mobile user&apos;s Salesforce Record Id. --></help>
        <label><!-- Profile Id --></label>
        <name>SVMXC__Profile_Id__c</name>
    </fields>
    <fields>
        <help><!-- Date time at which a sync request from a client app is received by the server. --></help>
        <label><!-- Request Received Timestamp --></label>
        <name>SVMXC__Request_Received_Timestamp__c</name>
    </fields>
    <fields>
        <help><!-- Date time at which server initiates the response to a sync request from the client app. --></help>
        <label><!-- Response Returned Timestamp --></label>
        <name>SVMXC__Response_Returned_Timestamp__c</name>
    </fields>
    <fields>
        <help><!-- Stores the name of the device that initiated the sync request. In master sync time log records which store cumulative durations, the device name is stored with the prefix MASTER_. --></help>
        <label><!-- Type --></label>
        <name>SVMXC__Type__c</name>
        <picklistValues>
            <masterLabel>ANDROID</masterLabel>
            <translation><!-- ANDROID --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>MASTER_ANDROID</masterLabel>
            <translation><!-- MASTER_ANDROID --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>MASTER_IPAD</masterLabel>
            <translation><!-- MASTER_IPAD --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>MASTER_IPHONE</masterLabel>
            <translation><!-- MASTER_IPHONE --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>MASTER_MFL</masterLabel>
            <translation><!-- MASTER_MFL --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>MFL</masterLabel>
            <translation><!-- MFL --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>OptiMax</masterLabel>
            <translation>OptiMax</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>ServicePower</masterLabel>
            <translation><!-- ServicePower --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>iPad</masterLabel>
            <translation><!-- iPad --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>iPhone</masterLabel>
            <translation><!-- iPhone --></translation>
        </picklistValues>
    </fields>
    <gender>Feminine</gender>
    <nameFieldLabel>Nome do registro</nameFieldLabel>
</CustomObjectTranslation>
