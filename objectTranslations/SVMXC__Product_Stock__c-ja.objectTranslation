<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>製品在庫</value>
    </caseValues>
    <fields>
        <label><!-- Location Name --></label>
        <name>Location_Name__c</name>
    </fields>
    <fields>
        <help><!-- Shows the SAP Employee Id for this Technician --></help>
        <label><!-- SAP Employee Id --></label>
        <name>SAP_Employee_Id__c</name>
    </fields>
    <fields>
        <help><!-- This is where the product stock is located, customer location --></help>
        <label><!-- SAP Ship To ID --></label>
        <name>SAP_Ship_To_ID__c</name>
    </fields>
    <fields>
        <help><!-- This field used internally by ServiceMax. This field is being calculated by inventory engine to stop simultaneous update of records. --></help>
        <label><!-- Actual Quantity Before Update --></label>
        <name>SVMXC__ActualQtyBeforeUpdate__c</name>
    </fields>
    <fields>
        <label><!-- Allocated Qty --></label>
        <name>SVMXC__Allocated_Qty__c</name>
    </fields>
    <fields>
        <label><!-- At or Below Reorder --></label>
        <name>SVMXC__At_or_Below_Reorder__c</name>
    </fields>
    <fields>
        <label><!-- Available Qty --></label>
        <name>SVMXC__Available_Qty__c</name>
    </fields>
    <fields>
        <help><!-- Flag indicates if transaction is for/ by a Partner or not. --></help>
        <label><!-- IsPartnerRecord --></label>
        <name>SVMXC__IsPartnerRecord__c</name>
    </fields>
    <fields>
        <help><!-- Flag indicates if transaction is for/by a Partner or not. --></help>
        <label><!-- Is Partner --></label>
        <name>SVMXC__IsPartner__c</name>
    </fields>
    <fields>
        <help><!-- Current physical location of the product. This is a lookup to an existing site record. --></help>
        <label><!-- Location --></label>
        <name>SVMXC__Location__c</name>
        <relationshipLabel><!-- Product Stock --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Lookup to Partner Account. --></help>
        <label><!-- Partner Account --></label>
        <name>SVMXC__Partner_Account__c</name>
        <relationshipLabel><!-- Product Stock --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Lookup to Partner Contact. --></help>
        <label><!-- Partner Contact --></label>
        <name>SVMXC__Partner_Contact__c</name>
        <relationshipLabel><!-- Product Stock --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- This field is combination of Product, Location and Status field to make the record unique. --></help>
        <label><!-- Composite Key --></label>
        <name>SVMXC__ProdStockCompositeKey__c</name>
    </fields>
    <fields>
        <help><!-- Product cost, copied from Product. For internal use only. --></help>
        <label><!-- Product Cost --></label>
        <name>SVMXC__Product_Cost__c</name>
    </fields>
    <fields>
        <help><!-- Name of the product. This is a lookup to an existing salesforce product record. --></help>
        <label><!-- Product --></label>
        <name>SVMXC__Product__c</name>
        <relationshipLabel><!-- Product Stock --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Quantity of product in the provided location. --></help>
        <label><!-- Qty --></label>
        <name>SVMXC__Quantity2__c</name>
    </fields>
    <fields>
        <help><!-- Reorder level of this product for the provided location. --></help>
        <label><!-- Reorder Point --></label>
        <name>SVMXC__Reorder_Level2__c</name>
    </fields>
    <fields>
        <help><!-- Reorder quantity for the provided product and location when reorder level is reached --></help>
        <label><!-- Reorder Qty --></label>
        <name>SVMXC__Reorder_Quantity2__c</name>
    </fields>
    <fields>
        <help><!-- Calculated required quantity for reordering. --></help>
        <label><!-- Required Qty --></label>
        <name>SVMXC__Required_Quantity2__c</name>
    </fields>
    <fields>
        <help><!-- Current Status of Product-Stock. --></help>
        <label><!-- Status --></label>
        <name>SVMXC__Status__c</name>
        <picklistValues>
            <masterLabel>Available</masterLabel>
            <translation>入手可能</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Consumed</masterLabel>
            <translation>使用済み</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>In Transit</masterLabel>
            <translation>輸送中</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Returned</masterLabel>
            <translation>返品済み</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Scrap</masterLabel>
            <translation>スクラップ</translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Value of current stock. Used for reporting purposes. --></help>
        <label><!-- Stock Value --></label>
        <name>SVMXC__Stock_Value__c</name>
    </fields>
    <fields>
        <label><!-- Batch Number --></label>
        <name>SVMX_Batch_Number__c</name>
    </fields>
    <fields>
        <help><!-- Part number of the Product in Stock --></help>
        <label><!-- Part Number --></label>
        <name>SVMX_Part_Number__c</name>
    </fields>
    <fields>
        <label><!-- Product Name --></label>
        <name>SVMX_Product_Name__c</name>
    </fields>
    <fields>
        <label><!-- Returnable Part --></label>
        <name>SVMX_Returnable_Part__c</name>
    </fields>
    <fields>
        <help><!-- This is the storage plant where the stock is held --></help>
        <label><!-- SAP Plant --></label>
        <name>SVMX_SAP_Plant__c</name>
    </fields>
    <fields>
        <label><!-- Serial Number --></label>
        <name>SVMX_Serial_Number__c</name>
    </fields>
    <fields>
        <help><!-- This indicates whether the stock has any special attributes. Such as label W in SAP --></help>
        <label><!-- Stock Type Indicator --></label>
        <name>SVMX_Stock_Type_Indicator__c</name>
    </fields>
    <fields>
        <help><!-- Name of the service group. Is a lookup to an existing service group in ServiceMax --></help>
        <label><!-- Service Team --></label>
        <name>Service_Team__c</name>
        <relationshipLabel><!-- Product Stock --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Source Batch Number, which is kept in this field when spareparts are re-labeled in SAP --></help>
        <label><!-- Source Batch Number --></label>
        <name>Source_Batch_Number__c</name>
    </fields>
    <fields>
        <help><!-- Source Serial Number, which is kept in this field when spareparts are re-labeled in SAP --></help>
        <label><!-- Source Serial Number --></label>
        <name>Source_Serial_Number__c</name>
    </fields>
    <fields>
        <help><!-- The technician to whom the product stock is linked to --></help>
        <label><!-- Technician/Equipment --></label>
        <name>Technician__c</name>
        <relationshipLabel><!-- Product Stock --></relationshipLabel>
    </fields>
    <nameFieldLabel>名前</nameFieldLabel>
</CustomObjectTranslation>
