<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>greaterOrEqual</operator>
            <values>LAST_N_DAYS:7</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>greaterOrEqual</operator>
            <values>LAST_N_DAYS:30</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>greaterOrEqual</operator>
            <values>LAST_90_DAYS</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>greaterOrEqual</operator>
            <values>LAST_N_DAYS:180</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>greaterOrEqual</operator>
            <values>LAST_N_DAYS:365</values>
        </dashboardFilterOptions>
        <name>Select implant date time frame</name>
    </dashboardFilters>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>United Kingdom</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Ireland</values>
        </dashboardFilterOptions>
        <name>Select a country</name>
    </dashboardFilters>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Amy Grigg</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Alex Jackson</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Ben Thomas</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Craig Jeavons</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Gina Willis</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Irvin Verallo</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Jonathan Allen</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Luke Martin</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Nicholas Bouwer</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Sarah Anderson</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Suze Bennett</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Neil Barbour</values>
        </dashboardFilterOptions>
        <name>Select a user</name>
    </dashboardFilters>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Medium</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Donut</componentType>
            <dashboardFilterColumns>
                <column>Implant__c.Implant_Date_Of_Surgery__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Implant__c.Country_Text__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Implant__c.User_ID__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>Implant__c.Procedure_Text__c</groupingColumn>
            <groupingSortProperties/>
            <header>Implant Support - Procedure ratio</header>
            <legendPosition>Bottom</legendPosition>
            <report>UKI_CRHF_Technical_Service/Implants_with_contacts_in_UKI</report>
            <showPercentage>true</showPercentage>
            <showTotal>true</showTotal>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Procedure ratio</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Implant__c.Implant_Date_Of_Surgery__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Implant__c.Country_Text__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Implant__c.User_ID__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>Implant__c.User_ID__c</column>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>RowCount</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <groupingSortProperties/>
            <header>Implant support - by employee</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>UKI_CRHF_Technical_Service/Implants_with_contacts_in_UKI</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>By employee</title>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Medium</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Donut</componentType>
            <dashboardFilterColumns>
                <column>Implant__c.Implant_Date_Of_Surgery__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Implant__c.Country_Text__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Implant__c.User_ID__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>Implant__c.Therapy__c</groupingColumn>
            <groupingSortProperties/>
            <header>Implant Support - Therapy ratio</header>
            <legendPosition>Bottom</legendPosition>
            <report>UKI_CRHF_Technical_Service/Implants_with_contacts_in_UKI_v2</report>
            <showPercentage>true</showPercentage>
            <showTotal>true</showTotal>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Therapy ratio</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Implant__c.Implant_Date_Of_Surgery__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Implant__c.Country_Text__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Implant__c.User_ID__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>Implant__c.Implant_Implanting_Account__c</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>RowCount</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <groupingSortProperties/>
            <header>Implant support - by account</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>UKI_CRHF_Technical_Service/Implants_with_contacts_in_UKI_v2</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>By account</title>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Medium</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>x</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Line</componentType>
            <dashboardFilterColumns>
                <column>Implant__c.Implant_Date_Of_Surgery__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Implant__c.Country_Text__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Implant__c.User_ID__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>Implant__c.Implant_Date_Of_Surgery__c</groupingColumn>
            <groupingSortProperties/>
            <header>Implant Support - trend</header>
            <legendPosition>Bottom</legendPosition>
            <report>UKI_CRHF_Technical_Service/Implants_with_contacts_in_UKI_v2</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>by date</title>
            <useReportChart>false</useReportChart>
        </components>
    </rightSection>
    <runningUser>xavier.boutin@medtronic.com</runningUser>
    <textColor>#000000</textColor>
    <title>CRHF - UK &amp; IE - Implant support Dashboard</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
