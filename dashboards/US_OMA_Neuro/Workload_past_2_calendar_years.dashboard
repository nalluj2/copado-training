<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>LeftToRight</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardType>SpecifiedUser</dashboardType>
    <description>High level perspective on Med Info workload</description>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Manual</chartAxisRange>
            <chartAxisRangeMax>100.0</chartAxisRangeMax>
            <chartAxisRangeMin>0.0</chartAxisRangeMin>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA1</column>
            </chartSummary>
            <componentType>LineGrouped</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>(Kelly as contractor through 8-20-15)</footer>
            <groupingColumn>Case.Date_Received_Date__c</groupingColumn>
            <groupingColumn>BucketField_49003468</groupingColumn>
            <groupingSortProperties/>
            <header>Contractor Coverage</header>
            <legendPosition>Bottom</legendPosition>
            <report>US_OMA_Neuro/Metrics_MI_IR_Contractor</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>HCP MI: Q1 FY14 to start of Q2 FY16</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>LineGrouped</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>Case.Month__c</groupingColumn>
            <groupingColumn>Case.Year__c</groupingColumn>
            <groupingSortProperties/>
            <header>Overall Activity since FY14</header>
            <legendPosition>Bottom</legendPosition>
            <report>US_OMA_Neuro/Cases_by_Q_FY_and_Therapy</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>HCP + Internal MI</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>ColumnStacked</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>We are seeing a decline in urgent and complex cases. Number of urgent cases dropped by over half in FY17.</footer>
            <groupingSortProperties/>
            <header>By Priority</header>
            <legendPosition>Bottom</legendPosition>
            <report>US_OMA_Neuro/Metrics_MI_IR_Priority</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>HCP MI</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnStacked</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>Case.Date_Received_Date__c</groupingColumn>
            <groupingColumn>Case.Repeat_Case__c</groupingColumn>
            <groupingSortProperties/>
            <header>By Repeat Case</header>
            <legendPosition>Bottom</legendPosition>
            <report>US_OMA_Neuro/MI_Repeats</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>HCP MI</title>
            <useReportChart>false</useReportChart>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Manual</chartAxisRange>
            <chartAxisRangeMax>100.0</chartAxisRangeMax>
            <chartAxisRangeMin>0.0</chartAxisRangeMin>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA1</column>
            </chartSummary>
            <componentType>LineGrouped</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>(Kelly as FTE starting 8-21-15)</footer>
            <groupingColumn>Case.Date_Received_Date__c</groupingColumn>
            <groupingColumn>BucketField_49003468</groupingColumn>
            <groupingSortProperties/>
            <header>Contractor Coverage</header>
            <legendPosition>Bottom</legendPosition>
            <report>US_OMA_Neuro/MI_Contractor_vs_FTE_from_8_21_15</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>HCP MI: Most of Q2 FY16 to present</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>BarStacked</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>OWNER</groupingColumn>
            <groupingColumn>BucketField_86869050</groupingColumn>
            <groupingSortProperties/>
            <header>MA Staff therapy coverage, past 2 years</header>
            <legendPosition>Bottom</legendPosition>
            <report>US_OMA_Neuro/MI_Past_2_Years</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowValueAscending</sortBy>
            <title>HCP MI</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <groupingSortProperties/>
            <header>MA Staff, past 2 years</header>
            <legendPosition>Bottom</legendPosition>
            <report>US_OMA_Neuro/MI_Past_2_Years</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>HCP MI</title>
            <useReportChart>true</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Medium</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <groupingSortProperties/>
            <header>Timeliness</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>US_OMA_Neuro/MI_Cases_by_Priority_Level</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <sortBy>RowLabelAscending</sortBy>
            <title>HCP MI:  FYTD Normal Priority</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <groupingSortProperties/>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>US_OMA_Neuro/MI_Turnaround_Urgent_Priority</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <sortBy>RowLabelAscending</sortBy>
            <title>HCP MI:  FYTD Urgent Priority</title>
        </components>
    </rightSection>
    <runningUser>liz.a.driessen@medtronic.com</runningUser>
    <textColor>#000000</textColor>
    <title>Workload - past 2 calendar years</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
