<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>notContain</operator>
            <values>MMX</values>
        </dashboardFilterOptions>
        <name>Created By</name>
    </dashboardFilters>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>User.Call_Records__c$OwnerId$Created_By_TEXT__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Last 30 Days, Field Force users only</footer>
            <groupingSortProperties/>
            <header>Average Call Records Logged Per FF</header>
            <legendPosition>Bottom</legendPosition>
            <report>South_Africa_Adoption_Reports/Avg_CR_per_FF_Last_30_Days_by_BU_SA</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Average Call Records Logged Per FF L30D</title>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>User.Call_Records__c$OwnerId$Created_By_TEXT__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Last 7 Days, Field Force users only</footer>
            <groupingSortProperties/>
            <legendPosition>Bottom</legendPosition>
            <report>South_Africa_Adoption_Reports/Avg_CR_per_FF_L7D_by_BU_South_Africa</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Average Call Records Logged Per FF L7D</title>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA1</column>
            </chartSummary>
            <componentType>ColumnStacked</componentType>
            <dashboardFilterColumns>
                <column>Call_Records__c.Created_By_TEXT__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>BucketField_4729708</groupingColumn>
            <groupingColumn>Call_Records__c.Business_Unit__c</groupingColumn>
            <groupingSortProperties/>
            <header>Call Record Quality</header>
            <legendPosition>Bottom</legendPosition>
            <report>South_Africa_Adoption_Reports/South_Africa_Call_Records_w_wo_comments</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelDescending</sortBy>
            <title>Call Records With / Without Comments</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA1</column>
            </chartSummary>
            <componentType>ColumnStacked</componentType>
            <dashboardFilterColumns>
                <column>Call_Records__c.Created_By_TEXT__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>BucketField_89397129</groupingColumn>
            <groupingColumn>Call_Records__c.Business_Unit__c</groupingColumn>
            <groupingSortProperties/>
            <legendPosition>Bottom</legendPosition>
            <report>South_Africa_Adoption_Reports/South_Africa_Call_Records_w_wo_duration</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Call Records With / Without Duration</title>
            <useReportChart>false</useReportChart>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>ColumnStacked</componentType>
            <dashboardFilterColumns>
                <column>User.Call_Records__c$OwnerId$Created_By_TEXT__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>All users in South Africa.  &quot;All&quot; denotes a user with a XBU profile</footer>
            <groupingSortProperties/>
            <header>Call Records Logged Per Week</header>
            <legendPosition>Bottom</legendPosition>
            <report>South_Africa_Adoption_Reports/Call_Records_Per_Week_BU_South_Africa</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Call Records Logged Per Week by BU</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA1</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>User.Call_Records__c$OwnerId$Created_By_TEXT__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>All users in South Africa.  &quot;All&quot; denotes a user with a XBU profile</footer>
            <groupingColumn>User.Call_Records__c$OwnerId$Business_Unit__c</groupingColumn>
            <groupingSortProperties/>
            <header>Total Call Records Logged</header>
            <legendPosition>Bottom</legendPosition>
            <report>South_Africa_Adoption_Reports/Total_Call_Records_per_BU_South_Africa</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Total # of Call Records Logged by BU</title>
            <useReportChart>false</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Call_Records__c.Created_By_TEXT__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>CUST_OWNER_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>FORMULA1</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>Current FY, excluding admin time</footer>
            <groupingSortProperties/>
            <header>Hall of Fame</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>South_Africa_Adoption_Reports/Total_No_of_Call_Records_South_Africa</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Top 10 # of CRs Logged</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Call_Records__c.Created_By_TEXT__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>CUST_OWNER_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>FORMULA2</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>Current FY, excluding admin time</footer>
            <groupingSortProperties/>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>South_Africa_Adoption_Reports/Total_No_of_Call_Records_South_Africa1</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Top 10 Duration of Calls</title>
        </components>
    </rightSection>
    <runningUser>diane.pithey@medtronic.com</runningUser>
    <textColor>#000000</textColor>
    <title>South Africa Call Record Adoption</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
