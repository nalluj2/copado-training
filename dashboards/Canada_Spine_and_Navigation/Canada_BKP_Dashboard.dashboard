<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Medium</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>last 7 days</footer>
            <groupingSortProperties/>
            <header>BKP call records</header>
            <indicatorBreakpoint1>5.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>7.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Canada_Spine_and_Navigation/BKP_Avg_Calls_Per_Country</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <sortBy>RowValueDescending</sortBy>
            <title>Call Records by Sales Rep</title>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Medium</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Pie</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>last 30 days</footer>
            <groupingSortProperties/>
            <header>BKP Accounts Canada</header>
            <legendPosition>Bottom</legendPosition>
            <report>Canada_Spine_and_Navigation/BKP_Call_Records</report>
            <showPercentage>true</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Call Records by Sales Rep L30D</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Pie</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>Call_Topics__c.Call_Topic_Products_Concatendated__c</groupingColumn>
            <groupingSortProperties/>
            <header>BKP Accounts Canada</header>
            <legendPosition>Bottom</legendPosition>
            <report>Canada_Spine_and_Navigation/BKP_Call_Records</report>
            <showPercentage>true</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Call Records by Product L30D</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Pie</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>Call_Records__c.Call_ActTypes_Concatenated__c</groupingColumn>
            <groupingSortProperties/>
            <header>BKP Accounts Canada</header>
            <legendPosition>Bottom</legendPosition>
            <report>Canada_Spine_and_Navigation/BKP_Call_Record_Activities</report>
            <showPercentage>true</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Call Records by Activity Type L30D</title>
            <useReportChart>false</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Medium</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Manual</chartAxisRange>
            <chartAxisRangeMax>100.0</chartAxisRangeMax>
            <chartAxisRangeMin>0.0</chartAxisRangeMin>
            <componentType>Column</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>last 30 days</footer>
            <groupingSortProperties/>
            <header>BKP Accounts Canada</header>
            <legendPosition>Bottom</legendPosition>
            <report>Canada_Spine_and_Navigation/Canada_Accts_Biologics</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Account Coverage by Segment</title>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Bar</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>last 30 days</footer>
            <groupingColumn>Call_Topics__c.Call_Topic_Products_Concatendated__c</groupingColumn>
            <groupingSortProperties/>
            <header>BKP Accounts Canada</header>
            <legendPosition>Bottom</legendPosition>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Canada_Spine_and_Navigation/BKP_Call_Records</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Call Records by Product</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>ColumnStacked</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>last 30 days</footer>
            <groupingSortProperties/>
            <header>BKP Accounts Canada</header>
            <legendPosition>Bottom</legendPosition>
            <report>Canada_Spine_and_Navigation/BKP_Call_Records_Accounts</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Call Records by Sales Rep</title>
            <useReportChart>false</useReportChart>
        </components>
    </rightSection>
    <runningUser>sfe.jeff.mignacco@medtronic.com</runningUser>
    <textColor>#000000</textColor>
    <title>Canada BKP Dashboard</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
