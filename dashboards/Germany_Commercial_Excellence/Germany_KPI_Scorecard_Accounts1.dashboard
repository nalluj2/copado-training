<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardType>SpecifiedUser</dashboardType>
    <description>KPI Scorecard</description>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Narrow</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <groupingSortProperties/>
            <header>Alle aktiven Kunden in Deutschland</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Germany_KPI/KPI_Alle_Accounts</report>
            <showPicturesOnTables>false</showPicturesOnTables>
            <sortBy>RowLabelAscending</sortBy>
            <title>All &quot;Accounts&quot;</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <groupingSortProperties/>
            <header>SFDC Kundentypen</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Germany_KPI/KPI_Account_Record_Type21</report>
            <showPicturesOnTables>false</showPicturesOnTables>
            <sortBy>RowValueDescending</sortBy>
            <title>Account Record Type</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <groupingSortProperties/>
            <header>Praxen und Krankenhäuser</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Germany_KPI/KPI_Account_Type_KHs_oder_Praxen1</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <sortBy>RowValueDescending</sortBy>
            <title>Practice and Hospitals</title>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Narrow</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <groupingSortProperties/>
            <header>Kunden keiner BU zugeordnet</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Germany_KPI/KPI_Accounts_without_BU</report>
            <showPicturesOnTables>false</showPicturesOnTables>
            <sortBy>RowValueDescending</sortBy>
            <title>Accounts without BU</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <groupingSortProperties/>
            <header>PLZ, Straße oder Ort fehlt</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Germany_KPI/KPI_without_correct_address</report>
            <showPicturesOnTables>false</showPicturesOnTables>
            <sortBy>RowValueDescending</sortBy>
            <title>Accounts without correct address</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <groupingSortProperties/>
            <header>Anzahl der betreuten Goldkunden</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Germany_KPI/KPI_Goldkunden1</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <sortBy>RowLabelAscending</sortBy>
            <title>Gold Customers</title>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Narrow</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>Die Anzahl ist eine Einschätzung des Systems. Die Vorschläge des Systems sind nicht immer zutreffend</footer>
            <groupingSortProperties/>
            <header>Doppelte Kunden in SFDC</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Germany_KPI/KPI_Accounts_and_Contacts_Duplicate</report>
            <showPicturesOnTables>false</showPicturesOnTables>
            <sortBy>RowValueDescending</sortBy>
            <title>All Duplicates</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>Account.EUR_Buying_Group_ID_Text__c</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>RowCount</column>
                <decimalPlaces>0</decimalPlaces>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <decimalPrecision>0</decimalPrecision>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <groupingSortProperties/>
            <header>SAP Accounts verbunden zu einer BG</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Germany_KPI/KPI_GPO</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>GPOs with SAP Accounts</title>
        </components>
    </rightSection>
    <runningUser>kasra.arzideh@medtronic.com2</runningUser>
    <textColor>#000000</textColor>
    <title>Germany KPI Scorecard Accounts</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
