<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CPT Clinic 12</label>
    <protected>false</protected>
    <values>
        <field>Compensation_Amount__c</field>
        <value xsi:type="xsd:double">175.0</value>
    </values>
    <values>
        <field>Compensation_Hour_Price__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Training_Category__c</field>
        <value xsi:type="xsd:string">CPT Clinic</value>
    </values>
    <values>
        <field>Training_Method__c</field>
        <value xsi:type="xsd:string">Group 4+</value>
    </values>
    <values>
        <field>Training_Type__c</field>
        <value xsi:type="xsd:string">New Pump</value>
    </values>
</CustomMetadata>
