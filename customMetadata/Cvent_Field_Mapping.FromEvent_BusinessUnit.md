<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>FromEvent - BusinessUnit</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Cvent_API_Setting__c</field>
        <value xsi:type="xsd:string">CVENT</value>
    </values>
    <values>
        <field>Cvent_Fieldname__c</field>
        <value xsi:type="xsd:string">A0ED3774-AE4A-44D8-9BDA-9E5C27BBD670</value>
    </values>
    <values>
        <field>Salesforce_Fieldname__c</field>
        <value xsi:type="xsd:string">CVent_BusinessUnit__c</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">FromEvent</value>
    </values>
</CustomMetadata>
