<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>FromEvent - CampaignId</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Cvent_API_Setting__c</field>
        <value xsi:type="xsd:string">CVENT</value>
    </values>
    <values>
        <field>Cvent_Fieldname__c</field>
        <value xsi:type="xsd:string">77E6BDCA-025A-4324-B4C1-4C31B7A3E7EF</value>
    </values>
    <values>
        <field>Salesforce_Fieldname__c</field>
        <value xsi:type="xsd:string">CampaignId</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">FromEvent</value>
    </values>
</CustomMetadata>
