<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>FromEvent - TherapyProductGroup</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Cvent_API_Setting__c</field>
        <value xsi:type="xsd:string">CVENT</value>
    </values>
    <values>
        <field>Cvent_Fieldname__c</field>
        <value xsi:type="xsd:string">1506F3AF-80AE-4D42-B206-E7AF2934FE86</value>
    </values>
    <values>
        <field>Salesforce_Fieldname__c</field>
        <value xsi:type="xsd:string">CVent_TherapyProductGroup__c</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">FromEvent</value>
    </values>
</CustomMetadata>
