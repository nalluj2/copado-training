<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CVENT EMEA</label>
    <protected>false</protected>
    <values>
        <field>APEX_Batch_Size__c</field>
        <value xsi:type="xsd:double">100.0</value>
    </values>
    <values>
        <field>Accountnumber__c</field>
        <value xsi:type="xsd:string">MEDTRMN01</value>
    </values>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Password__c</field>
        <value xsi:type="xsd:string">tzuEgUiBl8D</value>
    </values>
    <values>
        <field>Timeout__c</field>
        <value xsi:type="xsd:double">55000.0</value>
    </values>
    <values>
        <field>URLEndpoint__c</field>
        <value xsi:type="xsd:string">https://api.cvent.com/soap/V200611.ASMX</value>
    </values>
    <values>
        <field>Username__c</field>
        <value xsi:type="xsd:string">MEDTRMN01Api5</value>
    </values>
</CustomMetadata>
