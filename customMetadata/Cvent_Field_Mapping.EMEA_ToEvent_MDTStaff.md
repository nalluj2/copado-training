<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>EMEA ToEvent - MDTStaff</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Cvent_API_Setting__c</field>
        <value xsi:type="xsd:string">CVENT_EMEA</value>
    </values>
    <values>
        <field>Cvent_Fieldname__c</field>
        <value xsi:type="xsd:string">A202A786-4386-49CF-8FAD-CC6C2FA6BF58</value>
    </values>
    <values>
        <field>Salesforce_Fieldname__c</field>
        <value xsi:type="xsd:string">Total_MDT_Staff__c</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">ToEvent</value>
    </values>
</CustomMetadata>
