<apex:component controller="ctrl_RelatedList" selfClosing="true">

    <apex:attribute required="true" type="wr_RelatedList" name="wrRelatedList" description="Parameters that are collected in a wr_RelatedList Wrapper Class" assignTo="{!owrRelatedList}" />

    <style type="text/css">
        /* ERROR CSS */
        .exceptionText{
            font-style:italic;
            font-size:14px;
            font-weight:bold;
            text-align:center;
            color:red;
        }

        .custPopup{
            background: #fff ;
            border-width: 2px;
            border-style:inset;
            z-index: 9999;
            left: 720px;
            padding:10px;
            position: absolute;
            /* These are the 3 css properties you will need to change so the popup
            displays in the center of the screen. First set the width. Then set
            margin-left to negative half of what the width is. You can add
            the height property for a fixed size pop up if you want.*/
            width: 650px; 
            margin-left: -250px;
            top:100px;
        }
        .popupBackground{
            background-color:black;
            opacity: 0.20;
            filter: alpha(opacity = 20);
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            z-index: 9998;
        }
    </style>

    <script>
        
        function noenter(ev)  {
            if (window.event && window.event.keyCode == 13 || ev.which == 13) {
                doSearch();
                return false;
            } else {
                return true;
            }
        }
    
    </script>


    <apex:outputPanel id="opTitle" rendered="{!owrRelatedList.bShowTitle}">
        <apex:sectionHeader title="{!tTitle}" subtitle="{!tSubTitle}"/>
    </apex:outputPanel> 
    <apex:outputPanel id="opMain">
        <apex:outputPanel id="opRelatedData">

            <apex:outputPanel id="opBackToParent" rendered="{!owrRelatedList.bShowBackToParent}">
                <apex:commandButton value="{!tTilteBackButton}" action="{!backToMaster}" />
                <br /><br />
            </apex:outputPanel>
            
            <apex:outputPanel id="opFilterSBUData">
                <table id="tblFilterSBU" name="tblFilterSBU" border="0">
                    <tr>
                        <td valign="top">
                            BU
                        </td>
                        <td>
                        <apex:selectList id="slFilterBU" value="{!mapFilterFieldName_Value['BU']}" size="1" disabled="{!bDisabledFilter_BU}">
                                <apex:selectOptions value="{!mapFilterFieldName_SelectOption['BU']}"/>
                                <apex:actionSupport event="onchange" reRender="pbData,opFilterSBUData,opFilterData,opDebug" action="{!filterBUData}" status="asPleaseWait" />
                            </apex:selectList><p/>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td valign="top">
                            SBU
                        </td>
                        <td>
                            <apex:selectList id="slFilterSBU" value="{!mapFilterFieldName_Value['SBU']}" size="1" disabled="{!bDisabledFilter_SBU}">
                                <apex:selectOptions value="{!mapFilterFieldName_SelectOption['SBU']}"/>
                                <apex:actionSupport event="onchange" reRender="pbData,opFilterData,opDebug" action="{!filterBUData}" status="asPleaseWait" />
                            </apex:selectList><p/>
                        </td>
                    </tr>
                </table>
            </apex:outputPanel>
            
            <apex:outputPanel id="opFilterData">
                <table id="tblFilter" name="tblFilter" border="0">
                    <tr>
                        <apex:repeat value="{!filterFields}" var="oFilter">
                            <td valign="top">
                                {!mapFieldName_Label[oFilter]}
                            </td>
                            <td>
                                <apex:selectList id="slFilter" value="{!mapFilterFieldName_Value[oFilter]}" size="1">
                                    <apex:selectOptions value="{!mapFilterFieldName_SelectOption[oFilter]}"/>
                                    <apex:actionSupport event="onchange" reRender="pbData,opFilterData,opDebug" action="{!filterData}" status="asPleaseWait" />
                                </apex:selectList><p/>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </apex:repeat>
                    </tr>
                </table>
            </apex:outputPanel>

            <apex:actionstatus id="asPleaseWait">
                <apex:facet name="start">
                    <center>
                        <span>
                            <img class="waitingImage" src="/img/loading.gif" title="Loading..." />
                            <br />
                            Loading...
                        </span>
                    </center>
                     <br /><br />
                </apex:facet>
                <apex:facet name="stop">

                    <apex:outputPanel id="opData">
                        <apex:pageblock mode="maindetail" id="pbData">
                            <br />
                            <apex:messages id="apexMessages"  styleclass="exceptionText"/>

                            <apex:actionFunction name="doSearch" action="{!doSearch}" reRender="opData"/>

                            <apex:outputPanel id="opTableActions" style="width: 100%;">

                                <div style="float:right">
                                    <apex:inputText value="{!searchString}" onkeypress="return noenter(event);" style="margin-right : 10px"/>
                                    <apex:commandButton value="Search" action="{!doSearch}" reRender="opData" /> 
                                </div>

                                <div style="float:left">
                                    <apex:outputPanel id="opButtonNewData" rendered="{!owrRelatedList.bShowAddNewButton}">
                                        <input type="button" class="btn inputButtonSelectData" onclick="relatedList_NewData_OnClick();" value="{!tTitleNewData}" />
                                    </apex:outputPanel>

                                    <apex:outputPanel id="opButtonExport" rendered="{!owrRelatedList.bShowExportButton}">
                                        <apex:commandButton value="Export ({!owrRelatedList.tExportType})" 
                                            onclick="window.open('{!ExportDataURL}','_blank','height=600,location=no,resizable=yes,toolbar=no,status=no,menubar=no,scrollbars=1', 1)"
                                            rendered="{!owrRelatedList.bShowExportButton}"
                                        />
                                    </apex:outputPanel>

                                </div>

                            </apex:outputPanel>

                            <br/><br/>
                            
                            <apex:pageblockTable id="pbtData" value="{!records}" var="oData">

                                <apex:repeat value="{!visibleFields}" var="oField">
                                    <apex:column >
                                        <apex:facet name="header" >
                                            <apex:commandLink action="{!sortData}">
                                                <apex:outputText value="{!mapFieldName_Label[oField]}" style="color:{!IF(oField == tSortBy, 'red', '')};"/>
                                                
                                                <apex:outputText escape="false" rendered="{!IF(AND(tSortDir == 'ASC', oField == tSortBy), TRUE, FALSE)}">
                                                    <img src="/s.gif" alt="Sorted Ascending" class="sortAsc" title="Sorted Ascending" />
                                                </apex:outputText>
                                                <apex:outputText escape="false" rendered="{!IF(AND(tSortDir == 'DESC', oField == tSortBy), TRUE, FALSE)}">
                                                    <img src="/s.gif" alt="Sorted Descending" class="sortDesc" title="Sorted Descending" />
                                                </apex:outputText>
            
                                                <apex:param value="{!oField}" name="column" assignTo="{!tSortBy}" />
                                            </apex:commandLink>
                                        </apex:facet>
                                        
                                        <apex:outputLink value="{!IF(mapFieldName_Link[oField]=='NONE', '', '/' + oData[mapFieldName_Link[oField]])}" disabled="{!IF(mapFieldName_Link[oField]=='NONE', TRUE, FALSE)}" target="_parent">
                                            <apex:outputField value="{!oData[oField]}"/>
                                        </apex:outputLink>
                                    </apex:column>
                                </apex:repeat>                    
                            </apex:pageblockTable>

                            <apex:outputPanel layout="block" style="width: 100%; margin-top : 10px" >
                                                 
                                <apex:outputLabel value="Total records: {!resultSize}" />
                                                
                                <apex:selectList value="{!pageSize}" size="1" style="float : right">
                                    <apex:selectOption itemValue="5" itemLabel="5" />
                                    <apex:selectOption itemValue="10" itemLabel="10" />
                                    <apex:selectOption itemValue="25" itemLabel="25" />
                                    <apex:selectOption itemValue="50" itemLabel="50" />
                                    <apex:selectOption itemValue="100" itemLabel="100" />
                                    <apex:actionSupport event="onchange" reRender="opData" />
                                </apex:selectList>
                                
                                <apex:outputLabel value="Records per page: " style="float : right; margin-right: 10px"/>
                                
                            </apex:outputPanel>   

                            <apex:outputPanel layout="block" style="text-align:center" >

                                <apex:commandButton value="Previous" action="{!previous}" disabled="{! hasPrevious == false}" reRender="opData" /> 
                                <apex:outputLabel value="{!pageNumber} / {!totalPages}" style="margin: 15px"/>
                                <apex:commandButton value="Next" action="{!next}" disabled="{! hasNext == false}" reRender="opData" />

                            </apex:outputPanel>                                             
                            

                            <apex:outputPanel id="opWarning">
                                <center>
                                    <br />
                                    <apex:outputLabel value="{!tWarning}" styleClass="exceptionText"/>
                                </center>
                            </apex:outputPanel>


                            <apex:outputPanel id="opMessage">
                                <center>
                                    <apex:outputText styleclass="exceptionText" rendered="{!IF(tMessageDescription=='',false,true)}">
                                        <br />
                                        {!$Label[tMessageDescription]}
                                    </apex:outputText>
                                </center>
                            </apex:outputPanel>
                            
                        </apex:pageBlock>
                    </apex:outputPanel>

                </apex:facet>
            </apex:actionstatus>

            <apex:outputPanel id="opDebug" rendered="{!owrRelatedList.bShowDebug}">
                <apex:repeat value="{!lstDebug}" var="tDebug">
                    {!tDebug}
                    <br /><hr /><br />
                </apex:repeat>
            </apex:outputPanel>

            <script>
                function relatedList_NewData_OnClick(){
                    try{
                        var tFunctionName   = '{!owrRelatedList.tJSNewData}';
                        if (tFunctionName != ''){
                            // Create the function
                            var oFunction = window[tFunctionName];
                 
                            // Call the function
                            oFunction();
                        }
                    }catch(oEX){
                    }       
                }
            </script>
            
        </apex:outputPanel>

    </apex:outputPanel>

</apex:component>