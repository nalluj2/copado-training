import {LightningElement, api} from 'lwc';

export default class ConfirmationDialog extends LightningElement {
    @api visible; //used to hide/show dialog
    @api title; //modal title
    //@api name; //reference name of the component
    @api message; //modal message
    @api confirmLabel; //confirm button label
    @api cancelLabel; //cancel button label
    @api recordId; //Id of the record
    @api action;//action

    //handles button clicks
    handleClick(event){
        //creates object which will be published to the parent component
        console.log('in lwc component');
        let modalDetails = {
            recordId: this.recordId,
            status: event.target.name,
            action: this.action
        };

        //dispatch a 'click' event so the parent component can handle it
        this.dispatchEvent(new CustomEvent('click', {detail: {modalDetails}}));
    }
}