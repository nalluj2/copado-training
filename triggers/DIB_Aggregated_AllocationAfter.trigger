/**
 *	Description : It will find out when the Fiscal period starts (FM & FY must match) and it will 
 *		fill out that date format field to Fiscal_Start_Date__c on Aggregated Entry table. 
 *		Even if a before statement is more correct for this quind of needs, by using an after statement, we will have less problems and more performance. 
 *	Date : 9/03/2010
 *	Created by MC : ABSI 
 */
trigger DIB_Aggregated_AllocationAfter on DIB_Aggregated_Allocation__c (after insert, after update) {

	// Get the trigger.new values and put them on a FiscalMonth & FiscalYear list to have all the possible combinations
	
	List<String> fiscalMonths = new List<String>{} ; 
	List<String> fiscalYears = new List<String>{} ;
	set<Id> sUpdateIds = new Set<Id>();
	if(controllerAllocationAggregated.mAccountAggregationUpsert == null){
		controllerAllocationAggregated.mAccountAggregationUpsert = new Map<String,set<Id>>(); 
	}else if(controllerAllocationAggregated.mAccountAggregationUpsert != null){
		sUpdateIds = controllerAllocationAggregated.mAccountAggregationUpsert.get(Userinfo.getUserId());
	}
		
	// Items fired : 	
	List<DIB_Aggregated_Allocation__c> items = Trigger.new; 
	
	// items to Update : 
	List<DIB_Aggregated_Allocation__c> itemsToUpdate = new List<DIB_Aggregated_Allocation__c>{} ; 

		 	 
	for(DIB_Aggregated_Allocation__c aa : items){  
		fiscalMonths.add(aa.Fiscal_Month__c); 
		fiscalYears.add(aa.Fiscal_Year__c);
	}
	
	System.debug('#####fiscalMonths :' + fiscalMonths); 	 
	System.debug('#####fiscalYears :' + fiscalYears); 	 
	
	// get all the fiscalPeriod Values for the specific months & Year combinations
	List <DIB_Fiscal_Period__c> fiscalPeriods = [
			SELECT d.Start_Date__c, d.Id, d.Fiscal_Year__c, d.Fiscal_Month__c, d.End_Date__c 
			FROM DIB_Fiscal_Period__c d	
			WHERE d.Fiscal_Month__c in: fiscalMonths
			AND d.Fiscal_Year__c in: fiscalYears] ; 
		
	System.debug('#####fiscalPeriods :' + fiscalPeriods); 	
	// FM & FY values must match in order to know when the Fiscal Period starts :  	
	for(DIB_Aggregated_Allocation__c aa : items){
		// This condition will avoid calling this 
		if (aa.Fiscal_Start_Date__c == null){
			// Must have a FM & FY values 			
			if (aa.Fiscal_Month__c == null || aa.Fiscal_Year__c == null){
				continue ; 	
			}
			for (DIB_Fiscal_Period__c ff: fiscalPeriods){				
				if (aa.Fiscal_Month__c == ff.Fiscal_Month__c && aa.Fiscal_Year__c == ff.Fiscal_Year__c){
						//aa.Fiscal_Start_Date__c = ff.Start_Date__c ; 
						DIB_Aggregated_Allocation__c aToUpdate = new DIB_Aggregated_Allocation__c (id=aa.Id, Fiscal_Start_Date__c = ff.Start_Date__c) ;
						itemsToUpdate.add(aToUpdate) ; 
						sUpdateIds.add(aToUpdate.Id);
						break ; 
					}
			}
		}
	}
	
	if(!itemsToUpdate.isEmpty()){
		controllerAllocationAggregated.mAccountAggregationUpsert.put(Userinfo.getUserId(), sUpdateIds);
		try{
			update itemsToUpdate ; 
		}catch(DMLException e){
			items[0].addError('An Error has occured :' + e) ; 
		}		
	}
		
}