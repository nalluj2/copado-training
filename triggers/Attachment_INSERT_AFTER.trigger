/*
    Trigger Name    : Attachment_INSERT_AFTER
    Description     : This trigger is used to create case comment and update Case/Work Order when
                    an attachment is added to Case/Wokr Order
    Author          : Wiro Tech
    Created Date    : 07/08/2013
*/

trigger Attachment_INSERT_AFTER on Attachment (after insert) {    
    
    //If we check the flag in the Custom Settings we skip the execution of this logic
	if(bl_Trigger_Deactivation.isTriggerDeactivated('Attachment_INSERT_AFTER')) return;
    
    Set<Id> setOfCaseIds=new set<Id>();
    Set<Id> setOfWOIds=new set<Id>();
    Set<Id> setOfCPIds=new set<Id>();
    List<Case_Comment__c> lstOfCaseCommentToInsert=new List<Case_Comment__c>();
    Map<Id, List<Attachment>> attachmentByParentId = new Map<Id, List<Attachment>>();
    
    //get object type of form prent id
    for(Attachment objAttahment:Trigger.new){
        if(string.valueof(objAttahment.ParentId.getSObjectType())=='Case'){
            setOfCaseIds.add(objAttahment.ParentId);            
        }
        if(string.valueof(objAttahment.ParentId.getSObjectType())=='Workorder__c'){
            setOfWOIds.add(objAttahment.ParentId);
        }
        if(string.valueof(objAttahment.ParentId.getSObjectType())=='Workorder_Sparepart__c'){
            setOfCPIds.add(objAttahment.ParentId);
        }
        
        //Build map to be used later on for the synchronization process
        List<Attachment> parentAttachments = attachmentByParentId.get(objAttahment.ParentId); 
        
        if(parentAttachments == null){
        	parentAttachments = new List<Attachment>();
        	attachmentByParentId.put(objAttahment.ParentId, parentAttachments);
        }
        
        parentAttachments.add(objAttahment);
    }
    
    List<Attachment> forSync = new List<Attachment>();    
    
    if(setOfCaseIds.size() > 0){
	    
	    //get list of case to update if parent is Case
	    List<Case> lstOfCaseToUpdate=[select id,CaseNumber,Complaint__c,Has_Attachment__c from Case where id in:setOfCaseIds AND RecordType.DeveloperName IN ('New_Case', 'Closed_Case')];
	    
	    //Creating Case Comment and updating case record
	    if(lstOfCaseToUpdate.size()>0){
	    	
	        for(Case objCase:lstOfCaseToUpdate){          
	           Case_Comment__c comment=new Case_Comment__c();
	           comment.Comment_Subject__c='Attachment Uploaded to Case '+objCase.CaseNumber;
	           comment.Comment__c='Attachment Uploaded to Case '+objCase.CaseNumber;
	           comment.Case__c=objCase.id;
	           comment.Complaint__c=objCase.Complaint__c;
	           lstOfCaseCommentToInsert.add(comment);
	           objCase.Has_Attachment__c=true;
	           objCase.Attachment_Last_Uploaded__c=System.now();     
	           
	           forSync.addAll(attachmentByParentId.get(objCase.Id));      
	        }
	        
	        update lstOfCaseToUpdate;
	    } 
    }
    
    if(setOfWOIds.size() > 0){
    
	    //get list of Work order if parent id Work order
	    List<Workorder__c>  lstOfWoToUpdate = [select id,Name,Complaint__c,Has_Attachment__c from Workorder__c where id in:setOfWOIds AND RecordType.DeveloperName IN (
	    	'Eval_Nav_Checkout',
	    	'Eval_Nav_Checkout_Closed',
	    	'Nav_Checkout', 
	    	'Nav_System_Checkout_Closed', 
	    	'Nav_Installation', 
	    	'Nav_System_Installation_Closed',
	    	'Nav_PM', 
	    	'Nav_PM_Closed', 
	    	'O_Arm_System_Checkout', 
	    	'O_Arm_System_Checkout_Closed', 
	    	'O_Arm_System_Installation', 
	    	'O_Arm_System_Install_Closed', 
	    	'O_Arm_PM',
	    	'O_Arm_System_PM_Closed',	    	
	    	'O2_System_Checkout',
	    	'O2_System_Checkout_Closed',
	    	'O2_System_Installation',
	    	'O2_System_Installation_Closed',
	    	'O2_System_PM',
	    	'O2_System_PM_Closed',	    	 
	    	'Other_Site_Visit',  
	    	'Other_Site_Visit_Closed', 
	    	'PoleStar_Checkout', 
	    	'PoleStar_System_Checkout_Closed', 
	    	'PoleStar_System_Install_Closed', 
	    	'PoleStar_System_Installation', 
	    	'PoleStar_PM', 
	    	'PoleStar_System_PM_Closed', 
	    	'Surgery_Coverage', 
	    	'Surgery_Coverage_Closed', 
	    	'Training', 
	    	'Training_Closed',
			'Visualase_Surgery_Coverage',
			'Visualase_Surgery_Coverage_Closed',
			'Visualase_System_Checkout',
			'Visualase_System_Checkout_Closed',
			'Visualase_System_Installation',
			'Visualase_System_Installation_Closed',
			'Visualase_System_PM',
			'Visualase_System_PM_Closed'
	    )];
	    //Creating Case Comment and updating Work Order record
	    if(lstOfWoToUpdate.size()>0){
	    	
	        for(Workorder__c objWo:lstOfWoToUpdate){           
	            if(objWo.Complaint__c!=null){
	                Case_Comment__c comment=new Case_Comment__c();              
	                comment.Comment_Subject__c='Attachment Uploaded to Workorder '+objWo.Name;
	                comment.Comment__c='Attachment Uploaded to Workorder '+objWo.Name;
	                comment.Workorder__c=objWo.id;
	                comment.Complaint__c=objWo.Complaint__c;
	                lstOfCaseCommentToInsert.add(comment);                               
	            }
	            objWo.Has_Attachment__c=true;
	            objWo.Attachment_Last_Uploaded__c=System.now(); 
	            
	            forSync.addAll(attachmentByParentId.get(objWo.Id));
	        } 
	        
	        update lstOfWoToUpdate;
	    }
    }
    
    if(setOfCPIds.size() > 0){
    	
    	//get list of Work order if parnet id Case Part
	    List<Workorder_Sparepart__c> lstOfCpToUpdate=[select id,Name,Complaint__c from Workorder_Sparepart__c where id in:setOfCPIds AND RecordType.DeveloperName IN ('Hardware', 'Inaccuracy', 'Service', 'Software')];
	    //Creating Case Comment and updating Case Part record
	    if(lstOfCpToUpdate.size()>0){
	        for(Workorder_Sparepart__c objCP:lstOfCpToUpdate){           
	            if(objCP.Complaint__c!=null){
	                Case_Comment__c comment=new Case_Comment__c();              
	                comment.Comment_Subject__c='Attachment Uploaded to Workorder Sparepart '+objCP.Name;
	                comment.Comment__c='Attachment Uploaded to Workorder Sparepart '+objCP.Name;
	                comment.Case_Part__c=objCP.id;
	                comment.Complaint__c=objCP.Complaint__c;
	                lstOfCaseCommentToInsert.add(comment);                               
	            }
	            	            
	            forSync.addAll(attachmentByParentId.get(objCP.Id));
	        } 
	    }
    }
       
    insert lstOfCaseCommentToInsert;  
    
    if(forSync.size() > 0) bl_SynchronizationService_Source.createNotificationIfChanged(forSync, null);   	   
}