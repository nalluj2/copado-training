trigger tr_Procedural_Solutions_Kit_Share on Procedural_Solutions_Kit_Share__c (after insert, after delete, after undelete, before delete, before insert, before update) {
	
	if(Trigger.isBefore){    
		
		if(Trigger.isInsert){
			
			if(bl_ProceduralSolutionKit.isDMLAllowed(Trigger.new) == false) return;
			
			GuidUtil.populateMobileId(Trigger.new, 'Mobile_ID__c');
			
			bl_Procedural_Solutions_Kit_Share.populateUniqueKey(Trigger.new);
			
		}else if(Trigger.isUpdate){
			
			if(bl_ProceduralSolutionKit.isDMLAllowed(Trigger.new) == false) return;
			
			GuidUtil.populateMobileId(Trigger.new, 'Mobile_ID__c');
			
			bl_Procedural_Solutions_Kit_Share.populateUniqueKey(Trigger.new);
			
		}else if(Trigger.isDelete){
			
			if(bl_ProceduralSolutionKit.isDMLAllowed(Trigger.old) == false) return;
		}
		
	}else{
		
		if(Trigger.isInsert){
			
			bl_Procedural_Solutions_Kit_Share.manageSharing(Trigger.new, 'INSERT');
			
		}else if(Trigger.isDelete){
			
			bl_Procedural_Solutions_Kit_Share.manageSharing(Trigger.old, 'DELETE');
			
		}else if(Trigger.isUndelete){
			
			if(bl_ProceduralSolutionKit.isDMLAllowed(Trigger.new) == false) return;
			
			bl_Procedural_Solutions_Kit_Share.populateUniqueKey(Trigger.new);
			
			bl_Procedural_Solutions_Kit_Share.manageSharing(Trigger.new, 'INSERT');
		}
	}        
}