/*
 * Work Item Nr		: WI-221
 * Description		: WI-221: Create Mobile Id if null
 * Author        	: Patrick Brinksma
 * Created Date    	: 23-07-2013
 */
trigger tr_AccountPlanStakeholder_BeforeInsertUpdate on Account_Plan_Stakeholder__c (before insert, before update) {
	
	GuidUtil.populateMobileId(Trigger.new, 'Mobile_ID__c');
	
	Set<Id> contactIds = new Set<Id>();
	
	for(Account_Plan_Stakeholder__c apStakeholder : Trigger.new){
				
		if(apStakeholder.Stakeholder_Name__c == null) apStakeholder.Comments__c = null;
		else if(Trigger.isInsert || apStakeholder.Stakeholder_Name__c != Trigger.oldMap.get(apStakeholder.Id).Stakeholder_Name__c) contactIds.add(apStakeholder.Stakeholder_Name__c);	
	}   
	
	if(contactIds.size() > 0){
		
		Map<Id, Contact> contactMap = new Map<Id, Contact>([Select Id, Contact_Comments__c from Contact where Id IN :contactIds]);
		
		for(Account_Plan_Stakeholder__c apStakeholder : Trigger.new){
			
			if(apStakeholder.Stakeholder_Name__c != null){
				
				Contact cnt = contactMap.get(apStakeholder.Stakeholder_Name__c);
				apStakeholder.Comments__c = cnt.Contact_Comments__c;
			}			
		}		
	}
}