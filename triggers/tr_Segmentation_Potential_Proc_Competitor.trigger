trigger tr_Segmentation_Potential_Proc_Competitor on Segmentation_Potential_Proc_Competitor__c (before insert, before update) {
    
    for(Segmentation_Potential_Proc_Competitor__c procCompetitor : Trigger.new){
    	
    	procCompetitor.Unique_Key__c = procCompetitor.Segmentation_Potential_Procedure__c + ':' + procCompetitor.Competitor_picklist__c;
    }
}