trigger tr_Account_Segmentation_Validation on Account_Segmentation_Validation__c (before insert, before update) {
	
	for(Account_Segmentation_Validation__c segmentation : trigger.New){
    	
    	segmentation.Unique_Key__c = segmentation.Account__c ;
    	
    	if(segmentation.Sub_Business_Unit__c != null) segmentation.Unique_Key__c += ':' + segmentation.Sub_Business_Unit__c;
    	else if(segmentation.Business_Unit_Group__c != null) segmentation.Unique_Key__c += ':' + segmentation.Business_Unit_Group__c;
    	
    	segmentation.Unique_Key__c += ':' + segmentation.Fiscal_Year__c;
    }    
}