trigger newUserSBUAfterUpsert on User_Business_Unit__c (after insert,after delete){
map<id,List<User_Business_Unit__c>> mapuseridWithUSBU=new map<id,List<User_Business_Unit__c>>();
set<id> setUserid=new set<id>();
map<id,string> mapUseridwithSBUvalue=new map<id,string>();
List<User_Business_Unit__c> lstallUserSBU=new List<User_Business_Unit__c>();
map<id,user> mapuseridwithUser=new map<id,user>();
List<user> lstUserUpdate=new list<user>();
map<id,user> mapReqUseridwithUser=new map<id,user>();//map need to update
if(Trigger.isInsert )
{
    for(integer i=0;i<trigger.new.size();i++)
    {
        setUserid.add(trigger.new[i].User__c);
    }
    
    List<user> lstUser=[select id,User_Sub_Bus__c from user where id in: setUserid];
    
    for(user u: lstUser)
    {
    mapUseridwithSBUvalue.put(u.id,u.User_Sub_Bus__c);
    mapuseridwithUser.put(u.id,u);
    }
    //lstallUserSBU=[Select user__c,Sub_Business_Unit__r.name,Sub_Business_Unit__r.Business_Unit__r.Name,Sub_Business_Unit__r.Business_Unit__r.Company__r.Company_Code_Text__c from User_Business_Unit__c where id in: trigger.newmap.keyset()];
    for(integer i=0;i<trigger.new.size();i++)
    {
        if(mapuseridwithUser.get(trigger.new[i].user__c).User_Sub_Bus__c!=null)
        {
            if(mapuseridwithUser.get(trigger.new[i].user__c).User_Sub_Bus__c.contains(trigger.new[i].Sub_Business_Unit_name__c))
            {
            }
            else{
            //user u=new user(id=trigger.new[i].User__c,User_Sub_Bus__c=mapUseridwithSBUvalue.get(trigger.new[i].User__c)+';'+trigger.new[i].Sub_Business_Unit_name__c);
            //mapuseridwithUser
            user u=mapuseridwithUser.get(trigger.new[i].user__c);
            u.User_Sub_Bus__c=mapuseridwithUser.get(trigger.new[i].user__c).User_Sub_Bus__c+';'+trigger.new[i].Sub_Business_Unit_name__c;
            mapuseridwithUser.put(trigger.new[i].user__c,u);
            mapReqUseridwithUser.put(trigger.new[i].user__c,u);
            }
        }
        else
        {
         //user u=new user(id=trigger.new[i].user__c,User_Sub_Bus__c=trigger.new[i].Sub_Business_Unit_name__c);
            user u=mapuseridwithUser.get(trigger.new[i].user__c);
            u.User_Sub_Bus__c=trigger.new[i].Sub_Business_Unit_name__c;
            mapuseridwithUser.put(trigger.new[i].user__c,u);
            mapReqUseridwithUser.put(trigger.new[i].user__c,u);
         }  
        if(trigger.new[i].checkSBUAutoFlagDibAccount__c=='true')
        {
            user u=mapuseridwithUser.get(trigger.new[i].user__c);
            u.Autoflag_DiB_Account_fields__c=true;
            mapuseridwithUser.put(trigger.new[i].user__c,u);
            mapReqUseridwithUser.put(trigger.new[i].user__c,u);
        }
         
    }
    
    for(id userid: mapReqUseridwithUser.keyset())
    {
        lstUserUpdate.add(mapReqUseridwithUser.get(userid));
    }    
    
    
    if(lstUserUpdate.size()>0)
    {
    update lstUserUpdate; 
    }   
}
if(Trigger.isDelete)
{
        for(integer i=0;i<trigger.old.size();i++)
        {
            setUserid.add(trigger.old[i].User__c);
        }
        List<user> lstUser=[select id,User_Sub_Bus__c from user where id in: setUserid];
        for(user u: lstUser)
        {
            mapUseridwithSBUvalue.put(u.id,u.User_Sub_Bus__c);
            mapuseridwithUser.put(u.id,u);
        }
//lstallUserSBU=[Select user__c,Sub_Business_Unit__r.name,Sub_Business_Unit__r.Business_Unit__r.Name,Sub_Business_Unit__r.Business_Unit__r.Company__r.Company_Code_Text__c from User_Business_Unit__c where id in: trigger.newmap.keyset()];
        for(integer i=0;i<trigger.old.size();i++)
        {
            if(mapuseridwithUser.get(trigger.old[i].user__c).User_Sub_Bus__c!=null)
            {
                if(mapuseridwithUser.get(trigger.old[i].user__c).User_Sub_Bus__c.contains(trigger.old[i].Sub_Business_Unit_name__c))
                {
                    user u=mapuseridwithUser.get(trigger.old[i].user__c);
                    
                    if(u.User_Sub_Bus__c!=null && u.User_Sub_Bus__c.contains(trigger.old[i].Sub_Business_Unit_name__c))
                    //u.User_Sub_Bus__c=mapuseridwithUser.get(trigger.new[i].user__c).User_Sub_Bus__c+';'+trigger.new[i].Sub_Business_Unit_name__c;
                        if(u.User_Sub_Bus__c.contains(trigger.old[i].Sub_Business_Unit_name__c+';'))
                        u.User_Sub_Bus__c=u.User_Sub_Bus__c.replace(trigger.old[i].Sub_Business_Unit_name__c+';','');
                        else
                        u.User_Sub_Bus__c=u.User_Sub_Bus__c.replace(trigger.old[i].Sub_Business_Unit_name__c,'');
                        
                    mapuseridwithUser.put(trigger.old[i].user__c,u);
                    mapReqUseridwithUser.put(trigger.old[i].user__c,u);
                }
                else{            
                }
            }
             
        }
    for(id userid: mapReqUseridwithUser.keyset())
    {
        lstUserUpdate.add(mapReqUseridwithUser.get(userid));
    }    
        
    if(lstUserUpdate.size()>0)
    update lstUserUpdate; 
    }

}