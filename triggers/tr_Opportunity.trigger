//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 10-05-2016
//  Description      : APEX Trigger on Opportunity 
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
trigger tr_Opportunity on Opportunity (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
	
	if(bl_Trigger_Deactivation.isTriggerDeactivated('tr_Opportunity')) return;
	
	if (Trigger.isBefore){

		if (Trigger.isInsert){
			
			bl_Opportunity_Trigger.CAN_DiB_DibType_PumpGroup_Validation(Trigger.new, Trigger.OldMap);

			bl_Opportunity_Trigger.populateCountryFromAccount(Trigger.New);
			
			bl_Opportunity_Trigger.updateOwnerAndAssignedTo(Trigger.New);
			
			//bl_Opportunity_Trigger.EUR_DiB_SetOwner(Trigger.New);
			
			bl_Opportunity_Trigger.CAN_DiB_PopulateEmailFromAccount(Trigger.New);
			
			bl_Opportunity_Trigger.CAN_DiB_SetCloseDateWhenReimbursementVerification(Trigger.New, Trigger.OldMap);
			
			bl_Opportunity_Trigger.CAN_DiB_SetName(Trigger.New);
			
			bl_Opportunity_Trigger.CAN_DIB_SetRating(Trigger.New);
			
			bl_Opportunity_Trigger.updateNextActionTakerChanged(Trigger.New, Trigger.OldMap);

			bl_Opportunity_Trigger.CAN_SetName(Trigger.New);
			
			bl_Opportunity_Trigger.tenderSetSalesProcessMapping(Trigger.New, Trigger.OldMap);
			
			bl_Opportunity_Trigger.EUR_Tender_CopyAmount(Trigger.New, Trigger.OldMap);
			
			bl_Opportunity_Trigger.MITG_setName(Trigger.New);
			
			bl_Opportunity_Trigger.populateRevenueStartDate(Trigger.New);
			
			bl_Opportunity_Trigger.setEnrolledDateWhenReimbursementChecked(Trigger.New, Trigger.OldMap);
			
			bl_Opportunity_Trigger.validateAccountInUserTerritory(Trigger.New, Trigger.OldMap);

			bl_Opportunity_Trigger.assignTerritory_MITG_Insert(Trigger.New);

			bl_Opportunity_Trigger_Tender.copyExpectedEffectiveDate_To_CloseDate(Trigger.new);

			bl_Opportunity_Trigger.updateStageCompletion(Trigger.New);
			
			bl_Opportunity_Trigger.setPriceBook(Trigger.new);
			
			bl_Opportunity_Trigger.updateAccountContactId(Trigger.New, Trigger.oldMap);
			
			bl_Opportunity_Trigger.verifyAndSetPhysicianAccountId(Trigger.New, Trigger.oldMap);
			
			bl_Opportunity_Trigger.cvgProject_populateMobileId(Trigger.new);
			
			bl_Opportunity_Trigger.rtg_populateMobileId(Trigger.new);
			
			bl_Opportunity_Trigger.mapSAPStage(Trigger.New, Trigger.oldMap);
			
			bl_Opportunity_Trigger.setBusinessUnit(Trigger.new);
			
			bl_Opportunity_Trigger.populateOwnerManagerEmail(Trigger.new);
			
			bl_Opportunity_Trigger.setClosedDateOnLostCancelled(Trigger.new, Trigger.oldMap);
			
			bl_Opportunity_Trigger.setBUG_BU_SBU(Trigger.new, Trigger.oldMap);

			bl_Opportunity_Trigger_Tender.copyBUInfoOnPrevOpportunity(Trigger.new);

			bl_Opportunity_Trigger_Tender.copyDataFromPreviousOpportunity(Trigger.new);
			
			bl_Opportunity_Trigger_Tender.copyOppCountryToText(Trigger.new);

			bl_Opportunity_Trigger.updateFiscalPeriod(Trigger.New, Trigger.oldMap);

			bl_Opportunity_Trigger.populateBusinessUnitGroupText(Trigger.New, Trigger.oldMap);

			bl_Opportunity_Trigger.IHS_CalculateContractEndDate(Trigger.New, Trigger.oldMap);
			
			bl_Opportunity_Trigger.setPhysicianAccountProvince(Trigger.New, Trigger.oldMap);

		}else if (Trigger.isUpdate){

			bl_Opportunity_Trigger.approvalFieldRequired(Trigger.new, Trigger.OldMap);

			bl_Opportunity_Trigger.CAN_DiB_DibType_PumpGroup_Validation(Trigger.new, Trigger.OldMap);

			bl_Opportunity_Trigger.rollbackCloseDate(Trigger.New, Trigger.OldMap);
			
			bl_Opportunity_Trigger.populateCountryFromAccount(Trigger.New);
									
			bl_Opportunity_Trigger.CAN_DiB_PopulateEmailFromAccount(Trigger.New);
			
			bl_Opportunity_Trigger.CAN_DiB_SetCloseDateWhenReimbursementVerification(Trigger.New, Trigger.OldMap);
			
			bl_Opportunity_Trigger.CAN_DiB_SetName(Trigger.New);
			
			bl_Opportunity_Trigger.CAN_DIB_SetRating(Trigger.New);

			bl_Opportunity_Trigger.CAN_DiB_SetCloseDate(Trigger.New, Trigger.OldMap);

			bl_Opportunity_Trigger.CAN_DIB_Set_WCPPumpReturnDueDate(Trigger.new, Trigger.oldMap);
			
			bl_Opportunity_Trigger.updateNextActionTakerChanged(Trigger.New, Trigger.OldMap);

			bl_Opportunity_Trigger.tenderSetSalesProcessMapping(Trigger.New, Trigger.OldMap);
			
			bl_Opportunity_Trigger.MITG_ClearClosedDateWhenReopen(Trigger.New, Trigger.OldMap);
			
			bl_Opportunity_Trigger.cleanReviewedFlagOnStageChange(Trigger.New, Trigger.OldMap);
			
			bl_Opportunity_Trigger.EUR_Tender_CopyAmount(Trigger.New, Trigger.OldMap);
						
			bl_Opportunity_Trigger.populateRevenueStartDate(Trigger.New);
			
			bl_Opportunity_Trigger.MITG_setClosedDate(Trigger.New, Trigger.OldMap);
			
			bl_Opportunity_Trigger.pushCounter(Trigger.New, Trigger.OldMap);
			
			bl_Opportunity_Trigger.setEnrolledDateWhenReimbursementChecked(Trigger.New, Trigger.OldMap);

			bl_Opportunity_Trigger.validateAccountInUserTerritory(Trigger.New, Trigger.OldMap);

			bl_Opportunity_Trigger.assignTerritory_MITG_Update(Trigger.New, Trigger.OldMap);

			bl_Opportunity_Trigger_Tender.copyExpectedEffectiveDate_To_CloseDate(Trigger.new);

			bl_Opportunity_Trigger.updatePreviousStage(Trigger.New, Trigger.oldMap);

			bl_Opportunity_Trigger.updateStageCompletion(Trigger.New);
			
			bl_Opportunity_Trigger.setStageChangeDate(Trigger.New, Trigger.oldMap);
			
			bl_Opportunity_Trigger.updateAccountContactId(Trigger.New, Trigger.oldMap);
			
			bl_Opportunity_Trigger.verifyAndSetPhysicianAccountId(Trigger.New, Trigger.oldMap);
			
			bl_Opportunity_Trigger.cvgProject_populateMobileId(Trigger.new);
			
			bl_Opportunity_Trigger.rtg_populateMobileId(Trigger.new);
			
			bl_Opportunity_Trigger.mapSAPStage(Trigger.New, Trigger.oldMap);
			
			bl_Opportunity_Trigger.setBusinessUnit(Trigger.new);
			
			bl_Opportunity_Trigger.populateOwnerManagerEmail(Trigger.new);
			
			bl_Opportunity_Trigger.setClosedDateOnLostCancelled(Trigger.new, Trigger.oldMap);

			bl_Opportunity_Trigger.setBUG_BU_SBU(Trigger.new, Trigger.oldMap);
			
			bl_Opportunity_Trigger_Tender.updateOnPrevOpportunityChange(Trigger.new, Trigger.oldMap);
			
			bl_Opportunity_Trigger_Tender.copyOppCountryToText(Trigger.new);
			
			bl_Opportunity_Trigger.IHS_CloneOpportunity(Trigger.New, Trigger.oldMap);
			
			bl_Opportunity_Trigger.updateFiscalPeriod(Trigger.New, Trigger.oldMap);

			bl_Opportunity_Trigger.populateBusinessUnitGroupText(Trigger.New, Trigger.oldMap);

			bl_Opportunity_Trigger.IHS_CalculateContractEndDate(Trigger.New, Trigger.oldMap);
			
			bl_Opportunity_Trigger.setPhysicianAccountProvince(Trigger.New, Trigger.oldMap);

		}else if(Trigger.isDelete){
			
			bl_Opportunity_Trigger.preventDeletionSAPOpp(Trigger.old);
		}

	}else if (Trigger.isAfter){

		if (Trigger.isInsert){
            			
			bl_Opportunity_Trigger.checkMITGMasterShipToAccount(Trigger.New);
			
			bl_Opportunity_Trigger.createInsuranceVerifications(trigger.New);
			
			bl_Opportunity_Trigger.sendOpportunityToSAP(trigger.New);
			
			bl_Opportunity_Trigger.ANZ_CreatePendingTasks(Trigger.New, Trigger.oldMap);
			
			bl_Opportunity_Trigger.ANZ_UpdateHealthInsurer(Trigger.New);
			
			bl_Opportunity_Trigger.setOppBundleBusinessUnits(Trigger.New, Trigger.oldMap);
			
			bl_Opportunity_Trigger.managePrimaryCompetitors(Trigger.New, Trigger.oldMap);
			
			//bl_Opportunity_Trigger.validateAssetAccountId(Trigger.New);
			
			bl_Opportunity_Trigger.updatePhysicianHealthInsurer(Trigger.New);

			//bl_Opportunity_Trigger.populateOppTeam_AccountTerritory(Trigger.new);

			bl_Opportunity_Trigger.addOppOwnerToOppTeam(Trigger.New, Trigger.oldMap);
			
			bl_Opportunity_Trigger_Tender.deepCloneTenderOpportunity(Trigger.New);
			
			bl_Opportunity_Trigger.createDefaultOpportunityAccounts(Trigger.New, null);

			bl_Opportunity_Trigger.IHS_UpdateClientStatusOnPreviousOpp(Trigger.new, null);

			bl_Opportunity_Trigger.IHS_CreateTask(Trigger.new, Trigger.oldMap);
			
			bl_Opportunity_Trigger.validateAgentBusinessUnits(Trigger.new);
			
			bl_Opportunity_Trigger.syncAPTM_OTM(Trigger.new, Trigger.oldMap);
			
            bl_Opportunity_Trigger.acceptSuggestedActivity(Trigger.new);

		}else if (Trigger.isUpdate){

			bl_Opportunity_Trigger.checkMITGMasterShipToAccount(Trigger.New);
			
			bl_Opportunity_Trigger.sendOpportunityToSAP(trigger.New);
			
			bl_Opportunity_Trigger.ANZ_CreatePendingTasks(Trigger.New, Trigger.oldMap);
			
			bl_Opportunity_Trigger.ANZ_UpdateHealthInsurer(Trigger.New);
			
			bl_Opportunity_Trigger.setOppBundleBusinessUnits(Trigger.New, Trigger.oldMap);
			
			bl_Opportunity_Trigger.managePrimaryCompetitors(Trigger.New, Trigger.oldMap);
			
			bl_Opportunity_Trigger.updateLineItemScheduleDates(Trigger.New, Trigger.oldMap);
			
			//bl_Opportunity_Trigger.validateAssetAccountId(Trigger.New);
			
			bl_Opportunity_Trigger.updatePhysicianHealthInsurer(Trigger.New);
			
			bl_Opportunity_Trigger.addOppOwnerToOppTeam(Trigger.New, Trigger.oldMap);
			
			bl_Opportunity_Trigger.setRTGRevenueSchedule(Trigger.New, Trigger.oldMap);
			
			bl_Opportunity_Trigger_Tender.updateNextOpportuniesOnChange(Trigger.New, Trigger.oldMap);
			
			bl_Opportunity_Trigger.createDefaultOpportunityAccounts(Trigger.New, Trigger.oldMap);						

			bl_Opportunity_Trigger.IHS_UpdateClientStatusOnPreviousOpp(Trigger.new, Trigger.oldMap);

			bl_Opportunity_Trigger.IHS_CreateTask(Trigger.new, Trigger.oldMap);
			
			bl_Opportunity_Trigger.validateAgentBusinessUnits(Trigger.new);
						
			bl_Opportunity_Trigger.CAN_DiB_ValidatePumpGroup(Trigger.newMap);
            
			bl_Opportunity_Trigger.syncAPTM_OTM(Trigger.new, Trigger.oldMap);

		}else if (Trigger.isDelete){
			
			bl_Opportunity_Trigger.setOppBundleBusinessUnits(Trigger.Old, Trigger.oldMap);
			
			// bl_Opportunity_Trigger.validatDelStrategicAccountMng(Trigger.Old); Inactive record type
			
		}else if(Trigger.isUndelete){
			
			bl_Opportunity_Trigger.validateAgentBusinessUnits(Trigger.new);
		}
	}

}
//--------------------------------------------------------------------------------------------------------------------