trigger tr_Product_Procedure_Availability on Product_Procedure_Availability__c (before insert, before update) {
        
    for(Product_Procedure_Availability__c prodProcedure : Trigger.new){
    
        prodProcedure.Unique_Key__c = prodProcedure.Product__c + ':' + prodProcedure.Procedure__c;
    }
}