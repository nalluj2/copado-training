/**
 *      Created Date : 20121108
 *      Description : This class is used to link the uploaded data to the lookup fields provided
 * 
 *      Author = Rudy De Coninck
 */

trigger tr_AccountTerritoryChangeBefore on Account_Territory_Change__c (before insert, before update) {
					
	bl_Territory.linkRelationshipDataToAccountTerritoryChange(Trigger.new);	
}