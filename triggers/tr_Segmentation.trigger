trigger tr_Segmentation on Segmentation__c (after delete, after insert, after undelete, after update) {
	
	if(Trigger.isBefore){
				
	}else{
		
		if(Trigger.isInsert){
			
			bl_Segmentation.endPreviousSegmentation(Trigger.new);
			
			bl_Segmentation.validateSegmentOverlap(Trigger.new);
			
			bl_Segmentation.updateAccountSegment(Trigger.new);			
			
		}else if(Trigger.isUpdate){
			
			bl_Segmentation.validateSegmentOverlap(Trigger.new);
			
			bl_Segmentation.updateAccountSegment(Trigger.new);		
			
		}else if(Trigger.isDelete){
			
			bl_Segmentation.clearAccountSegmentValue(Trigger.old);
			
		}else if(Trigger.isUndelete){
			
			bl_Segmentation.validateSegmentOverlap(Trigger.new);
						
			bl_Segmentation.updateAccountSegment(Trigger.new);		
		}		
		
	}    
}