trigger tr_Task on Task (before insert, before update, after insert, after update) {
    
	if (Trigger.isBefore){

		if (Trigger.isInsert){

			bl_Task_Trigger.setTaskAsPublic(Trigger.New);

            bl_Activity.validateActiveAccount(Trigger.New, 'Task');
			
			bl_Task_Trigger.DIBAccountPlanObjective_Mandatory(Trigger.new);

			bl_Task_Trigger.CVG_MET_PopulateAccount(Trigger.new, Trigger.oldMap);
			
			bl_Task_Trigger.CVG_MET_PopulateShortDescription(Trigger.new, Trigger.oldMap);

		}else if(Trigger.isUpdate){
			
			bl_Task_Trigger.DIBAccountPlanObjective_Mandatory(Trigger.new);

			bl_Task_Trigger.CVG_MET_PopulateAccount(Trigger.new, Trigger.oldMap);

			bl_Task_Trigger.CVG_MET_PopulateShortDescription(Trigger.new, Trigger.oldMap);

		}

	}else if (Trigger.isAfter){
        
	    if (Trigger.isInsert){
	    
		    bl_Task.sendCVGTaskEmail(trigger.New, trigger.oldMap);

			bl_Task_Trigger.CVG_MET_SendNotificationEmail(Trigger.new, Trigger.oldMap);

	    }else if (Trigger.isUpdate){
	    
		    bl_Task.sendCVGTaskEmail(trigger.New, trigger.oldMap);

			bl_Task_Trigger.CVG_MET_SendNotificationEmail(Trigger.new, Trigger.oldMap);

			bl_Task_Trigger.CVG_MET_UpdateCampaignMember(Trigger.new, Trigger.oldMap);

	    }

    }

}