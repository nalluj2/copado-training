/*
 * Work Item Nr		: WI-221
 * Description		: WI-221: Create Mobile Id if null
 * Author        	: Patrick Brinksma
 * Created Date    	: 23-07-2013
 */
trigger tr_AccountPlan2_BeforeInsertUpdate on Account_Plan_2__c (before insert, before update) {
	// Create Mobile Id for inserted and updated records
	GuidUtil.populateMobileId(Trigger.new, 'Mobile_ID__c');
	
	// Christophe Saenen	20160426	CR-11736
	if(Trigger.isInsert){
		
		bl_AccountPlanning.setBUGAccountPlan(Trigger.New);		
		bl_AccountPlanning.setBUGAccountPlan_BU(Trigger.New);		

	}
	
	bl_AccountPlanning.updateFields(Trigger.new); 
}