//------------------------------------------------------------------------------------------------------------
//	Author		:	Bart Caelen
//	Created		:	2014-04-25
//	Description	:   APEX Trigger on Compaign
//                  When a Campaign is created, additional Campaign Member Statuses are created based on the Custom Setting CampaignMemberStatus__c
//------------------------------------------------------------------------------------------------------------------------------
trigger tr_Campaign_AddCampaignMemberStatus on Campaign (after insert) {
	
	if (trigger.isAfter){

		if (trigger.isInsert){

			Map<String, CampaignMemberStatus__c> mapCampaignMemberStatus = CampaignMemberStatus__c.getAll();

			Map<String, List<CampaignMemberStatus__c>> mapField_CampaignMemberStatus = new Map<String, List<CampaignMemberStatus__c>>();

			for (CampaignMemberStatus__c oCampaignMemberStatus : mapCampaignMemberStatus.values()){
				if (clsUtil.isNull(oCampaignMemberStatus.Filter_Field__c, '') != ''){
					List<CampaignMemberStatus__c> lstCampaignMemberStatus = new List<CampaignMemberStatus__c>();
					if (mapField_CampaignMemberStatus.containsKey(oCampaignMemberStatus.Filter_Field__c)){
						lstCampaignMemberStatus = mapField_CampaignMemberStatus.get(oCampaignMemberStatus.Filter_Field__c);
					}
					lstCampaignMemberStatus.add(oCampaignMemberStatus);
					mapField_CampaignMemberStatus.put(oCampaignMemberStatus.Filter_Field__c, lstCampaignMemberStatus);
				}
			}

			List<CampaignMemberStatus> lstCampaignMemberStatus_Insert	= new List<CampaignMemberStatus>();
			Set<String> setPreventDuplicates = new Set<String>();
			// Loop the processing Campaigns
			for (Campaign oCampaign : trigger.new){
				
				if(oCampaign.isClone()) continue;
				
				// Loop through the collected fields that need to be checked to determine if we need to create an additional CampaignMemberStatus
				for (String tField : mapField_CampaignMemberStatus.keySet()){

					// Loop through the defined CampaignMemberStatus__c records in the Custom Setting to validate if the Filter Field and Filter Value match
					for (CampaignMemberStatus__c oCMS : mapField_CampaignMemberStatus.get(tField)){

						// Get the Filter Value Text values which can be an array (separated by ',')
						//	We put the different Filter Value Text values in a SET to prevent duplicates
						//	If there is no Filter Value Text value specified, we skip this
						String tFilterValueText_All = clsUtil.isNull(oCMS.Filter_Value_Text__c, '');
						Set<String> setFilterValueText = new Set<String>();
						if (tFilterValueText_All != ''){
							List<String> lstFilterValueText = tFilterValueText_All.split(',');
							setFilterValueText.addAll(lstFilterValueText);

							for (String tFilterValueText : setFilterValueText){

								if ( oCampaign.get(tField) == tFilterValueText ){
									String tUnique = oCampaign.Id + '_' + oCMS.Member_Status_Label__c;
									if (!setPreventDuplicates.contains(tUnique)){
										// Create Additional Campaign Member Status for this Campaign
										CampaignMemberStatus oCMS_New 	= new CampaignMemberStatus();
											oCMS_New.CampaignId			= oCampaign.Id;
											oCMS_New.SortOrder			= oCMS.SortOrder__c.intValue();
											oCMS_New.Label				= oCMS.Member_Status_Label__c;
											oCMS_New.IsDefault			= oCMS.IsDefault__c;
											oCMS_New.HasResponded		= oCMS.HasResponded__c;
										lstCampaignMemberStatus_Insert.add(oCMS_New);

										setPreventDuplicates.add(tUnique);
									}
								}
							}
						}
					}
				}
			}

			if (lstCampaignMemberStatus_Insert.size()>0){
				
//				insert lstCampaignMemberStatus_Insert;
				List<Database.SaveResult> lstSaveResult_Insert = Database.insert(lstCampaignMemberStatus_Insert, false);

				Integer iCounter = 0;
				for (Database.SaveResult oSaveResult : lstSaveResult_Insert){
					
					String tMessage = '';
					if (oSaveResult.isSuccess()){				

						tMessage = 'Added CampaignMemberStatus : ' + lstCampaignMemberStatus_Insert[iCounter].Label;

					}else{
						
						tMessage = 'Error while adding CampaignMemberStatus "' + lstCampaignMemberStatus_Insert[iCounter].Label + '" : ';
						for (Database.Error oError : oSaveResult.getErrors()) tMessage += oError.getMessage()+'; ';

					}

					System.debug(tMessage);
					
					iCounter++;
				}
			}			
		}
	}
}