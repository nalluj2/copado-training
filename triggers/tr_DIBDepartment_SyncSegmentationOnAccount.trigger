//------------------------------------------------------------------------------------------------------------------------------------------------------------
//  Author      : Bart Caelen
//  Date        : 2015-10-08
//  Description : CR-9628
//                  This trigger will keept the DIB Segmentation Field on Account in sync with the data that is specified on DIB_Department__c.
//  Version     :  1.0
//------------------------------------------------------------------------------------------------------------------------------------------------------------
trigger tr_DIBDepartment_SyncSegmentationOnAccount on DIB_Department__c (after insert, after update, after delete) {
	
	if(bl_Account_Trigger.isInAccountMerge == true) return;
	
    if (trigger.isAfter){

        //------------------------------------------------------------------------------
        // Prepare Mapping Data
        //------------------------------------------------------------------------------
        Map<Id, Department_Master__c> mapDepartmentMaster = new Map<Id, Department_Master__c>([SELECT Id, Name FROM Department_Master__c LIMIT 1000]);

        Map<String, Map<String, String>> mapDepartmentName_DepartmentFieldName_AccountFieldName = new Map<String, Map<String,String>>();
            Map<String, String> mapDepartmentFieldName_AccountFieldName = new Map<String, String>();
                mapDepartmentFieldName_AccountFieldName.put('DiB_Segment__c', 'Pediatric_Segment__c');
                mapDepartmentFieldName_AccountFieldName.put('DiB_Segment_Explanation__c', 'Pediatric_Segment_Explanation__c');
            mapDepartmentName_DepartmentFieldName_AccountFieldName.put('Pediatric', mapDepartmentFieldName_AccountFieldName);
            
            mapDepartmentFieldName_AccountFieldName = new Map<String, String>();
                mapDepartmentFieldName_AccountFieldName.put('DiB_Segment__c', 'Adult_Pediatric_Segment__c');
                mapDepartmentFieldName_AccountFieldName.put('DiB_Segment_Explanation__c', 'Adult_Pediatric_Segment_Explanation__c');
            mapDepartmentName_DepartmentFieldName_AccountFieldName.put('Adult/Pediatric', mapDepartmentFieldName_AccountFieldName);

            mapDepartmentFieldName_AccountFieldName = new Map<String, String>();
                mapDepartmentFieldName_AccountFieldName.put('DiB_Segment__c', 'Other_Segment__c');
                mapDepartmentFieldName_AccountFieldName.put('DiB_Segment_Explanation__c', 'Other_Segment_Explanation__c');
            mapDepartmentName_DepartmentFieldName_AccountFieldName.put('Other', mapDepartmentFieldName_AccountFieldName);

            mapDepartmentFieldName_AccountFieldName = new Map<String, String>();
                mapDepartmentFieldName_AccountFieldName.put('DiB_Segment__c', 'Adult_Type_1_Segment__c');
                mapDepartmentFieldName_AccountFieldName.put('DiB_Segment_Explanation__c', 'Adult_Type_1_Segment_Explanation__c');
            mapDepartmentName_DepartmentFieldName_AccountFieldName.put('Adult Type 1', mapDepartmentFieldName_AccountFieldName);

            mapDepartmentFieldName_AccountFieldName = new Map<String, String>();
                mapDepartmentFieldName_AccountFieldName.put('DiB_Segment__c', 'Adult_Type_2_Segment__c');
                mapDepartmentFieldName_AccountFieldName.put('DiB_Segment_Explanation__c', 'Adult_Type_2_Segment_Explanation__c');
            mapDepartmentName_DepartmentFieldName_AccountFieldName.put('Adult Type 2', mapDepartmentFieldName_AccountFieldName);
        //------------------------------------------------------------------------------

        List<sObject> lstAccount_Update = new List<sObject>();
        Map<Id, sObject> mapAccount_Update = new Map<Id, sObject>();

        //------------------------------------------------------------------------------
        // AFTER INSERT
        //------------------------------------------------------------------------------
        if (trigger.isInsert){
            
            for (DIB_Department__c oDIBDepartment : trigger.new){
                if (mapDepartmentMaster.containsKey(oDIBDepartment.Department_ID__c)){
                    String tDepartmentName = mapDepartmentMaster.get(oDIBDepartment.Department_ID__c).Name;

                    if (mapDepartmentName_DepartmentFieldName_AccountFieldName.containsKey(tDepartmentName)){
                        mapDepartmentFieldName_AccountFieldName = mapDepartmentName_DepartmentFieldName_AccountFieldName.get(tDepartmentName);
                        for (String tDepartmentFieldName : mapDepartmentFieldName_AccountFieldName.keySet()){
                            String tAccountFieldName = mapDepartmentFieldName_AccountFieldName.get(tDepartmentFieldName);
                            sObject oAccount = new Account(Id = oDIBDepartment.Account__c);       
                            if (mapAccount_Update.containsKey(oDIBDepartment.Account__c)){
                                oAccount = mapAccount_Update.get(oDIBDepartment.Account__c);
                            }
                                oAccount.put(tAccountFieldName, oDIBDepartment.get(tDepartmentFieldName));
                            mapAccount_Update.put(oAccount.Id, oAccount);
                        }
                    }
                }
            }
        //------------------------------------------------------------------------------


        //------------------------------------------------------------------------------
        // AFTER UPDATE
        //------------------------------------------------------------------------------
        }else if (trigger.isUpdate){

            for (DIB_Department__c oDIBDepartment : trigger.new){

                Boolean bUpdated = false;

                DIB_Department__c oDIBDepartment_Old = trigger.oldMap.get(oDIBDepartment.Id);

                sObject oAccount = new Account(Id = oDIBDepartment.Account__c);       
                if (mapAccount_Update.containsKey(oDIBDepartment.Account__c)){
                    oAccount = mapAccount_Update.get(oDIBDepartment.Account__c);
                }

                if (oDIBDepartment.Department_ID__c != oDIBDepartment_Old.Department_ID__c){

                    // Clear the Account Segmantation Field which is related to the Old Segmentation value on DIB_Department
                    if (mapDepartmentMaster.containsKey(oDIBDepartment_Old.Department_ID__c)){
                        String tDepartmentName = mapDepartmentMaster.get(oDIBDepartment_Old.Department_ID__c).Name;

                        if (mapDepartmentName_DepartmentFieldName_AccountFieldName.containsKey(tDepartmentName)){
                            mapDepartmentFieldName_AccountFieldName = mapDepartmentName_DepartmentFieldName_AccountFieldName.get(tDepartmentName);
                            for (String tDepartmentFieldName : mapDepartmentFieldName_AccountFieldName.keySet()){
                                String tAccountFieldName = mapDepartmentFieldName_AccountFieldName.get(tDepartmentFieldName);
                                oAccount = new Account(Id = oDIBDepartment.Account__c);       
                                if (mapAccount_Update.containsKey(oDIBDepartment.Account__c)){
                                    oAccount = mapAccount_Update.get(oDIBDepartment.Account__c);
                                }
                                    oAccount.put(tAccountFieldName, null);
                                mapAccount_Update.put(oAccount.Id, oAccount);
                                bUpdated = true;
                            }
                        }
                    }

                    // Set the Account Segmantation Field which is related to the New Segmentation value on DIB_Department
                    if (mapDepartmentMaster.containsKey(oDIBDepartment.Department_ID__c)){
                        String tDepartmentName = mapDepartmentMaster.get(oDIBDepartment.Department_ID__c).Name;

                        if (mapDepartmentName_DepartmentFieldName_AccountFieldName.containsKey(tDepartmentName)){
                            mapDepartmentFieldName_AccountFieldName = mapDepartmentName_DepartmentFieldName_AccountFieldName.get(tDepartmentName);
                            for (String tDepartmentFieldName : mapDepartmentFieldName_AccountFieldName.keySet()){
                                String tAccountFieldName = mapDepartmentFieldName_AccountFieldName.get(tDepartmentFieldName);
                                oAccount = new Account(Id = oDIBDepartment.Account__c);       
                                if (mapAccount_Update.containsKey(oDIBDepartment.Account__c)){
                                    oAccount = mapAccount_Update.get(oDIBDepartment.Account__c);
                                }
                                    oAccount.put(tAccountFieldName, oDIBDepartment.get(tDepartmentFieldName));
                                mapAccount_Update.put(oAccount.Id, oAccount);
                                bUpdated = true;
                            }
                        }
                    }
                }

                if (bUpdated == false){
                    if (oDIBDepartment.DiB_Segment__c != oDIBDepartment_Old.DiB_Segment__c){
                        // Set the Account Segmantation Field which is related to the New Segmentation value on DIB_Department
                        if (mapDepartmentMaster.containsKey(oDIBDepartment.Department_ID__c)){
                            String tDepartmentName = mapDepartmentMaster.get(oDIBDepartment.Department_ID__c).Name;

                            if (mapDepartmentName_DepartmentFieldName_AccountFieldName.containsKey(tDepartmentName)){
                                mapDepartmentFieldName_AccountFieldName = mapDepartmentName_DepartmentFieldName_AccountFieldName.get(tDepartmentName);
                                for (String tDepartmentFieldName : mapDepartmentFieldName_AccountFieldName.keySet()){
                                    String tAccountFieldName = mapDepartmentFieldName_AccountFieldName.get(tDepartmentFieldName);
                                    oAccount = new Account(Id = oDIBDepartment.Account__c);       
                                    if (mapAccount_Update.containsKey(oDIBDepartment.Account__c)){
                                        oAccount = mapAccount_Update.get(oDIBDepartment.Account__c);
                                    }
                                        oAccount.put(tAccountFieldName, oDIBDepartment.get(tDepartmentFieldName));
                                    mapAccount_Update.put(oAccount.Id, oAccount);
                                    bUpdated = true;
                                }
                            }
                        }
                    }
                }

                if (bUpdated){
                    mapAccount_Update.put(oAccount.Id, oAccount);                    
                }
            }
        //------------------------------------------------------------------------------


        //------------------------------------------------------------------------------
        // AFTER DELETE
        //------------------------------------------------------------------------------
        }else if (trigger.isDelete){

            for (DIB_Department__c oDIBDepartment : trigger.old){

                // Clear the Account Segmantation Field which is related to the Old Segmentation value on DIB_Department
                if (mapDepartmentMaster.containsKey(oDIBDepartment.Department_ID__c)){
                    String tDepartmentName = mapDepartmentMaster.get(oDIBDepartment.Department_ID__c).Name;

                    if (mapDepartmentName_DepartmentFieldName_AccountFieldName.containsKey(tDepartmentName)){
                        mapDepartmentFieldName_AccountFieldName = mapDepartmentName_DepartmentFieldName_AccountFieldName.get(tDepartmentName);
                        for (String tDepartmentFieldName : mapDepartmentFieldName_AccountFieldName.keySet()){
                            String tAccountFieldName = mapDepartmentFieldName_AccountFieldName.get(tDepartmentFieldName);
                            sObject oAccount = new Account(Id = oDIBDepartment.Account__c);       
                            if (mapAccount_Update.containsKey(oDIBDepartment.Account__c)){
                                oAccount = mapAccount_Update.get(oDIBDepartment.Account__c);
                            }
                                oAccount.put(tAccountFieldName, null);
                            mapAccount_Update.put(oAccount.Id, oAccount);
                        }
                    }
                }
            }
        }
        //------------------------------------------------------------------------------

        if (mapAccount_Update.size() > 0){
            update mapAccount_Update.values();
        }

    }
}
//------------------------------------------------------------------------------------------------------------------------------------------------------------