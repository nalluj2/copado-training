trigger tr_Implant_Scheduling_Team on Implant_Scheduling_Team__c (after insert, after update, before insert, before update) {
    
    
    if(Trigger.isBefore){
    	    	
    	
    	if(Trigger.isInsert){
    	
    		bl_Implant_Scheduling_Team.setUniqueKey(Trigger.new);
    		
    		bl_Implant_Scheduling_Team.createPublicGroup(Trigger.new);
    	
    	}else if(Trigger.isUpdate){
    	    		
    		bl_Implant_Scheduling_Team.setUniqueKey(Trigger.new);	
    	}
    	
    }else{
    	
    	if(Trigger.isInsert){
    		
    		bl_Implant_Scheduling_Team.shareTeamWithGroup(Trigger.new);
    		
    		bl_Implant_Scheduling_Team.addAdminForOwner(Trigger.new);
    		
    	}else if(Trigger.isUpdate){    	
    		
    		bl_Implant_Scheduling_Team.updatePublicGroup(Trigger.new, Trigger.oldMap);
    		
    		bl_Implant_Scheduling_Team.updateAdminForOwner(Trigger.new, Trigger.oldMap);
    	}    	
    }
}