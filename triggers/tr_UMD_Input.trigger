trigger tr_UMD_Input on UMD_Input__c (before insert, before update, before delete) {
	
	if(Trigger.isInsert || Trigger.isUpdate){
	       		
    	bl_Customer_Segmentation.isDMLAllowed(Trigger.New);
    	
		for(UMD_Input__c input : Trigger.new){
			
			input.Unique_Key__c = input.UMD_Input_Complete__c + ':' + input.UMD_Question__c;
		}
		
	}else{
    	
    	bl_Customer_Segmentation.isDMLAllowed(Trigger.old);
    }
}