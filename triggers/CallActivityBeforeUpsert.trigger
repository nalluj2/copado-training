trigger CallActivityBeforeUpsert on Call_Activity_Type__c (before insert, before update, before delete) {
    /*if(Trigger.isInsert || Trigger.isUpdate){
        for(Integer i=0; i<trigger.new.size(); i++){
            trigger.new[i].Call_Activity__c = trigger.new[i].Name;    
        }    
    }*/      
   if(Trigger.isDelete)    {                
        // Check if current Call category Type is there in the Call Topic Setting Details...                   
        boolean recCount=false;        
        List<Call_Topic_Settings__c> lstSubject = [Select s.id From Call_Topic_Settings__c s where s.Call_Activity_Type__c in : Trigger.old LIMIT 1];        
        if(lstSubject.size()>0)        {            
            recCount=true;            
        }
        List<Call_Topics__c> lstCallTopic = [Select s.id From Call_Topics__c s where s.Call_Activity_Type__c in : Trigger.old LIMIT 1];        
        if(lstCallTopic.size()>0)        {            
            recCount=true;            
        }
        if(recCount==true)        {            
            trigger.old[0].name.addError('This Call Category Type is present in Call Topic or Call Topic Setting. Please delete the child records and then try again.');            
        }                
    }
}