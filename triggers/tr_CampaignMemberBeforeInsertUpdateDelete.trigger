/** 
  Description - This trigger is used to prevent creation of Campaign Members under these conditions :
  				- Campaign RecordType equals CVent
  				- User Profile differs from CVent,IT Business ANalyst, Sys Admin,Sys Admin MDT
  Author - Fabrizio Truzzu
  Created Date - 25-11-2013  
*/
//--------------------------------------------------------------------------------------------------------------------
//  Author      : Bart Caelen
//  Version     : 2.0
//  Date        : 20150629
//  Description : Rewritten the trigger to be more performant
//--------------------------------------------------------------------------------------------------------------------
//  Author      : Bart Caelen
//  Version     : 3.0
//  Date        : 20150731
//  Description : Added logic to allow user that are members of the permissionset "Campaign_Cvent_Edit" 
//					to update CampaignMembers for Cvent Campaigns.  
//					They are still not allowed to add or delete CampaignMembers for Cvent Campaigns.
//--------------------------------------------------------------------------------------------------------------------
trigger tr_CampaignMemberBeforeInsertUpdateDelete on CampaignMember (before delete, before insert, before update) {
	

	if (
			!ProfileMedtronic.isSystemAdministratorProfile(userinfo.GetProfileId()) 
			&&
			!ProfileMedtronic.isBusinessAnalystProfile(userinfo.GetProfileId())
			&&
			!ProfileMedtronic.isCVentProfile(userinfo.GetProfileId())
	){

		if (trigger.isBefore){

			Id id_RecordType_Cvent = clsUtil.getRecordTypeByDevName('Campaign', 'CVent').Id;
			List<CampaignMember> lstCampainMember = new List<CampaignMember>();

			if (Trigger.isUpdate || Trigger.isInsert){
				lstCampainMember = Trigger.new;
			}else if (Trigger.isDelete){
				lstCampainMember = Trigger.old;
			}

			// Collect the Campaign Id's of the processing Campaign Members
			Set<Id> setID_Campaign = new Set<Id>();
		    for (Campaignmember oCampaignMember : lstCampainMember){
        		setID_Campaign.add(oCampaignMember.CampaignId);
    		}
    
			// Get the CVent Campaigns for the collected Campaign ID's
	    	Map<Id, Campaign> mapCampaign_CVent = new Map<Id, Campaign>(
	    		[
	    			SELECT 
	    				id, RecordTypeId 
					FROM 
						Campaign
					WHERE
						Id in :setID_Campaign
						and RecordTypeId = :id_RecordType_Cvent
				]
			);

			//- BC - 20150731 - CR-9562 - START
			Boolean bAllowCampaignMemberUpdate = false;
			if (mapCampaign_CVent.size() > 0){
				// Get the users that are assigned to the Permission Set "Campaign_Cvent_Edit".  All these user will be allowed to update Campaign Members for CVent Campaigns
				List<PermissionSet> lstPermissionSet = [SELECT Id FROM PermissionSet WHERE name = 'Campaign_Cvent_Edit'];
				List<PermissionSetAssignment> lstPermissionSetAssignment = [SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSetId = :lstPermissionSet];
				Set<Id> setID_Assignee = new Set<Id>();
				for (PermissionSetAssignment oPSA : lstPermissionSetAssignment){
					setID_Assignee.add(oPSA.AssigneeId);
				}
				if (setID_Assignee.contains(UserInfo.getUserId()) && Trigger.isUpdate){
					bAllowCampaignMemberUpdate = true;
				}
			}
			//- BC - 20150731 - CR-9562 - STOP

		    for (Campaignmember oCampaignMember : lstCampainMember){
		    	if (mapCampaign_CVent.containsKey(oCampaignMember.CampaignId)){
		    		// Processing Campaign Member is related to a CVent Campaign
		    		if (!bAllowCampaignMemberUpdate){ //- BC - 20150731 - CR-9562 - Added
			    		oCampaignMember.addError('CVent campaign members can only be managed from CVent.');
	    			}
		    	}
		    }


		}
	}	    	   	

/*
	if(Trigger.isUpdate || Trigger.isInsert){
		Set<id> campSet=new Set<id>();
	    for(Campaignmember camp:Trigger.new){
	        campSet.add(camp.CampaignId);
	    }
	    
	    List<Campaign> campaigns = new List<Campaign>();
	    campaigns = [select c.RecordTypeId 
	    			 from Campaign c
	    			 where c.Id in: campSet];
	    			 System.debug('is SystemAdmin '+ProfileMedtronic.isSystemAdministratorProfile(userinfo.GetProfileId()) );
	    for(Campaign camp : campaigns){	    	
	    	if(camp.RecordTypeId == RecordTypeMedtronic.Campaign('CVent').ID &&
	    	   (!ProfileMedtronic.isSystemAdministratorProfile(userinfo.GetProfileId()) &&
	    	   !ProfileMedtronic.isBusinessAnalystProfile(userinfo.GetProfileId()) &&
	    	   !ProfileMedtronic.isCVentProfile(userinfo.GetProfileId()))){	    	   	
	    		Trigger.new[0].addError('CVent campaign members can only be managed from CVent.');
	    	}
	    }
	}
	
	if(Trigger.isDelete){
		Set<id> campSet=new Set<id>();
	    for(Campaignmember camp:Trigger.old){
	        campSet.add(camp.CampaignId);
	    }
	    
	    List<Campaign> campaigns = new List<Campaign>();
	    campaigns = [select c.RecordTypeId 
	    			 from Campaign c
	    			 where c.Id in: campSet];
	    			 
	    for(Campaign camp : campaigns){
	    	if(camp.RecordTypeId == RecordTypeMedtronic.Campaign('CVent').ID && 
	    	   (!ProfileMedtronic.isSystemAdministratorProfile(userinfo.GetProfileId()) &&
	    	   !ProfileMedtronic.isSystemAdministratorProfile(userinfo.GetProfileId()) &&
	    	   !ProfileMedtronic.isCVentProfile(userinfo.GetProfileId()))){
	    		Trigger.old[0].addError('CVent campaign members can only be managed from CVent.');
	    	}
	    }
	}
*/	
}