trigger UserSBUAfterUpsert on User_Business_Unit__c (after insert, after update, after Delete) {
    if (Trigger.isInsert || Trigger.isUpdate) {
        list<User_Business_Unit__c> UserSBUNew = Trigger.new;
        
        Map<Id,String> MapUserBU = new Map<Id,String>();
        Map<Id,String> MapUserCompany = new Map<Id,String>();
        Map<Id,String> MapUserBUName = new Map<Id,String>();
        set<id> setreqDibcheckUseridtrue=new set<id>();
        map<id,id> mapCheckuserid=new map<id,id>();
                //Variable Declare--Manish
        Map<id,String> MapUSerSBUName=new Map<id,String>();
       
        //End Variable Declare
        list<User> lstUser=[Select id,User_Business_Unit_vs__c,User_Sub_Bus__c from user];
        for(User u:lstUser){
            MapUserBU.put(u.id,u.User_Business_Unit_vs__c);    
        }
        set<id> usersBuIds=new set<id>();
        //Manish:variable declare
        set<id> alllinkeduserid=new set<id>();
        //Manish:End variable Declaration
        for(integer i=0;i<UserSBUNew.size();i++){
            if(UserSBUNew[i].Primary__c!=null && UserSBUNew[i].Primary__c==true){
                usersBuIds.add(UserSBUNew[i].id); 
                  
            }
            alllinkeduserid.add(UserSBUNew[i].user__c);
        }
        
        if(usersBuIds.size()>0){
            list<User_Business_Unit__c> lstUserBU=[Select user__c,Sub_Business_Unit__r.name,Sub_Business_Unit__r.Business_Unit__r.Name,Sub_Business_Unit__r.Business_Unit__r.Company__r.Company_Code_Text__c from User_Business_Unit__c where id in:usersBuIds];
            for(User_Business_Unit__c uBU:lstUserBU){
                MapUserCompany.put(uBU.user__c, uBU.Sub_Business_Unit__r.Business_Unit__r.Company__r.Company_Code_Text__c); 
                MapUserBUName.put(uBU.user__c, uBU.Sub_Business_Unit__r.Business_Unit__r.Name); 
              
                           }           
        }
        //Added By Manish: in order to update SBU on User Record in comma separated format.
        if(alllinkeduserid.size()>0)
        {
        system.debug('lstsize-->'+alllinkeduserid.size());
        list<User_Business_Unit__c> lstallUserBU=[Select user__c,Sub_Business_Unit__r.name,Sub_Business_Unit__r.Autoflag_DiB_Account_fields__c,Sub_Business_Unit__r.Business_Unit__r.Name,Sub_Business_Unit__r.Business_Unit__r.Company__r.Company_Code_Text__c from User_Business_Unit__c where user__c in:alllinkeduserid order by Sub_Business_Unit__r.name desc];
           
            for(User_Business_Unit__c uBU:lstallUserBU){
            //Start:Added by Kaushal
            if(uBU.Sub_Business_Unit__r.Autoflag_DiB_Account_fields__c == true){
                setreqDibcheckUseridtrue.add(uBU.User__c);
                mapCheckuserid.put(uBU.User__c,uBU.User__c);
                }
            
            //End:Added by Kaushal
            if(MapUSerSBUName.get(uBU.User__c)!=null)
               {
               system.debug('uBUSubBUname in if'+uBU.Sub_Business_Unit__r.name);      

                MapUSerSBUName.put(uBU.user__c,uBU.Sub_Business_Unit__r.name+';'+MapUSerSBUName.get(uBU.User__c));
               system.debug('map--> in if'+ MapUSerSBUName.get(uBU.user__c));

                }
                else{   
                system.debug('uBUSubBUname in else'+uBU.Sub_Business_Unit__r.name);      
                MapUSerSBUName.put(uBU.user__c,uBU.Sub_Business_Unit__r.name);
                system.debug('map--> in else'+ MapUSerSBUName.get(uBU.user__c));
               
                }
            }
          //Start:Added by Kaushal
          set<id> setreqDibcheckUseridfalse=new set<id>();
          for(User_Business_Unit__c uBU:lstallUserBU){
          if(mapCheckuserid.get(uBU.User__c)==null){
              if(uBU.Sub_Business_Unit__r.Autoflag_DiB_Account_fields__c == false)
              { 
              setreqDibcheckUseridfalse.add(uBU.User__c);
              }
          }
            }
         List<User> lstreqDibCheckUsertrue=[select id,name, Autoflag_DiB_Account_fields__c from User where id in: setreqDibcheckUseridtrue];
             for(User U:lstreqDibCheckUsertrue){
                 U.Autoflag_DiB_Account_fields__c =true;
                 System.debug('check flag'+U.name);
                  }   
         update lstreqDibcheckUsertrue;  
         
         List<User> lstreqDibCheckUserfalse=[select id,name, Autoflag_DiB_Account_fields__c from User where id in: setreqDibcheckUseridfalse];
         for(User U:lstreqDibCheckUserfalse){
                 U.Autoflag_DiB_Account_fields__c =false;
                 System.debug('check flag'+U.name);
                  }
         Update lstreqDibCheckUserfalse;
         //End:Added by Kaushal   
        }
        //End: Logic Added By Manish
        list<User> updateUsers = new List<User>();
        for(integer i=0;i<UserSBUNew.size();i++){
            //System.Debug('>>>>>>>USer Name - ' + UserSBUNew[i].User__r.Name + ' User BU - ' + UserSBUNew[i].User__r.User_Business_Unit_vs__c);
            if(UserSBUNew[i].Primary__c!=null && UserSBUNew[i].Primary__c==true && 
            UserSBUNew[i].User__c!=null && MapUserBU.get(UserSBUNew[i].User__c)!='All'){
                System.Debug('>>>>>>>Inside if');
                user u=new User(id=UserSBUNew[i].User__c,
                Company_Code_Text__c=MapUserCompany.get(UserSBUNew[i].User__c),
                User_Business_Unit_vs__c=MapUserBUName.get(UserSBUNew[i].User__c),
                User_Sub_Bus__c=MapUSerSBUName.get(UserSBUNew[i].User__c));
                updateUsers.add(u);                                                         
            } 
        }
        if(updateUsers.size()>0){
            update updateUsers;
        }
        updateUsers = new List<User>();
        map<id,id> MapuserIds = new map<id,id>();
        //Start: Logic Added By Manish    
        for(integer i=0;i<UserSBUNew.size();i++){
            if(UserSBUNew[i].User__c!=null && UserSBUNew[i].Primary__c!=true && MapuserIds.get(UserSBUNew[i].User__c)==null)
            {
                system.debug('hi1'+MapUSerSBUName.get(UserSBUNew[i].User__c));
                user u=new User(id=UserSBUNew[i].User__c,User_Sub_Bus__c=MapUSerSBUName.get(UserSBUNew[i].User__c));
                updateUsers.add(u);
                MapuserIds.put(UserSBUNew[i].User__c,UserSBUNew[i].User__c);
            }
        }
        //End: Logic Added By Manish   
        if(updateUsers.size()>0){
            update updateUsers;
        }        
    }
    if(Trigger.IsDelete){
        list<User_Business_Unit__c> UserSBUold = Trigger.old;
        set<id> alllinkeduserdeleteid=new set<id>();
       
        Map<id,String> MapUSerSBUDeleteName=new Map<id,String>();
        list<User> updateDeleteUsers = new List<User>();
        map<id,id> MapuserDeleteIds = new map<id,id>();
       for(integer i=0;i<UserSBUold.size();i++){
            alllinkeduserdeleteid.add(UserSBUold[i].user__c);
        }
          if(alllinkeduserdeleteid.size()>0)
        {
       
        list<User_Business_Unit__c> lstallUserBU=[Select user__c,Sub_Business_Unit__r.name,Sub_Business_Unit__r.Autoflag_DiB_Account_fields__c,Sub_Business_Unit__r.Business_Unit__r.Name,Sub_Business_Unit__r.Business_Unit__r.Company__r.Company_Code_Text__c from User_Business_Unit__c where user__c in:alllinkeduserdeleteid order by Sub_Business_Unit__r.name desc];
           
            for(User_Business_Unit__c uBU:lstallUserBU){
           
            if(MapUSerSBUDeleteName.get(uBU.User__c)!=null)
               {
                     MapUSerSBUDeleteName.put(uBU.user__c,uBU.Sub_Business_Unit__r.name+';'+MapUSerSBUDeleteName.get(uBU.User__c));
               
               }
                else{   
                      MapUSerSBUDeleteName.put(uBU.user__c,uBU.Sub_Business_Unit__r.name);
                
               }
            }
         
        }
        for(integer i=0;i<UserSBUold.size();i++){
            if(UserSBUold[i].User__c!=null && MapuserDeleteIds .get(UserSBUold[i].User__c)==null)
            {
                
                user u=new User(id=UserSBUold[i].User__c,User_Sub_Bus__c=MapUSerSBUDeleteName.get(UserSBUold[i].User__c));
                updateDeleteUsers.add(u);
                MapuserDeleteIds .put(UserSBUold[i].User__c,UserSBUold[i].User__c);
            }
        }
        if(updateDeleteUsers.size()>0){
            update updateDeleteUsers;
        } 
      
    }
}