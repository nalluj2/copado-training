trigger AssetBeforeDelete on Asset (before delete) {
    String userProfId = UserInfo.getProfileId();
    userProfId = userProfId.substring(0, 15);
    if(userProfId != FinalConstants.SystemAdminProfileId){        	
		id SAPProductId = [select id from RecordType where SobjectType='Product2' and name='SAP Product'].id;
		//System.Debug('>>>>>SAPProductId - ' + SAPProductId);
	    Set<Id> sProdIds = new Set<Id>();
			
	    for(Asset asset:trigger.old){
	    	if(Asset.Product2Id!=null){
	        	sProdIds.add(Asset.Product2Id);    		
	    	}
	    }
	
	
		if(sProdIds.size()>0){
			list<Product2> lstProducts = [select id,recordtypeId from Product2 where Id in: sProdIds];
	
			//System.Debug('>>>>>lstProducts - ' + lstProducts);
			
			map<string,id> AssetProduct = new map<string,id>();
			if(lstProducts.size()>0){
				
				for(Product2 p:lstProducts){
					AssetProduct.put(p.id,p.recordTypeId);	
				}	
				
				for(Asset asset:trigger.old){
					//System.Debug('>>>>>Asset PId - ' + AssetProduct.get(asset.Product2Id) + ' Prod Rec Type - ' + SAPProductId);
					if(AssetProduct.get(asset.Product2Id)==SAPProductId){
						asset.addError('This Asset is linked to a SAP Product, so it cannot be deleted.');	
					}
				}
				
			}
			
		}
    }
}