trigger Comment_UPSERT_BEFORE on Case_Comment__c (Before insert,Before Update) {
    
    //Populate the External_Id__c field with a new Id if no value was set before.
	GuidUtil.populateMobileId(Trigger.new, 'External_Id__c');
    
    //If we check the flag in the Custom Settings we skip the execution of this logic
	if(bl_Trigger_Deactivation.isTriggerDeactivated('Comment_UPSERT_BEFORE')) return;
    
    List<Case_Comment__c> listOfCommentToUpdate=new List<Case_Comment__c>();
    if(Trigger.Isinsert){
        for(Case_Comment__c objCommment:Trigger.New){
            
            if(objCommment.Case__c != null ){
                listOfCommentToUpdate.add(objCommment);
                system.debug('++process++'+objCommment.id);
            }
        }
        if(listOfCommentToUpdate.size()>0 && Comment_Logic.updateComplaintFlag==false){
            Comment_Logic.UpdateComplaintNumber(listOfCommentToUpdate);
        }
    }
}