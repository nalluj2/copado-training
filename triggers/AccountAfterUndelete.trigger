/**
 * Creation Date :  20100302
 * Description : 	When an account is undeleted, Its related Account_Delete_Info__c will be deleted as well!
 * Author : 		Wipro - Dheeraj
 */
trigger AccountAfterUndelete on Account (after undelete) {
	/*
	Account[] Acc = Trigger.new ;

	list<string> AccUndeleteIds = new list<string>();
	for(integer i=0 ; i<Acc.size() ; i++){
		AccUndeleteIds.add(string.valueOf(Acc[i].id));	
	} 
	
	Account_Delete_Info__c[] AccInfo ; 		// Account Delete Info record should be deleted if account is restored from recycle...
	AccInfo = [Select id from Account_Delete_Info__c where isDeleted = false and Deleted_Account_SFDC_ID__c in :AccUndeleteIds] ;

	if (AccInfo.size() > 0){
		delete AccInfo ;
	}
	*/
}