trigger tr_Product2 on Product2 (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    
    if(trigger.isBefore){
		if(trigger.isInsert){
			// CR-12836 Replaced by the Product availability functionality
			//bl_Product2.setKitable(trigger.New); 
		}

		if(trigger.isUpdate){
			// CR-12836 Replaced by the Product availability functionality
			//bl_Product2.setKitable(trigger.New, trigger.OldMap);
		}
    	
    //after	
    }else{
    	if(trigger.isInsert){
    		
    		bl_Product2.checkMarketingProductHierarchy(trigger.New, trigger.OldMap);
    			
    	}else if(trigger.isUpdate){
    		
    		bl_Product2.checkMarketingProductHierarchy(trigger.New, trigger.OldMap);
    		
    		//bl_Product2.validateNotKitable(trigger.New, trigger.OldMap);
    		
    		bl_Product2.informBoxOwnersOnProdRejection(trigger.New, trigger.OldMap);
    		
    	}else if(trigger.isDelete){
    		
    	}else if(trigger.isUndelete){
    		
    		bl_Product2.checkMarketingProductHierarchy(trigger.New, trigger.OldMap);
    	}
    }
}