/*
 *      Author 		: Bart Caelen
 *		Date		: 2014-09
 *      Description : This trigger will process created/updated/deleted/undeleted Task records in order to update the  
 *						field "Number_of_Activities__c" on the parent object "Account_Plan_2__c" with the number of Open and Closed Activities
 *						for this Account_Plan_2__c record
 *		Version		:  1.0
*/
trigger tr_Task_UpdateParent on Task (after insert, after delete, after undelete, after update) {

	if (trigger.isAfter){
		List<sObject> lstSObject = new List<sObject>();
		Map<Id, sObject> mapSObject_Old = new Map<Id, sObject>();
		if (trigger.isInsert ||  trigger.isUndelete){
			lstSObject = trigger.new;
		}else if (trigger.isDelete){
			lstSObject = trigger.old;
		}else if (trigger.isUpdate){
			lstSObject = trigger.new;
			mapSObject_Old = trigger.oldMap;
		}

		if (lstSObject.size() > 0){
			bl_Activity.updateParentWithRecordCountOfChildActivities(lstSObject, mapSObject_Old);
		}
	}
}