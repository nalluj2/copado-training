/**
    *   Description  : This trigger is to update the Relatiship Detail From Date and To Date with
    *               the start date and end date of Slected Fiscal Year.
    *    Author       : Priti Jaiswal
    *    Created Date : 01/07/2013
**/

trigger tr_RelationshipDetail_BeforeInsertUpdate on Affiliation_Details__c (before insert,before update) {
    
    if (GTUtil.relationshipDetailBefore){
    
    	GTUtil.relationshipDetailBefore = false; //Only execute logic once (limit soql queries)
	    Set<String> setAffFiscalYear=new Set<String>();
	    Map<String,FiscalYearSettings> mapFiscalYear=new Map<String,FiscalYearSettings>();
	    for(Affiliation_Details__c aff:trigger.new){        
	        setAffFiscalYear.add(aff.Fiscal_Year__c);
	    }
	    system.debug('set of fiscal year'+setAffFiscalYear);    
	    //get the fiscal year detail..
	    List<FiscalYearSettings> lstFY=[SELECT Name,StartDate,EndDate FROM FiscalYearSettings where Name in:setAffFiscalYear];
	    for(FiscalYearSettings FY:lstFY){
	        mapFiscalYear.put(FY.name,FY);
	    }
	    
	    //update the start date and end date....
	    for(Affiliation_Details__c aff:trigger.new){
	        if(mapFiscalYear.get(aff.Fiscal_Year__c)!=null){
	            aff.Affiliation_Details_Start_Date__c=mapFiscalYear.get(aff.Fiscal_Year__c).StartDate;
	            aff.Affiliation_Details_End_Date__c=mapFiscalYear.get(aff.Fiscal_Year__c).EndDate;
	        }
	    }
    }
    
}