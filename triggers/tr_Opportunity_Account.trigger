trigger tr_Opportunity_Account on Opportunity_Account__c (before insert, before update) {
    
    if(Trigger.isBefore){
    	
    	if(Trigger.isInsert){
    		
			GuidUtil.populateMobileId(Trigger.new, 'Mobile_ID__c');

    		bl_Opportunity_Account.populateUniqueKey(Trigger.new);
    		
    	}else if(Trigger.isUpdate){
    		
    		bl_Opportunity_Account.populateUniqueKey(Trigger.new);
    	}
    }
}