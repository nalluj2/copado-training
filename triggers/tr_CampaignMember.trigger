//--------------------------------------------------------------------------------------------------------------------
//	Author 			: Bart Caelen
//  Created Date 	: 22-11-2017
//  Description 	: APEX Trigger on CampaignMember
//  Change Log 		: 
//--------------------------------------------------------------------------------------------------------------------
trigger tr_CampaignMember on CampaignMember (after insert, after update) {

	if (Trigger.isAfter){

		if (Trigger.isInsert){
			
			bl_CampaignMember_Trigger.learning_provisioning(Trigger.new, Trigger.oldMap);
			
		}else if(Trigger.isUpdate){
			
			bl_CampaignMember_Trigger.learning_provisioning(Trigger.new, Trigger.oldMap);
			
		}


	}

}
//--------------------------------------------------------------------------------------------------------------------