trigger tr_OpportunityCompetitor on Opportunity_Competitor__c (after delete) {
    for(Opportunity_Competitor__c opc:Trigger.old){
        if(Trigger.oldMap.get(opc.id).Primary__c==true){
        Trigger.oldMap.get(opc.id).addError(' Remove or change the primary competitor by changing the primary competitor on the opportunity page');
        }
    }
}