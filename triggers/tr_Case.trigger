//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 05-01-2016
//  Description      : APEX Trigger on Case 
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
trigger tr_Case on Case (before insert, before update, before delete, after insert, after update) {


	if (Trigger.isBefore){

		if (Trigger.isInsert){
			
			bl_Case_Trigger.activitySchedulingBlockOutsideApps(Trigger.new);
			
			bl_Case_Trigger.activitySchedulingSetInternalOwner(Trigger.new);
			
			bl_Case_Trigger.setCaseComplexity(Trigger.new, null);
			
			//bl_Case_Trigger.activitySchedulingBlockOutsideWorkingHours(Trigger.new);
			
			bl_Case_Trigger.activitySchedulingSetStatus(Trigger.new, null);

		}else if (Trigger.isUpdate){
			
			bl_Case_Trigger.activitySchedulingBlockOutsideApps(Trigger.new);
				
			bl_Case_Trigger.preventUpdateOnRestrictedFields(Trigger.new, Trigger.oldMap);

			bl_Case_Trigger.updateNotificationDescription(Trigger.new, Trigger.oldMap);

			bl_Case_Trigger.setCaseComplexity(Trigger.new, Trigger.oldMap);
			
			//bl_Case_Trigger.activitySchedulingBlockOutsideWorkingHours(Trigger.new);
			
			bl_Case_Trigger.activitySchedulingSetStatus(Trigger.new, Trigger.oldMap);
		
		}else if(Trigger.isDelete){
			
			bl_Case_Trigger.activitySchedulingBlockOutsideApps(Trigger.old);
			
			bl_Case_Trigger.activityDeleteRecurrency(Trigger.old);			
		}

	}else if (Trigger.isAfter){

		if (Trigger.isInsert){
			
			bl_Case_Trigger.cloneOMACase(Trigger.new);
			
			bl_Case_Trigger.activitySchedulingShareWithTeam(Trigger.new, null);
			
			bl_Case_Trigger.activitySchedulingManageEvent(Trigger.new, null);
			
			bl_Case_Trigger.activitySchedulingRemoveAgentContact(Trigger.new);
			
			bl_Case_Trigger.activityManageRecurrency(Trigger.new, null);

		}else if (Trigger.isUpdate){
		
			bl_Case_Trigger.notificationSAP(Trigger.new, Trigger.oldMap, 'UPDATE');
			
			bl_Case_Trigger.updateSOGCHNumber(Trigger.new, Trigger.oldMap);
			
			bl_Case_Trigger.activitySchedulingShareWithTeam(Trigger.new, Trigger.oldMap);
			
			bl_Case_Trigger.activitySchedulingManageEvent(Trigger.new, Trigger.oldMap);
			
			bl_Case_Trigger.activitySchedulingNotifyOnRejection(Trigger.new, Trigger.oldMap);
			
			bl_Case_Trigger.activityManageRecurrency(Trigger.new, Trigger.oldMap);
			
		}
	}

}
//--------------------------------------------------------------------------------------------------------------------