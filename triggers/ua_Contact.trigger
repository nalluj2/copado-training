/*
*	Trigger to log user actions on Contact (create / update/ delete)
* 	Author: Rudy De Coninck
*	Creation date: 17-12-2014
*/

trigger ua_Contact on Contact (after delete, after insert, after undelete, after update) {

	//We do not track bulk operations
	if (Trigger.size == 1){
		String operation='none';
		List<Contact> recordsToProcess = new List<Contact>();
		if (Trigger.isDelete){
			operation = 'Delete';		
			for (Contact a :Trigger.old){
				recordsToProcess.add(a);				
			}  
		}else{
			if (Trigger.isInsert){
				operation = 'Insert';
			}
			if(Trigger.isUpdate){
				operation='Update';
			}
			for (Contact a :Trigger.new){
				recordsToProcess.add(a);				
			}  
		}
		if(recordsToProcess.size()>0){
			for(Contact a : recordsToProcess){
				//Only log initial action and not if contact action plan as source
				if (!bl_UserAction.userActionInProgress && !bl_UserAction.contactActionPlan2Operation){
					bl_UserAction.logObjectWithParent(UserInfo.getUserId(), operation,'Contact',a.id,'',a.AccountId,'Account');
					bl_UserAction.userActionInProgress = true;  
				} 
			}
		}
	} 
}