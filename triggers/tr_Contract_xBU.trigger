trigger tr_Contract_xBU on Contract_xBU__c (before insert, after insert, after update) {

	if(Trigger.isBefore){
		
		if(Trigger.isInsert){
			
			bl_Contract_xBU.setContractNumber(Trigger.new);
		
		}
		
	}else{
		
		if(Trigger.isInsert){
			
			bl_Contract_xBU.createHistoryTracking(Trigger.new, null);
			
			bl_Contract_xBU.updateOldContract(Trigger.new, null);

		}else if(Trigger.isUpdate){
			
			bl_Contract_xBU.createHistoryTracking(Trigger.new, Trigger.oldMap);

			bl_Contract_xBU.updateOldContract(Trigger.new, Trigger.oldMap);

		}
	}    
}