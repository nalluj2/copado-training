trigger trgTherapyBeforeDelete on Therapy__c (Before Delete) 
{
    list<Product_Group__c> pgData = [select id from Product_Group__c where Therapy_ID__c in:trigger.old];
    if(pgData.size()>0)
    {
       trigger.old[0].name.addError('This Therapy is present in Product Group. Please delete the child records and then try again.');                
    }
   
}