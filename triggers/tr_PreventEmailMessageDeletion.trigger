/*
 *      Created Date : 07 -Mar-2013
 *      Description : This is the class is called by tr_PreventActivityDeletion on task and activity to prevent the deletion of
                     activity if they are related to case and the case record type is one of the OMA record types and user profile 
                     is OMA profile.
 *
 *      Author = Kaushal Singh
 *      
 */
trigger tr_PreventEmailMessageDeletion on EmailMessage (before delete) {
  
    bl_tr_PreventActivityDeletion.PreventDeleteEmailMessage(Trigger.Old);

}