trigger tr_Case_BeforeUpdate on Case (before update) {
    
        if(Trigger.isUpdate && Trigger.isBefore){
        
        Case caseToUpdate; 
        
        for (Case cs : Trigger.new) {
            caseToUpdate = cs;
        } 

   		Map<Id,Schema.RecordTypeInfo> recordTypesById = Schema.SObjectType.Case.getRecordTypeInfosById();
		
		String rtName = recordTypesById.get(caseToUpdate.RecordTypeId).getDeveloperName();
			
        if( (rtName  == 'OMA' || rtName  == 'OMA_Neoro_MedInfo' ||
           rtName  == 'OMA_Neuro_Physician_Scientist_Exchange' ||
           rtName  == 'OMA_Spine_Clinical_Support_Observation' ||
           rtName  == 'OMA_Spine_Scientific_Exchange_Product')
           && caseToUpdate.ContactId != null){ 
            Contact contact = [Select c.Phone,c.Email,
                                      c.Fax, c.MobilePhone,
                                      c.MailingCountry, c.MailingPostalCode,
                                      c.MailingState, c.MailingCity,
                                      c.MailingStreet,  c.Account.name
                               from Contact c
                               Where c.Id =: caseToUpdate.ContactId];
            caseToUpdate.Contact_Phone__c = contact.Phone;
            caseToUpdate.Contact_Email__c = contact.Email;
            caseToUpdate.Contact_Fax__c = contact.Fax;
            caseToUpdate.Contact_Mobile__c = contact.MobilePhone;
            caseToUpdate.Contact_Country__c = contact.MailingCountry;
            caseToUpdate.Contact_Mailing_Zip__c = contact.MailingPostalCode;
            caseToUpdate.Contact_Mailing_State__c = contact.MailingState;
            caseToUpdate.Contact_Mailing_City__c = contact.MailingCity;
            caseToUpdate.Contact_Mailing_Street__c = contact.MailingStreet;  
            caseToUpdate.Contact_Primary_Account__c = contact.Account.name;  
        }
    }
}