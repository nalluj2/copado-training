trigger SubBusinessUnitBeforeUpsert on Sub_Business_Units__c (before insert, before update, before delete) {
    if(Trigger.isDelete)
    {
        // Check if current record SBU is there in the Therapy etc then it should not be deleted...
        boolean recCount=false;
        List<Therapy__c> lstTherapy = [Select s.id From Therapy__c s where s.Sub_Business_Unit__c in :  Trigger.old LIMIT 1];
        if(lstTherapy.size()>0)
        {
            recCount=true;    
        }  
              
        if(recCount==true)
        {
            trigger.old[0].name.addError('This Sub Business Unit is present in Therapy. Please delete the child records and then try again.');    
        }
    }    
}