trigger trgConcatImplainDetail on Implant_Detail_Junction__c (after insert, after update) {

	if (GTutil.trgConcatImplainDetail){
		GTutil.trgConcatImplainDetail = false;
	
	    if(Trigger.isInsert || Trigger.isUpdate){
	        set<id> ImplantIds = new set<id>();
	        for(Implant_Detail_Junction__c impDetail:trigger.new){
	            ImplantIds.add(impDetail.Implant_ID__c);
	        }
	        System.Debug('>>>>>ImplantIds - ' + ImplantIds);
	        map<id,string> mapImplantOption = new map<id,string>();
	        string strImpOption = '';
	        boolean boolFlag = true;
	        list<Implant_Detail_Junction__c> lstImpDet  = [SELECT Implant_ID__c, Implant_Option_ID__r.Name 
	                                                        FROM Implant_Detail_Junction__c where Implant_ID__c in: ImplantIds order by Implant_ID__c];
	        for(Implant_Detail_Junction__c impDetJunction:lstImpDet){
	            if(mapImplantOption.get(impDetJunction.Implant_ID__c)==null && boolFlag==false){
	                strImpOption = strImpOption + impDetJunction.Implant_Option_ID__r.Name + ';'; 
	                mapImplantOption.put(impDetJunction.Implant_ID__c,strImpOption);                
	                strImpOption = '';
	            } else {
	                strImpOption = strImpOption + impDetJunction.Implant_Option_ID__r.Name + ';';
	                mapImplantOption.put(impDetJunction.Implant_ID__c,strImpOption);                
	                boolFlag = false;               
	            }   
	        }
	        System.Debug('>>>>>strImpOption - ' + strImpOption);
	        list<Implant__c> impupdate = new list<Implant__c>();
	        for(id ImpId:ImplantIds){
	            Implant__c imp=new Implant__c(id=ImpId, Implant_Option__c=mapImplantOption.get(ImpId));
	            impupdate.add(imp);
	        }
	        System.Debug('>>>>>impupdate - ' + impupdate);      
	        if(impupdate.size()>0){
	            update impupdate;
	        }
	    }
	}
}