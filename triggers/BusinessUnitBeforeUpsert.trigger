trigger BusinessUnitBeforeUpsert on Business_Unit__c (before insert, before update, before delete) {
    /*if(Trigger.isInsert || Trigger.isUpdate)    {       
        for(Integer i=0; i<trigger.new.size(); i++){
            trigger.new[i].Business_Unit__c = trigger.new[i].Name + trigger.new[i].Company__c;    
        }            
    }*/
    if(Trigger.isDelete)
    {
        // Check if current record BU is there in the Therapy etc then it should not be deleted...
        boolean recCount=false;
        List<Therapy__c> lstTherapy = [Select s.id From Therapy__c s where s.Business_Unit__c in :  Trigger.old LIMIT 1];
        if(lstTherapy.size()>0)
        {
            recCount=true;    
        }  
        
        List<Affiliation__c> lstAff = [Select s.id From Affiliation__c s where s.Business_Unit__c in :  Trigger.old LIMIT 1];
        if(lstAff.size()>0)
        {
            recCount=true;    
        }  

        List<Call_Records__c> lstVisit = [Select s.id From Call_Records__c s where s.Business_Unit__c in :  Trigger.old LIMIT 1];
        if(lstVisit.size()>0)
        {
            recCount=true;    
        }  

        List<Implant__c> lstImplant = [Select s.id From Implant__c s where s.Business_Unit__c in :  Trigger.old LIMIT 1];
        if(lstImplant.size()>0)
        {
            recCount=true;    
        }  

        List<Call_Topic_Settings__c> lstCall = [Select s.id From Call_Topic_Settings__c s where s.Business_Unit__c in :  Trigger.old LIMIT 1];
        if(lstCall.size()>0)
        {
            recCount=true;    
        }  

       /* List<Procedures__c> lstProd = [Select s.id From Procedures__c s where s.Business_Unit__c in :  Trigger.old LIMIT 1];
        if(lstProd.size()>0)
        {
            recCount=true;    
        }  
        */

        List<Account_Team_Member__c> lstAcc = [Select s.id From Account_Team_Member__c s where s.Business_Unit__c in :  Trigger.old LIMIT 1];
        if(lstAcc.size()>0)
        {
            recCount=true;    
        }  
           
        List<Sub_Business_Units__c> lstSBU = [Select s.id From Sub_Business_Units__c s where s.Business_Unit__c in :  Trigger.old LIMIT 1];
        if(lstAcc.size()>0)
        {
            recCount=true;    
        }  

        List<User_Business_Unit__c> lstUserSBU = [Select s.id From User_Business_Unit__c s where s.Sub_Business_Unit__r.Business_Unit__c in :  Trigger.old LIMIT 1];
        if(lstUserSBU.size()>0)
        {
            recCount=true;    
        }  

              
        if(recCount==true)
        {
            trigger.old[0].name.addError('This Business Unit is present atleast in on of the records of Therapy, Competitor, Affiliation, Call Record, Implant, Call Topic Settings, Procedures, Account Team Member, User Business Unit. Please delete the child records and then try again.');    
        }
    }    
}