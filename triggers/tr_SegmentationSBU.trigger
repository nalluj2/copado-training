/*
 *      Author 		: Bart Caelen
 *		Date		: 2014-02
 *      Description : 
 *			BEFORE INSERT TRIGGER
 * 				This trigger will check for duplicate Segmentation_SBU__c records based on the Start Date and will throw an error if needed in order to automatically 
 *				This trigger will also look for the same records based on Account__c and Sub_Business_Unit__c and it will De-Activate the old record (fill in the End date) and insert the new active record.
 *		Version		:  1.0
 * ------------------------------------------------------------------------------------------------------------------------
 *      Author 		: Bart Caelen
 *		Date		: 2016-06-20
 *      Description : 
 *			Added mapping data to Master Data and added Account_Segmentation_Type__c
 * ------------------------------------------------------------------------------------------------------------------------
 *      Author 		: Bart Caelen
 *		Date		: 2018-11-29
 *      Description : 
 *			Deleted the logic in the AFTER Trigger
 *
 *			AFTER INSERT / UPDATE
 *				When a Segmentation_SBU__c record is created or update we need to copy the values of some fields to the coresponding fields on Account.  This fieldmapping is stored in a MAP but can alos be stored
 *					in a Custom Setting or another Setting Object if needed in the future.
 *			AFTER DELETE
 *				When a Segmentation_SBU__c record is deleted we need to clear the coresponding fields on Account.
 * ------------------------------------------------------------------------------------------------------------------------
*/
trigger tr_SegmentationSBU on Segmentation_SBU__c (before insert, after insert) {

	//--------------------------------------------
	// BEFORE TRIGGER
	//--------------------------------------------
	if (Trigger.isBefore){
		//--------------------------------------------
		// BEFORE INSERT TRIGGER
		//--------------------------------------------
		if (Trigger.isInsert){

			Map<string, Segmentation_SBU__c> mapSegmentationSBU_NEW =  new Map<string,Segmentation_SBU__c>();
			Set<id> setID_Account = new Set<id>();
			Set<id> setID_SubBusinessUnit = new Set<id>();
			for (Segmentation_SBU__c oSegmentationSBU : Trigger.new){
				String tUniqueKey = String.valueOf(oSegmentationSBU.Account__c) + String.valueOf(oSegmentationSBU.Sub_Business_Unit__c) + clsUtil.isNull(oSegmentationSBU.Account_Segmentation_Type__c, '');
				mapSegmentationSBU_NEW.put(tUniqueKey,oSegmentationSBU);
				setID_Account.add(oSegmentationSBU.Account__c);
				setID_SubBusinessUnit.add(oSegmentationSBU.Sub_Business_Unit__c);
			}
		    
		    // Get existing Segmentation_SBU__c without End Date in a map
		  	Map<string, Segmentation_SBU__c> mapSegmentationSBU_OLD =  new Map<string, Segmentation_SBU__c>();
		  	for(Segmentation_SBU__c oSegmentationSBU : 
		  		[
		    		SELECT 
		    			Id, Account__c, Sub_Business_Unit__c, End__c, Start__c, Unique_Key__c, Account_Segmentation_Type__c
		    		FROM 
		    			Segmentation_SBU__c 
		    		WHERE 
		    			Account__c in :setID_Account 
		    			AND Sub_Business_Unit__c in :setID_SubBusinessUnit 
		    			AND End__c = null
	    		]
		  	){
		  		String tUniqueKey = String.valueOf(Id.valueOf(String.valueOf(oSegmentationSBU.Account__c))) + String.valueOf(Id.valueOf(String.valueOf(oSegmentationSBU.Sub_Business_Unit__c))) + clsUtil.isNull(oSegmentationSBU.Account_Segmentation_Type__c, '');
		    	mapSegmentationSBU_OLD.put(tUniqueKey, oSegmentationSBU);
		  	} 

			List<Segmentation_SBU__c> lstSegmentationSBU_Update = new List<Segmentation_SBU__c>();
			for (Segmentation_SBU__c oSegmentationSBU_NEW : Trigger.new){
		  		String tUniqueKey = String.valueOf(Id.valueOf(String.valueOf(oSegmentationSBU_NEW.Account__c))) + String.valueOf(Id.valueOf(String.valueOf(oSegmentationSBU_NEW.Sub_Business_Unit__c))) + clsUtil.isNull(oSegmentationSBU_NEW.Account_Segmentation_Type__c, '');
				
				if (mapSegmentationSBU_OLD.containsKey(tUniqueKey)){
					Segmentation_SBU__c oSegmentationSBU_OLD = mapSegmentationSBU_OLD.get(tUniqueKey);

					if (oSegmentationSBU_NEW.Start__c == oSegmentationSBU_OLD.Start__c){
						oSegmentationSBU_NEW.addError('Segmentation record exists, please update existing record');
					}else{
						if(oSegmentationSBU_NEW.Start__c > oSegmentationSBU_OLD.Start__c){
							Date dDate = oSegmentationSBU_NEW.Start__c - 1;
							oSegmentationSBU_OLD.End__c = dDate;
							lstSegmentationSBU_Update.add(oSegmentationSBU_OLD);
						}
					}
				}
			}

			if(lstSegmentationSBU_Update.size()>0){
				update lstSegmentationSBU_Update;      
			}
		}
	}
	
}