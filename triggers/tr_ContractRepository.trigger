trigger tr_ContractRepository on Contract_Repository__c (before insert, before update, before delete, after insert, after update, after delete){ 

    if (Trigger.isBefore){

        if (Trigger.isInsert){

            bl_ContractRepository_Trigger.populateWithAccountData(Trigger.new, null);
             bl_ContractRepository_Trigger.populateEmailReminderDates(Trigger.new, null);

        }else if (Trigger.isUpdate){

            bl_ContractRepository_Trigger.populateWithAccountData(Trigger.new, Trigger.oldMap);
            bl_ContractRepository_Trigger.populateEmailReminderDates(Trigger.new, Trigger.oldMap);

        }else if (Trigger.isDelete){
                        
            bl_ContractRepository_Trigger.protectInterfacedContracts(Trigger.old);

            bl_Contract_Repository_Document.collectDocumentVersionsToDelete(Trigger.old);
        }

    }else if (Trigger.isAfter){

        if (Trigger.isInsert){

            bl_ContractRepository_Trigger.createOwnerTeamMember(Trigger.new, null);
            
            bl_ContractRepository_Trigger.createDefaultTeamMembers(Trigger.new, null);
            
            bl_ContractRepository_Trigger.createPrimaryAccount(Trigger.new, null);

            bl_ContractRepository_Trigger.createHistoryTracking(Trigger.new, null);
            
        }else if (Trigger.isUpdate){

            bl_ContractRepository_Trigger.createOwnerTeamMember(Trigger.new, Trigger.oldMap);
            
            bl_ContractRepository_Trigger.createDefaultTeamMembers(Trigger.new, Trigger.oldMap);
            
            bl_ContractRepository_Trigger.createPrimaryAccount(Trigger.new, Trigger.oldMap);

            bl_ContractRepository_Trigger.createHistoryTracking(Trigger.new, Trigger.oldMap);
            
        }else if(Trigger.isDelete){
            
            bl_Contract_Repository_Document.setDocumentVersionsToDelete();
        
        }

    
    }
}