//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 11-04-2016
//  Description      : APEX Trigger on Lead 
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
trigger tr_Lead on Lead (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

	// Pass the Trigger newMap and oldMap to the APEX Class (TriggerHandler)
	bl_Lead_Trigger.lstTriggerNew = Trigger.new;
	bl_Lead_Trigger.mapTriggerNew = Trigger.newMap;
	bl_Lead_Trigger.mapTriggerOld = Trigger.oldMap;

	if (Trigger.isBefore) {

		if (Trigger.isInsert){

			bl_Lead_Trigger.setAccountAffiliation();
			bl_Lead_Trigger.copyLeadSalutation('INSERT');

		}else if (Trigger.isUpdate){

			bl_Lead_Trigger.setAccountAffiliation();
			bl_Lead_Trigger.copyLeadSalutation('UPDATE');

		}
    
	} else if (Trigger.isAfter) {

		if (trigger.isInsert){

			bl_Lead_Trigger.addLeadToCampaign_Insert();

		}
    
	}

}
//--------------------------------------------------------------------------------------------------------------------