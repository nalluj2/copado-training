trigger tr_TerritoryChange on Territory_Change__c (after insert, after update) {

	
	if (!System.isFuture()){
	
		List<Id> terrChangeIds = new List<Id>();
		
		for (Territory_Change__c tc : Trigger.new){
			
			terrChangeIds.add(tc.id);		
			
		}
		
		if (terrChangeIds.size()>0){
			bl_TerritoryChange.changeTerritories(terrChangeIds);
			bl_TerritoryChange.changeTerritoriesDelete(terrChangeIds);
			
		}

	}
}