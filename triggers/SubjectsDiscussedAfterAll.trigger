/**
 * Creation Date :  20091001
 * Description :    After All Apex Trigger : The Subjects discussed are shown in the Visit Report related list in the Contact screen.
 *
 * Author :         Dheeraj Gupta
 */
 
trigger SubjectsDiscussedAfterAll on Call_Topics__c (before delete, after insert, after update, after undelete) {
 
   if(Trigger.isDelete){
        SubjectsDiscussed.updateSubjectDiscussed(Trigger.old, 0); 
         // Check if current Call Topic is present in there in the Call Topic Product...        
          boolean recCount=false;        
         List<Call_Topic_Products__c> lstProd = [Select s.id From Call_Topic_Products__c s where s.Call_Topic__c in : Trigger.old LIMIT 1];        
         if(lstProd.size()>0)        
         {            
             recCount=true;            
         }  
         List<Call_Topic_Subject__c> lstSub = [Select s.id From Call_Topic_Subject__c s where s.Call_Topic__c in : Trigger.old LIMIT 1];        
         if(lstSub.size()>0)        
         {            
             recCount=true;            
         }  
         if(recCount==true && !bl_CallManagement.forceDeleteCallTopic)        
         {            
             trigger.old[0].name.addError('This Call Topic is present in Call Topic Product or Call Topic Subject. Please delete the child records and then try again.');            
         }         
    }        
    if(Trigger.isUnDelete)
        SubjectsDiscussed.updateSubjectDiscussed(Trigger.new, 1);
       
    if(Trigger.isInsert && GTutil.chkInsertSubjectDiscusedAfterAll){
        GTutil.chkInsertSubjectDiscusedAfterAll=false;
        SubjectsDiscussed.updateSubjectDiscussed(Trigger.new, 2);
        Europe_Call_Record__c EurCallDetails = Europe_Call_Record__c.getInstance();
    	String idCallActivityProductDemonstration=null;
        if(EurCallDetails !=null){
       		idCallActivityProductDemonstration=EurCallDetails.Product_Demonstration__c;
       		if (idCallActivityProductDemonstration!=null){
       			idCallActivityProductDemonstration=idCallActivityProductDemonstration.substring(0,15);
       		}
    	} 
        List<Id> callTopicProductDemonstrations =  new List<Id>();
        For (Call_topics__c ct : Trigger.new){
        	String callActivityID = ct.Call_Activity_Type__c;
        	callActivityID = callActivityID.substring(0,15);
        	if (ct.Call_Activity_Type__c != null && callActivityID == idCallActivityProductDemonstration){
        		callTopicProductDemonstrations.add(ct.id);
        	}
        }
        
        if (callTopicProductDemonstrations.size()>0){
        	bl_CallManagement.updateProductDemonstrationOpportunity(callTopicProductDemonstrations);
        }
    }
    if(Trigger.isUpdate && GTutil.chkUpdateSubjectDiscusedAfterAll){
        GTutil.chkUpdateSubjectDiscusedAfterAll=false;
        SubjectsDiscussed.updateSubjectDiscussed(Trigger.new, 2);
        Europe_Call_Record__c EurCallDetails = Europe_Call_Record__c.getInstance();
    	String idCallActivityProductDemonstration=null;
        if(EurCallDetails !=null){
       		idCallActivityProductDemonstration=EurCallDetails.Product_Demonstration__c;
       		if (idCallActivityProductDemonstration!=null){
       			idCallActivityProductDemonstration=idCallActivityProductDemonstration.substring(0,15);
       		}
    	} 
        List<Id> callTopicProductDemonstrations =  new List<Id>();
        For (Call_topics__c ct : Trigger.new){
        	String callActivityID = ct.Call_Activity_Type__c;
        	callActivityID = callActivityID.substring(0,15);
        	if (ct.Call_Activity_Type__c != null && callActivityID == idCallActivityProductDemonstration){
        		callTopicProductDemonstrations.add(ct.id);
        	}
        }
        
        if (callTopicProductDemonstrations.size()>0){
        	bl_CallManagement.updateProductDemonstrationOpportunity(callTopicProductDemonstrations);
        }
    }
}