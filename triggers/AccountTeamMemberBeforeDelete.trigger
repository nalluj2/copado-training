/**
 * Creation Date :  20101214
 * Description :    Before Delete Apex Trigger : Prevents the deletion of any account member which is primary and system admin profile
 * Specifications : Only System Administrator Profiles can Mass Delete ; All others can't !
 * Author :         Wipro - Navneet Rajput
 */

trigger AccountTeamMemberBeforeDelete on Account_Team_Member__c (before delete) 
{

        try
        {
        List<Account_Team_Member__c> oldActTeamMember=Trigger.old;
        System.Debug('Number of records = ' + Trigger.size) ;
        List <Account_Team_Member__c> ATM;
        ATM=[Select Name,Id,User__c,Primary__c from Account_Team_Member__c where Id in :oldActTeamMember]; 
//        for (Account_Team_Member__c act :ATM) 
        for (Account_Team_Member__c act :trigger.old) 

        {
            //Only System user (batch process) can delete primary accountmembers
            boolean result=act.Primary__c;
            if (result!=null && result==true) 
            {
                if (UserInfo.getLastName().compareTo('System')!=0)
               {
                    act.addError('Primary account team members cannot be deleted');
              }
           }    
    
       }
       }
       
       catch (Exception ex)
       {
       }
}