/*
*	Trigger to log user actions on Stakeholder_Mapping__c (create / update/ delete)
* 	Author: Rudy De Coninck
*	Creation date: 17-12-2014
*/

trigger ua_StakeholderMapping on Stakeholder_Mapping__c (after delete, after insert, after undelete, after update) {

	//We do not track bulk operations
	if (Trigger.size == 1){
		String operation='none';
		List<Stakeholder_Mapping__c> recordsToProcess = new List<Stakeholder_Mapping__c>();
		if (Trigger.isDelete){
			operation = 'Delete';		
			for (Stakeholder_Mapping__c a :Trigger.old){
				recordsToProcess.add(a);				
			}  
		}else{
			if (Trigger.isInsert){
				operation = 'Insert';
			}
			if(Trigger.isUpdate){
				operation='Update';
			}
			for (Stakeholder_Mapping__c a :Trigger.new){
				recordsToProcess.add(a);				
			}  
		}
		if(recordsToProcess.size()>0){
			for(Stakeholder_Mapping__c a : recordsToProcess){
				//Only log initial action
				if (!bl_UserAction.userActionInProgress){
					bl_UserAction.logObject(UserInfo.getUserId(), operation,'Stakeholder_Mapping__c',a.id,'');
					bl_UserAction.userActionInProgress = true;  
				} 
			}
		}
	} 
}