trigger tr_Contract_Repository_Bundle on Contract_Repository_Bundle__c (before delete, before update) {
	
	if(Trigger.isBefore){
		
		if(Trigger.isUpdate){
			
			bl_Contract_Repository_Bundle.protectInterfacedBundles(Trigger.new);
			
		}else if(Trigger.isDelete){
			
			bl_Contract_Repository_Bundle.protectInterfacedBundles(Trigger.old);
		}		
	}    
}