/**
 * Creation Date :  20090924
 * Description :    After Insert & Update Apex Trigger : The last visited date will be updated to the Contact's last visit date field.
 *
 * Author :         Tuan Abdeen / ABSI
 * ------------------------------------------------------------
 * Author 		:	Bart Caelen
 * Date			:	20140129
 * Description	:	CR-3006 - Added logic to count the distinct related Accounts for a Call Record
 * ------------------------------------------------------------
 * Author 		:	Bart Caelen
 * Date			:	20140129
 * Description	:	CR-3663 - Update the logic so that the Distinct Count of the Accounts and Contacts are not performed in a @Future call but it will
 *					indicate the Call Record as "updated" and a nightly batch apex will process the indicated Call Records.
  */
 
trigger ContactVisitReportAfterAll on Contact_Visit_Report__c (after delete, after insert, after undelete, after update) {
		 
	if (GTUtil.ContactVisitReportAfterAll){
	
		GTUtil.ContactVisitReportAfterAll = false;	 

		if (Trigger.isAfter && (Trigger.isInsert || Trigger.IsUpdate || Trigger.isDelete || Trigger.isUndelete) ){
			
			list<Contact_Visit_Report__c> lstCVR_Process = new list<Contact_Visit_Report__c>();
			
			set<id> setCallRecordID = new set<id>();
			if (Trigger.isInsert || Trigger.IsUpdate || Trigger.isUndelete){
				for (Contact_Visit_Report__c cc : Trigger.new){
					if (clsUtil.isNull(cc.Call_Records__c, '') != ''){
						setCallRecordID.add(cc.Call_Records__c);
					}
				}
			}else if (Trigger.isDelete){
				for(Contact_Visit_Report__c cc : Trigger.old){
					if (clsUtil.isNull(cc.Call_Records__c, '') != ''){
						setCallRecordID.add(cc.Call_Records__c);
					}
				}
			}
			
			if (setCallRecordID.size() > 0){
				list<Contact_Visit_Report__c> lstContactVisitReport = 
					[
						select 
							id, Attending_Affiliated_Account__r.Account_Country_vs__c
							, Call_Records__c, Call_Records__r.Id, Call_Records__r.Countries__c
						from 
							Contact_Visit_Report__c 
						where 
							Call_Records__c in :setCallRecordID
						order by 
							CreatedDate
					];
	
				map<id, Call_Records__c> mapCallRecord = new map<id, Call_Records__c>();
				for (Contact_Visit_Report__c oContactVisitReport : lstContactVisitReport ){
					Call_Records__c oCallRecord = oContactVisitReport.Call_Records__r;
					if (!mapCallRecord.containsKey(oCallRecord.Id)){
			        	oCallRecord.Process_Batch__c = true;
						if (!trigger.isDelete){
			        		oCallRecord.Countries__c = oContactVisitReport.Attending_Affiliated_Account__r.Account_Country_vs__c;
						}
			        	mapCallRecord.put(oCallRecord.Id, oCallRecord);
					}
				}

				if (mapCallRecord.size() > 0){
					update mapCallRecord.values();
				}
			}
			
//			if (setcallRecId.size() > 0){
//				bl_CallRecord.updateAttendedAccountContact_FUTURE(setcallRecId);
//			}
		}
	}
/*	- BC - 201401 - OLD CODE		
	if (GTUtil.ContactVisitReportAfterAll){
	
		GTUtil.ContactVisitReportAfterAll = false;	 

	    if(Trigger.isDelete){
	        //lastDateVisitReport.updateLastVisitedDate(Trigger.old, 0);
	        AttendedContacts.updateAttendedContacts(Trigger.old, 0);
	    }
	    if (Trigger.isUnDelete){ 
	        //lastDateVisitReport.updateLastVisitedDate(Trigger.old, 0); 
	        AttendedContacts.updateAttendedContacts(Trigger.old, 1);
	    }
	    if (Trigger.isUpdate || Trigger.isInsert ){
	        //lastDateVisitReport.updateLastVisitedDate(Trigger.new, 0); 
	        AttendedContacts.updateAttendedContacts(Trigger.new, 2);
	    }

		//Start: Added by Manish
		if(Trigger.isAfter && (Trigger.isInsert || Trigger.IsUpdate || Trigger.isDelete)){
		
			set<id> setcallRecId=new set<id>();
			if(Trigger.isInsert || Trigger.IsUpdate){
				for(Contact_Visit_Report__c cc:Trigger.new){
					setcallRecId.add(cc.Call_Records__c);
				}
			}
			if(Trigger.isDelete){
				for(Contact_Visit_Report__c cc:Trigger.old){
					setcallRecId.add(cc.Call_Records__c);
				}
			}
		
			map<id,Call_Records__c> mapcallRec = new map<id,Call_Records__c>([select id,Countries__c,Related_Accounts__c from Call_Records__c where id in:setcallRecId]);
			List<Call_records__c> lstCRUpd = new List<Call_records__c>();
			map<id,id> mapCallRecid = new map<id,id>();
			map<id,id> mapAvoidduplicateAccount = new map<id,id>();

		for(Contact_Visit_Report__c cc:[select id,Attending_Affiliated_Account__c,Attending_Affiliated_Account__r.name,Attending_Affiliated_Account__r.Account_Country__c,Call_Records__r.Countries__c from Contact_Visit_Report__c where Call_Records__c in :setCallRecId order by createddate])
		{
			if(mapCallRecid.get(cc.Call_Records__c)==null){  
				if(cc.Attending_Affiliated_Account__c!=null){ 
					mapcallRec.get(cc.Call_Records__c).Related_Accounts__c='<a target="_self" href="/'+cc.Attending_Affiliated_Account__c+'">'+cc.Attending_Affiliated_Account__r.name+'</a>;';  
		            mapcallRec.get(cc.Call_Records__c).Countries__c=cc.Attending_Affiliated_Account__r.Account_Country__c;
		            lstCRUpd.add(mapcallRec.get(cc.Call_Records__c));
		           	mapCallRecid.put(cc.Call_Records__C,cc.Call_Records__C);
		           	mapAvoidduplicateAccount.put(cc.Attending_Affiliated_Account__c,cc.Attending_Affiliated_Account__c);
	            }

	        }else{  
			    //else part cover logic for updating 'Related Accounts' field on Call Record.(Work Plan ID:R4.2 003 CR-244(15thMayRelease).)  
				if( mapAvoidduplicateAccount.get(cc.Attending_Affiliated_Account__c)==null){
		        	mapcallRec.get(cc.Call_Records__c).Related_Accounts__c=mapcallRec.get(cc.Call_Records__c).Related_Accounts__c+'<a target="_self" href="/'+cc.Attending_Affiliated_Account__r.id+'">'+cc.Attending_Affiliated_Account__r.name+'</a>;';  
					mapAvoidduplicateAccount.put(cc.Attending_Affiliated_Account__c,cc.Attending_Affiliated_Account__c);
                }            
	         }
		
		}
		update lstCRUpd;

		
		}
		//End: Added by Manish
		}    
*/
}