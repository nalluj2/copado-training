trigger Contract_UPDATE_AFTER on Contract (after update, after insert) {

	if(Trigger.isInsert) { 
		bl_Contract.copyAssets(Trigger.New);
		
		Contract_Logic.cloneContractConnections(Trigger.new);
	} 
	 

	//If we check the flag in the Custom Settings we skip the execution of this logic
	if(bl_Trigger_Deactivation.isTriggerDeactivated('Contract_UPDATE_AFTER')) return;
	
	Contract_Logic.setContractName(Trigger.newMap);
	
}