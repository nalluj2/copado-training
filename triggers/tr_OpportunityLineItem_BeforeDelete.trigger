/*
 * Work Item Nr		: WI-336
 * Description		: WI-336 - Prevent deletion of OpportunityLineItem with a SAP ID  		 
 * Author        	: Patrick Brinksma
 * Created Date    	: 03-09-2013
 */
trigger tr_OpportunityLineItem_BeforeDelete on OpportunityLineItem (before delete) {
	for (OpportunityLineItem thisOpptyLine : Trigger.Old){
		if (thisOpptyLine.SAP_Item_ID_Text__c != null){
			thisOpptyLine.addError(Label.OpportunityLineItemDeleteError);
		}
	}
}