trigger tr_ContactAutoMail on Contact (after insert, after update) {

    if (bl_Trigger_Deactivation.isTriggerDeactivated('tr_ContactAutoMail')) return;	//-BC - 20170621 - Added
	
	Boolean extralinecodecoverage = true;

	if(Test.isRunningTest()) return;

    et4ae5.triggerUtility.automate('Contact');
}