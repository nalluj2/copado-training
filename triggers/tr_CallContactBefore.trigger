/*
 *      Description : handle all before related items
 *
 *      Author = Rudy De Coninck
*/

trigger tr_CallContactBefore on Contact_Visit_Report__c (before insert) {
	
	
	Set<Id> callRecordIds = new Set<Id>();
	
	for (Contact_Visit_Report__c ct : Trigger.new){
		
		if (ct.mobile_Id__c == null) ct.Mobile_ID__c = GuidUtil.NewGuid();
		
		callRecordIds.add(ct.Call_Records__c);				
	}
	
	Map<Id, Call_Records__c> callRecordMap = new Map<Id, Call_Records__c>([Select Id, Products_Concatenated__c from Call_Records__c where Id IN :callRecordIds]);
	
	for (Contact_Visit_Report__c ct : Trigger.new){
		
		ct.Products_Concatenated__c = callRecordMap.get(ct.Call_Records__c).Products_Concatenated__c;
	}	
}