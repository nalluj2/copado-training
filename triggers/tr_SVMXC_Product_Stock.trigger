trigger tr_SVMXC_Product_Stock on SVMXC__Product_Stock__c (before insert, before update) {
	
	// Before trigger logic
	if(Trigger.isBefore){
		
		if(Trigger.isInsert){
			
			bl_SVMXC_Product_Stock_Trigger.populateProductInformation(Trigger.new);
			
		}else if(Trigger.isUpdate){
			
			bl_SVMXC_Product_Stock_Trigger.populateProductInformation(Trigger.new);
		}
		
	// After trigger logic	
	}else{    
		
	}
}