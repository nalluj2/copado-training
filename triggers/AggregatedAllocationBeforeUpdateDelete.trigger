/**
 * Creation Date :  20101118
 * Description : 	Before Delete Apex Trigger : Prevents the deletion of any DIB_Aggregated_Allocation__c
 * Specifications : Only System Administrator Profiles and some other profile can Edit/Delete 
 *					Sytem Administrators, Data Stewards can delete any Account ; All others can't !
 * Author : 		ABSI - Malaka Silva
 */
trigger AggregatedAllocationBeforeUpdateDelete on DIB_Aggregated_Allocation__c (before delete, before update) {
	SharedMethods sharedMethod = new SharedMethods();
	if(sharedMethod.checkEditDeletePermission(Userinfo.getProfileId())){
		set<Id> sUpdateIds = null;
		if(controllerAllocationAggregated.mAccountAggregationUpsert != null){
			sUpdateIds = controllerAllocationAggregated.mAccountAggregationUpsert.get(Userinfo.getUserId());
		}
		DIB_Aggregated_Allocation__c [] dibAggregatedAllocations = trigger.new;
		if(dibAggregatedAllocations == null){
			dibAggregatedAllocations = trigger.old;
		}
		System.debug('########################-delete permission##################');
		System.debug(sUpdateIds);
		for(DIB_Aggregated_Allocation__c dibAggregatedAllocation:dibAggregatedAllocations){
			if(sUpdateIds == null || !sUpdateIds.contains(dibAggregatedAllocation.Id)){
				dibAggregatedAllocation.addError(FinalConstants.NO_EDIT_DELETE_PERMISSION);
			}
		}
	}
}