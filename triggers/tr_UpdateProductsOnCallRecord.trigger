/**
 *      Created Date : 20130226
 *      Description : This Trigger is used to update concatenate all the products of call topic belonging to a 
                      call record and put this in Products Concatenated field on call record.
 * 
 *      Author = Kaushal
 *      Modified By: Manish(R8.0-36 CR-1928) Date: 25/4/2013:To update related Products name on Call Topic
 */
trigger tr_UpdateProductsOnCallRecord on Call_Topic_Products__c (after insert){
    if(trigger.IsInsert){
        bl_tr_UpdateProductsOnCallRecord.UpdateProductsConcatenated(Trigger.newmap.keyset());
        //start: Added by Manish
        bl_CallTopicUpdates.UpdateProductsConcatenated(Trigger.new);
        //End: Added by manish
    }
}