/*
	Unified trigger for Opportunity_Asset__c
*/
trigger tr_OpportunityAsset on Opportunity_Asset__c (before insert, before update) {

	if (Trigger.isBefore){
	
		if (Trigger.isInsert){

			bl_OpportunityAsset_Trigger.updateDIENCode(Trigger.new, null);
		
		}else if (Trigger.isUpdate){
		
			bl_OpportunityAsset_Trigger.updateDIENCode(Trigger.new, Trigger.oldMap);

		}
	
	}

}