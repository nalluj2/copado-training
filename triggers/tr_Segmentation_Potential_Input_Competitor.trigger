trigger tr_Segmentation_Potential_Input_Competitor on Segmentation_Potential_Input_Competitor__c (before insert, before update, before delete) {
       
    if(Trigger.isInsert || Trigger.isUpdate){
	       		
    	bl_Customer_Segmentation.isDMLAllowed(Trigger.New);
    		    
	    for(Segmentation_Potential_Input_Competitor__c inputComp : Trigger.new){
	    	
	    	inputComp.Unique_Key__c = inputComp.Segmentation_Potential_Input__c + ':' + inputComp.Segmentation_Potential_Proc_Competitor__c;    	
	    }
	    
    }else{
    	
    	bl_Customer_Segmentation.isDMLAllowed(Trigger.old);
    }
}