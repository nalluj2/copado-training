//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 05-01-2016
//  Description      : APEX Trigger on SVMXC__Service_Order_Line__c 
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
trigger tr_SVMXC_ServiceOrderLine on SVMXC__Service_Order_Line__c (before insert, before update, before delete, after insert, after update, after delete, after undelete){

	
	if (Trigger.isBefore){

		if (Trigger.isInsert){

			// ALL BEFORE INSERT ACTIONS
			bl_SVMXC_ServiceOrderLine_Trigger.populateOperationAndTechnician(trigger.new);
			
			bl_SVMXC_ServiceOrderLine_Trigger.defaultQtyOnTimeRegistration(trigger.new);

		}else if (Trigger.isUpdate){
			
			bl_SVMXC_ServiceOrderLine_Trigger.populateOperationAndTechnician(trigger.new);
			
			bl_SVMXC_ServiceOrderLine_Trigger.preventUpdateOnRestrictedFields(trigger.new, trigger.oldMap, 'UPDATE');
		}


	}else if (Trigger.isAfter){

		if (Trigger.isInsert){
			// ALL AFTER INSERT ACTIONS
			//bl_SVMXC_ServiceOrderLine_Trigger.sendEmail(trigger.new, 'INSERT');
		}
	}
}
//--------------------------------------------------------------------------------------------------------------------