trigger Comment_UPSERT_AFTER on Case_Comment__c (after insert, after update) {
    
    //If we check the flag in the Custom Settings we skip the execution of this logic
	if(bl_Trigger_Deactivation.isTriggerDeactivated('Comment_UPSERT_AFTER')) return;
    
    if(bl_SynchronizationService_Utils.isSyncProcess == false){
		
		if(Trigger.isInsert) bl_SynchronizationService_Source.createNotificationIfChanged(Trigger.New, null);
		else bl_SynchronizationService_Source.createNotificationIfChanged(Trigger.New, Trigger.OldMap);	
	}
    
    List<ID> complaintIdlist = new List<ID>();
    Map<Id, Complaint__c> mapOfComplaint = new Map<Id, Complaint__c>();
    Map<Id, Case_Comment__c> mapOfComment = new Map<Id, Case_Comment__c>();
    if(trigger.IsInsert){
        for(Case_Comment__c com:Trigger.New ){
           
            if(com.Complaint__c != Null ){
                ComplaintIdlist.add(com.Complaint__c);
                mapOfComment.put(com.Complaint__c, com);     
                     
            }
        }  
        /*if(ComplaintIdlist.size() > 0) {
            Complaint_Logic.AutoCreateMDR(ComplaintIdlist);
        }*/           
    }    
    
    if(mapOfComment.size() > 0 && Case_logic.updateFlag == false) {       
        for(Complaint__c comp : [select Id, Parent_Complaint_No__c, Name from Complaint__c Where Id IN : ComplaintIdlist]) {
            mapOfComplaint.put(comp.Id, comp);
        }
        //Map<Id, String> mapOfNameForComp = new Map<Id, String>(); 
        Case_logic.complaintRemove(mapOfComplaint, null, 'Complaint', 'Update', mapOfComment);
    }
}