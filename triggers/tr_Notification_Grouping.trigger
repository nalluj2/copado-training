trigger tr_Notification_Grouping on Notification_Grouping__c (before update) {
    
    if(Trigger.isBefore){
    	
    	if(Trigger.isUpdate){
    		
    		bl_Notificaton_Grouping.calculateInboundStatus(Trigger.New);
    		
    		bl_Notificaton_Grouping.calculateOutboundStatus(Trigger.New);
    	}
    }
}