//----------------------------------------------------------------------------------------
//	Unified trigger for Patient_Training__c
//----------------------------------------------------------------------------------------
trigger tr_PatientTraining on Patient_Training__c (before insert, before update){ 


	if (Trigger.isBefore){

		if (Trigger.isInsert){

			bl_PatientTraining_Trigger.calculateCompensation(Trigger.new, Trigger.oldMap);

		}else if (Trigger.isUpdate){

			bl_PatientTraining_Trigger.calculateCompensation(Trigger.new, Trigger.oldMap);

		}

	}

}
//----------------------------------------------------------------------------------------