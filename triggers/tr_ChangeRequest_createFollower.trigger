/*
*   Description  - This trigger is used to automatically create the requestor as a follower
*                  of Change Request record . 
*   Author       - Priti Jaiswal
*   Created Date - 04/02/2013
*/

trigger tr_ChangeRequest_createFollower on Change_Request__c (after insert,after update) {

    List<EntitySubscription> allExistingFollowersList=new List<EntitySubscription>();
    List<EntitySubscription> followersToBeaddedList=new List<EntitySubscription>();
    List<EntitySubscription> followersToBeRemovedList=new List<EntitySubscription>();
    set<Id> changeRequestIDs=new set<Id>();
    Boolean bl=false;
    
    for(Change_Request__c CR:Trigger.new){
        changeRequestIDs.add(CR.id);
    }
    allExistingFollowersList=[select ParentId,SubscriberId from EntitySubscription where ParentId in:changeRequestIDs];
    Map<id,List<EntitySubscription>> accFollowerMap=new Map<id,List<EntitySubscription>>();
    For(Change_Request__c CR:Trigger.new){
        EntitySubscription follower=new EntitySubscription();  
              
        if(trigger.isInsert){
            follower.ParentId=CR.id;
            follower.SubscriberId=CR.Requestor_Lookup__c; 
            followersToBeaddedList.add(follower);           
        }        
        List<EntitySubscription> ExistingFollowersList=new List<EntitySubscription>();   
        
        if(trigger.isUpdate){
            for(EntitySubscription f :allExistingFollowersList){                
                if(f.parentId==CR.id){
                    ExistingFollowersList.add(f);                    
                    accFollowerMap.put(CR.id,ExistingFollowersList);
                }
            }  
            if(CR.Requestor_Lookup__c!=Trigger.oldMap.get(CR.id).Requestor_Lookup__c && (CR.Status__c!='Completed' || CR.Status__c!='Closed' || CR.Status__c!='Closed - duplicate' || CR.Status__c!='Closed - out of scope' || CR.Status__c!='Closed - resolved' || CR.Status__c!='Closed - Vendavo Product Enhancement' || CR.Status__c!='Closed - workaround'))
            {
                if(accFollowerMap.get(CR.id)!=null){
                    for(EntitySubscription f:accFollowerMap.get(CR.id)){                
                        if(CR.Requestor_Lookup__c == f.SubscriberID){
                            bl=true;                    
                            break;
                        }
                    }
                }
                if(bl==false){                
                    follower.ParentId=CR.id;
                    follower.SubscriberId=CR.Requestor_Lookup__c;
                    followersToBeaddedList.add(follower);                    
                }
            } 
            //added this logic to remove existing follower for record when Status is completed/closed----Priti
            if(CR.Status__c=='Completed' || CR.Status__c=='Closed' || CR.Status__c=='Closed - duplicate' || CR.Status__c=='Closed - out of scope' || CR.Status__c=='Closed - resolved' || CR.Status__c=='Closed - Vendavo Product Enhancement' || CR.Status__c=='Closed - workaround')  {
                for(EntitySubscription f:allExistingFollowersList){
                    followersToBeRemovedList.add(f);    
                }
            }       
        } 
                
    }
    if(followersToBeaddedList.size()>0){
        insert followersToBeaddedList;
    }
    if(followersToBeRemovedList.size()>0){
        delete followersToBeRemovedList;
    }
}