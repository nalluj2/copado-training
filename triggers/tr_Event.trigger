/*
 *  Description  - This trigger handles all Event related executions
 *                
 *  Author       -  Rudy De Coninck
 *  Created Date - 24-12-2014
 */
//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 04-05-2016
//  Description      : Added the logic BEFORE INSERT
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
trigger tr_Event on Event (before insert, before update, before delete, after delete, after insert, after update) {

    if (Trigger.isBefore){

        if (Trigger.isInsert){
        	
        	bl_Event_Trigger.activitySchedulingBlockOutsideApps(Trigger.New);
        	
            bl_Event_Trigger.populateMobileId(Trigger.New);

            bl_Activity.validateActiveAccount(Trigger.New, 'Event');
                        
        }else if(Trigger.isUpdate){
        	
        	bl_Event_Trigger.activitySchedulingBlockOutsideApps(Trigger.New);
        	
        	bl_Event_Trigger.populateMobileId(Trigger.New);

        }else if(Trigger.isDelete){
        	
        	bl_Event_Trigger.activitySchedulingBlockOutsideApps(Trigger.Old);

        }

	}else if (Trigger.isAfter){
	
	/*
		List<Event> eventsToProcess = new List<Event>();
		String operation='None';
		
		if(Trigger.isDelete){
			operation='Delete';
			for (Event event : Trigger.old){
				eventsToProcess.add (event);
			}
		}else{
			for (Event event : Trigger.new){
				eventsToProcess.add (event);
			}
			
			if(Trigger.isInsert){
				operation='Insert';
			}
			if(Trigger.isUpdate){
				operation='Update';
			}
		}



		boolean mmxInterface = ProfileMedtronic.isMMXInterface(UserInfo.getProfileId());
		//Only send notification if not mmx interface to avoid loops
		if (eventsToProcess.size()>0 && !mmxInterface){
			bl_Activity.changeNotification(eventsToProcess,operation);
		}
	*/

	}

}