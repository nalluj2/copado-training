trigger Workorder_Sparepart_after_del_undel on Workorder_Sparepart__c (after delete, after undelete) {
	
	//Roll up Order Part prices to parent Work Order	
	Set<Id> woToCalculate = new Set<Id>();
	
	List<Workorder_Sparepart__c> sParts;
	
	if(Trigger.isDelete) sParts = Trigger.Old;	
	if(Trigger.isUndelete) sParts = Trigger.New;
				
	for(Workorder_Sparepart__c sPart : sParts){
		
		if(sPart.Workorder__c != null && sPart.Order_Part_Total_Price__c != null) woToCalculate.add(sPart.Workorder__c);
	}
		
	if(woToCalculate.size() > 0){
		
		CasePartTriggers.calculateWorkOrderTotal(woToCalculate);	
	}	
}