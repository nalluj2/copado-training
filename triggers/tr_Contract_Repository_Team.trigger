trigger tr_Contract_Repository_Team on Contract_Repository_Team__c (after delete, after insert, after undelete, after update, before insert, before update, before delete) {
	
	if(Trigger.isBefore){
		
		if(Trigger.isInsert){
			
			bl_Contract_Repository_Team.setUniqueKey(Trigger.new);
			
		}else if(Trigger.isUpdate){
			
			bl_Contract_Repository_Team.protectAutoMembers(Trigger.new, Trigger.oldMap);
			
			bl_Contract_Repository_Team.setUniqueKey(Trigger.new);
						
		}else if(Trigger.isDelete){
			
			bl_Contract_Repository_Team.protectAutoMembers(Trigger.old, null);
		}
		
	}else{
		
		if(Trigger.isInsert){
			
			bl_Contract_Repository_Team.manageSharing(Trigger.new);
			
		}else if(Trigger.isUpdate){
			
			bl_Contract_Repository_Team.manageSharing(Trigger.new);			
		
		}else if(Trigger.isDelete){
			
			bl_Contract_Repository_Team.manageSharing(Trigger.old);			
		
		}else if(Trigger.isUndelete){
			
			bl_Contract_Repository_Team.manageSharing(Trigger.new);			
		}
	}
}