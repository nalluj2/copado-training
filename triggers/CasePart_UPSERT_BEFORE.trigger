trigger CasePart_UPSERT_BEFORE on Workorder_Sparepart__c (before insert,before Update) 
{      
    //Populate the External_Id__c field with a new Id if no value was set before.
	GuidUtil.populateMobileId(Trigger.new, 'External_Id__c');
    
    Set<Id> rtInScope = new Set<Id>();
    for(RecordType rt : [Select Id from RecordType where SObjectType = 'Workorder_Sparepart__c' and DeveloperName IN ('Hardware', 'Inaccuracy', 'Service', 'Software')]) rtInScope.add(rt.Id);
    
    Set<Id> productIds = new Set<Id>();
    List<Workorder_Sparepart__c> setPrice = new List<Workorder_Sparepart__c>();
    
    for(Workorder_Sparepart__c sparepart : Trigger.New){
    	
    	if(rtInScope.contains(sparepart.RecordTypeId)){
    		
    		if(sparepart.Order_Part__c != null){
    	
    			if( Trigger.isInsert || sparepart.Order_Part_List_Price__c == null || 
    					(Trigger.isUpdate && 
    						( sparepart.Order_Part__c != Trigger.oldMap.get(sparepart.Id).Order_Part__c || sparepart.CurrencyIsoCode != Trigger.oldMap.get(sparepart.Id).CurrencyIsoCode))){
    				
    				productIds.add(sparepart.Order_Part__c);
    				setPrice.add(sparepart);
    			}
	    		
    		}else{
    			
    			sparepart.Order_Part_List_Price__c = null;
    		}
    	}    	
    }
    
    if(setPrice.size() > 0){
    	
    	Map<String, Decimal> productPrices = new Map<String, Decimal>();
    	
    	for(PricebookEntry priceEntry : [Select Product2Id, UnitPrice, CurrencyIsoCode From PricebookEntry where Pricebook2.Name = 'EMEA Service Pricebook' AND Product2Id IN :productIds AND IsActive = true]){
    		
    		productPrices.put(priceEntry.Product2Id + ':' + priceEntry.CurrencyIsoCode, priceEntry.UnitPrice);    		
    	}
    	
    	for(Workorder_Sparepart__c sparepart : setPrice){
    		
    		sparepart.Order_Part_List_Price__c =  productPrices.get(sparepart.Order_Part__c + ':' + sparepart.CurrencyIsoCode); 		
    	}	
    }
        
    //If we check the flag in the Custom Settings we skip the execution of this logic
	if(bl_Trigger_Deactivation.isTriggerDeactivated('CasePart_UPSERT_BEFORE')) return;
               
    CasePartTriggers.ValidateSW(Trigger.new); 
  
    CasePartTriggers.Associate_Case_Parts(Trigger.new);     
}