/*
 * Description    : fill mobile id (mobile ADPR)       
 * Author         : Jesus Lozano
 * Created Date   : 7-5-2015
 */
trigger tr_ReviewRecordBefore on Review_Record__c (before insert, before update) {

  // Create Mobile Id for inserted and updated records
  GuidUtil.populateMobileId(Trigger.new, 'Mobile_ID__c');

}