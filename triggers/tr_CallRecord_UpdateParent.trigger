/*
 *      Author 		: Bart Caelen
 *		Date		: 2014-09
 *      Description : This trigger will process created/updated/deleted/undeleted Call_Records__c records in order to update the  
 *						field "Number_of_Call_Records__c" on the parent object "Account_Plan_2__c" with the number of Call Records
 *						for this Account_Plan_2__c record
 *		Version		:  1.0
*/
trigger tr_CallRecord_UpdateParent on Call_Records__c (after insert, after delete, after undelete, after update) {

	if (trigger.isAfter){
		String tParentSObjectName = 'Account_Plan_2__c';
		String tChildParentField = 'Account_Plan__c';
		String tParentFieldToUpdate = 'Number_of_Call_Records__c';

		List<sObject> lstSObject = new List<sObject>();
		if (trigger.isInsert ||  trigger.isUndelete){
			lstSObject = trigger.new;
		}else if (trigger.isDelete){
			lstSObject = trigger.old;
		}else if (trigger.isUpdate){
			for (Call_Records__c oCallRecord_New : trigger.new){
				Call_Records__c oCallRecord_Old = trigger.oldMap.get(oCallRecord_New.Id);
				if (oCallRecord_New.get(tChildParentField) != oCallRecord_New.get(tChildParentField)){
					lstSObject.add(oCallRecord_New);
					lstSObject.add(oCallRecord_Old);
				}
			}
		}

		
		Set<String> setChildObjectName = new Set<String>();
			setChildObjectName.add('Call_Records__c');

        String tSObjectPrefix = clsUtil.getPrefixFromSObjectName(tParentSObjectName);

        if (lstSObject.size() > 0){
            Set<String> setParentID = new Set<String>();
            for (sObject oSObject : lstSObject){
                if (oSObject.get(tChildParentField) != null){
                    if (String.valueOf(oSObject.get(tChildParentField)).startsWith(tSObjectPrefix)){
                        setParentID.add(String.valueOf(oSObject.get(tChildParentField)));
                    }
                }
            }

            if (setParentID.size() > 0){
                clsUtil.updateParentWithRecordCountOfChild(tParentSObjectName, 'Id', tParentFieldToUpdate, setParentID, setChildObjectName, tChildParentField, true);
            }
        }
	}

}