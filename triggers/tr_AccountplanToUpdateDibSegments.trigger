/*
 *      Created Date : 28/10/2012
 *      Trigger Name: tr_AccountPlanToUpdateDibSegments
 *      Test class for the trigger: Test_ContactActionPlanNew
 *      Description : This trigger is used to update Dib Segment of a Account if there exist a Europe/MEA Dibetes Core SBU Account Plan for that account
 *      Author = Manish Kumar Srivastava
 */

trigger tr_AccountplanToUpdateDibSegments on Account_Plan_2__c (after insert) {
           
    Set<Id> diabetesCoreSBUs = new Set<Id>();    
    for(Sub_Business_Units__c sbu : [Select Id From Sub_Business_Units__c where Name = 'Diabetes Core' AND Company_Text__c = 'Europe']) diabetesCoreSBUs.add(sbu.Id);
        
    Map<Id, Id> accPlansByAccount = new Map<Id, Id>();
    
    for(Account_Plan_2__c accPlan : Trigger.new){
    	
        if(accPlan.Sub_Business_Unit__c != null && diabetesCoreSBUs.contains(accPlan.Sub_Business_Unit__c)){
        	        	      	
            accPlansByAccount.put(accPlan.Account__c, accPlan.Id);
        }
    }
    
    if(accPlansByAccount.keyset().size()>0){
    	
    	// Connect Diabetes Segmentation records
    	List<DIB_Department__c> segmentationToUpdate = new List<DIB_Department__c>();
        
        for(DIB_Department__c dibDep : [Select Id, Account_Plan__c, Account__c from DIB_Department__c where Account__c IN :accPlansByAccount.keyset()]){
                
            Id accPlanId = accPlansByAccount.get(dibDep.Account__c);
                        
            if(accPlanId != dibDep.Account_Plan__c){
	            	
	           	dibDep.Account_Plan__c = accPlanId;
	          	segmentationToUpdate.add(dibDep);
	        }   
        }
        
        if(segmentationToUpdate.size() > 0) update segmentationToUpdate;                
    }
}