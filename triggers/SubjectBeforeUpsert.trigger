trigger SubjectBeforeUpsert on Subject__c (before insert, before update, before delete) {
    /*if(Trigger.isInsert || Trigger.isUpdate){
        for(Integer i=0; i<trigger.new.size(); i++){
            trigger.new[i].Subject__c = trigger.new[i].Name;    
        }    
    }*/      
    if(Trigger.isDelete)    {                
        // Check if current Call category Type is there in the Call Topic Setting Details...                   
        boolean recCount=false;        
        List<Call_topic_Setting_Details__c> lstSubject = [Select s.id From Call_topic_Setting_Details__c s where s.Subject__c in : Trigger.old LIMIT 1];        
        if(lstSubject.size()>0)        {            
            recCount=true;            
        }
        List<Call_Topic_Subject__c> lstSubject1 = [Select s.id From Call_Topic_Subject__c s where s.Subject__c in : Trigger.old LIMIT 1];        
        if(lstSubject1.size()>0)        {            
            recCount=true;            
        }
        if(recCount==true)        
        {            
         trigger.old[0].name.addError('This subject has related child records. Please delete the child records and try again');            
        }                
    }           
}