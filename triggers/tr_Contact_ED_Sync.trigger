trigger tr_Contact_ED_Sync on Contact_ED_Sync__c (after insert) {
    
	if (bl_Trigger_Deactivation.isTriggerDeactivated('extDirectoryProvisioning')) return;


	if (bl_BMPSIntegration.oAPISettings != null && bl_BMPSIntegration.oAPISettings.Active__c){

		// Use the new Logic and send the request to BMPS

		Integer iMaxRecords = Integer.valueOf(bl_BMPSIntegration.oAPISettings.Maximum_Records__c);

		if (Trigger.new.size() > iMaxRecords) throw new ValidationException('No more than ' + iMaxRecords + ' BPMS synchronizations can be requested at a time');
    
		if (System.IsBatch() || System.isFuture()) return;
    
		for (Contact_ED_Sync__c sync : Trigger.new){

			bl_BMPSIntegration.initiateOnboarding(sync.Id);

		}    
		

	}else{
    
		// Use the old Logic and executed the External Directory and CSOD Integrations

		if (ED_API_Settings__c.getInstance().Target_Server_URL__c == null) return;

		Integer iMaxRecords = 50;
		CSOD_API_Settings__c oSettings = CSOD_API_Settings__c.getInstance();
		if (oSettings.Org_Id__c == UserInfo.getOrganizationId()) iMaxRecords = Integer.valueOf(oSettings.Maximum_Records__c);

		if (Trigger.new.size() > iMaxRecords) throw new ValidationException('No more than ' + iMaxRecords + ' User Provisioning synchronizations can be requested at a time');
    
		if (System.IsBatch() || System.isFuture()) return;
    
		for (Contact_ED_Sync__c sync : Trigger.new){
    	
    		bl_Contact_LearningManagement_Provision.doUserProvision(sync.Id);

		}    

	}

}