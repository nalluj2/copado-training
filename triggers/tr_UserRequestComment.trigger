//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 28-03-2017
//  Description      : Unified trigger for User_Request_Comment__c (Support Portal)
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
trigger tr_UserRequestComment on User_Request_Comment__c (after insert) {

	if (Trigger.isAfter){

		if (Trigger.isInsert){

			bl_UserRequestComment_Trigger.updateCreateUserRequest(Trigger.new);
			
		}

	}

}
//--------------------------------------------------------------------------------------------------------------------