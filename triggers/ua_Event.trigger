/*
*	Trigger to log user actions on Event (create / update/ delete)
* 	Author: Rudy De Coninck
*	Creation date: 17-12-2014
*/

trigger ua_Event on Event (after delete, after insert, after undelete, after update) {

	//We do not track bulk operations
	if (Trigger.size == 1){
		String operation='none';
		List<Event> recordsToProcess = new List<Event>();
		if (Trigger.isDelete){
			operation = 'Delete';		
			for (Event a :Trigger.old){
				recordsToProcess.add(a);				
			}  
		}else{
			if (Trigger.isInsert){
				operation = 'Insert';
			}
			if(Trigger.isUpdate){
				operation='Update';
			}
			for (Event a :Trigger.new){
				recordsToProcess.add(a);				
			}  
		}
		if(recordsToProcess.size()>0){
			for(Event a : recordsToProcess){
				//Only log initial action
				if (!bl_UserAction.userActionInProgress){
					bl_UserAction.logObject(UserInfo.getUserId(), operation,'Event',a.id,'');
					bl_UserAction.userActionInProgress = true;  
				} 
			}
		}
	} 
}