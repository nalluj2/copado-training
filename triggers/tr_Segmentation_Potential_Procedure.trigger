trigger tr_Segmentation_Potential_Procedure on Segmentation_Potential_Procedure__c (before insert, before update) {
	
	Set<String> sbuNames = new Set<String>();
	
	for(Segmentation_Potential_Procedure__c procedure : Trigger.new){
		
		if(Trigger.isInsert || procedure.Unique_Key__c == null) sbuNames.add(procedure.Sub_Business_Unit__c);
	}    
	
	if(sbuNames.isEmpty()) return;
	
	Map<String, String> sbuBUGroup = new Map<String, String>();
	
	for(Sub_Business_Units__c sbu : [Select Name, Business_Unit__r.Business_Unit_Group__r.Name from Sub_Business_Units__c where Name IN :sbuNames]){
		
		sbuBUGroup.put(sbu.Name, sbu.Business_Unit__r.Business_Unit_Group__r.Name);
	}
	
	for(Segmentation_Potential_Procedure__c procedure : Trigger.new){
		
		if(Trigger.isInsert || procedure.Unique_Key__c == null) procedure.Unique_Key__c = procedure.Name + ':' + sbuBUGroup.get(procedure.Sub_Business_Unit__c);
	}    
}