//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 22-05-2017
//  Description      : Unified APEX Trigger on Country_ProductGroup__c 
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
trigger tr_CountryProductGroup on Country_ProductGroup__c (before insert, before update) {


	if (Trigger.isBefore){

		if (Trigger.isInsert){

			bl_CountryProductGroup_Trigger.populateExternalId(Trigger.new, Trigger.oldMap);

		}else if (Trigger.isUpdate){

			bl_CountryProductGroup_Trigger.populateExternalId(Trigger.new, Trigger.oldMap);

		}

	}


}
//--------------------------------------------------------------------------------------------------------------------