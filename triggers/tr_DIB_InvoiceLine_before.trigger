trigger tr_DIB_InvoiceLine_before on DIB_Invoice_Line__c (before insert) {

    for(DIB_Invoice_Line__c line : Trigger.New){
    
        if(line.Confidence_Level__c == null){
            line.Confidence_Level__c = '100%';
        }
    }
}