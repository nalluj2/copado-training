/*
*	Trigger to log user actions on Account (create / update/ delete)
* 	Author: Rudy De Coninck
*	Creation date: 17-12-2014
*/


trigger ua_Account on Account (after delete, after insert, after undelete, after update) {
	
	//We do not track bulk operations
	if (Trigger.size == 1){
		String operation='none';
		List<Account> recordsToProcess = new List<Account>();
		if (Trigger.isDelete){
			operation = 'Delete';		
			for (Account a :Trigger.old){
				recordsToProcess.add(a);				
			}  
		}else{
			if (Trigger.isInsert){
				operation = 'Insert';
			}
			if(Trigger.isUpdate){
				operation='Update';
			}
			for (Account a :Trigger.new){
				recordsToProcess.add(a);				
			}  
		}
		if(recordsToProcess.size()>0){
			for(Account a : recordsToProcess){
				//Only log initial action
				if (!bl_UserAction.userActionInProgress){
					bl_UserAction.logObject(UserInfo.getUserId(), operation,'Account',a.id,'');
					bl_UserAction.userActionInProgress = true;  
				} 
			}
		}
	} 

}