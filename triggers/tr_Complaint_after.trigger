trigger tr_Complaint_after on Complaint__c (after insert) {
	
	//If we check the flag in the Custom Settings we skip the execution of this logic
	if(bl_Trigger_Deactivation.isTriggerDeactivated('tr_Complaint_after')) return;
	
	if(Trigger.isInsert && bl_SynchronizationService_Utils.isSyncProcess == false){
		
		bl_SynchronizationService_Source.createNotificationIfChanged(Trigger.New, null);		
	}
}