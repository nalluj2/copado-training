trigger tr_Product_Country_Availability on Product_Country_Availability__c (before insert, before update, after insert, after update) {
    
    // Before events
    if(Trigger.isBefore){
    	
    	if(Trigger.isInsert){
    		
    		bl_Product_Country_Availability.populateUniqueKey(Trigger.new);
    		
    	}else if (Trigger.isUpdate){
    		
    		bl_Product_Country_Availability.populateUniqueKey(Trigger.new);
    	}
    
    // After events	
    }else{
    	
    	if(Trigger.isInsert){
    		
    		bl_Product_Country_Availability.informBoxOwnersOnProdApproval(Trigger.new, Trigger.oldMap);
    		
    		bl_Product_Country_Availability.informBoxOwnersOnProdRejection(Trigger.new, Trigger.oldMap);
    		
    		bl_Product_Country_Availability.alignSpainWithCanaryIslands(Trigger.new);
    		
    	}else if (Trigger.isUpdate){
    		
    		bl_Product_Country_Availability.informBoxOwnersOnProdApproval(Trigger.new, Trigger.oldMap);
    		
    		bl_Product_Country_Availability.informBoxOwnersOnProdRejection(Trigger.new, Trigger.oldMap);
    		
    		bl_Product_Country_Availability.alignSpainWithCanaryIslands(Trigger.new);
    	}
    }    
}