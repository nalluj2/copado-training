/*
  *  Trigger Name    : WorkOrder_UPSERT_BEFORE
  *  Description   : This Trigger execute the methods from WorkOrder_Logic class.
  *  Created Date  : 30/7/2013
  *  Author        : Wipro Tech.
*/

trigger WorkOrder_UPSERT_BEFORE on Workorder__c (before insert, before update) {
    
    //Populate the External_Id__c field with a new Id if no value was set before.
    GuidUtil.populateMobileId(Trigger.new, 'External_Id__c');
    
    Set<String> recordTypeNames = WorkOrder_Logic.recordTypeToProductCode.keySet();
        
    Map<Id, Schema.RecordTypeInfo> rtInfos = WorkOrder_Logic.rtInfos;
    
    List<Workorder__c> calculateValues = new List<Workorder__c>();
            
    for(Workorder__c wOrder : Trigger.new){
        
        if(recordTypeNames.contains(rtInfos.get(wOrder.recordTypeId).getName())){
            
            if(Trigger.isInsert || wOrder.Work_Order_Value_INTL__c == null 
                || wOrder.RecordTypeId != Trigger.oldMap.get(wOrder.Id).RecordTypeId 
                || wOrder.Number_of_Software_Upgraded__c != Trigger.oldMap.get(wOrder.Id).Number_of_Software_Upgraded__c
                || wOrder.CurrencyIsoCode != Trigger.oldMap.get(wOrder.Id).CurrencyIsoCode){
                calculateValues.add(wOrder);
            }
        }           
    }
    
    if(calculateValues.size() > 0){
        WorkOrder_Logic.setWorkorderPrices(calculateValues);
    }
    
    //If we check the flag in the Custom Settings we skip the execution of this logic
    if(bl_Trigger_Deactivation.isTriggerDeactivated('WorkOrder_UPSERT_BEFORE')) return;
    
    Set<Id> surgicalRecordtypeIDs = WorkOrder_Logic.surgicalRecordtypeIDs;
    List<Workorder__c> lstSurgeryCoverageWorkOrder=new List<Workorder__c>();

    List<Workorder__c> lstWorkOrder_PM = new List<Workorder__c>();

    List<Workorder__c> lstFSEVisitWorkOrder=new List<Workorder__c>(); 
    List<Workorder__c> listWOForInsSoft = new List<Workorder__c>();  
    List<Workorder__c> lstCheckCoveredByWorkOrder=new List<Workorder__c>();   
    Set<Id> SurgeryCovRecTypeset = WorkOrder_Logic.surgeryCoverageRecordTypes;
    Boolean doNotExecuteFromWorkflow = false;
    Boolean doNotExecuteForReopen = false;
    Boolean doNotExecuteForReopenSC = false;
    String caseComplaint;
     
        
    for(Workorder__c objworkorder:Trigger.new)
    {
        //MJH added 1-9-14 to fix work orders being tied to cases with complaints and the complaint getting duplicated
        //check to see if the complaint is null and the case is not null
        
        //Modified by Wipro
        objworkorder.Is_Executing_Reopen_only_onceFollow2__c=objworkorder.Is_Executing_Reopen_only_onceFollow__c;
         if(
         		(objworkorder.Is_Failure_Resolved_App_Testing__c == 'No' || objworkorder.Is_Cable_Grade_Failure_Resolved__c == 'No'
                || objworkorder.Is_System_Inspection_Failure_Resolved__c == 'No' || objworkorder.Is_Compliance_Testing_Failure_Resolved__c == 'No'
                || objworkorder.Is_Failure_Resolved_on_Demo_Equip__c == 'No' || objworkorder.Is_Electrical_Insp_Failure_Resolved__c == 'No'
                || objworkorder.Is_General_Failure_Resolved__c == 'No' || objworkorder.Is_Image_Quality_Check_Failure_Resolved__c == 'No'
                || objworkorder.Is_Imaging_Failure_Resolved__c == 'No' || objworkorder.Is_Mechanical_Insp_Failure_Resolved__c == 'No'
                || objworkorder.Is_Mechanical_Tests_Failure_Resolved__c == 'No' || objworkorder.Is_Nav_Interface_Failure_Resolved__c == 'No'
                || objworkorder.Is_visual_inspection_failure_resolved__c == 'No' || objworkorder.Is_Hardware_Checkout_Failure_Resolved__c == 'No' 
                || objworkorder.Is_Software_Checkout_Failure_Resolved__c == 'No' || objworkorder.Is_Scanner_Config_Failure_Resolved__c == 'No'
                || objworkorder.Is_Nav_Checkout_Failure_Resolved__c == 'No' || objworkorder.Is_EM_Checkout_Failure_Resolved__c == 'No'
                || objworkorder.Is_Laser_Tests_Failure_Resolved__c  == 'No' || objworkorder.Is_Clean_Inspect_MVS_IAS_Fail_resolved__c  == 'No'
                || objworkorder.Is_IAS_Batteries_Failure_resolved__c  == 'No' || objworkorder.Is_Mechanical_Test_Failure_Resolved__c  == 'No'
                || objworkorder.Is_Planned_Maintenance_Failure_Resolved__c  == 'No' || objworkorder.Is_Autoguide_Checkout_Failure_Resolved__c == 'No'
				|| objworkorder.Is_MVS_IAS_Batteries_Failure_Resolved__c  == 'No') 
                && objworkorder.Status__c=='Completed') 
         {
             objworkorder.Is_Executing_Reopen_only_onceFollow__c=true;       
        }
        
        if((objworkorder.Case__c != null && String.valueOf(objworkorder.Case__c) != '') && objworkorder.Complaint__c == null )
            {
                for(Case complaintOnCase:[select Complaint__c from Case where Id = :objworkorder.Case__c])
                {
                    caseComplaint = complaintOnCase.Complaint__c;
                }
                //check to see if there is a complaint on the case and if there is then populate on the work order
                if(caseComplaint != null && String.valueOf(caseComplaint) != '')
                    objworkorder.Complaint__c = caseComplaint;
            }
        if(objworkorder.Related_Workorder__c != null && objworkorder.Complaint__c == null)
        {
            for(Workorder__c complaintOnWO:[select Complaint__c from Workorder__c where Id = :objworkorder.Related_Workorder__c])
                {
                    caseComplaint = complaintOnWO.Complaint__c;
                }
                //check to see if there is a complaint on the related WO and if there is then populate on the work order
                if(caseComplaint != null && String.valueOf(caseComplaint) != '')
                    objworkorder.Complaint__c = caseComplaint;
        }
        
        doNotExecuteFromWorkflow=objworkorder.Is_Executing_Workflow_Internal_use_only__c;
        doNotExecuteForReopenSC =objworkorder.Is_Executing_Reopen_use_only__c;
        doNotExecuteForReopen=objworkorder.Is_Executing_Reopen_only_once__c;
        

        if (surgicalRecordtypeIDs.contains(objworkorder.recordtypeid) && objworkorder.Status__c=='Completed' && objworkorder.Covered_Under__c=='Contract' && objworkorder.Contract__c==null){
            lstSurgeryCoverageWorkOrder.add(objworkorder);    
        }

        if ( WorkOrder_Logic.setID_RecordType_PM.contains(objworkorder.RecordTypeId) && objworkorder.Status__c == 'Completed' && objworkorder.Covered_Under__c == 'Contract' && objworkorder.Contract__c==null){
            lstWorkOrder_PM.add(objworkorder);    
        }

        if(objworkorder.Bio_Med_FSE_Visit__c=='Yes' && objworkorder.Status__c=='Completed' && objworkorder.Covered_Under__c=='Contract'){
            lstFSEVisitWorkOrder.add(objworkorder);        
             objworkorder.Is_Executing_Reopen_only_once__c=true;    
        }
        if(objworkorder.recordtypeid !=null){
            listWOForInsSoft.add(objworkorder);           
        }
        
        if( SurgeryCovRecTypeset!=null && !SurgeryCovRecTypeset.Contains(objworkorder.recordtypeid) && objworkorder.Status__c == 'Completed'
            && objworkorder.Covered_Under__c == 'Contract' && (objworkorder.Bio_Med_FSE_Visit__c == 'No'|| objworkorder.Bio_Med_FSE_Visit__c == null)){
                
                lstCheckCoveredByWorkOrder.add(objworkorder);
        }

        
    }

    if(lstFSEVisitWorkOrder.size()>0 && CaseLink.updateFlag==false)
    {
        if(doNotExecuteForReopen == false)
        {
             WorkOrder_Logic.FSEVisitDeduction(lstFSEVisitWorkOrder);
        }
    }
    if(lstSurgeryCoverageWorkOrder.size()>0 && CaseLink.updateFlag==false)
    {
    
        WorkOrder_Logic.SurgeryCoverageDeduction(lstSurgeryCoverageWorkOrder);

    }

    if (lstWorkOrder_PM.size() > 0 && CaseLink.updateFlag == false){

        WorkOrder_Logic.PMDeduction(lstWorkOrder_PM);

    }
   
    if (doNotExecuteFromWorkflow == true)
    { 
        // doNothing
    }
    else
    {
            
        WorkOrder_Logic.validateAutoguideRelatedSystem(Trigger.new);//Check if the Autoguide Assets are used on the proper fields
             
        if(listWOForInsSoft.size()>0){
            WorkOrder_Logic.validateSoftware(listWOForInsSoft);
            WorkOrder_Logic.UpdateFCASoftware(listWOForInsSoft);
        } 
        if(lstCheckCoveredByWorkOrder.size()>0){
            WorkOrder_Logic.CheckCoveredBy(lstCheckCoveredByWorkOrder);
        } 

    }

}