trigger Case_UPSERT_BEFORE on Case (before insert,before Update) 
{
	//Populate the External_Id__c field with a new Id if no value was set before.
	GuidUtil.populateMobileId(Trigger.new, 'External_Id__c');
	
	//If we check the flag in the Custom Settings we skip the execution of this logic
	if(bl_Trigger_Deactivation.isTriggerDeactivated('Case_UPSERT_BEFORE')) return;
	
	//Use this logic only to the Cases with Record Types related to ST Service Cases (New_Case, Closed_Case)
	List<Case> caseInScope = new List<Case>();
	
	for(Case caseRecord : Trigger.New){
		
		if(Case_Logic.caseRTInScope.contains(caseRecord.RecordTypeId)) caseInScope.add(caseRecord);		
	}
	
	if(caseInScope.size() == 0) return;
	    
    // BC - 20161117 - New logic for replaced logic below - START
    // Set the Case.Asset_Record_Type__c based on the RecordType of the Asset
    // Set the Case.Camera_Cart_Serial_Number__c based on the Camera_Cart_Serial_Number__c of the Asset
    Set<Id> setID_Asset = new Set<Id>();
    Set<Id> setID_Contact = new Set<Id>();
    
    Id openCaseRTId = clsUtil.getRecordTypeByDevName('Case', 'New_Case').Id;
    
    for (Case oCase : caseInScope){
    	
    	if((oCase.Patient_Impact__c == 'Yes' || oCase.Customer_Communicate_Dissatisfaction__c == 'Yes') && oCase.Complaint_Case__c != 'Yes'){
    		
    		if(oCase.Date_Complaint_Became_Known__c == null || oCase.Complaint_Handling_Notified_Date__c == null) oCase.addError('Based on entered data this Case will be marked as a Complaint Case. Please provide a value for \'Date Complaint Became Known\' and \'Submitted Date\'');
    		else oCase.Complaint_Case__c = 'Yes';    		
    	}    	
    	
        if (oCase.AssetId != null){
            setID_Asset.add(oCase.AssetId);
        }
        
        if(oCase.Initial_Reporter__c != null && oCase.RecordTypeId == openCaseRTId){
        	setID_Contact.add(oCase.Initial_Reporter__c);
        }
    }
    
    Map<Id, Asset> mapAsset = new Map<Id, Asset>([SELECT RecordType.Name, Camera_Cart_Serial_Number__c, Asset_Product_Type__c FROM Asset WHERE id = :setID_Asset]);
    Map<String, String> mapAssetCaseRecordTypeName = new Map<String, String>{'Navigation' => 'Navigation', 'Mazor' => 'Mazor', 'O-Arm' => 'O-Arm', 'PoleStar' => 'PoleStar', 'Visualase' => 'Visualase'};
    
    Map<Id, Contact> mapInitialReporter;
    if(setID_Contact.size() > 0) mapInitialReporter = new Map<Id, Contact>([Select Id, Email, Phone, MobilePhone, HomePhone, Primary_Job_Title_vs__c from Contact where Id IN :setID_Contact]);

    for (Case oCase : caseInScope){
    	
        if (mapAsset.containsKey(oCase.AssetId)){

            Asset oAsset = mapAsset.get(oCase.AssetId);

            if (mapAssetCaseRecordTypeName.containsKey(oAsset.RecordType.Name)){
                oCase.Asset_Record_Type__c = mapAssetCaseRecordTypeName.get(oAsset.RecordType.Name);
            }

            // Copy Camera_Cart_Serial_Number__c from Asset to Case if this field is empty on the Case
            //  This field should not be updated if it contains a value
            if ( (clsUtil.isBlank(oCase.Camera_Cart_Serial_Number__c)) && (!clsUtil.isBlank(oAsset.Camera_Cart_Serial_Number__c)) ){
                oCase.Camera_Cart_Serial_Number__c = oAsset.Camera_Cart_Serial_Number__c;
            }

        }

		if(oCase.Initial_Reporter__c != null && oCase.RecordTypeId == openCaseRTId){
			
			Contact initialReporter = mapInitialReporter.get(oCase.Initial_Reporter__c);
			
			oCase.Initial_Reporter_Email__c = initialReporter.Email;
			
			if(initialReporter.Phone != null) oCase.Initial_Reporter_Phone__c = initialReporter.Phone;
			else if (initialReporter.MobilePhone != null) oCase.Initial_Reporter_Phone__c = initialReporter.MobilePhone;
			else if(initialReporter.HomePhone != null) oCase.Initial_Reporter_Phone__c = initialReporter.HomePhone;
			else oCase.Initial_Reporter_Phone__c = null;
			
			oCase.Initial_Reporter_Occupation__c = initialReporter.Primary_Job_Title_vs__c;			
		}
    } 
    
    // Bart Caelen - 14-02-2017 - CR-11545 - START
    if (Trigger.isBefore && Trigger.isUpdate){
    	
        Case_Logic.calculateMeanTimeToRepair(caseInScope, Trigger.oldMap);        
    }
    // Bart Caelen - 14-02-2017 - CR-11545 - STOP
    
    // BC - 20161117 - New logic for replaced logic below - STOP
/*
	
	       
        if (Trigger.isUpdate)
        {
            map<Id, Case> caseNewMap = new map<Id, Case>() ;
            map<Id, Case> oldCaseMap = new map<Id, Case>();
            //Integer caseCount = 0;
            //if(recTypeSet.size() > 0 && recTypeSet != null)
            //{
                for(Case objCase:caseInScope) 
                {
                    caseNewMap.put(objCase.Id, objCase);
                    oldCaseMap.put(objCase.Id, Trigger.oldMap.get(objCase.Id));
                    //caseCount++;
                }
            //}
        if(caseNewMap.size() > 0)
            case_Logic.UpdateComplaintFromCase(caseNewMap, oldCaseMap);
        }


        // BC - 20161117 - Replaced logic with logic above - START
        //Set system record type picklist to drive work order create list
        for(Case objCase:caseInScope) 
        {
            List<Asset> systemRecordType = [Select RecordType.Name from Asset where id = :objCase.AssetId];
            if(systemRecordType.size()>0)
            {
                if(systemRecordType[0].RecordType.Name == 'Navigation')
                    objCase.Asset_Record_Type__c = 'Navigation';
                else if(systemRecordType[0].RecordType.Name == 'O-Arm')
                    objCase.Asset_Record_Type__c = 'O-Arm';
                else if(systemRecordType[0].RecordType.Name == 'PoleStar')
                    objCase.Asset_Record_Type__c = 'PoleStar';
                else if(systemRecordType[0].RecordType.Name == 'Visualase')
                    objCase.Asset_Record_Type__c = 'Visualase';
            }
        } 
        // BC - 20161117 - Replaced logic with logic above - STOP
*/


}