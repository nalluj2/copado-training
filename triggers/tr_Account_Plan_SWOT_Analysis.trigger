trigger tr_Account_Plan_SWOT_Analysis on Account_Plan_SWOT_Analysis__c (before insert, before update) {
    
    GuidUtil.populateMobileId(Trigger.new, 'Mobile_ID__c');
    
    // For Department check
	Set<Id> setID_AccountPlan = new Set<Id>();
    
    for(Account_Plan_SWOT_Analysis__c swot : Trigger.new){
    	
    	setID_AccountPlan.add(swot.Account_Plan__c);
    	
    	swot.Unique_Key__c = swot.Account_Plan__c + ':' + swot.Department__c;
    }
    
    Map<Id, Map<String, DIB_Department__c>> mapAccountDepartments = new Map<Id, Map<String, DIB_Department__c>>();
	Map<Id, Id> accPlanAccount = new Map<Id, Id>();
			
	for(Account_Plan_2__c accPlan : [Select Id, Account__c from Account_Plan_2__c where Id IN :setID_AccountPlan]){
		
		accPlanAccount.put(accPlan.Id, accPlan.Account__c);
	}
	
	for(Account acc : [Select Id, (Select Id, Department_ID__r.Name from Segmentations__r where Active__c = true) from Account where Id IN (Select Account__c from Account_Plan_2__c where Id IN :setID_AccountPlan)]){
			
		Map<String, DIB_Department__c> accDepartments = new Map<String, DIB_Department__c>();
		
		for(DIB_Department__c department : acc.Segmentations__r){	
			
			accDepartments.put(department.Department_ID__r.Name, department);
		}
		
		mapAccountDepartments.put(acc.Id, accDepartments);
	}		
	
	for(Account_Plan_SWOT_Analysis__c swot : Trigger.new){
			
		Map<String, DIB_Department__c> accDepartments = mapAccountDepartments.get(accPlanAccount.get(swot.Account_Plan__c));
		
		if(accDepartments.containsKey(swot.Department__c)){
			
			swot.Account_Department__c = accDepartments.get(swot.Department__c).Id;
				
		}else if(accDepartments.containsKey('Adult/Pediatric')){
			
			swot.Account_Department__c = accDepartments.get('Adult/Pediatric').Id;
			
		}else{
			
			swot.addError('The selected Department is not available for this Account. Please choose another Department or create a Diabetes Segmentation record for the Department first.');				
		}				
	}	
}