//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 05-01-2016
//  Description      : APEX Trigger on SVMXC__Service_Order__c 
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
trigger tr_SVMXC_ServiceOrder on SVMXC__Service_Order__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
	
	if (Trigger.isBefore){

		if (Trigger.isInsert){
			
			bl_SVMXC_ServiceOrder_Trigger.debugServiceOrderFields(Trigger.new);
			
			bl_SVMXC_ServiceOrder_Trigger.alignBillingTypeAndMisuseIndicator(Trigger.new, Trigger.oldMap);
			
			bl_SVMXC_ServiceOrder_Trigger.populateTechnicianTeamInfo(Trigger.new);

			bl_SVMXC_ServiceOrder_Trigger.onBeforeInsert(Trigger.new);

			bl_SVMXC_ServiceOrder_Trigger.copyAccountCountry(Trigger.new, Trigger.oldMap, 'INSERT');
			
			bl_SVMXC_ServiceOrder_Trigger.copyInstalledProductData(Trigger.new, Trigger.oldMap);
			
			bl_SVMXC_ServiceOrder_Trigger.setLockedOnCompletion(Trigger.new);
			
			bl_SVMXC_ServiceOrder_Trigger.clearWhenOffHold(Trigger.new);
			
			bl_SVMXC_ServiceOrder_Trigger.clearWhenOffHold2(Trigger.new);
			
			bl_SVMXC_ServiceOrder_Trigger.copyScheduledDate(Trigger.new, Trigger.oldMap);

		}else if (Trigger.isUpdate){
			
			bl_SVMXC_ServiceOrder_Trigger.copyScheduledDate(Trigger.new, Trigger.oldMap);
			
			bl_SVMXC_ServiceOrder_Trigger.incorporateProblemDescriptionComment(Trigger.new);
			
			bl_SVMXC_ServiceOrder_Trigger.debugServiceOrderFields(Trigger.new);
			
			bl_SVMXC_ServiceOrder_Trigger.alignBillingTypeAndMisuseIndicator(Trigger.new, Trigger.oldMap);

			bl_SVMXC_ServiceOrder_Trigger.preventUpdateOnRestrictedFields(Trigger.new, Trigger.oldMap, 'UPDATE');
			
			bl_SVMXC_ServiceOrder_Trigger.updateTechnicianTeamInfo(Trigger.new, Trigger.oldMap);

			bl_SVMXC_ServiceOrder_Trigger.validateCompleted(Trigger.new, trigger.newMap, Trigger.oldMap, 'UPDATE');
			
			bl_SVMXC_ServiceOrder_Trigger.copyAccountCountry(Trigger.new, Trigger.oldMap, 'UPDATE');
			
			bl_SVMXC_ServiceOrder_Trigger.copyInstalledProductData(Trigger.new, Trigger.oldMap);

			bl_SVMXC_ServiceOrder_Trigger.onBeforeUpdate(Trigger.newMap, Trigger.oldMap);
			
			bl_SVMXC_ServiceOrder_Trigger.setLockedOnCompletion(Trigger.new);
			
			bl_SVMXC_ServiceOrder_Trigger.clearWhenOffHold(Trigger.new);
			
			bl_SVMXC_ServiceOrder_Trigger.clearWhenOffHold2(Trigger.new);
			
			// Servicemax, Inc, 29/09/2020 (dd/mm/yyyy), call method to automatically remove the Parent WO relationshipon child WOs
			bl_SVMXC_ServiceOrder_Trigger.updateParentWORelationship(Trigger.newMap, Trigger.oldMap);
					
		}else if (Trigger.isDelete){
			// ALL BEFORE DELETE ACTIONS

		}

	}else if (Trigger.isAfter){

		if (Trigger.isInsert){
			
			// ALL AFTER INSERT ACTIONS			
			bl_SVMXC_ServiceOrder_Trigger.createDefaultOperation(Trigger.new);
						
			bl_SVMXC_ServiceOrder_Trigger.manageWorkOrderEvent(Trigger.new, Trigger.oldMap);
			
			bl_SVMXC_ServiceOrder_Trigger.onAfterInsert(Trigger.newMap);
			
		}else if (Trigger.isUpdate){
			
			// ALL AFTER UPDATE ACTIONS			
			bl_SVMXC_ServiceOrder_Trigger.manageWorkOrderEvent(Trigger.new, Trigger.oldMap);		
			
			bl_SVMXC_ServiceOrder_Trigger.notificationSAP(Trigger.new, Trigger.oldMap, 'UPDATE');

			bl_SVMXC_ServiceOrder_Trigger.updateInstallProduct(Trigger.new, Trigger.oldMap);

			bl_SVMXC_ServiceOrder_Trigger.updateInstallProduct_PM(Trigger.new, Trigger.oldMap);

			bl_SVMXC_ServiceOrder_Trigger.onAfterUpdate(Trigger.newMap, Trigger.oldMap);

			bl_SVMXC_ServiceOrder_Trigger.sendEmail(Trigger.new, Trigger.oldMap, 'UPDATE');

		}else if (Trigger.isDelete){
			// ALL AFTER DELETE ACTIONS

		}else if (Trigger.isUnDelete){
			// ALL AFTER UNDELETE ACTIONS

		}

	}

}
//--------------------------------------------------------------------------------------------------------------------