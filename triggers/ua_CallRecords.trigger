/*
*	Trigger to log user actions on Account (create / update/ delete)
* 	Author: Rudy De Coninck
*	Creation date: 17-12-2014
*/


trigger ua_CallRecords on Call_Records__c (after delete, after insert, after undelete, after update) {
	
	//We do not track bulk operations
	if (Trigger.size == 1){
		String operation='none';
		List<Call_Records__c> recordsToProcess = new List<Call_Records__c>();
		if (Trigger.isDelete){
			operation = 'Delete';		
			for (Call_Records__c a :Trigger.old){
				recordsToProcess.add(a);				
			}  
		}else{
			if (Trigger.isInsert){
				operation = 'Insert';
			}
			if(Trigger.isUpdate){
				operation='Update';
			}
			for (Call_Records__c a :Trigger.new){
				recordsToProcess.add(a);				
			}  
		}
		if(recordsToProcess.size()>0){
			for(Call_Records__c a : recordsToProcess){
				//Only log initial action
				if (!bl_UserAction.userActionInProgress){
					bl_UserAction.logObject(UserInfo.getUserId(), operation,'Call_Records__c',a.id,'');
					bl_UserAction.userActionInProgress = true;  
				} 
			}
		}
	} 

}