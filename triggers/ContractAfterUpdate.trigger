trigger ContractAfterUpdate on Contract (after update, after delete, after undelete) {
	
	//If this update was triggered by the re-calculation due to modification of child Asset Contract record, we don't need to execute this code 
	if (bl_Contract.assetContractCalculationInProcess == true) return;
		
	if (trigger.isUnDelete || trigger.isUpdate){
		
		List<AssetContract__c> lstAssetContract = new List<AssetContract__c>([SELECT Asset__c, Contract__c, Contract__r.Status FROM AssetContract__c WHERE Contract__c IN : trigger.newMap.keySet() AND ( (Asset__r.Status not in ('Obsolete', 'Removed', 'Returned to US')) OR (Contract__c != null AND Contract__r.Status in ('Expired', 'Canceled')) )] );
		if (lstAssetContract.size() > 0) update lstAssetContract;	// This will trigger the code to update the Assets if the Contract changed its terms

	}else if(trigger.isDelete){	
		
		List<AssetContract__c> lstAssetContract = new List<AssetContract__c>([SELECT Asset__c, Contract__c, Contract__r.Status  FROM AssetContract__c WHERE Contract__c IN : trigger.oldMap.keySet() AND ( (Asset__r.Status not in ('Obsolete', 'Removed', 'Returned to US')) OR (Contract__c != null AND Contract__r.Status in ('Expired', 'Canceled')) ) ALL ROWS]);
		
		Set<Id> setID_Asset = new Set<Id>();
		Set<Id> setID_Asset_ContractNotActive = new Set<Id>();
		for(AssetContract__c oAssetContract : lstAssetContract){
			
			if (oAssetContract.Contract__r.Status == 'Expired' || oAssetContract.Contract__r.Status == 'Canceled'){

				setID_Asset_ContractNotActive.add(oAssetContract.Asset__c);

			}else{

				setID_Asset.add(oAssetContract.Asset__c);

			}

		}
		
		bl_Contract.calculateCurrentContract(setID_Asset);
		bl_Contract.calculateContractNotActive(setID_Asset_ContractNotActive);

	}

}