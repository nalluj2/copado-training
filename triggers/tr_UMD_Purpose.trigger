trigger tr_UMD_Purpose on UMD_Purpose__c (before insert, before update, after insert) {
	
	if(Trigger.isBefore){
	
		if(Trigger.isInsert){
			
			bl_UMD_Purpose_Trigger.setUniqueKeys(Trigger.new);
			
		}else if(Trigger.isUpdate){
			
			bl_UMD_Purpose_Trigger.setUniqueKeys(Trigger.new);
			
			bl_UMD_Purpose_Trigger.blockUpdateFY(Trigger.new, Trigger.oldMap);
		}
		
	}else{
		
		if(Trigger.isInsert){
			
			bl_UMD_Purpose_Trigger.cloneWithQuestions(Trigger.new);
		}		
	}	 
}