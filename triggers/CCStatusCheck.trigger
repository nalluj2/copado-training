trigger CCStatusCheck  on CampaignMember (after update) {

    map<id,integer> mapofcmbridandindex=new map<id,integer>();
    set<id> setCmMembers = new set<id>();
    for(integer i=0;i<trigger.new.size();i++){
        mapofcmbridandindex.put(trigger.new[i].id,i);
        if(trigger.new[i].CC_Status__c=='Feedback Call Completed' && trigger.new[i].CC_Status__c!=trigger.old[i].CC_Status__c){
        	setCmMembers.add(trigger.new[i].id);	
        }
    }

    List<Campaignmember> lstcmpmbr=new List<Campaignmember>();
    lstcmpmbr=[select id,CampaignID,CC_Status__c,Campaign.status from campaignmember where id=:trigger.newmap.keyset()];

    for(campaignmember cmp:lstcmpmbr){
        if(cmp.Campaign.status!='Invitation Callers Assigned' && cmp.CC_Status__c=='Invitation Call Declined'){
            trigger.new[mapofcmbridandindex.get(cmp.id)].CC_Status__c.adderror('In order to make the status Invitation Call Declined Campaign status should be Invitation Callers Assigned ');
        }
        if(cmp.Campaign.status!='Invitation Callers Assigned' && cmp.CC_Status__c=='Invitation Call Accepted'){
            trigger.new[mapofcmbridandindex.get(cmp.id)].CC_Status__c.adderror('In order to make the status Invitation Call Accepted Campaign status should be Invitation Callers Assigned ');
        }
    }
	
	// Start Campaign Members task creation
	   List<CampaignMember> lstcmpgnmbrsexist=[select id,CampaignId,Invitation_Caller__c,Feedback_Received__c from CampaignMember where id in:setCmMembers and Invitation_Caller__c!=null];
	   System.Debug('lstcmpgnmbrsexist-'+lstcmpgnmbrsexist);
   	   list<task> lsttaskfeedback=new list<task>();	   
	   for(CampaignMember cm:lstcmpgnmbrsexist){
            Task t=new Task();
            t.status = 'Not Started';                                    
            t.priority = 'High';
            t.subject='Contact Action Plan';
            t.whatid=cm.CampaignId;
            t.ownerid=cm.Invitation_Caller__c;
            t.ActivityDate=Date.Today()+7;
            t.IsReminderSet = true;
            t.ReminderDateTime=Date.Today()+3;
            if(cm.Feedback_Received__c==null){
            	t.Description=' ';
            } else {
            	t.Description=cm.Feedback_Received__c;            	
            }
            lsttaskfeedback.add(t);
            system.debug('>>>>>lsttask - '+t);	   	
	   }
	   // Send email to task owner & also sent Feedback_task_generated__c flag to true so that for these memebers shoudl not get tasks.
	   if(lsttaskfeedback.size()>0){ 
		    //insert lsttaskfeedback;
		    Database.DMLOptions dmlo = new Database.DMLOptions();
		    dmlo.EmailHeader.triggerUserEmail = true;
		    database.insert(lsttaskfeedback, dmlo);
		    System.Debug('lsttaskfeedback - ' + lsttaskfeedback + ' dmlo- ' + dmlo);  
	   }	   
		
	// End Campaign Members task creation
	
}