/*
 * Work Item Nr		: WI-198 / WI-215
 * Description		: WI-198: Add owner of account plan to account plan team member
 *					  WI-215: For iPlan2 created Account Plans:
 *							  1. CVG Account plan records, get iPlan2 SBU Records and update reference to CVG iPlan2
 *							  2. CVG Account plan records, get iPlan1 SBU Records and update reference to CVG iPlan2
 *							  3. SBU Account plan records, get iPlan2 CVG Records and update reference to CVG iPlan2 
 * Author        	: Patrick Brinksma
 * Created Date    	: 22-07-2013
 *
 * Revision 		: Christophe Saenen
 *					: 02-05-2016 
 *					: CR 11736
 */
 
trigger tr_AccountPlan2_AfterInsertUpdate on Account_Plan_2__c (after insert, after update) {
	
	if(Triggers_Soft_Deactivation.tr_AccountPlan2_AfterInsertUpdate == true) return;
	
	if(Trigger.isInsert) {
		
		bl_AccountPlanning.doAccPlanBugSbuProcess(Trigger.new);	
		bl_AccountPlanning.doAccPlanBugBuProcess(Trigger.new);	
	} 
	
	bl_AccountPlanning.doAccPlanTeamMembersProcess(Trigger.new, Trigger.oldMap);
	bl_AccountPlanning.doAppUsageProcess(Trigger.new, Trigger.old);	
}