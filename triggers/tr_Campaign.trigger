//--------------------------------------------------------------------------------------------------------------------
//	Author 			: Bart Caelen
//  Created Date 	: 30-04-2018
//  Description 	: APEX Trigger on Campaign
//  Change Log 		: 
//--------------------------------------------------------------------------------------------------------------------
trigger tr_Campaign on Campaign (before insert, before update, after insert, after update) {

	if (Trigger.isBefore){

		if (Trigger.isInsert){

			bl_Campaign_Trigger.updateBusinessUnit(Trigger.new, Trigger.oldMap);

			bl_Campaign_Trigger.preventDuplicateCampaign_SBU(Trigger.new, Trigger.oldMap);
			
			bl_Campaign_Trigger.validateCampaign(Trigger.new, Trigger.oldMap);

			bl_Campaign_Trigger.populateFields(Trigger.new, Trigger.oldMap);

			bl_Campaign_Trigger.setCampaignActiveStatus(Trigger.new, Trigger.oldMap);

		}else if (Trigger.isUpdate){

			bl_Campaign_Trigger.updateBusinessUnit(Trigger.new, Trigger.oldMap);

			bl_Campaign_Trigger.preventDuplicateCampaign_SBU(Trigger.new, Trigger.oldMap);

			bl_Campaign_Trigger.validateCampaign(Trigger.new, Trigger.oldMap);

			bl_Campaign_Trigger.populateFields(Trigger.new, Trigger.oldMap);

			bl_Campaign_Trigger.setCampaignActiveStatus(Trigger.new, Trigger.oldMap);

		}

	}else if (Trigger.isAfter){

		if (Trigger.isInsert){

			bl_Campaign_Trigger.createCalendarEvent(Trigger.new);

		}else if (Trigger.isUpdate){

			bl_Campaign_Trigger.updateCalendarEvent(Trigger.newMap, Trigger.oldMap);

		}

	}

}
//--------------------------------------------------------------------------------------------------------------------