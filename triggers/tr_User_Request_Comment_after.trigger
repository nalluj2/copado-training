trigger tr_User_Request_Comment_after on User_Request_Comment__c (after insert) {

    Set<Id> requestIds = new Set<Id>();
    
    for(User_Request_Comment__c comment : Trigger.new){
        requestIds.add(comment.User_Request__c);
    }
    
    Map<Id, Create_User_Request__c> requestsById = new Map<Id, Create_User_Request__c>([Select Id, Requestor_Email__c, Manager_Email__c, 
                                                                Data_Load_Approvers__c, Application__c, Request_Type__c, Status__c 
                                                                from Create_User_Request__c where Id IN: requestIds]);  
         
    Map<Id, Set<String>> sendToEmails = new Map<Id, Set<String>>();
    Set<String> allEmails = new Set<String>();
    
    for(User_Request_Comment__c comment : Trigger.new){
    
        Set<String> commentEmails = new Set<String>();
        sendToEmails.put(comment.Id, commentEmails);
        
        Create_User_Request__c request = requestsById.get(comment.User_Request__c);
            
        commentEmails.addAll(bl_User_Request_Comment.isAdminUser(comment, request));
            
        commentEmails.addAll(bl_User_Request_Comment.isRequestor(comment, request));
        
        commentEmails.addAll(bl_User_Request_Comment.isApprover(comment, request));
            
        allEmails.addAll(commentEmails);        
    }   
        
    Map<String, Contact> dummyContactsByEmail = bl_User_Request_Comment.createDummyContacts(allEmails);
    
    EmailTemplate commentTemplate = [select Id from EmailTemplate where DeveloperName='Request_Comment_Created'];
                       
    List<Messaging.Singleemailmessage> mails = new List<Messaging.Singleemailmessage>();
    
    for(User_Request_Comment__c comment : Trigger.new){
        
        Set<String> emails = sendToEmails.get(comment.Id);
        
        for(String contactEmail : emails){    
            
            Contact contact = dummyContactsByEmail.get(contactEmail);
                 
            Messaging.Singleemailmessage mail = new Messaging.Singleemailmessage();
           
            mail.setTargetObjectId(contact.Id);
            mail.setTemplateId(commentTemplate.Id);           
            mail.setUseSignature(false);
            mail.setSaveAsActivity(false);
            mail.setWhatId(comment.Id);
            
            mails.add(mail);
        }    
    }
    
    // Send the email
    if (clsUtil.bEmailDeliverabilityEnabled()) Messaging.sendEmail(mails);
    
    Triggers_Soft_Deactivation.AffiliationBeforeDelete = true;
    delete dummyContactsByEmail.values();                         
    Triggers_Soft_Deactivation.AffiliationBeforeDelete = false;
}