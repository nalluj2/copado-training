trigger tr_Implant on Implant__c (after delete, after insert, after undelete, after update){

	if (trigger.isAfter){

		if (Trigger.isInsert){


			bl_Implant_Trigger.countImplants('INSERT', Trigger.new);
			bl_Implant_Trigger.createCallRecord('INSERT', Trigger.new);
			bl_Implant_Trigger.setExecutionVariable('INSERT', false);


		}else if (Trigger.isUpdate){

			bl_Implant_Trigger.countImplants('UPDATE', Trigger.new);
			bl_Implant_Trigger.createCallRecord('UPDATE', Trigger.new);
			bl_Implant_Trigger.updateCallRecord(Trigger.new, Trigger.oldMap);
			bl_Implant_Trigger.setExecutionVariable('UPDATE', false);

		}else if (Trigger.isDelete){


			bl_Implant_Trigger.countImplants('INSERT', Trigger.old);


		}else if (Trigger.isUndelete){


			bl_Implant_Trigger.countImplants('INSERT', Trigger.old);


		}

	}

}