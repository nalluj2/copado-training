/*
    Apex Trigger:trgUpdateBUCaseRecorTypeField
    Description: This trigger is used to autofill the Business unit and Therapy lookups before inserting and updating the case records.
    Author - Manish Kumar Srivastava
    Created Date  - 11/09/2011
    ------------------------
    Description     :   If there are 1 or more Case RecordType IDs defined on Sub-Business Unit and the processing Case
                            is of one of the defined RecordTypes, we need to link the Case to this Sub-Business Unit
    Author          :   Bart Caelen  
    Date            :   2014-03-19
    CR              :   CR-3780
    Version         :   1.1
*/
trigger trgUpdateBUCaseRecorTypeField on Case (before insert,before update, after insert, after update) {

    //-BC - 20140401 - START
    // There are some test classes on Case which performs many updates on Cases and therefor this class is called many times and generates a Too-many-SOQL-Error.
    // Whith the clsUtil.bDoNotExecute we can skip APEX code if needed
    if (clsUtil.bDoNotExecute){
        return;
    }
    //-BC - 20140401 - STOP

    if (trigger.isAfter){

        if (trigger.isInsert){

            //-BC - 20150415 - CR-8394 - START
            // Moved this logic to the After Insert trigger because of an issue when the internal comment was entered upon creation of the case (insufficient privileges)
            // We also need to change the OMA Case Owner with an @Future update and therefor we need the Case ID which is only available in the After Insert trigger
            if(bl_tr_CaseTriggerActions.checkBeforeUpdate==false){
                bl_tr_CaseTriggerActions.updateOMAFields(Trigger.new, null);
                bl_tr_CaseTriggerActions.checkBeforeUpdate=true;
            }
            //-BC - 20150415 - CR-8394 - STOP			
        }
        
        //Logic moved from tr_InsertQuestionAndAnswers to ensure order of execution
		bl_tr_InsertQuestionAndAnswers.getInsertQAs(Trigger.New);
       	bl_tr_UpdateOMASearchKey.OMASearckKey(Trigger.New);		

    }else if (trigger.isBefore){
        //Sart: Manish: OMA: To validate Case Account and Contact according Relationship object
        if(Trigger.isInsert){
            bl_tr_CaseTriggerActions.validateAccountContactAreC2ARelationship(Trigger.new);
        }

        if(Trigger.isUpdate){
            List<Case> lstCase=new List<Case>();
            case oldCase;
            for(case newCase: Trigger.new){
                oldCase=Trigger.oldmap.get(newCase.id);
                if((newCase.Accountid!=oldCase.Accountid)||(newCase.Contactid!=oldCase.Contactid)){
                    lstCase.add(newCase);
                }
            }
            if(lstCase.size()>0){
                bl_tr_CaseTriggerActions.validateAccountContactAreC2ARelationship(lstCase);
            }
            if(bl_tr_CaseTriggerActions.checkBeforeUpdate==false){
                //-BC - 20150309 - CR-6846 - START
                bl_tr_CaseTriggerActions.updateOMAFields(Trigger.new, Trigger.oldmap);
    //            bl_tr_CaseTriggerActions.updateOMAFields(Trigger.new);
                //-BC - 20150309 - CR-6846 - STOP
                bl_tr_CaseTriggerActions.checkBeforeUpdate=true;
            }
        }
        //End: Manish: OMA: To validate Case Account and Contact according Relationship object
        //Start:Kaushal:OMA: To update Business hours on Case based on user's timezone.
        bl_tr_UpdateBusinessHours.BusinessHoursLogic(Trigger.new);
        //End:Kaushal:OMA: To update Business hours on Case based on user's timezone.
        List<Case> lstCase=new List<Case>();
        map<string,id> map_TherapyidTherapyName=new map<string,id>();
        map<string,id> map_TherapyBUCompWithid=new map<string,id>();
        List<string> lstCaseTherapyPicklistValue=new List<String>();
        string s;
        
        List<Business_Unit__c> lstBu = bl_Case.lstBu;

        //-BC - 20140319 - CR-3780 - Collect a map with the Case Recordtype ID and the corresponding Sub Business Unit ID - START
        List<Sub_Business_Units__c> lstSBU = bl_Case.lstSBU;            

        Set<Id> setID_Case = new Set<Id>();
        Map<Id, Id> mapCaseRecordTypeID_SBUID = new Map<Id, Id>();
        for (Sub_Business_Units__c oSBU : lstSBU){
            String tCaseRecordTypeIDs = oSBU.Case_Record_Type_ID__c;
            if (tCaseRecordTypeIDs != null){
                List<Id> lstID = tCaseRecordTypeIDs.split(',');
                setID_Case = new Set<Id>();
                setID_Case.addAll(lstID);

                for (Id idCase : setID_Case){
                    mapCaseRecordTypeID_SBUID.put(idCase, oSBU.ID);
                }

            }
        }
        //-BC - 20140319 - CR-3780 - Collect a map with the Case Recordtype ID and the corresponding Sub Business Unit ID - STOP
        

        for(Case cs:Trigger.new)
        {
            if(cs.Therapy_Picklist__c !=null && cs.Therapy_Picklist__c != '') lstCaseTherapyPicklistValue.add(cs.Therapy_Picklist__c);
        }
        
        if(lstCaseTherapyPicklistValue.size() > 0){
	        
	        List<Therapy__c> lstTherapy=[
	                                     select id,name,Business_Unit__r.name,Business_Unit__c,Company_Id__c 
	                                     from Therapy__c 
	                                     where name in:lstCaseTherapyPicklistValue
	                                    ];
	      
	        for(Therapy__c t:lstTherapy)
	        {
	            string companyid;
	            companyid=t.Company_Id__c.substring(0,15);
	            map_TherapyBUCompWithid.put(t.Business_Unit__c+companyid,t.id);
	        }
        }
        
        for(Case o:Trigger.new)
        {  
            if(o.Therapy_Picklist__c==null || o.Therapy_Picklist__c=='')
            {
                o.Therapy_Lookup__c=null;
            }
            if(o.recordtypeid!=null)
            {
                //-BC - 20140319 - CR-3780 - Get the Sub Business Unit for the Case Record Type Id - START
                Id id_CaseRecordType = o.recordtypeid;
                if (mapCaseRecordTypeID_SBUID.containsKey(id_CaseRecordType)){
                    o.Sub_Business_Unit_Lookup__c = mapCaseRecordTypeID_SBUID.get(id_CaseRecordType);
                }
                //-BC - 20140319 - CR-3780 - Get the Sub Business Unit for the Case Record Type Id - STOP

                s=string.valueof(o.recordtypeid).substring(0,15);
                for(Business_Unit__c b:lstBu)
                {
                    if(b.Case_Record_Types__c!=null)
                    {
                        if(b.Case_Record_Types__c.contains(s))
                        {
                            o.Business_Unit_Lookup__c=b.id;
                            if(map_TherapyBUCompWithid.get(string.valueof(b.id)+string.valueof(b.Company__c).substring(0,15))!=null)
                            {
                                o.Therapy_Lookup__c=map_TherapyBUCompWithid.get(string.valueof(b.id)+string.valueof(b.Company__c).substring(0,15));
                            }
                        }  
                    }
                } 
            }  
        }
    } 
}