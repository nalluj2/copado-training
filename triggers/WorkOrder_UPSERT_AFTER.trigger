/*
  *  Trigger Name  : WorkOrder_UPSERT_AFTER
  *  Description   : This Trigger execute the methods from WorkOrder_Logic class.
  *  Created Date  : 30/7/2013
  *  Author        : Wipro Tech.
  *Modified by     :REQ004277--Wipro(Pavan)Fix WO Re-open to not double deduct surgery coverages.
*/

trigger WorkOrder_UPSERT_AFTER on Workorder__c (after insert,after Update) {
    
    //If we check the flag in the Custom Settings we skip the execution of this logic
	if(bl_Trigger_Deactivation.isTriggerDeactivated('WorkOrder_UPSERT_AFTER')) return;
    
    if(bl_SynchronizationService_Utils.isSyncProcess == false){
		
		if(Trigger.isInsert) bl_SynchronizationService_Source.createNotificationIfChanged(Trigger.New, null);
		else bl_SynchronizationService_Source.createNotificationIfChanged(Trigger.New, Trigger.OldMap);	
	}
    
    System.debug('AFTER TRIGGER BEGINS ---->'+Workorder_Logic.isWORecursive);
    //List<Workorder__c> listWOForInsSoft = new List<Workorder__c>(); Out of scope for integration
    List<Workorder__c> listWOForUpdateSys = new List<Workorder__c>();    
    List<Workorder__c> listWOForUpdateTrackParts = new List<Workorder__c>();
    List<Workorder__c> listFollowUpWO = new List<Workorder__c>();
    List<Workorder__c> listOfWOforComplaint = new List<Workorder__c>();
    List<Workorder__c> lstOfWoFailureToCreateComplaint=new List<Workorder__c>();
    map<id,Workorder__c>  mapComplaintIdWOObj= new map<id,Workorder__c>(); 
    Integer i=0;
    Boolean doNotExecuteFromWorkflowinAfter = false;
    Boolean doNotExecuteForReopenAfter = false;
    System.debug('variable doNotExecuteFromWorkflowinAfter >>>' + doNotExecuteFromWorkflowinAfter);
    /*Set<Id> navRTs = new Set<Id>();    
    navRTs.add(RecordTypeMedtronic.getRecordTypeByDevName('Workorder__c', 'Visualase_System_Installation').Id);    
    navRTs.add(RecordTypeMedtronic.getRecordTypeByDevName('Workorder__c', 'Nav_System_Installation_Closed').Id);
    */
        
   //Modified by Wipro Tech--
    if(Workorder_Logic.isWORecursive)
    {
        Workorder_Logic.isWORecursive=false;
         System.debug('AFTER TRIGGER BEGINS 123 ---->'+Workorder_Logic.isWORecursive);
     for(Workorder__c objworkorder:Trigger.new)
     {
        
        System.debug('Inside for loop of AFTER TRIGGER');
       // doNotExecuteFromWorkflowinAfter = objworkorder.Is_Executing_Workflow_Internal_use_only__c;
       doNotExecuteFromWorkflowinAfter = objworkorder.Is_Executing_Workflow_Internal_once_only__c;
        doNotExecuteForReopenAfter=objworkorder.Is_Executing_Reopen_only_onceFollow2__c;
      //  objworkorder.Is_Executing_Reopen_use_only__c= false;
        System.debug('variable doNotExecuteFromWorkflowinAfter >>> '+ doNotExecuteFromWorkflowinAfter);
        /* Out of scope for integration
        if(objworkorder.recordtypeid !=null && objworkorder.Status__c=='Completed'){
            listWOForInsSoft.add(objworkorder);           
        }*/
        if(objworkorder.recordtypeid != null && objworkorder.Status__c == 'Completed' || (Trigger.isUpdate && Trigger.oldMap.get(objworkorder.Id).Status__c == 'Completed')){
            listWOForUpdateSys.add(objworkorder);          
        }
        if(objworkorder.Date_Completed__c != null){
            listWOForUpdateTrackParts.add(objworkorder);          
        }
        if(Trigger.isUpdate){
            System.debug('Trigger isUpdate AFTER TRIGGER');
            if(objworkorder.Status__c !='Completed' && Trigger.old[i].Status__c == 'Completed' && objworkorder.Complaint__c != Null && String.valueOf(objworkorder.Complaint__c)!=''){
                mapComplaintIdWOObj.put(objworkorder.Complaint__c,objworkorder);
                System.debug('Calling Re-Open in AFTER TRIGGER and mapComplaintIdWOObj size = ' +mapComplaintIdWOObj.size());
                if(mapComplaintIdWOObj.size() > 0){
                    system.debug('Inside AFTER TRIGGER call function ReopenComplaintFromWO');         
                    WorkOrder_Logic.ReopenComplaintFromWO(mapComplaintIdWOObj); 
                }
            }
        }
        
        if((objworkorder.Is_Failure_Resolved_App_Testing__c == 'No' || objworkorder.Is_Cable_Grade_Failure_Resolved__c == 'No'
                || objworkorder.Is_System_Inspection_Failure_Resolved__c == 'No' || objworkorder.Is_Compliance_Testing_Failure_Resolved__c == 'No'
                || objworkorder.Is_Failure_Resolved_on_Demo_Equip__c == 'No' || objworkorder.Is_Electrical_Insp_Failure_Resolved__c == 'No'
                || objworkorder.Is_General_Failure_Resolved__c == 'No' || objworkorder.Is_Image_Quality_Check_Failure_Resolved__c == 'No'
                || objworkorder.Is_Imaging_Failure_Resolved__c == 'No' || objworkorder.Is_Mechanical_Insp_Failure_Resolved__c == 'No'
                || objworkorder.Is_Mechanical_Tests_Failure_Resolved__c == 'No' || objworkorder.Is_Nav_Interface_Failure_Resolved__c == 'No'
                || objworkorder.Is_visual_inspection_failure_resolved__c == 'No' || objworkorder.Is_Hardware_Checkout_Failure_Resolved__c == 'No' 
                || objworkorder.Is_Software_Checkout_Failure_Resolved__c == 'No' || objworkorder.Is_Scanner_Config_Failure_Resolved__c == 'No'
                || objworkorder.Is_Nav_Checkout_Failure_Resolved__c == 'No' || objworkorder.Is_EM_Checkout_Failure_Resolved__c == 'No'
                || objworkorder.Is_Laser_Tests_Failure_Resolved__c  == 'No' || objworkorder.Is_Clean_Inspect_MVS_IAS_Fail_resolved__c  == 'No'
                || objworkorder.Is_IAS_Batteries_Failure_resolved__c  == 'No' || objworkorder.Is_Mechanical_Test_Failure_Resolved__c  == 'No'
                || objworkorder.Is_Planned_Maintenance_Failure_Resolved__c  == 'No' || objworkorder.Is_Autoguide_Checkout_Failure_Resolved__c == 'No'
                || objworkorder.Is_MVS_IAS_Batteries_Failure_Resolved__c  == 'No' 
                ) && objworkorder.Status__c=='Completed') 
        {
            
            listFollowUpWO.add(objworkorder);   
          //  objworkorder.Is_Executing_Reopen_only_onceFollow__c=true;       
        }
        if(objworkorder.Complaint__c != null){
            listOfWOforComplaint.add(objworkorder);          
        } 
        system.debug('objworkorder.Complaint__c --->'+objworkorder.Complaint__c);
        if(objworkorder.Complaint__c == null && 
			(  objworkorder.Hardware__c=='FAIL' || objworkorder.Software__c=='FAIL' || objworkorder.Instruments__c=='FAIL'
		    || objworkorder.Software_1_P_F__c=='FAIL' || objworkorder.Software_2_P_F__c=='FAIL' || objworkorder.Software_3_P_F__c=='FAIL'
		    || objworkorder.Software_4_P_F__c=='FAIL'|| objworkorder.Software_5_P_F__c=='FAIL' || objworkorder.Software_6_P_F__c=='FAIL' 
		    || objworkorder.Software_7_P_F__c=='FAIL' || objworkorder.Software_8_P_F__c=='FAIL' || objworkorder.Demo_Eval_General_System_Check__c=='FAIL' 
		    || objworkorder.Motion_P_F__c=='FAIL' || objworkorder.Image_Quality_P_F__c=='FAIL' || objworkorder.Navigation_P_F__c=='FAIL'
		    || objworkorder.Temperature_Control_Subsystem_P_F__c=='FAIL' || objworkorder.Mechanical_Subsystem_P_F__c=='FAIL'
		    || objworkorder.StarShield_V2_V3_P_F__c=='FAIL' || objworkorder.Asset_P_F__c=='FAIL' 
		    || objworkorder.Test_Finalization_P_F__c=='FAIL' || objworkorder.Checks_After_Shutdown_P_F__c=='FAIL' 
		    || objworkorder.Image_Quality_and_Navigation_P_F__c=='FAIL' || objworkorder.Backup_Data_P_F__c=='FAIL' || objworkorder.Chill_Water_Level_P_F__c=='FAIL' 
		    || objworkorder.Data_Files_Cleanup_P_F__c=='FAIL'  || objworkorder.Integrated_System_P_F__c=='FAIL'
		    || objworkorder.Remote_Access_P_F__c=='FAIL' || objworkorder.PCAnywhere_Configuration_P_F__c=='FAIL'|| objworkorder.OR_Table_Comp_Install_Only_P_F__c=='FAIL'
		    || objworkorder.Asset_Inspection_P_F__c=='FAIL' || objworkorder.Electrical_Inspection_P_F__c=='FAIL' 
		    || objworkorder.Compliance_Testing_P_F__c=='FAIL'  || objworkorder.O_Arm_image_transfers_to_Stealth__c=='FAIL'
		    || objworkorder.Accurate_Navigation_on_O_Arm_Image_P_F__c=='FAIL' || objworkorder.Mechanical_Tests_P_F__c=='FAIL'|| objworkorder.Imaging_Modalities_P_F__c=='FAIL'
		    || objworkorder.Does_this_Result_in_a_Complaint__c=='YES' || objworkorder.Cable_Grade__c=='B' || objworkorder.Cable_Grade__c=='C' || objworkorder.Cable_Grade__c=='D'
		    || objworkorder.System_Perform_As_Intended__c=='NO' || objworkorder.Visually_inspected_parts_prior_to_instal__c == 'FAIL'
		    || objworkorder.Software_Checkout_P_F__c == 'FAIL' || objworkorder.Scanner_Configuration_P_F__c == 'FAIL' 
   			|| objworkorder.Instrument_Checkout__c == 'FAIL' || objworkorder.Asset_Checkout_P_F__c == 'FAIL' 
       		|| objworkorder.Navigation_Checkout__c == 'FAIL' || objworkorder.Laser_Test_Result_P_F__c == 'FAIL'       		
       		|| objworkorder.Cleaning_Inspecting_MVS_IAS_P_F__c == 'FAIL' || objworkorder.IAS_Batteries_P_F__c == 'FAIL'
       		|| objworkorder.Mechanical_Test_P_F__c == 'FAIL' || objworkorder.Planned_Maintenance_Calibrations_P_F__c == 'FAIL'
       		|| objworkorder.Autoguide_Checkout_P_F__c == 'FAIL' || objworkorder.Mechanical_Fit_of_part_upon_install__c == 'FAIL'
			)
		){
				
			lstOfWoFailureToCreateComplaint.add(objworkorder);			
        }
        // objworkorder.Is_Executing_Workflow_Internal_use_only__c = false;      
    }  
/*system.debug('Inside AFTER TRIGGER call function createFollowupWO 1234 ---->'+Workorder_Logic.isWORecursive);    
 if(listFollowUpWO.size()>0 && WorkOrder_Logic.flagUpdateFollowUp == false){
                system.debug('Inside AFTER TRIGGER call function createFollowupWO ---->'+Workorder_Logic.isWORecursive);        
                if(doNotExecuteForReopenAfter == false)
                {
                    WorkOrder_Logic.createFollowupWO(listFollowUpWO);
                }
            } */
            
          if (doNotExecuteFromWorkflowinAfter == true)
        { 
            System.debug('variable doNotExecuteFromWorkflowinAfter >>>' + doNotExecuteFromWorkflowinAfter);
            System.debug('Inside After Trigger do nothing after this');
            // doNothing
        }
        else
        {
System.debug('Inside After Trigger do nothing after this else');

            /* Out of scope for integration
            if(listWOForInsSoft.size()>0){
                system.debug('Inside AFTER TRIGGER call function UpdateInstalledSoftware');
                WorkOrder_Logic.UpdateInstalledSoftware(listWOForInsSoft);        
            } */    
        
            if(listWOForUpdateSys.size()>0 && WorkOrder_Logic.flagSysUpdate==false){
                system.debug('Inside AFTER TRIGGER call function UpdateSystem');
                WorkOrder_Logic.UpdateSystem(listWOForUpdateSys, Trigger.oldMap);
            }
           
            if(listWOForUpdateTrackParts.size()>0){       
                system.debug('Inside AFTER TRIGGER call function updateTrackedParts');
                WorkOrder_Logic.updateTrackedParts(listWOForUpdateTrackParts);
            }
            if(listOfWOforComplaint.size()>0 && WorkOrder_Logic.updateFlag==false){
                system.debug('Inside AFTER TRIGGER call function UpdateComplaintFromWO');       
                WorkOrder_Logic.updateComplaintFieldsFromWorkOrder(listOfWOforComplaint, Trigger.oldMap);    // Bart Caelen - 20161118 - Update added Trigger.oldMap parameter
            }  
            if(lstOfWoFailureToCreateComplaint.size()>0 && WorkOrder_Logic.updateFlag == false){
                system.debug('Inside AFTER TRIGGER call function CreateComplaintFromWO');         
                WorkOrder_Logic.CreateComplaintFromWO(lstOfWoFailureToCreateComplaint);
            } 
    
        }
        system.debug('Inside AFTER TRIGGER call function createFollowupWO 1234 ---->'+Workorder_Logic.isWORecursive);    
 		if(listFollowUpWO.size()>0 && WorkOrder_Logic.flagUpdateFollowUp == false)
        {
                system.debug('Inside AFTER TRIGGER call function createFollowupWO ---->'+Workorder_Logic.isWORecursive);        
                if(doNotExecuteForReopenAfter == false)
                {
                    WorkOrder_Logic.createFollowupWO(listFollowUpWO);
                }
            }
        
	}
    
	List<WorkOrder__c> updatedWorkOrders = new List<WorkOrder__c>();
	List<WorkOrder__c> workOrdersLinkedToComplaints = new List<WorkOrder__c>();

	for(WorkOrder__c wo : Trigger.new)
	{
		if(wo.Complaint__c != null){
			
			//If updated and connected to complaint we check updated fields
			if(Trigger.isUpdate){
				
				if(WorkOrder_Logic.processedUpdated.add(wo.Id) == true) updatedWorkOrders.add(wo);
			}          
			//If created linked to a Complaint or updated the Complaint of the WO we create full snapshot
			if(Trigger.isInsert || wo.Complaint__c != Trigger.oldMap.get(wo.Id).Complaint__c){
				
				if(WorkOrder_Logic.processedLinked.add(wo.Id) == true) workOrdersLinkedToComplaints.add(wo);
			}
		} 
	}
	
	if(updatedWorkOrders.size() > 0){
		WorkOrder_Logic.UpdateComplaintFromWO(updatedWorkOrders, Trigger.oldMap);
	} 
	
	if(workOrdersLinkedToComplaints.size() > 0){
		WorkOrder_Logic.UpdateComplaintFromWO(workOrdersLinkedToComplaints, null);
	}  
    
    if(Trigger.isUpdate == true) 
    {
        system.debug('Inside AFTER TRIGGER ISUPDATE IS TRUE');

        // Bart Caelen - 14-02-2017 - CR-11545 - START
        if (Trigger.isAfter){

            Workorder_Logic.updateCase_MeanTimeToRepair(Trigger.new, Trigger.OldMap);

        }
        // Bart Caelen - 14-02-2017 - CR-11545 - STOP

        Integer compCount = 0;
        Map<Id, Complaint__c> complaintMap = new Map<Id, Complaint__c>();
        Map<Id, String> mapOfNameForComp = new Map<Id, String>();
        set<Id> complaintIdSet = new Set<Id>();
        for(Workorder__c woObj :Trigger.old) {
            if(woObj.Complaint__c != Trigger.new[compCount].Complaint__c) {
                if(Trigger.new[compCount].Complaint__c == null && woObj.Complaint__c != null) {
                    complaintIdSet.add(woObj.Complaint__c);                    
                    mapOfNameForComp.put(woObj.Complaint__c, woObj.Name);                   
                }
            }
            compCount ++;
        }
        if(complaintIdSet.size() > 0) {
            for(Complaint__c compObj:[select id, Parent_Complaint_No__c,Name from Complaint__c where Id IN:complaintIdSet]) {
                complaintMap.put(compObj.Id, compObj);
            }                        
        }
        
        if(complaintMap.size() > 0) 
            Case_logic.complaintRemove(complaintMap, mapOfNameForComp, 'Workorder', 'Delink', null);
             
       
    } 
     /*for(Workorder__c updateWO:Trigger.new)
     {
      	 Workorder__c woObj;
         
         if(updateWO.Related_Workorder__c != null && updateWO.Complaint__c == null)
         {
             //woObj.id = updateWO.Related_Workorder__c;
             updateWO.Complaint__c = updateWO.Related_Work_Order__r.complaint__c;
             update updateWO;
         }
     }*/
    
}