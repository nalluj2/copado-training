/**
 * Creation Date :  20110331
 * Description :   When Procedure is inserted or updated check if Therapy is changed or Therapy text, Business Unit or Business Unit text
 *                 are empty.
 * Author :     MDT - JaKe
 * 
 * UPDATED 20140122
 * Author		: Bart Caelen
 * Description	: Changed the layout of the trigger by implementing Best Practices (minimize SOQL's, minimize script lines, ....)
 */
trigger ImplantBeforeInsertUpdate on Implant__c (before insert, before update) {
 

	//- BC - 201401 - Changed the layout of the trigger and applied best practices -// 
    if (GTutil.ImplantBeforeInsertUpdate){
    	
    	GTutil.ImplantBeforeInsertUpdate=false;
	    	
	    if (trigger.isBefore){
			// Load Therapy, Business Unit and Account Data with a single SOQL and store it in a map
	    	map<id, Therapy__c> mapTherapy = new map<id, Therapy__c>
	    		([
	    			SELECT 
	    				ID, Therapy_Name_Hidden__c
	    				, Business_Unit__c, Business_Unit__r.Id, Business_Unit__r.Name
	    				, Business_Unit__r.Company__c, Business_Unit__r.Company__r.Company_Code_Text__c, Business_Unit__r.Company__r.name
	    			FROM 
	    				Therapy__c
				]);
	    
			for (Implant__c oImplant : trigger.new){
				if (mapTherapy.containsKey(oImplant.Therapy__c)){
					Therapy__c oTherapy = mapTherapy.get(oImplant.Therapy__c);

	                oImplant.Therapy_Text__c = oTherapy.Therapy_Name_Hidden__c;
	                oImplant.Business_Unit__c = oTherapy.Business_Unit__c;
	                oImplant.Business_Unit_Text__c = oTherapy.Business_Unit__r.Company__r.name + ' - ' + oTherapy.Business_Unit__r.Name;
	                oImplant.Implant_Therapy__c = oTherapy.Id;
				}
			}
	    }
    }

}