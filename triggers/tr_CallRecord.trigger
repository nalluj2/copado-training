//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 19-08-2016
//  Description      : APEX Trigger on Call_Records__c 
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
trigger tr_CallRecord on Call_Records__c (before insert, before update, after update) {

	if (Trigger.isBefore){

		if (Trigger.isInsert){

			bl_CallRecord_Trigger.updateFiscalPeriod(Trigger.new, Trigger.oldMap);

		}else if (Trigger.isUpdate){

			bl_CallRecord_Trigger.updateFiscalPeriod(Trigger.new, Trigger.oldMap);

		}

	}else{
		
		if(Trigger.isUpdate){
			
			bl_CallRecord_Trigger.copyProductsToCallContacts(Trigger.new, Trigger.oldMap);
		}
		
	}

}
//--------------------------------------------------------------------------------------------------------------------