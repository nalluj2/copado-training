/*
  *  Trigger Name   : Case_UPSERT_AFTER
  *  Description   : This trigger is used to execute the CreateWOfromCase from Case_Logic class
  *  Created Date  : 6/8/2013
  *  Author        : Wipro Tech. 
*/
trigger Case_UPSERT_AFTER on Case (after insert, after update) 
{	
	//If we check the flag in the Custom Settings we skip the execution of this logic
	if(bl_Trigger_Deactivation.isTriggerDeactivated('Case_UPSERT_AFTER')) return;
	
	//Use this logic only to the Cases with Record Types related to ST Service Cases (New_Case, Closed_Case)
	List<Case> caseInScope = new List<Case>();
	
	for(Case caseRecord : Trigger.New){
		
		if(Case_Logic.caseRTInScope.contains(caseRecord.RecordTypeId)) caseInScope.add(caseRecord);		
	}
	
	if(caseInScope.size() == 0) return;
		
	if(bl_SynchronizationService_Utils.isSyncProcess == false){
		
		if(Trigger.isInsert) bl_SynchronizationService_Source.createNotificationIfChanged(caseInScope, null);
		else bl_SynchronizationService_Source.createNotificationIfChanged(caseInScope, Trigger.OldMap);	
	}
	
	Case_Logic.createComplaintLinkedComment(caseInScope, Trigger.oldMap);
	   
    //start-- added by Pavan to bulkify this trigger
    set<id> setcaseIds = new set<id>();
    //List<ProcessInstance> pi;
    map<id,id> mapcaseids = new map<id,id>();
    //map<id,list<ProcessInstance>> mapProcessInstanceids = new map<id,list<ProcessInstance>>();
    //end-- Added by Pavan to bulkify this trigger
    
        List<Case> listofCaseToCreateWokrOrder=new List<Case>();
        for(Case objCase:caseInScope)
        {
            setcaseIds.add(objCase.id);// added by Pavan
            mapcaseids.put(objCase.id,objCase.id);// added by Pavan
            if(objCase.Workorder__c!=null)
            {
                listofCaseToCreateWokrOrder.add(objCase);
            }            
        }
        
        if(listofCaseToCreateWokrOrder.size()>0)
        {
            Case_Logic.CreateWOfromCase(listofCaseToCreateWokrOrder);
        }
        /* NO APPROVAL PROCESS IN INTL INSTANCE, ONLY IN US
        // start-- modified by Pavan in order to bulkify this trigger
        if(!setcaseIds.isempty()){
            pi = [SELECT status, TargetObjectId 
                                        FROM ProcessInstance     
                                        WHERE TargetObjectId in :setcaseIds //:objCase.Id
                                        AND IsDeleted = false
                                        AND status = 'Pending'];
            if(pi != null && pi.size() > 0){
                for(ProcessInstance objProcessInstance : pi){
                    if(mapProcessInstanceids.containskey(objProcessInstance.TargetObjectId)){
                        list<ProcessInstance> colids= mapProcessInstanceids.get(objProcessInstance.TargetObjectId);
                        colids.add(objProcessInstance);
                        mapProcessInstanceids.put(objProcessInstance.TargetObjectId,colids);
                    }
                    else{
                        list<ProcessInstance> colids= new list<ProcessInstance>();
                        colids.add(objProcessInstance);
                        mapProcessInstanceids.put(objProcessInstance.TargetObjectId,colids);
                    }
                }
            }
        }

        for (Integer i = 0; i < Trigger.new.size(); i++)
        {    
            
            //kick off approval process if case status is closed
            if (Trigger.new[i].Status == 'Closed'  && (mapProcessInstanceids == null || !mapProcessInstanceids.containskey(Trigger.new[i].id)) && (Trigger.isupdate && Trigger.old[i].Status !='Closed')) 
            
            // end-- Modified by Pavan to bulkify this trigger
            {
              System.debug('In If statement');
                
              Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
              req.setComments('Submitted for approval. Please review.');
              req.setObjectId(Trigger.new[i].Id);
                  // submit the approval request for processing
              Approval.ProcessResult result = Approval.process(req);
                  // display if the reqeust was successful
              if(result.isSuccess())
                 System.debug('Submitted for approval successfully:  '+result.isSuccess());
              else
                 System.debug('Submitted for approval failed:  '+result);
            }
        }
        */
        
        if(Trigger.isUpdate){
        	
        	Case_Logic.UpdateComplaintFromCase(caseInScope, Trigger.oldMap);
        	
        	Case_Logic.createCommentFromCase(caseInScope, Trigger.oldMap);
        }
                
        
        if(Case_Logic.updateFlag == false)
        {
            Case_Logic.CreateComplaintFromCase(caseInScope, Trigger.oldMap); // Bart Caelen - 20161118 - Update added Trigger.oldMap parameter
        }
                
        if(Trigger.isUpdate == true) 
        {
            Integer compCount = 0;
            Map<Id, Complaint__c> complaintMap = new Map<Id, Complaint__c>();
            Map<Id, String> mapOfNameForComp = new Map<Id, String>();
            set<Id> compainyIdSet = new Set<Id>();
            for(Case caseObj :Trigger.old)
            {
                if(caseObj.Complaint__c != Trigger.new[compCount].Complaint__c) 
                {
                    if(Trigger.new[compCount].Complaint__c == null) 
                    {
                        compainyIdSet.add(caseObj.Complaint__c);
                        mapOfNameForComp.put(caseObj.Complaint__c, caseObj.CaseNumber);
                    } 
                }
                compCount++;
            }
            if(compainyIdSet.size() > 0) 
            {
                for(Complaint__c compObj:[select id, Name, Parent_Complaint_No__c from Complaint__c where Id IN:compainyIdSet]) 
                {
                    complaintMap.put(compObj.Id, compObj);
                }
            }
            
            if(complaintMap.size() > 0 && Case_Logic.updateFlag == false) 
                Case_logic.complaintRemove(complaintMap, mapOfNameForComp, 'Case', 'Delink', null);
        }   
        //Integer i=0;
        map<Id, Case> mapCaseIdCaseObj = new map<id,Case>();// to call case closure method
        map<id,Case>  mapComplaintIdCaseObj= new map<id,Case>(); 
        if(Trigger.isUpdate)
        {
            for(Case CaseInst: caseInScope)
            {
                //below condition is for caseClosure() method
                if(CaseInst.status =='Closed' && Trigger.oldMap.get(caseInst.Id).status != 'Closed' && !Case_Logic.alreadyClosed.contains(caseInst.Id))
                {
                    mapCaseIdCaseObj.put(CaseInst.Id,CaseInst);    
                }
                if(CaseInst.status !='Closed' && Trigger.oldMap.get(caseInst.Id).status == 'Closed' && CaseInst.Complaint_Case__c == 'Yes' && CaseInst.Complaint__c != Null && String.valueOf(CaseInst.Complaint__c)!='')
                {
                    mapComplaintIdCaseObj.put(CaseInst.Complaint__c,CaseInst) ;
                }
                if(CaseInst.status !='Closed' && Trigger.oldMap.get(caseInst.Id).status == 'Closed' && CaseInst.Complaint_Case__c == 'Yes' && (CaseInst.Complaint__c == Null || String.valueOf(CaseInst.Complaint__c)==''))
                {
                    CaseInst.addError('Exception: Case needs to have a Complaint number.');
                }
                //i=i+1;
             }
            if(mapComplaintIdCaseObj.size() > 0)
            {
                Case_Logic.ReopenComplaintFromCase(mapComplaintIdCaseObj);
            }
            if(mapCaseIdCaseObj.size() > 0)
            {
                Case_Logic.CaseClosureProcess(mapCaseIdCaseObj); 
            }
        }
}