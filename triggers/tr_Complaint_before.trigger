trigger tr_Complaint_before on Complaint__c (before insert, before update) {
	
	//Populate the External_Id__c field with a new Id if no value was set before.
	GuidUtil.populateMobileId(Trigger.new, 'External_Id__c');
}