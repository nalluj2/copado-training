trigger InstalledSoftware_BEFORE_UPSERT on Installed_Software__c (before insert, before update) {
	
	GuidUtil.populateMobileId(Trigger.new, 'External_Id__c');
	
	//If we check the flag in the Custom Settings we skip the execution of this logic
	if(bl_Trigger_Deactivation.isTriggerDeactivated('InstalledSoftware_BEFORE_UPSERT')) return;
	
	for(Installed_Software__c obj : Trigger.new)
    {
        String systemType;
        String[] validSoftwareValues;
        
        List<Asset> systemSysType = [select Asset_Product_Type__c from Asset where id = :obj.Asset__c];
        for(Asset mySystemSysType: systemSysType){
            systemType = mySystemSysType.Asset_Product_Type__c;
        }
        List<Software__c> softwareSysType = [select Asset_Product_Type__c from Software__c where id = :obj.Software__c];
        for(Software__c mySoftwareSysType: softwareSysType){
            //split picklist into list
            System.debug('MH: mySoftwareSysType.Asset_Product_Type__c: '+ mySoftwareSysType.Asset_Product_Type__c);
            if(mySoftwareSysType.Asset_Product_Type__c != null && mySoftwareSysType.Asset_Product_Type__c != ''){
                validSoftwareValues = mySoftwareSysType.Asset_Product_Type__c.split(';', 0);
                System.debug('MH: validSoftwareValues: '+ validSoftwareValues);
                Integer match = 0;
                for(integer i=0;i<validSoftwareValues.size();i++){
                    if(validSoftwareValues[i].indexOf(systemType)!= -1){
                        match = match + 1;
                        System.debug('MH: validSoftwareValues.indexOf(systemType): '+ validSoftwareValues[i].indexOf(systemType));
                        System.debug('MH: match: '+ match);
                    }
                }
                if(match == 0){
                    obj.Software__c.addError('You must choose software that is valid for the Asset');
                }
            }
            else{
                //if system type is null, but it is required so shouldn't happen!
                obj.Software__c.addError('You must choose software that is valid for the Asset');
            }    
        }
        System.debug('MH: systemType: '+ systemType);
        System.debug('MH: softwareSysType: '+ softwareSysType);
    }

}