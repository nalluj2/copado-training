/**
 * Creation Date :  20101118
 * Description : 	Before Delete Apex Trigger : Prevents the deletion of any DIB_Campaign_Account__c
 * Specifications : Only System Administrator Profiles and some other profile can Edit/Delete 
 *					Sytem Administrators, Data Stewards can delete any Account ; All others can't !
 * Author : 		ABSI - Malaka Silva
 */
trigger CampainAccountBeforeUpdateDelete on DIB_Campaign_Account__c (before update, before delete) {
	SharedMethods sharedMethod = new SharedMethods();
	if(sharedMethod.checkEditDeletePermission(Userinfo.getProfileId())){
		
		Set<Id> updateAccountCampainList = null;
		if(controllerDIB_Campaign.updateAccountCampainMap != null){
			updateAccountCampainList = controllerDIB_Campaign.updateAccountCampainMap.get(Userinfo.getUserId());
			if(updateAccountCampainList != null){
				controllerDIB_Campaign.updateAccountCampainMap.remove(Userinfo.getUserId());
			}
		}
		
		DIB_Campaign_Account__c [] dibCampaignAccounts = null;
		if(trigger.isDelete){
			dibCampaignAccounts = trigger.old;
		}else{
			dibCampaignAccounts = trigger.new;
		}
		for(DIB_Campaign_Account__c dibCampaignAccount:dibCampaignAccounts){
			if(updateAccountCampainList == null || !updateAccountCampainList.contains(dibCampaignAccount.Id)){
				dibCampaignAccount.addError(FinalConstants.NO_EDIT_DELETE_PERMISSION);
			}
		}
	}
}