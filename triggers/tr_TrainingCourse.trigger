//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 28-06-2016
//  Description      : APEX Trigger
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
trigger tr_TrainingCourse on Training_Course__c (before update, after update) {

	//--------------------------------------------------------------------------------------------------------------------
	// Pass Trigger variables to APEX Class
	//--------------------------------------------------------------------------------------------------------------------
	bl_TrainingCourse_Trigger.lstTriggerNew = Trigger.new;
	bl_TrainingCourse_Trigger.mapTriggerNew = Trigger.newMap;
	bl_TrainingCourse_Trigger.mapTriggerOld = Trigger.oldMap;
	//--------------------------------------------------------------------------------------------------------------------

	if (Trigger.isBefore){

		if (Trigger.isUpdate){

			bl_TrainingCourse_Trigger.validateTrainingCourseClosure();

		}
	
	}else if (Trigger.isAfter){

		if (Trigger.isUpdate){

			bl_TrainingCourse_Trigger.updateTrainingNomination_Url();

			bl_TrainingCourse_Trigger.updateTrainingNomination_Status();

			bl_TrainingCourse_Trigger.updateTrainingNomination_PACEApprover();

		}
	}

}
//--------------------------------------------------------------------------------------------------------------------