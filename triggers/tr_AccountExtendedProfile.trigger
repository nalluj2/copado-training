//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 16-08-2017
//  Description      : APEX Trigger on Account_Extended_Profile__c 
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
trigger tr_AccountExtendedProfile on Account_Extended_Profile__c (before insert, before update, after insert, after update) {

	if (Trigger.isBefore){

		if (Trigger.isInsert){

			bl_AccountExtendedProfile_Trigger.setLastUpdated(Trigger.new, Trigger.oldMap);

			bl_AccountExtendedProfile_Trigger.setApproverEmail(Trigger.new, Trigger.oldMap);

			bl_AccountExtendedProfile_Trigger.setUniqueKey(Trigger.new, Trigger.oldMap);

		}else if (Trigger.isUpdate){

			bl_AccountExtendedProfile_Trigger.setLastUpdated(Trigger.new, Trigger.oldMap);

			bl_AccountExtendedProfile_Trigger.resetApprovalStatus(Trigger.new, Trigger.oldMap);

			bl_AccountExtendedProfile_Trigger.setApproverEmail(Trigger.new, Trigger.oldMap);

			bl_AccountExtendedProfile_Trigger.setUniqueKey(Trigger.new, Trigger.oldMap);

		}

	}else if (Trigger.isAfter){

		if (Trigger.isInsert){

			bl_AccountExtendedProfile_Trigger.createHistoryTracking(Trigger.new, Trigger.oldMap);

		}else if (Trigger.isUpdate){

			bl_AccountExtendedProfile_Trigger.createHistoryTracking(Trigger.new, Trigger.oldMap);

		}

	}

}
//--------------------------------------------------------------------------------------------------------------------