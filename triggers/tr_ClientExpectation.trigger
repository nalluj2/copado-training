//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 13-02-2019
//  Description      : APEX Trigger on Client_Expectation__c
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
trigger tr_ClientExpectation on Client_Expectation__c (before insert, before update){

	if (Trigger.isBefore){
	
		if (Trigger.isInsert){

			bl_ClientExpectation_Trigger.IHS_SetExpectationReadyDate(Trigger.new, Trigger.oldMap);
		
		}else if (Trigger.isUpdate){

			bl_ClientExpectation_Trigger.IHS_SetExpectationReadyDate(Trigger.new, Trigger.oldMap);

		}
		
	}

}
//--------------------------------------------------------------------------------------------------------------------