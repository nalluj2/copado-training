/*
 *      Created Date : 18 -Jun-2013
 *      Description : This trigger CalculateBusinessHoursAges.

 *      Author = Kaushal Singh
 *      
 */
trigger tr_CalculateBusinessHoursAges on Case (before insert, before update) {

    if (Trigger.isInsert && bl_CalculateBusinessHoursAges.blnInsertCheck){

		bl_CalculateBusinessHoursAges.UpdateCase(Trigger.New);

	}else{

		if (Trigger.isUpdate && bl_CalculateBusinessHoursAges.blnUpdateCheck == true){
        
		   bl_CalculateBusinessHoursAges.CalculateBusinessCaseAges(Trigger.New,Trigger.OldMap);
		
		}
	
	}
	
}