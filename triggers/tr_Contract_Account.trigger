trigger tr_Contract_Account on Contract_Account__c (after delete, after insert, after undelete, after update) {
	
	if(Trigger.isBefore){
		
	}else{
		
		if(Trigger.isInsert){
			
			bl_Contract_Account.rollupAccountSAPIds(Trigger.new);
			
		}else if(Trigger.isUpdate){
			
			bl_Contract_Account.rollupAccountSAPIds(Trigger.new);
			
		}else if(Trigger.isDelete){
			
			bl_Contract_Account.rollupAccountSAPIds(Trigger.old);
			
		}else if(Trigger.isUndelete){
			
			bl_Contract_Account.rollupAccountSAPIds(Trigger.new);			
		}	
	}    
}