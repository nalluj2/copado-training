//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 25-02-2016
//  Description      : APEX Trigger on SVMXC__PM_Plan__c 
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
trigger tr_SVMXC_PMPlan on SVMXC__PM_Plan__c (before insert) {

	// Pass the Trigger newMap and oldMap to the APEX Class (TriggerHandler)
	bl_SVMXC_PMPlan_Trigger.lstTriggerNew = Trigger.new;
	bl_SVMXC_PMPlan_Trigger.mapTriggerNew = Trigger.newMap;
	bl_SVMXC_PMPlan_Trigger.mapTriggerOld = Trigger.oldMap;


	if (Trigger.isBefore){

		if (Trigger.isInsert){

			// ALL BEFORE INSERT ACTIONS
			bl_SVMXC_PMPlan_Trigger.copyAccountCountry('INSERT');

		}else if (Trigger.isUpdate){

			// ALL BEFORE UPDATE ACTIONS
			bl_SVMXC_PMPlan_Trigger.copyAccountCountry('UPDATE');
		
		}else if (Trigger.isDelete){

			// ALL BEFORE DELETE ACTIONS

		}

	}else if (Trigger.isAfter){

		if (Trigger.isInsert){

			// ALL AFTER INSERT ACTIONS

		}else if (Trigger.isUpdate){

			// ALL AFTER UPDATE ACTIONS

		}else if (Trigger.isDelete){

			// ALL AFTER DELETE ACTIONS

		}else if (Trigger.isUnDelete){

			// ALL AFTER UNDELETE ACTIONS

		}

	}

}
//--------------------------------------------------------------------------------------------------------------------