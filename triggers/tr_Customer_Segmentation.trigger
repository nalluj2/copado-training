trigger tr_Customer_Segmentation on Customer_Segmentation__c (before insert, before update, after insert, after update) {
    
    if(Trigger.isBefore){
    	
    	if(Trigger.isInsert){
    		
    		bl_Customer_Segmentation_Trigger.setUniqueKey(Trigger.new);
    		
    	}else if(Trigger.isUpdate){
    		
    		bl_Customer_Segmentation_Trigger.setUniqueKey(Trigger.new);    		
    	}
    	    	
    }else{
    	
    	if(Trigger.isInsert){
    		
    		bl_Customer_Segmentation_Trigger.matchInputs(Trigger.new, null);
    	
    	}else if(Trigger.isUpdate){
    		
    		bl_Customer_Segmentation_Trigger.matchInputs(Trigger.new, Trigger.oldMap);
    	}
    }
}