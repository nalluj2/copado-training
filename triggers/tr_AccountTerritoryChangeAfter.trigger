/**
 *      Created Date : 20121109
 *      Description : This class is used to execute Account TerritoryChanges
 * 
 *      Author = Rudy De Coninck
 */

trigger tr_AccountTerritoryChangeAfter on Account_Territory_Change__c (after insert, after update) {
	
	List<Account_Territory_Change__c> atcUpsert = new List<Account_Territory_Change__c>();
	List<Account_Territory_Change__c> atcDelete = new List<Account_Territory_Change__c>();
		
	for (Account_Territory_Change__c atc : Trigger.new){
			
		if(atc.Operation__c == 'DELETE') atcDelete.add(atc);
		else atcUpsert.add(atc);
	}
		
	if(atcDelete.size() > 0) bl_Territory.deleteAccountsFromTerritory(atcDelete);
	
	if(atcUpsert.size() > 0) bl_Territory.migrateAccountsToNewTerritory(atcUpsert);			
}