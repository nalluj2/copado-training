trigger tr_CreateUserRequestBefore on Create_User_Request__c (before insert, before update) {

    if (bl_Trigger_Deactivation.isTriggerDeactivated('tr_CreateUserRequestBefore')) return;

	List<Create_User_Request__c> createUserRequestList = new List<Create_User_Request__c>();
	
	for(Create_User_Request__c req : Trigger.new){
		
		if(req.Application__c == 'Salesforce.com' && req.Request_Type__c == 'Create User'){
			createUserRequestList.add(req);
		}	
		
		if(Trigger.isUpdate){
			
			Create_User_Request__c oldReq = Trigger.oldMap.get(req.Id);
			
			if(isClosed(req, oldReq)){
				req.addError('Request cannot be updated because it has been closed. Current status: '+Trigger.oldMap.get(req.Id).Status__c);
			}
			
			if((req.Status__c == 'Completed' && oldReq.Status__c != 'Completed') || (req.Status__c == 'Accepted' && oldReq.Status__c != 'Accepted')){
				req.Completed_Date__c = DateTime.now();
				bl_CreateUserRequest.setCompletionTimeBusinessHours(req);
			}
			
			if(req.Assignee__c != null && oldReq.Assignee__c == null){
				req.Assigned_Date__c = DateTime.now();
				if(req.Application__c == 'Salesforce.com' && req.Request_Type__c == 'Create User' && req.License_Type__c == 'Chatter Free') req.Status__c = 'Approved';
				bl_CreateUserRequest.setResponseTimeBusinessHours(req);
			}
		}
	}
	       	
	if(createUserRequestList.isEmpty() == false){
		bl_CreateUserRequest.enrichUserCreateRequests(createUserRequestList);
	}
	
	private static Boolean isClosed(Create_User_Request__c req, Create_User_Request__c oldReq){
		
		if(req.Status__c == oldReq.Status__c) return false;
		
		if(oldReq.Status__c == 'Cancelled') return true;
		if(oldReq.Status__c == 'Completed') return true;
		if(req.Request_Type__c != 'Data Load' && oldReq.Status__c == 'Rejected') return true;
		if(oldReq.Status__c == 'Accepted') return true;
		
		return false;
	}
}