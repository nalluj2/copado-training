trigger tr_Contract_Repository_Document on Contract_Repository_Document__c (before Insert,before update,after delete, before delete) {
   
    switch on Trigger.operationType {
            when BEFORE_INSERT, BEFORE_UPDATE {
                bl_Contract_Repository_Document.manageDocumentPermissions(Trigger.new,trigger.isInsert);
            }
            when BEFORE_DELETE {
                bl_Contract_Repository_Document.manageDocumentPermissions(Trigger.old,trigger.isInsert);
                bl_Contract_Repository_Document.collectDocumentVersionsToDelete(Trigger.old);
            }
            when AFTER_DELETE {
                bl_Contract_Repository_Document.setDocumentVersionsToDelete();
            }
             when AFTER_UNDELETE {
                bl_Contract_Repository_Document.manageDocumentPermissions(Trigger.new,trigger.isInsert);
            }
        }    
}