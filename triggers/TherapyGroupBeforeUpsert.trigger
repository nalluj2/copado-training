trigger TherapyGroupBeforeUpsert on Therapy_Group__c (before insert, before update, before delete) {    
    /*if(Trigger.isInsert || Trigger.isUpdate){        
        for(Integer i=0; i<trigger.new.size(); i++){
            trigger.new[i].Therapy_Group__c = trigger.new[i].Name;    
        }    
    }*/  
    if(Trigger.isDelete){                
        // Check if current Call category Type is there in the Call Topic Setting Details...                   
        boolean recCount=false;        
        List<Therapy__c> lstTherapy = [Select s.id From Therapy__c s where s.Therapy_Group__c in : Trigger.old LIMIT 1];        
        if(lstTherapy.size()>0)        {            
            recCount=true;            
        }
        List<Account_Team_Member__c> lstAcTeam = [Select s.id From Account_Team_Member__c s where s.Therapy_Group__c in : Trigger.old LIMIT 1];        
        if(lstAcTeam.size()>0)        {            
            recCount=true;            
        }
        if(recCount==true)        {            
            trigger.old[0].name.addError('This Call Category Type is present in Therapy or Account Team Member. Please delete the child records and then try again.');            
        }                
    }              
}