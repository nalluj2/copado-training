trigger tr_AccountPerformanceLoadBefore on Account_Performance_Load__c (before insert, before update) {

	if(Trigger.isInsert){
		bl_AccountPerformanceLoad.convertToUSD(Trigger.new);
	}

	/*
	if(Trigger.isUpdate){
		Trigger.new.get(0).addError('Only inserts allowed'); 
	}*/	

}