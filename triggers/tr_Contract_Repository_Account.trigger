trigger tr_Contract_Repository_Account on Contract_Repository_Account__c (before delete, before insert, before update) {
	
	if(Trigger.isBefore){
		
		if(Trigger.isInsert){
			
			bl_Contract_Repository_Account.setUniqueKey(Trigger.new);
			
		}else if(Trigger.isUpdate){
			
			bl_Contract_Repository_Account.setUniqueKey(Trigger.new);
			
			bl_Contract_Repository_Account.blockMainAccount(Trigger.new, Trigger.oldMap);
			
		}else if(Trigger.isDelete){
				
			bl_Contract_Repository_Account.blockMainAccount(Trigger.old, null);
		}		
	}    
}