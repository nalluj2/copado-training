/**
 * Created by dijkea2 on 2-2-2017.
 */

// This trigger calls a global class of Marketing Cloud, see CR-13688 for more info
trigger Trig_Opportunity on Opportunity (after insert, after update) {

    if (bl_Trigger_Deactivation.isTriggerDeactivated('Trig_Opportunity')) return;   //-BC - 20171108 - Added

    // CR-14901 - BC - 20171016 - Only execute the Exact Target logic if at least 1 Opportunity has the RecordType "CAN DIB OPPORTUNITY" and the Stage = "Closed Won" - START
    Boolean bProcess = false;
    Id idRecordType = clsUtil.getRecordTypeByDevName('Opportunity', 'CAN_DIB_OPPORTUNITY').Id;
    Set<String> setStageName = new Set<String>();
    setStageName.add('Closed Won');
    for (Opportunity oOpportunity : trigger.new){
    	
    	if ( (oOpportunity.RecordTypeId == idRecordType) && (setStageName.contains(oOpportunity.StageName)) ){
			bProcess = true;
			break;
		}
    }

    if (bProcess == false) return;
    // CR-14901 - BC - 20171016 - Only execute the Exact Target logic if at least 1 Opportunity has the RecordType "CAN DIB OPPORTUNITY" and the Stage = "Closed Won" - STOP

    if (Test.isRunningTest()) return;    //-BC - 20171108 - Added

    et4ae5.triggerUtility.automate('Opportunity');

}