/*
 *      Author 		: Bart Caelen
 *		Date		: 2014-02
 *      Description : This trigger will process created Note records in order to automatically 
 *						create chatter feeds on the related Account Plan (CR-1908)
 *		Version		:  1.0
 *		13/11/2017 	: Extended to MITG Opportunities (Jesus Lozano)
 *		18/02/2029	: PMT-22787: MITG EMEA User Setup - Added Opportunity Record Type EMEA MITG Standard Opportunity
*/
trigger tr_Note_ChatterFeed on Note (after insert) {

	if (trigger.isAfter){
		//------------------------------------------------------------------------------
		// CHANGE THESE SETTINGS (IF NEEDED) 
		//	- Can als be replaced by Custom Setting or another Setting Object in the Future
		//------------------------------------------------------------------------------
		string tParentIDFieldName 	= 'ParentID';
		string tFieldForURLLabel 	= 'Title';
		set<string> setFieldNamesToMonitor = new set<string>();
		//------------------------------------------------------------------------------

		map<id, sObject> mapOldData = new map<id, sObject>();
		map<id, sObject> mapNewData = new map<id, sObject>();
		
		String accPlanSObjectPrefix = clsUtil.getPrefixFromSObjectName('Account_Plan_2__c');	// Get the prefix of an Account_Plan__c id so that we can process only the tasks related to an Account_Plan__c
		string oppObjectPrefix = clsUtil.getPrefixFromSObjectName('Opportunity');
		
		string tAction = '';
		
		Set<Id> parentOppIds = new Set<Id>();
		
		if (trigger.isInsert){
			
			tAction = 'INSERT';			
			
			for (sObject oSObject : trigger.new){
				
				String parentId = String.valueOf(oSObject.get(tParentIDFieldName));
				clsUtil.debug('string.valueOf(oSObject.get(tParentIDFieldName)) : ' + parentId);
												
				if (parentId.startsWith(accPlanSObjectPrefix)){
					
					mapNewData.put((Id) oSObject.get('id'), oSObject);
					
				}else if(parentId.startsWith(oppObjectPrefix)){
					
					parentOppIds.add(parentId);
				}
			}
				
			if(parentOppIds.size() > 0){
				//PMT-22787: MITG EMEA User Setup - Added Opportunity Record Type EMEA MITG Standard Opportunity
				Map<Id, Opportunity> mitgOpps = new Map<Id, Opportunity>([Select Id from Opportunity where Id IN :parentOppIds AND RecordType.DeveloperName IN ('EUR_MITG_Standard_Opportunity', 'MEA_MITG_Standard_Opportunity', 'EMEA MITG Standard Opportunity')]);
				
				for (sObject oSObject : trigger.new){
				
					String parentId = String.valueOf(oSObject.get(tParentIDFieldName));
					
					if(mitgOpps.containsKey(parentId)){
						
						mapNewData.put((Id) oSObject.get('id'), oSObject);
					}
				}
			}		
		}
		
		clsUtil.debug('mapNewData : ' + mapNewData);
		
		if (mapNewData.size() > 0){
			bl_Chatter.createChatterFeed(mapOldData, mapNewData, tAction, tParentIDFieldName, setFieldNamesToMonitor, tFieldForURLLabel);
		}

	}

}