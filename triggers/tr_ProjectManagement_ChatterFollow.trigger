/**
 * Creation Date :  20140130
 * Description : 	Trigger for following and unfollowing the Project Management requestor
 * Author : 		Bluewolf - Paul Rice
 * Change Log:
 */
trigger tr_ProjectManagement_ChatterFollow on Project_Management__c (after insert, before update) {
		
	if (trigger.isAfter && trigger.isInsert){
		// sdd the project requestor to the chatter feed on insertion		
		ProjectManagementFollowService.addChatterFollow(Trigger.newMap);		
	}
	
	if (trigger.isBefore && trigger.isUpdate){
		// remove the requestor from chatter that when the project stage is final
		Map<ID,Project_Management__c> finalStatusProjects = ProjectManagementFollowService.finalStatusChatterFilter(Trigger.newMap, Trigger.oldMap);
		ProjectManagementFollowService.removeChatterFollow(finalStatusProjects);
		// update the requestor when changed and the status is not final
		Map<ID,Project_Management__c> requestorUpdateProjects = ProjectManagementFollowService.chatterFollowerChangeFilter(Trigger.newMap, Trigger.oldMap);
		ProjectManagementFollowService.updateChatterFollow(requestorUpdateProjects, Trigger.oldMap);		
	}	
}