/*
	Unified trigger for Account
*/
trigger tr_Account on Account (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    

    if(Trigger.isBefore){
    	
    	if(Trigger.isInsert){
    		
    		// Before Insert      		
    		bl_Account_Trigger.setSAPData(Trigger.new, null);

    		bl_Account_Trigger.syncAccountCountry_AccountCountryvs(Trigger.new, null);
    		  		
    		bl_Account_Trigger.setPersonAccountRecordType(Trigger.new); 
    		
    		bl_Account_Trigger.setDistributorRecordType(Trigger.new);
            
            bl_Account_Trigger.setSAPAccountRecordType(Trigger.new); 
            
            bl_Account_Trigger.setDiabetesFlags(Trigger.new);  		
    		
            bl_Account_Trigger.copyAccountDataToPatient_Patient(Trigger.new, Trigger.oldMap);
            
            bl_Account_Trigger.checkSAPAccountActiveFlag(Trigger.new);
            
            bl_Account_Trigger.copyAddressFields(Trigger.new);
            
            bl_Account_Trigger.copySAPAddressFields(Trigger.new);
            
            bl_Account_Trigger.copyPatientEmailToEmail(Trigger.new, Trigger.oldMap);
            
            bl_Account_Trigger.copyLeadBirthdateToBirthdate(Trigger.new);
            
            bl_Account_Trigger.setFieldPersonnelTrunk(Trigger.new);
                       
            bl_Account_Trigger.setDIBSFIdSAP(Trigger.new);
            
            bl_Account_Trigger.setAccountOwner(Trigger.new);
            
            bl_Account_Trigger.setAccountRegion(Trigger.new);
            
            bl_Account_Trigger.matchCanadaSAPPatient(Trigger.new);
            
            bl_Account_Trigger.alignDiBSalesRep(Trigger.new);
            
            bl_Account_Trigger.setDefaultCurrency(Trigger.new);

			bl_Account_Trigger.setAccountISOCode(Trigger.new);

			bl_Account_Trigger.verifyAndSetPhysicianAccountId(Trigger.new, Trigger.oldMap);
    		
    	}else if(Trigger.isUpdate){
    		
    		// Before Update  
    		bl_Account_Trigger.setSAPData(Trigger.new, Trigger.oldMap);

    		bl_Account_Trigger.syncAccountCountry_AccountCountryvs(Trigger.new, Trigger.oldMap);
    		  		
    		bl_Account_Trigger.setPersonAccountRecordType(Trigger.new);
    		
    		bl_Account_Trigger.setDistributorRecordType(Trigger.new);
    		
    		bl_Account_Trigger.setSAPAccountRecordType(Trigger.new);
    		
    		bl_Account_Trigger.blockChangeAddressSAPAccount(Trigger.new, Trigger.oldMap);
    		
            bl_Account_Trigger.copyAccountDataToPatient_Patient(Trigger.new, Trigger.oldMap);
    		
    		bl_Account_Trigger.checkSAPAccountActiveFlag(Trigger.new);
    		
    		bl_Account_Trigger.copyAddressFields(Trigger.new);
    		
    		bl_Account_Trigger.copyPatientEmailToEmail(Trigger.new, Trigger.oldMap);
    		
    		bl_Account_Trigger.copyLeadBirthdateToBirthdate(Trigger.new);
    		
    		bl_Account_Trigger.setFieldPersonnelTrunk(Trigger.new);
    		   		
    		bl_Account_Trigger.setDIBSFIdNonSAP_Update(Trigger.new);
    		
    		bl_Account_Trigger.setDIBSFIdSAP(Trigger.new);
    		
    		bl_Account_Trigger.setAccountOwner(Trigger.new);
    		
    		bl_Account_Trigger.setAccountRegion(Trigger.new);
    		
    		bl_Account_Trigger.alignDiBSalesRep(Trigger.new);
    		
    		bl_Account_Trigger.setDefaultCurrency(Trigger.new);

			bl_Account_Trigger.setAccountISOCode(Trigger.new);

			bl_Account_Trigger.verifyAndSetPhysicianAccountId(Trigger.new, Trigger.oldMap);

			bl_Account_Trigger.verifyAttachment_DiabetesType(Trigger.new, Trigger.oldMap);
    	
    	}else if(Trigger.isDelete){
    		
    		bl_Account_Trigger.blockDeletionSAPAccount(Trigger.old);
    		
    		bl_Account_Trigger.setDIBDepartmentMerge(Trigger.old);
    		
    		bl_Account_Trigger.deleteRelatedData(Trigger.old);
    		
    		bl_Account_Trigger.setAccountShareForMerge(Trigger.old);
    	}
    	
    }else{
    	
    	if(Trigger.isInsert){
    		
    		// After Insert
    		bl_Account.createSMAXLocations(Trigger.new);
    		
    		bl_Account_Trigger.createUpdateANZPatientRelationship(Trigger.new, null);
			
			bl_Account_Trigger.mergeEURSAPPatient(Trigger.new);
			    		    		
            bl_Account_Trigger.setDIBSFIdNonSAP_Insert(Trigger.new);
                		
    	}else if(Trigger.isUpdate){
    		
    		// After Update
    		bl_Account.createSMAXLocations(Trigger.new);

            bl_Account_Trigger.copyAccountDataToPatient_Account(Trigger.new, Trigger.oldMap);
            
            bl_Account_Trigger.mergeAccountShares(Trigger.new);
            
            bl_Account_Trigger.createUpdateANZPatientRelationship(Trigger.new, Trigger.oldMap);
    		
    	}else if(Trigger.isDelete){
    		
    		bl_Account_Trigger.mergeDiBDepartments(Trigger.old);
    		
    		bl_Account_Trigger.createAccountDeleteInfo(Trigger.old);
    		
    		bl_Account_Trigger.SAPOpportunityAfterMerge(Trigger.old);
    		    		
    	}else if (Trigger.isUndelete){
    		
    		bl_Account_Trigger.deleteAccountDeleteInfo(Trigger.new);
    	}
    }

}