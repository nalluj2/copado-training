/** 
  Description - This trigger is used to Update the Primary account field of
                the campaign member with the Primary Account of the contact that
                has been added to a campaign.
  Author - Priti Jaiswal
  Created Date - 18-01-2013
  
  CR-10005		Christophe Saenen	20160112		'before update' removed from trigger
  
*/
trigger tr_CampaignMember_UpdatePrimaryAccount on CampaignMember(before insert) {
  
	Schema.sObjectType oSObjectType_Contact = clsUtil.getSObjectType('Contact');

	Set<Id> setID_Contact = new Set<Id>();
    for (CampaignMember oCampaignMember : Trigger.new){
		
		if (!String.isBlank(oCampaignMember.ContactId)){

			if (clsUtil.bIsValidId(oCampaignMember.ContactId, oSObjectType_Contact)) setID_Contact.add(oCampaignMember.ContactId);

		}

    }

	if (setID_Contact.size() == 0) return;

    Map<Id, Contact> mapContact = new Map<Id, Contact>([SELECT Id, AccountId FROM Contact WHERE Id = :setID_Contact AND AccountId != null]);

    for (CampaignMember oCampaignMember : Trigger.new){
		
		if (oCampaignMember.ContactId != null){

			if (mapContact.containsKey(oCampaignMember.ContactId)){

				oCampaignMember.Primary_Account__c = mapContact.get(oCampaignMember.ContactId).AccountId;

			}else{

				oCampaignMember.addError('No Contact found with the Id "' + oCampaignMember.ContactId + '" - please validate that the Contact exists');

			}

		}

    }

}