trigger tr_Procedural_Solutions_Kit on Procedural_Solutions_Kit__c (before insert, before update, before delete, after insert, after update, after undelete) {
	
	if(Trigger.isBefore){    
		
		if(Trigger.isInsert){
			
			if(bl_ProceduralSolutionKit.isDMLAllowed(Trigger.new) == false) return;
			
			GuidUtil.populateMobileId(Trigger.new, 'Mobile_ID__c');
			
			bl_Procedural_Solutions_Kit.checkVersionOf(Trigger.new);
			
			bl_Procedural_Solutions_Kit.setCountry(Trigger.new);
						
		}else if(Trigger.isUpdate){
			
			if(bl_ProceduralSolutionKit.isDMLAllowed(Trigger.new) == false) return;
			
			GuidUtil.populateMobileId(Trigger.new, 'Mobile_ID__c');
			
			bl_Procedural_Solutions_Kit.setCountry(Trigger.new);
			
			bl_Procedural_Solutions_Kit.blockReOpenAfterApproved(Trigger.new, Trigger.oldMap);
				
		}else if(Trigger.isDelete){
			
			if(bl_ProceduralSolutionKit.isDMLAllowed(Trigger.old) == false) return;
		}
		
	}else{
		
		if(Trigger.isInsert){
			
			bl_Procedural_Solutions_Kit.setCalculatedName(Trigger.new);
			
			bl_Procedural_Solutions_Kit.sendSubmissionEmails(Trigger.new, null);
			
		}else if(Trigger.isUpdate){
			
			bl_Procedural_Solutions_Kit.sendSubmissionEmails(Trigger.new, Trigger.oldMap);
			
			bl_Procedural_Solutions_Kit.sendReOpenEmails(Trigger.new, Trigger.oldMap);
			
			bl_Procedural_Solutions_Kit.sendRejectionEmails(Trigger.new, Trigger.oldMap);		
			
		}else if(Trigger.isUndelete){
			
			if(bl_ProceduralSolutionKit.isDMLAllowed(Trigger.new) == false) return;
		}
	}
}