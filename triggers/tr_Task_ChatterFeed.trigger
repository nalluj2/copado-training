/*
 *      Author 		: Bart Caelen
 *		Date		: 2014-02
 *      Description : This trigger will process created/updated/deleted/undeleted Task/Activity records in order to automatically 
 *						create chatter feeds on the related Account Plan (CR-1908)
 *		Version		:  1.0
*/
trigger tr_Task_ChatterFeed on Task (after delete, after insert, after undelete, after update) {

	if (trigger.isAfter){
		//------------------------------------------------------------------------------
		// CHANGE THESE SETTINGS (IF NEEDED) 
		//	- Can als be replaced by Custom Setting or another Setting Object in the Future
		//------------------------------------------------------------------------------
		string tParentIDFieldName 	= 'WhatId';
		string tFieldForURLLabel 	= 'Subject';
		set<string> setFieldNamesToMonitor = new set<string>();
			setFieldNamesToMonitor.add('Status');
			setFieldNamesToMonitor.add('ActivityDate');
		//------------------------------------------------------------------------------

		map<id, sObject> mapOldData = new map<id, sObject>();
		map<id, sObject> mapNewData = new map<id, sObject>();
		
		// Get the prefix of Account_Plan_2__c and Contact_Pland__c ids so that we can process only the tasks related to them		
		String accPlanPrefix = clsUtil.getPrefixFromSObjectName('Account_Plan_2__c');
		
		Set<String> tSObjectPrefix = new Set<String>{accPlanPrefix};	
		
		String tAction = '';
		String udAction;
		if (trigger.isInsert){
			tAction = 'INSERT';
			udAction = 'Create';			
			for (Task oSObject : trigger.new){
				
				if(oSObject.WhatId == null) continue;
				
				Id idSObject = oSObject.id;
				String parentObjectPrefix = String.valueOf(oSObject.WhatId).substring(0,3);
				
				if (tSObjectPrefix.contains(parentObjectPrefix)){
					mapNewData.put(idSObject, oSObject);
				}
			}
		}else if (trigger.isUnDelete){
			tAction = 'UNDELETE';			
			for (Task oSObject : trigger.new){
				
				if(oSObject.WhatId == null) continue;
				
				Id idSObject = oSObject.id;
				String parentObjectPrefix = String.valueOf(oSObject.WhatId).substring(0,3);
				
				if (tSObjectPrefix.contains(parentObjectPrefix)){
					mapNewData.put(idSObject, oSObject);
				}
			}
		}else if (trigger.isDelete){
			tAction = 'DELETE';	
			udAction = 'Delete';			
			for (Task oSObject : trigger.old){
				
				if(oSObject.WhatId == null) continue;
				
				Id idSObject = oSObject.id;
				String parentObjectPrefix = String.valueOf(oSObject.WhatId).substring(0,3);
				
				if (tSObjectPrefix.contains(parentObjectPrefix)){
					mapOldData.put(idSObject, oSObject);
				}
			}
		}else if (trigger.isUpdate){
			tAction = 'UPDATE';	
			udAction = 'Update';		
			for (Task oSObject : trigger.new){
				
				if(oSObject.WhatId == null) continue;
				
				Id idSObject = oSObject.id;
				String parentObjectPrefix = String.valueOf(oSObject.WhatId).substring(0,3);
				
				if (tSObjectPrefix.contains(parentObjectPrefix)){
					mapNewData.put(idSObject, oSObject);
					mapOldData.put(idSObject, trigger.oldMap.get(idSObject));
				}
			}
		}
		
		
		if (tAction != '' && (mapNewData.size()>0 || mapOldData.size()>0)){
			bl_Chatter.createChatterFeed(mapOldData, mapNewData, tAction, tParentIDFieldName, setFieldNamesToMonitor, tFieldForURLLabel);
		}
		
		List<Task> records;
			
		if(Trigger.isInsert || Trigger.isUpdate || Trigger.isUnDelete) records = Trigger.new;
		else records = Trigger.old;
		
		//Update Parent Account Plan 2 to update the Last Modified Date/By fields. If the user is SA we skip this functionality
		if(ProfileMedtronic.isSystemAdministratorProfile(UserInfo.getProfileId()) == false || Test.isRunningTest() == true){
			
			Set<Id> parentAccPlans = new Set<Id>();
							
			for(Task record : records){
								
				if(record.WhatId == null) continue;
				
				String parentObjectPrefix = String.valueOf(record.WhatId).substring(0,3);
				
				if (parentObjectPrefix == accPlanPrefix){
					parentAccPlans.add(record.WhatId);
				}
			}
			
			if(parentAccPlans.size()>0) bl_AccountPlanning.simpleUpdateParentAccountPlan2(parentAccPlans);
			
		}
		
		//Application usage details	
		if(udAction == null) return;
		
		List<Application_Usage_Detail__c> usageDetails = new List<Application_Usage_Detail__c>();
		
		for(Task record : records){
								
			if(record.WhatId == null) continue;
			
			String parentObjectPrefix = String.valueOf(record.WhatId).substring(0,3);
			
			if (parentObjectPrefix != accPlanPrefix) continue;
			
			Application_Usage_Detail__c usageDetail = new Application_Usage_Detail__c();			
			usageDetail.Action_Date__c = Date.today();
			usageDetail.Action_DateTime__c = Datetime.now();
			usageDetail.Action_Type__c = udAction;			
			usageDetail.External_Record_Id__c = record.Id;
			usageDetail.Internal_Record_Id__c = record.Id;
			usageDetail.Object__c = 'Task';			
			usageDetail.Source__c = 'SFDC';
			usageDetail.User_Id__c = UserInfo.getUserId();
					
			if (parentObjectPrefix == accPlanPrefix){
				
				usageDetail.Capability__c = 'Account Plan';
				usageDetail.Process_Area__c = 'Account Planning';
				
			}
			
			usageDetails.add(usageDetail);						
		}
		
		if(usageDetails.size()>0) insert usageDetails;
	}

}