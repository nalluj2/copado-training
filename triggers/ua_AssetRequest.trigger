/*
*	Trigger to log user actions on Asset_Request__c (create / update/ delete)
* 	Author: Rudy De Coninck
*	Creation date: 17-12-2014
*/

trigger ua_AssetRequest on Asset_Request__c (after delete, after insert, after undelete, after update) {

	//We do not track bulk operations
	if (Trigger.size == 1){
		String operation='none';
		List<Asset_Request__c> recordsToProcess = new List<Asset_Request__c>();
		if (Trigger.isDelete){
			operation = 'Delete';		
			for (Asset_Request__c a :Trigger.old){
				recordsToProcess.add(a);				
			}  
		}else{
			if (Trigger.isInsert){
				operation = 'Insert';
			}
			if(Trigger.isUpdate){
				operation='Update';
			}
			for (Asset_Request__c a :Trigger.new){
				recordsToProcess.add(a);				
			}  
		}
		if(recordsToProcess.size()>0){
			for(Asset_Request__c a : recordsToProcess){
				//Only log initial action
				if (!bl_UserAction.userActionInProgress){
					bl_UserAction.logObject(UserInfo.getUserId(), operation,'Asset_Request__c',a.id,'');
					bl_UserAction.userActionInProgress = true;  
				} 
			}
		}
	} 
}