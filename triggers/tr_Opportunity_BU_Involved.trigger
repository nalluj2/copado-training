trigger tr_Opportunity_BU_Involved on Opportunity_BU_Involved__c (before insert, before update, before delete, after delete, after insert, after undelete, after update) {
	
	if(Trigger.isBefore){
		
		if(Trigger.isInsert){
			
			bl_Opportunity_BU_Involved.setUniqueKey(Trigger.new);
			
		}else if (Trigger.isUpdate){
			
			bl_Opportunity_BU_Involved.setUniqueKey(Trigger.new);
			
		}else if (Trigger.isDelete){
			
		}
		
	}else{
		
		if(Trigger.isInsert){
			
			bl_Opportunity_BU_Involved.calculateOpportunityBUInvolvement(Trigger.new);
			
		}else if (Trigger.isUpdate){
			
			bl_Opportunity_BU_Involved.calculateOpportunityBUInvolvement(Trigger.new);
			
		}else if (Trigger.isDelete){
			
			bl_Opportunity_BU_Involved.calculateOpportunityBUInvolvement(Trigger.old);
			
		}else if (Trigger.isUndelete){
			
			bl_Opportunity_BU_Involved.calculateOpportunityBUInvolvement(Trigger.new);			
		}
	}    
}