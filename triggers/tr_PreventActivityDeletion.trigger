/*
Created Date    : 07-Mar-2013
Description: This Trigger is use to prevent the deletion of tasks when the task is releated
            to case the the case record type is one of the OMA record type and current user
            profile is one of the OMA profile.
Author        : Kaushal Singh

*/
trigger tr_PreventActivityDeletion on Task (before delete) {
    
    if(bl_tr_PreventActivityDeletion.ProfileCheck()){
        bl_tr_PreventActivityDeletion.PreventDeleteTask(Trigger.old);
    }
}