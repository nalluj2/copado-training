trigger tr_Procedural_Solutions_Kit_Product on Procedural_Solutions_Kit_Product__c (after undelete, before delete, before insert, before update) {
	
	if(Trigger.isBefore){    
		
		if(Trigger.isInsert){
			
			if(bl_ProceduralSolutionKit.isDMLAllowed(Trigger.new) == false) return;
			
			GuidUtil.populateMobileId(Trigger.new, 'Mobile_ID__c');
			
		}else if(Trigger.isUpdate){
			
			if(bl_ProceduralSolutionKit.isDMLAllowed(Trigger.new) == false) return;
			
			GuidUtil.populateMobileId(Trigger.new, 'Mobile_ID__c');
			
		}else if(Trigger.isDelete){
			
			if(bl_ProceduralSolutionKit.isDMLAllowed(Trigger.old) == false) return;
		}
		
	}else{
		
		if(Trigger.isUndelete){
			
			if(bl_ProceduralSolutionKit.isDMLAllowed(Trigger.new) == false) return;
		}
	}    
}