/*
 *      Author 		: Bart Caelen
 *		Date		: 2014-05
 *      Description : Based on a setting Follow_Task__c which is defined on Country (DIB_Country__c) we determine if the Creator of a Task will be defined as a follower of the created task.
 *						If the Task becomes completed, we will set the Creator of the task to Un-Follow the task while potential other followers remain followers of the task.
 *		Version		: 1.0
 * ------------------------------------------------------------------------------------
 *      Author 		: Bart Caelen
 *		Date		: 2014-06
 *      Description : CR-5315 - When the creator follows the task or unfollows the task when it's completed, a Chatter feed will be created
 *						When the task is completed we als delete the Chatter Feed that is created when the user start to follow the task when the task is created
 *		Version		: 2.0
 * ------------------------------------------------------------------------------------
 *      Author 		: Bart Caelen
 *		Date		: 2014-07-18
 *      Description : CR-5836 - If the Creator and Owner (Assigned User) of a Task are equal, we don't need to follow the task and we don't need to create a chatter feed
 *		Version		: 2.1
 * ------------------------------------------------------------------------------------
 *      Author 		: Bart Caelen
 *		Date		: 2020-10-01
 *      Description : CR-28123 - Only process Task with a predefined Record Type
 *		Version		: 2.1
*/
trigger tr_Task_FollowTask on Task (after insert, after update) {

	// Only execute this logic for the following Task Record Types
	Set<Id> setID_RecordType = new Set<Id>();
	setID_RecordType.add(clsUtil.getRecordTypeByDevName('Task', 'Generic_Task').Id);

	if (trigger.isAfter){

		Set<Id> setID_User = new Set<Id>();
		String tFeedBody_Created = 'Created a new task for user ';

		// Read the Country defined settings based on the Country of the CreatedById of the Task to check if the Task Creator will have to follow or unfollow the Task record
		if (trigger.isInsert || trigger.isUpdate){

			// Get the Country Names that have the setting Follow_Task__c = true
			Set<String> setCountryWithFollowTask = new Set<String>();
			for (DIB_Country__c oCountry : [SELECT Id, Name, Follow_Task__c FROM DIB_Country__c WHERE Follow_Task__c = true]){
				setCountryWithFollowTask.add(oCountry.Name.toLowerCase());
			}

			// Get the CreatedById's from the processing Task records
			Set<Id> setID_CreatedById = new Set<Id>();
			for (Task oTask : trigger.new){
				setID_CreatedById.add(oTask.CreatedById);
			}

			// Get the UserId's based on the collected Countries which have Follow_Task__c = true and which are the creator of one or more processing Task records
			// This collection can be used to determine if the Creator of the task needs to be set as a follower of that Task record - if he is in the collection, he needs to become a follower
			for (User oUser : [SELECT Id, CountryOR__c FROM User WHERE Id = :setID_CreatedById AND CountryOR__c = :setCountryWithFollowTask]){
				setID_User.add(oUser.Id);
			}
		}


		if (trigger.isInsert){

			List<EntitySubscription> lstEntitySubscription_Insert = new List<EntitySubscription>();
			List<FeedItem> lstFeedItem_Insert = new List<FeedItem>();
			Map<Id, Task> mapID_Task = new Map<Id, Task>();
			Set<Id> setID_User_Assigned = new Set<Id>();

			// Collect the Assigned User for the processing Tasks
			List<Task> lstTask_Processing = new List<Task>();	
			for (Task oTask : trigger.new){

				if (
					(setID_RecordType.contains(oTask.RecordTypeId))
					&& (setID_User.contains(oTask.CreatedByID))
					&& (oTask.OwnerId != oTask.CreatedById)			//- BC - 20140718 - CR-5836
				){
				
					lstTask_Processing.add(oTask);			//- BC - 20140718 - CR-5836
					setID_User_Assigned.add(oTask.OwnerId);
				
				}

			}

			// SOQL additional fields for the collected AssignedUser ID's
			Map<Id, User> mapID_User_Assigned = new map<Id, User>([SELECT ID, Name FROM User WHERE Id = : setID_User_Assigned]);

			// When a Task is created we need to add the creator of the task (CreatedById) as a follower of this record, but only if this is specified for the country of the creator
			for (Task oTask : lstTask_Processing){				//- BC - 20140718 - CR-5836 - Replaced trigger.new by lstTask_Processing

				mapID_Task.put(oTask.Id, oTask);
				if (setID_User.contains(oTask.CreatedByID)){

					Boolean bCreateEntitySubscription = false;

					// Create a Chatter Feed
					FeedItem oFeedItem = new FeedItem();
						oFeedItem.ParentId = oTask.CreatedById;
						oFeedItem.LinkUrl = '/' + oTask.Id;
						oFeedItem.Title = oTask.Subject;
						if (oTask.Status == 'Completed'){
							oFeedItem.Body = 'Task for user ' + mapID_User_Assigned.get(oTask.OwnerId).Name + ' is completed';
							bCreateEntitySubscription = false;
						}else{
							oFeedItem.Body = tFeedBody_Created + mapID_User_Assigned.get(oTask.OwnerId).Name;
							bCreateEntitySubscription = true;
						}

					if (bCreateEntitySubscription){
						EntitySubscription oEntitySubscription = new EntitySubscription();
							oEntitySubscription.parentId = oTask.Id;
							oEntitySubscription.subscriberid = oTask.CreatedById;
						lstEntitySubscription_Insert.add(oEntitySubscription);
					}

					lstFeedItem_Insert.add(oFeedItem);
				}
			}


			if (lstEntitySubscription_Insert.size()>0){
				Savepoint oSavePoint = Database.setSavepoint();
				try{
					insert lstEntitySubscription_Insert;
					insert lstFeedItem_Insert;
				}catch(Exception oEX){
					Database.rollback(oSavePoint);
				}
			}


		}else if (trigger.isUpdate){


			List<EntitySubscription> lstEntitySubscription_Delete = new List<EntitySubscription>();
			List<FeedItem> lstFeedItem_Insert = new List<FeedItem>();
			List<FeedItem> lstFeedItem_Delete = new List<FeedItem>();
			Map<Id, Task> mapID_Task = new Map<Id, Task>();
			Set<Id> setID_User_Assigned = new Set<Id>();

			// Based on the processing Task records we need to collect the Task ID and the CreatedByID of that Task if the Task is "Completed"
			//	For these records we need to unsubscribe the followers - but only the follower with an ID that is equal to the CreatedByID 
			Map<Id, Id> mapTaskID_CreatedById = new Map<Id, Id>();
			for (Task oTask : trigger.new){

				if (
					(setID_RecordType.contains(oTask.RecordTypeId))
					&& (oTask.OwnerId != oTask.CreatedById)			//- BC - 20140718 - CR-5836
				){			

					mapID_Task.put(oTask.Id, oTask);

//					if ( (oTask.Status == 'Completed') && (setID_User.contains(oTask.CreatedByID)) ){
					if (
							(oTask.Status == 'Completed') 
							&& (trigger.oldMap.get(oTask.Id).Status != oTask.Status)
							&& (setID_User.contains(oTask.CreatedByID))
					){
						mapTaskID_CreatedById.put(oTask.Id, oTask.CreatedById);
						mapID_Task.put(oTask.Id, oTask);
						setID_User_Assigned.add(oTask.OwnerId);
					}
				}
			}

			if (mapTaskID_CreatedById.size()>0){
				// SOQL additional fields for the collected AssignedUser ID's
				Map<Id, User> mapID_User_Assigned = new map<Id, User>([SELECT ID, Name FROM User WHERE Id = : setID_User_Assigned]);

				// Get all Entitiy Subscription records for the processing Tasks (ParentID) and the CreatedByID of the processing Tasks (SubscriberId)
				List<EntitySubscription> lstEntitySubscription = 
					[
						SELECT ID, ParentId, SubscriberId
						FROM EntitySubscription 
						WHERE ParentId = : mapTaskID_CreatedById.keySet()
						AND SubscriberId = : mapTaskID_CreatedById.values()
					];

				// Create a map that combines the Parent ID (Task ID) and the Subscriber ID (Task Creator ID) with the corresponding Entity Subscription
				//	We need this map because the above SOQL might select records that we don't need to process because we only need to process a specific ParentID - SubscriberID combination
				Map<String, EntitySubscription> mapKey_EntitySubscription = new Map<String, EntitySubscription>();
				for (EntitySubscription oEntitySubscription : lstEntitySubscription){
					String tKey = clsUtil.convertSFDCID(oEntitySubscription.ParentId) + '_' + clsUtil.convertSFDCID(oEntitySubscription.SubscriberId);
					mapKey_EntitySubscription.put(tKey, oEntitySubscription);
				}

				// Loop through the mapTaskID_CreatedById which contains the Task ID and CreatedByID of the Completed Tasks and remove the follower (CreatedById)
				//	Only the follower which is the creator of the Task will be removed while the other followers will keep following this record
				//	When the task is completed, we need to delete the Chatter Feed that was created when the task was created (collected in setID_Parent, setLinkURL, setBody)
				Set<Id> setID_Parent = new Set<Id>();
				Set<String> setLinkURL = new Set<String>();
				Set<String> setBody = new Set<String>();
				for (Id idTask : mapTaskID_CreatedById.keySet()){
					String tKey = idTask + '_' + mapTaskID_CreatedById.get(idTask);
					if (mapKey_EntitySubscription.containsKey(tKey)){
						lstEntitySubscription_Delete.add(mapKey_EntitySubscription.get(tKey));

						// Create a Chatter Feed
						Task oTask = mapID_Task.get(idTask);
						FeedItem oFeedItem = new FeedItem();
							oFeedItem.ParentId = oTask.CreatedById;
							oFeedItem.LinkUrl = '/' + oTask.Id;
							oFeedItem.Title = oTask.Subject;
							oFeedItem.Body = 'Task for user ' + mapID_User_Assigned.get(oTask.OwnerId).Name + ' is completed';
						lstFeedItem_Insert.add(oFeedItem);

						setID_Parent.add(oTask.CreatedById);
						setLinkURL.add('/' + oTask.Id);
						setBody.add(tFeedBody_Created + mapID_User_Assigned.get(oTask.OwnerId).Name);
					}
				}

				// Get the FeedItems that can be deleted based on the ParentId
				List<FeedItem> lstFeedItem = [SELECT Id, ParentID, Body, LinkURL FROM FeedItem WHERE ParentId = :setID_Parent];
				for (FeedItem oFeedItem : lstFeedItem){
					// If the FeedItem contains a collected LinkURL and a collected Body, it can be deleted
					String tLinkURL = oFeedItem.LinkURL;
					if (setLinkURL.contains(tLinkURL)){
						String tBody = oFeedItem.Body;
						if (setBody.contains(tBody)){
							lstFeedItem_Delete.add(oFeedItem);
						}

					}
				}
			}

			if (lstEntitySubscription_Delete.size()>0){
				Savepoint oSavePoint = Database.setSavepoint();
				try{
					insert lstFeedItem_Insert;
					delete lstEntitySubscription_Delete;
					delete lstFeedItem_Delete;
				}catch(Exception oEX){
					Database.rollback(oSavePoint);
				}
			}
		}
	}
}