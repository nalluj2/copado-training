/**
 *      Created Date : 20130425
 *      Description : This Trigger is used to update concatenated name of BUs related to a Call Topic
 * 
 *      Author = Manish
 *      
 */
trigger tr_CallTopicBU on Call_Topic_Business_Unit__c (after insert){
    if(Trigger.isInsert){
        bl_CallTopicUpdates.UpdateBUsConcatenated(Trigger.new);
    }
}