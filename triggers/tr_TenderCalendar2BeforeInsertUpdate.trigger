/*
 * Work Item Nr		: WI-221
 * Description		: WI-221: Create Mobile Id if null
 * Author        	: Rudy De Coninck
 * Created Date    	: 24-07-2013
 */

trigger tr_TenderCalendar2BeforeInsertUpdate on Tender_Calendar_2__c (before insert, before update) {
	GuidUtil.populateMobileId(Trigger.new, 'Mobile_ID__c');

	//select iplan bugIplan link and replicate to tenderCalendar

	Set<ID> apIds = new Set<Id>();

	
	for (Tender_Calendar_2__c tc : Trigger.new){
		apIds.add(tc.Account_Plan_ID__c);
	}
	
	List<Account_Plan_2__c> iplans = [ select Id, Account_Plan__c from Account_Plan_2__c where  id in :apIds ];
	
	Map<Id,Id> sbuToBUGMap = new Map<Id,Id>();
	
	for (Account_Plan_2__c iplan : iplans){
		sbuToBUGMap.put(iplan.id,iplan.Account_Plan__c);		
	}

	for (Tender_Calendar_2__c tc : Trigger.new){
		ID bugAPId = sbuToBUGMap.get(tc.Account_Plan_ID__c);
		if (null!= bugAPId){
		  tc.BUG_iPlan2__c = bugAPId;
		}
	}


}