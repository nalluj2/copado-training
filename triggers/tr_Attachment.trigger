//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 05-01-2016
//  Description      : APEX Trigger on Attachment 
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
trigger tr_Attachment on Attachment (before insert, after insert, after delete, after undelete) {

	if (Trigger.isBefore){
		
		if (Trigger.isInsert){

			bl_Attachment_Trigger.renameAttachment(Trigger.new);

		}

	}else{

		if (Trigger.isInsert){

			bl_Attachment_Trigger.notificationSAP(Trigger.new, 'INSERT');
			
			bl_Attachment_Trigger.sendAttachmentDocumentum(Trigger.new);
		
			bl_Attachment_Trigger.createAttachment_MITG(Trigger.new);

		}else if(Trigger.isDelete){

			bl_Attachment_Trigger.deleteAttachment_MITG(Trigger.old);

		}else if(Trigger.isUndelete){

			bl_Attachment_Trigger.createAttachment_MITG(Trigger.new);

		}

	}

}
//--------------------------------------------------------------------------------------------------------------------