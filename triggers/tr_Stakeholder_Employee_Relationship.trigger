trigger tr_Stakeholder_Employee_Relationship on Stakeholder_Employee_Relationship__c (after delete, after insert, after undelete, after update) {
    
    if(Trigger.isAfter){
    	
    	if(Trigger.isInsert){
    		
    		bl_StakeholderEmployeeRelationship.calculateStakeholderMappingRelationship(Trigger.new, null);
    		
    	}else if(Trigger.isUpdate){
    		
    		bl_StakeholderEmployeeRelationship.calculateStakeholderMappingRelationship(Trigger.new, Trigger.oldMap);
    		
    	}else if(Trigger.isDelete){
    		
    		bl_StakeholderEmployeeRelationship.calculateStakeholderMappingRelationship(Trigger.old, null);
    		
    	}else if(Trigger.isUndelete){
    		
    		bl_StakeholderEmployeeRelationship.calculateStakeholderMappingRelationship(Trigger.new, null);
    	}    	
    }
}