//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 11-04-2016
//  Description      : APEX Trigger on Opportunity Line Item 
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
trigger tr_OpportunityLineItem on OpportunityLineItem (before insert, before update, after insert, after update, after delete) {
    
	if (Trigger.isBefore){

		if (Trigger.isInsert){


			bl_OpportunityLineItem_Trigger.setPriceBasedOnAllLost(Trigger.new);


		}else if (Trigger.isUpdate){


			bl_OpportunityLineItem_Trigger.setPriceBasedOnAllLost(Trigger.new, Trigger.oldMap);


		}
    
	}else if (Trigger.isAfter){

		if (trigger.isInsert){


			bl_OpportunityLineItem_Trigger.deleteOpportunityLineItemSchedule(Trigger.new);

			bl_OpportunityLineItem_Trigger.updateOpportunityField_RLS_IHS(Trigger.new, Trigger.oldMap);

			bl_OpportunityLineItem_Trigger.setOpportunity_BUG_BU_SBU(Trigger.new);			            


		}else if (trigger.isUpdate){


			bl_OpportunityLineItem_Trigger.deleteOpportunityLineItemSchedule(Trigger.new);

			bl_OpportunityLineItem_Trigger.updateOpportunityField_RLS_IHS(Trigger.new, Trigger.oldMap);

			
		}else if (trigger.isDelete){


			bl_OpportunityLineItem_Trigger.updateOpportunityField_RLS_IHS(Trigger.new, Trigger.oldMap);

			bl_OpportunityLineItem_Trigger.setOpportunity_BUG_BU_SBU(Trigger.old);


		}

	}

}
//--------------------------------------------------------------------------------------------------------------------