trigger InstalledSoftware_AFTER_UPSERT on Installed_Software__c (after insert, after update) {
        
    //Only for O-Arm Assets
    
    Set<Id> assetIds = new Set<Id>();
        
    for(Installed_Software__c instSoft : Trigger.New){
    	
    	assetIds.add(instSoft.Asset__c);
    }
    
    Map<Id, Asset> assetMap = new Map<Id, Asset>([Select Id, RecordType.DeveloperName from Asset where Id IN :assetIds]);
    
    List<Installed_Software__c> inScope = new List<Installed_Software__c>();
    
    for(Installed_Software__c instSoft : Trigger.New){
    	
    	Asset asset = assetMap.get(instSoft.Asset__c);
    	
    	if(asset != null && asset.RecordType.DeveloperName == 'O_Arm') inScope.add(instSoft);
    	
    }
    
    if(inScope.size() > 0) bl_SynchronizationService_Source.createNotificationIfChanged(inScope, Trigger.oldMap);
}