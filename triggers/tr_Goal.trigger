//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 03-05-2019
//  Description      : APEX Trigger on Goal
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
trigger tr_Goal on Goal__c (before insert, before update){

	if (Trigger.isBefore){

		if (Trigger.isInsert){

			bl_Goal_Trigger.populateBusinessUnitGroup(trigger.new, trigger.oldMap);

		}else if (Trigger.isUpdate){

			bl_Goal_Trigger.populateBusinessUnitGroup(trigger.new, trigger.oldMap);

		}

	}

}
//--------------------------------------------------------------------------------------------------------------------