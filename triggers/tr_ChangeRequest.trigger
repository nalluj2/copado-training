//------------------------------------------------------------------------------------------
//	Unified trigger for Account
//------------------------------------------------------------------------------------------
trigger tr_ChangeRequest on Change_Request__c (after insert, after update, after delete, after undelete)  { 

	if (Trigger.isAfter){

		if (Trigger.isInsert){

			bl_ChangeRequest_Trigger.calculateTotalCostOnBundle(Trigger.new, null);

		}else if (Trigger.isUpdate){

			bl_ChangeRequest_Trigger.calculateTotalCostOnBundle(Trigger.new, Trigger.oldMap);

		}else if (Trigger.isDelete){

			bl_ChangeRequest_Trigger.calculateTotalCostOnBundle(Trigger.old, null);

		}else if (Trigger.isUndelete){

			bl_ChangeRequest_Trigger.calculateTotalCostOnBundle(Trigger.new, null);

		}

	}

}