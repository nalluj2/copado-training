trigger trgFillManufacturer on Product2 (before insert, before update) {
    id MedAccountId=null;
    list<Account> acc = [Select Id From Account where RecordType.Name=:'Competitor' and DIB_Sales_Force_Id__c=:'DO_NOT_CHANGE'];
    for(Account a:acc){
        MedAccountId=a.id;  
    }
    
    map<id,string> RecrodTypeMapping = new map<id,string>();    
    list<Recordtype> rt = [Select Id,Name From RecordType where SobjectType='Product2' and IsActive=true and (Name=:'MDT Marketing Product' or Name=:'SAP Product')];
    for(RecordType r:rt){
        RecrodTypeMapping.put(r.Id,r.Name);
    }
    /*map<string,id> MapProductBUMPGMapping = new map<string,id>();    
    list<ProductBUMPGMapping__c> lstProductBUMPGMapping = [Select BUID__c,MPGCode__c From ProductBUMPGMapping__c where BUID__c!=null and MPGCode__c!=null];
    for(ProductBUMPGMapping__c p:lstProductBUMPGMapping){
        MapProductBUMPGMapping.put(p.MPGCode__c,p.BUID__c);
    }
    */
    for(Product2 p:Trigger.new)
    {
        if(RecrodTypeMapping.get(p.RecordTypeId)!=null && MedAccountId!=null){
            p.Manufacturer_Account_ID__c = MedAccountId;    
        }
        /*
        if(p.Business_Unit_ID__c==null && p.MPG_Code_Text__c!=null && MapProductBUMPGMapping.get(p.MPG_Code_Text__c)!=null){
            p.Business_Unit_ID__c = MapProductBUMPGMapping.get(p.MPG_Code_Text__c);
        }
        */
        //Start:Added by manish
        if(RecrodTypeMapping.get(p.RecordTypeId)=='MDT Marketing Product' && p.Region_vs__c=='Europe' && p.Product_Group__c!=null)
        {
        p.Family=p.Product_Group_Name__c;
        }
        
        //End:Added by manish
        
        
        
        
        
    }           
}