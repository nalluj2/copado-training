trigger CallRecordBeforeDelete on Call_Records__c (before delete) {
    if (Trigger.isDelete){
    
         Set<Id> CallIds = new Set<Id>();
    
         for(Integer i = 0; i< Trigger.old.size();i++) {
             CallIds.add(Trigger.old[i].Id);
         }
         map<id,string> MMXCallRecords=new map<id,string>();
         List <Implant__c> MMXImplants = [select id,Call_Record_ID__c,MMX_Implant_ID_Text__c from Implant__C where Call_Record_ID__c in: CallIds and MMX_Implant_ID_Text__c!=null];
         if(MMXImplants.size()>0){
             for(implant__c i:MMXImplants){
                 MMXCallRecords.put(i.Call_Record_ID__c,i.MMX_Implant_ID_Text__c);
             }
         }
         
         
         Set<Id> visitReportsIDs = new Set<Id>();
        
        for(Integer i = 0; i< Trigger.old.size();i++) {
            System.Debug('>>>>>>>>>>>>>MMX ID ' + Trigger.old[i].Implant__r.MMX_Implant_ID_Text__c);
            if(MMXCallRecords!=null && MMXCallRecords.get(trigger.old[i].id)!=null)
            {
                 trigger.old[i].name.addError('This Call Record linked to a MMX Implant & cannot be deleted.');
            }
            else
            {
                visitReportsIDs.add(Trigger.old[i].Id);    // adding the Visit Report ID
            }                
        }
        if(visitReportsIDs.size()>0){
            // To delete the Call Topic Subject & Call Topic Products If Call Report is being deleted
            // Get all the Call Topics of selected Call Records   
            List <Call_Topics__c> existingCallTopics = [Select Id from Call_Topics__c where Call_Records__c in: visitReportsIDs] ;     
            
            if(existingCallTopics.size()>0)
            {
                List <Call_Topic_Products__c> existingCallTopicsProducts = [select id from Call_Topic_Products__c where Call_Topic__c in: existingCallTopics];
        
                List <Call_Topic_Subject__c> existingCallTopicsSubject = [select id from Call_Topic_Subject__c where Call_Topic__c in: existingCallTopics];
        
                if(existingCallTopicsProducts.size()>0)
                {
                    delete existingCallTopicsProducts;
                }
        
                if(existingCallTopicsSubject.size()>0)
                {
                    delete existingCallTopicsSubject;
                }

            }         

            List <Implant__c> existingImplants = [select id from Implant__C where Call_Record_ID__c in: visitReportsIDs];
            if(existingImplants.size()>0)
            {
                try{
                    delete existingImplants;                    
                } catch(Exception ex){ }
            }

            // End delete the Call Topic Subject, Call Topic Products & Implant If Call Report is being deleted
        }
    }

}