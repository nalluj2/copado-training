//----------------------------------------------------------------------------------------
//	Unified trigger for Account
//----------------------------------------------------------------------------------------
trigger tr_AccountPlan2 on Account_Plan_2__c (before update){

	if (Trigger.isBefore){
	
		if (Trigger.isUpdate){

			bl_AccountPlan2_Trigger.updateLastModifiedDateFields(Trigger.new, Trigger.oldMap);
		
		}
	
	} 

}