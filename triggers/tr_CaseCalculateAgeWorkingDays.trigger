trigger tr_CaseCalculateAgeWorkingDays on Case (before update) {
		
	for(Case cs : Trigger.new){
		
		//Case with Record Type for OMA
		if(bl_CaseAgeCalculation.omaRecordTypes.contains(cs.RecordTypeId)){ 
				
			if(cs.IsClosed){
				
				cs.Working_Days_Age__c = bl_CaseAgeCalculation.getClosedCaseAgeInWorkingDays(cs);
				bl_CaseAgeCalculation.alreadyCalculated.add(cs.Id);
				
			}else if(bl_CaseAgeCalculation.alreadyCalculated.contains(cs.Id) == false){
				
				cs.Working_Days_Age__c = bl_CaseAgeCalculation.getOpenCaseAgeInWorkingDays(cs);
				bl_CaseAgeCalculation.alreadyCalculated.add(cs.Id);
				
			}				
		}
	}	
}