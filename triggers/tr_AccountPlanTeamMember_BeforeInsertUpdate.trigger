/*
 * Work Item Nr		: WI-221
 * Description		: WI-221: Create Mobile Id if null
 * Author        	: Patrick Brinksma
 * Created Date    	: 23-07-2013
 *
 * CR-9701			Christophe Saenen	20160121	check for existing teammember with role Acc Team Leader
 *
 */
trigger tr_AccountPlanTeamMember_BeforeInsertUpdate on Account_Plan_Team_Member__c (before insert, before update) {
	GuidUtil.populateMobileId(Trigger.new, 'Mobile_ID__c'); 

	// get all teamleaders and put them in a map using Account_Plan__c as id
	list<Account_Plan_Team_Member__c> aptmLst = [Select id,
														Account_Plan__c
												   From	Account_Plan_Team_Member__c
												  Where	Account_Plan__r.RecordType.Name in ('MEA - CVG - BUG', 'EUR - CVG - BUG') 
												 	And Role__c = 'Acc Team Leader']; 

	Map<Id, Account_Plan_Team_Member__c> tmMap = new Map<Id, Account_Plan_Team_Member__c>(); 
	for(Account_Plan_Team_Member__c ap : aptmLst) if(!tmMap.containsKey(ap.Account_Plan__c)) tmMap.put(ap.Account_Plan__c, ap);

	for(Account_Plan_Team_Member__c member : Trigger.New) {
		member.Unique_Key__c = member.Account_Plan__c + ':' + member.Team_Member__c;
		
		// "Account Plan Team: Send email to newly added account plan team member"
		if(Trigger.isInsert || (Trigger.isUpdate && member.Role__c <> Trigger.Oldmap.get(member.Id).Role__c)) {
			if(member.Role__c == 'Acc Team Leader'
				&& tmMap.containsKey(member.Account_Plan__c) ) {
					member.adderror('Account plan already contains a member with role \'Acc Team Leader\'');
			}	
		}
	}  
}