/*
 * Author 		:	Bart Caelen
 * Date			:	20140623
 * Description	:	This trigger will process Contract records
 *					- before update, before insert (CR-4545)
 *						- BEFORE UPDATE
 *							- RULES
 *								- if the field is PO_Amount_derived_from_linked_assets__c is changed from FALSE to TRUE
 *								- if the field PO_Amount_derived_from_linked_assets__c is TRUE and the field PO_amount__c is changed
 *							- ACTIONS
 *								- update the field PO_Amount__c with the sum of the field PO_Amount2__c for the related AssetContract__c records
 *						- BEFORE INSERT
 *							- RULES
 *								- if the field is PO_Amount_derived_from_linked_assets__c is TRUE
 *							- ACTIONS
 *								- update the field PO_Amount__c with the value 0
 */
trigger tr_Contract_POAmount on Contract (before insert, before update) {


	if (trigger.isBefore){
		if (trigger.isInsert){

			for (Contract oContract : trigger.new){
				if (oContract.PO_Amount_derived_from_linked_assets__c == true){
					oContract.PO_amount__c = 0;
				}
			}


		}else if (trigger.isUpdate){
			
			//If this update was triggered by the re-calculation due to modification of child Asset Contract record, we don't need a second calculation 
			if(bl_Contract.assetContractCalculationInProcess == true) return;
			
			List<Contract> lstContract = new List<Contract>();

			for (Contract oContract : trigger.new){
				Contract oContract_Old = trigger.oldMap.get(oContract.Id);

				if (
					(oContract.PO_Amount_derived_from_linked_assets__c == true)
					&&
					(oContract.PO_Amount_derived_from_linked_assets__c != oContract_Old.PO_Amount_derived_from_linked_assets__c)
				){
					// Add Contract to list for further processing
					lstContract.add(oContract);
				}else{
					if (
						(oContract.PO_Amount_derived_from_linked_assets__c == true)
						&&
						(oContract.PO_amount__c != oContract_Old.PO_amount__c)
					){
						// Add Contract to list for further processing - we could also copy the OLD value instead
						lstContract.add(oContract);
					}
				}
			}

			if (lstContract.size() > 0){
				// Perform calculation
				lstContract = bl_Contract.getPOAmountSumFromRelatedAssetContract(lstContract);
			}
		}
	}

}