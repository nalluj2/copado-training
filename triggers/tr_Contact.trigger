trigger tr_Contact on Contact (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
 
	if(Trigger.isBefore){   
		
		if(Trigger.isInsert){
			
			bl_Contact_Trigger.populateExternalId(Trigger.new);
			
			bl_Contact_Trigger.setContactOwner(Trigger.new);
			
			bl_Contact_Trigger.setBUContactFlags(Trigger.new);
			
			bl_Contact_Trigger.setKORsBUContactFlags(Trigger.new);
			
			bl_Contact_Trigger.checkBUContactFlags(Trigger.new);
			
			// Switch Account from Master ShipTo to Sold To if needed
			bl_Contact_Trigger.handleMasterShipToAccount(Trigger.new);
			
			bl_Contact_Trigger.copySalutationToTitleANZ(Trigger.new, null);
			
			bl_Contact_Trigger.MDTEmployeeCopyFields(Trigger.new);
			
			bl_Contact_Trigger.setMDTAgentFlag(Trigger.new);	
						
			bl_Contact_Trigger.sBUValidation(Trigger.new);
			
			bl_Contact_Trigger.syncJobTitle_JobTitlevs(Trigger.new);				
			
		}else if(Trigger.isUpdate){
			
			bl_Contact_Trigger.populateExternalId(Trigger.new);
			
			bl_Contact_Trigger.setContactOwner(Trigger.new);
			
			bl_Contact_Trigger.validateOwner(Trigger.new, Trigger.oldMap);
			
			bl_Contact_Trigger.checkBUContactFlags(Trigger.new);
			
			bl_Contact_Trigger.copySalutationToTitleANZ(Trigger.new, Trigger.oldMap);
			
			bl_Contact_Trigger.updateAddresOnAccountChange(Trigger.new, Trigger.oldMap); 
			
			bl_Contact_Trigger.MDTEmployeeCopyFields(Trigger.new);
			
			bl_Contact_Trigger.setMDTAgentFlag(Trigger.new);
			
			bl_Contact_Trigger.populateUniqueHCPId(Trigger.new, Trigger.oldMap);
			
			bl_Contact_Trigger.keepIntegrateMMXFlag(Trigger.new, Trigger.oldMap);
						
			bl_Contact_Trigger.sBUValidation(Trigger.new);

			bl_Contact_Trigger.syncJobTitle_JobTitlevs(Trigger.new);				
			
		}else if(Trigger.isDelete){
			
			bl_Contact_Trigger.deleteRelatedData(Trigger.old);
			
		}		
		
	}else{
		
		if(Trigger.isInsert){
			
			bl_Contact_Trigger.createPrimaryAffiliation(Trigger.new);
			
			bl_Contact_Trigger.createMasterShipToRelationship(Trigger.new);
					
			bl_Contact_Trigger.populateUniqueHCPId_Create(Trigger.new);
						
		}else if(Trigger.isUpdate){
						
			bl_Contact_Trigger.updateRelationshipOnAccountChange(Trigger.new, Trigger.oldMap);
			
			bl_Contact_Trigger.updatePrimaryRelationshipOnChange(Trigger.new, Trigger.oldMap);
			
			bl_contact_Trigger.updateAccPlanStakeholderComments(Trigger.new, Trigger.oldMap);
			
		}else if(Trigger.isDelete){
			
			bl_Contact_Trigger.generateContactDeleteInfo(Trigger.old);
															
		}else if(Trigger.isUndelete){
			
			bl_Contact_Trigger.updateContactDeleteInfo(Trigger.new);
			
			bl_Contact_Trigger.undeleteAffiliations(Trigger.new);
			
		}

	}

}