/*
 *      Created Date : 28/10/2012
 *      Name:tr_AccountPlanTeamMember
 *      Description : This trigger is used to share Account plan with his Account plan team member
 *      Test class for the trigger: Test_ContactActionPlanNew
 *      Author = Manish Kumar Srivastava
 *		Change Log: 
 *		Patrick Brinksma - 22/07/2013 - Added logic for WI-198 - If an Account Team Member is added and is Owner of the Account Plan, set flag to prevent email being sent
 */

trigger tr_AccountPlanTeamMember on Account_Plan_Team_Member__c (before insert, before update, after insert,after update,after delete, after undelete) {
	// Execute on before insert and update
    if(Trigger.isBefore){
    	
    	if(Trigger.isInsert){
    		
    		bl_AccountPlan_TeamMember.validateTeamMemberIsOwner(Trigger.new);

    	}
    	
    }else{
   	
    	if(trigger.isInsert){
        
    		bl_AccountPlan_TeamMember.manageAccountPlanSharing(Trigger.new, null);
    		
    		bl_AccountPlan_TeamMember.validateUniqueStrategicAccountManager(Trigger.new);

			bl_AccountPlan_TeamMember.syncAPTM_OTM(Trigger.new, Trigger.oldMap, false);
    		
    	}else if(Trigger.isUpdate){
    		
    		bl_AccountPlan_TeamMember.manageAccountPlanSharing(Trigger.new, Trigger.OldMap);
    		
    		bl_AccountPlan_TeamMember.validateUniqueStrategicAccountManager(Trigger.new);
    		
			bl_AccountPlan_TeamMember.syncAPTM_OTM(Trigger.new, Trigger.oldMap, false);
    		
    	}else if(Trigger.isDelete){
        	
        	bl_AccountPlan_TeamMember.manageAccountPlanSharingDelete(Trigger.old);
        	
        	bl_AccountPlan_TeamMember.validateUniqueStrategicAccountManager(Trigger.old);
        	
			bl_AccountPlan_TeamMember.syncAPTM_OTM(Trigger.old, Trigger.oldMap, true);

    	}else if(Trigger.isUndelete){
    		
    		bl_AccountPlan_TeamMember.manageAccountPlanSharing(Trigger.new, null);
        	
        	bl_AccountPlan_TeamMember.validateUniqueStrategicAccountManager(Trigger.new);

			bl_AccountPlan_TeamMember.syncAPTM_OTM(Trigger.new, Trigger.oldMap, false);

    	}    	

    }

}