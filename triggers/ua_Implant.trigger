/*
*	Trigger to log user actions on Implant__c (create / update/ delete)
* 	Author: Rudy De Coninck
*	Creation date: 17-12-2014
*/

trigger ua_Implant on Implant__c (after delete, after insert, after undelete, after update) {

	//We do not track bulk operations
	if (Trigger.size == 1){
		String operation='none';
		List<Implant__c> recordsToProcess = new List<Implant__c>();
		if (Trigger.isDelete){
			operation = 'Delete';		
			for (Implant__c a :Trigger.old){
				recordsToProcess.add(a);				
			}  
		}else{
			if (Trigger.isInsert){
				operation = 'Insert';
			}
			if(Trigger.isUpdate){
				operation='Update';
			}
			for (Implant__c a :Trigger.new){
				recordsToProcess.add(a);				
			}  
		}
		if(recordsToProcess.size()>0){
			for(Implant__c a : recordsToProcess){
				//Only log initial action
				if (!bl_UserAction.userActionInProgress){
					
					if (ProfileMedtronic.isMMXInterface(UserInfo.getProfileId())){
						System.debug('MMX: '+a.OwnerId);
						bl_UserAction.logObject(a.OwnerId, operation,'Implant__c',a.id,'');
					}else{
						System.debug('not MMX: '+a.OwnerId);
						bl_UserAction.logObject(UserInfo.getUserId(), operation,'Implant__c',a.id,'');
					}
					
					bl_UserAction.userActionInProgress = true;  
				} 
			}
		}
	} 
}