trigger tr_CreateUserRequestAfter on Create_User_Request__c (after update) {
	
	List<Id> createUserRequestListIds =  new List<Id>();
			
	for(Create_User_Request__c request: Trigger.New){
					
		if(request.Application__c == 'Salesforce.com' && request.Request_Type__c == 'Create User'){
		
			if(request.Status__c=='In Execution'  && Trigger.oldMap.get(request.Id).Status__c=='Approved'){
				
				if(request.Profile__c == null || request.Profile__c == '' || (request.License_Type__c == 'Full Salesforce' && (request.Role__c == null || request.Role__c == ''))){
					
					request.addError('Profile and Role are mandatory fields for the ticket execution');
						
				}else{
								
					createUserRequestListIds.add(request.id);
				}
			}
		}
	}  
	
	if(createUserRequestListIds.size()>0){
					
		ba_Create_User_Process createUserProcess = new ba_Create_User_Process();
		createUserProcess.createUserRequestListIds = createUserRequestListIds;
		Database.executeBatch(createUserProcess, 200);				
	}		
}