/*
*	Trigger to log user actions on Opportunity (create / update/ delete)
* 	Author: Rudy De Coninck
*	Creation date: 17-12-2014
*/

trigger ua_Opportunity on Opportunity (after delete, after insert, after undelete, after update) {

	//We do not track bulk operations
	if (Trigger.size == 1){
		String operation='none';
		List<Opportunity> recordsToProcess = new List<Opportunity>();
		if (Trigger.isDelete){
			operation = 'Delete';		
			for (Opportunity a :Trigger.old){
				recordsToProcess.add(a);				
			}  
		}else{
			if (Trigger.isInsert){
				operation = 'Insert';
			}
			if(Trigger.isUpdate){
				operation='Update';
			}
			for (Opportunity a :Trigger.new){
				recordsToProcess.add(a);				
			}  
		}
		if(recordsToProcess.size()>0){
			for(Opportunity a : recordsToProcess){
				//Only log initial action
				if (!bl_UserAction.userActionInProgress){
					bl_UserAction.logObject(UserInfo.getUserId(), operation,'Opportunity',a.id,'');
					bl_UserAction.userActionInProgress = true;  
				} 
			}
		}
	} 
}