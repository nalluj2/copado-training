/*
  *  Trigger Name      :   CasePart_UPSERT_AFTER
  *  Created Date      :   06/08/2013
  *  Author            :   Niha Saha 
*/

trigger CasePart_UPSERT_AFTER on Workorder_Sparepart__c (after insert, after update) 
{	
	
	//Roll up Order Part prices to parent Work Order	
	Set<Id> woToCalculate = new Set<Id>();
	
	for(Workorder_Sparepart__c sPart : Trigger.New){
		
		if(Trigger.isInsert && sPart.Workorder__c != null && sPart.Order_Part_Total_Price__c != null) woToCalculate.add(sPart.Workorder__c);
				
		if(Trigger.isUpdate){
			
			if(sPart.Workorder__c != Trigger.oldMap.get(sPart.Id).Workorder__c){
			
				if(sPart.Order_Part_Total_Price__c != null) woToCalculate.add(sPart.Workorder__c);
				if(Trigger.oldMap.get(sPart.Id).Order_Part_Total_Price__c != null) woToCalculate.add(Trigger.oldMap.get(sPart.Id).Workorder__c);	
			
			}else if(sPart.Order_Part_Total_Price__c != Trigger.oldMap.get(sPart.Id).Order_Part_Total_Price__c) woToCalculate.add(sPart.Workorder__c);
		}
	}
	
	if(woToCalculate.size() > 0){
		
		CasePartTriggers.calculateWorkOrderTotal(woToCalculate);	
	}	
			
	//If we check the flag in the Custom Settings we skip the execution of this logic
	if(bl_Trigger_Deactivation.isTriggerDeactivated('CasePart_UPSERT_AFTER')) return;
	
	if(bl_SynchronizationService_Utils.isSyncProcess == false){
		
		if(Trigger.isInsert) bl_SynchronizationService_Source.createNotificationIfChanged(Trigger.New, null);
		else bl_SynchronizationService_Source.createNotificationIfChanged(Trigger.New, Trigger.OldMap);	
	}
	   
    
    List<Workorder_Sparepart__c> listOfCasePartToUpdate=new List<Workorder_Sparepart__c>();
    
    for(Workorder_Sparepart__c casepartObj:Trigger.new)
    {
        if(casepartObj.Workorder__c==null) 
        {
            listOfCasePartToUpdate.add(casepartObj);            
       }
    }
    
    if(listOfCasePartToUpdate.size()>0 && CasePartTriggers.updateFlag == false)
    {
        CasePartTriggers.Update_Tracked_Parts(listOfCasePartToUpdate);
    }    
    
    List<Workorder_Sparepart__c> listofCasePartToCreateComment = new List<Workorder_Sparepart__c>();
    List<Workorder_Sparepart__c> casePartsNewComplaintForComment = new List<Workorder_Sparepart__c>();
    
    if(Trigger.isUpdate == true) 
    {
    // added by Pavan Hegde to call the handler-- tocalculate the count of Hardware case parts, having the order status as Shipped
      CaseShipmentandPartsCountHandler.updatecaseRollup('Workorder_Sparepart__c'); 
        Integer compCount = 0;
        Map<Id, Complaint__c> complaintMap = new Map<Id, Complaint__c>();
        Map<Id, String> mapOfNameForComp = new Map<Id, String>();
        set<Id> complaintIdSet = new Set<Id>();
        
        for(Workorder_Sparepart__c casePartObj :Trigger.old) 
        {    
             if(casePartObj.Complaint__c != Trigger.new[compCount].Complaint__c) 
             {
                if(Trigger.new[compCount].Complaint__c == null) 
                {
                    complaintIdSet.add(casePartObj.Complaint__c);
                    mapOfNameForComp.put(casePartObj.Complaint__c, casePartObj.Name);
                }
            }
            compCount++;
        }
        if(complaintIdSet.size() > 0) 
        {
            for(Complaint__c compObj:[select id, Parent_Complaint_No__c, Name from Complaint__c where Id IN:complaintIdSet]) 
            {
                complaintMap.put(compObj.Id, compObj);
            }
        }
   // If Complaint is removed from Case parts then the comments gets populated on the removed Complaint     
        if(complaintMap.size() > 0)         
            Case_logic.complaintRemove(complaintMap, mapOfNameForComp, 'Workorder Sparepart', 'Delink', null);
        
                    
        for(Workorder_Sparepart__c objCasePart:Trigger.New)
        {

            Workorder_Sparepart__c objCasePart_Old = Trigger.OldMap.get(objCasePart.Id);  //-Bart Caelen - 2017-05-10 

            if(objCasePart.Complaint__c != null){
            	
            /*	 && (objCasePart.Return_Item_Status__c=='Analysis Complete' 
                || objCasePart.Return_Item_Status__c=='No Return Expected' 
                || objCasePart.Return_Item_Status__c=='Part Not Returned' 
                || objCasePart.Return_Item_Status__c=='Returned Unused'))
            {*/
            
                if(    objCasePart.Findings_and_Conclusions__c != objCasePart_Old.Findings_and_Conclusions__c                    
                    || objCasePart.Return_Item_Status__c != objCasePart_Old.Return_Item_Status__c
                    || objCasePart.Lot_Number__c != objCasePart_Old.Lot_Number__c                                        
                    || objCasePart.RI_Part__c != objCasePart_Old.RI_Part__c                    
                    || objCasePart.BAI_OOBF__c != objCasePart_Old.BAI_OOBF__c                    
                    || objCasePart.Order_Part__c != objCasePart_Old.Order_Part__c
                    || objCasePart.Software__c != objCasePart_Old.Software__c    
                    || objCasePart.Possession__c != objCasePart_Old.Possession__c
                    || objCasePart.Requested_Return__c != objCasePart_Old.Requested_Return__c
                    || objCasePart.Return_item_Tracking_info_DC_to_MFR__c != objCasePart_Old.Return_item_Tracking_info_DC_to_MFR__c
                    || objCasePart.No_Return_Justification__c != objCasePart_Old.No_Return_Justification__c
                    || objCasePart.SAP_Return__c != objCasePart_Old.SAP_Return__c
                    || objCasePart.Return_Item_Tracking_Number__c != objCasePart_Old.Return_Item_Tracking_Number__c
                    || objCasePart.ZRIC__c != objCasePart_Old.ZRIC__c
                    ) 
                {
                    listofCasePartToCreateComment.add(objCasePart);
                }
                
                if(objCasePart.Complaint__c != objCasePart_Old.Complaint__c){
                	if(Comment_Logic.commentToNewComplaint.add(objCasePart.Id) == true) casePartsNewComplaintForComment.add(objCasePart);
                }
            }

            //-Bart Caelen - 2017-05-10 - REQ005661 - START - Not needed on International
//            if (
//                (objCasePart.Complaint__c != null)
//                && (objCasePart.Return_Item_Status__c == 'Under Analysis')
//                && 
//                (
//                    (objCasePart.Findings_and_Conclusions__c != objCasePart_Old.Findings_and_Conclusions__c)
//                    || (objCasePart.Evaluation_Notes__c != objCasePart_Old.Evaluation_Notes__c)
//                    || (objCasePart.Vendor_Root_Cause_Results__c != objCasePart_Old.Vendor_Root_Cause_Results__c)
//                )
//            ){
//
//                listofCasePartToCreateComment.add(objCasePart);
//
//            }
            //-Bart Caelen - 2017-05-10 - REQ005661 - STOP
        }
    }
    //If case part is created with Analysis complete, create a comment
    if(Trigger.isInsert == true) 
    {
    // added by Pavan Hegde to call the handler-- tocalculate the count of Hardware case parts, having the order status as Shipped
  CaseShipmentandPartsCountHandler.updatecaseRollup('Workorder_Sparepart__c'); 
     
        for(Workorder_Sparepart__c objCasePart:Trigger.New)
        {
            /*
            if(objCasePart.Complaint__c != null && (objCasePart.Return_Item_Status__c=='Analysis Complete' 
                || objCasePart.Return_Item_Status__c=='No Return Expected' 
                || objCasePart.Return_Item_Status__c=='Part Not Returned' 
                || objCasePart.Return_Item_Status__c=='Returned Unused'))
            {
                listofCasePartToCreateComment.add(objCasePart);
            }*/
            if(objCasePart.Complaint__c != null){
            	casePartsNewComplaintForComment.add(objCasePart);
               	Comment_Logic.commentToNewComplaint.add(objCasePart.Id);
            }
        }
    }  
    
    //If any case parts need to have comments created, call logic to create them
    if(listofCasePartToCreateComment.size()>0 && Comment_Logic.firstRun == true)
    {
            Comment_Logic.CreateCommentFromCP(listofCasePartToCreateComment, Trigger.oldMap);
            Comment_Logic.firstRun = false;
    }    
    
    if(casePartsNewComplaintForComment.size() > 0){
        	Comment_Logic.CreateCommentFromCP(casePartsNewComplaintForComment, null);
    }
      //CasePart_Logic.Associate_Case_Parts(Trigger.new);  
}