//---------------------------------------------------------------------------------------------------------
//	Unified trigger for Opportunity_Lot__c
//---------------------------------------------------------------------------------------------------------
trigger tr_OpportunityLot on Opportunity_Lot__c (after insert, after update, after delete, after undelete){

	if (Trigger.isAfter){
	
		if (Trigger.isInsert){

			bl_OpportunityLot_Trigger.calculateRLSFieldOnOpportunity(Trigger.new, null);
		
		}else if (Trigger.isUpdate){

			bl_OpportunityLot_Trigger.calculateRLSFieldOnOpportunity(Trigger.new, Trigger.oldMap);

		}else if (Trigger.isDelete){

			bl_OpportunityLot_Trigger.calculateRLSFieldOnOpportunity(Trigger.old, null);

		}else if (Trigger.isUndelete){

			bl_OpportunityLot_Trigger.calculateRLSFieldOnOpportunity(Trigger.new, null);

		}
	
	}
	
}
//---------------------------------------------------------------------------------------------------------