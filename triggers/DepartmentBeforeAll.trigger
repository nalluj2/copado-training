/**
 * Creation date : 23/02/2010
 * Description : There can be only one unique department for a SF account.
                Enforce via code (department name cannot be the same for the same account ID)
 * Created by : ABSI / Miguel Coimbra
 
 * updated by Manish: 28/10/2012, in order to update Account Plan if there exist Europe Diabetes core Account Plan for related Account of Diabetes Segment 
 **/

trigger DepartmentBeforeAll on DIB_Department__c (before insert, before update) {
    
    GuidUtil.populateMobileId(Trigger.new, 'Mobile_ID__c');
    
    Set<Id> accIds = new Set<Id>();
    
    for(DIB_Department__c dibDep : Trigger.new){
    
    	if(dibDep.Account__c != null) accIds.add(dibDep.Account__c);
    }
    
    // Link Account Plan 
    Map<Id, Id> accPlansByAccount = new Map<Id, Id>();
           
    if(accIds.size()>0){  	
    	
        for(Account_Plan_2__c accPlan : [select Id, Account__c from Account_Plan_2__c where Account__c IN: accIds and Sub_Business_Unit__r.Name = 'Diabetes Core' AND Sub_Business_Unit__r.Business_Unit__r.Company__r.Name IN ('Europe') ORDER BY End_Date__c ASC NULLS LAST]){
                	
            accPlansByAccount.put(accPlan.Account__c, accPlan.Id);            
        }
    }
                
    for(DIB_Department__c dibDep : Trigger.new){
    	
    	dibDep.Account_Plan__c = accPlansByAccount.get(dibDep.Account__c);    	         
    }                  
   
    // Check duplicate Diabetes Segmentations
     
    List <DIB_Department__c> existingDepartments = [Select Id, Department_ID__c, Department_ID__r.Name, Account__c, IsTransfer__c from DIB_Department__c where Account__c IN: accIds] ; 
    
    // If there is already one Account with a department,  catch it and drop an error Message if it's the same department name (department__c)
    Map<String, Integer> mNewDepartments = new Map<String, Integer>();
    
    for (DIB_Department__c newDep : Trigger.new) {
    	
        String tmpString = '' + newDep.Department_ID__c + newDep.Account__c;
        
        Integer iCount = mNewDepartments.get(tmpString);
        
        if(iCount == null){
        
                iCount = 1;
        }else{
                iCount++;
        }
        
        mNewDepartments.put(tmpString, iCount);
    }
    
    for (DIB_Department__c newDep : Trigger.New){
        
        String tmpString = '' + newDep.Department_ID__c + newDep.Account__c;
        
        Integer iCount = mNewDepartments.get(tmpString);
        
        if(iCount != null && iCount >= 2 && newDep.IsTransfer__c==false){
        	
            newDep.Department_ID__c.addError(FinalConstants.N0_MULTIPLE_DEPARTMENTS_ERROR_MSG); 
            continue;
        }
        
        for (DIB_Department__c exDep : existingDepartments) {
            
            // Same Account, Same Department but not same Id otherwise it will fire also the error msn on update. 
            if (newDep.Account__c == exDep.Account__c && exDep.Department_ID__c == newDep.Department_ID__c && newDep.Id != exDep.Id && newDep.IsTransfer__c==false){
                newDep.Department_ID__c.addError(FinalConstants.N0_MULTIPLE_DEPARTMENTS_ERROR_MSG); 
            }
            // new  201003 : Can only have 2 possible combinations: Adult & Pediatric or Adult/Pediatric not both ! 
            /*if (newDep.Account__c == exDep.Account__c && newDep.Id != exDep.Id){
                if (exDep.Department__c == 'Adult' &&  newDep.Department__c == 'Adult/Pediatric'
                    || exDep.Department__c == 'Pediatric' &&  newDep.Department__c == 'Adult/Pediatric'
                    || exDep.Department__c == 'Adult/Pediatric' &&  newDep.Department__c == 'Adult'
                    || exDep.Department__c == 'Adult/Pediatric' &&  newDep.Department__c == 'Pediatric'             
                ){
                    newDep.Department__c.addError(FinalConstants.N0_MULTIPLE_DEPARTMENTS_ERROR_MSG);            
                }
            }*/     
        }
    }
}