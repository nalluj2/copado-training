//------------------------------------------------------------------------------------------------------------
//	Author		:	Bart Caelen
//	Created		:	2014-10-22
//	Description	:   APEX Trigger on DIB_Department__c
//                  When a DIB_Department__c is ceatead, the field isSales_Force_Account__c (Sales Force Account) 
//					on the related Account will be updated to true (if it isn't true already)
//	CR 			:	CR-5831
//------------------------------------------------------------------------------------------------------------------------------
trigger tr_DIBDepartment on DIB_Department__c (after insert) {

	if (trigger.isAfter){

		if (trigger.isInsert){
			Set<Id> setID_Account = new Set<Id>();

			for (DIB_Department__c oDIBDepartment : trigger.new){
				setID_Account.add(oDIBDepartment.Account__c);
			}

			Map<Id, Account> mapAccount = new Map<Id, Account> 
				([
					SELECT Id, isSales_Force_Account__c
					FROM Account
					WHERE Id = :setID_Account
				]);

			List<Account> lstAccount_Update = new List<Account>();
			for (DIB_Department__c oDIBDepartment : trigger.new){
				Account oAccount = mapAccount.get(oDIBDepartment.Account__c);
				if (!oAccount.isSales_Force_Account__c){
					oAccount.isSales_Force_Account__c = true;
					lstAccount_Update.add(oAccount);
				}
			}

			if (lstAccount_Update.size() > 0){
				update lstAccount_Update;
			}
		}

	}

}
//------------------------------------------------------------------------------------------------------------------------------