/*
 * Work Item Nr		: WI-221
 * Description		: WI-221: Create Mobile Id if null
 * Author        	: Patrick Brinksma
 * Created Date    	: 23-07-2013
 */
trigger tr_AccountPlanObjective_BeforeInsertUpdate on Account_Plan_Objective__c (before insert, before update) {

	GuidUtil.populateMobileId(Trigger.new, 'Mobile_ID__c');
	
	//- Bart Caelen - 20161125 - CR-12913 - START
	Id rtgRecordType = clsUtil.getRecordTypeByDevName('Account_Plan_Objective__c', 'RTG').Id;

	// For duplicate check
	Set<Id> setID_BusinessObjective = new Set<Id>();
	// For Department check
	Set<Id> setID_AccountPlan = new Set<Id>();
		
	for (Account_Plan_Objective__c oAPO : Trigger.New){
		
		if(oAPO.RecordTypeId == rtgRecordType){
			
			setID_BusinessObjective.add(oAPO.Business_Objective__c);
			
			if(oAPO.Department__c != null) setID_AccountPlan.add(oAPO.Account_Plan__c);
		}
	}
	
	Map<Id, Business_Objective__c> mapBusinessObjective = new Map<Id, Business_Objective__c>();
	
	if(setID_BusinessObjective.size() > 0 ){	
		
		mapBusinessObjective = new Map<Id, Business_Objective__c>([SELECT Id, Type__c FROM Business_Objective__c WHERE Id = :setID_BusinessObjective]);
	}
		
	Map<Id, Map<String, DIB_Department__c>> mapAccountDepartments = new Map<Id, Map<String, DIB_Department__c>>();
	Map<Id, Id> accPlanAccount = new Map<Id, Id>();
	
	if(setID_AccountPlan.size() > 0){
		
		for(Account_Plan_2__c accPlan : [Select Id, Account__c from Account_Plan_2__c where Id IN :setID_AccountPlan]){
			
			accPlanAccount.put(accPlan.Id, accPlan.Account__c);
		}
		
		for(Account acc : [Select Id, (Select Id, Department_ID__r.Name from Segmentations__r where Active__c = true) from Account where Id IN (Select Account__c from Account_Plan_2__c where Id IN :setID_AccountPlan)]){
				
			Map<String, DIB_Department__c> accDepartments = new Map<String, DIB_Department__c>();
			
			for(DIB_Department__c department : acc.Segmentations__r){	
				
				accDepartments.put(department.Department_ID__r.Name, department);
			}
			
			mapAccountDepartments.put(acc.Id, accDepartments);
		}				
	}
	
	for(Account_Plan_Objective__c objective : Trigger.New){
		
		if (objective.RecordTypeId == rtgRecordType && 
			objective.Business_Objective__c != null && 
			mapBusinessObjective.get(objective.Business_Objective__c).Type__c == 'Quantitative'){
					
				objective.Unique_Key__c = objective.Account_Plan__c + ':' + objective.Business_Objective__c ;				
				if(objective.Department__c != null) objective.Unique_Key__c += ':' + objective.Department__c;			
										
		}else{
			
			objective.Unique_Key__c = null;
		}
		
		if(objective.Department__c != null){
			
			Map<String, DIB_Department__c> accDepartments = mapAccountDepartments.get(accPlanAccount.get(objective.Account_Plan__c));
			
			if(accDepartments.containsKey(objective.Department__c)){
				
				objective.Account_Department__c = accDepartments.get(objective.Department__c).Id;
					
			}else if(accDepartments.containsKey('Adult/Pediatric')){
				
				objective.Account_Department__c = accDepartments.get('Adult/Pediatric').Id;
				
			}else{
				
				objective.addError('The selected Department is not available for this Account. Please choose another Department or create a Diabetes Segmentation record for the Department first.');				
			}
		}		
	}
}