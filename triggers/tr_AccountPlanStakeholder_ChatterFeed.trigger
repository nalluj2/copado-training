/*
 *      Author 		: Bart Caelen
 *		Date		: 2014-02
 *      Description : This trigger will process created/updated/deleted/undeleted Account_Plan_Stakeholder__c records in order to automatically 
 *						create chatter feeds on the related Account Plan (CR-1908)
 *		Version		:  1.0
*/
trigger tr_AccountPlanStakeholder_ChatterFeed on Account_Plan_Stakeholder__c (after delete, after insert, after undelete, after update) {

	if (trigger.isAfter){
		//------------------------------------------------------------------------------
		// CHANGE THESE SETTINGS (IF NEEDED) 
		//	- Can als be replaced by Custom Setting or another Setting Object in the Future
		//------------------------------------------------------------------------------
		string tParentIDFieldName 	= 'Account_Plan__c';
		string tFieldForURLLabel 	= 'Name';
		set<string> setFieldNamesToMonitor = clsUtil.getUpdateableFields('Account_Plan_Stakeholder__c');	// All updateable fields will be monitored
		//------------------------------------------------------------------------------
		
		String tAction = '';
		String udAction;
		if (trigger.isInsert){
			tAction = 'INSERT';			
			udAction = 'Create';
		}else if (trigger.isUnDelete){
			tAction = 'UNDELETE';			
		}else if (trigger.isDelete){
			tAction = 'DELETE';	
			udAction = 'Delete';		
		}else if (trigger.isUpdate){
			tAction = 'UPDATE';
			udAction = 'Update';			
		}
		
		if (tAction != ''){
			bl_Chatter.createChatterFeed(trigger.oldMap, trigger.newMap, tAction, tParentIDFieldName, setFieldNamesToMonitor, tFieldForURLLabel);
		}
		
		List<Account_Plan_Stakeholder__c> records;
			
		if(Trigger.isInsert || Trigger.isUpdate || Trigger.isUnDelete) records = Trigger.new;
		else records = Trigger.old;
			
		//Update Parent Account Plan 2 to update the Last Modified Date/By fields. If the user is SA we skip this functionality
		if(ProfileMedtronic.isSystemAdministratorProfile(UserInfo.getProfileId()) == false){
			
			Set<Id> parentPlans = new Set<Id>();
			
			for(Account_Plan_Stakeholder__c record : records){
				parentPlans.add(record.Account_Plan__c);
			}
			
			if(parentPlans.size()>0) bl_AccountPlanning.simpleUpdateParentAccountPlan2(parentPlans);
		}
		
		//Application usage details	
		if(udAction == null) return;
		
		List<Application_Usage_Detail__c> usageDetails = new List<Application_Usage_Detail__c>();
		
		for(Account_Plan_Stakeholder__c record : records){
								
			Application_Usage_Detail__c usageDetail = new Application_Usage_Detail__c();			
			usageDetail.Action_Date__c = Date.today();
			usageDetail.Action_DateTime__c = Datetime.now();
			usageDetail.Action_Type__c = udAction;
			usageDetail.Capability__c = 'Account Plan';
			usageDetail.External_Record_Id__c = record.Id;
			usageDetail.Internal_Record_Id__c = record.Id;
			usageDetail.Object__c = 'Account_Plan_Stakeholder__c';
			usageDetail.Process_Area__c = 'Account Planning';
			usageDetail.Source__c = 'SFDC';
			usageDetail.User_Id__c = UserInfo.getUserId();
						
			usageDetails.add(usageDetail);
		}	
		
		if(usageDetails.size()>0) insert usageDetails;
	}
}