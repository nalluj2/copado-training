/**
 *      Created Date : 20130222
 *      Description : This Triggrt is used to update concatenate all the subjects of call topic belonging to a 
                      call record and put this in Subjects field on call record.
 * 
 *      Author = Kaushal
 *      Modified By: Manish(R8.0-36 CR-1928) Date: 23/4/2013: To update related Subject name on Call topic
 */
trigger tr_UpdateSubjectsOnCallRecord on Call_Topic_Subject__c (after insert){
    
    if(trigger.IsInsert){
        bl_tr_UpdateSubjectsOnCallRecord.UpdateSubjectsConcatenated(Trigger.newmap.keyset());
        //start: Added by Manish
        bl_CallTopicUpdates.UpdateSubjectsConcatenated(Trigger.new);
        //End: Added by manish
    }
}