trigger ImplantedProductAfterUpsert on Implanted_Product__c (after insert,after update) {

	if (bl_Trigger_Deactivation.isTriggerDeactivated('ip_ImplantedProductAfterUpsert')) return;

    if (
        (trigger.isinsert && GTutil.chkinsertImplantedProduct) 
        || (trigger.isupdate && GTutil.chkUpdateImplantedProduct)
    ){
        List<Id> calltopicIds = new List<Id>();
        List<Implanted_Product__c> lstimpprdt = new List<Implanted_Product__c>();
        List<Id>calltopicId = new List<Id>();

        Set<Id> ImplantIds = new Set<Id>();
        for (Implanted_Product__c implnt:trigger.new){
            ImplantIds.add(implnt.Implant_ID__c);      
        }

        // Get a list of Implants that we need to process
        List<Implant__c> lstImplant = [SELECT id, Name, Call_Record_ID__c, Serial_Numbers__c FROM Implant__c WHERE id in :ImplantIds];
      
        // Get a map of Call Record ID and Implant ID
        Map<id,id> MapofImplantAndCallrecordId = new Map<id,id>();
        for (Implant__c oImplant : lstImplant){
            if (clsUtil.isNull(oImplant.Call_Record_ID__c, '') != ''){
                MapofImplantAndCallrecordId.put(oImplant.Call_Record_ID__c ,oImplant.id);
                calltopicIds.add(oImplant.Call_Record_ID__c);
            }
        }

        // Get a list of Implanted Products for each processing Implant in a Map
        Map<id, List<Implanted_Product__c>> MapofImplantIdandImplntdProduct = new Map<id, List<Implanted_Product__c>>();
        List<Implanted_Product__c> lstas1 = 
            [
                SELECT id, Name, Product2__c, Product2__r.name, Implant_ID__c, SerialNumber__c 
                FROM Implanted_Product__c 
                WHERE Implant_ID__c in :ImplantIds
            ];

        for (Implanted_Product__c imprdt : lstas1){
            List<Implanted_Product__c> lstimprodct = MapofImplantIdandImplntdProduct.get(imprdt.Implant_ID__c);
            if (lstimprodct == null){
                lstimprodct = new List<Implanted_Product__c>();
                lstimprodct.add(imprdt);
                MapofImplantIdandImplntdProduct.put(imprdt.Implant_ID__c, lstimprodct);
            }else{
                 lstimprodct.add(imprdt);
                 MapofImplantIdandImplntdProduct.put(imprdt.Implant_ID__c, lstimprodct);
             }
        }

        // Get a list of Call Topics for each collected Call Record filtered on the Call Activity Type and Call Category that are defined in the Custom Setting
        Map<id,List<Call_Topics__c>> MapofCallrecordandCallTopicIds = new Map<id,List<Call_Topics__c>>();
        Europe_Call_Record__c EurCallDetails = Europe_Call_Record__c.getInstance();
        id idCallActivity = null;
        id idCallCategory = null;        
        id idCallActivity_AFS = null;   //- BC - 20160128 - CR-10314 - Added
        id idCallCategory_AFS = null;   //- BC - 20160128 - CR-10314 - Added        
        if (EurCallDetails != null){
           idCallActivity = EurCallDetails.Implant_Support__c;  
           idCallCategory = EurCallDetails.Support__c;  

           idCallActivity_AFS = EurCallDetails.Case_Support__c; //- BC - 20160128 - CR-10314 - Added
           idCallCategory_AFS = EurCallDetails.Support__c;      //- BC - 20160128 - CR-10314 - Added
        }    

        //- BC - 20160128 - CR-10314 - Adjusted the SOQL - START
        List<Call_Topics__c> lstcltpc=
            [
                SELECT id, name, Call_Records__c, Call_Topic_Products_Concatendated__c 
                FROM Call_Topics__c 
                WHERE 
                    Call_Records__c in: calltopicIds 
                    AND 
                    (
                        (
                            Call_Topics__c.Call_Activity_Type__c = :idCallActivity 
                            AND Call_Topics__c.Call_Category__c = :idCallCategory
                        )
                        OR
                        (
                            Call_Topics__c.Call_Activity_Type__c = :idCallActivity_AFS 
                            AND Call_Topics__c.Call_Category__c = :idCallCategory_AFS
                        )
                    )
            ];
//        List<Call_Topics__c> lstcltpc=
//            [
//                SELECT id, name, Call_Records__c, Call_Topic_Products_Concatendated__c 
//                FROM Call_Topics__c 
//                WHERE Call_Records__c in: calltopicIds and Call_Topics__c.Call_Activity_Type__c = :idCallActivity and Call_Topics__c.Call_Category__c = :idCallCategory
//            ];
        //- BC - 20160128 - CR-10314 - STOP

        for (Call_Topics__c cltpc : lstcltpc){
            calltopicId.add(cltpc.id); 
            List<Call_Topics__c> lstCallTopic = MapofCallrecordandCallTopicIds.get(cltpc.Call_Records__c);
            if (lstCallTopic == null){
                lstCallTopic = new List<Call_Topics__c>();
                lstCallTopic.add(cltpc);
                MapofCallrecordandCallTopicIds.put(cltpc.Call_Records__c, lstCallTopic);
            }else{
                 lstCallTopic.add(cltpc);
                 MapofCallrecordandCallTopicIds.put(cltpc.Call_Records__c, lstCallTopic);
             }
        }

        List<Call_Topic_Products__c> lstCallProduct_Insert =new List<Call_Topic_Products__c>();
        List<Call_Topic_Products__c> lstCalltopic = 
            [
                SELECT id,Call_Topic__c,  Product__c 
                FROM Call_Topic_Products__c 
                WHERE Call_Topic__c in :calltopicId
            ];

        for (Call_Topics__c cltpc : lstcltpc){
            cltpc.Call_Topic_Products_Concatendated__c = '';
            Id implantid = MapofImplantAndCallrecordId.get(cltpc.Call_Records__c);
            List<Implanted_Product__c> lstimpltprdct = MapofImplantIdandImplntdProduct.get(implantid);
            for (Implanted_Product__c impdt : lstimpltprdct){
                if (cltpc.Call_Topic_Products_Concatendated__c == null){
                    cltpc.Call_Topic_Products_Concatendated__c = impdt.Product2__r.name;
                }else{
                    cltpc.Call_Topic_Products_Concatendated__c = impdt.Product2__r.name + ';' + cltpc.Call_Topic_Products_Concatendated__c;
                }
                Call_Topic_Products__c Callprodobj = new Call_Topic_Products__c(Call_Topic__c = cltpc.id, Product__c = impdt.Product2__c);
                lstCallProduct_Insert.add(Callprodobj);
            }
        }
        
        //added by priti to update new serial no field with the impalnt product serial no on implant
        for (Implant__c imp : lstImplant){
            imp.Serial_Numbers__c = '';
            string srNo = '';
            List<Implanted_Product__c> lstimpltprdct = MapofImplantIdandImplntdProduct.get(imp.id);
            for (Implanted_Product__c impdt : lstimpltprdct){
                if (impdt.SerialNumber__c != null){
                    if (srNO == '' || srNO == null){
                        srNo = impdt.SerialNumber__c; 
                        imp.Serial_Numbers__c = srNO;
                    } else{
                        imp.Serial_Numbers__c = imp.Serial_Numbers__c + ';' + impdt.SerialNumber__c;
                    }              
                }
            }   
        }

        if (lstImplant.size() > 0){
            update lstImplant;        
        }
        
        if (lstCalltopic.size() > 0){
            delete lstCalltopic;
        }
        
        if (lstcltpc.size() > 0){
            Update lstcltpc;
        }
        
        if (lstCallProduct_Insert.size() > 0){
            insert lstCallProduct_Insert;
        }
        
        if(trigger.isinsert){
            GTutil.chkInsertImplantedProduct = false;
        }
        
        if(trigger.isUpdate){
            GTutil.chkUpdateImplantedProduct = false;
        }
    }

}