trigger tr_CFN_Mapping_Asset on CFN_Mapping_Asset__c (before insert, before update) {
    
    if(Trigger.isBefore){
    	
    	if(Trigger.isInsert){
    	
    		bl_CFN_Mapping_Asset.populateUniqueKey(trigger.new);
    		
    	}else if(Trigger.isUpdate){
    	
    		bl_CFN_Mapping_Asset.populateUniqueKey(trigger.new);
    	}
    	
    }
}