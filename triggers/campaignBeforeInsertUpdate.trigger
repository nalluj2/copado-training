trigger campaignBeforeInsertUpdate on Campaign (before insert, before update) {
    triggerCampaign.beforeInsertUpdate(Trigger.new);  
}