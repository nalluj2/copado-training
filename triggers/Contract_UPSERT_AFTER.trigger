trigger Contract_UPSERT_AFTER on Contract (after update, after insert, before insert,before update)
{
	//If we check the flag in the Custom Settings we skip the execution of this logic
	if(bl_Trigger_Deactivation.isTriggerDeactivated('Contract_UPSERT_AFTER')) return;
	
    List<Contract> ContractList = new List<Contract>();
    Integer cnt = 0;
    Integer cnt1 = 0;
    
    
   
    //Written by Archana as part of SFDC Changes - Re-engineering Interim Project Phase 1
    // If the record was inserted, or was updated and there has been a status change and the record's new status is Activated
    /*
   if(trigger.isInsert && trigger.isbefore)
    {
        for(Contract sc : Trigger.new)
        {
             //if the new end date is null
             if (sc.EndDate == null) 
             {
       			   Date tempDate = sc.StartDate; //create a new variable tempDate ans assign it to the record's Start Date
                   tempDate = tempDate.addMonths(sc.Term); //add the months from contract term to the tempDate
                   sc.EndDate = tempDate.addDays(-1);//decrease the day for the End Date by 1 and store the new value
             }
             if(sc.EndDate != null && sc.EndDate < sc.startDate.addYears(1))//if the new end date is not equal to null and end date less than start date
                    sc.Anniversary__c = sc.EndDate;   //assign end date as anniversary
                else
                    sc.Anniversary__c = sc.startDate.addYears(1).addDays(-1);//Assign start date as Anniversary Date
                    
             
             
             ContractList.add(sc); //Adding Serivice contracts to service contract List
        }
    
            
    }
    else 
    */
    if(trigger.isUpdate && trigger.isBefore)
    {
        
        for(Contract sc : Trigger.new)
        {  
        	if(sc.entitlement_anniversary_check__c == true)
        	{
        		//sc.entitlement_anniversary_check__c=false;
        	system.debug('sc.entitlement_anniversary_check__c before update 123 ---->'+sc.entitlement_anniversary_check__c);
        		//system.debug('sc.Initial_Hotel_Nights__c before update 123 ---->'+sc.Initial_Hotel_Nights__c);
        		 /* End Date is a calculated field in Contract object
        		 if (sc.EndDate == null) 
	             {
	       			   Date tempDate = sc.StartDate; //create a new variable tempDate ans assign it to the record's Start Date
	                   tempDate = tempDate.addMonths(sc.ContractTerm); //add the months from contract term to the tempDate
	                   sc.EndDate = tempDate.addDays(-1);//decrease the day for the End Date by 1 and store the new value
	             }
	             */
	             if(sc.EndDate != null && sc.EndDate < sc.startDate.addYears(1))//if the new end date is not equal to null and end date less than start date
	                    sc.Anniversary__c = sc.EndDate;   //assign end date as anniversary
	                else
	                    sc.Anniversary__c = sc.startDate.addYears(1).addDays(-1);//Assign start date as Anniversary Date
	             
	             ContractList.add(sc); //Adding Serivice contracts to service contract List
        	}
        	else
        	{
        		system.debug('sc.entitlement_anniversary_check__c before update abc ---->'+sc.entitlement_anniversary_check__c);
            // if (Trigger.old[cnt].EndDate != Trigger.new[cnt].EndDate) { //if the old end date is not the same as the new end date
                /* End Date is a calculated field in Contract object
                if (sc.EndDate == null) { //if the new end date is null
                    System.debug('In Update - Start Date - ' + sc.StartDate);
                    System.debug('In Update - Term - ' + sc.ContractTerm);
                    Date tempDate = sc.StartDate; //create a new variable tempDate ans assign it to the record's Start Date
                    tempDate = tempDate.addMonths(sc.ContractTerm); //add the months from contract term to the tempDate
                    sc.EndDate = tempDate.addDays(-1); //decrease the day for the End Date by 1 and store the new value
                }
                */
           // }
           
           System.debug('sc.StartDate is : '+ sc.StartDate);
           System.debug('sc.StartDate is : '+ Trigger.old[cnt].StartDate);
           
           //if the old start date is not the same as the new Start date
           if(sc.StartDate != Trigger.old[cnt].StartDate)
           {
              /* if(sc.EndDate != null && sc.EndDate < sc.startDate.addYears(1))
                    sc.Anniversary__c = sc.EndDate;
                else
                    sc.Anniversary__c = sc.startDate.addYears(1).addDays(-1);
                ContractList.add(sc);*/
                if(sc.Anniversary__c == null)//if Anniversary equal to null
                    {
                        if(sc.enddate != null && sc.enddate < Date.Today())//if the new end date is not equal to null and end date less than today date
                            sc.Anniversary__c = sc.enddate;//Assign End date as anniversary date
                        else if(sc.enddate != null && sc.enddate > Date.Today())//if the new end date is not equal to null and end date greater than today date
                        {
                            sc.Anniversary__c = sc.startDate.addYears(1).addDays(-1);//decrease the day for the add years by 1 and store the new value
                                while(sc.Anniversary__c < Date.Today())//Checking Anniversary value with today date value
                                        sc.Anniversary__c = sc.Anniversary__c.addYears(1);
                        }
                    }
                if(sc.startDate != null && Trigger.old[cnt].StartDate != null && sc.Anniversary__c != null)//if start date is not equal to null and old strat date equal to not null and anniversary value not equal to null
                    sc.Anniversary__c = sc.Anniversary__c.addDays(Trigger.old[cnt].StartDate.daysBetween(sc.startDate));//Add date between new start date and old start date value to Anniversary
            cnt++; //incremenr counter variable
           System.debug('cnt ----->'+cnt);
           }
           
           //if end date is not equal to null and old end date equal to not null and end date value greater than to today date value
           if(sc.endDate != null && sc.endDate != Trigger.old[cnt1].endDate && sc.endDate > date.today())
            {
                if(sc.Anniversary__c == null || sc.Anniversary__c < Date.Today())//if Anniversary equal to null or anniversary less than to today daye
                        sc.Anniversary__c = sc.startDate.addYears(1).addDays(-1);//decrease the day for the add years by 1 and store the new value
                                    
                    while(sc.Anniversary__c < Date.Today())//Checking Anniversary value with today date value
                        sc.Anniversary__c = sc.Anniversary__c.addYears(1);
                         cnt1++; //incremenr counter variable
           System.debug('cnt1 ----->'+cnt1);
                     
            }
                 if(sc.Anniversary__c > sc.enddate)//if anniversary greater than enddate
                    sc.Anniversary__c = sc.endDate;  //Assign end date to Anniversary Date
            
          
        }
         
        }
        
        
        /* Added by wipro for refilling the sservice contract after the scheduler has run
        */
        System.debug('cnt1234  ----->'+cnt);
       if(cnt > 0 && ContractList.size() > 0 && !System.isBatch())
            Contract_Logic.LoadContractData(ContractList);//call the service contract class.
        
    }
    
  /* commented by wipro 
   if(ContractList.size() > 0 && !System.isBatch())
            Contract_Logic.LoadContractData(ContractList);//call the service contract class.
   */
     
     
    
      
    
}