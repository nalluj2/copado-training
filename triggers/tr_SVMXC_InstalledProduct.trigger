//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 05-01-2016
//  Description      : APEX Trigger on SVMXC__Installed_Product__c 
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
trigger tr_SVMXC_InstalledProduct on SVMXC__Installed_Product__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

	// Pass the Trigger newMap and oldMap to the APEX Class (TriggerHandler)
	bl_SVMXC_InstalledProduct_Trigger.lstTriggerNew = Trigger.new;
	bl_SVMXC_InstalledProduct_Trigger.mapTriggerNew = Trigger.newMap;
	bl_SVMXC_InstalledProduct_Trigger.mapTriggerOld = Trigger.oldMap;


	if (Trigger.isBefore){

		if (Trigger.isInsert){

			// ALL BEFORE INSERT ACTIONS
			bl_SVMXC_InstalledProduct_Trigger.copyAccountCountry('INSERT');
			
			bl_SVMXC_InstalledProduct_Trigger.processKeyValueField();

		}else if (Trigger.isUpdate){

			// ALL BEFORE UPDATE ACTIONS
			bl_SVMXC_InstalledProduct_Trigger.copyAccountCountry('UPDATE');
			
			bl_SVMXC_InstalledProduct_Trigger.processKeyValueField();
		
		}else if (Trigger.isDelete){

			// ALL BEFORE DELETE ACTIONS

		}

	}else if (Trigger.isAfter){

		if (Trigger.isInsert){

			// ALL AFTER INSERT ACTIONS

		}else if (Trigger.isUpdate){

			// ALL AFTER UPDATE ACTIONS
			bl_SVMXC_InstalledProduct_Trigger.notificationSAP('UPDATE');

		}else if (Trigger.isDelete){

			// ALL AFTER DELETE ACTIONS

		}else if (Trigger.isUnDelete){

			// ALL AFTER UNDELETE ACTIONS

		}

	}

}
//--------------------------------------------------------------------------------------------------------------------