trigger tr_ImplantSchedulingTeamAdmin_Before on Implant_Scheduling_Team_Admin__c (before insert, before update, after insert, after delete, after undelete) {
	
	if(Trigger.isBefore){
		
		if(Trigger.isInsert){
					
			for(Implant_Scheduling_Team_Admin__c teamAdmin : Trigger.New){
			
				teamAdmin.Unique_key__c = teamAdmin.Team__c + ':' + teamAdmin.Admin__c;		
			}
			
		}else if(Trigger.isUpdate){
			
			for(Implant_Scheduling_Team_Admin__c teamAdmin : Trigger.New){
			
				teamAdmin.addError('Team Admins cannot be modified. If you wish to change the Team Admin delete this record and create a new one with a different User');		
			}				
		}		
		
	}else{
		
		if(Trigger.isInsert || Trigger.isUndelete){
			
			List<Implant_Scheduling_Team__Share> shares = new List<Implant_Scheduling_Team__Share>();
			Set<Id> userIds = new Set<Id>();
			
	    	for(Implant_Scheduling_Team_Admin__c teamAdmin : trigger.New){
				
				if(teamAdmin.Owner__c == false){
				
					Implant_Scheduling_Team__Share share = new Implant_Scheduling_Team__Share();
					share.ParentId  = teamAdmin.Team__c;
					share.UserOrGroupId = teamAdmin.Admin__c;
					share.AccessLevel = 'Edit';
					share.RowCause = Schema.Implant_Scheduling_Team__Share.RowCause.Team_Admin__c;
				
					shares.add(share);					
				}
				
				userIds.add(teamAdmin.Admin__c);
	    	}
	    	
	    	insert shares;
	    		    	
			bl_Implant_Scheduling_Team.addMobileApp(userIds);
	    	
	    	if(bl_Implant_Scheduling_Team.skipDuringTest == false) bl_Implant_Scheduling_Team.addTeamAdmin(Trigger.newMap.keySet());
			
		}else if(Trigger.isDelete){
						
			Set<Id> teamIds = new Set<Id>();
			Set<Id> adminIds = new Set<Id>();
			
			Set<String> toDeleteKeys = new Set<String>();
			
			for(Implant_Scheduling_Team_Admin__c teamAdmin : Trigger.old){
							
				teamIds.add(teamAdmin.Team__c);
				adminIds.add(teamAdmin.Admin__c);
					
				toDeleteKeys.add(teamAdmin.Team__c + ':' + teamAdmin.Admin__c);
			}
						
			List<Implant_Scheduling_Team__Share> toDelete = new List<Implant_Scheduling_Team__Share>();
    	
	    	for(Implant_Scheduling_Team__Share share : [Select Id, ParentId, UserOrGroupId from Implant_Scheduling_Team__Share where ParentId IN :teamIds AND UserOrGroupId IN :adminIds AND RowCause = 'Team_Admin__c']){
				
				String key = share.ParentId + ':' + share.UserOrGroupId;
				
				if(toDeleteKeys.contains(key)) toDelete.add(share);
	    	}
	    	
	    	delete toDelete;	
	    	
	    	bl_Implant_Scheduling_Team.removeMobileApp(adminIds);	
	    	
	    	if(bl_Implant_Scheduling_Team.skipDuringTest == false) bl_Implant_Scheduling_Team.deleteTeamAdmins(Trigger.oldMap.keySet());		
		}	
	}
	
}