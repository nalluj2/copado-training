trigger tr_Release_Deployment_Component_before on Release_Deployment_Component__c (before insert, before update) {

	Map<String, RDC_Settings__c> settings = RDC_Settings__c.getAll();
	
	Pattern APINamePattern = Pattern.compile('^[a-zA-Z0-9_$]*$');
	
	for(Release_Deployment_Component__c rdc : Trigger.new){
		
		//Set Eclipse as default Deployment Type when no value has been specified
		if(rdc.Deployment_Type_to_Staging__c == null) rdc.Deployment_Type_to_Staging__c = 'Eclipse';
		
		//Validate RDC	
		if(rdc.Type__c == 'Data Manipulation'){
			
			if(rdc.Deployment_Type_to_Staging__c != 'Manual'){
				
				rdc.addError('Data Manipulations must be set as Manual deployment type');
				continue;
			}
			
			if(String.isBlank(rdc.Object__c) == true){
				
				rdc.addError('Data Manipulations must specify the Object API Name.');
				continue;
			}			
		}
		
		if(rdc.API_Field_name_Change__c == true){
			
			if(rdc.Deployment_Type_to_Staging__c != 'Manual'){
				
				rdc.addError('When the API Name has changed it must be set as Manual deployment type');
				continue;
			}
			
			if(String.isBlank(rdc.Old_API_Name__c) == true || String.isBlank(rdc.Old_Label__c )  == true){
				
				rdc.addError('When the API Name has changed you must provide the Old Label and Old API Name values');
				continue;
			}
			
			for(String APINamePart : rdc.Old_API_Name__c.split('/')){
								
				if(APINamePattern.matcher(APINamePart).matches() == false){
					rdc.addError('Incorrect Old API Name format');
					continue;						
				}	
			}
		}
		
		RDC_Settings__c typeSettings = settings.get(rdc.Meta_data_type__c);
			
		if(rdc.Deployment_Type_to_Staging__c == 'Manual'){
				
			if(typeSettings != null && typeSettings.Allow_Manual__c == false){
			
				rdc.addError(rdc.Meta_data_type__c + ' cannot be defined as Manual deployment type');
				continue;
			}			
				
			if(rdc.Responsible_for_Deployment__c == null){
			
				rdc.addError('A Responsible for Deployment is required for Manual deployment types');
				continue;
			}

		}else{
			
			//Clear the value for Responsible for Deployment if its not a manual item
			rdc.Responsible_for_Deployment__c = null;
						
			if(typeSettings == null){			
			
				rdc.addError(rdc.Meta_data_type__c + ' must be set as Manual deployment type');
				continue;
				
			}else{
				
				if(rdc.Action_Type__c == 'Deleted' && String.isBlank(rdc.Deletion_Order__c) == true){
					
					rdc.addError('When deleting through Eclipse deployment, please specify if it should be deleted before or after the deployment');
					continue;
				}
								
				if(typeSettings.Object_API_Required__c == true){
					
					if(String.isBlank(rdc.Object__c) == true){
			
						rdc.addError(rdc.Meta_data_type__c + ' requires the Object API Name to be specified');
						continue;
					}
					
					if(APINamePattern.matcher(rdc.Object__c).matches() == false){
			
						rdc.addError('Incorrect Object API Name format');
						continue;						
					}					
				}
				
				if(typeSettings.Field_API_Required__c == true){
					
					if(String.isBlank(rdc.API_Field_name__c) == true || String.isBlank(rdc.Item_Name__c) == true){
			
						rdc.addError(rdc.Meta_data_type__c + ' requires the Item API Name and Label to be specified');
						continue;
					}
					
					if(typeSettings.Uses_Folder__c == true && rdc.API_Field_name__c.contains('/') == false){	
			
						rdc.addError(rdc.Meta_data_type__c + ' requires the API Name to be specified in the following format: folder_name/item_API_name');
						continue;
					}
					
					if(typeSettings.Validate_Item_API_Name__c == true){
						
						for(String itemAPINamePart : rdc.API_Field_name__c.split('/')){
							
							if(APINamePattern.matcher(itemAPINamePart).matches() == false){
			
								rdc.addError('Incorrect Item API Name format');
								continue;						
							}	
						}
					}					
				}
			}
		}
		
		if(String.isBlank(rdc.Meta_data_type__c) == false && rdc.Meta_data_type__c == 'CustomField'){
			
			if(rdc.API_Field_name__c != null && rdc.API_Field_name__c.endsWith('__c') == false && rdc.Deployment_Type_to_Staging__c != 'Manual'){
				
				rdc.addError('Standard Fields must be set as Manual deployment type');
				continue;
			}
		}
	}
}