trigger CallRecordBeforeUpSert on Call_Records__c (before insert,after insert, before update) {

	if(Trigger.IsBefore && (Trigger.isInsert || Trigger.isUpdate) && GTutil.boolContactCallTopic) {
		GTutil.boolContactCallTopic=false; 
		//boolean boolUpdateFlag = false;
		set<id> setUsersIds = new set<id>();
    	for(Call_Records__c callRecord : Trigger.new) {
	        
	        setUsersIds.add(callRecord.OwnerId);
	        setUsersIds.add(callRecord.CreatedById);
        	
        	if(callRecord.Comments__c != null && callRecord.Comments__c.length()>255){
        		callRecord.Comments_short__c = callRecord.Comments__c.substring(0,252)+'...';
        	} else{
        		callRecord.Comments_short__c = callRecord.Comments__c;
        	}
        	
	        /*if(Trigger.isUpdate && (trigger.new[i].OwnerId!=trigger.old[i].OwnerId || trigger.new[i].Country_Owner__c==null)){
	            boolUpdateFlag = true;
	        }*/
		}
		//-BC - CR-3752 -20140321 - Exluded the filtering on Active Users - START
		//	We need to do this because if a user is De-Activatedand the Call Record is updated, this would result in empty fields on the Call Record (Owner__c, Call_Owner_JobTitle__c, Call_Created_By_JobTitle__c)
	    list<User> lstUsers = [Select id,CountryOR__c,Job_Title_vs__c,FirstName,LastName from User where id in: setUsersIds];
//	    list<User> lstUsers = [Select id,CountryOR__c,Job_Title_vs__c,FirstName,LastName from User where isactive=true and id in: setUsersIds];
		//-BC - CR-3752 -20140321 - Exluded the filtering on Active Users - STOP
	    //Start:Added by Manish
	    Map<id,string> MapCreatedByIdJobTitle=new Map<id,string>();
	    map<id,string> mapOwneridWithName=new map<id,string>();
	    map<id,string> mapOwneridWithJobTitle=new map<id,string>();/*Bart Debruyne added: 12/11/2012 - R5.3 035 CR-734*/
	    for(user u:lstUsers)
	    {
	    	mapOwneridWithName.put(u.id,u.LastName+', '+u.FirstName);
	    	mapOwneridWithJobTitle.put(u.id,u.Job_Title_vs__c);/*Bart Debruyne added: 12/11/2012 - R5.3 035 CR-734*/
			MapCreatedByIdJobTitle.put(u.id,u.Job_Title_vs__c);
	    	
	    }
	    for(integer i=0;i<trigger.new.size();i++)
	    {
	    	trigger.new[i].Owner__c=mapOwneridWithName.get(trigger.new[i].ownerid);
	    	trigger.new[i].Call_Owner_JobTitle__c=mapOwneridWithJobTitle.get(trigger.new[i].ownerid);/*Bart Debruyne added: 12/11/2012 - R5.3 035 CR-734*/
	    	trigger.new[i].Call_Created_By_JobTitle__c=MapCreatedByIdJobTitle.get(trigger.new[i].CreatedById);
	    }
		//End:Added by Manish        
			
	        Map<id,string> MapOwnerCountry = new Map<id,string>(); 
	        for(User u:lstUsers){
	            if(u.CountryOR__c!=null)
	            MapOwnerCountry.put(u.Id,u.CountryOR__c); 
	        }
	        string strOwnerCountry='';
	        for(Integer i=0; i<trigger.new.size(); i++) {
	            strOwnerCountry='';
	            if(MapOwnerCountry.get(trigger.new[i].OwnerId)!=null){
	                strOwnerCountry = MapOwnerCountry.get(trigger.new[i].OwnerId);
	                trigger.new[i].Country_Owner__c = strOwnerCountry;                  
	            }
	        }
	}
    
}