trigger tr_Contract_Repository_Document_Version on Contract_Repository_Document_Version__c (after update) {
    
    if(Trigger.isAfter){
    	
    	if(Trigger.isUpdate){
    		
    		bl_Contract_Repository_Document_Version.sendDeletionNotification(Trigger.new, Trigger.oldMap);
    	}    	
    }
}