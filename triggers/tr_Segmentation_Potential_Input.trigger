trigger tr_Segmentation_Potential_Input on Segmentation_Potential_Input__c (before insert, before update, before delete, after insert, after update) {
	
	if(Trigger.isBefore){
    	
    	if(Trigger.isInsert){
    		
    		bl_Customer_Segmentation.isDMLAllowed(Trigger.new);
    		
    		bl_Segmentation_Potential_Input_Trigger.setUniqueKey(Trigger.new, null);
    		
    		bl_Segmentation_Potential_Input_Trigger.matchSegmentation(Trigger.new, null);
    	
    	}else if(Trigger.isUpdate){
    		
    		bl_Customer_Segmentation.isDMLAllowed(Trigger.new);
    		
    		bl_Segmentation_Potential_Input_Trigger.setUniqueKey(Trigger.new, Trigger.oldMap);
    		
    		bl_Segmentation_Potential_Input_Trigger.matchSegmentation(Trigger.new, Trigger.oldMap);
    		
    	}else if(Trigger.isDelete){
    		
    		bl_Customer_Segmentation.isDMLAllowed(Trigger.old);
    	}
    	
    }else{
    	
    	if(Trigger.isInsert){
    		
    		bl_Segmentation_Potential_Input_Trigger.manageSharing(Trigger.new, null);
    		
    	}else if(Trigger.isUpdate){
    		
    		bl_Segmentation_Potential_Input_Trigger.manageSharing(Trigger.new, Trigger.oldMap);
    	}
    }  
}