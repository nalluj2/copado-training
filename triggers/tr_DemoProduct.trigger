//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 26-04-2016
//  Description      : APEX Trigger on Demo_Product__c 
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
trigger tr_DemoProduct on Demo_Product__c (before update) {

	// Pass the Trigger newMap and oldMap to the APEX Class (TriggerHandler)
	bl_DemoProduct_Trigger.lstTriggerNew = Trigger.new;
	bl_DemoProduct_Trigger.mapTriggerNew = Trigger.newMap;
	bl_DemoProduct_Trigger.mapTriggerOld = Trigger.oldMap;


	if (Trigger.isBefore) {

		if (Trigger.isUpdate){

			bl_DemoProduct_Trigger.setLocationOfDemoProduct();

		}
    
	}

}
//--------------------------------------------------------------------------------------------------------------------