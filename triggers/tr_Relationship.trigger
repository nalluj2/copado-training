trigger tr_Relationship on Affiliation__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	
	if(Trigger.isBefore){    
		
		if(Trigger.isInsert){
			
			bl_Relationship.setUpdateActiveFlag(Trigger.new);
			
			bl_Relationship.validateContactAffiliations(Trigger.new);
									
			bl_Relationship.swithFromTo(Trigger.new);
			
			bl_Relationship.validateA2ARelationships(Trigger.new);
            
            bl_Relationship.validateA2AUniqueSBU(Trigger.new);
				
		}else if(Trigger.isUpdate){
			
			bl_Relationship.setUpdateActiveFlag(Trigger.new);
			
			bl_Relationship.validateContactAffiliations(Trigger.new);
									
			bl_Relationship.swithFromTo(Trigger.new);
			
			bl_Relationship.validateA2ARelationships(Trigger.new);
            
            bl_Relationship.validateA2AUniqueSBU(Trigger.new);
			
		}else if(Trigger.isDelete){
			
			bl_Relationship.validateDeletionPrimaryRelationship(Trigger.old);
		}			
		
	}else{
		
		if(Trigger.isInsert){
			
			bl_Relationship.setContactIntegrateWithMMXFlag(Trigger.new);
            bl_Relationship.updateA2A_BU_Info(Trigger.new);
			
		}else if(Trigger.isUpdate){
			
			bl_Relationship.setContactIntegrateWithMMXFlag(Trigger.new);
            bl_Relationship.updateA2A_BU_Info(Trigger.new);
			
		}else if(Trigger.isDelete){
			
			bl_Relationship.upsertRelDeletedInfo(Trigger.old);
            bl_Relationship.updateA2A_BU_Info(Trigger.old);
			
		}else if(Trigger.isUndelete){
			
			bl_Relationship.updateRelDeletedInfo(Trigger.new);
            bl_Relationship.updateA2A_BU_Info(Trigger.new);
		}
	}
}