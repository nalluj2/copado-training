/**
 * Created by dijkea2 on 27-1-2017.
 */

trigger tr_StakeholderMapping on Stakeholder_Mapping__c (before insert, before update, after insert) {

    if (bl_Trigger_Deactivation.isTriggerDeactivated('tr_StakeholderMapping')) return;

    if (Trigger.isBefore){

        if (Trigger.isInsert){

            bl_StakeholderMapping_Trigger.populateMobileId(Trigger.new);

        }else if (Trigger.isUpdate){

            bl_StakeholderMapping_Trigger.populateMobileId(Trigger.new);

        }

    } else if (Trigger.isAfter){

        if (Trigger.isInsert){

			bl_StakeholderMapping_Trigger.IHS_CreateTask(trigger.new);

        }

    }

}