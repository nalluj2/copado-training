trigger tr_Account_Plan_2_Dup_Validation on Account_Plan_2__c (after insert, after undelete, after update) {
	
	if(bl_Trigger_Deactivation.isTriggerDeactivated('tr_Account_Plan_2_Dup_Validation')) return;
	
	if(bl_AccountPlanning.escapeDupValidation == true) return;
	
	Set<Id> BUGs = new Set<Id>();
	Set<Id> BUs = new Set<Id>();
	Set<Id> SBUs = new Set<Id>();
	
	Set<Id> BUGAccounts = new Set<Id>();
	Set<Id> BUAccounts = new Set<Id>();
	Set<Id> SBUAccounts = new Set<Id>();
	
	for(Account_Plan_2__c ap : Trigger.new){
		
		if(ap.Account_Plan_Level__c == 'Business Unit Group'){
			
			BUGS.add(ap.Business_Unit_Group__c);
			BUGAccounts.add(ap.Account__c);
			
		}else if(ap.Account_Plan_Level__c == 'Business Unit'){
			
			BUs.add(ap.Business_Unit__c);
			BUAccounts.add(ap.Account__c);
			
		}else if(ap.Account_Plan_Level__c == 'Sub Business Unit'){
			
			SBUs.add(ap.Sub_Business_Unit__c);
			SBUAccounts.add(ap.Account__c);			
		}	
	}

	if (BUGS.size() == 0 && BUs.size() == 0 && SBUs.size() == 0) return;	//- CR-27800 - SAM Account Plan has xBU in Account_Plan_Level__c
	
	Map<Id, Business_Unit_Group__c> BUGById = new Map<Id, Business_Unit_Group__c>([Select Id, Name, Master_Data__r.Name from Business_Unit_Group__c where Id IN :BUGs]);
	Map<Id, Business_Unit__c> BUById = new Map<Id, Business_Unit__c>([Select Id, Name, Company__r.Name from Business_Unit__c where Id IN :BUs]);
	Map<Id, Sub_Business_Units__c> SBUById = new Map<Id, Sub_Business_Units__c>([Select Id, Business_Unit__r.Name, Business_Unit__r.Company__r.Name from Sub_Business_Units__c where Id IN :SBUs]);
			
	Map<String, List<Account_Plan_2__c>> plansByKey = new Map<String, List<Account_Plan_2__c>>();
	
	if(BUGAccounts.size()>0){
		for(Account_Plan_2__c plan : [Select Id, Account__c, Business_Unit_Group__c, Start_Date__c, End_Date__c, Account_Plan_Level__c from Account_Plan_2__c
										where Account__c IN :BUGAccounts AND Account_Plan_Level__c = 'Business Unit Group' AND Business_Unit_Group__c IN :BUGs]){
		
			String key = getKey(plan, BUGById, BUById, SBUById); 
			
			List<Account_Plan_2__c> plans = plansByKey.get(key);
			
			if(plans == null){
				
				plans = new List<Account_Plan_2__c>();
				plansByKey.put(key, plans);
			}
			
			plans.add(plan);
		}
	}
	
	if(BUAccounts.size()>0){
		for(Account_Plan_2__c plan : [Select Id, Account__c, Business_Unit__c, Start_Date__c, End_Date__c, Account_Plan_Level__c from Account_Plan_2__c
										where Account__c IN :BUAccounts AND Account_Plan_Level__c = 'Business Unit' AND Business_Unit__c IN :BUs]){
		
			String key = getKey(plan, BUGById, BUById, SBUById); 
			
			List<Account_Plan_2__c> plans = plansByKey.get(key);
			
			if(plans == null){
				
				plans = new List<Account_Plan_2__c>();
				plansByKey.put(key, plans);
			}
			
			plans.add(plan);
		}
	}
	
	if(SBUAccounts.size()>0){
		for(Account_Plan_2__c plan : [Select Id, Account__c, Sub_Business_Unit__c, Start_Date__c, End_Date__c, Account_Plan_Level__c from Account_Plan_2__c
										where Account__c IN :SBUAccounts AND Account_Plan_Level__c = 'Sub Business Unit' AND Sub_Business_Unit__c IN :SBUs]){
		
			String key = getKey(plan, BUGById, BUById, SBUById); 
			
			List<Account_Plan_2__c> plans = plansByKey.get(key);
			
			if(plans == null){
				
				plans = new List<Account_Plan_2__c>();
				plansByKey.put(key, plans);
			}
			
			plans.add(plan);
		}
	}
	
	for(Account_Plan_2__c plan : Trigger.new){
		
		String key = getKey(plan, BUGById, BUById, SBUById); 
				
		List<Account_Plan_2__c> keyPlans = plansByKey.get(key);
				
		//We will always find at lest the Account Plan being inserted/updated/undeleted
		if(keyPlans.size()>1){
			plan.addError('There is already an active account plan for the selected options');
		}			
	}
	
	private String getKey(Account_Plan_2__c plan, Map<Id, Business_Unit_Group__c> bugMap, Map<Id, Business_Unit__c> buMap, Map<Id, Sub_Business_Units__c> sbuMap){
				
		String key = plan.Account__c + ':' + plan.Account_Plan_Level__c + ':';
		
		if(plan.Account_Plan_Level__c == 'Business Unit Group'){
			
			key += plan.Business_Unit_Group__c;
			
		}else if(plan.Account_Plan_Level__c == 'Business Unit'){
			
			key += plan.Business_Unit__c;
						
		}else if(plan.Account_Plan_Level__c == 'Sub Business Unit'){
			
			key += plan.Sub_Business_Unit__c;						
		}
				
		return key;				
	}
}