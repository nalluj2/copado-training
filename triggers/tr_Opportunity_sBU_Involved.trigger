trigger tr_Opportunity_sBU_Involved on Opportunity_sBU_Involved__c (before insert, before update, after delete, after insert, after undelete, after update){
    
    if(Trigger.isBefore){
    	
    	if(Trigger.isInsert){
    		
    		bl_Opportunity_sBU_Involved.setUniqueKey(Trigger.new);
    		
    	}else if(Trigger.isUpdate){
    		
    		bl_Opportunity_sBU_Involved.setUniqueKey(Trigger.new);
    	}
    	
    }else{
    	
    	if(Trigger.isInsert){
    		
    		bl_Opportunity_sBU_Involved.validateTotalsAndCalculateSummary(Trigger.new);
    		
    	}else if(Trigger.isUpdate){
    		
    		bl_Opportunity_sBU_Involved.validateTotalsAndCalculateSummary(Trigger.new);
    		
    	}else if(Trigger.isDelete){
    		
    		bl_Opportunity_sBU_Involved.validateTotalsAndCalculateSummary(Trigger.old);
    		
    	}else if(Trigger.isUndelete){
    		
    		bl_Opportunity_sBU_Involved.validateTotalsAndCalculateSummary(Trigger.new);    		
    	}
    }
    
}