/*
  *  Trigger Name   : PartShipment_UPDATE_AFTER
  *  Description   : This trigger is used to execute the after update event of Part_Shipment objects
  *  Created Date  : 8/20/2013
  *  Author        : Wipro Tech. 
*/
trigger PartShipment_Update_After on Parts_Shipment__c (after insert, after update) {
	
	//If we check the flag in the Custom Settings we skip the execution of this logic
	if(bl_Trigger_Deactivation.isTriggerDeactivated('PartShipment_Update_After')) return;
	
	if(bl_SynchronizationService_Utils.isSyncProcess == false){
		
		if(Trigger.isInsert) bl_SynchronizationService_Source.createNotificationIfChanged(Trigger.New, null);
		else bl_SynchronizationService_Source.createNotificationIfChanged(Trigger.New, Trigger.OldMap);	
	}

if(trigger.isinsert){

// added by Pavan Hegde-- to get the count of Part Shipment attached to a case.
     CaseShipmentandPartsCountHandler.updatecaseRollup('Parts_Shipment__c');  
}

if(trigger.isupdate){
// added by Pavan Hegde-- to get the count of Part Shipment attached to a case.
     CaseShipmentandPartsCountHandler.updatecaseRollup('Parts_Shipment__c'); 
    Integer compCount = 0;
    //Boolean updateflag;
    Map<Id, Complaint__c> complaintMap = new Map<Id, Complaint__c>();
    Map<Id, String> mapOfNameForComp = new Map<Id, String>();
    set<Id> complaintIdSet = new Set<Id>();
    for(Parts_Shipment__c partShipObj :Trigger.old){
        
        if(partShipObj.Complaint__c != Trigger.new[compCount].Complaint__c && Trigger.new[compCount].Complaint__c == null) {
            complaintIdSet.add(partShipObj.Complaint__c);
            mapOfNameForComp.put(partShipObj.Complaint__c, partShipObj.Name);
        }
    }
    if(complaintIdSet.size() > 0) {
        for(Complaint__c compObj:[select id, Parent_Complaint_No__c, Name from Complaint__c where Id IN:complaintIdSet]) {
            complaintMap.put(compObj.Id, compObj);
        }
    }    
    if(complaintMap.size() > 0) 
        Case_logic.complaintRemove(complaintMap, mapOfNameForComp, 'Part Shipment', 'Delink', null);
                     
   }
}