//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 28-07-2016
//  Description      : APEX Trigger on Time_Reporting 
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
trigger tr_TimeReporting on Time_Reporting__c (before insert, after insert, after update, after delete, after undelete) {

	if (Trigger.isBefore){

		if (Trigger.isInsert){

			bl_TimeReporting_Trigger.copyTimeReportFieldsRelatedToKronos(Trigger.new);

		}

	}else if (Trigger.isAfter){

		if (Trigger.isInsert){

			//bl_TimeReporting_Trigger.updateActualHoursOnProject(Trigger.new, Trigger.oldMap, 'INSERT');

		}else if (Trigger.isUpdate){

			//bl_TimeReporting_Trigger.updateActualHoursOnProject(Trigger.new, Trigger.oldMap, 'UPDATE');

		}else if (Trigger.isDelete){

			//bl_TimeReporting_Trigger.updateActualHoursOnProject(Trigger.new, Trigger.oldMap, 'DELETE');

		}else if (Trigger.isUndelete){

			//bl_TimeReporting_Trigger.updateActualHoursOnProject(Trigger.new, Trigger.oldMap, 'UNDELETE');

		}

	}

}
//--------------------------------------------------------------------------------------------------------------------