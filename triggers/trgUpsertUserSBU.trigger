trigger trgUpsertUserSBU on User (after insert,after update) {
map<id,string> mapUseridBUName=new map<id,string>();
map<id,string> mapUseridCompCode=new map<id,string>();
list<string> lstBUName=new list<string>();
list<Sub_Business_Units__c> lstSBU=new List<Sub_Business_Units__c>();
list<User_Business_Unit__c> lstUserSBU=new List<User_Business_Unit__c>();
map<id,List<Sub_Business_Units__c>> mapUseridWithSBUs=new map<id,List<Sub_Business_Units__c>>();
List<string> useridwithSBUid=new List<string>();
if(Trigger.isInsert)
{            
            if(trigger.isAfter)
            {
                for(integer i=0;i<trigger.new.size();i++)
                {
                    if(trigger.new[i].User_Business_Unit_vs__c!=null && trigger.new[i].User_Business_Unit_vs__c!='All' && trigger.new[i].User_Business_Unit_vs__c!='--None--')
                    {
                    lstBUName.add(trigger.new[i].User_Business_Unit_vs__c);
                    mapUseridBUName.put(Trigger.new[i].id,trigger.new[i].User_Business_Unit_vs__c);
                    mapUseridCompCode.put(Trigger.new[i].id,trigger.new[i].Company_Code_Text__c);
                    }
                }
             }   
            
            if(lstBUName.size()>0)
            {
            lstSBU=[select id,name,Autoflag_DiB_Account_fields__c,Business_Unit__r.name,Business_Unit__r.Company__r.Company_Code_Text__c from Sub_Business_Units__c where Business_Unit__r.name in:lstBUName];
            }
            
           
            
            
            
            
            
            if(lstSBU.size()>0 && trigger.isAfter)
            {
            
                for(integer i=0;i<trigger.new.size();i++)
                {
                   // trigger.new[i].Autoflag_DiB_Account_fields__c=false;
                    for(Sub_Business_Units__c sbu: lstSBU)
                    {
                        if(mapUseridBUName.get(Trigger.new[i].id)==sbu.Business_Unit__r.name && mapUseridCompCode.get(Trigger.new[i].id)==sbu.Business_Unit__r.Company__r.Company_Code_Text__c)
                        {
                            if(trigger.new[i].Profile_Name__c!='Chatter Free User')
                            {
                            User_Business_Unit__c ubu=new User_Business_Unit__c(user__c=Trigger.new[i].id,  Sub_Business_Unit__c=sbu.id);
                            lstUserSBU.add(ubu);
                            useridwithSBUid.add(string.valueof(Trigger.new[i].id)+'@'+string.valueof(sbu.id));
                                
                            } 
                                               
                        }    
                    }    
                }
                if(useridwithSBUid.size()>0)
                {
                
                    //GTutil.createUserSBU(useridwithSBUid);
                    //insert lstUserSBU;
                    
                }
            }       
        
        
}
List<string> useridwithSBUidUpd=new List<string>();    
if(Trigger.isUpdate)
{

    set<id> setUserid=new set<id>();
    map<id,string> mapuseridWithBUCompany=new map<id,string>();
    for(integer i=0;i<trigger.new.size();i++)
    {
        system.debug(trigger.old[i].User_Business_Unit_vs__c+'usertrigger'+trigger.new[i].User_Business_Unit_vs__c);
        if((trigger.old[i].User_Business_Unit_vs__c!=trigger.new[i].User_Business_Unit_vs__c)|| (trigger.old[i].Company_Code_Text__c!=trigger.new[i].Company_Code_Text__c) )
        {
            if(trigger.new[i].Profile_Name__c!='Chatter Free User')
            {
            system.debug('usertrigger1'+trigger.new[i].id);
            setUserid.add(trigger.new[i].id);
            mapuseridWithBUCompany.put(trigger.new[i].id,trigger.new[i].User_Business_Unit_vs__c+';'+trigger.new[i].Company_Code_Text__c);
            }
         }    
        
    }  
    if(setUserid.size()>0){     
        
        // Start: When changing BU User Delete New BU's User SBU Records.  
        list<User_Business_Unit__c> lstUserSBU1=new list<User_Business_Unit__c>();      
        //list<User_Business_Unit__c> lstDelUserSBU=new list<User_Business_Unit__c>();
        set<id> setDelUserSBU=new set<id>();
        lstUserSBU1=[select id,user__c,Business_Unit_text__c,Company_Code__c from User_Business_Unit__c where user__c in:setUserid];
        if(lstUserSBU1.size()>0)
        {
            for(User_Business_Unit__c ubu:lstUserSBU1)
            {
                if(mapuseridWithBUCompany.get(ubu.User__c).contains(ubu.Business_Unit_text__c) && mapuseridWithBUCompany.get(ubu.User__c).contains(ubu.Company_Code__c))
                {
                //lstDelUserSBU.add(ubu);
                setDelUserSBU.add(ubu.id);
                
                }
            }          
        }
        if(setDelUserSBU.size()>0)
        {
            //GTutil.deleteUserSBU(setDelUserSBU);
        //delete lstDelUserSBU;
        }
      
        //End
        
        for(integer i=0;i<trigger.new.size();i++)
        {
            if((trigger.old[i].User_Business_Unit_vs__c!=trigger.new[i].User_Business_Unit_vs__c) || (trigger.old[i].Company_Code_Text__c!=trigger.new[i].Company_Code_Text__c))
            {
                if(trigger.new[i].User_Business_Unit_vs__c!=null && trigger.new[i].User_Business_Unit_vs__c!='All' && trigger.new[i].User_Business_Unit_vs__c!='--None--')
                {
                lstBUName.add(trigger.new[i].User_Business_Unit_vs__c);
                mapUseridBUName.put(Trigger.new[i].id,trigger.new[i].User_Business_Unit_vs__c);
                mapUseridCompCode.put(Trigger.new[i].id,trigger.new[i].Company_Code_Text__c);
                }
            }
        }
        if(lstBUName.size()>0)
        {
        lstSBU=[select id,name,Autoflag_DiB_Account_fields__c,Business_Unit__r.name,Business_Unit__r.Company__r.Company_Code_Text__c from Sub_Business_Units__c where Business_Unit__r.name in:lstBUName];
        }
        if(lstSBU.size()>0)
        {            
            for(integer i=0;i<trigger.new.size();i++)
            {            
                for(Sub_Business_Units__c sbu: lstSBU)
                {
                    if(mapUseridBUName.get(Trigger.new[i].id)==sbu.Business_Unit__r.name && mapUseridCompCode.get(Trigger.new[i].id)==sbu.Business_Unit__r.Company__r.Company_Code_Text__c)
                    {
                    
                         if(trigger.new[i].Profile_Name__c!='Chatter Free User')
                            {
                            User_Business_Unit__c ubu=new User_Business_Unit__c(user__c=Trigger.new[i].id,  Sub_Business_Unit__c=sbu.id);
                            lstUserSBU.add(ubu);
                            useridwithSBUidUpd.add(string.valueof(Trigger.new[i].id)+'@'+string.valueof(sbu.id));
                            //system.debug(ubu.Sub_Business_Unit__c+'@@@@@@'+ubu.User__c);   
                            } 
                        //User_Business_Unit__c ubu=new User_Business_Unit__c(user__c=Trigger.new[i].id,  Sub_Business_Unit__c=sbu.id);
                        //lstUserSBU.add(ubu);
                    }        
                }    
            }
            if(useridwithSBUidUpd.size()>0)
            {
                
                    //GTutil.createUserSBU(useridwithSBUidUpd);
                    //insert lstUserSBU;
                    
             }
           /* if(lstUserSBU.size()>0 )
            {
                insert lstUserSBU;
            }*/
        }   
    }
}   


}