/*
Author : Rudy De Coninck
Version info: 1.0
Information: This is the trigger when an opportunityLineItem is updated or deleted
*/


trigger trgOppLineItemAfterUpsert on OpportunityLineItem (after insert, after update,after delete) {

	if (bl_Trigger_Deactivation.isTriggerDeactivated('oli_trgOppLineItemAfterUpsert')) return;

	/* Update Therapies and Sbu field on opportunity with concatenated values from opp line item
	 * If one line is missing a product code a error sentence is shown for the complete opportunity
	 */
	Set<Id> setID_RecordType_Exclude = new Set<Id>();
	setID_RecordType_Exclude.add(clsUtil.getRecordTypeByDevName('Opportunity', 'Capital_Opportunity').Id);
	setID_RecordType_Exclude.add(clsUtil.getRecordTypeByDevName('Opportunity', 'Non_Capital_Opportunity').Id);
	setID_RecordType_Exclude.add(clsUtil.getRecordTypeByDevName('Opportunity', 'Service_Contract_Opportunity').Id);

	Set<ID> opportunityIds = new Set<ID>();

	if (Trigger.isInsert || Trigger.isUpdate){
		for (OpportunityLineItem opportunityLineItem : Trigger.new){
			opportunityIds.add(opportunityLineItem.OpportunityId);
		}
	}
	if (Trigger.isDelete){
		for (OpportunityLineItem opportunityLineItem : Trigger.old){
			opportunityIds.add(opportunityLineItem.OpportunityId);
		}
	}

	// Exclude the Opportunities that have an Exclude RecordType
	List<Opportunity> lstOpportunity = [SELECT Id FROM Opportunity WHERE Id = :opportunityIds AND RecordTypeId != :setID_RecordType_Exclude];
	Set<Id> setID_Opportunity_Processing = new Set<Id>();
	for (Opportunity oOpportunity : lstOpportunity){
		setID_Opportunity_Processing.add(oOpportunity.Id);
	}
	opportunityIds = new Set<ID>();
	if (Trigger.isInsert || Trigger.isUpdate){
		for (OpportunityLineItem oOLI : Trigger.new){
			if (setID_Opportunity_Processing.contains(oOLI.OpportunityId)) opportunityIds.add(oOLI.OpportunityId);
		}
	}
	if (Trigger.isDelete){
		for (OpportunityLineItem oOLI : Trigger.old){
			if (setID_Opportunity_Processing.contains(oOLI.OpportunityId)) opportunityIds.add(oOLI.OpportunityId);
		}
	}


	List<OpportunityLineItem> opportunityLineItems = 
		[
			SELECT 
				Id, OpportunityId, Product_Group__c, Product_SBU__c, Products_Therapies__c, PriceBookEntry.Product2.Sub_Business_Unit__c 
			FROM 
				OpportunityLineItem
			WHERE 
				OpportunityId in :opportunityIds
		];

	Map<ID,List<OpportunityLineItem>> mapOpportunityLineItemsById = new Map<Id,List<OpportunityLineItem>>();

	Set<Id> checkProductConfiguration = new Set<Id>();
	Set<Id> productConfigurationOk = new Set<Id>();
	for (OpportunityLineItem lineItem : opportunityLineItems){
		List<OpportunityLineItem> lineItemsForOpp;
		if (mapOpportunityLineItemsById.containsKey(lineItem.opportunityId)){
			lineItemsForOpp = mapOpportunityLineItemsById.get(lineItem.opportunityId);
		}else{
			lineItemsForOpp = new List<OpportunityLineItem>();
			mapOpportunityLineItemsById.put(lineItem.opportunityId,lineItemsForOpp);
		}
		lineItemsForOpp.add(lineItem);
		
		if (lineItem.Product_Group__c==null || lineItem.Product_Group__c=='0'|| lineItem.Product_Group__c=='' || 
			lineItem.PriceBookEntry.Product2.Sub_Business_Unit__c==null || lineItem.PriceBookEntry.Product2.Sub_Business_Unit__c==''){
			checkProductConfiguration.add(lineItem.opportunityid);
		}else{
			productConfigurationOk.add(lineItem.opportunityid);
		}
	}

	//Remove all opp id from ok if already in not ok
	for (Id id : checkProductConfiguration){
		if (productConfigurationOk.contains(id)){
			productConfigurationOk.remove(id);
		}
	}
	
	List<Opportunity> listOpportunitiesToUpdate = new List<Opportunity>();

	if (productConfigurationOk.size() > 0){	//-BC - 20160705 - Optimisation APEX Code
		
		Map<Id,Opportunity> opportunitiesConfigurationOk = new Map<ID,Opportunity>([
			select id,type,Accountid,Sub_Business_Units__c, Therapies__c 
			from opportunity 
			where id in:productConfigurationOk
		]);
		
		System.debug('All opp configuration ok'+mapOpportunityLineItemsById);
		/*
		*	Set all opportunities where product configuration is ok to therapies
		*/
		for (ID inputId : productConfigurationOk){
			String therapies='';
			String sbus='';
			Opportunity opp = opportunitiesConfigurationOk.get(inputId);
			List<OpportunityLineItem> lineItemsOpp = mapOpportunityLineItemsById.get(inputId);
			for (OpportunityLineItem item : lineItemsOpp){
				therapies+=item.Products_Therapies__c+';';
				sbus+=item.Product_SBU__c+';';
			}
			therapies=therapies.substring(0,therapies.length()-1);
			sbus=sbus.substring(0,sbus.length()-1);
			//Filter out doubles from therapy names
			String[] therapyList = therapies.split(';');
			Set<String> therapySet = new Set<String>();
			for(Integer i=0 ; i<therapyList.size() ; i++) {
					therapySet.add((String)therapyList[i]);
			}
			System.debug('TherapySet'+therapySet);
			List<String> therapySortedList = new List<String>(therapySet);
			therapySortedList.sort();
			String therapyString='';
			for(String th : therapySortedList){
				therapyString+=th+';';
			}
			therapyString=therapyString.substring(0,therapyString.length()-1);
			//Filter out doubles from sbu names
			String[] sbuList = sbus.split(';');
			Set<String> sbuSet = new Set<String>();
			for (Integer i=0 ; i<sbuList.size() ; i++){
				sbuSet.add((String)sbuList[i]);
			}
			String sbuString='';
			List<String> sbuSortedList = new List<String>(sbuSet);
			sbuSortedList.sort();
			for(String th : sbuSortedList){
				sbuString+=th+';';
			}
			sbuString=sbuString.substring(0,sbuString.length()-1);
			
			opp.Sub_Business_Units__c=sbuString;
	        opp.Therapies__c=therapyString;
	        listOpportunitiesToUpdate.add(opp);
		}
	}
	
	
	/*
	*	Remove 
	*/
	for (ID id : productConfigurationOk){
		opportunityIds.remove(id);
	}	
	
	/*
	*	Remove 
	*/
	for (ID id : checkProductConfiguration){
		opportunityIds.remove(id);
	}
	
	/*
	*	Set all opportunities where product configuration needs to be checked to error phrase
	*/
	if (checkProductConfiguration.size() > 0){	//-BC - 20160705 - Optimisation APEX Code
		Map<Id,Opportunity> opportunitiesConfigurationNotOk = new Map<ID,Opportunity>([
			select id,type,Accountid,Sub_Business_Units__c 
			from opportunity 
			where id in:checkProductConfiguration
		]);
		
		for (ID inputId : checkProductConfiguration){
			String nok='Cannot be determined, please contact business support to check the product configuration';
			Opportunity opp = opportunitiesConfigurationNotOk.get(inputId);
			opp.Sub_Business_Units__c=nok;
	        opp.Therapies__c=nok;
	        listOpportunitiesToUpdate.add(opp);
		}
	}
	
	/*
	*	Set all opportunities where no lineItems exist to empty
	*/
	if (opportunityIds.size() > 0){	//-BC - 20160705 - Optimisation APEX Code
		Map<Id,Opportunity> opportunitiesNoLines = new Map<ID,Opportunity>([
			select id,type,Accountid,Sub_Business_Units__c 
			from opportunity 
			where id in:opportunityIds
		]);
		
		for (ID inputId : opportunityIds){
			Opportunity opp = opportunitiesNoLines.get(inputId);
			opp.Sub_Business_Units__c='';
	        opp.Therapies__c='';
	        listOpportunitiesToUpdate.add(opp);
		}
	}
	
	if (listOpportunitiesToUpdate.size() > 0){
		update listOpportunitiesToUpdate;
	}

}