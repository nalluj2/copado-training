trigger tr_Account_Procedure on Account_Procedure__c (before insert, before update) {
 	
 	if (Trigger.isBefore){
 		
 		if (Trigger.isInsert){
 			
 			bl_AccountProcedure_Trigger.preventDuplicate(Trigger.new);

 			GuidUtil.populateMobileId(Trigger.new, 'Mobile_ID__c');
 			
 		}else if(Trigger.isUpdate){
 			
 			bl_AccountProcedure_Trigger.preventDuplicate(Trigger.new);

 			GuidUtil.populateMobileId(Trigger.new, 'Mobile_ID__c');

 		}

 	}   

}