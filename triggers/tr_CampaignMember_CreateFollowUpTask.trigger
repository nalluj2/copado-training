//--------------------------------------------------------------------------------------------------------------------
//  Author      : Bart Caelen
//  Version     : 1.0
//  Date        : 20150819
//  Description : CR-6811
//              This APEX Trigger will create a follow up task when a new Campaign Member is created related to 
//                  to a CEE Campaign (Training & Education) for which this logic has been enabled (Automatically_create_follow_up_tasks__c = true).
//              The Task will be linked to the Campaign and the Campaign Member and assigend to 
//                  the creator of the Campaign Member.
//              The Task will be get an Activity Date of Campaign EndDate + 21 days or if the Campaign Member 
//                  is created after the EndDate of the Campaign the Activity Date will be the current date (today).
//              On the Campaign the user can enable/disable this functionalit
//                  is created in Salesforce.com by the MMX Interface User.
//--------------------------------------------------------------------------------------------------------------------
//  Author      : Bart Caelen
//  Version     : 2.0
//  Date        : 20151009
//  Description : CR-8680
//              Extended this logic for MEA
//--------------------------------------------------------------------------------------------------------------------
//  Author      : Christophe Saenen
//  Version     : 2.0
//  Date        : 20160115
//  Description : CR-9924
//--------------------------------------------------------------------------------------------------------------------
//  Author      : Christophe Saenen
//  Version     : 3.0
//  Date        : 201600302
//  Description : CR-10656 creation of task from trigger tr_CampaignMember_CreateFollowUpTask to batch ba_Campaign_AddCampaignMemberStatus
//--------------------------------------------------------------------------------------------------------------------
trigger tr_CampaignMember_CreateFollowUpTask on CampaignMember (after insert, after update) {

    if (trigger.isAfter){
        if (trigger.isUpdate){
//        	List<CampaignMember> createLst = new List<CampaignMember>();
        	Map<CampaignMember, List<string>> deleteLst = new Map<CampaignMember, List<string>>();
        	
			for (CampaignMember newCM : Trigger.new) {
				CampaignMember oldCM = Trigger.oldMap.get(newCM.Id); 
				
				system.debug('### newCM =' + newCM.Id);
				system.debug('### ' + newCM.Campaign_Member_Type__c + '<>' + oldCM.Campaign_Member_Type__c);
			
				if(newCM.Campaign_Member_Type__c <> oldCM.Campaign_Member_Type__c) {
//					if(newCM.Campaign_Member_Type__c == 'Participant') createLst.add(newCM); // create task
					if(oldCM.Campaign_Member_Type__c == 'Participant') {
						list<string> tmpLst = new list<string>();
						tmpLst.add((newCM.ContactId!=null) ? newCM.ContactId : newCM.LeadId);
						tmpLst.add((newCM.ContactId!=null) ? newCM.CampaignId : '');

						system.debug('### newCM =' + newCM.ContactId + ' ' + newCM.LeadId + ' ' + tmpLst[0]);
						
						deleteLst.put(newCM, tmpLst); // delete task
					}
				}
			}
			
//            if(createLst.size() > 0) bl_CampaignMember.createFollowUpTask(createLst);
			system.debug('### deleteLst.size() = ' + deleteLst.size());
            if(deleteLst.size() > 0) {
            	bl_CampaignMember.deleteFollowUpTask(deleteLst);
            
	            list<CampaignMember> updList = new list<CampaignMember>();
	            for(CampaignMember cm : [select id, FollowUpTask_Created__c from CampaignMember where Id in :deleteLst.keyset()]) {
	            	cm.FollowUpTask_Created__c = false;
	            	updList.add(cm);
	            }
	            
	            update updList;
            }
        }
    }

/*    if (trigger.isAfter){
        if (trigger.isInsert){
        	List<CampaignMember> createLst = new List<CampaignMember>();
			for (CampaignMember newCM : Trigger.new) {
            	if(newCM.Campaign_Member_Type__c == 'Participant')  createLst.add(newCM); // create task;
			}
            if(createLst.size() > 0) bl_CampaignMember.createFollowUpTask(createLst);
        }
    }
*/
}