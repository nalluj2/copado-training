/*
*	Trigger to log user actions on AccountPlan2 (create / update/ delete)
* 	Author: Rudy De Coninck
*	Creation date: 17-12-2014
*/

trigger ua_AccountPlan2 on Account_Plan_2__c (after delete, after insert, after undelete, after update) {

	//We do not track bulk operations
	if (Trigger.size == 1){
		String operation='none';
		List<Account_Plan_2__c> recordsToProcess = new List<Account_Plan_2__c>();
		if (Trigger.isDelete){
			operation = 'Delete';		
			for (Account_Plan_2__c a :Trigger.old){
				recordsToProcess.add(a);				
			}  
		}else{
			if (Trigger.isInsert){
				operation = 'Insert';
			}
			if(Trigger.isUpdate){
				operation='Update';
			}
			for (Account_Plan_2__c a :Trigger.new){
				recordsToProcess.add(a);				
			}  
		}
		if(recordsToProcess.size()>0){
			for(Account_Plan_2__c a : recordsToProcess){
				//Only log initial action
				if (!bl_UserAction.userActionInProgress && !bl_UserAction.childAccountPlanOperation){
					bl_UserAction.logObject(UserInfo.getUserId(), operation,'Account_Plan_2__c',a.id,'');
					bl_UserAction.userActionInProgress = true;  
				} 
			}
		}
	} 
}