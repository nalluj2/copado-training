/**
 * Creation date : 10/01/2011
 * Description : Block the deletion if it's been used.
 * Created by : ABSI / Malaka Silva
 **/
 
trigger DepartmentMasterBeforeDelete on Department_Master__c (before delete) {
	Set<Id> ids = new Set<Id>();
	for(Department_Master__c departmentMaster:trigger.old){
		ids.add(departmentMaster.Id);
	}
	DIB_Department__c [] dibDepartments = [Select d.Department_ID__c From DIB_Department__c d where d.Department_ID__c in :ids order by d.SystemModstamp desc limit 1000];
	if(dibDepartments != null && !dibDepartments.isEmpty()){
		ids = new Set<Id>();
		for(DIB_Department__c dibDepartment:dibDepartments){
			ids.add(dibDepartment.Department_ID__c);
		}
		for(Department_Master__c departmentMaster:trigger.old){
			if(ids.contains(departmentMaster.Id)){
				departmentMaster.addError('Unable to delete. Record already been used.');
			}
		}
	}
}