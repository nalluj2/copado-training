/*
*	Trigger to log user actions on Asset (create / update/ delete)
* 	Author: Rudy De Coninck
*	Creation date: 17-12-2014
*/

trigger ua_Asset on Asset (after delete, after insert, after undelete, after update) {

	//We do not track bulk operations
	if (Trigger.size == 1){
		String operation='none';
		List<Asset> recordsToProcess = new List<Asset>();
		if (Trigger.isDelete){
			operation = 'Delete';		
			for (Asset a :Trigger.old){
				recordsToProcess.add(a);				
			}  
		}else{
			if (Trigger.isInsert){
				operation = 'Insert';
			}
			if(Trigger.isUpdate){
				operation='Update';
			}
			for (Asset a :Trigger.new){
				recordsToProcess.add(a);				
			}  
		}
		if(recordsToProcess.size()>0){
			for(Asset a : recordsToProcess){
				//Only log initial action
				if (!bl_UserAction.userActionInProgress){
					bl_UserAction.logObject(UserInfo.getUserId(), operation,'Asset',a.id,'');
					bl_UserAction.userActionInProgress = true;  
				} 
			}
		}
	} 
}