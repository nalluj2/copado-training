trigger tr_Installed_Product_Measures on Installed_Product_Measures__c (before insert, before update, after update) {
	
	if(Trigger.isBefore){

		if (Trigger.isInsert){

			bl_Installed_Product_Measures.setAccountFromIP(Trigger.new);
		
		}else if(Trigger.isUpdate){
			
			bl_Installed_Product_Measures.setReadBy(Trigger.new, Trigger.oldMap);

			bl_Installed_Product_Measures.setAccountFromIP(Trigger.new);
		}		
		
	}else{
		
		if(Trigger.isUpdate){
			
			bl_Installed_Product_Measures.notificationSAP(Trigger.new, Trigger.oldMap);			

		}

	}

}