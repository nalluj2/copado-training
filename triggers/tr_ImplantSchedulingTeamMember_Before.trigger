trigger tr_ImplantSchedulingTeamMember_Before on Implant_Scheduling_Team_Member__c (before insert, before update, before delete, after insert, after delete, after undelete) {
	
	if(Trigger.isBefore){
		
		if(Trigger.isInsert){
					
			for(Implant_Scheduling_Team_Member__c teamMember : Trigger.New){
			
				teamMember.Unique_key__c = teamMember.Team__c + ':' + teamMember.Member__c;		
			}
			
		}else if(Trigger.isUpdate){
			
			for(Implant_Scheduling_Team_Member__c teamMember : Trigger.New){
			
				teamMember.addError('Team Members cannot be modified. If you wish to change the Team Member delete this record and create a new one with a different User');		
			}				
		
		}else if(Trigger.isDelete){
			
			bl_Implant_Scheduling_Team.blockDeleteAssignedMembers(Trigger.old);
		}
		
		
	}else{
		
		if(Trigger.isInsert || Trigger.isUndelete){
			
			Set<Id> userIds = new Set<Id>();
			for(Implant_Scheduling_Team_Member__c teamMember : Trigger.new) userIds.add(teamMember.Member__c);
			
			bl_Implant_Scheduling_Team.addMobileApp(userIds);
			
			if(bl_Implant_Scheduling_Team.skipDuringTest == false) bl_Implant_Scheduling_Team.addTeamMembers(Trigger.newMap.keySet());		
			
		}else if(Trigger.isDelete){
			
			Set<Id> userIds = new Set<Id>();
			for(Implant_Scheduling_Team_Member__c teamMember : Trigger.old) userIds.add(teamMember.Member__c);
			
			bl_Implant_Scheduling_Team.removeMobileApp(userIds);
			
			if(bl_Implant_Scheduling_Team.skipDuringTest == false) bl_Implant_Scheduling_Team.deleteTeamMembers(Trigger.oldMap.keySet());					
		}	
	}
}