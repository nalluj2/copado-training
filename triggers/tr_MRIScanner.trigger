trigger tr_MRIScanner on MRI_Scanner__c (before insert, before update, after insert, after update) {


	if (Trigger.isBefore){

		if (Trigger.isInsert){

			bl_MRIScanner_Trigger.populateMobileId(Trigger.new);

		}else if (Trigger.isUpdate){

			bl_MRIScanner_Trigger.populateMobileId(Trigger.new);

		}


	}else if (Trigger.isAfter){

		if (Trigger.isInsert){

			bl_MRIScanner_Trigger.SynchronizationService_createNotificationIfChanged(Trigger.new, Trigger.oldMap);

		}else if (Trigger.isUpdate){

			bl_MRIScanner_Trigger.SynchronizationService_createNotificationIfChanged(Trigger.new, Trigger.oldMap);
		
		}

	}

}