trigger TherapyBeforeUpsert on Therapy__c (before insert, before update, before delete) {

    /*if(Trigger.isInsert || Trigger.isUpdate)    
    {        
        for(Integer i=0; i<trigger.new.size(); i++){
            trigger.new[i].Therapy__c = trigger.new[i].Name;    
        }    
    }*/   
    
    if(Trigger.isDelete)
    {
        // Check if current Therapy is there in the Call Record & Call Topic...
        boolean recCount=false;
        List<Call_Topics__c> lstSubject = [Select s.id From Call_Topics__c s where s.Call_Topic_Therapy__c in : Trigger.old LIMIT 1];
        if(lstSubject.size()>0)
        {
            recCount=true;    
        }  
        
      /*  List<Procedures__c> lstProcedure = [Select s.id From Procedures__c s where s.Therapy__c in : Trigger.old LIMIT 1];
        if(lstProcedure.size()>0)
        {
            recCount=true;    
        }  */

        //Condition added on 18th-Jan-11
        List<Implant__c> lstImp = [Select s.id From Implant__c s where s.Therapy__c in : Trigger.old LIMIT 1];
        if(lstImp.size()>0)
        {
            recCount=true;    
        }
                
        List<Affiliation__c> lstAff = [Select s.id From Affiliation__c s where s.Therapy__c in : Trigger.old LIMIT 1];
        if(lstAff.size()>0)
        {
            recCount=true;    
        }  
        
        List<Segmentation__c> lstSeg = [Select s.id From Segmentation__c s where s.Therapy__c in : Trigger.old LIMIT 1];
        if(lstSeg.size()>0)
        {
            recCount=true;    
        } 
        
        List<Therapy_Product__c> lstTP = [Select s.id From Therapy_Product__c s where s.Therapy__c in : Trigger.old LIMIT 1];
        if(lstTP.size()>0)
        {
            recCount=true;    
        } 

        if(recCount==true)
        {
            trigger.old[0].name.addError('This Therapy is present in Call Record or Call Topic or Therapy Product. Please delete the child records and then try again.');    
        }
    }
    
}