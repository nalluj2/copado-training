trigger tr_CampaignMemberAutoEmail on CampaignMember (after insert, after update) {

    if (bl_Trigger_Deactivation.isTriggerDeactivated('tr_CampaignMemberAutoEmail') || Test.isRunningTest()) return;	//-BC - 20170621 - Added

    et4ae5.triggerUtility.automate('CampaignMember');

}