trigger CCcampaignStatusCheck on Campaign (before update) {

    if (bl_Trigger_Deactivation.isTriggerDeactivated('campaign_CCcampaignStatusCheck')) return;

    boolean accstatuscheck=true;
    map<id,integer> mapCamIndexwithid=new map<id,integer>();
    
    for(integer i=0;i<trigger.new.size();i++)
    {
        mapCamIndexwithid.put(trigger.new[i].id,i);
    }
    List<Target_Account__c> lsttrgtacc = new List<Target_Account__c>();
    lsttrgtacc=[select id,name,Status__c,Campaign__c,Campaign__r.status from Target_Account__c where Campaign__c =: trigger.newmap.keyset()];
    
    for(Target_Account__c tac:lsttrgtacc){
        if(Trigger.new[mapCamIndexwithid.get(tac.Campaign__c)].Status=='Accounts Approved' && tac.Status__c != 'Approved'){
           
          Trigger.new[mapCamIndexwithid.get(tac.Campaign__c)].Status.addError('Please make sure that all target accounts are approved');
        }
    }
   
  
    List<Campaignmember> lstcmpnmbr=new List<Campaignmember>();
    //- CR-12919 - BC - 05/10/2016 - START 
//    lstcmpnmbr=[Select id,CC_Status__c,CampaignID,Campaign.status,Invitation_Caller__r.name from Campaignmember where CampaignID =: trigger.newmap.keyset()];
    lstcmpnmbr = 
        [
            SELECT
                id, CC_Status__c, CampaignID, Campaign.status, Invitation_Caller__r.name 
            FROM 
                Campaignmember 
            WHERE 
                CampaignID = :trigger.newmap.keyset() 
                AND Campaign.Status in ('Contacts Approved', 'Invitation Callers Assigned', 'Invitation Calls Completed', 'Feedback Calls Completed')
        ];
    //- CR-12919 - BC - 05/10/2016 - STOP 
    for(campaignmember cmp:lstcmpnmbr){
        if(Trigger.new[mapCamIndexwithid.get(cmp.CampaignID)].Status=='Contacts Approved' && cmp.CC_Status__c != 'Approved'){
            Trigger.new[mapCamIndexwithid.get(cmp.CampaignID)].Status.addError('Please make sure that all contacts are approved');
        }
        if(Trigger.new[mapCamIndexwithid.get(cmp.CampaignID)].Status=='Invitation Callers Assigned' && (cmp.Invitation_Caller__r.name == '' || cmp.Invitation_Caller__r.name == null)){
            Trigger.new[mapCamIndexwithid.get(cmp.CampaignID)].Status.addError('Please make sure to assign an invitation caller to all contacts');
        }
        if((Trigger.new[mapCamIndexwithid.get(cmp.CampaignID)].Status=='Invitation Calls Completed' && !(cmp.CC_Status__c == 'Invitation Call Declined' || cmp.CC_Status__c == 'Invitation Call Accepted' ))){
            Trigger.new[mapCamIndexwithid.get(cmp.CampaignID)].Status.addError('Please make sure to complete all invitation calls');
        }
        if(Trigger.new[mapCamIndexwithid.get(cmp.CampaignID)].Status=='Feedback Calls Completed' && cmp.CC_Status__c == 'Feedback Call Required' ){
            Trigger.new[mapCamIndexwithid.get(cmp.CampaignID)].Status.addError('Please make sure to complete all feedback calls');
        }
    }
  
}