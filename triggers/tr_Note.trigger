//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 11-04-2019
//  Description      : APEX Trigger on Note
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
trigger tr_Note on Note (after insert, after delete, after undelete){ 

	if (Trigger.isAfter){

		if (Trigger.isInsert){
		
			bl_Note_Trigger.createNote_MITG(Trigger.new);

		}else if(Trigger.isDelete){

			bl_Note_Trigger.deleteNote_MITG(Trigger.old);

		}else if(Trigger.isUndelete){

			bl_Note_Trigger.createNote_MITG(Trigger.new);

		}

	}

}
//--------------------------------------------------------------------------------------------------------------------