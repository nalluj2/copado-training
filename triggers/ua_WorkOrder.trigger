/*
*	Trigger to log user actions on Workorder__c (create / update/ delete)
* 	Author: Rudy De Coninck
*	Creation date: 17-12-2014
*/

trigger ua_WorkOrder on Workorder__c (after delete, after insert, after undelete, after update) {

	//We do not track bulk operations
	if (Trigger.size == 1){
		String operation='none';
		List<Workorder__c> recordsToProcess = new List<Workorder__c>();
		if (Trigger.isDelete){
			operation = 'Delete';		
			for (Workorder__c a :Trigger.old){
				recordsToProcess.add(a);				
			}  
		}else{
			if (Trigger.isInsert){
				operation = 'Insert';
			}
			if(Trigger.isUpdate){
				operation='Update';
			}
			for (Workorder__c a :Trigger.new){
				recordsToProcess.add(a);				
			}  
		}
		if(recordsToProcess.size()>0){
			for(Workorder__c a : recordsToProcess){
				//Only log initial action
				if (!bl_UserAction.userActionInProgress){
					bl_UserAction.logObject(UserInfo.getUserId(), operation,'Workorder__c',a.id,'');
					bl_UserAction.userActionInProgress = true;  
				} 
			}
		}
	} 
}