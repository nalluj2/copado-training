trigger tr_Cost_Analysis_Related_Product on Cost_Analysis_Related_Product__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    
    if(Trigger.isBefore){
    	
    	if(Trigger.isInsert){
    		
    		if(bl_SETCase.isDMLAllowed(Trigger.new) == false) return;
    		
    		GuidUtil.populateMobileId(Trigger.new, 'Mobile_ID__c');
    		
    	}else if(Trigger.isUpdate){
    		
    		if(bl_SETCase.isDMLAllowed(Trigger.new) == false) return;
    		
    		GuidUtil.populateMobileId(Trigger.new, 'Mobile_ID__c');
    		
    	}else if(Trigger.isDelete){
    		
    		if(bl_SETCase.isDMLAllowed(Trigger.old) == false) return;
    	}
    	
    }else{
    	
    	if(Trigger.isInsert){
    		
    	}else if(Trigger.isUpdate){
    		
    	}else if(Trigger.isDelete){
    		
    	}else if(Trigger.isUndelete){
    		
    		if(bl_SETCase.isDMLAllowed(Trigger.new) == false) return;
    	}
    }
}