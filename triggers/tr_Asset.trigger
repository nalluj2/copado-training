//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 30-03-2017
//  Description      : Unified trigger for Asset
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
trigger tr_Asset on Asset (before insert, before update, after update) {

	if (Trigger.isBefore){

		if (Trigger.isInsert){

			bl_Asset_Trigger.preventCreation(Trigger.new);

			bl_Asset_Trigger.populateCFNMarketingProduct(Trigger.new, null);

		}else if (Trigger.isUpdate){

			bl_Asset_Trigger.processAsset_ClearServiceLevel(Trigger.new, Trigger.oldMap);

			bl_Asset_Trigger.populateCFNMarketingProduct(Trigger.new, Trigger.oldMap);

		}

	}else if (Trigger.isAfter){

		if (Trigger.isUpdate){

			bl_Asset_Trigger.processAssetContract_ClearServiceLevel(Trigger.new, Trigger.oldMap);

		}

	}

}
//--------------------------------------------------------------------------------------------------------------------