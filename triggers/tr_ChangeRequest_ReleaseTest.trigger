/*
 *      Author 		: Bart Caelen
 *		Date		: 2014-02
 *      Description : This trigger will process created/updated/deleted/undeleted Change_Record__c records in order to automatically 
 *						create/update/delete Release_Test__c and Release_Test_Step__c records based on predefined logic in CR-3490
 *		Version		:  1.0
 * ------------------------------------------------------------------------------------
 *      Author 		: Bart Caelen
 *		Date		: 2014-04-16
 *      Description : Added an additional filtering so that only Change Requests with the General Record Type are processed
 *		Version		:  2.0
*/
trigger tr_ChangeRequest_ReleaseTest on Change_Request__c (after insert, after update, after delete) {

	Id idRecordType_ChangeRequest_General = RecordTypeMedtronic.getRecordTypeByDevName('Change_Request__c', 'General').Id;
		
	if (trigger.isAfter){
		list<Change_Request__c> lstCR_Deleted = new list<Change_Request__c>();
		set<id> setID_ChangeRequest_Deleted = new set<id>();
		set<id> setID_ProcessArea_Deleted = new set<id>();

		list<Change_Request__c> lstCR_Planned_Major = new list<Change_Request__c>();
		set<id> setID_ChangeRequest_Planned_Major = new set<id>();
		set<id> setID_ProcessArea_Planned_Major = new set<id>();

		list<Change_Request__c> lstCR_Planned_Minor = new list<Change_Request__c>();
		set<id> setID_ChangeRequest_Planned_Minor = new set<id>();
		set<id> setID_ProcessArea_Planned_Minor = new set<id>();

		list<Change_Request__c> lstCR_PlannedToNotCompleted = new list<Change_Request__c>();
		set<id> setID_ChangeRequest_PlannedToNotCompleted = new set<id>();
		set<id> setID_ProcessArea_PlannedToNotCompleted = new set<id>();

		if (trigger.isInsert){		
			for (Change_Request__c oCR : trigger.new){
				if (oCR.RecordTypeID == idRecordType_ChangeRequest_General){

					if (oCR.Status__c == 'Planned'){
						if (oCR.Change_Weight__c == 'Major'){
							lstCR_Planned_Major.add(oCR);
							setID_ChangeRequest_Planned_Major.add(oCR.Id);
							setID_ProcessArea_Planned_Major.add(oCR.Process_Area__c);
						}else if (oCR.Change_Weight__c == 'Minor'){
							lstCR_Planned_Minor.add(oCR);
							setID_ChangeRequest_Planned_Minor.add(oCR.Id);
							setID_ProcessArea_Planned_Minor.add(oCR.Process_Area__c);
						}
					}

				}
			}
			if (lstCR_Planned_Major.size() > 0){
				bl_ChangeRequest.processData(lstCR_Planned_Major, setID_ChangeRequest_Planned_Major, setID_ProcessArea_Planned_Major, 'ADD_MAJOR');
			}
			if (lstCR_Planned_Minor.size() > 0){
				bl_ChangeRequest.processData(lstCR_Planned_Minor, setID_ChangeRequest_Planned_Minor, setID_ProcessArea_Planned_Minor, 'ADD_MINOR');
			}

		}else if (trigger.isUpdate){
			for (Change_Request__c oCR : trigger.new){
				if (oCR.RecordTypeID == idRecordType_ChangeRequest_General){

					// Status is Changed
					if (trigger.oldMap.get(oCR.ID).Status__c != oCR.Status__c){
						
						// Change_Request__c STATUS is updated to 'Planned'
						if ( (oCR.Status__c == 'Planned') &&  (trigger.oldMap.get(oCR.Id).Status__c != 'Completed') ){
							if (oCR.Change_Weight__c == 'Major'){
								lstCR_Planned_Major.add(oCR);
								setID_ChangeRequest_Planned_Major.add(oCR.Id);
								setID_ProcessArea_Planned_Major.add(oCR.Process_Area__c);
							}else if (oCR.Change_Weight__c == 'Minor'){
								lstCR_Planned_Minor.add(oCR);
								setID_ChangeRequest_Planned_Minor.add(oCR.Id);
								setID_ProcessArea_Planned_Minor.add(oCR.Process_Area__c);
							}
							
		
						// Change_Request__c STATUS is changed from 'Planned' to something else then 'Completed'
						}else if ( (trigger.oldMap.get(oCR.ID).Status__c == 'Planned') && (oCR.Status__c != 'Completed') ){
							lstCR_PlannedToNotCompleted.add(oCR);
							setID_ChangeRequest_PlannedToNotCompleted.add(oCR.Id);
							setID_ProcessArea_PlannedToNotCompleted.add(oCR.Process_Area__c);
						}
					}

				}
			}
			
			if (lstCR_Planned_Major.size() > 0){
				bl_ChangeRequest.processData(lstCR_Planned_Major, setID_ChangeRequest_Planned_Major, setID_ProcessArea_Planned_Major, 'ADD_MAJOR');
			}
			if (lstCR_Planned_Minor.size() > 0){
				bl_ChangeRequest.processData(lstCR_Planned_Minor, setID_ChangeRequest_Planned_Minor, setID_ProcessArea_Planned_Minor, 'ADD_MINOR');
			}
			if (lstCR_PlannedToNotCompleted.size() > 0){
				bl_ChangeRequest.processData(lstCR_PlannedToNotCompleted, setID_ChangeRequest_PlannedToNotCompleted, setID_ProcessArea_PlannedToNotCompleted, 'PLANNED_TO_NOT_COMPLETED');
			}
			
		}else if (trigger.isDelete){
			
			set<id> setID_ProcessArea = new set<id>();
			set<id> setID_ChangeRequest = new set<id>();

			for (Change_Request__c oCR : trigger.old){
				if (oCR.RecordTypeID == idRecordType_ChangeRequest_General){
					setID_ProcessArea_Deleted.add(oCR.Process_Area__c);
					setID_ChangeRequest_Deleted.add(oCR.Id);
					lstCR_Deleted.add(oCR);
				}
			}

			if (lstCR_Deleted.size() > 0){
				// Execute logic for Delete
				bl_ChangeRequest.processData(lstCR_Deleted, setID_ChangeRequest_Deleted, setID_ProcessArea_Deleted, 'DELETE');
			}
		}

	}
}