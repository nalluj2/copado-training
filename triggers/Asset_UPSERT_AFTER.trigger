trigger Asset_UPSERT_AFTER on Asset (after update, after insert){
	
	//If we check the flag in the Custom Settings we skip the execution of this logic
	if(bl_Trigger_Deactivation.isTriggerDeactivated('Asset_UPSERT_AFTER')) return;
	
	if(Trigger.isInsert) bl_SynchronizationService_Source.createNotificationIfChanged(Trigger.New, null);	
	else bl_SynchronizationService_Source.createNotificationIfChanged(Trigger.New, Trigger.OldMap);
	
}