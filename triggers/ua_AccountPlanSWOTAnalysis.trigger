trigger ua_AccountPlanSWOTAnalysis on Account_Plan_SWOT_Analysis__c (after delete, after insert, after update) {
	
	//We do not track bulk operations
	if (Trigger.size == 1){
		String operation='none';
		List<Account_Plan_SWOT_Analysis__c> recordsToProcess = new List<Account_Plan_SWOT_Analysis__c>();
		if (Trigger.isDelete){
			operation = 'Delete';		
			for (Account_Plan_SWOT_Analysis__c a :Trigger.old){
				recordsToProcess.add(a);				
			}  
		}else{
			if (Trigger.isInsert){
				operation = 'Insert';
			}
			if(Trigger.isUpdate){
				operation='Update';
			}
			for (Account_Plan_SWOT_Analysis__c a :Trigger.new){
				recordsToProcess.add(a);				
			}  
		}
		if(recordsToProcess.size()>0){
			for(Account_Plan_SWOT_Analysis__c a : recordsToProcess){
				//Only log initial action
				if (!bl_UserAction.userActionInProgress){
					bl_UserAction.logObjectWithParent(UserInfo.getUserId(), operation,'Account_Plan_SWOT_Analysis__c',a.id,'',a.Account_Plan__c,'Account_Plan_2__c');
					bl_UserAction.userActionInProgress = true;  
				} 
			}
		}
	}     
}