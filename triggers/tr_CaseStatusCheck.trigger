trigger tr_CaseStatusCheck on Case (after update) {
    
    //Only works with Cases of record type = OMA_Spine_Scientific_Exchange_Product
    if(Trigger.isAfter && bl_tr_CaseTriggerActions.checkAfterUpdate==false){
        bl_CaseStatusLogic.UpdateCaseStatus(Trigger.OldMap,Trigger.New);
    }
    
    /*
    Id implantScheduling_RT = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Implant_Scheduling').getRecordTypeId();
    
    List<Case> implantSupportCases = new List<Case>();
    for (Case c : Trigger.new){
    	if (implantScheduling_RT == c.recordTypeId){
    	
    		implantSupportCases.add(c);
    	}
    }
    
    if(implantSupportCases.size() > 0){
    
    	List<Event> eventsLinkedToCase = [select Id, WhatId, Type from Event where WhatId in :implantSupportCases];
    
	    //See if already an event created for it, if so send update notification
	    //TODO check with John Update event if linked or send notification of change
	    bl_Activity.changeNotification(eventsLinkedToCase, 'Update');
    }
    */
}