trigger tr_MITG_App_Procedure on MITG_App_Procedure__c (before insert, before update) {
    
    for(MITG_App_Procedure__c procedure : Trigger.new){
    	
    	procedure.Unique_Key__c = procedure.Name + ':' + procedure.App__c;
    }
}