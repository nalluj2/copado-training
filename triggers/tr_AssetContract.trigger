//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//  Author       :   Bart Caelen
//  Date         :   20140623
//  Description  :   This trigger will process AssetContract__c records and update Contract records
//    - after insert, after update, after delete, after undelete (CR-4545)
//      - RULES
//          - AFTER INSERT, AFTER UNDELETE
//              - if the field PO_Amount2__c is not empty and not 0
//          - AFTER UPDATE
//              - if the field is Contract__c is changed, process the old and new Contract__c records
//              OR
//              - if the field PO_Amount2__c is changed
//          - AFTER DELETE
//              - if the field PO_Amount2__c is not empty and not 0
//      - ACTIONS
//          - Verify that the field PO_Amount_derived_from_linked_assets__c = TRUE for the collected Contract__c records
//          - update the field PO_Amount__c with the sum of the field PO_Amount2__c for the related AssetContract__c records of the collected Contract records
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//  Author       :   Bart Caelen
//  Date         :   20150403
//  Description  :   This trigger will process AssetContract__c records and update Contract records
//    - after insert, after update, after delete, after undelete (CR-7670)
//      - RULES
//          - AFTER INSERT, AFTER UNDELETE
//              - if the field Service_Level__c is not empty
//          - AFTER UPDATE
//              - if the field is Contract__c is changed, process the old and new Contract__c records
//              OR
//              - if the field Service_Level__c is changed
//          - AFTER DELETE
//              - if the field Service_Level__c is not empty
//      - ACTIONS
//          - Verify that the field PO_Amount_derived_from_linked_assets__c = TRUE for the collected Contract__c records
//          - update the Contract field Contract_Level__c with the concatenated (separated by ;) values of Service_Level__c value of the related AssetContract__c values
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
trigger tr_AssetContract on AssetContract__c (before insert, before update, after insert, after update, after delete, after undelete) {

    if(trigger.isBefore){
        
    	if(trigger.isInsert){
    		
    		bl_Asset_Contract.updateDIENCode(trigger.new, null);
    		
    	}else if(trigger.isUpdate){

		    bl_Asset_Contract.updateDIENCode(trigger.new, trigger.oldMap);

		}


    }else{
    	
    	if(trigger.isInsert){
    		
    		bl_Asset_Contract.logUserAction(trigger.new, 'Insert');
    		
    		bl_Asset_Contract.calculatePOAmountAndServiceLevel(trigger.new, null);
    		
    		bl_Asset_Contract.calculateAssetContractInfo(trigger.new, trigger.isDelete);
    		
    		bl_Asset_Contract.setContractAniversaryFlag(trigger.new);
    		
    	}else if(trigger.isUpdate){
    		
    		bl_Asset_Contract.logUserAction(trigger.new, 'Update');
    		
    		bl_Asset_Contract.calculatePOAmountAndServiceLevel(trigger.new, trigger.OldMap);
    		
    		bl_Asset_Contract.calculateAssetContractInfo(trigger.new, trigger.isDelete);
    		
    	}else if(trigger.isDelete){
    		
    		bl_Asset_Contract.logUserAction(trigger.old, 'Delete');
    		
    		bl_Asset_Contract.calculatePOAmountAndServiceLevel(trigger.old, null);
    		
    		bl_Asset_Contract.calculateAssetContractInfo(trigger.old, trigger.isDelete);
    		
    	}else if(trigger.isUndelete){
    		
    		bl_Asset_Contract.calculatePOAmountAndServiceLevel(trigger.new, null);
    		
    		bl_Asset_Contract.calculateAssetContractInfo(trigger.new, trigger.isDelete);
    	}
    	
    }
}
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------