/*
Asset->Implant->Distributor Account-> Sap ID ->Pricebook Name->     

*/
trigger ImplantProAfterInsert on Implanted_Product__c (after insert) 
{
    
      map<id, id> mpImplantDistributorID = new map<id, id>();
      map<id, String> mpAccIDSapID = new map<id, string>();
      map<id, decimal> mpProductPrice=new map<id, decimal>();
      map<id, string> mpProductISOCode=new map<id, string>();
      map<id, id> mpAssetProductID = new map<id, id>();
      list<id> lstImplantIds = new list<id>();
      list<id> lstNewAssets = new list<id>();
      list<id> lstDistributor = new list<id>(); 
      list<id> lstProduct = new list<id>(); 
      list<string> strSapIds = new list<string>(); 
      list<Implanted_Product__c > lstAsset = new list<Implanted_Product__c >();
      for(Implanted_Product__c  asset:trigger.new)
      {
        lstImplantIds.add(asset.Implant_ID__c);
        lstNewAssets.add(asset.id);
        lstProduct.add(asset.Product2__c);
        mpAssetProductID.put(asset.id, asset.Product2__c);

      }
    //get all the implants based on ASSET
       list<Implant__c> lstImplant = [select id, distributor__c from Implant__c where id in :lstImplantIds];
       for(Implant__c i : lstImplant)
       {
           if(i.distributor__c!=null)
           {
             mpImplantDistributorID.put(i.id, i.distributor__c);
             lstDistributor.add(i.distributor__c);
           }
       }
    //Get all the accounts based on Implant's Distributor Account 
       list<account> lstAccount = [select id,SAP_ID__c from account where id in:lstDistributor];
       string stringSapIds='';
       for(account a: lstAccount)
       {
            mpAccIDSapID.put(a.id, a.SAP_ID__c);
            strSapIds.add(a.SAP_ID__c);
            if(a.SAP_ID__c!='')
            {
                if(stringSapIds=='')
                {
                    stringSapIds = a.SAP_ID__c;

                }
                else
                {
                    stringSapIds =stringSapIds+a.SAP_ID__c+',';
                }
            }
       }
       if(stringSapIds!='' &&  stringSapIds!=null)
       {
       //if(stringSapIds.length()>0)
      // {
          stringSapIds = stringSapIds.substring(0,stringSapIds.length()-1);
      // }
       }

       system.debug('strSapIds' + strSapIds);
       system.debug('lstProduct'+ lstProduct);
       list<Pricebook2> pbdata = [select id,name from Pricebook2 where name like :('%'+stringSapIds +'%')];
       list<id> lstpbIds = new list<id>();
       for(Pricebook2 s:pbdata)
       {
           lstpbIds.add(s.id);
       }

      list<PricebookEntry> pbe=[select id, Product2Id,UnitPrice,CurrencyIsoCode from PricebookEntry where Product2Id in :lstProduct and Pricebook2Id in:lstpbIds];
      
      for(PricebookEntry pb:pbe)
      {
        mpProductPrice.put(pb.Product2Id, pb.UnitPrice);
        mpProductISOCode.put(pb.Product2Id, pb.CurrencyIsoCode);
      }
      system.debug('mpProductPrice'+ mpProductPrice);
    
      //Finally update Asset with the Unit price of the associated PriceBook    
      list<Implanted_Product__c> lstAsstAll= [select id, Price__c,Price_Override__c from Implanted_Product__c where id in:lstNewAssets];       
      for(Implanted_Product__c asst:lstAsstAll)
      {
        //update only when user doesn't enter Price value
        if(asst.Price__c==null || asst.Price__c==0 || asst.Price__c==0.0)
        {
         asst.Price__c = mpProductPrice.get(mpAssetProductID.get(asst.id));
         asst.CurrencyIsoCode = mpProductISOCode.get(mpAssetProductID.get(asst.id));
         lstAsset.add(asst);
        }

      }
      update lstAsset;
       
}