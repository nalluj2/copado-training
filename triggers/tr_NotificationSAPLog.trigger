trigger tr_NotificationSAPLog on NotificationSAPLog__c (after delete, after insert, after undelete, after update, before insert) {
	
	if(Trigger.isBefore){
		
		if(Trigger.isInsert){
			
			bl_NotificationSAPLog.populateNotificationGrouping(Trigger.new);
		}	
		
	}else{
		
		if(Trigger.isInsert){
			
			bl_NotificationSAPLog.updateNotificationGrouping(Trigger.new);
			
		}else if(Trigger.isUpdate){
			
			bl_NotificationSAPLog.updateNotificationGrouping(Trigger.new);
			
		}else if(Trigger.isDelete){
			
			bl_NotificationSAPLog.updateNotificationGrouping(Trigger.old);
			
		}else if(Trigger.isUndelete){
			
			bl_NotificationSAPLog.updateNotificationGrouping(Trigger.new);
		}
	}     
}