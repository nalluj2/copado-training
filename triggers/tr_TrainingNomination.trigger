//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 28-06-2016
//  Description      : APEX Trigger
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
trigger tr_TrainingNomination on Training_Nomination__c (before insert, before update, after update) {

	if (Trigger.isBefore){

		if (Trigger.isInsert){

			bl_TrainingNomination_Trigger.preventDuplicateNomination(Trigger.new);

			bl_TrainingNomination_Trigger.updateTrainingNomination_PACEApprover(Trigger.new);

		}else if (Trigger.isUpdate){

			bl_TrainingNomination_Trigger.updateTrainingNomination_Url(Trigger.new, Trigger.oldMap);

			bl_TrainingNomination_Trigger.validateTrainingNominationApproval(Trigger.new, Trigger.oldMap);

		}

	}else if (Trigger.isAfter){

		if (Trigger.isUpdate){

			bl_TrainingNomination_Trigger.createFollowUpTask(Trigger.new, Trigger.oldMap);
			
		}
	}

}
//--------------------------------------------------------------------------------------------------------------------