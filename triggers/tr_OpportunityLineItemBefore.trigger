/*
 * Description		: fill mobile id when opp line for CVG project (mobile ADPR)  		 
 * Author        	: Rudy De Coninck
 * Created Date    	: 16-10-2013
 */
trigger tr_OpportunityLineItemBefore on OpportunityLineItem (before insert, before update) {

	List<ID> oppIds = new List<ID>();
	for (OpportunityLineItem oppLineItem : Trigger.new){
		oppIds.add(oppLineItem.opportunityid);
	}
	
	List<Opportunity> listOpp = [select Id, RecordTypeId from Opportunity where Id in :oppIds];

	Set<ID> cvgProjectOpportunities = new Set<ID>();
	Set<ID> rtgOpportunities = new Set<ID>();
	for (Opportunity opp : listOpp){
		if (opp.recordtypeId == RecordTypeMedtronic.opportunity('CVG_Project').id){
			cvgProjectOpportunities.add(opp.id);
		}
		
		if(opp.RecordTypeId == RecordTypeMedtronic.Opportunity('Capital_Opportunity').Id
			|| opp.RecordTypeId == RecordTypeMedtronic.Opportunity('Service_Contract_Opportunity').Id
			|| opp.RecordTypeId == RecordTypeMedtronic.Opportunity('Business_Critical_Tender').Id
			|| opp.RecordTypeId == RecordTypeMedtronic.Opportunity('Non_Capital_Opportunity').Id
			
		){
			rtgOpportunities.add(opp.id);
		}
		
	}

	List<OpportunityLineItem> cvgOppLineItems = new List<OpportunityLineItem>();
	List<OpportunityLineItem> rtgOppLineItems = new List<OpportunityLineItem>();
	for(OpportunityLineItem item : Trigger.new){
		if (cvgProjectOpportunities.contains(item.opportunityId)){
			cvgOppLineItems.add(item);
		}
		if (rtgOpportunities.contains(item.opportunityId)){
			rtgOppLineItems.add(item);
		}
	}

	if (cvgOppLineItems!=null && cvgOppLineItems.size()>0){
		GuidUtil.populateMobileId(cvgOppLineItems, 'Mobile_ID__c');
	}

	if (rtgOppLineItems!=null && rtgOppLineItems.size()>0){
		GuidUtil.populateMobileId(rtgOppLineItems, 'Mobile_ID__c');
	}

	
}