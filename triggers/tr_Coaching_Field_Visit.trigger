trigger tr_Coaching_Field_Visit on Coaching_Field_Visit__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
 	
 	if(Trigger.isBefore){   
 		
 		if(Trigger.isUpdate){
 			
 			bl_Coaching_Field_Visit.checkMEATasksOnClosure(Trigger.new, Trigger.newMap);
 		}
 		
 	}else{
 		
 		if(Trigger.isInsert){
 			
 			bl_Coaching_Field_Visit.createShares(Trigger.new, Trigger.oldMap);
 			
 		}else if(Trigger.isUpdate){
 			
 			bl_Coaching_Field_Visit.createShares(Trigger.new, Trigger.oldMap);
 		}		
 	}
}