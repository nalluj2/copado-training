/**
 * Creation Date:   20140220
 * Description:     Trigger to fill checkbox on User if application mobile APDR
 * Author:          Rudy De Coninck
 */

trigger tr_MobileAppAssignment on Mobile_App_Assignment__c (after insert, after undelete, after update, before delete) {

    if(Trigger.isAfter) {
        
        if(Trigger.isInsert){
            
            bl_MobileAppAssignment.updateUserFromAssignment(Trigger.new);           
            bl_MobileAppAssignment.addtoCallRecording(Trigger.newMap);
            bl_MobileAppAssignment.performCrossCheck(Trigger.new);
            
        }else if(Trigger.isUpdate){
            
            bl_MobileAppAssignment.updateUserFromAssignment(Trigger.new);           
            bl_MobileAppAssignment.addtoCallRecording(Trigger.newMap);
            bl_MobileAppAssignment.performCrossCheck(Trigger.new);
            
        }else if(Trigger.isUndelete){   
            
            bl_MobileAppAssignment.performCrossCheck(Trigger.new);
        }
        
    }else{

        if (Trigger.isDelete){

            bl_MobileAppAssignment.preventDelete(Trigger.old);

        }
        
    } 
}