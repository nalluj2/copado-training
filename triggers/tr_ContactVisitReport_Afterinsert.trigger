/*
    Description : This trigger is to create the chatter post for call contact whenever a call record is created
    Author      : Priti Jaiswal
    Created Date : 28/6/2013
*/

trigger tr_ContactVisitReport_Afterinsert on Contact_Visit_Report__c(after insert){
    
    if(UserInfo.getUserType() != 'PowerPartner'){
        
        List<FeedItem> lstChatterPostToInsert=new List<FeedItem>();
        
        set<id> setcallRecordID=new set<id>();
        set<id> setAttedndedContact=new set<Id>();        
        set<Id> setAccountId=new set<Id>();
        set<String> setCountry=new set<String>();
        
        List<Contact_Visit_Report__c> excludeconList=new List<Contact_Visit_Report__c>();
        List<Contact_Visit_Report__c> includeconList=new List<Contact_Visit_Report__c>();
        Map<string,DIB_Country__c> mapofCountry=new Map<String,DIB_Country__c>();
        map<id,ContactFeed> mapContactandPost=new map<id,ContactFeed>();//map to get the chatter post for existing call contact of call Record 
        
        for(Contact_Visit_Report__c CVR:Trigger.new){
            setcallRecordID.add(CVR.Call_Records__c); 
            setAttedndedContact.add(CVR.Attending_Contact__c);           
            setAccountId.add(cvr.Attending_Affiliated_Account__c);
        }  
        
        //get list of Account countries...   
        List<Account> lstAcc=[select id,Account_Country_vs__c from Account where id in:setAccountId];
        for(Account acc:lstAcc){
            setCountry.add(acc.Account_Country_vs__c);
        }  
        List<DIB_Country__c> lstCountry=[select name,Call_Record_Chatter_Post_on_Contact__c from DIB_Country__c where name in:setCountry];
        for(DIB_Country__c country:lstCountry){           
            mapofCountry.put(country.name.toUpperCase(),country);
        }
       
        map<id,Call_Records__c> mapcallRec=new map<id,Call_Records__c>([select id,Owner.name,name,Related_Accounts__c,CreatedDate from Call_Records__c where id in:setcallRecordID]);
        if(setAttedndedContact!=null){
            List<ContactFeed> lstExistingPost=[select parentid,Body,Title from ContactFeed where parentId in:setAttedndedContact and CreatedDate = LAST_N_DAYS:30 ];        
            
            //get all the contacts for which chatter post is already created 
            for(Contact_Visit_Report__c CVR:Trigger.new){
                for(ContactFeed confeed:lstExistingPost){
                    if(confeed.Title==mapcallRec.get(cvr.Call_Records__c).name)
                    mapContactandPost.put(confeed.parentid,confeed);
                }
                
            }  
        }
        
       //exclude those contacts for which chatter post is already created or checkbox(to create chatter post) is not checked at Country level
        for(Contact_Visit_Report__c cvr:[select Call_Records__c,Attending_Contact__c,Attending_Affiliated_Account__c,Attending_Affiliated_Account__r.Account_Country_vs__c from Contact_Visit_Report__c where Call_Records__c in :setcallRecordID]){           
            if(mapContactandPost.get(cvr.Attending_Contact__c)!=null){                
                excludeconList.add(cvr);
            } 
            else if(mapofCountry.get(cvr.Attending_Affiliated_Account__r.Account_Country_vs__c)!=null && mapofCountry.get(cvr.Attending_Affiliated_Account__r.Account_Country_vs__c.toUpperCase()).Call_Record_Chatter_Post_on_Contact__c==true){
                includeconList.add(cvr);
            } 
            
        }
        
        //insert chatter post....
        if(includeconList.size()>0){
            for(Contact_Visit_Report__c cvr:includeconList){                
                FeedItem post = new FeedItem();            
                post.parentId = cvr.Attending_Contact__c;           
                post.Body = mapcallRec.get(cvr.Call_Records__c).Owner.name + ' added this contact as attended contact on call record '+mapcallRec.get(cvr.Call_Records__c).name;
                post.Title =mapcallRec.get(cvr.Call_Records__c).name;         
                post.LinkUrl = '/'+cvr.Call_Records__c;
                lstChatterPostToInsert.add(post);
            }               
        }
        insert lstChatterPostToInsert;
    }
}