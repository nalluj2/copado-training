/*
 *   Apex Trigger:tr_EmailToCaseOwner
 *   Description: This trigger is used to send email to the case owner who is related to the updated task.
 *   Author - Kaushal Singh
 *   Created Date  - 30/05/2013
 *-------------------------------------------------------   
 *   Description	: Rewritten the code so that it first checks if there is a Case connected to the processing Account
 *   Author			: Bart Caelen
 *   Created Date  	: 20140226
*/
trigger tr_EmailToCaseOwner on Task (after update) {

    set<id> setWhatId = new set<id>();
    for (Task oTask : trigger.new){
    	if (oTask.WhatId!=null &&
    		string.valueOf(oTask.WhatId).startsWith(clsUtil.getPrefixFromSObjectName('Case'))){
	        if(oTask.status == 'Completed' && trigger.oldmap.get(oTask.id).status !='Completed'){
	            setWhatId.add(oTask.Whatid);        
	        }
    	}
    }
	
	if (setWhatId.size() > 0){
	    set<string> setRTNames = new set<string>();
	    setRTNames = bl_OMARecordTypes.OMARecordType();
	
	    map<Id,Case> mapOfIdandCase =new map<Id,Case>( [select Id,OwnerId,Owner.Email,RecordType.DeveloperName from Case where Id in:setWhatId and RecordType.DeveloperName in:setRTNames]);

		if (mapOfIdandCase.keyset().size() > 0){
		    String EmailTemplateId;
		    OMAEmailTemplateId__c ETId = [select  Template_Id__c from OMAEmailTemplateId__c limit 1];
		    EmailTemplateId = ETId.Template_Id__c;
		    Messaging.SingleEmailMessage[] lstmail = new Messaging.SingleEmailMessage[]{};
		    for (Task t: Trigger.new){
		        if (t.status=='Completed' && trigger.oldmap.get(t.id).status !='Completed' && mapOfIdandCase.keyset().size() > 0 && userinfo.getuserid() !=mapOfIdandCase.get(t.Whatid).OwnerId){
		            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			            mail.setSaveAsActivity(false);
			            mail.setWhatId(t.id);
			            mail.setTargetObjectId(mapOfIdandCase.get(t.Whatid).OwnerId);
			            mail.setTemplateId(EmailTemplateId);
		            lstmail.add(mail);
		        }
		    }
		    if (lstMail.size()>0){
			    Messaging.sendEmail( lstmail );
		    }
		}
	}
    

/*
    set<id> setWhatId=new set<id>();
    for(Task t:Trigger.new){
        if(t.status=='Completed' && trigger.oldmap.get(t.id).status !='Completed'){
            setWhatId.add(t.Whatid);        
        }
    }
    set<String> setRTNames=new set<String>();
    setRTNames=bl_OMARecordTypes.OMARecordType();
    String EmailTemplateId;
    OMAEmailTemplateId__c ETId=[
                                select  Template_Id__c 
                                from OMAEmailTemplateId__c limit 1
                                ];
    EmailTemplateId=ETId.Template_Id__c;
    map<Id,Case> mapOfIdandCase =new map<Id,Case>( [select Id,OwnerId,Owner.Email,RecordType.DeveloperName from Case where Id in:setWhatId and RecordType.DeveloperName in:setRTNames]);
    Messaging.SingleEmailMessage[] lstmail = new Messaging.SingleEmailMessage[]{};
    for(Task t: Trigger.new){
        if(t.status=='Completed' && trigger.oldmap.get(t.id).status !='Completed' && mapOfIdandCase.keyset().size() > 0 && userinfo.getuserid() !=mapOfIdandCase.get(t.Whatid).OwnerId){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setSaveAsActivity(false);
            mail.setWhatId(t.id);
            mail.setTargetObjectId(mapOfIdandCase.get(t.Whatid).OwnerId);
            mail.setTemplateId(EmailTemplateId);
            lstmail.add(mail);
        }
    }
    if (lstMail.size()>0){
	    Messaging.sendEmail( lstmail );
    }
 */   
    
}