/*
 * Work Item Nr     : WI-156 / WI-150 / WI-130
 * Description      : WI-156: This trigger updates the Primary and Secondary Health Insurer lookup fields with the 
 *                    value of the corresponding pick list. The picklist is used on the WebToLead form.
 *                    WI-150: moved code from tr_Lead_AfterUpdate to this Trigger, as it was Before Insert. Also applies change to get Bucket assignment Rule if no value is found  
 *                    WI-130: Populate Account Id from Contact_ID__c is changed and not blank for Canada record types   
 *                    Moved unfinished code from tr_Lead_AfterUpdateAndAssignment to this trigger, to update DIB Sales Rep Selector if Owner changes.    
 * Author           : Patrick Brinksma
 * Created Date     : 15-07-2013
 */
trigger tr_Lead_BeforeInsertUpdate on Lead (before insert, before update) {
    // Select Leads which have Primary_Health_Insurer_Web__c or Secondary_Health_Insurer_Web__c populated
    List<Lead> listOfHILead = new List<Lead>();
    // Filter Leads that have specific recordtype, a zipcode and not flagged for manual assignment
    List<Lead> listOFAssignLead = new List<Lead>();
    // Filter Leads that need new Account Id of the Physician
    //-BC - 20150209 - CR-7099 - START
    List<Lead> lstLead_Physician = new List<Lead>();    //-BC - 20150209 - CR-7099 - ADDED
    // Set of Postal Codes including bucket for default
    Set<string> setOfPostalCode = new Set<String>{ut_StaticValues.COUNTRY_ASSIGNMENT_BUCKET};
    // Set of Lead Contact Id
    Set<Id> setOfContactId = new Set<Id>();
    
    // Filter Trigger to get Leads to be processed
    for (Lead thisLead : Trigger.new){
        // Leads for which Owner changed, update DIB Sales Rep Selector
        // Validate RecordType
        Boolean isRecordType = (thisLead.RecordTypeId == RecordTypeMedtronic.getRecordTypeByDevName(ut_StaticValues.RTDEVNAME_CAN_DIB_LEAD).Id || thisLead.RecordTypeId == RecordTypeMedtronic.getRecordTypeByDevName(ut_StaticValues.RTDEVNAME_CAN_DIB_LEAD_CONVERSION).Id);
        if (Trigger.isUpdate){
            if (isRecordType && thisLead.OwnerId <> Trigger.oldMap.get(thisLead.Id).OwnerId){
                thisLead.DiB_Sales_Rep_Selector__c = thisLead.OwnerId;
            }
        }
        if (Trigger.isInsert){
            if (isRecordType && (thisLead.Primary_Health_Insurer_Web__c != null || thisLead.Secondary_Health_Insurer_Web__c != null)){
                listOfHILead.add(thisLead);
            }
            if(thisLead.RecordTypeId == RecordTypeMedtronic.getRecordTypeByDevName(ut_StaticValues.RTDEVNAME_CAN_DIB_LEAD).Id && thisLead.Lead_Zip_Postal_Code__c != null && thisLead.Lead_Zip_Postal_Code__c != '' && thisLead.Manually_Assinged_To_IS_Bool__c == false){
                listOFAssignLead.add(thisLead);
                String postalCode = thisLead.Lead_Zip_Postal_Code__c.left(3);
                if (postalCode <> null){
                    postalCode = postalCode.toUpperCase();
                }
                setOfPostalCode.add(postalCode);
            }
        }

        //-BC - 20150209 - CR-7099 - START
        if (
            (Trigger.isInsert && thisLead.Contact_ID__c != null)
            || 
            (Trigger.isUpdate && thisLead.Contact_ID__c != null &&
                ( 
                    (thisLead.Contact_ID__c != Trigger.oldMap.get(thisLead.Id).Contact_ID__c)
                    || 
                    (thisLead.Account_ID__c != Trigger.oldMap.get(thisLead.Id).Account_ID__c)
                )
            )
        ){
            lstLead_Physician.add(thisLead);
        }
        //-BC - 20150209 - CR-7099 - STOP
    }
    if (Trigger.isInsert){
        // Process filtered Health Insurance Leads
        if (!listOfHILead.isEmpty()){
            bl_Lead.populatePrimaryAndSecondaryInsurer(listOfHILead);
        }
        // Process filtered assignment Leads
        if (!listOfAssignLead.isEmpty()){
            bl_Lead.assignLeads(listOfAssignLead, setOfPostalCode);
        }
    }
    //-BC - 20150209 - CR-7099 - START
    // Process filtered Physician Leads
    if (!lstLead_Physician.isEmpty()){
        bl_Lead.verifyAndSetPhysicianAccountId(lstLead_Physician);
    }
    //-BC - 20150209 - CR-7099 - STOP
}