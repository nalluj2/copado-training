//--------------------------------------------------------------------------------------------------------------------
//  Author      : Bart Caelen
//  Version     : 1.0
//  Date        : 20150814
//  Description : This APEX Trigger will create an Application_Usage_Detail__c record when a new Implant__c record 
//                  is created in Salesforce.com by the MMX Interface User.
//                  When a user creates an implant in MMX, this will be seen as a login in Salesforce.com
//--------------------------------------------------------------------------------------------------------------------
trigger tr_Implant_ApplicationUsage on Implant__c (after insert) {

    if (trigger.isAfter){

        if (trigger.isInsert){
            List<Application_Usage_Detail__c> lstInsert_ApplicationUsageDetail = new List<Application_Usage_Detail__c>();
            for (Implant__c oImplant : trigger.new){

                // Do we need to check if the CreatedBy is MMX Interface or can we assume that the record is created on MMX if MMX_Implant_ID_Text__c is populated?
                if (clsUtil.isNull(oImplant.MMX_Implant_ID_Text__c, '') != ''){
                    // Create record in Application_Usage_Detail__c
                    Application_Usage_Detail__c oApplicationUsageDetail = new Application_Usage_Detail__c();
                        oApplicationUsageDetail.Capability__c = 'Login';
                        oApplicationUsageDetail.Source__c = 'MMX';
                        oApplicationUsageDetail.User_ID__c = oImplant.OwnerId;
                        oApplicationUsageDetail.Action_DateTime__c = oImplant.CreatedDate;
                        oApplicationUsageDetail.Action_Date__c = oImplant.CreatedDate.Date();
                        oApplicationUsageDetail.Action_Type__c = 'Create';
                        oApplicationUsageDetail.Object__c = 'Implant';
                    lstInsert_ApplicationUsageDetail.add(oApplicationUsageDetail);
                }
            }

            if (lstInsert_ApplicationUsageDetail.size() > 0){
                Database.insert(lstInsert_ApplicationUsageDetail, false);
            }

        }

    }

}
//--------------------------------------------------------------------------------------------------------------------