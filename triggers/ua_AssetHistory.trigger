/*
*	Trigger to log user actions on Asset_History__c (create / update/ delete)
* 	Author: Rudy De Coninck
*	Creation date: 17-12-2014
*/

trigger ua_AssetHistory on Asset_History__c (after delete, after insert, after undelete, after update) {

	//We do not track bulk operations
	if (Trigger.size == 1){
		String operation='none';
		List<Asset_History__c> recordsToProcess = new List<Asset_History__c>();
		if (Trigger.isDelete){
			operation = 'Delete';		
			for (Asset_History__c a :Trigger.old){
				recordsToProcess.add(a);				
			}  
		}else{
			if (Trigger.isInsert){
				operation = 'Insert';
			}
			if(Trigger.isUpdate){
				operation='Update';
			}
			for (Asset_History__c a :Trigger.new){
				recordsToProcess.add(a);				
			}  
		}
		if(recordsToProcess.size()>0){
			for(Asset_History__c a : recordsToProcess){
				//Only log initial action
				if (!bl_UserAction.userActionInProgress){
					bl_UserAction.logObjectWithParent(UserInfo.getUserId(), operation,'Asset_History__c',a.id,'',a.Asset__c,'Asset');
					bl_UserAction.userActionInProgress = true;  
				} 
			}
		}
	} 
}