trigger tr_Activity_Scheduling_Attendee on Activity_Scheduling_Attendee__c (after delete, after insert, after undelete, after update, before insert, before update) {
	
	if(Trigger.isBefore){
		
		if(Trigger.isInsert){
			
			bl_Activity_Scheduling_Attendee.checkAttendeeNotAssignedTo(Trigger.new);
			
			bl_Activity_Scheduling_Attendee.setExternalKey(Trigger.new);
			
		}else if(Trigger.isUpdate){
			
			bl_Activity_Scheduling_Attendee.checkAttendeeNotAssignedTo(Trigger.new);
			
			bl_Activity_Scheduling_Attendee.setExternalKey(Trigger.new);			
		}
		
	}else{
		
		if(Trigger.isInsert){
			
			bl_Activity_Scheduling_Attendee.manageEventAttendee(Trigger.new, null);
			
			bl_Activity_Scheduling_Attendee.manageRecurringAttendee(Trigger.new, null, 'INSERT');
			
		}else if(Trigger.isUpdate){
			
			bl_Activity_Scheduling_Attendee.manageEventAttendee(Trigger.new, Trigger.oldMap);
			
			bl_Activity_Scheduling_Attendee.manageRecurringAttendee(Trigger.new, Trigger.oldMap, 'UPDATE');
									
		}else if(Trigger.isDelete){
			
			bl_Activity_Scheduling_Attendee.manageEventAttendee(Trigger.old, null);
			
			bl_Activity_Scheduling_Attendee.manageRecurringAttendee(Trigger.old, null, 'DELETE');
									
		}else if(Trigger.isUndelete){
			
			bl_Activity_Scheduling_Attendee.manageEventAttendee(Trigger.new, null);
			
			bl_Activity_Scheduling_Attendee.manageRecurringAttendee(Trigger.new, null, 'UNDELETE');
									
		}
	}    
}