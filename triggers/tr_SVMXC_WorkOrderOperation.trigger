//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 05-01-2016
//  Description      : APEX Trigger on SVMXC_Work_Order_Operation__c 
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
trigger tr_SVMXC_WorkOrderOperation on SVMXC_Work_Order_Operation__c (before insert, before update, before delete, after insert, after update, after delete, after undelete){

	// Pass the Trigger newMap and oldMap to the APEX Class (TriggerHandler)
	bl_SVMXC_WorkOrderOperation_Trigger.lstTriggerNew = Trigger.new;
	bl_SVMXC_WorkOrderOperation_Trigger.mapTriggerNew = Trigger.newMap;
	bl_SVMXC_WorkOrderOperation_Trigger.mapTriggerOld = Trigger.oldMap;


	if (Trigger.isBefore){

		if (Trigger.isUpdate){

			bl_SVMXC_WorkOrderOperation_Trigger.preventUpdateOnRestrictedFields('UPDATE');

		}

	}else if (Trigger.isAfter){


	}

}
//--------------------------------------------------------------------------------------------------------------------