trigger ProductBeforeDelete on Product2 (before delete) {
     // Check if current Product is there in the Therapy Product, Call Topic Products...        
     boolean recCount=false;        
     List<Therapy_Product__c> lstProd = [Select s.id From Therapy_Product__c s where s.Product__c in : Trigger.old LIMIT 1];        
     if(lstProd.size()>0)        
     {            
         recCount=true;            
     }  
     List<Call_Topic_Products__c> lstCTProd = [Select s.id From Call_Topic_Products__c s where s.Product__c in : Trigger.old LIMIT 1];        
     if(lstCTProd.size()>0)        
     {            
         recCount=true;            
     }  
     List<Asset> lstAsset = [Select id,Product2Id From Asset where Product2Id in : Trigger.old LIMIT 1];        
     if(lstAsset.size()>0)        
     {            
         recCount=true;            
     }  
    if(recCount==true)        
    {            
        trigger.old[0].name.addError('This Product is present in Therapy Product or Call Topic Products or Assets. Please delete the child records and then try again.');            
    }
}