/*
 *      Created Date : 23-1-2014
 *      Description : Trigger to centralize before logic for the User object
 *
 *      Author = Rudy De Coninck
 */
//------------------------------------------------------------------------------------------------------------------------------
//  Author      : Bart Caelen
//  Date        : 20151021
//  Description : CR-8006
//                  Executed logic in bl_User.updateUserFields
//------------------------------------------------------------------------------------------------------------------------------
//  Author      : Bart Caelen
//  Date        : 20160418
//  Description : CR-11646 (MITG Project)
//                  Added AFTER Insert, AFTER Update logic to set the correct public group assignments 
//------------------------------------------------------------------------------------------------------------------------------
trigger tr_User on User (before insert, before update, after update) {

    if (Trigger.isBefore){

		bl_user.updateUserWithLicense(Trigger.new);
		bl_user.updateUserWithLastApplicationLoginStatus(Trigger.new);

	    bl_User.updateUserFields(trigger.new, false);
	    
	    bl_User.countryFieldsSync(trigger.New, trigger.OldMap);

	}else if (Trigger.isAfter){

		bl_User_Trigger.lstTriggerNew = Trigger.new;
		bl_User_Trigger.mapTriggerNew = Trigger.newMap;
		bl_User_Trigger.mapTriggerOld = Trigger.oldMap;

		if (Trigger.isUpdate){

			bl_User_Trigger.setPublicGroupAssignment_Update();

		}

	}
}
//------------------------------------------------------------------------------------------------------------------------------