/*
*	Trigger to log user actions on Tender_Calendar_2__c (create / update/ delete)
* 	Author: Rudy De Coninck
*	Creation date: 17-12-2014
*/

trigger ua_TenderCalendar2 on Tender_Calendar_2__c (after delete, after insert, after undelete, after update) {

	//We do not track bulk operations
	if (Trigger.size == 1){
		String operation='none';
		List<Tender_Calendar_2__c> recordsToProcess = new List<Tender_Calendar_2__c>();
		if (Trigger.isDelete){
			operation = 'Delete';		
			for (Tender_Calendar_2__c a :Trigger.old){
				recordsToProcess.add(a);				
			}  
		}else{
			if (Trigger.isInsert){
				operation = 'Insert';
			}
			if(Trigger.isUpdate){
				operation='Update';
			}
			for (Tender_Calendar_2__c a :Trigger.new){
				recordsToProcess.add(a);				
			}  
		}
		if(recordsToProcess.size()>0){
			for(Tender_Calendar_2__c a : recordsToProcess){
				//Only log initial action
				if (!bl_UserAction.userActionInProgress){
					bl_UserAction.logObjectWithParent(UserInfo.getUserId(), operation,'Tender_Calendar_2__c',a.id,'',a.Account_Plan_ID__c,'Account_Plan_2__c');
					bl_UserAction.userActionInProgress = true;  
				} 
			}
		}
	} 
}