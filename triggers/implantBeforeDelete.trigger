/**
 * Creation Date :  20090325
 * Modify Date   :  20100706
 * Description :    Trigger : Before delete: If Implant is a MMX Implaint then it can not be deleted. 
 *
 * Author :         Dheeraj - Wipro
 */
 
trigger implantBeforeDelete on Implant__c (before delete) {
    // Check If Implant is an MMX Implant, if yes thn Implance cannot be deleted else
    // Delete Call record as well along with Implant.
    
    list<id> ImpCallRecordsList = new list<id>();
    for(Integer i=0; i<trigger.old.size(); i++){
        if(trigger.old[i].MMX_Implant_ID_Text__c!=null){
            trigger.old[i].name.addError('MMX Implants can not be deleted.');                       
        }
        else {
            ImpCallRecordsList.add(trigger.old[i].id);  
        }
    } 
    if(ImpCallRecordsList.size()>0){
        // Delete Asset records    
        //list<Asset> Asset=[select id from Asset where Implant_ID__c in  :ImpCallRecordsList];
        //if(Asset.size()>0){
        //    delete Asset;
        //}       
        list<Implant__c> ImpCallRecords=[select Call_Record_ID__c from Implant__c where id in :ImpCallRecordsList];
        if(ImpCallRecords.size()>0){
            list<id> CallRecordIds=new list<id>();
            for(Implant__c i:ImpCallRecords){
                if(i.Call_Record_ID__c!=null){                  
                    CallRecordIds.add(i.Call_Record_ID__c);                         
                }
            }
            System.Debug('>>>>>>>>>>>>>>>>CallRecordIds - ' + CallRecordIds);
            if(CallRecordIds.size()>0){
                list<Call_Records__c> CallRecordList=[select id from Call_Records__c where id in : CallRecordIds];
                System.Debug('>>>>>>>>>>>>>>>>CallRecordList - ' + CallRecordList);
                if(CallRecordList.size()>0){
                    try{                    
                        Delete CallRecordList;                  
                    } catch(Exception ex){ }
                    
                }
                                    
            }
        }       
    }         
    
}