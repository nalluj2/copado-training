//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 28-03-2017
//  Description      : Unified trigger for Create_User_Request__c (Support Portal)
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
trigger tr_CreateUserRequest on Create_User_Request__c (before update) {

	if (Trigger.isBefore){

		if (Trigger.isBefore){

			bl_CreateUserRequest_Trigger.processStatusChange(Trigger.new, Trigger.oldMap);

		}

	}

}
//--------------------------------------------------------------------------------------------------------------------