trigger tr_UMD_Purpose_Question on UMD_Purpose_Question__c (before insert, before update) {
    
    for(UMD_Purpose_Question__c purposeQuestion : Trigger.new){
    	
    	purposeQuestion.Unique_Sequence__c = purposeQuestion.UMD_Purpose__c + ':' + purposeQuestion.Sequence__c;
    }
}