trigger CallCategoryBeforeUpsert on Call_Category__c (before insert, before update, before delete) {
 
    /*if(Trigger.isInsert || Trigger.isUpdate)
    {
        Call_Category__c[] CallCat = Trigger.new;
        CallCat[0].Call_Category__c = CallCat[0].Name;
    }*/
    if(Trigger.isDelete)
    {
        // Check if current record Call category is there in the Call Topic or Call Topic Settings...
        boolean recCount=false;
        List<Call_Topics__c> lstSubject = [Select s.id From Call_Topics__c s where s.Call_Category__c in :  Trigger.old LIMIT 1];
        if(lstSubject.size()>0)
        {
            recCount=true;    
        }  
        
        List<Call_Topic_Settings__c> lstSubjectDetails = [Select s.id From Call_Topic_Settings__c s where s.Call_Category__c in :  Trigger.old LIMIT 1];
        if(lstSubjectDetails.size()>0)
        {
            recCount=true;    
        }  
              
        if(recCount==true)
        {
            trigger.old[0].name.addError('This Call Category is present in Call Topics or Call Topic Setting. Please delete the child records and then try again.');    
        }
    }
    
}