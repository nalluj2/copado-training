trigger tr_UMD_Input_Complete on UMD_Input_Complete__c (before insert, before update, before delete, after insert, after update) {
    
    if(Trigger.isBefore){
    	
    	if(Trigger.isInsert){
    		
    		bl_Customer_Segmentation.isDMLAllowed(Trigger.new);
    		
    		bl_UMD_Input_Complete_Trigger.setUniqueKey(Trigger.new, null);
    		
    		bl_UMD_Input_Complete_Trigger.matchSegmentation(Trigger.new, null);
    	
    	}else if(Trigger.isUpdate){
    		
    		bl_Customer_Segmentation.isDMLAllowed(Trigger.new);
    		
    		bl_UMD_Input_Complete_Trigger.setUniqueKey(Trigger.new, Trigger.oldMap);
    		
    		bl_UMD_Input_Complete_Trigger.matchSegmentation(Trigger.new, Trigger.oldMap);
    		
    	}else if(Trigger.isDelete){
    		
    		bl_Customer_Segmentation.isDMLAllowed(Trigger.old);
    	}
    	    	
    }else{
    	
    	if(Trigger.isInsert){
    		
    		bl_UMD_Input_Complete_Trigger.manageSharing(Trigger.new, null);
    		
    	}else if(Trigger.isUpdate){
    		
    		bl_UMD_Input_Complete_Trigger.manageSharing(Trigger.new, Trigger.oldMap);
    	}
    }
}