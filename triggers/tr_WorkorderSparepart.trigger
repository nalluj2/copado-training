//----------------------------------------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 01-11-2016
//  Description      : APEX Trigger on Workorder_Sparepart__c 
//  Change Log       : 
//----------------------------------------------------------------------------------------------------------------------------------------------------
trigger tr_WorkorderSparepart on Workorder_Sparepart__c (after insert, after update) {

	if (Trigger.isAfter){

		if (Trigger.isInsert){

			bl_WorkorderSparepart_Trigger.updateRelatedAsset(Trigger.new, Trigger.oldMap);

		}else if (Trigger.isUpdate){

			bl_WorkorderSparepart_Trigger.updateRelatedAsset(Trigger.new, Trigger.oldMap);

		}

	}

}
//----------------------------------------------------------------------------------------------------------------------------------------------------