//--------------------------------------------------------------------------------------------------------------------
//  Author           : dijkea2
//  Created Date     : 09-02-2017
//  Description      : APEX Trigger on Opportunity 
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 29-05-2017
//  Description      : Converted APEX Trigger to unified trigger on Opportunity
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
trigger tr_OpportunityTeamMember on OpportunityTeamMember (before delete, after insert) {


    if (Trigger.isBefore){

    	if (Trigger.isDelete){

    		bl_OpportunityTeamMember_Trigger.preventDeleteOppOwner(Trigger.old);
    		
    	}

    }else{
    	
    	if(Trigger.isInsert){
    		
    		bl_OpportunityTeamMember_Trigger.giveEditAccess(Trigger.new);
    	}
    }
    

}
//--------------------------------------------------------------------------------------------------------------------