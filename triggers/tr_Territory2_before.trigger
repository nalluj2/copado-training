trigger tr_Territory2_before on Territory2 (before insert, before update) {

	for(Territory2 ter : Trigger.New){
		
		if(Trigger.isInsert){
			
			if(ter.Devart_Therapy_Groups__c != null) ter.Therapy_Groups_Text__c = ter.Devart_Therapy_Groups__c;
			else if(ter.Therapy_Groups_Text__c != null) ter.Devart_Therapy_Groups__c = ter.Therapy_Groups_Text__c;
						
		}else if(Trigger.isUpdate){
			
			if(ter.Devart_Therapy_Groups__c != Trigger.OldMap.get(ter.Id).Devart_Therapy_Groups__c){
				
				ter.Therapy_Groups_Text__c = ter.Devart_Therapy_Groups__c;
				
			}else if(ter.Therapy_Groups_Text__c != Trigger.OldMap.get(ter.Id).Therapy_Groups_Text__c || ter.Devart_Therapy_Groups__c == null){
				
				ter.Devart_Therapy_Groups__c = ter.Therapy_Groups_Text__c;
			}
		}
		
		// Set exclude from automatic alignment
		if(ter.Exclude_from_sales_structure_alignment__c == false && ter.External_Key__c == null && 
			(ter.Territory2TypeId == bl_Territory.territoryTypeMap.get('SalesForce') || ter.Territory2TypeId == bl_Territory.territoryTypeMap.get('District') || ter.Territory2TypeId == bl_Territory.territoryTypeMap.get('Territory'))
			&& ter.Business_Unit__c != 'Diabetes'){
				
			ter.Exclude_from_sales_structure_alignment__c = true;
		}
	}
}