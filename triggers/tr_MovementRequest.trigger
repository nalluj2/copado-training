//--------------------------------------------------------------------------------------------------------------------
//  Author           : Bart Caelen
//  Created Date     : 03-05-2016
//  Description      : APEX Trigger on Movement_Request__c 
//  Change Log       : 
//--------------------------------------------------------------------------------------------------------------------
trigger tr_MovementRequest on Movement_Request__c (before insert, after insert) {

	// Pass the Trigger newMap and oldMap to the APEX Class (TriggerHandler)
	bl_MovementRequest_Trigger.lstTriggerNew = Trigger.new;
	bl_MovementRequest_Trigger.mapTriggerNew = Trigger.newMap;
	bl_MovementRequest_Trigger.mapTriggerOld = Trigger.oldMap;


	if (Trigger.isBefore){

		if (Trigger.isInsert){

			bl_MovementRequest_Trigger.validateProductLoanToOtherHospital();

		}
    
	}else if (Trigger.isAfter){

		if (trigger.isInsert){

			bl_MovementRequest_Trigger.addProductManagerToOpportunityTeam();
		}
	}

}
//--------------------------------------------------------------------------------------------------------------------