/*
 * Work Item Nr		: WI-204
 * Description		: WI-204: Copied logic from trgUpdateTC: The written Trigger will update the Currency 
 *                    of the created or updated Tender Calendar based on the selected Account.
 * Author        	: Patrick Brinksma
 * Created Date    	: 23-07-2013
 */
trigger tr_Tender_Calendar_2_AfterInsertUpdate on Tender_Calendar_2__c (after insert, after update, after delete, after undelete) {
	
	if(Trigger.isInsert || Trigger.isUpdate){
		list<id> acIds = new list<id>();
		list<String> strCountry = new list<String>();
		map<id,string> mpAccCountry = new map<id, string>();
		list<id> lstAPNew = new list<id>();
		map<id,id> mpAccPer = new map<id, id>();
		map<id,string> mpAccIDCountry = new map<id, string>();
		list<Tender_Calendar_2__c> lstTC = new list<Tender_Calendar_2__c>();
		
		 for(Tender_Calendar_2__c a:trigger.new)
		 {  
		 	acIds.add(a.APAccount__c);  
		 }
		
		account[] acData = [select id,CurrencyIsoCode,Account_Country_vs__c from account where id in :acIds];
		for(account ac:acData)
		{
			mpAccCountry.put(ac.id,ac.CurrencyIsoCode);
		}
		
		for(Tender_Calendar_2__c  t: trigger.new)
		{
			lstAPNew.add(t.id); 
			mpAccPer.put(t.id,t.APAccount__c);
			mpAccIDCountry.put(t.APAccount__c,t.CurrencyIsoCode);
		}
		
		for(integer i=0; i<lstAPNew.size();i++)
		{
		    id accId =  mpAccPer.get(lstAPNew.get(i));
		    string curId =  mpAccCountry.get(accId);
		    Tender_Calendar_2__c tenderCal = new Tender_Calendar_2__c(id=lstAPNew.get(i),CurrencyIsoCode=curId);
		    if(mpAccCountry.get(accId)!='' && mpAccCountry.get(accId)!=mpAccIDCountry.get(accID))      
		    {
		        lstTC.add(tenderCal);
		    }
		}
		if(lstTC.size()>0)
		{
		   update lstTC;     
		}
	}
	
	List<Tender_Calendar_2__c> records;
	
	if(Trigger.isInsert || Trigger.isUpdate || Trigger.isUnDelete) records = Trigger.new;
	else records = Trigger.old;
			
	//Update Parent Account Plan 2 to update the Last Modified Date/By fields. If the user is SA we skip this functionality
	if(ProfileMedtronic.isSystemAdministratorProfile(UserInfo.getProfileId()) == false){
		
		Set<Id> parentPlans = new Set<Id>();
		
		for(Tender_Calendar_2__c record : records){
			parentPlans.add(record.Account_Plan_ID__c);
		}
		
		if(parentPlans.size()>0) bl_AccountPlanning.simpleUpdateParentAccountPlan2(parentPlans);
	}
	
	String udAction;
	if (trigger.isInsert){			
		udAction = 'Create';
	}else if (trigger.isDelete){	
		udAction = 'Delete';		
	}else if (trigger.isUpdate){
		udAction = 'Update';			
	}
	
	//Application usage details	
	if(udAction == null) return;
	
	List<Application_Usage_Detail__c> usageDetails = new List<Application_Usage_Detail__c>();
	
	for(Tender_Calendar_2__c record : records){
							
		Application_Usage_Detail__c usageDetail = new Application_Usage_Detail__c();			
		usageDetail.Action_Date__c = Date.today();
		usageDetail.Action_DateTime__c = Datetime.now();
		usageDetail.Action_Type__c = udAction;
		usageDetail.Capability__c = 'Account Plan';
		usageDetail.External_Record_Id__c = record.Id;
		usageDetail.Internal_Record_Id__c = record.Id;
		usageDetail.Object__c = 'Tender_Calendar_2__c';
		usageDetail.Process_Area__c = 'Account Planning';
		usageDetail.Source__c = 'SFDC';
		usageDetail.User_Id__c = UserInfo.getUserId();
					
		usageDetails.add(usageDetail);
	}	
	
	if(usageDetails.size()>0) insert usageDetails;
}