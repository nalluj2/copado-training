trigger CTSDBeforeUpsert on Call_topic_Setting_Details__c (before insert,before update) {
    boolean recCount=false;
    for(Integer i=0; i<trigger.new.size(); i++){
     if(trigger.isInsert)
     {      
        List<Call_topic_Setting_Details__c> CTSD1 = [SELECT Id FROM Call_topic_Setting_Details__c WHERE Call_Topic_Setting__c =: trigger.new[i].Call_Topic_Setting__c and 
                                            Subject__c =: trigger.new[i].Subject__c limit 1];                                      
            if(CTSD1.size()>0)
            {
                recCount=true;    
                break;
            }
     }
     else if(trigger.isUpdate)
     {
        List<Call_topic_Setting_Details__c> CTSD1 = [SELECT Id FROM Call_topic_Setting_Details__c WHERE Call_Topic_Setting__c =: trigger.new[i].Call_Topic_Setting__c and 
                                            Subject__c =: trigger.new[i].Subject__c and id <> :trigger.new[i].ID limit 1];                                      
            if(CTSD1.size()>0)
            {
                recCount=true;    
                break;
            }     
     }               
    }            
     if(recCount==true){            
         trigger.new[0].addError('This Subject is already present in the Existing Call Topic Setting ');            
     }
}