/*
 * Description		: Create Mobile Id if null
 * Author        	: Rudy De Coninck
 * Created Date    	: 11-10-2013
 */

trigger tr_TaskBefore on Task (before insert, before update) {
	
	List<Task> tasksToProcess = new List<Task>();
	
	for(Task t : Trigger.new){
		
		//In case a recurring series is edited the occurrences get the same mobile id at creation, to avoid this we nullify the mobile id 
		if(t.mobile_id__c!= null && bl_Task.processedMobileIds.contains(t.mobile_id__c)){
			t.mobile_id__c=null;
		}
		tasksToProcess.add(t);

	}
	if (tasksToProcess!=null && tasksToProcess.size()>0){
		GuidUtil.populateMobileId(tasksToProcess, 'Mobile_ID__c');
		
		for(Task t : tasksToProcess){
			bl_Task.processedMobileIds.add(t.mobile_id__c);
		}
	}
	
}