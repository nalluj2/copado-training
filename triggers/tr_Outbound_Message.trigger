trigger tr_Outbound_Message on Outbound_Message__c (after insert, before insert) {
	
	if(Trigger.isBefore){
		
		if(Trigger.isInsert){
			
			bl_Outbound_Message.populateNotificationGrouping(Trigger.new);
		}	
		
	}else{
		
		if(Trigger.isInsert){
			
			bl_Outbound_Message.updateNotificationGrouping(Trigger.new);
		}
	}  
}