/*
 *      Description : handle all before related items
 *
 *      Author = Rudy De Coninck
*/

trigger tr_CallTopicSubjectBefore on Call_Topic_Subject__c (before insert) {

	for (Call_Topic_Subject__c ct : Trigger.new){
		if (ct.mobile_Id__c==null){
			ct.Mobile_ID__c = GuidUtil.NewGuid();	
		}
		
	}

}